//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__SourceA(
  input         rf_reset,
  input         clock,
  input         reset,
  output        io_req_ready,
  input         io_req_valid,
  input  [17:0] io_req_bits_tag,
  input  [9:0]  io_req_bits_set,
  input  [2:0]  io_req_bits_param,
  input  [2:0]  io_req_bits_source,
  input         io_req_bits_block,
  input         io_a_ready,
  output        io_a_valid,
  output [2:0]  io_a_bits_opcode,
  output [2:0]  io_a_bits_param,
  output [2:0]  io_a_bits_source,
  output [35:0] io_a_bits_address
);
  wire  io_a_q_rf_reset; // @[Decoupled.scala 296:21]
  wire  io_a_q_clock; // @[Decoupled.scala 296:21]
  wire  io_a_q_reset; // @[Decoupled.scala 296:21]
  wire  io_a_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  io_a_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] io_a_q_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] io_a_q_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] io_a_q_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [35:0] io_a_q_io_enq_bits_address; // @[Decoupled.scala 296:21]
  wire  io_a_q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  io_a_q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] io_a_q_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] io_a_q_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] io_a_q_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [35:0] io_a_q_io_deq_bits_address; // @[Decoupled.scala 296:21]
  wire [33:0] a_bits_address_base = {io_req_bits_tag,io_req_bits_set,6'h0}; // @[Cat.scala 30:58]
  wire  a_bits_address_lo_lo_lo_lo_lo = a_bits_address_base[0]; // @[Parameters.scala 242:72]
  wire  a_bits_address_lo_lo_lo_lo_hi = a_bits_address_base[1]; // @[Parameters.scala 242:72]
  wire  a_bits_address_lo_lo_lo_hi_lo = a_bits_address_base[2]; // @[Parameters.scala 242:72]
  wire  a_bits_address_lo_lo_lo_hi_hi = a_bits_address_base[3]; // @[Parameters.scala 242:72]
  wire  a_bits_address_lo_lo_hi_lo_lo = a_bits_address_base[4]; // @[Parameters.scala 242:72]
  wire  a_bits_address_lo_lo_hi_lo_hi = a_bits_address_base[5]; // @[Parameters.scala 242:72]
  wire  a_bits_address_lo_lo_hi_hi_hi_hi = a_bits_address_base[6]; // @[Parameters.scala 242:72]
  wire  a_bits_address_lo_hi_lo_lo_lo = a_bits_address_base[7]; // @[Parameters.scala 242:72]
  wire  a_bits_address_lo_hi_lo_lo_hi = a_bits_address_base[8]; // @[Parameters.scala 242:72]
  wire  a_bits_address_lo_hi_lo_hi_lo = a_bits_address_base[9]; // @[Parameters.scala 242:72]
  wire  a_bits_address_lo_hi_lo_hi_hi = a_bits_address_base[10]; // @[Parameters.scala 242:72]
  wire  a_bits_address_lo_hi_hi_lo_lo = a_bits_address_base[11]; // @[Parameters.scala 242:72]
  wire  a_bits_address_lo_hi_hi_lo_hi = a_bits_address_base[12]; // @[Parameters.scala 242:72]
  wire  a_bits_address_lo_hi_hi_hi_lo = a_bits_address_base[13]; // @[Parameters.scala 242:72]
  wire  a_bits_address_lo_hi_hi_hi_hi_lo = a_bits_address_base[14]; // @[Parameters.scala 242:72]
  wire  a_bits_address_lo_hi_hi_hi_hi_hi = a_bits_address_base[15]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_lo_lo_lo_lo = a_bits_address_base[16]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_lo_lo_lo_hi = a_bits_address_base[17]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_lo_lo_hi_lo = a_bits_address_base[18]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_lo_lo_hi_hi = a_bits_address_base[19]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_lo_hi_lo_lo = a_bits_address_base[20]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_lo_hi_lo_hi = a_bits_address_base[21]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_lo_hi_hi_lo = a_bits_address_base[22]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_lo_hi_hi_hi_lo = a_bits_address_base[23]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_lo_hi_hi_hi_hi = a_bits_address_base[24]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_hi_lo_lo_lo = a_bits_address_base[25]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_hi_lo_lo_hi = a_bits_address_base[26]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_hi_lo_hi_lo = a_bits_address_base[27]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_hi_lo_hi_hi = a_bits_address_base[28]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_hi_hi_lo_lo = a_bits_address_base[29]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_hi_hi_lo_hi = a_bits_address_base[30]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_hi_hi_hi_lo = a_bits_address_base[31]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_hi_hi_hi_hi_lo = a_bits_address_base[32]; // @[Parameters.scala 242:72]
  wire  a_bits_address_hi_hi_hi_hi_hi_hi = a_bits_address_base[33]; // @[Parameters.scala 242:72]
  wire [8:0] a_bits_address_lo_lo = {a_bits_address_lo_lo_hi_hi_hi_hi,1'h0,1'h0,a_bits_address_lo_lo_hi_lo_hi,
    a_bits_address_lo_lo_hi_lo_lo,a_bits_address_lo_lo_lo_hi_hi,a_bits_address_lo_lo_lo_hi_lo,
    a_bits_address_lo_lo_lo_lo_hi,a_bits_address_lo_lo_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [17:0] a_bits_address_lo = {a_bits_address_lo_hi_hi_hi_hi_hi,a_bits_address_lo_hi_hi_hi_hi_lo,
    a_bits_address_lo_hi_hi_hi_lo,a_bits_address_lo_hi_hi_lo_hi,a_bits_address_lo_hi_hi_lo_lo,
    a_bits_address_lo_hi_lo_hi_hi,a_bits_address_lo_hi_lo_hi_lo,a_bits_address_lo_hi_lo_lo_hi,
    a_bits_address_lo_hi_lo_lo_lo,a_bits_address_lo_lo}; // @[Cat.scala 30:58]
  wire [8:0] a_bits_address_hi_lo = {a_bits_address_hi_lo_hi_hi_hi_hi,a_bits_address_hi_lo_hi_hi_hi_lo,
    a_bits_address_hi_lo_hi_hi_lo,a_bits_address_hi_lo_hi_lo_hi,a_bits_address_hi_lo_hi_lo_lo,
    a_bits_address_hi_lo_lo_hi_hi,a_bits_address_hi_lo_lo_hi_lo,a_bits_address_hi_lo_lo_lo_hi,
    a_bits_address_hi_lo_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [17:0] a_bits_address_hi = {a_bits_address_hi_hi_hi_hi_hi_hi,a_bits_address_hi_hi_hi_hi_hi_lo,
    a_bits_address_hi_hi_hi_hi_lo,a_bits_address_hi_hi_hi_lo_hi,a_bits_address_hi_hi_hi_lo_lo,
    a_bits_address_hi_hi_lo_hi_hi,a_bits_address_hi_hi_lo_hi_lo,a_bits_address_hi_hi_lo_lo_hi,
    a_bits_address_hi_hi_lo_lo_lo,a_bits_address_hi_lo}; // @[Cat.scala 30:58]
  RHEA__Queue_120 io_a_q ( // @[Decoupled.scala 296:21]
    .rf_reset(io_a_q_rf_reset),
    .clock(io_a_q_clock),
    .reset(io_a_q_reset),
    .io_enq_ready(io_a_q_io_enq_ready),
    .io_enq_valid(io_a_q_io_enq_valid),
    .io_enq_bits_opcode(io_a_q_io_enq_bits_opcode),
    .io_enq_bits_param(io_a_q_io_enq_bits_param),
    .io_enq_bits_source(io_a_q_io_enq_bits_source),
    .io_enq_bits_address(io_a_q_io_enq_bits_address),
    .io_deq_ready(io_a_q_io_deq_ready),
    .io_deq_valid(io_a_q_io_deq_valid),
    .io_deq_bits_opcode(io_a_q_io_deq_bits_opcode),
    .io_deq_bits_param(io_a_q_io_deq_bits_param),
    .io_deq_bits_source(io_a_q_io_deq_bits_source),
    .io_deq_bits_address(io_a_q_io_deq_bits_address)
  );
  assign io_a_q_rf_reset = rf_reset;
  assign io_req_ready = io_a_q_io_enq_ready; // @[SourceA.scala 49:15 Decoupled.scala 299:17]
  assign io_a_valid = io_a_q_io_deq_valid; // @[SourceA.scala 50:8]
  assign io_a_bits_opcode = io_a_q_io_deq_bits_opcode; // @[SourceA.scala 50:8]
  assign io_a_bits_param = io_a_q_io_deq_bits_param; // @[SourceA.scala 50:8]
  assign io_a_bits_source = io_a_q_io_deq_bits_source; // @[SourceA.scala 50:8]
  assign io_a_bits_address = io_a_q_io_deq_bits_address; // @[SourceA.scala 50:8]
  assign io_a_q_clock = clock;
  assign io_a_q_reset = reset;
  assign io_a_q_io_enq_valid = io_req_valid; // @[SourceA.scala 49:15 SourceA.scala 53:11]
  assign io_a_q_io_enq_bits_opcode = io_req_bits_block ? 3'h6 : 3'h7; // @[SourceA.scala 56:24]
  assign io_a_q_io_enq_bits_param = io_req_bits_param; // @[SourceA.scala 49:15 SourceA.scala 57:18]
  assign io_a_q_io_enq_bits_source = io_req_bits_source; // @[SourceA.scala 49:15 SourceA.scala 59:18]
  assign io_a_q_io_enq_bits_address = {a_bits_address_hi,a_bits_address_lo}; // @[Cat.scala 30:58]
  assign io_a_q_io_deq_ready = io_a_ready; // @[SourceA.scala 50:8]
endmodule
