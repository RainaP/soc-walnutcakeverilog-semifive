//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_68_assert(
  input         clock,
  input         reset,
  input         io_in_a_ready,
  input         io_in_a_valid,
  input  [2:0]  io_in_a_bits_opcode,
  input  [2:0]  io_in_a_bits_param,
  input  [3:0]  io_in_a_bits_size,
  input  [3:0]  io_in_a_bits_source,
  input  [35:0] io_in_a_bits_address,
  input  [7:0]  io_in_a_bits_mask,
  input         io_in_a_bits_corrupt,
  input         io_in_b_ready,
  input         io_in_b_valid,
  input  [1:0]  io_in_b_bits_param,
  input  [3:0]  io_in_b_bits_source,
  input  [35:0] io_in_b_bits_address,
  input         io_in_c_ready,
  input         io_in_c_valid,
  input  [2:0]  io_in_c_bits_opcode,
  input  [2:0]  io_in_c_bits_param,
  input  [3:0]  io_in_c_bits_size,
  input  [3:0]  io_in_c_bits_source,
  input  [35:0] io_in_c_bits_address,
  input         io_in_c_bits_corrupt,
  input         io_in_d_ready,
  input         io_in_d_valid,
  input  [2:0]  io_in_d_bits_opcode,
  input  [1:0]  io_in_d_bits_param,
  input  [3:0]  io_in_d_bits_size,
  input  [3:0]  io_in_d_bits_source,
  input  [3:0]  io_in_d_bits_sink,
  input         io_in_d_bits_denied,
  input         io_in_d_bits_corrupt,
  input         io_in_e_ready,
  input         io_in_e_valid,
  input  [3:0]  io_in_e_bits_sink
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire  _T_4; // @[Parameters.scala 47:9]
  wire  _T_5; // @[Parameters.scala 47:9]
  wire  _T_10; // @[Parameters.scala 57:34]
  wire  _T_12; // @[Parameters.scala 58:20]
  wire  _T_13; // @[Parameters.scala 57:50]
  wire  _T_15; // @[Parameters.scala 924:46]
  wire  _T_16; // @[Parameters.scala 924:46]
  wire [22:0] _T_18; // @[package.scala 189:77]
  wire [7:0] _T_20; // @[package.scala 189:46]
  wire [35:0] _GEN_33; // @[Edges.scala 22:16]
  wire [35:0] _T_21; // @[Edges.scala 22:16]
  wire  _T_22; // @[Edges.scala 22:24]
  wire [3:0] _T_25; // @[OneHot.scala 65:12]
  wire [2:0] _T_27; // @[Misc.scala 201:81]
  wire  _T_28; // @[Misc.scala 205:21]
  wire  _T_31; // @[Misc.scala 210:20]
  wire  _T_33; // @[Misc.scala 214:38]
  wire  _T_34; // @[Misc.scala 214:29]
  wire  _T_36; // @[Misc.scala 214:38]
  wire  _T_37; // @[Misc.scala 214:29]
  wire  _T_40; // @[Misc.scala 210:20]
  wire  _T_41; // @[Misc.scala 213:27]
  wire  _T_42; // @[Misc.scala 214:38]
  wire  _T_43; // @[Misc.scala 214:29]
  wire  _T_44; // @[Misc.scala 213:27]
  wire  _T_45; // @[Misc.scala 214:38]
  wire  _T_46; // @[Misc.scala 214:29]
  wire  _T_47; // @[Misc.scala 213:27]
  wire  _T_48; // @[Misc.scala 214:38]
  wire  _T_49; // @[Misc.scala 214:29]
  wire  _T_50; // @[Misc.scala 213:27]
  wire  _T_51; // @[Misc.scala 214:38]
  wire  _T_52; // @[Misc.scala 214:29]
  wire  _T_55; // @[Misc.scala 210:20]
  wire  _T_56; // @[Misc.scala 213:27]
  wire  _T_57; // @[Misc.scala 214:38]
  wire  _T_58; // @[Misc.scala 214:29]
  wire  _T_59; // @[Misc.scala 213:27]
  wire  _T_60; // @[Misc.scala 214:38]
  wire  _T_61; // @[Misc.scala 214:29]
  wire  _T_62; // @[Misc.scala 213:27]
  wire  _T_63; // @[Misc.scala 214:38]
  wire  _T_64; // @[Misc.scala 214:29]
  wire  _T_65; // @[Misc.scala 213:27]
  wire  _T_66; // @[Misc.scala 214:38]
  wire  _T_67; // @[Misc.scala 214:29]
  wire  _T_68; // @[Misc.scala 213:27]
  wire  _T_69; // @[Misc.scala 214:38]
  wire  _T_70; // @[Misc.scala 214:29]
  wire  _T_71; // @[Misc.scala 213:27]
  wire  _T_72; // @[Misc.scala 214:38]
  wire  _T_73; // @[Misc.scala 214:29]
  wire  _T_74; // @[Misc.scala 213:27]
  wire  _T_75; // @[Misc.scala 214:38]
  wire  _T_76; // @[Misc.scala 214:29]
  wire  _T_77; // @[Misc.scala 213:27]
  wire  _T_78; // @[Misc.scala 214:38]
  wire  _T_79; // @[Misc.scala 214:29]
  wire [7:0] _T_86; // @[Cat.scala 29:58]
  wire [36:0] _T_90; // @[Parameters.scala 137:49]
  wire  _T_123; // @[Monitor.scala 79:25]
  wire [35:0] _T_125; // @[Parameters.scala 137:31]
  wire [36:0] _T_126; // @[Parameters.scala 137:49]
  wire [36:0] _T_128; // @[Parameters.scala 137:52]
  wire  _T_129; // @[Parameters.scala 137:67]
  wire [35:0] _T_130; // @[Parameters.scala 137:31]
  wire [36:0] _T_131; // @[Parameters.scala 137:49]
  wire [35:0] _T_135; // @[Parameters.scala 137:31]
  wire [36:0] _T_136; // @[Parameters.scala 137:49]
  wire [36:0] _T_138; // @[Parameters.scala 137:52]
  wire  _T_139; // @[Parameters.scala 137:67]
  wire [35:0] _T_140; // @[Parameters.scala 137:31]
  wire [36:0] _T_141; // @[Parameters.scala 137:49]
  wire [36:0] _T_143; // @[Parameters.scala 137:52]
  wire  _T_144; // @[Parameters.scala 137:67]
  wire [35:0] _T_145; // @[Parameters.scala 137:31]
  wire [36:0] _T_146; // @[Parameters.scala 137:49]
  wire [36:0] _T_148; // @[Parameters.scala 137:52]
  wire  _T_149; // @[Parameters.scala 137:67]
  wire [36:0] _T_153; // @[Parameters.scala 137:52]
  wire  _T_154; // @[Parameters.scala 137:67]
  wire [35:0] _T_155; // @[Parameters.scala 137:31]
  wire [36:0] _T_156; // @[Parameters.scala 137:49]
  wire [36:0] _T_158; // @[Parameters.scala 137:52]
  wire  _T_159; // @[Parameters.scala 137:67]
  wire [35:0] _T_160; // @[Parameters.scala 137:31]
  wire [36:0] _T_161; // @[Parameters.scala 137:49]
  wire [36:0] _T_163; // @[Parameters.scala 137:52]
  wire  _T_164; // @[Parameters.scala 137:67]
  wire [35:0] _T_165; // @[Parameters.scala 137:31]
  wire [36:0] _T_166; // @[Parameters.scala 137:49]
  wire [36:0] _T_168; // @[Parameters.scala 137:52]
  wire  _T_169; // @[Parameters.scala 137:67]
  wire [35:0] _T_170; // @[Parameters.scala 137:31]
  wire [36:0] _T_171; // @[Parameters.scala 137:49]
  wire [36:0] _T_173; // @[Parameters.scala 137:52]
  wire  _T_174; // @[Parameters.scala 137:67]
  wire  _T_185; // @[Parameters.scala 92:48]
  wire [35:0] _T_187; // @[Parameters.scala 137:31]
  wire [36:0] _T_188; // @[Parameters.scala 137:49]
  wire [36:0] _T_190; // @[Parameters.scala 137:52]
  wire  _T_191; // @[Parameters.scala 137:67]
  wire [35:0] _T_192; // @[Parameters.scala 137:31]
  wire [36:0] _T_193; // @[Parameters.scala 137:49]
  wire [36:0] _T_195; // @[Parameters.scala 137:52]
  wire  _T_196; // @[Parameters.scala 137:67]
  wire  _T_197; // @[Parameters.scala 552:42]
  wire  _T_198; // @[Parameters.scala 551:56]
  wire  _T_202; // @[Monitor.scala 44:11]
  wire  _T_203; // @[Monitor.scala 44:11]
  wire  _T_217; // @[Mux.scala 27:72]
  wire  _T_218; // @[Mux.scala 27:72]
  wire  _T_220; // @[Mux.scala 27:72]
  wire  _T_224; // @[Monitor.scala 44:11]
  wire  _T_225; // @[Monitor.scala 44:11]
  wire  _T_227; // @[Monitor.scala 44:11]
  wire  _T_228; // @[Monitor.scala 44:11]
  wire  _T_231; // @[Monitor.scala 44:11]
  wire  _T_232; // @[Monitor.scala 44:11]
  wire  _T_234; // @[Monitor.scala 44:11]
  wire  _T_235; // @[Monitor.scala 44:11]
  wire  _T_236; // @[Bundles.scala 110:27]
  wire  _T_238; // @[Monitor.scala 44:11]
  wire  _T_239; // @[Monitor.scala 44:11]
  wire [7:0] _T_240; // @[Monitor.scala 86:18]
  wire  _T_241; // @[Monitor.scala 86:31]
  wire  _T_243; // @[Monitor.scala 44:11]
  wire  _T_244; // @[Monitor.scala 44:11]
  wire  _T_245; // @[Monitor.scala 87:18]
  wire  _T_247; // @[Monitor.scala 44:11]
  wire  _T_248; // @[Monitor.scala 44:11]
  wire  _T_249; // @[Monitor.scala 90:25]
  wire  _T_366; // @[Monitor.scala 97:31]
  wire  _T_368; // @[Monitor.scala 44:11]
  wire  _T_369; // @[Monitor.scala 44:11]
  wire  _T_379; // @[Monitor.scala 102:25]
  wire  _T_381; // @[Parameters.scala 93:42]
  wire [36:0] _T_392; // @[Parameters.scala 137:52]
  wire  _T_393; // @[Parameters.scala 137:67]
  wire [36:0] _T_422; // @[Parameters.scala 137:52]
  wire  _T_423; // @[Parameters.scala 137:67]
  wire  _T_434; // @[Parameters.scala 552:42]
  wire  _T_435; // @[Parameters.scala 552:42]
  wire  _T_436; // @[Parameters.scala 552:42]
  wire  _T_437; // @[Parameters.scala 552:42]
  wire  _T_438; // @[Parameters.scala 552:42]
  wire  _T_439; // @[Parameters.scala 552:42]
  wire  _T_440; // @[Parameters.scala 552:42]
  wire  _T_441; // @[Parameters.scala 552:42]
  wire  _T_442; // @[Parameters.scala 552:42]
  wire  _T_443; // @[Parameters.scala 551:56]
  wire  _T_445; // @[Parameters.scala 93:42]
  wire [36:0] _T_451; // @[Parameters.scala 137:52]
  wire  _T_452; // @[Parameters.scala 137:67]
  wire  _T_453; // @[Parameters.scala 551:56]
  wire  _T_455; // @[Parameters.scala 553:30]
  wire  _T_457; // @[Monitor.scala 44:11]
  wire  _T_458; // @[Monitor.scala 44:11]
  wire  _T_465; // @[Monitor.scala 106:31]
  wire  _T_467; // @[Monitor.scala 44:11]
  wire  _T_468; // @[Monitor.scala 44:11]
  wire  _T_469; // @[Monitor.scala 107:30]
  wire  _T_471; // @[Monitor.scala 44:11]
  wire  _T_472; // @[Monitor.scala 44:11]
  wire  _T_477; // @[Monitor.scala 111:25]
  wire  _T_479; // @[Parameters.scala 93:42]
  wire  _T_487; // @[Parameters.scala 551:56]
  wire  _T_537; // @[Parameters.scala 552:42]
  wire  _T_538; // @[Parameters.scala 552:42]
  wire  _T_539; // @[Parameters.scala 552:42]
  wire  _T_540; // @[Parameters.scala 552:42]
  wire  _T_541; // @[Parameters.scala 552:42]
  wire  _T_542; // @[Parameters.scala 552:42]
  wire  _T_543; // @[Parameters.scala 552:42]
  wire  _T_544; // @[Parameters.scala 552:42]
  wire  _T_545; // @[Parameters.scala 551:56]
  wire  _T_557; // @[Parameters.scala 553:30]
  wire  _T_558; // @[Parameters.scala 553:30]
  wire  _T_560; // @[Monitor.scala 44:11]
  wire  _T_561; // @[Monitor.scala 44:11]
  wire  _T_576; // @[Monitor.scala 119:25]
  wire [7:0] _T_671; // @[Monitor.scala 124:33]
  wire [7:0] _T_672; // @[Monitor.scala 124:31]
  wire  _T_673; // @[Monitor.scala 124:40]
  wire  _T_675; // @[Monitor.scala 44:11]
  wire  _T_676; // @[Monitor.scala 44:11]
  wire  _T_677; // @[Monitor.scala 127:25]
  wire  _T_686; // @[Parameters.scala 93:42]
  wire  _T_740; // @[Parameters.scala 552:42]
  wire  _T_741; // @[Parameters.scala 552:42]
  wire  _T_742; // @[Parameters.scala 552:42]
  wire  _T_743; // @[Parameters.scala 552:42]
  wire  _T_744; // @[Parameters.scala 552:42]
  wire  _T_745; // @[Parameters.scala 552:42]
  wire  _T_746; // @[Parameters.scala 552:42]
  wire  _T_747; // @[Parameters.scala 552:42]
  wire  _T_748; // @[Parameters.scala 551:56]
  wire  _T_752; // @[Monitor.scala 44:11]
  wire  _T_753; // @[Monitor.scala 44:11]
  wire  _T_760; // @[Bundles.scala 140:33]
  wire  _T_762; // @[Monitor.scala 44:11]
  wire  _T_763; // @[Monitor.scala 44:11]
  wire  _T_768; // @[Monitor.scala 135:25]
  wire  _T_851; // @[Bundles.scala 147:30]
  wire  _T_853; // @[Monitor.scala 44:11]
  wire  _T_854; // @[Monitor.scala 44:11]
  wire  _T_859; // @[Monitor.scala 143:25]
  wire  _T_930; // @[Parameters.scala 551:56]
  wire  _T_943; // @[Parameters.scala 553:30]
  wire  _T_945; // @[Monitor.scala 44:11]
  wire  _T_946; // @[Monitor.scala 44:11]
  wire  _T_953; // @[Bundles.scala 160:28]
  wire  _T_955; // @[Monitor.scala 44:11]
  wire  _T_956; // @[Monitor.scala 44:11]
  wire  _T_965; // @[Bundles.scala 44:24]
  wire  _T_967; // @[Monitor.scala 51:11]
  wire  _T_968; // @[Monitor.scala 51:11]
  wire  _T_969; // @[Parameters.scala 47:9]
  wire  _T_970; // @[Parameters.scala 47:9]
  wire  _T_975; // @[Parameters.scala 57:34]
  wire  _T_977; // @[Parameters.scala 58:20]
  wire  _T_978; // @[Parameters.scala 57:50]
  wire  _T_980; // @[Parameters.scala 924:46]
  wire  _T_981; // @[Parameters.scala 924:46]
  wire  _T_983; // @[Monitor.scala 307:25]
  wire  _T_985; // @[Monitor.scala 51:11]
  wire  _T_986; // @[Monitor.scala 51:11]
  wire  _T_987; // @[Monitor.scala 309:27]
  wire  _T_989; // @[Monitor.scala 51:11]
  wire  _T_990; // @[Monitor.scala 51:11]
  wire  _T_991; // @[Monitor.scala 310:28]
  wire  _T_993; // @[Monitor.scala 51:11]
  wire  _T_994; // @[Monitor.scala 51:11]
  wire  _T_995; // @[Monitor.scala 311:15]
  wire  _T_997; // @[Monitor.scala 51:11]
  wire  _T_998; // @[Monitor.scala 51:11]
  wire  _T_999; // @[Monitor.scala 312:15]
  wire  _T_1001; // @[Monitor.scala 51:11]
  wire  _T_1002; // @[Monitor.scala 51:11]
  wire  _T_1003; // @[Monitor.scala 315:25]
  wire  _T_1014; // @[Bundles.scala 104:26]
  wire  _T_1016; // @[Monitor.scala 51:11]
  wire  _T_1017; // @[Monitor.scala 51:11]
  wire  _T_1018; // @[Monitor.scala 320:28]
  wire  _T_1020; // @[Monitor.scala 51:11]
  wire  _T_1021; // @[Monitor.scala 51:11]
  wire  _T_1031; // @[Monitor.scala 325:25]
  wire  _T_1051; // @[Monitor.scala 331:30]
  wire  _T_1053; // @[Monitor.scala 51:11]
  wire  _T_1054; // @[Monitor.scala 51:11]
  wire  _T_1060; // @[Monitor.scala 335:25]
  wire  _T_1077; // @[Monitor.scala 343:25]
  wire  _T_1095; // @[Monitor.scala 351:25]
  wire  _T_1116; // @[Parameters.scala 47:9]
  wire [36:0] _T_1119; // @[Parameters.scala 137:49]
  wire  _T_1124; // @[Parameters.scala 47:9]
  wire  _T_1136; // @[Parameters.scala 57:34]
  wire  _T_1138; // @[Parameters.scala 58:20]
  wire  _T_1139; // @[Parameters.scala 57:50]
  wire [35:0] _T_1152; // @[Parameters.scala 137:31]
  wire [36:0] _T_1153; // @[Parameters.scala 137:49]
  wire [36:0] _T_1155; // @[Parameters.scala 137:52]
  wire  _T_1156; // @[Parameters.scala 137:67]
  wire [35:0] _T_1157; // @[Parameters.scala 137:31]
  wire [36:0] _T_1158; // @[Parameters.scala 137:49]
  wire [36:0] _T_1160; // @[Parameters.scala 137:52]
  wire  _T_1161; // @[Parameters.scala 137:67]
  wire [35:0] _T_1162; // @[Parameters.scala 137:31]
  wire [36:0] _T_1163; // @[Parameters.scala 137:49]
  wire [36:0] _T_1165; // @[Parameters.scala 137:52]
  wire  _T_1166; // @[Parameters.scala 137:67]
  wire [35:0] _T_1167; // @[Parameters.scala 137:31]
  wire [36:0] _T_1168; // @[Parameters.scala 137:49]
  wire [36:0] _T_1170; // @[Parameters.scala 137:52]
  wire  _T_1171; // @[Parameters.scala 137:67]
  wire [35:0] _T_1172; // @[Parameters.scala 137:31]
  wire [36:0] _T_1173; // @[Parameters.scala 137:49]
  wire [36:0] _T_1175; // @[Parameters.scala 137:52]
  wire  _T_1176; // @[Parameters.scala 137:67]
  wire [35:0] _T_1177; // @[Parameters.scala 137:31]
  wire [36:0] _T_1178; // @[Parameters.scala 137:49]
  wire [36:0] _T_1180; // @[Parameters.scala 137:52]
  wire  _T_1181; // @[Parameters.scala 137:67]
  wire [35:0] _T_1182; // @[Parameters.scala 137:31]
  wire [36:0] _T_1183; // @[Parameters.scala 137:49]
  wire [36:0] _T_1185; // @[Parameters.scala 137:52]
  wire  _T_1186; // @[Parameters.scala 137:67]
  wire [36:0] _T_1190; // @[Parameters.scala 137:52]
  wire  _T_1191; // @[Parameters.scala 137:67]
  wire [35:0] _T_1192; // @[Parameters.scala 137:31]
  wire [36:0] _T_1193; // @[Parameters.scala 137:49]
  wire [36:0] _T_1195; // @[Parameters.scala 137:52]
  wire  _T_1196; // @[Parameters.scala 137:67]
  wire [35:0] _T_1197; // @[Parameters.scala 137:31]
  wire [36:0] _T_1198; // @[Parameters.scala 137:49]
  wire [36:0] _T_1200; // @[Parameters.scala 137:52]
  wire  _T_1201; // @[Parameters.scala 137:67]
  wire [35:0] _T_1202; // @[Parameters.scala 137:31]
  wire [36:0] _T_1203; // @[Parameters.scala 137:49]
  wire [36:0] _T_1205; // @[Parameters.scala 137:52]
  wire  _T_1206; // @[Parameters.scala 137:67]
  wire [35:0] _T_1207; // @[Parameters.scala 137:31]
  wire [36:0] _T_1208; // @[Parameters.scala 137:49]
  wire [36:0] _T_1210; // @[Parameters.scala 137:52]
  wire  _T_1211; // @[Parameters.scala 137:67]
  wire [35:0] _T_1212; // @[Parameters.scala 137:31]
  wire [36:0] _T_1213; // @[Parameters.scala 137:49]
  wire [36:0] _T_1215; // @[Parameters.scala 137:52]
  wire  _T_1216; // @[Parameters.scala 137:67]
  wire [35:0] _T_1217; // @[Parameters.scala 137:31]
  wire [36:0] _T_1218; // @[Parameters.scala 137:49]
  wire [36:0] _T_1220; // @[Parameters.scala 137:52]
  wire  _T_1221; // @[Parameters.scala 137:67]
  wire  _T_1223; // @[Parameters.scala 535:64]
  wire  _T_1224; // @[Parameters.scala 535:64]
  wire  _T_1225; // @[Parameters.scala 535:64]
  wire  _T_1226; // @[Parameters.scala 535:64]
  wire  _T_1227; // @[Parameters.scala 535:64]
  wire  _T_1228; // @[Parameters.scala 535:64]
  wire  _T_1229; // @[Parameters.scala 535:64]
  wire  _T_1230; // @[Parameters.scala 535:64]
  wire  _T_1231; // @[Parameters.scala 535:64]
  wire  _T_1232; // @[Parameters.scala 535:64]
  wire  _T_1233; // @[Parameters.scala 535:64]
  wire  _T_1234; // @[Parameters.scala 535:64]
  wire  _T_1235; // @[Parameters.scala 535:64]
  wire [35:0] _T_1240; // @[Edges.scala 22:16]
  wire  _T_1241; // @[Edges.scala 22:24]
  wire [1:0] _T_1319; // @[Mux.scala 27:72]
  wire [1:0] _GEN_34; // @[Mux.scala 27:72]
  wire [1:0] _T_1321; // @[Mux.scala 27:72]
  wire [3:0] _GEN_35; // @[Monitor.scala 162:113]
  wire  _T_1323; // @[Monitor.scala 162:113]
  wire  _T_1341; // @[Mux.scala 27:72]
  wire  _T_1345; // @[Monitor.scala 44:11]
  wire  _T_1346; // @[Monitor.scala 44:11]
  wire  _T_1348; // @[Monitor.scala 44:11]
  wire  _T_1349; // @[Monitor.scala 44:11]
  wire  _T_1351; // @[Monitor.scala 44:11]
  wire  _T_1352; // @[Monitor.scala 44:11]
  wire  _T_1354; // @[Monitor.scala 44:11]
  wire  _T_1355; // @[Monitor.scala 44:11]
  wire  _T_1356; // @[Bundles.scala 104:26]
  wire  _T_1358; // @[Monitor.scala 44:11]
  wire  _T_1359; // @[Monitor.scala 44:11]
  wire  _T_1504; // @[Parameters.scala 47:9]
  wire  _T_1505; // @[Parameters.scala 47:9]
  wire  _T_1510; // @[Parameters.scala 57:34]
  wire  _T_1512; // @[Parameters.scala 58:20]
  wire  _T_1513; // @[Parameters.scala 57:50]
  wire  _T_1515; // @[Parameters.scala 924:46]
  wire  _T_1516; // @[Parameters.scala 924:46]
  wire [22:0] _T_1518; // @[package.scala 189:77]
  wire [7:0] _T_1520; // @[package.scala 189:46]
  wire [35:0] _GEN_36; // @[Edges.scala 22:16]
  wire [35:0] _T_1521; // @[Edges.scala 22:16]
  wire  _T_1522; // @[Edges.scala 22:24]
  wire [35:0] _T_1523; // @[Parameters.scala 137:31]
  wire [36:0] _T_1524; // @[Parameters.scala 137:49]
  wire [36:0] _T_1526; // @[Parameters.scala 137:52]
  wire  _T_1527; // @[Parameters.scala 137:67]
  wire [35:0] _T_1528; // @[Parameters.scala 137:31]
  wire [36:0] _T_1529; // @[Parameters.scala 137:49]
  wire [36:0] _T_1531; // @[Parameters.scala 137:52]
  wire  _T_1532; // @[Parameters.scala 137:67]
  wire [35:0] _T_1533; // @[Parameters.scala 137:31]
  wire [36:0] _T_1534; // @[Parameters.scala 137:49]
  wire [36:0] _T_1536; // @[Parameters.scala 137:52]
  wire  _T_1537; // @[Parameters.scala 137:67]
  wire [35:0] _T_1538; // @[Parameters.scala 137:31]
  wire [36:0] _T_1539; // @[Parameters.scala 137:49]
  wire [36:0] _T_1541; // @[Parameters.scala 137:52]
  wire  _T_1542; // @[Parameters.scala 137:67]
  wire [35:0] _T_1543; // @[Parameters.scala 137:31]
  wire [36:0] _T_1544; // @[Parameters.scala 137:49]
  wire [36:0] _T_1546; // @[Parameters.scala 137:52]
  wire  _T_1547; // @[Parameters.scala 137:67]
  wire [35:0] _T_1548; // @[Parameters.scala 137:31]
  wire [36:0] _T_1549; // @[Parameters.scala 137:49]
  wire [36:0] _T_1551; // @[Parameters.scala 137:52]
  wire  _T_1552; // @[Parameters.scala 137:67]
  wire [35:0] _T_1553; // @[Parameters.scala 137:31]
  wire [36:0] _T_1554; // @[Parameters.scala 137:49]
  wire [36:0] _T_1556; // @[Parameters.scala 137:52]
  wire  _T_1557; // @[Parameters.scala 137:67]
  wire [36:0] _T_1559; // @[Parameters.scala 137:49]
  wire [36:0] _T_1561; // @[Parameters.scala 137:52]
  wire  _T_1562; // @[Parameters.scala 137:67]
  wire [35:0] _T_1563; // @[Parameters.scala 137:31]
  wire [36:0] _T_1564; // @[Parameters.scala 137:49]
  wire [36:0] _T_1566; // @[Parameters.scala 137:52]
  wire  _T_1567; // @[Parameters.scala 137:67]
  wire [35:0] _T_1568; // @[Parameters.scala 137:31]
  wire [36:0] _T_1569; // @[Parameters.scala 137:49]
  wire [36:0] _T_1571; // @[Parameters.scala 137:52]
  wire  _T_1572; // @[Parameters.scala 137:67]
  wire [35:0] _T_1573; // @[Parameters.scala 137:31]
  wire [36:0] _T_1574; // @[Parameters.scala 137:49]
  wire [36:0] _T_1576; // @[Parameters.scala 137:52]
  wire  _T_1577; // @[Parameters.scala 137:67]
  wire [35:0] _T_1578; // @[Parameters.scala 137:31]
  wire [36:0] _T_1579; // @[Parameters.scala 137:49]
  wire [36:0] _T_1581; // @[Parameters.scala 137:52]
  wire  _T_1582; // @[Parameters.scala 137:67]
  wire [35:0] _T_1583; // @[Parameters.scala 137:31]
  wire [36:0] _T_1584; // @[Parameters.scala 137:49]
  wire [36:0] _T_1586; // @[Parameters.scala 137:52]
  wire  _T_1587; // @[Parameters.scala 137:67]
  wire [35:0] _T_1588; // @[Parameters.scala 137:31]
  wire [36:0] _T_1589; // @[Parameters.scala 137:49]
  wire [36:0] _T_1591; // @[Parameters.scala 137:52]
  wire  _T_1592; // @[Parameters.scala 137:67]
  wire  _T_1594; // @[Parameters.scala 535:64]
  wire  _T_1595; // @[Parameters.scala 535:64]
  wire  _T_1596; // @[Parameters.scala 535:64]
  wire  _T_1597; // @[Parameters.scala 535:64]
  wire  _T_1598; // @[Parameters.scala 535:64]
  wire  _T_1599; // @[Parameters.scala 535:64]
  wire  _T_1600; // @[Parameters.scala 535:64]
  wire  _T_1601; // @[Parameters.scala 535:64]
  wire  _T_1602; // @[Parameters.scala 535:64]
  wire  _T_1603; // @[Parameters.scala 535:64]
  wire  _T_1604; // @[Parameters.scala 535:64]
  wire  _T_1605; // @[Parameters.scala 535:64]
  wire  _T_1606; // @[Parameters.scala 535:64]
  wire  _T_1643; // @[Monitor.scala 239:25]
  wire  _T_1645; // @[Monitor.scala 44:11]
  wire  _T_1646; // @[Monitor.scala 44:11]
  wire  _T_1648; // @[Monitor.scala 44:11]
  wire  _T_1649; // @[Monitor.scala 44:11]
  wire  _T_1650; // @[Monitor.scala 242:30]
  wire  _T_1652; // @[Monitor.scala 44:11]
  wire  _T_1653; // @[Monitor.scala 44:11]
  wire  _T_1655; // @[Monitor.scala 44:11]
  wire  _T_1656; // @[Monitor.scala 44:11]
  wire  _T_1657; // @[Bundles.scala 122:29]
  wire  _T_1659; // @[Monitor.scala 44:11]
  wire  _T_1660; // @[Monitor.scala 44:11]
  wire  _T_1661; // @[Monitor.scala 245:18]
  wire  _T_1663; // @[Monitor.scala 44:11]
  wire  _T_1664; // @[Monitor.scala 44:11]
  wire  _T_1665; // @[Monitor.scala 248:25]
  wire  _T_1683; // @[Monitor.scala 256:25]
  wire  _T_1745; // @[Parameters.scala 92:48]
  wire  _T_1757; // @[Parameters.scala 552:42]
  wire  _T_1758; // @[Parameters.scala 551:56]
  wire  _T_1762; // @[Monitor.scala 44:11]
  wire  _T_1763; // @[Monitor.scala 44:11]
  wire  _T_1777; // @[Mux.scala 27:72]
  wire  _T_1778; // @[Mux.scala 27:72]
  wire  _T_1780; // @[Mux.scala 27:72]
  wire  _T_1784; // @[Monitor.scala 44:11]
  wire  _T_1785; // @[Monitor.scala 44:11]
  wire  _T_1796; // @[Bundles.scala 116:29]
  wire  _T_1798; // @[Monitor.scala 44:11]
  wire  _T_1799; // @[Monitor.scala 44:11]
  wire  _T_1804; // @[Monitor.scala 266:25]
  wire  _T_1921; // @[Monitor.scala 275:25]
  wire  _T_1931; // @[Monitor.scala 279:31]
  wire  _T_1933; // @[Monitor.scala 44:11]
  wire  _T_1934; // @[Monitor.scala 44:11]
  wire  _T_1939; // @[Monitor.scala 283:25]
  wire  _T_1953; // @[Monitor.scala 290:25]
  wire  _T_1975; // @[Decoupled.scala 40:37]
  wire  _T_1982; // @[Edges.scala 93:28]
  reg [4:0] _T_1984; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire [4:0] _T_1986; // @[Edges.scala 231:28]
  wire  _T_1987; // @[Edges.scala 232:25]
  reg [2:0] _T_1995; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [2:0] _T_1996; // @[Monitor.scala 382:22]
  reg [31:0] _RAND_2;
  reg [3:0] _T_1997; // @[Monitor.scala 383:22]
  reg [31:0] _RAND_3;
  reg [3:0] _T_1998; // @[Monitor.scala 384:22]
  reg [31:0] _RAND_4;
  reg [35:0] _T_1999; // @[Monitor.scala 385:22]
  reg [63:0] _RAND_5;
  wire  _T_2000; // @[Monitor.scala 386:22]
  wire  _T_2001; // @[Monitor.scala 386:19]
  wire  _T_2002; // @[Monitor.scala 387:32]
  wire  _T_2004; // @[Monitor.scala 44:11]
  wire  _T_2005; // @[Monitor.scala 44:11]
  wire  _T_2006; // @[Monitor.scala 388:32]
  wire  _T_2008; // @[Monitor.scala 44:11]
  wire  _T_2009; // @[Monitor.scala 44:11]
  wire  _T_2010; // @[Monitor.scala 389:32]
  wire  _T_2012; // @[Monitor.scala 44:11]
  wire  _T_2013; // @[Monitor.scala 44:11]
  wire  _T_2014; // @[Monitor.scala 390:32]
  wire  _T_2016; // @[Monitor.scala 44:11]
  wire  _T_2017; // @[Monitor.scala 44:11]
  wire  _T_2018; // @[Monitor.scala 391:32]
  wire  _T_2020; // @[Monitor.scala 44:11]
  wire  _T_2021; // @[Monitor.scala 44:11]
  wire  _T_2023; // @[Monitor.scala 393:20]
  wire  _T_2024; // @[Decoupled.scala 40:37]
  wire [22:0] _T_2026; // @[package.scala 189:77]
  wire [7:0] _T_2028; // @[package.scala 189:46]
  reg [4:0] _T_2032; // @[Edges.scala 230:27]
  reg [31:0] _RAND_6;
  wire [4:0] _T_2034; // @[Edges.scala 231:28]
  wire  _T_2035; // @[Edges.scala 232:25]
  reg [2:0] _T_2043; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_7;
  reg [1:0] _T_2044; // @[Monitor.scala 533:22]
  reg [31:0] _RAND_8;
  reg [3:0] _T_2045; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_9;
  reg [3:0] _T_2046; // @[Monitor.scala 535:22]
  reg [31:0] _RAND_10;
  reg [3:0] _T_2047; // @[Monitor.scala 536:22]
  reg [31:0] _RAND_11;
  reg  _T_2048; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_12;
  wire  _T_2049; // @[Monitor.scala 538:22]
  wire  _T_2050; // @[Monitor.scala 538:19]
  wire  _T_2051; // @[Monitor.scala 539:29]
  wire  _T_2053; // @[Monitor.scala 51:11]
  wire  _T_2054; // @[Monitor.scala 51:11]
  wire  _T_2055; // @[Monitor.scala 540:29]
  wire  _T_2057; // @[Monitor.scala 51:11]
  wire  _T_2058; // @[Monitor.scala 51:11]
  wire  _T_2059; // @[Monitor.scala 541:29]
  wire  _T_2061; // @[Monitor.scala 51:11]
  wire  _T_2062; // @[Monitor.scala 51:11]
  wire  _T_2063; // @[Monitor.scala 542:29]
  wire  _T_2065; // @[Monitor.scala 51:11]
  wire  _T_2066; // @[Monitor.scala 51:11]
  wire  _T_2067; // @[Monitor.scala 543:29]
  wire  _T_2069; // @[Monitor.scala 51:11]
  wire  _T_2070; // @[Monitor.scala 51:11]
  wire  _T_2071; // @[Monitor.scala 544:29]
  wire  _T_2073; // @[Monitor.scala 51:11]
  wire  _T_2074; // @[Monitor.scala 51:11]
  wire  _T_2076; // @[Monitor.scala 546:20]
  wire  _T_2077; // @[Decoupled.scala 40:37]
  reg [4:0] _T_2086; // @[Edges.scala 230:27]
  reg [31:0] _RAND_13;
  wire [4:0] _T_2088; // @[Edges.scala 231:28]
  wire  _T_2089; // @[Edges.scala 232:25]
  reg [1:0] _T_2098; // @[Monitor.scala 405:22]
  reg [31:0] _RAND_14;
  reg [3:0] _T_2100; // @[Monitor.scala 407:22]
  reg [31:0] _RAND_15;
  reg [35:0] _T_2101; // @[Monitor.scala 408:22]
  reg [63:0] _RAND_16;
  wire  _T_2102; // @[Monitor.scala 409:22]
  wire  _T_2103; // @[Monitor.scala 409:19]
  wire  _T_2108; // @[Monitor.scala 411:32]
  wire  _T_2110; // @[Monitor.scala 44:11]
  wire  _T_2111; // @[Monitor.scala 44:11]
  wire  _T_2116; // @[Monitor.scala 413:32]
  wire  _T_2118; // @[Monitor.scala 44:11]
  wire  _T_2119; // @[Monitor.scala 44:11]
  wire  _T_2120; // @[Monitor.scala 414:32]
  wire  _T_2122; // @[Monitor.scala 44:11]
  wire  _T_2123; // @[Monitor.scala 44:11]
  wire  _T_2125; // @[Monitor.scala 416:20]
  wire  _T_2126; // @[Decoupled.scala 40:37]
  reg [4:0] _T_2134; // @[Edges.scala 230:27]
  reg [31:0] _RAND_17;
  wire [4:0] _T_2136; // @[Edges.scala 231:28]
  wire  _T_2137; // @[Edges.scala 232:25]
  reg [2:0] _T_2145; // @[Monitor.scala 509:22]
  reg [31:0] _RAND_18;
  reg [2:0] _T_2146; // @[Monitor.scala 510:22]
  reg [31:0] _RAND_19;
  reg [3:0] _T_2147; // @[Monitor.scala 511:22]
  reg [31:0] _RAND_20;
  reg [3:0] _T_2148; // @[Monitor.scala 512:22]
  reg [31:0] _RAND_21;
  reg [35:0] _T_2149; // @[Monitor.scala 513:22]
  reg [63:0] _RAND_22;
  wire  _T_2150; // @[Monitor.scala 514:22]
  wire  _T_2151; // @[Monitor.scala 514:19]
  wire  _T_2152; // @[Monitor.scala 515:32]
  wire  _T_2154; // @[Monitor.scala 44:11]
  wire  _T_2155; // @[Monitor.scala 44:11]
  wire  _T_2156; // @[Monitor.scala 516:32]
  wire  _T_2158; // @[Monitor.scala 44:11]
  wire  _T_2159; // @[Monitor.scala 44:11]
  wire  _T_2160; // @[Monitor.scala 517:32]
  wire  _T_2162; // @[Monitor.scala 44:11]
  wire  _T_2163; // @[Monitor.scala 44:11]
  wire  _T_2164; // @[Monitor.scala 518:32]
  wire  _T_2166; // @[Monitor.scala 44:11]
  wire  _T_2167; // @[Monitor.scala 44:11]
  wire  _T_2168; // @[Monitor.scala 519:32]
  wire  _T_2170; // @[Monitor.scala 44:11]
  wire  _T_2171; // @[Monitor.scala 44:11]
  wire  _T_2173; // @[Monitor.scala 521:20]
  reg [8:0] _T_2174; // @[Monitor.scala 568:27]
  reg [31:0] _RAND_23;
  reg [4:0] _T_2184; // @[Edges.scala 230:27]
  reg [31:0] _RAND_24;
  wire [4:0] _T_2186; // @[Edges.scala 231:28]
  wire  _T_2187; // @[Edges.scala 232:25]
  reg [4:0] _T_2203; // @[Edges.scala 230:27]
  reg [31:0] _RAND_25;
  wire [4:0] _T_2205; // @[Edges.scala 231:28]
  wire  _T_2206; // @[Edges.scala 232:25]
  wire  _T_2216; // @[Monitor.scala 574:27]
  wire [15:0] _T_2218; // @[OneHot.scala 58:35]
  wire [8:0] _T_2219; // @[Monitor.scala 576:23]
  wire  _T_2221; // @[Monitor.scala 576:14]
  wire  _T_2223; // @[Monitor.scala 576:13]
  wire  _T_2224; // @[Monitor.scala 576:13]
  wire [15:0] _GEN_27; // @[Monitor.scala 574:72]
  wire  _T_2228; // @[Monitor.scala 581:27]
  wire  _T_2230; // @[Monitor.scala 581:75]
  wire  _T_2231; // @[Monitor.scala 581:72]
  wire [15:0] _T_2232; // @[OneHot.scala 58:35]
  wire [8:0] _T_2233; // @[Monitor.scala 583:21]
  wire [8:0] _T_2234; // @[Monitor.scala 583:32]
  wire  _T_2237; // @[Monitor.scala 51:11]
  wire  _T_2238; // @[Monitor.scala 51:11]
  wire [15:0] _GEN_28; // @[Monitor.scala 581:91]
  wire  _T_2239; // @[Monitor.scala 587:20]
  wire  _T_2240; // @[Monitor.scala 587:40]
  wire  _T_2241; // @[Monitor.scala 587:33]
  wire  _T_2242; // @[Monitor.scala 587:30]
  wire  _T_2244; // @[Monitor.scala 51:11]
  wire  _T_2245; // @[Monitor.scala 51:11]
  wire [8:0] _T_2246; // @[Monitor.scala 590:27]
  wire [8:0] _T_2247; // @[Monitor.scala 590:38]
  wire [8:0] _T_2248; // @[Monitor.scala 590:36]
  reg [31:0] _T_2249; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_26;
  wire  _T_2250; // @[Monitor.scala 595:23]
  wire  _T_2251; // @[Monitor.scala 595:13]
  wire  _T_2252; // @[Monitor.scala 595:36]
  wire  _T_2253; // @[Monitor.scala 595:27]
  wire  _T_2254; // @[Monitor.scala 595:56]
  wire  _T_2255; // @[Monitor.scala 595:44]
  wire  _T_2257; // @[Monitor.scala 595:12]
  wire  _T_2258; // @[Monitor.scala 595:12]
  wire [31:0] _T_2260; // @[Monitor.scala 597:26]
  wire  _T_2263; // @[Monitor.scala 598:27]
  reg [15:0] _T_2264; // @[Monitor.scala 694:27]
  reg [31:0] _RAND_27;
  reg [4:0] _T_2273; // @[Edges.scala 230:27]
  reg [31:0] _RAND_28;
  wire [4:0] _T_2275; // @[Edges.scala 231:28]
  wire  _T_2276; // @[Edges.scala 232:25]
  wire  _T_2286; // @[Monitor.scala 700:27]
  wire  _T_2289; // @[Edges.scala 72:43]
  wire  _T_2290; // @[Edges.scala 72:40]
  wire  _T_2291; // @[Monitor.scala 700:38]
  wire [15:0] _T_2292; // @[OneHot.scala 58:35]
  wire [15:0] _T_2293; // @[Monitor.scala 702:23]
  wire  _T_2295; // @[Monitor.scala 702:14]
  wire  _T_2297; // @[Monitor.scala 51:11]
  wire  _T_2298; // @[Monitor.scala 51:11]
  wire [15:0] _GEN_31; // @[Monitor.scala 700:72]
  wire  _T_2300; // @[Decoupled.scala 40:37]
  wire [15:0] _T_2303; // @[OneHot.scala 58:35]
  wire [15:0] _T_2304; // @[Monitor.scala 708:24]
  wire [15:0] _T_2305; // @[Monitor.scala 708:35]
  wire  _T_2308; // @[Monitor.scala 44:11]
  wire  _T_2309; // @[Monitor.scala 44:11]
  wire [15:0] _GEN_32; // @[Monitor.scala 706:73]
  wire [15:0] _T_2310; // @[Monitor.scala 713:27]
  wire [15:0] _T_2311; // @[Monitor.scala 713:38]
  wire [15:0] _T_2312; // @[Monitor.scala 713:36]
  wire  _GEN_37; // @[Monitor.scala 44:11]
  wire  _GEN_53; // @[Monitor.scala 44:11]
  wire  _GEN_71; // @[Monitor.scala 44:11]
  wire  _GEN_83; // @[Monitor.scala 44:11]
  wire  _GEN_93; // @[Monitor.scala 44:11]
  wire  _GEN_103; // @[Monitor.scala 44:11]
  wire  _GEN_113; // @[Monitor.scala 44:11]
  wire  _GEN_123; // @[Monitor.scala 44:11]
  wire  _GEN_135; // @[Monitor.scala 51:11]
  wire  _GEN_145; // @[Monitor.scala 51:11]
  wire  _GEN_155; // @[Monitor.scala 51:11]
  wire  _GEN_165; // @[Monitor.scala 51:11]
  wire  _GEN_171; // @[Monitor.scala 51:11]
  wire  _GEN_177; // @[Monitor.scala 51:11]
  wire  _GEN_183; // @[Monitor.scala 44:11]
  wire  _GEN_195; // @[Monitor.scala 44:11]
  wire  _GEN_205; // @[Monitor.scala 44:11]
  wire  _GEN_219; // @[Monitor.scala 44:11]
  wire  _GEN_231; // @[Monitor.scala 44:11]
  wire  _GEN_241; // @[Monitor.scala 44:11]
  wire  _GEN_249; // @[Monitor.scala 44:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_4 = io_in_a_bits_source == 4'h0; // @[Parameters.scala 47:9]
  assign _T_5 = io_in_a_bits_source == 4'h1; // @[Parameters.scala 47:9]
  assign _T_10 = 4'h2 <= io_in_a_bits_source; // @[Parameters.scala 57:34]
  assign _T_12 = io_in_a_bits_source <= 4'h8; // @[Parameters.scala 58:20]
  assign _T_13 = _T_10 & _T_12; // @[Parameters.scala 57:50]
  assign _T_15 = _T_4 | _T_5; // @[Parameters.scala 924:46]
  assign _T_16 = _T_15 | _T_13; // @[Parameters.scala 924:46]
  assign _T_18 = 23'hff << io_in_a_bits_size; // @[package.scala 189:77]
  assign _T_20 = ~_T_18[7:0]; // @[package.scala 189:46]
  assign _GEN_33 = {{28'd0}, _T_20}; // @[Edges.scala 22:16]
  assign _T_21 = io_in_a_bits_address & _GEN_33; // @[Edges.scala 22:16]
  assign _T_22 = _T_21 == 36'h0; // @[Edges.scala 22:24]
  assign _T_25 = 4'h1 << io_in_a_bits_size[1:0]; // @[OneHot.scala 65:12]
  assign _T_27 = _T_25[2:0] | 3'h1; // @[Misc.scala 201:81]
  assign _T_28 = io_in_a_bits_size >= 4'h3; // @[Misc.scala 205:21]
  assign _T_31 = ~io_in_a_bits_address[2]; // @[Misc.scala 210:20]
  assign _T_33 = _T_27[2] & _T_31; // @[Misc.scala 214:38]
  assign _T_34 = _T_28 | _T_33; // @[Misc.scala 214:29]
  assign _T_36 = _T_27[2] & io_in_a_bits_address[2]; // @[Misc.scala 214:38]
  assign _T_37 = _T_28 | _T_36; // @[Misc.scala 214:29]
  assign _T_40 = ~io_in_a_bits_address[1]; // @[Misc.scala 210:20]
  assign _T_41 = _T_31 & _T_40; // @[Misc.scala 213:27]
  assign _T_42 = _T_27[1] & _T_41; // @[Misc.scala 214:38]
  assign _T_43 = _T_34 | _T_42; // @[Misc.scala 214:29]
  assign _T_44 = _T_31 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_45 = _T_27[1] & _T_44; // @[Misc.scala 214:38]
  assign _T_46 = _T_34 | _T_45; // @[Misc.scala 214:29]
  assign _T_47 = io_in_a_bits_address[2] & _T_40; // @[Misc.scala 213:27]
  assign _T_48 = _T_27[1] & _T_47; // @[Misc.scala 214:38]
  assign _T_49 = _T_37 | _T_48; // @[Misc.scala 214:29]
  assign _T_50 = io_in_a_bits_address[2] & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_51 = _T_27[1] & _T_50; // @[Misc.scala 214:38]
  assign _T_52 = _T_37 | _T_51; // @[Misc.scala 214:29]
  assign _T_55 = ~io_in_a_bits_address[0]; // @[Misc.scala 210:20]
  assign _T_56 = _T_41 & _T_55; // @[Misc.scala 213:27]
  assign _T_57 = _T_27[0] & _T_56; // @[Misc.scala 214:38]
  assign _T_58 = _T_43 | _T_57; // @[Misc.scala 214:29]
  assign _T_59 = _T_41 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_60 = _T_27[0] & _T_59; // @[Misc.scala 214:38]
  assign _T_61 = _T_43 | _T_60; // @[Misc.scala 214:29]
  assign _T_62 = _T_44 & _T_55; // @[Misc.scala 213:27]
  assign _T_63 = _T_27[0] & _T_62; // @[Misc.scala 214:38]
  assign _T_64 = _T_46 | _T_63; // @[Misc.scala 214:29]
  assign _T_65 = _T_44 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_66 = _T_27[0] & _T_65; // @[Misc.scala 214:38]
  assign _T_67 = _T_46 | _T_66; // @[Misc.scala 214:29]
  assign _T_68 = _T_47 & _T_55; // @[Misc.scala 213:27]
  assign _T_69 = _T_27[0] & _T_68; // @[Misc.scala 214:38]
  assign _T_70 = _T_49 | _T_69; // @[Misc.scala 214:29]
  assign _T_71 = _T_47 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_72 = _T_27[0] & _T_71; // @[Misc.scala 214:38]
  assign _T_73 = _T_49 | _T_72; // @[Misc.scala 214:29]
  assign _T_74 = _T_50 & _T_55; // @[Misc.scala 213:27]
  assign _T_75 = _T_27[0] & _T_74; // @[Misc.scala 214:38]
  assign _T_76 = _T_52 | _T_75; // @[Misc.scala 214:29]
  assign _T_77 = _T_50 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_78 = _T_27[0] & _T_77; // @[Misc.scala 214:38]
  assign _T_79 = _T_52 | _T_78; // @[Misc.scala 214:29]
  assign _T_86 = {_T_79,_T_76,_T_73,_T_70,_T_67,_T_64,_T_61,_T_58}; // @[Cat.scala 29:58]
  assign _T_90 = {1'b0,$signed(io_in_a_bits_address)}; // @[Parameters.scala 137:49]
  assign _T_123 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_125 = io_in_a_bits_address ^ 36'h400000000; // @[Parameters.scala 137:31]
  assign _T_126 = {1'b0,$signed(_T_125)}; // @[Parameters.scala 137:49]
  assign _T_128 = $signed(_T_126) & -37'sh400000000; // @[Parameters.scala 137:52]
  assign _T_129 = $signed(_T_128) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_130 = io_in_a_bits_address ^ 36'h8000000; // @[Parameters.scala 137:31]
  assign _T_131 = {1'b0,$signed(_T_130)}; // @[Parameters.scala 137:49]
  assign _T_135 = io_in_a_bits_address ^ 36'h3000; // @[Parameters.scala 137:31]
  assign _T_136 = {1'b0,$signed(_T_135)}; // @[Parameters.scala 137:49]
  assign _T_138 = $signed(_T_136) & -37'sh10001000; // @[Parameters.scala 137:52]
  assign _T_139 = $signed(_T_138) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_140 = io_in_a_bits_address ^ 36'hc000000; // @[Parameters.scala 137:31]
  assign _T_141 = {1'b0,$signed(_T_140)}; // @[Parameters.scala 137:49]
  assign _T_143 = $signed(_T_141) & -37'sh4000000; // @[Parameters.scala 137:52]
  assign _T_144 = $signed(_T_143) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_145 = io_in_a_bits_address ^ 36'h2000000; // @[Parameters.scala 137:31]
  assign _T_146 = {1'b0,$signed(_T_145)}; // @[Parameters.scala 137:49]
  assign _T_148 = $signed(_T_146) & -37'sh10000; // @[Parameters.scala 137:52]
  assign _T_149 = $signed(_T_148) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_153 = $signed(_T_90) & -37'sh10001000; // @[Parameters.scala 137:52]
  assign _T_154 = $signed(_T_153) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_155 = io_in_a_bits_address ^ 36'h20000000; // @[Parameters.scala 137:31]
  assign _T_156 = {1'b0,$signed(_T_155)}; // @[Parameters.scala 137:49]
  assign _T_158 = $signed(_T_156) & -37'sh20000000; // @[Parameters.scala 137:52]
  assign _T_159 = $signed(_T_158) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_160 = io_in_a_bits_address ^ 36'h10001000; // @[Parameters.scala 137:31]
  assign _T_161 = {1'b0,$signed(_T_160)}; // @[Parameters.scala 137:49]
  assign _T_163 = $signed(_T_161) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_164 = $signed(_T_163) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_165 = io_in_a_bits_address ^ 36'h4000; // @[Parameters.scala 137:31]
  assign _T_166 = {1'b0,$signed(_T_165)}; // @[Parameters.scala 137:49]
  assign _T_168 = $signed(_T_166) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_169 = $signed(_T_168) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_170 = io_in_a_bits_address ^ 36'h2010000; // @[Parameters.scala 137:31]
  assign _T_171 = {1'b0,$signed(_T_170)}; // @[Parameters.scala 137:49]
  assign _T_173 = $signed(_T_171) & -37'sh4000; // @[Parameters.scala 137:52]
  assign _T_174 = $signed(_T_173) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_185 = 4'h6 == io_in_a_bits_size; // @[Parameters.scala 92:48]
  assign _T_187 = io_in_a_bits_address ^ 36'ha000000; // @[Parameters.scala 137:31]
  assign _T_188 = {1'b0,$signed(_T_187)}; // @[Parameters.scala 137:49]
  assign _T_190 = $signed(_T_188) & -37'sh200000; // @[Parameters.scala 137:52]
  assign _T_191 = $signed(_T_190) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_192 = io_in_a_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  assign _T_193 = {1'b0,$signed(_T_192)}; // @[Parameters.scala 137:49]
  assign _T_195 = $signed(_T_193) & -37'sh800000000; // @[Parameters.scala 137:52]
  assign _T_196 = $signed(_T_195) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_197 = _T_191 | _T_196; // @[Parameters.scala 552:42]
  assign _T_198 = _T_185 & _T_197; // @[Parameters.scala 551:56]
  assign _T_202 = _T_198 | reset; // @[Monitor.scala 44:11]
  assign _T_203 = ~_T_202; // @[Monitor.scala 44:11]
  assign _T_217 = _T_4 & _T_185; // @[Mux.scala 27:72]
  assign _T_218 = _T_5 & _T_185; // @[Mux.scala 27:72]
  assign _T_220 = _T_217 | _T_218; // @[Mux.scala 27:72]
  assign _T_224 = _T_220 | reset; // @[Monitor.scala 44:11]
  assign _T_225 = ~_T_224; // @[Monitor.scala 44:11]
  assign _T_227 = _T_16 | reset; // @[Monitor.scala 44:11]
  assign _T_228 = ~_T_227; // @[Monitor.scala 44:11]
  assign _T_231 = _T_28 | reset; // @[Monitor.scala 44:11]
  assign _T_232 = ~_T_231; // @[Monitor.scala 44:11]
  assign _T_234 = _T_22 | reset; // @[Monitor.scala 44:11]
  assign _T_235 = ~_T_234; // @[Monitor.scala 44:11]
  assign _T_236 = io_in_a_bits_param <= 3'h2; // @[Bundles.scala 110:27]
  assign _T_238 = _T_236 | reset; // @[Monitor.scala 44:11]
  assign _T_239 = ~_T_238; // @[Monitor.scala 44:11]
  assign _T_240 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_241 = _T_240 == 8'h0; // @[Monitor.scala 86:31]
  assign _T_243 = _T_241 | reset; // @[Monitor.scala 44:11]
  assign _T_244 = ~_T_243; // @[Monitor.scala 44:11]
  assign _T_245 = ~io_in_a_bits_corrupt; // @[Monitor.scala 87:18]
  assign _T_247 = _T_245 | reset; // @[Monitor.scala 44:11]
  assign _T_248 = ~_T_247; // @[Monitor.scala 44:11]
  assign _T_249 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_366 = io_in_a_bits_param != 3'h0; // @[Monitor.scala 97:31]
  assign _T_368 = _T_366 | reset; // @[Monitor.scala 44:11]
  assign _T_369 = ~_T_368; // @[Monitor.scala 44:11]
  assign _T_379 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_381 = io_in_a_bits_size <= 4'h6; // @[Parameters.scala 93:42]
  assign _T_392 = $signed(_T_131) & -37'sh2200000; // @[Parameters.scala 137:52]
  assign _T_393 = $signed(_T_392) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_422 = $signed(_T_161) & -37'sh3000; // @[Parameters.scala 137:52]
  assign _T_423 = $signed(_T_422) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_434 = _T_129 | _T_393; // @[Parameters.scala 552:42]
  assign _T_435 = _T_434 | _T_196; // @[Parameters.scala 552:42]
  assign _T_436 = _T_435 | _T_144; // @[Parameters.scala 552:42]
  assign _T_437 = _T_436 | _T_149; // @[Parameters.scala 552:42]
  assign _T_438 = _T_437 | _T_154; // @[Parameters.scala 552:42]
  assign _T_439 = _T_438 | _T_159; // @[Parameters.scala 552:42]
  assign _T_440 = _T_439 | _T_423; // @[Parameters.scala 552:42]
  assign _T_441 = _T_440 | _T_169; // @[Parameters.scala 552:42]
  assign _T_442 = _T_441 | _T_174; // @[Parameters.scala 552:42]
  assign _T_443 = _T_381 & _T_442; // @[Parameters.scala 551:56]
  assign _T_445 = io_in_a_bits_size <= 4'h8; // @[Parameters.scala 93:42]
  assign _T_451 = $signed(_T_136) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_452 = $signed(_T_451) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_453 = _T_445 & _T_452; // @[Parameters.scala 551:56]
  assign _T_455 = _T_443 | _T_453; // @[Parameters.scala 553:30]
  assign _T_457 = _T_455 | reset; // @[Monitor.scala 44:11]
  assign _T_458 = ~_T_457; // @[Monitor.scala 44:11]
  assign _T_465 = io_in_a_bits_param == 3'h0; // @[Monitor.scala 106:31]
  assign _T_467 = _T_465 | reset; // @[Monitor.scala 44:11]
  assign _T_468 = ~_T_467; // @[Monitor.scala 44:11]
  assign _T_469 = io_in_a_bits_mask == _T_86; // @[Monitor.scala 107:30]
  assign _T_471 = _T_469 | reset; // @[Monitor.scala 44:11]
  assign _T_472 = ~_T_471; // @[Monitor.scala 44:11]
  assign _T_477 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_479 = io_in_a_bits_size <= 4'h7; // @[Parameters.scala 93:42]
  assign _T_487 = _T_479 & _T_129; // @[Parameters.scala 551:56]
  assign _T_537 = _T_393 | _T_196; // @[Parameters.scala 552:42]
  assign _T_538 = _T_537 | _T_144; // @[Parameters.scala 552:42]
  assign _T_539 = _T_538 | _T_149; // @[Parameters.scala 552:42]
  assign _T_540 = _T_539 | _T_154; // @[Parameters.scala 552:42]
  assign _T_541 = _T_540 | _T_159; // @[Parameters.scala 552:42]
  assign _T_542 = _T_541 | _T_423; // @[Parameters.scala 552:42]
  assign _T_543 = _T_542 | _T_169; // @[Parameters.scala 552:42]
  assign _T_544 = _T_543 | _T_174; // @[Parameters.scala 552:42]
  assign _T_545 = _T_381 & _T_544; // @[Parameters.scala 551:56]
  assign _T_557 = _T_487 | _T_545; // @[Parameters.scala 553:30]
  assign _T_558 = _T_557 | _T_453; // @[Parameters.scala 553:30]
  assign _T_560 = _T_558 | reset; // @[Monitor.scala 44:11]
  assign _T_561 = ~_T_560; // @[Monitor.scala 44:11]
  assign _T_576 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_671 = ~_T_86; // @[Monitor.scala 124:33]
  assign _T_672 = io_in_a_bits_mask & _T_671; // @[Monitor.scala 124:31]
  assign _T_673 = _T_672 == 8'h0; // @[Monitor.scala 124:40]
  assign _T_675 = _T_673 | reset; // @[Monitor.scala 44:11]
  assign _T_676 = ~_T_675; // @[Monitor.scala 44:11]
  assign _T_677 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_686 = io_in_a_bits_size <= 4'h3; // @[Parameters.scala 93:42]
  assign _T_740 = _T_537 | _T_139; // @[Parameters.scala 552:42]
  assign _T_741 = _T_740 | _T_144; // @[Parameters.scala 552:42]
  assign _T_742 = _T_741 | _T_149; // @[Parameters.scala 552:42]
  assign _T_743 = _T_742 | _T_154; // @[Parameters.scala 552:42]
  assign _T_744 = _T_743 | _T_159; // @[Parameters.scala 552:42]
  assign _T_745 = _T_744 | _T_164; // @[Parameters.scala 552:42]
  assign _T_746 = _T_745 | _T_169; // @[Parameters.scala 552:42]
  assign _T_747 = _T_746 | _T_174; // @[Parameters.scala 552:42]
  assign _T_748 = _T_686 & _T_747; // @[Parameters.scala 551:56]
  assign _T_752 = _T_748 | reset; // @[Monitor.scala 44:11]
  assign _T_753 = ~_T_752; // @[Monitor.scala 44:11]
  assign _T_760 = io_in_a_bits_param <= 3'h4; // @[Bundles.scala 140:33]
  assign _T_762 = _T_760 | reset; // @[Monitor.scala 44:11]
  assign _T_763 = ~_T_762; // @[Monitor.scala 44:11]
  assign _T_768 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_851 = io_in_a_bits_param <= 3'h3; // @[Bundles.scala 147:30]
  assign _T_853 = _T_851 | reset; // @[Monitor.scala 44:11]
  assign _T_854 = ~_T_853; // @[Monitor.scala 44:11]
  assign _T_859 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_930 = _T_381 & _T_197; // @[Parameters.scala 551:56]
  assign _T_943 = _T_930 | _T_453; // @[Parameters.scala 553:30]
  assign _T_945 = _T_943 | reset; // @[Monitor.scala 44:11]
  assign _T_946 = ~_T_945; // @[Monitor.scala 44:11]
  assign _T_953 = io_in_a_bits_param <= 3'h1; // @[Bundles.scala 160:28]
  assign _T_955 = _T_953 | reset; // @[Monitor.scala 44:11]
  assign _T_956 = ~_T_955; // @[Monitor.scala 44:11]
  assign _T_965 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_967 = _T_965 | reset; // @[Monitor.scala 51:11]
  assign _T_968 = ~_T_967; // @[Monitor.scala 51:11]
  assign _T_969 = io_in_d_bits_source == 4'h0; // @[Parameters.scala 47:9]
  assign _T_970 = io_in_d_bits_source == 4'h1; // @[Parameters.scala 47:9]
  assign _T_975 = 4'h2 <= io_in_d_bits_source; // @[Parameters.scala 57:34]
  assign _T_977 = io_in_d_bits_source <= 4'h8; // @[Parameters.scala 58:20]
  assign _T_978 = _T_975 & _T_977; // @[Parameters.scala 57:50]
  assign _T_980 = _T_969 | _T_970; // @[Parameters.scala 924:46]
  assign _T_981 = _T_980 | _T_978; // @[Parameters.scala 924:46]
  assign _T_983 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_985 = _T_981 | reset; // @[Monitor.scala 51:11]
  assign _T_986 = ~_T_985; // @[Monitor.scala 51:11]
  assign _T_987 = io_in_d_bits_size >= 4'h3; // @[Monitor.scala 309:27]
  assign _T_989 = _T_987 | reset; // @[Monitor.scala 51:11]
  assign _T_990 = ~_T_989; // @[Monitor.scala 51:11]
  assign _T_991 = io_in_d_bits_param == 2'h0; // @[Monitor.scala 310:28]
  assign _T_993 = _T_991 | reset; // @[Monitor.scala 51:11]
  assign _T_994 = ~_T_993; // @[Monitor.scala 51:11]
  assign _T_995 = ~io_in_d_bits_corrupt; // @[Monitor.scala 311:15]
  assign _T_997 = _T_995 | reset; // @[Monitor.scala 51:11]
  assign _T_998 = ~_T_997; // @[Monitor.scala 51:11]
  assign _T_999 = ~io_in_d_bits_denied; // @[Monitor.scala 312:15]
  assign _T_1001 = _T_999 | reset; // @[Monitor.scala 51:11]
  assign _T_1002 = ~_T_1001; // @[Monitor.scala 51:11]
  assign _T_1003 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_1014 = io_in_d_bits_param <= 2'h2; // @[Bundles.scala 104:26]
  assign _T_1016 = _T_1014 | reset; // @[Monitor.scala 51:11]
  assign _T_1017 = ~_T_1016; // @[Monitor.scala 51:11]
  assign _T_1018 = io_in_d_bits_param != 2'h2; // @[Monitor.scala 320:28]
  assign _T_1020 = _T_1018 | reset; // @[Monitor.scala 51:11]
  assign _T_1021 = ~_T_1020; // @[Monitor.scala 51:11]
  assign _T_1031 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_1051 = _T_999 | io_in_d_bits_corrupt; // @[Monitor.scala 331:30]
  assign _T_1053 = _T_1051 | reset; // @[Monitor.scala 51:11]
  assign _T_1054 = ~_T_1053; // @[Monitor.scala 51:11]
  assign _T_1060 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_1077 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_1095 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_1116 = io_in_b_bits_source == 4'h0; // @[Parameters.scala 47:9]
  assign _T_1119 = {1'b0,$signed(io_in_b_bits_address)}; // @[Parameters.scala 137:49]
  assign _T_1124 = io_in_b_bits_source == 4'h1; // @[Parameters.scala 47:9]
  assign _T_1136 = 4'h2 <= io_in_b_bits_source; // @[Parameters.scala 57:34]
  assign _T_1138 = io_in_b_bits_source <= 4'h8; // @[Parameters.scala 58:20]
  assign _T_1139 = _T_1136 & _T_1138; // @[Parameters.scala 57:50]
  assign _T_1152 = io_in_b_bits_address ^ 36'h400000000; // @[Parameters.scala 137:31]
  assign _T_1153 = {1'b0,$signed(_T_1152)}; // @[Parameters.scala 137:49]
  assign _T_1155 = $signed(_T_1153) & -37'sh400000000; // @[Parameters.scala 137:52]
  assign _T_1156 = $signed(_T_1155) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1157 = io_in_b_bits_address ^ 36'h8000000; // @[Parameters.scala 137:31]
  assign _T_1158 = {1'b0,$signed(_T_1157)}; // @[Parameters.scala 137:49]
  assign _T_1160 = $signed(_T_1158) & -37'sh200000; // @[Parameters.scala 137:52]
  assign _T_1161 = $signed(_T_1160) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1162 = io_in_b_bits_address ^ 36'ha000000; // @[Parameters.scala 137:31]
  assign _T_1163 = {1'b0,$signed(_T_1162)}; // @[Parameters.scala 137:49]
  assign _T_1165 = $signed(_T_1163) & -37'sh200000; // @[Parameters.scala 137:52]
  assign _T_1166 = $signed(_T_1165) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1167 = io_in_b_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  assign _T_1168 = {1'b0,$signed(_T_1167)}; // @[Parameters.scala 137:49]
  assign _T_1170 = $signed(_T_1168) & -37'sh800000000; // @[Parameters.scala 137:52]
  assign _T_1171 = $signed(_T_1170) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1172 = io_in_b_bits_address ^ 36'h3000; // @[Parameters.scala 137:31]
  assign _T_1173 = {1'b0,$signed(_T_1172)}; // @[Parameters.scala 137:49]
  assign _T_1175 = $signed(_T_1173) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_1176 = $signed(_T_1175) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1177 = io_in_b_bits_address ^ 36'hc000000; // @[Parameters.scala 137:31]
  assign _T_1178 = {1'b0,$signed(_T_1177)}; // @[Parameters.scala 137:49]
  assign _T_1180 = $signed(_T_1178) & -37'sh4000000; // @[Parameters.scala 137:52]
  assign _T_1181 = $signed(_T_1180) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1182 = io_in_b_bits_address ^ 36'h2000000; // @[Parameters.scala 137:31]
  assign _T_1183 = {1'b0,$signed(_T_1182)}; // @[Parameters.scala 137:49]
  assign _T_1185 = $signed(_T_1183) & -37'sh10000; // @[Parameters.scala 137:52]
  assign _T_1186 = $signed(_T_1185) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1190 = $signed(_T_1119) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_1191 = $signed(_T_1190) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1192 = io_in_b_bits_address ^ 36'h20000000; // @[Parameters.scala 137:31]
  assign _T_1193 = {1'b0,$signed(_T_1192)}; // @[Parameters.scala 137:49]
  assign _T_1195 = $signed(_T_1193) & -37'sh20000000; // @[Parameters.scala 137:52]
  assign _T_1196 = $signed(_T_1195) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1197 = io_in_b_bits_address ^ 36'h10000000; // @[Parameters.scala 137:31]
  assign _T_1198 = {1'b0,$signed(_T_1197)}; // @[Parameters.scala 137:49]
  assign _T_1200 = $signed(_T_1198) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_1201 = $signed(_T_1200) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1202 = io_in_b_bits_address ^ 36'h10001000; // @[Parameters.scala 137:31]
  assign _T_1203 = {1'b0,$signed(_T_1202)}; // @[Parameters.scala 137:49]
  assign _T_1205 = $signed(_T_1203) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_1206 = $signed(_T_1205) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1207 = io_in_b_bits_address ^ 36'h10003000; // @[Parameters.scala 137:31]
  assign _T_1208 = {1'b0,$signed(_T_1207)}; // @[Parameters.scala 137:49]
  assign _T_1210 = $signed(_T_1208) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_1211 = $signed(_T_1210) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1212 = io_in_b_bits_address ^ 36'h4000; // @[Parameters.scala 137:31]
  assign _T_1213 = {1'b0,$signed(_T_1212)}; // @[Parameters.scala 137:49]
  assign _T_1215 = $signed(_T_1213) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_1216 = $signed(_T_1215) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1217 = io_in_b_bits_address ^ 36'h2010000; // @[Parameters.scala 137:31]
  assign _T_1218 = {1'b0,$signed(_T_1217)}; // @[Parameters.scala 137:49]
  assign _T_1220 = $signed(_T_1218) & -37'sh4000; // @[Parameters.scala 137:52]
  assign _T_1221 = $signed(_T_1220) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1223 = _T_1156 | _T_1161; // @[Parameters.scala 535:64]
  assign _T_1224 = _T_1223 | _T_1166; // @[Parameters.scala 535:64]
  assign _T_1225 = _T_1224 | _T_1171; // @[Parameters.scala 535:64]
  assign _T_1226 = _T_1225 | _T_1176; // @[Parameters.scala 535:64]
  assign _T_1227 = _T_1226 | _T_1181; // @[Parameters.scala 535:64]
  assign _T_1228 = _T_1227 | _T_1186; // @[Parameters.scala 535:64]
  assign _T_1229 = _T_1228 | _T_1191; // @[Parameters.scala 535:64]
  assign _T_1230 = _T_1229 | _T_1196; // @[Parameters.scala 535:64]
  assign _T_1231 = _T_1230 | _T_1201; // @[Parameters.scala 535:64]
  assign _T_1232 = _T_1231 | _T_1206; // @[Parameters.scala 535:64]
  assign _T_1233 = _T_1232 | _T_1211; // @[Parameters.scala 535:64]
  assign _T_1234 = _T_1233 | _T_1216; // @[Parameters.scala 535:64]
  assign _T_1235 = _T_1234 | _T_1221; // @[Parameters.scala 535:64]
  assign _T_1240 = io_in_b_bits_address & 36'h3f; // @[Edges.scala 22:16]
  assign _T_1241 = _T_1240 == 36'h0; // @[Edges.scala 22:24]
  assign _T_1319 = _T_1139 ? 2'h2 : 2'h0; // @[Mux.scala 27:72]
  assign _GEN_34 = {{1'd0}, _T_1124}; // @[Mux.scala 27:72]
  assign _T_1321 = _GEN_34 | _T_1319; // @[Mux.scala 27:72]
  assign _GEN_35 = {{2'd0}, _T_1321}; // @[Monitor.scala 162:113]
  assign _T_1323 = _GEN_35 == io_in_b_bits_source; // @[Monitor.scala 162:113]
  assign _T_1341 = _T_1116 | _T_1124; // @[Mux.scala 27:72]
  assign _T_1345 = _T_1341 | reset; // @[Monitor.scala 44:11]
  assign _T_1346 = ~_T_1345; // @[Monitor.scala 44:11]
  assign _T_1348 = _T_1235 | reset; // @[Monitor.scala 44:11]
  assign _T_1349 = ~_T_1348; // @[Monitor.scala 44:11]
  assign _T_1351 = _T_1323 | reset; // @[Monitor.scala 44:11]
  assign _T_1352 = ~_T_1351; // @[Monitor.scala 44:11]
  assign _T_1354 = _T_1241 | reset; // @[Monitor.scala 44:11]
  assign _T_1355 = ~_T_1354; // @[Monitor.scala 44:11]
  assign _T_1356 = io_in_b_bits_param <= 2'h2; // @[Bundles.scala 104:26]
  assign _T_1358 = _T_1356 | reset; // @[Monitor.scala 44:11]
  assign _T_1359 = ~_T_1358; // @[Monitor.scala 44:11]
  assign _T_1504 = io_in_c_bits_source == 4'h0; // @[Parameters.scala 47:9]
  assign _T_1505 = io_in_c_bits_source == 4'h1; // @[Parameters.scala 47:9]
  assign _T_1510 = 4'h2 <= io_in_c_bits_source; // @[Parameters.scala 57:34]
  assign _T_1512 = io_in_c_bits_source <= 4'h8; // @[Parameters.scala 58:20]
  assign _T_1513 = _T_1510 & _T_1512; // @[Parameters.scala 57:50]
  assign _T_1515 = _T_1504 | _T_1505; // @[Parameters.scala 924:46]
  assign _T_1516 = _T_1515 | _T_1513; // @[Parameters.scala 924:46]
  assign _T_1518 = 23'hff << io_in_c_bits_size; // @[package.scala 189:77]
  assign _T_1520 = ~_T_1518[7:0]; // @[package.scala 189:46]
  assign _GEN_36 = {{28'd0}, _T_1520}; // @[Edges.scala 22:16]
  assign _T_1521 = io_in_c_bits_address & _GEN_36; // @[Edges.scala 22:16]
  assign _T_1522 = _T_1521 == 36'h0; // @[Edges.scala 22:24]
  assign _T_1523 = io_in_c_bits_address ^ 36'h400000000; // @[Parameters.scala 137:31]
  assign _T_1524 = {1'b0,$signed(_T_1523)}; // @[Parameters.scala 137:49]
  assign _T_1526 = $signed(_T_1524) & -37'sh400000000; // @[Parameters.scala 137:52]
  assign _T_1527 = $signed(_T_1526) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1528 = io_in_c_bits_address ^ 36'h8000000; // @[Parameters.scala 137:31]
  assign _T_1529 = {1'b0,$signed(_T_1528)}; // @[Parameters.scala 137:49]
  assign _T_1531 = $signed(_T_1529) & -37'sh200000; // @[Parameters.scala 137:52]
  assign _T_1532 = $signed(_T_1531) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1533 = io_in_c_bits_address ^ 36'ha000000; // @[Parameters.scala 137:31]
  assign _T_1534 = {1'b0,$signed(_T_1533)}; // @[Parameters.scala 137:49]
  assign _T_1536 = $signed(_T_1534) & -37'sh200000; // @[Parameters.scala 137:52]
  assign _T_1537 = $signed(_T_1536) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1538 = io_in_c_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  assign _T_1539 = {1'b0,$signed(_T_1538)}; // @[Parameters.scala 137:49]
  assign _T_1541 = $signed(_T_1539) & -37'sh800000000; // @[Parameters.scala 137:52]
  assign _T_1542 = $signed(_T_1541) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1543 = io_in_c_bits_address ^ 36'h3000; // @[Parameters.scala 137:31]
  assign _T_1544 = {1'b0,$signed(_T_1543)}; // @[Parameters.scala 137:49]
  assign _T_1546 = $signed(_T_1544) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_1547 = $signed(_T_1546) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1548 = io_in_c_bits_address ^ 36'hc000000; // @[Parameters.scala 137:31]
  assign _T_1549 = {1'b0,$signed(_T_1548)}; // @[Parameters.scala 137:49]
  assign _T_1551 = $signed(_T_1549) & -37'sh4000000; // @[Parameters.scala 137:52]
  assign _T_1552 = $signed(_T_1551) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1553 = io_in_c_bits_address ^ 36'h2000000; // @[Parameters.scala 137:31]
  assign _T_1554 = {1'b0,$signed(_T_1553)}; // @[Parameters.scala 137:49]
  assign _T_1556 = $signed(_T_1554) & -37'sh10000; // @[Parameters.scala 137:52]
  assign _T_1557 = $signed(_T_1556) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1559 = {1'b0,$signed(io_in_c_bits_address)}; // @[Parameters.scala 137:49]
  assign _T_1561 = $signed(_T_1559) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_1562 = $signed(_T_1561) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1563 = io_in_c_bits_address ^ 36'h20000000; // @[Parameters.scala 137:31]
  assign _T_1564 = {1'b0,$signed(_T_1563)}; // @[Parameters.scala 137:49]
  assign _T_1566 = $signed(_T_1564) & -37'sh20000000; // @[Parameters.scala 137:52]
  assign _T_1567 = $signed(_T_1566) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1568 = io_in_c_bits_address ^ 36'h10000000; // @[Parameters.scala 137:31]
  assign _T_1569 = {1'b0,$signed(_T_1568)}; // @[Parameters.scala 137:49]
  assign _T_1571 = $signed(_T_1569) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_1572 = $signed(_T_1571) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1573 = io_in_c_bits_address ^ 36'h10001000; // @[Parameters.scala 137:31]
  assign _T_1574 = {1'b0,$signed(_T_1573)}; // @[Parameters.scala 137:49]
  assign _T_1576 = $signed(_T_1574) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_1577 = $signed(_T_1576) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1578 = io_in_c_bits_address ^ 36'h10003000; // @[Parameters.scala 137:31]
  assign _T_1579 = {1'b0,$signed(_T_1578)}; // @[Parameters.scala 137:49]
  assign _T_1581 = $signed(_T_1579) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_1582 = $signed(_T_1581) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1583 = io_in_c_bits_address ^ 36'h4000; // @[Parameters.scala 137:31]
  assign _T_1584 = {1'b0,$signed(_T_1583)}; // @[Parameters.scala 137:49]
  assign _T_1586 = $signed(_T_1584) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_1587 = $signed(_T_1586) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1588 = io_in_c_bits_address ^ 36'h2010000; // @[Parameters.scala 137:31]
  assign _T_1589 = {1'b0,$signed(_T_1588)}; // @[Parameters.scala 137:49]
  assign _T_1591 = $signed(_T_1589) & -37'sh4000; // @[Parameters.scala 137:52]
  assign _T_1592 = $signed(_T_1591) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_1594 = _T_1527 | _T_1532; // @[Parameters.scala 535:64]
  assign _T_1595 = _T_1594 | _T_1537; // @[Parameters.scala 535:64]
  assign _T_1596 = _T_1595 | _T_1542; // @[Parameters.scala 535:64]
  assign _T_1597 = _T_1596 | _T_1547; // @[Parameters.scala 535:64]
  assign _T_1598 = _T_1597 | _T_1552; // @[Parameters.scala 535:64]
  assign _T_1599 = _T_1598 | _T_1557; // @[Parameters.scala 535:64]
  assign _T_1600 = _T_1599 | _T_1562; // @[Parameters.scala 535:64]
  assign _T_1601 = _T_1600 | _T_1567; // @[Parameters.scala 535:64]
  assign _T_1602 = _T_1601 | _T_1572; // @[Parameters.scala 535:64]
  assign _T_1603 = _T_1602 | _T_1577; // @[Parameters.scala 535:64]
  assign _T_1604 = _T_1603 | _T_1582; // @[Parameters.scala 535:64]
  assign _T_1605 = _T_1604 | _T_1587; // @[Parameters.scala 535:64]
  assign _T_1606 = _T_1605 | _T_1592; // @[Parameters.scala 535:64]
  assign _T_1643 = io_in_c_bits_opcode == 3'h4; // @[Monitor.scala 239:25]
  assign _T_1645 = _T_1606 | reset; // @[Monitor.scala 44:11]
  assign _T_1646 = ~_T_1645; // @[Monitor.scala 44:11]
  assign _T_1648 = _T_1516 | reset; // @[Monitor.scala 44:11]
  assign _T_1649 = ~_T_1648; // @[Monitor.scala 44:11]
  assign _T_1650 = io_in_c_bits_size >= 4'h3; // @[Monitor.scala 242:30]
  assign _T_1652 = _T_1650 | reset; // @[Monitor.scala 44:11]
  assign _T_1653 = ~_T_1652; // @[Monitor.scala 44:11]
  assign _T_1655 = _T_1522 | reset; // @[Monitor.scala 44:11]
  assign _T_1656 = ~_T_1655; // @[Monitor.scala 44:11]
  assign _T_1657 = io_in_c_bits_param <= 3'h5; // @[Bundles.scala 122:29]
  assign _T_1659 = _T_1657 | reset; // @[Monitor.scala 44:11]
  assign _T_1660 = ~_T_1659; // @[Monitor.scala 44:11]
  assign _T_1661 = ~io_in_c_bits_corrupt; // @[Monitor.scala 245:18]
  assign _T_1663 = _T_1661 | reset; // @[Monitor.scala 44:11]
  assign _T_1664 = ~_T_1663; // @[Monitor.scala 44:11]
  assign _T_1665 = io_in_c_bits_opcode == 3'h5; // @[Monitor.scala 248:25]
  assign _T_1683 = io_in_c_bits_opcode == 3'h6; // @[Monitor.scala 256:25]
  assign _T_1745 = 4'h6 == io_in_c_bits_size; // @[Parameters.scala 92:48]
  assign _T_1757 = _T_1537 | _T_1542; // @[Parameters.scala 552:42]
  assign _T_1758 = _T_1745 & _T_1757; // @[Parameters.scala 551:56]
  assign _T_1762 = _T_1758 | reset; // @[Monitor.scala 44:11]
  assign _T_1763 = ~_T_1762; // @[Monitor.scala 44:11]
  assign _T_1777 = _T_1504 & _T_1745; // @[Mux.scala 27:72]
  assign _T_1778 = _T_1505 & _T_1745; // @[Mux.scala 27:72]
  assign _T_1780 = _T_1777 | _T_1778; // @[Mux.scala 27:72]
  assign _T_1784 = _T_1780 | reset; // @[Monitor.scala 44:11]
  assign _T_1785 = ~_T_1784; // @[Monitor.scala 44:11]
  assign _T_1796 = io_in_c_bits_param <= 3'h2; // @[Bundles.scala 116:29]
  assign _T_1798 = _T_1796 | reset; // @[Monitor.scala 44:11]
  assign _T_1799 = ~_T_1798; // @[Monitor.scala 44:11]
  assign _T_1804 = io_in_c_bits_opcode == 3'h7; // @[Monitor.scala 266:25]
  assign _T_1921 = io_in_c_bits_opcode == 3'h0; // @[Monitor.scala 275:25]
  assign _T_1931 = io_in_c_bits_param == 3'h0; // @[Monitor.scala 279:31]
  assign _T_1933 = _T_1931 | reset; // @[Monitor.scala 44:11]
  assign _T_1934 = ~_T_1933; // @[Monitor.scala 44:11]
  assign _T_1939 = io_in_c_bits_opcode == 3'h1; // @[Monitor.scala 283:25]
  assign _T_1953 = io_in_c_bits_opcode == 3'h2; // @[Monitor.scala 290:25]
  assign _T_1975 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_1982 = ~io_in_a_bits_opcode[2]; // @[Edges.scala 93:28]
  assign _T_1986 = _T_1984 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1987 = _T_1984 == 5'h0; // @[Edges.scala 232:25]
  assign _T_2000 = ~_T_1987; // @[Monitor.scala 386:22]
  assign _T_2001 = io_in_a_valid & _T_2000; // @[Monitor.scala 386:19]
  assign _T_2002 = io_in_a_bits_opcode == _T_1995; // @[Monitor.scala 387:32]
  assign _T_2004 = _T_2002 | reset; // @[Monitor.scala 44:11]
  assign _T_2005 = ~_T_2004; // @[Monitor.scala 44:11]
  assign _T_2006 = io_in_a_bits_param == _T_1996; // @[Monitor.scala 388:32]
  assign _T_2008 = _T_2006 | reset; // @[Monitor.scala 44:11]
  assign _T_2009 = ~_T_2008; // @[Monitor.scala 44:11]
  assign _T_2010 = io_in_a_bits_size == _T_1997; // @[Monitor.scala 389:32]
  assign _T_2012 = _T_2010 | reset; // @[Monitor.scala 44:11]
  assign _T_2013 = ~_T_2012; // @[Monitor.scala 44:11]
  assign _T_2014 = io_in_a_bits_source == _T_1998; // @[Monitor.scala 390:32]
  assign _T_2016 = _T_2014 | reset; // @[Monitor.scala 44:11]
  assign _T_2017 = ~_T_2016; // @[Monitor.scala 44:11]
  assign _T_2018 = io_in_a_bits_address == _T_1999; // @[Monitor.scala 391:32]
  assign _T_2020 = _T_2018 | reset; // @[Monitor.scala 44:11]
  assign _T_2021 = ~_T_2020; // @[Monitor.scala 44:11]
  assign _T_2023 = _T_1975 & _T_1987; // @[Monitor.scala 393:20]
  assign _T_2024 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_2026 = 23'hff << io_in_d_bits_size; // @[package.scala 189:77]
  assign _T_2028 = ~_T_2026[7:0]; // @[package.scala 189:46]
  assign _T_2034 = _T_2032 - 5'h1; // @[Edges.scala 231:28]
  assign _T_2035 = _T_2032 == 5'h0; // @[Edges.scala 232:25]
  assign _T_2049 = ~_T_2035; // @[Monitor.scala 538:22]
  assign _T_2050 = io_in_d_valid & _T_2049; // @[Monitor.scala 538:19]
  assign _T_2051 = io_in_d_bits_opcode == _T_2043; // @[Monitor.scala 539:29]
  assign _T_2053 = _T_2051 | reset; // @[Monitor.scala 51:11]
  assign _T_2054 = ~_T_2053; // @[Monitor.scala 51:11]
  assign _T_2055 = io_in_d_bits_param == _T_2044; // @[Monitor.scala 540:29]
  assign _T_2057 = _T_2055 | reset; // @[Monitor.scala 51:11]
  assign _T_2058 = ~_T_2057; // @[Monitor.scala 51:11]
  assign _T_2059 = io_in_d_bits_size == _T_2045; // @[Monitor.scala 541:29]
  assign _T_2061 = _T_2059 | reset; // @[Monitor.scala 51:11]
  assign _T_2062 = ~_T_2061; // @[Monitor.scala 51:11]
  assign _T_2063 = io_in_d_bits_source == _T_2046; // @[Monitor.scala 542:29]
  assign _T_2065 = _T_2063 | reset; // @[Monitor.scala 51:11]
  assign _T_2066 = ~_T_2065; // @[Monitor.scala 51:11]
  assign _T_2067 = io_in_d_bits_sink == _T_2047; // @[Monitor.scala 543:29]
  assign _T_2069 = _T_2067 | reset; // @[Monitor.scala 51:11]
  assign _T_2070 = ~_T_2069; // @[Monitor.scala 51:11]
  assign _T_2071 = io_in_d_bits_denied == _T_2048; // @[Monitor.scala 544:29]
  assign _T_2073 = _T_2071 | reset; // @[Monitor.scala 51:11]
  assign _T_2074 = ~_T_2073; // @[Monitor.scala 51:11]
  assign _T_2076 = _T_2024 & _T_2035; // @[Monitor.scala 546:20]
  assign _T_2077 = io_in_b_ready & io_in_b_valid; // @[Decoupled.scala 40:37]
  assign _T_2088 = _T_2086 - 5'h1; // @[Edges.scala 231:28]
  assign _T_2089 = _T_2086 == 5'h0; // @[Edges.scala 232:25]
  assign _T_2102 = ~_T_2089; // @[Monitor.scala 409:22]
  assign _T_2103 = io_in_b_valid & _T_2102; // @[Monitor.scala 409:19]
  assign _T_2108 = io_in_b_bits_param == _T_2098; // @[Monitor.scala 411:32]
  assign _T_2110 = _T_2108 | reset; // @[Monitor.scala 44:11]
  assign _T_2111 = ~_T_2110; // @[Monitor.scala 44:11]
  assign _T_2116 = io_in_b_bits_source == _T_2100; // @[Monitor.scala 413:32]
  assign _T_2118 = _T_2116 | reset; // @[Monitor.scala 44:11]
  assign _T_2119 = ~_T_2118; // @[Monitor.scala 44:11]
  assign _T_2120 = io_in_b_bits_address == _T_2101; // @[Monitor.scala 414:32]
  assign _T_2122 = _T_2120 | reset; // @[Monitor.scala 44:11]
  assign _T_2123 = ~_T_2122; // @[Monitor.scala 44:11]
  assign _T_2125 = _T_2077 & _T_2089; // @[Monitor.scala 416:20]
  assign _T_2126 = io_in_c_ready & io_in_c_valid; // @[Decoupled.scala 40:37]
  assign _T_2136 = _T_2134 - 5'h1; // @[Edges.scala 231:28]
  assign _T_2137 = _T_2134 == 5'h0; // @[Edges.scala 232:25]
  assign _T_2150 = ~_T_2137; // @[Monitor.scala 514:22]
  assign _T_2151 = io_in_c_valid & _T_2150; // @[Monitor.scala 514:19]
  assign _T_2152 = io_in_c_bits_opcode == _T_2145; // @[Monitor.scala 515:32]
  assign _T_2154 = _T_2152 | reset; // @[Monitor.scala 44:11]
  assign _T_2155 = ~_T_2154; // @[Monitor.scala 44:11]
  assign _T_2156 = io_in_c_bits_param == _T_2146; // @[Monitor.scala 516:32]
  assign _T_2158 = _T_2156 | reset; // @[Monitor.scala 44:11]
  assign _T_2159 = ~_T_2158; // @[Monitor.scala 44:11]
  assign _T_2160 = io_in_c_bits_size == _T_2147; // @[Monitor.scala 517:32]
  assign _T_2162 = _T_2160 | reset; // @[Monitor.scala 44:11]
  assign _T_2163 = ~_T_2162; // @[Monitor.scala 44:11]
  assign _T_2164 = io_in_c_bits_source == _T_2148; // @[Monitor.scala 518:32]
  assign _T_2166 = _T_2164 | reset; // @[Monitor.scala 44:11]
  assign _T_2167 = ~_T_2166; // @[Monitor.scala 44:11]
  assign _T_2168 = io_in_c_bits_address == _T_2149; // @[Monitor.scala 519:32]
  assign _T_2170 = _T_2168 | reset; // @[Monitor.scala 44:11]
  assign _T_2171 = ~_T_2170; // @[Monitor.scala 44:11]
  assign _T_2173 = _T_2126 & _T_2137; // @[Monitor.scala 521:20]
  assign _T_2186 = _T_2184 - 5'h1; // @[Edges.scala 231:28]
  assign _T_2187 = _T_2184 == 5'h0; // @[Edges.scala 232:25]
  assign _T_2205 = _T_2203 - 5'h1; // @[Edges.scala 231:28]
  assign _T_2206 = _T_2203 == 5'h0; // @[Edges.scala 232:25]
  assign _T_2216 = _T_1975 & _T_2187; // @[Monitor.scala 574:27]
  assign _T_2218 = 16'h1 << io_in_a_bits_source; // @[OneHot.scala 58:35]
  assign _T_2219 = _T_2174 >> io_in_a_bits_source; // @[Monitor.scala 576:23]
  assign _T_2221 = ~_T_2219[0]; // @[Monitor.scala 576:14]
  assign _T_2223 = _T_2221 | reset; // @[Monitor.scala 576:13]
  assign _T_2224 = ~_T_2223; // @[Monitor.scala 576:13]
  assign _GEN_27 = _T_2216 ? _T_2218 : 16'h0; // @[Monitor.scala 574:72]
  assign _T_2228 = _T_2024 & _T_2206; // @[Monitor.scala 581:27]
  assign _T_2230 = ~_T_983; // @[Monitor.scala 581:75]
  assign _T_2231 = _T_2228 & _T_2230; // @[Monitor.scala 581:72]
  assign _T_2232 = 16'h1 << io_in_d_bits_source; // @[OneHot.scala 58:35]
  assign _T_2233 = _GEN_27[8:0] | _T_2174; // @[Monitor.scala 583:21]
  assign _T_2234 = _T_2233 >> io_in_d_bits_source; // @[Monitor.scala 583:32]
  assign _T_2237 = _T_2234[0] | reset; // @[Monitor.scala 51:11]
  assign _T_2238 = ~_T_2237; // @[Monitor.scala 51:11]
  assign _GEN_28 = _T_2231 ? _T_2232 : 16'h0; // @[Monitor.scala 581:91]
  assign _T_2239 = _GEN_27[8:0] != _GEN_28[8:0]; // @[Monitor.scala 587:20]
  assign _T_2240 = _GEN_27[8:0] != 9'h0; // @[Monitor.scala 587:40]
  assign _T_2241 = ~_T_2240; // @[Monitor.scala 587:33]
  assign _T_2242 = _T_2239 | _T_2241; // @[Monitor.scala 587:30]
  assign _T_2244 = _T_2242 | reset; // @[Monitor.scala 51:11]
  assign _T_2245 = ~_T_2244; // @[Monitor.scala 51:11]
  assign _T_2246 = _T_2174 | _GEN_27[8:0]; // @[Monitor.scala 590:27]
  assign _T_2247 = ~_GEN_28[8:0]; // @[Monitor.scala 590:38]
  assign _T_2248 = _T_2246 & _T_2247; // @[Monitor.scala 590:36]
  assign _T_2250 = _T_2174 != 9'h0; // @[Monitor.scala 595:23]
  assign _T_2251 = ~_T_2250; // @[Monitor.scala 595:13]
  assign _T_2252 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_2253 = _T_2251 | _T_2252; // @[Monitor.scala 595:27]
  assign _T_2254 = _T_2249 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_2255 = _T_2253 | _T_2254; // @[Monitor.scala 595:44]
  assign _T_2257 = _T_2255 | reset; // @[Monitor.scala 595:12]
  assign _T_2258 = ~_T_2257; // @[Monitor.scala 595:12]
  assign _T_2260 = _T_2249 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_2263 = _T_1975 | _T_2024; // @[Monitor.scala 598:27]
  assign _T_2275 = _T_2273 - 5'h1; // @[Edges.scala 231:28]
  assign _T_2276 = _T_2273 == 5'h0; // @[Edges.scala 232:25]
  assign _T_2286 = _T_2024 & _T_2276; // @[Monitor.scala 700:27]
  assign _T_2289 = ~io_in_d_bits_opcode[1]; // @[Edges.scala 72:43]
  assign _T_2290 = io_in_d_bits_opcode[2] & _T_2289; // @[Edges.scala 72:40]
  assign _T_2291 = _T_2286 & _T_2290; // @[Monitor.scala 700:38]
  assign _T_2292 = 16'h1 << io_in_d_bits_sink; // @[OneHot.scala 58:35]
  assign _T_2293 = _T_2264 >> io_in_d_bits_sink; // @[Monitor.scala 702:23]
  assign _T_2295 = ~_T_2293[0]; // @[Monitor.scala 702:14]
  assign _T_2297 = _T_2295 | reset; // @[Monitor.scala 51:11]
  assign _T_2298 = ~_T_2297; // @[Monitor.scala 51:11]
  assign _GEN_31 = _T_2291 ? _T_2292 : 16'h0; // @[Monitor.scala 700:72]
  assign _T_2300 = io_in_e_ready & io_in_e_valid; // @[Decoupled.scala 40:37]
  assign _T_2303 = 16'h1 << io_in_e_bits_sink; // @[OneHot.scala 58:35]
  assign _T_2304 = _GEN_31 | _T_2264; // @[Monitor.scala 708:24]
  assign _T_2305 = _T_2304 >> io_in_e_bits_sink; // @[Monitor.scala 708:35]
  assign _T_2308 = _T_2305[0] | reset; // @[Monitor.scala 44:11]
  assign _T_2309 = ~_T_2308; // @[Monitor.scala 44:11]
  assign _GEN_32 = _T_2300 ? _T_2303 : 16'h0; // @[Monitor.scala 706:73]
  assign _T_2310 = _T_2264 | _GEN_31; // @[Monitor.scala 713:27]
  assign _T_2311 = ~_GEN_32; // @[Monitor.scala 713:38]
  assign _T_2312 = _T_2310 & _T_2311; // @[Monitor.scala 713:36]
  assign _GEN_37 = io_in_a_valid & _T_123; // @[Monitor.scala 44:11]
  assign _GEN_53 = io_in_a_valid & _T_249; // @[Monitor.scala 44:11]
  assign _GEN_71 = io_in_a_valid & _T_379; // @[Monitor.scala 44:11]
  assign _GEN_83 = io_in_a_valid & _T_477; // @[Monitor.scala 44:11]
  assign _GEN_93 = io_in_a_valid & _T_576; // @[Monitor.scala 44:11]
  assign _GEN_103 = io_in_a_valid & _T_677; // @[Monitor.scala 44:11]
  assign _GEN_113 = io_in_a_valid & _T_768; // @[Monitor.scala 44:11]
  assign _GEN_123 = io_in_a_valid & _T_859; // @[Monitor.scala 44:11]
  assign _GEN_135 = io_in_d_valid & _T_983; // @[Monitor.scala 51:11]
  assign _GEN_145 = io_in_d_valid & _T_1003; // @[Monitor.scala 51:11]
  assign _GEN_155 = io_in_d_valid & _T_1031; // @[Monitor.scala 51:11]
  assign _GEN_165 = io_in_d_valid & _T_1060; // @[Monitor.scala 51:11]
  assign _GEN_171 = io_in_d_valid & _T_1077; // @[Monitor.scala 51:11]
  assign _GEN_177 = io_in_d_valid & _T_1095; // @[Monitor.scala 51:11]
  assign _GEN_183 = io_in_c_valid & _T_1643; // @[Monitor.scala 44:11]
  assign _GEN_195 = io_in_c_valid & _T_1665; // @[Monitor.scala 44:11]
  assign _GEN_205 = io_in_c_valid & _T_1683; // @[Monitor.scala 44:11]
  assign _GEN_219 = io_in_c_valid & _T_1804; // @[Monitor.scala 44:11]
  assign _GEN_231 = io_in_c_valid & _T_1921; // @[Monitor.scala 44:11]
  assign _GEN_241 = io_in_c_valid & _T_1939; // @[Monitor.scala 44:11]
  assign _GEN_249 = io_in_c_valid & _T_1953; // @[Monitor.scala 44:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_1984 = _RAND_0[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_1995 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_1996 = _RAND_2[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_1997 = _RAND_3[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_1998 = _RAND_4[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {2{`RANDOM}};
  _T_1999 = _RAND_5[35:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_2032 = _RAND_6[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_2043 = _RAND_7[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_2044 = _RAND_8[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_2045 = _RAND_9[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_2046 = _RAND_10[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_2047 = _RAND_11[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_2048 = _RAND_12[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  _T_2086 = _RAND_13[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_14 = {1{`RANDOM}};
  _T_2098 = _RAND_14[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_15 = {1{`RANDOM}};
  _T_2100 = _RAND_15[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_16 = {2{`RANDOM}};
  _T_2101 = _RAND_16[35:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_17 = {1{`RANDOM}};
  _T_2134 = _RAND_17[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_18 = {1{`RANDOM}};
  _T_2145 = _RAND_18[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_19 = {1{`RANDOM}};
  _T_2146 = _RAND_19[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_20 = {1{`RANDOM}};
  _T_2147 = _RAND_20[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_21 = {1{`RANDOM}};
  _T_2148 = _RAND_21[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_22 = {2{`RANDOM}};
  _T_2149 = _RAND_22[35:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_23 = {1{`RANDOM}};
  _T_2174 = _RAND_23[8:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_24 = {1{`RANDOM}};
  _T_2184 = _RAND_24[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_25 = {1{`RANDOM}};
  _T_2203 = _RAND_25[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_26 = {1{`RANDOM}};
  _T_2249 = _RAND_26[31:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_27 = {1{`RANDOM}};
  _T_2264 = _RAND_27[15:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_28 = {1{`RANDOM}};
  _T_2273 = _RAND_28[4:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_1984 <= 5'h0;
    end else if (_T_1975) begin
      if (_T_1987) begin
        if (_T_1982) begin
          _T_1984 <= _T_20[7:3];
        end else begin
          _T_1984 <= 5'h0;
        end
      end else begin
        _T_1984 <= _T_1986;
      end
    end
    if (_T_2023) begin
      _T_1995 <= io_in_a_bits_opcode;
    end
    if (_T_2023) begin
      _T_1996 <= io_in_a_bits_param;
    end
    if (_T_2023) begin
      _T_1997 <= io_in_a_bits_size;
    end
    if (_T_2023) begin
      _T_1998 <= io_in_a_bits_source;
    end
    if (_T_2023) begin
      _T_1999 <= io_in_a_bits_address;
    end
    if (reset) begin
      _T_2032 <= 5'h0;
    end else if (_T_2024) begin
      if (_T_2035) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_2032 <= _T_2028[7:3];
        end else begin
          _T_2032 <= 5'h0;
        end
      end else begin
        _T_2032 <= _T_2034;
      end
    end
    if (_T_2076) begin
      _T_2043 <= io_in_d_bits_opcode;
    end
    if (_T_2076) begin
      _T_2044 <= io_in_d_bits_param;
    end
    if (_T_2076) begin
      _T_2045 <= io_in_d_bits_size;
    end
    if (_T_2076) begin
      _T_2046 <= io_in_d_bits_source;
    end
    if (_T_2076) begin
      _T_2047 <= io_in_d_bits_sink;
    end
    if (_T_2076) begin
      _T_2048 <= io_in_d_bits_denied;
    end
    if (reset) begin
      _T_2086 <= 5'h0;
    end else if (_T_2077) begin
      if (_T_2089) begin
        _T_2086 <= 5'h0;
      end else begin
        _T_2086 <= _T_2088;
      end
    end
    if (_T_2125) begin
      _T_2098 <= io_in_b_bits_param;
    end
    if (_T_2125) begin
      _T_2100 <= io_in_b_bits_source;
    end
    if (_T_2125) begin
      _T_2101 <= io_in_b_bits_address;
    end
    if (reset) begin
      _T_2134 <= 5'h0;
    end else if (_T_2126) begin
      if (_T_2137) begin
        if (io_in_c_bits_opcode[0]) begin
          _T_2134 <= _T_1520[7:3];
        end else begin
          _T_2134 <= 5'h0;
        end
      end else begin
        _T_2134 <= _T_2136;
      end
    end
    if (_T_2173) begin
      _T_2145 <= io_in_c_bits_opcode;
    end
    if (_T_2173) begin
      _T_2146 <= io_in_c_bits_param;
    end
    if (_T_2173) begin
      _T_2147 <= io_in_c_bits_size;
    end
    if (_T_2173) begin
      _T_2148 <= io_in_c_bits_source;
    end
    if (_T_2173) begin
      _T_2149 <= io_in_c_bits_address;
    end
    if (reset) begin
      _T_2174 <= 9'h0;
    end else begin
      _T_2174 <= _T_2248;
    end
    if (reset) begin
      _T_2184 <= 5'h0;
    end else if (_T_1975) begin
      if (_T_2187) begin
        if (_T_1982) begin
          _T_2184 <= _T_20[7:3];
        end else begin
          _T_2184 <= 5'h0;
        end
      end else begin
        _T_2184 <= _T_2186;
      end
    end
    if (reset) begin
      _T_2203 <= 5'h0;
    end else if (_T_2024) begin
      if (_T_2206) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_2203 <= _T_2028[7:3];
        end else begin
          _T_2203 <= 5'h0;
        end
      end else begin
        _T_2203 <= _T_2205;
      end
    end
    if (reset) begin
      _T_2249 <= 32'h0;
    end else if (_T_2263) begin
      _T_2249 <= 32'h0;
    end else begin
      _T_2249 <= _T_2260;
    end
    if (reset) begin
      _T_2264 <= 16'h0;
    end else begin
      _T_2264 <= _T_2312;
    end
    if (reset) begin
      _T_2273 <= 5'h0;
    end else if (_T_2024) begin
      if (_T_2276) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_2273 <= _T_2028[7:3];
        end else begin
          _T_2273 <= 5'h0;
        end
      end else begin
        _T_2273 <= _T_2275;
      end
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_37 & _T_203) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_37 & _T_203) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_37 & _T_225) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_37 & _T_225) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_37 & _T_228) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_37 & _T_228) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_37 & _T_232) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock smaller than a beat (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_37 & _T_232) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_37 & _T_235) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_37 & _T_235) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_37 & _T_239) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid grow param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_37 & _T_239) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_37 & _T_244) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_37 & _T_244) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_37 & _T_248) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock is corrupt (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_37 & _T_248) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_203) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_203) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_225) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_225) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_228) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_228) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_232) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm smaller than a beat (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_232) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_235) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_235) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_239) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid grow param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_239) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_369) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_369) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_244) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_244) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_248) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm is corrupt (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_248) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_71 & _T_458) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_71 & _T_458) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_71 & _T_228) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_71 & _T_228) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_71 & _T_235) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_71 & _T_235) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_71 & _T_468) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_71 & _T_468) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_71 & _T_472) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_71 & _T_472) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_71 & _T_248) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get is corrupt (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_71 & _T_248) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_561) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_561) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_228) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_228) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_235) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_235) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_468) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_468) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_472) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_472) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_93 & _T_561) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_93 & _T_561) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_93 & _T_228) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_93 & _T_228) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_93 & _T_235) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_93 & _T_235) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_93 & _T_468) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_93 & _T_468) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_93 & _T_676) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial contains invalid mask (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_93 & _T_676) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_103 & _T_753) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_103 & _T_753) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_103 & _T_228) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_103 & _T_228) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_103 & _T_235) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_103 & _T_235) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_103 & _T_763) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid opcode param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_103 & _T_763) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_103 & _T_472) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_103 & _T_472) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_113 & _T_753) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_113 & _T_753) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_113 & _T_228) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_113 & _T_228) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_113 & _T_235) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_113 & _T_235) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_113 & _T_854) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid opcode param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_113 & _T_854) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_113 & _T_472) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_113 & _T_472) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_123 & _T_946) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_123 & _T_946) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_123 & _T_228) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_123 & _T_228) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_123 & _T_235) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_123 & _T_235) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_123 & _T_956) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid opcode param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_123 & _T_956) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_123 & _T_472) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_123 & _T_472) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_123 & _T_248) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint is corrupt (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_123 & _T_248) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_968) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_968) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_135 & _T_986) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_135 & _T_986) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_135 & _T_990) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_135 & _T_990) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_135 & _T_994) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseeAck carries invalid param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_135 & _T_994) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_135 & _T_998) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is corrupt (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_135 & _T_998) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_135 & _T_1002) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is denied (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_135 & _T_1002) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_145 & _T_986) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_145 & _T_986) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_145 & _T_990) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_145 & _T_990) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_145 & _T_1017) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid cap param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_145 & _T_1017) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_145 & _T_1021) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries toN param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_145 & _T_1021) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_145 & _T_998) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is corrupt (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_145 & _T_998) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_155 & _T_986) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_155 & _T_986) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_155 & _T_990) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_155 & _T_990) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_155 & _T_1017) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid cap param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_155 & _T_1017) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_155 & _T_1021) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries toN param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_155 & _T_1021) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_155 & _T_1054) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied but not corrupt (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_155 & _T_1054) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_165 & _T_986) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_165 & _T_986) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_165 & _T_994) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_165 & _T_994) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_165 & _T_998) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is corrupt (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_165 & _T_998) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_171 & _T_986) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_171 & _T_986) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_171 & _T_994) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_171 & _T_994) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_171 & _T_1054) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied but not corrupt (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_171 & _T_1054) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_177 & _T_986) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_177 & _T_986) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_177 & _T_994) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_177 & _T_994) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_177 & _T_998) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is corrupt (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_177 & _T_998) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_b_valid & _T_1346) begin
          $fwrite(32'h80000002,"Assertion Failed: 'B' channel carries Probe type unsupported by client (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_b_valid & _T_1346) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_b_valid & _T_1349) begin
          $fwrite(32'h80000002,"Assertion Failed: 'B' channel Probe carries unmanaged address (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_b_valid & _T_1349) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_b_valid & _T_1352) begin
          $fwrite(32'h80000002,"Assertion Failed: 'B' channel Probe carries source that is not first source (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_b_valid & _T_1352) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_b_valid & _T_1355) begin
          $fwrite(32'h80000002,"Assertion Failed: 'B' channel Probe address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_b_valid & _T_1355) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_b_valid & _T_1359) begin
          $fwrite(32'h80000002,"Assertion Failed: 'B' channel Probe carries invalid cap param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_b_valid & _T_1359) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_183 & _T_1646) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAck carries unmanaged address (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_183 & _T_1646) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_183 & _T_1649) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAck carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_183 & _T_1649) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_183 & _T_1653) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAck smaller than a beat (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_183 & _T_1653) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_183 & _T_1656) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAck address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_183 & _T_1656) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_183 & _T_1660) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAck carries invalid report param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_183 & _T_1660) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_183 & _T_1664) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAck is corrupt (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_183 & _T_1664) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_195 & _T_1646) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAckData carries unmanaged address (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_195 & _T_1646) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_195 & _T_1649) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAckData carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_195 & _T_1649) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_195 & _T_1653) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAckData smaller than a beat (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_195 & _T_1653) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_195 & _T_1656) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAckData address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_195 & _T_1656) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_195 & _T_1660) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAckData carries invalid report param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_195 & _T_1660) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_205 & _T_1763) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel carries Release type unsupported by manager (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_205 & _T_1763) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_205 & _T_1785) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel carries Release from a client which does not support Probe (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_205 & _T_1785) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_205 & _T_1649) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel Release carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_205 & _T_1649) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_205 & _T_1653) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel Release smaller than a beat (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_205 & _T_1653) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_205 & _T_1656) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel Release address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_205 & _T_1656) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_205 & _T_1799) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel Release carries invalid shrink param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_205 & _T_1799) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_205 & _T_1664) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel Release is corrupt (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_205 & _T_1664) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_219 & _T_1763) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel carries ReleaseData type unsupported by manager (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_219 & _T_1763) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_219 & _T_1785) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel carries Release from a client which does not support Probe (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_219 & _T_1785) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_219 & _T_1649) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ReleaseData carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_219 & _T_1649) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_219 & _T_1653) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ReleaseData smaller than a beat (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_219 & _T_1653) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_219 & _T_1656) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ReleaseData address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_219 & _T_1656) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_219 & _T_1799) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ReleaseData carries invalid shrink param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_219 & _T_1799) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_231 & _T_1646) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAck carries unmanaged address (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_231 & _T_1646) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_231 & _T_1649) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAck carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_231 & _T_1649) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_231 & _T_1656) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAck address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_231 & _T_1656) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_231 & _T_1934) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAck carries invalid param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_231 & _T_1934) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_231 & _T_1664) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAck is corrupt (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_231 & _T_1664) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_241 & _T_1646) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAckData carries unmanaged address (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_241 & _T_1646) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_241 & _T_1649) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAckData carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_241 & _T_1649) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_241 & _T_1656) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAckData address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_241 & _T_1656) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_241 & _T_1934) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAckData carries invalid param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_241 & _T_1934) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_249 & _T_1646) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel HintAck carries unmanaged address (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_249 & _T_1646) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_249 & _T_1649) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel HintAck carries invalid source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_249 & _T_1649) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_249 & _T_1656) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel HintAck address not aligned to size (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_249 & _T_1656) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_249 & _T_1934) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel HintAck carries invalid param (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_249 & _T_1934) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_249 & _T_1664) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel HintAck is corrupt (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_249 & _T_1664) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2001 & _T_2005) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2001 & _T_2005) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2001 & _T_2009) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel param changed within multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2001 & _T_2009) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2001 & _T_2013) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel size changed within multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2001 & _T_2013) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2001 & _T_2017) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel source changed within multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2001 & _T_2017) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2001 & _T_2021) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2001 & _T_2021) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2050 & _T_2054) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2050 & _T_2054) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2050 & _T_2058) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel param changed within multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2050 & _T_2058) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2050 & _T_2062) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2050 & _T_2062) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2050 & _T_2066) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel source changed within multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2050 & _T_2066) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2050 & _T_2070) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel sink changed with multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2050 & _T_2070) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2050 & _T_2074) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2050 & _T_2074) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2103 & _T_2111) begin
          $fwrite(32'h80000002,"Assertion Failed: 'B' channel param changed within multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2103 & _T_2111) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2103 & _T_2119) begin
          $fwrite(32'h80000002,"Assertion Failed: 'B' channel source changed within multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2103 & _T_2119) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2103 & _T_2123) begin
          $fwrite(32'h80000002,"Assertion Failed: 'B' channel addresss changed with multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2103 & _T_2123) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2151 & _T_2155) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel opcode changed within multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2151 & _T_2155) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2151 & _T_2159) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel param changed within multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2151 & _T_2159) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2151 & _T_2163) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel size changed within multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2151 & _T_2163) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2151 & _T_2167) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel source changed within multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2151 & _T_2167) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2151 & _T_2171) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel address changed with multibeat operation (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2151 & _T_2171) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2216 & _T_2224) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2216 & _T_2224) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2231 & _T_2238) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2231 & _T_2238) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2245) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' and 'D' concurrent, despite minlatency 1 (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2245) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2258) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at HellaCache.scala:254:21)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2258) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2291 & _T_2298) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel re-used a sink ID (connected at HellaCache.scala:254:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2291 & _T_2298) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2300 & _T_2309) begin
          $fwrite(32'h80000002,"Assertion Failed: 'E' channel acknowledged for nothing inflight (connected at HellaCache.scala:254:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2300 & _T_2309) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
