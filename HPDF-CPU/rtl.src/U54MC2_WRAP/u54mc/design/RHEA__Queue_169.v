//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__Queue_169(
  input          rf_reset,
  input          clock,
  input          reset,
  output         io_enq_ready,
  input          io_enq_valid,
  input  [2:0]   io_enq_bits_opcode,
  input  [2:0]   io_enq_bits_param,
  input  [3:0]   io_enq_bits_size,
  input  [3:0]   io_enq_bits_source,
  input  [36:0]  io_enq_bits_address,
  input          io_enq_bits_user_amba_prot_bufferable,
  input          io_enq_bits_user_amba_prot_modifiable,
  input          io_enq_bits_user_amba_prot_readalloc,
  input          io_enq_bits_user_amba_prot_writealloc,
  input          io_enq_bits_user_amba_prot_privileged,
  input  [15:0]  io_enq_bits_mask,
  input  [127:0] io_enq_bits_data,
  input          io_deq_ready,
  output         io_deq_valid,
  output [2:0]   io_deq_bits_opcode,
  output [2:0]   io_deq_bits_param,
  output [3:0]   io_deq_bits_size,
  output [3:0]   io_deq_bits_source,
  output [36:0]  io_deq_bits_address,
  output         io_deq_bits_user_amba_prot_bufferable,
  output         io_deq_bits_user_amba_prot_modifiable,
  output         io_deq_bits_user_amba_prot_readalloc,
  output         io_deq_bits_user_amba_prot_writealloc,
  output         io_deq_bits_user_amba_prot_privileged,
  output [15:0]  io_deq_bits_mask,
  output [127:0] io_deq_bits_data
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [63:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [127:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [63:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [127:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
`endif // RANDOMIZE_REG_INIT
  reg [2:0] ram_0_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_0_param; // @[Decoupled.scala 218:16]
  reg [3:0] ram_0_size; // @[Decoupled.scala 218:16]
  reg [3:0] ram_0_source; // @[Decoupled.scala 218:16]
  reg [36:0] ram_0_address; // @[Decoupled.scala 218:16]
  reg  ram_0_user_amba_prot_bufferable; // @[Decoupled.scala 218:16]
  reg  ram_0_user_amba_prot_modifiable; // @[Decoupled.scala 218:16]
  reg  ram_0_user_amba_prot_readalloc; // @[Decoupled.scala 218:16]
  reg  ram_0_user_amba_prot_writealloc; // @[Decoupled.scala 218:16]
  reg  ram_0_user_amba_prot_privileged; // @[Decoupled.scala 218:16]
  reg [15:0] ram_0_mask; // @[Decoupled.scala 218:16]
  reg [127:0] ram_0_data; // @[Decoupled.scala 218:16]
  reg [2:0] ram_1_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_1_param; // @[Decoupled.scala 218:16]
  reg [3:0] ram_1_size; // @[Decoupled.scala 218:16]
  reg [3:0] ram_1_source; // @[Decoupled.scala 218:16]
  reg [36:0] ram_1_address; // @[Decoupled.scala 218:16]
  reg  ram_1_user_amba_prot_bufferable; // @[Decoupled.scala 218:16]
  reg  ram_1_user_amba_prot_modifiable; // @[Decoupled.scala 218:16]
  reg  ram_1_user_amba_prot_readalloc; // @[Decoupled.scala 218:16]
  reg  ram_1_user_amba_prot_writealloc; // @[Decoupled.scala 218:16]
  reg  ram_1_user_amba_prot_privileged; // @[Decoupled.scala 218:16]
  reg [15:0] ram_1_mask; // @[Decoupled.scala 218:16]
  reg [127:0] ram_1_data; // @[Decoupled.scala 218:16]
  reg  value_1; // @[Counter.scala 60:40]
  wire [2:0] ramIntf_io_deq_bits_MPORT_data_opcode = value_1 ? ram_1_opcode : ram_0_opcode; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] ramIntf_io_deq_bits_MPORT_data_param = value_1 ? ram_1_param : ram_0_param; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [3:0] ramIntf_io_deq_bits_MPORT_data_size = value_1 ? ram_1_size : ram_0_size; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [3:0] ramIntf_io_deq_bits_MPORT_data_source = value_1 ? ram_1_source : ram_0_source; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [36:0] ramIntf_io_deq_bits_MPORT_data_address = value_1 ? ram_1_address : ram_0_address; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire  ramIntf_io_deq_bits_MPORT_data_user_amba_prot_bufferable = value_1 ? ram_1_user_amba_prot_bufferable :
    ram_0_user_amba_prot_bufferable; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire  ramIntf_io_deq_bits_MPORT_data_user_amba_prot_modifiable = value_1 ? ram_1_user_amba_prot_modifiable :
    ram_0_user_amba_prot_modifiable; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire  ramIntf_io_deq_bits_MPORT_data_user_amba_prot_readalloc = value_1 ? ram_1_user_amba_prot_readalloc :
    ram_0_user_amba_prot_readalloc; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire  ramIntf_io_deq_bits_MPORT_data_user_amba_prot_writealloc = value_1 ? ram_1_user_amba_prot_writealloc :
    ram_0_user_amba_prot_writealloc; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire  ramIntf_io_deq_bits_MPORT_data_user_amba_prot_privileged = value_1 ? ram_1_user_amba_prot_privileged :
    ram_0_user_amba_prot_privileged; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [15:0] ramIntf_io_deq_bits_MPORT_data_mask = value_1 ? ram_1_mask : ram_0_mask; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [127:0] ramIntf_io_deq_bits_MPORT_data_data = value_1 ? ram_1_data : ram_0_data; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  reg  value; // @[Counter.scala 60:40]
  wire  ptr_match = value == value_1; // @[Decoupled.scala 223:33]
  reg  maybe_full; // @[Decoupled.scala 221:27]
  wire  empty = ptr_match & ~maybe_full; // @[Decoupled.scala 224:25]
  wire  _do_enq_T = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  wire  _GEN_143 = io_deq_ready ? 1'h0 : _do_enq_T; // @[Decoupled.scala 249:27 Decoupled.scala 249:36]
  wire  do_enq = empty ? _GEN_143 : _do_enq_T; // @[Decoupled.scala 246:18]
  wire  full = ptr_match & maybe_full; // @[Decoupled.scala 225:24]
  wire  _do_deq_T = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  wire  do_deq = empty ? 1'h0 : _do_deq_T; // @[Decoupled.scala 246:18 Decoupled.scala 248:14]
  assign io_enq_ready = ~full; // @[Decoupled.scala 241:19]
  assign io_deq_valid = io_enq_valid | ~empty; // @[Decoupled.scala 245:25 Decoupled.scala 245:40 Decoupled.scala 240:16]
  assign io_deq_bits_opcode = empty ? io_enq_bits_opcode : ramIntf_io_deq_bits_MPORT_data_opcode; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_param = empty ? io_enq_bits_param : ramIntf_io_deq_bits_MPORT_data_param; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_size = empty ? io_enq_bits_size : ramIntf_io_deq_bits_MPORT_data_size; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_source = empty ? io_enq_bits_source : ramIntf_io_deq_bits_MPORT_data_source; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_address = empty ? io_enq_bits_address : ramIntf_io_deq_bits_MPORT_data_address; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_user_amba_prot_bufferable = empty ? io_enq_bits_user_amba_prot_bufferable :
    ramIntf_io_deq_bits_MPORT_data_user_amba_prot_bufferable; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_user_amba_prot_modifiable = empty ? io_enq_bits_user_amba_prot_modifiable :
    ramIntf_io_deq_bits_MPORT_data_user_amba_prot_modifiable; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_user_amba_prot_readalloc = empty ? io_enq_bits_user_amba_prot_readalloc :
    ramIntf_io_deq_bits_MPORT_data_user_amba_prot_readalloc; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_user_amba_prot_writealloc = empty ? io_enq_bits_user_amba_prot_writealloc :
    ramIntf_io_deq_bits_MPORT_data_user_amba_prot_writealloc; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_user_amba_prot_privileged = empty ? io_enq_bits_user_amba_prot_privileged :
    ramIntf_io_deq_bits_MPORT_data_user_amba_prot_privileged; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_mask = empty ? io_enq_bits_mask : ramIntf_io_deq_bits_MPORT_data_mask; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_data = empty ? io_enq_bits_data : ramIntf_io_deq_bits_MPORT_data_data; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_opcode <= 3'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_param <= 3'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_size <= 4'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_size <= io_enq_bits_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_source <= 4'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_address <= 37'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_user_amba_prot_bufferable <= 1'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_user_amba_prot_bufferable <= io_enq_bits_user_amba_prot_bufferable;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_user_amba_prot_modifiable <= 1'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_user_amba_prot_modifiable <= io_enq_bits_user_amba_prot_modifiable;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_user_amba_prot_readalloc <= 1'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_user_amba_prot_readalloc <= io_enq_bits_user_amba_prot_readalloc;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_user_amba_prot_writealloc <= 1'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_user_amba_prot_writealloc <= io_enq_bits_user_amba_prot_writealloc;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_user_amba_prot_privileged <= 1'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_user_amba_prot_privileged <= io_enq_bits_user_amba_prot_privileged;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_mask <= 16'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_mask <= io_enq_bits_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_data <= 128'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_opcode <= 3'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_param <= 3'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_size <= 4'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_size <= io_enq_bits_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_source <= 4'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_address <= 37'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_user_amba_prot_bufferable <= 1'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_user_amba_prot_bufferable <= io_enq_bits_user_amba_prot_bufferable;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_user_amba_prot_modifiable <= 1'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_user_amba_prot_modifiable <= io_enq_bits_user_amba_prot_modifiable;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_user_amba_prot_readalloc <= 1'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_user_amba_prot_readalloc <= io_enq_bits_user_amba_prot_readalloc;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_user_amba_prot_writealloc <= 1'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_user_amba_prot_writealloc <= io_enq_bits_user_amba_prot_writealloc;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_user_amba_prot_privileged <= 1'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_user_amba_prot_privileged <= io_enq_bits_user_amba_prot_privileged;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_mask <= 16'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_mask <= io_enq_bits_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_data <= 128'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      value_1 <= 1'h0;
    end else if (reset) begin
      value_1 <= 1'h0;
    end else if (do_deq) begin
      value_1 <= value_1 + 1'h1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      value <= 1'h0;
    end else if (reset) begin
      value <= 1'h0;
    end else if (do_enq) begin
      value <= value + 1'h1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      maybe_full <= 1'h0;
    end else if (reset) begin
      maybe_full <= 1'h0;
    end else if (do_enq != do_deq) begin
      if (empty) begin
        if (io_deq_ready) begin
          maybe_full <= 1'h0;
        end else begin
          maybe_full <= _do_enq_T;
        end
      end else begin
        maybe_full <= _do_enq_T;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ram_0_opcode = _RAND_0[2:0];
  _RAND_1 = {1{`RANDOM}};
  ram_0_param = _RAND_1[2:0];
  _RAND_2 = {1{`RANDOM}};
  ram_0_size = _RAND_2[3:0];
  _RAND_3 = {1{`RANDOM}};
  ram_0_source = _RAND_3[3:0];
  _RAND_4 = {2{`RANDOM}};
  ram_0_address = _RAND_4[36:0];
  _RAND_5 = {1{`RANDOM}};
  ram_0_user_amba_prot_bufferable = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  ram_0_user_amba_prot_modifiable = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  ram_0_user_amba_prot_readalloc = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  ram_0_user_amba_prot_writealloc = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  ram_0_user_amba_prot_privileged = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  ram_0_mask = _RAND_10[15:0];
  _RAND_11 = {4{`RANDOM}};
  ram_0_data = _RAND_11[127:0];
  _RAND_12 = {1{`RANDOM}};
  ram_1_opcode = _RAND_12[2:0];
  _RAND_13 = {1{`RANDOM}};
  ram_1_param = _RAND_13[2:0];
  _RAND_14 = {1{`RANDOM}};
  ram_1_size = _RAND_14[3:0];
  _RAND_15 = {1{`RANDOM}};
  ram_1_source = _RAND_15[3:0];
  _RAND_16 = {2{`RANDOM}};
  ram_1_address = _RAND_16[36:0];
  _RAND_17 = {1{`RANDOM}};
  ram_1_user_amba_prot_bufferable = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  ram_1_user_amba_prot_modifiable = _RAND_18[0:0];
  _RAND_19 = {1{`RANDOM}};
  ram_1_user_amba_prot_readalloc = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  ram_1_user_amba_prot_writealloc = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  ram_1_user_amba_prot_privileged = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  ram_1_mask = _RAND_22[15:0];
  _RAND_23 = {4{`RANDOM}};
  ram_1_data = _RAND_23[127:0];
  _RAND_24 = {1{`RANDOM}};
  value_1 = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  value = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  maybe_full = _RAND_26[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ram_0_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_0_param = 3'h0;
  end
  if (rf_reset) begin
    ram_0_size = 4'h0;
  end
  if (rf_reset) begin
    ram_0_source = 4'h0;
  end
  if (rf_reset) begin
    ram_0_address = 37'h0;
  end
  if (rf_reset) begin
    ram_0_user_amba_prot_bufferable = 1'h0;
  end
  if (rf_reset) begin
    ram_0_user_amba_prot_modifiable = 1'h0;
  end
  if (rf_reset) begin
    ram_0_user_amba_prot_readalloc = 1'h0;
  end
  if (rf_reset) begin
    ram_0_user_amba_prot_writealloc = 1'h0;
  end
  if (rf_reset) begin
    ram_0_user_amba_prot_privileged = 1'h0;
  end
  if (rf_reset) begin
    ram_0_mask = 16'h0;
  end
  if (rf_reset) begin
    ram_0_data = 128'h0;
  end
  if (rf_reset) begin
    ram_1_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_1_param = 3'h0;
  end
  if (rf_reset) begin
    ram_1_size = 4'h0;
  end
  if (rf_reset) begin
    ram_1_source = 4'h0;
  end
  if (rf_reset) begin
    ram_1_address = 37'h0;
  end
  if (rf_reset) begin
    ram_1_user_amba_prot_bufferable = 1'h0;
  end
  if (rf_reset) begin
    ram_1_user_amba_prot_modifiable = 1'h0;
  end
  if (rf_reset) begin
    ram_1_user_amba_prot_readalloc = 1'h0;
  end
  if (rf_reset) begin
    ram_1_user_amba_prot_writealloc = 1'h0;
  end
  if (rf_reset) begin
    ram_1_user_amba_prot_privileged = 1'h0;
  end
  if (rf_reset) begin
    ram_1_mask = 16'h0;
  end
  if (rf_reset) begin
    ram_1_data = 128'h0;
  end
  if (rf_reset) begin
    value_1 = 1'h0;
  end
  if (rf_reset) begin
    value = 1'h0;
  end
  if (rf_reset) begin
    maybe_full = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
