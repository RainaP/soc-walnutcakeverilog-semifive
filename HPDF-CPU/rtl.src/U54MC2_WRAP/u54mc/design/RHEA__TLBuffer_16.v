//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLBuffer_16(
  input          rf_reset,
  input          clock,
  input          reset,
  output         auto_in_3_a_ready,
  input          auto_in_3_a_valid,
  input  [2:0]   auto_in_3_a_bits_opcode,
  input  [2:0]   auto_in_3_a_bits_param,
  input  [2:0]   auto_in_3_a_bits_size,
  input  [6:0]   auto_in_3_a_bits_source,
  input  [35:0]  auto_in_3_a_bits_address,
  input  [15:0]  auto_in_3_a_bits_mask,
  input  [127:0] auto_in_3_a_bits_data,
  input          auto_in_3_a_bits_corrupt,
  input          auto_in_3_b_ready,
  output         auto_in_3_b_valid,
  output [2:0]   auto_in_3_b_bits_opcode,
  output [1:0]   auto_in_3_b_bits_param,
  output [2:0]   auto_in_3_b_bits_size,
  output [6:0]   auto_in_3_b_bits_source,
  output [35:0]  auto_in_3_b_bits_address,
  output [15:0]  auto_in_3_b_bits_mask,
  output [127:0] auto_in_3_b_bits_data,
  output         auto_in_3_b_bits_corrupt,
  output         auto_in_3_c_ready,
  input          auto_in_3_c_valid,
  input  [2:0]   auto_in_3_c_bits_opcode,
  input  [2:0]   auto_in_3_c_bits_param,
  input  [2:0]   auto_in_3_c_bits_size,
  input  [6:0]   auto_in_3_c_bits_source,
  input  [35:0]  auto_in_3_c_bits_address,
  input  [127:0] auto_in_3_c_bits_data,
  input          auto_in_3_c_bits_corrupt,
  input          auto_in_3_d_ready,
  output         auto_in_3_d_valid,
  output [2:0]   auto_in_3_d_bits_opcode,
  output [1:0]   auto_in_3_d_bits_param,
  output [2:0]   auto_in_3_d_bits_size,
  output [6:0]   auto_in_3_d_bits_source,
  output [2:0]   auto_in_3_d_bits_sink,
  output         auto_in_3_d_bits_denied,
  output [127:0] auto_in_3_d_bits_data,
  output         auto_in_3_d_bits_corrupt,
  output         auto_in_3_e_ready,
  input          auto_in_3_e_valid,
  input  [2:0]   auto_in_3_e_bits_sink,
  output         auto_in_2_a_ready,
  input          auto_in_2_a_valid,
  input  [2:0]   auto_in_2_a_bits_opcode,
  input  [2:0]   auto_in_2_a_bits_param,
  input  [2:0]   auto_in_2_a_bits_size,
  input  [6:0]   auto_in_2_a_bits_source,
  input  [35:0]  auto_in_2_a_bits_address,
  input  [15:0]  auto_in_2_a_bits_mask,
  input  [127:0] auto_in_2_a_bits_data,
  input          auto_in_2_a_bits_corrupt,
  input          auto_in_2_b_ready,
  output         auto_in_2_b_valid,
  output [2:0]   auto_in_2_b_bits_opcode,
  output [1:0]   auto_in_2_b_bits_param,
  output [2:0]   auto_in_2_b_bits_size,
  output [6:0]   auto_in_2_b_bits_source,
  output [35:0]  auto_in_2_b_bits_address,
  output [15:0]  auto_in_2_b_bits_mask,
  output [127:0] auto_in_2_b_bits_data,
  output         auto_in_2_b_bits_corrupt,
  output         auto_in_2_c_ready,
  input          auto_in_2_c_valid,
  input  [2:0]   auto_in_2_c_bits_opcode,
  input  [2:0]   auto_in_2_c_bits_param,
  input  [2:0]   auto_in_2_c_bits_size,
  input  [6:0]   auto_in_2_c_bits_source,
  input  [35:0]  auto_in_2_c_bits_address,
  input  [127:0] auto_in_2_c_bits_data,
  input          auto_in_2_c_bits_corrupt,
  input          auto_in_2_d_ready,
  output         auto_in_2_d_valid,
  output [2:0]   auto_in_2_d_bits_opcode,
  output [1:0]   auto_in_2_d_bits_param,
  output [2:0]   auto_in_2_d_bits_size,
  output [6:0]   auto_in_2_d_bits_source,
  output [2:0]   auto_in_2_d_bits_sink,
  output         auto_in_2_d_bits_denied,
  output [127:0] auto_in_2_d_bits_data,
  output         auto_in_2_d_bits_corrupt,
  output         auto_in_2_e_ready,
  input          auto_in_2_e_valid,
  input  [2:0]   auto_in_2_e_bits_sink,
  output         auto_in_1_a_ready,
  input          auto_in_1_a_valid,
  input  [2:0]   auto_in_1_a_bits_opcode,
  input  [2:0]   auto_in_1_a_bits_param,
  input  [2:0]   auto_in_1_a_bits_size,
  input  [6:0]   auto_in_1_a_bits_source,
  input  [35:0]  auto_in_1_a_bits_address,
  input  [15:0]  auto_in_1_a_bits_mask,
  input  [127:0] auto_in_1_a_bits_data,
  input          auto_in_1_a_bits_corrupt,
  input          auto_in_1_b_ready,
  output         auto_in_1_b_valid,
  output [2:0]   auto_in_1_b_bits_opcode,
  output [1:0]   auto_in_1_b_bits_param,
  output [2:0]   auto_in_1_b_bits_size,
  output [6:0]   auto_in_1_b_bits_source,
  output [35:0]  auto_in_1_b_bits_address,
  output [15:0]  auto_in_1_b_bits_mask,
  output [127:0] auto_in_1_b_bits_data,
  output         auto_in_1_b_bits_corrupt,
  output         auto_in_1_c_ready,
  input          auto_in_1_c_valid,
  input  [2:0]   auto_in_1_c_bits_opcode,
  input  [2:0]   auto_in_1_c_bits_param,
  input  [2:0]   auto_in_1_c_bits_size,
  input  [6:0]   auto_in_1_c_bits_source,
  input  [35:0]  auto_in_1_c_bits_address,
  input  [127:0] auto_in_1_c_bits_data,
  input          auto_in_1_c_bits_corrupt,
  input          auto_in_1_d_ready,
  output         auto_in_1_d_valid,
  output [2:0]   auto_in_1_d_bits_opcode,
  output [1:0]   auto_in_1_d_bits_param,
  output [2:0]   auto_in_1_d_bits_size,
  output [6:0]   auto_in_1_d_bits_source,
  output [2:0]   auto_in_1_d_bits_sink,
  output         auto_in_1_d_bits_denied,
  output [127:0] auto_in_1_d_bits_data,
  output         auto_in_1_d_bits_corrupt,
  output         auto_in_1_e_ready,
  input          auto_in_1_e_valid,
  input  [2:0]   auto_in_1_e_bits_sink,
  output         auto_in_0_a_ready,
  input          auto_in_0_a_valid,
  input  [2:0]   auto_in_0_a_bits_opcode,
  input  [2:0]   auto_in_0_a_bits_param,
  input  [2:0]   auto_in_0_a_bits_size,
  input  [6:0]   auto_in_0_a_bits_source,
  input  [35:0]  auto_in_0_a_bits_address,
  input  [15:0]  auto_in_0_a_bits_mask,
  input  [127:0] auto_in_0_a_bits_data,
  input          auto_in_0_a_bits_corrupt,
  input          auto_in_0_b_ready,
  output         auto_in_0_b_valid,
  output [2:0]   auto_in_0_b_bits_opcode,
  output [1:0]   auto_in_0_b_bits_param,
  output [2:0]   auto_in_0_b_bits_size,
  output [6:0]   auto_in_0_b_bits_source,
  output [35:0]  auto_in_0_b_bits_address,
  output [15:0]  auto_in_0_b_bits_mask,
  output [127:0] auto_in_0_b_bits_data,
  output         auto_in_0_b_bits_corrupt,
  output         auto_in_0_c_ready,
  input          auto_in_0_c_valid,
  input  [2:0]   auto_in_0_c_bits_opcode,
  input  [2:0]   auto_in_0_c_bits_param,
  input  [2:0]   auto_in_0_c_bits_size,
  input  [6:0]   auto_in_0_c_bits_source,
  input  [35:0]  auto_in_0_c_bits_address,
  input  [127:0] auto_in_0_c_bits_data,
  input          auto_in_0_c_bits_corrupt,
  input          auto_in_0_d_ready,
  output         auto_in_0_d_valid,
  output [2:0]   auto_in_0_d_bits_opcode,
  output [1:0]   auto_in_0_d_bits_param,
  output [2:0]   auto_in_0_d_bits_size,
  output [6:0]   auto_in_0_d_bits_source,
  output [2:0]   auto_in_0_d_bits_sink,
  output         auto_in_0_d_bits_denied,
  output [127:0] auto_in_0_d_bits_data,
  output         auto_in_0_d_bits_corrupt,
  output         auto_in_0_e_ready,
  input          auto_in_0_e_valid,
  input  [2:0]   auto_in_0_e_bits_sink,
  input          auto_out_3_a_ready,
  output         auto_out_3_a_valid,
  output [2:0]   auto_out_3_a_bits_opcode,
  output [2:0]   auto_out_3_a_bits_param,
  output [2:0]   auto_out_3_a_bits_size,
  output [6:0]   auto_out_3_a_bits_source,
  output [35:0]  auto_out_3_a_bits_address,
  output [15:0]  auto_out_3_a_bits_mask,
  output [127:0] auto_out_3_a_bits_data,
  output         auto_out_3_a_bits_corrupt,
  output         auto_out_3_b_ready,
  input          auto_out_3_b_valid,
  input  [2:0]   auto_out_3_b_bits_opcode,
  input  [1:0]   auto_out_3_b_bits_param,
  input  [2:0]   auto_out_3_b_bits_size,
  input  [6:0]   auto_out_3_b_bits_source,
  input  [35:0]  auto_out_3_b_bits_address,
  input  [15:0]  auto_out_3_b_bits_mask,
  input  [127:0] auto_out_3_b_bits_data,
  input          auto_out_3_b_bits_corrupt,
  input          auto_out_3_c_ready,
  output         auto_out_3_c_valid,
  output [2:0]   auto_out_3_c_bits_opcode,
  output [2:0]   auto_out_3_c_bits_param,
  output [2:0]   auto_out_3_c_bits_size,
  output [6:0]   auto_out_3_c_bits_source,
  output [35:0]  auto_out_3_c_bits_address,
  output [127:0] auto_out_3_c_bits_data,
  output         auto_out_3_c_bits_corrupt,
  output         auto_out_3_d_ready,
  input          auto_out_3_d_valid,
  input  [2:0]   auto_out_3_d_bits_opcode,
  input  [1:0]   auto_out_3_d_bits_param,
  input  [2:0]   auto_out_3_d_bits_size,
  input  [6:0]   auto_out_3_d_bits_source,
  input  [2:0]   auto_out_3_d_bits_sink,
  input          auto_out_3_d_bits_denied,
  input  [127:0] auto_out_3_d_bits_data,
  input          auto_out_3_d_bits_corrupt,
  input          auto_out_3_e_ready,
  output         auto_out_3_e_valid,
  output [2:0]   auto_out_3_e_bits_sink,
  input          auto_out_2_a_ready,
  output         auto_out_2_a_valid,
  output [2:0]   auto_out_2_a_bits_opcode,
  output [2:0]   auto_out_2_a_bits_param,
  output [2:0]   auto_out_2_a_bits_size,
  output [6:0]   auto_out_2_a_bits_source,
  output [35:0]  auto_out_2_a_bits_address,
  output [15:0]  auto_out_2_a_bits_mask,
  output [127:0] auto_out_2_a_bits_data,
  output         auto_out_2_a_bits_corrupt,
  output         auto_out_2_b_ready,
  input          auto_out_2_b_valid,
  input  [2:0]   auto_out_2_b_bits_opcode,
  input  [1:0]   auto_out_2_b_bits_param,
  input  [2:0]   auto_out_2_b_bits_size,
  input  [6:0]   auto_out_2_b_bits_source,
  input  [35:0]  auto_out_2_b_bits_address,
  input  [15:0]  auto_out_2_b_bits_mask,
  input  [127:0] auto_out_2_b_bits_data,
  input          auto_out_2_b_bits_corrupt,
  input          auto_out_2_c_ready,
  output         auto_out_2_c_valid,
  output [2:0]   auto_out_2_c_bits_opcode,
  output [2:0]   auto_out_2_c_bits_param,
  output [2:0]   auto_out_2_c_bits_size,
  output [6:0]   auto_out_2_c_bits_source,
  output [35:0]  auto_out_2_c_bits_address,
  output [127:0] auto_out_2_c_bits_data,
  output         auto_out_2_c_bits_corrupt,
  output         auto_out_2_d_ready,
  input          auto_out_2_d_valid,
  input  [2:0]   auto_out_2_d_bits_opcode,
  input  [1:0]   auto_out_2_d_bits_param,
  input  [2:0]   auto_out_2_d_bits_size,
  input  [6:0]   auto_out_2_d_bits_source,
  input  [2:0]   auto_out_2_d_bits_sink,
  input          auto_out_2_d_bits_denied,
  input  [127:0] auto_out_2_d_bits_data,
  input          auto_out_2_d_bits_corrupt,
  input          auto_out_2_e_ready,
  output         auto_out_2_e_valid,
  output [2:0]   auto_out_2_e_bits_sink,
  input          auto_out_1_a_ready,
  output         auto_out_1_a_valid,
  output [2:0]   auto_out_1_a_bits_opcode,
  output [2:0]   auto_out_1_a_bits_param,
  output [2:0]   auto_out_1_a_bits_size,
  output [6:0]   auto_out_1_a_bits_source,
  output [35:0]  auto_out_1_a_bits_address,
  output [15:0]  auto_out_1_a_bits_mask,
  output [127:0] auto_out_1_a_bits_data,
  output         auto_out_1_a_bits_corrupt,
  output         auto_out_1_b_ready,
  input          auto_out_1_b_valid,
  input  [2:0]   auto_out_1_b_bits_opcode,
  input  [1:0]   auto_out_1_b_bits_param,
  input  [2:0]   auto_out_1_b_bits_size,
  input  [6:0]   auto_out_1_b_bits_source,
  input  [35:0]  auto_out_1_b_bits_address,
  input  [15:0]  auto_out_1_b_bits_mask,
  input  [127:0] auto_out_1_b_bits_data,
  input          auto_out_1_b_bits_corrupt,
  input          auto_out_1_c_ready,
  output         auto_out_1_c_valid,
  output [2:0]   auto_out_1_c_bits_opcode,
  output [2:0]   auto_out_1_c_bits_param,
  output [2:0]   auto_out_1_c_bits_size,
  output [6:0]   auto_out_1_c_bits_source,
  output [35:0]  auto_out_1_c_bits_address,
  output [127:0] auto_out_1_c_bits_data,
  output         auto_out_1_c_bits_corrupt,
  output         auto_out_1_d_ready,
  input          auto_out_1_d_valid,
  input  [2:0]   auto_out_1_d_bits_opcode,
  input  [1:0]   auto_out_1_d_bits_param,
  input  [2:0]   auto_out_1_d_bits_size,
  input  [6:0]   auto_out_1_d_bits_source,
  input  [2:0]   auto_out_1_d_bits_sink,
  input          auto_out_1_d_bits_denied,
  input  [127:0] auto_out_1_d_bits_data,
  input          auto_out_1_d_bits_corrupt,
  input          auto_out_1_e_ready,
  output         auto_out_1_e_valid,
  output [2:0]   auto_out_1_e_bits_sink,
  input          auto_out_0_a_ready,
  output         auto_out_0_a_valid,
  output [2:0]   auto_out_0_a_bits_opcode,
  output [2:0]   auto_out_0_a_bits_param,
  output [2:0]   auto_out_0_a_bits_size,
  output [6:0]   auto_out_0_a_bits_source,
  output [35:0]  auto_out_0_a_bits_address,
  output [15:0]  auto_out_0_a_bits_mask,
  output [127:0] auto_out_0_a_bits_data,
  output         auto_out_0_a_bits_corrupt,
  output         auto_out_0_b_ready,
  input          auto_out_0_b_valid,
  input  [2:0]   auto_out_0_b_bits_opcode,
  input  [1:0]   auto_out_0_b_bits_param,
  input  [2:0]   auto_out_0_b_bits_size,
  input  [6:0]   auto_out_0_b_bits_source,
  input  [35:0]  auto_out_0_b_bits_address,
  input  [15:0]  auto_out_0_b_bits_mask,
  input  [127:0] auto_out_0_b_bits_data,
  input          auto_out_0_b_bits_corrupt,
  input          auto_out_0_c_ready,
  output         auto_out_0_c_valid,
  output [2:0]   auto_out_0_c_bits_opcode,
  output [2:0]   auto_out_0_c_bits_param,
  output [2:0]   auto_out_0_c_bits_size,
  output [6:0]   auto_out_0_c_bits_source,
  output [35:0]  auto_out_0_c_bits_address,
  output [127:0] auto_out_0_c_bits_data,
  output         auto_out_0_c_bits_corrupt,
  output         auto_out_0_d_ready,
  input          auto_out_0_d_valid,
  input  [2:0]   auto_out_0_d_bits_opcode,
  input  [1:0]   auto_out_0_d_bits_param,
  input  [2:0]   auto_out_0_d_bits_size,
  input  [6:0]   auto_out_0_d_bits_source,
  input  [2:0]   auto_out_0_d_bits_sink,
  input          auto_out_0_d_bits_denied,
  input  [127:0] auto_out_0_d_bits_data,
  input          auto_out_0_d_bits_corrupt,
  input          auto_out_0_e_ready,
  output         auto_out_0_e_valid,
  output [2:0]   auto_out_0_e_bits_sink
);
  wire  bundleOut_0_a_q_rf_reset; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_clock; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_reset; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_0_a_q_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_0_a_q_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_0_a_q_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleOut_0_a_q_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [35:0] bundleOut_0_a_q_io_enq_bits_address; // @[Decoupled.scala 296:21]
  wire [15:0] bundleOut_0_a_q_io_enq_bits_mask; // @[Decoupled.scala 296:21]
  wire [127:0] bundleOut_0_a_q_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_0_a_q_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_0_a_q_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_0_a_q_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleOut_0_a_q_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [35:0] bundleOut_0_a_q_io_deq_bits_address; // @[Decoupled.scala 296:21]
  wire [15:0] bundleOut_0_a_q_io_deq_bits_mask; // @[Decoupled.scala 296:21]
  wire [127:0] bundleOut_0_a_q_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_rf_reset; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_clock; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_reset; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_0_d_q_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] bundleIn_0_d_q_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_0_d_q_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleIn_0_d_q_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_0_d_q_io_enq_bits_sink; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_enq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] bundleIn_0_d_q_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_0_d_q_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] bundleIn_0_d_q_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_0_d_q_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleIn_0_d_q_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_0_d_q_io_deq_bits_sink; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_deq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] bundleIn_0_d_q_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleOut_1_a_q_rf_reset; // @[Decoupled.scala 296:21]
  wire  bundleOut_1_a_q_clock; // @[Decoupled.scala 296:21]
  wire  bundleOut_1_a_q_reset; // @[Decoupled.scala 296:21]
  wire  bundleOut_1_a_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  bundleOut_1_a_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_1_a_q_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_1_a_q_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_1_a_q_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleOut_1_a_q_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [35:0] bundleOut_1_a_q_io_enq_bits_address; // @[Decoupled.scala 296:21]
  wire [15:0] bundleOut_1_a_q_io_enq_bits_mask; // @[Decoupled.scala 296:21]
  wire [127:0] bundleOut_1_a_q_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleOut_1_a_q_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleOut_1_a_q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  bundleOut_1_a_q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_1_a_q_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_1_a_q_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_1_a_q_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleOut_1_a_q_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [35:0] bundleOut_1_a_q_io_deq_bits_address; // @[Decoupled.scala 296:21]
  wire [15:0] bundleOut_1_a_q_io_deq_bits_mask; // @[Decoupled.scala 296:21]
  wire [127:0] bundleOut_1_a_q_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleOut_1_a_q_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleIn_1_d_q_rf_reset; // @[Decoupled.scala 296:21]
  wire  bundleIn_1_d_q_clock; // @[Decoupled.scala 296:21]
  wire  bundleIn_1_d_q_reset; // @[Decoupled.scala 296:21]
  wire  bundleIn_1_d_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  bundleIn_1_d_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_1_d_q_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] bundleIn_1_d_q_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_1_d_q_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleIn_1_d_q_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_1_d_q_io_enq_bits_sink; // @[Decoupled.scala 296:21]
  wire  bundleIn_1_d_q_io_enq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] bundleIn_1_d_q_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleIn_1_d_q_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleIn_1_d_q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  bundleIn_1_d_q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_1_d_q_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] bundleIn_1_d_q_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_1_d_q_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleIn_1_d_q_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_1_d_q_io_deq_bits_sink; // @[Decoupled.scala 296:21]
  wire  bundleIn_1_d_q_io_deq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] bundleIn_1_d_q_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleIn_1_d_q_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleOut_2_a_q_rf_reset; // @[Decoupled.scala 296:21]
  wire  bundleOut_2_a_q_clock; // @[Decoupled.scala 296:21]
  wire  bundleOut_2_a_q_reset; // @[Decoupled.scala 296:21]
  wire  bundleOut_2_a_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  bundleOut_2_a_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_2_a_q_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_2_a_q_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_2_a_q_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleOut_2_a_q_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [35:0] bundleOut_2_a_q_io_enq_bits_address; // @[Decoupled.scala 296:21]
  wire [15:0] bundleOut_2_a_q_io_enq_bits_mask; // @[Decoupled.scala 296:21]
  wire [127:0] bundleOut_2_a_q_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleOut_2_a_q_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleOut_2_a_q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  bundleOut_2_a_q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_2_a_q_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_2_a_q_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_2_a_q_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleOut_2_a_q_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [35:0] bundleOut_2_a_q_io_deq_bits_address; // @[Decoupled.scala 296:21]
  wire [15:0] bundleOut_2_a_q_io_deq_bits_mask; // @[Decoupled.scala 296:21]
  wire [127:0] bundleOut_2_a_q_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleOut_2_a_q_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleIn_2_d_q_rf_reset; // @[Decoupled.scala 296:21]
  wire  bundleIn_2_d_q_clock; // @[Decoupled.scala 296:21]
  wire  bundleIn_2_d_q_reset; // @[Decoupled.scala 296:21]
  wire  bundleIn_2_d_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  bundleIn_2_d_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_2_d_q_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] bundleIn_2_d_q_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_2_d_q_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleIn_2_d_q_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_2_d_q_io_enq_bits_sink; // @[Decoupled.scala 296:21]
  wire  bundleIn_2_d_q_io_enq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] bundleIn_2_d_q_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleIn_2_d_q_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleIn_2_d_q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  bundleIn_2_d_q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_2_d_q_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] bundleIn_2_d_q_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_2_d_q_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleIn_2_d_q_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_2_d_q_io_deq_bits_sink; // @[Decoupled.scala 296:21]
  wire  bundleIn_2_d_q_io_deq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] bundleIn_2_d_q_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleIn_2_d_q_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleOut_3_a_q_rf_reset; // @[Decoupled.scala 296:21]
  wire  bundleOut_3_a_q_clock; // @[Decoupled.scala 296:21]
  wire  bundleOut_3_a_q_reset; // @[Decoupled.scala 296:21]
  wire  bundleOut_3_a_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  bundleOut_3_a_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_3_a_q_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_3_a_q_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_3_a_q_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleOut_3_a_q_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [35:0] bundleOut_3_a_q_io_enq_bits_address; // @[Decoupled.scala 296:21]
  wire [15:0] bundleOut_3_a_q_io_enq_bits_mask; // @[Decoupled.scala 296:21]
  wire [127:0] bundleOut_3_a_q_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleOut_3_a_q_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleOut_3_a_q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  bundleOut_3_a_q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_3_a_q_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_3_a_q_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_3_a_q_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleOut_3_a_q_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [35:0] bundleOut_3_a_q_io_deq_bits_address; // @[Decoupled.scala 296:21]
  wire [15:0] bundleOut_3_a_q_io_deq_bits_mask; // @[Decoupled.scala 296:21]
  wire [127:0] bundleOut_3_a_q_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleOut_3_a_q_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleIn_3_d_q_rf_reset; // @[Decoupled.scala 296:21]
  wire  bundleIn_3_d_q_clock; // @[Decoupled.scala 296:21]
  wire  bundleIn_3_d_q_reset; // @[Decoupled.scala 296:21]
  wire  bundleIn_3_d_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  bundleIn_3_d_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_3_d_q_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] bundleIn_3_d_q_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_3_d_q_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleIn_3_d_q_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_3_d_q_io_enq_bits_sink; // @[Decoupled.scala 296:21]
  wire  bundleIn_3_d_q_io_enq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] bundleIn_3_d_q_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleIn_3_d_q_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleIn_3_d_q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  bundleIn_3_d_q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_3_d_q_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] bundleIn_3_d_q_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_3_d_q_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] bundleIn_3_d_q_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_3_d_q_io_deq_bits_sink; // @[Decoupled.scala 296:21]
  wire  bundleIn_3_d_q_io_deq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] bundleIn_3_d_q_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleIn_3_d_q_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  RHEA__Queue_152 bundleOut_0_a_q ( // @[Decoupled.scala 296:21]
    .rf_reset(bundleOut_0_a_q_rf_reset),
    .clock(bundleOut_0_a_q_clock),
    .reset(bundleOut_0_a_q_reset),
    .io_enq_ready(bundleOut_0_a_q_io_enq_ready),
    .io_enq_valid(bundleOut_0_a_q_io_enq_valid),
    .io_enq_bits_opcode(bundleOut_0_a_q_io_enq_bits_opcode),
    .io_enq_bits_param(bundleOut_0_a_q_io_enq_bits_param),
    .io_enq_bits_size(bundleOut_0_a_q_io_enq_bits_size),
    .io_enq_bits_source(bundleOut_0_a_q_io_enq_bits_source),
    .io_enq_bits_address(bundleOut_0_a_q_io_enq_bits_address),
    .io_enq_bits_mask(bundleOut_0_a_q_io_enq_bits_mask),
    .io_enq_bits_data(bundleOut_0_a_q_io_enq_bits_data),
    .io_enq_bits_corrupt(bundleOut_0_a_q_io_enq_bits_corrupt),
    .io_deq_ready(bundleOut_0_a_q_io_deq_ready),
    .io_deq_valid(bundleOut_0_a_q_io_deq_valid),
    .io_deq_bits_opcode(bundleOut_0_a_q_io_deq_bits_opcode),
    .io_deq_bits_param(bundleOut_0_a_q_io_deq_bits_param),
    .io_deq_bits_size(bundleOut_0_a_q_io_deq_bits_size),
    .io_deq_bits_source(bundleOut_0_a_q_io_deq_bits_source),
    .io_deq_bits_address(bundleOut_0_a_q_io_deq_bits_address),
    .io_deq_bits_mask(bundleOut_0_a_q_io_deq_bits_mask),
    .io_deq_bits_data(bundleOut_0_a_q_io_deq_bits_data),
    .io_deq_bits_corrupt(bundleOut_0_a_q_io_deq_bits_corrupt)
  );
  RHEA__Queue_153 bundleIn_0_d_q ( // @[Decoupled.scala 296:21]
    .rf_reset(bundleIn_0_d_q_rf_reset),
    .clock(bundleIn_0_d_q_clock),
    .reset(bundleIn_0_d_q_reset),
    .io_enq_ready(bundleIn_0_d_q_io_enq_ready),
    .io_enq_valid(bundleIn_0_d_q_io_enq_valid),
    .io_enq_bits_opcode(bundleIn_0_d_q_io_enq_bits_opcode),
    .io_enq_bits_param(bundleIn_0_d_q_io_enq_bits_param),
    .io_enq_bits_size(bundleIn_0_d_q_io_enq_bits_size),
    .io_enq_bits_source(bundleIn_0_d_q_io_enq_bits_source),
    .io_enq_bits_sink(bundleIn_0_d_q_io_enq_bits_sink),
    .io_enq_bits_denied(bundleIn_0_d_q_io_enq_bits_denied),
    .io_enq_bits_data(bundleIn_0_d_q_io_enq_bits_data),
    .io_enq_bits_corrupt(bundleIn_0_d_q_io_enq_bits_corrupt),
    .io_deq_ready(bundleIn_0_d_q_io_deq_ready),
    .io_deq_valid(bundleIn_0_d_q_io_deq_valid),
    .io_deq_bits_opcode(bundleIn_0_d_q_io_deq_bits_opcode),
    .io_deq_bits_param(bundleIn_0_d_q_io_deq_bits_param),
    .io_deq_bits_size(bundleIn_0_d_q_io_deq_bits_size),
    .io_deq_bits_source(bundleIn_0_d_q_io_deq_bits_source),
    .io_deq_bits_sink(bundleIn_0_d_q_io_deq_bits_sink),
    .io_deq_bits_denied(bundleIn_0_d_q_io_deq_bits_denied),
    .io_deq_bits_data(bundleIn_0_d_q_io_deq_bits_data),
    .io_deq_bits_corrupt(bundleIn_0_d_q_io_deq_bits_corrupt)
  );
  RHEA__Queue_152 bundleOut_1_a_q ( // @[Decoupled.scala 296:21]
    .rf_reset(bundleOut_1_a_q_rf_reset),
    .clock(bundleOut_1_a_q_clock),
    .reset(bundleOut_1_a_q_reset),
    .io_enq_ready(bundleOut_1_a_q_io_enq_ready),
    .io_enq_valid(bundleOut_1_a_q_io_enq_valid),
    .io_enq_bits_opcode(bundleOut_1_a_q_io_enq_bits_opcode),
    .io_enq_bits_param(bundleOut_1_a_q_io_enq_bits_param),
    .io_enq_bits_size(bundleOut_1_a_q_io_enq_bits_size),
    .io_enq_bits_source(bundleOut_1_a_q_io_enq_bits_source),
    .io_enq_bits_address(bundleOut_1_a_q_io_enq_bits_address),
    .io_enq_bits_mask(bundleOut_1_a_q_io_enq_bits_mask),
    .io_enq_bits_data(bundleOut_1_a_q_io_enq_bits_data),
    .io_enq_bits_corrupt(bundleOut_1_a_q_io_enq_bits_corrupt),
    .io_deq_ready(bundleOut_1_a_q_io_deq_ready),
    .io_deq_valid(bundleOut_1_a_q_io_deq_valid),
    .io_deq_bits_opcode(bundleOut_1_a_q_io_deq_bits_opcode),
    .io_deq_bits_param(bundleOut_1_a_q_io_deq_bits_param),
    .io_deq_bits_size(bundleOut_1_a_q_io_deq_bits_size),
    .io_deq_bits_source(bundleOut_1_a_q_io_deq_bits_source),
    .io_deq_bits_address(bundleOut_1_a_q_io_deq_bits_address),
    .io_deq_bits_mask(bundleOut_1_a_q_io_deq_bits_mask),
    .io_deq_bits_data(bundleOut_1_a_q_io_deq_bits_data),
    .io_deq_bits_corrupt(bundleOut_1_a_q_io_deq_bits_corrupt)
  );
  RHEA__Queue_153 bundleIn_1_d_q ( // @[Decoupled.scala 296:21]
    .rf_reset(bundleIn_1_d_q_rf_reset),
    .clock(bundleIn_1_d_q_clock),
    .reset(bundleIn_1_d_q_reset),
    .io_enq_ready(bundleIn_1_d_q_io_enq_ready),
    .io_enq_valid(bundleIn_1_d_q_io_enq_valid),
    .io_enq_bits_opcode(bundleIn_1_d_q_io_enq_bits_opcode),
    .io_enq_bits_param(bundleIn_1_d_q_io_enq_bits_param),
    .io_enq_bits_size(bundleIn_1_d_q_io_enq_bits_size),
    .io_enq_bits_source(bundleIn_1_d_q_io_enq_bits_source),
    .io_enq_bits_sink(bundleIn_1_d_q_io_enq_bits_sink),
    .io_enq_bits_denied(bundleIn_1_d_q_io_enq_bits_denied),
    .io_enq_bits_data(bundleIn_1_d_q_io_enq_bits_data),
    .io_enq_bits_corrupt(bundleIn_1_d_q_io_enq_bits_corrupt),
    .io_deq_ready(bundleIn_1_d_q_io_deq_ready),
    .io_deq_valid(bundleIn_1_d_q_io_deq_valid),
    .io_deq_bits_opcode(bundleIn_1_d_q_io_deq_bits_opcode),
    .io_deq_bits_param(bundleIn_1_d_q_io_deq_bits_param),
    .io_deq_bits_size(bundleIn_1_d_q_io_deq_bits_size),
    .io_deq_bits_source(bundleIn_1_d_q_io_deq_bits_source),
    .io_deq_bits_sink(bundleIn_1_d_q_io_deq_bits_sink),
    .io_deq_bits_denied(bundleIn_1_d_q_io_deq_bits_denied),
    .io_deq_bits_data(bundleIn_1_d_q_io_deq_bits_data),
    .io_deq_bits_corrupt(bundleIn_1_d_q_io_deq_bits_corrupt)
  );
  RHEA__Queue_152 bundleOut_2_a_q ( // @[Decoupled.scala 296:21]
    .rf_reset(bundleOut_2_a_q_rf_reset),
    .clock(bundleOut_2_a_q_clock),
    .reset(bundleOut_2_a_q_reset),
    .io_enq_ready(bundleOut_2_a_q_io_enq_ready),
    .io_enq_valid(bundleOut_2_a_q_io_enq_valid),
    .io_enq_bits_opcode(bundleOut_2_a_q_io_enq_bits_opcode),
    .io_enq_bits_param(bundleOut_2_a_q_io_enq_bits_param),
    .io_enq_bits_size(bundleOut_2_a_q_io_enq_bits_size),
    .io_enq_bits_source(bundleOut_2_a_q_io_enq_bits_source),
    .io_enq_bits_address(bundleOut_2_a_q_io_enq_bits_address),
    .io_enq_bits_mask(bundleOut_2_a_q_io_enq_bits_mask),
    .io_enq_bits_data(bundleOut_2_a_q_io_enq_bits_data),
    .io_enq_bits_corrupt(bundleOut_2_a_q_io_enq_bits_corrupt),
    .io_deq_ready(bundleOut_2_a_q_io_deq_ready),
    .io_deq_valid(bundleOut_2_a_q_io_deq_valid),
    .io_deq_bits_opcode(bundleOut_2_a_q_io_deq_bits_opcode),
    .io_deq_bits_param(bundleOut_2_a_q_io_deq_bits_param),
    .io_deq_bits_size(bundleOut_2_a_q_io_deq_bits_size),
    .io_deq_bits_source(bundleOut_2_a_q_io_deq_bits_source),
    .io_deq_bits_address(bundleOut_2_a_q_io_deq_bits_address),
    .io_deq_bits_mask(bundleOut_2_a_q_io_deq_bits_mask),
    .io_deq_bits_data(bundleOut_2_a_q_io_deq_bits_data),
    .io_deq_bits_corrupt(bundleOut_2_a_q_io_deq_bits_corrupt)
  );
  RHEA__Queue_153 bundleIn_2_d_q ( // @[Decoupled.scala 296:21]
    .rf_reset(bundleIn_2_d_q_rf_reset),
    .clock(bundleIn_2_d_q_clock),
    .reset(bundleIn_2_d_q_reset),
    .io_enq_ready(bundleIn_2_d_q_io_enq_ready),
    .io_enq_valid(bundleIn_2_d_q_io_enq_valid),
    .io_enq_bits_opcode(bundleIn_2_d_q_io_enq_bits_opcode),
    .io_enq_bits_param(bundleIn_2_d_q_io_enq_bits_param),
    .io_enq_bits_size(bundleIn_2_d_q_io_enq_bits_size),
    .io_enq_bits_source(bundleIn_2_d_q_io_enq_bits_source),
    .io_enq_bits_sink(bundleIn_2_d_q_io_enq_bits_sink),
    .io_enq_bits_denied(bundleIn_2_d_q_io_enq_bits_denied),
    .io_enq_bits_data(bundleIn_2_d_q_io_enq_bits_data),
    .io_enq_bits_corrupt(bundleIn_2_d_q_io_enq_bits_corrupt),
    .io_deq_ready(bundleIn_2_d_q_io_deq_ready),
    .io_deq_valid(bundleIn_2_d_q_io_deq_valid),
    .io_deq_bits_opcode(bundleIn_2_d_q_io_deq_bits_opcode),
    .io_deq_bits_param(bundleIn_2_d_q_io_deq_bits_param),
    .io_deq_bits_size(bundleIn_2_d_q_io_deq_bits_size),
    .io_deq_bits_source(bundleIn_2_d_q_io_deq_bits_source),
    .io_deq_bits_sink(bundleIn_2_d_q_io_deq_bits_sink),
    .io_deq_bits_denied(bundleIn_2_d_q_io_deq_bits_denied),
    .io_deq_bits_data(bundleIn_2_d_q_io_deq_bits_data),
    .io_deq_bits_corrupt(bundleIn_2_d_q_io_deq_bits_corrupt)
  );
  RHEA__Queue_152 bundleOut_3_a_q ( // @[Decoupled.scala 296:21]
    .rf_reset(bundleOut_3_a_q_rf_reset),
    .clock(bundleOut_3_a_q_clock),
    .reset(bundleOut_3_a_q_reset),
    .io_enq_ready(bundleOut_3_a_q_io_enq_ready),
    .io_enq_valid(bundleOut_3_a_q_io_enq_valid),
    .io_enq_bits_opcode(bundleOut_3_a_q_io_enq_bits_opcode),
    .io_enq_bits_param(bundleOut_3_a_q_io_enq_bits_param),
    .io_enq_bits_size(bundleOut_3_a_q_io_enq_bits_size),
    .io_enq_bits_source(bundleOut_3_a_q_io_enq_bits_source),
    .io_enq_bits_address(bundleOut_3_a_q_io_enq_bits_address),
    .io_enq_bits_mask(bundleOut_3_a_q_io_enq_bits_mask),
    .io_enq_bits_data(bundleOut_3_a_q_io_enq_bits_data),
    .io_enq_bits_corrupt(bundleOut_3_a_q_io_enq_bits_corrupt),
    .io_deq_ready(bundleOut_3_a_q_io_deq_ready),
    .io_deq_valid(bundleOut_3_a_q_io_deq_valid),
    .io_deq_bits_opcode(bundleOut_3_a_q_io_deq_bits_opcode),
    .io_deq_bits_param(bundleOut_3_a_q_io_deq_bits_param),
    .io_deq_bits_size(bundleOut_3_a_q_io_deq_bits_size),
    .io_deq_bits_source(bundleOut_3_a_q_io_deq_bits_source),
    .io_deq_bits_address(bundleOut_3_a_q_io_deq_bits_address),
    .io_deq_bits_mask(bundleOut_3_a_q_io_deq_bits_mask),
    .io_deq_bits_data(bundleOut_3_a_q_io_deq_bits_data),
    .io_deq_bits_corrupt(bundleOut_3_a_q_io_deq_bits_corrupt)
  );
  RHEA__Queue_153 bundleIn_3_d_q ( // @[Decoupled.scala 296:21]
    .rf_reset(bundleIn_3_d_q_rf_reset),
    .clock(bundleIn_3_d_q_clock),
    .reset(bundleIn_3_d_q_reset),
    .io_enq_ready(bundleIn_3_d_q_io_enq_ready),
    .io_enq_valid(bundleIn_3_d_q_io_enq_valid),
    .io_enq_bits_opcode(bundleIn_3_d_q_io_enq_bits_opcode),
    .io_enq_bits_param(bundleIn_3_d_q_io_enq_bits_param),
    .io_enq_bits_size(bundleIn_3_d_q_io_enq_bits_size),
    .io_enq_bits_source(bundleIn_3_d_q_io_enq_bits_source),
    .io_enq_bits_sink(bundleIn_3_d_q_io_enq_bits_sink),
    .io_enq_bits_denied(bundleIn_3_d_q_io_enq_bits_denied),
    .io_enq_bits_data(bundleIn_3_d_q_io_enq_bits_data),
    .io_enq_bits_corrupt(bundleIn_3_d_q_io_enq_bits_corrupt),
    .io_deq_ready(bundleIn_3_d_q_io_deq_ready),
    .io_deq_valid(bundleIn_3_d_q_io_deq_valid),
    .io_deq_bits_opcode(bundleIn_3_d_q_io_deq_bits_opcode),
    .io_deq_bits_param(bundleIn_3_d_q_io_deq_bits_param),
    .io_deq_bits_size(bundleIn_3_d_q_io_deq_bits_size),
    .io_deq_bits_source(bundleIn_3_d_q_io_deq_bits_source),
    .io_deq_bits_sink(bundleIn_3_d_q_io_deq_bits_sink),
    .io_deq_bits_denied(bundleIn_3_d_q_io_deq_bits_denied),
    .io_deq_bits_data(bundleIn_3_d_q_io_deq_bits_data),
    .io_deq_bits_corrupt(bundleIn_3_d_q_io_deq_bits_corrupt)
  );
  assign bundleOut_0_a_q_rf_reset = rf_reset;
  assign bundleIn_0_d_q_rf_reset = rf_reset;
  assign bundleOut_1_a_q_rf_reset = rf_reset;
  assign bundleIn_1_d_q_rf_reset = rf_reset;
  assign bundleOut_2_a_q_rf_reset = rf_reset;
  assign bundleIn_2_d_q_rf_reset = rf_reset;
  assign bundleOut_3_a_q_rf_reset = rf_reset;
  assign bundleIn_3_d_q_rf_reset = rf_reset;
  assign auto_in_3_a_ready = bundleOut_3_a_q_io_enq_ready; // @[Nodes.scala 1529:13 Decoupled.scala 299:17]
  assign auto_in_3_b_valid = auto_out_3_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_3_b_bits_opcode = auto_out_3_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_3_b_bits_param = auto_out_3_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_3_b_bits_size = auto_out_3_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_3_b_bits_source = auto_out_3_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_3_b_bits_address = auto_out_3_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_3_b_bits_mask = auto_out_3_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_3_b_bits_data = auto_out_3_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_3_b_bits_corrupt = auto_out_3_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_3_c_ready = auto_out_3_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_3_d_valid = bundleIn_3_d_q_io_deq_valid; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_3_d_bits_opcode = bundleIn_3_d_q_io_deq_bits_opcode; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_3_d_bits_param = bundleIn_3_d_q_io_deq_bits_param; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_3_d_bits_size = bundleIn_3_d_q_io_deq_bits_size; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_3_d_bits_source = bundleIn_3_d_q_io_deq_bits_source; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_3_d_bits_sink = bundleIn_3_d_q_io_deq_bits_sink; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_3_d_bits_denied = bundleIn_3_d_q_io_deq_bits_denied; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_3_d_bits_data = bundleIn_3_d_q_io_deq_bits_data; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_3_d_bits_corrupt = bundleIn_3_d_q_io_deq_bits_corrupt; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_3_e_ready = auto_out_3_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_a_ready = bundleOut_2_a_q_io_enq_ready; // @[Nodes.scala 1529:13 Decoupled.scala 299:17]
  assign auto_in_2_b_valid = auto_out_2_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_opcode = auto_out_2_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_param = auto_out_2_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_size = auto_out_2_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_source = auto_out_2_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_address = auto_out_2_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_mask = auto_out_2_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_data = auto_out_2_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_corrupt = auto_out_2_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_c_ready = auto_out_2_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_d_valid = bundleIn_2_d_q_io_deq_valid; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_2_d_bits_opcode = bundleIn_2_d_q_io_deq_bits_opcode; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_2_d_bits_param = bundleIn_2_d_q_io_deq_bits_param; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_2_d_bits_size = bundleIn_2_d_q_io_deq_bits_size; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_2_d_bits_source = bundleIn_2_d_q_io_deq_bits_source; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_2_d_bits_sink = bundleIn_2_d_q_io_deq_bits_sink; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_2_d_bits_denied = bundleIn_2_d_q_io_deq_bits_denied; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_2_d_bits_data = bundleIn_2_d_q_io_deq_bits_data; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_2_d_bits_corrupt = bundleIn_2_d_q_io_deq_bits_corrupt; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_2_e_ready = auto_out_2_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_a_ready = bundleOut_1_a_q_io_enq_ready; // @[Nodes.scala 1529:13 Decoupled.scala 299:17]
  assign auto_in_1_b_valid = auto_out_1_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_opcode = auto_out_1_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_param = auto_out_1_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_size = auto_out_1_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_source = auto_out_1_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_address = auto_out_1_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_mask = auto_out_1_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_data = auto_out_1_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_corrupt = auto_out_1_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_c_ready = auto_out_1_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_d_valid = bundleIn_1_d_q_io_deq_valid; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_1_d_bits_opcode = bundleIn_1_d_q_io_deq_bits_opcode; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_1_d_bits_param = bundleIn_1_d_q_io_deq_bits_param; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_1_d_bits_size = bundleIn_1_d_q_io_deq_bits_size; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_1_d_bits_source = bundleIn_1_d_q_io_deq_bits_source; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_1_d_bits_sink = bundleIn_1_d_q_io_deq_bits_sink; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_1_d_bits_denied = bundleIn_1_d_q_io_deq_bits_denied; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_1_d_bits_data = bundleIn_1_d_q_io_deq_bits_data; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_1_d_bits_corrupt = bundleIn_1_d_q_io_deq_bits_corrupt; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_1_e_ready = auto_out_1_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_a_ready = bundleOut_0_a_q_io_enq_ready; // @[Nodes.scala 1529:13 Decoupled.scala 299:17]
  assign auto_in_0_b_valid = auto_out_0_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_b_bits_opcode = auto_out_0_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_b_bits_param = auto_out_0_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_b_bits_size = auto_out_0_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_b_bits_source = auto_out_0_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_b_bits_address = auto_out_0_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_b_bits_mask = auto_out_0_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_b_bits_data = auto_out_0_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_b_bits_corrupt = auto_out_0_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_c_ready = auto_out_0_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_d_valid = bundleIn_0_d_q_io_deq_valid; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_0_d_bits_opcode = bundleIn_0_d_q_io_deq_bits_opcode; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_0_d_bits_param = bundleIn_0_d_q_io_deq_bits_param; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_0_d_bits_size = bundleIn_0_d_q_io_deq_bits_size; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_0_d_bits_source = bundleIn_0_d_q_io_deq_bits_source; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_0_d_bits_sink = bundleIn_0_d_q_io_deq_bits_sink; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_0_d_bits_denied = bundleIn_0_d_q_io_deq_bits_denied; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_0_d_bits_data = bundleIn_0_d_q_io_deq_bits_data; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_0_d_bits_corrupt = bundleIn_0_d_q_io_deq_bits_corrupt; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_0_e_ready = auto_out_0_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_out_3_a_valid = bundleOut_3_a_q_io_deq_valid; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_3_a_bits_opcode = bundleOut_3_a_q_io_deq_bits_opcode; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_3_a_bits_param = bundleOut_3_a_q_io_deq_bits_param; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_3_a_bits_size = bundleOut_3_a_q_io_deq_bits_size; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_3_a_bits_source = bundleOut_3_a_q_io_deq_bits_source; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_3_a_bits_address = bundleOut_3_a_q_io_deq_bits_address; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_3_a_bits_mask = bundleOut_3_a_q_io_deq_bits_mask; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_3_a_bits_data = bundleOut_3_a_q_io_deq_bits_data; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_3_a_bits_corrupt = bundleOut_3_a_q_io_deq_bits_corrupt; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_3_b_ready = auto_in_3_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_3_c_valid = auto_in_3_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_3_c_bits_opcode = auto_in_3_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_3_c_bits_param = auto_in_3_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_3_c_bits_size = auto_in_3_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_3_c_bits_source = auto_in_3_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_3_c_bits_address = auto_in_3_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_3_c_bits_data = auto_in_3_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_3_c_bits_corrupt = auto_in_3_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_3_d_ready = bundleIn_3_d_q_io_enq_ready; // @[Nodes.scala 1529:13 Decoupled.scala 299:17]
  assign auto_out_3_e_valid = auto_in_3_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_3_e_bits_sink = auto_in_3_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_a_valid = bundleOut_2_a_q_io_deq_valid; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_2_a_bits_opcode = bundleOut_2_a_q_io_deq_bits_opcode; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_2_a_bits_param = bundleOut_2_a_q_io_deq_bits_param; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_2_a_bits_size = bundleOut_2_a_q_io_deq_bits_size; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_2_a_bits_source = bundleOut_2_a_q_io_deq_bits_source; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_2_a_bits_address = bundleOut_2_a_q_io_deq_bits_address; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_2_a_bits_mask = bundleOut_2_a_q_io_deq_bits_mask; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_2_a_bits_data = bundleOut_2_a_q_io_deq_bits_data; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_2_a_bits_corrupt = bundleOut_2_a_q_io_deq_bits_corrupt; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_2_b_ready = auto_in_2_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_valid = auto_in_2_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_bits_opcode = auto_in_2_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_bits_param = auto_in_2_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_bits_size = auto_in_2_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_bits_source = auto_in_2_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_bits_address = auto_in_2_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_bits_data = auto_in_2_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_bits_corrupt = auto_in_2_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_d_ready = bundleIn_2_d_q_io_enq_ready; // @[Nodes.scala 1529:13 Decoupled.scala 299:17]
  assign auto_out_2_e_valid = auto_in_2_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_e_bits_sink = auto_in_2_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_valid = bundleOut_1_a_q_io_deq_valid; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_1_a_bits_opcode = bundleOut_1_a_q_io_deq_bits_opcode; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_1_a_bits_param = bundleOut_1_a_q_io_deq_bits_param; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_1_a_bits_size = bundleOut_1_a_q_io_deq_bits_size; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_1_a_bits_source = bundleOut_1_a_q_io_deq_bits_source; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_1_a_bits_address = bundleOut_1_a_q_io_deq_bits_address; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_1_a_bits_mask = bundleOut_1_a_q_io_deq_bits_mask; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_1_a_bits_data = bundleOut_1_a_q_io_deq_bits_data; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_1_a_bits_corrupt = bundleOut_1_a_q_io_deq_bits_corrupt; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_1_b_ready = auto_in_1_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_valid = auto_in_1_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_bits_opcode = auto_in_1_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_bits_param = auto_in_1_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_bits_size = auto_in_1_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_bits_source = auto_in_1_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_bits_address = auto_in_1_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_bits_data = auto_in_1_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_bits_corrupt = auto_in_1_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_d_ready = bundleIn_1_d_q_io_enq_ready; // @[Nodes.scala 1529:13 Decoupled.scala 299:17]
  assign auto_out_1_e_valid = auto_in_1_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_e_bits_sink = auto_in_1_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_a_valid = bundleOut_0_a_q_io_deq_valid; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_0_a_bits_opcode = bundleOut_0_a_q_io_deq_bits_opcode; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_0_a_bits_param = bundleOut_0_a_q_io_deq_bits_param; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_0_a_bits_size = bundleOut_0_a_q_io_deq_bits_size; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_0_a_bits_source = bundleOut_0_a_q_io_deq_bits_source; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_0_a_bits_address = bundleOut_0_a_q_io_deq_bits_address; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_0_a_bits_mask = bundleOut_0_a_q_io_deq_bits_mask; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_0_a_bits_data = bundleOut_0_a_q_io_deq_bits_data; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_0_a_bits_corrupt = bundleOut_0_a_q_io_deq_bits_corrupt; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_0_b_ready = auto_in_0_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_c_valid = auto_in_0_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_c_bits_opcode = auto_in_0_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_c_bits_param = auto_in_0_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_c_bits_size = auto_in_0_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_c_bits_source = auto_in_0_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_c_bits_address = auto_in_0_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_c_bits_data = auto_in_0_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_c_bits_corrupt = auto_in_0_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_d_ready = bundleIn_0_d_q_io_enq_ready; // @[Nodes.scala 1529:13 Decoupled.scala 299:17]
  assign auto_out_0_e_valid = auto_in_0_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_e_bits_sink = auto_in_0_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_clock = clock;
  assign bundleOut_0_a_q_reset = reset;
  assign bundleOut_0_a_q_io_enq_valid = auto_in_0_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_opcode = auto_in_0_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_param = auto_in_0_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_size = auto_in_0_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_source = auto_in_0_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_address = auto_in_0_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_mask = auto_in_0_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_data = auto_in_0_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_corrupt = auto_in_0_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_deq_ready = auto_out_0_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_clock = clock;
  assign bundleIn_0_d_q_reset = reset;
  assign bundleIn_0_d_q_io_enq_valid = auto_out_0_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_opcode = auto_out_0_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_param = auto_out_0_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_size = auto_out_0_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_source = auto_out_0_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_sink = auto_out_0_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_denied = auto_out_0_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_data = auto_out_0_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_corrupt = auto_out_0_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_deq_ready = auto_in_0_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_1_a_q_clock = clock;
  assign bundleOut_1_a_q_reset = reset;
  assign bundleOut_1_a_q_io_enq_valid = auto_in_1_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_1_a_q_io_enq_bits_opcode = auto_in_1_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_1_a_q_io_enq_bits_param = auto_in_1_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_1_a_q_io_enq_bits_size = auto_in_1_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_1_a_q_io_enq_bits_source = auto_in_1_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_1_a_q_io_enq_bits_address = auto_in_1_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_1_a_q_io_enq_bits_mask = auto_in_1_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_1_a_q_io_enq_bits_data = auto_in_1_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_1_a_q_io_enq_bits_corrupt = auto_in_1_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_1_a_q_io_deq_ready = auto_out_1_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_1_d_q_clock = clock;
  assign bundleIn_1_d_q_reset = reset;
  assign bundleIn_1_d_q_io_enq_valid = auto_out_1_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_1_d_q_io_enq_bits_opcode = auto_out_1_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_1_d_q_io_enq_bits_param = auto_out_1_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_1_d_q_io_enq_bits_size = auto_out_1_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_1_d_q_io_enq_bits_source = auto_out_1_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_1_d_q_io_enq_bits_sink = auto_out_1_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_1_d_q_io_enq_bits_denied = auto_out_1_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_1_d_q_io_enq_bits_data = auto_out_1_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_1_d_q_io_enq_bits_corrupt = auto_out_1_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_1_d_q_io_deq_ready = auto_in_1_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_2_a_q_clock = clock;
  assign bundleOut_2_a_q_reset = reset;
  assign bundleOut_2_a_q_io_enq_valid = auto_in_2_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_2_a_q_io_enq_bits_opcode = auto_in_2_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_2_a_q_io_enq_bits_param = auto_in_2_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_2_a_q_io_enq_bits_size = auto_in_2_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_2_a_q_io_enq_bits_source = auto_in_2_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_2_a_q_io_enq_bits_address = auto_in_2_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_2_a_q_io_enq_bits_mask = auto_in_2_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_2_a_q_io_enq_bits_data = auto_in_2_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_2_a_q_io_enq_bits_corrupt = auto_in_2_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_2_a_q_io_deq_ready = auto_out_2_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_2_d_q_clock = clock;
  assign bundleIn_2_d_q_reset = reset;
  assign bundleIn_2_d_q_io_enq_valid = auto_out_2_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_2_d_q_io_enq_bits_opcode = auto_out_2_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_2_d_q_io_enq_bits_param = auto_out_2_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_2_d_q_io_enq_bits_size = auto_out_2_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_2_d_q_io_enq_bits_source = auto_out_2_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_2_d_q_io_enq_bits_sink = auto_out_2_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_2_d_q_io_enq_bits_denied = auto_out_2_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_2_d_q_io_enq_bits_data = auto_out_2_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_2_d_q_io_enq_bits_corrupt = auto_out_2_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_2_d_q_io_deq_ready = auto_in_2_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_3_a_q_clock = clock;
  assign bundleOut_3_a_q_reset = reset;
  assign bundleOut_3_a_q_io_enq_valid = auto_in_3_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_3_a_q_io_enq_bits_opcode = auto_in_3_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_3_a_q_io_enq_bits_param = auto_in_3_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_3_a_q_io_enq_bits_size = auto_in_3_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_3_a_q_io_enq_bits_source = auto_in_3_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_3_a_q_io_enq_bits_address = auto_in_3_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_3_a_q_io_enq_bits_mask = auto_in_3_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_3_a_q_io_enq_bits_data = auto_in_3_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_3_a_q_io_enq_bits_corrupt = auto_in_3_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_3_a_q_io_deq_ready = auto_out_3_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_3_d_q_clock = clock;
  assign bundleIn_3_d_q_reset = reset;
  assign bundleIn_3_d_q_io_enq_valid = auto_out_3_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_3_d_q_io_enq_bits_opcode = auto_out_3_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_3_d_q_io_enq_bits_param = auto_out_3_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_3_d_q_io_enq_bits_size = auto_out_3_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_3_d_q_io_enq_bits_source = auto_out_3_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_3_d_q_io_enq_bits_sink = auto_out_3_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_3_d_q_io_enq_bits_denied = auto_out_3_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_3_d_q_io_enq_bits_data = auto_out_3_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_3_d_q_io_enq_bits_corrupt = auto_out_3_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_3_d_q_io_deq_ready = auto_in_3_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
endmodule
