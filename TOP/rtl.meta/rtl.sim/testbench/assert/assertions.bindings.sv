//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
bind SiFive_TLXbar SiFive_TLXbar_assert SiFive_TLXbar_assert_0 (.*);
bind SiFive_TLBuffer SiFive_TLBuffer_assert SiFive_TLBuffer_assert_0 (.*);
bind SiFive_TLFIFOFixer SiFive_TLFIFOFixer_assert SiFive_TLFIFOFixer_assert_0 (.*);
bind SiFive_RationalCrossingSink SiFive_RationalCrossingSink_assert SiFive_RationalCrossingSink_assert_0 (.*);
bind SiFive_RationalCrossingSource SiFive_RationalCrossingSource_assert SiFive_RationalCrossingSource_assert_0 (.*);
bind SiFive_RationalCrossingSource_1 SiFive_RationalCrossingSource_1_assert SiFive_RationalCrossingSource_1_assert_0 (.*);
bind SiFive_RationalCrossingSink_1 SiFive_RationalCrossingSink_1_assert SiFive_RationalCrossingSink_1_assert_0 (.*);
bind SiFive_RationalCrossingSink_2 SiFive_RationalCrossingSink_2_assert SiFive_RationalCrossingSink_2_assert_0 (.*);
bind SiFive_AXI4Deinterleaver SiFive_AXI4Deinterleaver_assert SiFive_AXI4Deinterleaver_assert_0 (.*);
bind SiFive_AXI4UserYanker SiFive_AXI4UserYanker_assert SiFive_AXI4UserYanker_assert_0 (.*);
bind SiFive_TLToAXI4 SiFive_TLToAXI4_assert SiFive_TLToAXI4_assert_0 (.*);
bind SiFive_TLWidthWidget SiFive_TLWidthWidget_assert SiFive_TLWidthWidget_assert_0 (.*);
bind SiFive_TLBuffer_2 SiFive_TLBuffer_2_assert SiFive_TLBuffer_2_assert_0 (.*);
bind SiFive_TLBuffer_3 SiFive_TLBuffer_3_assert SiFive_TLBuffer_3_assert_0 (.*);
bind SiFive_TLFragmenter SiFive_TLFragmenter_assert SiFive_TLFragmenter_assert_0 (.*);
bind SiFive_TLWidthWidget_1 SiFive_TLWidthWidget_1_assert SiFive_TLWidthWidget_1_assert_0 (.*);
bind SiFive_TLWidthWidget_2 SiFive_TLWidthWidget_2_assert SiFive_TLWidthWidget_2_assert_0 (.*);
bind SiFive_TLXbar_3 SiFive_TLXbar_3_assert SiFive_TLXbar_3_assert_0 (.*);
bind SiFive_TLBuffer_6 SiFive_TLBuffer_6_assert SiFive_TLBuffer_6_assert_0 (.*);
bind SiFive_TLFIFOFixer_3 SiFive_TLFIFOFixer_3_assert SiFive_TLFIFOFixer_3_assert_0 (.*);
bind SiFive_TLWidthWidget_3 SiFive_TLWidthWidget_3_assert SiFive_TLWidthWidget_3_assert_0 (.*);
bind SiFive_TLBuffer_7 SiFive_TLBuffer_7_assert SiFive_TLBuffer_7_assert_0 (.*);
bind SiFive_TLFIFOFixer_4 SiFive_TLFIFOFixer_4_assert SiFive_TLFIFOFixer_4_assert_0 (.*);
bind SiFive_TLWidthWidget_4 SiFive_TLWidthWidget_4_assert SiFive_TLWidthWidget_4_assert_0 (.*);
bind SiFive_AXI4ToTL SiFive_AXI4ToTL_assert SiFive_AXI4ToTL_assert_0 (.*);
bind SiFive_AXI4UserYanker_1 SiFive_AXI4UserYanker_1_assert SiFive_AXI4UserYanker_1_assert_0 (.*);
bind SiFive_AXI4Fragmenter SiFive_AXI4Fragmenter_assert SiFive_AXI4Fragmenter_assert_0 (.*);
bind SiFive_TLBuffer_8 SiFive_TLBuffer_8_assert SiFive_TLBuffer_8_assert_0 (.*);
bind SiFive_TLFIFOFixer_5 SiFive_TLFIFOFixer_5_assert SiFive_TLFIFOFixer_5_assert_0 (.*);
bind SiFive_TLWidthWidget_5 SiFive_TLWidthWidget_5_assert SiFive_TLWidthWidget_5_assert_0 (.*);
bind SiFive_TLBuffer_9 SiFive_TLBuffer_9_assert SiFive_TLBuffer_9_assert_0 (.*);
bind SiFive_TLXbar_4 SiFive_TLXbar_4_assert SiFive_TLXbar_4_assert_0 (.*);
bind SiFive_ProbePicker SiFive_ProbePicker_assert SiFive_ProbePicker_assert_0 (.*);
bind SiFive_TLZero SiFive_TLZero_assert SiFive_TLZero_assert_0 (.*);
bind SiFive_TLFragmenter_1 SiFive_TLFragmenter_1_assert SiFive_TLFragmenter_1_assert_0 (.*);
bind SiFive_TLBuffer_10 SiFive_TLBuffer_10_assert SiFive_TLBuffer_10_assert_0 (.*);
bind SiFive_AXI4UserYanker_2 SiFive_AXI4UserYanker_2_assert SiFive_AXI4UserYanker_2_assert_0 (.*);
bind SiFive_AXI4Deinterleaver_1 SiFive_AXI4Deinterleaver_1_assert SiFive_AXI4Deinterleaver_1_assert_0 (.*);
bind SiFive_TLToAXI4_1 SiFive_TLToAXI4_1_assert SiFive_TLToAXI4_1_assert_0 (.*);
bind SiFive_TLWidthWidget_6 SiFive_TLWidthWidget_6_assert SiFive_TLWidthWidget_6_assert_0 (.*);
bind SiFive_TLBuffer_11 SiFive_TLBuffer_11_assert SiFive_TLBuffer_11_assert_0 (.*);
bind SiFive_ProbePicker_1 SiFive_ProbePicker_1_assert SiFive_ProbePicker_1_assert_0 (.*);
bind SiFive_BankBinder SiFive_BankBinder_assert SiFive_BankBinder_assert_0 (.*);
bind SiFive_TLFIFOFixer_6 SiFive_TLFIFOFixer_6_assert SiFive_TLFIFOFixer_6_assert_0 (.*);
bind SiFive_TLXbar_5 SiFive_TLXbar_5_assert SiFive_TLXbar_5_assert_0 (.*);
bind SiFive_TLXbar_6 SiFive_TLXbar_6_assert SiFive_TLXbar_6_assert_0 (.*);
bind SiFive_TLBuffer_12 SiFive_TLBuffer_12_assert SiFive_TLBuffer_12_assert_0 (.*);
bind SiFive_TLAtomicAutomata_1 SiFive_TLAtomicAutomata_1_assert SiFive_TLAtomicAutomata_1_assert_0 (.*);
bind SiFive_TLError SiFive_TLError_assert SiFive_TLError_assert_0 (.*);
bind SiFive_TLBuffer_13 SiFive_TLBuffer_13_assert SiFive_TLBuffer_13_assert_0 (.*);
bind SiFive_TLFragmenter_2 SiFive_TLFragmenter_2_assert SiFive_TLFragmenter_2_assert_0 (.*);
bind SiFive_TLFragmenter_3 SiFive_TLFragmenter_3_assert SiFive_TLFragmenter_3_assert_0 (.*);
bind SiFive_TLFragmenter_4 SiFive_TLFragmenter_4_assert SiFive_TLFragmenter_4_assert_0 (.*);
bind SiFive_TLFragmenter_5 SiFive_TLFragmenter_5_assert SiFive_TLFragmenter_5_assert_0 (.*);
bind SiFive_AXI4UserYanker_3 SiFive_AXI4UserYanker_3_assert SiFive_AXI4UserYanker_3_assert_0 (.*);
bind SiFive_TLToAXI4_2 SiFive_TLToAXI4_2_assert SiFive_TLToAXI4_2_assert_0 (.*);
bind SiFive_TLWidthWidget_9 SiFive_TLWidthWidget_9_assert SiFive_TLWidthWidget_9_assert_0 (.*);
bind SiFive_TLBuffer_16 SiFive_TLBuffer_16_assert SiFive_TLBuffer_16_assert_0 (.*);
bind SiFive_TLFragmenter_6 SiFive_TLFragmenter_6_assert SiFive_TLFragmenter_6_assert_0 (.*);
bind SiFive_TLFragmenter_7 SiFive_TLFragmenter_7_assert SiFive_TLFragmenter_7_assert_0 (.*);
bind SiFive_TLFragmenter_8 SiFive_TLFragmenter_8_assert SiFive_TLFragmenter_8_assert_0 (.*);
bind SiFive_TLFragmenter_9 SiFive_TLFragmenter_9_assert SiFive_TLFragmenter_9_assert_0 (.*);
bind SiFive_TLWidthWidget_10 SiFive_TLWidthWidget_10_assert SiFive_TLWidthWidget_10_assert_0 (.*);
bind SiFive_TLBuffer_17 SiFive_TLBuffer_17_assert SiFive_TLBuffer_17_assert_0 (.*);
bind SiFive_TLFragmenter_10 SiFive_TLFragmenter_10_assert SiFive_TLFragmenter_10_assert_0 (.*);
bind SiFive_TLBuffer_18 SiFive_TLBuffer_18_assert SiFive_TLBuffer_18_assert_0 (.*);
bind SiFive_TLPLIC SiFive_TLPLIC_assert SiFive_TLPLIC_assert_0 (.*);
bind SiFive_CLINT SiFive_CLINT_assert SiFive_CLINT_assert_0 (.*);
bind SiFive_TLXbar_9 SiFive_TLXbar_9_assert SiFive_TLXbar_9_assert_0 (.*);
bind SiFive_TLDebugModuleOuter SiFive_TLDebugModuleOuter_assert SiFive_TLDebugModuleOuter_assert_0 (.*);
bind SiFive_TLBusBypassBar SiFive_TLBusBypassBar_assert SiFive_TLBusBypassBar_assert_0 (.*);
bind SiFive_TLError_1 SiFive_TLError_1_assert SiFive_TLError_1_assert_0 (.*);
bind SiFive_TLAsyncCrossingSource SiFive_TLAsyncCrossingSource_assert SiFive_TLAsyncCrossingSource_assert_0 (.*);
bind SiFive_SBToTL SiFive_SBToTL_assert SiFive_SBToTL_assert_0 (.*);
bind SiFive_TLDebugModuleInner SiFive_TLDebugModuleInner_assert SiFive_TLDebugModuleInner_assert_0 (.*);
bind SiFive_TLXbar_10 SiFive_TLXbar_10_assert SiFive_TLXbar_10_assert_0 (.*);
bind SiFive_DCache SiFive_DCache_assert SiFive_DCache_assert_0 (.*);
bind SiFive_ICache SiFive_ICache_assert SiFive_ICache_assert_0 (.*);
bind SiFive_TLB_1 SiFive_TLB_1_assert SiFive_TLB_1_assert_0 (.*);
bind SiFive_Frontend SiFive_Frontend_assert SiFive_Frontend_assert_0 (.*);
bind SiFive_TLBuffer_20 SiFive_TLBuffer_20_assert SiFive_TLBuffer_20_assert_0 (.*);
bind SiFive_RationalCrossingSource_4 SiFive_RationalCrossingSource_4_assert SiFive_RationalCrossingSource_4_assert_0 (.*);
bind SiFive_RationalCrossingSink_6 SiFive_RationalCrossingSink_6_assert SiFive_RationalCrossingSink_6_assert_0 (.*);
bind SiFive_RationalCrossingSink_7 SiFive_RationalCrossingSink_7_assert SiFive_RationalCrossingSink_7_assert_0 (.*);
bind SiFive_RationalCrossingSource_5 SiFive_RationalCrossingSource_5_assert SiFive_RationalCrossingSource_5_assert_0 (.*);
bind SiFive_RationalCrossingSource_6 SiFive_RationalCrossingSource_6_assert SiFive_RationalCrossingSource_6_assert_0 (.*);
bind SiFive_TLRationalCrossingSource_2 SiFive_TLRationalCrossingSource_2_assert SiFive_TLRationalCrossingSource_2_assert_0 (.*);
bind SiFive_FPU SiFive_FPU_assert SiFive_FPU_assert_0 (.*);
bind SiFive_PTW SiFive_PTW_assert SiFive_PTW_assert_0 (.*);
bind SiFive_IBuf SiFive_IBuf_assert SiFive_IBuf_assert_0 (.*);
bind SiFive_CSRFile SiFive_CSRFile_assert SiFive_CSRFile_assert_0 (.*);
bind SiFive_Rocket SiFive_Rocket_assert SiFive_Rocket_assert_0 (.*);
bind SiFive_RocketTile SiFive_RocketTile_assert SiFive_RocketTile_assert_0 (.*);
bind SiFive_RationalCrossingSink_10 SiFive_RationalCrossingSink_10_assert SiFive_RationalCrossingSink_10_assert_0 (.*);
bind SiFive_RationalCrossingSource_10 SiFive_RationalCrossingSource_10_assert SiFive_RationalCrossingSource_10_assert_0 (.*);
bind SiFive_TLXbar_14 SiFive_TLXbar_14_assert SiFive_TLXbar_14_assert_0 (.*);
bind SiFive_TLTraceEncoder SiFive_TLTraceEncoder_assert SiFive_TLTraceEncoder_assert_0 (.*);
bind SiFive_TLTraceEncoder_1 SiFive_TLTraceEncoder_1_assert SiFive_TLTraceEncoder_1_assert_0 (.*);
bind SiFive_TraceFunnel SiFive_TraceFunnel_assert SiFive_TraceFunnel_assert_0 (.*);
bind SiFive_TLBuffer_24 SiFive_TLBuffer_24_assert SiFive_TLBuffer_24_assert_0 (.*);
bind SiFive_TLTestIndicator SiFive_TLTestIndicator_assert SiFive_TLTestIndicator_assert_0 (.*);
bind SiFive_SourceB SiFive_SourceB_assert SiFive_SourceB_assert_0 (.*);
bind SiFive_SourceC SiFive_SourceC_assert SiFive_SourceC_assert_0 (.*);
bind SiFive_SourceD SiFive_SourceD_assert SiFive_SourceD_assert_0 (.*);
bind SiFive_ListBuffer SiFive_ListBuffer_assert SiFive_ListBuffer_assert_0 (.*);
bind SiFive_ListBuffer_1 SiFive_ListBuffer_1_assert SiFive_ListBuffer_1_assert_0 (.*);
bind SiFive_SinkD SiFive_SinkD_assert SiFive_SinkD_assert_0 (.*);
bind SiFive_Directory SiFive_Directory_assert SiFive_Directory_assert_0 (.*);
bind SiFive_BankedStore SiFive_BankedStore_assert SiFive_BankedStore_assert_0 (.*);
bind SiFive_ListBuffer_2 SiFive_ListBuffer_2_assert SiFive_ListBuffer_2_assert_0 (.*);
bind SiFive_MSHR SiFive_MSHR_assert SiFive_MSHR_assert_0 (.*);
bind SiFive_Scheduler SiFive_Scheduler_assert SiFive_Scheduler_assert_0 (.*);
bind SiFive_Sideband SiFive_Sideband_assert SiFive_Sideband_assert_0 (.*);
bind SiFive_ComposableCache SiFive_ComposableCache_assert SiFive_ComposableCache_assert_0 (.*);
bind SiFive_TLFilter SiFive_TLFilter_assert SiFive_TLFilter_assert_0 (.*);
bind SiFive_TLBuffer_25 SiFive_TLBuffer_25_assert SiFive_TLBuffer_25_assert_0 (.*);
bind SiFive_TLBuffer_26 SiFive_TLBuffer_26_assert SiFive_TLBuffer_26_assert_0 (.*);
bind SiFive_IDPool SiFive_IDPool_assert SiFive_IDPool_assert_0 (.*);
bind SiFive_TLCacheCork SiFive_TLCacheCork_assert SiFive_TLCacheCork_assert_0 (.*);
bind SiFive_CaptureUpdateChain SiFive_CaptureUpdateChain_assert SiFive_CaptureUpdateChain_assert_0 (.*);
bind SiFive_CaptureUpdateChain_1 SiFive_CaptureUpdateChain_1_assert SiFive_CaptureUpdateChain_1_assert_0 (.*);
bind SiFive_CaptureChain SiFive_CaptureChain_assert SiFive_CaptureChain_assert_0 (.*);
bind SiFive_CaptureUpdateChain_2 SiFive_CaptureUpdateChain_2_assert SiFive_CaptureUpdateChain_2_assert_0 (.*);
bind SiFive_JtagBypassChain SiFive_JtagBypassChain_assert SiFive_JtagBypassChain_assert_0 (.*);
bind SiFive_DebugTransportModuleJTAG SiFive_DebugTransportModuleJTAG_assert SiFive_DebugTransportModuleJTAG_assert_0 (.*);