//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLFragmenter_5_assert(
  input         clock,
  input         reset,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [2:0]  auto_in_a_bits_param,
  input  [2:0]  auto_in_a_bits_size,
  input  [6:0]  auto_in_a_bits_source,
  input  [29:0] auto_in_a_bits_address,
  input  [7:0]  auto_in_a_bits_mask,
  input         auto_in_a_bits_corrupt,
  input         auto_in_d_ready,
  input         auto_out_d_valid,
  input  [2:0]  auto_out_d_bits_opcode,
  input  [10:0] auto_out_d_bits_source,
  input         auto_out_d_bits_corrupt,
  input  [2:0]  _T_3,
  input         _T_6,
  input         _T_15,
  input  [2:0]  _T_46,
  input         _T_57,
  input         _T_64,
  input         _GEN_6,
  input         _T_68,
  input         _T_120,
  input         Repeater_io_enq_ready,
  input         Repeater_io_full,
  input  [7:0]  Repeater_io_deq_bits_mask
);
  wire  TLMonitor_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [6:0] TLMonitor_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [29:0] TLMonitor_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [6:0] TLMonitor_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  _T_132; // @[Fragmenter.scala 301:17]
  wire  _T_134; // @[Fragmenter.scala 301:35]
  wire  _T_136; // @[Fragmenter.scala 301:16]
  wire  _T_137; // @[Fragmenter.scala 301:16]
  wire  _T_139; // @[Fragmenter.scala 304:53]
  wire  _T_140; // @[Fragmenter.scala 304:35]
  wire  _T_142; // @[Fragmenter.scala 304:16]
  wire  _T_143; // @[Fragmenter.scala 304:16]
  SiFive_TLMonitor_47_assert TLMonitor ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_clock),
    .reset(TLMonitor_reset),
    .io_in_a_ready(TLMonitor_io_in_a_ready),
    .io_in_a_valid(TLMonitor_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_io_in_a_bits_corrupt),
    .io_in_d_ready(TLMonitor_io_in_d_ready),
    .io_in_d_valid(TLMonitor_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_io_in_d_bits_opcode),
    .io_in_d_bits_size(TLMonitor_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_io_in_d_bits_source),
    .io_in_d_bits_denied(TLMonitor_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_io_in_d_bits_corrupt)
  );
  assign _T_132 = ~Repeater_io_full; // @[Fragmenter.scala 301:17]
  assign _T_134 = _T_132 | _T_120; // @[Fragmenter.scala 301:35]
  assign _T_136 = _T_134 | reset; // @[Fragmenter.scala 301:16]
  assign _T_137 = ~_T_136; // @[Fragmenter.scala 301:16]
  assign _T_139 = Repeater_io_deq_bits_mask == 8'hff; // @[Fragmenter.scala 304:53]
  assign _T_140 = _T_132 | _T_139; // @[Fragmenter.scala 304:35]
  assign _T_142 = _T_140 | reset; // @[Fragmenter.scala 304:16]
  assign _T_143 = ~_T_142; // @[Fragmenter.scala 304:16]
  assign TLMonitor_clock = clock;
  assign TLMonitor_reset = reset;
  assign TLMonitor_io_in_a_ready = Repeater_io_enq_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_valid = auto_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_size = auto_in_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_source = auto_in_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_ready = auto_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_valid = auto_out_d_valid & _T_57; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_size = _T_6 ? _T_46 : _T_3; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_source = auto_out_d_bits_source[10:4]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_denied = _T_15 ? _GEN_6 : _T_64; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_corrupt = _T_15 ? _T_68 : auto_out_d_bits_corrupt; // @[Nodes.scala 26:19]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_137) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Fragmenter.scala:301 assert (!repeater.io.full || !aHasData)\n"); // @[Fragmenter.scala 301:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_137) begin
          $fatal; // @[Fragmenter.scala 301:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_143) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Fragmenter.scala:304 assert (!repeater.io.full || in_a.bits.mask === fullMask)\n"); // @[Fragmenter.scala 304:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_143) begin
          $fatal; // @[Fragmenter.scala 304:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
