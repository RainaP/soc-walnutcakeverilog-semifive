//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__SinkD(
  input          rf_reset,
  input          clock,
  input          reset,
  output         io_resp_valid,
  output         io_resp_bits_last,
  output [2:0]   io_resp_bits_opcode,
  output [2:0]   io_resp_bits_param,
  output [2:0]   io_resp_bits_source,
  output [2:0]   io_resp_bits_sink,
  output         io_resp_bits_denied,
  output         io_d_ready,
  input          io_d_valid,
  input  [2:0]   io_d_bits_opcode,
  input  [1:0]   io_d_bits_param,
  input  [2:0]   io_d_bits_size,
  input  [2:0]   io_d_bits_source,
  input  [2:0]   io_d_bits_sink,
  input          io_d_bits_denied,
  input  [127:0] io_d_bits_data,
  input          io_d_bits_corrupt,
  output [2:0]   io_source,
  input  [2:0]   io_way,
  input  [9:0]   io_set,
  input          io_bs_adr_ready,
  output         io_bs_adr_valid,
  output         io_bs_adr_bits_noop,
  output [2:0]   io_bs_adr_bits_way,
  output [9:0]   io_bs_adr_bits_set,
  output [1:0]   io_bs_adr_bits_beat,
  output [127:0] io_bs_dat_data,
  output [9:0]   io_grant_req_set,
  output [2:0]   io_grant_req_way,
  input          io_grant_safe
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
`endif // RANDOMIZE_REG_INIT
  wire  d_rf_reset; // @[Decoupled.scala 296:21]
  wire  d_clock; // @[Decoupled.scala 296:21]
  wire  d_reset; // @[Decoupled.scala 296:21]
  wire  d_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  d_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] d_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] d_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] d_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] d_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] d_io_enq_bits_sink; // @[Decoupled.scala 296:21]
  wire  d_io_enq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] d_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  d_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  d_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  d_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] d_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] d_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] d_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] d_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] d_io_deq_bits_sink; // @[Decoupled.scala 296:21]
  wire  d_io_deq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] d_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  d_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  _T = d_io_deq_ready & d_io_deq_valid; // @[Decoupled.scala 40:37]
  wire [12:0] _beats1_decode_T_1 = 13'h3f << d_io_deq_bits_size; // @[package.scala 234:77]
  wire [5:0] _beats1_decode_T_3 = ~_beats1_decode_T_1[5:0]; // @[package.scala 234:46]
  wire [1:0] beats1_decode = _beats1_decode_T_3[5:4]; // @[Edges.scala 219:59]
  wire  beats1_opdata = d_io_deq_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [1:0] beats1 = beats1_opdata ? beats1_decode : 2'h0; // @[Edges.scala 220:14]
  reg [1:0] counter; // @[Edges.scala 228:27]
  wire [1:0] counter1 = counter - 2'h1; // @[Edges.scala 229:28]
  wire  first = counter == 2'h0; // @[Edges.scala 230:25]
  wire  last = counter == 2'h1 | beats1 == 2'h0; // @[Edges.scala 231:37]
  wire [1:0] _count_T = ~counter1; // @[Edges.scala 233:27]
  wire [1:0] beat = beats1 & _count_T; // @[Edges.scala 233:25]
  reg [2:0] io_source_r; // @[Reg.scala 15:16]
  wire  _d_io_deq_ready_T = ~first; // @[SinkD.scala 66:34]
  wire  _io_bs_adr_bits_noop_T = ~d_io_deq_valid; // @[SinkD.scala 78:26]
  wire [1:0] _GEN_3 = {{1'd0}, io_bs_adr_ready}; // @[SinkD.scala 81:60]
  wire [1:0] _io_bs_adr_bits_beat_T_1 = beat + _GEN_3; // @[SinkD.scala 81:60]
  reg [1:0] io_bs_adr_bits_beat_r; // @[Reg.scala 15:16]
  RHEA__Queue_125 d ( // @[Decoupled.scala 296:21]
    .rf_reset(d_rf_reset),
    .clock(d_clock),
    .reset(d_reset),
    .io_enq_ready(d_io_enq_ready),
    .io_enq_valid(d_io_enq_valid),
    .io_enq_bits_opcode(d_io_enq_bits_opcode),
    .io_enq_bits_param(d_io_enq_bits_param),
    .io_enq_bits_size(d_io_enq_bits_size),
    .io_enq_bits_source(d_io_enq_bits_source),
    .io_enq_bits_sink(d_io_enq_bits_sink),
    .io_enq_bits_denied(d_io_enq_bits_denied),
    .io_enq_bits_data(d_io_enq_bits_data),
    .io_enq_bits_corrupt(d_io_enq_bits_corrupt),
    .io_deq_ready(d_io_deq_ready),
    .io_deq_valid(d_io_deq_valid),
    .io_deq_bits_opcode(d_io_deq_bits_opcode),
    .io_deq_bits_param(d_io_deq_bits_param),
    .io_deq_bits_size(d_io_deq_bits_size),
    .io_deq_bits_source(d_io_deq_bits_source),
    .io_deq_bits_sink(d_io_deq_bits_sink),
    .io_deq_bits_denied(d_io_deq_bits_denied),
    .io_deq_bits_data(d_io_deq_bits_data),
    .io_deq_bits_corrupt(d_io_deq_bits_corrupt)
  );
  assign d_rf_reset = rf_reset;
  assign io_resp_valid = (first | last) & _T; // @[SinkD.scala 65:36]
  assign io_resp_bits_last = counter == 2'h1 | beats1 == 2'h0; // @[Edges.scala 231:37]
  assign io_resp_bits_opcode = d_io_deq_bits_opcode; // @[SinkD.scala 72:23]
  assign io_resp_bits_param = {{1'd0}, d_io_deq_bits_param}; // @[SinkD.scala 73:23]
  assign io_resp_bits_source = d_io_deq_bits_source; // @[SinkD.scala 74:23]
  assign io_resp_bits_sink = d_io_deq_bits_sink; // @[SinkD.scala 75:23]
  assign io_resp_bits_denied = d_io_deq_bits_denied; // @[SinkD.scala 76:23]
  assign io_d_ready = d_io_enq_ready; // @[Decoupled.scala 299:17]
  assign io_source = d_io_deq_valid ? d_io_deq_bits_source : io_source_r; // @[SinkD.scala 60:19]
  assign io_bs_adr_valid = _d_io_deq_ready_T | d_io_deq_valid & io_grant_safe; // @[SinkD.scala 67:29]
  assign io_bs_adr_bits_noop = _io_bs_adr_bits_noop_T | ~beats1_opdata; // @[SinkD.scala 78:35]
  assign io_bs_adr_bits_way = io_way; // @[SinkD.scala 79:23]
  assign io_bs_adr_bits_set = io_set; // @[SinkD.scala 80:23]
  assign io_bs_adr_bits_beat = d_io_deq_valid ? beat : io_bs_adr_bits_beat_r; // @[SinkD.scala 81:29]
  assign io_bs_dat_data = d_io_deq_bits_data; // @[SinkD.scala 83:23]
  assign io_grant_req_set = io_set; // @[SinkD.scala 62:20]
  assign io_grant_req_way = io_way; // @[SinkD.scala 61:20]
  assign d_clock = clock;
  assign d_reset = reset;
  assign d_io_enq_valid = io_d_valid; // @[Decoupled.scala 297:22]
  assign d_io_enq_bits_opcode = io_d_bits_opcode; // @[Decoupled.scala 298:21]
  assign d_io_enq_bits_param = io_d_bits_param; // @[Decoupled.scala 298:21]
  assign d_io_enq_bits_size = io_d_bits_size; // @[Decoupled.scala 298:21]
  assign d_io_enq_bits_source = io_d_bits_source; // @[Decoupled.scala 298:21]
  assign d_io_enq_bits_sink = io_d_bits_sink; // @[Decoupled.scala 298:21]
  assign d_io_enq_bits_denied = io_d_bits_denied; // @[Decoupled.scala 298:21]
  assign d_io_enq_bits_data = io_d_bits_data; // @[Decoupled.scala 298:21]
  assign d_io_enq_bits_corrupt = io_d_bits_corrupt; // @[Decoupled.scala 298:21]
  assign d_io_deq_ready = io_bs_adr_ready & (~first | io_grant_safe); // @[SinkD.scala 66:30]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      counter <= 2'h0;
    end else if (_T) begin
      if (first) begin
        if (beats1_opdata) begin
          counter <= beats1_decode;
        end else begin
          counter <= 2'h0;
        end
      end else begin
        counter <= counter1;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      io_source_r <= 3'h0;
    end else if (d_io_deq_valid) begin
      io_source_r <= d_io_deq_bits_source;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      io_bs_adr_bits_beat_r <= 2'h0;
    end else if (d_io_deq_valid) begin
      io_bs_adr_bits_beat_r <= _io_bs_adr_bits_beat_T_1;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  counter = _RAND_0[1:0];
  _RAND_1 = {1{`RANDOM}};
  io_source_r = _RAND_1[2:0];
  _RAND_2 = {1{`RANDOM}};
  io_bs_adr_bits_beat_r = _RAND_2[1:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    counter = 2'h0;
  end
  if (rf_reset) begin
    io_source_r = 3'h0;
  end
  if (rf_reset) begin
    io_bs_adr_bits_beat_r = 2'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
