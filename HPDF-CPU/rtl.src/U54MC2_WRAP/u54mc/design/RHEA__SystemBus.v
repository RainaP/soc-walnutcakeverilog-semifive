//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__SystemBus(
  input          rf_reset,
  input          auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_ready,
  output         auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_valid,
  output [7:0]   auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_bits_id,
  output [36:0]  auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_bits_addr,
  output [7:0]   auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_bits_len,
  output [2:0]   auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_bits_size,
  output [3:0]   auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_bits_cache,
  output [2:0]   auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_bits_prot,
  input          auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_w_ready,
  output         auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_w_valid,
  output [127:0] auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_w_bits_data,
  output [15:0]  auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_w_bits_strb,
  output         auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_w_bits_last,
  output         auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_b_ready,
  input          auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_b_valid,
  input  [7:0]   auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_b_bits_id,
  input  [1:0]   auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_b_bits_resp,
  input          auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_ready,
  output         auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_valid,
  output [7:0]   auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_bits_id,
  output [36:0]  auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_bits_addr,
  output [7:0]   auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_bits_len,
  output [2:0]   auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_bits_size,
  output [3:0]   auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_bits_cache,
  output [2:0]   auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_bits_prot,
  output         auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_r_ready,
  input          auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_r_valid,
  input  [7:0]   auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_r_bits_id,
  input  [127:0] auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_r_bits_data,
  input  [1:0]   auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_r_bits_resp,
  input          auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_r_bits_last,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_opcode,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_param,
  input  [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_size,
  input  [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_source,
  input  [36:0]  auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_address,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_user_amba_prot_bufferable,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_user_amba_prot_modifiable,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_user_amba_prot_readalloc,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_user_amba_prot_writealloc,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_user_amba_prot_privileged,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_user_amba_prot_secure,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_user_amba_prot_fetch,
  input  [15:0]  auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_mask,
  input  [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_data,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_corrupt,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_opcode,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_param,
  input  [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_size,
  input  [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_source,
  input  [36:0]  auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_address,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_user_amba_prot_bufferable,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_user_amba_prot_modifiable,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_user_amba_prot_readalloc,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_user_amba_prot_writealloc,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_user_amba_prot_privileged,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_user_amba_prot_secure,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_user_amba_prot_fetch,
  input  [15:0]  auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_mask,
  input  [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_data,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_corrupt,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_a_valid,
  input  [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_a_source,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_1_a_ready,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_a_sink,
  output [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_opcode,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_param,
  output [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_size,
  output [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_source,
  output [36:0]  auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_address,
  output [15:0]  auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_mask,
  output [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_data,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_corrupt,
  output [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_opcode,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_param,
  output [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_size,
  output [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_source,
  output [36:0]  auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_address,
  output [15:0]  auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_mask,
  output [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_data,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_corrupt,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_1_b_valid,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_b_source,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_b_ready,
  input  [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_b_sink,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits0_opcode,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits0_param,
  input  [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits0_size,
  input  [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits0_source,
  input  [36:0]  auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits0_address,
  input  [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits0_data,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits0_corrupt,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits1_opcode,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits1_param,
  input  [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits1_size,
  input  [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits1_source,
  input  [36:0]  auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits1_address,
  input  [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits1_data,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits1_corrupt,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_c_valid,
  input  [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_c_source,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_1_c_ready,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_c_sink,
  output [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_opcode,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_param,
  output [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_size,
  output [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_source,
  output [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_sink,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_denied,
  output [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_data,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_corrupt,
  output [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_opcode,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_param,
  output [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_size,
  output [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_source,
  output [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_sink,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_denied,
  output [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_data,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_corrupt,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_1_d_valid,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_d_source,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_d_ready,
  input  [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_d_sink,
  input  [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_e_bits0_sink,
  input  [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_e_bits1_sink,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_1_e_valid,
  input  [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_e_source,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_1_e_ready,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_1_e_sink,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_opcode,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_param,
  input  [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_size,
  input  [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_source,
  input  [36:0]  auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_address,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_user_amba_prot_bufferable,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_user_amba_prot_modifiable,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_user_amba_prot_readalloc,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_user_amba_prot_writealloc,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_user_amba_prot_privileged,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_user_amba_prot_secure,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_user_amba_prot_fetch,
  input  [15:0]  auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_mask,
  input  [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_data,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_corrupt,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_opcode,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_param,
  input  [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_size,
  input  [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_source,
  input  [36:0]  auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_address,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_user_amba_prot_bufferable,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_user_amba_prot_modifiable,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_user_amba_prot_readalloc,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_user_amba_prot_writealloc,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_user_amba_prot_privileged,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_user_amba_prot_secure,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_user_amba_prot_fetch,
  input  [15:0]  auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_mask,
  input  [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_data,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_corrupt,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_a_valid,
  input  [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_a_source,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_0_a_ready,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_a_sink,
  output [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_opcode,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_param,
  output [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_size,
  output [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_source,
  output [36:0]  auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_address,
  output [15:0]  auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_mask,
  output [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_data,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_corrupt,
  output [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_opcode,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_param,
  output [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_size,
  output [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_source,
  output [36:0]  auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_address,
  output [15:0]  auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_mask,
  output [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_data,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_corrupt,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_0_b_valid,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_b_source,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_b_ready,
  input  [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_b_sink,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits0_opcode,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits0_param,
  input  [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits0_size,
  input  [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits0_source,
  input  [36:0]  auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits0_address,
  input  [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits0_data,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits0_corrupt,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits1_opcode,
  input  [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits1_param,
  input  [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits1_size,
  input  [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits1_source,
  input  [36:0]  auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits1_address,
  input  [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits1_data,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits1_corrupt,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_c_valid,
  input  [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_c_source,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_0_c_ready,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_c_sink,
  output [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_opcode,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_param,
  output [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_size,
  output [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_source,
  output [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_sink,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_denied,
  output [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_data,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_corrupt,
  output [2:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_opcode,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_param,
  output [3:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_size,
  output [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_source,
  output [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_sink,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_denied,
  output [127:0] auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_data,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_corrupt,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_0_d_valid,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_d_source,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_d_ready,
  input  [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_d_sink,
  input  [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_e_bits0_sink,
  input  [4:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_e_bits1_sink,
  input          auto_coupler_from_tile_tl_master_clock_xing_in_0_e_valid,
  input  [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_e_source,
  output         auto_coupler_from_tile_tl_master_clock_xing_in_0_e_ready,
  output [1:0]   auto_coupler_from_tile_tl_master_clock_xing_in_0_e_sink,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_ready,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_opcode,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_param,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_size,
  output [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_source,
  output [35:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_address,
  output [15:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_mask,
  output [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_data,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_corrupt,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_ready,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_valid,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_opcode,
  input  [1:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_param,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_size,
  input  [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_source,
  input  [35:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_address,
  input  [15:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_mask,
  input  [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_data,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_corrupt,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_ready,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_bits_opcode,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_bits_param,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_bits_size,
  output [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_bits_source,
  output [35:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_bits_address,
  output [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_bits_data,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_bits_corrupt,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_ready,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_valid,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_opcode,
  input  [1:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_param,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_size,
  input  [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_source,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_sink,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_denied,
  input  [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_data,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_corrupt,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_e_ready,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_e_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_e_bits_sink,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_ready,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_opcode,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_param,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_size,
  output [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_source,
  output [35:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_address,
  output [15:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_mask,
  output [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_data,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_corrupt,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_ready,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_valid,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_opcode,
  input  [1:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_param,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_size,
  input  [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_source,
  input  [35:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_address,
  input  [15:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_mask,
  input  [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_data,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_corrupt,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_ready,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_bits_opcode,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_bits_param,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_bits_size,
  output [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_bits_source,
  output [35:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_bits_address,
  output [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_bits_data,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_bits_corrupt,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_ready,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_valid,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_opcode,
  input  [1:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_param,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_size,
  input  [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_source,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_sink,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_denied,
  input  [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_data,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_corrupt,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_e_ready,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_e_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_e_bits_sink,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_ready,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_opcode,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_param,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_size,
  output [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_source,
  output [35:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_address,
  output [15:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_mask,
  output [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_data,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_corrupt,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_ready,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_valid,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_opcode,
  input  [1:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_param,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_size,
  input  [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_source,
  input  [35:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_address,
  input  [15:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_mask,
  input  [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_data,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_corrupt,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_ready,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_bits_opcode,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_bits_param,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_bits_size,
  output [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_bits_source,
  output [35:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_bits_address,
  output [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_bits_data,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_bits_corrupt,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_ready,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_valid,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_opcode,
  input  [1:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_param,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_size,
  input  [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_source,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_sink,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_denied,
  input  [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_data,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_corrupt,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_e_ready,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_e_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_e_bits_sink,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_ready,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_opcode,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_param,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_size,
  output [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_source,
  output [35:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_address,
  output [15:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_mask,
  output [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_data,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_corrupt,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_ready,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_valid,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_opcode,
  input  [1:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_param,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_size,
  input  [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_source,
  input  [35:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_address,
  input  [15:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_mask,
  input  [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_data,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_corrupt,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_ready,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_bits_opcode,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_bits_param,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_bits_size,
  output [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_bits_source,
  output [35:0]  auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_bits_address,
  output [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_bits_data,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_bits_corrupt,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_ready,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_valid,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_opcode,
  input  [1:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_param,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_size,
  input  [6:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_source,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_sink,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_denied,
  input  [127:0] auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_data,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_corrupt,
  input          auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_e_ready,
  output         auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_e_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_e_bits_sink,
  output         auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_ready,
  input          auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_valid,
  input  [2:0]   auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_opcode,
  input  [3:0]   auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_size,
  input  [5:0]   auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_source,
  input  [36:0]  auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_address,
  input          auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_user_amba_prot_bufferable,
  input          auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_user_amba_prot_modifiable,
  input          auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_user_amba_prot_readalloc,
  input          auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_user_amba_prot_writealloc,
  input          auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_user_amba_prot_privileged,
  input          auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_user_amba_prot_secure,
  input          auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_user_amba_prot_fetch,
  input  [7:0]   auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_mask,
  input  [63:0]  auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_data,
  input          auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_ready,
  output         auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_valid,
  output [2:0]   auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_opcode,
  output [1:0]   auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_param,
  output [3:0]   auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_size,
  output [5:0]   auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_source,
  output [4:0]   auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_sink,
  output         auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_denied,
  output [63:0]  auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_data,
  output         auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_corrupt,
  input          auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_ready,
  output         auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_opcode,
  output [2:0]   auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_param,
  output [3:0]   auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_size,
  output [6:0]   auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_source,
  output [31:0]  auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_address,
  output         auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_user_amba_prot_bufferable
    ,
  output         auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_user_amba_prot_modifiable
    ,
  output         auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_user_amba_prot_readalloc,
  output         auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_user_amba_prot_writealloc
    ,
  output         auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_user_amba_prot_privileged
    ,
  output         auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_user_amba_prot_secure,
  output         auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_user_amba_prot_fetch,
  output [7:0]   auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_mask,
  output [63:0]  auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_data,
  output         auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_corrupt,
  output         auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_ready,
  input          auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_valid,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_opcode,
  input  [1:0]   auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_param,
  input  [3:0]   auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_size,
  input  [6:0]   auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_source,
  input          auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_sink,
  input          auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_denied,
  input  [63:0]  auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_data,
  input          auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_corrupt,
  input          auto_coupler_to_l2_lim_buffer_out_a_ready,
  output         auto_coupler_to_l2_lim_buffer_out_a_valid,
  output [2:0]   auto_coupler_to_l2_lim_buffer_out_a_bits_opcode,
  output [2:0]   auto_coupler_to_l2_lim_buffer_out_a_bits_param,
  output [2:0]   auto_coupler_to_l2_lim_buffer_out_a_bits_size,
  output [9:0]   auto_coupler_to_l2_lim_buffer_out_a_bits_source,
  output [27:0]  auto_coupler_to_l2_lim_buffer_out_a_bits_address,
  output [15:0]  auto_coupler_to_l2_lim_buffer_out_a_bits_mask,
  output [127:0] auto_coupler_to_l2_lim_buffer_out_a_bits_data,
  output         auto_coupler_to_l2_lim_buffer_out_a_bits_corrupt,
  output         auto_coupler_to_l2_lim_buffer_out_d_ready,
  input          auto_coupler_to_l2_lim_buffer_out_d_valid,
  input  [2:0]   auto_coupler_to_l2_lim_buffer_out_d_bits_opcode,
  input  [2:0]   auto_coupler_to_l2_lim_buffer_out_d_bits_size,
  input  [9:0]   auto_coupler_to_l2_lim_buffer_out_d_bits_source,
  input  [127:0] auto_coupler_to_l2_lim_buffer_out_d_bits_data,
  input          auto_coupler_to_l2_lim_buffer_out_d_bits_corrupt,
  output         auto_clockGroup_out_1_clock,
  output         auto_clockGroup_out_1_reset,
  output         auto_clockGroup_out_0_clock,
  output         auto_clockGroup_out_0_reset,
  input          auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_9_clock,
  input          auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_9_reset,
  input          auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_7_clock,
  input          auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_7_reset,
  input          auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_6_clock,
  input          auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_6_reset,
  input          auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_4_clock,
  input          auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_4_reset,
  input          auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_core_1_clock,
  input          auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_core_1_reset,
  input          auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_core_0_clock,
  input          auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_core_0_reset,
  input          auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_0_clock,
  input          auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_0_reset,
  output         auto_subsystem_sbus_clock_groups_out_2_member_subsystem_l2_2_clock,
  output         auto_subsystem_sbus_clock_groups_out_2_member_subsystem_l2_2_reset,
  output         auto_subsystem_sbus_clock_groups_out_2_member_subsystem_l2_0_clock,
  output         auto_subsystem_sbus_clock_groups_out_2_member_subsystem_l2_0_reset,
  output         auto_subsystem_sbus_clock_groups_out_1_member_subsystem_fbus_0_clock,
  output         auto_subsystem_sbus_clock_groups_out_1_member_subsystem_fbus_0_reset,
  output         auto_subsystem_sbus_clock_groups_out_0_member_subsystem_cbus_address_adjuster_1_clock,
  output         auto_subsystem_sbus_clock_groups_out_0_member_subsystem_cbus_address_adjuster_1_reset
);
  wire  subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_9_clock;
  wire  subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_9_reset;
  wire  subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_7_clock;
  wire  subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_7_reset;
  wire  subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_6_clock;
  wire  subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_6_reset;
  wire  subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_4_clock;
  wire  subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_4_reset;
  wire  subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_core_1_clock;
  wire  subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_core_1_reset;
  wire  subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_core_0_clock;
  wire  subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_core_0_reset;
  wire  subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_0_clock;
  wire  subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_0_reset;
  wire  subsystem_sbus_clock_groups_auto_out_3_member_subsystem_l2_2_clock;
  wire  subsystem_sbus_clock_groups_auto_out_3_member_subsystem_l2_2_reset;
  wire  subsystem_sbus_clock_groups_auto_out_3_member_subsystem_l2_0_clock;
  wire  subsystem_sbus_clock_groups_auto_out_3_member_subsystem_l2_0_reset;
  wire  subsystem_sbus_clock_groups_auto_out_2_member_subsystem_fbus_0_clock;
  wire  subsystem_sbus_clock_groups_auto_out_2_member_subsystem_fbus_0_reset;
  wire  subsystem_sbus_clock_groups_auto_out_1_member_subsystem_cbus_address_adjuster_1_clock;
  wire  subsystem_sbus_clock_groups_auto_out_1_member_subsystem_cbus_address_adjuster_1_reset;
  wire  subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_core_1_clock;
  wire  subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_core_1_reset;
  wire  subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_core_0_clock;
  wire  subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_core_0_reset;
  wire  subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_0_clock;
  wire  subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_0_reset;
  wire  clockGroup_auto_in_member_subsystem_sbus_core_1_clock;
  wire  clockGroup_auto_in_member_subsystem_sbus_core_1_reset;
  wire  clockGroup_auto_in_member_subsystem_sbus_core_0_clock;
  wire  clockGroup_auto_in_member_subsystem_sbus_core_0_reset;
  wire  clockGroup_auto_in_member_subsystem_sbus_0_clock;
  wire  clockGroup_auto_in_member_subsystem_sbus_0_reset;
  wire  clockGroup_auto_out_2_clock;
  wire  clockGroup_auto_out_2_reset;
  wire  clockGroup_auto_out_1_clock;
  wire  clockGroup_auto_out_1_reset;
  wire  clockGroup_auto_out_0_clock;
  wire  clockGroup_auto_out_0_reset;
  wire  fixedClockNode_auto_in_clock; // @[ClockGroup.scala 106:107]
  wire  fixedClockNode_auto_in_reset; // @[ClockGroup.scala 106:107]
  wire  fixedClockNode_auto_out_0_clock; // @[ClockGroup.scala 106:107]
  wire  fixedClockNode_auto_out_0_reset; // @[ClockGroup.scala 106:107]
  wire  system_bus_xbar_rf_reset; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_clock; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_reset; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_a_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_a_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_in_2_a_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_in_2_a_bits_param; // @[SystemBus.scala 40:43]
  wire [3:0] system_bus_xbar_auto_in_2_a_bits_size; // @[SystemBus.scala 40:43]
  wire [4:0] system_bus_xbar_auto_in_2_a_bits_source; // @[SystemBus.scala 40:43]
  wire [36:0] system_bus_xbar_auto_in_2_a_bits_address; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_a_bits_user_amba_prot_bufferable; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_a_bits_user_amba_prot_modifiable; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_a_bits_user_amba_prot_readalloc; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_a_bits_user_amba_prot_writealloc; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_a_bits_user_amba_prot_privileged; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_a_bits_user_amba_prot_secure; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_a_bits_user_amba_prot_fetch; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_in_2_a_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_in_2_a_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_a_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_b_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_b_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_in_2_b_bits_opcode; // @[SystemBus.scala 40:43]
  wire [1:0] system_bus_xbar_auto_in_2_b_bits_param; // @[SystemBus.scala 40:43]
  wire [3:0] system_bus_xbar_auto_in_2_b_bits_size; // @[SystemBus.scala 40:43]
  wire [4:0] system_bus_xbar_auto_in_2_b_bits_source; // @[SystemBus.scala 40:43]
  wire [36:0] system_bus_xbar_auto_in_2_b_bits_address; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_in_2_b_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_in_2_b_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_b_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_c_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_c_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_in_2_c_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_in_2_c_bits_param; // @[SystemBus.scala 40:43]
  wire [3:0] system_bus_xbar_auto_in_2_c_bits_size; // @[SystemBus.scala 40:43]
  wire [4:0] system_bus_xbar_auto_in_2_c_bits_source; // @[SystemBus.scala 40:43]
  wire [36:0] system_bus_xbar_auto_in_2_c_bits_address; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_in_2_c_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_c_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_d_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_d_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_in_2_d_bits_opcode; // @[SystemBus.scala 40:43]
  wire [1:0] system_bus_xbar_auto_in_2_d_bits_param; // @[SystemBus.scala 40:43]
  wire [3:0] system_bus_xbar_auto_in_2_d_bits_size; // @[SystemBus.scala 40:43]
  wire [4:0] system_bus_xbar_auto_in_2_d_bits_source; // @[SystemBus.scala 40:43]
  wire [4:0] system_bus_xbar_auto_in_2_d_bits_sink; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_d_bits_denied; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_in_2_d_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_d_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_e_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_2_e_valid; // @[SystemBus.scala 40:43]
  wire [4:0] system_bus_xbar_auto_in_2_e_bits_sink; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_a_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_a_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_in_1_a_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_in_1_a_bits_param; // @[SystemBus.scala 40:43]
  wire [3:0] system_bus_xbar_auto_in_1_a_bits_size; // @[SystemBus.scala 40:43]
  wire [4:0] system_bus_xbar_auto_in_1_a_bits_source; // @[SystemBus.scala 40:43]
  wire [36:0] system_bus_xbar_auto_in_1_a_bits_address; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_a_bits_user_amba_prot_bufferable; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_a_bits_user_amba_prot_modifiable; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_a_bits_user_amba_prot_readalloc; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_a_bits_user_amba_prot_writealloc; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_a_bits_user_amba_prot_privileged; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_a_bits_user_amba_prot_secure; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_a_bits_user_amba_prot_fetch; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_in_1_a_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_in_1_a_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_a_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_b_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_b_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_in_1_b_bits_opcode; // @[SystemBus.scala 40:43]
  wire [1:0] system_bus_xbar_auto_in_1_b_bits_param; // @[SystemBus.scala 40:43]
  wire [3:0] system_bus_xbar_auto_in_1_b_bits_size; // @[SystemBus.scala 40:43]
  wire [4:0] system_bus_xbar_auto_in_1_b_bits_source; // @[SystemBus.scala 40:43]
  wire [36:0] system_bus_xbar_auto_in_1_b_bits_address; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_in_1_b_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_in_1_b_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_b_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_c_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_c_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_in_1_c_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_in_1_c_bits_param; // @[SystemBus.scala 40:43]
  wire [3:0] system_bus_xbar_auto_in_1_c_bits_size; // @[SystemBus.scala 40:43]
  wire [4:0] system_bus_xbar_auto_in_1_c_bits_source; // @[SystemBus.scala 40:43]
  wire [36:0] system_bus_xbar_auto_in_1_c_bits_address; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_in_1_c_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_c_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_d_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_d_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_in_1_d_bits_opcode; // @[SystemBus.scala 40:43]
  wire [1:0] system_bus_xbar_auto_in_1_d_bits_param; // @[SystemBus.scala 40:43]
  wire [3:0] system_bus_xbar_auto_in_1_d_bits_size; // @[SystemBus.scala 40:43]
  wire [4:0] system_bus_xbar_auto_in_1_d_bits_source; // @[SystemBus.scala 40:43]
  wire [4:0] system_bus_xbar_auto_in_1_d_bits_sink; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_d_bits_denied; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_in_1_d_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_d_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_e_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_1_e_valid; // @[SystemBus.scala 40:43]
  wire [4:0] system_bus_xbar_auto_in_1_e_bits_sink; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_0_a_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_0_a_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_in_0_a_bits_opcode; // @[SystemBus.scala 40:43]
  wire [3:0] system_bus_xbar_auto_in_0_a_bits_size; // @[SystemBus.scala 40:43]
  wire [5:0] system_bus_xbar_auto_in_0_a_bits_source; // @[SystemBus.scala 40:43]
  wire [36:0] system_bus_xbar_auto_in_0_a_bits_address; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_0_a_bits_user_amba_prot_bufferable; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_0_a_bits_user_amba_prot_modifiable; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_0_a_bits_user_amba_prot_readalloc; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_0_a_bits_user_amba_prot_writealloc; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_0_a_bits_user_amba_prot_privileged; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_0_a_bits_user_amba_prot_secure; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_0_a_bits_user_amba_prot_fetch; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_in_0_a_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_in_0_a_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_0_d_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_0_d_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_in_0_d_bits_opcode; // @[SystemBus.scala 40:43]
  wire [1:0] system_bus_xbar_auto_in_0_d_bits_param; // @[SystemBus.scala 40:43]
  wire [3:0] system_bus_xbar_auto_in_0_d_bits_size; // @[SystemBus.scala 40:43]
  wire [5:0] system_bus_xbar_auto_in_0_d_bits_source; // @[SystemBus.scala 40:43]
  wire [4:0] system_bus_xbar_auto_in_0_d_bits_sink; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_0_d_bits_denied; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_in_0_d_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_in_0_d_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_6_a_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_6_a_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_6_a_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_6_a_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_6_a_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_6_a_bits_source; // @[SystemBus.scala 40:43]
  wire [36:0] system_bus_xbar_auto_out_6_a_bits_address; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_6_a_bits_user_amba_prot_bufferable; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_6_a_bits_user_amba_prot_modifiable; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_6_a_bits_user_amba_prot_readalloc; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_6_a_bits_user_amba_prot_writealloc; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_6_a_bits_user_amba_prot_privileged; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_6_a_bits_user_amba_prot_secure; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_6_a_bits_user_amba_prot_fetch; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_out_6_a_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_6_a_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_6_a_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_6_d_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_6_d_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_6_d_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_6_d_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_6_d_bits_source; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_6_d_bits_denied; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_6_d_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_6_d_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_5_a_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_5_a_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_5_a_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_5_a_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_5_a_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_5_a_bits_source; // @[SystemBus.scala 40:43]
  wire [35:0] system_bus_xbar_auto_out_5_a_bits_address; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_out_5_a_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_5_a_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_5_a_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_5_b_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_5_b_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_5_b_bits_opcode; // @[SystemBus.scala 40:43]
  wire [1:0] system_bus_xbar_auto_out_5_b_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_5_b_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_5_b_bits_source; // @[SystemBus.scala 40:43]
  wire [35:0] system_bus_xbar_auto_out_5_b_bits_address; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_out_5_b_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_5_b_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_5_b_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_5_c_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_5_c_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_5_c_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_5_c_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_5_c_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_5_c_bits_source; // @[SystemBus.scala 40:43]
  wire [35:0] system_bus_xbar_auto_out_5_c_bits_address; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_5_c_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_5_c_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_5_d_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_5_d_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_5_d_bits_opcode; // @[SystemBus.scala 40:43]
  wire [1:0] system_bus_xbar_auto_out_5_d_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_5_d_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_5_d_bits_source; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_5_d_bits_sink; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_5_d_bits_denied; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_5_d_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_5_d_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_5_e_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_5_e_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_5_e_bits_sink; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_4_a_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_4_a_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_4_a_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_4_a_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_4_a_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_4_a_bits_source; // @[SystemBus.scala 40:43]
  wire [35:0] system_bus_xbar_auto_out_4_a_bits_address; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_out_4_a_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_4_a_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_4_a_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_4_b_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_4_b_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_4_b_bits_opcode; // @[SystemBus.scala 40:43]
  wire [1:0] system_bus_xbar_auto_out_4_b_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_4_b_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_4_b_bits_source; // @[SystemBus.scala 40:43]
  wire [35:0] system_bus_xbar_auto_out_4_b_bits_address; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_out_4_b_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_4_b_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_4_b_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_4_c_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_4_c_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_4_c_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_4_c_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_4_c_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_4_c_bits_source; // @[SystemBus.scala 40:43]
  wire [35:0] system_bus_xbar_auto_out_4_c_bits_address; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_4_c_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_4_c_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_4_d_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_4_d_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_4_d_bits_opcode; // @[SystemBus.scala 40:43]
  wire [1:0] system_bus_xbar_auto_out_4_d_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_4_d_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_4_d_bits_source; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_4_d_bits_sink; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_4_d_bits_denied; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_4_d_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_4_d_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_4_e_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_4_e_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_4_e_bits_sink; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_3_a_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_3_a_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_3_a_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_3_a_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_3_a_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_3_a_bits_source; // @[SystemBus.scala 40:43]
  wire [35:0] system_bus_xbar_auto_out_3_a_bits_address; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_out_3_a_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_3_a_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_3_a_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_3_b_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_3_b_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_3_b_bits_opcode; // @[SystemBus.scala 40:43]
  wire [1:0] system_bus_xbar_auto_out_3_b_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_3_b_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_3_b_bits_source; // @[SystemBus.scala 40:43]
  wire [35:0] system_bus_xbar_auto_out_3_b_bits_address; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_out_3_b_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_3_b_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_3_b_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_3_c_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_3_c_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_3_c_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_3_c_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_3_c_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_3_c_bits_source; // @[SystemBus.scala 40:43]
  wire [35:0] system_bus_xbar_auto_out_3_c_bits_address; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_3_c_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_3_c_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_3_d_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_3_d_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_3_d_bits_opcode; // @[SystemBus.scala 40:43]
  wire [1:0] system_bus_xbar_auto_out_3_d_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_3_d_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_3_d_bits_source; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_3_d_bits_sink; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_3_d_bits_denied; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_3_d_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_3_d_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_3_e_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_3_e_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_3_e_bits_sink; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_2_a_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_2_a_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_2_a_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_2_a_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_2_a_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_2_a_bits_source; // @[SystemBus.scala 40:43]
  wire [35:0] system_bus_xbar_auto_out_2_a_bits_address; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_out_2_a_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_2_a_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_2_a_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_2_b_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_2_b_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_2_b_bits_opcode; // @[SystemBus.scala 40:43]
  wire [1:0] system_bus_xbar_auto_out_2_b_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_2_b_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_2_b_bits_source; // @[SystemBus.scala 40:43]
  wire [35:0] system_bus_xbar_auto_out_2_b_bits_address; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_out_2_b_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_2_b_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_2_b_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_2_c_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_2_c_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_2_c_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_2_c_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_2_c_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_2_c_bits_source; // @[SystemBus.scala 40:43]
  wire [35:0] system_bus_xbar_auto_out_2_c_bits_address; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_2_c_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_2_c_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_2_d_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_2_d_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_2_d_bits_opcode; // @[SystemBus.scala 40:43]
  wire [1:0] system_bus_xbar_auto_out_2_d_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_2_d_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_2_d_bits_source; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_2_d_bits_sink; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_2_d_bits_denied; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_2_d_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_2_d_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_2_e_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_2_e_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_2_e_bits_sink; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_1_a_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_1_a_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_1_a_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_1_a_bits_param; // @[SystemBus.scala 40:43]
  wire [3:0] system_bus_xbar_auto_out_1_a_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_1_a_bits_source; // @[SystemBus.scala 40:43]
  wire [31:0] system_bus_xbar_auto_out_1_a_bits_address; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_1_a_bits_user_amba_prot_bufferable; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_1_a_bits_user_amba_prot_modifiable; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_1_a_bits_user_amba_prot_readalloc; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_1_a_bits_user_amba_prot_writealloc; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_1_a_bits_user_amba_prot_privileged; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_1_a_bits_user_amba_prot_secure; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_1_a_bits_user_amba_prot_fetch; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_out_1_a_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_1_a_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_1_a_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_1_d_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_1_d_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_1_d_bits_opcode; // @[SystemBus.scala 40:43]
  wire [1:0] system_bus_xbar_auto_out_1_d_bits_param; // @[SystemBus.scala 40:43]
  wire [3:0] system_bus_xbar_auto_out_1_d_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_1_d_bits_source; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_1_d_bits_sink; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_1_d_bits_denied; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_1_d_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_1_d_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_0_a_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_0_a_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_0_a_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_0_a_bits_param; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_0_a_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_0_a_bits_source; // @[SystemBus.scala 40:43]
  wire [27:0] system_bus_xbar_auto_out_0_a_bits_address; // @[SystemBus.scala 40:43]
  wire [15:0] system_bus_xbar_auto_out_0_a_bits_mask; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_0_a_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_0_a_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_0_d_ready; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_0_d_valid; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_0_d_bits_opcode; // @[SystemBus.scala 40:43]
  wire [2:0] system_bus_xbar_auto_out_0_d_bits_size; // @[SystemBus.scala 40:43]
  wire [6:0] system_bus_xbar_auto_out_0_d_bits_source; // @[SystemBus.scala 40:43]
  wire [127:0] system_bus_xbar_auto_out_0_d_bits_data; // @[SystemBus.scala 40:43]
  wire  system_bus_xbar_auto_out_0_d_bits_corrupt; // @[SystemBus.scala 40:43]
  wire  fixer_rf_reset; // @[FIFOFixer.scala 144:27]
  wire  fixer_clock; // @[FIFOFixer.scala 144:27]
  wire  fixer_reset; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_2_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_2_a_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_2_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_2_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_in_2_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_bits_user_amba_prot_fetch; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_in_2_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_2_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_b_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_b_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_2_b_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_in_2_b_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_2_b_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_2_b_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_in_2_b_bits_address; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_in_2_b_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_2_b_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_b_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_c_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_c_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_2_c_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_2_c_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_2_c_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_2_c_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_in_2_c_bits_address; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_2_c_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_c_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_2_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_in_2_d_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_2_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_2_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_2_d_bits_sink; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_2_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_e_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_e_valid; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_2_e_bits_sink; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_1_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_1_a_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_1_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_1_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_in_1_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_bits_user_amba_prot_fetch; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_in_1_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_1_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_b_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_b_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_1_b_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_in_1_b_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_1_b_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_1_b_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_in_1_b_bits_address; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_in_1_b_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_1_b_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_b_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_c_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_c_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_1_c_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_1_c_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_1_c_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_1_c_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_in_1_c_bits_address; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_1_c_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_c_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_1_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_in_1_d_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_1_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_1_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_1_d_bits_sink; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_1_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_e_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_e_valid; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_1_e_bits_sink; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_0_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_0_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [5:0] fixer_auto_in_0_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_in_0_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_bits_user_amba_prot_fetch; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_in_0_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_0_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_0_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_in_0_d_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_0_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [5:0] fixer_auto_in_0_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_0_d_bits_sink; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_0_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_2_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_2_a_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_2_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_2_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_out_2_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_bits_user_amba_prot_fetch; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_out_2_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_2_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_b_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_b_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_2_b_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_out_2_b_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_2_b_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_2_b_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_out_2_b_bits_address; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_out_2_b_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_2_b_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_b_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_c_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_c_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_2_c_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_2_c_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_2_c_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_2_c_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_out_2_c_bits_address; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_2_c_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_c_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_2_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_out_2_d_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_2_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_2_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_2_d_bits_sink; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_2_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_e_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_e_valid; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_2_e_bits_sink; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_1_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_1_a_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_1_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_1_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_out_1_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_bits_user_amba_prot_fetch; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_out_1_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_1_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_b_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_b_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_1_b_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_out_1_b_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_1_b_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_1_b_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_out_1_b_bits_address; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_out_1_b_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_1_b_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_b_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_c_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_c_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_1_c_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_1_c_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_1_c_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_1_c_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_out_1_c_bits_address; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_1_c_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_c_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_1_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_out_1_d_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_1_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_1_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_1_d_bits_sink; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_1_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_e_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_e_valid; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_1_e_bits_sink; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_0_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_0_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [5:0] fixer_auto_out_0_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_out_0_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_bits_user_amba_prot_fetch; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_out_0_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_0_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_0_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_out_0_d_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_0_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [5:0] fixer_auto_out_0_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_0_d_bits_sink; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_0_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  coupler_to_l2_lim_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_lim_clock; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_lim_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_lim_auto_buffer_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_lim_auto_buffer_out_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_lim_auto_buffer_out_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_lim_auto_buffer_out_a_bits_param; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_lim_auto_buffer_out_a_bits_size; // @[LazyModule.scala 524:27]
  wire [9:0] coupler_to_l2_lim_auto_buffer_out_a_bits_source; // @[LazyModule.scala 524:27]
  wire [27:0] coupler_to_l2_lim_auto_buffer_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_to_l2_lim_auto_buffer_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_to_l2_lim_auto_buffer_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_lim_auto_buffer_out_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_lim_auto_buffer_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_lim_auto_buffer_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_lim_auto_buffer_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_lim_auto_buffer_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [9:0] coupler_to_l2_lim_auto_buffer_out_d_bits_source; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_to_l2_lim_auto_buffer_out_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_lim_auto_buffer_out_d_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_lim_auto_tl_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_lim_auto_tl_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_lim_auto_tl_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_lim_auto_tl_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_lim_auto_tl_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_l2_lim_auto_tl_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [27:0] coupler_to_l2_lim_auto_tl_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_to_l2_lim_auto_tl_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_to_l2_lim_auto_tl_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_lim_auto_tl_in_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_lim_auto_tl_in_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_lim_auto_tl_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_lim_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_lim_auto_tl_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_l2_lim_auto_tl_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_to_l2_lim_auto_tl_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_lim_auto_tl_in_d_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_clock; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [31:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_secure; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_sink; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_denied; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_source; // @[LazyModule.scala 524:27]
  wire [31:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_source; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_sink; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_denied; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_clock; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_reset; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_size; // @[LazyModule.scala 524:27]
  wire [5:0] coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [5:0] coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_source; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_denied; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [5:0] coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_secure; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [5:0] coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_denied; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_3_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_3_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_3_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_2_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_2_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_2_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_1_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_1_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_1_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_0_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_widget_in_0_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_widget_in_0_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_3_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_3_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_3_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_2_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_2_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_2_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_1_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_1_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_1_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_0_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_in_0_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_in_0_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_3_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_3_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_3_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_2_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_2_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_2_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_1_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_1_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_1_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_address;
  wire [15:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_address;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_opcode;
  wire [1:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_size;
  wire [6:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_source;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_sink;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_data;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_0_e_ready;
  wire  coupler_to_bus_named_subsystem_l2_widget_auto_out_0_e_valid;
  wire [2:0] coupler_to_bus_named_subsystem_l2_widget_auto_out_0_e_bits_sink;
  wire  coupler_from_tile_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_clock; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_reset; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_address; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_bufferable; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_modifiable; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_readalloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_writealloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_privileged; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_secure; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_fetch; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_corrupt; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_address; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_bufferable; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_modifiable; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_readalloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_writealloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_privileged; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_secure; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_fetch; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_valid; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_source; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_a_ready; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_master_clock_xing_in_a_sink; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_address; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_corrupt; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_address; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_b_valid; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_source; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_b_ready; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_master_clock_xing_in_b_sink; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_address; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_corrupt; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_address; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_c_valid; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_master_clock_xing_in_c_source; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_c_ready; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_master_clock_xing_in_c_sink; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_source; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_denied; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_corrupt; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_source; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_denied; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_d_valid; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_master_clock_xing_in_d_source; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_d_ready; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_master_clock_xing_in_d_sink; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_master_clock_xing_in_e_bits0_sink; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_master_clock_xing_in_e_bits1_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_e_valid; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_master_clock_xing_in_e_source; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_master_clock_xing_in_e_ready; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_master_clock_xing_in_e_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_out_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_out_a_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_auto_tl_out_a_bits_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_out_a_bits_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_auto_tl_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_tile_auto_tl_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_auto_tl_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_b_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_b_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_out_b_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_out_b_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_auto_tl_out_b_bits_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_out_b_bits_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_auto_tl_out_b_bits_address; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_tile_auto_tl_out_b_bits_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_auto_tl_out_b_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_b_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_c_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_c_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_out_c_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_out_c_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_auto_tl_out_c_bits_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_out_c_bits_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_auto_tl_out_c_bits_address; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_auto_tl_out_c_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_c_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_auto_tl_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_auto_tl_out_d_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_auto_tl_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_out_d_bits_source; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_out_d_bits_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_d_bits_denied; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_auto_tl_out_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_d_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_e_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_auto_tl_out_e_valid; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_auto_tl_out_e_bits_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_clock; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_reset; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_address; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_bufferable; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_modifiable; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_readalloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_writealloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_privileged; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_secure; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_fetch; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_corrupt; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_address; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_bufferable; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_modifiable; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_readalloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_writealloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_privileged; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_secure; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_fetch; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_valid; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_source; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_a_ready; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_a_sink; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_address; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_corrupt; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_address; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_b_valid; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_source; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_b_ready; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_b_sink; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_address; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_corrupt; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_address; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_c_valid; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_c_source; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_c_ready; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_c_sink; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_source; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_denied; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_corrupt; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_source; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_denied; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_d_valid; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_d_source; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_d_ready; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_d_sink; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_e_bits0_sink; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_e_bits1_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_e_valid; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_e_source; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_master_clock_xing_in_e_ready; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_master_clock_xing_in_e_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_out_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_out_a_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_1_auto_tl_out_a_bits_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_out_a_bits_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_1_auto_tl_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_tile_1_auto_tl_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_1_auto_tl_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_b_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_b_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_out_b_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_out_b_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_1_auto_tl_out_b_bits_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_out_b_bits_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_1_auto_tl_out_b_bits_address; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_tile_1_auto_tl_out_b_bits_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_1_auto_tl_out_b_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_b_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_c_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_c_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_out_c_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_out_c_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_1_auto_tl_out_c_bits_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_out_c_bits_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_tile_1_auto_tl_out_c_bits_address; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_1_auto_tl_out_c_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_c_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_tile_1_auto_tl_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_tile_1_auto_tl_out_d_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_tile_1_auto_tl_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_out_d_bits_source; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_out_d_bits_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_d_bits_denied; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_from_tile_1_auto_tl_out_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_d_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_e_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_tile_1_auto_tl_out_e_valid; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_tile_1_auto_tl_out_e_bits_sink; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_clock; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_valid; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_id; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_addr; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_len; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_size; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_cache; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_prot; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_w_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_w_valid; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_w_bits_data; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_w_bits_strb; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_w_bits_last; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_b_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_b_valid; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_b_bits_id; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_b_bits_resp; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_valid; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_id; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_addr; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_len; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_size; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_cache; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_prot; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_valid; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_bits_id; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_bits_data; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_bits_resp; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_bits_last; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_tl_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_tl_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_secure; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_tl_in_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_tl_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_denied; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_corrupt; // @[LazyModule.scala 524:27]
  RHEA__FixedClockBroadcast fixedClockNode ( // @[ClockGroup.scala 106:107]
    .auto_in_clock(fixedClockNode_auto_in_clock),
    .auto_in_reset(fixedClockNode_auto_in_reset),
    .auto_out_0_clock(fixedClockNode_auto_out_0_clock),
    .auto_out_0_reset(fixedClockNode_auto_out_0_reset)
  );
  RHEA__TLXbar system_bus_xbar ( // @[SystemBus.scala 40:43]
    .rf_reset(system_bus_xbar_rf_reset),
    .clock(system_bus_xbar_clock),
    .reset(system_bus_xbar_reset),
    .auto_in_2_a_ready(system_bus_xbar_auto_in_2_a_ready),
    .auto_in_2_a_valid(system_bus_xbar_auto_in_2_a_valid),
    .auto_in_2_a_bits_opcode(system_bus_xbar_auto_in_2_a_bits_opcode),
    .auto_in_2_a_bits_param(system_bus_xbar_auto_in_2_a_bits_param),
    .auto_in_2_a_bits_size(system_bus_xbar_auto_in_2_a_bits_size),
    .auto_in_2_a_bits_source(system_bus_xbar_auto_in_2_a_bits_source),
    .auto_in_2_a_bits_address(system_bus_xbar_auto_in_2_a_bits_address),
    .auto_in_2_a_bits_user_amba_prot_bufferable(system_bus_xbar_auto_in_2_a_bits_user_amba_prot_bufferable),
    .auto_in_2_a_bits_user_amba_prot_modifiable(system_bus_xbar_auto_in_2_a_bits_user_amba_prot_modifiable),
    .auto_in_2_a_bits_user_amba_prot_readalloc(system_bus_xbar_auto_in_2_a_bits_user_amba_prot_readalloc),
    .auto_in_2_a_bits_user_amba_prot_writealloc(system_bus_xbar_auto_in_2_a_bits_user_amba_prot_writealloc),
    .auto_in_2_a_bits_user_amba_prot_privileged(system_bus_xbar_auto_in_2_a_bits_user_amba_prot_privileged),
    .auto_in_2_a_bits_user_amba_prot_secure(system_bus_xbar_auto_in_2_a_bits_user_amba_prot_secure),
    .auto_in_2_a_bits_user_amba_prot_fetch(system_bus_xbar_auto_in_2_a_bits_user_amba_prot_fetch),
    .auto_in_2_a_bits_mask(system_bus_xbar_auto_in_2_a_bits_mask),
    .auto_in_2_a_bits_data(system_bus_xbar_auto_in_2_a_bits_data),
    .auto_in_2_a_bits_corrupt(system_bus_xbar_auto_in_2_a_bits_corrupt),
    .auto_in_2_b_ready(system_bus_xbar_auto_in_2_b_ready),
    .auto_in_2_b_valid(system_bus_xbar_auto_in_2_b_valid),
    .auto_in_2_b_bits_opcode(system_bus_xbar_auto_in_2_b_bits_opcode),
    .auto_in_2_b_bits_param(system_bus_xbar_auto_in_2_b_bits_param),
    .auto_in_2_b_bits_size(system_bus_xbar_auto_in_2_b_bits_size),
    .auto_in_2_b_bits_source(system_bus_xbar_auto_in_2_b_bits_source),
    .auto_in_2_b_bits_address(system_bus_xbar_auto_in_2_b_bits_address),
    .auto_in_2_b_bits_mask(system_bus_xbar_auto_in_2_b_bits_mask),
    .auto_in_2_b_bits_data(system_bus_xbar_auto_in_2_b_bits_data),
    .auto_in_2_b_bits_corrupt(system_bus_xbar_auto_in_2_b_bits_corrupt),
    .auto_in_2_c_ready(system_bus_xbar_auto_in_2_c_ready),
    .auto_in_2_c_valid(system_bus_xbar_auto_in_2_c_valid),
    .auto_in_2_c_bits_opcode(system_bus_xbar_auto_in_2_c_bits_opcode),
    .auto_in_2_c_bits_param(system_bus_xbar_auto_in_2_c_bits_param),
    .auto_in_2_c_bits_size(system_bus_xbar_auto_in_2_c_bits_size),
    .auto_in_2_c_bits_source(system_bus_xbar_auto_in_2_c_bits_source),
    .auto_in_2_c_bits_address(system_bus_xbar_auto_in_2_c_bits_address),
    .auto_in_2_c_bits_data(system_bus_xbar_auto_in_2_c_bits_data),
    .auto_in_2_c_bits_corrupt(system_bus_xbar_auto_in_2_c_bits_corrupt),
    .auto_in_2_d_ready(system_bus_xbar_auto_in_2_d_ready),
    .auto_in_2_d_valid(system_bus_xbar_auto_in_2_d_valid),
    .auto_in_2_d_bits_opcode(system_bus_xbar_auto_in_2_d_bits_opcode),
    .auto_in_2_d_bits_param(system_bus_xbar_auto_in_2_d_bits_param),
    .auto_in_2_d_bits_size(system_bus_xbar_auto_in_2_d_bits_size),
    .auto_in_2_d_bits_source(system_bus_xbar_auto_in_2_d_bits_source),
    .auto_in_2_d_bits_sink(system_bus_xbar_auto_in_2_d_bits_sink),
    .auto_in_2_d_bits_denied(system_bus_xbar_auto_in_2_d_bits_denied),
    .auto_in_2_d_bits_data(system_bus_xbar_auto_in_2_d_bits_data),
    .auto_in_2_d_bits_corrupt(system_bus_xbar_auto_in_2_d_bits_corrupt),
    .auto_in_2_e_ready(system_bus_xbar_auto_in_2_e_ready),
    .auto_in_2_e_valid(system_bus_xbar_auto_in_2_e_valid),
    .auto_in_2_e_bits_sink(system_bus_xbar_auto_in_2_e_bits_sink),
    .auto_in_1_a_ready(system_bus_xbar_auto_in_1_a_ready),
    .auto_in_1_a_valid(system_bus_xbar_auto_in_1_a_valid),
    .auto_in_1_a_bits_opcode(system_bus_xbar_auto_in_1_a_bits_opcode),
    .auto_in_1_a_bits_param(system_bus_xbar_auto_in_1_a_bits_param),
    .auto_in_1_a_bits_size(system_bus_xbar_auto_in_1_a_bits_size),
    .auto_in_1_a_bits_source(system_bus_xbar_auto_in_1_a_bits_source),
    .auto_in_1_a_bits_address(system_bus_xbar_auto_in_1_a_bits_address),
    .auto_in_1_a_bits_user_amba_prot_bufferable(system_bus_xbar_auto_in_1_a_bits_user_amba_prot_bufferable),
    .auto_in_1_a_bits_user_amba_prot_modifiable(system_bus_xbar_auto_in_1_a_bits_user_amba_prot_modifiable),
    .auto_in_1_a_bits_user_amba_prot_readalloc(system_bus_xbar_auto_in_1_a_bits_user_amba_prot_readalloc),
    .auto_in_1_a_bits_user_amba_prot_writealloc(system_bus_xbar_auto_in_1_a_bits_user_amba_prot_writealloc),
    .auto_in_1_a_bits_user_amba_prot_privileged(system_bus_xbar_auto_in_1_a_bits_user_amba_prot_privileged),
    .auto_in_1_a_bits_user_amba_prot_secure(system_bus_xbar_auto_in_1_a_bits_user_amba_prot_secure),
    .auto_in_1_a_bits_user_amba_prot_fetch(system_bus_xbar_auto_in_1_a_bits_user_amba_prot_fetch),
    .auto_in_1_a_bits_mask(system_bus_xbar_auto_in_1_a_bits_mask),
    .auto_in_1_a_bits_data(system_bus_xbar_auto_in_1_a_bits_data),
    .auto_in_1_a_bits_corrupt(system_bus_xbar_auto_in_1_a_bits_corrupt),
    .auto_in_1_b_ready(system_bus_xbar_auto_in_1_b_ready),
    .auto_in_1_b_valid(system_bus_xbar_auto_in_1_b_valid),
    .auto_in_1_b_bits_opcode(system_bus_xbar_auto_in_1_b_bits_opcode),
    .auto_in_1_b_bits_param(system_bus_xbar_auto_in_1_b_bits_param),
    .auto_in_1_b_bits_size(system_bus_xbar_auto_in_1_b_bits_size),
    .auto_in_1_b_bits_source(system_bus_xbar_auto_in_1_b_bits_source),
    .auto_in_1_b_bits_address(system_bus_xbar_auto_in_1_b_bits_address),
    .auto_in_1_b_bits_mask(system_bus_xbar_auto_in_1_b_bits_mask),
    .auto_in_1_b_bits_data(system_bus_xbar_auto_in_1_b_bits_data),
    .auto_in_1_b_bits_corrupt(system_bus_xbar_auto_in_1_b_bits_corrupt),
    .auto_in_1_c_ready(system_bus_xbar_auto_in_1_c_ready),
    .auto_in_1_c_valid(system_bus_xbar_auto_in_1_c_valid),
    .auto_in_1_c_bits_opcode(system_bus_xbar_auto_in_1_c_bits_opcode),
    .auto_in_1_c_bits_param(system_bus_xbar_auto_in_1_c_bits_param),
    .auto_in_1_c_bits_size(system_bus_xbar_auto_in_1_c_bits_size),
    .auto_in_1_c_bits_source(system_bus_xbar_auto_in_1_c_bits_source),
    .auto_in_1_c_bits_address(system_bus_xbar_auto_in_1_c_bits_address),
    .auto_in_1_c_bits_data(system_bus_xbar_auto_in_1_c_bits_data),
    .auto_in_1_c_bits_corrupt(system_bus_xbar_auto_in_1_c_bits_corrupt),
    .auto_in_1_d_ready(system_bus_xbar_auto_in_1_d_ready),
    .auto_in_1_d_valid(system_bus_xbar_auto_in_1_d_valid),
    .auto_in_1_d_bits_opcode(system_bus_xbar_auto_in_1_d_bits_opcode),
    .auto_in_1_d_bits_param(system_bus_xbar_auto_in_1_d_bits_param),
    .auto_in_1_d_bits_size(system_bus_xbar_auto_in_1_d_bits_size),
    .auto_in_1_d_bits_source(system_bus_xbar_auto_in_1_d_bits_source),
    .auto_in_1_d_bits_sink(system_bus_xbar_auto_in_1_d_bits_sink),
    .auto_in_1_d_bits_denied(system_bus_xbar_auto_in_1_d_bits_denied),
    .auto_in_1_d_bits_data(system_bus_xbar_auto_in_1_d_bits_data),
    .auto_in_1_d_bits_corrupt(system_bus_xbar_auto_in_1_d_bits_corrupt),
    .auto_in_1_e_ready(system_bus_xbar_auto_in_1_e_ready),
    .auto_in_1_e_valid(system_bus_xbar_auto_in_1_e_valid),
    .auto_in_1_e_bits_sink(system_bus_xbar_auto_in_1_e_bits_sink),
    .auto_in_0_a_ready(system_bus_xbar_auto_in_0_a_ready),
    .auto_in_0_a_valid(system_bus_xbar_auto_in_0_a_valid),
    .auto_in_0_a_bits_opcode(system_bus_xbar_auto_in_0_a_bits_opcode),
    .auto_in_0_a_bits_size(system_bus_xbar_auto_in_0_a_bits_size),
    .auto_in_0_a_bits_source(system_bus_xbar_auto_in_0_a_bits_source),
    .auto_in_0_a_bits_address(system_bus_xbar_auto_in_0_a_bits_address),
    .auto_in_0_a_bits_user_amba_prot_bufferable(system_bus_xbar_auto_in_0_a_bits_user_amba_prot_bufferable),
    .auto_in_0_a_bits_user_amba_prot_modifiable(system_bus_xbar_auto_in_0_a_bits_user_amba_prot_modifiable),
    .auto_in_0_a_bits_user_amba_prot_readalloc(system_bus_xbar_auto_in_0_a_bits_user_amba_prot_readalloc),
    .auto_in_0_a_bits_user_amba_prot_writealloc(system_bus_xbar_auto_in_0_a_bits_user_amba_prot_writealloc),
    .auto_in_0_a_bits_user_amba_prot_privileged(system_bus_xbar_auto_in_0_a_bits_user_amba_prot_privileged),
    .auto_in_0_a_bits_user_amba_prot_secure(system_bus_xbar_auto_in_0_a_bits_user_amba_prot_secure),
    .auto_in_0_a_bits_user_amba_prot_fetch(system_bus_xbar_auto_in_0_a_bits_user_amba_prot_fetch),
    .auto_in_0_a_bits_mask(system_bus_xbar_auto_in_0_a_bits_mask),
    .auto_in_0_a_bits_data(system_bus_xbar_auto_in_0_a_bits_data),
    .auto_in_0_d_ready(system_bus_xbar_auto_in_0_d_ready),
    .auto_in_0_d_valid(system_bus_xbar_auto_in_0_d_valid),
    .auto_in_0_d_bits_opcode(system_bus_xbar_auto_in_0_d_bits_opcode),
    .auto_in_0_d_bits_param(system_bus_xbar_auto_in_0_d_bits_param),
    .auto_in_0_d_bits_size(system_bus_xbar_auto_in_0_d_bits_size),
    .auto_in_0_d_bits_source(system_bus_xbar_auto_in_0_d_bits_source),
    .auto_in_0_d_bits_sink(system_bus_xbar_auto_in_0_d_bits_sink),
    .auto_in_0_d_bits_denied(system_bus_xbar_auto_in_0_d_bits_denied),
    .auto_in_0_d_bits_data(system_bus_xbar_auto_in_0_d_bits_data),
    .auto_in_0_d_bits_corrupt(system_bus_xbar_auto_in_0_d_bits_corrupt),
    .auto_out_6_a_ready(system_bus_xbar_auto_out_6_a_ready),
    .auto_out_6_a_valid(system_bus_xbar_auto_out_6_a_valid),
    .auto_out_6_a_bits_opcode(system_bus_xbar_auto_out_6_a_bits_opcode),
    .auto_out_6_a_bits_param(system_bus_xbar_auto_out_6_a_bits_param),
    .auto_out_6_a_bits_size(system_bus_xbar_auto_out_6_a_bits_size),
    .auto_out_6_a_bits_source(system_bus_xbar_auto_out_6_a_bits_source),
    .auto_out_6_a_bits_address(system_bus_xbar_auto_out_6_a_bits_address),
    .auto_out_6_a_bits_user_amba_prot_bufferable(system_bus_xbar_auto_out_6_a_bits_user_amba_prot_bufferable),
    .auto_out_6_a_bits_user_amba_prot_modifiable(system_bus_xbar_auto_out_6_a_bits_user_amba_prot_modifiable),
    .auto_out_6_a_bits_user_amba_prot_readalloc(system_bus_xbar_auto_out_6_a_bits_user_amba_prot_readalloc),
    .auto_out_6_a_bits_user_amba_prot_writealloc(system_bus_xbar_auto_out_6_a_bits_user_amba_prot_writealloc),
    .auto_out_6_a_bits_user_amba_prot_privileged(system_bus_xbar_auto_out_6_a_bits_user_amba_prot_privileged),
    .auto_out_6_a_bits_user_amba_prot_secure(system_bus_xbar_auto_out_6_a_bits_user_amba_prot_secure),
    .auto_out_6_a_bits_user_amba_prot_fetch(system_bus_xbar_auto_out_6_a_bits_user_amba_prot_fetch),
    .auto_out_6_a_bits_mask(system_bus_xbar_auto_out_6_a_bits_mask),
    .auto_out_6_a_bits_data(system_bus_xbar_auto_out_6_a_bits_data),
    .auto_out_6_a_bits_corrupt(system_bus_xbar_auto_out_6_a_bits_corrupt),
    .auto_out_6_d_ready(system_bus_xbar_auto_out_6_d_ready),
    .auto_out_6_d_valid(system_bus_xbar_auto_out_6_d_valid),
    .auto_out_6_d_bits_opcode(system_bus_xbar_auto_out_6_d_bits_opcode),
    .auto_out_6_d_bits_size(system_bus_xbar_auto_out_6_d_bits_size),
    .auto_out_6_d_bits_source(system_bus_xbar_auto_out_6_d_bits_source),
    .auto_out_6_d_bits_denied(system_bus_xbar_auto_out_6_d_bits_denied),
    .auto_out_6_d_bits_data(system_bus_xbar_auto_out_6_d_bits_data),
    .auto_out_6_d_bits_corrupt(system_bus_xbar_auto_out_6_d_bits_corrupt),
    .auto_out_5_a_ready(system_bus_xbar_auto_out_5_a_ready),
    .auto_out_5_a_valid(system_bus_xbar_auto_out_5_a_valid),
    .auto_out_5_a_bits_opcode(system_bus_xbar_auto_out_5_a_bits_opcode),
    .auto_out_5_a_bits_param(system_bus_xbar_auto_out_5_a_bits_param),
    .auto_out_5_a_bits_size(system_bus_xbar_auto_out_5_a_bits_size),
    .auto_out_5_a_bits_source(system_bus_xbar_auto_out_5_a_bits_source),
    .auto_out_5_a_bits_address(system_bus_xbar_auto_out_5_a_bits_address),
    .auto_out_5_a_bits_mask(system_bus_xbar_auto_out_5_a_bits_mask),
    .auto_out_5_a_bits_data(system_bus_xbar_auto_out_5_a_bits_data),
    .auto_out_5_a_bits_corrupt(system_bus_xbar_auto_out_5_a_bits_corrupt),
    .auto_out_5_b_ready(system_bus_xbar_auto_out_5_b_ready),
    .auto_out_5_b_valid(system_bus_xbar_auto_out_5_b_valid),
    .auto_out_5_b_bits_opcode(system_bus_xbar_auto_out_5_b_bits_opcode),
    .auto_out_5_b_bits_param(system_bus_xbar_auto_out_5_b_bits_param),
    .auto_out_5_b_bits_size(system_bus_xbar_auto_out_5_b_bits_size),
    .auto_out_5_b_bits_source(system_bus_xbar_auto_out_5_b_bits_source),
    .auto_out_5_b_bits_address(system_bus_xbar_auto_out_5_b_bits_address),
    .auto_out_5_b_bits_mask(system_bus_xbar_auto_out_5_b_bits_mask),
    .auto_out_5_b_bits_data(system_bus_xbar_auto_out_5_b_bits_data),
    .auto_out_5_b_bits_corrupt(system_bus_xbar_auto_out_5_b_bits_corrupt),
    .auto_out_5_c_ready(system_bus_xbar_auto_out_5_c_ready),
    .auto_out_5_c_valid(system_bus_xbar_auto_out_5_c_valid),
    .auto_out_5_c_bits_opcode(system_bus_xbar_auto_out_5_c_bits_opcode),
    .auto_out_5_c_bits_param(system_bus_xbar_auto_out_5_c_bits_param),
    .auto_out_5_c_bits_size(system_bus_xbar_auto_out_5_c_bits_size),
    .auto_out_5_c_bits_source(system_bus_xbar_auto_out_5_c_bits_source),
    .auto_out_5_c_bits_address(system_bus_xbar_auto_out_5_c_bits_address),
    .auto_out_5_c_bits_data(system_bus_xbar_auto_out_5_c_bits_data),
    .auto_out_5_c_bits_corrupt(system_bus_xbar_auto_out_5_c_bits_corrupt),
    .auto_out_5_d_ready(system_bus_xbar_auto_out_5_d_ready),
    .auto_out_5_d_valid(system_bus_xbar_auto_out_5_d_valid),
    .auto_out_5_d_bits_opcode(system_bus_xbar_auto_out_5_d_bits_opcode),
    .auto_out_5_d_bits_param(system_bus_xbar_auto_out_5_d_bits_param),
    .auto_out_5_d_bits_size(system_bus_xbar_auto_out_5_d_bits_size),
    .auto_out_5_d_bits_source(system_bus_xbar_auto_out_5_d_bits_source),
    .auto_out_5_d_bits_sink(system_bus_xbar_auto_out_5_d_bits_sink),
    .auto_out_5_d_bits_denied(system_bus_xbar_auto_out_5_d_bits_denied),
    .auto_out_5_d_bits_data(system_bus_xbar_auto_out_5_d_bits_data),
    .auto_out_5_d_bits_corrupt(system_bus_xbar_auto_out_5_d_bits_corrupt),
    .auto_out_5_e_ready(system_bus_xbar_auto_out_5_e_ready),
    .auto_out_5_e_valid(system_bus_xbar_auto_out_5_e_valid),
    .auto_out_5_e_bits_sink(system_bus_xbar_auto_out_5_e_bits_sink),
    .auto_out_4_a_ready(system_bus_xbar_auto_out_4_a_ready),
    .auto_out_4_a_valid(system_bus_xbar_auto_out_4_a_valid),
    .auto_out_4_a_bits_opcode(system_bus_xbar_auto_out_4_a_bits_opcode),
    .auto_out_4_a_bits_param(system_bus_xbar_auto_out_4_a_bits_param),
    .auto_out_4_a_bits_size(system_bus_xbar_auto_out_4_a_bits_size),
    .auto_out_4_a_bits_source(system_bus_xbar_auto_out_4_a_bits_source),
    .auto_out_4_a_bits_address(system_bus_xbar_auto_out_4_a_bits_address),
    .auto_out_4_a_bits_mask(system_bus_xbar_auto_out_4_a_bits_mask),
    .auto_out_4_a_bits_data(system_bus_xbar_auto_out_4_a_bits_data),
    .auto_out_4_a_bits_corrupt(system_bus_xbar_auto_out_4_a_bits_corrupt),
    .auto_out_4_b_ready(system_bus_xbar_auto_out_4_b_ready),
    .auto_out_4_b_valid(system_bus_xbar_auto_out_4_b_valid),
    .auto_out_4_b_bits_opcode(system_bus_xbar_auto_out_4_b_bits_opcode),
    .auto_out_4_b_bits_param(system_bus_xbar_auto_out_4_b_bits_param),
    .auto_out_4_b_bits_size(system_bus_xbar_auto_out_4_b_bits_size),
    .auto_out_4_b_bits_source(system_bus_xbar_auto_out_4_b_bits_source),
    .auto_out_4_b_bits_address(system_bus_xbar_auto_out_4_b_bits_address),
    .auto_out_4_b_bits_mask(system_bus_xbar_auto_out_4_b_bits_mask),
    .auto_out_4_b_bits_data(system_bus_xbar_auto_out_4_b_bits_data),
    .auto_out_4_b_bits_corrupt(system_bus_xbar_auto_out_4_b_bits_corrupt),
    .auto_out_4_c_ready(system_bus_xbar_auto_out_4_c_ready),
    .auto_out_4_c_valid(system_bus_xbar_auto_out_4_c_valid),
    .auto_out_4_c_bits_opcode(system_bus_xbar_auto_out_4_c_bits_opcode),
    .auto_out_4_c_bits_param(system_bus_xbar_auto_out_4_c_bits_param),
    .auto_out_4_c_bits_size(system_bus_xbar_auto_out_4_c_bits_size),
    .auto_out_4_c_bits_source(system_bus_xbar_auto_out_4_c_bits_source),
    .auto_out_4_c_bits_address(system_bus_xbar_auto_out_4_c_bits_address),
    .auto_out_4_c_bits_data(system_bus_xbar_auto_out_4_c_bits_data),
    .auto_out_4_c_bits_corrupt(system_bus_xbar_auto_out_4_c_bits_corrupt),
    .auto_out_4_d_ready(system_bus_xbar_auto_out_4_d_ready),
    .auto_out_4_d_valid(system_bus_xbar_auto_out_4_d_valid),
    .auto_out_4_d_bits_opcode(system_bus_xbar_auto_out_4_d_bits_opcode),
    .auto_out_4_d_bits_param(system_bus_xbar_auto_out_4_d_bits_param),
    .auto_out_4_d_bits_size(system_bus_xbar_auto_out_4_d_bits_size),
    .auto_out_4_d_bits_source(system_bus_xbar_auto_out_4_d_bits_source),
    .auto_out_4_d_bits_sink(system_bus_xbar_auto_out_4_d_bits_sink),
    .auto_out_4_d_bits_denied(system_bus_xbar_auto_out_4_d_bits_denied),
    .auto_out_4_d_bits_data(system_bus_xbar_auto_out_4_d_bits_data),
    .auto_out_4_d_bits_corrupt(system_bus_xbar_auto_out_4_d_bits_corrupt),
    .auto_out_4_e_ready(system_bus_xbar_auto_out_4_e_ready),
    .auto_out_4_e_valid(system_bus_xbar_auto_out_4_e_valid),
    .auto_out_4_e_bits_sink(system_bus_xbar_auto_out_4_e_bits_sink),
    .auto_out_3_a_ready(system_bus_xbar_auto_out_3_a_ready),
    .auto_out_3_a_valid(system_bus_xbar_auto_out_3_a_valid),
    .auto_out_3_a_bits_opcode(system_bus_xbar_auto_out_3_a_bits_opcode),
    .auto_out_3_a_bits_param(system_bus_xbar_auto_out_3_a_bits_param),
    .auto_out_3_a_bits_size(system_bus_xbar_auto_out_3_a_bits_size),
    .auto_out_3_a_bits_source(system_bus_xbar_auto_out_3_a_bits_source),
    .auto_out_3_a_bits_address(system_bus_xbar_auto_out_3_a_bits_address),
    .auto_out_3_a_bits_mask(system_bus_xbar_auto_out_3_a_bits_mask),
    .auto_out_3_a_bits_data(system_bus_xbar_auto_out_3_a_bits_data),
    .auto_out_3_a_bits_corrupt(system_bus_xbar_auto_out_3_a_bits_corrupt),
    .auto_out_3_b_ready(system_bus_xbar_auto_out_3_b_ready),
    .auto_out_3_b_valid(system_bus_xbar_auto_out_3_b_valid),
    .auto_out_3_b_bits_opcode(system_bus_xbar_auto_out_3_b_bits_opcode),
    .auto_out_3_b_bits_param(system_bus_xbar_auto_out_3_b_bits_param),
    .auto_out_3_b_bits_size(system_bus_xbar_auto_out_3_b_bits_size),
    .auto_out_3_b_bits_source(system_bus_xbar_auto_out_3_b_bits_source),
    .auto_out_3_b_bits_address(system_bus_xbar_auto_out_3_b_bits_address),
    .auto_out_3_b_bits_mask(system_bus_xbar_auto_out_3_b_bits_mask),
    .auto_out_3_b_bits_data(system_bus_xbar_auto_out_3_b_bits_data),
    .auto_out_3_b_bits_corrupt(system_bus_xbar_auto_out_3_b_bits_corrupt),
    .auto_out_3_c_ready(system_bus_xbar_auto_out_3_c_ready),
    .auto_out_3_c_valid(system_bus_xbar_auto_out_3_c_valid),
    .auto_out_3_c_bits_opcode(system_bus_xbar_auto_out_3_c_bits_opcode),
    .auto_out_3_c_bits_param(system_bus_xbar_auto_out_3_c_bits_param),
    .auto_out_3_c_bits_size(system_bus_xbar_auto_out_3_c_bits_size),
    .auto_out_3_c_bits_source(system_bus_xbar_auto_out_3_c_bits_source),
    .auto_out_3_c_bits_address(system_bus_xbar_auto_out_3_c_bits_address),
    .auto_out_3_c_bits_data(system_bus_xbar_auto_out_3_c_bits_data),
    .auto_out_3_c_bits_corrupt(system_bus_xbar_auto_out_3_c_bits_corrupt),
    .auto_out_3_d_ready(system_bus_xbar_auto_out_3_d_ready),
    .auto_out_3_d_valid(system_bus_xbar_auto_out_3_d_valid),
    .auto_out_3_d_bits_opcode(system_bus_xbar_auto_out_3_d_bits_opcode),
    .auto_out_3_d_bits_param(system_bus_xbar_auto_out_3_d_bits_param),
    .auto_out_3_d_bits_size(system_bus_xbar_auto_out_3_d_bits_size),
    .auto_out_3_d_bits_source(system_bus_xbar_auto_out_3_d_bits_source),
    .auto_out_3_d_bits_sink(system_bus_xbar_auto_out_3_d_bits_sink),
    .auto_out_3_d_bits_denied(system_bus_xbar_auto_out_3_d_bits_denied),
    .auto_out_3_d_bits_data(system_bus_xbar_auto_out_3_d_bits_data),
    .auto_out_3_d_bits_corrupt(system_bus_xbar_auto_out_3_d_bits_corrupt),
    .auto_out_3_e_ready(system_bus_xbar_auto_out_3_e_ready),
    .auto_out_3_e_valid(system_bus_xbar_auto_out_3_e_valid),
    .auto_out_3_e_bits_sink(system_bus_xbar_auto_out_3_e_bits_sink),
    .auto_out_2_a_ready(system_bus_xbar_auto_out_2_a_ready),
    .auto_out_2_a_valid(system_bus_xbar_auto_out_2_a_valid),
    .auto_out_2_a_bits_opcode(system_bus_xbar_auto_out_2_a_bits_opcode),
    .auto_out_2_a_bits_param(system_bus_xbar_auto_out_2_a_bits_param),
    .auto_out_2_a_bits_size(system_bus_xbar_auto_out_2_a_bits_size),
    .auto_out_2_a_bits_source(system_bus_xbar_auto_out_2_a_bits_source),
    .auto_out_2_a_bits_address(system_bus_xbar_auto_out_2_a_bits_address),
    .auto_out_2_a_bits_mask(system_bus_xbar_auto_out_2_a_bits_mask),
    .auto_out_2_a_bits_data(system_bus_xbar_auto_out_2_a_bits_data),
    .auto_out_2_a_bits_corrupt(system_bus_xbar_auto_out_2_a_bits_corrupt),
    .auto_out_2_b_ready(system_bus_xbar_auto_out_2_b_ready),
    .auto_out_2_b_valid(system_bus_xbar_auto_out_2_b_valid),
    .auto_out_2_b_bits_opcode(system_bus_xbar_auto_out_2_b_bits_opcode),
    .auto_out_2_b_bits_param(system_bus_xbar_auto_out_2_b_bits_param),
    .auto_out_2_b_bits_size(system_bus_xbar_auto_out_2_b_bits_size),
    .auto_out_2_b_bits_source(system_bus_xbar_auto_out_2_b_bits_source),
    .auto_out_2_b_bits_address(system_bus_xbar_auto_out_2_b_bits_address),
    .auto_out_2_b_bits_mask(system_bus_xbar_auto_out_2_b_bits_mask),
    .auto_out_2_b_bits_data(system_bus_xbar_auto_out_2_b_bits_data),
    .auto_out_2_b_bits_corrupt(system_bus_xbar_auto_out_2_b_bits_corrupt),
    .auto_out_2_c_ready(system_bus_xbar_auto_out_2_c_ready),
    .auto_out_2_c_valid(system_bus_xbar_auto_out_2_c_valid),
    .auto_out_2_c_bits_opcode(system_bus_xbar_auto_out_2_c_bits_opcode),
    .auto_out_2_c_bits_param(system_bus_xbar_auto_out_2_c_bits_param),
    .auto_out_2_c_bits_size(system_bus_xbar_auto_out_2_c_bits_size),
    .auto_out_2_c_bits_source(system_bus_xbar_auto_out_2_c_bits_source),
    .auto_out_2_c_bits_address(system_bus_xbar_auto_out_2_c_bits_address),
    .auto_out_2_c_bits_data(system_bus_xbar_auto_out_2_c_bits_data),
    .auto_out_2_c_bits_corrupt(system_bus_xbar_auto_out_2_c_bits_corrupt),
    .auto_out_2_d_ready(system_bus_xbar_auto_out_2_d_ready),
    .auto_out_2_d_valid(system_bus_xbar_auto_out_2_d_valid),
    .auto_out_2_d_bits_opcode(system_bus_xbar_auto_out_2_d_bits_opcode),
    .auto_out_2_d_bits_param(system_bus_xbar_auto_out_2_d_bits_param),
    .auto_out_2_d_bits_size(system_bus_xbar_auto_out_2_d_bits_size),
    .auto_out_2_d_bits_source(system_bus_xbar_auto_out_2_d_bits_source),
    .auto_out_2_d_bits_sink(system_bus_xbar_auto_out_2_d_bits_sink),
    .auto_out_2_d_bits_denied(system_bus_xbar_auto_out_2_d_bits_denied),
    .auto_out_2_d_bits_data(system_bus_xbar_auto_out_2_d_bits_data),
    .auto_out_2_d_bits_corrupt(system_bus_xbar_auto_out_2_d_bits_corrupt),
    .auto_out_2_e_ready(system_bus_xbar_auto_out_2_e_ready),
    .auto_out_2_e_valid(system_bus_xbar_auto_out_2_e_valid),
    .auto_out_2_e_bits_sink(system_bus_xbar_auto_out_2_e_bits_sink),
    .auto_out_1_a_ready(system_bus_xbar_auto_out_1_a_ready),
    .auto_out_1_a_valid(system_bus_xbar_auto_out_1_a_valid),
    .auto_out_1_a_bits_opcode(system_bus_xbar_auto_out_1_a_bits_opcode),
    .auto_out_1_a_bits_param(system_bus_xbar_auto_out_1_a_bits_param),
    .auto_out_1_a_bits_size(system_bus_xbar_auto_out_1_a_bits_size),
    .auto_out_1_a_bits_source(system_bus_xbar_auto_out_1_a_bits_source),
    .auto_out_1_a_bits_address(system_bus_xbar_auto_out_1_a_bits_address),
    .auto_out_1_a_bits_user_amba_prot_bufferable(system_bus_xbar_auto_out_1_a_bits_user_amba_prot_bufferable),
    .auto_out_1_a_bits_user_amba_prot_modifiable(system_bus_xbar_auto_out_1_a_bits_user_amba_prot_modifiable),
    .auto_out_1_a_bits_user_amba_prot_readalloc(system_bus_xbar_auto_out_1_a_bits_user_amba_prot_readalloc),
    .auto_out_1_a_bits_user_amba_prot_writealloc(system_bus_xbar_auto_out_1_a_bits_user_amba_prot_writealloc),
    .auto_out_1_a_bits_user_amba_prot_privileged(system_bus_xbar_auto_out_1_a_bits_user_amba_prot_privileged),
    .auto_out_1_a_bits_user_amba_prot_secure(system_bus_xbar_auto_out_1_a_bits_user_amba_prot_secure),
    .auto_out_1_a_bits_user_amba_prot_fetch(system_bus_xbar_auto_out_1_a_bits_user_amba_prot_fetch),
    .auto_out_1_a_bits_mask(system_bus_xbar_auto_out_1_a_bits_mask),
    .auto_out_1_a_bits_data(system_bus_xbar_auto_out_1_a_bits_data),
    .auto_out_1_a_bits_corrupt(system_bus_xbar_auto_out_1_a_bits_corrupt),
    .auto_out_1_d_ready(system_bus_xbar_auto_out_1_d_ready),
    .auto_out_1_d_valid(system_bus_xbar_auto_out_1_d_valid),
    .auto_out_1_d_bits_opcode(system_bus_xbar_auto_out_1_d_bits_opcode),
    .auto_out_1_d_bits_param(system_bus_xbar_auto_out_1_d_bits_param),
    .auto_out_1_d_bits_size(system_bus_xbar_auto_out_1_d_bits_size),
    .auto_out_1_d_bits_source(system_bus_xbar_auto_out_1_d_bits_source),
    .auto_out_1_d_bits_sink(system_bus_xbar_auto_out_1_d_bits_sink),
    .auto_out_1_d_bits_denied(system_bus_xbar_auto_out_1_d_bits_denied),
    .auto_out_1_d_bits_data(system_bus_xbar_auto_out_1_d_bits_data),
    .auto_out_1_d_bits_corrupt(system_bus_xbar_auto_out_1_d_bits_corrupt),
    .auto_out_0_a_ready(system_bus_xbar_auto_out_0_a_ready),
    .auto_out_0_a_valid(system_bus_xbar_auto_out_0_a_valid),
    .auto_out_0_a_bits_opcode(system_bus_xbar_auto_out_0_a_bits_opcode),
    .auto_out_0_a_bits_param(system_bus_xbar_auto_out_0_a_bits_param),
    .auto_out_0_a_bits_size(system_bus_xbar_auto_out_0_a_bits_size),
    .auto_out_0_a_bits_source(system_bus_xbar_auto_out_0_a_bits_source),
    .auto_out_0_a_bits_address(system_bus_xbar_auto_out_0_a_bits_address),
    .auto_out_0_a_bits_mask(system_bus_xbar_auto_out_0_a_bits_mask),
    .auto_out_0_a_bits_data(system_bus_xbar_auto_out_0_a_bits_data),
    .auto_out_0_a_bits_corrupt(system_bus_xbar_auto_out_0_a_bits_corrupt),
    .auto_out_0_d_ready(system_bus_xbar_auto_out_0_d_ready),
    .auto_out_0_d_valid(system_bus_xbar_auto_out_0_d_valid),
    .auto_out_0_d_bits_opcode(system_bus_xbar_auto_out_0_d_bits_opcode),
    .auto_out_0_d_bits_size(system_bus_xbar_auto_out_0_d_bits_size),
    .auto_out_0_d_bits_source(system_bus_xbar_auto_out_0_d_bits_source),
    .auto_out_0_d_bits_data(system_bus_xbar_auto_out_0_d_bits_data),
    .auto_out_0_d_bits_corrupt(system_bus_xbar_auto_out_0_d_bits_corrupt)
  );
  RHEA__TLFIFOFixer fixer ( // @[FIFOFixer.scala 144:27]
    .rf_reset(fixer_rf_reset),
    .clock(fixer_clock),
    .reset(fixer_reset),
    .auto_in_2_a_ready(fixer_auto_in_2_a_ready),
    .auto_in_2_a_valid(fixer_auto_in_2_a_valid),
    .auto_in_2_a_bits_opcode(fixer_auto_in_2_a_bits_opcode),
    .auto_in_2_a_bits_param(fixer_auto_in_2_a_bits_param),
    .auto_in_2_a_bits_size(fixer_auto_in_2_a_bits_size),
    .auto_in_2_a_bits_source(fixer_auto_in_2_a_bits_source),
    .auto_in_2_a_bits_address(fixer_auto_in_2_a_bits_address),
    .auto_in_2_a_bits_user_amba_prot_bufferable(fixer_auto_in_2_a_bits_user_amba_prot_bufferable),
    .auto_in_2_a_bits_user_amba_prot_modifiable(fixer_auto_in_2_a_bits_user_amba_prot_modifiable),
    .auto_in_2_a_bits_user_amba_prot_readalloc(fixer_auto_in_2_a_bits_user_amba_prot_readalloc),
    .auto_in_2_a_bits_user_amba_prot_writealloc(fixer_auto_in_2_a_bits_user_amba_prot_writealloc),
    .auto_in_2_a_bits_user_amba_prot_privileged(fixer_auto_in_2_a_bits_user_amba_prot_privileged),
    .auto_in_2_a_bits_user_amba_prot_secure(fixer_auto_in_2_a_bits_user_amba_prot_secure),
    .auto_in_2_a_bits_user_amba_prot_fetch(fixer_auto_in_2_a_bits_user_amba_prot_fetch),
    .auto_in_2_a_bits_mask(fixer_auto_in_2_a_bits_mask),
    .auto_in_2_a_bits_data(fixer_auto_in_2_a_bits_data),
    .auto_in_2_a_bits_corrupt(fixer_auto_in_2_a_bits_corrupt),
    .auto_in_2_b_ready(fixer_auto_in_2_b_ready),
    .auto_in_2_b_valid(fixer_auto_in_2_b_valid),
    .auto_in_2_b_bits_opcode(fixer_auto_in_2_b_bits_opcode),
    .auto_in_2_b_bits_param(fixer_auto_in_2_b_bits_param),
    .auto_in_2_b_bits_size(fixer_auto_in_2_b_bits_size),
    .auto_in_2_b_bits_source(fixer_auto_in_2_b_bits_source),
    .auto_in_2_b_bits_address(fixer_auto_in_2_b_bits_address),
    .auto_in_2_b_bits_mask(fixer_auto_in_2_b_bits_mask),
    .auto_in_2_b_bits_data(fixer_auto_in_2_b_bits_data),
    .auto_in_2_b_bits_corrupt(fixer_auto_in_2_b_bits_corrupt),
    .auto_in_2_c_ready(fixer_auto_in_2_c_ready),
    .auto_in_2_c_valid(fixer_auto_in_2_c_valid),
    .auto_in_2_c_bits_opcode(fixer_auto_in_2_c_bits_opcode),
    .auto_in_2_c_bits_param(fixer_auto_in_2_c_bits_param),
    .auto_in_2_c_bits_size(fixer_auto_in_2_c_bits_size),
    .auto_in_2_c_bits_source(fixer_auto_in_2_c_bits_source),
    .auto_in_2_c_bits_address(fixer_auto_in_2_c_bits_address),
    .auto_in_2_c_bits_data(fixer_auto_in_2_c_bits_data),
    .auto_in_2_c_bits_corrupt(fixer_auto_in_2_c_bits_corrupt),
    .auto_in_2_d_ready(fixer_auto_in_2_d_ready),
    .auto_in_2_d_valid(fixer_auto_in_2_d_valid),
    .auto_in_2_d_bits_opcode(fixer_auto_in_2_d_bits_opcode),
    .auto_in_2_d_bits_param(fixer_auto_in_2_d_bits_param),
    .auto_in_2_d_bits_size(fixer_auto_in_2_d_bits_size),
    .auto_in_2_d_bits_source(fixer_auto_in_2_d_bits_source),
    .auto_in_2_d_bits_sink(fixer_auto_in_2_d_bits_sink),
    .auto_in_2_d_bits_denied(fixer_auto_in_2_d_bits_denied),
    .auto_in_2_d_bits_data(fixer_auto_in_2_d_bits_data),
    .auto_in_2_d_bits_corrupt(fixer_auto_in_2_d_bits_corrupt),
    .auto_in_2_e_ready(fixer_auto_in_2_e_ready),
    .auto_in_2_e_valid(fixer_auto_in_2_e_valid),
    .auto_in_2_e_bits_sink(fixer_auto_in_2_e_bits_sink),
    .auto_in_1_a_ready(fixer_auto_in_1_a_ready),
    .auto_in_1_a_valid(fixer_auto_in_1_a_valid),
    .auto_in_1_a_bits_opcode(fixer_auto_in_1_a_bits_opcode),
    .auto_in_1_a_bits_param(fixer_auto_in_1_a_bits_param),
    .auto_in_1_a_bits_size(fixer_auto_in_1_a_bits_size),
    .auto_in_1_a_bits_source(fixer_auto_in_1_a_bits_source),
    .auto_in_1_a_bits_address(fixer_auto_in_1_a_bits_address),
    .auto_in_1_a_bits_user_amba_prot_bufferable(fixer_auto_in_1_a_bits_user_amba_prot_bufferable),
    .auto_in_1_a_bits_user_amba_prot_modifiable(fixer_auto_in_1_a_bits_user_amba_prot_modifiable),
    .auto_in_1_a_bits_user_amba_prot_readalloc(fixer_auto_in_1_a_bits_user_amba_prot_readalloc),
    .auto_in_1_a_bits_user_amba_prot_writealloc(fixer_auto_in_1_a_bits_user_amba_prot_writealloc),
    .auto_in_1_a_bits_user_amba_prot_privileged(fixer_auto_in_1_a_bits_user_amba_prot_privileged),
    .auto_in_1_a_bits_user_amba_prot_secure(fixer_auto_in_1_a_bits_user_amba_prot_secure),
    .auto_in_1_a_bits_user_amba_prot_fetch(fixer_auto_in_1_a_bits_user_amba_prot_fetch),
    .auto_in_1_a_bits_mask(fixer_auto_in_1_a_bits_mask),
    .auto_in_1_a_bits_data(fixer_auto_in_1_a_bits_data),
    .auto_in_1_a_bits_corrupt(fixer_auto_in_1_a_bits_corrupt),
    .auto_in_1_b_ready(fixer_auto_in_1_b_ready),
    .auto_in_1_b_valid(fixer_auto_in_1_b_valid),
    .auto_in_1_b_bits_opcode(fixer_auto_in_1_b_bits_opcode),
    .auto_in_1_b_bits_param(fixer_auto_in_1_b_bits_param),
    .auto_in_1_b_bits_size(fixer_auto_in_1_b_bits_size),
    .auto_in_1_b_bits_source(fixer_auto_in_1_b_bits_source),
    .auto_in_1_b_bits_address(fixer_auto_in_1_b_bits_address),
    .auto_in_1_b_bits_mask(fixer_auto_in_1_b_bits_mask),
    .auto_in_1_b_bits_data(fixer_auto_in_1_b_bits_data),
    .auto_in_1_b_bits_corrupt(fixer_auto_in_1_b_bits_corrupt),
    .auto_in_1_c_ready(fixer_auto_in_1_c_ready),
    .auto_in_1_c_valid(fixer_auto_in_1_c_valid),
    .auto_in_1_c_bits_opcode(fixer_auto_in_1_c_bits_opcode),
    .auto_in_1_c_bits_param(fixer_auto_in_1_c_bits_param),
    .auto_in_1_c_bits_size(fixer_auto_in_1_c_bits_size),
    .auto_in_1_c_bits_source(fixer_auto_in_1_c_bits_source),
    .auto_in_1_c_bits_address(fixer_auto_in_1_c_bits_address),
    .auto_in_1_c_bits_data(fixer_auto_in_1_c_bits_data),
    .auto_in_1_c_bits_corrupt(fixer_auto_in_1_c_bits_corrupt),
    .auto_in_1_d_ready(fixer_auto_in_1_d_ready),
    .auto_in_1_d_valid(fixer_auto_in_1_d_valid),
    .auto_in_1_d_bits_opcode(fixer_auto_in_1_d_bits_opcode),
    .auto_in_1_d_bits_param(fixer_auto_in_1_d_bits_param),
    .auto_in_1_d_bits_size(fixer_auto_in_1_d_bits_size),
    .auto_in_1_d_bits_source(fixer_auto_in_1_d_bits_source),
    .auto_in_1_d_bits_sink(fixer_auto_in_1_d_bits_sink),
    .auto_in_1_d_bits_denied(fixer_auto_in_1_d_bits_denied),
    .auto_in_1_d_bits_data(fixer_auto_in_1_d_bits_data),
    .auto_in_1_d_bits_corrupt(fixer_auto_in_1_d_bits_corrupt),
    .auto_in_1_e_ready(fixer_auto_in_1_e_ready),
    .auto_in_1_e_valid(fixer_auto_in_1_e_valid),
    .auto_in_1_e_bits_sink(fixer_auto_in_1_e_bits_sink),
    .auto_in_0_a_ready(fixer_auto_in_0_a_ready),
    .auto_in_0_a_valid(fixer_auto_in_0_a_valid),
    .auto_in_0_a_bits_opcode(fixer_auto_in_0_a_bits_opcode),
    .auto_in_0_a_bits_size(fixer_auto_in_0_a_bits_size),
    .auto_in_0_a_bits_source(fixer_auto_in_0_a_bits_source),
    .auto_in_0_a_bits_address(fixer_auto_in_0_a_bits_address),
    .auto_in_0_a_bits_user_amba_prot_bufferable(fixer_auto_in_0_a_bits_user_amba_prot_bufferable),
    .auto_in_0_a_bits_user_amba_prot_modifiable(fixer_auto_in_0_a_bits_user_amba_prot_modifiable),
    .auto_in_0_a_bits_user_amba_prot_readalloc(fixer_auto_in_0_a_bits_user_amba_prot_readalloc),
    .auto_in_0_a_bits_user_amba_prot_writealloc(fixer_auto_in_0_a_bits_user_amba_prot_writealloc),
    .auto_in_0_a_bits_user_amba_prot_privileged(fixer_auto_in_0_a_bits_user_amba_prot_privileged),
    .auto_in_0_a_bits_user_amba_prot_secure(fixer_auto_in_0_a_bits_user_amba_prot_secure),
    .auto_in_0_a_bits_user_amba_prot_fetch(fixer_auto_in_0_a_bits_user_amba_prot_fetch),
    .auto_in_0_a_bits_mask(fixer_auto_in_0_a_bits_mask),
    .auto_in_0_a_bits_data(fixer_auto_in_0_a_bits_data),
    .auto_in_0_d_ready(fixer_auto_in_0_d_ready),
    .auto_in_0_d_valid(fixer_auto_in_0_d_valid),
    .auto_in_0_d_bits_opcode(fixer_auto_in_0_d_bits_opcode),
    .auto_in_0_d_bits_param(fixer_auto_in_0_d_bits_param),
    .auto_in_0_d_bits_size(fixer_auto_in_0_d_bits_size),
    .auto_in_0_d_bits_source(fixer_auto_in_0_d_bits_source),
    .auto_in_0_d_bits_sink(fixer_auto_in_0_d_bits_sink),
    .auto_in_0_d_bits_denied(fixer_auto_in_0_d_bits_denied),
    .auto_in_0_d_bits_data(fixer_auto_in_0_d_bits_data),
    .auto_in_0_d_bits_corrupt(fixer_auto_in_0_d_bits_corrupt),
    .auto_out_2_a_ready(fixer_auto_out_2_a_ready),
    .auto_out_2_a_valid(fixer_auto_out_2_a_valid),
    .auto_out_2_a_bits_opcode(fixer_auto_out_2_a_bits_opcode),
    .auto_out_2_a_bits_param(fixer_auto_out_2_a_bits_param),
    .auto_out_2_a_bits_size(fixer_auto_out_2_a_bits_size),
    .auto_out_2_a_bits_source(fixer_auto_out_2_a_bits_source),
    .auto_out_2_a_bits_address(fixer_auto_out_2_a_bits_address),
    .auto_out_2_a_bits_user_amba_prot_bufferable(fixer_auto_out_2_a_bits_user_amba_prot_bufferable),
    .auto_out_2_a_bits_user_amba_prot_modifiable(fixer_auto_out_2_a_bits_user_amba_prot_modifiable),
    .auto_out_2_a_bits_user_amba_prot_readalloc(fixer_auto_out_2_a_bits_user_amba_prot_readalloc),
    .auto_out_2_a_bits_user_amba_prot_writealloc(fixer_auto_out_2_a_bits_user_amba_prot_writealloc),
    .auto_out_2_a_bits_user_amba_prot_privileged(fixer_auto_out_2_a_bits_user_amba_prot_privileged),
    .auto_out_2_a_bits_user_amba_prot_secure(fixer_auto_out_2_a_bits_user_amba_prot_secure),
    .auto_out_2_a_bits_user_amba_prot_fetch(fixer_auto_out_2_a_bits_user_amba_prot_fetch),
    .auto_out_2_a_bits_mask(fixer_auto_out_2_a_bits_mask),
    .auto_out_2_a_bits_data(fixer_auto_out_2_a_bits_data),
    .auto_out_2_a_bits_corrupt(fixer_auto_out_2_a_bits_corrupt),
    .auto_out_2_b_ready(fixer_auto_out_2_b_ready),
    .auto_out_2_b_valid(fixer_auto_out_2_b_valid),
    .auto_out_2_b_bits_opcode(fixer_auto_out_2_b_bits_opcode),
    .auto_out_2_b_bits_param(fixer_auto_out_2_b_bits_param),
    .auto_out_2_b_bits_size(fixer_auto_out_2_b_bits_size),
    .auto_out_2_b_bits_source(fixer_auto_out_2_b_bits_source),
    .auto_out_2_b_bits_address(fixer_auto_out_2_b_bits_address),
    .auto_out_2_b_bits_mask(fixer_auto_out_2_b_bits_mask),
    .auto_out_2_b_bits_data(fixer_auto_out_2_b_bits_data),
    .auto_out_2_b_bits_corrupt(fixer_auto_out_2_b_bits_corrupt),
    .auto_out_2_c_ready(fixer_auto_out_2_c_ready),
    .auto_out_2_c_valid(fixer_auto_out_2_c_valid),
    .auto_out_2_c_bits_opcode(fixer_auto_out_2_c_bits_opcode),
    .auto_out_2_c_bits_param(fixer_auto_out_2_c_bits_param),
    .auto_out_2_c_bits_size(fixer_auto_out_2_c_bits_size),
    .auto_out_2_c_bits_source(fixer_auto_out_2_c_bits_source),
    .auto_out_2_c_bits_address(fixer_auto_out_2_c_bits_address),
    .auto_out_2_c_bits_data(fixer_auto_out_2_c_bits_data),
    .auto_out_2_c_bits_corrupt(fixer_auto_out_2_c_bits_corrupt),
    .auto_out_2_d_ready(fixer_auto_out_2_d_ready),
    .auto_out_2_d_valid(fixer_auto_out_2_d_valid),
    .auto_out_2_d_bits_opcode(fixer_auto_out_2_d_bits_opcode),
    .auto_out_2_d_bits_param(fixer_auto_out_2_d_bits_param),
    .auto_out_2_d_bits_size(fixer_auto_out_2_d_bits_size),
    .auto_out_2_d_bits_source(fixer_auto_out_2_d_bits_source),
    .auto_out_2_d_bits_sink(fixer_auto_out_2_d_bits_sink),
    .auto_out_2_d_bits_denied(fixer_auto_out_2_d_bits_denied),
    .auto_out_2_d_bits_data(fixer_auto_out_2_d_bits_data),
    .auto_out_2_d_bits_corrupt(fixer_auto_out_2_d_bits_corrupt),
    .auto_out_2_e_ready(fixer_auto_out_2_e_ready),
    .auto_out_2_e_valid(fixer_auto_out_2_e_valid),
    .auto_out_2_e_bits_sink(fixer_auto_out_2_e_bits_sink),
    .auto_out_1_a_ready(fixer_auto_out_1_a_ready),
    .auto_out_1_a_valid(fixer_auto_out_1_a_valid),
    .auto_out_1_a_bits_opcode(fixer_auto_out_1_a_bits_opcode),
    .auto_out_1_a_bits_param(fixer_auto_out_1_a_bits_param),
    .auto_out_1_a_bits_size(fixer_auto_out_1_a_bits_size),
    .auto_out_1_a_bits_source(fixer_auto_out_1_a_bits_source),
    .auto_out_1_a_bits_address(fixer_auto_out_1_a_bits_address),
    .auto_out_1_a_bits_user_amba_prot_bufferable(fixer_auto_out_1_a_bits_user_amba_prot_bufferable),
    .auto_out_1_a_bits_user_amba_prot_modifiable(fixer_auto_out_1_a_bits_user_amba_prot_modifiable),
    .auto_out_1_a_bits_user_amba_prot_readalloc(fixer_auto_out_1_a_bits_user_amba_prot_readalloc),
    .auto_out_1_a_bits_user_amba_prot_writealloc(fixer_auto_out_1_a_bits_user_amba_prot_writealloc),
    .auto_out_1_a_bits_user_amba_prot_privileged(fixer_auto_out_1_a_bits_user_amba_prot_privileged),
    .auto_out_1_a_bits_user_amba_prot_secure(fixer_auto_out_1_a_bits_user_amba_prot_secure),
    .auto_out_1_a_bits_user_amba_prot_fetch(fixer_auto_out_1_a_bits_user_amba_prot_fetch),
    .auto_out_1_a_bits_mask(fixer_auto_out_1_a_bits_mask),
    .auto_out_1_a_bits_data(fixer_auto_out_1_a_bits_data),
    .auto_out_1_a_bits_corrupt(fixer_auto_out_1_a_bits_corrupt),
    .auto_out_1_b_ready(fixer_auto_out_1_b_ready),
    .auto_out_1_b_valid(fixer_auto_out_1_b_valid),
    .auto_out_1_b_bits_opcode(fixer_auto_out_1_b_bits_opcode),
    .auto_out_1_b_bits_param(fixer_auto_out_1_b_bits_param),
    .auto_out_1_b_bits_size(fixer_auto_out_1_b_bits_size),
    .auto_out_1_b_bits_source(fixer_auto_out_1_b_bits_source),
    .auto_out_1_b_bits_address(fixer_auto_out_1_b_bits_address),
    .auto_out_1_b_bits_mask(fixer_auto_out_1_b_bits_mask),
    .auto_out_1_b_bits_data(fixer_auto_out_1_b_bits_data),
    .auto_out_1_b_bits_corrupt(fixer_auto_out_1_b_bits_corrupt),
    .auto_out_1_c_ready(fixer_auto_out_1_c_ready),
    .auto_out_1_c_valid(fixer_auto_out_1_c_valid),
    .auto_out_1_c_bits_opcode(fixer_auto_out_1_c_bits_opcode),
    .auto_out_1_c_bits_param(fixer_auto_out_1_c_bits_param),
    .auto_out_1_c_bits_size(fixer_auto_out_1_c_bits_size),
    .auto_out_1_c_bits_source(fixer_auto_out_1_c_bits_source),
    .auto_out_1_c_bits_address(fixer_auto_out_1_c_bits_address),
    .auto_out_1_c_bits_data(fixer_auto_out_1_c_bits_data),
    .auto_out_1_c_bits_corrupt(fixer_auto_out_1_c_bits_corrupt),
    .auto_out_1_d_ready(fixer_auto_out_1_d_ready),
    .auto_out_1_d_valid(fixer_auto_out_1_d_valid),
    .auto_out_1_d_bits_opcode(fixer_auto_out_1_d_bits_opcode),
    .auto_out_1_d_bits_param(fixer_auto_out_1_d_bits_param),
    .auto_out_1_d_bits_size(fixer_auto_out_1_d_bits_size),
    .auto_out_1_d_bits_source(fixer_auto_out_1_d_bits_source),
    .auto_out_1_d_bits_sink(fixer_auto_out_1_d_bits_sink),
    .auto_out_1_d_bits_denied(fixer_auto_out_1_d_bits_denied),
    .auto_out_1_d_bits_data(fixer_auto_out_1_d_bits_data),
    .auto_out_1_d_bits_corrupt(fixer_auto_out_1_d_bits_corrupt),
    .auto_out_1_e_ready(fixer_auto_out_1_e_ready),
    .auto_out_1_e_valid(fixer_auto_out_1_e_valid),
    .auto_out_1_e_bits_sink(fixer_auto_out_1_e_bits_sink),
    .auto_out_0_a_ready(fixer_auto_out_0_a_ready),
    .auto_out_0_a_valid(fixer_auto_out_0_a_valid),
    .auto_out_0_a_bits_opcode(fixer_auto_out_0_a_bits_opcode),
    .auto_out_0_a_bits_size(fixer_auto_out_0_a_bits_size),
    .auto_out_0_a_bits_source(fixer_auto_out_0_a_bits_source),
    .auto_out_0_a_bits_address(fixer_auto_out_0_a_bits_address),
    .auto_out_0_a_bits_user_amba_prot_bufferable(fixer_auto_out_0_a_bits_user_amba_prot_bufferable),
    .auto_out_0_a_bits_user_amba_prot_modifiable(fixer_auto_out_0_a_bits_user_amba_prot_modifiable),
    .auto_out_0_a_bits_user_amba_prot_readalloc(fixer_auto_out_0_a_bits_user_amba_prot_readalloc),
    .auto_out_0_a_bits_user_amba_prot_writealloc(fixer_auto_out_0_a_bits_user_amba_prot_writealloc),
    .auto_out_0_a_bits_user_amba_prot_privileged(fixer_auto_out_0_a_bits_user_amba_prot_privileged),
    .auto_out_0_a_bits_user_amba_prot_secure(fixer_auto_out_0_a_bits_user_amba_prot_secure),
    .auto_out_0_a_bits_user_amba_prot_fetch(fixer_auto_out_0_a_bits_user_amba_prot_fetch),
    .auto_out_0_a_bits_mask(fixer_auto_out_0_a_bits_mask),
    .auto_out_0_a_bits_data(fixer_auto_out_0_a_bits_data),
    .auto_out_0_d_ready(fixer_auto_out_0_d_ready),
    .auto_out_0_d_valid(fixer_auto_out_0_d_valid),
    .auto_out_0_d_bits_opcode(fixer_auto_out_0_d_bits_opcode),
    .auto_out_0_d_bits_param(fixer_auto_out_0_d_bits_param),
    .auto_out_0_d_bits_size(fixer_auto_out_0_d_bits_size),
    .auto_out_0_d_bits_source(fixer_auto_out_0_d_bits_source),
    .auto_out_0_d_bits_sink(fixer_auto_out_0_d_bits_sink),
    .auto_out_0_d_bits_denied(fixer_auto_out_0_d_bits_denied),
    .auto_out_0_d_bits_data(fixer_auto_out_0_d_bits_data),
    .auto_out_0_d_bits_corrupt(fixer_auto_out_0_d_bits_corrupt)
  );
  RHEA__TLInterconnectCoupler coupler_to_l2_lim ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_to_l2_lim_rf_reset),
    .clock(coupler_to_l2_lim_clock),
    .reset(coupler_to_l2_lim_reset),
    .auto_buffer_out_a_ready(coupler_to_l2_lim_auto_buffer_out_a_ready),
    .auto_buffer_out_a_valid(coupler_to_l2_lim_auto_buffer_out_a_valid),
    .auto_buffer_out_a_bits_opcode(coupler_to_l2_lim_auto_buffer_out_a_bits_opcode),
    .auto_buffer_out_a_bits_param(coupler_to_l2_lim_auto_buffer_out_a_bits_param),
    .auto_buffer_out_a_bits_size(coupler_to_l2_lim_auto_buffer_out_a_bits_size),
    .auto_buffer_out_a_bits_source(coupler_to_l2_lim_auto_buffer_out_a_bits_source),
    .auto_buffer_out_a_bits_address(coupler_to_l2_lim_auto_buffer_out_a_bits_address),
    .auto_buffer_out_a_bits_mask(coupler_to_l2_lim_auto_buffer_out_a_bits_mask),
    .auto_buffer_out_a_bits_data(coupler_to_l2_lim_auto_buffer_out_a_bits_data),
    .auto_buffer_out_a_bits_corrupt(coupler_to_l2_lim_auto_buffer_out_a_bits_corrupt),
    .auto_buffer_out_d_ready(coupler_to_l2_lim_auto_buffer_out_d_ready),
    .auto_buffer_out_d_valid(coupler_to_l2_lim_auto_buffer_out_d_valid),
    .auto_buffer_out_d_bits_opcode(coupler_to_l2_lim_auto_buffer_out_d_bits_opcode),
    .auto_buffer_out_d_bits_size(coupler_to_l2_lim_auto_buffer_out_d_bits_size),
    .auto_buffer_out_d_bits_source(coupler_to_l2_lim_auto_buffer_out_d_bits_source),
    .auto_buffer_out_d_bits_data(coupler_to_l2_lim_auto_buffer_out_d_bits_data),
    .auto_buffer_out_d_bits_corrupt(coupler_to_l2_lim_auto_buffer_out_d_bits_corrupt),
    .auto_tl_in_a_ready(coupler_to_l2_lim_auto_tl_in_a_ready),
    .auto_tl_in_a_valid(coupler_to_l2_lim_auto_tl_in_a_valid),
    .auto_tl_in_a_bits_opcode(coupler_to_l2_lim_auto_tl_in_a_bits_opcode),
    .auto_tl_in_a_bits_param(coupler_to_l2_lim_auto_tl_in_a_bits_param),
    .auto_tl_in_a_bits_size(coupler_to_l2_lim_auto_tl_in_a_bits_size),
    .auto_tl_in_a_bits_source(coupler_to_l2_lim_auto_tl_in_a_bits_source),
    .auto_tl_in_a_bits_address(coupler_to_l2_lim_auto_tl_in_a_bits_address),
    .auto_tl_in_a_bits_mask(coupler_to_l2_lim_auto_tl_in_a_bits_mask),
    .auto_tl_in_a_bits_data(coupler_to_l2_lim_auto_tl_in_a_bits_data),
    .auto_tl_in_a_bits_corrupt(coupler_to_l2_lim_auto_tl_in_a_bits_corrupt),
    .auto_tl_in_d_ready(coupler_to_l2_lim_auto_tl_in_d_ready),
    .auto_tl_in_d_valid(coupler_to_l2_lim_auto_tl_in_d_valid),
    .auto_tl_in_d_bits_opcode(coupler_to_l2_lim_auto_tl_in_d_bits_opcode),
    .auto_tl_in_d_bits_size(coupler_to_l2_lim_auto_tl_in_d_bits_size),
    .auto_tl_in_d_bits_source(coupler_to_l2_lim_auto_tl_in_d_bits_source),
    .auto_tl_in_d_bits_data(coupler_to_l2_lim_auto_tl_in_d_bits_data),
    .auto_tl_in_d_bits_corrupt(coupler_to_l2_lim_auto_tl_in_d_bits_corrupt)
  );
  RHEA__TLInterconnectCoupler_1 coupler_to_bus_named_subsystem_cbus_address_adjuster ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_to_bus_named_subsystem_cbus_address_adjuster_rf_reset),
    .clock(coupler_to_bus_named_subsystem_cbus_address_adjuster_clock),
    .reset(coupler_to_bus_named_subsystem_cbus_address_adjuster_reset),
    .auto_widget_in_a_ready(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_ready),
    .auto_widget_in_a_valid(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_valid),
    .auto_widget_in_a_bits_opcode(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_opcode),
    .auto_widget_in_a_bits_param(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_param),
    .auto_widget_in_a_bits_size(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_size),
    .auto_widget_in_a_bits_source(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_source),
    .auto_widget_in_a_bits_address(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_address),
    .auto_widget_in_a_bits_user_amba_prot_bufferable(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_bufferable),
    .auto_widget_in_a_bits_user_amba_prot_modifiable(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_modifiable),
    .auto_widget_in_a_bits_user_amba_prot_readalloc(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_readalloc),
    .auto_widget_in_a_bits_user_amba_prot_writealloc(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_writealloc),
    .auto_widget_in_a_bits_user_amba_prot_privileged(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_privileged),
    .auto_widget_in_a_bits_user_amba_prot_secure(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_secure),
    .auto_widget_in_a_bits_user_amba_prot_fetch(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_fetch),
    .auto_widget_in_a_bits_mask(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_mask),
    .auto_widget_in_a_bits_data(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_data),
    .auto_widget_in_a_bits_corrupt(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_corrupt),
    .auto_widget_in_d_ready(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_ready),
    .auto_widget_in_d_valid(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_valid),
    .auto_widget_in_d_bits_opcode(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_opcode),
    .auto_widget_in_d_bits_param(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_param),
    .auto_widget_in_d_bits_size(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_size),
    .auto_widget_in_d_bits_source(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_source),
    .auto_widget_in_d_bits_sink(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_sink),
    .auto_widget_in_d_bits_denied(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_denied),
    .auto_widget_in_d_bits_data(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_data),
    .auto_widget_in_d_bits_corrupt(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_corrupt),
    .auto_bus_xing_out_a_ready(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_ready),
    .auto_bus_xing_out_a_valid(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_valid),
    .auto_bus_xing_out_a_bits_opcode(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_opcode),
    .auto_bus_xing_out_a_bits_param(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_param)
      ,
    .auto_bus_xing_out_a_bits_size(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_size),
    .auto_bus_xing_out_a_bits_source(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_source),
    .auto_bus_xing_out_a_bits_address(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_address),
    .auto_bus_xing_out_a_bits_user_amba_prot_bufferable(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_bufferable),
    .auto_bus_xing_out_a_bits_user_amba_prot_modifiable(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_modifiable),
    .auto_bus_xing_out_a_bits_user_amba_prot_readalloc(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_readalloc),
    .auto_bus_xing_out_a_bits_user_amba_prot_writealloc(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_writealloc),
    .auto_bus_xing_out_a_bits_user_amba_prot_privileged(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_privileged),
    .auto_bus_xing_out_a_bits_user_amba_prot_secure(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_secure),
    .auto_bus_xing_out_a_bits_user_amba_prot_fetch(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_fetch),
    .auto_bus_xing_out_a_bits_mask(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_mask),
    .auto_bus_xing_out_a_bits_data(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_data),
    .auto_bus_xing_out_a_bits_corrupt(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_corrupt),
    .auto_bus_xing_out_d_ready(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_ready),
    .auto_bus_xing_out_d_valid(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_valid),
    .auto_bus_xing_out_d_bits_opcode(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_opcode),
    .auto_bus_xing_out_d_bits_param(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_param)
      ,
    .auto_bus_xing_out_d_bits_size(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_size),
    .auto_bus_xing_out_d_bits_source(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_source),
    .auto_bus_xing_out_d_bits_sink(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_sink),
    .auto_bus_xing_out_d_bits_denied(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_denied),
    .auto_bus_xing_out_d_bits_data(coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_data),
    .auto_bus_xing_out_d_bits_corrupt(
      coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_corrupt)
  );
  RHEA__TLInterconnectCoupler_2 coupler_from_bus_named_subsystem_fbus ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_from_bus_named_subsystem_fbus_rf_reset),
    .clock(coupler_from_bus_named_subsystem_fbus_clock),
    .reset(coupler_from_bus_named_subsystem_fbus_reset),
    .auto_widget_out_a_ready(coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_ready),
    .auto_widget_out_a_valid(coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_valid),
    .auto_widget_out_a_bits_opcode(coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_opcode),
    .auto_widget_out_a_bits_size(coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_size),
    .auto_widget_out_a_bits_source(coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_source),
    .auto_widget_out_a_bits_address(coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_address),
    .auto_widget_out_a_bits_user_amba_prot_bufferable(
      coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_bufferable),
    .auto_widget_out_a_bits_user_amba_prot_modifiable(
      coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_modifiable),
    .auto_widget_out_a_bits_user_amba_prot_readalloc(
      coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_readalloc),
    .auto_widget_out_a_bits_user_amba_prot_writealloc(
      coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_writealloc),
    .auto_widget_out_a_bits_user_amba_prot_privileged(
      coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_privileged),
    .auto_widget_out_a_bits_user_amba_prot_secure(
      coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_secure),
    .auto_widget_out_a_bits_user_amba_prot_fetch(
      coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_fetch),
    .auto_widget_out_a_bits_mask(coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_mask),
    .auto_widget_out_a_bits_data(coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_data),
    .auto_widget_out_d_ready(coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_ready),
    .auto_widget_out_d_valid(coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_valid),
    .auto_widget_out_d_bits_opcode(coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_opcode),
    .auto_widget_out_d_bits_param(coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_param),
    .auto_widget_out_d_bits_size(coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_size),
    .auto_widget_out_d_bits_source(coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_source),
    .auto_widget_out_d_bits_sink(coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_sink),
    .auto_widget_out_d_bits_denied(coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_denied),
    .auto_widget_out_d_bits_data(coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_data),
    .auto_widget_out_d_bits_corrupt(coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_corrupt),
    .auto_bus_xing_in_a_ready(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_ready),
    .auto_bus_xing_in_a_valid(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_valid),
    .auto_bus_xing_in_a_bits_opcode(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_opcode),
    .auto_bus_xing_in_a_bits_size(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_size),
    .auto_bus_xing_in_a_bits_source(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_source),
    .auto_bus_xing_in_a_bits_address(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_address),
    .auto_bus_xing_in_a_bits_user_amba_prot_bufferable(
      coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_bufferable),
    .auto_bus_xing_in_a_bits_user_amba_prot_modifiable(
      coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_modifiable),
    .auto_bus_xing_in_a_bits_user_amba_prot_readalloc(
      coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_readalloc),
    .auto_bus_xing_in_a_bits_user_amba_prot_writealloc(
      coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_writealloc),
    .auto_bus_xing_in_a_bits_user_amba_prot_privileged(
      coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_privileged),
    .auto_bus_xing_in_a_bits_user_amba_prot_secure(
      coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_secure),
    .auto_bus_xing_in_a_bits_user_amba_prot_fetch(
      coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_fetch),
    .auto_bus_xing_in_a_bits_mask(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_mask),
    .auto_bus_xing_in_a_bits_data(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_data),
    .auto_bus_xing_in_d_ready(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_ready),
    .auto_bus_xing_in_d_valid(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_valid),
    .auto_bus_xing_in_d_bits_opcode(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_opcode),
    .auto_bus_xing_in_d_bits_param(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_param),
    .auto_bus_xing_in_d_bits_size(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_size),
    .auto_bus_xing_in_d_bits_source(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_source),
    .auto_bus_xing_in_d_bits_sink(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_sink),
    .auto_bus_xing_in_d_bits_denied(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_denied),
    .auto_bus_xing_in_d_bits_data(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_data),
    .auto_bus_xing_in_d_bits_corrupt(coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_corrupt)
  );
  RHEA__TLInterconnectCoupler_4 coupler_from_tile ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_from_tile_rf_reset),
    .clock(coupler_from_tile_clock),
    .reset(coupler_from_tile_reset),
    .auto_tl_master_clock_xing_in_a_bits0_opcode(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_opcode),
    .auto_tl_master_clock_xing_in_a_bits0_param(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_param),
    .auto_tl_master_clock_xing_in_a_bits0_size(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_size),
    .auto_tl_master_clock_xing_in_a_bits0_source(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_source),
    .auto_tl_master_clock_xing_in_a_bits0_address(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_address),
    .auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_bufferable(
      coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_bufferable),
    .auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_modifiable(
      coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_modifiable),
    .auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_readalloc(
      coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_readalloc),
    .auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_writealloc(
      coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_writealloc),
    .auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_privileged(
      coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_privileged),
    .auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_secure(
      coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_secure),
    .auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_fetch(
      coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_fetch),
    .auto_tl_master_clock_xing_in_a_bits0_mask(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_mask),
    .auto_tl_master_clock_xing_in_a_bits0_data(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_data),
    .auto_tl_master_clock_xing_in_a_bits0_corrupt(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_corrupt),
    .auto_tl_master_clock_xing_in_a_bits1_opcode(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_opcode),
    .auto_tl_master_clock_xing_in_a_bits1_param(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_param),
    .auto_tl_master_clock_xing_in_a_bits1_size(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_size),
    .auto_tl_master_clock_xing_in_a_bits1_source(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_source),
    .auto_tl_master_clock_xing_in_a_bits1_address(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_address),
    .auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_bufferable(
      coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_bufferable),
    .auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_modifiable(
      coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_modifiable),
    .auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_readalloc(
      coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_readalloc),
    .auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_writealloc(
      coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_writealloc),
    .auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_privileged(
      coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_privileged),
    .auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_secure(
      coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_secure),
    .auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_fetch(
      coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_fetch),
    .auto_tl_master_clock_xing_in_a_bits1_mask(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_mask),
    .auto_tl_master_clock_xing_in_a_bits1_data(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_data),
    .auto_tl_master_clock_xing_in_a_bits1_corrupt(coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_corrupt),
    .auto_tl_master_clock_xing_in_a_valid(coupler_from_tile_auto_tl_master_clock_xing_in_a_valid),
    .auto_tl_master_clock_xing_in_a_source(coupler_from_tile_auto_tl_master_clock_xing_in_a_source),
    .auto_tl_master_clock_xing_in_a_ready(coupler_from_tile_auto_tl_master_clock_xing_in_a_ready),
    .auto_tl_master_clock_xing_in_a_sink(coupler_from_tile_auto_tl_master_clock_xing_in_a_sink),
    .auto_tl_master_clock_xing_in_b_bits0_opcode(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_opcode),
    .auto_tl_master_clock_xing_in_b_bits0_param(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_param),
    .auto_tl_master_clock_xing_in_b_bits0_size(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_size),
    .auto_tl_master_clock_xing_in_b_bits0_source(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_source),
    .auto_tl_master_clock_xing_in_b_bits0_address(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_address),
    .auto_tl_master_clock_xing_in_b_bits0_mask(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_mask),
    .auto_tl_master_clock_xing_in_b_bits0_data(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_data),
    .auto_tl_master_clock_xing_in_b_bits0_corrupt(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_corrupt),
    .auto_tl_master_clock_xing_in_b_bits1_opcode(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_opcode),
    .auto_tl_master_clock_xing_in_b_bits1_param(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_param),
    .auto_tl_master_clock_xing_in_b_bits1_size(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_size),
    .auto_tl_master_clock_xing_in_b_bits1_source(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_source),
    .auto_tl_master_clock_xing_in_b_bits1_address(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_address),
    .auto_tl_master_clock_xing_in_b_bits1_mask(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_mask),
    .auto_tl_master_clock_xing_in_b_bits1_data(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_data),
    .auto_tl_master_clock_xing_in_b_bits1_corrupt(coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_corrupt),
    .auto_tl_master_clock_xing_in_b_valid(coupler_from_tile_auto_tl_master_clock_xing_in_b_valid),
    .auto_tl_master_clock_xing_in_b_source(coupler_from_tile_auto_tl_master_clock_xing_in_b_source),
    .auto_tl_master_clock_xing_in_b_ready(coupler_from_tile_auto_tl_master_clock_xing_in_b_ready),
    .auto_tl_master_clock_xing_in_b_sink(coupler_from_tile_auto_tl_master_clock_xing_in_b_sink),
    .auto_tl_master_clock_xing_in_c_bits0_opcode(coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_opcode),
    .auto_tl_master_clock_xing_in_c_bits0_param(coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_param),
    .auto_tl_master_clock_xing_in_c_bits0_size(coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_size),
    .auto_tl_master_clock_xing_in_c_bits0_source(coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_source),
    .auto_tl_master_clock_xing_in_c_bits0_address(coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_address),
    .auto_tl_master_clock_xing_in_c_bits0_data(coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_data),
    .auto_tl_master_clock_xing_in_c_bits0_corrupt(coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_corrupt),
    .auto_tl_master_clock_xing_in_c_bits1_opcode(coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_opcode),
    .auto_tl_master_clock_xing_in_c_bits1_param(coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_param),
    .auto_tl_master_clock_xing_in_c_bits1_size(coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_size),
    .auto_tl_master_clock_xing_in_c_bits1_source(coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_source),
    .auto_tl_master_clock_xing_in_c_bits1_address(coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_address),
    .auto_tl_master_clock_xing_in_c_bits1_data(coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_data),
    .auto_tl_master_clock_xing_in_c_bits1_corrupt(coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_corrupt),
    .auto_tl_master_clock_xing_in_c_valid(coupler_from_tile_auto_tl_master_clock_xing_in_c_valid),
    .auto_tl_master_clock_xing_in_c_source(coupler_from_tile_auto_tl_master_clock_xing_in_c_source),
    .auto_tl_master_clock_xing_in_c_ready(coupler_from_tile_auto_tl_master_clock_xing_in_c_ready),
    .auto_tl_master_clock_xing_in_c_sink(coupler_from_tile_auto_tl_master_clock_xing_in_c_sink),
    .auto_tl_master_clock_xing_in_d_bits0_opcode(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_opcode),
    .auto_tl_master_clock_xing_in_d_bits0_param(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_param),
    .auto_tl_master_clock_xing_in_d_bits0_size(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_size),
    .auto_tl_master_clock_xing_in_d_bits0_source(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_source),
    .auto_tl_master_clock_xing_in_d_bits0_sink(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_sink),
    .auto_tl_master_clock_xing_in_d_bits0_denied(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_denied),
    .auto_tl_master_clock_xing_in_d_bits0_data(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_data),
    .auto_tl_master_clock_xing_in_d_bits0_corrupt(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_corrupt),
    .auto_tl_master_clock_xing_in_d_bits1_opcode(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_opcode),
    .auto_tl_master_clock_xing_in_d_bits1_param(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_param),
    .auto_tl_master_clock_xing_in_d_bits1_size(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_size),
    .auto_tl_master_clock_xing_in_d_bits1_source(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_source),
    .auto_tl_master_clock_xing_in_d_bits1_sink(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_sink),
    .auto_tl_master_clock_xing_in_d_bits1_denied(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_denied),
    .auto_tl_master_clock_xing_in_d_bits1_data(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_data),
    .auto_tl_master_clock_xing_in_d_bits1_corrupt(coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_corrupt),
    .auto_tl_master_clock_xing_in_d_valid(coupler_from_tile_auto_tl_master_clock_xing_in_d_valid),
    .auto_tl_master_clock_xing_in_d_source(coupler_from_tile_auto_tl_master_clock_xing_in_d_source),
    .auto_tl_master_clock_xing_in_d_ready(coupler_from_tile_auto_tl_master_clock_xing_in_d_ready),
    .auto_tl_master_clock_xing_in_d_sink(coupler_from_tile_auto_tl_master_clock_xing_in_d_sink),
    .auto_tl_master_clock_xing_in_e_bits0_sink(coupler_from_tile_auto_tl_master_clock_xing_in_e_bits0_sink),
    .auto_tl_master_clock_xing_in_e_bits1_sink(coupler_from_tile_auto_tl_master_clock_xing_in_e_bits1_sink),
    .auto_tl_master_clock_xing_in_e_valid(coupler_from_tile_auto_tl_master_clock_xing_in_e_valid),
    .auto_tl_master_clock_xing_in_e_source(coupler_from_tile_auto_tl_master_clock_xing_in_e_source),
    .auto_tl_master_clock_xing_in_e_ready(coupler_from_tile_auto_tl_master_clock_xing_in_e_ready),
    .auto_tl_master_clock_xing_in_e_sink(coupler_from_tile_auto_tl_master_clock_xing_in_e_sink),
    .auto_tl_out_a_ready(coupler_from_tile_auto_tl_out_a_ready),
    .auto_tl_out_a_valid(coupler_from_tile_auto_tl_out_a_valid),
    .auto_tl_out_a_bits_opcode(coupler_from_tile_auto_tl_out_a_bits_opcode),
    .auto_tl_out_a_bits_param(coupler_from_tile_auto_tl_out_a_bits_param),
    .auto_tl_out_a_bits_size(coupler_from_tile_auto_tl_out_a_bits_size),
    .auto_tl_out_a_bits_source(coupler_from_tile_auto_tl_out_a_bits_source),
    .auto_tl_out_a_bits_address(coupler_from_tile_auto_tl_out_a_bits_address),
    .auto_tl_out_a_bits_user_amba_prot_bufferable(coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_bufferable),
    .auto_tl_out_a_bits_user_amba_prot_modifiable(coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_modifiable),
    .auto_tl_out_a_bits_user_amba_prot_readalloc(coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_readalloc),
    .auto_tl_out_a_bits_user_amba_prot_writealloc(coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_writealloc),
    .auto_tl_out_a_bits_user_amba_prot_privileged(coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_privileged),
    .auto_tl_out_a_bits_user_amba_prot_secure(coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_secure),
    .auto_tl_out_a_bits_user_amba_prot_fetch(coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_fetch),
    .auto_tl_out_a_bits_mask(coupler_from_tile_auto_tl_out_a_bits_mask),
    .auto_tl_out_a_bits_data(coupler_from_tile_auto_tl_out_a_bits_data),
    .auto_tl_out_a_bits_corrupt(coupler_from_tile_auto_tl_out_a_bits_corrupt),
    .auto_tl_out_b_ready(coupler_from_tile_auto_tl_out_b_ready),
    .auto_tl_out_b_valid(coupler_from_tile_auto_tl_out_b_valid),
    .auto_tl_out_b_bits_opcode(coupler_from_tile_auto_tl_out_b_bits_opcode),
    .auto_tl_out_b_bits_param(coupler_from_tile_auto_tl_out_b_bits_param),
    .auto_tl_out_b_bits_size(coupler_from_tile_auto_tl_out_b_bits_size),
    .auto_tl_out_b_bits_source(coupler_from_tile_auto_tl_out_b_bits_source),
    .auto_tl_out_b_bits_address(coupler_from_tile_auto_tl_out_b_bits_address),
    .auto_tl_out_b_bits_mask(coupler_from_tile_auto_tl_out_b_bits_mask),
    .auto_tl_out_b_bits_data(coupler_from_tile_auto_tl_out_b_bits_data),
    .auto_tl_out_b_bits_corrupt(coupler_from_tile_auto_tl_out_b_bits_corrupt),
    .auto_tl_out_c_ready(coupler_from_tile_auto_tl_out_c_ready),
    .auto_tl_out_c_valid(coupler_from_tile_auto_tl_out_c_valid),
    .auto_tl_out_c_bits_opcode(coupler_from_tile_auto_tl_out_c_bits_opcode),
    .auto_tl_out_c_bits_param(coupler_from_tile_auto_tl_out_c_bits_param),
    .auto_tl_out_c_bits_size(coupler_from_tile_auto_tl_out_c_bits_size),
    .auto_tl_out_c_bits_source(coupler_from_tile_auto_tl_out_c_bits_source),
    .auto_tl_out_c_bits_address(coupler_from_tile_auto_tl_out_c_bits_address),
    .auto_tl_out_c_bits_data(coupler_from_tile_auto_tl_out_c_bits_data),
    .auto_tl_out_c_bits_corrupt(coupler_from_tile_auto_tl_out_c_bits_corrupt),
    .auto_tl_out_d_ready(coupler_from_tile_auto_tl_out_d_ready),
    .auto_tl_out_d_valid(coupler_from_tile_auto_tl_out_d_valid),
    .auto_tl_out_d_bits_opcode(coupler_from_tile_auto_tl_out_d_bits_opcode),
    .auto_tl_out_d_bits_param(coupler_from_tile_auto_tl_out_d_bits_param),
    .auto_tl_out_d_bits_size(coupler_from_tile_auto_tl_out_d_bits_size),
    .auto_tl_out_d_bits_source(coupler_from_tile_auto_tl_out_d_bits_source),
    .auto_tl_out_d_bits_sink(coupler_from_tile_auto_tl_out_d_bits_sink),
    .auto_tl_out_d_bits_denied(coupler_from_tile_auto_tl_out_d_bits_denied),
    .auto_tl_out_d_bits_data(coupler_from_tile_auto_tl_out_d_bits_data),
    .auto_tl_out_d_bits_corrupt(coupler_from_tile_auto_tl_out_d_bits_corrupt),
    .auto_tl_out_e_ready(coupler_from_tile_auto_tl_out_e_ready),
    .auto_tl_out_e_valid(coupler_from_tile_auto_tl_out_e_valid),
    .auto_tl_out_e_bits_sink(coupler_from_tile_auto_tl_out_e_bits_sink)
  );
  RHEA__TLInterconnectCoupler_4 coupler_from_tile_1 ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_from_tile_1_rf_reset),
    .clock(coupler_from_tile_1_clock),
    .reset(coupler_from_tile_1_reset),
    .auto_tl_master_clock_xing_in_a_bits0_opcode(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_opcode),
    .auto_tl_master_clock_xing_in_a_bits0_param(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_param),
    .auto_tl_master_clock_xing_in_a_bits0_size(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_size),
    .auto_tl_master_clock_xing_in_a_bits0_source(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_source),
    .auto_tl_master_clock_xing_in_a_bits0_address(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_address),
    .auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_bufferable(
      coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_bufferable),
    .auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_modifiable(
      coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_modifiable),
    .auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_readalloc(
      coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_readalloc),
    .auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_writealloc(
      coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_writealloc),
    .auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_privileged(
      coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_privileged),
    .auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_secure(
      coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_secure),
    .auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_fetch(
      coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_fetch),
    .auto_tl_master_clock_xing_in_a_bits0_mask(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_mask),
    .auto_tl_master_clock_xing_in_a_bits0_data(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_data),
    .auto_tl_master_clock_xing_in_a_bits0_corrupt(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_corrupt),
    .auto_tl_master_clock_xing_in_a_bits1_opcode(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_opcode),
    .auto_tl_master_clock_xing_in_a_bits1_param(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_param),
    .auto_tl_master_clock_xing_in_a_bits1_size(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_size),
    .auto_tl_master_clock_xing_in_a_bits1_source(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_source),
    .auto_tl_master_clock_xing_in_a_bits1_address(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_address),
    .auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_bufferable(
      coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_bufferable),
    .auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_modifiable(
      coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_modifiable),
    .auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_readalloc(
      coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_readalloc),
    .auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_writealloc(
      coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_writealloc),
    .auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_privileged(
      coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_privileged),
    .auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_secure(
      coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_secure),
    .auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_fetch(
      coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_fetch),
    .auto_tl_master_clock_xing_in_a_bits1_mask(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_mask),
    .auto_tl_master_clock_xing_in_a_bits1_data(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_data),
    .auto_tl_master_clock_xing_in_a_bits1_corrupt(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_corrupt),
    .auto_tl_master_clock_xing_in_a_valid(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_valid),
    .auto_tl_master_clock_xing_in_a_source(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_source),
    .auto_tl_master_clock_xing_in_a_ready(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_ready),
    .auto_tl_master_clock_xing_in_a_sink(coupler_from_tile_1_auto_tl_master_clock_xing_in_a_sink),
    .auto_tl_master_clock_xing_in_b_bits0_opcode(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_opcode),
    .auto_tl_master_clock_xing_in_b_bits0_param(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_param),
    .auto_tl_master_clock_xing_in_b_bits0_size(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_size),
    .auto_tl_master_clock_xing_in_b_bits0_source(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_source),
    .auto_tl_master_clock_xing_in_b_bits0_address(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_address),
    .auto_tl_master_clock_xing_in_b_bits0_mask(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_mask),
    .auto_tl_master_clock_xing_in_b_bits0_data(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_data),
    .auto_tl_master_clock_xing_in_b_bits0_corrupt(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_corrupt),
    .auto_tl_master_clock_xing_in_b_bits1_opcode(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_opcode),
    .auto_tl_master_clock_xing_in_b_bits1_param(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_param),
    .auto_tl_master_clock_xing_in_b_bits1_size(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_size),
    .auto_tl_master_clock_xing_in_b_bits1_source(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_source),
    .auto_tl_master_clock_xing_in_b_bits1_address(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_address),
    .auto_tl_master_clock_xing_in_b_bits1_mask(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_mask),
    .auto_tl_master_clock_xing_in_b_bits1_data(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_data),
    .auto_tl_master_clock_xing_in_b_bits1_corrupt(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_corrupt),
    .auto_tl_master_clock_xing_in_b_valid(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_valid),
    .auto_tl_master_clock_xing_in_b_source(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_source),
    .auto_tl_master_clock_xing_in_b_ready(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_ready),
    .auto_tl_master_clock_xing_in_b_sink(coupler_from_tile_1_auto_tl_master_clock_xing_in_b_sink),
    .auto_tl_master_clock_xing_in_c_bits0_opcode(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_opcode),
    .auto_tl_master_clock_xing_in_c_bits0_param(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_param),
    .auto_tl_master_clock_xing_in_c_bits0_size(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_size),
    .auto_tl_master_clock_xing_in_c_bits0_source(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_source),
    .auto_tl_master_clock_xing_in_c_bits0_address(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_address),
    .auto_tl_master_clock_xing_in_c_bits0_data(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_data),
    .auto_tl_master_clock_xing_in_c_bits0_corrupt(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_corrupt),
    .auto_tl_master_clock_xing_in_c_bits1_opcode(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_opcode),
    .auto_tl_master_clock_xing_in_c_bits1_param(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_param),
    .auto_tl_master_clock_xing_in_c_bits1_size(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_size),
    .auto_tl_master_clock_xing_in_c_bits1_source(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_source),
    .auto_tl_master_clock_xing_in_c_bits1_address(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_address),
    .auto_tl_master_clock_xing_in_c_bits1_data(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_data),
    .auto_tl_master_clock_xing_in_c_bits1_corrupt(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_corrupt),
    .auto_tl_master_clock_xing_in_c_valid(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_valid),
    .auto_tl_master_clock_xing_in_c_source(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_source),
    .auto_tl_master_clock_xing_in_c_ready(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_ready),
    .auto_tl_master_clock_xing_in_c_sink(coupler_from_tile_1_auto_tl_master_clock_xing_in_c_sink),
    .auto_tl_master_clock_xing_in_d_bits0_opcode(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_opcode),
    .auto_tl_master_clock_xing_in_d_bits0_param(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_param),
    .auto_tl_master_clock_xing_in_d_bits0_size(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_size),
    .auto_tl_master_clock_xing_in_d_bits0_source(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_source),
    .auto_tl_master_clock_xing_in_d_bits0_sink(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_sink),
    .auto_tl_master_clock_xing_in_d_bits0_denied(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_denied),
    .auto_tl_master_clock_xing_in_d_bits0_data(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_data),
    .auto_tl_master_clock_xing_in_d_bits0_corrupt(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_corrupt),
    .auto_tl_master_clock_xing_in_d_bits1_opcode(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_opcode),
    .auto_tl_master_clock_xing_in_d_bits1_param(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_param),
    .auto_tl_master_clock_xing_in_d_bits1_size(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_size),
    .auto_tl_master_clock_xing_in_d_bits1_source(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_source),
    .auto_tl_master_clock_xing_in_d_bits1_sink(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_sink),
    .auto_tl_master_clock_xing_in_d_bits1_denied(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_denied),
    .auto_tl_master_clock_xing_in_d_bits1_data(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_data),
    .auto_tl_master_clock_xing_in_d_bits1_corrupt(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_corrupt),
    .auto_tl_master_clock_xing_in_d_valid(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_valid),
    .auto_tl_master_clock_xing_in_d_source(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_source),
    .auto_tl_master_clock_xing_in_d_ready(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_ready),
    .auto_tl_master_clock_xing_in_d_sink(coupler_from_tile_1_auto_tl_master_clock_xing_in_d_sink),
    .auto_tl_master_clock_xing_in_e_bits0_sink(coupler_from_tile_1_auto_tl_master_clock_xing_in_e_bits0_sink),
    .auto_tl_master_clock_xing_in_e_bits1_sink(coupler_from_tile_1_auto_tl_master_clock_xing_in_e_bits1_sink),
    .auto_tl_master_clock_xing_in_e_valid(coupler_from_tile_1_auto_tl_master_clock_xing_in_e_valid),
    .auto_tl_master_clock_xing_in_e_source(coupler_from_tile_1_auto_tl_master_clock_xing_in_e_source),
    .auto_tl_master_clock_xing_in_e_ready(coupler_from_tile_1_auto_tl_master_clock_xing_in_e_ready),
    .auto_tl_master_clock_xing_in_e_sink(coupler_from_tile_1_auto_tl_master_clock_xing_in_e_sink),
    .auto_tl_out_a_ready(coupler_from_tile_1_auto_tl_out_a_ready),
    .auto_tl_out_a_valid(coupler_from_tile_1_auto_tl_out_a_valid),
    .auto_tl_out_a_bits_opcode(coupler_from_tile_1_auto_tl_out_a_bits_opcode),
    .auto_tl_out_a_bits_param(coupler_from_tile_1_auto_tl_out_a_bits_param),
    .auto_tl_out_a_bits_size(coupler_from_tile_1_auto_tl_out_a_bits_size),
    .auto_tl_out_a_bits_source(coupler_from_tile_1_auto_tl_out_a_bits_source),
    .auto_tl_out_a_bits_address(coupler_from_tile_1_auto_tl_out_a_bits_address),
    .auto_tl_out_a_bits_user_amba_prot_bufferable(coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_bufferable),
    .auto_tl_out_a_bits_user_amba_prot_modifiable(coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_modifiable),
    .auto_tl_out_a_bits_user_amba_prot_readalloc(coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_readalloc),
    .auto_tl_out_a_bits_user_amba_prot_writealloc(coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_writealloc),
    .auto_tl_out_a_bits_user_amba_prot_privileged(coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_privileged),
    .auto_tl_out_a_bits_user_amba_prot_secure(coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_secure),
    .auto_tl_out_a_bits_user_amba_prot_fetch(coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_fetch),
    .auto_tl_out_a_bits_mask(coupler_from_tile_1_auto_tl_out_a_bits_mask),
    .auto_tl_out_a_bits_data(coupler_from_tile_1_auto_tl_out_a_bits_data),
    .auto_tl_out_a_bits_corrupt(coupler_from_tile_1_auto_tl_out_a_bits_corrupt),
    .auto_tl_out_b_ready(coupler_from_tile_1_auto_tl_out_b_ready),
    .auto_tl_out_b_valid(coupler_from_tile_1_auto_tl_out_b_valid),
    .auto_tl_out_b_bits_opcode(coupler_from_tile_1_auto_tl_out_b_bits_opcode),
    .auto_tl_out_b_bits_param(coupler_from_tile_1_auto_tl_out_b_bits_param),
    .auto_tl_out_b_bits_size(coupler_from_tile_1_auto_tl_out_b_bits_size),
    .auto_tl_out_b_bits_source(coupler_from_tile_1_auto_tl_out_b_bits_source),
    .auto_tl_out_b_bits_address(coupler_from_tile_1_auto_tl_out_b_bits_address),
    .auto_tl_out_b_bits_mask(coupler_from_tile_1_auto_tl_out_b_bits_mask),
    .auto_tl_out_b_bits_data(coupler_from_tile_1_auto_tl_out_b_bits_data),
    .auto_tl_out_b_bits_corrupt(coupler_from_tile_1_auto_tl_out_b_bits_corrupt),
    .auto_tl_out_c_ready(coupler_from_tile_1_auto_tl_out_c_ready),
    .auto_tl_out_c_valid(coupler_from_tile_1_auto_tl_out_c_valid),
    .auto_tl_out_c_bits_opcode(coupler_from_tile_1_auto_tl_out_c_bits_opcode),
    .auto_tl_out_c_bits_param(coupler_from_tile_1_auto_tl_out_c_bits_param),
    .auto_tl_out_c_bits_size(coupler_from_tile_1_auto_tl_out_c_bits_size),
    .auto_tl_out_c_bits_source(coupler_from_tile_1_auto_tl_out_c_bits_source),
    .auto_tl_out_c_bits_address(coupler_from_tile_1_auto_tl_out_c_bits_address),
    .auto_tl_out_c_bits_data(coupler_from_tile_1_auto_tl_out_c_bits_data),
    .auto_tl_out_c_bits_corrupt(coupler_from_tile_1_auto_tl_out_c_bits_corrupt),
    .auto_tl_out_d_ready(coupler_from_tile_1_auto_tl_out_d_ready),
    .auto_tl_out_d_valid(coupler_from_tile_1_auto_tl_out_d_valid),
    .auto_tl_out_d_bits_opcode(coupler_from_tile_1_auto_tl_out_d_bits_opcode),
    .auto_tl_out_d_bits_param(coupler_from_tile_1_auto_tl_out_d_bits_param),
    .auto_tl_out_d_bits_size(coupler_from_tile_1_auto_tl_out_d_bits_size),
    .auto_tl_out_d_bits_source(coupler_from_tile_1_auto_tl_out_d_bits_source),
    .auto_tl_out_d_bits_sink(coupler_from_tile_1_auto_tl_out_d_bits_sink),
    .auto_tl_out_d_bits_denied(coupler_from_tile_1_auto_tl_out_d_bits_denied),
    .auto_tl_out_d_bits_data(coupler_from_tile_1_auto_tl_out_d_bits_data),
    .auto_tl_out_d_bits_corrupt(coupler_from_tile_1_auto_tl_out_d_bits_corrupt),
    .auto_tl_out_e_ready(coupler_from_tile_1_auto_tl_out_e_ready),
    .auto_tl_out_e_valid(coupler_from_tile_1_auto_tl_out_e_valid),
    .auto_tl_out_e_bits_sink(coupler_from_tile_1_auto_tl_out_e_bits_sink)
  );
  RHEA__TLInterconnectCoupler_6 coupler_to_port_named_axi4_sys_port ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_to_port_named_axi4_sys_port_rf_reset),
    .clock(coupler_to_port_named_axi4_sys_port_clock),
    .reset(coupler_to_port_named_axi4_sys_port_reset),
    .auto_bridge_for_axi4_sys_port_axi4_out_aw_ready(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_ready),
    .auto_bridge_for_axi4_sys_port_axi4_out_aw_valid(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_valid),
    .auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_id(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_id),
    .auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_addr(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_addr),
    .auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_len(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_len),
    .auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_size(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_size),
    .auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_cache(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_cache),
    .auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_prot(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_prot),
    .auto_bridge_for_axi4_sys_port_axi4_out_w_ready(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_w_ready),
    .auto_bridge_for_axi4_sys_port_axi4_out_w_valid(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_w_valid),
    .auto_bridge_for_axi4_sys_port_axi4_out_w_bits_data(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_w_bits_data),
    .auto_bridge_for_axi4_sys_port_axi4_out_w_bits_strb(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_w_bits_strb),
    .auto_bridge_for_axi4_sys_port_axi4_out_w_bits_last(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_w_bits_last),
    .auto_bridge_for_axi4_sys_port_axi4_out_b_ready(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_b_ready),
    .auto_bridge_for_axi4_sys_port_axi4_out_b_valid(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_b_valid),
    .auto_bridge_for_axi4_sys_port_axi4_out_b_bits_id(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_b_bits_id),
    .auto_bridge_for_axi4_sys_port_axi4_out_b_bits_resp(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_b_bits_resp),
    .auto_bridge_for_axi4_sys_port_axi4_out_ar_ready(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_ready),
    .auto_bridge_for_axi4_sys_port_axi4_out_ar_valid(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_valid),
    .auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_id(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_id),
    .auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_addr(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_addr),
    .auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_len(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_len),
    .auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_size(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_size),
    .auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_cache(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_cache),
    .auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_prot(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_prot),
    .auto_bridge_for_axi4_sys_port_axi4_out_r_ready(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_ready),
    .auto_bridge_for_axi4_sys_port_axi4_out_r_valid(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_valid),
    .auto_bridge_for_axi4_sys_port_axi4_out_r_bits_id(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_bits_id),
    .auto_bridge_for_axi4_sys_port_axi4_out_r_bits_data(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_bits_data),
    .auto_bridge_for_axi4_sys_port_axi4_out_r_bits_resp(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_bits_resp),
    .auto_bridge_for_axi4_sys_port_axi4_out_r_bits_last(
      coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_bits_last),
    .auto_tl_in_a_ready(coupler_to_port_named_axi4_sys_port_auto_tl_in_a_ready),
    .auto_tl_in_a_valid(coupler_to_port_named_axi4_sys_port_auto_tl_in_a_valid),
    .auto_tl_in_a_bits_opcode(coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_opcode),
    .auto_tl_in_a_bits_param(coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_param),
    .auto_tl_in_a_bits_size(coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_size),
    .auto_tl_in_a_bits_source(coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_source),
    .auto_tl_in_a_bits_address(coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_address),
    .auto_tl_in_a_bits_user_amba_prot_bufferable(
      coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_bufferable),
    .auto_tl_in_a_bits_user_amba_prot_modifiable(
      coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_modifiable),
    .auto_tl_in_a_bits_user_amba_prot_readalloc(
      coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_readalloc),
    .auto_tl_in_a_bits_user_amba_prot_writealloc(
      coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_writealloc),
    .auto_tl_in_a_bits_user_amba_prot_privileged(
      coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_privileged),
    .auto_tl_in_a_bits_user_amba_prot_secure(coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_secure
      ),
    .auto_tl_in_a_bits_user_amba_prot_fetch(coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_fetch),
    .auto_tl_in_a_bits_mask(coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_mask),
    .auto_tl_in_a_bits_data(coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_data),
    .auto_tl_in_a_bits_corrupt(coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_corrupt),
    .auto_tl_in_d_ready(coupler_to_port_named_axi4_sys_port_auto_tl_in_d_ready),
    .auto_tl_in_d_valid(coupler_to_port_named_axi4_sys_port_auto_tl_in_d_valid),
    .auto_tl_in_d_bits_opcode(coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_opcode),
    .auto_tl_in_d_bits_size(coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_size),
    .auto_tl_in_d_bits_source(coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_source),
    .auto_tl_in_d_bits_denied(coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_denied),
    .auto_tl_in_d_bits_data(coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_data),
    .auto_tl_in_d_bits_corrupt(coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_corrupt)
  );
  assign subsystem_sbus_clock_groups_auto_out_3_member_subsystem_l2_2_clock =
    subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_9_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_out_3_member_subsystem_l2_2_reset =
    subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_9_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_out_3_member_subsystem_l2_0_clock =
    subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_7_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_out_3_member_subsystem_l2_0_reset =
    subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_7_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_out_2_member_subsystem_fbus_0_clock =
    subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_6_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_out_2_member_subsystem_fbus_0_reset =
    subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_6_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_out_1_member_subsystem_cbus_address_adjuster_1_clock =
    subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_4_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_out_1_member_subsystem_cbus_address_adjuster_1_reset =
    subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_4_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_core_1_clock =
    subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_core_1_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_core_1_reset =
    subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_core_1_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_core_0_clock =
    subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_core_0_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_core_0_reset =
    subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_core_0_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_0_clock =
    subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_0_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_0_reset =
    subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_0_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign clockGroup_auto_out_2_clock = clockGroup_auto_in_member_subsystem_sbus_core_1_clock; // @[ClockGroup.scala 8:11 LazyModule.scala 388:16]
  assign clockGroup_auto_out_2_reset = clockGroup_auto_in_member_subsystem_sbus_core_1_reset; // @[ClockGroup.scala 8:11 LazyModule.scala 388:16]
  assign clockGroup_auto_out_1_clock = clockGroup_auto_in_member_subsystem_sbus_core_0_clock; // @[ClockGroup.scala 8:11 LazyModule.scala 388:16]
  assign clockGroup_auto_out_1_reset = clockGroup_auto_in_member_subsystem_sbus_core_0_reset; // @[ClockGroup.scala 8:11 LazyModule.scala 388:16]
  assign clockGroup_auto_out_0_clock = clockGroup_auto_in_member_subsystem_sbus_0_clock; // @[ClockGroup.scala 8:11 LazyModule.scala 388:16]
  assign clockGroup_auto_out_0_reset = clockGroup_auto_in_member_subsystem_sbus_0_reset; // @[ClockGroup.scala 8:11 LazyModule.scala 388:16]
  assign system_bus_xbar_rf_reset = rf_reset;
  assign fixer_rf_reset = rf_reset;
  assign coupler_to_l2_lim_rf_reset = rf_reset;
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_rf_reset = rf_reset;
  assign coupler_from_bus_named_subsystem_fbus_rf_reset = rf_reset;
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_denied =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_e_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_denied =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_e_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_denied =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_e_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_denied =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_e_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_e_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_e_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_e_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_e_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_mask; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_sink; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_denied =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_denied; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_e_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_3_e_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_mask; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_sink; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_denied =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_denied; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_e_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_2_e_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_mask; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_sink; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_denied =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_denied; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_e_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_1_e_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_mask; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_sink; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_denied =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_denied; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_e_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_in_0_e_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_e_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_3_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_e_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_2_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_e_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_1_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_param =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_size =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_source =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_address =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_data =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_ready =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_e_valid =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_widget_auto_out_0_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_valid =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_mask; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_b_ready =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_valid =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_d_ready =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_e_valid =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_e_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_3_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_3_e_bits_sink; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_valid =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_mask; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_b_ready =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_valid =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_d_ready =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_e_valid =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_e_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_2_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_2_e_bits_sink; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_valid =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_mask; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_b_ready =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_valid =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_d_ready =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_e_valid =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_e_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_1_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_1_e_bits_sink; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_valid =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_mask; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_b_ready =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_valid =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_d_ready =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_e_valid =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_e_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_in_0_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_auto_widget_in_0_e_bits_sink; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_a_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_mask =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_b_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_c_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_sink =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_denied =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_d_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_3_e_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_a_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_mask =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_b_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_c_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_sink =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_denied =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_d_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_2_e_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_a_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_mask =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_b_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_c_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_sink =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_denied =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_d_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_1_e_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_a_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_mask =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_b_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_c_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_sink =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_denied =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_d_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_widget_auto_out_0_e_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_from_tile_rf_reset = rf_reset;
  assign coupler_from_tile_1_rf_reset = rf_reset;
  assign coupler_to_port_named_axi4_sys_port_rf_reset = rf_reset;
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_valid =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_bits_id =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_id; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_bits_addr =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_addr; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_bits_len =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_len; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_bits_size =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_bits_cache =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_cache; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_bits_prot =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_bits_prot; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_w_valid =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_w_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_w_bits_data =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_w_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_w_bits_strb =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_w_bits_strb; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_w_bits_last =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_w_bits_last; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_b_ready =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_b_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_valid =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_bits_id =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_id; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_bits_addr =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_addr; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_bits_len =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_len; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_bits_size =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_bits_cache =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_cache; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_bits_prot =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_bits_prot; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_r_ready =
    coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_a_ready =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_a_ready; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_a_sink =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_a_sink; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_opcode =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_opcode; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_param =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_param; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_size =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_size; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_source =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_source; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_address =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_address; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_mask =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_mask; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_data =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_data; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits0_corrupt =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits0_corrupt; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_opcode =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_opcode; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_param =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_param; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_size =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_size; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_source =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_source; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_address =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_address; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_mask =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_mask; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_data =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_data; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_bits1_corrupt =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_bits1_corrupt; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_valid =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_valid; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_b_source =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_b_source; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_c_ready =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_c_ready; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_c_sink =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_c_sink; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_opcode =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_opcode; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_param =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_param; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_size =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_size; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_source =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_source; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_sink =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_sink; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_denied =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_denied; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_data =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_data; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits0_corrupt =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits0_corrupt; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_opcode =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_opcode; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_param =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_param; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_size =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_size; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_source =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_source; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_sink =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_sink; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_denied =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_denied; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_data =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_data; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_bits1_corrupt =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_bits1_corrupt; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_valid =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_valid; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_d_source =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_d_source; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_e_ready =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_e_ready; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_1_e_sink =
    coupler_from_tile_1_auto_tl_master_clock_xing_in_e_sink; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_a_ready =
    coupler_from_tile_auto_tl_master_clock_xing_in_a_ready; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_a_sink = coupler_from_tile_auto_tl_master_clock_xing_in_a_sink
    ; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_opcode =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_opcode; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_param =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_param; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_size =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_size; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_source =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_source; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_address =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_address; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_mask =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_mask; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_data =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_data; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits0_corrupt =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits0_corrupt; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_opcode =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_opcode; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_param =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_param; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_size =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_size; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_source =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_source; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_address =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_address; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_mask =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_mask; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_data =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_data; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_bits1_corrupt =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_bits1_corrupt; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_valid =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_valid; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_b_source =
    coupler_from_tile_auto_tl_master_clock_xing_in_b_source; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_c_ready =
    coupler_from_tile_auto_tl_master_clock_xing_in_c_ready; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_c_sink = coupler_from_tile_auto_tl_master_clock_xing_in_c_sink
    ; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_opcode =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_opcode; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_param =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_param; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_size =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_size; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_source =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_source; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_sink =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_sink; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_denied =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_denied; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_data =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_data; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits0_corrupt =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits0_corrupt; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_opcode =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_opcode; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_param =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_param; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_size =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_size; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_source =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_source; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_sink =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_sink; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_denied =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_denied; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_data =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_data; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_bits1_corrupt =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_bits1_corrupt; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_valid =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_valid; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_d_source =
    coupler_from_tile_auto_tl_master_clock_xing_in_d_source; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_e_ready =
    coupler_from_tile_auto_tl_master_clock_xing_in_e_ready; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_tile_tl_master_clock_xing_in_0_e_sink = coupler_from_tile_auto_tl_master_clock_xing_in_e_sink
    ; // @[LazyModule.scala 388:16]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_e_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_e_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_e_bits_sink; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_e_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_e_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_e_bits_sink; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_e_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_e_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_e_bits_sink; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_mask =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_bits_opcode =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_bits_param =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_bits_size =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_bits_source =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_bits_address =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_bits_data =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_bits_corrupt =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_ready =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_e_valid =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_e_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_e_bits_sink =
    coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_e_bits_sink; // @[LazyModule.scala 390:12]
  assign auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_ready =
    coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_ready; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_valid =
    coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_valid; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_opcode =
    coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_param =
    coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_param; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_size =
    coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_size; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_source =
    coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_source; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_sink =
    coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_sink; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_denied =
    coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_denied; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_data =
    coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_data; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_bits_corrupt =
    coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_bits_corrupt; // @[LazyModule.scala 388:16]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_valid =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_opcode =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_param =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_size =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_source =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_address =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_user_amba_prot_fetch =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_mask =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_data =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_bits_corrupt =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_ready =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_lim_buffer_out_a_valid = coupler_to_l2_lim_auto_buffer_out_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_lim_buffer_out_a_bits_opcode = coupler_to_l2_lim_auto_buffer_out_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_lim_buffer_out_a_bits_param = coupler_to_l2_lim_auto_buffer_out_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_lim_buffer_out_a_bits_size = coupler_to_l2_lim_auto_buffer_out_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_lim_buffer_out_a_bits_source = coupler_to_l2_lim_auto_buffer_out_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_lim_buffer_out_a_bits_address = coupler_to_l2_lim_auto_buffer_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_lim_buffer_out_a_bits_mask = coupler_to_l2_lim_auto_buffer_out_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_lim_buffer_out_a_bits_data = coupler_to_l2_lim_auto_buffer_out_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_lim_buffer_out_a_bits_corrupt = coupler_to_l2_lim_auto_buffer_out_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_lim_buffer_out_d_ready = coupler_to_l2_lim_auto_buffer_out_d_ready; // @[LazyModule.scala 390:12]
  assign auto_clockGroup_out_1_clock = clockGroup_auto_out_2_clock; // @[LazyModule.scala 390:12]
  assign auto_clockGroup_out_1_reset = clockGroup_auto_out_2_reset; // @[LazyModule.scala 390:12]
  assign auto_clockGroup_out_0_clock = clockGroup_auto_out_1_clock; // @[LazyModule.scala 390:12]
  assign auto_clockGroup_out_0_reset = clockGroup_auto_out_1_reset; // @[LazyModule.scala 390:12]
  assign auto_subsystem_sbus_clock_groups_out_2_member_subsystem_l2_2_clock =
    subsystem_sbus_clock_groups_auto_out_3_member_subsystem_l2_2_clock; // @[LazyModule.scala 390:12]
  assign auto_subsystem_sbus_clock_groups_out_2_member_subsystem_l2_2_reset =
    subsystem_sbus_clock_groups_auto_out_3_member_subsystem_l2_2_reset; // @[LazyModule.scala 390:12]
  assign auto_subsystem_sbus_clock_groups_out_2_member_subsystem_l2_0_clock =
    subsystem_sbus_clock_groups_auto_out_3_member_subsystem_l2_0_clock; // @[LazyModule.scala 390:12]
  assign auto_subsystem_sbus_clock_groups_out_2_member_subsystem_l2_0_reset =
    subsystem_sbus_clock_groups_auto_out_3_member_subsystem_l2_0_reset; // @[LazyModule.scala 390:12]
  assign auto_subsystem_sbus_clock_groups_out_1_member_subsystem_fbus_0_clock =
    subsystem_sbus_clock_groups_auto_out_2_member_subsystem_fbus_0_clock; // @[LazyModule.scala 390:12]
  assign auto_subsystem_sbus_clock_groups_out_1_member_subsystem_fbus_0_reset =
    subsystem_sbus_clock_groups_auto_out_2_member_subsystem_fbus_0_reset; // @[LazyModule.scala 390:12]
  assign auto_subsystem_sbus_clock_groups_out_0_member_subsystem_cbus_address_adjuster_1_clock =
    subsystem_sbus_clock_groups_auto_out_1_member_subsystem_cbus_address_adjuster_1_clock; // @[LazyModule.scala 390:12]
  assign auto_subsystem_sbus_clock_groups_out_0_member_subsystem_cbus_address_adjuster_1_reset =
    subsystem_sbus_clock_groups_auto_out_1_member_subsystem_cbus_address_adjuster_1_reset; // @[LazyModule.scala 390:12]
  assign subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_9_clock =
    auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_9_clock; // @[LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_9_reset =
    auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_9_reset; // @[LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_7_clock =
    auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_7_clock; // @[LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_7_reset =
    auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_7_reset; // @[LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_6_clock =
    auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_6_clock; // @[LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_6_reset =
    auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_6_reset; // @[LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_4_clock =
    auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_4_clock; // @[LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_4_reset =
    auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_4_reset; // @[LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_core_1_clock =
    auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_core_1_clock; // @[LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_core_1_reset =
    auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_core_1_reset; // @[LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_core_0_clock =
    auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_core_0_clock; // @[LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_core_0_reset =
    auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_core_0_reset; // @[LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_0_clock =
    auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_0_clock; // @[LazyModule.scala 388:16]
  assign subsystem_sbus_clock_groups_auto_in_member_subsystem_sbus_0_reset =
    auto_subsystem_sbus_clock_groups_in_member_subsystem_sbus_0_reset; // @[LazyModule.scala 388:16]
  assign clockGroup_auto_in_member_subsystem_sbus_core_1_clock =
    subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_core_1_clock; // @[LazyModule.scala 377:16]
  assign clockGroup_auto_in_member_subsystem_sbus_core_1_reset =
    subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_core_1_reset; // @[LazyModule.scala 377:16]
  assign clockGroup_auto_in_member_subsystem_sbus_core_0_clock =
    subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_core_0_clock; // @[LazyModule.scala 377:16]
  assign clockGroup_auto_in_member_subsystem_sbus_core_0_reset =
    subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_core_0_reset; // @[LazyModule.scala 377:16]
  assign clockGroup_auto_in_member_subsystem_sbus_0_clock =
    subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_0_clock; // @[LazyModule.scala 377:16]
  assign clockGroup_auto_in_member_subsystem_sbus_0_reset =
    subsystem_sbus_clock_groups_auto_out_0_member_subsystem_sbus_0_reset; // @[LazyModule.scala 377:16]
  assign fixedClockNode_auto_in_clock = clockGroup_auto_out_0_clock; // @[LazyModule.scala 377:16]
  assign fixedClockNode_auto_in_reset = clockGroup_auto_out_0_reset; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign system_bus_xbar_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_valid = fixer_auto_out_2_a_valid; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_bits_opcode = fixer_auto_out_2_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_bits_param = fixer_auto_out_2_a_bits_param; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_bits_size = fixer_auto_out_2_a_bits_size; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_bits_source = fixer_auto_out_2_a_bits_source; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_bits_address = fixer_auto_out_2_a_bits_address; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_bits_user_amba_prot_bufferable = fixer_auto_out_2_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_bits_user_amba_prot_modifiable = fixer_auto_out_2_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_bits_user_amba_prot_readalloc = fixer_auto_out_2_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_bits_user_amba_prot_writealloc = fixer_auto_out_2_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_bits_user_amba_prot_privileged = fixer_auto_out_2_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_bits_user_amba_prot_secure = fixer_auto_out_2_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_bits_user_amba_prot_fetch = fixer_auto_out_2_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_bits_mask = fixer_auto_out_2_a_bits_mask; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_bits_data = fixer_auto_out_2_a_bits_data; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_a_bits_corrupt = fixer_auto_out_2_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_b_ready = fixer_auto_out_2_b_ready; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_c_valid = fixer_auto_out_2_c_valid; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_c_bits_opcode = fixer_auto_out_2_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_c_bits_param = fixer_auto_out_2_c_bits_param; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_c_bits_size = fixer_auto_out_2_c_bits_size; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_c_bits_source = fixer_auto_out_2_c_bits_source; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_c_bits_address = fixer_auto_out_2_c_bits_address; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_c_bits_data = fixer_auto_out_2_c_bits_data; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_c_bits_corrupt = fixer_auto_out_2_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_d_ready = fixer_auto_out_2_d_ready; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_e_valid = fixer_auto_out_2_e_valid; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_2_e_bits_sink = fixer_auto_out_2_e_bits_sink; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_valid = fixer_auto_out_1_a_valid; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_bits_opcode = fixer_auto_out_1_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_bits_param = fixer_auto_out_1_a_bits_param; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_bits_size = fixer_auto_out_1_a_bits_size; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_bits_source = fixer_auto_out_1_a_bits_source; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_bits_address = fixer_auto_out_1_a_bits_address; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_bits_user_amba_prot_bufferable = fixer_auto_out_1_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_bits_user_amba_prot_modifiable = fixer_auto_out_1_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_bits_user_amba_prot_readalloc = fixer_auto_out_1_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_bits_user_amba_prot_writealloc = fixer_auto_out_1_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_bits_user_amba_prot_privileged = fixer_auto_out_1_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_bits_user_amba_prot_secure = fixer_auto_out_1_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_bits_user_amba_prot_fetch = fixer_auto_out_1_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_bits_mask = fixer_auto_out_1_a_bits_mask; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_bits_data = fixer_auto_out_1_a_bits_data; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_a_bits_corrupt = fixer_auto_out_1_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_b_ready = fixer_auto_out_1_b_ready; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_c_valid = fixer_auto_out_1_c_valid; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_c_bits_opcode = fixer_auto_out_1_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_c_bits_param = fixer_auto_out_1_c_bits_param; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_c_bits_size = fixer_auto_out_1_c_bits_size; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_c_bits_source = fixer_auto_out_1_c_bits_source; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_c_bits_address = fixer_auto_out_1_c_bits_address; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_c_bits_data = fixer_auto_out_1_c_bits_data; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_c_bits_corrupt = fixer_auto_out_1_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_d_ready = fixer_auto_out_1_d_ready; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_e_valid = fixer_auto_out_1_e_valid; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_1_e_bits_sink = fixer_auto_out_1_e_bits_sink; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_0_a_valid = fixer_auto_out_0_a_valid; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_0_a_bits_opcode = fixer_auto_out_0_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_0_a_bits_size = fixer_auto_out_0_a_bits_size; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_0_a_bits_source = fixer_auto_out_0_a_bits_source; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_0_a_bits_address = fixer_auto_out_0_a_bits_address; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_0_a_bits_user_amba_prot_bufferable = fixer_auto_out_0_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_0_a_bits_user_amba_prot_modifiable = fixer_auto_out_0_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_0_a_bits_user_amba_prot_readalloc = fixer_auto_out_0_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_0_a_bits_user_amba_prot_writealloc = fixer_auto_out_0_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_0_a_bits_user_amba_prot_privileged = fixer_auto_out_0_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_0_a_bits_user_amba_prot_secure = fixer_auto_out_0_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_0_a_bits_user_amba_prot_fetch = fixer_auto_out_0_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_0_a_bits_mask = fixer_auto_out_0_a_bits_mask; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_0_a_bits_data = fixer_auto_out_0_a_bits_data; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_in_0_d_ready = fixer_auto_out_0_d_ready; // @[LazyModule.scala 375:16]
  assign system_bus_xbar_auto_out_6_a_ready = coupler_to_port_named_axi4_sys_port_auto_tl_in_a_ready; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_6_d_valid = coupler_to_port_named_axi4_sys_port_auto_tl_in_d_valid; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_6_d_bits_opcode = coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_6_d_bits_size = coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_6_d_bits_source = coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_6_d_bits_denied = coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_denied; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_6_d_bits_data = coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_6_d_bits_corrupt = coupler_to_port_named_axi4_sys_port_auto_tl_in_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_a_ready = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_ready; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_b_valid = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_valid; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_b_bits_opcode = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_opcode; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_b_bits_param = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_param; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_b_bits_size = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_size; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_b_bits_source = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_source; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_b_bits_address = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_address; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_b_bits_mask = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_mask; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_b_bits_data = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_data; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_b_bits_corrupt = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_bits_corrupt; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_c_ready = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_ready; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_d_valid = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_valid; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_d_bits_opcode = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_d_bits_param = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_param; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_d_bits_size = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_size; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_d_bits_source = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_source; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_d_bits_sink = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_sink; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_d_bits_denied = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_denied; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_d_bits_data = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_data; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_d_bits_corrupt = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_5_e_ready = coupler_to_bus_named_subsystem_l2_auto_widget_in_3_e_ready; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_a_ready = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_ready; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_b_valid = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_valid; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_b_bits_opcode = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_opcode; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_b_bits_param = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_param; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_b_bits_size = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_size; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_b_bits_source = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_source; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_b_bits_address = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_address; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_b_bits_mask = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_mask; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_b_bits_data = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_data; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_b_bits_corrupt = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_bits_corrupt; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_c_ready = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_ready; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_d_valid = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_valid; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_d_bits_opcode = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_d_bits_param = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_param; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_d_bits_size = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_size; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_d_bits_source = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_source; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_d_bits_sink = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_sink; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_d_bits_denied = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_denied; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_d_bits_data = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_data; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_d_bits_corrupt = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_4_e_ready = coupler_to_bus_named_subsystem_l2_auto_widget_in_2_e_ready; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_a_ready = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_ready; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_b_valid = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_valid; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_b_bits_opcode = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_opcode; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_b_bits_param = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_param; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_b_bits_size = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_size; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_b_bits_source = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_source; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_b_bits_address = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_address; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_b_bits_mask = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_mask; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_b_bits_data = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_data; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_b_bits_corrupt = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_bits_corrupt; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_c_ready = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_ready; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_d_valid = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_valid; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_d_bits_opcode = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_d_bits_param = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_param; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_d_bits_size = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_size; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_d_bits_source = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_source; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_d_bits_sink = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_sink; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_d_bits_denied = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_denied; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_d_bits_data = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_data; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_d_bits_corrupt = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_3_e_ready = coupler_to_bus_named_subsystem_l2_auto_widget_in_1_e_ready; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_a_ready = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_ready; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_b_valid = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_valid; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_b_bits_opcode = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_opcode; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_b_bits_param = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_param; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_b_bits_size = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_size; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_b_bits_source = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_source; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_b_bits_address = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_address; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_b_bits_mask = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_mask; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_b_bits_data = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_data; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_b_bits_corrupt = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_bits_corrupt; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_c_ready = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_ready; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_d_valid = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_valid; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_d_bits_opcode = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_d_bits_param = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_param; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_d_bits_size = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_size; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_d_bits_source = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_source; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_d_bits_sink = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_sink; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_d_bits_denied = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_denied; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_d_bits_data = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_data; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_d_bits_corrupt = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_2_e_ready = coupler_to_bus_named_subsystem_l2_auto_widget_in_0_e_ready; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_1_a_ready =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_ready; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_1_d_valid =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_valid; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_1_d_bits_opcode =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_1_d_bits_param =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_param; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_1_d_bits_size =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_1_d_bits_source =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_1_d_bits_sink =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_sink; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_1_d_bits_denied =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_denied; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_1_d_bits_data =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_1_d_bits_corrupt =
    coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_0_a_ready = coupler_to_l2_lim_auto_tl_in_a_ready; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_0_d_valid = coupler_to_l2_lim_auto_tl_in_d_valid; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_0_d_bits_opcode = coupler_to_l2_lim_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_0_d_bits_size = coupler_to_l2_lim_auto_tl_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_0_d_bits_source = coupler_to_l2_lim_auto_tl_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_0_d_bits_data = coupler_to_l2_lim_auto_tl_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign system_bus_xbar_auto_out_0_d_bits_corrupt = coupler_to_l2_lim_auto_tl_in_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign fixer_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign fixer_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_valid = coupler_from_tile_1_auto_tl_out_a_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_opcode = coupler_from_tile_1_auto_tl_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_param = coupler_from_tile_1_auto_tl_out_a_bits_param; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_size = coupler_from_tile_1_auto_tl_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_source = coupler_from_tile_1_auto_tl_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_address = coupler_from_tile_1_auto_tl_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_user_amba_prot_bufferable =
    coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_user_amba_prot_modifiable =
    coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_user_amba_prot_readalloc =
    coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_user_amba_prot_writealloc =
    coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_user_amba_prot_privileged =
    coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_user_amba_prot_secure = coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_user_amba_prot_fetch = coupler_from_tile_1_auto_tl_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_mask = coupler_from_tile_1_auto_tl_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_data = coupler_from_tile_1_auto_tl_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_corrupt = coupler_from_tile_1_auto_tl_out_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_b_ready = coupler_from_tile_1_auto_tl_out_b_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_c_valid = coupler_from_tile_1_auto_tl_out_c_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_c_bits_opcode = coupler_from_tile_1_auto_tl_out_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_c_bits_param = coupler_from_tile_1_auto_tl_out_c_bits_param; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_c_bits_size = coupler_from_tile_1_auto_tl_out_c_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_c_bits_source = coupler_from_tile_1_auto_tl_out_c_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_c_bits_address = coupler_from_tile_1_auto_tl_out_c_bits_address; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_c_bits_data = coupler_from_tile_1_auto_tl_out_c_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_c_bits_corrupt = coupler_from_tile_1_auto_tl_out_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_d_ready = coupler_from_tile_1_auto_tl_out_d_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_e_valid = coupler_from_tile_1_auto_tl_out_e_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_e_bits_sink = coupler_from_tile_1_auto_tl_out_e_bits_sink; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_valid = coupler_from_tile_auto_tl_out_a_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_opcode = coupler_from_tile_auto_tl_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_param = coupler_from_tile_auto_tl_out_a_bits_param; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_size = coupler_from_tile_auto_tl_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_source = coupler_from_tile_auto_tl_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_address = coupler_from_tile_auto_tl_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_user_amba_prot_bufferable =
    coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_user_amba_prot_modifiable =
    coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_user_amba_prot_readalloc = coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_readalloc
    ; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_user_amba_prot_writealloc =
    coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_user_amba_prot_privileged =
    coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_user_amba_prot_secure = coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_user_amba_prot_fetch = coupler_from_tile_auto_tl_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_mask = coupler_from_tile_auto_tl_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_data = coupler_from_tile_auto_tl_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_corrupt = coupler_from_tile_auto_tl_out_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_b_ready = coupler_from_tile_auto_tl_out_b_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_c_valid = coupler_from_tile_auto_tl_out_c_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_c_bits_opcode = coupler_from_tile_auto_tl_out_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_c_bits_param = coupler_from_tile_auto_tl_out_c_bits_param; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_c_bits_size = coupler_from_tile_auto_tl_out_c_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_c_bits_source = coupler_from_tile_auto_tl_out_c_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_c_bits_address = coupler_from_tile_auto_tl_out_c_bits_address; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_c_bits_data = coupler_from_tile_auto_tl_out_c_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_c_bits_corrupt = coupler_from_tile_auto_tl_out_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_d_ready = coupler_from_tile_auto_tl_out_d_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_e_valid = coupler_from_tile_auto_tl_out_e_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_e_bits_sink = coupler_from_tile_auto_tl_out_e_bits_sink; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_valid = coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_opcode = coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_size = coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_source = coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_address = coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_user_amba_prot_bufferable =
    coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_user_amba_prot_modifiable =
    coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_user_amba_prot_readalloc =
    coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_user_amba_prot_writealloc =
    coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_user_amba_prot_privileged =
    coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_user_amba_prot_secure =
    coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_user_amba_prot_fetch =
    coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_mask = coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_data = coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_d_ready = coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_a_ready = system_bus_xbar_auto_in_2_a_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_b_valid = system_bus_xbar_auto_in_2_b_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_b_bits_opcode = system_bus_xbar_auto_in_2_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_b_bits_param = system_bus_xbar_auto_in_2_b_bits_param; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_b_bits_size = system_bus_xbar_auto_in_2_b_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_b_bits_source = system_bus_xbar_auto_in_2_b_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_b_bits_address = system_bus_xbar_auto_in_2_b_bits_address; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_b_bits_mask = system_bus_xbar_auto_in_2_b_bits_mask; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_b_bits_data = system_bus_xbar_auto_in_2_b_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_b_bits_corrupt = system_bus_xbar_auto_in_2_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_c_ready = system_bus_xbar_auto_in_2_c_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_valid = system_bus_xbar_auto_in_2_d_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_bits_opcode = system_bus_xbar_auto_in_2_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_bits_param = system_bus_xbar_auto_in_2_d_bits_param; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_bits_size = system_bus_xbar_auto_in_2_d_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_bits_source = system_bus_xbar_auto_in_2_d_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_bits_sink = system_bus_xbar_auto_in_2_d_bits_sink; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_bits_denied = system_bus_xbar_auto_in_2_d_bits_denied; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_bits_data = system_bus_xbar_auto_in_2_d_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_bits_corrupt = system_bus_xbar_auto_in_2_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_e_ready = system_bus_xbar_auto_in_2_e_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_a_ready = system_bus_xbar_auto_in_1_a_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_b_valid = system_bus_xbar_auto_in_1_b_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_b_bits_opcode = system_bus_xbar_auto_in_1_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_b_bits_param = system_bus_xbar_auto_in_1_b_bits_param; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_b_bits_size = system_bus_xbar_auto_in_1_b_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_b_bits_source = system_bus_xbar_auto_in_1_b_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_b_bits_address = system_bus_xbar_auto_in_1_b_bits_address; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_b_bits_mask = system_bus_xbar_auto_in_1_b_bits_mask; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_b_bits_data = system_bus_xbar_auto_in_1_b_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_b_bits_corrupt = system_bus_xbar_auto_in_1_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_c_ready = system_bus_xbar_auto_in_1_c_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_valid = system_bus_xbar_auto_in_1_d_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_bits_opcode = system_bus_xbar_auto_in_1_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_bits_param = system_bus_xbar_auto_in_1_d_bits_param; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_bits_size = system_bus_xbar_auto_in_1_d_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_bits_source = system_bus_xbar_auto_in_1_d_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_bits_sink = system_bus_xbar_auto_in_1_d_bits_sink; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_bits_denied = system_bus_xbar_auto_in_1_d_bits_denied; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_bits_data = system_bus_xbar_auto_in_1_d_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_bits_corrupt = system_bus_xbar_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_e_ready = system_bus_xbar_auto_in_1_e_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_a_ready = system_bus_xbar_auto_in_0_a_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_valid = system_bus_xbar_auto_in_0_d_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_bits_opcode = system_bus_xbar_auto_in_0_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_bits_param = system_bus_xbar_auto_in_0_d_bits_param; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_bits_size = system_bus_xbar_auto_in_0_d_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_bits_source = system_bus_xbar_auto_in_0_d_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_bits_sink = system_bus_xbar_auto_in_0_d_bits_sink; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_bits_denied = system_bus_xbar_auto_in_0_d_bits_denied; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_bits_data = system_bus_xbar_auto_in_0_d_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_bits_corrupt = system_bus_xbar_auto_in_0_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coupler_to_l2_lim_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_l2_lim_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_l2_lim_auto_buffer_out_a_ready = auto_coupler_to_l2_lim_buffer_out_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_l2_lim_auto_buffer_out_d_valid = auto_coupler_to_l2_lim_buffer_out_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_l2_lim_auto_buffer_out_d_bits_opcode = auto_coupler_to_l2_lim_buffer_out_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_l2_lim_auto_buffer_out_d_bits_size = auto_coupler_to_l2_lim_buffer_out_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_l2_lim_auto_buffer_out_d_bits_source = auto_coupler_to_l2_lim_buffer_out_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_l2_lim_auto_buffer_out_d_bits_data = auto_coupler_to_l2_lim_buffer_out_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_l2_lim_auto_buffer_out_d_bits_corrupt = auto_coupler_to_l2_lim_buffer_out_d_bits_corrupt; // @[LazyModule.scala 390:12]
  assign coupler_to_l2_lim_auto_tl_in_a_valid = system_bus_xbar_auto_out_0_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_lim_auto_tl_in_a_bits_opcode = system_bus_xbar_auto_out_0_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_lim_auto_tl_in_a_bits_param = system_bus_xbar_auto_out_0_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_lim_auto_tl_in_a_bits_size = system_bus_xbar_auto_out_0_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_lim_auto_tl_in_a_bits_source = system_bus_xbar_auto_out_0_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_lim_auto_tl_in_a_bits_address = system_bus_xbar_auto_out_0_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_lim_auto_tl_in_a_bits_mask = system_bus_xbar_auto_out_0_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_lim_auto_tl_in_a_bits_data = system_bus_xbar_auto_out_0_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_lim_auto_tl_in_a_bits_corrupt = system_bus_xbar_auto_out_0_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_lim_auto_tl_in_d_ready = system_bus_xbar_auto_out_0_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_valid =
    system_bus_xbar_auto_out_1_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_opcode =
    system_bus_xbar_auto_out_1_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_param =
    system_bus_xbar_auto_out_1_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_size =
    system_bus_xbar_auto_out_1_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_source =
    system_bus_xbar_auto_out_1_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_address =
    system_bus_xbar_auto_out_1_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_bufferable =
    system_bus_xbar_auto_out_1_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_modifiable =
    system_bus_xbar_auto_out_1_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_readalloc =
    system_bus_xbar_auto_out_1_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_writealloc =
    system_bus_xbar_auto_out_1_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_privileged =
    system_bus_xbar_auto_out_1_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_secure =
    system_bus_xbar_auto_out_1_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_user_amba_prot_fetch =
    system_bus_xbar_auto_out_1_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_mask =
    system_bus_xbar_auto_out_1_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_data =
    system_bus_xbar_auto_out_1_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_a_bits_corrupt =
    system_bus_xbar_auto_out_1_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_widget_in_d_ready =
    system_bus_xbar_auto_out_1_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_a_ready =
    auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_valid =
    auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_opcode =
    auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_param =
    auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_param; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_size =
    auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_source =
    auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_sink =
    auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_sink; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_denied =
    auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_denied; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_data =
    auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_cbus_address_adjuster_auto_bus_xing_out_d_bits_corrupt =
    auto_coupler_to_bus_named_subsystem_cbus_address_adjuster_bus_xing_out_d_bits_corrupt; // @[LazyModule.scala 390:12]
  assign coupler_from_bus_named_subsystem_fbus_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_from_bus_named_subsystem_fbus_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_widget_out_a_ready = fixer_auto_in_0_a_ready; // @[LazyModule.scala 375:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_valid = fixer_auto_in_0_d_valid; // @[LazyModule.scala 375:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_opcode = fixer_auto_in_0_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_param = fixer_auto_in_0_d_bits_param; // @[LazyModule.scala 375:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_size = fixer_auto_in_0_d_bits_size; // @[LazyModule.scala 375:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_source = fixer_auto_in_0_d_bits_source; // @[LazyModule.scala 375:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_sink = fixer_auto_in_0_d_bits_sink; // @[LazyModule.scala 375:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_denied = fixer_auto_in_0_d_bits_denied; // @[LazyModule.scala 375:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_data = fixer_auto_in_0_d_bits_data; // @[LazyModule.scala 375:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_widget_out_d_bits_corrupt = fixer_auto_in_0_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_valid =
    auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_valid; // @[LazyModule.scala 388:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_opcode =
    auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_size =
    auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_source =
    auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_address =
    auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_bufferable =
    auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 388:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_modifiable =
    auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 388:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_readalloc =
    auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 388:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_writealloc =
    auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 388:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_privileged =
    auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 388:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_secure =
    auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_user_amba_prot_secure; // @[LazyModule.scala 388:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_user_amba_prot_fetch =
    auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 388:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_mask =
    auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_mask; // @[LazyModule.scala 388:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_a_bits_data =
    auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_a_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_from_bus_named_subsystem_fbus_auto_bus_xing_in_d_ready =
    auto_coupler_from_bus_named_subsystem_fbus_bus_xing_in_d_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_valid = system_bus_xbar_auto_out_5_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_opcode = system_bus_xbar_auto_out_5_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_param = system_bus_xbar_auto_out_5_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_size = system_bus_xbar_auto_out_5_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_source = system_bus_xbar_auto_out_5_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_address = system_bus_xbar_auto_out_5_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_mask = system_bus_xbar_auto_out_5_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_data = system_bus_xbar_auto_out_5_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_a_bits_corrupt = system_bus_xbar_auto_out_5_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_b_ready = system_bus_xbar_auto_out_5_b_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_valid = system_bus_xbar_auto_out_5_c_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_opcode = system_bus_xbar_auto_out_5_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_param = system_bus_xbar_auto_out_5_c_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_size = system_bus_xbar_auto_out_5_c_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_source = system_bus_xbar_auto_out_5_c_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_address = system_bus_xbar_auto_out_5_c_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_data = system_bus_xbar_auto_out_5_c_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_c_bits_corrupt = system_bus_xbar_auto_out_5_c_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_d_ready = system_bus_xbar_auto_out_5_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_e_valid = system_bus_xbar_auto_out_5_e_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_3_e_bits_sink = system_bus_xbar_auto_out_5_e_bits_sink; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_valid = system_bus_xbar_auto_out_4_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_opcode = system_bus_xbar_auto_out_4_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_param = system_bus_xbar_auto_out_4_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_size = system_bus_xbar_auto_out_4_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_source = system_bus_xbar_auto_out_4_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_address = system_bus_xbar_auto_out_4_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_mask = system_bus_xbar_auto_out_4_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_data = system_bus_xbar_auto_out_4_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_a_bits_corrupt = system_bus_xbar_auto_out_4_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_b_ready = system_bus_xbar_auto_out_4_b_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_valid = system_bus_xbar_auto_out_4_c_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_opcode = system_bus_xbar_auto_out_4_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_param = system_bus_xbar_auto_out_4_c_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_size = system_bus_xbar_auto_out_4_c_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_source = system_bus_xbar_auto_out_4_c_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_address = system_bus_xbar_auto_out_4_c_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_data = system_bus_xbar_auto_out_4_c_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_c_bits_corrupt = system_bus_xbar_auto_out_4_c_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_d_ready = system_bus_xbar_auto_out_4_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_e_valid = system_bus_xbar_auto_out_4_e_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_2_e_bits_sink = system_bus_xbar_auto_out_4_e_bits_sink; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_valid = system_bus_xbar_auto_out_3_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_opcode = system_bus_xbar_auto_out_3_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_param = system_bus_xbar_auto_out_3_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_size = system_bus_xbar_auto_out_3_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_source = system_bus_xbar_auto_out_3_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_address = system_bus_xbar_auto_out_3_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_mask = system_bus_xbar_auto_out_3_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_data = system_bus_xbar_auto_out_3_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_a_bits_corrupt = system_bus_xbar_auto_out_3_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_b_ready = system_bus_xbar_auto_out_3_b_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_valid = system_bus_xbar_auto_out_3_c_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_opcode = system_bus_xbar_auto_out_3_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_param = system_bus_xbar_auto_out_3_c_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_size = system_bus_xbar_auto_out_3_c_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_source = system_bus_xbar_auto_out_3_c_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_address = system_bus_xbar_auto_out_3_c_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_data = system_bus_xbar_auto_out_3_c_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_c_bits_corrupt = system_bus_xbar_auto_out_3_c_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_d_ready = system_bus_xbar_auto_out_3_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_e_valid = system_bus_xbar_auto_out_3_e_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_1_e_bits_sink = system_bus_xbar_auto_out_3_e_bits_sink; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_valid = system_bus_xbar_auto_out_2_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_opcode = system_bus_xbar_auto_out_2_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_param = system_bus_xbar_auto_out_2_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_size = system_bus_xbar_auto_out_2_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_source = system_bus_xbar_auto_out_2_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_address = system_bus_xbar_auto_out_2_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_mask = system_bus_xbar_auto_out_2_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_data = system_bus_xbar_auto_out_2_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_a_bits_corrupt = system_bus_xbar_auto_out_2_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_b_ready = system_bus_xbar_auto_out_2_b_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_valid = system_bus_xbar_auto_out_2_c_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_opcode = system_bus_xbar_auto_out_2_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_param = system_bus_xbar_auto_out_2_c_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_size = system_bus_xbar_auto_out_2_c_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_source = system_bus_xbar_auto_out_2_c_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_address = system_bus_xbar_auto_out_2_c_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_data = system_bus_xbar_auto_out_2_c_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_c_bits_corrupt = system_bus_xbar_auto_out_2_c_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_d_ready = system_bus_xbar_auto_out_2_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_e_valid = system_bus_xbar_auto_out_2_e_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_widget_in_0_e_bits_sink = system_bus_xbar_auto_out_2_e_bits_sink; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_a_ready =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_valid =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_opcode =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_param =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_param; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_size =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_source =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_address =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_address; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_mask =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_mask; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_data =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_b_bits_corrupt =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_b_bits_corrupt; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_c_ready =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_c_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_valid =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_opcode =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_param =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_param; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_size =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_source =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_sink =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_sink; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_denied =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_denied; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_data =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_d_bits_corrupt =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_d_bits_corrupt; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_3_e_ready =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_3_e_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_a_ready =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_valid =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_opcode =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_param =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_param; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_size =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_source =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_address =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_address; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_mask =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_mask; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_data =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_b_bits_corrupt =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_b_bits_corrupt; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_c_ready =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_c_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_valid =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_opcode =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_param =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_param; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_size =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_source =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_sink =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_sink; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_denied =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_denied; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_data =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_d_bits_corrupt =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_d_bits_corrupt; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_2_e_ready =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_2_e_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_a_ready =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_valid =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_opcode =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_param =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_param; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_size =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_source =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_address =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_address; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_mask =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_mask; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_data =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_b_bits_corrupt =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_b_bits_corrupt; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_c_ready =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_c_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_valid =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_opcode =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_param =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_param; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_size =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_source =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_sink =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_sink; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_denied =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_denied; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_data =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_d_bits_corrupt =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_d_bits_corrupt; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_1_e_ready =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_1_e_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_a_ready =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_valid =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_opcode =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_param =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_param; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_size =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_source =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_address =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_address; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_mask =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_mask; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_data =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_b_bits_corrupt =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_b_bits_corrupt; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_c_ready =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_c_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_valid =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_opcode =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_param =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_param; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_size =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_source =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_sink =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_sink; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_denied =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_denied; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_data =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_d_bits_corrupt =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_d_bits_corrupt; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_l2_auto_bus_xing_out_0_e_ready =
    auto_coupler_to_bus_named_subsystem_l2_bus_xing_out_0_e_ready; // @[LazyModule.scala 390:12]
  assign coupler_from_tile_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_from_tile_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_opcode =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_opcode; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_param =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_param; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_size =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_size; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_source =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_source; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_address =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_address; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_bufferable =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_user_amba_prot_bufferable; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_modifiable =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_user_amba_prot_modifiable; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_readalloc =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_user_amba_prot_readalloc; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_writealloc =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_user_amba_prot_writealloc; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_privileged =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_user_amba_prot_privileged; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_secure =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_user_amba_prot_secure; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_fetch =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_user_amba_prot_fetch; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_mask =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_mask; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_data =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_data; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits0_corrupt =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits0_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_opcode =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_opcode; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_param =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_param; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_size =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_size; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_source =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_source; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_address =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_address; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_bufferable =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_user_amba_prot_bufferable; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_modifiable =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_user_amba_prot_modifiable; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_readalloc =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_user_amba_prot_readalloc; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_writealloc =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_user_amba_prot_writealloc; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_privileged =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_user_amba_prot_privileged; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_secure =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_user_amba_prot_secure; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_fetch =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_user_amba_prot_fetch; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_mask =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_mask; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_data =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_data; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_bits1_corrupt =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_bits1_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_valid =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_valid; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_a_source =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_a_source; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_b_ready =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_b_ready; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_b_sink = auto_coupler_from_tile_tl_master_clock_xing_in_0_b_sink
    ; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_opcode =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits0_opcode; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_param =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits0_param; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_size =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits0_size; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_source =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits0_source; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_address =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits0_address; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_data =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits0_data; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_bits0_corrupt =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits0_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_opcode =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits1_opcode; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_param =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits1_param; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_size =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits1_size; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_source =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits1_source; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_address =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits1_address; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_data =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits1_data; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_bits1_corrupt =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_bits1_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_valid =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_valid; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_c_source =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_c_source; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_d_ready =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_d_ready; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_d_sink = auto_coupler_from_tile_tl_master_clock_xing_in_0_d_sink
    ; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_e_bits0_sink =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_e_bits0_sink; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_e_bits1_sink =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_e_bits1_sink; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_e_valid =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_e_valid; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_master_clock_xing_in_e_source =
    auto_coupler_from_tile_tl_master_clock_xing_in_0_e_source; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_auto_tl_out_a_ready = fixer_auto_in_1_a_ready; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_b_valid = fixer_auto_in_1_b_valid; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_b_bits_opcode = fixer_auto_in_1_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_b_bits_param = fixer_auto_in_1_b_bits_param; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_b_bits_size = fixer_auto_in_1_b_bits_size; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_b_bits_source = fixer_auto_in_1_b_bits_source; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_b_bits_address = fixer_auto_in_1_b_bits_address; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_b_bits_mask = fixer_auto_in_1_b_bits_mask; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_b_bits_data = fixer_auto_in_1_b_bits_data; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_b_bits_corrupt = fixer_auto_in_1_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_c_ready = fixer_auto_in_1_c_ready; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_d_valid = fixer_auto_in_1_d_valid; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_d_bits_opcode = fixer_auto_in_1_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_d_bits_param = fixer_auto_in_1_d_bits_param; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_d_bits_size = fixer_auto_in_1_d_bits_size; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_d_bits_source = fixer_auto_in_1_d_bits_source; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_d_bits_sink = fixer_auto_in_1_d_bits_sink; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_d_bits_denied = fixer_auto_in_1_d_bits_denied; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_d_bits_data = fixer_auto_in_1_d_bits_data; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_d_bits_corrupt = fixer_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_auto_tl_out_e_ready = fixer_auto_in_1_e_ready; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_from_tile_1_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_opcode =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_opcode; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_param =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_param; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_size =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_size; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_source =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_source; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_address =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_address; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_bufferable =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_user_amba_prot_bufferable; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_modifiable =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_user_amba_prot_modifiable; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_readalloc =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_user_amba_prot_readalloc; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_writealloc =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_user_amba_prot_writealloc; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_privileged =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_user_amba_prot_privileged; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_secure =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_user_amba_prot_secure; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_fetch =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_user_amba_prot_fetch; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_mask =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_mask; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_data =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_data; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits0_corrupt =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits0_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_opcode =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_opcode; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_param =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_param; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_size =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_size; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_source =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_source; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_address =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_address; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_bufferable =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_user_amba_prot_bufferable; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_modifiable =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_user_amba_prot_modifiable; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_readalloc =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_user_amba_prot_readalloc; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_writealloc =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_user_amba_prot_writealloc; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_privileged =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_user_amba_prot_privileged; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_secure =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_user_amba_prot_secure; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_fetch =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_user_amba_prot_fetch; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_mask =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_mask; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_data =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_data; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_bits1_corrupt =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_bits1_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_valid =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_valid; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_a_source =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_a_source; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_b_ready =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_b_ready; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_b_sink =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_b_sink; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_opcode =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits0_opcode; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_param =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits0_param; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_size =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits0_size; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_source =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits0_source; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_address =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits0_address; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_data =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits0_data; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits0_corrupt =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits0_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_opcode =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits1_opcode; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_param =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits1_param; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_size =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits1_size; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_source =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits1_source; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_address =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits1_address; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_data =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits1_data; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_bits1_corrupt =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_bits1_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_valid =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_valid; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_c_source =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_c_source; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_d_ready =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_d_ready; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_d_sink =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_d_sink; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_e_bits0_sink =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_e_bits0_sink; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_e_bits1_sink =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_e_bits1_sink; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_e_valid =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_e_valid; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_master_clock_xing_in_e_source =
    auto_coupler_from_tile_tl_master_clock_xing_in_1_e_source; // @[LazyModule.scala 388:16]
  assign coupler_from_tile_1_auto_tl_out_a_ready = fixer_auto_in_2_a_ready; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_b_valid = fixer_auto_in_2_b_valid; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_b_bits_opcode = fixer_auto_in_2_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_b_bits_param = fixer_auto_in_2_b_bits_param; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_b_bits_size = fixer_auto_in_2_b_bits_size; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_b_bits_source = fixer_auto_in_2_b_bits_source; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_b_bits_address = fixer_auto_in_2_b_bits_address; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_b_bits_mask = fixer_auto_in_2_b_bits_mask; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_b_bits_data = fixer_auto_in_2_b_bits_data; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_b_bits_corrupt = fixer_auto_in_2_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_c_ready = fixer_auto_in_2_c_ready; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_d_valid = fixer_auto_in_2_d_valid; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_d_bits_opcode = fixer_auto_in_2_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_d_bits_param = fixer_auto_in_2_d_bits_param; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_d_bits_size = fixer_auto_in_2_d_bits_size; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_d_bits_source = fixer_auto_in_2_d_bits_source; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_d_bits_sink = fixer_auto_in_2_d_bits_sink; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_d_bits_denied = fixer_auto_in_2_d_bits_denied; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_d_bits_data = fixer_auto_in_2_d_bits_data; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_d_bits_corrupt = fixer_auto_in_2_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coupler_from_tile_1_auto_tl_out_e_ready = fixer_auto_in_2_e_ready; // @[LazyModule.scala 375:16]
  assign coupler_to_port_named_axi4_sys_port_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_port_named_axi4_sys_port_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_aw_ready =
    auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_aw_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_w_ready =
    auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_w_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_b_valid =
    auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_b_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_b_bits_id =
    auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_b_bits_id; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_b_bits_resp =
    auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_b_bits_resp; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_ar_ready =
    auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_ar_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_valid =
    auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_r_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_bits_id =
    auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_r_bits_id; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_bits_data =
    auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_r_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_bits_resp =
    auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_r_bits_resp; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_sys_port_auto_bridge_for_axi4_sys_port_axi4_out_r_bits_last =
    auto_coupler_to_port_named_axi4_sys_port_bridge_for_axi4_sys_port_axi4_out_r_bits_last; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_valid = system_bus_xbar_auto_out_6_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_opcode = system_bus_xbar_auto_out_6_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_param = system_bus_xbar_auto_out_6_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_size = system_bus_xbar_auto_out_6_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_source = system_bus_xbar_auto_out_6_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_address = system_bus_xbar_auto_out_6_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_bufferable =
    system_bus_xbar_auto_out_6_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_modifiable =
    system_bus_xbar_auto_out_6_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_readalloc =
    system_bus_xbar_auto_out_6_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_writealloc =
    system_bus_xbar_auto_out_6_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_privileged =
    system_bus_xbar_auto_out_6_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_secure =
    system_bus_xbar_auto_out_6_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_user_amba_prot_fetch =
    system_bus_xbar_auto_out_6_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_mask = system_bus_xbar_auto_out_6_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_data = system_bus_xbar_auto_out_6_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_a_bits_corrupt = system_bus_xbar_auto_out_6_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_sys_port_auto_tl_in_d_ready = system_bus_xbar_auto_out_6_d_ready; // @[LazyModule.scala 377:16]
endmodule
