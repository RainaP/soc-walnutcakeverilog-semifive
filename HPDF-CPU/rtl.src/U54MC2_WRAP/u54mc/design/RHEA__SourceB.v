//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__SourceB(
  input         rf_reset,
  input         clock,
  input         reset,
  output        io_req_ready,
  input         io_req_valid,
  input  [2:0]  io_req_bits_param,
  input  [17:0] io_req_bits_tag,
  input  [9:0]  io_req_bits_set,
  input  [3:0]  io_req_bits_clients,
  input         io_b_ready,
  output        io_b_valid,
  output [1:0]  io_b_bits_param,
  output [6:0]  io_b_bits_source,
  output [35:0] io_b_bits_address
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
`endif // RANDOMIZE_REG_INIT
  reg [3:0] remain; // @[SourceB.scala 48:25]
  wire  _T_19 = io_req_ready & io_req_valid; // @[Decoupled.scala 40:37]
  wire [3:0] remain_set = _T_19 ? io_req_bits_clients : 4'h0; // @[SourceB.scala 64:26 SourceB.scala 64:39]
  wire [3:0] _remain_T = remain | remain_set; // @[SourceB.scala 51:23]
  wire  busy = |remain; // @[SourceB.scala 53:26]
  wire  b_valid = busy | io_req_valid; // @[SourceB.scala 70:21]
  wire  _T_20 = io_b_ready & b_valid; // @[Decoupled.scala 40:37]
  wire [3:0] todo = busy ? remain : io_req_bits_clients; // @[SourceB.scala 54:19]
  wire [4:0] _next_T = {todo, 1'h0}; // @[package.scala 244:48]
  wire [3:0] _next_T_2 = todo | _next_T[3:0]; // @[package.scala 244:43]
  wire [5:0] _next_T_3 = {_next_T_2, 2'h0}; // @[package.scala 244:48]
  wire [3:0] _next_T_5 = _next_T_2 | _next_T_3[3:0]; // @[package.scala 244:43]
  wire [4:0] _next_T_7 = {_next_T_5, 1'h0}; // @[SourceB.scala 55:31]
  wire [4:0] _next_T_8 = ~_next_T_7; // @[SourceB.scala 55:16]
  wire [4:0] _GEN_5 = {{1'd0}, todo}; // @[SourceB.scala 55:37]
  wire [4:0] next = _next_T_8 & _GEN_5; // @[SourceB.scala 55:37]
  wire [4:0] _GEN_1 = _T_20 ? next : 5'h0; // @[SourceB.scala 71:21 SourceB.scala 71:34]
  wire [3:0] remain_clr = _GEN_1[3:0];
  wire [3:0] _remain_T_1 = ~remain_clr; // @[SourceB.scala 51:39]
  wire  _io_req_ready_T = ~busy; // @[SourceB.scala 63:21]
  reg [17:0] tag_r; // @[Reg.scala 15:16]
  wire [17:0] tag = _io_req_ready_T ? io_req_bits_tag : tag_r; // @[SourceB.scala 74:18]
  reg [9:0] set_r; // @[Reg.scala 15:16]
  wire [9:0] set = _io_req_ready_T ? io_req_bits_set : set_r; // @[SourceB.scala 75:18]
  reg [2:0] param_r; // @[Reg.scala 15:16]
  wire [2:0] param = _io_req_ready_T ? io_req_bits_param : param_r; // @[SourceB.scala 76:20]
  wire [6:0] _b_bits_source_T_4 = next[0] ? 7'h60 : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _b_bits_source_T_5 = next[1] ? 7'h61 : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _b_bits_source_T_6 = next[2] ? 7'h40 : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _b_bits_source_T_7 = next[3] ? 7'h41 : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _b_bits_source_T_8 = _b_bits_source_T_4 | _b_bits_source_T_5; // @[Mux.scala 27:72]
  wire [6:0] _b_bits_source_T_9 = _b_bits_source_T_8 | _b_bits_source_T_6; // @[Mux.scala 27:72]
  wire [33:0] b_bits_address_base = {tag,set,6'h0}; // @[Cat.scala 30:58]
  wire  b_bits_address_lo_lo_lo_lo_lo = b_bits_address_base[0]; // @[Parameters.scala 242:72]
  wire  b_bits_address_lo_lo_lo_lo_hi = b_bits_address_base[1]; // @[Parameters.scala 242:72]
  wire  b_bits_address_lo_lo_lo_hi_lo = b_bits_address_base[2]; // @[Parameters.scala 242:72]
  wire  b_bits_address_lo_lo_lo_hi_hi = b_bits_address_base[3]; // @[Parameters.scala 242:72]
  wire  b_bits_address_lo_lo_hi_lo_lo = b_bits_address_base[4]; // @[Parameters.scala 242:72]
  wire  b_bits_address_lo_lo_hi_lo_hi = b_bits_address_base[5]; // @[Parameters.scala 242:72]
  wire  b_bits_address_lo_lo_hi_hi_hi_hi = b_bits_address_base[6]; // @[Parameters.scala 242:72]
  wire  b_bits_address_lo_hi_lo_lo_lo = b_bits_address_base[7]; // @[Parameters.scala 242:72]
  wire  b_bits_address_lo_hi_lo_lo_hi = b_bits_address_base[8]; // @[Parameters.scala 242:72]
  wire  b_bits_address_lo_hi_lo_hi_lo = b_bits_address_base[9]; // @[Parameters.scala 242:72]
  wire  b_bits_address_lo_hi_lo_hi_hi = b_bits_address_base[10]; // @[Parameters.scala 242:72]
  wire  b_bits_address_lo_hi_hi_lo_lo = b_bits_address_base[11]; // @[Parameters.scala 242:72]
  wire  b_bits_address_lo_hi_hi_lo_hi = b_bits_address_base[12]; // @[Parameters.scala 242:72]
  wire  b_bits_address_lo_hi_hi_hi_lo = b_bits_address_base[13]; // @[Parameters.scala 242:72]
  wire  b_bits_address_lo_hi_hi_hi_hi_lo = b_bits_address_base[14]; // @[Parameters.scala 242:72]
  wire  b_bits_address_lo_hi_hi_hi_hi_hi = b_bits_address_base[15]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_lo_lo_lo_lo = b_bits_address_base[16]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_lo_lo_lo_hi = b_bits_address_base[17]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_lo_lo_hi_lo = b_bits_address_base[18]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_lo_lo_hi_hi = b_bits_address_base[19]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_lo_hi_lo_lo = b_bits_address_base[20]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_lo_hi_lo_hi = b_bits_address_base[21]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_lo_hi_hi_lo = b_bits_address_base[22]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_lo_hi_hi_hi_lo = b_bits_address_base[23]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_lo_hi_hi_hi_hi = b_bits_address_base[24]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_hi_lo_lo_lo = b_bits_address_base[25]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_hi_lo_lo_hi = b_bits_address_base[26]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_hi_lo_hi_lo = b_bits_address_base[27]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_hi_lo_hi_hi = b_bits_address_base[28]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_hi_hi_lo_lo = b_bits_address_base[29]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_hi_hi_lo_hi = b_bits_address_base[30]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_hi_hi_hi_lo = b_bits_address_base[31]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_hi_hi_hi_hi_lo = b_bits_address_base[32]; // @[Parameters.scala 242:72]
  wire  b_bits_address_hi_hi_hi_hi_hi_hi = b_bits_address_base[33]; // @[Parameters.scala 242:72]
  wire [8:0] b_bits_address_lo_lo = {b_bits_address_lo_lo_hi_hi_hi_hi,1'h0,1'h0,b_bits_address_lo_lo_hi_lo_hi,
    b_bits_address_lo_lo_hi_lo_lo,b_bits_address_lo_lo_lo_hi_hi,b_bits_address_lo_lo_lo_hi_lo,
    b_bits_address_lo_lo_lo_lo_hi,b_bits_address_lo_lo_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [17:0] b_bits_address_lo = {b_bits_address_lo_hi_hi_hi_hi_hi,b_bits_address_lo_hi_hi_hi_hi_lo,
    b_bits_address_lo_hi_hi_hi_lo,b_bits_address_lo_hi_hi_lo_hi,b_bits_address_lo_hi_hi_lo_lo,
    b_bits_address_lo_hi_lo_hi_hi,b_bits_address_lo_hi_lo_hi_lo,b_bits_address_lo_hi_lo_lo_hi,
    b_bits_address_lo_hi_lo_lo_lo,b_bits_address_lo_lo}; // @[Cat.scala 30:58]
  wire [8:0] b_bits_address_hi_lo = {b_bits_address_hi_lo_hi_hi_hi_hi,b_bits_address_hi_lo_hi_hi_hi_lo,
    b_bits_address_hi_lo_hi_hi_lo,b_bits_address_hi_lo_hi_lo_hi,b_bits_address_hi_lo_hi_lo_lo,
    b_bits_address_hi_lo_lo_hi_hi,b_bits_address_hi_lo_lo_hi_lo,b_bits_address_hi_lo_lo_lo_hi,
    b_bits_address_hi_lo_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [17:0] b_bits_address_hi = {b_bits_address_hi_hi_hi_hi_hi_hi,b_bits_address_hi_hi_hi_hi_hi_lo,
    b_bits_address_hi_hi_hi_hi_lo,b_bits_address_hi_hi_hi_lo_hi,b_bits_address_hi_hi_hi_lo_lo,
    b_bits_address_hi_hi_lo_hi_hi,b_bits_address_hi_hi_lo_hi_lo,b_bits_address_hi_hi_lo_lo_hi,
    b_bits_address_hi_hi_lo_lo_lo,b_bits_address_hi_lo}; // @[Cat.scala 30:58]
  assign io_req_ready = ~busy; // @[SourceB.scala 63:21]
  assign io_b_valid = busy | io_req_valid; // @[SourceB.scala 70:21]
  assign io_b_bits_param = param[1:0]; // @[SourceB.scala 67:17 SourceB.scala 79:20]
  assign io_b_bits_source = _b_bits_source_T_9 | _b_bits_source_T_7; // @[Mux.scala 27:72]
  assign io_b_bits_address = {b_bits_address_hi,b_bits_address_lo}; // @[Cat.scala 30:58]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      remain <= 4'h0;
    end else begin
      remain <= _remain_T & _remain_T_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tag_r <= 18'h0;
    end else if (_T_19) begin
      tag_r <= io_req_bits_tag;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      set_r <= 10'h0;
    end else if (_T_19) begin
      set_r <= io_req_bits_set;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      param_r <= 3'h0;
    end else if (_T_19) begin
      param_r <= io_req_bits_param;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  remain = _RAND_0[3:0];
  _RAND_1 = {1{`RANDOM}};
  tag_r = _RAND_1[17:0];
  _RAND_2 = {1{`RANDOM}};
  set_r = _RAND_2[9:0];
  _RAND_3 = {1{`RANDOM}};
  param_r = _RAND_3[2:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    remain = 4'h0;
  end
  if (rf_reset) begin
    tag_r = 18'h0;
  end
  if (rf_reset) begin
    set_r = 10'h0;
  end
  if (rf_reset) begin
    param_r = 3'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
