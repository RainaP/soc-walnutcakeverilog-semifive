
/*
 ------------------------------------------------------------------------
--
// ------------------------------------------------------------------------------
// 
// Copyright 2001 - 2020 Synopsys, INC.
// 
// This Synopsys IP and all associated documentation are proprietary to
// Synopsys, Inc. and may only be used pursuant to the terms and conditions of a
// written license agreement with Synopsys, Inc. All other use, reproduction,
// modification, or distribution of the Synopsys IP or the associated
// documentation is strictly prohibited.
// 
// Component Name   : DW_axi_x2p
// Component Version: 2.04a
// Release Type     : GA
// ------------------------------------------------------------------------------

// 
// Release version :  2.04a
// File Version     :        $Revision: #9 $ 
// Revision: $Id: //dwh/DW_ocb/DW_axi_x2p/amba_dev/src/DW_axi_x2p_first_last_strobe.v#9 $ 
*/
//
//

//-----------------------------------------------------------------------------
// Filename    : DW_axi_x2p_first_last_strobe.v
// Created     : Jan 6 2005
// Description : Finds the first and last nonzero strobes
//              
//-----------------------------------------------------------------------------

`include "axi2apb_cpu_cpupbus_DW_axi_x2p_all_includes.vh"

module axi2apb_cpu_cpupbus_DW_axi_x2p_first_last_strobe (/*AUTOARG*/
  // Outputs
  last_strobe, 
                                     next_first_strobe,
                                     // Inputs
                                     clk, 
                                     rstn, 
                                     write_strobes,
                                     sample_strobes
                                     );

   parameter PRIMARY_DATA_WIDTH = `axi2apb_cpu_cpupbus_X2P_AXI_DW;
   parameter SECONDARY_DATA_WIDTH = `axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH;

   input     clk;
   input     rstn;
   
   input [(PRIMARY_DATA_WIDTH/8)-1:0] write_strobes;
   input                              sample_strobes;
   output [7:0]                       last_strobe;
   output [7:0]                       next_first_strobe;
   
   wire [(PRIMARY_DATA_WIDTH/8)-1:0]  write_strobes;
   reg [(PRIMARY_DATA_WIDTH/8)-1:0]   first_wr_strobes;
   
//   reg [7:0]                        first_strobe;
   reg [7:0]                          last_strobe;
   reg [7:0]                          next_first_strobe;
   reg [7:0]                          next_last_strobe;
//   wire [7:0]                       first_strobe_ns;
   wire [7:0]                         last_strobe_ns;

  reg [(SECONDARY_DATA_WIDTH/8)-1:0]  slice;
   reg [(PRIMARY_DATA_WIDTH/SECONDARY_DATA_WIDTH)-1:0] active_slice;

   integer                            i,j,k,m;
  
   parameter [7:0]                    APB_WDS = (PRIMARY_DATA_WIDTH/SECONDARY_DATA_WIDTH);
   parameter                          APB_WDS_INT = (PRIMARY_DATA_WIDTH/SECONDARY_DATA_WIDTH);

   //***********************************************************************
   //
   // first_strobes
   // from bottom up, check the strobes in groups the width of the secondary
   // count will indicate the first non-sero set of strobes
   //
   //***********************************************************************
   
  always@(*)
    begin: SLICES_1_PROC
      first_wr_strobes = write_strobes;
      for(i=0;i<APB_WDS_INT;i=i+1)
        begin
          for(j=0; j<SECONDARY_DATA_WIDTH/8; j=j+1)
            //spyglass disable_block SelfDeterminedExpr-ML
            //SMD: Self determined expression present in the design.
            //SJ: The expression indexing the vector/array will never exceed the boundary of the vector/array.
            slice[j] = first_wr_strobes[j+(SECONDARY_DATA_WIDTH/8)*i];
            //spyglass enable_block SelfDeterminedExpr-ML
          active_slice[i] = |slice;
        end
    end // always@ (...

// spyglass disable_block W415a
// SMD: Signal may be multiply assigned(beside initialization) in the same scope.
// SJ : Here we are updating the next_first_strobe based on active_slice signal inside a for loop. Multiply assignment of this net is intended.
  always@(*)
    begin: NEXT_FIRST_STROBE_1_PROC
      next_first_strobe = APB_WDS;
      for(k=0;k<APB_WDS_INT;k=k+1)
        begin
           //spyglass disable_block SelfDeterminedExpr-ML
           //SMD: Self determined expression present in the design.
           //SJ: The expression used in if condition refers to active_slice[0] and this is intended as per the design.
          if(active_slice[APB_WDS_INT-1-k])
           //spyglass enable_block SelfDeterminedExpr-ML
            begin
              next_first_strobe = APB_WDS-1-k;
            end
        end
    end // always@ (...
// spyglass enable_block W415a
  
   //***********************************************************************
   //
   // last_strobes
   // from top down, check the strobes in groups the width of the secondary
   // count will indicate the first of the trailing  non-zero set of strobes
   //
   //***********************************************************************
   

// spyglass disable_block W415a
// SMD: Signal may be multiply assigned(beside initialization) in the same scope.
// SJ : Here we are updating the next_last_strobe based on active_slice signal inside a for loop. Multiply assignment of this net is intended.
  always@(*)
    begin: NEXT_LAST_STROBE_1_PROC
      next_last_strobe = 8'h00;
      for(m=0;m<APB_WDS_INT;m=m+1)
        begin
          if(active_slice[m])
            begin
  //Unrecommended blocking assignment (converting integer to unsigned)
  //This is not an issue as integer m only takes positive values.
              next_last_strobe = m+1;
            end
        end
    end // always@ (...
// spyglass enable_block W415a
   
   assign last_strobe_ns = (sample_strobes == 1'b1) ? next_last_strobe : last_strobe;

//   assign first_strobe_ns = (sample_strobes == 1'b1) ? next_first_strobe : first_strobe;

   always @(posedge clk or negedge rstn)
     begin: S_STROBE_PROC
     if (!rstn)
       begin
         last_strobe <= 8'h00;
//         first_strobe <= 8'h00;
       end
     else
       begin
         last_strobe <= last_strobe_ns;
//         first_strobe <= first_strobe_ns;
       end
     end // always @ (posedge clk or negedge rstn)
      
endmodule // DW_axi_x2_first_last_strobe















