#include "SemiCPU.h"
#include <stdlib.h>
#include <stdio.h>

extern "C" int main(void){
    #define TEST_SRAM_SIZE 1024
    static unsigned char buffer0[TEST_SRAM_SIZE];
    static unsigned char buffer1[TEST_SRAM_SIZE];
    int errorCount=0;

    for( unsigned int k=0; k<128;k++ ){
        unsigned int address, size; 
        switch( rand()%10 ){
        default:
            address = rand() % TEST_SRAM_SIZE;
            size    = rand() % TEST_SRAM_SIZE;
            if( address + size > TEST_SRAM_SIZE ) size = TEST_SRAM_SIZE-address;        
            break;
        case 0:
            address = (rand() % TEST_SRAM_SIZE);
            size    = 1;
            break;
        case 1:
            address = (rand() % TEST_SRAM_SIZE) & (~1);
            size    = 2;
            break;
        case 2:
            address = (rand() % TEST_SRAM_SIZE) & (~3);
            size    = 4;
            break;
        case 3:
            address = (rand() % TEST_SRAM_SIZE) & (~7);
            size    = 8;
            break;
        }
        for( unsigned int i=0; i<size;i++ ){
            buffer0[i] = rand();
        }
        writeDevice (buffer0, 0x400000000L + address, size);
        readDevice  (buffer1, 0x400000000L + address, size);
        for( unsigned int i=0; i<size;i++ ){
            if( buffer0[i] != buffer1[i] ){
                printf( "*E: buffer0[%d](0x%02x) != buffer1[%d](0x%02x)\n", i,buffer0[i],i,buffer1[i] );
                errorCount++;
            }
        }
    }
    if( 0==errorCount ){
        printf( "*I: PASSED\n" );
    } else {
        printf( "*E: FAILED (%d errors)\n", errorCount );
    }

    return 0;
}

extern "C" void IRQHandler( int interrupt_number ){
    printf( "IRQHandler: %d\n", interrupt_number );
}

