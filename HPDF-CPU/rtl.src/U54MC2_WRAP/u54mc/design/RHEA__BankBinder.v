//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__BankBinder(
  input          rf_reset,
  input          clock,
  input          reset,
  output         auto_in_3_a_ready,
  input          auto_in_3_a_valid,
  input  [2:0]   auto_in_3_a_bits_opcode,
  input  [2:0]   auto_in_3_a_bits_param,
  input  [2:0]   auto_in_3_a_bits_size,
  input  [3:0]   auto_in_3_a_bits_source,
  input  [35:0]  auto_in_3_a_bits_address,
  input          auto_in_3_a_bits_user_amba_prot_bufferable,
  input          auto_in_3_a_bits_user_amba_prot_modifiable,
  input          auto_in_3_a_bits_user_amba_prot_readalloc,
  input          auto_in_3_a_bits_user_amba_prot_writealloc,
  input          auto_in_3_a_bits_user_amba_prot_privileged,
  input          auto_in_3_a_bits_user_amba_prot_secure,
  input  [15:0]  auto_in_3_a_bits_mask,
  input  [127:0] auto_in_3_a_bits_data,
  input          auto_in_3_d_ready,
  output         auto_in_3_d_valid,
  output [2:0]   auto_in_3_d_bits_opcode,
  output [2:0]   auto_in_3_d_bits_size,
  output [3:0]   auto_in_3_d_bits_source,
  output         auto_in_3_d_bits_denied,
  output [127:0] auto_in_3_d_bits_data,
  output         auto_in_3_d_bits_corrupt,
  output         auto_in_2_a_ready,
  input          auto_in_2_a_valid,
  input  [2:0]   auto_in_2_a_bits_opcode,
  input  [2:0]   auto_in_2_a_bits_param,
  input  [2:0]   auto_in_2_a_bits_size,
  input  [3:0]   auto_in_2_a_bits_source,
  input  [35:0]  auto_in_2_a_bits_address,
  input          auto_in_2_a_bits_user_amba_prot_bufferable,
  input          auto_in_2_a_bits_user_amba_prot_modifiable,
  input          auto_in_2_a_bits_user_amba_prot_readalloc,
  input          auto_in_2_a_bits_user_amba_prot_writealloc,
  input          auto_in_2_a_bits_user_amba_prot_privileged,
  input          auto_in_2_a_bits_user_amba_prot_secure,
  input  [15:0]  auto_in_2_a_bits_mask,
  input  [127:0] auto_in_2_a_bits_data,
  input          auto_in_2_d_ready,
  output         auto_in_2_d_valid,
  output [2:0]   auto_in_2_d_bits_opcode,
  output [2:0]   auto_in_2_d_bits_size,
  output [3:0]   auto_in_2_d_bits_source,
  output         auto_in_2_d_bits_denied,
  output [127:0] auto_in_2_d_bits_data,
  output         auto_in_2_d_bits_corrupt,
  output         auto_in_1_a_ready,
  input          auto_in_1_a_valid,
  input  [2:0]   auto_in_1_a_bits_opcode,
  input  [2:0]   auto_in_1_a_bits_param,
  input  [2:0]   auto_in_1_a_bits_size,
  input  [3:0]   auto_in_1_a_bits_source,
  input  [35:0]  auto_in_1_a_bits_address,
  input          auto_in_1_a_bits_user_amba_prot_bufferable,
  input          auto_in_1_a_bits_user_amba_prot_modifiable,
  input          auto_in_1_a_bits_user_amba_prot_readalloc,
  input          auto_in_1_a_bits_user_amba_prot_writealloc,
  input          auto_in_1_a_bits_user_amba_prot_privileged,
  input          auto_in_1_a_bits_user_amba_prot_secure,
  input  [15:0]  auto_in_1_a_bits_mask,
  input  [127:0] auto_in_1_a_bits_data,
  input          auto_in_1_d_ready,
  output         auto_in_1_d_valid,
  output [2:0]   auto_in_1_d_bits_opcode,
  output [2:0]   auto_in_1_d_bits_size,
  output [3:0]   auto_in_1_d_bits_source,
  output         auto_in_1_d_bits_denied,
  output [127:0] auto_in_1_d_bits_data,
  output         auto_in_1_d_bits_corrupt,
  output         auto_in_0_a_ready,
  input          auto_in_0_a_valid,
  input  [2:0]   auto_in_0_a_bits_opcode,
  input  [2:0]   auto_in_0_a_bits_param,
  input  [2:0]   auto_in_0_a_bits_size,
  input  [3:0]   auto_in_0_a_bits_source,
  input  [35:0]  auto_in_0_a_bits_address,
  input          auto_in_0_a_bits_user_amba_prot_bufferable,
  input          auto_in_0_a_bits_user_amba_prot_modifiable,
  input          auto_in_0_a_bits_user_amba_prot_readalloc,
  input          auto_in_0_a_bits_user_amba_prot_writealloc,
  input          auto_in_0_a_bits_user_amba_prot_privileged,
  input          auto_in_0_a_bits_user_amba_prot_secure,
  input  [15:0]  auto_in_0_a_bits_mask,
  input  [127:0] auto_in_0_a_bits_data,
  input          auto_in_0_d_ready,
  output         auto_in_0_d_valid,
  output [2:0]   auto_in_0_d_bits_opcode,
  output [2:0]   auto_in_0_d_bits_size,
  output [3:0]   auto_in_0_d_bits_source,
  output         auto_in_0_d_bits_denied,
  output [127:0] auto_in_0_d_bits_data,
  output         auto_in_0_d_bits_corrupt,
  input          auto_out_3_a_ready,
  output         auto_out_3_a_valid,
  output [2:0]   auto_out_3_a_bits_opcode,
  output [2:0]   auto_out_3_a_bits_param,
  output [2:0]   auto_out_3_a_bits_size,
  output [3:0]   auto_out_3_a_bits_source,
  output [35:0]  auto_out_3_a_bits_address,
  output         auto_out_3_a_bits_user_amba_prot_bufferable,
  output         auto_out_3_a_bits_user_amba_prot_modifiable,
  output         auto_out_3_a_bits_user_amba_prot_readalloc,
  output         auto_out_3_a_bits_user_amba_prot_writealloc,
  output         auto_out_3_a_bits_user_amba_prot_privileged,
  output         auto_out_3_a_bits_user_amba_prot_secure,
  output [15:0]  auto_out_3_a_bits_mask,
  output [127:0] auto_out_3_a_bits_data,
  output         auto_out_3_d_ready,
  input          auto_out_3_d_valid,
  input  [2:0]   auto_out_3_d_bits_opcode,
  input  [2:0]   auto_out_3_d_bits_size,
  input  [3:0]   auto_out_3_d_bits_source,
  input          auto_out_3_d_bits_denied,
  input  [127:0] auto_out_3_d_bits_data,
  input          auto_out_3_d_bits_corrupt,
  input          auto_out_2_a_ready,
  output         auto_out_2_a_valid,
  output [2:0]   auto_out_2_a_bits_opcode,
  output [2:0]   auto_out_2_a_bits_param,
  output [2:0]   auto_out_2_a_bits_size,
  output [3:0]   auto_out_2_a_bits_source,
  output [35:0]  auto_out_2_a_bits_address,
  output         auto_out_2_a_bits_user_amba_prot_bufferable,
  output         auto_out_2_a_bits_user_amba_prot_modifiable,
  output         auto_out_2_a_bits_user_amba_prot_readalloc,
  output         auto_out_2_a_bits_user_amba_prot_writealloc,
  output         auto_out_2_a_bits_user_amba_prot_privileged,
  output         auto_out_2_a_bits_user_amba_prot_secure,
  output [15:0]  auto_out_2_a_bits_mask,
  output [127:0] auto_out_2_a_bits_data,
  output         auto_out_2_d_ready,
  input          auto_out_2_d_valid,
  input  [2:0]   auto_out_2_d_bits_opcode,
  input  [2:0]   auto_out_2_d_bits_size,
  input  [3:0]   auto_out_2_d_bits_source,
  input          auto_out_2_d_bits_denied,
  input  [127:0] auto_out_2_d_bits_data,
  input          auto_out_2_d_bits_corrupt,
  input          auto_out_1_a_ready,
  output         auto_out_1_a_valid,
  output [2:0]   auto_out_1_a_bits_opcode,
  output [2:0]   auto_out_1_a_bits_param,
  output [2:0]   auto_out_1_a_bits_size,
  output [3:0]   auto_out_1_a_bits_source,
  output [35:0]  auto_out_1_a_bits_address,
  output         auto_out_1_a_bits_user_amba_prot_bufferable,
  output         auto_out_1_a_bits_user_amba_prot_modifiable,
  output         auto_out_1_a_bits_user_amba_prot_readalloc,
  output         auto_out_1_a_bits_user_amba_prot_writealloc,
  output         auto_out_1_a_bits_user_amba_prot_privileged,
  output         auto_out_1_a_bits_user_amba_prot_secure,
  output [15:0]  auto_out_1_a_bits_mask,
  output [127:0] auto_out_1_a_bits_data,
  output         auto_out_1_d_ready,
  input          auto_out_1_d_valid,
  input  [2:0]   auto_out_1_d_bits_opcode,
  input  [2:0]   auto_out_1_d_bits_size,
  input  [3:0]   auto_out_1_d_bits_source,
  input          auto_out_1_d_bits_denied,
  input  [127:0] auto_out_1_d_bits_data,
  input          auto_out_1_d_bits_corrupt,
  input          auto_out_0_a_ready,
  output         auto_out_0_a_valid,
  output [2:0]   auto_out_0_a_bits_opcode,
  output [2:0]   auto_out_0_a_bits_param,
  output [2:0]   auto_out_0_a_bits_size,
  output [3:0]   auto_out_0_a_bits_source,
  output [35:0]  auto_out_0_a_bits_address,
  output         auto_out_0_a_bits_user_amba_prot_bufferable,
  output         auto_out_0_a_bits_user_amba_prot_modifiable,
  output         auto_out_0_a_bits_user_amba_prot_readalloc,
  output         auto_out_0_a_bits_user_amba_prot_writealloc,
  output         auto_out_0_a_bits_user_amba_prot_privileged,
  output         auto_out_0_a_bits_user_amba_prot_secure,
  output [15:0]  auto_out_0_a_bits_mask,
  output [127:0] auto_out_0_a_bits_data,
  output         auto_out_0_d_ready,
  input          auto_out_0_d_valid,
  input  [2:0]   auto_out_0_d_bits_opcode,
  input  [2:0]   auto_out_0_d_bits_size,
  input  [3:0]   auto_out_0_d_bits_source,
  input          auto_out_0_d_bits_denied,
  input  [127:0] auto_out_0_d_bits_data,
  input          auto_out_0_d_bits_corrupt
);
  assign auto_in_3_a_ready = auto_out_3_a_ready; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_3_d_valid = auto_out_3_d_valid; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_3_d_bits_opcode = auto_out_3_d_bits_opcode; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_3_d_bits_size = auto_out_3_d_bits_size; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_3_d_bits_source = auto_out_3_d_bits_source; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_3_d_bits_denied = auto_out_3_d_bits_denied; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_3_d_bits_data = auto_out_3_d_bits_data; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_3_d_bits_corrupt = auto_out_3_d_bits_corrupt; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_2_a_ready = auto_out_2_a_ready; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_2_d_valid = auto_out_2_d_valid; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_opcode = auto_out_2_d_bits_opcode; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_size = auto_out_2_d_bits_size; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_source = auto_out_2_d_bits_source; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_denied = auto_out_2_d_bits_denied; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_data = auto_out_2_d_bits_data; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_corrupt = auto_out_2_d_bits_corrupt; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_1_a_ready = auto_out_1_a_ready; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_1_d_valid = auto_out_1_d_valid; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_opcode = auto_out_1_d_bits_opcode; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_size = auto_out_1_d_bits_size; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_source = auto_out_1_d_bits_source; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_denied = auto_out_1_d_bits_denied; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_data = auto_out_1_d_bits_data; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_corrupt = auto_out_1_d_bits_corrupt; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_0_a_ready = auto_out_0_a_ready; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_0_d_valid = auto_out_0_d_valid; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_opcode = auto_out_0_d_bits_opcode; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_size = auto_out_0_d_bits_size; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_source = auto_out_0_d_bits_source; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_denied = auto_out_0_d_bits_denied; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_data = auto_out_0_d_bits_data; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_corrupt = auto_out_0_d_bits_corrupt; // @[Nodes.scala 1360:11 LazyModule.scala 390:12]
  assign auto_out_3_a_valid = auto_in_3_a_valid; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_opcode = auto_in_3_a_bits_opcode; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_param = auto_in_3_a_bits_param; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_size = auto_in_3_a_bits_size; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_source = auto_in_3_a_bits_source; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_address = auto_in_3_a_bits_address; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_user_amba_prot_bufferable = auto_in_3_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_user_amba_prot_modifiable = auto_in_3_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_user_amba_prot_readalloc = auto_in_3_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_user_amba_prot_writealloc = auto_in_3_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_user_amba_prot_privileged = auto_in_3_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_user_amba_prot_secure = auto_in_3_a_bits_user_amba_prot_secure; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_mask = auto_in_3_a_bits_mask; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_data = auto_in_3_a_bits_data; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_3_d_ready = auto_in_3_d_ready; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_2_a_valid = auto_in_2_a_valid; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_opcode = auto_in_2_a_bits_opcode; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_param = auto_in_2_a_bits_param; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_size = auto_in_2_a_bits_size; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_source = auto_in_2_a_bits_source; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_address = auto_in_2_a_bits_address; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_user_amba_prot_bufferable = auto_in_2_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_user_amba_prot_modifiable = auto_in_2_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_user_amba_prot_readalloc = auto_in_2_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_user_amba_prot_writealloc = auto_in_2_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_user_amba_prot_privileged = auto_in_2_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_user_amba_prot_secure = auto_in_2_a_bits_user_amba_prot_secure; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_mask = auto_in_2_a_bits_mask; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_data = auto_in_2_a_bits_data; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_2_d_ready = auto_in_2_d_ready; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_1_a_valid = auto_in_1_a_valid; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_opcode = auto_in_1_a_bits_opcode; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_param = auto_in_1_a_bits_param; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_size = auto_in_1_a_bits_size; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_source = auto_in_1_a_bits_source; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_address = auto_in_1_a_bits_address; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_user_amba_prot_bufferable = auto_in_1_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_user_amba_prot_modifiable = auto_in_1_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_user_amba_prot_readalloc = auto_in_1_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_user_amba_prot_writealloc = auto_in_1_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_user_amba_prot_privileged = auto_in_1_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_user_amba_prot_secure = auto_in_1_a_bits_user_amba_prot_secure; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_mask = auto_in_1_a_bits_mask; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_data = auto_in_1_a_bits_data; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_1_d_ready = auto_in_1_d_ready; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_0_a_valid = auto_in_0_a_valid; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_opcode = auto_in_0_a_bits_opcode; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_param = auto_in_0_a_bits_param; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_size = auto_in_0_a_bits_size; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_source = auto_in_0_a_bits_source; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_address = auto_in_0_a_bits_address; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_user_amba_prot_bufferable = auto_in_0_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_user_amba_prot_modifiable = auto_in_0_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_user_amba_prot_readalloc = auto_in_0_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_user_amba_prot_writealloc = auto_in_0_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_user_amba_prot_privileged = auto_in_0_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_user_amba_prot_secure = auto_in_0_a_bits_user_amba_prot_secure; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_mask = auto_in_0_a_bits_mask; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_data = auto_in_0_a_bits_data; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
  assign auto_out_0_d_ready = auto_in_0_d_ready; // @[Nodes.scala 1360:11 LazyModule.scala 388:16]
endmodule
