//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__cc_banks_0(
  input  [11:0] RW0_addr,
  input         RW0_en,
  input         RW0_clk,
  input         RW0_wmode,
  input  [63:0] RW0_wdata,
  output [63:0] RW0_rdata
);
  wire [11:0] cc_banks_0_ext_RW0_addr;
  wire  cc_banks_0_ext_RW0_en;
  wire  cc_banks_0_ext_RW0_clk;
  wire  cc_banks_0_ext_RW0_wmode;
  wire [63:0] cc_banks_0_ext_RW0_wdata;
  wire [63:0] cc_banks_0_ext_RW0_rdata;
  RHEA__cc_banks_0_ext cc_banks_0_ext (
    .RW0_addr(cc_banks_0_ext_RW0_addr),
    .RW0_en(cc_banks_0_ext_RW0_en),
    .RW0_clk(cc_banks_0_ext_RW0_clk),
    .RW0_wmode(cc_banks_0_ext_RW0_wmode),
    .RW0_wdata(cc_banks_0_ext_RW0_wdata),
    .RW0_rdata(cc_banks_0_ext_RW0_rdata)
  );
  assign cc_banks_0_ext_RW0_clk = RW0_clk;
  assign cc_banks_0_ext_RW0_en = RW0_en;
  assign cc_banks_0_ext_RW0_addr = RW0_addr;
  assign RW0_rdata = cc_banks_0_ext_RW0_rdata;
  assign cc_banks_0_ext_RW0_wmode = RW0_wmode;
  assign cc_banks_0_ext_RW0_wdata = RW0_wdata;
endmodule
