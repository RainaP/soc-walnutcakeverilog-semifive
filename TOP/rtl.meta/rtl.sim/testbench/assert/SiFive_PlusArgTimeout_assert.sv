//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_PlusArgTimeout_assert(
  input         clock,
  input         reset,
  input  [31:0] io_count
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 28:19]
  wire  _T; // @[PlusArg.scala 29:13]
  wire  _T_1; // @[PlusArg.scala 30:22]
  wire  _T_3; // @[PlusArg.scala 30:12]
  wire  _T_4; // @[PlusArg.scala 30:12]
  plusarg_reader #(.FORMAT("max_core_cycles=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 28:19]
    .out(plusarg_reader_out)
  );
  assign _T = plusarg_reader_out > 32'h0; // @[PlusArg.scala 29:13]
  assign _T_1 = io_count < plusarg_reader_out; // @[PlusArg.scala 30:22]
  assign _T_3 = _T_1 | reset; // @[PlusArg.scala 30:12]
  assign _T_4 = ~_T_3; // @[PlusArg.scala 30:12]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T & _T_4) begin
          $fwrite(32'h80000002,"Assertion Failed: Timeout exceeded: Kill the emulation after INT rdtime cycles. Off if 0.\n    at PlusArg.scala:30 assert (io.count < max, s\"Timeout exceeded: $docstring\")\n"); // @[PlusArg.scala 30:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T & _T_4) begin
          $fatal; // @[PlusArg.scala 30:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
