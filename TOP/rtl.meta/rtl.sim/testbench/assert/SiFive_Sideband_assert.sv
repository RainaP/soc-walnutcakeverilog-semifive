//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_Sideband_assert(
  input        clock,
  input        reset,
  input        room,
  input        r2_valid,
  input        r4_valid,
  input        r6_valid,
  input        r8_valid,
  input        _T_25,
  input        queue_io_enq_ready,
  input        queue_io_enq_valid,
  input  [2:0] queue_io_count
);
  wire  _T_16; // @[Sideband.scala 92:35]
  wire  _T_17; // @[Sideband.scala 92:16]
  wire  _T_19; // @[Sideband.scala 92:10]
  wire  _T_20; // @[Sideband.scala 92:10]
  wire  _T_26; // @[Sideband.scala 124:22]
  wire  _T_27; // @[Sideband.scala 124:34]
  wire  _T_28; // @[Sideband.scala 124:46]
  wire  _T_29; // @[Sideband.scala 124:11]
  wire  _T_30; // @[Sideband.scala 123:59]
  wire  _T_32; // @[Sideband.scala 123:10]
  wire  _T_33; // @[Sideband.scala 123:10]
  wire  _T_256; // @[Sideband.scala 206:11]
  wire  _T_257; // @[Sideband.scala 206:20]
  wire  _T_259; // @[Sideband.scala 206:10]
  wire  _T_260; // @[Sideband.scala 206:10]
  assign _T_16 = queue_io_count <= 3'h1; // @[Sideband.scala 92:35]
  assign _T_17 = room == _T_16; // @[Sideband.scala 92:16]
  assign _T_19 = _T_17 | reset; // @[Sideband.scala 92:10]
  assign _T_20 = ~_T_19; // @[Sideband.scala 92:10]
  assign _T_26 = r2_valid | r4_valid; // @[Sideband.scala 124:22]
  assign _T_27 = _T_26 | r6_valid; // @[Sideband.scala 124:34]
  assign _T_28 = _T_27 | r8_valid; // @[Sideband.scala 124:46]
  assign _T_29 = ~_T_28; // @[Sideband.scala 124:11]
  assign _T_30 = _T_25 | _T_29; // @[Sideband.scala 123:59]
  assign _T_32 = _T_30 | reset; // @[Sideband.scala 123:10]
  assign _T_33 = ~_T_32; // @[Sideband.scala 123:10]
  assign _T_256 = ~queue_io_enq_valid; // @[Sideband.scala 206:11]
  assign _T_257 = _T_256 | queue_io_enq_ready; // @[Sideband.scala 206:20]
  assign _T_259 = _T_257 | reset; // @[Sideband.scala 206:10]
  assign _T_260 = ~_T_259; // @[Sideband.scala 206:10]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_20) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Sideband.scala:92 assert (room === queue.io.count <= UInt(1))\n"); // @[Sideband.scala 92:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_20) begin
          $fatal; // @[Sideband.scala 92:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_33) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Sideband.scala:123 assert (!(r1_valid || r3_valid || r5_valid || r7_valid) ||\n"); // @[Sideband.scala 123:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_33) begin
          $fatal; // @[Sideband.scala 123:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_260) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Sideband.scala:206 assert (!d.valid || d.ready)\n"); // @[Sideband.scala 206:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_260) begin
          $fatal; // @[Sideband.scala 206:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
