//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__Queue_41(
  input         rf_reset,
  input         clock,
  input         reset,
  output        io_enq_ready,
  input         io_enq_valid,
  input  [2:0]  io_enq_bits_opcode,
  input  [2:0]  io_enq_bits_param,
  input  [1:0]  io_enq_bits_size,
  input  [10:0] io_enq_bits_source,
  input  [25:0] io_enq_bits_address,
  input  [7:0]  io_enq_bits_mask,
  input  [63:0] io_enq_bits_data,
  input         io_enq_bits_corrupt,
  input         io_deq_ready,
  output        io_deq_valid,
  output [2:0]  io_deq_bits_opcode,
  output [2:0]  io_deq_bits_param,
  output [1:0]  io_deq_bits_size,
  output [10:0] io_deq_bits_source,
  output [25:0] io_deq_bits_address,
  output [7:0]  io_deq_bits_mask,
  output [63:0] io_deq_bits_data,
  output        io_deq_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [63:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
`endif // RANDOMIZE_REG_INIT
  reg [2:0] ram_0_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_0_param; // @[Decoupled.scala 218:16]
  reg [1:0] ram_0_size; // @[Decoupled.scala 218:16]
  reg [10:0] ram_0_source; // @[Decoupled.scala 218:16]
  reg [25:0] ram_0_address; // @[Decoupled.scala 218:16]
  reg [7:0] ram_0_mask; // @[Decoupled.scala 218:16]
  reg [63:0] ram_0_data; // @[Decoupled.scala 218:16]
  reg  ram_0_corrupt; // @[Decoupled.scala 218:16]
  wire  do_enq = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  reg  maybe_full; // @[Decoupled.scala 221:27]
  wire  empty = ~maybe_full; // @[Decoupled.scala 224:28]
  wire  do_deq = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  assign io_enq_ready = ~maybe_full; // @[Decoupled.scala 241:19]
  assign io_deq_valid = ~empty; // @[Decoupled.scala 240:19]
  assign io_deq_bits_opcode = ram_0_opcode; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_param = ram_0_param; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_size = ram_0_size; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_source = ram_0_source; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_address = ram_0_address; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_mask = ram_0_mask; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_data = ram_0_data; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_corrupt = ram_0_corrupt; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_opcode <= 3'h0;
    end else if (do_enq) begin
      ram_0_opcode <= io_enq_bits_opcode;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_param <= 3'h0;
    end else if (do_enq) begin
      ram_0_param <= io_enq_bits_param;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_size <= 2'h0;
    end else if (do_enq) begin
      ram_0_size <= io_enq_bits_size;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_source <= 11'h0;
    end else if (do_enq) begin
      ram_0_source <= io_enq_bits_source;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_address <= 26'h0;
    end else if (do_enq) begin
      ram_0_address <= io_enq_bits_address;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_mask <= 8'h0;
    end else if (do_enq) begin
      ram_0_mask <= io_enq_bits_mask;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_data <= 64'h0;
    end else if (do_enq) begin
      ram_0_data <= io_enq_bits_data;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_corrupt <= 1'h0;
    end else if (do_enq) begin
      ram_0_corrupt <= io_enq_bits_corrupt;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      maybe_full <= 1'h0;
    end else if (do_enq != do_deq) begin
      maybe_full <= do_enq;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ram_0_opcode = _RAND_0[2:0];
  _RAND_1 = {1{`RANDOM}};
  ram_0_param = _RAND_1[2:0];
  _RAND_2 = {1{`RANDOM}};
  ram_0_size = _RAND_2[1:0];
  _RAND_3 = {1{`RANDOM}};
  ram_0_source = _RAND_3[10:0];
  _RAND_4 = {1{`RANDOM}};
  ram_0_address = _RAND_4[25:0];
  _RAND_5 = {1{`RANDOM}};
  ram_0_mask = _RAND_5[7:0];
  _RAND_6 = {2{`RANDOM}};
  ram_0_data = _RAND_6[63:0];
  _RAND_7 = {1{`RANDOM}};
  ram_0_corrupt = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  maybe_full = _RAND_8[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ram_0_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_0_param = 3'h0;
  end
  if (rf_reset) begin
    ram_0_size = 2'h0;
  end
  if (rf_reset) begin
    ram_0_source = 11'h0;
  end
  if (rf_reset) begin
    ram_0_address = 26'h0;
  end
  if (rf_reset) begin
    ram_0_mask = 8'h0;
  end
  if (rf_reset) begin
    ram_0_data = 64'h0;
  end
  if (rf_reset) begin
    ram_0_corrupt = 1'h0;
  end
  if (reset) begin
    maybe_full = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
