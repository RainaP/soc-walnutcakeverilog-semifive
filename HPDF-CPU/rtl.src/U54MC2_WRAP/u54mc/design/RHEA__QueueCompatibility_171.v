//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__QueueCompatibility_171(
  input          rf_reset,
  input          clock,
  input          reset,
  output         io_enq_ready,
  input          io_enq_valid,
  input  [2:0]   io_enq_bits_opcode,
  input  [2:0]   io_enq_bits_param,
  input  [3:0]   io_enq_bits_size,
  input  [3:0]   io_enq_bits_source,
  input  [36:0]  io_enq_bits_address,
  input  [127:0] io_enq_bits_data,
  input          io_deq_ready,
  output         io_deq_valid,
  output [2:0]   io_deq_bits_opcode,
  output [2:0]   io_deq_bits_param,
  output [3:0]   io_deq_bits_size,
  output [3:0]   io_deq_bits_source,
  output [36:0]  io_deq_bits_address,
  output [127:0] io_deq_bits_data,
  output [2:0]   io_count
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [63:0] _RAND_4;
  reg [127:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [63:0] _RAND_10;
  reg [127:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [63:0] _RAND_16;
  reg [127:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [63:0] _RAND_22;
  reg [127:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
`endif // RANDOMIZE_REG_INIT
  reg [2:0] ram_0_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_0_param; // @[Decoupled.scala 218:16]
  reg [3:0] ram_0_size; // @[Decoupled.scala 218:16]
  reg [3:0] ram_0_source; // @[Decoupled.scala 218:16]
  reg [36:0] ram_0_address; // @[Decoupled.scala 218:16]
  reg [127:0] ram_0_data; // @[Decoupled.scala 218:16]
  reg [2:0] ram_1_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_1_param; // @[Decoupled.scala 218:16]
  reg [3:0] ram_1_size; // @[Decoupled.scala 218:16]
  reg [3:0] ram_1_source; // @[Decoupled.scala 218:16]
  reg [36:0] ram_1_address; // @[Decoupled.scala 218:16]
  reg [127:0] ram_1_data; // @[Decoupled.scala 218:16]
  reg [2:0] ram_2_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_2_param; // @[Decoupled.scala 218:16]
  reg [3:0] ram_2_size; // @[Decoupled.scala 218:16]
  reg [3:0] ram_2_source; // @[Decoupled.scala 218:16]
  reg [36:0] ram_2_address; // @[Decoupled.scala 218:16]
  reg [127:0] ram_2_data; // @[Decoupled.scala 218:16]
  reg [2:0] ram_3_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_3_param; // @[Decoupled.scala 218:16]
  reg [3:0] ram_3_size; // @[Decoupled.scala 218:16]
  reg [3:0] ram_3_source; // @[Decoupled.scala 218:16]
  reg [36:0] ram_3_address; // @[Decoupled.scala 218:16]
  reg [127:0] ram_3_data; // @[Decoupled.scala 218:16]
  reg [1:0] value_1; // @[Counter.scala 60:40]
  wire [2:0] _GEN_14 = 2'h1 == value_1 ? ram_1_opcode : ram_0_opcode; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_15 = 2'h1 == value_1 ? ram_1_param : ram_0_param; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [3:0] _GEN_16 = 2'h1 == value_1 ? ram_1_size : ram_0_size; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [3:0] _GEN_17 = 2'h1 == value_1 ? ram_1_source : ram_0_source; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [36:0] _GEN_18 = 2'h1 == value_1 ? ram_1_address : ram_0_address; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [127:0] _GEN_26 = 2'h1 == value_1 ? ram_1_data : ram_0_data; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_28 = 2'h2 == value_1 ? ram_2_opcode : _GEN_14; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_29 = 2'h2 == value_1 ? ram_2_param : _GEN_15; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [3:0] _GEN_30 = 2'h2 == value_1 ? ram_2_size : _GEN_16; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [3:0] _GEN_31 = 2'h2 == value_1 ? ram_2_source : _GEN_17; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [36:0] _GEN_32 = 2'h2 == value_1 ? ram_2_address : _GEN_18; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [127:0] _GEN_40 = 2'h2 == value_1 ? ram_2_data : _GEN_26; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] ramIntf_io_deq_bits_MPORT_data_opcode = 2'h3 == value_1 ? ram_3_opcode : _GEN_28; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] ramIntf_io_deq_bits_MPORT_data_param = 2'h3 == value_1 ? ram_3_param : _GEN_29; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [3:0] ramIntf_io_deq_bits_MPORT_data_size = 2'h3 == value_1 ? ram_3_size : _GEN_30; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [3:0] ramIntf_io_deq_bits_MPORT_data_source = 2'h3 == value_1 ? ram_3_source : _GEN_31; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [36:0] ramIntf_io_deq_bits_MPORT_data_address = 2'h3 == value_1 ? ram_3_address : _GEN_32; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [127:0] ramIntf_io_deq_bits_MPORT_data_data = 2'h3 == value_1 ? ram_3_data : _GEN_40; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  reg [1:0] value; // @[Counter.scala 60:40]
  wire  ptr_match = value == value_1; // @[Decoupled.scala 223:33]
  reg  maybe_full; // @[Decoupled.scala 221:27]
  wire  empty = ptr_match & ~maybe_full; // @[Decoupled.scala 224:25]
  wire  _do_enq_T = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  wire  _GEN_246 = io_deq_ready ? 1'h0 : _do_enq_T; // @[Decoupled.scala 249:27 Decoupled.scala 249:36]
  wire  do_enq = empty ? _GEN_246 : _do_enq_T; // @[Decoupled.scala 246:18]
  wire  full = ptr_match & maybe_full; // @[Decoupled.scala 225:24]
  wire  _do_deq_T = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  wire [1:0] _value_T_1 = value + 2'h1; // @[Counter.scala 76:24]
  wire [1:0] _value_T_3 = value_1 + 2'h1; // @[Counter.scala 76:24]
  wire  do_deq = empty ? 1'h0 : _do_deq_T; // @[Decoupled.scala 246:18 Decoupled.scala 248:14]
  wire [1:0] ptr_diff = value - value_1; // @[Decoupled.scala 257:32]
  wire [2:0] _io_count_T_1 = maybe_full & ptr_match ? 3'h4 : 3'h0; // @[Decoupled.scala 259:20]
  wire [2:0] _GEN_225 = {{1'd0}, ptr_diff}; // @[Decoupled.scala 259:62]
  assign io_enq_ready = ~full; // @[Decoupled.scala 241:19]
  assign io_deq_valid = io_enq_valid | ~empty; // @[Decoupled.scala 245:25 Decoupled.scala 245:40 Decoupled.scala 240:16]
  assign io_deq_bits_opcode = empty ? io_enq_bits_opcode : ramIntf_io_deq_bits_MPORT_data_opcode; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_param = empty ? io_enq_bits_param : ramIntf_io_deq_bits_MPORT_data_param; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_size = empty ? io_enq_bits_size : ramIntf_io_deq_bits_MPORT_data_size; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_source = empty ? io_enq_bits_source : ramIntf_io_deq_bits_MPORT_data_source; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_address = empty ? io_enq_bits_address : ramIntf_io_deq_bits_MPORT_data_address; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_data = empty ? io_enq_bits_data : ramIntf_io_deq_bits_MPORT_data_data; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_count = _io_count_T_1 | _GEN_225; // @[Decoupled.scala 259:62]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_opcode <= 3'h0;
    end else if (do_enq) begin
      if (2'h0 == value) begin
        ram_0_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_param <= 3'h0;
    end else if (do_enq) begin
      if (2'h0 == value) begin
        ram_0_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_size <= 4'h0;
    end else if (do_enq) begin
      if (2'h0 == value) begin
        ram_0_size <= io_enq_bits_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_source <= 4'h0;
    end else if (do_enq) begin
      if (2'h0 == value) begin
        ram_0_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_address <= 37'h0;
    end else if (do_enq) begin
      if (2'h0 == value) begin
        ram_0_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_data <= 128'h0;
    end else if (do_enq) begin
      if (2'h0 == value) begin
        ram_0_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_opcode <= 3'h0;
    end else if (do_enq) begin
      if (2'h1 == value) begin
        ram_1_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_param <= 3'h0;
    end else if (do_enq) begin
      if (2'h1 == value) begin
        ram_1_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_size <= 4'h0;
    end else if (do_enq) begin
      if (2'h1 == value) begin
        ram_1_size <= io_enq_bits_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_source <= 4'h0;
    end else if (do_enq) begin
      if (2'h1 == value) begin
        ram_1_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_address <= 37'h0;
    end else if (do_enq) begin
      if (2'h1 == value) begin
        ram_1_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_data <= 128'h0;
    end else if (do_enq) begin
      if (2'h1 == value) begin
        ram_1_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_opcode <= 3'h0;
    end else if (do_enq) begin
      if (2'h2 == value) begin
        ram_2_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_param <= 3'h0;
    end else if (do_enq) begin
      if (2'h2 == value) begin
        ram_2_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_size <= 4'h0;
    end else if (do_enq) begin
      if (2'h2 == value) begin
        ram_2_size <= io_enq_bits_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_source <= 4'h0;
    end else if (do_enq) begin
      if (2'h2 == value) begin
        ram_2_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_address <= 37'h0;
    end else if (do_enq) begin
      if (2'h2 == value) begin
        ram_2_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_data <= 128'h0;
    end else if (do_enq) begin
      if (2'h2 == value) begin
        ram_2_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_opcode <= 3'h0;
    end else if (do_enq) begin
      if (2'h3 == value) begin
        ram_3_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_param <= 3'h0;
    end else if (do_enq) begin
      if (2'h3 == value) begin
        ram_3_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_size <= 4'h0;
    end else if (do_enq) begin
      if (2'h3 == value) begin
        ram_3_size <= io_enq_bits_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_source <= 4'h0;
    end else if (do_enq) begin
      if (2'h3 == value) begin
        ram_3_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_address <= 37'h0;
    end else if (do_enq) begin
      if (2'h3 == value) begin
        ram_3_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_data <= 128'h0;
    end else if (do_enq) begin
      if (2'h3 == value) begin
        ram_3_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      value_1 <= 2'h0;
    end else if (reset) begin
      value_1 <= 2'h0;
    end else if (do_deq) begin
      value_1 <= _value_T_3;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      value <= 2'h0;
    end else if (reset) begin
      value <= 2'h0;
    end else if (do_enq) begin
      value <= _value_T_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      maybe_full <= 1'h0;
    end else if (reset) begin
      maybe_full <= 1'h0;
    end else if (do_enq != do_deq) begin
      if (empty) begin
        if (io_deq_ready) begin
          maybe_full <= 1'h0;
        end else begin
          maybe_full <= _do_enq_T;
        end
      end else begin
        maybe_full <= _do_enq_T;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ram_0_opcode = _RAND_0[2:0];
  _RAND_1 = {1{`RANDOM}};
  ram_0_param = _RAND_1[2:0];
  _RAND_2 = {1{`RANDOM}};
  ram_0_size = _RAND_2[3:0];
  _RAND_3 = {1{`RANDOM}};
  ram_0_source = _RAND_3[3:0];
  _RAND_4 = {2{`RANDOM}};
  ram_0_address = _RAND_4[36:0];
  _RAND_5 = {4{`RANDOM}};
  ram_0_data = _RAND_5[127:0];
  _RAND_6 = {1{`RANDOM}};
  ram_1_opcode = _RAND_6[2:0];
  _RAND_7 = {1{`RANDOM}};
  ram_1_param = _RAND_7[2:0];
  _RAND_8 = {1{`RANDOM}};
  ram_1_size = _RAND_8[3:0];
  _RAND_9 = {1{`RANDOM}};
  ram_1_source = _RAND_9[3:0];
  _RAND_10 = {2{`RANDOM}};
  ram_1_address = _RAND_10[36:0];
  _RAND_11 = {4{`RANDOM}};
  ram_1_data = _RAND_11[127:0];
  _RAND_12 = {1{`RANDOM}};
  ram_2_opcode = _RAND_12[2:0];
  _RAND_13 = {1{`RANDOM}};
  ram_2_param = _RAND_13[2:0];
  _RAND_14 = {1{`RANDOM}};
  ram_2_size = _RAND_14[3:0];
  _RAND_15 = {1{`RANDOM}};
  ram_2_source = _RAND_15[3:0];
  _RAND_16 = {2{`RANDOM}};
  ram_2_address = _RAND_16[36:0];
  _RAND_17 = {4{`RANDOM}};
  ram_2_data = _RAND_17[127:0];
  _RAND_18 = {1{`RANDOM}};
  ram_3_opcode = _RAND_18[2:0];
  _RAND_19 = {1{`RANDOM}};
  ram_3_param = _RAND_19[2:0];
  _RAND_20 = {1{`RANDOM}};
  ram_3_size = _RAND_20[3:0];
  _RAND_21 = {1{`RANDOM}};
  ram_3_source = _RAND_21[3:0];
  _RAND_22 = {2{`RANDOM}};
  ram_3_address = _RAND_22[36:0];
  _RAND_23 = {4{`RANDOM}};
  ram_3_data = _RAND_23[127:0];
  _RAND_24 = {1{`RANDOM}};
  value_1 = _RAND_24[1:0];
  _RAND_25 = {1{`RANDOM}};
  value = _RAND_25[1:0];
  _RAND_26 = {1{`RANDOM}};
  maybe_full = _RAND_26[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ram_0_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_0_param = 3'h0;
  end
  if (rf_reset) begin
    ram_0_size = 4'h0;
  end
  if (rf_reset) begin
    ram_0_source = 4'h0;
  end
  if (rf_reset) begin
    ram_0_address = 37'h0;
  end
  if (rf_reset) begin
    ram_0_data = 128'h0;
  end
  if (rf_reset) begin
    ram_1_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_1_param = 3'h0;
  end
  if (rf_reset) begin
    ram_1_size = 4'h0;
  end
  if (rf_reset) begin
    ram_1_source = 4'h0;
  end
  if (rf_reset) begin
    ram_1_address = 37'h0;
  end
  if (rf_reset) begin
    ram_1_data = 128'h0;
  end
  if (rf_reset) begin
    ram_2_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_2_param = 3'h0;
  end
  if (rf_reset) begin
    ram_2_size = 4'h0;
  end
  if (rf_reset) begin
    ram_2_source = 4'h0;
  end
  if (rf_reset) begin
    ram_2_address = 37'h0;
  end
  if (rf_reset) begin
    ram_2_data = 128'h0;
  end
  if (rf_reset) begin
    ram_3_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_3_param = 3'h0;
  end
  if (rf_reset) begin
    ram_3_size = 4'h0;
  end
  if (rf_reset) begin
    ram_3_source = 4'h0;
  end
  if (rf_reset) begin
    ram_3_address = 37'h0;
  end
  if (rf_reset) begin
    ram_3_data = 128'h0;
  end
  if (rf_reset) begin
    value_1 = 2'h0;
  end
  if (rf_reset) begin
    value = 2'h0;
  end
  if (rf_reset) begin
    maybe_full = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
