//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_IBuf_assert(
  input   clock,
  input   reset,
  input   io_imem_valid,
  input   io_imem_bits_btb_taken,
  input   io_imem_bits_btb_bridx,
  input   pcWordBits
);
  wire  _T_72; // @[IBuf.scala 79:10]
  wire  _T_73; // @[IBuf.scala 79:28]
  wire  _T_74; // @[IBuf.scala 79:25]
  wire  _T_75; // @[IBuf.scala 79:78]
  wire  _T_76; // @[IBuf.scala 79:52]
  wire  _T_78; // @[IBuf.scala 79:9]
  wire  _T_79; // @[IBuf.scala 79:9]
  assign _T_72 = ~io_imem_valid; // @[IBuf.scala 79:10]
  assign _T_73 = ~io_imem_bits_btb_taken; // @[IBuf.scala 79:28]
  assign _T_74 = _T_72 | _T_73; // @[IBuf.scala 79:25]
  assign _T_75 = io_imem_bits_btb_bridx >= pcWordBits; // @[IBuf.scala 79:78]
  assign _T_76 = _T_74 | _T_75; // @[IBuf.scala 79:52]
  assign _T_78 = _T_76 | reset; // @[IBuf.scala 79:9]
  assign _T_79 = ~_T_78; // @[IBuf.scala 79:9]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_79) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at IBuf.scala:79 assert(!io.imem.valid || !io.imem.bits.btb.taken || io.imem.bits.btb.bridx >= pcWordBits)\n"); // @[IBuf.scala 79:9]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_79) begin
          $fatal; // @[IBuf.scala 79:9]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
