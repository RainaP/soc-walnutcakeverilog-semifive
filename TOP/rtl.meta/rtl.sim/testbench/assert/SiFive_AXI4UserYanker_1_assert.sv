//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_AXI4UserYanker_1_assert(
  input        clock,
  input        reset,
  input        auto_out_b_valid,
  input  [1:0] auto_out_b_bits_id,
  input        auto_out_r_valid,
  input  [1:0] auto_out_r_bits_id,
  input        QueueCompatibility_3_io_deq_valid,
  input        QueueCompatibility_7_io_deq_valid,
  input        QueueCompatibility_2_io_deq_valid,
  input        QueueCompatibility_6_io_deq_valid,
  input        QueueCompatibility_1_io_deq_valid,
  input        QueueCompatibility_io_deq_valid,
  input        QueueCompatibility_5_io_deq_valid,
  input        QueueCompatibility_4_io_deq_valid
);
  wire  _T_7; // @[UserYanker.scala 55:15]
  wire  _T_5_0; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _T_5_1; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_5; // @[UserYanker.scala 55:28]
  wire  _T_5_2; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_6; // @[UserYanker.scala 55:28]
  wire  _T_5_3; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_7; // @[UserYanker.scala 55:28]
  wire  _T_8; // @[UserYanker.scala 55:28]
  wire  _T_10; // @[UserYanker.scala 55:14]
  wire  _T_11; // @[UserYanker.scala 55:14]
  wire  _T_51; // @[UserYanker.scala 76:15]
  wire  _T_49_0; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _T_49_1; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_21; // @[UserYanker.scala 76:28]
  wire  _T_49_2; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_22; // @[UserYanker.scala 76:28]
  wire  _T_49_3; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_23; // @[UserYanker.scala 76:28]
  wire  _T_52; // @[UserYanker.scala 76:28]
  wire  _T_54; // @[UserYanker.scala 76:14]
  wire  _T_55; // @[UserYanker.scala 76:14]
  assign _T_7 = ~auto_out_r_valid; // @[UserYanker.scala 55:15]
  assign _T_5_0 = QueueCompatibility_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _T_5_1 = QueueCompatibility_1_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_5 = 2'h1 == auto_out_r_bits_id ? _T_5_1 : _T_5_0; // @[UserYanker.scala 55:28]
  assign _T_5_2 = QueueCompatibility_2_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_6 = 2'h2 == auto_out_r_bits_id ? _T_5_2 : _GEN_5; // @[UserYanker.scala 55:28]
  assign _T_5_3 = QueueCompatibility_3_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_7 = 2'h3 == auto_out_r_bits_id ? _T_5_3 : _GEN_6; // @[UserYanker.scala 55:28]
  assign _T_8 = _T_7 | _GEN_7; // @[UserYanker.scala 55:28]
  assign _T_10 = _T_8 | reset; // @[UserYanker.scala 55:14]
  assign _T_11 = ~_T_10; // @[UserYanker.scala 55:14]
  assign _T_51 = ~auto_out_b_valid; // @[UserYanker.scala 76:15]
  assign _T_49_0 = QueueCompatibility_4_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _T_49_1 = QueueCompatibility_5_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_21 = 2'h1 == auto_out_b_bits_id ? _T_49_1 : _T_49_0; // @[UserYanker.scala 76:28]
  assign _T_49_2 = QueueCompatibility_6_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_22 = 2'h2 == auto_out_b_bits_id ? _T_49_2 : _GEN_21; // @[UserYanker.scala 76:28]
  assign _T_49_3 = QueueCompatibility_7_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_23 = 2'h3 == auto_out_b_bits_id ? _T_49_3 : _GEN_22; // @[UserYanker.scala 76:28]
  assign _T_52 = _T_51 | _GEN_23; // @[UserYanker.scala 76:28]
  assign _T_54 = _T_52 | reset; // @[UserYanker.scala 76:14]
  assign _T_55 = ~_T_54; // @[UserYanker.scala 76:14]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_11) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at UserYanker.scala:55 assert (!out.r.valid || r_valid) // Q must be ready faster than the response\n"); // @[UserYanker.scala 55:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_11) begin
          $fatal; // @[UserYanker.scala 55:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_55) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at UserYanker.scala:76 assert (!out.b.valid || b_valid) // Q must be ready faster than the response\n"); // @[UserYanker.scala 76:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_55) begin
          $fatal; // @[UserYanker.scala 76:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
