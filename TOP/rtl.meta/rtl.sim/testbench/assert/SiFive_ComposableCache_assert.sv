//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_ComposableCache_assert(
  input         clock,
  input         reset,
  input         auto_ctl_in_a_valid,
  input  [2:0]  auto_ctl_in_a_bits_opcode,
  input  [2:0]  auto_ctl_in_a_bits_param,
  input  [1:0]  auto_ctl_in_a_bits_size,
  input  [10:0] auto_ctl_in_a_bits_source,
  input  [25:0] auto_ctl_in_a_bits_address,
  input  [7:0]  auto_ctl_in_a_bits_mask,
  input         auto_ctl_in_a_bits_corrupt,
  input         auto_ctl_in_d_ready,
  input         auto_side_in_a_valid,
  input  [2:0]  auto_side_in_a_bits_opcode,
  input  [2:0]  auto_side_in_a_bits_param,
  input  [1:0]  auto_side_in_a_bits_size,
  input  [10:0] auto_side_in_a_bits_source,
  input  [27:0] auto_side_in_a_bits_address,
  input  [7:0]  auto_side_in_a_bits_mask,
  input         auto_side_in_a_bits_corrupt,
  input         auto_side_in_d_ready,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [2:0]  auto_in_a_bits_param,
  input  [2:0]  auto_in_a_bits_size,
  input  [6:0]  auto_in_a_bits_source,
  input  [35:0] auto_in_a_bits_address,
  input  [7:0]  auto_in_a_bits_mask,
  input         auto_in_a_bits_corrupt,
  input         auto_in_b_ready,
  input         auto_in_c_valid,
  input  [2:0]  auto_in_c_bits_opcode,
  input  [2:0]  auto_in_c_bits_param,
  input  [2:0]  auto_in_c_bits_size,
  input  [6:0]  auto_in_c_bits_source,
  input  [35:0] auto_in_c_bits_address,
  input         auto_in_c_bits_corrupt,
  input         auto_in_d_ready,
  input         auto_in_e_valid,
  input  [3:0]  auto_in_e_bits_sink,
  input         _T_8660,
  input         _T_555_ready,
  input         _T_8582,
  input         _T_8583,
  input         _T_554_bits_read,
  input         mods_0_io_in_a_ready,
  input         mods_0_io_in_b_valid,
  input  [1:0]  mods_0_io_in_b_bits_param,
  input  [6:0]  mods_0_io_in_b_bits_source,
  input  [35:0] mods_0_io_in_b_bits_address,
  input         mods_0_io_in_c_ready,
  input         mods_0_io_in_d_valid,
  input  [2:0]  mods_0_io_in_d_bits_opcode,
  input  [1:0]  mods_0_io_in_d_bits_param,
  input  [2:0]  mods_0_io_in_d_bits_size,
  input  [6:0]  mods_0_io_in_d_bits_source,
  input  [3:0]  mods_0_io_in_d_bits_sink,
  input         mods_0_io_in_d_bits_denied,
  input         mods_0_io_in_d_bits_corrupt,
  input         Sideband_io_a_ready,
  input         Sideband_io_d_valid,
  input  [2:0]  Sideband_io_d_bits_opcode,
  input  [1:0]  Sideband_io_d_bits_param,
  input  [1:0]  Sideband_io_d_bits_size,
  input  [10:0] Sideband_io_d_bits_source,
  input         Sideband_io_d_bits_sink,
  input         Sideband_io_d_bits_denied,
  input         Sideband_io_d_bits_corrupt,
  input         Queue_io_deq_valid,
  input  [1:0]  Queue_io_deq_bits_extra_tlrr_extra_size,
  input  [10:0] Queue_io_deq_bits_extra_tlrr_extra_source,
  input         mods_0_io_resp_valid
);
  wire  TLMonitor_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [6:0] TLMonitor_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_b_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_b_valid; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_b_bits_param; // @[Nodes.scala 25:25]
  wire [6:0] TLMonitor_io_in_b_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_io_in_b_bits_address; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_c_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_c_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_c_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_c_bits_param; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_c_bits_size; // @[Nodes.scala 25:25]
  wire [6:0] TLMonitor_io_in_c_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_io_in_c_bits_address; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_c_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_d_bits_param; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [6:0] TLMonitor_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_d_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_e_valid; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_e_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_1_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [10:0] TLMonitor_1_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [27:0] TLMonitor_1_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_1_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_1_io_in_d_bits_param; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_1_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [10:0] TLMonitor_1_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_2_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_2_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_2_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [10:0] TLMonitor_2_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [25:0] TLMonitor_2_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_2_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_2_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_2_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [10:0] TLMonitor_2_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire  _T_8662; // @[ComposableCache.scala 411:15]
  wire  _T_8663; // @[ComposableCache.scala 411:40]
  wire  _T_8665; // @[ComposableCache.scala 411:14]
  wire  _T_8666; // @[ComposableCache.scala 411:14]
  SiFive_TLMonitor_85_assert TLMonitor ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_clock),
    .reset(TLMonitor_reset),
    .io_in_a_ready(TLMonitor_io_in_a_ready),
    .io_in_a_valid(TLMonitor_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_io_in_a_bits_corrupt),
    .io_in_b_ready(TLMonitor_io_in_b_ready),
    .io_in_b_valid(TLMonitor_io_in_b_valid),
    .io_in_b_bits_param(TLMonitor_io_in_b_bits_param),
    .io_in_b_bits_source(TLMonitor_io_in_b_bits_source),
    .io_in_b_bits_address(TLMonitor_io_in_b_bits_address),
    .io_in_c_ready(TLMonitor_io_in_c_ready),
    .io_in_c_valid(TLMonitor_io_in_c_valid),
    .io_in_c_bits_opcode(TLMonitor_io_in_c_bits_opcode),
    .io_in_c_bits_param(TLMonitor_io_in_c_bits_param),
    .io_in_c_bits_size(TLMonitor_io_in_c_bits_size),
    .io_in_c_bits_source(TLMonitor_io_in_c_bits_source),
    .io_in_c_bits_address(TLMonitor_io_in_c_bits_address),
    .io_in_c_bits_corrupt(TLMonitor_io_in_c_bits_corrupt),
    .io_in_d_ready(TLMonitor_io_in_d_ready),
    .io_in_d_valid(TLMonitor_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_io_in_d_bits_opcode),
    .io_in_d_bits_param(TLMonitor_io_in_d_bits_param),
    .io_in_d_bits_size(TLMonitor_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_io_in_d_bits_source),
    .io_in_d_bits_sink(TLMonitor_io_in_d_bits_sink),
    .io_in_d_bits_denied(TLMonitor_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_io_in_d_bits_corrupt),
    .io_in_e_valid(TLMonitor_io_in_e_valid),
    .io_in_e_bits_sink(TLMonitor_io_in_e_bits_sink)
  );
  SiFive_TLMonitor_86_assert TLMonitor_1 ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_1_clock),
    .reset(TLMonitor_1_reset),
    .io_in_a_ready(TLMonitor_1_io_in_a_ready),
    .io_in_a_valid(TLMonitor_1_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_1_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_1_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_1_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_1_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_1_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_1_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_1_io_in_a_bits_corrupt),
    .io_in_d_ready(TLMonitor_1_io_in_d_ready),
    .io_in_d_valid(TLMonitor_1_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_1_io_in_d_bits_opcode),
    .io_in_d_bits_param(TLMonitor_1_io_in_d_bits_param),
    .io_in_d_bits_size(TLMonitor_1_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_1_io_in_d_bits_source),
    .io_in_d_bits_sink(TLMonitor_1_io_in_d_bits_sink),
    .io_in_d_bits_denied(TLMonitor_1_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_1_io_in_d_bits_corrupt)
  );
  SiFive_TLMonitor_87_assert TLMonitor_2 ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_2_clock),
    .reset(TLMonitor_2_reset),
    .io_in_a_ready(TLMonitor_2_io_in_a_ready),
    .io_in_a_valid(TLMonitor_2_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_2_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_2_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_2_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_2_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_2_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_2_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_2_io_in_a_bits_corrupt),
    .io_in_d_ready(TLMonitor_2_io_in_d_ready),
    .io_in_d_valid(TLMonitor_2_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_2_io_in_d_bits_opcode),
    .io_in_d_bits_size(TLMonitor_2_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_2_io_in_d_bits_source)
  );
  assign _T_8662 = ~mods_0_io_resp_valid; // @[ComposableCache.scala 411:15]
  assign _T_8663 = _T_8662 | _T_8660; // @[ComposableCache.scala 411:40]
  assign _T_8665 = _T_8663 | reset; // @[ComposableCache.scala 411:14]
  assign _T_8666 = ~_T_8665; // @[ComposableCache.scala 411:14]
  assign TLMonitor_clock = clock;
  assign TLMonitor_reset = reset;
  assign TLMonitor_io_in_a_ready = mods_0_io_in_a_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_valid = auto_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_size = auto_in_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_source = auto_in_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_b_ready = auto_in_b_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_b_valid = mods_0_io_in_b_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_b_bits_param = mods_0_io_in_b_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_b_bits_source = mods_0_io_in_b_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_b_bits_address = mods_0_io_in_b_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_ready = mods_0_io_in_c_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_valid = auto_in_c_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_opcode = auto_in_c_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_param = auto_in_c_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_size = auto_in_c_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_source = auto_in_c_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_address = auto_in_c_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_corrupt = auto_in_c_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_ready = auto_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_valid = mods_0_io_in_d_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_opcode = mods_0_io_in_d_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_param = mods_0_io_in_d_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_size = mods_0_io_in_d_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_source = mods_0_io_in_d_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_sink = mods_0_io_in_d_bits_sink; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_denied = mods_0_io_in_d_bits_denied; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_corrupt = mods_0_io_in_d_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_e_valid = auto_in_e_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_e_bits_sink = auto_in_e_bits_sink; // @[Nodes.scala 26:19]
  assign TLMonitor_1_clock = clock;
  assign TLMonitor_1_reset = reset;
  assign TLMonitor_1_io_in_a_ready = Sideband_io_a_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_valid = auto_side_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_opcode = auto_side_in_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_param = auto_side_in_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_size = auto_side_in_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_source = auto_side_in_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_address = auto_side_in_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_mask = auto_side_in_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_corrupt = auto_side_in_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_ready = auto_side_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_valid = Sideband_io_d_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_opcode = Sideband_io_d_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_param = Sideband_io_d_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_size = Sideband_io_d_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_source = Sideband_io_d_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_sink = Sideband_io_d_bits_sink; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_denied = Sideband_io_d_bits_denied; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_corrupt = Sideband_io_d_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_2_clock = clock;
  assign TLMonitor_2_reset = reset;
  assign TLMonitor_2_io_in_a_ready = _T_555_ready & _T_8582; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_valid = auto_ctl_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_opcode = auto_ctl_in_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_param = auto_ctl_in_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_size = auto_ctl_in_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_source = auto_ctl_in_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_address = auto_ctl_in_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_mask = auto_ctl_in_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_corrupt = auto_ctl_in_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_ready = auto_ctl_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_valid = Queue_io_deq_valid & _T_8583; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_opcode = {{2'd0}, _T_554_bits_read}; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_size = Queue_io_deq_bits_extra_tlrr_extra_size; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_source = Queue_io_deq_bits_extra_tlrr_extra_source; // @[Nodes.scala 26:19]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_8666) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ComposableCache.scala:411 assert (!scheduler.io.resp.valid || flushSelect)\n"); // @[ComposableCache.scala 411:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_8666) begin
          $fatal; // @[ComposableCache.scala 411:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
