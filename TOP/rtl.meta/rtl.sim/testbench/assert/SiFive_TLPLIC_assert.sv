//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLPLIC_assert(
  input         clock,
  input         reset,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [2:0]  auto_in_a_bits_param,
  input  [1:0]  auto_in_a_bits_size,
  input  [10:0] auto_in_a_bits_source,
  input  [27:0] auto_in_a_bits_address,
  input  [7:0]  auto_in_a_bits_mask,
  input         auto_in_a_bits_corrupt,
  input         auto_in_d_ready,
  input         claimer_1,
  input         claimer_0,
  input         claimer_3,
  input         claimer_2,
  input  [8:0]  completerDev,
  input         completer_1,
  input         completer_0,
  input         completer_3,
  input         completer_2,
  input         _T_1750_bits_read,
  input         Queue_io_enq_ready,
  input         Queue_io_deq_valid,
  input  [1:0]  Queue_io_deq_bits_extra_tlrr_extra_size,
  input  [10:0] Queue_io_deq_bits_extra_tlrr_extra_source
);
  wire  TLMonitor_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [10:0] TLMonitor_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [27:0] TLMonitor_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [10:0] TLMonitor_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire [3:0] _T_406; // @[Plic.scala 238:21]
  wire [3:0] _T_411; // @[Plic.scala 238:46]
  wire [3:0] _T_412; // @[Plic.scala 238:28]
  wire  _T_413; // @[Plic.scala 238:58]
  wire  _T_415; // @[Plic.scala 238:11]
  wire  _T_416; // @[Plic.scala 238:11]
  wire [3:0] _T_1469; // @[Plic.scala 255:23]
  wire [3:0] _T_1474; // @[Plic.scala 255:50]
  wire [3:0] _T_1475; // @[Plic.scala 255:30]
  wire  _T_1476; // @[Plic.scala 255:62]
  wire  _T_1478; // @[Plic.scala 255:11]
  wire  _T_1479; // @[Plic.scala 255:11]
  wire  _T_16021; // @[Plic.scala 285:33]
  wire  _T_16023; // @[Plic.scala 285:19]
  wire  _T_16024; // @[Plic.scala 285:19]
  SiFive_TLMonitor_59_assert TLMonitor ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_clock),
    .reset(TLMonitor_reset),
    .io_in_a_ready(TLMonitor_io_in_a_ready),
    .io_in_a_valid(TLMonitor_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_io_in_a_bits_corrupt),
    .io_in_d_ready(TLMonitor_io_in_d_ready),
    .io_in_d_valid(TLMonitor_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_io_in_d_bits_opcode),
    .io_in_d_bits_size(TLMonitor_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_io_in_d_bits_source)
  );
  assign _T_406 = {claimer_3,claimer_2,claimer_1,claimer_0}; // @[Plic.scala 238:21]
  assign _T_411 = _T_406 - 4'h1; // @[Plic.scala 238:46]
  assign _T_412 = _T_406 & _T_411; // @[Plic.scala 238:28]
  assign _T_413 = _T_412 == 4'h0; // @[Plic.scala 238:58]
  assign _T_415 = _T_413 | reset; // @[Plic.scala 238:11]
  assign _T_416 = ~_T_415; // @[Plic.scala 238:11]
  assign _T_1469 = {completer_3,completer_2,completer_1,completer_0}; // @[Plic.scala 255:23]
  assign _T_1474 = _T_1469 - 4'h1; // @[Plic.scala 255:50]
  assign _T_1475 = _T_1469 & _T_1474; // @[Plic.scala 255:30]
  assign _T_1476 = _T_1475 == 4'h0; // @[Plic.scala 255:62]
  assign _T_1478 = _T_1476 | reset; // @[Plic.scala 255:11]
  assign _T_1479 = ~_T_1478; // @[Plic.scala 255:11]
  assign _T_16021 = completerDev == completerDev; // @[Plic.scala 285:33]
  assign _T_16023 = _T_16021 | reset; // @[Plic.scala 285:19]
  assign _T_16024 = ~_T_16023; // @[Plic.scala 285:19]
  assign TLMonitor_clock = clock;
  assign TLMonitor_reset = reset;
  assign TLMonitor_io_in_a_ready = Queue_io_enq_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_valid = auto_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_size = auto_in_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_source = auto_in_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_ready = auto_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_valid = Queue_io_deq_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_opcode = {{2'd0}, _T_1750_bits_read}; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_size = Queue_io_deq_bits_extra_tlrr_extra_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_source = Queue_io_deq_bits_extra_tlrr_extra_source; // @[Nodes.scala 26:19]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_416) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Plic.scala:238 assert((claimer.asUInt & (claimer.asUInt - UInt(1))) === UInt(0)) // One-Hot\n"); // @[Plic.scala 238:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_416) begin
          $fatal; // @[Plic.scala 238:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1479) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Plic.scala:255 assert((completer.asUInt & (completer.asUInt - UInt(1))) === UInt(0)) // One-Hot\n"); // @[Plic.scala 255:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1479) begin
          $fatal; // @[Plic.scala 255:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_16024) begin
          $fwrite(32'h80000002,"Assertion Failed: completerDev should be consistent for all harts\n    at Plic.scala:285 assert(completerDev === data.extract(log2Ceil(nDevices+1)-1, 0),\n"); // @[Plic.scala 285:19]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_16024) begin
          $fatal; // @[Plic.scala 285:19]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_16024) begin
          $fwrite(32'h80000002,"Assertion Failed: completerDev should be consistent for all harts\n    at Plic.scala:285 assert(completerDev === data.extract(log2Ceil(nDevices+1)-1, 0),\n"); // @[Plic.scala 285:19]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_16024) begin
          $fatal; // @[Plic.scala 285:19]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_16024) begin
          $fwrite(32'h80000002,"Assertion Failed: completerDev should be consistent for all harts\n    at Plic.scala:285 assert(completerDev === data.extract(log2Ceil(nDevices+1)-1, 0),\n"); // @[Plic.scala 285:19]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_16024) begin
          $fatal; // @[Plic.scala 285:19]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_16024) begin
          $fwrite(32'h80000002,"Assertion Failed: completerDev should be consistent for all harts\n    at Plic.scala:285 assert(completerDev === data.extract(log2Ceil(nDevices+1)-1, 0),\n"); // @[Plic.scala 285:19]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_16024) begin
          $fatal; // @[Plic.scala 285:19]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
