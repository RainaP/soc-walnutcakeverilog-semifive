//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLXbar_6(
  input          rf_reset,
  input          clock,
  input          reset,
  output         auto_in_3_a_ready,
  input          auto_in_3_a_valid,
  input  [2:0]   auto_in_3_a_bits_opcode,
  input  [2:0]   auto_in_3_a_bits_param,
  input  [2:0]   auto_in_3_a_bits_size,
  input  [3:0]   auto_in_3_a_bits_source,
  input  [35:0]  auto_in_3_a_bits_address,
  input          auto_in_3_a_bits_user_amba_prot_bufferable,
  input          auto_in_3_a_bits_user_amba_prot_modifiable,
  input          auto_in_3_a_bits_user_amba_prot_readalloc,
  input          auto_in_3_a_bits_user_amba_prot_writealloc,
  input          auto_in_3_a_bits_user_amba_prot_privileged,
  input          auto_in_3_a_bits_user_amba_prot_secure,
  input  [15:0]  auto_in_3_a_bits_mask,
  input  [127:0] auto_in_3_a_bits_data,
  input          auto_in_3_d_ready,
  output         auto_in_3_d_valid,
  output [2:0]   auto_in_3_d_bits_opcode,
  output [2:0]   auto_in_3_d_bits_size,
  output [3:0]   auto_in_3_d_bits_source,
  output         auto_in_3_d_bits_denied,
  output [127:0] auto_in_3_d_bits_data,
  output         auto_in_3_d_bits_corrupt,
  output         auto_in_2_a_ready,
  input          auto_in_2_a_valid,
  input  [2:0]   auto_in_2_a_bits_opcode,
  input  [2:0]   auto_in_2_a_bits_param,
  input  [2:0]   auto_in_2_a_bits_size,
  input  [3:0]   auto_in_2_a_bits_source,
  input  [35:0]  auto_in_2_a_bits_address,
  input          auto_in_2_a_bits_user_amba_prot_bufferable,
  input          auto_in_2_a_bits_user_amba_prot_modifiable,
  input          auto_in_2_a_bits_user_amba_prot_readalloc,
  input          auto_in_2_a_bits_user_amba_prot_writealloc,
  input          auto_in_2_a_bits_user_amba_prot_privileged,
  input          auto_in_2_a_bits_user_amba_prot_secure,
  input  [15:0]  auto_in_2_a_bits_mask,
  input  [127:0] auto_in_2_a_bits_data,
  input          auto_in_2_d_ready,
  output         auto_in_2_d_valid,
  output [2:0]   auto_in_2_d_bits_opcode,
  output [2:0]   auto_in_2_d_bits_size,
  output [3:0]   auto_in_2_d_bits_source,
  output         auto_in_2_d_bits_denied,
  output [127:0] auto_in_2_d_bits_data,
  output         auto_in_2_d_bits_corrupt,
  output         auto_in_1_a_ready,
  input          auto_in_1_a_valid,
  input  [2:0]   auto_in_1_a_bits_opcode,
  input  [2:0]   auto_in_1_a_bits_param,
  input  [2:0]   auto_in_1_a_bits_size,
  input  [3:0]   auto_in_1_a_bits_source,
  input  [35:0]  auto_in_1_a_bits_address,
  input          auto_in_1_a_bits_user_amba_prot_bufferable,
  input          auto_in_1_a_bits_user_amba_prot_modifiable,
  input          auto_in_1_a_bits_user_amba_prot_readalloc,
  input          auto_in_1_a_bits_user_amba_prot_writealloc,
  input          auto_in_1_a_bits_user_amba_prot_privileged,
  input          auto_in_1_a_bits_user_amba_prot_secure,
  input  [15:0]  auto_in_1_a_bits_mask,
  input  [127:0] auto_in_1_a_bits_data,
  input          auto_in_1_d_ready,
  output         auto_in_1_d_valid,
  output [2:0]   auto_in_1_d_bits_opcode,
  output [2:0]   auto_in_1_d_bits_size,
  output [3:0]   auto_in_1_d_bits_source,
  output         auto_in_1_d_bits_denied,
  output [127:0] auto_in_1_d_bits_data,
  output         auto_in_1_d_bits_corrupt,
  output         auto_in_0_a_ready,
  input          auto_in_0_a_valid,
  input  [2:0]   auto_in_0_a_bits_opcode,
  input  [2:0]   auto_in_0_a_bits_param,
  input  [2:0]   auto_in_0_a_bits_size,
  input  [3:0]   auto_in_0_a_bits_source,
  input  [35:0]  auto_in_0_a_bits_address,
  input          auto_in_0_a_bits_user_amba_prot_bufferable,
  input          auto_in_0_a_bits_user_amba_prot_modifiable,
  input          auto_in_0_a_bits_user_amba_prot_readalloc,
  input          auto_in_0_a_bits_user_amba_prot_writealloc,
  input          auto_in_0_a_bits_user_amba_prot_privileged,
  input          auto_in_0_a_bits_user_amba_prot_secure,
  input  [15:0]  auto_in_0_a_bits_mask,
  input  [127:0] auto_in_0_a_bits_data,
  input          auto_in_0_d_ready,
  output         auto_in_0_d_valid,
  output [2:0]   auto_in_0_d_bits_opcode,
  output [2:0]   auto_in_0_d_bits_size,
  output [3:0]   auto_in_0_d_bits_source,
  output         auto_in_0_d_bits_denied,
  output [127:0] auto_in_0_d_bits_data,
  output         auto_in_0_d_bits_corrupt,
  input          auto_out_1_a_ready,
  output         auto_out_1_a_valid,
  output [2:0]   auto_out_1_a_bits_opcode,
  output [2:0]   auto_out_1_a_bits_param,
  output [2:0]   auto_out_1_a_bits_size,
  output [5:0]   auto_out_1_a_bits_source,
  output [35:0]  auto_out_1_a_bits_address,
  output         auto_out_1_a_bits_user_amba_prot_bufferable,
  output         auto_out_1_a_bits_user_amba_prot_modifiable,
  output         auto_out_1_a_bits_user_amba_prot_readalloc,
  output         auto_out_1_a_bits_user_amba_prot_writealloc,
  output         auto_out_1_a_bits_user_amba_prot_privileged,
  output         auto_out_1_a_bits_user_amba_prot_secure,
  output [15:0]  auto_out_1_a_bits_mask,
  output [127:0] auto_out_1_a_bits_data,
  output         auto_out_1_d_ready,
  input          auto_out_1_d_valid,
  input  [2:0]   auto_out_1_d_bits_opcode,
  input  [2:0]   auto_out_1_d_bits_size,
  input  [5:0]   auto_out_1_d_bits_source,
  input          auto_out_1_d_bits_denied,
  input  [127:0] auto_out_1_d_bits_data,
  input          auto_out_1_d_bits_corrupt,
  input          auto_out_0_a_ready,
  output         auto_out_0_a_valid,
  output [2:0]   auto_out_0_a_bits_opcode,
  output [2:0]   auto_out_0_a_bits_param,
  output [2:0]   auto_out_0_a_bits_size,
  output [5:0]   auto_out_0_a_bits_source,
  output [27:0]  auto_out_0_a_bits_address,
  output [15:0]  auto_out_0_a_bits_mask,
  output         auto_out_0_d_ready,
  input          auto_out_0_d_valid,
  input  [2:0]   auto_out_0_d_bits_opcode,
  input  [2:0]   auto_out_0_d_bits_size,
  input  [5:0]   auto_out_0_d_bits_source
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
`endif // RANDOMIZE_REG_INIT
  wire [5:0] _GEN_6 = {{2'd0}, auto_in_0_a_bits_source}; // @[Xbar.scala 237:55]
  wire [5:0] in_0_a_bits_source = _GEN_6 | 6'h30; // @[Xbar.scala 237:55]
  reg [1:0] beatsLeft_2; // @[Arbiter.scala 87:30]
  wire  idle_2 = beatsLeft_2 == 2'h0; // @[Arbiter.scala 88:28]
  wire  requestDOI_1_0 = auto_out_1_d_bits_source[5:4] == 2'h3; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_1_0_valid = auto_out_1_d_valid & requestDOI_1_0; // @[Xbar.scala 179:40]
  wire  requestDOI_0_0 = auto_out_0_d_bits_source[5:4] == 2'h3; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered__0_valid = auto_out_0_d_valid & requestDOI_0_0; // @[Xbar.scala 179:40]
  wire [1:0] readys_filter_lo_2 = {portsDIO_filtered_1_0_valid,portsDIO_filtered__0_valid}; // @[Cat.scala 30:58]
  reg [1:0] readys_mask_2; // @[Arbiter.scala 23:23]
  wire [1:0] _readys_filter_T_2 = ~readys_mask_2; // @[Arbiter.scala 24:30]
  wire [1:0] readys_filter_hi_2 = readys_filter_lo_2 & _readys_filter_T_2; // @[Arbiter.scala 24:28]
  wire [3:0] readys_filter_2 = {readys_filter_hi_2,portsDIO_filtered_1_0_valid,portsDIO_filtered__0_valid}; // @[Cat.scala 30:58]
  wire [3:0] _GEN_7 = {{1'd0}, readys_filter_2[3:1]}; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_15 = readys_filter_2 | _GEN_7; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_18 = {readys_mask_2, 2'h0}; // @[Arbiter.scala 25:66]
  wire [3:0] _GEN_8 = {{1'd0}, _readys_unready_T_15[3:1]}; // @[Arbiter.scala 25:58]
  wire [3:0] readys_unready_2 = _GEN_8 | _readys_unready_T_18; // @[Arbiter.scala 25:58]
  wire [1:0] _readys_readys_T_8 = readys_unready_2[3:2] & readys_unready_2[1:0]; // @[Arbiter.scala 26:39]
  wire [1:0] readys_readys_2 = ~_readys_readys_T_8; // @[Arbiter.scala 26:18]
  wire  readys_2_0 = readys_readys_2[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_2_0 = readys_2_0 & portsDIO_filtered__0_valid; // @[Arbiter.scala 97:79]
  reg  state_2_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_2_0 = idle_2 ? earlyWinner_2_0 : state_2_0; // @[Arbiter.scala 117:30]
  wire [5:0] _T_326 = muxStateEarly_2_0 ? auto_out_0_d_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire  readys_2_1 = readys_readys_2[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_2_1 = readys_2_1 & portsDIO_filtered_1_0_valid; // @[Arbiter.scala 97:79]
  reg  state_2_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_2_1 = idle_2 ? earlyWinner_2_1 : state_2_1; // @[Arbiter.scala 117:30]
  wire [5:0] _T_327 = muxStateEarly_2_1 ? auto_out_1_d_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] sink_ACancel_5_bits_source = _T_326 | _T_327; // @[Mux.scala 27:72]
  wire [5:0] _GEN_9 = {{2'd0}, auto_in_1_a_bits_source}; // @[Xbar.scala 237:55]
  wire [5:0] in_1_a_bits_source = _GEN_9 | 6'h20; // @[Xbar.scala 237:55]
  reg [1:0] beatsLeft_3; // @[Arbiter.scala 87:30]
  wire  idle_3 = beatsLeft_3 == 2'h0; // @[Arbiter.scala 88:28]
  wire  requestDOI_1_1 = auto_out_1_d_bits_source[5:4] == 2'h2; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_1_1_valid = auto_out_1_d_valid & requestDOI_1_1; // @[Xbar.scala 179:40]
  wire  requestDOI_0_1 = auto_out_0_d_bits_source[5:4] == 2'h2; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered__1_valid = auto_out_0_d_valid & requestDOI_0_1; // @[Xbar.scala 179:40]
  wire [1:0] readys_filter_lo_3 = {portsDIO_filtered_1_1_valid,portsDIO_filtered__1_valid}; // @[Cat.scala 30:58]
  reg [1:0] readys_mask_3; // @[Arbiter.scala 23:23]
  wire [1:0] _readys_filter_T_3 = ~readys_mask_3; // @[Arbiter.scala 24:30]
  wire [1:0] readys_filter_hi_3 = readys_filter_lo_3 & _readys_filter_T_3; // @[Arbiter.scala 24:28]
  wire [3:0] readys_filter_3 = {readys_filter_hi_3,portsDIO_filtered_1_1_valid,portsDIO_filtered__1_valid}; // @[Cat.scala 30:58]
  wire [3:0] _GEN_10 = {{1'd0}, readys_filter_3[3:1]}; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_20 = readys_filter_3 | _GEN_10; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_23 = {readys_mask_3, 2'h0}; // @[Arbiter.scala 25:66]
  wire [3:0] _GEN_11 = {{1'd0}, _readys_unready_T_20[3:1]}; // @[Arbiter.scala 25:58]
  wire [3:0] readys_unready_3 = _GEN_11 | _readys_unready_T_23; // @[Arbiter.scala 25:58]
  wire [1:0] _readys_readys_T_11 = readys_unready_3[3:2] & readys_unready_3[1:0]; // @[Arbiter.scala 26:39]
  wire [1:0] readys_readys_3 = ~_readys_readys_T_11; // @[Arbiter.scala 26:18]
  wire  readys_3_0 = readys_readys_3[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_3_0 = readys_3_0 & portsDIO_filtered__1_valid; // @[Arbiter.scala 97:79]
  reg  state_3_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_3_0 = idle_3 ? earlyWinner_3_0 : state_3_0; // @[Arbiter.scala 117:30]
  wire [5:0] _T_374 = muxStateEarly_3_0 ? auto_out_0_d_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire  readys_3_1 = readys_readys_3[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_3_1 = readys_3_1 & portsDIO_filtered_1_1_valid; // @[Arbiter.scala 97:79]
  reg  state_3_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_3_1 = idle_3 ? earlyWinner_3_1 : state_3_1; // @[Arbiter.scala 117:30]
  wire [5:0] _T_375 = muxStateEarly_3_1 ? auto_out_1_d_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] sink_ACancel_7_bits_source = _T_374 | _T_375; // @[Mux.scala 27:72]
  wire [4:0] _GEN_12 = {{1'd0}, auto_in_2_a_bits_source}; // @[Xbar.scala 237:55]
  wire [4:0] _in_2_a_bits_source_T = _GEN_12 | 5'h10; // @[Xbar.scala 237:55]
  reg [1:0] beatsLeft_4; // @[Arbiter.scala 87:30]
  wire  idle_4 = beatsLeft_4 == 2'h0; // @[Arbiter.scala 88:28]
  wire  requestDOI_1_2 = auto_out_1_d_bits_source[5:4] == 2'h1; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_1_2_valid = auto_out_1_d_valid & requestDOI_1_2; // @[Xbar.scala 179:40]
  wire  requestDOI_0_2 = auto_out_0_d_bits_source[5:4] == 2'h1; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered__2_valid = auto_out_0_d_valid & requestDOI_0_2; // @[Xbar.scala 179:40]
  wire [1:0] readys_filter_lo_4 = {portsDIO_filtered_1_2_valid,portsDIO_filtered__2_valid}; // @[Cat.scala 30:58]
  reg [1:0] readys_mask_4; // @[Arbiter.scala 23:23]
  wire [1:0] _readys_filter_T_4 = ~readys_mask_4; // @[Arbiter.scala 24:30]
  wire [1:0] readys_filter_hi_4 = readys_filter_lo_4 & _readys_filter_T_4; // @[Arbiter.scala 24:28]
  wire [3:0] readys_filter_4 = {readys_filter_hi_4,portsDIO_filtered_1_2_valid,portsDIO_filtered__2_valid}; // @[Cat.scala 30:58]
  wire [3:0] _GEN_13 = {{1'd0}, readys_filter_4[3:1]}; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_25 = readys_filter_4 | _GEN_13; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_28 = {readys_mask_4, 2'h0}; // @[Arbiter.scala 25:66]
  wire [3:0] _GEN_14 = {{1'd0}, _readys_unready_T_25[3:1]}; // @[Arbiter.scala 25:58]
  wire [3:0] readys_unready_4 = _GEN_14 | _readys_unready_T_28; // @[Arbiter.scala 25:58]
  wire [1:0] _readys_readys_T_14 = readys_unready_4[3:2] & readys_unready_4[1:0]; // @[Arbiter.scala 26:39]
  wire [1:0] readys_readys_4 = ~_readys_readys_T_14; // @[Arbiter.scala 26:18]
  wire  readys_4_0 = readys_readys_4[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_4_0 = readys_4_0 & portsDIO_filtered__2_valid; // @[Arbiter.scala 97:79]
  reg  state_4_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_4_0 = idle_4 ? earlyWinner_4_0 : state_4_0; // @[Arbiter.scala 117:30]
  wire [5:0] _T_422 = muxStateEarly_4_0 ? auto_out_0_d_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire  readys_4_1 = readys_readys_4[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_4_1 = readys_4_1 & portsDIO_filtered_1_2_valid; // @[Arbiter.scala 97:79]
  reg  state_4_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_4_1 = idle_4 ? earlyWinner_4_1 : state_4_1; // @[Arbiter.scala 117:30]
  wire [5:0] _T_423 = muxStateEarly_4_1 ? auto_out_1_d_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] sink_ACancel_9_bits_source = _T_422 | _T_423; // @[Mux.scala 27:72]
  reg [1:0] beatsLeft_5; // @[Arbiter.scala 87:30]
  wire  idle_5 = beatsLeft_5 == 2'h0; // @[Arbiter.scala 88:28]
  wire  requestDOI_1_3 = auto_out_1_d_bits_source[5:4] == 2'h0; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_1_3_valid = auto_out_1_d_valid & requestDOI_1_3; // @[Xbar.scala 179:40]
  wire  requestDOI_0_3 = auto_out_0_d_bits_source[5:4] == 2'h0; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered__3_valid = auto_out_0_d_valid & requestDOI_0_3; // @[Xbar.scala 179:40]
  wire [1:0] readys_filter_lo_5 = {portsDIO_filtered_1_3_valid,portsDIO_filtered__3_valid}; // @[Cat.scala 30:58]
  reg [1:0] readys_mask_5; // @[Arbiter.scala 23:23]
  wire [1:0] _readys_filter_T_5 = ~readys_mask_5; // @[Arbiter.scala 24:30]
  wire [1:0] readys_filter_hi_5 = readys_filter_lo_5 & _readys_filter_T_5; // @[Arbiter.scala 24:28]
  wire [3:0] readys_filter_5 = {readys_filter_hi_5,portsDIO_filtered_1_3_valid,portsDIO_filtered__3_valid}; // @[Cat.scala 30:58]
  wire [3:0] _GEN_15 = {{1'd0}, readys_filter_5[3:1]}; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_30 = readys_filter_5 | _GEN_15; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_33 = {readys_mask_5, 2'h0}; // @[Arbiter.scala 25:66]
  wire [3:0] _GEN_16 = {{1'd0}, _readys_unready_T_30[3:1]}; // @[Arbiter.scala 25:58]
  wire [3:0] readys_unready_5 = _GEN_16 | _readys_unready_T_33; // @[Arbiter.scala 25:58]
  wire [1:0] _readys_readys_T_17 = readys_unready_5[3:2] & readys_unready_5[1:0]; // @[Arbiter.scala 26:39]
  wire [1:0] readys_readys_5 = ~_readys_readys_T_17; // @[Arbiter.scala 26:18]
  wire  readys_5_0 = readys_readys_5[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_5_0 = readys_5_0 & portsDIO_filtered__3_valid; // @[Arbiter.scala 97:79]
  reg  state_5_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_5_0 = idle_5 ? earlyWinner_5_0 : state_5_0; // @[Arbiter.scala 117:30]
  wire [5:0] _T_470 = muxStateEarly_5_0 ? auto_out_0_d_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire  readys_5_1 = readys_readys_5[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_5_1 = readys_5_1 & portsDIO_filtered_1_3_valid; // @[Arbiter.scala 97:79]
  reg  state_5_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_5_1 = idle_5 ? earlyWinner_5_1 : state_5_1; // @[Arbiter.scala 117:30]
  wire [5:0] _T_471 = muxStateEarly_5_1 ? auto_out_1_d_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] sink_ACancel_11_bits_source = _T_470 | _T_471; // @[Mux.scala 27:72]
  wire [36:0] _requestAIO_T_1 = {1'b0,$signed(auto_in_0_a_bits_address)}; // @[Parameters.scala 137:49]
  wire [36:0] _requestAIO_T_3 = $signed(_requestAIO_T_1) & 37'sh800000000; // @[Parameters.scala 137:52]
  wire  requestAIO_0_0 = $signed(_requestAIO_T_3) == 37'sh0; // @[Parameters.scala 137:67]
  wire [35:0] _requestAIO_T_5 = auto_in_0_a_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  wire [36:0] _requestAIO_T_6 = {1'b0,$signed(_requestAIO_T_5)}; // @[Parameters.scala 137:49]
  wire [36:0] _requestAIO_T_8 = $signed(_requestAIO_T_6) & 37'sh800000000; // @[Parameters.scala 137:52]
  wire  requestAIO_0_1 = $signed(_requestAIO_T_8) == 37'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_11 = {1'b0,$signed(auto_in_1_a_bits_address)}; // @[Parameters.scala 137:49]
  wire [36:0] _requestAIO_T_13 = $signed(_requestAIO_T_11) & 37'sh800000000; // @[Parameters.scala 137:52]
  wire  requestAIO_1_0 = $signed(_requestAIO_T_13) == 37'sh0; // @[Parameters.scala 137:67]
  wire [35:0] _requestAIO_T_15 = auto_in_1_a_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  wire [36:0] _requestAIO_T_16 = {1'b0,$signed(_requestAIO_T_15)}; // @[Parameters.scala 137:49]
  wire [36:0] _requestAIO_T_18 = $signed(_requestAIO_T_16) & 37'sh800000000; // @[Parameters.scala 137:52]
  wire  requestAIO_1_1 = $signed(_requestAIO_T_18) == 37'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_21 = {1'b0,$signed(auto_in_2_a_bits_address)}; // @[Parameters.scala 137:49]
  wire [36:0] _requestAIO_T_23 = $signed(_requestAIO_T_21) & 37'sh800000000; // @[Parameters.scala 137:52]
  wire  requestAIO_2_0 = $signed(_requestAIO_T_23) == 37'sh0; // @[Parameters.scala 137:67]
  wire [35:0] _requestAIO_T_25 = auto_in_2_a_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  wire [36:0] _requestAIO_T_26 = {1'b0,$signed(_requestAIO_T_25)}; // @[Parameters.scala 137:49]
  wire [36:0] _requestAIO_T_28 = $signed(_requestAIO_T_26) & 37'sh800000000; // @[Parameters.scala 137:52]
  wire  requestAIO_2_1 = $signed(_requestAIO_T_28) == 37'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_31 = {1'b0,$signed(auto_in_3_a_bits_address)}; // @[Parameters.scala 137:49]
  wire [36:0] _requestAIO_T_33 = $signed(_requestAIO_T_31) & 37'sh800000000; // @[Parameters.scala 137:52]
  wire  requestAIO_3_0 = $signed(_requestAIO_T_33) == 37'sh0; // @[Parameters.scala 137:67]
  wire [35:0] _requestAIO_T_35 = auto_in_3_a_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  wire [36:0] _requestAIO_T_36 = {1'b0,$signed(_requestAIO_T_35)}; // @[Parameters.scala 137:49]
  wire [36:0] _requestAIO_T_38 = $signed(_requestAIO_T_36) & 37'sh800000000; // @[Parameters.scala 137:52]
  wire  requestAIO_3_1 = $signed(_requestAIO_T_38) == 37'sh0; // @[Parameters.scala 137:67]
  wire [12:0] _beatsAI_decode_T_1 = 13'h3f << auto_in_0_a_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsAI_decode_T_3 = ~_beatsAI_decode_T_1[5:0]; // @[package.scala 234:46]
  wire [1:0] beatsAI_decode = _beatsAI_decode_T_3[5:4]; // @[Edges.scala 219:59]
  wire  beatsAI_opdata = ~auto_in_0_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [1:0] beatsAI_0 = beatsAI_opdata ? beatsAI_decode : 2'h0; // @[Edges.scala 220:14]
  wire [12:0] _beatsAI_decode_T_5 = 13'h3f << auto_in_1_a_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsAI_decode_T_7 = ~_beatsAI_decode_T_5[5:0]; // @[package.scala 234:46]
  wire [1:0] beatsAI_decode_1 = _beatsAI_decode_T_7[5:4]; // @[Edges.scala 219:59]
  wire  beatsAI_opdata_1 = ~auto_in_1_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [1:0] beatsAI_1 = beatsAI_opdata_1 ? beatsAI_decode_1 : 2'h0; // @[Edges.scala 220:14]
  wire [12:0] _beatsAI_decode_T_9 = 13'h3f << auto_in_2_a_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsAI_decode_T_11 = ~_beatsAI_decode_T_9[5:0]; // @[package.scala 234:46]
  wire [1:0] beatsAI_decode_2 = _beatsAI_decode_T_11[5:4]; // @[Edges.scala 219:59]
  wire  beatsAI_opdata_2 = ~auto_in_2_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [1:0] beatsAI_2 = beatsAI_opdata_2 ? beatsAI_decode_2 : 2'h0; // @[Edges.scala 220:14]
  wire [12:0] _beatsAI_decode_T_13 = 13'h3f << auto_in_3_a_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsAI_decode_T_15 = ~_beatsAI_decode_T_13[5:0]; // @[package.scala 234:46]
  wire [1:0] beatsAI_decode_3 = _beatsAI_decode_T_15[5:4]; // @[Edges.scala 219:59]
  wire  beatsAI_opdata_3 = ~auto_in_3_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [1:0] beatsAI_3 = beatsAI_opdata_3 ? beatsAI_decode_3 : 2'h0; // @[Edges.scala 220:14]
  wire [12:0] _beatsDO_decode_T_1 = 13'h3f << auto_out_0_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_3 = ~_beatsDO_decode_T_1[5:0]; // @[package.scala 234:46]
  wire [1:0] beatsDO_decode = _beatsDO_decode_T_3[5:4]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata = auto_out_0_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [1:0] beatsDO_0 = beatsDO_opdata ? beatsDO_decode : 2'h0; // @[Edges.scala 220:14]
  wire [12:0] _beatsDO_decode_T_5 = 13'h3f << auto_out_1_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_7 = ~_beatsDO_decode_T_5[5:0]; // @[package.scala 234:46]
  wire [1:0] beatsDO_decode_1 = _beatsDO_decode_T_7[5:4]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_1 = auto_out_1_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [1:0] beatsDO_1 = beatsDO_opdata_1 ? beatsDO_decode_1 : 2'h0; // @[Edges.scala 220:14]
  wire  portsAOI_filtered__0_earlyValid = auto_in_0_a_valid & requestAIO_0_0; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered__1_earlyValid = auto_in_0_a_valid & requestAIO_0_1; // @[Xbar.scala 428:50]
  reg [1:0] beatsLeft; // @[Arbiter.scala 87:30]
  wire  idle = beatsLeft == 2'h0; // @[Arbiter.scala 88:28]
  wire  portsAOI_filtered_3_0_earlyValid = auto_in_3_a_valid & requestAIO_3_0; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered_2_0_earlyValid = auto_in_2_a_valid & requestAIO_2_0; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered_1_0_earlyValid = auto_in_1_a_valid & requestAIO_1_0; // @[Xbar.scala 428:50]
  wire [3:0] readys_filter_lo = {portsAOI_filtered_3_0_earlyValid,portsAOI_filtered_2_0_earlyValid,
    portsAOI_filtered_1_0_earlyValid,portsAOI_filtered__0_earlyValid}; // @[Cat.scala 30:58]
  reg [3:0] readys_mask; // @[Arbiter.scala 23:23]
  wire [3:0] _readys_filter_T = ~readys_mask; // @[Arbiter.scala 24:30]
  wire [3:0] readys_filter_hi = readys_filter_lo & _readys_filter_T; // @[Arbiter.scala 24:28]
  wire [7:0] readys_filter = {readys_filter_hi,portsAOI_filtered_3_0_earlyValid,portsAOI_filtered_2_0_earlyValid,
    portsAOI_filtered_1_0_earlyValid,portsAOI_filtered__0_earlyValid}; // @[Cat.scala 30:58]
  wire [7:0] _GEN_17 = {{1'd0}, readys_filter[7:1]}; // @[package.scala 253:43]
  wire [7:0] _readys_unready_T_1 = readys_filter | _GEN_17; // @[package.scala 253:43]
  wire [7:0] _GEN_18 = {{2'd0}, _readys_unready_T_1[7:2]}; // @[package.scala 253:43]
  wire [7:0] _readys_unready_T_3 = _readys_unready_T_1 | _GEN_18; // @[package.scala 253:43]
  wire [7:0] _readys_unready_T_6 = {readys_mask, 4'h0}; // @[Arbiter.scala 25:66]
  wire [7:0] _GEN_19 = {{1'd0}, _readys_unready_T_3[7:1]}; // @[Arbiter.scala 25:58]
  wire [7:0] readys_unready = _GEN_19 | _readys_unready_T_6; // @[Arbiter.scala 25:58]
  wire [3:0] _readys_readys_T_2 = readys_unready[7:4] & readys_unready[3:0]; // @[Arbiter.scala 26:39]
  wire [3:0] readys_readys = ~_readys_readys_T_2; // @[Arbiter.scala 26:18]
  wire  readys__0 = readys_readys[0]; // @[Arbiter.scala 95:86]
  reg  state__0; // @[Arbiter.scala 116:26]
  wire  allowed__0 = idle ? readys__0 : state__0; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered__0_ready = auto_out_0_a_ready & allowed__0; // @[Arbiter.scala 123:31]
  wire  _portsAOI_in_0_a_ready_T = requestAIO_0_0 & portsAOI_filtered__0_ready; // @[Mux.scala 27:72]
  reg [1:0] beatsLeft_1; // @[Arbiter.scala 87:30]
  wire  idle_1 = beatsLeft_1 == 2'h0; // @[Arbiter.scala 88:28]
  wire  portsAOI_filtered_3_1_earlyValid = auto_in_3_a_valid & requestAIO_3_1; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered_2_1_earlyValid = auto_in_2_a_valid & requestAIO_2_1; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered_1_1_earlyValid = auto_in_1_a_valid & requestAIO_1_1; // @[Xbar.scala 428:50]
  wire [3:0] readys_filter_lo_1 = {portsAOI_filtered_3_1_earlyValid,portsAOI_filtered_2_1_earlyValid,
    portsAOI_filtered_1_1_earlyValid,portsAOI_filtered__1_earlyValid}; // @[Cat.scala 30:58]
  reg [3:0] readys_mask_1; // @[Arbiter.scala 23:23]
  wire [3:0] _readys_filter_T_1 = ~readys_mask_1; // @[Arbiter.scala 24:30]
  wire [3:0] readys_filter_hi_1 = readys_filter_lo_1 & _readys_filter_T_1; // @[Arbiter.scala 24:28]
  wire [7:0] readys_filter_1 = {readys_filter_hi_1,portsAOI_filtered_3_1_earlyValid,portsAOI_filtered_2_1_earlyValid,
    portsAOI_filtered_1_1_earlyValid,portsAOI_filtered__1_earlyValid}; // @[Cat.scala 30:58]
  wire [7:0] _GEN_20 = {{1'd0}, readys_filter_1[7:1]}; // @[package.scala 253:43]
  wire [7:0] _readys_unready_T_8 = readys_filter_1 | _GEN_20; // @[package.scala 253:43]
  wire [7:0] _GEN_21 = {{2'd0}, _readys_unready_T_8[7:2]}; // @[package.scala 253:43]
  wire [7:0] _readys_unready_T_10 = _readys_unready_T_8 | _GEN_21; // @[package.scala 253:43]
  wire [7:0] _readys_unready_T_13 = {readys_mask_1, 4'h0}; // @[Arbiter.scala 25:66]
  wire [7:0] _GEN_22 = {{1'd0}, _readys_unready_T_10[7:1]}; // @[Arbiter.scala 25:58]
  wire [7:0] readys_unready_1 = _GEN_22 | _readys_unready_T_13; // @[Arbiter.scala 25:58]
  wire [3:0] _readys_readys_T_5 = readys_unready_1[7:4] & readys_unready_1[3:0]; // @[Arbiter.scala 26:39]
  wire [3:0] readys_readys_1 = ~_readys_readys_T_5; // @[Arbiter.scala 26:18]
  wire  readys_1_0 = readys_readys_1[0]; // @[Arbiter.scala 95:86]
  reg  state_1_0; // @[Arbiter.scala 116:26]
  wire  allowed_1_0 = idle_1 ? readys_1_0 : state_1_0; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered__1_ready = auto_out_1_a_ready & allowed_1_0; // @[Arbiter.scala 123:31]
  wire  _portsAOI_in_0_a_ready_T_1 = requestAIO_0_1 & portsAOI_filtered__1_ready; // @[Mux.scala 27:72]
  wire  readys__1 = readys_readys[1]; // @[Arbiter.scala 95:86]
  reg  state__1; // @[Arbiter.scala 116:26]
  wire  allowed__1 = idle ? readys__1 : state__1; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_1_0_ready = auto_out_0_a_ready & allowed__1; // @[Arbiter.scala 123:31]
  wire  _portsAOI_in_1_a_ready_T = requestAIO_1_0 & portsAOI_filtered_1_0_ready; // @[Mux.scala 27:72]
  wire  readys_1_1 = readys_readys_1[1]; // @[Arbiter.scala 95:86]
  reg  state_1_1; // @[Arbiter.scala 116:26]
  wire  allowed_1_1 = idle_1 ? readys_1_1 : state_1_1; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_1_1_ready = auto_out_1_a_ready & allowed_1_1; // @[Arbiter.scala 123:31]
  wire  _portsAOI_in_1_a_ready_T_1 = requestAIO_1_1 & portsAOI_filtered_1_1_ready; // @[Mux.scala 27:72]
  wire  readys__2 = readys_readys[2]; // @[Arbiter.scala 95:86]
  reg  state__2; // @[Arbiter.scala 116:26]
  wire  allowed__2 = idle ? readys__2 : state__2; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_2_0_ready = auto_out_0_a_ready & allowed__2; // @[Arbiter.scala 123:31]
  wire  _portsAOI_in_2_a_ready_T = requestAIO_2_0 & portsAOI_filtered_2_0_ready; // @[Mux.scala 27:72]
  wire  readys_1_2 = readys_readys_1[2]; // @[Arbiter.scala 95:86]
  reg  state_1_2; // @[Arbiter.scala 116:26]
  wire  allowed_1_2 = idle_1 ? readys_1_2 : state_1_2; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_2_1_ready = auto_out_1_a_ready & allowed_1_2; // @[Arbiter.scala 123:31]
  wire  _portsAOI_in_2_a_ready_T_1 = requestAIO_2_1 & portsAOI_filtered_2_1_ready; // @[Mux.scala 27:72]
  wire  readys__3 = readys_readys[3]; // @[Arbiter.scala 95:86]
  reg  state__3; // @[Arbiter.scala 116:26]
  wire  allowed__3 = idle ? readys__3 : state__3; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_3_0_ready = auto_out_0_a_ready & allowed__3; // @[Arbiter.scala 123:31]
  wire  _portsAOI_in_3_a_ready_T = requestAIO_3_0 & portsAOI_filtered_3_0_ready; // @[Mux.scala 27:72]
  wire  readys_1_3 = readys_readys_1[3]; // @[Arbiter.scala 95:86]
  reg  state_1_3; // @[Arbiter.scala 116:26]
  wire  allowed_1_3 = idle_1 ? readys_1_3 : state_1_3; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_3_1_ready = auto_out_1_a_ready & allowed_1_3; // @[Arbiter.scala 123:31]
  wire  _portsAOI_in_3_a_ready_T_1 = requestAIO_3_1 & portsAOI_filtered_3_1_ready; // @[Mux.scala 27:72]
  wire  allowed_2_0 = idle_2 ? readys_2_0 : state_2_0; // @[Arbiter.scala 121:24]
  wire  out_10_ready = auto_in_0_d_ready & allowed_2_0; // @[Arbiter.scala 123:31]
  wire  allowed_3_0 = idle_3 ? readys_3_0 : state_3_0; // @[Arbiter.scala 121:24]
  wire  out_14_ready = auto_in_1_d_ready & allowed_3_0; // @[Arbiter.scala 123:31]
  wire  allowed_4_0 = idle_4 ? readys_4_0 : state_4_0; // @[Arbiter.scala 121:24]
  wire  out_18_ready = auto_in_2_d_ready & allowed_4_0; // @[Arbiter.scala 123:31]
  wire  allowed_5_0 = idle_5 ? readys_5_0 : state_5_0; // @[Arbiter.scala 121:24]
  wire  out_22_ready = auto_in_3_d_ready & allowed_5_0; // @[Arbiter.scala 123:31]
  wire  allowed_2_1 = idle_2 ? readys_2_1 : state_2_1; // @[Arbiter.scala 121:24]
  wire  out_11_ready = auto_in_0_d_ready & allowed_2_1; // @[Arbiter.scala 123:31]
  wire  allowed_3_1 = idle_3 ? readys_3_1 : state_3_1; // @[Arbiter.scala 121:24]
  wire  out_15_ready = auto_in_1_d_ready & allowed_3_1; // @[Arbiter.scala 123:31]
  wire  allowed_4_1 = idle_4 ? readys_4_1 : state_4_1; // @[Arbiter.scala 121:24]
  wire  out_19_ready = auto_in_2_d_ready & allowed_4_1; // @[Arbiter.scala 123:31]
  wire  allowed_5_1 = idle_5 ? readys_5_1 : state_5_1; // @[Arbiter.scala 121:24]
  wire  out_23_ready = auto_in_3_d_ready & allowed_5_1; // @[Arbiter.scala 123:31]
  wire  latch = idle & auto_out_0_a_ready; // @[Arbiter.scala 89:24]
  wire [3:0] _readys_mask_T = readys_readys & readys_filter_lo; // @[Arbiter.scala 28:29]
  wire [4:0] _readys_mask_T_1 = {_readys_mask_T, 1'h0}; // @[package.scala 244:48]
  wire [3:0] _readys_mask_T_3 = _readys_mask_T | _readys_mask_T_1[3:0]; // @[package.scala 244:43]
  wire [5:0] _readys_mask_T_4 = {_readys_mask_T_3, 2'h0}; // @[package.scala 244:48]
  wire [3:0] _readys_mask_T_6 = _readys_mask_T_3 | _readys_mask_T_4[3:0]; // @[package.scala 244:43]
  wire  earlyWinner__0 = readys__0 & portsAOI_filtered__0_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner__1 = readys__1 & portsAOI_filtered_1_0_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner__2 = readys__2 & portsAOI_filtered_2_0_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner__3 = readys__3 & portsAOI_filtered_3_0_earlyValid; // @[Arbiter.scala 97:79]
  wire  _T_20 = portsAOI_filtered__0_earlyValid | portsAOI_filtered_1_0_earlyValid | portsAOI_filtered_2_0_earlyValid |
    portsAOI_filtered_3_0_earlyValid; // @[Arbiter.scala 107:36]
  wire [1:0] maskedBeats_0 = earlyWinner__0 ? beatsAI_0 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_1 = earlyWinner__1 ? beatsAI_1 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_2 = earlyWinner__2 ? beatsAI_2 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_3 = earlyWinner__3 ? beatsAI_3 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] _initBeats_T = maskedBeats_0 | maskedBeats_1; // @[Arbiter.scala 112:44]
  wire [1:0] _initBeats_T_1 = _initBeats_T | maskedBeats_2; // @[Arbiter.scala 112:44]
  wire [1:0] initBeats = _initBeats_T_1 | maskedBeats_3; // @[Arbiter.scala 112:44]
  wire  muxStateEarly__0 = idle ? earlyWinner__0 : state__0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly__1 = idle ? earlyWinner__1 : state__1; // @[Arbiter.scala 117:30]
  wire  muxStateEarly__2 = idle ? earlyWinner__2 : state__2; // @[Arbiter.scala 117:30]
  wire  muxStateEarly__3 = idle ? earlyWinner__3 : state__3; // @[Arbiter.scala 117:30]
  wire  _out_0_a_earlyValid_T_9 = state__0 & portsAOI_filtered__0_earlyValid | state__1 &
    portsAOI_filtered_1_0_earlyValid | state__2 & portsAOI_filtered_2_0_earlyValid | state__3 &
    portsAOI_filtered_3_0_earlyValid; // @[Mux.scala 27:72]
  wire  out_4_0_a_earlyValid = idle ? _T_20 : _out_0_a_earlyValid_T_9; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_2 = auto_out_0_a_ready & out_4_0_a_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [1:0] _GEN_23 = {{1'd0}, _beatsLeft_T_2}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_4 = beatsLeft - _GEN_23; // @[Arbiter.scala 113:52]
  wire [15:0] _T_54 = muxStateEarly__0 ? auto_in_0_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_55 = muxStateEarly__1 ? auto_in_1_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_56 = muxStateEarly__2 ? auto_in_2_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_57 = muxStateEarly__3 ? auto_in_3_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_58 = _T_54 | _T_55; // @[Mux.scala 27:72]
  wire [15:0] _T_59 = _T_58 | _T_56; // @[Mux.scala 27:72]
  wire [35:0] _T_110 = muxStateEarly__0 ? auto_in_0_a_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_111 = muxStateEarly__1 ? auto_in_1_a_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_112 = muxStateEarly__2 ? auto_in_2_a_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_113 = muxStateEarly__3 ? auto_in_3_a_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_114 = _T_110 | _T_111; // @[Mux.scala 27:72]
  wire [35:0] _T_115 = _T_114 | _T_112; // @[Mux.scala 27:72]
  wire [35:0] out_4_0_a_bits_address = _T_115 | _T_113; // @[Mux.scala 27:72]
  wire [5:0] _T_117 = muxStateEarly__0 ? in_0_a_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _T_118 = muxStateEarly__1 ? in_1_a_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] in_2_a_bits_source = {{1'd0}, _in_2_a_bits_source_T}; // @[Xbar.scala 231:18 Xbar.scala 237:29]
  wire [5:0] _T_119 = muxStateEarly__2 ? in_2_a_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] in_3_a_bits_source = {{2'd0}, auto_in_3_a_bits_source}; // @[Xbar.scala 231:18 Xbar.scala 237:29]
  wire [5:0] _T_120 = muxStateEarly__3 ? in_3_a_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _T_121 = _T_117 | _T_118; // @[Mux.scala 27:72]
  wire [5:0] _T_122 = _T_121 | _T_119; // @[Mux.scala 27:72]
  wire [2:0] _T_124 = muxStateEarly__0 ? auto_in_0_a_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_125 = muxStateEarly__1 ? auto_in_1_a_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_126 = muxStateEarly__2 ? auto_in_2_a_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_127 = muxStateEarly__3 ? auto_in_3_a_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_128 = _T_124 | _T_125; // @[Mux.scala 27:72]
  wire [2:0] _T_129 = _T_128 | _T_126; // @[Mux.scala 27:72]
  wire [2:0] _T_131 = muxStateEarly__0 ? auto_in_0_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_132 = muxStateEarly__1 ? auto_in_1_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_133 = muxStateEarly__2 ? auto_in_2_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_134 = muxStateEarly__3 ? auto_in_3_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_135 = _T_131 | _T_132; // @[Mux.scala 27:72]
  wire [2:0] _T_136 = _T_135 | _T_133; // @[Mux.scala 27:72]
  wire [2:0] _T_138 = muxStateEarly__0 ? auto_in_0_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_139 = muxStateEarly__1 ? auto_in_1_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_140 = muxStateEarly__2 ? auto_in_2_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_141 = muxStateEarly__3 ? auto_in_3_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_142 = _T_138 | _T_139; // @[Mux.scala 27:72]
  wire [2:0] _T_143 = _T_142 | _T_140; // @[Mux.scala 27:72]
  wire  latch_1 = idle_1 & auto_out_1_a_ready; // @[Arbiter.scala 89:24]
  wire [3:0] _readys_mask_T_8 = readys_readys_1 & readys_filter_lo_1; // @[Arbiter.scala 28:29]
  wire [4:0] _readys_mask_T_9 = {_readys_mask_T_8, 1'h0}; // @[package.scala 244:48]
  wire [3:0] _readys_mask_T_11 = _readys_mask_T_8 | _readys_mask_T_9[3:0]; // @[package.scala 244:43]
  wire [5:0] _readys_mask_T_12 = {_readys_mask_T_11, 2'h0}; // @[package.scala 244:48]
  wire [3:0] _readys_mask_T_14 = _readys_mask_T_11 | _readys_mask_T_12[3:0]; // @[package.scala 244:43]
  wire  earlyWinner_1_0 = readys_1_0 & portsAOI_filtered__1_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_1_1 = readys_1_1 & portsAOI_filtered_1_1_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_1_2 = readys_1_2 & portsAOI_filtered_2_1_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_1_3 = readys_1_3 & portsAOI_filtered_3_1_earlyValid; // @[Arbiter.scala 97:79]
  wire  _T_165 = portsAOI_filtered__1_earlyValid | portsAOI_filtered_1_1_earlyValid | portsAOI_filtered_2_1_earlyValid
     | portsAOI_filtered_3_1_earlyValid; // @[Arbiter.scala 107:36]
  wire [1:0] maskedBeats_0_1 = earlyWinner_1_0 ? beatsAI_0 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_1_1 = earlyWinner_1_1 ? beatsAI_1 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_2_1 = earlyWinner_1_2 ? beatsAI_2 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_3_1 = earlyWinner_1_3 ? beatsAI_3 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] _initBeats_T_2 = maskedBeats_0_1 | maskedBeats_1_1; // @[Arbiter.scala 112:44]
  wire [1:0] _initBeats_T_3 = _initBeats_T_2 | maskedBeats_2_1; // @[Arbiter.scala 112:44]
  wire [1:0] initBeats_1 = _initBeats_T_3 | maskedBeats_3_1; // @[Arbiter.scala 112:44]
  wire  muxStateEarly_1_0 = idle_1 ? earlyWinner_1_0 : state_1_0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_1_1 = idle_1 ? earlyWinner_1_1 : state_1_1; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_1_2 = idle_1 ? earlyWinner_1_2 : state_1_2; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_1_3 = idle_1 ? earlyWinner_1_3 : state_1_3; // @[Arbiter.scala 117:30]
  wire  _out_1_a_earlyValid_T_9 = state_1_0 & portsAOI_filtered__1_earlyValid | state_1_1 &
    portsAOI_filtered_1_1_earlyValid | state_1_2 & portsAOI_filtered_2_1_earlyValid | state_1_3 &
    portsAOI_filtered_3_1_earlyValid; // @[Mux.scala 27:72]
  wire  out_4_1_a_earlyValid = idle_1 ? _T_165 : _out_1_a_earlyValid_T_9; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_8 = auto_out_1_a_ready & out_4_1_a_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [1:0] _GEN_24 = {{1'd0}, _beatsLeft_T_8}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_10 = beatsLeft_1 - _GEN_24; // @[Arbiter.scala 113:52]
  wire [127:0] _T_192 = muxStateEarly_1_0 ? auto_in_0_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_193 = muxStateEarly_1_1 ? auto_in_1_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_194 = muxStateEarly_1_2 ? auto_in_2_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_195 = muxStateEarly_1_3 ? auto_in_3_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_196 = _T_192 | _T_193; // @[Mux.scala 27:72]
  wire [127:0] _T_197 = _T_196 | _T_194; // @[Mux.scala 27:72]
  wire [15:0] _T_199 = muxStateEarly_1_0 ? auto_in_0_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_200 = muxStateEarly_1_1 ? auto_in_1_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_201 = muxStateEarly_1_2 ? auto_in_2_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_202 = muxStateEarly_1_3 ? auto_in_3_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_203 = _T_199 | _T_200; // @[Mux.scala 27:72]
  wire [15:0] _T_204 = _T_203 | _T_201; // @[Mux.scala 27:72]
  wire [35:0] _T_255 = muxStateEarly_1_0 ? auto_in_0_a_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_256 = muxStateEarly_1_1 ? auto_in_1_a_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_257 = muxStateEarly_1_2 ? auto_in_2_a_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_258 = muxStateEarly_1_3 ? auto_in_3_a_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_259 = _T_255 | _T_256; // @[Mux.scala 27:72]
  wire [35:0] _T_260 = _T_259 | _T_257; // @[Mux.scala 27:72]
  wire [5:0] _T_262 = muxStateEarly_1_0 ? in_0_a_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _T_263 = muxStateEarly_1_1 ? in_1_a_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _T_264 = muxStateEarly_1_2 ? in_2_a_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _T_265 = muxStateEarly_1_3 ? in_3_a_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _T_266 = _T_262 | _T_263; // @[Mux.scala 27:72]
  wire [5:0] _T_267 = _T_266 | _T_264; // @[Mux.scala 27:72]
  wire [2:0] _T_269 = muxStateEarly_1_0 ? auto_in_0_a_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_270 = muxStateEarly_1_1 ? auto_in_1_a_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_271 = muxStateEarly_1_2 ? auto_in_2_a_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_272 = muxStateEarly_1_3 ? auto_in_3_a_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_273 = _T_269 | _T_270; // @[Mux.scala 27:72]
  wire [2:0] _T_274 = _T_273 | _T_271; // @[Mux.scala 27:72]
  wire [2:0] _T_276 = muxStateEarly_1_0 ? auto_in_0_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_277 = muxStateEarly_1_1 ? auto_in_1_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_278 = muxStateEarly_1_2 ? auto_in_2_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_279 = muxStateEarly_1_3 ? auto_in_3_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_280 = _T_276 | _T_277; // @[Mux.scala 27:72]
  wire [2:0] _T_281 = _T_280 | _T_278; // @[Mux.scala 27:72]
  wire [2:0] _T_283 = muxStateEarly_1_0 ? auto_in_0_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_284 = muxStateEarly_1_1 ? auto_in_1_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_285 = muxStateEarly_1_2 ? auto_in_2_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_286 = muxStateEarly_1_3 ? auto_in_3_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_287 = _T_283 | _T_284; // @[Mux.scala 27:72]
  wire [2:0] _T_288 = _T_287 | _T_285; // @[Mux.scala 27:72]
  wire  latch_2 = idle_2 & auto_in_0_d_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _readys_mask_T_16 = readys_readys_2 & readys_filter_lo_2; // @[Arbiter.scala 28:29]
  wire [2:0] _readys_mask_T_17 = {_readys_mask_T_16, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_mask_T_19 = _readys_mask_T_16 | _readys_mask_T_17[1:0]; // @[package.scala 244:43]
  wire  _T_300 = portsDIO_filtered__0_valid | portsDIO_filtered_1_0_valid; // @[Arbiter.scala 107:36]
  wire [1:0] maskedBeats_0_2 = earlyWinner_2_0 ? beatsDO_0 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_1_2 = earlyWinner_2_1 ? beatsDO_1 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] initBeats_2 = maskedBeats_0_2 | maskedBeats_1_2; // @[Arbiter.scala 112:44]
  wire  _sink_ACancel_earlyValid_T_3 = state_2_0 & portsDIO_filtered__0_valid | state_2_1 & portsDIO_filtered_1_0_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_5_earlyValid = idle_2 ? _T_300 : _sink_ACancel_earlyValid_T_3; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_14 = auto_in_0_d_ready & sink_ACancel_5_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [1:0] _GEN_25 = {{1'd0}, _beatsLeft_T_14}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_16 = beatsLeft_2 - _GEN_25; // @[Arbiter.scala 113:52]
  wire [2:0] _T_329 = muxStateEarly_2_0 ? auto_out_0_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_330 = muxStateEarly_2_1 ? auto_out_1_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_335 = muxStateEarly_2_0 ? auto_out_0_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_336 = muxStateEarly_2_1 ? auto_out_1_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire  latch_3 = idle_3 & auto_in_1_d_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _readys_mask_T_21 = readys_readys_3 & readys_filter_lo_3; // @[Arbiter.scala 28:29]
  wire [2:0] _readys_mask_T_22 = {_readys_mask_T_21, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_mask_T_24 = _readys_mask_T_21 | _readys_mask_T_22[1:0]; // @[package.scala 244:43]
  wire  _T_348 = portsDIO_filtered__1_valid | portsDIO_filtered_1_1_valid; // @[Arbiter.scala 107:36]
  wire [1:0] maskedBeats_0_3 = earlyWinner_3_0 ? beatsDO_0 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_1_3 = earlyWinner_3_1 ? beatsDO_1 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] initBeats_3 = maskedBeats_0_3 | maskedBeats_1_3; // @[Arbiter.scala 112:44]
  wire  _sink_ACancel_earlyValid_T_8 = state_3_0 & portsDIO_filtered__1_valid | state_3_1 & portsDIO_filtered_1_1_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_7_earlyValid = idle_3 ? _T_348 : _sink_ACancel_earlyValid_T_8; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_20 = auto_in_1_d_ready & sink_ACancel_7_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [1:0] _GEN_26 = {{1'd0}, _beatsLeft_T_20}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_22 = beatsLeft_3 - _GEN_26; // @[Arbiter.scala 113:52]
  wire [2:0] _T_377 = muxStateEarly_3_0 ? auto_out_0_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_378 = muxStateEarly_3_1 ? auto_out_1_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_383 = muxStateEarly_3_0 ? auto_out_0_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_384 = muxStateEarly_3_1 ? auto_out_1_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire  latch_4 = idle_4 & auto_in_2_d_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _readys_mask_T_26 = readys_readys_4 & readys_filter_lo_4; // @[Arbiter.scala 28:29]
  wire [2:0] _readys_mask_T_27 = {_readys_mask_T_26, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_mask_T_29 = _readys_mask_T_26 | _readys_mask_T_27[1:0]; // @[package.scala 244:43]
  wire  _T_396 = portsDIO_filtered__2_valid | portsDIO_filtered_1_2_valid; // @[Arbiter.scala 107:36]
  wire [1:0] maskedBeats_0_4 = earlyWinner_4_0 ? beatsDO_0 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_1_4 = earlyWinner_4_1 ? beatsDO_1 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] initBeats_4 = maskedBeats_0_4 | maskedBeats_1_4; // @[Arbiter.scala 112:44]
  wire  _sink_ACancel_earlyValid_T_13 = state_4_0 & portsDIO_filtered__2_valid | state_4_1 & portsDIO_filtered_1_2_valid
    ; // @[Mux.scala 27:72]
  wire  sink_ACancel_9_earlyValid = idle_4 ? _T_396 : _sink_ACancel_earlyValid_T_13; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_26 = auto_in_2_d_ready & sink_ACancel_9_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [1:0] _GEN_27 = {{1'd0}, _beatsLeft_T_26}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_28 = beatsLeft_4 - _GEN_27; // @[Arbiter.scala 113:52]
  wire [2:0] _T_425 = muxStateEarly_4_0 ? auto_out_0_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_426 = muxStateEarly_4_1 ? auto_out_1_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_431 = muxStateEarly_4_0 ? auto_out_0_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_432 = muxStateEarly_4_1 ? auto_out_1_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire  latch_5 = idle_5 & auto_in_3_d_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _readys_mask_T_31 = readys_readys_5 & readys_filter_lo_5; // @[Arbiter.scala 28:29]
  wire [2:0] _readys_mask_T_32 = {_readys_mask_T_31, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_mask_T_34 = _readys_mask_T_31 | _readys_mask_T_32[1:0]; // @[package.scala 244:43]
  wire  _T_444 = portsDIO_filtered__3_valid | portsDIO_filtered_1_3_valid; // @[Arbiter.scala 107:36]
  wire [1:0] maskedBeats_0_5 = earlyWinner_5_0 ? beatsDO_0 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_1_5 = earlyWinner_5_1 ? beatsDO_1 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] initBeats_5 = maskedBeats_0_5 | maskedBeats_1_5; // @[Arbiter.scala 112:44]
  wire  _sink_ACancel_earlyValid_T_18 = state_5_0 & portsDIO_filtered__3_valid | state_5_1 & portsDIO_filtered_1_3_valid
    ; // @[Mux.scala 27:72]
  wire  sink_ACancel_11_earlyValid = idle_5 ? _T_444 : _sink_ACancel_earlyValid_T_18; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_32 = auto_in_3_d_ready & sink_ACancel_11_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [1:0] _GEN_28 = {{1'd0}, _beatsLeft_T_32}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_34 = beatsLeft_5 - _GEN_28; // @[Arbiter.scala 113:52]
  wire [2:0] _T_473 = muxStateEarly_5_0 ? auto_out_0_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_474 = muxStateEarly_5_1 ? auto_out_1_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_479 = muxStateEarly_5_0 ? auto_out_0_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_480 = muxStateEarly_5_1 ? auto_out_1_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  assign auto_in_3_a_ready = _portsAOI_in_3_a_ready_T | _portsAOI_in_3_a_ready_T_1; // @[Mux.scala 27:72]
  assign auto_in_3_d_valid = idle_5 ? _T_444 : _sink_ACancel_earlyValid_T_18; // @[Arbiter.scala 125:29]
  assign auto_in_3_d_bits_opcode = _T_479 | _T_480; // @[Mux.scala 27:72]
  assign auto_in_3_d_bits_size = _T_473 | _T_474; // @[Mux.scala 27:72]
  assign auto_in_3_d_bits_source = sink_ACancel_11_bits_source[3:0]; // @[Xbar.scala 228:69]
  assign auto_in_3_d_bits_denied = muxStateEarly_5_1 & auto_out_1_d_bits_denied; // @[Mux.scala 27:72]
  assign auto_in_3_d_bits_data = muxStateEarly_5_1 ? auto_out_1_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  assign auto_in_3_d_bits_corrupt = muxStateEarly_5_1 & auto_out_1_d_bits_corrupt; // @[Mux.scala 27:72]
  assign auto_in_2_a_ready = _portsAOI_in_2_a_ready_T | _portsAOI_in_2_a_ready_T_1; // @[Mux.scala 27:72]
  assign auto_in_2_d_valid = idle_4 ? _T_396 : _sink_ACancel_earlyValid_T_13; // @[Arbiter.scala 125:29]
  assign auto_in_2_d_bits_opcode = _T_431 | _T_432; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_size = _T_425 | _T_426; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_source = sink_ACancel_9_bits_source[3:0]; // @[Xbar.scala 228:69]
  assign auto_in_2_d_bits_denied = muxStateEarly_4_1 & auto_out_1_d_bits_denied; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_data = muxStateEarly_4_1 ? auto_out_1_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_corrupt = muxStateEarly_4_1 & auto_out_1_d_bits_corrupt; // @[Mux.scala 27:72]
  assign auto_in_1_a_ready = _portsAOI_in_1_a_ready_T | _portsAOI_in_1_a_ready_T_1; // @[Mux.scala 27:72]
  assign auto_in_1_d_valid = idle_3 ? _T_348 : _sink_ACancel_earlyValid_T_8; // @[Arbiter.scala 125:29]
  assign auto_in_1_d_bits_opcode = _T_383 | _T_384; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_size = _T_377 | _T_378; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_source = sink_ACancel_7_bits_source[3:0]; // @[Xbar.scala 228:69]
  assign auto_in_1_d_bits_denied = muxStateEarly_3_1 & auto_out_1_d_bits_denied; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_data = muxStateEarly_3_1 ? auto_out_1_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_corrupt = muxStateEarly_3_1 & auto_out_1_d_bits_corrupt; // @[Mux.scala 27:72]
  assign auto_in_0_a_ready = _portsAOI_in_0_a_ready_T | _portsAOI_in_0_a_ready_T_1; // @[Mux.scala 27:72]
  assign auto_in_0_d_valid = idle_2 ? _T_300 : _sink_ACancel_earlyValid_T_3; // @[Arbiter.scala 125:29]
  assign auto_in_0_d_bits_opcode = _T_335 | _T_336; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_size = _T_329 | _T_330; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_source = sink_ACancel_5_bits_source[3:0]; // @[Xbar.scala 228:69]
  assign auto_in_0_d_bits_denied = muxStateEarly_2_1 & auto_out_1_d_bits_denied; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_data = muxStateEarly_2_1 ? auto_out_1_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_corrupt = muxStateEarly_2_1 & auto_out_1_d_bits_corrupt; // @[Mux.scala 27:72]
  assign auto_out_1_a_valid = idle_1 ? _T_165 : _out_1_a_earlyValid_T_9; // @[Arbiter.scala 125:29]
  assign auto_out_1_a_bits_opcode = _T_288 | _T_286; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_param = _T_281 | _T_279; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_size = _T_274 | _T_272; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_source = _T_267 | _T_265; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_address = _T_260 | _T_258; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_bufferable = muxStateEarly_1_0 & auto_in_0_a_bits_user_amba_prot_bufferable |
    muxStateEarly_1_1 & auto_in_1_a_bits_user_amba_prot_bufferable | muxStateEarly_1_2 &
    auto_in_2_a_bits_user_amba_prot_bufferable | muxStateEarly_1_3 & auto_in_3_a_bits_user_amba_prot_bufferable; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_modifiable = muxStateEarly_1_0 & auto_in_0_a_bits_user_amba_prot_modifiable |
    muxStateEarly_1_1 & auto_in_1_a_bits_user_amba_prot_modifiable | muxStateEarly_1_2 &
    auto_in_2_a_bits_user_amba_prot_modifiable | muxStateEarly_1_3 & auto_in_3_a_bits_user_amba_prot_modifiable; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_readalloc = muxStateEarly_1_0 & auto_in_0_a_bits_user_amba_prot_readalloc |
    muxStateEarly_1_1 & auto_in_1_a_bits_user_amba_prot_readalloc | muxStateEarly_1_2 &
    auto_in_2_a_bits_user_amba_prot_readalloc | muxStateEarly_1_3 & auto_in_3_a_bits_user_amba_prot_readalloc; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_writealloc = muxStateEarly_1_0 & auto_in_0_a_bits_user_amba_prot_writealloc |
    muxStateEarly_1_1 & auto_in_1_a_bits_user_amba_prot_writealloc | muxStateEarly_1_2 &
    auto_in_2_a_bits_user_amba_prot_writealloc | muxStateEarly_1_3 & auto_in_3_a_bits_user_amba_prot_writealloc; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_privileged = muxStateEarly_1_0 & auto_in_0_a_bits_user_amba_prot_privileged |
    muxStateEarly_1_1 & auto_in_1_a_bits_user_amba_prot_privileged | muxStateEarly_1_2 &
    auto_in_2_a_bits_user_amba_prot_privileged | muxStateEarly_1_3 & auto_in_3_a_bits_user_amba_prot_privileged; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_secure = muxStateEarly_1_0 & auto_in_0_a_bits_user_amba_prot_secure |
    muxStateEarly_1_1 & auto_in_1_a_bits_user_amba_prot_secure | muxStateEarly_1_2 &
    auto_in_2_a_bits_user_amba_prot_secure | muxStateEarly_1_3 & auto_in_3_a_bits_user_amba_prot_secure; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_mask = _T_204 | _T_202; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_data = _T_197 | _T_195; // @[Mux.scala 27:72]
  assign auto_out_1_d_ready = requestDOI_1_0 & out_11_ready | requestDOI_1_1 & out_15_ready | requestDOI_1_2 &
    out_19_ready | requestDOI_1_3 & out_23_ready; // @[Mux.scala 27:72]
  assign auto_out_0_a_valid = idle ? _T_20 : _out_0_a_earlyValid_T_9; // @[Arbiter.scala 125:29]
  assign auto_out_0_a_bits_opcode = _T_143 | _T_141; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_param = _T_136 | _T_134; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_size = _T_129 | _T_127; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_source = _T_122 | _T_120; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_address = out_4_0_a_bits_address[27:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_0_a_bits_mask = _T_59 | _T_57; // @[Mux.scala 27:72]
  assign auto_out_0_d_ready = requestDOI_0_0 & out_10_ready | requestDOI_0_1 & out_14_ready | requestDOI_0_2 &
    out_18_ready | requestDOI_0_3 & out_22_ready; // @[Mux.scala 27:72]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_2 <= 2'h0;
    end else if (latch_2) begin
      beatsLeft_2 <= initBeats_2;
    end else begin
      beatsLeft_2 <= _beatsLeft_T_16;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_2 <= 2'h3;
    end else if (latch_2 & |readys_filter_lo_2) begin
      readys_mask_2 <= _readys_mask_T_19;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_2_0 <= 1'h0;
    end else if (idle_2) begin
      state_2_0 <= earlyWinner_2_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_2_1 <= 1'h0;
    end else if (idle_2) begin
      state_2_1 <= earlyWinner_2_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_3 <= 2'h0;
    end else if (latch_3) begin
      beatsLeft_3 <= initBeats_3;
    end else begin
      beatsLeft_3 <= _beatsLeft_T_22;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_3 <= 2'h3;
    end else if (latch_3 & |readys_filter_lo_3) begin
      readys_mask_3 <= _readys_mask_T_24;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_3_0 <= 1'h0;
    end else if (idle_3) begin
      state_3_0 <= earlyWinner_3_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_3_1 <= 1'h0;
    end else if (idle_3) begin
      state_3_1 <= earlyWinner_3_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_4 <= 2'h0;
    end else if (latch_4) begin
      beatsLeft_4 <= initBeats_4;
    end else begin
      beatsLeft_4 <= _beatsLeft_T_28;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_4 <= 2'h3;
    end else if (latch_4 & |readys_filter_lo_4) begin
      readys_mask_4 <= _readys_mask_T_29;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_4_0 <= 1'h0;
    end else if (idle_4) begin
      state_4_0 <= earlyWinner_4_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_4_1 <= 1'h0;
    end else if (idle_4) begin
      state_4_1 <= earlyWinner_4_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_5 <= 2'h0;
    end else if (latch_5) begin
      beatsLeft_5 <= initBeats_5;
    end else begin
      beatsLeft_5 <= _beatsLeft_T_34;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_5 <= 2'h3;
    end else if (latch_5 & |readys_filter_lo_5) begin
      readys_mask_5 <= _readys_mask_T_34;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_5_0 <= 1'h0;
    end else if (idle_5) begin
      state_5_0 <= earlyWinner_5_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_5_1 <= 1'h0;
    end else if (idle_5) begin
      state_5_1 <= earlyWinner_5_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft <= 2'h0;
    end else if (latch) begin
      beatsLeft <= initBeats;
    end else begin
      beatsLeft <= _beatsLeft_T_4;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask <= 4'hf;
    end else if (latch & |readys_filter_lo) begin
      readys_mask <= _readys_mask_T_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state__0 <= 1'h0;
    end else if (idle) begin
      state__0 <= earlyWinner__0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_1 <= 2'h0;
    end else if (latch_1) begin
      beatsLeft_1 <= initBeats_1;
    end else begin
      beatsLeft_1 <= _beatsLeft_T_10;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_1 <= 4'hf;
    end else if (latch_1 & |readys_filter_lo_1) begin
      readys_mask_1 <= _readys_mask_T_14;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_1_0 <= 1'h0;
    end else if (idle_1) begin
      state_1_0 <= earlyWinner_1_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state__1 <= 1'h0;
    end else if (idle) begin
      state__1 <= earlyWinner__1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_1_1 <= 1'h0;
    end else if (idle_1) begin
      state_1_1 <= earlyWinner_1_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state__2 <= 1'h0;
    end else if (idle) begin
      state__2 <= earlyWinner__2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_1_2 <= 1'h0;
    end else if (idle_1) begin
      state_1_2 <= earlyWinner_1_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state__3 <= 1'h0;
    end else if (idle) begin
      state__3 <= earlyWinner__3;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_1_3 <= 1'h0;
    end else if (idle_1) begin
      state_1_3 <= earlyWinner_1_3;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  beatsLeft_2 = _RAND_0[1:0];
  _RAND_1 = {1{`RANDOM}};
  readys_mask_2 = _RAND_1[1:0];
  _RAND_2 = {1{`RANDOM}};
  state_2_0 = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  state_2_1 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  beatsLeft_3 = _RAND_4[1:0];
  _RAND_5 = {1{`RANDOM}};
  readys_mask_3 = _RAND_5[1:0];
  _RAND_6 = {1{`RANDOM}};
  state_3_0 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  state_3_1 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  beatsLeft_4 = _RAND_8[1:0];
  _RAND_9 = {1{`RANDOM}};
  readys_mask_4 = _RAND_9[1:0];
  _RAND_10 = {1{`RANDOM}};
  state_4_0 = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  state_4_1 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  beatsLeft_5 = _RAND_12[1:0];
  _RAND_13 = {1{`RANDOM}};
  readys_mask_5 = _RAND_13[1:0];
  _RAND_14 = {1{`RANDOM}};
  state_5_0 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  state_5_1 = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  beatsLeft = _RAND_16[1:0];
  _RAND_17 = {1{`RANDOM}};
  readys_mask = _RAND_17[3:0];
  _RAND_18 = {1{`RANDOM}};
  state__0 = _RAND_18[0:0];
  _RAND_19 = {1{`RANDOM}};
  beatsLeft_1 = _RAND_19[1:0];
  _RAND_20 = {1{`RANDOM}};
  readys_mask_1 = _RAND_20[3:0];
  _RAND_21 = {1{`RANDOM}};
  state_1_0 = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  state__1 = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  state_1_1 = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  state__2 = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  state_1_2 = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  state__3 = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  state_1_3 = _RAND_27[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    beatsLeft_2 = 2'h0;
  end
  if (reset) begin
    readys_mask_2 = 2'h3;
  end
  if (reset) begin
    state_2_0 = 1'h0;
  end
  if (reset) begin
    state_2_1 = 1'h0;
  end
  if (reset) begin
    beatsLeft_3 = 2'h0;
  end
  if (reset) begin
    readys_mask_3 = 2'h3;
  end
  if (reset) begin
    state_3_0 = 1'h0;
  end
  if (reset) begin
    state_3_1 = 1'h0;
  end
  if (reset) begin
    beatsLeft_4 = 2'h0;
  end
  if (reset) begin
    readys_mask_4 = 2'h3;
  end
  if (reset) begin
    state_4_0 = 1'h0;
  end
  if (reset) begin
    state_4_1 = 1'h0;
  end
  if (reset) begin
    beatsLeft_5 = 2'h0;
  end
  if (reset) begin
    readys_mask_5 = 2'h3;
  end
  if (reset) begin
    state_5_0 = 1'h0;
  end
  if (reset) begin
    state_5_1 = 1'h0;
  end
  if (reset) begin
    beatsLeft = 2'h0;
  end
  if (reset) begin
    readys_mask = 4'hf;
  end
  if (reset) begin
    state__0 = 1'h0;
  end
  if (reset) begin
    beatsLeft_1 = 2'h0;
  end
  if (reset) begin
    readys_mask_1 = 4'hf;
  end
  if (reset) begin
    state_1_0 = 1'h0;
  end
  if (reset) begin
    state__1 = 1'h0;
  end
  if (reset) begin
    state_1_1 = 1'h0;
  end
  if (reset) begin
    state__2 = 1'h0;
  end
  if (reset) begin
    state_1_2 = 1'h0;
  end
  if (reset) begin
    state__3 = 1'h0;
  end
  if (reset) begin
    state_1_3 = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
