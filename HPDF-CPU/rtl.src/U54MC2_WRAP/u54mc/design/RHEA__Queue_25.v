//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__Queue_25(
  input         rf_reset,
  input         clock,
  input         reset,
  output        io_enq_ready,
  input         io_enq_valid,
  input  [2:0]  io_enq_bits_opcode,
  input  [1:0]  io_enq_bits_param,
  input  [3:0]  io_enq_bits_size,
  input  [5:0]  io_enq_bits_source,
  input  [4:0]  io_enq_bits_sink,
  input         io_enq_bits_denied,
  input  [63:0] io_enq_bits_data,
  input         io_enq_bits_corrupt,
  input         io_deq_ready,
  output        io_deq_valid,
  output [2:0]  io_deq_bits_opcode,
  output [1:0]  io_deq_bits_param,
  output [3:0]  io_deq_bits_size,
  output [5:0]  io_deq_bits_source,
  output [4:0]  io_deq_bits_sink,
  output        io_deq_bits_denied,
  output [63:0] io_deq_bits_data,
  output        io_deq_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [63:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [63:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
`endif // RANDOMIZE_REG_INIT
  reg [2:0] ram_0_opcode; // @[Decoupled.scala 218:16]
  reg [1:0] ram_0_param; // @[Decoupled.scala 218:16]
  reg [3:0] ram_0_size; // @[Decoupled.scala 218:16]
  reg [5:0] ram_0_source; // @[Decoupled.scala 218:16]
  reg [4:0] ram_0_sink; // @[Decoupled.scala 218:16]
  reg  ram_0_denied; // @[Decoupled.scala 218:16]
  reg [63:0] ram_0_data; // @[Decoupled.scala 218:16]
  reg  ram_0_corrupt; // @[Decoupled.scala 218:16]
  reg [2:0] ram_1_opcode; // @[Decoupled.scala 218:16]
  reg [1:0] ram_1_param; // @[Decoupled.scala 218:16]
  reg [3:0] ram_1_size; // @[Decoupled.scala 218:16]
  reg [5:0] ram_1_source; // @[Decoupled.scala 218:16]
  reg [4:0] ram_1_sink; // @[Decoupled.scala 218:16]
  reg  ram_1_denied; // @[Decoupled.scala 218:16]
  reg [63:0] ram_1_data; // @[Decoupled.scala 218:16]
  reg  ram_1_corrupt; // @[Decoupled.scala 218:16]
  reg  value_1; // @[Counter.scala 60:40]
  wire  do_enq = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  reg  value; // @[Counter.scala 60:40]
  reg  maybe_full; // @[Decoupled.scala 221:27]
  wire  ptr_match = value == value_1; // @[Decoupled.scala 223:33]
  wire  empty = ptr_match & ~maybe_full; // @[Decoupled.scala 224:25]
  wire  full = ptr_match & maybe_full; // @[Decoupled.scala 225:24]
  wire  do_deq = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  assign io_enq_ready = ~full; // @[Decoupled.scala 241:19]
  assign io_deq_valid = ~empty; // @[Decoupled.scala 240:19]
  assign io_deq_bits_opcode = value_1 ? ram_1_opcode : ram_0_opcode; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_param = value_1 ? ram_1_param : ram_0_param; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_size = value_1 ? ram_1_size : ram_0_size; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_source = value_1 ? ram_1_source : ram_0_source; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_sink = value_1 ? ram_1_sink : ram_0_sink; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_denied = value_1 ? ram_1_denied : ram_0_denied; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_data = value_1 ? ram_1_data : ram_0_data; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_corrupt = value_1 ? ram_1_corrupt : ram_0_corrupt; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_opcode <= 3'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_param <= 2'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_size <= 4'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_size <= io_enq_bits_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_source <= 6'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_sink <= 5'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_sink <= io_enq_bits_sink;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_denied <= 1'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_denied <= io_enq_bits_denied;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_data <= 64'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_corrupt <= 1'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_corrupt <= io_enq_bits_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_opcode <= 3'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_param <= 2'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_size <= 4'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_size <= io_enq_bits_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_source <= 6'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_sink <= 5'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_sink <= io_enq_bits_sink;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_denied <= 1'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_denied <= io_enq_bits_denied;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_data <= 64'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_corrupt <= 1'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_corrupt <= io_enq_bits_corrupt;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      value_1 <= 1'h0;
    end else if (do_deq) begin
      value_1 <= value_1 + 1'h1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      value <= 1'h0;
    end else if (do_enq) begin
      value <= value + 1'h1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      maybe_full <= 1'h0;
    end else if (do_enq != do_deq) begin
      maybe_full <= do_enq;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ram_0_opcode = _RAND_0[2:0];
  _RAND_1 = {1{`RANDOM}};
  ram_0_param = _RAND_1[1:0];
  _RAND_2 = {1{`RANDOM}};
  ram_0_size = _RAND_2[3:0];
  _RAND_3 = {1{`RANDOM}};
  ram_0_source = _RAND_3[5:0];
  _RAND_4 = {1{`RANDOM}};
  ram_0_sink = _RAND_4[4:0];
  _RAND_5 = {1{`RANDOM}};
  ram_0_denied = _RAND_5[0:0];
  _RAND_6 = {2{`RANDOM}};
  ram_0_data = _RAND_6[63:0];
  _RAND_7 = {1{`RANDOM}};
  ram_0_corrupt = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  ram_1_opcode = _RAND_8[2:0];
  _RAND_9 = {1{`RANDOM}};
  ram_1_param = _RAND_9[1:0];
  _RAND_10 = {1{`RANDOM}};
  ram_1_size = _RAND_10[3:0];
  _RAND_11 = {1{`RANDOM}};
  ram_1_source = _RAND_11[5:0];
  _RAND_12 = {1{`RANDOM}};
  ram_1_sink = _RAND_12[4:0];
  _RAND_13 = {1{`RANDOM}};
  ram_1_denied = _RAND_13[0:0];
  _RAND_14 = {2{`RANDOM}};
  ram_1_data = _RAND_14[63:0];
  _RAND_15 = {1{`RANDOM}};
  ram_1_corrupt = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  value_1 = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  value = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  maybe_full = _RAND_18[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ram_0_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_0_param = 2'h0;
  end
  if (rf_reset) begin
    ram_0_size = 4'h0;
  end
  if (rf_reset) begin
    ram_0_source = 6'h0;
  end
  if (rf_reset) begin
    ram_0_sink = 5'h0;
  end
  if (rf_reset) begin
    ram_0_denied = 1'h0;
  end
  if (rf_reset) begin
    ram_0_data = 64'h0;
  end
  if (rf_reset) begin
    ram_0_corrupt = 1'h0;
  end
  if (rf_reset) begin
    ram_1_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_1_param = 2'h0;
  end
  if (rf_reset) begin
    ram_1_size = 4'h0;
  end
  if (rf_reset) begin
    ram_1_source = 6'h0;
  end
  if (rf_reset) begin
    ram_1_sink = 5'h0;
  end
  if (rf_reset) begin
    ram_1_denied = 1'h0;
  end
  if (rf_reset) begin
    ram_1_data = 64'h0;
  end
  if (rf_reset) begin
    ram_1_corrupt = 1'h0;
  end
  if (reset) begin
    value_1 = 1'h0;
  end
  if (reset) begin
    value = 1'h0;
  end
  if (reset) begin
    maybe_full = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
