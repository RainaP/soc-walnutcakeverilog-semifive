# Init workspace & Run simple test
```
wit init workspace-walnutcakeverilog -a git@portal:league/soc-walnutcakeverilog-semifive.git
```
# Module load
```
module load synopsys/vcs/Q-2020.03-SP2-3
module load synopsys/verdi/Q-2020.03-SP2-3
```
# Run simple test
```
cd soc-walnutcake-verilog/sim
make hello.vcs.out
```
