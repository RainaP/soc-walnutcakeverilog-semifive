`timescale 1ns/1ps
module tb_top;

/*AUTOWIRE*/
// Beginning of automatic wires (for undeclared instantiated-module outputs)
wire			OTP_0_VBG;		// To/From duv of TOP.v
wire			OTP_0_VPP;		// To/From duv of TOP.v
wire			OTP_0_VREFM;		// To/From duv of TOP.v
wire			OTP_0_VTDO;		// To/From duv of TOP.v
wire			OTP_1_VBG;		// To/From duv of TOP.v
wire			OTP_1_VPP;		// To/From duv of TOP.v
wire			OTP_1_VREFM;		// To/From duv of TOP.v
wire			OTP_1_VTDO;		// To/From duv of TOP.v
wire			SDMMC_0;		// To/From duv of TOP.v
wire			SDMMC_1;		// To/From duv of TOP.v
wire			SDMMC_10;		// To/From duv of TOP.v
wire			SDMMC_11;		// To/From duv of TOP.v
wire			SDMMC_2;		// To/From duv of TOP.v
wire			SDMMC_3;		// To/From duv of TOP.v
wire			SDMMC_4;		// To/From duv of TOP.v
wire			SDMMC_5;		// To/From duv of TOP.v
wire			SDMMC_6;		// To/From duv of TOP.v
wire			SDMMC_7;		// To/From duv of TOP.v
wire			SDMMC_8;		// To/From duv of TOP.v
wire			SDMMC_9;		// To/From duv of TOP.v
wire			USB3_0__DM;		// To/From duv of TOP.v
wire			USB3_0__DP;		// To/From duv of TOP.v
wire			USB3_0__RESREF;		// To/From duv of TOP.v
wire			USB3_0__TXM;		// From duv of TOP.v
wire			USB3_0__TXP;		// From duv of TOP.v
wire			USB3_0__VBUS;		// To/From duv of TOP.v
// End of automatics
/*AUTOREG*/
reg rstn;
reg clk;
wire [15:0] GPIOA;
wire [15:0] GPIOB;
wire [15:0] GPIOC;
wire [15:0] GPIOD;
pullup (weak1) (GPIOA[0]);
pullup (weak1) (GPIOA[1]);
pullup (weak1) (GPIOA[2]);
pullup (weak1) (GPIOA[3]);
pullup (weak1) (GPIOA[4]);
pullup (weak1) (GPIOA[5]);
pullup (weak1) (GPIOA[6]);
pullup (weak1) (GPIOA[7]);
pullup (weak1) (GPIOA[8]);
pullup (weak1) (GPIOA[9]);
pullup (weak1) (GPIOA[10]);
pullup (weak1) (GPIOA[11]);
pullup (weak1) (GPIOA[12]);
pullup (weak1) (GPIOA[13]);
pullup (weak1) (GPIOA[14]);
pullup (weak1) (GPIOA[15]);
pullup (weak1) (GPIOB[0]);
pullup (weak1) (GPIOB[1]);
pullup (weak1) (GPIOB[2]);
pullup (weak1) (GPIOB[3]);
pullup (weak1) (GPIOB[4]);
pullup (weak1) (GPIOB[5]);
pullup (weak1) (GPIOB[6]);
pullup (weak1) (GPIOB[7]);
pullup (weak1) (GPIOB[8]);
pullup (weak1) (GPIOB[9]);
pullup (weak1) (GPIOB[10]);
pullup (weak1) (GPIOB[11]);
pullup (weak1) (GPIOB[12]);
pullup (weak1) (GPIOB[13]);
pullup (weak1) (GPIOB[14]);
pullup (weak1) (GPIOB[15]);
pullup (weak1) (GPIOC[0]);
pullup (weak1) (GPIOC[1]);
pullup (weak1) (GPIOC[2]);
pullup (weak1) (GPIOC[3]);
pullup (weak1) (GPIOC[4]);
pullup (weak1) (GPIOC[5]);
pullup (weak1) (GPIOC[6]);
pullup (weak1) (GPIOC[7]);
pullup (weak1) (GPIOC[8]);
pullup (weak1) (GPIOC[9]);
pullup (weak1) (GPIOC[10]);
pullup (weak1) (GPIOC[11]);
pullup (weak1) (GPIOC[12]);
pullup (weak1) (GPIOC[13]);
pullup (weak1) (GPIOC[14]);
pullup (weak1) (GPIOC[15]);
pullup (weak1) (GPIOD[0]);
pullup (weak1) (GPIOD[1]);
pullup (weak1) (GPIOD[2]);
pullup (weak1) (GPIOD[3]);
pullup (weak1) (GPIOD[4]);
pullup (weak1) (GPIOD[5]);
pullup (weak1) (GPIOD[6]);
pullup (weak1) (GPIOD[7]);
pullup (weak1) (GPIOD[8]);
pullup (weak1) (GPIOD[9]);
pullup (weak1) (GPIOD[10]);
pullup (weak1) (GPIOD[11]);
pullup (weak1) (GPIOD[12]);
pullup (weak1) (GPIOD[13]);
pullup (weak1) (GPIOD[14]);
pullup (weak1) (GPIOD[15]);

// UART0 Loopback
// GPIOA14 = UART0_TX
// GPIOA15 = UART0_RX
assign GPIOA[15] = GPIOA[14];
initial begin
    rstn = 1;
    clk = 0;
end

always begin
    #20.833 clk = ~clk;
end

//`ifdef DDR_EMPTY
//`else
LPDDR4_itf #(.C(2), .D(16)) LPDDR4__0();
LPDDR4_itf #(.C(2), .D(16)) LPDDR4__1();
lpddr4_wrapper LPDDR4__MODEL_0(.LPDDR4(LPDDR4__0));
lpddr4_wrapper LPDDR4__MODEL_1(.LPDDR4(LPDDR4__1));
//`endif

assign USB3_0__ID = 0;
assign USB3_0__RXM =0;
assign USB3_0__RXP =0 ;
assign USB3_0__REFPADCLKM = 0;
assign USB3_0__REFPADCLKP = 0;
/*TOP AUTO_TEMPLATE (
    .PKG0            (1'b0),
    .PKG1            (1'b1),
    .PKG2            (1'b1),
    .RSTN            (rstn),
    .RSTN_OUT        (),
    .XTALI           (clk),
    .XTALO           (),
    .OSC             (clk),
    .CLKSEL          (1'b0),
    .JTAG_TCK        (1'b0),
    .JTAG_TMS        (1'b0),
    .JTAG_TDI        (1'b0),
    .JTAG_TDO        (),
    .JTAG_TRST       (1'b0),
    .GPIO\(.\)_\([0-9]*\) (GPIO\1[\2]),
    .LPDDR4_\([0-9]\)_\(.*\) (LPDDR4__\1.\2),
); */
TOP duv (/*AUTOINST*/
	 // Outputs
	 .USB3_0__TXM			(USB3_0__TXM),
	 .USB3_0__TXP			(USB3_0__TXP),
	 .LPDDR4_0_ADCT			(LPDDR4__0.ADCT),	 // Templated
	 .LPDDR4_0_CKE			(LPDDR4__0.CKE),	 // Templated
	 .LPDDR4_0_CS			(LPDDR4__0.CS),		 // Templated
	 .LPDDR4_0_ODT			(LPDDR4__0.ODT),	 // Templated
	 .LPDDR4_0_RESET		(LPDDR4__0.RESET),	 // Templated
	 .LPDDR4_1_ADCT			(LPDDR4__1.ADCT),	 // Templated
	 .LPDDR4_1_CKE			(LPDDR4__1.CKE),	 // Templated
	 .LPDDR4_1_CS			(LPDDR4__1.CS),		 // Templated
	 .LPDDR4_1_ODT			(LPDDR4__1.ODT),	 // Templated
	 .LPDDR4_1_RESET		(LPDDR4__1.RESET),	 // Templated
	 .RSTN_OUT			(),			 // Templated
	 .JTAG_TDO			(),			 // Templated
	 .XTALO				(),			 // Templated
	 // Inouts
	 .USB3_0__VBUS			(USB3_0__VBUS),
	 .USB3_0__DM			(USB3_0__DM),
	 .USB3_0__DP			(USB3_0__DP),
	 .USB3_0__RESREF		(USB3_0__RESREF),
	 .LPDDR4_0_CK			(LPDDR4__0.CK),		 // Templated
	 .LPDDR4_0_CKB			(LPDDR4__0.CKB),	 // Templated
	 .LPDDR4_0_DM_A			(LPDDR4__0.DM_A),	 // Templated
	 .LPDDR4_0_DM_B			(LPDDR4__0.DM_B),	 // Templated
	 .LPDDR4_0_DQ_A			(LPDDR4__0.DQ_A),	 // Templated
	 .LPDDR4_0_DQ_B			(LPDDR4__0.DQ_B),	 // Templated
	 .LPDDR4_0_NDQS_A		(LPDDR4__0.NDQS_A),	 // Templated
	 .LPDDR4_0_NDQS_B		(LPDDR4__0.NDQS_B),	 // Templated
	 .LPDDR4_0_PDQS_A		(LPDDR4__0.PDQS_A),	 // Templated
	 .LPDDR4_0_PDQS_B		(LPDDR4__0.PDQS_B),	 // Templated
	 .LPDDR4_0_ZQ			(LPDDR4__0.ZQ),		 // Templated
	 .LPDDR4_1_CK			(LPDDR4__1.CK),		 // Templated
	 .LPDDR4_1_CKB			(LPDDR4__1.CKB),	 // Templated
	 .LPDDR4_1_DM_A			(LPDDR4__1.DM_A),	 // Templated
	 .LPDDR4_1_DM_B			(LPDDR4__1.DM_B),	 // Templated
	 .LPDDR4_1_DQ_A			(LPDDR4__1.DQ_A),	 // Templated
	 .LPDDR4_1_DQ_B			(LPDDR4__1.DQ_B),	 // Templated
	 .LPDDR4_1_NDQS_A		(LPDDR4__1.NDQS_A),	 // Templated
	 .LPDDR4_1_NDQS_B		(LPDDR4__1.NDQS_B),	 // Templated
	 .LPDDR4_1_PDQS_A		(LPDDR4__1.PDQS_A),	 // Templated
	 .LPDDR4_1_PDQS_B		(LPDDR4__1.PDQS_B),	 // Templated
	 .LPDDR4_1_ZQ			(LPDDR4__1.ZQ),		 // Templated
	 .GPIOA_0			(GPIOA[0]),		 // Templated
	 .GPIOA_1			(GPIOA[1]),		 // Templated
	 .GPIOA_2			(GPIOA[2]),		 // Templated
	 .GPIOA_3			(GPIOA[3]),		 // Templated
	 .GPIOA_4			(GPIOA[4]),		 // Templated
	 .GPIOA_5			(GPIOA[5]),		 // Templated
	 .GPIOA_6			(GPIOA[6]),		 // Templated
	 .GPIOA_7			(GPIOA[7]),		 // Templated
	 .GPIOA_8			(GPIOA[8]),		 // Templated
	 .GPIOA_9			(GPIOA[9]),		 // Templated
	 .GPIOA_10			(GPIOA[10]),		 // Templated
	 .GPIOA_11			(GPIOA[11]),		 // Templated
	 .GPIOA_12			(GPIOA[12]),		 // Templated
	 .GPIOA_13			(GPIOA[13]),		 // Templated
	 .GPIOA_14			(GPIOA[14]),		 // Templated
	 .GPIOA_15			(GPIOA[15]),		 // Templated
	 .GPIOB_0			(GPIOB[0]),		 // Templated
	 .GPIOB_1			(GPIOB[1]),		 // Templated
	 .GPIOB_2			(GPIOB[2]),		 // Templated
	 .GPIOB_3			(GPIOB[3]),		 // Templated
	 .GPIOB_4			(GPIOB[4]),		 // Templated
	 .GPIOB_5			(GPIOB[5]),		 // Templated
	 .GPIOB_6			(GPIOB[6]),		 // Templated
	 .GPIOB_7			(GPIOB[7]),		 // Templated
	 .GPIOB_8			(GPIOB[8]),		 // Templated
	 .GPIOB_9			(GPIOB[9]),		 // Templated
	 .GPIOB_10			(GPIOB[10]),		 // Templated
	 .GPIOB_11			(GPIOB[11]),		 // Templated
	 .GPIOB_12			(GPIOB[12]),		 // Templated
	 .GPIOB_13			(GPIOB[13]),		 // Templated
	 .GPIOB_14			(GPIOB[14]),		 // Templated
	 .GPIOB_15			(GPIOB[15]),		 // Templated
	 .GPIOC_0			(GPIOC[0]),		 // Templated
	 .GPIOC_1			(GPIOC[1]),		 // Templated
	 .GPIOC_2			(GPIOC[2]),		 // Templated
	 .GPIOC_3			(GPIOC[3]),		 // Templated
	 .GPIOC_4			(GPIOC[4]),		 // Templated
	 .GPIOC_5			(GPIOC[5]),		 // Templated
	 .GPIOC_6			(GPIOC[6]),		 // Templated
	 .GPIOC_7			(GPIOC[7]),		 // Templated
	 .GPIOC_8			(GPIOC[8]),		 // Templated
	 .GPIOC_9			(GPIOC[9]),		 // Templated
	 .GPIOC_10			(GPIOC[10]),		 // Templated
	 .GPIOC_11			(GPIOC[11]),		 // Templated
	 .GPIOC_12			(GPIOC[12]),		 // Templated
	 .GPIOC_13			(GPIOC[13]),		 // Templated
	 .GPIOC_14			(GPIOC[14]),		 // Templated
	 .GPIOC_15			(GPIOC[15]),		 // Templated
	 .GPIOD_0			(GPIOD[0]),		 // Templated
	 .GPIOD_1			(GPIOD[1]),		 // Templated
	 .GPIOD_2			(GPIOD[2]),		 // Templated
	 .GPIOD_3			(GPIOD[3]),		 // Templated
	 .GPIOD_4			(GPIOD[4]),		 // Templated
	 .GPIOD_5			(GPIOD[5]),		 // Templated
	 .GPIOD_6			(GPIOD[6]),		 // Templated
	 .GPIOD_7			(GPIOD[7]),		 // Templated
	 .GPIOD_8			(GPIOD[8]),		 // Templated
	 .GPIOD_9			(GPIOD[9]),		 // Templated
	 .GPIOD_10			(GPIOD[10]),		 // Templated
	 .GPIOD_11			(GPIOD[11]),		 // Templated
	 .GPIOD_12			(GPIOD[12]),		 // Templated
	 .GPIOD_13			(GPIOD[13]),		 // Templated
	 .GPIOD_14			(GPIOD[14]),		 // Templated
	 .GPIOD_15			(GPIOD[15]),		 // Templated
	 .SDMMC_0			(SDMMC_0),
	 .SDMMC_1			(SDMMC_1),
	 .SDMMC_2			(SDMMC_2),
	 .SDMMC_3			(SDMMC_3),
	 .SDMMC_4			(SDMMC_4),
	 .SDMMC_5			(SDMMC_5),
	 .SDMMC_6			(SDMMC_6),
	 .SDMMC_7			(SDMMC_7),
	 .SDMMC_8			(SDMMC_8),
	 .SDMMC_9			(SDMMC_9),
	 .SDMMC_10			(SDMMC_10),
	 .SDMMC_11			(SDMMC_11),
	 .OTP_0_VPP			(OTP_0_VPP),
	 .OTP_0_VBG			(OTP_0_VBG),
	 .OTP_0_VTDO			(OTP_0_VTDO),
	 .OTP_0_VREFM			(OTP_0_VREFM),
	 .OTP_1_VPP			(OTP_1_VPP),
	 .OTP_1_VBG			(OTP_1_VBG),
	 .OTP_1_VTDO			(OTP_1_VTDO),
	 .OTP_1_VREFM			(OTP_1_VREFM),
	 // Inputs
	 .USB3_0__ID			(USB3_0__ID),
	 .USB3_0__RXM			(USB3_0__RXM),
	 .USB3_0__RXP			(USB3_0__RXP),
	 .USB3_0__REFPADCLKM		(USB3_0__REFPADCLKM),
	 .USB3_0__REFPADCLKP		(USB3_0__REFPADCLKP),
	 .RSTN				(rstn),			 // Templated
	 .PKG0				(0),			 // Templated
	 .PKG1				(1),			 // Templated
	 .PKG2				(1),			 // Templated
	 .JTAG_TCK			(1'b0),			 // Templated
	 .JTAG_TMS			(1'b0),			 // Templated
	 .JTAG_TDI			(1'b0),			 // Templated
	 .JTAG_TRST			(1'b0),			 // Templated
	 .CLKSEL			(1'b0),			 // Templated
	 .XTALI				(clk),			 // Templated
	 .OSC				(clk));			 // Templated

reg [1000:0] TESTFILE;
initial begin
    // hello program memory loading
    //$readmemh("./freedom-e-sdk/software/hello/release/hello.hex", tb_top.duv.uCORE.uSUBSYSTEM_PERI_0.uSRAM_0.uMEM.mem);
    if ($value$plusargs("testfile=%s", TESTFILE)) begin
        $readmemh(TESTFILE, tb_top.duv.CORE_0.CPU_SUBSYSTEM_0.ROM_0.CPU_ROM_0.Mem);
        $display("Loading Test file %s to ROM",TESTFILE);
    end
    else begin
        //$readmemh("./freedom-e-sdk/software/hello/release/hello.hex", tb_top.duv.CORE_0.CPU_SUBSYSTEM_0.ROM_CPU_0.CPU_ROM_0.Mem);
        $display("TESTFILE is not define. Did not load Test file to ROM");
    end
end

initial begin
    force tb_top.duv.CORE_0.CPU_SUBSYSTEM_0.CPU_0__RESET_VECTOR_0 = 36'h0_8000_0000;
    force tb_top.duv.CORE_0.CPU_SUBSYSTEM_0.CPU_0__RESET_VECTOR_1 = 36'h0_8000_0000;
    #1000 rstn = 0;
    #1000 rstn = 1;

    // For CI fail test
//    #10000 ;
//    force tb_top.duv.uCORE.uSUBSYSTEM_RISCV_0.uCOREIP_wrap_0.uCOREIP_0.Logic.testIndicator.status_regs_0 = 32'h0000_3333;
end

// Simulation finish condition
  bit verbose = |($test$plusargs("verbose"));
  bit printf_cond;
  assign printf_cond = verbose && rstn;

bind `INDICATOR
     TestFinisher #(.NCONCURRENT(1),
                    .REGBYTES(4))
     i_TF (.reset(reset),
           .clock(clock),
`ifndef VERILATOR
           .status_regs({>>{status_regs_0}}));
`else
           .status_regs('{status_regs_0}));

`endif

/*
initial begin
   $fsdbDumpvars("+all");
end
*/

/*
initial begin
    #2000000; $finish;
end
*/

`ifndef SYNTHESIS
SemiCPU #(
    .MBUS_ADDR_BASE(64'h8_0000_0000), .MBUS_ADDR_SIZE(64'h8_0000_0000),
    .SBUS_ADDR_BASE(64'h18_0000_0000), .SBUS_ADDR_SIZE(64'h8_0000_0000),
    .PBUS_ADDR_BASE(64'h0_8000_0000), .PBUS_ADDR_SIZE(64'h0_8000_0000)
) uSEMICPU_0(
    .CLK__SYS(clk),
    .RSTN__SYS(rstn)
);
`endif

endmodule
// Local Variables:
// verilog-library-directories:(".")
// verilog-auto-inst-param-value:t
// verilog-library-files:("../../../rtl.src/TOP/TOP.v")
// End:
// vim:tabstop=4:shiftwidth=4
