// SPDX-License-Identifier: Apache-2.0

`ifndef SYNTHESIS

program SemiCPU #( parameter
    MBUS_ADDR_BASE, MBUS_ADDR_SIZE,
    SBUS_ADDR_BASE, SBUS_ADDR_SIZE,
    PBUS_ADDR_BASE, PBUS_ADDR_SIZE
) (input CLK__SYS, input RSTN__SYS );   

  //--------------------------------------------------------------------------------------------
  //  DPI services
  //--------------------------------------------------------------------------------------------
  import "DPI-C" context task semiCPUStartup(input string application_path);
  import "DPI-C" context task semiCPUIRQHandler(input int interrupt_number);

  export "DPI-C" task semiCPUPutc;
  export "DPI-C" task simulationTick;
  export "DPI-C" task semiCPUEnterCritialSection;
  export "DPI-C" task semiCPULeaveCritialSection;
  export "DPI-C" task semiCPUWriteReq;
  export "DPI-C" task semiCPUWriteFlush;
  export "DPI-C" task semiCPUReadReq;
  export "DPI-C" task semiCPURead;

  //--------------------------------------------------------------------------------------------
  //  Prevents re-entry of service
  //--------------------------------------------------------------------------------------------
  semaphore  semaphore_service_blocker  ;
  semaphore  semaphore_interrupt_blocker;

  vmaster_driver mbus_driver;
  vmaster_driver sbus_driver;
  vmaster_driver pbus_driver;  
  interrupt_vslave_driver interrupt_drivers[$];

  initial begin    
    #1;
    semaphore_service_blocker = new(1);
    mbus_driver = semifive_hookup_config#(vmaster_driver,string)::get("SemiCPU_MBUS");
    sbus_driver = semifive_hookup_config#(vmaster_driver,string)::get("SemiCPU_SBUS");
    pbus_driver = semifive_hookup_config#(vmaster_driver,string)::get("SemiCPU_PBUS");

    if ($test$plusargs("ENABLE_SEMICPU")) begin
      reg [1000:0] TESTFILE;
      $display("####### SemiCPU is enabled #######");
      mbus_driver.hookup(1);
      sbus_driver.hookup(1);
      pbus_driver.hookup(1);
      @(posedge CLK__SYS);
      @(posedge RSTN__SYS);
      repeat(100) @(posedge CLK__SYS);
      $value$plusargs("testfile=%s",TESTFILE);
      semiCPUStartup(TESTFILE);
      $finish;
    end else begin
      $display("####### RISC-V is enabled #######");
      mbus_driver.hookup(0);
      sbus_driver.hookup(0);
      pbus_driver.hookup(0);
      forever @(posedge CLK__SYS);
    end
  end

  initial begin
    #1;
    semaphore_interrupt_blocker = new(1);
    $display( "The design has %d interrupts", semifive_hookup_config#(interrupt_vslave_driver,int)::size() ) ;
    for( int i=0; i<semifive_hookup_config#(interrupt_vslave_driver,int)::size(); i++ ) begin
        interrupt_drivers.push_back( semifive_hookup_config#(interrupt_vslave_driver,int)::get(i) );
    end    

    if ($test$plusargs("ENABLE_SEMICPU")) begin
      for( int i=0; i<interrupt_drivers.size(); i++ ) interrupt_drivers[i].hookup(1);
      @(posedge CLK__SYS);
      @(posedge RSTN__SYS);
      repeat(100) @(posedge CLK__SYS);          
      forever begin
          @(posedge CLK__SYS);
          for( int i=0; i<interrupt_drivers.size(); i++ ) begin
              if( interrupt_drivers[i].get_irq() ) begin
                  semaphore_interrupt_blocker.get(1);
                  semiCPUIRQHandler(i);
                  semaphore_interrupt_blocker.put(1);
              end
          end
      end
    end else begin
      for( int i=0; i<interrupt_drivers.size(); i++ ) interrupt_drivers[i].hookup(0);
      forever @(posedge CLK__SYS);
    end
  end

  function vmaster_driver systemAddressDecoder(input longint unsigned ADDR);
    if     ( ADDR >= MBUS_ADDR_BASE && ADDR < MBUS_ADDR_BASE + MBUS_ADDR_SIZE ) return mbus_driver;
    else if( ADDR >= SBUS_ADDR_BASE && ADDR < SBUS_ADDR_BASE + SBUS_ADDR_SIZE ) return sbus_driver;
    else if( ADDR >= PBUS_ADDR_BASE && ADDR < PBUS_ADDR_BASE + PBUS_ADDR_SIZE ) return pbus_driver;
    else begin
      $display( "Invalid address : %x", ADDR );
      $display( "Simulation Time : %d", $time );
      $finish;
    end
  endfunction

  //--------------------------------------------------------------------------------------------
  task semiCPUPutc( input int data ); 
    $write( "%c", data );
  endtask
  //--------------------------------------------------------------------------------------------
  task simulationTick; 
    @(posedge CLK__SYS);
  endtask  
  //--------------------------------------------------------------------------------------------
  task	semiCPUEnterCritialSection;
    semaphore_service_blocker.get(1);
  endtask
  //--------------------------------------------------------------------------------------------
  task	semiCPULeaveCritialSection;
    semaphore_service_blocker.put(1);
  endtask
  //--------------------------------------------------------------------------------------------
  task semiCPUWriteReq(input longint unsigned deviceAddress, input int unsigned data);
    vmaster_driver driver;
    driver = systemAddressDecoder(deviceAddress);
    driver.write_req(deviceAddress,data[7:0]);    
  endtask
  
  task semiCPUWriteFlush(input longint unsigned deviceAddress);
    vmaster_driver driver;
    driver = systemAddressDecoder(deviceAddress);
    driver.write_flush();
  endtask
  
  task semiCPUReadReq(input longint unsigned deviceAddress, input int unsigned len);
    vmaster_driver driver;
    logic[31:0] offset;    
    driver = systemAddressDecoder(deviceAddress);
    for(offset =0;offset<len;offset++) begin
      driver.read_req(deviceAddress+offset);
    end    
    driver.read_flush();
  endtask
  
  task semiCPURead(input longint unsigned deviceAddress, output int unsigned data);
    vmaster_driver driver;
    driver = systemAddressDecoder(deviceAddress);
    data = driver.get_read_data();
  endtask

endprogram
`endif

