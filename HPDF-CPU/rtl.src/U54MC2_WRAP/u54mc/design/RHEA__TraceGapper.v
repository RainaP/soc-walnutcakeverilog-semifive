//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TraceGapper(
  output        io_streamIn_ready,
  input         io_streamIn_valid,
  input  [31:0] io_streamIn_bits_data,
  input         io_streamOut_ready,
  output        io_streamOut_valid,
  output [31:0] io_streamOut_bits_data
);
  wire [63:0] _GEN_11 = {{32'd0}, io_streamIn_bits_data}; // @[TraceGapper.scala 23:28 TraceGapper.scala 60:28 TraceGapper.scala 75:18]
  assign io_streamIn_ready = io_streamOut_ready; // @[TraceGapper.scala 23:28 TraceGapper.scala 39:23 TraceGapper.scala 75:18]
  assign io_streamOut_valid = io_streamIn_valid; // @[TraceGapper.scala 23:28 TraceGapper.scala 59:24 TraceGapper.scala 75:18]
  assign io_streamOut_bits_data = _GEN_11[31:0];
endmodule
