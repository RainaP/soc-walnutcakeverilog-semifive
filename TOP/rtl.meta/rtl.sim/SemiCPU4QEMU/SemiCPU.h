// SPDX-License-Identifier: Apache-2.0

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

void simulationTick(void);

///////////////////////////////////////////////////////////////////////////////
// AXI bus driver
///////////////////////////////////////////////////////////////////////////////
//read from device
unsigned char      readDevice1B(unsigned long long deviceAddress);  //read 1 Bytes from device
unsigned short     readDevice2B(unsigned long long deviceAddress);  //read 2 Bytes from device
unsigned int       readDevice4B(unsigned long long deviceAddress);  //read 4 Bytes from device
unsigned long long readDevice8B(unsigned long long deviceAddress);  //read 8 Bytes from device
void               readDevice  (void* buffer, unsigned long long deviceAddress, unsigned int len);

//write to device
void writeDevice1B(unsigned long long deviceAddress, unsigned char      data); //write 1 Bytes to device
void writeDevice2B(unsigned long long deviceAddress, unsigned short     data); //write 2 Bytes to device
void writeDevice4B(unsigned long long deviceAddress, unsigned int       data); //write 4 Bytes to device
void writeDevice8B(unsigned long long deviceAddress, unsigned long long data); //write 8 Bytes to device
void writeDevice  (const void* buffer, unsigned long long deviceAddress, unsigned int len);

///////////////////////////////////////////////////////////////////////////////
// Backdoor service
///////////////////////////////////////////////////////////////////////////////
//void backdoorReadDevice  (void* buffer, unsigned long long deviceAddress, unsigned int len);
//void backdoorWriteDeviceBackdoor (const void* buffer, unsigned long long deviceAddress, unsigned int len);

// void* backdoorOpenFile( const char* filename,  );
// void saveToFile   (unsigned long long deviceAddress, const char* fileName, unsigned int offset, unsigned int len);
// void loadFromFile (unsigned long long deviceAddress, const char* fileName, unsigned int offset, unsigned int len);

#ifdef __cplusplus
}
#endif




// #include <stdio.h>

// #ifdef VCSMX
// #define USE_DPI     //enable DPI interface if defined
// #else
// #define NO_UART_AVAILABLE
// #endif

//IP Control Registers

/////////////////////////////////////////////////////////////////////////////////
//// Macro
//#define clipping(x,a,b) ( ((x)<(a)) ? (a) : ( ((x)>(b)) ? (b) : (x) ) )
//
//#ifndef __func__
//#define __func__ __FUNCTION__
//#endif
//
///////////////////////////////////////////////////////////////////////////////
// Type Definition

////Packet on TileLink bus
//typedef struct{
//    enum{WR, RD}       opcode;  //bus operation
//    int                portId;  //port number to execute the transaction
//    unsigned long long addr;    //64-bit of destination address
//    int                wordLen; //word length of payload (32-bit word)
//    unsigned int*      payload; //data to access, array of unsigned int
//} t_metalTlPacket;
//
//namespace METAL_CPU_MAILBOX{
//enum {
//    //NOTE: to simplify, make bit-0 always '1'
//    //NOTE: only lower 16-bit is available for message
//    //      since upper 16-bit is used for 
//    MSG_RISC_V_READY       = 0x0001,   //RISC-V is ready to handle interrups
//    MSG_METAL_CPU_INTR_CLR = 0x0011,   //Interrupt clear for MetalCPU
//    MSG_INTR_FORWARDING    = 0x0101,   //Forwarding interrupt to MetalCPU
//    MSG_CARRY_PAYLOAD      = 0x0111,   //carrying payload
//}; //enum
//}; //METAL_CPU_MAILBOX
//
///////////////////////////////////////////////////////////////////////////////
// Function Declaration


////Register Read
//unsigned char      regRd1B(unsigned long long addr);  //read 1 Bytes from register
//unsigned short     regRd2B(unsigned long long addr);  //read 2 Bytes from register
//unsigned int       regRd4B(unsigned long long addr);  //read 4 Bytes from register
//unsigned long long regRd8B(unsigned long long addr);  //read 8 Bytes from register
//
////Register Write
//void regWr1B(unsigned long long addr, unsigned char      data); //write 1 Bytes to register
//void regWr2B(unsigned long long addr, unsigned short     data); //write 2 Bytes to register
//void regWr4B(unsigned long long addr, unsigned int       data); //write 4 Bytes to register
//void regWr8B(unsigned long long addr, unsigned long long data); //write 8 Bytes to register
//
////Backdoor Read on DDR
//unsigned char      backRd1B(unsigned long long addr);  //read 1 Bytes from DDR
//unsigned short     backRd2B(unsigned long long addr);  //read 2 Bytes from DDR
//unsigned int       backRd4B(unsigned long long addr);  //read 4 Bytes from DDR
//unsigned long long backRd8B(unsigned long long addr);  //read 8 Bytes from DDR
//
////Backdoor Write on DDR
//void backWr1B(unsigned long long addr, unsigned char      data); //write 1 Bytes to DDR
//void backWr2B(unsigned long long addr, unsigned short     data); //write 2 Bytes to DDR
//void backWr4B(unsigned long long addr, unsigned int       data); //write 4 Bytes to DDR
//void backWr8B(unsigned long long addr, unsigned long long data); //write 8 Bytes to DDR
//
////DMA access
//int dmaWrBulk(unsigned long long dest, unsigned long long src, unsigned int len); //transfer data from system to device using PCIe DMA
//int dmaRdBulk(unsigned long long dest, unsigned long long src, unsigned int len); //transfer data from device to system using PCIe DMA
//
//void simPrintf(const char *format,...);  //execute print on both of ncsim.log and STDOUT
//
/////////////////////////////////////////////////////////////////////////////////
////Function Export for Host program
//
//#ifndef USE_DPI
//typedef struct{
//    int version;              //version of tlMetalCPU
//    struct{
//      int numPort;            //number of port
//      int dataWidth;          //bit width of tlMetalCPU on data bus
//      int maxBurstLenWr;      //maximum burst length tlMetalCPU can issue for writing
//      int maxBurstLenRd;      //maximum burst length tlMetalCPU can issue for reading
//    } bus;
//} t_metalCpuCap;
//
//typedef struct{
//    unsigned long long addr;        //upper 32-bit of destination Byte address; ignored if WIDTH_ADDR <= 32
//    unsigned int       wordLength;  //length in 32-bit word
//} t_metalTlAddr;
//
//void tlWrPost     (unsigned int payload);                       //post write data for write transaction
//void tlWrite      (const t_metalTlAddr *header, int portNum=0); //execute the posted  write transaction
//
//void tlRead       (const t_metalTlAddr *header, int portNum=0); //execute a read transaction
//void tlRdFetch    (unsigned int *payload);                      //transfer read data
//
//void intrEnable   ();                   //enable  for incoming interrupt
//void intrDisable  ();                   //disable for incoming interrupt
//void intrClear    ();                   //clear   for incoming interrupt
//
//int  intrStatus   ();                   //return incoming interrupt status
//int  getIntrEn    ();                   //return incoming interrupt enable bit
//
//void intrAssert   ();                   //   assert outgoing interrupt
//void intrDeassert ();                   //de-assert outgoing interrupt
//int  intrPeek     ();                   //return outgoing interrupt status
//
//int  getVer       ();                   //return version number
//void getCapability(t_metalCpuCap *cap); //return capability of metalCPU 
//
//void waitRst      ();                   //wait until reset released
//void waitClk      (int cycle);          //wait cycles
//
//void cpuExit      ();                   //terminate simulation by CPU
//
//const char* getCfgName();               //return filename of test configuration
//const char* getCfgFile();               //return test configuration filename
//#endif//USE_DPI
//
//#ifdef __cplusplus
// }
//#endif
//
//