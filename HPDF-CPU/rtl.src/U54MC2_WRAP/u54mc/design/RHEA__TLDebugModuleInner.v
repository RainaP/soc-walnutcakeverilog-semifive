//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLDebugModuleInner(
  input         rf_reset,
  input         clock,
  input         reset,
  input         auto_sb2tlOpt_out_a_ready,
  output        auto_sb2tlOpt_out_a_valid,
  output [2:0]  auto_sb2tlOpt_out_a_bits_opcode,
  output [3:0]  auto_sb2tlOpt_out_a_bits_size,
  output [36:0] auto_sb2tlOpt_out_a_bits_address,
  output [7:0]  auto_sb2tlOpt_out_a_bits_data,
  output        auto_sb2tlOpt_out_d_ready,
  input         auto_sb2tlOpt_out_d_valid,
  input         auto_sb2tlOpt_out_d_bits_denied,
  input  [7:0]  auto_sb2tlOpt_out_d_bits_data,
  input         auto_sb2tlOpt_out_d_bits_corrupt,
  output        auto_tl_in_a_ready,
  input         auto_tl_in_a_valid,
  input  [2:0]  auto_tl_in_a_bits_opcode,
  input  [2:0]  auto_tl_in_a_bits_param,
  input  [1:0]  auto_tl_in_a_bits_size,
  input  [10:0] auto_tl_in_a_bits_source,
  input  [11:0] auto_tl_in_a_bits_address,
  input  [7:0]  auto_tl_in_a_bits_mask,
  input  [63:0] auto_tl_in_a_bits_data,
  input         auto_tl_in_a_bits_corrupt,
  input         auto_tl_in_d_ready,
  output        auto_tl_in_d_valid,
  output [2:0]  auto_tl_in_d_bits_opcode,
  output [1:0]  auto_tl_in_d_bits_size,
  output [10:0] auto_tl_in_d_bits_source,
  output [63:0] auto_tl_in_d_bits_data,
  output        auto_dmi_in_a_ready,
  input         auto_dmi_in_a_valid,
  input  [2:0]  auto_dmi_in_a_bits_opcode,
  input  [2:0]  auto_dmi_in_a_bits_param,
  input  [1:0]  auto_dmi_in_a_bits_size,
  input         auto_dmi_in_a_bits_source,
  input  [8:0]  auto_dmi_in_a_bits_address,
  input  [3:0]  auto_dmi_in_a_bits_mask,
  input  [31:0] auto_dmi_in_a_bits_data,
  input         auto_dmi_in_a_bits_corrupt,
  input         auto_dmi_in_d_ready,
  output        auto_dmi_in_d_valid,
  output [2:0]  auto_dmi_in_d_bits_opcode,
  output [1:0]  auto_dmi_in_d_bits_size,
  output        auto_dmi_in_d_bits_source,
  output [31:0] auto_dmi_in_d_bits_data,
  input         io_dmactive,
  output        io_innerCtrl_ready,
  input         io_innerCtrl_valid,
  input         io_innerCtrl_bits_resumereq,
  input  [9:0]  io_innerCtrl_bits_hartsel,
  input         io_innerCtrl_bits_ackhavereset,
  input         io_innerCtrl_bits_hasel,
  input         io_innerCtrl_bits_hamask_0,
  input         io_innerCtrl_bits_hamask_1,
  input         io_innerCtrl_bits_hrmask_0,
  input         io_innerCtrl_bits_hrmask_1,
  output        io_hgDebugInt_0,
  output        io_hgDebugInt_1,
  input         io_hartIsInReset_0,
  input         io_hartIsInReset_1,
  input         io_tl_clock,
  input         io_tl_reset
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [31:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [31:0] _RAND_59;
  reg [31:0] _RAND_60;
  reg [31:0] _RAND_61;
  reg [31:0] _RAND_62;
  reg [31:0] _RAND_63;
  reg [31:0] _RAND_64;
  reg [31:0] _RAND_65;
  reg [31:0] _RAND_66;
  reg [31:0] _RAND_67;
  reg [31:0] _RAND_68;
  reg [31:0] _RAND_69;
  reg [31:0] _RAND_70;
  reg [31:0] _RAND_71;
  reg [31:0] _RAND_72;
  reg [31:0] _RAND_73;
  reg [31:0] _RAND_74;
  reg [31:0] _RAND_75;
  reg [31:0] _RAND_76;
  reg [31:0] _RAND_77;
  reg [31:0] _RAND_78;
  reg [31:0] _RAND_79;
  reg [31:0] _RAND_80;
  reg [31:0] _RAND_81;
  reg [31:0] _RAND_82;
  reg [31:0] _RAND_83;
  reg [31:0] _RAND_84;
  reg [31:0] _RAND_85;
  reg [31:0] _RAND_86;
  reg [31:0] _RAND_87;
  reg [31:0] _RAND_88;
  reg [31:0] _RAND_89;
  reg [31:0] _RAND_90;
  reg [31:0] _RAND_91;
  reg [31:0] _RAND_92;
  reg [31:0] _RAND_93;
  reg [31:0] _RAND_94;
  reg [31:0] _RAND_95;
  reg [31:0] _RAND_96;
  reg [31:0] _RAND_97;
  reg [31:0] _RAND_98;
  reg [31:0] _RAND_99;
  reg [31:0] _RAND_100;
  reg [31:0] _RAND_101;
  reg [31:0] _RAND_102;
  reg [31:0] _RAND_103;
  reg [31:0] _RAND_104;
  reg [31:0] _RAND_105;
  reg [31:0] _RAND_106;
  reg [31:0] _RAND_107;
  reg [31:0] _RAND_108;
  reg [31:0] _RAND_109;
  reg [31:0] _RAND_110;
  reg [31:0] _RAND_111;
  reg [31:0] _RAND_112;
  reg [31:0] _RAND_113;
  reg [31:0] _RAND_114;
  reg [31:0] _RAND_115;
  reg [31:0] _RAND_116;
`endif // RANDOMIZE_REG_INIT
  wire  sb2tlOpt_clock; // @[Debug.scala 740:52]
  wire  sb2tlOpt_reset; // @[Debug.scala 740:52]
  wire  sb2tlOpt_auto_out_a_ready; // @[Debug.scala 740:52]
  wire  sb2tlOpt_auto_out_a_valid; // @[Debug.scala 740:52]
  wire [2:0] sb2tlOpt_auto_out_a_bits_opcode; // @[Debug.scala 740:52]
  wire [3:0] sb2tlOpt_auto_out_a_bits_size; // @[Debug.scala 740:52]
  wire [36:0] sb2tlOpt_auto_out_a_bits_address; // @[Debug.scala 740:52]
  wire [7:0] sb2tlOpt_auto_out_a_bits_data; // @[Debug.scala 740:52]
  wire  sb2tlOpt_auto_out_d_ready; // @[Debug.scala 740:52]
  wire  sb2tlOpt_auto_out_d_valid; // @[Debug.scala 740:52]
  wire  sb2tlOpt_auto_out_d_bits_denied; // @[Debug.scala 740:52]
  wire [7:0] sb2tlOpt_auto_out_d_bits_data; // @[Debug.scala 740:52]
  wire  sb2tlOpt_auto_out_d_bits_corrupt; // @[Debug.scala 740:52]
  wire  sb2tlOpt_io_rdEn; // @[Debug.scala 740:52]
  wire  sb2tlOpt_io_wrEn; // @[Debug.scala 740:52]
  wire [127:0] sb2tlOpt_io_addrIn; // @[Debug.scala 740:52]
  wire [127:0] sb2tlOpt_io_dataIn; // @[Debug.scala 740:52]
  wire [2:0] sb2tlOpt_io_sizeIn; // @[Debug.scala 740:52]
  wire  sb2tlOpt_io_rdLegal; // @[Debug.scala 740:52]
  wire  sb2tlOpt_io_wrLegal; // @[Debug.scala 740:52]
  wire  sb2tlOpt_io_rdDone; // @[Debug.scala 740:52]
  wire  sb2tlOpt_io_wrDone; // @[Debug.scala 740:52]
  wire  sb2tlOpt_io_respError; // @[Debug.scala 740:52]
  wire [7:0] sb2tlOpt_io_dataOut; // @[Debug.scala 740:52]
  wire  sb2tlOpt_io_rdLoad_0; // @[Debug.scala 740:52]
  wire  sb2tlOpt_io_rdLoad_1; // @[Debug.scala 740:52]
  wire  sb2tlOpt_io_rdLoad_2; // @[Debug.scala 740:52]
  wire  sb2tlOpt_io_rdLoad_3; // @[Debug.scala 740:52]
  wire  sb2tlOpt_io_rdLoad_4; // @[Debug.scala 740:52]
  wire  sb2tlOpt_io_rdLoad_5; // @[Debug.scala 740:52]
  wire  sb2tlOpt_io_rdLoad_6; // @[Debug.scala 740:52]
  wire  sb2tlOpt_io_rdLoad_7; // @[Debug.scala 740:52]
  wire [2:0] sb2tlOpt_io_sbStateOut; // @[Debug.scala 740:52]
  wire  sb2tlOpt_rf_reset; // @[Debug.scala 740:52]
  wire  hartIsInResetSync_0_debug_hartReset_0_clock; // @[ShiftReg.scala 45:23]
  wire  hartIsInResetSync_0_debug_hartReset_0_reset; // @[ShiftReg.scala 45:23]
  wire  hartIsInResetSync_0_debug_hartReset_0_io_d; // @[ShiftReg.scala 45:23]
  wire  hartIsInResetSync_0_debug_hartReset_0_io_q; // @[ShiftReg.scala 45:23]
  wire  hartIsInResetSync_1_debug_hartReset_1_clock; // @[ShiftReg.scala 45:23]
  wire  hartIsInResetSync_1_debug_hartReset_1_reset; // @[ShiftReg.scala 45:23]
  wire  hartIsInResetSync_1_debug_hartReset_1_io_d; // @[ShiftReg.scala 45:23]
  wire  hartIsInResetSync_1_debug_hartReset_1_io_q; // @[ShiftReg.scala 45:23]
  reg [1:0] haltedBitRegs; // @[Debug.scala 801:31]
  reg [1:0] resumeReqRegs; // @[Debug.scala 802:31]
  reg [1:0] haveResetBitRegs; // @[Debug.scala 803:31]
  reg [1:0] selectedHartReg; // @[Debug.scala 835:30]
  wire  _T = ~io_dmactive; // @[Debug.scala 840:13]
  wire  _T_1 = io_innerCtrl_ready & io_innerCtrl_valid; // @[Decoupled.scala 40:37]
  wire [9:0] _GEN_0 = _T_1 ? io_innerCtrl_bits_hartsel : {{8'd0}, selectedHartReg}; // @[Debug.scala 842:39 Debug.scala 843:25 Debug.scala 835:30]
  wire [9:0] _GEN_1 = _T ? 10'h0 : _GEN_0; // @[Debug.scala 840:27 Debug.scala 841:25]
  reg  hamaskReg_0; // @[Debug.scala 849:26]
  reg  hamaskReg_1; // @[Debug.scala 849:26]
  wire  _T_6_0 = io_innerCtrl_bits_hasel & io_innerCtrl_bits_hamask_0; // @[Debug.scala 853:25]
  wire  _T_6_1 = io_innerCtrl_bits_hasel & io_innerCtrl_bits_hamask_1; // @[Debug.scala 853:25]
  wire  _GEN_6 = ~selectedHartReg[0] | hamaskReg_0; // @[Debug.scala 859:35 Debug.scala 859:35 Debug.scala 855:18]
  wire  _GEN_7 = selectedHartReg[0] | hamaskReg_1; // @[Debug.scala 859:35 Debug.scala 859:35 Debug.scala 855:18]
  wire  hamaskFull_0 = selectedHartReg < 2'h2 ? _GEN_6 : hamaskReg_0; // @[Debug.scala 858:44 Debug.scala 855:18]
  wire  hamaskFull_1 = selectedHartReg < 2'h2 ? _GEN_7 : hamaskReg_1; // @[Debug.scala 858:44 Debug.scala 855:18]
  wire  hamaskWrSel_0 = io_innerCtrl_bits_hartsel == 10'h0 | _T_6_0; // @[Debug.scala 868:78]
  wire  hamaskWrSel_1 = io_innerCtrl_bits_hartsel == 10'h1 | _T_6_1; // @[Debug.scala 868:78]
  reg  hrmaskReg_0; // @[Debug.scala 880:29]
  reg  hrmaskReg_1; // @[Debug.scala 880:29]
  reg  hrDebugIntReg_0; // @[Debug.scala 894:34]
  reg  hrDebugIntReg_1; // @[Debug.scala 894:34]
  wire  _T_19 = ~haltedBitRegs[0]; // @[package.scala 70:38]
  wire  _T_20 = ~haltedBitRegs[1]; // @[package.scala 70:38]
  wire  _T_21 = hrDebugIntReg_0 & _T_19; // @[package.scala 65:72]
  wire  _T_22 = hrDebugIntReg_1 & _T_20; // @[package.scala 65:72]
  wire  hartIsInResetSync_0 = hartIsInResetSync_0_debug_hartReset_0_io_q; // @[ShiftReg.scala 48:24 ShiftReg.scala 48:24]
  wire  _T_23 = hartIsInResetSync_0 | _T_21; // @[package.scala 66:75]
  wire  hartIsInResetSync_1 = hartIsInResetSync_1_debug_hartReset_1_io_q; // @[ShiftReg.scala 48:24 ShiftReg.scala 48:24]
  wire  _T_24 = hartIsInResetSync_1 | _T_22; // @[package.scala 66:75]
  wire  _T_25 = hrmaskReg_0 & _T_23; // @[package.scala 65:72]
  wire  _T_26 = hrmaskReg_1 & _T_24; // @[package.scala 65:72]
  wire  resumereq = _T_1 & io_innerCtrl_bits_resumereq; // @[Debug.scala 916:41]
  wire  DMSTATUSRdData_anynonexistent = selectedHartReg >= 2'h2; // @[Debug.scala 921:57]
  wire  DMSTATUSRdData_allnonexistent = DMSTATUSRdData_anynonexistent & ~(hamaskFull_0 | hamaskFull_1); // @[Debug.scala 923:75]
  wire  _DMSTATUSRdData_anyhalted_T_6 = haltedBitRegs[0] & hamaskFull_0; // @[package.scala 65:72]
  wire  _DMSTATUSRdData_anyhalted_T_7 = haltedBitRegs[1] & hamaskFull_1; // @[package.scala 65:72]
  wire  _DMSTATUSRdData_anyrunning_T_8 = _T_19 & hamaskFull_0; // @[package.scala 65:72]
  wire  _DMSTATUSRdData_anyrunning_T_9 = _T_20 & hamaskFull_1; // @[package.scala 65:72]
  wire  _DMSTATUSRdData_anyhavereset_T_2 = haveResetBitRegs[0] & hamaskFull_0; // @[package.scala 65:72]
  wire  _DMSTATUSRdData_anyhavereset_T_3 = haveResetBitRegs[1] & hamaskFull_1; // @[package.scala 65:72]
  wire [1:0] _resumeAcks_T = ~resumeReqRegs; // @[Debug.scala 1301:24]
  wire [1:0] _resumeAcks_T_1 = {hamaskWrSel_1,hamaskWrSel_0}; // @[Debug.scala 1301:55]
  wire [1:0] _resumeAcks_T_2 = ~_resumeAcks_T_1; // @[Debug.scala 1301:41]
  wire [1:0] _resumeAcks_T_3 = _resumeAcks_T & _resumeAcks_T_2; // @[Debug.scala 1301:39]
  wire [1:0] resumeAcks = resumereq ? _resumeAcks_T_3 : _resumeAcks_T; // @[Debug.scala 1300:24 Debug.scala 1301:20 Debug.scala 1303:20]
  wire  _DMSTATUSRdData_anyresumeack_T_2 = resumeAcks[0] & hamaskFull_0; // @[package.scala 65:72]
  wire  _DMSTATUSRdData_anyresumeack_T_3 = resumeAcks[1] & hamaskFull_1; // @[package.scala 65:72]
  wire  _DMSTATUSRdData_allunavail_T = ~hamaskFull_0; // @[package.scala 70:38]
  wire  _DMSTATUSRdData_allunavail_T_1 = ~hamaskFull_1; // @[package.scala 70:38]
  wire  _DMSTATUSRdData_allhalted_T_8 = haltedBitRegs[0] | _DMSTATUSRdData_allunavail_T; // @[package.scala 66:75]
  wire  _DMSTATUSRdData_allhalted_T_9 = haltedBitRegs[1] | _DMSTATUSRdData_allunavail_T_1; // @[package.scala 66:75]
  wire  _DMSTATUSRdData_allrunning_T_10 = _T_19 | _DMSTATUSRdData_allunavail_T; // @[package.scala 66:75]
  wire  _DMSTATUSRdData_allrunning_T_11 = _T_20 | _DMSTATUSRdData_allunavail_T_1; // @[package.scala 66:75]
  wire  _DMSTATUSRdData_allhavereset_T_4 = haveResetBitRegs[0] | _DMSTATUSRdData_allunavail_T; // @[package.scala 66:75]
  wire  _DMSTATUSRdData_allhavereset_T_5 = haveResetBitRegs[1] | _DMSTATUSRdData_allunavail_T_1; // @[package.scala 66:75]
  wire  _DMSTATUSRdData_allresumeack_T_4 = resumeAcks[0] | _DMSTATUSRdData_allunavail_T; // @[package.scala 66:75]
  wire  _DMSTATUSRdData_allresumeack_T_5 = resumeAcks[1] | _DMSTATUSRdData_allunavail_T_1; // @[package.scala 66:75]
  wire  _GEN_16 = ~DMSTATUSRdData_anynonexistent & (_DMSTATUSRdData_allunavail_T & _DMSTATUSRdData_allunavail_T_1); // @[Debug.scala 931:47 Debug.scala 932:39]
  wire  _GEN_17 = ~DMSTATUSRdData_anynonexistent & (_DMSTATUSRdData_allhalted_T_8 & _DMSTATUSRdData_allhalted_T_9); // @[Debug.scala 931:47 Debug.scala 933:39]
  wire  _GEN_18 = ~DMSTATUSRdData_anynonexistent & (_DMSTATUSRdData_allrunning_T_10 & _DMSTATUSRdData_allrunning_T_11); // @[Debug.scala 931:47 Debug.scala 934:39]
  wire  _GEN_19 = ~DMSTATUSRdData_anynonexistent & (_DMSTATUSRdData_allhavereset_T_4 & _DMSTATUSRdData_allhavereset_T_5)
    ; // @[Debug.scala 931:47 Debug.scala 935:39]
  wire  _GEN_20 = ~DMSTATUSRdData_anynonexistent & (_DMSTATUSRdData_allresumeack_T_4 & _DMSTATUSRdData_allresumeack_T_5)
    ; // @[Debug.scala 931:47 Debug.scala 936:39]
  wire  DMSTATUSRdData_anyhalted = ~DMSTATUSRdData_allnonexistent & (_DMSTATUSRdData_anyhalted_T_6 |
    _DMSTATUSRdData_anyhalted_T_7); // @[Debug.scala 925:45 Debug.scala 927:37]
  wire  DMSTATUSRdData_anyrunning = ~DMSTATUSRdData_allnonexistent & (_DMSTATUSRdData_anyrunning_T_8 |
    _DMSTATUSRdData_anyrunning_T_9); // @[Debug.scala 925:45 Debug.scala 928:37]
  wire  DMSTATUSRdData_anyhavereset = ~DMSTATUSRdData_allnonexistent & (_DMSTATUSRdData_anyhavereset_T_2 |
    _DMSTATUSRdData_anyhavereset_T_3); // @[Debug.scala 925:45 Debug.scala 929:37]
  wire  DMSTATUSRdData_anyresumeack = ~DMSTATUSRdData_allnonexistent & (_DMSTATUSRdData_anyresumeack_T_2 |
    _DMSTATUSRdData_anyresumeack_T_3); // @[Debug.scala 925:45 Debug.scala 930:37]
  wire  DMSTATUSRdData_allunavail = ~DMSTATUSRdData_allnonexistent & _GEN_16; // @[Debug.scala 925:45]
  wire  DMSTATUSRdData_allhalted = ~DMSTATUSRdData_allnonexistent & _GEN_17; // @[Debug.scala 925:45]
  wire  DMSTATUSRdData_allrunning = ~DMSTATUSRdData_allnonexistent & _GEN_18; // @[Debug.scala 925:45]
  wire  DMSTATUSRdData_allhavereset = ~DMSTATUSRdData_allnonexistent & _GEN_19; // @[Debug.scala 925:45]
  wire  DMSTATUSRdData_allresumeack = ~DMSTATUSRdData_allnonexistent & _GEN_20; // @[Debug.scala 925:45]
  wire [1:0] _haveResetBitRegs_T_2 = haveResetBitRegs & _resumeAcks_T_2; // @[Debug.scala 949:47]
  wire [1:0] _haveResetBitRegs_T_3 = {hartIsInResetSync_1,hartIsInResetSync_0}; // @[Debug.scala 949:94]
  wire [1:0] _haveResetBitRegs_T_4 = _haveResetBitRegs_T_2 | _haveResetBitRegs_T_3; // @[Debug.scala 949:74]
  wire [1:0] _haveResetBitRegs_T_6 = haveResetBitRegs | _haveResetBitRegs_T_3; // @[Debug.scala 951:46]
  reg  grouptype; // @[Debug.scala 967:26]
  wire  in_bits_read = auto_dmi_in_a_bits_opcode == 3'h4; // @[Debug.scala 721:31]
  wire [6:0] in_bits_index = auto_dmi_in_a_bits_address[8:2]; // @[Edges.scala 191:34]
  wire  out_iindex_hi_hi_hi = in_bits_index[5]; // @[Debug.scala 721:31]
  wire  out_iindex_hi_hi_lo = in_bits_index[4]; // @[Debug.scala 721:31]
  wire  out_iindex_hi_lo = in_bits_index[3]; // @[Debug.scala 721:31]
  wire  out_iindex_lo_hi_hi = in_bits_index[2]; // @[Debug.scala 721:31]
  wire  out_iindex_lo_hi_lo = in_bits_index[1]; // @[Debug.scala 721:31]
  wire  out_iindex_lo_lo = in_bits_index[0]; // @[Debug.scala 721:31]
  wire [5:0] out_iindex = {out_iindex_hi_hi_hi,out_iindex_hi_hi_lo,out_iindex_hi_lo,out_iindex_lo_hi_hi,
    out_iindex_lo_hi_lo,out_iindex_lo_lo}; // @[Cat.scala 30:58]
  wire [6:0] out_findex = in_bits_index & 7'h40; // @[Debug.scala 721:31]
  wire  _out_T_14 = out_findex == 7'h0; // @[Debug.scala 721:31]
  wire  _out_T_30 = out_findex == 7'h40; // @[Debug.scala 721:31]
  wire [63:0] _out_backSel_T = 64'h1 << out_iindex; // @[OneHot.scala 58:35]
  wire  out_backSel_50 = _out_backSel_T[50]; // @[Debug.scala 721:31]
  wire  out_woready__104 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_50 & _out_T_14; // @[Debug.scala 721:31]
  wire [7:0] out_backMask_hi_hi = auto_dmi_in_a_bits_mask[3] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_lo = auto_dmi_in_a_bits_mask[2] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_hi = auto_dmi_in_a_bits_mask[1] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_lo = auto_dmi_in_a_bits_mask[0] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [31:0] out_backMask = {out_backMask_hi_hi,out_backMask_hi_lo,out_backMask_lo_hi,out_backMask_lo_lo}; // @[Cat.scala 30:58]
  wire  _out_womask_T_104 = out_backMask[11]; // @[Debug.scala 721:31]
  wire  out_womask_104 = &out_backMask[11]; // @[Debug.scala 721:31]
  wire  out_f_woready_104 = out_woready__104 & out_womask_104; // @[Debug.scala 721:31]
  wire  DMCS2WrData_grouptype = auto_dmi_in_a_bits_data[11]; // @[Debug.scala 721:31]
  reg  rgParticipateHart_0; // @[Debug.scala 977:38]
  reg  rgParticipateHart_1; // @[Debug.scala 977:38]
  reg  hgParticipateHart_0; // @[Debug.scala 980:38]
  reg  hgParticipateHart_1; // @[Debug.scala 980:38]
  wire [4:0] _out_womask_T_102 = out_backMask[6:2]; // @[Debug.scala 721:31]
  wire  out_womask_102 = &out_backMask[6:2]; // @[Debug.scala 721:31]
  wire  out_f_woready_102 = out_woready__104 & out_womask_102; // @[Debug.scala 721:31]
  wire  DMCS2WrData_hgwrite = auto_dmi_in_a_bits_data[1]; // @[Debug.scala 721:31]
  wire  DMCS2WrData_hgselect = auto_dmi_in_a_bits_data[0]; // @[Debug.scala 721:31]
  wire [4:0] DMCS2WrData_group = auto_dmi_in_a_bits_data[6:2]; // @[Debug.scala 721:31]
  wire [4:0] _GEN_50 = DMCS2WrData_grouptype ? DMCS2WrData_group : {{4'd0}, rgParticipateHart_0}; // @[Debug.scala 988:40 Debug.scala 989:42 Debug.scala 977:38]
  wire [4:0] _GEN_51 = DMCS2WrData_grouptype ? {{4'd0}, hgParticipateHart_0} : DMCS2WrData_group; // @[Debug.scala 988:40 Debug.scala 980:38 Debug.scala 991:42]
  wire [4:0] _GEN_52 = out_f_woready_102 & DMCS2WrData_hgwrite & ~DMCS2WrData_hgselect & hamaskFull_0 &
    DMCS2WrData_group <= 5'h1 ? _GEN_50 : {{4'd0}, rgParticipateHart_0}; // @[Debug.scala 987:141 Debug.scala 977:38]
  wire [4:0] _GEN_53 = out_f_woready_102 & DMCS2WrData_hgwrite & ~DMCS2WrData_hgselect & hamaskFull_0 &
    DMCS2WrData_group <= 5'h1 ? _GEN_51 : {{4'd0}, hgParticipateHart_0}; // @[Debug.scala 987:141 Debug.scala 980:38]
  wire [4:0] _GEN_54 = _T ? 5'h0 : _GEN_52; // @[Debug.scala 984:49 Debug.scala 985:40]
  wire [4:0] _GEN_55 = _T ? 5'h0 : _GEN_53; // @[Debug.scala 984:49 Debug.scala 986:40]
  wire [4:0] _GEN_56 = DMCS2WrData_grouptype ? DMCS2WrData_group : {{4'd0}, rgParticipateHart_1}; // @[Debug.scala 988:40 Debug.scala 989:42 Debug.scala 977:38]
  wire [4:0] _GEN_57 = DMCS2WrData_grouptype ? {{4'd0}, hgParticipateHart_1} : DMCS2WrData_group; // @[Debug.scala 988:40 Debug.scala 980:38 Debug.scala 991:42]
  wire [4:0] _GEN_58 = out_f_woready_102 & DMCS2WrData_hgwrite & ~DMCS2WrData_hgselect & hamaskFull_1 &
    DMCS2WrData_group <= 5'h1 ? _GEN_56 : {{4'd0}, rgParticipateHart_1}; // @[Debug.scala 987:141 Debug.scala 977:38]
  wire [4:0] _GEN_59 = out_f_woready_102 & DMCS2WrData_hgwrite & ~DMCS2WrData_hgselect & hamaskFull_1 &
    DMCS2WrData_group <= 5'h1 ? _GEN_57 : {{4'd0}, hgParticipateHart_1}; // @[Debug.scala 987:141 Debug.scala 980:38]
  wire [4:0] _GEN_60 = _T ? 5'h0 : _GEN_58; // @[Debug.scala 984:49 Debug.scala 985:40]
  wire [4:0] _GEN_61 = _T ? 5'h0 : _GEN_59; // @[Debug.scala 984:49 Debug.scala 986:40]
  wire  _GEN_63 = selectedHartReg[0] ? rgParticipateHart_1 : rgParticipateHart_0; // @[Debug.scala 995:31 Debug.scala 995:31]
  wire  _GEN_65 = selectedHartReg[0] ? hgParticipateHart_1 : hgParticipateHart_0; // @[Debug.scala 995:31 Debug.scala 995:31]
  wire  _DMCS2RdData_group_T_2 = grouptype ? _GEN_63 : _GEN_65; // @[Debug.scala 995:31]
  reg  hgFired_1; // @[Debug.scala 1053:38]
  reg  rgFired_1; // @[Debug.scala 1068:38]
  wire [9:0] hartHaltedId = auto_tl_in_a_bits_data[9:0]; // @[Debug.scala 733:30]
  wire [1:0] _hgHartFiring_1_T = haltedBitRegs >> hartHaltedId; // @[Debug.scala 1091:60]
  wire  in_1_bits_read = auto_tl_in_a_bits_opcode == 3'h4; // @[Debug.scala 733:30]
  wire [8:0] in_1_bits_index = auto_tl_in_a_bits_address[11:3]; // @[Edges.scala 191:34]
  wire  out_iindex_hi_hi_hi_1 = in_1_bits_index[8]; // @[Debug.scala 733:30]
  wire  out_iindex_hi_hi_lo_1 = in_1_bits_index[6]; // @[Debug.scala 733:30]
  wire  out_iindex_hi_lo_hi = in_1_bits_index[5]; // @[Debug.scala 733:30]
  wire  out_iindex_hi_lo_lo = in_1_bits_index[4]; // @[Debug.scala 733:30]
  wire  out_iindex_lo_hi_hi_1 = in_1_bits_index[3]; // @[Debug.scala 733:30]
  wire  out_iindex_lo_hi_lo_1 = in_1_bits_index[2]; // @[Debug.scala 733:30]
  wire  out_iindex_lo_lo_hi = in_1_bits_index[1]; // @[Debug.scala 733:30]
  wire  out_iindex_lo_lo_lo = in_1_bits_index[0]; // @[Debug.scala 733:30]
  wire [7:0] out_iindex_1 = {out_iindex_hi_hi_hi_1,out_iindex_hi_hi_lo_1,out_iindex_hi_lo_hi,out_iindex_hi_lo_lo,
    out_iindex_lo_hi_hi_1,out_iindex_lo_hi_lo_1,out_iindex_lo_lo_hi,out_iindex_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [8:0] out_findex_1 = in_1_bits_index & 9'h80; // @[Debug.scala 733:30]
  wire  _out_T_2282 = out_findex_1 == 9'h0; // @[Debug.scala 733:30]
  wire  _out_T_2278 = out_findex_1 == 9'h80; // @[Debug.scala 733:30]
  wire  _out_wofireMux_T_260 = auto_tl_in_a_valid & auto_tl_in_d_ready; // @[Debug.scala 733:30]
  wire [255:0] _out_backSel_T_1 = 256'h1 << out_iindex_1; // @[OneHot.scala 58:35]
  wire  out_backSel_32_1 = _out_backSel_T_1[32]; // @[Debug.scala 733:30]
  wire  out_woready_1_83 = _out_wofireMux_T_260 & ~in_1_bits_read & out_backSel_32_1 & _out_T_2282; // @[Debug.scala 733:30]
  wire [7:0] out_backMask_hi_hi_hi = auto_tl_in_a_bits_mask[7] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_hi_lo = auto_tl_in_a_bits_mask[6] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_lo_hi = auto_tl_in_a_bits_mask[5] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_lo_lo = auto_tl_in_a_bits_mask[4] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_hi_hi = auto_tl_in_a_bits_mask[3] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_hi_lo = auto_tl_in_a_bits_mask[2] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_lo_hi = auto_tl_in_a_bits_mask[1] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_lo_lo = auto_tl_in_a_bits_mask[0] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [63:0] out_backMask_1 = {out_backMask_hi_hi_hi,out_backMask_hi_hi_lo,out_backMask_hi_lo_hi,out_backMask_hi_lo_lo,
    out_backMask_lo_hi_hi,out_backMask_lo_hi_lo,out_backMask_lo_lo_hi,out_backMask_lo_lo_lo}; // @[Cat.scala 30:58]
  wire  out_womask_211 = &out_backMask_1[9:0]; // @[Debug.scala 733:30]
  wire  out_f_woready_211 = out_woready_1_83 & out_womask_211; // @[Debug.scala 733:30]
  wire  _GEN_67 = hartHaltedId[0] ? hgParticipateHart_1 : hgParticipateHart_0; // @[Debug.scala 1091:140 Debug.scala 1091:140]
  wire  hgHartFiring_1 = out_f_woready_211 & ~_hgHartFiring_1_T[0] & _GEN_67; // @[Debug.scala 1091:75]
  wire  _hgHartsAllHalted_1_T_4 = ~resumeReqRegs[0]; // @[package.scala 70:38]
  wire  _hgHartsAllHalted_1_T_5 = ~resumeReqRegs[1]; // @[package.scala 70:38]
  wire  _hgHartsAllHalted_1_T_6 = haltedBitRegs[0] & _hgHartsAllHalted_1_T_4; // @[package.scala 65:72]
  wire  _hgHartsAllHalted_1_T_7 = haltedBitRegs[1] & _hgHartsAllHalted_1_T_5; // @[package.scala 65:72]
  wire  _hgHartsAllHalted_1_T_8 = ~hgParticipateHart_0; // @[Debug.scala 1092:108]
  wire  _hgHartsAllHalted_1_T_9 = ~hgParticipateHart_1; // @[Debug.scala 1092:108]
  wire  _hgHartsAllHalted_1_T_10 = _hgHartsAllHalted_1_T_6 | _hgHartsAllHalted_1_T_8; // @[package.scala 66:75]
  wire  _hgHartsAllHalted_1_T_11 = _hgHartsAllHalted_1_T_7 | _hgHartsAllHalted_1_T_9; // @[package.scala 66:75]
  wire  hgHartsAllHalted_1 = _hgHartsAllHalted_1_T_10 & _hgHartsAllHalted_1_T_11; // @[Debug.scala 1092:128]
  wire  _GEN_68 = hgFired_1 & hgHartsAllHalted_1 ? 1'h0 : hgFired_1; // @[Debug.scala 1098:80 Debug.scala 1099:23 Debug.scala 1053:38]
  wire  _GEN_69 = ~hgFired_1 & hgHartFiring_1 | _GEN_68; // @[Debug.scala 1096:75 Debug.scala 1097:23]
  wire  _rgHartFiring_1_T_4 = resumeReqRegs[0] & rgParticipateHart_0; // @[package.scala 65:72]
  wire  _rgHartFiring_1_T_5 = resumeReqRegs[1] & rgParticipateHart_1; // @[package.scala 65:72]
  wire  rgHartFiring_1 = _rgHartFiring_1_T_4 | _rgHartFiring_1_T_5; // @[Debug.scala 1102:98]
  wire  _rgHartsAllResumed_1_T_2 = ~rgParticipateHart_0; // @[Debug.scala 1103:80]
  wire  _rgHartsAllResumed_1_T_3 = ~rgParticipateHart_1; // @[Debug.scala 1103:80]
  wire  _rgHartsAllResumed_1_T_4 = resumeAcks[0] | _rgHartsAllResumed_1_T_2; // @[package.scala 66:75]
  wire  _rgHartsAllResumed_1_T_5 = resumeAcks[1] | _rgHartsAllResumed_1_T_3; // @[package.scala 66:75]
  wire  rgHartsAllResumed_1 = _rgHartsAllResumed_1_T_4 & _rgHartsAllResumed_1_T_5; // @[Debug.scala 1103:100]
  wire [1:0] _resumeReqSetFromRg_1_T = {rgParticipateHart_1,rgParticipateHart_0}; // @[Cat.scala 30:58]
  wire  _GEN_71 = rgFired_1 & rgHartsAllResumed_1 ? 1'h0 : rgFired_1; // @[Debug.scala 1110:81 Debug.scala 1111:23 Debug.scala 1068:38]
  wire  _GEN_72 = ~rgFired_1 & rgHartFiring_1 | _GEN_71; // @[Debug.scala 1107:75 Debug.scala 1108:23]
  wire [1:0] _GEN_73 = ~rgFired_1 & rgHartFiring_1 ? _resumeReqSetFromRg_1_T : 2'h0; // @[Debug.scala 1107:75 Debug.scala 1109:34]
  wire [1:0] resumeReqSetFromRg_1 = _T ? 2'h0 : _GEN_73; // @[Debug.scala 1105:49]
  wire  hgDebugInt_0 = hgParticipateHart_0 & hgFired_1; // @[Debug.scala 1117:31 Debug.scala 1117:31]
  wire  hgDebugInt_1 = hgParticipateHart_1 & hgFired_1; // @[Debug.scala 1117:31 Debug.scala 1117:31]
  wire [31:0] haltedStatus_0 = {{30'd0}, haltedBitRegs}; // @[Debug.scala 1134:30]
  wire  haltedSummary = |haltedStatus_0; // @[Debug.scala 1144:48]
  wire [31:0] HALTSUM1RdData_haltsum1 = {{31'd0}, haltedSummary};
  reg [2:0] ABSTRACTCSReg_cmderr; // @[Debug.scala 1158:34]
  wire  out_backSel_22 = _out_backSel_T[22]; // @[Debug.scala 721:31]
  wire  out_woready__83 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_22 & _out_T_14; // @[Debug.scala 721:31]
  wire [2:0] _out_womask_T_83 = out_backMask[10:8]; // @[Debug.scala 721:31]
  wire  out_womask_83 = &out_backMask[10:8]; // @[Debug.scala 721:31]
  wire  out_f_woready_83 = out_woready__83 & out_womask_83; // @[Debug.scala 721:31]
  reg [1:0] ctrlStateReg; // @[Debug.scala 1685:27]
  wire  ABSTRACTCSWrEnLegal = ctrlStateReg == 2'h0; // @[Debug.scala 1695:44]
  wire  ABSTRACTCSWrEn = out_f_woready_83 & ABSTRACTCSWrEnLegal; // @[Debug.scala 1166:51]
  wire [2:0] ABSTRACTCSWrData_cmderr = auto_dmi_in_a_bits_data[10:8]; // @[Debug.scala 721:31]
  wire [2:0] _ABSTRACTCSReg_cmderr_T = ~ABSTRACTCSWrData_cmderr; // @[Debug.scala 1186:58]
  wire [2:0] _ABSTRACTCSReg_cmderr_T_1 = ABSTRACTCSReg_cmderr & _ABSTRACTCSReg_cmderr_T; // @[Debug.scala 1186:56]
  wire [2:0] _GEN_81 = ABSTRACTCSWrEn ? _ABSTRACTCSReg_cmderr_T_1 : ABSTRACTCSReg_cmderr; // @[Debug.scala 1185:30 Debug.scala 1186:32 Debug.scala 1158:34]
  wire  _T_475 = ctrlStateReg == 2'h1; // @[Debug.scala 1750:30]
  reg [7:0] COMMANDRdData_cmdtype; // @[Debug.scala 1243:25]
  wire  commandRegIsAccessRegister = COMMANDRdData_cmdtype == 8'h0; // @[Debug.scala 1710:58]
  reg [23:0] COMMANDRdData_control; // @[Debug.scala 1243:25]
  wire [31:0] _accessRegisterCommandReg_T = {COMMANDRdData_cmdtype,COMMANDRdData_control}; // @[Debug.scala 1485:62]
  wire  accessRegisterCommandReg_transfer = _accessRegisterCommandReg_T[17]; // @[Debug.scala 1485:73]
  wire  accessRegisterCommandReg_write = _accessRegisterCommandReg_T[16]; // @[Debug.scala 1485:73]
  wire [15:0] accessRegisterCommandReg_regno = _accessRegisterCommandReg_T[15:0]; // @[Debug.scala 1485:73]
  wire [2:0] accessRegisterCommandReg_size = _accessRegisterCommandReg_T[22:20]; // @[Debug.scala 1485:73]
  wire  accessRegIsLegalSize = accessRegisterCommandReg_size == 3'h2 | accessRegisterCommandReg_size == 3'h3; // @[Debug.scala 1718:72]
  wire  accessRegIsGPR = accessRegisterCommandReg_regno >= 16'h1000 & accessRegisterCommandReg_regno <= 16'h101f &
    accessRegIsLegalSize; // @[Debug.scala 1719:117]
  wire  _GEN_1855 = ~accessRegisterCommandReg_transfer | accessRegIsGPR ? 1'h0 : 1'h1; // @[Debug.scala 1729:73 Debug.scala 1730:33]
  wire  commandRegIsUnsupported = commandRegIsAccessRegister ? _GEN_1855 : 1'h1; // @[Debug.scala 1726:39]
  wire [1:0] _hartHalted_T = haltedBitRegs >> selectedHartReg; // @[Debug.scala 1687:37]
  wire  hartHalted = _hartHalted_T[0]; // @[Debug.scala 1687:37]
  wire  _GEN_1856 = (~accessRegisterCommandReg_transfer | accessRegIsGPR) & ~hartHalted; // @[Debug.scala 1729:73 Debug.scala 1731:33]
  wire  commandRegBadHaltResume = commandRegIsAccessRegister & _GEN_1856; // @[Debug.scala 1726:39]
  wire  _GEN_1872 = commandRegIsUnsupported ? 1'h0 : commandRegBadHaltResume; // @[Debug.scala 1757:38]
  wire  _GEN_1885 = _T_475 & _GEN_1872; // @[Debug.scala 1750:59]
  wire  errorHaltResume = ABSTRACTCSWrEnLegal ? 1'h0 : _GEN_1885; // @[Debug.scala 1742:47]
  wire [2:0] _GEN_82 = errorHaltResume ? 3'h4 : _GEN_81; // @[Debug.scala 1182:36 Debug.scala 1183:30]
  wire  out_backSel_23 = _out_backSel_T[23]; // @[Debug.scala 721:31]
  wire  out_woready__113 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_23 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_womask_113 = &out_backMask; // @[Debug.scala 721:31]
  wire  out_f_woready_113 = out_woready__113 & out_womask_113; // @[Debug.scala 721:31]
  wire  COMMANDWrEn = out_f_woready_113 & ABSTRACTCSWrEnLegal; // @[Debug.scala 1251:40]
  wire [31:0] COMMANDWrDataVal = out_f_woready_113 ? auto_dmi_in_a_bits_data : 32'h0; // @[Debug.scala 273:24 Debug.scala 273:30]
  wire [7:0] COMMANDWrData_cmdtype = COMMANDWrDataVal[31:24]; // @[Debug.scala 1246:65]
  wire  commandWrIsAccessRegister = COMMANDWrData_cmdtype == 8'h0; // @[Debug.scala 1709:60]
  wire  _wrAccessRegisterCommand_T_1 = ABSTRACTCSReg_cmderr == 3'h0; // @[Debug.scala 1735:103]
  wire  wrAccessRegisterCommand = COMMANDWrEn & commandWrIsAccessRegister & ABSTRACTCSReg_cmderr == 3'h0; // @[Debug.scala 1735:78]
  wire  out_backSel_4 = _out_backSel_T[4]; // @[Debug.scala 721:31]
  wire  out_woready__120 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_4 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_womask_120 = &out_backMask[7:0]; // @[Debug.scala 721:31]
  wire  out_f_woready_120 = out_woready__120 & out_womask_120; // @[Debug.scala 721:31]
  wire  _out_rofireMux_T_1 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & in_bits_read; // @[Debug.scala 721:31]
  wire  out_roready__120 = _out_rofireMux_T_1 & out_backSel_4 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_romask_120 = |out_backMask[7:0]; // @[Debug.scala 721:31]
  wire  out_f_roready_120 = out_roready__120 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiAbstractDataAccessVec_0 = out_f_woready_120 | out_f_roready_120; // @[Debug.scala 1224:105]
  reg [11:0] ABSTRACTAUTOReg_autoexecdata; // @[Debug.scala 1202:36]
  wire  autoexecData_0 = dmiAbstractDataAccessVec_0 & ABSTRACTAUTOReg_autoexecdata[0]; // @[Debug.scala 1235:140]
  wire  out_backSel_5 = _out_backSel_T[5]; // @[Debug.scala 721:31]
  wire  out_woready__0 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_5 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready = out_woready__0 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__0 = _out_rofireMux_T_1 & out_backSel_5 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready = out_roready__0 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiAbstractDataAccessVec_4 = out_f_woready | out_f_roready; // @[Debug.scala 1224:105]
  wire  autoexecData_1 = dmiAbstractDataAccessVec_4 & ABSTRACTAUTOReg_autoexecdata[1]; // @[Debug.scala 1235:140]
  wire  out_backSel_32 = _out_backSel_T[32]; // @[Debug.scala 721:31]
  wire  out_woready__49 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_32 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_49 = out_woready__49 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__49 = _out_rofireMux_T_1 & out_backSel_32 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_49 = out_roready__49 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_0 = out_f_woready_49 | out_f_roready_49; // @[Debug.scala 1227:108]
  reg [15:0] ABSTRACTAUTOReg_autoexecprogbuf; // @[Debug.scala 1202:36]
  wire  autoexecProg_0 = dmiProgramBufferAccessVec_0 & ABSTRACTAUTOReg_autoexecprogbuf[0]; // @[Debug.scala 1236:144]
  wire  out_backSel_33 = _out_backSel_T[33]; // @[Debug.scala 721:31]
  wire  out_woready__41 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_33 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_41 = out_woready__41 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__41 = _out_rofireMux_T_1 & out_backSel_33 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_41 = out_roready__41 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_4 = out_f_woready_41 | out_f_roready_41; // @[Debug.scala 1227:108]
  wire  autoexecProg_1 = dmiProgramBufferAccessVec_4 & ABSTRACTAUTOReg_autoexecprogbuf[1]; // @[Debug.scala 1236:144]
  wire  out_backSel_34 = _out_backSel_T[34]; // @[Debug.scala 721:31]
  wire  out_woready__53 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_34 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_53 = out_woready__53 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__53 = _out_rofireMux_T_1 & out_backSel_34 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_53 = out_roready__53 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_8 = out_f_woready_53 | out_f_roready_53; // @[Debug.scala 1227:108]
  wire  autoexecProg_2 = dmiProgramBufferAccessVec_8 & ABSTRACTAUTOReg_autoexecprogbuf[2]; // @[Debug.scala 1236:144]
  wire  out_backSel_35 = _out_backSel_T[35]; // @[Debug.scala 721:31]
  wire  out_woready__96 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_35 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_96 = out_woready__96 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__96 = _out_rofireMux_T_1 & out_backSel_35 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_96 = out_roready__96 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_12 = out_f_woready_96 | out_f_roready_96; // @[Debug.scala 1227:108]
  wire  autoexecProg_3 = dmiProgramBufferAccessVec_12 & ABSTRACTAUTOReg_autoexecprogbuf[3]; // @[Debug.scala 1236:144]
  wire  out_backSel_36 = _out_backSel_T[36]; // @[Debug.scala 721:31]
  wire  out_woready__115 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_36 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_115 = out_woready__115 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__115 = _out_rofireMux_T_1 & out_backSel_36 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_115 = out_roready__115 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_16 = out_f_woready_115 | out_f_roready_115; // @[Debug.scala 1227:108]
  wire  autoexecProg_4 = dmiProgramBufferAccessVec_16 & ABSTRACTAUTOReg_autoexecprogbuf[4]; // @[Debug.scala 1236:144]
  wire  out_backSel_37 = _out_backSel_T[37]; // @[Debug.scala 721:31]
  wire  out_woready__26 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_37 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_26 = out_woready__26 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__26 = _out_rofireMux_T_1 & out_backSel_37 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_26 = out_roready__26 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_20 = out_f_woready_26 | out_f_roready_26; // @[Debug.scala 1227:108]
  wire  autoexecProg_5 = dmiProgramBufferAccessVec_20 & ABSTRACTAUTOReg_autoexecprogbuf[5]; // @[Debug.scala 1236:144]
  wire  out_backSel_38 = _out_backSel_T[38]; // @[Debug.scala 721:31]
  wire  out_woready__37 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_38 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_37 = out_woready__37 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__37 = _out_rofireMux_T_1 & out_backSel_38 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_37 = out_roready__37 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_24 = out_f_woready_37 | out_f_roready_37; // @[Debug.scala 1227:108]
  wire  autoexecProg_6 = dmiProgramBufferAccessVec_24 & ABSTRACTAUTOReg_autoexecprogbuf[6]; // @[Debug.scala 1236:144]
  wire  out_backSel_39 = _out_backSel_T[39]; // @[Debug.scala 721:31]
  wire  out_woready__92 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_39 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_92 = out_woready__92 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__92 = _out_rofireMux_T_1 & out_backSel_39 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_92 = out_roready__92 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_28 = out_f_woready_92 | out_f_roready_92; // @[Debug.scala 1227:108]
  wire  autoexecProg_7 = dmiProgramBufferAccessVec_28 & ABSTRACTAUTOReg_autoexecprogbuf[7]; // @[Debug.scala 1236:144]
  wire  out_backSel_40 = _out_backSel_T[40]; // @[Debug.scala 721:31]
  wire  out_woready__109 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_40 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_109 = out_woready__109 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__109 = _out_rofireMux_T_1 & out_backSel_40 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_109 = out_roready__109 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_32 = out_f_woready_109 | out_f_roready_109; // @[Debug.scala 1227:108]
  wire  autoexecProg_8 = dmiProgramBufferAccessVec_32 & ABSTRACTAUTOReg_autoexecprogbuf[8]; // @[Debug.scala 1236:144]
  wire  out_backSel_41 = _out_backSel_T[41]; // @[Debug.scala 721:31]
  wire  out_woready__45 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_41 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_45 = out_woready__45 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__45 = _out_rofireMux_T_1 & out_backSel_41 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_45 = out_roready__45 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_36 = out_f_woready_45 | out_f_roready_45; // @[Debug.scala 1227:108]
  wire  autoexecProg_9 = dmiProgramBufferAccessVec_36 & ABSTRACTAUTOReg_autoexecprogbuf[9]; // @[Debug.scala 1236:144]
  wire  out_backSel_42 = _out_backSel_T[42]; // @[Debug.scala 721:31]
  wire  out_woready__19 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_42 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_19 = out_woready__19 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__19 = _out_rofireMux_T_1 & out_backSel_42 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_19 = out_roready__19 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_40 = out_f_woready_19 | out_f_roready_19; // @[Debug.scala 1227:108]
  wire  autoexecProg_10 = dmiProgramBufferAccessVec_40 & ABSTRACTAUTOReg_autoexecprogbuf[10]; // @[Debug.scala 1236:144]
  wire  out_backSel_43 = _out_backSel_T[43]; // @[Debug.scala 721:31]
  wire  out_woready__105 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_43 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_105 = out_woready__105 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__105 = _out_rofireMux_T_1 & out_backSel_43 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_105 = out_roready__105 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_44 = out_f_woready_105 | out_f_roready_105; // @[Debug.scala 1227:108]
  wire  autoexecProg_11 = dmiProgramBufferAccessVec_44 & ABSTRACTAUTOReg_autoexecprogbuf[11]; // @[Debug.scala 1236:144]
  wire  out_backSel_44 = _out_backSel_T[44]; // @[Debug.scala 721:31]
  wire  out_woready__88 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_44 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_88 = out_woready__88 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__88 = _out_rofireMux_T_1 & out_backSel_44 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_88 = out_roready__88 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_48 = out_f_woready_88 | out_f_roready_88; // @[Debug.scala 1227:108]
  wire  autoexecProg_12 = dmiProgramBufferAccessVec_48 & ABSTRACTAUTOReg_autoexecprogbuf[12]; // @[Debug.scala 1236:144]
  wire  out_backSel_45 = _out_backSel_T[45]; // @[Debug.scala 721:31]
  wire  out_woready__57 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_45 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_57 = out_woready__57 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__57 = _out_rofireMux_T_1 & out_backSel_45 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_57 = out_roready__57 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_52 = out_f_woready_57 | out_f_roready_57; // @[Debug.scala 1227:108]
  wire  autoexecProg_13 = dmiProgramBufferAccessVec_52 & ABSTRACTAUTOReg_autoexecprogbuf[13]; // @[Debug.scala 1236:144]
  wire  out_backSel_46 = _out_backSel_T[46]; // @[Debug.scala 721:31]
  wire  out_woready__30 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_46 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_30 = out_woready__30 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__30 = _out_rofireMux_T_1 & out_backSel_46 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_30 = out_roready__30 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_56 = out_f_woready_30 | out_f_roready_30; // @[Debug.scala 1227:108]
  wire  autoexecProg_14 = dmiProgramBufferAccessVec_56 & ABSTRACTAUTOReg_autoexecprogbuf[14]; // @[Debug.scala 1236:144]
  wire  out_backSel_47 = _out_backSel_T[47]; // @[Debug.scala 721:31]
  wire  out_woready__124 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_47 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_124 = out_woready__124 & out_womask_120; // @[Debug.scala 721:31]
  wire  out_roready__124 = _out_rofireMux_T_1 & out_backSel_47 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_124 = out_roready__124 & out_romask_120; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_60 = out_f_woready_124 | out_f_roready_124; // @[Debug.scala 1227:108]
  wire  autoexecProg_15 = dmiProgramBufferAccessVec_60 & ABSTRACTAUTOReg_autoexecprogbuf[15]; // @[Debug.scala 1236:144]
  wire  autoexec = autoexecData_0 | autoexecData_1 | (autoexecProg_0 | autoexecProg_1 | autoexecProg_2 | autoexecProg_3
     | autoexecProg_4 | autoexecProg_5 | autoexecProg_6 | autoexecProg_7 | autoexecProg_8 | autoexecProg_9 |
    autoexecProg_10 | autoexecProg_11 | autoexecProg_12 | autoexecProg_13 | autoexecProg_14 | autoexecProg_15); // @[Debug.scala 1238:48]
  wire  regAccessRegisterCommand = autoexec & commandRegIsAccessRegister & _wrAccessRegisterCommand_T_1; // @[Debug.scala 1736:78]
  wire  commandWrIsUnsupported = COMMANDWrEn & ~commandWrIsAccessRegister; // @[Debug.scala 1712:46]
  wire  _T_474 = autoexec & commandRegIsUnsupported; // @[Debug.scala 1747:28]
  wire  _GEN_1862 = commandWrIsUnsupported | _T_474; // @[Debug.scala 1745:43 Debug.scala 1746:26]
  wire  _GEN_1864 = wrAccessRegisterCommand | regAccessRegisterCommand ? 1'h0 : _GEN_1862; // @[Debug.scala 1743:66]
  wire  _GEN_1883 = _T_475 & commandRegIsUnsupported; // @[Debug.scala 1750:59]
  wire  errorUnsupported = ABSTRACTCSWrEnLegal ? _GEN_1864 : _GEN_1883; // @[Debug.scala 1742:47]
  wire  _T_476 = ctrlStateReg == 2'h2; // @[Debug.scala 1771:30]
  wire  out_backSel_33_1 = _out_backSel_T_1[33]; // @[Debug.scala 733:30]
  wire  out_woready_1_49 = _out_wofireMux_T_260 & ~in_1_bits_read & out_backSel_33_1 & _out_T_2282; // @[Debug.scala 733:30]
  wire  out_womask_177 = &out_backMask_1[41:32]; // @[Debug.scala 733:30]
  wire  out_f_woready_177 = out_woready_1_49 & out_womask_177; // @[Debug.scala 733:30]
  wire  _GEN_1881 = _T_476 & out_f_woready_177; // @[Debug.scala 1771:51]
  wire  _GEN_1887 = _T_475 ? 1'h0 : _GEN_1881; // @[Debug.scala 1750:59]
  wire  errorException = ABSTRACTCSWrEnLegal ? 1'h0 : _GEN_1887; // @[Debug.scala 1742:47]
  wire  _errorBusy_T = ~ABSTRACTCSWrEnLegal; // @[Debug.scala 1701:45]
  wire  out_backSel_24 = _out_backSel_T[24]; // @[Debug.scala 721:31]
  wire  out_woready__23 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_24 & _out_T_14; // @[Debug.scala 721:31]
  wire [1:0] _out_womask_T_23 = out_backMask[1:0]; // @[Debug.scala 721:31]
  wire  out_womask_23 = &out_backMask[1:0]; // @[Debug.scala 721:31]
  wire  out_f_woready_23 = out_woready__23 & out_womask_23; // @[Debug.scala 721:31]
  wire  _errorBusy_T_3 = out_f_woready_23 & _errorBusy_T; // @[Debug.scala 1702:42]
  wire  _errorBusy_T_4 = out_f_woready_83 & _errorBusy_T | _errorBusy_T_3; // @[Debug.scala 1701:74]
  wire [15:0] _out_womask_T_25 = out_backMask[31:16]; // @[Debug.scala 721:31]
  wire  out_womask_25 = &out_backMask[31:16]; // @[Debug.scala 721:31]
  wire  out_f_woready_25 = out_woready__23 & out_womask_25; // @[Debug.scala 721:31]
  wire  _errorBusy_T_6 = out_f_woready_25 & _errorBusy_T; // @[Debug.scala 1703:44]
  wire  _errorBusy_T_7 = _errorBusy_T_4 | _errorBusy_T_6; // @[Debug.scala 1702:74]
  wire  _errorBusy_T_9 = out_f_woready_113 & _errorBusy_T; // @[Debug.scala 1704:42]
  wire  _errorBusy_T_10 = _errorBusy_T_7 | _errorBusy_T_9; // @[Debug.scala 1703:74]
  wire  out_womask_121 = &out_backMask[15:8]; // @[Debug.scala 721:31]
  wire  out_f_woready_121 = out_woready__120 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_romask_121 = |out_backMask[15:8]; // @[Debug.scala 721:31]
  wire  out_f_roready_121 = out_roready__120 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiAbstractDataAccessVec_1 = out_f_woready_121 | out_f_roready_121; // @[Debug.scala 1224:105]
  wire  out_womask_122 = &out_backMask[23:16]; // @[Debug.scala 721:31]
  wire  out_f_woready_122 = out_woready__120 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_romask_122 = |out_backMask[23:16]; // @[Debug.scala 721:31]
  wire  out_f_roready_122 = out_roready__120 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiAbstractDataAccessVec_2 = out_f_woready_122 | out_f_roready_122; // @[Debug.scala 1224:105]
  wire  out_womask_123 = &out_backMask[31:24]; // @[Debug.scala 721:31]
  wire  out_f_woready_123 = out_woready__120 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_romask_123 = |out_backMask[31:24]; // @[Debug.scala 721:31]
  wire  out_f_roready_123 = out_roready__120 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiAbstractDataAccessVec_3 = out_f_woready_123 | out_f_roready_123; // @[Debug.scala 1224:105]
  wire  out_f_woready_1 = out_woready__0 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_1 = out_roready__0 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiAbstractDataAccessVec_5 = out_f_woready_1 | out_f_roready_1; // @[Debug.scala 1224:105]
  wire  out_f_woready_2 = out_woready__0 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_2 = out_roready__0 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiAbstractDataAccessVec_6 = out_f_woready_2 | out_f_roready_2; // @[Debug.scala 1224:105]
  wire  out_f_woready_3 = out_woready__0 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_3 = out_roready__0 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiAbstractDataAccessVec_7 = out_f_woready_3 | out_f_roready_3; // @[Debug.scala 1224:105]
  wire  dmiAbstractDataAccess = dmiAbstractDataAccessVec_0 | dmiAbstractDataAccessVec_1 | dmiAbstractDataAccessVec_2 |
    dmiAbstractDataAccessVec_3 | dmiAbstractDataAccessVec_4 | dmiAbstractDataAccessVec_5 | dmiAbstractDataAccessVec_6 |
    dmiAbstractDataAccessVec_7; // @[Debug.scala 1229:68]
  wire  _errorBusy_T_12 = dmiAbstractDataAccess & _errorBusy_T; // @[Debug.scala 1705:42]
  wire  _errorBusy_T_13 = _errorBusy_T_10 | _errorBusy_T_12; // @[Debug.scala 1704:74]
  wire  out_f_woready_50 = out_woready__49 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_50 = out_roready__49 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_1 = out_f_woready_50 | out_f_roready_50; // @[Debug.scala 1227:108]
  wire  out_f_woready_51 = out_woready__49 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_51 = out_roready__49 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_2 = out_f_woready_51 | out_f_roready_51; // @[Debug.scala 1227:108]
  wire  out_f_woready_52 = out_woready__49 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_52 = out_roready__49 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_3 = out_f_woready_52 | out_f_roready_52; // @[Debug.scala 1227:108]
  wire  out_f_woready_42 = out_woready__41 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_42 = out_roready__41 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_5 = out_f_woready_42 | out_f_roready_42; // @[Debug.scala 1227:108]
  wire  out_f_woready_43 = out_woready__41 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_43 = out_roready__41 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_6 = out_f_woready_43 | out_f_roready_43; // @[Debug.scala 1227:108]
  wire  out_f_woready_44 = out_woready__41 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_44 = out_roready__41 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_7 = out_f_woready_44 | out_f_roready_44; // @[Debug.scala 1227:108]
  wire  out_f_woready_54 = out_woready__53 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_54 = out_roready__53 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_9 = out_f_woready_54 | out_f_roready_54; // @[Debug.scala 1227:108]
  wire  out_f_woready_55 = out_woready__53 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_55 = out_roready__53 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_10 = out_f_woready_55 | out_f_roready_55; // @[Debug.scala 1227:108]
  wire  out_f_woready_56 = out_woready__53 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_56 = out_roready__53 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_11 = out_f_woready_56 | out_f_roready_56; // @[Debug.scala 1227:108]
  wire  out_f_woready_97 = out_woready__96 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_97 = out_roready__96 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_13 = out_f_woready_97 | out_f_roready_97; // @[Debug.scala 1227:108]
  wire  out_f_woready_98 = out_woready__96 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_98 = out_roready__96 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_14 = out_f_woready_98 | out_f_roready_98; // @[Debug.scala 1227:108]
  wire  out_f_woready_99 = out_woready__96 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_99 = out_roready__96 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_15 = out_f_woready_99 | out_f_roready_99; // @[Debug.scala 1227:108]
  wire  out_f_woready_116 = out_woready__115 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_116 = out_roready__115 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_17 = out_f_woready_116 | out_f_roready_116; // @[Debug.scala 1227:108]
  wire  out_f_woready_117 = out_woready__115 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_117 = out_roready__115 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_18 = out_f_woready_117 | out_f_roready_117; // @[Debug.scala 1227:108]
  wire  out_f_woready_118 = out_woready__115 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_118 = out_roready__115 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_19 = out_f_woready_118 | out_f_roready_118; // @[Debug.scala 1227:108]
  wire  out_f_woready_27 = out_woready__26 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_27 = out_roready__26 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_21 = out_f_woready_27 | out_f_roready_27; // @[Debug.scala 1227:108]
  wire  out_f_woready_28 = out_woready__26 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_28 = out_roready__26 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_22 = out_f_woready_28 | out_f_roready_28; // @[Debug.scala 1227:108]
  wire  out_f_woready_29 = out_woready__26 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_29 = out_roready__26 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_23 = out_f_woready_29 | out_f_roready_29; // @[Debug.scala 1227:108]
  wire  out_f_woready_38 = out_woready__37 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_38 = out_roready__37 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_25 = out_f_woready_38 | out_f_roready_38; // @[Debug.scala 1227:108]
  wire  out_f_woready_39 = out_woready__37 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_39 = out_roready__37 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_26 = out_f_woready_39 | out_f_roready_39; // @[Debug.scala 1227:108]
  wire  out_f_woready_40 = out_woready__37 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_40 = out_roready__37 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_27 = out_f_woready_40 | out_f_roready_40; // @[Debug.scala 1227:108]
  wire  out_f_woready_93 = out_woready__92 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_93 = out_roready__92 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_29 = out_f_woready_93 | out_f_roready_93; // @[Debug.scala 1227:108]
  wire  out_f_woready_94 = out_woready__92 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_94 = out_roready__92 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_30 = out_f_woready_94 | out_f_roready_94; // @[Debug.scala 1227:108]
  wire  _dmiProgramBufferAccess_T_29 = dmiProgramBufferAccessVec_0 | dmiProgramBufferAccessVec_1 |
    dmiProgramBufferAccessVec_2 | dmiProgramBufferAccessVec_3 | dmiProgramBufferAccessVec_4 |
    dmiProgramBufferAccessVec_5 | dmiProgramBufferAccessVec_6 | dmiProgramBufferAccessVec_7 |
    dmiProgramBufferAccessVec_8 | dmiProgramBufferAccessVec_9 | dmiProgramBufferAccessVec_10 |
    dmiProgramBufferAccessVec_11 | dmiProgramBufferAccessVec_12 | dmiProgramBufferAccessVec_13 |
    dmiProgramBufferAccessVec_14 | dmiProgramBufferAccessVec_15 | dmiProgramBufferAccessVec_16 |
    dmiProgramBufferAccessVec_17 | dmiProgramBufferAccessVec_18 | dmiProgramBufferAccessVec_19 |
    dmiProgramBufferAccessVec_20 | dmiProgramBufferAccessVec_21 | dmiProgramBufferAccessVec_22 |
    dmiProgramBufferAccessVec_23 | dmiProgramBufferAccessVec_24 | dmiProgramBufferAccessVec_25 |
    dmiProgramBufferAccessVec_26 | dmiProgramBufferAccessVec_27 | dmiProgramBufferAccessVec_28 |
    dmiProgramBufferAccessVec_29 | dmiProgramBufferAccessVec_30; // @[Debug.scala 1230:69]
  wire  out_f_woready_95 = out_woready__92 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_95 = out_roready__92 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_31 = out_f_woready_95 | out_f_roready_95; // @[Debug.scala 1227:108]
  wire  out_f_woready_110 = out_woready__109 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_110 = out_roready__109 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_33 = out_f_woready_110 | out_f_roready_110; // @[Debug.scala 1227:108]
  wire  out_f_woready_111 = out_woready__109 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_111 = out_roready__109 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_34 = out_f_woready_111 | out_f_roready_111; // @[Debug.scala 1227:108]
  wire  out_f_woready_112 = out_woready__109 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_112 = out_roready__109 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_35 = out_f_woready_112 | out_f_roready_112; // @[Debug.scala 1227:108]
  wire  out_f_woready_46 = out_woready__45 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_46 = out_roready__45 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_37 = out_f_woready_46 | out_f_roready_46; // @[Debug.scala 1227:108]
  wire  out_f_woready_47 = out_woready__45 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_47 = out_roready__45 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_38 = out_f_woready_47 | out_f_roready_47; // @[Debug.scala 1227:108]
  wire  out_f_woready_48 = out_woready__45 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_48 = out_roready__45 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_39 = out_f_woready_48 | out_f_roready_48; // @[Debug.scala 1227:108]
  wire  out_f_woready_20 = out_woready__19 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_20 = out_roready__19 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_41 = out_f_woready_20 | out_f_roready_20; // @[Debug.scala 1227:108]
  wire  out_f_woready_21 = out_woready__19 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_21 = out_roready__19 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_42 = out_f_woready_21 | out_f_roready_21; // @[Debug.scala 1227:108]
  wire  out_f_woready_22 = out_woready__19 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_22 = out_roready__19 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_43 = out_f_woready_22 | out_f_roready_22; // @[Debug.scala 1227:108]
  wire  out_f_woready_106 = out_woready__105 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_106 = out_roready__105 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_45 = out_f_woready_106 | out_f_roready_106; // @[Debug.scala 1227:108]
  wire  out_f_woready_107 = out_woready__105 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_107 = out_roready__105 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_46 = out_f_woready_107 | out_f_roready_107; // @[Debug.scala 1227:108]
  wire  out_f_woready_108 = out_woready__105 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_108 = out_roready__105 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_47 = out_f_woready_108 | out_f_roready_108; // @[Debug.scala 1227:108]
  wire  out_f_woready_89 = out_woready__88 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_89 = out_roready__88 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_49 = out_f_woready_89 | out_f_roready_89; // @[Debug.scala 1227:108]
  wire  out_f_woready_90 = out_woready__88 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_90 = out_roready__88 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_50 = out_f_woready_90 | out_f_roready_90; // @[Debug.scala 1227:108]
  wire  out_f_woready_91 = out_woready__88 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_91 = out_roready__88 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_51 = out_f_woready_91 | out_f_roready_91; // @[Debug.scala 1227:108]
  wire  out_f_woready_58 = out_woready__57 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_58 = out_roready__57 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_53 = out_f_woready_58 | out_f_roready_58; // @[Debug.scala 1227:108]
  wire  out_f_woready_59 = out_woready__57 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_59 = out_roready__57 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_54 = out_f_woready_59 | out_f_roready_59; // @[Debug.scala 1227:108]
  wire  out_f_woready_60 = out_woready__57 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_60 = out_roready__57 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_55 = out_f_woready_60 | out_f_roready_60; // @[Debug.scala 1227:108]
  wire  out_f_woready_31 = out_woready__30 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_31 = out_roready__30 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_57 = out_f_woready_31 | out_f_roready_31; // @[Debug.scala 1227:108]
  wire  out_f_woready_32 = out_woready__30 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_32 = out_roready__30 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_58 = out_f_woready_32 | out_f_roready_32; // @[Debug.scala 1227:108]
  wire  out_f_woready_33 = out_woready__30 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_33 = out_roready__30 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_59 = out_f_woready_33 | out_f_roready_33; // @[Debug.scala 1227:108]
  wire  _dmiProgramBufferAccess_T_59 = _dmiProgramBufferAccess_T_29 | dmiProgramBufferAccessVec_31 |
    dmiProgramBufferAccessVec_32 | dmiProgramBufferAccessVec_33 | dmiProgramBufferAccessVec_34 |
    dmiProgramBufferAccessVec_35 | dmiProgramBufferAccessVec_36 | dmiProgramBufferAccessVec_37 |
    dmiProgramBufferAccessVec_38 | dmiProgramBufferAccessVec_39 | dmiProgramBufferAccessVec_40 |
    dmiProgramBufferAccessVec_41 | dmiProgramBufferAccessVec_42 | dmiProgramBufferAccessVec_43 |
    dmiProgramBufferAccessVec_44 | dmiProgramBufferAccessVec_45 | dmiProgramBufferAccessVec_46 |
    dmiProgramBufferAccessVec_47 | dmiProgramBufferAccessVec_48 | dmiProgramBufferAccessVec_49 |
    dmiProgramBufferAccessVec_50 | dmiProgramBufferAccessVec_51 | dmiProgramBufferAccessVec_52 |
    dmiProgramBufferAccessVec_53 | dmiProgramBufferAccessVec_54 | dmiProgramBufferAccessVec_55 |
    dmiProgramBufferAccessVec_56 | dmiProgramBufferAccessVec_57 | dmiProgramBufferAccessVec_58 |
    dmiProgramBufferAccessVec_59 | dmiProgramBufferAccessVec_60; // @[Debug.scala 1230:69]
  wire  out_f_woready_125 = out_woready__124 & out_womask_121; // @[Debug.scala 721:31]
  wire  out_f_roready_125 = out_roready__124 & out_romask_121; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_61 = out_f_woready_125 | out_f_roready_125; // @[Debug.scala 1227:108]
  wire  out_f_woready_126 = out_woready__124 & out_womask_122; // @[Debug.scala 721:31]
  wire  out_f_roready_126 = out_roready__124 & out_romask_122; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_62 = out_f_woready_126 | out_f_roready_126; // @[Debug.scala 1227:108]
  wire  out_f_woready_127 = out_woready__124 & out_womask_123; // @[Debug.scala 721:31]
  wire  out_f_roready_127 = out_roready__124 & out_romask_123; // @[Debug.scala 721:31]
  wire  dmiProgramBufferAccessVec_63 = out_f_woready_127 | out_f_roready_127; // @[Debug.scala 1227:108]
  wire  dmiProgramBufferAccess = _dmiProgramBufferAccess_T_59 | dmiProgramBufferAccessVec_61 |
    dmiProgramBufferAccessVec_62 | dmiProgramBufferAccessVec_63; // @[Debug.scala 1230:69]
  wire  _errorBusy_T_15 = dmiProgramBufferAccess & _errorBusy_T; // @[Debug.scala 1706:42]
  wire  errorBusy = _errorBusy_T_13 | _errorBusy_T_15; // @[Debug.scala 1705:74]
  wire [15:0] ABSTRACTAUTOWrData_autoexecprogbuf = auto_dmi_in_a_bits_data[31:16]; // @[Debug.scala 721:31]
  wire [11:0] ABSTRACTAUTOWrData_autoexecdata = {{10'd0}, auto_dmi_in_a_bits_data[1:0]};
  wire [11:0] _ABSTRACTAUTOReg_autoexecdata_T = ABSTRACTAUTOWrData_autoexecdata & 12'h3; // @[Debug.scala 1219:73]
  wire [23:0] COMMANDWrData_control = COMMANDWrDataVal[23:0]; // @[Debug.scala 1246:65]
  reg [7:0] abstractDataMem_0; // @[Debug.scala 1266:36]
  reg [7:0] abstractDataMem_1; // @[Debug.scala 1266:36]
  reg [7:0] abstractDataMem_2; // @[Debug.scala 1266:36]
  reg [7:0] abstractDataMem_3; // @[Debug.scala 1266:36]
  reg [7:0] abstractDataMem_4; // @[Debug.scala 1266:36]
  reg [7:0] abstractDataMem_5; // @[Debug.scala 1266:36]
  reg [7:0] abstractDataMem_6; // @[Debug.scala 1266:36]
  reg [7:0] abstractDataMem_7; // @[Debug.scala 1266:36]
  reg [7:0] programBufferMem_0; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_1; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_2; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_3; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_4; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_5; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_6; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_7; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_8; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_9; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_10; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_11; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_12; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_13; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_14; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_15; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_16; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_17; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_18; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_19; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_20; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_21; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_22; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_23; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_24; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_25; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_26; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_27; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_28; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_29; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_30; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_31; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_32; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_33; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_34; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_35; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_36; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_37; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_38; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_39; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_40; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_41; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_42; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_43; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_44; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_45; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_46; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_47; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_48; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_49; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_50; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_51; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_52; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_53; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_54; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_55; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_56; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_57; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_58; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_59; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_60; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_61; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_62; // @[Debug.scala 1270:34]
  reg [7:0] programBufferMem_63; // @[Debug.scala 1270:34]
  wire [1023:0] hartHaltedIdIndex = 1024'h1 << hartHaltedId; // @[OneHot.scala 58:35]
  wire [1023:0] _GEN_105 = out_f_woready_211 ? hartHaltedIdIndex : 1024'h0; // @[Debug.scala 1286:31 Debug.scala 1287:32]
  wire  out_f_woready_176 = out_woready_1_49 & out_womask_211; // @[Debug.scala 733:30]
  wire [1023:0] _GEN_106 = out_f_woready_176 ? hartHaltedIdIndex : 1024'h0; // @[Debug.scala 1289:33 Debug.scala 1290:33]
  wire [1:0] _GEN_107 = resumereq ? _resumeAcks_T_1 : 2'h0; // @[Debug.scala 1292:26 Debug.scala 1293:33]
  wire [1:0] haltedSetFromHalting = _GEN_105[1:0];
  wire [1:0] _haltedBitRegs_T = haltedBitRegs | haltedSetFromHalting; // @[Debug.scala 1296:41]
  wire [1:0] haltedClrFromResuming = _GEN_106[1:0];
  wire [1:0] _haltedBitRegs_T_2 = haltedClrFromResuming | _haveResetBitRegs_T_3; // @[Debug.scala 1296:91]
  wire [1:0] _haltedBitRegs_T_3 = ~_haltedBitRegs_T_2; // @[Debug.scala 1296:67]
  wire [1:0] _haltedBitRegs_T_4 = _haltedBitRegs_T & _haltedBitRegs_T_3; // @[Debug.scala 1296:65]
  wire [1:0] _resumeReqRegs_T_1 = resumeReqRegs | resumeReqSetFromRg_1; // @[Debug.scala 1297:41]
  wire [1:0] resumeReqSetFromOuter = _T ? 2'h0 : _GEN_107; // @[Debug.scala 1278:45]
  wire [1:0] _resumeReqRegs_T_2 = _resumeReqRegs_T_1 | resumeReqSetFromOuter; // @[Debug.scala 1297:74]
  wire [1:0] _resumeReqRegs_T_3 = _resumeReqRegs_T_2 & haltedBitRegs; // @[Debug.scala 1297:99]
  reg  SBCSFieldsReg_sbbusyerror; // @[SBA.scala 44:28]
  reg  SBCSFieldsReg_sbbusy; // @[SBA.scala 44:28]
  reg  SBCSFieldsReg_sbreadonaddr; // @[SBA.scala 44:28]
  reg [2:0] SBCSFieldsReg_sbaccess; // @[SBA.scala 44:28]
  reg  SBCSFieldsReg_sbautoincrement; // @[SBA.scala 44:28]
  reg  SBCSFieldsReg_sbreadondata; // @[SBA.scala 44:28]
  wire  SBCSFieldsRegReset_sbbusy = sb2tlOpt_io_sbStateOut != 3'h0; // @[SBA.scala 48:67]
  reg [31:0] SBADDRESSFieldsReg_0; // @[SBA.scala 101:33]
  reg [31:0] SBADDRESSFieldsReg_1; // @[SBA.scala 101:33]
  wire [127:0] _autoIncrementedAddr_T = {64'h0,SBADDRESSFieldsReg_1,SBADDRESSFieldsReg_0}; // @[Cat.scala 30:58]
  wire [7:0] _autoIncrementedAddr_T_1 = 8'h1 << SBCSFieldsReg_sbaccess; // @[SBA.scala 108:67]
  wire [127:0] _GEN_89 = {{120'd0}, _autoIncrementedAddr_T_1}; // @[SBA.scala 108:60]
  wire [127:0] autoIncrementedAddr = _autoIncrementedAddr_T + _GEN_89; // @[SBA.scala 108:60]
  reg  sbErrorReg_2; // @[SBA.scala 216:25]
  reg  sbErrorReg_1; // @[SBA.scala 216:25]
  reg  sbErrorReg_0; // @[SBA.scala 216:25]
  wire [3:0] _SBCSRdData_sberror_T = {1'h0,sbErrorReg_2,sbErrorReg_1,sbErrorReg_0}; // @[SBA.scala 237:42]
  wire [2:0] SBCSRdData_sberror = _SBCSRdData_sberror_T[2:0];
  wire  _SBADDRESSFieldsReg_0_T = SBCSRdData_sberror == 3'h0; // @[SBA.scala 116:40]
  wire  out_backSel_57 = _out_backSel_T[57]; // @[Debug.scala 721:31]
  wire  out_woready__34 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_57 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_34 = out_woready__34 & out_womask_113; // @[Debug.scala 721:31]
  wire  _SBADDRESSFieldsReg_0_T_2 = ~SBCSFieldsReg_sbbusy; // @[SBA.scala 116:63]
  wire  _SBADDRESSFieldsReg_0_T_4 = ~SBCSFieldsReg_sbbusyerror; // @[SBA.scala 116:88]
  wire  _SBADDRESSFieldsReg_0_T_6 = sb2tlOpt_io_rdDone | sb2tlOpt_io_wrDone; // @[SBA.scala 117:44]
  wire [31:0] SBADDRESSWrData_0 = out_f_woready_34 ? auto_dmi_in_a_bits_data : 32'h0; // @[Debug.scala 273:24 Debug.scala 273:30]
  wire  out_backSel_58 = _out_backSel_T[58]; // @[Debug.scala 721:31]
  wire  out_woready__114 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_58 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_114 = out_woready__114 & out_womask_113; // @[Debug.scala 721:31]
  wire [127:0] _sb2tlOpt_io_addrIn_T = {64'h0,SBADDRESSFieldsReg_1,SBADDRESSWrData_0}; // @[Cat.scala 30:58]
  wire  anyAddressWrEn = out_f_woready_34 | out_f_woready_114; // @[SBA.scala 131:54]
  reg [7:0] SBDATAFieldsReg_0_0; // @[SBA.scala 140:30]
  reg [7:0] SBDATAFieldsReg_0_1; // @[SBA.scala 140:30]
  reg [7:0] SBDATAFieldsReg_0_2; // @[SBA.scala 140:30]
  reg [7:0] SBDATAFieldsReg_0_3; // @[SBA.scala 140:30]
  reg [7:0] SBDATAFieldsReg_1_0; // @[SBA.scala 140:30]
  reg [7:0] SBDATAFieldsReg_1_1; // @[SBA.scala 140:30]
  reg [7:0] SBDATAFieldsReg_1_2; // @[SBA.scala 140:30]
  reg [7:0] SBDATAFieldsReg_1_3; // @[SBA.scala 140:30]
  wire  out_backSel_60 = _out_backSel_T[60]; // @[Debug.scala 721:31]
  wire  out_woready__36 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_60 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_36 = out_woready__36 & out_womask_113; // @[Debug.scala 721:31]
  wire  _SBDATAFieldsReg_0_0_T_5 = out_f_woready_36 & _SBADDRESSFieldsReg_0_T_2 & _SBADDRESSFieldsReg_0_T_4 &
    _SBADDRESSFieldsReg_0_T; // @[SBA.scala 157:94]
  wire [31:0] SBDATAWrData_0 = out_f_woready_36 ? auto_dmi_in_a_bits_data : 32'h0; // @[Debug.scala 273:24 Debug.scala 273:30]
  wire [31:0] SBDATARdData_0 = {SBDATAFieldsReg_0_3,SBDATAFieldsReg_0_2,SBDATAFieldsReg_0_1,SBDATAFieldsReg_0_0}; // @[Cat.scala 30:58]
  wire  out_backSel_61 = _out_backSel_T[61]; // @[Debug.scala 721:31]
  wire  out_woready__35 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_61 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_woready_35 = out_woready__35 & out_womask_113; // @[Debug.scala 721:31]
  wire [31:0] SBDATAWrData_1 = out_f_woready_35 ? auto_dmi_in_a_bits_data : 32'h0; // @[Debug.scala 273:24 Debug.scala 273:30]
  wire [31:0] SBDATARdData_1 = {SBDATAFieldsReg_1_3,SBDATAFieldsReg_1_2,SBDATAFieldsReg_1_1,SBDATAFieldsReg_1_0}; // @[Cat.scala 30:58]
  wire [127:0] _sb2tlOpt_io_dataIn_T = {64'h0,SBDATAWrData_1,SBDATAWrData_0}; // @[Cat.scala 30:58]
  wire [127:0] _sb2tlOpt_io_dataIn_T_1 = {64'h0,SBDATAFieldsReg_1_3,SBDATAFieldsReg_1_2,SBDATAFieldsReg_1_1,
    SBDATAFieldsReg_1_0,SBDATAFieldsReg_0_3,SBDATAFieldsReg_0_2,SBDATAFieldsReg_0_1,SBDATAFieldsReg_0_0}; // @[Cat.scala 30:58]
  wire  out_roready__36 = _out_rofireMux_T_1 & out_backSel_60 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_romask_36 = |out_backMask; // @[Debug.scala 721:31]
  wire  out_f_roready_36 = out_roready__36 & out_romask_36; // @[Debug.scala 721:31]
  wire  out_roready__35 = _out_rofireMux_T_1 & out_backSel_61 & _out_T_14; // @[Debug.scala 721:31]
  wire  out_f_roready_35 = out_roready__35 & out_romask_36; // @[Debug.scala 721:31]
  wire  anyDataRdEn = out_f_roready_36 | out_f_roready_35; // @[SBA.scala 173:51]
  wire  anyDataWrEn = out_f_woready_36 | out_f_woready_35; // @[SBA.scala 174:51]
  wire  tryRdEn = out_f_woready_34 & SBCSFieldsReg_sbreadonaddr | out_f_roready_36 & SBCSFieldsReg_sbreadondata; // @[SBA.scala 177:68]
  wire  _sbAccessError_T_3 = SBCSFieldsReg_sbaccess == 3'h1; // @[SBA.scala 180:49]
  wire  _sbAccessError_T_7 = SBCSFieldsReg_sbaccess == 3'h2; // @[SBA.scala 181:49]
  wire  _sbAccessError_T_11 = SBCSFieldsReg_sbaccess == 3'h3; // @[SBA.scala 182:49]
  wire  _sbAccessError_T_15 = SBCSFieldsReg_sbaccess == 3'h4; // @[SBA.scala 183:49]
  wire  _sbAccessError_T_19 = SBCSFieldsReg_sbaccess > 3'h4; // @[SBA.scala 183:124]
  wire  sbAccessError = _sbAccessError_T_15 | _sbAccessError_T_19; // @[SBA.scala 183:97]
  wire [31:0] compareAddr = out_f_woready_34 ? SBADDRESSWrData_0 : SBADDRESSFieldsReg_0; // @[SBA.scala 186:23]
  wire  _sbAlignmentError_T_7 = _sbAccessError_T_7 & compareAddr[1:0] != 2'h0; // @[SBA.scala 189:61]
  wire  _sbAlignmentError_T_8 = _sbAccessError_T_3 & compareAddr[0] | _sbAlignmentError_T_7; // @[SBA.scala 188:91]
  wire  _sbAlignmentError_T_12 = _sbAccessError_T_11 & compareAddr[2:0] != 3'h0; // @[SBA.scala 190:61]
  wire  _sbAlignmentError_T_13 = _sbAlignmentError_T_8 | _sbAlignmentError_T_12; // @[SBA.scala 189:91]
  wire  _sbAlignmentError_T_17 = _sbAccessError_T_15 & compareAddr[3:0] != 4'h0; // @[SBA.scala 191:61]
  wire  sbAlignmentError = _sbAlignmentError_T_13 | _sbAlignmentError_T_17; // @[SBA.scala 190:91]
  wire  _sb2tlOpt_io_wrEn_T_7 = ~sbAccessError; // @[SBA.scala 196:141]
  wire  _sb2tlOpt_io_wrEn_T_9 = ~sbAlignmentError; // @[SBA.scala 196:159]
  wire  out_backSel_56 = _out_backSel_T[56]; // @[Debug.scala 721:31]
  wire  out_woready__16 = auto_dmi_in_a_valid & auto_dmi_in_d_ready & ~in_bits_read & out_backSel_56 & _out_T_14; // @[Debug.scala 721:31]
  wire  _out_womask_T_16 = out_backMask[22]; // @[Debug.scala 721:31]
  wire  out_womask_16 = &out_backMask[22]; // @[Debug.scala 721:31]
  wire  out_f_woready_16 = out_woready__16 & out_womask_16; // @[Debug.scala 721:31]
  wire  SBCSWrData_sbbusyerror = auto_dmi_in_a_bits_data[22]; // @[Debug.scala 721:31]
  wire  _SBCSFieldsReg_sbbusyerror_T_4 = (anyDataRdEn | anyDataWrEn) & SBCSFieldsRegReset_sbbusy |
    SBCSFieldsReg_sbbusyerror; // @[SBA.scala 207:43]
  wire  _SBCSFieldsReg_sbbusyerror_T_5 = anyAddressWrEn & SBCSFieldsRegReset_sbbusy | _SBCSFieldsReg_sbbusyerror_T_4; // @[SBA.scala 206:43]
  wire  _out_womask_T_14 = out_backMask[20]; // @[Debug.scala 721:31]
  wire  out_womask_14 = &out_backMask[20]; // @[Debug.scala 721:31]
  wire  out_f_woready_14 = out_woready__16 & out_womask_14; // @[Debug.scala 721:31]
  wire  SBCSWrData_sbreadonaddr = auto_dmi_in_a_bits_data[20]; // @[Debug.scala 721:31]
  wire  _out_womask_T_12 = out_backMask[16]; // @[Debug.scala 721:31]
  wire  out_womask_12 = &out_backMask[16]; // @[Debug.scala 721:31]
  wire  out_f_woready_12 = out_woready__16 & out_womask_12; // @[Debug.scala 721:31]
  wire  SBCSWrData_sbautoincrement = auto_dmi_in_a_bits_data[16]; // @[Debug.scala 721:31]
  wire  _out_womask_T_11 = out_backMask[15]; // @[Debug.scala 721:31]
  wire  out_womask_11 = &out_backMask[15]; // @[Debug.scala 721:31]
  wire  out_f_woready_11 = out_woready__16 & out_womask_11; // @[Debug.scala 721:31]
  wire  SBCSWrData_sbreadondata = auto_dmi_in_a_bits_data[15]; // @[Debug.scala 721:31]
  wire [2:0] _out_womask_T_13 = out_backMask[19:17]; // @[Debug.scala 721:31]
  wire  out_womask_13 = &out_backMask[19:17]; // @[Debug.scala 721:31]
  wire  out_f_woready_13 = out_woready__16 & out_womask_13; // @[Debug.scala 721:31]
  wire [2:0] SBCSWrData_sbaccess = auto_dmi_in_a_bits_data[19:17]; // @[Debug.scala 721:31]
  wire [2:0] SBCSWrData_sberror = auto_dmi_in_a_bits_data[14:12]; // @[Debug.scala 721:31]
  wire [2:0] _out_womask_T_10 = out_backMask[14:12]; // @[Debug.scala 721:31]
  wire  out_womask_10 = &out_backMask[14:12]; // @[Debug.scala 721:31]
  wire  out_f_woready_10 = out_woready__16 & out_womask_10; // @[Debug.scala 721:31]
  wire  _sbErrorReg_0_T_8 = out_f_woready_36 | tryRdEn; // @[SBA.scala 224:39]
  wire  _sbErrorReg_0_T_14 = _SBADDRESSFieldsReg_0_T_6 & sb2tlOpt_io_respError | sbErrorReg_0; // @[SBA.scala 226:29]
  wire  _sbErrorReg_0_T_15 = _sbErrorReg_0_T_8 & sbAccessError ? 1'h0 : _sbErrorReg_0_T_14; // @[SBA.scala 225:29]
  wire  _sbErrorReg_0_T_16 = (out_f_woready_36 | tryRdEn) & sbAlignmentError | _sbErrorReg_0_T_15; // @[SBA.scala 224:29]
  wire  _sbErrorReg_1_T_14 = _SBADDRESSFieldsReg_0_T_6 & sb2tlOpt_io_respError | sbErrorReg_1; // @[SBA.scala 226:29]
  wire  _sbErrorReg_1_T_15 = _sbErrorReg_0_T_8 & sbAccessError ? 1'h0 : _sbErrorReg_1_T_14; // @[SBA.scala 225:29]
  wire  _sbErrorReg_1_T_16 = (out_f_woready_36 | tryRdEn) & sbAlignmentError | _sbErrorReg_1_T_15; // @[SBA.scala 224:29]
  wire  _sbErrorReg_1_T_17 = sb2tlOpt_io_wrEn & ~sb2tlOpt_io_wrLegal | sb2tlOpt_io_rdEn & ~sb2tlOpt_io_rdLegal |
    _sbErrorReg_1_T_16; // @[SBA.scala 223:29]
  wire  _sbErrorReg_2_T_14 = _SBADDRESSFieldsReg_0_T_6 & sb2tlOpt_io_respError | sbErrorReg_2; // @[SBA.scala 226:29]
  wire  _sbErrorReg_2_T_15 = _sbErrorReg_0_T_8 & sbAccessError | _sbErrorReg_2_T_14; // @[SBA.scala 225:29]
  wire [31:0] out_prepend_2 = {abstractDataMem_7,abstractDataMem_6,abstractDataMem_5,abstractDataMem_4}; // @[Cat.scala 30:58]
  wire [23:0] out_prepend_15 = {1'h0,SBCSFieldsReg_sbbusyerror,SBCSFieldsRegReset_sbbusy,SBCSFieldsReg_sbreadonaddr,
    SBCSFieldsReg_sbaccess,SBCSFieldsReg_sbautoincrement,SBCSFieldsReg_sbreadondata,SBCSRdData_sberror,12'h4af}; // @[Cat.scala 30:58]
  wire [28:0] out_prepend_lo_16 = {{5'd0}, out_prepend_15}; // @[Debug.scala 721:31]
  wire [31:0] out_prepend_16 = {3'h1,out_prepend_lo_16}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_19 = {programBufferMem_43,programBufferMem_42,programBufferMem_41,programBufferMem_40}; // @[Cat.scala 30:58]
  wire [1:0] out_prepend_lo_20 = ABSTRACTAUTOReg_autoexecdata[1:0]; // @[Debug.scala 721:31]
  wire [2:0] out_prepend_20 = {1'h0,out_prepend_lo_20}; // @[Cat.scala 30:58]
  wire [15:0] out_prepend_lo_21 = {{13'd0}, out_prepend_20}; // @[Debug.scala 721:31]
  wire [31:0] out_prepend_21 = {ABSTRACTAUTOReg_autoexecprogbuf,out_prepend_lo_21}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_24 = {programBufferMem_23,programBufferMem_22,programBufferMem_21,programBufferMem_20}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_27 = {programBufferMem_59,programBufferMem_58,programBufferMem_57,programBufferMem_56}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_30 = {programBufferMem_27,programBufferMem_26,programBufferMem_25,programBufferMem_24}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_33 = {programBufferMem_7,programBufferMem_6,programBufferMem_5,programBufferMem_4}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_36 = {programBufferMem_39,programBufferMem_38,programBufferMem_37,programBufferMem_36}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_39 = {programBufferMem_3,programBufferMem_2,programBufferMem_1,programBufferMem_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_42 = {programBufferMem_11,programBufferMem_10,programBufferMem_9,programBufferMem_8}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_45 = {programBufferMem_55,programBufferMem_54,programBufferMem_53,programBufferMem_52}; // @[Cat.scala 30:58]
  wire [16:0] out_prepend_58 = {DMSTATUSRdData_anyresumeack,DMSTATUSRdData_allnonexistent,DMSTATUSRdData_anynonexistent,
    DMSTATUSRdData_allunavail,1'h0,DMSTATUSRdData_allrunning,DMSTATUSRdData_anyrunning,DMSTATUSRdData_allhalted,
    DMSTATUSRdData_anyhalted,8'ha2}; // @[Cat.scala 30:58]
  wire [20:0] out_prepend_62 = {1'h0,DMSTATUSRdData_allhavereset,DMSTATUSRdData_anyhavereset,DMSTATUSRdData_allresumeack
    ,out_prepend_58}; // @[Cat.scala 30:58]
  wire [21:0] out_prepend_lo_63 = {{1'd0}, out_prepend_62}; // @[Debug.scala 721:31]
  wire [22:0] out_prepend_63 = {1'h0,out_prepend_lo_63}; // @[Cat.scala 30:58]
  wire  abstractCommandBusy = ctrlStateReg != 2'h0; // @[Debug.scala 1693:42]
  wire [13:0] out_prepend_68 = {1'h0,abstractCommandBusy,1'h0,ABSTRACTCSReg_cmderr,8'h2}; // @[Cat.scala 30:58]
  wire [23:0] out_prepend_lo_69 = {{10'd0}, out_prepend_68}; // @[Debug.scala 721:31]
  wire [28:0] out_prepend_69 = {5'h10,out_prepend_lo_69}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_72 = {programBufferMem_51,programBufferMem_50,programBufferMem_49,programBufferMem_48}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_75 = {programBufferMem_31,programBufferMem_30,programBufferMem_29,programBufferMem_28}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_78 = {programBufferMem_15,programBufferMem_14,programBufferMem_13,programBufferMem_12}; // @[Cat.scala 30:58]
  wire [4:0] DMCS2RdData_group = {{4'd0}, _DMCS2RdData_group_T_2};
  wire [7:0] out_prepend_81 = {1'h0,DMCS2RdData_group,2'h0}; // @[Cat.scala 30:58]
  wire [10:0] out_prepend_lo_82 = {{3'd0}, out_prepend_81}; // @[Debug.scala 721:31]
  wire [11:0] out_prepend_82 = {grouptype,out_prepend_lo_82}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_85 = {programBufferMem_47,programBufferMem_46,programBufferMem_45,programBufferMem_44}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_88 = {programBufferMem_35,programBufferMem_34,programBufferMem_33,programBufferMem_32}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_91 = {programBufferMem_19,programBufferMem_18,programBufferMem_17,programBufferMem_16}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_94 = {abstractDataMem_3,abstractDataMem_2,abstractDataMem_1,abstractDataMem_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_97 = {programBufferMem_63,programBufferMem_62,programBufferMem_61,programBufferMem_60}; // @[Cat.scala 30:58]
  wire  _out_out_bits_data_T = 6'h0 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_1 = 6'h4 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_2 = 6'h5 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_3 = 6'h11 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_4 = 6'h13 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_5 = 6'h16 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_6 = 6'h17 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_7 = 6'h18 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_8 = 6'h20 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_9 = 6'h21 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_10 = 6'h22 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_11 = 6'h23 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_12 = 6'h24 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_13 = 6'h25 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_14 = 6'h26 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_15 = 6'h27 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_16 = 6'h28 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_17 = 6'h29 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_18 = 6'h2a == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_19 = 6'h2b == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_20 = 6'h2c == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_21 = 6'h2d == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_22 = 6'h2e == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_23 = 6'h2f == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_24 = 6'h32 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_25 = 6'h38 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_26 = 6'h39 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_27 = 6'h3a == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_28 = 6'h3c == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_29 = 6'h3d == out_iindex; // @[Conditional.scala 37:30]
  wire  _GEN_488 = _out_out_bits_data_T_29 ? _out_T_14 : 1'h1; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_489 = _out_out_bits_data_T_28 ? _out_T_14 : _GEN_488; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_490 = _out_out_bits_data_T_27 ? _out_T_14 : _GEN_489; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_491 = _out_out_bits_data_T_26 ? _out_T_14 : _GEN_490; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_492 = _out_out_bits_data_T_25 ? _out_T_14 : _GEN_491; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_493 = _out_out_bits_data_T_24 ? _out_T_14 : _GEN_492; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_494 = _out_out_bits_data_T_23 ? _out_T_14 : _GEN_493; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_495 = _out_out_bits_data_T_22 ? _out_T_14 : _GEN_494; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_496 = _out_out_bits_data_T_21 ? _out_T_14 : _GEN_495; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_497 = _out_out_bits_data_T_20 ? _out_T_14 : _GEN_496; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_498 = _out_out_bits_data_T_19 ? _out_T_14 : _GEN_497; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_499 = _out_out_bits_data_T_18 ? _out_T_14 : _GEN_498; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_500 = _out_out_bits_data_T_17 ? _out_T_14 : _GEN_499; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_501 = _out_out_bits_data_T_16 ? _out_T_14 : _GEN_500; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_502 = _out_out_bits_data_T_15 ? _out_T_14 : _GEN_501; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_503 = _out_out_bits_data_T_14 ? _out_T_14 : _GEN_502; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_504 = _out_out_bits_data_T_13 ? _out_T_14 : _GEN_503; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_505 = _out_out_bits_data_T_12 ? _out_T_14 : _GEN_504; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_506 = _out_out_bits_data_T_11 ? _out_T_14 : _GEN_505; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_507 = _out_out_bits_data_T_10 ? _out_T_14 : _GEN_506; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_508 = _out_out_bits_data_T_9 ? _out_T_14 : _GEN_507; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_509 = _out_out_bits_data_T_8 ? _out_T_14 : _GEN_508; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_510 = _out_out_bits_data_T_7 ? _out_T_14 : _GEN_509; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_511 = _out_out_bits_data_T_6 ? _out_T_14 : _GEN_510; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_512 = _out_out_bits_data_T_5 ? _out_T_14 : _GEN_511; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_513 = _out_out_bits_data_T_4 ? _out_T_14 : _GEN_512; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_514 = _out_out_bits_data_T_3 ? _out_T_14 : _GEN_513; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_515 = _out_out_bits_data_T_2 ? _out_T_14 : _GEN_514; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_516 = _out_out_bits_data_T_1 ? _out_T_14 : _GEN_515; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  out_out_bits_data_out = _out_out_bits_data_T ? _out_T_30 : _GEN_516; // @[Conditional.scala 40:58 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_518 = _out_out_bits_data_T_29 ? SBDATARdData_1 : 32'h0; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_519 = _out_out_bits_data_T_28 ? SBDATARdData_0 : _GEN_518; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_520 = _out_out_bits_data_T_27 ? SBADDRESSFieldsReg_1 : _GEN_519; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_521 = _out_out_bits_data_T_26 ? SBADDRESSFieldsReg_0 : _GEN_520; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_522 = _out_out_bits_data_T_25 ? out_prepend_16 : _GEN_521; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_523 = _out_out_bits_data_T_24 ? {{20'd0}, out_prepend_82} : _GEN_522; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_524 = _out_out_bits_data_T_23 ? out_prepend_97 : _GEN_523; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_525 = _out_out_bits_data_T_22 ? out_prepend_27 : _GEN_524; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_526 = _out_out_bits_data_T_21 ? out_prepend_45 : _GEN_525; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_527 = _out_out_bits_data_T_20 ? out_prepend_72 : _GEN_526; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_528 = _out_out_bits_data_T_19 ? out_prepend_85 : _GEN_527; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_529 = _out_out_bits_data_T_18 ? out_prepend_19 : _GEN_528; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_530 = _out_out_bits_data_T_17 ? out_prepend_36 : _GEN_529; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_531 = _out_out_bits_data_T_16 ? out_prepend_88 : _GEN_530; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_532 = _out_out_bits_data_T_15 ? out_prepend_75 : _GEN_531; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_533 = _out_out_bits_data_T_14 ? out_prepend_30 : _GEN_532; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_534 = _out_out_bits_data_T_13 ? out_prepend_24 : _GEN_533; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_535 = _out_out_bits_data_T_12 ? out_prepend_91 : _GEN_534; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_536 = _out_out_bits_data_T_11 ? out_prepend_78 : _GEN_535; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_537 = _out_out_bits_data_T_10 ? out_prepend_42 : _GEN_536; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_538 = _out_out_bits_data_T_9 ? out_prepend_33 : _GEN_537; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_539 = _out_out_bits_data_T_8 ? out_prepend_39 : _GEN_538; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_540 = _out_out_bits_data_T_7 ? out_prepend_21 : _GEN_539; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_541 = _out_out_bits_data_T_6 ? _accessRegisterCommandReg_T : _GEN_540; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_542 = _out_out_bits_data_T_5 ? {{3'd0}, out_prepend_69} : _GEN_541; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_543 = _out_out_bits_data_T_4 ? HALTSUM1RdData_haltsum1 : _GEN_542; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_544 = _out_out_bits_data_T_3 ? {{9'd0}, out_prepend_63} : _GEN_543; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_545 = _out_out_bits_data_T_2 ? out_prepend_2 : _GEN_544; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_546 = _out_out_bits_data_T_1 ? out_prepend_94 : _GEN_545; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [31:0] out_out_bits_data_out_1 = _out_out_bits_data_T ? haltedStatus_0 : _GEN_546; // @[Conditional.scala 40:58 MuxLiteral.scala 53:32]
  reg  goReg; // @[Debug.scala 1447:27]
  wire  out_f_woready_212 = out_woready_1_83 & out_womask_177; // @[Debug.scala 733:30]
  wire  _GEN_620 = out_f_woready_212 ? 1'h0 : goReg; // @[Debug.scala 1458:33 Debug.scala 1460:15 Debug.scala 1447:27]
  wire  _GEN_1869 = commandRegBadHaltResume ? 1'h0 : 1'h1; // @[Debug.scala 1760:43]
  wire  _GEN_1873 = commandRegIsUnsupported ? 1'h0 : _GEN_1869; // @[Debug.scala 1757:38]
  wire  _GEN_1886 = _T_475 & _GEN_1873; // @[Debug.scala 1750:59]
  wire  goAbstract = ABSTRACTCSWrEnLegal ? 1'h0 : _GEN_1886; // @[Debug.scala 1742:47]
  wire  _GEN_621 = goAbstract | _GEN_620; // @[Debug.scala 1456:25 Debug.scala 1457:15]
  wire  flags_0_go = 2'h0 == selectedHartReg & goReg; // @[Debug.scala 1473:61 Debug.scala 1473:61]
  wire  flags_1_go = 2'h1 == selectedHartReg & goReg; // @[Debug.scala 1473:61 Debug.scala 1473:61]
  wire  accessRegisterCommandReg_postexec = _accessRegisterCommandReg_T[18]; // @[Debug.scala 1485:73]
  reg [31:0] abstractGeneratedMem_0; // @[Debug.scala 1538:35]
  reg [31:0] abstractGeneratedMem_1; // @[Debug.scala 1538:35]
  wire [15:0] _abstractGeneratedMem_0_inst_rd_T = accessRegisterCommandReg_regno & 16'h1f; // @[Debug.scala 1545:54]
  wire [4:0] abstractGeneratedMem_0_inst_rd = _abstractGeneratedMem_0_inst_rd_T[4:0]; // @[Debug.scala 1541:22 Debug.scala 1545:19]
  wire [31:0] _abstractGeneratedMem_0_T = {17'h7000,accessRegisterCommandReg_size,abstractGeneratedMem_0_inst_rd,7'h3}; // @[Debug.scala 1549:12]
  wire [31:0] _abstractGeneratedMem_0_T_1 = {7'h1c,abstractGeneratedMem_0_inst_rd,5'h0,accessRegisterCommandReg_size,5'h0
    ,7'h23}; // @[Debug.scala 1562:12]
  wire [7:0] _out_rimask_T_128 = out_backMask_1[7:0]; // @[Debug.scala 733:30]
  wire [7:0] _out_rimask_T_129 = out_backMask_1[15:8]; // @[Debug.scala 733:30]
  wire [7:0] _out_rimask_T_130 = out_backMask_1[23:16]; // @[Debug.scala 733:30]
  wire [7:0] _out_rimask_T_131 = out_backMask_1[31:24]; // @[Debug.scala 733:30]
  wire [7:0] _out_rimask_T_132 = out_backMask_1[39:32]; // @[Debug.scala 733:30]
  wire [7:0] _out_rimask_T_133 = out_backMask_1[47:40]; // @[Debug.scala 733:30]
  wire [7:0] _out_rimask_T_134 = out_backMask_1[55:48]; // @[Debug.scala 733:30]
  wire [7:0] _out_rimask_T_135 = out_backMask_1[63:56]; // @[Debug.scala 733:30]
  wire  out_wimask_136 = &out_backMask_1[7:0]; // @[Debug.scala 733:30]
  wire  out_frontSel_110 = _out_backSel_T_1[110]; // @[Debug.scala 733:30]
  wire  out_wivalid_1_8 = _out_wofireMux_T_260 & ~in_1_bits_read & out_frontSel_110 & _out_T_2282; // @[Debug.scala 733:30]
  wire  out_f_wivalid_136 = out_wivalid_1_8 & out_wimask_136; // @[Debug.scala 733:30]
  wire  out_wimask_137 = &out_backMask_1[15:8]; // @[Debug.scala 733:30]
  wire  out_f_wivalid_137 = out_wivalid_1_8 & out_wimask_137; // @[Debug.scala 733:30]
  wire  out_wimask_138 = &out_backMask_1[23:16]; // @[Debug.scala 733:30]
  wire  out_f_wivalid_138 = out_wivalid_1_8 & out_wimask_138; // @[Debug.scala 733:30]
  wire  out_wimask_139 = &out_backMask_1[31:24]; // @[Debug.scala 733:30]
  wire  out_f_wivalid_139 = out_wivalid_1_8 & out_wimask_139; // @[Debug.scala 733:30]
  wire  out_wimask_140 = &out_backMask_1[39:32]; // @[Debug.scala 733:30]
  wire  out_f_wivalid_140 = out_wivalid_1_8 & out_wimask_140; // @[Debug.scala 733:30]
  wire  out_wimask_141 = &out_backMask_1[47:40]; // @[Debug.scala 733:30]
  wire  out_f_wivalid_141 = out_wivalid_1_8 & out_wimask_141; // @[Debug.scala 733:30]
  wire  out_wimask_142 = &out_backMask_1[55:48]; // @[Debug.scala 733:30]
  wire  out_f_wivalid_142 = out_wivalid_1_8 & out_wimask_142; // @[Debug.scala 733:30]
  wire  out_wimask_143 = &out_backMask_1[63:56]; // @[Debug.scala 733:30]
  wire  out_f_wivalid_143 = out_wivalid_1_8 & out_wimask_143; // @[Debug.scala 733:30]
  wire [63:0] out_prepend_111 = {programBufferMem_55,programBufferMem_54,programBufferMem_53,programBufferMem_52,
    programBufferMem_51,programBufferMem_50,programBufferMem_49,programBufferMem_48}; // @[Cat.scala 30:58]
  wire  out_frontSel_106 = _out_backSel_T_1[106]; // @[Debug.scala 733:30]
  wire  out_wivalid_1_24 = _out_wofireMux_T_260 & ~in_1_bits_read & out_frontSel_106 & _out_T_2282; // @[Debug.scala 733:30]
  wire  out_f_wivalid_152 = out_wivalid_1_24 & out_wimask_136; // @[Debug.scala 733:30]
  wire  out_f_wivalid_153 = out_wivalid_1_24 & out_wimask_137; // @[Debug.scala 733:30]
  wire  out_f_wivalid_154 = out_wivalid_1_24 & out_wimask_138; // @[Debug.scala 733:30]
  wire  out_f_wivalid_155 = out_wivalid_1_24 & out_wimask_139; // @[Debug.scala 733:30]
  wire  out_f_wivalid_156 = out_wivalid_1_24 & out_wimask_140; // @[Debug.scala 733:30]
  wire  out_f_wivalid_157 = out_wivalid_1_24 & out_wimask_141; // @[Debug.scala 733:30]
  wire  out_f_wivalid_158 = out_wivalid_1_24 & out_wimask_142; // @[Debug.scala 733:30]
  wire  out_f_wivalid_159 = out_wivalid_1_24 & out_wimask_143; // @[Debug.scala 733:30]
  wire [63:0] out_prepend_125 = {programBufferMem_23,programBufferMem_22,programBufferMem_21,programBufferMem_20,
    programBufferMem_19,programBufferMem_18,programBufferMem_17,programBufferMem_16}; // @[Cat.scala 30:58]
  wire  out_frontSel_109 = _out_backSel_T_1[109]; // @[Debug.scala 733:30]
  wire  out_wivalid_1_50 = _out_wofireMux_T_260 & ~in_1_bits_read & out_frontSel_109 & _out_T_2282; // @[Debug.scala 733:30]
  wire  out_f_wivalid_178 = out_wivalid_1_50 & out_wimask_136; // @[Debug.scala 733:30]
  wire  out_f_wivalid_179 = out_wivalid_1_50 & out_wimask_137; // @[Debug.scala 733:30]
  wire  out_f_wivalid_180 = out_wivalid_1_50 & out_wimask_138; // @[Debug.scala 733:30]
  wire  out_f_wivalid_181 = out_wivalid_1_50 & out_wimask_139; // @[Debug.scala 733:30]
  wire  out_f_wivalid_182 = out_wivalid_1_50 & out_wimask_140; // @[Debug.scala 733:30]
  wire  out_f_wivalid_183 = out_wivalid_1_50 & out_wimask_141; // @[Debug.scala 733:30]
  wire  out_f_wivalid_184 = out_wivalid_1_50 & out_wimask_142; // @[Debug.scala 733:30]
  wire  out_f_wivalid_185 = out_wivalid_1_50 & out_wimask_143; // @[Debug.scala 733:30]
  wire [63:0] out_prepend_147 = {programBufferMem_47,programBufferMem_46,programBufferMem_45,programBufferMem_44,
    programBufferMem_43,programBufferMem_42,programBufferMem_41,programBufferMem_40}; // @[Cat.scala 30:58]
  wire [16:0] out_prepend_156 = {1'h0,6'h0,resumeReqRegs[1],flags_1_go,6'h0,resumeReqRegs[0],flags_0_go}; // @[Cat.scala 30:58]
  wire [23:0] out_prepend_lo_157 = {{7'd0}, out_prepend_156}; // @[Debug.scala 733:30]
  wire [24:0] out_prepend_157 = {1'h0,out_prepend_lo_157}; // @[Cat.scala 30:58]
  wire [31:0] _out_T_3373 = {{7'd0}, out_prepend_157}; // @[Debug.scala 733:30]
  wire  out_frontSel_105 = _out_backSel_T_1[105]; // @[Debug.scala 733:30]
  wire  out_wivalid_1_71 = _out_wofireMux_T_260 & ~in_1_bits_read & out_frontSel_105 & _out_T_2282; // @[Debug.scala 733:30]
  wire  out_f_wivalid_199 = out_wivalid_1_71 & out_wimask_136; // @[Debug.scala 733:30]
  wire  out_f_wivalid_200 = out_wivalid_1_71 & out_wimask_137; // @[Debug.scala 733:30]
  wire  out_f_wivalid_201 = out_wivalid_1_71 & out_wimask_138; // @[Debug.scala 733:30]
  wire  out_f_wivalid_202 = out_wivalid_1_71 & out_wimask_139; // @[Debug.scala 733:30]
  wire  out_f_wivalid_203 = out_wivalid_1_71 & out_wimask_140; // @[Debug.scala 733:30]
  wire  out_f_wivalid_204 = out_wivalid_1_71 & out_wimask_141; // @[Debug.scala 733:30]
  wire  out_f_wivalid_205 = out_wivalid_1_71 & out_wimask_142; // @[Debug.scala 733:30]
  wire  out_f_wivalid_206 = out_wivalid_1_71 & out_wimask_143; // @[Debug.scala 733:30]
  wire [63:0] out_prepend_164 = {programBufferMem_15,programBufferMem_14,programBufferMem_13,programBufferMem_12,
    programBufferMem_11,programBufferMem_10,programBufferMem_9,programBufferMem_8}; // @[Cat.scala 30:58]
  wire [63:0] out_prepend_183 = {abstractGeneratedMem_1,abstractGeneratedMem_0}; // @[Cat.scala 30:58]
  wire  out_frontSel_108 = _out_backSel_T_1[108]; // @[Debug.scala 733:30]
  wire  out_wivalid_1_103 = _out_wofireMux_T_260 & ~in_1_bits_read & out_frontSel_108 & _out_T_2282; // @[Debug.scala 733:30]
  wire  out_f_wivalid_231 = out_wivalid_1_103 & out_wimask_136; // @[Debug.scala 733:30]
  wire  out_f_wivalid_232 = out_wivalid_1_103 & out_wimask_137; // @[Debug.scala 733:30]
  wire  out_f_wivalid_233 = out_wivalid_1_103 & out_wimask_138; // @[Debug.scala 733:30]
  wire  out_f_wivalid_234 = out_wivalid_1_103 & out_wimask_139; // @[Debug.scala 733:30]
  wire  out_f_wivalid_235 = out_wivalid_1_103 & out_wimask_140; // @[Debug.scala 733:30]
  wire  out_f_wivalid_236 = out_wivalid_1_103 & out_wimask_141; // @[Debug.scala 733:30]
  wire  out_f_wivalid_237 = out_wivalid_1_103 & out_wimask_142; // @[Debug.scala 733:30]
  wire  out_f_wivalid_238 = out_wivalid_1_103 & out_wimask_143; // @[Debug.scala 733:30]
  wire [63:0] out_prepend_190 = {programBufferMem_39,programBufferMem_38,programBufferMem_37,programBufferMem_36,
    programBufferMem_35,programBufferMem_34,programBufferMem_33,programBufferMem_32}; // @[Cat.scala 30:58]
  wire  out_frontSel_112 = _out_backSel_T_1[112]; // @[Debug.scala 733:30]
  wire  out_wivalid_1_111 = _out_wofireMux_T_260 & ~in_1_bits_read & out_frontSel_112 & _out_T_2282; // @[Debug.scala 733:30]
  wire  out_f_wivalid_239 = out_wivalid_1_111 & out_wimask_136; // @[Debug.scala 733:30]
  wire  out_f_wivalid_240 = out_wivalid_1_111 & out_wimask_137; // @[Debug.scala 733:30]
  wire  out_f_wivalid_241 = out_wivalid_1_111 & out_wimask_138; // @[Debug.scala 733:30]
  wire  out_f_wivalid_242 = out_wivalid_1_111 & out_wimask_139; // @[Debug.scala 733:30]
  wire  out_f_wivalid_243 = out_wivalid_1_111 & out_wimask_140; // @[Debug.scala 733:30]
  wire  out_f_wivalid_244 = out_wivalid_1_111 & out_wimask_141; // @[Debug.scala 733:30]
  wire  out_f_wivalid_245 = out_wivalid_1_111 & out_wimask_142; // @[Debug.scala 733:30]
  wire  out_f_wivalid_246 = out_wivalid_1_111 & out_wimask_143; // @[Debug.scala 733:30]
  wire [63:0] out_prepend_197 = {abstractDataMem_7,abstractDataMem_6,abstractDataMem_5,abstractDataMem_4,
    abstractDataMem_3,abstractDataMem_2,abstractDataMem_1,abstractDataMem_0}; // @[Cat.scala 30:58]
  wire  out_frontSel_104 = _out_backSel_T_1[104]; // @[Debug.scala 733:30]
  wire  out_wivalid_1_127 = _out_wofireMux_T_260 & ~in_1_bits_read & out_frontSel_104 & _out_T_2282; // @[Debug.scala 733:30]
  wire  out_f_wivalid_255 = out_wivalid_1_127 & out_wimask_136; // @[Debug.scala 733:30]
  wire  out_f_wivalid_256 = out_wivalid_1_127 & out_wimask_137; // @[Debug.scala 733:30]
  wire  out_f_wivalid_257 = out_wivalid_1_127 & out_wimask_138; // @[Debug.scala 733:30]
  wire  out_f_wivalid_258 = out_wivalid_1_127 & out_wimask_139; // @[Debug.scala 733:30]
  wire  out_f_wivalid_259 = out_wivalid_1_127 & out_wimask_140; // @[Debug.scala 733:30]
  wire  out_f_wivalid_260 = out_wivalid_1_127 & out_wimask_141; // @[Debug.scala 733:30]
  wire  out_f_wivalid_261 = out_wivalid_1_127 & out_wimask_142; // @[Debug.scala 733:30]
  wire  out_f_wivalid_262 = out_wivalid_1_127 & out_wimask_143; // @[Debug.scala 733:30]
  wire [63:0] out_prepend_211 = {programBufferMem_7,programBufferMem_6,programBufferMem_5,programBufferMem_4,
    programBufferMem_3,programBufferMem_2,programBufferMem_1,programBufferMem_0}; // @[Cat.scala 30:58]
  wire  out_frontSel_107 = _out_backSel_T_1[107]; // @[Debug.scala 733:30]
  wire  out_wivalid_1_151 = _out_wofireMux_T_260 & ~in_1_bits_read & out_frontSel_107 & _out_T_2282; // @[Debug.scala 733:30]
  wire  out_f_wivalid_279 = out_wivalid_1_151 & out_wimask_136; // @[Debug.scala 733:30]
  wire  out_f_wivalid_280 = out_wivalid_1_151 & out_wimask_137; // @[Debug.scala 733:30]
  wire  out_f_wivalid_281 = out_wivalid_1_151 & out_wimask_138; // @[Debug.scala 733:30]
  wire  out_f_wivalid_282 = out_wivalid_1_151 & out_wimask_139; // @[Debug.scala 733:30]
  wire  out_f_wivalid_283 = out_wivalid_1_151 & out_wimask_140; // @[Debug.scala 733:30]
  wire  out_f_wivalid_284 = out_wivalid_1_151 & out_wimask_141; // @[Debug.scala 733:30]
  wire  out_f_wivalid_285 = out_wivalid_1_151 & out_wimask_142; // @[Debug.scala 733:30]
  wire  out_f_wivalid_286 = out_wivalid_1_151 & out_wimask_143; // @[Debug.scala 733:30]
  wire [63:0] out_prepend_232 = {programBufferMem_31,programBufferMem_30,programBufferMem_29,programBufferMem_28,
    programBufferMem_27,programBufferMem_26,programBufferMem_25,programBufferMem_24}; // @[Cat.scala 30:58]
  wire  out_frontSel_111 = _out_backSel_T_1[111]; // @[Debug.scala 733:30]
  wire  out_wivalid_1_159 = _out_wofireMux_T_260 & ~in_1_bits_read & out_frontSel_111 & _out_T_2282; // @[Debug.scala 733:30]
  wire  out_f_wivalid_287 = out_wivalid_1_159 & out_wimask_136; // @[Debug.scala 733:30]
  wire  out_f_wivalid_288 = out_wivalid_1_159 & out_wimask_137; // @[Debug.scala 733:30]
  wire  out_f_wivalid_289 = out_wivalid_1_159 & out_wimask_138; // @[Debug.scala 733:30]
  wire  out_f_wivalid_290 = out_wivalid_1_159 & out_wimask_139; // @[Debug.scala 733:30]
  wire  out_f_wivalid_291 = out_wivalid_1_159 & out_wimask_140; // @[Debug.scala 733:30]
  wire  out_f_wivalid_292 = out_wivalid_1_159 & out_wimask_141; // @[Debug.scala 733:30]
  wire  out_f_wivalid_293 = out_wivalid_1_159 & out_wimask_142; // @[Debug.scala 733:30]
  wire  out_f_wivalid_294 = out_wivalid_1_159 & out_wimask_143; // @[Debug.scala 733:30]
  wire [63:0] out_prepend_239 = {programBufferMem_63,programBufferMem_62,programBufferMem_61,programBufferMem_60,
    programBufferMem_59,programBufferMem_58,programBufferMem_57,programBufferMem_56}; // @[Cat.scala 30:58]
  wire  _out_out_bits_data_T_61 = 8'h0 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_62 = 8'h20 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_63 = 8'h21 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_64 = 8'h60 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_65 = 8'h67 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_66 = 8'h68 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_67 = 8'h69 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_68 = 8'h6a == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_69 = 8'h6b == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_70 = 8'h6c == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_71 = 8'h6d == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_72 = 8'h6e == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_73 = 8'h6f == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_74 = 8'h70 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_75 = 8'h80 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_76 = 8'h81 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_77 = 8'h82 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_78 = 8'h83 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_79 = 8'h84 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_80 = 8'h85 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_81 = 8'h86 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_82 = 8'h87 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_83 = 8'h88 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_84 = 8'h89 == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_85 = 8'h8a == out_iindex_1; // @[Conditional.scala 37:30]
  wire  _GEN_1733 = _out_out_bits_data_T_85 ? _out_T_2282 : 1'h1; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1734 = _out_out_bits_data_T_84 ? _out_T_2282 : _GEN_1733; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1735 = _out_out_bits_data_T_83 ? _out_T_2282 : _GEN_1734; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1736 = _out_out_bits_data_T_82 ? _out_T_2282 : _GEN_1735; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1737 = _out_out_bits_data_T_81 ? _out_T_2282 : _GEN_1736; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1738 = _out_out_bits_data_T_80 ? _out_T_2282 : _GEN_1737; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1739 = _out_out_bits_data_T_79 ? _out_T_2282 : _GEN_1738; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1740 = _out_out_bits_data_T_78 ? _out_T_2282 : _GEN_1739; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1741 = _out_out_bits_data_T_77 ? _out_T_2282 : _GEN_1740; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1742 = _out_out_bits_data_T_76 ? _out_T_2282 : _GEN_1741; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1743 = _out_out_bits_data_T_75 ? _out_T_2282 : _GEN_1742; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1744 = _out_out_bits_data_T_74 ? _out_T_2282 : _GEN_1743; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1745 = _out_out_bits_data_T_73 ? _out_T_2282 : _GEN_1744; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1746 = _out_out_bits_data_T_72 ? _out_T_2282 : _GEN_1745; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1747 = _out_out_bits_data_T_71 ? _out_T_2282 : _GEN_1746; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1748 = _out_out_bits_data_T_70 ? _out_T_2282 : _GEN_1747; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1749 = _out_out_bits_data_T_69 ? _out_T_2282 : _GEN_1748; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1750 = _out_out_bits_data_T_68 ? _out_T_2282 : _GEN_1749; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1751 = _out_out_bits_data_T_67 ? _out_T_2282 : _GEN_1750; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1752 = _out_out_bits_data_T_66 ? _out_T_2282 : _GEN_1751; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1753 = _out_out_bits_data_T_65 ? _out_T_2282 : _GEN_1752; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1754 = _out_out_bits_data_T_64 ? _out_T_2282 : _GEN_1753; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1755 = _out_out_bits_data_T_63 ? _out_T_2282 : _GEN_1754; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_1756 = _out_out_bits_data_T_62 ? _out_T_2282 : _GEN_1755; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  out_out_bits_data_out_2 = _out_out_bits_data_T_61 ? _out_T_2278 : _GEN_1756; // @[Conditional.scala 40:58 MuxLiteral.scala 53:32]
  wire [31:0] _GEN_1758 = _out_out_bits_data_T_85 ? 32'h100073 : 32'h0; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1759 = _out_out_bits_data_T_84 ? 64'h100026237b200073 : {{32'd0}, _GEN_1758}; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1760 = _out_out_bits_data_T_83 ? 64'h7b20247310802423 : _GEN_1759; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1761 = _out_out_bits_data_T_82 ? 64'hf140247330000067 : _GEN_1760; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1762 = _out_out_bits_data_T_81 ? 64'h100022237b202473 : _GEN_1761; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1763 = _out_out_bits_data_T_80 ? 64'h4086300147413 : _GEN_1762; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1764 = _out_out_bits_data_T_79 ? 64'hfe0408e300347413 : _GEN_1763; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1765 = _out_out_bits_data_T_78 ? 64'h4004440310802023 : _GEN_1764; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1766 = _out_out_bits_data_T_77 ? 64'hf14024737b241073 : _GEN_1765; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1767 = _out_out_bits_data_T_76 ? 64'hff0000f0440006f : _GEN_1766; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1768 = _out_out_bits_data_T_75 ? 64'h380006f00c0006f : _GEN_1767; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1769 = _out_out_bits_data_T_74 ? out_prepend_197 : _GEN_1768; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1770 = _out_out_bits_data_T_73 ? out_prepend_239 : _GEN_1769; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1771 = _out_out_bits_data_T_72 ? out_prepend_111 : _GEN_1770; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1772 = _out_out_bits_data_T_71 ? out_prepend_147 : _GEN_1771; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1773 = _out_out_bits_data_T_70 ? out_prepend_190 : _GEN_1772; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1774 = _out_out_bits_data_T_69 ? out_prepend_232 : _GEN_1773; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1775 = _out_out_bits_data_T_68 ? out_prepend_125 : _GEN_1774; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1776 = _out_out_bits_data_T_67 ? out_prepend_164 : _GEN_1775; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1777 = _out_out_bits_data_T_66 ? out_prepend_211 : _GEN_1776; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1778 = _out_out_bits_data_T_65 ? out_prepend_183 : _GEN_1777; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1779 = _out_out_bits_data_T_64 ? 64'h380006f : _GEN_1778; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1780 = _out_out_bits_data_T_63 ? 64'h0 : _GEN_1779; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_1781 = _out_out_bits_data_T_62 ? 64'h0 : _GEN_1780; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] out_out_bits_data_out_3 = _out_out_bits_data_T_61 ? {{32'd0}, _out_T_3373} : _GEN_1781; // @[Conditional.scala 40:58 MuxLiteral.scala 53:32]
  wire [1:0] _GEN_1868 = commandRegBadHaltResume ? 2'h0 : 2'h2; // @[Debug.scala 1760:43 Debug.scala 1762:22]
  wire [9:0] _GEN_99 = {{8'd0}, selectedHartReg}; // @[Debug.scala 1776:95]
  wire [1:0] _GEN_1874 = ~goReg & out_f_woready_211 & hartHaltedId == _GEN_99 ? 2'h0 : ctrlStateReg; // @[Debug.scala 1776:116 Debug.scala 1777:22]
  wire [1:0] _GEN_1875 = out_f_woready_177 ? 2'h0 : _GEN_1874; // @[Debug.scala 1779:31 Debug.scala 1781:24]
  RHEA__SBToTL sb2tlOpt ( // @[Debug.scala 740:52]
    .clock(sb2tlOpt_clock),
    .reset(sb2tlOpt_reset),
    .auto_out_a_ready(sb2tlOpt_auto_out_a_ready),
    .auto_out_a_valid(sb2tlOpt_auto_out_a_valid),
    .auto_out_a_bits_opcode(sb2tlOpt_auto_out_a_bits_opcode),
    .auto_out_a_bits_size(sb2tlOpt_auto_out_a_bits_size),
    .auto_out_a_bits_address(sb2tlOpt_auto_out_a_bits_address),
    .auto_out_a_bits_data(sb2tlOpt_auto_out_a_bits_data),
    .auto_out_d_ready(sb2tlOpt_auto_out_d_ready),
    .auto_out_d_valid(sb2tlOpt_auto_out_d_valid),
    .auto_out_d_bits_denied(sb2tlOpt_auto_out_d_bits_denied),
    .auto_out_d_bits_data(sb2tlOpt_auto_out_d_bits_data),
    .auto_out_d_bits_corrupt(sb2tlOpt_auto_out_d_bits_corrupt),
    .io_rdEn(sb2tlOpt_io_rdEn),
    .io_wrEn(sb2tlOpt_io_wrEn),
    .io_addrIn(sb2tlOpt_io_addrIn),
    .io_dataIn(sb2tlOpt_io_dataIn),
    .io_sizeIn(sb2tlOpt_io_sizeIn),
    .io_rdLegal(sb2tlOpt_io_rdLegal),
    .io_wrLegal(sb2tlOpt_io_wrLegal),
    .io_rdDone(sb2tlOpt_io_rdDone),
    .io_wrDone(sb2tlOpt_io_wrDone),
    .io_respError(sb2tlOpt_io_respError),
    .io_dataOut(sb2tlOpt_io_dataOut),
    .io_rdLoad_0(sb2tlOpt_io_rdLoad_0),
    .io_rdLoad_1(sb2tlOpt_io_rdLoad_1),
    .io_rdLoad_2(sb2tlOpt_io_rdLoad_2),
    .io_rdLoad_3(sb2tlOpt_io_rdLoad_3),
    .io_rdLoad_4(sb2tlOpt_io_rdLoad_4),
    .io_rdLoad_5(sb2tlOpt_io_rdLoad_5),
    .io_rdLoad_6(sb2tlOpt_io_rdLoad_6),
    .io_rdLoad_7(sb2tlOpt_io_rdLoad_7),
    .io_sbStateOut(sb2tlOpt_io_sbStateOut),
    .rf_reset(sb2tlOpt_rf_reset)
  );
  RHEA__AsyncResetSynchronizerShiftReg_w1_d3_i0 hartIsInResetSync_0_debug_hartReset_0 ( // @[ShiftReg.scala 45:23]
    .clock(hartIsInResetSync_0_debug_hartReset_0_clock),
    .reset(hartIsInResetSync_0_debug_hartReset_0_reset),
    .io_d(hartIsInResetSync_0_debug_hartReset_0_io_d),
    .io_q(hartIsInResetSync_0_debug_hartReset_0_io_q)
  );
  RHEA__AsyncResetSynchronizerShiftReg_w1_d3_i0 hartIsInResetSync_1_debug_hartReset_1 ( // @[ShiftReg.scala 45:23]
    .clock(hartIsInResetSync_1_debug_hartReset_1_clock),
    .reset(hartIsInResetSync_1_debug_hartReset_1_reset),
    .io_d(hartIsInResetSync_1_debug_hartReset_1_io_d),
    .io_q(hartIsInResetSync_1_debug_hartReset_1_io_q)
  );
  assign auto_sb2tlOpt_out_a_valid = sb2tlOpt_auto_out_a_valid; // @[LazyModule.scala 390:12]
  assign auto_sb2tlOpt_out_a_bits_opcode = sb2tlOpt_auto_out_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_sb2tlOpt_out_a_bits_size = sb2tlOpt_auto_out_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_sb2tlOpt_out_a_bits_address = sb2tlOpt_auto_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_sb2tlOpt_out_a_bits_data = sb2tlOpt_auto_out_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_sb2tlOpt_out_d_ready = sb2tlOpt_auto_out_d_ready; // @[LazyModule.scala 390:12]
  assign auto_tl_in_a_ready = auto_tl_in_d_ready; // @[Debug.scala 733:30]
  assign auto_tl_in_d_valid = auto_tl_in_a_valid; // @[Debug.scala 733:30]
  assign auto_tl_in_d_bits_opcode = {{2'd0}, in_1_bits_read}; // @[Debug.scala 733:30 Debug.scala 733:30]
  assign auto_tl_in_d_bits_size = auto_tl_in_a_bits_size; // @[Debug.scala 733:30 LazyModule.scala 388:16]
  assign auto_tl_in_d_bits_source = auto_tl_in_a_bits_source; // @[Debug.scala 733:30 LazyModule.scala 388:16]
  assign auto_tl_in_d_bits_data = out_out_bits_data_out_2 ? out_out_bits_data_out_3 : 64'h0; // @[Debug.scala 733:30]
  assign auto_dmi_in_a_ready = auto_dmi_in_d_ready; // @[Debug.scala 721:31]
  assign auto_dmi_in_d_valid = auto_dmi_in_a_valid; // @[Debug.scala 721:31]
  assign auto_dmi_in_d_bits_opcode = {{2'd0}, in_bits_read}; // @[Debug.scala 721:31 Debug.scala 721:31]
  assign auto_dmi_in_d_bits_size = auto_dmi_in_a_bits_size; // @[Debug.scala 721:31 LazyModule.scala 388:16]
  assign auto_dmi_in_d_bits_source = auto_dmi_in_a_bits_source; // @[Debug.scala 721:31 LazyModule.scala 388:16]
  assign auto_dmi_in_d_bits_data = out_out_bits_data_out ? out_out_bits_data_out_1 : 32'h0; // @[Debug.scala 721:31]
  assign io_innerCtrl_ready = 1'h1; // @[Debug.scala 862:24]
  assign io_hgDebugInt_0 = hgDebugInt_0 | hrDebugIntReg_0; // @[package.scala 66:75]
  assign io_hgDebugInt_1 = hgDebugInt_1 | hrDebugIntReg_1; // @[package.scala 66:75]
  assign sb2tlOpt_clock = io_tl_clock; // @[Debug.scala 777:23]
  assign sb2tlOpt_reset = io_tl_reset; // @[Debug.scala 778:23]
  assign sb2tlOpt_auto_out_a_ready = auto_sb2tlOpt_out_a_ready; // @[LazyModule.scala 390:12]
  assign sb2tlOpt_auto_out_d_valid = auto_sb2tlOpt_out_d_valid; // @[LazyModule.scala 390:12]
  assign sb2tlOpt_auto_out_d_bits_denied = auto_sb2tlOpt_out_d_bits_denied; // @[LazyModule.scala 390:12]
  assign sb2tlOpt_auto_out_d_bits_data = auto_sb2tlOpt_out_d_bits_data; // @[LazyModule.scala 390:12]
  assign sb2tlOpt_auto_out_d_bits_corrupt = auto_sb2tlOpt_out_d_bits_corrupt; // @[LazyModule.scala 390:12]
  assign sb2tlOpt_io_rdEn = tryRdEn & _SBADDRESSFieldsReg_0_T_2 & _SBADDRESSFieldsReg_0_T_4 & _SBADDRESSFieldsReg_0_T &
    _sb2tlOpt_io_wrEn_T_7 & _sb2tlOpt_io_wrEn_T_9; // @[SBA.scala 197:156]
  assign sb2tlOpt_io_wrEn = _SBDATAFieldsReg_0_0_T_5 & _sb2tlOpt_io_wrEn_T_7 & _sb2tlOpt_io_wrEn_T_9; // @[SBA.scala 196:156]
  assign sb2tlOpt_io_addrIn = out_f_woready_34 ? _sb2tlOpt_io_addrIn_T : _autoIncrementedAddr_T; // @[SBA.scala 128:34]
  assign sb2tlOpt_io_dataIn = sb2tlOpt_io_wrEn ? _sb2tlOpt_io_dataIn_T : _sb2tlOpt_io_dataIn_T_1; // @[SBA.scala 172:34]
  assign sb2tlOpt_io_sizeIn = SBCSFieldsReg_sbaccess; // @[SBA.scala 198:30]
  assign sb2tlOpt_rf_reset = io_tl_reset; // @[Debug.scala 779:26]
  assign hartIsInResetSync_0_debug_hartReset_0_clock = clock;
  assign hartIsInResetSync_0_debug_hartReset_0_reset = reset;
  assign hartIsInResetSync_0_debug_hartReset_0_io_d = io_hartIsInReset_0; // @[ShiftReg.scala 47:16]
  assign hartIsInResetSync_1_debug_hartReset_1_clock = clock;
  assign hartIsInResetSync_1_debug_hartReset_1_reset = reset;
  assign hartIsInResetSync_1_debug_hartReset_1_io_d = io_hartIsInReset_1; // @[ShiftReg.scala 47:16]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      haltedBitRegs <= 2'h0;
    end else if (_T) begin
      haltedBitRegs <= 2'h0;
    end else begin
      haltedBitRegs <= _haltedBitRegs_T_4;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      resumeReqRegs <= 2'h0;
    end else if (_T) begin
      resumeReqRegs <= 2'h0;
    end else begin
      resumeReqRegs <= _resumeReqRegs_T_3;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      haveResetBitRegs <= 2'h0;
    end else if (_T) begin
      haveResetBitRegs <= 2'h0;
    end else if (_T_1 & io_innerCtrl_bits_ackhavereset) begin
      haveResetBitRegs <= _haveResetBitRegs_T_4;
    end else begin
      haveResetBitRegs <= _haveResetBitRegs_T_6;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      selectedHartReg <= 2'h0;
    end else begin
      selectedHartReg <= _GEN_1[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hamaskReg_0 <= 1'h0;
    end else if (_T) begin
      hamaskReg_0 <= 1'h0;
    end else if (_T_1) begin
      hamaskReg_0 <= io_innerCtrl_bits_hasel & io_innerCtrl_bits_hamask_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hamaskReg_1 <= 1'h0;
    end else if (_T) begin
      hamaskReg_1 <= 1'h0;
    end else if (_T_1) begin
      hamaskReg_1 <= io_innerCtrl_bits_hasel & io_innerCtrl_bits_hamask_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      hrmaskReg_0 <= 1'h0;
    end else if (_T) begin
      hrmaskReg_0 <= 1'h0;
    end else if (_T_1) begin
      hrmaskReg_0 <= io_innerCtrl_bits_hrmask_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      hrmaskReg_1 <= 1'h0;
    end else if (_T) begin
      hrmaskReg_1 <= 1'h0;
    end else if (_T_1) begin
      hrmaskReg_1 <= io_innerCtrl_bits_hrmask_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      hrDebugIntReg_0 <= 1'h0;
    end else if (_T) begin
      hrDebugIntReg_0 <= 1'h0;
    end else begin
      hrDebugIntReg_0 <= _T_25;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      hrDebugIntReg_1 <= 1'h0;
    end else if (_T) begin
      hrDebugIntReg_1 <= 1'h0;
    end else begin
      hrDebugIntReg_1 <= _T_26;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      grouptype <= 1'h0;
    end else if (_T) begin
      grouptype <= 1'h0;
    end else if (out_f_woready_104) begin
      grouptype <= DMCS2WrData_grouptype;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      rgParticipateHart_0 <= 1'h0;
    end else begin
      rgParticipateHart_0 <= _GEN_54[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      rgParticipateHart_1 <= 1'h0;
    end else begin
      rgParticipateHart_1 <= _GEN_60[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      hgParticipateHart_0 <= 1'h0;
    end else begin
      hgParticipateHart_0 <= _GEN_55[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      hgParticipateHart_1 <= 1'h0;
    end else begin
      hgParticipateHart_1 <= _GEN_61[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      hgFired_1 <= 1'h0;
    end else if (_T) begin
      hgFired_1 <= 1'h0;
    end else begin
      hgFired_1 <= _GEN_69;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      rgFired_1 <= 1'h0;
    end else if (_T) begin
      rgFired_1 <= 1'h0;
    end else begin
      rgFired_1 <= _GEN_72;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ABSTRACTCSReg_cmderr <= 3'h0;
    end else if (_T) begin
      ABSTRACTCSReg_cmderr <= 3'h0;
    end else if (errorBusy) begin
      ABSTRACTCSReg_cmderr <= 3'h1;
    end else if (errorException) begin
      ABSTRACTCSReg_cmderr <= 3'h3;
    end else if (errorUnsupported) begin
      ABSTRACTCSReg_cmderr <= 3'h2;
    end else begin
      ABSTRACTCSReg_cmderr <= _GEN_82;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ctrlStateReg <= 2'h0;
    end else if (_T) begin
      ctrlStateReg <= 2'h0;
    end else if (ABSTRACTCSWrEnLegal) begin
      if (wrAccessRegisterCommand | regAccessRegisterCommand) begin
        ctrlStateReg <= 2'h1;
      end
    end else if (_T_475) begin
      if (commandRegIsUnsupported) begin
        ctrlStateReg <= 2'h0;
      end else begin
        ctrlStateReg <= _GEN_1868;
      end
    end else if (_T_476) begin
      ctrlStateReg <= _GEN_1875;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      COMMANDRdData_cmdtype <= 8'h0;
    end else if (_T) begin
      COMMANDRdData_cmdtype <= 8'h0;
    end else if (COMMANDWrEn) begin
      COMMANDRdData_cmdtype <= COMMANDWrData_cmdtype;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      COMMANDRdData_control <= 24'h0;
    end else if (_T) begin
      COMMANDRdData_control <= 24'h0;
    end else if (COMMANDWrEn) begin
      COMMANDRdData_control <= COMMANDWrData_control;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ABSTRACTAUTOReg_autoexecdata <= 12'h0;
    end else if (_T) begin
      ABSTRACTAUTOReg_autoexecdata <= 12'h0;
    end else if (out_f_woready_23 & ABSTRACTCSWrEnLegal) begin
      ABSTRACTAUTOReg_autoexecdata <= _ABSTRACTAUTOReg_autoexecdata_T;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ABSTRACTAUTOReg_autoexecprogbuf <= 16'h0;
    end else if (_T) begin
      ABSTRACTAUTOReg_autoexecprogbuf <= 16'h0;
    end else if (out_f_woready_25 & ABSTRACTCSWrEnLegal) begin
      ABSTRACTAUTOReg_autoexecprogbuf <= ABSTRACTAUTOWrData_autoexecprogbuf;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      abstractDataMem_0 <= 8'h0;
    end else if (_T) begin
      abstractDataMem_0 <= 8'h0;
    end else if (out_f_wivalid_239) begin
      abstractDataMem_0 <= auto_tl_in_a_bits_data[7:0];
    end else if (out_f_woready_120 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_120) begin
        abstractDataMem_0 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      abstractDataMem_1 <= 8'h0;
    end else if (_T) begin
      abstractDataMem_1 <= 8'h0;
    end else if (out_f_wivalid_240) begin
      abstractDataMem_1 <= auto_tl_in_a_bits_data[15:8];
    end else if (out_f_woready_121 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_121) begin
        abstractDataMem_1 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      abstractDataMem_2 <= 8'h0;
    end else if (_T) begin
      abstractDataMem_2 <= 8'h0;
    end else if (out_f_wivalid_241) begin
      abstractDataMem_2 <= auto_tl_in_a_bits_data[23:16];
    end else if (out_f_woready_122 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_122) begin
        abstractDataMem_2 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      abstractDataMem_3 <= 8'h0;
    end else if (_T) begin
      abstractDataMem_3 <= 8'h0;
    end else if (out_f_wivalid_242) begin
      abstractDataMem_3 <= auto_tl_in_a_bits_data[31:24];
    end else if (out_f_woready_123 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_123) begin
        abstractDataMem_3 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      abstractDataMem_4 <= 8'h0;
    end else if (_T) begin
      abstractDataMem_4 <= 8'h0;
    end else if (out_f_wivalid_243) begin
      abstractDataMem_4 <= auto_tl_in_a_bits_data[39:32];
    end else if (out_f_woready & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready) begin
        abstractDataMem_4 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      abstractDataMem_5 <= 8'h0;
    end else if (_T) begin
      abstractDataMem_5 <= 8'h0;
    end else if (out_f_wivalid_244) begin
      abstractDataMem_5 <= auto_tl_in_a_bits_data[47:40];
    end else if (out_f_woready_1 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_1) begin
        abstractDataMem_5 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      abstractDataMem_6 <= 8'h0;
    end else if (_T) begin
      abstractDataMem_6 <= 8'h0;
    end else if (out_f_wivalid_245) begin
      abstractDataMem_6 <= auto_tl_in_a_bits_data[55:48];
    end else if (out_f_woready_2 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_2) begin
        abstractDataMem_6 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      abstractDataMem_7 <= 8'h0;
    end else if (_T) begin
      abstractDataMem_7 <= 8'h0;
    end else if (out_f_wivalid_246) begin
      abstractDataMem_7 <= auto_tl_in_a_bits_data[63:56];
    end else if (out_f_woready_3 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_3) begin
        abstractDataMem_7 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_0 <= 8'h0;
    end else if (_T) begin
      programBufferMem_0 <= 8'h0;
    end else if (out_f_wivalid_255) begin
      programBufferMem_0 <= auto_tl_in_a_bits_data[7:0];
    end else if (out_f_woready_49 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_49) begin
        programBufferMem_0 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_1 <= 8'h0;
    end else if (_T) begin
      programBufferMem_1 <= 8'h0;
    end else if (out_f_wivalid_256) begin
      programBufferMem_1 <= auto_tl_in_a_bits_data[15:8];
    end else if (out_f_woready_50 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_50) begin
        programBufferMem_1 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_2 <= 8'h0;
    end else if (_T) begin
      programBufferMem_2 <= 8'h0;
    end else if (out_f_wivalid_257) begin
      programBufferMem_2 <= auto_tl_in_a_bits_data[23:16];
    end else if (out_f_woready_51 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_51) begin
        programBufferMem_2 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_3 <= 8'h0;
    end else if (_T) begin
      programBufferMem_3 <= 8'h0;
    end else if (out_f_wivalid_258) begin
      programBufferMem_3 <= auto_tl_in_a_bits_data[31:24];
    end else if (out_f_woready_52 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_52) begin
        programBufferMem_3 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_4 <= 8'h0;
    end else if (_T) begin
      programBufferMem_4 <= 8'h0;
    end else if (out_f_wivalid_259) begin
      programBufferMem_4 <= auto_tl_in_a_bits_data[39:32];
    end else if (out_f_woready_41 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_41) begin
        programBufferMem_4 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_5 <= 8'h0;
    end else if (_T) begin
      programBufferMem_5 <= 8'h0;
    end else if (out_f_wivalid_260) begin
      programBufferMem_5 <= auto_tl_in_a_bits_data[47:40];
    end else if (out_f_woready_42 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_42) begin
        programBufferMem_5 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_6 <= 8'h0;
    end else if (_T) begin
      programBufferMem_6 <= 8'h0;
    end else if (out_f_wivalid_261) begin
      programBufferMem_6 <= auto_tl_in_a_bits_data[55:48];
    end else if (out_f_woready_43 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_43) begin
        programBufferMem_6 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_7 <= 8'h0;
    end else if (_T) begin
      programBufferMem_7 <= 8'h0;
    end else if (out_f_wivalid_262) begin
      programBufferMem_7 <= auto_tl_in_a_bits_data[63:56];
    end else if (out_f_woready_44 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_44) begin
        programBufferMem_7 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_8 <= 8'h0;
    end else if (_T) begin
      programBufferMem_8 <= 8'h0;
    end else if (out_f_wivalid_199) begin
      programBufferMem_8 <= auto_tl_in_a_bits_data[7:0];
    end else if (out_f_woready_53 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_53) begin
        programBufferMem_8 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_9 <= 8'h0;
    end else if (_T) begin
      programBufferMem_9 <= 8'h0;
    end else if (out_f_wivalid_200) begin
      programBufferMem_9 <= auto_tl_in_a_bits_data[15:8];
    end else if (out_f_woready_54 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_54) begin
        programBufferMem_9 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_10 <= 8'h0;
    end else if (_T) begin
      programBufferMem_10 <= 8'h0;
    end else if (out_f_wivalid_201) begin
      programBufferMem_10 <= auto_tl_in_a_bits_data[23:16];
    end else if (out_f_woready_55 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_55) begin
        programBufferMem_10 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_11 <= 8'h0;
    end else if (_T) begin
      programBufferMem_11 <= 8'h0;
    end else if (out_f_wivalid_202) begin
      programBufferMem_11 <= auto_tl_in_a_bits_data[31:24];
    end else if (out_f_woready_56 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_56) begin
        programBufferMem_11 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_12 <= 8'h0;
    end else if (_T) begin
      programBufferMem_12 <= 8'h0;
    end else if (out_f_wivalid_203) begin
      programBufferMem_12 <= auto_tl_in_a_bits_data[39:32];
    end else if (out_f_woready_96 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_96) begin
        programBufferMem_12 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_13 <= 8'h0;
    end else if (_T) begin
      programBufferMem_13 <= 8'h0;
    end else if (out_f_wivalid_204) begin
      programBufferMem_13 <= auto_tl_in_a_bits_data[47:40];
    end else if (out_f_woready_97 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_97) begin
        programBufferMem_13 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_14 <= 8'h0;
    end else if (_T) begin
      programBufferMem_14 <= 8'h0;
    end else if (out_f_wivalid_205) begin
      programBufferMem_14 <= auto_tl_in_a_bits_data[55:48];
    end else if (out_f_woready_98 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_98) begin
        programBufferMem_14 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_15 <= 8'h0;
    end else if (_T) begin
      programBufferMem_15 <= 8'h0;
    end else if (out_f_wivalid_206) begin
      programBufferMem_15 <= auto_tl_in_a_bits_data[63:56];
    end else if (out_f_woready_99 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_99) begin
        programBufferMem_15 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_16 <= 8'h0;
    end else if (_T) begin
      programBufferMem_16 <= 8'h0;
    end else if (out_f_wivalid_152) begin
      programBufferMem_16 <= auto_tl_in_a_bits_data[7:0];
    end else if (out_f_woready_115 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_115) begin
        programBufferMem_16 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_17 <= 8'h0;
    end else if (_T) begin
      programBufferMem_17 <= 8'h0;
    end else if (out_f_wivalid_153) begin
      programBufferMem_17 <= auto_tl_in_a_bits_data[15:8];
    end else if (out_f_woready_116 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_116) begin
        programBufferMem_17 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_18 <= 8'h0;
    end else if (_T) begin
      programBufferMem_18 <= 8'h0;
    end else if (out_f_wivalid_154) begin
      programBufferMem_18 <= auto_tl_in_a_bits_data[23:16];
    end else if (out_f_woready_117 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_117) begin
        programBufferMem_18 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_19 <= 8'h0;
    end else if (_T) begin
      programBufferMem_19 <= 8'h0;
    end else if (out_f_wivalid_155) begin
      programBufferMem_19 <= auto_tl_in_a_bits_data[31:24];
    end else if (out_f_woready_118 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_118) begin
        programBufferMem_19 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_20 <= 8'h0;
    end else if (_T) begin
      programBufferMem_20 <= 8'h0;
    end else if (out_f_wivalid_156) begin
      programBufferMem_20 <= auto_tl_in_a_bits_data[39:32];
    end else if (out_f_woready_26 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_26) begin
        programBufferMem_20 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_21 <= 8'h0;
    end else if (_T) begin
      programBufferMem_21 <= 8'h0;
    end else if (out_f_wivalid_157) begin
      programBufferMem_21 <= auto_tl_in_a_bits_data[47:40];
    end else if (out_f_woready_27 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_27) begin
        programBufferMem_21 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_22 <= 8'h0;
    end else if (_T) begin
      programBufferMem_22 <= 8'h0;
    end else if (out_f_wivalid_158) begin
      programBufferMem_22 <= auto_tl_in_a_bits_data[55:48];
    end else if (out_f_woready_28 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_28) begin
        programBufferMem_22 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_23 <= 8'h0;
    end else if (_T) begin
      programBufferMem_23 <= 8'h0;
    end else if (out_f_wivalid_159) begin
      programBufferMem_23 <= auto_tl_in_a_bits_data[63:56];
    end else if (out_f_woready_29 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_29) begin
        programBufferMem_23 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_24 <= 8'h0;
    end else if (_T) begin
      programBufferMem_24 <= 8'h0;
    end else if (out_f_wivalid_279) begin
      programBufferMem_24 <= auto_tl_in_a_bits_data[7:0];
    end else if (out_f_woready_37 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_37) begin
        programBufferMem_24 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_25 <= 8'h0;
    end else if (_T) begin
      programBufferMem_25 <= 8'h0;
    end else if (out_f_wivalid_280) begin
      programBufferMem_25 <= auto_tl_in_a_bits_data[15:8];
    end else if (out_f_woready_38 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_38) begin
        programBufferMem_25 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_26 <= 8'h0;
    end else if (_T) begin
      programBufferMem_26 <= 8'h0;
    end else if (out_f_wivalid_281) begin
      programBufferMem_26 <= auto_tl_in_a_bits_data[23:16];
    end else if (out_f_woready_39 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_39) begin
        programBufferMem_26 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_27 <= 8'h0;
    end else if (_T) begin
      programBufferMem_27 <= 8'h0;
    end else if (out_f_wivalid_282) begin
      programBufferMem_27 <= auto_tl_in_a_bits_data[31:24];
    end else if (out_f_woready_40 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_40) begin
        programBufferMem_27 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_28 <= 8'h0;
    end else if (_T) begin
      programBufferMem_28 <= 8'h0;
    end else if (out_f_wivalid_283) begin
      programBufferMem_28 <= auto_tl_in_a_bits_data[39:32];
    end else if (out_f_woready_92 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_92) begin
        programBufferMem_28 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_29 <= 8'h0;
    end else if (_T) begin
      programBufferMem_29 <= 8'h0;
    end else if (out_f_wivalid_284) begin
      programBufferMem_29 <= auto_tl_in_a_bits_data[47:40];
    end else if (out_f_woready_93 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_93) begin
        programBufferMem_29 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_30 <= 8'h0;
    end else if (_T) begin
      programBufferMem_30 <= 8'h0;
    end else if (out_f_wivalid_285) begin
      programBufferMem_30 <= auto_tl_in_a_bits_data[55:48];
    end else if (out_f_woready_94 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_94) begin
        programBufferMem_30 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_31 <= 8'h0;
    end else if (_T) begin
      programBufferMem_31 <= 8'h0;
    end else if (out_f_wivalid_286) begin
      programBufferMem_31 <= auto_tl_in_a_bits_data[63:56];
    end else if (out_f_woready_95 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_95) begin
        programBufferMem_31 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_32 <= 8'h0;
    end else if (_T) begin
      programBufferMem_32 <= 8'h0;
    end else if (out_f_wivalid_231) begin
      programBufferMem_32 <= auto_tl_in_a_bits_data[7:0];
    end else if (out_f_woready_109 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_109) begin
        programBufferMem_32 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_33 <= 8'h0;
    end else if (_T) begin
      programBufferMem_33 <= 8'h0;
    end else if (out_f_wivalid_232) begin
      programBufferMem_33 <= auto_tl_in_a_bits_data[15:8];
    end else if (out_f_woready_110 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_110) begin
        programBufferMem_33 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_34 <= 8'h0;
    end else if (_T) begin
      programBufferMem_34 <= 8'h0;
    end else if (out_f_wivalid_233) begin
      programBufferMem_34 <= auto_tl_in_a_bits_data[23:16];
    end else if (out_f_woready_111 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_111) begin
        programBufferMem_34 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_35 <= 8'h0;
    end else if (_T) begin
      programBufferMem_35 <= 8'h0;
    end else if (out_f_wivalid_234) begin
      programBufferMem_35 <= auto_tl_in_a_bits_data[31:24];
    end else if (out_f_woready_112 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_112) begin
        programBufferMem_35 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_36 <= 8'h0;
    end else if (_T) begin
      programBufferMem_36 <= 8'h0;
    end else if (out_f_wivalid_235) begin
      programBufferMem_36 <= auto_tl_in_a_bits_data[39:32];
    end else if (out_f_woready_45 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_45) begin
        programBufferMem_36 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_37 <= 8'h0;
    end else if (_T) begin
      programBufferMem_37 <= 8'h0;
    end else if (out_f_wivalid_236) begin
      programBufferMem_37 <= auto_tl_in_a_bits_data[47:40];
    end else if (out_f_woready_46 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_46) begin
        programBufferMem_37 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_38 <= 8'h0;
    end else if (_T) begin
      programBufferMem_38 <= 8'h0;
    end else if (out_f_wivalid_237) begin
      programBufferMem_38 <= auto_tl_in_a_bits_data[55:48];
    end else if (out_f_woready_47 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_47) begin
        programBufferMem_38 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_39 <= 8'h0;
    end else if (_T) begin
      programBufferMem_39 <= 8'h0;
    end else if (out_f_wivalid_238) begin
      programBufferMem_39 <= auto_tl_in_a_bits_data[63:56];
    end else if (out_f_woready_48 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_48) begin
        programBufferMem_39 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_40 <= 8'h0;
    end else if (_T) begin
      programBufferMem_40 <= 8'h0;
    end else if (out_f_wivalid_178) begin
      programBufferMem_40 <= auto_tl_in_a_bits_data[7:0];
    end else if (out_f_woready_19 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_19) begin
        programBufferMem_40 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_41 <= 8'h0;
    end else if (_T) begin
      programBufferMem_41 <= 8'h0;
    end else if (out_f_wivalid_179) begin
      programBufferMem_41 <= auto_tl_in_a_bits_data[15:8];
    end else if (out_f_woready_20 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_20) begin
        programBufferMem_41 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_42 <= 8'h0;
    end else if (_T) begin
      programBufferMem_42 <= 8'h0;
    end else if (out_f_wivalid_180) begin
      programBufferMem_42 <= auto_tl_in_a_bits_data[23:16];
    end else if (out_f_woready_21 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_21) begin
        programBufferMem_42 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_43 <= 8'h0;
    end else if (_T) begin
      programBufferMem_43 <= 8'h0;
    end else if (out_f_wivalid_181) begin
      programBufferMem_43 <= auto_tl_in_a_bits_data[31:24];
    end else if (out_f_woready_22 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_22) begin
        programBufferMem_43 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_44 <= 8'h0;
    end else if (_T) begin
      programBufferMem_44 <= 8'h0;
    end else if (out_f_wivalid_182) begin
      programBufferMem_44 <= auto_tl_in_a_bits_data[39:32];
    end else if (out_f_woready_105 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_105) begin
        programBufferMem_44 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_45 <= 8'h0;
    end else if (_T) begin
      programBufferMem_45 <= 8'h0;
    end else if (out_f_wivalid_183) begin
      programBufferMem_45 <= auto_tl_in_a_bits_data[47:40];
    end else if (out_f_woready_106 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_106) begin
        programBufferMem_45 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_46 <= 8'h0;
    end else if (_T) begin
      programBufferMem_46 <= 8'h0;
    end else if (out_f_wivalid_184) begin
      programBufferMem_46 <= auto_tl_in_a_bits_data[55:48];
    end else if (out_f_woready_107 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_107) begin
        programBufferMem_46 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_47 <= 8'h0;
    end else if (_T) begin
      programBufferMem_47 <= 8'h0;
    end else if (out_f_wivalid_185) begin
      programBufferMem_47 <= auto_tl_in_a_bits_data[63:56];
    end else if (out_f_woready_108 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_108) begin
        programBufferMem_47 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_48 <= 8'h0;
    end else if (_T) begin
      programBufferMem_48 <= 8'h0;
    end else if (out_f_wivalid_136) begin
      programBufferMem_48 <= auto_tl_in_a_bits_data[7:0];
    end else if (out_f_woready_88 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_88) begin
        programBufferMem_48 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_49 <= 8'h0;
    end else if (_T) begin
      programBufferMem_49 <= 8'h0;
    end else if (out_f_wivalid_137) begin
      programBufferMem_49 <= auto_tl_in_a_bits_data[15:8];
    end else if (out_f_woready_89 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_89) begin
        programBufferMem_49 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_50 <= 8'h0;
    end else if (_T) begin
      programBufferMem_50 <= 8'h0;
    end else if (out_f_wivalid_138) begin
      programBufferMem_50 <= auto_tl_in_a_bits_data[23:16];
    end else if (out_f_woready_90 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_90) begin
        programBufferMem_50 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_51 <= 8'h0;
    end else if (_T) begin
      programBufferMem_51 <= 8'h0;
    end else if (out_f_wivalid_139) begin
      programBufferMem_51 <= auto_tl_in_a_bits_data[31:24];
    end else if (out_f_woready_91 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_91) begin
        programBufferMem_51 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_52 <= 8'h0;
    end else if (_T) begin
      programBufferMem_52 <= 8'h0;
    end else if (out_f_wivalid_140) begin
      programBufferMem_52 <= auto_tl_in_a_bits_data[39:32];
    end else if (out_f_woready_57 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_57) begin
        programBufferMem_52 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_53 <= 8'h0;
    end else if (_T) begin
      programBufferMem_53 <= 8'h0;
    end else if (out_f_wivalid_141) begin
      programBufferMem_53 <= auto_tl_in_a_bits_data[47:40];
    end else if (out_f_woready_58 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_58) begin
        programBufferMem_53 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_54 <= 8'h0;
    end else if (_T) begin
      programBufferMem_54 <= 8'h0;
    end else if (out_f_wivalid_142) begin
      programBufferMem_54 <= auto_tl_in_a_bits_data[55:48];
    end else if (out_f_woready_59 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_59) begin
        programBufferMem_54 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_55 <= 8'h0;
    end else if (_T) begin
      programBufferMem_55 <= 8'h0;
    end else if (out_f_wivalid_143) begin
      programBufferMem_55 <= auto_tl_in_a_bits_data[63:56];
    end else if (out_f_woready_60 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_60) begin
        programBufferMem_55 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_56 <= 8'h0;
    end else if (_T) begin
      programBufferMem_56 <= 8'h0;
    end else if (out_f_wivalid_287) begin
      programBufferMem_56 <= auto_tl_in_a_bits_data[7:0];
    end else if (out_f_woready_30 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_30) begin
        programBufferMem_56 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_57 <= 8'h0;
    end else if (_T) begin
      programBufferMem_57 <= 8'h0;
    end else if (out_f_wivalid_288) begin
      programBufferMem_57 <= auto_tl_in_a_bits_data[15:8];
    end else if (out_f_woready_31 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_31) begin
        programBufferMem_57 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_58 <= 8'h0;
    end else if (_T) begin
      programBufferMem_58 <= 8'h0;
    end else if (out_f_wivalid_289) begin
      programBufferMem_58 <= auto_tl_in_a_bits_data[23:16];
    end else if (out_f_woready_32 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_32) begin
        programBufferMem_58 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_59 <= 8'h0;
    end else if (_T) begin
      programBufferMem_59 <= 8'h0;
    end else if (out_f_wivalid_290) begin
      programBufferMem_59 <= auto_tl_in_a_bits_data[31:24];
    end else if (out_f_woready_33 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_33) begin
        programBufferMem_59 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_60 <= 8'h0;
    end else if (_T) begin
      programBufferMem_60 <= 8'h0;
    end else if (out_f_wivalid_291) begin
      programBufferMem_60 <= auto_tl_in_a_bits_data[39:32];
    end else if (out_f_woready_124 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_124) begin
        programBufferMem_60 <= auto_dmi_in_a_bits_data[7:0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_61 <= 8'h0;
    end else if (_T) begin
      programBufferMem_61 <= 8'h0;
    end else if (out_f_wivalid_292) begin
      programBufferMem_61 <= auto_tl_in_a_bits_data[47:40];
    end else if (out_f_woready_125 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_125) begin
        programBufferMem_61 <= auto_dmi_in_a_bits_data[15:8];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_62 <= 8'h0;
    end else if (_T) begin
      programBufferMem_62 <= 8'h0;
    end else if (out_f_wivalid_293) begin
      programBufferMem_62 <= auto_tl_in_a_bits_data[55:48];
    end else if (out_f_woready_126 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_126) begin
        programBufferMem_62 <= auto_dmi_in_a_bits_data[23:16];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      programBufferMem_63 <= 8'h0;
    end else if (_T) begin
      programBufferMem_63 <= 8'h0;
    end else if (out_f_wivalid_294) begin
      programBufferMem_63 <= auto_tl_in_a_bits_data[63:56];
    end else if (out_f_woready_127 & ABSTRACTCSWrEnLegal) begin
      if (out_f_woready_127) begin
        programBufferMem_63 <= auto_dmi_in_a_bits_data[31:24];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBCSFieldsReg_sbbusyerror <= 1'h0;
    end else if (_T) begin
      SBCSFieldsReg_sbbusyerror <= 1'h0;
    end else if (out_f_woready_16 & SBCSWrData_sbbusyerror) begin
      SBCSFieldsReg_sbbusyerror <= 1'h0;
    end else begin
      SBCSFieldsReg_sbbusyerror <= _SBCSFieldsReg_sbbusyerror_T_5;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBCSFieldsReg_sbbusy <= 1'h0;
    end else if (_T) begin
      SBCSFieldsReg_sbbusy <= SBCSFieldsRegReset_sbbusy;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBCSFieldsReg_sbreadonaddr <= 1'h0;
    end else if (_T) begin
      SBCSFieldsReg_sbreadonaddr <= 1'h0;
    end else if (out_f_woready_14) begin
      SBCSFieldsReg_sbreadonaddr <= SBCSWrData_sbreadonaddr;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBCSFieldsReg_sbaccess <= 3'h0;
    end else if (_T) begin
      SBCSFieldsReg_sbaccess <= 3'h2;
    end else if (out_f_woready_13) begin
      SBCSFieldsReg_sbaccess <= SBCSWrData_sbaccess;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBCSFieldsReg_sbautoincrement <= 1'h0;
    end else if (_T) begin
      SBCSFieldsReg_sbautoincrement <= 1'h0;
    end else if (out_f_woready_12) begin
      SBCSFieldsReg_sbautoincrement <= SBCSWrData_sbautoincrement;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBCSFieldsReg_sbreadondata <= 1'h0;
    end else if (_T) begin
      SBCSFieldsReg_sbreadondata <= 1'h0;
    end else if (out_f_woready_11) begin
      SBCSFieldsReg_sbreadondata <= SBCSWrData_sbreadondata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBADDRESSFieldsReg_0 <= 32'h0;
    end else if (_T) begin
      SBADDRESSFieldsReg_0 <= 32'h0;
    end else if (out_f_woready_34 & SBCSRdData_sberror == 3'h0 & ~SBCSFieldsReg_sbbusy & ~SBCSFieldsReg_sbbusyerror
      ) begin
      if (out_f_woready_34) begin
        SBADDRESSFieldsReg_0 <= auto_dmi_in_a_bits_data;
      end else begin
        SBADDRESSFieldsReg_0 <= 32'h0;
      end
    end else if ((sb2tlOpt_io_rdDone | sb2tlOpt_io_wrDone) & SBCSFieldsReg_sbautoincrement) begin
      SBADDRESSFieldsReg_0 <= autoIncrementedAddr[31:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBADDRESSFieldsReg_1 <= 32'h0;
    end else if (_T) begin
      SBADDRESSFieldsReg_1 <= 32'h0;
    end else if (out_f_woready_114 & SBCSRdData_sberror == 3'h0 & ~SBCSFieldsReg_sbbusy & ~SBCSFieldsReg_sbbusyerror
      ) begin
      if (out_f_woready_114) begin
        SBADDRESSFieldsReg_1 <= auto_dmi_in_a_bits_data;
      end else begin
        SBADDRESSFieldsReg_1 <= 32'h0;
      end
    end else if ((sb2tlOpt_io_rdDone | sb2tlOpt_io_wrDone) & SBCSFieldsReg_sbautoincrement) begin
      SBADDRESSFieldsReg_1 <= autoIncrementedAddr[63:32];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      sbErrorReg_2 <= 1'h0;
    end else if (_T) begin
      sbErrorReg_2 <= 1'h0;
    end else if (out_f_woready_10 & SBCSWrData_sberror[2]) begin
      sbErrorReg_2 <= 1'h0;
    end else if (sb2tlOpt_io_wrEn & ~sb2tlOpt_io_wrLegal | sb2tlOpt_io_rdEn & ~sb2tlOpt_io_rdLegal) begin
      sbErrorReg_2 <= 1'h0;
    end else if ((out_f_woready_36 | tryRdEn) & sbAlignmentError) begin
      sbErrorReg_2 <= 1'h0;
    end else begin
      sbErrorReg_2 <= _sbErrorReg_2_T_15;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      sbErrorReg_1 <= 1'h0;
    end else if (_T) begin
      sbErrorReg_1 <= 1'h0;
    end else if (out_f_woready_10 & SBCSWrData_sberror[1]) begin
      sbErrorReg_1 <= 1'h0;
    end else begin
      sbErrorReg_1 <= _sbErrorReg_1_T_17;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      sbErrorReg_0 <= 1'h0;
    end else if (_T) begin
      sbErrorReg_0 <= 1'h0;
    end else if (out_f_woready_10 & SBCSWrData_sberror[0]) begin
      sbErrorReg_0 <= 1'h0;
    end else if (sb2tlOpt_io_wrEn & ~sb2tlOpt_io_wrLegal | sb2tlOpt_io_rdEn & ~sb2tlOpt_io_rdLegal) begin
      sbErrorReg_0 <= 1'h0;
    end else begin
      sbErrorReg_0 <= _sbErrorReg_0_T_16;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBDATAFieldsReg_0_0 <= 8'h0;
    end else if (_T) begin
      SBDATAFieldsReg_0_0 <= 8'h0;
    end else if (out_f_woready_36 & _SBADDRESSFieldsReg_0_T_2 & _SBADDRESSFieldsReg_0_T_4 & _SBADDRESSFieldsReg_0_T
      ) begin
      SBDATAFieldsReg_0_0 <= SBDATAWrData_0[7:0];
    end else if (sb2tlOpt_io_rdLoad_0) begin
      SBDATAFieldsReg_0_0 <= sb2tlOpt_io_dataOut;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBDATAFieldsReg_0_1 <= 8'h0;
    end else if (_T) begin
      SBDATAFieldsReg_0_1 <= 8'h0;
    end else if (out_f_woready_36 & _SBADDRESSFieldsReg_0_T_2 & _SBADDRESSFieldsReg_0_T_4 & _SBADDRESSFieldsReg_0_T
      ) begin
      SBDATAFieldsReg_0_1 <= SBDATAWrData_0[15:8];
    end else if (sb2tlOpt_io_rdLoad_1) begin
      SBDATAFieldsReg_0_1 <= sb2tlOpt_io_dataOut;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBDATAFieldsReg_0_2 <= 8'h0;
    end else if (_T) begin
      SBDATAFieldsReg_0_2 <= 8'h0;
    end else if (out_f_woready_36 & _SBADDRESSFieldsReg_0_T_2 & _SBADDRESSFieldsReg_0_T_4 & _SBADDRESSFieldsReg_0_T
      ) begin
      SBDATAFieldsReg_0_2 <= SBDATAWrData_0[23:16];
    end else if (sb2tlOpt_io_rdLoad_2) begin
      SBDATAFieldsReg_0_2 <= sb2tlOpt_io_dataOut;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBDATAFieldsReg_0_3 <= 8'h0;
    end else if (_T) begin
      SBDATAFieldsReg_0_3 <= 8'h0;
    end else if (out_f_woready_36 & _SBADDRESSFieldsReg_0_T_2 & _SBADDRESSFieldsReg_0_T_4 & _SBADDRESSFieldsReg_0_T
      ) begin
      SBDATAFieldsReg_0_3 <= SBDATAWrData_0[31:24];
    end else if (sb2tlOpt_io_rdLoad_3) begin
      SBDATAFieldsReg_0_3 <= sb2tlOpt_io_dataOut;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBDATAFieldsReg_1_0 <= 8'h0;
    end else if (_T) begin
      SBDATAFieldsReg_1_0 <= 8'h0;
    end else if (out_f_woready_35 & _SBADDRESSFieldsReg_0_T_2 & _SBADDRESSFieldsReg_0_T_4 & _SBADDRESSFieldsReg_0_T
      ) begin
      SBDATAFieldsReg_1_0 <= SBDATAWrData_1[7:0];
    end else if (sb2tlOpt_io_rdLoad_4) begin
      SBDATAFieldsReg_1_0 <= sb2tlOpt_io_dataOut;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBDATAFieldsReg_1_1 <= 8'h0;
    end else if (_T) begin
      SBDATAFieldsReg_1_1 <= 8'h0;
    end else if (out_f_woready_35 & _SBADDRESSFieldsReg_0_T_2 & _SBADDRESSFieldsReg_0_T_4 & _SBADDRESSFieldsReg_0_T
      ) begin
      SBDATAFieldsReg_1_1 <= SBDATAWrData_1[15:8];
    end else if (sb2tlOpt_io_rdLoad_5) begin
      SBDATAFieldsReg_1_1 <= sb2tlOpt_io_dataOut;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBDATAFieldsReg_1_2 <= 8'h0;
    end else if (_T) begin
      SBDATAFieldsReg_1_2 <= 8'h0;
    end else if (out_f_woready_35 & _SBADDRESSFieldsReg_0_T_2 & _SBADDRESSFieldsReg_0_T_4 & _SBADDRESSFieldsReg_0_T
      ) begin
      SBDATAFieldsReg_1_2 <= SBDATAWrData_1[23:16];
    end else if (sb2tlOpt_io_rdLoad_6) begin
      SBDATAFieldsReg_1_2 <= sb2tlOpt_io_dataOut;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      SBDATAFieldsReg_1_3 <= 8'h0;
    end else if (_T) begin
      SBDATAFieldsReg_1_3 <= 8'h0;
    end else if (out_f_woready_35 & _SBADDRESSFieldsReg_0_T_2 & _SBADDRESSFieldsReg_0_T_4 & _SBADDRESSFieldsReg_0_T
      ) begin
      SBDATAFieldsReg_1_3 <= SBDATAWrData_1[31:24];
    end else if (sb2tlOpt_io_rdLoad_7) begin
      SBDATAFieldsReg_1_3 <= sb2tlOpt_io_dataOut;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      goReg <= 1'h0;
    end else if (_T) begin
      goReg <= 1'h0;
    end else begin
      goReg <= _GEN_621;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      abstractGeneratedMem_0 <= 32'h0;
    end else if (goAbstract) begin
      if (accessRegisterCommandReg_transfer) begin
        if (accessRegisterCommandReg_write) begin
          abstractGeneratedMem_0 <= _abstractGeneratedMem_0_T;
        end else begin
          abstractGeneratedMem_0 <= _abstractGeneratedMem_0_T_1;
        end
      end else begin
        abstractGeneratedMem_0 <= 32'h13;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      abstractGeneratedMem_1 <= 32'h0;
    end else if (goAbstract) begin
      if (accessRegisterCommandReg_postexec) begin
        abstractGeneratedMem_1 <= 32'h13;
      end else begin
        abstractGeneratedMem_1 <= 32'h100073;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  haltedBitRegs = _RAND_0[1:0];
  _RAND_1 = {1{`RANDOM}};
  resumeReqRegs = _RAND_1[1:0];
  _RAND_2 = {1{`RANDOM}};
  haveResetBitRegs = _RAND_2[1:0];
  _RAND_3 = {1{`RANDOM}};
  selectedHartReg = _RAND_3[1:0];
  _RAND_4 = {1{`RANDOM}};
  hamaskReg_0 = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  hamaskReg_1 = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  hrmaskReg_0 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  hrmaskReg_1 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  hrDebugIntReg_0 = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  hrDebugIntReg_1 = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  grouptype = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  rgParticipateHart_0 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  rgParticipateHart_1 = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  hgParticipateHart_0 = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  hgParticipateHart_1 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  hgFired_1 = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  rgFired_1 = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  ABSTRACTCSReg_cmderr = _RAND_17[2:0];
  _RAND_18 = {1{`RANDOM}};
  ctrlStateReg = _RAND_18[1:0];
  _RAND_19 = {1{`RANDOM}};
  COMMANDRdData_cmdtype = _RAND_19[7:0];
  _RAND_20 = {1{`RANDOM}};
  COMMANDRdData_control = _RAND_20[23:0];
  _RAND_21 = {1{`RANDOM}};
  ABSTRACTAUTOReg_autoexecdata = _RAND_21[11:0];
  _RAND_22 = {1{`RANDOM}};
  ABSTRACTAUTOReg_autoexecprogbuf = _RAND_22[15:0];
  _RAND_23 = {1{`RANDOM}};
  abstractDataMem_0 = _RAND_23[7:0];
  _RAND_24 = {1{`RANDOM}};
  abstractDataMem_1 = _RAND_24[7:0];
  _RAND_25 = {1{`RANDOM}};
  abstractDataMem_2 = _RAND_25[7:0];
  _RAND_26 = {1{`RANDOM}};
  abstractDataMem_3 = _RAND_26[7:0];
  _RAND_27 = {1{`RANDOM}};
  abstractDataMem_4 = _RAND_27[7:0];
  _RAND_28 = {1{`RANDOM}};
  abstractDataMem_5 = _RAND_28[7:0];
  _RAND_29 = {1{`RANDOM}};
  abstractDataMem_6 = _RAND_29[7:0];
  _RAND_30 = {1{`RANDOM}};
  abstractDataMem_7 = _RAND_30[7:0];
  _RAND_31 = {1{`RANDOM}};
  programBufferMem_0 = _RAND_31[7:0];
  _RAND_32 = {1{`RANDOM}};
  programBufferMem_1 = _RAND_32[7:0];
  _RAND_33 = {1{`RANDOM}};
  programBufferMem_2 = _RAND_33[7:0];
  _RAND_34 = {1{`RANDOM}};
  programBufferMem_3 = _RAND_34[7:0];
  _RAND_35 = {1{`RANDOM}};
  programBufferMem_4 = _RAND_35[7:0];
  _RAND_36 = {1{`RANDOM}};
  programBufferMem_5 = _RAND_36[7:0];
  _RAND_37 = {1{`RANDOM}};
  programBufferMem_6 = _RAND_37[7:0];
  _RAND_38 = {1{`RANDOM}};
  programBufferMem_7 = _RAND_38[7:0];
  _RAND_39 = {1{`RANDOM}};
  programBufferMem_8 = _RAND_39[7:0];
  _RAND_40 = {1{`RANDOM}};
  programBufferMem_9 = _RAND_40[7:0];
  _RAND_41 = {1{`RANDOM}};
  programBufferMem_10 = _RAND_41[7:0];
  _RAND_42 = {1{`RANDOM}};
  programBufferMem_11 = _RAND_42[7:0];
  _RAND_43 = {1{`RANDOM}};
  programBufferMem_12 = _RAND_43[7:0];
  _RAND_44 = {1{`RANDOM}};
  programBufferMem_13 = _RAND_44[7:0];
  _RAND_45 = {1{`RANDOM}};
  programBufferMem_14 = _RAND_45[7:0];
  _RAND_46 = {1{`RANDOM}};
  programBufferMem_15 = _RAND_46[7:0];
  _RAND_47 = {1{`RANDOM}};
  programBufferMem_16 = _RAND_47[7:0];
  _RAND_48 = {1{`RANDOM}};
  programBufferMem_17 = _RAND_48[7:0];
  _RAND_49 = {1{`RANDOM}};
  programBufferMem_18 = _RAND_49[7:0];
  _RAND_50 = {1{`RANDOM}};
  programBufferMem_19 = _RAND_50[7:0];
  _RAND_51 = {1{`RANDOM}};
  programBufferMem_20 = _RAND_51[7:0];
  _RAND_52 = {1{`RANDOM}};
  programBufferMem_21 = _RAND_52[7:0];
  _RAND_53 = {1{`RANDOM}};
  programBufferMem_22 = _RAND_53[7:0];
  _RAND_54 = {1{`RANDOM}};
  programBufferMem_23 = _RAND_54[7:0];
  _RAND_55 = {1{`RANDOM}};
  programBufferMem_24 = _RAND_55[7:0];
  _RAND_56 = {1{`RANDOM}};
  programBufferMem_25 = _RAND_56[7:0];
  _RAND_57 = {1{`RANDOM}};
  programBufferMem_26 = _RAND_57[7:0];
  _RAND_58 = {1{`RANDOM}};
  programBufferMem_27 = _RAND_58[7:0];
  _RAND_59 = {1{`RANDOM}};
  programBufferMem_28 = _RAND_59[7:0];
  _RAND_60 = {1{`RANDOM}};
  programBufferMem_29 = _RAND_60[7:0];
  _RAND_61 = {1{`RANDOM}};
  programBufferMem_30 = _RAND_61[7:0];
  _RAND_62 = {1{`RANDOM}};
  programBufferMem_31 = _RAND_62[7:0];
  _RAND_63 = {1{`RANDOM}};
  programBufferMem_32 = _RAND_63[7:0];
  _RAND_64 = {1{`RANDOM}};
  programBufferMem_33 = _RAND_64[7:0];
  _RAND_65 = {1{`RANDOM}};
  programBufferMem_34 = _RAND_65[7:0];
  _RAND_66 = {1{`RANDOM}};
  programBufferMem_35 = _RAND_66[7:0];
  _RAND_67 = {1{`RANDOM}};
  programBufferMem_36 = _RAND_67[7:0];
  _RAND_68 = {1{`RANDOM}};
  programBufferMem_37 = _RAND_68[7:0];
  _RAND_69 = {1{`RANDOM}};
  programBufferMem_38 = _RAND_69[7:0];
  _RAND_70 = {1{`RANDOM}};
  programBufferMem_39 = _RAND_70[7:0];
  _RAND_71 = {1{`RANDOM}};
  programBufferMem_40 = _RAND_71[7:0];
  _RAND_72 = {1{`RANDOM}};
  programBufferMem_41 = _RAND_72[7:0];
  _RAND_73 = {1{`RANDOM}};
  programBufferMem_42 = _RAND_73[7:0];
  _RAND_74 = {1{`RANDOM}};
  programBufferMem_43 = _RAND_74[7:0];
  _RAND_75 = {1{`RANDOM}};
  programBufferMem_44 = _RAND_75[7:0];
  _RAND_76 = {1{`RANDOM}};
  programBufferMem_45 = _RAND_76[7:0];
  _RAND_77 = {1{`RANDOM}};
  programBufferMem_46 = _RAND_77[7:0];
  _RAND_78 = {1{`RANDOM}};
  programBufferMem_47 = _RAND_78[7:0];
  _RAND_79 = {1{`RANDOM}};
  programBufferMem_48 = _RAND_79[7:0];
  _RAND_80 = {1{`RANDOM}};
  programBufferMem_49 = _RAND_80[7:0];
  _RAND_81 = {1{`RANDOM}};
  programBufferMem_50 = _RAND_81[7:0];
  _RAND_82 = {1{`RANDOM}};
  programBufferMem_51 = _RAND_82[7:0];
  _RAND_83 = {1{`RANDOM}};
  programBufferMem_52 = _RAND_83[7:0];
  _RAND_84 = {1{`RANDOM}};
  programBufferMem_53 = _RAND_84[7:0];
  _RAND_85 = {1{`RANDOM}};
  programBufferMem_54 = _RAND_85[7:0];
  _RAND_86 = {1{`RANDOM}};
  programBufferMem_55 = _RAND_86[7:0];
  _RAND_87 = {1{`RANDOM}};
  programBufferMem_56 = _RAND_87[7:0];
  _RAND_88 = {1{`RANDOM}};
  programBufferMem_57 = _RAND_88[7:0];
  _RAND_89 = {1{`RANDOM}};
  programBufferMem_58 = _RAND_89[7:0];
  _RAND_90 = {1{`RANDOM}};
  programBufferMem_59 = _RAND_90[7:0];
  _RAND_91 = {1{`RANDOM}};
  programBufferMem_60 = _RAND_91[7:0];
  _RAND_92 = {1{`RANDOM}};
  programBufferMem_61 = _RAND_92[7:0];
  _RAND_93 = {1{`RANDOM}};
  programBufferMem_62 = _RAND_93[7:0];
  _RAND_94 = {1{`RANDOM}};
  programBufferMem_63 = _RAND_94[7:0];
  _RAND_95 = {1{`RANDOM}};
  SBCSFieldsReg_sbbusyerror = _RAND_95[0:0];
  _RAND_96 = {1{`RANDOM}};
  SBCSFieldsReg_sbbusy = _RAND_96[0:0];
  _RAND_97 = {1{`RANDOM}};
  SBCSFieldsReg_sbreadonaddr = _RAND_97[0:0];
  _RAND_98 = {1{`RANDOM}};
  SBCSFieldsReg_sbaccess = _RAND_98[2:0];
  _RAND_99 = {1{`RANDOM}};
  SBCSFieldsReg_sbautoincrement = _RAND_99[0:0];
  _RAND_100 = {1{`RANDOM}};
  SBCSFieldsReg_sbreadondata = _RAND_100[0:0];
  _RAND_101 = {1{`RANDOM}};
  SBADDRESSFieldsReg_0 = _RAND_101[31:0];
  _RAND_102 = {1{`RANDOM}};
  SBADDRESSFieldsReg_1 = _RAND_102[31:0];
  _RAND_103 = {1{`RANDOM}};
  sbErrorReg_2 = _RAND_103[0:0];
  _RAND_104 = {1{`RANDOM}};
  sbErrorReg_1 = _RAND_104[0:0];
  _RAND_105 = {1{`RANDOM}};
  sbErrorReg_0 = _RAND_105[0:0];
  _RAND_106 = {1{`RANDOM}};
  SBDATAFieldsReg_0_0 = _RAND_106[7:0];
  _RAND_107 = {1{`RANDOM}};
  SBDATAFieldsReg_0_1 = _RAND_107[7:0];
  _RAND_108 = {1{`RANDOM}};
  SBDATAFieldsReg_0_2 = _RAND_108[7:0];
  _RAND_109 = {1{`RANDOM}};
  SBDATAFieldsReg_0_3 = _RAND_109[7:0];
  _RAND_110 = {1{`RANDOM}};
  SBDATAFieldsReg_1_0 = _RAND_110[7:0];
  _RAND_111 = {1{`RANDOM}};
  SBDATAFieldsReg_1_1 = _RAND_111[7:0];
  _RAND_112 = {1{`RANDOM}};
  SBDATAFieldsReg_1_2 = _RAND_112[7:0];
  _RAND_113 = {1{`RANDOM}};
  SBDATAFieldsReg_1_3 = _RAND_113[7:0];
  _RAND_114 = {1{`RANDOM}};
  goReg = _RAND_114[0:0];
  _RAND_115 = {1{`RANDOM}};
  abstractGeneratedMem_0 = _RAND_115[31:0];
  _RAND_116 = {1{`RANDOM}};
  abstractGeneratedMem_1 = _RAND_116[31:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    haltedBitRegs = 2'h0;
  end
  if (rf_reset) begin
    resumeReqRegs = 2'h0;
  end
  if (rf_reset) begin
    haveResetBitRegs = 2'h0;
  end
  if (rf_reset) begin
    selectedHartReg = 2'h0;
  end
  if (rf_reset) begin
    hamaskReg_0 = 1'h0;
  end
  if (rf_reset) begin
    hamaskReg_1 = 1'h0;
  end
  if (reset) begin
    hrmaskReg_0 = 1'h0;
  end
  if (reset) begin
    hrmaskReg_1 = 1'h0;
  end
  if (reset) begin
    hrDebugIntReg_0 = 1'h0;
  end
  if (reset) begin
    hrDebugIntReg_1 = 1'h0;
  end
  if (rf_reset) begin
    grouptype = 1'h0;
  end
  if (reset) begin
    rgParticipateHart_0 = 1'h0;
  end
  if (reset) begin
    rgParticipateHart_1 = 1'h0;
  end
  if (reset) begin
    hgParticipateHart_0 = 1'h0;
  end
  if (reset) begin
    hgParticipateHart_1 = 1'h0;
  end
  if (reset) begin
    hgFired_1 = 1'h0;
  end
  if (reset) begin
    rgFired_1 = 1'h0;
  end
  if (rf_reset) begin
    ABSTRACTCSReg_cmderr = 3'h0;
  end
  if (rf_reset) begin
    ctrlStateReg = 2'h0;
  end
  if (rf_reset) begin
    COMMANDRdData_cmdtype = 8'h0;
  end
  if (rf_reset) begin
    COMMANDRdData_control = 24'h0;
  end
  if (rf_reset) begin
    ABSTRACTAUTOReg_autoexecdata = 12'h0;
  end
  if (rf_reset) begin
    ABSTRACTAUTOReg_autoexecprogbuf = 16'h0;
  end
  if (rf_reset) begin
    abstractDataMem_0 = 8'h0;
  end
  if (rf_reset) begin
    abstractDataMem_1 = 8'h0;
  end
  if (rf_reset) begin
    abstractDataMem_2 = 8'h0;
  end
  if (rf_reset) begin
    abstractDataMem_3 = 8'h0;
  end
  if (rf_reset) begin
    abstractDataMem_4 = 8'h0;
  end
  if (rf_reset) begin
    abstractDataMem_5 = 8'h0;
  end
  if (rf_reset) begin
    abstractDataMem_6 = 8'h0;
  end
  if (rf_reset) begin
    abstractDataMem_7 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_0 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_1 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_2 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_3 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_4 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_5 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_6 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_7 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_8 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_9 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_10 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_11 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_12 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_13 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_14 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_15 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_16 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_17 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_18 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_19 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_20 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_21 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_22 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_23 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_24 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_25 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_26 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_27 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_28 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_29 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_30 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_31 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_32 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_33 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_34 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_35 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_36 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_37 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_38 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_39 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_40 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_41 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_42 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_43 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_44 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_45 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_46 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_47 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_48 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_49 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_50 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_51 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_52 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_53 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_54 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_55 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_56 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_57 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_58 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_59 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_60 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_61 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_62 = 8'h0;
  end
  if (rf_reset) begin
    programBufferMem_63 = 8'h0;
  end
  if (rf_reset) begin
    SBCSFieldsReg_sbbusyerror = 1'h0;
  end
  if (rf_reset) begin
    SBCSFieldsReg_sbbusy = 1'h0;
  end
  if (rf_reset) begin
    SBCSFieldsReg_sbreadonaddr = 1'h0;
  end
  if (rf_reset) begin
    SBCSFieldsReg_sbaccess = 3'h0;
  end
  if (rf_reset) begin
    SBCSFieldsReg_sbautoincrement = 1'h0;
  end
  if (rf_reset) begin
    SBCSFieldsReg_sbreadondata = 1'h0;
  end
  if (rf_reset) begin
    SBADDRESSFieldsReg_0 = 32'h0;
  end
  if (rf_reset) begin
    SBADDRESSFieldsReg_1 = 32'h0;
  end
  if (rf_reset) begin
    sbErrorReg_2 = 1'h0;
  end
  if (rf_reset) begin
    sbErrorReg_1 = 1'h0;
  end
  if (rf_reset) begin
    sbErrorReg_0 = 1'h0;
  end
  if (rf_reset) begin
    SBDATAFieldsReg_0_0 = 8'h0;
  end
  if (rf_reset) begin
    SBDATAFieldsReg_0_1 = 8'h0;
  end
  if (rf_reset) begin
    SBDATAFieldsReg_0_2 = 8'h0;
  end
  if (rf_reset) begin
    SBDATAFieldsReg_0_3 = 8'h0;
  end
  if (rf_reset) begin
    SBDATAFieldsReg_1_0 = 8'h0;
  end
  if (rf_reset) begin
    SBDATAFieldsReg_1_1 = 8'h0;
  end
  if (rf_reset) begin
    SBDATAFieldsReg_1_2 = 8'h0;
  end
  if (rf_reset) begin
    SBDATAFieldsReg_1_3 = 8'h0;
  end
  if (rf_reset) begin
    goReg = 1'h0;
  end
  if (rf_reset) begin
    abstractGeneratedMem_0 = 32'h0;
  end
  if (rf_reset) begin
    abstractGeneratedMem_1 = 32'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
