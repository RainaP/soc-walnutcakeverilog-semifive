//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__ShiftQueue_12(
  input         rf_reset,
  input         clock,
  input         reset,
  output        io_enq_ready,
  input         io_enq_valid,
  input  [4:0]  io_enq_bits_reason,
  input  [38:0] io_enq_bits_iaddr,
  input  [13:0] io_enq_bits_icnt,
  input  [31:0] io_enq_bits_hist,
  input  [1:0]  io_enq_bits_hisv,
  input         io_deq_ready,
  output        io_deq_valid,
  output [4:0]  io_deq_bits_reason,
  output [38:0] io_deq_bits_iaddr,
  output [13:0] io_deq_bits_icnt,
  output [31:0] io_deq_bits_hist,
  output [1:0]  io_deq_bits_hisv
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [63:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [63:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
`endif // RANDOMIZE_REG_INIT
  reg  valid_0; // @[ShiftQueue.scala 21:30]
  reg  valid_1; // @[ShiftQueue.scala 21:30]
  reg [4:0] elts_0_reason; // @[ShiftQueue.scala 22:25]
  reg [38:0] elts_0_iaddr; // @[ShiftQueue.scala 22:25]
  reg [13:0] elts_0_icnt; // @[ShiftQueue.scala 22:25]
  reg [31:0] elts_0_hist; // @[ShiftQueue.scala 22:25]
  reg [1:0] elts_0_hisv; // @[ShiftQueue.scala 22:25]
  reg [4:0] elts_1_reason; // @[ShiftQueue.scala 22:25]
  reg [38:0] elts_1_iaddr; // @[ShiftQueue.scala 22:25]
  reg [13:0] elts_1_icnt; // @[ShiftQueue.scala 22:25]
  reg [31:0] elts_1_hist; // @[ShiftQueue.scala 22:25]
  reg [1:0] elts_1_hisv; // @[ShiftQueue.scala 22:25]
  wire  _wen_T = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  wire  _wen_T_3 = valid_1 | _wen_T; // @[ShiftQueue.scala 30:28]
  wire  _wen_T_7 = _wen_T & ~valid_0; // @[ShiftQueue.scala 31:45]
  wire  wen = io_deq_ready ? _wen_T_3 : _wen_T_7; // @[ShiftQueue.scala 29:10]
  wire  _valid_0_T_6 = _wen_T | valid_0; // @[ShiftQueue.scala 37:45]
  wire  _wen_T_10 = _wen_T & valid_1; // @[ShiftQueue.scala 30:45]
  wire  _wen_T_13 = _wen_T & valid_0; // @[ShiftQueue.scala 31:25]
  wire  _wen_T_15 = _wen_T & valid_0 & ~valid_1; // @[ShiftQueue.scala 31:45]
  wire  wen_1 = io_deq_ready ? _wen_T_10 : _wen_T_15; // @[ShiftQueue.scala 29:10]
  wire  _valid_1_T_6 = _wen_T_13 | valid_1; // @[ShiftQueue.scala 37:45]
  assign io_enq_ready = ~valid_1; // @[ShiftQueue.scala 40:19]
  assign io_deq_valid = valid_0; // @[ShiftQueue.scala 41:16]
  assign io_deq_bits_reason = elts_0_reason; // @[ShiftQueue.scala 42:15]
  assign io_deq_bits_iaddr = elts_0_iaddr; // @[ShiftQueue.scala 42:15]
  assign io_deq_bits_icnt = elts_0_icnt; // @[ShiftQueue.scala 42:15]
  assign io_deq_bits_hist = elts_0_hist; // @[ShiftQueue.scala 42:15]
  assign io_deq_bits_hisv = elts_0_hisv; // @[ShiftQueue.scala 42:15]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid_0 <= 1'h0;
    end else if (io_deq_ready) begin
      valid_0 <= _wen_T_3;
    end else begin
      valid_0 <= _valid_0_T_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid_1 <= 1'h0;
    end else if (io_deq_ready) begin
      valid_1 <= _wen_T_10;
    end else begin
      valid_1 <= _valid_1_T_6;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_0_reason <= 5'h0;
    end else if (wen) begin
      if (valid_1) begin
        elts_0_reason <= elts_1_reason;
      end else begin
        elts_0_reason <= io_enq_bits_reason;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_0_iaddr <= 39'h0;
    end else if (wen) begin
      if (valid_1) begin
        elts_0_iaddr <= elts_1_iaddr;
      end else begin
        elts_0_iaddr <= io_enq_bits_iaddr;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_0_icnt <= 14'h0;
    end else if (wen) begin
      if (valid_1) begin
        elts_0_icnt <= elts_1_icnt;
      end else begin
        elts_0_icnt <= io_enq_bits_icnt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_0_hist <= 32'h0;
    end else if (wen) begin
      if (valid_1) begin
        elts_0_hist <= elts_1_hist;
      end else begin
        elts_0_hist <= io_enq_bits_hist;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_0_hisv <= 2'h0;
    end else if (wen) begin
      if (valid_1) begin
        elts_0_hisv <= elts_1_hisv;
      end else begin
        elts_0_hisv <= io_enq_bits_hisv;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_1_reason <= 5'h0;
    end else if (wen_1) begin
      elts_1_reason <= io_enq_bits_reason;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_1_iaddr <= 39'h0;
    end else if (wen_1) begin
      elts_1_iaddr <= io_enq_bits_iaddr;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_1_icnt <= 14'h0;
    end else if (wen_1) begin
      elts_1_icnt <= io_enq_bits_icnt;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_1_hist <= 32'h0;
    end else if (wen_1) begin
      elts_1_hist <= io_enq_bits_hist;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_1_hisv <= 2'h0;
    end else if (wen_1) begin
      elts_1_hisv <= io_enq_bits_hisv;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  valid_0 = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  valid_1 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  elts_0_reason = _RAND_2[4:0];
  _RAND_3 = {2{`RANDOM}};
  elts_0_iaddr = _RAND_3[38:0];
  _RAND_4 = {1{`RANDOM}};
  elts_0_icnt = _RAND_4[13:0];
  _RAND_5 = {1{`RANDOM}};
  elts_0_hist = _RAND_5[31:0];
  _RAND_6 = {1{`RANDOM}};
  elts_0_hisv = _RAND_6[1:0];
  _RAND_7 = {1{`RANDOM}};
  elts_1_reason = _RAND_7[4:0];
  _RAND_8 = {2{`RANDOM}};
  elts_1_iaddr = _RAND_8[38:0];
  _RAND_9 = {1{`RANDOM}};
  elts_1_icnt = _RAND_9[13:0];
  _RAND_10 = {1{`RANDOM}};
  elts_1_hist = _RAND_10[31:0];
  _RAND_11 = {1{`RANDOM}};
  elts_1_hisv = _RAND_11[1:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    valid_0 = 1'h0;
  end
  if (reset) begin
    valid_1 = 1'h0;
  end
  if (rf_reset) begin
    elts_0_reason = 5'h0;
  end
  if (rf_reset) begin
    elts_0_iaddr = 39'h0;
  end
  if (rf_reset) begin
    elts_0_icnt = 14'h0;
  end
  if (rf_reset) begin
    elts_0_hist = 32'h0;
  end
  if (rf_reset) begin
    elts_0_hisv = 2'h0;
  end
  if (rf_reset) begin
    elts_1_reason = 5'h0;
  end
  if (rf_reset) begin
    elts_1_iaddr = 39'h0;
  end
  if (rf_reset) begin
    elts_1_icnt = 14'h0;
  end
  if (rf_reset) begin
    elts_1_hist = 32'h0;
  end
  if (rf_reset) begin
    elts_1_hisv = 2'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
