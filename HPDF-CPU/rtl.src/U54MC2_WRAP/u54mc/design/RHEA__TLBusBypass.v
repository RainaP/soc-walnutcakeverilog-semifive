//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLBusBypass(
  input         rf_reset,
  input         clock,
  input         reset,
  input         auto_node_out_out_a_ready,
  output        auto_node_out_out_a_valid,
  output [2:0]  auto_node_out_out_a_bits_opcode,
  output [8:0]  auto_node_out_out_a_bits_address,
  output [31:0] auto_node_out_out_a_bits_data,
  output        auto_node_out_out_d_ready,
  input         auto_node_out_out_d_valid,
  input  [2:0]  auto_node_out_out_d_bits_opcode,
  input  [1:0]  auto_node_out_out_d_bits_param,
  input  [1:0]  auto_node_out_out_d_bits_size,
  input         auto_node_out_out_d_bits_source,
  input         auto_node_out_out_d_bits_sink,
  input         auto_node_out_out_d_bits_denied,
  input  [31:0] auto_node_out_out_d_bits_data,
  input         auto_node_out_out_d_bits_corrupt,
  output        auto_node_in_in_a_ready,
  input         auto_node_in_in_a_valid,
  input  [2:0]  auto_node_in_in_a_bits_opcode,
  input  [8:0]  auto_node_in_in_a_bits_address,
  input  [31:0] auto_node_in_in_a_bits_data,
  input         auto_node_in_in_d_ready,
  output        auto_node_in_in_d_valid,
  output [2:0]  auto_node_in_in_d_bits_opcode,
  output [1:0]  auto_node_in_in_d_bits_param,
  output [1:0]  auto_node_in_in_d_bits_size,
  output        auto_node_in_in_d_bits_sink,
  output        auto_node_in_in_d_bits_denied,
  output [31:0] auto_node_in_in_d_bits_data,
  output        auto_node_in_in_d_bits_corrupt,
  input         io_bypass
);
  wire  bar_rf_reset; // @[BusBypass.scala 17:33]
  wire  bar_clock; // @[BusBypass.scala 17:33]
  wire  bar_reset; // @[BusBypass.scala 17:33]
  wire  bar_auto_in_a_ready; // @[BusBypass.scala 17:33]
  wire  bar_auto_in_a_valid; // @[BusBypass.scala 17:33]
  wire [2:0] bar_auto_in_a_bits_opcode; // @[BusBypass.scala 17:33]
  wire [8:0] bar_auto_in_a_bits_address; // @[BusBypass.scala 17:33]
  wire [31:0] bar_auto_in_a_bits_data; // @[BusBypass.scala 17:33]
  wire  bar_auto_in_d_ready; // @[BusBypass.scala 17:33]
  wire  bar_auto_in_d_valid; // @[BusBypass.scala 17:33]
  wire [2:0] bar_auto_in_d_bits_opcode; // @[BusBypass.scala 17:33]
  wire [1:0] bar_auto_in_d_bits_param; // @[BusBypass.scala 17:33]
  wire [1:0] bar_auto_in_d_bits_size; // @[BusBypass.scala 17:33]
  wire  bar_auto_in_d_bits_sink; // @[BusBypass.scala 17:33]
  wire  bar_auto_in_d_bits_denied; // @[BusBypass.scala 17:33]
  wire [31:0] bar_auto_in_d_bits_data; // @[BusBypass.scala 17:33]
  wire  bar_auto_in_d_bits_corrupt; // @[BusBypass.scala 17:33]
  wire  bar_auto_out_1_a_ready; // @[BusBypass.scala 17:33]
  wire  bar_auto_out_1_a_valid; // @[BusBypass.scala 17:33]
  wire [2:0] bar_auto_out_1_a_bits_opcode; // @[BusBypass.scala 17:33]
  wire [8:0] bar_auto_out_1_a_bits_address; // @[BusBypass.scala 17:33]
  wire [31:0] bar_auto_out_1_a_bits_data; // @[BusBypass.scala 17:33]
  wire  bar_auto_out_1_d_ready; // @[BusBypass.scala 17:33]
  wire  bar_auto_out_1_d_valid; // @[BusBypass.scala 17:33]
  wire [2:0] bar_auto_out_1_d_bits_opcode; // @[BusBypass.scala 17:33]
  wire [1:0] bar_auto_out_1_d_bits_param; // @[BusBypass.scala 17:33]
  wire [1:0] bar_auto_out_1_d_bits_size; // @[BusBypass.scala 17:33]
  wire  bar_auto_out_1_d_bits_source; // @[BusBypass.scala 17:33]
  wire  bar_auto_out_1_d_bits_sink; // @[BusBypass.scala 17:33]
  wire  bar_auto_out_1_d_bits_denied; // @[BusBypass.scala 17:33]
  wire [31:0] bar_auto_out_1_d_bits_data; // @[BusBypass.scala 17:33]
  wire  bar_auto_out_1_d_bits_corrupt; // @[BusBypass.scala 17:33]
  wire  bar_auto_out_0_a_ready; // @[BusBypass.scala 17:33]
  wire  bar_auto_out_0_a_valid; // @[BusBypass.scala 17:33]
  wire [2:0] bar_auto_out_0_a_bits_opcode; // @[BusBypass.scala 17:33]
  wire [127:0] bar_auto_out_0_a_bits_address; // @[BusBypass.scala 17:33]
  wire  bar_auto_out_0_d_ready; // @[BusBypass.scala 17:33]
  wire  bar_auto_out_0_d_valid; // @[BusBypass.scala 17:33]
  wire [2:0] bar_auto_out_0_d_bits_opcode; // @[BusBypass.scala 17:33]
  wire  bar_auto_out_0_d_bits_corrupt; // @[BusBypass.scala 17:33]
  wire  bar_io_bypass; // @[BusBypass.scala 17:33]
  wire  error_rf_reset; // @[BusBypass.scala 27:40]
  wire  error_clock; // @[BusBypass.scala 27:40]
  wire  error_reset; // @[BusBypass.scala 27:40]
  wire  error_auto_in_a_ready; // @[BusBypass.scala 27:40]
  wire  error_auto_in_a_valid; // @[BusBypass.scala 27:40]
  wire [2:0] error_auto_in_a_bits_opcode; // @[BusBypass.scala 27:40]
  wire [127:0] error_auto_in_a_bits_address; // @[BusBypass.scala 27:40]
  wire  error_auto_in_d_ready; // @[BusBypass.scala 27:40]
  wire  error_auto_in_d_valid; // @[BusBypass.scala 27:40]
  wire [2:0] error_auto_in_d_bits_opcode; // @[BusBypass.scala 27:40]
  wire  error_auto_in_d_bits_corrupt; // @[BusBypass.scala 27:40]
  RHEA__TLBusBypassBar bar ( // @[BusBypass.scala 17:33]
    .rf_reset(bar_rf_reset),
    .clock(bar_clock),
    .reset(bar_reset),
    .auto_in_a_ready(bar_auto_in_a_ready),
    .auto_in_a_valid(bar_auto_in_a_valid),
    .auto_in_a_bits_opcode(bar_auto_in_a_bits_opcode),
    .auto_in_a_bits_address(bar_auto_in_a_bits_address),
    .auto_in_a_bits_data(bar_auto_in_a_bits_data),
    .auto_in_d_ready(bar_auto_in_d_ready),
    .auto_in_d_valid(bar_auto_in_d_valid),
    .auto_in_d_bits_opcode(bar_auto_in_d_bits_opcode),
    .auto_in_d_bits_param(bar_auto_in_d_bits_param),
    .auto_in_d_bits_size(bar_auto_in_d_bits_size),
    .auto_in_d_bits_sink(bar_auto_in_d_bits_sink),
    .auto_in_d_bits_denied(bar_auto_in_d_bits_denied),
    .auto_in_d_bits_data(bar_auto_in_d_bits_data),
    .auto_in_d_bits_corrupt(bar_auto_in_d_bits_corrupt),
    .auto_out_1_a_ready(bar_auto_out_1_a_ready),
    .auto_out_1_a_valid(bar_auto_out_1_a_valid),
    .auto_out_1_a_bits_opcode(bar_auto_out_1_a_bits_opcode),
    .auto_out_1_a_bits_address(bar_auto_out_1_a_bits_address),
    .auto_out_1_a_bits_data(bar_auto_out_1_a_bits_data),
    .auto_out_1_d_ready(bar_auto_out_1_d_ready),
    .auto_out_1_d_valid(bar_auto_out_1_d_valid),
    .auto_out_1_d_bits_opcode(bar_auto_out_1_d_bits_opcode),
    .auto_out_1_d_bits_param(bar_auto_out_1_d_bits_param),
    .auto_out_1_d_bits_size(bar_auto_out_1_d_bits_size),
    .auto_out_1_d_bits_source(bar_auto_out_1_d_bits_source),
    .auto_out_1_d_bits_sink(bar_auto_out_1_d_bits_sink),
    .auto_out_1_d_bits_denied(bar_auto_out_1_d_bits_denied),
    .auto_out_1_d_bits_data(bar_auto_out_1_d_bits_data),
    .auto_out_1_d_bits_corrupt(bar_auto_out_1_d_bits_corrupt),
    .auto_out_0_a_ready(bar_auto_out_0_a_ready),
    .auto_out_0_a_valid(bar_auto_out_0_a_valid),
    .auto_out_0_a_bits_opcode(bar_auto_out_0_a_bits_opcode),
    .auto_out_0_a_bits_address(bar_auto_out_0_a_bits_address),
    .auto_out_0_d_ready(bar_auto_out_0_d_ready),
    .auto_out_0_d_valid(bar_auto_out_0_d_valid),
    .auto_out_0_d_bits_opcode(bar_auto_out_0_d_bits_opcode),
    .auto_out_0_d_bits_corrupt(bar_auto_out_0_d_bits_corrupt),
    .io_bypass(bar_io_bypass)
  );
  RHEA__TLError_1 error ( // @[BusBypass.scala 27:40]
    .rf_reset(error_rf_reset),
    .clock(error_clock),
    .reset(error_reset),
    .auto_in_a_ready(error_auto_in_a_ready),
    .auto_in_a_valid(error_auto_in_a_valid),
    .auto_in_a_bits_opcode(error_auto_in_a_bits_opcode),
    .auto_in_a_bits_address(error_auto_in_a_bits_address),
    .auto_in_d_ready(error_auto_in_d_ready),
    .auto_in_d_valid(error_auto_in_d_valid),
    .auto_in_d_bits_opcode(error_auto_in_d_bits_opcode),
    .auto_in_d_bits_corrupt(error_auto_in_d_bits_corrupt)
  );
  assign bar_rf_reset = rf_reset;
  assign error_rf_reset = rf_reset;
  assign auto_node_out_out_a_valid = bar_auto_out_1_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_node_out_out_a_bits_opcode = bar_auto_out_1_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_node_out_out_a_bits_address = bar_auto_out_1_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_node_out_out_a_bits_data = bar_auto_out_1_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_node_out_out_d_ready = bar_auto_out_1_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_node_in_in_a_ready = bar_auto_in_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_node_in_in_d_valid = bar_auto_in_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_node_in_in_d_bits_opcode = bar_auto_in_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_node_in_in_d_bits_param = bar_auto_in_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_node_in_in_d_bits_size = bar_auto_in_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_node_in_in_d_bits_sink = bar_auto_in_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_node_in_in_d_bits_denied = bar_auto_in_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_node_in_in_d_bits_data = bar_auto_in_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_node_in_in_d_bits_corrupt = bar_auto_in_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign bar_clock = clock;
  assign bar_reset = reset;
  assign bar_auto_in_a_valid = auto_node_in_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bar_auto_in_a_bits_opcode = auto_node_in_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bar_auto_in_a_bits_address = auto_node_in_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bar_auto_in_a_bits_data = auto_node_in_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bar_auto_in_d_ready = auto_node_in_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bar_auto_out_1_a_ready = auto_node_out_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bar_auto_out_1_d_valid = auto_node_out_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bar_auto_out_1_d_bits_opcode = auto_node_out_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bar_auto_out_1_d_bits_param = auto_node_out_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bar_auto_out_1_d_bits_size = auto_node_out_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bar_auto_out_1_d_bits_source = auto_node_out_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bar_auto_out_1_d_bits_sink = auto_node_out_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bar_auto_out_1_d_bits_denied = auto_node_out_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bar_auto_out_1_d_bits_data = auto_node_out_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bar_auto_out_1_d_bits_corrupt = auto_node_out_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bar_auto_out_0_a_ready = error_auto_in_a_ready; // @[LazyModule.scala 377:16]
  assign bar_auto_out_0_d_valid = error_auto_in_d_valid; // @[LazyModule.scala 377:16]
  assign bar_auto_out_0_d_bits_opcode = error_auto_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign bar_auto_out_0_d_bits_corrupt = error_auto_in_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign bar_io_bypass = io_bypass; // @[BusBypass.scala 45:26]
  assign error_clock = clock;
  assign error_reset = reset;
  assign error_auto_in_a_valid = bar_auto_out_0_a_valid; // @[LazyModule.scala 377:16]
  assign error_auto_in_a_bits_opcode = bar_auto_out_0_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign error_auto_in_a_bits_address = bar_auto_out_0_a_bits_address; // @[LazyModule.scala 377:16]
  assign error_auto_in_d_ready = bar_auto_out_0_d_ready; // @[LazyModule.scala 377:16]
endmodule
