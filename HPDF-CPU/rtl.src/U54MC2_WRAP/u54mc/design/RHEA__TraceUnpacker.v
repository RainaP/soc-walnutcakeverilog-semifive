//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TraceUnpacker(
  input         rf_reset,
  input         clock,
  input         reset,
  input         io_teActive,
  output        io_stream_ready,
  input         io_stream_valid,
  input  [31:0] io_stream_bits,
  input         io_btm_ready,
  output        io_btm_valid,
  output [4:0]  io_btm_bits_length,
  output [5:0]  io_btm_bits_slices_0_data,
  output [1:0]  io_btm_bits_slices_0_mseo,
  output [5:0]  io_btm_bits_slices_1_data,
  output [1:0]  io_btm_bits_slices_1_mseo,
  output [5:0]  io_btm_bits_slices_2_data,
  output [1:0]  io_btm_bits_slices_2_mseo,
  output [5:0]  io_btm_bits_slices_3_data,
  output [1:0]  io_btm_bits_slices_3_mseo,
  output [5:0]  io_btm_bits_slices_4_data,
  output [1:0]  io_btm_bits_slices_4_mseo,
  output [5:0]  io_btm_bits_slices_5_data,
  output [1:0]  io_btm_bits_slices_5_mseo,
  output [5:0]  io_btm_bits_slices_6_data,
  output [1:0]  io_btm_bits_slices_6_mseo,
  output [5:0]  io_btm_bits_slices_7_data,
  output [1:0]  io_btm_bits_slices_7_mseo,
  output [5:0]  io_btm_bits_slices_8_data,
  output [1:0]  io_btm_bits_slices_8_mseo,
  output [5:0]  io_btm_bits_slices_9_data,
  output [1:0]  io_btm_bits_slices_9_mseo,
  output [5:0]  io_btm_bits_slices_10_data,
  output [1:0]  io_btm_bits_slices_10_mseo,
  output [5:0]  io_btm_bits_slices_11_data,
  output [1:0]  io_btm_bits_slices_11_mseo,
  output [5:0]  io_btm_bits_slices_12_data,
  output [1:0]  io_btm_bits_slices_12_mseo,
  output [5:0]  io_btm_bits_slices_13_data,
  output [1:0]  io_btm_bits_slices_13_mseo,
  output [5:0]  io_btm_bits_slices_14_data,
  output [1:0]  io_btm_bits_slices_14_mseo,
  output [5:0]  io_btm_bits_slices_15_data,
  output [1:0]  io_btm_bits_slices_15_mseo,
  output [5:0]  io_btm_bits_slices_16_data,
  output [1:0]  io_btm_bits_slices_16_mseo,
  output [5:0]  io_btm_bits_slices_17_data,
  output [1:0]  io_btm_bits_slices_17_mseo,
  output [5:0]  io_btm_bits_slices_18_data,
  output [1:0]  io_btm_bits_slices_18_mseo,
  output [5:0]  io_btm_bits_slices_19_data,
  output [1:0]  io_btm_bits_slices_19_mseo,
  output [5:0]  io_btm_bits_slices_20_data,
  output [1:0]  io_btm_bits_slices_20_mseo,
  output [5:0]  io_btm_bits_slices_21_data,
  output [1:0]  io_btm_bits_slices_21_mseo,
  output [5:0]  io_btm_bits_slices_22_data,
  output [1:0]  io_btm_bits_slices_22_mseo,
  output [5:0]  io_btm_bits_slices_23_data,
  output [1:0]  io_btm_bits_slices_23_mseo,
  output [5:0]  io_btm_bits_slices_24_data,
  output [1:0]  io_btm_bits_slices_24_mseo
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
`endif // RANDOMIZE_REG_INIT
  reg [5:0] hold_0_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_0_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_1_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_1_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_2_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_2_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_3_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_3_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_4_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_4_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_5_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_5_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_6_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_6_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_7_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_7_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_8_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_8_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_9_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_9_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_10_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_10_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_11_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_11_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_12_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_12_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_13_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_13_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_14_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_14_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_15_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_15_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_16_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_16_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_17_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_17_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_18_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_18_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_19_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_19_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_20_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_20_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_21_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_21_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_22_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_22_mseo; // @[TraceUnpacker.scala 21:19]
  reg [5:0] hold_23_data; // @[TraceUnpacker.scala 21:19]
  reg [1:0] hold_23_mseo; // @[TraceUnpacker.scala 21:19]
  reg [4:0] holdV; // @[TraceUnpacker.scala 22:23]
  wire  _inV_T_8 = io_stream_bits[1:0] == 2'h3; // @[TraceUnpacker.scala 27:100]
  wire  _inV_T_9 = io_stream_bits[9:8] == 2'h3; // @[TraceUnpacker.scala 27:100]
  wire  _inV_T_10 = io_stream_bits[17:16] == 2'h3; // @[TraceUnpacker.scala 27:100]
  wire [1:0] _inV_T_12 = io_stream_bits[17:16] == 2'h3 ? 2'h2 : 2'h3; // @[TraceUnpacker.scala 27:92]
  wire [1:0] _inV_T_13 = io_stream_bits[9:8] == 2'h3 ? 2'h1 : _inV_T_12; // @[TraceUnpacker.scala 27:92]
  wire [1:0] _inV_T_14 = io_stream_bits[1:0] == 2'h3 ? 2'h0 : _inV_T_13; // @[TraceUnpacker.scala 27:92]
  wire [2:0] inV = 2'h1 + _inV_T_14; // @[TraceUnpacker.scala 27:14]
  wire [4:0] _hold_0_T_1 = 5'h0 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_0_T_2 = 4'h8 * _hold_0_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_0_T_3 = io_stream_bits >> _hold_0_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_1_T_1 = 5'h1 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_1_T_2 = 4'h8 * _hold_1_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_1_T_3 = io_stream_bits >> _hold_1_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_2_T_1 = 5'h2 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_2_T_2 = 4'h8 * _hold_2_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_2_T_3 = io_stream_bits >> _hold_2_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_3_T_1 = 5'h3 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_3_T_2 = 4'h8 * _hold_3_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_3_T_3 = io_stream_bits >> _hold_3_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_4_T_1 = 5'h4 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_4_T_2 = 4'h8 * _hold_4_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_4_T_3 = io_stream_bits >> _hold_4_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_5_T_1 = 5'h5 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_5_T_2 = 4'h8 * _hold_5_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_5_T_3 = io_stream_bits >> _hold_5_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_6_T_1 = 5'h6 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_6_T_2 = 4'h8 * _hold_6_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_6_T_3 = io_stream_bits >> _hold_6_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_7_T_1 = 5'h7 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_7_T_2 = 4'h8 * _hold_7_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_7_T_3 = io_stream_bits >> _hold_7_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_8_T_1 = 5'h8 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_8_T_2 = 4'h8 * _hold_8_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_8_T_3 = io_stream_bits >> _hold_8_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_9_T_1 = 5'h9 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_9_T_2 = 4'h8 * _hold_9_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_9_T_3 = io_stream_bits >> _hold_9_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_10_T_1 = 5'ha - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_10_T_2 = 4'h8 * _hold_10_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_10_T_3 = io_stream_bits >> _hold_10_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_11_T_1 = 5'hb - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_11_T_2 = 4'h8 * _hold_11_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_11_T_3 = io_stream_bits >> _hold_11_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_12_T_1 = 5'hc - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_12_T_2 = 4'h8 * _hold_12_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_12_T_3 = io_stream_bits >> _hold_12_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_13_T_1 = 5'hd - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_13_T_2 = 4'h8 * _hold_13_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_13_T_3 = io_stream_bits >> _hold_13_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_14_T_1 = 5'he - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_14_T_2 = 4'h8 * _hold_14_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_14_T_3 = io_stream_bits >> _hold_14_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_15_T_1 = 5'hf - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_15_T_2 = 4'h8 * _hold_15_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_15_T_3 = io_stream_bits >> _hold_15_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_16_T_1 = 5'h10 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_16_T_2 = 4'h8 * _hold_16_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_16_T_3 = io_stream_bits >> _hold_16_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_17_T_1 = 5'h11 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_17_T_2 = 4'h8 * _hold_17_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_17_T_3 = io_stream_bits >> _hold_17_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_18_T_1 = 5'h12 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_18_T_2 = 4'h8 * _hold_18_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_18_T_3 = io_stream_bits >> _hold_18_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_19_T_1 = 5'h13 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_19_T_2 = 4'h8 * _hold_19_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_19_T_3 = io_stream_bits >> _hold_19_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_20_T_1 = 5'h14 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_20_T_2 = 4'h8 * _hold_20_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_20_T_3 = io_stream_bits >> _hold_20_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_21_T_1 = 5'h15 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_21_T_2 = 4'h8 * _hold_21_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_21_T_3 = io_stream_bits >> _hold_21_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_22_T_1 = 5'h16 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_22_T_2 = 4'h8 * _hold_22_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_22_T_3 = io_stream_bits >> _hold_22_T_2; // @[TraceUnpacker.scala 32:40]
  wire [4:0] _hold_23_T_1 = 5'h17 - holdV; // @[TraceUnpacker.scala 32:68]
  wire [8:0] _hold_23_T_2 = 4'h8 * _hold_23_T_1; // @[TraceUnpacker.scala 32:58]
  wire [31:0] _hold_23_T_3 = io_stream_bits >> _hold_23_T_2; // @[TraceUnpacker.scala 32:40]
  wire  _T_25 = io_btm_ready & io_btm_valid; // @[Decoupled.scala 40:37]
  wire  _T_26 = io_stream_ready & io_stream_valid; // @[Decoupled.scala 40:37]
  wire [4:0] _holdV_T_1 = holdV + 5'h4; // @[TraceUnpacker.scala 42:20]
  wire  inlast = _inV_T_8 | _inV_T_9 | _inV_T_10 | io_stream_bits[25:24] == 2'h3; // @[TraceUnpacker.scala 46:111]
  wire [4:0] _io_btm_bits_slices_24_T_1 = 5'h18 - holdV; // @[TraceUnpacker.scala 61:80]
  wire [8:0] _io_btm_bits_slices_24_T_2 = 4'h8 * _io_btm_bits_slices_24_T_1; // @[TraceUnpacker.scala 61:70]
  wire [31:0] _io_btm_bits_slices_24_T_3 = io_stream_bits >> _io_btm_bits_slices_24_T_2; // @[TraceUnpacker.scala 61:52]
  wire [4:0] _GEN_99 = {{2'd0}, inV}; // @[TraceUnpacker.scala 64:31]
  wire [5:0] _io_btm_bits_length_T = holdV + _GEN_99; // @[TraceUnpacker.scala 64:31]
  assign io_stream_ready = io_btm_ready | ~inlast; // @[TraceUnpacker.scala 50:35]
  assign io_btm_valid = io_stream_valid & inlast; // @[TraceUnpacker.scala 47:35]
  assign io_btm_bits_length = _io_btm_bits_length_T[4:0]; // @[TraceUnpacker.scala 64:22]
  assign io_btm_bits_slices_0_data = 5'h0 < holdV ? hold_0_data : _hold_0_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_0_mseo = 5'h0 < holdV ? hold_0_mseo : _hold_0_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_1_data = 5'h1 < holdV ? hold_1_data : _hold_1_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_1_mseo = 5'h1 < holdV ? hold_1_mseo : _hold_1_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_2_data = 5'h2 < holdV ? hold_2_data : _hold_2_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_2_mseo = 5'h2 < holdV ? hold_2_mseo : _hold_2_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_3_data = 5'h3 < holdV ? hold_3_data : _hold_3_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_3_mseo = 5'h3 < holdV ? hold_3_mseo : _hold_3_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_4_data = 5'h4 < holdV ? hold_4_data : _hold_4_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_4_mseo = 5'h4 < holdV ? hold_4_mseo : _hold_4_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_5_data = 5'h5 < holdV ? hold_5_data : _hold_5_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_5_mseo = 5'h5 < holdV ? hold_5_mseo : _hold_5_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_6_data = 5'h6 < holdV ? hold_6_data : _hold_6_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_6_mseo = 5'h6 < holdV ? hold_6_mseo : _hold_6_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_7_data = 5'h7 < holdV ? hold_7_data : _hold_7_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_7_mseo = 5'h7 < holdV ? hold_7_mseo : _hold_7_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_8_data = 5'h8 < holdV ? hold_8_data : _hold_8_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_8_mseo = 5'h8 < holdV ? hold_8_mseo : _hold_8_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_9_data = 5'h9 < holdV ? hold_9_data : _hold_9_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_9_mseo = 5'h9 < holdV ? hold_9_mseo : _hold_9_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_10_data = 5'ha < holdV ? hold_10_data : _hold_10_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_10_mseo = 5'ha < holdV ? hold_10_mseo : _hold_10_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_11_data = 5'hb < holdV ? hold_11_data : _hold_11_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_11_mseo = 5'hb < holdV ? hold_11_mseo : _hold_11_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_12_data = 5'hc < holdV ? hold_12_data : _hold_12_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_12_mseo = 5'hc < holdV ? hold_12_mseo : _hold_12_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_13_data = 5'hd < holdV ? hold_13_data : _hold_13_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_13_mseo = 5'hd < holdV ? hold_13_mseo : _hold_13_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_14_data = 5'he < holdV ? hold_14_data : _hold_14_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_14_mseo = 5'he < holdV ? hold_14_mseo : _hold_14_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_15_data = 5'hf < holdV ? hold_15_data : _hold_15_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_15_mseo = 5'hf < holdV ? hold_15_mseo : _hold_15_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_16_data = 5'h10 < holdV ? hold_16_data : _hold_16_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_16_mseo = 5'h10 < holdV ? hold_16_mseo : _hold_16_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_17_data = 5'h11 < holdV ? hold_17_data : _hold_17_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_17_mseo = 5'h11 < holdV ? hold_17_mseo : _hold_17_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_18_data = 5'h12 < holdV ? hold_18_data : _hold_18_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_18_mseo = 5'h12 < holdV ? hold_18_mseo : _hold_18_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_19_data = 5'h13 < holdV ? hold_19_data : _hold_19_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_19_mseo = 5'h13 < holdV ? hold_19_mseo : _hold_19_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_20_data = 5'h14 < holdV ? hold_20_data : _hold_20_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_20_mseo = 5'h14 < holdV ? hold_20_mseo : _hold_20_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_21_data = 5'h15 < holdV ? hold_21_data : _hold_21_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_21_mseo = 5'h15 < holdV ? hold_21_mseo : _hold_21_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_22_data = 5'h16 < holdV ? hold_22_data : _hold_22_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_22_mseo = 5'h16 < holdV ? hold_22_mseo : _hold_22_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_23_data = 5'h17 < holdV ? hold_23_data : _hold_23_T_3[7:2]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_23_mseo = 5'h17 < holdV ? hold_23_mseo : _hold_23_T_3[1:0]; // @[TraceUnpacker.scala 54:28 TraceUnpacker.scala 55:35 TraceUnpacker.scala 57:35]
  assign io_btm_bits_slices_24_data = _io_btm_bits_slices_24_T_3[7:2]; // @[TraceUnpacker.scala 61:99]
  assign io_btm_bits_slices_24_mseo = _io_btm_bits_slices_24_T_3[1:0]; // @[TraceUnpacker.scala 61:99]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_0_data <= 6'h0;
    end else if (5'h0 >= holdV) begin
      hold_0_data <= _hold_0_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_0_mseo <= 2'h0;
    end else if (5'h0 >= holdV) begin
      hold_0_mseo <= _hold_0_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_1_data <= 6'h0;
    end else if (5'h1 >= holdV) begin
      hold_1_data <= _hold_1_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_1_mseo <= 2'h0;
    end else if (5'h1 >= holdV) begin
      hold_1_mseo <= _hold_1_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_2_data <= 6'h0;
    end else if (5'h2 >= holdV) begin
      hold_2_data <= _hold_2_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_2_mseo <= 2'h0;
    end else if (5'h2 >= holdV) begin
      hold_2_mseo <= _hold_2_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_3_data <= 6'h0;
    end else if (5'h3 >= holdV) begin
      hold_3_data <= _hold_3_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_3_mseo <= 2'h0;
    end else if (5'h3 >= holdV) begin
      hold_3_mseo <= _hold_3_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_4_data <= 6'h0;
    end else if (5'h4 >= holdV) begin
      hold_4_data <= _hold_4_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_4_mseo <= 2'h0;
    end else if (5'h4 >= holdV) begin
      hold_4_mseo <= _hold_4_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_5_data <= 6'h0;
    end else if (5'h5 >= holdV) begin
      hold_5_data <= _hold_5_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_5_mseo <= 2'h0;
    end else if (5'h5 >= holdV) begin
      hold_5_mseo <= _hold_5_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_6_data <= 6'h0;
    end else if (5'h6 >= holdV) begin
      hold_6_data <= _hold_6_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_6_mseo <= 2'h0;
    end else if (5'h6 >= holdV) begin
      hold_6_mseo <= _hold_6_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_7_data <= 6'h0;
    end else if (5'h7 >= holdV) begin
      hold_7_data <= _hold_7_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_7_mseo <= 2'h0;
    end else if (5'h7 >= holdV) begin
      hold_7_mseo <= _hold_7_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_8_data <= 6'h0;
    end else if (5'h8 >= holdV) begin
      hold_8_data <= _hold_8_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_8_mseo <= 2'h0;
    end else if (5'h8 >= holdV) begin
      hold_8_mseo <= _hold_8_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_9_data <= 6'h0;
    end else if (5'h9 >= holdV) begin
      hold_9_data <= _hold_9_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_9_mseo <= 2'h0;
    end else if (5'h9 >= holdV) begin
      hold_9_mseo <= _hold_9_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_10_data <= 6'h0;
    end else if (5'ha >= holdV) begin
      hold_10_data <= _hold_10_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_10_mseo <= 2'h0;
    end else if (5'ha >= holdV) begin
      hold_10_mseo <= _hold_10_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_11_data <= 6'h0;
    end else if (5'hb >= holdV) begin
      hold_11_data <= _hold_11_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_11_mseo <= 2'h0;
    end else if (5'hb >= holdV) begin
      hold_11_mseo <= _hold_11_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_12_data <= 6'h0;
    end else if (5'hc >= holdV) begin
      hold_12_data <= _hold_12_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_12_mseo <= 2'h0;
    end else if (5'hc >= holdV) begin
      hold_12_mseo <= _hold_12_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_13_data <= 6'h0;
    end else if (5'hd >= holdV) begin
      hold_13_data <= _hold_13_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_13_mseo <= 2'h0;
    end else if (5'hd >= holdV) begin
      hold_13_mseo <= _hold_13_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_14_data <= 6'h0;
    end else if (5'he >= holdV) begin
      hold_14_data <= _hold_14_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_14_mseo <= 2'h0;
    end else if (5'he >= holdV) begin
      hold_14_mseo <= _hold_14_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_15_data <= 6'h0;
    end else if (5'hf >= holdV) begin
      hold_15_data <= _hold_15_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_15_mseo <= 2'h0;
    end else if (5'hf >= holdV) begin
      hold_15_mseo <= _hold_15_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_16_data <= 6'h0;
    end else if (5'h10 >= holdV) begin
      hold_16_data <= _hold_16_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_16_mseo <= 2'h0;
    end else if (5'h10 >= holdV) begin
      hold_16_mseo <= _hold_16_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_17_data <= 6'h0;
    end else if (5'h11 >= holdV) begin
      hold_17_data <= _hold_17_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_17_mseo <= 2'h0;
    end else if (5'h11 >= holdV) begin
      hold_17_mseo <= _hold_17_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_18_data <= 6'h0;
    end else if (5'h12 >= holdV) begin
      hold_18_data <= _hold_18_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_18_mseo <= 2'h0;
    end else if (5'h12 >= holdV) begin
      hold_18_mseo <= _hold_18_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_19_data <= 6'h0;
    end else if (5'h13 >= holdV) begin
      hold_19_data <= _hold_19_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_19_mseo <= 2'h0;
    end else if (5'h13 >= holdV) begin
      hold_19_mseo <= _hold_19_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_20_data <= 6'h0;
    end else if (5'h14 >= holdV) begin
      hold_20_data <= _hold_20_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_20_mseo <= 2'h0;
    end else if (5'h14 >= holdV) begin
      hold_20_mseo <= _hold_20_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_21_data <= 6'h0;
    end else if (5'h15 >= holdV) begin
      hold_21_data <= _hold_21_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_21_mseo <= 2'h0;
    end else if (5'h15 >= holdV) begin
      hold_21_mseo <= _hold_21_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_22_data <= 6'h0;
    end else if (5'h16 >= holdV) begin
      hold_22_data <= _hold_22_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_22_mseo <= 2'h0;
    end else if (5'h16 >= holdV) begin
      hold_22_mseo <= _hold_22_T_3[1:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_23_data <= 6'h0;
    end else if (5'h17 >= holdV) begin
      hold_23_data <= _hold_23_T_3[7:2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hold_23_mseo <= 2'h0;
    end else if (5'h17 >= holdV) begin
      hold_23_mseo <= _hold_23_T_3[1:0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      holdV <= 5'h0;
    end else if (~io_teActive) begin
      holdV <= 5'h0;
    end else if (_T_25) begin
      holdV <= 5'h0;
    end else if (_T_26) begin
      holdV <= _holdV_T_1;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  hold_0_data = _RAND_0[5:0];
  _RAND_1 = {1{`RANDOM}};
  hold_0_mseo = _RAND_1[1:0];
  _RAND_2 = {1{`RANDOM}};
  hold_1_data = _RAND_2[5:0];
  _RAND_3 = {1{`RANDOM}};
  hold_1_mseo = _RAND_3[1:0];
  _RAND_4 = {1{`RANDOM}};
  hold_2_data = _RAND_4[5:0];
  _RAND_5 = {1{`RANDOM}};
  hold_2_mseo = _RAND_5[1:0];
  _RAND_6 = {1{`RANDOM}};
  hold_3_data = _RAND_6[5:0];
  _RAND_7 = {1{`RANDOM}};
  hold_3_mseo = _RAND_7[1:0];
  _RAND_8 = {1{`RANDOM}};
  hold_4_data = _RAND_8[5:0];
  _RAND_9 = {1{`RANDOM}};
  hold_4_mseo = _RAND_9[1:0];
  _RAND_10 = {1{`RANDOM}};
  hold_5_data = _RAND_10[5:0];
  _RAND_11 = {1{`RANDOM}};
  hold_5_mseo = _RAND_11[1:0];
  _RAND_12 = {1{`RANDOM}};
  hold_6_data = _RAND_12[5:0];
  _RAND_13 = {1{`RANDOM}};
  hold_6_mseo = _RAND_13[1:0];
  _RAND_14 = {1{`RANDOM}};
  hold_7_data = _RAND_14[5:0];
  _RAND_15 = {1{`RANDOM}};
  hold_7_mseo = _RAND_15[1:0];
  _RAND_16 = {1{`RANDOM}};
  hold_8_data = _RAND_16[5:0];
  _RAND_17 = {1{`RANDOM}};
  hold_8_mseo = _RAND_17[1:0];
  _RAND_18 = {1{`RANDOM}};
  hold_9_data = _RAND_18[5:0];
  _RAND_19 = {1{`RANDOM}};
  hold_9_mseo = _RAND_19[1:0];
  _RAND_20 = {1{`RANDOM}};
  hold_10_data = _RAND_20[5:0];
  _RAND_21 = {1{`RANDOM}};
  hold_10_mseo = _RAND_21[1:0];
  _RAND_22 = {1{`RANDOM}};
  hold_11_data = _RAND_22[5:0];
  _RAND_23 = {1{`RANDOM}};
  hold_11_mseo = _RAND_23[1:0];
  _RAND_24 = {1{`RANDOM}};
  hold_12_data = _RAND_24[5:0];
  _RAND_25 = {1{`RANDOM}};
  hold_12_mseo = _RAND_25[1:0];
  _RAND_26 = {1{`RANDOM}};
  hold_13_data = _RAND_26[5:0];
  _RAND_27 = {1{`RANDOM}};
  hold_13_mseo = _RAND_27[1:0];
  _RAND_28 = {1{`RANDOM}};
  hold_14_data = _RAND_28[5:0];
  _RAND_29 = {1{`RANDOM}};
  hold_14_mseo = _RAND_29[1:0];
  _RAND_30 = {1{`RANDOM}};
  hold_15_data = _RAND_30[5:0];
  _RAND_31 = {1{`RANDOM}};
  hold_15_mseo = _RAND_31[1:0];
  _RAND_32 = {1{`RANDOM}};
  hold_16_data = _RAND_32[5:0];
  _RAND_33 = {1{`RANDOM}};
  hold_16_mseo = _RAND_33[1:0];
  _RAND_34 = {1{`RANDOM}};
  hold_17_data = _RAND_34[5:0];
  _RAND_35 = {1{`RANDOM}};
  hold_17_mseo = _RAND_35[1:0];
  _RAND_36 = {1{`RANDOM}};
  hold_18_data = _RAND_36[5:0];
  _RAND_37 = {1{`RANDOM}};
  hold_18_mseo = _RAND_37[1:0];
  _RAND_38 = {1{`RANDOM}};
  hold_19_data = _RAND_38[5:0];
  _RAND_39 = {1{`RANDOM}};
  hold_19_mseo = _RAND_39[1:0];
  _RAND_40 = {1{`RANDOM}};
  hold_20_data = _RAND_40[5:0];
  _RAND_41 = {1{`RANDOM}};
  hold_20_mseo = _RAND_41[1:0];
  _RAND_42 = {1{`RANDOM}};
  hold_21_data = _RAND_42[5:0];
  _RAND_43 = {1{`RANDOM}};
  hold_21_mseo = _RAND_43[1:0];
  _RAND_44 = {1{`RANDOM}};
  hold_22_data = _RAND_44[5:0];
  _RAND_45 = {1{`RANDOM}};
  hold_22_mseo = _RAND_45[1:0];
  _RAND_46 = {1{`RANDOM}};
  hold_23_data = _RAND_46[5:0];
  _RAND_47 = {1{`RANDOM}};
  hold_23_mseo = _RAND_47[1:0];
  _RAND_48 = {1{`RANDOM}};
  holdV = _RAND_48[4:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    hold_0_data = 6'h0;
  end
  if (rf_reset) begin
    hold_0_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_1_data = 6'h0;
  end
  if (rf_reset) begin
    hold_1_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_2_data = 6'h0;
  end
  if (rf_reset) begin
    hold_2_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_3_data = 6'h0;
  end
  if (rf_reset) begin
    hold_3_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_4_data = 6'h0;
  end
  if (rf_reset) begin
    hold_4_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_5_data = 6'h0;
  end
  if (rf_reset) begin
    hold_5_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_6_data = 6'h0;
  end
  if (rf_reset) begin
    hold_6_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_7_data = 6'h0;
  end
  if (rf_reset) begin
    hold_7_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_8_data = 6'h0;
  end
  if (rf_reset) begin
    hold_8_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_9_data = 6'h0;
  end
  if (rf_reset) begin
    hold_9_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_10_data = 6'h0;
  end
  if (rf_reset) begin
    hold_10_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_11_data = 6'h0;
  end
  if (rf_reset) begin
    hold_11_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_12_data = 6'h0;
  end
  if (rf_reset) begin
    hold_12_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_13_data = 6'h0;
  end
  if (rf_reset) begin
    hold_13_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_14_data = 6'h0;
  end
  if (rf_reset) begin
    hold_14_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_15_data = 6'h0;
  end
  if (rf_reset) begin
    hold_15_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_16_data = 6'h0;
  end
  if (rf_reset) begin
    hold_16_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_17_data = 6'h0;
  end
  if (rf_reset) begin
    hold_17_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_18_data = 6'h0;
  end
  if (rf_reset) begin
    hold_18_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_19_data = 6'h0;
  end
  if (rf_reset) begin
    hold_19_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_20_data = 6'h0;
  end
  if (rf_reset) begin
    hold_20_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_21_data = 6'h0;
  end
  if (rf_reset) begin
    hold_21_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_22_data = 6'h0;
  end
  if (rf_reset) begin
    hold_22_mseo = 2'h0;
  end
  if (rf_reset) begin
    hold_23_data = 6'h0;
  end
  if (rf_reset) begin
    hold_23_mseo = 2'h0;
  end
  if (reset) begin
    holdV = 5'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
