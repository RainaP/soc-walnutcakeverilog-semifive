//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLRationalCrossingSink(
  input          rf_reset,
  input          clock,
  input          reset,
  input  [2:0]   auto_in_a_bits0_opcode,
  input  [2:0]   auto_in_a_bits0_param,
  input  [3:0]   auto_in_a_bits0_size,
  input  [4:0]   auto_in_a_bits0_source,
  input  [36:0]  auto_in_a_bits0_address,
  input          auto_in_a_bits0_user_amba_prot_bufferable,
  input          auto_in_a_bits0_user_amba_prot_modifiable,
  input          auto_in_a_bits0_user_amba_prot_readalloc,
  input          auto_in_a_bits0_user_amba_prot_writealloc,
  input          auto_in_a_bits0_user_amba_prot_privileged,
  input          auto_in_a_bits0_user_amba_prot_secure,
  input          auto_in_a_bits0_user_amba_prot_fetch,
  input  [15:0]  auto_in_a_bits0_mask,
  input  [127:0] auto_in_a_bits0_data,
  input          auto_in_a_bits0_corrupt,
  input  [2:0]   auto_in_a_bits1_opcode,
  input  [2:0]   auto_in_a_bits1_param,
  input  [3:0]   auto_in_a_bits1_size,
  input  [4:0]   auto_in_a_bits1_source,
  input  [36:0]  auto_in_a_bits1_address,
  input          auto_in_a_bits1_user_amba_prot_bufferable,
  input          auto_in_a_bits1_user_amba_prot_modifiable,
  input          auto_in_a_bits1_user_amba_prot_readalloc,
  input          auto_in_a_bits1_user_amba_prot_writealloc,
  input          auto_in_a_bits1_user_amba_prot_privileged,
  input          auto_in_a_bits1_user_amba_prot_secure,
  input          auto_in_a_bits1_user_amba_prot_fetch,
  input  [15:0]  auto_in_a_bits1_mask,
  input  [127:0] auto_in_a_bits1_data,
  input          auto_in_a_bits1_corrupt,
  input          auto_in_a_valid,
  input  [1:0]   auto_in_a_source,
  output         auto_in_a_ready,
  output [1:0]   auto_in_a_sink,
  output [2:0]   auto_in_b_bits0_opcode,
  output [1:0]   auto_in_b_bits0_param,
  output [3:0]   auto_in_b_bits0_size,
  output [4:0]   auto_in_b_bits0_source,
  output [36:0]  auto_in_b_bits0_address,
  output [15:0]  auto_in_b_bits0_mask,
  output [127:0] auto_in_b_bits0_data,
  output         auto_in_b_bits0_corrupt,
  output [2:0]   auto_in_b_bits1_opcode,
  output [1:0]   auto_in_b_bits1_param,
  output [3:0]   auto_in_b_bits1_size,
  output [4:0]   auto_in_b_bits1_source,
  output [36:0]  auto_in_b_bits1_address,
  output [15:0]  auto_in_b_bits1_mask,
  output [127:0] auto_in_b_bits1_data,
  output         auto_in_b_bits1_corrupt,
  output         auto_in_b_valid,
  output [1:0]   auto_in_b_source,
  input          auto_in_b_ready,
  input  [1:0]   auto_in_b_sink,
  input  [2:0]   auto_in_c_bits0_opcode,
  input  [2:0]   auto_in_c_bits0_param,
  input  [3:0]   auto_in_c_bits0_size,
  input  [4:0]   auto_in_c_bits0_source,
  input  [36:0]  auto_in_c_bits0_address,
  input  [127:0] auto_in_c_bits0_data,
  input          auto_in_c_bits0_corrupt,
  input  [2:0]   auto_in_c_bits1_opcode,
  input  [2:0]   auto_in_c_bits1_param,
  input  [3:0]   auto_in_c_bits1_size,
  input  [4:0]   auto_in_c_bits1_source,
  input  [36:0]  auto_in_c_bits1_address,
  input  [127:0] auto_in_c_bits1_data,
  input          auto_in_c_bits1_corrupt,
  input          auto_in_c_valid,
  input  [1:0]   auto_in_c_source,
  output         auto_in_c_ready,
  output [1:0]   auto_in_c_sink,
  output [2:0]   auto_in_d_bits0_opcode,
  output [1:0]   auto_in_d_bits0_param,
  output [3:0]   auto_in_d_bits0_size,
  output [4:0]   auto_in_d_bits0_source,
  output [4:0]   auto_in_d_bits0_sink,
  output         auto_in_d_bits0_denied,
  output [127:0] auto_in_d_bits0_data,
  output         auto_in_d_bits0_corrupt,
  output [2:0]   auto_in_d_bits1_opcode,
  output [1:0]   auto_in_d_bits1_param,
  output [3:0]   auto_in_d_bits1_size,
  output [4:0]   auto_in_d_bits1_source,
  output [4:0]   auto_in_d_bits1_sink,
  output         auto_in_d_bits1_denied,
  output [127:0] auto_in_d_bits1_data,
  output         auto_in_d_bits1_corrupt,
  output         auto_in_d_valid,
  output [1:0]   auto_in_d_source,
  input          auto_in_d_ready,
  input  [1:0]   auto_in_d_sink,
  input  [4:0]   auto_in_e_bits0_sink,
  input  [4:0]   auto_in_e_bits1_sink,
  input          auto_in_e_valid,
  input  [1:0]   auto_in_e_source,
  output         auto_in_e_ready,
  output [1:0]   auto_in_e_sink,
  input          auto_out_a_ready,
  output         auto_out_a_valid,
  output [2:0]   auto_out_a_bits_opcode,
  output [2:0]   auto_out_a_bits_param,
  output [3:0]   auto_out_a_bits_size,
  output [4:0]   auto_out_a_bits_source,
  output [36:0]  auto_out_a_bits_address,
  output         auto_out_a_bits_user_amba_prot_bufferable,
  output         auto_out_a_bits_user_amba_prot_modifiable,
  output         auto_out_a_bits_user_amba_prot_readalloc,
  output         auto_out_a_bits_user_amba_prot_writealloc,
  output         auto_out_a_bits_user_amba_prot_privileged,
  output         auto_out_a_bits_user_amba_prot_secure,
  output         auto_out_a_bits_user_amba_prot_fetch,
  output [15:0]  auto_out_a_bits_mask,
  output [127:0] auto_out_a_bits_data,
  output         auto_out_a_bits_corrupt,
  output         auto_out_b_ready,
  input          auto_out_b_valid,
  input  [2:0]   auto_out_b_bits_opcode,
  input  [1:0]   auto_out_b_bits_param,
  input  [3:0]   auto_out_b_bits_size,
  input  [4:0]   auto_out_b_bits_source,
  input  [36:0]  auto_out_b_bits_address,
  input  [15:0]  auto_out_b_bits_mask,
  input  [127:0] auto_out_b_bits_data,
  input          auto_out_b_bits_corrupt,
  input          auto_out_c_ready,
  output         auto_out_c_valid,
  output [2:0]   auto_out_c_bits_opcode,
  output [2:0]   auto_out_c_bits_param,
  output [3:0]   auto_out_c_bits_size,
  output [4:0]   auto_out_c_bits_source,
  output [36:0]  auto_out_c_bits_address,
  output [127:0] auto_out_c_bits_data,
  output         auto_out_c_bits_corrupt,
  output         auto_out_d_ready,
  input          auto_out_d_valid,
  input  [2:0]   auto_out_d_bits_opcode,
  input  [1:0]   auto_out_d_bits_param,
  input  [3:0]   auto_out_d_bits_size,
  input  [4:0]   auto_out_d_bits_source,
  input  [4:0]   auto_out_d_bits_sink,
  input          auto_out_d_bits_denied,
  input  [127:0] auto_out_d_bits_data,
  input          auto_out_d_bits_corrupt,
  input          auto_out_e_ready,
  output         auto_out_e_valid,
  output [4:0]   auto_out_e_bits_sink
);
  wire  bundleOut_0_a_sink_rf_reset; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_clock; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_reset; // @[RationalCrossing.scala 167:22]
  wire [2:0] bundleOut_0_a_sink_io_enq_bits0_opcode; // @[RationalCrossing.scala 167:22]
  wire [2:0] bundleOut_0_a_sink_io_enq_bits0_param; // @[RationalCrossing.scala 167:22]
  wire [3:0] bundleOut_0_a_sink_io_enq_bits0_size; // @[RationalCrossing.scala 167:22]
  wire [4:0] bundleOut_0_a_sink_io_enq_bits0_source; // @[RationalCrossing.scala 167:22]
  wire [36:0] bundleOut_0_a_sink_io_enq_bits0_address; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_bufferable; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_modifiable; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_readalloc; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_writealloc; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_privileged; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_secure; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_fetch; // @[RationalCrossing.scala 167:22]
  wire [15:0] bundleOut_0_a_sink_io_enq_bits0_mask; // @[RationalCrossing.scala 167:22]
  wire [127:0] bundleOut_0_a_sink_io_enq_bits0_data; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits0_corrupt; // @[RationalCrossing.scala 167:22]
  wire [2:0] bundleOut_0_a_sink_io_enq_bits1_opcode; // @[RationalCrossing.scala 167:22]
  wire [2:0] bundleOut_0_a_sink_io_enq_bits1_param; // @[RationalCrossing.scala 167:22]
  wire [3:0] bundleOut_0_a_sink_io_enq_bits1_size; // @[RationalCrossing.scala 167:22]
  wire [4:0] bundleOut_0_a_sink_io_enq_bits1_source; // @[RationalCrossing.scala 167:22]
  wire [36:0] bundleOut_0_a_sink_io_enq_bits1_address; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_bufferable; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_modifiable; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_readalloc; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_writealloc; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_privileged; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_secure; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_fetch; // @[RationalCrossing.scala 167:22]
  wire [15:0] bundleOut_0_a_sink_io_enq_bits1_mask; // @[RationalCrossing.scala 167:22]
  wire [127:0] bundleOut_0_a_sink_io_enq_bits1_data; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_bits1_corrupt; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_valid; // @[RationalCrossing.scala 167:22]
  wire [1:0] bundleOut_0_a_sink_io_enq_source; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_enq_ready; // @[RationalCrossing.scala 167:22]
  wire [1:0] bundleOut_0_a_sink_io_enq_sink; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_deq_ready; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_deq_valid; // @[RationalCrossing.scala 167:22]
  wire [2:0] bundleOut_0_a_sink_io_deq_bits_opcode; // @[RationalCrossing.scala 167:22]
  wire [2:0] bundleOut_0_a_sink_io_deq_bits_param; // @[RationalCrossing.scala 167:22]
  wire [3:0] bundleOut_0_a_sink_io_deq_bits_size; // @[RationalCrossing.scala 167:22]
  wire [4:0] bundleOut_0_a_sink_io_deq_bits_source; // @[RationalCrossing.scala 167:22]
  wire [36:0] bundleOut_0_a_sink_io_deq_bits_address; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_deq_bits_user_amba_prot_bufferable; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_deq_bits_user_amba_prot_modifiable; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_deq_bits_user_amba_prot_readalloc; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_deq_bits_user_amba_prot_writealloc; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_deq_bits_user_amba_prot_privileged; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_deq_bits_user_amba_prot_secure; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_deq_bits_user_amba_prot_fetch; // @[RationalCrossing.scala 167:22]
  wire [15:0] bundleOut_0_a_sink_io_deq_bits_mask; // @[RationalCrossing.scala 167:22]
  wire [127:0] bundleOut_0_a_sink_io_deq_bits_data; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_a_sink_io_deq_bits_corrupt; // @[RationalCrossing.scala 167:22]
  wire  bundleIn_0_d_source_rf_reset; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_d_source_clock; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_d_source_reset; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_d_source_io_enq_ready; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_d_source_io_enq_valid; // @[RationalCrossing.scala 158:24]
  wire [2:0] bundleIn_0_d_source_io_enq_bits_opcode; // @[RationalCrossing.scala 158:24]
  wire [1:0] bundleIn_0_d_source_io_enq_bits_param; // @[RationalCrossing.scala 158:24]
  wire [3:0] bundleIn_0_d_source_io_enq_bits_size; // @[RationalCrossing.scala 158:24]
  wire [4:0] bundleIn_0_d_source_io_enq_bits_source; // @[RationalCrossing.scala 158:24]
  wire [4:0] bundleIn_0_d_source_io_enq_bits_sink; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_d_source_io_enq_bits_denied; // @[RationalCrossing.scala 158:24]
  wire [127:0] bundleIn_0_d_source_io_enq_bits_data; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_d_source_io_enq_bits_corrupt; // @[RationalCrossing.scala 158:24]
  wire [2:0] bundleIn_0_d_source_io_deq_bits0_opcode; // @[RationalCrossing.scala 158:24]
  wire [1:0] bundleIn_0_d_source_io_deq_bits0_param; // @[RationalCrossing.scala 158:24]
  wire [3:0] bundleIn_0_d_source_io_deq_bits0_size; // @[RationalCrossing.scala 158:24]
  wire [4:0] bundleIn_0_d_source_io_deq_bits0_source; // @[RationalCrossing.scala 158:24]
  wire [4:0] bundleIn_0_d_source_io_deq_bits0_sink; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_d_source_io_deq_bits0_denied; // @[RationalCrossing.scala 158:24]
  wire [127:0] bundleIn_0_d_source_io_deq_bits0_data; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_d_source_io_deq_bits0_corrupt; // @[RationalCrossing.scala 158:24]
  wire [2:0] bundleIn_0_d_source_io_deq_bits1_opcode; // @[RationalCrossing.scala 158:24]
  wire [1:0] bundleIn_0_d_source_io_deq_bits1_param; // @[RationalCrossing.scala 158:24]
  wire [3:0] bundleIn_0_d_source_io_deq_bits1_size; // @[RationalCrossing.scala 158:24]
  wire [4:0] bundleIn_0_d_source_io_deq_bits1_source; // @[RationalCrossing.scala 158:24]
  wire [4:0] bundleIn_0_d_source_io_deq_bits1_sink; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_d_source_io_deq_bits1_denied; // @[RationalCrossing.scala 158:24]
  wire [127:0] bundleIn_0_d_source_io_deq_bits1_data; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_d_source_io_deq_bits1_corrupt; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_d_source_io_deq_valid; // @[RationalCrossing.scala 158:24]
  wire [1:0] bundleIn_0_d_source_io_deq_source; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_d_source_io_deq_ready; // @[RationalCrossing.scala 158:24]
  wire [1:0] bundleIn_0_d_source_io_deq_sink; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_b_source_rf_reset; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_b_source_clock; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_b_source_reset; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_b_source_io_enq_ready; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_b_source_io_enq_valid; // @[RationalCrossing.scala 158:24]
  wire [2:0] bundleIn_0_b_source_io_enq_bits_opcode; // @[RationalCrossing.scala 158:24]
  wire [1:0] bundleIn_0_b_source_io_enq_bits_param; // @[RationalCrossing.scala 158:24]
  wire [3:0] bundleIn_0_b_source_io_enq_bits_size; // @[RationalCrossing.scala 158:24]
  wire [4:0] bundleIn_0_b_source_io_enq_bits_source; // @[RationalCrossing.scala 158:24]
  wire [36:0] bundleIn_0_b_source_io_enq_bits_address; // @[RationalCrossing.scala 158:24]
  wire [15:0] bundleIn_0_b_source_io_enq_bits_mask; // @[RationalCrossing.scala 158:24]
  wire [127:0] bundleIn_0_b_source_io_enq_bits_data; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_b_source_io_enq_bits_corrupt; // @[RationalCrossing.scala 158:24]
  wire [2:0] bundleIn_0_b_source_io_deq_bits0_opcode; // @[RationalCrossing.scala 158:24]
  wire [1:0] bundleIn_0_b_source_io_deq_bits0_param; // @[RationalCrossing.scala 158:24]
  wire [3:0] bundleIn_0_b_source_io_deq_bits0_size; // @[RationalCrossing.scala 158:24]
  wire [4:0] bundleIn_0_b_source_io_deq_bits0_source; // @[RationalCrossing.scala 158:24]
  wire [36:0] bundleIn_0_b_source_io_deq_bits0_address; // @[RationalCrossing.scala 158:24]
  wire [15:0] bundleIn_0_b_source_io_deq_bits0_mask; // @[RationalCrossing.scala 158:24]
  wire [127:0] bundleIn_0_b_source_io_deq_bits0_data; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_b_source_io_deq_bits0_corrupt; // @[RationalCrossing.scala 158:24]
  wire [2:0] bundleIn_0_b_source_io_deq_bits1_opcode; // @[RationalCrossing.scala 158:24]
  wire [1:0] bundleIn_0_b_source_io_deq_bits1_param; // @[RationalCrossing.scala 158:24]
  wire [3:0] bundleIn_0_b_source_io_deq_bits1_size; // @[RationalCrossing.scala 158:24]
  wire [4:0] bundleIn_0_b_source_io_deq_bits1_source; // @[RationalCrossing.scala 158:24]
  wire [36:0] bundleIn_0_b_source_io_deq_bits1_address; // @[RationalCrossing.scala 158:24]
  wire [15:0] bundleIn_0_b_source_io_deq_bits1_mask; // @[RationalCrossing.scala 158:24]
  wire [127:0] bundleIn_0_b_source_io_deq_bits1_data; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_b_source_io_deq_bits1_corrupt; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_b_source_io_deq_valid; // @[RationalCrossing.scala 158:24]
  wire [1:0] bundleIn_0_b_source_io_deq_source; // @[RationalCrossing.scala 158:24]
  wire  bundleIn_0_b_source_io_deq_ready; // @[RationalCrossing.scala 158:24]
  wire [1:0] bundleIn_0_b_source_io_deq_sink; // @[RationalCrossing.scala 158:24]
  wire  bundleOut_0_c_sink_rf_reset; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_c_sink_clock; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_c_sink_reset; // @[RationalCrossing.scala 167:22]
  wire [2:0] bundleOut_0_c_sink_io_enq_bits0_opcode; // @[RationalCrossing.scala 167:22]
  wire [2:0] bundleOut_0_c_sink_io_enq_bits0_param; // @[RationalCrossing.scala 167:22]
  wire [3:0] bundleOut_0_c_sink_io_enq_bits0_size; // @[RationalCrossing.scala 167:22]
  wire [4:0] bundleOut_0_c_sink_io_enq_bits0_source; // @[RationalCrossing.scala 167:22]
  wire [36:0] bundleOut_0_c_sink_io_enq_bits0_address; // @[RationalCrossing.scala 167:22]
  wire [127:0] bundleOut_0_c_sink_io_enq_bits0_data; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_c_sink_io_enq_bits0_corrupt; // @[RationalCrossing.scala 167:22]
  wire [2:0] bundleOut_0_c_sink_io_enq_bits1_opcode; // @[RationalCrossing.scala 167:22]
  wire [2:0] bundleOut_0_c_sink_io_enq_bits1_param; // @[RationalCrossing.scala 167:22]
  wire [3:0] bundleOut_0_c_sink_io_enq_bits1_size; // @[RationalCrossing.scala 167:22]
  wire [4:0] bundleOut_0_c_sink_io_enq_bits1_source; // @[RationalCrossing.scala 167:22]
  wire [36:0] bundleOut_0_c_sink_io_enq_bits1_address; // @[RationalCrossing.scala 167:22]
  wire [127:0] bundleOut_0_c_sink_io_enq_bits1_data; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_c_sink_io_enq_bits1_corrupt; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_c_sink_io_enq_valid; // @[RationalCrossing.scala 167:22]
  wire [1:0] bundleOut_0_c_sink_io_enq_source; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_c_sink_io_enq_ready; // @[RationalCrossing.scala 167:22]
  wire [1:0] bundleOut_0_c_sink_io_enq_sink; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_c_sink_io_deq_ready; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_c_sink_io_deq_valid; // @[RationalCrossing.scala 167:22]
  wire [2:0] bundleOut_0_c_sink_io_deq_bits_opcode; // @[RationalCrossing.scala 167:22]
  wire [2:0] bundleOut_0_c_sink_io_deq_bits_param; // @[RationalCrossing.scala 167:22]
  wire [3:0] bundleOut_0_c_sink_io_deq_bits_size; // @[RationalCrossing.scala 167:22]
  wire [4:0] bundleOut_0_c_sink_io_deq_bits_source; // @[RationalCrossing.scala 167:22]
  wire [36:0] bundleOut_0_c_sink_io_deq_bits_address; // @[RationalCrossing.scala 167:22]
  wire [127:0] bundleOut_0_c_sink_io_deq_bits_data; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_c_sink_io_deq_bits_corrupt; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_e_sink_rf_reset; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_e_sink_clock; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_e_sink_reset; // @[RationalCrossing.scala 167:22]
  wire [4:0] bundleOut_0_e_sink_io_enq_bits0_sink; // @[RationalCrossing.scala 167:22]
  wire [4:0] bundleOut_0_e_sink_io_enq_bits1_sink; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_e_sink_io_enq_valid; // @[RationalCrossing.scala 167:22]
  wire [1:0] bundleOut_0_e_sink_io_enq_source; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_e_sink_io_enq_ready; // @[RationalCrossing.scala 167:22]
  wire [1:0] bundleOut_0_e_sink_io_enq_sink; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_e_sink_io_deq_ready; // @[RationalCrossing.scala 167:22]
  wire  bundleOut_0_e_sink_io_deq_valid; // @[RationalCrossing.scala 167:22]
  wire [4:0] bundleOut_0_e_sink_io_deq_bits_sink; // @[RationalCrossing.scala 167:22]
  RHEA__RationalCrossingSink bundleOut_0_a_sink ( // @[RationalCrossing.scala 167:22]
    .rf_reset(bundleOut_0_a_sink_rf_reset),
    .clock(bundleOut_0_a_sink_clock),
    .reset(bundleOut_0_a_sink_reset),
    .io_enq_bits0_opcode(bundleOut_0_a_sink_io_enq_bits0_opcode),
    .io_enq_bits0_param(bundleOut_0_a_sink_io_enq_bits0_param),
    .io_enq_bits0_size(bundleOut_0_a_sink_io_enq_bits0_size),
    .io_enq_bits0_source(bundleOut_0_a_sink_io_enq_bits0_source),
    .io_enq_bits0_address(bundleOut_0_a_sink_io_enq_bits0_address),
    .io_enq_bits0_user_amba_prot_bufferable(bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_bufferable),
    .io_enq_bits0_user_amba_prot_modifiable(bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_modifiable),
    .io_enq_bits0_user_amba_prot_readalloc(bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_readalloc),
    .io_enq_bits0_user_amba_prot_writealloc(bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_writealloc),
    .io_enq_bits0_user_amba_prot_privileged(bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_privileged),
    .io_enq_bits0_user_amba_prot_secure(bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_secure),
    .io_enq_bits0_user_amba_prot_fetch(bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_fetch),
    .io_enq_bits0_mask(bundleOut_0_a_sink_io_enq_bits0_mask),
    .io_enq_bits0_data(bundleOut_0_a_sink_io_enq_bits0_data),
    .io_enq_bits0_corrupt(bundleOut_0_a_sink_io_enq_bits0_corrupt),
    .io_enq_bits1_opcode(bundleOut_0_a_sink_io_enq_bits1_opcode),
    .io_enq_bits1_param(bundleOut_0_a_sink_io_enq_bits1_param),
    .io_enq_bits1_size(bundleOut_0_a_sink_io_enq_bits1_size),
    .io_enq_bits1_source(bundleOut_0_a_sink_io_enq_bits1_source),
    .io_enq_bits1_address(bundleOut_0_a_sink_io_enq_bits1_address),
    .io_enq_bits1_user_amba_prot_bufferable(bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_bufferable),
    .io_enq_bits1_user_amba_prot_modifiable(bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_modifiable),
    .io_enq_bits1_user_amba_prot_readalloc(bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_readalloc),
    .io_enq_bits1_user_amba_prot_writealloc(bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_writealloc),
    .io_enq_bits1_user_amba_prot_privileged(bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_privileged),
    .io_enq_bits1_user_amba_prot_secure(bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_secure),
    .io_enq_bits1_user_amba_prot_fetch(bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_fetch),
    .io_enq_bits1_mask(bundleOut_0_a_sink_io_enq_bits1_mask),
    .io_enq_bits1_data(bundleOut_0_a_sink_io_enq_bits1_data),
    .io_enq_bits1_corrupt(bundleOut_0_a_sink_io_enq_bits1_corrupt),
    .io_enq_valid(bundleOut_0_a_sink_io_enq_valid),
    .io_enq_source(bundleOut_0_a_sink_io_enq_source),
    .io_enq_ready(bundleOut_0_a_sink_io_enq_ready),
    .io_enq_sink(bundleOut_0_a_sink_io_enq_sink),
    .io_deq_ready(bundleOut_0_a_sink_io_deq_ready),
    .io_deq_valid(bundleOut_0_a_sink_io_deq_valid),
    .io_deq_bits_opcode(bundleOut_0_a_sink_io_deq_bits_opcode),
    .io_deq_bits_param(bundleOut_0_a_sink_io_deq_bits_param),
    .io_deq_bits_size(bundleOut_0_a_sink_io_deq_bits_size),
    .io_deq_bits_source(bundleOut_0_a_sink_io_deq_bits_source),
    .io_deq_bits_address(bundleOut_0_a_sink_io_deq_bits_address),
    .io_deq_bits_user_amba_prot_bufferable(bundleOut_0_a_sink_io_deq_bits_user_amba_prot_bufferable),
    .io_deq_bits_user_amba_prot_modifiable(bundleOut_0_a_sink_io_deq_bits_user_amba_prot_modifiable),
    .io_deq_bits_user_amba_prot_readalloc(bundleOut_0_a_sink_io_deq_bits_user_amba_prot_readalloc),
    .io_deq_bits_user_amba_prot_writealloc(bundleOut_0_a_sink_io_deq_bits_user_amba_prot_writealloc),
    .io_deq_bits_user_amba_prot_privileged(bundleOut_0_a_sink_io_deq_bits_user_amba_prot_privileged),
    .io_deq_bits_user_amba_prot_secure(bundleOut_0_a_sink_io_deq_bits_user_amba_prot_secure),
    .io_deq_bits_user_amba_prot_fetch(bundleOut_0_a_sink_io_deq_bits_user_amba_prot_fetch),
    .io_deq_bits_mask(bundleOut_0_a_sink_io_deq_bits_mask),
    .io_deq_bits_data(bundleOut_0_a_sink_io_deq_bits_data),
    .io_deq_bits_corrupt(bundleOut_0_a_sink_io_deq_bits_corrupt)
  );
  RHEA__RationalCrossingSource bundleIn_0_d_source ( // @[RationalCrossing.scala 158:24]
    .rf_reset(bundleIn_0_d_source_rf_reset),
    .clock(bundleIn_0_d_source_clock),
    .reset(bundleIn_0_d_source_reset),
    .io_enq_ready(bundleIn_0_d_source_io_enq_ready),
    .io_enq_valid(bundleIn_0_d_source_io_enq_valid),
    .io_enq_bits_opcode(bundleIn_0_d_source_io_enq_bits_opcode),
    .io_enq_bits_param(bundleIn_0_d_source_io_enq_bits_param),
    .io_enq_bits_size(bundleIn_0_d_source_io_enq_bits_size),
    .io_enq_bits_source(bundleIn_0_d_source_io_enq_bits_source),
    .io_enq_bits_sink(bundleIn_0_d_source_io_enq_bits_sink),
    .io_enq_bits_denied(bundleIn_0_d_source_io_enq_bits_denied),
    .io_enq_bits_data(bundleIn_0_d_source_io_enq_bits_data),
    .io_enq_bits_corrupt(bundleIn_0_d_source_io_enq_bits_corrupt),
    .io_deq_bits0_opcode(bundleIn_0_d_source_io_deq_bits0_opcode),
    .io_deq_bits0_param(bundleIn_0_d_source_io_deq_bits0_param),
    .io_deq_bits0_size(bundleIn_0_d_source_io_deq_bits0_size),
    .io_deq_bits0_source(bundleIn_0_d_source_io_deq_bits0_source),
    .io_deq_bits0_sink(bundleIn_0_d_source_io_deq_bits0_sink),
    .io_deq_bits0_denied(bundleIn_0_d_source_io_deq_bits0_denied),
    .io_deq_bits0_data(bundleIn_0_d_source_io_deq_bits0_data),
    .io_deq_bits0_corrupt(bundleIn_0_d_source_io_deq_bits0_corrupt),
    .io_deq_bits1_opcode(bundleIn_0_d_source_io_deq_bits1_opcode),
    .io_deq_bits1_param(bundleIn_0_d_source_io_deq_bits1_param),
    .io_deq_bits1_size(bundleIn_0_d_source_io_deq_bits1_size),
    .io_deq_bits1_source(bundleIn_0_d_source_io_deq_bits1_source),
    .io_deq_bits1_sink(bundleIn_0_d_source_io_deq_bits1_sink),
    .io_deq_bits1_denied(bundleIn_0_d_source_io_deq_bits1_denied),
    .io_deq_bits1_data(bundleIn_0_d_source_io_deq_bits1_data),
    .io_deq_bits1_corrupt(bundleIn_0_d_source_io_deq_bits1_corrupt),
    .io_deq_valid(bundleIn_0_d_source_io_deq_valid),
    .io_deq_source(bundleIn_0_d_source_io_deq_source),
    .io_deq_ready(bundleIn_0_d_source_io_deq_ready),
    .io_deq_sink(bundleIn_0_d_source_io_deq_sink)
  );
  RHEA__RationalCrossingSource_1 bundleIn_0_b_source ( // @[RationalCrossing.scala 158:24]
    .rf_reset(bundleIn_0_b_source_rf_reset),
    .clock(bundleIn_0_b_source_clock),
    .reset(bundleIn_0_b_source_reset),
    .io_enq_ready(bundleIn_0_b_source_io_enq_ready),
    .io_enq_valid(bundleIn_0_b_source_io_enq_valid),
    .io_enq_bits_opcode(bundleIn_0_b_source_io_enq_bits_opcode),
    .io_enq_bits_param(bundleIn_0_b_source_io_enq_bits_param),
    .io_enq_bits_size(bundleIn_0_b_source_io_enq_bits_size),
    .io_enq_bits_source(bundleIn_0_b_source_io_enq_bits_source),
    .io_enq_bits_address(bundleIn_0_b_source_io_enq_bits_address),
    .io_enq_bits_mask(bundleIn_0_b_source_io_enq_bits_mask),
    .io_enq_bits_data(bundleIn_0_b_source_io_enq_bits_data),
    .io_enq_bits_corrupt(bundleIn_0_b_source_io_enq_bits_corrupt),
    .io_deq_bits0_opcode(bundleIn_0_b_source_io_deq_bits0_opcode),
    .io_deq_bits0_param(bundleIn_0_b_source_io_deq_bits0_param),
    .io_deq_bits0_size(bundleIn_0_b_source_io_deq_bits0_size),
    .io_deq_bits0_source(bundleIn_0_b_source_io_deq_bits0_source),
    .io_deq_bits0_address(bundleIn_0_b_source_io_deq_bits0_address),
    .io_deq_bits0_mask(bundleIn_0_b_source_io_deq_bits0_mask),
    .io_deq_bits0_data(bundleIn_0_b_source_io_deq_bits0_data),
    .io_deq_bits0_corrupt(bundleIn_0_b_source_io_deq_bits0_corrupt),
    .io_deq_bits1_opcode(bundleIn_0_b_source_io_deq_bits1_opcode),
    .io_deq_bits1_param(bundleIn_0_b_source_io_deq_bits1_param),
    .io_deq_bits1_size(bundleIn_0_b_source_io_deq_bits1_size),
    .io_deq_bits1_source(bundleIn_0_b_source_io_deq_bits1_source),
    .io_deq_bits1_address(bundleIn_0_b_source_io_deq_bits1_address),
    .io_deq_bits1_mask(bundleIn_0_b_source_io_deq_bits1_mask),
    .io_deq_bits1_data(bundleIn_0_b_source_io_deq_bits1_data),
    .io_deq_bits1_corrupt(bundleIn_0_b_source_io_deq_bits1_corrupt),
    .io_deq_valid(bundleIn_0_b_source_io_deq_valid),
    .io_deq_source(bundleIn_0_b_source_io_deq_source),
    .io_deq_ready(bundleIn_0_b_source_io_deq_ready),
    .io_deq_sink(bundleIn_0_b_source_io_deq_sink)
  );
  RHEA__RationalCrossingSink_1 bundleOut_0_c_sink ( // @[RationalCrossing.scala 167:22]
    .rf_reset(bundleOut_0_c_sink_rf_reset),
    .clock(bundleOut_0_c_sink_clock),
    .reset(bundleOut_0_c_sink_reset),
    .io_enq_bits0_opcode(bundleOut_0_c_sink_io_enq_bits0_opcode),
    .io_enq_bits0_param(bundleOut_0_c_sink_io_enq_bits0_param),
    .io_enq_bits0_size(bundleOut_0_c_sink_io_enq_bits0_size),
    .io_enq_bits0_source(bundleOut_0_c_sink_io_enq_bits0_source),
    .io_enq_bits0_address(bundleOut_0_c_sink_io_enq_bits0_address),
    .io_enq_bits0_data(bundleOut_0_c_sink_io_enq_bits0_data),
    .io_enq_bits0_corrupt(bundleOut_0_c_sink_io_enq_bits0_corrupt),
    .io_enq_bits1_opcode(bundleOut_0_c_sink_io_enq_bits1_opcode),
    .io_enq_bits1_param(bundleOut_0_c_sink_io_enq_bits1_param),
    .io_enq_bits1_size(bundleOut_0_c_sink_io_enq_bits1_size),
    .io_enq_bits1_source(bundleOut_0_c_sink_io_enq_bits1_source),
    .io_enq_bits1_address(bundleOut_0_c_sink_io_enq_bits1_address),
    .io_enq_bits1_data(bundleOut_0_c_sink_io_enq_bits1_data),
    .io_enq_bits1_corrupt(bundleOut_0_c_sink_io_enq_bits1_corrupt),
    .io_enq_valid(bundleOut_0_c_sink_io_enq_valid),
    .io_enq_source(bundleOut_0_c_sink_io_enq_source),
    .io_enq_ready(bundleOut_0_c_sink_io_enq_ready),
    .io_enq_sink(bundleOut_0_c_sink_io_enq_sink),
    .io_deq_ready(bundleOut_0_c_sink_io_deq_ready),
    .io_deq_valid(bundleOut_0_c_sink_io_deq_valid),
    .io_deq_bits_opcode(bundleOut_0_c_sink_io_deq_bits_opcode),
    .io_deq_bits_param(bundleOut_0_c_sink_io_deq_bits_param),
    .io_deq_bits_size(bundleOut_0_c_sink_io_deq_bits_size),
    .io_deq_bits_source(bundleOut_0_c_sink_io_deq_bits_source),
    .io_deq_bits_address(bundleOut_0_c_sink_io_deq_bits_address),
    .io_deq_bits_data(bundleOut_0_c_sink_io_deq_bits_data),
    .io_deq_bits_corrupt(bundleOut_0_c_sink_io_deq_bits_corrupt)
  );
  RHEA__RationalCrossingSink_2 bundleOut_0_e_sink ( // @[RationalCrossing.scala 167:22]
    .rf_reset(bundleOut_0_e_sink_rf_reset),
    .clock(bundleOut_0_e_sink_clock),
    .reset(bundleOut_0_e_sink_reset),
    .io_enq_bits0_sink(bundleOut_0_e_sink_io_enq_bits0_sink),
    .io_enq_bits1_sink(bundleOut_0_e_sink_io_enq_bits1_sink),
    .io_enq_valid(bundleOut_0_e_sink_io_enq_valid),
    .io_enq_source(bundleOut_0_e_sink_io_enq_source),
    .io_enq_ready(bundleOut_0_e_sink_io_enq_ready),
    .io_enq_sink(bundleOut_0_e_sink_io_enq_sink),
    .io_deq_ready(bundleOut_0_e_sink_io_deq_ready),
    .io_deq_valid(bundleOut_0_e_sink_io_deq_valid),
    .io_deq_bits_sink(bundleOut_0_e_sink_io_deq_bits_sink)
  );
  assign bundleOut_0_a_sink_rf_reset = rf_reset;
  assign bundleIn_0_d_source_rf_reset = rf_reset;
  assign bundleIn_0_b_source_rf_reset = rf_reset;
  assign bundleOut_0_c_sink_rf_reset = rf_reset;
  assign bundleOut_0_e_sink_rf_reset = rf_reset;
  assign auto_in_a_ready = bundleOut_0_a_sink_io_enq_ready; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 168:17]
  assign auto_in_a_sink = bundleOut_0_a_sink_io_enq_sink; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 168:17]
  assign auto_in_b_bits0_opcode = bundleIn_0_b_source_io_deq_bits0_opcode; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_bits0_param = bundleIn_0_b_source_io_deq_bits0_param; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_bits0_size = bundleIn_0_b_source_io_deq_bits0_size; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_bits0_source = bundleIn_0_b_source_io_deq_bits0_source; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_bits0_address = bundleIn_0_b_source_io_deq_bits0_address; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_bits0_mask = bundleIn_0_b_source_io_deq_bits0_mask; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_bits0_data = bundleIn_0_b_source_io_deq_bits0_data; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_bits0_corrupt = bundleIn_0_b_source_io_deq_bits0_corrupt; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_bits1_opcode = bundleIn_0_b_source_io_deq_bits1_opcode; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_bits1_param = bundleIn_0_b_source_io_deq_bits1_param; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_bits1_size = bundleIn_0_b_source_io_deq_bits1_size; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_bits1_source = bundleIn_0_b_source_io_deq_bits1_source; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_bits1_address = bundleIn_0_b_source_io_deq_bits1_address; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_bits1_mask = bundleIn_0_b_source_io_deq_bits1_mask; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_bits1_data = bundleIn_0_b_source_io_deq_bits1_data; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_bits1_corrupt = bundleIn_0_b_source_io_deq_bits1_corrupt; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_valid = bundleIn_0_b_source_io_deq_valid; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_b_source = bundleIn_0_b_source_io_deq_source; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 61:14]
  assign auto_in_c_ready = bundleOut_0_c_sink_io_enq_ready; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 168:17]
  assign auto_in_c_sink = bundleOut_0_c_sink_io_enq_sink; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 168:17]
  assign auto_in_d_bits0_opcode = bundleIn_0_d_source_io_deq_bits0_opcode; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_bits0_param = bundleIn_0_d_source_io_deq_bits0_param; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_bits0_size = bundleIn_0_d_source_io_deq_bits0_size; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_bits0_source = bundleIn_0_d_source_io_deq_bits0_source; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_bits0_sink = bundleIn_0_d_source_io_deq_bits0_sink; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_bits0_denied = bundleIn_0_d_source_io_deq_bits0_denied; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_bits0_data = bundleIn_0_d_source_io_deq_bits0_data; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_bits0_corrupt = bundleIn_0_d_source_io_deq_bits0_corrupt; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_bits1_opcode = bundleIn_0_d_source_io_deq_bits1_opcode; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_bits1_param = bundleIn_0_d_source_io_deq_bits1_param; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_bits1_size = bundleIn_0_d_source_io_deq_bits1_size; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_bits1_source = bundleIn_0_d_source_io_deq_bits1_source; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_bits1_sink = bundleIn_0_d_source_io_deq_bits1_sink; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_bits1_denied = bundleIn_0_d_source_io_deq_bits1_denied; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_bits1_data = bundleIn_0_d_source_io_deq_bits1_data; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_bits1_corrupt = bundleIn_0_d_source_io_deq_bits1_corrupt; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_valid = bundleIn_0_d_source_io_deq_valid; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_d_source = bundleIn_0_d_source_io_deq_source; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 58:12]
  assign auto_in_e_ready = bundleOut_0_e_sink_io_enq_ready; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 168:17]
  assign auto_in_e_sink = bundleOut_0_e_sink_io_enq_sink; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 168:17]
  assign auto_out_a_valid = bundleOut_0_a_sink_io_deq_valid; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_a_bits_opcode = bundleOut_0_a_sink_io_deq_bits_opcode; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_a_bits_param = bundleOut_0_a_sink_io_deq_bits_param; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_a_bits_size = bundleOut_0_a_sink_io_deq_bits_size; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_a_bits_source = bundleOut_0_a_sink_io_deq_bits_source; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_a_bits_address = bundleOut_0_a_sink_io_deq_bits_address; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_a_bits_user_amba_prot_bufferable = bundleOut_0_a_sink_io_deq_bits_user_amba_prot_bufferable; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_a_bits_user_amba_prot_modifiable = bundleOut_0_a_sink_io_deq_bits_user_amba_prot_modifiable; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_a_bits_user_amba_prot_readalloc = bundleOut_0_a_sink_io_deq_bits_user_amba_prot_readalloc; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_a_bits_user_amba_prot_writealloc = bundleOut_0_a_sink_io_deq_bits_user_amba_prot_writealloc; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_a_bits_user_amba_prot_privileged = bundleOut_0_a_sink_io_deq_bits_user_amba_prot_privileged; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_a_bits_user_amba_prot_secure = bundleOut_0_a_sink_io_deq_bits_user_amba_prot_secure; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_a_bits_user_amba_prot_fetch = bundleOut_0_a_sink_io_deq_bits_user_amba_prot_fetch; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_a_bits_mask = bundleOut_0_a_sink_io_deq_bits_mask; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_a_bits_data = bundleOut_0_a_sink_io_deq_bits_data; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_a_bits_corrupt = bundleOut_0_a_sink_io_deq_bits_corrupt; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 57:13]
  assign auto_out_b_ready = bundleIn_0_b_source_io_enq_ready; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 159:19]
  assign auto_out_c_valid = bundleOut_0_c_sink_io_deq_valid; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 62:15]
  assign auto_out_c_bits_opcode = bundleOut_0_c_sink_io_deq_bits_opcode; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 62:15]
  assign auto_out_c_bits_param = bundleOut_0_c_sink_io_deq_bits_param; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 62:15]
  assign auto_out_c_bits_size = bundleOut_0_c_sink_io_deq_bits_size; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 62:15]
  assign auto_out_c_bits_source = bundleOut_0_c_sink_io_deq_bits_source; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 62:15]
  assign auto_out_c_bits_address = bundleOut_0_c_sink_io_deq_bits_address; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 62:15]
  assign auto_out_c_bits_data = bundleOut_0_c_sink_io_deq_bits_data; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 62:15]
  assign auto_out_c_bits_corrupt = bundleOut_0_c_sink_io_deq_bits_corrupt; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 62:15]
  assign auto_out_d_ready = bundleIn_0_d_source_io_enq_ready; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 159:19]
  assign auto_out_e_valid = bundleOut_0_e_sink_io_deq_valid; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 63:15]
  assign auto_out_e_bits_sink = bundleOut_0_e_sink_io_deq_bits_sink; // @[RationalCrossing.scala 50:32 RationalCrossing.scala 63:15]
  assign bundleOut_0_a_sink_clock = clock;
  assign bundleOut_0_a_sink_reset = reset;
  assign bundleOut_0_a_sink_io_enq_bits0_opcode = auto_in_a_bits0_opcode; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits0_param = auto_in_a_bits0_param; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits0_size = auto_in_a_bits0_size; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits0_source = auto_in_a_bits0_source; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits0_address = auto_in_a_bits0_address; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_bufferable = auto_in_a_bits0_user_amba_prot_bufferable; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_modifiable = auto_in_a_bits0_user_amba_prot_modifiable; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_readalloc = auto_in_a_bits0_user_amba_prot_readalloc; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_writealloc = auto_in_a_bits0_user_amba_prot_writealloc; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_privileged = auto_in_a_bits0_user_amba_prot_privileged; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_secure = auto_in_a_bits0_user_amba_prot_secure; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits0_user_amba_prot_fetch = auto_in_a_bits0_user_amba_prot_fetch; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits0_mask = auto_in_a_bits0_mask; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits0_data = auto_in_a_bits0_data; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits0_corrupt = auto_in_a_bits0_corrupt; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits1_opcode = auto_in_a_bits1_opcode; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits1_param = auto_in_a_bits1_param; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits1_size = auto_in_a_bits1_size; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits1_source = auto_in_a_bits1_source; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits1_address = auto_in_a_bits1_address; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_bufferable = auto_in_a_bits1_user_amba_prot_bufferable; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_modifiable = auto_in_a_bits1_user_amba_prot_modifiable; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_readalloc = auto_in_a_bits1_user_amba_prot_readalloc; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_writealloc = auto_in_a_bits1_user_amba_prot_writealloc; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_privileged = auto_in_a_bits1_user_amba_prot_privileged; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_secure = auto_in_a_bits1_user_amba_prot_secure; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits1_user_amba_prot_fetch = auto_in_a_bits1_user_amba_prot_fetch; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits1_mask = auto_in_a_bits1_mask; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits1_data = auto_in_a_bits1_data; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_bits1_corrupt = auto_in_a_bits1_corrupt; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_valid = auto_in_a_valid; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_enq_source = auto_in_a_source; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_a_sink_io_deq_ready = auto_out_a_ready; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_d_source_clock = clock;
  assign bundleIn_0_d_source_reset = reset;
  assign bundleIn_0_d_source_io_enq_valid = auto_out_d_valid; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_d_source_io_enq_bits_opcode = auto_out_d_bits_opcode; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_d_source_io_enq_bits_param = auto_out_d_bits_param; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_d_source_io_enq_bits_size = auto_out_d_bits_size; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_d_source_io_enq_bits_source = auto_out_d_bits_source; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_d_source_io_enq_bits_sink = auto_out_d_bits_sink; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_d_source_io_enq_bits_denied = auto_out_d_bits_denied; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_d_source_io_enq_bits_data = auto_out_d_bits_data; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_d_source_io_enq_bits_corrupt = auto_out_d_bits_corrupt; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_d_source_io_deq_ready = auto_in_d_ready; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleIn_0_d_source_io_deq_sink = auto_in_d_sink; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleIn_0_b_source_clock = clock;
  assign bundleIn_0_b_source_reset = reset;
  assign bundleIn_0_b_source_io_enq_valid = auto_out_b_valid; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_b_source_io_enq_bits_opcode = auto_out_b_bits_opcode; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_b_source_io_enq_bits_param = auto_out_b_bits_param; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_b_source_io_enq_bits_size = auto_out_b_bits_size; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_b_source_io_enq_bits_source = auto_out_b_bits_source; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_b_source_io_enq_bits_address = auto_out_b_bits_address; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_b_source_io_enq_bits_mask = auto_out_b_bits_mask; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_b_source_io_enq_bits_data = auto_out_b_bits_data; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_b_source_io_enq_bits_corrupt = auto_out_b_bits_corrupt; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleIn_0_b_source_io_deq_ready = auto_in_b_ready; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleIn_0_b_source_io_deq_sink = auto_in_b_sink; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_clock = clock;
  assign bundleOut_0_c_sink_reset = reset;
  assign bundleOut_0_c_sink_io_enq_bits0_opcode = auto_in_c_bits0_opcode; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_enq_bits0_param = auto_in_c_bits0_param; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_enq_bits0_size = auto_in_c_bits0_size; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_enq_bits0_source = auto_in_c_bits0_source; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_enq_bits0_address = auto_in_c_bits0_address; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_enq_bits0_data = auto_in_c_bits0_data; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_enq_bits0_corrupt = auto_in_c_bits0_corrupt; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_enq_bits1_opcode = auto_in_c_bits1_opcode; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_enq_bits1_param = auto_in_c_bits1_param; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_enq_bits1_size = auto_in_c_bits1_size; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_enq_bits1_source = auto_in_c_bits1_source; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_enq_bits1_address = auto_in_c_bits1_address; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_enq_bits1_data = auto_in_c_bits1_data; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_enq_bits1_corrupt = auto_in_c_bits1_corrupt; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_enq_valid = auto_in_c_valid; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_enq_source = auto_in_c_source; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_c_sink_io_deq_ready = auto_out_c_ready; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
  assign bundleOut_0_e_sink_clock = clock;
  assign bundleOut_0_e_sink_reset = reset;
  assign bundleOut_0_e_sink_io_enq_bits0_sink = auto_in_e_bits0_sink; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_e_sink_io_enq_bits1_sink = auto_in_e_bits1_sink; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_e_sink_io_enq_valid = auto_in_e_valid; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_e_sink_io_enq_source = auto_in_e_source; // @[RationalCrossing.scala 50:32 LazyModule.scala 388:16]
  assign bundleOut_0_e_sink_io_deq_ready = auto_out_e_ready; // @[RationalCrossing.scala 50:32 LazyModule.scala 390:12]
endmodule
