//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TracePCSample(
  input         clock,
  input         reset,
  input         io_pcControlReg_pcActive,
  input         io_pcSampleRdEn,
  input         io_pcSampleHRdEn,
  input         io_ingPcCapture_valid,
  input  [38:0] io_ingPcCapture_bits,
  input         io_ingPcSample_valid,
  input  [38:0] io_ingPcSample_bits,
  output        io_pcCaptureValid,
  output        io_pcSampleValid,
  output [38:0] io_pcCapture,
  output [38:0] io_pcSample,
  output        io_pcActive
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [63:0] _RAND_2;
  reg [63:0] _RAND_3;
  reg [31:0] _RAND_4;
`endif // RANDOMIZE_REG_INIT
  reg  pcCaptureValid; // @[TracePCSample.scala 62:31]
  reg  pcSampleValid; // @[TracePCSample.scala 63:31]
  reg [38:0] pcCapture; // @[TracePCSample.scala 64:31]
  reg [38:0] pcSample; // @[TracePCSample.scala 65:31]
  reg  frozen; // @[TracePCSample.scala 66:31]
  wire  _GEN_0 = io_ingPcCapture_valid | pcCaptureValid; // @[TracePCSample.scala 69:32 TracePCSample.scala 70:20 TracePCSample.scala 62:31]
  wire  _GEN_2 = io_pcSampleHRdEn | frozen; // @[TracePCSample.scala 74:27 TracePCSample.scala 75:12 TracePCSample.scala 66:31]
  wire  _GEN_3 = io_pcSampleRdEn ? 1'h0 : pcSampleValid; // @[TracePCSample.scala 82:32 TracePCSample.scala 83:19 TracePCSample.scala 63:31]
  wire  _GEN_5 = io_ingPcSample_valid & ~io_pcSampleHRdEn & ~frozen | _GEN_3; // @[TracePCSample.scala 79:61 TracePCSample.scala 80:19]
  assign io_pcCaptureValid = pcCaptureValid; // @[TracePCSample.scala 87:21]
  assign io_pcSampleValid = pcSampleValid; // @[TracePCSample.scala 88:21]
  assign io_pcCapture = pcCapture; // @[TracePCSample.scala 89:21]
  assign io_pcSample = pcSample; // @[TracePCSample.scala 90:21]
  assign io_pcActive = io_pcControlReg_pcActive; // @[TracePCSample.scala 60:15]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pcCaptureValid <= 1'h0;
    end else if (~io_pcControlReg_pcActive) begin
      pcCaptureValid <= 1'h0;
    end else begin
      pcCaptureValid <= _GEN_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pcSampleValid <= 1'h0;
    end else if (~io_pcControlReg_pcActive) begin
      pcSampleValid <= 1'h0;
    end else begin
      pcSampleValid <= _GEN_5;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pcCapture <= 39'h0;
    end else if (~io_pcControlReg_pcActive) begin
      pcCapture <= 39'h0;
    end else if (io_ingPcCapture_valid) begin
      pcCapture <= io_ingPcCapture_bits;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pcSample <= 39'h0;
    end else if (~io_pcControlReg_pcActive) begin
      pcSample <= 39'h0;
    end else if (io_ingPcSample_valid & ~io_pcSampleHRdEn & ~frozen) begin
      pcSample <= io_ingPcSample_bits;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      frozen <= 1'h0;
    end else if (~io_pcControlReg_pcActive) begin
      frozen <= 1'h0;
    end else if (io_ingPcSample_valid & ~io_pcSampleHRdEn & ~frozen) begin
      frozen <= _GEN_2;
    end else if (io_pcSampleRdEn) begin
      frozen <= 1'h0;
    end else begin
      frozen <= _GEN_2;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  pcCaptureValid = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  pcSampleValid = _RAND_1[0:0];
  _RAND_2 = {2{`RANDOM}};
  pcCapture = _RAND_2[38:0];
  _RAND_3 = {2{`RANDOM}};
  pcSample = _RAND_3[38:0];
  _RAND_4 = {1{`RANDOM}};
  frozen = _RAND_4[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    pcCaptureValid = 1'h0;
  end
  if (reset) begin
    pcSampleValid = 1'h0;
  end
  if (reset) begin
    pcCapture = 39'h0;
  end
  if (reset) begin
    pcSample = 39'h0;
  end
  if (reset) begin
    frozen = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
