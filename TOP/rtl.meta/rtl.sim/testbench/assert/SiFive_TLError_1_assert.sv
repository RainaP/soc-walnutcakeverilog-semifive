//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLError_1_assert(
  input          clock,
  input          reset,
  input          auto_in_a_valid,
  input  [2:0]   auto_in_a_bits_opcode,
  input  [127:0] auto_in_a_bits_address,
  input  [3:0]   auto_in_a_bits_mask,
  input          auto_in_d_ready,
  input          idle,
  input          _T_114,
  input          da_valid,
  input          da_ready,
  input          _T_19,
  input          _T_128,
  input          _T_164,
  input  [42:0]  _T_183
);
  wire  TLMonitor_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [127:0] TLMonitor_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_d_bits_param; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  reg  _T_27; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire  _T_29; // @[Edges.scala 231:28]
  wire  da_first; // @[Edges.scala 232:25]
  wire  _T_36; // @[Error.scala 51:18]
  wire  _T_38; // @[Error.scala 51:12]
  wire  _T_39; // @[Error.scala 51:12]
  wire  _T_143; // @[Arbiter.scala 77:15]
  wire  _T_145; // @[Arbiter.scala 77:36]
  wire  _T_147; // @[Arbiter.scala 77:14]
  wire  _T_148; // @[Arbiter.scala 77:14]
  SiFive_TLMonitor_64_assert TLMonitor ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_clock),
    .reset(TLMonitor_reset),
    .io_in_a_ready(TLMonitor_io_in_a_ready),
    .io_in_a_valid(TLMonitor_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_io_in_a_bits_opcode),
    .io_in_a_bits_address(TLMonitor_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_io_in_a_bits_mask),
    .io_in_d_ready(TLMonitor_io_in_d_ready),
    .io_in_d_valid(TLMonitor_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_io_in_d_bits_opcode),
    .io_in_d_bits_param(TLMonitor_io_in_d_bits_param),
    .io_in_d_bits_size(TLMonitor_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_io_in_d_bits_source),
    .io_in_d_bits_sink(TLMonitor_io_in_d_bits_sink),
    .io_in_d_bits_denied(TLMonitor_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_io_in_d_bits_corrupt)
  );
  assign _T_29 = _T_27 - 1'h1; // @[Edges.scala 231:28]
  assign da_first = ~_T_27; // @[Edges.scala 232:25]
  assign _T_36 = idle | da_first; // @[Error.scala 51:18]
  assign _T_38 = _T_36 | reset; // @[Error.scala 51:12]
  assign _T_39 = ~_T_38; // @[Error.scala 51:12]
  assign _T_143 = ~da_valid; // @[Arbiter.scala 77:15]
  assign _T_145 = _T_143 | _T_128; // @[Arbiter.scala 77:36]
  assign _T_147 = _T_145 | reset; // @[Arbiter.scala 77:14]
  assign _T_148 = ~_T_147; // @[Arbiter.scala 77:14]
  assign TLMonitor_clock = clock;
  assign TLMonitor_reset = reset;
  assign TLMonitor_io_in_a_ready = da_ready & idle; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_valid = auto_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_ready = auto_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_valid = _T_114 ? da_valid : _T_164; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_opcode = _T_183[42:40]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_param = _T_183[39:38]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_size = _T_183[37:36]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_source = _T_183[35]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_sink = _T_183[34]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_denied = _T_183[33]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_corrupt = _T_183[0]; // @[Nodes.scala 26:19]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_27 = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  if (reset) begin
    _T_27 = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_39) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Error.scala:51 assert (idle || da_first) // we only send Grant, never GrantData => simplified flow control below\n"); // @[Error.scala 51:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_39) begin
          $fatal; // @[Error.scala 51:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_148) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_148) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_27 <= 1'h0;
    end else if (_T_19) begin
      if (da_first) begin
        _T_27 <= 1'h0;
      end else begin
        _T_27 <= _T_29;
      end
    end
  end

endmodule
