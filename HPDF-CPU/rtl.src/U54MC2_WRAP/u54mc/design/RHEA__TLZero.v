//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLZero(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_a_ready,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [2:0]  auto_in_a_bits_param,
  input  [2:0]  auto_in_a_bits_size,
  input  [8:0]  auto_in_a_bits_source,
  input  [27:0] auto_in_a_bits_address,
  input  [15:0] auto_in_a_bits_mask,
  input         auto_in_d_ready,
  output        auto_in_d_valid,
  output [2:0]  auto_in_d_bits_opcode,
  output [2:0]  auto_in_d_bits_size,
  output [8:0]  auto_in_d_bits_source
);
  wire  a_rf_reset; // @[Decoupled.scala 296:21]
  wire  a_clock; // @[Decoupled.scala 296:21]
  wire  a_reset; // @[Decoupled.scala 296:21]
  wire  a_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  a_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] a_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] a_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [8:0] a_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire  a_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  a_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] a_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] a_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [8:0] a_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] _GEN_2 = 3'h2 == a_io_deq_bits_opcode ? 3'h1 : 3'h0; // @[Zero.scala 56:22 Zero.scala 56:22]
  wire [2:0] _GEN_3 = 3'h3 == a_io_deq_bits_opcode ? 3'h1 : _GEN_2; // @[Zero.scala 56:22 Zero.scala 56:22]
  wire [2:0] _GEN_4 = 3'h4 == a_io_deq_bits_opcode ? 3'h1 : _GEN_3; // @[Zero.scala 56:22 Zero.scala 56:22]
  wire [2:0] _GEN_5 = 3'h5 == a_io_deq_bits_opcode ? 3'h2 : _GEN_4; // @[Zero.scala 56:22 Zero.scala 56:22]
  wire [2:0] _GEN_6 = 3'h6 == a_io_deq_bits_opcode ? 3'h4 : _GEN_5; // @[Zero.scala 56:22 Zero.scala 56:22]
  RHEA__Queue_50 a ( // @[Decoupled.scala 296:21]
    .rf_reset(a_rf_reset),
    .clock(a_clock),
    .reset(a_reset),
    .io_enq_ready(a_io_enq_ready),
    .io_enq_valid(a_io_enq_valid),
    .io_enq_bits_opcode(a_io_enq_bits_opcode),
    .io_enq_bits_size(a_io_enq_bits_size),
    .io_enq_bits_source(a_io_enq_bits_source),
    .io_deq_ready(a_io_deq_ready),
    .io_deq_valid(a_io_deq_valid),
    .io_deq_bits_opcode(a_io_deq_bits_opcode),
    .io_deq_bits_size(a_io_deq_bits_size),
    .io_deq_bits_source(a_io_deq_bits_source)
  );
  assign a_rf_reset = rf_reset;
  assign auto_in_a_ready = a_io_enq_ready; // @[DevNull.scala 36:27 Decoupled.scala 299:17]
  assign auto_in_d_valid = a_io_deq_valid; // @[DevNull.scala 36:27 Zero.scala 54:16]
  assign auto_in_d_bits_opcode = 3'h7 == a_io_deq_bits_opcode ? 3'h4 : _GEN_6; // @[Zero.scala 56:22 Zero.scala 56:22]
  assign auto_in_d_bits_size = a_io_deq_bits_size; // @[Edges.scala 755:17 Edges.scala 758:15]
  assign auto_in_d_bits_source = a_io_deq_bits_source; // @[Edges.scala 755:17 Edges.scala 759:15]
  assign a_clock = clock;
  assign a_reset = reset;
  assign a_io_enq_valid = auto_in_a_valid; // @[DevNull.scala 36:27 LazyModule.scala 388:16]
  assign a_io_enq_bits_opcode = auto_in_a_bits_opcode; // @[DevNull.scala 36:27 LazyModule.scala 388:16]
  assign a_io_enq_bits_size = auto_in_a_bits_size; // @[DevNull.scala 36:27 LazyModule.scala 388:16]
  assign a_io_enq_bits_source = auto_in_a_bits_source; // @[DevNull.scala 36:27 LazyModule.scala 388:16]
  assign a_io_deq_ready = auto_in_d_ready; // @[DevNull.scala 36:27 LazyModule.scala 388:16]
endmodule
