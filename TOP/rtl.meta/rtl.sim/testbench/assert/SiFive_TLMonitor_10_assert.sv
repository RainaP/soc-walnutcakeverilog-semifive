//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_10_assert(
  input         clock,
  input         reset,
  input         io_in_a_ready,
  input         io_in_a_valid,
  input  [2:0]  io_in_a_bits_opcode,
  input  [2:0]  io_in_a_bits_param,
  input  [1:0]  io_in_a_bits_size,
  input  [10:0] io_in_a_bits_source,
  input  [27:0] io_in_a_bits_address,
  input  [7:0]  io_in_a_bits_mask,
  input         io_in_a_bits_corrupt,
  input         io_in_d_ready,
  input         io_in_d_valid,
  input  [2:0]  io_in_d_bits_opcode,
  input  [1:0]  io_in_d_bits_param,
  input  [1:0]  io_in_d_bits_size,
  input  [10:0] io_in_d_bits_source,
  input         io_in_d_bits_sink,
  input         io_in_d_bits_denied,
  input         io_in_d_bits_corrupt
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire  _T_10; // @[Parameters.scala 58:20]
  wire [5:0] _T_14; // @[package.scala 189:77]
  wire [2:0] _T_16; // @[package.scala 189:46]
  wire [27:0] _GEN_18; // @[Edges.scala 22:16]
  wire [27:0] _T_17; // @[Edges.scala 22:16]
  wire  _T_18; // @[Edges.scala 22:24]
  wire [2:0] _T_19; // @[Misc.scala 201:34]
  wire [3:0] _T_21; // @[OneHot.scala 65:12]
  wire [2:0] _T_23; // @[Misc.scala 201:81]
  wire  _T_24; // @[Misc.scala 205:21]
  wire  _T_27; // @[Misc.scala 210:20]
  wire  _T_29; // @[Misc.scala 214:38]
  wire  _T_30; // @[Misc.scala 214:29]
  wire  _T_32; // @[Misc.scala 214:38]
  wire  _T_33; // @[Misc.scala 214:29]
  wire  _T_36; // @[Misc.scala 210:20]
  wire  _T_37; // @[Misc.scala 213:27]
  wire  _T_38; // @[Misc.scala 214:38]
  wire  _T_39; // @[Misc.scala 214:29]
  wire  _T_40; // @[Misc.scala 213:27]
  wire  _T_41; // @[Misc.scala 214:38]
  wire  _T_42; // @[Misc.scala 214:29]
  wire  _T_43; // @[Misc.scala 213:27]
  wire  _T_44; // @[Misc.scala 214:38]
  wire  _T_45; // @[Misc.scala 214:29]
  wire  _T_46; // @[Misc.scala 213:27]
  wire  _T_47; // @[Misc.scala 214:38]
  wire  _T_48; // @[Misc.scala 214:29]
  wire  _T_51; // @[Misc.scala 210:20]
  wire  _T_52; // @[Misc.scala 213:27]
  wire  _T_53; // @[Misc.scala 214:38]
  wire  _T_54; // @[Misc.scala 214:29]
  wire  _T_55; // @[Misc.scala 213:27]
  wire  _T_56; // @[Misc.scala 214:38]
  wire  _T_57; // @[Misc.scala 214:29]
  wire  _T_58; // @[Misc.scala 213:27]
  wire  _T_59; // @[Misc.scala 214:38]
  wire  _T_60; // @[Misc.scala 214:29]
  wire  _T_61; // @[Misc.scala 213:27]
  wire  _T_62; // @[Misc.scala 214:38]
  wire  _T_63; // @[Misc.scala 214:29]
  wire  _T_64; // @[Misc.scala 213:27]
  wire  _T_65; // @[Misc.scala 214:38]
  wire  _T_66; // @[Misc.scala 214:29]
  wire  _T_67; // @[Misc.scala 213:27]
  wire  _T_68; // @[Misc.scala 214:38]
  wire  _T_69; // @[Misc.scala 214:29]
  wire  _T_70; // @[Misc.scala 213:27]
  wire  _T_71; // @[Misc.scala 214:38]
  wire  _T_72; // @[Misc.scala 214:29]
  wire  _T_73; // @[Misc.scala 213:27]
  wire  _T_74; // @[Misc.scala 214:38]
  wire  _T_75; // @[Misc.scala 214:29]
  wire [7:0] _T_82; // @[Cat.scala 29:58]
  wire  _T_101; // @[Monitor.scala 79:25]
  wire [27:0] _T_103; // @[Parameters.scala 137:31]
  wire [28:0] _T_104; // @[Parameters.scala 137:49]
  wire [28:0] _T_106; // @[Parameters.scala 137:52]
  wire  _T_107; // @[Parameters.scala 137:67]
  wire  _T_112; // @[Monitor.scala 44:11]
  wire  _T_117; // @[Monitor.scala 44:11]
  wire  _T_118; // @[Monitor.scala 44:11]
  wire  _T_121; // @[Monitor.scala 44:11]
  wire  _T_122; // @[Monitor.scala 44:11]
  wire  _T_124; // @[Monitor.scala 44:11]
  wire  _T_125; // @[Monitor.scala 44:11]
  wire  _T_126; // @[Bundles.scala 110:27]
  wire  _T_128; // @[Monitor.scala 44:11]
  wire  _T_129; // @[Monitor.scala 44:11]
  wire [7:0] _T_130; // @[Monitor.scala 86:18]
  wire  _T_131; // @[Monitor.scala 86:31]
  wire  _T_133; // @[Monitor.scala 44:11]
  wire  _T_134; // @[Monitor.scala 44:11]
  wire  _T_135; // @[Monitor.scala 87:18]
  wire  _T_137; // @[Monitor.scala 44:11]
  wire  _T_138; // @[Monitor.scala 44:11]
  wire  _T_139; // @[Monitor.scala 90:25]
  wire  _T_168; // @[Monitor.scala 97:31]
  wire  _T_170; // @[Monitor.scala 44:11]
  wire  _T_171; // @[Monitor.scala 44:11]
  wire  _T_181; // @[Monitor.scala 102:25]
  wire  _T_194; // @[Monitor.scala 44:11]
  wire  _T_195; // @[Monitor.scala 44:11]
  wire  _T_202; // @[Monitor.scala 106:31]
  wire  _T_204; // @[Monitor.scala 44:11]
  wire  _T_205; // @[Monitor.scala 44:11]
  wire  _T_206; // @[Monitor.scala 107:30]
  wire  _T_208; // @[Monitor.scala 44:11]
  wire  _T_209; // @[Monitor.scala 44:11]
  wire  _T_214; // @[Monitor.scala 111:25]
  wire  _T_243; // @[Monitor.scala 119:25]
  wire [7:0] _T_268; // @[Monitor.scala 124:33]
  wire [7:0] _T_269; // @[Monitor.scala 124:31]
  wire  _T_270; // @[Monitor.scala 124:40]
  wire  _T_272; // @[Monitor.scala 44:11]
  wire  _T_273; // @[Monitor.scala 44:11]
  wire  _T_274; // @[Monitor.scala 127:25]
  wire  _T_295; // @[Bundles.scala 140:33]
  wire  _T_297; // @[Monitor.scala 44:11]
  wire  _T_298; // @[Monitor.scala 44:11]
  wire  _T_303; // @[Monitor.scala 135:25]
  wire  _T_324; // @[Bundles.scala 147:30]
  wire  _T_326; // @[Monitor.scala 44:11]
  wire  _T_327; // @[Monitor.scala 44:11]
  wire  _T_332; // @[Monitor.scala 143:25]
  wire  _T_350; // @[Bundles.scala 160:28]
  wire  _T_352; // @[Monitor.scala 44:11]
  wire  _T_353; // @[Monitor.scala 44:11]
  wire  _T_362; // @[Bundles.scala 44:24]
  wire  _T_364; // @[Monitor.scala 51:11]
  wire  _T_365; // @[Monitor.scala 51:11]
  wire  _T_372; // @[Parameters.scala 58:20]
  wire  _T_376; // @[Monitor.scala 307:25]
  wire  _T_378; // @[Monitor.scala 51:11]
  wire  _T_379; // @[Monitor.scala 51:11]
  wire  _T_380; // @[Monitor.scala 309:27]
  wire  _T_382; // @[Monitor.scala 51:11]
  wire  _T_383; // @[Monitor.scala 51:11]
  wire  _T_384; // @[Monitor.scala 310:28]
  wire  _T_386; // @[Monitor.scala 51:11]
  wire  _T_387; // @[Monitor.scala 51:11]
  wire  _T_388; // @[Monitor.scala 311:15]
  wire  _T_390; // @[Monitor.scala 51:11]
  wire  _T_391; // @[Monitor.scala 51:11]
  wire  _T_392; // @[Monitor.scala 312:15]
  wire  _T_394; // @[Monitor.scala 51:11]
  wire  _T_395; // @[Monitor.scala 51:11]
  wire  _T_396; // @[Monitor.scala 315:25]
  wire  _T_407; // @[Bundles.scala 104:26]
  wire  _T_409; // @[Monitor.scala 51:11]
  wire  _T_410; // @[Monitor.scala 51:11]
  wire  _T_411; // @[Monitor.scala 320:28]
  wire  _T_413; // @[Monitor.scala 51:11]
  wire  _T_414; // @[Monitor.scala 51:11]
  wire  _T_424; // @[Monitor.scala 325:25]
  wire  _T_444; // @[Monitor.scala 331:30]
  wire  _T_446; // @[Monitor.scala 51:11]
  wire  _T_447; // @[Monitor.scala 51:11]
  wire  _T_453; // @[Monitor.scala 335:25]
  wire  _T_470; // @[Monitor.scala 343:25]
  wire  _T_488; // @[Monitor.scala 351:25]
  wire  _T_520; // @[Decoupled.scala 40:37]
  reg  _T_529; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire  _T_531; // @[Edges.scala 231:28]
  wire  _T_532; // @[Edges.scala 232:25]
  reg [2:0] _T_540; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [2:0] _T_541; // @[Monitor.scala 382:22]
  reg [31:0] _RAND_2;
  reg [1:0] _T_542; // @[Monitor.scala 383:22]
  reg [31:0] _RAND_3;
  reg [10:0] _T_543; // @[Monitor.scala 384:22]
  reg [31:0] _RAND_4;
  reg [27:0] _T_544; // @[Monitor.scala 385:22]
  reg [31:0] _RAND_5;
  wire  _T_545; // @[Monitor.scala 386:22]
  wire  _T_546; // @[Monitor.scala 386:19]
  wire  _T_547; // @[Monitor.scala 387:32]
  wire  _T_549; // @[Monitor.scala 44:11]
  wire  _T_550; // @[Monitor.scala 44:11]
  wire  _T_551; // @[Monitor.scala 388:32]
  wire  _T_553; // @[Monitor.scala 44:11]
  wire  _T_554; // @[Monitor.scala 44:11]
  wire  _T_555; // @[Monitor.scala 389:32]
  wire  _T_557; // @[Monitor.scala 44:11]
  wire  _T_558; // @[Monitor.scala 44:11]
  wire  _T_559; // @[Monitor.scala 390:32]
  wire  _T_561; // @[Monitor.scala 44:11]
  wire  _T_562; // @[Monitor.scala 44:11]
  wire  _T_563; // @[Monitor.scala 391:32]
  wire  _T_565; // @[Monitor.scala 44:11]
  wire  _T_566; // @[Monitor.scala 44:11]
  wire  _T_568; // @[Monitor.scala 393:20]
  wire  _T_569; // @[Decoupled.scala 40:37]
  reg  _T_577; // @[Edges.scala 230:27]
  reg [31:0] _RAND_6;
  wire  _T_579; // @[Edges.scala 231:28]
  wire  _T_580; // @[Edges.scala 232:25]
  reg [2:0] _T_588; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_7;
  reg [1:0] _T_589; // @[Monitor.scala 533:22]
  reg [31:0] _RAND_8;
  reg [1:0] _T_590; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_9;
  reg [10:0] _T_591; // @[Monitor.scala 535:22]
  reg [31:0] _RAND_10;
  reg  _T_592; // @[Monitor.scala 536:22]
  reg [31:0] _RAND_11;
  reg  _T_593; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_12;
  wire  _T_594; // @[Monitor.scala 538:22]
  wire  _T_595; // @[Monitor.scala 538:19]
  wire  _T_596; // @[Monitor.scala 539:29]
  wire  _T_598; // @[Monitor.scala 51:11]
  wire  _T_599; // @[Monitor.scala 51:11]
  wire  _T_600; // @[Monitor.scala 540:29]
  wire  _T_602; // @[Monitor.scala 51:11]
  wire  _T_603; // @[Monitor.scala 51:11]
  wire  _T_604; // @[Monitor.scala 541:29]
  wire  _T_606; // @[Monitor.scala 51:11]
  wire  _T_607; // @[Monitor.scala 51:11]
  wire  _T_608; // @[Monitor.scala 542:29]
  wire  _T_610; // @[Monitor.scala 51:11]
  wire  _T_611; // @[Monitor.scala 51:11]
  wire  _T_612; // @[Monitor.scala 543:29]
  wire  _T_614; // @[Monitor.scala 51:11]
  wire  _T_615; // @[Monitor.scala 51:11]
  wire  _T_616; // @[Monitor.scala 544:29]
  wire  _T_618; // @[Monitor.scala 51:11]
  wire  _T_619; // @[Monitor.scala 51:11]
  wire  _T_621; // @[Monitor.scala 546:20]
  reg [1823:0] _T_622; // @[Monitor.scala 568:27]
  reg [1823:0] _RAND_13;
  reg  _T_632; // @[Edges.scala 230:27]
  reg [31:0] _RAND_14;
  wire  _T_634; // @[Edges.scala 231:28]
  wire  _T_635; // @[Edges.scala 232:25]
  reg  _T_651; // @[Edges.scala 230:27]
  reg [31:0] _RAND_15;
  wire  _T_653; // @[Edges.scala 231:28]
  wire  _T_654; // @[Edges.scala 232:25]
  wire  _T_664; // @[Monitor.scala 574:27]
  wire [2047:0] _T_666; // @[OneHot.scala 58:35]
  wire [1823:0] _T_667; // @[Monitor.scala 576:23]
  wire  _T_669; // @[Monitor.scala 576:14]
  wire  _T_671; // @[Monitor.scala 576:13]
  wire  _T_672; // @[Monitor.scala 576:13]
  wire [2047:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_676; // @[Monitor.scala 581:27]
  wire  _T_678; // @[Monitor.scala 581:75]
  wire  _T_679; // @[Monitor.scala 581:72]
  wire [2047:0] _T_680; // @[OneHot.scala 58:35]
  wire [1823:0] _T_681; // @[Monitor.scala 583:21]
  wire [1823:0] _T_682; // @[Monitor.scala 583:32]
  wire  _T_685; // @[Monitor.scala 51:11]
  wire  _T_686; // @[Monitor.scala 51:11]
  wire [2047:0] _GEN_16; // @[Monitor.scala 581:91]
  wire  _T_687; // @[Monitor.scala 587:20]
  wire  _T_688; // @[Monitor.scala 587:40]
  wire  _T_689; // @[Monitor.scala 587:33]
  wire  _T_690; // @[Monitor.scala 587:30]
  wire  _T_692; // @[Monitor.scala 51:11]
  wire  _T_693; // @[Monitor.scala 51:11]
  wire [1823:0] _T_694; // @[Monitor.scala 590:27]
  wire [1823:0] _T_695; // @[Monitor.scala 590:38]
  wire [1823:0] _T_696; // @[Monitor.scala 590:36]
  reg [31:0] _T_697; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_16;
  wire  _T_698; // @[Monitor.scala 595:23]
  wire  _T_699; // @[Monitor.scala 595:13]
  wire  _T_700; // @[Monitor.scala 595:36]
  wire  _T_701; // @[Monitor.scala 595:27]
  wire  _T_702; // @[Monitor.scala 595:56]
  wire  _T_703; // @[Monitor.scala 595:44]
  wire  _T_705; // @[Monitor.scala 595:12]
  wire  _T_706; // @[Monitor.scala 595:12]
  wire [31:0] _T_708; // @[Monitor.scala 597:26]
  wire  _T_711; // @[Monitor.scala 598:27]
  wire  _GEN_19; // @[Monitor.scala 44:11]
  wire  _GEN_35; // @[Monitor.scala 44:11]
  wire  _GEN_53; // @[Monitor.scala 44:11]
  wire  _GEN_65; // @[Monitor.scala 44:11]
  wire  _GEN_75; // @[Monitor.scala 44:11]
  wire  _GEN_85; // @[Monitor.scala 44:11]
  wire  _GEN_95; // @[Monitor.scala 44:11]
  wire  _GEN_105; // @[Monitor.scala 44:11]
  wire  _GEN_117; // @[Monitor.scala 51:11]
  wire  _GEN_127; // @[Monitor.scala 51:11]
  wire  _GEN_141; // @[Monitor.scala 51:11]
  wire  _GEN_155; // @[Monitor.scala 51:11]
  wire  _GEN_163; // @[Monitor.scala 51:11]
  wire  _GEN_171; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_10 = io_in_a_bits_source <= 11'h71f; // @[Parameters.scala 58:20]
  assign _T_14 = 6'h7 << io_in_a_bits_size; // @[package.scala 189:77]
  assign _T_16 = ~_T_14[2:0]; // @[package.scala 189:46]
  assign _GEN_18 = {{25'd0}, _T_16}; // @[Edges.scala 22:16]
  assign _T_17 = io_in_a_bits_address & _GEN_18; // @[Edges.scala 22:16]
  assign _T_18 = _T_17 == 28'h0; // @[Edges.scala 22:24]
  assign _T_19 = {{1'd0}, io_in_a_bits_size}; // @[Misc.scala 201:34]
  assign _T_21 = 4'h1 << _T_19[1:0]; // @[OneHot.scala 65:12]
  assign _T_23 = _T_21[2:0] | 3'h1; // @[Misc.scala 201:81]
  assign _T_24 = io_in_a_bits_size >= 2'h3; // @[Misc.scala 205:21]
  assign _T_27 = ~io_in_a_bits_address[2]; // @[Misc.scala 210:20]
  assign _T_29 = _T_23[2] & _T_27; // @[Misc.scala 214:38]
  assign _T_30 = _T_24 | _T_29; // @[Misc.scala 214:29]
  assign _T_32 = _T_23[2] & io_in_a_bits_address[2]; // @[Misc.scala 214:38]
  assign _T_33 = _T_24 | _T_32; // @[Misc.scala 214:29]
  assign _T_36 = ~io_in_a_bits_address[1]; // @[Misc.scala 210:20]
  assign _T_37 = _T_27 & _T_36; // @[Misc.scala 213:27]
  assign _T_38 = _T_23[1] & _T_37; // @[Misc.scala 214:38]
  assign _T_39 = _T_30 | _T_38; // @[Misc.scala 214:29]
  assign _T_40 = _T_27 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_41 = _T_23[1] & _T_40; // @[Misc.scala 214:38]
  assign _T_42 = _T_30 | _T_41; // @[Misc.scala 214:29]
  assign _T_43 = io_in_a_bits_address[2] & _T_36; // @[Misc.scala 213:27]
  assign _T_44 = _T_23[1] & _T_43; // @[Misc.scala 214:38]
  assign _T_45 = _T_33 | _T_44; // @[Misc.scala 214:29]
  assign _T_46 = io_in_a_bits_address[2] & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_47 = _T_23[1] & _T_46; // @[Misc.scala 214:38]
  assign _T_48 = _T_33 | _T_47; // @[Misc.scala 214:29]
  assign _T_51 = ~io_in_a_bits_address[0]; // @[Misc.scala 210:20]
  assign _T_52 = _T_37 & _T_51; // @[Misc.scala 213:27]
  assign _T_53 = _T_23[0] & _T_52; // @[Misc.scala 214:38]
  assign _T_54 = _T_39 | _T_53; // @[Misc.scala 214:29]
  assign _T_55 = _T_37 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_56 = _T_23[0] & _T_55; // @[Misc.scala 214:38]
  assign _T_57 = _T_39 | _T_56; // @[Misc.scala 214:29]
  assign _T_58 = _T_40 & _T_51; // @[Misc.scala 213:27]
  assign _T_59 = _T_23[0] & _T_58; // @[Misc.scala 214:38]
  assign _T_60 = _T_42 | _T_59; // @[Misc.scala 214:29]
  assign _T_61 = _T_40 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_62 = _T_23[0] & _T_61; // @[Misc.scala 214:38]
  assign _T_63 = _T_42 | _T_62; // @[Misc.scala 214:29]
  assign _T_64 = _T_43 & _T_51; // @[Misc.scala 213:27]
  assign _T_65 = _T_23[0] & _T_64; // @[Misc.scala 214:38]
  assign _T_66 = _T_45 | _T_65; // @[Misc.scala 214:29]
  assign _T_67 = _T_43 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_68 = _T_23[0] & _T_67; // @[Misc.scala 214:38]
  assign _T_69 = _T_45 | _T_68; // @[Misc.scala 214:29]
  assign _T_70 = _T_46 & _T_51; // @[Misc.scala 213:27]
  assign _T_71 = _T_23[0] & _T_70; // @[Misc.scala 214:38]
  assign _T_72 = _T_48 | _T_71; // @[Misc.scala 214:29]
  assign _T_73 = _T_46 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_74 = _T_23[0] & _T_73; // @[Misc.scala 214:38]
  assign _T_75 = _T_48 | _T_74; // @[Misc.scala 214:29]
  assign _T_82 = {_T_75,_T_72,_T_69,_T_66,_T_63,_T_60,_T_57,_T_54}; // @[Cat.scala 29:58]
  assign _T_101 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_103 = io_in_a_bits_address ^ 28'h8000000; // @[Parameters.scala 137:31]
  assign _T_104 = {1'b0,$signed(_T_103)}; // @[Parameters.scala 137:49]
  assign _T_106 = $signed(_T_104) & -29'sh200000; // @[Parameters.scala 137:52]
  assign _T_107 = $signed(_T_106) == 29'sh0; // @[Parameters.scala 137:67]
  assign _T_112 = ~reset; // @[Monitor.scala 44:11]
  assign _T_117 = _T_10 | reset; // @[Monitor.scala 44:11]
  assign _T_118 = ~_T_117; // @[Monitor.scala 44:11]
  assign _T_121 = _T_24 | reset; // @[Monitor.scala 44:11]
  assign _T_122 = ~_T_121; // @[Monitor.scala 44:11]
  assign _T_124 = _T_18 | reset; // @[Monitor.scala 44:11]
  assign _T_125 = ~_T_124; // @[Monitor.scala 44:11]
  assign _T_126 = io_in_a_bits_param <= 3'h2; // @[Bundles.scala 110:27]
  assign _T_128 = _T_126 | reset; // @[Monitor.scala 44:11]
  assign _T_129 = ~_T_128; // @[Monitor.scala 44:11]
  assign _T_130 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_131 = _T_130 == 8'h0; // @[Monitor.scala 86:31]
  assign _T_133 = _T_131 | reset; // @[Monitor.scala 44:11]
  assign _T_134 = ~_T_133; // @[Monitor.scala 44:11]
  assign _T_135 = ~io_in_a_bits_corrupt; // @[Monitor.scala 87:18]
  assign _T_137 = _T_135 | reset; // @[Monitor.scala 44:11]
  assign _T_138 = ~_T_137; // @[Monitor.scala 44:11]
  assign _T_139 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_168 = io_in_a_bits_param != 3'h0; // @[Monitor.scala 97:31]
  assign _T_170 = _T_168 | reset; // @[Monitor.scala 44:11]
  assign _T_171 = ~_T_170; // @[Monitor.scala 44:11]
  assign _T_181 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_194 = _T_107 | reset; // @[Monitor.scala 44:11]
  assign _T_195 = ~_T_194; // @[Monitor.scala 44:11]
  assign _T_202 = io_in_a_bits_param == 3'h0; // @[Monitor.scala 106:31]
  assign _T_204 = _T_202 | reset; // @[Monitor.scala 44:11]
  assign _T_205 = ~_T_204; // @[Monitor.scala 44:11]
  assign _T_206 = io_in_a_bits_mask == _T_82; // @[Monitor.scala 107:30]
  assign _T_208 = _T_206 | reset; // @[Monitor.scala 44:11]
  assign _T_209 = ~_T_208; // @[Monitor.scala 44:11]
  assign _T_214 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_243 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_268 = ~_T_82; // @[Monitor.scala 124:33]
  assign _T_269 = io_in_a_bits_mask & _T_268; // @[Monitor.scala 124:31]
  assign _T_270 = _T_269 == 8'h0; // @[Monitor.scala 124:40]
  assign _T_272 = _T_270 | reset; // @[Monitor.scala 44:11]
  assign _T_273 = ~_T_272; // @[Monitor.scala 44:11]
  assign _T_274 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_295 = io_in_a_bits_param <= 3'h4; // @[Bundles.scala 140:33]
  assign _T_297 = _T_295 | reset; // @[Monitor.scala 44:11]
  assign _T_298 = ~_T_297; // @[Monitor.scala 44:11]
  assign _T_303 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_324 = io_in_a_bits_param <= 3'h3; // @[Bundles.scala 147:30]
  assign _T_326 = _T_324 | reset; // @[Monitor.scala 44:11]
  assign _T_327 = ~_T_326; // @[Monitor.scala 44:11]
  assign _T_332 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_350 = io_in_a_bits_param <= 3'h1; // @[Bundles.scala 160:28]
  assign _T_352 = _T_350 | reset; // @[Monitor.scala 44:11]
  assign _T_353 = ~_T_352; // @[Monitor.scala 44:11]
  assign _T_362 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_364 = _T_362 | reset; // @[Monitor.scala 51:11]
  assign _T_365 = ~_T_364; // @[Monitor.scala 51:11]
  assign _T_372 = io_in_d_bits_source <= 11'h71f; // @[Parameters.scala 58:20]
  assign _T_376 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_378 = _T_372 | reset; // @[Monitor.scala 51:11]
  assign _T_379 = ~_T_378; // @[Monitor.scala 51:11]
  assign _T_380 = io_in_d_bits_size >= 2'h3; // @[Monitor.scala 309:27]
  assign _T_382 = _T_380 | reset; // @[Monitor.scala 51:11]
  assign _T_383 = ~_T_382; // @[Monitor.scala 51:11]
  assign _T_384 = io_in_d_bits_param == 2'h0; // @[Monitor.scala 310:28]
  assign _T_386 = _T_384 | reset; // @[Monitor.scala 51:11]
  assign _T_387 = ~_T_386; // @[Monitor.scala 51:11]
  assign _T_388 = ~io_in_d_bits_corrupt; // @[Monitor.scala 311:15]
  assign _T_390 = _T_388 | reset; // @[Monitor.scala 51:11]
  assign _T_391 = ~_T_390; // @[Monitor.scala 51:11]
  assign _T_392 = ~io_in_d_bits_denied; // @[Monitor.scala 312:15]
  assign _T_394 = _T_392 | reset; // @[Monitor.scala 51:11]
  assign _T_395 = ~_T_394; // @[Monitor.scala 51:11]
  assign _T_396 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_407 = io_in_d_bits_param <= 2'h2; // @[Bundles.scala 104:26]
  assign _T_409 = _T_407 | reset; // @[Monitor.scala 51:11]
  assign _T_410 = ~_T_409; // @[Monitor.scala 51:11]
  assign _T_411 = io_in_d_bits_param != 2'h2; // @[Monitor.scala 320:28]
  assign _T_413 = _T_411 | reset; // @[Monitor.scala 51:11]
  assign _T_414 = ~_T_413; // @[Monitor.scala 51:11]
  assign _T_424 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_444 = _T_392 | io_in_d_bits_corrupt; // @[Monitor.scala 331:30]
  assign _T_446 = _T_444 | reset; // @[Monitor.scala 51:11]
  assign _T_447 = ~_T_446; // @[Monitor.scala 51:11]
  assign _T_453 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_470 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_488 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_520 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_531 = _T_529 - 1'h1; // @[Edges.scala 231:28]
  assign _T_532 = ~_T_529; // @[Edges.scala 232:25]
  assign _T_545 = ~_T_532; // @[Monitor.scala 386:22]
  assign _T_546 = io_in_a_valid & _T_545; // @[Monitor.scala 386:19]
  assign _T_547 = io_in_a_bits_opcode == _T_540; // @[Monitor.scala 387:32]
  assign _T_549 = _T_547 | reset; // @[Monitor.scala 44:11]
  assign _T_550 = ~_T_549; // @[Monitor.scala 44:11]
  assign _T_551 = io_in_a_bits_param == _T_541; // @[Monitor.scala 388:32]
  assign _T_553 = _T_551 | reset; // @[Monitor.scala 44:11]
  assign _T_554 = ~_T_553; // @[Monitor.scala 44:11]
  assign _T_555 = io_in_a_bits_size == _T_542; // @[Monitor.scala 389:32]
  assign _T_557 = _T_555 | reset; // @[Monitor.scala 44:11]
  assign _T_558 = ~_T_557; // @[Monitor.scala 44:11]
  assign _T_559 = io_in_a_bits_source == _T_543; // @[Monitor.scala 390:32]
  assign _T_561 = _T_559 | reset; // @[Monitor.scala 44:11]
  assign _T_562 = ~_T_561; // @[Monitor.scala 44:11]
  assign _T_563 = io_in_a_bits_address == _T_544; // @[Monitor.scala 391:32]
  assign _T_565 = _T_563 | reset; // @[Monitor.scala 44:11]
  assign _T_566 = ~_T_565; // @[Monitor.scala 44:11]
  assign _T_568 = _T_520 & _T_532; // @[Monitor.scala 393:20]
  assign _T_569 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_579 = _T_577 - 1'h1; // @[Edges.scala 231:28]
  assign _T_580 = ~_T_577; // @[Edges.scala 232:25]
  assign _T_594 = ~_T_580; // @[Monitor.scala 538:22]
  assign _T_595 = io_in_d_valid & _T_594; // @[Monitor.scala 538:19]
  assign _T_596 = io_in_d_bits_opcode == _T_588; // @[Monitor.scala 539:29]
  assign _T_598 = _T_596 | reset; // @[Monitor.scala 51:11]
  assign _T_599 = ~_T_598; // @[Monitor.scala 51:11]
  assign _T_600 = io_in_d_bits_param == _T_589; // @[Monitor.scala 540:29]
  assign _T_602 = _T_600 | reset; // @[Monitor.scala 51:11]
  assign _T_603 = ~_T_602; // @[Monitor.scala 51:11]
  assign _T_604 = io_in_d_bits_size == _T_590; // @[Monitor.scala 541:29]
  assign _T_606 = _T_604 | reset; // @[Monitor.scala 51:11]
  assign _T_607 = ~_T_606; // @[Monitor.scala 51:11]
  assign _T_608 = io_in_d_bits_source == _T_591; // @[Monitor.scala 542:29]
  assign _T_610 = _T_608 | reset; // @[Monitor.scala 51:11]
  assign _T_611 = ~_T_610; // @[Monitor.scala 51:11]
  assign _T_612 = io_in_d_bits_sink == _T_592; // @[Monitor.scala 543:29]
  assign _T_614 = _T_612 | reset; // @[Monitor.scala 51:11]
  assign _T_615 = ~_T_614; // @[Monitor.scala 51:11]
  assign _T_616 = io_in_d_bits_denied == _T_593; // @[Monitor.scala 544:29]
  assign _T_618 = _T_616 | reset; // @[Monitor.scala 51:11]
  assign _T_619 = ~_T_618; // @[Monitor.scala 51:11]
  assign _T_621 = _T_569 & _T_580; // @[Monitor.scala 546:20]
  assign _T_634 = _T_632 - 1'h1; // @[Edges.scala 231:28]
  assign _T_635 = ~_T_632; // @[Edges.scala 232:25]
  assign _T_653 = _T_651 - 1'h1; // @[Edges.scala 231:28]
  assign _T_654 = ~_T_651; // @[Edges.scala 232:25]
  assign _T_664 = _T_520 & _T_635; // @[Monitor.scala 574:27]
  assign _T_666 = 2048'h1 << io_in_a_bits_source; // @[OneHot.scala 58:35]
  assign _T_667 = _T_622 >> io_in_a_bits_source; // @[Monitor.scala 576:23]
  assign _T_669 = ~_T_667[0]; // @[Monitor.scala 576:14]
  assign _T_671 = _T_669 | reset; // @[Monitor.scala 576:13]
  assign _T_672 = ~_T_671; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_664 ? _T_666 : 2048'h0; // @[Monitor.scala 574:72]
  assign _T_676 = _T_569 & _T_654; // @[Monitor.scala 581:27]
  assign _T_678 = ~_T_376; // @[Monitor.scala 581:75]
  assign _T_679 = _T_676 & _T_678; // @[Monitor.scala 581:72]
  assign _T_680 = 2048'h1 << io_in_d_bits_source; // @[OneHot.scala 58:35]
  assign _T_681 = _GEN_15[1823:0] | _T_622; // @[Monitor.scala 583:21]
  assign _T_682 = _T_681 >> io_in_d_bits_source; // @[Monitor.scala 583:32]
  assign _T_685 = _T_682[0] | reset; // @[Monitor.scala 51:11]
  assign _T_686 = ~_T_685; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_679 ? _T_680 : 2048'h0; // @[Monitor.scala 581:91]
  assign _T_687 = _GEN_15[1823:0] != _GEN_16[1823:0]; // @[Monitor.scala 587:20]
  assign _T_688 = _GEN_15[1823:0] != 1824'h0; // @[Monitor.scala 587:40]
  assign _T_689 = ~_T_688; // @[Monitor.scala 587:33]
  assign _T_690 = _T_687 | _T_689; // @[Monitor.scala 587:30]
  assign _T_692 = _T_690 | reset; // @[Monitor.scala 51:11]
  assign _T_693 = ~_T_692; // @[Monitor.scala 51:11]
  assign _T_694 = _T_622 | _GEN_15[1823:0]; // @[Monitor.scala 590:27]
  assign _T_695 = ~_GEN_16[1823:0]; // @[Monitor.scala 590:38]
  assign _T_696 = _T_694 & _T_695; // @[Monitor.scala 590:36]
  assign _T_698 = _T_622 != 1824'h0; // @[Monitor.scala 595:23]
  assign _T_699 = ~_T_698; // @[Monitor.scala 595:13]
  assign _T_700 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_701 = _T_699 | _T_700; // @[Monitor.scala 595:27]
  assign _T_702 = _T_697 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_703 = _T_701 | _T_702; // @[Monitor.scala 595:44]
  assign _T_705 = _T_703 | reset; // @[Monitor.scala 595:12]
  assign _T_706 = ~_T_705; // @[Monitor.scala 595:12]
  assign _T_708 = _T_697 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_711 = _T_520 | _T_569; // @[Monitor.scala 598:27]
  assign _GEN_19 = io_in_a_valid & _T_101; // @[Monitor.scala 44:11]
  assign _GEN_35 = io_in_a_valid & _T_139; // @[Monitor.scala 44:11]
  assign _GEN_53 = io_in_a_valid & _T_181; // @[Monitor.scala 44:11]
  assign _GEN_65 = io_in_a_valid & _T_214; // @[Monitor.scala 44:11]
  assign _GEN_75 = io_in_a_valid & _T_243; // @[Monitor.scala 44:11]
  assign _GEN_85 = io_in_a_valid & _T_274; // @[Monitor.scala 44:11]
  assign _GEN_95 = io_in_a_valid & _T_303; // @[Monitor.scala 44:11]
  assign _GEN_105 = io_in_a_valid & _T_332; // @[Monitor.scala 44:11]
  assign _GEN_117 = io_in_d_valid & _T_376; // @[Monitor.scala 51:11]
  assign _GEN_127 = io_in_d_valid & _T_396; // @[Monitor.scala 51:11]
  assign _GEN_141 = io_in_d_valid & _T_424; // @[Monitor.scala 51:11]
  assign _GEN_155 = io_in_d_valid & _T_453; // @[Monitor.scala 51:11]
  assign _GEN_163 = io_in_d_valid & _T_470; // @[Monitor.scala 51:11]
  assign _GEN_171 = io_in_d_valid & _T_488; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_529 = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_540 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_541 = _RAND_2[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_542 = _RAND_3[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_543 = _RAND_4[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  _T_544 = _RAND_5[27:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_577 = _RAND_6[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_588 = _RAND_7[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_589 = _RAND_8[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_590 = _RAND_9[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_591 = _RAND_10[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_592 = _RAND_11[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_593 = _RAND_12[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {57{`RANDOM}};
  _T_622 = _RAND_13[1823:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_14 = {1{`RANDOM}};
  _T_632 = _RAND_14[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_15 = {1{`RANDOM}};
  _T_651 = _RAND_15[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_16 = {1{`RANDOM}};
  _T_697 = _RAND_16[31:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_529 <= 1'h0;
    end else if (_T_520) begin
      if (_T_532) begin
        _T_529 <= 1'h0;
      end else begin
        _T_529 <= _T_531;
      end
    end
    if (_T_568) begin
      _T_540 <= io_in_a_bits_opcode;
    end
    if (_T_568) begin
      _T_541 <= io_in_a_bits_param;
    end
    if (_T_568) begin
      _T_542 <= io_in_a_bits_size;
    end
    if (_T_568) begin
      _T_543 <= io_in_a_bits_source;
    end
    if (_T_568) begin
      _T_544 <= io_in_a_bits_address;
    end
    if (reset) begin
      _T_577 <= 1'h0;
    end else if (_T_569) begin
      if (_T_580) begin
        _T_577 <= 1'h0;
      end else begin
        _T_577 <= _T_579;
      end
    end
    if (_T_621) begin
      _T_588 <= io_in_d_bits_opcode;
    end
    if (_T_621) begin
      _T_589 <= io_in_d_bits_param;
    end
    if (_T_621) begin
      _T_590 <= io_in_d_bits_size;
    end
    if (_T_621) begin
      _T_591 <= io_in_d_bits_source;
    end
    if (_T_621) begin
      _T_592 <= io_in_d_bits_sink;
    end
    if (_T_621) begin
      _T_593 <= io_in_d_bits_denied;
    end
    if (reset) begin
      _T_622 <= 1824'h0;
    end else begin
      _T_622 <= _T_696;
    end
    if (reset) begin
      _T_632 <= 1'h0;
    end else if (_T_520) begin
      if (_T_635) begin
        _T_632 <= 1'h0;
      end else begin
        _T_632 <= _T_634;
      end
    end
    if (reset) begin
      _T_651 <= 1'h0;
    end else if (_T_569) begin
      if (_T_654) begin
        _T_651 <= 1'h0;
      end else begin
        _T_651 <= _T_653;
      end
    end
    if (reset) begin
      _T_697 <= 32'h0;
    end else if (_T_711) begin
      _T_697 <= 32'h0;
    end else begin
      _T_697 <= _T_708;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_112) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_112) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid source ID (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_122) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock smaller than a beat (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_122) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_129) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid grow param (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_129) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_134) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_134) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_138) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock is corrupt (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_138) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_112) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_112) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid source ID (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_122) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm smaller than a beat (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_122) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_129) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid grow param (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_129) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_171) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_171) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_134) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_134) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_138) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm is corrupt (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_138) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_195) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_195) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid source ID (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_205) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid param (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_205) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_209) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_209) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_138) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get is corrupt (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_138) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_195) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_195) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid source ID (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_205) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid param (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_205) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_209) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_209) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_195) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_195) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid source ID (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_205) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid param (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_205) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_273) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial contains invalid mask (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_273) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_195) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_195) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid source ID (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_298) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid opcode param (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_298) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_209) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_209) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_195) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_195) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid source ID (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_327) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid opcode param (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_327) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_209) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_209) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_112) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid source ID (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_353) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid opcode param (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_353) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_209) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_209) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_138) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint is corrupt (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_138) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_365) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_365) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_379) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck carries invalid source ID (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_379) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_383) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_383) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_387) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseeAck carries invalid param (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_387) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_391) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is corrupt (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_391) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_395) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is denied (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_395) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_379) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid source ID (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_379) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid sink ID (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_112) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_383) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_383) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_410) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid cap param (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_410) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_414) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries toN param (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_414) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_391) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is corrupt (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_391) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_395) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is denied (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_395) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_141 & _T_379) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid source ID (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_141 & _T_379) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_141 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid sink ID (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_141 & _T_112) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_141 & _T_383) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_141 & _T_383) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_141 & _T_410) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid cap param (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_141 & _T_410) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_141 & _T_414) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries toN param (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_141 & _T_414) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_141 & _T_447) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied but not corrupt (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_141 & _T_447) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_141 & _T_395) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_141 & _T_395) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_155 & _T_379) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid source ID (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_155 & _T_379) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_155 & _T_387) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid param (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_155 & _T_387) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_155 & _T_391) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is corrupt (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_155 & _T_391) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_155 & _T_395) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is denied (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_155 & _T_395) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_163 & _T_379) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid source ID (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_163 & _T_379) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_163 & _T_387) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid param (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_163 & _T_387) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_163 & _T_447) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied but not corrupt (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_163 & _T_447) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_163 & _T_395) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_163 & _T_395) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_171 & _T_379) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid source ID (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_171 & _T_379) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_171 & _T_387) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid param (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_171 & _T_387) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_171 & _T_391) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is corrupt (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_171 & _T_391) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_171 & _T_395) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is denied (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_171 & _T_395) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_546 & _T_550) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_546 & _T_550) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_546 & _T_554) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel param changed within multibeat operation (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_546 & _T_554) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_546 & _T_558) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel size changed within multibeat operation (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_546 & _T_558) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_546 & _T_562) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel source changed within multibeat operation (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_546 & _T_562) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_546 & _T_566) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at Configs.scala:171:59)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_546 & _T_566) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_595 & _T_599) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_595 & _T_599) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_595 & _T_603) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel param changed within multibeat operation (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_595 & _T_603) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_595 & _T_607) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_595 & _T_607) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_595 & _T_611) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel source changed within multibeat operation (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_595 & _T_611) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_595 & _T_615) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel sink changed with multibeat operation (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_595 & _T_615) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_595 & _T_619) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_595 & _T_619) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_664 & _T_672) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at Configs.scala:171:59)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_664 & _T_672) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_679 & _T_686) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_679 & _T_686) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_693) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' and 'D' concurrent, despite minlatency 6 (connected at Configs.scala:171:59)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_693) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_706) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at Configs.scala:171:59)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_706) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
