//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_SinkD_assert(
  input   clock,
  input   reset,
  input   _T_36,
  input   d_io_deq_bits_denied,
  input   d_io_deq_bits_corrupt
);
  wire  _T_45; // @[SinkD.scala 87:22]
  wire  _T_46; // @[SinkD.scala 87:42]
  wire  _T_47; // @[SinkD.scala 87:39]
  wire  _T_49; // @[SinkD.scala 87:12]
  wire  _T_50; // @[SinkD.scala 87:12]
  assign _T_45 = _T_36 | d_io_deq_bits_denied; // @[SinkD.scala 87:22]
  assign _T_46 = ~d_io_deq_bits_corrupt; // @[SinkD.scala 87:42]
  assign _T_47 = _T_45 | _T_46; // @[SinkD.scala 87:39]
  assign _T_49 = _T_47 | reset; // @[SinkD.scala 87:12]
  assign _T_50 = ~_T_49; // @[SinkD.scala 87:12]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_50) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at SinkD.scala:87 assert (!d.valid || d.bits.denied || !d.bits.corrupt)\n"); // @[SinkD.scala 87:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_50) begin
          $fatal; // @[SinkD.scala 87:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
