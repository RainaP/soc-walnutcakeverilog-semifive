//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_65_assert(
  input        clock,
  input        reset,
  input        io_in_a_ready,
  input        io_in_a_valid,
  input  [2:0] io_in_a_bits_opcode,
  input  [8:0] io_in_a_bits_address,
  input  [3:0] io_in_a_bits_mask,
  input        io_in_d_ready,
  input        io_in_d_valid,
  input  [2:0] io_in_d_bits_opcode,
  input  [1:0] io_in_d_bits_param,
  input  [1:0] io_in_d_bits_size,
  input        io_in_d_bits_source,
  input        io_in_d_bits_sink,
  input        io_in_d_bits_denied,
  input        io_in_d_bits_corrupt
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire [8:0] _T_10; // @[Edges.scala 22:16]
  wire  _T_11; // @[Edges.scala 22:24]
  wire [9:0] _T_48; // @[Parameters.scala 137:49]
  wire  _T_56; // @[Monitor.scala 79:25]
  wire [9:0] _T_61; // @[Parameters.scala 137:52]
  wire  _T_62; // @[Parameters.scala 137:67]
  wire [8:0] _T_63; // @[Parameters.scala 137:31]
  wire [9:0] _T_64; // @[Parameters.scala 137:49]
  wire [9:0] _T_66; // @[Parameters.scala 137:52]
  wire  _T_67; // @[Parameters.scala 137:67]
  wire [8:0] _T_68; // @[Parameters.scala 137:31]
  wire [9:0] _T_69; // @[Parameters.scala 137:49]
  wire [9:0] _T_71; // @[Parameters.scala 137:52]
  wire  _T_72; // @[Parameters.scala 137:67]
  wire [8:0] _T_73; // @[Parameters.scala 137:31]
  wire [9:0] _T_74; // @[Parameters.scala 137:49]
  wire [9:0] _T_76; // @[Parameters.scala 137:52]
  wire  _T_77; // @[Parameters.scala 137:67]
  wire [8:0] _T_78; // @[Parameters.scala 137:31]
  wire [9:0] _T_79; // @[Parameters.scala 137:49]
  wire [9:0] _T_81; // @[Parameters.scala 137:52]
  wire  _T_82; // @[Parameters.scala 137:67]
  wire [8:0] _T_83; // @[Parameters.scala 137:31]
  wire [9:0] _T_84; // @[Parameters.scala 137:49]
  wire [9:0] _T_86; // @[Parameters.scala 137:52]
  wire  _T_87; // @[Parameters.scala 137:67]
  wire  _T_88; // @[Parameters.scala 552:42]
  wire  _T_89; // @[Parameters.scala 552:42]
  wire  _T_90; // @[Parameters.scala 552:42]
  wire  _T_91; // @[Parameters.scala 552:42]
  wire  _T_92; // @[Parameters.scala 552:42]
  wire  _T_97; // @[Monitor.scala 44:11]
  wire  _T_109; // @[Monitor.scala 44:11]
  wire  _T_110; // @[Monitor.scala 44:11]
  wire [3:0] _T_115; // @[Monitor.scala 86:18]
  wire  _T_116; // @[Monitor.scala 86:31]
  wire  _T_118; // @[Monitor.scala 44:11]
  wire  _T_119; // @[Monitor.scala 44:11]
  wire  _T_124; // @[Monitor.scala 90:25]
  wire  _T_196; // @[Monitor.scala 102:25]
  wire  _T_239; // @[Monitor.scala 44:11]
  wire  _T_240; // @[Monitor.scala 44:11]
  wire  _T_251; // @[Monitor.scala 107:30]
  wire  _T_253; // @[Monitor.scala 44:11]
  wire  _T_254; // @[Monitor.scala 44:11]
  wire  _T_259; // @[Monitor.scala 111:25]
  wire  _T_318; // @[Monitor.scala 119:25]
  wire  _T_379; // @[Monitor.scala 127:25]
  wire  _T_435; // @[Monitor.scala 135:25]
  wire  _T_491; // @[Monitor.scala 143:25]
  wire  _T_551; // @[Bundles.scala 44:24]
  wire  _T_553; // @[Monitor.scala 51:11]
  wire  _T_554; // @[Monitor.scala 51:11]
  wire  _T_555; // @[Parameters.scala 47:9]
  wire  _T_558; // @[Monitor.scala 307:25]
  wire  _T_560; // @[Monitor.scala 51:11]
  wire  _T_561; // @[Monitor.scala 51:11]
  wire  _T_562; // @[Monitor.scala 309:27]
  wire  _T_564; // @[Monitor.scala 51:11]
  wire  _T_565; // @[Monitor.scala 51:11]
  wire  _T_566; // @[Monitor.scala 310:28]
  wire  _T_568; // @[Monitor.scala 51:11]
  wire  _T_569; // @[Monitor.scala 51:11]
  wire  _T_570; // @[Monitor.scala 311:15]
  wire  _T_572; // @[Monitor.scala 51:11]
  wire  _T_573; // @[Monitor.scala 51:11]
  wire  _T_574; // @[Monitor.scala 312:15]
  wire  _T_576; // @[Monitor.scala 51:11]
  wire  _T_577; // @[Monitor.scala 51:11]
  wire  _T_578; // @[Monitor.scala 315:25]
  wire  _T_589; // @[Bundles.scala 104:26]
  wire  _T_591; // @[Monitor.scala 51:11]
  wire  _T_592; // @[Monitor.scala 51:11]
  wire  _T_593; // @[Monitor.scala 320:28]
  wire  _T_595; // @[Monitor.scala 51:11]
  wire  _T_596; // @[Monitor.scala 51:11]
  wire  _T_606; // @[Monitor.scala 325:25]
  wire  _T_626; // @[Monitor.scala 331:30]
  wire  _T_628; // @[Monitor.scala 51:11]
  wire  _T_629; // @[Monitor.scala 51:11]
  wire  _T_635; // @[Monitor.scala 335:25]
  wire  _T_652; // @[Monitor.scala 343:25]
  wire  _T_670; // @[Monitor.scala 351:25]
  wire  _T_702; // @[Decoupled.scala 40:37]
  reg  _T_711; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire  _T_713; // @[Edges.scala 231:28]
  wire  _T_714; // @[Edges.scala 232:25]
  reg [2:0] _T_722; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [8:0] _T_726; // @[Monitor.scala 385:22]
  reg [31:0] _RAND_2;
  wire  _T_727; // @[Monitor.scala 386:22]
  wire  _T_728; // @[Monitor.scala 386:19]
  wire  _T_729; // @[Monitor.scala 387:32]
  wire  _T_731; // @[Monitor.scala 44:11]
  wire  _T_732; // @[Monitor.scala 44:11]
  wire  _T_745; // @[Monitor.scala 391:32]
  wire  _T_747; // @[Monitor.scala 44:11]
  wire  _T_748; // @[Monitor.scala 44:11]
  wire  _T_750; // @[Monitor.scala 393:20]
  wire  _T_751; // @[Decoupled.scala 40:37]
  reg  _T_759; // @[Edges.scala 230:27]
  reg [31:0] _RAND_3;
  wire  _T_761; // @[Edges.scala 231:28]
  wire  _T_762; // @[Edges.scala 232:25]
  reg [2:0] _T_770; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_4;
  reg [1:0] _T_771; // @[Monitor.scala 533:22]
  reg [31:0] _RAND_5;
  reg [1:0] _T_772; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_6;
  reg  _T_773; // @[Monitor.scala 535:22]
  reg [31:0] _RAND_7;
  reg  _T_774; // @[Monitor.scala 536:22]
  reg [31:0] _RAND_8;
  reg  _T_775; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_9;
  wire  _T_776; // @[Monitor.scala 538:22]
  wire  _T_777; // @[Monitor.scala 538:19]
  wire  _T_778; // @[Monitor.scala 539:29]
  wire  _T_780; // @[Monitor.scala 51:11]
  wire  _T_781; // @[Monitor.scala 51:11]
  wire  _T_782; // @[Monitor.scala 540:29]
  wire  _T_784; // @[Monitor.scala 51:11]
  wire  _T_785; // @[Monitor.scala 51:11]
  wire  _T_786; // @[Monitor.scala 541:29]
  wire  _T_788; // @[Monitor.scala 51:11]
  wire  _T_789; // @[Monitor.scala 51:11]
  wire  _T_790; // @[Monitor.scala 542:29]
  wire  _T_792; // @[Monitor.scala 51:11]
  wire  _T_793; // @[Monitor.scala 51:11]
  wire  _T_794; // @[Monitor.scala 543:29]
  wire  _T_796; // @[Monitor.scala 51:11]
  wire  _T_797; // @[Monitor.scala 51:11]
  wire  _T_798; // @[Monitor.scala 544:29]
  wire  _T_800; // @[Monitor.scala 51:11]
  wire  _T_801; // @[Monitor.scala 51:11]
  wire  _T_803; // @[Monitor.scala 546:20]
  reg  _T_804; // @[Monitor.scala 568:27]
  reg [31:0] _RAND_10;
  reg  _T_814; // @[Edges.scala 230:27]
  reg [31:0] _RAND_11;
  wire  _T_816; // @[Edges.scala 231:28]
  wire  _T_817; // @[Edges.scala 232:25]
  reg  _T_833; // @[Edges.scala 230:27]
  reg [31:0] _RAND_12;
  wire  _T_835; // @[Edges.scala 231:28]
  wire  _T_836; // @[Edges.scala 232:25]
  wire  _T_846; // @[Monitor.scala 574:27]
  wire  _T_851; // @[Monitor.scala 576:14]
  wire  _T_853; // @[Monitor.scala 576:13]
  wire  _T_854; // @[Monitor.scala 576:13]
  wire [1:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_858; // @[Monitor.scala 581:27]
  wire  _T_860; // @[Monitor.scala 581:75]
  wire  _T_861; // @[Monitor.scala 581:72]
  wire [1:0] _T_862; // @[OneHot.scala 58:35]
  wire  _T_863; // @[Monitor.scala 583:21]
  wire  _T_864; // @[Monitor.scala 583:32]
  wire  _T_867; // @[Monitor.scala 51:11]
  wire  _T_868; // @[Monitor.scala 51:11]
  wire [1:0] _GEN_16; // @[Monitor.scala 581:91]
  wire  _T_869; // @[Monitor.scala 587:20]
  wire  _T_871; // @[Monitor.scala 587:33]
  wire  _T_872; // @[Monitor.scala 587:30]
  wire  _T_874; // @[Monitor.scala 51:11]
  wire  _T_875; // @[Monitor.scala 51:11]
  wire  _T_876; // @[Monitor.scala 590:27]
  wire  _T_877; // @[Monitor.scala 590:38]
  reg [31:0] _T_879; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_13;
  wire  _T_882; // @[Monitor.scala 595:36]
  wire  _T_883; // @[Monitor.scala 595:27]
  wire  _T_884; // @[Monitor.scala 595:56]
  wire  _T_885; // @[Monitor.scala 595:44]
  wire  _T_887; // @[Monitor.scala 595:12]
  wire  _T_888; // @[Monitor.scala 595:12]
  wire [31:0] _T_890; // @[Monitor.scala 597:26]
  wire  _T_893; // @[Monitor.scala 598:27]
  wire  _GEN_18; // @[Monitor.scala 44:11]
  wire  _GEN_26; // @[Monitor.scala 44:11]
  wire  _GEN_36; // @[Monitor.scala 44:11]
  wire  _GEN_42; // @[Monitor.scala 44:11]
  wire  _GEN_48; // @[Monitor.scala 44:11]
  wire  _GEN_52; // @[Monitor.scala 44:11]
  wire  _GEN_58; // @[Monitor.scala 44:11]
  wire  _GEN_64; // @[Monitor.scala 44:11]
  wire  _GEN_70; // @[Monitor.scala 51:11]
  wire  _GEN_80; // @[Monitor.scala 51:11]
  wire  _GEN_94; // @[Monitor.scala 51:11]
  wire  _GEN_108; // @[Monitor.scala 51:11]
  wire  _GEN_116; // @[Monitor.scala 51:11]
  wire  _GEN_124; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_10 = io_in_a_bits_address & 9'h3; // @[Edges.scala 22:16]
  assign _T_11 = _T_10 == 9'h0; // @[Edges.scala 22:24]
  assign _T_48 = {1'b0,$signed(io_in_a_bits_address)}; // @[Parameters.scala 137:49]
  assign _T_56 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_61 = $signed(_T_48) & -10'sh40; // @[Parameters.scala 137:52]
  assign _T_62 = $signed(_T_61) == 10'sh0; // @[Parameters.scala 137:67]
  assign _T_63 = io_in_a_bits_address ^ 9'h44; // @[Parameters.scala 137:31]
  assign _T_64 = {1'b0,$signed(_T_63)}; // @[Parameters.scala 137:49]
  assign _T_66 = $signed(_T_64) & -10'sh4; // @[Parameters.scala 137:52]
  assign _T_67 = $signed(_T_66) == 10'sh0; // @[Parameters.scala 137:67]
  assign _T_68 = io_in_a_bits_address ^ 9'h48; // @[Parameters.scala 137:31]
  assign _T_69 = {1'b0,$signed(_T_68)}; // @[Parameters.scala 137:49]
  assign _T_71 = $signed(_T_69) & -10'sh18; // @[Parameters.scala 137:52]
  assign _T_72 = $signed(_T_71) == 10'sh0; // @[Parameters.scala 137:67]
  assign _T_73 = io_in_a_bits_address ^ 9'h60; // @[Parameters.scala 137:31]
  assign _T_74 = {1'b0,$signed(_T_73)}; // @[Parameters.scala 137:49]
  assign _T_76 = $signed(_T_74) & -10'sh20; // @[Parameters.scala 137:52]
  assign _T_77 = $signed(_T_76) == 10'sh0; // @[Parameters.scala 137:67]
  assign _T_78 = io_in_a_bits_address ^ 9'h80; // @[Parameters.scala 137:31]
  assign _T_79 = {1'b0,$signed(_T_78)}; // @[Parameters.scala 137:49]
  assign _T_81 = $signed(_T_79) & -10'sh80; // @[Parameters.scala 137:52]
  assign _T_82 = $signed(_T_81) == 10'sh0; // @[Parameters.scala 137:67]
  assign _T_83 = io_in_a_bits_address ^ 9'h100; // @[Parameters.scala 137:31]
  assign _T_84 = {1'b0,$signed(_T_83)}; // @[Parameters.scala 137:49]
  assign _T_86 = $signed(_T_84) & -10'sh100; // @[Parameters.scala 137:52]
  assign _T_87 = $signed(_T_86) == 10'sh0; // @[Parameters.scala 137:67]
  assign _T_88 = _T_62 | _T_67; // @[Parameters.scala 552:42]
  assign _T_89 = _T_88 | _T_72; // @[Parameters.scala 552:42]
  assign _T_90 = _T_89 | _T_77; // @[Parameters.scala 552:42]
  assign _T_91 = _T_90 | _T_82; // @[Parameters.scala 552:42]
  assign _T_92 = _T_91 | _T_87; // @[Parameters.scala 552:42]
  assign _T_97 = ~reset; // @[Monitor.scala 44:11]
  assign _T_109 = _T_11 | reset; // @[Monitor.scala 44:11]
  assign _T_110 = ~_T_109; // @[Monitor.scala 44:11]
  assign _T_115 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_116 = _T_115 == 4'h0; // @[Monitor.scala 86:31]
  assign _T_118 = _T_116 | reset; // @[Monitor.scala 44:11]
  assign _T_119 = ~_T_118; // @[Monitor.scala 44:11]
  assign _T_124 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_196 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_239 = _T_92 | reset; // @[Monitor.scala 44:11]
  assign _T_240 = ~_T_239; // @[Monitor.scala 44:11]
  assign _T_251 = io_in_a_bits_mask == 4'hf; // @[Monitor.scala 107:30]
  assign _T_253 = _T_251 | reset; // @[Monitor.scala 44:11]
  assign _T_254 = ~_T_253; // @[Monitor.scala 44:11]
  assign _T_259 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_318 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_379 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_435 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_491 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_551 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_553 = _T_551 | reset; // @[Monitor.scala 51:11]
  assign _T_554 = ~_T_553; // @[Monitor.scala 51:11]
  assign _T_555 = ~io_in_d_bits_source; // @[Parameters.scala 47:9]
  assign _T_558 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_560 = _T_555 | reset; // @[Monitor.scala 51:11]
  assign _T_561 = ~_T_560; // @[Monitor.scala 51:11]
  assign _T_562 = io_in_d_bits_size >= 2'h2; // @[Monitor.scala 309:27]
  assign _T_564 = _T_562 | reset; // @[Monitor.scala 51:11]
  assign _T_565 = ~_T_564; // @[Monitor.scala 51:11]
  assign _T_566 = io_in_d_bits_param == 2'h0; // @[Monitor.scala 310:28]
  assign _T_568 = _T_566 | reset; // @[Monitor.scala 51:11]
  assign _T_569 = ~_T_568; // @[Monitor.scala 51:11]
  assign _T_570 = ~io_in_d_bits_corrupt; // @[Monitor.scala 311:15]
  assign _T_572 = _T_570 | reset; // @[Monitor.scala 51:11]
  assign _T_573 = ~_T_572; // @[Monitor.scala 51:11]
  assign _T_574 = ~io_in_d_bits_denied; // @[Monitor.scala 312:15]
  assign _T_576 = _T_574 | reset; // @[Monitor.scala 51:11]
  assign _T_577 = ~_T_576; // @[Monitor.scala 51:11]
  assign _T_578 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_589 = io_in_d_bits_param <= 2'h2; // @[Bundles.scala 104:26]
  assign _T_591 = _T_589 | reset; // @[Monitor.scala 51:11]
  assign _T_592 = ~_T_591; // @[Monitor.scala 51:11]
  assign _T_593 = io_in_d_bits_param != 2'h2; // @[Monitor.scala 320:28]
  assign _T_595 = _T_593 | reset; // @[Monitor.scala 51:11]
  assign _T_596 = ~_T_595; // @[Monitor.scala 51:11]
  assign _T_606 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_626 = _T_574 | io_in_d_bits_corrupt; // @[Monitor.scala 331:30]
  assign _T_628 = _T_626 | reset; // @[Monitor.scala 51:11]
  assign _T_629 = ~_T_628; // @[Monitor.scala 51:11]
  assign _T_635 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_652 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_670 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_702 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_713 = _T_711 - 1'h1; // @[Edges.scala 231:28]
  assign _T_714 = ~_T_711; // @[Edges.scala 232:25]
  assign _T_727 = ~_T_714; // @[Monitor.scala 386:22]
  assign _T_728 = io_in_a_valid & _T_727; // @[Monitor.scala 386:19]
  assign _T_729 = io_in_a_bits_opcode == _T_722; // @[Monitor.scala 387:32]
  assign _T_731 = _T_729 | reset; // @[Monitor.scala 44:11]
  assign _T_732 = ~_T_731; // @[Monitor.scala 44:11]
  assign _T_745 = io_in_a_bits_address == _T_726; // @[Monitor.scala 391:32]
  assign _T_747 = _T_745 | reset; // @[Monitor.scala 44:11]
  assign _T_748 = ~_T_747; // @[Monitor.scala 44:11]
  assign _T_750 = _T_702 & _T_714; // @[Monitor.scala 393:20]
  assign _T_751 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_761 = _T_759 - 1'h1; // @[Edges.scala 231:28]
  assign _T_762 = ~_T_759; // @[Edges.scala 232:25]
  assign _T_776 = ~_T_762; // @[Monitor.scala 538:22]
  assign _T_777 = io_in_d_valid & _T_776; // @[Monitor.scala 538:19]
  assign _T_778 = io_in_d_bits_opcode == _T_770; // @[Monitor.scala 539:29]
  assign _T_780 = _T_778 | reset; // @[Monitor.scala 51:11]
  assign _T_781 = ~_T_780; // @[Monitor.scala 51:11]
  assign _T_782 = io_in_d_bits_param == _T_771; // @[Monitor.scala 540:29]
  assign _T_784 = _T_782 | reset; // @[Monitor.scala 51:11]
  assign _T_785 = ~_T_784; // @[Monitor.scala 51:11]
  assign _T_786 = io_in_d_bits_size == _T_772; // @[Monitor.scala 541:29]
  assign _T_788 = _T_786 | reset; // @[Monitor.scala 51:11]
  assign _T_789 = ~_T_788; // @[Monitor.scala 51:11]
  assign _T_790 = io_in_d_bits_source == _T_773; // @[Monitor.scala 542:29]
  assign _T_792 = _T_790 | reset; // @[Monitor.scala 51:11]
  assign _T_793 = ~_T_792; // @[Monitor.scala 51:11]
  assign _T_794 = io_in_d_bits_sink == _T_774; // @[Monitor.scala 543:29]
  assign _T_796 = _T_794 | reset; // @[Monitor.scala 51:11]
  assign _T_797 = ~_T_796; // @[Monitor.scala 51:11]
  assign _T_798 = io_in_d_bits_denied == _T_775; // @[Monitor.scala 544:29]
  assign _T_800 = _T_798 | reset; // @[Monitor.scala 51:11]
  assign _T_801 = ~_T_800; // @[Monitor.scala 51:11]
  assign _T_803 = _T_751 & _T_762; // @[Monitor.scala 546:20]
  assign _T_816 = _T_814 - 1'h1; // @[Edges.scala 231:28]
  assign _T_817 = ~_T_814; // @[Edges.scala 232:25]
  assign _T_835 = _T_833 - 1'h1; // @[Edges.scala 231:28]
  assign _T_836 = ~_T_833; // @[Edges.scala 232:25]
  assign _T_846 = _T_702 & _T_817; // @[Monitor.scala 574:27]
  assign _T_851 = ~_T_804; // @[Monitor.scala 576:14]
  assign _T_853 = _T_851 | reset; // @[Monitor.scala 576:13]
  assign _T_854 = ~_T_853; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_846 ? 2'h1 : 2'h0; // @[Monitor.scala 574:72]
  assign _T_858 = _T_751 & _T_836; // @[Monitor.scala 581:27]
  assign _T_860 = ~_T_558; // @[Monitor.scala 581:75]
  assign _T_861 = _T_858 & _T_860; // @[Monitor.scala 581:72]
  assign _T_862 = 2'h1 << io_in_d_bits_source; // @[OneHot.scala 58:35]
  assign _T_863 = _GEN_15[0] | _T_804; // @[Monitor.scala 583:21]
  assign _T_864 = _T_863 >> io_in_d_bits_source; // @[Monitor.scala 583:32]
  assign _T_867 = _T_864 | reset; // @[Monitor.scala 51:11]
  assign _T_868 = ~_T_867; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_861 ? _T_862 : 2'h0; // @[Monitor.scala 581:91]
  assign _T_869 = _GEN_15[0] != _GEN_16[0]; // @[Monitor.scala 587:20]
  assign _T_871 = ~_GEN_15[0]; // @[Monitor.scala 587:33]
  assign _T_872 = _T_869 | _T_871; // @[Monitor.scala 587:30]
  assign _T_874 = _T_872 | reset; // @[Monitor.scala 51:11]
  assign _T_875 = ~_T_874; // @[Monitor.scala 51:11]
  assign _T_876 = _T_804 | _GEN_15[0]; // @[Monitor.scala 590:27]
  assign _T_877 = ~_GEN_16[0]; // @[Monitor.scala 590:38]
  assign _T_882 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_883 = _T_851 | _T_882; // @[Monitor.scala 595:27]
  assign _T_884 = _T_879 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_885 = _T_883 | _T_884; // @[Monitor.scala 595:44]
  assign _T_887 = _T_885 | reset; // @[Monitor.scala 595:12]
  assign _T_888 = ~_T_887; // @[Monitor.scala 595:12]
  assign _T_890 = _T_879 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_893 = _T_702 | _T_751; // @[Monitor.scala 598:27]
  assign _GEN_18 = io_in_a_valid & _T_56; // @[Monitor.scala 44:11]
  assign _GEN_26 = io_in_a_valid & _T_124; // @[Monitor.scala 44:11]
  assign _GEN_36 = io_in_a_valid & _T_196; // @[Monitor.scala 44:11]
  assign _GEN_42 = io_in_a_valid & _T_259; // @[Monitor.scala 44:11]
  assign _GEN_48 = io_in_a_valid & _T_318; // @[Monitor.scala 44:11]
  assign _GEN_52 = io_in_a_valid & _T_379; // @[Monitor.scala 44:11]
  assign _GEN_58 = io_in_a_valid & _T_435; // @[Monitor.scala 44:11]
  assign _GEN_64 = io_in_a_valid & _T_491; // @[Monitor.scala 44:11]
  assign _GEN_70 = io_in_d_valid & _T_558; // @[Monitor.scala 51:11]
  assign _GEN_80 = io_in_d_valid & _T_578; // @[Monitor.scala 51:11]
  assign _GEN_94 = io_in_d_valid & _T_606; // @[Monitor.scala 51:11]
  assign _GEN_108 = io_in_d_valid & _T_635; // @[Monitor.scala 51:11]
  assign _GEN_116 = io_in_d_valid & _T_652; // @[Monitor.scala 51:11]
  assign _GEN_124 = io_in_d_valid & _T_670; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_711 = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_722 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_726 = _RAND_2[8:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_759 = _RAND_3[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_770 = _RAND_4[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  _T_771 = _RAND_5[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_772 = _RAND_6[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_773 = _RAND_7[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_774 = _RAND_8[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_775 = _RAND_9[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_804 = _RAND_10[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_814 = _RAND_11[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_833 = _RAND_12[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  _T_879 = _RAND_13[31:0];
  `endif // RANDOMIZE_REG_INIT
  if (reset) begin
    _T_711 = 1'h0;
  end
  if (reset) begin
    _T_759 = 1'h0;
  end
  if (reset) begin
    _T_804 = 1'h0;
  end
  if (reset) begin
    _T_814 = 1'h0;
  end
  if (reset) begin
    _T_833 = 1'h0;
  end
  if (reset) begin
    _T_879 = 32'h0;
  end
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (_T_750) begin
      _T_722 <= io_in_a_bits_opcode;
    end
    if (_T_750) begin
      _T_726 <= io_in_a_bits_address;
    end
    if (_T_803) begin
      _T_770 <= io_in_d_bits_opcode;
    end
    if (_T_803) begin
      _T_771 <= io_in_d_bits_param;
    end
    if (_T_803) begin
      _T_772 <= io_in_d_bits_size;
    end
    if (_T_803) begin
      _T_773 <= io_in_d_bits_source;
    end
    if (_T_803) begin
      _T_774 <= io_in_d_bits_sink;
    end
    if (_T_803) begin
      _T_775 <= io_in_d_bits_denied;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_97) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_97) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_119) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_119) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_97) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_97) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_97) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_119) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_119) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_36 & _T_240) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_36 & _T_240) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_36 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_36 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_36 & _T_254) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_36 & _T_254) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_42 & _T_240) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_42 & _T_240) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_42 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_42 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_42 & _T_254) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_42 & _T_254) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_48 & _T_240) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_48 & _T_240) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_48 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_48 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_52 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_52 & _T_97) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_52 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_52 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_52 & _T_254) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_52 & _T_254) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_58 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_58 & _T_97) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_58 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_58 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_58 & _T_254) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_58 & _T_254) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_97) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_254) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_254) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_554) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_554) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_70 & _T_561) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck carries invalid source ID (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_70 & _T_561) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_70 & _T_565) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_70 & _T_565) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_70 & _T_569) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseeAck carries invalid param (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_70 & _T_569) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_70 & _T_573) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is corrupt (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_70 & _T_573) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_70 & _T_577) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is denied (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_70 & _T_577) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_80 & _T_561) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid source ID (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_80 & _T_561) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_80 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid sink ID (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_80 & _T_97) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_80 & _T_565) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_80 & _T_565) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_80 & _T_592) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid cap param (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_80 & _T_592) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_80 & _T_596) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries toN param (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_80 & _T_596) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_80 & _T_573) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is corrupt (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_80 & _T_573) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_80 & _T_577) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is denied (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_80 & _T_577) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_94 & _T_561) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid source ID (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_94 & _T_561) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_94 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid sink ID (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_94 & _T_97) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_94 & _T_565) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_94 & _T_565) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_94 & _T_592) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid cap param (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_94 & _T_592) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_94 & _T_596) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries toN param (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_94 & _T_596) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_94 & _T_629) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied but not corrupt (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_94 & _T_629) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_94 & _T_577) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_94 & _T_577) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_108 & _T_561) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid source ID (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_108 & _T_561) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_108 & _T_569) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid param (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_108 & _T_569) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_108 & _T_573) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is corrupt (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_108 & _T_573) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_108 & _T_577) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is denied (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_108 & _T_577) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_116 & _T_561) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid source ID (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_116 & _T_561) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_116 & _T_569) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid param (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_116 & _T_569) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_116 & _T_629) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied but not corrupt (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_116 & _T_629) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_116 & _T_577) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_116 & _T_577) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_124 & _T_561) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid source ID (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_124 & _T_561) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_124 & _T_569) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid param (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_124 & _T_569) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_124 & _T_573) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is corrupt (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_124 & _T_573) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_124 & _T_577) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is denied (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_124 & _T_577) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_728 & _T_732) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_728 & _T_732) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_728 & _T_748) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at Debug.scala:631:46)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_728 & _T_748) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_777 & _T_781) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_777 & _T_781) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_777 & _T_785) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel param changed within multibeat operation (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_777 & _T_785) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_777 & _T_789) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_777 & _T_789) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_777 & _T_793) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel source changed within multibeat operation (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_777 & _T_793) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_777 & _T_797) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel sink changed with multibeat operation (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_777 & _T_797) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_777 & _T_801) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_777 & _T_801) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_846 & _T_854) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at Debug.scala:631:46)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_846 & _T_854) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_861 & _T_868) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_861 & _T_868) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_875) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' and 'D' concurrent, despite minlatency 3 (connected at Debug.scala:631:46)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_875) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_888) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at Debug.scala:631:46)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_888) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_711 <= 1'h0;
    end else if (_T_702) begin
      if (_T_714) begin
        _T_711 <= 1'h0;
      end else begin
        _T_711 <= _T_713;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_759 <= 1'h0;
    end else if (_T_751) begin
      if (_T_762) begin
        _T_759 <= 1'h0;
      end else begin
        _T_759 <= _T_761;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_804 <= 1'h0;
    end else begin
      _T_804 <= _T_876 & _T_877;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_814 <= 1'h0;
    end else if (_T_702) begin
      if (_T_817) begin
        _T_814 <= 1'h0;
      end else begin
        _T_814 <= _T_816;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_833 <= 1'h0;
    end else if (_T_751) begin
      if (_T_836) begin
        _T_833 <= 1'h0;
      end else begin
        _T_833 <= _T_835;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_879 <= 32'h0;
    end else if (_T_893) begin
      _T_879 <= 32'h0;
    end else begin
      _T_879 <= _T_890;
    end
  end

endmodule
