//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__Queue_2(
  input          rf_reset,
  input          clock,
  input          reset,
  output         io_enq_ready,
  input          io_enq_valid,
  input  [7:0]   io_enq_bits_id,
  input  [127:0] io_enq_bits_data,
  input  [1:0]   io_enq_bits_resp,
  input  [3:0]   io_enq_bits_echo_tl_state_size,
  input  [6:0]   io_enq_bits_echo_tl_state_source,
  input          io_enq_bits_last,
  input          io_deq_ready,
  output         io_deq_valid,
  output [7:0]   io_deq_bits_id,
  output [127:0] io_deq_bits_data,
  output [1:0]   io_deq_bits_resp,
  output [3:0]   io_deq_bits_echo_tl_state_size,
  output [6:0]   io_deq_bits_echo_tl_state_source,
  output         io_deq_bits_last
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [127:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [127:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [127:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [127:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
`endif // RANDOMIZE_REG_INIT
  reg [7:0] ram_0_id; // @[Decoupled.scala 218:16]
  reg [127:0] ram_0_data; // @[Decoupled.scala 218:16]
  reg [1:0] ram_0_resp; // @[Decoupled.scala 218:16]
  reg [3:0] ram_0_echo_tl_state_size; // @[Decoupled.scala 218:16]
  reg [6:0] ram_0_echo_tl_state_source; // @[Decoupled.scala 218:16]
  reg  ram_0_last; // @[Decoupled.scala 218:16]
  reg [7:0] ram_1_id; // @[Decoupled.scala 218:16]
  reg [127:0] ram_1_data; // @[Decoupled.scala 218:16]
  reg [1:0] ram_1_resp; // @[Decoupled.scala 218:16]
  reg [3:0] ram_1_echo_tl_state_size; // @[Decoupled.scala 218:16]
  reg [6:0] ram_1_echo_tl_state_source; // @[Decoupled.scala 218:16]
  reg  ram_1_last; // @[Decoupled.scala 218:16]
  reg [7:0] ram_2_id; // @[Decoupled.scala 218:16]
  reg [127:0] ram_2_data; // @[Decoupled.scala 218:16]
  reg [1:0] ram_2_resp; // @[Decoupled.scala 218:16]
  reg [3:0] ram_2_echo_tl_state_size; // @[Decoupled.scala 218:16]
  reg [6:0] ram_2_echo_tl_state_source; // @[Decoupled.scala 218:16]
  reg  ram_2_last; // @[Decoupled.scala 218:16]
  reg [7:0] ram_3_id; // @[Decoupled.scala 218:16]
  reg [127:0] ram_3_data; // @[Decoupled.scala 218:16]
  reg [1:0] ram_3_resp; // @[Decoupled.scala 218:16]
  reg [3:0] ram_3_echo_tl_state_size; // @[Decoupled.scala 218:16]
  reg [6:0] ram_3_echo_tl_state_source; // @[Decoupled.scala 218:16]
  reg  ram_3_last; // @[Decoupled.scala 218:16]
  reg [1:0] deq_ptr_value; // @[Counter.scala 60:40]
  wire [7:0] _GEN_6 = 2'h1 == deq_ptr_value ? ram_1_id : ram_0_id; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [127:0] _GEN_7 = 2'h1 == deq_ptr_value ? ram_1_data : ram_0_data; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [1:0] _GEN_8 = 2'h1 == deq_ptr_value ? ram_1_resp : ram_0_resp; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [3:0] _GEN_9 = 2'h1 == deq_ptr_value ? ram_1_echo_tl_state_size : ram_0_echo_tl_state_size; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [6:0] _GEN_10 = 2'h1 == deq_ptr_value ? ram_1_echo_tl_state_source : ram_0_echo_tl_state_source; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire  _GEN_11 = 2'h1 == deq_ptr_value ? ram_1_last : ram_0_last; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [7:0] _GEN_12 = 2'h2 == deq_ptr_value ? ram_2_id : _GEN_6; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [127:0] _GEN_13 = 2'h2 == deq_ptr_value ? ram_2_data : _GEN_7; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [1:0] _GEN_14 = 2'h2 == deq_ptr_value ? ram_2_resp : _GEN_8; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [3:0] _GEN_15 = 2'h2 == deq_ptr_value ? ram_2_echo_tl_state_size : _GEN_9; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [6:0] _GEN_16 = 2'h2 == deq_ptr_value ? ram_2_echo_tl_state_source : _GEN_10; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire  _GEN_17 = 2'h2 == deq_ptr_value ? ram_2_last : _GEN_11; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire  do_enq = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  reg [1:0] enq_ptr_value; // @[Counter.scala 60:40]
  reg  maybe_full; // @[Decoupled.scala 221:27]
  wire  ptr_match = enq_ptr_value == deq_ptr_value; // @[Decoupled.scala 223:33]
  wire  empty = ptr_match & ~maybe_full; // @[Decoupled.scala 224:25]
  wire  full = ptr_match & maybe_full; // @[Decoupled.scala 225:24]
  wire  do_deq = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  wire [1:0] _value_T_1 = enq_ptr_value + 2'h1; // @[Counter.scala 76:24]
  wire [1:0] _value_T_3 = deq_ptr_value + 2'h1; // @[Counter.scala 76:24]
  assign io_enq_ready = ~full; // @[Decoupled.scala 241:19]
  assign io_deq_valid = ~empty; // @[Decoupled.scala 240:19]
  assign io_deq_bits_id = 2'h3 == deq_ptr_value ? ram_3_id : _GEN_12; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_data = 2'h3 == deq_ptr_value ? ram_3_data : _GEN_13; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_resp = 2'h3 == deq_ptr_value ? ram_3_resp : _GEN_14; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_echo_tl_state_size = 2'h3 == deq_ptr_value ? ram_3_echo_tl_state_size : _GEN_15; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_echo_tl_state_source = 2'h3 == deq_ptr_value ? ram_3_echo_tl_state_source : _GEN_16; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_last = 2'h3 == deq_ptr_value ? ram_3_last : _GEN_17; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_id <= 8'h0;
    end else if (do_enq) begin
      if (2'h0 == enq_ptr_value) begin
        ram_0_id <= io_enq_bits_id;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_data <= 128'h0;
    end else if (do_enq) begin
      if (2'h0 == enq_ptr_value) begin
        ram_0_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_resp <= 2'h0;
    end else if (do_enq) begin
      if (2'h0 == enq_ptr_value) begin
        ram_0_resp <= io_enq_bits_resp;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_echo_tl_state_size <= 4'h0;
    end else if (do_enq) begin
      if (2'h0 == enq_ptr_value) begin
        ram_0_echo_tl_state_size <= io_enq_bits_echo_tl_state_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_echo_tl_state_source <= 7'h0;
    end else if (do_enq) begin
      if (2'h0 == enq_ptr_value) begin
        ram_0_echo_tl_state_source <= io_enq_bits_echo_tl_state_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_last <= 1'h0;
    end else if (do_enq) begin
      if (2'h0 == enq_ptr_value) begin
        ram_0_last <= io_enq_bits_last;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_id <= 8'h0;
    end else if (do_enq) begin
      if (2'h1 == enq_ptr_value) begin
        ram_1_id <= io_enq_bits_id;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_data <= 128'h0;
    end else if (do_enq) begin
      if (2'h1 == enq_ptr_value) begin
        ram_1_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_resp <= 2'h0;
    end else if (do_enq) begin
      if (2'h1 == enq_ptr_value) begin
        ram_1_resp <= io_enq_bits_resp;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_echo_tl_state_size <= 4'h0;
    end else if (do_enq) begin
      if (2'h1 == enq_ptr_value) begin
        ram_1_echo_tl_state_size <= io_enq_bits_echo_tl_state_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_echo_tl_state_source <= 7'h0;
    end else if (do_enq) begin
      if (2'h1 == enq_ptr_value) begin
        ram_1_echo_tl_state_source <= io_enq_bits_echo_tl_state_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_last <= 1'h0;
    end else if (do_enq) begin
      if (2'h1 == enq_ptr_value) begin
        ram_1_last <= io_enq_bits_last;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_id <= 8'h0;
    end else if (do_enq) begin
      if (2'h2 == enq_ptr_value) begin
        ram_2_id <= io_enq_bits_id;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_data <= 128'h0;
    end else if (do_enq) begin
      if (2'h2 == enq_ptr_value) begin
        ram_2_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_resp <= 2'h0;
    end else if (do_enq) begin
      if (2'h2 == enq_ptr_value) begin
        ram_2_resp <= io_enq_bits_resp;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_echo_tl_state_size <= 4'h0;
    end else if (do_enq) begin
      if (2'h2 == enq_ptr_value) begin
        ram_2_echo_tl_state_size <= io_enq_bits_echo_tl_state_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_echo_tl_state_source <= 7'h0;
    end else if (do_enq) begin
      if (2'h2 == enq_ptr_value) begin
        ram_2_echo_tl_state_source <= io_enq_bits_echo_tl_state_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_last <= 1'h0;
    end else if (do_enq) begin
      if (2'h2 == enq_ptr_value) begin
        ram_2_last <= io_enq_bits_last;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_id <= 8'h0;
    end else if (do_enq) begin
      if (2'h3 == enq_ptr_value) begin
        ram_3_id <= io_enq_bits_id;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_data <= 128'h0;
    end else if (do_enq) begin
      if (2'h3 == enq_ptr_value) begin
        ram_3_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_resp <= 2'h0;
    end else if (do_enq) begin
      if (2'h3 == enq_ptr_value) begin
        ram_3_resp <= io_enq_bits_resp;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_echo_tl_state_size <= 4'h0;
    end else if (do_enq) begin
      if (2'h3 == enq_ptr_value) begin
        ram_3_echo_tl_state_size <= io_enq_bits_echo_tl_state_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_echo_tl_state_source <= 7'h0;
    end else if (do_enq) begin
      if (2'h3 == enq_ptr_value) begin
        ram_3_echo_tl_state_source <= io_enq_bits_echo_tl_state_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_last <= 1'h0;
    end else if (do_enq) begin
      if (2'h3 == enq_ptr_value) begin
        ram_3_last <= io_enq_bits_last;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      deq_ptr_value <= 2'h0;
    end else if (do_deq) begin
      deq_ptr_value <= _value_T_3;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      enq_ptr_value <= 2'h0;
    end else if (do_enq) begin
      enq_ptr_value <= _value_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      maybe_full <= 1'h0;
    end else if (do_enq != do_deq) begin
      maybe_full <= do_enq;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ram_0_id = _RAND_0[7:0];
  _RAND_1 = {4{`RANDOM}};
  ram_0_data = _RAND_1[127:0];
  _RAND_2 = {1{`RANDOM}};
  ram_0_resp = _RAND_2[1:0];
  _RAND_3 = {1{`RANDOM}};
  ram_0_echo_tl_state_size = _RAND_3[3:0];
  _RAND_4 = {1{`RANDOM}};
  ram_0_echo_tl_state_source = _RAND_4[6:0];
  _RAND_5 = {1{`RANDOM}};
  ram_0_last = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  ram_1_id = _RAND_6[7:0];
  _RAND_7 = {4{`RANDOM}};
  ram_1_data = _RAND_7[127:0];
  _RAND_8 = {1{`RANDOM}};
  ram_1_resp = _RAND_8[1:0];
  _RAND_9 = {1{`RANDOM}};
  ram_1_echo_tl_state_size = _RAND_9[3:0];
  _RAND_10 = {1{`RANDOM}};
  ram_1_echo_tl_state_source = _RAND_10[6:0];
  _RAND_11 = {1{`RANDOM}};
  ram_1_last = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  ram_2_id = _RAND_12[7:0];
  _RAND_13 = {4{`RANDOM}};
  ram_2_data = _RAND_13[127:0];
  _RAND_14 = {1{`RANDOM}};
  ram_2_resp = _RAND_14[1:0];
  _RAND_15 = {1{`RANDOM}};
  ram_2_echo_tl_state_size = _RAND_15[3:0];
  _RAND_16 = {1{`RANDOM}};
  ram_2_echo_tl_state_source = _RAND_16[6:0];
  _RAND_17 = {1{`RANDOM}};
  ram_2_last = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  ram_3_id = _RAND_18[7:0];
  _RAND_19 = {4{`RANDOM}};
  ram_3_data = _RAND_19[127:0];
  _RAND_20 = {1{`RANDOM}};
  ram_3_resp = _RAND_20[1:0];
  _RAND_21 = {1{`RANDOM}};
  ram_3_echo_tl_state_size = _RAND_21[3:0];
  _RAND_22 = {1{`RANDOM}};
  ram_3_echo_tl_state_source = _RAND_22[6:0];
  _RAND_23 = {1{`RANDOM}};
  ram_3_last = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  deq_ptr_value = _RAND_24[1:0];
  _RAND_25 = {1{`RANDOM}};
  enq_ptr_value = _RAND_25[1:0];
  _RAND_26 = {1{`RANDOM}};
  maybe_full = _RAND_26[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ram_0_id = 8'h0;
  end
  if (rf_reset) begin
    ram_0_data = 128'h0;
  end
  if (rf_reset) begin
    ram_0_resp = 2'h0;
  end
  if (rf_reset) begin
    ram_0_echo_tl_state_size = 4'h0;
  end
  if (rf_reset) begin
    ram_0_echo_tl_state_source = 7'h0;
  end
  if (rf_reset) begin
    ram_0_last = 1'h0;
  end
  if (rf_reset) begin
    ram_1_id = 8'h0;
  end
  if (rf_reset) begin
    ram_1_data = 128'h0;
  end
  if (rf_reset) begin
    ram_1_resp = 2'h0;
  end
  if (rf_reset) begin
    ram_1_echo_tl_state_size = 4'h0;
  end
  if (rf_reset) begin
    ram_1_echo_tl_state_source = 7'h0;
  end
  if (rf_reset) begin
    ram_1_last = 1'h0;
  end
  if (rf_reset) begin
    ram_2_id = 8'h0;
  end
  if (rf_reset) begin
    ram_2_data = 128'h0;
  end
  if (rf_reset) begin
    ram_2_resp = 2'h0;
  end
  if (rf_reset) begin
    ram_2_echo_tl_state_size = 4'h0;
  end
  if (rf_reset) begin
    ram_2_echo_tl_state_source = 7'h0;
  end
  if (rf_reset) begin
    ram_2_last = 1'h0;
  end
  if (rf_reset) begin
    ram_3_id = 8'h0;
  end
  if (rf_reset) begin
    ram_3_data = 128'h0;
  end
  if (rf_reset) begin
    ram_3_resp = 2'h0;
  end
  if (rf_reset) begin
    ram_3_echo_tl_state_size = 4'h0;
  end
  if (rf_reset) begin
    ram_3_echo_tl_state_source = 7'h0;
  end
  if (rf_reset) begin
    ram_3_last = 1'h0;
  end
  if (reset) begin
    deq_ptr_value = 2'h0;
  end
  if (reset) begin
    enq_ptr_value = 2'h0;
  end
  if (reset) begin
    maybe_full = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
