// ---------------------------------------------------------------------
//
// ------------------------------------------------------------------------------
// 
// Copyright 2005 - 2020 Synopsys, INC.
// 
// This Synopsys IP and all associated documentation are proprietary to
// Synopsys, Inc. and may only be used pursuant to the terms and conditions of a
// written license agreement with Synopsys, Inc. All other use, reproduction,
// modification, or distribution of the Synopsys IP or the associated
// documentation is strictly prohibited.
// 
// Component Name   : DW_axi_gs
// Component Version: 2.04a
// Release Type     : GA
// ------------------------------------------------------------------------------

// 
// Release version :  2.04a
// File Version     :        $Revision: #13 $ 
// Revision: $Id: //dwh/DW_ocb/DW_axi_gs/amba_dev/src/DW_axi_gs_resp.v#13 $ 
//
// -------------------------------------------------------------------------
//
// AUTHOR:    James Feagans      2/24/2005
//
// VERSION:   DW_axi_gs_resp Verilog Synthesis Model
//
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// ABSTRACT:  AXI to GIF Response Module
//
// This module handles the GIF to AXI transaction responses for RD and WR.
//
// Response datapath (resp):         <from req data path>
//                                        |          |
//                                   |--------| |--------|
//                                   |fifo_bid| |fifo_rid|
//                                   |--------| |--------|
//                                        |__________|
//                                             |                   mready
// AXI BRESP -\                    |---------| | /-----\           svalid
//  Channel    \___________________|fifo_resp|---|logic|------ GIF sdata
//             /                   |_________|   \-----/           sresp
// AXI RDATA -/
//  Channel
//
//
//-----------------------------------------------------------------------------

`include "axi_gs_64_11_DW_axi_gs_all_includes.vh"

module axi_gs_64_11_DW_axi_gs_resp(
  // AXI INTERFACE
  // Global
  aclk, 
                      aresetn,
                      // Write Response Channel
                      bid, 
                      bresp, 
                      bvalid, 
                      bready,
                      // Read Data Channel
                      rid, 
                      rdata, 
                      rresp, 
                      rlast, 
                      rvalid, 
                      rready,
                      // GENERIC SLAVE INTERFACE
                      // Global
                      gclken,
                      // Response Channel
                      sdata,
                      mready,
                      // INTERNAL CONNECTIONS
                      // Inputs from sm
                      start_wr, 
                      start_rd, 
                      advance_rd,
                      auto_sresp,
                      auto_svalid,
                      // Inputs from req
                      id,
                      len,
                      exokay, 
                      exfail,
                      resp_cactive, // Output to low_pwr
                      // Outputs to sm
                      advance_rd_ready, 
                      start_wr_ready, 
                      start_rd_ready
                      );
 
  
// ----------------------------------------------------------------------------
// PARAMETERS
// ----------------------------------------------------------------------------

parameter FBID_WIDTH  = `axi_gs_64_11_GS_ID+1+1;
parameter FRID_WIDTH  = `axi_gs_64_11_GS_ID+`axi_gs_64_11_GS_BW+1+1;
parameter FRESP_WIDTH =
        `axi_gs_64_11_GS_DW+`axi_gs_64_11_GS_ID+2+1+1+1;


// ----------------------------------------------------------------------------
// PORTS
// ----------------------------------------------------------------------------

// AXI INTERFACE
// Global
input  aclk;
input  aresetn;
// Write response channel
output [`axi_gs_64_11_GS_ID-1:0] bid;
output [1:0] bresp;
output bvalid;
input  bready;
// Read data channel
output [`axi_gs_64_11_GS_ID-1:0] rid;
output [`axi_gs_64_11_GS_DW-1:0] rdata;
output [1:0] rresp;
output rlast;
output rvalid;
input  rready;

// GENERIC SLAVE INTERFACE
// Global
input  gclken;
// Response Channel
input  [`axi_gs_64_11_GS_DW-1:0] sdata;
output mready;


// INTERNAL CONNECTIONS
// Inputs from sm
input  start_wr;
input  start_rd;
input  advance_rd;
input  auto_sresp;
input  auto_svalid;
// Inputs from req
input  [`axi_gs_64_11_GS_ID-1:0] id;
input  [`axi_gs_64_11_GS_BW-1:0] len;
input  exokay;
input  exfail;
// Outputs to sm
output start_wr_ready;
output start_rd_ready;
output advance_rd_ready;
// Outputs to low_pwr
output resp_cactive;


// ----------------------------------------------------------------------------
// INTERNAL SIGNALS
// ----------------------------------------------------------------------------

wire fbexfail, fbexokay;
wire frexfail, frexokay;

// fifo_bid
wire fbid_src_rdy;
wire [FBID_WIDTH-1:0] fbid_src_data;
wire [FBID_WIDTH-1:0] fbid_dst_data;
wire [`axi_gs_64_11_GS_ID-1:0] fbid;

//In the configuration, ((@GS_LOWPWR_HS_IF==1) && (@GS_LOWPWR_LEGACY_IF == 0)), these signals are not used. 
// They are not removed because they come from different instances of the same module.
wire fbid_dst_vld;
wire frid_dst_vld;

wire [FRID_WIDTH-1:0] frid_dst_data;
// fifo_rid
wire frid_src_rdy;
wire [FRID_WIDTH-1:0] frid_src_data;
wire [`axi_gs_64_11_GS_BW-1:0] frlen;
wire [`axi_gs_64_11_GS_ID-1:0] frid;
wire [`axi_gs_64_11_GS_ID-1:0] muxed_id;
wire start_rd_gc;
wire start_wr_gc;
wire wr_resp_rdy;

// fifo_resp
wire [FRESP_WIDTH-1:0] fresp_src_data, fresp_dst_data;
wire fresp_src_rdy;
wire fresp_dst_rdy, fresp_dst_vld;

// indicates read response, write response, and last read response
wire rd_resp, wr_resp;
wire last_rd_resp;

// read response counter
reg  [`axi_gs_64_11_GS_BW-1:0] rd_resp_ctr;
wire  [`axi_gs_64_11_GS_BW-1:0] rd_resp_ctr_c;
wire [`axi_gs_64_11_GS_BW-1:0] next_rd_resp_ctr;


// internally or externally driven depending on `GS_GIF_SRESP
wire [1:0] sresp_int;
wire svalid_int;

// registers used for storing next automatic response
reg  sresp_reg;
wire  sresp_reg_c;
reg  svalid_reg;
wire  svalid_reg_c;

// multiplexed output of exfail and exokay signals of frid and fbid
wire dout_exfail, dout_exokay;

// AXI format of sresp_int for input into fifo_bresp or fifo_rdata
reg  [1:0] axi_sresp_int;

wire sresp_int_ok;


//`ifndef EXTD_GIF_MODE
// These counters are used in GIF Lite mode to guarantee that all outstanding
// GIF responses will be accepted by the gasket. This means that the gasket may
// only issue as many response-generating requests as it has available response
// buffer space. The token counter is initialized to the depth of fresp. It
// is decremented upon a new GIF (rd/wr) request and incremented upon AXI
// accepting a (rd/wr) response. If the counter reaches zero, new requests are
// stalled.
reg  [2:0] token, next_token;
wire [1:0] token_select;
//`endif

wire rw_resp;
 

// ----------------------------------------------------------------------------
// DESIGN
// ----------------------------------------------------------------------------

// In some configurations this signal is tied to logic0/1. This is not an issue
// as it takes a constant value based on configuration parameters.
// indicate to sm whether ready to advance the beat of a read transaction
assign advance_rd_ready =
  (
//`ifndef EXTD_GIF_MODE
(token != 3'b000) | 
//`endif
((`axi_gs_64_11_GS_DIRECT_AXI_READY == 1'b1) ? fresp_dst_rdy : 1'b0));

// indicate to sm whether ready to begin a new write transaction
assign start_wr_ready = 
 fbid_src_rdy & 
advance_rd_ready;

// indicate to sm whether ready to begin a new read transaction
assign start_rd_ready = 
 frid_src_rdy & 
advance_rd_ready;

// indicate the clock is required
assign resp_cactive = 
 fbid_dst_vld | frid_dst_vld | 
 fresp_dst_vld;

// If in GIF Lite mode, use internally driven sresp/svalid and drive mready = 1.
// Otherwise, use the port connections and fifo ready signal.
assign mready     = 1'b1;
// In LITE mode the MSBit is tied to zero as that bit is not valid.
assign sresp_int  = {1'b0, sresp_reg};
assign svalid_int = svalid_reg;
  
// Response type identification
// Controls rid, bid, rdata, and bresp FIFOs
assign rd_resp = gclken & svalid_int & (~sresp_int[0]);
assign wr_resp = gclken & svalid_int & sresp_int[0];
assign rw_resp = (mready & (rd_resp | wr_resp));
assign last_rd_resp = rd_resp & (rd_resp_ctr == frlen) & mready;


// ----------------------------------------------------------------------------
// FIFO instantiations
// ----------------------------------------------------------------------------
// fifo_bid
assign fbid_src_data = {id, exokay, exfail};
assign start_wr_gc =  start_wr & gclken;
assign wr_resp_rdy = wr_resp & mready;
  // Clock enable signal is not used in this design.
  // it will not cause any functional failure.
  // turn off check for unconnecte ports.
  axi_gs_64_11_DW_axi_gs_fifo
   #(FBID_WIDTH, `axi_gs_64_11_GS_BID_BUFFER, 0, 0)
  fifo_bid (.clk(aclk),
//            .clk_en(),
            .rst_n(aresetn),      
            .src_data(fbid_src_data),
            .src_vld(start_wr_gc), 
            .src_rdy(fbid_src_rdy),   
            .dst_data(fbid_dst_data),
            .dst_vld(fbid_dst_vld), 
            .dst_rdy(wr_resp_rdy)
            );
assign fbid     = fbid_dst_data[`axi_gs_64_11_GS_ID+1:2];
assign fbexokay = fbid_dst_data[1];
assign fbexfail = fbid_dst_data[0];

//wire [`axi_gs_64_11_GS_W_SBW_INT-1:0] rsideband ;
// fifo_rid
assign frid_src_data = {id, len, exokay, exfail};
assign start_rd_gc = start_rd & gclken;
  // Clock enable signal is not used in this design.
  // it will not cause any functional failure.
  //turn off check for unconnected port
  axi_gs_64_11_DW_axi_gs_fifo
   #(FRID_WIDTH, `axi_gs_64_11_GS_RID_BUFFER, 0, 0)
  fifo_rid (.clk(aclk),
//            .clk_en(),
            .rst_n(aresetn),      
            .src_data(frid_src_data),
            .src_vld(start_rd_gc), 
            .src_rdy(frid_src_rdy),   
            .dst_data(frid_dst_data),
            .dst_vld(frid_dst_vld),
            .dst_rdy(last_rd_resp)
            );
assign frid     = frid_dst_data[`axi_gs_64_11_GS_ID+`axi_gs_64_11_GS_BW+1:`axi_gs_64_11_GS_BW+2];
assign frlen    = frid_dst_data[`axi_gs_64_11_GS_BW+1:2];
assign frexokay = frid_dst_data[1];
assign frexfail = frid_dst_data[0];

// fifo_resp
assign muxed_id = (rd_resp) ? frid: fbid;

assign fresp_src_data = {
sdata,
muxed_id, 
axi_sresp_int, 
last_rd_resp, 
rd_resp, wr_resp};

assign fresp_dst_rdy = (rvalid & rready) | (bvalid & bready);
  // Clock enable signal is not used in this design.
  // it will not cause any functional failure.
  //turn off check for unconnected port
  //spyglass disable_block W528
  //SMD: A signal or variable is set but never read
  //SJ : unused signals from the bcm65 module.
  axi_gs_64_11_DW_axi_gs_fifo
   #(FRESP_WIDTH, `axi_gs_64_11_GS_RESP_BUFFER, 0, `axi_gs_64_11_GS_DIRECT_AXI_READY)
  fifo_resp (.clk(aclk),
//             .clk_en(),
             .rst_n(aresetn),      
             .src_data(fresp_src_data),
             .src_vld(rw_resp), 
             .src_rdy(fresp_src_rdy),   
             .dst_data(fresp_dst_data),
             .dst_vld(fresp_dst_vld), 
             .dst_rdy(fresp_dst_rdy)
             );
//spyglass enable_block W528
// Output assignments for AXI RDATA and BRESP channels
assign rdata  = fresp_dst_data[FRESP_WIDTH-1
 : 5+`axi_gs_64_11_GS_ID];
assign rid    = fresp_dst_data[4+`axi_gs_64_11_GS_ID:5];
assign rresp  = fresp_dst_data[4:3];
assign rlast  = fresp_dst_data[2];
assign rvalid = fresp_dst_vld & fresp_dst_data[1];
assign bvalid = fresp_dst_vld & fresp_dst_data[0];
assign bid    = rid;
assign bresp  = rresp;



// ----------------------------------------------------------------------------
// sresp_int to axi_sresp_int conversion
// ----------------------------------------------------------------------------

assign sresp_int_ok = (sresp_int == `axi_gs_64_11_GS_SRESP_OK_R) |
  (sresp_int == `axi_gs_64_11_GS_SRESP_OK_W);
/*
assign dout_exokay = 
`ifndef EXTD_GIF_MODE
(rd_resp) ? frexokay: fbexokay;
`else
1'b0;
`endif
assign dout_exfail = 
`ifndef EXTD_GIF_MODE
(rd_resp) ? frexfail: fbexfail;
`else
1'b0;
`endif
*/
assign dout_exokay = (rd_resp) ? frexokay: fbexokay;
assign dout_exfail = (rd_resp) ? frexfail: fbexfail;

always @(*)
begin : gen_axi_resp1_PROC
  case ({dout_exfail, dout_exokay})
    // exclusive access table check succeeded
    2'b01:
      begin
        if(sresp_int_ok)
          axi_sresp_int = `axi_gs_64_11_GS_AXI_EXOKAY;
        else
          axi_sresp_int = `axi_gs_64_11_GS_AXI_SLVERR;
      end
    // normal access
    default:
      begin
        if(sresp_int_ok)
          axi_sresp_int = `axi_gs_64_11_GS_AXI_OKAY;
        else
          axi_sresp_int = `axi_gs_64_11_GS_AXI_SLVERR;
      end
  endcase
end // gen_axi_resp

parameter [`axi_gs_64_11_GS_BW-1:0] TEMP_VAL_1 = 1;
assign next_rd_resp_ctr = (last_rd_resp) ? 0 : ((rd_resp & mready) ? rd_resp_ctr + TEMP_VAL_1 : rd_resp_ctr);  

// ----------------------------------------------------------------------------
// Stall request if necessary
// ----------------------------------------------------------------------------
assign token_select = 
    {(bvalid & bready) | (rvalid & rready), gclken & (start_wr | advance_rd)};

always @(token_select or token)
begin : token_inc_dec_mux_PROC
  case (token_select)
    2'b01: next_token = token - 3'b001;
    2'b10: next_token = token + 3'b001;
    default: next_token = token;
  endcase
end // token_inc_dec_mux

parameter [2:0] TOKEN_RST_VAL = `axi_gs_64_11_GS_RESP_BUFFER;
always @(posedge aclk or negedge aresetn)
begin : dff_PROC
  if (!aresetn) begin
    token       <= TOKEN_RST_VAL;
  end
  else begin
    if (gclken) begin
      token       <= next_token;
    end
    else begin
      token       <= next_token;
    end
  end
end // dff

// ----------------------------------------------------------------------------
// Flip flops
// ----------------------------------------------------------------------------

assign rd_resp_ctr_c = rd_resp_ctr;
always @(posedge aclk or negedge aresetn)
begin : dff_1_PROC
  if (!aresetn) begin
    rd_resp_ctr <= {`axi_gs_64_11_GS_BW{1'b0}};
  end
  else begin
    if (gclken) begin
      rd_resp_ctr <= next_rd_resp_ctr;
    end
    else begin
      rd_resp_ctr <= rd_resp_ctr_c;
    end
  end
end // dff


// ----------------------------------------------------------------------------
// Flip flops
// ----------------------------------------------------------------------------
assign sresp_reg_c = sresp_reg;
assign svalid_reg_c = svalid_reg;
always @(posedge aclk or negedge aresetn)
begin : dff1_PROC
  if (!aresetn) begin
    sresp_reg   <= 1'b0;
    svalid_reg  <= 1'b0;
  end
  else begin
    if (gclken) begin
      sresp_reg   <= auto_sresp;
      svalid_reg  <= auto_svalid;
    end
    else begin
      sresp_reg   <= sresp_reg_c;
      svalid_reg  <= svalid_reg_c;
    end
  end
end // dff1
endmodule
