//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__BundleBridgeNexus_28(
  output [1:0] auto_out_1,
  output [1:0] auto_out_0
);
  wire [1:0] outputs_0 = 2'h0; // @[HasTiles.scala 166:32]
  wire [1:0] outputs_1 = 2'h1; // @[HasTiles.scala 166:32]
  assign auto_out_1 = outputs_1; // @[Nodes.scala 1617:13 BundleBridge.scala 151:67]
  assign auto_out_0 = outputs_0; // @[Nodes.scala 1617:13 BundleBridge.scala 151:67]
endmodule
