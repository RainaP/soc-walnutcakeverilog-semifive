#-----------------------------------------------------------------------------------
DESIGN_NAME = "connecting_hierarchy_peri"
OUTPUT_PATH = "../../rtl.src/core"
#-----------------------------------------------------------------------------------
INSTANTIATIONS = {
    'MBUS_0  '   => ['BusDomainBridge    ', "../../../../block-widgets-semifive/src/main/resources/vsrc/BusDomainBridge.sv",
        {'AW_FIFO_DEPTH' => 4, 'W_FIFO_DEPTH' => 6, 'B_FIFO_DEPTH' => 2, 'AR_FIFO_DEPTH' => 4, 'R_FIFO_DEPTH' => 6,
		'WR_MAX_OT' => 64, 'RD_MAX_OT' => 64,
		'NUM_OF_M2S_SLICE' => 6, 'NUM_OF_S2M_SLICE' => 6, 'MST_REG_SLICE_PAYLOAD_OUT' => 1, 'SLV_REG_SLICE_PAYLOAD_OUT' => 1,
		'ARID_W' => 6, 'AWID_W' => 6, 'BID_W' => 6, 'RID_W' => 6, 'WID_W' => 1, 
		'ARADDR_W' => 36, 'AWADDR_W' => 36,
		'AWUSER_W' => 10, 'ARUSER_W' => 10,
		'RDATA_W' => 64, 'WDATA_W' => 64, 'WSTRB_W' => 8, 
		'BUSER_W' => 1, 'RUSER_W' => 1, 'WUSER_W' => 1, 'AWLEN_W' => 8, 'AWBURST_W' => 2, 'RRESP_W' => 2, 'ARQOS_W' => 1,
		'ARBURST_W' => 2, 'ARLOCK_W' => 1, 'BRESP_W' => 2, 'AWSIZE_W' => 3, 'AWQOS_W' => 1, 'AWPROT_W' => 3, 'AWREGION_W' => 1,
		'AWCACHE_W' => 4, 'AWLOCK_W' => 1, 'ARPROT_W' => 3, 'ARCACHE_W' => 4, 'ARREGION_W' => 1, 'ARLEN_W' => 8, 'ARSIZE_W' => 3}],
    'PBUS_0  '   => ['BusDomainBridge    ', "../../../../block-widgets-semifive/src/main/resources/vsrc/BusDomainBridge.sv",
        {'AW_FIFO_DEPTH' => 4, 'W_FIFO_DEPTH' => 6, 'B_FIFO_DEPTH' => 2, 'AR_FIFO_DEPTH' => 4, 'R_FIFO_DEPTH' => 6,
		'WR_MAX_OT' => 64, 'RD_MAX_OT' => 64,
		'NUM_OF_M2S_SLICE' => 6, 'NUM_OF_S2M_SLICE' => 6, 'MST_REG_SLICE_PAYLOAD_OUT' => 1, 'SLV_REG_SLICE_PAYLOAD_OUT' => 1,
		'ARID_W' => 1, 'AWID_W' => 1, 'BID_W' => 1, 'RID_W' => 1, 'WID_W' => 1, 
		'ARADDR_W' => 32, 'AWADDR_W' => 32,
		'AWUSER_W' => 1, 'ARUSER_W' => 1,
		'RDATA_W' => 32, 'WDATA_W' => 32, 'WSTRB_W' => 4, 
		'BUSER_W' => 1, 'RUSER_W' => 1, 'WUSER_W' => 1, 'AWLEN_W' => 8, 'AWBURST_W' => 2, 'RRESP_W' => 2, 'ARQOS_W' => 1,
		'ARBURST_W' => 2, 'ARLOCK_W' => 1, 'BRESP_W' => 2, 'AWSIZE_W' => 3, 'AWQOS_W' => 1, 'AWPROT_W' => 3, 'AWREGION_W' => 1,
		'AWCACHE_W' => 4, 'AWLOCK_W' => 1, 'ARPROT_W' => 3, 'ARCACHE_W' => 4, 'ARREGION_W' => 1, 'ARLEN_W' => 8, 'ARSIZE_W' => 3}]
}
#-----------------------------------------------------------------------------------
CONNECTIONS = [
    ['MBUS_0  ', '(RSTN|CLK)__MST                                          ', '<top>', 'PERI_SUBSYSTEM_0__MBUS_PERI_0__\1__TX'],
    ['PBUS_0  ', '(RSTN|CLK)__SLV                                          ', '<top>', 'PERI_SUBSYSTEM_0__PBUS_PERI_0__\1__RX'],
    ['MBUS_0  ', '(RSTN|CLK)__SLV                                          ', '<top>', 'MAINBUS_SUBSYSTEM_0__MBUS_MAIN_0__\1__PERI_RX'],
    ['PBUS_0  ', '(RSTN|CLK)__MST                                          ', '<top>', 'MAINBUS_SUBSYSTEM_0__PBUS_MAIN_0__\1__PERI_TX'],
    ['MBUS_0  ', 'AXI4__MST__((:not(WUSER|BUSER|RUSER|A.REGION|A.QOS|WID)))', '<top>', 'PERI_SUBSYSTEM_0__MBUS_PERI_0__AXI4__TX__\1'],
    ['PBUS_0  ', 'AXI4__SLV__((:not(WUSER|BUSER|RUSER|A.REGION|A.QOS|WID)))', '<top>', 'PERI_SUBSYSTEM_0__PBUS_PERI_0__AXI4__RX__\1'],
    ['MBUS_0  ', 'AXI4__SLV__((:not(WUSER|BUSER|RUSER|A.REGION|A.QOS|WID)))', '<top>', 'MAINBUS_SUBSYSTEM_0__MBUS_MAIN_0__AXI4__PERI_RX__\1'],
    ['PBUS_0  ', 'AXI4__MST__((:not(WUSER|BUSER|RUSER|A.REGION|A.QOS|WID)))', '<top>', 'MAINBUS_SUBSYSTEM_0__PBUS_MAIN_0__AXI4__PERI_TX__\1']
]
#-----------------------------------------------------------------------------------
#IO_INSTANTIATIONS = [
#]
#-----------------------------------------------------------------------------------
OPENED_OR_TIED = [
#    ['<top>', '.*       ', '1']
    ['MBUS_0', 'AXI4__MST__(WUSER|BUSER|RUSER|A.REGION|A.QOS|WID)', '0'],
    ['MBUS_0', 'AXI4__SLV__(WUSER|BUSER|RUSER|A.REGION|A.QOS|WID)', '0'],
    ['MBUS_0', '(CFG|STS)__.*', '0'],
    ['PBUS_0', 'AXI4__MST__(WUSER|BUSER|RUSER|A.REGION|A.QOS|WID)', '0'],
    ['PBUS_0', 'AXI4__SLV__(WUSER|BUSER|RUSER|A.REGION|A.QOS|WID)', '0'],
    ['PBUS_0', '(CFG|STS)__.*', '0']
]
