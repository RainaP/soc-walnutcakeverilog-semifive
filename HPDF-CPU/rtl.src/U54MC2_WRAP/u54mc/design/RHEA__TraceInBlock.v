//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TraceInBlock(
  input         rf_reset,
  input         clock,
  input         reset,
  input         auto_trace_legacy_in_0_valid,
  input  [39:0] auto_trace_legacy_in_0_iaddr,
  input  [31:0] auto_trace_legacy_in_0_insn,
  input  [2:0]  auto_trace_legacy_in_0_priv,
  input         auto_trace_legacy_in_0_exception,
  input         auto_trace_legacy_in_0_interrupt,
  input         io_teActive,
  output        io_teActiveCore,
  input         io_tlClock,
  input         io_tlReset,
  input         io_hartIsInReset,
  input         io_ingControlAsync_mem_0_teEnable,
  input         io_ingControlAsync_mem_0_teTracing,
  input  [2:0]  io_ingControlAsync_mem_0_teInstruction,
  input         io_ingControlAsync_mem_0_teStallEnable,
  input  [3:0]  io_ingControlAsync_mem_0_teSyncMaxInst,
  input  [3:0]  io_ingControlAsync_mem_0_teSyncMaxBTM,
  output        io_ingControlAsync_ridx,
  input         io_ingControlAsync_widx,
  output        io_traceOn,
  output        io_iretire_0,
  output        io_ivalid_0,
  output        io_ingValid,
  input         io_ingTrigger_0_traceon,
  input         io_ingTrigger_0_traceoff,
  input         io_ingTrigger_0_wpsync,
  input         io_teIdle,
  input         io_btmEvent_ready,
  output        io_btmEvent_valid,
  output [4:0]  io_btmEvent_bits_reason,
  output [38:0] io_btmEvent_bits_iaddr,
  output [13:0] io_btmEvent_bits_icnt,
  output [31:0] io_btmEvent_bits_hist,
  output [1:0]  io_btmEvent_bits_hisv,
  output        io_traceStall,
  input         io_pcActive,
  output        io_pcCapture_valid,
  output [38:0] io_pcCapture_bits,
  output        io_pcSample_valid,
  output [38:0] io_pcSample_bits
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [63:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [63:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [63:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [63:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [63:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [63:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [63:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [63:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [31:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
`endif // RANDOMIZE_REG_INIT
  wire  inner__clock; // @[TraceInBlock.scala 84:23]
  wire  inner__reset; // @[TraceInBlock.scala 84:23]
  wire  inner__io_traceOnCore; // @[TraceInBlock.scala 84:23]
  wire  inner__io_traceOn; // @[TraceInBlock.scala 84:23]
  wire [4:0] inner__io_btmEventRational_bits0_reason; // @[TraceInBlock.scala 84:23]
  wire [38:0] inner__io_btmEventRational_bits0_iaddr; // @[TraceInBlock.scala 84:23]
  wire [13:0] inner__io_btmEventRational_bits0_icnt; // @[TraceInBlock.scala 84:23]
  wire [31:0] inner__io_btmEventRational_bits0_hist; // @[TraceInBlock.scala 84:23]
  wire [1:0] inner__io_btmEventRational_bits0_hisv; // @[TraceInBlock.scala 84:23]
  wire [4:0] inner__io_btmEventRational_bits1_reason; // @[TraceInBlock.scala 84:23]
  wire [38:0] inner__io_btmEventRational_bits1_iaddr; // @[TraceInBlock.scala 84:23]
  wire [13:0] inner__io_btmEventRational_bits1_icnt; // @[TraceInBlock.scala 84:23]
  wire [31:0] inner__io_btmEventRational_bits1_hist; // @[TraceInBlock.scala 84:23]
  wire [1:0] inner__io_btmEventRational_bits1_hisv; // @[TraceInBlock.scala 84:23]
  wire  inner__io_btmEventRational_valid; // @[TraceInBlock.scala 84:23]
  wire [1:0] inner__io_btmEventRational_source; // @[TraceInBlock.scala 84:23]
  wire  inner__io_btmEventRational_ready; // @[TraceInBlock.scala 84:23]
  wire [1:0] inner__io_btmEventRational_sink; // @[TraceInBlock.scala 84:23]
  wire  inner__io_btmEvent_ready; // @[TraceInBlock.scala 84:23]
  wire  inner__io_btmEvent_valid; // @[TraceInBlock.scala 84:23]
  wire [4:0] inner__io_btmEvent_bits_reason; // @[TraceInBlock.scala 84:23]
  wire [38:0] inner__io_btmEvent_bits_iaddr; // @[TraceInBlock.scala 84:23]
  wire [13:0] inner__io_btmEvent_bits_icnt; // @[TraceInBlock.scala 84:23]
  wire [31:0] inner__io_btmEvent_bits_hist; // @[TraceInBlock.scala 84:23]
  wire [1:0] inner__io_btmEvent_bits_hisv; // @[TraceInBlock.scala 84:23]
  wire [38:0] inner__io_pcCaptureAsync_mem_0; // @[TraceInBlock.scala 84:23]
  wire  inner__io_pcCaptureAsync_ridx; // @[TraceInBlock.scala 84:23]
  wire  inner__io_pcCaptureAsync_widx; // @[TraceInBlock.scala 84:23]
  wire [38:0] inner__io_pcSampleAsync_mem_0; // @[TraceInBlock.scala 84:23]
  wire  inner__io_pcSampleAsync_ridx; // @[TraceInBlock.scala 84:23]
  wire  inner__io_pcSampleAsync_widx; // @[TraceInBlock.scala 84:23]
  wire  inner__io_pcCapture_valid; // @[TraceInBlock.scala 84:23]
  wire [38:0] inner__io_pcCapture_bits; // @[TraceInBlock.scala 84:23]
  wire  inner__io_pcSample_valid; // @[TraceInBlock.scala 84:23]
  wire [38:0] inner__io_pcSample_bits; // @[TraceInBlock.scala 84:23]
  wire  ingControlCoreIn_sink_rf_reset; // @[AsyncQueue.scala 207:22]
  wire  ingControlCoreIn_sink_clock; // @[AsyncQueue.scala 207:22]
  wire  ingControlCoreIn_sink_reset; // @[AsyncQueue.scala 207:22]
  wire  ingControlCoreIn_sink_io_deq_valid; // @[AsyncQueue.scala 207:22]
  wire  ingControlCoreIn_sink_io_deq_bits_teEnable; // @[AsyncQueue.scala 207:22]
  wire  ingControlCoreIn_sink_io_deq_bits_teTracing; // @[AsyncQueue.scala 207:22]
  wire [2:0] ingControlCoreIn_sink_io_deq_bits_teInstruction; // @[AsyncQueue.scala 207:22]
  wire  ingControlCoreIn_sink_io_deq_bits_teStallEnable; // @[AsyncQueue.scala 207:22]
  wire [3:0] ingControlCoreIn_sink_io_deq_bits_teSyncMaxInst; // @[AsyncQueue.scala 207:22]
  wire [3:0] ingControlCoreIn_sink_io_deq_bits_teSyncMaxBTM; // @[AsyncQueue.scala 207:22]
  wire  ingControlCoreIn_sink_io_async_mem_0_teEnable; // @[AsyncQueue.scala 207:22]
  wire  ingControlCoreIn_sink_io_async_mem_0_teTracing; // @[AsyncQueue.scala 207:22]
  wire [2:0] ingControlCoreIn_sink_io_async_mem_0_teInstruction; // @[AsyncQueue.scala 207:22]
  wire  ingControlCoreIn_sink_io_async_mem_0_teStallEnable; // @[AsyncQueue.scala 207:22]
  wire [3:0] ingControlCoreIn_sink_io_async_mem_0_teSyncMaxInst; // @[AsyncQueue.scala 207:22]
  wire [3:0] ingControlCoreIn_sink_io_async_mem_0_teSyncMaxBTM; // @[AsyncQueue.scala 207:22]
  wire  ingControlCoreIn_sink_io_async_ridx; // @[AsyncQueue.scala 207:22]
  wire  ingControlCoreIn_sink_io_async_widx; // @[AsyncQueue.scala 207:22]
  wire  fifo_rf_reset; // @[TraceInBlock.scala 507:22]
  wire  fifo_clock; // @[TraceInBlock.scala 507:22]
  wire  fifo_reset; // @[TraceInBlock.scala 507:22]
  wire  fifo_io_enq_0_ready; // @[TraceInBlock.scala 507:22]
  wire  fifo_io_enq_0_valid; // @[TraceInBlock.scala 507:22]
  wire [4:0] fifo_io_enq_0_bits_reason; // @[TraceInBlock.scala 507:22]
  wire [38:0] fifo_io_enq_0_bits_iaddr; // @[TraceInBlock.scala 507:22]
  wire [13:0] fifo_io_enq_0_bits_icnt; // @[TraceInBlock.scala 507:22]
  wire [31:0] fifo_io_enq_0_bits_hist; // @[TraceInBlock.scala 507:22]
  wire [1:0] fifo_io_enq_0_bits_hisv; // @[TraceInBlock.scala 507:22]
  wire  fifo_io_enq_1_ready; // @[TraceInBlock.scala 507:22]
  wire  fifo_io_enq_1_valid; // @[TraceInBlock.scala 507:22]
  wire [4:0] fifo_io_enq_1_bits_reason; // @[TraceInBlock.scala 507:22]
  wire [38:0] fifo_io_enq_1_bits_iaddr; // @[TraceInBlock.scala 507:22]
  wire [13:0] fifo_io_enq_1_bits_icnt; // @[TraceInBlock.scala 507:22]
  wire [31:0] fifo_io_enq_1_bits_hist; // @[TraceInBlock.scala 507:22]
  wire [1:0] fifo_io_enq_1_bits_hisv; // @[TraceInBlock.scala 507:22]
  wire  fifo_io_deq_0_ready; // @[TraceInBlock.scala 507:22]
  wire  fifo_io_deq_0_valid; // @[TraceInBlock.scala 507:22]
  wire [4:0] fifo_io_deq_0_bits_reason; // @[TraceInBlock.scala 507:22]
  wire [38:0] fifo_io_deq_0_bits_iaddr; // @[TraceInBlock.scala 507:22]
  wire [13:0] fifo_io_deq_0_bits_icnt; // @[TraceInBlock.scala 507:22]
  wire [31:0] fifo_io_deq_0_bits_hist; // @[TraceInBlock.scala 507:22]
  wire [1:0] fifo_io_deq_0_bits_hisv; // @[TraceInBlock.scala 507:22]
  wire  inner_io_btmEventRational_source_rf_reset; // @[RationalCrossing.scala 158:24]
  wire  inner_io_btmEventRational_source_clock; // @[RationalCrossing.scala 158:24]
  wire  inner_io_btmEventRational_source_reset; // @[RationalCrossing.scala 158:24]
  wire  inner_io_btmEventRational_source_io_enq_ready; // @[RationalCrossing.scala 158:24]
  wire  inner_io_btmEventRational_source_io_enq_valid; // @[RationalCrossing.scala 158:24]
  wire [4:0] inner_io_btmEventRational_source_io_enq_bits_reason; // @[RationalCrossing.scala 158:24]
  wire [38:0] inner_io_btmEventRational_source_io_enq_bits_iaddr; // @[RationalCrossing.scala 158:24]
  wire [13:0] inner_io_btmEventRational_source_io_enq_bits_icnt; // @[RationalCrossing.scala 158:24]
  wire [31:0] inner_io_btmEventRational_source_io_enq_bits_hist; // @[RationalCrossing.scala 158:24]
  wire [1:0] inner_io_btmEventRational_source_io_enq_bits_hisv; // @[RationalCrossing.scala 158:24]
  wire [4:0] inner_io_btmEventRational_source_io_deq_bits0_reason; // @[RationalCrossing.scala 158:24]
  wire [38:0] inner_io_btmEventRational_source_io_deq_bits0_iaddr; // @[RationalCrossing.scala 158:24]
  wire [13:0] inner_io_btmEventRational_source_io_deq_bits0_icnt; // @[RationalCrossing.scala 158:24]
  wire [31:0] inner_io_btmEventRational_source_io_deq_bits0_hist; // @[RationalCrossing.scala 158:24]
  wire [1:0] inner_io_btmEventRational_source_io_deq_bits0_hisv; // @[RationalCrossing.scala 158:24]
  wire [4:0] inner_io_btmEventRational_source_io_deq_bits1_reason; // @[RationalCrossing.scala 158:24]
  wire [38:0] inner_io_btmEventRational_source_io_deq_bits1_iaddr; // @[RationalCrossing.scala 158:24]
  wire [13:0] inner_io_btmEventRational_source_io_deq_bits1_icnt; // @[RationalCrossing.scala 158:24]
  wire [31:0] inner_io_btmEventRational_source_io_deq_bits1_hist; // @[RationalCrossing.scala 158:24]
  wire [1:0] inner_io_btmEventRational_source_io_deq_bits1_hisv; // @[RationalCrossing.scala 158:24]
  wire  inner_io_btmEventRational_source_io_deq_valid; // @[RationalCrossing.scala 158:24]
  wire [1:0] inner_io_btmEventRational_source_io_deq_source; // @[RationalCrossing.scala 158:24]
  wire  inner_io_btmEventRational_source_io_deq_ready; // @[RationalCrossing.scala 158:24]
  wire [1:0] inner_io_btmEventRational_source_io_deq_sink; // @[RationalCrossing.scala 158:24]
  wire  inner_io_pcCaptureAsync_source_rf_reset; // @[AsyncQueue.scala 216:24]
  wire  inner_io_pcCaptureAsync_source_clock; // @[AsyncQueue.scala 216:24]
  wire  inner_io_pcCaptureAsync_source_reset; // @[AsyncQueue.scala 216:24]
  wire  inner_io_pcCaptureAsync_source_io_enq_ready; // @[AsyncQueue.scala 216:24]
  wire  inner_io_pcCaptureAsync_source_io_enq_valid; // @[AsyncQueue.scala 216:24]
  wire [38:0] inner_io_pcCaptureAsync_source_io_enq_bits; // @[AsyncQueue.scala 216:24]
  wire [38:0] inner_io_pcCaptureAsync_source_io_async_mem_0; // @[AsyncQueue.scala 216:24]
  wire  inner_io_pcCaptureAsync_source_io_async_ridx; // @[AsyncQueue.scala 216:24]
  wire  inner_io_pcCaptureAsync_source_io_async_widx; // @[AsyncQueue.scala 216:24]
  wire  inner_io_pcSampleAsync_source_rf_reset; // @[AsyncQueue.scala 216:24]
  wire  inner_io_pcSampleAsync_source_clock; // @[AsyncQueue.scala 216:24]
  wire  inner_io_pcSampleAsync_source_reset; // @[AsyncQueue.scala 216:24]
  wire  inner_io_pcSampleAsync_source_io_enq_ready; // @[AsyncQueue.scala 216:24]
  wire  inner_io_pcSampleAsync_source_io_enq_valid; // @[AsyncQueue.scala 216:24]
  wire [38:0] inner_io_pcSampleAsync_source_io_enq_bits; // @[AsyncQueue.scala 216:24]
  wire [38:0] inner_io_pcSampleAsync_source_io_async_mem_0; // @[AsyncQueue.scala 216:24]
  wire  inner_io_pcSampleAsync_source_io_async_ridx; // @[AsyncQueue.scala 216:24]
  wire  inner_io_pcSampleAsync_source_io_async_widx; // @[AsyncQueue.scala 216:24]
  reg [2:0] ingress_ingressPriv; // @[Reg.scala 15:16]
  reg  ingress_ingressValid; // @[TraceInBlock.scala 51:40]
  reg [31:0] ingress_ingressInsn; // @[Reg.scala 15:16]
  reg [39:0] ingress_ingressIaddr; // @[Reg.scala 15:16]
  reg  ingress_ingressException; // @[Reg.scala 15:16]
  reg  ingress_ingressInterrupt; // @[Reg.scala 15:16]
  wire  ingress_group_0_ilastsize = &ingress_ingressInsn[1:0]; // @[TraceInBlock.scala 58:45]
  wire [1:0] _ingress_tci_group_0_itype_T = ingress_ingressInterrupt ? 2'h2 : 2'h1; // @[TraceInBlock.scala 62:35]
  wire [1:0] _ingress_tci_group_0_iretire_T_2 = ingress_group_0_ilastsize ? 2'h2 : 2'h1; // @[TraceInBlock.scala 64:35]
  wire [6:0] ingress_tci_group_0_itype_op = ingress_ingressInsn[6:0]; // @[TraceInXlt.scala 14:18]
  wire [4:0] ingress_tci_group_0_itype_rd = ingress_ingressInsn[11:7]; // @[TraceInXlt.scala 15:18]
  wire [2:0] ingress_tci_group_0_itype_f3 = ingress_ingressInsn[14:12]; // @[TraceInXlt.scala 16:18]
  wire [4:0] ingress_tci_group_0_itype_rs = ingress_ingressInsn[19:15]; // @[TraceInXlt.scala 17:18]
  wire [2:0] ingress_tci_group_0_itype_c5_hi = ingress_ingressInsn[15:13]; // @[TraceInXlt.scala 18:22]
  wire [4:0] ingress_tci_group_0_itype_c5 = {ingress_tci_group_0_itype_c5_hi,ingress_ingressInsn[1:0]}; // @[Cat.scala 30:58]
  wire [4:0] _ingress_tci_group_0_itype_rdlink_T = ingress_tci_group_0_itype_rd & 5'h1b; // @[TraceInXlt.scala 19:23]
  wire  ingress_tci_group_0_itype_rdlink = _ingress_tci_group_0_itype_rdlink_T == 5'h1; // @[TraceInXlt.scala 19:33]
  wire [4:0] _ingress_tci_group_0_itype_rslink_T = ingress_tci_group_0_itype_rs & 5'h1b; // @[TraceInXlt.scala 20:23]
  wire  ingress_tci_group_0_itype_rslink = _ingress_tci_group_0_itype_rslink_T == 5'h1; // @[TraceInXlt.scala 20:33]
  wire  ingress_tci_group_0_itype_jal = ingress_tci_group_0_itype_op == 7'h6f; // @[TraceInXlt.scala 21:21]
  wire  ingress_tci_group_0_itype_jalr = ingress_tci_group_0_itype_op == 7'h67 & ingress_tci_group_0_itype_f3 == 3'h0; // @[TraceInXlt.scala 22:33]
  wire  ingress_tci_group_0_itype_br = ingress_tci_group_0_itype_op == 7'h63 & ingress_tci_group_0_itype_f3 != 3'h2 &
    ingress_tci_group_0_itype_f3 != 3'h3; // @[TraceInXlt.scala 23:49]
  wire [31:0] _ingress_tci_group_0_itype_xret_T = ingress_ingressInsn & 32'h8fffffff; // @[TraceInXlt.scala 24:24]
  wire  ingress_tci_group_0_itype_xret = _ingress_tci_group_0_itype_xret_T == 32'h200073; // @[TraceInXlt.scala 24:41]
  wire  ingress_tci_group_0_itype_cj = ingress_tci_group_0_itype_c5 == 5'h15; // @[TraceInXlt.scala 26:21]
  wire  ingress_tci_group_0_itype_cbr = ingress_tci_group_0_itype_c5 == 5'h19 | ingress_tci_group_0_itype_c5 == 5'h1d; // @[TraceInXlt.scala 27:33]
  wire  _ingress_tci_group_0_itype_cjr_T = ingress_tci_group_0_itype_c5 == 5'h12; // @[TraceInXlt.scala 28:21]
  wire  _ingress_tci_group_0_itype_cjr_T_5 = |ingress_tci_group_0_itype_rd; // @[TraceInXlt.scala 28:60]
  wire  _ingress_tci_group_0_itype_cjr_T_9 = ~(|ingress_ingressInsn[6:2]); // @[TraceInXlt.scala 28:67]
  wire  ingress_tci_group_0_itype_cjr = ingress_tci_group_0_itype_c5 == 5'h12 & ~ingress_ingressInsn[12] & |
    ingress_tci_group_0_itype_rd & ~(|ingress_ingressInsn[6:2]); // @[TraceInXlt.scala 28:64]
  wire  ingress_tci_group_0_itype_cjalr = _ingress_tci_group_0_itype_cjr_T & ingress_ingressInsn[12] &
    _ingress_tci_group_0_itype_cjr_T_5 & _ingress_tci_group_0_itype_cjr_T_9; // @[TraceInXlt.scala 29:64]
  wire  _ingress_tci_group_0_itype_T_1 = ingress_tci_group_0_itype_jal & ingress_tci_group_0_itype_rdlink; // @[TraceInXlt.scala 31:13]
  wire  _ingress_tci_group_0_itype_T_2 = ingress_tci_group_0_itype_jalr & ingress_tci_group_0_itype_rdlink; // @[TraceInXlt.scala 33:13]
  wire  _ingress_tci_group_0_itype_T_3 = ingress_tci_group_0_itype_jalr & ingress_tci_group_0_itype_rdlink &
    ingress_tci_group_0_itype_rslink; // @[TraceInXlt.scala 33:23]
  wire  _ingress_tci_group_0_itype_T_5 = ingress_tci_group_0_itype_jalr & ingress_tci_group_0_itype_rdlink &
    ingress_tci_group_0_itype_rslink & ingress_tci_group_0_itype_rd == ingress_tci_group_0_itype_rs; // @[TraceInXlt.scala 33:33]
  wire  _ingress_tci_group_0_itype_T_9 = ingress_tci_group_0_itype_jalr & ingress_tci_group_0_itype_rslink; // @[TraceInXlt.scala 36:13]
  wire  _ingress_tci_group_0_itype_T_10 = ingress_tci_group_0_itype_br | ingress_tci_group_0_itype_cbr; // @[TraceInXlt.scala 38:11]
  wire  _ingress_tci_group_0_itype_T_11 = ingress_tci_group_0_itype_cjr & ingress_tci_group_0_itype_rdlink; // @[TraceInXlt.scala 41:12]
  wire  _ingress_tci_group_0_itype_T_13 = ingress_tci_group_0_itype_cjalr & ingress_tci_group_0_itype_rd == 5'h5; // @[TraceInXlt.scala 43:14]
  wire [3:0] _ingress_tci_group_0_itype_T_14 = ingress_tci_group_0_itype_cjalr ? 4'h8 : 4'h0; // @[Mux.scala 98:16]
  wire [3:0] _ingress_tci_group_0_itype_T_15 = _ingress_tci_group_0_itype_T_13 ? 4'hc : _ingress_tci_group_0_itype_T_14; // @[Mux.scala 98:16]
  wire [3:0] _ingress_tci_group_0_itype_T_16 = ingress_tci_group_0_itype_cjr ? 4'he : _ingress_tci_group_0_itype_T_15; // @[Mux.scala 98:16]
  wire [3:0] _ingress_tci_group_0_itype_T_17 = _ingress_tci_group_0_itype_T_11 ? 4'hd : _ingress_tci_group_0_itype_T_16; // @[Mux.scala 98:16]
  wire [3:0] _ingress_tci_group_0_itype_T_18 = ingress_tci_group_0_itype_cj ? 4'hf : _ingress_tci_group_0_itype_T_17; // @[Mux.scala 98:16]
  wire [3:0] _ingress_tci_group_0_itype_T_19 = ingress_tci_group_0_itype_xret ? 4'h3 : _ingress_tci_group_0_itype_T_18; // @[Mux.scala 98:16]
  wire [3:0] _ingress_tci_group_0_itype_T_20 = _ingress_tci_group_0_itype_T_10 ? 4'h7 : _ingress_tci_group_0_itype_T_19; // @[Mux.scala 98:16]
  wire [3:0] _ingress_tci_group_0_itype_T_21 = ingress_tci_group_0_itype_jalr ? 4'he : _ingress_tci_group_0_itype_T_20; // @[Mux.scala 98:16]
  wire [3:0] _ingress_tci_group_0_itype_T_22 = _ingress_tci_group_0_itype_T_9 ? 4'hd : _ingress_tci_group_0_itype_T_21; // @[Mux.scala 98:16]
  wire [3:0] _ingress_tci_group_0_itype_T_23 = _ingress_tci_group_0_itype_T_2 ? 4'h8 : _ingress_tci_group_0_itype_T_22; // @[Mux.scala 98:16]
  wire [3:0] _ingress_tci_group_0_itype_T_24 = _ingress_tci_group_0_itype_T_3 ? 4'hc : _ingress_tci_group_0_itype_T_23; // @[Mux.scala 98:16]
  wire [3:0] _ingress_tci_group_0_itype_T_25 = _ingress_tci_group_0_itype_T_5 ? 4'h8 : _ingress_tci_group_0_itype_T_24; // @[Mux.scala 98:16]
  wire [3:0] _ingress_tci_group_0_itype_T_26 = ingress_tci_group_0_itype_jal ? 4'hf : _ingress_tci_group_0_itype_T_25; // @[Mux.scala 98:16]
  wire [3:0] _ingress_tci_group_0_itype_T_27 = _ingress_tci_group_0_itype_T_1 ? 4'h9 : _ingress_tci_group_0_itype_T_26; // @[Mux.scala 98:16]
  wire [1:0] _GEN_5 = ingress_ingressException ? 2'h0 : _ingress_tci_group_0_iretire_T_2; // @[TraceInBlock.scala 60:35 TraceInBlock.scala 61:29 TraceInBlock.scala 64:29]
  wire [3:0] _GEN_6 = ingress_ingressException ? {{2'd0}, _ingress_tci_group_0_itype_T} :
    _ingress_tci_group_0_itype_T_27; // @[TraceInBlock.scala 60:35 TraceInBlock.scala 62:29 TraceInBlock.scala 65:29]
  wire [1:0] ingress_group_0_iretire = ingress_ingressValid ? _GEN_5 : 2'h0; // @[TraceInBlock.scala 59:29 TraceInBlock.scala 68:27]
  wire [3:0] ingress_group_0_itype = ingress_ingressValid ? _GEN_6 : 4'h0; // @[TraceInBlock.scala 59:29 TraceInBlock.scala 69:27]
  reg  teActiveCore_teActiveCore; // @[TraceInBlock.scala 129:30]
  reg  pcActiveCore_pcActiveCore; // @[TraceInBlock.scala 130:30]
  reg  teIdleCore_teIdleCore; // @[TraceInBlock.scala 131:30]
  reg  ingControlCoreReg_teEnable; // @[TraceInBlock.scala 148:38]
  reg  ingControlCoreReg_teTracing; // @[TraceInBlock.scala 148:38]
  reg [2:0] ingControlCoreReg_teInstruction; // @[TraceInBlock.scala 148:38]
  reg  ingControlCoreReg_teStallEnable; // @[TraceInBlock.scala 148:38]
  reg [3:0] ingControlCoreReg_teSyncMaxInst; // @[TraceInBlock.scala 148:38]
  reg [3:0] ingControlCoreReg_teSyncMaxBTM; // @[TraceInBlock.scala 148:38]
  wire  _T = ~teActiveCore_teActiveCore; // @[TraceInBlock.scala 149:13]
  wire  ingControlCoreIn_valid = ingControlCoreIn_sink_io_deq_valid; // @[TraceInBlock.scala 145:34 TraceInBlock.scala 146:24]
  wire [3:0] ingControlCoreIn_bits_teSyncMaxBTM = ingControlCoreIn_sink_io_deq_bits_teSyncMaxBTM; // @[TraceInBlock.scala 145:34 TraceInBlock.scala 146:24]
  wire [3:0] ingControlCoreIn_bits_teSyncMaxInst = ingControlCoreIn_sink_io_deq_bits_teSyncMaxInst; // @[TraceInBlock.scala 145:34 TraceInBlock.scala 146:24]
  wire  ingControlCoreIn_bits_teStallEnable = ingControlCoreIn_sink_io_deq_bits_teStallEnable; // @[TraceInBlock.scala 145:34 TraceInBlock.scala 146:24]
  wire [2:0] ingControlCoreIn_bits_teInstruction = ingControlCoreIn_sink_io_deq_bits_teInstruction; // @[TraceInBlock.scala 145:34 TraceInBlock.scala 146:24]
  wire  ingControlCoreIn_bits_teTracing = ingControlCoreIn_sink_io_deq_bits_teTracing; // @[TraceInBlock.scala 145:34 TraceInBlock.scala 146:24]
  wire  ingControlCoreIn_bits_teEnable = ingControlCoreIn_sink_io_deq_bits_teEnable; // @[TraceInBlock.scala 145:34 TraceInBlock.scala 146:24]
  reg  ingControlCore_valid_REG; // @[TraceInBlock.scala 154:38]
  reg  inReset_r; // @[Reg.scala 15:16]
  reg  inReset_r_1; // @[Reg.scala 15:16]
  reg  inReset; // @[Reg.scala 15:16]
  wire  rawRetire_0 = |ingress_group_0_iretire; // @[TraceInBlock.scala 167:54]
  wire  rawValid_0 = rawRetire_0 | ingress_group_0_itype != 4'h0; // @[TraceInBlock.scala 168:58]
  reg  iValid0; // @[TraceInBlock.scala 169:28]
  reg [1:0] inGroupReg_0_iretire; // @[Reg.scala 15:16]
  reg [39:0] inGroupReg_0_iaddr; // @[Reg.scala 15:16]
  reg [3:0] inGroupReg_0_itype; // @[Reg.scala 15:16]
  reg  inGroupReg_0_ilastsize; // @[Reg.scala 15:16]
  wire [3:0] ingress_priv = {{1'd0}, ingress_ingressPriv}; // @[TraceInBlock.scala 43:21 TraceInBlock.scala 46:16]
  reg  inDebug; // @[Reg.scala 15:16]
  reg  inRetire_0; // @[TraceInBlock.scala 172:28]
  wire [38:0] _GEN_362 = {{37'd0}, ingress_group_0_iretire}; // @[TraceInBlock.scala 174:53]
  wire [38:0] paddr_0 = ingress_ingressIaddr[39:1] + _GEN_362; // @[TraceInBlock.scala 174:53]
  reg [38:0] inGroupPA_0; // @[Reg.scala 15:16]
  wire [1:0] _lastExecuted_T_1 = inGroupReg_0_ilastsize ? 2'h2 : 2'h1; // @[TraceInBlock.scala 191:79]
  wire [1:0] _lastExecuted_T_3 = inGroupReg_0_iretire - _lastExecuted_T_1; // @[TraceInBlock.scala 191:74]
  wire [39:0] inGroup_0_iaddr = {{1'd0}, inGroupReg_0_iaddr[39:1]};
  wire [39:0] _GEN_374 = {{38'd0}, _lastExecuted_T_3}; // @[TraceInBlock.scala 191:45]
  wire [39:0] lastExecuted = inGroup_0_iaddr + _GEN_374; // @[TraceInBlock.scala 191:45]
  reg  ingressIaddrValid; // @[TraceInBlock.scala 192:36]
  reg [39:0] pcSampleCoreReg; // @[Reg.scala 15:16]
  reg  pcSampleCoreValid; // @[TraceInBlock.scala 195:36]
  wire  _pcCaptureCoreReg_T = inReset & ingressIaddrValid; // @[TraceInBlock.scala 198:63]
  reg [39:0] pcCaptureCoreReg; // @[Reg.scala 15:16]
  reg  pcCaptureCoreValid; // @[TraceInBlock.scala 199:37]
  reg  btmEvent_0_valid; // @[TraceInBlock.scala 221:27]
  reg [4:0] btmEvent_0_bits_reason; // @[TraceInBlock.scala 221:27]
  reg [38:0] btmEvent_0_bits_iaddr; // @[TraceInBlock.scala 221:27]
  reg [13:0] btmEvent_0_bits_icnt; // @[TraceInBlock.scala 221:27]
  reg [31:0] btmEvent_0_bits_hist; // @[TraceInBlock.scala 221:27]
  reg [1:0] btmEvent_0_bits_hisv; // @[TraceInBlock.scala 221:27]
  reg  btmEvent_1_valid; // @[TraceInBlock.scala 221:27]
  reg [4:0] btmEvent_1_bits_reason; // @[TraceInBlock.scala 221:27]
  reg [38:0] btmEvent_1_bits_iaddr; // @[TraceInBlock.scala 221:27]
  reg [13:0] btmEvent_1_bits_icnt; // @[TraceInBlock.scala 221:27]
  reg [31:0] btmEvent_1_bits_hist; // @[TraceInBlock.scala 221:27]
  reg [1:0] btmEvent_1_bits_hisv; // @[TraceInBlock.scala 221:27]
  reg [3:0] lastCycle_itype; // @[TraceInBlock.scala 226:27]
  reg [38:0] lastCycle_iaddr; // @[TraceInBlock.scala 226:27]
  reg [13:0] lastCycle_icnt; // @[TraceInBlock.scala 226:27]
  reg [31:0] lastCycle_hist; // @[TraceInBlock.scala 226:27]
  reg [1:0] lastCycle_hisv; // @[TraceInBlock.scala 226:27]
  reg [12:0] lastCycle_hcnt; // @[TraceInBlock.scala 226:27]
  reg [15:0] lastCycle_ras; // @[TraceInBlock.scala 226:27]
  reg [4:0] lastCycle_gapReason; // @[TraceInBlock.scala 226:27]
  reg  lastCycle_tracing; // @[TraceInBlock.scala 226:27]
  reg  lastCycle_wpsync; // @[TraceInBlock.scala 226:27]
  reg [1:0] ovfState; // @[TraceInBlock.scala 230:27]
  reg [16:0] btmCount; // @[TraceInBlock.scala 231:27]
  reg  btmCountFull; // @[TraceInBlock.scala 233:27]
  wire  chicken = ~ingControlCoreReg_teInstruction[0]; // @[TraceInBlock.scala 236:33]
  wire  syncOnly = ingControlCoreReg_teInstruction == 3'h1; // @[TraceInBlock.scala 237:68]
  wire  probeton = ingControlCore_valid_REG & ingControlCoreReg_teEnable & ingControlCoreReg_teTracing; // @[TraceInBlock.scala 238:87]
  wire  _probetoff_T_1 = ingControlCoreReg_teEnable & ingControlCoreReg_teTracing; // @[TraceInBlock.scala 239:89]
  wire  probetoff = ingControlCore_valid_REG & ~(ingControlCoreReg_teEnable & ingControlCoreReg_teTracing); // @[TraceInBlock.scala 239:55]
  wire [3:0] _maxicnt_T_1 = ingControlCoreReg_teSyncMaxInst + 4'h4; // @[TraceInBlock.scala 240:88]
  wire  _T_6 = ~ingControlCoreReg_teEnable; // @[TraceInBlock.scala 300:11]
  wire  sendingGap = ~lastCycle_gapReason[4]; // @[TraceInBlock.scala 404:42]
  wire [3:0] stateA_0_itype = sendingGap ? 4'h0 : lastCycle_itype; // @[TraceInBlock.scala 257:28 TraceInBlock.scala 257:46 TraceInBlock.scala 252:17]
  wire  _isbranch_T_1 = stateA_0_itype == 4'h5; // @[TraceInBlock.scala 374:59]
  wire  _isbranch_T_3 = stateA_0_itype == 4'h7; // @[TraceInBlock.scala 374:79]
  wire  isbranch = stateA_0_itype == 4'h4 | stateA_0_itype == 4'h5 | stateA_0_itype == 4'h7; // @[TraceInBlock.scala 374:73]
  wire  _sendingDecoupledSync_T = ~isbranch; // @[TraceInBlock.scala 405:58]
  wire  sendingDecoupledSync = btmCountFull & ~isbranch; // @[TraceInBlock.scala 405:55]
  wire  _sendingIcnt_T = sendingGap | sendingDecoupledSync; // @[TraceInBlock.scala 407:53]
  wire  isexception = stateA_0_itype == 4'h1 | stateA_0_itype == 4'h2; // @[TraceInBlock.scala 376:58]
  wire  stateA_0_wpsync = lastCycle_wpsync | io_ingTrigger_0_wpsync; // @[TraceInBlock.scala 253:80]
  wire  istrig = _sendingDecoupledSync_T & stateA_0_wpsync; // @[TraceInBlock.scala 380:43]
  wire  _isuninf_T_1 = stateA_0_itype == 4'h8; // @[TraceInBlock.scala 378:60]
  wire  isreturn = stateA_0_itype == 4'hd; // @[TraceInBlock.scala 377:37]
  wire  _isuninf_T_7 = |lastCycle_ras; // @[TraceInBlock.scala 379:64]
  wire  _isuninf_T_9 = (stateA_0_itype == 4'hc | isreturn) & ~(|lastCycle_ras); // @[TraceInBlock.scala 379:46]
  wire  isuninf = stateA_0_itype == 4'h3 | stateA_0_itype == 4'h8 | stateA_0_itype == 4'he | _isuninf_T_9; // @[TraceInBlock.scala 378:92]
  wire  sendingIcnt = sendingGap | sendingDecoupledSync | isexception | istrig | isuninf; // @[TraceInBlock.scala 407:102]
  wire [13:0] icntM = sendingIcnt ? 14'h0 : lastCycle_icnt; // @[TraceInBlock.scala 422:30]
  wire [13:0] _GEN_375 = {{12'd0}, inGroupReg_0_iretire}; // @[TraceInBlock.scala 427:39]
  wire [13:0] _stateZ_0_icnt_T_1 = icntM + _GEN_375; // @[TraceInBlock.scala 427:39]
  wire [13:0] _GEN_69 = iValid0 ? _stateZ_0_icnt_T_1 : lastCycle_icnt; // @[TraceInBlock.scala 395:45 TraceInBlock.scala 427:30 TraceInBlock.scala 259:17]
  wire [13:0] _GEN_83 = ~lastCycle_tracing & ~io_ingTrigger_0_traceon ? lastCycle_icnt : _GEN_69; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 259:17]
  wire [13:0] _GEN_144 = inDebug ? lastCycle_icnt : _GEN_83; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 259:17]
  wire [13:0] _GEN_181 = inReset ? lastCycle_icnt : _GEN_144; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 259:17]
  wire [13:0] _GEN_218 = syncOnly ? lastCycle_icnt : _GEN_181; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire [13:0] _GEN_255 = ovfState[1] ? lastCycle_icnt : _GEN_218; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 259:17]
  wire [13:0] _GEN_292 = ovfState[0] ? lastCycle_icnt : _GEN_255; // @[TraceInBlock.scala 308:26 TraceInBlock.scala 259:17]
  wire [13:0] stateZ_0_icnt = _T_6 ? lastCycle_icnt : _GEN_292; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 259:17]
  wire [13:0] _maxicnt_T_2 = stateZ_0_icnt >> _maxicnt_T_1; // @[TraceInBlock.scala 240:53]
  wire  maxicnt = _maxicnt_T_2[0]; // @[TraceInBlock.scala 240:53]
  wire  icntFull = ingControlCoreReg_teSyncMaxInst == 4'ha ? &stateZ_0_icnt[13:10] : maxicnt; // @[TraceInBlock.scala 245:20]
  wire  _stateA_0_useHcnt_T = ~chicken; // @[TraceInBlock.scala 255:28]
  wire  stateA_0_useHcnt = ~chicken & lastCycle_hisv[1] & |lastCycle_hcnt[12:4]; // @[TraceInBlock.scala 255:58]
  wire [39:0] _GEN_380 = {{1'd0}, lastCycle_iaddr}; // @[TraceInBlock.scala 397:77]
  wire  _taken_T_3 = _isbranch_T_3 & _GEN_380 != inGroup_0_iaddr; // @[TraceInBlock.scala 397:57]
  wire  stateZ_0_hist_lo = _isbranch_T_1 | _taken_T_3; // @[TraceInBlock.scala 396:55]
  wire  _histNeedsSending_T_8 = isbranch & stateA_0_useHcnt & lastCycle_hisv[0] != stateZ_0_hist_lo; // @[TraceInBlock.scala 401:48]
  wire  histNeedsSending = isbranch & ~lastCycle_hisv[1] & lastCycle_hist[31] | _histNeedsSending_T_8; // @[TraceInBlock.scala 400:78]
  wire  sendingHist = sendingIcnt | histNeedsSending; // @[TraceInBlock.scala 408:113]
  wire [12:0] hcntM = sendingHist ? 13'h0 : lastCycle_hcnt; // @[TraceInBlock.scala 425:30]
  wire [12:0] _stateZ_0_hcnt_T_1 = hcntM + 13'h1; // @[TraceInBlock.scala 429:53]
  wire [12:0] _stateZ_0_hcnt_T_2 = isbranch ? _stateZ_0_hcnt_T_1 : hcntM; // @[TraceInBlock.scala 429:36]
  wire [12:0] _GEN_71 = iValid0 ? _stateZ_0_hcnt_T_2 : lastCycle_hcnt; // @[TraceInBlock.scala 395:45 TraceInBlock.scala 429:30 TraceInBlock.scala 259:17]
  wire [12:0] _GEN_85 = ~lastCycle_tracing & ~io_ingTrigger_0_traceon ? lastCycle_hcnt : _GEN_71; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 259:17]
  wire [12:0] _GEN_146 = inDebug ? lastCycle_hcnt : _GEN_85; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 259:17]
  wire [12:0] _GEN_183 = inReset ? lastCycle_hcnt : _GEN_146; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 259:17]
  wire [12:0] _GEN_220 = syncOnly ? lastCycle_hcnt : _GEN_183; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire [12:0] _GEN_257 = ovfState[1] ? lastCycle_hcnt : _GEN_220; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 259:17]
  wire [12:0] _GEN_294 = ovfState[0] ? lastCycle_hcnt : _GEN_257; // @[TraceInBlock.scala 308:26 TraceInBlock.scala 259:17]
  wire [12:0] stateZ_0_hcnt = _T_6 ? lastCycle_hcnt : _GEN_294; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 259:17]
  wire  hcntFull = &stateZ_0_hcnt; // @[TraceInBlock.scala 248:72]
  wire  _GEN_76 = iValid0 ? stateA_0_wpsync & isexception : stateA_0_wpsync; // @[TraceInBlock.scala 395:45 TraceInBlock.scala 442:32 TraceInBlock.scala 259:17]
  wire  _GEN_90 = ~lastCycle_tracing & ~io_ingTrigger_0_traceon ? stateA_0_wpsync : _GEN_76; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 259:17]
  wire  _GEN_151 = inDebug ? stateA_0_wpsync : _GEN_90; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 259:17]
  wire  _GEN_188 = inReset ? stateA_0_wpsync : _GEN_151; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 259:17]
  wire  _GEN_225 = syncOnly ? stateA_0_wpsync : _GEN_188; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire  _GEN_262 = ovfState[1] ? stateA_0_wpsync : _GEN_225; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 259:17]
  wire  _GEN_299 = ovfState[0] ? stateA_0_wpsync : _GEN_262; // @[TraceInBlock.scala 308:26 TraceInBlock.scala 259:17]
  wire  stateZ_0_wpsync = _T_6 ? stateA_0_wpsync : _GEN_299; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 259:17]
  wire [1:0] hisvM = sendingHist ? 2'h0 : lastCycle_hisv; // @[TraceInBlock.scala 424:30]
  wire  stateZ_0_hisv_hi = ~(^hisvM); // @[TraceInBlock.scala 432:21]
  wire [1:0] _stateZ_0_hisv_T_1 = {stateZ_0_hisv_hi,1'h1}; // @[Cat.scala 30:58]
  wire  stateZ_0_hisv_hi_1 = ~hisvM[0]; // @[TraceInBlock.scala 433:21]
  wire [1:0] _stateZ_0_hisv_T_3 = {stateZ_0_hisv_hi_1,hisvM[0]}; // @[Cat.scala 30:58]
  wire [1:0] _stateZ_0_hisv_T_4 = stateZ_0_hist_lo ? _stateZ_0_hisv_T_1 : _stateZ_0_hisv_T_3; // @[TraceInBlock.scala 431:20]
  wire [1:0] _stateZ_0_hisv_T_5 = chicken ? 2'h1 : _stateZ_0_hisv_T_4; // @[TraceInBlock.scala 430:50]
  wire [1:0] _stateZ_0_hisv_T_6 = isbranch ? _stateZ_0_hisv_T_5 : hisvM; // @[TraceInBlock.scala 430:36]
  wire [1:0] _GEN_72 = iValid0 ? _stateZ_0_hisv_T_6 : lastCycle_hisv; // @[TraceInBlock.scala 395:45 TraceInBlock.scala 430:30 TraceInBlock.scala 259:17]
  wire [1:0] _GEN_86 = ~lastCycle_tracing & ~io_ingTrigger_0_traceon ? lastCycle_hisv : _GEN_72; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 259:17]
  wire [1:0] _GEN_147 = inDebug ? lastCycle_hisv : _GEN_86; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 259:17]
  wire [1:0] _GEN_184 = inReset ? lastCycle_hisv : _GEN_147; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 259:17]
  wire [1:0] _GEN_221 = syncOnly ? lastCycle_hisv : _GEN_184; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire [1:0] _GEN_258 = ovfState[1] ? lastCycle_hisv : _GEN_221; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 259:17]
  wire [1:0] _GEN_295 = ovfState[0] ? lastCycle_hisv : _GEN_258; // @[TraceInBlock.scala 308:26 TraceInBlock.scala 259:17]
  wire [1:0] stateZ_0_hisv = _T_6 ? lastCycle_hisv : _GEN_295; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 259:17]
  wire  stateA_1_useHcnt = ~chicken & stateZ_0_hisv[1] & |stateZ_0_hcnt[12:4]; // @[TraceInBlock.scala 255:58]
  wire [4:0] _GEN_78 = iValid0 ? 5'h1f : lastCycle_gapReason; // @[TraceInBlock.scala 395:45 TraceInBlock.scala 444:35 TraceInBlock.scala 259:17]
  wire [4:0] _GEN_81 = ~lastCycle_tracing & ~io_ingTrigger_0_traceon ? 5'h5 : _GEN_78; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 392:33]
  wire [4:0] _GEN_142 = inDebug ? lastCycle_gapReason : _GEN_81; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 259:17]
  wire [4:0] _GEN_179 = inReset ? lastCycle_gapReason : _GEN_142; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 259:17]
  wire [4:0] _GEN_216 = syncOnly ? lastCycle_gapReason : _GEN_179; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire [4:0] _GEN_253 = ovfState[1] ? lastCycle_gapReason : _GEN_216; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 259:17]
  wire [4:0] _GEN_290 = ovfState[0] ? lastCycle_gapReason : _GEN_253; // @[TraceInBlock.scala 308:26 TraceInBlock.scala 259:17]
  wire [4:0] stateZ_0_gapReason = _T_6 ? lastCycle_gapReason : _GEN_290; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 259:17]
  wire  _GEN_58 = io_ingTrigger_0_traceon | lastCycle_tracing; // @[TraceInBlock.scala 385:23 TraceInBlock.scala 385:43 TraceInBlock.scala 259:17]
  wire  _GEN_59 = io_ingTrigger_0_traceoff ? 1'h0 : _GEN_58; // @[TraceInBlock.scala 386:23 TraceInBlock.scala 386:43]
  wire  _GEN_139 = inDebug ? lastCycle_tracing : _GEN_59; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 259:17]
  wire  _GEN_176 = inReset ? lastCycle_tracing : _GEN_139; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 259:17]
  wire  _GEN_213 = syncOnly ? lastCycle_tracing : _GEN_176; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire  _GEN_250 = ovfState[1] ? lastCycle_tracing : _GEN_213; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 259:17]
  wire  _GEN_287 = ovfState[0] ? lastCycle_tracing : _GEN_250; // @[TraceInBlock.scala 308:26 TraceInBlock.scala 259:17]
  wire  stateZ_0_tracing = _T_6 ? lastCycle_tracing : _GEN_287; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 259:17]
  wire  _T_35 = stateZ_0_wpsync & ingressIaddrValid & ~inRetire_0; // @[TraceInBlock.scala 455:87]
  wire  _GEN_99 = stateZ_0_wpsync & ingressIaddrValid & ~inRetire_0 ? 1'h0 : stateZ_0_wpsync; // @[TraceInBlock.scala 455:104 TraceInBlock.scala 458:34 TraceInBlock.scala 259:17]
  wire  _GEN_105 = icntFull ? stateZ_0_wpsync : _GEN_99; // @[TraceInBlock.scala 452:37 TraceInBlock.scala 259:17]
  wire  _GEN_114 = hcntFull ? stateZ_0_wpsync : _GEN_105; // @[TraceInBlock.scala 447:31 TraceInBlock.scala 259:17]
  wire  _GEN_124 = ~stateZ_0_tracing & ~probeton ? stateZ_0_wpsync : _GEN_114; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 259:17]
  wire  _GEN_161 = inDebug ? stateZ_0_wpsync : _GEN_124; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 259:17]
  wire  _GEN_198 = inReset ? stateZ_0_wpsync : _GEN_161; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 259:17]
  wire  _GEN_235 = syncOnly ? stateZ_0_wpsync : _GEN_198; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire  _GEN_272 = ovfState[1] ? stateZ_0_wpsync : _GEN_235; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 259:17]
  wire  _GEN_309 = ovfState[0] ? stateZ_0_wpsync : _GEN_272; // @[TraceInBlock.scala 308:26 TraceInBlock.scala 259:17]
  wire  stateZ_1_wpsync = _T_6 ? stateZ_0_wpsync : _GEN_309; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 259:17]
  wire  _GEN_38 = ingControlCore_valid_REG ? _probetoff_T_1 : stateZ_0_tracing; // @[TraceInBlock.scala 306:36 TraceInBlock.scala 306:62 TraceInBlock.scala 259:17]
  wire  _GEN_92 = probeton | _GEN_38; // @[TraceInBlock.scala 385:23 TraceInBlock.scala 385:43]
  wire  _GEN_93 = probetoff ? 1'h0 : _GEN_92; // @[TraceInBlock.scala 386:23 TraceInBlock.scala 386:43]
  wire  _GEN_153 = inDebug ? _GEN_38 : _GEN_93; // @[TraceInBlock.scala 333:28]
  wire  _GEN_190 = inReset ? _GEN_38 : _GEN_153; // @[TraceInBlock.scala 328:28]
  wire  _GEN_227 = syncOnly ? _GEN_38 : _GEN_190; // @[TraceInBlock.scala 313:29]
  wire  _GEN_264 = ovfState[1] ? _GEN_38 : _GEN_227; // @[TraceInBlock.scala 310:32]
  wire  _GEN_301 = ovfState[0] ? _GEN_38 : _GEN_264; // @[TraceInBlock.scala 308:26]
  wire  stateZ_1_tracing = _T_6 ? stateZ_0_tracing : _GEN_301; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 259:17]
  wire  _GEN_135 = inDebug ? 1'h0 : btmEvent_0_valid; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 297:16 TraceInBlock.scala 367:20]
  wire  _GEN_172 = inReset ? 1'h0 : _GEN_135; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 297:16]
  wire  _GEN_210 = syncOnly ? 1'h0 : _GEN_172; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 297:16]
  wire  _GEN_247 = ovfState[1] ? 1'h0 : _GEN_210; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 297:16]
  wire  _GEN_284 = ovfState[0] ? 1'h0 : _GEN_247; // @[TraceInBlock.scala 308:26 TraceInBlock.scala 297:16]
  wire  _GEN_313 = ingControlCoreReg_teInstruction == 3'h0 ? 1'h0 : _GEN_284; // @[TraceInBlock.scala 466:56 TraceInBlock.scala 468:40]
  wire  btmEventFifo_0_valid = _T_6 ? 1'h0 : _GEN_313; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 297:16]
  reg [13:0] scnt; // @[TraceInBlock.scala 314:23]
  wire [13:0] _maxscnt_T_2 = scnt >> _maxicnt_T_1; // @[TraceInBlock.scala 315:36]
  wire  maxscnt = _maxscnt_T_2[0]; // @[TraceInBlock.scala 315:36]
  reg [3:0] fifoCount; // @[TraceInBlock.scala 506:28]
  wire  traceStall = ingControlCoreReg_teStallEnable & fifoCount >= 4'h2; // @[TraceInBlock.scala 517:53]
  wire  _T_14 = ~traceStall; // @[TraceInBlock.scala 321:19]
  wire  _GEN_43 = maxscnt & _T_14; // @[TraceInBlock.scala 320:26 TraceInBlock.scala 297:16]
  wire  _GEN_48 = _T | inDebug | ~ingressIaddrValid ? 1'h0 : _GEN_43; // @[TraceInBlock.scala 317:63 TraceInBlock.scala 297:16]
  wire  _GEN_126 = inDebug ? stateZ_0_gapReason[4] : btmEvent_1_valid; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 367:20]
  wire  _GEN_163 = inReset ? stateZ_0_gapReason[4] : _GEN_126; // @[TraceInBlock.scala 328:28]
  wire  _GEN_201 = syncOnly ? _GEN_48 : _GEN_163; // @[TraceInBlock.scala 313:29]
  wire  _GEN_239 = ovfState[1] ? 1'h0 : _GEN_201; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 297:16]
  wire  _GEN_274 = ovfState[0] | _GEN_239; // @[TraceInBlock.scala 308:26 TraceInBlock.scala 278:35]
  wire  _GEN_314 = ingControlCoreReg_teInstruction == 3'h0 ? 1'h0 : _GEN_274; // @[TraceInBlock.scala 466:56 TraceInBlock.scala 468:40]
  wire  btmEventFifo_1_valid = _T_6 ? 1'h0 : _GEN_314; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 297:16]
  wire [1:0] _btmCountNext_T = btmEventFifo_0_valid + btmEventFifo_1_valid; // @[TraceInBlock.scala 267:50]
  wire [16:0] _GEN_381 = {{15'd0}, _btmCountNext_T}; // @[TraceInBlock.scala 267:30]
  wire [16:0] btmCountNext = btmCount + _GEN_381; // @[TraceInBlock.scala 267:30]
  wire [3:0] _btmCountFull_T_1 = ingControlCoreReg_teSyncMaxBTM + 4'h5; // @[TraceInBlock.scala 273:69]
  wire [16:0] _btmCountFull_T_2 = btmCountNext >> _btmCountFull_T_1; // @[TraceInBlock.scala 273:35]
  wire  sendingSync = _sendingIcnt_T | istrig | btmCountFull & (isexception | isuninf); // @[TraceInBlock.scala 406:87]
  wire  _GEN_75 = iValid0 & sendingSync; // @[TraceInBlock.scala 395:45 TraceInBlock.scala 441:34 TraceInBlock.scala 259:17]
  wire  _GEN_89 = ~lastCycle_tracing & ~io_ingTrigger_0_traceon ? 1'h0 : _GEN_75; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 259:17]
  wire  _GEN_150 = inDebug ? 1'h0 : _GEN_89; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 259:17]
  wire  _GEN_187 = inReset ? 1'h0 : _GEN_150; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 259:17]
  wire  _GEN_224 = syncOnly ? 1'h0 : _GEN_187; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire  _GEN_261 = ovfState[1] ? 1'h0 : _GEN_224; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 259:17]
  wire  _GEN_298 = ovfState[0] ? 1'h0 : _GEN_261; // @[TraceInBlock.scala 308:26 TraceInBlock.scala 259:17]
  wire  stateZ_0_syncSent = _T_6 ? 1'h0 : _GEN_298; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 259:17]
  wire [39:0] _btmEvent_0_bits_iaddr_T = iValid0 ? inGroup_0_iaddr : {{1'd0}, lastCycle_iaddr}; // @[TraceInBlock.scala 292:26]
  wire  btmEvent_0_bits_hisv_lo = |lastCycle_hisv; // @[TraceInBlock.scala 295:94]
  wire [1:0] _btmEvent_0_bits_hisv_T = {1'h0,btmEvent_0_bits_hisv_lo}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_74 = iValid0 ? inGroupPA_0 : lastCycle_iaddr; // @[TraceInBlock.scala 395:45 TraceInBlock.scala 440:31 TraceInBlock.scala 259:17]
  wire [38:0] _GEN_88 = ~lastCycle_tracing & ~io_ingTrigger_0_traceon ? lastCycle_iaddr : _GEN_74; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 259:17]
  wire [38:0] _GEN_149 = inDebug ? lastCycle_iaddr : _GEN_88; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 259:17]
  wire [38:0] _GEN_186 = inReset ? lastCycle_iaddr : _GEN_149; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 259:17]
  wire [38:0] _GEN_223 = syncOnly ? lastCycle_iaddr : _GEN_186; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire [38:0] _GEN_260 = ovfState[1] ? lastCycle_iaddr : _GEN_223; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 259:17]
  wire [38:0] _GEN_297 = ovfState[0] ? lastCycle_iaddr : _GEN_260; // @[TraceInBlock.scala 308:26 TraceInBlock.scala 259:17]
  wire [38:0] stateZ_0_iaddr = _T_6 ? lastCycle_iaddr : _GEN_297; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 259:17]
  wire [39:0] _btmEvent_1_bits_iaddr_T = {{1'd0}, stateZ_0_iaddr}; // @[TraceInBlock.scala 292:26]
  wire [31:0] histM = sendingHist ? 32'h1 : lastCycle_hist; // @[TraceInBlock.scala 423:30]
  wire [30:0] stateZ_0_hist_hi = histM[30:0]; // @[TraceInBlock.scala 428:56]
  wire [31:0] _stateZ_0_hist_T = {stateZ_0_hist_hi,stateZ_0_hist_lo}; // @[Cat.scala 30:58]
  wire [31:0] _stateZ_0_hist_T_1 = isbranch ? _stateZ_0_hist_T : histM; // @[TraceInBlock.scala 428:36]
  wire [31:0] _GEN_70 = iValid0 ? _stateZ_0_hist_T_1 : lastCycle_hist; // @[TraceInBlock.scala 395:45 TraceInBlock.scala 428:30 TraceInBlock.scala 259:17]
  wire [31:0] _GEN_84 = ~lastCycle_tracing & ~io_ingTrigger_0_traceon ? lastCycle_hist : _GEN_70; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 259:17]
  wire [31:0] _GEN_145 = inDebug ? lastCycle_hist : _GEN_84; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 259:17]
  wire [31:0] _GEN_182 = inReset ? lastCycle_hist : _GEN_145; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 259:17]
  wire [31:0] _GEN_219 = syncOnly ? lastCycle_hist : _GEN_182; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire [31:0] _GEN_256 = ovfState[1] ? lastCycle_hist : _GEN_219; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 259:17]
  wire [31:0] _GEN_293 = ovfState[0] ? lastCycle_hist : _GEN_256; // @[TraceInBlock.scala 308:26 TraceInBlock.scala 259:17]
  wire [31:0] stateZ_0_hist = _T_6 ? lastCycle_hist : _GEN_293; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 259:17]
  wire  btmEvent_1_bits_hisv_lo = |stateZ_0_hisv; // @[TraceInBlock.scala 295:94]
  wire [1:0] _btmEvent_1_bits_hisv_T = {1'h0,btmEvent_1_bits_hisv_lo}; // @[Cat.scala 30:58]
  wire [4:0] _GEN_41 = ~traceStall ? 5'hf : btmEvent_1_bits_reason; // @[TraceInBlock.scala 321:32 TraceInBlock.scala 279:41 TraceInBlock.scala 296:15]
  wire [13:0] _GEN_42 = ~traceStall ? 14'h0 : btmEvent_1_bits_icnt; // @[TraceInBlock.scala 321:32 TraceInBlock.scala 296:15]
  wire [13:0] _scnt_T_1 = scnt + 14'h1; // @[TraceInBlock.scala 324:26]
  wire [4:0] _GEN_44 = maxscnt ? _GEN_41 : btmEvent_1_bits_reason; // @[TraceInBlock.scala 320:26 TraceInBlock.scala 296:15]
  wire [13:0] _GEN_45 = maxscnt ? _GEN_42 : btmEvent_1_bits_icnt; // @[TraceInBlock.scala 320:26 TraceInBlock.scala 296:15]
  wire [4:0] _GEN_49 = _T | inDebug | ~ingressIaddrValid ? btmEvent_1_bits_reason : _GEN_44; // @[TraceInBlock.scala 317:63 TraceInBlock.scala 296:15]
  wire [13:0] _GEN_50 = _T | inDebug | ~ingressIaddrValid ? btmEvent_1_bits_icnt : _GEN_45; // @[TraceInBlock.scala 317:63 TraceInBlock.scala 296:15]
  wire [4:0] _GEN_53 = stateZ_0_gapReason[4] ? 5'h13 : btmEvent_1_bits_reason; // @[TraceInBlock.scala 330:29 TraceInBlock.scala 279:41 TraceInBlock.scala 296:15]
  wire [4:0] _GEN_56 = stateZ_0_gapReason[4] ? 5'h12 : btmEvent_1_bits_reason; // @[TraceInBlock.scala 335:29 TraceInBlock.scala 279:41 TraceInBlock.scala 296:15]
  wire  iscall = _isuninf_T_1 | stateA_0_itype == 4'h9; // @[TraceInBlock.scala 375:51]
  wire [4:0] _GEN_61 = lastCycle_gapReason[4] ? 5'h11 : 5'h1f; // @[TraceInBlock.scala 391:33 TraceInBlock.scala 285:37 TraceInBlock.scala 291:21]
  wire [4:0] _btmEvent_0_bits_reason_T = btmCountFull ? 5'h1e : 5'h16; // @[TraceInBlock.scala 412:37]
  wire [2:0] _btmEvent_0_bits_reason_T_1 = stateA_0_wpsync ? 3'h6 : 3'h0; // @[TraceInBlock.scala 413:37]
  wire [4:0] _btmEvent_0_bits_reason_T_2 = btmCountFull ? 5'h1d : 5'h15; // @[TraceInBlock.scala 414:37]
  wire [4:0] _btmEvent_0_bits_reason_T_3 = btmCountFull ? 5'h1d : 5'h1f; // @[Mux.scala 98:16]
  wire [4:0] _btmEvent_0_bits_reason_T_4 = histNeedsSending ? 5'h19 : _btmEvent_0_bits_reason_T_3; // @[Mux.scala 98:16]
  wire [4:0] _btmEvent_0_bits_reason_T_5 = isuninf ? _btmEvent_0_bits_reason_T_2 : _btmEvent_0_bits_reason_T_4; // @[Mux.scala 98:16]
  wire [4:0] _btmEvent_0_bits_reason_T_6 = istrig ? {{2'd0}, _btmEvent_0_bits_reason_T_1} : _btmEvent_0_bits_reason_T_5; // @[Mux.scala 98:16]
  wire [4:0] _btmEvent_0_bits_reason_T_7 = isexception ? _btmEvent_0_bits_reason_T : _btmEvent_0_bits_reason_T_6; // @[Mux.scala 98:16]
  wire [4:0] _btmEvent_0_bits_reason_T_8 = sendingGap ? lastCycle_gapReason : _btmEvent_0_bits_reason_T_7; // @[Mux.scala 98:16]
  wire [13:0] _GEN_62 = sendingGap ? 14'h0 : lastCycle_icnt; // @[TraceInBlock.scala 420:33 TraceInBlock.scala 420:57 TraceInBlock.scala 293:19]
  wire [15:0] _stateZ_0_ras_T_1 = lastCycle_ras + 16'h1; // @[TraceInBlock.scala 437:102]
  wire [15:0] _stateZ_0_ras_T_3 = lastCycle_ras - 16'h1; // @[TraceInBlock.scala 438:102]
  wire [15:0] _GEN_63 = isreturn & _isuninf_T_7 ? _stateZ_0_ras_T_3 : lastCycle_ras; // @[TraceInBlock.scala 438:69 TraceInBlock.scala 438:85 TraceInBlock.scala 259:17]
  wire [15:0] _GEN_64 = iscall & ~(&lastCycle_ras) & _stateA_0_useHcnt_T ? _stateZ_0_ras_T_1 : _GEN_63; // @[TraceInBlock.scala 437:69 TraceInBlock.scala 437:85]
  wire [15:0] _GEN_65 = sendingSync ? 16'h0 : _GEN_64; // @[TraceInBlock.scala 436:69 TraceInBlock.scala 436:85]
  wire [4:0] _GEN_66 = iValid0 ? _btmEvent_0_bits_reason_T_8 : 5'h1f; // @[TraceInBlock.scala 395:45 TraceInBlock.scala 410:39 TraceInBlock.scala 291:21]
  wire  _GEN_67 = iValid0 & (sendingGap | isexception | istrig | isuninf | histNeedsSending | sendingDecoupledSync); // @[TraceInBlock.scala 395:45 TraceInBlock.scala 419:33 TraceInBlock.scala 290:15]
  wire [13:0] _GEN_68 = iValid0 ? _GEN_62 : lastCycle_icnt; // @[TraceInBlock.scala 395:45 TraceInBlock.scala 293:19]
  wire [15:0] _GEN_73 = iValid0 ? _GEN_65 : lastCycle_ras; // @[TraceInBlock.scala 395:45 TraceInBlock.scala 259:17]
  wire  _GEN_79 = ~lastCycle_tracing & ~io_ingTrigger_0_traceon ? lastCycle_gapReason[4] : _GEN_67; // @[TraceInBlock.scala 390:45]
  wire [4:0] _GEN_80 = ~lastCycle_tracing & ~io_ingTrigger_0_traceon ? _GEN_61 : _GEN_66; // @[TraceInBlock.scala 390:45]
  wire [13:0] _GEN_82 = ~lastCycle_tracing & ~io_ingTrigger_0_traceon ? lastCycle_icnt : _GEN_68; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 293:19]
  wire [15:0] _GEN_87 = ~lastCycle_tracing & ~io_ingTrigger_0_traceon ? lastCycle_ras : _GEN_73; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 259:17]
  wire [15:0] _GEN_148 = inDebug ? lastCycle_ras : _GEN_87; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 259:17]
  wire [15:0] _GEN_185 = inReset ? lastCycle_ras : _GEN_148; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 259:17]
  wire [15:0] _GEN_222 = syncOnly ? lastCycle_ras : _GEN_185; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire [4:0] _GEN_95 = stateZ_0_gapReason[4] ? 5'h11 : 5'h1f; // @[TraceInBlock.scala 391:33 TraceInBlock.scala 285:37 TraceInBlock.scala 291:21]
  wire [3:0] _T_36 = stateZ_0_wpsync ? 4'he : 4'h8; // @[TraceInBlock.scala 456:32]
  wire [4:0] _GEN_97 = stateZ_0_wpsync & ingressIaddrValid & ~inRetire_0 ? {{1'd0}, _T_36} : 5'h1f; // @[TraceInBlock.scala 455:104 TraceInBlock.scala 285:37 TraceInBlock.scala 291:21]
  wire [39:0] _GEN_98 = stateZ_0_wpsync & ingressIaddrValid & ~inRetire_0 ? pcSampleCoreReg : _btmEvent_1_bits_iaddr_T; // @[TraceInBlock.scala 455:104 TraceInBlock.scala 457:40 TraceInBlock.scala 292:20]
  wire  _GEN_101 = icntFull | _T_35; // @[TraceInBlock.scala 452:37 TraceInBlock.scala 284:31]
  wire [4:0] _GEN_102 = icntFull ? 5'h18 : _GEN_97; // @[TraceInBlock.scala 452:37 TraceInBlock.scala 285:37]
  wire [13:0] _GEN_103 = icntFull ? 14'h0 : stateZ_0_icnt; // @[TraceInBlock.scala 452:37 TraceInBlock.scala 454:32 TraceInBlock.scala 259:17]
  wire [39:0] _GEN_104 = icntFull ? _btmEvent_1_bits_iaddr_T : _GEN_98; // @[TraceInBlock.scala 452:37 TraceInBlock.scala 292:20]
  wire  _GEN_107 = hcntFull | _GEN_101; // @[TraceInBlock.scala 447:31 TraceInBlock.scala 284:31]
  wire [4:0] _GEN_108 = hcntFull ? 5'h19 : _GEN_102; // @[TraceInBlock.scala 447:31 TraceInBlock.scala 285:37]
  wire [31:0] _GEN_109 = hcntFull ? 32'h1 : stateZ_0_hist; // @[TraceInBlock.scala 447:31 TraceInBlock.scala 449:32 TraceInBlock.scala 259:17]
  wire [1:0] _GEN_110 = hcntFull ? 2'h0 : stateZ_0_hisv; // @[TraceInBlock.scala 447:31 TraceInBlock.scala 450:32 TraceInBlock.scala 259:17]
  wire [12:0] _GEN_111 = hcntFull ? 13'h0 : stateZ_0_hcnt; // @[TraceInBlock.scala 447:31 TraceInBlock.scala 451:32 TraceInBlock.scala 259:17]
  wire [13:0] _GEN_112 = hcntFull ? stateZ_0_icnt : _GEN_103; // @[TraceInBlock.scala 447:31 TraceInBlock.scala 259:17]
  wire [39:0] _GEN_113 = hcntFull ? _btmEvent_1_bits_iaddr_T : _GEN_104; // @[TraceInBlock.scala 447:31 TraceInBlock.scala 292:20]
  wire  _GEN_116 = ~stateZ_0_tracing & ~probeton ? stateZ_0_gapReason[4] : _GEN_107; // @[TraceInBlock.scala 390:45]
  wire [4:0] _GEN_117 = ~stateZ_0_tracing & ~probeton ? _GEN_95 : _GEN_108; // @[TraceInBlock.scala 390:45]
  wire [4:0] _GEN_118 = ~stateZ_0_tracing & ~probeton ? 5'h5 : stateZ_0_gapReason; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 392:33 TraceInBlock.scala 259:17]
  wire [31:0] _GEN_119 = ~stateZ_0_tracing & ~probeton ? stateZ_0_hist : _GEN_109; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 259:17]
  wire [1:0] _GEN_120 = ~stateZ_0_tracing & ~probeton ? stateZ_0_hisv : _GEN_110; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 259:17]
  wire [12:0] _GEN_121 = ~stateZ_0_tracing & ~probeton ? stateZ_0_hcnt : _GEN_111; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 259:17]
  wire [13:0] _GEN_122 = ~stateZ_0_tracing & ~probeton ? stateZ_0_icnt : _GEN_112; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 259:17]
  wire [39:0] _GEN_123 = ~stateZ_0_tracing & ~probeton ? _btmEvent_1_bits_iaddr_T : _GEN_113; // @[TraceInBlock.scala 390:45 TraceInBlock.scala 292:20]
  wire [4:0] _GEN_127 = inDebug ? _GEN_56 : btmEvent_1_bits_reason; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 366:19]
  wire [4:0] _GEN_129 = inDebug ? 5'h3 : _GEN_118; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 336:35]
  wire  _GEN_140 = inDebug ? 1'h0 : _GEN_79; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 290:15]
  wire [4:0] _GEN_141 = inDebug ? 5'h1f : _GEN_80; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 291:21]
  wire [13:0] _GEN_143 = inDebug ? lastCycle_icnt : _GEN_82; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 293:19]
  wire  _GEN_154 = inDebug ? 1'h0 : _GEN_116; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 290:15]
  wire [4:0] _GEN_155 = inDebug ? 5'h1f : _GEN_117; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 291:21]
  wire [31:0] _GEN_156 = inDebug ? stateZ_0_hist : _GEN_119; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 259:17]
  wire [1:0] _GEN_157 = inDebug ? stateZ_0_hisv : _GEN_120; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 259:17]
  wire [12:0] _GEN_158 = inDebug ? stateZ_0_hcnt : _GEN_121; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 259:17]
  wire [13:0] _GEN_159 = inDebug ? stateZ_0_icnt : _GEN_122; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 259:17]
  wire [39:0] _GEN_160 = inDebug ? _btmEvent_1_bits_iaddr_T : _GEN_123; // @[TraceInBlock.scala 333:28 TraceInBlock.scala 292:20]
  wire [4:0] _GEN_164 = inReset ? _GEN_53 : _GEN_127; // @[TraceInBlock.scala 328:28]
  wire [4:0] _GEN_166 = inReset ? 5'h1 : _GEN_129; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 331:35]
  wire  _GEN_177 = inReset ? 1'h0 : _GEN_140; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 290:15]
  wire [4:0] _GEN_178 = inReset ? 5'h1f : _GEN_141; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 291:21]
  wire [13:0] _GEN_180 = inReset ? lastCycle_icnt : _GEN_143; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 293:19]
  wire  _GEN_191 = inReset ? 1'h0 : _GEN_154; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 290:15]
  wire [4:0] _GEN_192 = inReset ? 5'h1f : _GEN_155; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 291:21]
  wire [31:0] _GEN_193 = inReset ? stateZ_0_hist : _GEN_156; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 259:17]
  wire [1:0] _GEN_194 = inReset ? stateZ_0_hisv : _GEN_157; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 259:17]
  wire [12:0] _GEN_195 = inReset ? stateZ_0_hcnt : _GEN_158; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 259:17]
  wire [13:0] _GEN_196 = inReset ? stateZ_0_icnt : _GEN_159; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 259:17]
  wire [39:0] _GEN_197 = inReset ? _btmEvent_1_bits_iaddr_T : _GEN_160; // @[TraceInBlock.scala 328:28 TraceInBlock.scala 292:20]
  wire [39:0] _GEN_200 = syncOnly ? pcSampleCoreReg : {{1'd0}, btmEvent_1_bits_iaddr}; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 316:42]
  wire [4:0] _GEN_202 = syncOnly ? _GEN_49 : _GEN_164; // @[TraceInBlock.scala 313:29]
  wire [13:0] _GEN_203 = syncOnly ? _GEN_50 : btmEvent_1_bits_icnt; // @[TraceInBlock.scala 313:29]
  wire [4:0] _GEN_204 = syncOnly ? stateZ_0_gapReason : _GEN_166; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire  _GEN_214 = syncOnly ? 1'h0 : _GEN_177; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 290:15]
  wire  _GEN_228 = syncOnly ? 1'h0 : _GEN_191; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 290:15]
  wire [31:0] _GEN_230 = syncOnly ? stateZ_0_hist : _GEN_193; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire [1:0] _GEN_231 = syncOnly ? stateZ_0_hisv : _GEN_194; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire [12:0] _GEN_232 = syncOnly ? stateZ_0_hcnt : _GEN_195; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire [13:0] _GEN_233 = syncOnly ? stateZ_0_icnt : _GEN_196; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 259:17]
  wire [39:0] _GEN_234 = syncOnly ? _btmEvent_1_bits_iaddr_T : _GEN_197; // @[TraceInBlock.scala 313:29 TraceInBlock.scala 292:20]
  wire [39:0] _GEN_238 = ovfState[1] ? {{1'd0}, btmEvent_1_bits_iaddr} : _GEN_200; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 296:15]
  wire [4:0] _GEN_240 = ovfState[1] ? btmEvent_1_bits_reason : _GEN_202; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 296:15]
  wire [13:0] _GEN_241 = ovfState[1] ? btmEvent_1_bits_icnt : _GEN_203; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 296:15]
  wire [39:0] _GEN_271 = ovfState[1] ? _btmEvent_1_bits_iaddr_T : _GEN_234; // @[TraceInBlock.scala 310:32 TraceInBlock.scala 292:20]
  wire [4:0] _GEN_275 = ovfState[0] ? 5'h17 : _GEN_240; // @[TraceInBlock.scala 308:26 TraceInBlock.scala 279:41]
  wire [13:0] _GEN_276 = ovfState[0] ? 14'h0 : _GEN_241; // @[TraceInBlock.scala 308:26]
  wire [39:0] _GEN_278 = ovfState[0] ? {{1'd0}, btmEvent_1_bits_iaddr} : _GEN_238; // @[TraceInBlock.scala 308:26 TraceInBlock.scala 296:15]
  wire [39:0] _GEN_308 = ovfState[0] ? _btmEvent_1_bits_iaddr_T : _GEN_271; // @[TraceInBlock.scala 308:26 TraceInBlock.scala 292:20]
  wire  btmEventFifo_0_ready = fifo_io_enq_0_ready; // @[TraceInBlock.scala 222:28 TraceInBlock.scala 508:17]
  wire  btmEventFifo_1_ready = fifo_io_enq_1_ready; // @[TraceInBlock.scala 222:28 TraceInBlock.scala 508:17]
  wire [1:0] _GEN_315 = ovfState[1] & teIdleCore_teIdleCore ? 2'h0 : ovfState; // @[TraceInBlock.scala 481:45 TraceInBlock.scala 482:18 TraceInBlock.scala 230:27]
  wire [39:0] _GEN_324 = _T_6 ? {{1'd0}, btmEvent_1_bits_iaddr} : _GEN_278; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 296:15]
  wire [39:0] _GEN_353 = _T_6 ? _btmEvent_1_bits_iaddr_T : _GEN_308; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 292:20]
  wire  _fifoCount_T = btmEventFifo_0_ready & btmEventFifo_0_valid; // @[Decoupled.scala 40:37]
  wire  _fifoCount_T_1 = btmEventFifo_1_ready & btmEventFifo_1_valid; // @[Decoupled.scala 40:37]
  wire [1:0] _fifoCount_T_2 = _fifoCount_T + _fifoCount_T_1; // @[TraceInBlock.scala 514:50]
  wire [3:0] _GEN_382 = {{2'd0}, _fifoCount_T_2}; // @[TraceInBlock.scala 514:30]
  wire [3:0] _fifoCount_T_5 = fifoCount + _GEN_382; // @[TraceInBlock.scala 514:30]
  wire  btmEventCore_ready = inner_io_btmEventRational_source_io_enq_ready; // @[TraceInBlock.scala 223:28 RationalCrossing.scala 159:19]
  wire  btmEventCore_valid = fifo_io_deq_0_valid; // @[TraceInBlock.scala 223:28 TraceInBlock.scala 509:18]
  wire  _fifoCount_T_6 = btmEventCore_ready & btmEventCore_valid; // @[Decoupled.scala 40:37]
  wire [3:0] _GEN_383 = {{3'd0}, _fifoCount_T_6}; // @[TraceInBlock.scala 514:61]
  wire [3:0] _fifoCount_T_8 = _fifoCount_T_5 - _GEN_383; // @[TraceInBlock.scala 514:61]
  wire  _GEN_376 = inRetire_0 | ingressIaddrValid; // @[TraceInBlock.scala 595:30 TraceInBlock.scala 596:25 TraceInBlock.scala 192:36]
  wire  pcSampleCore_ready = inner_io_pcSampleAsync_source_io_enq_ready; // @[TraceInBlock.scala 196:29 AsyncQueue.scala 217:19]
  wire  pcCaptureCore_ready = inner_io_pcCaptureAsync_source_io_enq_ready; // @[TraceInBlock.scala 200:29 AsyncQueue.scala 217:19]
  RHEA__TraceIngressInner inner_ ( // @[TraceInBlock.scala 84:23]
    .clock(inner__clock),
    .reset(inner__reset),
    .io_traceOnCore(inner__io_traceOnCore),
    .io_traceOn(inner__io_traceOn),
    .io_btmEventRational_bits0_reason(inner__io_btmEventRational_bits0_reason),
    .io_btmEventRational_bits0_iaddr(inner__io_btmEventRational_bits0_iaddr),
    .io_btmEventRational_bits0_icnt(inner__io_btmEventRational_bits0_icnt),
    .io_btmEventRational_bits0_hist(inner__io_btmEventRational_bits0_hist),
    .io_btmEventRational_bits0_hisv(inner__io_btmEventRational_bits0_hisv),
    .io_btmEventRational_bits1_reason(inner__io_btmEventRational_bits1_reason),
    .io_btmEventRational_bits1_iaddr(inner__io_btmEventRational_bits1_iaddr),
    .io_btmEventRational_bits1_icnt(inner__io_btmEventRational_bits1_icnt),
    .io_btmEventRational_bits1_hist(inner__io_btmEventRational_bits1_hist),
    .io_btmEventRational_bits1_hisv(inner__io_btmEventRational_bits1_hisv),
    .io_btmEventRational_valid(inner__io_btmEventRational_valid),
    .io_btmEventRational_source(inner__io_btmEventRational_source),
    .io_btmEventRational_ready(inner__io_btmEventRational_ready),
    .io_btmEventRational_sink(inner__io_btmEventRational_sink),
    .io_btmEvent_ready(inner__io_btmEvent_ready),
    .io_btmEvent_valid(inner__io_btmEvent_valid),
    .io_btmEvent_bits_reason(inner__io_btmEvent_bits_reason),
    .io_btmEvent_bits_iaddr(inner__io_btmEvent_bits_iaddr),
    .io_btmEvent_bits_icnt(inner__io_btmEvent_bits_icnt),
    .io_btmEvent_bits_hist(inner__io_btmEvent_bits_hist),
    .io_btmEvent_bits_hisv(inner__io_btmEvent_bits_hisv),
    .io_pcCaptureAsync_mem_0(inner__io_pcCaptureAsync_mem_0),
    .io_pcCaptureAsync_ridx(inner__io_pcCaptureAsync_ridx),
    .io_pcCaptureAsync_widx(inner__io_pcCaptureAsync_widx),
    .io_pcSampleAsync_mem_0(inner__io_pcSampleAsync_mem_0),
    .io_pcSampleAsync_ridx(inner__io_pcSampleAsync_ridx),
    .io_pcSampleAsync_widx(inner__io_pcSampleAsync_widx),
    .io_pcCapture_valid(inner__io_pcCapture_valid),
    .io_pcCapture_bits(inner__io_pcCapture_bits),
    .io_pcSample_valid(inner__io_pcSample_valid),
    .io_pcSample_bits(inner__io_pcSample_bits)
  );
  RHEA__AsyncQueueSink_5 ingControlCoreIn_sink ( // @[AsyncQueue.scala 207:22]
    .rf_reset(ingControlCoreIn_sink_rf_reset),
    .clock(ingControlCoreIn_sink_clock),
    .reset(ingControlCoreIn_sink_reset),
    .io_deq_valid(ingControlCoreIn_sink_io_deq_valid),
    .io_deq_bits_teEnable(ingControlCoreIn_sink_io_deq_bits_teEnable),
    .io_deq_bits_teTracing(ingControlCoreIn_sink_io_deq_bits_teTracing),
    .io_deq_bits_teInstruction(ingControlCoreIn_sink_io_deq_bits_teInstruction),
    .io_deq_bits_teStallEnable(ingControlCoreIn_sink_io_deq_bits_teStallEnable),
    .io_deq_bits_teSyncMaxInst(ingControlCoreIn_sink_io_deq_bits_teSyncMaxInst),
    .io_deq_bits_teSyncMaxBTM(ingControlCoreIn_sink_io_deq_bits_teSyncMaxBTM),
    .io_async_mem_0_teEnable(ingControlCoreIn_sink_io_async_mem_0_teEnable),
    .io_async_mem_0_teTracing(ingControlCoreIn_sink_io_async_mem_0_teTracing),
    .io_async_mem_0_teInstruction(ingControlCoreIn_sink_io_async_mem_0_teInstruction),
    .io_async_mem_0_teStallEnable(ingControlCoreIn_sink_io_async_mem_0_teStallEnable),
    .io_async_mem_0_teSyncMaxInst(ingControlCoreIn_sink_io_async_mem_0_teSyncMaxInst),
    .io_async_mem_0_teSyncMaxBTM(ingControlCoreIn_sink_io_async_mem_0_teSyncMaxBTM),
    .io_async_ridx(ingControlCoreIn_sink_io_async_ridx),
    .io_async_widx(ingControlCoreIn_sink_io_async_widx)
  );
  RHEA__MultiPortQueue fifo ( // @[TraceInBlock.scala 507:22]
    .rf_reset(fifo_rf_reset),
    .clock(fifo_clock),
    .reset(fifo_reset),
    .io_enq_0_ready(fifo_io_enq_0_ready),
    .io_enq_0_valid(fifo_io_enq_0_valid),
    .io_enq_0_bits_reason(fifo_io_enq_0_bits_reason),
    .io_enq_0_bits_iaddr(fifo_io_enq_0_bits_iaddr),
    .io_enq_0_bits_icnt(fifo_io_enq_0_bits_icnt),
    .io_enq_0_bits_hist(fifo_io_enq_0_bits_hist),
    .io_enq_0_bits_hisv(fifo_io_enq_0_bits_hisv),
    .io_enq_1_ready(fifo_io_enq_1_ready),
    .io_enq_1_valid(fifo_io_enq_1_valid),
    .io_enq_1_bits_reason(fifo_io_enq_1_bits_reason),
    .io_enq_1_bits_iaddr(fifo_io_enq_1_bits_iaddr),
    .io_enq_1_bits_icnt(fifo_io_enq_1_bits_icnt),
    .io_enq_1_bits_hist(fifo_io_enq_1_bits_hist),
    .io_enq_1_bits_hisv(fifo_io_enq_1_bits_hisv),
    .io_deq_0_ready(fifo_io_deq_0_ready),
    .io_deq_0_valid(fifo_io_deq_0_valid),
    .io_deq_0_bits_reason(fifo_io_deq_0_bits_reason),
    .io_deq_0_bits_iaddr(fifo_io_deq_0_bits_iaddr),
    .io_deq_0_bits_icnt(fifo_io_deq_0_bits_icnt),
    .io_deq_0_bits_hist(fifo_io_deq_0_bits_hist),
    .io_deq_0_bits_hisv(fifo_io_deq_0_bits_hisv)
  );
  RHEA__RationalCrossingSource_10 inner_io_btmEventRational_source ( // @[RationalCrossing.scala 158:24]
    .rf_reset(inner_io_btmEventRational_source_rf_reset),
    .clock(inner_io_btmEventRational_source_clock),
    .reset(inner_io_btmEventRational_source_reset),
    .io_enq_ready(inner_io_btmEventRational_source_io_enq_ready),
    .io_enq_valid(inner_io_btmEventRational_source_io_enq_valid),
    .io_enq_bits_reason(inner_io_btmEventRational_source_io_enq_bits_reason),
    .io_enq_bits_iaddr(inner_io_btmEventRational_source_io_enq_bits_iaddr),
    .io_enq_bits_icnt(inner_io_btmEventRational_source_io_enq_bits_icnt),
    .io_enq_bits_hist(inner_io_btmEventRational_source_io_enq_bits_hist),
    .io_enq_bits_hisv(inner_io_btmEventRational_source_io_enq_bits_hisv),
    .io_deq_bits0_reason(inner_io_btmEventRational_source_io_deq_bits0_reason),
    .io_deq_bits0_iaddr(inner_io_btmEventRational_source_io_deq_bits0_iaddr),
    .io_deq_bits0_icnt(inner_io_btmEventRational_source_io_deq_bits0_icnt),
    .io_deq_bits0_hist(inner_io_btmEventRational_source_io_deq_bits0_hist),
    .io_deq_bits0_hisv(inner_io_btmEventRational_source_io_deq_bits0_hisv),
    .io_deq_bits1_reason(inner_io_btmEventRational_source_io_deq_bits1_reason),
    .io_deq_bits1_iaddr(inner_io_btmEventRational_source_io_deq_bits1_iaddr),
    .io_deq_bits1_icnt(inner_io_btmEventRational_source_io_deq_bits1_icnt),
    .io_deq_bits1_hist(inner_io_btmEventRational_source_io_deq_bits1_hist),
    .io_deq_bits1_hisv(inner_io_btmEventRational_source_io_deq_bits1_hisv),
    .io_deq_valid(inner_io_btmEventRational_source_io_deq_valid),
    .io_deq_source(inner_io_btmEventRational_source_io_deq_source),
    .io_deq_ready(inner_io_btmEventRational_source_io_deq_ready),
    .io_deq_sink(inner_io_btmEventRational_source_io_deq_sink)
  );
  RHEA__AsyncQueueSource_3 inner_io_pcCaptureAsync_source ( // @[AsyncQueue.scala 216:24]
    .rf_reset(inner_io_pcCaptureAsync_source_rf_reset),
    .clock(inner_io_pcCaptureAsync_source_clock),
    .reset(inner_io_pcCaptureAsync_source_reset),
    .io_enq_ready(inner_io_pcCaptureAsync_source_io_enq_ready),
    .io_enq_valid(inner_io_pcCaptureAsync_source_io_enq_valid),
    .io_enq_bits(inner_io_pcCaptureAsync_source_io_enq_bits),
    .io_async_mem_0(inner_io_pcCaptureAsync_source_io_async_mem_0),
    .io_async_ridx(inner_io_pcCaptureAsync_source_io_async_ridx),
    .io_async_widx(inner_io_pcCaptureAsync_source_io_async_widx)
  );
  RHEA__AsyncQueueSource_3 inner_io_pcSampleAsync_source ( // @[AsyncQueue.scala 216:24]
    .rf_reset(inner_io_pcSampleAsync_source_rf_reset),
    .clock(inner_io_pcSampleAsync_source_clock),
    .reset(inner_io_pcSampleAsync_source_reset),
    .io_enq_ready(inner_io_pcSampleAsync_source_io_enq_ready),
    .io_enq_valid(inner_io_pcSampleAsync_source_io_enq_valid),
    .io_enq_bits(inner_io_pcSampleAsync_source_io_enq_bits),
    .io_async_mem_0(inner_io_pcSampleAsync_source_io_async_mem_0),
    .io_async_ridx(inner_io_pcSampleAsync_source_io_async_ridx),
    .io_async_widx(inner_io_pcSampleAsync_source_io_async_widx)
  );
  assign ingControlCoreIn_sink_rf_reset = rf_reset;
  assign fifo_rf_reset = rf_reset;
  assign inner_io_btmEventRational_source_rf_reset = rf_reset;
  assign inner_io_pcCaptureAsync_source_rf_reset = rf_reset;
  assign inner_io_pcSampleAsync_source_rf_reset = rf_reset;
  assign io_teActiveCore = teActiveCore_teActiveCore; // @[TraceInBlock.scala 114:28 TraceInBlock.scala 129:20]
  assign io_ingControlAsync_ridx = ingControlCoreIn_sink_io_async_ridx; // @[AsyncQueue.scala 208:19]
  assign io_traceOn = inner__io_traceOn; // @[TraceInBlock.scala 588:16]
  assign io_iretire_0 = |ingress_group_0_iretire; // @[TraceInBlock.scala 167:54]
  assign io_ivalid_0 = rawRetire_0 | ingress_group_0_itype != 4'h0; // @[TraceInBlock.scala 168:58]
  assign io_ingValid = auto_trace_legacy_in_0_valid; // @[BundleBridge.scala 43:11 LazyModule.scala 388:16]
  assign io_btmEvent_valid = inner__io_btmEvent_valid; // @[TraceInBlock.scala 567:19]
  assign io_btmEvent_bits_reason = inner__io_btmEvent_bits_reason; // @[TraceInBlock.scala 567:19]
  assign io_btmEvent_bits_iaddr = inner__io_btmEvent_bits_iaddr; // @[TraceInBlock.scala 567:19]
  assign io_btmEvent_bits_icnt = inner__io_btmEvent_bits_icnt; // @[TraceInBlock.scala 567:19]
  assign io_btmEvent_bits_hist = inner__io_btmEvent_bits_hist; // @[TraceInBlock.scala 567:19]
  assign io_btmEvent_bits_hisv = inner__io_btmEvent_bits_hisv; // @[TraceInBlock.scala 567:19]
  assign io_traceStall = ingControlCoreReg_teStallEnable & fifoCount >= 4'h2; // @[TraceInBlock.scala 517:53]
  assign io_pcCapture_valid = inner__io_pcCapture_valid; // @[TraceInBlock.scala 614:20]
  assign io_pcCapture_bits = inner__io_pcCapture_bits; // @[TraceInBlock.scala 614:20]
  assign io_pcSample_valid = inner__io_pcSample_valid; // @[TraceInBlock.scala 615:20]
  assign io_pcSample_bits = inner__io_pcSample_bits; // @[TraceInBlock.scala 615:20]
  assign inner__clock = io_tlClock; // @[TraceInBlock.scala 118:17]
  assign inner__reset = io_tlReset; // @[TraceInBlock.scala 119:17]
  assign inner__io_traceOnCore = lastCycle_tracing; // @[TraceInBlock.scala 587:26]
  assign inner__io_btmEventRational_bits0_reason = inner_io_btmEventRational_source_io_deq_bits0_reason; // @[TraceInBlock.scala 566:37]
  assign inner__io_btmEventRational_bits0_iaddr = inner_io_btmEventRational_source_io_deq_bits0_iaddr; // @[TraceInBlock.scala 566:37]
  assign inner__io_btmEventRational_bits0_icnt = inner_io_btmEventRational_source_io_deq_bits0_icnt; // @[TraceInBlock.scala 566:37]
  assign inner__io_btmEventRational_bits0_hist = inner_io_btmEventRational_source_io_deq_bits0_hist; // @[TraceInBlock.scala 566:37]
  assign inner__io_btmEventRational_bits0_hisv = inner_io_btmEventRational_source_io_deq_bits0_hisv; // @[TraceInBlock.scala 566:37]
  assign inner__io_btmEventRational_bits1_reason = inner_io_btmEventRational_source_io_deq_bits1_reason; // @[TraceInBlock.scala 566:37]
  assign inner__io_btmEventRational_bits1_iaddr = inner_io_btmEventRational_source_io_deq_bits1_iaddr; // @[TraceInBlock.scala 566:37]
  assign inner__io_btmEventRational_bits1_icnt = inner_io_btmEventRational_source_io_deq_bits1_icnt; // @[TraceInBlock.scala 566:37]
  assign inner__io_btmEventRational_bits1_hist = inner_io_btmEventRational_source_io_deq_bits1_hist; // @[TraceInBlock.scala 566:37]
  assign inner__io_btmEventRational_bits1_hisv = inner_io_btmEventRational_source_io_deq_bits1_hisv; // @[TraceInBlock.scala 566:37]
  assign inner__io_btmEventRational_valid = inner_io_btmEventRational_source_io_deq_valid; // @[TraceInBlock.scala 566:37]
  assign inner__io_btmEventRational_source = inner_io_btmEventRational_source_io_deq_source; // @[TraceInBlock.scala 566:37]
  assign inner__io_btmEvent_ready = io_btmEvent_ready; // @[TraceInBlock.scala 567:19]
  assign inner__io_pcCaptureAsync_mem_0 = inner_io_pcCaptureAsync_source_io_async_mem_0; // @[TraceInBlock.scala 612:35]
  assign inner__io_pcCaptureAsync_widx = inner_io_pcCaptureAsync_source_io_async_widx; // @[TraceInBlock.scala 612:35]
  assign inner__io_pcSampleAsync_mem_0 = inner_io_pcSampleAsync_source_io_async_mem_0; // @[TraceInBlock.scala 613:35]
  assign inner__io_pcSampleAsync_widx = inner_io_pcSampleAsync_source_io_async_widx; // @[TraceInBlock.scala 613:35]
  assign ingControlCoreIn_sink_clock = clock;
  assign ingControlCoreIn_sink_reset = reset;
  assign ingControlCoreIn_sink_io_async_mem_0_teEnable = io_ingControlAsync_mem_0_teEnable; // @[AsyncQueue.scala 208:19]
  assign ingControlCoreIn_sink_io_async_mem_0_teTracing = io_ingControlAsync_mem_0_teTracing; // @[AsyncQueue.scala 208:19]
  assign ingControlCoreIn_sink_io_async_mem_0_teInstruction = io_ingControlAsync_mem_0_teInstruction; // @[AsyncQueue.scala 208:19]
  assign ingControlCoreIn_sink_io_async_mem_0_teStallEnable = io_ingControlAsync_mem_0_teStallEnable; // @[AsyncQueue.scala 208:19]
  assign ingControlCoreIn_sink_io_async_mem_0_teSyncMaxInst = io_ingControlAsync_mem_0_teSyncMaxInst; // @[AsyncQueue.scala 208:19]
  assign ingControlCoreIn_sink_io_async_mem_0_teSyncMaxBTM = io_ingControlAsync_mem_0_teSyncMaxBTM; // @[AsyncQueue.scala 208:19]
  assign ingControlCoreIn_sink_io_async_widx = io_ingControlAsync_widx; // @[AsyncQueue.scala 208:19]
  assign fifo_clock = clock;
  assign fifo_reset = reset;
  assign fifo_io_enq_0_valid = _T_6 ? 1'h0 : _GEN_313; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 297:16]
  assign fifo_io_enq_0_bits_reason = btmEvent_0_bits_reason; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 296:15]
  assign fifo_io_enq_0_bits_iaddr = btmEvent_0_bits_iaddr; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 296:15]
  assign fifo_io_enq_0_bits_icnt = btmEvent_0_bits_icnt; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 296:15]
  assign fifo_io_enq_0_bits_hist = btmEvent_0_bits_hist; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 296:15]
  assign fifo_io_enq_0_bits_hisv = btmEvent_0_bits_hisv; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 296:15]
  assign fifo_io_enq_1_valid = _T_6 ? 1'h0 : _GEN_314; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 297:16]
  assign fifo_io_enq_1_bits_reason = _T_6 ? btmEvent_1_bits_reason : _GEN_275; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 296:15]
  assign fifo_io_enq_1_bits_iaddr = _GEN_324[38:0]; // @[TraceInBlock.scala 222:28]
  assign fifo_io_enq_1_bits_icnt = _T_6 ? btmEvent_1_bits_icnt : _GEN_276; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 296:15]
  assign fifo_io_enq_1_bits_hist = btmEvent_1_bits_hist; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 296:15]
  assign fifo_io_enq_1_bits_hisv = btmEvent_1_bits_hisv; // @[TraceInBlock.scala 300:42 TraceInBlock.scala 296:15]
  assign fifo_io_deq_0_ready = inner_io_btmEventRational_source_io_enq_ready; // @[TraceInBlock.scala 223:28 RationalCrossing.scala 159:19]
  assign inner_io_btmEventRational_source_clock = clock;
  assign inner_io_btmEventRational_source_reset = reset;
  assign inner_io_btmEventRational_source_io_enq_valid = fifo_io_deq_0_valid; // @[TraceInBlock.scala 223:28 TraceInBlock.scala 509:18]
  assign inner_io_btmEventRational_source_io_enq_bits_reason = fifo_io_deq_0_bits_reason; // @[TraceInBlock.scala 223:28 TraceInBlock.scala 509:18]
  assign inner_io_btmEventRational_source_io_enq_bits_iaddr = fifo_io_deq_0_bits_iaddr; // @[TraceInBlock.scala 223:28 TraceInBlock.scala 509:18]
  assign inner_io_btmEventRational_source_io_enq_bits_icnt = fifo_io_deq_0_bits_icnt; // @[TraceInBlock.scala 223:28 TraceInBlock.scala 509:18]
  assign inner_io_btmEventRational_source_io_enq_bits_hist = fifo_io_deq_0_bits_hist; // @[TraceInBlock.scala 223:28 TraceInBlock.scala 509:18]
  assign inner_io_btmEventRational_source_io_enq_bits_hisv = fifo_io_deq_0_bits_hisv; // @[TraceInBlock.scala 223:28 TraceInBlock.scala 509:18]
  assign inner_io_btmEventRational_source_io_deq_ready = inner__io_btmEventRational_ready; // @[TraceInBlock.scala 566:37]
  assign inner_io_btmEventRational_source_io_deq_sink = inner__io_btmEventRational_sink; // @[TraceInBlock.scala 566:37]
  assign inner_io_pcCaptureAsync_source_clock = clock;
  assign inner_io_pcCaptureAsync_source_reset = reset;
  assign inner_io_pcCaptureAsync_source_io_enq_valid = pcCaptureCoreValid; // @[TraceInBlock.scala 200:29 TraceInBlock.scala 604:25]
  assign inner_io_pcCaptureAsync_source_io_enq_bits = pcCaptureCoreReg[38:0]; // @[TraceInBlock.scala 200:29 TraceInBlock.scala 603:24]
  assign inner_io_pcCaptureAsync_source_io_async_ridx = inner__io_pcCaptureAsync_ridx; // @[TraceInBlock.scala 612:35]
  assign inner_io_pcSampleAsync_source_clock = clock;
  assign inner_io_pcSampleAsync_source_reset = reset;
  assign inner_io_pcSampleAsync_source_io_enq_valid = pcSampleCoreValid; // @[TraceInBlock.scala 196:29 TraceInBlock.scala 600:24]
  assign inner_io_pcSampleAsync_source_io_enq_bits = pcSampleCoreReg[38:0]; // @[TraceInBlock.scala 196:29 TraceInBlock.scala 599:23]
  assign inner_io_pcSampleAsync_source_io_async_ridx = inner__io_pcSampleAsync_ridx; // @[TraceInBlock.scala 613:35]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ingress_ingressPriv <= 3'h0;
    end else if (auto_trace_legacy_in_0_valid) begin
      ingress_ingressPriv <= auto_trace_legacy_in_0_priv;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ingress_ingressValid <= 1'h0;
    end else begin
      ingress_ingressValid <= auto_trace_legacy_in_0_valid;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ingress_ingressInsn <= 32'h0;
    end else if (auto_trace_legacy_in_0_valid) begin
      ingress_ingressInsn <= auto_trace_legacy_in_0_insn;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ingress_ingressIaddr <= 40'h0;
    end else if (auto_trace_legacy_in_0_valid) begin
      ingress_ingressIaddr <= auto_trace_legacy_in_0_iaddr;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ingress_ingressException <= 1'h0;
    end else if (auto_trace_legacy_in_0_valid) begin
      ingress_ingressException <= auto_trace_legacy_in_0_exception;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ingress_ingressInterrupt <= 1'h0;
    end else if (auto_trace_legacy_in_0_valid) begin
      ingress_ingressInterrupt <= auto_trace_legacy_in_0_interrupt;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teActiveCore_teActiveCore <= 1'h0;
    end else begin
      teActiveCore_teActiveCore <= io_teActive;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pcActiveCore_pcActiveCore <= 1'h0;
    end else begin
      pcActiveCore_pcActiveCore <= io_pcActive;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teIdleCore_teIdleCore <= 1'h0;
    end else begin
      teIdleCore_teIdleCore <= io_teIdle;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      ingControlCoreReg_teEnable <= 1'h0;
    end else if (~teActiveCore_teActiveCore) begin
      ingControlCoreReg_teEnable <= 1'h0;
    end else if (ingControlCoreIn_valid) begin
      ingControlCoreReg_teEnable <= ingControlCoreIn_bits_teEnable;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      ingControlCoreReg_teTracing <= 1'h0;
    end else if (~teActiveCore_teActiveCore) begin
      ingControlCoreReg_teTracing <= 1'h0;
    end else if (ingControlCoreIn_valid) begin
      ingControlCoreReg_teTracing <= ingControlCoreIn_bits_teTracing;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      ingControlCoreReg_teInstruction <= 3'h0;
    end else if (~teActiveCore_teActiveCore) begin
      ingControlCoreReg_teInstruction <= 3'h0;
    end else if (ingControlCoreIn_valid) begin
      ingControlCoreReg_teInstruction <= ingControlCoreIn_bits_teInstruction;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      ingControlCoreReg_teStallEnable <= 1'h0;
    end else if (~teActiveCore_teActiveCore) begin
      ingControlCoreReg_teStallEnable <= 1'h0;
    end else if (ingControlCoreIn_valid) begin
      ingControlCoreReg_teStallEnable <= ingControlCoreIn_bits_teStallEnable;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      ingControlCoreReg_teSyncMaxInst <= 4'h0;
    end else if (~teActiveCore_teActiveCore) begin
      ingControlCoreReg_teSyncMaxInst <= 4'h0;
    end else if (ingControlCoreIn_valid) begin
      ingControlCoreReg_teSyncMaxInst <= ingControlCoreIn_bits_teSyncMaxInst;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      ingControlCoreReg_teSyncMaxBTM <= 4'h0;
    end else if (~teActiveCore_teActiveCore) begin
      ingControlCoreReg_teSyncMaxBTM <= 4'h0;
    end else if (ingControlCoreIn_valid) begin
      ingControlCoreReg_teSyncMaxBTM <= ingControlCoreIn_bits_teSyncMaxBTM;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      ingControlCore_valid_REG <= 1'h0;
    end else begin
      ingControlCore_valid_REG <= ingControlCoreIn_sink_io_deq_valid;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      inReset_r <= 1'h0;
    end else begin
      inReset_r <= io_hartIsInReset;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      inReset_r_1 <= 1'h0;
    end else begin
      inReset_r_1 <= inReset_r;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      inReset <= 1'h0;
    end else begin
      inReset <= inReset_r_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      iValid0 <= 1'h0;
    end else begin
      iValid0 <= rawRetire_0 | ingress_group_0_itype != 4'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      inGroupReg_0_iretire <= 2'h0;
    end else if (rawValid_0) begin
      if (ingress_ingressValid) begin
        if (ingress_ingressException) begin
          inGroupReg_0_iretire <= 2'h0;
        end else if (ingress_group_0_ilastsize) begin
          inGroupReg_0_iretire <= 2'h2;
        end else begin
          inGroupReg_0_iretire <= 2'h1;
        end
      end else begin
        inGroupReg_0_iretire <= 2'h0;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      inGroupReg_0_iaddr <= 40'h0;
    end else if (rawValid_0) begin
      inGroupReg_0_iaddr <= ingress_ingressIaddr;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      inGroupReg_0_itype <= 4'h0;
    end else if (rawValid_0) begin
      if (ingress_ingressValid) begin
        if (ingress_ingressException) begin
          inGroupReg_0_itype <= {{2'd0}, _ingress_tci_group_0_itype_T};
        end else if (_ingress_tci_group_0_itype_T_1) begin
          inGroupReg_0_itype <= 4'h9;
        end else begin
          inGroupReg_0_itype <= _ingress_tci_group_0_itype_T_26;
        end
      end else begin
        inGroupReg_0_itype <= 4'h0;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      inGroupReg_0_ilastsize <= 1'h0;
    end else if (rawValid_0) begin
      inGroupReg_0_ilastsize <= ingress_group_0_ilastsize;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      inDebug <= 1'h0;
    end else if (_T) begin
      inDebug <= 1'h0;
    end else if (rawValid_0) begin
      inDebug <= ingress_priv[2];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      inRetire_0 <= 1'h0;
    end else begin
      inRetire_0 <= |ingress_group_0_iretire;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      inGroupPA_0 <= 39'h0;
    end else if (rawValid_0) begin
      inGroupPA_0 <= paddr_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      ingressIaddrValid <= 1'h0;
    end else if (inReset) begin
      ingressIaddrValid <= 1'h0;
    end else begin
      ingressIaddrValid <= _GEN_376;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      pcSampleCoreReg <= 40'h0;
    end else if (inRetire_0) begin
      pcSampleCoreReg <= lastExecuted;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pcSampleCoreValid <= 1'h0;
    end else if (~pcActiveCore_pcActiveCore) begin
      pcSampleCoreValid <= 1'h0;
    end else begin
      pcSampleCoreValid <= inRetire_0 | pcSampleCoreValid & ~pcSampleCore_ready;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      pcCaptureCoreReg <= 40'h0;
    end else if (_pcCaptureCoreReg_T) begin
      pcCaptureCoreReg <= pcSampleCoreReg;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pcCaptureCoreValid <= 1'h0;
    end else if (~pcActiveCore_pcActiveCore) begin
      pcCaptureCoreValid <= 1'h0;
    end else begin
      pcCaptureCoreValid <= _pcCaptureCoreReg_T | pcCaptureCoreValid & ~pcCaptureCore_ready;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      btmEvent_0_valid <= 1'h0;
    end else if (_T_6) begin
      btmEvent_0_valid <= 1'h0;
    end else if (ingControlCoreReg_teInstruction == 3'h0) begin
      btmEvent_0_valid <= 1'h0;
    end else if (ovfState[0]) begin
      btmEvent_0_valid <= 1'h0;
    end else if (ovfState[1]) begin
      btmEvent_0_valid <= 1'h0;
    end else begin
      btmEvent_0_valid <= _GEN_214;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      btmEvent_0_bits_reason <= 5'h0;
    end else if (_T_6) begin
      btmEvent_0_bits_reason <= 5'h1f;
    end else if (ovfState[0]) begin
      btmEvent_0_bits_reason <= 5'h1f;
    end else if (ovfState[1]) begin
      btmEvent_0_bits_reason <= 5'h1f;
    end else if (syncOnly) begin
      btmEvent_0_bits_reason <= 5'h1f;
    end else begin
      btmEvent_0_bits_reason <= _GEN_178;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      btmEvent_0_bits_iaddr <= 39'h0;
    end else begin
      btmEvent_0_bits_iaddr <= _btmEvent_0_bits_iaddr_T[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      btmEvent_0_bits_icnt <= 14'h0;
    end else if (_T_6) begin
      btmEvent_0_bits_icnt <= lastCycle_icnt;
    end else if (ovfState[0]) begin
      btmEvent_0_bits_icnt <= lastCycle_icnt;
    end else if (ovfState[1]) begin
      btmEvent_0_bits_icnt <= lastCycle_icnt;
    end else if (syncOnly) begin
      btmEvent_0_bits_icnt <= lastCycle_icnt;
    end else begin
      btmEvent_0_bits_icnt <= _GEN_180;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      btmEvent_0_bits_hist <= 32'h0;
    end else if (stateA_0_useHcnt) begin
      btmEvent_0_bits_hist <= {{19'd0}, lastCycle_hcnt};
    end else begin
      btmEvent_0_bits_hist <= lastCycle_hist;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      btmEvent_0_bits_hisv <= 2'h0;
    end else if (stateA_0_useHcnt) begin
      btmEvent_0_bits_hisv <= lastCycle_hisv;
    end else begin
      btmEvent_0_bits_hisv <= _btmEvent_0_bits_hisv_T;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      btmEvent_1_valid <= 1'h0;
    end else if (_T_6) begin
      btmEvent_1_valid <= 1'h0;
    end else if (ingControlCoreReg_teInstruction == 3'h0) begin
      btmEvent_1_valid <= 1'h0;
    end else if (ovfState[0]) begin
      btmEvent_1_valid <= 1'h0;
    end else if (ovfState[1]) begin
      btmEvent_1_valid <= 1'h0;
    end else begin
      btmEvent_1_valid <= _GEN_228;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      btmEvent_1_bits_reason <= 5'h0;
    end else if (_T_6) begin
      btmEvent_1_bits_reason <= 5'h1f;
    end else if (ovfState[0]) begin
      btmEvent_1_bits_reason <= 5'h1f;
    end else if (ovfState[1]) begin
      btmEvent_1_bits_reason <= 5'h1f;
    end else if (syncOnly) begin
      btmEvent_1_bits_reason <= 5'h1f;
    end else begin
      btmEvent_1_bits_reason <= _GEN_192;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      btmEvent_1_bits_iaddr <= 39'h0;
    end else begin
      btmEvent_1_bits_iaddr <= _GEN_353[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      btmEvent_1_bits_icnt <= 14'h0;
    end else if (_T_6) begin
      btmEvent_1_bits_icnt <= lastCycle_icnt;
    end else if (ovfState[0]) begin
      btmEvent_1_bits_icnt <= lastCycle_icnt;
    end else if (ovfState[1]) begin
      btmEvent_1_bits_icnt <= lastCycle_icnt;
    end else if (syncOnly) begin
      btmEvent_1_bits_icnt <= lastCycle_icnt;
    end else begin
      btmEvent_1_bits_icnt <= _GEN_181;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      btmEvent_1_bits_hist <= 32'h0;
    end else if (stateA_1_useHcnt) begin
      btmEvent_1_bits_hist <= {{19'd0}, stateZ_0_hcnt};
    end else if (_T_6) begin
      btmEvent_1_bits_hist <= lastCycle_hist;
    end else if (ovfState[0]) begin
      btmEvent_1_bits_hist <= lastCycle_hist;
    end else if (ovfState[1]) begin
      btmEvent_1_bits_hist <= lastCycle_hist;
    end else begin
      btmEvent_1_bits_hist <= _GEN_219;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      btmEvent_1_bits_hisv <= 2'h0;
    end else if (stateA_1_useHcnt) begin
      if (_T_6) begin
        btmEvent_1_bits_hisv <= lastCycle_hisv;
      end else if (ovfState[0]) begin
        btmEvent_1_bits_hisv <= lastCycle_hisv;
      end else if (ovfState[1]) begin
        btmEvent_1_bits_hisv <= lastCycle_hisv;
      end else begin
        btmEvent_1_bits_hisv <= _GEN_221;
      end
    end else begin
      btmEvent_1_bits_hisv <= _btmEvent_1_bits_hisv_T;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      lastCycle_itype <= 4'h0;
    end else if (_T) begin
      lastCycle_itype <= 4'h0;
    end else if (~stateZ_0_gapReason[4]) begin
      lastCycle_itype <= 4'h0;
    end else if (iValid0) begin
      lastCycle_itype <= inGroupReg_0_itype;
    end else if (sendingGap) begin
      lastCycle_itype <= 4'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      lastCycle_iaddr <= 39'h0;
    end else if (_T) begin
      lastCycle_iaddr <= 39'h0;
    end else if (!(_T_6)) begin
      if (!(ovfState[0])) begin
        if (!(ovfState[1])) begin
          lastCycle_iaddr <= _GEN_223;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      lastCycle_icnt <= 14'h0;
    end else if (_T) begin
      lastCycle_icnt <= 14'h0;
    end else if (_T_6) begin
      lastCycle_icnt <= stateZ_0_icnt;
    end else if (ovfState[0]) begin
      lastCycle_icnt <= stateZ_0_icnt;
    end else if (ovfState[1]) begin
      lastCycle_icnt <= stateZ_0_icnt;
    end else begin
      lastCycle_icnt <= _GEN_233;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      lastCycle_hist <= 32'h0;
    end else if (_T) begin
      lastCycle_hist <= 32'h0;
    end else if (_T_6) begin
      lastCycle_hist <= stateZ_0_hist;
    end else if (ovfState[0]) begin
      lastCycle_hist <= stateZ_0_hist;
    end else if (ovfState[1]) begin
      lastCycle_hist <= stateZ_0_hist;
    end else begin
      lastCycle_hist <= _GEN_230;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      lastCycle_hisv <= 2'h0;
    end else if (_T) begin
      lastCycle_hisv <= 2'h0;
    end else if (_T_6) begin
      lastCycle_hisv <= stateZ_0_hisv;
    end else if (ovfState[0]) begin
      lastCycle_hisv <= stateZ_0_hisv;
    end else if (ovfState[1]) begin
      lastCycle_hisv <= stateZ_0_hisv;
    end else begin
      lastCycle_hisv <= _GEN_231;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      lastCycle_hcnt <= 13'h0;
    end else if (_T) begin
      lastCycle_hcnt <= 13'h0;
    end else if (_T_6) begin
      lastCycle_hcnt <= stateZ_0_hcnt;
    end else if (ovfState[0]) begin
      lastCycle_hcnt <= stateZ_0_hcnt;
    end else if (ovfState[1]) begin
      lastCycle_hcnt <= stateZ_0_hcnt;
    end else begin
      lastCycle_hcnt <= _GEN_232;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      lastCycle_ras <= 16'h0;
    end else if (_T) begin
      lastCycle_ras <= 16'h0;
    end else if (!(_T_6)) begin
      if (!(ovfState[0])) begin
        if (!(ovfState[1])) begin
          lastCycle_ras <= _GEN_222;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      lastCycle_gapReason <= 5'h0;
    end else if (_T) begin
      lastCycle_gapReason <= 5'h0;
    end else if (_T_6) begin
      lastCycle_gapReason <= 5'h5;
    end else if (ovfState[0]) begin
      if (!(_T_6)) begin
        lastCycle_gapReason <= _GEN_290;
      end
    end else if (ovfState[1]) begin
      lastCycle_gapReason <= 5'h7;
    end else begin
      lastCycle_gapReason <= _GEN_204;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      lastCycle_tracing <= 1'h0;
    end else if (_T) begin
      lastCycle_tracing <= 1'h0;
    end else if (_T_6) begin
      lastCycle_tracing <= stateZ_0_tracing;
    end else if (ovfState[0]) begin
      lastCycle_tracing <= _GEN_38;
    end else if (ovfState[1]) begin
      lastCycle_tracing <= _GEN_38;
    end else begin
      lastCycle_tracing <= _GEN_227;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      lastCycle_wpsync <= 1'h0;
    end else if (_T) begin
      lastCycle_wpsync <= 1'h0;
    end else begin
      lastCycle_wpsync <= stateZ_1_wpsync & stateZ_1_tracing;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ovfState <= 2'h0;
    end else if (_T) begin
      ovfState <= 2'h0;
    end else if (_T_6) begin
      ovfState <= 2'h0;
    end else if (btmEventFifo_0_valid & ~btmEventFifo_0_ready | btmEventFifo_1_valid & ~btmEventFifo_1_ready) begin
      ovfState <= 2'h1;
    end else if (ovfState[0] & btmEventFifo_1_ready) begin
      ovfState <= 2'h2;
    end else begin
      ovfState <= _GEN_315;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      btmCount <= 17'h0;
    end else if (_T) begin
      btmCount <= 17'h0;
    end else if (stateZ_0_syncSent) begin
      btmCount <= 17'h0;
    end else begin
      btmCount <= btmCountNext;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      btmCountFull <= 1'h0;
    end else if (_T) begin
      btmCountFull <= 1'h0;
    end else if (stateZ_0_syncSent) begin
      btmCountFull <= 1'h0;
    end else begin
      btmCountFull <= _btmCountFull_T_2[0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      scnt <= 14'h0;
    end else if (_T | inDebug | ~ingressIaddrValid) begin
      scnt <= 14'h0;
    end else if (maxscnt) begin
      scnt <= 14'h1;
    end else begin
      scnt <= _scnt_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifoCount <= 4'h0;
    end else if (_T) begin
      fifoCount <= 4'h0;
    end else begin
      fifoCount <= _fifoCount_T_8;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ingress_ingressPriv = _RAND_0[2:0];
  _RAND_1 = {1{`RANDOM}};
  ingress_ingressValid = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  ingress_ingressInsn = _RAND_2[31:0];
  _RAND_3 = {2{`RANDOM}};
  ingress_ingressIaddr = _RAND_3[39:0];
  _RAND_4 = {1{`RANDOM}};
  ingress_ingressException = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  ingress_ingressInterrupt = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  teActiveCore_teActiveCore = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  pcActiveCore_pcActiveCore = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  teIdleCore_teIdleCore = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  ingControlCoreReg_teEnable = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  ingControlCoreReg_teTracing = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  ingControlCoreReg_teInstruction = _RAND_11[2:0];
  _RAND_12 = {1{`RANDOM}};
  ingControlCoreReg_teStallEnable = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  ingControlCoreReg_teSyncMaxInst = _RAND_13[3:0];
  _RAND_14 = {1{`RANDOM}};
  ingControlCoreReg_teSyncMaxBTM = _RAND_14[3:0];
  _RAND_15 = {1{`RANDOM}};
  ingControlCore_valid_REG = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  inReset_r = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  inReset_r_1 = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  inReset = _RAND_18[0:0];
  _RAND_19 = {1{`RANDOM}};
  iValid0 = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  inGroupReg_0_iretire = _RAND_20[1:0];
  _RAND_21 = {2{`RANDOM}};
  inGroupReg_0_iaddr = _RAND_21[39:0];
  _RAND_22 = {1{`RANDOM}};
  inGroupReg_0_itype = _RAND_22[3:0];
  _RAND_23 = {1{`RANDOM}};
  inGroupReg_0_ilastsize = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  inDebug = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  inRetire_0 = _RAND_25[0:0];
  _RAND_26 = {2{`RANDOM}};
  inGroupPA_0 = _RAND_26[38:0];
  _RAND_27 = {1{`RANDOM}};
  ingressIaddrValid = _RAND_27[0:0];
  _RAND_28 = {2{`RANDOM}};
  pcSampleCoreReg = _RAND_28[39:0];
  _RAND_29 = {1{`RANDOM}};
  pcSampleCoreValid = _RAND_29[0:0];
  _RAND_30 = {2{`RANDOM}};
  pcCaptureCoreReg = _RAND_30[39:0];
  _RAND_31 = {1{`RANDOM}};
  pcCaptureCoreValid = _RAND_31[0:0];
  _RAND_32 = {1{`RANDOM}};
  btmEvent_0_valid = _RAND_32[0:0];
  _RAND_33 = {1{`RANDOM}};
  btmEvent_0_bits_reason = _RAND_33[4:0];
  _RAND_34 = {2{`RANDOM}};
  btmEvent_0_bits_iaddr = _RAND_34[38:0];
  _RAND_35 = {1{`RANDOM}};
  btmEvent_0_bits_icnt = _RAND_35[13:0];
  _RAND_36 = {1{`RANDOM}};
  btmEvent_0_bits_hist = _RAND_36[31:0];
  _RAND_37 = {1{`RANDOM}};
  btmEvent_0_bits_hisv = _RAND_37[1:0];
  _RAND_38 = {1{`RANDOM}};
  btmEvent_1_valid = _RAND_38[0:0];
  _RAND_39 = {1{`RANDOM}};
  btmEvent_1_bits_reason = _RAND_39[4:0];
  _RAND_40 = {2{`RANDOM}};
  btmEvent_1_bits_iaddr = _RAND_40[38:0];
  _RAND_41 = {1{`RANDOM}};
  btmEvent_1_bits_icnt = _RAND_41[13:0];
  _RAND_42 = {1{`RANDOM}};
  btmEvent_1_bits_hist = _RAND_42[31:0];
  _RAND_43 = {1{`RANDOM}};
  btmEvent_1_bits_hisv = _RAND_43[1:0];
  _RAND_44 = {1{`RANDOM}};
  lastCycle_itype = _RAND_44[3:0];
  _RAND_45 = {2{`RANDOM}};
  lastCycle_iaddr = _RAND_45[38:0];
  _RAND_46 = {1{`RANDOM}};
  lastCycle_icnt = _RAND_46[13:0];
  _RAND_47 = {1{`RANDOM}};
  lastCycle_hist = _RAND_47[31:0];
  _RAND_48 = {1{`RANDOM}};
  lastCycle_hisv = _RAND_48[1:0];
  _RAND_49 = {1{`RANDOM}};
  lastCycle_hcnt = _RAND_49[12:0];
  _RAND_50 = {1{`RANDOM}};
  lastCycle_ras = _RAND_50[15:0];
  _RAND_51 = {1{`RANDOM}};
  lastCycle_gapReason = _RAND_51[4:0];
  _RAND_52 = {1{`RANDOM}};
  lastCycle_tracing = _RAND_52[0:0];
  _RAND_53 = {1{`RANDOM}};
  lastCycle_wpsync = _RAND_53[0:0];
  _RAND_54 = {1{`RANDOM}};
  ovfState = _RAND_54[1:0];
  _RAND_55 = {1{`RANDOM}};
  btmCount = _RAND_55[16:0];
  _RAND_56 = {1{`RANDOM}};
  btmCountFull = _RAND_56[0:0];
  _RAND_57 = {1{`RANDOM}};
  scnt = _RAND_57[13:0];
  _RAND_58 = {1{`RANDOM}};
  fifoCount = _RAND_58[3:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ingress_ingressPriv = 3'h0;
  end
  if (rf_reset) begin
    ingress_ingressValid = 1'h0;
  end
  if (rf_reset) begin
    ingress_ingressInsn = 32'h0;
  end
  if (rf_reset) begin
    ingress_ingressIaddr = 40'h0;
  end
  if (rf_reset) begin
    ingress_ingressException = 1'h0;
  end
  if (rf_reset) begin
    ingress_ingressInterrupt = 1'h0;
  end
  if (reset) begin
    teActiveCore_teActiveCore = 1'h0;
  end
  if (reset) begin
    pcActiveCore_pcActiveCore = 1'h0;
  end
  if (reset) begin
    teIdleCore_teIdleCore = 1'h0;
  end
  if (reset) begin
    ingControlCoreReg_teEnable = 1'h0;
  end
  if (reset) begin
    ingControlCoreReg_teTracing = 1'h0;
  end
  if (reset) begin
    ingControlCoreReg_teInstruction = 3'h0;
  end
  if (reset) begin
    ingControlCoreReg_teStallEnable = 1'h0;
  end
  if (reset) begin
    ingControlCoreReg_teSyncMaxInst = 4'h0;
  end
  if (reset) begin
    ingControlCoreReg_teSyncMaxBTM = 4'h0;
  end
  if (reset) begin
    ingControlCore_valid_REG = 1'h0;
  end
  if (rf_reset) begin
    inReset_r = 1'h0;
  end
  if (rf_reset) begin
    inReset_r_1 = 1'h0;
  end
  if (rf_reset) begin
    inReset = 1'h0;
  end
  if (rf_reset) begin
    iValid0 = 1'h0;
  end
  if (rf_reset) begin
    inGroupReg_0_iretire = 2'h0;
  end
  if (rf_reset) begin
    inGroupReg_0_iaddr = 40'h0;
  end
  if (rf_reset) begin
    inGroupReg_0_itype = 4'h0;
  end
  if (rf_reset) begin
    inGroupReg_0_ilastsize = 1'h0;
  end
  if (rf_reset) begin
    inDebug = 1'h0;
  end
  if (rf_reset) begin
    inRetire_0 = 1'h0;
  end
  if (rf_reset) begin
    inGroupPA_0 = 39'h0;
  end
  if (reset) begin
    ingressIaddrValid = 1'h0;
  end
  if (rf_reset) begin
    pcSampleCoreReg = 40'h0;
  end
  if (reset) begin
    pcSampleCoreValid = 1'h0;
  end
  if (rf_reset) begin
    pcCaptureCoreReg = 40'h0;
  end
  if (reset) begin
    pcCaptureCoreValid = 1'h0;
  end
  if (rf_reset) begin
    btmEvent_0_valid = 1'h0;
  end
  if (rf_reset) begin
    btmEvent_0_bits_reason = 5'h0;
  end
  if (rf_reset) begin
    btmEvent_0_bits_iaddr = 39'h0;
  end
  if (rf_reset) begin
    btmEvent_0_bits_icnt = 14'h0;
  end
  if (rf_reset) begin
    btmEvent_0_bits_hist = 32'h0;
  end
  if (rf_reset) begin
    btmEvent_0_bits_hisv = 2'h0;
  end
  if (rf_reset) begin
    btmEvent_1_valid = 1'h0;
  end
  if (rf_reset) begin
    btmEvent_1_bits_reason = 5'h0;
  end
  if (rf_reset) begin
    btmEvent_1_bits_iaddr = 39'h0;
  end
  if (rf_reset) begin
    btmEvent_1_bits_icnt = 14'h0;
  end
  if (rf_reset) begin
    btmEvent_1_bits_hist = 32'h0;
  end
  if (rf_reset) begin
    btmEvent_1_bits_hisv = 2'h0;
  end
  if (rf_reset) begin
    lastCycle_itype = 4'h0;
  end
  if (rf_reset) begin
    lastCycle_iaddr = 39'h0;
  end
  if (rf_reset) begin
    lastCycle_icnt = 14'h0;
  end
  if (rf_reset) begin
    lastCycle_hist = 32'h0;
  end
  if (rf_reset) begin
    lastCycle_hisv = 2'h0;
  end
  if (rf_reset) begin
    lastCycle_hcnt = 13'h0;
  end
  if (rf_reset) begin
    lastCycle_ras = 16'h0;
  end
  if (rf_reset) begin
    lastCycle_gapReason = 5'h0;
  end
  if (rf_reset) begin
    lastCycle_tracing = 1'h0;
  end
  if (rf_reset) begin
    lastCycle_wpsync = 1'h0;
  end
  if (rf_reset) begin
    ovfState = 2'h0;
  end
  if (rf_reset) begin
    btmCount = 17'h0;
  end
  if (rf_reset) begin
    btmCountFull = 1'h0;
  end
  if (rf_reset) begin
    scnt = 14'h0;
  end
  if (reset) begin
    fifoCount = 4'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
