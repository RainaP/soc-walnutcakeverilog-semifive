//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TracePIBOuter(
  input         clock,
  input         reset,
  input  [12:0] io_pibControlSync_pibDivider,
  input         io_pibControlSync_pibCalibrate,
  input         io_pibControlSync_pibRefCenter,
  input  [3:0]  io_pibControlSync_pibMode,
  input         io_pibControlSync_pibEnable,
  input         io_pibControlSync_pibActive,
  output        io_streamIn_ready,
  input         io_streamIn_valid,
  input  [31:0] io_streamIn_bits,
  output        io_pib_tref,
  output [3:0]  io_pib_tdata,
  output        io_pibEmpty
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [223:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
`endif // RANDOMIZE_REG_INIT
  wire  rf_reset;
  wire  unpacker_rf_reset; // @[TracePIB.scala 307:29]
  wire  unpacker_clock; // @[TracePIB.scala 307:29]
  wire  unpacker_reset; // @[TracePIB.scala 307:29]
  wire  unpacker_io_teActive; // @[TracePIB.scala 307:29]
  wire  unpacker_io_stream_ready; // @[TracePIB.scala 307:29]
  wire  unpacker_io_stream_valid; // @[TracePIB.scala 307:29]
  wire [31:0] unpacker_io_stream_bits; // @[TracePIB.scala 307:29]
  wire  unpacker_io_btm_ready; // @[TracePIB.scala 307:29]
  wire  unpacker_io_btm_valid; // @[TracePIB.scala 307:29]
  wire [4:0] unpacker_io_btm_bits_length; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_0_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_0_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_1_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_1_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_2_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_2_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_3_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_3_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_4_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_4_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_5_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_5_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_6_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_6_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_7_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_7_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_8_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_8_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_9_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_9_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_10_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_10_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_11_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_11_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_12_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_12_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_13_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_13_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_14_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_14_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_15_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_15_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_16_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_16_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_17_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_17_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_18_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_18_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_19_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_19_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_20_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_20_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_21_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_21_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_22_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_22_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_23_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_23_mseo; // @[TracePIB.scala 307:29]
  wire [5:0] unpacker_io_btm_bits_slices_24_data; // @[TracePIB.scala 307:29]
  wire [1:0] unpacker_io_btm_bits_slices_24_mseo; // @[TracePIB.scala 307:29]
  reg [12:0] divcount; // @[TracePIB.scala 268:27]
  reg [1:0] pibref; // @[Reg.scala 27:20]
  wire  pibdiv = divcount >= io_pibControlSync_pibDivider; // @[TracePIB.scala 275:23]
  wire  _T_4 = ~io_pibControlSync_pibActive; // @[TracePIB.scala 284:9]
  wire [1:0] _pibrefNext_T = io_pibControlSync_pibRefCenter ? 2'h1 : 2'h2; // @[TracePIB.scala 287:31]
  wire [1:0] _pibrefNext_T_2 = pibref + _pibrefNext_T; // @[TracePIB.scala 287:26]
  wire [1:0] _GEN_3 = io_pibControlSync_pibMode[3] ? _pibrefNext_T_2 : 2'h0; // @[TracePIB.scala 286:45 TracePIB.scala 287:16]
  wire [1:0] pibrefNext = ~io_pibControlSync_pibActive ? 2'h0 : _GEN_3; // @[TracePIB.scala 284:39 TracePIB.scala 285:16]
  reg [3:0] tdata; // @[TracePIB.scala 272:23]
  reg  pibEmpty; // @[TracePIB.scala 273:27]
  wire [12:0] _divcount_T_1 = divcount + 13'h1; // @[TracePIB.scala 281:26]
  wire  pibdivX = io_pibControlSync_pibRefCenter ? ~pibrefNext[0] & pibdiv : pibdiv; // @[TracePIB.scala 290:17]
  reg [3:0] pibSt; // @[Reg.scala 27:20]
  reg [199:0] pibSR; // @[Reg.scala 15:16]
  reg [3:0] framing; // @[Reg.scala 15:16]
  wire [199:0] _tdataX_T_1 = pibSt[3] ? pibSR : {{196'd0}, framing}; // @[TracePIB.scala 438:16]
  wire  _tdataX_T_4 = io_pibControlSync_pibMode[3] & io_pibControlSync_pibMode[1]; // @[TracePIB.scala 351:19]
  wire  _tdataX_T_6 = io_pibControlSync_pibMode[3] & io_pibControlSync_pibMode[1] & io_pibControlSync_pibMode[0]; // @[TracePIB.scala 351:33]
  wire  _tdataX_T_11 = ~io_pibControlSync_pibMode[0]; // @[TracePIB.scala 352:35]
  wire  _tdataX_T_12 = _tdataX_T_4 & ~io_pibControlSync_pibMode[0]; // @[TracePIB.scala 352:33]
  wire  _tdataX_T_18 = io_pibControlSync_pibMode[3] & ~io_pibControlSync_pibMode[1] & io_pibControlSync_pibMode[0]; // @[TracePIB.scala 353:33]
  wire [7:0] _tdataX_T_19 = _tdataX_T_18 ? 8'hfc : 8'hfe; // @[Mux.scala 98:16]
  wire [7:0] _tdataX_T_20 = _tdataX_T_12 ? 8'hf0 : _tdataX_T_19; // @[Mux.scala 98:16]
  wire [7:0] _tdataX_T_21 = _tdataX_T_6 ? 8'h0 : _tdataX_T_20; // @[Mux.scala 98:16]
  wire [199:0] _GEN_61 = {{192'd0}, _tdataX_T_21}; // @[TracePIB.scala 438:47]
  wire [199:0] _tdataX_T_22 = _tdataX_T_1 | _GEN_61; // @[TracePIB.scala 438:47]
  wire [3:0] tdataX = _tdataX_T_22[3:0]; // @[TracePIB.scala 263:24 TracePIB.scala 438:10]
  wire [3:0] _GEN_5 = pibdiv | ~io_pibControlSync_pibEnable ? tdataX : tdata; // @[TracePIB.scala 295:55 TracePIB.scala 296:11 TracePIB.scala 272:23]
  wire  _pibEmptyX_T_1 = pibSt == 4'h0; // @[TracePIB.scala 379:49]
  wire  pibEmptyX = ~unpacker_io_btm_valid & pibSt == 4'h0; // @[TracePIB.scala 379:39]
  wire  _GEN_6 = pibdiv | ~io_pibControlSync_pibEnable ? pibEmptyX : pibEmpty; // @[TracePIB.scala 295:55 TracePIB.scala 297:14 TracePIB.scala 273:27]
  wire [7:0] _GEN_7 = _T_4 ? 8'hff : {{4'd0}, _GEN_5}; // @[TracePIB.scala 292:39 TracePIB.scala 293:11]
  wire  _pibIdle_T_1 = pibSt == 4'h8; // @[TracePIB.scala 383:13]
  reg  pibLast; // @[Reg.scala 27:20]
  reg [2:0] pibcount; // @[Reg.scala 27:20]
  wire  _pibIdle_T_6 = ~(|pibcount); // @[TracePIB.scala 383:75]
  wire  _pibIdle_T_7 = pibSt == 4'h8 & io_pibControlSync_pibMode[3] & pibLast & ~(|pibcount); // @[TracePIB.scala 383:72]
  wire  _pibIdle_T_8 = _pibEmptyX_T_1 | _pibIdle_T_7; // @[TracePIB.scala 382:40]
  wire  _pibIdle_T_9 = pibSt == 4'h3; // @[TracePIB.scala 384:13]
  reg  pibLastUart; // @[Reg.scala 27:20]
  wire  _pibIdle_T_12 = pibSt == 4'h3 & io_pibControlSync_pibMode == 4'h5 & pibLastUart; // @[TracePIB.scala 384:68]
  wire  _pibIdle_T_13 = _pibIdle_T_8 | _pibIdle_T_12; // @[TracePIB.scala 383:90]
  wire  _pibIdle_T_14 = pibSt == 4'h4; // @[TracePIB.scala 385:13]
  wire  _pibIdle_T_15 = io_pibControlSync_pibMode == 4'h4; // @[TracePIB.scala 385:59]
  wire  _pibIdle_T_17 = pibSt == 4'h4 & io_pibControlSync_pibMode == 4'h4 & pibLastUart; // @[TracePIB.scala 385:68]
  wire  pibIdle = _pibIdle_T_13 | _pibIdle_T_17; // @[TracePIB.scala 384:84]
  reg [6:0] caltimer; // @[Reg.scala 27:20]
  wire  _pibGo_T = |caltimer; // @[TracePIB.scala 378:93]
  wire  pibGo = io_pibControlSync_pibCalibrate ? io_pibControlSync_pibEnable & ~(|caltimer) : unpacker_io_btm_valid; // @[TracePIB.scala 378:27]
  wire  _pibSRNext_T = io_pibControlSync_pibMode == 4'ha; // @[TracePIB.scala 359:16]
  wire  _pibSRNext_T_1 = io_pibControlSync_pibMode == 4'h9; // @[TracePIB.scala 360:16]
  wire [31:0] _pibSRNext_T_2 = _pibSRNext_T_1 ? 32'h33cc6666 : 32'hff0055aa; // @[Mux.scala 98:16]
  wire [31:0] _pibSRNext_T_3 = _pibSRNext_T ? 32'hff05a5a : _pibSRNext_T_2; // @[Mux.scala 98:16]
  wire [23:0] pibSRNext_hi_hi_lo = {unpacker_io_btm_bits_slices_21_mseo,unpacker_io_btm_bits_slices_20_data,
    unpacker_io_btm_bits_slices_20_mseo,unpacker_io_btm_bits_slices_19_data,unpacker_io_btm_bits_slices_19_mseo,
    unpacker_io_btm_bits_slices_18_data}; // @[TracePIB.scala 395:43]
  wire [23:0] pibSRNext_hi_lo_lo = {unpacker_io_btm_bits_slices_15_mseo,unpacker_io_btm_bits_slices_14_data,
    unpacker_io_btm_bits_slices_14_mseo,unpacker_io_btm_bits_slices_13_data,unpacker_io_btm_bits_slices_13_mseo,
    unpacker_io_btm_bits_slices_12_data}; // @[TracePIB.scala 395:43]
  wire [47:0] pibSRNext_hi_lo = {unpacker_io_btm_bits_slices_18_mseo,unpacker_io_btm_bits_slices_17_data,
    unpacker_io_btm_bits_slices_17_mseo,unpacker_io_btm_bits_slices_16_data,unpacker_io_btm_bits_slices_16_mseo,
    unpacker_io_btm_bits_slices_15_data,pibSRNext_hi_lo_lo}; // @[TracePIB.scala 395:43]
  wire [23:0] pibSRNext_lo_hi_lo = {unpacker_io_btm_bits_slices_8_data,unpacker_io_btm_bits_slices_8_mseo,
    unpacker_io_btm_bits_slices_7_data,unpacker_io_btm_bits_slices_7_mseo,unpacker_io_btm_bits_slices_6_data,
    unpacker_io_btm_bits_slices_6_mseo}; // @[TracePIB.scala 395:43]
  wire [23:0] pibSRNext_lo_lo_lo = {unpacker_io_btm_bits_slices_2_data,unpacker_io_btm_bits_slices_2_mseo,
    unpacker_io_btm_bits_slices_1_data,unpacker_io_btm_bits_slices_1_mseo,unpacker_io_btm_bits_slices_0_data,
    unpacker_io_btm_bits_slices_0_mseo}; // @[TracePIB.scala 395:43]
  wire [47:0] pibSRNext_lo_lo = {unpacker_io_btm_bits_slices_5_data,unpacker_io_btm_bits_slices_5_mseo,
    unpacker_io_btm_bits_slices_4_data,unpacker_io_btm_bits_slices_4_mseo,unpacker_io_btm_bits_slices_3_data,
    unpacker_io_btm_bits_slices_3_mseo,pibSRNext_lo_lo_lo}; // @[TracePIB.scala 395:43]
  wire [97:0] pibSRNext_lo = {unpacker_io_btm_bits_slices_12_mseo,unpacker_io_btm_bits_slices_11_data,
    unpacker_io_btm_bits_slices_11_mseo,unpacker_io_btm_bits_slices_10_data,unpacker_io_btm_bits_slices_10_mseo,
    unpacker_io_btm_bits_slices_9_data,unpacker_io_btm_bits_slices_9_mseo,pibSRNext_lo_hi_lo,pibSRNext_lo_lo}; // @[TracePIB.scala 395:43]
  wire [199:0] _pibSRNext_T_4 = {unpacker_io_btm_bits_slices_24_data,unpacker_io_btm_bits_slices_24_mseo,
    unpacker_io_btm_bits_slices_23_data,unpacker_io_btm_bits_slices_23_mseo,unpacker_io_btm_bits_slices_22_data,
    unpacker_io_btm_bits_slices_22_mseo,unpacker_io_btm_bits_slices_21_data,pibSRNext_hi_hi_lo,pibSRNext_hi_lo,
    pibSRNext_lo}; // @[TracePIB.scala 395:43]
  wire [199:0] _pibSRNext_T_5 = io_pibControlSync_pibCalibrate ? {{168'd0}, _pibSRNext_T_3} : _pibSRNext_T_4; // @[TracePIB.scala 394:21]
  wire [199:0] _pibSRNext_T_11 = {{8'd0}, pibSR[199:8]}; // @[TracePIB.scala 335:54]
  wire [199:0] _pibSRNext_T_18 = {{4'd0}, pibSR[199:4]}; // @[TracePIB.scala 336:54]
  wire [199:0] _pibSRNext_T_25 = {{2'd0}, pibSR[199:2]}; // @[TracePIB.scala 337:54]
  wire [199:0] pibSRNext_x2 = {{1'd0}, pibSR[199:1]}; // @[TracePIB.scala 338:23]
  wire [199:0] _pibSRNext_T_26 = _tdataX_T_18 ? _pibSRNext_T_25 : pibSRNext_x2; // @[Mux.scala 98:16]
  wire [199:0] _pibSRNext_T_27 = _tdataX_T_12 ? _pibSRNext_T_18 : _pibSRNext_T_26; // @[Mux.scala 98:16]
  wire [199:0] _pibSRNext_T_28 = _tdataX_T_6 ? _pibSRNext_T_11 : _pibSRNext_T_27; // @[Mux.scala 98:16]
  wire [199:0] _GEN_23 = _pibIdle_T_1 & _pibIdle_T_15 ? pibSR : _pibSRNext_T_28; // @[TracePIB.scala 410:74 TracePIB.scala 368:13 TracePIB.scala 414:17]
  wire [199:0] _GEN_32 = _pibIdle_T_1 | pibSt == 4'h5 ? _GEN_23 : pibSR; // @[TracePIB.scala 409:72 TracePIB.scala 368:13]
  wire [199:0] _GEN_37 = pibSt == 4'h2 ? pibSR : _GEN_32; // @[TracePIB.scala 406:41 TracePIB.scala 368:13]
  wire [199:0] _GEN_42 = pibSt == 4'h1 ? pibSR : _GEN_37; // @[TracePIB.scala 402:41 TracePIB.scala 368:13]
  wire [199:0] _GEN_45 = pibIdle & pibGo ? _pibSRNext_T_5 : _GEN_42; // @[TracePIB.scala 393:32 TracePIB.scala 394:15]
  wire [199:0] pibSRNext = pibIdle & ~pibGo ? pibSR : _GEN_45; // @[TracePIB.scala 387:28 TracePIB.scala 368:13]
  wire [3:0] _pibStNext_T_1 = io_pibControlSync_pibMode[3] ? 4'h8 : 4'h1; // @[TracePIB.scala 400:21]
  wire [3:0] _pibStNext_T_3 = io_pibControlSync_pibMode[0] ? 4'h8 : 4'h2; // @[TracePIB.scala 404:21]
  wire [3:0] _pibStNext_T_10 = io_pibControlSync_pibMode[3] | _tdataX_T_11 & ~pibLast ? 4'h8 : 4'h3; // @[TracePIB.scala 421:25]
  wire [3:0] _GEN_20 = _pibIdle_T_6 ? _pibStNext_T_10 : 4'h8; // @[TracePIB.scala 417:28 TracePIB.scala 421:19 TracePIB.scala 416:17]
  wire [3:0] _GEN_22 = _pibIdle_T_1 & _pibIdle_T_15 ? 4'h5 : _GEN_20; // @[TracePIB.scala 410:74 TracePIB.scala 412:17]
  wire [2:0] _pibStNext_T_12 = io_pibControlSync_pibMode[0] ? 3'h1 : 3'h4; // @[TracePIB.scala 427:21]
  wire [2:0] _GEN_29 = _pibIdle_T_9 ? _pibStNext_T_12 : {{2'd0}, _pibIdle_T_14}; // @[TracePIB.scala 425:40 TracePIB.scala 427:15]
  wire [3:0] _GEN_31 = _pibIdle_T_1 | pibSt == 4'h5 ? _GEN_22 : {{1'd0}, _GEN_29}; // @[TracePIB.scala 409:72]
  wire [3:0] _GEN_35 = pibSt == 4'h2 ? 4'h8 : _GEN_31; // @[TracePIB.scala 406:41 TracePIB.scala 407:15]
  wire [3:0] _GEN_41 = pibSt == 4'h1 ? _pibStNext_T_3 : _GEN_35; // @[TracePIB.scala 402:41 TracePIB.scala 404:15]
  wire [2:0] _pibcountNext_T_17 = _tdataX_T_18 ? 3'h3 : 3'h7; // @[Mux.scala 98:16]
  wire [2:0] _pibcountNext_T_18 = _tdataX_T_12 ? 3'h1 : _pibcountNext_T_17; // @[Mux.scala 98:16]
  wire [2:0] _pibcountNext_T_19 = _tdataX_T_6 ? 3'h0 : _pibcountNext_T_18; // @[Mux.scala 98:16]
  wire [2:0] _pibcountNext_T_41 = pibcount - 3'h1; // @[TracePIB.scala 415:32]
  wire [2:0] _GEN_17 = _pibIdle_T_6 ? _pibcountNext_T_19 : _pibcountNext_T_41; // @[TracePIB.scala 417:28 TracePIB.scala 418:22 TracePIB.scala 415:20]
  wire [2:0] _GEN_24 = _pibIdle_T_1 & _pibIdle_T_15 ? pibcount : _GEN_17; // @[TracePIB.scala 410:74 TracePIB.scala 370:16]
  wire [2:0] _GEN_33 = _pibIdle_T_1 | pibSt == 4'h5 ? _GEN_24 : pibcount; // @[TracePIB.scala 409:72 TracePIB.scala 370:16]
  wire [2:0] _GEN_38 = pibSt == 4'h2 ? pibcount : _GEN_33; // @[TracePIB.scala 406:41 TracePIB.scala 370:16]
  wire [2:0] _GEN_43 = pibSt == 4'h1 ? pibcount : _GEN_38; // @[TracePIB.scala 402:41 TracePIB.scala 370:16]
  wire  _framingNext_T_6 = ~pibSR[0]; // @[TracePIB.scala 411:22]
  wire [3:0] _GEN_19 = _pibIdle_T_6 ? {{3'd0}, io_pibControlSync_pibMode[0]} : framing; // @[TracePIB.scala 417:28 TracePIB.scala 420:21 TracePIB.scala 371:15]
  wire [3:0] _GEN_21 = _pibIdle_T_1 & _pibIdle_T_15 ? {{3'd0}, _framingNext_T_6} : _GEN_19; // @[TracePIB.scala 410:74 TracePIB.scala 411:19]
  wire  _GEN_26 = _pibIdle_T_14 ? _tdataX_T_11 : io_pibControlSync_pibMode[0]; // @[TracePIB.scala 429:40 TracePIB.scala 430:17 TracePIB.scala 434:17]
  wire  _GEN_28 = _pibIdle_T_9 ? 1'h0 : _GEN_26; // @[TracePIB.scala 425:40 TracePIB.scala 426:17]
  wire [3:0] _GEN_30 = _pibIdle_T_1 | pibSt == 4'h5 ? _GEN_21 : {{3'd0}, _GEN_28}; // @[TracePIB.scala 409:72]
  wire [3:0] _GEN_36 = pibSt == 4'h2 ? framing : _GEN_30; // @[TracePIB.scala 406:41 TracePIB.scala 371:15]
  wire [206:0] _pibLastNext_WIRE_1 = {{7'd0}, pibSRNext};
  wire  _GEN_18 = _pibIdle_T_6 ? _pibLastNext_WIRE_1[1:0] == 2'h3 : pibLast; // @[TracePIB.scala 417:28 TracePIB.scala 419:21 TracePIB.scala 372:15]
  wire  _GEN_25 = _pibIdle_T_1 & _pibIdle_T_15 ? pibLast : _GEN_18; // @[TracePIB.scala 410:74 TracePIB.scala 372:15]
  wire  _GEN_34 = _pibIdle_T_1 | pibSt == 4'h5 ? _GEN_25 : pibLast; // @[TracePIB.scala 409:72 TracePIB.scala 372:15]
  wire  _GEN_39 = pibSt == 4'h2 ? pibLast : _GEN_34; // @[TracePIB.scala 406:41 TracePIB.scala 372:15]
  wire  _GEN_44 = pibSt == 4'h1 ? pibLast : _GEN_39; // @[TracePIB.scala 402:41 TracePIB.scala 372:15]
  wire  _pibLastUart_T_1 = pibdivX & _pibIdle_T_1; // @[TracePIB.scala 326:59]
  wire [6:0] _caltimerNext_T_1 = caltimer - 7'h1; // @[TracePIB.scala 375:30]
  wire [6:0] _GEN_16 = _pibGo_T & io_pibControlSync_pibCalibrate ? _caltimerNext_T_1 : 7'h0; // @[TracePIB.scala 374:53 TracePIB.scala 375:18]
  wire [6:0] _caltimerNext_T_3 = io_pibControlSync_pibMode[3] ? 7'h0 : 7'h4f; // @[TracePIB.scala 399:24]
  RHEA__TraceUnpacker unpacker ( // @[TracePIB.scala 307:29]
    .rf_reset(unpacker_rf_reset),
    .clock(unpacker_clock),
    .reset(unpacker_reset),
    .io_teActive(unpacker_io_teActive),
    .io_stream_ready(unpacker_io_stream_ready),
    .io_stream_valid(unpacker_io_stream_valid),
    .io_stream_bits(unpacker_io_stream_bits),
    .io_btm_ready(unpacker_io_btm_ready),
    .io_btm_valid(unpacker_io_btm_valid),
    .io_btm_bits_length(unpacker_io_btm_bits_length),
    .io_btm_bits_slices_0_data(unpacker_io_btm_bits_slices_0_data),
    .io_btm_bits_slices_0_mseo(unpacker_io_btm_bits_slices_0_mseo),
    .io_btm_bits_slices_1_data(unpacker_io_btm_bits_slices_1_data),
    .io_btm_bits_slices_1_mseo(unpacker_io_btm_bits_slices_1_mseo),
    .io_btm_bits_slices_2_data(unpacker_io_btm_bits_slices_2_data),
    .io_btm_bits_slices_2_mseo(unpacker_io_btm_bits_slices_2_mseo),
    .io_btm_bits_slices_3_data(unpacker_io_btm_bits_slices_3_data),
    .io_btm_bits_slices_3_mseo(unpacker_io_btm_bits_slices_3_mseo),
    .io_btm_bits_slices_4_data(unpacker_io_btm_bits_slices_4_data),
    .io_btm_bits_slices_4_mseo(unpacker_io_btm_bits_slices_4_mseo),
    .io_btm_bits_slices_5_data(unpacker_io_btm_bits_slices_5_data),
    .io_btm_bits_slices_5_mseo(unpacker_io_btm_bits_slices_5_mseo),
    .io_btm_bits_slices_6_data(unpacker_io_btm_bits_slices_6_data),
    .io_btm_bits_slices_6_mseo(unpacker_io_btm_bits_slices_6_mseo),
    .io_btm_bits_slices_7_data(unpacker_io_btm_bits_slices_7_data),
    .io_btm_bits_slices_7_mseo(unpacker_io_btm_bits_slices_7_mseo),
    .io_btm_bits_slices_8_data(unpacker_io_btm_bits_slices_8_data),
    .io_btm_bits_slices_8_mseo(unpacker_io_btm_bits_slices_8_mseo),
    .io_btm_bits_slices_9_data(unpacker_io_btm_bits_slices_9_data),
    .io_btm_bits_slices_9_mseo(unpacker_io_btm_bits_slices_9_mseo),
    .io_btm_bits_slices_10_data(unpacker_io_btm_bits_slices_10_data),
    .io_btm_bits_slices_10_mseo(unpacker_io_btm_bits_slices_10_mseo),
    .io_btm_bits_slices_11_data(unpacker_io_btm_bits_slices_11_data),
    .io_btm_bits_slices_11_mseo(unpacker_io_btm_bits_slices_11_mseo),
    .io_btm_bits_slices_12_data(unpacker_io_btm_bits_slices_12_data),
    .io_btm_bits_slices_12_mseo(unpacker_io_btm_bits_slices_12_mseo),
    .io_btm_bits_slices_13_data(unpacker_io_btm_bits_slices_13_data),
    .io_btm_bits_slices_13_mseo(unpacker_io_btm_bits_slices_13_mseo),
    .io_btm_bits_slices_14_data(unpacker_io_btm_bits_slices_14_data),
    .io_btm_bits_slices_14_mseo(unpacker_io_btm_bits_slices_14_mseo),
    .io_btm_bits_slices_15_data(unpacker_io_btm_bits_slices_15_data),
    .io_btm_bits_slices_15_mseo(unpacker_io_btm_bits_slices_15_mseo),
    .io_btm_bits_slices_16_data(unpacker_io_btm_bits_slices_16_data),
    .io_btm_bits_slices_16_mseo(unpacker_io_btm_bits_slices_16_mseo),
    .io_btm_bits_slices_17_data(unpacker_io_btm_bits_slices_17_data),
    .io_btm_bits_slices_17_mseo(unpacker_io_btm_bits_slices_17_mseo),
    .io_btm_bits_slices_18_data(unpacker_io_btm_bits_slices_18_data),
    .io_btm_bits_slices_18_mseo(unpacker_io_btm_bits_slices_18_mseo),
    .io_btm_bits_slices_19_data(unpacker_io_btm_bits_slices_19_data),
    .io_btm_bits_slices_19_mseo(unpacker_io_btm_bits_slices_19_mseo),
    .io_btm_bits_slices_20_data(unpacker_io_btm_bits_slices_20_data),
    .io_btm_bits_slices_20_mseo(unpacker_io_btm_bits_slices_20_mseo),
    .io_btm_bits_slices_21_data(unpacker_io_btm_bits_slices_21_data),
    .io_btm_bits_slices_21_mseo(unpacker_io_btm_bits_slices_21_mseo),
    .io_btm_bits_slices_22_data(unpacker_io_btm_bits_slices_22_data),
    .io_btm_bits_slices_22_mseo(unpacker_io_btm_bits_slices_22_mseo),
    .io_btm_bits_slices_23_data(unpacker_io_btm_bits_slices_23_data),
    .io_btm_bits_slices_23_mseo(unpacker_io_btm_bits_slices_23_mseo),
    .io_btm_bits_slices_24_data(unpacker_io_btm_bits_slices_24_data),
    .io_btm_bits_slices_24_mseo(unpacker_io_btm_bits_slices_24_mseo)
  );
  assign unpacker_rf_reset = rf_reset;
  assign rf_reset = reset;
  assign io_streamIn_ready = unpacker_io_stream_ready; // @[TracePIB.scala 365:22]
  assign io_pib_tref = pibref[1]; // @[TracePIB.scala 300:43]
  assign io_pib_tdata = tdata; // @[TracePIB.scala 299:16]
  assign io_pibEmpty = pibEmpty; // @[TracePIB.scala 301:15]
  assign unpacker_clock = clock;
  assign unpacker_reset = reset;
  assign unpacker_io_teActive = io_pibControlSync_pibActive; // @[TracePIB.scala 364:24]
  assign unpacker_io_stream_valid = io_streamIn_valid; // @[TracePIB.scala 365:22]
  assign unpacker_io_stream_bits = io_streamIn_bits; // @[TracePIB.scala 365:22]
  assign unpacker_io_btm_ready = pibdivX & pibIdle | io_pibControlSync_pibCalibrate; // @[TracePIB.scala 366:49]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      divcount <= 13'h0;
    end else if (_T_4 | ~(|io_pibControlSync_pibMode)) begin
      divcount <= 13'h0;
    end else if (pibdiv) begin
      divcount <= 13'h0;
    end else begin
      divcount <= _divcount_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pibref <= 2'h0;
    end else if (pibdiv) begin
      if (~io_pibControlSync_pibActive) begin
        pibref <= 2'h0;
      end else if (io_pibControlSync_pibMode[3]) begin
        pibref <= _pibrefNext_T_2;
      end else begin
        pibref <= 2'h0;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tdata <= 4'h0;
    end else begin
      tdata <= _GEN_7[3:0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pibEmpty <= 1'h1;
    end else begin
      pibEmpty <= _T_4 | _GEN_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pibSt <= 4'h0;
    end else if (pibdivX) begin
      if (_T_4) begin
        pibSt <= 4'h0;
      end else if (pibIdle & ~pibGo) begin
        pibSt <= 4'h0;
      end else if (pibIdle & pibGo) begin
        pibSt <= _pibStNext_T_1;
      end else begin
        pibSt <= _GEN_41;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      pibSR <= 200'h0;
    end else if (pibdivX) begin
      if (!(pibIdle & ~pibGo)) begin
        if (pibIdle & pibGo) begin
          if (io_pibControlSync_pibCalibrate) begin
            pibSR <= {{168'd0}, _pibSRNext_T_3};
          end else begin
            pibSR <= _pibSRNext_T_4;
          end
        end else if (!(pibSt == 4'h1)) begin
          pibSR <= _GEN_37;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      framing <= 4'h0;
    end else if (pibdivX) begin
      if (pibIdle & ~pibGo) begin
        if (io_pibControlSync_pibMode[3]) begin
          framing <= 4'hf;
        end else begin
          framing <= {{3'd0}, io_pibControlSync_pibMode[0]};
        end
      end else if (pibIdle & pibGo) begin
        framing <= {{3'd0}, _tdataX_T_11};
      end else if (pibSt == 4'h1) begin
        framing <= 4'h0;
      end else begin
        framing <= _GEN_36;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pibLast <= 1'h0;
    end else if (pibdivX) begin
      if (_T_4) begin
        pibLast <= 1'h0;
      end else if (pibIdle & ~pibGo) begin
        pibLast <= 1'h0;
      end else if (pibIdle & pibGo) begin
        pibLast <= 1'h0;
      end else begin
        pibLast <= _GEN_44;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pibcount <= 3'h0;
    end else if (pibdivX) begin
      if (_T_4) begin
        pibcount <= 3'h0;
      end else if (pibIdle & ~pibGo) begin
        pibcount <= _pibcountNext_T_19;
      end else if (pibIdle & pibGo) begin
        pibcount <= _pibcountNext_T_19;
      end else begin
        pibcount <= _GEN_43;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pibLastUart <= 1'h0;
    end else if (_pibLastUart_T_1) begin
      pibLastUart <= pibLast;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      caltimer <= 7'h0;
    end else if (pibdivX) begin
      if (_T_4) begin
        caltimer <= 7'h0;
      end else if (pibIdle & ~pibGo) begin
        caltimer <= _GEN_16;
      end else if (pibIdle & pibGo) begin
        caltimer <= _caltimerNext_T_3;
      end else begin
        caltimer <= _GEN_16;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  divcount = _RAND_0[12:0];
  _RAND_1 = {1{`RANDOM}};
  pibref = _RAND_1[1:0];
  _RAND_2 = {1{`RANDOM}};
  tdata = _RAND_2[3:0];
  _RAND_3 = {1{`RANDOM}};
  pibEmpty = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  pibSt = _RAND_4[3:0];
  _RAND_5 = {7{`RANDOM}};
  pibSR = _RAND_5[199:0];
  _RAND_6 = {1{`RANDOM}};
  framing = _RAND_6[3:0];
  _RAND_7 = {1{`RANDOM}};
  pibLast = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  pibcount = _RAND_8[2:0];
  _RAND_9 = {1{`RANDOM}};
  pibLastUart = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  caltimer = _RAND_10[6:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    divcount = 13'h0;
  end
  if (reset) begin
    pibref = 2'h0;
  end
  if (rf_reset) begin
    tdata = 4'h0;
  end
  if (reset) begin
    pibEmpty = 1'h1;
  end
  if (reset) begin
    pibSt = 4'h0;
  end
  if (rf_reset) begin
    pibSR = 200'h0;
  end
  if (rf_reset) begin
    framing = 4'h0;
  end
  if (reset) begin
    pibLast = 1'h0;
  end
  if (reset) begin
    pibcount = 3'h0;
  end
  if (reset) begin
    pibLastUart = 1'h0;
  end
  if (reset) begin
    caltimer = 7'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
