//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__AsyncQueueSink_3(
  input         rf_reset,
  input         clock,
  input         reset,
  output        io_deq_valid,
  output [38:0] io_deq_bits,
  input  [38:0] io_async_mem_0,
  output        io_async_ridx,
  input         io_async_widx
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
`endif // RANDOMIZE_REG_INIT
  wire  widx_widx_gray_clock; // @[ShiftReg.scala 45:23]
  wire  widx_widx_gray_reset; // @[ShiftReg.scala 45:23]
  wire  widx_widx_gray_io_d; // @[ShiftReg.scala 45:23]
  wire  widx_widx_gray_io_q; // @[ShiftReg.scala 45:23]
  wire  io_deq_bits_deq_bits_reg_rf_reset; // @[SynchronizerReg.scala 207:25]
  wire  io_deq_bits_deq_bits_reg_clock; // @[SynchronizerReg.scala 207:25]
  wire [38:0] io_deq_bits_deq_bits_reg_io_d; // @[SynchronizerReg.scala 207:25]
  wire [38:0] io_deq_bits_deq_bits_reg_io_q; // @[SynchronizerReg.scala 207:25]
  wire  io_deq_bits_deq_bits_reg_io_en; // @[SynchronizerReg.scala 207:25]
  reg  ridx_ridx_bin; // @[AsyncQueue.scala 52:25]
  wire  ridx_incremented = ridx_ridx_bin + io_deq_valid; // @[AsyncQueue.scala 53:43]
  wire  widx = widx_widx_gray_io_q; // @[ShiftReg.scala 48:24 ShiftReg.scala 48:24]
  reg  valid_reg; // @[AsyncQueue.scala 161:56]
  reg  ridx_gray; // @[AsyncQueue.scala 164:55]
  RHEA__AsyncResetSynchronizerShiftReg_w1_d3_i0 widx_widx_gray ( // @[ShiftReg.scala 45:23]
    .clock(widx_widx_gray_clock),
    .reset(widx_widx_gray_reset),
    .io_d(widx_widx_gray_io_d),
    .io_q(widx_widx_gray_io_q)
  );
  RHEA__ClockCrossingReg_w39 io_deq_bits_deq_bits_reg ( // @[SynchronizerReg.scala 207:25]
    .rf_reset(io_deq_bits_deq_bits_reg_rf_reset),
    .clock(io_deq_bits_deq_bits_reg_clock),
    .io_d(io_deq_bits_deq_bits_reg_io_d),
    .io_q(io_deq_bits_deq_bits_reg_io_q),
    .io_en(io_deq_bits_deq_bits_reg_io_en)
  );
  assign io_deq_bits_deq_bits_reg_rf_reset = rf_reset;
  assign io_deq_valid = valid_reg; // @[AsyncQueue.scala 162:29]
  assign io_deq_bits = io_deq_bits_deq_bits_reg_io_q; // @[SynchronizerReg.scala 211:26 SynchronizerReg.scala 211:26]
  assign io_async_ridx = ridx_gray; // @[AsyncQueue.scala 165:17]
  assign widx_widx_gray_clock = clock;
  assign widx_widx_gray_reset = reset;
  assign widx_widx_gray_io_d = io_async_widx; // @[ShiftReg.scala 47:16]
  assign io_deq_bits_deq_bits_reg_clock = clock;
  assign io_deq_bits_deq_bits_reg_io_d = io_async_mem_0; // @[SynchronizerReg.scala 209:18]
  assign io_deq_bits_deq_bits_reg_io_en = ridx_incremented != widx; // @[AsyncQueue.scala 146:36]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      ridx_ridx_bin <= 1'h0;
    end else begin
      ridx_ridx_bin <= ridx_ridx_bin + io_deq_valid;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid_reg <= 1'h0;
    end else begin
      valid_reg <= ridx_incremented != widx;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      ridx_gray <= 1'h0;
    end else begin
      ridx_gray <= ridx_ridx_bin + io_deq_valid;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ridx_ridx_bin = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  valid_reg = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  ridx_gray = _RAND_2[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    ridx_ridx_bin = 1'h0;
  end
  if (reset) begin
    valid_reg = 1'h0;
  end
  if (reset) begin
    ridx_gray = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
