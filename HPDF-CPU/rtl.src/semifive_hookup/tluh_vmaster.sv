// SPDX-License-Identifier: Apache-2.0

//----------------------------------------------------------------------------------------
//  VIP interface
//----------------------------------------------------------------------------------------
`ifndef SYNTHESIS
interface tluh_vmaster_driver_if #(
    parameter Z = 4, 
              O = 5, 
              I = 2, 
              A =36, 
              W = 8,
              MAX_ID = 24,
              MAX_BYTES = 64, 
              MAX_OUTSTANDING = 16 
)(
    input bit CLK,
    input bit RSTN
);
    logic           ENABLE    ;
    logic [2:0]     a_opcode  ;
    logic [2:0]     a_param   ;
    logic [Z  -1:0] a_size    ;
    logic [O  -1:0] a_source  ;
    logic [A  -1:0] a_address ;
    logic [W  -1:0] a_mask    ;
    logic [W*8-1:0] a_data    ;
    logic           a_corrupt ;
    logic           a_valid   ;
    logic           a_ready   ;
    logic [2:0]     d_opcode  ;
    logic [1:0]     d_param   ;
    logic [Z  -1:0] d_size    ;
    logic [O  -1:0] d_source  ;
    logic [I  -1:0] d_sink    ;
    logic           d_denied  ;
    logic [W*8-1:0] d_data    ;
    logic           d_corrupt ;
    logic           d_valid   ;
    logic           d_ready   ;
 
    //--------------------------------------------------------------------------------
    //
    //--------------------------------------------------------------------------------
    class BURST_TRANSACTION;
        reg              VALID;
        reg              WRITE;
        reg  [A-1:0]     ADDR ;
        reg  [  7:0]     DATA [$];
    endclass

    BURST_TRANSACTION   cur_burst;

    initial begin
        ENABLE = 0;
        cur_burst = new();
    end

    always@(posedge CLK or negedge RSTN)
        if(!RSTN) begin
            a_corrupt <= 0;
            d_ready   <= 1;
            cur_burst.VALID = 0;
        end

    task collect(logic in_WRITE, logic[63:0] in_ADDR, logic[7:0] in_DATA);        
        if( cur_burst.VALID && (
                (cur_burst.WRITE !== in_WRITE)
                || (cur_burst.ADDR + cur_burst.DATA.size() != in_ADDR)
        )) begin
            flush_a_burst(cur_burst);
            cur_burst.VALID = 0;
        end
        if( 0==cur_burst.VALID ) begin
            cur_burst.VALID = 1;
            cur_burst.ADDR  = in_ADDR ;
            cur_burst.WRITE = in_WRITE;
            cur_burst.DATA.delete();
        end        
        cur_burst.DATA.push_back(in_DATA);
    endtask

    //--------------------------------------------------------------------------------
    //  - Support multiple outstanding
    //  - Not support Simultaneous response
    //  - Not support out of order
    //--------------------------------------------------------------------------------
    parameter L = $clog2(W);

    class TLUH_TRANSACTION_A;
        reg  [2:0]       opcode ;
        reg  [2:0]       param  ;
        reg  [A-1:0]     address;
        reg  [Z-1:0]     size   ;
        reg  [W-1:0]     mask   ;
        reg  [W*8-1:0]   data   ;
        reg              last   ;
    endclass

    class TLUH_TRANSACTION_D;
        reg              last   ;
        reg  [W-1:0]     mask   ;
    endclass

    //--------------------------------------------------------------------------------
    TLUH_TRANSACTION_A  transaction_a_queue[$];
    TLUH_TRANSACTION_D  transaction_d_queue[$];
    reg[7:0]            transaction_read[$];

    task flush_a_burst(input BURST_TRANSACTION a_burst);
        reg [A-1:0]  cur_address;
        int          cur_bytes;        
        int tl_size  ;
        int tl_beats ;
        int tl_mask  ;
        cur_address= a_burst.ADDR;
        cur_bytes  = a_burst.DATA.size();
        @(negedge CLK );
        while( 0<cur_bytes ) begin            
            int max_log2_size_address;
            int max_log2_size_bytes;
            max_log2_size_address = 0;
            max_log2_size_bytes = 0;
            for(int i=0;i<A;i++) begin
                if( cur_address[i] ) break;
                max_log2_size_address=i+1;
            end            
            for(int i=0;i<A;i++) begin
                if( (1<<i) > cur_bytes ) break;
                max_log2_size_bytes = i;
            end
            tl_size  = (max_log2_size_bytes<max_log2_size_address) ? max_log2_size_bytes : max_log2_size_address;
            while( MAX_BYTES < (1<<(tl_size)) ) tl_size--;
            tl_beats = ((1<<tl_size)>=W) ? ((1<<(tl_size))/W) : 1;
            tl_mask  = ((1<<tl_size)>=W) ? {W{1'b1}} : (((1<<(1<<tl_size))-1)<<(cur_address[L-1:0]));
            for(int i=0;i<tl_beats;i++) begin
                TLUH_TRANSACTION_A a;
                TLUH_TRANSACTION_D d;
                if(0!=a_burst.WRITE || 0==i) begin
                    a = new();
                    a.opcode = a_burst.WRITE ? 0 : 4;
                    a.param  = 0;
                    a.address= cur_address;
                    a.size   = tl_size;
                    a.mask   = tl_mask;
                    a.data   = 0;
                    for(int j=0;j<(1<<tl_size) && j<W;j++) begin
                        a.data = a.data | (a_burst.DATA.pop_front() <<(j*8));
                    end                
                    a.data   = a.data<<(cur_address[L-1:0]*8);
                    a.last   = ((0==a_burst.WRITE) || ((tl_beats-1)==i));
                    transaction_a_queue.push_back(a);
                end
                if(0==a_burst.WRITE || 0==i) begin
                    d = new;
                    d.mask = tl_mask;
                    d.last = ((0!=a_burst.WRITE) || ((tl_beats-1)==i));
                    transaction_d_queue.push_back(d);
                end
            end
            cur_address += (1<<tl_size);
            cur_bytes   -= (1<<tl_size);
        end
    endtask

    function [7:0] read_data;
        assert( 0!=transaction_read.size() );
        return transaction_read.pop_front();        
    endfunction

    task wait_for;
        if(cur_burst.VALID) begin
            flush_a_burst(cur_burst);
            cur_burst.VALID = 0;
        end
        @(negedge CLK );            
        while(0<transaction_a_queue.size() || 0<transaction_d_queue.size() || a_valid || d_valid ) @(negedge CLK );
        @(posedge CLK );
    endtask

    reg [O-1:0] r_a_source;

    always@(posedge CLK or negedge RSTN)
        if(!RSTN) begin
            a_source  <= {O{1'bx}};
            a_opcode  <= 3'bx;
            a_param   <= 3'bx;
            a_size    <= {Z{1'bx}};
            a_address <= {A{1'bx}};
            a_mask    <= {W{1'bx}};
            a_data    <= {(W*8){1'bx}};
            a_valid   <= 0;
            r_a_source<= 0;
        end else if(((0==a_valid) || a_ready ) && (0<transaction_a_queue.size())) begin
            TLUH_TRANSACTION_A  a_transaction;
            a_transaction = transaction_a_queue.pop_front();
            a_source  <= r_a_source;
            a_opcode  <= a_transaction.opcode;
            a_param   <= a_transaction.param;
            a_size    <= a_transaction.size;
            a_address <= a_transaction.address;
            a_mask    <= a_transaction.mask;
            a_data    <= a_transaction.data;
            a_valid   <= 1;
            if(a_transaction.last) begin
                if(MAX_ID==r_a_source) r_a_source <= 0;
                else                   r_a_source <= r_a_source+1;
            end
        end else if( a_ready ) begin
            a_source  <= {O{1'bx}};
            a_opcode  <= 3'bx;
            a_param   <= 3'bx;
            a_size    <= {Z{1'bx}};
            a_address <= {A{1'bx}};
            a_mask    <= {W{1'bx}};
            a_data    <= {(W*8){1'bx}};
            a_valid   <= 0;
        end

    reg [O-1:0] r_d_source;

    always@(posedge CLK or negedge RSTN)
        if(~RSTN) begin
            r_d_source <= 0;
        end else if(d_valid && d_ready) begin
            integer g;
            TLUH_TRANSACTION_D item;
            reg  [W-1:0]  mask;
            assert( 0 < transaction_d_queue.size() );
            if(r_d_source != d_source) $display("r_d_source(%d) != d_source(%d)", r_d_source, d_source );
            assert( r_d_source == d_source );
            item = transaction_d_queue.pop_front();
            if( item.last ) begin
                if(MAX_ID==r_d_source) r_d_source <= 0;
                else                   r_d_source <= r_d_source+1;
            end 
            if(d_opcode==1) begin
                mask = item.mask;
                for( g=0; g<W; g=g+1 ) begin
                    if( mask[g] ) begin
                        reg[7:0] ritem;
                        ritem = (d_data >> (g*8));
                        transaction_read.push_back(ritem);
                    end
                end
            end
        end

endinterface

//----------------------------------------------------------------------------------------
//  VIPs
//----------------------------------------------------------------------------------------
class tluh_vmaster_driver #(
    parameter Z = 4, 
              O = 5, 
              I = 2, 
              A =36, 
              W = 8,
              MAX_ID = 24,
              MAX_BYTES = 64,
              MAX_OUTSTANDING = 64 ) extends vmaster_driver;
    typedef virtual tluh_vmaster_driver_if #(.Z(Z),.O(O),.I(I),.A(A),.W(W),.MAX_ID(MAX_ID),.MAX_BYTES(MAX_BYTES),.MAX_OUTSTANDING(MAX_OUTSTANDING)) vif_type;
    vif_type vif;
    function new(string name,vif_type vif);
        this.vif = vif;
        semifive_hookup_config#(vmaster_driver,string)::set(name,this);
    endfunction

    //--------------------------------------------------------------------------------
    //  Service functions
    //--------------------------------------------------------------------------------
    virtual task write_req( input logic[63:0] ADDR, input logic[7:0] DATA );
        vif.collect( 1, ADDR, DATA );
    endtask

    virtual task write_flush;
        vif.wait_for();
    endtask

    virtual task read_req( input logic[63:0] ADDR );
        vif.collect( 0, ADDR, 8'hXX );
    endtask

    virtual task read_flush;
        vif.wait_for();
    endtask

    virtual function [7:0] get_read_data;
        return vif.read_data();
    endfunction

    virtual task hookup( input logic enable );
        vif.ENABLE = enable;
    endtask

    virtual task tick;
        @(posedge vif.CLK);
    endtask

endclass

`endif

//----------------------------------------------------------------------------------------
//  VIP RTL wrapper
//----------------------------------------------------------------------------------------
module tluh_vmaster_binder #(
    parameter string name="name for semifive_hookup_config",
              int Z = 4, 
              O = 5, 
              I = 2, 
              A =36, 
              W = 8,
              MAX_ID = 24,
              MAX_BYTES = 64, 
              MAX_OUTSTANDING = 64 
)(
    input               CLK__BUS,
    input               RSTN__BUS, 

    input  [2:0]        TLUH__BUS__SLV__a_opcode  ,
    input  [2:0]        TLUH__BUS__SLV__a_param   ,
    input  [Z  -1:0]    TLUH__BUS__SLV__a_size    ,
    input  [O  -1:0]    TLUH__BUS__SLV__a_source  ,
    input  [A  -1:0]    TLUH__BUS__SLV__a_address ,
    input  [W  -1:0]    TLUH__BUS__SLV__a_mask    ,
    input  [W*8-1:0]    TLUH__BUS__SLV__a_data    ,
    input               TLUH__BUS__SLV__a_corrupt ,
    input               TLUH__BUS__SLV__a_valid   ,
    output              TLUH__BUS__SLV__a_ready   ,
    output [2:0]        TLUH__BUS__SLV__d_opcode  ,
    output [1:0]        TLUH__BUS__SLV__d_param   ,
    output [Z  -1:0]    TLUH__BUS__SLV__d_size    ,
    output [O  -1:0]    TLUH__BUS__SLV__d_source  ,
    output [I  -1:0]    TLUH__BUS__SLV__d_sink    ,
    output              TLUH__BUS__SLV__d_denied  ,
    output [W*8-1:0]    TLUH__BUS__SLV__d_data    ,
    output              TLUH__BUS__SLV__d_corrupt ,
    output              TLUH__BUS__SLV__d_valid   ,
    input               TLUH__BUS__SLV__d_ready   ,

    output [2:0]        TLUH__BUS__MST__a_opcode  ,
    output [2:0]        TLUH__BUS__MST__a_param   ,
    output [Z  -1:0]    TLUH__BUS__MST__a_size    ,
    output [O  -1:0]    TLUH__BUS__MST__a_source  ,
    output [A  -1:0]    TLUH__BUS__MST__a_address ,
    output [W  -1:0]    TLUH__BUS__MST__a_mask    ,
    output [W*8-1:0]    TLUH__BUS__MST__a_data    ,
    output              TLUH__BUS__MST__a_corrupt ,
    output              TLUH__BUS__MST__a_valid   ,
    input               TLUH__BUS__MST__a_ready   ,
    input  [2:0]        TLUH__BUS__MST__d_opcode  ,
    input  [1:0]        TLUH__BUS__MST__d_param   ,
    input  [Z  -1:0]    TLUH__BUS__MST__d_size    ,
    input  [O  -1:0]    TLUH__BUS__MST__d_source  ,
    input  [I  -1:0]    TLUH__BUS__MST__d_sink    ,
    input               TLUH__BUS__MST__d_denied  ,
    input  [W*8-1:0]    TLUH__BUS__MST__d_data    ,
    input               TLUH__BUS__MST__d_corrupt ,
    input               TLUH__BUS__MST__d_valid   ,
    output              TLUH__BUS__MST__d_ready   
);
`ifndef SYNTHESIS
    tluh_vmaster_driver_if #(.Z(Z),.O(O),.I(I),.A(A),.W(W),.MAX_ID(MAX_ID),.MAX_BYTES(MAX_BYTES),.MAX_OUTSTANDING(MAX_OUTSTANDING)) tluh_if(CLK__BUS,RSTN__BUS);
    tluh_vmaster_driver#(.Z(Z),.O(O),.I(I),.A(A),.W(W),.MAX_ID(MAX_ID),.MAX_BYTES(MAX_BYTES),.MAX_OUTSTANDING(MAX_OUTSTANDING)) driver;
    initial driver = new(name, tluh_if);

    //-----------------------------------------------------
    assign TLUH__BUS__MST__a_opcode  = tluh_if.ENABLE ? tluh_if.a_opcode  : TLUH__BUS__SLV__a_opcode  ;
    assign TLUH__BUS__MST__a_param   = tluh_if.ENABLE ? tluh_if.a_param   : TLUH__BUS__SLV__a_param   ;
    assign TLUH__BUS__MST__a_size    = tluh_if.ENABLE ? tluh_if.a_size    : TLUH__BUS__SLV__a_size    ;
    assign TLUH__BUS__MST__a_source  = tluh_if.ENABLE ? tluh_if.a_source  : TLUH__BUS__SLV__a_source  ;
    assign TLUH__BUS__MST__a_address = tluh_if.ENABLE ? tluh_if.a_address : TLUH__BUS__SLV__a_address ;
    assign TLUH__BUS__MST__a_mask    = tluh_if.ENABLE ? tluh_if.a_mask    : TLUH__BUS__SLV__a_mask    ;
    assign TLUH__BUS__MST__a_data    = tluh_if.ENABLE ? tluh_if.a_data    : TLUH__BUS__SLV__a_data    ;
    assign TLUH__BUS__MST__a_corrupt = tluh_if.ENABLE ? tluh_if.a_corrupt : TLUH__BUS__SLV__a_corrupt ;
    assign TLUH__BUS__MST__a_valid   = tluh_if.ENABLE ? tluh_if.a_valid   : TLUH__BUS__SLV__a_valid   ;
    assign TLUH__BUS__MST__d_ready   = tluh_if.ENABLE ? tluh_if.d_ready   : TLUH__BUS__SLV__d_ready   ;

    assign TLUH__BUS__SLV__a_ready   = (0==tluh_if.ENABLE) & TLUH__BUS__MST__a_ready  ;
    assign TLUH__BUS__SLV__d_opcode  = TLUH__BUS__MST__d_opcode ;
    assign TLUH__BUS__SLV__d_param   = TLUH__BUS__MST__d_param  ;
    assign TLUH__BUS__SLV__d_size    = TLUH__BUS__MST__d_size   ;
    assign TLUH__BUS__SLV__d_source  = TLUH__BUS__MST__d_source ;
    assign TLUH__BUS__SLV__d_sink    = TLUH__BUS__MST__d_sink   ;
    assign TLUH__BUS__SLV__d_denied  = TLUH__BUS__MST__d_denied ;
    assign TLUH__BUS__SLV__d_data    = TLUH__BUS__MST__d_data   ;
    assign TLUH__BUS__SLV__d_corrupt = TLUH__BUS__MST__d_corrupt;
    assign TLUH__BUS__SLV__d_valid   = (0==tluh_if.ENABLE) & TLUH__BUS__MST__d_valid  ;

    assign tluh_if.a_ready   = (1==tluh_if.ENABLE) & TLUH__BUS__MST__a_ready  ;
    assign tluh_if.d_opcode  = TLUH__BUS__MST__d_opcode ;
    assign tluh_if.d_param   = TLUH__BUS__MST__d_param  ;
    assign tluh_if.d_size    = TLUH__BUS__MST__d_size   ;
    assign tluh_if.d_source  = TLUH__BUS__MST__d_source ;
    assign tluh_if.d_sink    = TLUH__BUS__MST__d_sink   ;
    assign tluh_if.d_denied  = TLUH__BUS__MST__d_denied ;
    assign tluh_if.d_data    = TLUH__BUS__MST__d_data   ;
    assign tluh_if.d_corrupt = TLUH__BUS__MST__d_corrupt;
    assign tluh_if.d_valid   = (1==tluh_if.ENABLE) & TLUH__BUS__MST__d_valid  ;
`else
    //-----------------------------------------------------
    assign TLUH__BUS__MST__a_opcode  = TLUH__BUS__SLV__a_opcode  ;
    assign TLUH__BUS__MST__a_param   = TLUH__BUS__SLV__a_param   ;
    assign TLUH__BUS__MST__a_size    = TLUH__BUS__SLV__a_size    ;
    assign TLUH__BUS__MST__a_source  = TLUH__BUS__SLV__a_source  ;
    assign TLUH__BUS__MST__a_address = TLUH__BUS__SLV__a_address ;
    assign TLUH__BUS__MST__a_mask    = TLUH__BUS__SLV__a_mask    ;
    assign TLUH__BUS__MST__a_data    = TLUH__BUS__SLV__a_data    ;
    assign TLUH__BUS__MST__a_corrupt = TLUH__BUS__SLV__a_corrupt ;
    assign TLUH__BUS__MST__a_valid   = TLUH__BUS__SLV__a_valid   ;
    assign TLUH__BUS__MST__d_ready   = TLUH__BUS__SLV__d_ready   ;

    assign TLUH__BUS__SLV__a_ready   = TLUH__BUS__MST__a_ready  ;
    assign TLUH__BUS__SLV__d_opcode  = TLUH__BUS__MST__d_opcode ;
    assign TLUH__BUS__SLV__d_param   = TLUH__BUS__MST__d_param  ;
    assign TLUH__BUS__SLV__d_size    = TLUH__BUS__MST__d_size   ;
    assign TLUH__BUS__SLV__d_source  = TLUH__BUS__MST__d_source ;
    assign TLUH__BUS__SLV__d_sink    = TLUH__BUS__MST__d_sink   ;
    assign TLUH__BUS__SLV__d_denied  = TLUH__BUS__MST__d_denied ;
    assign TLUH__BUS__SLV__d_data    = TLUH__BUS__MST__d_data   ;
    assign TLUH__BUS__SLV__d_corrupt = TLUH__BUS__MST__d_corrupt;
    assign TLUH__BUS__SLV__d_valid   = TLUH__BUS__MST__d_valid  ;
`endif
endmodule
