module axi4_id_padder #(
    parameter IW_IN = 10,
    parameter IW_OUT = 2,
    parameter AW = 32,
    parameter DW = 64,
    parameter SW = DW/8,
    parameter UW = 10)
    (
    input   [AW-1:0]        AXI4__IN__AWADDR,
    input   [IW_IN-1:0]     AXI4__IN__AWID,
    input   [7:0]           AXI4__IN__AWLEN,
    input   [2:0]           AXI4__IN__AWSIZE,
    input   [1:0]           AXI4__IN__AWBURST,
    input   [0:0]           AXI4__IN__AWLOCK,
    input   [3:0]           AXI4__IN__AWCACHE,
    input   [2:0]           AXI4__IN__AWPROT,
    input   [UW-1:0]        AXI4__IN__AWUSER,
    input                   AXI4__IN__AWVALID,
    output                  AXI4__IN__AWREADY,
    input   [DW-1:0]        AXI4__IN__WDATA,
    input   [SW-1:0]        AXI4__IN__WSTRB,
    input                   AXI4__IN__WVALID,
    input                   AXI4__IN__WLAST,
    output                  AXI4__IN__WREADY,
    output  [IW_IN-1:0]     AXI4__IN__BID,
    output                  AXI4__IN__BVALID,
    output  [1:0]           AXI4__IN__BRESP,
    input                   AXI4__IN__BREADY,
    input   [IW_IN-1:0]     AXI4__IN__ARID,
    input   [AW-1:0]        AXI4__IN__ARADDR,
    input   [7:0]           AXI4__IN__ARLEN,
    input   [2:0]           AXI4__IN__ARSIZE,
    input   [1:0]           AXI4__IN__ARBURST,
    input   [0:0]           AXI4__IN__ARLOCK,
    input   [3:0]           AXI4__IN__ARCACHE,
    input   [2:0]           AXI4__IN__ARPROT,
    input   [UW-1:0]        AXI4__IN__ARUSER,
    input                   AXI4__IN__ARVALID,
    output                  AXI4__IN__ARREADY,
    output  [IW_IN-1:0]     AXI4__IN__RID,
    output  [DW-1:0]        AXI4__IN__RDATA,
    output  [1:0]           AXI4__IN__RRESP,
    output                  AXI4__IN__RLAST,
    output                  AXI4__IN__RVALID,
    input                   AXI4__IN__RREADY,
    output  [AW-1:0]        AXI4__OUT__AWADDR,
    output  [IW_OUT-1:0]    AXI4__OUT__AWID,
    output  [7:0]           AXI4__OUT__AWLEN,
    output  [2:0]           AXI4__OUT__AWSIZE,
    output  [1:0]           AXI4__OUT__AWBURST,
    output  [0:0]           AXI4__OUT__AWLOCK,
    output  [3:0]           AXI4__OUT__AWCACHE,
    output  [2:0]           AXI4__OUT__AWPROT,
    output  [UW-1:0]        AXI4__OUT__AWUSER,
    output                  AXI4__OUT__AWVALID,
    input                   AXI4__OUT__AWREADY,
    output  [DW-1:0]        AXI4__OUT__WDATA,
    output  [SW-1:0]        AXI4__OUT__WSTRB,
    output                  AXI4__OUT__WVALID,
    output                  AXI4__OUT__WLAST,
    input                   AXI4__OUT__WREADY,
    input   [IW_OUT-1:0]    AXI4__OUT__BID,
    input                   AXI4__OUT__BVALID,
    input   [1:0]           AXI4__OUT__BRESP,
    output                  AXI4__OUT__BREADY,
    output  [IW_OUT-1:0]    AXI4__OUT__ARID,
    output  [AW-1:0]        AXI4__OUT__ARADDR,
    output  [7:0]           AXI4__OUT__ARLEN,
    output  [2:0]           AXI4__OUT__ARSIZE,
    output  [1:0]           AXI4__OUT__ARBURST,
    output  [0:0]           AXI4__OUT__ARLOCK,
    output  [3:0]           AXI4__OUT__ARCACHE,
    output  [2:0]           AXI4__OUT__ARPROT,
    output  [UW-1:0]        AXI4__OUT__ARUSER,
    output                  AXI4__OUT__ARVALID,
    input                   AXI4__OUT__ARREADY,
    input   [IW_OUT-1:0]    AXI4__OUT__RID,
    input   [DW-1:0]        AXI4__OUT__RDATA,
    input   [1:0]           AXI4__OUT__RRESP,
    input                   AXI4__OUT__RLAST,
    input                   AXI4__OUT__RVALID,
    output                  AXI4__OUT__RREADY);

    
    assign AXI4__IN__AWREADY  = AXI4__OUT__AWREADY ;
    assign AXI4__IN__WREADY   = AXI4__OUT__WREADY  ;
    assign AXI4__IN__BVALID   = AXI4__OUT__BVALID  ;
    assign AXI4__IN__BRESP    = AXI4__OUT__BRESP   ;
    assign AXI4__IN__ARREADY  = AXI4__OUT__ARREADY ;
    assign AXI4__IN__RDATA    = AXI4__OUT__RDATA   ;
    assign AXI4__IN__RRESP    = AXI4__OUT__RRESP   ;
    assign AXI4__IN__RLAST    = AXI4__OUT__RLAST   ;
    assign AXI4__IN__RVALID   = AXI4__OUT__RVALID  ;
    assign AXI4__OUT__AWADDR  = AXI4__IN__AWADDR ;
    assign AXI4__OUT__AWLEN   = AXI4__IN__AWLEN  ;
    assign AXI4__OUT__AWSIZE  = AXI4__IN__AWSIZE ;
    assign AXI4__OUT__AWBURST = AXI4__IN__AWBURST;
    assign AXI4__OUT__AWLOCK  = AXI4__IN__AWLOCK ;
    assign AXI4__OUT__AWCACHE = AXI4__IN__AWCACHE;
    assign AXI4__OUT__AWPROT  = AXI4__IN__AWPROT ;
    assign AXI4__OUT__AWUSER  = AXI4__IN__AWUSER ;
    assign AXI4__OUT__AWVALID = AXI4__IN__AWVALID;
    assign AXI4__OUT__WDATA   = AXI4__IN__WDATA  ;
    assign AXI4__OUT__WSTRB   = AXI4__IN__WSTRB  ;
    assign AXI4__OUT__WVALID  = AXI4__IN__WVALID ;
    assign AXI4__OUT__WLAST   = AXI4__IN__WLAST  ;
    assign AXI4__OUT__BREADY  = AXI4__IN__BREADY ;
    assign AXI4__OUT__ARADDR  = AXI4__IN__ARADDR ;
    assign AXI4__OUT__ARLEN   = AXI4__IN__ARLEN  ;
    assign AXI4__OUT__ARSIZE  = AXI4__IN__ARSIZE ;
    assign AXI4__OUT__ARBURST = AXI4__IN__ARBURST;
    assign AXI4__OUT__ARLOCK  = AXI4__IN__ARLOCK ;
    assign AXI4__OUT__ARCACHE = AXI4__IN__ARCACHE;
    assign AXI4__OUT__ARPROT  = AXI4__IN__ARPROT ;
    assign AXI4__OUT__ARUSER  = AXI4__IN__ARUSER ;
    assign AXI4__OUT__ARVALID = AXI4__IN__ARVALID;
    assign AXI4__OUT__RREADY  = AXI4__IN__RREADY ;

    generate
        if  (IW_IN==IW_OUT) begin
            assign AXI4__IN__BID   = AXI4__OUT__BID;
            assign AXI4__IN__RID   = AXI4__OUT__RID;
            assign AXI4__OUT__AWID = AXI4__IN__AWID;
            assign AXI4__OUT__ARID = AXI4__IN__ARID;
        end
        else if (IW_IN < IW_OUT) begin
            assign AXI4__IN__BID   = AXI4__OUT__BID[IW_IN-1:0];
            assign AXI4__IN__RID   = AXI4__OUT__RID[IW_IN-1:0];
            assign AXI4__OUT__AWID = {{(IW_OUT-IW_IN){1'b0}},AXI4__IN__AWID};
            assign AXI4__OUT__ARID = {{(IW_OUT-IW_IN){1'b0}},AXI4__IN__ARID};
        end
        else begin
            assign AXI4__IN__BID   = {IW_IN{1'bx}};
            assign AXI4__IN__RID   = {IW_IN{1'bx}};
            assign AXI4__OUT__AWID = {IW_OUT{1'bx}};
            assign AXI4__OUT__ARID = {IW_OUT{1'bx}};
    
        end
    endgenerate
endmodule

module axi3_id_padder #(
    parameter IW_IN = 10,
    parameter IW_OUT = 2,
    parameter AW = 32,
    parameter DW = 64,
    parameter SW = DW/8,
    parameter UW = 10)
    (
    input   [AW-1:0]        AXI3__IN__AWADDR,
    input   [IW_IN-1:0]     AXI3__IN__AWID,
    input   [3:0]           AXI3__IN__AWLEN,
    input   [2:0]           AXI3__IN__AWSIZE,
    input   [1:0]           AXI3__IN__AWBURST,
    input   [1:0]           AXI3__IN__AWLOCK,
    input   [3:0]           AXI3__IN__AWCACHE,
    input   [2:0]           AXI3__IN__AWPROT,
    input   [UW-1:0]        AXI3__IN__AWUSER,
    input                   AXI3__IN__AWVALID,
    output                  AXI3__IN__AWREADY,
    input   [IW_IN-1:0]     AXI3__IN__WID,
    input   [DW-1:0]        AXI3__IN__WDATA,
    input   [SW-1:0]        AXI3__IN__WSTRB,
    input                   AXI3__IN__WVALID,
    input                   AXI3__IN__WLAST,
    output                  AXI3__IN__WREADY,
    output  [IW_IN-1:0]     AXI3__IN__BID,
    output                  AXI3__IN__BVALID,
    output  [1:0]           AXI3__IN__BRESP,
    input                   AXI3__IN__BREADY,
    input   [IW_IN-1:0]     AXI3__IN__ARID,
    input   [AW-1:0]        AXI3__IN__ARADDR,
    input   [3:0]           AXI3__IN__ARLEN,
    input   [2:0]           AXI3__IN__ARSIZE,
    input   [1:0]           AXI3__IN__ARBURST,
    input   [1:0]           AXI3__IN__ARLOCK,
    input   [3:0]           AXI3__IN__ARCACHE,
    input   [2:0]           AXI3__IN__ARPROT,
    input   [UW-1:0]        AXI3__IN__ARUSER,
    input                   AXI3__IN__ARVALID,
    output                  AXI3__IN__ARREADY,
    output  [IW_IN-1:0]     AXI3__IN__RID,
    output  [DW-1:0]        AXI3__IN__RDATA,
    output  [1:0]           AXI3__IN__RRESP,
    output                  AXI3__IN__RLAST,
    output                  AXI3__IN__RVALID,
    input                   AXI3__IN__RREADY,
    output  [AW-1:0]        AXI3__OUT__AWADDR,
    output  [IW_OUT-1:0]    AXI3__OUT__AWID,
    output  [3:0]           AXI3__OUT__AWLEN,
    output  [2:0]           AXI3__OUT__AWSIZE,
    output  [1:0]           AXI3__OUT__AWBURST,
    output  [1:0]           AXI3__OUT__AWLOCK,
    output  [3:0]           AXI3__OUT__AWCACHE,
    output  [2:0]           AXI3__OUT__AWPROT,
    output  [UW-1:0]        AXI3__OUT__AWUSER,
    output                  AXI3__OUT__AWVALID,
    input                   AXI3__OUT__AWREADY,
    output  [IW_OUT-1:0]    AXI3__OUT__WID,
    output  [DW-1:0]        AXI3__OUT__WDATA,
    output  [SW-1:0]        AXI3__OUT__WSTRB,
    output                  AXI3__OUT__WVALID,
    output                  AXI3__OUT__WLAST,
    input                   AXI3__OUT__WREADY,
    input   [IW_OUT-1:0]    AXI3__OUT__BID,
    input                   AXI3__OUT__BVALID,
    input   [1:0]           AXI3__OUT__BRESP,
    output                  AXI3__OUT__BREADY,
    output  [IW_OUT-1:0]    AXI3__OUT__ARID,
    output  [AW-1:0]        AXI3__OUT__ARADDR,
    output  [3:0]           AXI3__OUT__ARLEN,
    output  [2:0]           AXI3__OUT__ARSIZE,
    output  [1:0]           AXI3__OUT__ARBURST,
    output  [1:0]           AXI3__OUT__ARLOCK,
    output  [3:0]           AXI3__OUT__ARCACHE,
    output  [2:0]           AXI3__OUT__ARPROT,
    output  [UW-1:0]        AXI3__OUT__ARUSER,
    output                  AXI3__OUT__ARVALID,
    input                   AXI3__OUT__ARREADY,
    input   [IW_OUT-1:0]    AXI3__OUT__RID,
    input   [DW-1:0]        AXI3__OUT__RDATA,
    input   [1:0]           AXI3__OUT__RRESP,
    input                   AXI3__OUT__RLAST,
    input                   AXI3__OUT__RVALID,
    output                  AXI3__OUT__RREADY);

    
    assign AXI3__IN__AWREADY  = AXI3__OUT__AWREADY ;
    assign AXI3__IN__WREADY   = AXI3__OUT__WREADY  ;
    assign AXI3__IN__BVALID   = AXI3__OUT__BVALID  ;
    assign AXI3__IN__BRESP    = AXI3__OUT__BRESP   ;
    assign AXI3__IN__ARREADY  = AXI3__OUT__ARREADY ;
    assign AXI3__IN__RDATA    = AXI3__OUT__RDATA   ;
    assign AXI3__IN__RRESP    = AXI3__OUT__RRESP   ;
    assign AXI3__IN__RLAST    = AXI3__OUT__RLAST   ;
    assign AXI3__IN__RVALID   = AXI3__OUT__RVALID  ;
    assign AXI3__OUT__AWADDR  = AXI3__IN__AWADDR ;
    assign AXI3__OUT__AWLEN   = AXI3__IN__AWLEN  ;
    assign AXI3__OUT__AWSIZE  = AXI3__IN__AWSIZE ;
    assign AXI3__OUT__AWBURST = AXI3__IN__AWBURST;
    assign AXI3__OUT__AWLOCK  = AXI3__IN__AWLOCK ;
    assign AXI3__OUT__AWCACHE = AXI3__IN__AWCACHE;
    assign AXI3__OUT__AWPROT  = AXI3__IN__AWPROT ;
    assign AXI3__OUT__AWUSER  = AXI3__IN__AWUSER ;
    assign AXI3__OUT__AWVALID = AXI3__IN__AWVALID;
    assign AXI3__OUT__WDATA   = AXI3__IN__WDATA  ;
    assign AXI3__OUT__WSTRB   = AXI3__IN__WSTRB  ;
    assign AXI3__OUT__WVALID  = AXI3__IN__WVALID ;
    assign AXI3__OUT__WLAST   = AXI3__IN__WLAST  ;
    assign AXI3__OUT__BREADY  = AXI3__IN__BREADY ;
    assign AXI3__OUT__ARADDR  = AXI3__IN__ARADDR ;
    assign AXI3__OUT__ARLEN   = AXI3__IN__ARLEN  ;
    assign AXI3__OUT__ARSIZE  = AXI3__IN__ARSIZE ;
    assign AXI3__OUT__ARBURST = AXI3__IN__ARBURST;
    assign AXI3__OUT__ARLOCK  = AXI3__IN__ARLOCK ;
    assign AXI3__OUT__ARCACHE = AXI3__IN__ARCACHE;
    assign AXI3__OUT__ARPROT  = AXI3__IN__ARPROT ;
    assign AXI3__OUT__ARUSER  = AXI3__IN__ARUSER ;
    assign AXI3__OUT__ARVALID = AXI3__IN__ARVALID;
    assign AXI3__OUT__RREADY  = AXI3__IN__RREADY ;

    generate
        if  (IW_IN==IW_OUT) begin
            assign AXI3__IN__BID   = AXI3__OUT__BID;
            assign AXI3__IN__RID   = AXI3__OUT__RID;
            assign AXI3__OUT__AWID = AXI3__IN__AWID;
            assign AXI3__OUT__WID  = AXI3__IN__WID;
            assign AXI3__OUT__ARID = AXI3__IN__ARID;
        end
        else if (IW_IN < IW_OUT) begin
            assign AXI3__IN__BID   = AXI3__OUT__BID[IW_IN-1:0];
            assign AXI3__IN__RID   = AXI3__OUT__RID[IW_IN-1:0];
            assign AXI3__OUT__AWID = {{(IW_OUT-IW_IN){1'b0}},AXI3__IN__AWID};
            assign AXI3__OUT__WID  = {{(IW_OUT-IW_IN){1'b0}},AXI3__IN__WID};
            assign AXI3__OUT__ARID = {{(IW_OUT-IW_IN){1'b0}},AXI3__IN__ARID};
        end
        else begin
            assign AXI3__IN__BID   = {IW_IN{1'bx}};
            assign AXI3__IN__RID   = {IW_IN{1'bx}};
            assign AXI3__OUT__AWID = {IW_OUT{1'bx}};
            assign AXI3__OUT__WID = {IW_OUT{1'bx}};
            assign AXI3__OUT__ARID = {IW_OUT{1'bx}};
        end
    endgenerate
endmodule
