//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_DCache_assert(
  input         reset,
  input  [7:0]  io_cpu_s1_data_mask,
  input         io_cpu_s2_nack,
  input         clock_gate_0_out,
  input         tlb_clock,
  input         tlb_reset,
  input         tlb_io_sfence_valid,
  input         tlb_io_sfence_bits_rs1,
  input  [26:0] tlb_vpn,
  input         pma_checker_clock,
  input         pma_checker_reset,
  input         pma_checker_io_sfence_valid,
  input         pma_checker_io_sfence_bits_rs1,
  input  [26:0] pma_checker_vpn,
  input         release_ack_wait,
  input         s1_valid_masked,
  input  [39:0] s1_req_addr,
  input         _T_53,
  input  [7:0]  s1_mask_xwr,
  input         cached_grant_wait,
  input         uncachedInFlight_0,
  input         uncachedInFlight_1,
  input         uncachedInFlight_2,
  input         uncachedInFlight_3,
  input         uncachedInFlight_4,
  input         uncachedInFlight_5,
  input         uncachedInFlight_6,
  input         res,
  input         _T_161,
  input         s2_flush_valid_pre_tag_ecc,
  input         grantIsCached,
  input         d_first,
  input         tl_out__e_ready,
  input         _T_3706,
  input         grantIsUncached,
  input         s2_valid_flush_line,
  input         s2_want_victimize,
  input         _T_1491,
  input         pstore1_rmw,
  input         pstore1_valid,
  input         _T_1702,
  input         grantIsVoluntary,
  input         _T_3717,
  input         _T_3721,
  input         _T_3725,
  input         _T_3729,
  input         _T_3733,
  input         _T_3737,
  input         _T_3741,
  input         tl_out__e_valid,
  input         doUncachedResp
);
  wire [38:0] tlb_io_sfence_bits_addr;
  wire  tlb__T_2476; // @[TLB.scala 373:14]
  wire [26:0] tlb__T_2477; // @[TLB.scala 373:58]
  wire  tlb__T_2478; // @[TLB.scala 373:72]
  wire  tlb__T_2479; // @[TLB.scala 373:34]
  wire  tlb__T_2481; // @[TLB.scala 373:13]
  wire  tlb__T_2482; // @[TLB.scala 373:13]
  wire [38:0] pma_checker_io_sfence_bits_addr;
  wire  pma_checker__T_2476; // @[TLB.scala 373:14]
  wire [26:0] pma_checker__T_2477; // @[TLB.scala 373:58]
  wire  pma_checker__T_2478; // @[TLB.scala 373:72]
  wire  pma_checker__T_2479; // @[TLB.scala 373:34]
  wire  pma_checker__T_2481; // @[TLB.scala 373:13]
  wire  pma_checker__T_2482; // @[TLB.scala 373:13]
  wire  _T_162; // @[DCache.scala 1080:12]
  wire  _T_163; // @[DCache.scala 1080:28]
  wire  _T_165; // @[DCache.scala 1080:11]
  wire  _T_166; // @[DCache.scala 1080:11]
  wire  _T_363; // @[DCache.scala 281:28]
  wire  _T_364; // @[DCache.scala 281:10]
  wire [7:0] _T_365; // @[DCache.scala 281:71]
  wire [7:0] _T_366; // @[DCache.scala 281:69]
  wire  _T_367; // @[DCache.scala 281:93]
  wire  _T_368; // @[DCache.scala 281:53]
  wire  _T_370; // @[DCache.scala 281:9]
  wire  _T_371; // @[DCache.scala 281:9]
  wire  _T_1703; // @[DCache.scala 461:63]
  wire  _T_1704; // @[DCache.scala 461:22]
  wire  _T_1706; // @[DCache.scala 461:9]
  wire  _T_1707; // @[DCache.scala 461:9]
  wire  _T_3571; // @[DCache.scala 606:129]
  wire  _T_3708; // @[DCache.scala 620:13]
  wire  _T_3709; // @[DCache.scala 620:13]
  wire  _T_3719; // @[DCache.scala 630:17]
  wire  _T_3720; // @[DCache.scala 630:17]
  wire  _T_3723; // @[DCache.scala 630:17]
  wire  _T_3724; // @[DCache.scala 630:17]
  wire  _T_3727; // @[DCache.scala 630:17]
  wire  _T_3728; // @[DCache.scala 630:17]
  wire  _T_3731; // @[DCache.scala 630:17]
  wire  _T_3732; // @[DCache.scala 630:17]
  wire  _T_3735; // @[DCache.scala 630:17]
  wire  _T_3736; // @[DCache.scala 630:17]
  wire  _T_3739; // @[DCache.scala 630:17]
  wire  _T_3740; // @[DCache.scala 630:17]
  wire  _T_3743; // @[DCache.scala 630:17]
  wire  _T_3744; // @[DCache.scala 630:17]
  wire  _T_3750; // @[DCache.scala 651:13]
  wire  _T_3751; // @[DCache.scala 651:13]
  wire  _T_3756; // @[Decoupled.scala 40:37]
  wire  _T_3758; // @[DCache.scala 659:47]
  wire  _T_3759; // @[DCache.scala 659:58]
  wire  _T_3760; // @[DCache.scala 659:26]
  wire  _T_3762; // @[DCache.scala 659:9]
  wire  _T_3763; // @[DCache.scala 659:9]
  wire  _T_3907; // @[DCache.scala 739:34]
  wire  _T_3908; // @[DCache.scala 739:52]
  wire  _T_3910; // @[DCache.scala 739:13]
  wire  _T_3911; // @[DCache.scala 739:13]
  wire  _T_4056; // @[DCache.scala 851:11]
  wire  _T_4057; // @[DCache.scala 851:11]
  wire  _T_4360; // @[DCache.scala 1027:35]
  wire  _GEN_646; // @[DCache.scala 620:13]
  wire  _GEN_649; // @[DCache.scala 630:17]
  wire  _GEN_650; // @[DCache.scala 630:17]
  wire  _GEN_651; // @[DCache.scala 630:17]
  wire  _GEN_659; // @[DCache.scala 630:17]
  wire  _GEN_667; // @[DCache.scala 630:17]
  wire  _GEN_675; // @[DCache.scala 630:17]
  wire  _GEN_683; // @[DCache.scala 630:17]
  wire  _GEN_691; // @[DCache.scala 630:17]
  wire  _GEN_699; // @[DCache.scala 630:17]
  wire  _GEN_707; // @[DCache.scala 651:13]
  wire  _GEN_708; // @[DCache.scala 651:13]
  assign tlb__T_2476 = ~tlb_io_sfence_bits_rs1; // @[TLB.scala 373:14]
  assign tlb__T_2477 = tlb_io_sfence_bits_addr[38:12]; // @[TLB.scala 373:58]
  assign tlb__T_2478 = tlb__T_2477 == tlb_vpn; // @[TLB.scala 373:72]
  assign tlb__T_2479 = tlb__T_2476 | tlb__T_2478; // @[TLB.scala 373:34]
  assign tlb__T_2481 = tlb__T_2479 | tlb_reset; // @[TLB.scala 373:13]
  assign tlb__T_2482 = ~tlb__T_2481; // @[TLB.scala 373:13]
  assign pma_checker__T_2476 = ~pma_checker_io_sfence_bits_rs1; // @[TLB.scala 373:14]
  assign pma_checker__T_2477 = pma_checker_io_sfence_bits_addr[38:12]; // @[TLB.scala 373:58]
  assign pma_checker__T_2478 = pma_checker__T_2477 == pma_checker_vpn; // @[TLB.scala 373:72]
  assign pma_checker__T_2479 = pma_checker__T_2476 | pma_checker__T_2478; // @[TLB.scala 373:34]
  assign pma_checker__T_2481 = pma_checker__T_2479 | pma_checker_reset; // @[TLB.scala 373:13]
  assign pma_checker__T_2482 = ~pma_checker__T_2481; // @[TLB.scala 373:13]
  assign _T_162 = ~_T_161; // @[DCache.scala 1080:12]
  assign _T_163 = _T_162 | res; // @[DCache.scala 1080:28]
  assign _T_165 = _T_163 | reset; // @[DCache.scala 1080:11]
  assign _T_166 = ~_T_165; // @[DCache.scala 1080:11]
  assign _T_363 = s1_valid_masked & _T_53; // @[DCache.scala 281:28]
  assign _T_364 = ~_T_363; // @[DCache.scala 281:10]
  assign _T_365 = ~io_cpu_s1_data_mask; // @[DCache.scala 281:71]
  assign _T_366 = s1_mask_xwr | _T_365; // @[DCache.scala 281:69]
  assign _T_367 = _T_366 == 8'hff; // @[DCache.scala 281:93]
  assign _T_368 = _T_364 | _T_367; // @[DCache.scala 281:53]
  assign _T_370 = _T_368 | reset; // @[DCache.scala 281:9]
  assign _T_371 = ~_T_370; // @[DCache.scala 281:9]
  assign _T_1703 = _T_1702 == pstore1_valid; // @[DCache.scala 461:63]
  assign _T_1704 = pstore1_rmw | _T_1703; // @[DCache.scala 461:22]
  assign _T_1706 = _T_1704 | reset; // @[DCache.scala 461:9]
  assign _T_1707 = ~_T_1706; // @[DCache.scala 461:9]
  assign _T_3571 = ~grantIsUncached; // @[DCache.scala 606:129]
  assign _T_3708 = cached_grant_wait | reset; // @[DCache.scala 620:13]
  assign _T_3709 = ~_T_3708; // @[DCache.scala 620:13]
  assign _T_3719 = uncachedInFlight_0 | reset; // @[DCache.scala 630:17]
  assign _T_3720 = ~_T_3719; // @[DCache.scala 630:17]
  assign _T_3723 = uncachedInFlight_1 | reset; // @[DCache.scala 630:17]
  assign _T_3724 = ~_T_3723; // @[DCache.scala 630:17]
  assign _T_3727 = uncachedInFlight_2 | reset; // @[DCache.scala 630:17]
  assign _T_3728 = ~_T_3727; // @[DCache.scala 630:17]
  assign _T_3731 = uncachedInFlight_3 | reset; // @[DCache.scala 630:17]
  assign _T_3732 = ~_T_3731; // @[DCache.scala 630:17]
  assign _T_3735 = uncachedInFlight_4 | reset; // @[DCache.scala 630:17]
  assign _T_3736 = ~_T_3735; // @[DCache.scala 630:17]
  assign _T_3739 = uncachedInFlight_5 | reset; // @[DCache.scala 630:17]
  assign _T_3740 = ~_T_3739; // @[DCache.scala 630:17]
  assign _T_3743 = uncachedInFlight_6 | reset; // @[DCache.scala 630:17]
  assign _T_3744 = ~_T_3743; // @[DCache.scala 630:17]
  assign _T_3750 = release_ack_wait | reset; // @[DCache.scala 651:13]
  assign _T_3751 = ~_T_3750; // @[DCache.scala 651:13]
  assign _T_3756 = tl_out__e_ready & tl_out__e_valid; // @[Decoupled.scala 40:37]
  assign _T_3758 = _T_3706 & d_first; // @[DCache.scala 659:47]
  assign _T_3759 = _T_3758 & grantIsCached; // @[DCache.scala 659:58]
  assign _T_3760 = _T_3756 == _T_3759; // @[DCache.scala 659:26]
  assign _T_3762 = _T_3760 | reset; // @[DCache.scala 659:9]
  assign _T_3763 = ~_T_3762; // @[DCache.scala 659:9]
  assign _T_3907 = s2_valid_flush_line | s2_flush_valid_pre_tag_ecc; // @[DCache.scala 739:34]
  assign _T_3908 = _T_3907 | io_cpu_s2_nack; // @[DCache.scala 739:52]
  assign _T_3910 = _T_3908 | reset; // @[DCache.scala 739:13]
  assign _T_3911 = ~_T_3910; // @[DCache.scala 739:13]
  assign _T_4056 = _T_1491 | reset; // @[DCache.scala 851:11]
  assign _T_4057 = ~_T_4056; // @[DCache.scala 851:11]
  assign _T_4360 = ~grantIsCached; // @[DCache.scala 1027:35]
  assign tlb_io_sfence_bits_addr = s1_req_addr[38:0]; // @[DCache.scala 241:27]
  assign pma_checker_io_sfence_bits_addr = 39'h0;
  assign _GEN_646 = _T_3706 & grantIsCached; // @[DCache.scala 620:13]
  assign _GEN_649 = _T_3706 & _T_4360; // @[DCache.scala 630:17]
  assign _GEN_650 = _GEN_649 & grantIsUncached; // @[DCache.scala 630:17]
  assign _GEN_651 = _GEN_650 & _T_3717; // @[DCache.scala 630:17]
  assign _GEN_659 = _GEN_650 & _T_3721; // @[DCache.scala 630:17]
  assign _GEN_667 = _GEN_650 & _T_3725; // @[DCache.scala 630:17]
  assign _GEN_675 = _GEN_650 & _T_3729; // @[DCache.scala 630:17]
  assign _GEN_683 = _GEN_650 & _T_3733; // @[DCache.scala 630:17]
  assign _GEN_691 = _GEN_650 & _T_3737; // @[DCache.scala 630:17]
  assign _GEN_699 = _GEN_650 & _T_3741; // @[DCache.scala 630:17]
  assign _GEN_707 = _GEN_649 & _T_3571; // @[DCache.scala 651:13]
  assign _GEN_708 = _GEN_707 & grantIsVoluntary; // @[DCache.scala 651:13]
  always @(posedge tlb_clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (tlb_io_sfence_valid & tlb__T_2482) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at TLB.scala:373 assert(!io.sfence.bits.rs1 || (io.sfence.bits.addr >> pgIdxBits) === vpn)\n"); // @[TLB.scala 373:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (tlb_io_sfence_valid & tlb__T_2482) begin
          $fatal; // @[TLB.scala 373:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end
  always @(posedge pma_checker_clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (pma_checker_io_sfence_valid & pma_checker__T_2482) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at TLB.scala:373 assert(!io.sfence.bits.rs1 || (io.sfence.bits.addr >> pgIdxBits) === vpn)\n"); // @[TLB.scala 373:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (pma_checker_io_sfence_valid & pma_checker__T_2482) begin
          $fatal; // @[TLB.scala 373:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end
  always @(posedge clock_gate_0_out) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_166) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at DCache.scala:1080 assert(!needsRead(req) || res)\n"); // @[DCache.scala 1080:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_166) begin
          $fatal; // @[DCache.scala 1080:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_371) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at DCache.scala:281 assert(!(s1_valid_masked && s1_req.cmd === M_PWR) || (s1_mask_xwr | ~io.cpu.s1_data.mask).andR)\n"); // @[DCache.scala 281:9]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_371) begin
          $fatal; // @[DCache.scala 281:9]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_166) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at DCache.scala:1080 assert(!needsRead(req) || res)\n"); // @[DCache.scala 1080:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_166) begin
          $fatal; // @[DCache.scala 1080:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1707) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at DCache.scala:461 assert(pstore1_rmw || pstore1_valid_not_rmw(io.cpu.s2_kill) === pstore1_valid)\n"); // @[DCache.scala 461:9]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1707) begin
          $fatal; // @[DCache.scala 461:9]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_646 & _T_3709) begin
          $fwrite(32'h80000002,"Assertion Failed: A GrantData was unexpected by the dcache.\n    at DCache.scala:620 assert(cached_grant_wait, \"A GrantData was unexpected by the dcache.\")\n"); // @[DCache.scala 620:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_646 & _T_3709) begin
          $fatal; // @[DCache.scala 620:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_651 & _T_3720) begin
          $fwrite(32'h80000002,"Assertion Failed: An AccessAck was unexpected by the dcache.\n    at DCache.scala:630 assert(f, \"An AccessAck was unexpected by the dcache.\") // TODO must handle Ack coming back on same cycle!\n"); // @[DCache.scala 630:17]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_651 & _T_3720) begin
          $fatal; // @[DCache.scala 630:17]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_659 & _T_3724) begin
          $fwrite(32'h80000002,"Assertion Failed: An AccessAck was unexpected by the dcache.\n    at DCache.scala:630 assert(f, \"An AccessAck was unexpected by the dcache.\") // TODO must handle Ack coming back on same cycle!\n"); // @[DCache.scala 630:17]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_659 & _T_3724) begin
          $fatal; // @[DCache.scala 630:17]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_667 & _T_3728) begin
          $fwrite(32'h80000002,"Assertion Failed: An AccessAck was unexpected by the dcache.\n    at DCache.scala:630 assert(f, \"An AccessAck was unexpected by the dcache.\") // TODO must handle Ack coming back on same cycle!\n"); // @[DCache.scala 630:17]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_667 & _T_3728) begin
          $fatal; // @[DCache.scala 630:17]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_675 & _T_3732) begin
          $fwrite(32'h80000002,"Assertion Failed: An AccessAck was unexpected by the dcache.\n    at DCache.scala:630 assert(f, \"An AccessAck was unexpected by the dcache.\") // TODO must handle Ack coming back on same cycle!\n"); // @[DCache.scala 630:17]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_675 & _T_3732) begin
          $fatal; // @[DCache.scala 630:17]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_683 & _T_3736) begin
          $fwrite(32'h80000002,"Assertion Failed: An AccessAck was unexpected by the dcache.\n    at DCache.scala:630 assert(f, \"An AccessAck was unexpected by the dcache.\") // TODO must handle Ack coming back on same cycle!\n"); // @[DCache.scala 630:17]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_683 & _T_3736) begin
          $fatal; // @[DCache.scala 630:17]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_691 & _T_3740) begin
          $fwrite(32'h80000002,"Assertion Failed: An AccessAck was unexpected by the dcache.\n    at DCache.scala:630 assert(f, \"An AccessAck was unexpected by the dcache.\") // TODO must handle Ack coming back on same cycle!\n"); // @[DCache.scala 630:17]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_691 & _T_3740) begin
          $fatal; // @[DCache.scala 630:17]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_699 & _T_3744) begin
          $fwrite(32'h80000002,"Assertion Failed: An AccessAck was unexpected by the dcache.\n    at DCache.scala:630 assert(f, \"An AccessAck was unexpected by the dcache.\") // TODO must handle Ack coming back on same cycle!\n"); // @[DCache.scala 630:17]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_699 & _T_3744) begin
          $fatal; // @[DCache.scala 630:17]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_708 & _T_3751) begin
          $fwrite(32'h80000002,"Assertion Failed: A ReleaseAck was unexpected by the dcache.\n    at DCache.scala:651 assert(release_ack_wait, \"A ReleaseAck was unexpected by the dcache.\") // TODO should handle Ack coming back on same cycle!\n"); // @[DCache.scala 651:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_708 & _T_3751) begin
          $fatal; // @[DCache.scala 651:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_3763) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at DCache.scala:659 assert(tl_out.e.fire() === (tl_out.d.fire() && d_first && grantIsCached))\n"); // @[DCache.scala 659:9]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_3763) begin
          $fatal; // @[DCache.scala 659:9]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (s2_want_victimize & _T_3911) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at DCache.scala:739 assert(s2_valid_flush_line || s2_flush_valid || io.cpu.s2_nack)\n"); // @[DCache.scala 739:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (s2_want_victimize & _T_3911) begin
          $fatal; // @[DCache.scala 739:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (doUncachedResp & _T_4057) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at DCache.scala:851 assert(!s2_valid_hit)\n"); // @[DCache.scala 851:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (doUncachedResp & _T_4057) begin
          $fatal; // @[DCache.scala 851:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
