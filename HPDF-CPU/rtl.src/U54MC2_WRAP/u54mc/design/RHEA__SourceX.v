//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__SourceX(
  input   clock,
  input   reset,
  output  io_req_ready,
  input   io_req_valid,
  input   io_x_ready,
  output  io_x_valid
);
  wire  io_x_q_clock; // @[Decoupled.scala 296:21]
  wire  io_x_q_reset; // @[Decoupled.scala 296:21]
  wire  io_x_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  io_x_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire  io_x_q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  io_x_q_io_deq_valid; // @[Decoupled.scala 296:21]
  RHEA__Queue_122 io_x_q ( // @[Decoupled.scala 296:21]
    .clock(io_x_q_clock),
    .reset(io_x_q_reset),
    .io_enq_ready(io_x_q_io_enq_ready),
    .io_enq_valid(io_x_q_io_enq_valid),
    .io_deq_ready(io_x_q_io_deq_ready),
    .io_deq_valid(io_x_q_io_deq_valid)
  );
  assign io_req_ready = io_x_q_io_enq_ready; // @[SourceX.scala 40:15 Decoupled.scala 299:17]
  assign io_x_valid = io_x_q_io_deq_valid; // @[SourceX.scala 41:8]
  assign io_x_q_clock = clock;
  assign io_x_q_reset = reset;
  assign io_x_q_io_enq_valid = io_req_valid; // @[SourceX.scala 40:15 SourceX.scala 44:11]
  assign io_x_q_io_deq_ready = io_x_ready; // @[SourceX.scala 41:8]
endmodule
