//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLTraceSink_2(
  input         clock,
  input         reset,
  input         auto_sba_out_a_ready,
  output        auto_sba_out_a_valid,
  output [1:0]  auto_sba_out_a_bits_source,
  output [36:0] auto_sba_out_a_bits_address,
  output [31:0] auto_sba_out_a_bits_data,
  input         auto_sba_out_d_valid,
  input  [1:0]  auto_sba_out_d_bits_source,
  input         auto_sba_out_d_bits_denied,
  input         auto_sink_out_a_ready,
  output        auto_sink_out_a_valid,
  output [2:0]  auto_sink_out_a_bits_opcode,
  output        auto_sink_out_a_bits_source,
  output [30:0] auto_sink_out_a_bits_address,
  output [31:0] auto_sink_out_a_bits_data,
  input         auto_sink_out_d_valid,
  input         auto_sink_out_d_bits_denied,
  input  [31:0] auto_sink_out_d_bits_data,
  input         io_teActive,
  input  [3:0]  io_teControl_teSink,
  input         io_teControl_teStopOnWrap,
  output        io_stream_ready,
  input         io_stream_valid,
  input  [31:0] io_stream_bits_data,
  input  [31:0] io_streamBaseH,
  input  [29:0] io_streamWP,
  output        io_streamWPInc,
  input         io_streamWPWrap,
  input         io_busWrEn,
  input         io_busRdEn,
  output        io_busReady,
  input  [5:0]  io_busAP,
  input  [31:0] io_busWrData,
  output [31:0] io_busRdData,
  output        io_sinkError
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
`endif // RANDOMIZE_REG_INIT
  wire  sbaSinkSelected = io_teControl_teSink == 4'h7; // @[TraceSink.scala 289:33]
  wire  wrapStop = io_teControl_teStopOnWrap & io_streamWPWrap; // @[TraceSink.scala 291:57]
  reg  tlState; // @[TraceSink.scala 300:31]
  reg  inFlight_0; // @[TraceSink.scala 302:31]
  reg  inFlight_1; // @[TraceSink.scala 302:31]
  reg  inFlight_2; // @[TraceSink.scala 302:31]
  reg  inFlight_3; // @[TraceSink.scala 302:31]
  wire  sbaFifoFull = inFlight_0 & inFlight_1 & inFlight_2 & inFlight_3; // @[package.scala 71:59]
  wire [63:0] _putAddr_T = {io_streamBaseH, 32'h0}; // @[TraceSink.scala 304:44]
  wire [31:0] _putAddr_T_1 = {io_streamWP, 2'h0}; // @[TraceSink.scala 304:66]
  wire [63:0] _GEN_32 = {{32'd0}, _putAddr_T_1}; // @[TraceSink.scala 304:51]
  wire [63:0] putAddr = _putAddr_T | _GEN_32; // @[TraceSink.scala 304:51]
  wire [64:0] _legalPut_T_5 = {1'b0,$signed(putAddr)}; // @[Parameters.scala 137:49]
  wire [64:0] _legalPut_T_7 = $signed(_legalPut_T_5) & -65'sh5000; // @[Parameters.scala 137:52]
  wire  _legalPut_T_8 = $signed(_legalPut_T_7) == 65'sh0; // @[Parameters.scala 137:67]
  wire [63:0] _legalPut_T_9 = putAddr ^ 64'h2000000; // @[Parameters.scala 137:31]
  wire [64:0] _legalPut_T_10 = {1'b0,$signed(_legalPut_T_9)}; // @[Parameters.scala 137:49]
  wire [64:0] _legalPut_T_12 = $signed(_legalPut_T_10) & -65'sh10000; // @[Parameters.scala 137:52]
  wire  _legalPut_T_13 = $signed(_legalPut_T_12) == 65'sh0; // @[Parameters.scala 137:67]
  wire [63:0] _legalPut_T_14 = putAddr ^ 64'h2010000; // @[Parameters.scala 137:31]
  wire [64:0] _legalPut_T_15 = {1'b0,$signed(_legalPut_T_14)}; // @[Parameters.scala 137:49]
  wire [64:0] _legalPut_T_17 = $signed(_legalPut_T_15) & -65'sh4000; // @[Parameters.scala 137:52]
  wire  _legalPut_T_18 = $signed(_legalPut_T_17) == 65'sh0; // @[Parameters.scala 137:67]
  wire [63:0] _legalPut_T_19 = putAddr ^ 64'h8000000; // @[Parameters.scala 137:31]
  wire [64:0] _legalPut_T_20 = {1'b0,$signed(_legalPut_T_19)}; // @[Parameters.scala 137:49]
  wire [64:0] _legalPut_T_22 = $signed(_legalPut_T_20) & -65'sh2200000; // @[Parameters.scala 137:52]
  wire  _legalPut_T_23 = $signed(_legalPut_T_22) == 65'sh0; // @[Parameters.scala 137:67]
  wire [63:0] _legalPut_T_24 = putAddr ^ 64'hc000000; // @[Parameters.scala 137:31]
  wire [64:0] _legalPut_T_25 = {1'b0,$signed(_legalPut_T_24)}; // @[Parameters.scala 137:49]
  wire [64:0] _legalPut_T_27 = $signed(_legalPut_T_25) & -65'sh4000000; // @[Parameters.scala 137:52]
  wire  _legalPut_T_28 = $signed(_legalPut_T_27) == 65'sh0; // @[Parameters.scala 137:67]
  wire [63:0] _legalPut_T_29 = putAddr ^ 64'h10000000; // @[Parameters.scala 137:31]
  wire [64:0] _legalPut_T_30 = {1'b0,$signed(_legalPut_T_29)}; // @[Parameters.scala 137:49]
  wire [64:0] _legalPut_T_32 = $signed(_legalPut_T_30) & -65'sh2000; // @[Parameters.scala 137:52]
  wire  _legalPut_T_33 = $signed(_legalPut_T_32) == 65'sh0; // @[Parameters.scala 137:67]
  wire [63:0] _legalPut_T_34 = putAddr ^ 64'h10003000; // @[Parameters.scala 137:31]
  wire [64:0] _legalPut_T_35 = {1'b0,$signed(_legalPut_T_34)}; // @[Parameters.scala 137:49]
  wire [64:0] _legalPut_T_37 = $signed(_legalPut_T_35) & -65'sh1000; // @[Parameters.scala 137:52]
  wire  _legalPut_T_38 = $signed(_legalPut_T_37) == 65'sh0; // @[Parameters.scala 137:67]
  wire [63:0] _legalPut_T_39 = putAddr ^ 64'h80000000; // @[Parameters.scala 137:31]
  wire [64:0] _legalPut_T_40 = {1'b0,$signed(_legalPut_T_39)}; // @[Parameters.scala 137:49]
  wire [64:0] _legalPut_T_42 = $signed(_legalPut_T_40) & -65'sh80000000; // @[Parameters.scala 137:52]
  wire  _legalPut_T_43 = $signed(_legalPut_T_42) == 65'sh0; // @[Parameters.scala 137:67]
  wire [63:0] _legalPut_T_44 = putAddr ^ 64'h800000000; // @[Parameters.scala 137:31]
  wire [64:0] _legalPut_T_45 = {1'b0,$signed(_legalPut_T_44)}; // @[Parameters.scala 137:49]
  wire [64:0] _legalPut_T_47 = $signed(_legalPut_T_45) & -65'sh800000000; // @[Parameters.scala 137:52]
  wire  _legalPut_T_48 = $signed(_legalPut_T_47) == 65'sh0; // @[Parameters.scala 137:67]
  wire  _legalPut_T_56 = _legalPut_T_8 | _legalPut_T_13 | _legalPut_T_18 | _legalPut_T_23 | _legalPut_T_28 |
    _legalPut_T_33 | _legalPut_T_38 | _legalPut_T_43 | _legalPut_T_48; // @[Parameters.scala 686:42]
  wire [63:0] _legalPut_T_62 = putAddr ^ 64'h3000; // @[Parameters.scala 137:31]
  wire [64:0] _legalPut_T_63 = {1'b0,$signed(_legalPut_T_62)}; // @[Parameters.scala 137:49]
  wire [64:0] _legalPut_T_65 = $signed(_legalPut_T_63) & -65'sh1000; // @[Parameters.scala 137:52]
  wire  _legalPut_T_66 = $signed(_legalPut_T_65) == 65'sh0; // @[Parameters.scala 137:67]
  wire [63:0] _legalPut_T_72 = putAddr ^ 64'h1000000000; // @[Parameters.scala 137:31]
  wire [64:0] _legalPut_T_73 = {1'b0,$signed(_legalPut_T_72)}; // @[Parameters.scala 137:49]
  wire [64:0] _legalPut_T_75 = $signed(_legalPut_T_73) & -65'sh1000000000; // @[Parameters.scala 137:52]
  wire  _legalPut_T_76 = $signed(_legalPut_T_75) == 65'sh0; // @[Parameters.scala 137:67]
  wire  legalPut = _legalPut_T_56 | _legalPut_T_66 | _legalPut_T_76; // @[Parameters.scala 687:30]
  wire  _writeGated_T = sbaSinkSelected & io_stream_valid; // @[TraceSink.scala 306:49]
  wire  _writeGated_T_1 = _writeGated_T & legalPut; // @[TraceSink.scala 306:67]
  wire  _writeGated_T_2 = ~wrapStop; // @[TraceSink.scala 306:80]
  wire  _writeGated_T_4 = ~sbaFifoFull; // @[TraceSink.scala 306:92]
  wire  writeGated = _writeGated_T_1 & ~wrapStop & ~sbaFifoFull; // @[TraceSink.scala 306:90]
  wire  _T = ~io_teActive; // @[TraceSink.scala 308:13]
  wire  _GEN_0 = auto_sba_out_a_ready ? 1'h0 : tlState; // @[TraceSink.scala 312:31 TraceSink.scala 313:17 TraceSink.scala 300:31]
  wire  _GEN_1 = ~tlState & writeGated & ~auto_sba_out_a_ready | _GEN_0; // @[TraceSink.scala 310:56 TraceSink.scala 311:17]
  wire [1:0] _pfbits_T_4 = ~inFlight_2 ? 2'h2 : 2'h3; // @[TraceSink.scala 316:53]
  wire [1:0] _pfbits_T_5 = ~inFlight_1 ? 2'h1 : _pfbits_T_4; // @[TraceSink.scala 316:53]
  wire [1:0] pfbits_source = ~inFlight_0 ? 2'h0 : _pfbits_T_5; // @[TraceSink.scala 316:53]
  wire  bundleOut_0_1_a_valid = writeGated | tlState; // @[TraceSink.scala 318:32]
  wire  _T_6 = auto_sba_out_a_ready & bundleOut_0_1_a_valid; // @[Decoupled.scala 40:37]
  wire  _GEN_3 = 2'h0 == pfbits_source | inFlight_0; // @[TraceSink.scala 336:38 TraceSink.scala 336:38 TraceSink.scala 302:31]
  wire  _GEN_4 = 2'h1 == pfbits_source | inFlight_1; // @[TraceSink.scala 336:38 TraceSink.scala 336:38 TraceSink.scala 302:31]
  wire  _GEN_5 = 2'h2 == pfbits_source | inFlight_2; // @[TraceSink.scala 336:38 TraceSink.scala 336:38 TraceSink.scala 302:31]
  wire  _GEN_6 = 2'h3 == pfbits_source | inFlight_3; // @[TraceSink.scala 336:38 TraceSink.scala 336:38 TraceSink.scala 302:31]
  wire  _GEN_7 = _T_6 ? _GEN_3 : inFlight_0; // @[TraceSink.scala 335:28 TraceSink.scala 302:31]
  wire  _GEN_8 = _T_6 ? _GEN_4 : inFlight_1; // @[TraceSink.scala 335:28 TraceSink.scala 302:31]
  wire  _GEN_9 = _T_6 ? _GEN_5 : inFlight_2; // @[TraceSink.scala 335:28 TraceSink.scala 302:31]
  wire  _GEN_10 = _T_6 ? _GEN_6 : inFlight_3; // @[TraceSink.scala 335:28 TraceSink.scala 302:31]
  wire  _sbaSinkError_T_1 = ~legalPut; // @[TraceSink.scala 343:78]
  wire  sbaSinkError = auto_sba_out_d_valid & auto_sba_out_d_bits_denied | io_stream_valid & _sbaSinkError_T_1; // @[TraceSink.scala 343:57]
  wire  sbaStreamReady = wrapStop | auto_sba_out_a_ready & _writeGated_T_4 | _sbaSinkError_T_1; // @[TraceSink.scala 344:64]
  wire  sbaStreamWPInc = writeGated & sbaStreamReady; // @[TraceSink.scala 345:36]
  reg [3:0] tlState_1; // @[TraceSink.scala 358:31]
  wire  writeGated_1 = ~sbaSinkSelected & io_stream_valid & _writeGated_T_2; // @[TraceSink.scala 360:69]
  wire  _T_27 = ~auto_sink_out_d_valid; // @[TraceSink.scala 367:47]
  wire [3:0] _GEN_23 = io_busRdEn ? 4'hd : tlState_1; // @[TraceSink.scala 371:33 TraceSink.scala 372:19 TraceSink.scala 358:31]
  wire [3:0] _GEN_24 = io_busWrEn ? 4'h9 : _GEN_23; // @[TraceSink.scala 369:33 TraceSink.scala 370:19]
  wire  _T_30 = tlState_1[0] & auto_sink_out_a_ready; // @[TraceSink.scala 374:30]
  wire [3:0] _tlState_T = tlState_1 ^ 4'h3; // @[TraceSink.scala 377:27]
  wire [3:0] _GEN_27 = tlState_1[1] & auto_sink_out_d_valid ? 4'h0 : tlState_1; // @[TraceSink.scala 378:44 TraceSink.scala 379:17 TraceSink.scala 358:31]
  wire [31:0] _pfbits_T_7 = {io_teControl_teSink, 28'h0}; // @[TraceSink.scala 382:68]
  wire [31:0] _pfbits_T_9 = _pfbits_T_7 | _putAddr_T_1; // @[TraceSink.scala 382:75]
  wire [7:0] _rbits_T_1 = {io_busAP, 2'h0}; // @[TraceSink.scala 384:80]
  wire [31:0] _GEN_37 = {{24'd0}, _rbits_T_1}; // @[TraceSink.scala 384:69]
  wire [31:0] _rbits_T_2 = _pfbits_T_7 | _GEN_37; // @[TraceSink.scala 384:69]
  wire [2:0] _bundleOut_0_a_bits_T_2_opcode = tlState_1[2] ? 3'h4 : 3'h0; // @[TraceSink.scala 386:41]
  wire [30:0] rbits_address = _rbits_T_2[30:0]; // @[Edges.scala 447:17 Edges.scala 452:15]
  wire [30:0] _bundleOut_0_a_bits_T_2_address = tlState_1[2] ? rbits_address : rbits_address; // @[TraceSink.scala 386:41]
  wire [31:0] _bundleOut_0_a_bits_T_2_data = tlState_1[2] ? 32'h0 : io_busWrData; // @[TraceSink.scala 386:41]
  wire  pfbits_1_source = io_streamWP[0]; // @[Edges.scala 465:17 Edges.scala 469:15]
  wire [30:0] pfbits_1_address = _pfbits_T_9[30:0]; // @[Edges.scala 465:17 Edges.scala 470:15]
  wire  otherSinkError = auto_sink_out_d_valid & auto_sink_out_d_bits_denied; // @[TraceSink.scala 404:38]
  wire  otherStreamReady = wrapStop | auto_sink_out_a_ready & ~tlState_1[3]; // @[TraceSink.scala 405:36]
  wire  otherStreamWPInc = writeGated_1 & otherStreamReady; // @[TraceSink.scala 406:38]
  assign auto_sba_out_a_valid = writeGated | tlState; // @[TraceSink.scala 318:32]
  assign auto_sba_out_a_bits_source = ~inFlight_0 ? 2'h0 : _pfbits_T_5; // @[TraceSink.scala 316:53]
  assign auto_sba_out_a_bits_address = putAddr[36:0]; // @[Edges.scala 465:17 Edges.scala 470:15]
  assign auto_sba_out_a_bits_data = io_stream_bits_data; // @[Edges.scala 465:17 Edges.scala 472:15]
  assign auto_sink_out_a_valid = writeGated_1 | tlState_1[0]; // @[TraceSink.scala 401:32]
  assign auto_sink_out_a_bits_opcode = tlState_1[3] ? _bundleOut_0_a_bits_T_2_opcode : 3'h0; // @[TraceSink.scala 386:25]
  assign auto_sink_out_a_bits_source = tlState_1[3] ? 1'h0 : pfbits_1_source; // @[TraceSink.scala 386:25]
  assign auto_sink_out_a_bits_address = tlState_1[3] ? _bundleOut_0_a_bits_T_2_address : pfbits_1_address; // @[TraceSink.scala 386:25]
  assign auto_sink_out_a_bits_data = tlState_1[3] ? _bundleOut_0_a_bits_T_2_data : io_stream_bits_data; // @[TraceSink.scala 386:25]
  assign io_stream_ready = sbaSinkSelected ? sbaStreamReady : otherStreamReady; // @[TraceSink.scala 415:27]
  assign io_streamWPInc = sbaSinkSelected ? sbaStreamWPInc : otherStreamWPInc; // @[TraceSink.scala 416:27]
  assign io_busReady = auto_sink_out_d_valid & tlState_1[3]; // @[TraceSink.scala 387:35]
  assign io_busRdData = auto_sink_out_d_bits_data; // @[TraceSink.scala 262:53 LazyModule.scala 390:12]
  assign io_sinkError = sbaSinkSelected ? sbaSinkError : otherSinkError; // @[TraceSink.scala 414:27]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      tlState <= 1'h0;
    end else if (~io_teActive) begin
      tlState <= 1'h0;
    end else begin
      tlState <= _GEN_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inFlight_0 <= 1'h0;
    end else if (_T) begin
      inFlight_0 <= 1'h0;
    end else if (auto_sba_out_d_valid) begin
      if (2'h0 == auto_sba_out_d_bits_source) begin
        inFlight_0 <= 1'h0;
      end else begin
        inFlight_0 <= _GEN_7;
      end
    end else begin
      inFlight_0 <= _GEN_7;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inFlight_1 <= 1'h0;
    end else if (_T) begin
      inFlight_1 <= 1'h0;
    end else if (auto_sba_out_d_valid) begin
      if (2'h1 == auto_sba_out_d_bits_source) begin
        inFlight_1 <= 1'h0;
      end else begin
        inFlight_1 <= _GEN_8;
      end
    end else begin
      inFlight_1 <= _GEN_8;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inFlight_2 <= 1'h0;
    end else if (_T) begin
      inFlight_2 <= 1'h0;
    end else if (auto_sba_out_d_valid) begin
      if (2'h2 == auto_sba_out_d_bits_source) begin
        inFlight_2 <= 1'h0;
      end else begin
        inFlight_2 <= _GEN_9;
      end
    end else begin
      inFlight_2 <= _GEN_9;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inFlight_3 <= 1'h0;
    end else if (_T) begin
      inFlight_3 <= 1'h0;
    end else if (auto_sba_out_d_valid) begin
      if (2'h3 == auto_sba_out_d_bits_source) begin
        inFlight_3 <= 1'h0;
      end else begin
        inFlight_3 <= _GEN_10;
      end
    end else begin
      inFlight_3 <= _GEN_10;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      tlState_1 <= 4'h0;
    end else if (_T) begin
      tlState_1 <= 4'h0;
    end else if (tlState_1 == 4'h0) begin
      if (writeGated_1 & ~auto_sink_out_a_ready) begin
        tlState_1 <= 4'h1;
      end else if (writeGated_1 & auto_sink_out_a_ready & ~auto_sink_out_d_valid) begin
        tlState_1 <= 4'h2;
      end else begin
        tlState_1 <= _GEN_24;
      end
    end else if (tlState_1[0] & auto_sink_out_a_ready & auto_sink_out_d_valid) begin
      tlState_1 <= 4'h0;
    end else if (_T_30 & _T_27) begin
      tlState_1 <= _tlState_T;
    end else begin
      tlState_1 <= _GEN_27;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  tlState = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  inFlight_0 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  inFlight_1 = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  inFlight_2 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  inFlight_3 = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  tlState_1 = _RAND_5[3:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    tlState = 1'h0;
  end
  if (reset) begin
    inFlight_0 = 1'h0;
  end
  if (reset) begin
    inFlight_1 = 1'h0;
  end
  if (reset) begin
    inFlight_2 = 1'h0;
  end
  if (reset) begin
    inFlight_3 = 1'h0;
  end
  if (reset) begin
    tlState_1 = 4'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
