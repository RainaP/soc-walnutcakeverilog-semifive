//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__SourceC(
  input          rf_reset,
  input          clock,
  input          reset,
  output         io_req_ready,
  input          io_req_valid,
  input  [2:0]   io_req_bits_opcode,
  input  [2:0]   io_req_bits_param,
  input  [2:0]   io_req_bits_source,
  input  [17:0]  io_req_bits_tag,
  input  [9:0]   io_req_bits_set,
  input  [2:0]   io_req_bits_way,
  input          io_req_bits_dirty,
  input          io_c_ready,
  output         io_c_valid,
  output [2:0]   io_c_bits_opcode,
  output [2:0]   io_c_bits_param,
  output [2:0]   io_c_bits_source,
  output [35:0]  io_c_bits_address,
  output [127:0] io_c_bits_data,
  input          io_bs_adr_ready,
  output         io_bs_adr_valid,
  output [2:0]   io_bs_adr_bits_way,
  output [9:0]   io_bs_adr_bits_set,
  output [1:0]   io_bs_adr_bits_beat,
  input  [127:0] io_bs_dat_data,
  output [9:0]   io_evict_req_set,
  output [2:0]   io_evict_req_way,
  input          io_evict_safe,
  output         io_corrected_valid,
  output         io_uncorrected_valid
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
`endif // RANDOMIZE_REG_INIT
  wire  queue_rf_reset; // @[SourceC.scala 63:21]
  wire  queue_clock; // @[SourceC.scala 63:21]
  wire  queue_reset; // @[SourceC.scala 63:21]
  wire  queue_io_enq_ready; // @[SourceC.scala 63:21]
  wire  queue_io_enq_valid; // @[SourceC.scala 63:21]
  wire [2:0] queue_io_enq_bits_opcode; // @[SourceC.scala 63:21]
  wire [2:0] queue_io_enq_bits_param; // @[SourceC.scala 63:21]
  wire [2:0] queue_io_enq_bits_source; // @[SourceC.scala 63:21]
  wire [35:0] queue_io_enq_bits_address; // @[SourceC.scala 63:21]
  wire [127:0] queue_io_enq_bits_data; // @[SourceC.scala 63:21]
  wire  queue_io_deq_ready; // @[SourceC.scala 63:21]
  wire  queue_io_deq_valid; // @[SourceC.scala 63:21]
  wire [2:0] queue_io_deq_bits_opcode; // @[SourceC.scala 63:21]
  wire [2:0] queue_io_deq_bits_param; // @[SourceC.scala 63:21]
  wire [2:0] queue_io_deq_bits_source; // @[SourceC.scala 63:21]
  wire [35:0] queue_io_deq_bits_address; // @[SourceC.scala 63:21]
  wire [127:0] queue_io_deq_bits_data; // @[SourceC.scala 63:21]
  wire [3:0] queue_io_count; // @[SourceC.scala 63:21]
  reg [2:0] fill; // @[SourceC.scala 67:21]
  reg  room; // @[SourceC.scala 68:21]
  wire  _T = queue_io_enq_ready & queue_io_enq_valid; // @[Decoupled.scala 40:37]
  wire  _T_1 = queue_io_deq_ready & queue_io_deq_valid; // @[Decoupled.scala 40:37]
  wire [2:0] _fill_T_2 = _T ? 3'h1 : 3'h7; // @[SourceC.scala 70:23]
  wire [2:0] _fill_T_4 = fill + _fill_T_2; // @[SourceC.scala 70:18]
  reg  busy; // @[SourceC.scala 75:21]
  reg [1:0] beat; // @[SourceC.scala 76:21]
  wire  last = &beat; // @[SourceC.scala 77:19]
  wire  _req_T = ~busy; // @[SourceC.scala 78:18]
  wire  _req_T_2 = ~busy & io_req_valid; // @[SourceC.scala 78:67]
  reg [2:0] req_r_opcode; // @[Reg.scala 15:16]
  reg [2:0] req_r_param; // @[Reg.scala 15:16]
  reg [2:0] req_r_source; // @[Reg.scala 15:16]
  reg [17:0] req_r_tag; // @[Reg.scala 15:16]
  reg [9:0] req_r_set; // @[Reg.scala 15:16]
  reg [2:0] req_r_way; // @[Reg.scala 15:16]
  wire  _want_data_T_1 = io_req_valid & room & io_req_bits_dirty; // @[SourceC.scala 79:49]
  wire  want_data = busy | io_req_valid & room & io_req_bits_dirty; // @[SourceC.scala 79:24]
  wire  _GEN_9 = _want_data_T_1 | busy; // @[SourceC.scala 96:52 SourceC.scala 96:59 SourceC.scala 75:21]
  wire  _T_20 = io_bs_adr_ready & io_bs_adr_valid; // @[Decoupled.scala 40:37]
  wire [1:0] _beat_T_1 = beat + 2'h1; // @[SourceC.scala 99:18]
  wire  _s2_latch_T_1 = io_req_ready & io_req_valid; // @[Decoupled.scala 40:37]
  wire  s2_latch = want_data ? _T_20 : _s2_latch_T_1; // @[SourceC.scala 102:21]
  reg  s2_valid; // @[SourceC.scala 103:25]
  reg [2:0] s2_req_opcode; // @[Reg.scala 15:16]
  reg [2:0] s2_req_param; // @[Reg.scala 15:16]
  reg [2:0] s2_req_source; // @[Reg.scala 15:16]
  reg [17:0] s2_req_tag; // @[Reg.scala 15:16]
  reg [9:0] s2_req_set; // @[Reg.scala 15:16]
  reg  s3_valid; // @[SourceC.scala 109:25]
  reg [2:0] s3_req_opcode; // @[Reg.scala 15:16]
  reg [2:0] s3_req_param; // @[Reg.scala 15:16]
  reg [2:0] s3_req_source; // @[Reg.scala 15:16]
  reg [17:0] s3_req_tag; // @[Reg.scala 15:16]
  reg [9:0] s3_req_set; // @[Reg.scala 15:16]
  wire [33:0] c_bits_address_base = {s3_req_tag,s3_req_set,6'h0}; // @[Cat.scala 30:58]
  wire  c_bits_address_lo_lo_lo_lo_lo = c_bits_address_base[0]; // @[Parameters.scala 242:72]
  wire  c_bits_address_lo_lo_lo_lo_hi = c_bits_address_base[1]; // @[Parameters.scala 242:72]
  wire  c_bits_address_lo_lo_lo_hi_lo = c_bits_address_base[2]; // @[Parameters.scala 242:72]
  wire  c_bits_address_lo_lo_lo_hi_hi = c_bits_address_base[3]; // @[Parameters.scala 242:72]
  wire  c_bits_address_lo_lo_hi_lo_lo = c_bits_address_base[4]; // @[Parameters.scala 242:72]
  wire  c_bits_address_lo_lo_hi_lo_hi = c_bits_address_base[5]; // @[Parameters.scala 242:72]
  wire  c_bits_address_lo_lo_hi_hi_hi_hi = c_bits_address_base[6]; // @[Parameters.scala 242:72]
  wire  c_bits_address_lo_hi_lo_lo_lo = c_bits_address_base[7]; // @[Parameters.scala 242:72]
  wire  c_bits_address_lo_hi_lo_lo_hi = c_bits_address_base[8]; // @[Parameters.scala 242:72]
  wire  c_bits_address_lo_hi_lo_hi_lo = c_bits_address_base[9]; // @[Parameters.scala 242:72]
  wire  c_bits_address_lo_hi_lo_hi_hi = c_bits_address_base[10]; // @[Parameters.scala 242:72]
  wire  c_bits_address_lo_hi_hi_lo_lo = c_bits_address_base[11]; // @[Parameters.scala 242:72]
  wire  c_bits_address_lo_hi_hi_lo_hi = c_bits_address_base[12]; // @[Parameters.scala 242:72]
  wire  c_bits_address_lo_hi_hi_hi_lo = c_bits_address_base[13]; // @[Parameters.scala 242:72]
  wire  c_bits_address_lo_hi_hi_hi_hi_lo = c_bits_address_base[14]; // @[Parameters.scala 242:72]
  wire  c_bits_address_lo_hi_hi_hi_hi_hi = c_bits_address_base[15]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_lo_lo_lo_lo = c_bits_address_base[16]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_lo_lo_lo_hi = c_bits_address_base[17]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_lo_lo_hi_lo = c_bits_address_base[18]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_lo_lo_hi_hi = c_bits_address_base[19]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_lo_hi_lo_lo = c_bits_address_base[20]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_lo_hi_lo_hi = c_bits_address_base[21]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_lo_hi_hi_lo = c_bits_address_base[22]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_lo_hi_hi_hi_lo = c_bits_address_base[23]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_lo_hi_hi_hi_hi = c_bits_address_base[24]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_hi_lo_lo_lo = c_bits_address_base[25]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_hi_lo_lo_hi = c_bits_address_base[26]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_hi_lo_hi_lo = c_bits_address_base[27]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_hi_lo_hi_hi = c_bits_address_base[28]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_hi_hi_lo_lo = c_bits_address_base[29]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_hi_hi_lo_hi = c_bits_address_base[30]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_hi_hi_hi_lo = c_bits_address_base[31]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_hi_hi_hi_hi_lo = c_bits_address_base[32]; // @[Parameters.scala 242:72]
  wire  c_bits_address_hi_hi_hi_hi_hi_hi = c_bits_address_base[33]; // @[Parameters.scala 242:72]
  wire [8:0] c_bits_address_lo_lo = {c_bits_address_lo_lo_hi_hi_hi_hi,1'h0,1'h0,c_bits_address_lo_lo_hi_lo_hi,
    c_bits_address_lo_lo_hi_lo_lo,c_bits_address_lo_lo_lo_hi_hi,c_bits_address_lo_lo_lo_hi_lo,
    c_bits_address_lo_lo_lo_lo_hi,c_bits_address_lo_lo_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [17:0] c_bits_address_lo = {c_bits_address_lo_hi_hi_hi_hi_hi,c_bits_address_lo_hi_hi_hi_hi_lo,
    c_bits_address_lo_hi_hi_hi_lo,c_bits_address_lo_hi_hi_lo_hi,c_bits_address_lo_hi_hi_lo_lo,
    c_bits_address_lo_hi_lo_hi_hi,c_bits_address_lo_hi_lo_hi_lo,c_bits_address_lo_hi_lo_lo_hi,
    c_bits_address_lo_hi_lo_lo_lo,c_bits_address_lo_lo}; // @[Cat.scala 30:58]
  wire [8:0] c_bits_address_hi_lo = {c_bits_address_hi_lo_hi_hi_hi_hi,c_bits_address_hi_lo_hi_hi_hi_lo,
    c_bits_address_hi_lo_hi_hi_lo,c_bits_address_hi_lo_hi_lo_hi,c_bits_address_hi_lo_hi_lo_lo,
    c_bits_address_hi_lo_lo_hi_hi,c_bits_address_hi_lo_lo_hi_lo,c_bits_address_hi_lo_lo_lo_hi,
    c_bits_address_hi_lo_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [17:0] c_bits_address_hi = {c_bits_address_hi_hi_hi_hi_hi_hi,c_bits_address_hi_hi_hi_hi_hi_lo,
    c_bits_address_hi_hi_hi_hi_lo,c_bits_address_hi_hi_hi_lo_hi,c_bits_address_hi_hi_hi_lo_lo,
    c_bits_address_hi_hi_lo_hi_hi,c_bits_address_hi_hi_lo_hi_lo,c_bits_address_hi_hi_lo_lo_hi,
    c_bits_address_hi_hi_lo_lo_lo,c_bits_address_hi_lo}; // @[Cat.scala 30:58]
  RHEA__QueueCompatibility_162 queue ( // @[SourceC.scala 63:21]
    .rf_reset(queue_rf_reset),
    .clock(queue_clock),
    .reset(queue_reset),
    .io_enq_ready(queue_io_enq_ready),
    .io_enq_valid(queue_io_enq_valid),
    .io_enq_bits_opcode(queue_io_enq_bits_opcode),
    .io_enq_bits_param(queue_io_enq_bits_param),
    .io_enq_bits_source(queue_io_enq_bits_source),
    .io_enq_bits_address(queue_io_enq_bits_address),
    .io_enq_bits_data(queue_io_enq_bits_data),
    .io_deq_ready(queue_io_deq_ready),
    .io_deq_valid(queue_io_deq_valid),
    .io_deq_bits_opcode(queue_io_deq_bits_opcode),
    .io_deq_bits_param(queue_io_deq_bits_param),
    .io_deq_bits_source(queue_io_deq_bits_source),
    .io_deq_bits_address(queue_io_deq_bits_address),
    .io_deq_bits_data(queue_io_deq_bits_data),
    .io_count(queue_io_count)
  );
  assign queue_rf_reset = rf_reset;
  assign io_req_ready = _req_T & room; // @[SourceC.scala 81:25]
  assign io_c_valid = queue_io_deq_valid; // @[SourceC.scala 148:8]
  assign io_c_bits_opcode = queue_io_deq_bits_opcode; // @[SourceC.scala 148:8]
  assign io_c_bits_param = queue_io_deq_bits_param; // @[SourceC.scala 148:8]
  assign io_c_bits_source = queue_io_deq_bits_source; // @[SourceC.scala 148:8]
  assign io_c_bits_address = queue_io_deq_bits_address; // @[SourceC.scala 148:8]
  assign io_c_bits_data = queue_io_deq_bits_data; // @[SourceC.scala 148:8]
  assign io_bs_adr_valid = (|beat | io_evict_safe) & want_data; // @[SourceC.scala 86:50]
  assign io_bs_adr_bits_way = ~busy ? io_req_bits_way : req_r_way; // @[SourceC.scala 78:17]
  assign io_bs_adr_bits_set = ~busy ? io_req_bits_set : req_r_set; // @[SourceC.scala 78:17]
  assign io_bs_adr_bits_beat = beat; // @[SourceC.scala 90:23]
  assign io_evict_req_set = ~busy ? io_req_bits_set : req_r_set; // @[SourceC.scala 78:17]
  assign io_evict_req_way = ~busy ? io_req_bits_way : req_r_way; // @[SourceC.scala 78:17]
  assign io_corrected_valid = 1'h0; // @[SourceC.scala 135:52]
  assign io_uncorrected_valid = 1'h0; // @[SourceC.scala 136:52]
  assign queue_clock = clock;
  assign queue_reset = reset;
  assign queue_io_enq_valid = s3_valid; // @[SourceC.scala 114:15 SourceC.scala 115:18]
  assign queue_io_enq_bits_opcode = s3_req_opcode; // @[SourceC.scala 114:15 SourceC.scala 116:18]
  assign queue_io_enq_bits_param = s3_req_param; // @[SourceC.scala 114:15 SourceC.scala 117:18]
  assign queue_io_enq_bits_source = s3_req_source; // @[SourceC.scala 114:15 SourceC.scala 119:18]
  assign queue_io_enq_bits_address = {c_bits_address_hi,c_bits_address_lo}; // @[Cat.scala 30:58]
  assign queue_io_enq_bits_data = io_bs_dat_data; // @[SourceC.scala 114:15 SourceC.scala 121:18]
  assign queue_io_deq_ready = io_c_ready; // @[SourceC.scala 148:8]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fill <= 3'h0;
    end else if (_T != _T_1) begin
      fill <= _fill_T_4;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      room <= 1'h1;
    end else if (_T != _T_1) begin
      room <= fill == 3'h0 | (fill == 3'h1 | fill == 3'h2) & ~_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      busy <= 1'h0;
    end else if (_T_20) begin
      if (last) begin
        busy <= 1'h0;
      end else begin
        busy <= _GEN_9;
      end
    end else begin
      busy <= _GEN_9;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beat <= 2'h0;
    end else if (_T_20) begin
      beat <= _beat_T_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      req_r_opcode <= 3'h0;
    end else if (_req_T_2) begin
      req_r_opcode <= io_req_bits_opcode;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      req_r_param <= 3'h0;
    end else if (_req_T_2) begin
      req_r_param <= io_req_bits_param;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      req_r_source <= 3'h0;
    end else if (_req_T_2) begin
      req_r_source <= io_req_bits_source;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      req_r_tag <= 18'h0;
    end else if (_req_T_2) begin
      req_r_tag <= io_req_bits_tag;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      req_r_set <= 10'h0;
    end else if (_req_T_2) begin
      req_r_set <= io_req_bits_set;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      req_r_way <= 3'h0;
    end else if (_req_T_2) begin
      req_r_way <= io_req_bits_way;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s2_valid <= 1'h0;
    end else if (want_data) begin
      s2_valid <= _T_20;
    end else begin
      s2_valid <= _s2_latch_T_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_opcode <= 3'h0;
    end else if (s2_latch) begin
      if (~busy) begin
        s2_req_opcode <= io_req_bits_opcode;
      end else begin
        s2_req_opcode <= req_r_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_param <= 3'h0;
    end else if (s2_latch) begin
      if (~busy) begin
        s2_req_param <= io_req_bits_param;
      end else begin
        s2_req_param <= req_r_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_source <= 3'h0;
    end else if (s2_latch) begin
      if (~busy) begin
        s2_req_source <= io_req_bits_source;
      end else begin
        s2_req_source <= req_r_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_tag <= 18'h0;
    end else if (s2_latch) begin
      if (~busy) begin
        s2_req_tag <= io_req_bits_tag;
      end else begin
        s2_req_tag <= req_r_tag;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_set <= 10'h0;
    end else if (s2_latch) begin
      if (~busy) begin
        s2_req_set <= io_req_bits_set;
      end else begin
        s2_req_set <= req_r_set;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s3_valid <= 1'h0;
    end else begin
      s3_valid <= s2_valid;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_opcode <= 3'h0;
    end else if (s2_valid) begin
      s3_req_opcode <= s2_req_opcode;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_param <= 3'h0;
    end else if (s2_valid) begin
      s3_req_param <= s2_req_param;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_source <= 3'h0;
    end else if (s2_valid) begin
      s3_req_source <= s2_req_source;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_tag <= 18'h0;
    end else if (s2_valid) begin
      s3_req_tag <= s2_req_tag;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_set <= 10'h0;
    end else if (s2_valid) begin
      s3_req_set <= s2_req_set;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  fill = _RAND_0[2:0];
  _RAND_1 = {1{`RANDOM}};
  room = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  busy = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  beat = _RAND_3[1:0];
  _RAND_4 = {1{`RANDOM}};
  req_r_opcode = _RAND_4[2:0];
  _RAND_5 = {1{`RANDOM}};
  req_r_param = _RAND_5[2:0];
  _RAND_6 = {1{`RANDOM}};
  req_r_source = _RAND_6[2:0];
  _RAND_7 = {1{`RANDOM}};
  req_r_tag = _RAND_7[17:0];
  _RAND_8 = {1{`RANDOM}};
  req_r_set = _RAND_8[9:0];
  _RAND_9 = {1{`RANDOM}};
  req_r_way = _RAND_9[2:0];
  _RAND_10 = {1{`RANDOM}};
  s2_valid = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  s2_req_opcode = _RAND_11[2:0];
  _RAND_12 = {1{`RANDOM}};
  s2_req_param = _RAND_12[2:0];
  _RAND_13 = {1{`RANDOM}};
  s2_req_source = _RAND_13[2:0];
  _RAND_14 = {1{`RANDOM}};
  s2_req_tag = _RAND_14[17:0];
  _RAND_15 = {1{`RANDOM}};
  s2_req_set = _RAND_15[9:0];
  _RAND_16 = {1{`RANDOM}};
  s3_valid = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  s3_req_opcode = _RAND_17[2:0];
  _RAND_18 = {1{`RANDOM}};
  s3_req_param = _RAND_18[2:0];
  _RAND_19 = {1{`RANDOM}};
  s3_req_source = _RAND_19[2:0];
  _RAND_20 = {1{`RANDOM}};
  s3_req_tag = _RAND_20[17:0];
  _RAND_21 = {1{`RANDOM}};
  s3_req_set = _RAND_21[9:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    fill = 3'h0;
  end
  if (reset) begin
    room = 1'h1;
  end
  if (reset) begin
    busy = 1'h0;
  end
  if (reset) begin
    beat = 2'h0;
  end
  if (rf_reset) begin
    req_r_opcode = 3'h0;
  end
  if (rf_reset) begin
    req_r_param = 3'h0;
  end
  if (rf_reset) begin
    req_r_source = 3'h0;
  end
  if (rf_reset) begin
    req_r_tag = 18'h0;
  end
  if (rf_reset) begin
    req_r_set = 10'h0;
  end
  if (rf_reset) begin
    req_r_way = 3'h0;
  end
  if (reset) begin
    s2_valid = 1'h0;
  end
  if (rf_reset) begin
    s2_req_opcode = 3'h0;
  end
  if (rf_reset) begin
    s2_req_param = 3'h0;
  end
  if (rf_reset) begin
    s2_req_source = 3'h0;
  end
  if (rf_reset) begin
    s2_req_tag = 18'h0;
  end
  if (rf_reset) begin
    s2_req_set = 10'h0;
  end
  if (reset) begin
    s3_valid = 1'h0;
  end
  if (rf_reset) begin
    s3_req_opcode = 3'h0;
  end
  if (rf_reset) begin
    s3_req_param = 3'h0;
  end
  if (rf_reset) begin
    s3_req_source = 3'h0;
  end
  if (rf_reset) begin
    s3_req_tag = 18'h0;
  end
  if (rf_reset) begin
    s3_req_set = 10'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
