#!/usr/bin/env bash

DIST=`lsb_release -d | awk '{ print $2 }'`

if [ $DIST != "Ubuntu" ]; then
    echo "This build only support Ubuntu 18.04 or higher."
    exit
fi

IPADDR=`hostname -I | awk '{print $1}'`
PORT=4444

export PATH=$PWD:$PATH

qemu-system-riscv64 -nographic -machine sifive_u -cpu sifive-u54 -m 2G -chardev socket,id=sim0,host=$IPADDR,port=$PORT,server,nowait -device riscv-simio,chardev=sim0 -kernel /ssd1tb_mattew/workspace/riscv/linux/arch/riscv/boot/Image -append "rdinit=/sbin/init loglevel=8 console=hvc0"
