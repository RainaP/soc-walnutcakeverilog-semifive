//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__RationalCrossingSink_10(
  input         rf_reset,
  input         clock,
  input         reset,
  input  [4:0]  io_enq_bits0_reason,
  input  [38:0] io_enq_bits0_iaddr,
  input  [13:0] io_enq_bits0_icnt,
  input  [31:0] io_enq_bits0_hist,
  input  [1:0]  io_enq_bits0_hisv,
  input  [4:0]  io_enq_bits1_reason,
  input  [38:0] io_enq_bits1_iaddr,
  input  [13:0] io_enq_bits1_icnt,
  input  [31:0] io_enq_bits1_hist,
  input  [1:0]  io_enq_bits1_hisv,
  input         io_enq_valid,
  input  [1:0]  io_enq_source,
  output        io_enq_ready,
  output [1:0]  io_enq_sink,
  input         io_deq_ready,
  output        io_deq_valid,
  output [4:0]  io_deq_bits_reason,
  output [38:0] io_deq_bits_iaddr,
  output [13:0] io_deq_bits_icnt,
  output [31:0] io_deq_bits_hist,
  output [1:0]  io_deq_bits_hisv
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
`endif // RANDOMIZE_REG_INIT
  wire  io_deq_q_rf_reset; // @[ShiftQueue.scala 60:19]
  wire  io_deq_q_clock; // @[ShiftQueue.scala 60:19]
  wire  io_deq_q_reset; // @[ShiftQueue.scala 60:19]
  wire  io_deq_q_io_enq_ready; // @[ShiftQueue.scala 60:19]
  wire  io_deq_q_io_enq_valid; // @[ShiftQueue.scala 60:19]
  wire [4:0] io_deq_q_io_enq_bits_reason; // @[ShiftQueue.scala 60:19]
  wire [38:0] io_deq_q_io_enq_bits_iaddr; // @[ShiftQueue.scala 60:19]
  wire [13:0] io_deq_q_io_enq_bits_icnt; // @[ShiftQueue.scala 60:19]
  wire [31:0] io_deq_q_io_enq_bits_hist; // @[ShiftQueue.scala 60:19]
  wire [1:0] io_deq_q_io_enq_bits_hisv; // @[ShiftQueue.scala 60:19]
  wire  io_deq_q_io_deq_ready; // @[ShiftQueue.scala 60:19]
  wire  io_deq_q_io_deq_valid; // @[ShiftQueue.scala 60:19]
  wire [4:0] io_deq_q_io_deq_bits_reason; // @[ShiftQueue.scala 60:19]
  wire [38:0] io_deq_q_io_deq_bits_iaddr; // @[ShiftQueue.scala 60:19]
  wire [13:0] io_deq_q_io_deq_bits_icnt; // @[ShiftQueue.scala 60:19]
  wire [31:0] io_deq_q_io_deq_bits_hist; // @[ShiftQueue.scala 60:19]
  wire [1:0] io_deq_q_io_deq_bits_hisv; // @[ShiftQueue.scala 60:19]
  reg [1:0] count; // @[RationalCrossing.scala 120:22]
  wire  equal = count == io_enq_source; // @[RationalCrossing.scala 121:21]
  wire  _deq_valid_T_2 = count[1] != io_enq_source[0]; // @[RationalCrossing.scala 126:47]
  wire  deq_valid = equal ? io_enq_valid : _deq_valid_T_2; // @[RationalCrossing.scala 126:19]
  wire  deq_ready = io_deq_q_io_enq_ready; // @[RationalCrossing.scala 112:17 ShiftQueue.scala 61:14]
  wire  _T = deq_ready & deq_valid; // @[Decoupled.scala 40:37]
  wire  count_hi = count[0]; // @[RationalCrossing.scala 128:41]
  wire  count_lo = ~count[1]; // @[RationalCrossing.scala 128:46]
  wire [1:0] _count_T_1 = {count_hi,count_lo}; // @[Cat.scala 30:58]
  RHEA__ShiftQueue_12 io_deq_q ( // @[ShiftQueue.scala 60:19]
    .rf_reset(io_deq_q_rf_reset),
    .clock(io_deq_q_clock),
    .reset(io_deq_q_reset),
    .io_enq_ready(io_deq_q_io_enq_ready),
    .io_enq_valid(io_deq_q_io_enq_valid),
    .io_enq_bits_reason(io_deq_q_io_enq_bits_reason),
    .io_enq_bits_iaddr(io_deq_q_io_enq_bits_iaddr),
    .io_enq_bits_icnt(io_deq_q_io_enq_bits_icnt),
    .io_enq_bits_hist(io_deq_q_io_enq_bits_hist),
    .io_enq_bits_hisv(io_deq_q_io_enq_bits_hisv),
    .io_deq_ready(io_deq_q_io_deq_ready),
    .io_deq_valid(io_deq_q_io_deq_valid),
    .io_deq_bits_reason(io_deq_q_io_deq_bits_reason),
    .io_deq_bits_iaddr(io_deq_q_io_deq_bits_iaddr),
    .io_deq_bits_icnt(io_deq_q_io_deq_bits_icnt),
    .io_deq_bits_hist(io_deq_q_io_deq_bits_hist),
    .io_deq_bits_hisv(io_deq_q_io_deq_bits_hisv)
  );
  assign io_deq_q_rf_reset = rf_reset;
  assign io_enq_ready = io_deq_q_io_enq_ready; // @[RationalCrossing.scala 112:17 ShiftQueue.scala 61:14]
  assign io_enq_sink = count; // @[RationalCrossing.scala 124:13]
  assign io_deq_valid = io_deq_q_io_deq_valid; // @[RationalCrossing.scala 116:31]
  assign io_deq_bits_reason = io_deq_q_io_deq_bits_reason; // @[RationalCrossing.scala 116:31]
  assign io_deq_bits_iaddr = io_deq_q_io_deq_bits_iaddr; // @[RationalCrossing.scala 116:31]
  assign io_deq_bits_icnt = io_deq_q_io_deq_bits_icnt; // @[RationalCrossing.scala 116:31]
  assign io_deq_bits_hist = io_deq_q_io_deq_bits_hist; // @[RationalCrossing.scala 116:31]
  assign io_deq_bits_hisv = io_deq_q_io_deq_bits_hisv; // @[RationalCrossing.scala 116:31]
  assign io_deq_q_clock = clock;
  assign io_deq_q_reset = reset;
  assign io_deq_q_io_enq_valid = equal ? io_enq_valid : _deq_valid_T_2; // @[RationalCrossing.scala 126:19]
  assign io_deq_q_io_enq_bits_reason = equal ? io_enq_bits0_reason : io_enq_bits1_reason; // @[RationalCrossing.scala 125:19]
  assign io_deq_q_io_enq_bits_iaddr = equal ? io_enq_bits0_iaddr : io_enq_bits1_iaddr; // @[RationalCrossing.scala 125:19]
  assign io_deq_q_io_enq_bits_icnt = equal ? io_enq_bits0_icnt : io_enq_bits1_icnt; // @[RationalCrossing.scala 125:19]
  assign io_deq_q_io_enq_bits_hist = equal ? io_enq_bits0_hist : io_enq_bits1_hist; // @[RationalCrossing.scala 125:19]
  assign io_deq_q_io_enq_bits_hisv = equal ? io_enq_bits0_hisv : io_enq_bits1_hisv; // @[RationalCrossing.scala 125:19]
  assign io_deq_q_io_deq_ready = io_deq_ready; // @[RationalCrossing.scala 116:31]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count <= 2'h0;
    end else if (_T) begin
      count <= _count_T_1;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  count = _RAND_0[1:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    count = 2'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
