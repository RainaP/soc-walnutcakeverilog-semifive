//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__AXI4UserYanker_2(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_aw_ready,
  input         auto_in_aw_valid,
  input  [31:0] auto_in_aw_bits_addr,
  input  [7:0]  auto_in_aw_bits_len,
  input  [2:0]  auto_in_aw_bits_size,
  input  [3:0]  auto_in_aw_bits_cache,
  input  [2:0]  auto_in_aw_bits_prot,
  input  [3:0]  auto_in_aw_bits_echo_tl_state_size,
  input  [10:0] auto_in_aw_bits_echo_tl_state_source,
  output        auto_in_w_ready,
  input         auto_in_w_valid,
  input  [63:0] auto_in_w_bits_data,
  input  [7:0]  auto_in_w_bits_strb,
  input         auto_in_w_bits_last,
  input         auto_in_b_ready,
  output        auto_in_b_valid,
  output [1:0]  auto_in_b_bits_resp,
  output [3:0]  auto_in_b_bits_echo_tl_state_size,
  output [10:0] auto_in_b_bits_echo_tl_state_source,
  output        auto_in_ar_ready,
  input         auto_in_ar_valid,
  input  [31:0] auto_in_ar_bits_addr,
  input  [7:0]  auto_in_ar_bits_len,
  input  [2:0]  auto_in_ar_bits_size,
  input  [3:0]  auto_in_ar_bits_cache,
  input  [2:0]  auto_in_ar_bits_prot,
  input  [3:0]  auto_in_ar_bits_echo_tl_state_size,
  input  [10:0] auto_in_ar_bits_echo_tl_state_source,
  input         auto_in_r_ready,
  output        auto_in_r_valid,
  output [63:0] auto_in_r_bits_data,
  output [1:0]  auto_in_r_bits_resp,
  output [3:0]  auto_in_r_bits_echo_tl_state_size,
  output [10:0] auto_in_r_bits_echo_tl_state_source,
  output        auto_in_r_bits_last,
  input         auto_out_aw_ready,
  output        auto_out_aw_valid,
  output [31:0] auto_out_aw_bits_addr,
  output [7:0]  auto_out_aw_bits_len,
  output [2:0]  auto_out_aw_bits_size,
  output [3:0]  auto_out_aw_bits_cache,
  output [2:0]  auto_out_aw_bits_prot,
  input         auto_out_w_ready,
  output        auto_out_w_valid,
  output [63:0] auto_out_w_bits_data,
  output [7:0]  auto_out_w_bits_strb,
  output        auto_out_w_bits_last,
  output        auto_out_b_ready,
  input         auto_out_b_valid,
  input  [1:0]  auto_out_b_bits_resp,
  input         auto_out_ar_ready,
  output        auto_out_ar_valid,
  output [31:0] auto_out_ar_bits_addr,
  output [7:0]  auto_out_ar_bits_len,
  output [2:0]  auto_out_ar_bits_size,
  output [3:0]  auto_out_ar_bits_cache,
  output [2:0]  auto_out_ar_bits_prot,
  output        auto_out_r_ready,
  input         auto_out_r_valid,
  input  [63:0] auto_out_r_bits_data,
  input  [1:0]  auto_out_r_bits_resp,
  input         auto_out_r_bits_last
);
  wire  QueueCompatibility_rf_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_clock; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_io_enq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_io_enq_valid; // @[UserYanker.scala 55:17]
  wire [3:0] QueueCompatibility_io_enq_bits_tl_state_size; // @[UserYanker.scala 55:17]
  wire [10:0] QueueCompatibility_io_enq_bits_tl_state_source; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_io_deq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_io_deq_valid; // @[UserYanker.scala 55:17]
  wire [3:0] QueueCompatibility_io_deq_bits_tl_state_size; // @[UserYanker.scala 55:17]
  wire [10:0] QueueCompatibility_io_deq_bits_tl_state_source; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_rf_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_clock; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_io_enq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_io_enq_valid; // @[UserYanker.scala 55:17]
  wire [3:0] QueueCompatibility_1_io_enq_bits_tl_state_size; // @[UserYanker.scala 55:17]
  wire [10:0] QueueCompatibility_1_io_enq_bits_tl_state_source; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_io_deq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_io_deq_valid; // @[UserYanker.scala 55:17]
  wire [3:0] QueueCompatibility_1_io_deq_bits_tl_state_size; // @[UserYanker.scala 55:17]
  wire [10:0] QueueCompatibility_1_io_deq_bits_tl_state_source; // @[UserYanker.scala 55:17]
  wire  _ar_ready_WIRE_0 = QueueCompatibility_io_enq_ready; // @[UserYanker.scala 63:25 UserYanker.scala 63:25]
  wire  _aw_ready_WIRE_0 = QueueCompatibility_1_io_enq_ready; // @[UserYanker.scala 84:25 UserYanker.scala 84:25]
  RHEA__QueueCompatibility_40 QueueCompatibility ( // @[UserYanker.scala 55:17]
    .rf_reset(QueueCompatibility_rf_reset),
    .clock(QueueCompatibility_clock),
    .reset(QueueCompatibility_reset),
    .io_enq_ready(QueueCompatibility_io_enq_ready),
    .io_enq_valid(QueueCompatibility_io_enq_valid),
    .io_enq_bits_tl_state_size(QueueCompatibility_io_enq_bits_tl_state_size),
    .io_enq_bits_tl_state_source(QueueCompatibility_io_enq_bits_tl_state_source),
    .io_deq_ready(QueueCompatibility_io_deq_ready),
    .io_deq_valid(QueueCompatibility_io_deq_valid),
    .io_deq_bits_tl_state_size(QueueCompatibility_io_deq_bits_tl_state_size),
    .io_deq_bits_tl_state_source(QueueCompatibility_io_deq_bits_tl_state_source)
  );
  RHEA__QueueCompatibility_40 QueueCompatibility_1 ( // @[UserYanker.scala 55:17]
    .rf_reset(QueueCompatibility_1_rf_reset),
    .clock(QueueCompatibility_1_clock),
    .reset(QueueCompatibility_1_reset),
    .io_enq_ready(QueueCompatibility_1_io_enq_ready),
    .io_enq_valid(QueueCompatibility_1_io_enq_valid),
    .io_enq_bits_tl_state_size(QueueCompatibility_1_io_enq_bits_tl_state_size),
    .io_enq_bits_tl_state_source(QueueCompatibility_1_io_enq_bits_tl_state_source),
    .io_deq_ready(QueueCompatibility_1_io_deq_ready),
    .io_deq_valid(QueueCompatibility_1_io_deq_valid),
    .io_deq_bits_tl_state_size(QueueCompatibility_1_io_deq_bits_tl_state_size),
    .io_deq_bits_tl_state_source(QueueCompatibility_1_io_deq_bits_tl_state_source)
  );
  assign QueueCompatibility_rf_reset = rf_reset;
  assign QueueCompatibility_1_rf_reset = rf_reset;
  assign auto_in_aw_ready = auto_out_aw_ready & _aw_ready_WIRE_0; // @[UserYanker.scala 85:36]
  assign auto_in_w_ready = auto_out_w_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_b_valid = auto_out_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_b_bits_resp = auto_out_b_bits_resp; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_b_bits_echo_tl_state_size = QueueCompatibility_1_io_deq_bits_tl_state_size; // @[UserYanker.scala 91:23 UserYanker.scala 91:23]
  assign auto_in_b_bits_echo_tl_state_source = QueueCompatibility_1_io_deq_bits_tl_state_source; // @[UserYanker.scala 91:23 UserYanker.scala 91:23]
  assign auto_in_ar_ready = auto_out_ar_ready & _ar_ready_WIRE_0; // @[UserYanker.scala 64:36]
  assign auto_in_r_valid = auto_out_r_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_bits_data = auto_out_r_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_bits_resp = auto_out_r_bits_resp; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_bits_echo_tl_state_size = QueueCompatibility_io_deq_bits_tl_state_size; // @[UserYanker.scala 70:23 UserYanker.scala 70:23]
  assign auto_in_r_bits_echo_tl_state_source = QueueCompatibility_io_deq_bits_tl_state_source; // @[UserYanker.scala 70:23 UserYanker.scala 70:23]
  assign auto_in_r_bits_last = auto_out_r_bits_last; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_out_aw_valid = auto_in_aw_valid & _aw_ready_WIRE_0; // @[UserYanker.scala 86:36]
  assign auto_out_aw_bits_addr = auto_in_aw_bits_addr; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_len = auto_in_aw_bits_len; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_size = auto_in_aw_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_cache = auto_in_aw_bits_cache; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_prot = auto_in_aw_bits_prot; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_w_valid = auto_in_w_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_w_bits_data = auto_in_w_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_w_bits_strb = auto_in_w_bits_strb; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_w_bits_last = auto_in_w_bits_last; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_b_ready = auto_in_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_valid = auto_in_ar_valid & _ar_ready_WIRE_0; // @[UserYanker.scala 65:36]
  assign auto_out_ar_bits_addr = auto_in_ar_bits_addr; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_len = auto_in_ar_bits_len; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_size = auto_in_ar_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_cache = auto_in_ar_bits_cache; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_prot = auto_in_ar_bits_prot; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_r_ready = auto_in_r_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_clock = clock;
  assign QueueCompatibility_reset = reset;
  assign QueueCompatibility_io_enq_valid = auto_in_ar_valid & auto_out_ar_ready; // @[UserYanker.scala 79:37]
  assign QueueCompatibility_io_enq_bits_tl_state_size = auto_in_ar_bits_echo_tl_state_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_io_enq_bits_tl_state_source = auto_in_ar_bits_echo_tl_state_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_io_deq_ready = auto_out_r_valid & auto_in_r_ready & auto_out_r_bits_last; // @[UserYanker.scala 78:58]
  assign QueueCompatibility_1_clock = clock;
  assign QueueCompatibility_1_reset = reset;
  assign QueueCompatibility_1_io_enq_valid = auto_in_aw_valid & auto_out_aw_ready; // @[UserYanker.scala 100:37]
  assign QueueCompatibility_1_io_enq_bits_tl_state_size = auto_in_aw_bits_echo_tl_state_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_1_io_enq_bits_tl_state_source = auto_in_aw_bits_echo_tl_state_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_1_io_deq_ready = auto_out_b_valid & auto_in_b_ready; // @[UserYanker.scala 99:37]
endmodule
