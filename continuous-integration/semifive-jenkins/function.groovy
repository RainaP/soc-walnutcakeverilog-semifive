def configuration() {
	env.project	  = "soc-walnutcakeverilog-semifive"
    env.gitlab    = "portal"
    env.group     = "league"
    environment   = "environment.sh"
    source_branch = "$gitlabSourceBranch"
    target_branch = "$gitlabTargetBranch"		
}

def path(){
	ciPath = "/continuous-integration/semifive-jenkins"
	archivePath = "/user/jenkins/archive"
    tmp_pwd = sh(script: "pwd", returnStdout:true).trim()
    tmp_basedir = sh(script: "basename ${tmp_pwd}", returnStdout:true).trim()
}

def getGitBranchName(){
    return sh(returnStdout: true, script: """echo "${scm.branches[0].name}" | awk -F "/" '{print \$2}'""").trim()
}

def checkBranch(){
    default_branch = function.getGitBranchName()
    println(default_branch)    

    if(target_branch != default_branch){
        println("CI skipped")             
        sh "exit 0"
    }
}

def prepareGitWit(){
    println("CI applied to ${target_branch}")
    sh """        
        git clone git@${env.gitlab}:${env.group}/${env.project}.git -b ${default_branch}
        source ${env.project}/${ciPath}/${environment}		
        git -C ${env.project} merge origin/${target_branch} origin/${source_branch}
		wit init .
        wit add-pkg git@${env.gitlab}:${env.group}/${env.project}.git
		wit update-pkg ${env.project}
		wit update
    """
}

def runSim(){
    sh """
        source ${env.project}/${ciPath}/${environment}
        source ${env.project}/${ciPath}/ci-command.sh
    """
}

def archive(){
    try{
        if (fileExists("${archivePath}/${JOB_NAME}") == false){
                sh "mkdir -p ${archivePath}/${JOB_NAME}"
            }	          
            sh "tar -czf ${archivePath}/${JOB_NAME}/${JOB_NAME}.${BUILD_NUMBER}.tar.gz --ignore-failed-read --exclude=.fuse ../${tmp_basedir}"
    }catch(e){
        println("archive error")
    }
}

def checkFailure(){	
	dir("${tmp_pwd}/${env.project}/TOP/rtl.meta/rtl.sim") {
		read = readFile 'sim.out'
		if((read =~ /Assertion Failed/))
   		{
			sh """
				set +x
				exit 1
			"""
		}
	}
}
return this
