//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLFIFOFixer(
  input          rf_reset,
  input          clock,
  input          reset,
  output         auto_in_2_a_ready,
  input          auto_in_2_a_valid,
  input  [2:0]   auto_in_2_a_bits_opcode,
  input  [2:0]   auto_in_2_a_bits_param,
  input  [3:0]   auto_in_2_a_bits_size,
  input  [4:0]   auto_in_2_a_bits_source,
  input  [36:0]  auto_in_2_a_bits_address,
  input          auto_in_2_a_bits_user_amba_prot_bufferable,
  input          auto_in_2_a_bits_user_amba_prot_modifiable,
  input          auto_in_2_a_bits_user_amba_prot_readalloc,
  input          auto_in_2_a_bits_user_amba_prot_writealloc,
  input          auto_in_2_a_bits_user_amba_prot_privileged,
  input          auto_in_2_a_bits_user_amba_prot_secure,
  input          auto_in_2_a_bits_user_amba_prot_fetch,
  input  [15:0]  auto_in_2_a_bits_mask,
  input  [127:0] auto_in_2_a_bits_data,
  input          auto_in_2_a_bits_corrupt,
  input          auto_in_2_b_ready,
  output         auto_in_2_b_valid,
  output [2:0]   auto_in_2_b_bits_opcode,
  output [1:0]   auto_in_2_b_bits_param,
  output [3:0]   auto_in_2_b_bits_size,
  output [4:0]   auto_in_2_b_bits_source,
  output [36:0]  auto_in_2_b_bits_address,
  output [15:0]  auto_in_2_b_bits_mask,
  output [127:0] auto_in_2_b_bits_data,
  output         auto_in_2_b_bits_corrupt,
  output         auto_in_2_c_ready,
  input          auto_in_2_c_valid,
  input  [2:0]   auto_in_2_c_bits_opcode,
  input  [2:0]   auto_in_2_c_bits_param,
  input  [3:0]   auto_in_2_c_bits_size,
  input  [4:0]   auto_in_2_c_bits_source,
  input  [36:0]  auto_in_2_c_bits_address,
  input  [127:0] auto_in_2_c_bits_data,
  input          auto_in_2_c_bits_corrupt,
  input          auto_in_2_d_ready,
  output         auto_in_2_d_valid,
  output [2:0]   auto_in_2_d_bits_opcode,
  output [1:0]   auto_in_2_d_bits_param,
  output [3:0]   auto_in_2_d_bits_size,
  output [4:0]   auto_in_2_d_bits_source,
  output [4:0]   auto_in_2_d_bits_sink,
  output         auto_in_2_d_bits_denied,
  output [127:0] auto_in_2_d_bits_data,
  output         auto_in_2_d_bits_corrupt,
  output         auto_in_2_e_ready,
  input          auto_in_2_e_valid,
  input  [4:0]   auto_in_2_e_bits_sink,
  output         auto_in_1_a_ready,
  input          auto_in_1_a_valid,
  input  [2:0]   auto_in_1_a_bits_opcode,
  input  [2:0]   auto_in_1_a_bits_param,
  input  [3:0]   auto_in_1_a_bits_size,
  input  [4:0]   auto_in_1_a_bits_source,
  input  [36:0]  auto_in_1_a_bits_address,
  input          auto_in_1_a_bits_user_amba_prot_bufferable,
  input          auto_in_1_a_bits_user_amba_prot_modifiable,
  input          auto_in_1_a_bits_user_amba_prot_readalloc,
  input          auto_in_1_a_bits_user_amba_prot_writealloc,
  input          auto_in_1_a_bits_user_amba_prot_privileged,
  input          auto_in_1_a_bits_user_amba_prot_secure,
  input          auto_in_1_a_bits_user_amba_prot_fetch,
  input  [15:0]  auto_in_1_a_bits_mask,
  input  [127:0] auto_in_1_a_bits_data,
  input          auto_in_1_a_bits_corrupt,
  input          auto_in_1_b_ready,
  output         auto_in_1_b_valid,
  output [2:0]   auto_in_1_b_bits_opcode,
  output [1:0]   auto_in_1_b_bits_param,
  output [3:0]   auto_in_1_b_bits_size,
  output [4:0]   auto_in_1_b_bits_source,
  output [36:0]  auto_in_1_b_bits_address,
  output [15:0]  auto_in_1_b_bits_mask,
  output [127:0] auto_in_1_b_bits_data,
  output         auto_in_1_b_bits_corrupt,
  output         auto_in_1_c_ready,
  input          auto_in_1_c_valid,
  input  [2:0]   auto_in_1_c_bits_opcode,
  input  [2:0]   auto_in_1_c_bits_param,
  input  [3:0]   auto_in_1_c_bits_size,
  input  [4:0]   auto_in_1_c_bits_source,
  input  [36:0]  auto_in_1_c_bits_address,
  input  [127:0] auto_in_1_c_bits_data,
  input          auto_in_1_c_bits_corrupt,
  input          auto_in_1_d_ready,
  output         auto_in_1_d_valid,
  output [2:0]   auto_in_1_d_bits_opcode,
  output [1:0]   auto_in_1_d_bits_param,
  output [3:0]   auto_in_1_d_bits_size,
  output [4:0]   auto_in_1_d_bits_source,
  output [4:0]   auto_in_1_d_bits_sink,
  output         auto_in_1_d_bits_denied,
  output [127:0] auto_in_1_d_bits_data,
  output         auto_in_1_d_bits_corrupt,
  output         auto_in_1_e_ready,
  input          auto_in_1_e_valid,
  input  [4:0]   auto_in_1_e_bits_sink,
  output         auto_in_0_a_ready,
  input          auto_in_0_a_valid,
  input  [2:0]   auto_in_0_a_bits_opcode,
  input  [3:0]   auto_in_0_a_bits_size,
  input  [5:0]   auto_in_0_a_bits_source,
  input  [36:0]  auto_in_0_a_bits_address,
  input          auto_in_0_a_bits_user_amba_prot_bufferable,
  input          auto_in_0_a_bits_user_amba_prot_modifiable,
  input          auto_in_0_a_bits_user_amba_prot_readalloc,
  input          auto_in_0_a_bits_user_amba_prot_writealloc,
  input          auto_in_0_a_bits_user_amba_prot_privileged,
  input          auto_in_0_a_bits_user_amba_prot_secure,
  input          auto_in_0_a_bits_user_amba_prot_fetch,
  input  [15:0]  auto_in_0_a_bits_mask,
  input  [127:0] auto_in_0_a_bits_data,
  input          auto_in_0_d_ready,
  output         auto_in_0_d_valid,
  output [2:0]   auto_in_0_d_bits_opcode,
  output [1:0]   auto_in_0_d_bits_param,
  output [3:0]   auto_in_0_d_bits_size,
  output [5:0]   auto_in_0_d_bits_source,
  output [4:0]   auto_in_0_d_bits_sink,
  output         auto_in_0_d_bits_denied,
  output [127:0] auto_in_0_d_bits_data,
  output         auto_in_0_d_bits_corrupt,
  input          auto_out_2_a_ready,
  output         auto_out_2_a_valid,
  output [2:0]   auto_out_2_a_bits_opcode,
  output [2:0]   auto_out_2_a_bits_param,
  output [3:0]   auto_out_2_a_bits_size,
  output [4:0]   auto_out_2_a_bits_source,
  output [36:0]  auto_out_2_a_bits_address,
  output         auto_out_2_a_bits_user_amba_prot_bufferable,
  output         auto_out_2_a_bits_user_amba_prot_modifiable,
  output         auto_out_2_a_bits_user_amba_prot_readalloc,
  output         auto_out_2_a_bits_user_amba_prot_writealloc,
  output         auto_out_2_a_bits_user_amba_prot_privileged,
  output         auto_out_2_a_bits_user_amba_prot_secure,
  output         auto_out_2_a_bits_user_amba_prot_fetch,
  output [15:0]  auto_out_2_a_bits_mask,
  output [127:0] auto_out_2_a_bits_data,
  output         auto_out_2_a_bits_corrupt,
  output         auto_out_2_b_ready,
  input          auto_out_2_b_valid,
  input  [2:0]   auto_out_2_b_bits_opcode,
  input  [1:0]   auto_out_2_b_bits_param,
  input  [3:0]   auto_out_2_b_bits_size,
  input  [4:0]   auto_out_2_b_bits_source,
  input  [36:0]  auto_out_2_b_bits_address,
  input  [15:0]  auto_out_2_b_bits_mask,
  input  [127:0] auto_out_2_b_bits_data,
  input          auto_out_2_b_bits_corrupt,
  input          auto_out_2_c_ready,
  output         auto_out_2_c_valid,
  output [2:0]   auto_out_2_c_bits_opcode,
  output [2:0]   auto_out_2_c_bits_param,
  output [3:0]   auto_out_2_c_bits_size,
  output [4:0]   auto_out_2_c_bits_source,
  output [36:0]  auto_out_2_c_bits_address,
  output [127:0] auto_out_2_c_bits_data,
  output         auto_out_2_c_bits_corrupt,
  output         auto_out_2_d_ready,
  input          auto_out_2_d_valid,
  input  [2:0]   auto_out_2_d_bits_opcode,
  input  [1:0]   auto_out_2_d_bits_param,
  input  [3:0]   auto_out_2_d_bits_size,
  input  [4:0]   auto_out_2_d_bits_source,
  input  [4:0]   auto_out_2_d_bits_sink,
  input          auto_out_2_d_bits_denied,
  input  [127:0] auto_out_2_d_bits_data,
  input          auto_out_2_d_bits_corrupt,
  input          auto_out_2_e_ready,
  output         auto_out_2_e_valid,
  output [4:0]   auto_out_2_e_bits_sink,
  input          auto_out_1_a_ready,
  output         auto_out_1_a_valid,
  output [2:0]   auto_out_1_a_bits_opcode,
  output [2:0]   auto_out_1_a_bits_param,
  output [3:0]   auto_out_1_a_bits_size,
  output [4:0]   auto_out_1_a_bits_source,
  output [36:0]  auto_out_1_a_bits_address,
  output         auto_out_1_a_bits_user_amba_prot_bufferable,
  output         auto_out_1_a_bits_user_amba_prot_modifiable,
  output         auto_out_1_a_bits_user_amba_prot_readalloc,
  output         auto_out_1_a_bits_user_amba_prot_writealloc,
  output         auto_out_1_a_bits_user_amba_prot_privileged,
  output         auto_out_1_a_bits_user_amba_prot_secure,
  output         auto_out_1_a_bits_user_amba_prot_fetch,
  output [15:0]  auto_out_1_a_bits_mask,
  output [127:0] auto_out_1_a_bits_data,
  output         auto_out_1_a_bits_corrupt,
  output         auto_out_1_b_ready,
  input          auto_out_1_b_valid,
  input  [2:0]   auto_out_1_b_bits_opcode,
  input  [1:0]   auto_out_1_b_bits_param,
  input  [3:0]   auto_out_1_b_bits_size,
  input  [4:0]   auto_out_1_b_bits_source,
  input  [36:0]  auto_out_1_b_bits_address,
  input  [15:0]  auto_out_1_b_bits_mask,
  input  [127:0] auto_out_1_b_bits_data,
  input          auto_out_1_b_bits_corrupt,
  input          auto_out_1_c_ready,
  output         auto_out_1_c_valid,
  output [2:0]   auto_out_1_c_bits_opcode,
  output [2:0]   auto_out_1_c_bits_param,
  output [3:0]   auto_out_1_c_bits_size,
  output [4:0]   auto_out_1_c_bits_source,
  output [36:0]  auto_out_1_c_bits_address,
  output [127:0] auto_out_1_c_bits_data,
  output         auto_out_1_c_bits_corrupt,
  output         auto_out_1_d_ready,
  input          auto_out_1_d_valid,
  input  [2:0]   auto_out_1_d_bits_opcode,
  input  [1:0]   auto_out_1_d_bits_param,
  input  [3:0]   auto_out_1_d_bits_size,
  input  [4:0]   auto_out_1_d_bits_source,
  input  [4:0]   auto_out_1_d_bits_sink,
  input          auto_out_1_d_bits_denied,
  input  [127:0] auto_out_1_d_bits_data,
  input          auto_out_1_d_bits_corrupt,
  input          auto_out_1_e_ready,
  output         auto_out_1_e_valid,
  output [4:0]   auto_out_1_e_bits_sink,
  input          auto_out_0_a_ready,
  output         auto_out_0_a_valid,
  output [2:0]   auto_out_0_a_bits_opcode,
  output [3:0]   auto_out_0_a_bits_size,
  output [5:0]   auto_out_0_a_bits_source,
  output [36:0]  auto_out_0_a_bits_address,
  output         auto_out_0_a_bits_user_amba_prot_bufferable,
  output         auto_out_0_a_bits_user_amba_prot_modifiable,
  output         auto_out_0_a_bits_user_amba_prot_readalloc,
  output         auto_out_0_a_bits_user_amba_prot_writealloc,
  output         auto_out_0_a_bits_user_amba_prot_privileged,
  output         auto_out_0_a_bits_user_amba_prot_secure,
  output         auto_out_0_a_bits_user_amba_prot_fetch,
  output [15:0]  auto_out_0_a_bits_mask,
  output [127:0] auto_out_0_a_bits_data,
  output         auto_out_0_d_ready,
  input          auto_out_0_d_valid,
  input  [2:0]   auto_out_0_d_bits_opcode,
  input  [1:0]   auto_out_0_d_bits_param,
  input  [3:0]   auto_out_0_d_bits_size,
  input  [5:0]   auto_out_0_d_bits_source,
  input  [4:0]   auto_out_0_d_bits_sink,
  input          auto_out_0_d_bits_denied,
  input  [127:0] auto_out_0_d_bits_data,
  input          auto_out_0_d_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [31:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [31:0] _RAND_59;
  reg [31:0] _RAND_60;
  reg [31:0] _RAND_61;
  reg [31:0] _RAND_62;
`endif // RANDOMIZE_REG_INIT
  wire [36:0] _a_notFIFO_T = auto_in_0_a_bits_address ^ 37'h8000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_notFIFO_T_1 = {1'b0,$signed(_a_notFIFO_T)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_notFIFO_T_3 = $signed(_a_notFIFO_T_1) & 38'sh188c000000; // @[Parameters.scala 137:52]
  wire  _a_notFIFO_T_4 = $signed(_a_notFIFO_T_3) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _a_notFIFO_T_5 = auto_in_0_a_bits_address ^ 37'h800000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_notFIFO_T_6 = {1'b0,$signed(_a_notFIFO_T_5)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_notFIFO_T_8 = $signed(_a_notFIFO_T_6) & 38'sh1800000000; // @[Parameters.scala 137:52]
  wire  _a_notFIFO_T_9 = $signed(_a_notFIFO_T_8) == 38'sh0; // @[Parameters.scala 137:67]
  wire  a_notFIFO = _a_notFIFO_T_4 | _a_notFIFO_T_9; // @[Parameters.scala 630:89]
  wire [37:0] _a_notFIFO_T_12 = {1'b0,$signed(auto_in_0_a_bits_address)}; // @[Parameters.scala 137:49]
  wire [36:0] _a_notFIFO_T_26 = auto_in_0_a_bits_address ^ 37'h1000000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_notFIFO_T_27 = {1'b0,$signed(_a_notFIFO_T_26)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_notFIFO_T_29 = $signed(_a_notFIFO_T_27) & 38'sh1000000000; // @[Parameters.scala 137:52]
  wire  _a_notFIFO_T_30 = $signed(_a_notFIFO_T_29) == 38'sh0; // @[Parameters.scala 137:67]
  wire [37:0] _a_id_T_3 = $signed(_a_notFIFO_T_12) & 38'sh1000000000; // @[Parameters.scala 137:52]
  wire  _a_id_T_4 = $signed(_a_id_T_3) == 38'sh0; // @[Parameters.scala 137:67]
  wire [1:0] _a_id_T_11 = _a_notFIFO_T_30 ? 2'h2 : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _GEN_311 = {{1'd0}, _a_id_T_4}; // @[Mux.scala 27:72]
  wire [1:0] a_id = _GEN_311 | _a_id_T_11; // @[Mux.scala 27:72]
  wire  a_noDomain = a_id == 2'h0; // @[FIFOFixer.scala 55:29]
  wire  stalls_a_sel = auto_in_0_a_bits_source[5:3] == 3'h0; // @[Parameters.scala 54:32]
  reg [3:0] a_first_counter; // @[Edges.scala 228:27]
  wire  a_first = a_first_counter == 4'h0; // @[Edges.scala 230:25]
  reg  flight__0; // @[FIFOFixer.scala 71:27]
  reg  flight__1; // @[FIFOFixer.scala 71:27]
  reg  flight__2; // @[FIFOFixer.scala 71:27]
  reg  flight__3; // @[FIFOFixer.scala 71:27]
  reg  flight__4; // @[FIFOFixer.scala 71:27]
  reg  flight__5; // @[FIFOFixer.scala 71:27]
  reg  flight__6; // @[FIFOFixer.scala 71:27]
  reg  flight__7; // @[FIFOFixer.scala 71:27]
  wire  _stalls_T_7 = flight__0 | flight__1 | flight__2 | flight__3 | flight__4 | flight__5 | flight__6 | flight__7; // @[FIFOFixer.scala 80:44]
  reg [1:0] stalls_id; // @[Reg.scala 15:16]
  wire  stalls_0 = stalls_a_sel & a_first & _stalls_T_7 & (a_noDomain | stalls_id != a_id); // @[FIFOFixer.scala 80:50]
  wire  stalls_a_sel_1 = auto_in_0_a_bits_source[5:3] == 3'h1; // @[Parameters.scala 54:32]
  reg  flight__8; // @[FIFOFixer.scala 71:27]
  reg  flight__9; // @[FIFOFixer.scala 71:27]
  reg  flight__10; // @[FIFOFixer.scala 71:27]
  reg  flight__11; // @[FIFOFixer.scala 71:27]
  reg  flight__12; // @[FIFOFixer.scala 71:27]
  reg  flight__13; // @[FIFOFixer.scala 71:27]
  reg  flight__14; // @[FIFOFixer.scala 71:27]
  reg  flight__15; // @[FIFOFixer.scala 71:27]
  reg [1:0] stalls_id_1; // @[Reg.scala 15:16]
  wire  stalls_1 = stalls_a_sel_1 & a_first & (flight__8 | flight__9 | flight__10 | flight__11 | flight__12 | flight__13
     | flight__14 | flight__15) & (a_noDomain | stalls_id_1 != a_id); // @[FIFOFixer.scala 80:50]
  wire  stalls_a_sel_2 = auto_in_0_a_bits_source[5:3] == 3'h2; // @[Parameters.scala 54:32]
  reg  flight__16; // @[FIFOFixer.scala 71:27]
  reg  flight__17; // @[FIFOFixer.scala 71:27]
  reg  flight__18; // @[FIFOFixer.scala 71:27]
  reg  flight__19; // @[FIFOFixer.scala 71:27]
  reg  flight__20; // @[FIFOFixer.scala 71:27]
  reg  flight__21; // @[FIFOFixer.scala 71:27]
  reg  flight__22; // @[FIFOFixer.scala 71:27]
  reg  flight__23; // @[FIFOFixer.scala 71:27]
  reg [1:0] stalls_id_2; // @[Reg.scala 15:16]
  wire  stalls_2 = stalls_a_sel_2 & a_first & (flight__16 | flight__17 | flight__18 | flight__19 | flight__20 |
    flight__21 | flight__22 | flight__23) & (a_noDomain | stalls_id_2 != a_id); // @[FIFOFixer.scala 80:50]
  wire  stalls_a_sel_3 = auto_in_0_a_bits_source[5:3] == 3'h3; // @[Parameters.scala 54:32]
  reg  flight__24; // @[FIFOFixer.scala 71:27]
  reg  flight__25; // @[FIFOFixer.scala 71:27]
  reg  flight__26; // @[FIFOFixer.scala 71:27]
  reg  flight__27; // @[FIFOFixer.scala 71:27]
  reg  flight__28; // @[FIFOFixer.scala 71:27]
  reg  flight__29; // @[FIFOFixer.scala 71:27]
  reg  flight__30; // @[FIFOFixer.scala 71:27]
  reg  flight__31; // @[FIFOFixer.scala 71:27]
  reg [1:0] stalls_id_3; // @[Reg.scala 15:16]
  wire  stalls_3 = stalls_a_sel_3 & a_first & (flight__24 | flight__25 | flight__26 | flight__27 | flight__28 |
    flight__29 | flight__30 | flight__31) & (a_noDomain | stalls_id_3 != a_id); // @[FIFOFixer.scala 80:50]
  wire  stalls_a_sel_4 = auto_in_0_a_bits_source[5:2] == 4'h8; // @[Parameters.scala 54:32]
  reg  flight__32; // @[FIFOFixer.scala 71:27]
  reg  flight__33; // @[FIFOFixer.scala 71:27]
  reg  flight__34; // @[FIFOFixer.scala 71:27]
  reg  flight__35; // @[FIFOFixer.scala 71:27]
  reg [1:0] stalls_id_4; // @[Reg.scala 15:16]
  wire  stalls_4 = stalls_a_sel_4 & a_first & (flight__32 | flight__33 | flight__34 | flight__35) & (a_noDomain |
    stalls_id_4 != a_id); // @[FIFOFixer.scala 80:50]
  wire  stall = stalls_0 | stalls_1 | stalls_2 | stalls_3 | stalls_4; // @[FIFOFixer.scala 83:49]
  wire  _bundleIn_0_a_ready_T_1 = a_notFIFO | ~stall; // @[FIFOFixer.scala 88:47]
  wire  bundleIn_0_a_ready = auto_out_0_a_ready & _bundleIn_0_a_ready_T_1; // @[FIFOFixer.scala 88:33]
  wire  _a_first_T = bundleIn_0_a_ready & auto_in_0_a_valid; // @[Decoupled.scala 40:37]
  wire [22:0] _a_first_beats1_decode_T_1 = 23'hff << auto_in_0_a_bits_size; // @[package.scala 234:77]
  wire [7:0] _a_first_beats1_decode_T_3 = ~_a_first_beats1_decode_T_1[7:0]; // @[package.scala 234:46]
  wire [3:0] a_first_beats1_decode = _a_first_beats1_decode_T_3[7:4]; // @[Edges.scala 219:59]
  wire  a_first_beats1_opdata = ~auto_in_0_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [3:0] a_first_counter1 = a_first_counter - 4'h1; // @[Edges.scala 229:28]
  wire  _d_first_T = auto_in_0_d_ready & auto_out_0_d_valid; // @[Decoupled.scala 40:37]
  wire [22:0] _d_first_beats1_decode_T_1 = 23'hff << auto_out_0_d_bits_size; // @[package.scala 234:77]
  wire [7:0] _d_first_beats1_decode_T_3 = ~_d_first_beats1_decode_T_1[7:0]; // @[package.scala 234:46]
  wire [3:0] d_first_beats1_decode = _d_first_beats1_decode_T_3[7:4]; // @[Edges.scala 219:59]
  wire  d_first_beats1_opdata = auto_out_0_d_bits_opcode[0]; // @[Edges.scala 105:36]
  reg [3:0] d_first_counter; // @[Edges.scala 228:27]
  wire [3:0] d_first_counter1 = d_first_counter - 4'h1; // @[Edges.scala 229:28]
  wire  d_first_first = d_first_counter == 4'h0; // @[Edges.scala 230:25]
  wire  d_first = d_first_first & auto_out_0_d_bits_opcode != 3'h6; // @[FIFOFixer.scala 67:42]
  wire  _T_1 = a_first & _a_first_T; // @[FIFOFixer.scala 72:21]
  wire  _flight_T = ~a_notFIFO; // @[FIFOFixer.scala 72:67]
  wire  _GEN_2 = 6'h0 == auto_in_0_a_bits_source ? _flight_T : flight__0; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_3 = 6'h1 == auto_in_0_a_bits_source ? _flight_T : flight__1; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_4 = 6'h2 == auto_in_0_a_bits_source ? _flight_T : flight__2; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_5 = 6'h3 == auto_in_0_a_bits_source ? _flight_T : flight__3; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_6 = 6'h4 == auto_in_0_a_bits_source ? _flight_T : flight__4; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_7 = 6'h5 == auto_in_0_a_bits_source ? _flight_T : flight__5; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_8 = 6'h6 == auto_in_0_a_bits_source ? _flight_T : flight__6; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_9 = 6'h7 == auto_in_0_a_bits_source ? _flight_T : flight__7; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_10 = 6'h8 == auto_in_0_a_bits_source ? _flight_T : flight__8; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_11 = 6'h9 == auto_in_0_a_bits_source ? _flight_T : flight__9; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_12 = 6'ha == auto_in_0_a_bits_source ? _flight_T : flight__10; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_13 = 6'hb == auto_in_0_a_bits_source ? _flight_T : flight__11; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_14 = 6'hc == auto_in_0_a_bits_source ? _flight_T : flight__12; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_15 = 6'hd == auto_in_0_a_bits_source ? _flight_T : flight__13; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_16 = 6'he == auto_in_0_a_bits_source ? _flight_T : flight__14; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_17 = 6'hf == auto_in_0_a_bits_source ? _flight_T : flight__15; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_18 = 6'h10 == auto_in_0_a_bits_source ? _flight_T : flight__16; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_19 = 6'h11 == auto_in_0_a_bits_source ? _flight_T : flight__17; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_20 = 6'h12 == auto_in_0_a_bits_source ? _flight_T : flight__18; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_21 = 6'h13 == auto_in_0_a_bits_source ? _flight_T : flight__19; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_22 = 6'h14 == auto_in_0_a_bits_source ? _flight_T : flight__20; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_23 = 6'h15 == auto_in_0_a_bits_source ? _flight_T : flight__21; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_24 = 6'h16 == auto_in_0_a_bits_source ? _flight_T : flight__22; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_25 = 6'h17 == auto_in_0_a_bits_source ? _flight_T : flight__23; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_26 = 6'h18 == auto_in_0_a_bits_source ? _flight_T : flight__24; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_27 = 6'h19 == auto_in_0_a_bits_source ? _flight_T : flight__25; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_28 = 6'h1a == auto_in_0_a_bits_source ? _flight_T : flight__26; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_29 = 6'h1b == auto_in_0_a_bits_source ? _flight_T : flight__27; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_30 = 6'h1c == auto_in_0_a_bits_source ? _flight_T : flight__28; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_31 = 6'h1d == auto_in_0_a_bits_source ? _flight_T : flight__29; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_32 = 6'h1e == auto_in_0_a_bits_source ? _flight_T : flight__30; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_33 = 6'h1f == auto_in_0_a_bits_source ? _flight_T : flight__31; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_34 = 6'h20 == auto_in_0_a_bits_source ? _flight_T : flight__32; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_35 = 6'h21 == auto_in_0_a_bits_source ? _flight_T : flight__33; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_36 = 6'h22 == auto_in_0_a_bits_source ? _flight_T : flight__34; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_37 = 6'h23 == auto_in_0_a_bits_source ? _flight_T : flight__35; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_39 = _T_1 ? _GEN_2 : flight__0; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_40 = _T_1 ? _GEN_3 : flight__1; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_41 = _T_1 ? _GEN_4 : flight__2; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_42 = _T_1 ? _GEN_5 : flight__3; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_43 = _T_1 ? _GEN_6 : flight__4; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_44 = _T_1 ? _GEN_7 : flight__5; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_45 = _T_1 ? _GEN_8 : flight__6; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_46 = _T_1 ? _GEN_9 : flight__7; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_47 = _T_1 ? _GEN_10 : flight__8; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_48 = _T_1 ? _GEN_11 : flight__9; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_49 = _T_1 ? _GEN_12 : flight__10; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_50 = _T_1 ? _GEN_13 : flight__11; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_51 = _T_1 ? _GEN_14 : flight__12; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_52 = _T_1 ? _GEN_15 : flight__13; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_53 = _T_1 ? _GEN_16 : flight__14; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_54 = _T_1 ? _GEN_17 : flight__15; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_55 = _T_1 ? _GEN_18 : flight__16; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_56 = _T_1 ? _GEN_19 : flight__17; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_57 = _T_1 ? _GEN_20 : flight__18; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_58 = _T_1 ? _GEN_21 : flight__19; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_59 = _T_1 ? _GEN_22 : flight__20; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_60 = _T_1 ? _GEN_23 : flight__21; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_61 = _T_1 ? _GEN_24 : flight__22; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_62 = _T_1 ? _GEN_25 : flight__23; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_63 = _T_1 ? _GEN_26 : flight__24; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_64 = _T_1 ? _GEN_27 : flight__25; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_65 = _T_1 ? _GEN_28 : flight__26; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_66 = _T_1 ? _GEN_29 : flight__27; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_67 = _T_1 ? _GEN_30 : flight__28; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_68 = _T_1 ? _GEN_31 : flight__29; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_69 = _T_1 ? _GEN_32 : flight__30; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_70 = _T_1 ? _GEN_33 : flight__31; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_71 = _T_1 ? _GEN_34 : flight__32; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_72 = _T_1 ? _GEN_35 : flight__33; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_73 = _T_1 ? _GEN_36 : flight__34; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_74 = _T_1 ? _GEN_37 : flight__35; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _T_3 = d_first & _d_first_T; // @[FIFOFixer.scala 73:21]
  wire  _stalls_id_T_3 = _a_first_T & stalls_a_sel & _flight_T; // @[FIFOFixer.scala 77:58]
  wire  _stalls_id_T_7 = _a_first_T & stalls_a_sel_1 & _flight_T; // @[FIFOFixer.scala 77:58]
  wire  _stalls_id_T_11 = _a_first_T & stalls_a_sel_2 & _flight_T; // @[FIFOFixer.scala 77:58]
  wire  _stalls_id_T_15 = _a_first_T & stalls_a_sel_3 & _flight_T; // @[FIFOFixer.scala 77:58]
  wire  _stalls_id_T_19 = _a_first_T & stalls_a_sel_4 & _flight_T; // @[FIFOFixer.scala 77:58]
  wire [36:0] _a_notFIFO_T_37 = auto_in_1_a_bits_address ^ 37'h8000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_notFIFO_T_38 = {1'b0,$signed(_a_notFIFO_T_37)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_notFIFO_T_40 = $signed(_a_notFIFO_T_38) & 38'sh188c000000; // @[Parameters.scala 137:52]
  wire  _a_notFIFO_T_41 = $signed(_a_notFIFO_T_40) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _a_notFIFO_T_42 = auto_in_1_a_bits_address ^ 37'h800000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_notFIFO_T_43 = {1'b0,$signed(_a_notFIFO_T_42)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_notFIFO_T_45 = $signed(_a_notFIFO_T_43) & 38'sh1800000000; // @[Parameters.scala 137:52]
  wire  _a_notFIFO_T_46 = $signed(_a_notFIFO_T_45) == 38'sh0; // @[Parameters.scala 137:67]
  wire  a_notFIFO_1 = _a_notFIFO_T_41 | _a_notFIFO_T_46; // @[Parameters.scala 630:89]
  wire [37:0] _a_notFIFO_T_49 = {1'b0,$signed(auto_in_1_a_bits_address)}; // @[Parameters.scala 137:49]
  wire [36:0] _a_notFIFO_T_63 = auto_in_1_a_bits_address ^ 37'h1000000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_notFIFO_T_64 = {1'b0,$signed(_a_notFIFO_T_63)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_notFIFO_T_66 = $signed(_a_notFIFO_T_64) & 38'sh1000000000; // @[Parameters.scala 137:52]
  wire  _a_notFIFO_T_67 = $signed(_a_notFIFO_T_66) == 38'sh0; // @[Parameters.scala 137:67]
  wire [37:0] _a_id_T_16 = $signed(_a_notFIFO_T_49) & 38'sh1000000000; // @[Parameters.scala 137:52]
  wire  _a_id_T_17 = $signed(_a_id_T_16) == 38'sh0; // @[Parameters.scala 137:67]
  wire [1:0] _a_id_T_24 = _a_notFIFO_T_67 ? 2'h2 : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _GEN_312 = {{1'd0}, _a_id_T_17}; // @[Mux.scala 27:72]
  wire [1:0] a_id_1 = _GEN_312 | _a_id_T_24; // @[Mux.scala 27:72]
  wire  a_noDomain_1 = a_id_1 == 2'h0; // @[FIFOFixer.scala 55:29]
  wire [3:0] stalls_a_sel_uncommonBits_5 = auto_in_1_a_bits_source[3:0]; // @[Parameters.scala 52:64]
  wire  _stalls_a_sel_T_27 = 4'h2 <= stalls_a_sel_uncommonBits_5; // @[Parameters.scala 56:34]
  wire  _stalls_a_sel_T_28 = ~auto_in_1_a_bits_source[4] & _stalls_a_sel_T_27; // @[Parameters.scala 54:69]
  wire  _stalls_a_sel_T_29 = stalls_a_sel_uncommonBits_5 <= 4'h8; // @[Parameters.scala 57:20]
  wire  stalls_a_sel_5 = _stalls_a_sel_T_28 & _stalls_a_sel_T_29; // @[Parameters.scala 56:50]
  reg [3:0] a_first_counter_1; // @[Edges.scala 228:27]
  wire  a_first_1 = a_first_counter_1 == 4'h0; // @[Edges.scala 230:25]
  reg  flight_1_2; // @[FIFOFixer.scala 71:27]
  reg  flight_1_3; // @[FIFOFixer.scala 71:27]
  reg  flight_1_4; // @[FIFOFixer.scala 71:27]
  reg  flight_1_5; // @[FIFOFixer.scala 71:27]
  reg  flight_1_6; // @[FIFOFixer.scala 71:27]
  reg  flight_1_7; // @[FIFOFixer.scala 71:27]
  reg  flight_1_8; // @[FIFOFixer.scala 71:27]
  reg [1:0] stalls_id_5; // @[Reg.scala 15:16]
  wire  stalls_0_1 = stalls_a_sel_5 & a_first_1 & (flight_1_2 | flight_1_3 | flight_1_4 | flight_1_5 | flight_1_6 |
    flight_1_7 | flight_1_8) & (a_noDomain_1 | stalls_id_5 != a_id_1); // @[FIFOFixer.scala 80:50]
  wire  _bundleIn_1_a_ready_T_1 = a_notFIFO_1 | ~stalls_0_1; // @[FIFOFixer.scala 88:47]
  wire  bundleIn_1_a_ready = auto_out_1_a_ready & _bundleIn_1_a_ready_T_1; // @[FIFOFixer.scala 88:33]
  wire  _a_first_T_1 = bundleIn_1_a_ready & auto_in_1_a_valid; // @[Decoupled.scala 40:37]
  wire [22:0] _a_first_beats1_decode_T_5 = 23'hff << auto_in_1_a_bits_size; // @[package.scala 234:77]
  wire [7:0] _a_first_beats1_decode_T_7 = ~_a_first_beats1_decode_T_5[7:0]; // @[package.scala 234:46]
  wire [3:0] a_first_beats1_decode_1 = _a_first_beats1_decode_T_7[7:4]; // @[Edges.scala 219:59]
  wire  a_first_beats1_opdata_1 = ~auto_in_1_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [3:0] a_first_counter1_1 = a_first_counter_1 - 4'h1; // @[Edges.scala 229:28]
  wire  _d_first_T_2 = auto_in_1_d_ready & auto_out_1_d_valid; // @[Decoupled.scala 40:37]
  wire [22:0] _d_first_beats1_decode_T_5 = 23'hff << auto_out_1_d_bits_size; // @[package.scala 234:77]
  wire [7:0] _d_first_beats1_decode_T_7 = ~_d_first_beats1_decode_T_5[7:0]; // @[package.scala 234:46]
  wire [3:0] d_first_beats1_decode_1 = _d_first_beats1_decode_T_7[7:4]; // @[Edges.scala 219:59]
  wire  d_first_beats1_opdata_1 = auto_out_1_d_bits_opcode[0]; // @[Edges.scala 105:36]
  reg [3:0] d_first_counter_1; // @[Edges.scala 228:27]
  wire [3:0] d_first_counter1_1 = d_first_counter_1 - 4'h1; // @[Edges.scala 229:28]
  wire  d_first_first_1 = d_first_counter_1 == 4'h0; // @[Edges.scala 230:25]
  wire  d_first_1 = d_first_first_1 & auto_out_1_d_bits_opcode != 3'h6; // @[FIFOFixer.scala 67:42]
  wire  _T_61 = a_first_1 & _a_first_T_1; // @[FIFOFixer.scala 72:21]
  wire  _flight_T_1 = ~a_notFIFO_1; // @[FIFOFixer.scala 72:67]
  wire  _GEN_161 = 5'h2 == auto_in_1_a_bits_source ? _flight_T_1 : flight_1_2; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_162 = 5'h3 == auto_in_1_a_bits_source ? _flight_T_1 : flight_1_3; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_163 = 5'h4 == auto_in_1_a_bits_source ? _flight_T_1 : flight_1_4; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_164 = 5'h5 == auto_in_1_a_bits_source ? _flight_T_1 : flight_1_5; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_165 = 5'h6 == auto_in_1_a_bits_source ? _flight_T_1 : flight_1_6; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_166 = 5'h7 == auto_in_1_a_bits_source ? _flight_T_1 : flight_1_7; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_167 = 5'h8 == auto_in_1_a_bits_source ? _flight_T_1 : flight_1_8; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_179 = _T_61 ? _GEN_161 : flight_1_2; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_180 = _T_61 ? _GEN_162 : flight_1_3; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_181 = _T_61 ? _GEN_163 : flight_1_4; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_182 = _T_61 ? _GEN_164 : flight_1_5; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_183 = _T_61 ? _GEN_165 : flight_1_6; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_184 = _T_61 ? _GEN_166 : flight_1_7; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_185 = _T_61 ? _GEN_167 : flight_1_8; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _T_63 = d_first_1 & _d_first_T_2; // @[FIFOFixer.scala 73:21]
  wire  _stalls_id_T_23 = _a_first_T_1 & stalls_a_sel_5 & _flight_T_1; // @[FIFOFixer.scala 77:58]
  wire [36:0] _a_notFIFO_T_74 = auto_in_2_a_bits_address ^ 37'h8000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_notFIFO_T_75 = {1'b0,$signed(_a_notFIFO_T_74)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_notFIFO_T_77 = $signed(_a_notFIFO_T_75) & 38'sh188c000000; // @[Parameters.scala 137:52]
  wire  _a_notFIFO_T_78 = $signed(_a_notFIFO_T_77) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _a_notFIFO_T_79 = auto_in_2_a_bits_address ^ 37'h800000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_notFIFO_T_80 = {1'b0,$signed(_a_notFIFO_T_79)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_notFIFO_T_82 = $signed(_a_notFIFO_T_80) & 38'sh1800000000; // @[Parameters.scala 137:52]
  wire  _a_notFIFO_T_83 = $signed(_a_notFIFO_T_82) == 38'sh0; // @[Parameters.scala 137:67]
  wire  a_notFIFO_2 = _a_notFIFO_T_78 | _a_notFIFO_T_83; // @[Parameters.scala 630:89]
  wire [37:0] _a_notFIFO_T_86 = {1'b0,$signed(auto_in_2_a_bits_address)}; // @[Parameters.scala 137:49]
  wire [36:0] _a_notFIFO_T_100 = auto_in_2_a_bits_address ^ 37'h1000000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_notFIFO_T_101 = {1'b0,$signed(_a_notFIFO_T_100)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_notFIFO_T_103 = $signed(_a_notFIFO_T_101) & 38'sh1000000000; // @[Parameters.scala 137:52]
  wire  _a_notFIFO_T_104 = $signed(_a_notFIFO_T_103) == 38'sh0; // @[Parameters.scala 137:67]
  wire [37:0] _a_id_T_29 = $signed(_a_notFIFO_T_86) & 38'sh1000000000; // @[Parameters.scala 137:52]
  wire  _a_id_T_30 = $signed(_a_id_T_29) == 38'sh0; // @[Parameters.scala 137:67]
  wire [1:0] _a_id_T_37 = _a_notFIFO_T_104 ? 2'h2 : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _GEN_313 = {{1'd0}, _a_id_T_30}; // @[Mux.scala 27:72]
  wire [1:0] a_id_2 = _GEN_313 | _a_id_T_37; // @[Mux.scala 27:72]
  wire  a_noDomain_2 = a_id_2 == 2'h0; // @[FIFOFixer.scala 55:29]
  wire [3:0] stalls_a_sel_uncommonBits_6 = auto_in_2_a_bits_source[3:0]; // @[Parameters.scala 52:64]
  wire  _stalls_a_sel_T_32 = 4'h2 <= stalls_a_sel_uncommonBits_6; // @[Parameters.scala 56:34]
  wire  _stalls_a_sel_T_33 = ~auto_in_2_a_bits_source[4] & _stalls_a_sel_T_32; // @[Parameters.scala 54:69]
  wire  _stalls_a_sel_T_34 = stalls_a_sel_uncommonBits_6 <= 4'h8; // @[Parameters.scala 57:20]
  wire  stalls_a_sel_6 = _stalls_a_sel_T_33 & _stalls_a_sel_T_34; // @[Parameters.scala 56:50]
  reg [3:0] a_first_counter_2; // @[Edges.scala 228:27]
  wire  a_first_2 = a_first_counter_2 == 4'h0; // @[Edges.scala 230:25]
  reg  flight_2_2; // @[FIFOFixer.scala 71:27]
  reg  flight_2_3; // @[FIFOFixer.scala 71:27]
  reg  flight_2_4; // @[FIFOFixer.scala 71:27]
  reg  flight_2_5; // @[FIFOFixer.scala 71:27]
  reg  flight_2_6; // @[FIFOFixer.scala 71:27]
  reg  flight_2_7; // @[FIFOFixer.scala 71:27]
  reg  flight_2_8; // @[FIFOFixer.scala 71:27]
  reg [1:0] stalls_id_6; // @[Reg.scala 15:16]
  wire  stalls_0_2 = stalls_a_sel_6 & a_first_2 & (flight_2_2 | flight_2_3 | flight_2_4 | flight_2_5 | flight_2_6 |
    flight_2_7 | flight_2_8) & (a_noDomain_2 | stalls_id_6 != a_id_2); // @[FIFOFixer.scala 80:50]
  wire  _bundleIn_2_a_ready_T_1 = a_notFIFO_2 | ~stalls_0_2; // @[FIFOFixer.scala 88:47]
  wire  bundleIn_2_a_ready = auto_out_2_a_ready & _bundleIn_2_a_ready_T_1; // @[FIFOFixer.scala 88:33]
  wire  _a_first_T_2 = bundleIn_2_a_ready & auto_in_2_a_valid; // @[Decoupled.scala 40:37]
  wire [22:0] _a_first_beats1_decode_T_9 = 23'hff << auto_in_2_a_bits_size; // @[package.scala 234:77]
  wire [7:0] _a_first_beats1_decode_T_11 = ~_a_first_beats1_decode_T_9[7:0]; // @[package.scala 234:46]
  wire [3:0] a_first_beats1_decode_2 = _a_first_beats1_decode_T_11[7:4]; // @[Edges.scala 219:59]
  wire  a_first_beats1_opdata_2 = ~auto_in_2_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [3:0] a_first_counter1_2 = a_first_counter_2 - 4'h1; // @[Edges.scala 229:28]
  wire  _d_first_T_4 = auto_in_2_d_ready & auto_out_2_d_valid; // @[Decoupled.scala 40:37]
  wire [22:0] _d_first_beats1_decode_T_9 = 23'hff << auto_out_2_d_bits_size; // @[package.scala 234:77]
  wire [7:0] _d_first_beats1_decode_T_11 = ~_d_first_beats1_decode_T_9[7:0]; // @[package.scala 234:46]
  wire [3:0] d_first_beats1_decode_2 = _d_first_beats1_decode_T_11[7:4]; // @[Edges.scala 219:59]
  wire  d_first_beats1_opdata_2 = auto_out_2_d_bits_opcode[0]; // @[Edges.scala 105:36]
  reg [3:0] d_first_counter_2; // @[Edges.scala 228:27]
  wire [3:0] d_first_counter1_2 = d_first_counter_2 - 4'h1; // @[Edges.scala 229:28]
  wire  d_first_first_2 = d_first_counter_2 == 4'h0; // @[Edges.scala 230:25]
  wire  d_first_2 = d_first_first_2 & auto_out_2_d_bits_opcode != 3'h6; // @[FIFOFixer.scala 67:42]
  wire  _T_102 = a_first_2 & _a_first_T_2; // @[FIFOFixer.scala 72:21]
  wire  _flight_T_2 = ~a_notFIFO_2; // @[FIFOFixer.scala 72:67]
  wire  _GEN_238 = 5'h2 == auto_in_2_a_bits_source ? _flight_T_2 : flight_2_2; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_239 = 5'h3 == auto_in_2_a_bits_source ? _flight_T_2 : flight_2_3; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_240 = 5'h4 == auto_in_2_a_bits_source ? _flight_T_2 : flight_2_4; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_241 = 5'h5 == auto_in_2_a_bits_source ? _flight_T_2 : flight_2_5; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_242 = 5'h6 == auto_in_2_a_bits_source ? _flight_T_2 : flight_2_6; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_243 = 5'h7 == auto_in_2_a_bits_source ? _flight_T_2 : flight_2_7; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_244 = 5'h8 == auto_in_2_a_bits_source ? _flight_T_2 : flight_2_8; // @[FIFOFixer.scala 72:64 FIFOFixer.scala 72:64 FIFOFixer.scala 71:27]
  wire  _GEN_256 = _T_102 ? _GEN_238 : flight_2_2; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_257 = _T_102 ? _GEN_239 : flight_2_3; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_258 = _T_102 ? _GEN_240 : flight_2_4; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_259 = _T_102 ? _GEN_241 : flight_2_5; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_260 = _T_102 ? _GEN_242 : flight_2_6; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_261 = _T_102 ? _GEN_243 : flight_2_7; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_262 = _T_102 ? _GEN_244 : flight_2_8; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _T_104 = d_first_2 & _d_first_T_4; // @[FIFOFixer.scala 73:21]
  wire  _stalls_id_T_27 = _a_first_T_2 & stalls_a_sel_6 & _flight_T_2; // @[FIFOFixer.scala 77:58]
  assign auto_in_2_a_ready = auto_out_2_a_ready & _bundleIn_2_a_ready_T_1; // @[FIFOFixer.scala 88:33]
  assign auto_in_2_b_valid = auto_out_2_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_opcode = auto_out_2_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_param = auto_out_2_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_size = auto_out_2_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_source = auto_out_2_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_address = auto_out_2_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_mask = auto_out_2_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_data = auto_out_2_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_b_bits_corrupt = auto_out_2_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_c_ready = auto_out_2_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_d_valid = auto_out_2_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_opcode = auto_out_2_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_param = auto_out_2_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_size = auto_out_2_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_source = auto_out_2_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_sink = auto_out_2_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_denied = auto_out_2_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_data = auto_out_2_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_corrupt = auto_out_2_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_2_e_ready = auto_out_2_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_a_ready = auto_out_1_a_ready & _bundleIn_1_a_ready_T_1; // @[FIFOFixer.scala 88:33]
  assign auto_in_1_b_valid = auto_out_1_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_opcode = auto_out_1_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_param = auto_out_1_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_size = auto_out_1_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_source = auto_out_1_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_address = auto_out_1_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_mask = auto_out_1_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_data = auto_out_1_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_b_bits_corrupt = auto_out_1_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_c_ready = auto_out_1_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_d_valid = auto_out_1_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_opcode = auto_out_1_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_param = auto_out_1_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_size = auto_out_1_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_source = auto_out_1_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_sink = auto_out_1_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_denied = auto_out_1_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_data = auto_out_1_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_corrupt = auto_out_1_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_1_e_ready = auto_out_1_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_a_ready = auto_out_0_a_ready & _bundleIn_0_a_ready_T_1; // @[FIFOFixer.scala 88:33]
  assign auto_in_0_d_valid = auto_out_0_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_opcode = auto_out_0_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_param = auto_out_0_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_size = auto_out_0_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_source = auto_out_0_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_sink = auto_out_0_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_denied = auto_out_0_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_data = auto_out_0_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_corrupt = auto_out_0_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_out_2_a_valid = auto_in_2_a_valid & _bundleIn_2_a_ready_T_1; // @[FIFOFixer.scala 87:33]
  assign auto_out_2_a_bits_opcode = auto_in_2_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_param = auto_in_2_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_size = auto_in_2_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_source = auto_in_2_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_address = auto_in_2_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_user_amba_prot_bufferable = auto_in_2_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_user_amba_prot_modifiable = auto_in_2_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_user_amba_prot_readalloc = auto_in_2_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_user_amba_prot_writealloc = auto_in_2_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_user_amba_prot_privileged = auto_in_2_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_user_amba_prot_secure = auto_in_2_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_user_amba_prot_fetch = auto_in_2_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_mask = auto_in_2_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_data = auto_in_2_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_corrupt = auto_in_2_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_b_ready = auto_in_2_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_valid = auto_in_2_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_bits_opcode = auto_in_2_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_bits_param = auto_in_2_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_bits_size = auto_in_2_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_bits_source = auto_in_2_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_bits_address = auto_in_2_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_bits_data = auto_in_2_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_c_bits_corrupt = auto_in_2_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_d_ready = auto_in_2_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_e_valid = auto_in_2_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_2_e_bits_sink = auto_in_2_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_valid = auto_in_1_a_valid & _bundleIn_1_a_ready_T_1; // @[FIFOFixer.scala 87:33]
  assign auto_out_1_a_bits_opcode = auto_in_1_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_param = auto_in_1_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_size = auto_in_1_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_source = auto_in_1_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_address = auto_in_1_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_user_amba_prot_bufferable = auto_in_1_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_user_amba_prot_modifiable = auto_in_1_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_user_amba_prot_readalloc = auto_in_1_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_user_amba_prot_writealloc = auto_in_1_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_user_amba_prot_privileged = auto_in_1_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_user_amba_prot_secure = auto_in_1_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_user_amba_prot_fetch = auto_in_1_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_mask = auto_in_1_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_data = auto_in_1_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_corrupt = auto_in_1_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_b_ready = auto_in_1_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_valid = auto_in_1_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_bits_opcode = auto_in_1_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_bits_param = auto_in_1_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_bits_size = auto_in_1_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_bits_source = auto_in_1_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_bits_address = auto_in_1_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_bits_data = auto_in_1_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_c_bits_corrupt = auto_in_1_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_d_ready = auto_in_1_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_e_valid = auto_in_1_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_1_e_bits_sink = auto_in_1_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_a_valid = auto_in_0_a_valid & _bundleIn_0_a_ready_T_1; // @[FIFOFixer.scala 87:33]
  assign auto_out_0_a_bits_opcode = auto_in_0_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_size = auto_in_0_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_source = auto_in_0_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_address = auto_in_0_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_user_amba_prot_bufferable = auto_in_0_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_user_amba_prot_modifiable = auto_in_0_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_user_amba_prot_readalloc = auto_in_0_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_user_amba_prot_writealloc = auto_in_0_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_user_amba_prot_privileged = auto_in_0_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_user_amba_prot_secure = auto_in_0_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_user_amba_prot_fetch = auto_in_0_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_mask = auto_in_0_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_data = auto_in_0_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_0_d_ready = auto_in_0_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      a_first_counter <= 4'h0;
    end else if (_a_first_T) begin
      if (a_first) begin
        if (a_first_beats1_opdata) begin
          a_first_counter <= a_first_beats1_decode;
        end else begin
          a_first_counter <= 4'h0;
        end
      end else begin
        a_first_counter <= a_first_counter1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__0 <= 1'h0;
    end else if (_T_3) begin
      if (6'h0 == auto_out_0_d_bits_source) begin
        flight__0 <= 1'h0;
      end else begin
        flight__0 <= _GEN_39;
      end
    end else begin
      flight__0 <= _GEN_39;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__1 <= 1'h0;
    end else if (_T_3) begin
      if (6'h1 == auto_out_0_d_bits_source) begin
        flight__1 <= 1'h0;
      end else begin
        flight__1 <= _GEN_40;
      end
    end else begin
      flight__1 <= _GEN_40;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__2 <= 1'h0;
    end else if (_T_3) begin
      if (6'h2 == auto_out_0_d_bits_source) begin
        flight__2 <= 1'h0;
      end else begin
        flight__2 <= _GEN_41;
      end
    end else begin
      flight__2 <= _GEN_41;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__3 <= 1'h0;
    end else if (_T_3) begin
      if (6'h3 == auto_out_0_d_bits_source) begin
        flight__3 <= 1'h0;
      end else begin
        flight__3 <= _GEN_42;
      end
    end else begin
      flight__3 <= _GEN_42;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__4 <= 1'h0;
    end else if (_T_3) begin
      if (6'h4 == auto_out_0_d_bits_source) begin
        flight__4 <= 1'h0;
      end else begin
        flight__4 <= _GEN_43;
      end
    end else begin
      flight__4 <= _GEN_43;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__5 <= 1'h0;
    end else if (_T_3) begin
      if (6'h5 == auto_out_0_d_bits_source) begin
        flight__5 <= 1'h0;
      end else begin
        flight__5 <= _GEN_44;
      end
    end else begin
      flight__5 <= _GEN_44;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__6 <= 1'h0;
    end else if (_T_3) begin
      if (6'h6 == auto_out_0_d_bits_source) begin
        flight__6 <= 1'h0;
      end else begin
        flight__6 <= _GEN_45;
      end
    end else begin
      flight__6 <= _GEN_45;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__7 <= 1'h0;
    end else if (_T_3) begin
      if (6'h7 == auto_out_0_d_bits_source) begin
        flight__7 <= 1'h0;
      end else begin
        flight__7 <= _GEN_46;
      end
    end else begin
      flight__7 <= _GEN_46;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id <= 2'h0;
    end else if (_stalls_id_T_3) begin
      stalls_id <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__8 <= 1'h0;
    end else if (_T_3) begin
      if (6'h8 == auto_out_0_d_bits_source) begin
        flight__8 <= 1'h0;
      end else begin
        flight__8 <= _GEN_47;
      end
    end else begin
      flight__8 <= _GEN_47;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__9 <= 1'h0;
    end else if (_T_3) begin
      if (6'h9 == auto_out_0_d_bits_source) begin
        flight__9 <= 1'h0;
      end else begin
        flight__9 <= _GEN_48;
      end
    end else begin
      flight__9 <= _GEN_48;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__10 <= 1'h0;
    end else if (_T_3) begin
      if (6'ha == auto_out_0_d_bits_source) begin
        flight__10 <= 1'h0;
      end else begin
        flight__10 <= _GEN_49;
      end
    end else begin
      flight__10 <= _GEN_49;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__11 <= 1'h0;
    end else if (_T_3) begin
      if (6'hb == auto_out_0_d_bits_source) begin
        flight__11 <= 1'h0;
      end else begin
        flight__11 <= _GEN_50;
      end
    end else begin
      flight__11 <= _GEN_50;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__12 <= 1'h0;
    end else if (_T_3) begin
      if (6'hc == auto_out_0_d_bits_source) begin
        flight__12 <= 1'h0;
      end else begin
        flight__12 <= _GEN_51;
      end
    end else begin
      flight__12 <= _GEN_51;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__13 <= 1'h0;
    end else if (_T_3) begin
      if (6'hd == auto_out_0_d_bits_source) begin
        flight__13 <= 1'h0;
      end else begin
        flight__13 <= _GEN_52;
      end
    end else begin
      flight__13 <= _GEN_52;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__14 <= 1'h0;
    end else if (_T_3) begin
      if (6'he == auto_out_0_d_bits_source) begin
        flight__14 <= 1'h0;
      end else begin
        flight__14 <= _GEN_53;
      end
    end else begin
      flight__14 <= _GEN_53;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__15 <= 1'h0;
    end else if (_T_3) begin
      if (6'hf == auto_out_0_d_bits_source) begin
        flight__15 <= 1'h0;
      end else begin
        flight__15 <= _GEN_54;
      end
    end else begin
      flight__15 <= _GEN_54;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id_1 <= 2'h0;
    end else if (_stalls_id_T_7) begin
      stalls_id_1 <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__16 <= 1'h0;
    end else if (_T_3) begin
      if (6'h10 == auto_out_0_d_bits_source) begin
        flight__16 <= 1'h0;
      end else begin
        flight__16 <= _GEN_55;
      end
    end else begin
      flight__16 <= _GEN_55;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__17 <= 1'h0;
    end else if (_T_3) begin
      if (6'h11 == auto_out_0_d_bits_source) begin
        flight__17 <= 1'h0;
      end else begin
        flight__17 <= _GEN_56;
      end
    end else begin
      flight__17 <= _GEN_56;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__18 <= 1'h0;
    end else if (_T_3) begin
      if (6'h12 == auto_out_0_d_bits_source) begin
        flight__18 <= 1'h0;
      end else begin
        flight__18 <= _GEN_57;
      end
    end else begin
      flight__18 <= _GEN_57;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__19 <= 1'h0;
    end else if (_T_3) begin
      if (6'h13 == auto_out_0_d_bits_source) begin
        flight__19 <= 1'h0;
      end else begin
        flight__19 <= _GEN_58;
      end
    end else begin
      flight__19 <= _GEN_58;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__20 <= 1'h0;
    end else if (_T_3) begin
      if (6'h14 == auto_out_0_d_bits_source) begin
        flight__20 <= 1'h0;
      end else begin
        flight__20 <= _GEN_59;
      end
    end else begin
      flight__20 <= _GEN_59;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__21 <= 1'h0;
    end else if (_T_3) begin
      if (6'h15 == auto_out_0_d_bits_source) begin
        flight__21 <= 1'h0;
      end else begin
        flight__21 <= _GEN_60;
      end
    end else begin
      flight__21 <= _GEN_60;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__22 <= 1'h0;
    end else if (_T_3) begin
      if (6'h16 == auto_out_0_d_bits_source) begin
        flight__22 <= 1'h0;
      end else begin
        flight__22 <= _GEN_61;
      end
    end else begin
      flight__22 <= _GEN_61;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__23 <= 1'h0;
    end else if (_T_3) begin
      if (6'h17 == auto_out_0_d_bits_source) begin
        flight__23 <= 1'h0;
      end else begin
        flight__23 <= _GEN_62;
      end
    end else begin
      flight__23 <= _GEN_62;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id_2 <= 2'h0;
    end else if (_stalls_id_T_11) begin
      stalls_id_2 <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__24 <= 1'h0;
    end else if (_T_3) begin
      if (6'h18 == auto_out_0_d_bits_source) begin
        flight__24 <= 1'h0;
      end else begin
        flight__24 <= _GEN_63;
      end
    end else begin
      flight__24 <= _GEN_63;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__25 <= 1'h0;
    end else if (_T_3) begin
      if (6'h19 == auto_out_0_d_bits_source) begin
        flight__25 <= 1'h0;
      end else begin
        flight__25 <= _GEN_64;
      end
    end else begin
      flight__25 <= _GEN_64;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__26 <= 1'h0;
    end else if (_T_3) begin
      if (6'h1a == auto_out_0_d_bits_source) begin
        flight__26 <= 1'h0;
      end else begin
        flight__26 <= _GEN_65;
      end
    end else begin
      flight__26 <= _GEN_65;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__27 <= 1'h0;
    end else if (_T_3) begin
      if (6'h1b == auto_out_0_d_bits_source) begin
        flight__27 <= 1'h0;
      end else begin
        flight__27 <= _GEN_66;
      end
    end else begin
      flight__27 <= _GEN_66;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__28 <= 1'h0;
    end else if (_T_3) begin
      if (6'h1c == auto_out_0_d_bits_source) begin
        flight__28 <= 1'h0;
      end else begin
        flight__28 <= _GEN_67;
      end
    end else begin
      flight__28 <= _GEN_67;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__29 <= 1'h0;
    end else if (_T_3) begin
      if (6'h1d == auto_out_0_d_bits_source) begin
        flight__29 <= 1'h0;
      end else begin
        flight__29 <= _GEN_68;
      end
    end else begin
      flight__29 <= _GEN_68;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__30 <= 1'h0;
    end else if (_T_3) begin
      if (6'h1e == auto_out_0_d_bits_source) begin
        flight__30 <= 1'h0;
      end else begin
        flight__30 <= _GEN_69;
      end
    end else begin
      flight__30 <= _GEN_69;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__31 <= 1'h0;
    end else if (_T_3) begin
      if (6'h1f == auto_out_0_d_bits_source) begin
        flight__31 <= 1'h0;
      end else begin
        flight__31 <= _GEN_70;
      end
    end else begin
      flight__31 <= _GEN_70;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id_3 <= 2'h0;
    end else if (_stalls_id_T_15) begin
      stalls_id_3 <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__32 <= 1'h0;
    end else if (_T_3) begin
      if (6'h20 == auto_out_0_d_bits_source) begin
        flight__32 <= 1'h0;
      end else begin
        flight__32 <= _GEN_71;
      end
    end else begin
      flight__32 <= _GEN_71;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__33 <= 1'h0;
    end else if (_T_3) begin
      if (6'h21 == auto_out_0_d_bits_source) begin
        flight__33 <= 1'h0;
      end else begin
        flight__33 <= _GEN_72;
      end
    end else begin
      flight__33 <= _GEN_72;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__34 <= 1'h0;
    end else if (_T_3) begin
      if (6'h22 == auto_out_0_d_bits_source) begin
        flight__34 <= 1'h0;
      end else begin
        flight__34 <= _GEN_73;
      end
    end else begin
      flight__34 <= _GEN_73;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight__35 <= 1'h0;
    end else if (_T_3) begin
      if (6'h23 == auto_out_0_d_bits_source) begin
        flight__35 <= 1'h0;
      end else begin
        flight__35 <= _GEN_74;
      end
    end else begin
      flight__35 <= _GEN_74;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id_4 <= 2'h0;
    end else if (_stalls_id_T_19) begin
      stalls_id_4 <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      d_first_counter <= 4'h0;
    end else if (_d_first_T) begin
      if (d_first_first) begin
        if (d_first_beats1_opdata) begin
          d_first_counter <= d_first_beats1_decode;
        end else begin
          d_first_counter <= 4'h0;
        end
      end else begin
        d_first_counter <= d_first_counter1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      a_first_counter_1 <= 4'h0;
    end else if (_a_first_T_1) begin
      if (a_first_1) begin
        if (a_first_beats1_opdata_1) begin
          a_first_counter_1 <= a_first_beats1_decode_1;
        end else begin
          a_first_counter_1 <= 4'h0;
        end
      end else begin
        a_first_counter_1 <= a_first_counter1_1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_1_2 <= 1'h0;
    end else if (_T_63) begin
      if (5'h2 == auto_out_1_d_bits_source) begin
        flight_1_2 <= 1'h0;
      end else begin
        flight_1_2 <= _GEN_179;
      end
    end else begin
      flight_1_2 <= _GEN_179;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_1_3 <= 1'h0;
    end else if (_T_63) begin
      if (5'h3 == auto_out_1_d_bits_source) begin
        flight_1_3 <= 1'h0;
      end else begin
        flight_1_3 <= _GEN_180;
      end
    end else begin
      flight_1_3 <= _GEN_180;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_1_4 <= 1'h0;
    end else if (_T_63) begin
      if (5'h4 == auto_out_1_d_bits_source) begin
        flight_1_4 <= 1'h0;
      end else begin
        flight_1_4 <= _GEN_181;
      end
    end else begin
      flight_1_4 <= _GEN_181;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_1_5 <= 1'h0;
    end else if (_T_63) begin
      if (5'h5 == auto_out_1_d_bits_source) begin
        flight_1_5 <= 1'h0;
      end else begin
        flight_1_5 <= _GEN_182;
      end
    end else begin
      flight_1_5 <= _GEN_182;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_1_6 <= 1'h0;
    end else if (_T_63) begin
      if (5'h6 == auto_out_1_d_bits_source) begin
        flight_1_6 <= 1'h0;
      end else begin
        flight_1_6 <= _GEN_183;
      end
    end else begin
      flight_1_6 <= _GEN_183;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_1_7 <= 1'h0;
    end else if (_T_63) begin
      if (5'h7 == auto_out_1_d_bits_source) begin
        flight_1_7 <= 1'h0;
      end else begin
        flight_1_7 <= _GEN_184;
      end
    end else begin
      flight_1_7 <= _GEN_184;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_1_8 <= 1'h0;
    end else if (_T_63) begin
      if (5'h8 == auto_out_1_d_bits_source) begin
        flight_1_8 <= 1'h0;
      end else begin
        flight_1_8 <= _GEN_185;
      end
    end else begin
      flight_1_8 <= _GEN_185;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id_5 <= 2'h0;
    end else if (_stalls_id_T_23) begin
      stalls_id_5 <= a_id_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      d_first_counter_1 <= 4'h0;
    end else if (_d_first_T_2) begin
      if (d_first_first_1) begin
        if (d_first_beats1_opdata_1) begin
          d_first_counter_1 <= d_first_beats1_decode_1;
        end else begin
          d_first_counter_1 <= 4'h0;
        end
      end else begin
        d_first_counter_1 <= d_first_counter1_1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      a_first_counter_2 <= 4'h0;
    end else if (_a_first_T_2) begin
      if (a_first_2) begin
        if (a_first_beats1_opdata_2) begin
          a_first_counter_2 <= a_first_beats1_decode_2;
        end else begin
          a_first_counter_2 <= 4'h0;
        end
      end else begin
        a_first_counter_2 <= a_first_counter1_2;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_2_2 <= 1'h0;
    end else if (_T_104) begin
      if (5'h2 == auto_out_2_d_bits_source) begin
        flight_2_2 <= 1'h0;
      end else begin
        flight_2_2 <= _GEN_256;
      end
    end else begin
      flight_2_2 <= _GEN_256;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_2_3 <= 1'h0;
    end else if (_T_104) begin
      if (5'h3 == auto_out_2_d_bits_source) begin
        flight_2_3 <= 1'h0;
      end else begin
        flight_2_3 <= _GEN_257;
      end
    end else begin
      flight_2_3 <= _GEN_257;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_2_4 <= 1'h0;
    end else if (_T_104) begin
      if (5'h4 == auto_out_2_d_bits_source) begin
        flight_2_4 <= 1'h0;
      end else begin
        flight_2_4 <= _GEN_258;
      end
    end else begin
      flight_2_4 <= _GEN_258;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_2_5 <= 1'h0;
    end else if (_T_104) begin
      if (5'h5 == auto_out_2_d_bits_source) begin
        flight_2_5 <= 1'h0;
      end else begin
        flight_2_5 <= _GEN_259;
      end
    end else begin
      flight_2_5 <= _GEN_259;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_2_6 <= 1'h0;
    end else if (_T_104) begin
      if (5'h6 == auto_out_2_d_bits_source) begin
        flight_2_6 <= 1'h0;
      end else begin
        flight_2_6 <= _GEN_260;
      end
    end else begin
      flight_2_6 <= _GEN_260;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_2_7 <= 1'h0;
    end else if (_T_104) begin
      if (5'h7 == auto_out_2_d_bits_source) begin
        flight_2_7 <= 1'h0;
      end else begin
        flight_2_7 <= _GEN_261;
      end
    end else begin
      flight_2_7 <= _GEN_261;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_2_8 <= 1'h0;
    end else if (_T_104) begin
      if (5'h8 == auto_out_2_d_bits_source) begin
        flight_2_8 <= 1'h0;
      end else begin
        flight_2_8 <= _GEN_262;
      end
    end else begin
      flight_2_8 <= _GEN_262;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id_6 <= 2'h0;
    end else if (_stalls_id_T_27) begin
      stalls_id_6 <= a_id_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      d_first_counter_2 <= 4'h0;
    end else if (_d_first_T_4) begin
      if (d_first_first_2) begin
        if (d_first_beats1_opdata_2) begin
          d_first_counter_2 <= d_first_beats1_decode_2;
        end else begin
          d_first_counter_2 <= 4'h0;
        end
      end else begin
        d_first_counter_2 <= d_first_counter1_2;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  a_first_counter = _RAND_0[3:0];
  _RAND_1 = {1{`RANDOM}};
  flight__0 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  flight__1 = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  flight__2 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  flight__3 = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  flight__4 = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  flight__5 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  flight__6 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  flight__7 = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  stalls_id = _RAND_9[1:0];
  _RAND_10 = {1{`RANDOM}};
  flight__8 = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  flight__9 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  flight__10 = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  flight__11 = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  flight__12 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  flight__13 = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  flight__14 = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  flight__15 = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  stalls_id_1 = _RAND_18[1:0];
  _RAND_19 = {1{`RANDOM}};
  flight__16 = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  flight__17 = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  flight__18 = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  flight__19 = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  flight__20 = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  flight__21 = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  flight__22 = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  flight__23 = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  stalls_id_2 = _RAND_27[1:0];
  _RAND_28 = {1{`RANDOM}};
  flight__24 = _RAND_28[0:0];
  _RAND_29 = {1{`RANDOM}};
  flight__25 = _RAND_29[0:0];
  _RAND_30 = {1{`RANDOM}};
  flight__26 = _RAND_30[0:0];
  _RAND_31 = {1{`RANDOM}};
  flight__27 = _RAND_31[0:0];
  _RAND_32 = {1{`RANDOM}};
  flight__28 = _RAND_32[0:0];
  _RAND_33 = {1{`RANDOM}};
  flight__29 = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  flight__30 = _RAND_34[0:0];
  _RAND_35 = {1{`RANDOM}};
  flight__31 = _RAND_35[0:0];
  _RAND_36 = {1{`RANDOM}};
  stalls_id_3 = _RAND_36[1:0];
  _RAND_37 = {1{`RANDOM}};
  flight__32 = _RAND_37[0:0];
  _RAND_38 = {1{`RANDOM}};
  flight__33 = _RAND_38[0:0];
  _RAND_39 = {1{`RANDOM}};
  flight__34 = _RAND_39[0:0];
  _RAND_40 = {1{`RANDOM}};
  flight__35 = _RAND_40[0:0];
  _RAND_41 = {1{`RANDOM}};
  stalls_id_4 = _RAND_41[1:0];
  _RAND_42 = {1{`RANDOM}};
  d_first_counter = _RAND_42[3:0];
  _RAND_43 = {1{`RANDOM}};
  a_first_counter_1 = _RAND_43[3:0];
  _RAND_44 = {1{`RANDOM}};
  flight_1_2 = _RAND_44[0:0];
  _RAND_45 = {1{`RANDOM}};
  flight_1_3 = _RAND_45[0:0];
  _RAND_46 = {1{`RANDOM}};
  flight_1_4 = _RAND_46[0:0];
  _RAND_47 = {1{`RANDOM}};
  flight_1_5 = _RAND_47[0:0];
  _RAND_48 = {1{`RANDOM}};
  flight_1_6 = _RAND_48[0:0];
  _RAND_49 = {1{`RANDOM}};
  flight_1_7 = _RAND_49[0:0];
  _RAND_50 = {1{`RANDOM}};
  flight_1_8 = _RAND_50[0:0];
  _RAND_51 = {1{`RANDOM}};
  stalls_id_5 = _RAND_51[1:0];
  _RAND_52 = {1{`RANDOM}};
  d_first_counter_1 = _RAND_52[3:0];
  _RAND_53 = {1{`RANDOM}};
  a_first_counter_2 = _RAND_53[3:0];
  _RAND_54 = {1{`RANDOM}};
  flight_2_2 = _RAND_54[0:0];
  _RAND_55 = {1{`RANDOM}};
  flight_2_3 = _RAND_55[0:0];
  _RAND_56 = {1{`RANDOM}};
  flight_2_4 = _RAND_56[0:0];
  _RAND_57 = {1{`RANDOM}};
  flight_2_5 = _RAND_57[0:0];
  _RAND_58 = {1{`RANDOM}};
  flight_2_6 = _RAND_58[0:0];
  _RAND_59 = {1{`RANDOM}};
  flight_2_7 = _RAND_59[0:0];
  _RAND_60 = {1{`RANDOM}};
  flight_2_8 = _RAND_60[0:0];
  _RAND_61 = {1{`RANDOM}};
  stalls_id_6 = _RAND_61[1:0];
  _RAND_62 = {1{`RANDOM}};
  d_first_counter_2 = _RAND_62[3:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    a_first_counter = 4'h0;
  end
  if (reset) begin
    flight__0 = 1'h0;
  end
  if (reset) begin
    flight__1 = 1'h0;
  end
  if (reset) begin
    flight__2 = 1'h0;
  end
  if (reset) begin
    flight__3 = 1'h0;
  end
  if (reset) begin
    flight__4 = 1'h0;
  end
  if (reset) begin
    flight__5 = 1'h0;
  end
  if (reset) begin
    flight__6 = 1'h0;
  end
  if (reset) begin
    flight__7 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id = 2'h0;
  end
  if (reset) begin
    flight__8 = 1'h0;
  end
  if (reset) begin
    flight__9 = 1'h0;
  end
  if (reset) begin
    flight__10 = 1'h0;
  end
  if (reset) begin
    flight__11 = 1'h0;
  end
  if (reset) begin
    flight__12 = 1'h0;
  end
  if (reset) begin
    flight__13 = 1'h0;
  end
  if (reset) begin
    flight__14 = 1'h0;
  end
  if (reset) begin
    flight__15 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id_1 = 2'h0;
  end
  if (reset) begin
    flight__16 = 1'h0;
  end
  if (reset) begin
    flight__17 = 1'h0;
  end
  if (reset) begin
    flight__18 = 1'h0;
  end
  if (reset) begin
    flight__19 = 1'h0;
  end
  if (reset) begin
    flight__20 = 1'h0;
  end
  if (reset) begin
    flight__21 = 1'h0;
  end
  if (reset) begin
    flight__22 = 1'h0;
  end
  if (reset) begin
    flight__23 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id_2 = 2'h0;
  end
  if (reset) begin
    flight__24 = 1'h0;
  end
  if (reset) begin
    flight__25 = 1'h0;
  end
  if (reset) begin
    flight__26 = 1'h0;
  end
  if (reset) begin
    flight__27 = 1'h0;
  end
  if (reset) begin
    flight__28 = 1'h0;
  end
  if (reset) begin
    flight__29 = 1'h0;
  end
  if (reset) begin
    flight__30 = 1'h0;
  end
  if (reset) begin
    flight__31 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id_3 = 2'h0;
  end
  if (reset) begin
    flight__32 = 1'h0;
  end
  if (reset) begin
    flight__33 = 1'h0;
  end
  if (reset) begin
    flight__34 = 1'h0;
  end
  if (reset) begin
    flight__35 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id_4 = 2'h0;
  end
  if (reset) begin
    d_first_counter = 4'h0;
  end
  if (reset) begin
    a_first_counter_1 = 4'h0;
  end
  if (reset) begin
    flight_1_2 = 1'h0;
  end
  if (reset) begin
    flight_1_3 = 1'h0;
  end
  if (reset) begin
    flight_1_4 = 1'h0;
  end
  if (reset) begin
    flight_1_5 = 1'h0;
  end
  if (reset) begin
    flight_1_6 = 1'h0;
  end
  if (reset) begin
    flight_1_7 = 1'h0;
  end
  if (reset) begin
    flight_1_8 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id_5 = 2'h0;
  end
  if (reset) begin
    d_first_counter_1 = 4'h0;
  end
  if (reset) begin
    a_first_counter_2 = 4'h0;
  end
  if (reset) begin
    flight_2_2 = 1'h0;
  end
  if (reset) begin
    flight_2_3 = 1'h0;
  end
  if (reset) begin
    flight_2_4 = 1'h0;
  end
  if (reset) begin
    flight_2_5 = 1'h0;
  end
  if (reset) begin
    flight_2_6 = 1'h0;
  end
  if (reset) begin
    flight_2_7 = 1'h0;
  end
  if (reset) begin
    flight_2_8 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id_6 = 2'h0;
  end
  if (reset) begin
    d_first_counter_2 = 4'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
