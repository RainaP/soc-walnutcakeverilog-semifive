//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLRAM(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_a_ready,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input         auto_in_a_bits_source,
  input  [30:0] auto_in_a_bits_address,
  input  [31:0] auto_in_a_bits_data,
  input         auto_in_d_ready,
  output        auto_in_d_valid,
  output [2:0]  auto_in_d_bits_opcode,
  output        auto_in_d_bits_source,
  output [31:0] auto_in_d_bits_data
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
`endif // RANDOMIZE_REG_INIT
  wire [5:0] TraceSRAM_0_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  TraceSRAM_0_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  TraceSRAM_0_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  TraceSRAM_0_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [7:0] TraceSRAM_0_RW0_wdata_0; // @[DescribedSRAM.scala 18:26]
  wire [7:0] TraceSRAM_0_RW0_wdata_1; // @[DescribedSRAM.scala 18:26]
  wire [7:0] TraceSRAM_0_RW0_wdata_2; // @[DescribedSRAM.scala 18:26]
  wire [7:0] TraceSRAM_0_RW0_wdata_3; // @[DescribedSRAM.scala 18:26]
  wire [7:0] TraceSRAM_0_RW0_rdata_0; // @[DescribedSRAM.scala 18:26]
  wire [7:0] TraceSRAM_0_RW0_rdata_1; // @[DescribedSRAM.scala 18:26]
  wire [7:0] TraceSRAM_0_RW0_rdata_2; // @[DescribedSRAM.scala 18:26]
  wire [7:0] TraceSRAM_0_RW0_rdata_3; // @[DescribedSRAM.scala 18:26]
  reg  r_full; // @[SRAM.scala 140:30]
  reg  r_source; // @[SRAM.scala 144:26]
  reg  r_read; // @[SRAM.scala 145:26]
  reg  REG; // @[SRAM.scala 327:58]
  reg [7:0] r_1; // @[Reg.scala 15:16]
  wire [7:0] r_raw_data_1 = REG ? TraceSRAM_0_RW0_rdata_1 : r_1; // @[package.scala 79:42]
  reg [7:0] r_0; // @[Reg.scala 15:16]
  wire [7:0] r_raw_data_0 = REG ? TraceSRAM_0_RW0_rdata_0 : r_0; // @[package.scala 79:42]
  reg [7:0] r_3; // @[Reg.scala 15:16]
  wire [7:0] r_raw_data_3 = REG ? TraceSRAM_0_RW0_rdata_3 : r_3; // @[package.scala 79:42]
  reg [7:0] r_2; // @[Reg.scala 15:16]
  wire [7:0] r_raw_data_2 = REG ? TraceSRAM_0_RW0_rdata_2 : r_2; // @[package.scala 79:42]
  wire [15:0] r_uncorrected_lo = {r_raw_data_1,r_raw_data_0}; // @[Cat.scala 30:58]
  wire [15:0] r_uncorrected_hi = {r_raw_data_3,r_raw_data_2}; // @[Cat.scala 30:58]
  wire  _bundleIn_0_a_ready_T_2 = ~r_full; // @[SRAM.scala 249:41]
  wire  in_a_ready = _bundleIn_0_a_ready_T_2 | auto_in_d_ready; // @[SRAM.scala 249:49]
  wire  a_read = auto_in_a_bits_opcode == 3'h4; // @[SRAM.scala 257:35]
  wire  _GEN_18 = auto_in_d_ready ? 1'h0 : r_full; // @[SRAM.scala 279:20 SRAM.scala 279:29 SRAM.scala 140:30]
  wire  _T_24 = in_a_ready & auto_in_a_valid; // @[Decoupled.scala 40:37]
  wire  _T_25 = ~a_read; // @[SRAM.scala 293:13]
  wire  wen = _T_24 & _T_25; // @[SRAM.scala 315:52]
  wire  ren = ~wen & _T_24; // @[SRAM.scala 316:20]
  wire  index_lo_lo = auto_in_a_bits_address[2]; // @[SRAM.scala 326:60]
  wire  index_lo_hi_lo = auto_in_a_bits_address[3]; // @[SRAM.scala 326:60]
  wire  index_lo_hi_hi = auto_in_a_bits_address[4]; // @[SRAM.scala 326:60]
  wire  index_hi_lo = auto_in_a_bits_address[5]; // @[SRAM.scala 326:60]
  wire  index_hi_hi_lo = auto_in_a_bits_address[6]; // @[SRAM.scala 326:60]
  wire  index_hi_hi_hi = auto_in_a_bits_address[7]; // @[SRAM.scala 326:60]
  wire [2:0] index_lo = {index_lo_hi_hi,index_lo_hi_lo,index_lo_lo}; // @[Cat.scala 30:58]
  wire [2:0] index_hi = {index_hi_hi_hi,index_hi_hi_lo,index_hi_lo}; // @[Cat.scala 30:58]
  RHEA__TraceSRAM_0 TraceSRAM_0 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(TraceSRAM_0_RW0_addr),
    .RW0_en(TraceSRAM_0_RW0_en),
    .RW0_clk(TraceSRAM_0_RW0_clk),
    .RW0_wmode(TraceSRAM_0_RW0_wmode),
    .RW0_wdata_0(TraceSRAM_0_RW0_wdata_0),
    .RW0_wdata_1(TraceSRAM_0_RW0_wdata_1),
    .RW0_wdata_2(TraceSRAM_0_RW0_wdata_2),
    .RW0_wdata_3(TraceSRAM_0_RW0_wdata_3),
    .RW0_rdata_0(TraceSRAM_0_RW0_rdata_0),
    .RW0_rdata_1(TraceSRAM_0_RW0_rdata_1),
    .RW0_rdata_2(TraceSRAM_0_RW0_rdata_2),
    .RW0_rdata_3(TraceSRAM_0_RW0_rdata_3)
  );
  assign auto_in_a_ready = _bundleIn_0_a_ready_T_2 | auto_in_d_ready; // @[SRAM.scala 249:49]
  assign auto_in_d_valid = r_full; // @[SRAM.scala 246:65]
  assign auto_in_d_bits_opcode = {{2'd0}, r_read}; // @[SRAM.scala 43:27 SRAM.scala 215:23]
  assign auto_in_d_bits_source = r_source; // @[SRAM.scala 218:29]
  assign auto_in_d_bits_data = {r_uncorrected_hi,r_uncorrected_lo}; // @[Cat.scala 30:58]
  assign TraceSRAM_0_RW0_wdata_0 = auto_in_a_bits_data[7:0]; // @[SRAM.scala 297:67]
  assign TraceSRAM_0_RW0_wdata_1 = auto_in_a_bits_data[15:8]; // @[SRAM.scala 297:67]
  assign TraceSRAM_0_RW0_wdata_2 = auto_in_a_bits_data[23:16]; // @[SRAM.scala 297:67]
  assign TraceSRAM_0_RW0_wdata_3 = auto_in_a_bits_data[31:24]; // @[SRAM.scala 297:67]
  assign TraceSRAM_0_RW0_wmode = _T_24 & _T_25; // @[SRAM.scala 315:52]
  assign TraceSRAM_0_RW0_clk = clock;
  assign TraceSRAM_0_RW0_en = ren | wen;
  assign TraceSRAM_0_RW0_addr = {index_hi,index_lo}; // @[Cat.scala 30:58]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r_full <= 1'h0;
    end else begin
      r_full <= _T_24 | _GEN_18;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r_source <= 1'h0;
    end else if (_T_24) begin
      r_source <= auto_in_a_bits_source;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r_read <= 1'h0;
    end else if (_T_24) begin
      r_read <= a_read;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG <= 1'h0;
    end else begin
      REG <= ~wen & _T_24;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r_1 <= 8'h0;
    end else if (REG) begin
      r_1 <= TraceSRAM_0_RW0_rdata_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r_0 <= 8'h0;
    end else if (REG) begin
      r_0 <= TraceSRAM_0_RW0_rdata_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r_3 <= 8'h0;
    end else if (REG) begin
      r_3 <= TraceSRAM_0_RW0_rdata_3;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r_2 <= 8'h0;
    end else if (REG) begin
      r_2 <= TraceSRAM_0_RW0_rdata_2;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  r_full = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  r_source = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  r_read = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  REG = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  r_1 = _RAND_4[7:0];
  _RAND_5 = {1{`RANDOM}};
  r_0 = _RAND_5[7:0];
  _RAND_6 = {1{`RANDOM}};
  r_3 = _RAND_6[7:0];
  _RAND_7 = {1{`RANDOM}};
  r_2 = _RAND_7[7:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    r_full = 1'h0;
  end
  if (rf_reset) begin
    r_source = 1'h0;
  end
  if (rf_reset) begin
    r_read = 1'h0;
  end
  if (rf_reset) begin
    REG = 1'h0;
  end
  if (rf_reset) begin
    r_1 = 8'h0;
  end
  if (rf_reset) begin
    r_0 = 8'h0;
  end
  if (rf_reset) begin
    r_3 = 8'h0;
  end
  if (rf_reset) begin
    r_2 = 8'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
