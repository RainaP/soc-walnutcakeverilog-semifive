//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__AXI4IdIndexer_1(
  output        auto_in_aw_ready,
  input         auto_in_aw_valid,
  input  [15:0] auto_in_aw_bits_id,
  input  [36:0] auto_in_aw_bits_addr,
  input  [7:0]  auto_in_aw_bits_len,
  input  [2:0]  auto_in_aw_bits_size,
  input  [1:0]  auto_in_aw_bits_burst,
  input  [3:0]  auto_in_aw_bits_cache,
  input  [2:0]  auto_in_aw_bits_prot,
  output        auto_in_w_ready,
  input         auto_in_w_valid,
  input  [63:0] auto_in_w_bits_data,
  input  [7:0]  auto_in_w_bits_strb,
  input         auto_in_w_bits_last,
  input         auto_in_b_ready,
  output        auto_in_b_valid,
  output [15:0] auto_in_b_bits_id,
  output [1:0]  auto_in_b_bits_resp,
  output        auto_in_ar_ready,
  input         auto_in_ar_valid,
  input  [15:0] auto_in_ar_bits_id,
  input  [36:0] auto_in_ar_bits_addr,
  input  [7:0]  auto_in_ar_bits_len,
  input  [2:0]  auto_in_ar_bits_size,
  input  [1:0]  auto_in_ar_bits_burst,
  input  [3:0]  auto_in_ar_bits_cache,
  input  [2:0]  auto_in_ar_bits_prot,
  input         auto_in_r_ready,
  output        auto_in_r_valid,
  output [15:0] auto_in_r_bits_id,
  output [63:0] auto_in_r_bits_data,
  output [1:0]  auto_in_r_bits_resp,
  output        auto_in_r_bits_last,
  input         auto_out_aw_ready,
  output        auto_out_aw_valid,
  output [1:0]  auto_out_aw_bits_id,
  output [36:0] auto_out_aw_bits_addr,
  output [7:0]  auto_out_aw_bits_len,
  output [2:0]  auto_out_aw_bits_size,
  output [1:0]  auto_out_aw_bits_burst,
  output [3:0]  auto_out_aw_bits_cache,
  output [2:0]  auto_out_aw_bits_prot,
  output [13:0] auto_out_aw_bits_echo_extra_id,
  input         auto_out_w_ready,
  output        auto_out_w_valid,
  output [63:0] auto_out_w_bits_data,
  output [7:0]  auto_out_w_bits_strb,
  output        auto_out_w_bits_last,
  output        auto_out_b_ready,
  input         auto_out_b_valid,
  input  [1:0]  auto_out_b_bits_id,
  input  [1:0]  auto_out_b_bits_resp,
  input  [13:0] auto_out_b_bits_echo_extra_id,
  input         auto_out_ar_ready,
  output        auto_out_ar_valid,
  output [1:0]  auto_out_ar_bits_id,
  output [36:0] auto_out_ar_bits_addr,
  output [7:0]  auto_out_ar_bits_len,
  output [2:0]  auto_out_ar_bits_size,
  output [1:0]  auto_out_ar_bits_burst,
  output [3:0]  auto_out_ar_bits_cache,
  output [2:0]  auto_out_ar_bits_prot,
  output [13:0] auto_out_ar_bits_echo_extra_id,
  output        auto_out_r_ready,
  input         auto_out_r_valid,
  input  [1:0]  auto_out_r_bits_id,
  input  [63:0] auto_out_r_bits_data,
  input  [1:0]  auto_out_r_bits_resp,
  input  [13:0] auto_out_r_bits_echo_extra_id,
  input         auto_out_r_bits_last
);
  assign auto_in_aw_ready = auto_out_aw_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_w_ready = auto_out_w_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_b_valid = auto_out_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_b_bits_id = {auto_out_b_bits_echo_extra_id,auto_out_b_bits_id}; // @[Cat.scala 30:58]
  assign auto_in_b_bits_resp = auto_out_b_bits_resp; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_ar_ready = auto_out_ar_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_valid = auto_out_r_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_bits_id = {auto_out_r_bits_echo_extra_id,auto_out_r_bits_id}; // @[Cat.scala 30:58]
  assign auto_in_r_bits_data = auto_out_r_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_bits_resp = auto_out_r_bits_resp; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_bits_last = auto_out_r_bits_last; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_out_aw_valid = auto_in_aw_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_id = auto_in_aw_bits_id[1:0]; // @[Nodes.scala 1529:13 BundleMap.scala 247:19]
  assign auto_out_aw_bits_addr = auto_in_aw_bits_addr; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_len = auto_in_aw_bits_len; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_size = auto_in_aw_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_burst = auto_in_aw_bits_burst; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_cache = auto_in_aw_bits_cache; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_prot = auto_in_aw_bits_prot; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_echo_extra_id = auto_in_aw_bits_id[15:2]; // @[IdIndexer.scala 87:56]
  assign auto_out_w_valid = auto_in_w_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_w_bits_data = auto_in_w_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_w_bits_strb = auto_in_w_bits_strb; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_w_bits_last = auto_in_w_bits_last; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_b_ready = auto_in_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_valid = auto_in_ar_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_id = auto_in_ar_bits_id[1:0]; // @[Nodes.scala 1529:13 BundleMap.scala 247:19]
  assign auto_out_ar_bits_addr = auto_in_ar_bits_addr; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_len = auto_in_ar_bits_len; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_size = auto_in_ar_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_burst = auto_in_ar_bits_burst; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_cache = auto_in_ar_bits_cache; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_prot = auto_in_ar_bits_prot; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_echo_extra_id = auto_in_ar_bits_id[15:2]; // @[IdIndexer.scala 86:56]
  assign auto_out_r_ready = auto_in_r_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
endmodule
