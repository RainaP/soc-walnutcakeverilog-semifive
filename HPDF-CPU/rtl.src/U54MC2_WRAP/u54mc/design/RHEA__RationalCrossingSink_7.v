//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__RationalCrossingSink_7(
  input          clock,
  input          reset,
  input  [2:0]   io_enq_bits0_opcode,
  input  [1:0]   io_enq_bits0_param,
  input  [3:0]   io_enq_bits0_size,
  input  [4:0]   io_enq_bits0_source,
  input  [36:0]  io_enq_bits0_address,
  input  [15:0]  io_enq_bits0_mask,
  input  [127:0] io_enq_bits0_data,
  input          io_enq_bits0_corrupt,
  input  [2:0]   io_enq_bits1_opcode,
  input  [1:0]   io_enq_bits1_param,
  input  [3:0]   io_enq_bits1_size,
  input  [4:0]   io_enq_bits1_source,
  input  [36:0]  io_enq_bits1_address,
  input  [15:0]  io_enq_bits1_mask,
  input  [127:0] io_enq_bits1_data,
  input          io_enq_bits1_corrupt,
  input          io_enq_valid,
  input  [1:0]   io_enq_source,
  output         io_enq_ready,
  output [1:0]   io_enq_sink,
  input          io_deq_ready,
  output         io_deq_valid,
  output [2:0]   io_deq_bits_opcode,
  output [1:0]   io_deq_bits_param,
  output [3:0]   io_deq_bits_size,
  output [4:0]   io_deq_bits_source,
  output [36:0]  io_deq_bits_address,
  output [15:0]  io_deq_bits_mask,
  output [127:0] io_deq_bits_data,
  output         io_deq_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
`endif // RANDOMIZE_REG_INIT
  reg [1:0] count; // @[RationalCrossing.scala 120:22]
  wire  equal = count == io_enq_source; // @[RationalCrossing.scala 121:21]
  wire  _deq_valid_T = count[1]; // @[RationalCrossing.scala 126:43]
  wire  _deq_valid_T_1 = io_enq_source[0]; // @[RationalCrossing.scala 126:61]
  wire  deq_valid = equal ? io_enq_valid : count[1] != io_enq_source[0]; // @[RationalCrossing.scala 126:19]
  wire  _T = io_deq_ready & deq_valid; // @[Decoupled.scala 40:37]
  wire  count_hi = count[0]; // @[RationalCrossing.scala 128:41]
  wire  count_lo = ~count[1]; // @[RationalCrossing.scala 128:46]
  wire [1:0] _count_T_1 = {count_hi,count_lo}; // @[Cat.scala 30:58]
  assign io_enq_ready = io_deq_ready; // @[RationalCrossing.scala 112:17 RationalCrossing.scala 117:31]
  assign io_enq_sink = count; // @[RationalCrossing.scala 124:13]
  assign io_deq_valid = equal ? io_enq_valid : count[1] != io_enq_source[0]; // @[RationalCrossing.scala 126:19]
  assign io_deq_bits_opcode = equal ? io_enq_bits0_opcode : io_enq_bits1_opcode; // @[RationalCrossing.scala 125:19]
  assign io_deq_bits_param = equal ? io_enq_bits0_param : io_enq_bits1_param; // @[RationalCrossing.scala 125:19]
  assign io_deq_bits_size = equal ? io_enq_bits0_size : io_enq_bits1_size; // @[RationalCrossing.scala 125:19]
  assign io_deq_bits_source = equal ? io_enq_bits0_source : io_enq_bits1_source; // @[RationalCrossing.scala 125:19]
  assign io_deq_bits_address = equal ? io_enq_bits0_address : io_enq_bits1_address; // @[RationalCrossing.scala 125:19]
  assign io_deq_bits_mask = equal ? io_enq_bits0_mask : io_enq_bits1_mask; // @[RationalCrossing.scala 125:19]
  assign io_deq_bits_data = equal ? io_enq_bits0_data : io_enq_bits1_data; // @[RationalCrossing.scala 125:19]
  assign io_deq_bits_corrupt = equal ? io_enq_bits0_corrupt : io_enq_bits1_corrupt; // @[RationalCrossing.scala 125:19]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count <= 2'h0;
    end else if (_T) begin
      count <= _count_T_1;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  count = _RAND_0[1:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    count = 2'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
