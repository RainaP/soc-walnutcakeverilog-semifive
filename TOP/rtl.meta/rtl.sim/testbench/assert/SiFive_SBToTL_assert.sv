//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_SBToTL_assert(
  input   clock,
  input   reset,
  input   _T,
  input   _T_1,
  input   _T_289,
  input   _T_290,
  input   _T_320
);
  wire  _T_349; // @[SBA.scala 373:35]
  wire  _T_351; // @[SBA.scala 374:44]
  wire  _T_353; // @[SBA.scala 375:45]
  wire  _T_355; // @[SBA.scala 376:45]
  wire  _T_357; // @[SBA.scala 373:12]
  wire  _T_358; // @[SBA.scala 373:12]
  assign _T_349 = _T_320 | _T_289; // @[SBA.scala 373:35]
  assign _T_351 = _T_349 | _T_290; // @[SBA.scala 374:44]
  assign _T_353 = _T_351 | _T; // @[SBA.scala 375:45]
  assign _T_355 = _T_353 | _T_1; // @[SBA.scala 376:45]
  assign _T_357 = _T_355 | reset; // @[SBA.scala 373:12]
  assign _T_358 = ~_T_357; // @[SBA.scala 373:12]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_358) begin
          $fwrite(32'h80000002,"Assertion Failed: SBA state machine in undefined state\n    at SBA.scala:373 assert (sbState === Idle.id.U ||\n"); // @[SBA.scala 373:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_358) begin
          $fatal; // @[SBA.scala 373:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
