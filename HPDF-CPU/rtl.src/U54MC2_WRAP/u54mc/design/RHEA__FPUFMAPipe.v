//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__FPUFMAPipe(
  input         rf_reset,
  input         clock,
  input         reset,
  input         io_in_valid,
  input         io_in_bits_ren3,
  input         io_in_bits_swap23,
  input  [2:0]  io_in_bits_rm,
  input  [1:0]  io_in_bits_fmaCmd,
  input  [64:0] io_in_bits_in1,
  input  [64:0] io_in_bits_in2,
  input  [64:0] io_in_bits_in3,
  output [64:0] io_out_bits_data,
  output [4:0]  io_out_bits_exc
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [95:0] _RAND_3;
  reg [95:0] _RAND_4;
  reg [95:0] _RAND_5;
`endif // RANDOMIZE_REG_INIT
  wire  fma_rf_reset; // @[FPU.scala 715:19]
  wire  fma_clock; // @[FPU.scala 715:19]
  wire  fma_reset; // @[FPU.scala 715:19]
  wire  fma_io_validin; // @[FPU.scala 715:19]
  wire [1:0] fma_io_op; // @[FPU.scala 715:19]
  wire [32:0] fma_io_a; // @[FPU.scala 715:19]
  wire [32:0] fma_io_b; // @[FPU.scala 715:19]
  wire [32:0] fma_io_c; // @[FPU.scala 715:19]
  wire [2:0] fma_io_roundingMode; // @[FPU.scala 715:19]
  wire [32:0] fma_io_out; // @[FPU.scala 715:19]
  wire [4:0] fma_io_exceptionFlags; // @[FPU.scala 715:19]
  reg  valid; // @[FPU.scala 703:18]
  reg [2:0] in_rm; // @[FPU.scala 704:15]
  reg [1:0] in_fmaCmd; // @[FPU.scala 704:15]
  reg [64:0] in_in1; // @[FPU.scala 704:15]
  reg [64:0] in_in2; // @[FPU.scala 704:15]
  reg [64:0] in_in3; // @[FPU.scala 704:15]
  wire [64:0] _zero_T = io_in_bits_in1 ^ io_in_bits_in2; // @[FPU.scala 707:32]
  wire [64:0] zero = _zero_T & 65'h100000000; // @[FPU.scala 707:50]
  RHEA__MulAddRecFNPipe fma ( // @[FPU.scala 715:19]
    .rf_reset(fma_rf_reset),
    .clock(fma_clock),
    .reset(fma_reset),
    .io_validin(fma_io_validin),
    .io_op(fma_io_op),
    .io_a(fma_io_a),
    .io_b(fma_io_b),
    .io_c(fma_io_c),
    .io_roundingMode(fma_io_roundingMode),
    .io_out(fma_io_out),
    .io_exceptionFlags(fma_io_exceptionFlags)
  );
  assign fma_rf_reset = rf_reset;
  assign io_out_bits_data = {{32'd0}, fma_io_out}; // @[FPU.scala 724:17 FPU.scala 725:12]
  assign io_out_bits_exc = fma_io_exceptionFlags; // @[FPU.scala 724:17 FPU.scala 726:11]
  assign fma_clock = clock;
  assign fma_reset = reset;
  assign fma_io_validin = valid; // @[FPU.scala 716:18]
  assign fma_io_op = in_fmaCmd; // @[FPU.scala 717:13]
  assign fma_io_a = in_in1[32:0]; // @[FPU.scala 720:12]
  assign fma_io_b = in_in2[32:0]; // @[FPU.scala 721:12]
  assign fma_io_c = in_in3[32:0]; // @[FPU.scala 722:12]
  assign fma_io_roundingMode = in_rm; // @[FPU.scala 718:23]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      valid <= 1'h0;
    end else begin
      valid <= io_in_valid;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      in_rm <= 3'h0;
    end else if (io_in_valid) begin
      in_rm <= io_in_bits_rm;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      in_fmaCmd <= 2'h0;
    end else if (io_in_valid) begin
      in_fmaCmd <= io_in_bits_fmaCmd;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      in_in1 <= 65'h0;
    end else if (io_in_valid) begin
      in_in1 <= io_in_bits_in1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      in_in2 <= 65'h0;
    end else if (io_in_valid) begin
      if (io_in_bits_swap23) begin
        in_in2 <= 65'h80000000;
      end else begin
        in_in2 <= io_in_bits_in2;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      in_in3 <= 65'h0;
    end else if (io_in_valid) begin
      if (~(io_in_bits_ren3 | io_in_bits_swap23)) begin
        in_in3 <= zero;
      end else begin
        in_in3 <= io_in_bits_in3;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  valid = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  in_rm = _RAND_1[2:0];
  _RAND_2 = {1{`RANDOM}};
  in_fmaCmd = _RAND_2[1:0];
  _RAND_3 = {3{`RANDOM}};
  in_in1 = _RAND_3[64:0];
  _RAND_4 = {3{`RANDOM}};
  in_in2 = _RAND_4[64:0];
  _RAND_5 = {3{`RANDOM}};
  in_in3 = _RAND_5[64:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    valid = 1'h0;
  end
  if (rf_reset) begin
    in_rm = 3'h0;
  end
  if (rf_reset) begin
    in_fmaCmd = 2'h0;
  end
  if (rf_reset) begin
    in_in1 = 65'h0;
  end
  if (rf_reset) begin
    in_in2 = 65'h0;
  end
  if (rf_reset) begin
    in_in3 = 65'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
