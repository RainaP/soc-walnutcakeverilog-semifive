//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TraceTrigBlock(
  input        rf_reset,
  input        clock,
  input        reset,
  input        auto_bpwatch_in_0_valid_0,
  input  [2:0] auto_bpwatch_in_0_action,
  input        auto_bpwatch_in_1_valid_0,
  input  [2:0] auto_bpwatch_in_1_action,
  input        auto_bpwatch_in_2_valid_0,
  input  [2:0] auto_bpwatch_in_2_action,
  input        auto_bpwatch_in_3_valid_0,
  input  [2:0] auto_bpwatch_in_3_action,
  input        auto_bpwatch_in_4_valid_0,
  input  [2:0] auto_bpwatch_in_4_action,
  input        auto_bpwatch_in_5_valid_0,
  input  [2:0] auto_bpwatch_in_5_action,
  input        auto_bpwatch_in_6_valid_0,
  input  [2:0] auto_bpwatch_in_6_action,
  input        auto_bpwatch_in_7_valid_0,
  input  [2:0] auto_bpwatch_in_7_action,
  input        auto_bpwatch_in_8_valid_0,
  input  [2:0] auto_bpwatch_in_8_action,
  input        auto_bpwatch_in_9_valid_0,
  input  [2:0] auto_bpwatch_in_9_action,
  input        auto_bpwatch_in_10_valid_0,
  input  [2:0] auto_bpwatch_in_10_action,
  input        auto_bpwatch_in_11_valid_0,
  input  [2:0] auto_bpwatch_in_11_action,
  input        auto_bpwatch_in_12_valid_0,
  input  [2:0] auto_bpwatch_in_12_action,
  input        auto_bpwatch_in_13_valid_0,
  input  [2:0] auto_bpwatch_in_13_action,
  input        auto_bpwatch_in_14_valid_0,
  input  [2:0] auto_bpwatch_in_14_action,
  input        auto_bpwatch_in_15_valid_0,
  input  [2:0] auto_bpwatch_in_15_action,
  input        io_teActiveCore,
  input        io_wpControl_mem_0_0,
  input        io_wpControl_mem_0_1,
  input        io_wpControl_mem_0_2,
  input        io_wpControl_mem_0_3,
  input        io_wpControl_mem_0_4,
  input        io_wpControl_mem_0_5,
  input        io_wpControl_mem_0_6,
  input        io_wpControl_mem_0_7,
  input        io_wpControl_mem_0_8,
  input        io_wpControl_mem_0_9,
  input        io_wpControl_mem_0_10,
  input        io_wpControl_mem_0_11,
  input        io_wpControl_mem_0_12,
  input        io_wpControl_mem_0_13,
  input        io_wpControl_mem_0_14,
  input        io_wpControl_mem_0_15,
  input        io_iretire_0,
  input        io_ivalid_0,
  input        io_ingValid,
  output       io_ingTrigger_0_traceon,
  output       io_ingTrigger_0_traceoff,
  output       io_ingTrigger_0_wpsync
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [31:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [31:0] _RAND_59;
  reg [31:0] _RAND_60;
  reg [31:0] _RAND_61;
  reg [31:0] _RAND_62;
  reg [31:0] _RAND_63;
  reg [31:0] _RAND_64;
  reg [31:0] _RAND_65;
  reg [31:0] _RAND_66;
`endif // RANDOMIZE_REG_INIT
  reg  inReg_0; // @[TraceEncoder.scala 784:28]
  reg  inReg_1; // @[TraceEncoder.scala 784:28]
  reg  inReg_2; // @[TraceEncoder.scala 784:28]
  reg  inReg_3; // @[TraceEncoder.scala 784:28]
  reg  inReg_4; // @[TraceEncoder.scala 784:28]
  reg  inReg_5; // @[TraceEncoder.scala 784:28]
  reg  inReg_6; // @[TraceEncoder.scala 784:28]
  reg  inReg_7; // @[TraceEncoder.scala 784:28]
  reg  inReg_8; // @[TraceEncoder.scala 784:28]
  reg  inReg_9; // @[TraceEncoder.scala 784:28]
  reg  inReg_10; // @[TraceEncoder.scala 784:28]
  reg  inReg_11; // @[TraceEncoder.scala 784:28]
  reg  inReg_12; // @[TraceEncoder.scala 784:28]
  reg  inReg_13; // @[TraceEncoder.scala 784:28]
  reg  inReg_14; // @[TraceEncoder.scala 784:28]
  reg  inReg_15; // @[TraceEncoder.scala 784:28]
  reg  bpwatchin_0_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_0_action; // @[Reg.scala 15:16]
  reg  bpwatchin_1_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_1_action; // @[Reg.scala 15:16]
  reg  bpwatchin_2_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_2_action; // @[Reg.scala 15:16]
  reg  bpwatchin_3_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_3_action; // @[Reg.scala 15:16]
  reg  bpwatchin_4_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_4_action; // @[Reg.scala 15:16]
  reg  bpwatchin_5_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_5_action; // @[Reg.scala 15:16]
  reg  bpwatchin_6_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_6_action; // @[Reg.scala 15:16]
  reg  bpwatchin_7_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_7_action; // @[Reg.scala 15:16]
  reg  bpwatchin_8_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_8_action; // @[Reg.scala 15:16]
  reg  bpwatchin_9_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_9_action; // @[Reg.scala 15:16]
  reg  bpwatchin_10_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_10_action; // @[Reg.scala 15:16]
  reg  bpwatchin_11_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_11_action; // @[Reg.scala 15:16]
  reg  bpwatchin_12_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_12_action; // @[Reg.scala 15:16]
  reg  bpwatchin_13_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_13_action; // @[Reg.scala 15:16]
  reg  bpwatchin_14_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_14_action; // @[Reg.scala 15:16]
  reg  bpwatchin_15_valid_0; // @[Reg.scala 15:16]
  reg [2:0] bpwatchin_15_action; // @[Reg.scala 15:16]
  reg [1:0] rstate_0; // @[TraceTrigBlock.scala 81:26]
  reg [1:0] rstate_1; // @[TraceTrigBlock.scala 81:26]
  reg [1:0] rstate_2; // @[TraceTrigBlock.scala 81:26]
  reg [1:0] rstate_3; // @[TraceTrigBlock.scala 81:26]
  reg [1:0] rstate_4; // @[TraceTrigBlock.scala 81:26]
  reg [1:0] rstate_5; // @[TraceTrigBlock.scala 81:26]
  reg [1:0] rstate_6; // @[TraceTrigBlock.scala 81:26]
  reg [1:0] rstate_7; // @[TraceTrigBlock.scala 81:26]
  reg [1:0] rstate_8; // @[TraceTrigBlock.scala 81:26]
  reg [1:0] rstate_9; // @[TraceTrigBlock.scala 81:26]
  reg [1:0] rstate_10; // @[TraceTrigBlock.scala 81:26]
  reg [1:0] rstate_11; // @[TraceTrigBlock.scala 81:26]
  reg [1:0] rstate_12; // @[TraceTrigBlock.scala 81:26]
  reg [1:0] rstate_13; // @[TraceTrigBlock.scala 81:26]
  reg [1:0] rstate_14; // @[TraceTrigBlock.scala 81:26]
  reg [1:0] rstate_15; // @[TraceTrigBlock.scala 81:26]
  wire  _rend_0_T = rstate_0 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_0_T_2 = ~bpwatchin_0_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_0 = _rend_0_T & io_iretire_0 & ~bpwatchin_0_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_0_T = io_ivalid_0 & bpwatchin_0_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_0 = _fire_0_T | rend_0; // @[TraceTrigBlock.scala 88:61]
  wire  _T_3 = bpwatchin_0_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  wire  _rend_1_T = rstate_1 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_1_T_2 = ~bpwatchin_1_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_1 = _rend_1_T & io_iretire_0 & ~bpwatchin_1_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_1_T = io_ivalid_0 & bpwatchin_1_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_1 = _fire_1_T | rend_1; // @[TraceTrigBlock.scala 88:61]
  wire  _T_27 = bpwatchin_1_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  wire  _rend_2_T = rstate_2 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_2_T_2 = ~bpwatchin_2_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_2 = _rend_2_T & io_iretire_0 & ~bpwatchin_2_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_2_T = io_ivalid_0 & bpwatchin_2_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_2 = _fire_2_T | rend_2; // @[TraceTrigBlock.scala 88:61]
  wire  _T_51 = bpwatchin_2_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  wire  _rend_3_T = rstate_3 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_3_T_2 = ~bpwatchin_3_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_3 = _rend_3_T & io_iretire_0 & ~bpwatchin_3_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_3_T = io_ivalid_0 & bpwatchin_3_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_3 = _fire_3_T | rend_3; // @[TraceTrigBlock.scala 88:61]
  wire  _T_75 = bpwatchin_3_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  wire  _rend_4_T = rstate_4 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_4_T_2 = ~bpwatchin_4_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_4 = _rend_4_T & io_iretire_0 & ~bpwatchin_4_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_4_T = io_ivalid_0 & bpwatchin_4_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_4 = _fire_4_T | rend_4; // @[TraceTrigBlock.scala 88:61]
  wire  _T_99 = bpwatchin_4_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  wire  _rend_5_T = rstate_5 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_5_T_2 = ~bpwatchin_5_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_5 = _rend_5_T & io_iretire_0 & ~bpwatchin_5_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_5_T = io_ivalid_0 & bpwatchin_5_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_5 = _fire_5_T | rend_5; // @[TraceTrigBlock.scala 88:61]
  wire  _T_123 = bpwatchin_5_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  wire  _rend_6_T = rstate_6 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_6_T_2 = ~bpwatchin_6_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_6 = _rend_6_T & io_iretire_0 & ~bpwatchin_6_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_6_T = io_ivalid_0 & bpwatchin_6_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_6 = _fire_6_T | rend_6; // @[TraceTrigBlock.scala 88:61]
  wire  _T_147 = bpwatchin_6_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  wire  _rend_7_T = rstate_7 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_7_T_2 = ~bpwatchin_7_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_7 = _rend_7_T & io_iretire_0 & ~bpwatchin_7_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_7_T = io_ivalid_0 & bpwatchin_7_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_7 = _fire_7_T | rend_7; // @[TraceTrigBlock.scala 88:61]
  wire  _T_171 = bpwatchin_7_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  wire  _rend_8_T = rstate_8 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_8_T_2 = ~bpwatchin_8_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_8 = _rend_8_T & io_iretire_0 & ~bpwatchin_8_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_8_T = io_ivalid_0 & bpwatchin_8_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_8 = _fire_8_T | rend_8; // @[TraceTrigBlock.scala 88:61]
  wire  _T_195 = bpwatchin_8_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  wire  _rend_9_T = rstate_9 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_9_T_2 = ~bpwatchin_9_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_9 = _rend_9_T & io_iretire_0 & ~bpwatchin_9_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_9_T = io_ivalid_0 & bpwatchin_9_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_9 = _fire_9_T | rend_9; // @[TraceTrigBlock.scala 88:61]
  wire  _T_219 = bpwatchin_9_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  wire  _rend_10_T = rstate_10 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_10_T_2 = ~bpwatchin_10_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_10 = _rend_10_T & io_iretire_0 & ~bpwatchin_10_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_10_T = io_ivalid_0 & bpwatchin_10_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_10 = _fire_10_T | rend_10; // @[TraceTrigBlock.scala 88:61]
  wire  _T_243 = bpwatchin_10_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  wire  _rend_11_T = rstate_11 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_11_T_2 = ~bpwatchin_11_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_11 = _rend_11_T & io_iretire_0 & ~bpwatchin_11_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_11_T = io_ivalid_0 & bpwatchin_11_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_11 = _fire_11_T | rend_11; // @[TraceTrigBlock.scala 88:61]
  wire  _T_267 = bpwatchin_11_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  wire  _rend_12_T = rstate_12 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_12_T_2 = ~bpwatchin_12_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_12 = _rend_12_T & io_iretire_0 & ~bpwatchin_12_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_12_T = io_ivalid_0 & bpwatchin_12_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_12 = _fire_12_T | rend_12; // @[TraceTrigBlock.scala 88:61]
  wire  _T_291 = bpwatchin_12_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  wire  _rend_13_T = rstate_13 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_13_T_2 = ~bpwatchin_13_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_13 = _rend_13_T & io_iretire_0 & ~bpwatchin_13_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_13_T = io_ivalid_0 & bpwatchin_13_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_13 = _fire_13_T | rend_13; // @[TraceTrigBlock.scala 88:61]
  wire  _T_315 = bpwatchin_13_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  wire  _rend_14_T = rstate_14 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_14_T_2 = ~bpwatchin_14_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_14 = _rend_14_T & io_iretire_0 & ~bpwatchin_14_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_14_T = io_ivalid_0 & bpwatchin_14_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_14 = _fire_14_T | rend_14; // @[TraceTrigBlock.scala 88:61]
  wire  _T_339 = bpwatchin_14_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  wire  _rend_15_T = rstate_15 == 2'h1; // @[TraceTrigBlock.scala 87:92]
  wire  _rend_15_T_2 = ~bpwatchin_15_valid_0; // @[TraceTrigBlock.scala 87:113]
  wire  rend_15 = _rend_15_T & io_iretire_0 & ~bpwatchin_15_valid_0; // @[TraceTrigBlock.scala 87:111]
  wire  _fire_15_T = io_ivalid_0 & bpwatchin_15_valid_0; // @[TraceTrigBlock.scala 88:41]
  wire  fire_15 = _fire_15_T | rend_15; // @[TraceTrigBlock.scala 88:61]
  wire  _T_363 = bpwatchin_15_action == 3'h2; // @[TraceTrigBlock.scala 90:74]
  reg  ingTrigger_0_traceon; // @[TraceTrigBlock.scala 132:29]
  reg  ingTrigger_0_traceoff; // @[TraceTrigBlock.scala 132:29]
  reg  ingTrigger_0_wpsync; // @[TraceTrigBlock.scala 132:29]
  wire [3:0] action_0 = {{1'd0}, bpwatchin_0_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_0 = fire_0 & ~rstate_0[1] & (action_0 == 4'h3 | action_0 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire [3:0] action_1 = {{1'd0}, bpwatchin_1_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_1 = fire_1 & ~rstate_1[1] & (action_1 == 4'h3 | action_1 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire [3:0] action_2 = {{1'd0}, bpwatchin_2_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_2 = fire_2 & ~rstate_2[1] & (action_2 == 4'h3 | action_2 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire [3:0] action_3 = {{1'd0}, bpwatchin_3_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_3 = fire_3 & ~rstate_3[1] & (action_3 == 4'h3 | action_3 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire [3:0] action_4 = {{1'd0}, bpwatchin_4_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_4 = fire_4 & ~rstate_4[1] & (action_4 == 4'h3 | action_4 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire [3:0] action_5 = {{1'd0}, bpwatchin_5_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_5 = fire_5 & ~rstate_5[1] & (action_5 == 4'h3 | action_5 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire [3:0] action_6 = {{1'd0}, bpwatchin_6_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_6 = fire_6 & ~rstate_6[1] & (action_6 == 4'h3 | action_6 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire [3:0] action_7 = {{1'd0}, bpwatchin_7_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_7 = fire_7 & ~rstate_7[1] & (action_7 == 4'h3 | action_7 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire [3:0] action_8 = {{1'd0}, bpwatchin_8_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_8 = fire_8 & ~rstate_8[1] & (action_8 == 4'h3 | action_8 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire [3:0] action_9 = {{1'd0}, bpwatchin_9_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_9 = fire_9 & ~rstate_9[1] & (action_9 == 4'h3 | action_9 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire [3:0] action_10 = {{1'd0}, bpwatchin_10_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_10 = fire_10 & ~rstate_10[1] & (action_10 == 4'h3 | action_10 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire [3:0] action_11 = {{1'd0}, bpwatchin_11_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_11 = fire_11 & ~rstate_11[1] & (action_11 == 4'h3 | action_11 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire [3:0] action_12 = {{1'd0}, bpwatchin_12_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_12 = fire_12 & ~rstate_12[1] & (action_12 == 4'h3 | action_12 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire [3:0] action_13 = {{1'd0}, bpwatchin_13_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_13 = fire_13 & ~rstate_13[1] & (action_13 == 4'h3 | action_13 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire [3:0] action_14 = {{1'd0}, bpwatchin_14_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_14 = fire_14 & ~rstate_14[1] & (action_14 == 4'h3 | action_14 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire [3:0] action_15 = {{1'd0}, bpwatchin_15_action}; // @[TraceTrigBlock.scala 80:26 TraceTrigBlock.scala 89:22]
  wire  s_15 = fire_15 & ~rstate_15[1] & (action_15 == 4'h3 | action_15 == 4'h2); // @[TraceTrigBlock.scala 142:88]
  wire  _toff_T_2 = action_0[0] | rend_0; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_5 = action_1[0] | rend_1; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_8 = action_2[0] | rend_2; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_11 = action_3[0] | rend_3; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_14 = action_4[0] | rend_4; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_17 = action_5[0] | rend_5; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_20 = action_6[0] | rend_6; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_23 = action_7[0] | rend_7; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_26 = action_8[0] | rend_8; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_29 = action_9[0] | rend_9; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_32 = action_10[0] | rend_10; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_35 = action_11[0] | rend_11; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_38 = action_12[0] | rend_12; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_41 = action_13[0] | rend_13; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_44 = action_14[0] | rend_14; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_47 = action_15[0] | rend_15; // @[TraceTrigBlock.scala 143:89]
  wire  _toff_T_48 = s_14 ? _toff_T_44 : _toff_T_47; // @[Mux.scala 47:69]
  wire  _toff_T_49 = s_13 ? _toff_T_41 : _toff_T_48; // @[Mux.scala 47:69]
  wire  _toff_T_50 = s_12 ? _toff_T_38 : _toff_T_49; // @[Mux.scala 47:69]
  wire  _toff_T_51 = s_11 ? _toff_T_35 : _toff_T_50; // @[Mux.scala 47:69]
  wire  _toff_T_52 = s_10 ? _toff_T_32 : _toff_T_51; // @[Mux.scala 47:69]
  wire  _toff_T_53 = s_9 ? _toff_T_29 : _toff_T_52; // @[Mux.scala 47:69]
  wire  _toff_T_54 = s_8 ? _toff_T_26 : _toff_T_53; // @[Mux.scala 47:69]
  wire  _toff_T_55 = s_7 ? _toff_T_23 : _toff_T_54; // @[Mux.scala 47:69]
  wire  _toff_T_56 = s_6 ? _toff_T_20 : _toff_T_55; // @[Mux.scala 47:69]
  wire  _toff_T_57 = s_5 ? _toff_T_17 : _toff_T_56; // @[Mux.scala 47:69]
  wire  _toff_T_58 = s_4 ? _toff_T_14 : _toff_T_57; // @[Mux.scala 47:69]
  wire  _toff_T_59 = s_3 ? _toff_T_11 : _toff_T_58; // @[Mux.scala 47:69]
  wire  _toff_T_60 = s_2 ? _toff_T_8 : _toff_T_59; // @[Mux.scala 47:69]
  wire  _toff_T_61 = s_1 ? _toff_T_5 : _toff_T_60; // @[Mux.scala 47:69]
  wire  toff = s_0 ? _toff_T_2 : _toff_T_61; // @[Mux.scala 47:69]
  wire  _ingTrigger_0_traceon_T = s_0 | s_1; // @[package.scala 72:59]
  wire  _ingTrigger_0_traceon_T_1 = s_0 | s_1 | s_2; // @[package.scala 72:59]
  wire  _ingTrigger_0_traceon_T_2 = s_0 | s_1 | s_2 | s_3; // @[package.scala 72:59]
  wire  _ingTrigger_0_traceon_T_3 = s_0 | s_1 | s_2 | s_3 | s_4; // @[package.scala 72:59]
  wire  _ingTrigger_0_traceon_T_4 = s_0 | s_1 | s_2 | s_3 | s_4 | s_5; // @[package.scala 72:59]
  wire  _ingTrigger_0_traceon_T_5 = s_0 | s_1 | s_2 | s_3 | s_4 | s_5 | s_6; // @[package.scala 72:59]
  wire  _ingTrigger_0_traceon_T_6 = s_0 | s_1 | s_2 | s_3 | s_4 | s_5 | s_6 | s_7; // @[package.scala 72:59]
  wire  _ingTrigger_0_traceon_T_7 = s_0 | s_1 | s_2 | s_3 | s_4 | s_5 | s_6 | s_7 | s_8; // @[package.scala 72:59]
  wire  _ingTrigger_0_traceon_T_8 = s_0 | s_1 | s_2 | s_3 | s_4 | s_5 | s_6 | s_7 | s_8 | s_9; // @[package.scala 72:59]
  wire  _ingTrigger_0_traceon_T_9 = s_0 | s_1 | s_2 | s_3 | s_4 | s_5 | s_6 | s_7 | s_8 | s_9 | s_10; // @[package.scala 72:59]
  wire  _ingTrigger_0_traceon_T_10 = s_0 | s_1 | s_2 | s_3 | s_4 | s_5 | s_6 | s_7 | s_8 | s_9 | s_10 | s_11; // @[package.scala 72:59]
  wire  _ingTrigger_0_traceon_T_11 = s_0 | s_1 | s_2 | s_3 | s_4 | s_5 | s_6 | s_7 | s_8 | s_9 | s_10 | s_11 | s_12; // @[package.scala 72:59]
  wire  _ingTrigger_0_traceon_T_12 = s_0 | s_1 | s_2 | s_3 | s_4 | s_5 | s_6 | s_7 | s_8 | s_9 | s_10 | s_11 | s_12 |
    s_13; // @[package.scala 72:59]
  wire  _ingTrigger_0_traceon_T_13 = s_0 | s_1 | s_2 | s_3 | s_4 | s_5 | s_6 | s_7 | s_8 | s_9 | s_10 | s_11 | s_12 |
    s_13 | s_14; // @[package.scala 72:59]
  wire  _ingTrigger_0_traceon_T_14 = s_0 | s_1 | s_2 | s_3 | s_4 | s_5 | s_6 | s_7 | s_8 | s_9 | s_10 | s_11 | s_12 |
    s_13 | s_14 | s_15; // @[package.scala 72:59]
  wire  _ingTrigger_0_wpsync_T_2 = fire_0 & action_0 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_5 = fire_1 & action_1 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_8 = fire_2 & action_2 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_11 = fire_3 & action_3 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_14 = fire_4 & action_4 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_17 = fire_5 & action_5 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_20 = fire_6 & action_6 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_23 = fire_7 & action_7 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_26 = fire_8 & action_8 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_29 = fire_9 & action_9 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_32 = fire_10 & action_10 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_35 = fire_11 & action_11 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_38 = fire_12 & action_12 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_41 = fire_13 & action_13 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_44 = fire_14 & action_14 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_47 = fire_15 & action_15 == 4'h4; // @[TraceTrigBlock.scala 146:98]
  wire  _ingTrigger_0_wpsync_T_62 = _ingTrigger_0_wpsync_T_2 | _ingTrigger_0_wpsync_T_5 | _ingTrigger_0_wpsync_T_8 |
    _ingTrigger_0_wpsync_T_11 | _ingTrigger_0_wpsync_T_14 | _ingTrigger_0_wpsync_T_17 | _ingTrigger_0_wpsync_T_20 |
    _ingTrigger_0_wpsync_T_23 | _ingTrigger_0_wpsync_T_26 | _ingTrigger_0_wpsync_T_29 | _ingTrigger_0_wpsync_T_32 |
    _ingTrigger_0_wpsync_T_35 | _ingTrigger_0_wpsync_T_38 | _ingTrigger_0_wpsync_T_41 | _ingTrigger_0_wpsync_T_44 |
    _ingTrigger_0_wpsync_T_47; // @[package.scala 72:59]
  wire  _T_521 = io_iretire_0 & _rend_0_T_2; // @[package.scala 65:72]
  wire  lowest = ~s_0; // @[TraceTrigBlock.scala 156:49]
  wire  _T_534 = io_iretire_0 & _rend_1_T_2; // @[package.scala 65:72]
  wire  lowest_1 = ~_ingTrigger_0_traceon_T; // @[TraceTrigBlock.scala 156:49]
  wire  _T_547 = io_iretire_0 & _rend_2_T_2; // @[package.scala 65:72]
  wire  lowest_2 = ~_ingTrigger_0_traceon_T_1; // @[TraceTrigBlock.scala 156:49]
  wire  _T_560 = io_iretire_0 & _rend_3_T_2; // @[package.scala 65:72]
  wire  lowest_3 = ~_ingTrigger_0_traceon_T_2; // @[TraceTrigBlock.scala 156:49]
  wire  _T_573 = io_iretire_0 & _rend_4_T_2; // @[package.scala 65:72]
  wire  lowest_4 = ~_ingTrigger_0_traceon_T_3; // @[TraceTrigBlock.scala 156:49]
  wire  _T_586 = io_iretire_0 & _rend_5_T_2; // @[package.scala 65:72]
  wire  lowest_5 = ~_ingTrigger_0_traceon_T_4; // @[TraceTrigBlock.scala 156:49]
  wire  _T_599 = io_iretire_0 & _rend_6_T_2; // @[package.scala 65:72]
  wire  lowest_6 = ~_ingTrigger_0_traceon_T_5; // @[TraceTrigBlock.scala 156:49]
  wire  _T_612 = io_iretire_0 & _rend_7_T_2; // @[package.scala 65:72]
  wire  lowest_7 = ~_ingTrigger_0_traceon_T_6; // @[TraceTrigBlock.scala 156:49]
  wire  _T_625 = io_iretire_0 & _rend_8_T_2; // @[package.scala 65:72]
  wire  lowest_8 = ~_ingTrigger_0_traceon_T_7; // @[TraceTrigBlock.scala 156:49]
  wire  _T_638 = io_iretire_0 & _rend_9_T_2; // @[package.scala 65:72]
  wire  lowest_9 = ~_ingTrigger_0_traceon_T_8; // @[TraceTrigBlock.scala 156:49]
  wire  _T_651 = io_iretire_0 & _rend_10_T_2; // @[package.scala 65:72]
  wire  lowest_10 = ~_ingTrigger_0_traceon_T_9; // @[TraceTrigBlock.scala 156:49]
  wire  _T_664 = io_iretire_0 & _rend_11_T_2; // @[package.scala 65:72]
  wire  lowest_11 = ~_ingTrigger_0_traceon_T_10; // @[TraceTrigBlock.scala 156:49]
  wire  _T_677 = io_iretire_0 & _rend_12_T_2; // @[package.scala 65:72]
  wire  lowest_12 = ~_ingTrigger_0_traceon_T_11; // @[TraceTrigBlock.scala 156:49]
  wire  _T_690 = io_iretire_0 & _rend_13_T_2; // @[package.scala 65:72]
  wire  lowest_13 = ~_ingTrigger_0_traceon_T_12; // @[TraceTrigBlock.scala 156:49]
  wire  _T_703 = io_iretire_0 & _rend_14_T_2; // @[package.scala 65:72]
  wire  lowest_14 = ~_ingTrigger_0_traceon_T_13; // @[TraceTrigBlock.scala 156:49]
  wire  _T_716 = io_iretire_0 & _rend_15_T_2; // @[package.scala 65:72]
  wire  _T_721 = ~io_teActiveCore; // @[TraceTrigBlock.scala 163:15]
  assign io_ingTrigger_0_traceon = ingTrigger_0_traceon; // @[TraceTrigBlock.scala 164:23]
  assign io_ingTrigger_0_traceoff = ingTrigger_0_traceoff; // @[TraceTrigBlock.scala 164:23]
  assign io_ingTrigger_0_wpsync = ingTrigger_0_wpsync; // @[TraceTrigBlock.scala 164:23]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_0 <= 1'h0;
    end else begin
      inReg_0 <= io_wpControl_mem_0_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_1 <= 1'h0;
    end else begin
      inReg_1 <= io_wpControl_mem_0_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_2 <= 1'h0;
    end else begin
      inReg_2 <= io_wpControl_mem_0_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_3 <= 1'h0;
    end else begin
      inReg_3 <= io_wpControl_mem_0_3;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_4 <= 1'h0;
    end else begin
      inReg_4 <= io_wpControl_mem_0_4;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_5 <= 1'h0;
    end else begin
      inReg_5 <= io_wpControl_mem_0_5;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_6 <= 1'h0;
    end else begin
      inReg_6 <= io_wpControl_mem_0_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_7 <= 1'h0;
    end else begin
      inReg_7 <= io_wpControl_mem_0_7;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_8 <= 1'h0;
    end else begin
      inReg_8 <= io_wpControl_mem_0_8;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_9 <= 1'h0;
    end else begin
      inReg_9 <= io_wpControl_mem_0_9;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_10 <= 1'h0;
    end else begin
      inReg_10 <= io_wpControl_mem_0_10;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_11 <= 1'h0;
    end else begin
      inReg_11 <= io_wpControl_mem_0_11;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_12 <= 1'h0;
    end else begin
      inReg_12 <= io_wpControl_mem_0_12;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_13 <= 1'h0;
    end else begin
      inReg_13 <= io_wpControl_mem_0_13;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_14 <= 1'h0;
    end else begin
      inReg_14 <= io_wpControl_mem_0_14;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      inReg_15 <= 1'h0;
    end else begin
      inReg_15 <= io_wpControl_mem_0_15;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_0_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_0_valid_0 <= auto_bpwatch_in_0_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_0_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_0_action <= auto_bpwatch_in_0_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_1_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_1_valid_0 <= auto_bpwatch_in_1_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_1_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_1_action <= auto_bpwatch_in_1_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_2_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_2_valid_0 <= auto_bpwatch_in_2_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_2_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_2_action <= auto_bpwatch_in_2_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_3_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_3_valid_0 <= auto_bpwatch_in_3_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_3_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_3_action <= auto_bpwatch_in_3_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_4_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_4_valid_0 <= auto_bpwatch_in_4_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_4_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_4_action <= auto_bpwatch_in_4_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_5_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_5_valid_0 <= auto_bpwatch_in_5_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_5_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_5_action <= auto_bpwatch_in_5_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_6_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_6_valid_0 <= auto_bpwatch_in_6_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_6_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_6_action <= auto_bpwatch_in_6_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_7_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_7_valid_0 <= auto_bpwatch_in_7_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_7_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_7_action <= auto_bpwatch_in_7_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_8_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_8_valid_0 <= auto_bpwatch_in_8_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_8_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_8_action <= auto_bpwatch_in_8_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_9_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_9_valid_0 <= auto_bpwatch_in_9_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_9_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_9_action <= auto_bpwatch_in_9_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_10_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_10_valid_0 <= auto_bpwatch_in_10_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_10_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_10_action <= auto_bpwatch_in_10_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_11_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_11_valid_0 <= auto_bpwatch_in_11_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_11_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_11_action <= auto_bpwatch_in_11_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_12_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_12_valid_0 <= auto_bpwatch_in_12_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_12_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_12_action <= auto_bpwatch_in_12_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_13_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_13_valid_0 <= auto_bpwatch_in_13_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_13_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_13_action <= auto_bpwatch_in_13_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_14_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_14_valid_0 <= auto_bpwatch_in_14_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_14_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_14_action <= auto_bpwatch_in_14_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_15_valid_0 <= 1'h0;
    end else if (io_ingValid) begin
      bpwatchin_15_valid_0 <= auto_bpwatch_in_15_valid_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bpwatchin_15_action <= 3'h0;
    end else if (io_ingValid) begin
      bpwatchin_15_action <= auto_bpwatch_in_15_action;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_0 <= 2'h0;
    end else if (_T_721) begin
      rstate_0 <= 2'h0;
    end else if (rstate_0 == 2'h0 & inReg_0 & _T_3 & s_0) begin
      rstate_0 <= 2'h1;
    end else if (|rstate_0 & _T_521) begin
      rstate_0 <= 2'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_1 <= 2'h0;
    end else if (_T_721) begin
      rstate_1 <= 2'h0;
    end else if (rstate_1 == 2'h0 & inReg_1 & _T_27 & s_1 & lowest) begin
      rstate_1 <= 2'h1;
    end else if (|rstate_1 & _T_534) begin
      rstate_1 <= 2'h0;
    end else if (_rend_1_T & ~lowest) begin
      rstate_1 <= 2'h2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_2 <= 2'h0;
    end else if (_T_721) begin
      rstate_2 <= 2'h0;
    end else if (rstate_2 == 2'h0 & inReg_2 & _T_51 & s_2 & lowest_1) begin
      rstate_2 <= 2'h1;
    end else if (|rstate_2 & _T_547) begin
      rstate_2 <= 2'h0;
    end else if (_rend_2_T & ~lowest_1) begin
      rstate_2 <= 2'h2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_3 <= 2'h0;
    end else if (_T_721) begin
      rstate_3 <= 2'h0;
    end else if (rstate_3 == 2'h0 & inReg_3 & _T_75 & s_3 & lowest_2) begin
      rstate_3 <= 2'h1;
    end else if (|rstate_3 & _T_560) begin
      rstate_3 <= 2'h0;
    end else if (_rend_3_T & ~lowest_2) begin
      rstate_3 <= 2'h2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_4 <= 2'h0;
    end else if (_T_721) begin
      rstate_4 <= 2'h0;
    end else if (rstate_4 == 2'h0 & inReg_4 & _T_99 & s_4 & lowest_3) begin
      rstate_4 <= 2'h1;
    end else if (|rstate_4 & _T_573) begin
      rstate_4 <= 2'h0;
    end else if (_rend_4_T & ~lowest_3) begin
      rstate_4 <= 2'h2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_5 <= 2'h0;
    end else if (_T_721) begin
      rstate_5 <= 2'h0;
    end else if (rstate_5 == 2'h0 & inReg_5 & _T_123 & s_5 & lowest_4) begin
      rstate_5 <= 2'h1;
    end else if (|rstate_5 & _T_586) begin
      rstate_5 <= 2'h0;
    end else if (_rend_5_T & ~lowest_4) begin
      rstate_5 <= 2'h2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_6 <= 2'h0;
    end else if (_T_721) begin
      rstate_6 <= 2'h0;
    end else if (rstate_6 == 2'h0 & inReg_6 & _T_147 & s_6 & lowest_5) begin
      rstate_6 <= 2'h1;
    end else if (|rstate_6 & _T_599) begin
      rstate_6 <= 2'h0;
    end else if (_rend_6_T & ~lowest_5) begin
      rstate_6 <= 2'h2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_7 <= 2'h0;
    end else if (_T_721) begin
      rstate_7 <= 2'h0;
    end else if (rstate_7 == 2'h0 & inReg_7 & _T_171 & s_7 & lowest_6) begin
      rstate_7 <= 2'h1;
    end else if (|rstate_7 & _T_612) begin
      rstate_7 <= 2'h0;
    end else if (_rend_7_T & ~lowest_6) begin
      rstate_7 <= 2'h2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_8 <= 2'h0;
    end else if (_T_721) begin
      rstate_8 <= 2'h0;
    end else if (rstate_8 == 2'h0 & inReg_8 & _T_195 & s_8 & lowest_7) begin
      rstate_8 <= 2'h1;
    end else if (|rstate_8 & _T_625) begin
      rstate_8 <= 2'h0;
    end else if (_rend_8_T & ~lowest_7) begin
      rstate_8 <= 2'h2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_9 <= 2'h0;
    end else if (_T_721) begin
      rstate_9 <= 2'h0;
    end else if (rstate_9 == 2'h0 & inReg_9 & _T_219 & s_9 & lowest_8) begin
      rstate_9 <= 2'h1;
    end else if (|rstate_9 & _T_638) begin
      rstate_9 <= 2'h0;
    end else if (_rend_9_T & ~lowest_8) begin
      rstate_9 <= 2'h2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_10 <= 2'h0;
    end else if (_T_721) begin
      rstate_10 <= 2'h0;
    end else if (rstate_10 == 2'h0 & inReg_10 & _T_243 & s_10 & lowest_9) begin
      rstate_10 <= 2'h1;
    end else if (|rstate_10 & _T_651) begin
      rstate_10 <= 2'h0;
    end else if (_rend_10_T & ~lowest_9) begin
      rstate_10 <= 2'h2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_11 <= 2'h0;
    end else if (_T_721) begin
      rstate_11 <= 2'h0;
    end else if (rstate_11 == 2'h0 & inReg_11 & _T_267 & s_11 & lowest_10) begin
      rstate_11 <= 2'h1;
    end else if (|rstate_11 & _T_664) begin
      rstate_11 <= 2'h0;
    end else if (_rend_11_T & ~lowest_10) begin
      rstate_11 <= 2'h2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_12 <= 2'h0;
    end else if (_T_721) begin
      rstate_12 <= 2'h0;
    end else if (rstate_12 == 2'h0 & inReg_12 & _T_291 & s_12 & lowest_11) begin
      rstate_12 <= 2'h1;
    end else if (|rstate_12 & _T_677) begin
      rstate_12 <= 2'h0;
    end else if (_rend_12_T & ~lowest_11) begin
      rstate_12 <= 2'h2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_13 <= 2'h0;
    end else if (_T_721) begin
      rstate_13 <= 2'h0;
    end else if (rstate_13 == 2'h0 & inReg_13 & _T_315 & s_13 & lowest_12) begin
      rstate_13 <= 2'h1;
    end else if (|rstate_13 & _T_690) begin
      rstate_13 <= 2'h0;
    end else if (_rend_13_T & ~lowest_12) begin
      rstate_13 <= 2'h2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_14 <= 2'h0;
    end else if (_T_721) begin
      rstate_14 <= 2'h0;
    end else if (rstate_14 == 2'h0 & inReg_14 & _T_339 & s_14 & lowest_13) begin
      rstate_14 <= 2'h1;
    end else if (|rstate_14 & _T_703) begin
      rstate_14 <= 2'h0;
    end else if (_rend_14_T & ~lowest_13) begin
      rstate_14 <= 2'h2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      rstate_15 <= 2'h0;
    end else if (_T_721) begin
      rstate_15 <= 2'h0;
    end else if (rstate_15 == 2'h0 & inReg_15 & _T_363 & s_15 & lowest_14) begin
      rstate_15 <= 2'h1;
    end else if (|rstate_15 & _T_716) begin
      rstate_15 <= 2'h0;
    end else if (_rend_15_T & ~lowest_14) begin
      rstate_15 <= 2'h2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ingTrigger_0_traceon <= 1'h0;
    end else if (~io_teActiveCore) begin
      ingTrigger_0_traceon <= 1'h0;
    end else begin
      ingTrigger_0_traceon <= _ingTrigger_0_traceon_T_14 & ~toff;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ingTrigger_0_traceoff <= 1'h0;
    end else if (~io_teActiveCore) begin
      ingTrigger_0_traceoff <= 1'h0;
    end else begin
      ingTrigger_0_traceoff <= _ingTrigger_0_traceon_T_14 & toff;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ingTrigger_0_wpsync <= 1'h0;
    end else if (~io_teActiveCore) begin
      ingTrigger_0_wpsync <= 1'h0;
    end else begin
      ingTrigger_0_wpsync <= _ingTrigger_0_wpsync_T_62;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  inReg_0 = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  inReg_1 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  inReg_2 = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  inReg_3 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  inReg_4 = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  inReg_5 = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  inReg_6 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  inReg_7 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  inReg_8 = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  inReg_9 = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  inReg_10 = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  inReg_11 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  inReg_12 = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  inReg_13 = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  inReg_14 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  inReg_15 = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  bpwatchin_0_valid_0 = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  bpwatchin_0_action = _RAND_17[2:0];
  _RAND_18 = {1{`RANDOM}};
  bpwatchin_1_valid_0 = _RAND_18[0:0];
  _RAND_19 = {1{`RANDOM}};
  bpwatchin_1_action = _RAND_19[2:0];
  _RAND_20 = {1{`RANDOM}};
  bpwatchin_2_valid_0 = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  bpwatchin_2_action = _RAND_21[2:0];
  _RAND_22 = {1{`RANDOM}};
  bpwatchin_3_valid_0 = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  bpwatchin_3_action = _RAND_23[2:0];
  _RAND_24 = {1{`RANDOM}};
  bpwatchin_4_valid_0 = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  bpwatchin_4_action = _RAND_25[2:0];
  _RAND_26 = {1{`RANDOM}};
  bpwatchin_5_valid_0 = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  bpwatchin_5_action = _RAND_27[2:0];
  _RAND_28 = {1{`RANDOM}};
  bpwatchin_6_valid_0 = _RAND_28[0:0];
  _RAND_29 = {1{`RANDOM}};
  bpwatchin_6_action = _RAND_29[2:0];
  _RAND_30 = {1{`RANDOM}};
  bpwatchin_7_valid_0 = _RAND_30[0:0];
  _RAND_31 = {1{`RANDOM}};
  bpwatchin_7_action = _RAND_31[2:0];
  _RAND_32 = {1{`RANDOM}};
  bpwatchin_8_valid_0 = _RAND_32[0:0];
  _RAND_33 = {1{`RANDOM}};
  bpwatchin_8_action = _RAND_33[2:0];
  _RAND_34 = {1{`RANDOM}};
  bpwatchin_9_valid_0 = _RAND_34[0:0];
  _RAND_35 = {1{`RANDOM}};
  bpwatchin_9_action = _RAND_35[2:0];
  _RAND_36 = {1{`RANDOM}};
  bpwatchin_10_valid_0 = _RAND_36[0:0];
  _RAND_37 = {1{`RANDOM}};
  bpwatchin_10_action = _RAND_37[2:0];
  _RAND_38 = {1{`RANDOM}};
  bpwatchin_11_valid_0 = _RAND_38[0:0];
  _RAND_39 = {1{`RANDOM}};
  bpwatchin_11_action = _RAND_39[2:0];
  _RAND_40 = {1{`RANDOM}};
  bpwatchin_12_valid_0 = _RAND_40[0:0];
  _RAND_41 = {1{`RANDOM}};
  bpwatchin_12_action = _RAND_41[2:0];
  _RAND_42 = {1{`RANDOM}};
  bpwatchin_13_valid_0 = _RAND_42[0:0];
  _RAND_43 = {1{`RANDOM}};
  bpwatchin_13_action = _RAND_43[2:0];
  _RAND_44 = {1{`RANDOM}};
  bpwatchin_14_valid_0 = _RAND_44[0:0];
  _RAND_45 = {1{`RANDOM}};
  bpwatchin_14_action = _RAND_45[2:0];
  _RAND_46 = {1{`RANDOM}};
  bpwatchin_15_valid_0 = _RAND_46[0:0];
  _RAND_47 = {1{`RANDOM}};
  bpwatchin_15_action = _RAND_47[2:0];
  _RAND_48 = {1{`RANDOM}};
  rstate_0 = _RAND_48[1:0];
  _RAND_49 = {1{`RANDOM}};
  rstate_1 = _RAND_49[1:0];
  _RAND_50 = {1{`RANDOM}};
  rstate_2 = _RAND_50[1:0];
  _RAND_51 = {1{`RANDOM}};
  rstate_3 = _RAND_51[1:0];
  _RAND_52 = {1{`RANDOM}};
  rstate_4 = _RAND_52[1:0];
  _RAND_53 = {1{`RANDOM}};
  rstate_5 = _RAND_53[1:0];
  _RAND_54 = {1{`RANDOM}};
  rstate_6 = _RAND_54[1:0];
  _RAND_55 = {1{`RANDOM}};
  rstate_7 = _RAND_55[1:0];
  _RAND_56 = {1{`RANDOM}};
  rstate_8 = _RAND_56[1:0];
  _RAND_57 = {1{`RANDOM}};
  rstate_9 = _RAND_57[1:0];
  _RAND_58 = {1{`RANDOM}};
  rstate_10 = _RAND_58[1:0];
  _RAND_59 = {1{`RANDOM}};
  rstate_11 = _RAND_59[1:0];
  _RAND_60 = {1{`RANDOM}};
  rstate_12 = _RAND_60[1:0];
  _RAND_61 = {1{`RANDOM}};
  rstate_13 = _RAND_61[1:0];
  _RAND_62 = {1{`RANDOM}};
  rstate_14 = _RAND_62[1:0];
  _RAND_63 = {1{`RANDOM}};
  rstate_15 = _RAND_63[1:0];
  _RAND_64 = {1{`RANDOM}};
  ingTrigger_0_traceon = _RAND_64[0:0];
  _RAND_65 = {1{`RANDOM}};
  ingTrigger_0_traceoff = _RAND_65[0:0];
  _RAND_66 = {1{`RANDOM}};
  ingTrigger_0_wpsync = _RAND_66[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    inReg_0 = 1'h0;
  end
  if (reset) begin
    inReg_1 = 1'h0;
  end
  if (reset) begin
    inReg_2 = 1'h0;
  end
  if (reset) begin
    inReg_3 = 1'h0;
  end
  if (reset) begin
    inReg_4 = 1'h0;
  end
  if (reset) begin
    inReg_5 = 1'h0;
  end
  if (reset) begin
    inReg_6 = 1'h0;
  end
  if (reset) begin
    inReg_7 = 1'h0;
  end
  if (reset) begin
    inReg_8 = 1'h0;
  end
  if (reset) begin
    inReg_9 = 1'h0;
  end
  if (reset) begin
    inReg_10 = 1'h0;
  end
  if (reset) begin
    inReg_11 = 1'h0;
  end
  if (reset) begin
    inReg_12 = 1'h0;
  end
  if (reset) begin
    inReg_13 = 1'h0;
  end
  if (reset) begin
    inReg_14 = 1'h0;
  end
  if (reset) begin
    inReg_15 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_0_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_0_action = 3'h0;
  end
  if (rf_reset) begin
    bpwatchin_1_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_1_action = 3'h0;
  end
  if (rf_reset) begin
    bpwatchin_2_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_2_action = 3'h0;
  end
  if (rf_reset) begin
    bpwatchin_3_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_3_action = 3'h0;
  end
  if (rf_reset) begin
    bpwatchin_4_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_4_action = 3'h0;
  end
  if (rf_reset) begin
    bpwatchin_5_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_5_action = 3'h0;
  end
  if (rf_reset) begin
    bpwatchin_6_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_6_action = 3'h0;
  end
  if (rf_reset) begin
    bpwatchin_7_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_7_action = 3'h0;
  end
  if (rf_reset) begin
    bpwatchin_8_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_8_action = 3'h0;
  end
  if (rf_reset) begin
    bpwatchin_9_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_9_action = 3'h0;
  end
  if (rf_reset) begin
    bpwatchin_10_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_10_action = 3'h0;
  end
  if (rf_reset) begin
    bpwatchin_11_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_11_action = 3'h0;
  end
  if (rf_reset) begin
    bpwatchin_12_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_12_action = 3'h0;
  end
  if (rf_reset) begin
    bpwatchin_13_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_13_action = 3'h0;
  end
  if (rf_reset) begin
    bpwatchin_14_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_14_action = 3'h0;
  end
  if (rf_reset) begin
    bpwatchin_15_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    bpwatchin_15_action = 3'h0;
  end
  if (rf_reset) begin
    rstate_0 = 2'h0;
  end
  if (rf_reset) begin
    rstate_1 = 2'h0;
  end
  if (rf_reset) begin
    rstate_2 = 2'h0;
  end
  if (rf_reset) begin
    rstate_3 = 2'h0;
  end
  if (rf_reset) begin
    rstate_4 = 2'h0;
  end
  if (rf_reset) begin
    rstate_5 = 2'h0;
  end
  if (rf_reset) begin
    rstate_6 = 2'h0;
  end
  if (rf_reset) begin
    rstate_7 = 2'h0;
  end
  if (rf_reset) begin
    rstate_8 = 2'h0;
  end
  if (rf_reset) begin
    rstate_9 = 2'h0;
  end
  if (rf_reset) begin
    rstate_10 = 2'h0;
  end
  if (rf_reset) begin
    rstate_11 = 2'h0;
  end
  if (rf_reset) begin
    rstate_12 = 2'h0;
  end
  if (rf_reset) begin
    rstate_13 = 2'h0;
  end
  if (rf_reset) begin
    rstate_14 = 2'h0;
  end
  if (rf_reset) begin
    rstate_15 = 2'h0;
  end
  if (rf_reset) begin
    ingTrigger_0_traceon = 1'h0;
  end
  if (rf_reset) begin
    ingTrigger_0_traceoff = 1'h0;
  end
  if (rf_reset) begin
    ingTrigger_0_wpsync = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
