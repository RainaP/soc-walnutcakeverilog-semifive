//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLXbar(
  input          rf_reset,
  input          clock,
  input          reset,
  output         auto_in_2_a_ready,
  input          auto_in_2_a_valid,
  input  [2:0]   auto_in_2_a_bits_opcode,
  input  [2:0]   auto_in_2_a_bits_param,
  input  [3:0]   auto_in_2_a_bits_size,
  input  [4:0]   auto_in_2_a_bits_source,
  input  [36:0]  auto_in_2_a_bits_address,
  input          auto_in_2_a_bits_user_amba_prot_bufferable,
  input          auto_in_2_a_bits_user_amba_prot_modifiable,
  input          auto_in_2_a_bits_user_amba_prot_readalloc,
  input          auto_in_2_a_bits_user_amba_prot_writealloc,
  input          auto_in_2_a_bits_user_amba_prot_privileged,
  input          auto_in_2_a_bits_user_amba_prot_secure,
  input          auto_in_2_a_bits_user_amba_prot_fetch,
  input  [15:0]  auto_in_2_a_bits_mask,
  input  [127:0] auto_in_2_a_bits_data,
  input          auto_in_2_a_bits_corrupt,
  input          auto_in_2_b_ready,
  output         auto_in_2_b_valid,
  output [2:0]   auto_in_2_b_bits_opcode,
  output [1:0]   auto_in_2_b_bits_param,
  output [3:0]   auto_in_2_b_bits_size,
  output [4:0]   auto_in_2_b_bits_source,
  output [36:0]  auto_in_2_b_bits_address,
  output [15:0]  auto_in_2_b_bits_mask,
  output [127:0] auto_in_2_b_bits_data,
  output         auto_in_2_b_bits_corrupt,
  output         auto_in_2_c_ready,
  input          auto_in_2_c_valid,
  input  [2:0]   auto_in_2_c_bits_opcode,
  input  [2:0]   auto_in_2_c_bits_param,
  input  [3:0]   auto_in_2_c_bits_size,
  input  [4:0]   auto_in_2_c_bits_source,
  input  [36:0]  auto_in_2_c_bits_address,
  input  [127:0] auto_in_2_c_bits_data,
  input          auto_in_2_c_bits_corrupt,
  input          auto_in_2_d_ready,
  output         auto_in_2_d_valid,
  output [2:0]   auto_in_2_d_bits_opcode,
  output [1:0]   auto_in_2_d_bits_param,
  output [3:0]   auto_in_2_d_bits_size,
  output [4:0]   auto_in_2_d_bits_source,
  output [4:0]   auto_in_2_d_bits_sink,
  output         auto_in_2_d_bits_denied,
  output [127:0] auto_in_2_d_bits_data,
  output         auto_in_2_d_bits_corrupt,
  output         auto_in_2_e_ready,
  input          auto_in_2_e_valid,
  input  [4:0]   auto_in_2_e_bits_sink,
  output         auto_in_1_a_ready,
  input          auto_in_1_a_valid,
  input  [2:0]   auto_in_1_a_bits_opcode,
  input  [2:0]   auto_in_1_a_bits_param,
  input  [3:0]   auto_in_1_a_bits_size,
  input  [4:0]   auto_in_1_a_bits_source,
  input  [36:0]  auto_in_1_a_bits_address,
  input          auto_in_1_a_bits_user_amba_prot_bufferable,
  input          auto_in_1_a_bits_user_amba_prot_modifiable,
  input          auto_in_1_a_bits_user_amba_prot_readalloc,
  input          auto_in_1_a_bits_user_amba_prot_writealloc,
  input          auto_in_1_a_bits_user_amba_prot_privileged,
  input          auto_in_1_a_bits_user_amba_prot_secure,
  input          auto_in_1_a_bits_user_amba_prot_fetch,
  input  [15:0]  auto_in_1_a_bits_mask,
  input  [127:0] auto_in_1_a_bits_data,
  input          auto_in_1_a_bits_corrupt,
  input          auto_in_1_b_ready,
  output         auto_in_1_b_valid,
  output [2:0]   auto_in_1_b_bits_opcode,
  output [1:0]   auto_in_1_b_bits_param,
  output [3:0]   auto_in_1_b_bits_size,
  output [4:0]   auto_in_1_b_bits_source,
  output [36:0]  auto_in_1_b_bits_address,
  output [15:0]  auto_in_1_b_bits_mask,
  output [127:0] auto_in_1_b_bits_data,
  output         auto_in_1_b_bits_corrupt,
  output         auto_in_1_c_ready,
  input          auto_in_1_c_valid,
  input  [2:0]   auto_in_1_c_bits_opcode,
  input  [2:0]   auto_in_1_c_bits_param,
  input  [3:0]   auto_in_1_c_bits_size,
  input  [4:0]   auto_in_1_c_bits_source,
  input  [36:0]  auto_in_1_c_bits_address,
  input  [127:0] auto_in_1_c_bits_data,
  input          auto_in_1_c_bits_corrupt,
  input          auto_in_1_d_ready,
  output         auto_in_1_d_valid,
  output [2:0]   auto_in_1_d_bits_opcode,
  output [1:0]   auto_in_1_d_bits_param,
  output [3:0]   auto_in_1_d_bits_size,
  output [4:0]   auto_in_1_d_bits_source,
  output [4:0]   auto_in_1_d_bits_sink,
  output         auto_in_1_d_bits_denied,
  output [127:0] auto_in_1_d_bits_data,
  output         auto_in_1_d_bits_corrupt,
  output         auto_in_1_e_ready,
  input          auto_in_1_e_valid,
  input  [4:0]   auto_in_1_e_bits_sink,
  output         auto_in_0_a_ready,
  input          auto_in_0_a_valid,
  input  [2:0]   auto_in_0_a_bits_opcode,
  input  [3:0]   auto_in_0_a_bits_size,
  input  [5:0]   auto_in_0_a_bits_source,
  input  [36:0]  auto_in_0_a_bits_address,
  input          auto_in_0_a_bits_user_amba_prot_bufferable,
  input          auto_in_0_a_bits_user_amba_prot_modifiable,
  input          auto_in_0_a_bits_user_amba_prot_readalloc,
  input          auto_in_0_a_bits_user_amba_prot_writealloc,
  input          auto_in_0_a_bits_user_amba_prot_privileged,
  input          auto_in_0_a_bits_user_amba_prot_secure,
  input          auto_in_0_a_bits_user_amba_prot_fetch,
  input  [15:0]  auto_in_0_a_bits_mask,
  input  [127:0] auto_in_0_a_bits_data,
  input          auto_in_0_d_ready,
  output         auto_in_0_d_valid,
  output [2:0]   auto_in_0_d_bits_opcode,
  output [1:0]   auto_in_0_d_bits_param,
  output [3:0]   auto_in_0_d_bits_size,
  output [5:0]   auto_in_0_d_bits_source,
  output [4:0]   auto_in_0_d_bits_sink,
  output         auto_in_0_d_bits_denied,
  output [127:0] auto_in_0_d_bits_data,
  output         auto_in_0_d_bits_corrupt,
  input          auto_out_6_a_ready,
  output         auto_out_6_a_valid,
  output [2:0]   auto_out_6_a_bits_opcode,
  output [2:0]   auto_out_6_a_bits_param,
  output [2:0]   auto_out_6_a_bits_size,
  output [6:0]   auto_out_6_a_bits_source,
  output [36:0]  auto_out_6_a_bits_address,
  output         auto_out_6_a_bits_user_amba_prot_bufferable,
  output         auto_out_6_a_bits_user_amba_prot_modifiable,
  output         auto_out_6_a_bits_user_amba_prot_readalloc,
  output         auto_out_6_a_bits_user_amba_prot_writealloc,
  output         auto_out_6_a_bits_user_amba_prot_privileged,
  output         auto_out_6_a_bits_user_amba_prot_secure,
  output         auto_out_6_a_bits_user_amba_prot_fetch,
  output [15:0]  auto_out_6_a_bits_mask,
  output [127:0] auto_out_6_a_bits_data,
  output         auto_out_6_a_bits_corrupt,
  output         auto_out_6_d_ready,
  input          auto_out_6_d_valid,
  input  [2:0]   auto_out_6_d_bits_opcode,
  input  [2:0]   auto_out_6_d_bits_size,
  input  [6:0]   auto_out_6_d_bits_source,
  input          auto_out_6_d_bits_denied,
  input  [127:0] auto_out_6_d_bits_data,
  input          auto_out_6_d_bits_corrupt,
  input          auto_out_5_a_ready,
  output         auto_out_5_a_valid,
  output [2:0]   auto_out_5_a_bits_opcode,
  output [2:0]   auto_out_5_a_bits_param,
  output [2:0]   auto_out_5_a_bits_size,
  output [6:0]   auto_out_5_a_bits_source,
  output [35:0]  auto_out_5_a_bits_address,
  output [15:0]  auto_out_5_a_bits_mask,
  output [127:0] auto_out_5_a_bits_data,
  output         auto_out_5_a_bits_corrupt,
  output         auto_out_5_b_ready,
  input          auto_out_5_b_valid,
  input  [2:0]   auto_out_5_b_bits_opcode,
  input  [1:0]   auto_out_5_b_bits_param,
  input  [2:0]   auto_out_5_b_bits_size,
  input  [6:0]   auto_out_5_b_bits_source,
  input  [35:0]  auto_out_5_b_bits_address,
  input  [15:0]  auto_out_5_b_bits_mask,
  input  [127:0] auto_out_5_b_bits_data,
  input          auto_out_5_b_bits_corrupt,
  input          auto_out_5_c_ready,
  output         auto_out_5_c_valid,
  output [2:0]   auto_out_5_c_bits_opcode,
  output [2:0]   auto_out_5_c_bits_param,
  output [2:0]   auto_out_5_c_bits_size,
  output [6:0]   auto_out_5_c_bits_source,
  output [35:0]  auto_out_5_c_bits_address,
  output [127:0] auto_out_5_c_bits_data,
  output         auto_out_5_c_bits_corrupt,
  output         auto_out_5_d_ready,
  input          auto_out_5_d_valid,
  input  [2:0]   auto_out_5_d_bits_opcode,
  input  [1:0]   auto_out_5_d_bits_param,
  input  [2:0]   auto_out_5_d_bits_size,
  input  [6:0]   auto_out_5_d_bits_source,
  input  [2:0]   auto_out_5_d_bits_sink,
  input          auto_out_5_d_bits_denied,
  input  [127:0] auto_out_5_d_bits_data,
  input          auto_out_5_d_bits_corrupt,
  input          auto_out_5_e_ready,
  output         auto_out_5_e_valid,
  output [2:0]   auto_out_5_e_bits_sink,
  input          auto_out_4_a_ready,
  output         auto_out_4_a_valid,
  output [2:0]   auto_out_4_a_bits_opcode,
  output [2:0]   auto_out_4_a_bits_param,
  output [2:0]   auto_out_4_a_bits_size,
  output [6:0]   auto_out_4_a_bits_source,
  output [35:0]  auto_out_4_a_bits_address,
  output [15:0]  auto_out_4_a_bits_mask,
  output [127:0] auto_out_4_a_bits_data,
  output         auto_out_4_a_bits_corrupt,
  output         auto_out_4_b_ready,
  input          auto_out_4_b_valid,
  input  [2:0]   auto_out_4_b_bits_opcode,
  input  [1:0]   auto_out_4_b_bits_param,
  input  [2:0]   auto_out_4_b_bits_size,
  input  [6:0]   auto_out_4_b_bits_source,
  input  [35:0]  auto_out_4_b_bits_address,
  input  [15:0]  auto_out_4_b_bits_mask,
  input  [127:0] auto_out_4_b_bits_data,
  input          auto_out_4_b_bits_corrupt,
  input          auto_out_4_c_ready,
  output         auto_out_4_c_valid,
  output [2:0]   auto_out_4_c_bits_opcode,
  output [2:0]   auto_out_4_c_bits_param,
  output [2:0]   auto_out_4_c_bits_size,
  output [6:0]   auto_out_4_c_bits_source,
  output [35:0]  auto_out_4_c_bits_address,
  output [127:0] auto_out_4_c_bits_data,
  output         auto_out_4_c_bits_corrupt,
  output         auto_out_4_d_ready,
  input          auto_out_4_d_valid,
  input  [2:0]   auto_out_4_d_bits_opcode,
  input  [1:0]   auto_out_4_d_bits_param,
  input  [2:0]   auto_out_4_d_bits_size,
  input  [6:0]   auto_out_4_d_bits_source,
  input  [2:0]   auto_out_4_d_bits_sink,
  input          auto_out_4_d_bits_denied,
  input  [127:0] auto_out_4_d_bits_data,
  input          auto_out_4_d_bits_corrupt,
  input          auto_out_4_e_ready,
  output         auto_out_4_e_valid,
  output [2:0]   auto_out_4_e_bits_sink,
  input          auto_out_3_a_ready,
  output         auto_out_3_a_valid,
  output [2:0]   auto_out_3_a_bits_opcode,
  output [2:0]   auto_out_3_a_bits_param,
  output [2:0]   auto_out_3_a_bits_size,
  output [6:0]   auto_out_3_a_bits_source,
  output [35:0]  auto_out_3_a_bits_address,
  output [15:0]  auto_out_3_a_bits_mask,
  output [127:0] auto_out_3_a_bits_data,
  output         auto_out_3_a_bits_corrupt,
  output         auto_out_3_b_ready,
  input          auto_out_3_b_valid,
  input  [2:0]   auto_out_3_b_bits_opcode,
  input  [1:0]   auto_out_3_b_bits_param,
  input  [2:0]   auto_out_3_b_bits_size,
  input  [6:0]   auto_out_3_b_bits_source,
  input  [35:0]  auto_out_3_b_bits_address,
  input  [15:0]  auto_out_3_b_bits_mask,
  input  [127:0] auto_out_3_b_bits_data,
  input          auto_out_3_b_bits_corrupt,
  input          auto_out_3_c_ready,
  output         auto_out_3_c_valid,
  output [2:0]   auto_out_3_c_bits_opcode,
  output [2:0]   auto_out_3_c_bits_param,
  output [2:0]   auto_out_3_c_bits_size,
  output [6:0]   auto_out_3_c_bits_source,
  output [35:0]  auto_out_3_c_bits_address,
  output [127:0] auto_out_3_c_bits_data,
  output         auto_out_3_c_bits_corrupt,
  output         auto_out_3_d_ready,
  input          auto_out_3_d_valid,
  input  [2:0]   auto_out_3_d_bits_opcode,
  input  [1:0]   auto_out_3_d_bits_param,
  input  [2:0]   auto_out_3_d_bits_size,
  input  [6:0]   auto_out_3_d_bits_source,
  input  [2:0]   auto_out_3_d_bits_sink,
  input          auto_out_3_d_bits_denied,
  input  [127:0] auto_out_3_d_bits_data,
  input          auto_out_3_d_bits_corrupt,
  input          auto_out_3_e_ready,
  output         auto_out_3_e_valid,
  output [2:0]   auto_out_3_e_bits_sink,
  input          auto_out_2_a_ready,
  output         auto_out_2_a_valid,
  output [2:0]   auto_out_2_a_bits_opcode,
  output [2:0]   auto_out_2_a_bits_param,
  output [2:0]   auto_out_2_a_bits_size,
  output [6:0]   auto_out_2_a_bits_source,
  output [35:0]  auto_out_2_a_bits_address,
  output [15:0]  auto_out_2_a_bits_mask,
  output [127:0] auto_out_2_a_bits_data,
  output         auto_out_2_a_bits_corrupt,
  output         auto_out_2_b_ready,
  input          auto_out_2_b_valid,
  input  [2:0]   auto_out_2_b_bits_opcode,
  input  [1:0]   auto_out_2_b_bits_param,
  input  [2:0]   auto_out_2_b_bits_size,
  input  [6:0]   auto_out_2_b_bits_source,
  input  [35:0]  auto_out_2_b_bits_address,
  input  [15:0]  auto_out_2_b_bits_mask,
  input  [127:0] auto_out_2_b_bits_data,
  input          auto_out_2_b_bits_corrupt,
  input          auto_out_2_c_ready,
  output         auto_out_2_c_valid,
  output [2:0]   auto_out_2_c_bits_opcode,
  output [2:0]   auto_out_2_c_bits_param,
  output [2:0]   auto_out_2_c_bits_size,
  output [6:0]   auto_out_2_c_bits_source,
  output [35:0]  auto_out_2_c_bits_address,
  output [127:0] auto_out_2_c_bits_data,
  output         auto_out_2_c_bits_corrupt,
  output         auto_out_2_d_ready,
  input          auto_out_2_d_valid,
  input  [2:0]   auto_out_2_d_bits_opcode,
  input  [1:0]   auto_out_2_d_bits_param,
  input  [2:0]   auto_out_2_d_bits_size,
  input  [6:0]   auto_out_2_d_bits_source,
  input  [2:0]   auto_out_2_d_bits_sink,
  input          auto_out_2_d_bits_denied,
  input  [127:0] auto_out_2_d_bits_data,
  input          auto_out_2_d_bits_corrupt,
  input          auto_out_2_e_ready,
  output         auto_out_2_e_valid,
  output [2:0]   auto_out_2_e_bits_sink,
  input          auto_out_1_a_ready,
  output         auto_out_1_a_valid,
  output [2:0]   auto_out_1_a_bits_opcode,
  output [2:0]   auto_out_1_a_bits_param,
  output [3:0]   auto_out_1_a_bits_size,
  output [6:0]   auto_out_1_a_bits_source,
  output [31:0]  auto_out_1_a_bits_address,
  output         auto_out_1_a_bits_user_amba_prot_bufferable,
  output         auto_out_1_a_bits_user_amba_prot_modifiable,
  output         auto_out_1_a_bits_user_amba_prot_readalloc,
  output         auto_out_1_a_bits_user_amba_prot_writealloc,
  output         auto_out_1_a_bits_user_amba_prot_privileged,
  output         auto_out_1_a_bits_user_amba_prot_secure,
  output         auto_out_1_a_bits_user_amba_prot_fetch,
  output [15:0]  auto_out_1_a_bits_mask,
  output [127:0] auto_out_1_a_bits_data,
  output         auto_out_1_a_bits_corrupt,
  output         auto_out_1_d_ready,
  input          auto_out_1_d_valid,
  input  [2:0]   auto_out_1_d_bits_opcode,
  input  [1:0]   auto_out_1_d_bits_param,
  input  [3:0]   auto_out_1_d_bits_size,
  input  [6:0]   auto_out_1_d_bits_source,
  input          auto_out_1_d_bits_sink,
  input          auto_out_1_d_bits_denied,
  input  [127:0] auto_out_1_d_bits_data,
  input          auto_out_1_d_bits_corrupt,
  input          auto_out_0_a_ready,
  output         auto_out_0_a_valid,
  output [2:0]   auto_out_0_a_bits_opcode,
  output [2:0]   auto_out_0_a_bits_param,
  output [2:0]   auto_out_0_a_bits_size,
  output [6:0]   auto_out_0_a_bits_source,
  output [27:0]  auto_out_0_a_bits_address,
  output [15:0]  auto_out_0_a_bits_mask,
  output [127:0] auto_out_0_a_bits_data,
  output         auto_out_0_a_bits_corrupt,
  output         auto_out_0_d_ready,
  input          auto_out_0_d_valid,
  input  [2:0]   auto_out_0_d_bits_opcode,
  input  [2:0]   auto_out_0_d_bits_size,
  input  [6:0]   auto_out_0_d_bits_source,
  input  [127:0] auto_out_0_d_bits_data,
  input          auto_out_0_d_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [31:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [31:0] _RAND_59;
  reg [31:0] _RAND_60;
  reg [31:0] _RAND_61;
  reg [31:0] _RAND_62;
  reg [31:0] _RAND_63;
  reg [31:0] _RAND_64;
  reg [31:0] _RAND_65;
  reg [31:0] _RAND_66;
  reg [31:0] _RAND_67;
  reg [31:0] _RAND_68;
  reg [31:0] _RAND_69;
  reg [31:0] _RAND_70;
  reg [31:0] _RAND_71;
  reg [31:0] _RAND_72;
  reg [31:0] _RAND_73;
  reg [31:0] _RAND_74;
  reg [31:0] _RAND_75;
  reg [31:0] _RAND_76;
  reg [31:0] _RAND_77;
  reg [31:0] _RAND_78;
  reg [31:0] _RAND_79;
  reg [31:0] _RAND_80;
  reg [31:0] _RAND_81;
  reg [31:0] _RAND_82;
  reg [31:0] _RAND_83;
  reg [31:0] _RAND_84;
  reg [31:0] _RAND_85;
  reg [31:0] _RAND_86;
  reg [31:0] _RAND_87;
  reg [31:0] _RAND_88;
  reg [31:0] _RAND_89;
  reg [31:0] _RAND_90;
  reg [31:0] _RAND_91;
  reg [31:0] _RAND_92;
  reg [31:0] _RAND_93;
  reg [31:0] _RAND_94;
  reg [31:0] _RAND_95;
  reg [31:0] _RAND_96;
  reg [31:0] _RAND_97;
  reg [31:0] _RAND_98;
  reg [31:0] _RAND_99;
  reg [31:0] _RAND_100;
  reg [31:0] _RAND_101;
  reg [31:0] _RAND_102;
  reg [31:0] _RAND_103;
  reg [31:0] _RAND_104;
  reg [31:0] _RAND_105;
`endif // RANDOMIZE_REG_INIT
  reg [3:0] beatsLeft_15; // @[Arbiter.scala 87:30]
  wire  idle_15 = beatsLeft_15 == 4'h0; // @[Arbiter.scala 88:28]
  wire  requestDOI_6_0 = ~auto_out_6_d_bits_source[6]; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_6_0_valid = auto_out_6_d_valid & requestDOI_6_0; // @[Xbar.scala 179:40]
  wire  requestDOI_5_0 = ~auto_out_5_d_bits_source[6]; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_5_0_valid = auto_out_5_d_valid & requestDOI_5_0; // @[Xbar.scala 179:40]
  wire  requestDOI_4_0 = ~auto_out_4_d_bits_source[6]; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_4_0_valid = auto_out_4_d_valid & requestDOI_4_0; // @[Xbar.scala 179:40]
  wire  requestDOI_3_0 = ~auto_out_3_d_bits_source[6]; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_3_0_valid = auto_out_3_d_valid & requestDOI_3_0; // @[Xbar.scala 179:40]
  wire  requestDOI_2_0 = ~auto_out_2_d_bits_source[6]; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_2_0_valid = auto_out_2_d_valid & requestDOI_2_0; // @[Xbar.scala 179:40]
  wire  requestDOI_1_0 = ~auto_out_1_d_bits_source[6]; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_1_0_valid = auto_out_1_d_valid & requestDOI_1_0; // @[Xbar.scala 179:40]
  wire  requestDOI_0_0 = ~auto_out_0_d_bits_source[6]; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered__0_valid = auto_out_0_d_valid & requestDOI_0_0; // @[Xbar.scala 179:40]
  wire [6:0] readys_filter_lo_15 = {portsDIO_filtered_6_0_valid,portsDIO_filtered_5_0_valid,portsDIO_filtered_4_0_valid,
    portsDIO_filtered_3_0_valid,portsDIO_filtered_2_0_valid,portsDIO_filtered_1_0_valid,portsDIO_filtered__0_valid}; // @[Cat.scala 30:58]
  reg [6:0] readys_mask_15; // @[Arbiter.scala 23:23]
  wire [6:0] _readys_filter_T_15 = ~readys_mask_15; // @[Arbiter.scala 24:30]
  wire [6:0] readys_filter_hi_15 = readys_filter_lo_15 & _readys_filter_T_15; // @[Arbiter.scala 24:28]
  wire [13:0] readys_filter_15 = {readys_filter_hi_15,portsDIO_filtered_6_0_valid,portsDIO_filtered_5_0_valid,
    portsDIO_filtered_4_0_valid,portsDIO_filtered_3_0_valid,portsDIO_filtered_2_0_valid,portsDIO_filtered_1_0_valid,
    portsDIO_filtered__0_valid}; // @[Cat.scala 30:58]
  wire [13:0] _GEN_20 = {{1'd0}, readys_filter_15[13:1]}; // @[package.scala 253:43]
  wire [13:0] _readys_unready_T_90 = readys_filter_15 | _GEN_20; // @[package.scala 253:43]
  wire [13:0] _GEN_21 = {{2'd0}, _readys_unready_T_90[13:2]}; // @[package.scala 253:43]
  wire [13:0] _readys_unready_T_92 = _readys_unready_T_90 | _GEN_21; // @[package.scala 253:43]
  wire [13:0] _GEN_22 = {{4'd0}, _readys_unready_T_92[13:4]}; // @[package.scala 253:43]
  wire [13:0] _readys_unready_T_94 = _readys_unready_T_92 | _GEN_22; // @[package.scala 253:43]
  wire [13:0] _readys_unready_T_97 = {readys_mask_15, 7'h0}; // @[Arbiter.scala 25:66]
  wire [13:0] _GEN_23 = {{1'd0}, _readys_unready_T_94[13:1]}; // @[Arbiter.scala 25:58]
  wire [13:0] readys_unready_15 = _GEN_23 | _readys_unready_T_97; // @[Arbiter.scala 25:58]
  wire [6:0] _readys_readys_T_47 = readys_unready_15[13:7] & readys_unready_15[6:0]; // @[Arbiter.scala 26:39]
  wire [6:0] readys_readys_15 = ~_readys_readys_T_47; // @[Arbiter.scala 26:18]
  wire  readys_15_0 = readys_readys_15[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_15_0 = readys_15_0 & portsDIO_filtered__0_valid; // @[Arbiter.scala 97:79]
  reg  state_15_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_15_0 = idle_15 ? earlyWinner_15_0 : state_15_0; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1241 = muxStateEarly_15_0 ? auto_out_0_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire  readys_15_1 = readys_readys_15[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_15_1 = readys_15_1 & portsDIO_filtered_1_0_valid; // @[Arbiter.scala 97:79]
  reg  state_15_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_15_1 = idle_15 ? earlyWinner_15_1 : state_15_1; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1242 = muxStateEarly_15_1 ? auto_out_1_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1248 = _T_1241 | _T_1242; // @[Mux.scala 27:72]
  wire  readys_15_2 = readys_readys_15[2]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_15_2 = readys_15_2 & portsDIO_filtered_2_0_valid; // @[Arbiter.scala 97:79]
  reg  state_15_2; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_15_2 = idle_15 ? earlyWinner_15_2 : state_15_2; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1243 = muxStateEarly_15_2 ? auto_out_2_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1249 = _T_1248 | _T_1243; // @[Mux.scala 27:72]
  wire  readys_15_3 = readys_readys_15[3]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_15_3 = readys_15_3 & portsDIO_filtered_3_0_valid; // @[Arbiter.scala 97:79]
  reg  state_15_3; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_15_3 = idle_15 ? earlyWinner_15_3 : state_15_3; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1244 = muxStateEarly_15_3 ? auto_out_3_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1250 = _T_1249 | _T_1244; // @[Mux.scala 27:72]
  wire  readys_15_4 = readys_readys_15[4]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_15_4 = readys_15_4 & portsDIO_filtered_4_0_valid; // @[Arbiter.scala 97:79]
  reg  state_15_4; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_15_4 = idle_15 ? earlyWinner_15_4 : state_15_4; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1245 = muxStateEarly_15_4 ? auto_out_4_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1251 = _T_1250 | _T_1245; // @[Mux.scala 27:72]
  wire  readys_15_5 = readys_readys_15[5]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_15_5 = readys_15_5 & portsDIO_filtered_5_0_valid; // @[Arbiter.scala 97:79]
  reg  state_15_5; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_15_5 = idle_15 ? earlyWinner_15_5 : state_15_5; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1246 = muxStateEarly_15_5 ? auto_out_5_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1252 = _T_1251 | _T_1246; // @[Mux.scala 27:72]
  wire  readys_15_6 = readys_readys_15[6]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_15_6 = readys_15_6 & portsDIO_filtered_6_0_valid; // @[Arbiter.scala 97:79]
  reg  state_15_6; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_15_6 = idle_15 ? earlyWinner_15_6 : state_15_6; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1247 = muxStateEarly_15_6 ? auto_out_6_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] sink_ACancel_15_bits_source = _T_1252 | _T_1247; // @[Mux.scala 27:72]
  wire [6:0] _GEN_24 = {{2'd0}, auto_in_1_a_bits_source}; // @[Xbar.scala 237:55]
  wire [6:0] in_1_a_bits_source = _GEN_24 | 7'h60; // @[Xbar.scala 237:55]
  reg [1:0] beatsLeft_16; // @[Arbiter.scala 87:30]
  wire  idle_16 = beatsLeft_16 == 2'h0; // @[Arbiter.scala 88:28]
  wire  requestBOI_5_1 = auto_out_5_b_bits_source[6:5] == 2'h3; // @[Parameters.scala 54:32]
  wire  portsBIO_filtered_5_1_valid = auto_out_5_b_valid & requestBOI_5_1; // @[Xbar.scala 179:40]
  wire  requestBOI_4_1 = auto_out_4_b_bits_source[6:5] == 2'h3; // @[Parameters.scala 54:32]
  wire  portsBIO_filtered_4_1_valid = auto_out_4_b_valid & requestBOI_4_1; // @[Xbar.scala 179:40]
  wire  requestBOI_3_1 = auto_out_3_b_bits_source[6:5] == 2'h3; // @[Parameters.scala 54:32]
  wire  portsBIO_filtered_3_1_valid = auto_out_3_b_valid & requestBOI_3_1; // @[Xbar.scala 179:40]
  wire  requestBOI_2_1 = auto_out_2_b_bits_source[6:5] == 2'h3; // @[Parameters.scala 54:32]
  wire  portsBIO_filtered_2_1_valid = auto_out_2_b_valid & requestBOI_2_1; // @[Xbar.scala 179:40]
  wire [3:0] readys_filter_lo_16 = {portsBIO_filtered_5_1_valid,portsBIO_filtered_4_1_valid,portsBIO_filtered_3_1_valid,
    portsBIO_filtered_2_1_valid}; // @[Cat.scala 30:58]
  reg [3:0] readys_mask_16; // @[Arbiter.scala 23:23]
  wire [3:0] _readys_filter_T_16 = ~readys_mask_16; // @[Arbiter.scala 24:30]
  wire [3:0] readys_filter_hi_16 = readys_filter_lo_16 & _readys_filter_T_16; // @[Arbiter.scala 24:28]
  wire [7:0] readys_filter_16 = {readys_filter_hi_16,portsBIO_filtered_5_1_valid,portsBIO_filtered_4_1_valid,
    portsBIO_filtered_3_1_valid,portsBIO_filtered_2_1_valid}; // @[Cat.scala 30:58]
  wire [7:0] _GEN_25 = {{1'd0}, readys_filter_16[7:1]}; // @[package.scala 253:43]
  wire [7:0] _readys_unready_T_99 = readys_filter_16 | _GEN_25; // @[package.scala 253:43]
  wire [7:0] _GEN_26 = {{2'd0}, _readys_unready_T_99[7:2]}; // @[package.scala 253:43]
  wire [7:0] _readys_unready_T_101 = _readys_unready_T_99 | _GEN_26; // @[package.scala 253:43]
  wire [7:0] _readys_unready_T_104 = {readys_mask_16, 4'h0}; // @[Arbiter.scala 25:66]
  wire [7:0] _GEN_27 = {{1'd0}, _readys_unready_T_101[7:1]}; // @[Arbiter.scala 25:58]
  wire [7:0] readys_unready_16 = _GEN_27 | _readys_unready_T_104; // @[Arbiter.scala 25:58]
  wire [3:0] _readys_readys_T_50 = readys_unready_16[7:4] & readys_unready_16[3:0]; // @[Arbiter.scala 26:39]
  wire [3:0] readys_readys_16 = ~_readys_readys_T_50; // @[Arbiter.scala 26:18]
  wire  readys_16_0 = readys_readys_16[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_16_0 = readys_16_0 & portsBIO_filtered_2_1_valid; // @[Arbiter.scala 97:79]
  reg  state_16_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_16_0 = idle_16 ? earlyWinner_16_0 : state_16_0; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1361 = muxStateEarly_16_0 ? auto_out_2_b_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire  readys_16_1 = readys_readys_16[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_16_1 = readys_16_1 & portsBIO_filtered_3_1_valid; // @[Arbiter.scala 97:79]
  reg  state_16_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_16_1 = idle_16 ? earlyWinner_16_1 : state_16_1; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1362 = muxStateEarly_16_1 ? auto_out_3_b_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1365 = _T_1361 | _T_1362; // @[Mux.scala 27:72]
  wire  readys_16_2 = readys_readys_16[2]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_16_2 = readys_16_2 & portsBIO_filtered_4_1_valid; // @[Arbiter.scala 97:79]
  reg  state_16_2; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_16_2 = idle_16 ? earlyWinner_16_2 : state_16_2; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1363 = muxStateEarly_16_2 ? auto_out_4_b_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1366 = _T_1365 | _T_1363; // @[Mux.scala 27:72]
  wire  readys_16_3 = readys_readys_16[3]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_16_3 = readys_16_3 & portsBIO_filtered_5_1_valid; // @[Arbiter.scala 97:79]
  reg  state_16_3; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_16_3 = idle_16 ? earlyWinner_16_3 : state_16_3; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1364 = muxStateEarly_16_3 ? auto_out_5_b_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] sink_ACancel_16_bits_source = _T_1366 | _T_1364; // @[Mux.scala 27:72]
  wire [6:0] _GEN_28 = {{2'd0}, auto_in_1_c_bits_source}; // @[Xbar.scala 259:55]
  wire [6:0] in_1_c_bits_source = _GEN_28 | 7'h60; // @[Xbar.scala 259:55]
  reg [3:0] beatsLeft_17; // @[Arbiter.scala 87:30]
  wire  idle_17 = beatsLeft_17 == 4'h0; // @[Arbiter.scala 88:28]
  wire  requestDOI_6_1 = auto_out_6_d_bits_source[6:5] == 2'h3; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_6_1_valid = auto_out_6_d_valid & requestDOI_6_1; // @[Xbar.scala 179:40]
  wire  requestDOI_5_1 = auto_out_5_d_bits_source[6:5] == 2'h3; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_5_1_valid = auto_out_5_d_valid & requestDOI_5_1; // @[Xbar.scala 179:40]
  wire  requestDOI_4_1 = auto_out_4_d_bits_source[6:5] == 2'h3; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_4_1_valid = auto_out_4_d_valid & requestDOI_4_1; // @[Xbar.scala 179:40]
  wire  requestDOI_3_1 = auto_out_3_d_bits_source[6:5] == 2'h3; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_3_1_valid = auto_out_3_d_valid & requestDOI_3_1; // @[Xbar.scala 179:40]
  wire  requestDOI_2_1 = auto_out_2_d_bits_source[6:5] == 2'h3; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_2_1_valid = auto_out_2_d_valid & requestDOI_2_1; // @[Xbar.scala 179:40]
  wire  requestDOI_1_1 = auto_out_1_d_bits_source[6:5] == 2'h3; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_1_1_valid = auto_out_1_d_valid & requestDOI_1_1; // @[Xbar.scala 179:40]
  wire  requestDOI_0_1 = auto_out_0_d_bits_source[6:5] == 2'h3; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered__1_valid = auto_out_0_d_valid & requestDOI_0_1; // @[Xbar.scala 179:40]
  wire [6:0] readys_filter_lo_17 = {portsDIO_filtered_6_1_valid,portsDIO_filtered_5_1_valid,portsDIO_filtered_4_1_valid,
    portsDIO_filtered_3_1_valid,portsDIO_filtered_2_1_valid,portsDIO_filtered_1_1_valid,portsDIO_filtered__1_valid}; // @[Cat.scala 30:58]
  reg [6:0] readys_mask_17; // @[Arbiter.scala 23:23]
  wire [6:0] _readys_filter_T_17 = ~readys_mask_17; // @[Arbiter.scala 24:30]
  wire [6:0] readys_filter_hi_17 = readys_filter_lo_17 & _readys_filter_T_17; // @[Arbiter.scala 24:28]
  wire [13:0] readys_filter_17 = {readys_filter_hi_17,portsDIO_filtered_6_1_valid,portsDIO_filtered_5_1_valid,
    portsDIO_filtered_4_1_valid,portsDIO_filtered_3_1_valid,portsDIO_filtered_2_1_valid,portsDIO_filtered_1_1_valid,
    portsDIO_filtered__1_valid}; // @[Cat.scala 30:58]
  wire [13:0] _GEN_29 = {{1'd0}, readys_filter_17[13:1]}; // @[package.scala 253:43]
  wire [13:0] _readys_unready_T_106 = readys_filter_17 | _GEN_29; // @[package.scala 253:43]
  wire [13:0] _GEN_30 = {{2'd0}, _readys_unready_T_106[13:2]}; // @[package.scala 253:43]
  wire [13:0] _readys_unready_T_108 = _readys_unready_T_106 | _GEN_30; // @[package.scala 253:43]
  wire [13:0] _GEN_31 = {{4'd0}, _readys_unready_T_108[13:4]}; // @[package.scala 253:43]
  wire [13:0] _readys_unready_T_110 = _readys_unready_T_108 | _GEN_31; // @[package.scala 253:43]
  wire [13:0] _readys_unready_T_113 = {readys_mask_17, 7'h0}; // @[Arbiter.scala 25:66]
  wire [13:0] _GEN_32 = {{1'd0}, _readys_unready_T_110[13:1]}; // @[Arbiter.scala 25:58]
  wire [13:0] readys_unready_17 = _GEN_32 | _readys_unready_T_113; // @[Arbiter.scala 25:58]
  wire [6:0] _readys_readys_T_53 = readys_unready_17[13:7] & readys_unready_17[6:0]; // @[Arbiter.scala 26:39]
  wire [6:0] readys_readys_17 = ~_readys_readys_T_53; // @[Arbiter.scala 26:18]
  wire  readys_17_0 = readys_readys_17[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_17_0 = readys_17_0 & portsDIO_filtered__1_valid; // @[Arbiter.scala 97:79]
  reg  state_17_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_17_0 = idle_17 ? earlyWinner_17_0 : state_17_0; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1505 = muxStateEarly_17_0 ? auto_out_0_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire  readys_17_1 = readys_readys_17[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_17_1 = readys_17_1 & portsDIO_filtered_1_1_valid; // @[Arbiter.scala 97:79]
  reg  state_17_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_17_1 = idle_17 ? earlyWinner_17_1 : state_17_1; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1506 = muxStateEarly_17_1 ? auto_out_1_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1512 = _T_1505 | _T_1506; // @[Mux.scala 27:72]
  wire  readys_17_2 = readys_readys_17[2]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_17_2 = readys_17_2 & portsDIO_filtered_2_1_valid; // @[Arbiter.scala 97:79]
  reg  state_17_2; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_17_2 = idle_17 ? earlyWinner_17_2 : state_17_2; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1507 = muxStateEarly_17_2 ? auto_out_2_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1513 = _T_1512 | _T_1507; // @[Mux.scala 27:72]
  wire  readys_17_3 = readys_readys_17[3]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_17_3 = readys_17_3 & portsDIO_filtered_3_1_valid; // @[Arbiter.scala 97:79]
  reg  state_17_3; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_17_3 = idle_17 ? earlyWinner_17_3 : state_17_3; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1508 = muxStateEarly_17_3 ? auto_out_3_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1514 = _T_1513 | _T_1508; // @[Mux.scala 27:72]
  wire  readys_17_4 = readys_readys_17[4]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_17_4 = readys_17_4 & portsDIO_filtered_4_1_valid; // @[Arbiter.scala 97:79]
  reg  state_17_4; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_17_4 = idle_17 ? earlyWinner_17_4 : state_17_4; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1509 = muxStateEarly_17_4 ? auto_out_4_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1515 = _T_1514 | _T_1509; // @[Mux.scala 27:72]
  wire  readys_17_5 = readys_readys_17[5]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_17_5 = readys_17_5 & portsDIO_filtered_5_1_valid; // @[Arbiter.scala 97:79]
  reg  state_17_5; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_17_5 = idle_17 ? earlyWinner_17_5 : state_17_5; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1510 = muxStateEarly_17_5 ? auto_out_5_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1516 = _T_1515 | _T_1510; // @[Mux.scala 27:72]
  wire  readys_17_6 = readys_readys_17[6]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_17_6 = readys_17_6 & portsDIO_filtered_6_1_valid; // @[Arbiter.scala 97:79]
  reg  state_17_6; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_17_6 = idle_17 ? earlyWinner_17_6 : state_17_6; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1511 = muxStateEarly_17_6 ? auto_out_6_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] sink_ACancel_17_bits_source = _T_1516 | _T_1511; // @[Mux.scala 27:72]
  wire [6:0] _GEN_33 = {{2'd0}, auto_in_2_a_bits_source}; // @[Xbar.scala 237:55]
  wire [6:0] in_2_a_bits_source = _GEN_33 | 7'h40; // @[Xbar.scala 237:55]
  reg [1:0] beatsLeft_18; // @[Arbiter.scala 87:30]
  wire  idle_18 = beatsLeft_18 == 2'h0; // @[Arbiter.scala 88:28]
  wire  requestBOI_5_2 = auto_out_5_b_bits_source[6:5] == 2'h2; // @[Parameters.scala 54:32]
  wire  portsBIO_filtered_5_2_valid = auto_out_5_b_valid & requestBOI_5_2; // @[Xbar.scala 179:40]
  wire  requestBOI_4_2 = auto_out_4_b_bits_source[6:5] == 2'h2; // @[Parameters.scala 54:32]
  wire  portsBIO_filtered_4_2_valid = auto_out_4_b_valid & requestBOI_4_2; // @[Xbar.scala 179:40]
  wire  requestBOI_3_2 = auto_out_3_b_bits_source[6:5] == 2'h2; // @[Parameters.scala 54:32]
  wire  portsBIO_filtered_3_2_valid = auto_out_3_b_valid & requestBOI_3_2; // @[Xbar.scala 179:40]
  wire  requestBOI_2_2 = auto_out_2_b_bits_source[6:5] == 2'h2; // @[Parameters.scala 54:32]
  wire  portsBIO_filtered_2_2_valid = auto_out_2_b_valid & requestBOI_2_2; // @[Xbar.scala 179:40]
  wire [3:0] readys_filter_lo_18 = {portsBIO_filtered_5_2_valid,portsBIO_filtered_4_2_valid,portsBIO_filtered_3_2_valid,
    portsBIO_filtered_2_2_valid}; // @[Cat.scala 30:58]
  reg [3:0] readys_mask_18; // @[Arbiter.scala 23:23]
  wire [3:0] _readys_filter_T_18 = ~readys_mask_18; // @[Arbiter.scala 24:30]
  wire [3:0] readys_filter_hi_18 = readys_filter_lo_18 & _readys_filter_T_18; // @[Arbiter.scala 24:28]
  wire [7:0] readys_filter_18 = {readys_filter_hi_18,portsBIO_filtered_5_2_valid,portsBIO_filtered_4_2_valid,
    portsBIO_filtered_3_2_valid,portsBIO_filtered_2_2_valid}; // @[Cat.scala 30:58]
  wire [7:0] _GEN_34 = {{1'd0}, readys_filter_18[7:1]}; // @[package.scala 253:43]
  wire [7:0] _readys_unready_T_115 = readys_filter_18 | _GEN_34; // @[package.scala 253:43]
  wire [7:0] _GEN_35 = {{2'd0}, _readys_unready_T_115[7:2]}; // @[package.scala 253:43]
  wire [7:0] _readys_unready_T_117 = _readys_unready_T_115 | _GEN_35; // @[package.scala 253:43]
  wire [7:0] _readys_unready_T_120 = {readys_mask_18, 4'h0}; // @[Arbiter.scala 25:66]
  wire [7:0] _GEN_36 = {{1'd0}, _readys_unready_T_117[7:1]}; // @[Arbiter.scala 25:58]
  wire [7:0] readys_unready_18 = _GEN_36 | _readys_unready_T_120; // @[Arbiter.scala 25:58]
  wire [3:0] _readys_readys_T_56 = readys_unready_18[7:4] & readys_unready_18[3:0]; // @[Arbiter.scala 26:39]
  wire [3:0] readys_readys_18 = ~_readys_readys_T_56; // @[Arbiter.scala 26:18]
  wire  readys_18_0 = readys_readys_18[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_18_0 = readys_18_0 & portsBIO_filtered_2_2_valid; // @[Arbiter.scala 97:79]
  reg  state_18_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_18_0 = idle_18 ? earlyWinner_18_0 : state_18_0; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1625 = muxStateEarly_18_0 ? auto_out_2_b_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire  readys_18_1 = readys_readys_18[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_18_1 = readys_18_1 & portsBIO_filtered_3_2_valid; // @[Arbiter.scala 97:79]
  reg  state_18_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_18_1 = idle_18 ? earlyWinner_18_1 : state_18_1; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1626 = muxStateEarly_18_1 ? auto_out_3_b_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1629 = _T_1625 | _T_1626; // @[Mux.scala 27:72]
  wire  readys_18_2 = readys_readys_18[2]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_18_2 = readys_18_2 & portsBIO_filtered_4_2_valid; // @[Arbiter.scala 97:79]
  reg  state_18_2; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_18_2 = idle_18 ? earlyWinner_18_2 : state_18_2; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1627 = muxStateEarly_18_2 ? auto_out_4_b_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1630 = _T_1629 | _T_1627; // @[Mux.scala 27:72]
  wire  readys_18_3 = readys_readys_18[3]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_18_3 = readys_18_3 & portsBIO_filtered_5_2_valid; // @[Arbiter.scala 97:79]
  reg  state_18_3; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_18_3 = idle_18 ? earlyWinner_18_3 : state_18_3; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1628 = muxStateEarly_18_3 ? auto_out_5_b_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] sink_ACancel_18_bits_source = _T_1630 | _T_1628; // @[Mux.scala 27:72]
  wire [6:0] _GEN_37 = {{2'd0}, auto_in_2_c_bits_source}; // @[Xbar.scala 259:55]
  wire [6:0] in_2_c_bits_source = _GEN_37 | 7'h40; // @[Xbar.scala 259:55]
  reg [3:0] beatsLeft_19; // @[Arbiter.scala 87:30]
  wire  idle_19 = beatsLeft_19 == 4'h0; // @[Arbiter.scala 88:28]
  wire  requestDOI_6_2 = auto_out_6_d_bits_source[6:5] == 2'h2; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_6_2_valid = auto_out_6_d_valid & requestDOI_6_2; // @[Xbar.scala 179:40]
  wire  requestDOI_5_2 = auto_out_5_d_bits_source[6:5] == 2'h2; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_5_2_valid = auto_out_5_d_valid & requestDOI_5_2; // @[Xbar.scala 179:40]
  wire  requestDOI_4_2 = auto_out_4_d_bits_source[6:5] == 2'h2; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_4_2_valid = auto_out_4_d_valid & requestDOI_4_2; // @[Xbar.scala 179:40]
  wire  requestDOI_3_2 = auto_out_3_d_bits_source[6:5] == 2'h2; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_3_2_valid = auto_out_3_d_valid & requestDOI_3_2; // @[Xbar.scala 179:40]
  wire  requestDOI_2_2 = auto_out_2_d_bits_source[6:5] == 2'h2; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_2_2_valid = auto_out_2_d_valid & requestDOI_2_2; // @[Xbar.scala 179:40]
  wire  requestDOI_1_2 = auto_out_1_d_bits_source[6:5] == 2'h2; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered_1_2_valid = auto_out_1_d_valid & requestDOI_1_2; // @[Xbar.scala 179:40]
  wire  requestDOI_0_2 = auto_out_0_d_bits_source[6:5] == 2'h2; // @[Parameters.scala 54:32]
  wire  portsDIO_filtered__2_valid = auto_out_0_d_valid & requestDOI_0_2; // @[Xbar.scala 179:40]
  wire [6:0] readys_filter_lo_19 = {portsDIO_filtered_6_2_valid,portsDIO_filtered_5_2_valid,portsDIO_filtered_4_2_valid,
    portsDIO_filtered_3_2_valid,portsDIO_filtered_2_2_valid,portsDIO_filtered_1_2_valid,portsDIO_filtered__2_valid}; // @[Cat.scala 30:58]
  reg [6:0] readys_mask_19; // @[Arbiter.scala 23:23]
  wire [6:0] _readys_filter_T_19 = ~readys_mask_19; // @[Arbiter.scala 24:30]
  wire [6:0] readys_filter_hi_19 = readys_filter_lo_19 & _readys_filter_T_19; // @[Arbiter.scala 24:28]
  wire [13:0] readys_filter_19 = {readys_filter_hi_19,portsDIO_filtered_6_2_valid,portsDIO_filtered_5_2_valid,
    portsDIO_filtered_4_2_valid,portsDIO_filtered_3_2_valid,portsDIO_filtered_2_2_valid,portsDIO_filtered_1_2_valid,
    portsDIO_filtered__2_valid}; // @[Cat.scala 30:58]
  wire [13:0] _GEN_38 = {{1'd0}, readys_filter_19[13:1]}; // @[package.scala 253:43]
  wire [13:0] _readys_unready_T_122 = readys_filter_19 | _GEN_38; // @[package.scala 253:43]
  wire [13:0] _GEN_39 = {{2'd0}, _readys_unready_T_122[13:2]}; // @[package.scala 253:43]
  wire [13:0] _readys_unready_T_124 = _readys_unready_T_122 | _GEN_39; // @[package.scala 253:43]
  wire [13:0] _GEN_40 = {{4'd0}, _readys_unready_T_124[13:4]}; // @[package.scala 253:43]
  wire [13:0] _readys_unready_T_126 = _readys_unready_T_124 | _GEN_40; // @[package.scala 253:43]
  wire [13:0] _readys_unready_T_129 = {readys_mask_19, 7'h0}; // @[Arbiter.scala 25:66]
  wire [13:0] _GEN_41 = {{1'd0}, _readys_unready_T_126[13:1]}; // @[Arbiter.scala 25:58]
  wire [13:0] readys_unready_19 = _GEN_41 | _readys_unready_T_129; // @[Arbiter.scala 25:58]
  wire [6:0] _readys_readys_T_59 = readys_unready_19[13:7] & readys_unready_19[6:0]; // @[Arbiter.scala 26:39]
  wire [6:0] readys_readys_19 = ~_readys_readys_T_59; // @[Arbiter.scala 26:18]
  wire  readys_19_0 = readys_readys_19[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_19_0 = readys_19_0 & portsDIO_filtered__2_valid; // @[Arbiter.scala 97:79]
  reg  state_19_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_19_0 = idle_19 ? earlyWinner_19_0 : state_19_0; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1769 = muxStateEarly_19_0 ? auto_out_0_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire  readys_19_1 = readys_readys_19[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_19_1 = readys_19_1 & portsDIO_filtered_1_2_valid; // @[Arbiter.scala 97:79]
  reg  state_19_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_19_1 = idle_19 ? earlyWinner_19_1 : state_19_1; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1770 = muxStateEarly_19_1 ? auto_out_1_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1776 = _T_1769 | _T_1770; // @[Mux.scala 27:72]
  wire  readys_19_2 = readys_readys_19[2]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_19_2 = readys_19_2 & portsDIO_filtered_2_2_valid; // @[Arbiter.scala 97:79]
  reg  state_19_2; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_19_2 = idle_19 ? earlyWinner_19_2 : state_19_2; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1771 = muxStateEarly_19_2 ? auto_out_2_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1777 = _T_1776 | _T_1771; // @[Mux.scala 27:72]
  wire  readys_19_3 = readys_readys_19[3]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_19_3 = readys_19_3 & portsDIO_filtered_3_2_valid; // @[Arbiter.scala 97:79]
  reg  state_19_3; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_19_3 = idle_19 ? earlyWinner_19_3 : state_19_3; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1772 = muxStateEarly_19_3 ? auto_out_3_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1778 = _T_1777 | _T_1772; // @[Mux.scala 27:72]
  wire  readys_19_4 = readys_readys_19[4]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_19_4 = readys_19_4 & portsDIO_filtered_4_2_valid; // @[Arbiter.scala 97:79]
  reg  state_19_4; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_19_4 = idle_19 ? earlyWinner_19_4 : state_19_4; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1773 = muxStateEarly_19_4 ? auto_out_4_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1779 = _T_1778 | _T_1773; // @[Mux.scala 27:72]
  wire  readys_19_5 = readys_readys_19[5]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_19_5 = readys_19_5 & portsDIO_filtered_5_2_valid; // @[Arbiter.scala 97:79]
  reg  state_19_5; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_19_5 = idle_19 ? earlyWinner_19_5 : state_19_5; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1774 = muxStateEarly_19_5 ? auto_out_5_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1780 = _T_1779 | _T_1774; // @[Mux.scala 27:72]
  wire  readys_19_6 = readys_readys_19[6]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_19_6 = readys_19_6 & portsDIO_filtered_6_2_valid; // @[Arbiter.scala 97:79]
  reg  state_19_6; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_19_6 = idle_19 ? earlyWinner_19_6 : state_19_6; // @[Arbiter.scala 117:30]
  wire [6:0] _T_1775 = muxStateEarly_19_6 ? auto_out_6_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] sink_ACancel_19_bits_source = _T_1780 | _T_1775; // @[Mux.scala 27:72]
  wire [4:0] _GEN_42 = {{2'd0}, auto_out_2_d_bits_sink}; // @[Xbar.scala 323:53]
  wire [4:0] out_3_2_d_bits_sink = _GEN_42 | 5'h18; // @[Xbar.scala 323:53]
  reg  beatsLeft_4; // @[Arbiter.scala 87:30]
  wire  idle_4 = ~beatsLeft_4; // @[Arbiter.scala 88:28]
  wire  requestEIO_2_2 = auto_in_2_e_bits_sink[4:3] == 2'h3; // @[Parameters.scala 54:32]
  wire  portsEOI_filtered_2_2_valid = auto_in_2_e_valid & requestEIO_2_2; // @[Xbar.scala 179:40]
  wire  requestEIO_1_2 = auto_in_1_e_bits_sink[4:3] == 2'h3; // @[Parameters.scala 54:32]
  wire  portsEOI_filtered_1_2_valid = auto_in_1_e_valid & requestEIO_1_2; // @[Xbar.scala 179:40]
  wire [1:0] readys_filter_lo_4 = {portsEOI_filtered_2_2_valid,portsEOI_filtered_1_2_valid}; // @[Cat.scala 30:58]
  reg [1:0] readys_mask_4; // @[Arbiter.scala 23:23]
  wire [1:0] _readys_filter_T_4 = ~readys_mask_4; // @[Arbiter.scala 24:30]
  wire [1:0] readys_filter_hi_4 = readys_filter_lo_4 & _readys_filter_T_4; // @[Arbiter.scala 24:28]
  wire [3:0] readys_filter_4 = {readys_filter_hi_4,portsEOI_filtered_2_2_valid,portsEOI_filtered_1_2_valid}; // @[Cat.scala 30:58]
  wire [3:0] _GEN_43 = {{1'd0}, readys_filter_4[3:1]}; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_27 = readys_filter_4 | _GEN_43; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_30 = {readys_mask_4, 2'h0}; // @[Arbiter.scala 25:66]
  wire [3:0] _GEN_44 = {{1'd0}, _readys_unready_T_27[3:1]}; // @[Arbiter.scala 25:58]
  wire [3:0] readys_unready_4 = _GEN_44 | _readys_unready_T_30; // @[Arbiter.scala 25:58]
  wire [1:0] _readys_readys_T_14 = readys_unready_4[3:2] & readys_unready_4[1:0]; // @[Arbiter.scala 26:39]
  wire [1:0] readys_readys_4 = ~_readys_readys_T_14; // @[Arbiter.scala 26:18]
  wire  readys_4_0 = readys_readys_4[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_4_0 = readys_4_0 & portsEOI_filtered_1_2_valid; // @[Arbiter.scala 97:79]
  reg  state_4_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_4_0 = idle_4 ? earlyWinner_4_0 : state_4_0; // @[Arbiter.scala 117:30]
  wire [4:0] _T_415 = muxStateEarly_4_0 ? auto_in_1_e_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire  readys_4_1 = readys_readys_4[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_4_1 = readys_4_1 & portsEOI_filtered_2_2_valid; // @[Arbiter.scala 97:79]
  reg  state_4_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_4_1 = idle_4 ? earlyWinner_4_1 : state_4_1; // @[Arbiter.scala 117:30]
  wire [4:0] _T_416 = muxStateEarly_4_1 ? auto_in_2_e_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] sink_ACancel_5_bits_sink = _T_415 | _T_416; // @[Mux.scala 27:72]
  wire [4:0] _GEN_45 = {{2'd0}, auto_out_3_d_bits_sink}; // @[Xbar.scala 323:53]
  wire [4:0] out_3_3_d_bits_sink = _GEN_45 | 5'h10; // @[Xbar.scala 323:53]
  reg  beatsLeft_7; // @[Arbiter.scala 87:30]
  wire  idle_7 = ~beatsLeft_7; // @[Arbiter.scala 88:28]
  wire  requestEIO_2_3 = auto_in_2_e_bits_sink[4:3] == 2'h2; // @[Parameters.scala 54:32]
  wire  portsEOI_filtered_2_3_valid = auto_in_2_e_valid & requestEIO_2_3; // @[Xbar.scala 179:40]
  wire  requestEIO_1_3 = auto_in_1_e_bits_sink[4:3] == 2'h2; // @[Parameters.scala 54:32]
  wire  portsEOI_filtered_1_3_valid = auto_in_1_e_valid & requestEIO_1_3; // @[Xbar.scala 179:40]
  wire [1:0] readys_filter_lo_7 = {portsEOI_filtered_2_3_valid,portsEOI_filtered_1_3_valid}; // @[Cat.scala 30:58]
  reg [1:0] readys_mask_7; // @[Arbiter.scala 23:23]
  wire [1:0] _readys_filter_T_7 = ~readys_mask_7; // @[Arbiter.scala 24:30]
  wire [1:0] readys_filter_hi_7 = readys_filter_lo_7 & _readys_filter_T_7; // @[Arbiter.scala 24:28]
  wire [3:0] readys_filter_7 = {readys_filter_hi_7,portsEOI_filtered_2_3_valid,portsEOI_filtered_1_3_valid}; // @[Cat.scala 30:58]
  wire [3:0] _GEN_46 = {{1'd0}, readys_filter_7[3:1]}; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_44 = readys_filter_7 | _GEN_46; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_47 = {readys_mask_7, 2'h0}; // @[Arbiter.scala 25:66]
  wire [3:0] _GEN_47 = {{1'd0}, _readys_unready_T_44[3:1]}; // @[Arbiter.scala 25:58]
  wire [3:0] readys_unready_7 = _GEN_47 | _readys_unready_T_47; // @[Arbiter.scala 25:58]
  wire [1:0] _readys_readys_T_23 = readys_unready_7[3:2] & readys_unready_7[1:0]; // @[Arbiter.scala 26:39]
  wire [1:0] readys_readys_7 = ~_readys_readys_T_23; // @[Arbiter.scala 26:18]
  wire  readys_7_0 = readys_readys_7[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_7_0 = readys_7_0 & portsEOI_filtered_1_3_valid; // @[Arbiter.scala 97:79]
  reg  state_7_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_7_0 = idle_7 ? earlyWinner_7_0 : state_7_0; // @[Arbiter.scala 117:30]
  wire [4:0] _T_615 = muxStateEarly_7_0 ? auto_in_1_e_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire  readys_7_1 = readys_readys_7[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_7_1 = readys_7_1 & portsEOI_filtered_2_3_valid; // @[Arbiter.scala 97:79]
  reg  state_7_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_7_1 = idle_7 ? earlyWinner_7_1 : state_7_1; // @[Arbiter.scala 117:30]
  wire [4:0] _T_616 = muxStateEarly_7_1 ? auto_in_2_e_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] sink_ACancel_7_bits_sink = _T_615 | _T_616; // @[Mux.scala 27:72]
  wire [3:0] _GEN_48 = {{1'd0}, auto_out_4_d_bits_sink}; // @[Xbar.scala 323:53]
  wire [3:0] _out_4_d_bits_sink_T = _GEN_48 | 4'h8; // @[Xbar.scala 323:53]
  reg  beatsLeft_10; // @[Arbiter.scala 87:30]
  wire  idle_10 = ~beatsLeft_10; // @[Arbiter.scala 88:28]
  wire  requestEIO_2_4 = auto_in_2_e_bits_sink[4:3] == 2'h1; // @[Parameters.scala 54:32]
  wire  portsEOI_filtered_2_4_valid = auto_in_2_e_valid & requestEIO_2_4; // @[Xbar.scala 179:40]
  wire  requestEIO_1_4 = auto_in_1_e_bits_sink[4:3] == 2'h1; // @[Parameters.scala 54:32]
  wire  portsEOI_filtered_1_4_valid = auto_in_1_e_valid & requestEIO_1_4; // @[Xbar.scala 179:40]
  wire [1:0] readys_filter_lo_10 = {portsEOI_filtered_2_4_valid,portsEOI_filtered_1_4_valid}; // @[Cat.scala 30:58]
  reg [1:0] readys_mask_10; // @[Arbiter.scala 23:23]
  wire [1:0] _readys_filter_T_10 = ~readys_mask_10; // @[Arbiter.scala 24:30]
  wire [1:0] readys_filter_hi_10 = readys_filter_lo_10 & _readys_filter_T_10; // @[Arbiter.scala 24:28]
  wire [3:0] readys_filter_10 = {readys_filter_hi_10,portsEOI_filtered_2_4_valid,portsEOI_filtered_1_4_valid}; // @[Cat.scala 30:58]
  wire [3:0] _GEN_49 = {{1'd0}, readys_filter_10[3:1]}; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_61 = readys_filter_10 | _GEN_49; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_64 = {readys_mask_10, 2'h0}; // @[Arbiter.scala 25:66]
  wire [3:0] _GEN_50 = {{1'd0}, _readys_unready_T_61[3:1]}; // @[Arbiter.scala 25:58]
  wire [3:0] readys_unready_10 = _GEN_50 | _readys_unready_T_64; // @[Arbiter.scala 25:58]
  wire [1:0] _readys_readys_T_32 = readys_unready_10[3:2] & readys_unready_10[1:0]; // @[Arbiter.scala 26:39]
  wire [1:0] readys_readys_10 = ~_readys_readys_T_32; // @[Arbiter.scala 26:18]
  wire  readys_10_0 = readys_readys_10[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_10_0 = readys_10_0 & portsEOI_filtered_1_4_valid; // @[Arbiter.scala 97:79]
  reg  state_10_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_10_0 = idle_10 ? earlyWinner_10_0 : state_10_0; // @[Arbiter.scala 117:30]
  wire [4:0] _T_815 = muxStateEarly_10_0 ? auto_in_1_e_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire  readys_10_1 = readys_readys_10[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_10_1 = readys_10_1 & portsEOI_filtered_2_4_valid; // @[Arbiter.scala 97:79]
  reg  state_10_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_10_1 = idle_10 ? earlyWinner_10_1 : state_10_1; // @[Arbiter.scala 117:30]
  wire [4:0] _T_816 = muxStateEarly_10_1 ? auto_in_2_e_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] sink_ACancel_9_bits_sink = _T_815 | _T_816; // @[Mux.scala 27:72]
  reg  beatsLeft_13; // @[Arbiter.scala 87:30]
  wire  idle_13 = ~beatsLeft_13; // @[Arbiter.scala 88:28]
  wire  requestEIO_2_5 = auto_in_2_e_bits_sink[4:3] == 2'h0; // @[Parameters.scala 54:32]
  wire  portsEOI_filtered_2_5_valid = auto_in_2_e_valid & requestEIO_2_5; // @[Xbar.scala 179:40]
  wire  requestEIO_1_5 = auto_in_1_e_bits_sink[4:3] == 2'h0; // @[Parameters.scala 54:32]
  wire  portsEOI_filtered_1_5_valid = auto_in_1_e_valid & requestEIO_1_5; // @[Xbar.scala 179:40]
  wire [1:0] readys_filter_lo_13 = {portsEOI_filtered_2_5_valid,portsEOI_filtered_1_5_valid}; // @[Cat.scala 30:58]
  reg [1:0] readys_mask_13; // @[Arbiter.scala 23:23]
  wire [1:0] _readys_filter_T_13 = ~readys_mask_13; // @[Arbiter.scala 24:30]
  wire [1:0] readys_filter_hi_13 = readys_filter_lo_13 & _readys_filter_T_13; // @[Arbiter.scala 24:28]
  wire [3:0] readys_filter_13 = {readys_filter_hi_13,portsEOI_filtered_2_5_valid,portsEOI_filtered_1_5_valid}; // @[Cat.scala 30:58]
  wire [3:0] _GEN_51 = {{1'd0}, readys_filter_13[3:1]}; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_78 = readys_filter_13 | _GEN_51; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_81 = {readys_mask_13, 2'h0}; // @[Arbiter.scala 25:66]
  wire [3:0] _GEN_52 = {{1'd0}, _readys_unready_T_78[3:1]}; // @[Arbiter.scala 25:58]
  wire [3:0] readys_unready_13 = _GEN_52 | _readys_unready_T_81; // @[Arbiter.scala 25:58]
  wire [1:0] _readys_readys_T_41 = readys_unready_13[3:2] & readys_unready_13[1:0]; // @[Arbiter.scala 26:39]
  wire [1:0] readys_readys_13 = ~_readys_readys_T_41; // @[Arbiter.scala 26:18]
  wire  readys_13_0 = readys_readys_13[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_13_0 = readys_13_0 & portsEOI_filtered_1_5_valid; // @[Arbiter.scala 97:79]
  reg  state_13_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_13_0 = idle_13 ? earlyWinner_13_0 : state_13_0; // @[Arbiter.scala 117:30]
  wire [4:0] _T_1015 = muxStateEarly_13_0 ? auto_in_1_e_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire  readys_13_1 = readys_readys_13[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_13_1 = readys_13_1 & portsEOI_filtered_2_5_valid; // @[Arbiter.scala 97:79]
  reg  state_13_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_13_1 = idle_13 ? earlyWinner_13_1 : state_13_1; // @[Arbiter.scala 117:30]
  wire [4:0] _T_1016 = muxStateEarly_13_1 ? auto_in_2_e_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] sink_ACancel_11_bits_sink = _T_1015 | _T_1016; // @[Mux.scala 27:72]
  wire [36:0] _requestAIO_T = auto_in_0_a_bits_address ^ 37'h8000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_1 = {1'b0,$signed(_requestAIO_T)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_3 = $signed(_requestAIO_T_1) & 38'sh189e000000; // @[Parameters.scala 137:52]
  wire  requestAIO_0_0 = $signed(_requestAIO_T_3) == 38'sh0; // @[Parameters.scala 137:67]
  wire [37:0] _requestAIO_T_6 = {1'b0,$signed(auto_in_0_a_bits_address)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_8 = $signed(_requestAIO_T_6) & 38'sh189c000000; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_9 = $signed(_requestAIO_T_8) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_10 = auto_in_0_a_bits_address ^ 37'hc000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_11 = {1'b0,$signed(_requestAIO_T_10)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_13 = $signed(_requestAIO_T_11) & 38'sh189c000000; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_14 = $signed(_requestAIO_T_13) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_15 = auto_in_0_a_bits_address ^ 37'h10000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_16 = {1'b0,$signed(_requestAIO_T_15)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_18 = $signed(_requestAIO_T_16) & 38'sh189e000000; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_19 = $signed(_requestAIO_T_18) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_20 = auto_in_0_a_bits_address ^ 37'h80000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_21 = {1'b0,$signed(_requestAIO_T_20)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_23 = $signed(_requestAIO_T_21) & 38'sh1880000000; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_24 = $signed(_requestAIO_T_23) == 38'sh0; // @[Parameters.scala 137:67]
  wire  requestAIO_0_1 = _requestAIO_T_9 | _requestAIO_T_14 | _requestAIO_T_19 | _requestAIO_T_24; // @[Xbar.scala 363:92]
  wire [36:0] _requestAIO_T_28 = auto_in_0_a_bits_address ^ 37'ha000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_29 = {1'b0,$signed(_requestAIO_T_28)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_31 = $signed(_requestAIO_T_29) & 38'sh189e0000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_32 = $signed(_requestAIO_T_31) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_33 = auto_in_0_a_bits_address ^ 37'h800000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_34 = {1'b0,$signed(_requestAIO_T_33)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_36 = $signed(_requestAIO_T_34) & 38'sh18000000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_37 = $signed(_requestAIO_T_36) == 38'sh0; // @[Parameters.scala 137:67]
  wire  requestAIO_0_2 = _requestAIO_T_32 | _requestAIO_T_37; // @[Xbar.scala 363:92]
  wire [36:0] _requestAIO_T_39 = auto_in_0_a_bits_address ^ 37'ha000040; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_40 = {1'b0,$signed(_requestAIO_T_39)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_42 = $signed(_requestAIO_T_40) & 38'sh189e0000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_43 = $signed(_requestAIO_T_42) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_44 = auto_in_0_a_bits_address ^ 37'h800000040; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_45 = {1'b0,$signed(_requestAIO_T_44)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_47 = $signed(_requestAIO_T_45) & 38'sh18000000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_48 = $signed(_requestAIO_T_47) == 38'sh0; // @[Parameters.scala 137:67]
  wire  requestAIO_0_3 = _requestAIO_T_43 | _requestAIO_T_48; // @[Xbar.scala 363:92]
  wire [36:0] _requestAIO_T_50 = auto_in_0_a_bits_address ^ 37'ha000080; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_51 = {1'b0,$signed(_requestAIO_T_50)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_53 = $signed(_requestAIO_T_51) & 38'sh189e0000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_54 = $signed(_requestAIO_T_53) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_55 = auto_in_0_a_bits_address ^ 37'h800000080; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_56 = {1'b0,$signed(_requestAIO_T_55)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_58 = $signed(_requestAIO_T_56) & 38'sh18000000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_59 = $signed(_requestAIO_T_58) == 38'sh0; // @[Parameters.scala 137:67]
  wire  requestAIO_0_4 = _requestAIO_T_54 | _requestAIO_T_59; // @[Xbar.scala 363:92]
  wire [36:0] _requestAIO_T_61 = auto_in_0_a_bits_address ^ 37'ha0000c0; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_62 = {1'b0,$signed(_requestAIO_T_61)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_64 = $signed(_requestAIO_T_62) & 38'sh189e0000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_65 = $signed(_requestAIO_T_64) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_66 = auto_in_0_a_bits_address ^ 37'h8000000c0; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_67 = {1'b0,$signed(_requestAIO_T_66)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_69 = $signed(_requestAIO_T_67) & 38'sh18000000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_70 = $signed(_requestAIO_T_69) == 38'sh0; // @[Parameters.scala 137:67]
  wire  requestAIO_0_5 = _requestAIO_T_65 | _requestAIO_T_70; // @[Xbar.scala 363:92]
  wire [36:0] _requestAIO_T_72 = auto_in_0_a_bits_address ^ 37'h1000000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_73 = {1'b0,$signed(_requestAIO_T_72)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_75 = $signed(_requestAIO_T_73) & 38'sh1000000000; // @[Parameters.scala 137:52]
  wire  requestAIO_0_6 = $signed(_requestAIO_T_75) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_77 = auto_in_1_a_bits_address ^ 37'h8000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_78 = {1'b0,$signed(_requestAIO_T_77)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_80 = $signed(_requestAIO_T_78) & 38'sh189e000000; // @[Parameters.scala 137:52]
  wire  requestAIO_1_0 = $signed(_requestAIO_T_80) == 38'sh0; // @[Parameters.scala 137:67]
  wire [37:0] _requestAIO_T_83 = {1'b0,$signed(auto_in_1_a_bits_address)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_85 = $signed(_requestAIO_T_83) & 38'sh189c000000; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_86 = $signed(_requestAIO_T_85) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_87 = auto_in_1_a_bits_address ^ 37'hc000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_88 = {1'b0,$signed(_requestAIO_T_87)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_90 = $signed(_requestAIO_T_88) & 38'sh189c000000; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_91 = $signed(_requestAIO_T_90) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_92 = auto_in_1_a_bits_address ^ 37'h10000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_93 = {1'b0,$signed(_requestAIO_T_92)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_95 = $signed(_requestAIO_T_93) & 38'sh189e000000; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_96 = $signed(_requestAIO_T_95) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_97 = auto_in_1_a_bits_address ^ 37'h80000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_98 = {1'b0,$signed(_requestAIO_T_97)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_100 = $signed(_requestAIO_T_98) & 38'sh1880000000; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_101 = $signed(_requestAIO_T_100) == 38'sh0; // @[Parameters.scala 137:67]
  wire  requestAIO_1_1 = _requestAIO_T_86 | _requestAIO_T_91 | _requestAIO_T_96 | _requestAIO_T_101; // @[Xbar.scala 363:92]
  wire [36:0] _requestAIO_T_105 = auto_in_1_a_bits_address ^ 37'ha000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_106 = {1'b0,$signed(_requestAIO_T_105)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_108 = $signed(_requestAIO_T_106) & 38'sh189e0000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_109 = $signed(_requestAIO_T_108) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_110 = auto_in_1_a_bits_address ^ 37'h800000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_111 = {1'b0,$signed(_requestAIO_T_110)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_113 = $signed(_requestAIO_T_111) & 38'sh18000000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_114 = $signed(_requestAIO_T_113) == 38'sh0; // @[Parameters.scala 137:67]
  wire  requestAIO_1_2 = _requestAIO_T_109 | _requestAIO_T_114; // @[Xbar.scala 363:92]
  wire [36:0] _requestAIO_T_116 = auto_in_1_a_bits_address ^ 37'ha000040; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_117 = {1'b0,$signed(_requestAIO_T_116)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_119 = $signed(_requestAIO_T_117) & 38'sh189e0000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_120 = $signed(_requestAIO_T_119) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_121 = auto_in_1_a_bits_address ^ 37'h800000040; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_122 = {1'b0,$signed(_requestAIO_T_121)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_124 = $signed(_requestAIO_T_122) & 38'sh18000000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_125 = $signed(_requestAIO_T_124) == 38'sh0; // @[Parameters.scala 137:67]
  wire  requestAIO_1_3 = _requestAIO_T_120 | _requestAIO_T_125; // @[Xbar.scala 363:92]
  wire [36:0] _requestAIO_T_127 = auto_in_1_a_bits_address ^ 37'ha000080; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_128 = {1'b0,$signed(_requestAIO_T_127)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_130 = $signed(_requestAIO_T_128) & 38'sh189e0000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_131 = $signed(_requestAIO_T_130) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_132 = auto_in_1_a_bits_address ^ 37'h800000080; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_133 = {1'b0,$signed(_requestAIO_T_132)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_135 = $signed(_requestAIO_T_133) & 38'sh18000000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_136 = $signed(_requestAIO_T_135) == 38'sh0; // @[Parameters.scala 137:67]
  wire  requestAIO_1_4 = _requestAIO_T_131 | _requestAIO_T_136; // @[Xbar.scala 363:92]
  wire [36:0] _requestAIO_T_138 = auto_in_1_a_bits_address ^ 37'ha0000c0; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_139 = {1'b0,$signed(_requestAIO_T_138)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_141 = $signed(_requestAIO_T_139) & 38'sh189e0000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_142 = $signed(_requestAIO_T_141) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_143 = auto_in_1_a_bits_address ^ 37'h8000000c0; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_144 = {1'b0,$signed(_requestAIO_T_143)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_146 = $signed(_requestAIO_T_144) & 38'sh18000000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_147 = $signed(_requestAIO_T_146) == 38'sh0; // @[Parameters.scala 137:67]
  wire  requestAIO_1_5 = _requestAIO_T_142 | _requestAIO_T_147; // @[Xbar.scala 363:92]
  wire [36:0] _requestAIO_T_149 = auto_in_1_a_bits_address ^ 37'h1000000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_150 = {1'b0,$signed(_requestAIO_T_149)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_152 = $signed(_requestAIO_T_150) & 38'sh1000000000; // @[Parameters.scala 137:52]
  wire  requestAIO_1_6 = $signed(_requestAIO_T_152) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_154 = auto_in_2_a_bits_address ^ 37'h8000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_155 = {1'b0,$signed(_requestAIO_T_154)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_157 = $signed(_requestAIO_T_155) & 38'sh189e000000; // @[Parameters.scala 137:52]
  wire  requestAIO_2_0 = $signed(_requestAIO_T_157) == 38'sh0; // @[Parameters.scala 137:67]
  wire [37:0] _requestAIO_T_160 = {1'b0,$signed(auto_in_2_a_bits_address)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_162 = $signed(_requestAIO_T_160) & 38'sh189c000000; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_163 = $signed(_requestAIO_T_162) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_164 = auto_in_2_a_bits_address ^ 37'hc000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_165 = {1'b0,$signed(_requestAIO_T_164)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_167 = $signed(_requestAIO_T_165) & 38'sh189c000000; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_168 = $signed(_requestAIO_T_167) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_169 = auto_in_2_a_bits_address ^ 37'h10000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_170 = {1'b0,$signed(_requestAIO_T_169)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_172 = $signed(_requestAIO_T_170) & 38'sh189e000000; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_173 = $signed(_requestAIO_T_172) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_174 = auto_in_2_a_bits_address ^ 37'h80000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_175 = {1'b0,$signed(_requestAIO_T_174)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_177 = $signed(_requestAIO_T_175) & 38'sh1880000000; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_178 = $signed(_requestAIO_T_177) == 38'sh0; // @[Parameters.scala 137:67]
  wire  requestAIO_2_1 = _requestAIO_T_163 | _requestAIO_T_168 | _requestAIO_T_173 | _requestAIO_T_178; // @[Xbar.scala 363:92]
  wire [36:0] _requestAIO_T_182 = auto_in_2_a_bits_address ^ 37'ha000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_183 = {1'b0,$signed(_requestAIO_T_182)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_185 = $signed(_requestAIO_T_183) & 38'sh189e0000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_186 = $signed(_requestAIO_T_185) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_187 = auto_in_2_a_bits_address ^ 37'h800000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_188 = {1'b0,$signed(_requestAIO_T_187)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_190 = $signed(_requestAIO_T_188) & 38'sh18000000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_191 = $signed(_requestAIO_T_190) == 38'sh0; // @[Parameters.scala 137:67]
  wire  requestAIO_2_2 = _requestAIO_T_186 | _requestAIO_T_191; // @[Xbar.scala 363:92]
  wire [36:0] _requestAIO_T_193 = auto_in_2_a_bits_address ^ 37'ha000040; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_194 = {1'b0,$signed(_requestAIO_T_193)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_196 = $signed(_requestAIO_T_194) & 38'sh189e0000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_197 = $signed(_requestAIO_T_196) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_198 = auto_in_2_a_bits_address ^ 37'h800000040; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_199 = {1'b0,$signed(_requestAIO_T_198)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_201 = $signed(_requestAIO_T_199) & 38'sh18000000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_202 = $signed(_requestAIO_T_201) == 38'sh0; // @[Parameters.scala 137:67]
  wire  requestAIO_2_3 = _requestAIO_T_197 | _requestAIO_T_202; // @[Xbar.scala 363:92]
  wire [36:0] _requestAIO_T_204 = auto_in_2_a_bits_address ^ 37'ha000080; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_205 = {1'b0,$signed(_requestAIO_T_204)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_207 = $signed(_requestAIO_T_205) & 38'sh189e0000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_208 = $signed(_requestAIO_T_207) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_209 = auto_in_2_a_bits_address ^ 37'h800000080; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_210 = {1'b0,$signed(_requestAIO_T_209)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_212 = $signed(_requestAIO_T_210) & 38'sh18000000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_213 = $signed(_requestAIO_T_212) == 38'sh0; // @[Parameters.scala 137:67]
  wire  requestAIO_2_4 = _requestAIO_T_208 | _requestAIO_T_213; // @[Xbar.scala 363:92]
  wire [36:0] _requestAIO_T_215 = auto_in_2_a_bits_address ^ 37'ha0000c0; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_216 = {1'b0,$signed(_requestAIO_T_215)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_218 = $signed(_requestAIO_T_216) & 38'sh189e0000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_219 = $signed(_requestAIO_T_218) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestAIO_T_220 = auto_in_2_a_bits_address ^ 37'h8000000c0; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_221 = {1'b0,$signed(_requestAIO_T_220)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_223 = $signed(_requestAIO_T_221) & 38'sh18000000c0; // @[Parameters.scala 137:52]
  wire  _requestAIO_T_224 = $signed(_requestAIO_T_223) == 38'sh0; // @[Parameters.scala 137:67]
  wire  requestAIO_2_5 = _requestAIO_T_219 | _requestAIO_T_224; // @[Xbar.scala 363:92]
  wire [36:0] _requestAIO_T_226 = auto_in_2_a_bits_address ^ 37'h1000000000; // @[Parameters.scala 137:31]
  wire [37:0] _requestAIO_T_227 = {1'b0,$signed(_requestAIO_T_226)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestAIO_T_229 = $signed(_requestAIO_T_227) & 38'sh1000000000; // @[Parameters.scala 137:52]
  wire  requestAIO_2_6 = $signed(_requestAIO_T_229) == 38'sh0; // @[Parameters.scala 137:67]
  wire [37:0] _requestCIO_T_36 = {1'b0,$signed(auto_in_1_c_bits_address)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestCIO_T_48 = $signed(_requestCIO_T_36) & 38'shc0; // @[Parameters.scala 137:52]
  wire  requestCIO_1_2 = $signed(_requestCIO_T_48) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestCIO_T_50 = auto_in_1_c_bits_address ^ 37'h40; // @[Parameters.scala 137:31]
  wire [37:0] _requestCIO_T_51 = {1'b0,$signed(_requestCIO_T_50)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestCIO_T_53 = $signed(_requestCIO_T_51) & 38'shc0; // @[Parameters.scala 137:52]
  wire  requestCIO_1_3 = $signed(_requestCIO_T_53) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestCIO_T_55 = auto_in_1_c_bits_address ^ 37'h80; // @[Parameters.scala 137:31]
  wire [37:0] _requestCIO_T_56 = {1'b0,$signed(_requestCIO_T_55)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestCIO_T_58 = $signed(_requestCIO_T_56) & 38'shc0; // @[Parameters.scala 137:52]
  wire  requestCIO_1_4 = $signed(_requestCIO_T_58) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestCIO_T_60 = auto_in_1_c_bits_address ^ 37'hc0; // @[Parameters.scala 137:31]
  wire [37:0] _requestCIO_T_61 = {1'b0,$signed(_requestCIO_T_60)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestCIO_T_63 = $signed(_requestCIO_T_61) & 38'shc0; // @[Parameters.scala 137:52]
  wire  requestCIO_1_5 = $signed(_requestCIO_T_63) == 38'sh0; // @[Parameters.scala 137:67]
  wire [37:0] _requestCIO_T_71 = {1'b0,$signed(auto_in_2_c_bits_address)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestCIO_T_83 = $signed(_requestCIO_T_71) & 38'shc0; // @[Parameters.scala 137:52]
  wire  requestCIO_2_2 = $signed(_requestCIO_T_83) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestCIO_T_85 = auto_in_2_c_bits_address ^ 37'h40; // @[Parameters.scala 137:31]
  wire [37:0] _requestCIO_T_86 = {1'b0,$signed(_requestCIO_T_85)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestCIO_T_88 = $signed(_requestCIO_T_86) & 38'shc0; // @[Parameters.scala 137:52]
  wire  requestCIO_2_3 = $signed(_requestCIO_T_88) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestCIO_T_90 = auto_in_2_c_bits_address ^ 37'h80; // @[Parameters.scala 137:31]
  wire [37:0] _requestCIO_T_91 = {1'b0,$signed(_requestCIO_T_90)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestCIO_T_93 = $signed(_requestCIO_T_91) & 38'shc0; // @[Parameters.scala 137:52]
  wire  requestCIO_2_4 = $signed(_requestCIO_T_93) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _requestCIO_T_95 = auto_in_2_c_bits_address ^ 37'hc0; // @[Parameters.scala 137:31]
  wire [37:0] _requestCIO_T_96 = {1'b0,$signed(_requestCIO_T_95)}; // @[Parameters.scala 137:49]
  wire [37:0] _requestCIO_T_98 = $signed(_requestCIO_T_96) & 38'shc0; // @[Parameters.scala 137:52]
  wire  requestCIO_2_5 = $signed(_requestCIO_T_98) == 38'sh0; // @[Parameters.scala 137:67]
  wire [22:0] _beatsAI_decode_T_1 = 23'hff << auto_in_0_a_bits_size; // @[package.scala 234:77]
  wire [7:0] _beatsAI_decode_T_3 = ~_beatsAI_decode_T_1[7:0]; // @[package.scala 234:46]
  wire [3:0] beatsAI_decode = _beatsAI_decode_T_3[7:4]; // @[Edges.scala 219:59]
  wire  beatsAI_opdata = ~auto_in_0_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [3:0] beatsAI_0 = beatsAI_opdata ? beatsAI_decode : 4'h0; // @[Edges.scala 220:14]
  wire [22:0] _beatsAI_decode_T_5 = 23'hff << auto_in_1_a_bits_size; // @[package.scala 234:77]
  wire [7:0] _beatsAI_decode_T_7 = ~_beatsAI_decode_T_5[7:0]; // @[package.scala 234:46]
  wire [3:0] beatsAI_decode_1 = _beatsAI_decode_T_7[7:4]; // @[Edges.scala 219:59]
  wire  beatsAI_opdata_1 = ~auto_in_1_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [3:0] beatsAI_1 = beatsAI_opdata_1 ? beatsAI_decode_1 : 4'h0; // @[Edges.scala 220:14]
  wire [22:0] _beatsAI_decode_T_9 = 23'hff << auto_in_2_a_bits_size; // @[package.scala 234:77]
  wire [7:0] _beatsAI_decode_T_11 = ~_beatsAI_decode_T_9[7:0]; // @[package.scala 234:46]
  wire [3:0] beatsAI_decode_2 = _beatsAI_decode_T_11[7:4]; // @[Edges.scala 219:59]
  wire  beatsAI_opdata_2 = ~auto_in_2_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [3:0] beatsAI_2 = beatsAI_opdata_2 ? beatsAI_decode_2 : 4'h0; // @[Edges.scala 220:14]
  wire [3:0] out_3_2_b_bits_size = {{1'd0}, auto_out_2_b_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [3:0] out_3_3_b_bits_size = {{1'd0}, auto_out_3_b_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [3:0] out_3_4_b_bits_size = {{1'd0}, auto_out_4_b_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [3:0] out_3_5_b_bits_size = {{1'd0}, auto_out_5_b_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [22:0] _beatsCI_decode_T_5 = 23'hff << auto_in_1_c_bits_size; // @[package.scala 234:77]
  wire [7:0] _beatsCI_decode_T_7 = ~_beatsCI_decode_T_5[7:0]; // @[package.scala 234:46]
  wire [3:0] beatsCI_decode_1 = _beatsCI_decode_T_7[7:4]; // @[Edges.scala 219:59]
  wire  beatsCI_opdata_1 = auto_in_1_c_bits_opcode[0]; // @[Edges.scala 101:36]
  wire [3:0] beatsCI_1 = beatsCI_opdata_1 ? beatsCI_decode_1 : 4'h0; // @[Edges.scala 220:14]
  wire [22:0] _beatsCI_decode_T_9 = 23'hff << auto_in_2_c_bits_size; // @[package.scala 234:77]
  wire [7:0] _beatsCI_decode_T_11 = ~_beatsCI_decode_T_9[7:0]; // @[package.scala 234:46]
  wire [3:0] beatsCI_decode_2 = _beatsCI_decode_T_11[7:4]; // @[Edges.scala 219:59]
  wire  beatsCI_opdata_2 = auto_in_2_c_bits_opcode[0]; // @[Edges.scala 101:36]
  wire [3:0] beatsCI_2 = beatsCI_opdata_2 ? beatsCI_decode_2 : 4'h0; // @[Edges.scala 220:14]
  wire [3:0] out_3_0_d_bits_size = {{1'd0}, auto_out_0_d_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [20:0] _beatsDO_decode_T_1 = 21'h3f << out_3_0_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_3 = ~_beatsDO_decode_T_1[5:0]; // @[package.scala 234:46]
  wire [1:0] beatsDO_decode = _beatsDO_decode_T_3[5:4]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata = auto_out_0_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [1:0] beatsDO_0 = beatsDO_opdata ? beatsDO_decode : 2'h0; // @[Edges.scala 220:14]
  wire [22:0] _beatsDO_decode_T_5 = 23'hff << auto_out_1_d_bits_size; // @[package.scala 234:77]
  wire [7:0] _beatsDO_decode_T_7 = ~_beatsDO_decode_T_5[7:0]; // @[package.scala 234:46]
  wire [3:0] beatsDO_decode_1 = _beatsDO_decode_T_7[7:4]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_1 = auto_out_1_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [3:0] beatsDO_1 = beatsDO_opdata_1 ? beatsDO_decode_1 : 4'h0; // @[Edges.scala 220:14]
  wire [3:0] out_3_2_d_bits_size = {{1'd0}, auto_out_2_d_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [20:0] _beatsDO_decode_T_9 = 21'h3f << out_3_2_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_11 = ~_beatsDO_decode_T_9[5:0]; // @[package.scala 234:46]
  wire [1:0] beatsDO_decode_2 = _beatsDO_decode_T_11[5:4]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_2 = auto_out_2_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [1:0] beatsDO_2 = beatsDO_opdata_2 ? beatsDO_decode_2 : 2'h0; // @[Edges.scala 220:14]
  wire [3:0] out_3_3_d_bits_size = {{1'd0}, auto_out_3_d_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [20:0] _beatsDO_decode_T_13 = 21'h3f << out_3_3_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_15 = ~_beatsDO_decode_T_13[5:0]; // @[package.scala 234:46]
  wire [1:0] beatsDO_decode_3 = _beatsDO_decode_T_15[5:4]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_3 = auto_out_3_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [1:0] beatsDO_3 = beatsDO_opdata_3 ? beatsDO_decode_3 : 2'h0; // @[Edges.scala 220:14]
  wire [3:0] out_3_4_d_bits_size = {{1'd0}, auto_out_4_d_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [20:0] _beatsDO_decode_T_17 = 21'h3f << out_3_4_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_19 = ~_beatsDO_decode_T_17[5:0]; // @[package.scala 234:46]
  wire [1:0] beatsDO_decode_4 = _beatsDO_decode_T_19[5:4]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_4 = auto_out_4_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [1:0] beatsDO_4 = beatsDO_opdata_4 ? beatsDO_decode_4 : 2'h0; // @[Edges.scala 220:14]
  wire [3:0] out_3_5_d_bits_size = {{1'd0}, auto_out_5_d_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [20:0] _beatsDO_decode_T_21 = 21'h3f << out_3_5_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_23 = ~_beatsDO_decode_T_21[5:0]; // @[package.scala 234:46]
  wire [1:0] beatsDO_decode_5 = _beatsDO_decode_T_23[5:4]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_5 = auto_out_5_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [1:0] beatsDO_5 = beatsDO_opdata_5 ? beatsDO_decode_5 : 2'h0; // @[Edges.scala 220:14]
  wire [3:0] out_3_6_d_bits_size = {{1'd0}, auto_out_6_d_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [21:0] _beatsDO_decode_T_25 = 22'h7f << out_3_6_d_bits_size; // @[package.scala 234:77]
  wire [6:0] _beatsDO_decode_T_27 = ~_beatsDO_decode_T_25[6:0]; // @[package.scala 234:46]
  wire [2:0] beatsDO_decode_6 = _beatsDO_decode_T_27[6:4]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_6 = auto_out_6_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [2:0] beatsDO_6 = beatsDO_opdata_6 ? beatsDO_decode_6 : 3'h0; // @[Edges.scala 220:14]
  wire  portsAOI_filtered__0_earlyValid = auto_in_0_a_valid & requestAIO_0_0; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered__1_earlyValid = auto_in_0_a_valid & requestAIO_0_1; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered__2_earlyValid = auto_in_0_a_valid & requestAIO_0_2; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered__3_earlyValid = auto_in_0_a_valid & requestAIO_0_3; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered__4_earlyValid = auto_in_0_a_valid & requestAIO_0_4; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered__5_earlyValid = auto_in_0_a_valid & requestAIO_0_5; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered__6_earlyValid = auto_in_0_a_valid & requestAIO_0_6; // @[Xbar.scala 428:50]
  reg [3:0] beatsLeft; // @[Arbiter.scala 87:30]
  wire  idle = beatsLeft == 4'h0; // @[Arbiter.scala 88:28]
  wire  portsAOI_filtered_2_0_earlyValid = auto_in_2_a_valid & requestAIO_2_0; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered_1_0_earlyValid = auto_in_1_a_valid & requestAIO_1_0; // @[Xbar.scala 428:50]
  wire [2:0] readys_filter_lo = {portsAOI_filtered_2_0_earlyValid,portsAOI_filtered_1_0_earlyValid,
    portsAOI_filtered__0_earlyValid}; // @[Cat.scala 30:58]
  reg [2:0] readys_mask; // @[Arbiter.scala 23:23]
  wire [2:0] _readys_filter_T = ~readys_mask; // @[Arbiter.scala 24:30]
  wire [2:0] readys_filter_hi = readys_filter_lo & _readys_filter_T; // @[Arbiter.scala 24:28]
  wire [5:0] readys_filter = {readys_filter_hi,portsAOI_filtered_2_0_earlyValid,portsAOI_filtered_1_0_earlyValid,
    portsAOI_filtered__0_earlyValid}; // @[Cat.scala 30:58]
  wire [5:0] _GEN_53 = {{1'd0}, readys_filter[5:1]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_1 = readys_filter | _GEN_53; // @[package.scala 253:43]
  wire [5:0] _GEN_54 = {{2'd0}, _readys_unready_T_1[5:2]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_3 = _readys_unready_T_1 | _GEN_54; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_6 = {readys_mask, 3'h0}; // @[Arbiter.scala 25:66]
  wire [5:0] _GEN_55 = {{1'd0}, _readys_unready_T_3[5:1]}; // @[Arbiter.scala 25:58]
  wire [5:0] readys_unready = _GEN_55 | _readys_unready_T_6; // @[Arbiter.scala 25:58]
  wire [2:0] _readys_readys_T_2 = readys_unready[5:3] & readys_unready[2:0]; // @[Arbiter.scala 26:39]
  wire [2:0] readys_readys = ~_readys_readys_T_2; // @[Arbiter.scala 26:18]
  wire  readys__0 = readys_readys[0]; // @[Arbiter.scala 95:86]
  reg  state__0; // @[Arbiter.scala 116:26]
  wire  allowed__0 = idle ? readys__0 : state__0; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered__0_ready = auto_out_0_a_ready & allowed__0; // @[Arbiter.scala 123:31]
  reg [3:0] beatsLeft_1; // @[Arbiter.scala 87:30]
  wire  idle_1 = beatsLeft_1 == 4'h0; // @[Arbiter.scala 88:28]
  wire  portsAOI_filtered_2_1_earlyValid = auto_in_2_a_valid & requestAIO_2_1; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered_1_1_earlyValid = auto_in_1_a_valid & requestAIO_1_1; // @[Xbar.scala 428:50]
  wire [2:0] readys_filter_lo_1 = {portsAOI_filtered_2_1_earlyValid,portsAOI_filtered_1_1_earlyValid,
    portsAOI_filtered__1_earlyValid}; // @[Cat.scala 30:58]
  reg [2:0] readys_mask_1; // @[Arbiter.scala 23:23]
  wire [2:0] _readys_filter_T_1 = ~readys_mask_1; // @[Arbiter.scala 24:30]
  wire [2:0] readys_filter_hi_1 = readys_filter_lo_1 & _readys_filter_T_1; // @[Arbiter.scala 24:28]
  wire [5:0] readys_filter_1 = {readys_filter_hi_1,portsAOI_filtered_2_1_earlyValid,portsAOI_filtered_1_1_earlyValid,
    portsAOI_filtered__1_earlyValid}; // @[Cat.scala 30:58]
  wire [5:0] _GEN_56 = {{1'd0}, readys_filter_1[5:1]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_8 = readys_filter_1 | _GEN_56; // @[package.scala 253:43]
  wire [5:0] _GEN_57 = {{2'd0}, _readys_unready_T_8[5:2]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_10 = _readys_unready_T_8 | _GEN_57; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_13 = {readys_mask_1, 3'h0}; // @[Arbiter.scala 25:66]
  wire [5:0] _GEN_58 = {{1'd0}, _readys_unready_T_10[5:1]}; // @[Arbiter.scala 25:58]
  wire [5:0] readys_unready_1 = _GEN_58 | _readys_unready_T_13; // @[Arbiter.scala 25:58]
  wire [2:0] _readys_readys_T_5 = readys_unready_1[5:3] & readys_unready_1[2:0]; // @[Arbiter.scala 26:39]
  wire [2:0] readys_readys_1 = ~_readys_readys_T_5; // @[Arbiter.scala 26:18]
  wire  readys_1_0 = readys_readys_1[0]; // @[Arbiter.scala 95:86]
  reg  state_1_0; // @[Arbiter.scala 116:26]
  wire  allowed_1_0 = idle_1 ? readys_1_0 : state_1_0; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered__1_ready = auto_out_1_a_ready & allowed_1_0; // @[Arbiter.scala 123:31]
  reg [3:0] beatsLeft_2; // @[Arbiter.scala 87:30]
  wire  idle_2 = beatsLeft_2 == 4'h0; // @[Arbiter.scala 88:28]
  wire  portsAOI_filtered_2_2_earlyValid = auto_in_2_a_valid & requestAIO_2_2; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered_1_2_earlyValid = auto_in_1_a_valid & requestAIO_1_2; // @[Xbar.scala 428:50]
  wire [2:0] readys_filter_lo_2 = {portsAOI_filtered_2_2_earlyValid,portsAOI_filtered_1_2_earlyValid,
    portsAOI_filtered__2_earlyValid}; // @[Cat.scala 30:58]
  reg [2:0] readys_mask_2; // @[Arbiter.scala 23:23]
  wire [2:0] _readys_filter_T_2 = ~readys_mask_2; // @[Arbiter.scala 24:30]
  wire [2:0] readys_filter_hi_2 = readys_filter_lo_2 & _readys_filter_T_2; // @[Arbiter.scala 24:28]
  wire [5:0] readys_filter_2 = {readys_filter_hi_2,portsAOI_filtered_2_2_earlyValid,portsAOI_filtered_1_2_earlyValid,
    portsAOI_filtered__2_earlyValid}; // @[Cat.scala 30:58]
  wire [5:0] _GEN_59 = {{1'd0}, readys_filter_2[5:1]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_15 = readys_filter_2 | _GEN_59; // @[package.scala 253:43]
  wire [5:0] _GEN_60 = {{2'd0}, _readys_unready_T_15[5:2]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_17 = _readys_unready_T_15 | _GEN_60; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_20 = {readys_mask_2, 3'h0}; // @[Arbiter.scala 25:66]
  wire [5:0] _GEN_61 = {{1'd0}, _readys_unready_T_17[5:1]}; // @[Arbiter.scala 25:58]
  wire [5:0] readys_unready_2 = _GEN_61 | _readys_unready_T_20; // @[Arbiter.scala 25:58]
  wire [2:0] _readys_readys_T_8 = readys_unready_2[5:3] & readys_unready_2[2:0]; // @[Arbiter.scala 26:39]
  wire [2:0] readys_readys_2 = ~_readys_readys_T_8; // @[Arbiter.scala 26:18]
  wire  readys_2_0 = readys_readys_2[0]; // @[Arbiter.scala 95:86]
  reg  state_2_0; // @[Arbiter.scala 116:26]
  wire  allowed_2_0 = idle_2 ? readys_2_0 : state_2_0; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered__2_ready = auto_out_2_a_ready & allowed_2_0; // @[Arbiter.scala 123:31]
  reg [3:0] beatsLeft_5; // @[Arbiter.scala 87:30]
  wire  idle_5 = beatsLeft_5 == 4'h0; // @[Arbiter.scala 88:28]
  wire  portsAOI_filtered_2_3_earlyValid = auto_in_2_a_valid & requestAIO_2_3; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered_1_3_earlyValid = auto_in_1_a_valid & requestAIO_1_3; // @[Xbar.scala 428:50]
  wire [2:0] readys_filter_lo_5 = {portsAOI_filtered_2_3_earlyValid,portsAOI_filtered_1_3_earlyValid,
    portsAOI_filtered__3_earlyValid}; // @[Cat.scala 30:58]
  reg [2:0] readys_mask_5; // @[Arbiter.scala 23:23]
  wire [2:0] _readys_filter_T_5 = ~readys_mask_5; // @[Arbiter.scala 24:30]
  wire [2:0] readys_filter_hi_5 = readys_filter_lo_5 & _readys_filter_T_5; // @[Arbiter.scala 24:28]
  wire [5:0] readys_filter_5 = {readys_filter_hi_5,portsAOI_filtered_2_3_earlyValid,portsAOI_filtered_1_3_earlyValid,
    portsAOI_filtered__3_earlyValid}; // @[Cat.scala 30:58]
  wire [5:0] _GEN_62 = {{1'd0}, readys_filter_5[5:1]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_32 = readys_filter_5 | _GEN_62; // @[package.scala 253:43]
  wire [5:0] _GEN_63 = {{2'd0}, _readys_unready_T_32[5:2]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_34 = _readys_unready_T_32 | _GEN_63; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_37 = {readys_mask_5, 3'h0}; // @[Arbiter.scala 25:66]
  wire [5:0] _GEN_64 = {{1'd0}, _readys_unready_T_34[5:1]}; // @[Arbiter.scala 25:58]
  wire [5:0] readys_unready_5 = _GEN_64 | _readys_unready_T_37; // @[Arbiter.scala 25:58]
  wire [2:0] _readys_readys_T_17 = readys_unready_5[5:3] & readys_unready_5[2:0]; // @[Arbiter.scala 26:39]
  wire [2:0] readys_readys_5 = ~_readys_readys_T_17; // @[Arbiter.scala 26:18]
  wire  readys_5_0 = readys_readys_5[0]; // @[Arbiter.scala 95:86]
  reg  state_5_0; // @[Arbiter.scala 116:26]
  wire  allowed_5_0 = idle_5 ? readys_5_0 : state_5_0; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered__3_ready = auto_out_3_a_ready & allowed_5_0; // @[Arbiter.scala 123:31]
  reg [3:0] beatsLeft_8; // @[Arbiter.scala 87:30]
  wire  idle_8 = beatsLeft_8 == 4'h0; // @[Arbiter.scala 88:28]
  wire  portsAOI_filtered_2_4_earlyValid = auto_in_2_a_valid & requestAIO_2_4; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered_1_4_earlyValid = auto_in_1_a_valid & requestAIO_1_4; // @[Xbar.scala 428:50]
  wire [2:0] readys_filter_lo_8 = {portsAOI_filtered_2_4_earlyValid,portsAOI_filtered_1_4_earlyValid,
    portsAOI_filtered__4_earlyValid}; // @[Cat.scala 30:58]
  reg [2:0] readys_mask_8; // @[Arbiter.scala 23:23]
  wire [2:0] _readys_filter_T_8 = ~readys_mask_8; // @[Arbiter.scala 24:30]
  wire [2:0] readys_filter_hi_8 = readys_filter_lo_8 & _readys_filter_T_8; // @[Arbiter.scala 24:28]
  wire [5:0] readys_filter_8 = {readys_filter_hi_8,portsAOI_filtered_2_4_earlyValid,portsAOI_filtered_1_4_earlyValid,
    portsAOI_filtered__4_earlyValid}; // @[Cat.scala 30:58]
  wire [5:0] _GEN_65 = {{1'd0}, readys_filter_8[5:1]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_49 = readys_filter_8 | _GEN_65; // @[package.scala 253:43]
  wire [5:0] _GEN_66 = {{2'd0}, _readys_unready_T_49[5:2]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_51 = _readys_unready_T_49 | _GEN_66; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_54 = {readys_mask_8, 3'h0}; // @[Arbiter.scala 25:66]
  wire [5:0] _GEN_67 = {{1'd0}, _readys_unready_T_51[5:1]}; // @[Arbiter.scala 25:58]
  wire [5:0] readys_unready_8 = _GEN_67 | _readys_unready_T_54; // @[Arbiter.scala 25:58]
  wire [2:0] _readys_readys_T_26 = readys_unready_8[5:3] & readys_unready_8[2:0]; // @[Arbiter.scala 26:39]
  wire [2:0] readys_readys_8 = ~_readys_readys_T_26; // @[Arbiter.scala 26:18]
  wire  readys_8_0 = readys_readys_8[0]; // @[Arbiter.scala 95:86]
  reg  state_8_0; // @[Arbiter.scala 116:26]
  wire  allowed_8_0 = idle_8 ? readys_8_0 : state_8_0; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered__4_ready = auto_out_4_a_ready & allowed_8_0; // @[Arbiter.scala 123:31]
  reg [3:0] beatsLeft_11; // @[Arbiter.scala 87:30]
  wire  idle_11 = beatsLeft_11 == 4'h0; // @[Arbiter.scala 88:28]
  wire  portsAOI_filtered_2_5_earlyValid = auto_in_2_a_valid & requestAIO_2_5; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered_1_5_earlyValid = auto_in_1_a_valid & requestAIO_1_5; // @[Xbar.scala 428:50]
  wire [2:0] readys_filter_lo_11 = {portsAOI_filtered_2_5_earlyValid,portsAOI_filtered_1_5_earlyValid,
    portsAOI_filtered__5_earlyValid}; // @[Cat.scala 30:58]
  reg [2:0] readys_mask_11; // @[Arbiter.scala 23:23]
  wire [2:0] _readys_filter_T_11 = ~readys_mask_11; // @[Arbiter.scala 24:30]
  wire [2:0] readys_filter_hi_11 = readys_filter_lo_11 & _readys_filter_T_11; // @[Arbiter.scala 24:28]
  wire [5:0] readys_filter_11 = {readys_filter_hi_11,portsAOI_filtered_2_5_earlyValid,portsAOI_filtered_1_5_earlyValid,
    portsAOI_filtered__5_earlyValid}; // @[Cat.scala 30:58]
  wire [5:0] _GEN_68 = {{1'd0}, readys_filter_11[5:1]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_66 = readys_filter_11 | _GEN_68; // @[package.scala 253:43]
  wire [5:0] _GEN_69 = {{2'd0}, _readys_unready_T_66[5:2]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_68 = _readys_unready_T_66 | _GEN_69; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_71 = {readys_mask_11, 3'h0}; // @[Arbiter.scala 25:66]
  wire [5:0] _GEN_70 = {{1'd0}, _readys_unready_T_68[5:1]}; // @[Arbiter.scala 25:58]
  wire [5:0] readys_unready_11 = _GEN_70 | _readys_unready_T_71; // @[Arbiter.scala 25:58]
  wire [2:0] _readys_readys_T_35 = readys_unready_11[5:3] & readys_unready_11[2:0]; // @[Arbiter.scala 26:39]
  wire [2:0] readys_readys_11 = ~_readys_readys_T_35; // @[Arbiter.scala 26:18]
  wire  readys_11_0 = readys_readys_11[0]; // @[Arbiter.scala 95:86]
  reg  state_11_0; // @[Arbiter.scala 116:26]
  wire  allowed_11_0 = idle_11 ? readys_11_0 : state_11_0; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered__5_ready = auto_out_5_a_ready & allowed_11_0; // @[Arbiter.scala 123:31]
  reg [3:0] beatsLeft_14; // @[Arbiter.scala 87:30]
  wire  idle_14 = beatsLeft_14 == 4'h0; // @[Arbiter.scala 88:28]
  wire  portsAOI_filtered_2_6_earlyValid = auto_in_2_a_valid & requestAIO_2_6; // @[Xbar.scala 428:50]
  wire  portsAOI_filtered_1_6_earlyValid = auto_in_1_a_valid & requestAIO_1_6; // @[Xbar.scala 428:50]
  wire [2:0] readys_filter_lo_14 = {portsAOI_filtered_2_6_earlyValid,portsAOI_filtered_1_6_earlyValid,
    portsAOI_filtered__6_earlyValid}; // @[Cat.scala 30:58]
  reg [2:0] readys_mask_14; // @[Arbiter.scala 23:23]
  wire [2:0] _readys_filter_T_14 = ~readys_mask_14; // @[Arbiter.scala 24:30]
  wire [2:0] readys_filter_hi_14 = readys_filter_lo_14 & _readys_filter_T_14; // @[Arbiter.scala 24:28]
  wire [5:0] readys_filter_14 = {readys_filter_hi_14,portsAOI_filtered_2_6_earlyValid,portsAOI_filtered_1_6_earlyValid,
    portsAOI_filtered__6_earlyValid}; // @[Cat.scala 30:58]
  wire [5:0] _GEN_71 = {{1'd0}, readys_filter_14[5:1]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_83 = readys_filter_14 | _GEN_71; // @[package.scala 253:43]
  wire [5:0] _GEN_72 = {{2'd0}, _readys_unready_T_83[5:2]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_85 = _readys_unready_T_83 | _GEN_72; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_88 = {readys_mask_14, 3'h0}; // @[Arbiter.scala 25:66]
  wire [5:0] _GEN_73 = {{1'd0}, _readys_unready_T_85[5:1]}; // @[Arbiter.scala 25:58]
  wire [5:0] readys_unready_14 = _GEN_73 | _readys_unready_T_88; // @[Arbiter.scala 25:58]
  wire [2:0] _readys_readys_T_44 = readys_unready_14[5:3] & readys_unready_14[2:0]; // @[Arbiter.scala 26:39]
  wire [2:0] readys_readys_14 = ~_readys_readys_T_44; // @[Arbiter.scala 26:18]
  wire  readys_14_0 = readys_readys_14[0]; // @[Arbiter.scala 95:86]
  reg  state_14_0; // @[Arbiter.scala 116:26]
  wire  allowed_14_0 = idle_14 ? readys_14_0 : state_14_0; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered__6_ready = auto_out_6_a_ready & allowed_14_0; // @[Arbiter.scala 123:31]
  wire  _portsAOI_in_0_a_ready_T_6 = requestAIO_0_6 & portsAOI_filtered__6_ready; // @[Mux.scala 27:72]
  wire  _portsAOI_in_0_a_ready_T_11 = requestAIO_0_0 & portsAOI_filtered__0_ready | requestAIO_0_1 &
    portsAOI_filtered__1_ready | requestAIO_0_2 & portsAOI_filtered__2_ready | requestAIO_0_3 &
    portsAOI_filtered__3_ready | requestAIO_0_4 & portsAOI_filtered__4_ready | requestAIO_0_5 &
    portsAOI_filtered__5_ready; // @[Mux.scala 27:72]
  wire  readys__1 = readys_readys[1]; // @[Arbiter.scala 95:86]
  reg  state__1; // @[Arbiter.scala 116:26]
  wire  allowed__1 = idle ? readys__1 : state__1; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_1_0_ready = auto_out_0_a_ready & allowed__1; // @[Arbiter.scala 123:31]
  wire  readys_1_1 = readys_readys_1[1]; // @[Arbiter.scala 95:86]
  reg  state_1_1; // @[Arbiter.scala 116:26]
  wire  allowed_1_1 = idle_1 ? readys_1_1 : state_1_1; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_1_1_ready = auto_out_1_a_ready & allowed_1_1; // @[Arbiter.scala 123:31]
  wire  readys_2_1 = readys_readys_2[1]; // @[Arbiter.scala 95:86]
  reg  state_2_1; // @[Arbiter.scala 116:26]
  wire  allowed_2_1 = idle_2 ? readys_2_1 : state_2_1; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_1_2_ready = auto_out_2_a_ready & allowed_2_1; // @[Arbiter.scala 123:31]
  wire  readys_5_1 = readys_readys_5[1]; // @[Arbiter.scala 95:86]
  reg  state_5_1; // @[Arbiter.scala 116:26]
  wire  allowed_5_1 = idle_5 ? readys_5_1 : state_5_1; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_1_3_ready = auto_out_3_a_ready & allowed_5_1; // @[Arbiter.scala 123:31]
  wire  readys_8_1 = readys_readys_8[1]; // @[Arbiter.scala 95:86]
  reg  state_8_1; // @[Arbiter.scala 116:26]
  wire  allowed_8_1 = idle_8 ? readys_8_1 : state_8_1; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_1_4_ready = auto_out_4_a_ready & allowed_8_1; // @[Arbiter.scala 123:31]
  wire  readys_11_1 = readys_readys_11[1]; // @[Arbiter.scala 95:86]
  reg  state_11_1; // @[Arbiter.scala 116:26]
  wire  allowed_11_1 = idle_11 ? readys_11_1 : state_11_1; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_1_5_ready = auto_out_5_a_ready & allowed_11_1; // @[Arbiter.scala 123:31]
  wire  readys_14_1 = readys_readys_14[1]; // @[Arbiter.scala 95:86]
  reg  state_14_1; // @[Arbiter.scala 116:26]
  wire  allowed_14_1 = idle_14 ? readys_14_1 : state_14_1; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_1_6_ready = auto_out_6_a_ready & allowed_14_1; // @[Arbiter.scala 123:31]
  wire  _portsAOI_in_1_a_ready_T_6 = requestAIO_1_6 & portsAOI_filtered_1_6_ready; // @[Mux.scala 27:72]
  wire  _portsAOI_in_1_a_ready_T_11 = requestAIO_1_0 & portsAOI_filtered_1_0_ready | requestAIO_1_1 &
    portsAOI_filtered_1_1_ready | requestAIO_1_2 & portsAOI_filtered_1_2_ready | requestAIO_1_3 &
    portsAOI_filtered_1_3_ready | requestAIO_1_4 & portsAOI_filtered_1_4_ready | requestAIO_1_5 &
    portsAOI_filtered_1_5_ready; // @[Mux.scala 27:72]
  wire  readys__2 = readys_readys[2]; // @[Arbiter.scala 95:86]
  reg  state__2; // @[Arbiter.scala 116:26]
  wire  allowed__2 = idle ? readys__2 : state__2; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_2_0_ready = auto_out_0_a_ready & allowed__2; // @[Arbiter.scala 123:31]
  wire  readys_1_2 = readys_readys_1[2]; // @[Arbiter.scala 95:86]
  reg  state_1_2; // @[Arbiter.scala 116:26]
  wire  allowed_1_2 = idle_1 ? readys_1_2 : state_1_2; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_2_1_ready = auto_out_1_a_ready & allowed_1_2; // @[Arbiter.scala 123:31]
  wire  readys_2_2 = readys_readys_2[2]; // @[Arbiter.scala 95:86]
  reg  state_2_2; // @[Arbiter.scala 116:26]
  wire  allowed_2_2 = idle_2 ? readys_2_2 : state_2_2; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_2_2_ready = auto_out_2_a_ready & allowed_2_2; // @[Arbiter.scala 123:31]
  wire  readys_5_2 = readys_readys_5[2]; // @[Arbiter.scala 95:86]
  reg  state_5_2; // @[Arbiter.scala 116:26]
  wire  allowed_5_2 = idle_5 ? readys_5_2 : state_5_2; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_2_3_ready = auto_out_3_a_ready & allowed_5_2; // @[Arbiter.scala 123:31]
  wire  readys_8_2 = readys_readys_8[2]; // @[Arbiter.scala 95:86]
  reg  state_8_2; // @[Arbiter.scala 116:26]
  wire  allowed_8_2 = idle_8 ? readys_8_2 : state_8_2; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_2_4_ready = auto_out_4_a_ready & allowed_8_2; // @[Arbiter.scala 123:31]
  wire  readys_11_2 = readys_readys_11[2]; // @[Arbiter.scala 95:86]
  reg  state_11_2; // @[Arbiter.scala 116:26]
  wire  allowed_11_2 = idle_11 ? readys_11_2 : state_11_2; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_2_5_ready = auto_out_5_a_ready & allowed_11_2; // @[Arbiter.scala 123:31]
  wire  readys_14_2 = readys_readys_14[2]; // @[Arbiter.scala 95:86]
  reg  state_14_2; // @[Arbiter.scala 116:26]
  wire  allowed_14_2 = idle_14 ? readys_14_2 : state_14_2; // @[Arbiter.scala 121:24]
  wire  portsAOI_filtered_2_6_ready = auto_out_6_a_ready & allowed_14_2; // @[Arbiter.scala 123:31]
  wire  _portsAOI_in_2_a_ready_T_6 = requestAIO_2_6 & portsAOI_filtered_2_6_ready; // @[Mux.scala 27:72]
  wire  _portsAOI_in_2_a_ready_T_11 = requestAIO_2_0 & portsAOI_filtered_2_0_ready | requestAIO_2_1 &
    portsAOI_filtered_2_1_ready | requestAIO_2_2 & portsAOI_filtered_2_2_ready | requestAIO_2_3 &
    portsAOI_filtered_2_3_ready | requestAIO_2_4 & portsAOI_filtered_2_4_ready | requestAIO_2_5 &
    portsAOI_filtered_2_5_ready; // @[Mux.scala 27:72]
  wire  allowed_16_0 = idle_16 ? readys_16_0 : state_16_0; // @[Arbiter.scala 121:24]
  wire  out_43_ready = auto_in_1_b_ready & allowed_16_0; // @[Arbiter.scala 123:31]
  wire  allowed_18_0 = idle_18 ? readys_18_0 : state_18_0; // @[Arbiter.scala 121:24]
  wire  out_56_ready = auto_in_2_b_ready & allowed_18_0; // @[Arbiter.scala 123:31]
  wire  allowed_16_1 = idle_16 ? readys_16_1 : state_16_1; // @[Arbiter.scala 121:24]
  wire  out_44_ready = auto_in_1_b_ready & allowed_16_1; // @[Arbiter.scala 123:31]
  wire  allowed_18_1 = idle_18 ? readys_18_1 : state_18_1; // @[Arbiter.scala 121:24]
  wire  out_57_ready = auto_in_2_b_ready & allowed_18_1; // @[Arbiter.scala 123:31]
  wire  allowed_16_2 = idle_16 ? readys_16_2 : state_16_2; // @[Arbiter.scala 121:24]
  wire  out_45_ready = auto_in_1_b_ready & allowed_16_2; // @[Arbiter.scala 123:31]
  wire  allowed_18_2 = idle_18 ? readys_18_2 : state_18_2; // @[Arbiter.scala 121:24]
  wire  out_58_ready = auto_in_2_b_ready & allowed_18_2; // @[Arbiter.scala 123:31]
  wire  allowed_16_3 = idle_16 ? readys_16_3 : state_16_3; // @[Arbiter.scala 121:24]
  wire  out_46_ready = auto_in_1_b_ready & allowed_16_3; // @[Arbiter.scala 123:31]
  wire  allowed_18_3 = idle_18 ? readys_18_3 : state_18_3; // @[Arbiter.scala 121:24]
  wire  out_59_ready = auto_in_2_b_ready & allowed_18_3; // @[Arbiter.scala 123:31]
  wire  portsCOI_filtered_1_2_valid = auto_in_1_c_valid & requestCIO_1_2; // @[Xbar.scala 179:40]
  wire  portsCOI_filtered_1_3_valid = auto_in_1_c_valid & requestCIO_1_3; // @[Xbar.scala 179:40]
  wire  portsCOI_filtered_1_4_valid = auto_in_1_c_valid & requestCIO_1_4; // @[Xbar.scala 179:40]
  wire  portsCOI_filtered_1_5_valid = auto_in_1_c_valid & requestCIO_1_5; // @[Xbar.scala 179:40]
  reg [3:0] beatsLeft_3; // @[Arbiter.scala 87:30]
  wire  idle_3 = beatsLeft_3 == 4'h0; // @[Arbiter.scala 88:28]
  wire  portsCOI_filtered_2_2_valid = auto_in_2_c_valid & requestCIO_2_2; // @[Xbar.scala 179:40]
  wire [1:0] readys_filter_lo_3 = {portsCOI_filtered_2_2_valid,portsCOI_filtered_1_2_valid}; // @[Cat.scala 30:58]
  reg [1:0] readys_mask_3; // @[Arbiter.scala 23:23]
  wire [1:0] _readys_filter_T_3 = ~readys_mask_3; // @[Arbiter.scala 24:30]
  wire [1:0] readys_filter_hi_3 = readys_filter_lo_3 & _readys_filter_T_3; // @[Arbiter.scala 24:28]
  wire [3:0] readys_filter_3 = {readys_filter_hi_3,portsCOI_filtered_2_2_valid,portsCOI_filtered_1_2_valid}; // @[Cat.scala 30:58]
  wire [3:0] _GEN_74 = {{1'd0}, readys_filter_3[3:1]}; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_22 = readys_filter_3 | _GEN_74; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_25 = {readys_mask_3, 2'h0}; // @[Arbiter.scala 25:66]
  wire [3:0] _GEN_75 = {{1'd0}, _readys_unready_T_22[3:1]}; // @[Arbiter.scala 25:58]
  wire [3:0] readys_unready_3 = _GEN_75 | _readys_unready_T_25; // @[Arbiter.scala 25:58]
  wire [1:0] _readys_readys_T_11 = readys_unready_3[3:2] & readys_unready_3[1:0]; // @[Arbiter.scala 26:39]
  wire [1:0] readys_readys_3 = ~_readys_readys_T_11; // @[Arbiter.scala 26:18]
  wire  readys_3_0 = readys_readys_3[0]; // @[Arbiter.scala 95:86]
  reg  state_3_0; // @[Arbiter.scala 116:26]
  wire  allowed_3_0 = idle_3 ? readys_3_0 : state_3_0; // @[Arbiter.scala 121:24]
  wire  out_8_ready = auto_out_2_c_ready & allowed_3_0; // @[Arbiter.scala 123:31]
  reg [3:0] beatsLeft_6; // @[Arbiter.scala 87:30]
  wire  idle_6 = beatsLeft_6 == 4'h0; // @[Arbiter.scala 88:28]
  wire  portsCOI_filtered_2_3_valid = auto_in_2_c_valid & requestCIO_2_3; // @[Xbar.scala 179:40]
  wire [1:0] readys_filter_lo_6 = {portsCOI_filtered_2_3_valid,portsCOI_filtered_1_3_valid}; // @[Cat.scala 30:58]
  reg [1:0] readys_mask_6; // @[Arbiter.scala 23:23]
  wire [1:0] _readys_filter_T_6 = ~readys_mask_6; // @[Arbiter.scala 24:30]
  wire [1:0] readys_filter_hi_6 = readys_filter_lo_6 & _readys_filter_T_6; // @[Arbiter.scala 24:28]
  wire [3:0] readys_filter_6 = {readys_filter_hi_6,portsCOI_filtered_2_3_valid,portsCOI_filtered_1_3_valid}; // @[Cat.scala 30:58]
  wire [3:0] _GEN_76 = {{1'd0}, readys_filter_6[3:1]}; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_39 = readys_filter_6 | _GEN_76; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_42 = {readys_mask_6, 2'h0}; // @[Arbiter.scala 25:66]
  wire [3:0] _GEN_77 = {{1'd0}, _readys_unready_T_39[3:1]}; // @[Arbiter.scala 25:58]
  wire [3:0] readys_unready_6 = _GEN_77 | _readys_unready_T_42; // @[Arbiter.scala 25:58]
  wire [1:0] _readys_readys_T_20 = readys_unready_6[3:2] & readys_unready_6[1:0]; // @[Arbiter.scala 26:39]
  wire [1:0] readys_readys_6 = ~_readys_readys_T_20; // @[Arbiter.scala 26:18]
  wire  readys_6_0 = readys_readys_6[0]; // @[Arbiter.scala 95:86]
  reg  state_6_0; // @[Arbiter.scala 116:26]
  wire  allowed_6_0 = idle_6 ? readys_6_0 : state_6_0; // @[Arbiter.scala 121:24]
  wire  out_14_ready = auto_out_3_c_ready & allowed_6_0; // @[Arbiter.scala 123:31]
  reg [3:0] beatsLeft_9; // @[Arbiter.scala 87:30]
  wire  idle_9 = beatsLeft_9 == 4'h0; // @[Arbiter.scala 88:28]
  wire  portsCOI_filtered_2_4_valid = auto_in_2_c_valid & requestCIO_2_4; // @[Xbar.scala 179:40]
  wire [1:0] readys_filter_lo_9 = {portsCOI_filtered_2_4_valid,portsCOI_filtered_1_4_valid}; // @[Cat.scala 30:58]
  reg [1:0] readys_mask_9; // @[Arbiter.scala 23:23]
  wire [1:0] _readys_filter_T_9 = ~readys_mask_9; // @[Arbiter.scala 24:30]
  wire [1:0] readys_filter_hi_9 = readys_filter_lo_9 & _readys_filter_T_9; // @[Arbiter.scala 24:28]
  wire [3:0] readys_filter_9 = {readys_filter_hi_9,portsCOI_filtered_2_4_valid,portsCOI_filtered_1_4_valid}; // @[Cat.scala 30:58]
  wire [3:0] _GEN_78 = {{1'd0}, readys_filter_9[3:1]}; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_56 = readys_filter_9 | _GEN_78; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_59 = {readys_mask_9, 2'h0}; // @[Arbiter.scala 25:66]
  wire [3:0] _GEN_79 = {{1'd0}, _readys_unready_T_56[3:1]}; // @[Arbiter.scala 25:58]
  wire [3:0] readys_unready_9 = _GEN_79 | _readys_unready_T_59; // @[Arbiter.scala 25:58]
  wire [1:0] _readys_readys_T_29 = readys_unready_9[3:2] & readys_unready_9[1:0]; // @[Arbiter.scala 26:39]
  wire [1:0] readys_readys_9 = ~_readys_readys_T_29; // @[Arbiter.scala 26:18]
  wire  readys_9_0 = readys_readys_9[0]; // @[Arbiter.scala 95:86]
  reg  state_9_0; // @[Arbiter.scala 116:26]
  wire  allowed_9_0 = idle_9 ? readys_9_0 : state_9_0; // @[Arbiter.scala 121:24]
  wire  out_20_ready = auto_out_4_c_ready & allowed_9_0; // @[Arbiter.scala 123:31]
  reg [3:0] beatsLeft_12; // @[Arbiter.scala 87:30]
  wire  idle_12 = beatsLeft_12 == 4'h0; // @[Arbiter.scala 88:28]
  wire  portsCOI_filtered_2_5_valid = auto_in_2_c_valid & requestCIO_2_5; // @[Xbar.scala 179:40]
  wire [1:0] readys_filter_lo_12 = {portsCOI_filtered_2_5_valid,portsCOI_filtered_1_5_valid}; // @[Cat.scala 30:58]
  reg [1:0] readys_mask_12; // @[Arbiter.scala 23:23]
  wire [1:0] _readys_filter_T_12 = ~readys_mask_12; // @[Arbiter.scala 24:30]
  wire [1:0] readys_filter_hi_12 = readys_filter_lo_12 & _readys_filter_T_12; // @[Arbiter.scala 24:28]
  wire [3:0] readys_filter_12 = {readys_filter_hi_12,portsCOI_filtered_2_5_valid,portsCOI_filtered_1_5_valid}; // @[Cat.scala 30:58]
  wire [3:0] _GEN_80 = {{1'd0}, readys_filter_12[3:1]}; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_73 = readys_filter_12 | _GEN_80; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_76 = {readys_mask_12, 2'h0}; // @[Arbiter.scala 25:66]
  wire [3:0] _GEN_81 = {{1'd0}, _readys_unready_T_73[3:1]}; // @[Arbiter.scala 25:58]
  wire [3:0] readys_unready_12 = _GEN_81 | _readys_unready_T_76; // @[Arbiter.scala 25:58]
  wire [1:0] _readys_readys_T_38 = readys_unready_12[3:2] & readys_unready_12[1:0]; // @[Arbiter.scala 26:39]
  wire [1:0] readys_readys_12 = ~_readys_readys_T_38; // @[Arbiter.scala 26:18]
  wire  readys_12_0 = readys_readys_12[0]; // @[Arbiter.scala 95:86]
  reg  state_12_0; // @[Arbiter.scala 116:26]
  wire  allowed_12_0 = idle_12 ? readys_12_0 : state_12_0; // @[Arbiter.scala 121:24]
  wire  out_26_ready = auto_out_5_c_ready & allowed_12_0; // @[Arbiter.scala 123:31]
  wire  _portsCOI_in_1_c_ready_T_5 = requestCIO_1_5 & out_26_ready; // @[Mux.scala 27:72]
  wire  _portsCOI_in_1_c_ready_T_10 = requestCIO_1_2 & out_8_ready | requestCIO_1_3 & out_14_ready | requestCIO_1_4 &
    out_20_ready; // @[Mux.scala 27:72]
  wire  readys_3_1 = readys_readys_3[1]; // @[Arbiter.scala 95:86]
  reg  state_3_1; // @[Arbiter.scala 116:26]
  wire  allowed_3_1 = idle_3 ? readys_3_1 : state_3_1; // @[Arbiter.scala 121:24]
  wire  out_9_ready = auto_out_2_c_ready & allowed_3_1; // @[Arbiter.scala 123:31]
  wire  readys_6_1 = readys_readys_6[1]; // @[Arbiter.scala 95:86]
  reg  state_6_1; // @[Arbiter.scala 116:26]
  wire  allowed_6_1 = idle_6 ? readys_6_1 : state_6_1; // @[Arbiter.scala 121:24]
  wire  out_15_ready = auto_out_3_c_ready & allowed_6_1; // @[Arbiter.scala 123:31]
  wire  readys_9_1 = readys_readys_9[1]; // @[Arbiter.scala 95:86]
  reg  state_9_1; // @[Arbiter.scala 116:26]
  wire  allowed_9_1 = idle_9 ? readys_9_1 : state_9_1; // @[Arbiter.scala 121:24]
  wire  out_21_ready = auto_out_4_c_ready & allowed_9_1; // @[Arbiter.scala 123:31]
  wire  readys_12_1 = readys_readys_12[1]; // @[Arbiter.scala 95:86]
  reg  state_12_1; // @[Arbiter.scala 116:26]
  wire  allowed_12_1 = idle_12 ? readys_12_1 : state_12_1; // @[Arbiter.scala 121:24]
  wire  out_27_ready = auto_out_5_c_ready & allowed_12_1; // @[Arbiter.scala 123:31]
  wire  _portsCOI_in_2_c_ready_T_5 = requestCIO_2_5 & out_27_ready; // @[Mux.scala 27:72]
  wire  _portsCOI_in_2_c_ready_T_10 = requestCIO_2_2 & out_9_ready | requestCIO_2_3 & out_15_ready | requestCIO_2_4 &
    out_21_ready; // @[Mux.scala 27:72]
  wire  allowed_15_0 = idle_15 ? readys_15_0 : state_15_0; // @[Arbiter.scala 121:24]
  wire  out_35_ready = auto_in_0_d_ready & allowed_15_0; // @[Arbiter.scala 123:31]
  wire  allowed_17_0 = idle_17 ? readys_17_0 : state_17_0; // @[Arbiter.scala 121:24]
  wire  out_48_ready = auto_in_1_d_ready & allowed_17_0; // @[Arbiter.scala 123:31]
  wire  allowed_19_0 = idle_19 ? readys_19_0 : state_19_0; // @[Arbiter.scala 121:24]
  wire  out_61_ready = auto_in_2_d_ready & allowed_19_0; // @[Arbiter.scala 123:31]
  wire  allowed_15_1 = idle_15 ? readys_15_1 : state_15_1; // @[Arbiter.scala 121:24]
  wire  out_36_ready = auto_in_0_d_ready & allowed_15_1; // @[Arbiter.scala 123:31]
  wire  allowed_17_1 = idle_17 ? readys_17_1 : state_17_1; // @[Arbiter.scala 121:24]
  wire  out_49_ready = auto_in_1_d_ready & allowed_17_1; // @[Arbiter.scala 123:31]
  wire  allowed_19_1 = idle_19 ? readys_19_1 : state_19_1; // @[Arbiter.scala 121:24]
  wire  out_62_ready = auto_in_2_d_ready & allowed_19_1; // @[Arbiter.scala 123:31]
  wire  allowed_15_2 = idle_15 ? readys_15_2 : state_15_2; // @[Arbiter.scala 121:24]
  wire  out_37_ready = auto_in_0_d_ready & allowed_15_2; // @[Arbiter.scala 123:31]
  wire  allowed_17_2 = idle_17 ? readys_17_2 : state_17_2; // @[Arbiter.scala 121:24]
  wire  out_50_ready = auto_in_1_d_ready & allowed_17_2; // @[Arbiter.scala 123:31]
  wire  allowed_19_2 = idle_19 ? readys_19_2 : state_19_2; // @[Arbiter.scala 121:24]
  wire  out_63_ready = auto_in_2_d_ready & allowed_19_2; // @[Arbiter.scala 123:31]
  wire  allowed_15_3 = idle_15 ? readys_15_3 : state_15_3; // @[Arbiter.scala 121:24]
  wire  out_38_ready = auto_in_0_d_ready & allowed_15_3; // @[Arbiter.scala 123:31]
  wire  allowed_17_3 = idle_17 ? readys_17_3 : state_17_3; // @[Arbiter.scala 121:24]
  wire  out_51_ready = auto_in_1_d_ready & allowed_17_3; // @[Arbiter.scala 123:31]
  wire  allowed_19_3 = idle_19 ? readys_19_3 : state_19_3; // @[Arbiter.scala 121:24]
  wire  out_64_ready = auto_in_2_d_ready & allowed_19_3; // @[Arbiter.scala 123:31]
  wire  allowed_15_4 = idle_15 ? readys_15_4 : state_15_4; // @[Arbiter.scala 121:24]
  wire  out_39_ready = auto_in_0_d_ready & allowed_15_4; // @[Arbiter.scala 123:31]
  wire  allowed_17_4 = idle_17 ? readys_17_4 : state_17_4; // @[Arbiter.scala 121:24]
  wire  out_52_ready = auto_in_1_d_ready & allowed_17_4; // @[Arbiter.scala 123:31]
  wire  allowed_19_4 = idle_19 ? readys_19_4 : state_19_4; // @[Arbiter.scala 121:24]
  wire  out_65_ready = auto_in_2_d_ready & allowed_19_4; // @[Arbiter.scala 123:31]
  wire  allowed_15_5 = idle_15 ? readys_15_5 : state_15_5; // @[Arbiter.scala 121:24]
  wire  out_40_ready = auto_in_0_d_ready & allowed_15_5; // @[Arbiter.scala 123:31]
  wire  allowed_17_5 = idle_17 ? readys_17_5 : state_17_5; // @[Arbiter.scala 121:24]
  wire  out_53_ready = auto_in_1_d_ready & allowed_17_5; // @[Arbiter.scala 123:31]
  wire  allowed_19_5 = idle_19 ? readys_19_5 : state_19_5; // @[Arbiter.scala 121:24]
  wire  out_66_ready = auto_in_2_d_ready & allowed_19_5; // @[Arbiter.scala 123:31]
  wire  allowed_15_6 = idle_15 ? readys_15_6 : state_15_6; // @[Arbiter.scala 121:24]
  wire  out_41_ready = auto_in_0_d_ready & allowed_15_6; // @[Arbiter.scala 123:31]
  wire  allowed_17_6 = idle_17 ? readys_17_6 : state_17_6; // @[Arbiter.scala 121:24]
  wire  out_54_ready = auto_in_1_d_ready & allowed_17_6; // @[Arbiter.scala 123:31]
  wire  allowed_19_6 = idle_19 ? readys_19_6 : state_19_6; // @[Arbiter.scala 121:24]
  wire  out_67_ready = auto_in_2_d_ready & allowed_19_6; // @[Arbiter.scala 123:31]
  wire  allowed_4_0 = idle_4 ? readys_4_0 : state_4_0; // @[Arbiter.scala 121:24]
  wire  out_11_ready = auto_out_2_e_ready & allowed_4_0; // @[Arbiter.scala 123:31]
  wire  allowed_7_0 = idle_7 ? readys_7_0 : state_7_0; // @[Arbiter.scala 121:24]
  wire  out_17_ready = auto_out_3_e_ready & allowed_7_0; // @[Arbiter.scala 123:31]
  wire  allowed_10_0 = idle_10 ? readys_10_0 : state_10_0; // @[Arbiter.scala 121:24]
  wire  out_23_ready = auto_out_4_e_ready & allowed_10_0; // @[Arbiter.scala 123:31]
  wire  allowed_13_0 = idle_13 ? readys_13_0 : state_13_0; // @[Arbiter.scala 121:24]
  wire  out_29_ready = auto_out_5_e_ready & allowed_13_0; // @[Arbiter.scala 123:31]
  wire  _portsEOI_in_1_e_ready_T_5 = requestEIO_1_5 & out_29_ready; // @[Mux.scala 27:72]
  wire  _portsEOI_in_1_e_ready_T_10 = requestEIO_1_2 & out_11_ready | requestEIO_1_3 & out_17_ready | requestEIO_1_4 &
    out_23_ready; // @[Mux.scala 27:72]
  wire  allowed_4_1 = idle_4 ? readys_4_1 : state_4_1; // @[Arbiter.scala 121:24]
  wire  out_12_ready = auto_out_2_e_ready & allowed_4_1; // @[Arbiter.scala 123:31]
  wire  allowed_7_1 = idle_7 ? readys_7_1 : state_7_1; // @[Arbiter.scala 121:24]
  wire  out_18_ready = auto_out_3_e_ready & allowed_7_1; // @[Arbiter.scala 123:31]
  wire  allowed_10_1 = idle_10 ? readys_10_1 : state_10_1; // @[Arbiter.scala 121:24]
  wire  out_24_ready = auto_out_4_e_ready & allowed_10_1; // @[Arbiter.scala 123:31]
  wire  allowed_13_1 = idle_13 ? readys_13_1 : state_13_1; // @[Arbiter.scala 121:24]
  wire  out_30_ready = auto_out_5_e_ready & allowed_13_1; // @[Arbiter.scala 123:31]
  wire  _portsEOI_in_2_e_ready_T_5 = requestEIO_2_5 & out_30_ready; // @[Mux.scala 27:72]
  wire  _portsEOI_in_2_e_ready_T_10 = requestEIO_2_2 & out_12_ready | requestEIO_2_3 & out_18_ready | requestEIO_2_4 &
    out_24_ready; // @[Mux.scala 27:72]
  wire  latch = idle & auto_out_0_a_ready; // @[Arbiter.scala 89:24]
  wire [2:0] _readys_mask_T = readys_readys & readys_filter_lo; // @[Arbiter.scala 28:29]
  wire [3:0] _readys_mask_T_1 = {_readys_mask_T, 1'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_3 = _readys_mask_T | _readys_mask_T_1[2:0]; // @[package.scala 244:43]
  wire [4:0] _readys_mask_T_4 = {_readys_mask_T_3, 2'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_6 = _readys_mask_T_3 | _readys_mask_T_4[2:0]; // @[package.scala 244:43]
  wire  earlyWinner__0 = readys__0 & portsAOI_filtered__0_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner__1 = readys__1 & portsAOI_filtered_1_0_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner__2 = readys__2 & portsAOI_filtered_2_0_earlyValid; // @[Arbiter.scala 97:79]
  wire  _T_19 = portsAOI_filtered__0_earlyValid | portsAOI_filtered_1_0_earlyValid | portsAOI_filtered_2_0_earlyValid; // @[Arbiter.scala 107:36]
  wire [3:0] maskedBeats_0 = earlyWinner__0 ? beatsAI_0 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_1 = earlyWinner__1 ? beatsAI_1 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_2 = earlyWinner__2 ? beatsAI_2 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] _initBeats_T = maskedBeats_0 | maskedBeats_1; // @[Arbiter.scala 112:44]
  wire [3:0] initBeats = _initBeats_T | maskedBeats_2; // @[Arbiter.scala 112:44]
  wire  muxStateEarly__0 = idle ? earlyWinner__0 : state__0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly__1 = idle ? earlyWinner__1 : state__1; // @[Arbiter.scala 117:30]
  wire  muxStateEarly__2 = idle ? earlyWinner__2 : state__2; // @[Arbiter.scala 117:30]
  wire  _out_0_a_earlyValid_T_6 = state__0 & portsAOI_filtered__0_earlyValid | state__1 &
    portsAOI_filtered_1_0_earlyValid | state__2 & portsAOI_filtered_2_0_earlyValid; // @[Mux.scala 27:72]
  wire  out_3_0_a_earlyValid = idle ? _T_19 : _out_0_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_2 = auto_out_0_a_ready & out_3_0_a_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [3:0] _GEN_82 = {{3'd0}, _beatsLeft_T_2}; // @[Arbiter.scala 113:52]
  wire [3:0] _beatsLeft_T_4 = beatsLeft - _GEN_82; // @[Arbiter.scala 113:52]
  wire [127:0] _T_41 = muxStateEarly__0 ? auto_in_0_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_42 = muxStateEarly__1 ? auto_in_1_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_43 = muxStateEarly__2 ? auto_in_2_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_44 = _T_41 | _T_42; // @[Mux.scala 27:72]
  wire [15:0] _T_46 = muxStateEarly__0 ? auto_in_0_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_47 = muxStateEarly__1 ? auto_in_1_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_48 = muxStateEarly__2 ? auto_in_2_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_49 = _T_46 | _T_47; // @[Mux.scala 27:72]
  wire [36:0] _T_86 = muxStateEarly__0 ? auto_in_0_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_87 = muxStateEarly__1 ? auto_in_1_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_88 = muxStateEarly__2 ? auto_in_2_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_89 = _T_86 | _T_87; // @[Mux.scala 27:72]
  wire [36:0] out_3_0_a_bits_address = _T_89 | _T_88; // @[Mux.scala 27:72]
  wire [6:0] in_0_a_bits_source = {{1'd0}, auto_in_0_a_bits_source}; // @[Xbar.scala 231:18 Xbar.scala 237:29]
  wire [6:0] _T_91 = muxStateEarly__0 ? in_0_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_92 = muxStateEarly__1 ? in_1_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_93 = muxStateEarly__2 ? in_2_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_94 = _T_91 | _T_92; // @[Mux.scala 27:72]
  wire [3:0] _T_96 = muxStateEarly__0 ? auto_in_0_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_97 = muxStateEarly__1 ? auto_in_1_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_98 = muxStateEarly__2 ? auto_in_2_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_99 = _T_96 | _T_97; // @[Mux.scala 27:72]
  wire [3:0] out_3_0_a_bits_size = _T_99 | _T_98; // @[Mux.scala 27:72]
  wire [2:0] _T_102 = muxStateEarly__1 ? auto_in_1_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_103 = muxStateEarly__2 ? auto_in_2_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_106 = muxStateEarly__0 ? auto_in_0_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_107 = muxStateEarly__1 ? auto_in_1_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_108 = muxStateEarly__2 ? auto_in_2_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_109 = _T_106 | _T_107; // @[Mux.scala 27:72]
  wire  latch_1 = idle_1 & auto_out_1_a_ready; // @[Arbiter.scala 89:24]
  wire [2:0] _readys_mask_T_8 = readys_readys_1 & readys_filter_lo_1; // @[Arbiter.scala 28:29]
  wire [3:0] _readys_mask_T_9 = {_readys_mask_T_8, 1'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_11 = _readys_mask_T_8 | _readys_mask_T_9[2:0]; // @[package.scala 244:43]
  wire [4:0] _readys_mask_T_12 = {_readys_mask_T_11, 2'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_14 = _readys_mask_T_11 | _readys_mask_T_12[2:0]; // @[package.scala 244:43]
  wire  earlyWinner_1_0 = readys_1_0 & portsAOI_filtered__1_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_1_1 = readys_1_1 & portsAOI_filtered_1_1_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_1_2 = readys_1_2 & portsAOI_filtered_2_1_earlyValid; // @[Arbiter.scala 97:79]
  wire  _T_126 = portsAOI_filtered__1_earlyValid | portsAOI_filtered_1_1_earlyValid | portsAOI_filtered_2_1_earlyValid; // @[Arbiter.scala 107:36]
  wire [3:0] maskedBeats_0_1 = earlyWinner_1_0 ? beatsAI_0 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_1_1 = earlyWinner_1_1 ? beatsAI_1 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_2_1 = earlyWinner_1_2 ? beatsAI_2 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] _initBeats_T_1 = maskedBeats_0_1 | maskedBeats_1_1; // @[Arbiter.scala 112:44]
  wire [3:0] initBeats_1 = _initBeats_T_1 | maskedBeats_2_1; // @[Arbiter.scala 112:44]
  wire  muxStateEarly_1_0 = idle_1 ? earlyWinner_1_0 : state_1_0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_1_1 = idle_1 ? earlyWinner_1_1 : state_1_1; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_1_2 = idle_1 ? earlyWinner_1_2 : state_1_2; // @[Arbiter.scala 117:30]
  wire  _out_1_a_earlyValid_T_6 = state_1_0 & portsAOI_filtered__1_earlyValid | state_1_1 &
    portsAOI_filtered_1_1_earlyValid | state_1_2 & portsAOI_filtered_2_1_earlyValid; // @[Mux.scala 27:72]
  wire  out_3_1_a_earlyValid = idle_1 ? _T_126 : _out_1_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_8 = auto_out_1_a_ready & out_3_1_a_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [3:0] _GEN_83 = {{3'd0}, _beatsLeft_T_8}; // @[Arbiter.scala 113:52]
  wire [3:0] _beatsLeft_T_10 = beatsLeft_1 - _GEN_83; // @[Arbiter.scala 113:52]
  wire [127:0] _T_148 = muxStateEarly_1_0 ? auto_in_0_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_149 = muxStateEarly_1_1 ? auto_in_1_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_150 = muxStateEarly_1_2 ? auto_in_2_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_151 = _T_148 | _T_149; // @[Mux.scala 27:72]
  wire [15:0] _T_153 = muxStateEarly_1_0 ? auto_in_0_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_154 = muxStateEarly_1_1 ? auto_in_1_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_155 = muxStateEarly_1_2 ? auto_in_2_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_156 = _T_153 | _T_154; // @[Mux.scala 27:72]
  wire [36:0] _T_193 = muxStateEarly_1_0 ? auto_in_0_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_194 = muxStateEarly_1_1 ? auto_in_1_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_195 = muxStateEarly_1_2 ? auto_in_2_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_196 = _T_193 | _T_194; // @[Mux.scala 27:72]
  wire [36:0] out_3_1_a_bits_address = _T_196 | _T_195; // @[Mux.scala 27:72]
  wire [6:0] _T_198 = muxStateEarly_1_0 ? in_0_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_199 = muxStateEarly_1_1 ? in_1_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_200 = muxStateEarly_1_2 ? in_2_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_201 = _T_198 | _T_199; // @[Mux.scala 27:72]
  wire [3:0] _T_203 = muxStateEarly_1_0 ? auto_in_0_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_204 = muxStateEarly_1_1 ? auto_in_1_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_205 = muxStateEarly_1_2 ? auto_in_2_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_206 = _T_203 | _T_204; // @[Mux.scala 27:72]
  wire [2:0] _T_209 = muxStateEarly_1_1 ? auto_in_1_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_210 = muxStateEarly_1_2 ? auto_in_2_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_213 = muxStateEarly_1_0 ? auto_in_0_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_214 = muxStateEarly_1_1 ? auto_in_1_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_215 = muxStateEarly_1_2 ? auto_in_2_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_216 = _T_213 | _T_214; // @[Mux.scala 27:72]
  wire  latch_2 = idle_2 & auto_out_2_a_ready; // @[Arbiter.scala 89:24]
  wire [2:0] _readys_mask_T_16 = readys_readys_2 & readys_filter_lo_2; // @[Arbiter.scala 28:29]
  wire [3:0] _readys_mask_T_17 = {_readys_mask_T_16, 1'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_19 = _readys_mask_T_16 | _readys_mask_T_17[2:0]; // @[package.scala 244:43]
  wire [4:0] _readys_mask_T_20 = {_readys_mask_T_19, 2'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_22 = _readys_mask_T_19 | _readys_mask_T_20[2:0]; // @[package.scala 244:43]
  wire  earlyWinner_2_0 = readys_2_0 & portsAOI_filtered__2_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_2_1 = readys_2_1 & portsAOI_filtered_1_2_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_2_2 = readys_2_2 & portsAOI_filtered_2_2_earlyValid; // @[Arbiter.scala 97:79]
  wire  _T_233 = portsAOI_filtered__2_earlyValid | portsAOI_filtered_1_2_earlyValid | portsAOI_filtered_2_2_earlyValid; // @[Arbiter.scala 107:36]
  wire [3:0] maskedBeats_0_2 = earlyWinner_2_0 ? beatsAI_0 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_1_2 = earlyWinner_2_1 ? beatsAI_1 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_2_2 = earlyWinner_2_2 ? beatsAI_2 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] _initBeats_T_2 = maskedBeats_0_2 | maskedBeats_1_2; // @[Arbiter.scala 112:44]
  wire [3:0] initBeats_2 = _initBeats_T_2 | maskedBeats_2_2; // @[Arbiter.scala 112:44]
  wire  muxStateEarly_2_0 = idle_2 ? earlyWinner_2_0 : state_2_0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_2_1 = idle_2 ? earlyWinner_2_1 : state_2_1; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_2_2 = idle_2 ? earlyWinner_2_2 : state_2_2; // @[Arbiter.scala 117:30]
  wire  _out_2_a_earlyValid_T_6 = state_2_0 & portsAOI_filtered__2_earlyValid | state_2_1 &
    portsAOI_filtered_1_2_earlyValid | state_2_2 & portsAOI_filtered_2_2_earlyValid; // @[Mux.scala 27:72]
  wire  out_3_2_a_earlyValid = idle_2 ? _T_233 : _out_2_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_14 = auto_out_2_a_ready & out_3_2_a_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [3:0] _GEN_84 = {{3'd0}, _beatsLeft_T_14}; // @[Arbiter.scala 113:52]
  wire [3:0] _beatsLeft_T_16 = beatsLeft_2 - _GEN_84; // @[Arbiter.scala 113:52]
  wire [127:0] _T_255 = muxStateEarly_2_0 ? auto_in_0_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_256 = muxStateEarly_2_1 ? auto_in_1_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_257 = muxStateEarly_2_2 ? auto_in_2_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_258 = _T_255 | _T_256; // @[Mux.scala 27:72]
  wire [15:0] _T_260 = muxStateEarly_2_0 ? auto_in_0_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_261 = muxStateEarly_2_1 ? auto_in_1_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_262 = muxStateEarly_2_2 ? auto_in_2_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_263 = _T_260 | _T_261; // @[Mux.scala 27:72]
  wire [36:0] _T_300 = muxStateEarly_2_0 ? auto_in_0_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_301 = muxStateEarly_2_1 ? auto_in_1_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_302 = muxStateEarly_2_2 ? auto_in_2_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_303 = _T_300 | _T_301; // @[Mux.scala 27:72]
  wire [36:0] out_3_2_a_bits_address = _T_303 | _T_302; // @[Mux.scala 27:72]
  wire [6:0] _T_305 = muxStateEarly_2_0 ? in_0_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_306 = muxStateEarly_2_1 ? in_1_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_307 = muxStateEarly_2_2 ? in_2_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_308 = _T_305 | _T_306; // @[Mux.scala 27:72]
  wire [3:0] _T_310 = muxStateEarly_2_0 ? auto_in_0_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_311 = muxStateEarly_2_1 ? auto_in_1_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_312 = muxStateEarly_2_2 ? auto_in_2_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_313 = _T_310 | _T_311; // @[Mux.scala 27:72]
  wire [3:0] out_3_2_a_bits_size = _T_313 | _T_312; // @[Mux.scala 27:72]
  wire [2:0] _T_316 = muxStateEarly_2_1 ? auto_in_1_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_317 = muxStateEarly_2_2 ? auto_in_2_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_320 = muxStateEarly_2_0 ? auto_in_0_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_321 = muxStateEarly_2_1 ? auto_in_1_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_322 = muxStateEarly_2_2 ? auto_in_2_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_323 = _T_320 | _T_321; // @[Mux.scala 27:72]
  wire  latch_3 = idle_3 & auto_out_2_c_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _readys_mask_T_24 = readys_readys_3 & readys_filter_lo_3; // @[Arbiter.scala 28:29]
  wire [2:0] _readys_mask_T_25 = {_readys_mask_T_24, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_mask_T_27 = _readys_mask_T_24 | _readys_mask_T_25[1:0]; // @[package.scala 244:43]
  wire  earlyWinner_3_0 = readys_3_0 & portsCOI_filtered_1_2_valid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_3_1 = readys_3_1 & portsCOI_filtered_2_2_valid; // @[Arbiter.scala 97:79]
  wire  _T_335 = portsCOI_filtered_1_2_valid | portsCOI_filtered_2_2_valid; // @[Arbiter.scala 107:36]
  wire [3:0] maskedBeats_0_3 = earlyWinner_3_0 ? beatsCI_1 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_1_3 = earlyWinner_3_1 ? beatsCI_2 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] initBeats_3 = maskedBeats_0_3 | maskedBeats_1_3; // @[Arbiter.scala 112:44]
  wire  muxStateEarly_3_0 = idle_3 ? earlyWinner_3_0 : state_3_0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_3_1 = idle_3 ? earlyWinner_3_1 : state_3_1; // @[Arbiter.scala 117:30]
  wire  _sink_ACancel_earlyValid_T_3 = state_3_0 & portsCOI_filtered_1_2_valid | state_3_1 & portsCOI_filtered_2_2_valid
    ; // @[Mux.scala 27:72]
  wire  sink_ACancel_4_earlyValid = idle_3 ? _T_335 : _sink_ACancel_earlyValid_T_3; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_20 = auto_out_2_c_ready & sink_ACancel_4_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [3:0] _GEN_85 = {{3'd0}, _beatsLeft_T_20}; // @[Arbiter.scala 113:52]
  wire [3:0] _beatsLeft_T_22 = beatsLeft_3 - _GEN_85; // @[Arbiter.scala 113:52]
  wire [127:0] _T_352 = muxStateEarly_3_0 ? auto_in_1_c_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_353 = muxStateEarly_3_1 ? auto_in_2_c_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_376 = muxStateEarly_3_0 ? auto_in_1_c_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_377 = muxStateEarly_3_1 ? auto_in_2_c_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] sink_ACancel_4_bits_address = _T_376 | _T_377; // @[Mux.scala 27:72]
  wire [6:0] _T_379 = muxStateEarly_3_0 ? in_1_c_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_380 = muxStateEarly_3_1 ? in_2_c_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_382 = muxStateEarly_3_0 ? auto_in_1_c_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_383 = muxStateEarly_3_1 ? auto_in_2_c_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] sink_ACancel_4_bits_size = _T_382 | _T_383; // @[Mux.scala 27:72]
  wire [2:0] _T_385 = muxStateEarly_3_0 ? auto_in_1_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_386 = muxStateEarly_3_1 ? auto_in_2_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_388 = muxStateEarly_3_0 ? auto_in_1_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_389 = muxStateEarly_3_1 ? auto_in_2_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire  latch_4 = idle_4 & auto_out_2_e_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _readys_mask_T_29 = readys_readys_4 & readys_filter_lo_4; // @[Arbiter.scala 28:29]
  wire [2:0] _readys_mask_T_30 = {_readys_mask_T_29, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_mask_T_32 = _readys_mask_T_29 | _readys_mask_T_30[1:0]; // @[package.scala 244:43]
  wire  _T_401 = portsEOI_filtered_1_2_valid | portsEOI_filtered_2_2_valid; // @[Arbiter.scala 107:36]
  wire  _sink_ACancel_earlyValid_T_8 = state_4_0 & portsEOI_filtered_1_2_valid | state_4_1 & portsEOI_filtered_2_2_valid
    ; // @[Mux.scala 27:72]
  wire  sink_ACancel_5_earlyValid = idle_4 ? _T_401 : _sink_ACancel_earlyValid_T_8; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_26 = auto_out_2_e_ready & sink_ACancel_5_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire  latch_5 = idle_5 & auto_out_3_a_ready; // @[Arbiter.scala 89:24]
  wire [2:0] _readys_mask_T_34 = readys_readys_5 & readys_filter_lo_5; // @[Arbiter.scala 28:29]
  wire [3:0] _readys_mask_T_35 = {_readys_mask_T_34, 1'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_37 = _readys_mask_T_34 | _readys_mask_T_35[2:0]; // @[package.scala 244:43]
  wire [4:0] _readys_mask_T_38 = {_readys_mask_T_37, 2'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_40 = _readys_mask_T_37 | _readys_mask_T_38[2:0]; // @[package.scala 244:43]
  wire  earlyWinner_5_0 = readys_5_0 & portsAOI_filtered__3_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_5_1 = readys_5_1 & portsAOI_filtered_1_3_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_5_2 = readys_5_2 & portsAOI_filtered_2_3_earlyValid; // @[Arbiter.scala 97:79]
  wire  _T_433 = portsAOI_filtered__3_earlyValid | portsAOI_filtered_1_3_earlyValid | portsAOI_filtered_2_3_earlyValid; // @[Arbiter.scala 107:36]
  wire [3:0] maskedBeats_0_5 = earlyWinner_5_0 ? beatsAI_0 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_1_5 = earlyWinner_5_1 ? beatsAI_1 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_2_3 = earlyWinner_5_2 ? beatsAI_2 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] _initBeats_T_3 = maskedBeats_0_5 | maskedBeats_1_5; // @[Arbiter.scala 112:44]
  wire [3:0] initBeats_5 = _initBeats_T_3 | maskedBeats_2_3; // @[Arbiter.scala 112:44]
  wire  muxStateEarly_5_0 = idle_5 ? earlyWinner_5_0 : state_5_0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_5_1 = idle_5 ? earlyWinner_5_1 : state_5_1; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_5_2 = idle_5 ? earlyWinner_5_2 : state_5_2; // @[Arbiter.scala 117:30]
  wire  _out_3_a_earlyValid_T_6 = state_5_0 & portsAOI_filtered__3_earlyValid | state_5_1 &
    portsAOI_filtered_1_3_earlyValid | state_5_2 & portsAOI_filtered_2_3_earlyValid; // @[Mux.scala 27:72]
  wire  out_3_3_a_earlyValid = idle_5 ? _T_433 : _out_3_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_32 = auto_out_3_a_ready & out_3_3_a_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [3:0] _GEN_86 = {{3'd0}, _beatsLeft_T_32}; // @[Arbiter.scala 113:52]
  wire [3:0] _beatsLeft_T_34 = beatsLeft_5 - _GEN_86; // @[Arbiter.scala 113:52]
  wire [127:0] _T_455 = muxStateEarly_5_0 ? auto_in_0_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_456 = muxStateEarly_5_1 ? auto_in_1_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_457 = muxStateEarly_5_2 ? auto_in_2_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_458 = _T_455 | _T_456; // @[Mux.scala 27:72]
  wire [15:0] _T_460 = muxStateEarly_5_0 ? auto_in_0_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_461 = muxStateEarly_5_1 ? auto_in_1_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_462 = muxStateEarly_5_2 ? auto_in_2_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_463 = _T_460 | _T_461; // @[Mux.scala 27:72]
  wire [36:0] _T_500 = muxStateEarly_5_0 ? auto_in_0_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_501 = muxStateEarly_5_1 ? auto_in_1_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_502 = muxStateEarly_5_2 ? auto_in_2_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_503 = _T_500 | _T_501; // @[Mux.scala 27:72]
  wire [36:0] out_3_3_a_bits_address = _T_503 | _T_502; // @[Mux.scala 27:72]
  wire [6:0] _T_505 = muxStateEarly_5_0 ? in_0_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_506 = muxStateEarly_5_1 ? in_1_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_507 = muxStateEarly_5_2 ? in_2_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_508 = _T_505 | _T_506; // @[Mux.scala 27:72]
  wire [3:0] _T_510 = muxStateEarly_5_0 ? auto_in_0_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_511 = muxStateEarly_5_1 ? auto_in_1_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_512 = muxStateEarly_5_2 ? auto_in_2_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_513 = _T_510 | _T_511; // @[Mux.scala 27:72]
  wire [3:0] out_3_3_a_bits_size = _T_513 | _T_512; // @[Mux.scala 27:72]
  wire [2:0] _T_516 = muxStateEarly_5_1 ? auto_in_1_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_517 = muxStateEarly_5_2 ? auto_in_2_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_520 = muxStateEarly_5_0 ? auto_in_0_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_521 = muxStateEarly_5_1 ? auto_in_1_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_522 = muxStateEarly_5_2 ? auto_in_2_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_523 = _T_520 | _T_521; // @[Mux.scala 27:72]
  wire  latch_6 = idle_6 & auto_out_3_c_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _readys_mask_T_42 = readys_readys_6 & readys_filter_lo_6; // @[Arbiter.scala 28:29]
  wire [2:0] _readys_mask_T_43 = {_readys_mask_T_42, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_mask_T_45 = _readys_mask_T_42 | _readys_mask_T_43[1:0]; // @[package.scala 244:43]
  wire  earlyWinner_6_0 = readys_6_0 & portsCOI_filtered_1_3_valid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_6_1 = readys_6_1 & portsCOI_filtered_2_3_valid; // @[Arbiter.scala 97:79]
  wire  _T_535 = portsCOI_filtered_1_3_valid | portsCOI_filtered_2_3_valid; // @[Arbiter.scala 107:36]
  wire [3:0] maskedBeats_0_6 = earlyWinner_6_0 ? beatsCI_1 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_1_6 = earlyWinner_6_1 ? beatsCI_2 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] initBeats_6 = maskedBeats_0_6 | maskedBeats_1_6; // @[Arbiter.scala 112:44]
  wire  muxStateEarly_6_0 = idle_6 ? earlyWinner_6_0 : state_6_0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_6_1 = idle_6 ? earlyWinner_6_1 : state_6_1; // @[Arbiter.scala 117:30]
  wire  _sink_ACancel_earlyValid_T_13 = state_6_0 & portsCOI_filtered_1_3_valid | state_6_1 &
    portsCOI_filtered_2_3_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_6_earlyValid = idle_6 ? _T_535 : _sink_ACancel_earlyValid_T_13; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_38 = auto_out_3_c_ready & sink_ACancel_6_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [3:0] _GEN_87 = {{3'd0}, _beatsLeft_T_38}; // @[Arbiter.scala 113:52]
  wire [3:0] _beatsLeft_T_40 = beatsLeft_6 - _GEN_87; // @[Arbiter.scala 113:52]
  wire [127:0] _T_552 = muxStateEarly_6_0 ? auto_in_1_c_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_553 = muxStateEarly_6_1 ? auto_in_2_c_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_576 = muxStateEarly_6_0 ? auto_in_1_c_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_577 = muxStateEarly_6_1 ? auto_in_2_c_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] sink_ACancel_6_bits_address = _T_576 | _T_577; // @[Mux.scala 27:72]
  wire [6:0] _T_579 = muxStateEarly_6_0 ? in_1_c_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_580 = muxStateEarly_6_1 ? in_2_c_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_582 = muxStateEarly_6_0 ? auto_in_1_c_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_583 = muxStateEarly_6_1 ? auto_in_2_c_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] sink_ACancel_6_bits_size = _T_582 | _T_583; // @[Mux.scala 27:72]
  wire [2:0] _T_585 = muxStateEarly_6_0 ? auto_in_1_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_586 = muxStateEarly_6_1 ? auto_in_2_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_588 = muxStateEarly_6_0 ? auto_in_1_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_589 = muxStateEarly_6_1 ? auto_in_2_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire  latch_7 = idle_7 & auto_out_3_e_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _readys_mask_T_47 = readys_readys_7 & readys_filter_lo_7; // @[Arbiter.scala 28:29]
  wire [2:0] _readys_mask_T_48 = {_readys_mask_T_47, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_mask_T_50 = _readys_mask_T_47 | _readys_mask_T_48[1:0]; // @[package.scala 244:43]
  wire  _T_601 = portsEOI_filtered_1_3_valid | portsEOI_filtered_2_3_valid; // @[Arbiter.scala 107:36]
  wire  _sink_ACancel_earlyValid_T_18 = state_7_0 & portsEOI_filtered_1_3_valid | state_7_1 &
    portsEOI_filtered_2_3_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_7_earlyValid = idle_7 ? _T_601 : _sink_ACancel_earlyValid_T_18; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_44 = auto_out_3_e_ready & sink_ACancel_7_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire  latch_8 = idle_8 & auto_out_4_a_ready; // @[Arbiter.scala 89:24]
  wire [2:0] _readys_mask_T_52 = readys_readys_8 & readys_filter_lo_8; // @[Arbiter.scala 28:29]
  wire [3:0] _readys_mask_T_53 = {_readys_mask_T_52, 1'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_55 = _readys_mask_T_52 | _readys_mask_T_53[2:0]; // @[package.scala 244:43]
  wire [4:0] _readys_mask_T_56 = {_readys_mask_T_55, 2'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_58 = _readys_mask_T_55 | _readys_mask_T_56[2:0]; // @[package.scala 244:43]
  wire  earlyWinner_8_0 = readys_8_0 & portsAOI_filtered__4_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_8_1 = readys_8_1 & portsAOI_filtered_1_4_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_8_2 = readys_8_2 & portsAOI_filtered_2_4_earlyValid; // @[Arbiter.scala 97:79]
  wire  _T_633 = portsAOI_filtered__4_earlyValid | portsAOI_filtered_1_4_earlyValid | portsAOI_filtered_2_4_earlyValid; // @[Arbiter.scala 107:36]
  wire [3:0] maskedBeats_0_8 = earlyWinner_8_0 ? beatsAI_0 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_1_8 = earlyWinner_8_1 ? beatsAI_1 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_2_4 = earlyWinner_8_2 ? beatsAI_2 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] _initBeats_T_4 = maskedBeats_0_8 | maskedBeats_1_8; // @[Arbiter.scala 112:44]
  wire [3:0] initBeats_8 = _initBeats_T_4 | maskedBeats_2_4; // @[Arbiter.scala 112:44]
  wire  muxStateEarly_8_0 = idle_8 ? earlyWinner_8_0 : state_8_0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_8_1 = idle_8 ? earlyWinner_8_1 : state_8_1; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_8_2 = idle_8 ? earlyWinner_8_2 : state_8_2; // @[Arbiter.scala 117:30]
  wire  _out_4_a_earlyValid_T_6 = state_8_0 & portsAOI_filtered__4_earlyValid | state_8_1 &
    portsAOI_filtered_1_4_earlyValid | state_8_2 & portsAOI_filtered_2_4_earlyValid; // @[Mux.scala 27:72]
  wire  out_3_4_a_earlyValid = idle_8 ? _T_633 : _out_4_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_50 = auto_out_4_a_ready & out_3_4_a_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [3:0] _GEN_88 = {{3'd0}, _beatsLeft_T_50}; // @[Arbiter.scala 113:52]
  wire [3:0] _beatsLeft_T_52 = beatsLeft_8 - _GEN_88; // @[Arbiter.scala 113:52]
  wire [127:0] _T_655 = muxStateEarly_8_0 ? auto_in_0_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_656 = muxStateEarly_8_1 ? auto_in_1_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_657 = muxStateEarly_8_2 ? auto_in_2_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_658 = _T_655 | _T_656; // @[Mux.scala 27:72]
  wire [15:0] _T_660 = muxStateEarly_8_0 ? auto_in_0_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_661 = muxStateEarly_8_1 ? auto_in_1_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_662 = muxStateEarly_8_2 ? auto_in_2_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_663 = _T_660 | _T_661; // @[Mux.scala 27:72]
  wire [36:0] _T_700 = muxStateEarly_8_0 ? auto_in_0_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_701 = muxStateEarly_8_1 ? auto_in_1_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_702 = muxStateEarly_8_2 ? auto_in_2_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_703 = _T_700 | _T_701; // @[Mux.scala 27:72]
  wire [36:0] out_3_4_a_bits_address = _T_703 | _T_702; // @[Mux.scala 27:72]
  wire [6:0] _T_705 = muxStateEarly_8_0 ? in_0_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_706 = muxStateEarly_8_1 ? in_1_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_707 = muxStateEarly_8_2 ? in_2_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_708 = _T_705 | _T_706; // @[Mux.scala 27:72]
  wire [3:0] _T_710 = muxStateEarly_8_0 ? auto_in_0_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_711 = muxStateEarly_8_1 ? auto_in_1_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_712 = muxStateEarly_8_2 ? auto_in_2_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_713 = _T_710 | _T_711; // @[Mux.scala 27:72]
  wire [3:0] out_3_4_a_bits_size = _T_713 | _T_712; // @[Mux.scala 27:72]
  wire [2:0] _T_716 = muxStateEarly_8_1 ? auto_in_1_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_717 = muxStateEarly_8_2 ? auto_in_2_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_720 = muxStateEarly_8_0 ? auto_in_0_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_721 = muxStateEarly_8_1 ? auto_in_1_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_722 = muxStateEarly_8_2 ? auto_in_2_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_723 = _T_720 | _T_721; // @[Mux.scala 27:72]
  wire  latch_9 = idle_9 & auto_out_4_c_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _readys_mask_T_60 = readys_readys_9 & readys_filter_lo_9; // @[Arbiter.scala 28:29]
  wire [2:0] _readys_mask_T_61 = {_readys_mask_T_60, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_mask_T_63 = _readys_mask_T_60 | _readys_mask_T_61[1:0]; // @[package.scala 244:43]
  wire  earlyWinner_9_0 = readys_9_0 & portsCOI_filtered_1_4_valid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_9_1 = readys_9_1 & portsCOI_filtered_2_4_valid; // @[Arbiter.scala 97:79]
  wire  _T_735 = portsCOI_filtered_1_4_valid | portsCOI_filtered_2_4_valid; // @[Arbiter.scala 107:36]
  wire [3:0] maskedBeats_0_9 = earlyWinner_9_0 ? beatsCI_1 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_1_9 = earlyWinner_9_1 ? beatsCI_2 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] initBeats_9 = maskedBeats_0_9 | maskedBeats_1_9; // @[Arbiter.scala 112:44]
  wire  muxStateEarly_9_0 = idle_9 ? earlyWinner_9_0 : state_9_0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_9_1 = idle_9 ? earlyWinner_9_1 : state_9_1; // @[Arbiter.scala 117:30]
  wire  _sink_ACancel_earlyValid_T_23 = state_9_0 & portsCOI_filtered_1_4_valid | state_9_1 &
    portsCOI_filtered_2_4_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_8_earlyValid = idle_9 ? _T_735 : _sink_ACancel_earlyValid_T_23; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_56 = auto_out_4_c_ready & sink_ACancel_8_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [3:0] _GEN_89 = {{3'd0}, _beatsLeft_T_56}; // @[Arbiter.scala 113:52]
  wire [3:0] _beatsLeft_T_58 = beatsLeft_9 - _GEN_89; // @[Arbiter.scala 113:52]
  wire [127:0] _T_752 = muxStateEarly_9_0 ? auto_in_1_c_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_753 = muxStateEarly_9_1 ? auto_in_2_c_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_776 = muxStateEarly_9_0 ? auto_in_1_c_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_777 = muxStateEarly_9_1 ? auto_in_2_c_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] sink_ACancel_8_bits_address = _T_776 | _T_777; // @[Mux.scala 27:72]
  wire [6:0] _T_779 = muxStateEarly_9_0 ? in_1_c_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_780 = muxStateEarly_9_1 ? in_2_c_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_782 = muxStateEarly_9_0 ? auto_in_1_c_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_783 = muxStateEarly_9_1 ? auto_in_2_c_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] sink_ACancel_8_bits_size = _T_782 | _T_783; // @[Mux.scala 27:72]
  wire [2:0] _T_785 = muxStateEarly_9_0 ? auto_in_1_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_786 = muxStateEarly_9_1 ? auto_in_2_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_788 = muxStateEarly_9_0 ? auto_in_1_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_789 = muxStateEarly_9_1 ? auto_in_2_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire  latch_10 = idle_10 & auto_out_4_e_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _readys_mask_T_65 = readys_readys_10 & readys_filter_lo_10; // @[Arbiter.scala 28:29]
  wire [2:0] _readys_mask_T_66 = {_readys_mask_T_65, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_mask_T_68 = _readys_mask_T_65 | _readys_mask_T_66[1:0]; // @[package.scala 244:43]
  wire  _T_801 = portsEOI_filtered_1_4_valid | portsEOI_filtered_2_4_valid; // @[Arbiter.scala 107:36]
  wire  _sink_ACancel_earlyValid_T_28 = state_10_0 & portsEOI_filtered_1_4_valid | state_10_1 &
    portsEOI_filtered_2_4_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_9_earlyValid = idle_10 ? _T_801 : _sink_ACancel_earlyValid_T_28; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_62 = auto_out_4_e_ready & sink_ACancel_9_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire  latch_11 = idle_11 & auto_out_5_a_ready; // @[Arbiter.scala 89:24]
  wire [2:0] _readys_mask_T_70 = readys_readys_11 & readys_filter_lo_11; // @[Arbiter.scala 28:29]
  wire [3:0] _readys_mask_T_71 = {_readys_mask_T_70, 1'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_73 = _readys_mask_T_70 | _readys_mask_T_71[2:0]; // @[package.scala 244:43]
  wire [4:0] _readys_mask_T_74 = {_readys_mask_T_73, 2'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_76 = _readys_mask_T_73 | _readys_mask_T_74[2:0]; // @[package.scala 244:43]
  wire  earlyWinner_11_0 = readys_11_0 & portsAOI_filtered__5_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_11_1 = readys_11_1 & portsAOI_filtered_1_5_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_11_2 = readys_11_2 & portsAOI_filtered_2_5_earlyValid; // @[Arbiter.scala 97:79]
  wire  _T_833 = portsAOI_filtered__5_earlyValid | portsAOI_filtered_1_5_earlyValid | portsAOI_filtered_2_5_earlyValid; // @[Arbiter.scala 107:36]
  wire [3:0] maskedBeats_0_11 = earlyWinner_11_0 ? beatsAI_0 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_1_11 = earlyWinner_11_1 ? beatsAI_1 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_2_5 = earlyWinner_11_2 ? beatsAI_2 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] _initBeats_T_5 = maskedBeats_0_11 | maskedBeats_1_11; // @[Arbiter.scala 112:44]
  wire [3:0] initBeats_11 = _initBeats_T_5 | maskedBeats_2_5; // @[Arbiter.scala 112:44]
  wire  muxStateEarly_11_0 = idle_11 ? earlyWinner_11_0 : state_11_0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_11_1 = idle_11 ? earlyWinner_11_1 : state_11_1; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_11_2 = idle_11 ? earlyWinner_11_2 : state_11_2; // @[Arbiter.scala 117:30]
  wire  _out_5_a_earlyValid_T_6 = state_11_0 & portsAOI_filtered__5_earlyValid | state_11_1 &
    portsAOI_filtered_1_5_earlyValid | state_11_2 & portsAOI_filtered_2_5_earlyValid; // @[Mux.scala 27:72]
  wire  out_3_5_a_earlyValid = idle_11 ? _T_833 : _out_5_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_68 = auto_out_5_a_ready & out_3_5_a_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [3:0] _GEN_90 = {{3'd0}, _beatsLeft_T_68}; // @[Arbiter.scala 113:52]
  wire [3:0] _beatsLeft_T_70 = beatsLeft_11 - _GEN_90; // @[Arbiter.scala 113:52]
  wire [127:0] _T_855 = muxStateEarly_11_0 ? auto_in_0_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_856 = muxStateEarly_11_1 ? auto_in_1_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_857 = muxStateEarly_11_2 ? auto_in_2_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_858 = _T_855 | _T_856; // @[Mux.scala 27:72]
  wire [15:0] _T_860 = muxStateEarly_11_0 ? auto_in_0_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_861 = muxStateEarly_11_1 ? auto_in_1_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_862 = muxStateEarly_11_2 ? auto_in_2_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_863 = _T_860 | _T_861; // @[Mux.scala 27:72]
  wire [36:0] _T_900 = muxStateEarly_11_0 ? auto_in_0_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_901 = muxStateEarly_11_1 ? auto_in_1_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_902 = muxStateEarly_11_2 ? auto_in_2_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_903 = _T_900 | _T_901; // @[Mux.scala 27:72]
  wire [36:0] out_3_5_a_bits_address = _T_903 | _T_902; // @[Mux.scala 27:72]
  wire [6:0] _T_905 = muxStateEarly_11_0 ? in_0_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_906 = muxStateEarly_11_1 ? in_1_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_907 = muxStateEarly_11_2 ? in_2_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_908 = _T_905 | _T_906; // @[Mux.scala 27:72]
  wire [3:0] _T_910 = muxStateEarly_11_0 ? auto_in_0_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_911 = muxStateEarly_11_1 ? auto_in_1_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_912 = muxStateEarly_11_2 ? auto_in_2_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_913 = _T_910 | _T_911; // @[Mux.scala 27:72]
  wire [3:0] out_3_5_a_bits_size = _T_913 | _T_912; // @[Mux.scala 27:72]
  wire [2:0] _T_916 = muxStateEarly_11_1 ? auto_in_1_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_917 = muxStateEarly_11_2 ? auto_in_2_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_920 = muxStateEarly_11_0 ? auto_in_0_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_921 = muxStateEarly_11_1 ? auto_in_1_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_922 = muxStateEarly_11_2 ? auto_in_2_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_923 = _T_920 | _T_921; // @[Mux.scala 27:72]
  wire  latch_12 = idle_12 & auto_out_5_c_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _readys_mask_T_78 = readys_readys_12 & readys_filter_lo_12; // @[Arbiter.scala 28:29]
  wire [2:0] _readys_mask_T_79 = {_readys_mask_T_78, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_mask_T_81 = _readys_mask_T_78 | _readys_mask_T_79[1:0]; // @[package.scala 244:43]
  wire  earlyWinner_12_0 = readys_12_0 & portsCOI_filtered_1_5_valid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_12_1 = readys_12_1 & portsCOI_filtered_2_5_valid; // @[Arbiter.scala 97:79]
  wire  _T_935 = portsCOI_filtered_1_5_valid | portsCOI_filtered_2_5_valid; // @[Arbiter.scala 107:36]
  wire [3:0] maskedBeats_0_12 = earlyWinner_12_0 ? beatsCI_1 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_1_12 = earlyWinner_12_1 ? beatsCI_2 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] initBeats_12 = maskedBeats_0_12 | maskedBeats_1_12; // @[Arbiter.scala 112:44]
  wire  muxStateEarly_12_0 = idle_12 ? earlyWinner_12_0 : state_12_0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_12_1 = idle_12 ? earlyWinner_12_1 : state_12_1; // @[Arbiter.scala 117:30]
  wire  _sink_ACancel_earlyValid_T_33 = state_12_0 & portsCOI_filtered_1_5_valid | state_12_1 &
    portsCOI_filtered_2_5_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_10_earlyValid = idle_12 ? _T_935 : _sink_ACancel_earlyValid_T_33; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_74 = auto_out_5_c_ready & sink_ACancel_10_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [3:0] _GEN_91 = {{3'd0}, _beatsLeft_T_74}; // @[Arbiter.scala 113:52]
  wire [3:0] _beatsLeft_T_76 = beatsLeft_12 - _GEN_91; // @[Arbiter.scala 113:52]
  wire [127:0] _T_952 = muxStateEarly_12_0 ? auto_in_1_c_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_953 = muxStateEarly_12_1 ? auto_in_2_c_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_976 = muxStateEarly_12_0 ? auto_in_1_c_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_977 = muxStateEarly_12_1 ? auto_in_2_c_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] sink_ACancel_10_bits_address = _T_976 | _T_977; // @[Mux.scala 27:72]
  wire [6:0] _T_979 = muxStateEarly_12_0 ? in_1_c_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_980 = muxStateEarly_12_1 ? in_2_c_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_982 = muxStateEarly_12_0 ? auto_in_1_c_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_983 = muxStateEarly_12_1 ? auto_in_2_c_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] sink_ACancel_10_bits_size = _T_982 | _T_983; // @[Mux.scala 27:72]
  wire [2:0] _T_985 = muxStateEarly_12_0 ? auto_in_1_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_986 = muxStateEarly_12_1 ? auto_in_2_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_988 = muxStateEarly_12_0 ? auto_in_1_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_989 = muxStateEarly_12_1 ? auto_in_2_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire  latch_13 = idle_13 & auto_out_5_e_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _readys_mask_T_83 = readys_readys_13 & readys_filter_lo_13; // @[Arbiter.scala 28:29]
  wire [2:0] _readys_mask_T_84 = {_readys_mask_T_83, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_mask_T_86 = _readys_mask_T_83 | _readys_mask_T_84[1:0]; // @[package.scala 244:43]
  wire  _T_1001 = portsEOI_filtered_1_5_valid | portsEOI_filtered_2_5_valid; // @[Arbiter.scala 107:36]
  wire  _sink_ACancel_earlyValid_T_38 = state_13_0 & portsEOI_filtered_1_5_valid | state_13_1 &
    portsEOI_filtered_2_5_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_11_earlyValid = idle_13 ? _T_1001 : _sink_ACancel_earlyValid_T_38; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_80 = auto_out_5_e_ready & sink_ACancel_11_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire  latch_14 = idle_14 & auto_out_6_a_ready; // @[Arbiter.scala 89:24]
  wire [2:0] _readys_mask_T_88 = readys_readys_14 & readys_filter_lo_14; // @[Arbiter.scala 28:29]
  wire [3:0] _readys_mask_T_89 = {_readys_mask_T_88, 1'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_91 = _readys_mask_T_88 | _readys_mask_T_89[2:0]; // @[package.scala 244:43]
  wire [4:0] _readys_mask_T_92 = {_readys_mask_T_91, 2'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_94 = _readys_mask_T_91 | _readys_mask_T_92[2:0]; // @[package.scala 244:43]
  wire  earlyWinner_14_0 = readys_14_0 & portsAOI_filtered__6_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_14_1 = readys_14_1 & portsAOI_filtered_1_6_earlyValid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_14_2 = readys_14_2 & portsAOI_filtered_2_6_earlyValid; // @[Arbiter.scala 97:79]
  wire  _T_1033 = portsAOI_filtered__6_earlyValid | portsAOI_filtered_1_6_earlyValid | portsAOI_filtered_2_6_earlyValid; // @[Arbiter.scala 107:36]
  wire [3:0] maskedBeats_0_14 = earlyWinner_14_0 ? beatsAI_0 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_1_14 = earlyWinner_14_1 ? beatsAI_1 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_2_6 = earlyWinner_14_2 ? beatsAI_2 : 4'h0; // @[Arbiter.scala 111:73]
  wire [3:0] _initBeats_T_6 = maskedBeats_0_14 | maskedBeats_1_14; // @[Arbiter.scala 112:44]
  wire [3:0] initBeats_14 = _initBeats_T_6 | maskedBeats_2_6; // @[Arbiter.scala 112:44]
  wire  muxStateEarly_14_0 = idle_14 ? earlyWinner_14_0 : state_14_0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_14_1 = idle_14 ? earlyWinner_14_1 : state_14_1; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_14_2 = idle_14 ? earlyWinner_14_2 : state_14_2; // @[Arbiter.scala 117:30]
  wire  _out_6_a_earlyValid_T_6 = state_14_0 & portsAOI_filtered__6_earlyValid | state_14_1 &
    portsAOI_filtered_1_6_earlyValid | state_14_2 & portsAOI_filtered_2_6_earlyValid; // @[Mux.scala 27:72]
  wire  out_3_6_a_earlyValid = idle_14 ? _T_1033 : _out_6_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_86 = auto_out_6_a_ready & out_3_6_a_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [3:0] _GEN_92 = {{3'd0}, _beatsLeft_T_86}; // @[Arbiter.scala 113:52]
  wire [3:0] _beatsLeft_T_88 = beatsLeft_14 - _GEN_92; // @[Arbiter.scala 113:52]
  wire [127:0] _T_1055 = muxStateEarly_14_0 ? auto_in_0_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1056 = muxStateEarly_14_1 ? auto_in_1_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1057 = muxStateEarly_14_2 ? auto_in_2_a_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1058 = _T_1055 | _T_1056; // @[Mux.scala 27:72]
  wire [15:0] _T_1060 = muxStateEarly_14_0 ? auto_in_0_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_1061 = muxStateEarly_14_1 ? auto_in_1_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_1062 = muxStateEarly_14_2 ? auto_in_2_a_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_1063 = _T_1060 | _T_1061; // @[Mux.scala 27:72]
  wire [36:0] _T_1100 = muxStateEarly_14_0 ? auto_in_0_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_1101 = muxStateEarly_14_1 ? auto_in_1_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_1102 = muxStateEarly_14_2 ? auto_in_2_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_1103 = _T_1100 | _T_1101; // @[Mux.scala 27:72]
  wire [6:0] _T_1105 = muxStateEarly_14_0 ? in_0_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1106 = muxStateEarly_14_1 ? in_1_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1107 = muxStateEarly_14_2 ? in_2_a_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_1108 = _T_1105 | _T_1106; // @[Mux.scala 27:72]
  wire [3:0] _T_1110 = muxStateEarly_14_0 ? auto_in_0_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1111 = muxStateEarly_14_1 ? auto_in_1_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1112 = muxStateEarly_14_2 ? auto_in_2_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1113 = _T_1110 | _T_1111; // @[Mux.scala 27:72]
  wire [3:0] out_3_6_a_bits_size = _T_1113 | _T_1112; // @[Mux.scala 27:72]
  wire [2:0] _T_1116 = muxStateEarly_14_1 ? auto_in_1_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1117 = muxStateEarly_14_2 ? auto_in_2_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1120 = muxStateEarly_14_0 ? auto_in_0_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1121 = muxStateEarly_14_1 ? auto_in_1_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1122 = muxStateEarly_14_2 ? auto_in_2_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1123 = _T_1120 | _T_1121; // @[Mux.scala 27:72]
  wire  latch_15 = idle_15 & auto_in_0_d_ready; // @[Arbiter.scala 89:24]
  wire [6:0] _readys_mask_T_96 = readys_readys_15 & readys_filter_lo_15; // @[Arbiter.scala 28:29]
  wire [7:0] _readys_mask_T_97 = {_readys_mask_T_96, 1'h0}; // @[package.scala 244:48]
  wire [6:0] _readys_mask_T_99 = _readys_mask_T_96 | _readys_mask_T_97[6:0]; // @[package.scala 244:43]
  wire [8:0] _readys_mask_T_100 = {_readys_mask_T_99, 2'h0}; // @[package.scala 244:48]
  wire [6:0] _readys_mask_T_102 = _readys_mask_T_99 | _readys_mask_T_100[6:0]; // @[package.scala 244:43]
  wire [10:0] _readys_mask_T_103 = {_readys_mask_T_102, 4'h0}; // @[package.scala 244:48]
  wire [6:0] _readys_mask_T_105 = _readys_mask_T_102 | _readys_mask_T_103[6:0]; // @[package.scala 244:43]
  wire  _T_1160 = portsDIO_filtered__0_valid | portsDIO_filtered_1_0_valid | portsDIO_filtered_2_0_valid |
    portsDIO_filtered_3_0_valid | portsDIO_filtered_4_0_valid | portsDIO_filtered_5_0_valid |
    portsDIO_filtered_6_0_valid; // @[Arbiter.scala 107:36]
  wire [1:0] maskedBeats_0_15 = earlyWinner_15_0 ? beatsDO_0 : 2'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_1_15 = earlyWinner_15_1 ? beatsDO_1 : 4'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_2_7 = earlyWinner_15_2 ? beatsDO_2 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_3 = earlyWinner_15_3 ? beatsDO_3 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_4 = earlyWinner_15_4 ? beatsDO_4 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_5 = earlyWinner_15_5 ? beatsDO_5 : 2'h0; // @[Arbiter.scala 111:73]
  wire [2:0] maskedBeats_6 = earlyWinner_15_6 ? beatsDO_6 : 3'h0; // @[Arbiter.scala 111:73]
  wire [3:0] _GEN_93 = {{2'd0}, maskedBeats_0_15}; // @[Arbiter.scala 112:44]
  wire [3:0] _initBeats_T_7 = _GEN_93 | maskedBeats_1_15; // @[Arbiter.scala 112:44]
  wire [3:0] _GEN_94 = {{2'd0}, maskedBeats_2_7}; // @[Arbiter.scala 112:44]
  wire [3:0] _initBeats_T_8 = _initBeats_T_7 | _GEN_94; // @[Arbiter.scala 112:44]
  wire [3:0] _GEN_95 = {{2'd0}, maskedBeats_3}; // @[Arbiter.scala 112:44]
  wire [3:0] _initBeats_T_9 = _initBeats_T_8 | _GEN_95; // @[Arbiter.scala 112:44]
  wire [3:0] _GEN_96 = {{2'd0}, maskedBeats_4}; // @[Arbiter.scala 112:44]
  wire [3:0] _initBeats_T_10 = _initBeats_T_9 | _GEN_96; // @[Arbiter.scala 112:44]
  wire [3:0] _GEN_97 = {{2'd0}, maskedBeats_5}; // @[Arbiter.scala 112:44]
  wire [3:0] _initBeats_T_11 = _initBeats_T_10 | _GEN_97; // @[Arbiter.scala 112:44]
  wire [3:0] _GEN_98 = {{1'd0}, maskedBeats_6}; // @[Arbiter.scala 112:44]
  wire [3:0] initBeats_15 = _initBeats_T_11 | _GEN_98; // @[Arbiter.scala 112:44]
  wire  _sink_ACancel_earlyValid_T_58 = state_15_0 & portsDIO_filtered__0_valid | state_15_1 &
    portsDIO_filtered_1_0_valid | state_15_2 & portsDIO_filtered_2_0_valid | state_15_3 & portsDIO_filtered_3_0_valid |
    state_15_4 & portsDIO_filtered_4_0_valid | state_15_5 & portsDIO_filtered_5_0_valid | state_15_6 &
    portsDIO_filtered_6_0_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_15_earlyValid = idle_15 ? _T_1160 : _sink_ACancel_earlyValid_T_58; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_92 = auto_in_0_d_ready & sink_ACancel_15_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [3:0] _GEN_99 = {{3'd0}, _beatsLeft_T_92}; // @[Arbiter.scala 113:52]
  wire [3:0] _beatsLeft_T_94 = beatsLeft_15 - _GEN_99; // @[Arbiter.scala 113:52]
  wire  _T_1195 = muxStateEarly_15_6 & auto_out_6_d_bits_corrupt; // @[Mux.scala 27:72]
  wire  _T_1200 = muxStateEarly_15_0 & auto_out_0_d_bits_corrupt | muxStateEarly_15_1 & auto_out_1_d_bits_corrupt |
    muxStateEarly_15_2 & auto_out_2_d_bits_corrupt | muxStateEarly_15_3 & auto_out_3_d_bits_corrupt | muxStateEarly_15_4
     & auto_out_4_d_bits_corrupt | muxStateEarly_15_5 & auto_out_5_d_bits_corrupt; // @[Mux.scala 27:72]
  wire [127:0] _T_1202 = muxStateEarly_15_0 ? auto_out_0_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1203 = muxStateEarly_15_1 ? auto_out_1_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1204 = muxStateEarly_15_2 ? auto_out_2_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1205 = muxStateEarly_15_3 ? auto_out_3_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1206 = muxStateEarly_15_4 ? auto_out_4_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1207 = muxStateEarly_15_5 ? auto_out_5_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1208 = muxStateEarly_15_6 ? auto_out_6_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1209 = _T_1202 | _T_1203; // @[Mux.scala 27:72]
  wire [127:0] _T_1210 = _T_1209 | _T_1204; // @[Mux.scala 27:72]
  wire [127:0] _T_1211 = _T_1210 | _T_1205; // @[Mux.scala 27:72]
  wire [127:0] _T_1212 = _T_1211 | _T_1206; // @[Mux.scala 27:72]
  wire [127:0] _T_1213 = _T_1212 | _T_1207; // @[Mux.scala 27:72]
  wire  _T_1221 = muxStateEarly_15_6 & auto_out_6_d_bits_denied; // @[Mux.scala 27:72]
  wire  _T_1226 = muxStateEarly_15_1 & auto_out_1_d_bits_denied | muxStateEarly_15_2 & auto_out_2_d_bits_denied |
    muxStateEarly_15_3 & auto_out_3_d_bits_denied | muxStateEarly_15_4 & auto_out_4_d_bits_denied | muxStateEarly_15_5
     & auto_out_5_d_bits_denied; // @[Mux.scala 27:72]
  wire [4:0] out_3_1_d_bits_sink = {{4'd0}, auto_out_1_d_bits_sink}; // @[Xbar.scala 288:19 Xbar.scala 323:28]
  wire [4:0] _T_1229 = muxStateEarly_15_1 ? out_3_1_d_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _T_1230 = muxStateEarly_15_2 ? out_3_2_d_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _T_1231 = muxStateEarly_15_3 ? out_3_3_d_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] out_3_4_d_bits_sink = {{1'd0}, _out_4_d_bits_sink_T}; // @[Xbar.scala 288:19 Xbar.scala 323:28]
  wire [4:0] _T_1232 = muxStateEarly_15_4 ? out_3_4_d_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] out_3_5_d_bits_sink = {{2'd0}, auto_out_5_d_bits_sink}; // @[Xbar.scala 288:19 Xbar.scala 323:28]
  wire [4:0] _T_1233 = muxStateEarly_15_5 ? out_3_5_d_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _T_1236 = _T_1229 | _T_1230; // @[Mux.scala 27:72]
  wire [4:0] _T_1237 = _T_1236 | _T_1231; // @[Mux.scala 27:72]
  wire [4:0] _T_1238 = _T_1237 | _T_1232; // @[Mux.scala 27:72]
  wire [3:0] _T_1254 = muxStateEarly_15_0 ? out_3_0_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1255 = muxStateEarly_15_1 ? auto_out_1_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1256 = muxStateEarly_15_2 ? out_3_2_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1257 = muxStateEarly_15_3 ? out_3_3_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1258 = muxStateEarly_15_4 ? out_3_4_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1259 = muxStateEarly_15_5 ? out_3_5_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1260 = muxStateEarly_15_6 ? out_3_6_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1261 = _T_1254 | _T_1255; // @[Mux.scala 27:72]
  wire [3:0] _T_1262 = _T_1261 | _T_1256; // @[Mux.scala 27:72]
  wire [3:0] _T_1263 = _T_1262 | _T_1257; // @[Mux.scala 27:72]
  wire [3:0] _T_1264 = _T_1263 | _T_1258; // @[Mux.scala 27:72]
  wire [3:0] _T_1265 = _T_1264 | _T_1259; // @[Mux.scala 27:72]
  wire [1:0] _T_1268 = muxStateEarly_15_1 ? auto_out_1_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1269 = muxStateEarly_15_2 ? auto_out_2_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1270 = muxStateEarly_15_3 ? auto_out_3_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1271 = muxStateEarly_15_4 ? auto_out_4_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1272 = muxStateEarly_15_5 ? auto_out_5_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1275 = _T_1268 | _T_1269; // @[Mux.scala 27:72]
  wire [1:0] _T_1276 = _T_1275 | _T_1270; // @[Mux.scala 27:72]
  wire [1:0] _T_1277 = _T_1276 | _T_1271; // @[Mux.scala 27:72]
  wire [2:0] _T_1280 = muxStateEarly_15_0 ? auto_out_0_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1281 = muxStateEarly_15_1 ? auto_out_1_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1282 = muxStateEarly_15_2 ? auto_out_2_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1283 = muxStateEarly_15_3 ? auto_out_3_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1284 = muxStateEarly_15_4 ? auto_out_4_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1285 = muxStateEarly_15_5 ? auto_out_5_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1286 = muxStateEarly_15_6 ? auto_out_6_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1287 = _T_1280 | _T_1281; // @[Mux.scala 27:72]
  wire [2:0] _T_1288 = _T_1287 | _T_1282; // @[Mux.scala 27:72]
  wire [2:0] _T_1289 = _T_1288 | _T_1283; // @[Mux.scala 27:72]
  wire [2:0] _T_1290 = _T_1289 | _T_1284; // @[Mux.scala 27:72]
  wire [2:0] _T_1291 = _T_1290 | _T_1285; // @[Mux.scala 27:72]
  wire  latch_16 = idle_16 & auto_in_1_b_ready; // @[Arbiter.scala 89:24]
  wire [3:0] _readys_mask_T_107 = readys_readys_16 & readys_filter_lo_16; // @[Arbiter.scala 28:29]
  wire [4:0] _readys_mask_T_108 = {_readys_mask_T_107, 1'h0}; // @[package.scala 244:48]
  wire [3:0] _readys_mask_T_110 = _readys_mask_T_107 | _readys_mask_T_108[3:0]; // @[package.scala 244:43]
  wire [5:0] _readys_mask_T_111 = {_readys_mask_T_110, 2'h0}; // @[package.scala 244:48]
  wire [3:0] _readys_mask_T_113 = _readys_mask_T_110 | _readys_mask_T_111[3:0]; // @[package.scala 244:43]
  wire  _T_1313 = portsBIO_filtered_2_1_valid | portsBIO_filtered_3_1_valid | portsBIO_filtered_4_1_valid |
    portsBIO_filtered_5_1_valid; // @[Arbiter.scala 107:36]
  wire  _sink_ACancel_earlyValid_T_69 = state_16_0 & portsBIO_filtered_2_1_valid | state_16_1 &
    portsBIO_filtered_3_1_valid | state_16_2 & portsBIO_filtered_4_1_valid | state_16_3 & portsBIO_filtered_5_1_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_16_earlyValid = idle_16 ? _T_1313 : _sink_ACancel_earlyValid_T_69; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_98 = auto_in_1_b_ready & sink_ACancel_16_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [1:0] _GEN_100 = {{1'd0}, _beatsLeft_T_98}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_100 = beatsLeft_16 - _GEN_100; // @[Arbiter.scala 113:52]
  wire  _T_1336 = muxStateEarly_16_3 & auto_out_5_b_bits_corrupt; // @[Mux.scala 27:72]
  wire  _T_1338 = muxStateEarly_16_0 & auto_out_2_b_bits_corrupt | muxStateEarly_16_1 & auto_out_3_b_bits_corrupt |
    muxStateEarly_16_2 & auto_out_4_b_bits_corrupt; // @[Mux.scala 27:72]
  wire [127:0] _T_1340 = muxStateEarly_16_0 ? auto_out_2_b_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1341 = muxStateEarly_16_1 ? auto_out_3_b_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1342 = muxStateEarly_16_2 ? auto_out_4_b_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1343 = muxStateEarly_16_3 ? auto_out_5_b_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1344 = _T_1340 | _T_1341; // @[Mux.scala 27:72]
  wire [127:0] _T_1345 = _T_1344 | _T_1342; // @[Mux.scala 27:72]
  wire [15:0] _T_1347 = muxStateEarly_16_0 ? auto_out_2_b_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_1348 = muxStateEarly_16_1 ? auto_out_3_b_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_1349 = muxStateEarly_16_2 ? auto_out_4_b_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_1350 = muxStateEarly_16_3 ? auto_out_5_b_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_1351 = _T_1347 | _T_1348; // @[Mux.scala 27:72]
  wire [15:0] _T_1352 = _T_1351 | _T_1349; // @[Mux.scala 27:72]
  wire [36:0] out_3_2_b_bits_address = {{1'd0}, auto_out_2_b_bits_address}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [36:0] _T_1354 = muxStateEarly_16_0 ? out_3_2_b_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] out_3_3_b_bits_address = {{1'd0}, auto_out_3_b_bits_address}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [36:0] _T_1355 = muxStateEarly_16_1 ? out_3_3_b_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] out_3_4_b_bits_address = {{1'd0}, auto_out_4_b_bits_address}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [36:0] _T_1356 = muxStateEarly_16_2 ? out_3_4_b_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] out_3_5_b_bits_address = {{1'd0}, auto_out_5_b_bits_address}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [36:0] _T_1357 = muxStateEarly_16_3 ? out_3_5_b_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_1358 = _T_1354 | _T_1355; // @[Mux.scala 27:72]
  wire [36:0] _T_1359 = _T_1358 | _T_1356; // @[Mux.scala 27:72]
  wire [3:0] _T_1368 = muxStateEarly_16_0 ? out_3_2_b_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1369 = muxStateEarly_16_1 ? out_3_3_b_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1370 = muxStateEarly_16_2 ? out_3_4_b_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1371 = muxStateEarly_16_3 ? out_3_5_b_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1372 = _T_1368 | _T_1369; // @[Mux.scala 27:72]
  wire [3:0] _T_1373 = _T_1372 | _T_1370; // @[Mux.scala 27:72]
  wire [1:0] _T_1375 = muxStateEarly_16_0 ? auto_out_2_b_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1376 = muxStateEarly_16_1 ? auto_out_3_b_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1377 = muxStateEarly_16_2 ? auto_out_4_b_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1378 = muxStateEarly_16_3 ? auto_out_5_b_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1379 = _T_1375 | _T_1376; // @[Mux.scala 27:72]
  wire [1:0] _T_1380 = _T_1379 | _T_1377; // @[Mux.scala 27:72]
  wire [2:0] _T_1382 = muxStateEarly_16_0 ? auto_out_2_b_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1383 = muxStateEarly_16_1 ? auto_out_3_b_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1384 = muxStateEarly_16_2 ? auto_out_4_b_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1385 = muxStateEarly_16_3 ? auto_out_5_b_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1386 = _T_1382 | _T_1383; // @[Mux.scala 27:72]
  wire [2:0] _T_1387 = _T_1386 | _T_1384; // @[Mux.scala 27:72]
  wire  latch_17 = idle_17 & auto_in_1_d_ready; // @[Arbiter.scala 89:24]
  wire [6:0] _readys_mask_T_115 = readys_readys_17 & readys_filter_lo_17; // @[Arbiter.scala 28:29]
  wire [7:0] _readys_mask_T_116 = {_readys_mask_T_115, 1'h0}; // @[package.scala 244:48]
  wire [6:0] _readys_mask_T_118 = _readys_mask_T_115 | _readys_mask_T_116[6:0]; // @[package.scala 244:43]
  wire [8:0] _readys_mask_T_119 = {_readys_mask_T_118, 2'h0}; // @[package.scala 244:48]
  wire [6:0] _readys_mask_T_121 = _readys_mask_T_118 | _readys_mask_T_119[6:0]; // @[package.scala 244:43]
  wire [10:0] _readys_mask_T_122 = {_readys_mask_T_121, 4'h0}; // @[package.scala 244:48]
  wire [6:0] _readys_mask_T_124 = _readys_mask_T_121 | _readys_mask_T_122[6:0]; // @[package.scala 244:43]
  wire  _T_1424 = portsDIO_filtered__1_valid | portsDIO_filtered_1_1_valid | portsDIO_filtered_2_1_valid |
    portsDIO_filtered_3_1_valid | portsDIO_filtered_4_1_valid | portsDIO_filtered_5_1_valid |
    portsDIO_filtered_6_1_valid; // @[Arbiter.scala 107:36]
  wire [1:0] maskedBeats_0_17 = earlyWinner_17_0 ? beatsDO_0 : 2'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_1_17 = earlyWinner_17_1 ? beatsDO_1 : 4'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_2_9 = earlyWinner_17_2 ? beatsDO_2 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_3_2 = earlyWinner_17_3 ? beatsDO_3 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_4_1 = earlyWinner_17_4 ? beatsDO_4 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_5_1 = earlyWinner_17_5 ? beatsDO_5 : 2'h0; // @[Arbiter.scala 111:73]
  wire [2:0] maskedBeats_6_1 = earlyWinner_17_6 ? beatsDO_6 : 3'h0; // @[Arbiter.scala 111:73]
  wire [3:0] _GEN_101 = {{2'd0}, maskedBeats_0_17}; // @[Arbiter.scala 112:44]
  wire [3:0] _initBeats_T_14 = _GEN_101 | maskedBeats_1_17; // @[Arbiter.scala 112:44]
  wire [3:0] _GEN_102 = {{2'd0}, maskedBeats_2_9}; // @[Arbiter.scala 112:44]
  wire [3:0] _initBeats_T_15 = _initBeats_T_14 | _GEN_102; // @[Arbiter.scala 112:44]
  wire [3:0] _GEN_103 = {{2'd0}, maskedBeats_3_2}; // @[Arbiter.scala 112:44]
  wire [3:0] _initBeats_T_16 = _initBeats_T_15 | _GEN_103; // @[Arbiter.scala 112:44]
  wire [3:0] _GEN_104 = {{2'd0}, maskedBeats_4_1}; // @[Arbiter.scala 112:44]
  wire [3:0] _initBeats_T_17 = _initBeats_T_16 | _GEN_104; // @[Arbiter.scala 112:44]
  wire [3:0] _GEN_105 = {{2'd0}, maskedBeats_5_1}; // @[Arbiter.scala 112:44]
  wire [3:0] _initBeats_T_18 = _initBeats_T_17 | _GEN_105; // @[Arbiter.scala 112:44]
  wire [3:0] _GEN_106 = {{1'd0}, maskedBeats_6_1}; // @[Arbiter.scala 112:44]
  wire [3:0] initBeats_17 = _initBeats_T_18 | _GEN_106; // @[Arbiter.scala 112:44]
  wire  _sink_ACancel_earlyValid_T_89 = state_17_0 & portsDIO_filtered__1_valid | state_17_1 &
    portsDIO_filtered_1_1_valid | state_17_2 & portsDIO_filtered_2_1_valid | state_17_3 & portsDIO_filtered_3_1_valid |
    state_17_4 & portsDIO_filtered_4_1_valid | state_17_5 & portsDIO_filtered_5_1_valid | state_17_6 &
    portsDIO_filtered_6_1_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_17_earlyValid = idle_17 ? _T_1424 : _sink_ACancel_earlyValid_T_89; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_104 = auto_in_1_d_ready & sink_ACancel_17_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [3:0] _GEN_107 = {{3'd0}, _beatsLeft_T_104}; // @[Arbiter.scala 113:52]
  wire [3:0] _beatsLeft_T_106 = beatsLeft_17 - _GEN_107; // @[Arbiter.scala 113:52]
  wire  _T_1459 = muxStateEarly_17_6 & auto_out_6_d_bits_corrupt; // @[Mux.scala 27:72]
  wire  _T_1464 = muxStateEarly_17_0 & auto_out_0_d_bits_corrupt | muxStateEarly_17_1 & auto_out_1_d_bits_corrupt |
    muxStateEarly_17_2 & auto_out_2_d_bits_corrupt | muxStateEarly_17_3 & auto_out_3_d_bits_corrupt | muxStateEarly_17_4
     & auto_out_4_d_bits_corrupt | muxStateEarly_17_5 & auto_out_5_d_bits_corrupt; // @[Mux.scala 27:72]
  wire [127:0] _T_1466 = muxStateEarly_17_0 ? auto_out_0_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1467 = muxStateEarly_17_1 ? auto_out_1_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1468 = muxStateEarly_17_2 ? auto_out_2_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1469 = muxStateEarly_17_3 ? auto_out_3_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1470 = muxStateEarly_17_4 ? auto_out_4_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1471 = muxStateEarly_17_5 ? auto_out_5_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1472 = muxStateEarly_17_6 ? auto_out_6_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1473 = _T_1466 | _T_1467; // @[Mux.scala 27:72]
  wire [127:0] _T_1474 = _T_1473 | _T_1468; // @[Mux.scala 27:72]
  wire [127:0] _T_1475 = _T_1474 | _T_1469; // @[Mux.scala 27:72]
  wire [127:0] _T_1476 = _T_1475 | _T_1470; // @[Mux.scala 27:72]
  wire [127:0] _T_1477 = _T_1476 | _T_1471; // @[Mux.scala 27:72]
  wire  _T_1485 = muxStateEarly_17_6 & auto_out_6_d_bits_denied; // @[Mux.scala 27:72]
  wire  _T_1490 = muxStateEarly_17_1 & auto_out_1_d_bits_denied | muxStateEarly_17_2 & auto_out_2_d_bits_denied |
    muxStateEarly_17_3 & auto_out_3_d_bits_denied | muxStateEarly_17_4 & auto_out_4_d_bits_denied | muxStateEarly_17_5
     & auto_out_5_d_bits_denied; // @[Mux.scala 27:72]
  wire [4:0] _T_1493 = muxStateEarly_17_1 ? out_3_1_d_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _T_1494 = muxStateEarly_17_2 ? out_3_2_d_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _T_1495 = muxStateEarly_17_3 ? out_3_3_d_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _T_1496 = muxStateEarly_17_4 ? out_3_4_d_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _T_1497 = muxStateEarly_17_5 ? out_3_5_d_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _T_1500 = _T_1493 | _T_1494; // @[Mux.scala 27:72]
  wire [4:0] _T_1501 = _T_1500 | _T_1495; // @[Mux.scala 27:72]
  wire [4:0] _T_1502 = _T_1501 | _T_1496; // @[Mux.scala 27:72]
  wire [3:0] _T_1518 = muxStateEarly_17_0 ? out_3_0_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1519 = muxStateEarly_17_1 ? auto_out_1_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1520 = muxStateEarly_17_2 ? out_3_2_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1521 = muxStateEarly_17_3 ? out_3_3_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1522 = muxStateEarly_17_4 ? out_3_4_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1523 = muxStateEarly_17_5 ? out_3_5_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1524 = muxStateEarly_17_6 ? out_3_6_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1525 = _T_1518 | _T_1519; // @[Mux.scala 27:72]
  wire [3:0] _T_1526 = _T_1525 | _T_1520; // @[Mux.scala 27:72]
  wire [3:0] _T_1527 = _T_1526 | _T_1521; // @[Mux.scala 27:72]
  wire [3:0] _T_1528 = _T_1527 | _T_1522; // @[Mux.scala 27:72]
  wire [3:0] _T_1529 = _T_1528 | _T_1523; // @[Mux.scala 27:72]
  wire [1:0] _T_1532 = muxStateEarly_17_1 ? auto_out_1_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1533 = muxStateEarly_17_2 ? auto_out_2_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1534 = muxStateEarly_17_3 ? auto_out_3_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1535 = muxStateEarly_17_4 ? auto_out_4_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1536 = muxStateEarly_17_5 ? auto_out_5_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1539 = _T_1532 | _T_1533; // @[Mux.scala 27:72]
  wire [1:0] _T_1540 = _T_1539 | _T_1534; // @[Mux.scala 27:72]
  wire [1:0] _T_1541 = _T_1540 | _T_1535; // @[Mux.scala 27:72]
  wire [2:0] _T_1544 = muxStateEarly_17_0 ? auto_out_0_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1545 = muxStateEarly_17_1 ? auto_out_1_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1546 = muxStateEarly_17_2 ? auto_out_2_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1547 = muxStateEarly_17_3 ? auto_out_3_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1548 = muxStateEarly_17_4 ? auto_out_4_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1549 = muxStateEarly_17_5 ? auto_out_5_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1550 = muxStateEarly_17_6 ? auto_out_6_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1551 = _T_1544 | _T_1545; // @[Mux.scala 27:72]
  wire [2:0] _T_1552 = _T_1551 | _T_1546; // @[Mux.scala 27:72]
  wire [2:0] _T_1553 = _T_1552 | _T_1547; // @[Mux.scala 27:72]
  wire [2:0] _T_1554 = _T_1553 | _T_1548; // @[Mux.scala 27:72]
  wire [2:0] _T_1555 = _T_1554 | _T_1549; // @[Mux.scala 27:72]
  wire  latch_18 = idle_18 & auto_in_2_b_ready; // @[Arbiter.scala 89:24]
  wire [3:0] _readys_mask_T_126 = readys_readys_18 & readys_filter_lo_18; // @[Arbiter.scala 28:29]
  wire [4:0] _readys_mask_T_127 = {_readys_mask_T_126, 1'h0}; // @[package.scala 244:48]
  wire [3:0] _readys_mask_T_129 = _readys_mask_T_126 | _readys_mask_T_127[3:0]; // @[package.scala 244:43]
  wire [5:0] _readys_mask_T_130 = {_readys_mask_T_129, 2'h0}; // @[package.scala 244:48]
  wire [3:0] _readys_mask_T_132 = _readys_mask_T_129 | _readys_mask_T_130[3:0]; // @[package.scala 244:43]
  wire  _T_1577 = portsBIO_filtered_2_2_valid | portsBIO_filtered_3_2_valid | portsBIO_filtered_4_2_valid |
    portsBIO_filtered_5_2_valid; // @[Arbiter.scala 107:36]
  wire  _sink_ACancel_earlyValid_T_100 = state_18_0 & portsBIO_filtered_2_2_valid | state_18_1 &
    portsBIO_filtered_3_2_valid | state_18_2 & portsBIO_filtered_4_2_valid | state_18_3 & portsBIO_filtered_5_2_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_18_earlyValid = idle_18 ? _T_1577 : _sink_ACancel_earlyValid_T_100; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_110 = auto_in_2_b_ready & sink_ACancel_18_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [1:0] _GEN_108 = {{1'd0}, _beatsLeft_T_110}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_112 = beatsLeft_18 - _GEN_108; // @[Arbiter.scala 113:52]
  wire  _T_1600 = muxStateEarly_18_3 & auto_out_5_b_bits_corrupt; // @[Mux.scala 27:72]
  wire  _T_1602 = muxStateEarly_18_0 & auto_out_2_b_bits_corrupt | muxStateEarly_18_1 & auto_out_3_b_bits_corrupt |
    muxStateEarly_18_2 & auto_out_4_b_bits_corrupt; // @[Mux.scala 27:72]
  wire [127:0] _T_1604 = muxStateEarly_18_0 ? auto_out_2_b_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1605 = muxStateEarly_18_1 ? auto_out_3_b_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1606 = muxStateEarly_18_2 ? auto_out_4_b_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1607 = muxStateEarly_18_3 ? auto_out_5_b_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1608 = _T_1604 | _T_1605; // @[Mux.scala 27:72]
  wire [127:0] _T_1609 = _T_1608 | _T_1606; // @[Mux.scala 27:72]
  wire [15:0] _T_1611 = muxStateEarly_18_0 ? auto_out_2_b_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_1612 = muxStateEarly_18_1 ? auto_out_3_b_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_1613 = muxStateEarly_18_2 ? auto_out_4_b_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_1614 = muxStateEarly_18_3 ? auto_out_5_b_bits_mask : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_1615 = _T_1611 | _T_1612; // @[Mux.scala 27:72]
  wire [15:0] _T_1616 = _T_1615 | _T_1613; // @[Mux.scala 27:72]
  wire [36:0] _T_1618 = muxStateEarly_18_0 ? out_3_2_b_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_1619 = muxStateEarly_18_1 ? out_3_3_b_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_1620 = muxStateEarly_18_2 ? out_3_4_b_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_1621 = muxStateEarly_18_3 ? out_3_5_b_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_1622 = _T_1618 | _T_1619; // @[Mux.scala 27:72]
  wire [36:0] _T_1623 = _T_1622 | _T_1620; // @[Mux.scala 27:72]
  wire [3:0] _T_1632 = muxStateEarly_18_0 ? out_3_2_b_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1633 = muxStateEarly_18_1 ? out_3_3_b_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1634 = muxStateEarly_18_2 ? out_3_4_b_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1635 = muxStateEarly_18_3 ? out_3_5_b_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1636 = _T_1632 | _T_1633; // @[Mux.scala 27:72]
  wire [3:0] _T_1637 = _T_1636 | _T_1634; // @[Mux.scala 27:72]
  wire [1:0] _T_1639 = muxStateEarly_18_0 ? auto_out_2_b_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1640 = muxStateEarly_18_1 ? auto_out_3_b_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1641 = muxStateEarly_18_2 ? auto_out_4_b_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1642 = muxStateEarly_18_3 ? auto_out_5_b_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1643 = _T_1639 | _T_1640; // @[Mux.scala 27:72]
  wire [1:0] _T_1644 = _T_1643 | _T_1641; // @[Mux.scala 27:72]
  wire [2:0] _T_1646 = muxStateEarly_18_0 ? auto_out_2_b_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1647 = muxStateEarly_18_1 ? auto_out_3_b_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1648 = muxStateEarly_18_2 ? auto_out_4_b_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1649 = muxStateEarly_18_3 ? auto_out_5_b_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1650 = _T_1646 | _T_1647; // @[Mux.scala 27:72]
  wire [2:0] _T_1651 = _T_1650 | _T_1648; // @[Mux.scala 27:72]
  wire  latch_19 = idle_19 & auto_in_2_d_ready; // @[Arbiter.scala 89:24]
  wire [6:0] _readys_mask_T_134 = readys_readys_19 & readys_filter_lo_19; // @[Arbiter.scala 28:29]
  wire [7:0] _readys_mask_T_135 = {_readys_mask_T_134, 1'h0}; // @[package.scala 244:48]
  wire [6:0] _readys_mask_T_137 = _readys_mask_T_134 | _readys_mask_T_135[6:0]; // @[package.scala 244:43]
  wire [8:0] _readys_mask_T_138 = {_readys_mask_T_137, 2'h0}; // @[package.scala 244:48]
  wire [6:0] _readys_mask_T_140 = _readys_mask_T_137 | _readys_mask_T_138[6:0]; // @[package.scala 244:43]
  wire [10:0] _readys_mask_T_141 = {_readys_mask_T_140, 4'h0}; // @[package.scala 244:48]
  wire [6:0] _readys_mask_T_143 = _readys_mask_T_140 | _readys_mask_T_141[6:0]; // @[package.scala 244:43]
  wire  _T_1688 = portsDIO_filtered__2_valid | portsDIO_filtered_1_2_valid | portsDIO_filtered_2_2_valid |
    portsDIO_filtered_3_2_valid | portsDIO_filtered_4_2_valid | portsDIO_filtered_5_2_valid |
    portsDIO_filtered_6_2_valid; // @[Arbiter.scala 107:36]
  wire [1:0] maskedBeats_0_19 = earlyWinner_19_0 ? beatsDO_0 : 2'h0; // @[Arbiter.scala 111:73]
  wire [3:0] maskedBeats_1_19 = earlyWinner_19_1 ? beatsDO_1 : 4'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_2_11 = earlyWinner_19_2 ? beatsDO_2 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_3_4 = earlyWinner_19_3 ? beatsDO_3 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_4_2 = earlyWinner_19_4 ? beatsDO_4 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_5_2 = earlyWinner_19_5 ? beatsDO_5 : 2'h0; // @[Arbiter.scala 111:73]
  wire [2:0] maskedBeats_6_2 = earlyWinner_19_6 ? beatsDO_6 : 3'h0; // @[Arbiter.scala 111:73]
  wire [3:0] _GEN_109 = {{2'd0}, maskedBeats_0_19}; // @[Arbiter.scala 112:44]
  wire [3:0] _initBeats_T_21 = _GEN_109 | maskedBeats_1_19; // @[Arbiter.scala 112:44]
  wire [3:0] _GEN_110 = {{2'd0}, maskedBeats_2_11}; // @[Arbiter.scala 112:44]
  wire [3:0] _initBeats_T_22 = _initBeats_T_21 | _GEN_110; // @[Arbiter.scala 112:44]
  wire [3:0] _GEN_111 = {{2'd0}, maskedBeats_3_4}; // @[Arbiter.scala 112:44]
  wire [3:0] _initBeats_T_23 = _initBeats_T_22 | _GEN_111; // @[Arbiter.scala 112:44]
  wire [3:0] _GEN_112 = {{2'd0}, maskedBeats_4_2}; // @[Arbiter.scala 112:44]
  wire [3:0] _initBeats_T_24 = _initBeats_T_23 | _GEN_112; // @[Arbiter.scala 112:44]
  wire [3:0] _GEN_113 = {{2'd0}, maskedBeats_5_2}; // @[Arbiter.scala 112:44]
  wire [3:0] _initBeats_T_25 = _initBeats_T_24 | _GEN_113; // @[Arbiter.scala 112:44]
  wire [3:0] _GEN_114 = {{1'd0}, maskedBeats_6_2}; // @[Arbiter.scala 112:44]
  wire [3:0] initBeats_19 = _initBeats_T_25 | _GEN_114; // @[Arbiter.scala 112:44]
  wire  _sink_ACancel_earlyValid_T_120 = state_19_0 & portsDIO_filtered__2_valid | state_19_1 &
    portsDIO_filtered_1_2_valid | state_19_2 & portsDIO_filtered_2_2_valid | state_19_3 & portsDIO_filtered_3_2_valid |
    state_19_4 & portsDIO_filtered_4_2_valid | state_19_5 & portsDIO_filtered_5_2_valid | state_19_6 &
    portsDIO_filtered_6_2_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_19_earlyValid = idle_19 ? _T_1688 : _sink_ACancel_earlyValid_T_120; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_116 = auto_in_2_d_ready & sink_ACancel_19_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [3:0] _GEN_115 = {{3'd0}, _beatsLeft_T_116}; // @[Arbiter.scala 113:52]
  wire [3:0] _beatsLeft_T_118 = beatsLeft_19 - _GEN_115; // @[Arbiter.scala 113:52]
  wire  _T_1723 = muxStateEarly_19_6 & auto_out_6_d_bits_corrupt; // @[Mux.scala 27:72]
  wire  _T_1728 = muxStateEarly_19_0 & auto_out_0_d_bits_corrupt | muxStateEarly_19_1 & auto_out_1_d_bits_corrupt |
    muxStateEarly_19_2 & auto_out_2_d_bits_corrupt | muxStateEarly_19_3 & auto_out_3_d_bits_corrupt | muxStateEarly_19_4
     & auto_out_4_d_bits_corrupt | muxStateEarly_19_5 & auto_out_5_d_bits_corrupt; // @[Mux.scala 27:72]
  wire [127:0] _T_1730 = muxStateEarly_19_0 ? auto_out_0_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1731 = muxStateEarly_19_1 ? auto_out_1_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1732 = muxStateEarly_19_2 ? auto_out_2_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1733 = muxStateEarly_19_3 ? auto_out_3_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1734 = muxStateEarly_19_4 ? auto_out_4_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1735 = muxStateEarly_19_5 ? auto_out_5_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1736 = muxStateEarly_19_6 ? auto_out_6_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_1737 = _T_1730 | _T_1731; // @[Mux.scala 27:72]
  wire [127:0] _T_1738 = _T_1737 | _T_1732; // @[Mux.scala 27:72]
  wire [127:0] _T_1739 = _T_1738 | _T_1733; // @[Mux.scala 27:72]
  wire [127:0] _T_1740 = _T_1739 | _T_1734; // @[Mux.scala 27:72]
  wire [127:0] _T_1741 = _T_1740 | _T_1735; // @[Mux.scala 27:72]
  wire  _T_1749 = muxStateEarly_19_6 & auto_out_6_d_bits_denied; // @[Mux.scala 27:72]
  wire  _T_1754 = muxStateEarly_19_1 & auto_out_1_d_bits_denied | muxStateEarly_19_2 & auto_out_2_d_bits_denied |
    muxStateEarly_19_3 & auto_out_3_d_bits_denied | muxStateEarly_19_4 & auto_out_4_d_bits_denied | muxStateEarly_19_5
     & auto_out_5_d_bits_denied; // @[Mux.scala 27:72]
  wire [4:0] _T_1757 = muxStateEarly_19_1 ? out_3_1_d_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _T_1758 = muxStateEarly_19_2 ? out_3_2_d_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _T_1759 = muxStateEarly_19_3 ? out_3_3_d_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _T_1760 = muxStateEarly_19_4 ? out_3_4_d_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _T_1761 = muxStateEarly_19_5 ? out_3_5_d_bits_sink : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _T_1764 = _T_1757 | _T_1758; // @[Mux.scala 27:72]
  wire [4:0] _T_1765 = _T_1764 | _T_1759; // @[Mux.scala 27:72]
  wire [4:0] _T_1766 = _T_1765 | _T_1760; // @[Mux.scala 27:72]
  wire [3:0] _T_1782 = muxStateEarly_19_0 ? out_3_0_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1783 = muxStateEarly_19_1 ? auto_out_1_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1784 = muxStateEarly_19_2 ? out_3_2_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1785 = muxStateEarly_19_3 ? out_3_3_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1786 = muxStateEarly_19_4 ? out_3_4_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1787 = muxStateEarly_19_5 ? out_3_5_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1788 = muxStateEarly_19_6 ? out_3_6_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_1789 = _T_1782 | _T_1783; // @[Mux.scala 27:72]
  wire [3:0] _T_1790 = _T_1789 | _T_1784; // @[Mux.scala 27:72]
  wire [3:0] _T_1791 = _T_1790 | _T_1785; // @[Mux.scala 27:72]
  wire [3:0] _T_1792 = _T_1791 | _T_1786; // @[Mux.scala 27:72]
  wire [3:0] _T_1793 = _T_1792 | _T_1787; // @[Mux.scala 27:72]
  wire [1:0] _T_1796 = muxStateEarly_19_1 ? auto_out_1_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1797 = muxStateEarly_19_2 ? auto_out_2_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1798 = muxStateEarly_19_3 ? auto_out_3_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1799 = muxStateEarly_19_4 ? auto_out_4_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1800 = muxStateEarly_19_5 ? auto_out_5_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _T_1803 = _T_1796 | _T_1797; // @[Mux.scala 27:72]
  wire [1:0] _T_1804 = _T_1803 | _T_1798; // @[Mux.scala 27:72]
  wire [1:0] _T_1805 = _T_1804 | _T_1799; // @[Mux.scala 27:72]
  wire [2:0] _T_1808 = muxStateEarly_19_0 ? auto_out_0_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1809 = muxStateEarly_19_1 ? auto_out_1_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1810 = muxStateEarly_19_2 ? auto_out_2_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1811 = muxStateEarly_19_3 ? auto_out_3_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1812 = muxStateEarly_19_4 ? auto_out_4_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1813 = muxStateEarly_19_5 ? auto_out_5_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1814 = muxStateEarly_19_6 ? auto_out_6_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_1815 = _T_1808 | _T_1809; // @[Mux.scala 27:72]
  wire [2:0] _T_1816 = _T_1815 | _T_1810; // @[Mux.scala 27:72]
  wire [2:0] _T_1817 = _T_1816 | _T_1811; // @[Mux.scala 27:72]
  wire [2:0] _T_1818 = _T_1817 | _T_1812; // @[Mux.scala 27:72]
  wire [2:0] _T_1819 = _T_1818 | _T_1813; // @[Mux.scala 27:72]
  assign auto_in_2_a_ready = _portsAOI_in_2_a_ready_T_11 | _portsAOI_in_2_a_ready_T_6; // @[Mux.scala 27:72]
  assign auto_in_2_b_valid = idle_18 ? _T_1577 : _sink_ACancel_earlyValid_T_100; // @[Arbiter.scala 125:29]
  assign auto_in_2_b_bits_opcode = _T_1651 | _T_1649; // @[Mux.scala 27:72]
  assign auto_in_2_b_bits_param = _T_1644 | _T_1642; // @[Mux.scala 27:72]
  assign auto_in_2_b_bits_size = _T_1637 | _T_1635; // @[Mux.scala 27:72]
  assign auto_in_2_b_bits_source = sink_ACancel_18_bits_source[4:0]; // @[Xbar.scala 228:69]
  assign auto_in_2_b_bits_address = _T_1623 | _T_1621; // @[Mux.scala 27:72]
  assign auto_in_2_b_bits_mask = _T_1616 | _T_1614; // @[Mux.scala 27:72]
  assign auto_in_2_b_bits_data = _T_1609 | _T_1607; // @[Mux.scala 27:72]
  assign auto_in_2_b_bits_corrupt = _T_1602 | _T_1600; // @[Mux.scala 27:72]
  assign auto_in_2_c_ready = _portsCOI_in_2_c_ready_T_10 | _portsCOI_in_2_c_ready_T_5; // @[Mux.scala 27:72]
  assign auto_in_2_d_valid = idle_19 ? _T_1688 : _sink_ACancel_earlyValid_T_120; // @[Arbiter.scala 125:29]
  assign auto_in_2_d_bits_opcode = _T_1819 | _T_1814; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_param = _T_1805 | _T_1800; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_size = _T_1793 | _T_1788; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_source = sink_ACancel_19_bits_source[4:0]; // @[Xbar.scala 228:69]
  assign auto_in_2_d_bits_sink = _T_1766 | _T_1761; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_denied = _T_1754 | _T_1749; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_data = _T_1741 | _T_1736; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_corrupt = _T_1728 | _T_1723; // @[Mux.scala 27:72]
  assign auto_in_2_e_ready = _portsEOI_in_2_e_ready_T_10 | _portsEOI_in_2_e_ready_T_5; // @[Mux.scala 27:72]
  assign auto_in_1_a_ready = _portsAOI_in_1_a_ready_T_11 | _portsAOI_in_1_a_ready_T_6; // @[Mux.scala 27:72]
  assign auto_in_1_b_valid = idle_16 ? _T_1313 : _sink_ACancel_earlyValid_T_69; // @[Arbiter.scala 125:29]
  assign auto_in_1_b_bits_opcode = _T_1387 | _T_1385; // @[Mux.scala 27:72]
  assign auto_in_1_b_bits_param = _T_1380 | _T_1378; // @[Mux.scala 27:72]
  assign auto_in_1_b_bits_size = _T_1373 | _T_1371; // @[Mux.scala 27:72]
  assign auto_in_1_b_bits_source = sink_ACancel_16_bits_source[4:0]; // @[Xbar.scala 228:69]
  assign auto_in_1_b_bits_address = _T_1359 | _T_1357; // @[Mux.scala 27:72]
  assign auto_in_1_b_bits_mask = _T_1352 | _T_1350; // @[Mux.scala 27:72]
  assign auto_in_1_b_bits_data = _T_1345 | _T_1343; // @[Mux.scala 27:72]
  assign auto_in_1_b_bits_corrupt = _T_1338 | _T_1336; // @[Mux.scala 27:72]
  assign auto_in_1_c_ready = _portsCOI_in_1_c_ready_T_10 | _portsCOI_in_1_c_ready_T_5; // @[Mux.scala 27:72]
  assign auto_in_1_d_valid = idle_17 ? _T_1424 : _sink_ACancel_earlyValid_T_89; // @[Arbiter.scala 125:29]
  assign auto_in_1_d_bits_opcode = _T_1555 | _T_1550; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_param = _T_1541 | _T_1536; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_size = _T_1529 | _T_1524; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_source = sink_ACancel_17_bits_source[4:0]; // @[Xbar.scala 228:69]
  assign auto_in_1_d_bits_sink = _T_1502 | _T_1497; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_denied = _T_1490 | _T_1485; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_data = _T_1477 | _T_1472; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_corrupt = _T_1464 | _T_1459; // @[Mux.scala 27:72]
  assign auto_in_1_e_ready = _portsEOI_in_1_e_ready_T_10 | _portsEOI_in_1_e_ready_T_5; // @[Mux.scala 27:72]
  assign auto_in_0_a_ready = _portsAOI_in_0_a_ready_T_11 | _portsAOI_in_0_a_ready_T_6; // @[Mux.scala 27:72]
  assign auto_in_0_d_valid = idle_15 ? _T_1160 : _sink_ACancel_earlyValid_T_58; // @[Arbiter.scala 125:29]
  assign auto_in_0_d_bits_opcode = _T_1291 | _T_1286; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_param = _T_1277 | _T_1272; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_size = _T_1265 | _T_1260; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_source = sink_ACancel_15_bits_source[5:0]; // @[Xbar.scala 228:69]
  assign auto_in_0_d_bits_sink = _T_1238 | _T_1233; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_denied = _T_1226 | _T_1221; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_data = _T_1213 | _T_1208; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_corrupt = _T_1200 | _T_1195; // @[Mux.scala 27:72]
  assign auto_out_6_a_valid = idle_14 ? _T_1033 : _out_6_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  assign auto_out_6_a_bits_opcode = _T_1123 | _T_1122; // @[Mux.scala 27:72]
  assign auto_out_6_a_bits_param = _T_1116 | _T_1117; // @[Mux.scala 27:72]
  assign auto_out_6_a_bits_size = out_3_6_a_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_6_a_bits_source = _T_1108 | _T_1107; // @[Mux.scala 27:72]
  assign auto_out_6_a_bits_address = _T_1103 | _T_1102; // @[Mux.scala 27:72]
  assign auto_out_6_a_bits_user_amba_prot_bufferable = muxStateEarly_14_0 & auto_in_0_a_bits_user_amba_prot_bufferable
     | muxStateEarly_14_1 & auto_in_1_a_bits_user_amba_prot_bufferable | muxStateEarly_14_2 &
    auto_in_2_a_bits_user_amba_prot_bufferable; // @[Mux.scala 27:72]
  assign auto_out_6_a_bits_user_amba_prot_modifiable = muxStateEarly_14_0 & auto_in_0_a_bits_user_amba_prot_modifiable
     | muxStateEarly_14_1 & auto_in_1_a_bits_user_amba_prot_modifiable | muxStateEarly_14_2 &
    auto_in_2_a_bits_user_amba_prot_modifiable; // @[Mux.scala 27:72]
  assign auto_out_6_a_bits_user_amba_prot_readalloc = muxStateEarly_14_0 & auto_in_0_a_bits_user_amba_prot_readalloc |
    muxStateEarly_14_1 & auto_in_1_a_bits_user_amba_prot_readalloc | muxStateEarly_14_2 &
    auto_in_2_a_bits_user_amba_prot_readalloc; // @[Mux.scala 27:72]
  assign auto_out_6_a_bits_user_amba_prot_writealloc = muxStateEarly_14_0 & auto_in_0_a_bits_user_amba_prot_writealloc
     | muxStateEarly_14_1 & auto_in_1_a_bits_user_amba_prot_writealloc | muxStateEarly_14_2 &
    auto_in_2_a_bits_user_amba_prot_writealloc; // @[Mux.scala 27:72]
  assign auto_out_6_a_bits_user_amba_prot_privileged = muxStateEarly_14_0 & auto_in_0_a_bits_user_amba_prot_privileged
     | muxStateEarly_14_1 & auto_in_1_a_bits_user_amba_prot_privileged | muxStateEarly_14_2 &
    auto_in_2_a_bits_user_amba_prot_privileged; // @[Mux.scala 27:72]
  assign auto_out_6_a_bits_user_amba_prot_secure = muxStateEarly_14_0 & auto_in_0_a_bits_user_amba_prot_secure |
    muxStateEarly_14_1 & auto_in_1_a_bits_user_amba_prot_secure | muxStateEarly_14_2 &
    auto_in_2_a_bits_user_amba_prot_secure; // @[Mux.scala 27:72]
  assign auto_out_6_a_bits_user_amba_prot_fetch = muxStateEarly_14_0 & auto_in_0_a_bits_user_amba_prot_fetch |
    muxStateEarly_14_1 & auto_in_1_a_bits_user_amba_prot_fetch | muxStateEarly_14_2 &
    auto_in_2_a_bits_user_amba_prot_fetch; // @[Mux.scala 27:72]
  assign auto_out_6_a_bits_mask = _T_1063 | _T_1062; // @[Mux.scala 27:72]
  assign auto_out_6_a_bits_data = _T_1058 | _T_1057; // @[Mux.scala 27:72]
  assign auto_out_6_a_bits_corrupt = muxStateEarly_14_1 & auto_in_1_a_bits_corrupt | muxStateEarly_14_2 &
    auto_in_2_a_bits_corrupt; // @[Mux.scala 27:72]
  assign auto_out_6_d_ready = requestDOI_6_0 & out_41_ready | requestDOI_6_1 & out_54_ready | requestDOI_6_2 &
    out_67_ready; // @[Mux.scala 27:72]
  assign auto_out_5_a_valid = idle_11 ? _T_833 : _out_5_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  assign auto_out_5_a_bits_opcode = _T_923 | _T_922; // @[Mux.scala 27:72]
  assign auto_out_5_a_bits_param = _T_916 | _T_917; // @[Mux.scala 27:72]
  assign auto_out_5_a_bits_size = out_3_5_a_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_5_a_bits_source = _T_908 | _T_907; // @[Mux.scala 27:72]
  assign auto_out_5_a_bits_address = out_3_5_a_bits_address[35:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_5_a_bits_mask = _T_863 | _T_862; // @[Mux.scala 27:72]
  assign auto_out_5_a_bits_data = _T_858 | _T_857; // @[Mux.scala 27:72]
  assign auto_out_5_a_bits_corrupt = muxStateEarly_11_1 & auto_in_1_a_bits_corrupt | muxStateEarly_11_2 &
    auto_in_2_a_bits_corrupt; // @[Mux.scala 27:72]
  assign auto_out_5_b_ready = requestBOI_5_1 & out_46_ready | requestBOI_5_2 & out_59_ready; // @[Mux.scala 27:72]
  assign auto_out_5_c_valid = idle_12 ? _T_935 : _sink_ACancel_earlyValid_T_33; // @[Arbiter.scala 125:29]
  assign auto_out_5_c_bits_opcode = _T_988 | _T_989; // @[Mux.scala 27:72]
  assign auto_out_5_c_bits_param = _T_985 | _T_986; // @[Mux.scala 27:72]
  assign auto_out_5_c_bits_size = sink_ACancel_10_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_5_c_bits_source = _T_979 | _T_980; // @[Mux.scala 27:72]
  assign auto_out_5_c_bits_address = sink_ACancel_10_bits_address[35:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_5_c_bits_data = _T_952 | _T_953; // @[Mux.scala 27:72]
  assign auto_out_5_c_bits_corrupt = muxStateEarly_12_0 & auto_in_1_c_bits_corrupt | muxStateEarly_12_1 &
    auto_in_2_c_bits_corrupt; // @[Mux.scala 27:72]
  assign auto_out_5_d_ready = requestDOI_5_0 & out_40_ready | requestDOI_5_1 & out_53_ready | requestDOI_5_2 &
    out_66_ready; // @[Mux.scala 27:72]
  assign auto_out_5_e_valid = idle_13 ? _T_1001 : _sink_ACancel_earlyValid_T_38; // @[Arbiter.scala 125:29]
  assign auto_out_5_e_bits_sink = sink_ACancel_11_bits_sink[2:0]; // @[Xbar.scala 228:69]
  assign auto_out_4_a_valid = idle_8 ? _T_633 : _out_4_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  assign auto_out_4_a_bits_opcode = _T_723 | _T_722; // @[Mux.scala 27:72]
  assign auto_out_4_a_bits_param = _T_716 | _T_717; // @[Mux.scala 27:72]
  assign auto_out_4_a_bits_size = out_3_4_a_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_4_a_bits_source = _T_708 | _T_707; // @[Mux.scala 27:72]
  assign auto_out_4_a_bits_address = out_3_4_a_bits_address[35:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_4_a_bits_mask = _T_663 | _T_662; // @[Mux.scala 27:72]
  assign auto_out_4_a_bits_data = _T_658 | _T_657; // @[Mux.scala 27:72]
  assign auto_out_4_a_bits_corrupt = muxStateEarly_8_1 & auto_in_1_a_bits_corrupt | muxStateEarly_8_2 &
    auto_in_2_a_bits_corrupt; // @[Mux.scala 27:72]
  assign auto_out_4_b_ready = requestBOI_4_1 & out_45_ready | requestBOI_4_2 & out_58_ready; // @[Mux.scala 27:72]
  assign auto_out_4_c_valid = idle_9 ? _T_735 : _sink_ACancel_earlyValid_T_23; // @[Arbiter.scala 125:29]
  assign auto_out_4_c_bits_opcode = _T_788 | _T_789; // @[Mux.scala 27:72]
  assign auto_out_4_c_bits_param = _T_785 | _T_786; // @[Mux.scala 27:72]
  assign auto_out_4_c_bits_size = sink_ACancel_8_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_4_c_bits_source = _T_779 | _T_780; // @[Mux.scala 27:72]
  assign auto_out_4_c_bits_address = sink_ACancel_8_bits_address[35:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_4_c_bits_data = _T_752 | _T_753; // @[Mux.scala 27:72]
  assign auto_out_4_c_bits_corrupt = muxStateEarly_9_0 & auto_in_1_c_bits_corrupt | muxStateEarly_9_1 &
    auto_in_2_c_bits_corrupt; // @[Mux.scala 27:72]
  assign auto_out_4_d_ready = requestDOI_4_0 & out_39_ready | requestDOI_4_1 & out_52_ready | requestDOI_4_2 &
    out_65_ready; // @[Mux.scala 27:72]
  assign auto_out_4_e_valid = idle_10 ? _T_801 : _sink_ACancel_earlyValid_T_28; // @[Arbiter.scala 125:29]
  assign auto_out_4_e_bits_sink = sink_ACancel_9_bits_sink[2:0]; // @[Xbar.scala 228:69]
  assign auto_out_3_a_valid = idle_5 ? _T_433 : _out_3_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  assign auto_out_3_a_bits_opcode = _T_523 | _T_522; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_param = _T_516 | _T_517; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_size = out_3_3_a_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_3_a_bits_source = _T_508 | _T_507; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_address = out_3_3_a_bits_address[35:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_3_a_bits_mask = _T_463 | _T_462; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_data = _T_458 | _T_457; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_corrupt = muxStateEarly_5_1 & auto_in_1_a_bits_corrupt | muxStateEarly_5_2 &
    auto_in_2_a_bits_corrupt; // @[Mux.scala 27:72]
  assign auto_out_3_b_ready = requestBOI_3_1 & out_44_ready | requestBOI_3_2 & out_57_ready; // @[Mux.scala 27:72]
  assign auto_out_3_c_valid = idle_6 ? _T_535 : _sink_ACancel_earlyValid_T_13; // @[Arbiter.scala 125:29]
  assign auto_out_3_c_bits_opcode = _T_588 | _T_589; // @[Mux.scala 27:72]
  assign auto_out_3_c_bits_param = _T_585 | _T_586; // @[Mux.scala 27:72]
  assign auto_out_3_c_bits_size = sink_ACancel_6_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_3_c_bits_source = _T_579 | _T_580; // @[Mux.scala 27:72]
  assign auto_out_3_c_bits_address = sink_ACancel_6_bits_address[35:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_3_c_bits_data = _T_552 | _T_553; // @[Mux.scala 27:72]
  assign auto_out_3_c_bits_corrupt = muxStateEarly_6_0 & auto_in_1_c_bits_corrupt | muxStateEarly_6_1 &
    auto_in_2_c_bits_corrupt; // @[Mux.scala 27:72]
  assign auto_out_3_d_ready = requestDOI_3_0 & out_38_ready | requestDOI_3_1 & out_51_ready | requestDOI_3_2 &
    out_64_ready; // @[Mux.scala 27:72]
  assign auto_out_3_e_valid = idle_7 ? _T_601 : _sink_ACancel_earlyValid_T_18; // @[Arbiter.scala 125:29]
  assign auto_out_3_e_bits_sink = sink_ACancel_7_bits_sink[2:0]; // @[Xbar.scala 228:69]
  assign auto_out_2_a_valid = idle_2 ? _T_233 : _out_2_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  assign auto_out_2_a_bits_opcode = _T_323 | _T_322; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_param = _T_316 | _T_317; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_size = out_3_2_a_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_2_a_bits_source = _T_308 | _T_307; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_address = out_3_2_a_bits_address[35:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_2_a_bits_mask = _T_263 | _T_262; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_data = _T_258 | _T_257; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_corrupt = muxStateEarly_2_1 & auto_in_1_a_bits_corrupt | muxStateEarly_2_2 &
    auto_in_2_a_bits_corrupt; // @[Mux.scala 27:72]
  assign auto_out_2_b_ready = requestBOI_2_1 & out_43_ready | requestBOI_2_2 & out_56_ready; // @[Mux.scala 27:72]
  assign auto_out_2_c_valid = idle_3 ? _T_335 : _sink_ACancel_earlyValid_T_3; // @[Arbiter.scala 125:29]
  assign auto_out_2_c_bits_opcode = _T_388 | _T_389; // @[Mux.scala 27:72]
  assign auto_out_2_c_bits_param = _T_385 | _T_386; // @[Mux.scala 27:72]
  assign auto_out_2_c_bits_size = sink_ACancel_4_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_2_c_bits_source = _T_379 | _T_380; // @[Mux.scala 27:72]
  assign auto_out_2_c_bits_address = sink_ACancel_4_bits_address[35:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_2_c_bits_data = _T_352 | _T_353; // @[Mux.scala 27:72]
  assign auto_out_2_c_bits_corrupt = muxStateEarly_3_0 & auto_in_1_c_bits_corrupt | muxStateEarly_3_1 &
    auto_in_2_c_bits_corrupt; // @[Mux.scala 27:72]
  assign auto_out_2_d_ready = requestDOI_2_0 & out_37_ready | requestDOI_2_1 & out_50_ready | requestDOI_2_2 &
    out_63_ready; // @[Mux.scala 27:72]
  assign auto_out_2_e_valid = idle_4 ? _T_401 : _sink_ACancel_earlyValid_T_8; // @[Arbiter.scala 125:29]
  assign auto_out_2_e_bits_sink = sink_ACancel_5_bits_sink[2:0]; // @[Xbar.scala 228:69]
  assign auto_out_1_a_valid = idle_1 ? _T_126 : _out_1_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  assign auto_out_1_a_bits_opcode = _T_216 | _T_215; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_param = _T_209 | _T_210; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_size = _T_206 | _T_205; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_source = _T_201 | _T_200; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_address = out_3_1_a_bits_address[31:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_1_a_bits_user_amba_prot_bufferable = muxStateEarly_1_0 & auto_in_0_a_bits_user_amba_prot_bufferable |
    muxStateEarly_1_1 & auto_in_1_a_bits_user_amba_prot_bufferable | muxStateEarly_1_2 &
    auto_in_2_a_bits_user_amba_prot_bufferable; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_modifiable = muxStateEarly_1_0 & auto_in_0_a_bits_user_amba_prot_modifiable |
    muxStateEarly_1_1 & auto_in_1_a_bits_user_amba_prot_modifiable | muxStateEarly_1_2 &
    auto_in_2_a_bits_user_amba_prot_modifiable; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_readalloc = muxStateEarly_1_0 & auto_in_0_a_bits_user_amba_prot_readalloc |
    muxStateEarly_1_1 & auto_in_1_a_bits_user_amba_prot_readalloc | muxStateEarly_1_2 &
    auto_in_2_a_bits_user_amba_prot_readalloc; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_writealloc = muxStateEarly_1_0 & auto_in_0_a_bits_user_amba_prot_writealloc |
    muxStateEarly_1_1 & auto_in_1_a_bits_user_amba_prot_writealloc | muxStateEarly_1_2 &
    auto_in_2_a_bits_user_amba_prot_writealloc; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_privileged = muxStateEarly_1_0 & auto_in_0_a_bits_user_amba_prot_privileged |
    muxStateEarly_1_1 & auto_in_1_a_bits_user_amba_prot_privileged | muxStateEarly_1_2 &
    auto_in_2_a_bits_user_amba_prot_privileged; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_secure = muxStateEarly_1_0 & auto_in_0_a_bits_user_amba_prot_secure |
    muxStateEarly_1_1 & auto_in_1_a_bits_user_amba_prot_secure | muxStateEarly_1_2 &
    auto_in_2_a_bits_user_amba_prot_secure; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_fetch = muxStateEarly_1_0 & auto_in_0_a_bits_user_amba_prot_fetch |
    muxStateEarly_1_1 & auto_in_1_a_bits_user_amba_prot_fetch | muxStateEarly_1_2 &
    auto_in_2_a_bits_user_amba_prot_fetch; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_mask = _T_156 | _T_155; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_data = _T_151 | _T_150; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_corrupt = muxStateEarly_1_1 & auto_in_1_a_bits_corrupt | muxStateEarly_1_2 &
    auto_in_2_a_bits_corrupt; // @[Mux.scala 27:72]
  assign auto_out_1_d_ready = requestDOI_1_0 & out_36_ready | requestDOI_1_1 & out_49_ready | requestDOI_1_2 &
    out_62_ready; // @[Mux.scala 27:72]
  assign auto_out_0_a_valid = idle ? _T_19 : _out_0_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  assign auto_out_0_a_bits_opcode = _T_109 | _T_108; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_param = _T_102 | _T_103; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_size = out_3_0_a_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_0_a_bits_source = _T_94 | _T_93; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_address = out_3_0_a_bits_address[27:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_0_a_bits_mask = _T_49 | _T_48; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_data = _T_44 | _T_43; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_corrupt = muxStateEarly__1 & auto_in_1_a_bits_corrupt | muxStateEarly__2 &
    auto_in_2_a_bits_corrupt; // @[Mux.scala 27:72]
  assign auto_out_0_d_ready = requestDOI_0_0 & out_35_ready | requestDOI_0_1 & out_48_ready | requestDOI_0_2 &
    out_61_ready; // @[Mux.scala 27:72]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_15 <= 4'h0;
    end else if (latch_15) begin
      beatsLeft_15 <= initBeats_15;
    end else begin
      beatsLeft_15 <= _beatsLeft_T_94;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_15 <= 7'h7f;
    end else if (latch_15 & |readys_filter_lo_15) begin
      readys_mask_15 <= _readys_mask_T_105;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_15_0 <= 1'h0;
    end else if (idle_15) begin
      state_15_0 <= earlyWinner_15_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_15_1 <= 1'h0;
    end else if (idle_15) begin
      state_15_1 <= earlyWinner_15_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_15_2 <= 1'h0;
    end else if (idle_15) begin
      state_15_2 <= earlyWinner_15_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_15_3 <= 1'h0;
    end else if (idle_15) begin
      state_15_3 <= earlyWinner_15_3;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_15_4 <= 1'h0;
    end else if (idle_15) begin
      state_15_4 <= earlyWinner_15_4;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_15_5 <= 1'h0;
    end else if (idle_15) begin
      state_15_5 <= earlyWinner_15_5;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_15_6 <= 1'h0;
    end else if (idle_15) begin
      state_15_6 <= earlyWinner_15_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_16 <= 2'h0;
    end else if (latch_16) begin
      beatsLeft_16 <= 2'h0;
    end else begin
      beatsLeft_16 <= _beatsLeft_T_100;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_16 <= 4'hf;
    end else if (latch_16 & |readys_filter_lo_16) begin
      readys_mask_16 <= _readys_mask_T_113;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_16_0 <= 1'h0;
    end else if (idle_16) begin
      state_16_0 <= earlyWinner_16_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_16_1 <= 1'h0;
    end else if (idle_16) begin
      state_16_1 <= earlyWinner_16_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_16_2 <= 1'h0;
    end else if (idle_16) begin
      state_16_2 <= earlyWinner_16_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_16_3 <= 1'h0;
    end else if (idle_16) begin
      state_16_3 <= earlyWinner_16_3;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_17 <= 4'h0;
    end else if (latch_17) begin
      beatsLeft_17 <= initBeats_17;
    end else begin
      beatsLeft_17 <= _beatsLeft_T_106;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_17 <= 7'h7f;
    end else if (latch_17 & |readys_filter_lo_17) begin
      readys_mask_17 <= _readys_mask_T_124;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_17_0 <= 1'h0;
    end else if (idle_17) begin
      state_17_0 <= earlyWinner_17_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_17_1 <= 1'h0;
    end else if (idle_17) begin
      state_17_1 <= earlyWinner_17_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_17_2 <= 1'h0;
    end else if (idle_17) begin
      state_17_2 <= earlyWinner_17_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_17_3 <= 1'h0;
    end else if (idle_17) begin
      state_17_3 <= earlyWinner_17_3;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_17_4 <= 1'h0;
    end else if (idle_17) begin
      state_17_4 <= earlyWinner_17_4;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_17_5 <= 1'h0;
    end else if (idle_17) begin
      state_17_5 <= earlyWinner_17_5;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_17_6 <= 1'h0;
    end else if (idle_17) begin
      state_17_6 <= earlyWinner_17_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_18 <= 2'h0;
    end else if (latch_18) begin
      beatsLeft_18 <= 2'h0;
    end else begin
      beatsLeft_18 <= _beatsLeft_T_112;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_18 <= 4'hf;
    end else if (latch_18 & |readys_filter_lo_18) begin
      readys_mask_18 <= _readys_mask_T_132;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_18_0 <= 1'h0;
    end else if (idle_18) begin
      state_18_0 <= earlyWinner_18_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_18_1 <= 1'h0;
    end else if (idle_18) begin
      state_18_1 <= earlyWinner_18_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_18_2 <= 1'h0;
    end else if (idle_18) begin
      state_18_2 <= earlyWinner_18_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_18_3 <= 1'h0;
    end else if (idle_18) begin
      state_18_3 <= earlyWinner_18_3;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_19 <= 4'h0;
    end else if (latch_19) begin
      beatsLeft_19 <= initBeats_19;
    end else begin
      beatsLeft_19 <= _beatsLeft_T_118;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_19 <= 7'h7f;
    end else if (latch_19 & |readys_filter_lo_19) begin
      readys_mask_19 <= _readys_mask_T_143;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_19_0 <= 1'h0;
    end else if (idle_19) begin
      state_19_0 <= earlyWinner_19_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_19_1 <= 1'h0;
    end else if (idle_19) begin
      state_19_1 <= earlyWinner_19_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_19_2 <= 1'h0;
    end else if (idle_19) begin
      state_19_2 <= earlyWinner_19_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_19_3 <= 1'h0;
    end else if (idle_19) begin
      state_19_3 <= earlyWinner_19_3;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_19_4 <= 1'h0;
    end else if (idle_19) begin
      state_19_4 <= earlyWinner_19_4;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_19_5 <= 1'h0;
    end else if (idle_19) begin
      state_19_5 <= earlyWinner_19_5;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_19_6 <= 1'h0;
    end else if (idle_19) begin
      state_19_6 <= earlyWinner_19_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_4 <= 1'h0;
    end else if (latch_4) begin
      beatsLeft_4 <= 1'h0;
    end else begin
      beatsLeft_4 <= beatsLeft_4 - _beatsLeft_T_26;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_4 <= 2'h3;
    end else if (latch_4 & |readys_filter_lo_4) begin
      readys_mask_4 <= _readys_mask_T_32;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_4_0 <= 1'h0;
    end else if (idle_4) begin
      state_4_0 <= earlyWinner_4_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_4_1 <= 1'h0;
    end else if (idle_4) begin
      state_4_1 <= earlyWinner_4_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_7 <= 1'h0;
    end else if (latch_7) begin
      beatsLeft_7 <= 1'h0;
    end else begin
      beatsLeft_7 <= beatsLeft_7 - _beatsLeft_T_44;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_7 <= 2'h3;
    end else if (latch_7 & |readys_filter_lo_7) begin
      readys_mask_7 <= _readys_mask_T_50;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_7_0 <= 1'h0;
    end else if (idle_7) begin
      state_7_0 <= earlyWinner_7_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_7_1 <= 1'h0;
    end else if (idle_7) begin
      state_7_1 <= earlyWinner_7_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_10 <= 1'h0;
    end else if (latch_10) begin
      beatsLeft_10 <= 1'h0;
    end else begin
      beatsLeft_10 <= beatsLeft_10 - _beatsLeft_T_62;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_10 <= 2'h3;
    end else if (latch_10 & |readys_filter_lo_10) begin
      readys_mask_10 <= _readys_mask_T_68;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_10_0 <= 1'h0;
    end else if (idle_10) begin
      state_10_0 <= earlyWinner_10_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_10_1 <= 1'h0;
    end else if (idle_10) begin
      state_10_1 <= earlyWinner_10_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_13 <= 1'h0;
    end else if (latch_13) begin
      beatsLeft_13 <= 1'h0;
    end else begin
      beatsLeft_13 <= beatsLeft_13 - _beatsLeft_T_80;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_13 <= 2'h3;
    end else if (latch_13 & |readys_filter_lo_13) begin
      readys_mask_13 <= _readys_mask_T_86;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_13_0 <= 1'h0;
    end else if (idle_13) begin
      state_13_0 <= earlyWinner_13_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_13_1 <= 1'h0;
    end else if (idle_13) begin
      state_13_1 <= earlyWinner_13_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft <= 4'h0;
    end else if (latch) begin
      beatsLeft <= initBeats;
    end else begin
      beatsLeft <= _beatsLeft_T_4;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask <= 3'h7;
    end else if (latch & |readys_filter_lo) begin
      readys_mask <= _readys_mask_T_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state__0 <= 1'h0;
    end else if (idle) begin
      state__0 <= earlyWinner__0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_1 <= 4'h0;
    end else if (latch_1) begin
      beatsLeft_1 <= initBeats_1;
    end else begin
      beatsLeft_1 <= _beatsLeft_T_10;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_1 <= 3'h7;
    end else if (latch_1 & |readys_filter_lo_1) begin
      readys_mask_1 <= _readys_mask_T_14;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_1_0 <= 1'h0;
    end else if (idle_1) begin
      state_1_0 <= earlyWinner_1_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_2 <= 4'h0;
    end else if (latch_2) begin
      beatsLeft_2 <= initBeats_2;
    end else begin
      beatsLeft_2 <= _beatsLeft_T_16;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_2 <= 3'h7;
    end else if (latch_2 & |readys_filter_lo_2) begin
      readys_mask_2 <= _readys_mask_T_22;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_2_0 <= 1'h0;
    end else if (idle_2) begin
      state_2_0 <= earlyWinner_2_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_5 <= 4'h0;
    end else if (latch_5) begin
      beatsLeft_5 <= initBeats_5;
    end else begin
      beatsLeft_5 <= _beatsLeft_T_34;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_5 <= 3'h7;
    end else if (latch_5 & |readys_filter_lo_5) begin
      readys_mask_5 <= _readys_mask_T_40;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_5_0 <= 1'h0;
    end else if (idle_5) begin
      state_5_0 <= earlyWinner_5_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_8 <= 4'h0;
    end else if (latch_8) begin
      beatsLeft_8 <= initBeats_8;
    end else begin
      beatsLeft_8 <= _beatsLeft_T_52;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_8 <= 3'h7;
    end else if (latch_8 & |readys_filter_lo_8) begin
      readys_mask_8 <= _readys_mask_T_58;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_8_0 <= 1'h0;
    end else if (idle_8) begin
      state_8_0 <= earlyWinner_8_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_11 <= 4'h0;
    end else if (latch_11) begin
      beatsLeft_11 <= initBeats_11;
    end else begin
      beatsLeft_11 <= _beatsLeft_T_70;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_11 <= 3'h7;
    end else if (latch_11 & |readys_filter_lo_11) begin
      readys_mask_11 <= _readys_mask_T_76;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_11_0 <= 1'h0;
    end else if (idle_11) begin
      state_11_0 <= earlyWinner_11_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_14 <= 4'h0;
    end else if (latch_14) begin
      beatsLeft_14 <= initBeats_14;
    end else begin
      beatsLeft_14 <= _beatsLeft_T_88;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_14 <= 3'h7;
    end else if (latch_14 & |readys_filter_lo_14) begin
      readys_mask_14 <= _readys_mask_T_94;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_14_0 <= 1'h0;
    end else if (idle_14) begin
      state_14_0 <= earlyWinner_14_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state__1 <= 1'h0;
    end else if (idle) begin
      state__1 <= earlyWinner__1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_1_1 <= 1'h0;
    end else if (idle_1) begin
      state_1_1 <= earlyWinner_1_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_2_1 <= 1'h0;
    end else if (idle_2) begin
      state_2_1 <= earlyWinner_2_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_5_1 <= 1'h0;
    end else if (idle_5) begin
      state_5_1 <= earlyWinner_5_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_8_1 <= 1'h0;
    end else if (idle_8) begin
      state_8_1 <= earlyWinner_8_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_11_1 <= 1'h0;
    end else if (idle_11) begin
      state_11_1 <= earlyWinner_11_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_14_1 <= 1'h0;
    end else if (idle_14) begin
      state_14_1 <= earlyWinner_14_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state__2 <= 1'h0;
    end else if (idle) begin
      state__2 <= earlyWinner__2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_1_2 <= 1'h0;
    end else if (idle_1) begin
      state_1_2 <= earlyWinner_1_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_2_2 <= 1'h0;
    end else if (idle_2) begin
      state_2_2 <= earlyWinner_2_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_5_2 <= 1'h0;
    end else if (idle_5) begin
      state_5_2 <= earlyWinner_5_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_8_2 <= 1'h0;
    end else if (idle_8) begin
      state_8_2 <= earlyWinner_8_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_11_2 <= 1'h0;
    end else if (idle_11) begin
      state_11_2 <= earlyWinner_11_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_14_2 <= 1'h0;
    end else if (idle_14) begin
      state_14_2 <= earlyWinner_14_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_3 <= 4'h0;
    end else if (latch_3) begin
      beatsLeft_3 <= initBeats_3;
    end else begin
      beatsLeft_3 <= _beatsLeft_T_22;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_3 <= 2'h3;
    end else if (latch_3 & |readys_filter_lo_3) begin
      readys_mask_3 <= _readys_mask_T_27;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_3_0 <= 1'h0;
    end else if (idle_3) begin
      state_3_0 <= earlyWinner_3_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_6 <= 4'h0;
    end else if (latch_6) begin
      beatsLeft_6 <= initBeats_6;
    end else begin
      beatsLeft_6 <= _beatsLeft_T_40;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_6 <= 2'h3;
    end else if (latch_6 & |readys_filter_lo_6) begin
      readys_mask_6 <= _readys_mask_T_45;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_6_0 <= 1'h0;
    end else if (idle_6) begin
      state_6_0 <= earlyWinner_6_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_9 <= 4'h0;
    end else if (latch_9) begin
      beatsLeft_9 <= initBeats_9;
    end else begin
      beatsLeft_9 <= _beatsLeft_T_58;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_9 <= 2'h3;
    end else if (latch_9 & |readys_filter_lo_9) begin
      readys_mask_9 <= _readys_mask_T_63;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_9_0 <= 1'h0;
    end else if (idle_9) begin
      state_9_0 <= earlyWinner_9_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_12 <= 4'h0;
    end else if (latch_12) begin
      beatsLeft_12 <= initBeats_12;
    end else begin
      beatsLeft_12 <= _beatsLeft_T_76;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask_12 <= 2'h3;
    end else if (latch_12 & |readys_filter_lo_12) begin
      readys_mask_12 <= _readys_mask_T_81;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_12_0 <= 1'h0;
    end else if (idle_12) begin
      state_12_0 <= earlyWinner_12_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_3_1 <= 1'h0;
    end else if (idle_3) begin
      state_3_1 <= earlyWinner_3_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_6_1 <= 1'h0;
    end else if (idle_6) begin
      state_6_1 <= earlyWinner_6_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_9_1 <= 1'h0;
    end else if (idle_9) begin
      state_9_1 <= earlyWinner_9_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_12_1 <= 1'h0;
    end else if (idle_12) begin
      state_12_1 <= earlyWinner_12_1;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  beatsLeft_15 = _RAND_0[3:0];
  _RAND_1 = {1{`RANDOM}};
  readys_mask_15 = _RAND_1[6:0];
  _RAND_2 = {1{`RANDOM}};
  state_15_0 = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  state_15_1 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  state_15_2 = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  state_15_3 = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  state_15_4 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  state_15_5 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  state_15_6 = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  beatsLeft_16 = _RAND_9[1:0];
  _RAND_10 = {1{`RANDOM}};
  readys_mask_16 = _RAND_10[3:0];
  _RAND_11 = {1{`RANDOM}};
  state_16_0 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  state_16_1 = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  state_16_2 = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  state_16_3 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  beatsLeft_17 = _RAND_15[3:0];
  _RAND_16 = {1{`RANDOM}};
  readys_mask_17 = _RAND_16[6:0];
  _RAND_17 = {1{`RANDOM}};
  state_17_0 = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  state_17_1 = _RAND_18[0:0];
  _RAND_19 = {1{`RANDOM}};
  state_17_2 = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  state_17_3 = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  state_17_4 = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  state_17_5 = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  state_17_6 = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  beatsLeft_18 = _RAND_24[1:0];
  _RAND_25 = {1{`RANDOM}};
  readys_mask_18 = _RAND_25[3:0];
  _RAND_26 = {1{`RANDOM}};
  state_18_0 = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  state_18_1 = _RAND_27[0:0];
  _RAND_28 = {1{`RANDOM}};
  state_18_2 = _RAND_28[0:0];
  _RAND_29 = {1{`RANDOM}};
  state_18_3 = _RAND_29[0:0];
  _RAND_30 = {1{`RANDOM}};
  beatsLeft_19 = _RAND_30[3:0];
  _RAND_31 = {1{`RANDOM}};
  readys_mask_19 = _RAND_31[6:0];
  _RAND_32 = {1{`RANDOM}};
  state_19_0 = _RAND_32[0:0];
  _RAND_33 = {1{`RANDOM}};
  state_19_1 = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  state_19_2 = _RAND_34[0:0];
  _RAND_35 = {1{`RANDOM}};
  state_19_3 = _RAND_35[0:0];
  _RAND_36 = {1{`RANDOM}};
  state_19_4 = _RAND_36[0:0];
  _RAND_37 = {1{`RANDOM}};
  state_19_5 = _RAND_37[0:0];
  _RAND_38 = {1{`RANDOM}};
  state_19_6 = _RAND_38[0:0];
  _RAND_39 = {1{`RANDOM}};
  beatsLeft_4 = _RAND_39[0:0];
  _RAND_40 = {1{`RANDOM}};
  readys_mask_4 = _RAND_40[1:0];
  _RAND_41 = {1{`RANDOM}};
  state_4_0 = _RAND_41[0:0];
  _RAND_42 = {1{`RANDOM}};
  state_4_1 = _RAND_42[0:0];
  _RAND_43 = {1{`RANDOM}};
  beatsLeft_7 = _RAND_43[0:0];
  _RAND_44 = {1{`RANDOM}};
  readys_mask_7 = _RAND_44[1:0];
  _RAND_45 = {1{`RANDOM}};
  state_7_0 = _RAND_45[0:0];
  _RAND_46 = {1{`RANDOM}};
  state_7_1 = _RAND_46[0:0];
  _RAND_47 = {1{`RANDOM}};
  beatsLeft_10 = _RAND_47[0:0];
  _RAND_48 = {1{`RANDOM}};
  readys_mask_10 = _RAND_48[1:0];
  _RAND_49 = {1{`RANDOM}};
  state_10_0 = _RAND_49[0:0];
  _RAND_50 = {1{`RANDOM}};
  state_10_1 = _RAND_50[0:0];
  _RAND_51 = {1{`RANDOM}};
  beatsLeft_13 = _RAND_51[0:0];
  _RAND_52 = {1{`RANDOM}};
  readys_mask_13 = _RAND_52[1:0];
  _RAND_53 = {1{`RANDOM}};
  state_13_0 = _RAND_53[0:0];
  _RAND_54 = {1{`RANDOM}};
  state_13_1 = _RAND_54[0:0];
  _RAND_55 = {1{`RANDOM}};
  beatsLeft = _RAND_55[3:0];
  _RAND_56 = {1{`RANDOM}};
  readys_mask = _RAND_56[2:0];
  _RAND_57 = {1{`RANDOM}};
  state__0 = _RAND_57[0:0];
  _RAND_58 = {1{`RANDOM}};
  beatsLeft_1 = _RAND_58[3:0];
  _RAND_59 = {1{`RANDOM}};
  readys_mask_1 = _RAND_59[2:0];
  _RAND_60 = {1{`RANDOM}};
  state_1_0 = _RAND_60[0:0];
  _RAND_61 = {1{`RANDOM}};
  beatsLeft_2 = _RAND_61[3:0];
  _RAND_62 = {1{`RANDOM}};
  readys_mask_2 = _RAND_62[2:0];
  _RAND_63 = {1{`RANDOM}};
  state_2_0 = _RAND_63[0:0];
  _RAND_64 = {1{`RANDOM}};
  beatsLeft_5 = _RAND_64[3:0];
  _RAND_65 = {1{`RANDOM}};
  readys_mask_5 = _RAND_65[2:0];
  _RAND_66 = {1{`RANDOM}};
  state_5_0 = _RAND_66[0:0];
  _RAND_67 = {1{`RANDOM}};
  beatsLeft_8 = _RAND_67[3:0];
  _RAND_68 = {1{`RANDOM}};
  readys_mask_8 = _RAND_68[2:0];
  _RAND_69 = {1{`RANDOM}};
  state_8_0 = _RAND_69[0:0];
  _RAND_70 = {1{`RANDOM}};
  beatsLeft_11 = _RAND_70[3:0];
  _RAND_71 = {1{`RANDOM}};
  readys_mask_11 = _RAND_71[2:0];
  _RAND_72 = {1{`RANDOM}};
  state_11_0 = _RAND_72[0:0];
  _RAND_73 = {1{`RANDOM}};
  beatsLeft_14 = _RAND_73[3:0];
  _RAND_74 = {1{`RANDOM}};
  readys_mask_14 = _RAND_74[2:0];
  _RAND_75 = {1{`RANDOM}};
  state_14_0 = _RAND_75[0:0];
  _RAND_76 = {1{`RANDOM}};
  state__1 = _RAND_76[0:0];
  _RAND_77 = {1{`RANDOM}};
  state_1_1 = _RAND_77[0:0];
  _RAND_78 = {1{`RANDOM}};
  state_2_1 = _RAND_78[0:0];
  _RAND_79 = {1{`RANDOM}};
  state_5_1 = _RAND_79[0:0];
  _RAND_80 = {1{`RANDOM}};
  state_8_1 = _RAND_80[0:0];
  _RAND_81 = {1{`RANDOM}};
  state_11_1 = _RAND_81[0:0];
  _RAND_82 = {1{`RANDOM}};
  state_14_1 = _RAND_82[0:0];
  _RAND_83 = {1{`RANDOM}};
  state__2 = _RAND_83[0:0];
  _RAND_84 = {1{`RANDOM}};
  state_1_2 = _RAND_84[0:0];
  _RAND_85 = {1{`RANDOM}};
  state_2_2 = _RAND_85[0:0];
  _RAND_86 = {1{`RANDOM}};
  state_5_2 = _RAND_86[0:0];
  _RAND_87 = {1{`RANDOM}};
  state_8_2 = _RAND_87[0:0];
  _RAND_88 = {1{`RANDOM}};
  state_11_2 = _RAND_88[0:0];
  _RAND_89 = {1{`RANDOM}};
  state_14_2 = _RAND_89[0:0];
  _RAND_90 = {1{`RANDOM}};
  beatsLeft_3 = _RAND_90[3:0];
  _RAND_91 = {1{`RANDOM}};
  readys_mask_3 = _RAND_91[1:0];
  _RAND_92 = {1{`RANDOM}};
  state_3_0 = _RAND_92[0:0];
  _RAND_93 = {1{`RANDOM}};
  beatsLeft_6 = _RAND_93[3:0];
  _RAND_94 = {1{`RANDOM}};
  readys_mask_6 = _RAND_94[1:0];
  _RAND_95 = {1{`RANDOM}};
  state_6_0 = _RAND_95[0:0];
  _RAND_96 = {1{`RANDOM}};
  beatsLeft_9 = _RAND_96[3:0];
  _RAND_97 = {1{`RANDOM}};
  readys_mask_9 = _RAND_97[1:0];
  _RAND_98 = {1{`RANDOM}};
  state_9_0 = _RAND_98[0:0];
  _RAND_99 = {1{`RANDOM}};
  beatsLeft_12 = _RAND_99[3:0];
  _RAND_100 = {1{`RANDOM}};
  readys_mask_12 = _RAND_100[1:0];
  _RAND_101 = {1{`RANDOM}};
  state_12_0 = _RAND_101[0:0];
  _RAND_102 = {1{`RANDOM}};
  state_3_1 = _RAND_102[0:0];
  _RAND_103 = {1{`RANDOM}};
  state_6_1 = _RAND_103[0:0];
  _RAND_104 = {1{`RANDOM}};
  state_9_1 = _RAND_104[0:0];
  _RAND_105 = {1{`RANDOM}};
  state_12_1 = _RAND_105[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    beatsLeft_15 = 4'h0;
  end
  if (reset) begin
    readys_mask_15 = 7'h7f;
  end
  if (reset) begin
    state_15_0 = 1'h0;
  end
  if (reset) begin
    state_15_1 = 1'h0;
  end
  if (reset) begin
    state_15_2 = 1'h0;
  end
  if (reset) begin
    state_15_3 = 1'h0;
  end
  if (reset) begin
    state_15_4 = 1'h0;
  end
  if (reset) begin
    state_15_5 = 1'h0;
  end
  if (reset) begin
    state_15_6 = 1'h0;
  end
  if (reset) begin
    beatsLeft_16 = 2'h0;
  end
  if (reset) begin
    readys_mask_16 = 4'hf;
  end
  if (reset) begin
    state_16_0 = 1'h0;
  end
  if (reset) begin
    state_16_1 = 1'h0;
  end
  if (reset) begin
    state_16_2 = 1'h0;
  end
  if (reset) begin
    state_16_3 = 1'h0;
  end
  if (reset) begin
    beatsLeft_17 = 4'h0;
  end
  if (reset) begin
    readys_mask_17 = 7'h7f;
  end
  if (reset) begin
    state_17_0 = 1'h0;
  end
  if (reset) begin
    state_17_1 = 1'h0;
  end
  if (reset) begin
    state_17_2 = 1'h0;
  end
  if (reset) begin
    state_17_3 = 1'h0;
  end
  if (reset) begin
    state_17_4 = 1'h0;
  end
  if (reset) begin
    state_17_5 = 1'h0;
  end
  if (reset) begin
    state_17_6 = 1'h0;
  end
  if (reset) begin
    beatsLeft_18 = 2'h0;
  end
  if (reset) begin
    readys_mask_18 = 4'hf;
  end
  if (reset) begin
    state_18_0 = 1'h0;
  end
  if (reset) begin
    state_18_1 = 1'h0;
  end
  if (reset) begin
    state_18_2 = 1'h0;
  end
  if (reset) begin
    state_18_3 = 1'h0;
  end
  if (reset) begin
    beatsLeft_19 = 4'h0;
  end
  if (reset) begin
    readys_mask_19 = 7'h7f;
  end
  if (reset) begin
    state_19_0 = 1'h0;
  end
  if (reset) begin
    state_19_1 = 1'h0;
  end
  if (reset) begin
    state_19_2 = 1'h0;
  end
  if (reset) begin
    state_19_3 = 1'h0;
  end
  if (reset) begin
    state_19_4 = 1'h0;
  end
  if (reset) begin
    state_19_5 = 1'h0;
  end
  if (reset) begin
    state_19_6 = 1'h0;
  end
  if (reset) begin
    beatsLeft_4 = 1'h0;
  end
  if (reset) begin
    readys_mask_4 = 2'h3;
  end
  if (reset) begin
    state_4_0 = 1'h0;
  end
  if (reset) begin
    state_4_1 = 1'h0;
  end
  if (reset) begin
    beatsLeft_7 = 1'h0;
  end
  if (reset) begin
    readys_mask_7 = 2'h3;
  end
  if (reset) begin
    state_7_0 = 1'h0;
  end
  if (reset) begin
    state_7_1 = 1'h0;
  end
  if (reset) begin
    beatsLeft_10 = 1'h0;
  end
  if (reset) begin
    readys_mask_10 = 2'h3;
  end
  if (reset) begin
    state_10_0 = 1'h0;
  end
  if (reset) begin
    state_10_1 = 1'h0;
  end
  if (reset) begin
    beatsLeft_13 = 1'h0;
  end
  if (reset) begin
    readys_mask_13 = 2'h3;
  end
  if (reset) begin
    state_13_0 = 1'h0;
  end
  if (reset) begin
    state_13_1 = 1'h0;
  end
  if (reset) begin
    beatsLeft = 4'h0;
  end
  if (reset) begin
    readys_mask = 3'h7;
  end
  if (reset) begin
    state__0 = 1'h0;
  end
  if (reset) begin
    beatsLeft_1 = 4'h0;
  end
  if (reset) begin
    readys_mask_1 = 3'h7;
  end
  if (reset) begin
    state_1_0 = 1'h0;
  end
  if (reset) begin
    beatsLeft_2 = 4'h0;
  end
  if (reset) begin
    readys_mask_2 = 3'h7;
  end
  if (reset) begin
    state_2_0 = 1'h0;
  end
  if (reset) begin
    beatsLeft_5 = 4'h0;
  end
  if (reset) begin
    readys_mask_5 = 3'h7;
  end
  if (reset) begin
    state_5_0 = 1'h0;
  end
  if (reset) begin
    beatsLeft_8 = 4'h0;
  end
  if (reset) begin
    readys_mask_8 = 3'h7;
  end
  if (reset) begin
    state_8_0 = 1'h0;
  end
  if (reset) begin
    beatsLeft_11 = 4'h0;
  end
  if (reset) begin
    readys_mask_11 = 3'h7;
  end
  if (reset) begin
    state_11_0 = 1'h0;
  end
  if (reset) begin
    beatsLeft_14 = 4'h0;
  end
  if (reset) begin
    readys_mask_14 = 3'h7;
  end
  if (reset) begin
    state_14_0 = 1'h0;
  end
  if (reset) begin
    state__1 = 1'h0;
  end
  if (reset) begin
    state_1_1 = 1'h0;
  end
  if (reset) begin
    state_2_1 = 1'h0;
  end
  if (reset) begin
    state_5_1 = 1'h0;
  end
  if (reset) begin
    state_8_1 = 1'h0;
  end
  if (reset) begin
    state_11_1 = 1'h0;
  end
  if (reset) begin
    state_14_1 = 1'h0;
  end
  if (reset) begin
    state__2 = 1'h0;
  end
  if (reset) begin
    state_1_2 = 1'h0;
  end
  if (reset) begin
    state_2_2 = 1'h0;
  end
  if (reset) begin
    state_5_2 = 1'h0;
  end
  if (reset) begin
    state_8_2 = 1'h0;
  end
  if (reset) begin
    state_11_2 = 1'h0;
  end
  if (reset) begin
    state_14_2 = 1'h0;
  end
  if (reset) begin
    beatsLeft_3 = 4'h0;
  end
  if (reset) begin
    readys_mask_3 = 2'h3;
  end
  if (reset) begin
    state_3_0 = 1'h0;
  end
  if (reset) begin
    beatsLeft_6 = 4'h0;
  end
  if (reset) begin
    readys_mask_6 = 2'h3;
  end
  if (reset) begin
    state_6_0 = 1'h0;
  end
  if (reset) begin
    beatsLeft_9 = 4'h0;
  end
  if (reset) begin
    readys_mask_9 = 2'h3;
  end
  if (reset) begin
    state_9_0 = 1'h0;
  end
  if (reset) begin
    beatsLeft_12 = 4'h0;
  end
  if (reset) begin
    readys_mask_12 = 2'h3;
  end
  if (reset) begin
    state_12_0 = 1'h0;
  end
  if (reset) begin
    state_3_1 = 1'h0;
  end
  if (reset) begin
    state_6_1 = 1'h0;
  end
  if (reset) begin
    state_9_1 = 1'h0;
  end
  if (reset) begin
    state_12_1 = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
