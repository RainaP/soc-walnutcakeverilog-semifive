//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_DataTap_2(
  output        _6,
  output [7:0]  _5,
  output [63:0] _4,
  output        _3,
  output        _2_ready,
  output        _2_valid,
  output [39:0] _2_bits_addr,
  output [6:0]  _2_bits_tag,
  output [4:0]  _2_bits_cmd,
  output [1:0]  _2_bits_size,
  output        _2_bits_signed,
  output [1:0]  _2_bits_dprv,
  output        _2_bits_phys,
  output        _2_bits_no_alloc,
  output        _2_bits_no_xcpt,
  output [63:0] _2_bits_data,
  output [7:0]  _2_bits_mask,
  output        _1,
  output        _0
);
  assign _6 = rocket.core.io_dmem_s2_nack;
  assign _5 = rocket.core.io_dmem_s1_data_mask;
  assign _4 = rocket.core.io_dmem_s1_data_data;
  assign _3 = rocket.core.io_dmem_s1_kill;
  assign _2_ready = rocket.core.io_dmem_req_ready;
  assign _2_valid = rocket.core.io_dmem_req_valid;
  assign _2_bits_addr = rocket.core.io_dmem_req_bits_addr;
  assign _2_bits_tag = rocket.core.io_dmem_req_bits_tag;
  assign _2_bits_cmd = rocket.core.io_dmem_req_bits_cmd;
  assign _2_bits_size = rocket.core.io_dmem_req_bits_size;
  assign _2_bits_signed = rocket.core.io_dmem_req_bits_signed;
  assign _2_bits_dprv = rocket.core.io_dmem_req_bits_dprv;
  assign _2_bits_phys = rocket.core.io_dmem_req_bits_phys;
  assign _2_bits_no_alloc = rocket.core.io_dmem_req_bits_no_alloc;
  assign _2_bits_no_xcpt = rocket.core.io_dmem_req_bits_no_xcpt;
  assign _2_bits_data = rocket.core.io_dmem_req_bits_data;
  assign _2_bits_mask = rocket.core.io_dmem_req_bits_mask;
  assign _1 = rocket.core.reset;
  assign _0 = rocket.core.rocket_clock_gate_out;
endmodule
