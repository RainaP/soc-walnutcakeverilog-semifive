//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_91_assert(
  input         clock,
  input         reset,
  input         io_in_a_ready,
  input         io_in_a_valid,
  input  [2:0]  io_in_a_bits_opcode,
  input  [2:0]  io_in_a_bits_param,
  input  [2:0]  io_in_a_bits_size,
  input  [2:0]  io_in_a_bits_source,
  input  [35:0] io_in_a_bits_address,
  input  [15:0] io_in_a_bits_mask,
  input         io_in_a_bits_corrupt,
  input         io_in_c_ready,
  input         io_in_c_valid,
  input  [2:0]  io_in_c_bits_opcode,
  input  [2:0]  io_in_c_bits_param,
  input  [2:0]  io_in_c_bits_size,
  input  [2:0]  io_in_c_bits_source,
  input  [35:0] io_in_c_bits_address,
  input         io_in_c_bits_corrupt,
  input         io_in_d_ready,
  input         io_in_d_valid,
  input  [2:0]  io_in_d_bits_opcode,
  input  [1:0]  io_in_d_bits_param,
  input  [2:0]  io_in_d_bits_size,
  input  [2:0]  io_in_d_bits_source,
  input  [2:0]  io_in_d_bits_sink,
  input         io_in_d_bits_denied,
  input         io_in_d_bits_corrupt,
  input         io_in_e_valid,
  input  [2:0]  io_in_e_bits_sink
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire [12:0] _T_14; // @[package.scala 189:77]
  wire [5:0] _T_16; // @[package.scala 189:46]
  wire [35:0] _GEN_33; // @[Edges.scala 22:16]
  wire [35:0] _T_17; // @[Edges.scala 22:16]
  wire  _T_18; // @[Edges.scala 22:24]
  wire [3:0] _T_19; // @[Misc.scala 201:34]
  wire [3:0] _T_21; // @[OneHot.scala 65:12]
  wire [3:0] _T_23; // @[Misc.scala 201:81]
  wire  _T_24; // @[Misc.scala 205:21]
  wire  _T_27; // @[Misc.scala 210:20]
  wire  _T_29; // @[Misc.scala 214:38]
  wire  _T_30; // @[Misc.scala 214:29]
  wire  _T_32; // @[Misc.scala 214:38]
  wire  _T_33; // @[Misc.scala 214:29]
  wire  _T_36; // @[Misc.scala 210:20]
  wire  _T_37; // @[Misc.scala 213:27]
  wire  _T_38; // @[Misc.scala 214:38]
  wire  _T_39; // @[Misc.scala 214:29]
  wire  _T_40; // @[Misc.scala 213:27]
  wire  _T_41; // @[Misc.scala 214:38]
  wire  _T_42; // @[Misc.scala 214:29]
  wire  _T_43; // @[Misc.scala 213:27]
  wire  _T_44; // @[Misc.scala 214:38]
  wire  _T_45; // @[Misc.scala 214:29]
  wire  _T_46; // @[Misc.scala 213:27]
  wire  _T_47; // @[Misc.scala 214:38]
  wire  _T_48; // @[Misc.scala 214:29]
  wire  _T_51; // @[Misc.scala 210:20]
  wire  _T_52; // @[Misc.scala 213:27]
  wire  _T_53; // @[Misc.scala 214:38]
  wire  _T_54; // @[Misc.scala 214:29]
  wire  _T_55; // @[Misc.scala 213:27]
  wire  _T_56; // @[Misc.scala 214:38]
  wire  _T_57; // @[Misc.scala 214:29]
  wire  _T_58; // @[Misc.scala 213:27]
  wire  _T_59; // @[Misc.scala 214:38]
  wire  _T_60; // @[Misc.scala 214:29]
  wire  _T_61; // @[Misc.scala 213:27]
  wire  _T_62; // @[Misc.scala 214:38]
  wire  _T_63; // @[Misc.scala 214:29]
  wire  _T_64; // @[Misc.scala 213:27]
  wire  _T_65; // @[Misc.scala 214:38]
  wire  _T_66; // @[Misc.scala 214:29]
  wire  _T_67; // @[Misc.scala 213:27]
  wire  _T_68; // @[Misc.scala 214:38]
  wire  _T_69; // @[Misc.scala 214:29]
  wire  _T_70; // @[Misc.scala 213:27]
  wire  _T_71; // @[Misc.scala 214:38]
  wire  _T_72; // @[Misc.scala 214:29]
  wire  _T_73; // @[Misc.scala 213:27]
  wire  _T_74; // @[Misc.scala 214:38]
  wire  _T_75; // @[Misc.scala 214:29]
  wire  _T_78; // @[Misc.scala 210:20]
  wire  _T_79; // @[Misc.scala 213:27]
  wire  _T_80; // @[Misc.scala 214:38]
  wire  _T_81; // @[Misc.scala 214:29]
  wire  _T_82; // @[Misc.scala 213:27]
  wire  _T_83; // @[Misc.scala 214:38]
  wire  _T_84; // @[Misc.scala 214:29]
  wire  _T_85; // @[Misc.scala 213:27]
  wire  _T_86; // @[Misc.scala 214:38]
  wire  _T_87; // @[Misc.scala 214:29]
  wire  _T_88; // @[Misc.scala 213:27]
  wire  _T_89; // @[Misc.scala 214:38]
  wire  _T_90; // @[Misc.scala 214:29]
  wire  _T_91; // @[Misc.scala 213:27]
  wire  _T_92; // @[Misc.scala 214:38]
  wire  _T_93; // @[Misc.scala 214:29]
  wire  _T_94; // @[Misc.scala 213:27]
  wire  _T_95; // @[Misc.scala 214:38]
  wire  _T_96; // @[Misc.scala 214:29]
  wire  _T_97; // @[Misc.scala 213:27]
  wire  _T_98; // @[Misc.scala 214:38]
  wire  _T_99; // @[Misc.scala 214:29]
  wire  _T_100; // @[Misc.scala 213:27]
  wire  _T_101; // @[Misc.scala 214:38]
  wire  _T_102; // @[Misc.scala 214:29]
  wire  _T_103; // @[Misc.scala 213:27]
  wire  _T_104; // @[Misc.scala 214:38]
  wire  _T_105; // @[Misc.scala 214:29]
  wire  _T_106; // @[Misc.scala 213:27]
  wire  _T_107; // @[Misc.scala 214:38]
  wire  _T_108; // @[Misc.scala 214:29]
  wire  _T_109; // @[Misc.scala 213:27]
  wire  _T_110; // @[Misc.scala 214:38]
  wire  _T_111; // @[Misc.scala 214:29]
  wire  _T_112; // @[Misc.scala 213:27]
  wire  _T_113; // @[Misc.scala 214:38]
  wire  _T_114; // @[Misc.scala 214:29]
  wire  _T_115; // @[Misc.scala 213:27]
  wire  _T_116; // @[Misc.scala 214:38]
  wire  _T_117; // @[Misc.scala 214:29]
  wire  _T_118; // @[Misc.scala 213:27]
  wire  _T_119; // @[Misc.scala 214:38]
  wire  _T_120; // @[Misc.scala 214:29]
  wire  _T_121; // @[Misc.scala 213:27]
  wire  _T_122; // @[Misc.scala 214:38]
  wire  _T_123; // @[Misc.scala 214:29]
  wire  _T_124; // @[Misc.scala 213:27]
  wire  _T_125; // @[Misc.scala 214:38]
  wire  _T_126; // @[Misc.scala 214:29]
  wire [7:0] _T_133; // @[Cat.scala 29:58]
  wire [15:0] _T_141; // @[Cat.scala 29:58]
  wire  _T_160; // @[Monitor.scala 79:25]
  wire  _T_162; // @[Parameters.scala 93:42]
  wire [35:0] _T_165; // @[Parameters.scala 137:31]
  wire [36:0] _T_166; // @[Parameters.scala 137:49]
  wire [36:0] _T_168; // @[Parameters.scala 137:52]
  wire  _T_169; // @[Parameters.scala 137:67]
  wire [35:0] _T_170; // @[Parameters.scala 137:31]
  wire [36:0] _T_171; // @[Parameters.scala 137:49]
  wire [36:0] _T_173; // @[Parameters.scala 137:52]
  wire  _T_174; // @[Parameters.scala 137:67]
  wire  _T_175; // @[Parameters.scala 552:42]
  wire  _T_176; // @[Parameters.scala 551:56]
  wire  _T_179; // @[Monitor.scala 44:11]
  wire  _T_180; // @[Monitor.scala 44:11]
  wire  _T_181; // @[Parameters.scala 92:48]
  wire  _T_183; // @[Monitor.scala 44:11]
  wire  _T_184; // @[Monitor.scala 44:11]
  wire  _T_190; // @[Monitor.scala 44:11]
  wire  _T_191; // @[Monitor.scala 44:11]
  wire  _T_193; // @[Monitor.scala 44:11]
  wire  _T_194; // @[Monitor.scala 44:11]
  wire  _T_195; // @[Bundles.scala 110:27]
  wire  _T_197; // @[Monitor.scala 44:11]
  wire  _T_198; // @[Monitor.scala 44:11]
  wire [15:0] _T_199; // @[Monitor.scala 86:18]
  wire  _T_200; // @[Monitor.scala 86:31]
  wire  _T_202; // @[Monitor.scala 44:11]
  wire  _T_203; // @[Monitor.scala 44:11]
  wire  _T_204; // @[Monitor.scala 87:18]
  wire  _T_206; // @[Monitor.scala 44:11]
  wire  _T_207; // @[Monitor.scala 44:11]
  wire  _T_208; // @[Monitor.scala 90:25]
  wire  _T_247; // @[Monitor.scala 97:31]
  wire  _T_249; // @[Monitor.scala 44:11]
  wire  _T_250; // @[Monitor.scala 44:11]
  wire  _T_260; // @[Monitor.scala 102:25]
  wire  _T_287; // @[Monitor.scala 106:31]
  wire  _T_289; // @[Monitor.scala 44:11]
  wire  _T_290; // @[Monitor.scala 44:11]
  wire  _T_291; // @[Monitor.scala 107:30]
  wire  _T_293; // @[Monitor.scala 44:11]
  wire  _T_294; // @[Monitor.scala 44:11]
  wire  _T_299; // @[Monitor.scala 111:25]
  wire  _T_334; // @[Monitor.scala 119:25]
  wire [15:0] _T_365; // @[Monitor.scala 124:33]
  wire [15:0] _T_366; // @[Monitor.scala 124:31]
  wire  _T_367; // @[Monitor.scala 124:40]
  wire  _T_369; // @[Monitor.scala 44:11]
  wire  _T_370; // @[Monitor.scala 44:11]
  wire  _T_371; // @[Monitor.scala 127:25]
  wire  _T_373; // @[Parameters.scala 93:42]
  wire  _T_381; // @[Parameters.scala 551:56]
  wire  _T_392; // @[Monitor.scala 44:11]
  wire  _T_393; // @[Monitor.scala 44:11]
  wire  _T_400; // @[Bundles.scala 140:33]
  wire  _T_402; // @[Monitor.scala 44:11]
  wire  _T_403; // @[Monitor.scala 44:11]
  wire  _T_408; // @[Monitor.scala 135:25]
  wire  _T_437; // @[Bundles.scala 147:30]
  wire  _T_439; // @[Monitor.scala 44:11]
  wire  _T_440; // @[Monitor.scala 44:11]
  wire  _T_445; // @[Monitor.scala 143:25]
  wire  _T_455; // @[Parameters.scala 551:56]
  wire  _T_466; // @[Monitor.scala 44:11]
  wire  _T_467; // @[Monitor.scala 44:11]
  wire  _T_474; // @[Bundles.scala 160:28]
  wire  _T_476; // @[Monitor.scala 44:11]
  wire  _T_477; // @[Monitor.scala 44:11]
  wire  _T_486; // @[Bundles.scala 44:24]
  wire  _T_488; // @[Monitor.scala 51:11]
  wire  _T_489; // @[Monitor.scala 51:11]
  wire  _T_500; // @[Monitor.scala 307:25]
  wire  _T_504; // @[Monitor.scala 309:27]
  wire  _T_506; // @[Monitor.scala 51:11]
  wire  _T_507; // @[Monitor.scala 51:11]
  wire  _T_508; // @[Monitor.scala 310:28]
  wire  _T_510; // @[Monitor.scala 51:11]
  wire  _T_511; // @[Monitor.scala 51:11]
  wire  _T_512; // @[Monitor.scala 311:15]
  wire  _T_514; // @[Monitor.scala 51:11]
  wire  _T_515; // @[Monitor.scala 51:11]
  wire  _T_516; // @[Monitor.scala 312:15]
  wire  _T_518; // @[Monitor.scala 51:11]
  wire  _T_519; // @[Monitor.scala 51:11]
  wire  _T_520; // @[Monitor.scala 315:25]
  wire  _T_531; // @[Bundles.scala 104:26]
  wire  _T_533; // @[Monitor.scala 51:11]
  wire  _T_534; // @[Monitor.scala 51:11]
  wire  _T_535; // @[Monitor.scala 320:28]
  wire  _T_537; // @[Monitor.scala 51:11]
  wire  _T_538; // @[Monitor.scala 51:11]
  wire  _T_548; // @[Monitor.scala 325:25]
  wire  _T_568; // @[Monitor.scala 331:30]
  wire  _T_570; // @[Monitor.scala 51:11]
  wire  _T_571; // @[Monitor.scala 51:11]
  wire  _T_577; // @[Monitor.scala 335:25]
  wire  _T_594; // @[Monitor.scala 343:25]
  wire  _T_612; // @[Monitor.scala 351:25]
  wire [12:0] _T_974; // @[package.scala 189:77]
  wire [5:0] _T_976; // @[package.scala 189:46]
  wire [35:0] _GEN_34; // @[Edges.scala 22:16]
  wire [35:0] _T_977; // @[Edges.scala 22:16]
  wire  _T_978; // @[Edges.scala 22:24]
  wire [35:0] _T_979; // @[Parameters.scala 137:31]
  wire [36:0] _T_980; // @[Parameters.scala 137:49]
  wire [36:0] _T_982; // @[Parameters.scala 137:52]
  wire  _T_983; // @[Parameters.scala 137:67]
  wire [35:0] _T_984; // @[Parameters.scala 137:31]
  wire [36:0] _T_985; // @[Parameters.scala 137:49]
  wire [36:0] _T_987; // @[Parameters.scala 137:52]
  wire  _T_988; // @[Parameters.scala 137:67]
  wire  _T_990; // @[Parameters.scala 535:64]
  wire  _T_1009; // @[Monitor.scala 239:25]
  wire  _T_1011; // @[Monitor.scala 44:11]
  wire  _T_1012; // @[Monitor.scala 44:11]
  wire  _T_1016; // @[Monitor.scala 242:30]
  wire  _T_1018; // @[Monitor.scala 44:11]
  wire  _T_1019; // @[Monitor.scala 44:11]
  wire  _T_1021; // @[Monitor.scala 44:11]
  wire  _T_1022; // @[Monitor.scala 44:11]
  wire  _T_1023; // @[Bundles.scala 122:29]
  wire  _T_1025; // @[Monitor.scala 44:11]
  wire  _T_1026; // @[Monitor.scala 44:11]
  wire  _T_1027; // @[Monitor.scala 245:18]
  wire  _T_1029; // @[Monitor.scala 44:11]
  wire  _T_1030; // @[Monitor.scala 44:11]
  wire  _T_1031; // @[Monitor.scala 248:25]
  wire  _T_1049; // @[Monitor.scala 256:25]
  wire  _T_1051; // @[Parameters.scala 93:42]
  wire  _T_1065; // @[Parameters.scala 551:56]
  wire  _T_1068; // @[Monitor.scala 44:11]
  wire  _T_1069; // @[Monitor.scala 44:11]
  wire  _T_1070; // @[Parameters.scala 92:48]
  wire  _T_1072; // @[Monitor.scala 44:11]
  wire  _T_1073; // @[Monitor.scala 44:11]
  wire  _T_1084; // @[Bundles.scala 116:29]
  wire  _T_1086; // @[Monitor.scala 44:11]
  wire  _T_1087; // @[Monitor.scala 44:11]
  wire  _T_1092; // @[Monitor.scala 266:25]
  wire  _T_1131; // @[Monitor.scala 275:25]
  wire  _T_1141; // @[Monitor.scala 279:31]
  wire  _T_1143; // @[Monitor.scala 44:11]
  wire  _T_1144; // @[Monitor.scala 44:11]
  wire  _T_1149; // @[Monitor.scala 283:25]
  wire  _T_1163; // @[Monitor.scala 290:25]
  wire  _T_1185; // @[Decoupled.scala 40:37]
  wire  _T_1192; // @[Edges.scala 93:28]
  reg [1:0] _T_1194; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire [1:0] _T_1196; // @[Edges.scala 231:28]
  wire  _T_1197; // @[Edges.scala 232:25]
  reg [2:0] _T_1205; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [2:0] _T_1206; // @[Monitor.scala 382:22]
  reg [31:0] _RAND_2;
  reg [2:0] _T_1207; // @[Monitor.scala 383:22]
  reg [31:0] _RAND_3;
  reg [2:0] _T_1208; // @[Monitor.scala 384:22]
  reg [31:0] _RAND_4;
  reg [35:0] _T_1209; // @[Monitor.scala 385:22]
  reg [63:0] _RAND_5;
  wire  _T_1210; // @[Monitor.scala 386:22]
  wire  _T_1211; // @[Monitor.scala 386:19]
  wire  _T_1212; // @[Monitor.scala 387:32]
  wire  _T_1214; // @[Monitor.scala 44:11]
  wire  _T_1215; // @[Monitor.scala 44:11]
  wire  _T_1216; // @[Monitor.scala 388:32]
  wire  _T_1218; // @[Monitor.scala 44:11]
  wire  _T_1219; // @[Monitor.scala 44:11]
  wire  _T_1220; // @[Monitor.scala 389:32]
  wire  _T_1222; // @[Monitor.scala 44:11]
  wire  _T_1223; // @[Monitor.scala 44:11]
  wire  _T_1224; // @[Monitor.scala 390:32]
  wire  _T_1226; // @[Monitor.scala 44:11]
  wire  _T_1227; // @[Monitor.scala 44:11]
  wire  _T_1228; // @[Monitor.scala 391:32]
  wire  _T_1230; // @[Monitor.scala 44:11]
  wire  _T_1231; // @[Monitor.scala 44:11]
  wire  _T_1233; // @[Monitor.scala 393:20]
  wire  _T_1234; // @[Decoupled.scala 40:37]
  wire [12:0] _T_1236; // @[package.scala 189:77]
  wire [5:0] _T_1238; // @[package.scala 189:46]
  reg [1:0] _T_1242; // @[Edges.scala 230:27]
  reg [31:0] _RAND_6;
  wire [1:0] _T_1244; // @[Edges.scala 231:28]
  wire  _T_1245; // @[Edges.scala 232:25]
  reg [2:0] _T_1253; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_7;
  reg [1:0] _T_1254; // @[Monitor.scala 533:22]
  reg [31:0] _RAND_8;
  reg [2:0] _T_1255; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_9;
  reg [2:0] _T_1256; // @[Monitor.scala 535:22]
  reg [31:0] _RAND_10;
  reg [2:0] _T_1257; // @[Monitor.scala 536:22]
  reg [31:0] _RAND_11;
  reg  _T_1258; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_12;
  wire  _T_1259; // @[Monitor.scala 538:22]
  wire  _T_1260; // @[Monitor.scala 538:19]
  wire  _T_1261; // @[Monitor.scala 539:29]
  wire  _T_1263; // @[Monitor.scala 51:11]
  wire  _T_1264; // @[Monitor.scala 51:11]
  wire  _T_1265; // @[Monitor.scala 540:29]
  wire  _T_1267; // @[Monitor.scala 51:11]
  wire  _T_1268; // @[Monitor.scala 51:11]
  wire  _T_1269; // @[Monitor.scala 541:29]
  wire  _T_1271; // @[Monitor.scala 51:11]
  wire  _T_1272; // @[Monitor.scala 51:11]
  wire  _T_1273; // @[Monitor.scala 542:29]
  wire  _T_1275; // @[Monitor.scala 51:11]
  wire  _T_1276; // @[Monitor.scala 51:11]
  wire  _T_1277; // @[Monitor.scala 543:29]
  wire  _T_1279; // @[Monitor.scala 51:11]
  wire  _T_1280; // @[Monitor.scala 51:11]
  wire  _T_1281; // @[Monitor.scala 544:29]
  wire  _T_1283; // @[Monitor.scala 51:11]
  wire  _T_1284; // @[Monitor.scala 51:11]
  wire  _T_1286; // @[Monitor.scala 546:20]
  wire  _T_1336; // @[Decoupled.scala 40:37]
  reg [1:0] _T_1344; // @[Edges.scala 230:27]
  reg [31:0] _RAND_13;
  wire [1:0] _T_1346; // @[Edges.scala 231:28]
  wire  _T_1347; // @[Edges.scala 232:25]
  reg [2:0] _T_1355; // @[Monitor.scala 509:22]
  reg [31:0] _RAND_14;
  reg [2:0] _T_1356; // @[Monitor.scala 510:22]
  reg [31:0] _RAND_15;
  reg [2:0] _T_1357; // @[Monitor.scala 511:22]
  reg [31:0] _RAND_16;
  reg [2:0] _T_1358; // @[Monitor.scala 512:22]
  reg [31:0] _RAND_17;
  reg [35:0] _T_1359; // @[Monitor.scala 513:22]
  reg [63:0] _RAND_18;
  wire  _T_1360; // @[Monitor.scala 514:22]
  wire  _T_1361; // @[Monitor.scala 514:19]
  wire  _T_1362; // @[Monitor.scala 515:32]
  wire  _T_1364; // @[Monitor.scala 44:11]
  wire  _T_1365; // @[Monitor.scala 44:11]
  wire  _T_1366; // @[Monitor.scala 516:32]
  wire  _T_1368; // @[Monitor.scala 44:11]
  wire  _T_1369; // @[Monitor.scala 44:11]
  wire  _T_1370; // @[Monitor.scala 517:32]
  wire  _T_1372; // @[Monitor.scala 44:11]
  wire  _T_1373; // @[Monitor.scala 44:11]
  wire  _T_1374; // @[Monitor.scala 518:32]
  wire  _T_1376; // @[Monitor.scala 44:11]
  wire  _T_1377; // @[Monitor.scala 44:11]
  wire  _T_1378; // @[Monitor.scala 519:32]
  wire  _T_1380; // @[Monitor.scala 44:11]
  wire  _T_1381; // @[Monitor.scala 44:11]
  wire  _T_1383; // @[Monitor.scala 521:20]
  reg [7:0] _T_1384; // @[Monitor.scala 568:27]
  reg [31:0] _RAND_19;
  reg [1:0] _T_1394; // @[Edges.scala 230:27]
  reg [31:0] _RAND_20;
  wire [1:0] _T_1396; // @[Edges.scala 231:28]
  wire  _T_1397; // @[Edges.scala 232:25]
  reg [1:0] _T_1413; // @[Edges.scala 230:27]
  reg [31:0] _RAND_21;
  wire [1:0] _T_1415; // @[Edges.scala 231:28]
  wire  _T_1416; // @[Edges.scala 232:25]
  wire  _T_1426; // @[Monitor.scala 574:27]
  wire [7:0] _T_1428; // @[OneHot.scala 58:35]
  wire [7:0] _T_1429; // @[Monitor.scala 576:23]
  wire  _T_1431; // @[Monitor.scala 576:14]
  wire  _T_1433; // @[Monitor.scala 576:13]
  wire  _T_1434; // @[Monitor.scala 576:13]
  wire [7:0] _GEN_27; // @[Monitor.scala 574:72]
  wire  _T_1438; // @[Monitor.scala 581:27]
  wire  _T_1440; // @[Monitor.scala 581:75]
  wire  _T_1441; // @[Monitor.scala 581:72]
  wire [7:0] _T_1442; // @[OneHot.scala 58:35]
  wire [7:0] _T_1443; // @[Monitor.scala 583:21]
  wire [7:0] _T_1444; // @[Monitor.scala 583:32]
  wire  _T_1447; // @[Monitor.scala 51:11]
  wire  _T_1448; // @[Monitor.scala 51:11]
  wire [7:0] _GEN_28; // @[Monitor.scala 581:91]
  wire  _T_1449; // @[Monitor.scala 587:20]
  wire  _T_1450; // @[Monitor.scala 587:40]
  wire  _T_1451; // @[Monitor.scala 587:33]
  wire  _T_1452; // @[Monitor.scala 587:30]
  wire  _T_1454; // @[Monitor.scala 51:11]
  wire  _T_1455; // @[Monitor.scala 51:11]
  wire [7:0] _T_1456; // @[Monitor.scala 590:27]
  wire [7:0] _T_1457; // @[Monitor.scala 590:38]
  wire [7:0] _T_1458; // @[Monitor.scala 590:36]
  reg [31:0] _T_1459; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_22;
  wire  _T_1460; // @[Monitor.scala 595:23]
  wire  _T_1461; // @[Monitor.scala 595:13]
  wire  _T_1462; // @[Monitor.scala 595:36]
  wire  _T_1463; // @[Monitor.scala 595:27]
  wire  _T_1464; // @[Monitor.scala 595:56]
  wire  _T_1465; // @[Monitor.scala 595:44]
  wire  _T_1467; // @[Monitor.scala 595:12]
  wire  _T_1468; // @[Monitor.scala 595:12]
  wire [31:0] _T_1470; // @[Monitor.scala 597:26]
  wire  _T_1473; // @[Monitor.scala 598:27]
  reg [7:0] _T_1474; // @[Monitor.scala 694:27]
  reg [31:0] _RAND_23;
  reg [1:0] _T_1483; // @[Edges.scala 230:27]
  reg [31:0] _RAND_24;
  wire [1:0] _T_1485; // @[Edges.scala 231:28]
  wire  _T_1486; // @[Edges.scala 232:25]
  wire  _T_1496; // @[Monitor.scala 700:27]
  wire  _T_1499; // @[Edges.scala 72:43]
  wire  _T_1500; // @[Edges.scala 72:40]
  wire  _T_1501; // @[Monitor.scala 700:38]
  wire [7:0] _T_1502; // @[OneHot.scala 58:35]
  wire [7:0] _T_1503; // @[Monitor.scala 702:23]
  wire  _T_1505; // @[Monitor.scala 702:14]
  wire  _T_1507; // @[Monitor.scala 51:11]
  wire  _T_1508; // @[Monitor.scala 51:11]
  wire [7:0] _GEN_31; // @[Monitor.scala 700:72]
  wire [7:0] _T_1513; // @[OneHot.scala 58:35]
  wire [7:0] _T_1514; // @[Monitor.scala 708:24]
  wire [7:0] _T_1515; // @[Monitor.scala 708:35]
  wire  _T_1518; // @[Monitor.scala 44:11]
  wire  _T_1519; // @[Monitor.scala 44:11]
  wire [7:0] _GEN_32; // @[Monitor.scala 706:73]
  wire [7:0] _T_1520; // @[Monitor.scala 713:27]
  wire [7:0] _T_1521; // @[Monitor.scala 713:38]
  wire [7:0] _T_1522; // @[Monitor.scala 713:36]
  wire  _GEN_35; // @[Monitor.scala 44:11]
  wire  _GEN_49; // @[Monitor.scala 44:11]
  wire  _GEN_65; // @[Monitor.scala 44:11]
  wire  _GEN_75; // @[Monitor.scala 44:11]
  wire  _GEN_83; // @[Monitor.scala 44:11]
  wire  _GEN_91; // @[Monitor.scala 44:11]
  wire  _GEN_99; // @[Monitor.scala 44:11]
  wire  _GEN_107; // @[Monitor.scala 44:11]
  wire  _GEN_117; // @[Monitor.scala 51:11]
  wire  _GEN_125; // @[Monitor.scala 51:11]
  wire  _GEN_133; // @[Monitor.scala 51:11]
  wire  _GEN_141; // @[Monitor.scala 51:11]
  wire  _GEN_145; // @[Monitor.scala 51:11]
  wire  _GEN_149; // @[Monitor.scala 51:11]
  wire  _GEN_153; // @[Monitor.scala 44:11]
  wire  _GEN_163; // @[Monitor.scala 44:11]
  wire  _GEN_171; // @[Monitor.scala 44:11]
  wire  _GEN_183; // @[Monitor.scala 44:11]
  wire  _GEN_193; // @[Monitor.scala 44:11]
  wire  _GEN_201; // @[Monitor.scala 44:11]
  wire  _GEN_207; // @[Monitor.scala 44:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_14 = 13'h3f << io_in_a_bits_size; // @[package.scala 189:77]
  assign _T_16 = ~_T_14[5:0]; // @[package.scala 189:46]
  assign _GEN_33 = {{30'd0}, _T_16}; // @[Edges.scala 22:16]
  assign _T_17 = io_in_a_bits_address & _GEN_33; // @[Edges.scala 22:16]
  assign _T_18 = _T_17 == 36'h0; // @[Edges.scala 22:24]
  assign _T_19 = {{1'd0}, io_in_a_bits_size}; // @[Misc.scala 201:34]
  assign _T_21 = 4'h1 << _T_19[1:0]; // @[OneHot.scala 65:12]
  assign _T_23 = _T_21 | 4'h1; // @[Misc.scala 201:81]
  assign _T_24 = io_in_a_bits_size >= 3'h4; // @[Misc.scala 205:21]
  assign _T_27 = ~io_in_a_bits_address[3]; // @[Misc.scala 210:20]
  assign _T_29 = _T_23[3] & _T_27; // @[Misc.scala 214:38]
  assign _T_30 = _T_24 | _T_29; // @[Misc.scala 214:29]
  assign _T_32 = _T_23[3] & io_in_a_bits_address[3]; // @[Misc.scala 214:38]
  assign _T_33 = _T_24 | _T_32; // @[Misc.scala 214:29]
  assign _T_36 = ~io_in_a_bits_address[2]; // @[Misc.scala 210:20]
  assign _T_37 = _T_27 & _T_36; // @[Misc.scala 213:27]
  assign _T_38 = _T_23[2] & _T_37; // @[Misc.scala 214:38]
  assign _T_39 = _T_30 | _T_38; // @[Misc.scala 214:29]
  assign _T_40 = _T_27 & io_in_a_bits_address[2]; // @[Misc.scala 213:27]
  assign _T_41 = _T_23[2] & _T_40; // @[Misc.scala 214:38]
  assign _T_42 = _T_30 | _T_41; // @[Misc.scala 214:29]
  assign _T_43 = io_in_a_bits_address[3] & _T_36; // @[Misc.scala 213:27]
  assign _T_44 = _T_23[2] & _T_43; // @[Misc.scala 214:38]
  assign _T_45 = _T_33 | _T_44; // @[Misc.scala 214:29]
  assign _T_46 = io_in_a_bits_address[3] & io_in_a_bits_address[2]; // @[Misc.scala 213:27]
  assign _T_47 = _T_23[2] & _T_46; // @[Misc.scala 214:38]
  assign _T_48 = _T_33 | _T_47; // @[Misc.scala 214:29]
  assign _T_51 = ~io_in_a_bits_address[1]; // @[Misc.scala 210:20]
  assign _T_52 = _T_37 & _T_51; // @[Misc.scala 213:27]
  assign _T_53 = _T_23[1] & _T_52; // @[Misc.scala 214:38]
  assign _T_54 = _T_39 | _T_53; // @[Misc.scala 214:29]
  assign _T_55 = _T_37 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_56 = _T_23[1] & _T_55; // @[Misc.scala 214:38]
  assign _T_57 = _T_39 | _T_56; // @[Misc.scala 214:29]
  assign _T_58 = _T_40 & _T_51; // @[Misc.scala 213:27]
  assign _T_59 = _T_23[1] & _T_58; // @[Misc.scala 214:38]
  assign _T_60 = _T_42 | _T_59; // @[Misc.scala 214:29]
  assign _T_61 = _T_40 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_62 = _T_23[1] & _T_61; // @[Misc.scala 214:38]
  assign _T_63 = _T_42 | _T_62; // @[Misc.scala 214:29]
  assign _T_64 = _T_43 & _T_51; // @[Misc.scala 213:27]
  assign _T_65 = _T_23[1] & _T_64; // @[Misc.scala 214:38]
  assign _T_66 = _T_45 | _T_65; // @[Misc.scala 214:29]
  assign _T_67 = _T_43 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_68 = _T_23[1] & _T_67; // @[Misc.scala 214:38]
  assign _T_69 = _T_45 | _T_68; // @[Misc.scala 214:29]
  assign _T_70 = _T_46 & _T_51; // @[Misc.scala 213:27]
  assign _T_71 = _T_23[1] & _T_70; // @[Misc.scala 214:38]
  assign _T_72 = _T_48 | _T_71; // @[Misc.scala 214:29]
  assign _T_73 = _T_46 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_74 = _T_23[1] & _T_73; // @[Misc.scala 214:38]
  assign _T_75 = _T_48 | _T_74; // @[Misc.scala 214:29]
  assign _T_78 = ~io_in_a_bits_address[0]; // @[Misc.scala 210:20]
  assign _T_79 = _T_52 & _T_78; // @[Misc.scala 213:27]
  assign _T_80 = _T_23[0] & _T_79; // @[Misc.scala 214:38]
  assign _T_81 = _T_54 | _T_80; // @[Misc.scala 214:29]
  assign _T_82 = _T_52 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_83 = _T_23[0] & _T_82; // @[Misc.scala 214:38]
  assign _T_84 = _T_54 | _T_83; // @[Misc.scala 214:29]
  assign _T_85 = _T_55 & _T_78; // @[Misc.scala 213:27]
  assign _T_86 = _T_23[0] & _T_85; // @[Misc.scala 214:38]
  assign _T_87 = _T_57 | _T_86; // @[Misc.scala 214:29]
  assign _T_88 = _T_55 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_89 = _T_23[0] & _T_88; // @[Misc.scala 214:38]
  assign _T_90 = _T_57 | _T_89; // @[Misc.scala 214:29]
  assign _T_91 = _T_58 & _T_78; // @[Misc.scala 213:27]
  assign _T_92 = _T_23[0] & _T_91; // @[Misc.scala 214:38]
  assign _T_93 = _T_60 | _T_92; // @[Misc.scala 214:29]
  assign _T_94 = _T_58 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_95 = _T_23[0] & _T_94; // @[Misc.scala 214:38]
  assign _T_96 = _T_60 | _T_95; // @[Misc.scala 214:29]
  assign _T_97 = _T_61 & _T_78; // @[Misc.scala 213:27]
  assign _T_98 = _T_23[0] & _T_97; // @[Misc.scala 214:38]
  assign _T_99 = _T_63 | _T_98; // @[Misc.scala 214:29]
  assign _T_100 = _T_61 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_101 = _T_23[0] & _T_100; // @[Misc.scala 214:38]
  assign _T_102 = _T_63 | _T_101; // @[Misc.scala 214:29]
  assign _T_103 = _T_64 & _T_78; // @[Misc.scala 213:27]
  assign _T_104 = _T_23[0] & _T_103; // @[Misc.scala 214:38]
  assign _T_105 = _T_66 | _T_104; // @[Misc.scala 214:29]
  assign _T_106 = _T_64 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_107 = _T_23[0] & _T_106; // @[Misc.scala 214:38]
  assign _T_108 = _T_66 | _T_107; // @[Misc.scala 214:29]
  assign _T_109 = _T_67 & _T_78; // @[Misc.scala 213:27]
  assign _T_110 = _T_23[0] & _T_109; // @[Misc.scala 214:38]
  assign _T_111 = _T_69 | _T_110; // @[Misc.scala 214:29]
  assign _T_112 = _T_67 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_113 = _T_23[0] & _T_112; // @[Misc.scala 214:38]
  assign _T_114 = _T_69 | _T_113; // @[Misc.scala 214:29]
  assign _T_115 = _T_70 & _T_78; // @[Misc.scala 213:27]
  assign _T_116 = _T_23[0] & _T_115; // @[Misc.scala 214:38]
  assign _T_117 = _T_72 | _T_116; // @[Misc.scala 214:29]
  assign _T_118 = _T_70 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_119 = _T_23[0] & _T_118; // @[Misc.scala 214:38]
  assign _T_120 = _T_72 | _T_119; // @[Misc.scala 214:29]
  assign _T_121 = _T_73 & _T_78; // @[Misc.scala 213:27]
  assign _T_122 = _T_23[0] & _T_121; // @[Misc.scala 214:38]
  assign _T_123 = _T_75 | _T_122; // @[Misc.scala 214:29]
  assign _T_124 = _T_73 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_125 = _T_23[0] & _T_124; // @[Misc.scala 214:38]
  assign _T_126 = _T_75 | _T_125; // @[Misc.scala 214:29]
  assign _T_133 = {_T_102,_T_99,_T_96,_T_93,_T_90,_T_87,_T_84,_T_81}; // @[Cat.scala 29:58]
  assign _T_141 = {_T_126,_T_123,_T_120,_T_117,_T_114,_T_111,_T_108,_T_105,_T_133}; // @[Cat.scala 29:58]
  assign _T_160 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_162 = io_in_a_bits_size <= 3'h6; // @[Parameters.scala 93:42]
  assign _T_165 = io_in_a_bits_address ^ 36'ha000000; // @[Parameters.scala 137:31]
  assign _T_166 = {1'b0,$signed(_T_165)}; // @[Parameters.scala 137:49]
  assign _T_168 = $signed(_T_166) & -37'sh200000; // @[Parameters.scala 137:52]
  assign _T_169 = $signed(_T_168) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_170 = io_in_a_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  assign _T_171 = {1'b0,$signed(_T_170)}; // @[Parameters.scala 137:49]
  assign _T_173 = $signed(_T_171) & -37'sh800000000; // @[Parameters.scala 137:52]
  assign _T_174 = $signed(_T_173) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_175 = _T_169 | _T_174; // @[Parameters.scala 552:42]
  assign _T_176 = _T_162 & _T_175; // @[Parameters.scala 551:56]
  assign _T_179 = _T_176 | reset; // @[Monitor.scala 44:11]
  assign _T_180 = ~_T_179; // @[Monitor.scala 44:11]
  assign _T_181 = 3'h6 == io_in_a_bits_size; // @[Parameters.scala 92:48]
  assign _T_183 = _T_181 | reset; // @[Monitor.scala 44:11]
  assign _T_184 = ~_T_183; // @[Monitor.scala 44:11]
  assign _T_190 = _T_24 | reset; // @[Monitor.scala 44:11]
  assign _T_191 = ~_T_190; // @[Monitor.scala 44:11]
  assign _T_193 = _T_18 | reset; // @[Monitor.scala 44:11]
  assign _T_194 = ~_T_193; // @[Monitor.scala 44:11]
  assign _T_195 = io_in_a_bits_param <= 3'h2; // @[Bundles.scala 110:27]
  assign _T_197 = _T_195 | reset; // @[Monitor.scala 44:11]
  assign _T_198 = ~_T_197; // @[Monitor.scala 44:11]
  assign _T_199 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_200 = _T_199 == 16'h0; // @[Monitor.scala 86:31]
  assign _T_202 = _T_200 | reset; // @[Monitor.scala 44:11]
  assign _T_203 = ~_T_202; // @[Monitor.scala 44:11]
  assign _T_204 = ~io_in_a_bits_corrupt; // @[Monitor.scala 87:18]
  assign _T_206 = _T_204 | reset; // @[Monitor.scala 44:11]
  assign _T_207 = ~_T_206; // @[Monitor.scala 44:11]
  assign _T_208 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_247 = io_in_a_bits_param != 3'h0; // @[Monitor.scala 97:31]
  assign _T_249 = _T_247 | reset; // @[Monitor.scala 44:11]
  assign _T_250 = ~_T_249; // @[Monitor.scala 44:11]
  assign _T_260 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_287 = io_in_a_bits_param == 3'h0; // @[Monitor.scala 106:31]
  assign _T_289 = _T_287 | reset; // @[Monitor.scala 44:11]
  assign _T_290 = ~_T_289; // @[Monitor.scala 44:11]
  assign _T_291 = io_in_a_bits_mask == _T_141; // @[Monitor.scala 107:30]
  assign _T_293 = _T_291 | reset; // @[Monitor.scala 44:11]
  assign _T_294 = ~_T_293; // @[Monitor.scala 44:11]
  assign _T_299 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_334 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_365 = ~_T_141; // @[Monitor.scala 124:33]
  assign _T_366 = io_in_a_bits_mask & _T_365; // @[Monitor.scala 124:31]
  assign _T_367 = _T_366 == 16'h0; // @[Monitor.scala 124:40]
  assign _T_369 = _T_367 | reset; // @[Monitor.scala 44:11]
  assign _T_370 = ~_T_369; // @[Monitor.scala 44:11]
  assign _T_371 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_373 = io_in_a_bits_size <= 3'h4; // @[Parameters.scala 93:42]
  assign _T_381 = _T_373 & _T_169; // @[Parameters.scala 551:56]
  assign _T_392 = _T_381 | reset; // @[Monitor.scala 44:11]
  assign _T_393 = ~_T_392; // @[Monitor.scala 44:11]
  assign _T_400 = io_in_a_bits_param <= 3'h4; // @[Bundles.scala 140:33]
  assign _T_402 = _T_400 | reset; // @[Monitor.scala 44:11]
  assign _T_403 = ~_T_402; // @[Monitor.scala 44:11]
  assign _T_408 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_437 = io_in_a_bits_param <= 3'h3; // @[Bundles.scala 147:30]
  assign _T_439 = _T_437 | reset; // @[Monitor.scala 44:11]
  assign _T_440 = ~_T_439; // @[Monitor.scala 44:11]
  assign _T_445 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_455 = _T_162 & _T_169; // @[Parameters.scala 551:56]
  assign _T_466 = _T_455 | reset; // @[Monitor.scala 44:11]
  assign _T_467 = ~_T_466; // @[Monitor.scala 44:11]
  assign _T_474 = io_in_a_bits_param <= 3'h1; // @[Bundles.scala 160:28]
  assign _T_476 = _T_474 | reset; // @[Monitor.scala 44:11]
  assign _T_477 = ~_T_476; // @[Monitor.scala 44:11]
  assign _T_486 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_488 = _T_486 | reset; // @[Monitor.scala 51:11]
  assign _T_489 = ~_T_488; // @[Monitor.scala 51:11]
  assign _T_500 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_504 = io_in_d_bits_size >= 3'h4; // @[Monitor.scala 309:27]
  assign _T_506 = _T_504 | reset; // @[Monitor.scala 51:11]
  assign _T_507 = ~_T_506; // @[Monitor.scala 51:11]
  assign _T_508 = io_in_d_bits_param == 2'h0; // @[Monitor.scala 310:28]
  assign _T_510 = _T_508 | reset; // @[Monitor.scala 51:11]
  assign _T_511 = ~_T_510; // @[Monitor.scala 51:11]
  assign _T_512 = ~io_in_d_bits_corrupt; // @[Monitor.scala 311:15]
  assign _T_514 = _T_512 | reset; // @[Monitor.scala 51:11]
  assign _T_515 = ~_T_514; // @[Monitor.scala 51:11]
  assign _T_516 = ~io_in_d_bits_denied; // @[Monitor.scala 312:15]
  assign _T_518 = _T_516 | reset; // @[Monitor.scala 51:11]
  assign _T_519 = ~_T_518; // @[Monitor.scala 51:11]
  assign _T_520 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_531 = io_in_d_bits_param <= 2'h2; // @[Bundles.scala 104:26]
  assign _T_533 = _T_531 | reset; // @[Monitor.scala 51:11]
  assign _T_534 = ~_T_533; // @[Monitor.scala 51:11]
  assign _T_535 = io_in_d_bits_param != 2'h2; // @[Monitor.scala 320:28]
  assign _T_537 = _T_535 | reset; // @[Monitor.scala 51:11]
  assign _T_538 = ~_T_537; // @[Monitor.scala 51:11]
  assign _T_548 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_568 = _T_516 | io_in_d_bits_corrupt; // @[Monitor.scala 331:30]
  assign _T_570 = _T_568 | reset; // @[Monitor.scala 51:11]
  assign _T_571 = ~_T_570; // @[Monitor.scala 51:11]
  assign _T_577 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_594 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_612 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_974 = 13'h3f << io_in_c_bits_size; // @[package.scala 189:77]
  assign _T_976 = ~_T_974[5:0]; // @[package.scala 189:46]
  assign _GEN_34 = {{30'd0}, _T_976}; // @[Edges.scala 22:16]
  assign _T_977 = io_in_c_bits_address & _GEN_34; // @[Edges.scala 22:16]
  assign _T_978 = _T_977 == 36'h0; // @[Edges.scala 22:24]
  assign _T_979 = io_in_c_bits_address ^ 36'ha000000; // @[Parameters.scala 137:31]
  assign _T_980 = {1'b0,$signed(_T_979)}; // @[Parameters.scala 137:49]
  assign _T_982 = $signed(_T_980) & -37'sh200000; // @[Parameters.scala 137:52]
  assign _T_983 = $signed(_T_982) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_984 = io_in_c_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  assign _T_985 = {1'b0,$signed(_T_984)}; // @[Parameters.scala 137:49]
  assign _T_987 = $signed(_T_985) & -37'sh800000000; // @[Parameters.scala 137:52]
  assign _T_988 = $signed(_T_987) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_990 = _T_983 | _T_988; // @[Parameters.scala 535:64]
  assign _T_1009 = io_in_c_bits_opcode == 3'h4; // @[Monitor.scala 239:25]
  assign _T_1011 = _T_990 | reset; // @[Monitor.scala 44:11]
  assign _T_1012 = ~_T_1011; // @[Monitor.scala 44:11]
  assign _T_1016 = io_in_c_bits_size >= 3'h4; // @[Monitor.scala 242:30]
  assign _T_1018 = _T_1016 | reset; // @[Monitor.scala 44:11]
  assign _T_1019 = ~_T_1018; // @[Monitor.scala 44:11]
  assign _T_1021 = _T_978 | reset; // @[Monitor.scala 44:11]
  assign _T_1022 = ~_T_1021; // @[Monitor.scala 44:11]
  assign _T_1023 = io_in_c_bits_param <= 3'h5; // @[Bundles.scala 122:29]
  assign _T_1025 = _T_1023 | reset; // @[Monitor.scala 44:11]
  assign _T_1026 = ~_T_1025; // @[Monitor.scala 44:11]
  assign _T_1027 = ~io_in_c_bits_corrupt; // @[Monitor.scala 245:18]
  assign _T_1029 = _T_1027 | reset; // @[Monitor.scala 44:11]
  assign _T_1030 = ~_T_1029; // @[Monitor.scala 44:11]
  assign _T_1031 = io_in_c_bits_opcode == 3'h5; // @[Monitor.scala 248:25]
  assign _T_1049 = io_in_c_bits_opcode == 3'h6; // @[Monitor.scala 256:25]
  assign _T_1051 = io_in_c_bits_size <= 3'h6; // @[Parameters.scala 93:42]
  assign _T_1065 = _T_1051 & _T_990; // @[Parameters.scala 551:56]
  assign _T_1068 = _T_1065 | reset; // @[Monitor.scala 44:11]
  assign _T_1069 = ~_T_1068; // @[Monitor.scala 44:11]
  assign _T_1070 = 3'h6 == io_in_c_bits_size; // @[Parameters.scala 92:48]
  assign _T_1072 = _T_1070 | reset; // @[Monitor.scala 44:11]
  assign _T_1073 = ~_T_1072; // @[Monitor.scala 44:11]
  assign _T_1084 = io_in_c_bits_param <= 3'h2; // @[Bundles.scala 116:29]
  assign _T_1086 = _T_1084 | reset; // @[Monitor.scala 44:11]
  assign _T_1087 = ~_T_1086; // @[Monitor.scala 44:11]
  assign _T_1092 = io_in_c_bits_opcode == 3'h7; // @[Monitor.scala 266:25]
  assign _T_1131 = io_in_c_bits_opcode == 3'h0; // @[Monitor.scala 275:25]
  assign _T_1141 = io_in_c_bits_param == 3'h0; // @[Monitor.scala 279:31]
  assign _T_1143 = _T_1141 | reset; // @[Monitor.scala 44:11]
  assign _T_1144 = ~_T_1143; // @[Monitor.scala 44:11]
  assign _T_1149 = io_in_c_bits_opcode == 3'h1; // @[Monitor.scala 283:25]
  assign _T_1163 = io_in_c_bits_opcode == 3'h2; // @[Monitor.scala 290:25]
  assign _T_1185 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_1192 = ~io_in_a_bits_opcode[2]; // @[Edges.scala 93:28]
  assign _T_1196 = _T_1194 - 2'h1; // @[Edges.scala 231:28]
  assign _T_1197 = _T_1194 == 2'h0; // @[Edges.scala 232:25]
  assign _T_1210 = ~_T_1197; // @[Monitor.scala 386:22]
  assign _T_1211 = io_in_a_valid & _T_1210; // @[Monitor.scala 386:19]
  assign _T_1212 = io_in_a_bits_opcode == _T_1205; // @[Monitor.scala 387:32]
  assign _T_1214 = _T_1212 | reset; // @[Monitor.scala 44:11]
  assign _T_1215 = ~_T_1214; // @[Monitor.scala 44:11]
  assign _T_1216 = io_in_a_bits_param == _T_1206; // @[Monitor.scala 388:32]
  assign _T_1218 = _T_1216 | reset; // @[Monitor.scala 44:11]
  assign _T_1219 = ~_T_1218; // @[Monitor.scala 44:11]
  assign _T_1220 = io_in_a_bits_size == _T_1207; // @[Monitor.scala 389:32]
  assign _T_1222 = _T_1220 | reset; // @[Monitor.scala 44:11]
  assign _T_1223 = ~_T_1222; // @[Monitor.scala 44:11]
  assign _T_1224 = io_in_a_bits_source == _T_1208; // @[Monitor.scala 390:32]
  assign _T_1226 = _T_1224 | reset; // @[Monitor.scala 44:11]
  assign _T_1227 = ~_T_1226; // @[Monitor.scala 44:11]
  assign _T_1228 = io_in_a_bits_address == _T_1209; // @[Monitor.scala 391:32]
  assign _T_1230 = _T_1228 | reset; // @[Monitor.scala 44:11]
  assign _T_1231 = ~_T_1230; // @[Monitor.scala 44:11]
  assign _T_1233 = _T_1185 & _T_1197; // @[Monitor.scala 393:20]
  assign _T_1234 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_1236 = 13'h3f << io_in_d_bits_size; // @[package.scala 189:77]
  assign _T_1238 = ~_T_1236[5:0]; // @[package.scala 189:46]
  assign _T_1244 = _T_1242 - 2'h1; // @[Edges.scala 231:28]
  assign _T_1245 = _T_1242 == 2'h0; // @[Edges.scala 232:25]
  assign _T_1259 = ~_T_1245; // @[Monitor.scala 538:22]
  assign _T_1260 = io_in_d_valid & _T_1259; // @[Monitor.scala 538:19]
  assign _T_1261 = io_in_d_bits_opcode == _T_1253; // @[Monitor.scala 539:29]
  assign _T_1263 = _T_1261 | reset; // @[Monitor.scala 51:11]
  assign _T_1264 = ~_T_1263; // @[Monitor.scala 51:11]
  assign _T_1265 = io_in_d_bits_param == _T_1254; // @[Monitor.scala 540:29]
  assign _T_1267 = _T_1265 | reset; // @[Monitor.scala 51:11]
  assign _T_1268 = ~_T_1267; // @[Monitor.scala 51:11]
  assign _T_1269 = io_in_d_bits_size == _T_1255; // @[Monitor.scala 541:29]
  assign _T_1271 = _T_1269 | reset; // @[Monitor.scala 51:11]
  assign _T_1272 = ~_T_1271; // @[Monitor.scala 51:11]
  assign _T_1273 = io_in_d_bits_source == _T_1256; // @[Monitor.scala 542:29]
  assign _T_1275 = _T_1273 | reset; // @[Monitor.scala 51:11]
  assign _T_1276 = ~_T_1275; // @[Monitor.scala 51:11]
  assign _T_1277 = io_in_d_bits_sink == _T_1257; // @[Monitor.scala 543:29]
  assign _T_1279 = _T_1277 | reset; // @[Monitor.scala 51:11]
  assign _T_1280 = ~_T_1279; // @[Monitor.scala 51:11]
  assign _T_1281 = io_in_d_bits_denied == _T_1258; // @[Monitor.scala 544:29]
  assign _T_1283 = _T_1281 | reset; // @[Monitor.scala 51:11]
  assign _T_1284 = ~_T_1283; // @[Monitor.scala 51:11]
  assign _T_1286 = _T_1234 & _T_1245; // @[Monitor.scala 546:20]
  assign _T_1336 = io_in_c_ready & io_in_c_valid; // @[Decoupled.scala 40:37]
  assign _T_1346 = _T_1344 - 2'h1; // @[Edges.scala 231:28]
  assign _T_1347 = _T_1344 == 2'h0; // @[Edges.scala 232:25]
  assign _T_1360 = ~_T_1347; // @[Monitor.scala 514:22]
  assign _T_1361 = io_in_c_valid & _T_1360; // @[Monitor.scala 514:19]
  assign _T_1362 = io_in_c_bits_opcode == _T_1355; // @[Monitor.scala 515:32]
  assign _T_1364 = _T_1362 | reset; // @[Monitor.scala 44:11]
  assign _T_1365 = ~_T_1364; // @[Monitor.scala 44:11]
  assign _T_1366 = io_in_c_bits_param == _T_1356; // @[Monitor.scala 516:32]
  assign _T_1368 = _T_1366 | reset; // @[Monitor.scala 44:11]
  assign _T_1369 = ~_T_1368; // @[Monitor.scala 44:11]
  assign _T_1370 = io_in_c_bits_size == _T_1357; // @[Monitor.scala 517:32]
  assign _T_1372 = _T_1370 | reset; // @[Monitor.scala 44:11]
  assign _T_1373 = ~_T_1372; // @[Monitor.scala 44:11]
  assign _T_1374 = io_in_c_bits_source == _T_1358; // @[Monitor.scala 518:32]
  assign _T_1376 = _T_1374 | reset; // @[Monitor.scala 44:11]
  assign _T_1377 = ~_T_1376; // @[Monitor.scala 44:11]
  assign _T_1378 = io_in_c_bits_address == _T_1359; // @[Monitor.scala 519:32]
  assign _T_1380 = _T_1378 | reset; // @[Monitor.scala 44:11]
  assign _T_1381 = ~_T_1380; // @[Monitor.scala 44:11]
  assign _T_1383 = _T_1336 & _T_1347; // @[Monitor.scala 521:20]
  assign _T_1396 = _T_1394 - 2'h1; // @[Edges.scala 231:28]
  assign _T_1397 = _T_1394 == 2'h0; // @[Edges.scala 232:25]
  assign _T_1415 = _T_1413 - 2'h1; // @[Edges.scala 231:28]
  assign _T_1416 = _T_1413 == 2'h0; // @[Edges.scala 232:25]
  assign _T_1426 = _T_1185 & _T_1397; // @[Monitor.scala 574:27]
  assign _T_1428 = 8'h1 << io_in_a_bits_source; // @[OneHot.scala 58:35]
  assign _T_1429 = _T_1384 >> io_in_a_bits_source; // @[Monitor.scala 576:23]
  assign _T_1431 = ~_T_1429[0]; // @[Monitor.scala 576:14]
  assign _T_1433 = _T_1431 | reset; // @[Monitor.scala 576:13]
  assign _T_1434 = ~_T_1433; // @[Monitor.scala 576:13]
  assign _GEN_27 = _T_1426 ? _T_1428 : 8'h0; // @[Monitor.scala 574:72]
  assign _T_1438 = _T_1234 & _T_1416; // @[Monitor.scala 581:27]
  assign _T_1440 = ~_T_500; // @[Monitor.scala 581:75]
  assign _T_1441 = _T_1438 & _T_1440; // @[Monitor.scala 581:72]
  assign _T_1442 = 8'h1 << io_in_d_bits_source; // @[OneHot.scala 58:35]
  assign _T_1443 = _GEN_27 | _T_1384; // @[Monitor.scala 583:21]
  assign _T_1444 = _T_1443 >> io_in_d_bits_source; // @[Monitor.scala 583:32]
  assign _T_1447 = _T_1444[0] | reset; // @[Monitor.scala 51:11]
  assign _T_1448 = ~_T_1447; // @[Monitor.scala 51:11]
  assign _GEN_28 = _T_1441 ? _T_1442 : 8'h0; // @[Monitor.scala 581:91]
  assign _T_1449 = _GEN_27 != _GEN_28; // @[Monitor.scala 587:20]
  assign _T_1450 = _GEN_27 != 8'h0; // @[Monitor.scala 587:40]
  assign _T_1451 = ~_T_1450; // @[Monitor.scala 587:33]
  assign _T_1452 = _T_1449 | _T_1451; // @[Monitor.scala 587:30]
  assign _T_1454 = _T_1452 | reset; // @[Monitor.scala 51:11]
  assign _T_1455 = ~_T_1454; // @[Monitor.scala 51:11]
  assign _T_1456 = _T_1384 | _GEN_27; // @[Monitor.scala 590:27]
  assign _T_1457 = ~_GEN_28; // @[Monitor.scala 590:38]
  assign _T_1458 = _T_1456 & _T_1457; // @[Monitor.scala 590:36]
  assign _T_1460 = _T_1384 != 8'h0; // @[Monitor.scala 595:23]
  assign _T_1461 = ~_T_1460; // @[Monitor.scala 595:13]
  assign _T_1462 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_1463 = _T_1461 | _T_1462; // @[Monitor.scala 595:27]
  assign _T_1464 = _T_1459 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_1465 = _T_1463 | _T_1464; // @[Monitor.scala 595:44]
  assign _T_1467 = _T_1465 | reset; // @[Monitor.scala 595:12]
  assign _T_1468 = ~_T_1467; // @[Monitor.scala 595:12]
  assign _T_1470 = _T_1459 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_1473 = _T_1185 | _T_1234; // @[Monitor.scala 598:27]
  assign _T_1485 = _T_1483 - 2'h1; // @[Edges.scala 231:28]
  assign _T_1486 = _T_1483 == 2'h0; // @[Edges.scala 232:25]
  assign _T_1496 = _T_1234 & _T_1486; // @[Monitor.scala 700:27]
  assign _T_1499 = ~io_in_d_bits_opcode[1]; // @[Edges.scala 72:43]
  assign _T_1500 = io_in_d_bits_opcode[2] & _T_1499; // @[Edges.scala 72:40]
  assign _T_1501 = _T_1496 & _T_1500; // @[Monitor.scala 700:38]
  assign _T_1502 = 8'h1 << io_in_d_bits_sink; // @[OneHot.scala 58:35]
  assign _T_1503 = _T_1474 >> io_in_d_bits_sink; // @[Monitor.scala 702:23]
  assign _T_1505 = ~_T_1503[0]; // @[Monitor.scala 702:14]
  assign _T_1507 = _T_1505 | reset; // @[Monitor.scala 51:11]
  assign _T_1508 = ~_T_1507; // @[Monitor.scala 51:11]
  assign _GEN_31 = _T_1501 ? _T_1502 : 8'h0; // @[Monitor.scala 700:72]
  assign _T_1513 = 8'h1 << io_in_e_bits_sink; // @[OneHot.scala 58:35]
  assign _T_1514 = _GEN_31 | _T_1474; // @[Monitor.scala 708:24]
  assign _T_1515 = _T_1514 >> io_in_e_bits_sink; // @[Monitor.scala 708:35]
  assign _T_1518 = _T_1515[0] | reset; // @[Monitor.scala 44:11]
  assign _T_1519 = ~_T_1518; // @[Monitor.scala 44:11]
  assign _GEN_32 = io_in_e_valid ? _T_1513 : 8'h0; // @[Monitor.scala 706:73]
  assign _T_1520 = _T_1474 | _GEN_31; // @[Monitor.scala 713:27]
  assign _T_1521 = ~_GEN_32; // @[Monitor.scala 713:38]
  assign _T_1522 = _T_1520 & _T_1521; // @[Monitor.scala 713:36]
  assign _GEN_35 = io_in_a_valid & _T_160; // @[Monitor.scala 44:11]
  assign _GEN_49 = io_in_a_valid & _T_208; // @[Monitor.scala 44:11]
  assign _GEN_65 = io_in_a_valid & _T_260; // @[Monitor.scala 44:11]
  assign _GEN_75 = io_in_a_valid & _T_299; // @[Monitor.scala 44:11]
  assign _GEN_83 = io_in_a_valid & _T_334; // @[Monitor.scala 44:11]
  assign _GEN_91 = io_in_a_valid & _T_371; // @[Monitor.scala 44:11]
  assign _GEN_99 = io_in_a_valid & _T_408; // @[Monitor.scala 44:11]
  assign _GEN_107 = io_in_a_valid & _T_445; // @[Monitor.scala 44:11]
  assign _GEN_117 = io_in_d_valid & _T_500; // @[Monitor.scala 51:11]
  assign _GEN_125 = io_in_d_valid & _T_520; // @[Monitor.scala 51:11]
  assign _GEN_133 = io_in_d_valid & _T_548; // @[Monitor.scala 51:11]
  assign _GEN_141 = io_in_d_valid & _T_577; // @[Monitor.scala 51:11]
  assign _GEN_145 = io_in_d_valid & _T_594; // @[Monitor.scala 51:11]
  assign _GEN_149 = io_in_d_valid & _T_612; // @[Monitor.scala 51:11]
  assign _GEN_153 = io_in_c_valid & _T_1009; // @[Monitor.scala 44:11]
  assign _GEN_163 = io_in_c_valid & _T_1031; // @[Monitor.scala 44:11]
  assign _GEN_171 = io_in_c_valid & _T_1049; // @[Monitor.scala 44:11]
  assign _GEN_183 = io_in_c_valid & _T_1092; // @[Monitor.scala 44:11]
  assign _GEN_193 = io_in_c_valid & _T_1131; // @[Monitor.scala 44:11]
  assign _GEN_201 = io_in_c_valid & _T_1149; // @[Monitor.scala 44:11]
  assign _GEN_207 = io_in_c_valid & _T_1163; // @[Monitor.scala 44:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_1194 = _RAND_0[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_1205 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_1206 = _RAND_2[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_1207 = _RAND_3[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_1208 = _RAND_4[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {2{`RANDOM}};
  _T_1209 = _RAND_5[35:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_1242 = _RAND_6[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_1253 = _RAND_7[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_1254 = _RAND_8[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_1255 = _RAND_9[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_1256 = _RAND_10[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_1257 = _RAND_11[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_1258 = _RAND_12[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  _T_1344 = _RAND_13[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_14 = {1{`RANDOM}};
  _T_1355 = _RAND_14[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_15 = {1{`RANDOM}};
  _T_1356 = _RAND_15[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_16 = {1{`RANDOM}};
  _T_1357 = _RAND_16[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_17 = {1{`RANDOM}};
  _T_1358 = _RAND_17[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_18 = {2{`RANDOM}};
  _T_1359 = _RAND_18[35:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_19 = {1{`RANDOM}};
  _T_1384 = _RAND_19[7:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_20 = {1{`RANDOM}};
  _T_1394 = _RAND_20[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_21 = {1{`RANDOM}};
  _T_1413 = _RAND_21[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_22 = {1{`RANDOM}};
  _T_1459 = _RAND_22[31:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_23 = {1{`RANDOM}};
  _T_1474 = _RAND_23[7:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_24 = {1{`RANDOM}};
  _T_1483 = _RAND_24[1:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_1194 <= 2'h0;
    end else if (_T_1185) begin
      if (_T_1197) begin
        if (_T_1192) begin
          _T_1194 <= _T_16[5:4];
        end else begin
          _T_1194 <= 2'h0;
        end
      end else begin
        _T_1194 <= _T_1196;
      end
    end
    if (_T_1233) begin
      _T_1205 <= io_in_a_bits_opcode;
    end
    if (_T_1233) begin
      _T_1206 <= io_in_a_bits_param;
    end
    if (_T_1233) begin
      _T_1207 <= io_in_a_bits_size;
    end
    if (_T_1233) begin
      _T_1208 <= io_in_a_bits_source;
    end
    if (_T_1233) begin
      _T_1209 <= io_in_a_bits_address;
    end
    if (reset) begin
      _T_1242 <= 2'h0;
    end else if (_T_1234) begin
      if (_T_1245) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1242 <= _T_1238[5:4];
        end else begin
          _T_1242 <= 2'h0;
        end
      end else begin
        _T_1242 <= _T_1244;
      end
    end
    if (_T_1286) begin
      _T_1253 <= io_in_d_bits_opcode;
    end
    if (_T_1286) begin
      _T_1254 <= io_in_d_bits_param;
    end
    if (_T_1286) begin
      _T_1255 <= io_in_d_bits_size;
    end
    if (_T_1286) begin
      _T_1256 <= io_in_d_bits_source;
    end
    if (_T_1286) begin
      _T_1257 <= io_in_d_bits_sink;
    end
    if (_T_1286) begin
      _T_1258 <= io_in_d_bits_denied;
    end
    if (reset) begin
      _T_1344 <= 2'h0;
    end else if (_T_1336) begin
      if (_T_1347) begin
        if (io_in_c_bits_opcode[0]) begin
          _T_1344 <= _T_976[5:4];
        end else begin
          _T_1344 <= 2'h0;
        end
      end else begin
        _T_1344 <= _T_1346;
      end
    end
    if (_T_1383) begin
      _T_1355 <= io_in_c_bits_opcode;
    end
    if (_T_1383) begin
      _T_1356 <= io_in_c_bits_param;
    end
    if (_T_1383) begin
      _T_1357 <= io_in_c_bits_size;
    end
    if (_T_1383) begin
      _T_1358 <= io_in_c_bits_source;
    end
    if (_T_1383) begin
      _T_1359 <= io_in_c_bits_address;
    end
    if (reset) begin
      _T_1384 <= 8'h0;
    end else begin
      _T_1384 <= _T_1458;
    end
    if (reset) begin
      _T_1394 <= 2'h0;
    end else if (_T_1185) begin
      if (_T_1397) begin
        if (_T_1192) begin
          _T_1394 <= _T_16[5:4];
        end else begin
          _T_1394 <= 2'h0;
        end
      end else begin
        _T_1394 <= _T_1396;
      end
    end
    if (reset) begin
      _T_1413 <= 2'h0;
    end else if (_T_1234) begin
      if (_T_1416) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1413 <= _T_1238[5:4];
        end else begin
          _T_1413 <= 2'h0;
        end
      end else begin
        _T_1413 <= _T_1415;
      end
    end
    if (reset) begin
      _T_1459 <= 32'h0;
    end else if (_T_1473) begin
      _T_1459 <= 32'h0;
    end else begin
      _T_1459 <= _T_1470;
    end
    if (reset) begin
      _T_1474 <= 8'h0;
    end else begin
      _T_1474 <= _T_1522;
    end
    if (reset) begin
      _T_1483 <= 2'h0;
    end else if (_T_1234) begin
      if (_T_1486) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1483 <= _T_1238[5:4];
        end else begin
          _T_1483 <= 2'h0;
        end
      end else begin
        _T_1483 <= _T_1485;
      end
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_180) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_180) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_184) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_191) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock smaller than a beat (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_191) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_198) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid grow param (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_198) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_203) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_203) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_207) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock is corrupt (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_207) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_180) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_180) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_184) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_191) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm smaller than a beat (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_191) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_198) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid grow param (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_198) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_250) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_250) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_203) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_203) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_207) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm is corrupt (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_207) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_180) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_180) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_290) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid param (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_290) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_294) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_294) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_207) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get is corrupt (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_207) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_180) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_180) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_290) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid param (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_290) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_294) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_294) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_180) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_180) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_290) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid param (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_290) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_370) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial contains invalid mask (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_370) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_91 & _T_393) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_91 & _T_393) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_91 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_91 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_91 & _T_403) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid opcode param (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_91 & _T_403) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_91 & _T_294) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_91 & _T_294) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_99 & _T_393) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_99 & _T_393) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_99 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_99 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_99 & _T_440) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid opcode param (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_99 & _T_440) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_99 & _T_294) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_99 & _T_294) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_107 & _T_467) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_107 & _T_467) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_107 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_107 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_107 & _T_477) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid opcode param (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_107 & _T_477) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_107 & _T_294) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_107 & _T_294) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_107 & _T_207) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint is corrupt (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_107 & _T_207) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_489) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_489) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_507) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_507) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_511) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseeAck carries invalid param (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_511) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_515) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is corrupt (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_515) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_519) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is denied (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_519) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_125 & _T_507) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_125 & _T_507) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_125 & _T_534) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid cap param (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_125 & _T_534) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_125 & _T_538) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries toN param (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_125 & _T_538) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_125 & _T_515) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is corrupt (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_125 & _T_515) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_133 & _T_507) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_133 & _T_507) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_133 & _T_534) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid cap param (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_133 & _T_534) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_133 & _T_538) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries toN param (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_133 & _T_538) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_133 & _T_571) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied but not corrupt (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_133 & _T_571) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_141 & _T_511) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid param (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_141 & _T_511) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_141 & _T_515) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is corrupt (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_141 & _T_515) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_145 & _T_511) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid param (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_145 & _T_511) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_145 & _T_571) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied but not corrupt (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_145 & _T_571) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_149 & _T_511) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid param (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_149 & _T_511) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_149 & _T_515) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is corrupt (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_149 & _T_515) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_153 & _T_1012) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAck carries unmanaged address (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_153 & _T_1012) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_153 & _T_1019) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAck smaller than a beat (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_153 & _T_1019) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_153 & _T_1022) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAck address not aligned to size (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_153 & _T_1022) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_153 & _T_1026) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAck carries invalid report param (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_153 & _T_1026) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_153 & _T_1030) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAck is corrupt (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_153 & _T_1030) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_163 & _T_1012) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAckData carries unmanaged address (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_163 & _T_1012) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_163 & _T_1019) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAckData smaller than a beat (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_163 & _T_1019) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_163 & _T_1022) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAckData address not aligned to size (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_163 & _T_1022) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_163 & _T_1026) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ProbeAckData carries invalid report param (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_163 & _T_1026) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_171 & _T_1069) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel carries Release type unsupported by manager (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_171 & _T_1069) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_171 & _T_1073) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel carries Release from a client which does not support Probe (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_171 & _T_1073) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_171 & _T_1019) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel Release smaller than a beat (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_171 & _T_1019) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_171 & _T_1022) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel Release address not aligned to size (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_171 & _T_1022) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_171 & _T_1087) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel Release carries invalid shrink param (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_171 & _T_1087) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_171 & _T_1030) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel Release is corrupt (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_171 & _T_1030) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_183 & _T_1069) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel carries ReleaseData type unsupported by manager (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_183 & _T_1069) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_183 & _T_1073) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel carries Release from a client which does not support Probe (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_183 & _T_1073) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_183 & _T_1019) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ReleaseData smaller than a beat (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_183 & _T_1019) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_183 & _T_1022) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ReleaseData address not aligned to size (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_183 & _T_1022) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_183 & _T_1087) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel ReleaseData carries invalid shrink param (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_183 & _T_1087) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_193 & _T_1012) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAck carries unmanaged address (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_193 & _T_1012) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_193 & _T_1022) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAck address not aligned to size (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_193 & _T_1022) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_193 & _T_1144) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAck carries invalid param (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_193 & _T_1144) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_193 & _T_1030) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAck is corrupt (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_193 & _T_1030) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_201 & _T_1012) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAckData carries unmanaged address (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_201 & _T_1012) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_201 & _T_1022) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAckData address not aligned to size (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_201 & _T_1022) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_201 & _T_1144) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel AccessAckData carries invalid param (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_201 & _T_1144) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_207 & _T_1012) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel HintAck carries unmanaged address (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_207 & _T_1012) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_207 & _T_1022) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel HintAck address not aligned to size (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_207 & _T_1022) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_207 & _T_1144) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel HintAck carries invalid param (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_207 & _T_1144) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_207 & _T_1030) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel HintAck is corrupt (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_207 & _T_1030) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1211 & _T_1215) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1211 & _T_1215) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1211 & _T_1219) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel param changed within multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1211 & _T_1219) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1211 & _T_1223) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel size changed within multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1211 & _T_1223) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1211 & _T_1227) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel source changed within multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1211 & _T_1227) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1211 & _T_1231) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1211 & _T_1231) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1260 & _T_1264) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1260 & _T_1264) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1260 & _T_1268) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel param changed within multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1260 & _T_1268) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1260 & _T_1272) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1260 & _T_1272) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1260 & _T_1276) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel source changed within multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1260 & _T_1276) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1260 & _T_1280) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel sink changed with multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1260 & _T_1280) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1260 & _T_1284) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1260 & _T_1284) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1361 & _T_1365) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel opcode changed within multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1361 & _T_1365) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1361 & _T_1369) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel param changed within multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1361 & _T_1369) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1361 & _T_1373) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel size changed within multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1361 & _T_1373) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1361 & _T_1377) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel source changed within multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1361 & _T_1377) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1361 & _T_1381) begin
          $fwrite(32'h80000002,"Assertion Failed: 'C' channel address changed with multibeat operation (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1361 & _T_1381) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1426 & _T_1434) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at Configs.scala:156:34)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1426 & _T_1434) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1441 & _T_1448) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1441 & _T_1448) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1455) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' and 'D' concurrent, despite minlatency 2 (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1455) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1468) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at Configs.scala:156:34)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1468) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1501 & _T_1508) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel re-used a sink ID (connected at Configs.scala:156:34)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1501 & _T_1508) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_e_valid & _T_1519) begin
          $fwrite(32'h80000002,"Assertion Failed: 'E' channel acknowledged for nothing inflight (connected at Configs.scala:156:34)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_e_valid & _T_1519) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
