//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__FloppedLanePositionedQueueModule(
  input         rf_reset,
  input         clock,
  input         reset,
  output [3:0]  io_enq_ready,
  input  [1:0]  io_enq_valid,
  input  [4:0]  io_enq_bits_0_reason,
  input  [38:0] io_enq_bits_0_iaddr,
  input  [13:0] io_enq_bits_0_icnt,
  input  [31:0] io_enq_bits_0_hist,
  input  [1:0]  io_enq_bits_0_hisv,
  input  [4:0]  io_enq_bits_1_reason,
  input  [38:0] io_enq_bits_1_iaddr,
  input  [13:0] io_enq_bits_1_icnt,
  input  [31:0] io_enq_bits_1_hist,
  input  [1:0]  io_enq_bits_1_hisv,
  input  [1:0]  io_deq_ready,
  output [3:0]  io_deq_valid,
  output [4:0]  io_deq_bits_0_reason,
  output [38:0] io_deq_bits_0_iaddr,
  output [13:0] io_deq_bits_0_icnt,
  output [31:0] io_deq_bits_0_hist,
  output [1:0]  io_deq_bits_0_hisv,
  output [4:0]  io_deq_bits_1_reason,
  output [38:0] io_deq_bits_1_iaddr,
  output [13:0] io_deq_bits_1_icnt,
  output [31:0] io_deq_bits_1_hist,
  output [1:0]  io_deq_bits_1_hisv,
  output        io_enq_0_lane,
  output        io_deq_0_lane
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [63:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [63:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [63:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [63:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [63:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [63:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [63:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [63:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
`endif // RANDOMIZE_REG_INIT
  reg  enq_lane; // @[LanePositionedQueue.scala 151:24]
  wire [1:0] _GEN_194 = {{1'd0}, enq_lane}; // @[LanePositionedQueue.scala 152:19]
  wire [2:0] z = _GEN_194 + io_enq_valid; // @[LanePositionedQueue.scala 152:19]
  wire  enq_wrap = z[1]; // @[LanePositionedQueue.scala 155:16]
  reg  deq_lane; // @[LanePositionedQueue.scala 151:24]
  wire [1:0] _GEN_195 = {{1'd0}, deq_lane}; // @[LanePositionedQueue.scala 152:19]
  wire [2:0] z_1 = _GEN_195 + io_deq_ready; // @[LanePositionedQueue.scala 152:19]
  wire  deq_wrap = z_1[1]; // @[LanePositionedQueue.scala 155:16]
  reg [1:0] enq_row; // @[LanePositionedQueue.scala 167:24]
  wire [1:0] enq_row1 = enq_row + 2'h1; // @[LanePositionedQueue.scala 168:31]
  wire [1:0] _GEN_0 = enq_wrap ? enq_row1 : enq_row; // @[LanePositionedQueue.scala 172:18 LanePositionedQueue.scala 172:24 LanePositionedQueue.scala 167:24]
  reg [1:0] deq_row; // @[LanePositionedQueue.scala 167:24]
  wire [1:0] deq_row1 = deq_row + 2'h1; // @[LanePositionedQueue.scala 168:31]
  wire [1:0] _GEN_1 = deq_wrap ? deq_row1 : deq_row; // @[LanePositionedQueue.scala 172:18 LanePositionedQueue.scala 172:24 LanePositionedQueue.scala 167:24]
  reg [3:0] nDeq; // @[LanePositionedQueue.scala 183:24]
  reg [3:0] nEnq; // @[LanePositionedQueue.scala 185:24]
  wire [3:0] _GEN_196 = {{2'd0}, io_enq_valid}; // @[LanePositionedQueue.scala 194:30]
  wire [3:0] _GEN_197 = {{2'd0}, io_deq_ready}; // @[LanePositionedQueue.scala 195:30]
  wire [3:0] _nDeq_next_T_1 = nDeq + _GEN_196; // @[LanePositionedQueue.scala 197:30]
  wire [3:0] _nEnq_next_T_1 = nEnq + _GEN_197; // @[LanePositionedQueue.scala 199:30]
  wire [2:0] _GEN_3 = {{1'd0}, _GEN_0}; // @[LanePositionedQueue.scala 213:18 LanePositionedQueue.scala 215:29]
  wire [2:0] _GEN_7 = {{1'd0}, _GEN_1}; // @[LanePositionedQueue.scala 222:19 LanePositionedQueue.scala 224:29]
  wire [2:0] _enq_vmask_T = io_enq_valid + _GEN_194; // @[LanePositionedQueue.scala 247:42]
  wire [9:0] _enq_vmask_T_2 = 10'h7 << _enq_vmask_T; // @[package.scala 234:77]
  wire [2:0] _enq_vmask_T_4 = ~_enq_vmask_T_2[2:0]; // @[package.scala 234:46]
  wire [3:0] enq_vmask = {{1'd0}, _enq_vmask_T_4}; // @[LanePositionedQueue.scala 247:69]
  wire [1:0] _enq_lmask_T_1 = 2'h1 << enq_lane; // @[package.scala 234:77]
  wire  _enq_lmask_T_3 = ~_enq_lmask_T_1[0]; // @[package.scala 234:46]
  wire [3:0] enq_lmask = {{3'd0}, _enq_lmask_T_3}; // @[LanePositionedQueue.scala 248:77]
  wire [3:0] _enq_mask_T = ~enq_lmask; // @[LanePositionedQueue.scala 249:31]
  wire [3:0] enq_mask = enq_vmask & _enq_mask_T; // @[LanePositionedQueue.scala 249:29]
  wire [1:0] _deq_lmask_T_1 = 2'h1 << deq_lane; // @[package.scala 234:77]
  wire  _deq_lmask_T_3 = ~_deq_lmask_T_1[0]; // @[package.scala 234:46]
  wire [3:0] deq_lmask = {{3'd0}, _deq_lmask_T_3}; // @[LanePositionedQueue.scala 252:76]
  reg [4:0] bank_0_0_0_reason; // @[LanePositionedQueue.scala 287:31]
  reg [38:0] bank_0_0_0_iaddr; // @[LanePositionedQueue.scala 287:31]
  reg [13:0] bank_0_0_0_icnt; // @[LanePositionedQueue.scala 287:31]
  reg [31:0] bank_0_0_0_hist; // @[LanePositionedQueue.scala 287:31]
  reg [1:0] bank_0_0_0_hisv; // @[LanePositionedQueue.scala 287:31]
  reg [4:0] bank_0_0_1_reason; // @[LanePositionedQueue.scala 287:31]
  reg [38:0] bank_0_0_1_iaddr; // @[LanePositionedQueue.scala 287:31]
  reg [13:0] bank_0_0_1_icnt; // @[LanePositionedQueue.scala 287:31]
  reg [31:0] bank_0_0_1_hist; // @[LanePositionedQueue.scala 287:31]
  reg [1:0] bank_0_0_1_hisv; // @[LanePositionedQueue.scala 287:31]
  reg [4:0] bank_0_1_0_reason; // @[LanePositionedQueue.scala 287:31]
  reg [38:0] bank_0_1_0_iaddr; // @[LanePositionedQueue.scala 287:31]
  reg [13:0] bank_0_1_0_icnt; // @[LanePositionedQueue.scala 287:31]
  reg [31:0] bank_0_1_0_hist; // @[LanePositionedQueue.scala 287:31]
  reg [1:0] bank_0_1_0_hisv; // @[LanePositionedQueue.scala 287:31]
  reg [4:0] bank_0_1_1_reason; // @[LanePositionedQueue.scala 287:31]
  reg [38:0] bank_0_1_1_iaddr; // @[LanePositionedQueue.scala 287:31]
  reg [13:0] bank_0_1_1_icnt; // @[LanePositionedQueue.scala 287:31]
  reg [31:0] bank_0_1_1_hist; // @[LanePositionedQueue.scala 287:31]
  reg [1:0] bank_0_1_1_hisv; // @[LanePositionedQueue.scala 287:31]
  wire  bank_0Intf_b0_out_addr = deq_row1[1]; // @[LanePositionedQueue.scala 289:38]
  wire [4:0] bank_0Intf_b0_out_data_0_reason = bank_0Intf_b0_out_addr ? bank_0_1_0_reason : bank_0_0_0_reason; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [38:0] bank_0Intf_b0_out_data_0_iaddr = bank_0Intf_b0_out_addr ? bank_0_1_0_iaddr : bank_0_0_0_iaddr; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [13:0] bank_0Intf_b0_out_data_0_icnt = bank_0Intf_b0_out_addr ? bank_0_1_0_icnt : bank_0_0_0_icnt; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [31:0] bank_0Intf_b0_out_data_0_hist = bank_0Intf_b0_out_addr ? bank_0_1_0_hist : bank_0_0_0_hist; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [1:0] bank_0Intf_b0_out_data_0_hisv = bank_0Intf_b0_out_addr ? bank_0_1_0_hisv : bank_0_0_0_hisv; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [4:0] bank_0Intf_b0_out_data_1_reason = bank_0Intf_b0_out_addr ? bank_0_1_1_reason : bank_0_0_1_reason; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [38:0] bank_0Intf_b0_out_data_1_iaddr = bank_0Intf_b0_out_addr ? bank_0_1_1_iaddr : bank_0_0_1_iaddr; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [13:0] bank_0Intf_b0_out_data_1_icnt = bank_0Intf_b0_out_addr ? bank_0_1_1_icnt : bank_0_0_1_icnt; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [31:0] bank_0Intf_b0_out_data_1_hist = bank_0Intf_b0_out_addr ? bank_0_1_1_hist : bank_0_0_1_hist; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [1:0] bank_0Intf_b0_out_data_1_hisv = bank_0Intf_b0_out_addr ? bank_0_1_1_hisv : bank_0_0_1_hisv; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire  b0_row = enq_row1[1]; // @[LanePositionedQueue.scala 299:26]
  wire [1:0] hi_mask = enq_mask[3:2]; // @[LanePositionedQueue.scala 295:25]
  wire [1:0] lo_mask = enq_mask[1:0]; // @[LanePositionedQueue.scala 296:25]
  wire [1:0] _b0_mask_T_1 = enq_row[0] ? hi_mask : lo_mask; // @[LanePositionedQueue.scala 297:20]
  wire  b0_mask_0 = _b0_mask_T_1[0]; // @[LanePositionedQueue.scala 297:51]
  wire  b0_mask_1 = _b0_mask_T_1[1]; // @[LanePositionedQueue.scala 297:51]
  reg [4:0] bank_1_0_0_reason; // @[LanePositionedQueue.scala 287:31]
  reg [38:0] bank_1_0_0_iaddr; // @[LanePositionedQueue.scala 287:31]
  reg [13:0] bank_1_0_0_icnt; // @[LanePositionedQueue.scala 287:31]
  reg [31:0] bank_1_0_0_hist; // @[LanePositionedQueue.scala 287:31]
  reg [1:0] bank_1_0_0_hisv; // @[LanePositionedQueue.scala 287:31]
  reg [4:0] bank_1_0_1_reason; // @[LanePositionedQueue.scala 287:31]
  reg [38:0] bank_1_0_1_iaddr; // @[LanePositionedQueue.scala 287:31]
  reg [13:0] bank_1_0_1_icnt; // @[LanePositionedQueue.scala 287:31]
  reg [31:0] bank_1_0_1_hist; // @[LanePositionedQueue.scala 287:31]
  reg [1:0] bank_1_0_1_hisv; // @[LanePositionedQueue.scala 287:31]
  reg [4:0] bank_1_1_0_reason; // @[LanePositionedQueue.scala 287:31]
  reg [38:0] bank_1_1_0_iaddr; // @[LanePositionedQueue.scala 287:31]
  reg [13:0] bank_1_1_0_icnt; // @[LanePositionedQueue.scala 287:31]
  reg [31:0] bank_1_1_0_hist; // @[LanePositionedQueue.scala 287:31]
  reg [1:0] bank_1_1_0_hisv; // @[LanePositionedQueue.scala 287:31]
  reg [4:0] bank_1_1_1_reason; // @[LanePositionedQueue.scala 287:31]
  reg [38:0] bank_1_1_1_iaddr; // @[LanePositionedQueue.scala 287:31]
  reg [13:0] bank_1_1_1_icnt; // @[LanePositionedQueue.scala 287:31]
  reg [31:0] bank_1_1_1_hist; // @[LanePositionedQueue.scala 287:31]
  reg [1:0] bank_1_1_1_hisv; // @[LanePositionedQueue.scala 287:31]
  wire  bank_1Intf_b1_out_addr = deq_row[1]; // @[LanePositionedQueue.scala 290:38]
  wire [4:0] bank_1Intf_b1_out_data_0_reason = bank_1Intf_b1_out_addr ? bank_1_1_0_reason : bank_1_0_0_reason; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [38:0] bank_1Intf_b1_out_data_0_iaddr = bank_1Intf_b1_out_addr ? bank_1_1_0_iaddr : bank_1_0_0_iaddr; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [13:0] bank_1Intf_b1_out_data_0_icnt = bank_1Intf_b1_out_addr ? bank_1_1_0_icnt : bank_1_0_0_icnt; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [31:0] bank_1Intf_b1_out_data_0_hist = bank_1Intf_b1_out_addr ? bank_1_1_0_hist : bank_1_0_0_hist; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [1:0] bank_1Intf_b1_out_data_0_hisv = bank_1Intf_b1_out_addr ? bank_1_1_0_hisv : bank_1_0_0_hisv; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [4:0] bank_1Intf_b1_out_data_1_reason = bank_1Intf_b1_out_addr ? bank_1_1_1_reason : bank_1_0_1_reason; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [38:0] bank_1Intf_b1_out_data_1_iaddr = bank_1Intf_b1_out_addr ? bank_1_1_1_iaddr : bank_1_0_1_iaddr; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [13:0] bank_1Intf_b1_out_data_1_icnt = bank_1Intf_b1_out_addr ? bank_1_1_1_icnt : bank_1_0_1_icnt; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [31:0] bank_1Intf_b1_out_data_1_hist = bank_1Intf_b1_out_addr ? bank_1_1_1_hist : bank_1_0_1_hist; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire [1:0] bank_1Intf_b1_out_data_1_hisv = bank_1Intf_b1_out_addr ? bank_1_1_1_hisv : bank_1_0_1_hisv; // @[LanePositionedQueue.scala 287:31 LanePositionedQueue.scala 287:31]
  wire  b1_row = enq_row[1]; // @[LanePositionedQueue.scala 300:26]
  wire [1:0] _b1_mask_T_1 = enq_row[0] ? lo_mask : hi_mask; // @[LanePositionedQueue.scala 298:20]
  wire  b1_mask_0 = _b1_mask_T_1[0]; // @[LanePositionedQueue.scala 298:51]
  wire  b1_mask_1 = _b1_mask_T_1[1]; // @[LanePositionedQueue.scala 298:51]
  assign io_enq_ready = nEnq; // @[LanePositionedQueue.scala 238:16]
  assign io_deq_valid = nDeq; // @[LanePositionedQueue.scala 239:16]
  assign io_deq_bits_0_reason = deq_row[0] ^ deq_lmask[0] ? bank_1Intf_b1_out_data_0_reason :
    bank_0Intf_b0_out_data_0_reason; // @[LanePositionedQueue.scala 292:23]
  assign io_deq_bits_0_iaddr = deq_row[0] ^ deq_lmask[0] ? bank_1Intf_b1_out_data_0_iaddr :
    bank_0Intf_b0_out_data_0_iaddr; // @[LanePositionedQueue.scala 292:23]
  assign io_deq_bits_0_icnt = deq_row[0] ^ deq_lmask[0] ? bank_1Intf_b1_out_data_0_icnt : bank_0Intf_b0_out_data_0_icnt; // @[LanePositionedQueue.scala 292:23]
  assign io_deq_bits_0_hist = deq_row[0] ^ deq_lmask[0] ? bank_1Intf_b1_out_data_0_hist : bank_0Intf_b0_out_data_0_hist; // @[LanePositionedQueue.scala 292:23]
  assign io_deq_bits_0_hisv = deq_row[0] ^ deq_lmask[0] ? bank_1Intf_b1_out_data_0_hisv : bank_0Intf_b0_out_data_0_hisv; // @[LanePositionedQueue.scala 292:23]
  assign io_deq_bits_1_reason = deq_row[0] ^ deq_lmask[1] ? bank_1Intf_b1_out_data_1_reason :
    bank_0Intf_b0_out_data_1_reason; // @[LanePositionedQueue.scala 292:23]
  assign io_deq_bits_1_iaddr = deq_row[0] ^ deq_lmask[1] ? bank_1Intf_b1_out_data_1_iaddr :
    bank_0Intf_b0_out_data_1_iaddr; // @[LanePositionedQueue.scala 292:23]
  assign io_deq_bits_1_icnt = deq_row[0] ^ deq_lmask[1] ? bank_1Intf_b1_out_data_1_icnt : bank_0Intf_b0_out_data_1_icnt; // @[LanePositionedQueue.scala 292:23]
  assign io_deq_bits_1_hist = deq_row[0] ^ deq_lmask[1] ? bank_1Intf_b1_out_data_1_hist : bank_0Intf_b0_out_data_1_hist; // @[LanePositionedQueue.scala 292:23]
  assign io_deq_bits_1_hisv = deq_row[0] ^ deq_lmask[1] ? bank_1Intf_b1_out_data_1_hisv : bank_0Intf_b0_out_data_1_hisv; // @[LanePositionedQueue.scala 292:23]
  assign io_enq_0_lane = enq_lane; // @[LanePositionedQueue.scala 236:17]
  assign io_deq_0_lane = deq_lane; // @[LanePositionedQueue.scala 237:17]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      enq_lane <= 1'h0;
    end else begin
      enq_lane <= z[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      deq_lane <= 1'h0;
    end else begin
      deq_lane <= z_1[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      enq_row <= 2'h0;
    end else begin
      enq_row <= _GEN_3[1:0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      deq_row <= 2'h0;
    end else begin
      deq_row <= _GEN_7[1:0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      nDeq <= 4'h0;
    end else begin
      nDeq <= _nDeq_next_T_1 - _GEN_197;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      nEnq <= 4'h8;
    end else begin
      nEnq <= _nEnq_next_T_1 - _GEN_196;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_0_0_reason <= 5'h0;
    end else if (b0_mask_0) begin
      if (~b0_row) begin
        bank_0_0_0_reason <= io_enq_bits_0_reason;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_0_0_iaddr <= 39'h0;
    end else if (b0_mask_0) begin
      if (~b0_row) begin
        bank_0_0_0_iaddr <= io_enq_bits_0_iaddr;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_0_0_icnt <= 14'h0;
    end else if (b0_mask_0) begin
      if (~b0_row) begin
        bank_0_0_0_icnt <= io_enq_bits_0_icnt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_0_0_hist <= 32'h0;
    end else if (b0_mask_0) begin
      if (~b0_row) begin
        bank_0_0_0_hist <= io_enq_bits_0_hist;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_0_0_hisv <= 2'h0;
    end else if (b0_mask_0) begin
      if (~b0_row) begin
        bank_0_0_0_hisv <= io_enq_bits_0_hisv;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_0_1_reason <= 5'h0;
    end else if (b0_mask_1) begin
      if (~b0_row) begin
        bank_0_0_1_reason <= io_enq_bits_1_reason;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_0_1_iaddr <= 39'h0;
    end else if (b0_mask_1) begin
      if (~b0_row) begin
        bank_0_0_1_iaddr <= io_enq_bits_1_iaddr;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_0_1_icnt <= 14'h0;
    end else if (b0_mask_1) begin
      if (~b0_row) begin
        bank_0_0_1_icnt <= io_enq_bits_1_icnt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_0_1_hist <= 32'h0;
    end else if (b0_mask_1) begin
      if (~b0_row) begin
        bank_0_0_1_hist <= io_enq_bits_1_hist;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_0_1_hisv <= 2'h0;
    end else if (b0_mask_1) begin
      if (~b0_row) begin
        bank_0_0_1_hisv <= io_enq_bits_1_hisv;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_1_0_reason <= 5'h0;
    end else if (b0_mask_0) begin
      if (b0_row) begin
        bank_0_1_0_reason <= io_enq_bits_0_reason;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_1_0_iaddr <= 39'h0;
    end else if (b0_mask_0) begin
      if (b0_row) begin
        bank_0_1_0_iaddr <= io_enq_bits_0_iaddr;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_1_0_icnt <= 14'h0;
    end else if (b0_mask_0) begin
      if (b0_row) begin
        bank_0_1_0_icnt <= io_enq_bits_0_icnt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_1_0_hist <= 32'h0;
    end else if (b0_mask_0) begin
      if (b0_row) begin
        bank_0_1_0_hist <= io_enq_bits_0_hist;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_1_0_hisv <= 2'h0;
    end else if (b0_mask_0) begin
      if (b0_row) begin
        bank_0_1_0_hisv <= io_enq_bits_0_hisv;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_1_1_reason <= 5'h0;
    end else if (b0_mask_1) begin
      if (b0_row) begin
        bank_0_1_1_reason <= io_enq_bits_1_reason;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_1_1_iaddr <= 39'h0;
    end else if (b0_mask_1) begin
      if (b0_row) begin
        bank_0_1_1_iaddr <= io_enq_bits_1_iaddr;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_1_1_icnt <= 14'h0;
    end else if (b0_mask_1) begin
      if (b0_row) begin
        bank_0_1_1_icnt <= io_enq_bits_1_icnt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_1_1_hist <= 32'h0;
    end else if (b0_mask_1) begin
      if (b0_row) begin
        bank_0_1_1_hist <= io_enq_bits_1_hist;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_0_1_1_hisv <= 2'h0;
    end else if (b0_mask_1) begin
      if (b0_row) begin
        bank_0_1_1_hisv <= io_enq_bits_1_hisv;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_0_0_reason <= 5'h0;
    end else if (b1_mask_0) begin
      if (~b1_row) begin
        bank_1_0_0_reason <= io_enq_bits_0_reason;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_0_0_iaddr <= 39'h0;
    end else if (b1_mask_0) begin
      if (~b1_row) begin
        bank_1_0_0_iaddr <= io_enq_bits_0_iaddr;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_0_0_icnt <= 14'h0;
    end else if (b1_mask_0) begin
      if (~b1_row) begin
        bank_1_0_0_icnt <= io_enq_bits_0_icnt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_0_0_hist <= 32'h0;
    end else if (b1_mask_0) begin
      if (~b1_row) begin
        bank_1_0_0_hist <= io_enq_bits_0_hist;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_0_0_hisv <= 2'h0;
    end else if (b1_mask_0) begin
      if (~b1_row) begin
        bank_1_0_0_hisv <= io_enq_bits_0_hisv;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_0_1_reason <= 5'h0;
    end else if (b1_mask_1) begin
      if (~b1_row) begin
        bank_1_0_1_reason <= io_enq_bits_1_reason;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_0_1_iaddr <= 39'h0;
    end else if (b1_mask_1) begin
      if (~b1_row) begin
        bank_1_0_1_iaddr <= io_enq_bits_1_iaddr;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_0_1_icnt <= 14'h0;
    end else if (b1_mask_1) begin
      if (~b1_row) begin
        bank_1_0_1_icnt <= io_enq_bits_1_icnt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_0_1_hist <= 32'h0;
    end else if (b1_mask_1) begin
      if (~b1_row) begin
        bank_1_0_1_hist <= io_enq_bits_1_hist;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_0_1_hisv <= 2'h0;
    end else if (b1_mask_1) begin
      if (~b1_row) begin
        bank_1_0_1_hisv <= io_enq_bits_1_hisv;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_1_0_reason <= 5'h0;
    end else if (b1_mask_0) begin
      if (b1_row) begin
        bank_1_1_0_reason <= io_enq_bits_0_reason;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_1_0_iaddr <= 39'h0;
    end else if (b1_mask_0) begin
      if (b1_row) begin
        bank_1_1_0_iaddr <= io_enq_bits_0_iaddr;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_1_0_icnt <= 14'h0;
    end else if (b1_mask_0) begin
      if (b1_row) begin
        bank_1_1_0_icnt <= io_enq_bits_0_icnt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_1_0_hist <= 32'h0;
    end else if (b1_mask_0) begin
      if (b1_row) begin
        bank_1_1_0_hist <= io_enq_bits_0_hist;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_1_0_hisv <= 2'h0;
    end else if (b1_mask_0) begin
      if (b1_row) begin
        bank_1_1_0_hisv <= io_enq_bits_0_hisv;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_1_1_reason <= 5'h0;
    end else if (b1_mask_1) begin
      if (b1_row) begin
        bank_1_1_1_reason <= io_enq_bits_1_reason;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_1_1_iaddr <= 39'h0;
    end else if (b1_mask_1) begin
      if (b1_row) begin
        bank_1_1_1_iaddr <= io_enq_bits_1_iaddr;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_1_1_icnt <= 14'h0;
    end else if (b1_mask_1) begin
      if (b1_row) begin
        bank_1_1_1_icnt <= io_enq_bits_1_icnt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_1_1_hist <= 32'h0;
    end else if (b1_mask_1) begin
      if (b1_row) begin
        bank_1_1_1_hist <= io_enq_bits_1_hist;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bank_1_1_1_hisv <= 2'h0;
    end else if (b1_mask_1) begin
      if (b1_row) begin
        bank_1_1_1_hisv <= io_enq_bits_1_hisv;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  enq_lane = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  deq_lane = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  enq_row = _RAND_2[1:0];
  _RAND_3 = {1{`RANDOM}};
  deq_row = _RAND_3[1:0];
  _RAND_4 = {1{`RANDOM}};
  nDeq = _RAND_4[3:0];
  _RAND_5 = {1{`RANDOM}};
  nEnq = _RAND_5[3:0];
  _RAND_6 = {1{`RANDOM}};
  bank_0_0_0_reason = _RAND_6[4:0];
  _RAND_7 = {2{`RANDOM}};
  bank_0_0_0_iaddr = _RAND_7[38:0];
  _RAND_8 = {1{`RANDOM}};
  bank_0_0_0_icnt = _RAND_8[13:0];
  _RAND_9 = {1{`RANDOM}};
  bank_0_0_0_hist = _RAND_9[31:0];
  _RAND_10 = {1{`RANDOM}};
  bank_0_0_0_hisv = _RAND_10[1:0];
  _RAND_11 = {1{`RANDOM}};
  bank_0_0_1_reason = _RAND_11[4:0];
  _RAND_12 = {2{`RANDOM}};
  bank_0_0_1_iaddr = _RAND_12[38:0];
  _RAND_13 = {1{`RANDOM}};
  bank_0_0_1_icnt = _RAND_13[13:0];
  _RAND_14 = {1{`RANDOM}};
  bank_0_0_1_hist = _RAND_14[31:0];
  _RAND_15 = {1{`RANDOM}};
  bank_0_0_1_hisv = _RAND_15[1:0];
  _RAND_16 = {1{`RANDOM}};
  bank_0_1_0_reason = _RAND_16[4:0];
  _RAND_17 = {2{`RANDOM}};
  bank_0_1_0_iaddr = _RAND_17[38:0];
  _RAND_18 = {1{`RANDOM}};
  bank_0_1_0_icnt = _RAND_18[13:0];
  _RAND_19 = {1{`RANDOM}};
  bank_0_1_0_hist = _RAND_19[31:0];
  _RAND_20 = {1{`RANDOM}};
  bank_0_1_0_hisv = _RAND_20[1:0];
  _RAND_21 = {1{`RANDOM}};
  bank_0_1_1_reason = _RAND_21[4:0];
  _RAND_22 = {2{`RANDOM}};
  bank_0_1_1_iaddr = _RAND_22[38:0];
  _RAND_23 = {1{`RANDOM}};
  bank_0_1_1_icnt = _RAND_23[13:0];
  _RAND_24 = {1{`RANDOM}};
  bank_0_1_1_hist = _RAND_24[31:0];
  _RAND_25 = {1{`RANDOM}};
  bank_0_1_1_hisv = _RAND_25[1:0];
  _RAND_26 = {1{`RANDOM}};
  bank_1_0_0_reason = _RAND_26[4:0];
  _RAND_27 = {2{`RANDOM}};
  bank_1_0_0_iaddr = _RAND_27[38:0];
  _RAND_28 = {1{`RANDOM}};
  bank_1_0_0_icnt = _RAND_28[13:0];
  _RAND_29 = {1{`RANDOM}};
  bank_1_0_0_hist = _RAND_29[31:0];
  _RAND_30 = {1{`RANDOM}};
  bank_1_0_0_hisv = _RAND_30[1:0];
  _RAND_31 = {1{`RANDOM}};
  bank_1_0_1_reason = _RAND_31[4:0];
  _RAND_32 = {2{`RANDOM}};
  bank_1_0_1_iaddr = _RAND_32[38:0];
  _RAND_33 = {1{`RANDOM}};
  bank_1_0_1_icnt = _RAND_33[13:0];
  _RAND_34 = {1{`RANDOM}};
  bank_1_0_1_hist = _RAND_34[31:0];
  _RAND_35 = {1{`RANDOM}};
  bank_1_0_1_hisv = _RAND_35[1:0];
  _RAND_36 = {1{`RANDOM}};
  bank_1_1_0_reason = _RAND_36[4:0];
  _RAND_37 = {2{`RANDOM}};
  bank_1_1_0_iaddr = _RAND_37[38:0];
  _RAND_38 = {1{`RANDOM}};
  bank_1_1_0_icnt = _RAND_38[13:0];
  _RAND_39 = {1{`RANDOM}};
  bank_1_1_0_hist = _RAND_39[31:0];
  _RAND_40 = {1{`RANDOM}};
  bank_1_1_0_hisv = _RAND_40[1:0];
  _RAND_41 = {1{`RANDOM}};
  bank_1_1_1_reason = _RAND_41[4:0];
  _RAND_42 = {2{`RANDOM}};
  bank_1_1_1_iaddr = _RAND_42[38:0];
  _RAND_43 = {1{`RANDOM}};
  bank_1_1_1_icnt = _RAND_43[13:0];
  _RAND_44 = {1{`RANDOM}};
  bank_1_1_1_hist = _RAND_44[31:0];
  _RAND_45 = {1{`RANDOM}};
  bank_1_1_1_hisv = _RAND_45[1:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    enq_lane = 1'h0;
  end
  if (reset) begin
    deq_lane = 1'h0;
  end
  if (reset) begin
    enq_row = 2'h0;
  end
  if (reset) begin
    deq_row = 2'h0;
  end
  if (reset) begin
    nDeq = 4'h0;
  end
  if (reset) begin
    nEnq = 4'h8;
  end
  if (rf_reset) begin
    bank_0_0_0_reason = 5'h0;
  end
  if (rf_reset) begin
    bank_0_0_0_iaddr = 39'h0;
  end
  if (rf_reset) begin
    bank_0_0_0_icnt = 14'h0;
  end
  if (rf_reset) begin
    bank_0_0_0_hist = 32'h0;
  end
  if (rf_reset) begin
    bank_0_0_0_hisv = 2'h0;
  end
  if (rf_reset) begin
    bank_0_0_1_reason = 5'h0;
  end
  if (rf_reset) begin
    bank_0_0_1_iaddr = 39'h0;
  end
  if (rf_reset) begin
    bank_0_0_1_icnt = 14'h0;
  end
  if (rf_reset) begin
    bank_0_0_1_hist = 32'h0;
  end
  if (rf_reset) begin
    bank_0_0_1_hisv = 2'h0;
  end
  if (rf_reset) begin
    bank_0_1_0_reason = 5'h0;
  end
  if (rf_reset) begin
    bank_0_1_0_iaddr = 39'h0;
  end
  if (rf_reset) begin
    bank_0_1_0_icnt = 14'h0;
  end
  if (rf_reset) begin
    bank_0_1_0_hist = 32'h0;
  end
  if (rf_reset) begin
    bank_0_1_0_hisv = 2'h0;
  end
  if (rf_reset) begin
    bank_0_1_1_reason = 5'h0;
  end
  if (rf_reset) begin
    bank_0_1_1_iaddr = 39'h0;
  end
  if (rf_reset) begin
    bank_0_1_1_icnt = 14'h0;
  end
  if (rf_reset) begin
    bank_0_1_1_hist = 32'h0;
  end
  if (rf_reset) begin
    bank_0_1_1_hisv = 2'h0;
  end
  if (rf_reset) begin
    bank_1_0_0_reason = 5'h0;
  end
  if (rf_reset) begin
    bank_1_0_0_iaddr = 39'h0;
  end
  if (rf_reset) begin
    bank_1_0_0_icnt = 14'h0;
  end
  if (rf_reset) begin
    bank_1_0_0_hist = 32'h0;
  end
  if (rf_reset) begin
    bank_1_0_0_hisv = 2'h0;
  end
  if (rf_reset) begin
    bank_1_0_1_reason = 5'h0;
  end
  if (rf_reset) begin
    bank_1_0_1_iaddr = 39'h0;
  end
  if (rf_reset) begin
    bank_1_0_1_icnt = 14'h0;
  end
  if (rf_reset) begin
    bank_1_0_1_hist = 32'h0;
  end
  if (rf_reset) begin
    bank_1_0_1_hisv = 2'h0;
  end
  if (rf_reset) begin
    bank_1_1_0_reason = 5'h0;
  end
  if (rf_reset) begin
    bank_1_1_0_iaddr = 39'h0;
  end
  if (rf_reset) begin
    bank_1_1_0_icnt = 14'h0;
  end
  if (rf_reset) begin
    bank_1_1_0_hist = 32'h0;
  end
  if (rf_reset) begin
    bank_1_1_0_hisv = 2'h0;
  end
  if (rf_reset) begin
    bank_1_1_1_reason = 5'h0;
  end
  if (rf_reset) begin
    bank_1_1_1_iaddr = 39'h0;
  end
  if (rf_reset) begin
    bank_1_1_1_icnt = 14'h0;
  end
  if (rf_reset) begin
    bank_1_1_1_hist = 32'h0;
  end
  if (rf_reset) begin
    bank_1_1_1_hisv = 2'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
