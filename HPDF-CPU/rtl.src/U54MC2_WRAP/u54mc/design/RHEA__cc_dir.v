//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__cc_dir(
  input  [9:0]  RW0_addr,
  input         RW0_en,
  input         RW0_clk,
  input         RW0_wmode,
  input  [24:0] RW0_wdata_0,
  input  [24:0] RW0_wdata_1,
  input  [24:0] RW0_wdata_2,
  input  [24:0] RW0_wdata_3,
  input  [24:0] RW0_wdata_4,
  input  [24:0] RW0_wdata_5,
  input  [24:0] RW0_wdata_6,
  input  [24:0] RW0_wdata_7,
  output [24:0] RW0_rdata_0,
  output [24:0] RW0_rdata_1,
  output [24:0] RW0_rdata_2,
  output [24:0] RW0_rdata_3,
  output [24:0] RW0_rdata_4,
  output [24:0] RW0_rdata_5,
  output [24:0] RW0_rdata_6,
  output [24:0] RW0_rdata_7,
  input         RW0_wmask_0,
  input         RW0_wmask_1,
  input         RW0_wmask_2,
  input         RW0_wmask_3,
  input         RW0_wmask_4,
  input         RW0_wmask_5,
  input         RW0_wmask_6,
  input         RW0_wmask_7
);
  wire [9:0] cc_dir_ext_RW0_addr;
  wire  cc_dir_ext_RW0_en;
  wire  cc_dir_ext_RW0_clk;
  wire  cc_dir_ext_RW0_wmode;
  wire [199:0] cc_dir_ext_RW0_wdata;
  wire [199:0] cc_dir_ext_RW0_rdata;
  wire [7:0] cc_dir_ext_RW0_wmask;
  wire [99:0] _GEN_4 = {RW0_wdata_7,RW0_wdata_6,RW0_wdata_5,RW0_wdata_4};
  wire [99:0] _GEN_5 = {RW0_wdata_3,RW0_wdata_2,RW0_wdata_1,RW0_wdata_0};
  wire [3:0] _GEN_10 = {RW0_wmask_7,RW0_wmask_6,RW0_wmask_5,RW0_wmask_4};
  wire [3:0] _GEN_11 = {RW0_wmask_3,RW0_wmask_2,RW0_wmask_1,RW0_wmask_0};
  RHEA__cc_dir_ext cc_dir_ext (
    .RW0_addr(cc_dir_ext_RW0_addr),
    .RW0_en(cc_dir_ext_RW0_en),
    .RW0_clk(cc_dir_ext_RW0_clk),
    .RW0_wmode(cc_dir_ext_RW0_wmode),
    .RW0_wdata(cc_dir_ext_RW0_wdata),
    .RW0_rdata(cc_dir_ext_RW0_rdata),
    .RW0_wmask(cc_dir_ext_RW0_wmask)
  );
  assign cc_dir_ext_RW0_clk = RW0_clk;
  assign cc_dir_ext_RW0_en = RW0_en;
  assign cc_dir_ext_RW0_addr = RW0_addr;
  assign RW0_rdata_0 = cc_dir_ext_RW0_rdata[24:0];
  assign RW0_rdata_1 = cc_dir_ext_RW0_rdata[49:25];
  assign RW0_rdata_2 = cc_dir_ext_RW0_rdata[74:50];
  assign RW0_rdata_3 = cc_dir_ext_RW0_rdata[99:75];
  assign RW0_rdata_4 = cc_dir_ext_RW0_rdata[124:100];
  assign RW0_rdata_5 = cc_dir_ext_RW0_rdata[149:125];
  assign RW0_rdata_6 = cc_dir_ext_RW0_rdata[174:150];
  assign RW0_rdata_7 = cc_dir_ext_RW0_rdata[199:175];
  assign cc_dir_ext_RW0_wmode = RW0_wmode;
  assign cc_dir_ext_RW0_wdata = {_GEN_4,_GEN_5};
  assign cc_dir_ext_RW0_wmask = {_GEN_10,_GEN_11};
endmodule
