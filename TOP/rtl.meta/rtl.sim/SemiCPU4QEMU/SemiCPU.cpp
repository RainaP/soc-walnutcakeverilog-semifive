// SPDX-License-Identifier: Apache-2.0

#include "SemiCPU.h"

extern "C" void semiCPUEnterCritialSection(void);
extern "C" void semiCPULeaveCritialSection(void);
extern "C" void semiCPUWriteReq(unsigned long long deviceAddress, unsigned int data);
extern "C" void semiCPUWriteFlush(unsigned long long deviceAddress);
extern "C" void semiCPUReadReq(unsigned long long deviceAddress, unsigned int len);
extern "C" void semiCPURead(unsigned long long deviceAddress, unsigned int* data);
///////////////////////////////////////////////////////////////////////////////
// AXI bus driver
///////////////////////////////////////////////////////////////////////////////
//read from device
template <typename T>
T readDevice(unsigned long long deviceAddress){
    T data;
    readDevice((void*)&data, deviceAddress, (unsigned int)sizeof(data));
    return data;
}
unsigned char readDevice1B(unsigned long long deviceAddress){
    return readDevice<unsigned char>(deviceAddress);
}
unsigned short readDevice2B(unsigned long long deviceAddress){
    return readDevice<unsigned short>(deviceAddress);
}
unsigned int readDevice4B(unsigned long long deviceAddress){
    return readDevice<unsigned int>(deviceAddress);
}
unsigned long long readDevice8B(unsigned long long deviceAddress){
    return readDevice<unsigned long long>(deviceAddress);
}
void readDevice  (void* buffer, unsigned long long deviceAddress, unsigned int len){
    semiCPUEnterCritialSection();
    semiCPUReadReq(deviceAddress, len);
    for( unsigned int i=0; i<len; i++ ){
        unsigned int data;
        semiCPURead(deviceAddress+i, &data);
        ((unsigned char*)buffer)[i] = data;
    }
    semiCPULeaveCritialSection();
}

//write to device
template <typename T>
void writeDevice(unsigned long long deviceAddress, T data){
    writeDevice (&data, deviceAddress, sizeof(data));
}
void writeDevice1B(unsigned long long deviceAddress, unsigned char      data){
    writeDevice<unsigned char>(deviceAddress, data);
}
void writeDevice2B(unsigned long long deviceAddress, unsigned short     data){
    writeDevice<unsigned short>(deviceAddress, data);
}
void writeDevice4B(unsigned long long deviceAddress, unsigned int       data){
    writeDevice<unsigned int>(deviceAddress, data);
}
void writeDevice8B(unsigned long long deviceAddress, unsigned long long data){
    writeDevice<unsigned long long>(deviceAddress, data);
}
void writeDevice  (const void* buffer, unsigned long long deviceAddress, unsigned int len){
    semiCPUEnterCritialSection();
    for( unsigned int i=0; i<len; i++ ){
        semiCPUWriteReq(deviceAddress+i, (unsigned int)(((unsigned char*)buffer)[i]));
    }
    semiCPUWriteFlush(deviceAddress);
    semiCPULeaveCritialSection();    
}

