module U54MC2_WRAP (

    // Clock & Reset
    input           CLK__TS,
    input           RSTN__TS,

    input           CLK__CPUCORE0,
    input           CLK__CPUCORE1,
    input           CLK__CPU,
    input           RSTN__CPU,
    input           RSTN__CPUCORE0,
    input           RSTN__CPUCORE1,

    input          CLK__DEBUG,
    input          RSTN__DEBUG,

    input          CLK__TRACE_COM,
    input          RSTN__TRACE_COM,
    output         IO__TRACE_TREF,
    output [3:0]   IO__TRACE_TDATA,
    input          CLK__TRACE_0,
    input          RSTN__TRACE_0,
    input          CLK__TRACE_1,
    input          RSTN__TRACE_1,

    // Debug Interface
    input          IO__JTAG_TCK,
    input          IO__JTAG_TMS,
    input          IO__JTAG_TDI,
    output         IO__JTAG_TDO,
    output         IO__JTAG_TDOEN,
    input          IO__JTAG_TRST,
    input  [10:0]  SFR__debug_systemjtag_mfr_id,
    input  [15:0]  SFR__debug_systemjtag_part_number,
    input  [3:0]   SFR__debug_systemjtag_version,
    output         SFR__debug_ndreset,
    output         SFR__debug_dmactive,
    input          SFR__debug_dmactiveAck,

    // Reset Vector
    input  [36:0]  RESET_VECTOR_0,
    input  [36:0]  RESET_VECTOR_1,

    // TODO
    output         SFR__halt_from_tile_0,
    output         SFR__halt_from_tile_1,
    output         SFR__wfi_from_tile_0,
    output         SFR__wfi_from_tile_1,
    output         SFR__cease_from_tile_0,
    output         SFR__cease_from_tile_1,
    output         SFR__debug_from_tile_0,
    output         SFR__debug_from_tile_1,

    input          SFR__psd_test_clock_enable,
    input          SFR__resetctrl_hartIsInReset_0,
    input          SFR__resetctrl_hartIsInReset_1,
    input          SFR__rtc_toggle,

    // Interrupts
    input  [255:0] IRQ_GLB,

    input  [15:0]   local_interrupts_1,
    input  [15:0]   local_interrupts_0,

    // Non-maskable Interrupt
    input           nmi_0_rnmi,
    input  [36:0]   nmi_0_rnmi_interrupt_vector,
    input  [36:0]   nmi_0_rnmi_exception_vector,
    input           nmi_1_rnmi,
    input  [36:0]   nmi_1_rnmi_interrupt_vector,
    input  [36:0]   nmi_1_rnmi_exception_vector,


    // 3 AXI4 Ports
    input          AXI4__CPU__MEMPORT__AWREADY,
    output         AXI4__CPU__MEMPORT__AWVALID,
    output [7:0]   AXI4__CPU__MEMPORT__AWID,
    output [35:0]  AXI4__CPU__MEMPORT__AWADDR,
    output [7:0]   AXI4__CPU__MEMPORT__AWLEN,
    output [2:0]   AXI4__CPU__MEMPORT__AWSIZE,
    output [1:0]   AXI4__CPU__MEMPORT__AWBURST,
    output         AXI4__CPU__MEMPORT__AWLOCK,
    output [3:0]   AXI4__CPU__MEMPORT__AWCACHE,
    output [2:0]   AXI4__CPU__MEMPORT__AWPROT,
    output [3:0]   AXI4__CPU__MEMPORT__AWQOS,
    input          AXI4__CPU__MEMPORT__WREADY,
    output         AXI4__CPU__MEMPORT__WVALID,
    output [127:0] AXI4__CPU__MEMPORT__WDATA,
    output [15:0]  AXI4__CPU__MEMPORT__WSTRB,
    output         AXI4__CPU__MEMPORT__WLAST,
    output         AXI4__CPU__MEMPORT__BREADY,
    input          AXI4__CPU__MEMPORT__BVALID,
    input  [7:0]   AXI4__CPU__MEMPORT__BID,
    input  [1:0]   AXI4__CPU__MEMPORT__BRESP,
    input          AXI4__CPU__MEMPORT__ARREADY,
    output         AXI4__CPU__MEMPORT__ARVALID,
    output [7:0]   AXI4__CPU__MEMPORT__ARID,
    output [35:0]  AXI4__CPU__MEMPORT__ARADDR,
    output [7:0]   AXI4__CPU__MEMPORT__ARLEN,
    output [2:0]   AXI4__CPU__MEMPORT__ARSIZE,
    output [1:0]   AXI4__CPU__MEMPORT__ARBURST,
    output         AXI4__CPU__MEMPORT__ARLOCK,
    output [3:0]   AXI4__CPU__MEMPORT__ARCACHE,
    output [2:0]   AXI4__CPU__MEMPORT__ARPROT,
    output [3:0]   AXI4__CPU__MEMPORT__ARQOS,
    output         AXI4__CPU__MEMPORT__RREADY,
    input          AXI4__CPU__MEMPORT__RVALID,
    input  [7:0]   AXI4__CPU__MEMPORT__RID,
    input  [127:0] AXI4__CPU__MEMPORT__RDATA,
    input  [1:0]   AXI4__CPU__MEMPORT__RRESP,
    input          AXI4__CPU__MEMPORT__RLAST,
    input          AXI4__CPU__SYSPORT__AWREADY,
    output         AXI4__CPU__SYSPORT__AWVALID,
    output [7:0]   AXI4__CPU__SYSPORT__AWID,
    output [35:0]  AXI4__CPU__SYSPORT__AWADDR,
    output [7:0]   AXI4__CPU__SYSPORT__AWLEN,
    output [2:0]   AXI4__CPU__SYSPORT__AWSIZE,
    output [1:0]   AXI4__CPU__SYSPORT__AWBURST,
    output         AXI4__CPU__SYSPORT__AWLOCK,
    output [3:0]   AXI4__CPU__SYSPORT__AWCACHE,
    output [2:0]   AXI4__CPU__SYSPORT__AWPROT,
    output [3:0]   AXI4__CPU__SYSPORT__AWQOS,
    input          AXI4__CPU__SYSPORT__WREADY,
    output         AXI4__CPU__SYSPORT__WVALID,
    output [127:0] AXI4__CPU__SYSPORT__WDATA,
    output [15:0]  AXI4__CPU__SYSPORT__WSTRB,
    output         AXI4__CPU__SYSPORT__WLAST,
    output         AXI4__CPU__SYSPORT__BREADY,
    input          AXI4__CPU__SYSPORT__BVALID,
    input  [7:0]   AXI4__CPU__SYSPORT__BID,
    input  [1:0]   AXI4__CPU__SYSPORT__BRESP,
    input          AXI4__CPU__SYSPORT__ARREADY,
    output         AXI4__CPU__SYSPORT__ARVALID,
    output [7:0]   AXI4__CPU__SYSPORT__ARID,
    output [35:0]  AXI4__CPU__SYSPORT__ARADDR,
    output [7:0]   AXI4__CPU__SYSPORT__ARLEN,
    output [2:0]   AXI4__CPU__SYSPORT__ARSIZE,
    output [1:0]   AXI4__CPU__SYSPORT__ARBURST,
    output         AXI4__CPU__SYSPORT__ARLOCK,
    output [3:0]   AXI4__CPU__SYSPORT__ARCACHE,
    output [2:0]   AXI4__CPU__SYSPORT__ARPROT,
    output [3:0]   AXI4__CPU__SYSPORT__ARQOS,
    output         AXI4__CPU__SYSPORT__RREADY,
    input          AXI4__CPU__SYSPORT__RVALID,
    input  [7:0]   AXI4__CPU__SYSPORT__RID,
    input  [127:0] AXI4__CPU__SYSPORT__RDATA,
    input  [1:0]   AXI4__CPU__SYSPORT__RRESP,
    input          AXI4__CPU__SYSPORT__RLAST,
    input          AXI4__CPU__PERIPORT__AWREADY,
    output         AXI4__CPU__PERIPORT__AWVALID,
    output         AXI4__CPU__PERIPORT__AWID,
    output [31:0]  AXI4__CPU__PERIPORT__AWADDR,
    output [7:0]   AXI4__CPU__PERIPORT__AWLEN,
    output [2:0]   AXI4__CPU__PERIPORT__AWSIZE,
    output [1:0]   AXI4__CPU__PERIPORT__AWBURST,
    output         AXI4__CPU__PERIPORT__AWLOCK,
    output [3:0]   AXI4__CPU__PERIPORT__AWCACHE,
    output [2:0]   AXI4__CPU__PERIPORT__AWPROT,
    output [3:0]   AXI4__CPU__PERIPORT__AWQOS,
    input          AXI4__CPU__PERIPORT__WREADY,
    output         AXI4__CPU__PERIPORT__WVALID,
    output [63:0]  AXI4__CPU__PERIPORT__WDATA,
    output [7:0]   AXI4__CPU__PERIPORT__WSTRB,
    output         AXI4__CPU__PERIPORT__WLAST,
    output         AXI4__CPU__PERIPORT__BREADY,
    input          AXI4__CPU__PERIPORT__BVALID,
    input          AXI4__CPU__PERIPORT__BID,
    input  [1:0]   AXI4__CPU__PERIPORT__BRESP,
    input          AXI4__CPU__PERIPORT__ARREADY,
    output         AXI4__CPU__PERIPORT__ARVALID,
    output         AXI4__CPU__PERIPORT__ARID,
    output [31:0]  AXI4__CPU__PERIPORT__ARADDR,
    output [7:0]   AXI4__CPU__PERIPORT__ARLEN,
    output [2:0]   AXI4__CPU__PERIPORT__ARSIZE,
    output [1:0]   AXI4__CPU__PERIPORT__ARBURST,
    output         AXI4__CPU__PERIPORT__ARLOCK,
    output [3:0]   AXI4__CPU__PERIPORT__ARCACHE,
    output [2:0]   AXI4__CPU__PERIPORT__ARPROT,
    output [3:0]   AXI4__CPU__PERIPORT__ARQOS,
    output         AXI4__CPU__PERIPORT__RREADY,
    input          AXI4__CPU__PERIPORT__RVALID,
    input          AXI4__CPU__PERIPORT__RID,
    input  [63:0]  AXI4__CPU__PERIPORT__RDATA,
    input  [1:0]   AXI4__CPU__PERIPORT__RRESP,
    input          AXI4__CPU__PERIPORT__RLAST,
    output         AXI4__CPU__FRONTPORT__AWREADY,
    input          AXI4__CPU__FRONTPORT__AWVALID,
    input  [10:0]  AXI4__CPU__FRONTPORT__AWID,
    input  [31:0]  AXI4__CPU__FRONTPORT__AWADDR,
    input  [7:0]   AXI4__CPU__FRONTPORT__AWLEN,
    input  [2:0]   AXI4__CPU__FRONTPORT__AWSIZE,
    input  [1:0]   AXI4__CPU__FRONTPORT__AWBURST,
    input          AXI4__CPU__FRONTPORT__AWLOCK,
    input  [3:0]   AXI4__CPU__FRONTPORT__AWCACHE,
    input  [2:0]   AXI4__CPU__FRONTPORT__AWPROT,
    input  [3:0]   AXI4__CPU__FRONTPORT__AWQOS,
    output         AXI4__CPU__FRONTPORT__WREADY,
    input          AXI4__CPU__FRONTPORT__WVALID,
    input  [63:0]  AXI4__CPU__FRONTPORT__WDATA,
    input  [7:0]   AXI4__CPU__FRONTPORT__WSTRB,
    input          AXI4__CPU__FRONTPORT__WLAST,
    input          AXI4__CPU__FRONTPORT__BREADY,
    output         AXI4__CPU__FRONTPORT__BVALID,
    output [10:0]  AXI4__CPU__FRONTPORT__BID,
    output [1:0]   AXI4__CPU__FRONTPORT__BRESP,
    output         AXI4__CPU__FRONTPORT__ARREADY,
    input          AXI4__CPU__FRONTPORT__ARVALID,
    input  [10:0]  AXI4__CPU__FRONTPORT__ARID,
    input  [31:0]  AXI4__CPU__FRONTPORT__ARADDR,
    input  [7:0]   AXI4__CPU__FRONTPORT__ARLEN,
    input  [2:0]   AXI4__CPU__FRONTPORT__ARSIZE,
    input  [1:0]   AXI4__CPU__FRONTPORT__ARBURST,
    input          AXI4__CPU__FRONTPORT__ARLOCK,
    input  [3:0]   AXI4__CPU__FRONTPORT__ARCACHE,
    input  [2:0]   AXI4__CPU__FRONTPORT__ARPROT,
    input  [3:0]   AXI4__CPU__FRONTPORT__ARQOS,
    input          AXI4__CPU__FRONTPORT__RREADY,
    output         AXI4__CPU__FRONTPORT__RVALID,
    output [10:0]  AXI4__CPU__FRONTPORT__RID,
    output [63:0]  AXI4__CPU__FRONTPORT__RDATA,
    output [1:0]   AXI4__CPU__FRONTPORT__RRESP,
    output         AXI4__CPU__FRONTPORT__RLAST,

    // configuration register
    input          SFR__suppress_strb_on_corrupt
);

wire         CPU_MEMPORT_AWREADY   ;
wire         CPU_MEMPORT_AWVALID   ;
wire [7:0]   CPU_MEMPORT_AWID      ;
wire [35:0]  CPU_MEMPORT_AWADDR    ;
wire [7:0]   CPU_MEMPORT_AWLEN     ;
wire [2:0]   CPU_MEMPORT_AWSIZE    ;
wire [1:0]   CPU_MEMPORT_AWBURST   ;
wire         CPU_MEMPORT_AWLOCK    ;
wire [3:0]   CPU_MEMPORT_AWCACHE   ;
wire [2:0]   CPU_MEMPORT_AWPROT    ;
wire [3:0]   CPU_MEMPORT_AWQOS     ;
wire         CPU_MEMPORT_WREADY    ;
wire         CPU_MEMPORT_WVALID    ;
wire [127:0] CPU_MEMPORT_WDATA     ;
wire [15:0]  CPU_MEMPORT_WSTRB     ;
wire         CPU_MEMPORT_WLAST     ;
wire         CPU_MEMPORT_BREADY    ;
wire         CPU_MEMPORT_BVALID    ;
wire [7:0]   CPU_MEMPORT_BID       ;
wire [1:0]   CPU_MEMPORT_BRESP     ;
wire         CPU_MEMPORT_ARREADY   ;
wire         CPU_MEMPORT_ARVALID   ;
wire [7:0]   CPU_MEMPORT_ARID      ;
wire [35:0]  CPU_MEMPORT_ARADDR    ;
wire [7:0]   CPU_MEMPORT_ARLEN     ;
wire [2:0]   CPU_MEMPORT_ARSIZE    ;
wire [1:0]   CPU_MEMPORT_ARBURST   ;
wire         CPU_MEMPORT_ARLOCK    ;
wire [3:0]   CPU_MEMPORT_ARCACHE   ;
wire [2:0]   CPU_MEMPORT_ARPROT    ;
wire [3:0]   CPU_MEMPORT_ARQOS     ;
wire         CPU_MEMPORT_RREADY    ;
wire         CPU_MEMPORT_RVALID    ;
wire [7:0]   CPU_MEMPORT_RID       ;
wire [127:0] CPU_MEMPORT_RDATA     ;
wire [1:0]   CPU_MEMPORT_RRESP     ;
wire         CPU_MEMPORT_RLAST     ;
wire         CPU_SYSPORT_AWREADY   ;
wire         CPU_SYSPORT_AWVALID   ;
wire [7:0]   CPU_SYSPORT_AWID      ;
wire [36:0]  CPU_SYSPORT_AWADDR    ;
wire [7:0]   CPU_SYSPORT_AWLEN     ;
wire [2:0]   CPU_SYSPORT_AWSIZE    ;
wire [1:0]   CPU_SYSPORT_AWBURST   ;
wire         CPU_SYSPORT_AWLOCK    ;
wire [3:0]   CPU_SYSPORT_AWCACHE   ;
wire [2:0]   CPU_SYSPORT_AWPROT    ;
wire [3:0]   CPU_SYSPORT_AWQOS     ;
wire         CPU_SYSPORT_WREADY    ;
wire         CPU_SYSPORT_WVALID    ;
wire [127:0] CPU_SYSPORT_WDATA     ;
wire [15:0]  CPU_SYSPORT_WSTRB     ;
wire         CPU_SYSPORT_WLAST     ;
wire         CPU_SYSPORT_BREADY    ;
wire         CPU_SYSPORT_BVALID    ;
wire [7:0]   CPU_SYSPORT_BID       ;
wire [1:0]   CPU_SYSPORT_BRESP     ;
wire         CPU_SYSPORT_ARREADY   ;
wire         CPU_SYSPORT_ARVALID   ;
wire [7:0]   CPU_SYSPORT_ARID      ;
wire [36:0]  CPU_SYSPORT_ARADDR    ;
wire [7:0]   CPU_SYSPORT_ARLEN     ;
wire [2:0]   CPU_SYSPORT_ARSIZE    ;
wire [1:0]   CPU_SYSPORT_ARBURST   ;
wire         CPU_SYSPORT_ARLOCK    ;
wire [3:0]   CPU_SYSPORT_ARCACHE   ;
wire [2:0]   CPU_SYSPORT_ARPROT    ;
wire [3:0]   CPU_SYSPORT_ARQOS     ;
wire         CPU_SYSPORT_RREADY    ;
wire         CPU_SYSPORT_RVALID    ;
wire [7:0]   CPU_SYSPORT_RID       ;
wire [127:0] CPU_SYSPORT_RDATA     ;
wire [1:0]   CPU_SYSPORT_RRESP     ;
wire         CPU_SYSPORT_RLAST     ;
wire         CPU_PERIPORT_AWREADY   ;
wire         CPU_PERIPORT_AWVALID   ;
wire         CPU_PERIPORT_AWID      ;
wire [31:0]  CPU_PERIPORT_AWADDR    ;
wire [7:0]   CPU_PERIPORT_AWLEN     ;
wire [2:0]   CPU_PERIPORT_AWSIZE    ;
wire [1:0]   CPU_PERIPORT_AWBURST   ;
wire         CPU_PERIPORT_AWLOCK    ;
wire [3:0]   CPU_PERIPORT_AWCACHE   ;
wire [2:0]   CPU_PERIPORT_AWPROT    ;
wire [3:0]   CPU_PERIPORT_AWQOS     ;
wire         CPU_PERIPORT_WREADY    ;
wire         CPU_PERIPORT_WVALID    ;
wire [63:0]  CPU_PERIPORT_WDATA     ;
wire [7:0]   CPU_PERIPORT_WSTRB     ;
wire         CPU_PERIPORT_WLAST     ;
wire         CPU_PERIPORT_BREADY    ;
wire         CPU_PERIPORT_BVALID    ;
wire         CPU_PERIPORT_BID       ;
wire [1:0]   CPU_PERIPORT_BRESP     ;
wire         CPU_PERIPORT_ARREADY   ;
wire         CPU_PERIPORT_ARVALID   ;
wire         CPU_PERIPORT_ARID      ;
wire [31:0]  CPU_PERIPORT_ARADDR    ;
wire [7:0]   CPU_PERIPORT_ARLEN     ;
wire [2:0]   CPU_PERIPORT_ARSIZE    ;
wire [1:0]   CPU_PERIPORT_ARBURST   ;
wire         CPU_PERIPORT_ARLOCK    ;
wire [3:0]   CPU_PERIPORT_ARCACHE   ;
wire [2:0]   CPU_PERIPORT_ARPROT    ;
wire [3:0]   CPU_PERIPORT_ARQOS     ;
wire         CPU_PERIPORT_RREADY    ;
wire         CPU_PERIPORT_RVALID    ;
wire         CPU_PERIPORT_RID       ;
wire [63:0]  CPU_PERIPORT_RDATA     ;
wire [1:0]   CPU_PERIPORT_RRESP     ;
wire         CPU_PERIPORT_RLAST     ;
wire [255:0] CPU_IRQ_GLB;
wire [15:0]  wAXI4__CPU__FRONTPORT__BID;
wire [15:0]  wAXI4__CPU__FRONTPORT__RID;

wire CFG__suppress_strb_on_corrupt_synced;
wire [15:0]  CPU_MEMPORT_WSTRBint;
wire AXI4__CPU__MEMPORT_wcorrupt;

// Synchronizers for Configuration Registers
SemiDataSynchronizer SYNC_cfg_suppress_strb_on_corrupt_0 (.CLK(CLK__CPU), .RSTN(RSTN__CPU),.DIN(SFR__suppress_strb_on_corrupt), .DOUT(CFG__suppress_strb_on_corrupt_synced));

// Supress WSTRB along corrupt signal
assign CPU_MEMPORT_WSTRB = CFG__suppress_strb_on_corrupt_synced ? (CPU_MEMPORT_WSTRBint && {16{AXI4__CPU__MEMPORT_wcorrupt}}) : CPU_MEMPORT_WSTRBint;

// CoreIPSubsystem Instance
reg [39:0] trace_0_extTimestampIn;
wire NC0,NC1,NC2,NC3;
RHEA__CoreIPSubsystem CPU_0 (
    .clock                                  (CLK__CPU),
    .reset                                  (~RSTN__CPU),
    .external_source_for_core_0_clock       (CLK__CPUCORE0),
    .external_source_for_core_0_reset       (~RSTN__CPUCORE0),
    .external_source_for_core_1_clock       (CLK__CPUCORE1),
    .external_source_for_core_1_reset       (~RSTN__CPUCORE1),
    .halt_from_tile_0                       (SFR__halt_from_tile_0),
    .halt_from_tile_1                       (SFR__halt_from_tile_1),
    .wfi_from_tile_0                        (SFR__wfi_from_tile_0),
    .wfi_from_tile_1                        (SFR__wfi_from_tile_1),
    .cease_from_tile_0                      (SFR__cease_from_tile_0),
    .cease_from_tile_1                      (SFR__cease_from_tile_1),
    .debug_from_tile_0                      (SFR__debug_from_tile_0),
    .debug_from_tile_1                      (SFR__debug_from_tile_1),
    .psd_test_clock_enable                  (SFR__psd_test_clock_enable),
    .resetctrl_hartIsInReset_0              (SFR__resetctrl_hartIsInReset_0),
    .resetctrl_hartIsInReset_1              (SFR__resetctrl_hartIsInReset_1),
    .debug_clock                            (CLK__DEBUG),
    .debug_reset                            (~RSTN__DEBUG),
    .debug_systemjtag_jtag_TCK              (IO__JTAG_TCK),
    .debug_systemjtag_jtag_TMS              (IO__JTAG_TMS),
    .debug_systemjtag_jtag_TDI              (IO__JTAG_TDI),
    .debug_systemjtag_jtag_TDO_data         (IO__JTAG_TDO),
    .debug_systemjtag_jtag_TDO_driven       (IO__JTAG_TDOEN),
    .debug_systemjtag_reset                 (IO__JTAG_TRST),
    .debug_systemjtag_mfr_id                (SFR__debug_systemjtag_mfr_id),
    .debug_systemjtag_part_number           (SFR__debug_systemjtag_part_number),
    .debug_systemjtag_version               (SFR__debug_systemjtag_version),
    .debug_ndreset                          (SFR__debug_ndreset),
    .debug_dmactive                         (SFR__debug_dmactive),
    .debug_dmactiveAck                      (SFR__debug_dmactiveAck),
    .reset_vector_0                         (RESET_VECTOR_0),
    .reset_vector_1                         (RESET_VECTOR_1),
    .nmi_0_rnmi                             (nmi_0_rnmi),
    .nmi_0_rnmi_interrupt_vector            (nmi_0_rnmi_interrupt_vector),
    .nmi_0_rnmi_exception_vector            (nmi_0_rnmi_exception_vector),
    .nmi_1_rnmi                             (nmi_1_rnmi),
    .nmi_1_rnmi_interrupt_vector            (nmi_1_rnmi_interrupt_vector),
    .nmi_1_rnmi_exception_vector            (nmi_1_rnmi_exception_vector),
    .rtc_toggle                             (SFR__rtc_toggle),
    .global_interrupts                      (CPU_IRQ_GLB),
    .local_interrupts_1                     (local_interrupts_1),
    .local_interrupts_0                     (local_interrupts_0),
    .mem_port_axi4_0_aw_ready               (CPU_MEMPORT_AWREADY),
    .mem_port_axi4_0_aw_valid               (CPU_MEMPORT_AWVALID),
    .mem_port_axi4_0_aw_bits_id             (CPU_MEMPORT_AWID),
    .mem_port_axi4_0_aw_bits_addr           (CPU_MEMPORT_AWADDR),
    .mem_port_axi4_0_aw_bits_len            (CPU_MEMPORT_AWLEN),
    .mem_port_axi4_0_aw_bits_size           (CPU_MEMPORT_AWSIZE),
    .mem_port_axi4_0_aw_bits_burst          (CPU_MEMPORT_AWBURST),
    .mem_port_axi4_0_aw_bits_lock           (CPU_MEMPORT_AWLOCK),
    .mem_port_axi4_0_aw_bits_cache          (CPU_MEMPORT_AWCACHE),
    .mem_port_axi4_0_aw_bits_prot           (CPU_MEMPORT_AWPROT),
    .mem_port_axi4_0_aw_bits_qos            (CPU_MEMPORT_AWQOS),
    .mem_port_axi4_0_w_ready                (CPU_MEMPORT_WREADY),
    .mem_port_axi4_0_w_valid                (CPU_MEMPORT_WVALID),
    .mem_port_axi4_0_w_bits_data            (CPU_MEMPORT_WDATA),
    .mem_port_axi4_0_w_bits_strb            (CPU_MEMPORT_WSTRBint),
    .mem_port_axi4_0_w_bits_last            (CPU_MEMPORT_WLAST),
    .mem_port_axi4_0_w_bits_user_corrupt    (AXI4__CPU__MEMPORT_wcorrupt),
    .mem_port_axi4_0_b_ready                (CPU_MEMPORT_BREADY),
    .mem_port_axi4_0_b_valid                (CPU_MEMPORT_BVALID),
    .mem_port_axi4_0_b_bits_id              (CPU_MEMPORT_BID),
    .mem_port_axi4_0_b_bits_resp            (CPU_MEMPORT_BRESP),
    .mem_port_axi4_0_ar_ready               (CPU_MEMPORT_ARREADY),
    .mem_port_axi4_0_ar_valid               (CPU_MEMPORT_ARVALID),
    .mem_port_axi4_0_ar_bits_id             (CPU_MEMPORT_ARID),
    .mem_port_axi4_0_ar_bits_addr           (CPU_MEMPORT_ARADDR),
    .mem_port_axi4_0_ar_bits_len            (CPU_MEMPORT_ARLEN),
    .mem_port_axi4_0_ar_bits_size           (CPU_MEMPORT_ARSIZE),
    .mem_port_axi4_0_ar_bits_burst          (CPU_MEMPORT_ARBURST),
    .mem_port_axi4_0_ar_bits_lock           (CPU_MEMPORT_ARLOCK),
    .mem_port_axi4_0_ar_bits_cache          (CPU_MEMPORT_ARCACHE),
    .mem_port_axi4_0_ar_bits_prot           (CPU_MEMPORT_ARPROT),
    .mem_port_axi4_0_ar_bits_qos            (CPU_MEMPORT_ARQOS),
    .mem_port_axi4_0_r_ready                (CPU_MEMPORT_RREADY),
    .mem_port_axi4_0_r_valid                (CPU_MEMPORT_RVALID),
    .mem_port_axi4_0_r_bits_id              (CPU_MEMPORT_RID),
    .mem_port_axi4_0_r_bits_data            (CPU_MEMPORT_RDATA),
    .mem_port_axi4_0_r_bits_resp            (CPU_MEMPORT_RRESP),
    .mem_port_axi4_0_r_bits_last            (CPU_MEMPORT_RLAST),
    .sys_port_axi4_0_aw_ready               (CPU_SYSPORT_AWREADY),
    .sys_port_axi4_0_aw_valid               (CPU_SYSPORT_AWVALID),
    .sys_port_axi4_0_aw_bits_id             (CPU_SYSPORT_AWID),
    .sys_port_axi4_0_aw_bits_addr           (CPU_SYSPORT_AWADDR),
    .sys_port_axi4_0_aw_bits_len            (CPU_SYSPORT_AWLEN),
    .sys_port_axi4_0_aw_bits_size           (CPU_SYSPORT_AWSIZE),
    .sys_port_axi4_0_aw_bits_burst          (CPU_SYSPORT_AWBURST),
    .sys_port_axi4_0_aw_bits_lock           (CPU_SYSPORT_AWLOCK),
    .sys_port_axi4_0_aw_bits_cache          (CPU_SYSPORT_AWCACHE),
    .sys_port_axi4_0_aw_bits_prot           (CPU_SYSPORT_AWPROT),
    .sys_port_axi4_0_aw_bits_qos            (CPU_SYSPORT_AWQOS),
    .sys_port_axi4_0_w_ready                (CPU_SYSPORT_WREADY),
    .sys_port_axi4_0_w_valid                (CPU_SYSPORT_WVALID),
    .sys_port_axi4_0_w_bits_data            (CPU_SYSPORT_WDATA),
    .sys_port_axi4_0_w_bits_strb            (CPU_SYSPORT_WSTRB),
    .sys_port_axi4_0_w_bits_last            (CPU_SYSPORT_WLAST),
    .sys_port_axi4_0_b_ready                (CPU_SYSPORT_BREADY),
    .sys_port_axi4_0_b_valid                (CPU_SYSPORT_BVALID),
    .sys_port_axi4_0_b_bits_id              (CPU_SYSPORT_BID),
    .sys_port_axi4_0_b_bits_resp            (CPU_SYSPORT_BRESP),
    .sys_port_axi4_0_ar_ready               (CPU_SYSPORT_ARREADY),
    .sys_port_axi4_0_ar_valid               (CPU_SYSPORT_ARVALID),
    .sys_port_axi4_0_ar_bits_id             (CPU_SYSPORT_ARID),
    .sys_port_axi4_0_ar_bits_addr           (CPU_SYSPORT_ARADDR),
    .sys_port_axi4_0_ar_bits_len            (CPU_SYSPORT_ARLEN),
    .sys_port_axi4_0_ar_bits_size           (CPU_SYSPORT_ARSIZE),
    .sys_port_axi4_0_ar_bits_burst          (CPU_SYSPORT_ARBURST),
    .sys_port_axi4_0_ar_bits_lock           (CPU_SYSPORT_ARLOCK),
    .sys_port_axi4_0_ar_bits_cache          (CPU_SYSPORT_ARCACHE),
    .sys_port_axi4_0_ar_bits_prot           (CPU_SYSPORT_ARPROT),
    .sys_port_axi4_0_ar_bits_qos            (CPU_SYSPORT_ARQOS),
    .sys_port_axi4_0_r_ready                (CPU_SYSPORT_RREADY),
    .sys_port_axi4_0_r_valid                (CPU_SYSPORT_RVALID),
    .sys_port_axi4_0_r_bits_id              (CPU_SYSPORT_RID),
    .sys_port_axi4_0_r_bits_data            (CPU_SYSPORT_RDATA),
    .sys_port_axi4_0_r_bits_resp            (CPU_SYSPORT_RRESP),
    .sys_port_axi4_0_r_bits_last            (CPU_SYSPORT_RLAST),
    .periph_port_axi4_0_aw_ready            (CPU_PERIPORT_AWREADY),
    .periph_port_axi4_0_aw_valid            (CPU_PERIPORT_AWVALID),
    .periph_port_axi4_0_aw_bits_id          (CPU_PERIPORT_AWID),
    .periph_port_axi4_0_aw_bits_addr        (CPU_PERIPORT_AWADDR),
    .periph_port_axi4_0_aw_bits_len         (CPU_PERIPORT_AWLEN),
    .periph_port_axi4_0_aw_bits_size        (CPU_PERIPORT_AWSIZE),
    .periph_port_axi4_0_aw_bits_burst       (CPU_PERIPORT_AWBURST),
    .periph_port_axi4_0_aw_bits_lock        (CPU_PERIPORT_AWLOCK),
    .periph_port_axi4_0_aw_bits_cache       (CPU_PERIPORT_AWCACHE),
    .periph_port_axi4_0_aw_bits_prot        (CPU_PERIPORT_AWPROT),
    .periph_port_axi4_0_aw_bits_qos         (CPU_PERIPORT_AWQOS),
    .periph_port_axi4_0_w_ready             (CPU_PERIPORT_WREADY),
    .periph_port_axi4_0_w_valid             (CPU_PERIPORT_WVALID),
    .periph_port_axi4_0_w_bits_data         (CPU_PERIPORT_WDATA),
    .periph_port_axi4_0_w_bits_strb         (CPU_PERIPORT_WSTRB),
    .periph_port_axi4_0_w_bits_last         (CPU_PERIPORT_WLAST),
    .periph_port_axi4_0_b_ready             (CPU_PERIPORT_BREADY),
    .periph_port_axi4_0_b_valid             (CPU_PERIPORT_BVALID),
    .periph_port_axi4_0_b_bits_id           (CPU_PERIPORT_BID),
    .periph_port_axi4_0_b_bits_resp         (CPU_PERIPORT_BRESP),
    .periph_port_axi4_0_ar_ready            (CPU_PERIPORT_ARREADY),
    .periph_port_axi4_0_ar_valid            (CPU_PERIPORT_ARVALID),
    .periph_port_axi4_0_ar_bits_id          (CPU_PERIPORT_ARID),
    .periph_port_axi4_0_ar_bits_addr        (CPU_PERIPORT_ARADDR),
    .periph_port_axi4_0_ar_bits_len         (CPU_PERIPORT_ARLEN),
    .periph_port_axi4_0_ar_bits_size        (CPU_PERIPORT_ARSIZE),
    .periph_port_axi4_0_ar_bits_burst       (CPU_PERIPORT_ARBURST),
    .periph_port_axi4_0_ar_bits_lock        (CPU_PERIPORT_ARLOCK),
    .periph_port_axi4_0_ar_bits_cache       (CPU_PERIPORT_ARCACHE),
    .periph_port_axi4_0_ar_bits_prot        (CPU_PERIPORT_ARPROT),
    .periph_port_axi4_0_ar_bits_qos         (CPU_PERIPORT_ARQOS),
    .periph_port_axi4_0_r_ready             (CPU_PERIPORT_RREADY),
    .periph_port_axi4_0_r_valid             (CPU_PERIPORT_RVALID),
    .periph_port_axi4_0_r_bits_id           (CPU_PERIPORT_RID),
    .periph_port_axi4_0_r_bits_data         (CPU_PERIPORT_RDATA),
    .periph_port_axi4_0_r_bits_resp         (CPU_PERIPORT_RRESP),
    .periph_port_axi4_0_r_bits_last         (CPU_PERIPORT_RLAST),
    .front_port_axi4_0_aw_ready             (AXI4__CPU__FRONTPORT__AWREADY), //  output
    .front_port_axi4_0_aw_valid             (AXI4__CPU__FRONTPORT__AWVALID), //  input
    .front_port_axi4_0_aw_bits_id           ({5'h0,AXI4__CPU__FRONTPORT__AWID}), //  input  [15:0]
    .front_port_axi4_0_aw_bits_addr         ({5'h0,AXI4__CPU__FRONTPORT__AWADDR}), //  input  [36:0]
    .front_port_axi4_0_aw_bits_len          (AXI4__CPU__FRONTPORT__AWLEN), //  input  [7:0]
    .front_port_axi4_0_aw_bits_size         (AXI4__CPU__FRONTPORT__AWSIZE), //  input  [2:0]
    .front_port_axi4_0_aw_bits_burst        (AXI4__CPU__FRONTPORT__AWBURST), //  input  [1:0]
    .front_port_axi4_0_aw_bits_lock         (AXI4__CPU__FRONTPORT__AWLOCK), //  input
    .front_port_axi4_0_aw_bits_cache        (AXI4__CPU__FRONTPORT__AWCACHE), //  input  [3:0]
    .front_port_axi4_0_aw_bits_prot         (AXI4__CPU__FRONTPORT__AWPROT), //  input  [2:0]
    .front_port_axi4_0_aw_bits_qos          (AXI4__CPU__FRONTPORT__AWQOS), //  input  [3:0]
    .front_port_axi4_0_w_ready              (AXI4__CPU__FRONTPORT__WREADY), //  output
    .front_port_axi4_0_w_valid              (AXI4__CPU__FRONTPORT__WVALID), //  input
    .front_port_axi4_0_w_bits_data          (AXI4__CPU__FRONTPORT__WDATA), //  input  [63:0]
    .front_port_axi4_0_w_bits_strb          (AXI4__CPU__FRONTPORT__WSTRB), //  input  [7:0]
    .front_port_axi4_0_w_bits_last          (AXI4__CPU__FRONTPORT__WLAST), //  input
    .front_port_axi4_0_b_ready              (AXI4__CPU__FRONTPORT__BREADY), //  input
    .front_port_axi4_0_b_valid              (AXI4__CPU__FRONTPORT__BVALID), //  output
    .front_port_axi4_0_b_bits_id            (wAXI4__CPU__FRONTPORT__BID), //  output [15:0]
    .front_port_axi4_0_b_bits_resp          (AXI4__CPU__FRONTPORT__BRESP), //  output [1:0]
    .front_port_axi4_0_ar_ready             (AXI4__CPU__FRONTPORT__ARREADY), //  output
    .front_port_axi4_0_ar_valid             (AXI4__CPU__FRONTPORT__ARVALID), //  input
    .front_port_axi4_0_ar_bits_id           ({5'h0,AXI4__CPU__FRONTPORT__ARID}), //  input  [15:0]
    .front_port_axi4_0_ar_bits_addr         ({5'h0,AXI4__CPU__FRONTPORT__ARADDR}), //  input  [36:0]
    .front_port_axi4_0_ar_bits_len          (AXI4__CPU__FRONTPORT__ARLEN), //  input  [7:0]
    .front_port_axi4_0_ar_bits_size         (AXI4__CPU__FRONTPORT__ARSIZE), //  input  [2:0]
    .front_port_axi4_0_ar_bits_burst        (AXI4__CPU__FRONTPORT__ARBURST), //  input  [1:0]
    .front_port_axi4_0_ar_bits_lock         (AXI4__CPU__FRONTPORT__ARLOCK), //  input
    .front_port_axi4_0_ar_bits_cache        (AXI4__CPU__FRONTPORT__ARCACHE), //  input  [3:0]
    .front_port_axi4_0_ar_bits_prot         (AXI4__CPU__FRONTPORT__ARPROT), //  input  [2:0]
    .front_port_axi4_0_ar_bits_qos          (AXI4__CPU__FRONTPORT__ARQOS), //  input  [3:0]
    .front_port_axi4_0_r_ready              (AXI4__CPU__FRONTPORT__RREADY), //  input
    .front_port_axi4_0_r_valid              (AXI4__CPU__FRONTPORT__RVALID), //  output
    .front_port_axi4_0_r_bits_id            (wAXI4__CPU__FRONTPORT__RID), //  output [15:0]
    .front_port_axi4_0_r_bits_data          (AXI4__CPU__FRONTPORT__RDATA), //  output [63:0]
    .front_port_axi4_0_r_bits_resp          (AXI4__CPU__FRONTPORT__RRESP), //  output [1:0]
    .front_port_axi4_0_r_bits_last          (AXI4__CPU__FRONTPORT__RLAST), //  output
    .trace_com_clock                        (CLK__TRACE_COM),
    .trace_com_reset                        (~RSTN__TRACE_COM),
    .trace_com_pib_tref                     (IO__TRACE_TREF ),
    .trace_com_pib_tdata                    (IO__TRACE_TDATA),
    .trace_0_clock                          (CLK__TRACE_0),
    .trace_0_reset                          (~RSTN__TRACE_0),
    .trace_0_extTimestampIn                 (trace_0_extTimestampIn),
    .trace_1_clock                          (CLK__TRACE_1),
    .trace_1_reset                          (~RSTN__TRACE_1)
);
assign AXI4__CPU__FRONTPORT__BID = wAXI4__CPU__FRONTPORT__BID[10:0];
assign AXI4__CPU__FRONTPORT__RID = wAXI4__CPU__FRONTPORT__RID[10:0];
//assign CPU_SYSPORT_AWADDR[35] = 1'b0;
//assign CPU_SYSPORT_ARADDR[35] = 1'b0;
//assign CPU_PERIPORT_AWADDR[31:30] = 2'b0;
//assign CPU_PERIPORT_ARADDR[31:30] = 2'b0;
always @(posedge CLK__TS or negedge RSTN__TS) begin
//always @(posedge CLK__CPU or negedge RSTN__CPU) begin
    //if (~RSTN__CPU) begin
    if (~RSTN__TS) begin
        trace_0_extTimestampIn <= 0;
    end
    else begin
        trace_0_extTimestampIn <= trace_0_extTimestampIn+1;
    end
end


`ifdef FPGA
assign CPU_IRQ_GLB = IRQ_GLB; // below $bits generates VRFC 10-3736 Error on VIVADO
`else
genvar g;
generate
    for(g=0;g<$bits(IRQ_GLB);g=g+1) begin
        interrupt_vslave_binder #( .IRQ_NUMBER(g) ) iVS_CPUIRQ (
            .CLK__I     ( CLK__CPU   ), 
            .RSTN__I    ( RSTN__CPU   ), 
            .IRQ__I__IN ( IRQ_GLB    [g]),
            .IRQ__I__OUT( CPU_IRQ_GLB[g])
        );        
    end
endgenerate
`endif

//axi_vmaster_binder #( .name("SemiCPU_MBUS"), .ID(8), .A(36), .D(128), .MAX_BURST(16), .MAX_OUTSTANDING(60) ) uVM_CPUMBUS(
axi_vmaster_binder #( .name("SemiCPU_MBUS"), .ID(8), .A(36), .D(128), .MAX_BURST(16), .MAX_OUTSTANDING(16) ) uVM_CPUMBUS(
    .CLK__BUS (CLK__CPU),
    .RSTN__BUS(RSTN__CPU), 
    .AXI4__BUS__SLV__AWID    (CPU_MEMPORT_AWID    ),
    .AXI4__BUS__SLV__AWADDR  (CPU_MEMPORT_AWADDR  ),
    .AXI4__BUS__SLV__AWLEN   (CPU_MEMPORT_AWLEN   ),
    .AXI4__BUS__SLV__AWSIZE  (CPU_MEMPORT_AWSIZE  ),
    .AXI4__BUS__SLV__AWBURST (CPU_MEMPORT_AWBURST ),
    .AXI4__BUS__SLV__AWLOCK  (CPU_MEMPORT_AWLOCK  ),
    .AXI4__BUS__SLV__AWCACHE (CPU_MEMPORT_AWCACHE ),
    .AXI4__BUS__SLV__AWPROT  (CPU_MEMPORT_AWPROT  ),
    .AXI4__BUS__SLV__AWVALID (CPU_MEMPORT_AWVALID ),
    .AXI4__BUS__SLV__AWREADY (CPU_MEMPORT_AWREADY ),
    .AXI4__BUS__SLV__WDATA   (CPU_MEMPORT_WDATA   ),
    .AXI4__BUS__SLV__WSTRB   (CPU_MEMPORT_WSTRB   ),
    .AXI4__BUS__SLV__WLAST   (CPU_MEMPORT_WLAST   ),
    .AXI4__BUS__SLV__WVALID  (CPU_MEMPORT_WVALID  ),
    .AXI4__BUS__SLV__WREADY  (CPU_MEMPORT_WREADY  ),
    .AXI4__BUS__SLV__BID     (CPU_MEMPORT_BID     ),
    .AXI4__BUS__SLV__BVALID  (CPU_MEMPORT_BVALID  ),
    .AXI4__BUS__SLV__BRESP   (CPU_MEMPORT_BRESP   ),
    .AXI4__BUS__SLV__BREADY  (CPU_MEMPORT_BREADY  ),
    .AXI4__BUS__SLV__ARID    (CPU_MEMPORT_ARID    ),
    .AXI4__BUS__SLV__ARADDR  (CPU_MEMPORT_ARADDR  ),
    .AXI4__BUS__SLV__ARLEN   (CPU_MEMPORT_ARLEN   ),
    .AXI4__BUS__SLV__ARSIZE  (CPU_MEMPORT_ARSIZE  ),
    .AXI4__BUS__SLV__ARBURST (CPU_MEMPORT_ARBURST ),
    .AXI4__BUS__SLV__ARLOCK  (CPU_MEMPORT_ARLOCK  ),
    .AXI4__BUS__SLV__ARCACHE (CPU_MEMPORT_ARCACHE ),
    .AXI4__BUS__SLV__ARPROT  (CPU_MEMPORT_ARPROT  ),
    .AXI4__BUS__SLV__ARVALID (CPU_MEMPORT_ARVALID ),
    .AXI4__BUS__SLV__ARREADY (CPU_MEMPORT_ARREADY ),
    .AXI4__BUS__SLV__RID     (CPU_MEMPORT_RID     ),
    .AXI4__BUS__SLV__RDATA   (CPU_MEMPORT_RDATA   ),
    .AXI4__BUS__SLV__RRESP   (CPU_MEMPORT_RRESP   ),
    .AXI4__BUS__SLV__RLAST   (CPU_MEMPORT_RLAST   ),
    .AXI4__BUS__SLV__RVALID  (CPU_MEMPORT_RVALID  ),
    .AXI4__BUS__SLV__RREADY  (CPU_MEMPORT_RREADY  ),
    .AXI4__BUS__MST__AWID    (AXI4__CPU__MEMPORT__AWID    ), 
    .AXI4__BUS__MST__AWADDR  (AXI4__CPU__MEMPORT__AWADDR  ), 
    .AXI4__BUS__MST__AWLEN   (AXI4__CPU__MEMPORT__AWLEN   ), 
    .AXI4__BUS__MST__AWSIZE  (AXI4__CPU__MEMPORT__AWSIZE  ), 
    .AXI4__BUS__MST__AWBURST (AXI4__CPU__MEMPORT__AWBURST ), 
    .AXI4__BUS__MST__AWLOCK  (AXI4__CPU__MEMPORT__AWLOCK  ), 
    .AXI4__BUS__MST__AWCACHE (AXI4__CPU__MEMPORT__AWCACHE ), 
    .AXI4__BUS__MST__AWPROT  (AXI4__CPU__MEMPORT__AWPROT  ), 
    .AXI4__BUS__MST__AWVALID (AXI4__CPU__MEMPORT__AWVALID ), 
    .AXI4__BUS__MST__AWREADY (AXI4__CPU__MEMPORT__AWREADY ), 
    .AXI4__BUS__MST__WDATA   (AXI4__CPU__MEMPORT__WDATA   ), 
    .AXI4__BUS__MST__WSTRB   (AXI4__CPU__MEMPORT__WSTRB   ), 
    .AXI4__BUS__MST__WLAST   (AXI4__CPU__MEMPORT__WLAST   ), 
    .AXI4__BUS__MST__WVALID  (AXI4__CPU__MEMPORT__WVALID  ), 
    .AXI4__BUS__MST__WREADY  (AXI4__CPU__MEMPORT__WREADY  ), 
    .AXI4__BUS__MST__BID     (AXI4__CPU__MEMPORT__BID     ), 
    .AXI4__BUS__MST__BVALID  (AXI4__CPU__MEMPORT__BVALID  ), 
    .AXI4__BUS__MST__BRESP   (AXI4__CPU__MEMPORT__BRESP   ), 
    .AXI4__BUS__MST__BREADY  (AXI4__CPU__MEMPORT__BREADY  ), 
    .AXI4__BUS__MST__ARID    (AXI4__CPU__MEMPORT__ARID    ), 
    .AXI4__BUS__MST__ARADDR  (AXI4__CPU__MEMPORT__ARADDR  ), 
    .AXI4__BUS__MST__ARLEN   (AXI4__CPU__MEMPORT__ARLEN   ), 
    .AXI4__BUS__MST__ARSIZE  (AXI4__CPU__MEMPORT__ARSIZE  ), 
    .AXI4__BUS__MST__ARBURST (AXI4__CPU__MEMPORT__ARBURST ), 
    .AXI4__BUS__MST__ARLOCK  (AXI4__CPU__MEMPORT__ARLOCK  ), 
    .AXI4__BUS__MST__ARCACHE (AXI4__CPU__MEMPORT__ARCACHE ), 
    .AXI4__BUS__MST__ARPROT  (AXI4__CPU__MEMPORT__ARPROT  ), 
    .AXI4__BUS__MST__ARVALID (AXI4__CPU__MEMPORT__ARVALID ), 
    .AXI4__BUS__MST__ARREADY (AXI4__CPU__MEMPORT__ARREADY ), 
    .AXI4__BUS__MST__RID     (AXI4__CPU__MEMPORT__RID     ), 
    .AXI4__BUS__MST__RDATA   (AXI4__CPU__MEMPORT__RDATA   ), 
    .AXI4__BUS__MST__RRESP   (AXI4__CPU__MEMPORT__RRESP   ), 
    .AXI4__BUS__MST__RLAST   (AXI4__CPU__MEMPORT__RLAST   ), 
    .AXI4__BUS__MST__RVALID  (AXI4__CPU__MEMPORT__RVALID  ), 
    .AXI4__BUS__MST__RREADY  (AXI4__CPU__MEMPORT__RREADY  ) 
);
assign AXI4__CPU__MEMPORT__AWQOS = CPU_MEMPORT_AWQOS;
assign AXI4__CPU__MEMPORT__ARQOS = CPU_MEMPORT_ARQOS;

axi_vmaster_binder #( .name("SemiCPU_SBUS"), .ID(8), .A(36), .D(128), .MAX_BURST(16), .MAX_OUTSTANDING(16) ) uVM_CPUSBUS(
    .CLK__BUS (CLK__CPU),
    .RSTN__BUS(RSTN__CPU), 
    .AXI4__BUS__SLV__AWID    (CPU_SYSPORT_AWID    ),
    .AXI4__BUS__SLV__AWADDR  (CPU_SYSPORT_AWADDR[35:0]  ),
    .AXI4__BUS__SLV__AWLEN   (CPU_SYSPORT_AWLEN   ),
    .AXI4__BUS__SLV__AWSIZE  (CPU_SYSPORT_AWSIZE  ),
    .AXI4__BUS__SLV__AWBURST (CPU_SYSPORT_AWBURST ),
    .AXI4__BUS__SLV__AWLOCK  (CPU_SYSPORT_AWLOCK  ),
    .AXI4__BUS__SLV__AWCACHE (CPU_SYSPORT_AWCACHE ),
    .AXI4__BUS__SLV__AWPROT  (CPU_SYSPORT_AWPROT  ),
    .AXI4__BUS__SLV__AWVALID (CPU_SYSPORT_AWVALID ),
    .AXI4__BUS__SLV__AWREADY (CPU_SYSPORT_AWREADY ),
    .AXI4__BUS__SLV__WDATA   (CPU_SYSPORT_WDATA   ),
    .AXI4__BUS__SLV__WSTRB   (CPU_SYSPORT_WSTRB   ),
    .AXI4__BUS__SLV__WLAST   (CPU_SYSPORT_WLAST   ),
    .AXI4__BUS__SLV__WVALID  (CPU_SYSPORT_WVALID  ),
    .AXI4__BUS__SLV__WREADY  (CPU_SYSPORT_WREADY  ),
    .AXI4__BUS__SLV__BID     (CPU_SYSPORT_BID     ),
    .AXI4__BUS__SLV__BVALID  (CPU_SYSPORT_BVALID  ),
    .AXI4__BUS__SLV__BRESP   (CPU_SYSPORT_BRESP   ),
    .AXI4__BUS__SLV__BREADY  (CPU_SYSPORT_BREADY  ),
    .AXI4__BUS__SLV__ARID    (CPU_SYSPORT_ARID    ),
    .AXI4__BUS__SLV__ARADDR  (CPU_SYSPORT_ARADDR[35:0]  ),
    .AXI4__BUS__SLV__ARLEN   (CPU_SYSPORT_ARLEN   ),
    .AXI4__BUS__SLV__ARSIZE  (CPU_SYSPORT_ARSIZE  ),
    .AXI4__BUS__SLV__ARBURST (CPU_SYSPORT_ARBURST ),
    .AXI4__BUS__SLV__ARLOCK  (CPU_SYSPORT_ARLOCK  ),
    .AXI4__BUS__SLV__ARCACHE (CPU_SYSPORT_ARCACHE ),
    .AXI4__BUS__SLV__ARPROT  (CPU_SYSPORT_ARPROT  ),
    .AXI4__BUS__SLV__ARVALID (CPU_SYSPORT_ARVALID ),
    .AXI4__BUS__SLV__ARREADY (CPU_SYSPORT_ARREADY ),
    .AXI4__BUS__SLV__RID     (CPU_SYSPORT_RID     ),
    .AXI4__BUS__SLV__RDATA   (CPU_SYSPORT_RDATA   ),
    .AXI4__BUS__SLV__RRESP   (CPU_SYSPORT_RRESP   ),
    .AXI4__BUS__SLV__RLAST   (CPU_SYSPORT_RLAST   ),
    .AXI4__BUS__SLV__RVALID  (CPU_SYSPORT_RVALID  ),
    .AXI4__BUS__SLV__RREADY  (CPU_SYSPORT_RREADY  ),
    .AXI4__BUS__MST__AWID    (AXI4__CPU__SYSPORT__AWID    ), 
    .AXI4__BUS__MST__AWADDR  (AXI4__CPU__SYSPORT__AWADDR  ), 
    .AXI4__BUS__MST__AWLEN   (AXI4__CPU__SYSPORT__AWLEN   ), 
    .AXI4__BUS__MST__AWSIZE  (AXI4__CPU__SYSPORT__AWSIZE  ), 
    .AXI4__BUS__MST__AWBURST (AXI4__CPU__SYSPORT__AWBURST ), 
    .AXI4__BUS__MST__AWLOCK  (AXI4__CPU__SYSPORT__AWLOCK  ), 
    .AXI4__BUS__MST__AWCACHE (AXI4__CPU__SYSPORT__AWCACHE ), 
    .AXI4__BUS__MST__AWPROT  (AXI4__CPU__SYSPORT__AWPROT  ), 
    .AXI4__BUS__MST__AWVALID (AXI4__CPU__SYSPORT__AWVALID ), 
    .AXI4__BUS__MST__AWREADY (AXI4__CPU__SYSPORT__AWREADY ), 
    .AXI4__BUS__MST__WDATA   (AXI4__CPU__SYSPORT__WDATA   ), 
    .AXI4__BUS__MST__WSTRB   (AXI4__CPU__SYSPORT__WSTRB   ), 
    .AXI4__BUS__MST__WLAST   (AXI4__CPU__SYSPORT__WLAST   ), 
    .AXI4__BUS__MST__WVALID  (AXI4__CPU__SYSPORT__WVALID  ), 
    .AXI4__BUS__MST__WREADY  (AXI4__CPU__SYSPORT__WREADY  ), 
    .AXI4__BUS__MST__BID     (AXI4__CPU__SYSPORT__BID     ), 
    .AXI4__BUS__MST__BVALID  (AXI4__CPU__SYSPORT__BVALID  ), 
    .AXI4__BUS__MST__BRESP   (AXI4__CPU__SYSPORT__BRESP   ), 
    .AXI4__BUS__MST__BREADY  (AXI4__CPU__SYSPORT__BREADY  ), 
    .AXI4__BUS__MST__ARID    (AXI4__CPU__SYSPORT__ARID    ), 
    .AXI4__BUS__MST__ARADDR  (AXI4__CPU__SYSPORT__ARADDR  ), 
    .AXI4__BUS__MST__ARLEN   (AXI4__CPU__SYSPORT__ARLEN   ), 
    .AXI4__BUS__MST__ARSIZE  (AXI4__CPU__SYSPORT__ARSIZE  ), 
    .AXI4__BUS__MST__ARBURST (AXI4__CPU__SYSPORT__ARBURST ), 
    .AXI4__BUS__MST__ARLOCK  (AXI4__CPU__SYSPORT__ARLOCK  ), 
    .AXI4__BUS__MST__ARCACHE (AXI4__CPU__SYSPORT__ARCACHE ), 
    .AXI4__BUS__MST__ARPROT  (AXI4__CPU__SYSPORT__ARPROT  ), 
    .AXI4__BUS__MST__ARVALID (AXI4__CPU__SYSPORT__ARVALID ), 
    .AXI4__BUS__MST__ARREADY (AXI4__CPU__SYSPORT__ARREADY ), 
    .AXI4__BUS__MST__RID     (AXI4__CPU__SYSPORT__RID     ), 
    .AXI4__BUS__MST__RDATA   (AXI4__CPU__SYSPORT__RDATA   ), 
    .AXI4__BUS__MST__RRESP   (AXI4__CPU__SYSPORT__RRESP   ), 
    .AXI4__BUS__MST__RLAST   (AXI4__CPU__SYSPORT__RLAST   ), 
    .AXI4__BUS__MST__RVALID  (AXI4__CPU__SYSPORT__RVALID  ), 
    .AXI4__BUS__MST__RREADY  (AXI4__CPU__SYSPORT__RREADY  ) 
);
assign AXI4__CPU__SYSPORT__AWQOS = CPU_SYSPORT_AWQOS;
assign AXI4__CPU__SYSPORT__ARQOS = CPU_SYSPORT_ARQOS;

axi_vmaster_binder #( .name("SemiCPU_PBUS"), .ID(1), .A(32), .D(64), .MAX_BURST(1), .MAX_OUTSTANDING(8) ) uVM_CPUPBUS(
//axi_vmaster_binder #( .name("SemiCPU_PBUS"), .ID(1), .A(32), .D(64), .MAX_BURST(16), .MAX_OUTSTANDING(16) ) uVM_CPUPBUS(
    .CLK__BUS (CLK__CPU),
    .RSTN__BUS(RSTN__CPU), 
    .AXI4__BUS__SLV__AWID    (CPU_PERIPORT_AWID    ),
    .AXI4__BUS__SLV__AWADDR  (CPU_PERIPORT_AWADDR  ),
    .AXI4__BUS__SLV__AWLEN   (CPU_PERIPORT_AWLEN   ),
    .AXI4__BUS__SLV__AWSIZE  (CPU_PERIPORT_AWSIZE  ),
    .AXI4__BUS__SLV__AWBURST (CPU_PERIPORT_AWBURST ),
    .AXI4__BUS__SLV__AWLOCK  (CPU_PERIPORT_AWLOCK  ),
    .AXI4__BUS__SLV__AWCACHE (CPU_PERIPORT_AWCACHE ),
    .AXI4__BUS__SLV__AWPROT  (CPU_PERIPORT_AWPROT  ),
    .AXI4__BUS__SLV__AWVALID (CPU_PERIPORT_AWVALID ),
    .AXI4__BUS__SLV__AWREADY (CPU_PERIPORT_AWREADY ),
    .AXI4__BUS__SLV__WDATA   (CPU_PERIPORT_WDATA   ),
    .AXI4__BUS__SLV__WSTRB   (CPU_PERIPORT_WSTRB   ),
    .AXI4__BUS__SLV__WLAST   (CPU_PERIPORT_WLAST   ),
    .AXI4__BUS__SLV__WVALID  (CPU_PERIPORT_WVALID  ),
    .AXI4__BUS__SLV__WREADY  (CPU_PERIPORT_WREADY  ),
    .AXI4__BUS__SLV__BID     (CPU_PERIPORT_BID     ),
    .AXI4__BUS__SLV__BVALID  (CPU_PERIPORT_BVALID  ),
    .AXI4__BUS__SLV__BRESP   (CPU_PERIPORT_BRESP   ),
    .AXI4__BUS__SLV__BREADY  (CPU_PERIPORT_BREADY  ),
    .AXI4__BUS__SLV__ARID    (CPU_PERIPORT_ARID    ),
    .AXI4__BUS__SLV__ARADDR  (CPU_PERIPORT_ARADDR  ),
    .AXI4__BUS__SLV__ARLEN   (CPU_PERIPORT_ARLEN   ),
    .AXI4__BUS__SLV__ARSIZE  (CPU_PERIPORT_ARSIZE  ),
    .AXI4__BUS__SLV__ARBURST (CPU_PERIPORT_ARBURST ),
    .AXI4__BUS__SLV__ARLOCK  (CPU_PERIPORT_ARLOCK  ),
    .AXI4__BUS__SLV__ARCACHE (CPU_PERIPORT_ARCACHE ),
    .AXI4__BUS__SLV__ARPROT  (CPU_PERIPORT_ARPROT  ),
    .AXI4__BUS__SLV__ARVALID (CPU_PERIPORT_ARVALID ),
    .AXI4__BUS__SLV__ARREADY (CPU_PERIPORT_ARREADY ),
    .AXI4__BUS__SLV__RID     (CPU_PERIPORT_RID     ),
    .AXI4__BUS__SLV__RDATA   (CPU_PERIPORT_RDATA   ),
    .AXI4__BUS__SLV__RRESP   (CPU_PERIPORT_RRESP   ),
    .AXI4__BUS__SLV__RLAST   (CPU_PERIPORT_RLAST   ),
    .AXI4__BUS__SLV__RVALID  (CPU_PERIPORT_RVALID  ),
    .AXI4__BUS__SLV__RREADY  (CPU_PERIPORT_RREADY  ),
    .AXI4__BUS__MST__AWID    (AXI4__CPU__PERIPORT__AWID    ), 
    .AXI4__BUS__MST__AWADDR  (AXI4__CPU__PERIPORT__AWADDR  ), 
    .AXI4__BUS__MST__AWLEN   (AXI4__CPU__PERIPORT__AWLEN   ), 
    .AXI4__BUS__MST__AWSIZE  (AXI4__CPU__PERIPORT__AWSIZE  ), 
    .AXI4__BUS__MST__AWBURST (AXI4__CPU__PERIPORT__AWBURST ), 
    .AXI4__BUS__MST__AWLOCK  (AXI4__CPU__PERIPORT__AWLOCK  ), 
    .AXI4__BUS__MST__AWCACHE (AXI4__CPU__PERIPORT__AWCACHE ), 
    .AXI4__BUS__MST__AWPROT  (AXI4__CPU__PERIPORT__AWPROT  ), 
    .AXI4__BUS__MST__AWVALID (AXI4__CPU__PERIPORT__AWVALID ), 
    .AXI4__BUS__MST__AWREADY (AXI4__CPU__PERIPORT__AWREADY ), 
    .AXI4__BUS__MST__WDATA   (AXI4__CPU__PERIPORT__WDATA   ), 
    .AXI4__BUS__MST__WSTRB   (AXI4__CPU__PERIPORT__WSTRB   ), 
    .AXI4__BUS__MST__WLAST   (AXI4__CPU__PERIPORT__WLAST   ), 
    .AXI4__BUS__MST__WVALID  (AXI4__CPU__PERIPORT__WVALID  ), 
    .AXI4__BUS__MST__WREADY  (AXI4__CPU__PERIPORT__WREADY  ), 
    .AXI4__BUS__MST__BID     (AXI4__CPU__PERIPORT__BID     ), 
    .AXI4__BUS__MST__BVALID  (AXI4__CPU__PERIPORT__BVALID  ), 
    .AXI4__BUS__MST__BRESP   (AXI4__CPU__PERIPORT__BRESP   ), 
    .AXI4__BUS__MST__BREADY  (AXI4__CPU__PERIPORT__BREADY  ), 
    .AXI4__BUS__MST__ARID    (AXI4__CPU__PERIPORT__ARID    ), 
    .AXI4__BUS__MST__ARADDR  (AXI4__CPU__PERIPORT__ARADDR  ), 
    .AXI4__BUS__MST__ARLEN   (AXI4__CPU__PERIPORT__ARLEN   ), 
    .AXI4__BUS__MST__ARSIZE  (AXI4__CPU__PERIPORT__ARSIZE  ), 
    .AXI4__BUS__MST__ARBURST (AXI4__CPU__PERIPORT__ARBURST ), 
    .AXI4__BUS__MST__ARLOCK  (AXI4__CPU__PERIPORT__ARLOCK  ), 
    .AXI4__BUS__MST__ARCACHE (AXI4__CPU__PERIPORT__ARCACHE ), 
    .AXI4__BUS__MST__ARPROT  (AXI4__CPU__PERIPORT__ARPROT  ), 
    .AXI4__BUS__MST__ARVALID (AXI4__CPU__PERIPORT__ARVALID ), 
    .AXI4__BUS__MST__ARREADY (AXI4__CPU__PERIPORT__ARREADY ), 
    .AXI4__BUS__MST__RID     (AXI4__CPU__PERIPORT__RID     ), 
    .AXI4__BUS__MST__RDATA   (AXI4__CPU__PERIPORT__RDATA   ), 
    .AXI4__BUS__MST__RRESP   (AXI4__CPU__PERIPORT__RRESP   ), 
    .AXI4__BUS__MST__RLAST   (AXI4__CPU__PERIPORT__RLAST   ), 
    .AXI4__BUS__MST__RVALID  (AXI4__CPU__PERIPORT__RVALID  ), 
    .AXI4__BUS__MST__RREADY  (AXI4__CPU__PERIPORT__RREADY  ) 
);
assign AXI4__CPU__PERIPORT__AWQOS = CPU_PERIPORT_AWQOS;
assign AXI4__CPU__PERIPORT__ARQOS = CPU_PERIPORT_ARQOS;

`ifndef SYNTHESIS
`ifndef FPGA_IMPLEMENT
reg [31:0] peri_arot;
reg [31:0] peri_awot;
initial begin
    peri_arot = 0;
    peri_awot = 0;
end
always @(posedge CLK__CPU or negedge RSTN__CPU) begin
    if (~RSTN__CPU) begin
        peri_arot <= 0;
    end
    else if (AXI4__CPU__PERIPORT__ARVALID&AXI4__CPU__PERIPORT__ARREADY) begin
        if (AXI4__CPU__PERIPORT__RVALID&AXI4__CPU__PERIPORT__RLAST&AXI4__CPU__PERIPORT__RREADY) begin
            peri_arot <= peri_arot;
        end
        else begin
            peri_arot <= peri_arot+1;
        end
    end
    else if (AXI4__CPU__PERIPORT__RVALID&AXI4__CPU__PERIPORT__RLAST&AXI4__CPU__PERIPORT__RREADY) begin
        peri_arot <= peri_arot-1;
    end
end
always @(posedge CLK__CPU or negedge RSTN__CPU) begin
    if (~RSTN__CPU) begin
        peri_awot <= 0;
    end
    else if (AXI4__CPU__PERIPORT__AWVALID&AXI4__CPU__PERIPORT__AWREADY) begin
        if (AXI4__CPU__PERIPORT__BVALID&AXI4__CPU__PERIPORT__BREADY) begin
            peri_awot <= peri_awot;
        end
        else begin
            peri_awot <= peri_awot+1;
        end
    end
    else if (AXI4__CPU__PERIPORT__BVALID&AXI4__CPU__PERIPORT__BREADY) begin
        peri_awot <= peri_awot-1;
    end
end
`endif
`endif

endmodule
// vim:tabstop=4:shiftwidth=4
