//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
interface SiFive_Insight_hart_0_CSRFile(
);

  // Hardware thread ID
  logic [1:0] hart_id;

  // Machine status register
  SiFive_Insight_MStatus mstatus();

  // ISA and extensions
  logic [63:0] misa;

  // Machine exception delegation register
  logic [63:0] medeleg;

  // Machine interrupt delegation register
  logic [63:0] mideleg;

  // Machine interrupt-enable register
  logic [63:0] mie;

  // Machine trap-handler base address
  logic [35:0] mtvec;

  // Scratch register for machine trap handlers
  logic [63:0] mscratch;

  // Machine exception program counter
  logic [39:0] mepc;

  // Machine trap cause
  logic [63:0] mcause;

  // Machine bad address or instruction
  logic [39:0] mtval;

  // Machine interrupt pending
  logic [15:0] mip;

  // Floating-Point Control and Status Register (frm + fflags)
  logic [9:0] fcsr;

  // Debug control and status register
  SiFive_Insight_dcsr dcsr();

  // Debug PC
  logic [39:0] dpc;

  // Cycle counter for RDCYCLE instruction
  logic [63:0] cycle;

  // Instruction-retired counter for RDINSTRET instruction
  logic [63:0] instret;

  // Machine counter enable
  logic [31:0] mcounteren;

  SiFive_Insight_hart_0_Counter hpmcounters[2]();

  // Supervisor trap-handler base address
  logic [38:0] stvec;

  // Scratch register for machine trap handlers
  logic [63:0] sscratch;

  // Supervisor exception program counter
  logic [39:0] sepc;

  // Supervisor trap cause
  logic [63:0] scause;

  // Supervisor bad address or instruction
  logic [39:0] stval;
endinterface
