//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLFIFOFixer_2(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_a_ready,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [3:0]  auto_in_a_bits_size,
  input  [4:0]  auto_in_a_bits_source,
  input  [36:0] auto_in_a_bits_address,
  input         auto_in_a_bits_user_amba_prot_bufferable,
  input         auto_in_a_bits_user_amba_prot_modifiable,
  input         auto_in_a_bits_user_amba_prot_readalloc,
  input         auto_in_a_bits_user_amba_prot_writealloc,
  input         auto_in_a_bits_user_amba_prot_privileged,
  input         auto_in_a_bits_user_amba_prot_secure,
  input         auto_in_a_bits_user_amba_prot_fetch,
  input  [7:0]  auto_in_a_bits_mask,
  input  [63:0] auto_in_a_bits_data,
  input         auto_in_d_ready,
  output        auto_in_d_valid,
  output [2:0]  auto_in_d_bits_opcode,
  output [3:0]  auto_in_d_bits_size,
  output [4:0]  auto_in_d_bits_source,
  output        auto_in_d_bits_denied,
  output [63:0] auto_in_d_bits_data,
  output        auto_in_d_bits_corrupt,
  input         auto_out_a_ready,
  output        auto_out_a_valid,
  output [2:0]  auto_out_a_bits_opcode,
  output [3:0]  auto_out_a_bits_size,
  output [4:0]  auto_out_a_bits_source,
  output [36:0] auto_out_a_bits_address,
  output        auto_out_a_bits_user_amba_prot_bufferable,
  output        auto_out_a_bits_user_amba_prot_modifiable,
  output        auto_out_a_bits_user_amba_prot_readalloc,
  output        auto_out_a_bits_user_amba_prot_writealloc,
  output        auto_out_a_bits_user_amba_prot_privileged,
  output        auto_out_a_bits_user_amba_prot_secure,
  output        auto_out_a_bits_user_amba_prot_fetch,
  output [7:0]  auto_out_a_bits_mask,
  output [63:0] auto_out_a_bits_data,
  output        auto_out_d_ready,
  input         auto_out_d_valid,
  input  [2:0]  auto_out_d_bits_opcode,
  input  [1:0]  auto_out_d_bits_param,
  input  [3:0]  auto_out_d_bits_size,
  input  [4:0]  auto_out_d_bits_source,
  input  [4:0]  auto_out_d_bits_sink,
  input         auto_out_d_bits_denied,
  input  [63:0] auto_out_d_bits_data,
  input         auto_out_d_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
`endif // RANDOMIZE_REG_INIT
  wire [36:0] _a_id_T = auto_in_a_bits_address ^ 37'h8000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_id_T_1 = {1'b0,$signed(_a_id_T)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_id_T_3 = $signed(_a_id_T_1) & 38'sh188e000000; // @[Parameters.scala 137:52]
  wire  _a_id_T_4 = $signed(_a_id_T_3) == 38'sh0; // @[Parameters.scala 137:67]
  wire [37:0] _a_id_T_6 = {1'b0,$signed(auto_in_a_bits_address)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_id_T_8 = $signed(_a_id_T_6) & 38'sh188c000000; // @[Parameters.scala 137:52]
  wire  _a_id_T_9 = $signed(_a_id_T_8) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _a_id_T_10 = auto_in_a_bits_address ^ 37'hc000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_id_T_11 = {1'b0,$signed(_a_id_T_10)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_id_T_13 = $signed(_a_id_T_11) & 38'sh188c000000; // @[Parameters.scala 137:52]
  wire  _a_id_T_14 = $signed(_a_id_T_13) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _a_id_T_15 = auto_in_a_bits_address ^ 37'h80000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_id_T_16 = {1'b0,$signed(_a_id_T_15)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_id_T_18 = $signed(_a_id_T_16) & 38'sh1880000000; // @[Parameters.scala 137:52]
  wire  _a_id_T_19 = $signed(_a_id_T_18) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _a_id_T_20 = auto_in_a_bits_address ^ 37'h1000000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_id_T_21 = {1'b0,$signed(_a_id_T_20)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_id_T_23 = $signed(_a_id_T_21) & 38'sh1000000000; // @[Parameters.scala 137:52]
  wire  _a_id_T_24 = $signed(_a_id_T_23) == 38'sh0; // @[Parameters.scala 137:67]
  wire  _a_id_T_27 = _a_id_T_9 | _a_id_T_14 | _a_id_T_19 | _a_id_T_24; // @[Parameters.scala 630:89]
  wire [1:0] _a_id_T_40 = _a_id_T_27 ? 2'h2 : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _GEN_136 = {{1'd0}, _a_id_T_4}; // @[Mux.scala 27:72]
  wire [1:0] a_id = _GEN_136 | _a_id_T_40; // @[Mux.scala 27:72]
  wire  a_noDomain = a_id == 2'h0; // @[FIFOFixer.scala 55:29]
  wire  stalls_a_sel = auto_in_a_bits_source[4:3] == 2'h0; // @[Parameters.scala 54:32]
  reg [4:0] a_first_counter; // @[Edges.scala 228:27]
  wire  a_first = a_first_counter == 5'h0; // @[Edges.scala 230:25]
  reg  flight_0; // @[FIFOFixer.scala 71:27]
  reg  flight_1; // @[FIFOFixer.scala 71:27]
  reg  flight_2; // @[FIFOFixer.scala 71:27]
  reg  flight_3; // @[FIFOFixer.scala 71:27]
  reg  flight_4; // @[FIFOFixer.scala 71:27]
  reg  flight_5; // @[FIFOFixer.scala 71:27]
  reg  flight_6; // @[FIFOFixer.scala 71:27]
  reg  flight_7; // @[FIFOFixer.scala 71:27]
  wire  _stalls_T_7 = flight_0 | flight_1 | flight_2 | flight_3 | flight_4 | flight_5 | flight_6 | flight_7; // @[FIFOFixer.scala 80:44]
  reg [1:0] stalls_id; // @[Reg.scala 15:16]
  wire  stalls_0 = stalls_a_sel & a_first & _stalls_T_7 & (a_noDomain | stalls_id != a_id); // @[FIFOFixer.scala 80:50]
  wire  stalls_a_sel_1 = auto_in_a_bits_source[4:3] == 2'h1; // @[Parameters.scala 54:32]
  reg  flight_8; // @[FIFOFixer.scala 71:27]
  reg  flight_9; // @[FIFOFixer.scala 71:27]
  reg  flight_10; // @[FIFOFixer.scala 71:27]
  reg  flight_11; // @[FIFOFixer.scala 71:27]
  reg  flight_12; // @[FIFOFixer.scala 71:27]
  reg  flight_13; // @[FIFOFixer.scala 71:27]
  reg  flight_14; // @[FIFOFixer.scala 71:27]
  reg  flight_15; // @[FIFOFixer.scala 71:27]
  reg [1:0] stalls_id_1; // @[Reg.scala 15:16]
  wire  stalls_1 = stalls_a_sel_1 & a_first & (flight_8 | flight_9 | flight_10 | flight_11 | flight_12 | flight_13 |
    flight_14 | flight_15) & (a_noDomain | stalls_id_1 != a_id); // @[FIFOFixer.scala 80:50]
  wire  stalls_a_sel_2 = auto_in_a_bits_source[4:3] == 2'h2; // @[Parameters.scala 54:32]
  reg  flight_16; // @[FIFOFixer.scala 71:27]
  reg  flight_17; // @[FIFOFixer.scala 71:27]
  reg  flight_18; // @[FIFOFixer.scala 71:27]
  reg  flight_19; // @[FIFOFixer.scala 71:27]
  reg  flight_20; // @[FIFOFixer.scala 71:27]
  reg  flight_21; // @[FIFOFixer.scala 71:27]
  reg  flight_22; // @[FIFOFixer.scala 71:27]
  reg  flight_23; // @[FIFOFixer.scala 71:27]
  reg [1:0] stalls_id_2; // @[Reg.scala 15:16]
  wire  stalls_2 = stalls_a_sel_2 & a_first & (flight_16 | flight_17 | flight_18 | flight_19 | flight_20 | flight_21 |
    flight_22 | flight_23) & (a_noDomain | stalls_id_2 != a_id); // @[FIFOFixer.scala 80:50]
  wire  stalls_a_sel_3 = auto_in_a_bits_source[4:3] == 2'h3; // @[Parameters.scala 54:32]
  reg  flight_24; // @[FIFOFixer.scala 71:27]
  reg  flight_25; // @[FIFOFixer.scala 71:27]
  reg  flight_26; // @[FIFOFixer.scala 71:27]
  reg  flight_27; // @[FIFOFixer.scala 71:27]
  reg  flight_28; // @[FIFOFixer.scala 71:27]
  reg  flight_29; // @[FIFOFixer.scala 71:27]
  reg  flight_30; // @[FIFOFixer.scala 71:27]
  reg  flight_31; // @[FIFOFixer.scala 71:27]
  reg [1:0] stalls_id_3; // @[Reg.scala 15:16]
  wire  stalls_3 = stalls_a_sel_3 & a_first & (flight_24 | flight_25 | flight_26 | flight_27 | flight_28 | flight_29 |
    flight_30 | flight_31) & (a_noDomain | stalls_id_3 != a_id); // @[FIFOFixer.scala 80:50]
  wire  stall = stalls_0 | stalls_1 | stalls_2 | stalls_3; // @[FIFOFixer.scala 83:49]
  wire  _bundleIn_0_a_ready_T = ~stall; // @[FIFOFixer.scala 88:50]
  wire  bundleIn_0_a_ready = auto_out_a_ready & _bundleIn_0_a_ready_T; // @[FIFOFixer.scala 88:33]
  wire  _a_first_T = bundleIn_0_a_ready & auto_in_a_valid; // @[Decoupled.scala 40:37]
  wire [22:0] _a_first_beats1_decode_T_1 = 23'hff << auto_in_a_bits_size; // @[package.scala 234:77]
  wire [7:0] _a_first_beats1_decode_T_3 = ~_a_first_beats1_decode_T_1[7:0]; // @[package.scala 234:46]
  wire [4:0] a_first_beats1_decode = _a_first_beats1_decode_T_3[7:3]; // @[Edges.scala 219:59]
  wire  a_first_beats1_opdata = ~auto_in_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [4:0] a_first_counter1 = a_first_counter - 5'h1; // @[Edges.scala 229:28]
  wire  _d_first_T = auto_in_d_ready & auto_out_d_valid; // @[Decoupled.scala 40:37]
  wire [22:0] _d_first_beats1_decode_T_1 = 23'hff << auto_out_d_bits_size; // @[package.scala 234:77]
  wire [7:0] _d_first_beats1_decode_T_3 = ~_d_first_beats1_decode_T_1[7:0]; // @[package.scala 234:46]
  wire [4:0] d_first_beats1_decode = _d_first_beats1_decode_T_3[7:3]; // @[Edges.scala 219:59]
  wire  d_first_beats1_opdata = auto_out_d_bits_opcode[0]; // @[Edges.scala 105:36]
  reg [4:0] d_first_counter; // @[Edges.scala 228:27]
  wire [4:0] d_first_counter1 = d_first_counter - 5'h1; // @[Edges.scala 229:28]
  wire  d_first_first = d_first_counter == 5'h0; // @[Edges.scala 230:25]
  wire  d_first = d_first_first & auto_out_d_bits_opcode != 3'h6; // @[FIFOFixer.scala 67:42]
  wire  _T_1 = a_first & _a_first_T; // @[FIFOFixer.scala 72:21]
  wire  _GEN_34 = _T_1 ? 5'h0 == auto_in_a_bits_source | flight_0 : flight_0; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_35 = _T_1 ? 5'h1 == auto_in_a_bits_source | flight_1 : flight_1; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_36 = _T_1 ? 5'h2 == auto_in_a_bits_source | flight_2 : flight_2; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_37 = _T_1 ? 5'h3 == auto_in_a_bits_source | flight_3 : flight_3; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_38 = _T_1 ? 5'h4 == auto_in_a_bits_source | flight_4 : flight_4; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_39 = _T_1 ? 5'h5 == auto_in_a_bits_source | flight_5 : flight_5; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_40 = _T_1 ? 5'h6 == auto_in_a_bits_source | flight_6 : flight_6; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_41 = _T_1 ? 5'h7 == auto_in_a_bits_source | flight_7 : flight_7; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_42 = _T_1 ? 5'h8 == auto_in_a_bits_source | flight_8 : flight_8; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_43 = _T_1 ? 5'h9 == auto_in_a_bits_source | flight_9 : flight_9; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_44 = _T_1 ? 5'ha == auto_in_a_bits_source | flight_10 : flight_10; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_45 = _T_1 ? 5'hb == auto_in_a_bits_source | flight_11 : flight_11; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_46 = _T_1 ? 5'hc == auto_in_a_bits_source | flight_12 : flight_12; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_47 = _T_1 ? 5'hd == auto_in_a_bits_source | flight_13 : flight_13; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_48 = _T_1 ? 5'he == auto_in_a_bits_source | flight_14 : flight_14; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_49 = _T_1 ? 5'hf == auto_in_a_bits_source | flight_15 : flight_15; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_50 = _T_1 ? 5'h10 == auto_in_a_bits_source | flight_16 : flight_16; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_51 = _T_1 ? 5'h11 == auto_in_a_bits_source | flight_17 : flight_17; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_52 = _T_1 ? 5'h12 == auto_in_a_bits_source | flight_18 : flight_18; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_53 = _T_1 ? 5'h13 == auto_in_a_bits_source | flight_19 : flight_19; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_54 = _T_1 ? 5'h14 == auto_in_a_bits_source | flight_20 : flight_20; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_55 = _T_1 ? 5'h15 == auto_in_a_bits_source | flight_21 : flight_21; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_56 = _T_1 ? 5'h16 == auto_in_a_bits_source | flight_22 : flight_22; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_57 = _T_1 ? 5'h17 == auto_in_a_bits_source | flight_23 : flight_23; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_58 = _T_1 ? 5'h18 == auto_in_a_bits_source | flight_24 : flight_24; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_59 = _T_1 ? 5'h19 == auto_in_a_bits_source | flight_25 : flight_25; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_60 = _T_1 ? 5'h1a == auto_in_a_bits_source | flight_26 : flight_26; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_61 = _T_1 ? 5'h1b == auto_in_a_bits_source | flight_27 : flight_27; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_62 = _T_1 ? 5'h1c == auto_in_a_bits_source | flight_28 : flight_28; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_63 = _T_1 ? 5'h1d == auto_in_a_bits_source | flight_29 : flight_29; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_64 = _T_1 ? 5'h1e == auto_in_a_bits_source | flight_30 : flight_30; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_65 = _T_1 ? 5'h1f == auto_in_a_bits_source | flight_31 : flight_31; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _T_3 = d_first & _d_first_T; // @[FIFOFixer.scala 73:21]
  wire  _stalls_id_T_1 = _a_first_T & stalls_a_sel; // @[FIFOFixer.scala 77:49]
  wire  _stalls_id_T_5 = _a_first_T & stalls_a_sel_1; // @[FIFOFixer.scala 77:49]
  wire  _stalls_id_T_9 = _a_first_T & stalls_a_sel_2; // @[FIFOFixer.scala 77:49]
  wire  _stalls_id_T_13 = _a_first_T & stalls_a_sel_3; // @[FIFOFixer.scala 77:49]
  assign auto_in_a_ready = auto_out_a_ready & _bundleIn_0_a_ready_T; // @[FIFOFixer.scala 88:33]
  assign auto_in_d_valid = auto_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_size = auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_source = auto_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_denied = auto_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_data = auto_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_corrupt = auto_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_out_a_valid = auto_in_a_valid & _bundleIn_0_a_ready_T; // @[FIFOFixer.scala 87:33]
  assign auto_out_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_size = auto_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_source = auto_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_bufferable = auto_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_modifiable = auto_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_readalloc = auto_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_writealloc = auto_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_privileged = auto_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_secure = auto_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_fetch = auto_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_data = auto_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_d_ready = auto_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      a_first_counter <= 5'h0;
    end else if (_a_first_T) begin
      if (a_first) begin
        if (a_first_beats1_opdata) begin
          a_first_counter <= a_first_beats1_decode;
        end else begin
          a_first_counter <= 5'h0;
        end
      end else begin
        a_first_counter <= a_first_counter1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_0 <= 1'h0;
    end else if (_T_3) begin
      if (5'h0 == auto_out_d_bits_source) begin
        flight_0 <= 1'h0;
      end else begin
        flight_0 <= _GEN_34;
      end
    end else begin
      flight_0 <= _GEN_34;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_1 <= 1'h0;
    end else if (_T_3) begin
      if (5'h1 == auto_out_d_bits_source) begin
        flight_1 <= 1'h0;
      end else begin
        flight_1 <= _GEN_35;
      end
    end else begin
      flight_1 <= _GEN_35;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_2 <= 1'h0;
    end else if (_T_3) begin
      if (5'h2 == auto_out_d_bits_source) begin
        flight_2 <= 1'h0;
      end else begin
        flight_2 <= _GEN_36;
      end
    end else begin
      flight_2 <= _GEN_36;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_3 <= 1'h0;
    end else if (_T_3) begin
      if (5'h3 == auto_out_d_bits_source) begin
        flight_3 <= 1'h0;
      end else begin
        flight_3 <= _GEN_37;
      end
    end else begin
      flight_3 <= _GEN_37;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_4 <= 1'h0;
    end else if (_T_3) begin
      if (5'h4 == auto_out_d_bits_source) begin
        flight_4 <= 1'h0;
      end else begin
        flight_4 <= _GEN_38;
      end
    end else begin
      flight_4 <= _GEN_38;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_5 <= 1'h0;
    end else if (_T_3) begin
      if (5'h5 == auto_out_d_bits_source) begin
        flight_5 <= 1'h0;
      end else begin
        flight_5 <= _GEN_39;
      end
    end else begin
      flight_5 <= _GEN_39;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_6 <= 1'h0;
    end else if (_T_3) begin
      if (5'h6 == auto_out_d_bits_source) begin
        flight_6 <= 1'h0;
      end else begin
        flight_6 <= _GEN_40;
      end
    end else begin
      flight_6 <= _GEN_40;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_7 <= 1'h0;
    end else if (_T_3) begin
      if (5'h7 == auto_out_d_bits_source) begin
        flight_7 <= 1'h0;
      end else begin
        flight_7 <= _GEN_41;
      end
    end else begin
      flight_7 <= _GEN_41;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id <= 2'h0;
    end else if (_stalls_id_T_1) begin
      stalls_id <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_8 <= 1'h0;
    end else if (_T_3) begin
      if (5'h8 == auto_out_d_bits_source) begin
        flight_8 <= 1'h0;
      end else begin
        flight_8 <= _GEN_42;
      end
    end else begin
      flight_8 <= _GEN_42;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_9 <= 1'h0;
    end else if (_T_3) begin
      if (5'h9 == auto_out_d_bits_source) begin
        flight_9 <= 1'h0;
      end else begin
        flight_9 <= _GEN_43;
      end
    end else begin
      flight_9 <= _GEN_43;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_10 <= 1'h0;
    end else if (_T_3) begin
      if (5'ha == auto_out_d_bits_source) begin
        flight_10 <= 1'h0;
      end else begin
        flight_10 <= _GEN_44;
      end
    end else begin
      flight_10 <= _GEN_44;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_11 <= 1'h0;
    end else if (_T_3) begin
      if (5'hb == auto_out_d_bits_source) begin
        flight_11 <= 1'h0;
      end else begin
        flight_11 <= _GEN_45;
      end
    end else begin
      flight_11 <= _GEN_45;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_12 <= 1'h0;
    end else if (_T_3) begin
      if (5'hc == auto_out_d_bits_source) begin
        flight_12 <= 1'h0;
      end else begin
        flight_12 <= _GEN_46;
      end
    end else begin
      flight_12 <= _GEN_46;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_13 <= 1'h0;
    end else if (_T_3) begin
      if (5'hd == auto_out_d_bits_source) begin
        flight_13 <= 1'h0;
      end else begin
        flight_13 <= _GEN_47;
      end
    end else begin
      flight_13 <= _GEN_47;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_14 <= 1'h0;
    end else if (_T_3) begin
      if (5'he == auto_out_d_bits_source) begin
        flight_14 <= 1'h0;
      end else begin
        flight_14 <= _GEN_48;
      end
    end else begin
      flight_14 <= _GEN_48;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_15 <= 1'h0;
    end else if (_T_3) begin
      if (5'hf == auto_out_d_bits_source) begin
        flight_15 <= 1'h0;
      end else begin
        flight_15 <= _GEN_49;
      end
    end else begin
      flight_15 <= _GEN_49;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id_1 <= 2'h0;
    end else if (_stalls_id_T_5) begin
      stalls_id_1 <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_16 <= 1'h0;
    end else if (_T_3) begin
      if (5'h10 == auto_out_d_bits_source) begin
        flight_16 <= 1'h0;
      end else begin
        flight_16 <= _GEN_50;
      end
    end else begin
      flight_16 <= _GEN_50;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_17 <= 1'h0;
    end else if (_T_3) begin
      if (5'h11 == auto_out_d_bits_source) begin
        flight_17 <= 1'h0;
      end else begin
        flight_17 <= _GEN_51;
      end
    end else begin
      flight_17 <= _GEN_51;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_18 <= 1'h0;
    end else if (_T_3) begin
      if (5'h12 == auto_out_d_bits_source) begin
        flight_18 <= 1'h0;
      end else begin
        flight_18 <= _GEN_52;
      end
    end else begin
      flight_18 <= _GEN_52;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_19 <= 1'h0;
    end else if (_T_3) begin
      if (5'h13 == auto_out_d_bits_source) begin
        flight_19 <= 1'h0;
      end else begin
        flight_19 <= _GEN_53;
      end
    end else begin
      flight_19 <= _GEN_53;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_20 <= 1'h0;
    end else if (_T_3) begin
      if (5'h14 == auto_out_d_bits_source) begin
        flight_20 <= 1'h0;
      end else begin
        flight_20 <= _GEN_54;
      end
    end else begin
      flight_20 <= _GEN_54;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_21 <= 1'h0;
    end else if (_T_3) begin
      if (5'h15 == auto_out_d_bits_source) begin
        flight_21 <= 1'h0;
      end else begin
        flight_21 <= _GEN_55;
      end
    end else begin
      flight_21 <= _GEN_55;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_22 <= 1'h0;
    end else if (_T_3) begin
      if (5'h16 == auto_out_d_bits_source) begin
        flight_22 <= 1'h0;
      end else begin
        flight_22 <= _GEN_56;
      end
    end else begin
      flight_22 <= _GEN_56;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_23 <= 1'h0;
    end else if (_T_3) begin
      if (5'h17 == auto_out_d_bits_source) begin
        flight_23 <= 1'h0;
      end else begin
        flight_23 <= _GEN_57;
      end
    end else begin
      flight_23 <= _GEN_57;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id_2 <= 2'h0;
    end else if (_stalls_id_T_9) begin
      stalls_id_2 <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_24 <= 1'h0;
    end else if (_T_3) begin
      if (5'h18 == auto_out_d_bits_source) begin
        flight_24 <= 1'h0;
      end else begin
        flight_24 <= _GEN_58;
      end
    end else begin
      flight_24 <= _GEN_58;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_25 <= 1'h0;
    end else if (_T_3) begin
      if (5'h19 == auto_out_d_bits_source) begin
        flight_25 <= 1'h0;
      end else begin
        flight_25 <= _GEN_59;
      end
    end else begin
      flight_25 <= _GEN_59;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_26 <= 1'h0;
    end else if (_T_3) begin
      if (5'h1a == auto_out_d_bits_source) begin
        flight_26 <= 1'h0;
      end else begin
        flight_26 <= _GEN_60;
      end
    end else begin
      flight_26 <= _GEN_60;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_27 <= 1'h0;
    end else if (_T_3) begin
      if (5'h1b == auto_out_d_bits_source) begin
        flight_27 <= 1'h0;
      end else begin
        flight_27 <= _GEN_61;
      end
    end else begin
      flight_27 <= _GEN_61;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_28 <= 1'h0;
    end else if (_T_3) begin
      if (5'h1c == auto_out_d_bits_source) begin
        flight_28 <= 1'h0;
      end else begin
        flight_28 <= _GEN_62;
      end
    end else begin
      flight_28 <= _GEN_62;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_29 <= 1'h0;
    end else if (_T_3) begin
      if (5'h1d == auto_out_d_bits_source) begin
        flight_29 <= 1'h0;
      end else begin
        flight_29 <= _GEN_63;
      end
    end else begin
      flight_29 <= _GEN_63;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_30 <= 1'h0;
    end else if (_T_3) begin
      if (5'h1e == auto_out_d_bits_source) begin
        flight_30 <= 1'h0;
      end else begin
        flight_30 <= _GEN_64;
      end
    end else begin
      flight_30 <= _GEN_64;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_31 <= 1'h0;
    end else if (_T_3) begin
      if (5'h1f == auto_out_d_bits_source) begin
        flight_31 <= 1'h0;
      end else begin
        flight_31 <= _GEN_65;
      end
    end else begin
      flight_31 <= _GEN_65;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id_3 <= 2'h0;
    end else if (_stalls_id_T_13) begin
      stalls_id_3 <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      d_first_counter <= 5'h0;
    end else if (_d_first_T) begin
      if (d_first_first) begin
        if (d_first_beats1_opdata) begin
          d_first_counter <= d_first_beats1_decode;
        end else begin
          d_first_counter <= 5'h0;
        end
      end else begin
        d_first_counter <= d_first_counter1;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  a_first_counter = _RAND_0[4:0];
  _RAND_1 = {1{`RANDOM}};
  flight_0 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  flight_1 = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  flight_2 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  flight_3 = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  flight_4 = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  flight_5 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  flight_6 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  flight_7 = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  stalls_id = _RAND_9[1:0];
  _RAND_10 = {1{`RANDOM}};
  flight_8 = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  flight_9 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  flight_10 = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  flight_11 = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  flight_12 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  flight_13 = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  flight_14 = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  flight_15 = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  stalls_id_1 = _RAND_18[1:0];
  _RAND_19 = {1{`RANDOM}};
  flight_16 = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  flight_17 = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  flight_18 = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  flight_19 = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  flight_20 = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  flight_21 = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  flight_22 = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  flight_23 = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  stalls_id_2 = _RAND_27[1:0];
  _RAND_28 = {1{`RANDOM}};
  flight_24 = _RAND_28[0:0];
  _RAND_29 = {1{`RANDOM}};
  flight_25 = _RAND_29[0:0];
  _RAND_30 = {1{`RANDOM}};
  flight_26 = _RAND_30[0:0];
  _RAND_31 = {1{`RANDOM}};
  flight_27 = _RAND_31[0:0];
  _RAND_32 = {1{`RANDOM}};
  flight_28 = _RAND_32[0:0];
  _RAND_33 = {1{`RANDOM}};
  flight_29 = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  flight_30 = _RAND_34[0:0];
  _RAND_35 = {1{`RANDOM}};
  flight_31 = _RAND_35[0:0];
  _RAND_36 = {1{`RANDOM}};
  stalls_id_3 = _RAND_36[1:0];
  _RAND_37 = {1{`RANDOM}};
  d_first_counter = _RAND_37[4:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    a_first_counter = 5'h0;
  end
  if (reset) begin
    flight_0 = 1'h0;
  end
  if (reset) begin
    flight_1 = 1'h0;
  end
  if (reset) begin
    flight_2 = 1'h0;
  end
  if (reset) begin
    flight_3 = 1'h0;
  end
  if (reset) begin
    flight_4 = 1'h0;
  end
  if (reset) begin
    flight_5 = 1'h0;
  end
  if (reset) begin
    flight_6 = 1'h0;
  end
  if (reset) begin
    flight_7 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id = 2'h0;
  end
  if (reset) begin
    flight_8 = 1'h0;
  end
  if (reset) begin
    flight_9 = 1'h0;
  end
  if (reset) begin
    flight_10 = 1'h0;
  end
  if (reset) begin
    flight_11 = 1'h0;
  end
  if (reset) begin
    flight_12 = 1'h0;
  end
  if (reset) begin
    flight_13 = 1'h0;
  end
  if (reset) begin
    flight_14 = 1'h0;
  end
  if (reset) begin
    flight_15 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id_1 = 2'h0;
  end
  if (reset) begin
    flight_16 = 1'h0;
  end
  if (reset) begin
    flight_17 = 1'h0;
  end
  if (reset) begin
    flight_18 = 1'h0;
  end
  if (reset) begin
    flight_19 = 1'h0;
  end
  if (reset) begin
    flight_20 = 1'h0;
  end
  if (reset) begin
    flight_21 = 1'h0;
  end
  if (reset) begin
    flight_22 = 1'h0;
  end
  if (reset) begin
    flight_23 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id_2 = 2'h0;
  end
  if (reset) begin
    flight_24 = 1'h0;
  end
  if (reset) begin
    flight_25 = 1'h0;
  end
  if (reset) begin
    flight_26 = 1'h0;
  end
  if (reset) begin
    flight_27 = 1'h0;
  end
  if (reset) begin
    flight_28 = 1'h0;
  end
  if (reset) begin
    flight_29 = 1'h0;
  end
  if (reset) begin
    flight_30 = 1'h0;
  end
  if (reset) begin
    flight_31 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id_3 = 2'h0;
  end
  if (reset) begin
    d_first_counter = 5'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
