//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88

module SCIEPipelined #(parameter XLEN = 32) (
    input clock,
    input valid,
    input [31:0] insn,
    input [XLEN-1:0] rs1,
    input [XLEN-1:0] rs2,
    output [XLEN-1:0] rd);

  /* This example SCIE implementation provides the following instructions:

     Major opcode custom-0:
     Funct3 = 2: AD.U8, compute absolute differences of packed uint8
       rd[7:0] = abs(rs1[7:0] - rs2[7:0])
       rd[15:8] = abs(rs1[15:8] - rs2[15:8])
       ...
       rd[XLEN-1:XLEN-8] = abs(rs1[XLEN-1:XLEN-8] - rs2[XLEN-1:XLEN-8])

     Funct3 = 3: SAD.U8, compute sum of absolute differences of packed uint8
       tmp[7:0] = abs(rs1[7:0] - rs2[7:0])
       tmp[15:8] = abs(rs1[15:8] - rs2[15:8])
       ...
       tmp[XLEN-1:XLEN-8] = abs(rs1[XLEN-1:XLEN-8] - rs2[XLEN-1:XLEN-8])

       rd = tmp[7:0] + tmp[15:8] + ... + tmp[XLEN-1:XLEN-8]
  */

  integer i;
  reg [XLEN-1:0] absolute_differences;
  reg funct3_0;
  reg [XLEN-1:0] result;

`ifndef RANDOM
`define RANDOM $random
`endif

  always @(posedge clock)
  begin
    /* Gating using the valid signal is optional, but saves power. */
    if (valid)
    begin
      /* Register Funct3[0] opcode bit for result muxing in next stage. */
      funct3_0 <= insn[12];

      /* Compute each absolute difference and register each result. */
      for (i = 0; i < XLEN/8; i = i + 1)
      begin
        absolute_differences[8*i +: 8] <= rs1[8*i +: 8] < rs2[8*i +: 8] ?
                                          rs2[8*i +: 8] - rs1[8*i +: 8] :
                                          rs1[8*i +: 8] - rs2[8*i +: 8];
      end
    end
  end

  /* In the second pipeline stage, compute the final result. */
  always @(*)
  begin
    if (!funct3_0)
    begin
      /* If Funct3[0] = 0, the output is the packed absolute differences. */
      result = absolute_differences;
    end
    else
    begin
      /* If Funct3[0] = 1, the output is their sum. */
      result = {XLEN{1'b0}};
      for (i = 0; i < XLEN/8; i = i + 1)
      begin
        result = result + {{(XLEN-8){1'b0}}, absolute_differences[8*i +: 8]};
      end
    end
  end

  /* Drive the output. */
  assign rd = result;

 /* Suppress Xs at simulation start */
 `ifdef RANDOMIZE_REG_INIT
 initial begin
   `ifndef VERILATOR
   #`RANDOMIZE_DELAY begin end
   `endif
   absolute_differences = {(XLEN / 32){`RANDOM}};
   funct3_0 = absolute_differences[0];
 end
 `endif

endmodule
     