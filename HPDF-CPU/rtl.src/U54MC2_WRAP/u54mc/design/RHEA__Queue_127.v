//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__Queue_127(
  input         rf_reset,
  input         clock,
  input         reset,
  output        io_enq_ready,
  input         io_enq_valid,
  input  [9:0]  io_enq_bits_set,
  input  [2:0]  io_enq_bits_way,
  input         io_enq_bits_data_dirty,
  input  [1:0]  io_enq_bits_data_state,
  input  [3:0]  io_enq_bits_data_clients,
  input  [17:0] io_enq_bits_data_tag,
  input         io_deq_ready,
  output        io_deq_valid,
  output [9:0]  io_deq_bits_set,
  output [2:0]  io_deq_bits_way,
  output        io_deq_bits_data_dirty,
  output [1:0]  io_deq_bits_data_state,
  output [3:0]  io_deq_bits_data_clients,
  output [17:0] io_deq_bits_data_tag
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
`endif // RANDOMIZE_REG_INIT
  reg [9:0] ram_0_set; // @[Decoupled.scala 218:16]
  reg [2:0] ram_0_way; // @[Decoupled.scala 218:16]
  reg  ram_0_data_dirty; // @[Decoupled.scala 218:16]
  reg [1:0] ram_0_data_state; // @[Decoupled.scala 218:16]
  reg [3:0] ram_0_data_clients; // @[Decoupled.scala 218:16]
  reg [17:0] ram_0_data_tag; // @[Decoupled.scala 218:16]
  wire  do_enq = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  reg  maybe_full; // @[Decoupled.scala 221:27]
  wire  empty = ~maybe_full; // @[Decoupled.scala 224:28]
  wire  do_deq = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  assign io_enq_ready = ~maybe_full; // @[Decoupled.scala 241:19]
  assign io_deq_valid = ~empty; // @[Decoupled.scala 240:19]
  assign io_deq_bits_set = ram_0_set; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_way = ram_0_way; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_data_dirty = ram_0_data_dirty; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_data_state = ram_0_data_state; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_data_clients = ram_0_data_clients; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_data_tag = ram_0_data_tag; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_set <= 10'h0;
    end else if (do_enq) begin
      ram_0_set <= io_enq_bits_set;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_way <= 3'h0;
    end else if (do_enq) begin
      ram_0_way <= io_enq_bits_way;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_data_dirty <= 1'h0;
    end else if (do_enq) begin
      ram_0_data_dirty <= io_enq_bits_data_dirty;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_data_state <= 2'h0;
    end else if (do_enq) begin
      ram_0_data_state <= io_enq_bits_data_state;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_data_clients <= 4'h0;
    end else if (do_enq) begin
      ram_0_data_clients <= io_enq_bits_data_clients;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_data_tag <= 18'h0;
    end else if (do_enq) begin
      ram_0_data_tag <= io_enq_bits_data_tag;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      maybe_full <= 1'h0;
    end else if (do_enq != do_deq) begin
      maybe_full <= do_enq;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ram_0_set = _RAND_0[9:0];
  _RAND_1 = {1{`RANDOM}};
  ram_0_way = _RAND_1[2:0];
  _RAND_2 = {1{`RANDOM}};
  ram_0_data_dirty = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  ram_0_data_state = _RAND_3[1:0];
  _RAND_4 = {1{`RANDOM}};
  ram_0_data_clients = _RAND_4[3:0];
  _RAND_5 = {1{`RANDOM}};
  ram_0_data_tag = _RAND_5[17:0];
  _RAND_6 = {1{`RANDOM}};
  maybe_full = _RAND_6[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ram_0_set = 10'h0;
  end
  if (rf_reset) begin
    ram_0_way = 3'h0;
  end
  if (rf_reset) begin
    ram_0_data_dirty = 1'h0;
  end
  if (rf_reset) begin
    ram_0_data_state = 2'h0;
  end
  if (rf_reset) begin
    ram_0_data_clients = 4'h0;
  end
  if (rf_reset) begin
    ram_0_data_tag = 18'h0;
  end
  if (reset) begin
    maybe_full = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
