//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__SourceE(
  input        rf_reset,
  input        clock,
  input        reset,
  output       io_req_ready,
  input        io_req_valid,
  input  [2:0] io_req_bits_sink,
  output       io_e_valid,
  output [2:0] io_e_bits_sink
);
  wire  io_e_q_rf_reset; // @[Decoupled.scala 296:21]
  wire  io_e_q_clock; // @[Decoupled.scala 296:21]
  wire  io_e_q_reset; // @[Decoupled.scala 296:21]
  wire  io_e_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  io_e_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] io_e_q_io_enq_bits_sink; // @[Decoupled.scala 296:21]
  wire  io_e_q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] io_e_q_io_deq_bits_sink; // @[Decoupled.scala 296:21]
  RHEA__Queue_121 io_e_q ( // @[Decoupled.scala 296:21]
    .rf_reset(io_e_q_rf_reset),
    .clock(io_e_q_clock),
    .reset(io_e_q_reset),
    .io_enq_ready(io_e_q_io_enq_ready),
    .io_enq_valid(io_e_q_io_enq_valid),
    .io_enq_bits_sink(io_e_q_io_enq_bits_sink),
    .io_deq_valid(io_e_q_io_deq_valid),
    .io_deq_bits_sink(io_e_q_io_deq_bits_sink)
  );
  assign io_e_q_rf_reset = rf_reset;
  assign io_req_ready = io_e_q_io_enq_ready; // @[SourceE.scala 42:15 Decoupled.scala 299:17]
  assign io_e_valid = io_e_q_io_deq_valid; // @[SourceE.scala 43:8]
  assign io_e_bits_sink = io_e_q_io_deq_bits_sink; // @[SourceE.scala 43:8]
  assign io_e_q_clock = clock;
  assign io_e_q_reset = reset;
  assign io_e_q_io_enq_valid = io_req_valid; // @[SourceE.scala 42:15 SourceE.scala 46:11]
  assign io_e_q_io_enq_bits_sink = io_req_bits_sink; // @[SourceE.scala 42:15 SourceE.scala 48:15]
endmodule
