//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLToAXI4_1_assert(
  input         clock,
  input         reset,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [2:0]  auto_in_a_bits_param,
  input  [2:0]  auto_in_a_bits_size,
  input  [3:0]  auto_in_a_bits_source,
  input  [35:0] auto_in_a_bits_address,
  input  [15:0] auto_in_a_bits_mask,
  input         auto_in_a_bits_corrupt,
  input         auto_in_d_ready,
  input         auto_out_b_valid,
  input  [3:0]  auto_out_b_bits_echo_tl_state_source,
  input         auto_out_r_valid,
  input  [3:0]  auto_out_r_bits_echo_tl_state_source,
  input         _T_549,
  input         _T_521,
  input         _T_493,
  input         _T_465,
  input         _T_437,
  input         _T_409,
  input         _T_381,
  input         _T_353,
  input         _T_325,
  input         _T_297,
  input         _T_269,
  input         _T_241,
  input         _T_213,
  input         _T_185,
  input         _T_157,
  input         _T_129,
  input         _T_55,
  input         _T_58,
  input         _T_74,
  input         _GEN_36,
  input         _T_84,
  input         _T_85,
  input  [2:0]  _T_86_size,
  input  [2:0]  _T_87_size,
  input         _T_133,
  input         _T_136,
  input         _T_161,
  input         _T_164,
  input         _T_189,
  input         _T_192,
  input         _T_217,
  input         _T_220,
  input         _T_245,
  input         _T_248,
  input         _T_273,
  input         _T_276,
  input         _T_301,
  input         _T_304,
  input         _T_329,
  input         _T_332,
  input         _T_357,
  input         _T_360,
  input         _T_385,
  input         _T_388,
  input         _T_413,
  input         _T_416,
  input         _T_441,
  input         _T_444,
  input         _T_469,
  input         _T_472,
  input         _T_497,
  input         _T_500,
  input         _T_525,
  input         _T_528,
  input         _T_553,
  input         _T_556
);
  wire  TLMonitor_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [15:0] TLMonitor_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  _T_551; // @[ToAXI4.scala 249:26]
  wire  _T_523; // @[ToAXI4.scala 249:26]
  wire  _T_495; // @[ToAXI4.scala 249:26]
  wire  _T_467; // @[ToAXI4.scala 249:26]
  wire  _T_439; // @[ToAXI4.scala 249:26]
  wire  _T_411; // @[ToAXI4.scala 249:26]
  wire  _T_383; // @[ToAXI4.scala 249:26]
  wire  _T_355; // @[ToAXI4.scala 249:26]
  wire  _T_327; // @[ToAXI4.scala 249:26]
  wire  _T_299; // @[ToAXI4.scala 249:26]
  wire  _T_271; // @[ToAXI4.scala 249:26]
  wire  _T_243; // @[ToAXI4.scala 249:26]
  wire  _T_215; // @[ToAXI4.scala 249:26]
  wire  _T_187; // @[ToAXI4.scala 249:26]
  wire  _T_159; // @[ToAXI4.scala 249:26]
  wire  _T_131; // @[ToAXI4.scala 249:26]
  wire  _T_141; // @[ToAXI4.scala 255:17]
  wire  _T_143; // @[ToAXI4.scala 255:22]
  wire  _T_145; // @[ToAXI4.scala 255:16]
  wire  _T_146; // @[ToAXI4.scala 255:16]
  wire  _T_147; // @[ToAXI4.scala 256:17]
  wire  _T_149; // @[ToAXI4.scala 256:22]
  wire  _T_151; // @[ToAXI4.scala 256:16]
  wire  _T_152; // @[ToAXI4.scala 256:16]
  wire  _T_169; // @[ToAXI4.scala 255:17]
  wire  _T_171; // @[ToAXI4.scala 255:22]
  wire  _T_173; // @[ToAXI4.scala 255:16]
  wire  _T_174; // @[ToAXI4.scala 255:16]
  wire  _T_175; // @[ToAXI4.scala 256:17]
  wire  _T_177; // @[ToAXI4.scala 256:22]
  wire  _T_179; // @[ToAXI4.scala 256:16]
  wire  _T_180; // @[ToAXI4.scala 256:16]
  wire  _T_197; // @[ToAXI4.scala 255:17]
  wire  _T_199; // @[ToAXI4.scala 255:22]
  wire  _T_201; // @[ToAXI4.scala 255:16]
  wire  _T_202; // @[ToAXI4.scala 255:16]
  wire  _T_203; // @[ToAXI4.scala 256:17]
  wire  _T_205; // @[ToAXI4.scala 256:22]
  wire  _T_207; // @[ToAXI4.scala 256:16]
  wire  _T_208; // @[ToAXI4.scala 256:16]
  wire  _T_225; // @[ToAXI4.scala 255:17]
  wire  _T_227; // @[ToAXI4.scala 255:22]
  wire  _T_229; // @[ToAXI4.scala 255:16]
  wire  _T_230; // @[ToAXI4.scala 255:16]
  wire  _T_231; // @[ToAXI4.scala 256:17]
  wire  _T_233; // @[ToAXI4.scala 256:22]
  wire  _T_235; // @[ToAXI4.scala 256:16]
  wire  _T_236; // @[ToAXI4.scala 256:16]
  wire  _T_253; // @[ToAXI4.scala 255:17]
  wire  _T_255; // @[ToAXI4.scala 255:22]
  wire  _T_257; // @[ToAXI4.scala 255:16]
  wire  _T_258; // @[ToAXI4.scala 255:16]
  wire  _T_259; // @[ToAXI4.scala 256:17]
  wire  _T_261; // @[ToAXI4.scala 256:22]
  wire  _T_263; // @[ToAXI4.scala 256:16]
  wire  _T_264; // @[ToAXI4.scala 256:16]
  wire  _T_281; // @[ToAXI4.scala 255:17]
  wire  _T_283; // @[ToAXI4.scala 255:22]
  wire  _T_285; // @[ToAXI4.scala 255:16]
  wire  _T_286; // @[ToAXI4.scala 255:16]
  wire  _T_287; // @[ToAXI4.scala 256:17]
  wire  _T_289; // @[ToAXI4.scala 256:22]
  wire  _T_291; // @[ToAXI4.scala 256:16]
  wire  _T_292; // @[ToAXI4.scala 256:16]
  wire  _T_309; // @[ToAXI4.scala 255:17]
  wire  _T_311; // @[ToAXI4.scala 255:22]
  wire  _T_313; // @[ToAXI4.scala 255:16]
  wire  _T_314; // @[ToAXI4.scala 255:16]
  wire  _T_315; // @[ToAXI4.scala 256:17]
  wire  _T_317; // @[ToAXI4.scala 256:22]
  wire  _T_319; // @[ToAXI4.scala 256:16]
  wire  _T_320; // @[ToAXI4.scala 256:16]
  wire  _T_337; // @[ToAXI4.scala 255:17]
  wire  _T_339; // @[ToAXI4.scala 255:22]
  wire  _T_341; // @[ToAXI4.scala 255:16]
  wire  _T_342; // @[ToAXI4.scala 255:16]
  wire  _T_343; // @[ToAXI4.scala 256:17]
  wire  _T_345; // @[ToAXI4.scala 256:22]
  wire  _T_347; // @[ToAXI4.scala 256:16]
  wire  _T_348; // @[ToAXI4.scala 256:16]
  wire  _T_365; // @[ToAXI4.scala 255:17]
  wire  _T_367; // @[ToAXI4.scala 255:22]
  wire  _T_369; // @[ToAXI4.scala 255:16]
  wire  _T_370; // @[ToAXI4.scala 255:16]
  wire  _T_371; // @[ToAXI4.scala 256:17]
  wire  _T_373; // @[ToAXI4.scala 256:22]
  wire  _T_375; // @[ToAXI4.scala 256:16]
  wire  _T_376; // @[ToAXI4.scala 256:16]
  wire  _T_393; // @[ToAXI4.scala 255:17]
  wire  _T_395; // @[ToAXI4.scala 255:22]
  wire  _T_397; // @[ToAXI4.scala 255:16]
  wire  _T_398; // @[ToAXI4.scala 255:16]
  wire  _T_399; // @[ToAXI4.scala 256:17]
  wire  _T_401; // @[ToAXI4.scala 256:22]
  wire  _T_403; // @[ToAXI4.scala 256:16]
  wire  _T_404; // @[ToAXI4.scala 256:16]
  wire  _T_421; // @[ToAXI4.scala 255:17]
  wire  _T_423; // @[ToAXI4.scala 255:22]
  wire  _T_425; // @[ToAXI4.scala 255:16]
  wire  _T_426; // @[ToAXI4.scala 255:16]
  wire  _T_427; // @[ToAXI4.scala 256:17]
  wire  _T_429; // @[ToAXI4.scala 256:22]
  wire  _T_431; // @[ToAXI4.scala 256:16]
  wire  _T_432; // @[ToAXI4.scala 256:16]
  wire  _T_449; // @[ToAXI4.scala 255:17]
  wire  _T_451; // @[ToAXI4.scala 255:22]
  wire  _T_453; // @[ToAXI4.scala 255:16]
  wire  _T_454; // @[ToAXI4.scala 255:16]
  wire  _T_455; // @[ToAXI4.scala 256:17]
  wire  _T_457; // @[ToAXI4.scala 256:22]
  wire  _T_459; // @[ToAXI4.scala 256:16]
  wire  _T_460; // @[ToAXI4.scala 256:16]
  wire  _T_477; // @[ToAXI4.scala 255:17]
  wire  _T_479; // @[ToAXI4.scala 255:22]
  wire  _T_481; // @[ToAXI4.scala 255:16]
  wire  _T_482; // @[ToAXI4.scala 255:16]
  wire  _T_483; // @[ToAXI4.scala 256:17]
  wire  _T_485; // @[ToAXI4.scala 256:22]
  wire  _T_487; // @[ToAXI4.scala 256:16]
  wire  _T_488; // @[ToAXI4.scala 256:16]
  wire  _T_505; // @[ToAXI4.scala 255:17]
  wire  _T_507; // @[ToAXI4.scala 255:22]
  wire  _T_509; // @[ToAXI4.scala 255:16]
  wire  _T_510; // @[ToAXI4.scala 255:16]
  wire  _T_511; // @[ToAXI4.scala 256:17]
  wire  _T_513; // @[ToAXI4.scala 256:22]
  wire  _T_515; // @[ToAXI4.scala 256:16]
  wire  _T_516; // @[ToAXI4.scala 256:16]
  wire  _T_533; // @[ToAXI4.scala 255:17]
  wire  _T_535; // @[ToAXI4.scala 255:22]
  wire  _T_537; // @[ToAXI4.scala 255:16]
  wire  _T_538; // @[ToAXI4.scala 255:16]
  wire  _T_539; // @[ToAXI4.scala 256:17]
  wire  _T_541; // @[ToAXI4.scala 256:22]
  wire  _T_543; // @[ToAXI4.scala 256:16]
  wire  _T_544; // @[ToAXI4.scala 256:16]
  wire  _T_561; // @[ToAXI4.scala 255:17]
  wire  _T_563; // @[ToAXI4.scala 255:22]
  wire  _T_565; // @[ToAXI4.scala 255:16]
  wire  _T_566; // @[ToAXI4.scala 255:16]
  wire  _T_567; // @[ToAXI4.scala 256:17]
  wire  _T_569; // @[ToAXI4.scala 256:22]
  wire  _T_571; // @[ToAXI4.scala 256:16]
  wire  _T_572; // @[ToAXI4.scala 256:16]
  SiFive_TLMonitor_32_assert TLMonitor ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_clock),
    .reset(TLMonitor_reset),
    .io_in_a_ready(TLMonitor_io_in_a_ready),
    .io_in_a_valid(TLMonitor_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_io_in_a_bits_corrupt),
    .io_in_d_ready(TLMonitor_io_in_d_ready),
    .io_in_d_valid(TLMonitor_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_io_in_d_bits_opcode),
    .io_in_d_bits_size(TLMonitor_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_io_in_d_bits_source),
    .io_in_d_bits_denied(TLMonitor_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_io_in_d_bits_corrupt)
  );
  assign _T_551 = ~_T_549; // @[ToAXI4.scala 249:26]
  assign _T_523 = ~_T_521; // @[ToAXI4.scala 249:26]
  assign _T_495 = ~_T_493; // @[ToAXI4.scala 249:26]
  assign _T_467 = ~_T_465; // @[ToAXI4.scala 249:26]
  assign _T_439 = ~_T_437; // @[ToAXI4.scala 249:26]
  assign _T_411 = ~_T_409; // @[ToAXI4.scala 249:26]
  assign _T_383 = ~_T_381; // @[ToAXI4.scala 249:26]
  assign _T_355 = ~_T_353; // @[ToAXI4.scala 249:26]
  assign _T_327 = ~_T_325; // @[ToAXI4.scala 249:26]
  assign _T_299 = ~_T_297; // @[ToAXI4.scala 249:26]
  assign _T_271 = ~_T_269; // @[ToAXI4.scala 249:26]
  assign _T_243 = ~_T_241; // @[ToAXI4.scala 249:26]
  assign _T_215 = ~_T_213; // @[ToAXI4.scala 249:26]
  assign _T_187 = ~_T_185; // @[ToAXI4.scala 249:26]
  assign _T_159 = ~_T_157; // @[ToAXI4.scala 249:26]
  assign _T_131 = ~_T_129; // @[ToAXI4.scala 249:26]
  assign _T_141 = ~_T_136; // @[ToAXI4.scala 255:17]
  assign _T_143 = _T_141 | _T_129; // @[ToAXI4.scala 255:22]
  assign _T_145 = _T_143 | reset; // @[ToAXI4.scala 255:16]
  assign _T_146 = ~_T_145; // @[ToAXI4.scala 255:16]
  assign _T_147 = ~_T_133; // @[ToAXI4.scala 256:17]
  assign _T_149 = _T_147 | _T_131; // @[ToAXI4.scala 256:22]
  assign _T_151 = _T_149 | reset; // @[ToAXI4.scala 256:16]
  assign _T_152 = ~_T_151; // @[ToAXI4.scala 256:16]
  assign _T_169 = ~_T_164; // @[ToAXI4.scala 255:17]
  assign _T_171 = _T_169 | _T_157; // @[ToAXI4.scala 255:22]
  assign _T_173 = _T_171 | reset; // @[ToAXI4.scala 255:16]
  assign _T_174 = ~_T_173; // @[ToAXI4.scala 255:16]
  assign _T_175 = ~_T_161; // @[ToAXI4.scala 256:17]
  assign _T_177 = _T_175 | _T_159; // @[ToAXI4.scala 256:22]
  assign _T_179 = _T_177 | reset; // @[ToAXI4.scala 256:16]
  assign _T_180 = ~_T_179; // @[ToAXI4.scala 256:16]
  assign _T_197 = ~_T_192; // @[ToAXI4.scala 255:17]
  assign _T_199 = _T_197 | _T_185; // @[ToAXI4.scala 255:22]
  assign _T_201 = _T_199 | reset; // @[ToAXI4.scala 255:16]
  assign _T_202 = ~_T_201; // @[ToAXI4.scala 255:16]
  assign _T_203 = ~_T_189; // @[ToAXI4.scala 256:17]
  assign _T_205 = _T_203 | _T_187; // @[ToAXI4.scala 256:22]
  assign _T_207 = _T_205 | reset; // @[ToAXI4.scala 256:16]
  assign _T_208 = ~_T_207; // @[ToAXI4.scala 256:16]
  assign _T_225 = ~_T_220; // @[ToAXI4.scala 255:17]
  assign _T_227 = _T_225 | _T_213; // @[ToAXI4.scala 255:22]
  assign _T_229 = _T_227 | reset; // @[ToAXI4.scala 255:16]
  assign _T_230 = ~_T_229; // @[ToAXI4.scala 255:16]
  assign _T_231 = ~_T_217; // @[ToAXI4.scala 256:17]
  assign _T_233 = _T_231 | _T_215; // @[ToAXI4.scala 256:22]
  assign _T_235 = _T_233 | reset; // @[ToAXI4.scala 256:16]
  assign _T_236 = ~_T_235; // @[ToAXI4.scala 256:16]
  assign _T_253 = ~_T_248; // @[ToAXI4.scala 255:17]
  assign _T_255 = _T_253 | _T_241; // @[ToAXI4.scala 255:22]
  assign _T_257 = _T_255 | reset; // @[ToAXI4.scala 255:16]
  assign _T_258 = ~_T_257; // @[ToAXI4.scala 255:16]
  assign _T_259 = ~_T_245; // @[ToAXI4.scala 256:17]
  assign _T_261 = _T_259 | _T_243; // @[ToAXI4.scala 256:22]
  assign _T_263 = _T_261 | reset; // @[ToAXI4.scala 256:16]
  assign _T_264 = ~_T_263; // @[ToAXI4.scala 256:16]
  assign _T_281 = ~_T_276; // @[ToAXI4.scala 255:17]
  assign _T_283 = _T_281 | _T_269; // @[ToAXI4.scala 255:22]
  assign _T_285 = _T_283 | reset; // @[ToAXI4.scala 255:16]
  assign _T_286 = ~_T_285; // @[ToAXI4.scala 255:16]
  assign _T_287 = ~_T_273; // @[ToAXI4.scala 256:17]
  assign _T_289 = _T_287 | _T_271; // @[ToAXI4.scala 256:22]
  assign _T_291 = _T_289 | reset; // @[ToAXI4.scala 256:16]
  assign _T_292 = ~_T_291; // @[ToAXI4.scala 256:16]
  assign _T_309 = ~_T_304; // @[ToAXI4.scala 255:17]
  assign _T_311 = _T_309 | _T_297; // @[ToAXI4.scala 255:22]
  assign _T_313 = _T_311 | reset; // @[ToAXI4.scala 255:16]
  assign _T_314 = ~_T_313; // @[ToAXI4.scala 255:16]
  assign _T_315 = ~_T_301; // @[ToAXI4.scala 256:17]
  assign _T_317 = _T_315 | _T_299; // @[ToAXI4.scala 256:22]
  assign _T_319 = _T_317 | reset; // @[ToAXI4.scala 256:16]
  assign _T_320 = ~_T_319; // @[ToAXI4.scala 256:16]
  assign _T_337 = ~_T_332; // @[ToAXI4.scala 255:17]
  assign _T_339 = _T_337 | _T_325; // @[ToAXI4.scala 255:22]
  assign _T_341 = _T_339 | reset; // @[ToAXI4.scala 255:16]
  assign _T_342 = ~_T_341; // @[ToAXI4.scala 255:16]
  assign _T_343 = ~_T_329; // @[ToAXI4.scala 256:17]
  assign _T_345 = _T_343 | _T_327; // @[ToAXI4.scala 256:22]
  assign _T_347 = _T_345 | reset; // @[ToAXI4.scala 256:16]
  assign _T_348 = ~_T_347; // @[ToAXI4.scala 256:16]
  assign _T_365 = ~_T_360; // @[ToAXI4.scala 255:17]
  assign _T_367 = _T_365 | _T_353; // @[ToAXI4.scala 255:22]
  assign _T_369 = _T_367 | reset; // @[ToAXI4.scala 255:16]
  assign _T_370 = ~_T_369; // @[ToAXI4.scala 255:16]
  assign _T_371 = ~_T_357; // @[ToAXI4.scala 256:17]
  assign _T_373 = _T_371 | _T_355; // @[ToAXI4.scala 256:22]
  assign _T_375 = _T_373 | reset; // @[ToAXI4.scala 256:16]
  assign _T_376 = ~_T_375; // @[ToAXI4.scala 256:16]
  assign _T_393 = ~_T_388; // @[ToAXI4.scala 255:17]
  assign _T_395 = _T_393 | _T_381; // @[ToAXI4.scala 255:22]
  assign _T_397 = _T_395 | reset; // @[ToAXI4.scala 255:16]
  assign _T_398 = ~_T_397; // @[ToAXI4.scala 255:16]
  assign _T_399 = ~_T_385; // @[ToAXI4.scala 256:17]
  assign _T_401 = _T_399 | _T_383; // @[ToAXI4.scala 256:22]
  assign _T_403 = _T_401 | reset; // @[ToAXI4.scala 256:16]
  assign _T_404 = ~_T_403; // @[ToAXI4.scala 256:16]
  assign _T_421 = ~_T_416; // @[ToAXI4.scala 255:17]
  assign _T_423 = _T_421 | _T_409; // @[ToAXI4.scala 255:22]
  assign _T_425 = _T_423 | reset; // @[ToAXI4.scala 255:16]
  assign _T_426 = ~_T_425; // @[ToAXI4.scala 255:16]
  assign _T_427 = ~_T_413; // @[ToAXI4.scala 256:17]
  assign _T_429 = _T_427 | _T_411; // @[ToAXI4.scala 256:22]
  assign _T_431 = _T_429 | reset; // @[ToAXI4.scala 256:16]
  assign _T_432 = ~_T_431; // @[ToAXI4.scala 256:16]
  assign _T_449 = ~_T_444; // @[ToAXI4.scala 255:17]
  assign _T_451 = _T_449 | _T_437; // @[ToAXI4.scala 255:22]
  assign _T_453 = _T_451 | reset; // @[ToAXI4.scala 255:16]
  assign _T_454 = ~_T_453; // @[ToAXI4.scala 255:16]
  assign _T_455 = ~_T_441; // @[ToAXI4.scala 256:17]
  assign _T_457 = _T_455 | _T_439; // @[ToAXI4.scala 256:22]
  assign _T_459 = _T_457 | reset; // @[ToAXI4.scala 256:16]
  assign _T_460 = ~_T_459; // @[ToAXI4.scala 256:16]
  assign _T_477 = ~_T_472; // @[ToAXI4.scala 255:17]
  assign _T_479 = _T_477 | _T_465; // @[ToAXI4.scala 255:22]
  assign _T_481 = _T_479 | reset; // @[ToAXI4.scala 255:16]
  assign _T_482 = ~_T_481; // @[ToAXI4.scala 255:16]
  assign _T_483 = ~_T_469; // @[ToAXI4.scala 256:17]
  assign _T_485 = _T_483 | _T_467; // @[ToAXI4.scala 256:22]
  assign _T_487 = _T_485 | reset; // @[ToAXI4.scala 256:16]
  assign _T_488 = ~_T_487; // @[ToAXI4.scala 256:16]
  assign _T_505 = ~_T_500; // @[ToAXI4.scala 255:17]
  assign _T_507 = _T_505 | _T_493; // @[ToAXI4.scala 255:22]
  assign _T_509 = _T_507 | reset; // @[ToAXI4.scala 255:16]
  assign _T_510 = ~_T_509; // @[ToAXI4.scala 255:16]
  assign _T_511 = ~_T_497; // @[ToAXI4.scala 256:17]
  assign _T_513 = _T_511 | _T_495; // @[ToAXI4.scala 256:22]
  assign _T_515 = _T_513 | reset; // @[ToAXI4.scala 256:16]
  assign _T_516 = ~_T_515; // @[ToAXI4.scala 256:16]
  assign _T_533 = ~_T_528; // @[ToAXI4.scala 255:17]
  assign _T_535 = _T_533 | _T_521; // @[ToAXI4.scala 255:22]
  assign _T_537 = _T_535 | reset; // @[ToAXI4.scala 255:16]
  assign _T_538 = ~_T_537; // @[ToAXI4.scala 255:16]
  assign _T_539 = ~_T_525; // @[ToAXI4.scala 256:17]
  assign _T_541 = _T_539 | _T_523; // @[ToAXI4.scala 256:22]
  assign _T_543 = _T_541 | reset; // @[ToAXI4.scala 256:16]
  assign _T_544 = ~_T_543; // @[ToAXI4.scala 256:16]
  assign _T_561 = ~_T_556; // @[ToAXI4.scala 255:17]
  assign _T_563 = _T_561 | _T_549; // @[ToAXI4.scala 255:22]
  assign _T_565 = _T_563 | reset; // @[ToAXI4.scala 255:16]
  assign _T_566 = ~_T_565; // @[ToAXI4.scala 255:16]
  assign _T_567 = ~_T_553; // @[ToAXI4.scala 256:17]
  assign _T_569 = _T_567 | _T_551; // @[ToAXI4.scala 256:22]
  assign _T_571 = _T_569 | reset; // @[ToAXI4.scala 256:16]
  assign _T_572 = ~_T_571; // @[ToAXI4.scala 256:16]
  assign TLMonitor_clock = clock;
  assign TLMonitor_reset = reset;
  assign TLMonitor_io_in_a_ready = _T_55 & _T_58; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_valid = auto_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_size = auto_in_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_source = auto_in_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_ready = auto_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_valid = _T_74 ? auto_out_r_valid : auto_out_b_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_opcode = _T_74 ? 3'h1 : 3'h0; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_size = _T_74 ? _T_86_size : _T_87_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_source = _T_74 ? auto_out_r_bits_echo_tl_state_source : auto_out_b_bits_echo_tl_state_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_denied = _T_74 ? _GEN_36 : _T_84; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_corrupt = _T_74 & _T_85; // @[Nodes.scala 26:19]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_146) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_146) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_152) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_152) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_174) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_174) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_180) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_180) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_202) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_202) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_208) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_208) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_230) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_230) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_236) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_236) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_258) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_258) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_264) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_264) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_286) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_286) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_292) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_292) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_314) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_314) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_320) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_320) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_342) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_342) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_348) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_348) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_370) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_370) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_376) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_376) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_398) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_398) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_404) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_404) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_426) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_426) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_432) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_432) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_454) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_454) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_460) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_460) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_482) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_482) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_488) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_488) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_510) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_510) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_516) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_516) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_538) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_538) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_544) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_544) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_566) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_566) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_572) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_572) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
