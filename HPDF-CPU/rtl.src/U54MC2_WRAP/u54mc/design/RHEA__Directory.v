//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__Directory(
  input         rf_reset,
  input         clock,
  input         reset,
  output        io_write_ready,
  input         io_write_valid,
  input  [9:0]  io_write_bits_set,
  input  [2:0]  io_write_bits_way,
  input         io_write_bits_data_dirty,
  input  [1:0]  io_write_bits_data_state,
  input  [3:0]  io_write_bits_data_clients,
  input  [17:0] io_write_bits_data_tag,
  input         io_read_valid,
  input  [6:0]  io_read_bits_source,
  input  [9:0]  io_read_bits_set,
  input  [17:0] io_read_bits_tag,
  output        io_result_bits_dirty,
  output [1:0]  io_result_bits_state,
  output [3:0]  io_result_bits_clients,
  output [17:0] io_result_bits_tag,
  output        io_result_bits_hit,
  output [2:0]  io_result_bits_way,
  output        io_ready,
  input  [7:0]  io_ways_0,
  input  [7:0]  io_ways_1,
  input  [7:0]  io_ways_2,
  input  [7:0]  io_ways_3,
  input  [7:0]  io_ways_4,
  input  [7:0]  io_ways_5,
  input  [7:0]  io_ways_6,
  input  [7:0]  io_ways_7,
  input  [7:0]  io_ways_8,
  input  [7:0]  io_ways_9,
  input  [7:0]  io_ways_10,
  input  [7:0]  io_ways_11,
  input  [7:0]  io_ways_12,
  input  [7:0]  io_ways_13,
  input  [10:0] io_divs_0,
  input  [10:0] io_divs_1,
  input  [10:0] io_divs_2,
  input  [10:0] io_divs_3,
  input  [10:0] io_divs_4,
  input  [10:0] io_divs_5,
  input  [10:0] io_divs_6,
  input  [10:0] io_divs_7,
  input  [10:0] io_divs_8,
  input  [10:0] io_divs_9,
  input  [10:0] io_divs_10,
  input  [10:0] io_divs_11,
  input  [10:0] io_divs_12,
  input  [10:0] io_divs_13,
  output        io_corrected_valid,
  output        io_error_ready,
  input         io_error_valid,
  input  [7:0]  io_error_bits_bit
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
`endif // RANDOMIZE_REG_INIT
  wire [9:0] cc_dir_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_dir_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_dir_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_dir_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_wdata_0; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_wdata_1; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_wdata_2; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_wdata_3; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_wdata_4; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_wdata_5; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_wdata_6; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_wdata_7; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_rdata_0; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_rdata_1; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_rdata_2; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_rdata_3; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_rdata_4; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_rdata_5; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_rdata_6; // @[DescribedSRAM.scala 18:26]
  wire [24:0] cc_dir_RW0_rdata_7; // @[DescribedSRAM.scala 18:26]
  wire  cc_dir_RW0_wmask_0; // @[DescribedSRAM.scala 18:26]
  wire  cc_dir_RW0_wmask_1; // @[DescribedSRAM.scala 18:26]
  wire  cc_dir_RW0_wmask_2; // @[DescribedSRAM.scala 18:26]
  wire  cc_dir_RW0_wmask_3; // @[DescribedSRAM.scala 18:26]
  wire  cc_dir_RW0_wmask_4; // @[DescribedSRAM.scala 18:26]
  wire  cc_dir_RW0_wmask_5; // @[DescribedSRAM.scala 18:26]
  wire  cc_dir_RW0_wmask_6; // @[DescribedSRAM.scala 18:26]
  wire  cc_dir_RW0_wmask_7; // @[DescribedSRAM.scala 18:26]
  wire  write_rf_reset; // @[Decoupled.scala 296:21]
  wire  write_clock; // @[Decoupled.scala 296:21]
  wire  write_reset; // @[Decoupled.scala 296:21]
  wire  write_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  write_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [9:0] write_io_enq_bits_set; // @[Decoupled.scala 296:21]
  wire [2:0] write_io_enq_bits_way; // @[Decoupled.scala 296:21]
  wire  write_io_enq_bits_data_dirty; // @[Decoupled.scala 296:21]
  wire [1:0] write_io_enq_bits_data_state; // @[Decoupled.scala 296:21]
  wire [3:0] write_io_enq_bits_data_clients; // @[Decoupled.scala 296:21]
  wire [17:0] write_io_enq_bits_data_tag; // @[Decoupled.scala 296:21]
  wire  write_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  write_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [9:0] write_io_deq_bits_set; // @[Decoupled.scala 296:21]
  wire [2:0] write_io_deq_bits_way; // @[Decoupled.scala 296:21]
  wire  write_io_deq_bits_data_dirty; // @[Decoupled.scala 296:21]
  wire [1:0] write_io_deq_bits_data_state; // @[Decoupled.scala 296:21]
  wire [3:0] write_io_deq_bits_data_clients; // @[Decoupled.scala 296:21]
  wire [17:0] write_io_deq_bits_data_tag; // @[Decoupled.scala 296:21]
  wire  victimLFSR_prng_clock; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_reset; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_increment; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_0; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_1; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_2; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_3; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_4; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_5; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_6; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_7; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_8; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_9; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_10; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_11; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_12; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_13; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_14; // @[PRNG.scala 82:22]
  wire  victimLFSR_prng_io_out_15; // @[PRNG.scala 82:22]
  reg [10:0] wipeCount; // @[Directory.scala 103:26]
  reg  wipeOff; // @[Directory.scala 104:24]
  wire  wipeDone = wipeCount[10]; // @[Directory.scala 105:27]
  wire [9:0] wipeSet = wipeCount[9:0]; // @[Directory.scala 106:26]
  wire  _T = ~wipeDone; // @[Directory.scala 109:9]
  wire  _T_2 = ~wipeDone & ~wipeOff; // @[Directory.scala 109:19]
  wire [10:0] _wipeCount_T_1 = wipeCount + 11'h1; // @[Directory.scala 109:57]
  wire  _T_3 = ~io_read_valid; // @[Directory.scala 110:23]
  wire  wen = _T_2 | write_io_deq_valid; // @[Directory.scala 114:37]
  wire [255:0] _error_T = 256'h1 << io_error_bits_bit; // @[Directory.scala 118:40]
  wire [24:0] error = io_error_valid ? _error_T[24:0] : 25'h0; // @[Directory.scala 118:18]
  wire  _T_14 = _T_3 & wen; // @[Directory.scala 122:14]
  wire [9:0] _T_15 = write_io_deq_bits_set; // @[Directory.scala 125:35]
  wire [9:0] _T_16 = wipeDone ? _T_15 : wipeSet; // @[Directory.scala 125:21]
  wire [24:0] _T_26 = {write_io_deq_bits_data_dirty,write_io_deq_bits_data_state,write_io_deq_bits_data_clients,
    write_io_deq_bits_data_tag}; // @[Directory.scala 128:89]
  wire [24:0] _T_27 = wipeDone ? _T_26 : 25'h0; // @[Directory.scala 128:62]
  wire [2:0] shiftAmount = write_io_deq_bits_way; // @[OneHot.scala 64:49]
  wire [7:0] _T_51 = 8'h1 << shiftAmount; // @[OneHot.scala 65:12]
  wire  sourceSelect_0 = io_read_bits_source == 7'h24; // @[Parameters.scala 46:9]
  wire  sourceSelect_1 = io_read_bits_source[6:3] == 4'h0; // @[Parameters.scala 54:32]
  wire  sourceSelect_2 = io_read_bits_source[6:3] == 4'h1; // @[Parameters.scala 54:32]
  wire  sourceSelect_3 = io_read_bits_source[6:3] == 4'h2; // @[Parameters.scala 54:32]
  wire  sourceSelect_4 = io_read_bits_source[6:3] == 4'h3; // @[Parameters.scala 54:32]
  wire  sourceSelect_5 = io_read_bits_source[6:2] == 5'h8; // @[Parameters.scala 54:32]
  wire  sourceSelect_6 = io_read_bits_source == 7'h60; // @[Parameters.scala 46:9]
  wire  sourceSelect_7 = io_read_bits_source == 7'h61; // @[Parameters.scala 46:9]
  wire [3:0] sourceSelect_uncommonBits_5 = io_read_bits_source[3:0]; // @[Parameters.scala 52:64]
  wire  _sourceSelect_T_27 = 4'h2 <= sourceSelect_uncommonBits_5; // @[Parameters.scala 56:34]
  wire  _sourceSelect_T_28 = io_read_bits_source[6:4] == 3'h6 & _sourceSelect_T_27; // @[Parameters.scala 54:69]
  wire  _sourceSelect_T_29 = sourceSelect_uncommonBits_5 <= 4'h8; // @[Parameters.scala 57:20]
  wire  sourceSelect_8 = _sourceSelect_T_28 & _sourceSelect_T_29; // @[Parameters.scala 56:50]
  wire  sourceSelect_9 = io_read_bits_source[6:1] == 6'h38; // @[Parameters.scala 54:32]
  wire  sourceSelect_10 = io_read_bits_source == 7'h40; // @[Parameters.scala 46:9]
  wire  sourceSelect_11 = io_read_bits_source == 7'h41; // @[Parameters.scala 46:9]
  wire  _sourceSelect_T_38 = io_read_bits_source[6:4] == 3'h4 & _sourceSelect_T_27; // @[Parameters.scala 54:69]
  wire  sourceSelect_12 = _sourceSelect_T_38 & _sourceSelect_T_29; // @[Parameters.scala 56:50]
  wire  sourceSelect_13 = io_read_bits_source[6:1] == 6'h28; // @[Parameters.scala 54:32]
  wire [7:0] _sourceWays_T = sourceSelect_0 ? io_ways_0 : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_1 = sourceSelect_1 ? io_ways_1 : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_2 = sourceSelect_2 ? io_ways_2 : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_3 = sourceSelect_3 ? io_ways_3 : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_4 = sourceSelect_4 ? io_ways_4 : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_5 = sourceSelect_5 ? io_ways_5 : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_6 = sourceSelect_6 ? io_ways_6 : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_7 = sourceSelect_7 ? io_ways_7 : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_8 = sourceSelect_8 ? io_ways_8 : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_9 = sourceSelect_9 ? io_ways_9 : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_10 = sourceSelect_10 ? io_ways_10 : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_11 = sourceSelect_11 ? io_ways_11 : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_12 = sourceSelect_12 ? io_ways_12 : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_13 = sourceSelect_13 ? io_ways_13 : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_14 = _sourceWays_T | _sourceWays_T_1; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_15 = _sourceWays_T_14 | _sourceWays_T_2; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_16 = _sourceWays_T_15 | _sourceWays_T_3; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_17 = _sourceWays_T_16 | _sourceWays_T_4; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_18 = _sourceWays_T_17 | _sourceWays_T_5; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_19 = _sourceWays_T_18 | _sourceWays_T_6; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_20 = _sourceWays_T_19 | _sourceWays_T_7; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_21 = _sourceWays_T_20 | _sourceWays_T_8; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_22 = _sourceWays_T_21 | _sourceWays_T_9; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_23 = _sourceWays_T_22 | _sourceWays_T_10; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_24 = _sourceWays_T_23 | _sourceWays_T_11; // @[Mux.scala 27:72]
  wire [7:0] _sourceWays_T_25 = _sourceWays_T_24 | _sourceWays_T_12; // @[Mux.scala 27:72]
  wire [7:0] sourceWays = _sourceWays_T_25 | _sourceWays_T_13; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T = sourceSelect_0 ? io_divs_0 : 11'h0; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_1 = sourceSelect_1 ? io_divs_1 : 11'h0; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_2 = sourceSelect_2 ? io_divs_2 : 11'h0; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_3 = sourceSelect_3 ? io_divs_3 : 11'h0; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_4 = sourceSelect_4 ? io_divs_4 : 11'h0; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_5 = sourceSelect_5 ? io_divs_5 : 11'h0; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_6 = sourceSelect_6 ? io_divs_6 : 11'h0; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_7 = sourceSelect_7 ? io_divs_7 : 11'h0; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_8 = sourceSelect_8 ? io_divs_8 : 11'h0; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_9 = sourceSelect_9 ? io_divs_9 : 11'h0; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_10 = sourceSelect_10 ? io_divs_10 : 11'h0; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_11 = sourceSelect_11 ? io_divs_11 : 11'h0; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_12 = sourceSelect_12 ? io_divs_12 : 11'h0; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_13 = sourceSelect_13 ? io_divs_13 : 11'h0; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_14 = _sourceDiv_T | _sourceDiv_T_1; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_15 = _sourceDiv_T_14 | _sourceDiv_T_2; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_16 = _sourceDiv_T_15 | _sourceDiv_T_3; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_17 = _sourceDiv_T_16 | _sourceDiv_T_4; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_18 = _sourceDiv_T_17 | _sourceDiv_T_5; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_19 = _sourceDiv_T_18 | _sourceDiv_T_6; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_20 = _sourceDiv_T_19 | _sourceDiv_T_7; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_21 = _sourceDiv_T_20 | _sourceDiv_T_8; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_22 = _sourceDiv_T_21 | _sourceDiv_T_9; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_23 = _sourceDiv_T_22 | _sourceDiv_T_10; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_24 = _sourceDiv_T_23 | _sourceDiv_T_11; // @[Mux.scala 27:72]
  wire [10:0] _sourceDiv_T_25 = _sourceDiv_T_24 | _sourceDiv_T_12; // @[Mux.scala 27:72]
  wire [10:0] sourceDiv = _sourceDiv_T_25 | _sourceDiv_T_13; // @[Mux.scala 27:72]
  reg [17:0] tag; // @[Reg.scala 15:16]
  reg [9:0] set; // @[Reg.scala 15:16]
  reg [7:0] victimWays; // @[Reg.scala 15:16]
  reg [10:0] victimDiv; // @[Reg.scala 15:16]
  wire [7:0] victimLFSR_lo = {victimLFSR_prng_io_out_7,victimLFSR_prng_io_out_6,victimLFSR_prng_io_out_5,
    victimLFSR_prng_io_out_4,victimLFSR_prng_io_out_3,victimLFSR_prng_io_out_2,victimLFSR_prng_io_out_1,
    victimLFSR_prng_io_out_0}; // @[PRNG.scala 86:17]
  wire [15:0] _victimLFSR_T = {victimLFSR_prng_io_out_15,victimLFSR_prng_io_out_14,victimLFSR_prng_io_out_13,
    victimLFSR_prng_io_out_12,victimLFSR_prng_io_out_11,victimLFSR_prng_io_out_10,victimLFSR_prng_io_out_9,
    victimLFSR_prng_io_out_8,victimLFSR_lo}; // @[PRNG.scala 86:17]
  wire [9:0] victimLFSR = _victimLFSR_T[9:0]; // @[Directory.scala 158:48]
  wire  _victimSums_T = victimWays[0]; // @[Directory.scala 159:31]
  wire  _victimSums_T_1 = victimWays[1]; // @[Directory.scala 159:31]
  wire  _victimSums_T_2 = victimWays[2]; // @[Directory.scala 159:31]
  wire  _victimSums_T_3 = victimWays[3]; // @[Directory.scala 159:31]
  wire  _victimSums_T_4 = victimWays[4]; // @[Directory.scala 159:31]
  wire  _victimSums_T_5 = victimWays[5]; // @[Directory.scala 159:31]
  wire  _victimSums_T_6 = victimWays[6]; // @[Directory.scala 159:31]
  wire  _victimSums_T_7 = victimWays[7]; // @[Directory.scala 159:31]
  wire [10:0] _victimSums_T_8 = victimWays[0] ? victimDiv : 11'h0; // @[Directory.scala 159:81]
  wire [11:0] _victimSums_T_9 = {{1'd0}, _victimSums_T_8}; // @[Directory.scala 159:76]
  wire [10:0] victimSums_1 = _victimSums_T_9[10:0]; // @[Directory.scala 159:76]
  wire [10:0] _victimSums_T_10 = victimWays[1] ? victimDiv : 11'h0; // @[Directory.scala 159:81]
  wire [10:0] victimSums_2 = victimSums_1 + _victimSums_T_10; // @[Directory.scala 159:76]
  wire [10:0] _victimSums_T_12 = victimWays[2] ? victimDiv : 11'h0; // @[Directory.scala 159:81]
  wire [10:0] victimSums_3 = victimSums_2 + _victimSums_T_12; // @[Directory.scala 159:76]
  wire [10:0] _victimSums_T_14 = victimWays[3] ? victimDiv : 11'h0; // @[Directory.scala 159:81]
  wire [10:0] victimSums_4 = victimSums_3 + _victimSums_T_14; // @[Directory.scala 159:76]
  wire [10:0] _victimSums_T_16 = victimWays[4] ? victimDiv : 11'h0; // @[Directory.scala 159:81]
  wire [10:0] victimSums_5 = victimSums_4 + _victimSums_T_16; // @[Directory.scala 159:76]
  wire [10:0] _victimSums_T_18 = victimWays[5] ? victimDiv : 11'h0; // @[Directory.scala 159:81]
  wire [10:0] victimSums_6 = victimSums_5 + _victimSums_T_18; // @[Directory.scala 159:76]
  wire [10:0] _victimSums_T_20 = victimWays[6] ? victimDiv : 11'h0; // @[Directory.scala 159:81]
  wire [10:0] victimSums_7 = victimSums_6 + _victimSums_T_20; // @[Directory.scala 159:76]
  wire [10:0] _victimSums_T_22 = victimWays[7] ? victimDiv : 11'h0; // @[Directory.scala 159:81]
  wire [10:0] victimSums_8 = victimSums_7 + _victimSums_T_22; // @[Directory.scala 159:76]
  wire [10:0] _GEN_20 = {{1'd0}, victimLFSR}; // @[Directory.scala 160:81]
  wire  victimLTE_lo_lo_hi = victimSums_1 <= _GEN_20; // @[Directory.scala 160:81]
  wire  victimLTE_lo_hi_lo = victimSums_2 <= _GEN_20; // @[Directory.scala 160:81]
  wire  victimLTE_lo_hi_hi = victimSums_3 <= _GEN_20; // @[Directory.scala 160:81]
  wire  victimLTE_hi_lo_lo = victimSums_4 <= _GEN_20; // @[Directory.scala 160:81]
  wire  victimLTE_hi_lo_hi = victimSums_5 <= _GEN_20; // @[Directory.scala 160:81]
  wire  victimLTE_hi_hi_lo = victimSums_6 <= _GEN_20; // @[Directory.scala 160:81]
  wire  victimLTE_hi_hi_hi_lo = victimSums_7 <= _GEN_20; // @[Directory.scala 160:81]
  wire  victimLTE_hi_hi_hi_hi = victimSums_8 <= _GEN_20; // @[Directory.scala 160:81]
  wire [8:0] victimLTE = {victimLTE_hi_hi_hi_hi,victimLTE_hi_hi_hi_lo,victimLTE_hi_hi_lo,victimLTE_hi_lo_hi,
    victimLTE_hi_lo_lo,victimLTE_lo_hi_hi,victimLTE_lo_hi_lo,victimLTE_lo_lo_hi,1'h1}; // @[Cat.scala 30:58]
  wire [6:0] victimSimp_hi_lo = victimLTE[7:1]; // @[Directory.scala 161:51]
  wire [8:0] victimSimp = {1'h0,victimSimp_hi_lo,1'h1}; // @[Cat.scala 30:58]
  wire [7:0] _victimWayOH_T_1 = victimSimp[8:1]; // @[Directory.scala 162:70]
  wire [7:0] _victimWayOH_T_2 = ~victimSimp[8:1]; // @[Directory.scala 162:57]
  wire [7:0] victimWayOH = victimSimp[7:0] & _victimWayOH_T_2; // @[Directory.scala 162:55]
  wire [3:0] victimWay_hi = victimWayOH[7:4]; // @[OneHot.scala 30:18]
  wire [3:0] victimWay_lo = victimWayOH[3:0]; // @[OneHot.scala 31:18]
  wire  victimWay_hi_1 = |victimWay_hi; // @[OneHot.scala 32:14]
  wire [3:0] _victimWay_T = victimWay_hi | victimWay_lo; // @[OneHot.scala 32:28]
  wire [1:0] victimWay_hi_2 = _victimWay_T[3:2]; // @[OneHot.scala 30:18]
  wire [1:0] victimWay_lo_1 = _victimWay_T[1:0]; // @[OneHot.scala 31:18]
  wire  victimWay_hi_3 = |victimWay_hi_2; // @[OneHot.scala 32:14]
  wire [1:0] _victimWay_T_1 = victimWay_hi_2 | victimWay_lo_1; // @[OneHot.scala 32:28]
  wire  victimWay_lo_2 = _victimWay_T_1[1]; // @[CircuitMath.scala 30:8]
  wire [2:0] victimWay = {victimWay_hi_1,victimWay_hi_3,victimWay_lo_2}; // @[Cat.scala 30:58]
  wire  _T_111 = victimWayOH[0]; // @[Bitwise.scala 49:65]
  wire  _T_112 = victimWayOH[1]; // @[Bitwise.scala 49:65]
  wire  _T_113 = victimWayOH[2]; // @[Bitwise.scala 49:65]
  wire  _T_114 = victimWayOH[3]; // @[Bitwise.scala 49:65]
  wire  _T_115 = victimWayOH[4]; // @[Bitwise.scala 49:65]
  wire  _T_116 = victimWayOH[5]; // @[Bitwise.scala 49:65]
  wire  _T_117 = victimWayOH[6]; // @[Bitwise.scala 49:65]
  wire  _T_118 = victimWayOH[7]; // @[Bitwise.scala 49:65]
  wire  setQuash = write_io_deq_valid & write_io_deq_bits_set == set; // @[Directory.scala 175:31]
  wire  tagMatch = write_io_deq_bits_data_tag == tag; // @[Directory.scala 176:34]
  wire  wayMatch = write_io_deq_bits_way == victimWay; // @[Directory.scala 177:29]
  wire [24:0] _ways_WIRE_1 = cc_dir_RW0_rdata_0;
  wire [17:0] ways_0_tag = _ways_WIRE_1[17:0]; // @[Directory.scala 180:71]
  wire [3:0] ways_0_clients = _ways_WIRE_1[21:18]; // @[Directory.scala 180:71]
  wire [1:0] ways_0_state = _ways_WIRE_1[23:22]; // @[Directory.scala 180:71]
  wire  ways_0_dirty = _ways_WIRE_1[24]; // @[Directory.scala 180:71]
  wire [24:0] _ways_WIRE_3 = cc_dir_RW0_rdata_1;
  wire [17:0] ways_1_tag = _ways_WIRE_3[17:0]; // @[Directory.scala 180:71]
  wire [3:0] ways_1_clients = _ways_WIRE_3[21:18]; // @[Directory.scala 180:71]
  wire [1:0] ways_1_state = _ways_WIRE_3[23:22]; // @[Directory.scala 180:71]
  wire  ways_1_dirty = _ways_WIRE_3[24]; // @[Directory.scala 180:71]
  wire [24:0] _ways_WIRE_5 = cc_dir_RW0_rdata_2;
  wire [17:0] ways_2_tag = _ways_WIRE_5[17:0]; // @[Directory.scala 180:71]
  wire [3:0] ways_2_clients = _ways_WIRE_5[21:18]; // @[Directory.scala 180:71]
  wire [1:0] ways_2_state = _ways_WIRE_5[23:22]; // @[Directory.scala 180:71]
  wire  ways_2_dirty = _ways_WIRE_5[24]; // @[Directory.scala 180:71]
  wire [24:0] _ways_WIRE_7 = cc_dir_RW0_rdata_3;
  wire [17:0] ways_3_tag = _ways_WIRE_7[17:0]; // @[Directory.scala 180:71]
  wire [3:0] ways_3_clients = _ways_WIRE_7[21:18]; // @[Directory.scala 180:71]
  wire [1:0] ways_3_state = _ways_WIRE_7[23:22]; // @[Directory.scala 180:71]
  wire  ways_3_dirty = _ways_WIRE_7[24]; // @[Directory.scala 180:71]
  wire [24:0] _ways_WIRE_9 = cc_dir_RW0_rdata_4;
  wire [17:0] ways_4_tag = _ways_WIRE_9[17:0]; // @[Directory.scala 180:71]
  wire [3:0] ways_4_clients = _ways_WIRE_9[21:18]; // @[Directory.scala 180:71]
  wire [1:0] ways_4_state = _ways_WIRE_9[23:22]; // @[Directory.scala 180:71]
  wire  ways_4_dirty = _ways_WIRE_9[24]; // @[Directory.scala 180:71]
  wire [24:0] _ways_WIRE_11 = cc_dir_RW0_rdata_5;
  wire [17:0] ways_5_tag = _ways_WIRE_11[17:0]; // @[Directory.scala 180:71]
  wire [3:0] ways_5_clients = _ways_WIRE_11[21:18]; // @[Directory.scala 180:71]
  wire [1:0] ways_5_state = _ways_WIRE_11[23:22]; // @[Directory.scala 180:71]
  wire  ways_5_dirty = _ways_WIRE_11[24]; // @[Directory.scala 180:71]
  wire [24:0] _ways_WIRE_13 = cc_dir_RW0_rdata_6;
  wire [17:0] ways_6_tag = _ways_WIRE_13[17:0]; // @[Directory.scala 180:71]
  wire [3:0] ways_6_clients = _ways_WIRE_13[21:18]; // @[Directory.scala 180:71]
  wire [1:0] ways_6_state = _ways_WIRE_13[23:22]; // @[Directory.scala 180:71]
  wire  ways_6_dirty = _ways_WIRE_13[24]; // @[Directory.scala 180:71]
  wire [24:0] _ways_WIRE_15 = cc_dir_RW0_rdata_7;
  wire [17:0] ways_7_tag = _ways_WIRE_15[17:0]; // @[Directory.scala 180:71]
  wire [3:0] ways_7_clients = _ways_WIRE_15[21:18]; // @[Directory.scala 180:71]
  wire [1:0] ways_7_state = _ways_WIRE_15[23:22]; // @[Directory.scala 180:71]
  wire  ways_7_dirty = _ways_WIRE_15[24]; // @[Directory.scala 180:71]
  wire  hits_lo_lo_lo = ways_0_tag == tag & ways_0_state != 2'h0 & (~setQuash | 3'h0 != write_io_deq_bits_way); // @[Directory.scala 182:42]
  wire  hits_lo_lo_hi = ways_1_tag == tag & ways_1_state != 2'h0 & (~setQuash | 3'h1 != write_io_deq_bits_way); // @[Directory.scala 182:42]
  wire  hits_lo_hi_lo = ways_2_tag == tag & ways_2_state != 2'h0 & (~setQuash | 3'h2 != write_io_deq_bits_way); // @[Directory.scala 182:42]
  wire  hits_lo_hi_hi = ways_3_tag == tag & ways_3_state != 2'h0 & (~setQuash | 3'h3 != write_io_deq_bits_way); // @[Directory.scala 182:42]
  wire  hits_hi_lo_lo = ways_4_tag == tag & ways_4_state != 2'h0 & (~setQuash | 3'h4 != write_io_deq_bits_way); // @[Directory.scala 182:42]
  wire  hits_hi_lo_hi = ways_5_tag == tag & ways_5_state != 2'h0 & (~setQuash | 3'h5 != write_io_deq_bits_way); // @[Directory.scala 182:42]
  wire  hits_hi_hi_lo = ways_6_tag == tag & ways_6_state != 2'h0 & (~setQuash | 3'h6 != write_io_deq_bits_way); // @[Directory.scala 182:42]
  wire  hits_hi_hi_hi = ways_7_tag == tag & ways_7_state != 2'h0 & (~setQuash | 3'h7 != write_io_deq_bits_way); // @[Directory.scala 182:42]
  wire [7:0] hits = {hits_hi_hi_hi,hits_hi_hi_lo,hits_hi_lo_hi,hits_hi_lo_lo,hits_lo_hi_hi,hits_lo_hi_lo,hits_lo_lo_hi,
    hits_lo_lo_lo}; // @[Cat.scala 30:58]
  wire  hit = |hits; // @[Directory.scala 184:21]
  wire  _hitOut_T = setQuash & tagMatch; // @[Directory.scala 185:33]
  wire [17:0] _io_result_bits_T_8 = hits[0] ? ways_0_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_9 = hits[1] ? ways_1_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_10 = hits[2] ? ways_2_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_11 = hits[3] ? ways_3_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_12 = hits[4] ? ways_4_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_13 = hits[5] ? ways_5_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_14 = hits[6] ? ways_6_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_15 = hits[7] ? ways_7_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_16 = _io_result_bits_T_8 | _io_result_bits_T_9; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_17 = _io_result_bits_T_16 | _io_result_bits_T_10; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_18 = _io_result_bits_T_17 | _io_result_bits_T_11; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_19 = _io_result_bits_T_18 | _io_result_bits_T_12; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_20 = _io_result_bits_T_19 | _io_result_bits_T_13; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_21 = _io_result_bits_T_20 | _io_result_bits_T_14; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_22 = _io_result_bits_T_21 | _io_result_bits_T_15; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_23 = hits[0] ? ways_0_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_24 = hits[1] ? ways_1_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_25 = hits[2] ? ways_2_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_26 = hits[3] ? ways_3_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_27 = hits[4] ? ways_4_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_28 = hits[5] ? ways_5_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_29 = hits[6] ? ways_6_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_30 = hits[7] ? ways_7_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_31 = _io_result_bits_T_23 | _io_result_bits_T_24; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_32 = _io_result_bits_T_31 | _io_result_bits_T_25; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_33 = _io_result_bits_T_32 | _io_result_bits_T_26; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_34 = _io_result_bits_T_33 | _io_result_bits_T_27; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_35 = _io_result_bits_T_34 | _io_result_bits_T_28; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_36 = _io_result_bits_T_35 | _io_result_bits_T_29; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_37 = _io_result_bits_T_36 | _io_result_bits_T_30; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_38 = hits[0] ? ways_0_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_39 = hits[1] ? ways_1_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_40 = hits[2] ? ways_2_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_41 = hits[3] ? ways_3_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_42 = hits[4] ? ways_4_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_43 = hits[5] ? ways_5_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_44 = hits[6] ? ways_6_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_45 = hits[7] ? ways_7_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_46 = _io_result_bits_T_38 | _io_result_bits_T_39; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_47 = _io_result_bits_T_46 | _io_result_bits_T_40; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_48 = _io_result_bits_T_47 | _io_result_bits_T_41; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_49 = _io_result_bits_T_48 | _io_result_bits_T_42; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_50 = _io_result_bits_T_49 | _io_result_bits_T_43; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_51 = _io_result_bits_T_50 | _io_result_bits_T_44; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_52 = _io_result_bits_T_51 | _io_result_bits_T_45; // @[Mux.scala 27:72]
  wire  _io_result_bits_T_67 = hits[0] & ways_0_dirty | hits[1] & ways_1_dirty | hits[2] & ways_2_dirty | hits[3] &
    ways_3_dirty | hits[4] & ways_4_dirty | hits[5] & ways_5_dirty | hits[6] & ways_6_dirty | hits[7] & ways_7_dirty; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_78 = victimWayOH[0] ? ways_0_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_79 = victimWayOH[1] ? ways_1_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_80 = victimWayOH[2] ? ways_2_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_81 = victimWayOH[3] ? ways_3_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_82 = victimWayOH[4] ? ways_4_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_83 = victimWayOH[5] ? ways_5_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_84 = victimWayOH[6] ? ways_6_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_85 = victimWayOH[7] ? ways_7_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_86 = _io_result_bits_T_78 | _io_result_bits_T_79; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_87 = _io_result_bits_T_86 | _io_result_bits_T_80; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_88 = _io_result_bits_T_87 | _io_result_bits_T_81; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_89 = _io_result_bits_T_88 | _io_result_bits_T_82; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_90 = _io_result_bits_T_89 | _io_result_bits_T_83; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_91 = _io_result_bits_T_90 | _io_result_bits_T_84; // @[Mux.scala 27:72]
  wire [17:0] _io_result_bits_T_92 = _io_result_bits_T_91 | _io_result_bits_T_85; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_93 = victimWayOH[0] ? ways_0_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_94 = victimWayOH[1] ? ways_1_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_95 = victimWayOH[2] ? ways_2_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_96 = victimWayOH[3] ? ways_3_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_97 = victimWayOH[4] ? ways_4_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_98 = victimWayOH[5] ? ways_5_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_99 = victimWayOH[6] ? ways_6_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_100 = victimWayOH[7] ? ways_7_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_101 = _io_result_bits_T_93 | _io_result_bits_T_94; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_102 = _io_result_bits_T_101 | _io_result_bits_T_95; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_103 = _io_result_bits_T_102 | _io_result_bits_T_96; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_104 = _io_result_bits_T_103 | _io_result_bits_T_97; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_105 = _io_result_bits_T_104 | _io_result_bits_T_98; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_106 = _io_result_bits_T_105 | _io_result_bits_T_99; // @[Mux.scala 27:72]
  wire [3:0] _io_result_bits_T_107 = _io_result_bits_T_106 | _io_result_bits_T_100; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_108 = victimWayOH[0] ? ways_0_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_109 = victimWayOH[1] ? ways_1_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_110 = victimWayOH[2] ? ways_2_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_111 = victimWayOH[3] ? ways_3_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_112 = victimWayOH[4] ? ways_4_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_113 = victimWayOH[5] ? ways_5_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_114 = victimWayOH[6] ? ways_6_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_115 = victimWayOH[7] ? ways_7_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_116 = _io_result_bits_T_108 | _io_result_bits_T_109; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_117 = _io_result_bits_T_116 | _io_result_bits_T_110; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_118 = _io_result_bits_T_117 | _io_result_bits_T_111; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_119 = _io_result_bits_T_118 | _io_result_bits_T_112; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_120 = _io_result_bits_T_119 | _io_result_bits_T_113; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_121 = _io_result_bits_T_120 | _io_result_bits_T_114; // @[Mux.scala 27:72]
  wire [1:0] _io_result_bits_T_122 = _io_result_bits_T_121 | _io_result_bits_T_115; // @[Mux.scala 27:72]
  wire  _io_result_bits_T_137 = victimWayOH[0] & ways_0_dirty | victimWayOH[1] & ways_1_dirty | victimWayOH[2] &
    ways_2_dirty | victimWayOH[3] & ways_3_dirty | victimWayOH[4] & ways_4_dirty | victimWayOH[5] & ways_5_dirty |
    victimWayOH[6] & ways_6_dirty | victimWayOH[7] & ways_7_dirty; // @[Mux.scala 27:72]
  wire  _io_result_bits_T_138_dirty = setQuash & (tagMatch | wayMatch) ? write_io_deq_bits_data_dirty :
    _io_result_bits_T_137; // @[Directory.scala 189:52]
  wire [1:0] _io_result_bits_T_138_state = setQuash & (tagMatch | wayMatch) ? write_io_deq_bits_data_state :
    _io_result_bits_T_122; // @[Directory.scala 189:52]
  wire [3:0] _io_result_bits_T_138_clients = setQuash & (tagMatch | wayMatch) ? write_io_deq_bits_data_clients :
    _io_result_bits_T_107; // @[Directory.scala 189:52]
  wire [17:0] _io_result_bits_T_138_tag = setQuash & (tagMatch | wayMatch) ? write_io_deq_bits_data_tag :
    _io_result_bits_T_92; // @[Directory.scala 189:52]
  wire [3:0] io_result_bits_way_hi = hits[7:4]; // @[OneHot.scala 30:18]
  wire [3:0] io_result_bits_way_lo = hits[3:0]; // @[OneHot.scala 31:18]
  wire  io_result_bits_way_hi_1 = |io_result_bits_way_hi; // @[OneHot.scala 32:14]
  wire [3:0] _io_result_bits_way_T = io_result_bits_way_hi | io_result_bits_way_lo; // @[OneHot.scala 32:28]
  wire [1:0] io_result_bits_way_hi_2 = _io_result_bits_way_T[3:2]; // @[OneHot.scala 30:18]
  wire [1:0] io_result_bits_way_lo_1 = _io_result_bits_way_T[1:0]; // @[OneHot.scala 31:18]
  wire  io_result_bits_way_hi_3 = |io_result_bits_way_hi_2; // @[OneHot.scala 32:14]
  wire [1:0] _io_result_bits_way_T_1 = io_result_bits_way_hi_2 | io_result_bits_way_lo_1; // @[OneHot.scala 32:28]
  wire  io_result_bits_way_lo_2 = _io_result_bits_way_T_1[1]; // @[CircuitMath.scala 30:8]
  wire [2:0] _io_result_bits_way_T_2 = {io_result_bits_way_hi_1,io_result_bits_way_hi_3,io_result_bits_way_lo_2}; // @[Cat.scala 30:58]
  wire [2:0] _io_result_bits_way_T_4 = _hitOut_T ? write_io_deq_bits_way : victimWay; // @[Directory.scala 192:53]
  RHEA__cc_dir cc_dir ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_dir_RW0_addr),
    .RW0_en(cc_dir_RW0_en),
    .RW0_clk(cc_dir_RW0_clk),
    .RW0_wmode(cc_dir_RW0_wmode),
    .RW0_wdata_0(cc_dir_RW0_wdata_0),
    .RW0_wdata_1(cc_dir_RW0_wdata_1),
    .RW0_wdata_2(cc_dir_RW0_wdata_2),
    .RW0_wdata_3(cc_dir_RW0_wdata_3),
    .RW0_wdata_4(cc_dir_RW0_wdata_4),
    .RW0_wdata_5(cc_dir_RW0_wdata_5),
    .RW0_wdata_6(cc_dir_RW0_wdata_6),
    .RW0_wdata_7(cc_dir_RW0_wdata_7),
    .RW0_rdata_0(cc_dir_RW0_rdata_0),
    .RW0_rdata_1(cc_dir_RW0_rdata_1),
    .RW0_rdata_2(cc_dir_RW0_rdata_2),
    .RW0_rdata_3(cc_dir_RW0_rdata_3),
    .RW0_rdata_4(cc_dir_RW0_rdata_4),
    .RW0_rdata_5(cc_dir_RW0_rdata_5),
    .RW0_rdata_6(cc_dir_RW0_rdata_6),
    .RW0_rdata_7(cc_dir_RW0_rdata_7),
    .RW0_wmask_0(cc_dir_RW0_wmask_0),
    .RW0_wmask_1(cc_dir_RW0_wmask_1),
    .RW0_wmask_2(cc_dir_RW0_wmask_2),
    .RW0_wmask_3(cc_dir_RW0_wmask_3),
    .RW0_wmask_4(cc_dir_RW0_wmask_4),
    .RW0_wmask_5(cc_dir_RW0_wmask_5),
    .RW0_wmask_6(cc_dir_RW0_wmask_6),
    .RW0_wmask_7(cc_dir_RW0_wmask_7)
  );
  RHEA__Queue_127 write ( // @[Decoupled.scala 296:21]
    .rf_reset(write_rf_reset),
    .clock(write_clock),
    .reset(write_reset),
    .io_enq_ready(write_io_enq_ready),
    .io_enq_valid(write_io_enq_valid),
    .io_enq_bits_set(write_io_enq_bits_set),
    .io_enq_bits_way(write_io_enq_bits_way),
    .io_enq_bits_data_dirty(write_io_enq_bits_data_dirty),
    .io_enq_bits_data_state(write_io_enq_bits_data_state),
    .io_enq_bits_data_clients(write_io_enq_bits_data_clients),
    .io_enq_bits_data_tag(write_io_enq_bits_data_tag),
    .io_deq_ready(write_io_deq_ready),
    .io_deq_valid(write_io_deq_valid),
    .io_deq_bits_set(write_io_deq_bits_set),
    .io_deq_bits_way(write_io_deq_bits_way),
    .io_deq_bits_data_dirty(write_io_deq_bits_data_dirty),
    .io_deq_bits_data_state(write_io_deq_bits_data_state),
    .io_deq_bits_data_clients(write_io_deq_bits_data_clients),
    .io_deq_bits_data_tag(write_io_deq_bits_data_tag)
  );
  RHEA__MaxPeriodFibonacciLFSR victimLFSR_prng ( // @[PRNG.scala 82:22]
    .clock(victimLFSR_prng_clock),
    .reset(victimLFSR_prng_reset),
    .io_increment(victimLFSR_prng_io_increment),
    .io_out_0(victimLFSR_prng_io_out_0),
    .io_out_1(victimLFSR_prng_io_out_1),
    .io_out_2(victimLFSR_prng_io_out_2),
    .io_out_3(victimLFSR_prng_io_out_3),
    .io_out_4(victimLFSR_prng_io_out_4),
    .io_out_5(victimLFSR_prng_io_out_5),
    .io_out_6(victimLFSR_prng_io_out_6),
    .io_out_7(victimLFSR_prng_io_out_7),
    .io_out_8(victimLFSR_prng_io_out_8),
    .io_out_9(victimLFSR_prng_io_out_9),
    .io_out_10(victimLFSR_prng_io_out_10),
    .io_out_11(victimLFSR_prng_io_out_11),
    .io_out_12(victimLFSR_prng_io_out_12),
    .io_out_13(victimLFSR_prng_io_out_13),
    .io_out_14(victimLFSR_prng_io_out_14),
    .io_out_15(victimLFSR_prng_io_out_15)
  );
  assign write_rf_reset = rf_reset;
  assign io_write_ready = write_io_enq_ready; // @[Decoupled.scala 299:17]
  assign io_result_bits_dirty = hit ? _io_result_bits_T_67 : _io_result_bits_T_138_dirty; // @[Directory.scala 189:24]
  assign io_result_bits_state = hit ? _io_result_bits_T_52 : _io_result_bits_T_138_state; // @[Directory.scala 189:24]
  assign io_result_bits_clients = hit ? _io_result_bits_T_37 : _io_result_bits_T_138_clients; // @[Directory.scala 189:24]
  assign io_result_bits_tag = hit ? _io_result_bits_T_22 : _io_result_bits_T_138_tag; // @[Directory.scala 189:24]
  assign io_result_bits_hit = hit | setQuash & tagMatch & write_io_deq_bits_data_state != 2'h0; // @[Directory.scala 185:20]
  assign io_result_bits_way = hit ? _io_result_bits_way_T_2 : _io_result_bits_way_T_4; // @[Directory.scala 192:28]
  assign io_ready = wipeCount[10]; // @[Directory.scala 105:27]
  assign io_corrected_valid = 1'h0; // @[Directory.scala 132:36]
  assign io_error_ready = _T_3 & wen; // @[Directory.scala 122:14]
  assign cc_dir_RW0_wdata_0 = error ^ _T_27; // @[Directory.scala 128:45]
  assign cc_dir_RW0_wdata_1 = error ^ _T_27; // @[Directory.scala 128:45]
  assign cc_dir_RW0_wdata_2 = error ^ _T_27; // @[Directory.scala 128:45]
  assign cc_dir_RW0_wdata_3 = error ^ _T_27; // @[Directory.scala 128:45]
  assign cc_dir_RW0_wdata_4 = error ^ _T_27; // @[Directory.scala 128:45]
  assign cc_dir_RW0_wdata_5 = error ^ _T_27; // @[Directory.scala 128:45]
  assign cc_dir_RW0_wdata_6 = error ^ _T_27; // @[Directory.scala 128:45]
  assign cc_dir_RW0_wdata_7 = error ^ _T_27; // @[Directory.scala 128:45]
  assign cc_dir_RW0_wmask_0 = _T_51[0] | _T; // @[Directory.scala 129:79]
  assign cc_dir_RW0_wmask_1 = _T_51[1] | _T; // @[Directory.scala 129:79]
  assign cc_dir_RW0_wmask_2 = _T_51[2] | _T; // @[Directory.scala 129:79]
  assign cc_dir_RW0_wmask_3 = _T_51[3] | _T; // @[Directory.scala 129:79]
  assign cc_dir_RW0_wmask_4 = _T_51[4] | _T; // @[Directory.scala 129:79]
  assign cc_dir_RW0_wmask_5 = _T_51[5] | _T; // @[Directory.scala 129:79]
  assign cc_dir_RW0_wmask_6 = _T_51[6] | _T; // @[Directory.scala 129:79]
  assign cc_dir_RW0_wmask_7 = _T_51[7] | _T; // @[Directory.scala 129:79]
  assign write_clock = clock;
  assign write_reset = reset;
  assign write_io_enq_valid = io_write_valid; // @[Decoupled.scala 297:22]
  assign write_io_enq_bits_set = io_write_bits_set; // @[Decoupled.scala 298:21]
  assign write_io_enq_bits_way = io_write_bits_way; // @[Decoupled.scala 298:21]
  assign write_io_enq_bits_data_dirty = io_write_bits_data_dirty; // @[Decoupled.scala 298:21]
  assign write_io_enq_bits_data_state = io_write_bits_data_state; // @[Decoupled.scala 298:21]
  assign write_io_enq_bits_data_clients = io_write_bits_data_clients; // @[Decoupled.scala 298:21]
  assign write_io_enq_bits_data_tag = io_write_bits_data_tag; // @[Decoupled.scala 298:21]
  assign write_io_deq_ready = ~io_read_valid; // @[Directory.scala 121:18]
  assign victimLFSR_prng_clock = clock;
  assign victimLFSR_prng_reset = reset;
  assign victimLFSR_prng_io_increment = io_read_valid; // @[PRNG.scala 85:23]
  assign cc_dir_RW0_wmode = ~io_read_valid;
  assign cc_dir_RW0_clk = clock;
  assign cc_dir_RW0_en = io_read_valid | _T_14;
  assign cc_dir_RW0_addr = _T_14 ? _T_16 : io_read_bits_set;
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wipeCount <= 11'h0;
    end else if (~wipeDone & ~wipeOff) begin
      wipeCount <= _wipeCount_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wipeOff <= 1'h1;
    end else begin
      wipeOff <= 1'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tag <= 18'h0;
    end else if (io_read_valid) begin
      tag <= io_read_bits_tag;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      set <= 10'h0;
    end else if (io_read_valid) begin
      set <= io_read_bits_set;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      victimWays <= 8'h0;
    end else if (io_read_valid) begin
      victimWays <= sourceWays;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      victimDiv <= 11'h0;
    end else if (io_read_valid) begin
      victimDiv <= sourceDiv;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  wipeCount = _RAND_0[10:0];
  _RAND_1 = {1{`RANDOM}};
  wipeOff = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  tag = _RAND_2[17:0];
  _RAND_3 = {1{`RANDOM}};
  set = _RAND_3[9:0];
  _RAND_4 = {1{`RANDOM}};
  victimWays = _RAND_4[7:0];
  _RAND_5 = {1{`RANDOM}};
  victimDiv = _RAND_5[10:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    wipeCount = 11'h0;
  end
  if (reset) begin
    wipeOff = 1'h1;
  end
  if (rf_reset) begin
    tag = 18'h0;
  end
  if (rf_reset) begin
    set = 10'h0;
  end
  if (rf_reset) begin
    victimWays = 8'h0;
  end
  if (rf_reset) begin
    victimDiv = 11'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
