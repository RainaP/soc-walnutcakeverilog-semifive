//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_64_assert(
  input          clock,
  input          reset,
  input          io_in_a_ready,
  input          io_in_a_valid,
  input  [2:0]   io_in_a_bits_opcode,
  input  [127:0] io_in_a_bits_address,
  input  [3:0]   io_in_a_bits_mask,
  input          io_in_d_ready,
  input          io_in_d_valid,
  input  [2:0]   io_in_d_bits_opcode,
  input  [1:0]   io_in_d_bits_param,
  input  [1:0]   io_in_d_bits_size,
  input          io_in_d_bits_source,
  input          io_in_d_bits_sink,
  input          io_in_d_bits_denied,
  input          io_in_d_bits_corrupt
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire [127:0] _T_10; // @[Edges.scala 22:16]
  wire  _T_11; // @[Edges.scala 22:24]
  wire [128:0] _T_48; // @[Parameters.scala 137:49]
  wire  _T_56; // @[Monitor.scala 79:25]
  wire [128:0] _T_64; // @[Parameters.scala 137:52]
  wire  _T_65; // @[Parameters.scala 137:67]
  wire  _T_69; // @[Monitor.scala 44:11]
  wire  _T_70; // @[Monitor.scala 44:11]
  wire  _T_73; // @[Monitor.scala 44:11]
  wire  _T_82; // @[Monitor.scala 44:11]
  wire  _T_83; // @[Monitor.scala 44:11]
  wire [3:0] _T_88; // @[Monitor.scala 86:18]
  wire  _T_89; // @[Monitor.scala 86:31]
  wire  _T_91; // @[Monitor.scala 44:11]
  wire  _T_92; // @[Monitor.scala 44:11]
  wire  _T_97; // @[Monitor.scala 90:25]
  wire  _T_142; // @[Monitor.scala 102:25]
  wire  _T_167; // @[Monitor.scala 107:30]
  wire  _T_169; // @[Monitor.scala 44:11]
  wire  _T_170; // @[Monitor.scala 44:11]
  wire  _T_175; // @[Monitor.scala 111:25]
  wire  _T_204; // @[Monitor.scala 119:25]
  wire  _T_235; // @[Monitor.scala 127:25]
  wire  _T_261; // @[Monitor.scala 135:25]
  wire  _T_287; // @[Monitor.scala 143:25]
  wire  _T_320; // @[Bundles.scala 44:24]
  wire  _T_322; // @[Monitor.scala 51:11]
  wire  _T_323; // @[Monitor.scala 51:11]
  wire  _T_324; // @[Parameters.scala 47:9]
  wire  _T_326; // @[Monitor.scala 303:31]
  wire  _T_327; // @[Monitor.scala 307:25]
  wire  _T_329; // @[Monitor.scala 51:11]
  wire  _T_330; // @[Monitor.scala 51:11]
  wire  _T_331; // @[Monitor.scala 309:27]
  wire  _T_333; // @[Monitor.scala 51:11]
  wire  _T_334; // @[Monitor.scala 51:11]
  wire  _T_335; // @[Monitor.scala 310:28]
  wire  _T_337; // @[Monitor.scala 51:11]
  wire  _T_338; // @[Monitor.scala 51:11]
  wire  _T_339; // @[Monitor.scala 311:15]
  wire  _T_341; // @[Monitor.scala 51:11]
  wire  _T_342; // @[Monitor.scala 51:11]
  wire  _T_343; // @[Monitor.scala 312:15]
  wire  _T_345; // @[Monitor.scala 51:11]
  wire  _T_346; // @[Monitor.scala 51:11]
  wire  _T_347; // @[Monitor.scala 315:25]
  wire  _T_352; // @[Monitor.scala 51:11]
  wire  _T_353; // @[Monitor.scala 51:11]
  wire  _T_358; // @[Bundles.scala 104:26]
  wire  _T_360; // @[Monitor.scala 51:11]
  wire  _T_361; // @[Monitor.scala 51:11]
  wire  _T_362; // @[Monitor.scala 320:28]
  wire  _T_364; // @[Monitor.scala 51:11]
  wire  _T_365; // @[Monitor.scala 51:11]
  wire  _T_375; // @[Monitor.scala 325:25]
  wire  _T_395; // @[Monitor.scala 331:30]
  wire  _T_397; // @[Monitor.scala 51:11]
  wire  _T_398; // @[Monitor.scala 51:11]
  wire  _T_404; // @[Monitor.scala 335:25]
  wire  _T_421; // @[Monitor.scala 343:25]
  wire  _T_439; // @[Monitor.scala 351:25]
  wire  _T_471; // @[Decoupled.scala 40:37]
  reg  _T_480; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire  _T_482; // @[Edges.scala 231:28]
  wire  _T_483; // @[Edges.scala 232:25]
  reg [2:0] _T_491; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [127:0] _T_495; // @[Monitor.scala 385:22]
  reg [127:0] _RAND_2;
  wire  _T_496; // @[Monitor.scala 386:22]
  wire  _T_497; // @[Monitor.scala 386:19]
  wire  _T_498; // @[Monitor.scala 387:32]
  wire  _T_500; // @[Monitor.scala 44:11]
  wire  _T_501; // @[Monitor.scala 44:11]
  wire  _T_514; // @[Monitor.scala 391:32]
  wire  _T_516; // @[Monitor.scala 44:11]
  wire  _T_517; // @[Monitor.scala 44:11]
  wire  _T_519; // @[Monitor.scala 393:20]
  wire  _T_520; // @[Decoupled.scala 40:37]
  reg  _T_528; // @[Edges.scala 230:27]
  reg [31:0] _RAND_3;
  wire  _T_530; // @[Edges.scala 231:28]
  wire  _T_531; // @[Edges.scala 232:25]
  reg [2:0] _T_539; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_4;
  reg [1:0] _T_540; // @[Monitor.scala 533:22]
  reg [31:0] _RAND_5;
  reg [1:0] _T_541; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_6;
  reg  _T_542; // @[Monitor.scala 535:22]
  reg [31:0] _RAND_7;
  reg  _T_543; // @[Monitor.scala 536:22]
  reg [31:0] _RAND_8;
  reg  _T_544; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_9;
  wire  _T_545; // @[Monitor.scala 538:22]
  wire  _T_546; // @[Monitor.scala 538:19]
  wire  _T_547; // @[Monitor.scala 539:29]
  wire  _T_549; // @[Monitor.scala 51:11]
  wire  _T_550; // @[Monitor.scala 51:11]
  wire  _T_551; // @[Monitor.scala 540:29]
  wire  _T_553; // @[Monitor.scala 51:11]
  wire  _T_554; // @[Monitor.scala 51:11]
  wire  _T_555; // @[Monitor.scala 541:29]
  wire  _T_557; // @[Monitor.scala 51:11]
  wire  _T_558; // @[Monitor.scala 51:11]
  wire  _T_559; // @[Monitor.scala 542:29]
  wire  _T_561; // @[Monitor.scala 51:11]
  wire  _T_562; // @[Monitor.scala 51:11]
  wire  _T_563; // @[Monitor.scala 543:29]
  wire  _T_565; // @[Monitor.scala 51:11]
  wire  _T_566; // @[Monitor.scala 51:11]
  wire  _T_567; // @[Monitor.scala 544:29]
  wire  _T_569; // @[Monitor.scala 51:11]
  wire  _T_570; // @[Monitor.scala 51:11]
  wire  _T_572; // @[Monitor.scala 546:20]
  reg  _T_573; // @[Monitor.scala 568:27]
  reg [31:0] _RAND_10;
  reg  _T_583; // @[Edges.scala 230:27]
  reg [31:0] _RAND_11;
  wire  _T_585; // @[Edges.scala 231:28]
  wire  _T_586; // @[Edges.scala 232:25]
  reg  _T_602; // @[Edges.scala 230:27]
  reg [31:0] _RAND_12;
  wire  _T_604; // @[Edges.scala 231:28]
  wire  _T_605; // @[Edges.scala 232:25]
  wire  _T_615; // @[Monitor.scala 574:27]
  wire  _T_620; // @[Monitor.scala 576:14]
  wire  _T_622; // @[Monitor.scala 576:13]
  wire  _T_623; // @[Monitor.scala 576:13]
  wire [1:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_627; // @[Monitor.scala 581:27]
  wire  _T_629; // @[Monitor.scala 581:75]
  wire  _T_630; // @[Monitor.scala 581:72]
  wire [1:0] _T_631; // @[OneHot.scala 58:35]
  wire  _T_632; // @[Monitor.scala 583:21]
  wire  _T_633; // @[Monitor.scala 583:32]
  wire  _T_636; // @[Monitor.scala 51:11]
  wire  _T_637; // @[Monitor.scala 51:11]
  wire [1:0] _GEN_16; // @[Monitor.scala 581:91]
  wire  _T_638; // @[Monitor.scala 590:27]
  wire  _T_639; // @[Monitor.scala 590:38]
  reg [31:0] _T_641; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_13;
  wire  _T_644; // @[Monitor.scala 595:36]
  wire  _T_645; // @[Monitor.scala 595:27]
  wire  _T_646; // @[Monitor.scala 595:56]
  wire  _T_647; // @[Monitor.scala 595:44]
  wire  _T_649; // @[Monitor.scala 595:12]
  wire  _T_650; // @[Monitor.scala 595:12]
  wire [31:0] _T_652; // @[Monitor.scala 597:26]
  wire  _T_655; // @[Monitor.scala 598:27]
  wire  _GEN_18; // @[Monitor.scala 44:11]
  wire  _GEN_26; // @[Monitor.scala 44:11]
  wire  _GEN_36; // @[Monitor.scala 44:11]
  wire  _GEN_42; // @[Monitor.scala 44:11]
  wire  _GEN_48; // @[Monitor.scala 44:11]
  wire  _GEN_52; // @[Monitor.scala 44:11]
  wire  _GEN_58; // @[Monitor.scala 44:11]
  wire  _GEN_64; // @[Monitor.scala 44:11]
  wire  _GEN_70; // @[Monitor.scala 51:11]
  wire  _GEN_80; // @[Monitor.scala 51:11]
  wire  _GEN_92; // @[Monitor.scala 51:11]
  wire  _GEN_104; // @[Monitor.scala 51:11]
  wire  _GEN_110; // @[Monitor.scala 51:11]
  wire  _GEN_116; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_10 = io_in_a_bits_address & 128'h3; // @[Edges.scala 22:16]
  assign _T_11 = _T_10 == 128'h0; // @[Edges.scala 22:24]
  assign _T_48 = {1'b0,$signed(io_in_a_bits_address)}; // @[Parameters.scala 137:49]
  assign _T_56 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_64 = $signed(_T_48) & -129'sh100000000000000000000000000000000; // @[Parameters.scala 137:52]
  assign _T_65 = $signed(_T_64) == 129'sh0; // @[Parameters.scala 137:67]
  assign _T_69 = _T_65 | reset; // @[Monitor.scala 44:11]
  assign _T_70 = ~_T_69; // @[Monitor.scala 44:11]
  assign _T_73 = ~reset; // @[Monitor.scala 44:11]
  assign _T_82 = _T_11 | reset; // @[Monitor.scala 44:11]
  assign _T_83 = ~_T_82; // @[Monitor.scala 44:11]
  assign _T_88 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_89 = _T_88 == 4'h0; // @[Monitor.scala 86:31]
  assign _T_91 = _T_89 | reset; // @[Monitor.scala 44:11]
  assign _T_92 = ~_T_91; // @[Monitor.scala 44:11]
  assign _T_97 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_142 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_167 = io_in_a_bits_mask == 4'hf; // @[Monitor.scala 107:30]
  assign _T_169 = _T_167 | reset; // @[Monitor.scala 44:11]
  assign _T_170 = ~_T_169; // @[Monitor.scala 44:11]
  assign _T_175 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_204 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_235 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_261 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_287 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_320 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_322 = _T_320 | reset; // @[Monitor.scala 51:11]
  assign _T_323 = ~_T_322; // @[Monitor.scala 51:11]
  assign _T_324 = ~io_in_d_bits_source; // @[Parameters.scala 47:9]
  assign _T_326 = io_in_d_bits_sink < 1'h1; // @[Monitor.scala 303:31]
  assign _T_327 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_329 = _T_324 | reset; // @[Monitor.scala 51:11]
  assign _T_330 = ~_T_329; // @[Monitor.scala 51:11]
  assign _T_331 = io_in_d_bits_size >= 2'h2; // @[Monitor.scala 309:27]
  assign _T_333 = _T_331 | reset; // @[Monitor.scala 51:11]
  assign _T_334 = ~_T_333; // @[Monitor.scala 51:11]
  assign _T_335 = io_in_d_bits_param == 2'h0; // @[Monitor.scala 310:28]
  assign _T_337 = _T_335 | reset; // @[Monitor.scala 51:11]
  assign _T_338 = ~_T_337; // @[Monitor.scala 51:11]
  assign _T_339 = ~io_in_d_bits_corrupt; // @[Monitor.scala 311:15]
  assign _T_341 = _T_339 | reset; // @[Monitor.scala 51:11]
  assign _T_342 = ~_T_341; // @[Monitor.scala 51:11]
  assign _T_343 = ~io_in_d_bits_denied; // @[Monitor.scala 312:15]
  assign _T_345 = _T_343 | reset; // @[Monitor.scala 51:11]
  assign _T_346 = ~_T_345; // @[Monitor.scala 51:11]
  assign _T_347 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_352 = _T_326 | reset; // @[Monitor.scala 51:11]
  assign _T_353 = ~_T_352; // @[Monitor.scala 51:11]
  assign _T_358 = io_in_d_bits_param <= 2'h2; // @[Bundles.scala 104:26]
  assign _T_360 = _T_358 | reset; // @[Monitor.scala 51:11]
  assign _T_361 = ~_T_360; // @[Monitor.scala 51:11]
  assign _T_362 = io_in_d_bits_param != 2'h2; // @[Monitor.scala 320:28]
  assign _T_364 = _T_362 | reset; // @[Monitor.scala 51:11]
  assign _T_365 = ~_T_364; // @[Monitor.scala 51:11]
  assign _T_375 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_395 = _T_343 | io_in_d_bits_corrupt; // @[Monitor.scala 331:30]
  assign _T_397 = _T_395 | reset; // @[Monitor.scala 51:11]
  assign _T_398 = ~_T_397; // @[Monitor.scala 51:11]
  assign _T_404 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_421 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_439 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_471 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_482 = _T_480 - 1'h1; // @[Edges.scala 231:28]
  assign _T_483 = ~_T_480; // @[Edges.scala 232:25]
  assign _T_496 = ~_T_483; // @[Monitor.scala 386:22]
  assign _T_497 = io_in_a_valid & _T_496; // @[Monitor.scala 386:19]
  assign _T_498 = io_in_a_bits_opcode == _T_491; // @[Monitor.scala 387:32]
  assign _T_500 = _T_498 | reset; // @[Monitor.scala 44:11]
  assign _T_501 = ~_T_500; // @[Monitor.scala 44:11]
  assign _T_514 = io_in_a_bits_address == _T_495; // @[Monitor.scala 391:32]
  assign _T_516 = _T_514 | reset; // @[Monitor.scala 44:11]
  assign _T_517 = ~_T_516; // @[Monitor.scala 44:11]
  assign _T_519 = _T_471 & _T_483; // @[Monitor.scala 393:20]
  assign _T_520 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_530 = _T_528 - 1'h1; // @[Edges.scala 231:28]
  assign _T_531 = ~_T_528; // @[Edges.scala 232:25]
  assign _T_545 = ~_T_531; // @[Monitor.scala 538:22]
  assign _T_546 = io_in_d_valid & _T_545; // @[Monitor.scala 538:19]
  assign _T_547 = io_in_d_bits_opcode == _T_539; // @[Monitor.scala 539:29]
  assign _T_549 = _T_547 | reset; // @[Monitor.scala 51:11]
  assign _T_550 = ~_T_549; // @[Monitor.scala 51:11]
  assign _T_551 = io_in_d_bits_param == _T_540; // @[Monitor.scala 540:29]
  assign _T_553 = _T_551 | reset; // @[Monitor.scala 51:11]
  assign _T_554 = ~_T_553; // @[Monitor.scala 51:11]
  assign _T_555 = io_in_d_bits_size == _T_541; // @[Monitor.scala 541:29]
  assign _T_557 = _T_555 | reset; // @[Monitor.scala 51:11]
  assign _T_558 = ~_T_557; // @[Monitor.scala 51:11]
  assign _T_559 = io_in_d_bits_source == _T_542; // @[Monitor.scala 542:29]
  assign _T_561 = _T_559 | reset; // @[Monitor.scala 51:11]
  assign _T_562 = ~_T_561; // @[Monitor.scala 51:11]
  assign _T_563 = io_in_d_bits_sink == _T_543; // @[Monitor.scala 543:29]
  assign _T_565 = _T_563 | reset; // @[Monitor.scala 51:11]
  assign _T_566 = ~_T_565; // @[Monitor.scala 51:11]
  assign _T_567 = io_in_d_bits_denied == _T_544; // @[Monitor.scala 544:29]
  assign _T_569 = _T_567 | reset; // @[Monitor.scala 51:11]
  assign _T_570 = ~_T_569; // @[Monitor.scala 51:11]
  assign _T_572 = _T_520 & _T_531; // @[Monitor.scala 546:20]
  assign _T_585 = _T_583 - 1'h1; // @[Edges.scala 231:28]
  assign _T_586 = ~_T_583; // @[Edges.scala 232:25]
  assign _T_604 = _T_602 - 1'h1; // @[Edges.scala 231:28]
  assign _T_605 = ~_T_602; // @[Edges.scala 232:25]
  assign _T_615 = _T_471 & _T_586; // @[Monitor.scala 574:27]
  assign _T_620 = ~_T_573; // @[Monitor.scala 576:14]
  assign _T_622 = _T_620 | reset; // @[Monitor.scala 576:13]
  assign _T_623 = ~_T_622; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_615 ? 2'h1 : 2'h0; // @[Monitor.scala 574:72]
  assign _T_627 = _T_520 & _T_605; // @[Monitor.scala 581:27]
  assign _T_629 = ~_T_327; // @[Monitor.scala 581:75]
  assign _T_630 = _T_627 & _T_629; // @[Monitor.scala 581:72]
  assign _T_631 = 2'h1 << io_in_d_bits_source; // @[OneHot.scala 58:35]
  assign _T_632 = _GEN_15[0] | _T_573; // @[Monitor.scala 583:21]
  assign _T_633 = _T_632 >> io_in_d_bits_source; // @[Monitor.scala 583:32]
  assign _T_636 = _T_633 | reset; // @[Monitor.scala 51:11]
  assign _T_637 = ~_T_636; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_630 ? _T_631 : 2'h0; // @[Monitor.scala 581:91]
  assign _T_638 = _T_573 | _GEN_15[0]; // @[Monitor.scala 590:27]
  assign _T_639 = ~_GEN_16[0]; // @[Monitor.scala 590:38]
  assign _T_644 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_645 = _T_620 | _T_644; // @[Monitor.scala 595:27]
  assign _T_646 = _T_641 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_647 = _T_645 | _T_646; // @[Monitor.scala 595:44]
  assign _T_649 = _T_647 | reset; // @[Monitor.scala 595:12]
  assign _T_650 = ~_T_649; // @[Monitor.scala 595:12]
  assign _T_652 = _T_641 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_655 = _T_471 | _T_520; // @[Monitor.scala 598:27]
  assign _GEN_18 = io_in_a_valid & _T_56; // @[Monitor.scala 44:11]
  assign _GEN_26 = io_in_a_valid & _T_97; // @[Monitor.scala 44:11]
  assign _GEN_36 = io_in_a_valid & _T_142; // @[Monitor.scala 44:11]
  assign _GEN_42 = io_in_a_valid & _T_175; // @[Monitor.scala 44:11]
  assign _GEN_48 = io_in_a_valid & _T_204; // @[Monitor.scala 44:11]
  assign _GEN_52 = io_in_a_valid & _T_235; // @[Monitor.scala 44:11]
  assign _GEN_58 = io_in_a_valid & _T_261; // @[Monitor.scala 44:11]
  assign _GEN_64 = io_in_a_valid & _T_287; // @[Monitor.scala 44:11]
  assign _GEN_70 = io_in_d_valid & _T_327; // @[Monitor.scala 51:11]
  assign _GEN_80 = io_in_d_valid & _T_347; // @[Monitor.scala 51:11]
  assign _GEN_92 = io_in_d_valid & _T_375; // @[Monitor.scala 51:11]
  assign _GEN_104 = io_in_d_valid & _T_404; // @[Monitor.scala 51:11]
  assign _GEN_110 = io_in_d_valid & _T_421; // @[Monitor.scala 51:11]
  assign _GEN_116 = io_in_d_valid & _T_439; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_480 = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_491 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {4{`RANDOM}};
  _T_495 = _RAND_2[127:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_528 = _RAND_3[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_539 = _RAND_4[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  _T_540 = _RAND_5[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_541 = _RAND_6[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_542 = _RAND_7[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_543 = _RAND_8[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_544 = _RAND_9[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_573 = _RAND_10[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_583 = _RAND_11[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_602 = _RAND_12[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  _T_641 = _RAND_13[31:0];
  `endif // RANDOMIZE_REG_INIT
  if (reset) begin
    _T_480 = 1'h0;
  end
  if (reset) begin
    _T_528 = 1'h0;
  end
  if (reset) begin
    _T_573 = 1'h0;
  end
  if (reset) begin
    _T_583 = 1'h0;
  end
  if (reset) begin
    _T_602 = 1'h0;
  end
  if (reset) begin
    _T_641 = 32'h0;
  end
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (_T_519) begin
      _T_491 <= io_in_a_bits_opcode;
    end
    if (_T_519) begin
      _T_495 <= io_in_a_bits_address;
    end
    if (_T_572) begin
      _T_539 <= io_in_d_bits_opcode;
    end
    if (_T_572) begin
      _T_540 <= io_in_d_bits_param;
    end
    if (_T_572) begin
      _T_541 <= io_in_d_bits_size;
    end
    if (_T_572) begin
      _T_542 <= io_in_d_bits_source;
    end
    if (_T_572) begin
      _T_543 <= io_in_d_bits_sink;
    end
    if (_T_572) begin
      _T_544 <= io_in_d_bits_denied;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_70) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_70) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_73) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_83) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_83) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_92) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_92) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_70) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_70) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_73) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_83) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_83) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_73) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_92) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_92) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_36 & _T_70) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_36 & _T_70) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_36 & _T_83) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_36 & _T_83) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_36 & _T_170) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_36 & _T_170) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_42 & _T_70) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_42 & _T_70) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_42 & _T_83) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_42 & _T_83) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_42 & _T_170) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_42 & _T_170) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_48 & _T_70) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_48 & _T_70) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_48 & _T_83) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_48 & _T_83) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_52 & _T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_52 & _T_73) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_52 & _T_83) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_52 & _T_83) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_52 & _T_170) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_52 & _T_170) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_58 & _T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_58 & _T_73) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_58 & _T_83) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_58 & _T_83) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_58 & _T_170) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_58 & _T_170) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_70) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_70) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_83) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_83) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_170) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_170) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_323) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_323) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_70 & _T_330) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck carries invalid source ID (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_70 & _T_330) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_70 & _T_334) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_70 & _T_334) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_70 & _T_338) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseeAck carries invalid param (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_70 & _T_338) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_70 & _T_342) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is corrupt (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_70 & _T_342) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_70 & _T_346) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is denied (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_70 & _T_346) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_80 & _T_330) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid source ID (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_80 & _T_330) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_80 & _T_353) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid sink ID (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_80 & _T_353) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_80 & _T_334) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_80 & _T_334) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_80 & _T_361) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid cap param (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_80 & _T_361) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_80 & _T_365) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries toN param (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_80 & _T_365) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_80 & _T_342) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is corrupt (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_80 & _T_342) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_92 & _T_330) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid source ID (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_92 & _T_330) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_92 & _T_353) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid sink ID (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_92 & _T_353) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_92 & _T_334) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_92 & _T_334) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_92 & _T_361) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid cap param (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_92 & _T_361) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_92 & _T_365) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries toN param (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_92 & _T_365) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_92 & _T_398) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied but not corrupt (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_92 & _T_398) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_104 & _T_330) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid source ID (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_104 & _T_330) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_104 & _T_338) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid param (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_104 & _T_338) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_104 & _T_342) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is corrupt (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_104 & _T_342) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_110 & _T_330) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid source ID (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_110 & _T_330) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_110 & _T_338) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid param (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_110 & _T_338) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_110 & _T_398) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied but not corrupt (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_110 & _T_398) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_116 & _T_330) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid source ID (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_116 & _T_330) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_116 & _T_338) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid param (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_116 & _T_338) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_116 & _T_342) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is corrupt (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_116 & _T_342) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_497 & _T_501) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_497 & _T_501) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_497 & _T_517) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at BusBypass.scala:36:14)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_497 & _T_517) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_546 & _T_550) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_546 & _T_550) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_546 & _T_554) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel param changed within multibeat operation (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_546 & _T_554) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_546 & _T_558) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_546 & _T_558) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_546 & _T_562) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel source changed within multibeat operation (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_546 & _T_562) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_546 & _T_566) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel sink changed with multibeat operation (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_546 & _T_566) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_546 & _T_570) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_546 & _T_570) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_615 & _T_623) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at BusBypass.scala:36:14)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_615 & _T_623) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_630 & _T_637) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at BusBypass.scala:36:14)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_630 & _T_637) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_650) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at BusBypass.scala:36:14)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_650) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_480 <= 1'h0;
    end else if (_T_471) begin
      if (_T_483) begin
        _T_480 <= 1'h0;
      end else begin
        _T_480 <= _T_482;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_528 <= 1'h0;
    end else if (_T_520) begin
      if (_T_531) begin
        _T_528 <= 1'h0;
      end else begin
        _T_528 <= _T_530;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_573 <= 1'h0;
    end else begin
      _T_573 <= _T_638 & _T_639;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_583 <= 1'h0;
    end else if (_T_471) begin
      if (_T_586) begin
        _T_583 <= 1'h0;
      end else begin
        _T_583 <= _T_585;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_602 <= 1'h0;
    end else if (_T_520) begin
      if (_T_605) begin
        _T_602 <= 1'h0;
      end else begin
        _T_602 <= _T_604;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_641 <= 32'h0;
    end else if (_T_655) begin
      _T_641 <= 32'h0;
    end else begin
      _T_641 <= _T_652;
    end
  end

endmodule
