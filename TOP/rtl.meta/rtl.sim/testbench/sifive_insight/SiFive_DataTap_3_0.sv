//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_DataTap_3_0(
  output        _1_clock,
  output        _1_reset,
  output        _1_excpt,
  output [2:0]  _1_priv_mode,
  output [63:0] _1_hartid,
  output [31:0] _1_timer,
  output        _1_valid,
  output [63:0] _1_pc,
  output [4:0]  _1_wrdst,
  output [63:0] _1_wrdata,
  output        _1_wrenx,
  output        _1_wrenf,
  output [4:0]  _1_rd0src,
  output [63:0] _1_rd0val,
  output [4:0]  _1_rd1src,
  output [63:0] _1_rd1val,
  output [31:0] _1_inst,
  output        _0_clock,
  output        _0_reset,
  output        _0_excpt,
  output [2:0]  _0_priv_mode,
  output [63:0] _0_hartid,
  output [31:0] _0_timer,
  output        _0_valid,
  output [63:0] _0_pc,
  output [4:0]  _0_wrdst,
  output [63:0] _0_wrdata,
  output        _0_wrenx,
  output        _0_wrenf,
  output [4:0]  _0_rd0src,
  output [63:0] _0_rd0val,
  output [4:0]  _0_rd1src,
  output [63:0] _0_rd1val,
  output [31:0] _0_inst
);
  assign _1_clock = rocket_1.core.xrfWriteBundle_clock;
  assign _1_reset = rocket_1.core.xrfWriteBundle_reset;
  assign _1_excpt = rocket_1.core.xrfWriteBundle_excpt;
  assign _1_priv_mode = rocket_1.core.xrfWriteBundle_priv_mode;
  assign _1_hartid = rocket_1.core.xrfWriteBundle_hartid;
  assign _1_timer = rocket_1.core.xrfWriteBundle_timer;
  assign _1_valid = rocket_1.core.xrfWriteBundle_valid;
  assign _1_pc = rocket_1.core.xrfWriteBundle_pc;
  assign _1_wrdst = rocket_1.core.xrfWriteBundle_wrdst;
  assign _1_wrdata = rocket_1.core.xrfWriteBundle_wrdata;
  assign _1_wrenx = rocket_1.core.xrfWriteBundle_wrenx;
  assign _1_wrenf = rocket_1.core.xrfWriteBundle_wrenf;
  assign _1_rd0src = rocket_1.core.xrfWriteBundle_rd0src;
  assign _1_rd0val = rocket_1.core.xrfWriteBundle_rd0val;
  assign _1_rd1src = rocket_1.core.xrfWriteBundle_rd1src;
  assign _1_rd1val = rocket_1.core.xrfWriteBundle_rd1val;
  assign _1_inst = rocket_1.core.xrfWriteBundle_inst;
  assign _0_clock = rocket_1.core.coreMonitorBundle_clock;
  assign _0_reset = rocket_1.core.coreMonitorBundle_reset;
  assign _0_excpt = rocket_1.core.coreMonitorBundle_excpt;
  assign _0_priv_mode = rocket_1.core.coreMonitorBundle_priv_mode;
  assign _0_hartid = rocket_1.core.coreMonitorBundle_hartid;
  assign _0_timer = rocket_1.core.coreMonitorBundle_timer;
  assign _0_valid = rocket_1.core.coreMonitorBundle_valid;
  assign _0_pc = rocket_1.core.coreMonitorBundle_pc;
  assign _0_wrdst = rocket_1.core.coreMonitorBundle_wrdst;
  assign _0_wrdata = rocket_1.core.coreMonitorBundle_wrdata;
  assign _0_wrenx = rocket_1.core.coreMonitorBundle_wrenx;
  assign _0_wrenf = rocket_1.core.coreMonitorBundle_wrenf;
  assign _0_rd0src = rocket_1.core.coreMonitorBundle_rd0src;
  assign _0_rd0val = rocket_1.core.coreMonitorBundle_rd0val;
  assign _0_rd1src = rocket_1.core.coreMonitorBundle_rd1src;
  assign _0_rd1val = rocket_1.core.coreMonitorBundle_rd1val;
  assign _0_inst = rocket_1.core.coreMonitorBundle_inst;
endmodule
