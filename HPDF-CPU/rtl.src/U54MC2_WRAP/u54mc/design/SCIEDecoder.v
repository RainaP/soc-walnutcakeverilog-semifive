//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88

module SCIEDecoder (
    input  [31:0] insn,
    output unpipelined,
    output pipelined,
    output multicycle);

  /* This module decodes a SCIE instruction and indicates which functional unit
     to send the instruction to (unpipelined, pipelined, or multicycle).  The
     outputs are don't-cares unless insn lies within the custom-0 or custom-1
     major opcodes.  If it is within custom-0 or custom-1, then at most one of
     the outputs may be high.  If none are high, an illegal-instruction trap
     occurs.  If multiple are high, the behavior is undefined.

     This example implementation permits Funct3 = 0 or 1 within both custom-0
     and custom-1 as Unpipelined instructions.

     It also permits Funct3 = 2 or 3 within custom-0 as Pipelined instructions.
  */

  wire [2:0] funct3 = insn[14:12];

  assign unpipelined = funct3 <= 3'h1;
  assign pipelined = funct3 == 3'h2 || funct3 == 3'h3;
  assign multicycle = 1'b0;

endmodule
     