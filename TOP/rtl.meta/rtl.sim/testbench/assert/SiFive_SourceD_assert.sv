//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_SourceD_assert(
  input   clock,
  input   reset,
  input   io_req_bits_user_hit_cache,
  input   _T,
  input   _T_1,
  input   _T_373,
  input   s1_latch_bypass,
  input   s1_2_match,
  input   s1_3_match,
  input   s1_4_match,
  input   _T_67,
  input   _T_97,
  input   s3_bypass,
  input   s3_req_prio_2,
  input   s3_need_r,
  input   s3_need_data,
  input   s3_valid,
  input   queue_io_enq_ready,
  input   queue_io_deq_valid,
  input   queue_io_enq_valid
);
  reg  s1_req_reg_user_hit_cache; // @[Reg.scala 15:16]
  reg [31:0] _RAND_0;
  wire  _T_58; // @[SourceD.scala 134:11]
  wire  _T_59; // @[SourceD.scala 134:31]
  wire  _T_61; // @[SourceD.scala 134:10]
  wire  _T_62; // @[SourceD.scala 134:10]
  reg  s2_req_user_hit_cache; // @[Reg.scala 15:16]
  reg [31:0] _RAND_1;
  reg  s3_req_user_hit_cache; // @[Reg.scala 15:16]
  reg [31:0] _RAND_2;
  wire  _T_147; // @[SourceD.scala 265:10]
  wire  _T_148; // @[SourceD.scala 265:23]
  wire  _T_149; // @[SourceD.scala 265:20]
  wire  _T_150; // @[SourceD.scala 265:39]
  wire  _T_152; // @[SourceD.scala 265:9]
  wire  _T_153; // @[SourceD.scala 265:9]
  wire  _T_359; // @[SourceD.scala 276:23]
  wire  _T_360; // @[SourceD.scala 276:20]
  wire  _T_361; // @[SourceD.scala 276:34]
  wire  _T_363; // @[SourceD.scala 276:10]
  wire  _T_364; // @[SourceD.scala 276:10]
  reg  _T_715; // @[Reg.scala 15:16]
  reg [31:0] _RAND_3;
  reg  _T_716; // @[Reg.scala 15:16]
  reg [31:0] _RAND_4;
  reg  _T_717; // @[Reg.scala 15:16]
  reg [31:0] _RAND_5;
  reg  _T_718; // @[Reg.scala 15:16]
  reg [31:0] _RAND_6;
  reg  s3_4_bypass_hwa; // @[Reg.scala 15:16]
  reg [31:0] _RAND_7;
  reg  _T_720; // @[Reg.scala 15:16]
  reg [31:0] _RAND_8;
  reg  s3_5_bypass_hwa; // @[Reg.scala 15:16]
  reg [31:0] _RAND_9;
  reg  _T_722; // @[Reg.scala 15:16]
  reg [31:0] _RAND_10;
  reg  s3_6_bypass_hwa; // @[Reg.scala 15:16]
  reg [31:0] _RAND_11;
  wire  _T_724; // @[SourceD.scala 498:23]
  wire  _T_725; // @[SourceD.scala 498:12]
  wire  _T_729; // @[SourceD.scala 498:82]
  wire  _T_731; // @[SourceD.scala 498:105]
  wire  _T_732; // @[SourceD.scala 498:57]
  wire  _T_733; // @[SourceD.scala 498:39]
  wire  _T_735; // @[SourceD.scala 498:10]
  wire  _T_736; // @[SourceD.scala 498:10]
  assign _T_58 = ~queue_io_enq_valid; // @[SourceD.scala 134:11]
  assign _T_59 = _T_58 | queue_io_enq_ready; // @[SourceD.scala 134:31]
  assign _T_61 = _T_59 | reset; // @[SourceD.scala 134:10]
  assign _T_62 = ~_T_61; // @[SourceD.scala 134:10]
  assign _T_147 = ~s3_valid; // @[SourceD.scala 265:10]
  assign _T_148 = ~s3_req_prio_2; // @[SourceD.scala 265:23]
  assign _T_149 = _T_147 | _T_148; // @[SourceD.scala 265:20]
  assign _T_150 = _T_149 | s3_req_user_hit_cache; // @[SourceD.scala 265:39]
  assign _T_152 = _T_150 | reset; // @[SourceD.scala 265:9]
  assign _T_153 = ~_T_152; // @[SourceD.scala 265:9]
  assign _T_359 = ~s3_need_r; // @[SourceD.scala 276:23]
  assign _T_360 = _T_373 | _T_359; // @[SourceD.scala 276:20]
  assign _T_361 = _T_360 | queue_io_deq_valid; // @[SourceD.scala 276:34]
  assign _T_363 = _T_361 | reset; // @[SourceD.scala 276:10]
  assign _T_364 = ~_T_363; // @[SourceD.scala 276:10]
  assign _T_724 = s3_valid & s3_need_data; // @[SourceD.scala 498:23]
  assign _T_725 = ~_T_724; // @[SourceD.scala 498:12]
  assign _T_729 = s3_4_bypass_hwa | s3_5_bypass_hwa; // @[SourceD.scala 498:82]
  assign _T_731 = _T_729 | s3_6_bypass_hwa; // @[SourceD.scala 498:105]
  assign _T_732 = s3_bypass == _T_731; // @[SourceD.scala 498:57]
  assign _T_733 = _T_725 | _T_732; // @[SourceD.scala 498:39]
  assign _T_735 = _T_733 | reset; // @[SourceD.scala 498:10]
  assign _T_736 = ~_T_735; // @[SourceD.scala 498:10]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  s1_req_reg_user_hit_cache = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  s2_req_user_hit_cache = _RAND_1[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  s3_req_user_hit_cache = _RAND_2[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_715 = _RAND_3[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_716 = _RAND_4[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  _T_717 = _RAND_5[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_718 = _RAND_6[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  s3_4_bypass_hwa = _RAND_7[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_720 = _RAND_8[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  s3_5_bypass_hwa = _RAND_9[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_722 = _RAND_10[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  s3_6_bypass_hwa = _RAND_11[0:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (_T_1) begin
      s1_req_reg_user_hit_cache <= io_req_bits_user_hit_cache;
    end
    if (_T_67) begin
      if (_T) begin
        s2_req_user_hit_cache <= io_req_bits_user_hit_cache;
      end else begin
        s2_req_user_hit_cache <= s1_req_reg_user_hit_cache;
      end
    end
    if (_T_97) begin
      s3_req_user_hit_cache <= s2_req_user_hit_cache;
    end
    if (s1_latch_bypass) begin
      _T_715 <= s1_2_match;
    end
    if (s1_latch_bypass) begin
      _T_716 <= s1_3_match;
    end
    if (s1_latch_bypass) begin
      _T_717 <= s1_4_match;
    end
    if (_T_67) begin
      if (s1_latch_bypass) begin
        _T_718 <= s1_2_match;
      end else begin
        _T_718 <= _T_715;
      end
    end
    if (_T_97) begin
      s3_4_bypass_hwa <= _T_718;
    end
    if (_T_67) begin
      if (s1_latch_bypass) begin
        _T_720 <= s1_3_match;
      end else begin
        _T_720 <= _T_716;
      end
    end
    if (_T_97) begin
      s3_5_bypass_hwa <= _T_720;
    end
    if (_T_67) begin
      if (s1_latch_bypass) begin
        _T_722 <= s1_4_match;
      end else begin
        _T_722 <= _T_717;
      end
    end
    if (_T_97) begin
      s3_6_bypass_hwa <= _T_722;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_62) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at SourceD.scala:134 assert (!queue.io.enq.valid || queue.io.enq.ready)\n"); // @[SourceD.scala 134:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_62) begin
          $fatal; // @[SourceD.scala 134:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_153) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at SourceD.scala:265 assert(!s3_valid || !s3_req.prio(2) || s3_req.user.hit_cache) // make sure that hit_cache is passed correctly\n"); // @[SourceD.scala 265:9]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_153) begin
          $fatal; // @[SourceD.scala 265:9]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_364) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at SourceD.scala:276 assert (!s3_full || !s3_need_r || queue.io.deq.valid)\n"); // @[SourceD.scala 276:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_364) begin
          $fatal; // @[SourceD.scala 276:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_736) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at SourceD.scala:498 assert ( !(s3_valid & s3_need_data) || (s3_bypass.orR === (s3_4_bypass_hwa.orR || s3_5_bypass_hwa.orR || s3_6_bypass_hwa.orR)) )\n"); // @[SourceD.scala 498:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_736) begin
          $fatal; // @[SourceD.scala 498:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
