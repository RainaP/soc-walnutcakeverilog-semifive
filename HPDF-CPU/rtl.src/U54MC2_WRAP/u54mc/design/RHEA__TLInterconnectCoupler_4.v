//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLInterconnectCoupler_4(
  input          rf_reset,
  input          clock,
  input          reset,
  input  [2:0]   auto_tl_master_clock_xing_in_a_bits0_opcode,
  input  [2:0]   auto_tl_master_clock_xing_in_a_bits0_param,
  input  [3:0]   auto_tl_master_clock_xing_in_a_bits0_size,
  input  [4:0]   auto_tl_master_clock_xing_in_a_bits0_source,
  input  [36:0]  auto_tl_master_clock_xing_in_a_bits0_address,
  input          auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_bufferable,
  input          auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_modifiable,
  input          auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_readalloc,
  input          auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_writealloc,
  input          auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_privileged,
  input          auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_secure,
  input          auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_fetch,
  input  [15:0]  auto_tl_master_clock_xing_in_a_bits0_mask,
  input  [127:0] auto_tl_master_clock_xing_in_a_bits0_data,
  input          auto_tl_master_clock_xing_in_a_bits0_corrupt,
  input  [2:0]   auto_tl_master_clock_xing_in_a_bits1_opcode,
  input  [2:0]   auto_tl_master_clock_xing_in_a_bits1_param,
  input  [3:0]   auto_tl_master_clock_xing_in_a_bits1_size,
  input  [4:0]   auto_tl_master_clock_xing_in_a_bits1_source,
  input  [36:0]  auto_tl_master_clock_xing_in_a_bits1_address,
  input          auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_bufferable,
  input          auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_modifiable,
  input          auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_readalloc,
  input          auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_writealloc,
  input          auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_privileged,
  input          auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_secure,
  input          auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_fetch,
  input  [15:0]  auto_tl_master_clock_xing_in_a_bits1_mask,
  input  [127:0] auto_tl_master_clock_xing_in_a_bits1_data,
  input          auto_tl_master_clock_xing_in_a_bits1_corrupt,
  input          auto_tl_master_clock_xing_in_a_valid,
  input  [1:0]   auto_tl_master_clock_xing_in_a_source,
  output         auto_tl_master_clock_xing_in_a_ready,
  output [1:0]   auto_tl_master_clock_xing_in_a_sink,
  output [2:0]   auto_tl_master_clock_xing_in_b_bits0_opcode,
  output [1:0]   auto_tl_master_clock_xing_in_b_bits0_param,
  output [3:0]   auto_tl_master_clock_xing_in_b_bits0_size,
  output [4:0]   auto_tl_master_clock_xing_in_b_bits0_source,
  output [36:0]  auto_tl_master_clock_xing_in_b_bits0_address,
  output [15:0]  auto_tl_master_clock_xing_in_b_bits0_mask,
  output [127:0] auto_tl_master_clock_xing_in_b_bits0_data,
  output         auto_tl_master_clock_xing_in_b_bits0_corrupt,
  output [2:0]   auto_tl_master_clock_xing_in_b_bits1_opcode,
  output [1:0]   auto_tl_master_clock_xing_in_b_bits1_param,
  output [3:0]   auto_tl_master_clock_xing_in_b_bits1_size,
  output [4:0]   auto_tl_master_clock_xing_in_b_bits1_source,
  output [36:0]  auto_tl_master_clock_xing_in_b_bits1_address,
  output [15:0]  auto_tl_master_clock_xing_in_b_bits1_mask,
  output [127:0] auto_tl_master_clock_xing_in_b_bits1_data,
  output         auto_tl_master_clock_xing_in_b_bits1_corrupt,
  output         auto_tl_master_clock_xing_in_b_valid,
  output [1:0]   auto_tl_master_clock_xing_in_b_source,
  input          auto_tl_master_clock_xing_in_b_ready,
  input  [1:0]   auto_tl_master_clock_xing_in_b_sink,
  input  [2:0]   auto_tl_master_clock_xing_in_c_bits0_opcode,
  input  [2:0]   auto_tl_master_clock_xing_in_c_bits0_param,
  input  [3:0]   auto_tl_master_clock_xing_in_c_bits0_size,
  input  [4:0]   auto_tl_master_clock_xing_in_c_bits0_source,
  input  [36:0]  auto_tl_master_clock_xing_in_c_bits0_address,
  input  [127:0] auto_tl_master_clock_xing_in_c_bits0_data,
  input          auto_tl_master_clock_xing_in_c_bits0_corrupt,
  input  [2:0]   auto_tl_master_clock_xing_in_c_bits1_opcode,
  input  [2:0]   auto_tl_master_clock_xing_in_c_bits1_param,
  input  [3:0]   auto_tl_master_clock_xing_in_c_bits1_size,
  input  [4:0]   auto_tl_master_clock_xing_in_c_bits1_source,
  input  [36:0]  auto_tl_master_clock_xing_in_c_bits1_address,
  input  [127:0] auto_tl_master_clock_xing_in_c_bits1_data,
  input          auto_tl_master_clock_xing_in_c_bits1_corrupt,
  input          auto_tl_master_clock_xing_in_c_valid,
  input  [1:0]   auto_tl_master_clock_xing_in_c_source,
  output         auto_tl_master_clock_xing_in_c_ready,
  output [1:0]   auto_tl_master_clock_xing_in_c_sink,
  output [2:0]   auto_tl_master_clock_xing_in_d_bits0_opcode,
  output [1:0]   auto_tl_master_clock_xing_in_d_bits0_param,
  output [3:0]   auto_tl_master_clock_xing_in_d_bits0_size,
  output [4:0]   auto_tl_master_clock_xing_in_d_bits0_source,
  output [4:0]   auto_tl_master_clock_xing_in_d_bits0_sink,
  output         auto_tl_master_clock_xing_in_d_bits0_denied,
  output [127:0] auto_tl_master_clock_xing_in_d_bits0_data,
  output         auto_tl_master_clock_xing_in_d_bits0_corrupt,
  output [2:0]   auto_tl_master_clock_xing_in_d_bits1_opcode,
  output [1:0]   auto_tl_master_clock_xing_in_d_bits1_param,
  output [3:0]   auto_tl_master_clock_xing_in_d_bits1_size,
  output [4:0]   auto_tl_master_clock_xing_in_d_bits1_source,
  output [4:0]   auto_tl_master_clock_xing_in_d_bits1_sink,
  output         auto_tl_master_clock_xing_in_d_bits1_denied,
  output [127:0] auto_tl_master_clock_xing_in_d_bits1_data,
  output         auto_tl_master_clock_xing_in_d_bits1_corrupt,
  output         auto_tl_master_clock_xing_in_d_valid,
  output [1:0]   auto_tl_master_clock_xing_in_d_source,
  input          auto_tl_master_clock_xing_in_d_ready,
  input  [1:0]   auto_tl_master_clock_xing_in_d_sink,
  input  [4:0]   auto_tl_master_clock_xing_in_e_bits0_sink,
  input  [4:0]   auto_tl_master_clock_xing_in_e_bits1_sink,
  input          auto_tl_master_clock_xing_in_e_valid,
  input  [1:0]   auto_tl_master_clock_xing_in_e_source,
  output         auto_tl_master_clock_xing_in_e_ready,
  output [1:0]   auto_tl_master_clock_xing_in_e_sink,
  input          auto_tl_out_a_ready,
  output         auto_tl_out_a_valid,
  output [2:0]   auto_tl_out_a_bits_opcode,
  output [2:0]   auto_tl_out_a_bits_param,
  output [3:0]   auto_tl_out_a_bits_size,
  output [4:0]   auto_tl_out_a_bits_source,
  output [36:0]  auto_tl_out_a_bits_address,
  output         auto_tl_out_a_bits_user_amba_prot_bufferable,
  output         auto_tl_out_a_bits_user_amba_prot_modifiable,
  output         auto_tl_out_a_bits_user_amba_prot_readalloc,
  output         auto_tl_out_a_bits_user_amba_prot_writealloc,
  output         auto_tl_out_a_bits_user_amba_prot_privileged,
  output         auto_tl_out_a_bits_user_amba_prot_secure,
  output         auto_tl_out_a_bits_user_amba_prot_fetch,
  output [15:0]  auto_tl_out_a_bits_mask,
  output [127:0] auto_tl_out_a_bits_data,
  output         auto_tl_out_a_bits_corrupt,
  output         auto_tl_out_b_ready,
  input          auto_tl_out_b_valid,
  input  [2:0]   auto_tl_out_b_bits_opcode,
  input  [1:0]   auto_tl_out_b_bits_param,
  input  [3:0]   auto_tl_out_b_bits_size,
  input  [4:0]   auto_tl_out_b_bits_source,
  input  [36:0]  auto_tl_out_b_bits_address,
  input  [15:0]  auto_tl_out_b_bits_mask,
  input  [127:0] auto_tl_out_b_bits_data,
  input          auto_tl_out_b_bits_corrupt,
  input          auto_tl_out_c_ready,
  output         auto_tl_out_c_valid,
  output [2:0]   auto_tl_out_c_bits_opcode,
  output [2:0]   auto_tl_out_c_bits_param,
  output [3:0]   auto_tl_out_c_bits_size,
  output [4:0]   auto_tl_out_c_bits_source,
  output [36:0]  auto_tl_out_c_bits_address,
  output [127:0] auto_tl_out_c_bits_data,
  output         auto_tl_out_c_bits_corrupt,
  output         auto_tl_out_d_ready,
  input          auto_tl_out_d_valid,
  input  [2:0]   auto_tl_out_d_bits_opcode,
  input  [1:0]   auto_tl_out_d_bits_param,
  input  [3:0]   auto_tl_out_d_bits_size,
  input  [4:0]   auto_tl_out_d_bits_source,
  input  [4:0]   auto_tl_out_d_bits_sink,
  input          auto_tl_out_d_bits_denied,
  input  [127:0] auto_tl_out_d_bits_data,
  input          auto_tl_out_d_bits_corrupt,
  input          auto_tl_out_e_ready,
  output         auto_tl_out_e_valid,
  output [4:0]   auto_tl_out_e_bits_sink
);
  wire  rsink_rf_reset; // @[RationalCrossing.scala 92:27]
  wire  rsink_clock; // @[RationalCrossing.scala 92:27]
  wire  rsink_reset; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_in_a_bits0_opcode; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_in_a_bits0_param; // @[RationalCrossing.scala 92:27]
  wire [3:0] rsink_auto_in_a_bits0_size; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_in_a_bits0_source; // @[RationalCrossing.scala 92:27]
  wire [36:0] rsink_auto_in_a_bits0_address; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits0_user_amba_prot_bufferable; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits0_user_amba_prot_modifiable; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits0_user_amba_prot_readalloc; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits0_user_amba_prot_writealloc; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits0_user_amba_prot_privileged; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits0_user_amba_prot_secure; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits0_user_amba_prot_fetch; // @[RationalCrossing.scala 92:27]
  wire [15:0] rsink_auto_in_a_bits0_mask; // @[RationalCrossing.scala 92:27]
  wire [127:0] rsink_auto_in_a_bits0_data; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits0_corrupt; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_in_a_bits1_opcode; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_in_a_bits1_param; // @[RationalCrossing.scala 92:27]
  wire [3:0] rsink_auto_in_a_bits1_size; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_in_a_bits1_source; // @[RationalCrossing.scala 92:27]
  wire [36:0] rsink_auto_in_a_bits1_address; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits1_user_amba_prot_bufferable; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits1_user_amba_prot_modifiable; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits1_user_amba_prot_readalloc; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits1_user_amba_prot_writealloc; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits1_user_amba_prot_privileged; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits1_user_amba_prot_secure; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits1_user_amba_prot_fetch; // @[RationalCrossing.scala 92:27]
  wire [15:0] rsink_auto_in_a_bits1_mask; // @[RationalCrossing.scala 92:27]
  wire [127:0] rsink_auto_in_a_bits1_data; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_bits1_corrupt; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_valid; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_in_a_source; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_a_ready; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_in_a_sink; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_in_b_bits0_opcode; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_in_b_bits0_param; // @[RationalCrossing.scala 92:27]
  wire [3:0] rsink_auto_in_b_bits0_size; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_in_b_bits0_source; // @[RationalCrossing.scala 92:27]
  wire [36:0] rsink_auto_in_b_bits0_address; // @[RationalCrossing.scala 92:27]
  wire [15:0] rsink_auto_in_b_bits0_mask; // @[RationalCrossing.scala 92:27]
  wire [127:0] rsink_auto_in_b_bits0_data; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_b_bits0_corrupt; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_in_b_bits1_opcode; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_in_b_bits1_param; // @[RationalCrossing.scala 92:27]
  wire [3:0] rsink_auto_in_b_bits1_size; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_in_b_bits1_source; // @[RationalCrossing.scala 92:27]
  wire [36:0] rsink_auto_in_b_bits1_address; // @[RationalCrossing.scala 92:27]
  wire [15:0] rsink_auto_in_b_bits1_mask; // @[RationalCrossing.scala 92:27]
  wire [127:0] rsink_auto_in_b_bits1_data; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_b_bits1_corrupt; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_b_valid; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_in_b_source; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_b_ready; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_in_b_sink; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_in_c_bits0_opcode; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_in_c_bits0_param; // @[RationalCrossing.scala 92:27]
  wire [3:0] rsink_auto_in_c_bits0_size; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_in_c_bits0_source; // @[RationalCrossing.scala 92:27]
  wire [36:0] rsink_auto_in_c_bits0_address; // @[RationalCrossing.scala 92:27]
  wire [127:0] rsink_auto_in_c_bits0_data; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_c_bits0_corrupt; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_in_c_bits1_opcode; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_in_c_bits1_param; // @[RationalCrossing.scala 92:27]
  wire [3:0] rsink_auto_in_c_bits1_size; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_in_c_bits1_source; // @[RationalCrossing.scala 92:27]
  wire [36:0] rsink_auto_in_c_bits1_address; // @[RationalCrossing.scala 92:27]
  wire [127:0] rsink_auto_in_c_bits1_data; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_c_bits1_corrupt; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_c_valid; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_in_c_source; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_c_ready; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_in_c_sink; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_in_d_bits0_opcode; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_in_d_bits0_param; // @[RationalCrossing.scala 92:27]
  wire [3:0] rsink_auto_in_d_bits0_size; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_in_d_bits0_source; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_in_d_bits0_sink; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_d_bits0_denied; // @[RationalCrossing.scala 92:27]
  wire [127:0] rsink_auto_in_d_bits0_data; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_d_bits0_corrupt; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_in_d_bits1_opcode; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_in_d_bits1_param; // @[RationalCrossing.scala 92:27]
  wire [3:0] rsink_auto_in_d_bits1_size; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_in_d_bits1_source; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_in_d_bits1_sink; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_d_bits1_denied; // @[RationalCrossing.scala 92:27]
  wire [127:0] rsink_auto_in_d_bits1_data; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_d_bits1_corrupt; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_d_valid; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_in_d_source; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_d_ready; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_in_d_sink; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_in_e_bits0_sink; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_in_e_bits1_sink; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_e_valid; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_in_e_source; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_in_e_ready; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_in_e_sink; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_a_ready; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_a_valid; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_out_a_bits_opcode; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_out_a_bits_param; // @[RationalCrossing.scala 92:27]
  wire [3:0] rsink_auto_out_a_bits_size; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_out_a_bits_source; // @[RationalCrossing.scala 92:27]
  wire [36:0] rsink_auto_out_a_bits_address; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_a_bits_user_amba_prot_bufferable; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_a_bits_user_amba_prot_modifiable; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_a_bits_user_amba_prot_readalloc; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_a_bits_user_amba_prot_writealloc; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_a_bits_user_amba_prot_privileged; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_a_bits_user_amba_prot_secure; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_a_bits_user_amba_prot_fetch; // @[RationalCrossing.scala 92:27]
  wire [15:0] rsink_auto_out_a_bits_mask; // @[RationalCrossing.scala 92:27]
  wire [127:0] rsink_auto_out_a_bits_data; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_a_bits_corrupt; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_b_ready; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_b_valid; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_out_b_bits_opcode; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_out_b_bits_param; // @[RationalCrossing.scala 92:27]
  wire [3:0] rsink_auto_out_b_bits_size; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_out_b_bits_source; // @[RationalCrossing.scala 92:27]
  wire [36:0] rsink_auto_out_b_bits_address; // @[RationalCrossing.scala 92:27]
  wire [15:0] rsink_auto_out_b_bits_mask; // @[RationalCrossing.scala 92:27]
  wire [127:0] rsink_auto_out_b_bits_data; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_b_bits_corrupt; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_c_ready; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_c_valid; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_out_c_bits_opcode; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_out_c_bits_param; // @[RationalCrossing.scala 92:27]
  wire [3:0] rsink_auto_out_c_bits_size; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_out_c_bits_source; // @[RationalCrossing.scala 92:27]
  wire [36:0] rsink_auto_out_c_bits_address; // @[RationalCrossing.scala 92:27]
  wire [127:0] rsink_auto_out_c_bits_data; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_c_bits_corrupt; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_d_ready; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_d_valid; // @[RationalCrossing.scala 92:27]
  wire [2:0] rsink_auto_out_d_bits_opcode; // @[RationalCrossing.scala 92:27]
  wire [1:0] rsink_auto_out_d_bits_param; // @[RationalCrossing.scala 92:27]
  wire [3:0] rsink_auto_out_d_bits_size; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_out_d_bits_source; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_out_d_bits_sink; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_d_bits_denied; // @[RationalCrossing.scala 92:27]
  wire [127:0] rsink_auto_out_d_bits_data; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_d_bits_corrupt; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_e_ready; // @[RationalCrossing.scala 92:27]
  wire  rsink_auto_out_e_valid; // @[RationalCrossing.scala 92:27]
  wire [4:0] rsink_auto_out_e_bits_sink; // @[RationalCrossing.scala 92:27]
  RHEA__TLRationalCrossingSink rsink ( // @[RationalCrossing.scala 92:27]
    .rf_reset(rsink_rf_reset),
    .clock(rsink_clock),
    .reset(rsink_reset),
    .auto_in_a_bits0_opcode(rsink_auto_in_a_bits0_opcode),
    .auto_in_a_bits0_param(rsink_auto_in_a_bits0_param),
    .auto_in_a_bits0_size(rsink_auto_in_a_bits0_size),
    .auto_in_a_bits0_source(rsink_auto_in_a_bits0_source),
    .auto_in_a_bits0_address(rsink_auto_in_a_bits0_address),
    .auto_in_a_bits0_user_amba_prot_bufferable(rsink_auto_in_a_bits0_user_amba_prot_bufferable),
    .auto_in_a_bits0_user_amba_prot_modifiable(rsink_auto_in_a_bits0_user_amba_prot_modifiable),
    .auto_in_a_bits0_user_amba_prot_readalloc(rsink_auto_in_a_bits0_user_amba_prot_readalloc),
    .auto_in_a_bits0_user_amba_prot_writealloc(rsink_auto_in_a_bits0_user_amba_prot_writealloc),
    .auto_in_a_bits0_user_amba_prot_privileged(rsink_auto_in_a_bits0_user_amba_prot_privileged),
    .auto_in_a_bits0_user_amba_prot_secure(rsink_auto_in_a_bits0_user_amba_prot_secure),
    .auto_in_a_bits0_user_amba_prot_fetch(rsink_auto_in_a_bits0_user_amba_prot_fetch),
    .auto_in_a_bits0_mask(rsink_auto_in_a_bits0_mask),
    .auto_in_a_bits0_data(rsink_auto_in_a_bits0_data),
    .auto_in_a_bits0_corrupt(rsink_auto_in_a_bits0_corrupt),
    .auto_in_a_bits1_opcode(rsink_auto_in_a_bits1_opcode),
    .auto_in_a_bits1_param(rsink_auto_in_a_bits1_param),
    .auto_in_a_bits1_size(rsink_auto_in_a_bits1_size),
    .auto_in_a_bits1_source(rsink_auto_in_a_bits1_source),
    .auto_in_a_bits1_address(rsink_auto_in_a_bits1_address),
    .auto_in_a_bits1_user_amba_prot_bufferable(rsink_auto_in_a_bits1_user_amba_prot_bufferable),
    .auto_in_a_bits1_user_amba_prot_modifiable(rsink_auto_in_a_bits1_user_amba_prot_modifiable),
    .auto_in_a_bits1_user_amba_prot_readalloc(rsink_auto_in_a_bits1_user_amba_prot_readalloc),
    .auto_in_a_bits1_user_amba_prot_writealloc(rsink_auto_in_a_bits1_user_amba_prot_writealloc),
    .auto_in_a_bits1_user_amba_prot_privileged(rsink_auto_in_a_bits1_user_amba_prot_privileged),
    .auto_in_a_bits1_user_amba_prot_secure(rsink_auto_in_a_bits1_user_amba_prot_secure),
    .auto_in_a_bits1_user_amba_prot_fetch(rsink_auto_in_a_bits1_user_amba_prot_fetch),
    .auto_in_a_bits1_mask(rsink_auto_in_a_bits1_mask),
    .auto_in_a_bits1_data(rsink_auto_in_a_bits1_data),
    .auto_in_a_bits1_corrupt(rsink_auto_in_a_bits1_corrupt),
    .auto_in_a_valid(rsink_auto_in_a_valid),
    .auto_in_a_source(rsink_auto_in_a_source),
    .auto_in_a_ready(rsink_auto_in_a_ready),
    .auto_in_a_sink(rsink_auto_in_a_sink),
    .auto_in_b_bits0_opcode(rsink_auto_in_b_bits0_opcode),
    .auto_in_b_bits0_param(rsink_auto_in_b_bits0_param),
    .auto_in_b_bits0_size(rsink_auto_in_b_bits0_size),
    .auto_in_b_bits0_source(rsink_auto_in_b_bits0_source),
    .auto_in_b_bits0_address(rsink_auto_in_b_bits0_address),
    .auto_in_b_bits0_mask(rsink_auto_in_b_bits0_mask),
    .auto_in_b_bits0_data(rsink_auto_in_b_bits0_data),
    .auto_in_b_bits0_corrupt(rsink_auto_in_b_bits0_corrupt),
    .auto_in_b_bits1_opcode(rsink_auto_in_b_bits1_opcode),
    .auto_in_b_bits1_param(rsink_auto_in_b_bits1_param),
    .auto_in_b_bits1_size(rsink_auto_in_b_bits1_size),
    .auto_in_b_bits1_source(rsink_auto_in_b_bits1_source),
    .auto_in_b_bits1_address(rsink_auto_in_b_bits1_address),
    .auto_in_b_bits1_mask(rsink_auto_in_b_bits1_mask),
    .auto_in_b_bits1_data(rsink_auto_in_b_bits1_data),
    .auto_in_b_bits1_corrupt(rsink_auto_in_b_bits1_corrupt),
    .auto_in_b_valid(rsink_auto_in_b_valid),
    .auto_in_b_source(rsink_auto_in_b_source),
    .auto_in_b_ready(rsink_auto_in_b_ready),
    .auto_in_b_sink(rsink_auto_in_b_sink),
    .auto_in_c_bits0_opcode(rsink_auto_in_c_bits0_opcode),
    .auto_in_c_bits0_param(rsink_auto_in_c_bits0_param),
    .auto_in_c_bits0_size(rsink_auto_in_c_bits0_size),
    .auto_in_c_bits0_source(rsink_auto_in_c_bits0_source),
    .auto_in_c_bits0_address(rsink_auto_in_c_bits0_address),
    .auto_in_c_bits0_data(rsink_auto_in_c_bits0_data),
    .auto_in_c_bits0_corrupt(rsink_auto_in_c_bits0_corrupt),
    .auto_in_c_bits1_opcode(rsink_auto_in_c_bits1_opcode),
    .auto_in_c_bits1_param(rsink_auto_in_c_bits1_param),
    .auto_in_c_bits1_size(rsink_auto_in_c_bits1_size),
    .auto_in_c_bits1_source(rsink_auto_in_c_bits1_source),
    .auto_in_c_bits1_address(rsink_auto_in_c_bits1_address),
    .auto_in_c_bits1_data(rsink_auto_in_c_bits1_data),
    .auto_in_c_bits1_corrupt(rsink_auto_in_c_bits1_corrupt),
    .auto_in_c_valid(rsink_auto_in_c_valid),
    .auto_in_c_source(rsink_auto_in_c_source),
    .auto_in_c_ready(rsink_auto_in_c_ready),
    .auto_in_c_sink(rsink_auto_in_c_sink),
    .auto_in_d_bits0_opcode(rsink_auto_in_d_bits0_opcode),
    .auto_in_d_bits0_param(rsink_auto_in_d_bits0_param),
    .auto_in_d_bits0_size(rsink_auto_in_d_bits0_size),
    .auto_in_d_bits0_source(rsink_auto_in_d_bits0_source),
    .auto_in_d_bits0_sink(rsink_auto_in_d_bits0_sink),
    .auto_in_d_bits0_denied(rsink_auto_in_d_bits0_denied),
    .auto_in_d_bits0_data(rsink_auto_in_d_bits0_data),
    .auto_in_d_bits0_corrupt(rsink_auto_in_d_bits0_corrupt),
    .auto_in_d_bits1_opcode(rsink_auto_in_d_bits1_opcode),
    .auto_in_d_bits1_param(rsink_auto_in_d_bits1_param),
    .auto_in_d_bits1_size(rsink_auto_in_d_bits1_size),
    .auto_in_d_bits1_source(rsink_auto_in_d_bits1_source),
    .auto_in_d_bits1_sink(rsink_auto_in_d_bits1_sink),
    .auto_in_d_bits1_denied(rsink_auto_in_d_bits1_denied),
    .auto_in_d_bits1_data(rsink_auto_in_d_bits1_data),
    .auto_in_d_bits1_corrupt(rsink_auto_in_d_bits1_corrupt),
    .auto_in_d_valid(rsink_auto_in_d_valid),
    .auto_in_d_source(rsink_auto_in_d_source),
    .auto_in_d_ready(rsink_auto_in_d_ready),
    .auto_in_d_sink(rsink_auto_in_d_sink),
    .auto_in_e_bits0_sink(rsink_auto_in_e_bits0_sink),
    .auto_in_e_bits1_sink(rsink_auto_in_e_bits1_sink),
    .auto_in_e_valid(rsink_auto_in_e_valid),
    .auto_in_e_source(rsink_auto_in_e_source),
    .auto_in_e_ready(rsink_auto_in_e_ready),
    .auto_in_e_sink(rsink_auto_in_e_sink),
    .auto_out_a_ready(rsink_auto_out_a_ready),
    .auto_out_a_valid(rsink_auto_out_a_valid),
    .auto_out_a_bits_opcode(rsink_auto_out_a_bits_opcode),
    .auto_out_a_bits_param(rsink_auto_out_a_bits_param),
    .auto_out_a_bits_size(rsink_auto_out_a_bits_size),
    .auto_out_a_bits_source(rsink_auto_out_a_bits_source),
    .auto_out_a_bits_address(rsink_auto_out_a_bits_address),
    .auto_out_a_bits_user_amba_prot_bufferable(rsink_auto_out_a_bits_user_amba_prot_bufferable),
    .auto_out_a_bits_user_amba_prot_modifiable(rsink_auto_out_a_bits_user_amba_prot_modifiable),
    .auto_out_a_bits_user_amba_prot_readalloc(rsink_auto_out_a_bits_user_amba_prot_readalloc),
    .auto_out_a_bits_user_amba_prot_writealloc(rsink_auto_out_a_bits_user_amba_prot_writealloc),
    .auto_out_a_bits_user_amba_prot_privileged(rsink_auto_out_a_bits_user_amba_prot_privileged),
    .auto_out_a_bits_user_amba_prot_secure(rsink_auto_out_a_bits_user_amba_prot_secure),
    .auto_out_a_bits_user_amba_prot_fetch(rsink_auto_out_a_bits_user_amba_prot_fetch),
    .auto_out_a_bits_mask(rsink_auto_out_a_bits_mask),
    .auto_out_a_bits_data(rsink_auto_out_a_bits_data),
    .auto_out_a_bits_corrupt(rsink_auto_out_a_bits_corrupt),
    .auto_out_b_ready(rsink_auto_out_b_ready),
    .auto_out_b_valid(rsink_auto_out_b_valid),
    .auto_out_b_bits_opcode(rsink_auto_out_b_bits_opcode),
    .auto_out_b_bits_param(rsink_auto_out_b_bits_param),
    .auto_out_b_bits_size(rsink_auto_out_b_bits_size),
    .auto_out_b_bits_source(rsink_auto_out_b_bits_source),
    .auto_out_b_bits_address(rsink_auto_out_b_bits_address),
    .auto_out_b_bits_mask(rsink_auto_out_b_bits_mask),
    .auto_out_b_bits_data(rsink_auto_out_b_bits_data),
    .auto_out_b_bits_corrupt(rsink_auto_out_b_bits_corrupt),
    .auto_out_c_ready(rsink_auto_out_c_ready),
    .auto_out_c_valid(rsink_auto_out_c_valid),
    .auto_out_c_bits_opcode(rsink_auto_out_c_bits_opcode),
    .auto_out_c_bits_param(rsink_auto_out_c_bits_param),
    .auto_out_c_bits_size(rsink_auto_out_c_bits_size),
    .auto_out_c_bits_source(rsink_auto_out_c_bits_source),
    .auto_out_c_bits_address(rsink_auto_out_c_bits_address),
    .auto_out_c_bits_data(rsink_auto_out_c_bits_data),
    .auto_out_c_bits_corrupt(rsink_auto_out_c_bits_corrupt),
    .auto_out_d_ready(rsink_auto_out_d_ready),
    .auto_out_d_valid(rsink_auto_out_d_valid),
    .auto_out_d_bits_opcode(rsink_auto_out_d_bits_opcode),
    .auto_out_d_bits_param(rsink_auto_out_d_bits_param),
    .auto_out_d_bits_size(rsink_auto_out_d_bits_size),
    .auto_out_d_bits_source(rsink_auto_out_d_bits_source),
    .auto_out_d_bits_sink(rsink_auto_out_d_bits_sink),
    .auto_out_d_bits_denied(rsink_auto_out_d_bits_denied),
    .auto_out_d_bits_data(rsink_auto_out_d_bits_data),
    .auto_out_d_bits_corrupt(rsink_auto_out_d_bits_corrupt),
    .auto_out_e_ready(rsink_auto_out_e_ready),
    .auto_out_e_valid(rsink_auto_out_e_valid),
    .auto_out_e_bits_sink(rsink_auto_out_e_bits_sink)
  );
  assign rsink_rf_reset = rf_reset;
  assign auto_tl_master_clock_xing_in_a_ready = rsink_auto_in_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_a_sink = rsink_auto_in_a_sink; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits0_opcode = rsink_auto_in_b_bits0_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits0_param = rsink_auto_in_b_bits0_param; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits0_size = rsink_auto_in_b_bits0_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits0_source = rsink_auto_in_b_bits0_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits0_address = rsink_auto_in_b_bits0_address; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits0_mask = rsink_auto_in_b_bits0_mask; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits0_data = rsink_auto_in_b_bits0_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits0_corrupt = rsink_auto_in_b_bits0_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits1_opcode = rsink_auto_in_b_bits1_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits1_param = rsink_auto_in_b_bits1_param; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits1_size = rsink_auto_in_b_bits1_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits1_source = rsink_auto_in_b_bits1_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits1_address = rsink_auto_in_b_bits1_address; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits1_mask = rsink_auto_in_b_bits1_mask; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits1_data = rsink_auto_in_b_bits1_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_bits1_corrupt = rsink_auto_in_b_bits1_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_valid = rsink_auto_in_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_b_source = rsink_auto_in_b_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_c_ready = rsink_auto_in_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_c_sink = rsink_auto_in_c_sink; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits0_opcode = rsink_auto_in_d_bits0_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits0_param = rsink_auto_in_d_bits0_param; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits0_size = rsink_auto_in_d_bits0_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits0_source = rsink_auto_in_d_bits0_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits0_sink = rsink_auto_in_d_bits0_sink; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits0_denied = rsink_auto_in_d_bits0_denied; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits0_data = rsink_auto_in_d_bits0_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits0_corrupt = rsink_auto_in_d_bits0_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits1_opcode = rsink_auto_in_d_bits1_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits1_param = rsink_auto_in_d_bits1_param; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits1_size = rsink_auto_in_d_bits1_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits1_source = rsink_auto_in_d_bits1_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits1_sink = rsink_auto_in_d_bits1_sink; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits1_denied = rsink_auto_in_d_bits1_denied; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits1_data = rsink_auto_in_d_bits1_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_bits1_corrupt = rsink_auto_in_d_bits1_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_valid = rsink_auto_in_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_d_source = rsink_auto_in_d_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_e_ready = rsink_auto_in_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_master_clock_xing_in_e_sink = rsink_auto_in_e_sink; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_out_a_valid = rsink_auto_out_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_opcode = rsink_auto_out_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_param = rsink_auto_out_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_size = rsink_auto_out_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_source = rsink_auto_out_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_address = rsink_auto_out_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_user_amba_prot_bufferable = rsink_auto_out_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_user_amba_prot_modifiable = rsink_auto_out_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_user_amba_prot_readalloc = rsink_auto_out_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_user_amba_prot_writealloc = rsink_auto_out_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_user_amba_prot_privileged = rsink_auto_out_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_user_amba_prot_secure = rsink_auto_out_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_user_amba_prot_fetch = rsink_auto_out_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_mask = rsink_auto_out_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_data = rsink_auto_out_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_corrupt = rsink_auto_out_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_b_ready = rsink_auto_out_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_c_valid = rsink_auto_out_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_c_bits_opcode = rsink_auto_out_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_c_bits_param = rsink_auto_out_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_c_bits_size = rsink_auto_out_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_c_bits_source = rsink_auto_out_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_c_bits_address = rsink_auto_out_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_c_bits_data = rsink_auto_out_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_c_bits_corrupt = rsink_auto_out_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_d_ready = rsink_auto_out_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_e_valid = rsink_auto_out_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_e_bits_sink = rsink_auto_out_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign rsink_clock = clock;
  assign rsink_reset = reset;
  assign rsink_auto_in_a_bits0_opcode = auto_tl_master_clock_xing_in_a_bits0_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits0_param = auto_tl_master_clock_xing_in_a_bits0_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits0_size = auto_tl_master_clock_xing_in_a_bits0_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits0_source = auto_tl_master_clock_xing_in_a_bits0_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits0_address = auto_tl_master_clock_xing_in_a_bits0_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits0_user_amba_prot_bufferable =
    auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits0_user_amba_prot_modifiable =
    auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits0_user_amba_prot_readalloc = auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits0_user_amba_prot_writealloc =
    auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits0_user_amba_prot_privileged =
    auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits0_user_amba_prot_secure = auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits0_user_amba_prot_fetch = auto_tl_master_clock_xing_in_a_bits0_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits0_mask = auto_tl_master_clock_xing_in_a_bits0_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits0_data = auto_tl_master_clock_xing_in_a_bits0_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits0_corrupt = auto_tl_master_clock_xing_in_a_bits0_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits1_opcode = auto_tl_master_clock_xing_in_a_bits1_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits1_param = auto_tl_master_clock_xing_in_a_bits1_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits1_size = auto_tl_master_clock_xing_in_a_bits1_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits1_source = auto_tl_master_clock_xing_in_a_bits1_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits1_address = auto_tl_master_clock_xing_in_a_bits1_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits1_user_amba_prot_bufferable =
    auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits1_user_amba_prot_modifiable =
    auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits1_user_amba_prot_readalloc = auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits1_user_amba_prot_writealloc =
    auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits1_user_amba_prot_privileged =
    auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits1_user_amba_prot_secure = auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits1_user_amba_prot_fetch = auto_tl_master_clock_xing_in_a_bits1_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits1_mask = auto_tl_master_clock_xing_in_a_bits1_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits1_data = auto_tl_master_clock_xing_in_a_bits1_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_bits1_corrupt = auto_tl_master_clock_xing_in_a_bits1_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_valid = auto_tl_master_clock_xing_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_a_source = auto_tl_master_clock_xing_in_a_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_b_ready = auto_tl_master_clock_xing_in_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_b_sink = auto_tl_master_clock_xing_in_b_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_bits0_opcode = auto_tl_master_clock_xing_in_c_bits0_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_bits0_param = auto_tl_master_clock_xing_in_c_bits0_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_bits0_size = auto_tl_master_clock_xing_in_c_bits0_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_bits0_source = auto_tl_master_clock_xing_in_c_bits0_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_bits0_address = auto_tl_master_clock_xing_in_c_bits0_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_bits0_data = auto_tl_master_clock_xing_in_c_bits0_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_bits0_corrupt = auto_tl_master_clock_xing_in_c_bits0_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_bits1_opcode = auto_tl_master_clock_xing_in_c_bits1_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_bits1_param = auto_tl_master_clock_xing_in_c_bits1_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_bits1_size = auto_tl_master_clock_xing_in_c_bits1_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_bits1_source = auto_tl_master_clock_xing_in_c_bits1_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_bits1_address = auto_tl_master_clock_xing_in_c_bits1_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_bits1_data = auto_tl_master_clock_xing_in_c_bits1_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_bits1_corrupt = auto_tl_master_clock_xing_in_c_bits1_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_valid = auto_tl_master_clock_xing_in_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_c_source = auto_tl_master_clock_xing_in_c_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_d_ready = auto_tl_master_clock_xing_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_d_sink = auto_tl_master_clock_xing_in_d_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_e_bits0_sink = auto_tl_master_clock_xing_in_e_bits0_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_e_bits1_sink = auto_tl_master_clock_xing_in_e_bits1_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_e_valid = auto_tl_master_clock_xing_in_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_in_e_source = auto_tl_master_clock_xing_in_e_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsink_auto_out_a_ready = auto_tl_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_b_valid = auto_tl_out_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_b_bits_opcode = auto_tl_out_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_b_bits_param = auto_tl_out_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_b_bits_size = auto_tl_out_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_b_bits_source = auto_tl_out_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_b_bits_address = auto_tl_out_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_b_bits_mask = auto_tl_out_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_b_bits_data = auto_tl_out_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_b_bits_corrupt = auto_tl_out_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_c_ready = auto_tl_out_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_d_valid = auto_tl_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_d_bits_opcode = auto_tl_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_d_bits_param = auto_tl_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_d_bits_size = auto_tl_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_d_bits_source = auto_tl_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_d_bits_sink = auto_tl_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_d_bits_denied = auto_tl_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_d_bits_data = auto_tl_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_d_bits_corrupt = auto_tl_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsink_auto_out_e_ready = auto_tl_out_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
endmodule
