//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__AXI4ToTL(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_aw_ready,
  input         auto_in_aw_valid,
  input  [1:0]  auto_in_aw_bits_id,
  input  [36:0] auto_in_aw_bits_addr,
  input  [7:0]  auto_in_aw_bits_len,
  input  [2:0]  auto_in_aw_bits_size,
  input  [3:0]  auto_in_aw_bits_cache,
  input  [2:0]  auto_in_aw_bits_prot,
  output        auto_in_w_ready,
  input         auto_in_w_valid,
  input  [63:0] auto_in_w_bits_data,
  input  [7:0]  auto_in_w_bits_strb,
  input         auto_in_w_bits_last,
  input         auto_in_b_ready,
  output        auto_in_b_valid,
  output [1:0]  auto_in_b_bits_id,
  output [1:0]  auto_in_b_bits_resp,
  output        auto_in_ar_ready,
  input         auto_in_ar_valid,
  input  [1:0]  auto_in_ar_bits_id,
  input  [36:0] auto_in_ar_bits_addr,
  input  [7:0]  auto_in_ar_bits_len,
  input  [2:0]  auto_in_ar_bits_size,
  input  [3:0]  auto_in_ar_bits_cache,
  input  [2:0]  auto_in_ar_bits_prot,
  input         auto_in_r_ready,
  output        auto_in_r_valid,
  output [1:0]  auto_in_r_bits_id,
  output [63:0] auto_in_r_bits_data,
  output [1:0]  auto_in_r_bits_resp,
  output        auto_in_r_bits_last,
  input         auto_out_a_ready,
  output        auto_out_a_valid,
  output [2:0]  auto_out_a_bits_opcode,
  output [3:0]  auto_out_a_bits_size,
  output [4:0]  auto_out_a_bits_source,
  output [36:0] auto_out_a_bits_address,
  output        auto_out_a_bits_user_amba_prot_bufferable,
  output        auto_out_a_bits_user_amba_prot_modifiable,
  output        auto_out_a_bits_user_amba_prot_readalloc,
  output        auto_out_a_bits_user_amba_prot_writealloc,
  output        auto_out_a_bits_user_amba_prot_privileged,
  output        auto_out_a_bits_user_amba_prot_secure,
  output        auto_out_a_bits_user_amba_prot_fetch,
  output [7:0]  auto_out_a_bits_mask,
  output [63:0] auto_out_a_bits_data,
  output        auto_out_d_ready,
  input         auto_out_d_valid,
  input  [2:0]  auto_out_d_bits_opcode,
  input  [3:0]  auto_out_d_bits_size,
  input  [4:0]  auto_out_d_bits_source,
  input         auto_out_d_bits_denied,
  input  [63:0] auto_out_d_bits_data,
  input         auto_out_d_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
`endif // RANDOMIZE_REG_INIT
  wire  deq_rf_reset; // @[Decoupled.scala 296:21]
  wire  deq_clock; // @[Decoupled.scala 296:21]
  wire  deq_reset; // @[Decoupled.scala 296:21]
  wire  deq_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  deq_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [1:0] deq_io_enq_bits_id; // @[Decoupled.scala 296:21]
  wire [63:0] deq_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire [1:0] deq_io_enq_bits_resp; // @[Decoupled.scala 296:21]
  wire  deq_io_enq_bits_last; // @[Decoupled.scala 296:21]
  wire  deq_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  deq_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [1:0] deq_io_deq_bits_id; // @[Decoupled.scala 296:21]
  wire [63:0] deq_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire [1:0] deq_io_deq_bits_resp; // @[Decoupled.scala 296:21]
  wire  deq_io_deq_bits_last; // @[Decoupled.scala 296:21]
  wire  q_b_deq_rf_reset; // @[Decoupled.scala 296:21]
  wire  q_b_deq_clock; // @[Decoupled.scala 296:21]
  wire  q_b_deq_reset; // @[Decoupled.scala 296:21]
  wire  q_b_deq_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  q_b_deq_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [1:0] q_b_deq_io_enq_bits_id; // @[Decoupled.scala 296:21]
  wire [1:0] q_b_deq_io_enq_bits_resp; // @[Decoupled.scala 296:21]
  wire  q_b_deq_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  q_b_deq_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [1:0] q_b_deq_io_deq_bits_id; // @[Decoupled.scala 296:21]
  wire [1:0] q_b_deq_io_deq_bits_resp; // @[Decoupled.scala 296:21]
  wire [15:0] _r_size1_T = {auto_in_ar_bits_len,8'hff}; // @[Cat.scala 30:58]
  wire [22:0] _GEN_30 = {{7'd0}, _r_size1_T}; // @[Bundles.scala 33:21]
  wire [22:0] _r_size1_T_1 = _GEN_30 << auto_in_ar_bits_size; // @[Bundles.scala 33:21]
  wire [14:0] r_size_lo = _r_size1_T_1[22:8]; // @[Bundles.scala 33:30]
  wire [15:0] _r_size_T = {r_size_lo, 1'h0}; // @[package.scala 232:35]
  wire [15:0] _r_size_T_1 = _r_size_T | 16'h1; // @[package.scala 232:40]
  wire [15:0] _r_size_T_2 = {1'h0,r_size_lo}; // @[Cat.scala 30:58]
  wire [15:0] _r_size_T_3 = ~_r_size_T_2; // @[package.scala 232:53]
  wire [15:0] _r_size_T_4 = _r_size_T_1 & _r_size_T_3; // @[package.scala 232:51]
  wire [7:0] r_size_hi = _r_size_T_4[15:8]; // @[OneHot.scala 30:18]
  wire [7:0] r_size_lo_1 = _r_size_T_4[7:0]; // @[OneHot.scala 31:18]
  wire  r_size_hi_1 = |r_size_hi; // @[OneHot.scala 32:14]
  wire [7:0] _r_size_T_5 = r_size_hi | r_size_lo_1; // @[OneHot.scala 32:28]
  wire [3:0] r_size_hi_2 = _r_size_T_5[7:4]; // @[OneHot.scala 30:18]
  wire [3:0] r_size_lo_2 = _r_size_T_5[3:0]; // @[OneHot.scala 31:18]
  wire  r_size_hi_3 = |r_size_hi_2; // @[OneHot.scala 32:14]
  wire [3:0] _r_size_T_6 = r_size_hi_2 | r_size_lo_2; // @[OneHot.scala 32:28]
  wire [1:0] r_size_hi_4 = _r_size_T_6[3:2]; // @[OneHot.scala 30:18]
  wire [1:0] r_size_lo_3 = _r_size_T_6[1:0]; // @[OneHot.scala 31:18]
  wire  r_size_hi_5 = |r_size_hi_4; // @[OneHot.scala 32:14]
  wire [1:0] _r_size_T_7 = r_size_hi_4 | r_size_lo_3; // @[OneHot.scala 32:28]
  wire  r_size_lo_4 = _r_size_T_7[1]; // @[CircuitMath.scala 30:8]
  wire [3:0] r_size = {r_size_hi_1,r_size_hi_3,r_size_hi_5,r_size_lo_4}; // @[Cat.scala 30:58]
  wire  _r_ok_T_1 = r_size <= 4'h6; // @[Parameters.scala 92:42]
  wire [37:0] _r_ok_T_5 = {1'b0,$signed(auto_in_ar_bits_addr)}; // @[Parameters.scala 137:49]
  wire [37:0] _r_ok_T_7 = $signed(_r_ok_T_5) & -38'sh5000; // @[Parameters.scala 137:52]
  wire  _r_ok_T_8 = $signed(_r_ok_T_7) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _r_ok_T_9 = auto_in_ar_bits_addr ^ 37'h2000000; // @[Parameters.scala 137:31]
  wire [37:0] _r_ok_T_10 = {1'b0,$signed(_r_ok_T_9)}; // @[Parameters.scala 137:49]
  wire [37:0] _r_ok_T_12 = $signed(_r_ok_T_10) & -38'sh10000; // @[Parameters.scala 137:52]
  wire  _r_ok_T_13 = $signed(_r_ok_T_12) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _r_ok_T_14 = auto_in_ar_bits_addr ^ 37'h2010000; // @[Parameters.scala 137:31]
  wire [37:0] _r_ok_T_15 = {1'b0,$signed(_r_ok_T_14)}; // @[Parameters.scala 137:49]
  wire [37:0] _r_ok_T_17 = $signed(_r_ok_T_15) & -38'sh4000; // @[Parameters.scala 137:52]
  wire  _r_ok_T_18 = $signed(_r_ok_T_17) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _r_ok_T_19 = auto_in_ar_bits_addr ^ 37'h8000000; // @[Parameters.scala 137:31]
  wire [37:0] _r_ok_T_20 = {1'b0,$signed(_r_ok_T_19)}; // @[Parameters.scala 137:49]
  wire [37:0] _r_ok_T_22 = $signed(_r_ok_T_20) & -38'sh2200000; // @[Parameters.scala 137:52]
  wire  _r_ok_T_23 = $signed(_r_ok_T_22) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _r_ok_T_24 = auto_in_ar_bits_addr ^ 37'hc000000; // @[Parameters.scala 137:31]
  wire [37:0] _r_ok_T_25 = {1'b0,$signed(_r_ok_T_24)}; // @[Parameters.scala 137:49]
  wire [37:0] _r_ok_T_27 = $signed(_r_ok_T_25) & -38'sh4000000; // @[Parameters.scala 137:52]
  wire  _r_ok_T_28 = $signed(_r_ok_T_27) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _r_ok_T_29 = auto_in_ar_bits_addr ^ 37'h10000000; // @[Parameters.scala 137:31]
  wire [37:0] _r_ok_T_30 = {1'b0,$signed(_r_ok_T_29)}; // @[Parameters.scala 137:49]
  wire [37:0] _r_ok_T_32 = $signed(_r_ok_T_30) & -38'sh2000; // @[Parameters.scala 137:52]
  wire  _r_ok_T_33 = $signed(_r_ok_T_32) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _r_ok_T_34 = auto_in_ar_bits_addr ^ 37'h10003000; // @[Parameters.scala 137:31]
  wire [37:0] _r_ok_T_35 = {1'b0,$signed(_r_ok_T_34)}; // @[Parameters.scala 137:49]
  wire [37:0] _r_ok_T_37 = $signed(_r_ok_T_35) & -38'sh1000; // @[Parameters.scala 137:52]
  wire  _r_ok_T_38 = $signed(_r_ok_T_37) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _r_ok_T_39 = auto_in_ar_bits_addr ^ 37'h80000000; // @[Parameters.scala 137:31]
  wire [37:0] _r_ok_T_40 = {1'b0,$signed(_r_ok_T_39)}; // @[Parameters.scala 137:49]
  wire [37:0] _r_ok_T_42 = $signed(_r_ok_T_40) & -38'sh80000000; // @[Parameters.scala 137:52]
  wire  _r_ok_T_43 = $signed(_r_ok_T_42) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _r_ok_T_44 = auto_in_ar_bits_addr ^ 37'h800000000; // @[Parameters.scala 137:31]
  wire [37:0] _r_ok_T_45 = {1'b0,$signed(_r_ok_T_44)}; // @[Parameters.scala 137:49]
  wire [37:0] _r_ok_T_47 = $signed(_r_ok_T_45) & -38'sh800000000; // @[Parameters.scala 137:52]
  wire  _r_ok_T_48 = $signed(_r_ok_T_47) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _r_ok_T_49 = auto_in_ar_bits_addr ^ 37'h1000000000; // @[Parameters.scala 137:31]
  wire [37:0] _r_ok_T_50 = {1'b0,$signed(_r_ok_T_49)}; // @[Parameters.scala 137:49]
  wire [37:0] _r_ok_T_52 = $signed(_r_ok_T_50) & -38'sh1000000000; // @[Parameters.scala 137:52]
  wire  _r_ok_T_53 = $signed(_r_ok_T_52) == 38'sh0; // @[Parameters.scala 137:67]
  wire  _r_ok_T_62 = _r_ok_T_8 | _r_ok_T_13 | _r_ok_T_18 | _r_ok_T_23 | _r_ok_T_28 | _r_ok_T_33 | _r_ok_T_38 |
    _r_ok_T_43 | _r_ok_T_48 | _r_ok_T_53; // @[Parameters.scala 686:42]
  wire  _r_ok_T_63 = _r_ok_T_1 & _r_ok_T_62; // @[Parameters.scala 685:56]
  wire  _r_ok_T_65 = r_size <= 4'h8; // @[Parameters.scala 92:42]
  wire [36:0] _r_ok_T_68 = auto_in_ar_bits_addr ^ 37'h3000; // @[Parameters.scala 137:31]
  wire [37:0] _r_ok_T_69 = {1'b0,$signed(_r_ok_T_68)}; // @[Parameters.scala 137:49]
  wire [37:0] _r_ok_T_71 = $signed(_r_ok_T_69) & -38'sh1000; // @[Parameters.scala 137:52]
  wire  _r_ok_T_72 = $signed(_r_ok_T_71) == 38'sh0; // @[Parameters.scala 137:67]
  wire  _r_ok_T_73 = _r_ok_T_65 & _r_ok_T_72; // @[Parameters.scala 685:56]
  wire  r_ok = _r_ok_T_63 | _r_ok_T_73; // @[Parameters.scala 687:30]
  wire [13:0] _GEN_31 = {{11'd0}, auto_in_ar_bits_addr[2:0]}; // @[ToTL.scala 91:59]
  wire [13:0] _r_addr_T_1 = 14'h3000 | _GEN_31; // @[ToTL.scala 91:59]
  wire [36:0] r_addr = r_ok ? auto_in_ar_bits_addr : {{23'd0}, _r_addr_T_1}; // @[ToTL.scala 91:23]
  reg [2:0] r_count_0; // @[ToTL.scala 92:28]
  reg [2:0] r_count_1; // @[ToTL.scala 92:28]
  reg [2:0] r_count_2; // @[ToTL.scala 92:28]
  reg [2:0] r_count_3; // @[ToTL.scala 92:28]
  wire [2:0] _GEN_1 = 2'h1 == auto_in_ar_bits_id ? r_count_1 : r_count_0; // @[ToTL.scala 96:50 ToTL.scala 96:50]
  wire [2:0] _GEN_2 = 2'h2 == auto_in_ar_bits_id ? r_count_2 : _GEN_1; // @[ToTL.scala 96:50 ToTL.scala 96:50]
  wire [2:0] _GEN_3 = 2'h3 == auto_in_ar_bits_id ? r_count_3 : _GEN_2; // @[ToTL.scala 96:50 ToTL.scala 96:50]
  wire [1:0] r_id_hi_lo = _GEN_3[1:0]; // @[ToTL.scala 96:50]
  wire [4:0] r_id = {auto_in_ar_bits_id,r_id_hi_lo,1'h0}; // @[Cat.scala 30:58]
  wire [1:0] a_mask_sizeOH_shiftAmount = r_size[1:0]; // @[OneHot.scala 64:49]
  wire [3:0] _a_mask_sizeOH_T_1 = 4'h1 << a_mask_sizeOH_shiftAmount; // @[OneHot.scala 65:12]
  wire [2:0] a_mask_sizeOH = _a_mask_sizeOH_T_1[2:0] | 3'h1; // @[Misc.scala 207:81]
  wire  _a_mask_T = r_size >= 4'h3; // @[Misc.scala 211:21]
  wire  a_mask_size = a_mask_sizeOH[2]; // @[Misc.scala 214:26]
  wire  a_mask_bit = r_addr[2]; // @[Misc.scala 215:26]
  wire  a_mask_nbit = ~a_mask_bit; // @[Misc.scala 216:20]
  wire  a_mask_acc = _a_mask_T | a_mask_size & a_mask_nbit; // @[Misc.scala 220:29]
  wire  a_mask_acc_1 = _a_mask_T | a_mask_size & a_mask_bit; // @[Misc.scala 220:29]
  wire  a_mask_size_1 = a_mask_sizeOH[1]; // @[Misc.scala 214:26]
  wire  a_mask_bit_1 = r_addr[1]; // @[Misc.scala 215:26]
  wire  a_mask_nbit_1 = ~a_mask_bit_1; // @[Misc.scala 216:20]
  wire  a_mask_eq_2 = a_mask_nbit & a_mask_nbit_1; // @[Misc.scala 219:27]
  wire  a_mask_acc_2 = a_mask_acc | a_mask_size_1 & a_mask_eq_2; // @[Misc.scala 220:29]
  wire  a_mask_eq_3 = a_mask_nbit & a_mask_bit_1; // @[Misc.scala 219:27]
  wire  a_mask_acc_3 = a_mask_acc | a_mask_size_1 & a_mask_eq_3; // @[Misc.scala 220:29]
  wire  a_mask_eq_4 = a_mask_bit & a_mask_nbit_1; // @[Misc.scala 219:27]
  wire  a_mask_acc_4 = a_mask_acc_1 | a_mask_size_1 & a_mask_eq_4; // @[Misc.scala 220:29]
  wire  a_mask_eq_5 = a_mask_bit & a_mask_bit_1; // @[Misc.scala 219:27]
  wire  a_mask_acc_5 = a_mask_acc_1 | a_mask_size_1 & a_mask_eq_5; // @[Misc.scala 220:29]
  wire  a_mask_size_2 = a_mask_sizeOH[0]; // @[Misc.scala 214:26]
  wire  a_mask_bit_2 = r_addr[0]; // @[Misc.scala 215:26]
  wire  a_mask_nbit_2 = ~a_mask_bit_2; // @[Misc.scala 216:20]
  wire  a_mask_eq_6 = a_mask_eq_2 & a_mask_nbit_2; // @[Misc.scala 219:27]
  wire  a_mask_lo_lo_lo = a_mask_acc_2 | a_mask_size_2 & a_mask_eq_6; // @[Misc.scala 220:29]
  wire  a_mask_eq_7 = a_mask_eq_2 & a_mask_bit_2; // @[Misc.scala 219:27]
  wire  a_mask_lo_lo_hi = a_mask_acc_2 | a_mask_size_2 & a_mask_eq_7; // @[Misc.scala 220:29]
  wire  a_mask_eq_8 = a_mask_eq_3 & a_mask_nbit_2; // @[Misc.scala 219:27]
  wire  a_mask_lo_hi_lo = a_mask_acc_3 | a_mask_size_2 & a_mask_eq_8; // @[Misc.scala 220:29]
  wire  a_mask_eq_9 = a_mask_eq_3 & a_mask_bit_2; // @[Misc.scala 219:27]
  wire  a_mask_lo_hi_hi = a_mask_acc_3 | a_mask_size_2 & a_mask_eq_9; // @[Misc.scala 220:29]
  wire  a_mask_eq_10 = a_mask_eq_4 & a_mask_nbit_2; // @[Misc.scala 219:27]
  wire  a_mask_hi_lo_lo = a_mask_acc_4 | a_mask_size_2 & a_mask_eq_10; // @[Misc.scala 220:29]
  wire  a_mask_eq_11 = a_mask_eq_4 & a_mask_bit_2; // @[Misc.scala 219:27]
  wire  a_mask_hi_lo_hi = a_mask_acc_4 | a_mask_size_2 & a_mask_eq_11; // @[Misc.scala 220:29]
  wire  a_mask_eq_12 = a_mask_eq_5 & a_mask_nbit_2; // @[Misc.scala 219:27]
  wire  a_mask_hi_hi_lo = a_mask_acc_5 | a_mask_size_2 & a_mask_eq_12; // @[Misc.scala 220:29]
  wire  a_mask_eq_13 = a_mask_eq_5 & a_mask_bit_2; // @[Misc.scala 219:27]
  wire  a_mask_hi_hi_hi = a_mask_acc_5 | a_mask_size_2 & a_mask_eq_13; // @[Misc.scala 220:29]
  wire [7:0] a_mask = {a_mask_hi_hi_hi,a_mask_hi_hi_lo,a_mask_hi_lo_hi,a_mask_hi_lo_lo,a_mask_lo_hi_hi,a_mask_lo_hi_lo,
    a_mask_lo_lo_hi,a_mask_lo_lo_lo}; // @[Cat.scala 30:58]
  wire  r_out_bits_user_amba_prot_privileged = auto_in_ar_bits_prot[0]; // @[ToTL.scala 106:45]
  wire  r_out_bits_user_amba_prot_secure = ~auto_in_ar_bits_prot[1]; // @[ToTL.scala 107:29]
  wire  r_out_bits_user_amba_prot_fetch = auto_in_ar_bits_prot[2]; // @[ToTL.scala 108:45]
  wire  r_out_bits_user_amba_prot_bufferable = auto_in_ar_bits_cache[0]; // @[ToTL.scala 109:46]
  wire  r_out_bits_user_amba_prot_modifiable = auto_in_ar_bits_cache[1]; // @[ToTL.scala 110:46]
  wire  r_out_bits_user_amba_prot_readalloc = auto_in_ar_bits_cache[2]; // @[ToTL.scala 111:46]
  wire  r_out_bits_user_amba_prot_writealloc = auto_in_ar_bits_cache[3]; // @[ToTL.scala 112:46]
  wire [3:0] r_sel = 4'h1 << auto_in_ar_bits_id; // @[OneHot.scala 65:12]
  reg [7:0] beatsLeft; // @[Arbiter.scala 87:30]
  wire  idle = beatsLeft == 8'h0; // @[Arbiter.scala 88:28]
  wire  w_out_valid = auto_in_aw_valid & auto_in_w_valid; // @[ToTL.scala 136:34]
  wire [1:0] readys_filter_lo = {w_out_valid,auto_in_ar_valid}; // @[Cat.scala 30:58]
  reg [1:0] readys_mask; // @[Arbiter.scala 23:23]
  wire [1:0] _readys_filter_T = ~readys_mask; // @[Arbiter.scala 24:30]
  wire [1:0] readys_filter_hi = readys_filter_lo & _readys_filter_T; // @[Arbiter.scala 24:28]
  wire [3:0] readys_filter = {readys_filter_hi,w_out_valid,auto_in_ar_valid}; // @[Cat.scala 30:58]
  wire [3:0] _GEN_32 = {{1'd0}, readys_filter[3:1]}; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_1 = readys_filter | _GEN_32; // @[package.scala 253:43]
  wire [3:0] _readys_unready_T_4 = {readys_mask, 2'h0}; // @[Arbiter.scala 25:66]
  wire [3:0] _GEN_33 = {{1'd0}, _readys_unready_T_1[3:1]}; // @[Arbiter.scala 25:58]
  wire [3:0] readys_unready = _GEN_33 | _readys_unready_T_4; // @[Arbiter.scala 25:58]
  wire [1:0] _readys_readys_T_2 = readys_unready[3:2] & readys_unready[1:0]; // @[Arbiter.scala 26:39]
  wire [1:0] readys_readys = ~_readys_readys_T_2; // @[Arbiter.scala 26:18]
  wire  readys_0 = readys_readys[0]; // @[Arbiter.scala 95:86]
  reg  state_0; // @[Arbiter.scala 116:26]
  wire  allowed_0 = idle ? readys_0 : state_0; // @[Arbiter.scala 121:24]
  wire  out_ready = auto_out_a_ready & allowed_0; // @[Arbiter.scala 123:31]
  wire  _T_14 = out_ready & auto_in_ar_valid; // @[Decoupled.scala 40:37]
  wire [2:0] _r_count_0_T_1 = r_count_0 + 3'h1; // @[ToTL.scala 117:43]
  wire [2:0] _r_count_1_T_1 = r_count_1 + 3'h1; // @[ToTL.scala 117:43]
  wire [2:0] _r_count_2_T_1 = r_count_2 + 3'h1; // @[ToTL.scala 117:43]
  wire [2:0] _r_count_3_T_1 = r_count_3 + 3'h1; // @[ToTL.scala 117:43]
  wire [15:0] _w_size1_T = {auto_in_aw_bits_len,8'hff}; // @[Cat.scala 30:58]
  wire [22:0] _GEN_34 = {{7'd0}, _w_size1_T}; // @[Bundles.scala 33:21]
  wire [22:0] _w_size1_T_1 = _GEN_34 << auto_in_aw_bits_size; // @[Bundles.scala 33:21]
  wire [14:0] w_size_lo = _w_size1_T_1[22:8]; // @[Bundles.scala 33:30]
  wire [15:0] _w_size_T = {w_size_lo, 1'h0}; // @[package.scala 232:35]
  wire [15:0] _w_size_T_1 = _w_size_T | 16'h1; // @[package.scala 232:40]
  wire [15:0] _w_size_T_2 = {1'h0,w_size_lo}; // @[Cat.scala 30:58]
  wire [15:0] _w_size_T_3 = ~_w_size_T_2; // @[package.scala 232:53]
  wire [15:0] _w_size_T_4 = _w_size_T_1 & _w_size_T_3; // @[package.scala 232:51]
  wire [7:0] w_size_hi = _w_size_T_4[15:8]; // @[OneHot.scala 30:18]
  wire [7:0] w_size_lo_1 = _w_size_T_4[7:0]; // @[OneHot.scala 31:18]
  wire  w_size_hi_1 = |w_size_hi; // @[OneHot.scala 32:14]
  wire [7:0] _w_size_T_5 = w_size_hi | w_size_lo_1; // @[OneHot.scala 32:28]
  wire [3:0] w_size_hi_2 = _w_size_T_5[7:4]; // @[OneHot.scala 30:18]
  wire [3:0] w_size_lo_2 = _w_size_T_5[3:0]; // @[OneHot.scala 31:18]
  wire  w_size_hi_3 = |w_size_hi_2; // @[OneHot.scala 32:14]
  wire [3:0] _w_size_T_6 = w_size_hi_2 | w_size_lo_2; // @[OneHot.scala 32:28]
  wire [1:0] w_size_hi_4 = _w_size_T_6[3:2]; // @[OneHot.scala 30:18]
  wire [1:0] w_size_lo_3 = _w_size_T_6[1:0]; // @[OneHot.scala 31:18]
  wire  w_size_hi_5 = |w_size_hi_4; // @[OneHot.scala 32:14]
  wire [1:0] _w_size_T_7 = w_size_hi_4 | w_size_lo_3; // @[OneHot.scala 32:28]
  wire  w_size_lo_4 = _w_size_T_7[1]; // @[CircuitMath.scala 30:8]
  wire [3:0] w_size = {w_size_hi_1,w_size_hi_3,w_size_hi_5,w_size_lo_4}; // @[Cat.scala 30:58]
  wire  _w_ok_T_1 = w_size <= 4'h6; // @[Parameters.scala 92:42]
  wire [37:0] _w_ok_T_5 = {1'b0,$signed(auto_in_aw_bits_addr)}; // @[Parameters.scala 137:49]
  wire [37:0] _w_ok_T_7 = $signed(_w_ok_T_5) & -38'sh5000; // @[Parameters.scala 137:52]
  wire  _w_ok_T_8 = $signed(_w_ok_T_7) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _w_ok_T_9 = auto_in_aw_bits_addr ^ 37'h2000000; // @[Parameters.scala 137:31]
  wire [37:0] _w_ok_T_10 = {1'b0,$signed(_w_ok_T_9)}; // @[Parameters.scala 137:49]
  wire [37:0] _w_ok_T_12 = $signed(_w_ok_T_10) & -38'sh10000; // @[Parameters.scala 137:52]
  wire  _w_ok_T_13 = $signed(_w_ok_T_12) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _w_ok_T_14 = auto_in_aw_bits_addr ^ 37'h2010000; // @[Parameters.scala 137:31]
  wire [37:0] _w_ok_T_15 = {1'b0,$signed(_w_ok_T_14)}; // @[Parameters.scala 137:49]
  wire [37:0] _w_ok_T_17 = $signed(_w_ok_T_15) & -38'sh4000; // @[Parameters.scala 137:52]
  wire  _w_ok_T_18 = $signed(_w_ok_T_17) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _w_ok_T_19 = auto_in_aw_bits_addr ^ 37'h8000000; // @[Parameters.scala 137:31]
  wire [37:0] _w_ok_T_20 = {1'b0,$signed(_w_ok_T_19)}; // @[Parameters.scala 137:49]
  wire [37:0] _w_ok_T_22 = $signed(_w_ok_T_20) & -38'sh2200000; // @[Parameters.scala 137:52]
  wire  _w_ok_T_23 = $signed(_w_ok_T_22) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _w_ok_T_24 = auto_in_aw_bits_addr ^ 37'hc000000; // @[Parameters.scala 137:31]
  wire [37:0] _w_ok_T_25 = {1'b0,$signed(_w_ok_T_24)}; // @[Parameters.scala 137:49]
  wire [37:0] _w_ok_T_27 = $signed(_w_ok_T_25) & -38'sh4000000; // @[Parameters.scala 137:52]
  wire  _w_ok_T_28 = $signed(_w_ok_T_27) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _w_ok_T_29 = auto_in_aw_bits_addr ^ 37'h10000000; // @[Parameters.scala 137:31]
  wire [37:0] _w_ok_T_30 = {1'b0,$signed(_w_ok_T_29)}; // @[Parameters.scala 137:49]
  wire [37:0] _w_ok_T_32 = $signed(_w_ok_T_30) & -38'sh2000; // @[Parameters.scala 137:52]
  wire  _w_ok_T_33 = $signed(_w_ok_T_32) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _w_ok_T_34 = auto_in_aw_bits_addr ^ 37'h10003000; // @[Parameters.scala 137:31]
  wire [37:0] _w_ok_T_35 = {1'b0,$signed(_w_ok_T_34)}; // @[Parameters.scala 137:49]
  wire [37:0] _w_ok_T_37 = $signed(_w_ok_T_35) & -38'sh1000; // @[Parameters.scala 137:52]
  wire  _w_ok_T_38 = $signed(_w_ok_T_37) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _w_ok_T_39 = auto_in_aw_bits_addr ^ 37'h80000000; // @[Parameters.scala 137:31]
  wire [37:0] _w_ok_T_40 = {1'b0,$signed(_w_ok_T_39)}; // @[Parameters.scala 137:49]
  wire [37:0] _w_ok_T_42 = $signed(_w_ok_T_40) & -38'sh80000000; // @[Parameters.scala 137:52]
  wire  _w_ok_T_43 = $signed(_w_ok_T_42) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _w_ok_T_44 = auto_in_aw_bits_addr ^ 37'h800000000; // @[Parameters.scala 137:31]
  wire [37:0] _w_ok_T_45 = {1'b0,$signed(_w_ok_T_44)}; // @[Parameters.scala 137:49]
  wire [37:0] _w_ok_T_47 = $signed(_w_ok_T_45) & -38'sh800000000; // @[Parameters.scala 137:52]
  wire  _w_ok_T_48 = $signed(_w_ok_T_47) == 38'sh0; // @[Parameters.scala 137:67]
  wire  _w_ok_T_56 = _w_ok_T_8 | _w_ok_T_13 | _w_ok_T_18 | _w_ok_T_23 | _w_ok_T_28 | _w_ok_T_33 | _w_ok_T_38 |
    _w_ok_T_43 | _w_ok_T_48; // @[Parameters.scala 686:42]
  wire  _w_ok_T_57 = _w_ok_T_1 & _w_ok_T_56; // @[Parameters.scala 685:56]
  wire  _w_ok_T_59 = w_size <= 4'h8; // @[Parameters.scala 92:42]
  wire [36:0] _w_ok_T_62 = auto_in_aw_bits_addr ^ 37'h3000; // @[Parameters.scala 137:31]
  wire [37:0] _w_ok_T_63 = {1'b0,$signed(_w_ok_T_62)}; // @[Parameters.scala 137:49]
  wire [37:0] _w_ok_T_65 = $signed(_w_ok_T_63) & -38'sh1000; // @[Parameters.scala 137:52]
  wire  _w_ok_T_66 = $signed(_w_ok_T_65) == 38'sh0; // @[Parameters.scala 137:67]
  wire  _w_ok_T_67 = _w_ok_T_59 & _w_ok_T_66; // @[Parameters.scala 685:56]
  wire  _w_ok_T_69 = w_size <= 4'h7; // @[Parameters.scala 92:42]
  wire [36:0] _w_ok_T_72 = auto_in_aw_bits_addr ^ 37'h1000000000; // @[Parameters.scala 137:31]
  wire [37:0] _w_ok_T_73 = {1'b0,$signed(_w_ok_T_72)}; // @[Parameters.scala 137:49]
  wire [37:0] _w_ok_T_75 = $signed(_w_ok_T_73) & -38'sh1000000000; // @[Parameters.scala 137:52]
  wire  _w_ok_T_76 = $signed(_w_ok_T_75) == 38'sh0; // @[Parameters.scala 137:67]
  wire  _w_ok_T_77 = _w_ok_T_69 & _w_ok_T_76; // @[Parameters.scala 685:56]
  wire  w_ok = _w_ok_T_57 | _w_ok_T_67 | _w_ok_T_77; // @[Parameters.scala 687:30]
  wire [13:0] _GEN_35 = {{11'd0}, auto_in_aw_bits_addr[2:0]}; // @[ToTL.scala 124:59]
  wire [13:0] _w_addr_T_1 = 14'h3000 | _GEN_35; // @[ToTL.scala 124:59]
  wire [36:0] w_addr = w_ok ? auto_in_aw_bits_addr : {{23'd0}, _w_addr_T_1}; // @[ToTL.scala 124:23]
  reg [2:0] w_count_0; // @[ToTL.scala 125:28]
  reg [2:0] w_count_1; // @[ToTL.scala 125:28]
  reg [2:0] w_count_2; // @[ToTL.scala 125:28]
  reg [2:0] w_count_3; // @[ToTL.scala 125:28]
  wire [2:0] _GEN_9 = 2'h1 == auto_in_aw_bits_id ? w_count_1 : w_count_0; // @[ToTL.scala 129:50 ToTL.scala 129:50]
  wire [2:0] _GEN_10 = 2'h2 == auto_in_aw_bits_id ? w_count_2 : _GEN_9; // @[ToTL.scala 129:50 ToTL.scala 129:50]
  wire [2:0] _GEN_11 = 2'h3 == auto_in_aw_bits_id ? w_count_3 : _GEN_10; // @[ToTL.scala 129:50 ToTL.scala 129:50]
  wire [1:0] w_id_hi_lo = _GEN_11[1:0]; // @[ToTL.scala 129:50]
  wire [4:0] w_id = {auto_in_aw_bits_id,w_id_hi_lo,1'h1}; // @[Cat.scala 30:58]
  wire  readys_1 = readys_readys[1]; // @[Arbiter.scala 95:86]
  reg  state_1; // @[Arbiter.scala 116:26]
  wire  allowed_1 = idle ? readys_1 : state_1; // @[Arbiter.scala 121:24]
  wire  out_1_ready = auto_out_a_ready & allowed_1; // @[Arbiter.scala 123:31]
  wire  bundleIn_0_aw_ready = out_1_ready & auto_in_w_valid & auto_in_w_bits_last; // @[ToTL.scala 134:48]
  wire  w_out_bits_user_amba_prot_privileged = auto_in_aw_bits_prot[0]; // @[ToTL.scala 142:45]
  wire  w_out_bits_user_amba_prot_secure = ~auto_in_aw_bits_prot[1]; // @[ToTL.scala 143:29]
  wire  w_out_bits_user_amba_prot_fetch = auto_in_aw_bits_prot[2]; // @[ToTL.scala 144:45]
  wire  w_out_bits_user_amba_prot_bufferable = auto_in_aw_bits_cache[0]; // @[ToTL.scala 145:46]
  wire  w_out_bits_user_amba_prot_modifiable = auto_in_aw_bits_cache[1]; // @[ToTL.scala 146:46]
  wire  w_out_bits_user_amba_prot_readalloc = auto_in_aw_bits_cache[2]; // @[ToTL.scala 147:46]
  wire  w_out_bits_user_amba_prot_writealloc = auto_in_aw_bits_cache[3]; // @[ToTL.scala 148:46]
  wire [3:0] w_sel = 4'h1 << auto_in_aw_bits_id; // @[OneHot.scala 65:12]
  wire  _T_44 = bundleIn_0_aw_ready & auto_in_aw_valid; // @[Decoupled.scala 40:37]
  wire [2:0] _w_count_0_T_1 = w_count_0 + 3'h1; // @[ToTL.scala 153:43]
  wire [2:0] _w_count_1_T_1 = w_count_1 + 3'h1; // @[ToTL.scala 153:43]
  wire [2:0] _w_count_2_T_1 = w_count_2 + 3'h1; // @[ToTL.scala 153:43]
  wire [2:0] _w_count_3_T_1 = w_count_3 + 3'h1; // @[ToTL.scala 153:43]
  wire  latch = idle & auto_out_a_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _readys_mask_T = readys_readys & readys_filter_lo; // @[Arbiter.scala 28:29]
  wire [2:0] _readys_mask_T_1 = {_readys_mask_T, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_mask_T_3 = _readys_mask_T | _readys_mask_T_1[1:0]; // @[package.scala 244:43]
  wire  earlyWinner_0 = readys_0 & auto_in_ar_valid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_1 = readys_1 & w_out_valid; // @[Arbiter.scala 97:79]
  wire  _T_62 = auto_in_ar_valid | w_out_valid; // @[Arbiter.scala 107:36]
  wire  muxStateEarly_0 = idle ? earlyWinner_0 : state_0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_1 = idle ? earlyWinner_1 : state_1; // @[Arbiter.scala 117:30]
  wire  _sink_ACancel_earlyValid_T_3 = state_0 & auto_in_ar_valid | state_1 & w_out_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_earlyValid = idle ? _T_62 : _sink_ACancel_earlyValid_T_3; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_2 = auto_out_a_ready & sink_ACancel_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [7:0] _GEN_36 = {{7'd0}, _beatsLeft_T_2}; // @[Arbiter.scala 113:52]
  wire [7:0] _beatsLeft_T_4 = beatsLeft - _GEN_36; // @[Arbiter.scala 113:52]
  wire [7:0] _T_82 = muxStateEarly_0 ? a_mask : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _T_83 = muxStateEarly_1 ? auto_in_w_bits_strb : 8'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_106 = muxStateEarly_0 ? r_addr : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_107 = muxStateEarly_1 ? w_addr : 37'h0; // @[Mux.scala 27:72]
  wire [4:0] _T_109 = muxStateEarly_0 ? r_id : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _T_110 = muxStateEarly_1 ? w_id : 5'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_112 = muxStateEarly_0 ? r_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_113 = muxStateEarly_1 ? w_size : 4'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_118 = muxStateEarly_0 ? 3'h4 : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_119 = muxStateEarly_1 ? 3'h1 : 3'h0; // @[Mux.scala 27:72]
  wire  d_hasData = auto_out_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire  ok_r_ready = deq_io_enq_ready; // @[ToTL.scala 159:23 Decoupled.scala 299:17]
  wire  ok_b_ready = q_b_deq_io_enq_ready; // @[ToTL.scala 158:23 Decoupled.scala 299:17]
  wire  bundleOut_0_d_ready = d_hasData ? ok_r_ready : ok_b_ready; // @[ToTL.scala 165:25]
  wire  _d_last_T = bundleOut_0_d_ready & auto_out_d_valid; // @[Decoupled.scala 40:37]
  wire [22:0] _d_last_beats1_decode_T_1 = 23'hff << auto_out_d_bits_size; // @[package.scala 234:77]
  wire [7:0] _d_last_beats1_decode_T_3 = ~_d_last_beats1_decode_T_1[7:0]; // @[package.scala 234:46]
  wire [4:0] d_last_beats1_decode = _d_last_beats1_decode_T_3[7:3]; // @[Edges.scala 219:59]
  wire [4:0] d_last_beats1 = d_hasData ? d_last_beats1_decode : 5'h0; // @[Edges.scala 220:14]
  reg [4:0] d_last_counter; // @[Edges.scala 228:27]
  wire [4:0] d_last_counter1 = d_last_counter - 5'h1; // @[Edges.scala 229:28]
  wire  d_last_first = d_last_counter == 5'h0; // @[Edges.scala 230:25]
  reg [2:0] b_count_0; // @[ToTL.scala 187:28]
  reg [2:0] b_count_1; // @[ToTL.scala 187:28]
  reg [2:0] b_count_2; // @[ToTL.scala 187:28]
  reg [2:0] b_count_3; // @[ToTL.scala 187:28]
  wire [1:0] q_b_bits_id = q_b_deq_io_deq_bits_id; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  wire [2:0] _GEN_19 = 2'h1 == q_b_bits_id ? b_count_1 : b_count_0; // @[ToTL.scala 188:43 ToTL.scala 188:43]
  wire [2:0] _GEN_20 = 2'h2 == q_b_bits_id ? b_count_2 : _GEN_19; // @[ToTL.scala 188:43 ToTL.scala 188:43]
  wire [2:0] _GEN_21 = 2'h3 == q_b_bits_id ? b_count_3 : _GEN_20; // @[ToTL.scala 188:43 ToTL.scala 188:43]
  wire [2:0] _GEN_23 = 2'h1 == q_b_bits_id ? w_count_1 : w_count_0; // @[ToTL.scala 188:43 ToTL.scala 188:43]
  wire [2:0] _GEN_24 = 2'h2 == q_b_bits_id ? w_count_2 : _GEN_23; // @[ToTL.scala 188:43 ToTL.scala 188:43]
  wire [2:0] _GEN_25 = 2'h3 == q_b_bits_id ? w_count_3 : _GEN_24; // @[ToTL.scala 188:43 ToTL.scala 188:43]
  wire  b_allow = _GEN_21 != _GEN_25; // @[ToTL.scala 188:43]
  wire [3:0] b_sel = 4'h1 << q_b_bits_id; // @[OneHot.scala 65:12]
  wire  q_b_valid = q_b_deq_io_deq_valid; // @[Decoupled.scala 317:19 Decoupled.scala 319:15]
  wire  bundleIn_0_b_valid = q_b_valid & b_allow; // @[ToTL.scala 196:31]
  wire  _T_125 = auto_in_b_ready & bundleIn_0_b_valid; // @[Decoupled.scala 40:37]
  wire [2:0] _b_count_0_T_1 = b_count_0 + 3'h1; // @[ToTL.scala 192:42]
  wire [2:0] _b_count_1_T_1 = b_count_1 + 3'h1; // @[ToTL.scala 192:42]
  wire [2:0] _b_count_2_T_1 = b_count_2 + 3'h1; // @[ToTL.scala 192:42]
  wire [2:0] _b_count_3_T_1 = b_count_3 + 3'h1; // @[ToTL.scala 192:42]
  RHEA__Queue_26 deq ( // @[Decoupled.scala 296:21]
    .rf_reset(deq_rf_reset),
    .clock(deq_clock),
    .reset(deq_reset),
    .io_enq_ready(deq_io_enq_ready),
    .io_enq_valid(deq_io_enq_valid),
    .io_enq_bits_id(deq_io_enq_bits_id),
    .io_enq_bits_data(deq_io_enq_bits_data),
    .io_enq_bits_resp(deq_io_enq_bits_resp),
    .io_enq_bits_last(deq_io_enq_bits_last),
    .io_deq_ready(deq_io_deq_ready),
    .io_deq_valid(deq_io_deq_valid),
    .io_deq_bits_id(deq_io_deq_bits_id),
    .io_deq_bits_data(deq_io_deq_bits_data),
    .io_deq_bits_resp(deq_io_deq_bits_resp),
    .io_deq_bits_last(deq_io_deq_bits_last)
  );
  RHEA__Queue_27 q_b_deq ( // @[Decoupled.scala 296:21]
    .rf_reset(q_b_deq_rf_reset),
    .clock(q_b_deq_clock),
    .reset(q_b_deq_reset),
    .io_enq_ready(q_b_deq_io_enq_ready),
    .io_enq_valid(q_b_deq_io_enq_valid),
    .io_enq_bits_id(q_b_deq_io_enq_bits_id),
    .io_enq_bits_resp(q_b_deq_io_enq_bits_resp),
    .io_deq_ready(q_b_deq_io_deq_ready),
    .io_deq_valid(q_b_deq_io_deq_valid),
    .io_deq_bits_id(q_b_deq_io_deq_bits_id),
    .io_deq_bits_resp(q_b_deq_io_deq_bits_resp)
  );
  assign deq_rf_reset = rf_reset;
  assign q_b_deq_rf_reset = rf_reset;
  assign auto_in_aw_ready = out_1_ready & auto_in_w_valid & auto_in_w_bits_last; // @[ToTL.scala 134:48]
  assign auto_in_w_ready = out_1_ready & auto_in_aw_valid; // @[ToTL.scala 135:34]
  assign auto_in_b_valid = q_b_valid & b_allow; // @[ToTL.scala 196:31]
  assign auto_in_b_bits_id = q_b_deq_io_deq_bits_id; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_in_b_bits_resp = q_b_deq_io_deq_bits_resp; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_in_ar_ready = auto_out_a_ready & allowed_0; // @[Arbiter.scala 123:31]
  assign auto_in_r_valid = deq_io_deq_valid; // @[Decoupled.scala 317:19 Decoupled.scala 319:15]
  assign auto_in_r_bits_id = deq_io_deq_bits_id; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_in_r_bits_data = deq_io_deq_bits_data; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_in_r_bits_resp = deq_io_deq_bits_resp; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_in_r_bits_last = deq_io_deq_bits_last; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_a_valid = idle ? _T_62 : _sink_ACancel_earlyValid_T_3; // @[Arbiter.scala 125:29]
  assign auto_out_a_bits_opcode = _T_118 | _T_119; // @[Mux.scala 27:72]
  assign auto_out_a_bits_size = _T_112 | _T_113; // @[Mux.scala 27:72]
  assign auto_out_a_bits_source = _T_109 | _T_110; // @[Mux.scala 27:72]
  assign auto_out_a_bits_address = _T_106 | _T_107; // @[Mux.scala 27:72]
  assign auto_out_a_bits_user_amba_prot_bufferable = muxStateEarly_0 & r_out_bits_user_amba_prot_bufferable |
    muxStateEarly_1 & w_out_bits_user_amba_prot_bufferable; // @[Mux.scala 27:72]
  assign auto_out_a_bits_user_amba_prot_modifiable = muxStateEarly_0 & r_out_bits_user_amba_prot_modifiable |
    muxStateEarly_1 & w_out_bits_user_amba_prot_modifiable; // @[Mux.scala 27:72]
  assign auto_out_a_bits_user_amba_prot_readalloc = muxStateEarly_0 & r_out_bits_user_amba_prot_readalloc |
    muxStateEarly_1 & w_out_bits_user_amba_prot_readalloc; // @[Mux.scala 27:72]
  assign auto_out_a_bits_user_amba_prot_writealloc = muxStateEarly_0 & r_out_bits_user_amba_prot_writealloc |
    muxStateEarly_1 & w_out_bits_user_amba_prot_writealloc; // @[Mux.scala 27:72]
  assign auto_out_a_bits_user_amba_prot_privileged = muxStateEarly_0 & r_out_bits_user_amba_prot_privileged |
    muxStateEarly_1 & w_out_bits_user_amba_prot_privileged; // @[Mux.scala 27:72]
  assign auto_out_a_bits_user_amba_prot_secure = muxStateEarly_0 & r_out_bits_user_amba_prot_secure | muxStateEarly_1 &
    w_out_bits_user_amba_prot_secure; // @[Mux.scala 27:72]
  assign auto_out_a_bits_user_amba_prot_fetch = muxStateEarly_0 & r_out_bits_user_amba_prot_fetch | muxStateEarly_1 &
    w_out_bits_user_amba_prot_fetch; // @[Mux.scala 27:72]
  assign auto_out_a_bits_mask = _T_82 | _T_83; // @[Mux.scala 27:72]
  assign auto_out_a_bits_data = muxStateEarly_1 ? auto_in_w_bits_data : 64'h0; // @[Mux.scala 27:72]
  assign auto_out_d_ready = d_hasData ? ok_r_ready : ok_b_ready; // @[ToTL.scala 165:25]
  assign deq_clock = clock;
  assign deq_reset = reset;
  assign deq_io_enq_valid = auto_out_d_valid & d_hasData; // @[ToTL.scala 166:33]
  assign deq_io_enq_bits_id = auto_out_d_bits_source[4:3]; // @[ToTL.scala 169:43]
  assign deq_io_enq_bits_data = auto_out_d_bits_data; // @[ToTL.scala 22:79 LazyModule.scala 390:12]
  assign deq_io_enq_bits_resp = auto_out_d_bits_denied | auto_out_d_bits_corrupt ? 2'h2 : 2'h0; // @[ToTL.scala 161:23]
  assign deq_io_enq_bits_last = d_last_counter == 5'h1 | d_last_beats1 == 5'h0; // @[Edges.scala 231:37]
  assign deq_io_deq_ready = auto_in_r_ready; // @[ToTL.scala 22:79 LazyModule.scala 388:16]
  assign q_b_deq_clock = clock;
  assign q_b_deq_reset = reset;
  assign q_b_deq_io_enq_valid = auto_out_d_valid & ~d_hasData; // @[ToTL.scala 167:33]
  assign q_b_deq_io_enq_bits_id = auto_out_d_bits_source[4:3]; // @[ToTL.scala 178:43]
  assign q_b_deq_io_enq_bits_resp = auto_out_d_bits_denied | auto_out_d_bits_corrupt ? 2'h2 : 2'h0; // @[ToTL.scala 161:23]
  assign q_b_deq_io_deq_ready = auto_in_b_ready & b_allow; // @[ToTL.scala 197:31]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r_count_0 <= 3'h0;
    end else if (_T_14 & r_sel[0]) begin
      r_count_0 <= _r_count_0_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r_count_1 <= 3'h0;
    end else if (_T_14 & r_sel[1]) begin
      r_count_1 <= _r_count_1_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r_count_2 <= 3'h0;
    end else if (_T_14 & r_sel[2]) begin
      r_count_2 <= _r_count_2_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r_count_3 <= 3'h0;
    end else if (_T_14 & r_sel[3]) begin
      r_count_3 <= _r_count_3_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft <= 8'h0;
    end else if (latch) begin
      if (earlyWinner_1) begin
        beatsLeft <= auto_in_aw_bits_len;
      end else begin
        beatsLeft <= 8'h0;
      end
    end else begin
      beatsLeft <= _beatsLeft_T_4;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask <= 2'h3;
    end else if (latch & |readys_filter_lo) begin
      readys_mask <= _readys_mask_T_3;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_0 <= 1'h0;
    end else if (idle) begin
      state_0 <= earlyWinner_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      w_count_0 <= 3'h0;
    end else if (_T_44 & w_sel[0]) begin
      w_count_0 <= _w_count_0_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      w_count_1 <= 3'h0;
    end else if (_T_44 & w_sel[1]) begin
      w_count_1 <= _w_count_1_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      w_count_2 <= 3'h0;
    end else if (_T_44 & w_sel[2]) begin
      w_count_2 <= _w_count_2_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      w_count_3 <= 3'h0;
    end else if (_T_44 & w_sel[3]) begin
      w_count_3 <= _w_count_3_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_1 <= 1'h0;
    end else if (idle) begin
      state_1 <= earlyWinner_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      d_last_counter <= 5'h0;
    end else if (_d_last_T) begin
      if (d_last_first) begin
        if (d_hasData) begin
          d_last_counter <= d_last_beats1_decode;
        end else begin
          d_last_counter <= 5'h0;
        end
      end else begin
        d_last_counter <= d_last_counter1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      b_count_0 <= 3'h0;
    end else if (_T_125 & b_sel[0]) begin
      b_count_0 <= _b_count_0_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      b_count_1 <= 3'h0;
    end else if (_T_125 & b_sel[1]) begin
      b_count_1 <= _b_count_1_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      b_count_2 <= 3'h0;
    end else if (_T_125 & b_sel[2]) begin
      b_count_2 <= _b_count_2_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      b_count_3 <= 3'h0;
    end else if (_T_125 & b_sel[3]) begin
      b_count_3 <= _b_count_3_T_1;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  r_count_0 = _RAND_0[2:0];
  _RAND_1 = {1{`RANDOM}};
  r_count_1 = _RAND_1[2:0];
  _RAND_2 = {1{`RANDOM}};
  r_count_2 = _RAND_2[2:0];
  _RAND_3 = {1{`RANDOM}};
  r_count_3 = _RAND_3[2:0];
  _RAND_4 = {1{`RANDOM}};
  beatsLeft = _RAND_4[7:0];
  _RAND_5 = {1{`RANDOM}};
  readys_mask = _RAND_5[1:0];
  _RAND_6 = {1{`RANDOM}};
  state_0 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  w_count_0 = _RAND_7[2:0];
  _RAND_8 = {1{`RANDOM}};
  w_count_1 = _RAND_8[2:0];
  _RAND_9 = {1{`RANDOM}};
  w_count_2 = _RAND_9[2:0];
  _RAND_10 = {1{`RANDOM}};
  w_count_3 = _RAND_10[2:0];
  _RAND_11 = {1{`RANDOM}};
  state_1 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  d_last_counter = _RAND_12[4:0];
  _RAND_13 = {1{`RANDOM}};
  b_count_0 = _RAND_13[2:0];
  _RAND_14 = {1{`RANDOM}};
  b_count_1 = _RAND_14[2:0];
  _RAND_15 = {1{`RANDOM}};
  b_count_2 = _RAND_15[2:0];
  _RAND_16 = {1{`RANDOM}};
  b_count_3 = _RAND_16[2:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    r_count_0 = 3'h0;
  end
  if (reset) begin
    r_count_1 = 3'h0;
  end
  if (reset) begin
    r_count_2 = 3'h0;
  end
  if (reset) begin
    r_count_3 = 3'h0;
  end
  if (reset) begin
    beatsLeft = 8'h0;
  end
  if (reset) begin
    readys_mask = 2'h3;
  end
  if (reset) begin
    state_0 = 1'h0;
  end
  if (reset) begin
    w_count_0 = 3'h0;
  end
  if (reset) begin
    w_count_1 = 3'h0;
  end
  if (reset) begin
    w_count_2 = 3'h0;
  end
  if (reset) begin
    w_count_3 = 3'h0;
  end
  if (reset) begin
    state_1 = 1'h0;
  end
  if (reset) begin
    d_last_counter = 5'h0;
  end
  if (reset) begin
    b_count_0 = 3'h0;
  end
  if (reset) begin
    b_count_1 = 3'h0;
  end
  if (reset) begin
    b_count_2 = 3'h0;
  end
  if (reset) begin
    b_count_3 = 3'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
