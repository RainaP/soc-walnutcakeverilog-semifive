//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_19_assert(
  input         clock,
  input         reset,
  input         io_in_a_ready,
  input         io_in_a_valid,
  input  [2:0]  io_in_a_bits_opcode,
  input  [3:0]  io_in_a_bits_size,
  input  [35:0] io_in_a_bits_address,
  input         io_in_d_ready,
  input         io_in_d_valid,
  input  [2:0]  io_in_d_bits_opcode,
  input  [1:0]  io_in_d_bits_param,
  input  [3:0]  io_in_d_bits_size,
  input  [3:0]  io_in_d_bits_sink,
  input         io_in_d_bits_denied,
  input         io_in_d_bits_corrupt
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire [22:0] _T_7; // @[package.scala 189:77]
  wire [7:0] _T_9; // @[package.scala 189:46]
  wire [35:0] _GEN_18; // @[Edges.scala 22:16]
  wire [35:0] _T_10; // @[Edges.scala 22:16]
  wire  _T_11; // @[Edges.scala 22:24]
  wire [36:0] _T_17; // @[Parameters.scala 137:49]
  wire  _T_25; // @[Monitor.scala 79:25]
  wire [35:0] _T_27; // @[Parameters.scala 137:31]
  wire [36:0] _T_28; // @[Parameters.scala 137:49]
  wire [36:0] _T_30; // @[Parameters.scala 137:52]
  wire  _T_31; // @[Parameters.scala 137:67]
  wire [35:0] _T_32; // @[Parameters.scala 137:31]
  wire [36:0] _T_33; // @[Parameters.scala 137:49]
  wire [35:0] _T_37; // @[Parameters.scala 137:31]
  wire [36:0] _T_38; // @[Parameters.scala 137:49]
  wire [36:0] _T_40; // @[Parameters.scala 137:52]
  wire  _T_41; // @[Parameters.scala 137:67]
  wire [35:0] _T_42; // @[Parameters.scala 137:31]
  wire [36:0] _T_43; // @[Parameters.scala 137:49]
  wire [36:0] _T_45; // @[Parameters.scala 137:52]
  wire  _T_46; // @[Parameters.scala 137:67]
  wire [35:0] _T_47; // @[Parameters.scala 137:31]
  wire [36:0] _T_48; // @[Parameters.scala 137:49]
  wire [36:0] _T_50; // @[Parameters.scala 137:52]
  wire  _T_51; // @[Parameters.scala 137:67]
  wire [36:0] _T_55; // @[Parameters.scala 137:52]
  wire  _T_56; // @[Parameters.scala 137:67]
  wire [35:0] _T_57; // @[Parameters.scala 137:31]
  wire [36:0] _T_58; // @[Parameters.scala 137:49]
  wire [36:0] _T_60; // @[Parameters.scala 137:52]
  wire  _T_61; // @[Parameters.scala 137:67]
  wire [35:0] _T_62; // @[Parameters.scala 137:31]
  wire [36:0] _T_63; // @[Parameters.scala 137:49]
  wire [36:0] _T_65; // @[Parameters.scala 137:52]
  wire  _T_66; // @[Parameters.scala 137:67]
  wire [35:0] _T_67; // @[Parameters.scala 137:31]
  wire [36:0] _T_68; // @[Parameters.scala 137:49]
  wire [36:0] _T_70; // @[Parameters.scala 137:52]
  wire  _T_71; // @[Parameters.scala 137:67]
  wire [35:0] _T_72; // @[Parameters.scala 137:31]
  wire [36:0] _T_73; // @[Parameters.scala 137:49]
  wire [36:0] _T_75; // @[Parameters.scala 137:52]
  wire  _T_76; // @[Parameters.scala 137:67]
  wire  _T_87; // @[Parameters.scala 92:48]
  wire [35:0] _T_89; // @[Parameters.scala 137:31]
  wire [36:0] _T_90; // @[Parameters.scala 137:49]
  wire [36:0] _T_92; // @[Parameters.scala 137:52]
  wire  _T_93; // @[Parameters.scala 137:67]
  wire [35:0] _T_94; // @[Parameters.scala 137:31]
  wire [36:0] _T_95; // @[Parameters.scala 137:49]
  wire [36:0] _T_97; // @[Parameters.scala 137:52]
  wire  _T_98; // @[Parameters.scala 137:67]
  wire  _T_99; // @[Parameters.scala 552:42]
  wire  _T_100; // @[Parameters.scala 551:56]
  wire  _T_104; // @[Monitor.scala 44:11]
  wire  _T_105; // @[Monitor.scala 44:11]
  wire  _T_108; // @[Monitor.scala 44:11]
  wire  _T_117; // @[Monitor.scala 44:11]
  wire  _T_118; // @[Monitor.scala 44:11]
  wire  _T_132; // @[Monitor.scala 90:25]
  wire  _T_243; // @[Monitor.scala 102:25]
  wire  _T_245; // @[Parameters.scala 93:42]
  wire [36:0] _T_256; // @[Parameters.scala 137:52]
  wire  _T_257; // @[Parameters.scala 137:67]
  wire [36:0] _T_286; // @[Parameters.scala 137:52]
  wire  _T_287; // @[Parameters.scala 137:67]
  wire  _T_298; // @[Parameters.scala 552:42]
  wire  _T_299; // @[Parameters.scala 552:42]
  wire  _T_300; // @[Parameters.scala 552:42]
  wire  _T_301; // @[Parameters.scala 552:42]
  wire  _T_302; // @[Parameters.scala 552:42]
  wire  _T_303; // @[Parameters.scala 552:42]
  wire  _T_304; // @[Parameters.scala 552:42]
  wire  _T_305; // @[Parameters.scala 552:42]
  wire  _T_306; // @[Parameters.scala 552:42]
  wire  _T_307; // @[Parameters.scala 551:56]
  wire  _T_309; // @[Parameters.scala 93:42]
  wire [36:0] _T_315; // @[Parameters.scala 137:52]
  wire  _T_316; // @[Parameters.scala 137:67]
  wire  _T_317; // @[Parameters.scala 551:56]
  wire  _T_319; // @[Parameters.scala 553:30]
  wire  _T_321; // @[Monitor.scala 44:11]
  wire  _T_322; // @[Monitor.scala 44:11]
  wire  _T_341; // @[Monitor.scala 111:25]
  wire  _T_343; // @[Parameters.scala 93:42]
  wire  _T_351; // @[Parameters.scala 551:56]
  wire  _T_401; // @[Parameters.scala 552:42]
  wire  _T_402; // @[Parameters.scala 552:42]
  wire  _T_403; // @[Parameters.scala 552:42]
  wire  _T_404; // @[Parameters.scala 552:42]
  wire  _T_405; // @[Parameters.scala 552:42]
  wire  _T_406; // @[Parameters.scala 552:42]
  wire  _T_407; // @[Parameters.scala 552:42]
  wire  _T_408; // @[Parameters.scala 552:42]
  wire  _T_409; // @[Parameters.scala 551:56]
  wire  _T_421; // @[Parameters.scala 553:30]
  wire  _T_422; // @[Parameters.scala 553:30]
  wire  _T_424; // @[Monitor.scala 44:11]
  wire  _T_425; // @[Monitor.scala 44:11]
  wire  _T_440; // @[Monitor.scala 119:25]
  wire  _T_541; // @[Monitor.scala 127:25]
  wire  _T_550; // @[Parameters.scala 93:42]
  wire  _T_604; // @[Parameters.scala 552:42]
  wire  _T_605; // @[Parameters.scala 552:42]
  wire  _T_606; // @[Parameters.scala 552:42]
  wire  _T_607; // @[Parameters.scala 552:42]
  wire  _T_608; // @[Parameters.scala 552:42]
  wire  _T_609; // @[Parameters.scala 552:42]
  wire  _T_610; // @[Parameters.scala 552:42]
  wire  _T_611; // @[Parameters.scala 552:42]
  wire  _T_612; // @[Parameters.scala 551:56]
  wire  _T_616; // @[Monitor.scala 44:11]
  wire  _T_617; // @[Monitor.scala 44:11]
  wire  _T_632; // @[Monitor.scala 135:25]
  wire  _T_723; // @[Monitor.scala 143:25]
  wire  _T_794; // @[Parameters.scala 551:56]
  wire  _T_807; // @[Parameters.scala 553:30]
  wire  _T_809; // @[Monitor.scala 44:11]
  wire  _T_810; // @[Monitor.scala 44:11]
  wire  _T_829; // @[Bundles.scala 44:24]
  wire  _T_831; // @[Monitor.scala 51:11]
  wire  _T_832; // @[Monitor.scala 51:11]
  wire  _T_836; // @[Monitor.scala 307:25]
  wire  _T_844; // @[Monitor.scala 310:28]
  wire  _T_846; // @[Monitor.scala 51:11]
  wire  _T_847; // @[Monitor.scala 51:11]
  wire  _T_848; // @[Monitor.scala 311:15]
  wire  _T_850; // @[Monitor.scala 51:11]
  wire  _T_851; // @[Monitor.scala 51:11]
  wire  _T_852; // @[Monitor.scala 312:15]
  wire  _T_854; // @[Monitor.scala 51:11]
  wire  _T_855; // @[Monitor.scala 51:11]
  wire  _T_856; // @[Monitor.scala 315:25]
  wire  _T_867; // @[Bundles.scala 104:26]
  wire  _T_869; // @[Monitor.scala 51:11]
  wire  _T_870; // @[Monitor.scala 51:11]
  wire  _T_871; // @[Monitor.scala 320:28]
  wire  _T_873; // @[Monitor.scala 51:11]
  wire  _T_874; // @[Monitor.scala 51:11]
  wire  _T_884; // @[Monitor.scala 325:25]
  wire  _T_904; // @[Monitor.scala 331:30]
  wire  _T_906; // @[Monitor.scala 51:11]
  wire  _T_907; // @[Monitor.scala 51:11]
  wire  _T_913; // @[Monitor.scala 335:25]
  wire  _T_930; // @[Monitor.scala 343:25]
  wire  _T_948; // @[Monitor.scala 351:25]
  wire  _T_980; // @[Decoupled.scala 40:37]
  wire  _T_987; // @[Edges.scala 93:28]
  reg [7:0] _T_989; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire [7:0] _T_991; // @[Edges.scala 231:28]
  wire  _T_992; // @[Edges.scala 232:25]
  reg [2:0] _T_1000; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [3:0] _T_1002; // @[Monitor.scala 383:22]
  reg [31:0] _RAND_2;
  reg [35:0] _T_1004; // @[Monitor.scala 385:22]
  reg [63:0] _RAND_3;
  wire  _T_1005; // @[Monitor.scala 386:22]
  wire  _T_1006; // @[Monitor.scala 386:19]
  wire  _T_1007; // @[Monitor.scala 387:32]
  wire  _T_1009; // @[Monitor.scala 44:11]
  wire  _T_1010; // @[Monitor.scala 44:11]
  wire  _T_1015; // @[Monitor.scala 389:32]
  wire  _T_1017; // @[Monitor.scala 44:11]
  wire  _T_1018; // @[Monitor.scala 44:11]
  wire  _T_1023; // @[Monitor.scala 391:32]
  wire  _T_1025; // @[Monitor.scala 44:11]
  wire  _T_1026; // @[Monitor.scala 44:11]
  wire  _T_1028; // @[Monitor.scala 393:20]
  wire  _T_1029; // @[Decoupled.scala 40:37]
  wire [22:0] _T_1031; // @[package.scala 189:77]
  wire [7:0] _T_1033; // @[package.scala 189:46]
  reg [7:0] _T_1037; // @[Edges.scala 230:27]
  reg [31:0] _RAND_4;
  wire [7:0] _T_1039; // @[Edges.scala 231:28]
  wire  _T_1040; // @[Edges.scala 232:25]
  reg [2:0] _T_1048; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_5;
  reg [1:0] _T_1049; // @[Monitor.scala 533:22]
  reg [31:0] _RAND_6;
  reg [3:0] _T_1050; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_7;
  reg [3:0] _T_1052; // @[Monitor.scala 536:22]
  reg [31:0] _RAND_8;
  reg  _T_1053; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_9;
  wire  _T_1054; // @[Monitor.scala 538:22]
  wire  _T_1055; // @[Monitor.scala 538:19]
  wire  _T_1056; // @[Monitor.scala 539:29]
  wire  _T_1058; // @[Monitor.scala 51:11]
  wire  _T_1059; // @[Monitor.scala 51:11]
  wire  _T_1060; // @[Monitor.scala 540:29]
  wire  _T_1062; // @[Monitor.scala 51:11]
  wire  _T_1063; // @[Monitor.scala 51:11]
  wire  _T_1064; // @[Monitor.scala 541:29]
  wire  _T_1066; // @[Monitor.scala 51:11]
  wire  _T_1067; // @[Monitor.scala 51:11]
  wire  _T_1072; // @[Monitor.scala 543:29]
  wire  _T_1074; // @[Monitor.scala 51:11]
  wire  _T_1075; // @[Monitor.scala 51:11]
  wire  _T_1076; // @[Monitor.scala 544:29]
  wire  _T_1078; // @[Monitor.scala 51:11]
  wire  _T_1079; // @[Monitor.scala 51:11]
  wire  _T_1081; // @[Monitor.scala 546:20]
  reg  _T_1082; // @[Monitor.scala 568:27]
  reg [31:0] _RAND_10;
  reg [7:0] _T_1092; // @[Edges.scala 230:27]
  reg [31:0] _RAND_11;
  wire [7:0] _T_1094; // @[Edges.scala 231:28]
  wire  _T_1095; // @[Edges.scala 232:25]
  reg [7:0] _T_1111; // @[Edges.scala 230:27]
  reg [31:0] _RAND_12;
  wire [7:0] _T_1113; // @[Edges.scala 231:28]
  wire  _T_1114; // @[Edges.scala 232:25]
  wire  _T_1124; // @[Monitor.scala 574:27]
  wire  _T_1129; // @[Monitor.scala 576:14]
  wire  _T_1131; // @[Monitor.scala 576:13]
  wire  _T_1132; // @[Monitor.scala 576:13]
  wire [1:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_1136; // @[Monitor.scala 581:27]
  wire  _T_1138; // @[Monitor.scala 581:75]
  wire  _T_1139; // @[Monitor.scala 581:72]
  wire  _T_1141; // @[Monitor.scala 583:21]
  wire  _T_1145; // @[Monitor.scala 51:11]
  wire  _T_1146; // @[Monitor.scala 51:11]
  wire [1:0] _GEN_16; // @[Monitor.scala 581:91]
  wire  _T_1147; // @[Monitor.scala 587:20]
  wire  _T_1149; // @[Monitor.scala 587:33]
  wire  _T_1150; // @[Monitor.scala 587:30]
  wire  _T_1152; // @[Monitor.scala 51:11]
  wire  _T_1153; // @[Monitor.scala 51:11]
  wire  _T_1154; // @[Monitor.scala 590:27]
  wire  _T_1155; // @[Monitor.scala 590:38]
  wire  _T_1156; // @[Monitor.scala 590:36]
  reg [31:0] _T_1157; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_13;
  wire  _T_1160; // @[Monitor.scala 595:36]
  wire  _T_1161; // @[Monitor.scala 595:27]
  wire  _T_1162; // @[Monitor.scala 595:56]
  wire  _T_1163; // @[Monitor.scala 595:44]
  wire  _T_1165; // @[Monitor.scala 595:12]
  wire  _T_1166; // @[Monitor.scala 595:12]
  wire [31:0] _T_1168; // @[Monitor.scala 597:26]
  wire  _T_1171; // @[Monitor.scala 598:27]
  wire  _GEN_19; // @[Monitor.scala 44:11]
  wire  _GEN_25; // @[Monitor.scala 44:11]
  wire  _GEN_33; // @[Monitor.scala 44:11]
  wire  _GEN_37; // @[Monitor.scala 44:11]
  wire  _GEN_41; // @[Monitor.scala 44:11]
  wire  _GEN_45; // @[Monitor.scala 44:11]
  wire  _GEN_49; // @[Monitor.scala 44:11]
  wire  _GEN_53; // @[Monitor.scala 44:11]
  wire  _GEN_57; // @[Monitor.scala 51:11]
  wire  _GEN_63; // @[Monitor.scala 51:11]
  wire  _GEN_69; // @[Monitor.scala 51:11]
  wire  _GEN_75; // @[Monitor.scala 51:11]
  wire  _GEN_79; // @[Monitor.scala 51:11]
  wire  _GEN_83; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_7 = 23'hff << io_in_a_bits_size; // @[package.scala 189:77]
  assign _T_9 = ~_T_7[7:0]; // @[package.scala 189:46]
  assign _GEN_18 = {{28'd0}, _T_9}; // @[Edges.scala 22:16]
  assign _T_10 = io_in_a_bits_address & _GEN_18; // @[Edges.scala 22:16]
  assign _T_11 = _T_10 == 36'h0; // @[Edges.scala 22:24]
  assign _T_17 = {1'b0,$signed(io_in_a_bits_address)}; // @[Parameters.scala 137:49]
  assign _T_25 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_27 = io_in_a_bits_address ^ 36'h400000000; // @[Parameters.scala 137:31]
  assign _T_28 = {1'b0,$signed(_T_27)}; // @[Parameters.scala 137:49]
  assign _T_30 = $signed(_T_28) & -37'sh400000000; // @[Parameters.scala 137:52]
  assign _T_31 = $signed(_T_30) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_32 = io_in_a_bits_address ^ 36'h8000000; // @[Parameters.scala 137:31]
  assign _T_33 = {1'b0,$signed(_T_32)}; // @[Parameters.scala 137:49]
  assign _T_37 = io_in_a_bits_address ^ 36'h3000; // @[Parameters.scala 137:31]
  assign _T_38 = {1'b0,$signed(_T_37)}; // @[Parameters.scala 137:49]
  assign _T_40 = $signed(_T_38) & -37'sh10001000; // @[Parameters.scala 137:52]
  assign _T_41 = $signed(_T_40) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_42 = io_in_a_bits_address ^ 36'hc000000; // @[Parameters.scala 137:31]
  assign _T_43 = {1'b0,$signed(_T_42)}; // @[Parameters.scala 137:49]
  assign _T_45 = $signed(_T_43) & -37'sh4000000; // @[Parameters.scala 137:52]
  assign _T_46 = $signed(_T_45) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_47 = io_in_a_bits_address ^ 36'h2000000; // @[Parameters.scala 137:31]
  assign _T_48 = {1'b0,$signed(_T_47)}; // @[Parameters.scala 137:49]
  assign _T_50 = $signed(_T_48) & -37'sh10000; // @[Parameters.scala 137:52]
  assign _T_51 = $signed(_T_50) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_55 = $signed(_T_17) & -37'sh10001000; // @[Parameters.scala 137:52]
  assign _T_56 = $signed(_T_55) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_57 = io_in_a_bits_address ^ 36'h20000000; // @[Parameters.scala 137:31]
  assign _T_58 = {1'b0,$signed(_T_57)}; // @[Parameters.scala 137:49]
  assign _T_60 = $signed(_T_58) & -37'sh20000000; // @[Parameters.scala 137:52]
  assign _T_61 = $signed(_T_60) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_62 = io_in_a_bits_address ^ 36'h10001000; // @[Parameters.scala 137:31]
  assign _T_63 = {1'b0,$signed(_T_62)}; // @[Parameters.scala 137:49]
  assign _T_65 = $signed(_T_63) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_66 = $signed(_T_65) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_67 = io_in_a_bits_address ^ 36'h4000; // @[Parameters.scala 137:31]
  assign _T_68 = {1'b0,$signed(_T_67)}; // @[Parameters.scala 137:49]
  assign _T_70 = $signed(_T_68) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_71 = $signed(_T_70) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_72 = io_in_a_bits_address ^ 36'h2010000; // @[Parameters.scala 137:31]
  assign _T_73 = {1'b0,$signed(_T_72)}; // @[Parameters.scala 137:49]
  assign _T_75 = $signed(_T_73) & -37'sh4000; // @[Parameters.scala 137:52]
  assign _T_76 = $signed(_T_75) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_87 = 4'h6 == io_in_a_bits_size; // @[Parameters.scala 92:48]
  assign _T_89 = io_in_a_bits_address ^ 36'ha000000; // @[Parameters.scala 137:31]
  assign _T_90 = {1'b0,$signed(_T_89)}; // @[Parameters.scala 137:49]
  assign _T_92 = $signed(_T_90) & -37'sh200000; // @[Parameters.scala 137:52]
  assign _T_93 = $signed(_T_92) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_94 = io_in_a_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  assign _T_95 = {1'b0,$signed(_T_94)}; // @[Parameters.scala 137:49]
  assign _T_97 = $signed(_T_95) & -37'sh800000000; // @[Parameters.scala 137:52]
  assign _T_98 = $signed(_T_97) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_99 = _T_93 | _T_98; // @[Parameters.scala 552:42]
  assign _T_100 = _T_87 & _T_99; // @[Parameters.scala 551:56]
  assign _T_104 = _T_100 | reset; // @[Monitor.scala 44:11]
  assign _T_105 = ~_T_104; // @[Monitor.scala 44:11]
  assign _T_108 = ~reset; // @[Monitor.scala 44:11]
  assign _T_117 = _T_11 | reset; // @[Monitor.scala 44:11]
  assign _T_118 = ~_T_117; // @[Monitor.scala 44:11]
  assign _T_132 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_243 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_245 = io_in_a_bits_size <= 4'h6; // @[Parameters.scala 93:42]
  assign _T_256 = $signed(_T_33) & -37'sh2200000; // @[Parameters.scala 137:52]
  assign _T_257 = $signed(_T_256) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_286 = $signed(_T_63) & -37'sh3000; // @[Parameters.scala 137:52]
  assign _T_287 = $signed(_T_286) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_298 = _T_31 | _T_257; // @[Parameters.scala 552:42]
  assign _T_299 = _T_298 | _T_98; // @[Parameters.scala 552:42]
  assign _T_300 = _T_299 | _T_46; // @[Parameters.scala 552:42]
  assign _T_301 = _T_300 | _T_51; // @[Parameters.scala 552:42]
  assign _T_302 = _T_301 | _T_56; // @[Parameters.scala 552:42]
  assign _T_303 = _T_302 | _T_61; // @[Parameters.scala 552:42]
  assign _T_304 = _T_303 | _T_287; // @[Parameters.scala 552:42]
  assign _T_305 = _T_304 | _T_71; // @[Parameters.scala 552:42]
  assign _T_306 = _T_305 | _T_76; // @[Parameters.scala 552:42]
  assign _T_307 = _T_245 & _T_306; // @[Parameters.scala 551:56]
  assign _T_309 = io_in_a_bits_size <= 4'h8; // @[Parameters.scala 93:42]
  assign _T_315 = $signed(_T_38) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_316 = $signed(_T_315) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_317 = _T_309 & _T_316; // @[Parameters.scala 551:56]
  assign _T_319 = _T_307 | _T_317; // @[Parameters.scala 553:30]
  assign _T_321 = _T_319 | reset; // @[Monitor.scala 44:11]
  assign _T_322 = ~_T_321; // @[Monitor.scala 44:11]
  assign _T_341 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_343 = io_in_a_bits_size <= 4'h7; // @[Parameters.scala 93:42]
  assign _T_351 = _T_343 & _T_31; // @[Parameters.scala 551:56]
  assign _T_401 = _T_257 | _T_98; // @[Parameters.scala 552:42]
  assign _T_402 = _T_401 | _T_46; // @[Parameters.scala 552:42]
  assign _T_403 = _T_402 | _T_51; // @[Parameters.scala 552:42]
  assign _T_404 = _T_403 | _T_56; // @[Parameters.scala 552:42]
  assign _T_405 = _T_404 | _T_61; // @[Parameters.scala 552:42]
  assign _T_406 = _T_405 | _T_287; // @[Parameters.scala 552:42]
  assign _T_407 = _T_406 | _T_71; // @[Parameters.scala 552:42]
  assign _T_408 = _T_407 | _T_76; // @[Parameters.scala 552:42]
  assign _T_409 = _T_245 & _T_408; // @[Parameters.scala 551:56]
  assign _T_421 = _T_351 | _T_409; // @[Parameters.scala 553:30]
  assign _T_422 = _T_421 | _T_317; // @[Parameters.scala 553:30]
  assign _T_424 = _T_422 | reset; // @[Monitor.scala 44:11]
  assign _T_425 = ~_T_424; // @[Monitor.scala 44:11]
  assign _T_440 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_541 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_550 = io_in_a_bits_size <= 4'h3; // @[Parameters.scala 93:42]
  assign _T_604 = _T_401 | _T_41; // @[Parameters.scala 552:42]
  assign _T_605 = _T_604 | _T_46; // @[Parameters.scala 552:42]
  assign _T_606 = _T_605 | _T_51; // @[Parameters.scala 552:42]
  assign _T_607 = _T_606 | _T_56; // @[Parameters.scala 552:42]
  assign _T_608 = _T_607 | _T_61; // @[Parameters.scala 552:42]
  assign _T_609 = _T_608 | _T_66; // @[Parameters.scala 552:42]
  assign _T_610 = _T_609 | _T_71; // @[Parameters.scala 552:42]
  assign _T_611 = _T_610 | _T_76; // @[Parameters.scala 552:42]
  assign _T_612 = _T_550 & _T_611; // @[Parameters.scala 551:56]
  assign _T_616 = _T_612 | reset; // @[Monitor.scala 44:11]
  assign _T_617 = ~_T_616; // @[Monitor.scala 44:11]
  assign _T_632 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_723 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_794 = _T_245 & _T_99; // @[Parameters.scala 551:56]
  assign _T_807 = _T_794 | _T_317; // @[Parameters.scala 553:30]
  assign _T_809 = _T_807 | reset; // @[Monitor.scala 44:11]
  assign _T_810 = ~_T_809; // @[Monitor.scala 44:11]
  assign _T_829 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_831 = _T_829 | reset; // @[Monitor.scala 51:11]
  assign _T_832 = ~_T_831; // @[Monitor.scala 51:11]
  assign _T_836 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_844 = io_in_d_bits_param == 2'h0; // @[Monitor.scala 310:28]
  assign _T_846 = _T_844 | reset; // @[Monitor.scala 51:11]
  assign _T_847 = ~_T_846; // @[Monitor.scala 51:11]
  assign _T_848 = ~io_in_d_bits_corrupt; // @[Monitor.scala 311:15]
  assign _T_850 = _T_848 | reset; // @[Monitor.scala 51:11]
  assign _T_851 = ~_T_850; // @[Monitor.scala 51:11]
  assign _T_852 = ~io_in_d_bits_denied; // @[Monitor.scala 312:15]
  assign _T_854 = _T_852 | reset; // @[Monitor.scala 51:11]
  assign _T_855 = ~_T_854; // @[Monitor.scala 51:11]
  assign _T_856 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_867 = io_in_d_bits_param <= 2'h2; // @[Bundles.scala 104:26]
  assign _T_869 = _T_867 | reset; // @[Monitor.scala 51:11]
  assign _T_870 = ~_T_869; // @[Monitor.scala 51:11]
  assign _T_871 = io_in_d_bits_param != 2'h2; // @[Monitor.scala 320:28]
  assign _T_873 = _T_871 | reset; // @[Monitor.scala 51:11]
  assign _T_874 = ~_T_873; // @[Monitor.scala 51:11]
  assign _T_884 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_904 = _T_852 | io_in_d_bits_corrupt; // @[Monitor.scala 331:30]
  assign _T_906 = _T_904 | reset; // @[Monitor.scala 51:11]
  assign _T_907 = ~_T_906; // @[Monitor.scala 51:11]
  assign _T_913 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_930 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_948 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_980 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_987 = ~io_in_a_bits_opcode[2]; // @[Edges.scala 93:28]
  assign _T_991 = _T_989 - 8'h1; // @[Edges.scala 231:28]
  assign _T_992 = _T_989 == 8'h0; // @[Edges.scala 232:25]
  assign _T_1005 = ~_T_992; // @[Monitor.scala 386:22]
  assign _T_1006 = io_in_a_valid & _T_1005; // @[Monitor.scala 386:19]
  assign _T_1007 = io_in_a_bits_opcode == _T_1000; // @[Monitor.scala 387:32]
  assign _T_1009 = _T_1007 | reset; // @[Monitor.scala 44:11]
  assign _T_1010 = ~_T_1009; // @[Monitor.scala 44:11]
  assign _T_1015 = io_in_a_bits_size == _T_1002; // @[Monitor.scala 389:32]
  assign _T_1017 = _T_1015 | reset; // @[Monitor.scala 44:11]
  assign _T_1018 = ~_T_1017; // @[Monitor.scala 44:11]
  assign _T_1023 = io_in_a_bits_address == _T_1004; // @[Monitor.scala 391:32]
  assign _T_1025 = _T_1023 | reset; // @[Monitor.scala 44:11]
  assign _T_1026 = ~_T_1025; // @[Monitor.scala 44:11]
  assign _T_1028 = _T_980 & _T_992; // @[Monitor.scala 393:20]
  assign _T_1029 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_1031 = 23'hff << io_in_d_bits_size; // @[package.scala 189:77]
  assign _T_1033 = ~_T_1031[7:0]; // @[package.scala 189:46]
  assign _T_1039 = _T_1037 - 8'h1; // @[Edges.scala 231:28]
  assign _T_1040 = _T_1037 == 8'h0; // @[Edges.scala 232:25]
  assign _T_1054 = ~_T_1040; // @[Monitor.scala 538:22]
  assign _T_1055 = io_in_d_valid & _T_1054; // @[Monitor.scala 538:19]
  assign _T_1056 = io_in_d_bits_opcode == _T_1048; // @[Monitor.scala 539:29]
  assign _T_1058 = _T_1056 | reset; // @[Monitor.scala 51:11]
  assign _T_1059 = ~_T_1058; // @[Monitor.scala 51:11]
  assign _T_1060 = io_in_d_bits_param == _T_1049; // @[Monitor.scala 540:29]
  assign _T_1062 = _T_1060 | reset; // @[Monitor.scala 51:11]
  assign _T_1063 = ~_T_1062; // @[Monitor.scala 51:11]
  assign _T_1064 = io_in_d_bits_size == _T_1050; // @[Monitor.scala 541:29]
  assign _T_1066 = _T_1064 | reset; // @[Monitor.scala 51:11]
  assign _T_1067 = ~_T_1066; // @[Monitor.scala 51:11]
  assign _T_1072 = io_in_d_bits_sink == _T_1052; // @[Monitor.scala 543:29]
  assign _T_1074 = _T_1072 | reset; // @[Monitor.scala 51:11]
  assign _T_1075 = ~_T_1074; // @[Monitor.scala 51:11]
  assign _T_1076 = io_in_d_bits_denied == _T_1053; // @[Monitor.scala 544:29]
  assign _T_1078 = _T_1076 | reset; // @[Monitor.scala 51:11]
  assign _T_1079 = ~_T_1078; // @[Monitor.scala 51:11]
  assign _T_1081 = _T_1029 & _T_1040; // @[Monitor.scala 546:20]
  assign _T_1094 = _T_1092 - 8'h1; // @[Edges.scala 231:28]
  assign _T_1095 = _T_1092 == 8'h0; // @[Edges.scala 232:25]
  assign _T_1113 = _T_1111 - 8'h1; // @[Edges.scala 231:28]
  assign _T_1114 = _T_1111 == 8'h0; // @[Edges.scala 232:25]
  assign _T_1124 = _T_980 & _T_1095; // @[Monitor.scala 574:27]
  assign _T_1129 = ~_T_1082; // @[Monitor.scala 576:14]
  assign _T_1131 = _T_1129 | reset; // @[Monitor.scala 576:13]
  assign _T_1132 = ~_T_1131; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_1124 ? 2'h1 : 2'h0; // @[Monitor.scala 574:72]
  assign _T_1136 = _T_1029 & _T_1114; // @[Monitor.scala 581:27]
  assign _T_1138 = ~_T_836; // @[Monitor.scala 581:75]
  assign _T_1139 = _T_1136 & _T_1138; // @[Monitor.scala 581:72]
  assign _T_1141 = _GEN_15[0] | _T_1082; // @[Monitor.scala 583:21]
  assign _T_1145 = _T_1141 | reset; // @[Monitor.scala 51:11]
  assign _T_1146 = ~_T_1145; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_1139 ? 2'h1 : 2'h0; // @[Monitor.scala 581:91]
  assign _T_1147 = _GEN_15[0] != _GEN_16[0]; // @[Monitor.scala 587:20]
  assign _T_1149 = ~_GEN_15[0]; // @[Monitor.scala 587:33]
  assign _T_1150 = _T_1147 | _T_1149; // @[Monitor.scala 587:30]
  assign _T_1152 = _T_1150 | reset; // @[Monitor.scala 51:11]
  assign _T_1153 = ~_T_1152; // @[Monitor.scala 51:11]
  assign _T_1154 = _T_1082 | _GEN_15[0]; // @[Monitor.scala 590:27]
  assign _T_1155 = ~_GEN_16[0]; // @[Monitor.scala 590:38]
  assign _T_1156 = _T_1154 & _T_1155; // @[Monitor.scala 590:36]
  assign _T_1160 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_1161 = _T_1129 | _T_1160; // @[Monitor.scala 595:27]
  assign _T_1162 = _T_1157 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_1163 = _T_1161 | _T_1162; // @[Monitor.scala 595:44]
  assign _T_1165 = _T_1163 | reset; // @[Monitor.scala 595:12]
  assign _T_1166 = ~_T_1165; // @[Monitor.scala 595:12]
  assign _T_1168 = _T_1157 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_1171 = _T_980 | _T_1029; // @[Monitor.scala 598:27]
  assign _GEN_19 = io_in_a_valid & _T_25; // @[Monitor.scala 44:11]
  assign _GEN_25 = io_in_a_valid & _T_132; // @[Monitor.scala 44:11]
  assign _GEN_33 = io_in_a_valid & _T_243; // @[Monitor.scala 44:11]
  assign _GEN_37 = io_in_a_valid & _T_341; // @[Monitor.scala 44:11]
  assign _GEN_41 = io_in_a_valid & _T_440; // @[Monitor.scala 44:11]
  assign _GEN_45 = io_in_a_valid & _T_541; // @[Monitor.scala 44:11]
  assign _GEN_49 = io_in_a_valid & _T_632; // @[Monitor.scala 44:11]
  assign _GEN_53 = io_in_a_valid & _T_723; // @[Monitor.scala 44:11]
  assign _GEN_57 = io_in_d_valid & _T_836; // @[Monitor.scala 51:11]
  assign _GEN_63 = io_in_d_valid & _T_856; // @[Monitor.scala 51:11]
  assign _GEN_69 = io_in_d_valid & _T_884; // @[Monitor.scala 51:11]
  assign _GEN_75 = io_in_d_valid & _T_913; // @[Monitor.scala 51:11]
  assign _GEN_79 = io_in_d_valid & _T_930; // @[Monitor.scala 51:11]
  assign _GEN_83 = io_in_d_valid & _T_948; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_989 = _RAND_0[7:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_1000 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_1002 = _RAND_2[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {2{`RANDOM}};
  _T_1004 = _RAND_3[35:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_1037 = _RAND_4[7:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  _T_1048 = _RAND_5[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_1049 = _RAND_6[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_1050 = _RAND_7[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_1052 = _RAND_8[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_1053 = _RAND_9[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_1082 = _RAND_10[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_1092 = _RAND_11[7:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_1111 = _RAND_12[7:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  _T_1157 = _RAND_13[31:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_989 <= 8'h0;
    end else if (_T_980) begin
      if (_T_992) begin
        if (_T_987) begin
          _T_989 <= _T_9;
        end else begin
          _T_989 <= 8'h0;
        end
      end else begin
        _T_989 <= _T_991;
      end
    end
    if (_T_1028) begin
      _T_1000 <= io_in_a_bits_opcode;
    end
    if (_T_1028) begin
      _T_1002 <= io_in_a_bits_size;
    end
    if (_T_1028) begin
      _T_1004 <= io_in_a_bits_address;
    end
    if (reset) begin
      _T_1037 <= 8'h0;
    end else if (_T_1029) begin
      if (_T_1040) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1037 <= _T_1033;
        end else begin
          _T_1037 <= 8'h0;
        end
      end else begin
        _T_1037 <= _T_1039;
      end
    end
    if (_T_1081) begin
      _T_1048 <= io_in_d_bits_opcode;
    end
    if (_T_1081) begin
      _T_1049 <= io_in_d_bits_param;
    end
    if (_T_1081) begin
      _T_1050 <= io_in_d_bits_size;
    end
    if (_T_1081) begin
      _T_1052 <= io_in_d_bits_sink;
    end
    if (_T_1081) begin
      _T_1053 <= io_in_d_bits_denied;
    end
    if (reset) begin
      _T_1082 <= 1'h0;
    end else begin
      _T_1082 <= _T_1156;
    end
    if (reset) begin
      _T_1092 <= 8'h0;
    end else if (_T_980) begin
      if (_T_1095) begin
        if (_T_987) begin
          _T_1092 <= _T_9;
        end else begin
          _T_1092 <= 8'h0;
        end
      end else begin
        _T_1092 <= _T_1094;
      end
    end
    if (reset) begin
      _T_1111 <= 8'h0;
    end else if (_T_1029) begin
      if (_T_1114) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1111 <= _T_1033;
        end else begin
          _T_1111 <= 8'h0;
        end
      end else begin
        _T_1111 <= _T_1113;
      end
    end
    if (reset) begin
      _T_1157 <= 32'h0;
    end else if (_T_1171) begin
      _T_1157 <= 32'h0;
    end else begin
      _T_1157 <= _T_1168;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_105) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_105) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_108) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_108) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_25 & _T_105) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_25 & _T_105) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_25 & _T_108) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_25 & _T_108) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_25 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_25 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_25 & _T_108) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_25 & _T_108) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_322) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_322) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_37 & _T_425) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_37 & _T_425) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_37 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_37 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_41 & _T_425) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_41 & _T_425) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_41 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_41 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_45 & _T_617) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_45 & _T_617) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_45 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_45 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_617) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_617) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_810) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_810) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_832) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_832) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_57 & _T_847) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseeAck carries invalid param (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_57 & _T_847) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_57 & _T_851) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is corrupt (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_57 & _T_851) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_57 & _T_855) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is denied (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_57 & _T_855) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_63 & _T_870) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid cap param (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_63 & _T_870) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_63 & _T_874) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries toN param (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_63 & _T_874) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_63 & _T_851) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is corrupt (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_63 & _T_851) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_69 & _T_870) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid cap param (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_69 & _T_870) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_69 & _T_874) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries toN param (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_69 & _T_874) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_69 & _T_907) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied but not corrupt (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_69 & _T_907) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_847) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid param (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_847) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_851) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is corrupt (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_851) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_79 & _T_847) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid param (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_79 & _T_847) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_79 & _T_907) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied but not corrupt (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_79 & _T_907) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_847) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid param (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_847) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_851) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is corrupt (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_851) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1006 & _T_1010) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1006 & _T_1010) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1006 & _T_1018) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel size changed within multibeat operation (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1006 & _T_1018) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1006 & _T_1026) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at Periphery.scala:93:56)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1006 & _T_1026) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1055 & _T_1059) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1055 & _T_1059) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1055 & _T_1063) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel param changed within multibeat operation (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1055 & _T_1063) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1055 & _T_1067) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1055 & _T_1067) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1055 & _T_1075) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel sink changed with multibeat operation (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1055 & _T_1075) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1055 & _T_1079) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1055 & _T_1079) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1124 & _T_1132) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at Periphery.scala:93:56)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1124 & _T_1132) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1139 & _T_1146) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1139 & _T_1146) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1153) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' and 'D' concurrent, despite minlatency 4 (connected at Periphery.scala:93:56)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1153) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1166) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at Periphery.scala:93:56)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1166) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
