module scr_cpu (/*AUTOARG*/
    // Outputs
    PRDATA, PREADY, PSLVERR, SCR__RSTREQN0, SCR__CLKDIS0,
    SCR__CLKSEL0, SCR__CLKSEL1, SCR__CLKSEL2, SCR__CLKDIV0,
    SCR__CLKDIV1, SCR__CLKDIV2, SCR__RSTCYCLE0, SCR__RSTCYCLE1,
    SCR__RSTCYCLE2, SCR__RSTCYCLE3, SCR__RSTCYCLE4,
    SCR__PLL_CPU_RESETB, SCR__PLL_CPU_P, SCR__PLL_CPU_M,
    SCR__PLL_CPU_BYPASS, SCR__PLL_CPU_S, SCR__PLL_CPU_FOUT_MASK,
    SCR__PLL_CPU_LOCK_EN, SCR__PLL_CPU_LOCK_CON_IN,
    SCR__PLL_CPU_LOCK_CON_OUT, SCR__PLL_CPU_LOCK_CON_DLY,
    SCR__PLL_CPU_LOCK_CON_REV, SCR__PLL_CPU_FEED_EN,
    SCR__PLL_CPU_FSEL, SCR__PLL_CPU_RSEL, SCR__PLL_CPU_AFC_ENB,
    SCR__PLL_CPU_AFCINIT_SEL, SCR__PLL_CPU_LRD_EN,
    SCR__PLL_CPU_EXTAFC, SCR__PLL_CPU_ICP, SCR__PLL_CPU_PBIAS_CTRL,
    SCR__PLL_CPU_PBIAS_CTRL_EN, SCR__PLL_CPU_VCO_BOOST,
    SFR__RTC_TOGGLE, SFR__PSD_TEST_CLOCK_ENABLE,
    SFR__SUPPRESS_STRB_ON_CORRUPT, SFR__RESETCTRL_HARTISINRESET_0,
    SFR__RESETCTRL_HARTISINRESET_1, SFR__DEBUG_DMACTIVEACK,
    SFR__DEBUG_SYSTEMJTAG_MFR_ID, SFR__DEBUG_SYSTEMJTAG_VERSION,
    SFR__DEBUG_SYSTEMJTAG_PART_NUMBER, SFR__SYSPORT_AWUSER,
    SFR__SYSPORT_ARUSER, SFR__MEMPORT_AWUSER, SFR__MEMPORT_ARUSER,
    // Inputs
    PCLK, PRESETn, PADDR, PENABLE, PSEL, PSTRB, PPROT, PWDATA, PWRITE,
    SCR__VERSION0, SCR__VERSION1, SCR__VERSION2, SCR__VERSION3,
    SCR__PLL_CPU_LOCK, SCR__PLL_CPU_AFC_CODE, SFR__DEBUG_NDRESET,
    SFR__DEBUG_DMACTIVE, SFR__HALT_FROM_TILE_0, SFR__HALT_FROM_TILE_1,
    SFR__WFI_FROM_TILE_0, SFR__WFI_FROM_TILE_1,
    SFR__CEASE_FROM_TILE_0, SFR__CEASE_FROM_TILE_1,
    SFR__DEBUG_FROM_TILE_0, SFR__DEBUG_FROM_TILE_1
    );

parameter DW=32;
parameter AW=12;

localparam NUM_ENTRY = 1<<(AW-$clog2(DW/8));

input                   PCLK;
input                   PRESETn;

input   [AW-1:0]        PADDR;
input                   PENABLE;
input                   PSEL;
input   [(DW/8)-1:0]    PSTRB;
input   [02:00]         PPROT;
input   [DW-1:0]        PWDATA;
input                   PWRITE;
output  [DW-1:0]        PRDATA;
output                  PREADY;
output                  PSLVERR;

//========================================
input  [31:0]   SCR__VERSION0;
input  [31:0]   SCR__VERSION1;
input  [31:0]   SCR__VERSION2;
input  [31:0]   SCR__VERSION3;
output [16:0]   SCR__RSTREQN0;
output [16:0]   SCR__CLKDIS0;
output          SCR__CLKSEL0;
output          SCR__CLKSEL1;
output          SCR__CLKSEL2;
output [7:0]    SCR__CLKDIV0;
output [7:0]    SCR__CLKDIV1;
output [7:0]    SCR__CLKDIV2;
output [7:0]    SCR__RSTCYCLE0;
output [7:0]    SCR__RSTCYCLE1;
output [7:0]    SCR__RSTCYCLE2;
output [7:0]    SCR__RSTCYCLE3;
output [7:0]    SCR__RSTCYCLE4;
output          SCR__PLL_CPU_RESETB;
output [5:0]    SCR__PLL_CPU_P;
output [9:0]    SCR__PLL_CPU_M;
output          SCR__PLL_CPU_BYPASS;
output [2:0]    SCR__PLL_CPU_S;
output          SCR__PLL_CPU_FOUT_MASK;
output          SCR__PLL_CPU_LOCK_EN;
output [1:0]    SCR__PLL_CPU_LOCK_CON_IN;
output [1:0]    SCR__PLL_CPU_LOCK_CON_OUT;
output [1:0]    SCR__PLL_CPU_LOCK_CON_DLY;
output [1:0]    SCR__PLL_CPU_LOCK_CON_REV;
output          SCR__PLL_CPU_FEED_EN;
output          SCR__PLL_CPU_FSEL;
output [3:0]    SCR__PLL_CPU_RSEL;
output          SCR__PLL_CPU_AFC_ENB;
output          SCR__PLL_CPU_AFCINIT_SEL;
output          SCR__PLL_CPU_LRD_EN;
output [4:0]    SCR__PLL_CPU_EXTAFC;
output [1:0]    SCR__PLL_CPU_ICP;
output          SCR__PLL_CPU_PBIAS_CTRL;
output          SCR__PLL_CPU_PBIAS_CTRL_EN;
output          SCR__PLL_CPU_VCO_BOOST;
input           SCR__PLL_CPU_LOCK;
input  [4:0]    SCR__PLL_CPU_AFC_CODE;
output          SFR__RTC_TOGGLE;
output          SFR__PSD_TEST_CLOCK_ENABLE;
output          SFR__SUPPRESS_STRB_ON_CORRUPT;
output          SFR__RESETCTRL_HARTISINRESET_0;
output          SFR__RESETCTRL_HARTISINRESET_1;
output          SFR__DEBUG_DMACTIVEACK;
output [10:0]   SFR__DEBUG_SYSTEMJTAG_MFR_ID;
output [3:0]    SFR__DEBUG_SYSTEMJTAG_VERSION;
output [15:0]   SFR__DEBUG_SYSTEMJTAG_PART_NUMBER;
output [9:0]    SFR__SYSPORT_AWUSER;
output [9:0]    SFR__SYSPORT_ARUSER;
output [9:0]    SFR__MEMPORT_AWUSER;
output [9:0]    SFR__MEMPORT_ARUSER;
input           SFR__DEBUG_NDRESET;
input           SFR__DEBUG_DMACTIVE;
input           SFR__HALT_FROM_TILE_0;
input           SFR__HALT_FROM_TILE_1;
input           SFR__WFI_FROM_TILE_0;
input           SFR__WFI_FROM_TILE_1;
input           SFR__CEASE_FROM_TILE_0;
input           SFR__CEASE_FROM_TILE_1;
input           SFR__DEBUG_FROM_TILE_0;
input           SFR__DEBUG_FROM_TILE_1;

/*AUTOINPUT*/
/*AUTOOUTPUT*/
/*AUTOINOUT*/

reg     [DW-1:0]        REGFILE[NUM_ENTRY-1:0];
reg     [DW-1:0]        READFILE[NUM_ENTRY-1:0];
reg     [DW-1:0]        DVALFILE[NUM_ENTRY-1:0];

/*AUTOWIRE*/
/*AUTOREG*/

/*AUTOTIEOFF*/

//========================================
/* Input descriptor
#Address,LSB,Width,Name,Default Value
0,0,32,SCR__VERSION0,EXT
1,0,32,SCR__VERSION1,EXT
2,0,32,SCR__VERSION2,EXT
3,0,32,SCR__VERSION3,EXT
4,0,17,SCR__RSTREQN0,'1
12,0,17,SCR__CLKDIS0,'0
64,0,1,SCR__CLKSEL0,1'b0
65,0,1,SCR__CLKSEL1,1'b0
66,0,1,SCR__CLKSEL2,1'b0
192,0,8,SCR__CLKDIV0,8'h0
193,0,8,SCR__CLKDIV1,8'h0
194,0,8,SCR__CLKDIV2,8'h0
320,0,8,SCR__RSTCYCLE0,8'd16
321,0,8,SCR__RSTCYCLE1,8'd16
322,0,8,SCR__RSTCYCLE2,8'd16
323,0,8,SCR__RSTCYCLE3,8'd16
324,0,8,SCR__RSTCYCLE4,8'd16
384,0,1,SCR__PLL_CPU_RESETB,1'b0
385,0,6,SCR__PLL_CPU_P,'0
385,6,10,SCR__PLL_CPU_M,'0
386,0,1,SCR__PLL_CPU_BYPASS,'1
386,1,3,SCR__PLL_CPU_S,'0
386,4,1,SCR__PLL_CPU_FOUT_MASK,'0
387,0,1,SCR__PLL_CPU_LOCK_EN,'1
387,1,2,SCR__PLL_CPU_LOCK_CON_IN,2'b11
387,3,2,SCR__PLL_CPU_LOCK_CON_OUT,2'b11
387,5,2,SCR__PLL_CPU_LOCK_CON_DLY,2'b11
387,7,2,SCR__PLL_CPU_LOCK_CON_REV,2'b00
388,0,1,SCR__PLL_CPU_FEED_EN,1'b0
388,1,1,SCR__PLL_CPU_FSEL,1'b0
388,2,4,SCR__PLL_CPU_RSEL,'0
389,0,1,SCR__PLL_CPU_AFC_ENB,'0
389,1,1,SCR__PLL_CPU_AFCINIT_SEL,'0
389,2,1,SCR__PLL_CPU_LRD_EN,'1
389,3,5,SCR__PLL_CPU_EXTAFC,'0
390,0,2,SCR__PLL_CPU_ICP,2'b01
391,0,1,SCR__PLL_CPU_PBIAS_CTRL,'0
392,1,1,SCR__PLL_CPU_PBIAS_CTRL_EN,'0
392,2,1,SCR__PLL_CPU_VCO_BOOST,'0
393,0,1,SCR__PLL_CPU_LOCK,EXT
394,0,5,SCR__PLL_CPU_AFC_CODE,EXT
512,0,1,SFR__RTC_TOGGLE,1'b0
512,1,1,SFR__PSD_TEST_CLOCK_ENABLE,1'b0
512,2,1,SFR__SUPPRESS_STRB_ON_CORRUPT,1'b0
512,3,1,SFR__RESETCTRL_HARTISINRESET_0,1'b0
512,4,1,SFR__RESETCTRL_HARTISINRESET_1,1'b0
512,5,1,SFR__DEBUG_DMACTIVEACK,1'b0
513,0,11,SFR__DEBUG_SYSTEMJTAG_MFR_ID,'0
513,12,4,SFR__DEBUG_SYSTEMJTAG_VERSION,'0
513,16,16,SFR__DEBUG_SYSTEMJTAG_PART_NUMBER,'0
514,0,10,SFR__SYSPORT_AWUSER,'0
514,16,10,SFR__SYSPORT_ARUSER,'0
515,0,10,SFR__MEMPORT_AWUSER,'0
515,16,10,SFR__MEMPORT_ARUSER,'0
576,0,1,SFR__DEBUG_NDRESET,EXT
576,1,1,SFR__DEBUG_DMACTIVE,EXT
576,2,1,SFR__HALT_FROM_TILE_0,EXT
576,3,1,SFR__HALT_FROM_TILE_1,EXT
576,4,1,SFR__WFI_FROM_TILE_0,EXT
576,5,1,SFR__WFI_FROM_TILE_1,EXT
576,6,1,SFR__CEASE_FROM_TILE_0,EXT
576,7,1,SFR__CEASE_FROM_TILE_1,EXT
576,8,1,SFR__DEBUG_FROM_TILE_0,EXT
576,9,1,SFR__DEBUG_FROM_TILE_1,EXT
*/

//========================================
// placeholder SCR__VERSION0
// placeholder SCR__VERSION1
// placeholder SCR__VERSION2
// placeholder SCR__VERSION3
assign SCR__RSTREQN0[16:0] = READFILE[4][0+:17];
assign SCR__CLKDIS0[16:0] = READFILE[12][0+:17];
assign SCR__CLKSEL0  = READFILE[64][0+:1];
assign SCR__CLKSEL1  = READFILE[65][0+:1];
assign SCR__CLKSEL2  = READFILE[66][0+:1];
assign SCR__CLKDIV0[7:0] = READFILE[192][0+:8];
assign SCR__CLKDIV1[7:0] = READFILE[193][0+:8];
assign SCR__CLKDIV2[7:0] = READFILE[194][0+:8];
assign SCR__RSTCYCLE0[7:0] = READFILE[320][0+:8];
assign SCR__RSTCYCLE1[7:0] = READFILE[321][0+:8];
assign SCR__RSTCYCLE2[7:0] = READFILE[322][0+:8];
assign SCR__RSTCYCLE3[7:0] = READFILE[323][0+:8];
assign SCR__RSTCYCLE4[7:0] = READFILE[324][0+:8];
assign SCR__PLL_CPU_RESETB  = READFILE[384][0+:1];
assign SCR__PLL_CPU_P[5:0] = READFILE[385][0+:6];
assign SCR__PLL_CPU_M[9:0] = READFILE[385][6+:10];
assign SCR__PLL_CPU_BYPASS  = READFILE[386][0+:1];
assign SCR__PLL_CPU_S[2:0] = READFILE[386][1+:3];
assign SCR__PLL_CPU_FOUT_MASK  = READFILE[386][4+:1];
assign SCR__PLL_CPU_LOCK_EN  = READFILE[387][0+:1];
assign SCR__PLL_CPU_LOCK_CON_IN[1:0] = READFILE[387][1+:2];
assign SCR__PLL_CPU_LOCK_CON_OUT[1:0] = READFILE[387][3+:2];
assign SCR__PLL_CPU_LOCK_CON_DLY[1:0] = READFILE[387][5+:2];
assign SCR__PLL_CPU_LOCK_CON_REV[1:0] = READFILE[387][7+:2];
assign SCR__PLL_CPU_FEED_EN  = READFILE[388][0+:1];
assign SCR__PLL_CPU_FSEL  = READFILE[388][1+:1];
assign SCR__PLL_CPU_RSEL[3:0] = READFILE[388][2+:4];
assign SCR__PLL_CPU_AFC_ENB  = READFILE[389][0+:1];
assign SCR__PLL_CPU_AFCINIT_SEL  = READFILE[389][1+:1];
assign SCR__PLL_CPU_LRD_EN  = READFILE[389][2+:1];
assign SCR__PLL_CPU_EXTAFC[4:0] = READFILE[389][3+:5];
assign SCR__PLL_CPU_ICP[1:0] = READFILE[390][0+:2];
assign SCR__PLL_CPU_PBIAS_CTRL  = READFILE[391][0+:1];
assign SCR__PLL_CPU_PBIAS_CTRL_EN  = READFILE[392][1+:1];
assign SCR__PLL_CPU_VCO_BOOST  = READFILE[392][2+:1];
// placeholder SCR__PLL_CPU_LOCK
// placeholder SCR__PLL_CPU_AFC_CODE
assign SFR__RTC_TOGGLE  = READFILE[512][0+:1];
assign SFR__PSD_TEST_CLOCK_ENABLE  = READFILE[512][1+:1];
assign SFR__SUPPRESS_STRB_ON_CORRUPT  = READFILE[512][2+:1];
assign SFR__RESETCTRL_HARTISINRESET_0  = READFILE[512][3+:1];
assign SFR__RESETCTRL_HARTISINRESET_1  = READFILE[512][4+:1];
assign SFR__DEBUG_DMACTIVEACK  = READFILE[512][5+:1];
assign SFR__DEBUG_SYSTEMJTAG_MFR_ID[10:0] = READFILE[513][0+:11];
assign SFR__DEBUG_SYSTEMJTAG_VERSION[3:0] = READFILE[513][12+:4];
assign SFR__DEBUG_SYSTEMJTAG_PART_NUMBER[15:0] = READFILE[513][16+:16];
assign SFR__SYSPORT_AWUSER[9:0] = READFILE[514][0+:10];
assign SFR__SYSPORT_ARUSER[9:0] = READFILE[514][16+:10];
assign SFR__MEMPORT_AWUSER[9:0] = READFILE[515][0+:10];
assign SFR__MEMPORT_ARUSER[9:0] = READFILE[515][16+:10];
// placeholder SFR__DEBUG_NDRESET
// placeholder SFR__DEBUG_DMACTIVE
// placeholder SFR__HALT_FROM_TILE_0
// placeholder SFR__HALT_FROM_TILE_1
// placeholder SFR__WFI_FROM_TILE_0
// placeholder SFR__WFI_FROM_TILE_1
// placeholder SFR__CEASE_FROM_TILE_0
// placeholder SFR__CEASE_FROM_TILE_1
// placeholder SFR__DEBUG_FROM_TILE_0
// placeholder SFR__DEBUG_FROM_TILE_1

wire    [$clog2(NUM_ENTRY)-1:0] ADDR_ENTRY;
assign  ADDR_ENTRY = PADDR[AW-1:$clog2(DW/8)];

integer i;
always @(posedge PCLK or negedge PRESETn) begin
    if ( !PRESETn ) begin
        for(i=0;i<NUM_ENTRY;i=i+1) 
            DVALFILE[i] = {DW{1'b0}};
//========================================
        // placeholder SCR__VERSION0 // 0,0,32,SCR__VERSION0,EXT
        // placeholder SCR__VERSION1 // 1,0,32,SCR__VERSION1,EXT
        // placeholder SCR__VERSION2 // 2,0,32,SCR__VERSION2,EXT
        // placeholder SCR__VERSION3 // 3,0,32,SCR__VERSION3,EXT
        DVALFILE[4][0+:17] = '1; // 4,0,17,SCR__RSTREQN0,'1
        DVALFILE[12][0+:17] = '0; // 12,0,17,SCR__CLKDIS0,'0
        DVALFILE[64][0+:1] = 1'b0; // 64,0,1,SCR__CLKSEL0,1'b0
        DVALFILE[65][0+:1] = 1'b0; // 65,0,1,SCR__CLKSEL1,1'b0
        DVALFILE[66][0+:1] = 1'b0; // 66,0,1,SCR__CLKSEL2,1'b0
        DVALFILE[192][0+:8] = 8'h0; // 192,0,8,SCR__CLKDIV0,8'h0
        DVALFILE[193][0+:8] = 8'h0; // 193,0,8,SCR__CLKDIV1,8'h0
        DVALFILE[194][0+:8] = 8'h0; // 194,0,8,SCR__CLKDIV2,8'h0
        DVALFILE[320][0+:8] = 8'd16; // 320,0,8,SCR__RSTCYCLE0,8'd16
        DVALFILE[321][0+:8] = 8'd16; // 321,0,8,SCR__RSTCYCLE1,8'd16
        DVALFILE[322][0+:8] = 8'd16; // 322,0,8,SCR__RSTCYCLE2,8'd16
        DVALFILE[323][0+:8] = 8'd16; // 323,0,8,SCR__RSTCYCLE3,8'd16
        DVALFILE[324][0+:8] = 8'd16; // 324,0,8,SCR__RSTCYCLE4,8'd16
        DVALFILE[384][0+:1] = 1'b0; // 384,0,1,SCR__PLL_CPU_RESETB,1'b0
        DVALFILE[385][0+:6] = '0; // 385,0,6,SCR__PLL_CPU_P,'0
        DVALFILE[385][6+:10] = '0; // 385,6,10,SCR__PLL_CPU_M,'0
        DVALFILE[386][0+:1] = '1; // 386,0,1,SCR__PLL_CPU_BYPASS,'1
        DVALFILE[386][1+:3] = '0; // 386,1,3,SCR__PLL_CPU_S,'0
        DVALFILE[386][4+:1] = '0; // 386,4,1,SCR__PLL_CPU_FOUT_MASK,'0
        DVALFILE[387][0+:1] = '1; // 387,0,1,SCR__PLL_CPU_LOCK_EN,'1
        DVALFILE[387][1+:2] = 2'b11; // 387,1,2,SCR__PLL_CPU_LOCK_CON_IN,2'b11
        DVALFILE[387][3+:2] = 2'b11; // 387,3,2,SCR__PLL_CPU_LOCK_CON_OUT,2'b11
        DVALFILE[387][5+:2] = 2'b11; // 387,5,2,SCR__PLL_CPU_LOCK_CON_DLY,2'b11
        DVALFILE[387][7+:2] = 2'b00; // 387,7,2,SCR__PLL_CPU_LOCK_CON_REV,2'b00
        DVALFILE[388][0+:1] = 1'b0; // 388,0,1,SCR__PLL_CPU_FEED_EN,1'b0
        DVALFILE[388][1+:1] = 1'b0; // 388,1,1,SCR__PLL_CPU_FSEL,1'b0
        DVALFILE[388][2+:4] = '0; // 388,2,4,SCR__PLL_CPU_RSEL,'0
        DVALFILE[389][0+:1] = '0; // 389,0,1,SCR__PLL_CPU_AFC_ENB,'0
        DVALFILE[389][1+:1] = '0; // 389,1,1,SCR__PLL_CPU_AFCINIT_SEL,'0
        DVALFILE[389][2+:1] = '1; // 389,2,1,SCR__PLL_CPU_LRD_EN,'1
        DVALFILE[389][3+:5] = '0; // 389,3,5,SCR__PLL_CPU_EXTAFC,'0
        DVALFILE[390][0+:2] = 2'b01; // 390,0,2,SCR__PLL_CPU_ICP,2'b01
        DVALFILE[391][0+:1] = '0; // 391,0,1,SCR__PLL_CPU_PBIAS_CTRL,'0
        DVALFILE[392][1+:1] = '0; // 392,1,1,SCR__PLL_CPU_PBIAS_CTRL_EN,'0
        DVALFILE[392][2+:1] = '0; // 392,2,1,SCR__PLL_CPU_VCO_BOOST,'0
        // placeholder SCR__PLL_CPU_LOCK // 393,0,1,SCR__PLL_CPU_LOCK,EXT
        // placeholder SCR__PLL_CPU_AFC_CODE // 394,0,5,SCR__PLL_CPU_AFC_CODE,EXT
        DVALFILE[512][0+:1] = 1'b0; // 512,0,1,SFR__RTC_TOGGLE,1'b0
        DVALFILE[512][1+:1] = 1'b0; // 512,1,1,SFR__PSD_TEST_CLOCK_ENABLE,1'b0
        DVALFILE[512][2+:1] = 1'b0; // 512,2,1,SFR__SUPPRESS_STRB_ON_CORRUPT,1'b0
        DVALFILE[512][3+:1] = 1'b0; // 512,3,1,SFR__RESETCTRL_HARTISINRESET_0,1'b0
        DVALFILE[512][4+:1] = 1'b0; // 512,4,1,SFR__RESETCTRL_HARTISINRESET_1,1'b0
        DVALFILE[512][5+:1] = 1'b0; // 512,5,1,SFR__DEBUG_DMACTIVEACK,1'b0
        DVALFILE[513][0+:11] = '0; // 513,0,11,SFR__DEBUG_SYSTEMJTAG_MFR_ID,'0
        DVALFILE[513][12+:4] = '0; // 513,12,4,SFR__DEBUG_SYSTEMJTAG_VERSION,'0
        DVALFILE[513][16+:16] = '0; // 513,16,16,SFR__DEBUG_SYSTEMJTAG_PART_NUMBER,'0
        DVALFILE[514][0+:10] = '0; // 514,0,10,SFR__SYSPORT_AWUSER,'0
        DVALFILE[514][16+:10] = '0; // 514,16,10,SFR__SYSPORT_ARUSER,'0
        DVALFILE[515][0+:10] = '0; // 515,0,10,SFR__MEMPORT_AWUSER,'0
        DVALFILE[515][16+:10] = '0; // 515,16,10,SFR__MEMPORT_ARUSER,'0
        // placeholder SFR__DEBUG_NDRESET // 576,0,1,SFR__DEBUG_NDRESET,EXT
        // placeholder SFR__DEBUG_DMACTIVE // 576,1,1,SFR__DEBUG_DMACTIVE,EXT
        // placeholder SFR__HALT_FROM_TILE_0 // 576,2,1,SFR__HALT_FROM_TILE_0,EXT
        // placeholder SFR__HALT_FROM_TILE_1 // 576,3,1,SFR__HALT_FROM_TILE_1,EXT
        // placeholder SFR__WFI_FROM_TILE_0 // 576,4,1,SFR__WFI_FROM_TILE_0,EXT
        // placeholder SFR__WFI_FROM_TILE_1 // 576,5,1,SFR__WFI_FROM_TILE_1,EXT
        // placeholder SFR__CEASE_FROM_TILE_0 // 576,6,1,SFR__CEASE_FROM_TILE_0,EXT
        // placeholder SFR__CEASE_FROM_TILE_1 // 576,7,1,SFR__CEASE_FROM_TILE_1,EXT
        // placeholder SFR__DEBUG_FROM_TILE_0 // 576,8,1,SFR__DEBUG_FROM_TILE_0,EXT
        // placeholder SFR__DEBUG_FROM_TILE_1 // 576,9,1,SFR__DEBUG_FROM_TILE_1,EXT
    end
end

integer cnt;
always @(posedge PCLK or negedge PRESETn) begin
    if ( !PRESETn ) begin
        for (cnt=0;cnt<NUM_ENTRY;cnt=cnt+1)
            REGFILE[cnt][DW-1:0] <= '0;
        /*AUTORESET*/
    end
    else begin
        if ( PSEL & PENABLE & PWRITE ) begin
            for (cnt=0;cnt<(DW/8);cnt=cnt+1) begin
                if ( PSTRB[cnt] ) begin
                    REGFILE[ADDR_ENTRY][8*cnt+:8] <= PWDATA[8*cnt+:8] ^ DVALFILE[ADDR_ENTRY][8*cnt+:8];
                end
            end
        end
    end
end

always @(*) begin
    for(i=0;i<NUM_ENTRY;i=i+1) 
        READFILE[i] = {DW{1'b0}};
    READFILE[0][0+:32] = SCR__VERSION0[31:0];
    READFILE[1][0+:32] = SCR__VERSION1[31:0];
    READFILE[2][0+:32] = SCR__VERSION2[31:0];
    READFILE[3][0+:32] = SCR__VERSION3[31:0];
    READFILE[4][0+:17] = REGFILE[4][0+:17] ^ DVALFILE[4][0+:17];
    READFILE[12][0+:17] = REGFILE[12][0+:17] ^ DVALFILE[12][0+:17];
    READFILE[64][0+:1] = REGFILE[64][0+:1] ^ DVALFILE[64][0+:1];
    READFILE[65][0+:1] = REGFILE[65][0+:1] ^ DVALFILE[65][0+:1];
    READFILE[66][0+:1] = REGFILE[66][0+:1] ^ DVALFILE[66][0+:1];
    READFILE[192][0+:8] = REGFILE[192][0+:8] ^ DVALFILE[192][0+:8];
    READFILE[193][0+:8] = REGFILE[193][0+:8] ^ DVALFILE[193][0+:8];
    READFILE[194][0+:8] = REGFILE[194][0+:8] ^ DVALFILE[194][0+:8];
    READFILE[320][0+:8] = REGFILE[320][0+:8] ^ DVALFILE[320][0+:8];
    READFILE[321][0+:8] = REGFILE[321][0+:8] ^ DVALFILE[321][0+:8];
    READFILE[322][0+:8] = REGFILE[322][0+:8] ^ DVALFILE[322][0+:8];
    READFILE[323][0+:8] = REGFILE[323][0+:8] ^ DVALFILE[323][0+:8];
    READFILE[324][0+:8] = REGFILE[324][0+:8] ^ DVALFILE[324][0+:8];
    READFILE[384][0+:1] = REGFILE[384][0+:1] ^ DVALFILE[384][0+:1];
    READFILE[385][0+:6] = REGFILE[385][0+:6] ^ DVALFILE[385][0+:6];
    READFILE[385][6+:10] = REGFILE[385][6+:10] ^ DVALFILE[385][6+:10];
    READFILE[386][0+:1] = REGFILE[386][0+:1] ^ DVALFILE[386][0+:1];
    READFILE[386][1+:3] = REGFILE[386][1+:3] ^ DVALFILE[386][1+:3];
    READFILE[386][4+:1] = REGFILE[386][4+:1] ^ DVALFILE[386][4+:1];
    READFILE[387][0+:1] = REGFILE[387][0+:1] ^ DVALFILE[387][0+:1];
    READFILE[387][1+:2] = REGFILE[387][1+:2] ^ DVALFILE[387][1+:2];
    READFILE[387][3+:2] = REGFILE[387][3+:2] ^ DVALFILE[387][3+:2];
    READFILE[387][5+:2] = REGFILE[387][5+:2] ^ DVALFILE[387][5+:2];
    READFILE[387][7+:2] = REGFILE[387][7+:2] ^ DVALFILE[387][7+:2];
    READFILE[388][0+:1] = REGFILE[388][0+:1] ^ DVALFILE[388][0+:1];
    READFILE[388][1+:1] = REGFILE[388][1+:1] ^ DVALFILE[388][1+:1];
    READFILE[388][2+:4] = REGFILE[388][2+:4] ^ DVALFILE[388][2+:4];
    READFILE[389][0+:1] = REGFILE[389][0+:1] ^ DVALFILE[389][0+:1];
    READFILE[389][1+:1] = REGFILE[389][1+:1] ^ DVALFILE[389][1+:1];
    READFILE[389][2+:1] = REGFILE[389][2+:1] ^ DVALFILE[389][2+:1];
    READFILE[389][3+:5] = REGFILE[389][3+:5] ^ DVALFILE[389][3+:5];
    READFILE[390][0+:2] = REGFILE[390][0+:2] ^ DVALFILE[390][0+:2];
    READFILE[391][0+:1] = REGFILE[391][0+:1] ^ DVALFILE[391][0+:1];
    READFILE[392][1+:1] = REGFILE[392][1+:1] ^ DVALFILE[392][1+:1];
    READFILE[392][2+:1] = REGFILE[392][2+:1] ^ DVALFILE[392][2+:1];
    READFILE[393][0+:1] = SCR__PLL_CPU_LOCK ;
    READFILE[394][0+:5] = SCR__PLL_CPU_AFC_CODE[4:0];
    READFILE[512][0+:1] = REGFILE[512][0+:1] ^ DVALFILE[512][0+:1];
    READFILE[512][1+:1] = REGFILE[512][1+:1] ^ DVALFILE[512][1+:1];
    READFILE[512][2+:1] = REGFILE[512][2+:1] ^ DVALFILE[512][2+:1];
    READFILE[512][3+:1] = REGFILE[512][3+:1] ^ DVALFILE[512][3+:1];
    READFILE[512][4+:1] = REGFILE[512][4+:1] ^ DVALFILE[512][4+:1];
    READFILE[512][5+:1] = REGFILE[512][5+:1] ^ DVALFILE[512][5+:1];
    READFILE[513][0+:11] = REGFILE[513][0+:11] ^ DVALFILE[513][0+:11];
    READFILE[513][12+:4] = REGFILE[513][12+:4] ^ DVALFILE[513][12+:4];
    READFILE[513][16+:16] = REGFILE[513][16+:16] ^ DVALFILE[513][16+:16];
    READFILE[514][0+:10] = REGFILE[514][0+:10] ^ DVALFILE[514][0+:10];
    READFILE[514][16+:10] = REGFILE[514][16+:10] ^ DVALFILE[514][16+:10];
    READFILE[515][0+:10] = REGFILE[515][0+:10] ^ DVALFILE[515][0+:10];
    READFILE[515][16+:10] = REGFILE[515][16+:10] ^ DVALFILE[515][16+:10];
    READFILE[576][0+:1] = SFR__DEBUG_NDRESET ;
    READFILE[576][1+:1] = SFR__DEBUG_DMACTIVE ;
    READFILE[576][2+:1] = SFR__HALT_FROM_TILE_0 ;
    READFILE[576][3+:1] = SFR__HALT_FROM_TILE_1 ;
    READFILE[576][4+:1] = SFR__WFI_FROM_TILE_0 ;
    READFILE[576][5+:1] = SFR__WFI_FROM_TILE_1 ;
    READFILE[576][6+:1] = SFR__CEASE_FROM_TILE_0 ;
    READFILE[576][7+:1] = SFR__CEASE_FROM_TILE_1 ;
    READFILE[576][8+:1] = SFR__DEBUG_FROM_TILE_0 ;
    READFILE[576][9+:1] = SFR__DEBUG_FROM_TILE_1 ;
end

assign PRDATA[DW-1:0] = READFILE[ADDR_ENTRY][DW-1:0];
assign PREADY = '1;
assign PSLVERR = '0;

endmodule

`ifdef __EN_BUILT_IN_TEST_BENCH__scr_cpu__

module tb();

    /*AUTOREGINPUT*/
    // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
    reg [11:0]          PADDR;                  // To duv of scr_cpu.v
    reg                 PCLK;                   // To duv of scr_cpu.v
    reg                 PENABLE;                // To duv of scr_cpu.v
    reg [2:0]           PPROT;                  // To duv of scr_cpu.v
    reg                 PRESETn;                // To duv of scr_cpu.v
    reg                 PSEL;                   // To duv of scr_cpu.v
    reg [3:0]           PSTRB;                  // To duv of scr_cpu.v
    reg [31:0]          PWDATA;                 // To duv of scr_cpu.v
    reg                 PWRITE;                 // To duv of scr_cpu.v
    reg [4:0]           SCR__PLL_CPU_AFC_CODE;  // To duv of scr_cpu.v
    reg                 SCR__PLL_CPU_LOCK;      // To duv of scr_cpu.v
    reg [31:0]          SCR__VERSION0;          // To duv of scr_cpu.v
    reg [31:0]          SCR__VERSION1;          // To duv of scr_cpu.v
    reg [31:0]          SCR__VERSION2;          // To duv of scr_cpu.v
    reg [31:0]          SCR__VERSION3;          // To duv of scr_cpu.v
    reg                 SFR__CEASE_FROM_TILE_0; // To duv of scr_cpu.v
    reg                 SFR__CEASE_FROM_TILE_1; // To duv of scr_cpu.v
    reg                 SFR__DEBUG_DMACTIVE;    // To duv of scr_cpu.v
    reg                 SFR__DEBUG_FROM_TILE_0; // To duv of scr_cpu.v
    reg                 SFR__DEBUG_FROM_TILE_1; // To duv of scr_cpu.v
    reg                 SFR__DEBUG_NDRESET;     // To duv of scr_cpu.v
    reg                 SFR__HALT_FROM_TILE_0;  // To duv of scr_cpu.v
    reg                 SFR__HALT_FROM_TILE_1;  // To duv of scr_cpu.v
    reg                 SFR__WFI_FROM_TILE_0;   // To duv of scr_cpu.v
    reg                 SFR__WFI_FROM_TILE_1;   // To duv of scr_cpu.v
    // End of automatics

    /*AUTOWIRE*/
    // Beginning of automatic wires (for undeclared instantiated-module outputs)
    wire [31:0]         PRDATA;                 // From duv of scr_cpu.v
    wire                PREADY;                 // From duv of scr_cpu.v
    wire                PSLVERR;                // From duv of scr_cpu.v
    wire [16:0]         SCR__CLKDIS0;           // From duv of scr_cpu.v
    wire [7:0]          SCR__CLKDIV0;           // From duv of scr_cpu.v
    wire [7:0]          SCR__CLKDIV1;           // From duv of scr_cpu.v
    wire [7:0]          SCR__CLKDIV2;           // From duv of scr_cpu.v
    wire                SCR__CLKSEL0;           // From duv of scr_cpu.v
    wire                SCR__CLKSEL1;           // From duv of scr_cpu.v
    wire                SCR__CLKSEL2;           // From duv of scr_cpu.v
    wire                SCR__PLL_CPU_AFCINIT_SEL;// From duv of scr_cpu.v
    wire                SCR__PLL_CPU_AFC_ENB;   // From duv of scr_cpu.v
    wire                SCR__PLL_CPU_BYPASS;    // From duv of scr_cpu.v
    wire [4:0]          SCR__PLL_CPU_EXTAFC;    // From duv of scr_cpu.v
    wire                SCR__PLL_CPU_FEED_EN;   // From duv of scr_cpu.v
    wire                SCR__PLL_CPU_FOUT_MASK; // From duv of scr_cpu.v
    wire                SCR__PLL_CPU_FSEL;      // From duv of scr_cpu.v
    wire [1:0]          SCR__PLL_CPU_ICP;       // From duv of scr_cpu.v
    wire [1:0]          SCR__PLL_CPU_LOCK_CON_DLY;// From duv of scr_cpu.v
    wire [1:0]          SCR__PLL_CPU_LOCK_CON_IN;// From duv of scr_cpu.v
    wire [1:0]          SCR__PLL_CPU_LOCK_CON_OUT;// From duv of scr_cpu.v
    wire [1:0]          SCR__PLL_CPU_LOCK_CON_REV;// From duv of scr_cpu.v
    wire                SCR__PLL_CPU_LOCK_EN;   // From duv of scr_cpu.v
    wire                SCR__PLL_CPU_LRD_EN;    // From duv of scr_cpu.v
    wire [9:0]          SCR__PLL_CPU_M;         // From duv of scr_cpu.v
    wire [5:0]          SCR__PLL_CPU_P;         // From duv of scr_cpu.v
    wire                SCR__PLL_CPU_PBIAS_CTRL;// From duv of scr_cpu.v
    wire                SCR__PLL_CPU_PBIAS_CTRL_EN;// From duv of scr_cpu.v
    wire                SCR__PLL_CPU_RESETB;    // From duv of scr_cpu.v
    wire [3:0]          SCR__PLL_CPU_RSEL;      // From duv of scr_cpu.v
    wire [2:0]          SCR__PLL_CPU_S;         // From duv of scr_cpu.v
    wire                SCR__PLL_CPU_VCO_BOOST; // From duv of scr_cpu.v
    wire [7:0]          SCR__RSTCYCLE0;         // From duv of scr_cpu.v
    wire [7:0]          SCR__RSTCYCLE1;         // From duv of scr_cpu.v
    wire [7:0]          SCR__RSTCYCLE2;         // From duv of scr_cpu.v
    wire [7:0]          SCR__RSTCYCLE3;         // From duv of scr_cpu.v
    wire [7:0]          SCR__RSTCYCLE4;         // From duv of scr_cpu.v
    wire [16:0]         SCR__RSTREQN0;          // From duv of scr_cpu.v
    wire                SFR__DEBUG_DMACTIVEACK; // From duv of scr_cpu.v
    wire [10:0]         SFR__DEBUG_SYSTEMJTAG_MFR_ID;// From duv of scr_cpu.v
    wire [15:0]         SFR__DEBUG_SYSTEMJTAG_PART_NUMBER;// From duv of scr_cpu.v
    wire [3:0]          SFR__DEBUG_SYSTEMJTAG_VERSION;// From duv of scr_cpu.v
    wire [9:0]          SFR__MEMPORT_ARUSER;    // From duv of scr_cpu.v
    wire [9:0]          SFR__MEMPORT_AWUSER;    // From duv of scr_cpu.v
    wire                SFR__PSD_TEST_CLOCK_ENABLE;// From duv of scr_cpu.v
    wire                SFR__RESETCTRL_HARTISINRESET_0;// From duv of scr_cpu.v
    wire                SFR__RESETCTRL_HARTISINRESET_1;// From duv of scr_cpu.v
    wire                SFR__RTC_TOGGLE;        // From duv of scr_cpu.v
    wire                SFR__SUPPRESS_STRB_ON_CORRUPT;// From duv of scr_cpu.v
    wire [9:0]          SFR__SYSPORT_ARUSER;    // From duv of scr_cpu.v
    wire [9:0]          SFR__SYSPORT_AWUSER;    // From duv of scr_cpu.v
    // End of automatics

    initial begin
        PCLK = 0;
        forever PCLK = #(1ns) ~PCLK;
    end
    initial begin
        PRESETn <= 1'b0;
        repeat (20) @(posedge PCLK);
        PRESETn <= 1'b1;
    end

    initial begin
        SCR__VERSION0[31:0] <= $random;
        SCR__VERSION1[31:0] <= $random;
        SCR__VERSION2[31:0] <= $random;
        SCR__VERSION3[31:0] <= $random;
        SCR__PLL_CPU_LOCK  <= $random;
        SCR__PLL_CPU_AFC_CODE[4:0] <= $random;
        SFR__DEBUG_NDRESET  <= $random;
        SFR__DEBUG_DMACTIVE  <= $random;
        SFR__HALT_FROM_TILE_0  <= $random;
        SFR__HALT_FROM_TILE_1  <= $random;
        SFR__WFI_FROM_TILE_0  <= $random;
        SFR__WFI_FROM_TILE_1  <= $random;
        SFR__CEASE_FROM_TILE_0  <= $random;
        SFR__CEASE_FROM_TILE_1  <= $random;
        SFR__DEBUG_FROM_TILE_0  <= $random;
        SFR__DEBUG_FROM_TILE_1  <= $random;
    end

    integer cnt;
    initial begin
        PADDR <= '0;
        PSEL <= '0;
        PENABLE <= '0;
        PPROT <= '0;
        PSTRB <= '0;
        PWDATA <= '0;
        PWRITE <= '0;
        wait (PRESETn);
        $display("Reset released!");
        @(posedge PCLK);

        repeat (20) @(posedge PCLK);
        $display("Read Default...");
        for(cnt=0;cnt<40;cnt=cnt+1) begin
            PADDR <= (cnt<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            $display("READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
        end

        repeat (20) @(posedge PCLK);
        $display("Read Ext. value...");
            PADDR <= (0<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[0+:32] !== SCR__VERSION0 ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[0+:32],SCR__VERSION0);
                $stop;
            end
            PADDR <= (1<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[0+:32] !== SCR__VERSION1 ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[0+:32],SCR__VERSION1);
                $stop;
            end
            PADDR <= (2<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[0+:32] !== SCR__VERSION2 ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[0+:32],SCR__VERSION2);
                $stop;
            end
            PADDR <= (3<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[0+:32] !== SCR__VERSION3 ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[0+:32],SCR__VERSION3);
                $stop;
            end
            PADDR <= (393<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[0+:1] !== SCR__PLL_CPU_LOCK ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[0+:1],SCR__PLL_CPU_LOCK);
                $stop;
            end
            PADDR <= (394<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[0+:5] !== SCR__PLL_CPU_AFC_CODE ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[0+:5],SCR__PLL_CPU_AFC_CODE);
                $stop;
            end
            PADDR <= (576<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[0+:1] !== SFR__DEBUG_NDRESET ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[0+:1],SFR__DEBUG_NDRESET);
                $stop;
            end
            PADDR <= (576<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[1+:1] !== SFR__DEBUG_DMACTIVE ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[1+:1],SFR__DEBUG_DMACTIVE);
                $stop;
            end
            PADDR <= (576<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[2+:1] !== SFR__HALT_FROM_TILE_0 ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[2+:1],SFR__HALT_FROM_TILE_0);
                $stop;
            end
            PADDR <= (576<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[3+:1] !== SFR__HALT_FROM_TILE_1 ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[3+:1],SFR__HALT_FROM_TILE_1);
                $stop;
            end
            PADDR <= (576<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[4+:1] !== SFR__WFI_FROM_TILE_0 ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[4+:1],SFR__WFI_FROM_TILE_0);
                $stop;
            end
            PADDR <= (576<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[5+:1] !== SFR__WFI_FROM_TILE_1 ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[5+:1],SFR__WFI_FROM_TILE_1);
                $stop;
            end
            PADDR <= (576<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[6+:1] !== SFR__CEASE_FROM_TILE_0 ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[6+:1],SFR__CEASE_FROM_TILE_0);
                $stop;
            end
            PADDR <= (576<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[7+:1] !== SFR__CEASE_FROM_TILE_1 ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[7+:1],SFR__CEASE_FROM_TILE_1);
                $stop;
            end
            PADDR <= (576<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[8+:1] !== SFR__DEBUG_FROM_TILE_0 ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[8+:1],SFR__DEBUG_FROM_TILE_0);
                $stop;
            end
            PADDR <= (576<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[9+:1] !== SFR__DEBUG_FROM_TILE_1 ) begin
                $display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                $display("     0x%08x !== 0x%08x",PRDATA[9+:1],SFR__DEBUG_FROM_TILE_1);
                $stop;
            end

        repeat (20) @(posedge PCLK);
        $display("Write sequencial data...");
        for(cnt=0;cnt<40;cnt=cnt+1) begin
            PADDR <= (cnt<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '1;
            PWDATA <= cnt;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
        end

        repeat (20) @(posedge PCLK);
        $display("Read back data...");
        for(cnt=0;cnt<40;cnt=cnt+1) begin
            PADDR <= (cnt<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            $display("READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
        end

        repeat (20) @(posedge PCLK);
        $display("Write all one...");
        for(cnt=0;cnt<40;cnt=cnt+1) begin
            PADDR <= (cnt<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '1;
            PWDATA <= '1;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
        end

        repeat (20) @(posedge PCLK);
        $display("Read back data...");
        for(cnt=0;cnt<40;cnt=cnt+1) begin
            PADDR <= (cnt<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            $display("READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
        end

        $finish;
    end

    scr_cpu #(.AW(12),.DW(32)) duv
        (/*AUTOINST*/
         // Outputs
         .PRDATA                        (PRDATA[31:0]),
         .PREADY                        (PREADY),
         .PSLVERR                       (PSLVERR),
         .SCR__RSTREQN0                 (SCR__RSTREQN0[16:0]),
         .SCR__CLKDIS0                  (SCR__CLKDIS0[16:0]),
         .SCR__CLKSEL0                  (SCR__CLKSEL0),
         .SCR__CLKSEL1                  (SCR__CLKSEL1),
         .SCR__CLKSEL2                  (SCR__CLKSEL2),
         .SCR__CLKDIV0                  (SCR__CLKDIV0[7:0]),
         .SCR__CLKDIV1                  (SCR__CLKDIV1[7:0]),
         .SCR__CLKDIV2                  (SCR__CLKDIV2[7:0]),
         .SCR__RSTCYCLE0                (SCR__RSTCYCLE0[7:0]),
         .SCR__RSTCYCLE1                (SCR__RSTCYCLE1[7:0]),
         .SCR__RSTCYCLE2                (SCR__RSTCYCLE2[7:0]),
         .SCR__RSTCYCLE3                (SCR__RSTCYCLE3[7:0]),
         .SCR__RSTCYCLE4                (SCR__RSTCYCLE4[7:0]),
         .SCR__PLL_CPU_RESETB           (SCR__PLL_CPU_RESETB),
         .SCR__PLL_CPU_P                (SCR__PLL_CPU_P[5:0]),
         .SCR__PLL_CPU_M                (SCR__PLL_CPU_M[9:0]),
         .SCR__PLL_CPU_BYPASS           (SCR__PLL_CPU_BYPASS),
         .SCR__PLL_CPU_S                (SCR__PLL_CPU_S[2:0]),
         .SCR__PLL_CPU_FOUT_MASK        (SCR__PLL_CPU_FOUT_MASK),
         .SCR__PLL_CPU_LOCK_EN          (SCR__PLL_CPU_LOCK_EN),
         .SCR__PLL_CPU_LOCK_CON_IN      (SCR__PLL_CPU_LOCK_CON_IN[1:0]),
         .SCR__PLL_CPU_LOCK_CON_OUT     (SCR__PLL_CPU_LOCK_CON_OUT[1:0]),
         .SCR__PLL_CPU_LOCK_CON_DLY     (SCR__PLL_CPU_LOCK_CON_DLY[1:0]),
         .SCR__PLL_CPU_LOCK_CON_REV     (SCR__PLL_CPU_LOCK_CON_REV[1:0]),
         .SCR__PLL_CPU_FEED_EN          (SCR__PLL_CPU_FEED_EN),
         .SCR__PLL_CPU_FSEL             (SCR__PLL_CPU_FSEL),
         .SCR__PLL_CPU_RSEL             (SCR__PLL_CPU_RSEL[3:0]),
         .SCR__PLL_CPU_AFC_ENB          (SCR__PLL_CPU_AFC_ENB),
         .SCR__PLL_CPU_AFCINIT_SEL      (SCR__PLL_CPU_AFCINIT_SEL),
         .SCR__PLL_CPU_LRD_EN           (SCR__PLL_CPU_LRD_EN),
         .SCR__PLL_CPU_EXTAFC           (SCR__PLL_CPU_EXTAFC[4:0]),
         .SCR__PLL_CPU_ICP              (SCR__PLL_CPU_ICP[1:0]),
         .SCR__PLL_CPU_PBIAS_CTRL       (SCR__PLL_CPU_PBIAS_CTRL),
         .SCR__PLL_CPU_PBIAS_CTRL_EN    (SCR__PLL_CPU_PBIAS_CTRL_EN),
         .SCR__PLL_CPU_VCO_BOOST        (SCR__PLL_CPU_VCO_BOOST),
         .SFR__RTC_TOGGLE               (SFR__RTC_TOGGLE),
         .SFR__PSD_TEST_CLOCK_ENABLE    (SFR__PSD_TEST_CLOCK_ENABLE),
         .SFR__SUPPRESS_STRB_ON_CORRUPT (SFR__SUPPRESS_STRB_ON_CORRUPT),
         .SFR__RESETCTRL_HARTISINRESET_0(SFR__RESETCTRL_HARTISINRESET_0),
         .SFR__RESETCTRL_HARTISINRESET_1(SFR__RESETCTRL_HARTISINRESET_1),
         .SFR__DEBUG_DMACTIVEACK        (SFR__DEBUG_DMACTIVEACK),
         .SFR__DEBUG_SYSTEMJTAG_MFR_ID  (SFR__DEBUG_SYSTEMJTAG_MFR_ID[10:0]),
         .SFR__DEBUG_SYSTEMJTAG_VERSION (SFR__DEBUG_SYSTEMJTAG_VERSION[3:0]),
         .SFR__DEBUG_SYSTEMJTAG_PART_NUMBER(SFR__DEBUG_SYSTEMJTAG_PART_NUMBER[15:0]),
         .SFR__SYSPORT_AWUSER           (SFR__SYSPORT_AWUSER[9:0]),
         .SFR__SYSPORT_ARUSER           (SFR__SYSPORT_ARUSER[9:0]),
         .SFR__MEMPORT_AWUSER           (SFR__MEMPORT_AWUSER[9:0]),
         .SFR__MEMPORT_ARUSER           (SFR__MEMPORT_ARUSER[9:0]),
         // Inputs
         .PCLK                          (PCLK),
         .PRESETn                       (PRESETn),
         .PADDR                         (PADDR[11:0]),
         .PENABLE                       (PENABLE),
         .PSEL                          (PSEL),
         .PSTRB                         (PSTRB[3:0]),
         .PPROT                         (PPROT[02:00]),
         .PWDATA                        (PWDATA[31:0]),
         .PWRITE                        (PWRITE),
         .SCR__VERSION0                 (SCR__VERSION0[31:0]),
         .SCR__VERSION1                 (SCR__VERSION1[31:0]),
         .SCR__VERSION2                 (SCR__VERSION2[31:0]),
         .SCR__VERSION3                 (SCR__VERSION3[31:0]),
         .SCR__PLL_CPU_LOCK             (SCR__PLL_CPU_LOCK),
         .SCR__PLL_CPU_AFC_CODE         (SCR__PLL_CPU_AFC_CODE[4:0]),
         .SFR__DEBUG_NDRESET            (SFR__DEBUG_NDRESET),
         .SFR__DEBUG_DMACTIVE           (SFR__DEBUG_DMACTIVE),
         .SFR__HALT_FROM_TILE_0         (SFR__HALT_FROM_TILE_0),
         .SFR__HALT_FROM_TILE_1         (SFR__HALT_FROM_TILE_1),
         .SFR__WFI_FROM_TILE_0          (SFR__WFI_FROM_TILE_0),
         .SFR__WFI_FROM_TILE_1          (SFR__WFI_FROM_TILE_1),
         .SFR__CEASE_FROM_TILE_0        (SFR__CEASE_FROM_TILE_0),
         .SFR__CEASE_FROM_TILE_1        (SFR__CEASE_FROM_TILE_1),
         .SFR__DEBUG_FROM_TILE_0        (SFR__DEBUG_FROM_TILE_0),
         .SFR__DEBUG_FROM_TILE_1        (SFR__DEBUG_FROM_TILE_1));

    initial begin
        $fsdbDumpfile("dump.fsdb");
        $fsdbDumpvars(tb,0);
    end

endmodule

`endif // `ifndef __EN_BUILT_IN_TEST_BENCH__scr_cpu__

/*
Local Variables:
verilog-library-extensions:(".v")
verilog-library-directories:("./")
indent-tabs-mode:nil
verilog-auto-inst-param-value:t
verilog-auto-inst-sort:nil
verilog-auto-inst-template-numbers:t
tab-width:4
End:
*/
// vim:ts=4:et:sw=4:sts=4
