//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLXbar_3(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_2_a_ready,
  input         auto_in_2_a_valid,
  input  [1:0]  auto_in_2_a_bits_source,
  input  [36:0] auto_in_2_a_bits_address,
  input  [7:0]  auto_in_2_a_bits_mask,
  input  [63:0] auto_in_2_a_bits_data,
  input         auto_in_2_d_ready,
  output        auto_in_2_d_valid,
  output [2:0]  auto_in_2_d_bits_opcode,
  output [1:0]  auto_in_2_d_bits_param,
  output [3:0]  auto_in_2_d_bits_size,
  output [1:0]  auto_in_2_d_bits_source,
  output [4:0]  auto_in_2_d_bits_sink,
  output        auto_in_2_d_bits_denied,
  output        auto_in_2_d_bits_corrupt,
  output        auto_in_1_a_ready,
  input         auto_in_1_a_valid,
  input  [2:0]  auto_in_1_a_bits_opcode,
  input  [3:0]  auto_in_1_a_bits_size,
  input  [4:0]  auto_in_1_a_bits_source,
  input  [36:0] auto_in_1_a_bits_address,
  input         auto_in_1_a_bits_user_amba_prot_bufferable,
  input         auto_in_1_a_bits_user_amba_prot_modifiable,
  input         auto_in_1_a_bits_user_amba_prot_readalloc,
  input         auto_in_1_a_bits_user_amba_prot_writealloc,
  input         auto_in_1_a_bits_user_amba_prot_privileged,
  input         auto_in_1_a_bits_user_amba_prot_secure,
  input         auto_in_1_a_bits_user_amba_prot_fetch,
  input  [7:0]  auto_in_1_a_bits_mask,
  input  [63:0] auto_in_1_a_bits_data,
  input         auto_in_1_d_ready,
  output        auto_in_1_d_valid,
  output [2:0]  auto_in_1_d_bits_opcode,
  output [1:0]  auto_in_1_d_bits_param,
  output [3:0]  auto_in_1_d_bits_size,
  output [4:0]  auto_in_1_d_bits_source,
  output [4:0]  auto_in_1_d_bits_sink,
  output        auto_in_1_d_bits_denied,
  output [63:0] auto_in_1_d_bits_data,
  output        auto_in_1_d_bits_corrupt,
  output        auto_in_0_a_ready,
  input         auto_in_0_a_valid,
  input  [2:0]  auto_in_0_a_bits_opcode,
  input  [3:0]  auto_in_0_a_bits_size,
  input  [36:0] auto_in_0_a_bits_address,
  input  [7:0]  auto_in_0_a_bits_mask,
  input  [63:0] auto_in_0_a_bits_data,
  input         auto_in_0_d_ready,
  output        auto_in_0_d_valid,
  output [2:0]  auto_in_0_d_bits_opcode,
  output [1:0]  auto_in_0_d_bits_param,
  output [3:0]  auto_in_0_d_bits_size,
  output [4:0]  auto_in_0_d_bits_sink,
  output        auto_in_0_d_bits_denied,
  output [63:0] auto_in_0_d_bits_data,
  output        auto_in_0_d_bits_corrupt,
  input         auto_out_a_ready,
  output        auto_out_a_valid,
  output [2:0]  auto_out_a_bits_opcode,
  output [3:0]  auto_out_a_bits_size,
  output [5:0]  auto_out_a_bits_source,
  output [36:0] auto_out_a_bits_address,
  output        auto_out_a_bits_user_amba_prot_bufferable,
  output        auto_out_a_bits_user_amba_prot_modifiable,
  output        auto_out_a_bits_user_amba_prot_readalloc,
  output        auto_out_a_bits_user_amba_prot_writealloc,
  output        auto_out_a_bits_user_amba_prot_privileged,
  output        auto_out_a_bits_user_amba_prot_secure,
  output        auto_out_a_bits_user_amba_prot_fetch,
  output [7:0]  auto_out_a_bits_mask,
  output [63:0] auto_out_a_bits_data,
  output        auto_out_d_ready,
  input         auto_out_d_valid,
  input  [2:0]  auto_out_d_bits_opcode,
  input  [1:0]  auto_out_d_bits_param,
  input  [3:0]  auto_out_d_bits_size,
  input  [5:0]  auto_out_d_bits_source,
  input  [4:0]  auto_out_d_bits_sink,
  input         auto_out_d_bits_denied,
  input  [63:0] auto_out_d_bits_data,
  input         auto_out_d_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
`endif // RANDOMIZE_REG_INIT
  wire [5:0] _GEN_1 = {{4'd0}, auto_in_2_a_bits_source}; // @[Xbar.scala 237:55]
  wire [5:0] in_2_a_bits_source = _GEN_1 | 6'h20; // @[Xbar.scala 237:55]
  wire  requestDOI_0_0 = auto_out_d_bits_source == 6'h24; // @[Parameters.scala 46:9]
  wire  requestDOI_0_1 = ~auto_out_d_bits_source[5]; // @[Parameters.scala 54:32]
  wire  requestDOI_0_2 = auto_out_d_bits_source[5:2] == 4'h8; // @[Parameters.scala 54:32]
  wire [22:0] _beatsAI_decode_T_1 = 23'hff << auto_in_0_a_bits_size; // @[package.scala 234:77]
  wire [7:0] _beatsAI_decode_T_3 = ~_beatsAI_decode_T_1[7:0]; // @[package.scala 234:46]
  wire [4:0] beatsAI_decode = _beatsAI_decode_T_3[7:3]; // @[Edges.scala 219:59]
  wire  beatsAI_opdata = ~auto_in_0_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [4:0] beatsAI_0 = beatsAI_opdata ? beatsAI_decode : 5'h0; // @[Edges.scala 220:14]
  wire [22:0] _beatsAI_decode_T_5 = 23'hff << auto_in_1_a_bits_size; // @[package.scala 234:77]
  wire [7:0] _beatsAI_decode_T_7 = ~_beatsAI_decode_T_5[7:0]; // @[package.scala 234:46]
  wire [4:0] beatsAI_decode_1 = _beatsAI_decode_T_7[7:3]; // @[Edges.scala 219:59]
  wire  beatsAI_opdata_1 = ~auto_in_1_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [4:0] beatsAI_1 = beatsAI_opdata_1 ? beatsAI_decode_1 : 5'h0; // @[Edges.scala 220:14]
  reg [4:0] beatsLeft; // @[Arbiter.scala 87:30]
  wire  idle = beatsLeft == 5'h0; // @[Arbiter.scala 88:28]
  wire  latch = idle & auto_out_a_ready; // @[Arbiter.scala 89:24]
  wire [2:0] readys_filter_lo = {auto_in_2_a_valid,auto_in_1_a_valid,auto_in_0_a_valid}; // @[Cat.scala 30:58]
  reg [2:0] readys_mask; // @[Arbiter.scala 23:23]
  wire [2:0] _readys_filter_T = ~readys_mask; // @[Arbiter.scala 24:30]
  wire [2:0] readys_filter_hi = readys_filter_lo & _readys_filter_T; // @[Arbiter.scala 24:28]
  wire [5:0] readys_filter = {readys_filter_hi,auto_in_2_a_valid,auto_in_1_a_valid,auto_in_0_a_valid}; // @[Cat.scala 30:58]
  wire [5:0] _GEN_2 = {{1'd0}, readys_filter[5:1]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_1 = readys_filter | _GEN_2; // @[package.scala 253:43]
  wire [5:0] _GEN_3 = {{2'd0}, _readys_unready_T_1[5:2]}; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_3 = _readys_unready_T_1 | _GEN_3; // @[package.scala 253:43]
  wire [5:0] _readys_unready_T_6 = {readys_mask, 3'h0}; // @[Arbiter.scala 25:66]
  wire [5:0] _GEN_4 = {{1'd0}, _readys_unready_T_3[5:1]}; // @[Arbiter.scala 25:58]
  wire [5:0] readys_unready = _GEN_4 | _readys_unready_T_6; // @[Arbiter.scala 25:58]
  wire [2:0] _readys_readys_T_2 = readys_unready[5:3] & readys_unready[2:0]; // @[Arbiter.scala 26:39]
  wire [2:0] readys_readys = ~_readys_readys_T_2; // @[Arbiter.scala 26:18]
  wire [2:0] _readys_mask_T = readys_readys & readys_filter_lo; // @[Arbiter.scala 28:29]
  wire [3:0] _readys_mask_T_1 = {_readys_mask_T, 1'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_3 = _readys_mask_T | _readys_mask_T_1[2:0]; // @[package.scala 244:43]
  wire [4:0] _readys_mask_T_4 = {_readys_mask_T_3, 2'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_mask_T_6 = _readys_mask_T_3 | _readys_mask_T_4[2:0]; // @[package.scala 244:43]
  wire  readys_0 = readys_readys[0]; // @[Arbiter.scala 95:86]
  wire  readys_1 = readys_readys[1]; // @[Arbiter.scala 95:86]
  wire  readys_2 = readys_readys[2]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_0 = readys_0 & auto_in_0_a_valid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_1 = readys_1 & auto_in_1_a_valid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_2 = readys_2 & auto_in_2_a_valid; // @[Arbiter.scala 97:79]
  wire  _T_15 = auto_in_0_a_valid | auto_in_1_a_valid | auto_in_2_a_valid; // @[Arbiter.scala 107:36]
  wire [4:0] maskedBeats_0 = earlyWinner_0 ? beatsAI_0 : 5'h0; // @[Arbiter.scala 111:73]
  wire [4:0] maskedBeats_1 = earlyWinner_1 ? beatsAI_1 : 5'h0; // @[Arbiter.scala 111:73]
  wire [4:0] initBeats = maskedBeats_0 | maskedBeats_1; // @[Arbiter.scala 112:44]
  reg  state_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_0 = idle ? earlyWinner_0 : state_0; // @[Arbiter.scala 117:30]
  reg  state_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_1 = idle ? earlyWinner_1 : state_1; // @[Arbiter.scala 117:30]
  reg  state_2; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_2 = idle ? earlyWinner_2 : state_2; // @[Arbiter.scala 117:30]
  wire  _out_0_a_earlyValid_T_6 = state_0 & auto_in_0_a_valid | state_1 & auto_in_1_a_valid | state_2 &
    auto_in_2_a_valid; // @[Mux.scala 27:72]
  wire  out_3_0_a_earlyValid = idle ? _T_15 : _out_0_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_2 = auto_out_a_ready & out_3_0_a_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [4:0] _GEN_5 = {{4'd0}, _beatsLeft_T_2}; // @[Arbiter.scala 113:52]
  wire [4:0] _beatsLeft_T_4 = beatsLeft - _GEN_5; // @[Arbiter.scala 113:52]
  wire  allowed_0 = idle ? readys_0 : state_0; // @[Arbiter.scala 121:24]
  wire  allowed_1 = idle ? readys_1 : state_1; // @[Arbiter.scala 121:24]
  wire  allowed_2 = idle ? readys_2 : state_2; // @[Arbiter.scala 121:24]
  wire [63:0] _T_37 = muxStateEarly_0 ? auto_in_0_a_bits_data : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _T_38 = muxStateEarly_1 ? auto_in_1_a_bits_data : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _T_39 = muxStateEarly_2 ? auto_in_2_a_bits_data : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _T_40 = _T_37 | _T_38; // @[Mux.scala 27:72]
  wire [7:0] _T_42 = muxStateEarly_0 ? auto_in_0_a_bits_mask : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _T_43 = muxStateEarly_1 ? auto_in_1_a_bits_mask : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _T_44 = muxStateEarly_2 ? auto_in_2_a_bits_mask : 8'h0; // @[Mux.scala 27:72]
  wire [7:0] _T_45 = _T_42 | _T_43; // @[Mux.scala 27:72]
  wire [36:0] _T_82 = muxStateEarly_0 ? auto_in_0_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_83 = muxStateEarly_1 ? auto_in_1_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_84 = muxStateEarly_2 ? auto_in_2_a_bits_address : 37'h0; // @[Mux.scala 27:72]
  wire [36:0] _T_85 = _T_82 | _T_83; // @[Mux.scala 27:72]
  wire [5:0] _T_87 = muxStateEarly_0 ? 6'h24 : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] in_1_a_bits_source = {{1'd0}, auto_in_1_a_bits_source}; // @[Xbar.scala 231:18 Xbar.scala 237:29]
  wire [5:0] _T_88 = muxStateEarly_1 ? in_1_a_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _T_89 = muxStateEarly_2 ? in_2_a_bits_source : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _T_90 = _T_87 | _T_88; // @[Mux.scala 27:72]
  wire [3:0] _T_92 = muxStateEarly_0 ? auto_in_0_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_93 = muxStateEarly_1 ? auto_in_1_a_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_94 = muxStateEarly_2 ? 4'h2 : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_95 = _T_92 | _T_93; // @[Mux.scala 27:72]
  wire [2:0] _T_102 = muxStateEarly_0 ? auto_in_0_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_103 = muxStateEarly_1 ? auto_in_1_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  assign auto_in_2_a_ready = auto_out_a_ready & allowed_2; // @[Arbiter.scala 123:31]
  assign auto_in_2_d_valid = auto_out_d_valid & requestDOI_0_2; // @[Xbar.scala 179:40]
  assign auto_in_2_d_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_param = auto_out_d_bits_param; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_size = auto_out_d_bits_size; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_source = auto_out_d_bits_source[1:0]; // @[Xbar.scala 228:69]
  assign auto_in_2_d_bits_sink = auto_out_d_bits_sink; // @[Xbar.scala 323:53]
  assign auto_in_2_d_bits_denied = auto_out_d_bits_denied; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_2_d_bits_corrupt = auto_out_d_bits_corrupt; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_1_a_ready = auto_out_a_ready & allowed_1; // @[Arbiter.scala 123:31]
  assign auto_in_1_d_valid = auto_out_d_valid & requestDOI_0_1; // @[Xbar.scala 179:40]
  assign auto_in_1_d_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_param = auto_out_d_bits_param; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_size = auto_out_d_bits_size; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_source = auto_out_d_bits_source[4:0]; // @[Xbar.scala 228:69]
  assign auto_in_1_d_bits_sink = auto_out_d_bits_sink; // @[Xbar.scala 323:53]
  assign auto_in_1_d_bits_denied = auto_out_d_bits_denied; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_data = auto_out_d_bits_data; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_1_d_bits_corrupt = auto_out_d_bits_corrupt; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_0_a_ready = auto_out_a_ready & allowed_0; // @[Arbiter.scala 123:31]
  assign auto_in_0_d_valid = auto_out_d_valid & requestDOI_0_0; // @[Xbar.scala 179:40]
  assign auto_in_0_d_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_param = auto_out_d_bits_param; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_size = auto_out_d_bits_size; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_sink = auto_out_d_bits_sink; // @[Xbar.scala 323:53]
  assign auto_in_0_d_bits_denied = auto_out_d_bits_denied; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_data = auto_out_d_bits_data; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_in_0_d_bits_corrupt = auto_out_d_bits_corrupt; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign auto_out_a_valid = idle ? _T_15 : _out_0_a_earlyValid_T_6; // @[Arbiter.scala 125:29]
  assign auto_out_a_bits_opcode = _T_102 | _T_103; // @[Mux.scala 27:72]
  assign auto_out_a_bits_size = _T_95 | _T_94; // @[Mux.scala 27:72]
  assign auto_out_a_bits_source = _T_90 | _T_89; // @[Mux.scala 27:72]
  assign auto_out_a_bits_address = _T_85 | _T_84; // @[Mux.scala 27:72]
  assign auto_out_a_bits_user_amba_prot_bufferable = muxStateEarly_1 & auto_in_1_a_bits_user_amba_prot_bufferable; // @[Mux.scala 27:72]
  assign auto_out_a_bits_user_amba_prot_modifiable = muxStateEarly_1 & auto_in_1_a_bits_user_amba_prot_modifiable; // @[Mux.scala 27:72]
  assign auto_out_a_bits_user_amba_prot_readalloc = muxStateEarly_1 & auto_in_1_a_bits_user_amba_prot_readalloc; // @[Mux.scala 27:72]
  assign auto_out_a_bits_user_amba_prot_writealloc = muxStateEarly_1 & auto_in_1_a_bits_user_amba_prot_writealloc; // @[Mux.scala 27:72]
  assign auto_out_a_bits_user_amba_prot_privileged = muxStateEarly_0 | muxStateEarly_1 &
    auto_in_1_a_bits_user_amba_prot_privileged | muxStateEarly_2; // @[Mux.scala 27:72]
  assign auto_out_a_bits_user_amba_prot_secure = muxStateEarly_0 | muxStateEarly_1 &
    auto_in_1_a_bits_user_amba_prot_secure | muxStateEarly_2; // @[Mux.scala 27:72]
  assign auto_out_a_bits_user_amba_prot_fetch = muxStateEarly_1 & auto_in_1_a_bits_user_amba_prot_fetch; // @[Mux.scala 27:72]
  assign auto_out_a_bits_mask = _T_45 | _T_44; // @[Mux.scala 27:72]
  assign auto_out_a_bits_data = _T_40 | _T_39; // @[Mux.scala 27:72]
  assign auto_out_d_ready = requestDOI_0_0 & auto_in_0_d_ready | requestDOI_0_1 & auto_in_1_d_ready | requestDOI_0_2 &
    auto_in_2_d_ready; // @[Mux.scala 27:72]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft <= 5'h0;
    end else if (latch) begin
      beatsLeft <= initBeats;
    end else begin
      beatsLeft <= _beatsLeft_T_4;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask <= 3'h7;
    end else if (latch & |readys_filter_lo) begin
      readys_mask <= _readys_mask_T_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_0 <= 1'h0;
    end else if (idle) begin
      state_0 <= earlyWinner_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_1 <= 1'h0;
    end else if (idle) begin
      state_1 <= earlyWinner_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_2 <= 1'h0;
    end else if (idle) begin
      state_2 <= earlyWinner_2;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  beatsLeft = _RAND_0[4:0];
  _RAND_1 = {1{`RANDOM}};
  readys_mask = _RAND_1[2:0];
  _RAND_2 = {1{`RANDOM}};
  state_0 = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  state_1 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  state_2 = _RAND_4[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    beatsLeft = 5'h0;
  end
  if (reset) begin
    readys_mask = 3'h7;
  end
  if (reset) begin
    state_0 = 1'h0;
  end
  if (reset) begin
    state_1 = 1'h0;
  end
  if (reset) begin
    state_2 = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
