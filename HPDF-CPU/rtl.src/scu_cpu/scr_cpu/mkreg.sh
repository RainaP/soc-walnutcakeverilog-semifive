#!/bin/bash

if [ "${#}" != "1" ];then
    echo "Error! Please specify csv file!"
    exit 1
fi

if [ ! -e "${1}" ];then
    echo "Error! ${1} not found!"
    echo "Generate template file... ${1}"
    echo "#Address,LSB,Width,Name,Default Value" > ${1}
    exit 2
fi

##  wire declare to csv
# cat 1 | sed -e "s/^wire........//" | sed -e "s/=//g" | cut -d\' -f 1 | awk '{ print $2 "," $1 }'

REGS=`cat ${1} | sed -e "s/#.*//" -e "s/ //g" -e "s/\t//g"`

VFILE="${1%.*}.v"

echo "Generate ${VFILE}..."

cat > ${VFILE} << EOI
module ${1%.*} (/*AUTOARG*/);

parameter DW=32;
parameter AW=12;

localparam NUM_ENTRY = 1<<(AW-\$clog2(DW/8));

input                   PCLK;
input                   PRESETn;

input   [AW-1:0]        PADDR;
input                   PENABLE;
input                   PSEL;
input   [(DW/8)-1:0]    PSTRB;
input   [02:00]         PPROT;
input   [DW-1:0]        PWDATA;
input                   PWRITE;
output  [DW-1:0]        PRDATA;
output                  PREADY;
output                  PSLVERR;

EOI
echo "//========================================" >> ${VFILE}
for REG in ${REGS};do
    ADDR=`echo ${REG} | cut -d, -f 1`
    LSB=`echo ${REG} | cut -d, -f 2`
    WIDTH=`echo ${REG} | cut -d, -f 3`
    NAME=`echo ${REG} | cut -d, -f 4`
    DVALUE=`echo ${REG} | cut -d, -f 5`
    if [ "${WIDTH}" == "1" ];then
        WIDTH_VEC=" "
    else
        WIDTH_VEC="[$((WIDTH-1)):0]"
    fi
    if [ "${DVALUE}" == "EXT" ];then
        printf "input  %-8s %-8s;\n" "${WIDTH_VEC}" "${NAME}" >> ${VFILE}
    else
        printf "output %-8s %-8s;\n" "${WIDTH_VEC}" "${NAME}" >> ${VFILE}
    fi
done
cat >> ${VFILE} << EOI

/*AUTOINPUT*/
/*AUTOOUTPUT*/
/*AUTOINOUT*/

reg     [DW-1:0]        REGFILE[NUM_ENTRY-1:0];
reg     [DW-1:0]        READFILE[NUM_ENTRY-1:0];
reg     [DW-1:0]        DVALFILE[NUM_ENTRY-1:0];

/*AUTOWIRE*/
/*AUTOREG*/

/*AUTOTIEOFF*/

EOI
echo "//========================================" >> ${VFILE}
echo "/* Input descriptor" >> ${VFILE}
cat ${1}  >> ${VFILE}
echo "*/" >> ${VFILE}
echo "" >> ${VFILE}
echo "//========================================" >> ${VFILE}
for REG in ${REGS[@]};do
    ADDR=`echo ${REG} | cut -d, -f 1`
    LSB=`echo ${REG} | cut -d, -f 2`
    WIDTH=`echo ${REG} | cut -d, -f 3`
    NAME=`echo ${REG} | cut -d, -f 4`
    DVALUE=`echo ${REG} | cut -d, -f 5`
    if [ "${WIDTH}" == "1" ];then
        WIDTH_VEC=" "
    else
        WIDTH_VEC="[$((WIDTH-1)):0]"
    fi
    if [ "${DVALUE}" == "EXT" ];then
        echo "// placeholder ${NAME}" >> ${VFILE}
    else
        echo "assign ${NAME}${WIDTH_VEC} = READFILE[${ADDR}][${LSB}+:${WIDTH}];" >> ${VFILE}
    fi
done
cat >> ${VFILE} << EOI

wire    [\$clog2(NUM_ENTRY)-1:0] ADDR_ENTRY;
assign  ADDR_ENTRY = PADDR[AW-1:\$clog2(DW/8)];

integer i;
always @(posedge PCLK or negedge PRESETn) begin
    if ( !PRESETn ) begin
        for(i=0;i<NUM_ENTRY;i=i+1) 
            DVALFILE[i] = {DW{1'b0}};
EOI
echo "//========================================" >> ${VFILE}
for REG in ${REGS[@]};do
    ADDR=`echo ${REG} | cut -d, -f 1`
    LSB=`echo ${REG} | cut -d, -f 2`
    WIDTH=`echo ${REG} | cut -d, -f 3`
    NAME=`echo ${REG} | cut -d, -f 4`
    DVALUE=`echo ${REG} | cut -d, -f 5`
    if [ "${DVALUE}" == "EXT" ];then
        echo "        // placeholder ${NAME} // ${REG}" >> ${VFILE}
    else
        echo "        DVALFILE[${ADDR}][${LSB}+:${WIDTH}] = ${DVALUE}; // ${REG}" >> ${VFILE}
    fi
done
cat >> ${VFILE} << EOI
    end
end

integer cnt;
always @(posedge PCLK or negedge PRESETn) begin
    if ( !PRESETn ) begin
        for (cnt=0;cnt<NUM_ENTRY;cnt=cnt+1)
            REGFILE[cnt][DW-1:0] <= '0;
        /*AUTORESET*/
    end
    else begin
        if ( PSEL & PENABLE & PWRITE ) begin
            for (cnt=0;cnt<(DW/8);cnt=cnt+1) begin
                if ( PSTRB[cnt] ) begin
                    REGFILE[ADDR_ENTRY][8*cnt+:8] <= PWDATA[8*cnt+:8] ^ DVALFILE[ADDR_ENTRY][8*cnt+:8];
                end
            end
        end
    end
end

always @(*) begin
    for(i=0;i<NUM_ENTRY;i=i+1) 
        READFILE[i] = {DW{1'b0}};
EOI
for REG in ${REGS[@]};do
    ADDR=`echo ${REG} | cut -d, -f 1`
    LSB=`echo ${REG} | cut -d, -f 2`
    WIDTH=`echo ${REG} | cut -d, -f 3`
    NAME=`echo ${REG} | cut -d, -f 4`
    DVALUE=`echo ${REG} | cut -d, -f 5`
    if [ "${WIDTH}" == "1" ];then
        WIDTH_VEC=" "
    else
        WIDTH_VEC="[$((WIDTH-1)):0]"
    fi
    if [ "${DVALUE}" == "EXT" ];then
        echo "    READFILE[${ADDR}][${LSB}+:${WIDTH}] = ${NAME}${WIDTH_VEC};" >> ${VFILE}
    else
        echo "    READFILE[${ADDR}][${LSB}+:${WIDTH}] = REGFILE[${ADDR}][${LSB}+:${WIDTH}] ^ DVALFILE[${ADDR}][${LSB}+:${WIDTH}];" >> ${VFILE}
    fi
done
cat >> ${VFILE} << EOI
end

assign PRDATA[DW-1:0] = READFILE[ADDR_ENTRY][DW-1:0];
assign PREADY = '1;
assign PSLVERR = '0;

endmodule
EOI

######################################################################
# TB..
######################################################################

#__EN_BUILT_IN_TEST_BENCH__${1%.*}__
#TB_DEF=`echo "__EN_BUILT_IN_TEST_BENCH__${1%.*}__" | tr [:lower:] [:upper:]`
TB_DEF="__EN_BUILT_IN_TEST_BENCH__${1%.*}__"

cat >> ${VFILE} << EOI

\`ifdef ${TB_DEF}

module tb();

    /*AUTOREGINPUT*/

    /*AUTOWIRE*/

    initial begin
        PCLK = 0;
        forever PCLK = #(1ns) ~PCLK;
    end
    initial begin
        PRESETn <= 1'b0;
        repeat (20) @(posedge PCLK);
        PRESETn <= 1'b1;
    end

    initial begin
EOI
for REG in ${REGS[@]};do
    ADDR=`echo ${REG} | cut -d, -f 1`
    LSB=`echo ${REG} | cut -d, -f 2`
    WIDTH=`echo ${REG} | cut -d, -f 3`
    NAME=`echo ${REG} | cut -d, -f 4`
    DVALUE=`echo ${REG} | cut -d, -f 5`
    if [ "${WIDTH}" == "1" ];then
        WIDTH_VEC=" "
    else
        WIDTH_VEC="[$((WIDTH-1)):0]"
    fi
    if [ "${DVALUE}" == "EXT" ];then
        echo "        ${NAME}${WIDTH_VEC} <= \$random;" >> ${VFILE}
    fi
done

cat >> ${VFILE} << EOI
    end

    integer cnt;
    initial begin
        PADDR <= '0;
        PSEL <= '0;
        PENABLE <= '0;
        PPROT <= '0;
        PSTRB <= '0;
        PWDATA <= '0;
        PWRITE <= '0;
        wait (PRESETn);
        \$display("Reset released!");
        @(posedge PCLK);

        repeat (20) @(posedge PCLK);
        \$display("Read Default...");
        for(cnt=0;cnt<40;cnt=cnt+1) begin
            PADDR <= (cnt<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            \$display("READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
        end

        repeat (20) @(posedge PCLK);
        \$display("Read Ext. value...");
EOI
for REG in ${REGS[@]};do
    ADDR=`echo ${REG} | cut -d, -f 1`
    LSB=`echo ${REG} | cut -d, -f 2`
    WIDTH=`echo ${REG} | cut -d, -f 3`
    NAME=`echo ${REG} | cut -d, -f 4`
    DVALUE=`echo ${REG} | cut -d, -f 5`
    if [ "${DVALUE}" == "EXT" ];then
        cat >> ${VFILE} << EOI
            PADDR <= (${ADDR}<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            if ( PRDATA[${LSB}+:${WIDTH}] !== ${NAME} ) begin
                \$display("FAIL!! READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
                \$display("     0x%08x !== 0x%08x",PRDATA[${LSB}+:${WIDTH}],${NAME});
                \$stop;
            end
EOI
    fi
done

cat >> ${VFILE} << EOI

        repeat (20) @(posedge PCLK);
        \$display("Write sequencial data...");
        for(cnt=0;cnt<40;cnt=cnt+1) begin
            PADDR <= (cnt<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '1;
            PWDATA <= cnt;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
        end

        repeat (20) @(posedge PCLK);
        \$display("Read back data...");
        for(cnt=0;cnt<40;cnt=cnt+1) begin
            PADDR <= (cnt<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            \$display("READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
        end

        repeat (20) @(posedge PCLK);
        \$display("Write all one...");
        for(cnt=0;cnt<40;cnt=cnt+1) begin
            PADDR <= (cnt<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '1;
            PWDATA <= '1;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
        end

        repeat (20) @(posedge PCLK);
        \$display("Read back data...");
        for(cnt=0;cnt<40;cnt=cnt+1) begin
            PADDR <= (cnt<<2);
            PSEL <= '1;
            PENABLE <= '0;
            PPROT <= '0;
            PSTRB <= '1;
            PWRITE <= '0;
            @(posedge PCLK);
            PENABLE <= '1;
            @(posedge PCLK);
            PSEL <= '0;
            PENABLE <= '0;
            @(posedge PCLK);
            #(0.1ns);
            \$display("READ.reg(0x%08x) => 0x%08x",PADDR,PRDATA);
        end

        \$finish;
    end

    ${1%.*} #(.AW(12),.DW(32)) duv
        (/*AUTOINST*/);

    initial begin
        \$fsdbDumpfile("dump.fsdb");
        \$fsdbDumpvars(tb,0);
    end

endmodule

\`endif // \`ifndef ${TB_DEF}

/*
Local Variables:
verilog-library-extensions:(".v")
verilog-library-directories:("./")
indent-tabs-mode:nil
verilog-auto-inst-param-value:t
verilog-auto-inst-sort:nil
verilog-auto-inst-template-numbers:t
tab-width:4
End:
*/
// vim:ts=4:et:sw=4:sts=4
EOI

emacs \
    --batch \
    -l ~/elisp/verilog-mode.elc \
    -l ~/elisp/verilog-default.el \
    ${VFILE} \
    -f verilog-batch-auto
