//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLInterconnectCoupler_2(
  input          rf_reset,
  input          clock,
  input          reset,
  input          auto_widget_out_a_ready,
  output         auto_widget_out_a_valid,
  output [2:0]   auto_widget_out_a_bits_opcode,
  output [3:0]   auto_widget_out_a_bits_size,
  output [5:0]   auto_widget_out_a_bits_source,
  output [36:0]  auto_widget_out_a_bits_address,
  output         auto_widget_out_a_bits_user_amba_prot_bufferable,
  output         auto_widget_out_a_bits_user_amba_prot_modifiable,
  output         auto_widget_out_a_bits_user_amba_prot_readalloc,
  output         auto_widget_out_a_bits_user_amba_prot_writealloc,
  output         auto_widget_out_a_bits_user_amba_prot_privileged,
  output         auto_widget_out_a_bits_user_amba_prot_secure,
  output         auto_widget_out_a_bits_user_amba_prot_fetch,
  output [15:0]  auto_widget_out_a_bits_mask,
  output [127:0] auto_widget_out_a_bits_data,
  output         auto_widget_out_d_ready,
  input          auto_widget_out_d_valid,
  input  [2:0]   auto_widget_out_d_bits_opcode,
  input  [1:0]   auto_widget_out_d_bits_param,
  input  [3:0]   auto_widget_out_d_bits_size,
  input  [5:0]   auto_widget_out_d_bits_source,
  input  [4:0]   auto_widget_out_d_bits_sink,
  input          auto_widget_out_d_bits_denied,
  input  [127:0] auto_widget_out_d_bits_data,
  input          auto_widget_out_d_bits_corrupt,
  output         auto_bus_xing_in_a_ready,
  input          auto_bus_xing_in_a_valid,
  input  [2:0]   auto_bus_xing_in_a_bits_opcode,
  input  [3:0]   auto_bus_xing_in_a_bits_size,
  input  [5:0]   auto_bus_xing_in_a_bits_source,
  input  [36:0]  auto_bus_xing_in_a_bits_address,
  input          auto_bus_xing_in_a_bits_user_amba_prot_bufferable,
  input          auto_bus_xing_in_a_bits_user_amba_prot_modifiable,
  input          auto_bus_xing_in_a_bits_user_amba_prot_readalloc,
  input          auto_bus_xing_in_a_bits_user_amba_prot_writealloc,
  input          auto_bus_xing_in_a_bits_user_amba_prot_privileged,
  input          auto_bus_xing_in_a_bits_user_amba_prot_secure,
  input          auto_bus_xing_in_a_bits_user_amba_prot_fetch,
  input  [7:0]   auto_bus_xing_in_a_bits_mask,
  input  [63:0]  auto_bus_xing_in_a_bits_data,
  input          auto_bus_xing_in_d_ready,
  output         auto_bus_xing_in_d_valid,
  output [2:0]   auto_bus_xing_in_d_bits_opcode,
  output [1:0]   auto_bus_xing_in_d_bits_param,
  output [3:0]   auto_bus_xing_in_d_bits_size,
  output [5:0]   auto_bus_xing_in_d_bits_source,
  output [4:0]   auto_bus_xing_in_d_bits_sink,
  output         auto_bus_xing_in_d_bits_denied,
  output [63:0]  auto_bus_xing_in_d_bits_data,
  output         auto_bus_xing_in_d_bits_corrupt
);
  wire  widget_rf_reset; // @[WidthWidget.scala 219:28]
  wire  widget_clock; // @[WidthWidget.scala 219:28]
  wire  widget_reset; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_a_ready; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_a_valid; // @[WidthWidget.scala 219:28]
  wire [2:0] widget_auto_in_a_bits_opcode; // @[WidthWidget.scala 219:28]
  wire [3:0] widget_auto_in_a_bits_size; // @[WidthWidget.scala 219:28]
  wire [5:0] widget_auto_in_a_bits_source; // @[WidthWidget.scala 219:28]
  wire [36:0] widget_auto_in_a_bits_address; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_a_bits_user_amba_prot_bufferable; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_a_bits_user_amba_prot_modifiable; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_a_bits_user_amba_prot_readalloc; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_a_bits_user_amba_prot_writealloc; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_a_bits_user_amba_prot_privileged; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_a_bits_user_amba_prot_secure; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_a_bits_user_amba_prot_fetch; // @[WidthWidget.scala 219:28]
  wire [7:0] widget_auto_in_a_bits_mask; // @[WidthWidget.scala 219:28]
  wire [63:0] widget_auto_in_a_bits_data; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_d_ready; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_d_valid; // @[WidthWidget.scala 219:28]
  wire [2:0] widget_auto_in_d_bits_opcode; // @[WidthWidget.scala 219:28]
  wire [1:0] widget_auto_in_d_bits_param; // @[WidthWidget.scala 219:28]
  wire [3:0] widget_auto_in_d_bits_size; // @[WidthWidget.scala 219:28]
  wire [5:0] widget_auto_in_d_bits_source; // @[WidthWidget.scala 219:28]
  wire [4:0] widget_auto_in_d_bits_sink; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_d_bits_denied; // @[WidthWidget.scala 219:28]
  wire [63:0] widget_auto_in_d_bits_data; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_d_bits_corrupt; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_a_ready; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_a_valid; // @[WidthWidget.scala 219:28]
  wire [2:0] widget_auto_out_a_bits_opcode; // @[WidthWidget.scala 219:28]
  wire [3:0] widget_auto_out_a_bits_size; // @[WidthWidget.scala 219:28]
  wire [5:0] widget_auto_out_a_bits_source; // @[WidthWidget.scala 219:28]
  wire [36:0] widget_auto_out_a_bits_address; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_a_bits_user_amba_prot_bufferable; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_a_bits_user_amba_prot_modifiable; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_a_bits_user_amba_prot_readalloc; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_a_bits_user_amba_prot_writealloc; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_a_bits_user_amba_prot_privileged; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_a_bits_user_amba_prot_secure; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_a_bits_user_amba_prot_fetch; // @[WidthWidget.scala 219:28]
  wire [15:0] widget_auto_out_a_bits_mask; // @[WidthWidget.scala 219:28]
  wire [127:0] widget_auto_out_a_bits_data; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_d_ready; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_d_valid; // @[WidthWidget.scala 219:28]
  wire [2:0] widget_auto_out_d_bits_opcode; // @[WidthWidget.scala 219:28]
  wire [1:0] widget_auto_out_d_bits_param; // @[WidthWidget.scala 219:28]
  wire [3:0] widget_auto_out_d_bits_size; // @[WidthWidget.scala 219:28]
  wire [5:0] widget_auto_out_d_bits_source; // @[WidthWidget.scala 219:28]
  wire [4:0] widget_auto_out_d_bits_sink; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_d_bits_denied; // @[WidthWidget.scala 219:28]
  wire [127:0] widget_auto_out_d_bits_data; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_d_bits_corrupt; // @[WidthWidget.scala 219:28]
  RHEA__TLWidthWidget_1 widget ( // @[WidthWidget.scala 219:28]
    .rf_reset(widget_rf_reset),
    .clock(widget_clock),
    .reset(widget_reset),
    .auto_in_a_ready(widget_auto_in_a_ready),
    .auto_in_a_valid(widget_auto_in_a_valid),
    .auto_in_a_bits_opcode(widget_auto_in_a_bits_opcode),
    .auto_in_a_bits_size(widget_auto_in_a_bits_size),
    .auto_in_a_bits_source(widget_auto_in_a_bits_source),
    .auto_in_a_bits_address(widget_auto_in_a_bits_address),
    .auto_in_a_bits_user_amba_prot_bufferable(widget_auto_in_a_bits_user_amba_prot_bufferable),
    .auto_in_a_bits_user_amba_prot_modifiable(widget_auto_in_a_bits_user_amba_prot_modifiable),
    .auto_in_a_bits_user_amba_prot_readalloc(widget_auto_in_a_bits_user_amba_prot_readalloc),
    .auto_in_a_bits_user_amba_prot_writealloc(widget_auto_in_a_bits_user_amba_prot_writealloc),
    .auto_in_a_bits_user_amba_prot_privileged(widget_auto_in_a_bits_user_amba_prot_privileged),
    .auto_in_a_bits_user_amba_prot_secure(widget_auto_in_a_bits_user_amba_prot_secure),
    .auto_in_a_bits_user_amba_prot_fetch(widget_auto_in_a_bits_user_amba_prot_fetch),
    .auto_in_a_bits_mask(widget_auto_in_a_bits_mask),
    .auto_in_a_bits_data(widget_auto_in_a_bits_data),
    .auto_in_d_ready(widget_auto_in_d_ready),
    .auto_in_d_valid(widget_auto_in_d_valid),
    .auto_in_d_bits_opcode(widget_auto_in_d_bits_opcode),
    .auto_in_d_bits_param(widget_auto_in_d_bits_param),
    .auto_in_d_bits_size(widget_auto_in_d_bits_size),
    .auto_in_d_bits_source(widget_auto_in_d_bits_source),
    .auto_in_d_bits_sink(widget_auto_in_d_bits_sink),
    .auto_in_d_bits_denied(widget_auto_in_d_bits_denied),
    .auto_in_d_bits_data(widget_auto_in_d_bits_data),
    .auto_in_d_bits_corrupt(widget_auto_in_d_bits_corrupt),
    .auto_out_a_ready(widget_auto_out_a_ready),
    .auto_out_a_valid(widget_auto_out_a_valid),
    .auto_out_a_bits_opcode(widget_auto_out_a_bits_opcode),
    .auto_out_a_bits_size(widget_auto_out_a_bits_size),
    .auto_out_a_bits_source(widget_auto_out_a_bits_source),
    .auto_out_a_bits_address(widget_auto_out_a_bits_address),
    .auto_out_a_bits_user_amba_prot_bufferable(widget_auto_out_a_bits_user_amba_prot_bufferable),
    .auto_out_a_bits_user_amba_prot_modifiable(widget_auto_out_a_bits_user_amba_prot_modifiable),
    .auto_out_a_bits_user_amba_prot_readalloc(widget_auto_out_a_bits_user_amba_prot_readalloc),
    .auto_out_a_bits_user_amba_prot_writealloc(widget_auto_out_a_bits_user_amba_prot_writealloc),
    .auto_out_a_bits_user_amba_prot_privileged(widget_auto_out_a_bits_user_amba_prot_privileged),
    .auto_out_a_bits_user_amba_prot_secure(widget_auto_out_a_bits_user_amba_prot_secure),
    .auto_out_a_bits_user_amba_prot_fetch(widget_auto_out_a_bits_user_amba_prot_fetch),
    .auto_out_a_bits_mask(widget_auto_out_a_bits_mask),
    .auto_out_a_bits_data(widget_auto_out_a_bits_data),
    .auto_out_d_ready(widget_auto_out_d_ready),
    .auto_out_d_valid(widget_auto_out_d_valid),
    .auto_out_d_bits_opcode(widget_auto_out_d_bits_opcode),
    .auto_out_d_bits_param(widget_auto_out_d_bits_param),
    .auto_out_d_bits_size(widget_auto_out_d_bits_size),
    .auto_out_d_bits_source(widget_auto_out_d_bits_source),
    .auto_out_d_bits_sink(widget_auto_out_d_bits_sink),
    .auto_out_d_bits_denied(widget_auto_out_d_bits_denied),
    .auto_out_d_bits_data(widget_auto_out_d_bits_data),
    .auto_out_d_bits_corrupt(widget_auto_out_d_bits_corrupt)
  );
  assign widget_rf_reset = rf_reset;
  assign auto_widget_out_a_valid = widget_auto_out_a_valid; // @[LazyModule.scala 390:12]
  assign auto_widget_out_a_bits_opcode = widget_auto_out_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_widget_out_a_bits_size = widget_auto_out_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_widget_out_a_bits_source = widget_auto_out_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_widget_out_a_bits_address = widget_auto_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_widget_out_a_bits_user_amba_prot_bufferable = widget_auto_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 390:12]
  assign auto_widget_out_a_bits_user_amba_prot_modifiable = widget_auto_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 390:12]
  assign auto_widget_out_a_bits_user_amba_prot_readalloc = widget_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 390:12]
  assign auto_widget_out_a_bits_user_amba_prot_writealloc = widget_auto_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 390:12]
  assign auto_widget_out_a_bits_user_amba_prot_privileged = widget_auto_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 390:12]
  assign auto_widget_out_a_bits_user_amba_prot_secure = widget_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 390:12]
  assign auto_widget_out_a_bits_user_amba_prot_fetch = widget_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 390:12]
  assign auto_widget_out_a_bits_mask = widget_auto_out_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_widget_out_a_bits_data = widget_auto_out_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_widget_out_d_ready = widget_auto_out_d_ready; // @[LazyModule.scala 390:12]
  assign auto_bus_xing_in_a_ready = widget_auto_in_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_valid = widget_auto_in_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_opcode = widget_auto_in_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_param = widget_auto_in_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_size = widget_auto_in_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_source = widget_auto_in_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_sink = widget_auto_in_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_denied = widget_auto_in_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_data = widget_auto_in_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_corrupt = widget_auto_in_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign widget_clock = clock;
  assign widget_reset = reset;
  assign widget_auto_in_a_valid = auto_bus_xing_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_opcode = auto_bus_xing_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_size = auto_bus_xing_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_source = auto_bus_xing_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_address = auto_bus_xing_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_user_amba_prot_bufferable = auto_bus_xing_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_user_amba_prot_modifiable = auto_bus_xing_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_user_amba_prot_readalloc = auto_bus_xing_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_user_amba_prot_writealloc = auto_bus_xing_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_user_amba_prot_privileged = auto_bus_xing_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_user_amba_prot_secure = auto_bus_xing_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_user_amba_prot_fetch = auto_bus_xing_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_mask = auto_bus_xing_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_data = auto_bus_xing_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_in_d_ready = auto_bus_xing_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_ready = auto_widget_out_a_ready; // @[LazyModule.scala 390:12]
  assign widget_auto_out_d_valid = auto_widget_out_d_valid; // @[LazyModule.scala 390:12]
  assign widget_auto_out_d_bits_opcode = auto_widget_out_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign widget_auto_out_d_bits_param = auto_widget_out_d_bits_param; // @[LazyModule.scala 390:12]
  assign widget_auto_out_d_bits_size = auto_widget_out_d_bits_size; // @[LazyModule.scala 390:12]
  assign widget_auto_out_d_bits_source = auto_widget_out_d_bits_source; // @[LazyModule.scala 390:12]
  assign widget_auto_out_d_bits_sink = auto_widget_out_d_bits_sink; // @[LazyModule.scala 390:12]
  assign widget_auto_out_d_bits_denied = auto_widget_out_d_bits_denied; // @[LazyModule.scala 390:12]
  assign widget_auto_out_d_bits_data = auto_widget_out_d_bits_data; // @[LazyModule.scala 390:12]
  assign widget_auto_out_d_bits_corrupt = auto_widget_out_d_bits_corrupt; // @[LazyModule.scala 390:12]
endmodule
