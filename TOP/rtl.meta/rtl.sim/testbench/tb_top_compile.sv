`timescale 1ns/1ps
module tb_top;

//reg rstn;
//reg OSC;
//reg PKG0;
//reg RTC;
//wire [15:0] GPIOA;
//pullup (weak1) (GPIOA[0]);
//pullup (weak1) (GPIOA[1]);
//pullup (weak1) (GPIOA[2]);
//pullup (weak1) (GPIOA[3]);
//pullup (weak1) (GPIOA[4]);
//pullup (weak1) (GPIOA[5]);
//pullup (weak1) (GPIOA[6]);
//pullup (weak1) (GPIOA[7]);
//pullup (weak1) (GPIOA[8]);
//pullup (weak1) (GPIOA[9]);
//pullup (weak1) (GPIOA[10]);
//pullup (weak1) (GPIOA[11]);
//pullup (weak1) (GPIOA[12]);
//pullup (weak1) (GPIOA[13]);
//pullup (weak1) (GPIOA[14]);
//pullup (weak1) (GPIOA[15]);
//
//initial begin
//    rstn = 1;
//    OSC = 0;
//    RTC = 0;
//    PKG0 = 1;
//end
//
//always begin
//    #20.833 OSC = ~OSC;
//end
//
//always begin
//    #15258.789 RTC = ~RTC;
//end
//
//`ifdef DDR_EMPTY
//`else
//LPDDR4_itf #(.C(2), .D(16)) LPDDR4__0();
//LPDDR4_itf #(.C(2), .D(16)) LPDDR4__1();
//lpddr4_wrapper LPDDR4__MODEL_0(.LPDDR4(LPDDR4__0));
//lpddr4_wrapper LPDDR4__MODEL_1(.LPDDR4(LPDDR4__1));
//`endif
//
//TOP duv (
//    .PAD__PKG0            (PKG0),
//    .PAD__RTC             (RTC),
//    .PAD__RSTN            (rstn),
//    .PAD__XTIN            (OSC),
//    .PAD__XTOUT           (),
//    .PAD__OSC             (OSC),
//    .PAD__JTAG_TCK        (1'b0),
//    .PAD__JTAG_TMS        (1'b0),
//    .PAD__JTAG_TDI        (1'b0),
//    .PAD__JTAG_TDO        (),
//    .PAD__JTAG_TRST       (1'b0),
//    .PAD__GPIOA_0         (GPIOA[0]),
//    .PAD__GPIOA_1         (GPIOA[1]),
//    .PAD__GPIOA_2         (GPIOA[2]),
//    .PAD__GPIOA_3         (GPIOA[3]),
//    .PAD__GPIOA_4         (GPIOA[4]),
//    .PAD__GPIOA_5         (GPIOA[5]),
//    .PAD__GPIOA_6         (GPIOA[6]),
//    .PAD__GPIOA_7         (GPIOA[7]),
//    .PAD__GPIOA_8         (GPIOA[8]),
//    .PAD__GPIOA_9         (GPIOA[9]),
//    .PAD__GPIOA_10        (GPIOA[10]),
//    .PAD__GPIOA_11        (GPIOA[11]),
//    .PAD__GPIOA_12        (GPIOA[12]),
//    .PAD__GPIOA_13        (GPIOA[13]),
//    .PAD__GPIOA_14        (GPIOA[14]),
//    .PAD__GPIOA_15        (GPIOA[15]),
//
//`ifdef DDR_EMPTY
//    .PAD__LPDDR4_0_ADCT   (),
//    .PAD__LPDDR4_0_CK     (),
//    .PAD__LPDDR4_0_CKB    (),
//    .PAD__LPDDR4_0_CKE    (),
//    .PAD__LPDDR4_0_CS     (),
//    .PAD__LPDDR4_0_DM_A   (),
//    .PAD__LPDDR4_0_DM_B   (),
//    .PAD__LPDDR4_0_DQ_A   (),
//    .PAD__LPDDR4_0_DQ_B   (),
//    .PAD__LPDDR4_0_NDQS_A (),
//    .PAD__LPDDR4_0_NDQS_B (),
//    .PAD__LPDDR4_0_ODT    (),
//    .PAD__LPDDR4_0_PDQS_A (),
//    .PAD__LPDDR4_0_PDQS_B (),
//    .PAD__LPDDR4_0_RESET  (),
//    .PAD__LPDDR4_0_ZQ     (),
//
//    .PAD__LPDDR4_1_ADCT   (),
//    .PAD__LPDDR4_1_CK     (),
//    .PAD__LPDDR4_1_CKB    (),
//    .PAD__LPDDR4_1_CKE    (),
//    .PAD__LPDDR4_1_CS     (),
//    .PAD__LPDDR4_1_DM_A   (),
//    .PAD__LPDDR4_1_DM_B   (),
//    .PAD__LPDDR4_1_DQ_A   (),
//    .PAD__LPDDR4_1_DQ_B   (),
//    .PAD__LPDDR4_1_NDQS_A (),
//    .PAD__LPDDR4_1_NDQS_B (),
//    .PAD__LPDDR4_1_ODT    (),
//    .PAD__LPDDR4_1_PDQS_A (),
//    .PAD__LPDDR4_1_PDQS_B (),
//    .PAD__LPDDR4_1_RESET  (),
//    .PAD__LPDDR4_1_ZQ     ()
//`else
//    .PAD__LPDDR4_0_ADCT   (LPDDR4__0.ADCT),
//    .PAD__LPDDR4_0_CK     (LPDDR4__0.CK),
//    .PAD__LPDDR4_0_CKB    (LPDDR4__0.CKB),
//    .PAD__LPDDR4_0_CKE    (LPDDR4__0.CKE),
//    .PAD__LPDDR4_0_CS     (LPDDR4__0.CS),
//    .PAD__LPDDR4_0_DM_A   (LPDDR4__0.DM_A),
//    .PAD__LPDDR4_0_DM_B   (LPDDR4__0.DM_B),
//    .PAD__LPDDR4_0_DQ_A   (LPDDR4__0.DQ_A),
//    .PAD__LPDDR4_0_DQ_B   (LPDDR4__0.DQ_B),
//    .PAD__LPDDR4_0_NDQS_A (LPDDR4__0.NDQS_A),
//    .PAD__LPDDR4_0_NDQS_B (LPDDR4__0.NDQS_B),
//    .PAD__LPDDR4_0_ODT    (LPDDR4__0.ODT),
//    .PAD__LPDDR4_0_PDQS_A (LPDDR4__0.PDQS_A),
//    .PAD__LPDDR4_0_PDQS_B (LPDDR4__0.PDQS_B),
//    .PAD__LPDDR4_0_RESET  (LPDDR4__0.RESET ),
//    .PAD__LPDDR4_0_ZQ     (LPDDR4__0.ZQ),
//
//    .PAD__LPDDR4_1_ADCT   (LPDDR4__1.ADCT),
//    .PAD__LPDDR4_1_CK     (LPDDR4__1.CK),
//    .PAD__LPDDR4_1_CKB    (LPDDR4__1.CKB),
//    .PAD__LPDDR4_1_CKE    (LPDDR4__1.CKE),
//    .PAD__LPDDR4_1_CS     (LPDDR4__1.CS),
//    .PAD__LPDDR4_1_DM_A   (LPDDR4__1.DM_A),
//    .PAD__LPDDR4_1_DM_B   (LPDDR4__1.DM_B),
//    .PAD__LPDDR4_1_DQ_A   (LPDDR4__1.DQ_A),
//    .PAD__LPDDR4_1_DQ_B   (LPDDR4__1.DQ_B),
//    .PAD__LPDDR4_1_NDQS_A (LPDDR4__1.NDQS_A),
//    .PAD__LPDDR4_1_NDQS_B (LPDDR4__1.NDQS_B),
//    .PAD__LPDDR4_1_ODT    (LPDDR4__1.ODT),
//    .PAD__LPDDR4_1_PDQS_A (LPDDR4__1.PDQS_A),
//    .PAD__LPDDR4_1_PDQS_B (LPDDR4__1.PDQS_B),
//    .PAD__LPDDR4_1_RESET  (LPDDR4__1.RESET),
//    .PAD__LPDDR4_1_ZQ     (LPDDR4__1.ZQ)
//`endif
//);
//
//// UART LOOPBACK
//assign GPIOA[1] = GPIOA[0];
//assign GPIOA[3] = GPIOA[2];
//
//reg [1000:0] TESTFILE;
//initial begin
//    // hello program memory loading
//    //$readmemh("./freedom-e-sdk/software/hello/release/hello.hex", tb_top.duv.uCORE.uSUBSYSTEM_PERI_0.ROM_0.uMEM.mem);
//    if ($value$plusargs("testfile=%s", TESTFILE)) begin
//        $display("testfile = %s",TESTFILE);
//        $readmemh(TESTFILE, tb_top.duv.uCORE.SUBSYSTEM_CPU_0.CPU_ROM_0.CPU_ROM_0.Mem);
//        //$readmemh(TESTFILE, tb_top.duv.uCORE.AXI4_MBUS_SRAM_0.SRAM_SPSRAM_16384x128_0.mem);
//    end
//    else begin
//        //$readmemh("./freedom-e-sdk/software/hello/release/hello.hex", tb_top.duv.uCORE.SUBSYSTEM_CPU_0.ROM_0.uMEM_0.mem);
//    end
//    //$display("ROM Data = %h",tb_top.duv.uCORE.SUBSYSTEM_CPU_0.CPU_ROM_0.CPU_ROM_0.Mem[0]);
//end
//
//initial begin
//    force tb_top.duv.uCORE.SUBSYSTEM_CPU_0.RESET_VECTOR_0 = 36'h0_8000_0000;
//    force tb_top.duv.uCORE.SUBSYSTEM_CPU_0.RESET_VECTOR_1 = 36'h0_8000_0000;
//    //force tb_top.duv.uCORE.SUBSYSTEM_CPU_0.RESET_VECTOR_0 = 36'h4_0000_0000;
//    //force tb_top.duv.uCORE.SUBSYSTEM_CPU_0.RESET_VECTOR_1 = 36'h4_0000_0000;
//    //force tb_top.duv.uCORE.SUBSYSTEM_CPU_0.RESET_VECTOR_0 = 36'hc_0000_0000;
//    //force tb_top.duv.uCORE.SUBSYSTEM_CPU_0.RESET_VECTOR_1 = 36'hc_0000_0000;
//    #10000 rstn = 0;
//    #10000 rstn = 1;
//
//    // For CI fail test
////    #10000 ;
////    force tb_top.duv.uCORE.SUBSYSTEM_CPU_0.uCOREIP_wrap_0.uCOREIP_0.Logic.testIndicator.status_regs_0 = 32'h0000_3333;
//end
//
//// Simulation finish condition
//  bit verbose = |($test$plusargs("verbose"));
//  bit printf_cond;
//  assign printf_cond = verbose && rstn;
//
//bind `INDICATOR
//     TestFinisher #(.NCONCURRENT(1),
//                    .REGBYTES(4))
//     i_TF (.reset(reset),
//           .clock(clock),
//`ifndef VERILATOR
//           .status_regs({>>{status_regs_0}}));
//`else
//           .status_regs('{status_regs_0}));
//
//`endif
//
///*
//initial begin
//   $fsdbDumpvars("+all");
//end
//*/
//
//initial begin
//    if (!$test$plusargs("ENABLE_SEMICPU")) begin
//        #1000000; $finish;
//    end
//end
//
//`ifndef SYNTHESIS
//SemiCPU #(
//    .MBUS_ADDR_BASE(64'h08_0000_0000), .MBUS_ADDR_SIZE(64'h8_0000_0000),
//    .SBUS_ADDR_BASE(64'h18_0000_0000), .SBUS_ADDR_SIZE(64'h8_0000_0000),
//    .PBUS_ADDR_BASE(64'h00_8000_0000), .PBUS_ADDR_SIZE(64'h0_8000_0000)
//) uSEMICPU_0(
//    .CLK__SYS(OSC),
//    .RSTN__SYS(rstn)
//);
//`endif
cpu_subsystem HPDF_CPU ();
usb3_subsystem HPDF_USB3 ();
lpddr4_subsystem HPDF_LPDDR4 ();
peripheral_subsystem HPDF_PERIPHERAL ();
initial begin
    $display("*** Passed ***");
    $finish;
end

endmodule
