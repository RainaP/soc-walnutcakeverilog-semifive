//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLTraceSink(
  input         clock,
  input         reset,
  input         auto_sink_out_a_ready,
  output        auto_sink_out_a_valid,
  output [31:0] auto_sink_out_a_bits_address,
  output [31:0] auto_sink_out_a_bits_data,
  input         auto_sink_out_d_valid,
  input         auto_sink_out_d_bits_denied,
  input         io_teActive,
  input  [3:0]  io_teControl_teSink,
  output        io_stream_ready,
  input         io_stream_valid,
  input  [31:0] io_stream_bits_data,
  output        io_sinkError
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
`endif // RANDOMIZE_REG_INIT
  reg [3:0] tlState; // @[TraceSink.scala 358:31]
  wire  _T_5 = ~auto_sink_out_d_valid; // @[TraceSink.scala 367:47]
  wire  _T_8 = tlState[0] & auto_sink_out_a_ready; // @[TraceSink.scala 374:30]
  wire [3:0] _tlState_T = tlState ^ 4'h3; // @[TraceSink.scala 377:27]
  wire [3:0] _GEN_4 = tlState[1] & auto_sink_out_d_valid ? 4'h0 : tlState; // @[TraceSink.scala 378:44 TraceSink.scala 379:17 TraceSink.scala 358:31]
  assign auto_sink_out_a_valid = io_stream_valid | tlState[0]; // @[TraceSink.scala 401:32]
  assign auto_sink_out_a_bits_address = {io_teControl_teSink, 28'h0}; // @[TraceSink.scala 382:68]
  assign auto_sink_out_a_bits_data = io_stream_bits_data; // @[Edges.scala 465:17 Edges.scala 472:15]
  assign io_stream_ready = auto_sink_out_a_ready & ~tlState[3]; // @[TraceSink.scala 405:50]
  assign io_sinkError = auto_sink_out_d_valid & auto_sink_out_d_bits_denied; // @[TraceSink.scala 404:38]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      tlState <= 4'h0;
    end else if (~io_teActive) begin
      tlState <= 4'h0;
    end else if (tlState == 4'h0) begin
      if (io_stream_valid & ~auto_sink_out_a_ready) begin
        tlState <= 4'h1;
      end else if (io_stream_valid & auto_sink_out_a_ready & ~auto_sink_out_d_valid) begin
        tlState <= 4'h2;
      end
    end else if (tlState[0] & auto_sink_out_a_ready & auto_sink_out_d_valid) begin
      tlState <= 4'h0;
    end else if (_T_8 & _T_5) begin
      tlState <= _tlState_T;
    end else begin
      tlState <= _GEN_4;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  tlState = _RAND_0[3:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    tlState = 4'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
