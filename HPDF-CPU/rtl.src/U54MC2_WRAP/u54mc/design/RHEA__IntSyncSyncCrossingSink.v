//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__IntSyncSyncCrossingSink(
  input   auto_in_sync_0,
  input   auto_in_sync_1,
  input   auto_in_sync_2,
  input   auto_in_sync_3,
  input   auto_in_sync_4,
  input   auto_in_sync_5,
  input   auto_in_sync_6,
  input   auto_in_sync_7,
  input   auto_in_sync_8,
  input   auto_in_sync_9,
  input   auto_in_sync_10,
  input   auto_in_sync_11,
  input   auto_in_sync_12,
  input   auto_in_sync_13,
  input   auto_in_sync_14,
  input   auto_in_sync_15,
  output  auto_out_0,
  output  auto_out_1,
  output  auto_out_2,
  output  auto_out_3,
  output  auto_out_4,
  output  auto_out_5,
  output  auto_out_6,
  output  auto_out_7,
  output  auto_out_8,
  output  auto_out_9,
  output  auto_out_10,
  output  auto_out_11,
  output  auto_out_12,
  output  auto_out_13,
  output  auto_out_14,
  output  auto_out_15
);
  assign auto_out_0 = auto_in_sync_0; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
  assign auto_out_1 = auto_in_sync_1; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
  assign auto_out_2 = auto_in_sync_2; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
  assign auto_out_3 = auto_in_sync_3; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
  assign auto_out_4 = auto_in_sync_4; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
  assign auto_out_5 = auto_in_sync_5; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
  assign auto_out_6 = auto_in_sync_6; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
  assign auto_out_7 = auto_in_sync_7; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
  assign auto_out_8 = auto_in_sync_8; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
  assign auto_out_9 = auto_in_sync_9; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
  assign auto_out_10 = auto_in_sync_10; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
  assign auto_out_11 = auto_in_sync_11; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
  assign auto_out_12 = auto_in_sync_12; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
  assign auto_out_13 = auto_in_sync_13; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
  assign auto_out_14 = auto_in_sync_14; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
  assign auto_out_15 = auto_in_sync_15; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
endmodule
