module pll1426xe_wrap_testmux (
    input           TEST__SCAN,
    input           TEST__PLL,
    input  [52:0]   TEST__IN,
    output [7:0]    TEST__OUT,
    input [5:0]     REG__P,
    input [9:0]     REG__M,
    input [2:0]     REG__S,
    input [1:0]     REG__ICP,
    input [3:0]     REG__RSEL,
    input [4:0]     REG__EXTAFC,
    input [1:0]     REG__LOCK_CON_DLY,
    input [1:0]     REG__LOCK_CON_REV,
    input [1:0]     REG__LOCK_CON_IN,
    input [1:0]     REG__LOCK_CON_OUT,
    input           REG__RESETB,
    input           REG__BYPASS,
    input           REG__AFC_ENB,
    input           REG__FSEL,
    input           REG__FEED_EN,
    input           REG__LOCK_EN,
    input           REG__AFCINIT_SEL,
    input           REG__LRD_EN,
    input           REG__FOUT_MASK,
    input           REG__VCO_BOOST,
    input           REG__PBIAS_CTRL_EN,
    input           REG__PBIAS_CTRL,
    output          REG__LOCK,
    output          REG__FEED_OUT,
    output [4:0]    REG__AFC_CODE,
    output [5:0]    PLL__P,
    output [9:0]    PLL__M,
    output [2:0]    PLL__S,
    output [1:0]    PLL__ICP,
    output [3:0]    PLL__RSEL,
    output [4:0]    PLL__EXTAFC,
    output [1:0]    PLL__LOCK_CON_DLY,
    output [1:0]    PLL__LOCK_CON_REV,
    output [1:0]    PLL__LOCK_CON_IN,
    output [1:0]    PLL__LOCK_CON_OUT,
    output          PLL__RESETB,
    output          PLL__BYPASS,
    output          PLL__AFC_ENB,
    output          PLL__FSEL,
    output          PLL__FEED_EN,
    output          PLL__LOCK_EN,
    output          PLL__AFCINIT_SEL,
    output          PLL__LRD_EN,
    output          PLL__FOUT_MASK,
    output          PLL__VCO_BOOST,
    output          PLL__PBIAS_CTRL_EN,
    output          PLL__PBIAS_CTRL,
    input           PLL__LOCK,
    input [4:0]     PLL__AFC_CODE,
    input           PLL__FEED_OUT,
    input           CLK__FOUT
);


wire        TEST__IN__RESETN        = TEST__IN[0];
wire        TEST__IN__FINSEL        = TEST__IN[1];
wire [5:0]  TEST__IN__P             = TEST__IN[7:2];
wire [9:0]  TEST__IN__M             = TEST__IN[17:8];
wire [2:0]  TEST__IN__S             = TEST__IN[20:18];
wire [1:0]  TEST__IN__ICP           = TEST__IN[22:21];
// TEST__IN[23]
wire [3:0]  TEST__IN__RSEL          = TEST__IN[27:24];
wire [4:0]  TEST__IN__EXTAFC        = TEST__IN[32:28];
wire [1:0]  TEST__IN__LOCK_CON_DLY  = TEST__IN[34:33];
wire [1:0]  TEST__IN__LOCK_CON_REV  = TEST__IN[36:35];
wire [1:0]  TEST__IN__LOCK_CON_IN   = TEST__IN[38:37];
wire [1:0]  TEST__IN__LOCK_CON_OUT  = TEST__IN[40:39];
wire        TEST__IN__RESETB        = TEST__IN[41];
wire        TEST__IN__BYPASS        = TEST__IN[42];
wire        TEST__IN__AFC_ENB       = TEST__IN[43];
wire        TEST__IN__FSEL          = TEST__IN[44];
wire        TEST__IN__FEED_EN       = TEST__IN[45];
wire        TEST__IN__LOCK_EN       = TEST__IN[46];
wire        TEST__IN__AFCINIT_SEL   = TEST__IN[47];
wire        TEST__IN__LRD_EN        = TEST__IN[48];
wire        TEST__IN__FOUT_MASK     = TEST__IN[49];
wire        TEST__IN__VCO_BOOST     = TEST__IN[50];
wire        TEST__IN__PBIAS_CTRL_EN = TEST__IN[51];
wire        TEST__IN__PBIAS_CTRL    = TEST__IN[52];
reg         CLK__FOUTdiv2;
reg         CLK__FOUTdiv4;
reg         CLK__FOUTdiv8;
reg         CLK__FOUTdiv16;
reg         CLK__FOUTdiv32;

assign PLL__P              = TEST__PLL ? TEST__IN__P               : REG__P             ;
assign PLL__M              = TEST__PLL ? TEST__IN__M               : REG__M             ;
assign PLL__S              = TEST__PLL ? TEST__IN__S               : REG__S             ;
assign PLL__ICP            = TEST__PLL ? TEST__IN__ICP             : REG__ICP           ;
assign PLL__RSEL           = TEST__PLL ? TEST__IN__RSEL            : REG__RSEL          ;
assign PLL__EXTAFC         = TEST__PLL ? TEST__IN__EXTAFC          : REG__EXTAFC        ;
assign PLL__LOCK_CON_DLY   = TEST__PLL ? TEST__IN__LOCK_CON_DLY    : REG__LOCK_CON_DLY  ;
assign PLL__LOCK_CON_REV   = TEST__PLL ? TEST__IN__LOCK_CON_REV    : REG__LOCK_CON_REV  ;
assign PLL__LOCK_CON_IN    = TEST__PLL ? TEST__IN__LOCK_CON_IN     : REG__LOCK_CON_IN   ;
assign PLL__LOCK_CON_OUT   = TEST__PLL ? TEST__IN__LOCK_CON_OUT    : REG__LOCK_CON_OUT  ;
assign PLL__RESETB         = TEST__PLL ? TEST__IN__RESETB          : REG__RESETB        ;
assign PLL__BYPASS         = TEST__PLL ? TEST__IN__BYPASS          : REG__BYPASS        ;
assign PLL__AFC_ENB        = TEST__PLL ? TEST__IN__AFC_ENB         : REG__AFC_ENB       ;
assign PLL__FSEL           = TEST__PLL ? TEST__IN__FSEL            : REG__FSEL          ;
assign PLL__FEED_EN        = TEST__PLL ? TEST__IN__FEED_EN         : REG__FEED_EN       ;
assign PLL__LOCK_EN        = TEST__PLL ? TEST__IN__LOCK_EN         : REG__LOCK_EN       ;
assign PLL__AFCINIT_SEL    = TEST__PLL ? TEST__IN__AFCINIT_SEL     : REG__AFCINIT_SEL   ;
assign PLL__LRD_EN         = TEST__PLL ? TEST__IN__LRD_EN          : REG__LRD_EN        ;
assign PLL__FOUT_MASK      = TEST__PLL ? TEST__IN__FOUT_MASK       : REG__FOUT_MASK     ;
assign PLL__VCO_BOOST      = TEST__PLL ? TEST__IN__VCO_BOOST       : REG__VCO_BOOST     ;
assign PLL__PBIAS_CTRL_EN  = TEST__PLL ? TEST__IN__PBIAS_CTRL_EN   : REG__PBIAS_CTRL_EN ;
assign PLL__PBIAS_CTRL     = TEST__PLL ? TEST__IN__PBIAS_CTRL      : REG__PBIAS_CTRL    ;

assign TEST__OUT[0] = TEST__PLL ? CLK__FOUTdiv32 : 1'b0;
assign TEST__OUT[5:1] = TEST__PLL ? PLL__AFC_CODE : 5'h0;
assign TEST__OUT[6] = TEST__PLL ? PLL__FEED_OUT : 1'b0;
assign TEST__OUT[7] = TEST__PLL ? PLL__LOCK : 1'b0;

assign REG__FEED_OUT = PLL__FEED_OUT;
assign REG__LOCK = PLL__LOCK;
assign REG__AFC_CODE = PLL__AFC_CODE;

always @(posedge CLK__FOUT or negedge TEST__IN__RESETN) begin
    if (~TEST__IN__RESETN) begin
        CLK__FOUTdiv2 <= 1'b0;
    end
    else begin
        CLK__FOUTdiv2 <= ~CLK__FOUTdiv2;
    end
end
always @(posedge CLK__FOUTdiv2 or negedge TEST__IN__RESETN) begin
    if (~TEST__IN__RESETN) begin
        CLK__FOUTdiv4 <= 1'b0;
    end
    else begin
        CLK__FOUTdiv4 <= ~CLK__FOUTdiv4;
    end
end
always @(posedge CLK__FOUTdiv4 or negedge TEST__IN__RESETN) begin
    if (~TEST__IN__RESETN) begin
        CLK__FOUTdiv8 <= 1'b0;
    end
    else begin
        CLK__FOUTdiv8 <= ~CLK__FOUTdiv8;
    end
end
always @(posedge CLK__FOUTdiv8 or negedge TEST__IN__RESETN) begin
    if (~TEST__IN__RESETN) begin
        CLK__FOUTdiv16 <= 1'b0;
    end
    else begin
        CLK__FOUTdiv16 <= ~CLK__FOUTdiv16;
    end
end
always @(posedge CLK__FOUTdiv16 or negedge TEST__IN__RESETN) begin
    if (~TEST__IN__RESETN) begin
        CLK__FOUTdiv32 <= 1'b0;
    end
    else begin
        CLK__FOUTdiv32 <= ~CLK__FOUTdiv32;
    end
end
endmodule
// vim:tabstop=4:shiftwidth=4
