//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__SBToTL(
  input          clock,
  input          reset,
  input          auto_out_a_ready,
  output         auto_out_a_valid,
  output [2:0]   auto_out_a_bits_opcode,
  output [3:0]   auto_out_a_bits_size,
  output [36:0]  auto_out_a_bits_address,
  output [7:0]   auto_out_a_bits_data,
  output         auto_out_d_ready,
  input          auto_out_d_valid,
  input          auto_out_d_bits_denied,
  input  [7:0]   auto_out_d_bits_data,
  input          auto_out_d_bits_corrupt,
  input          io_rdEn,
  input          io_wrEn,
  input  [127:0] io_addrIn,
  input  [127:0] io_dataIn,
  input  [2:0]   io_sizeIn,
  output         io_rdLegal,
  output         io_wrLegal,
  output         io_rdDone,
  output         io_wrDone,
  output         io_respError,
  output [7:0]   io_dataOut,
  output         io_rdLoad_0,
  output         io_rdLoad_1,
  output         io_rdLoad_2,
  output         io_rdLoad_3,
  output         io_rdLoad_4,
  output         io_rdLoad_5,
  output         io_rdLoad_6,
  output         io_rdLoad_7,
  output [2:0]   io_sbStateOut,
  input          rf_reset
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
`endif // RANDOMIZE_REG_INIT
  wire  d_rf_reset; // @[Decoupled.scala 296:21]
  wire  d_clock; // @[Decoupled.scala 296:21]
  wire  d_reset; // @[Decoupled.scala 296:21]
  wire  d_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  d_io_enq_valid; // @[Decoupled.scala 296:21]
  wire  d_io_enq_bits_denied; // @[Decoupled.scala 296:21]
  wire [7:0] d_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  d_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  d_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  d_io_deq_valid; // @[Decoupled.scala 296:21]
  wire  d_io_deq_bits_denied; // @[Decoupled.scala 296:21]
  wire [7:0] d_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  d_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  reg [2:0] sbState; // @[SBA.scala 294:26]
  wire  _d_io_deq_ready_T = sbState == 3'h3; // @[SBA.scala 298:25]
  wire  _d_io_deq_ready_T_1 = sbState == 3'h4; // @[SBA.scala 298:62]
  reg [3:0] counter; // @[SBA.scala 306:26]
  wire [7:0] vecData_0 = io_dataIn[7:0]; // @[SBA.scala 308:63]
  wire [7:0] vecData_1 = io_dataIn[15:8]; // @[SBA.scala 308:63]
  wire [7:0] vecData_2 = io_dataIn[23:16]; // @[SBA.scala 308:63]
  wire [7:0] vecData_3 = io_dataIn[31:24]; // @[SBA.scala 308:63]
  wire [7:0] vecData_4 = io_dataIn[39:32]; // @[SBA.scala 308:63]
  wire [7:0] vecData_5 = io_dataIn[47:40]; // @[SBA.scala 308:63]
  wire [7:0] vecData_6 = io_dataIn[55:48]; // @[SBA.scala 308:63]
  wire [7:0] vecData_7 = io_dataIn[63:56]; // @[SBA.scala 308:63]
  wire [7:0] _GEN_1 = 3'h1 == counter[2:0] ? vecData_1 : vecData_0; // @[SBA.scala 309:15 SBA.scala 309:15]
  wire [7:0] _GEN_2 = 3'h2 == counter[2:0] ? vecData_2 : _GEN_1; // @[SBA.scala 309:15 SBA.scala 309:15]
  wire [7:0] _GEN_3 = 3'h3 == counter[2:0] ? vecData_3 : _GEN_2; // @[SBA.scala 309:15 SBA.scala 309:15]
  wire [7:0] _GEN_4 = 3'h4 == counter[2:0] ? vecData_4 : _GEN_3; // @[SBA.scala 309:15 SBA.scala 309:15]
  wire [7:0] _GEN_5 = 3'h5 == counter[2:0] ? vecData_5 : _GEN_4; // @[SBA.scala 309:15 SBA.scala 309:15]
  wire [7:0] _GEN_6 = 3'h6 == counter[2:0] ? vecData_6 : _GEN_5; // @[SBA.scala 309:15 SBA.scala 309:15]
  wire [7:0] muxedData = 3'h7 == counter[2:0] ? vecData_7 : _GEN_6; // @[SBA.scala 309:15 SBA.scala 309:15]
  wire [128:0] _rdLegal_addr_T_5 = {1'b0,$signed(io_addrIn)}; // @[Parameters.scala 137:49]
  wire [128:0] _rdLegal_addr_T_7 = $signed(_rdLegal_addr_T_5) & -129'sh5000; // @[Parameters.scala 137:52]
  wire  _rdLegal_addr_T_8 = $signed(_rdLegal_addr_T_7) == 129'sh0; // @[Parameters.scala 137:67]
  wire [127:0] _rdLegal_addr_T_9 = io_addrIn ^ 128'h3000; // @[Parameters.scala 137:31]
  wire [128:0] _rdLegal_addr_T_10 = {1'b0,$signed(_rdLegal_addr_T_9)}; // @[Parameters.scala 137:49]
  wire [128:0] _rdLegal_addr_T_12 = $signed(_rdLegal_addr_T_10) & -129'sh10001000; // @[Parameters.scala 137:52]
  wire  _rdLegal_addr_T_13 = $signed(_rdLegal_addr_T_12) == 129'sh0; // @[Parameters.scala 137:67]
  wire [127:0] _rdLegal_addr_T_14 = io_addrIn ^ 128'h2000000; // @[Parameters.scala 137:31]
  wire [128:0] _rdLegal_addr_T_15 = {1'b0,$signed(_rdLegal_addr_T_14)}; // @[Parameters.scala 137:49]
  wire [128:0] _rdLegal_addr_T_17 = $signed(_rdLegal_addr_T_15) & -129'sh10000; // @[Parameters.scala 137:52]
  wire  _rdLegal_addr_T_18 = $signed(_rdLegal_addr_T_17) == 129'sh0; // @[Parameters.scala 137:67]
  wire [127:0] _rdLegal_addr_T_19 = io_addrIn ^ 128'h2010000; // @[Parameters.scala 137:31]
  wire [128:0] _rdLegal_addr_T_20 = {1'b0,$signed(_rdLegal_addr_T_19)}; // @[Parameters.scala 137:49]
  wire [128:0] _rdLegal_addr_T_22 = $signed(_rdLegal_addr_T_20) & -129'sh4000; // @[Parameters.scala 137:52]
  wire  _rdLegal_addr_T_23 = $signed(_rdLegal_addr_T_22) == 129'sh0; // @[Parameters.scala 137:67]
  wire [127:0] _rdLegal_addr_T_24 = io_addrIn ^ 128'h8000000; // @[Parameters.scala 137:31]
  wire [128:0] _rdLegal_addr_T_25 = {1'b0,$signed(_rdLegal_addr_T_24)}; // @[Parameters.scala 137:49]
  wire [128:0] _rdLegal_addr_T_27 = $signed(_rdLegal_addr_T_25) & -129'sh2200000; // @[Parameters.scala 137:52]
  wire  _rdLegal_addr_T_28 = $signed(_rdLegal_addr_T_27) == 129'sh0; // @[Parameters.scala 137:67]
  wire [127:0] _rdLegal_addr_T_29 = io_addrIn ^ 128'hc000000; // @[Parameters.scala 137:31]
  wire [128:0] _rdLegal_addr_T_30 = {1'b0,$signed(_rdLegal_addr_T_29)}; // @[Parameters.scala 137:49]
  wire [128:0] _rdLegal_addr_T_32 = $signed(_rdLegal_addr_T_30) & -129'sh4000000; // @[Parameters.scala 137:52]
  wire  _rdLegal_addr_T_33 = $signed(_rdLegal_addr_T_32) == 129'sh0; // @[Parameters.scala 137:67]
  wire [127:0] _rdLegal_addr_T_34 = io_addrIn ^ 128'h10000000; // @[Parameters.scala 137:31]
  wire [128:0] _rdLegal_addr_T_35 = {1'b0,$signed(_rdLegal_addr_T_34)}; // @[Parameters.scala 137:49]
  wire [128:0] _rdLegal_addr_T_37 = $signed(_rdLegal_addr_T_35) & -129'sh2000; // @[Parameters.scala 137:52]
  wire  _rdLegal_addr_T_38 = $signed(_rdLegal_addr_T_37) == 129'sh0; // @[Parameters.scala 137:67]
  wire [127:0] _rdLegal_addr_T_39 = io_addrIn ^ 128'h80000000; // @[Parameters.scala 137:31]
  wire [128:0] _rdLegal_addr_T_40 = {1'b0,$signed(_rdLegal_addr_T_39)}; // @[Parameters.scala 137:49]
  wire [128:0] _rdLegal_addr_T_42 = $signed(_rdLegal_addr_T_40) & -129'sh80000000; // @[Parameters.scala 137:52]
  wire  _rdLegal_addr_T_43 = $signed(_rdLegal_addr_T_42) == 129'sh0; // @[Parameters.scala 137:67]
  wire [127:0] _rdLegal_addr_T_44 = io_addrIn ^ 128'h800000000; // @[Parameters.scala 137:31]
  wire [128:0] _rdLegal_addr_T_45 = {1'b0,$signed(_rdLegal_addr_T_44)}; // @[Parameters.scala 137:49]
  wire [128:0] _rdLegal_addr_T_47 = $signed(_rdLegal_addr_T_45) & -129'sh800000000; // @[Parameters.scala 137:52]
  wire  _rdLegal_addr_T_48 = $signed(_rdLegal_addr_T_47) == 129'sh0; // @[Parameters.scala 137:67]
  wire [127:0] _rdLegal_addr_T_49 = io_addrIn ^ 128'h1000000000; // @[Parameters.scala 137:31]
  wire [128:0] _rdLegal_addr_T_50 = {1'b0,$signed(_rdLegal_addr_T_49)}; // @[Parameters.scala 137:49]
  wire [128:0] _rdLegal_addr_T_52 = $signed(_rdLegal_addr_T_50) & -129'sh1000000000; // @[Parameters.scala 137:52]
  wire  _rdLegal_addr_T_53 = $signed(_rdLegal_addr_T_52) == 129'sh0; // @[Parameters.scala 137:67]
  wire  _T = sbState == 3'h1; // @[SBA.scala 321:18]
  wire [36:0] gbits_address = io_addrIn[36:0]; // @[Edges.scala 447:17 Edges.scala 452:15]
  wire [3:0] gbits_size = {{1'd0}, io_sizeIn}; // @[Edges.scala 447:17 Edges.scala 450:15]
  wire  _wrTxValid_T = sbState == 3'h2; // @[SBA.scala 337:29]
  wire  tl_a_valid = _T | _wrTxValid_T; // @[SBA.scala 365:52]
  wire  wrTxValid = _wrTxValid_T & tl_a_valid & auto_out_a_ready; // @[SBA.scala 337:69]
  wire  rdTxValid = _d_io_deq_ready_T & d_io_deq_valid & d_io_deq_ready; // @[SBA.scala 338:70]
  wire [7:0] _txLast_T = 8'h1 << io_sizeIn; // @[SBA.scala 339:39]
  wire [7:0] _txLast_T_2 = _txLast_T - 8'h1; // @[SBA.scala 339:53]
  wire [7:0] _GEN_11 = {{4'd0}, counter}; // @[SBA.scala 339:29]
  wire  txLast = _GEN_11 == _txLast_T_2; // @[SBA.scala 339:29]
  wire  _counter_T = wrTxValid | rdTxValid; // @[SBA.scala 340:31]
  wire [3:0] _counter_T_4 = counter + 4'h1; // @[SBA.scala 341:63]
  wire  _T_1 = sbState == 3'h0; // @[SBA.scala 348:19]
  wire [2:0] _sbState_T_9 = rdTxValid & txLast ? 3'h0 : sbState; // @[SBA.scala 356:21]
  wire [2:0] _sbState_T_11 = d_io_deq_valid & d_io_deq_ready ? 3'h0 : sbState; // @[SBA.scala 358:21]
  wire [2:0] _GEN_23 = _d_io_deq_ready_T_1 ? _sbState_T_11 : sbState; // @[SBA.scala 357:50 SBA.scala 358:15 SBA.scala 294:26]
  RHEA__Queue_171 d ( // @[Decoupled.scala 296:21]
    .rf_reset(d_rf_reset),
    .clock(d_clock),
    .reset(d_reset),
    .io_enq_ready(d_io_enq_ready),
    .io_enq_valid(d_io_enq_valid),
    .io_enq_bits_denied(d_io_enq_bits_denied),
    .io_enq_bits_data(d_io_enq_bits_data),
    .io_enq_bits_corrupt(d_io_enq_bits_corrupt),
    .io_deq_ready(d_io_deq_ready),
    .io_deq_valid(d_io_deq_valid),
    .io_deq_bits_denied(d_io_deq_bits_denied),
    .io_deq_bits_data(d_io_deq_bits_data),
    .io_deq_bits_corrupt(d_io_deq_bits_corrupt)
  );
  assign d_rf_reset = rf_reset;
  assign auto_out_a_valid = _T | _wrTxValid_T; // @[SBA.scala 365:52]
  assign auto_out_a_bits_opcode = _T ? 3'h4 : 3'h0; // @[SBA.scala 321:42 SBA.scala 321:54 SBA.scala 322:54]
  assign auto_out_a_bits_size = _T ? gbits_size : gbits_size; // @[SBA.scala 321:42 SBA.scala 321:54 SBA.scala 322:54]
  assign auto_out_a_bits_address = _T ? gbits_address : gbits_address; // @[SBA.scala 321:42 SBA.scala 321:54 SBA.scala 322:54]
  assign auto_out_a_bits_data = _T ? 8'h0 : muxedData; // @[SBA.scala 321:42 SBA.scala 321:54 SBA.scala 322:54]
  assign auto_out_d_ready = d_io_enq_ready; // @[SBA.scala 265:26 Decoupled.scala 299:17]
  assign io_rdLegal = _rdLegal_addr_T_8 | _rdLegal_addr_T_13 | _rdLegal_addr_T_18 | _rdLegal_addr_T_23 |
    _rdLegal_addr_T_28 | _rdLegal_addr_T_33 | _rdLegal_addr_T_38 | _rdLegal_addr_T_43 | _rdLegal_addr_T_48 |
    _rdLegal_addr_T_53; // @[Parameters.scala 686:42]
  assign io_wrLegal = _rdLegal_addr_T_8 | _rdLegal_addr_T_13 | _rdLegal_addr_T_18 | _rdLegal_addr_T_23 |
    _rdLegal_addr_T_28 | _rdLegal_addr_T_33 | _rdLegal_addr_T_38 | _rdLegal_addr_T_43 | _rdLegal_addr_T_48 |
    _rdLegal_addr_T_53; // @[Parameters.scala 686:42]
  assign io_rdDone = rdTxValid & txLast; // @[SBA.scala 361:29]
  assign io_wrDone = _d_io_deq_ready_T_1 & d_io_deq_valid & d_io_deq_ready; // @[SBA.scala 362:71]
  assign io_respError = d_io_deq_bits_denied | d_io_deq_bits_corrupt; // @[SBA.scala 334:35]
  assign io_dataOut = d_io_deq_bits_data; // @[SBA.scala 363:16]
  assign io_rdLoad_0 = rdTxValid & counter == 4'h0; // @[SBA.scala 344:33]
  assign io_rdLoad_1 = rdTxValid & counter == 4'h1; // @[SBA.scala 344:33]
  assign io_rdLoad_2 = rdTxValid & counter == 4'h2; // @[SBA.scala 344:33]
  assign io_rdLoad_3 = rdTxValid & counter == 4'h3; // @[SBA.scala 344:33]
  assign io_rdLoad_4 = rdTxValid & counter == 4'h4; // @[SBA.scala 344:33]
  assign io_rdLoad_5 = rdTxValid & counter == 4'h5; // @[SBA.scala 344:33]
  assign io_rdLoad_6 = rdTxValid & counter == 4'h6; // @[SBA.scala 344:33]
  assign io_rdLoad_7 = rdTxValid & counter == 4'h7; // @[SBA.scala 344:33]
  assign io_sbStateOut = sbState; // @[SBA.scala 320:19]
  assign d_clock = clock;
  assign d_reset = reset;
  assign d_io_enq_valid = auto_out_d_valid; // @[SBA.scala 265:26 LazyModule.scala 390:12]
  assign d_io_enq_bits_denied = auto_out_d_bits_denied; // @[SBA.scala 265:26 LazyModule.scala 390:12]
  assign d_io_enq_bits_data = auto_out_d_bits_data; // @[SBA.scala 265:26 LazyModule.scala 390:12]
  assign d_io_enq_bits_corrupt = auto_out_d_bits_corrupt; // @[SBA.scala 265:26 LazyModule.scala 390:12]
  assign d_io_deq_ready = _d_io_deq_ready_T | _d_io_deq_ready_T_1; // @[SBA.scala 298:50]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      sbState <= 3'h0;
    end else if (_T_1) begin
      if (io_rdEn & io_rdLegal) begin
        sbState <= 3'h1;
      end else if (io_wrEn & io_wrLegal) begin
        sbState <= 3'h2;
      end
    end else if (_T) begin
      if (tl_a_valid & auto_out_a_ready) begin
        sbState <= 3'h3;
      end
    end else if (_wrTxValid_T) begin
      if (wrTxValid & txLast) begin
        sbState <= 3'h4;
      end
    end else if (_d_io_deq_ready_T) begin
      sbState <= _sbState_T_9;
    end else begin
      sbState <= _GEN_23;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      counter <= 4'h0;
    end else if ((wrTxValid | rdTxValid) & txLast) begin
      counter <= 4'h0;
    end else if (_counter_T) begin
      counter <= _counter_T_4;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  sbState = _RAND_0[2:0];
  _RAND_1 = {1{`RANDOM}};
  counter = _RAND_1[3:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    sbState = 3'h0;
  end
  if (reset) begin
    counter = 4'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
