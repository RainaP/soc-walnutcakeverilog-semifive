//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__Sideband(
  input          rf_reset,
  input          clock,
  input          reset,
  output         io_a_ready,
  input          io_a_valid,
  input  [2:0]   io_a_bits_opcode,
  input  [2:0]   io_a_bits_param,
  input  [2:0]   io_a_bits_size,
  input  [9:0]   io_a_bits_source,
  input  [27:0]  io_a_bits_address,
  input  [15:0]  io_a_bits_mask,
  input  [127:0] io_a_bits_data,
  input          io_a_bits_corrupt,
  input          io_d_ready,
  output         io_d_valid,
  output [2:0]   io_d_bits_opcode,
  output [2:0]   io_d_bits_size,
  output [9:0]   io_d_bits_source,
  output [127:0] io_d_bits_data,
  output         io_d_bits_corrupt,
  input  [2:0]   io_threshold,
  output         io_bs_adr_0_valid,
  output [2:0]   io_bs_adr_0_bits_way,
  output [9:0]   io_bs_adr_0_bits_set,
  output [1:0]   io_bs_adr_0_bits_beat,
  output [1:0]   io_bs_adr_0_bits_mask,
  output         io_bs_adr_0_bits_write,
  output         io_bs_adr_1_valid,
  output [2:0]   io_bs_adr_1_bits_way,
  output [9:0]   io_bs_adr_1_bits_set,
  output [1:0]   io_bs_adr_1_bits_beat,
  output [1:0]   io_bs_adr_1_bits_mask,
  output         io_bs_adr_1_bits_write,
  output         io_bs_adr_2_valid,
  output [2:0]   io_bs_adr_2_bits_way,
  output [9:0]   io_bs_adr_2_bits_set,
  output [1:0]   io_bs_adr_2_bits_beat,
  output [1:0]   io_bs_adr_2_bits_mask,
  output         io_bs_adr_2_bits_write,
  output         io_bs_adr_3_valid,
  output [2:0]   io_bs_adr_3_bits_way,
  output [9:0]   io_bs_adr_3_bits_set,
  output [1:0]   io_bs_adr_3_bits_beat,
  output [1:0]   io_bs_adr_3_bits_mask,
  output         io_bs_adr_3_bits_write,
  output [127:0] io_bs_wdat_0_data,
  output         io_bs_wdat_0_poison,
  output [127:0] io_bs_wdat_1_data,
  output         io_bs_wdat_1_poison,
  output [127:0] io_bs_wdat_2_data,
  output         io_bs_wdat_2_poison,
  output [127:0] io_bs_wdat_3_data,
  output         io_bs_wdat_3_poison,
  input  [127:0] io_bs_rdat_0_data,
  input  [127:0] io_bs_rdat_1_data,
  input  [127:0] io_bs_rdat_2_data,
  input  [127:0] io_bs_rdat_3_data,
  output         io_uncorrected_valid,
  output [35:0]  io_uncorrected_bits
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [127:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [127:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [127:0] _RAND_30;
  reg [127:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [127:0] _RAND_39;
  reg [127:0] _RAND_40;
  reg [31:0] _RAND_41;
`endif // RANDOMIZE_REG_INIT
  wire  queue_rf_reset; // @[Sideband.scala 63:21]
  wire  queue_clock; // @[Sideband.scala 63:21]
  wire  queue_reset; // @[Sideband.scala 63:21]
  wire  queue_io_enq_ready; // @[Sideband.scala 63:21]
  wire  queue_io_enq_valid; // @[Sideband.scala 63:21]
  wire [2:0] queue_io_enq_bits_opcode; // @[Sideband.scala 63:21]
  wire [2:0] queue_io_enq_bits_size; // @[Sideband.scala 63:21]
  wire [9:0] queue_io_enq_bits_source; // @[Sideband.scala 63:21]
  wire [127:0] queue_io_enq_bits_data; // @[Sideband.scala 63:21]
  wire  queue_io_enq_bits_corrupt; // @[Sideband.scala 63:21]
  wire  queue_io_deq_ready; // @[Sideband.scala 63:21]
  wire  queue_io_deq_valid; // @[Sideband.scala 63:21]
  wire [2:0] queue_io_deq_bits_opcode; // @[Sideband.scala 63:21]
  wire [2:0] queue_io_deq_bits_size; // @[Sideband.scala 63:21]
  wire [9:0] queue_io_deq_bits_source; // @[Sideband.scala 63:21]
  wire [127:0] queue_io_deq_bits_data; // @[Sideband.scala 63:21]
  wire  queue_io_deq_bits_corrupt; // @[Sideband.scala 63:21]
  wire [2:0] queue_io_count; // @[Sideband.scala 63:21]
  wire [2:0] amo_io_a_opcode; // @[Sideband.scala 94:19]
  wire [2:0] amo_io_a_param; // @[Sideband.scala 94:19]
  wire [15:0] amo_io_a_mask; // @[Sideband.scala 94:19]
  wire [127:0] amo_io_a_data; // @[Sideband.scala 94:19]
  wire [127:0] amo_io_data_in; // @[Sideband.scala 94:19]
  wire [127:0] amo_io_data_out; // @[Sideband.scala 94:19]
  reg  odd; // @[Sideband.scala 83:20]
  reg [2:0] fill; // @[Sideband.scala 86:21]
  reg  room; // @[Sideband.scala 87:21]
  wire  _T = queue_io_enq_ready & queue_io_enq_valid; // @[Decoupled.scala 40:37]
  wire  _T_1 = queue_io_deq_ready & queue_io_deq_valid; // @[Decoupled.scala 40:37]
  wire [2:0] _fill_T_2 = _T ? 3'h1 : 3'h7; // @[Sideband.scala 89:23]
  wire [2:0] _fill_T_4 = fill + _fill_T_2; // @[Sideband.scala 89:18]
  wire  r1_valid = io_a_ready & io_a_valid; // @[Decoupled.scala 40:37]
  reg  r2_valid; // @[Sideband.scala 99:21]
  reg [2:0] r2_bits_opcode; // @[Reg.scala 15:16]
  reg [2:0] r2_bits_param; // @[Reg.scala 15:16]
  reg [2:0] r2_bits_size; // @[Reg.scala 15:16]
  reg [9:0] r2_bits_source; // @[Reg.scala 15:16]
  reg [27:0] r2_bits_address; // @[Reg.scala 15:16]
  reg [15:0] r2_bits_mask; // @[Reg.scala 15:16]
  reg [127:0] r2_bits_data; // @[Reg.scala 15:16]
  reg  r2_bits_corrupt; // @[Reg.scala 15:16]
  reg  r3_valid; // @[Sideband.scala 104:21]
  reg  r4_valid; // @[Sideband.scala 105:21]
  reg [2:0] r34_bits_opcode; // @[Reg.scala 15:16]
  reg [2:0] r34_bits_param; // @[Reg.scala 15:16]
  reg [2:0] r34_bits_size; // @[Reg.scala 15:16]
  reg [9:0] r34_bits_source; // @[Reg.scala 15:16]
  reg [27:0] r34_bits_address; // @[Reg.scala 15:16]
  reg [15:0] r34_bits_mask; // @[Reg.scala 15:16]
  reg [127:0] r34_bits_data; // @[Reg.scala 15:16]
  reg  r34_bits_corrupt; // @[Reg.scala 15:16]
  reg  r5_valid; // @[Sideband.scala 110:21]
  reg  r6_valid; // @[Sideband.scala 111:21]
  reg [2:0] r56_bits_opcode; // @[Reg.scala 15:16]
  reg [2:0] r56_bits_param; // @[Reg.scala 15:16]
  reg [2:0] r56_bits_size; // @[Reg.scala 15:16]
  reg [9:0] r56_bits_source; // @[Reg.scala 15:16]
  reg [27:0] r56_bits_address; // @[Reg.scala 15:16]
  reg [15:0] r56_bits_mask; // @[Reg.scala 15:16]
  reg [127:0] r56_bits_data; // @[Reg.scala 15:16]
  reg [127:0] r5_data_data; // @[Reg.scala 15:16]
  reg  r5_data_uncorrected; // @[Reg.scala 15:16]
  wire [23:0] tail0_1 = r34_bits_address[27:4]; // @[Sideband.scala 70:82]
  wire [21:0] tail1_1 = tail0_1[23:2]; // @[Sideband.scala 70:82]
  wire [19:0] tail2_1 = tail1_1[21:2]; // @[Sideband.scala 70:82]
  wire [9:0] tail3_1 = tail2_1[19:10]; // @[Sideband.scala 70:82]
  wire [2:0] r4_way = tail3_1[2:0]; // @[Sideband.scala 70:71]
  wire [2:0] _r4_oob_T = ~io_threshold; // @[Sideband.scala 186:26]
  wire  r4_oob = r4_way >= _r4_oob_T; // @[Sideband.scala 186:23]
  wire  r34_ignore_read = r34_bits_opcode == 3'h0 & r34_bits_size >= 3'h3; // @[Sideband.scala 141:57]
  wire  r6_bypass_opdata = ~r56_bits_opcode[2]; // @[Edges.scala 91:28]
  wire  _r6_bypass_T = r6_valid & r6_bypass_opdata; // @[Sideband.scala 180:28]
  wire [23:0] tail0_2 = r56_bits_address[27:4]; // @[Sideband.scala 70:82]
  wire [21:0] tail1_2 = tail0_2[23:2]; // @[Sideband.scala 70:82]
  wire [19:0] tail2_2 = tail1_2[21:2]; // @[Sideband.scala 70:82]
  wire [9:0] tail3_2 = tail2_2[19:10]; // @[Sideband.scala 70:82]
  wire [2:0] r6_way = tail3_2[2:0]; // @[Sideband.scala 70:71]
  wire [9:0] r6_set = tail2_2[9:0]; // @[Sideband.scala 70:71]
  wire [1:0] r6_bank = tail1_2[1:0]; // @[Sideband.scala 70:71]
  wire [1:0] r6_beat = tail0_2[1:0]; // @[Sideband.scala 70:71]
  wire [16:0] r6_key = {r6_way,r6_set,r6_bank,r6_beat}; // @[Cat.scala 30:58]
  wire [9:0] r4_set = tail2_1[9:0]; // @[Sideband.scala 70:71]
  wire [1:0] r4_bank = tail1_1[1:0]; // @[Sideband.scala 70:71]
  wire [1:0] r4_beat = tail0_1[1:0]; // @[Sideband.scala 70:71]
  wire [16:0] r4_key = {r4_way,r4_set,r4_bank,r4_beat}; // @[Cat.scala 30:58]
  wire  r6_bypass = r6_valid & r6_bypass_opdata & r6_key == r4_key; // @[Sideband.scala 180:54]
  wire [15:0] _r6_byte_overlap_T = r56_bits_mask & r34_bits_mask; // @[Sideband.scala 184:40]
  wire  r6_byte_overlap = |_r6_byte_overlap_T; // @[Sideband.scala 184:57]
  reg  r6_data_uncorrected; // @[Reg.scala 15:16]
  wire [15:0] _r6_byte_satisfy_T = ~r34_bits_mask; // @[Sideband.scala 182:42]
  wire [15:0] _r6_byte_satisfy_T_1 = r56_bits_mask | _r6_byte_satisfy_T; // @[Sideband.scala 182:40]
  wire  r6_byte_satisfy = &_r6_byte_satisfy_T_1; // @[Sideband.scala 182:58]
  reg  r8_valid; // @[Sideband.scala 118:21]
  reg [2:0] r78_bits_opcode; // @[Reg.scala 15:16]
  wire  r8_bypass_opdata = ~r78_bits_opcode[2]; // @[Edges.scala 91:28]
  reg [27:0] r78_bits_address; // @[Reg.scala 15:16]
  wire [23:0] tail0_3 = r78_bits_address[27:4]; // @[Sideband.scala 70:82]
  wire [21:0] tail1_3 = tail0_3[23:2]; // @[Sideband.scala 70:82]
  wire [19:0] tail2_3 = tail1_3[21:2]; // @[Sideband.scala 70:82]
  wire [9:0] tail3_3 = tail2_3[19:10]; // @[Sideband.scala 70:82]
  wire [2:0] r8_way = tail3_3[2:0]; // @[Sideband.scala 70:71]
  wire [9:0] r8_set = tail2_3[9:0]; // @[Sideband.scala 70:71]
  wire [1:0] r8_bank = tail1_3[1:0]; // @[Sideband.scala 70:71]
  wire [1:0] r8_beat = tail0_3[1:0]; // @[Sideband.scala 70:71]
  wire [16:0] r8_key = {r8_way,r8_set,r8_bank,r8_beat}; // @[Cat.scala 30:58]
  wire  r8_bypass = r8_valid & r8_bypass_opdata & r8_key == r4_key; // @[Sideband.scala 181:54]
  reg [15:0] r78_bits_mask; // @[Reg.scala 15:16]
  wire [15:0] _r8_byte_overlap_T = r78_bits_mask & r34_bits_mask; // @[Sideband.scala 185:40]
  wire  r8_byte_overlap = |_r8_byte_overlap_T; // @[Sideband.scala 185:57]
  reg  r78_data_uncorrected; // @[Reg.scala 15:16]
  wire  _r4_data_uncorrected_T_8 = r8_bypass & r8_byte_overlap & r78_data_uncorrected; // @[Sideband.scala 196:35]
  wire  _r4_data_uncorrected_T_14 = r6_bypass & r6_byte_overlap & r6_data_uncorrected | ~(r6_bypass & r6_byte_satisfy)
     & _r4_data_uncorrected_T_8; // @[Sideband.scala 195:60]
  wire  r4_data_uncorrected = r34_bits_corrupt | ~r4_oob & ~r34_ignore_read & _r4_data_uncorrected_T_14; // @[Sideband.scala 194:43]
  reg [127:0] r6_data_data; // @[Reg.scala 15:16]
  wire [7:0] r4_data_data_dBytes_15_1 = r6_data_data[127:120]; // @[Sideband.scala 171:54]
  reg [127:0] r78_data_data; // @[Reg.scala 15:16]
  wire [7:0] r4_data_data_dBytes_15 = r78_data_data[127:120]; // @[Sideband.scala 171:54]
  wire [127:0] _GEN_47 = 2'h1 == r4_bank ? io_bs_rdat_1_data : io_bs_rdat_0_data; // @[Sideband.scala 172:54 Sideband.scala 172:54]
  wire [127:0] _GEN_50 = 2'h2 == r4_bank ? io_bs_rdat_2_data : _GEN_47; // @[Sideband.scala 172:54 Sideband.scala 172:54]
  wire [127:0] _GEN_53 = 2'h3 == r4_bank ? io_bs_rdat_3_data : _GEN_50; // @[Sideband.scala 172:54 Sideband.scala 172:54]
  wire [7:0] r4_data_data_oBytes_15 = _GEN_53[127:120]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_15 = r8_bypass & r78_bits_mask[15] ? r4_data_data_dBytes_15 : r4_data_data_oBytes_15; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_14 = r78_data_data[119:112]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_14 = _GEN_53[119:112]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_14 = r8_bypass & r78_bits_mask[14] ? r4_data_data_dBytes_14 : r4_data_data_oBytes_14; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_13 = r78_data_data[111:104]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_13 = _GEN_53[111:104]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_13 = r8_bypass & r78_bits_mask[13] ? r4_data_data_dBytes_13 : r4_data_data_oBytes_13; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_12 = r78_data_data[103:96]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_12 = _GEN_53[103:96]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_12 = r8_bypass & r78_bits_mask[12] ? r4_data_data_dBytes_12 : r4_data_data_oBytes_12; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_11 = r78_data_data[95:88]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_11 = _GEN_53[95:88]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_11 = r8_bypass & r78_bits_mask[11] ? r4_data_data_dBytes_11 : r4_data_data_oBytes_11; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_10 = r78_data_data[87:80]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_10 = _GEN_53[87:80]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_10 = r8_bypass & r78_bits_mask[10] ? r4_data_data_dBytes_10 : r4_data_data_oBytes_10; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_9 = r78_data_data[79:72]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_9 = _GEN_53[79:72]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_9 = r8_bypass & r78_bits_mask[9] ? r4_data_data_dBytes_9 : r4_data_data_oBytes_9; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_8 = r78_data_data[71:64]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_8 = _GEN_53[71:64]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_8 = r8_bypass & r78_bits_mask[8] ? r4_data_data_dBytes_8 : r4_data_data_oBytes_8; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_7 = r78_data_data[63:56]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_7 = _GEN_53[63:56]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_7 = r8_bypass & r78_bits_mask[7] ? r4_data_data_dBytes_7 : r4_data_data_oBytes_7; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_6 = r78_data_data[55:48]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_6 = _GEN_53[55:48]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_6 = r8_bypass & r78_bits_mask[6] ? r4_data_data_dBytes_6 : r4_data_data_oBytes_6; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_5 = r78_data_data[47:40]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_5 = _GEN_53[47:40]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_5 = r8_bypass & r78_bits_mask[5] ? r4_data_data_dBytes_5 : r4_data_data_oBytes_5; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_4 = r78_data_data[39:32]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_4 = _GEN_53[39:32]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_4 = r8_bypass & r78_bits_mask[4] ? r4_data_data_dBytes_4 : r4_data_data_oBytes_4; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_3 = r78_data_data[31:24]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_3 = _GEN_53[31:24]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_3 = r8_bypass & r78_bits_mask[3] ? r4_data_data_dBytes_3 : r4_data_data_oBytes_3; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_2 = r78_data_data[23:16]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_2 = _GEN_53[23:16]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_2 = r8_bypass & r78_bits_mask[2] ? r4_data_data_dBytes_2 : r4_data_data_oBytes_2; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_1 = r78_data_data[15:8]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_1 = _GEN_53[15:8]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_1 = r8_bypass & r78_bits_mask[1] ? r4_data_data_dBytes_1 : r4_data_data_oBytes_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_0 = r78_data_data[7:0]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_0 = _GEN_53[7:0]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_0 = r8_bypass & r78_bits_mask[0] ? r4_data_data_dBytes_0 : r4_data_data_oBytes_0; // @[Sideband.scala 174:10]
  wire [63:0] r4_data_data_lo = {r4_data_data_rBytes_7,r4_data_data_rBytes_6,r4_data_data_rBytes_5,r4_data_data_rBytes_4
    ,r4_data_data_rBytes_3,r4_data_data_rBytes_2,r4_data_data_rBytes_1,r4_data_data_rBytes_0}; // @[Sideband.scala 176:17]
  wire [127:0] _r4_data_data_T = {r4_data_data_rBytes_15,r4_data_data_rBytes_14,r4_data_data_rBytes_13,
    r4_data_data_rBytes_12,r4_data_data_rBytes_11,r4_data_data_rBytes_10,r4_data_data_rBytes_9,r4_data_data_rBytes_8,
    r4_data_data_lo}; // @[Sideband.scala 176:17]
  wire [7:0] r4_data_data_oBytes_15_1 = _r4_data_data_T[127:120]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_15_1 = r6_bypass & r56_bits_mask[15] ? r4_data_data_dBytes_15_1 :
    r4_data_data_oBytes_15_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_14_1 = r6_data_data[119:112]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_14_1 = _r4_data_data_T[119:112]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_14_1 = r6_bypass & r56_bits_mask[14] ? r4_data_data_dBytes_14_1 :
    r4_data_data_oBytes_14_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_13_1 = r6_data_data[111:104]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_13_1 = _r4_data_data_T[111:104]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_13_1 = r6_bypass & r56_bits_mask[13] ? r4_data_data_dBytes_13_1 :
    r4_data_data_oBytes_13_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_12_1 = r6_data_data[103:96]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_12_1 = _r4_data_data_T[103:96]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_12_1 = r6_bypass & r56_bits_mask[12] ? r4_data_data_dBytes_12_1 :
    r4_data_data_oBytes_12_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_11_1 = r6_data_data[95:88]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_11_1 = _r4_data_data_T[95:88]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_11_1 = r6_bypass & r56_bits_mask[11] ? r4_data_data_dBytes_11_1 :
    r4_data_data_oBytes_11_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_10_1 = r6_data_data[87:80]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_10_1 = _r4_data_data_T[87:80]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_10_1 = r6_bypass & r56_bits_mask[10] ? r4_data_data_dBytes_10_1 :
    r4_data_data_oBytes_10_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_9_1 = r6_data_data[79:72]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_9_1 = _r4_data_data_T[79:72]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_9_1 = r6_bypass & r56_bits_mask[9] ? r4_data_data_dBytes_9_1 : r4_data_data_oBytes_9_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_8_1 = r6_data_data[71:64]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_8_1 = _r4_data_data_T[71:64]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_8_1 = r6_bypass & r56_bits_mask[8] ? r4_data_data_dBytes_8_1 : r4_data_data_oBytes_8_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_7_1 = r6_data_data[63:56]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_7_1 = _r4_data_data_T[63:56]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_7_1 = r6_bypass & r56_bits_mask[7] ? r4_data_data_dBytes_7_1 : r4_data_data_oBytes_7_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_6_1 = r6_data_data[55:48]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_6_1 = _r4_data_data_T[55:48]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_6_1 = r6_bypass & r56_bits_mask[6] ? r4_data_data_dBytes_6_1 : r4_data_data_oBytes_6_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_5_1 = r6_data_data[47:40]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_5_1 = _r4_data_data_T[47:40]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_5_1 = r6_bypass & r56_bits_mask[5] ? r4_data_data_dBytes_5_1 : r4_data_data_oBytes_5_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_4_1 = r6_data_data[39:32]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_4_1 = _r4_data_data_T[39:32]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_4_1 = r6_bypass & r56_bits_mask[4] ? r4_data_data_dBytes_4_1 : r4_data_data_oBytes_4_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_3_1 = r6_data_data[31:24]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_3_1 = _r4_data_data_T[31:24]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_3_1 = r6_bypass & r56_bits_mask[3] ? r4_data_data_dBytes_3_1 : r4_data_data_oBytes_3_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_2_1 = r6_data_data[23:16]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_2_1 = _r4_data_data_T[23:16]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_2_1 = r6_bypass & r56_bits_mask[2] ? r4_data_data_dBytes_2_1 : r4_data_data_oBytes_2_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_1_1 = r6_data_data[15:8]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_1_1 = _r4_data_data_T[15:8]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_1_1 = r6_bypass & r56_bits_mask[1] ? r4_data_data_dBytes_1_1 : r4_data_data_oBytes_1_1; // @[Sideband.scala 174:10]
  wire [7:0] r4_data_data_dBytes_0_1 = r6_data_data[7:0]; // @[Sideband.scala 171:54]
  wire [7:0] r4_data_data_oBytes_0_1 = _r4_data_data_T[7:0]; // @[Sideband.scala 172:54]
  wire [7:0] r4_data_data_rBytes_0_1 = r6_bypass & r56_bits_mask[0] ? r4_data_data_dBytes_0_1 : r4_data_data_oBytes_0_1; // @[Sideband.scala 174:10]
  wire [63:0] r4_data_data_lo_1 = {r4_data_data_rBytes_7_1,r4_data_data_rBytes_6_1,r4_data_data_rBytes_5_1,
    r4_data_data_rBytes_4_1,r4_data_data_rBytes_3_1,r4_data_data_rBytes_2_1,r4_data_data_rBytes_1_1,
    r4_data_data_rBytes_0_1}; // @[Sideband.scala 176:17]
  wire [127:0] _r4_data_data_T_1 = {r4_data_data_rBytes_15_1,r4_data_data_rBytes_14_1,r4_data_data_rBytes_13_1,
    r4_data_data_rBytes_12_1,r4_data_data_rBytes_11_1,r4_data_data_rBytes_10_1,r4_data_data_rBytes_9_1,
    r4_data_data_rBytes_8_1,r4_data_data_lo_1}; // @[Sideband.scala 176:17]
  reg  r7_valid; // @[Sideband.scala 117:21]
  wire  _T_10 = r1_valid | r3_valid | r5_valid | r7_valid; // @[Sideband.scala 123:46]
  wire [15:0] mask = r6_valid ? r56_bits_mask : io_a_bits_mask; // @[Sideband.scala 129:17]
  wire [27:0] addr = r6_valid ? r56_bits_address : io_a_bits_address; // @[Sideband.scala 130:17]
  wire [23:0] tail0 = addr[27:4]; // @[Sideband.scala 70:82]
  wire [21:0] tail1 = tail0[23:2]; // @[Sideband.scala 70:82]
  wire [19:0] tail2 = tail1[21:2]; // @[Sideband.scala 70:82]
  wire [9:0] tail3 = tail2[19:10]; // @[Sideband.scala 70:82]
  wire [2:0] way = tail3[2:0]; // @[Sideband.scala 70:71]
  wire  en = way < _r4_oob_T; // @[Sideband.scala 133:20]
  wire [23:0] r1_sel_tail0 = io_a_bits_address[27:4]; // @[Sideband.scala 70:82]
  wire [21:0] r1_sel_tail1 = r1_sel_tail0[23:2]; // @[Sideband.scala 70:82]
  wire [1:0] r1_sel_bank = r1_sel_tail1[1:0]; // @[Sideband.scala 70:71]
  wire [3:0] r1_sel = 4'h1 << r1_sel_bank; // @[OneHot.scala 65:12]
  wire [3:0] r6_sel = 4'h1 << r6_bank; // @[OneHot.scala 65:12]
  wire  io_bs_adr_0_bits_mask_lo = mask[0] | mask[1] | mask[2] | mask[3] | mask[4] | mask[5] | mask[6] | mask[7]; // @[Sideband.scala 149:85]
  wire  io_bs_adr_0_bits_mask_hi = mask[8] | mask[9] | mask[10] | mask[11] | mask[12] | mask[13] | mask[14] | mask[15]; // @[Sideband.scala 149:85]
  wire  _GEN_60 = 3'h4 == r56_bits_opcode | (3'h3 == r56_bits_opcode | 3'h2 == r56_bits_opcode); // @[Sideband.scala 210:18 Sideband.scala 210:18]
  wire  queue_io_enq_bits_corrupt_opdata = queue_io_enq_bits_opcode[0]; // @[Edges.scala 105:36]
  RHEA__QueueCompatibility_170 queue ( // @[Sideband.scala 63:21]
    .rf_reset(queue_rf_reset),
    .clock(queue_clock),
    .reset(queue_reset),
    .io_enq_ready(queue_io_enq_ready),
    .io_enq_valid(queue_io_enq_valid),
    .io_enq_bits_opcode(queue_io_enq_bits_opcode),
    .io_enq_bits_size(queue_io_enq_bits_size),
    .io_enq_bits_source(queue_io_enq_bits_source),
    .io_enq_bits_data(queue_io_enq_bits_data),
    .io_enq_bits_corrupt(queue_io_enq_bits_corrupt),
    .io_deq_ready(queue_io_deq_ready),
    .io_deq_valid(queue_io_deq_valid),
    .io_deq_bits_opcode(queue_io_deq_bits_opcode),
    .io_deq_bits_size(queue_io_deq_bits_size),
    .io_deq_bits_source(queue_io_deq_bits_source),
    .io_deq_bits_data(queue_io_deq_bits_data),
    .io_deq_bits_corrupt(queue_io_deq_bits_corrupt),
    .io_count(queue_io_count)
  );
  RHEA__Atomics_4 amo ( // @[Sideband.scala 94:19]
    .io_a_opcode(amo_io_a_opcode),
    .io_a_param(amo_io_a_param),
    .io_a_mask(amo_io_a_mask),
    .io_a_data(amo_io_a_data),
    .io_data_in(amo_io_data_in),
    .io_data_out(amo_io_data_out)
  );
  assign queue_rf_reset = rf_reset;
  assign io_a_ready = room & odd; // @[Sideband.scala 97:22]
  assign io_d_valid = queue_io_deq_valid; // @[Sideband.scala 65:8]
  assign io_d_bits_opcode = queue_io_deq_bits_opcode; // @[Sideband.scala 65:8]
  assign io_d_bits_size = queue_io_deq_bits_size; // @[Sideband.scala 65:8]
  assign io_d_bits_source = queue_io_deq_bits_source; // @[Sideband.scala 65:8]
  assign io_d_bits_data = queue_io_deq_bits_data; // @[Sideband.scala 65:8]
  assign io_d_bits_corrupt = queue_io_deq_bits_corrupt; // @[Sideband.scala 65:8]
  assign io_bs_adr_0_valid = en & (r1_valid & r1_sel[0] | _r6_bypass_T & r6_sel[0]); // @[Sideband.scala 144:24]
  assign io_bs_adr_0_bits_way = ~way; // @[Sideband.scala 146:21]
  assign io_bs_adr_0_bits_set = tail2[9:0]; // @[Sideband.scala 70:71]
  assign io_bs_adr_0_bits_beat = tail0[1:0]; // @[Sideband.scala 70:71]
  assign io_bs_adr_0_bits_mask = {io_bs_adr_0_bits_mask_hi,io_bs_adr_0_bits_mask_lo}; // @[Cat.scala 30:58]
  assign io_bs_adr_0_bits_write = ~odd; // @[Sideband.scala 150:21]
  assign io_bs_adr_1_valid = en & (r1_valid & r1_sel[1] | _r6_bypass_T & r6_sel[1]); // @[Sideband.scala 144:24]
  assign io_bs_adr_1_bits_way = ~way; // @[Sideband.scala 146:21]
  assign io_bs_adr_1_bits_set = tail2[9:0]; // @[Sideband.scala 70:71]
  assign io_bs_adr_1_bits_beat = tail0[1:0]; // @[Sideband.scala 70:71]
  assign io_bs_adr_1_bits_mask = {io_bs_adr_0_bits_mask_hi,io_bs_adr_0_bits_mask_lo}; // @[Cat.scala 30:58]
  assign io_bs_adr_1_bits_write = ~odd; // @[Sideband.scala 150:21]
  assign io_bs_adr_2_valid = en & (r1_valid & r1_sel[2] | _r6_bypass_T & r6_sel[2]); // @[Sideband.scala 144:24]
  assign io_bs_adr_2_bits_way = ~way; // @[Sideband.scala 146:21]
  assign io_bs_adr_2_bits_set = tail2[9:0]; // @[Sideband.scala 70:71]
  assign io_bs_adr_2_bits_beat = tail0[1:0]; // @[Sideband.scala 70:71]
  assign io_bs_adr_2_bits_mask = {io_bs_adr_0_bits_mask_hi,io_bs_adr_0_bits_mask_lo}; // @[Cat.scala 30:58]
  assign io_bs_adr_2_bits_write = ~odd; // @[Sideband.scala 150:21]
  assign io_bs_adr_3_valid = en & (r1_valid & r1_sel[3] | _r6_bypass_T & r6_sel[3]); // @[Sideband.scala 144:24]
  assign io_bs_adr_3_bits_way = ~way; // @[Sideband.scala 146:21]
  assign io_bs_adr_3_bits_set = tail2[9:0]; // @[Sideband.scala 70:71]
  assign io_bs_adr_3_bits_beat = tail0[1:0]; // @[Sideband.scala 70:71]
  assign io_bs_adr_3_bits_mask = {io_bs_adr_0_bits_mask_hi,io_bs_adr_0_bits_mask_lo}; // @[Cat.scala 30:58]
  assign io_bs_adr_3_bits_write = ~odd; // @[Sideband.scala 150:21]
  assign io_bs_wdat_0_data = r6_data_data; // @[Sideband.scala 153:12]
  assign io_bs_wdat_0_poison = r6_data_uncorrected; // @[Sideband.scala 154:14]
  assign io_bs_wdat_1_data = r6_data_data; // @[Sideband.scala 153:12]
  assign io_bs_wdat_1_poison = r6_data_uncorrected; // @[Sideband.scala 154:14]
  assign io_bs_wdat_2_data = r6_data_data; // @[Sideband.scala 153:12]
  assign io_bs_wdat_2_poison = r6_data_uncorrected; // @[Sideband.scala 154:14]
  assign io_bs_wdat_3_data = r6_data_data; // @[Sideband.scala 153:12]
  assign io_bs_wdat_3_poison = r6_data_uncorrected; // @[Sideband.scala 154:14]
  assign io_uncorrected_valid = r5_valid & r5_data_uncorrected; // @[Sideband.scala 221:36]
  assign io_uncorrected_bits = {{8'd0}, r56_bits_address}; // @[Sideband.scala 222:24]
  assign queue_clock = clock;
  assign queue_reset = reset;
  assign queue_io_enq_valid = r5_valid; // @[Sideband.scala 209:11]
  assign queue_io_enq_bits_opcode = {{2'd0}, _GEN_60}; // @[Sideband.scala 210:18 Sideband.scala 210:18]
  assign queue_io_enq_bits_size = r56_bits_size; // @[Sideband.scala 212:18]
  assign queue_io_enq_bits_source = r56_bits_source; // @[Sideband.scala 213:18]
  assign queue_io_enq_bits_data = r5_data_data; // @[Sideband.scala 216:18]
  assign queue_io_enq_bits_corrupt = r5_data_uncorrected & queue_io_enq_bits_corrupt_opdata; // @[Sideband.scala 217:41]
  assign queue_io_deq_ready = io_d_ready; // @[Sideband.scala 65:8]
  assign amo_io_a_opcode = r56_bits_opcode; // @[Sideband.scala 158:18]
  assign amo_io_a_param = r56_bits_param; // @[Sideband.scala 158:18]
  assign amo_io_a_mask = r56_bits_mask; // @[Sideband.scala 158:18]
  assign amo_io_a_data = r56_bits_data; // @[Sideband.scala 158:18]
  assign amo_io_data_in = r5_data_data; // @[Sideband.scala 159:18]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      odd <= 1'h1;
    end else begin
      odd <= ~_T_10;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fill <= 3'h0;
    end else if (_T != _T_1) begin
      fill <= _fill_T_4;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      room <= 1'h1;
    end else if (_T != _T_1) begin
      room <= fill == 3'h0 | (fill == 3'h1 | fill == 3'h2) & ~_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r2_valid <= 1'h0;
    end else begin
      r2_valid <= io_a_ready & io_a_valid;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r2_bits_opcode <= 3'h0;
    end else if (r1_valid) begin
      r2_bits_opcode <= io_a_bits_opcode;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r2_bits_param <= 3'h0;
    end else if (r1_valid) begin
      r2_bits_param <= io_a_bits_param;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r2_bits_size <= 3'h0;
    end else if (r1_valid) begin
      r2_bits_size <= io_a_bits_size;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r2_bits_source <= 10'h0;
    end else if (r1_valid) begin
      r2_bits_source <= io_a_bits_source;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r2_bits_address <= 28'h0;
    end else if (r1_valid) begin
      r2_bits_address <= io_a_bits_address;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r2_bits_mask <= 16'h0;
    end else if (r1_valid) begin
      r2_bits_mask <= io_a_bits_mask;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r2_bits_data <= 128'h0;
    end else if (r1_valid) begin
      r2_bits_data <= io_a_bits_data;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r2_bits_corrupt <= 1'h0;
    end else if (r1_valid) begin
      r2_bits_corrupt <= io_a_bits_corrupt;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r3_valid <= 1'h0;
    end else begin
      r3_valid <= r2_valid;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r4_valid <= 1'h0;
    end else begin
      r4_valid <= r3_valid;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r34_bits_opcode <= 3'h0;
    end else if (r2_valid) begin
      r34_bits_opcode <= r2_bits_opcode;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r34_bits_param <= 3'h0;
    end else if (r2_valid) begin
      r34_bits_param <= r2_bits_param;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r34_bits_size <= 3'h0;
    end else if (r2_valid) begin
      r34_bits_size <= r2_bits_size;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r34_bits_source <= 10'h0;
    end else if (r2_valid) begin
      r34_bits_source <= r2_bits_source;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r34_bits_address <= 28'h0;
    end else if (r2_valid) begin
      r34_bits_address <= r2_bits_address;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r34_bits_mask <= 16'h0;
    end else if (r2_valid) begin
      r34_bits_mask <= r2_bits_mask;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r34_bits_data <= 128'h0;
    end else if (r2_valid) begin
      r34_bits_data <= r2_bits_data;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r34_bits_corrupt <= 1'h0;
    end else if (r2_valid) begin
      r34_bits_corrupt <= r2_bits_corrupt;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r5_valid <= 1'h0;
    end else begin
      r5_valid <= r4_valid;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r6_valid <= 1'h0;
    end else begin
      r6_valid <= r5_valid;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r56_bits_opcode <= 3'h0;
    end else if (r4_valid) begin
      r56_bits_opcode <= r34_bits_opcode;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r56_bits_param <= 3'h0;
    end else if (r4_valid) begin
      r56_bits_param <= r34_bits_param;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r56_bits_size <= 3'h0;
    end else if (r4_valid) begin
      r56_bits_size <= r34_bits_size;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r56_bits_source <= 10'h0;
    end else if (r4_valid) begin
      r56_bits_source <= r34_bits_source;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r56_bits_address <= 28'h0;
    end else if (r4_valid) begin
      r56_bits_address <= r34_bits_address;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r56_bits_mask <= 16'h0;
    end else if (r4_valid) begin
      r56_bits_mask <= r34_bits_mask;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r56_bits_data <= 128'h0;
    end else if (r4_valid) begin
      r56_bits_data <= r34_bits_data;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r5_data_data <= 128'h0;
    end else if (r4_valid) begin
      if (r4_oob) begin
        r5_data_data <= 128'h0;
      end else begin
        r5_data_data <= _r4_data_data_T_1;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r5_data_uncorrected <= 1'h0;
    end else if (r4_valid) begin
      r5_data_uncorrected <= r4_data_uncorrected;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r6_data_uncorrected <= 1'h0;
    end else if (r5_valid) begin
      r6_data_uncorrected <= r5_data_uncorrected;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r8_valid <= 1'h0;
    end else begin
      r8_valid <= r7_valid;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r78_bits_opcode <= 3'h0;
    end else if (r6_valid) begin
      r78_bits_opcode <= r56_bits_opcode;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r78_bits_address <= 28'h0;
    end else if (r6_valid) begin
      r78_bits_address <= r56_bits_address;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r78_bits_mask <= 16'h0;
    end else if (r6_valid) begin
      r78_bits_mask <= r56_bits_mask;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r78_data_uncorrected <= 1'h0;
    end else if (r6_valid) begin
      r78_data_uncorrected <= r6_data_uncorrected;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r6_data_data <= 128'h0;
    end else if (r5_valid) begin
      r6_data_data <= amo_io_data_out;
    end else if (r5_valid) begin
      r6_data_data <= r5_data_data;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r78_data_data <= 128'h0;
    end else if (r6_valid) begin
      r78_data_data <= r6_data_data;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r7_valid <= 1'h0;
    end else begin
      r7_valid <= r6_valid;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  odd = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  fill = _RAND_1[2:0];
  _RAND_2 = {1{`RANDOM}};
  room = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  r2_valid = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  r2_bits_opcode = _RAND_4[2:0];
  _RAND_5 = {1{`RANDOM}};
  r2_bits_param = _RAND_5[2:0];
  _RAND_6 = {1{`RANDOM}};
  r2_bits_size = _RAND_6[2:0];
  _RAND_7 = {1{`RANDOM}};
  r2_bits_source = _RAND_7[9:0];
  _RAND_8 = {1{`RANDOM}};
  r2_bits_address = _RAND_8[27:0];
  _RAND_9 = {1{`RANDOM}};
  r2_bits_mask = _RAND_9[15:0];
  _RAND_10 = {4{`RANDOM}};
  r2_bits_data = _RAND_10[127:0];
  _RAND_11 = {1{`RANDOM}};
  r2_bits_corrupt = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  r3_valid = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  r4_valid = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  r34_bits_opcode = _RAND_14[2:0];
  _RAND_15 = {1{`RANDOM}};
  r34_bits_param = _RAND_15[2:0];
  _RAND_16 = {1{`RANDOM}};
  r34_bits_size = _RAND_16[2:0];
  _RAND_17 = {1{`RANDOM}};
  r34_bits_source = _RAND_17[9:0];
  _RAND_18 = {1{`RANDOM}};
  r34_bits_address = _RAND_18[27:0];
  _RAND_19 = {1{`RANDOM}};
  r34_bits_mask = _RAND_19[15:0];
  _RAND_20 = {4{`RANDOM}};
  r34_bits_data = _RAND_20[127:0];
  _RAND_21 = {1{`RANDOM}};
  r34_bits_corrupt = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  r5_valid = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  r6_valid = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  r56_bits_opcode = _RAND_24[2:0];
  _RAND_25 = {1{`RANDOM}};
  r56_bits_param = _RAND_25[2:0];
  _RAND_26 = {1{`RANDOM}};
  r56_bits_size = _RAND_26[2:0];
  _RAND_27 = {1{`RANDOM}};
  r56_bits_source = _RAND_27[9:0];
  _RAND_28 = {1{`RANDOM}};
  r56_bits_address = _RAND_28[27:0];
  _RAND_29 = {1{`RANDOM}};
  r56_bits_mask = _RAND_29[15:0];
  _RAND_30 = {4{`RANDOM}};
  r56_bits_data = _RAND_30[127:0];
  _RAND_31 = {4{`RANDOM}};
  r5_data_data = _RAND_31[127:0];
  _RAND_32 = {1{`RANDOM}};
  r5_data_uncorrected = _RAND_32[0:0];
  _RAND_33 = {1{`RANDOM}};
  r6_data_uncorrected = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  r8_valid = _RAND_34[0:0];
  _RAND_35 = {1{`RANDOM}};
  r78_bits_opcode = _RAND_35[2:0];
  _RAND_36 = {1{`RANDOM}};
  r78_bits_address = _RAND_36[27:0];
  _RAND_37 = {1{`RANDOM}};
  r78_bits_mask = _RAND_37[15:0];
  _RAND_38 = {1{`RANDOM}};
  r78_data_uncorrected = _RAND_38[0:0];
  _RAND_39 = {4{`RANDOM}};
  r6_data_data = _RAND_39[127:0];
  _RAND_40 = {4{`RANDOM}};
  r78_data_data = _RAND_40[127:0];
  _RAND_41 = {1{`RANDOM}};
  r7_valid = _RAND_41[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    odd = 1'h1;
  end
  if (reset) begin
    fill = 3'h0;
  end
  if (reset) begin
    room = 1'h1;
  end
  if (reset) begin
    r2_valid = 1'h0;
  end
  if (rf_reset) begin
    r2_bits_opcode = 3'h0;
  end
  if (rf_reset) begin
    r2_bits_param = 3'h0;
  end
  if (rf_reset) begin
    r2_bits_size = 3'h0;
  end
  if (rf_reset) begin
    r2_bits_source = 10'h0;
  end
  if (rf_reset) begin
    r2_bits_address = 28'h0;
  end
  if (rf_reset) begin
    r2_bits_mask = 16'h0;
  end
  if (rf_reset) begin
    r2_bits_data = 128'h0;
  end
  if (rf_reset) begin
    r2_bits_corrupt = 1'h0;
  end
  if (reset) begin
    r3_valid = 1'h0;
  end
  if (reset) begin
    r4_valid = 1'h0;
  end
  if (rf_reset) begin
    r34_bits_opcode = 3'h0;
  end
  if (rf_reset) begin
    r34_bits_param = 3'h0;
  end
  if (rf_reset) begin
    r34_bits_size = 3'h0;
  end
  if (rf_reset) begin
    r34_bits_source = 10'h0;
  end
  if (rf_reset) begin
    r34_bits_address = 28'h0;
  end
  if (rf_reset) begin
    r34_bits_mask = 16'h0;
  end
  if (rf_reset) begin
    r34_bits_data = 128'h0;
  end
  if (rf_reset) begin
    r34_bits_corrupt = 1'h0;
  end
  if (reset) begin
    r5_valid = 1'h0;
  end
  if (reset) begin
    r6_valid = 1'h0;
  end
  if (rf_reset) begin
    r56_bits_opcode = 3'h0;
  end
  if (rf_reset) begin
    r56_bits_param = 3'h0;
  end
  if (rf_reset) begin
    r56_bits_size = 3'h0;
  end
  if (rf_reset) begin
    r56_bits_source = 10'h0;
  end
  if (rf_reset) begin
    r56_bits_address = 28'h0;
  end
  if (rf_reset) begin
    r56_bits_mask = 16'h0;
  end
  if (rf_reset) begin
    r56_bits_data = 128'h0;
  end
  if (rf_reset) begin
    r5_data_data = 128'h0;
  end
  if (rf_reset) begin
    r5_data_uncorrected = 1'h0;
  end
  if (rf_reset) begin
    r6_data_uncorrected = 1'h0;
  end
  if (reset) begin
    r8_valid = 1'h0;
  end
  if (rf_reset) begin
    r78_bits_opcode = 3'h0;
  end
  if (rf_reset) begin
    r78_bits_address = 28'h0;
  end
  if (rf_reset) begin
    r78_bits_mask = 16'h0;
  end
  if (rf_reset) begin
    r78_data_uncorrected = 1'h0;
  end
  if (rf_reset) begin
    r6_data_data = 128'h0;
  end
  if (rf_reset) begin
    r78_data_data = 128'h0;
  end
  if (reset) begin
    r7_valid = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
