//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_61_assert(
  input        clock,
  input        reset,
  input        io_in_a_ready,
  input        io_in_a_valid,
  input  [2:0] io_in_a_bits_opcode,
  input  [8:0] io_in_a_bits_address,
  input  [3:0] io_in_a_bits_mask,
  input        io_in_d_ready,
  input        io_in_d_valid,
  input  [2:0] io_in_d_bits_opcode,
  input  [1:0] io_in_d_bits_param,
  input  [1:0] io_in_d_bits_size,
  input        io_in_d_bits_sink,
  input        io_in_d_bits_denied,
  input        io_in_d_bits_corrupt
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire [8:0] _T_10; // @[Edges.scala 22:16]
  wire  _T_11; // @[Edges.scala 22:24]
  wire [9:0] _T_48; // @[Parameters.scala 137:49]
  wire  _T_56; // @[Monitor.scala 79:25]
  wire [9:0] _T_61; // @[Parameters.scala 137:52]
  wire  _T_62; // @[Parameters.scala 137:67]
  wire  _T_67; // @[Monitor.scala 44:11]
  wire  _T_79; // @[Monitor.scala 44:11]
  wire  _T_80; // @[Monitor.scala 44:11]
  wire [3:0] _T_85; // @[Monitor.scala 86:18]
  wire  _T_86; // @[Monitor.scala 86:31]
  wire  _T_88; // @[Monitor.scala 44:11]
  wire  _T_89; // @[Monitor.scala 44:11]
  wire  _T_94; // @[Monitor.scala 90:25]
  wire  _T_136; // @[Monitor.scala 102:25]
  wire  _T_149; // @[Monitor.scala 44:11]
  wire  _T_150; // @[Monitor.scala 44:11]
  wire  _T_161; // @[Monitor.scala 107:30]
  wire  _T_163; // @[Monitor.scala 44:11]
  wire  _T_164; // @[Monitor.scala 44:11]
  wire  _T_169; // @[Monitor.scala 111:25]
  wire  _T_198; // @[Monitor.scala 119:25]
  wire  _T_229; // @[Monitor.scala 127:25]
  wire  _T_255; // @[Monitor.scala 135:25]
  wire  _T_281; // @[Monitor.scala 143:25]
  wire  _T_311; // @[Bundles.scala 44:24]
  wire  _T_313; // @[Monitor.scala 51:11]
  wire  _T_314; // @[Monitor.scala 51:11]
  wire  _T_318; // @[Monitor.scala 307:25]
  wire  _T_322; // @[Monitor.scala 309:27]
  wire  _T_324; // @[Monitor.scala 51:11]
  wire  _T_325; // @[Monitor.scala 51:11]
  wire  _T_326; // @[Monitor.scala 310:28]
  wire  _T_328; // @[Monitor.scala 51:11]
  wire  _T_329; // @[Monitor.scala 51:11]
  wire  _T_330; // @[Monitor.scala 311:15]
  wire  _T_332; // @[Monitor.scala 51:11]
  wire  _T_333; // @[Monitor.scala 51:11]
  wire  _T_334; // @[Monitor.scala 312:15]
  wire  _T_336; // @[Monitor.scala 51:11]
  wire  _T_337; // @[Monitor.scala 51:11]
  wire  _T_338; // @[Monitor.scala 315:25]
  wire  _T_349; // @[Bundles.scala 104:26]
  wire  _T_351; // @[Monitor.scala 51:11]
  wire  _T_352; // @[Monitor.scala 51:11]
  wire  _T_353; // @[Monitor.scala 320:28]
  wire  _T_355; // @[Monitor.scala 51:11]
  wire  _T_356; // @[Monitor.scala 51:11]
  wire  _T_366; // @[Monitor.scala 325:25]
  wire  _T_386; // @[Monitor.scala 331:30]
  wire  _T_388; // @[Monitor.scala 51:11]
  wire  _T_389; // @[Monitor.scala 51:11]
  wire  _T_395; // @[Monitor.scala 335:25]
  wire  _T_412; // @[Monitor.scala 343:25]
  wire  _T_430; // @[Monitor.scala 351:25]
  wire  _T_462; // @[Decoupled.scala 40:37]
  reg  _T_471; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire  _T_473; // @[Edges.scala 231:28]
  wire  _T_474; // @[Edges.scala 232:25]
  reg [2:0] _T_482; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [8:0] _T_486; // @[Monitor.scala 385:22]
  reg [31:0] _RAND_2;
  wire  _T_487; // @[Monitor.scala 386:22]
  wire  _T_488; // @[Monitor.scala 386:19]
  wire  _T_489; // @[Monitor.scala 387:32]
  wire  _T_491; // @[Monitor.scala 44:11]
  wire  _T_492; // @[Monitor.scala 44:11]
  wire  _T_505; // @[Monitor.scala 391:32]
  wire  _T_507; // @[Monitor.scala 44:11]
  wire  _T_508; // @[Monitor.scala 44:11]
  wire  _T_510; // @[Monitor.scala 393:20]
  wire  _T_511; // @[Decoupled.scala 40:37]
  reg  _T_519; // @[Edges.scala 230:27]
  reg [31:0] _RAND_3;
  wire  _T_521; // @[Edges.scala 231:28]
  wire  _T_522; // @[Edges.scala 232:25]
  reg [2:0] _T_530; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_4;
  reg [1:0] _T_531; // @[Monitor.scala 533:22]
  reg [31:0] _RAND_5;
  reg [1:0] _T_532; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_6;
  reg  _T_534; // @[Monitor.scala 536:22]
  reg [31:0] _RAND_7;
  reg  _T_535; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_8;
  wire  _T_536; // @[Monitor.scala 538:22]
  wire  _T_537; // @[Monitor.scala 538:19]
  wire  _T_538; // @[Monitor.scala 539:29]
  wire  _T_540; // @[Monitor.scala 51:11]
  wire  _T_541; // @[Monitor.scala 51:11]
  wire  _T_542; // @[Monitor.scala 540:29]
  wire  _T_544; // @[Monitor.scala 51:11]
  wire  _T_545; // @[Monitor.scala 51:11]
  wire  _T_546; // @[Monitor.scala 541:29]
  wire  _T_548; // @[Monitor.scala 51:11]
  wire  _T_549; // @[Monitor.scala 51:11]
  wire  _T_554; // @[Monitor.scala 543:29]
  wire  _T_556; // @[Monitor.scala 51:11]
  wire  _T_557; // @[Monitor.scala 51:11]
  wire  _T_558; // @[Monitor.scala 544:29]
  wire  _T_560; // @[Monitor.scala 51:11]
  wire  _T_561; // @[Monitor.scala 51:11]
  wire  _T_563; // @[Monitor.scala 546:20]
  reg  _T_564; // @[Monitor.scala 568:27]
  reg [31:0] _RAND_9;
  reg  _T_574; // @[Edges.scala 230:27]
  reg [31:0] _RAND_10;
  wire  _T_576; // @[Edges.scala 231:28]
  wire  _T_577; // @[Edges.scala 232:25]
  reg  _T_593; // @[Edges.scala 230:27]
  reg [31:0] _RAND_11;
  wire  _T_595; // @[Edges.scala 231:28]
  wire  _T_596; // @[Edges.scala 232:25]
  wire  _T_606; // @[Monitor.scala 574:27]
  wire  _T_611; // @[Monitor.scala 576:14]
  wire  _T_613; // @[Monitor.scala 576:13]
  wire  _T_614; // @[Monitor.scala 576:13]
  wire [1:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_618; // @[Monitor.scala 581:27]
  wire  _T_620; // @[Monitor.scala 581:75]
  wire  _T_621; // @[Monitor.scala 581:72]
  wire  _T_623; // @[Monitor.scala 583:21]
  wire  _T_627; // @[Monitor.scala 51:11]
  wire  _T_628; // @[Monitor.scala 51:11]
  wire [1:0] _GEN_16; // @[Monitor.scala 581:91]
  wire  _T_629; // @[Monitor.scala 590:27]
  wire  _T_630; // @[Monitor.scala 590:38]
  reg [31:0] _T_632; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_12;
  wire  _T_635; // @[Monitor.scala 595:36]
  wire  _T_636; // @[Monitor.scala 595:27]
  wire  _T_637; // @[Monitor.scala 595:56]
  wire  _T_638; // @[Monitor.scala 595:44]
  wire  _T_640; // @[Monitor.scala 595:12]
  wire  _T_641; // @[Monitor.scala 595:12]
  wire [31:0] _T_643; // @[Monitor.scala 597:26]
  wire  _T_646; // @[Monitor.scala 598:27]
  wire  _GEN_18; // @[Monitor.scala 44:11]
  wire  _GEN_26; // @[Monitor.scala 44:11]
  wire  _GEN_36; // @[Monitor.scala 44:11]
  wire  _GEN_42; // @[Monitor.scala 44:11]
  wire  _GEN_48; // @[Monitor.scala 44:11]
  wire  _GEN_52; // @[Monitor.scala 44:11]
  wire  _GEN_58; // @[Monitor.scala 44:11]
  wire  _GEN_64; // @[Monitor.scala 44:11]
  wire  _GEN_70; // @[Monitor.scala 51:11]
  wire  _GEN_78; // @[Monitor.scala 51:11]
  wire  _GEN_88; // @[Monitor.scala 51:11]
  wire  _GEN_98; // @[Monitor.scala 51:11]
  wire  _GEN_102; // @[Monitor.scala 51:11]
  wire  _GEN_106; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_10 = io_in_a_bits_address & 9'h3; // @[Edges.scala 22:16]
  assign _T_11 = _T_10 == 9'h0; // @[Edges.scala 22:24]
  assign _T_48 = {1'b0,$signed(io_in_a_bits_address)}; // @[Parameters.scala 137:49]
  assign _T_56 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_61 = $signed(_T_48) & -10'sh200; // @[Parameters.scala 137:52]
  assign _T_62 = $signed(_T_61) == 10'sh0; // @[Parameters.scala 137:67]
  assign _T_67 = ~reset; // @[Monitor.scala 44:11]
  assign _T_79 = _T_11 | reset; // @[Monitor.scala 44:11]
  assign _T_80 = ~_T_79; // @[Monitor.scala 44:11]
  assign _T_85 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_86 = _T_85 == 4'h0; // @[Monitor.scala 86:31]
  assign _T_88 = _T_86 | reset; // @[Monitor.scala 44:11]
  assign _T_89 = ~_T_88; // @[Monitor.scala 44:11]
  assign _T_94 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_136 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_149 = _T_62 | reset; // @[Monitor.scala 44:11]
  assign _T_150 = ~_T_149; // @[Monitor.scala 44:11]
  assign _T_161 = io_in_a_bits_mask == 4'hf; // @[Monitor.scala 107:30]
  assign _T_163 = _T_161 | reset; // @[Monitor.scala 44:11]
  assign _T_164 = ~_T_163; // @[Monitor.scala 44:11]
  assign _T_169 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_198 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_229 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_255 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_281 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_311 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_313 = _T_311 | reset; // @[Monitor.scala 51:11]
  assign _T_314 = ~_T_313; // @[Monitor.scala 51:11]
  assign _T_318 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_322 = io_in_d_bits_size >= 2'h2; // @[Monitor.scala 309:27]
  assign _T_324 = _T_322 | reset; // @[Monitor.scala 51:11]
  assign _T_325 = ~_T_324; // @[Monitor.scala 51:11]
  assign _T_326 = io_in_d_bits_param == 2'h0; // @[Monitor.scala 310:28]
  assign _T_328 = _T_326 | reset; // @[Monitor.scala 51:11]
  assign _T_329 = ~_T_328; // @[Monitor.scala 51:11]
  assign _T_330 = ~io_in_d_bits_corrupt; // @[Monitor.scala 311:15]
  assign _T_332 = _T_330 | reset; // @[Monitor.scala 51:11]
  assign _T_333 = ~_T_332; // @[Monitor.scala 51:11]
  assign _T_334 = ~io_in_d_bits_denied; // @[Monitor.scala 312:15]
  assign _T_336 = _T_334 | reset; // @[Monitor.scala 51:11]
  assign _T_337 = ~_T_336; // @[Monitor.scala 51:11]
  assign _T_338 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_349 = io_in_d_bits_param <= 2'h2; // @[Bundles.scala 104:26]
  assign _T_351 = _T_349 | reset; // @[Monitor.scala 51:11]
  assign _T_352 = ~_T_351; // @[Monitor.scala 51:11]
  assign _T_353 = io_in_d_bits_param != 2'h2; // @[Monitor.scala 320:28]
  assign _T_355 = _T_353 | reset; // @[Monitor.scala 51:11]
  assign _T_356 = ~_T_355; // @[Monitor.scala 51:11]
  assign _T_366 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_386 = _T_334 | io_in_d_bits_corrupt; // @[Monitor.scala 331:30]
  assign _T_388 = _T_386 | reset; // @[Monitor.scala 51:11]
  assign _T_389 = ~_T_388; // @[Monitor.scala 51:11]
  assign _T_395 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_412 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_430 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_462 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_473 = _T_471 - 1'h1; // @[Edges.scala 231:28]
  assign _T_474 = ~_T_471; // @[Edges.scala 232:25]
  assign _T_487 = ~_T_474; // @[Monitor.scala 386:22]
  assign _T_488 = io_in_a_valid & _T_487; // @[Monitor.scala 386:19]
  assign _T_489 = io_in_a_bits_opcode == _T_482; // @[Monitor.scala 387:32]
  assign _T_491 = _T_489 | reset; // @[Monitor.scala 44:11]
  assign _T_492 = ~_T_491; // @[Monitor.scala 44:11]
  assign _T_505 = io_in_a_bits_address == _T_486; // @[Monitor.scala 391:32]
  assign _T_507 = _T_505 | reset; // @[Monitor.scala 44:11]
  assign _T_508 = ~_T_507; // @[Monitor.scala 44:11]
  assign _T_510 = _T_462 & _T_474; // @[Monitor.scala 393:20]
  assign _T_511 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_521 = _T_519 - 1'h1; // @[Edges.scala 231:28]
  assign _T_522 = ~_T_519; // @[Edges.scala 232:25]
  assign _T_536 = ~_T_522; // @[Monitor.scala 538:22]
  assign _T_537 = io_in_d_valid & _T_536; // @[Monitor.scala 538:19]
  assign _T_538 = io_in_d_bits_opcode == _T_530; // @[Monitor.scala 539:29]
  assign _T_540 = _T_538 | reset; // @[Monitor.scala 51:11]
  assign _T_541 = ~_T_540; // @[Monitor.scala 51:11]
  assign _T_542 = io_in_d_bits_param == _T_531; // @[Monitor.scala 540:29]
  assign _T_544 = _T_542 | reset; // @[Monitor.scala 51:11]
  assign _T_545 = ~_T_544; // @[Monitor.scala 51:11]
  assign _T_546 = io_in_d_bits_size == _T_532; // @[Monitor.scala 541:29]
  assign _T_548 = _T_546 | reset; // @[Monitor.scala 51:11]
  assign _T_549 = ~_T_548; // @[Monitor.scala 51:11]
  assign _T_554 = io_in_d_bits_sink == _T_534; // @[Monitor.scala 543:29]
  assign _T_556 = _T_554 | reset; // @[Monitor.scala 51:11]
  assign _T_557 = ~_T_556; // @[Monitor.scala 51:11]
  assign _T_558 = io_in_d_bits_denied == _T_535; // @[Monitor.scala 544:29]
  assign _T_560 = _T_558 | reset; // @[Monitor.scala 51:11]
  assign _T_561 = ~_T_560; // @[Monitor.scala 51:11]
  assign _T_563 = _T_511 & _T_522; // @[Monitor.scala 546:20]
  assign _T_576 = _T_574 - 1'h1; // @[Edges.scala 231:28]
  assign _T_577 = ~_T_574; // @[Edges.scala 232:25]
  assign _T_595 = _T_593 - 1'h1; // @[Edges.scala 231:28]
  assign _T_596 = ~_T_593; // @[Edges.scala 232:25]
  assign _T_606 = _T_462 & _T_577; // @[Monitor.scala 574:27]
  assign _T_611 = ~_T_564; // @[Monitor.scala 576:14]
  assign _T_613 = _T_611 | reset; // @[Monitor.scala 576:13]
  assign _T_614 = ~_T_613; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_606 ? 2'h1 : 2'h0; // @[Monitor.scala 574:72]
  assign _T_618 = _T_511 & _T_596; // @[Monitor.scala 581:27]
  assign _T_620 = ~_T_318; // @[Monitor.scala 581:75]
  assign _T_621 = _T_618 & _T_620; // @[Monitor.scala 581:72]
  assign _T_623 = _GEN_15[0] | _T_564; // @[Monitor.scala 583:21]
  assign _T_627 = _T_623 | reset; // @[Monitor.scala 51:11]
  assign _T_628 = ~_T_627; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_621 ? 2'h1 : 2'h0; // @[Monitor.scala 581:91]
  assign _T_629 = _T_564 | _GEN_15[0]; // @[Monitor.scala 590:27]
  assign _T_630 = ~_GEN_16[0]; // @[Monitor.scala 590:38]
  assign _T_635 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_636 = _T_611 | _T_635; // @[Monitor.scala 595:27]
  assign _T_637 = _T_632 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_638 = _T_636 | _T_637; // @[Monitor.scala 595:44]
  assign _T_640 = _T_638 | reset; // @[Monitor.scala 595:12]
  assign _T_641 = ~_T_640; // @[Monitor.scala 595:12]
  assign _T_643 = _T_632 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_646 = _T_462 | _T_511; // @[Monitor.scala 598:27]
  assign _GEN_18 = io_in_a_valid & _T_56; // @[Monitor.scala 44:11]
  assign _GEN_26 = io_in_a_valid & _T_94; // @[Monitor.scala 44:11]
  assign _GEN_36 = io_in_a_valid & _T_136; // @[Monitor.scala 44:11]
  assign _GEN_42 = io_in_a_valid & _T_169; // @[Monitor.scala 44:11]
  assign _GEN_48 = io_in_a_valid & _T_198; // @[Monitor.scala 44:11]
  assign _GEN_52 = io_in_a_valid & _T_229; // @[Monitor.scala 44:11]
  assign _GEN_58 = io_in_a_valid & _T_255; // @[Monitor.scala 44:11]
  assign _GEN_64 = io_in_a_valid & _T_281; // @[Monitor.scala 44:11]
  assign _GEN_70 = io_in_d_valid & _T_318; // @[Monitor.scala 51:11]
  assign _GEN_78 = io_in_d_valid & _T_338; // @[Monitor.scala 51:11]
  assign _GEN_88 = io_in_d_valid & _T_366; // @[Monitor.scala 51:11]
  assign _GEN_98 = io_in_d_valid & _T_395; // @[Monitor.scala 51:11]
  assign _GEN_102 = io_in_d_valid & _T_412; // @[Monitor.scala 51:11]
  assign _GEN_106 = io_in_d_valid & _T_430; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_471 = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_482 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_486 = _RAND_2[8:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_519 = _RAND_3[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_530 = _RAND_4[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  _T_531 = _RAND_5[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_532 = _RAND_6[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_534 = _RAND_7[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_535 = _RAND_8[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_564 = _RAND_9[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_574 = _RAND_10[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_593 = _RAND_11[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_632 = _RAND_12[31:0];
  `endif // RANDOMIZE_REG_INIT
  if (reset) begin
    _T_471 = 1'h0;
  end
  if (reset) begin
    _T_519 = 1'h0;
  end
  if (reset) begin
    _T_564 = 1'h0;
  end
  if (reset) begin
    _T_574 = 1'h0;
  end
  if (reset) begin
    _T_593 = 1'h0;
  end
  if (reset) begin
    _T_632 = 32'h0;
  end
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (_T_510) begin
      _T_482 <= io_in_a_bits_opcode;
    end
    if (_T_510) begin
      _T_486 <= io_in_a_bits_address;
    end
    if (_T_563) begin
      _T_530 <= io_in_d_bits_opcode;
    end
    if (_T_563) begin
      _T_531 <= io_in_d_bits_param;
    end
    if (_T_563) begin
      _T_532 <= io_in_d_bits_size;
    end
    if (_T_563) begin
      _T_534 <= io_in_d_bits_sink;
    end
    if (_T_563) begin
      _T_535 <= io_in_d_bits_denied;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_67) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_67) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_67) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_67) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_80) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_80) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_89) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_89) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_67) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_67) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_67) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_67) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_80) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_80) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_67) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_67) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_89) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_89) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_36 & _T_150) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_36 & _T_150) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_36 & _T_80) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_36 & _T_80) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_36 & _T_164) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_36 & _T_164) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_42 & _T_150) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_42 & _T_150) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_42 & _T_80) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_42 & _T_80) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_42 & _T_164) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_42 & _T_164) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_48 & _T_150) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_48 & _T_150) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_48 & _T_80) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_48 & _T_80) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_52 & _T_67) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_52 & _T_67) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_52 & _T_80) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_52 & _T_80) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_52 & _T_164) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_52 & _T_164) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_58 & _T_67) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_58 & _T_67) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_58 & _T_80) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_58 & _T_80) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_58 & _T_164) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_58 & _T_164) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_67) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_67) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_80) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_80) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_164) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_164) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_314) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_314) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_70 & _T_325) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_70 & _T_325) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_70 & _T_329) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseeAck carries invalid param (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_70 & _T_329) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_70 & _T_333) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is corrupt (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_70 & _T_333) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_70 & _T_337) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is denied (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_70 & _T_337) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_78 & _T_67) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid sink ID (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_78 & _T_67) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_78 & _T_325) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_78 & _T_325) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_78 & _T_352) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid cap param (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_78 & _T_352) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_78 & _T_356) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries toN param (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_78 & _T_356) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_78 & _T_333) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is corrupt (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_78 & _T_333) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_88 & _T_67) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid sink ID (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_88 & _T_67) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_88 & _T_325) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_88 & _T_325) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_88 & _T_352) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid cap param (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_88 & _T_352) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_88 & _T_356) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries toN param (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_88 & _T_356) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_88 & _T_389) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied but not corrupt (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_88 & _T_389) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_98 & _T_329) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid param (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_98 & _T_329) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_98 & _T_333) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is corrupt (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_98 & _T_333) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_102 & _T_329) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid param (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_102 & _T_329) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_102 & _T_389) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied but not corrupt (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_102 & _T_389) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_106 & _T_329) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid param (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_106 & _T_329) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_106 & _T_333) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is corrupt (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_106 & _T_333) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_488 & _T_492) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_488 & _T_492) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_488 & _T_508) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at Debug.scala:606:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_488 & _T_508) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_537 & _T_541) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_537 & _T_541) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_537 & _T_545) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel param changed within multibeat operation (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_537 & _T_545) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_537 & _T_549) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_537 & _T_549) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_537 & _T_557) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel sink changed with multibeat operation (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_537 & _T_557) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_537 & _T_561) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_537 & _T_561) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_606 & _T_614) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at Debug.scala:606:18)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_606 & _T_614) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_621 & _T_628) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at Debug.scala:606:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_621 & _T_628) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_641) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at Debug.scala:606:18)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_641) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_471 <= 1'h0;
    end else if (_T_462) begin
      if (_T_474) begin
        _T_471 <= 1'h0;
      end else begin
        _T_471 <= _T_473;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_519 <= 1'h0;
    end else if (_T_511) begin
      if (_T_522) begin
        _T_519 <= 1'h0;
      end else begin
        _T_519 <= _T_521;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_564 <= 1'h0;
    end else begin
      _T_564 <= _T_629 & _T_630;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_574 <= 1'h0;
    end else if (_T_462) begin
      if (_T_577) begin
        _T_574 <= 1'h0;
      end else begin
        _T_574 <= _T_576;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_593 <= 1'h0;
    end else if (_T_511) begin
      if (_T_596) begin
        _T_593 <= 1'h0;
      end else begin
        _T_593 <= _T_595;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_632 <= 32'h0;
    end else if (_T_646) begin
      _T_632 <= 32'h0;
    end else begin
      _T_632 <= _T_643;
    end
  end

endmodule
