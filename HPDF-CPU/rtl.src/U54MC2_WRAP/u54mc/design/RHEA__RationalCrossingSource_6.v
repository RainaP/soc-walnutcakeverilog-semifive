//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__RationalCrossingSource_6(
  input        rf_reset,
  input        clock,
  input        reset,
  output       io_enq_ready,
  input        io_enq_valid,
  input  [4:0] io_enq_bits_sink,
  output [4:0] io_deq_bits0_sink,
  output [4:0] io_deq_bits1_sink,
  output       io_deq_valid,
  output [1:0] io_deq_source,
  input        io_deq_ready,
  input  [1:0] io_deq_sink
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
`endif // RANDOMIZE_REG_INIT
  reg  enq_in_REG; // @[BlockDuringReset.scala 12:22]
  wire  _enq_in_T = ~enq_in_REG; // @[BlockDuringReset.scala 17:41]
  wire  enq_valid = _enq_in_T ? 1'h0 : io_enq_valid; // @[Blockable.scala 35:30 Blockable.scala 36:20 Blockable.scala 32:18]
  reg [1:0] count; // @[RationalCrossing.scala 84:22]
  wire  equal = count == io_deq_sink; // @[RationalCrossing.scala 85:21]
  wire  _enq_in_ready_T = count[1]; // @[RationalCrossing.scala 91:44]
  wire  _enq_in_ready_T_1 = io_deq_sink[0]; // @[RationalCrossing.scala 91:60]
  wire  enq_ready = equal ? io_deq_ready : count[1] != io_deq_sink[0]; // @[RationalCrossing.scala 91:20]
  reg [4:0] io_deq_bits1_r_sink; // @[Reg.scala 15:16]
  wire  _T = enq_ready & enq_valid; // @[Decoupled.scala 40:37]
  wire  count_hi = count[0]; // @[RationalCrossing.scala 93:41]
  wire  count_lo = ~count[1]; // @[RationalCrossing.scala 93:46]
  wire [1:0] _count_T_1 = {count_hi,count_lo}; // @[Cat.scala 30:58]
  assign io_enq_ready = _enq_in_T ? 1'h0 : enq_ready; // @[Blockable.scala 35:30 Blockable.scala 37:20 Blockable.scala 33:18]
  assign io_deq_bits0_sink = io_enq_bits_sink; // @[Blockable.scala 31:21 Blockable.scala 34:18]
  assign io_deq_bits1_sink = io_deq_bits1_r_sink; // @[RationalCrossing.scala 90:14]
  assign io_deq_valid = _enq_in_T ? 1'h0 : io_enq_valid; // @[Blockable.scala 35:30 Blockable.scala 36:20 Blockable.scala 32:18]
  assign io_deq_source = count; // @[RationalCrossing.scala 88:14]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      enq_in_REG <= 1'h0;
    end else begin
      enq_in_REG <= 1'h1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count <= 2'h0;
    end else if (_T) begin
      count <= _count_T_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      io_deq_bits1_r_sink <= 5'h0;
    end else if (equal) begin
      io_deq_bits1_r_sink <= io_enq_bits_sink;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  enq_in_REG = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  count = _RAND_1[1:0];
  _RAND_2 = {1{`RANDOM}};
  io_deq_bits1_r_sink = _RAND_2[4:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    enq_in_REG = 1'h0;
  end
  if (reset) begin
    count = 2'h0;
  end
  if (rf_reset) begin
    io_deq_bits1_r_sink = 5'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
