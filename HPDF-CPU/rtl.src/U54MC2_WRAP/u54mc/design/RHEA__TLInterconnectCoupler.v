//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLInterconnectCoupler(
  input          rf_reset,
  input          clock,
  input          reset,
  input          auto_buffer_out_a_ready,
  output         auto_buffer_out_a_valid,
  output [2:0]   auto_buffer_out_a_bits_opcode,
  output [2:0]   auto_buffer_out_a_bits_param,
  output [2:0]   auto_buffer_out_a_bits_size,
  output [9:0]   auto_buffer_out_a_bits_source,
  output [27:0]  auto_buffer_out_a_bits_address,
  output [15:0]  auto_buffer_out_a_bits_mask,
  output [127:0] auto_buffer_out_a_bits_data,
  output         auto_buffer_out_a_bits_corrupt,
  output         auto_buffer_out_d_ready,
  input          auto_buffer_out_d_valid,
  input  [2:0]   auto_buffer_out_d_bits_opcode,
  input  [2:0]   auto_buffer_out_d_bits_size,
  input  [9:0]   auto_buffer_out_d_bits_source,
  input  [127:0] auto_buffer_out_d_bits_data,
  input          auto_buffer_out_d_bits_corrupt,
  output         auto_tl_in_a_ready,
  input          auto_tl_in_a_valid,
  input  [2:0]   auto_tl_in_a_bits_opcode,
  input  [2:0]   auto_tl_in_a_bits_param,
  input  [2:0]   auto_tl_in_a_bits_size,
  input  [6:0]   auto_tl_in_a_bits_source,
  input  [27:0]  auto_tl_in_a_bits_address,
  input  [15:0]  auto_tl_in_a_bits_mask,
  input  [127:0] auto_tl_in_a_bits_data,
  input          auto_tl_in_a_bits_corrupt,
  input          auto_tl_in_d_ready,
  output         auto_tl_in_d_valid,
  output [2:0]   auto_tl_in_d_bits_opcode,
  output [2:0]   auto_tl_in_d_bits_size,
  output [6:0]   auto_tl_in_d_bits_source,
  output [127:0] auto_tl_in_d_bits_data,
  output         auto_tl_in_d_bits_corrupt
);
  wire  buffer_rf_reset; // @[Buffer.scala 68:28]
  wire  buffer_clock; // @[Buffer.scala 68:28]
  wire  buffer_reset; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_in_a_bits_opcode; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_in_a_bits_param; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_in_a_bits_size; // @[Buffer.scala 68:28]
  wire [9:0] buffer_auto_in_a_bits_source; // @[Buffer.scala 68:28]
  wire [27:0] buffer_auto_in_a_bits_address; // @[Buffer.scala 68:28]
  wire [15:0] buffer_auto_in_a_bits_mask; // @[Buffer.scala 68:28]
  wire [127:0] buffer_auto_in_a_bits_data; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_corrupt; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_d_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_d_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_in_d_bits_opcode; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_in_d_bits_size; // @[Buffer.scala 68:28]
  wire [9:0] buffer_auto_in_d_bits_source; // @[Buffer.scala 68:28]
  wire [127:0] buffer_auto_in_d_bits_data; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_d_bits_corrupt; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_out_a_bits_opcode; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_out_a_bits_param; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_out_a_bits_size; // @[Buffer.scala 68:28]
  wire [9:0] buffer_auto_out_a_bits_source; // @[Buffer.scala 68:28]
  wire [27:0] buffer_auto_out_a_bits_address; // @[Buffer.scala 68:28]
  wire [15:0] buffer_auto_out_a_bits_mask; // @[Buffer.scala 68:28]
  wire [127:0] buffer_auto_out_a_bits_data; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_corrupt; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_d_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_d_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_out_d_bits_opcode; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_out_d_bits_size; // @[Buffer.scala 68:28]
  wire [9:0] buffer_auto_out_d_bits_source; // @[Buffer.scala 68:28]
  wire [127:0] buffer_auto_out_d_bits_data; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_d_bits_corrupt; // @[Buffer.scala 68:28]
  wire  fragmenter_rf_reset; // @[Fragmenter.scala 333:34]
  wire  fragmenter_clock; // @[Fragmenter.scala 333:34]
  wire  fragmenter_reset; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_a_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_a_bits_param; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_a_bits_size; // @[Fragmenter.scala 333:34]
  wire [6:0] fragmenter_auto_in_a_bits_source; // @[Fragmenter.scala 333:34]
  wire [27:0] fragmenter_auto_in_a_bits_address; // @[Fragmenter.scala 333:34]
  wire [15:0] fragmenter_auto_in_a_bits_mask; // @[Fragmenter.scala 333:34]
  wire [127:0] fragmenter_auto_in_a_bits_data; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_bits_corrupt; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_d_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_d_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_d_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_d_bits_size; // @[Fragmenter.scala 333:34]
  wire [6:0] fragmenter_auto_in_d_bits_source; // @[Fragmenter.scala 333:34]
  wire [127:0] fragmenter_auto_in_d_bits_data; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_d_bits_corrupt; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_a_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_a_bits_param; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_a_bits_size; // @[Fragmenter.scala 333:34]
  wire [9:0] fragmenter_auto_out_a_bits_source; // @[Fragmenter.scala 333:34]
  wire [27:0] fragmenter_auto_out_a_bits_address; // @[Fragmenter.scala 333:34]
  wire [15:0] fragmenter_auto_out_a_bits_mask; // @[Fragmenter.scala 333:34]
  wire [127:0] fragmenter_auto_out_a_bits_data; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_bits_corrupt; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_d_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_d_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_d_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_d_bits_size; // @[Fragmenter.scala 333:34]
  wire [9:0] fragmenter_auto_out_d_bits_source; // @[Fragmenter.scala 333:34]
  wire [127:0] fragmenter_auto_out_d_bits_data; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_d_bits_corrupt; // @[Fragmenter.scala 333:34]
  RHEA__TLBuffer buffer ( // @[Buffer.scala 68:28]
    .rf_reset(buffer_rf_reset),
    .clock(buffer_clock),
    .reset(buffer_reset),
    .auto_in_a_ready(buffer_auto_in_a_ready),
    .auto_in_a_valid(buffer_auto_in_a_valid),
    .auto_in_a_bits_opcode(buffer_auto_in_a_bits_opcode),
    .auto_in_a_bits_param(buffer_auto_in_a_bits_param),
    .auto_in_a_bits_size(buffer_auto_in_a_bits_size),
    .auto_in_a_bits_source(buffer_auto_in_a_bits_source),
    .auto_in_a_bits_address(buffer_auto_in_a_bits_address),
    .auto_in_a_bits_mask(buffer_auto_in_a_bits_mask),
    .auto_in_a_bits_data(buffer_auto_in_a_bits_data),
    .auto_in_a_bits_corrupt(buffer_auto_in_a_bits_corrupt),
    .auto_in_d_ready(buffer_auto_in_d_ready),
    .auto_in_d_valid(buffer_auto_in_d_valid),
    .auto_in_d_bits_opcode(buffer_auto_in_d_bits_opcode),
    .auto_in_d_bits_size(buffer_auto_in_d_bits_size),
    .auto_in_d_bits_source(buffer_auto_in_d_bits_source),
    .auto_in_d_bits_data(buffer_auto_in_d_bits_data),
    .auto_in_d_bits_corrupt(buffer_auto_in_d_bits_corrupt),
    .auto_out_a_ready(buffer_auto_out_a_ready),
    .auto_out_a_valid(buffer_auto_out_a_valid),
    .auto_out_a_bits_opcode(buffer_auto_out_a_bits_opcode),
    .auto_out_a_bits_param(buffer_auto_out_a_bits_param),
    .auto_out_a_bits_size(buffer_auto_out_a_bits_size),
    .auto_out_a_bits_source(buffer_auto_out_a_bits_source),
    .auto_out_a_bits_address(buffer_auto_out_a_bits_address),
    .auto_out_a_bits_mask(buffer_auto_out_a_bits_mask),
    .auto_out_a_bits_data(buffer_auto_out_a_bits_data),
    .auto_out_a_bits_corrupt(buffer_auto_out_a_bits_corrupt),
    .auto_out_d_ready(buffer_auto_out_d_ready),
    .auto_out_d_valid(buffer_auto_out_d_valid),
    .auto_out_d_bits_opcode(buffer_auto_out_d_bits_opcode),
    .auto_out_d_bits_size(buffer_auto_out_d_bits_size),
    .auto_out_d_bits_source(buffer_auto_out_d_bits_source),
    .auto_out_d_bits_data(buffer_auto_out_d_bits_data),
    .auto_out_d_bits_corrupt(buffer_auto_out_d_bits_corrupt)
  );
  RHEA__TLFragmenter fragmenter ( // @[Fragmenter.scala 333:34]
    .rf_reset(fragmenter_rf_reset),
    .clock(fragmenter_clock),
    .reset(fragmenter_reset),
    .auto_in_a_ready(fragmenter_auto_in_a_ready),
    .auto_in_a_valid(fragmenter_auto_in_a_valid),
    .auto_in_a_bits_opcode(fragmenter_auto_in_a_bits_opcode),
    .auto_in_a_bits_param(fragmenter_auto_in_a_bits_param),
    .auto_in_a_bits_size(fragmenter_auto_in_a_bits_size),
    .auto_in_a_bits_source(fragmenter_auto_in_a_bits_source),
    .auto_in_a_bits_address(fragmenter_auto_in_a_bits_address),
    .auto_in_a_bits_mask(fragmenter_auto_in_a_bits_mask),
    .auto_in_a_bits_data(fragmenter_auto_in_a_bits_data),
    .auto_in_a_bits_corrupt(fragmenter_auto_in_a_bits_corrupt),
    .auto_in_d_ready(fragmenter_auto_in_d_ready),
    .auto_in_d_valid(fragmenter_auto_in_d_valid),
    .auto_in_d_bits_opcode(fragmenter_auto_in_d_bits_opcode),
    .auto_in_d_bits_size(fragmenter_auto_in_d_bits_size),
    .auto_in_d_bits_source(fragmenter_auto_in_d_bits_source),
    .auto_in_d_bits_data(fragmenter_auto_in_d_bits_data),
    .auto_in_d_bits_corrupt(fragmenter_auto_in_d_bits_corrupt),
    .auto_out_a_ready(fragmenter_auto_out_a_ready),
    .auto_out_a_valid(fragmenter_auto_out_a_valid),
    .auto_out_a_bits_opcode(fragmenter_auto_out_a_bits_opcode),
    .auto_out_a_bits_param(fragmenter_auto_out_a_bits_param),
    .auto_out_a_bits_size(fragmenter_auto_out_a_bits_size),
    .auto_out_a_bits_source(fragmenter_auto_out_a_bits_source),
    .auto_out_a_bits_address(fragmenter_auto_out_a_bits_address),
    .auto_out_a_bits_mask(fragmenter_auto_out_a_bits_mask),
    .auto_out_a_bits_data(fragmenter_auto_out_a_bits_data),
    .auto_out_a_bits_corrupt(fragmenter_auto_out_a_bits_corrupt),
    .auto_out_d_ready(fragmenter_auto_out_d_ready),
    .auto_out_d_valid(fragmenter_auto_out_d_valid),
    .auto_out_d_bits_opcode(fragmenter_auto_out_d_bits_opcode),
    .auto_out_d_bits_size(fragmenter_auto_out_d_bits_size),
    .auto_out_d_bits_source(fragmenter_auto_out_d_bits_source),
    .auto_out_d_bits_data(fragmenter_auto_out_d_bits_data),
    .auto_out_d_bits_corrupt(fragmenter_auto_out_d_bits_corrupt)
  );
  assign buffer_rf_reset = rf_reset;
  assign fragmenter_rf_reset = rf_reset;
  assign auto_buffer_out_a_valid = buffer_auto_out_a_valid; // @[LazyModule.scala 390:12]
  assign auto_buffer_out_a_bits_opcode = buffer_auto_out_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_buffer_out_a_bits_param = buffer_auto_out_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_buffer_out_a_bits_size = buffer_auto_out_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_buffer_out_a_bits_source = buffer_auto_out_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_buffer_out_a_bits_address = buffer_auto_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_buffer_out_a_bits_mask = buffer_auto_out_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_buffer_out_a_bits_data = buffer_auto_out_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_buffer_out_a_bits_corrupt = buffer_auto_out_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_buffer_out_d_ready = buffer_auto_out_d_ready; // @[LazyModule.scala 390:12]
  assign auto_tl_in_a_ready = fragmenter_auto_in_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_valid = fragmenter_auto_in_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_opcode = fragmenter_auto_in_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_size = fragmenter_auto_in_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_source = fragmenter_auto_in_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_data = fragmenter_auto_in_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_corrupt = fragmenter_auto_in_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign buffer_clock = clock;
  assign buffer_reset = reset;
  assign buffer_auto_in_a_valid = fragmenter_auto_out_a_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_opcode = fragmenter_auto_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_param = fragmenter_auto_out_a_bits_param; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_size = fragmenter_auto_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_source = fragmenter_auto_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_address = fragmenter_auto_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_mask = fragmenter_auto_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_data = fragmenter_auto_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_corrupt = fragmenter_auto_out_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_d_ready = fragmenter_auto_out_d_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_a_ready = auto_buffer_out_a_ready; // @[LazyModule.scala 390:12]
  assign buffer_auto_out_d_valid = auto_buffer_out_d_valid; // @[LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_opcode = auto_buffer_out_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_size = auto_buffer_out_d_bits_size; // @[LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_source = auto_buffer_out_d_bits_source; // @[LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_data = auto_buffer_out_d_bits_data; // @[LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_corrupt = auto_buffer_out_d_bits_corrupt; // @[LazyModule.scala 390:12]
  assign fragmenter_clock = clock;
  assign fragmenter_reset = reset;
  assign fragmenter_auto_in_a_valid = auto_tl_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_opcode = auto_tl_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_param = auto_tl_in_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_size = auto_tl_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_source = auto_tl_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_address = auto_tl_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_mask = auto_tl_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_data = auto_tl_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_corrupt = auto_tl_in_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_d_ready = auto_tl_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_out_a_ready = buffer_auto_in_a_ready; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_out_d_valid = buffer_auto_in_d_valid; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_out_d_bits_opcode = buffer_auto_in_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_out_d_bits_size = buffer_auto_in_d_bits_size; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_out_d_bits_source = buffer_auto_in_d_bits_source; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_out_d_bits_data = buffer_auto_in_d_bits_data; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_out_d_bits_corrupt = buffer_auto_in_d_bits_corrupt; // @[LazyModule.scala 375:16]
endmodule
