#include <dlfcn.h>
#include <stdio.h>

typedef int (*main_func) (void);
typedef void (*irq_func) (int);
typedef int (*conn_func) (int);
main_func custom_Main      = NULL;
irq_func  custom_IRQHandler= NULL;
conn_func custom_ConnQEMU  = NULL;

#define CONN_NONBLOCK   (0x1)
#define CONN_THREADED   (0x2)

extern "C" void semiCPUStartup(const char* application_path){
    void *plugin = dlopen(application_path, RTLD_NOW);
    if (!plugin)
    {
        printf("Cannot load %s: %s", application_path, dlerror ());
        return;
    }
    const char* result;
    custom_Main = (main_func)dlsym (plugin, "main");
    result = dlerror();
    if (result)
    {
        printf("Cannot find query in %s: %s", application_path, result);
        return;
    }

    custom_IRQHandler = (irq_func)dlsym (plugin, "SimIORaiseIRQ");
    result = dlerror();
    if (result)
    {
        printf("Cannot find query in %s: %s", application_path, result);
        return;
    }

    custom_ConnQEMU = (conn_func)dlsym (plugin, "SimIOConnService");
    result = dlerror();
    if (result)
    {
        printf("Cannot find query in %s: %s", application_path, result);
        return;
    }

    custom_Main();

    custom_ConnQEMU(CONN_NONBLOCK);
}

extern "C" void semiCPUIRQHandler(int interrupt_number){
    if(custom_IRQHandler){
        custom_IRQHandler(interrupt_number);
    }
}

