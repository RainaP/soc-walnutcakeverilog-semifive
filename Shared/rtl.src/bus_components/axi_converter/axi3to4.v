// Protocol Converter AXI3 to AXI4
// Temporary for RHEA FPGA

module axi3to4
#(parameter ADDR_WIDTH  = 38,
  parameter ID_WIDTH    = 10,
  parameter DATA_WIDTH  = 64,
  parameter USER_WIDTH  = 64,
  parameter LEN_WIDTH   = 8,
  parameter STRB_WIDTH  = DATA_WIDTH/8)
(
    input CLK__BUS,
    input RSTN__BUS,

    input [ADDR_WIDTH-1:0]  AXI3__BUS__AWADDR,
    input [ID_WIDTH-1:0]    AXI3__BUS__AWID,
    input [LEN_WIDTH-1:0]   AXI3__BUS__AWLEN,
    input [2:0]             AXI3__BUS__AWSIZE,
    input [1:0]             AXI3__BUS__AWBURST,
    input [1:0]             AXI3__BUS__AWLOCK,
    input [3:0]             AXI3__BUS__AWCACHE,
    input [2:0]             AXI3__BUS__AWPROT,
    input [USER_WIDTH-1:0]  AXI3__BUS__AWUSER,
    input                   AXI3__BUS__AWVALID,
    output                  AXI3__BUS__AWREADY,
    input [ID_WIDTH-1:0]    AXI3__BUS__WID,
    input [DATA_WIDTH-1:0]  AXI3__BUS__WDATA,
    input [STRB_WIDTH-1:0]  AXI3__BUS__WSTRB,
    input                   AXI3__BUS__WVALID,
    input                   AXI3__BUS__WLAST,
    output                  AXI3__BUS__WREADY,
    output [ID_WIDTH-1:0]   AXI3__BUS__BID,
    output                  AXI3__BUS__BVALID,
    output [1:0]            AXI3__BUS__BRESP,
    input                   AXI3__BUS__BREADY,
    input [ID_WIDTH-1:0]    AXI3__BUS__ARID,
    input [ADDR_WIDTH-1:0]  AXI3__BUS__ARADDR,
    input [LEN_WIDTH-1:0]   AXI3__BUS__ARLEN,
    input [2:0]             AXI3__BUS__ARSIZE,
    input [1:0]             AXI3__BUS__ARBURST,
    input [1:0]             AXI3__BUS__ARLOCK,
    input [3:0]             AXI3__BUS__ARCACHE,
    input [2:0]             AXI3__BUS__ARPROT,
    input [USER_WIDTH-1:0]  AXI3__BUS__ARUSER,
    input                   AXI3__BUS__ARVALID,
    output                  AXI3__BUS__ARREADY,
    output [ID_WIDTH-1:0]   AXI3__BUS__RID,
    output [DATA_WIDTH-1:0] AXI3__BUS__RDATA,
    output [1:0]            AXI3__BUS__RRESP,
    output                  AXI3__BUS__RLAST,
    output                  AXI3__BUS__RVALID,
    input                   AXI3__BUS__RREADY,

    output [ADDR_WIDTH-1:0] AXI4__BUS__AWADDR,
    output [ID_WIDTH-1:0]   AXI4__BUS__AWID,
    output [LEN_WIDTH-1:0]  AXI4__BUS__AWLEN,
    output [2:0]            AXI4__BUS__AWSIZE,
    output [1:0]            AXI4__BUS__AWBURST,
    output                  AXI4__BUS__AWLOCK,
    output [3:0]            AXI4__BUS__AWCACHE,
    output [2:0]            AXI4__BUS__AWPROT,
    output [USER_WIDTH-1:0] AXI4__BUS__AWUSER,
    output                  AXI4__BUS__AWVALID,
    input                   AXI4__BUS__AWREADY,
    output [DATA_WIDTH-1:0] AXI4__BUS__WDATA,
    output [STRB_WIDTH-1:0] AXI4__BUS__WSTRB,
    output                  AXI4__BUS__WLAST,
    output                  AXI4__BUS__WVALID,
    input                   AXI4__BUS__WREADY,
    input [ID_WIDTH-1:0]    AXI4__BUS__BID,
    input [1:0]             AXI4__BUS__BRESP,
    input                   AXI4__BUS__BVALID,
    output                  AXI4__BUS__BREADY,
    output [ID_WIDTH-1:0]   AXI4__BUS__ARID,
    output [ADDR_WIDTH-1:0] AXI4__BUS__ARADDR,
    output [LEN_WIDTH-1:0]  AXI4__BUS__ARLEN,
    output [2:0]            AXI4__BUS__ARSIZE,
    output [1:0]            AXI4__BUS__ARBURST,
    output                  AXI4__BUS__ARLOCK,
    output [3:0]            AXI4__BUS__ARCACHE,
    output [2:0]            AXI4__BUS__ARPROT,
    output [USER_WIDTH-1:0] AXI4__BUS__ARUSER,
    output                  AXI4__BUS__ARVALID,
    input                   AXI4__BUS__ARREADY,
    input [ID_WIDTH-1:0]    AXI4__BUS__RID,
    input [DATA_WIDTH-1:0]  AXI4__BUS__RDATA,
    input [1:0]             AXI4__BUS__RRESP,
    input                   AXI4__BUS__RLAST,
    input                   AXI4__BUS__RVALID,
    output                  AXI4__BUS__RREADY
);

assign AXI3__BUS__AWREADY   = AXI4__BUS__AWREADY;
assign AXI3__BUS__WREADY    = AXI4__BUS__WREADY;
assign AXI4__BUS__WVALID    = AXI3__BUS__WVALID;
assign AXI3__BUS__BID       = AXI4__BUS__BID;
assign AXI3__BUS__BVALID    = AXI4__BUS__BVALID;
assign AXI3__BUS__BRESP     = AXI4__BUS__BRESP;
assign AXI3__BUS__ARREADY   = AXI4__BUS__ARREADY;
assign AXI3__BUS__RID       = AXI4__BUS__RID;
assign AXI3__BUS__RDATA     = AXI4__BUS__RDATA;
assign AXI3__BUS__RRESP     = AXI4__BUS__RRESP;
assign AXI3__BUS__RLAST     = AXI4__BUS__RLAST;
assign AXI3__BUS__RVALID    = AXI4__BUS__RVALID;
assign AXI4__BUS__AWADDR    = AXI3__BUS__AWADDR;
assign AXI4__BUS__AWID      = AXI3__BUS__AWID;
assign AXI4__BUS__AWLEN     = AXI3__BUS__AWLEN;
assign AXI4__BUS__AWSIZE    = AXI3__BUS__AWSIZE;
assign AXI4__BUS__AWBURST   = AXI3__BUS__AWBURST;
assign AXI4__BUS__AWLOCK    = AXI3__BUS__AWLOCK[0];
assign AXI4__BUS__AWCACHE   = AXI3__BUS__AWCACHE;
assign AXI4__BUS__AWPROT    = AXI3__BUS__AWPROT;
assign AXI4__BUS__AWUSER    = AXI3__BUS__AWUSER;
assign AXI4__BUS__AWVALID   = AXI3__BUS__AWVALID;
assign AXI4__BUS__WDATA     = AXI3__BUS__WDATA;
assign AXI4__BUS__WSTRB     = AXI3__BUS__WSTRB;
assign AXI4__BUS__WLAST     = AXI3__BUS__WLAST;
assign AXI4__BUS__BREADY    = AXI3__BUS__BREADY;
assign AXI4__BUS__ARID      = AXI3__BUS__ARID;
assign AXI4__BUS__ARADDR    = AXI3__BUS__ARADDR;
assign AXI4__BUS__ARLEN     = AXI3__BUS__ARLEN;
assign AXI4__BUS__ARSIZE    = AXI3__BUS__ARSIZE;
assign AXI4__BUS__ARBURST   = AXI3__BUS__ARBURST;
assign AXI4__BUS__ARLOCK    = AXI3__BUS__ARLOCK[0];
assign AXI4__BUS__ARCACHE   = AXI3__BUS__ARCACHE;
assign AXI4__BUS__ARPROT    = AXI3__BUS__ARPROT;
assign AXI4__BUS__ARUSER    = AXI3__BUS__ARUSER;
assign AXI4__BUS__ARVALID   = AXI3__BUS__ARVALID;
assign AXI4__BUS__RREADY    = AXI3__BUS__RREADY;
endmodule
// vim:tabstop=4:shiftwidth=4
