//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_Directory_assert(
  input        clock,
  input        reset,
  input        io_read_valid,
  input        wipeDone,
  input        _T_5,
  input  [7:0] victimWays,
  input  [8:0] victimLTE,
  input  [8:0] victimSimp,
  input  [7:0] _T_416,
  input  [7:0] victimWayOH,
  input        _T_452,
  input        _T_453,
  input        _T_454,
  input        _T_455,
  input        _T_456,
  input        _T_457,
  input        _T_458,
  input        _T_459
);
  wire  _T_6; // @[Directory.scala 106:20]
  wire  _T_8; // @[Directory.scala 106:10]
  wire  _T_9; // @[Directory.scala 106:10]
  wire  _T_15; // @[Directory.scala 111:26]
  wire  _T_17; // @[Directory.scala 111:10]
  wire  _T_18; // @[Directory.scala 111:10]
  reg  ren1; // @[Directory.scala 139:21]
  reg [31:0] _RAND_0;
  wire  _T_428; // @[Directory.scala 159:11]
  wire  _T_430; // @[Directory.scala 159:49]
  wire  _T_431; // @[Directory.scala 159:17]
  wire  _T_433; // @[Directory.scala 159:10]
  wire  _T_434; // @[Directory.scala 159:10]
  wire  _T_438; // @[Directory.scala 160:17]
  wire  _T_440; // @[Directory.scala 160:10]
  wire  _T_441; // @[Directory.scala 160:10]
  wire [8:0] _T_444; // @[Directory.scala 161:41]
  wire [8:0] _GEN_88; // @[Directory.scala 161:39]
  wire [8:0] _T_445; // @[Directory.scala 161:39]
  wire  _T_446; // @[Directory.scala 161:54]
  wire  _T_447; // @[Directory.scala 161:17]
  wire  _T_449; // @[Directory.scala 161:10]
  wire  _T_450; // @[Directory.scala 161:10]
  wire [1:0] _T_460; // @[Bitwise.scala 47:55]
  wire [1:0] _T_462; // @[Bitwise.scala 47:55]
  wire [2:0] _T_464; // @[Bitwise.scala 47:55]
  wire [1:0] _T_466; // @[Bitwise.scala 47:55]
  wire [1:0] _T_468; // @[Bitwise.scala 47:55]
  wire [2:0] _T_470; // @[Bitwise.scala 47:55]
  wire [3:0] _T_472; // @[Bitwise.scala 47:55]
  wire  _T_474; // @[Directory.scala 162:42]
  wire  _T_475; // @[Directory.scala 162:17]
  wire  _T_477; // @[Directory.scala 162:10]
  wire  _T_478; // @[Directory.scala 162:10]
  wire [7:0] _T_480; // @[Directory.scala 163:33]
  wire  _T_481; // @[Directory.scala 163:50]
  wire  _T_482; // @[Directory.scala 163:17]
  wire  _T_484; // @[Directory.scala 163:10]
  wire  _T_485; // @[Directory.scala 163:10]
  assign _T_6 = wipeDone | _T_5; // @[Directory.scala 106:20]
  assign _T_8 = _T_6 | reset; // @[Directory.scala 106:10]
  assign _T_9 = ~_T_8; // @[Directory.scala 106:10]
  assign _T_15 = _T_5 | wipeDone; // @[Directory.scala 111:26]
  assign _T_17 = _T_15 | reset; // @[Directory.scala 111:10]
  assign _T_18 = ~_T_17; // @[Directory.scala 111:10]
  assign _T_428 = ~ren1; // @[Directory.scala 159:11]
  assign _T_430 = ~victimLTE[8]; // @[Directory.scala 159:49]
  assign _T_431 = _T_428 | _T_430; // @[Directory.scala 159:17]
  assign _T_433 = _T_431 | reset; // @[Directory.scala 159:10]
  assign _T_434 = ~_T_433; // @[Directory.scala 159:10]
  assign _T_438 = _T_428 | victimLTE[0]; // @[Directory.scala 160:17]
  assign _T_440 = _T_438 | reset; // @[Directory.scala 160:10]
  assign _T_441 = ~_T_440; // @[Directory.scala 160:10]
  assign _T_444 = ~victimSimp; // @[Directory.scala 161:41]
  assign _GEN_88 = {{1'd0}, _T_416}; // @[Directory.scala 161:39]
  assign _T_445 = _GEN_88 & _T_444; // @[Directory.scala 161:39]
  assign _T_446 = _T_445 == 9'h0; // @[Directory.scala 161:54]
  assign _T_447 = _T_428 | _T_446; // @[Directory.scala 161:17]
  assign _T_449 = _T_447 | reset; // @[Directory.scala 161:10]
  assign _T_450 = ~_T_449; // @[Directory.scala 161:10]
  assign _T_460 = _T_452 + _T_453; // @[Bitwise.scala 47:55]
  assign _T_462 = _T_454 + _T_455; // @[Bitwise.scala 47:55]
  assign _T_464 = _T_460 + _T_462; // @[Bitwise.scala 47:55]
  assign _T_466 = _T_456 + _T_457; // @[Bitwise.scala 47:55]
  assign _T_468 = _T_458 + _T_459; // @[Bitwise.scala 47:55]
  assign _T_470 = _T_466 + _T_468; // @[Bitwise.scala 47:55]
  assign _T_472 = _T_464 + _T_470; // @[Bitwise.scala 47:55]
  assign _T_474 = _T_472 == 4'h1; // @[Directory.scala 162:42]
  assign _T_475 = _T_428 | _T_474; // @[Directory.scala 162:17]
  assign _T_477 = _T_475 | reset; // @[Directory.scala 162:10]
  assign _T_478 = ~_T_477; // @[Directory.scala 162:10]
  assign _T_480 = victimWayOH & victimWays; // @[Directory.scala 163:33]
  assign _T_481 = _T_480 != 8'h0; // @[Directory.scala 163:50]
  assign _T_482 = _T_428 | _T_481; // @[Directory.scala 163:17]
  assign _T_484 = _T_482 | reset; // @[Directory.scala 163:10]
  assign _T_485 = ~_T_484; // @[Directory.scala 163:10]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ren1 = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      ren1 <= 1'h0;
    end else begin
      ren1 <= io_read_valid;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_9) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Directory.scala:106 assert (wipeDone || !io.read.valid)\n"); // @[Directory.scala 106:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_9) begin
          $fatal; // @[Directory.scala 106:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_18) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Directory.scala:111 assert (!io.read.valid || wipeDone)\n"); // @[Directory.scala 111:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_18) begin
          $fatal; // @[Directory.scala 111:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_434) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Directory.scala:159 assert (!ren2 || victimLTE(params.cache.ways) === UInt(0))\n"); // @[Directory.scala 159:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_434) begin
          $fatal; // @[Directory.scala 159:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_441) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Directory.scala:160 assert (!ren2 || victimLTE(0) === UInt(1))\n"); // @[Directory.scala 160:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_441) begin
          $fatal; // @[Directory.scala 160:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_450) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Directory.scala:161 assert (!ren2 || ((victimSimp >> 1) & ~victimSimp) === UInt(0)) // monotone\n"); // @[Directory.scala 161:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_450) begin
          $fatal; // @[Directory.scala 161:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_478) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Directory.scala:162 assert (!ren2 || PopCount(victimWayOH) === UInt(1))\n"); // @[Directory.scala 162:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_478) begin
          $fatal; // @[Directory.scala 162:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_485) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Directory.scala:163 assert (!ren2 || (victimWayOH & victimWays).orR()) // always pick a valid way\n"); // @[Directory.scala 163:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_485) begin
          $fatal; // @[Directory.scala 163:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
