// SPDX-License-Identifier: Apache-2.0

//----------------------------------------------------------------------------------------
//  
//----------------------------------------------------------------------------------------
`ifndef SYNTHESIS
interface interrupt_vslave_driver_if (
    input bit CLK,
    input bit RSTN
);
    logic              ENABLE;
    logic              IRQ;

    initial begin
        ENABLE = 0;
    end
endinterface

//----------------------------------------------------------------------------------------
//  
//----------------------------------------------------------------------------------------
class interrupt_vslave_driver_impl extends interrupt_vslave_driver;

    virtual interrupt_vslave_driver_if vif;
    int interrupt_number;
    function new(int interrupt_number, virtual interrupt_vslave_driver_if vif);
        this.vif = vif;
        this.interrupt_number = interrupt_number;
        semifive_hookup_config#(interrupt_vslave_driver,int)::set(interrupt_number,this);
    endfunction

    //--------------------------------------------------------------------------------
    //  Service
    //--------------------------------------------------------------------------------
    virtual task hookup( input logic enable );
        vif.ENABLE = enable;
    endtask

    virtual function logic get_irq;        
        return vif.IRQ;
    endfunction

endclass

`endif

//----------------------------------------------------------------------------------------
//  VIP RTL wrapper
//----------------------------------------------------------------------------------------
module interrupt_vslave_binder #(
    parameter IRQ_NUMBER = 1
)(
    input               CLK__I,
    input               RSTN__I, 

    input               IRQ__I__IN,
    output              IRQ__I__OUT
);
`ifndef SYNTHESIS
    interrupt_vslave_driver_if interrupt_if(CLK__I,RSTN__I);              
    interrupt_vslave_driver_impl driver;
    initial driver = new(IRQ_NUMBER, interrupt_if);
    //-----------------------------------------------------
    assign IRQ__I__OUT = interrupt_if.ENABLE ? 0 : IRQ__I__IN ;
    assign interrupt_if.IRQ = IRQ__I__IN;
`else
    //-----------------------------------------------------
    assign IRQ__I__OUT = IRQ__I__IN ;
`endif
endmodule
