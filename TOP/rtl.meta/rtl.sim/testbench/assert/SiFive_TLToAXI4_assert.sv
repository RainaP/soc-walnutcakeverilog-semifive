//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLToAXI4_assert(
  input         clock,
  input         reset,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [2:0]  auto_in_a_bits_param,
  input  [2:0]  auto_in_a_bits_size,
  input  [6:0]  auto_in_a_bits_source,
  input  [34:0] auto_in_a_bits_address,
  input  [7:0]  auto_in_a_bits_mask,
  input         auto_in_a_bits_corrupt,
  input         auto_in_d_ready,
  input         auto_out_b_valid,
  input  [6:0]  auto_out_b_bits_echo_tl_state_source,
  input         auto_out_r_valid,
  input  [6:0]  auto_out_r_bits_echo_tl_state_source,
  input         _T_241,
  input         _T_213,
  input  [2:0]  _T_443,
  input         _T_556,
  input         _T_528,
  input         _T_185,
  input         _T_157,
  input  [2:0]  _T_414,
  input         _T_500,
  input         _T_472,
  input         _T_129,
  input  [2:0]  _T_385,
  input  [3:0]  _T_356,
  input  [3:0]  _T_327,
  input  [3:0]  _T_298,
  input  [3:0]  _T_269,
  input         _T_55,
  input         _T_58,
  input         _T_74,
  input         _GEN_232,
  input         _T_84,
  input         _T_85,
  input  [2:0]  _T_86_size,
  input  [2:0]  _T_87_size,
  input         _T_133,
  input         _T_136,
  input         _T_161,
  input         _T_164,
  input         _T_189,
  input         _T_192,
  input         _T_217,
  input         _T_220,
  input         _T_245,
  input         _T_248,
  input         _T_273,
  input         _T_276,
  input         _T_302,
  input         _T_305,
  input         _T_331,
  input         _T_334,
  input         _T_360,
  input         _T_363,
  input         _T_389,
  input         _T_392,
  input         _T_418,
  input         _T_421,
  input         _T_447,
  input         _T_450,
  input         _T_476,
  input         _T_479,
  input         _T_504,
  input         _T_507,
  input         _T_532,
  input         _T_535,
  input         _T_560,
  input         _T_563
);
  wire  TLMonitor_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [6:0] TLMonitor_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [34:0] TLMonitor_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [6:0] TLMonitor_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  _T_243; // @[ToAXI4.scala 249:26]
  wire  _T_215; // @[ToAXI4.scala 249:26]
  wire  _T_558; // @[ToAXI4.scala 249:26]
  wire  _T_530; // @[ToAXI4.scala 249:26]
  wire  _T_187; // @[ToAXI4.scala 249:26]
  wire  _T_159; // @[ToAXI4.scala 249:26]
  wire  _T_502; // @[ToAXI4.scala 249:26]
  wire  _T_474; // @[ToAXI4.scala 249:26]
  wire  _T_131; // @[ToAXI4.scala 249:26]
  wire  _T_141; // @[ToAXI4.scala 255:17]
  wire  _T_143; // @[ToAXI4.scala 255:22]
  wire  _T_145; // @[ToAXI4.scala 255:16]
  wire  _T_146; // @[ToAXI4.scala 255:16]
  wire  _T_147; // @[ToAXI4.scala 256:17]
  wire  _T_149; // @[ToAXI4.scala 256:22]
  wire  _T_151; // @[ToAXI4.scala 256:16]
  wire  _T_152; // @[ToAXI4.scala 256:16]
  wire  _T_169; // @[ToAXI4.scala 255:17]
  wire  _T_171; // @[ToAXI4.scala 255:22]
  wire  _T_173; // @[ToAXI4.scala 255:16]
  wire  _T_174; // @[ToAXI4.scala 255:16]
  wire  _T_175; // @[ToAXI4.scala 256:17]
  wire  _T_177; // @[ToAXI4.scala 256:22]
  wire  _T_179; // @[ToAXI4.scala 256:16]
  wire  _T_180; // @[ToAXI4.scala 256:16]
  wire  _T_197; // @[ToAXI4.scala 255:17]
  wire  _T_199; // @[ToAXI4.scala 255:22]
  wire  _T_201; // @[ToAXI4.scala 255:16]
  wire  _T_202; // @[ToAXI4.scala 255:16]
  wire  _T_203; // @[ToAXI4.scala 256:17]
  wire  _T_205; // @[ToAXI4.scala 256:22]
  wire  _T_207; // @[ToAXI4.scala 256:16]
  wire  _T_208; // @[ToAXI4.scala 256:16]
  wire  _T_225; // @[ToAXI4.scala 255:17]
  wire  _T_227; // @[ToAXI4.scala 255:22]
  wire  _T_229; // @[ToAXI4.scala 255:16]
  wire  _T_230; // @[ToAXI4.scala 255:16]
  wire  _T_231; // @[ToAXI4.scala 256:17]
  wire  _T_233; // @[ToAXI4.scala 256:22]
  wire  _T_235; // @[ToAXI4.scala 256:16]
  wire  _T_236; // @[ToAXI4.scala 256:16]
  wire  _T_253; // @[ToAXI4.scala 255:17]
  wire  _T_255; // @[ToAXI4.scala 255:22]
  wire  _T_257; // @[ToAXI4.scala 255:16]
  wire  _T_258; // @[ToAXI4.scala 255:16]
  wire  _T_259; // @[ToAXI4.scala 256:17]
  wire  _T_261; // @[ToAXI4.scala 256:22]
  wire  _T_263; // @[ToAXI4.scala 256:16]
  wire  _T_264; // @[ToAXI4.scala 256:16]
  wire  _T_281; // @[ToAXI4.scala 255:17]
  wire  _T_282; // @[ToAXI4.scala 255:31]
  wire  _T_283; // @[ToAXI4.scala 255:22]
  wire  _T_285; // @[ToAXI4.scala 255:16]
  wire  _T_286; // @[ToAXI4.scala 255:16]
  wire  _T_287; // @[ToAXI4.scala 256:17]
  wire  _T_288; // @[ToAXI4.scala 256:31]
  wire  _T_289; // @[ToAXI4.scala 256:22]
  wire  _T_291; // @[ToAXI4.scala 256:16]
  wire  _T_292; // @[ToAXI4.scala 256:16]
  wire  _T_310; // @[ToAXI4.scala 255:17]
  wire  _T_311; // @[ToAXI4.scala 255:31]
  wire  _T_312; // @[ToAXI4.scala 255:22]
  wire  _T_314; // @[ToAXI4.scala 255:16]
  wire  _T_315; // @[ToAXI4.scala 255:16]
  wire  _T_316; // @[ToAXI4.scala 256:17]
  wire  _T_317; // @[ToAXI4.scala 256:31]
  wire  _T_318; // @[ToAXI4.scala 256:22]
  wire  _T_320; // @[ToAXI4.scala 256:16]
  wire  _T_321; // @[ToAXI4.scala 256:16]
  wire  _T_339; // @[ToAXI4.scala 255:17]
  wire  _T_340; // @[ToAXI4.scala 255:31]
  wire  _T_341; // @[ToAXI4.scala 255:22]
  wire  _T_343; // @[ToAXI4.scala 255:16]
  wire  _T_344; // @[ToAXI4.scala 255:16]
  wire  _T_345; // @[ToAXI4.scala 256:17]
  wire  _T_346; // @[ToAXI4.scala 256:31]
  wire  _T_347; // @[ToAXI4.scala 256:22]
  wire  _T_349; // @[ToAXI4.scala 256:16]
  wire  _T_350; // @[ToAXI4.scala 256:16]
  wire  _T_368; // @[ToAXI4.scala 255:17]
  wire  _T_369; // @[ToAXI4.scala 255:31]
  wire  _T_370; // @[ToAXI4.scala 255:22]
  wire  _T_372; // @[ToAXI4.scala 255:16]
  wire  _T_373; // @[ToAXI4.scala 255:16]
  wire  _T_374; // @[ToAXI4.scala 256:17]
  wire  _T_375; // @[ToAXI4.scala 256:31]
  wire  _T_376; // @[ToAXI4.scala 256:22]
  wire  _T_378; // @[ToAXI4.scala 256:16]
  wire  _T_379; // @[ToAXI4.scala 256:16]
  wire  _T_397; // @[ToAXI4.scala 255:17]
  wire  _T_398; // @[ToAXI4.scala 255:31]
  wire  _T_399; // @[ToAXI4.scala 255:22]
  wire  _T_401; // @[ToAXI4.scala 255:16]
  wire  _T_402; // @[ToAXI4.scala 255:16]
  wire  _T_403; // @[ToAXI4.scala 256:17]
  wire  _T_404; // @[ToAXI4.scala 256:31]
  wire  _T_405; // @[ToAXI4.scala 256:22]
  wire  _T_407; // @[ToAXI4.scala 256:16]
  wire  _T_408; // @[ToAXI4.scala 256:16]
  wire  _T_426; // @[ToAXI4.scala 255:17]
  wire  _T_427; // @[ToAXI4.scala 255:31]
  wire  _T_428; // @[ToAXI4.scala 255:22]
  wire  _T_430; // @[ToAXI4.scala 255:16]
  wire  _T_431; // @[ToAXI4.scala 255:16]
  wire  _T_432; // @[ToAXI4.scala 256:17]
  wire  _T_433; // @[ToAXI4.scala 256:31]
  wire  _T_434; // @[ToAXI4.scala 256:22]
  wire  _T_436; // @[ToAXI4.scala 256:16]
  wire  _T_437; // @[ToAXI4.scala 256:16]
  wire  _T_455; // @[ToAXI4.scala 255:17]
  wire  _T_456; // @[ToAXI4.scala 255:31]
  wire  _T_457; // @[ToAXI4.scala 255:22]
  wire  _T_459; // @[ToAXI4.scala 255:16]
  wire  _T_460; // @[ToAXI4.scala 255:16]
  wire  _T_461; // @[ToAXI4.scala 256:17]
  wire  _T_462; // @[ToAXI4.scala 256:31]
  wire  _T_463; // @[ToAXI4.scala 256:22]
  wire  _T_465; // @[ToAXI4.scala 256:16]
  wire  _T_466; // @[ToAXI4.scala 256:16]
  wire  _T_484; // @[ToAXI4.scala 255:17]
  wire  _T_486; // @[ToAXI4.scala 255:22]
  wire  _T_488; // @[ToAXI4.scala 255:16]
  wire  _T_489; // @[ToAXI4.scala 255:16]
  wire  _T_490; // @[ToAXI4.scala 256:17]
  wire  _T_492; // @[ToAXI4.scala 256:22]
  wire  _T_494; // @[ToAXI4.scala 256:16]
  wire  _T_495; // @[ToAXI4.scala 256:16]
  wire  _T_512; // @[ToAXI4.scala 255:17]
  wire  _T_514; // @[ToAXI4.scala 255:22]
  wire  _T_516; // @[ToAXI4.scala 255:16]
  wire  _T_517; // @[ToAXI4.scala 255:16]
  wire  _T_518; // @[ToAXI4.scala 256:17]
  wire  _T_520; // @[ToAXI4.scala 256:22]
  wire  _T_522; // @[ToAXI4.scala 256:16]
  wire  _T_523; // @[ToAXI4.scala 256:16]
  wire  _T_540; // @[ToAXI4.scala 255:17]
  wire  _T_542; // @[ToAXI4.scala 255:22]
  wire  _T_544; // @[ToAXI4.scala 255:16]
  wire  _T_545; // @[ToAXI4.scala 255:16]
  wire  _T_546; // @[ToAXI4.scala 256:17]
  wire  _T_548; // @[ToAXI4.scala 256:22]
  wire  _T_550; // @[ToAXI4.scala 256:16]
  wire  _T_551; // @[ToAXI4.scala 256:16]
  wire  _T_568; // @[ToAXI4.scala 255:17]
  wire  _T_570; // @[ToAXI4.scala 255:22]
  wire  _T_572; // @[ToAXI4.scala 255:16]
  wire  _T_573; // @[ToAXI4.scala 255:16]
  wire  _T_574; // @[ToAXI4.scala 256:17]
  wire  _T_576; // @[ToAXI4.scala 256:22]
  wire  _T_578; // @[ToAXI4.scala 256:16]
  wire  _T_579; // @[ToAXI4.scala 256:16]
  SiFive_TLMonitor_7_assert TLMonitor ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_clock),
    .reset(TLMonitor_reset),
    .io_in_a_ready(TLMonitor_io_in_a_ready),
    .io_in_a_valid(TLMonitor_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_io_in_a_bits_corrupt),
    .io_in_d_ready(TLMonitor_io_in_d_ready),
    .io_in_d_valid(TLMonitor_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_io_in_d_bits_opcode),
    .io_in_d_bits_size(TLMonitor_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_io_in_d_bits_source),
    .io_in_d_bits_denied(TLMonitor_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_io_in_d_bits_corrupt)
  );
  assign _T_243 = ~_T_241; // @[ToAXI4.scala 249:26]
  assign _T_215 = ~_T_213; // @[ToAXI4.scala 249:26]
  assign _T_558 = ~_T_556; // @[ToAXI4.scala 249:26]
  assign _T_530 = ~_T_528; // @[ToAXI4.scala 249:26]
  assign _T_187 = ~_T_185; // @[ToAXI4.scala 249:26]
  assign _T_159 = ~_T_157; // @[ToAXI4.scala 249:26]
  assign _T_502 = ~_T_500; // @[ToAXI4.scala 249:26]
  assign _T_474 = ~_T_472; // @[ToAXI4.scala 249:26]
  assign _T_131 = ~_T_129; // @[ToAXI4.scala 249:26]
  assign _T_141 = ~_T_136; // @[ToAXI4.scala 255:17]
  assign _T_143 = _T_141 | _T_129; // @[ToAXI4.scala 255:22]
  assign _T_145 = _T_143 | reset; // @[ToAXI4.scala 255:16]
  assign _T_146 = ~_T_145; // @[ToAXI4.scala 255:16]
  assign _T_147 = ~_T_133; // @[ToAXI4.scala 256:17]
  assign _T_149 = _T_147 | _T_131; // @[ToAXI4.scala 256:22]
  assign _T_151 = _T_149 | reset; // @[ToAXI4.scala 256:16]
  assign _T_152 = ~_T_151; // @[ToAXI4.scala 256:16]
  assign _T_169 = ~_T_164; // @[ToAXI4.scala 255:17]
  assign _T_171 = _T_169 | _T_157; // @[ToAXI4.scala 255:22]
  assign _T_173 = _T_171 | reset; // @[ToAXI4.scala 255:16]
  assign _T_174 = ~_T_173; // @[ToAXI4.scala 255:16]
  assign _T_175 = ~_T_161; // @[ToAXI4.scala 256:17]
  assign _T_177 = _T_175 | _T_159; // @[ToAXI4.scala 256:22]
  assign _T_179 = _T_177 | reset; // @[ToAXI4.scala 256:16]
  assign _T_180 = ~_T_179; // @[ToAXI4.scala 256:16]
  assign _T_197 = ~_T_192; // @[ToAXI4.scala 255:17]
  assign _T_199 = _T_197 | _T_185; // @[ToAXI4.scala 255:22]
  assign _T_201 = _T_199 | reset; // @[ToAXI4.scala 255:16]
  assign _T_202 = ~_T_201; // @[ToAXI4.scala 255:16]
  assign _T_203 = ~_T_189; // @[ToAXI4.scala 256:17]
  assign _T_205 = _T_203 | _T_187; // @[ToAXI4.scala 256:22]
  assign _T_207 = _T_205 | reset; // @[ToAXI4.scala 256:16]
  assign _T_208 = ~_T_207; // @[ToAXI4.scala 256:16]
  assign _T_225 = ~_T_220; // @[ToAXI4.scala 255:17]
  assign _T_227 = _T_225 | _T_213; // @[ToAXI4.scala 255:22]
  assign _T_229 = _T_227 | reset; // @[ToAXI4.scala 255:16]
  assign _T_230 = ~_T_229; // @[ToAXI4.scala 255:16]
  assign _T_231 = ~_T_217; // @[ToAXI4.scala 256:17]
  assign _T_233 = _T_231 | _T_215; // @[ToAXI4.scala 256:22]
  assign _T_235 = _T_233 | reset; // @[ToAXI4.scala 256:16]
  assign _T_236 = ~_T_235; // @[ToAXI4.scala 256:16]
  assign _T_253 = ~_T_248; // @[ToAXI4.scala 255:17]
  assign _T_255 = _T_253 | _T_241; // @[ToAXI4.scala 255:22]
  assign _T_257 = _T_255 | reset; // @[ToAXI4.scala 255:16]
  assign _T_258 = ~_T_257; // @[ToAXI4.scala 255:16]
  assign _T_259 = ~_T_245; // @[ToAXI4.scala 256:17]
  assign _T_261 = _T_259 | _T_243; // @[ToAXI4.scala 256:22]
  assign _T_263 = _T_261 | reset; // @[ToAXI4.scala 256:16]
  assign _T_264 = ~_T_263; // @[ToAXI4.scala 256:16]
  assign _T_281 = ~_T_276; // @[ToAXI4.scala 255:17]
  assign _T_282 = _T_269 != 4'h0; // @[ToAXI4.scala 255:31]
  assign _T_283 = _T_281 | _T_282; // @[ToAXI4.scala 255:22]
  assign _T_285 = _T_283 | reset; // @[ToAXI4.scala 255:16]
  assign _T_286 = ~_T_285; // @[ToAXI4.scala 255:16]
  assign _T_287 = ~_T_273; // @[ToAXI4.scala 256:17]
  assign _T_288 = _T_269 != 4'h8; // @[ToAXI4.scala 256:31]
  assign _T_289 = _T_287 | _T_288; // @[ToAXI4.scala 256:22]
  assign _T_291 = _T_289 | reset; // @[ToAXI4.scala 256:16]
  assign _T_292 = ~_T_291; // @[ToAXI4.scala 256:16]
  assign _T_310 = ~_T_305; // @[ToAXI4.scala 255:17]
  assign _T_311 = _T_298 != 4'h0; // @[ToAXI4.scala 255:31]
  assign _T_312 = _T_310 | _T_311; // @[ToAXI4.scala 255:22]
  assign _T_314 = _T_312 | reset; // @[ToAXI4.scala 255:16]
  assign _T_315 = ~_T_314; // @[ToAXI4.scala 255:16]
  assign _T_316 = ~_T_302; // @[ToAXI4.scala 256:17]
  assign _T_317 = _T_298 != 4'h8; // @[ToAXI4.scala 256:31]
  assign _T_318 = _T_316 | _T_317; // @[ToAXI4.scala 256:22]
  assign _T_320 = _T_318 | reset; // @[ToAXI4.scala 256:16]
  assign _T_321 = ~_T_320; // @[ToAXI4.scala 256:16]
  assign _T_339 = ~_T_334; // @[ToAXI4.scala 255:17]
  assign _T_340 = _T_327 != 4'h0; // @[ToAXI4.scala 255:31]
  assign _T_341 = _T_339 | _T_340; // @[ToAXI4.scala 255:22]
  assign _T_343 = _T_341 | reset; // @[ToAXI4.scala 255:16]
  assign _T_344 = ~_T_343; // @[ToAXI4.scala 255:16]
  assign _T_345 = ~_T_331; // @[ToAXI4.scala 256:17]
  assign _T_346 = _T_327 != 4'h8; // @[ToAXI4.scala 256:31]
  assign _T_347 = _T_345 | _T_346; // @[ToAXI4.scala 256:22]
  assign _T_349 = _T_347 | reset; // @[ToAXI4.scala 256:16]
  assign _T_350 = ~_T_349; // @[ToAXI4.scala 256:16]
  assign _T_368 = ~_T_363; // @[ToAXI4.scala 255:17]
  assign _T_369 = _T_356 != 4'h0; // @[ToAXI4.scala 255:31]
  assign _T_370 = _T_368 | _T_369; // @[ToAXI4.scala 255:22]
  assign _T_372 = _T_370 | reset; // @[ToAXI4.scala 255:16]
  assign _T_373 = ~_T_372; // @[ToAXI4.scala 255:16]
  assign _T_374 = ~_T_360; // @[ToAXI4.scala 256:17]
  assign _T_375 = _T_356 != 4'h8; // @[ToAXI4.scala 256:31]
  assign _T_376 = _T_374 | _T_375; // @[ToAXI4.scala 256:22]
  assign _T_378 = _T_376 | reset; // @[ToAXI4.scala 256:16]
  assign _T_379 = ~_T_378; // @[ToAXI4.scala 256:16]
  assign _T_397 = ~_T_392; // @[ToAXI4.scala 255:17]
  assign _T_398 = _T_385 != 3'h0; // @[ToAXI4.scala 255:31]
  assign _T_399 = _T_397 | _T_398; // @[ToAXI4.scala 255:22]
  assign _T_401 = _T_399 | reset; // @[ToAXI4.scala 255:16]
  assign _T_402 = ~_T_401; // @[ToAXI4.scala 255:16]
  assign _T_403 = ~_T_389; // @[ToAXI4.scala 256:17]
  assign _T_404 = _T_385 != 3'h4; // @[ToAXI4.scala 256:31]
  assign _T_405 = _T_403 | _T_404; // @[ToAXI4.scala 256:22]
  assign _T_407 = _T_405 | reset; // @[ToAXI4.scala 256:16]
  assign _T_408 = ~_T_407; // @[ToAXI4.scala 256:16]
  assign _T_426 = ~_T_421; // @[ToAXI4.scala 255:17]
  assign _T_427 = _T_414 != 3'h0; // @[ToAXI4.scala 255:31]
  assign _T_428 = _T_426 | _T_427; // @[ToAXI4.scala 255:22]
  assign _T_430 = _T_428 | reset; // @[ToAXI4.scala 255:16]
  assign _T_431 = ~_T_430; // @[ToAXI4.scala 255:16]
  assign _T_432 = ~_T_418; // @[ToAXI4.scala 256:17]
  assign _T_433 = _T_414 != 3'h7; // @[ToAXI4.scala 256:31]
  assign _T_434 = _T_432 | _T_433; // @[ToAXI4.scala 256:22]
  assign _T_436 = _T_434 | reset; // @[ToAXI4.scala 256:16]
  assign _T_437 = ~_T_436; // @[ToAXI4.scala 256:16]
  assign _T_455 = ~_T_450; // @[ToAXI4.scala 255:17]
  assign _T_456 = _T_443 != 3'h0; // @[ToAXI4.scala 255:31]
  assign _T_457 = _T_455 | _T_456; // @[ToAXI4.scala 255:22]
  assign _T_459 = _T_457 | reset; // @[ToAXI4.scala 255:16]
  assign _T_460 = ~_T_459; // @[ToAXI4.scala 255:16]
  assign _T_461 = ~_T_447; // @[ToAXI4.scala 256:17]
  assign _T_462 = _T_443 != 3'h7; // @[ToAXI4.scala 256:31]
  assign _T_463 = _T_461 | _T_462; // @[ToAXI4.scala 256:22]
  assign _T_465 = _T_463 | reset; // @[ToAXI4.scala 256:16]
  assign _T_466 = ~_T_465; // @[ToAXI4.scala 256:16]
  assign _T_484 = ~_T_479; // @[ToAXI4.scala 255:17]
  assign _T_486 = _T_484 | _T_472; // @[ToAXI4.scala 255:22]
  assign _T_488 = _T_486 | reset; // @[ToAXI4.scala 255:16]
  assign _T_489 = ~_T_488; // @[ToAXI4.scala 255:16]
  assign _T_490 = ~_T_476; // @[ToAXI4.scala 256:17]
  assign _T_492 = _T_490 | _T_474; // @[ToAXI4.scala 256:22]
  assign _T_494 = _T_492 | reset; // @[ToAXI4.scala 256:16]
  assign _T_495 = ~_T_494; // @[ToAXI4.scala 256:16]
  assign _T_512 = ~_T_507; // @[ToAXI4.scala 255:17]
  assign _T_514 = _T_512 | _T_500; // @[ToAXI4.scala 255:22]
  assign _T_516 = _T_514 | reset; // @[ToAXI4.scala 255:16]
  assign _T_517 = ~_T_516; // @[ToAXI4.scala 255:16]
  assign _T_518 = ~_T_504; // @[ToAXI4.scala 256:17]
  assign _T_520 = _T_518 | _T_502; // @[ToAXI4.scala 256:22]
  assign _T_522 = _T_520 | reset; // @[ToAXI4.scala 256:16]
  assign _T_523 = ~_T_522; // @[ToAXI4.scala 256:16]
  assign _T_540 = ~_T_535; // @[ToAXI4.scala 255:17]
  assign _T_542 = _T_540 | _T_528; // @[ToAXI4.scala 255:22]
  assign _T_544 = _T_542 | reset; // @[ToAXI4.scala 255:16]
  assign _T_545 = ~_T_544; // @[ToAXI4.scala 255:16]
  assign _T_546 = ~_T_532; // @[ToAXI4.scala 256:17]
  assign _T_548 = _T_546 | _T_530; // @[ToAXI4.scala 256:22]
  assign _T_550 = _T_548 | reset; // @[ToAXI4.scala 256:16]
  assign _T_551 = ~_T_550; // @[ToAXI4.scala 256:16]
  assign _T_568 = ~_T_563; // @[ToAXI4.scala 255:17]
  assign _T_570 = _T_568 | _T_556; // @[ToAXI4.scala 255:22]
  assign _T_572 = _T_570 | reset; // @[ToAXI4.scala 255:16]
  assign _T_573 = ~_T_572; // @[ToAXI4.scala 255:16]
  assign _T_574 = ~_T_560; // @[ToAXI4.scala 256:17]
  assign _T_576 = _T_574 | _T_558; // @[ToAXI4.scala 256:22]
  assign _T_578 = _T_576 | reset; // @[ToAXI4.scala 256:16]
  assign _T_579 = ~_T_578; // @[ToAXI4.scala 256:16]
  assign TLMonitor_clock = clock;
  assign TLMonitor_reset = reset;
  assign TLMonitor_io_in_a_ready = _T_55 & _T_58; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_valid = auto_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_size = auto_in_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_source = auto_in_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_ready = auto_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_valid = _T_74 ? auto_out_r_valid : auto_out_b_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_opcode = _T_74 ? 3'h1 : 3'h0; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_size = _T_74 ? _T_86_size : _T_87_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_source = _T_74 ? auto_out_r_bits_echo_tl_state_source : auto_out_b_bits_echo_tl_state_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_denied = _T_74 ? _GEN_232 : _T_84; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_corrupt = _T_74 & _T_85; // @[Nodes.scala 26:19]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_146) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_146) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_152) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_152) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_174) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_174) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_180) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_180) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_202) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_202) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_208) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_208) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_230) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_230) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_236) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_236) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_258) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_258) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_264) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_264) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_286) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_286) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_292) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_292) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_315) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_315) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_321) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_321) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_344) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_344) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_350) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_350) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_373) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_373) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_379) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_379) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_402) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_402) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_408) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_408) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_431) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_431) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_437) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_437) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_460) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_460) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_466) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_466) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_489) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_489) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_495) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_495) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_517) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_517) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_523) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_523) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_545) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_545) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_551) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_551) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_573) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_573) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_579) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_579) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
