//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade 
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module mainbus_subsystem (
	 input  [  6:0] MBUS_MAIN_0__AXI4__AIM0_RX__AWID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__AIM0_RX__AWADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__AIM0_RX__AWLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__AIM0_RX__AWSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__AIM0_RX__AWBURST
	,input          MBUS_MAIN_0__AXI4__AIM0_RX__AWLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__AIM0_RX__AWCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__AIM0_RX__AWPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__AIM0_RX__AWUSER
	,input          MBUS_MAIN_0__AXI4__AIM0_RX__AWVALID
	,output         MBUS_MAIN_0__AXI4__AIM0_RX__AWREADY
	,input  [255:0] MBUS_MAIN_0__AXI4__AIM0_RX__WDATA
	,input  [ 31:0] MBUS_MAIN_0__AXI4__AIM0_RX__WSTRB
	,input          MBUS_MAIN_0__AXI4__AIM0_RX__WLAST
	,input          MBUS_MAIN_0__AXI4__AIM0_RX__WVALID
	,output         MBUS_MAIN_0__AXI4__AIM0_RX__WREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__AIM0_RX__BID
	,output         MBUS_MAIN_0__AXI4__AIM0_RX__BVALID
	,output [  1:0] MBUS_MAIN_0__AXI4__AIM0_RX__BRESP
	,input          MBUS_MAIN_0__AXI4__AIM0_RX__BREADY
	,input  [  6:0] MBUS_MAIN_0__AXI4__AIM0_RX__ARID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__AIM0_RX__ARADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__AIM0_RX__ARLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__AIM0_RX__ARSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__AIM0_RX__ARBURST
	,input          MBUS_MAIN_0__AXI4__AIM0_RX__ARLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__AIM0_RX__ARCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__AIM0_RX__ARPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__AIM0_RX__ARUSER
	,input          MBUS_MAIN_0__AXI4__AIM0_RX__ARVALID
	,output         MBUS_MAIN_0__AXI4__AIM0_RX__ARREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__AIM0_RX__RID
	,output [255:0] MBUS_MAIN_0__AXI4__AIM0_RX__RDATA
	,output [  1:0] MBUS_MAIN_0__AXI4__AIM0_RX__RRESP
	,output         MBUS_MAIN_0__AXI4__AIM0_RX__RLAST
	,output         MBUS_MAIN_0__AXI4__AIM0_RX__RVALID
	,input          MBUS_MAIN_0__AXI4__AIM0_RX__RREADY
	,input  [  6:0] MBUS_MAIN_0__AXI4__AIM1_RX__AWID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__AIM1_RX__AWADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__AIM1_RX__AWLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__AIM1_RX__AWSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__AIM1_RX__AWBURST
	,input          MBUS_MAIN_0__AXI4__AIM1_RX__AWLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__AIM1_RX__AWCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__AIM1_RX__AWPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__AIM1_RX__AWUSER
	,input          MBUS_MAIN_0__AXI4__AIM1_RX__AWVALID
	,output         MBUS_MAIN_0__AXI4__AIM1_RX__AWREADY
	,input  [255:0] MBUS_MAIN_0__AXI4__AIM1_RX__WDATA
	,input  [ 31:0] MBUS_MAIN_0__AXI4__AIM1_RX__WSTRB
	,input          MBUS_MAIN_0__AXI4__AIM1_RX__WLAST
	,input          MBUS_MAIN_0__AXI4__AIM1_RX__WVALID
	,output         MBUS_MAIN_0__AXI4__AIM1_RX__WREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__AIM1_RX__BID
	,output         MBUS_MAIN_0__AXI4__AIM1_RX__BVALID
	,output [  1:0] MBUS_MAIN_0__AXI4__AIM1_RX__BRESP
	,input          MBUS_MAIN_0__AXI4__AIM1_RX__BREADY
	,input  [  6:0] MBUS_MAIN_0__AXI4__AIM1_RX__ARID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__AIM1_RX__ARADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__AIM1_RX__ARLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__AIM1_RX__ARSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__AIM1_RX__ARBURST
	,input          MBUS_MAIN_0__AXI4__AIM1_RX__ARLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__AIM1_RX__ARCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__AIM1_RX__ARPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__AIM1_RX__ARUSER
	,input          MBUS_MAIN_0__AXI4__AIM1_RX__ARVALID
	,output         MBUS_MAIN_0__AXI4__AIM1_RX__ARREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__AIM1_RX__RID
	,output [255:0] MBUS_MAIN_0__AXI4__AIM1_RX__RDATA
	,output [  1:0] MBUS_MAIN_0__AXI4__AIM1_RX__RRESP
	,output         MBUS_MAIN_0__AXI4__AIM1_RX__RLAST
	,output         MBUS_MAIN_0__AXI4__AIM1_RX__RVALID
	,input          MBUS_MAIN_0__AXI4__AIM1_RX__RREADY
	,input  [  6:0] MBUS_MAIN_0__AXI4__AIM2_RX__AWID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__AIM2_RX__AWADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__AIM2_RX__AWLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__AIM2_RX__AWSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__AIM2_RX__AWBURST
	,input          MBUS_MAIN_0__AXI4__AIM2_RX__AWLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__AIM2_RX__AWCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__AIM2_RX__AWPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__AIM2_RX__AWUSER
	,input          MBUS_MAIN_0__AXI4__AIM2_RX__AWVALID
	,output         MBUS_MAIN_0__AXI4__AIM2_RX__AWREADY
	,input  [255:0] MBUS_MAIN_0__AXI4__AIM2_RX__WDATA
	,input  [ 31:0] MBUS_MAIN_0__AXI4__AIM2_RX__WSTRB
	,input          MBUS_MAIN_0__AXI4__AIM2_RX__WLAST
	,input          MBUS_MAIN_0__AXI4__AIM2_RX__WVALID
	,output         MBUS_MAIN_0__AXI4__AIM2_RX__WREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__AIM2_RX__BID
	,output         MBUS_MAIN_0__AXI4__AIM2_RX__BVALID
	,output [  1:0] MBUS_MAIN_0__AXI4__AIM2_RX__BRESP
	,input          MBUS_MAIN_0__AXI4__AIM2_RX__BREADY
	,input  [  6:0] MBUS_MAIN_0__AXI4__AIM2_RX__ARID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__AIM2_RX__ARADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__AIM2_RX__ARLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__AIM2_RX__ARSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__AIM2_RX__ARBURST
	,input          MBUS_MAIN_0__AXI4__AIM2_RX__ARLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__AIM2_RX__ARCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__AIM2_RX__ARPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__AIM2_RX__ARUSER
	,input          MBUS_MAIN_0__AXI4__AIM2_RX__ARVALID
	,output         MBUS_MAIN_0__AXI4__AIM2_RX__ARREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__AIM2_RX__RID
	,output [255:0] MBUS_MAIN_0__AXI4__AIM2_RX__RDATA
	,output [  1:0] MBUS_MAIN_0__AXI4__AIM2_RX__RRESP
	,output         MBUS_MAIN_0__AXI4__AIM2_RX__RLAST
	,output         MBUS_MAIN_0__AXI4__AIM2_RX__RVALID
	,input          MBUS_MAIN_0__AXI4__AIM2_RX__RREADY
	,input          PKG__NPU2_DISABLE
	,output         SCU_MAIN_0__IO__RSTNOUT
	,input          SCU_MAIN_0__IO__CLK__OSC
	,input          SCU_MAIN_0__IO__CLK__XTAL
	,input          SCU_MAIN_0__IO__CLKSEL
	,input          SCU_MAIN_0__IO__RSTN
	,output         SCU_MAIN_0__DFT__SCAN_MODE
	,output         SCU_MAIN_0__DFT__SCAN_CLK
	,output         SCU_MAIN_0__DFT__SCAN_RSTN
	,input          SCU_MAIN_0__WDT_RSTREQN
	,output         SCU_MAIN_0__CLK__UART
	,output         SCU_MAIN_0__CLK__SPI
	,output         SCU_MAIN_0__CLK__QSPI
	,output         SCU_MAIN_0__CLK__RTC
	,output         SCU_MAIN_0__CLK__I2C
	,output         SCU_MAIN_0__CLK__I2S
	,output         SCU_MAIN_0__CLK__OTP
	,output         SCU_MAIN_0__CLK__OSC_FREE
	,output         SCU_MAIN_0__CLK__SUBSYSTEM_CPU
	,output         SCU_MAIN_0__CLK__SUBSYSTEM_DDR0
	,output         SCU_MAIN_0__CLK__SUBSYSTEM_DDR1
	,output         SCU_MAIN_0__CLK__SUBSYSTEM_PERI
	,output         SCU_MAIN_0__CLK__SUBSYSTEM_NEPES
	,output         SCU_MAIN_0__CLK__SUBSYSTEM_EYL
	,output         SCU_MAIN_0__CLK__SUBSYSTEM_AIM
	,output         SCU_MAIN_0__CLK__SUBSYSTEM_USB3
	,output         SCU_MAIN_0__CLK__SUBSYSTEM_VIDEO
	,output         SCU_MAIN_0__CLK__SUBSYSTEM_CAMERA
	,output         SCU_MAIN_0__RSTN__SUBSYSTEM_CPU
	,output         SCU_MAIN_0__RSTN__SUBSYSTEM_DDR0
	,output         SCU_MAIN_0__RSTN__SUBSYSTEM_DDR1
	,output         SCU_MAIN_0__RSTN__SUBSYSTEM_PERI
	,output         SCU_MAIN_0__RSTN__SUBSYSTEM_NEPES
	,output         SCU_MAIN_0__RSTN__SUBSYSTEM_EYL
	,output         SCU_MAIN_0__RSTN__SUBSYSTEM_AIM
	,output         SCU_MAIN_0__RSTN__SUBSYSTEM_USB3
	,output         SCU_MAIN_0__RSTN__SUBSYSTEM_VIDEO
	,output         SCU_MAIN_0__RSTN__SUBSYSTEM_CAMERA
	,output [ 36:0] SCU_MAIN_0__RESET_VECTOR_0
	,output [ 36:0] SCU_MAIN_0__RESET_VECTOR_1
	,output [ 36:0] SCU_MAIN_0__NMI_INTERRUPT_VECTOR_0
	,output [ 36:0] SCU_MAIN_0__NMI_INTERRUPT_VECTOR_1
	,output [ 36:0] SCU_MAIN_0__NMI_EXCEPTION_VECTOR_0
	,output [ 36:0] SCU_MAIN_0__NMI_EXCEPTION_VECTOR_1
	,output         SCU_MAIN_0__LPDDR4_1__RET_OFF
	,output         SCU_MAIN_0__LPDDR4_0__RET_OFF
	,output         MBUS_MAIN_0__CLK__CPU_TX
	,output         MBUS_MAIN_0__CLK__DDR0_TX
	,output         MBUS_MAIN_0__CLK__DDR1_TX
	,output         MBUS_MAIN_0__RSTN__CPU_TX
	,output         MBUS_MAIN_0__RSTN__DDR0_TX
	,output         MBUS_MAIN_0__RSTN__DDR1_TX
	,output         MBUS_MAIN_0__CLK__CPU_RX
	,output         MBUS_MAIN_0__CLK__PERI_RX
	,output         MBUS_MAIN_0__CLK__USB3_RX
	,output         MBUS_MAIN_0__CLK__CAMERA_RX
	,output         MBUS_MAIN_0__CLK__VIDEO_RX
	,output         MBUS_MAIN_0__CLK__NEPES0_RX
	,output         MBUS_MAIN_0__CLK__NEPES1_RX
	,output         MBUS_MAIN_0__CLK__NEPES2_RX
	,output         MBUS_MAIN_0__CLK__EYL0_RX
	,output         MBUS_MAIN_0__CLK__EYL1_RX
	,output         MBUS_MAIN_0__CLK__EYL2_RX
	,output         MBUS_MAIN_0__CLK__AIM0_RX
	,output         MBUS_MAIN_0__CLK__AIM1_RX
	,output         MBUS_MAIN_0__CLK__AIM2_RX
	,output         MBUS_MAIN_0__RSTN__CPU_RX
	,output         MBUS_MAIN_0__RSTN__PERI_RX
	,output         MBUS_MAIN_0__RSTN__USB3_RX
	,output         MBUS_MAIN_0__RSTN__CAMERA_RX
	,output         MBUS_MAIN_0__RSTN__VIDEO_RX
	,output         MBUS_MAIN_0__RSTN__NEPES0_RX
	,output         MBUS_MAIN_0__RSTN__NEPES1_RX
	,output         MBUS_MAIN_0__RSTN__NEPES2_RX
	,output         MBUS_MAIN_0__RSTN__EYL0_RX
	,output         MBUS_MAIN_0__RSTN__EYL1_RX
	,output         MBUS_MAIN_0__RSTN__EYL2_RX
	,output         MBUS_MAIN_0__RSTN__AIM0_RX
	,output         MBUS_MAIN_0__RSTN__AIM1_RX
	,output         MBUS_MAIN_0__RSTN__AIM2_RX
	,output         PBUS_MAIN_0__CLK__PERI_TX
	,output         PBUS_MAIN_0__CLK__DDR0_TX
	,output         PBUS_MAIN_0__CLK__DDR1_TX
	,output         PBUS_MAIN_0__CLK__USB3_TX
	,output         PBUS_MAIN_0__CLK__CAMERA_TX
	,output         PBUS_MAIN_0__CLK__VIDEO_TX
	,output         PBUS_MAIN_0__CLK__NEPES_TX
	,output         PBUS_MAIN_0__CLK__EYL_TX
	,output         PBUS_MAIN_0__CLK__AIM_TX
	,output         PBUS_MAIN_0__RSTN__PERI_TX
	,output         PBUS_MAIN_0__RSTN__DDR0_TX
	,output         PBUS_MAIN_0__RSTN__DDR1_TX
	,output         PBUS_MAIN_0__RSTN__USB3_TX
	,output         PBUS_MAIN_0__RSTN__CAMERA_TX
	,output         PBUS_MAIN_0__RSTN__VIDEO_TX
	,output         PBUS_MAIN_0__RSTN__NEPES_TX
	,output         PBUS_MAIN_0__RSTN__EYL_TX
	,output         PBUS_MAIN_0__RSTN__AIM_TX
	,output         PBUS_MAIN_0__CLK__CPU_RX
	,output         PBUS_MAIN_0__RSTN__CPU_RX
	,output         PBUS_MAIN_0__AXI4__CPU_RX__AWREADY
	,output         PBUS_MAIN_0__AXI4__CPU_RX__WREADY
	,output         PBUS_MAIN_0__AXI4__CPU_RX__BID
	,output         PBUS_MAIN_0__AXI4__CPU_RX__BVALID
	,output [  1:0] PBUS_MAIN_0__AXI4__CPU_RX__BRESP
	,output         PBUS_MAIN_0__AXI4__CPU_RX__ARREADY
	,output         PBUS_MAIN_0__AXI4__CPU_RX__RID
	,output [ 31:0] PBUS_MAIN_0__AXI4__CPU_RX__RDATA
	,output [  1:0] PBUS_MAIN_0__AXI4__CPU_RX__RRESP
	,output         PBUS_MAIN_0__AXI4__CPU_RX__RLAST
	,output         PBUS_MAIN_0__AXI4__CPU_RX__RVALID
	,output [ 31:0] PBUS_MAIN_0__AXI4__PERI_TX__AWADDR
	,output         PBUS_MAIN_0__AXI4__PERI_TX__AWID
	,output [  7:0] PBUS_MAIN_0__AXI4__PERI_TX__AWLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__PERI_TX__AWSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__PERI_TX__AWBURST
	,output         PBUS_MAIN_0__AXI4__PERI_TX__AWLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__PERI_TX__AWCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__PERI_TX__AWPROT
	,output [ 31:0] PBUS_MAIN_0__AXI4__PERI_TX__WDATA
	,output [  3:0] PBUS_MAIN_0__AXI4__PERI_TX__WSTRB
	,output         PBUS_MAIN_0__AXI4__PERI_TX__ARID
	,output [ 31:0] PBUS_MAIN_0__AXI4__PERI_TX__ARADDR
	,output [  7:0] PBUS_MAIN_0__AXI4__PERI_TX__ARLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__PERI_TX__ARSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__PERI_TX__ARBURST
	,output         PBUS_MAIN_0__AXI4__PERI_TX__ARLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__PERI_TX__ARCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__PERI_TX__ARPROT
	,output         PBUS_MAIN_0__AXI4__PERI_TX__AWVALID
	,output         PBUS_MAIN_0__AXI4__PERI_TX__WVALID
	,output         PBUS_MAIN_0__AXI4__PERI_TX__WLAST
	,output         PBUS_MAIN_0__AXI4__PERI_TX__BREADY
	,output         PBUS_MAIN_0__AXI4__PERI_TX__ARVALID
	,output         PBUS_MAIN_0__AXI4__PERI_TX__RREADY
	,output [ 31:0] PBUS_MAIN_0__AXI4__DDR0_TX__AWADDR
	,output         PBUS_MAIN_0__AXI4__DDR0_TX__AWID
	,output [  7:0] PBUS_MAIN_0__AXI4__DDR0_TX__AWLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__DDR0_TX__AWSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__DDR0_TX__AWBURST
	,output         PBUS_MAIN_0__AXI4__DDR0_TX__AWLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__DDR0_TX__AWCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__DDR0_TX__AWPROT
	,output [ 31:0] PBUS_MAIN_0__AXI4__DDR0_TX__WDATA
	,output [  3:0] PBUS_MAIN_0__AXI4__DDR0_TX__WSTRB
	,output         PBUS_MAIN_0__AXI4__DDR0_TX__ARID
	,output [ 31:0] PBUS_MAIN_0__AXI4__DDR0_TX__ARADDR
	,output [  7:0] PBUS_MAIN_0__AXI4__DDR0_TX__ARLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__DDR0_TX__ARSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__DDR0_TX__ARBURST
	,output         PBUS_MAIN_0__AXI4__DDR0_TX__ARLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__DDR0_TX__ARCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__DDR0_TX__ARPROT
	,output         PBUS_MAIN_0__AXI4__DDR0_TX__AWVALID
	,output         PBUS_MAIN_0__AXI4__DDR0_TX__WVALID
	,output         PBUS_MAIN_0__AXI4__DDR0_TX__WLAST
	,output         PBUS_MAIN_0__AXI4__DDR0_TX__BREADY
	,output         PBUS_MAIN_0__AXI4__DDR0_TX__ARVALID
	,output         PBUS_MAIN_0__AXI4__DDR0_TX__RREADY
	,output [ 31:0] PBUS_MAIN_0__AXI4__DDR1_TX__AWADDR
	,output         PBUS_MAIN_0__AXI4__DDR1_TX__AWID
	,output [  7:0] PBUS_MAIN_0__AXI4__DDR1_TX__AWLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__DDR1_TX__AWSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__DDR1_TX__AWBURST
	,output         PBUS_MAIN_0__AXI4__DDR1_TX__AWLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__DDR1_TX__AWCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__DDR1_TX__AWPROT
	,output [ 31:0] PBUS_MAIN_0__AXI4__DDR1_TX__WDATA
	,output [  3:0] PBUS_MAIN_0__AXI4__DDR1_TX__WSTRB
	,output         PBUS_MAIN_0__AXI4__DDR1_TX__ARID
	,output [ 31:0] PBUS_MAIN_0__AXI4__DDR1_TX__ARADDR
	,output [  7:0] PBUS_MAIN_0__AXI4__DDR1_TX__ARLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__DDR1_TX__ARSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__DDR1_TX__ARBURST
	,output         PBUS_MAIN_0__AXI4__DDR1_TX__ARLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__DDR1_TX__ARCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__DDR1_TX__ARPROT
	,output         PBUS_MAIN_0__AXI4__DDR1_TX__AWVALID
	,output         PBUS_MAIN_0__AXI4__DDR1_TX__WVALID
	,output         PBUS_MAIN_0__AXI4__DDR1_TX__WLAST
	,output         PBUS_MAIN_0__AXI4__DDR1_TX__BREADY
	,output         PBUS_MAIN_0__AXI4__DDR1_TX__ARVALID
	,output         PBUS_MAIN_0__AXI4__DDR1_TX__RREADY
	,output [ 31:0] PBUS_MAIN_0__AXI4__USB3_TX__AWADDR
	,output         PBUS_MAIN_0__AXI4__USB3_TX__AWID
	,output [  7:0] PBUS_MAIN_0__AXI4__USB3_TX__AWLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__USB3_TX__AWSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__USB3_TX__AWBURST
	,output         PBUS_MAIN_0__AXI4__USB3_TX__AWLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__USB3_TX__AWCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__USB3_TX__AWPROT
	,output [ 31:0] PBUS_MAIN_0__AXI4__USB3_TX__WDATA
	,output [  3:0] PBUS_MAIN_0__AXI4__USB3_TX__WSTRB
	,output         PBUS_MAIN_0__AXI4__USB3_TX__ARID
	,output [ 31:0] PBUS_MAIN_0__AXI4__USB3_TX__ARADDR
	,output [  7:0] PBUS_MAIN_0__AXI4__USB3_TX__ARLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__USB3_TX__ARSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__USB3_TX__ARBURST
	,output         PBUS_MAIN_0__AXI4__USB3_TX__ARLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__USB3_TX__ARCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__USB3_TX__ARPROT
	,output         PBUS_MAIN_0__AXI4__USB3_TX__AWVALID
	,output         PBUS_MAIN_0__AXI4__USB3_TX__WVALID
	,output         PBUS_MAIN_0__AXI4__USB3_TX__WLAST
	,output         PBUS_MAIN_0__AXI4__USB3_TX__BREADY
	,output         PBUS_MAIN_0__AXI4__USB3_TX__ARVALID
	,output         PBUS_MAIN_0__AXI4__USB3_TX__RREADY
	,output [ 31:0] PBUS_MAIN_0__AXI4__NEPES_TX__AWADDR
	,output         PBUS_MAIN_0__AXI4__NEPES_TX__AWID
	,output [  7:0] PBUS_MAIN_0__AXI4__NEPES_TX__AWLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__NEPES_TX__AWSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__NEPES_TX__AWBURST
	,output         PBUS_MAIN_0__AXI4__NEPES_TX__AWLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__NEPES_TX__AWCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__NEPES_TX__AWPROT
	,output [ 31:0] PBUS_MAIN_0__AXI4__NEPES_TX__WDATA
	,output [  3:0] PBUS_MAIN_0__AXI4__NEPES_TX__WSTRB
	,output         PBUS_MAIN_0__AXI4__NEPES_TX__ARID
	,output [ 31:0] PBUS_MAIN_0__AXI4__NEPES_TX__ARADDR
	,output [  7:0] PBUS_MAIN_0__AXI4__NEPES_TX__ARLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__NEPES_TX__ARSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__NEPES_TX__ARBURST
	,output         PBUS_MAIN_0__AXI4__NEPES_TX__ARLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__NEPES_TX__ARCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__NEPES_TX__ARPROT
	,output         PBUS_MAIN_0__AXI4__NEPES_TX__AWVALID
	,output         PBUS_MAIN_0__AXI4__NEPES_TX__WVALID
	,output         PBUS_MAIN_0__AXI4__NEPES_TX__WLAST
	,output         PBUS_MAIN_0__AXI4__NEPES_TX__BREADY
	,output         PBUS_MAIN_0__AXI4__NEPES_TX__ARVALID
	,output         PBUS_MAIN_0__AXI4__NEPES_TX__RREADY
	,output [ 31:0] PBUS_MAIN_0__AXI4__EYL_TX__AWADDR
	,output         PBUS_MAIN_0__AXI4__EYL_TX__AWID
	,output [  7:0] PBUS_MAIN_0__AXI4__EYL_TX__AWLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__EYL_TX__AWSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__EYL_TX__AWBURST
	,output         PBUS_MAIN_0__AXI4__EYL_TX__AWLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__EYL_TX__AWCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__EYL_TX__AWPROT
	,output [ 31:0] PBUS_MAIN_0__AXI4__EYL_TX__WDATA
	,output [  3:0] PBUS_MAIN_0__AXI4__EYL_TX__WSTRB
	,output         PBUS_MAIN_0__AXI4__EYL_TX__ARID
	,output [ 31:0] PBUS_MAIN_0__AXI4__EYL_TX__ARADDR
	,output [  7:0] PBUS_MAIN_0__AXI4__EYL_TX__ARLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__EYL_TX__ARSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__EYL_TX__ARBURST
	,output         PBUS_MAIN_0__AXI4__EYL_TX__ARLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__EYL_TX__ARCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__EYL_TX__ARPROT
	,output         PBUS_MAIN_0__AXI4__EYL_TX__AWVALID
	,output         PBUS_MAIN_0__AXI4__EYL_TX__WVALID
	,output         PBUS_MAIN_0__AXI4__EYL_TX__WLAST
	,output         PBUS_MAIN_0__AXI4__EYL_TX__BREADY
	,output         PBUS_MAIN_0__AXI4__EYL_TX__ARVALID
	,output         PBUS_MAIN_0__AXI4__EYL_TX__RREADY
	,output [ 31:0] PBUS_MAIN_0__AXI4__AIM_TX__AWADDR
	,output         PBUS_MAIN_0__AXI4__AIM_TX__AWID
	,output [  7:0] PBUS_MAIN_0__AXI4__AIM_TX__AWLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__AIM_TX__AWSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__AIM_TX__AWBURST
	,output         PBUS_MAIN_0__AXI4__AIM_TX__AWLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__AIM_TX__AWCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__AIM_TX__AWPROT
	,output [ 31:0] PBUS_MAIN_0__AXI4__AIM_TX__WDATA
	,output [  3:0] PBUS_MAIN_0__AXI4__AIM_TX__WSTRB
	,output         PBUS_MAIN_0__AXI4__AIM_TX__ARID
	,output [ 31:0] PBUS_MAIN_0__AXI4__AIM_TX__ARADDR
	,output [  7:0] PBUS_MAIN_0__AXI4__AIM_TX__ARLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__AIM_TX__ARSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__AIM_TX__ARBURST
	,output         PBUS_MAIN_0__AXI4__AIM_TX__ARLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__AIM_TX__ARCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__AIM_TX__ARPROT
	,output         PBUS_MAIN_0__AXI4__AIM_TX__AWVALID
	,output         PBUS_MAIN_0__AXI4__AIM_TX__WVALID
	,output         PBUS_MAIN_0__AXI4__AIM_TX__WLAST
	,output         PBUS_MAIN_0__AXI4__AIM_TX__BREADY
	,output         PBUS_MAIN_0__AXI4__AIM_TX__ARVALID
	,output         PBUS_MAIN_0__AXI4__AIM_TX__RREADY
	,output [ 31:0] PBUS_MAIN_0__AXI4__CAMERA_TX__AWADDR
	,output         PBUS_MAIN_0__AXI4__CAMERA_TX__AWID
	,output [  7:0] PBUS_MAIN_0__AXI4__CAMERA_TX__AWLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__CAMERA_TX__AWSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__CAMERA_TX__AWBURST
	,output         PBUS_MAIN_0__AXI4__CAMERA_TX__AWLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__CAMERA_TX__AWCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__CAMERA_TX__AWPROT
	,output [ 31:0] PBUS_MAIN_0__AXI4__CAMERA_TX__WDATA
	,output [  3:0] PBUS_MAIN_0__AXI4__CAMERA_TX__WSTRB
	,output         PBUS_MAIN_0__AXI4__CAMERA_TX__ARID
	,output [ 31:0] PBUS_MAIN_0__AXI4__CAMERA_TX__ARADDR
	,output [  7:0] PBUS_MAIN_0__AXI4__CAMERA_TX__ARLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__CAMERA_TX__ARSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__CAMERA_TX__ARBURST
	,output         PBUS_MAIN_0__AXI4__CAMERA_TX__ARLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__CAMERA_TX__ARCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__CAMERA_TX__ARPROT
	,output         PBUS_MAIN_0__AXI4__CAMERA_TX__AWVALID
	,output         PBUS_MAIN_0__AXI4__CAMERA_TX__WVALID
	,output         PBUS_MAIN_0__AXI4__CAMERA_TX__WLAST
	,output         PBUS_MAIN_0__AXI4__CAMERA_TX__BREADY
	,output         PBUS_MAIN_0__AXI4__CAMERA_TX__ARVALID
	,output         PBUS_MAIN_0__AXI4__CAMERA_TX__RREADY
	,output [ 31:0] PBUS_MAIN_0__AXI4__VIDEO_TX__AWADDR
	,output         PBUS_MAIN_0__AXI4__VIDEO_TX__AWID
	,output [  7:0] PBUS_MAIN_0__AXI4__VIDEO_TX__AWLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__VIDEO_TX__AWSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__VIDEO_TX__AWBURST
	,output         PBUS_MAIN_0__AXI4__VIDEO_TX__AWLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__VIDEO_TX__AWCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__VIDEO_TX__AWPROT
	,output [ 31:0] PBUS_MAIN_0__AXI4__VIDEO_TX__WDATA
	,output [  3:0] PBUS_MAIN_0__AXI4__VIDEO_TX__WSTRB
	,output         PBUS_MAIN_0__AXI4__VIDEO_TX__ARID
	,output [ 31:0] PBUS_MAIN_0__AXI4__VIDEO_TX__ARADDR
	,output [  7:0] PBUS_MAIN_0__AXI4__VIDEO_TX__ARLEN
	,output [  2:0] PBUS_MAIN_0__AXI4__VIDEO_TX__ARSIZE
	,output [  1:0] PBUS_MAIN_0__AXI4__VIDEO_TX__ARBURST
	,output         PBUS_MAIN_0__AXI4__VIDEO_TX__ARLOCK
	,output [  3:0] PBUS_MAIN_0__AXI4__VIDEO_TX__ARCACHE
	,output [  2:0] PBUS_MAIN_0__AXI4__VIDEO_TX__ARPROT
	,output         PBUS_MAIN_0__AXI4__VIDEO_TX__AWVALID
	,output         PBUS_MAIN_0__AXI4__VIDEO_TX__WVALID
	,output         PBUS_MAIN_0__AXI4__VIDEO_TX__WLAST
	,output         PBUS_MAIN_0__AXI4__VIDEO_TX__BREADY
	,output         PBUS_MAIN_0__AXI4__VIDEO_TX__ARVALID
	,output         PBUS_MAIN_0__AXI4__VIDEO_TX__RREADY
	,input          PBUS_MAIN_0__AXI4__CPU_RX__AWID
	,input  [ 31:0] PBUS_MAIN_0__AXI4__CPU_RX__AWADDR
	,input  [  7:0] PBUS_MAIN_0__AXI4__CPU_RX__AWLEN
	,input  [  2:0] PBUS_MAIN_0__AXI4__CPU_RX__AWSIZE
	,input  [  1:0] PBUS_MAIN_0__AXI4__CPU_RX__AWBURST
	,input          PBUS_MAIN_0__AXI4__CPU_RX__AWLOCK
	,input  [  3:0] PBUS_MAIN_0__AXI4__CPU_RX__AWCACHE
	,input  [  2:0] PBUS_MAIN_0__AXI4__CPU_RX__AWPROT
	,input          PBUS_MAIN_0__AXI4__CPU_RX__AWVALID
	,input  [ 31:0] PBUS_MAIN_0__AXI4__CPU_RX__WDATA
	,input  [  3:0] PBUS_MAIN_0__AXI4__CPU_RX__WSTRB
	,input          PBUS_MAIN_0__AXI4__CPU_RX__WLAST
	,input          PBUS_MAIN_0__AXI4__CPU_RX__WVALID
	,input          PBUS_MAIN_0__AXI4__CPU_RX__BREADY
	,input          PBUS_MAIN_0__AXI4__CPU_RX__ARID
	,input  [ 31:0] PBUS_MAIN_0__AXI4__CPU_RX__ARADDR
	,input  [  7:0] PBUS_MAIN_0__AXI4__CPU_RX__ARLEN
	,input  [  2:0] PBUS_MAIN_0__AXI4__CPU_RX__ARSIZE
	,input  [  1:0] PBUS_MAIN_0__AXI4__CPU_RX__ARBURST
	,input          PBUS_MAIN_0__AXI4__CPU_RX__ARLOCK
	,input  [  3:0] PBUS_MAIN_0__AXI4__CPU_RX__ARCACHE
	,input  [  2:0] PBUS_MAIN_0__AXI4__CPU_RX__ARPROT
	,input          PBUS_MAIN_0__AXI4__CPU_RX__ARVALID
	,input          PBUS_MAIN_0__AXI4__CPU_RX__RREADY
	,input          PBUS_MAIN_0__AXI4__PERI_TX__BID
	,input  [  1:0] PBUS_MAIN_0__AXI4__PERI_TX__BRESP
	,input          PBUS_MAIN_0__AXI4__PERI_TX__RID
	,input  [ 31:0] PBUS_MAIN_0__AXI4__PERI_TX__RDATA
	,input  [  1:0] PBUS_MAIN_0__AXI4__PERI_TX__RRESP
	,input          PBUS_MAIN_0__AXI4__PERI_TX__AWREADY
	,input          PBUS_MAIN_0__AXI4__PERI_TX__WREADY
	,input          PBUS_MAIN_0__AXI4__PERI_TX__BVALID
	,input          PBUS_MAIN_0__AXI4__PERI_TX__ARREADY
	,input          PBUS_MAIN_0__AXI4__PERI_TX__RVALID
	,input          PBUS_MAIN_0__AXI4__PERI_TX__RLAST
	,input          PBUS_MAIN_0__AXI4__DDR0_TX__BID
	,input  [  1:0] PBUS_MAIN_0__AXI4__DDR0_TX__BRESP
	,input          PBUS_MAIN_0__AXI4__DDR0_TX__RID
	,input  [ 31:0] PBUS_MAIN_0__AXI4__DDR0_TX__RDATA
	,input  [  1:0] PBUS_MAIN_0__AXI4__DDR0_TX__RRESP
	,input          PBUS_MAIN_0__AXI4__DDR0_TX__AWREADY
	,input          PBUS_MAIN_0__AXI4__DDR0_TX__WREADY
	,input          PBUS_MAIN_0__AXI4__DDR0_TX__BVALID
	,input          PBUS_MAIN_0__AXI4__DDR0_TX__ARREADY
	,input          PBUS_MAIN_0__AXI4__DDR0_TX__RVALID
	,input          PBUS_MAIN_0__AXI4__DDR0_TX__RLAST
	,input          PBUS_MAIN_0__AXI4__DDR1_TX__BID
	,input  [  1:0] PBUS_MAIN_0__AXI4__DDR1_TX__BRESP
	,input          PBUS_MAIN_0__AXI4__DDR1_TX__RID
	,input  [ 31:0] PBUS_MAIN_0__AXI4__DDR1_TX__RDATA
	,input  [  1:0] PBUS_MAIN_0__AXI4__DDR1_TX__RRESP
	,input          PBUS_MAIN_0__AXI4__DDR1_TX__AWREADY
	,input          PBUS_MAIN_0__AXI4__DDR1_TX__WREADY
	,input          PBUS_MAIN_0__AXI4__DDR1_TX__BVALID
	,input          PBUS_MAIN_0__AXI4__DDR1_TX__ARREADY
	,input          PBUS_MAIN_0__AXI4__DDR1_TX__RVALID
	,input          PBUS_MAIN_0__AXI4__DDR1_TX__RLAST
	,input          PBUS_MAIN_0__AXI4__USB3_TX__BID
	,input  [  1:0] PBUS_MAIN_0__AXI4__USB3_TX__BRESP
	,input          PBUS_MAIN_0__AXI4__USB3_TX__RID
	,input  [ 31:0] PBUS_MAIN_0__AXI4__USB3_TX__RDATA
	,input  [  1:0] PBUS_MAIN_0__AXI4__USB3_TX__RRESP
	,input          PBUS_MAIN_0__AXI4__USB3_TX__AWREADY
	,input          PBUS_MAIN_0__AXI4__USB3_TX__WREADY
	,input          PBUS_MAIN_0__AXI4__USB3_TX__BVALID
	,input          PBUS_MAIN_0__AXI4__USB3_TX__ARREADY
	,input          PBUS_MAIN_0__AXI4__USB3_TX__RVALID
	,input          PBUS_MAIN_0__AXI4__USB3_TX__RLAST
	,input          PBUS_MAIN_0__AXI4__NEPES_TX__BID
	,input  [  1:0] PBUS_MAIN_0__AXI4__NEPES_TX__BRESP
	,input          PBUS_MAIN_0__AXI4__NEPES_TX__RID
	,input  [ 31:0] PBUS_MAIN_0__AXI4__NEPES_TX__RDATA
	,input  [  1:0] PBUS_MAIN_0__AXI4__NEPES_TX__RRESP
	,input          PBUS_MAIN_0__AXI4__NEPES_TX__AWREADY
	,input          PBUS_MAIN_0__AXI4__NEPES_TX__WREADY
	,input          PBUS_MAIN_0__AXI4__NEPES_TX__BVALID
	,input          PBUS_MAIN_0__AXI4__NEPES_TX__ARREADY
	,input          PBUS_MAIN_0__AXI4__NEPES_TX__RVALID
	,input          PBUS_MAIN_0__AXI4__NEPES_TX__RLAST
	,input          PBUS_MAIN_0__AXI4__EYL_TX__BID
	,input  [  1:0] PBUS_MAIN_0__AXI4__EYL_TX__BRESP
	,input          PBUS_MAIN_0__AXI4__EYL_TX__RID
	,input  [ 31:0] PBUS_MAIN_0__AXI4__EYL_TX__RDATA
	,input  [  1:0] PBUS_MAIN_0__AXI4__EYL_TX__RRESP
	,input          PBUS_MAIN_0__AXI4__EYL_TX__AWREADY
	,input          PBUS_MAIN_0__AXI4__EYL_TX__WREADY
	,input          PBUS_MAIN_0__AXI4__EYL_TX__BVALID
	,input          PBUS_MAIN_0__AXI4__EYL_TX__ARREADY
	,input          PBUS_MAIN_0__AXI4__EYL_TX__RVALID
	,input          PBUS_MAIN_0__AXI4__EYL_TX__RLAST
	,input          PBUS_MAIN_0__AXI4__AIM_TX__BID
	,input  [  1:0] PBUS_MAIN_0__AXI4__AIM_TX__BRESP
	,input          PBUS_MAIN_0__AXI4__AIM_TX__RID
	,input  [ 31:0] PBUS_MAIN_0__AXI4__AIM_TX__RDATA
	,input  [  1:0] PBUS_MAIN_0__AXI4__AIM_TX__RRESP
	,input          PBUS_MAIN_0__AXI4__AIM_TX__AWREADY
	,input          PBUS_MAIN_0__AXI4__AIM_TX__WREADY
	,input          PBUS_MAIN_0__AXI4__AIM_TX__BVALID
	,input          PBUS_MAIN_0__AXI4__AIM_TX__ARREADY
	,input          PBUS_MAIN_0__AXI4__AIM_TX__RVALID
	,input          PBUS_MAIN_0__AXI4__AIM_TX__RLAST
	,input          PBUS_MAIN_0__AXI4__CAMERA_TX__BID
	,input  [  1:0] PBUS_MAIN_0__AXI4__CAMERA_TX__BRESP
	,input          PBUS_MAIN_0__AXI4__CAMERA_TX__RID
	,input  [ 31:0] PBUS_MAIN_0__AXI4__CAMERA_TX__RDATA
	,input  [  1:0] PBUS_MAIN_0__AXI4__CAMERA_TX__RRESP
	,input          PBUS_MAIN_0__AXI4__CAMERA_TX__AWREADY
	,input          PBUS_MAIN_0__AXI4__CAMERA_TX__WREADY
	,input          PBUS_MAIN_0__AXI4__CAMERA_TX__BVALID
	,input          PBUS_MAIN_0__AXI4__CAMERA_TX__ARREADY
	,input          PBUS_MAIN_0__AXI4__CAMERA_TX__RVALID
	,input          PBUS_MAIN_0__AXI4__CAMERA_TX__RLAST
	,input          PBUS_MAIN_0__AXI4__VIDEO_TX__BID
	,input  [  1:0] PBUS_MAIN_0__AXI4__VIDEO_TX__BRESP
	,input          PBUS_MAIN_0__AXI4__VIDEO_TX__RID
	,input  [ 31:0] PBUS_MAIN_0__AXI4__VIDEO_TX__RDATA
	,input  [  1:0] PBUS_MAIN_0__AXI4__VIDEO_TX__RRESP
	,input          PBUS_MAIN_0__AXI4__VIDEO_TX__AWREADY
	,input          PBUS_MAIN_0__AXI4__VIDEO_TX__WREADY
	,input          PBUS_MAIN_0__AXI4__VIDEO_TX__BVALID
	,input          PBUS_MAIN_0__AXI4__VIDEO_TX__ARREADY
	,input          PBUS_MAIN_0__AXI4__VIDEO_TX__RVALID
	,input          PBUS_MAIN_0__AXI4__VIDEO_TX__RLAST
	,output         MBUS_MAIN_0__AXI4__CPU_RX__AWREADY
	,output         MBUS_MAIN_0__AXI4__CPU_RX__WREADY
	,output [  8:0] MBUS_MAIN_0__AXI4__CPU_RX__BID
	,output         MBUS_MAIN_0__AXI4__CPU_RX__BVALID
	,output [  1:0] MBUS_MAIN_0__AXI4__CPU_RX__BRESP
	,output         MBUS_MAIN_0__AXI4__CPU_RX__ARREADY
	,output [  8:0] MBUS_MAIN_0__AXI4__CPU_RX__RID
	,output [127:0] MBUS_MAIN_0__AXI4__CPU_RX__RDATA
	,output [  1:0] MBUS_MAIN_0__AXI4__CPU_RX__RRESP
	,output         MBUS_MAIN_0__AXI4__CPU_RX__RLAST
	,output         MBUS_MAIN_0__AXI4__CPU_RX__RVALID
	,output         MBUS_MAIN_0__AXI4__CAMERA_RX__AWREADY
	,output         MBUS_MAIN_0__AXI4__CAMERA_RX__WREADY
	,output [  4:0] MBUS_MAIN_0__AXI4__CAMERA_RX__BID
	,output         MBUS_MAIN_0__AXI4__CAMERA_RX__BVALID
	,output [  1:0] MBUS_MAIN_0__AXI4__CAMERA_RX__BRESP
	,output         MBUS_MAIN_0__AXI4__CAMERA_RX__ARREADY
	,output [  4:0] MBUS_MAIN_0__AXI4__CAMERA_RX__RID
	,output [127:0] MBUS_MAIN_0__AXI4__CAMERA_RX__RDATA
	,output [  1:0] MBUS_MAIN_0__AXI4__CAMERA_RX__RRESP
	,output         MBUS_MAIN_0__AXI4__CAMERA_RX__RLAST
	,output         MBUS_MAIN_0__AXI4__CAMERA_RX__RVALID
	,output         MBUS_MAIN_0__AXI4__VIDEO_RX__AWREADY
	,output         MBUS_MAIN_0__AXI4__VIDEO_RX__WREADY
	,output [  4:0] MBUS_MAIN_0__AXI4__VIDEO_RX__BID
	,output         MBUS_MAIN_0__AXI4__VIDEO_RX__BVALID
	,output [  1:0] MBUS_MAIN_0__AXI4__VIDEO_RX__BRESP
	,output         MBUS_MAIN_0__AXI4__VIDEO_RX__ARREADY
	,output [  4:0] MBUS_MAIN_0__AXI4__VIDEO_RX__RID
	,output [ 63:0] MBUS_MAIN_0__AXI4__VIDEO_RX__RDATA
	,output [  1:0] MBUS_MAIN_0__AXI4__VIDEO_RX__RRESP
	,output         MBUS_MAIN_0__AXI4__VIDEO_RX__RLAST
	,output         MBUS_MAIN_0__AXI4__VIDEO_RX__RVALID
	,output         MBUS_MAIN_0__AXI4__PERI_RX__AWREADY
	,output         MBUS_MAIN_0__AXI4__PERI_RX__WREADY
	,output [  5:0] MBUS_MAIN_0__AXI4__PERI_RX__BID
	,output         MBUS_MAIN_0__AXI4__PERI_RX__BVALID
	,output [  1:0] MBUS_MAIN_0__AXI4__PERI_RX__BRESP
	,output         MBUS_MAIN_0__AXI4__PERI_RX__ARREADY
	,output [  5:0] MBUS_MAIN_0__AXI4__PERI_RX__RID
	,output [ 63:0] MBUS_MAIN_0__AXI4__PERI_RX__RDATA
	,output [  1:0] MBUS_MAIN_0__AXI4__PERI_RX__RRESP
	,output         MBUS_MAIN_0__AXI4__PERI_RX__RLAST
	,output         MBUS_MAIN_0__AXI4__PERI_RX__RVALID
	,output         MBUS_MAIN_0__AXI4__USB3_RX__AWREADY
	,output         MBUS_MAIN_0__AXI4__USB3_RX__WREADY
	,output [  5:0] MBUS_MAIN_0__AXI4__USB3_RX__BID
	,output         MBUS_MAIN_0__AXI4__USB3_RX__BVALID
	,output [  1:0] MBUS_MAIN_0__AXI4__USB3_RX__BRESP
	,output         MBUS_MAIN_0__AXI4__USB3_RX__ARREADY
	,output [  5:0] MBUS_MAIN_0__AXI4__USB3_RX__RID
	,output [ 63:0] MBUS_MAIN_0__AXI4__USB3_RX__RDATA
	,output [  1:0] MBUS_MAIN_0__AXI4__USB3_RX__RRESP
	,output         MBUS_MAIN_0__AXI4__USB3_RX__RLAST
	,output         MBUS_MAIN_0__AXI4__USB3_RX__RVALID
	,output         MBUS_MAIN_0__AXI4__NEPES0_RX__AWREADY
	,output         MBUS_MAIN_0__AXI4__NEPES0_RX__WREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__NEPES0_RX__BID
	,output         MBUS_MAIN_0__AXI4__NEPES0_RX__BVALID
	,output [  1:0] MBUS_MAIN_0__AXI4__NEPES0_RX__BRESP
	,output         MBUS_MAIN_0__AXI4__NEPES0_RX__ARREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__NEPES0_RX__RID
	,output [255:0] MBUS_MAIN_0__AXI4__NEPES0_RX__RDATA
	,output [  1:0] MBUS_MAIN_0__AXI4__NEPES0_RX__RRESP
	,output         MBUS_MAIN_0__AXI4__NEPES0_RX__RLAST
	,output         MBUS_MAIN_0__AXI4__NEPES0_RX__RVALID
	,output         MBUS_MAIN_0__AXI4__NEPES1_RX__AWREADY
	,output         MBUS_MAIN_0__AXI4__NEPES1_RX__WREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__NEPES1_RX__BID
	,output         MBUS_MAIN_0__AXI4__NEPES1_RX__BVALID
	,output [  1:0] MBUS_MAIN_0__AXI4__NEPES1_RX__BRESP
	,output         MBUS_MAIN_0__AXI4__NEPES1_RX__ARREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__NEPES1_RX__RID
	,output [255:0] MBUS_MAIN_0__AXI4__NEPES1_RX__RDATA
	,output [  1:0] MBUS_MAIN_0__AXI4__NEPES1_RX__RRESP
	,output         MBUS_MAIN_0__AXI4__NEPES1_RX__RLAST
	,output         MBUS_MAIN_0__AXI4__NEPES1_RX__RVALID
	,output         MBUS_MAIN_0__AXI4__NEPES2_RX__AWREADY
	,output         MBUS_MAIN_0__AXI4__NEPES2_RX__WREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__NEPES2_RX__BID
	,output         MBUS_MAIN_0__AXI4__NEPES2_RX__BVALID
	,output [  1:0] MBUS_MAIN_0__AXI4__NEPES2_RX__BRESP
	,output         MBUS_MAIN_0__AXI4__NEPES2_RX__ARREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__NEPES2_RX__RID
	,output [255:0] MBUS_MAIN_0__AXI4__NEPES2_RX__RDATA
	,output [  1:0] MBUS_MAIN_0__AXI4__NEPES2_RX__RRESP
	,output         MBUS_MAIN_0__AXI4__NEPES2_RX__RLAST
	,output         MBUS_MAIN_0__AXI4__NEPES2_RX__RVALID
	,output         MBUS_MAIN_0__AXI4__EYL0_RX__AWREADY
	,output         MBUS_MAIN_0__AXI4__EYL0_RX__WREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__EYL0_RX__BID
	,output         MBUS_MAIN_0__AXI4__EYL0_RX__BVALID
	,output [  1:0] MBUS_MAIN_0__AXI4__EYL0_RX__BRESP
	,output         MBUS_MAIN_0__AXI4__EYL0_RX__ARREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__EYL0_RX__RID
	,output [255:0] MBUS_MAIN_0__AXI4__EYL0_RX__RDATA
	,output [  1:0] MBUS_MAIN_0__AXI4__EYL0_RX__RRESP
	,output         MBUS_MAIN_0__AXI4__EYL0_RX__RLAST
	,output         MBUS_MAIN_0__AXI4__EYL0_RX__RVALID
	,output         MBUS_MAIN_0__AXI4__EYL1_RX__AWREADY
	,output         MBUS_MAIN_0__AXI4__EYL1_RX__WREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__EYL1_RX__BID
	,output         MBUS_MAIN_0__AXI4__EYL1_RX__BVALID
	,output [  1:0] MBUS_MAIN_0__AXI4__EYL1_RX__BRESP
	,output         MBUS_MAIN_0__AXI4__EYL1_RX__ARREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__EYL1_RX__RID
	,output [255:0] MBUS_MAIN_0__AXI4__EYL1_RX__RDATA
	,output [  1:0] MBUS_MAIN_0__AXI4__EYL1_RX__RRESP
	,output         MBUS_MAIN_0__AXI4__EYL1_RX__RLAST
	,output         MBUS_MAIN_0__AXI4__EYL1_RX__RVALID
	,output         MBUS_MAIN_0__AXI4__EYL2_RX__AWREADY
	,output         MBUS_MAIN_0__AXI4__EYL2_RX__WREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__EYL2_RX__BID
	,output         MBUS_MAIN_0__AXI4__EYL2_RX__BVALID
	,output [  1:0] MBUS_MAIN_0__AXI4__EYL2_RX__BRESP
	,output         MBUS_MAIN_0__AXI4__EYL2_RX__ARREADY
	,output [  6:0] MBUS_MAIN_0__AXI4__EYL2_RX__RID
	,output [255:0] MBUS_MAIN_0__AXI4__EYL2_RX__RDATA
	,output [  1:0] MBUS_MAIN_0__AXI4__EYL2_RX__RRESP
	,output         MBUS_MAIN_0__AXI4__EYL2_RX__RLAST
	,output         MBUS_MAIN_0__AXI4__EYL2_RX__RVALID
	,output [ 35:0] MBUS_MAIN_0__AXI4__DDR0_TX__ARADDR
	,output [  1:0] MBUS_MAIN_0__AXI4__DDR0_TX__ARBURST
	,output [  3:0] MBUS_MAIN_0__AXI4__DDR0_TX__ARCACHE
	,output [ 11:0] MBUS_MAIN_0__AXI4__DDR0_TX__ARID
	,output [  7:0] MBUS_MAIN_0__AXI4__DDR0_TX__ARLEN
	,output         MBUS_MAIN_0__AXI4__DDR0_TX__ARLOCK
	,output [  2:0] MBUS_MAIN_0__AXI4__DDR0_TX__ARPROT
	,output [  9:0] MBUS_MAIN_0__AXI4__DDR0_TX__ARUSER
	,output [  2:0] MBUS_MAIN_0__AXI4__DDR0_TX__ARSIZE
	,output         MBUS_MAIN_0__AXI4__DDR0_TX__ARVALID
	,output [ 35:0] MBUS_MAIN_0__AXI4__DDR0_TX__AWADDR
	,output [  1:0] MBUS_MAIN_0__AXI4__DDR0_TX__AWBURST
	,output [  3:0] MBUS_MAIN_0__AXI4__DDR0_TX__AWCACHE
	,output [ 11:0] MBUS_MAIN_0__AXI4__DDR0_TX__AWID
	,output [  7:0] MBUS_MAIN_0__AXI4__DDR0_TX__AWLEN
	,output         MBUS_MAIN_0__AXI4__DDR0_TX__AWLOCK
	,output [  2:0] MBUS_MAIN_0__AXI4__DDR0_TX__AWPROT
	,output [  9:0] MBUS_MAIN_0__AXI4__DDR0_TX__AWUSER
	,output [  2:0] MBUS_MAIN_0__AXI4__DDR0_TX__AWSIZE
	,output         MBUS_MAIN_0__AXI4__DDR0_TX__AWVALID
	,output         MBUS_MAIN_0__AXI4__DDR0_TX__BREADY
	,output         MBUS_MAIN_0__AXI4__DDR0_TX__RREADY
	,output [255:0] MBUS_MAIN_0__AXI4__DDR0_TX__WDATA
	,output         MBUS_MAIN_0__AXI4__DDR0_TX__WLAST
	,output [ 31:0] MBUS_MAIN_0__AXI4__DDR0_TX__WSTRB
	,output         MBUS_MAIN_0__AXI4__DDR0_TX__WVALID
	,output [ 35:0] MBUS_MAIN_0__AXI4__DDR1_TX__ARADDR
	,output [  1:0] MBUS_MAIN_0__AXI4__DDR1_TX__ARBURST
	,output [  3:0] MBUS_MAIN_0__AXI4__DDR1_TX__ARCACHE
	,output [ 11:0] MBUS_MAIN_0__AXI4__DDR1_TX__ARID
	,output [  7:0] MBUS_MAIN_0__AXI4__DDR1_TX__ARLEN
	,output         MBUS_MAIN_0__AXI4__DDR1_TX__ARLOCK
	,output [  2:0] MBUS_MAIN_0__AXI4__DDR1_TX__ARPROT
	,output [  9:0] MBUS_MAIN_0__AXI4__DDR1_TX__ARUSER
	,output [  2:0] MBUS_MAIN_0__AXI4__DDR1_TX__ARSIZE
	,output         MBUS_MAIN_0__AXI4__DDR1_TX__ARVALID
	,output [ 35:0] MBUS_MAIN_0__AXI4__DDR1_TX__AWADDR
	,output [  1:0] MBUS_MAIN_0__AXI4__DDR1_TX__AWBURST
	,output [  3:0] MBUS_MAIN_0__AXI4__DDR1_TX__AWCACHE
	,output [ 11:0] MBUS_MAIN_0__AXI4__DDR1_TX__AWID
	,output [  7:0] MBUS_MAIN_0__AXI4__DDR1_TX__AWLEN
	,output         MBUS_MAIN_0__AXI4__DDR1_TX__AWLOCK
	,output [  2:0] MBUS_MAIN_0__AXI4__DDR1_TX__AWPROT
	,output [  9:0] MBUS_MAIN_0__AXI4__DDR1_TX__AWUSER
	,output [  2:0] MBUS_MAIN_0__AXI4__DDR1_TX__AWSIZE
	,output         MBUS_MAIN_0__AXI4__DDR1_TX__AWVALID
	,output         MBUS_MAIN_0__AXI4__DDR1_TX__BREADY
	,output         MBUS_MAIN_0__AXI4__DDR1_TX__RREADY
	,output [255:0] MBUS_MAIN_0__AXI4__DDR1_TX__WDATA
	,output         MBUS_MAIN_0__AXI4__DDR1_TX__WLAST
	,output [ 31:0] MBUS_MAIN_0__AXI4__DDR1_TX__WSTRB
	,output         MBUS_MAIN_0__AXI4__DDR1_TX__WVALID
	,output [ 31:0] MBUS_MAIN_0__AXI4__CPU_TX__ARADDR
	,output [  1:0] MBUS_MAIN_0__AXI4__CPU_TX__ARBURST
	,output [  3:0] MBUS_MAIN_0__AXI4__CPU_TX__ARCACHE
	,output [  9:0] MBUS_MAIN_0__AXI4__CPU_TX__ARID
	,output [  7:0] MBUS_MAIN_0__AXI4__CPU_TX__ARLEN
	,output         MBUS_MAIN_0__AXI4__CPU_TX__ARLOCK
	,output [  2:0] MBUS_MAIN_0__AXI4__CPU_TX__ARPROT
	,output [  2:0] MBUS_MAIN_0__AXI4__CPU_TX__ARSIZE
	,output         MBUS_MAIN_0__AXI4__CPU_TX__ARVALID
	,output [ 31:0] MBUS_MAIN_0__AXI4__CPU_TX__AWADDR
	,output [  1:0] MBUS_MAIN_0__AXI4__CPU_TX__AWBURST
	,output [  3:0] MBUS_MAIN_0__AXI4__CPU_TX__AWCACHE
	,output [  9:0] MBUS_MAIN_0__AXI4__CPU_TX__AWID
	,output [  7:0] MBUS_MAIN_0__AXI4__CPU_TX__AWLEN
	,output         MBUS_MAIN_0__AXI4__CPU_TX__AWLOCK
	,output [  2:0] MBUS_MAIN_0__AXI4__CPU_TX__AWPROT
	,output [  2:0] MBUS_MAIN_0__AXI4__CPU_TX__AWSIZE
	,output         MBUS_MAIN_0__AXI4__CPU_TX__AWVALID
	,output         MBUS_MAIN_0__AXI4__CPU_TX__BREADY
	,output         MBUS_MAIN_0__AXI4__CPU_TX__RREADY
	,output [ 63:0] MBUS_MAIN_0__AXI4__CPU_TX__WDATA
	,output         MBUS_MAIN_0__AXI4__CPU_TX__WLAST
	,output [  7:0] MBUS_MAIN_0__AXI4__CPU_TX__WSTRB
	,output         MBUS_MAIN_0__AXI4__CPU_TX__WVALID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__CPU_RX__AWADDR
	,input  [  8:0] MBUS_MAIN_0__AXI4__CPU_RX__AWID
	,input  [  7:0] MBUS_MAIN_0__AXI4__CPU_RX__AWLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__CPU_RX__AWSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__CPU_RX__AWBURST
	,input          MBUS_MAIN_0__AXI4__CPU_RX__AWLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__CPU_RX__AWCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__CPU_RX__AWPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__CPU_RX__AWUSER
	,input          MBUS_MAIN_0__AXI4__CPU_RX__AWVALID
	,input  [127:0] MBUS_MAIN_0__AXI4__CPU_RX__WDATA
	,input  [ 15:0] MBUS_MAIN_0__AXI4__CPU_RX__WSTRB
	,input          MBUS_MAIN_0__AXI4__CPU_RX__WVALID
	,input          MBUS_MAIN_0__AXI4__CPU_RX__WLAST
	,input          MBUS_MAIN_0__AXI4__CPU_RX__BREADY
	,input  [  8:0] MBUS_MAIN_0__AXI4__CPU_RX__ARID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__CPU_RX__ARADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__CPU_RX__ARLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__CPU_RX__ARSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__CPU_RX__ARBURST
	,input          MBUS_MAIN_0__AXI4__CPU_RX__ARLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__CPU_RX__ARCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__CPU_RX__ARPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__CPU_RX__ARUSER
	,input          MBUS_MAIN_0__AXI4__CPU_RX__ARVALID
	,input          MBUS_MAIN_0__AXI4__CPU_RX__RREADY
	,input  [ 35:0] MBUS_MAIN_0__AXI4__CAMERA_RX__AWADDR
	,input  [  4:0] MBUS_MAIN_0__AXI4__CAMERA_RX__AWID
	,input  [  7:0] MBUS_MAIN_0__AXI4__CAMERA_RX__AWLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__CAMERA_RX__AWSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__CAMERA_RX__AWBURST
	,input          MBUS_MAIN_0__AXI4__CAMERA_RX__AWLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__CAMERA_RX__AWCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__CAMERA_RX__AWPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__CAMERA_RX__AWUSER
	,input          MBUS_MAIN_0__AXI4__CAMERA_RX__AWVALID
	,input  [127:0] MBUS_MAIN_0__AXI4__CAMERA_RX__WDATA
	,input  [ 15:0] MBUS_MAIN_0__AXI4__CAMERA_RX__WSTRB
	,input          MBUS_MAIN_0__AXI4__CAMERA_RX__WVALID
	,input          MBUS_MAIN_0__AXI4__CAMERA_RX__WLAST
	,input          MBUS_MAIN_0__AXI4__CAMERA_RX__BREADY
	,input  [  4:0] MBUS_MAIN_0__AXI4__CAMERA_RX__ARID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__CAMERA_RX__ARADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__CAMERA_RX__ARLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__CAMERA_RX__ARSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__CAMERA_RX__ARBURST
	,input          MBUS_MAIN_0__AXI4__CAMERA_RX__ARLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__CAMERA_RX__ARCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__CAMERA_RX__ARPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__CAMERA_RX__ARUSER
	,input          MBUS_MAIN_0__AXI4__CAMERA_RX__ARVALID
	,input          MBUS_MAIN_0__AXI4__CAMERA_RX__RREADY
	,input  [ 35:0] MBUS_MAIN_0__AXI4__VIDEO_RX__AWADDR
	,input  [  4:0] MBUS_MAIN_0__AXI4__VIDEO_RX__AWID
	,input  [  7:0] MBUS_MAIN_0__AXI4__VIDEO_RX__AWLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__VIDEO_RX__AWSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__VIDEO_RX__AWBURST
	,input          MBUS_MAIN_0__AXI4__VIDEO_RX__AWLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__VIDEO_RX__AWCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__VIDEO_RX__AWPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__VIDEO_RX__AWUSER
	,input          MBUS_MAIN_0__AXI4__VIDEO_RX__AWVALID
	,input  [ 63:0] MBUS_MAIN_0__AXI4__VIDEO_RX__WDATA
	,input  [  7:0] MBUS_MAIN_0__AXI4__VIDEO_RX__WSTRB
	,input          MBUS_MAIN_0__AXI4__VIDEO_RX__WVALID
	,input          MBUS_MAIN_0__AXI4__VIDEO_RX__WLAST
	,input          MBUS_MAIN_0__AXI4__VIDEO_RX__BREADY
	,input  [  4:0] MBUS_MAIN_0__AXI4__VIDEO_RX__ARID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__VIDEO_RX__ARADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__VIDEO_RX__ARLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__VIDEO_RX__ARSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__VIDEO_RX__ARBURST
	,input          MBUS_MAIN_0__AXI4__VIDEO_RX__ARLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__VIDEO_RX__ARCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__VIDEO_RX__ARPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__VIDEO_RX__ARUSER
	,input          MBUS_MAIN_0__AXI4__VIDEO_RX__ARVALID
	,input          MBUS_MAIN_0__AXI4__VIDEO_RX__RREADY
	,input  [ 35:0] MBUS_MAIN_0__AXI4__PERI_RX__AWADDR
	,input  [  5:0] MBUS_MAIN_0__AXI4__PERI_RX__AWID
	,input  [  7:0] MBUS_MAIN_0__AXI4__PERI_RX__AWLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__PERI_RX__AWSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__PERI_RX__AWBURST
	,input          MBUS_MAIN_0__AXI4__PERI_RX__AWLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__PERI_RX__AWCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__PERI_RX__AWPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__PERI_RX__AWUSER
	,input          MBUS_MAIN_0__AXI4__PERI_RX__AWVALID
	,input  [ 63:0] MBUS_MAIN_0__AXI4__PERI_RX__WDATA
	,input  [  7:0] MBUS_MAIN_0__AXI4__PERI_RX__WSTRB
	,input          MBUS_MAIN_0__AXI4__PERI_RX__WVALID
	,input          MBUS_MAIN_0__AXI4__PERI_RX__WLAST
	,input          MBUS_MAIN_0__AXI4__PERI_RX__BREADY
	,input  [  5:0] MBUS_MAIN_0__AXI4__PERI_RX__ARID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__PERI_RX__ARADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__PERI_RX__ARLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__PERI_RX__ARSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__PERI_RX__ARBURST
	,input          MBUS_MAIN_0__AXI4__PERI_RX__ARLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__PERI_RX__ARCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__PERI_RX__ARPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__PERI_RX__ARUSER
	,input          MBUS_MAIN_0__AXI4__PERI_RX__ARVALID
	,input          MBUS_MAIN_0__AXI4__PERI_RX__RREADY
	,input  [ 35:0] MBUS_MAIN_0__AXI4__USB3_RX__AWADDR
	,input  [  5:0] MBUS_MAIN_0__AXI4__USB3_RX__AWID
	,input  [  7:0] MBUS_MAIN_0__AXI4__USB3_RX__AWLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__USB3_RX__AWSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__USB3_RX__AWBURST
	,input          MBUS_MAIN_0__AXI4__USB3_RX__AWLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__USB3_RX__AWCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__USB3_RX__AWPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__USB3_RX__AWUSER
	,input          MBUS_MAIN_0__AXI4__USB3_RX__AWVALID
	,input  [ 63:0] MBUS_MAIN_0__AXI4__USB3_RX__WDATA
	,input  [  7:0] MBUS_MAIN_0__AXI4__USB3_RX__WSTRB
	,input          MBUS_MAIN_0__AXI4__USB3_RX__WVALID
	,input          MBUS_MAIN_0__AXI4__USB3_RX__WLAST
	,input          MBUS_MAIN_0__AXI4__USB3_RX__BREADY
	,input  [  5:0] MBUS_MAIN_0__AXI4__USB3_RX__ARID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__USB3_RX__ARADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__USB3_RX__ARLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__USB3_RX__ARSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__USB3_RX__ARBURST
	,input          MBUS_MAIN_0__AXI4__USB3_RX__ARLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__USB3_RX__ARCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__USB3_RX__ARPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__USB3_RX__ARUSER
	,input          MBUS_MAIN_0__AXI4__USB3_RX__ARVALID
	,input          MBUS_MAIN_0__AXI4__USB3_RX__RREADY
	,input  [ 35:0] MBUS_MAIN_0__AXI4__NEPES0_RX__AWADDR
	,input  [  6:0] MBUS_MAIN_0__AXI4__NEPES0_RX__AWID
	,input  [  7:0] MBUS_MAIN_0__AXI4__NEPES0_RX__AWLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__NEPES0_RX__AWSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__NEPES0_RX__AWBURST
	,input          MBUS_MAIN_0__AXI4__NEPES0_RX__AWLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__NEPES0_RX__AWCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__NEPES0_RX__AWPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__NEPES0_RX__AWUSER
	,input          MBUS_MAIN_0__AXI4__NEPES0_RX__AWVALID
	,input  [255:0] MBUS_MAIN_0__AXI4__NEPES0_RX__WDATA
	,input  [ 31:0] MBUS_MAIN_0__AXI4__NEPES0_RX__WSTRB
	,input          MBUS_MAIN_0__AXI4__NEPES0_RX__WVALID
	,input          MBUS_MAIN_0__AXI4__NEPES0_RX__WLAST
	,input          MBUS_MAIN_0__AXI4__NEPES0_RX__BREADY
	,input  [  6:0] MBUS_MAIN_0__AXI4__NEPES0_RX__ARID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__NEPES0_RX__ARADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__NEPES0_RX__ARLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__NEPES0_RX__ARSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__NEPES0_RX__ARBURST
	,input          MBUS_MAIN_0__AXI4__NEPES0_RX__ARLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__NEPES0_RX__ARCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__NEPES0_RX__ARPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__NEPES0_RX__ARUSER
	,input          MBUS_MAIN_0__AXI4__NEPES0_RX__ARVALID
	,input          MBUS_MAIN_0__AXI4__NEPES0_RX__RREADY
	,input  [ 35:0] MBUS_MAIN_0__AXI4__NEPES1_RX__AWADDR
	,input  [  6:0] MBUS_MAIN_0__AXI4__NEPES1_RX__AWID
	,input  [  7:0] MBUS_MAIN_0__AXI4__NEPES1_RX__AWLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__NEPES1_RX__AWSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__NEPES1_RX__AWBURST
	,input          MBUS_MAIN_0__AXI4__NEPES1_RX__AWLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__NEPES1_RX__AWCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__NEPES1_RX__AWPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__NEPES1_RX__AWUSER
	,input          MBUS_MAIN_0__AXI4__NEPES1_RX__AWVALID
	,input  [255:0] MBUS_MAIN_0__AXI4__NEPES1_RX__WDATA
	,input  [ 31:0] MBUS_MAIN_0__AXI4__NEPES1_RX__WSTRB
	,input          MBUS_MAIN_0__AXI4__NEPES1_RX__WVALID
	,input          MBUS_MAIN_0__AXI4__NEPES1_RX__WLAST
	,input          MBUS_MAIN_0__AXI4__NEPES1_RX__BREADY
	,input  [  6:0] MBUS_MAIN_0__AXI4__NEPES1_RX__ARID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__NEPES1_RX__ARADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__NEPES1_RX__ARLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__NEPES1_RX__ARSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__NEPES1_RX__ARBURST
	,input          MBUS_MAIN_0__AXI4__NEPES1_RX__ARLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__NEPES1_RX__ARCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__NEPES1_RX__ARPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__NEPES1_RX__ARUSER
	,input          MBUS_MAIN_0__AXI4__NEPES1_RX__ARVALID
	,input          MBUS_MAIN_0__AXI4__NEPES1_RX__RREADY
	,input  [ 35:0] MBUS_MAIN_0__AXI4__NEPES2_RX__AWADDR
	,input  [  6:0] MBUS_MAIN_0__AXI4__NEPES2_RX__AWID
	,input  [  7:0] MBUS_MAIN_0__AXI4__NEPES2_RX__AWLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__NEPES2_RX__AWSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__NEPES2_RX__AWBURST
	,input          MBUS_MAIN_0__AXI4__NEPES2_RX__AWLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__NEPES2_RX__AWCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__NEPES2_RX__AWPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__NEPES2_RX__AWUSER
	,input          MBUS_MAIN_0__AXI4__NEPES2_RX__AWVALID
	,input  [255:0] MBUS_MAIN_0__AXI4__NEPES2_RX__WDATA
	,input  [ 31:0] MBUS_MAIN_0__AXI4__NEPES2_RX__WSTRB
	,input          MBUS_MAIN_0__AXI4__NEPES2_RX__WVALID
	,input          MBUS_MAIN_0__AXI4__NEPES2_RX__WLAST
	,input          MBUS_MAIN_0__AXI4__NEPES2_RX__BREADY
	,input  [  6:0] MBUS_MAIN_0__AXI4__NEPES2_RX__ARID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__NEPES2_RX__ARADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__NEPES2_RX__ARLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__NEPES2_RX__ARSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__NEPES2_RX__ARBURST
	,input          MBUS_MAIN_0__AXI4__NEPES2_RX__ARLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__NEPES2_RX__ARCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__NEPES2_RX__ARPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__NEPES2_RX__ARUSER
	,input          MBUS_MAIN_0__AXI4__NEPES2_RX__ARVALID
	,input          MBUS_MAIN_0__AXI4__NEPES2_RX__RREADY
	,input  [ 35:0] MBUS_MAIN_0__AXI4__EYL0_RX__AWADDR
	,input  [  6:0] MBUS_MAIN_0__AXI4__EYL0_RX__AWID
	,input  [  7:0] MBUS_MAIN_0__AXI4__EYL0_RX__AWLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__EYL0_RX__AWSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__EYL0_RX__AWBURST
	,input          MBUS_MAIN_0__AXI4__EYL0_RX__AWLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__EYL0_RX__AWCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__EYL0_RX__AWPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__EYL0_RX__AWUSER
	,input          MBUS_MAIN_0__AXI4__EYL0_RX__AWVALID
	,input  [255:0] MBUS_MAIN_0__AXI4__EYL0_RX__WDATA
	,input  [ 31:0] MBUS_MAIN_0__AXI4__EYL0_RX__WSTRB
	,input          MBUS_MAIN_0__AXI4__EYL0_RX__WVALID
	,input          MBUS_MAIN_0__AXI4__EYL0_RX__WLAST
	,input          MBUS_MAIN_0__AXI4__EYL0_RX__BREADY
	,input  [  6:0] MBUS_MAIN_0__AXI4__EYL0_RX__ARID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__EYL0_RX__ARADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__EYL0_RX__ARLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__EYL0_RX__ARSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__EYL0_RX__ARBURST
	,input          MBUS_MAIN_0__AXI4__EYL0_RX__ARLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__EYL0_RX__ARCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__EYL0_RX__ARPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__EYL0_RX__ARUSER
	,input          MBUS_MAIN_0__AXI4__EYL0_RX__ARVALID
	,input          MBUS_MAIN_0__AXI4__EYL0_RX__RREADY
	,input  [ 35:0] MBUS_MAIN_0__AXI4__EYL1_RX__AWADDR
	,input  [  6:0] MBUS_MAIN_0__AXI4__EYL1_RX__AWID
	,input  [  7:0] MBUS_MAIN_0__AXI4__EYL1_RX__AWLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__EYL1_RX__AWSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__EYL1_RX__AWBURST
	,input          MBUS_MAIN_0__AXI4__EYL1_RX__AWLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__EYL1_RX__AWCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__EYL1_RX__AWPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__EYL1_RX__AWUSER
	,input          MBUS_MAIN_0__AXI4__EYL1_RX__AWVALID
	,input  [255:0] MBUS_MAIN_0__AXI4__EYL1_RX__WDATA
	,input  [ 31:0] MBUS_MAIN_0__AXI4__EYL1_RX__WSTRB
	,input          MBUS_MAIN_0__AXI4__EYL1_RX__WVALID
	,input          MBUS_MAIN_0__AXI4__EYL1_RX__WLAST
	,input          MBUS_MAIN_0__AXI4__EYL1_RX__BREADY
	,input  [  6:0] MBUS_MAIN_0__AXI4__EYL1_RX__ARID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__EYL1_RX__ARADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__EYL1_RX__ARLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__EYL1_RX__ARSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__EYL1_RX__ARBURST
	,input          MBUS_MAIN_0__AXI4__EYL1_RX__ARLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__EYL1_RX__ARCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__EYL1_RX__ARPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__EYL1_RX__ARUSER
	,input          MBUS_MAIN_0__AXI4__EYL1_RX__ARVALID
	,input          MBUS_MAIN_0__AXI4__EYL1_RX__RREADY
	,input  [ 35:0] MBUS_MAIN_0__AXI4__EYL2_RX__AWADDR
	,input  [  6:0] MBUS_MAIN_0__AXI4__EYL2_RX__AWID
	,input  [  7:0] MBUS_MAIN_0__AXI4__EYL2_RX__AWLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__EYL2_RX__AWSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__EYL2_RX__AWBURST
	,input          MBUS_MAIN_0__AXI4__EYL2_RX__AWLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__EYL2_RX__AWCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__EYL2_RX__AWPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__EYL2_RX__AWUSER
	,input          MBUS_MAIN_0__AXI4__EYL2_RX__AWVALID
	,input  [255:0] MBUS_MAIN_0__AXI4__EYL2_RX__WDATA
	,input  [ 31:0] MBUS_MAIN_0__AXI4__EYL2_RX__WSTRB
	,input          MBUS_MAIN_0__AXI4__EYL2_RX__WVALID
	,input          MBUS_MAIN_0__AXI4__EYL2_RX__WLAST
	,input          MBUS_MAIN_0__AXI4__EYL2_RX__BREADY
	,input  [  6:0] MBUS_MAIN_0__AXI4__EYL2_RX__ARID
	,input  [ 35:0] MBUS_MAIN_0__AXI4__EYL2_RX__ARADDR
	,input  [  7:0] MBUS_MAIN_0__AXI4__EYL2_RX__ARLEN
	,input  [  2:0] MBUS_MAIN_0__AXI4__EYL2_RX__ARSIZE
	,input  [  1:0] MBUS_MAIN_0__AXI4__EYL2_RX__ARBURST
	,input          MBUS_MAIN_0__AXI4__EYL2_RX__ARLOCK
	,input  [  3:0] MBUS_MAIN_0__AXI4__EYL2_RX__ARCACHE
	,input  [  2:0] MBUS_MAIN_0__AXI4__EYL2_RX__ARPROT
	,input  [  9:0] MBUS_MAIN_0__AXI4__EYL2_RX__ARUSER
	,input          MBUS_MAIN_0__AXI4__EYL2_RX__ARVALID
	,input          MBUS_MAIN_0__AXI4__EYL2_RX__RREADY
	,input          MBUS_MAIN_0__AXI4__DDR0_TX__ARREADY
	,input          MBUS_MAIN_0__AXI4__DDR0_TX__AWREADY
	,input  [ 11:0] MBUS_MAIN_0__AXI4__DDR0_TX__BID
	,input  [  1:0] MBUS_MAIN_0__AXI4__DDR0_TX__BRESP
	,input          MBUS_MAIN_0__AXI4__DDR0_TX__BVALID
	,input  [255:0] MBUS_MAIN_0__AXI4__DDR0_TX__RDATA
	,input  [ 11:0] MBUS_MAIN_0__AXI4__DDR0_TX__RID
	,input          MBUS_MAIN_0__AXI4__DDR0_TX__RLAST
	,input  [  1:0] MBUS_MAIN_0__AXI4__DDR0_TX__RRESP
	,input          MBUS_MAIN_0__AXI4__DDR0_TX__RVALID
	,input          MBUS_MAIN_0__AXI4__DDR0_TX__WREADY
	,input          MBUS_MAIN_0__AXI4__DDR1_TX__ARREADY
	,input          MBUS_MAIN_0__AXI4__DDR1_TX__AWREADY
	,input  [ 11:0] MBUS_MAIN_0__AXI4__DDR1_TX__BID
	,input  [  1:0] MBUS_MAIN_0__AXI4__DDR1_TX__BRESP
	,input          MBUS_MAIN_0__AXI4__DDR1_TX__BVALID
	,input  [255:0] MBUS_MAIN_0__AXI4__DDR1_TX__RDATA
	,input  [ 11:0] MBUS_MAIN_0__AXI4__DDR1_TX__RID
	,input          MBUS_MAIN_0__AXI4__DDR1_TX__RLAST
	,input  [  1:0] MBUS_MAIN_0__AXI4__DDR1_TX__RRESP
	,input          MBUS_MAIN_0__AXI4__DDR1_TX__RVALID
	,input          MBUS_MAIN_0__AXI4__DDR1_TX__WREADY
	,input          MBUS_MAIN_0__AXI4__CPU_TX__ARREADY
	,input          MBUS_MAIN_0__AXI4__CPU_TX__AWREADY
	,input  [  9:0] MBUS_MAIN_0__AXI4__CPU_TX__BID
	,input  [  1:0] MBUS_MAIN_0__AXI4__CPU_TX__BRESP
	,input          MBUS_MAIN_0__AXI4__CPU_TX__BVALID
	,input  [ 63:0] MBUS_MAIN_0__AXI4__CPU_TX__RDATA
	,input  [  9:0] MBUS_MAIN_0__AXI4__CPU_TX__RID
	,input          MBUS_MAIN_0__AXI4__CPU_TX__RLAST
	,input  [  1:0] MBUS_MAIN_0__AXI4__CPU_TX__RRESP
	,input          MBUS_MAIN_0__AXI4__CPU_TX__RVALID
	,input          MBUS_MAIN_0__AXI4__CPU_TX__WREADY
	,input          PKG__NPU0_DISABLE
	,input          PKG__NPU1_DISABLE
);
	assign SCU_MAIN_0__CLK__UART = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__SPI = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__QSPI = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__RTC = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__I2C = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__I2S = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__OTP = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__OSC_FREE = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__SUBSYSTEM_CPU = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__SUBSYSTEM_DDR0 = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__SUBSYSTEM_DDR1 = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__SUBSYSTEM_PERI = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__SUBSYSTEM_NEPES = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__SUBSYSTEM_EYL = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__SUBSYSTEM_AIM = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__SUBSYSTEM_USB3 = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__SUBSYSTEM_VIDEO = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__CLK__SUBSYSTEM_CAMERA = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__CPU_TX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__DDR0_TX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__DDR1_TX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__CPU_RX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__PERI_RX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__USB3_RX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__CAMERA_RX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__VIDEO_RX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__NEPES0_RX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__NEPES1_RX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__NEPES2_RX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__EYL0_RX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__EYL1_RX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__EYL2_RX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__AIM0_RX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__AIM1_RX = SCU_MAIN_0__IO__CLK__OSC;
	assign MBUS_MAIN_0__CLK__AIM2_RX = SCU_MAIN_0__IO__CLK__OSC;
	assign PBUS_MAIN_0__CLK__PERI_TX = SCU_MAIN_0__IO__CLK__OSC;
	assign PBUS_MAIN_0__CLK__DDR0_TX = SCU_MAIN_0__IO__CLK__OSC;
	assign PBUS_MAIN_0__CLK__DDR1_TX = SCU_MAIN_0__IO__CLK__OSC;
	assign PBUS_MAIN_0__CLK__USB3_TX = SCU_MAIN_0__IO__CLK__OSC;
	assign PBUS_MAIN_0__CLK__CAMERA_TX = SCU_MAIN_0__IO__CLK__OSC;
	assign PBUS_MAIN_0__CLK__VIDEO_TX = SCU_MAIN_0__IO__CLK__OSC;
	assign PBUS_MAIN_0__CLK__NEPES_TX = SCU_MAIN_0__IO__CLK__OSC;
	assign PBUS_MAIN_0__CLK__EYL_TX = SCU_MAIN_0__IO__CLK__OSC;
	assign PBUS_MAIN_0__CLK__AIM_TX = SCU_MAIN_0__IO__CLK__OSC;
	assign PBUS_MAIN_0__CLK__CPU_RX = SCU_MAIN_0__IO__CLK__OSC;
	assign SCU_MAIN_0__RSTN__SUBSYSTEM_CPU = SCU_MAIN_0__IO__RSTN;
	assign SCU_MAIN_0__RSTN__SUBSYSTEM_DDR0 = SCU_MAIN_0__IO__RSTN;
	assign SCU_MAIN_0__RSTN__SUBSYSTEM_DDR1 = SCU_MAIN_0__IO__RSTN;
	assign SCU_MAIN_0__RSTN__SUBSYSTEM_PERI = SCU_MAIN_0__IO__RSTN;
	assign SCU_MAIN_0__RSTN__SUBSYSTEM_NEPES = SCU_MAIN_0__IO__RSTN;
	assign SCU_MAIN_0__RSTN__SUBSYSTEM_EYL = SCU_MAIN_0__IO__RSTN;
	assign SCU_MAIN_0__RSTN__SUBSYSTEM_AIM = SCU_MAIN_0__IO__RSTN;
	assign SCU_MAIN_0__RSTN__SUBSYSTEM_USB3 = SCU_MAIN_0__IO__RSTN;
	assign SCU_MAIN_0__RSTN__SUBSYSTEM_VIDEO = SCU_MAIN_0__IO__RSTN;
	assign SCU_MAIN_0__RSTN__SUBSYSTEM_CAMERA = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__CPU_TX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__DDR0_TX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__DDR1_TX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__CPU_RX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__PERI_RX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__USB3_RX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__CAMERA_RX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__VIDEO_RX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__NEPES0_RX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__NEPES1_RX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__NEPES2_RX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__EYL0_RX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__EYL1_RX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__EYL2_RX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__AIM0_RX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__AIM1_RX = SCU_MAIN_0__IO__RSTN;
	assign MBUS_MAIN_0__RSTN__AIM2_RX = SCU_MAIN_0__IO__RSTN;
	assign PBUS_MAIN_0__RSTN__PERI_TX = SCU_MAIN_0__IO__RSTN;
	assign PBUS_MAIN_0__RSTN__DDR0_TX = SCU_MAIN_0__IO__RSTN;
	assign PBUS_MAIN_0__RSTN__DDR1_TX = SCU_MAIN_0__IO__RSTN;
	assign PBUS_MAIN_0__RSTN__USB3_TX = SCU_MAIN_0__IO__RSTN;
	assign PBUS_MAIN_0__RSTN__CAMERA_TX = SCU_MAIN_0__IO__RSTN;
	assign PBUS_MAIN_0__RSTN__VIDEO_TX = SCU_MAIN_0__IO__RSTN;
	assign PBUS_MAIN_0__RSTN__NEPES_TX = SCU_MAIN_0__IO__RSTN;
	assign PBUS_MAIN_0__RSTN__EYL_TX = SCU_MAIN_0__IO__RSTN;
	assign PBUS_MAIN_0__RSTN__AIM_TX = SCU_MAIN_0__IO__RSTN;
	assign PBUS_MAIN_0__RSTN__CPU_RX = SCU_MAIN_0__IO__RSTN;
	assign SCU_MAIN_0__IO__RSTNOUT = SCU_MAIN_0__IO__RSTN;
	assign SCU_MAIN_0__RESET_VECTOR_0 = 37'h80000000;
	assign SCU_MAIN_0__RESET_VECTOR_1 = 37'h80000000;
	assign MBUS_MAIN_0__AXI4__AIM0_RX__AWREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__AIM0_RX__WREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__AIM0_RX__ARREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__AIM1_RX__AWREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__AIM1_RX__WREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__AIM1_RX__ARREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__AIM2_RX__AWREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__AIM2_RX__WREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__AIM2_RX__ARREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__CPU_RX__AWREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__CPU_RX__WREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__CPU_RX__ARREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__PERI_TX__BREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__PERI_TX__RREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__BREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__RREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__BREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__RREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__USB3_TX__BREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__USB3_TX__RREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__BREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__RREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__EYL_TX__BREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__EYL_TX__RREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__AIM_TX__BREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__AIM_TX__RREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__BREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__RREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__BREADY = 1'h1;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__RREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__CPU_RX__AWREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__CPU_RX__WREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__CPU_RX__ARREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__CAMERA_RX__AWREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__CAMERA_RX__WREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__CAMERA_RX__ARREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__VIDEO_RX__AWREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__VIDEO_RX__WREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__VIDEO_RX__ARREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__PERI_RX__AWREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__PERI_RX__WREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__PERI_RX__ARREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__USB3_RX__AWREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__USB3_RX__WREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__USB3_RX__ARREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__NEPES0_RX__AWREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__NEPES0_RX__WREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__NEPES0_RX__ARREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__NEPES1_RX__AWREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__NEPES1_RX__WREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__NEPES1_RX__ARREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__NEPES2_RX__AWREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__NEPES2_RX__WREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__NEPES2_RX__ARREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__EYL0_RX__AWREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__EYL0_RX__WREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__EYL0_RX__ARREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__EYL1_RX__AWREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__EYL1_RX__WREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__EYL1_RX__ARREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__EYL2_RX__AWREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__EYL2_RX__WREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__EYL2_RX__ARREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__BREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__RREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__BREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__RREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__CPU_TX__BREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__CPU_TX__RREADY = 1'h1;
	assign MBUS_MAIN_0__AXI4__AIM0_RX__BID = 7'h0;
	assign MBUS_MAIN_0__AXI4__AIM0_RX__BVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__AIM0_RX__BRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__AIM0_RX__RID = 7'h0;
	assign MBUS_MAIN_0__AXI4__AIM0_RX__RDATA = 256'h0;
	assign MBUS_MAIN_0__AXI4__AIM0_RX__RRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__AIM0_RX__RLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__AIM0_RX__RVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__AIM1_RX__BID = 7'h0;
	assign MBUS_MAIN_0__AXI4__AIM1_RX__BVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__AIM1_RX__BRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__AIM1_RX__RID = 7'h0;
	assign MBUS_MAIN_0__AXI4__AIM1_RX__RDATA = 256'h0;
	assign MBUS_MAIN_0__AXI4__AIM1_RX__RRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__AIM1_RX__RLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__AIM1_RX__RVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__AIM2_RX__BID = 7'h0;
	assign MBUS_MAIN_0__AXI4__AIM2_RX__BVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__AIM2_RX__BRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__AIM2_RX__RID = 7'h0;
	assign MBUS_MAIN_0__AXI4__AIM2_RX__RDATA = 256'h0;
	assign MBUS_MAIN_0__AXI4__AIM2_RX__RRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__AIM2_RX__RLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__AIM2_RX__RVALID = 1'h0;
	assign SCU_MAIN_0__DFT__SCAN_MODE = 1'h0;
	assign SCU_MAIN_0__NMI_INTERRUPT_VECTOR_0 = 37'h0;
	assign SCU_MAIN_0__NMI_INTERRUPT_VECTOR_1 = 37'h0;
	assign SCU_MAIN_0__NMI_EXCEPTION_VECTOR_0 = 37'h0;
	assign SCU_MAIN_0__NMI_EXCEPTION_VECTOR_1 = 37'h0;
	assign SCU_MAIN_0__LPDDR4_1__RET_OFF = 1'h0;
	assign SCU_MAIN_0__LPDDR4_0__RET_OFF = 1'h0;
	assign PBUS_MAIN_0__AXI4__CPU_RX__BID = 1'h0;
	assign PBUS_MAIN_0__AXI4__CPU_RX__BVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__CPU_RX__BRESP = 2'h0;
	assign PBUS_MAIN_0__AXI4__CPU_RX__RID = 1'h0;
	assign PBUS_MAIN_0__AXI4__CPU_RX__RDATA = 32'h0;
	assign PBUS_MAIN_0__AXI4__CPU_RX__RRESP = 2'h0;
	assign PBUS_MAIN_0__AXI4__CPU_RX__RLAST = 1'h0;
	assign PBUS_MAIN_0__AXI4__CPU_RX__RVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__AWADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__AWID = 1'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__AWLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__AWSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__AWBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__AWLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__AWCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__AWPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__WDATA = 32'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__WSTRB = 4'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__ARID = 1'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__ARADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__ARLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__ARSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__ARBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__ARLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__ARCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__ARPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__AWVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__WVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__WLAST = 1'h0;
	assign PBUS_MAIN_0__AXI4__PERI_TX__ARVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__AWADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__AWID = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__AWLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__AWSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__AWBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__AWLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__AWCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__AWPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__WDATA = 32'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__WSTRB = 4'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__ARID = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__ARADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__ARLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__ARSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__ARBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__ARLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__ARCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__ARPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__AWVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__WVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__WLAST = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR0_TX__ARVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__AWADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__AWID = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__AWLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__AWSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__AWBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__AWLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__AWCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__AWPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__WDATA = 32'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__WSTRB = 4'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__ARID = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__ARADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__ARLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__ARSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__ARBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__ARLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__ARCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__ARPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__AWVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__WVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__WLAST = 1'h0;
	assign PBUS_MAIN_0__AXI4__DDR1_TX__ARVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__AWADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__AWID = 1'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__AWLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__AWSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__AWBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__AWLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__AWCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__AWPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__WDATA = 32'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__WSTRB = 4'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__ARID = 1'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__ARADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__ARLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__ARSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__ARBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__ARLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__ARCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__ARPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__AWVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__WVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__WLAST = 1'h0;
	assign PBUS_MAIN_0__AXI4__USB3_TX__ARVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__AWADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__AWID = 1'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__AWLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__AWSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__AWBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__AWLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__AWCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__AWPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__WDATA = 32'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__WSTRB = 4'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__ARID = 1'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__ARADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__ARLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__ARSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__ARBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__ARLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__ARCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__ARPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__AWVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__WVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__WLAST = 1'h0;
	assign PBUS_MAIN_0__AXI4__NEPES_TX__ARVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__AWADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__AWID = 1'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__AWLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__AWSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__AWBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__AWLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__AWCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__AWPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__WDATA = 32'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__WSTRB = 4'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__ARID = 1'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__ARADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__ARLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__ARSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__ARBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__ARLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__ARCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__ARPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__AWVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__WVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__WLAST = 1'h0;
	assign PBUS_MAIN_0__AXI4__EYL_TX__ARVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__AWADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__AWID = 1'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__AWLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__AWSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__AWBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__AWLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__AWCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__AWPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__WDATA = 32'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__WSTRB = 4'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__ARID = 1'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__ARADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__ARLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__ARSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__ARBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__ARLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__ARCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__ARPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__AWVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__WVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__WLAST = 1'h0;
	assign PBUS_MAIN_0__AXI4__AIM_TX__ARVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__AWADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__AWID = 1'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__AWLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__AWSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__AWBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__AWLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__AWCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__AWPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__WDATA = 32'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__WSTRB = 4'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__ARID = 1'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__ARADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__ARLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__ARSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__ARBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__ARLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__ARCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__ARPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__AWVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__WVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__WLAST = 1'h0;
	assign PBUS_MAIN_0__AXI4__CAMERA_TX__ARVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__AWADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__AWID = 1'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__AWLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__AWSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__AWBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__AWLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__AWCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__AWPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__WDATA = 32'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__WSTRB = 4'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__ARID = 1'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__ARADDR = 32'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__ARLEN = 8'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__ARSIZE = 3'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__ARBURST = 2'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__ARLOCK = 1'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__ARCACHE = 4'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__ARPROT = 3'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__AWVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__WVALID = 1'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__WLAST = 1'h0;
	assign PBUS_MAIN_0__AXI4__VIDEO_TX__ARVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__CPU_RX__BID = 9'h0;
	assign MBUS_MAIN_0__AXI4__CPU_RX__BVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__CPU_RX__BRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__CPU_RX__RID = 9'h0;
	assign MBUS_MAIN_0__AXI4__CPU_RX__RDATA = 128'h0;
	assign MBUS_MAIN_0__AXI4__CPU_RX__RRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__CPU_RX__RLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__CPU_RX__RVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__CAMERA_RX__BID = 5'h0;
	assign MBUS_MAIN_0__AXI4__CAMERA_RX__BVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__CAMERA_RX__BRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__CAMERA_RX__RID = 5'h0;
	assign MBUS_MAIN_0__AXI4__CAMERA_RX__RDATA = 128'h0;
	assign MBUS_MAIN_0__AXI4__CAMERA_RX__RRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__CAMERA_RX__RLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__CAMERA_RX__RVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__VIDEO_RX__BID = 5'h0;
	assign MBUS_MAIN_0__AXI4__VIDEO_RX__BVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__VIDEO_RX__BRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__VIDEO_RX__RID = 5'h0;
	assign MBUS_MAIN_0__AXI4__VIDEO_RX__RDATA = 64'h0;
	assign MBUS_MAIN_0__AXI4__VIDEO_RX__RRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__VIDEO_RX__RLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__VIDEO_RX__RVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__PERI_RX__BID = 6'h0;
	assign MBUS_MAIN_0__AXI4__PERI_RX__BVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__PERI_RX__BRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__PERI_RX__RID = 6'h0;
	assign MBUS_MAIN_0__AXI4__PERI_RX__RDATA = 64'h0;
	assign MBUS_MAIN_0__AXI4__PERI_RX__RRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__PERI_RX__RLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__PERI_RX__RVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__USB3_RX__BID = 6'h0;
	assign MBUS_MAIN_0__AXI4__USB3_RX__BVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__USB3_RX__BRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__USB3_RX__RID = 6'h0;
	assign MBUS_MAIN_0__AXI4__USB3_RX__RDATA = 64'h0;
	assign MBUS_MAIN_0__AXI4__USB3_RX__RRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__USB3_RX__RLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__USB3_RX__RVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__NEPES0_RX__BID = 7'h0;
	assign MBUS_MAIN_0__AXI4__NEPES0_RX__BVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__NEPES0_RX__BRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__NEPES0_RX__RID = 7'h0;
	assign MBUS_MAIN_0__AXI4__NEPES0_RX__RDATA = 256'h0;
	assign MBUS_MAIN_0__AXI4__NEPES0_RX__RRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__NEPES0_RX__RLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__NEPES0_RX__RVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__NEPES1_RX__BID = 7'h0;
	assign MBUS_MAIN_0__AXI4__NEPES1_RX__BVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__NEPES1_RX__BRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__NEPES1_RX__RID = 7'h0;
	assign MBUS_MAIN_0__AXI4__NEPES1_RX__RDATA = 256'h0;
	assign MBUS_MAIN_0__AXI4__NEPES1_RX__RRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__NEPES1_RX__RLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__NEPES1_RX__RVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__NEPES2_RX__BID = 7'h0;
	assign MBUS_MAIN_0__AXI4__NEPES2_RX__BVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__NEPES2_RX__BRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__NEPES2_RX__RID = 7'h0;
	assign MBUS_MAIN_0__AXI4__NEPES2_RX__RDATA = 256'h0;
	assign MBUS_MAIN_0__AXI4__NEPES2_RX__RRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__NEPES2_RX__RLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__NEPES2_RX__RVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__EYL0_RX__BID = 7'h0;
	assign MBUS_MAIN_0__AXI4__EYL0_RX__BVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__EYL0_RX__BRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__EYL0_RX__RID = 7'h0;
	assign MBUS_MAIN_0__AXI4__EYL0_RX__RDATA = 256'h0;
	assign MBUS_MAIN_0__AXI4__EYL0_RX__RRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__EYL0_RX__RLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__EYL0_RX__RVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__EYL1_RX__BID = 7'h0;
	assign MBUS_MAIN_0__AXI4__EYL1_RX__BVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__EYL1_RX__BRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__EYL1_RX__RID = 7'h0;
	assign MBUS_MAIN_0__AXI4__EYL1_RX__RDATA = 256'h0;
	assign MBUS_MAIN_0__AXI4__EYL1_RX__RRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__EYL1_RX__RLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__EYL1_RX__RVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__EYL2_RX__BID = 7'h0;
	assign MBUS_MAIN_0__AXI4__EYL2_RX__BVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__EYL2_RX__BRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__EYL2_RX__RID = 7'h0;
	assign MBUS_MAIN_0__AXI4__EYL2_RX__RDATA = 256'h0;
	assign MBUS_MAIN_0__AXI4__EYL2_RX__RRESP = 2'h0;
	assign MBUS_MAIN_0__AXI4__EYL2_RX__RLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__EYL2_RX__RVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__ARADDR = 36'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__ARBURST = 2'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__ARCACHE = 4'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__ARID = 12'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__ARLEN = 8'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__ARLOCK = 1'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__ARPROT = 3'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__ARUSER = 10'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__ARSIZE = 3'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__ARVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__AWADDR = 36'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__AWBURST = 2'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__AWCACHE = 4'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__AWID = 12'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__AWLEN = 8'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__AWLOCK = 1'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__AWPROT = 3'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__AWUSER = 10'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__AWSIZE = 3'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__AWVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__WDATA = 256'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__WLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__WSTRB = 32'h0;
	assign MBUS_MAIN_0__AXI4__DDR0_TX__WVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__ARADDR = 36'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__ARBURST = 2'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__ARCACHE = 4'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__ARID = 12'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__ARLEN = 8'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__ARLOCK = 1'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__ARPROT = 3'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__ARUSER = 10'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__ARSIZE = 3'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__ARVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__AWADDR = 36'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__AWBURST = 2'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__AWCACHE = 4'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__AWID = 12'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__AWLEN = 8'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__AWLOCK = 1'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__AWPROT = 3'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__AWUSER = 10'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__AWSIZE = 3'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__AWVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__WDATA = 256'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__WLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__WSTRB = 32'h0;
	assign MBUS_MAIN_0__AXI4__DDR1_TX__WVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__ARADDR = 32'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__ARBURST = 2'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__ARCACHE = 4'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__ARID = 10'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__ARLEN = 8'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__ARLOCK = 1'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__ARPROT = 3'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__ARSIZE = 3'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__ARVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__AWADDR = 32'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__AWBURST = 2'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__AWCACHE = 4'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__AWID = 10'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__AWLEN = 8'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__AWLOCK = 1'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__AWPROT = 3'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__AWSIZE = 3'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__AWVALID = 1'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__WDATA = 64'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__WLAST = 1'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__WSTRB = 8'h0;
	assign MBUS_MAIN_0__AXI4__CPU_TX__WVALID = 1'h0;
	assign SCU_MAIN_0__DFT__SCAN_CLK = 1'h0;
	assign SCU_MAIN_0__DFT__SCAN_RSTN = 1'h0;

endmodule 
