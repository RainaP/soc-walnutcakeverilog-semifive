//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLXbar_10_assert(
  input         clock,
  input         reset,
  input         auto_in_1_a_valid,
  input  [2:0]  auto_in_1_a_bits_opcode,
  input         auto_in_1_a_bits_source,
  input  [35:0] auto_in_1_a_bits_address,
  input         auto_in_0_a_valid,
  input  [2:0]  auto_in_0_a_bits_opcode,
  input  [2:0]  auto_in_0_a_bits_param,
  input  [3:0]  auto_in_0_a_bits_size,
  input  [3:0]  auto_in_0_a_bits_source,
  input  [35:0] auto_in_0_a_bits_address,
  input  [7:0]  auto_in_0_a_bits_mask,
  input         auto_in_0_a_bits_corrupt,
  input         auto_in_0_b_ready,
  input         auto_in_0_c_valid,
  input  [2:0]  auto_in_0_c_bits_opcode,
  input  [2:0]  auto_in_0_c_bits_param,
  input  [3:0]  auto_in_0_c_bits_size,
  input  [3:0]  auto_in_0_c_bits_source,
  input  [35:0] auto_in_0_c_bits_address,
  input         auto_in_0_c_bits_corrupt,
  input         auto_in_0_d_ready,
  input         auto_in_0_e_valid,
  input  [3:0]  auto_in_0_e_bits_sink,
  input         auto_out_a_ready,
  input         auto_out_b_valid,
  input  [1:0]  auto_out_b_bits_param,
  input  [4:0]  auto_out_b_bits_source,
  input  [35:0] auto_out_b_bits_address,
  input         auto_out_c_ready,
  input         auto_out_d_valid,
  input  [2:0]  auto_out_d_bits_opcode,
  input  [1:0]  auto_out_d_bits_param,
  input  [3:0]  auto_out_d_bits_size,
  input  [4:0]  auto_out_d_bits_source,
  input  [3:0]  auto_out_d_bits_sink,
  input         auto_out_d_bits_denied,
  input         auto_out_d_bits_corrupt,
  input         auto_out_e_ready,
  input         _T_43,
  input         _T_59,
  input         _T_67,
  input         _T_203,
  input         _T_204,
  input         _T_218,
  input         _T_235_0,
  input         _T_235_1
);
  wire  TLMonitor_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_b_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_b_valid; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_b_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_b_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_io_in_b_bits_address; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_c_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_c_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_c_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_c_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_c_bits_size; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_c_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_io_in_c_bits_address; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_c_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_d_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_d_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_e_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_e_valid; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_e_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_1_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_1_io_in_d_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_1_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_1_io_in_d_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  _T_207; // @[Arbiter.scala 74:52]
  wire  _T_209; // @[Arbiter.scala 75:62]
  wire  _T_212; // @[Arbiter.scala 75:62]
  wire  _T_213; // @[Arbiter.scala 75:59]
  wire  _T_216; // @[Arbiter.scala 75:13]
  wire  _T_217; // @[Arbiter.scala 75:13]
  wire  _T_219; // @[Arbiter.scala 77:15]
  wire  _T_221; // @[Arbiter.scala 77:36]
  wire  _T_223; // @[Arbiter.scala 77:14]
  wire  _T_224; // @[Arbiter.scala 77:14]
  SiFive_TLMonitor_68_assert TLMonitor ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_clock),
    .reset(TLMonitor_reset),
    .io_in_a_ready(TLMonitor_io_in_a_ready),
    .io_in_a_valid(TLMonitor_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_io_in_a_bits_corrupt),
    .io_in_b_ready(TLMonitor_io_in_b_ready),
    .io_in_b_valid(TLMonitor_io_in_b_valid),
    .io_in_b_bits_param(TLMonitor_io_in_b_bits_param),
    .io_in_b_bits_source(TLMonitor_io_in_b_bits_source),
    .io_in_b_bits_address(TLMonitor_io_in_b_bits_address),
    .io_in_c_ready(TLMonitor_io_in_c_ready),
    .io_in_c_valid(TLMonitor_io_in_c_valid),
    .io_in_c_bits_opcode(TLMonitor_io_in_c_bits_opcode),
    .io_in_c_bits_param(TLMonitor_io_in_c_bits_param),
    .io_in_c_bits_size(TLMonitor_io_in_c_bits_size),
    .io_in_c_bits_source(TLMonitor_io_in_c_bits_source),
    .io_in_c_bits_address(TLMonitor_io_in_c_bits_address),
    .io_in_c_bits_corrupt(TLMonitor_io_in_c_bits_corrupt),
    .io_in_d_ready(TLMonitor_io_in_d_ready),
    .io_in_d_valid(TLMonitor_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_io_in_d_bits_opcode),
    .io_in_d_bits_param(TLMonitor_io_in_d_bits_param),
    .io_in_d_bits_size(TLMonitor_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_io_in_d_bits_source),
    .io_in_d_bits_sink(TLMonitor_io_in_d_bits_sink),
    .io_in_d_bits_denied(TLMonitor_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_io_in_d_bits_corrupt),
    .io_in_e_ready(TLMonitor_io_in_e_ready),
    .io_in_e_valid(TLMonitor_io_in_e_valid),
    .io_in_e_bits_sink(TLMonitor_io_in_e_bits_sink)
  );
  SiFive_TLMonitor_69_assert TLMonitor_1 ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_1_clock),
    .reset(TLMonitor_1_reset),
    .io_in_a_ready(TLMonitor_1_io_in_a_ready),
    .io_in_a_valid(TLMonitor_1_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_1_io_in_a_bits_opcode),
    .io_in_a_bits_source(TLMonitor_1_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_1_io_in_a_bits_address),
    .io_in_d_valid(TLMonitor_1_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_1_io_in_d_bits_opcode),
    .io_in_d_bits_param(TLMonitor_1_io_in_d_bits_param),
    .io_in_d_bits_size(TLMonitor_1_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_1_io_in_d_bits_source),
    .io_in_d_bits_sink(TLMonitor_1_io_in_d_bits_sink),
    .io_in_d_bits_denied(TLMonitor_1_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_1_io_in_d_bits_corrupt)
  );
  assign _T_207 = _T_203 | _T_204; // @[Arbiter.scala 74:52]
  assign _T_209 = ~_T_203; // @[Arbiter.scala 75:62]
  assign _T_212 = ~_T_204; // @[Arbiter.scala 75:62]
  assign _T_213 = _T_209 | _T_212; // @[Arbiter.scala 75:59]
  assign _T_216 = _T_213 | reset; // @[Arbiter.scala 75:13]
  assign _T_217 = ~_T_216; // @[Arbiter.scala 75:13]
  assign _T_219 = ~_T_218; // @[Arbiter.scala 77:15]
  assign _T_221 = _T_219 | _T_207; // @[Arbiter.scala 77:36]
  assign _T_223 = _T_221 | reset; // @[Arbiter.scala 77:14]
  assign _T_224 = ~_T_223; // @[Arbiter.scala 77:14]
  assign TLMonitor_clock = clock;
  assign TLMonitor_reset = reset;
  assign TLMonitor_io_in_a_ready = auto_out_a_ready & _T_235_0; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_valid = auto_in_0_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_opcode = auto_in_0_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_param = auto_in_0_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_size = auto_in_0_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_source = auto_in_0_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_address = auto_in_0_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_mask = auto_in_0_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_corrupt = auto_in_0_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_b_ready = auto_in_0_b_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_b_valid = auto_out_b_valid & _T_43; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_b_bits_param = auto_out_b_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_b_bits_source = auto_out_b_bits_source[3:0]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_b_bits_address = auto_out_b_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_ready = auto_out_c_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_valid = auto_in_0_c_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_opcode = auto_in_0_c_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_param = auto_in_0_c_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_size = auto_in_0_c_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_source = auto_in_0_c_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_address = auto_in_0_c_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_corrupt = auto_in_0_c_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_ready = auto_in_0_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_valid = auto_out_d_valid & _T_59; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_param = auto_out_d_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_size = auto_out_d_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_source = auto_out_d_bits_source[3:0]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_sink = auto_out_d_bits_sink; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_denied = auto_out_d_bits_denied; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_corrupt = auto_out_d_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_e_ready = auto_out_e_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_e_valid = auto_in_0_e_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_e_bits_sink = auto_in_0_e_bits_sink; // @[Nodes.scala 26:19]
  assign TLMonitor_1_clock = clock;
  assign TLMonitor_1_reset = reset;
  assign TLMonitor_1_io_in_a_ready = auto_out_a_ready & _T_235_1; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_valid = auto_in_1_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_opcode = auto_in_1_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_source = auto_in_1_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_address = auto_in_1_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_valid = auto_out_d_valid & _T_67; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_param = auto_out_d_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_size = auto_out_d_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_source = auto_out_d_bits_source[0]; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_sink = auto_out_d_bits_sink; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_denied = auto_out_d_bits_denied; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_corrupt = auto_out_d_bits_corrupt; // @[Nodes.scala 26:19]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_217) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_217) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_224) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_224) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
