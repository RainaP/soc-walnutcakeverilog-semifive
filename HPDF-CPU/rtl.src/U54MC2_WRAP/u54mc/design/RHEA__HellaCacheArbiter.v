//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__HellaCacheArbiter(
  input         rf_reset,
  input         clock,
  output        io_requestor_0_req_ready,
  input         io_requestor_0_req_valid,
  input  [39:0] io_requestor_0_req_bits_addr,
  input  [39:0] io_requestor_0_req_bits_idx,
  input         io_requestor_0_s1_kill,
  output        io_requestor_0_s2_nack,
  output        io_requestor_0_resp_valid,
  output [63:0] io_requestor_0_resp_bits_data,
  output        io_requestor_0_s2_xcpt_ae_ld,
  output        io_requestor_1_req_ready,
  input         io_requestor_1_req_valid,
  input  [39:0] io_requestor_1_req_bits_addr,
  input  [39:0] io_requestor_1_req_bits_idx,
  input  [6:0]  io_requestor_1_req_bits_tag,
  input  [4:0]  io_requestor_1_req_bits_cmd,
  input  [1:0]  io_requestor_1_req_bits_size,
  input         io_requestor_1_req_bits_signed,
  input  [1:0]  io_requestor_1_req_bits_dprv,
  input         io_requestor_1_req_bits_phys,
  input         io_requestor_1_req_bits_no_alloc,
  input         io_requestor_1_req_bits_no_xcpt,
  input  [7:0]  io_requestor_1_req_bits_mask,
  input         io_requestor_1_s1_kill,
  input  [63:0] io_requestor_1_s1_data_data,
  input  [7:0]  io_requestor_1_s1_data_mask,
  output        io_requestor_1_s2_nack,
  output        io_requestor_1_resp_valid,
  output [39:0] io_requestor_1_resp_bits_addr,
  output [6:0]  io_requestor_1_resp_bits_tag,
  output [4:0]  io_requestor_1_resp_bits_cmd,
  output [1:0]  io_requestor_1_resp_bits_size,
  output        io_requestor_1_resp_bits_signed,
  output [63:0] io_requestor_1_resp_bits_data,
  output        io_requestor_1_resp_bits_replay,
  output        io_requestor_1_resp_bits_has_data,
  output [63:0] io_requestor_1_resp_bits_data_word_bypass,
  output        io_requestor_1_replay_next,
  output        io_requestor_1_s2_xcpt_ma_ld,
  output        io_requestor_1_s2_xcpt_ma_st,
  output        io_requestor_1_s2_xcpt_pf_ld,
  output        io_requestor_1_s2_xcpt_pf_st,
  output        io_requestor_1_s2_xcpt_ae_ld,
  output        io_requestor_1_s2_xcpt_ae_st,
  output        io_requestor_1_ordered,
  output        io_requestor_1_perf_acquire,
  output        io_requestor_1_perf_release,
  output        io_requestor_1_perf_grant,
  output        io_requestor_1_perf_tlbMiss,
  output        io_requestor_1_perf_blocked,
  input         io_requestor_1_keep_clock_enabled,
  output        io_requestor_1_clock_enabled,
  input         io_mem_req_ready,
  output        io_mem_req_valid,
  output [39:0] io_mem_req_bits_addr,
  output [39:0] io_mem_req_bits_idx,
  output [6:0]  io_mem_req_bits_tag,
  output [4:0]  io_mem_req_bits_cmd,
  output [1:0]  io_mem_req_bits_size,
  output        io_mem_req_bits_signed,
  output [1:0]  io_mem_req_bits_dprv,
  output        io_mem_req_bits_phys,
  output        io_mem_req_bits_no_alloc,
  output        io_mem_req_bits_no_xcpt,
  output [7:0]  io_mem_req_bits_mask,
  output        io_mem_s1_kill,
  output [63:0] io_mem_s1_data_data,
  output [7:0]  io_mem_s1_data_mask,
  input         io_mem_s2_nack,
  input         io_mem_resp_valid,
  input  [39:0] io_mem_resp_bits_addr,
  input  [6:0]  io_mem_resp_bits_tag,
  input  [4:0]  io_mem_resp_bits_cmd,
  input  [1:0]  io_mem_resp_bits_size,
  input         io_mem_resp_bits_signed,
  input  [63:0] io_mem_resp_bits_data,
  input         io_mem_resp_bits_replay,
  input         io_mem_resp_bits_has_data,
  input  [63:0] io_mem_resp_bits_data_word_bypass,
  input         io_mem_replay_next,
  input         io_mem_s2_xcpt_ma_ld,
  input         io_mem_s2_xcpt_ma_st,
  input         io_mem_s2_xcpt_pf_ld,
  input         io_mem_s2_xcpt_pf_st,
  input         io_mem_s2_xcpt_ae_ld,
  input         io_mem_s2_xcpt_ae_st,
  input         io_mem_ordered,
  input         io_mem_perf_acquire,
  input         io_mem_perf_release,
  input         io_mem_perf_grant,
  input         io_mem_perf_tlbMiss,
  input         io_mem_perf_blocked,
  output        io_mem_keep_clock_enabled,
  input         io_mem_clock_enabled
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
`endif // RANDOMIZE_REG_INIT
  reg  s1_id; // @[HellaCacheArbiter.scala 19:20]
  reg  s2_id; // @[HellaCacheArbiter.scala 20:20]
  wire [7:0] _io_mem_req_bits_tag_T = {io_requestor_1_req_bits_tag,1'h1}; // @[Cat.scala 30:58]
  wire [7:0] _GEN_2 = io_requestor_0_req_valid ? 8'h0 : _io_mem_req_bits_tag_T; // @[HellaCacheArbiter.scala 49:26 HellaCacheArbiter.scala 33:29 HellaCacheArbiter.scala 33:29]
  wire  _T_1 = ~s2_id; // @[HellaCacheArbiter.scala 51:21]
  wire  tag_hit = ~io_mem_resp_bits_tag[0]; // @[HellaCacheArbiter.scala 59:57]
  assign io_requestor_0_req_ready = io_mem_req_ready; // @[HellaCacheArbiter.scala 25:31]
  assign io_requestor_0_s2_nack = io_mem_s2_nack & _T_1; // @[HellaCacheArbiter.scala 64:49]
  assign io_requestor_0_resp_valid = io_mem_resp_valid & tag_hit; // @[HellaCacheArbiter.scala 60:39]
  assign io_requestor_0_resp_bits_data = io_mem_resp_bits_data; // @[HellaCacheArbiter.scala 69:17]
  assign io_requestor_0_s2_xcpt_ae_ld = io_mem_s2_xcpt_ae_ld; // @[HellaCacheArbiter.scala 61:31]
  assign io_requestor_1_req_ready = io_requestor_0_req_ready & ~io_requestor_0_req_valid; // @[HellaCacheArbiter.scala 27:64]
  assign io_requestor_1_s2_nack = io_mem_s2_nack & s2_id; // @[HellaCacheArbiter.scala 64:49]
  assign io_requestor_1_resp_valid = io_mem_resp_valid & io_mem_resp_bits_tag[0]; // @[HellaCacheArbiter.scala 60:39]
  assign io_requestor_1_resp_bits_addr = io_mem_resp_bits_addr; // @[HellaCacheArbiter.scala 69:17]
  assign io_requestor_1_resp_bits_tag = {{1'd0}, io_mem_resp_bits_tag[6:1]}; // @[HellaCacheArbiter.scala 70:45]
  assign io_requestor_1_resp_bits_cmd = io_mem_resp_bits_cmd; // @[HellaCacheArbiter.scala 69:17]
  assign io_requestor_1_resp_bits_size = io_mem_resp_bits_size; // @[HellaCacheArbiter.scala 69:17]
  assign io_requestor_1_resp_bits_signed = io_mem_resp_bits_signed; // @[HellaCacheArbiter.scala 69:17]
  assign io_requestor_1_resp_bits_data = io_mem_resp_bits_data; // @[HellaCacheArbiter.scala 69:17]
  assign io_requestor_1_resp_bits_replay = io_mem_resp_bits_replay; // @[HellaCacheArbiter.scala 69:17]
  assign io_requestor_1_resp_bits_has_data = io_mem_resp_bits_has_data; // @[HellaCacheArbiter.scala 69:17]
  assign io_requestor_1_resp_bits_data_word_bypass = io_mem_resp_bits_data_word_bypass; // @[HellaCacheArbiter.scala 69:17]
  assign io_requestor_1_replay_next = io_mem_replay_next; // @[HellaCacheArbiter.scala 72:35]
  assign io_requestor_1_s2_xcpt_ma_ld = io_mem_s2_xcpt_ma_ld; // @[HellaCacheArbiter.scala 61:31]
  assign io_requestor_1_s2_xcpt_ma_st = io_mem_s2_xcpt_ma_st; // @[HellaCacheArbiter.scala 61:31]
  assign io_requestor_1_s2_xcpt_pf_ld = io_mem_s2_xcpt_pf_ld; // @[HellaCacheArbiter.scala 61:31]
  assign io_requestor_1_s2_xcpt_pf_st = io_mem_s2_xcpt_pf_st; // @[HellaCacheArbiter.scala 61:31]
  assign io_requestor_1_s2_xcpt_ae_ld = io_mem_s2_xcpt_ae_ld; // @[HellaCacheArbiter.scala 61:31]
  assign io_requestor_1_s2_xcpt_ae_st = io_mem_s2_xcpt_ae_st; // @[HellaCacheArbiter.scala 61:31]
  assign io_requestor_1_ordered = io_mem_ordered; // @[HellaCacheArbiter.scala 62:31]
  assign io_requestor_1_perf_acquire = io_mem_perf_acquire; // @[HellaCacheArbiter.scala 63:28]
  assign io_requestor_1_perf_release = io_mem_perf_release; // @[HellaCacheArbiter.scala 63:28]
  assign io_requestor_1_perf_grant = io_mem_perf_grant; // @[HellaCacheArbiter.scala 63:28]
  assign io_requestor_1_perf_tlbMiss = io_mem_perf_tlbMiss; // @[HellaCacheArbiter.scala 63:28]
  assign io_requestor_1_perf_blocked = io_mem_perf_blocked; // @[HellaCacheArbiter.scala 63:28]
  assign io_requestor_1_clock_enabled = io_mem_clock_enabled; // @[HellaCacheArbiter.scala 68:37]
  assign io_mem_req_valid = io_requestor_0_req_valid | io_requestor_1_req_valid; // @[HellaCacheArbiter.scala 24:63]
  assign io_mem_req_bits_addr = io_requestor_0_req_valid ? io_requestor_0_req_bits_addr : io_requestor_1_req_bits_addr; // @[HellaCacheArbiter.scala 49:26 HellaCacheArbiter.scala 32:25 HellaCacheArbiter.scala 32:25]
  assign io_mem_req_bits_idx = io_requestor_0_req_valid ? io_requestor_0_req_bits_idx : io_requestor_1_req_bits_idx; // @[HellaCacheArbiter.scala 49:26 HellaCacheArbiter.scala 32:25 HellaCacheArbiter.scala 32:25]
  assign io_mem_req_bits_tag = _GEN_2[6:0];
  assign io_mem_req_bits_cmd = io_requestor_0_req_valid ? 5'h0 : io_requestor_1_req_bits_cmd; // @[HellaCacheArbiter.scala 49:26 HellaCacheArbiter.scala 32:25 HellaCacheArbiter.scala 32:25]
  assign io_mem_req_bits_size = io_requestor_0_req_valid ? 2'h3 : io_requestor_1_req_bits_size; // @[HellaCacheArbiter.scala 49:26 HellaCacheArbiter.scala 32:25 HellaCacheArbiter.scala 32:25]
  assign io_mem_req_bits_signed = io_requestor_0_req_valid ? 1'h0 : io_requestor_1_req_bits_signed; // @[HellaCacheArbiter.scala 49:26 HellaCacheArbiter.scala 32:25 HellaCacheArbiter.scala 32:25]
  assign io_mem_req_bits_dprv = io_requestor_0_req_valid ? 2'h1 : io_requestor_1_req_bits_dprv; // @[HellaCacheArbiter.scala 49:26 HellaCacheArbiter.scala 32:25 HellaCacheArbiter.scala 32:25]
  assign io_mem_req_bits_phys = io_requestor_0_req_valid | io_requestor_1_req_bits_phys; // @[HellaCacheArbiter.scala 49:26 HellaCacheArbiter.scala 32:25 HellaCacheArbiter.scala 32:25]
  assign io_mem_req_bits_no_alloc = io_requestor_0_req_valid ? 1'h0 : io_requestor_1_req_bits_no_alloc; // @[HellaCacheArbiter.scala 49:26 HellaCacheArbiter.scala 32:25 HellaCacheArbiter.scala 32:25]
  assign io_mem_req_bits_no_xcpt = io_requestor_0_req_valid ? 1'h0 : io_requestor_1_req_bits_no_xcpt; // @[HellaCacheArbiter.scala 49:26 HellaCacheArbiter.scala 32:25 HellaCacheArbiter.scala 32:25]
  assign io_mem_req_bits_mask = io_requestor_0_req_valid ? 8'h0 : io_requestor_1_req_bits_mask; // @[HellaCacheArbiter.scala 49:26 HellaCacheArbiter.scala 32:25 HellaCacheArbiter.scala 32:25]
  assign io_mem_s1_kill = ~s1_id ? io_requestor_0_s1_kill : io_requestor_1_s1_kill; // @[HellaCacheArbiter.scala 50:34 HellaCacheArbiter.scala 37:24 HellaCacheArbiter.scala 37:24]
  assign io_mem_s1_data_data = ~s1_id ? 64'h0 : io_requestor_1_s1_data_data; // @[HellaCacheArbiter.scala 50:34 HellaCacheArbiter.scala 38:24 HellaCacheArbiter.scala 38:24]
  assign io_mem_s1_data_mask = ~s1_id ? 8'h0 : io_requestor_1_s1_data_mask; // @[HellaCacheArbiter.scala 50:34 HellaCacheArbiter.scala 38:24 HellaCacheArbiter.scala 38:24]
  assign io_mem_keep_clock_enabled = io_requestor_1_keep_clock_enabled; // @[HellaCacheArbiter.scala 22:81]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_id <= 1'h0;
    end else if (io_requestor_0_req_valid) begin
      s1_id <= 1'h0;
    end else begin
      s1_id <= 1'h1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_id <= 1'h0;
    end else begin
      s2_id <= s1_id;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  s1_id = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  s2_id = _RAND_1[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    s1_id = 1'h0;
  end
  if (rf_reset) begin
    s2_id = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
