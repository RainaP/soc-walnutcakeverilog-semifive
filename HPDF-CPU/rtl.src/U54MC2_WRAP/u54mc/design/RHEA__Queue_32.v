//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__Queue_32(
  input         rf_reset,
  input         clock,
  input         reset,
  output        io_enq_ready,
  input         io_enq_valid,
  input  [63:0] io_enq_bits_data,
  input  [7:0]  io_enq_bits_strb,
  input         io_enq_bits_last,
  input         io_deq_ready,
  output        io_deq_valid,
  output [63:0] io_deq_bits_data,
  output [7:0]  io_deq_bits_strb,
  output        io_deq_bits_last
);
`ifdef RANDOMIZE_REG_INIT
  reg [63:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [63:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
`endif // RANDOMIZE_REG_INIT
  reg [63:0] ram_0_data; // @[Decoupled.scala 218:16]
  reg [7:0] ram_0_strb; // @[Decoupled.scala 218:16]
  reg  ram_0_last; // @[Decoupled.scala 218:16]
  reg [63:0] ram_1_data; // @[Decoupled.scala 218:16]
  reg [7:0] ram_1_strb; // @[Decoupled.scala 218:16]
  reg  ram_1_last; // @[Decoupled.scala 218:16]
  reg  value_1; // @[Counter.scala 60:40]
  wire  do_enq = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  reg  value; // @[Counter.scala 60:40]
  reg  maybe_full; // @[Decoupled.scala 221:27]
  wire  ptr_match = value == value_1; // @[Decoupled.scala 223:33]
  wire  empty = ptr_match & ~maybe_full; // @[Decoupled.scala 224:25]
  wire  full = ptr_match & maybe_full; // @[Decoupled.scala 225:24]
  wire  do_deq = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  assign io_enq_ready = ~full; // @[Decoupled.scala 241:19]
  assign io_deq_valid = ~empty; // @[Decoupled.scala 240:19]
  assign io_deq_bits_data = value_1 ? ram_1_data : ram_0_data; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_strb = value_1 ? ram_1_strb : ram_0_strb; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_last = value_1 ? ram_1_last : ram_0_last; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_data <= 64'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_strb <= 8'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_strb <= io_enq_bits_strb;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_last <= 1'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_last <= io_enq_bits_last;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_data <= 64'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_strb <= 8'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_strb <= io_enq_bits_strb;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_last <= 1'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_last <= io_enq_bits_last;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      value_1 <= 1'h0;
    end else if (do_deq) begin
      value_1 <= value_1 + 1'h1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      value <= 1'h0;
    end else if (do_enq) begin
      value <= value + 1'h1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      maybe_full <= 1'h0;
    end else if (do_enq != do_deq) begin
      maybe_full <= do_enq;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {2{`RANDOM}};
  ram_0_data = _RAND_0[63:0];
  _RAND_1 = {1{`RANDOM}};
  ram_0_strb = _RAND_1[7:0];
  _RAND_2 = {1{`RANDOM}};
  ram_0_last = _RAND_2[0:0];
  _RAND_3 = {2{`RANDOM}};
  ram_1_data = _RAND_3[63:0];
  _RAND_4 = {1{`RANDOM}};
  ram_1_strb = _RAND_4[7:0];
  _RAND_5 = {1{`RANDOM}};
  ram_1_last = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  value_1 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  value = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  maybe_full = _RAND_8[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ram_0_data = 64'h0;
  end
  if (rf_reset) begin
    ram_0_strb = 8'h0;
  end
  if (rf_reset) begin
    ram_0_last = 1'h0;
  end
  if (rf_reset) begin
    ram_1_data = 64'h0;
  end
  if (rf_reset) begin
    ram_1_strb = 8'h0;
  end
  if (rf_reset) begin
    ram_1_last = 1'h0;
  end
  if (reset) begin
    value_1 = 1'h0;
  end
  if (reset) begin
    value = 1'h0;
  end
  if (reset) begin
    maybe_full = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
