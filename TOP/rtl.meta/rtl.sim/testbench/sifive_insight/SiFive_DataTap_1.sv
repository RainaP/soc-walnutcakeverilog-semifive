//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_DataTap_1(
  output        _5,
  output [63:0] _4,
  output        _3,
  output        _2,
  output        _1,
  output        _0
);
  assign _5 = rocket.core.csr.io_trace_0_interrupt;
  assign _4 = rocket.core.csr.io_trace_0_cause;
  assign _3 = rocket.core.csr.io_trace_0_exception;
  assign _2 = rocket.core.csr.io_trace_0_valid;
  assign _1 = rocket.core.reset;
  assign _0 = rocket.core.rocket_clock_gate_out;
endmodule
