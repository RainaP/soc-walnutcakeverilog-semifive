//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__FPUDecoder(
  input  [31:0] io_inst,
  output        io_sigs_ldst,
  output        io_sigs_wen,
  output        io_sigs_ren1,
  output        io_sigs_ren2,
  output        io_sigs_ren3,
  output        io_sigs_swap12,
  output        io_sigs_swap23,
  output [1:0]  io_sigs_typeTagIn,
  output [1:0]  io_sigs_typeTagOut,
  output        io_sigs_fromint,
  output        io_sigs_toint,
  output        io_sigs_fastpipe,
  output        io_sigs_fma,
  output        io_sigs_div,
  output        io_sigs_sqrt,
  output        io_sigs_wflags
);
  wire [31:0] _decoder_T = io_inst & 32'h40; // @[Decode.scala 14:65]
  wire  decoder_0 = _decoder_T == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_2 = io_inst & 32'h80000020; // @[Decode.scala 14:65]
  wire  _decoder_T_3 = _decoder_T_2 == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_4 = io_inst & 32'h30; // @[Decode.scala 14:65]
  wire  _decoder_T_5 = _decoder_T_4 == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_6 = io_inst & 32'h10000020; // @[Decode.scala 14:65]
  wire  _decoder_T_7 = _decoder_T_6 == 32'h10000000; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_10 = io_inst & 32'h80000004; // @[Decode.scala 14:65]
  wire  _decoder_T_11 = _decoder_T_10 == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_12 = io_inst & 32'h10000004; // @[Decode.scala 14:65]
  wire  _decoder_T_13 = _decoder_T_12 == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_14 = io_inst & 32'h50; // @[Decode.scala 14:65]
  wire  decoder_4 = _decoder_T_14 == 32'h40; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_18 = io_inst & 32'h40000004; // @[Decode.scala 14:65]
  wire  _decoder_T_19 = _decoder_T_18 == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_20 = io_inst & 32'h20; // @[Decode.scala 14:65]
  wire  _decoder_T_21 = _decoder_T_20 == 32'h20; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_24 = io_inst & 32'h30000010; // @[Decode.scala 14:65]
  wire [31:0] _decoder_T_26 = io_inst & 32'h42000000; // @[Decode.scala 14:65]
  wire  _decoder_T_27 = _decoder_T_26 == 32'h2000000; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_28 = io_inst & 32'h2000010; // @[Decode.scala 14:65]
  wire  _decoder_T_29 = _decoder_T_28 == 32'h2000000; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_30 = io_inst & 32'h12000000; // @[Decode.scala 14:65]
  wire  _decoder_T_31 = _decoder_T_30 == 32'h12000000; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_32 = io_inst & 32'hd2000010; // @[Decode.scala 14:65]
  wire  _decoder_T_33 = _decoder_T_32 == 32'h40000010; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_34 = io_inst & 32'h70001010; // @[Decode.scala 14:65]
  wire  _decoder_T_35 = _decoder_T_34 == 32'h60000010; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_36 = io_inst & 32'h82000000; // @[Decode.scala 14:65]
  wire  _decoder_T_37 = _decoder_T_36 == 32'h82000000; // @[Decode.scala 14:121]
  wire  decoder_7 = decoder_0 | _decoder_T_27 | _decoder_T_29 | _decoder_T_31 | _decoder_T_33 | _decoder_T_35 |
    _decoder_T_37; // @[Decode.scala 15:30]
  wire [31:0] _decoder_T_44 = io_inst & 32'h1040; // @[Decode.scala 14:65]
  wire  _decoder_T_45 = _decoder_T_44 == 32'h1000; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_46 = io_inst & 32'h2000020; // @[Decode.scala 14:65]
  wire  _decoder_T_47 = _decoder_T_46 == 32'h2000000; // @[Decode.scala 14:121]
  wire  _decoder_T_49 = _decoder_T_24 == 32'h30000010; // @[Decode.scala 14:121]
  wire  decoder_8 = _decoder_T_45 | _decoder_T_47 | _decoder_T_49; // @[Decode.scala 15:30]
  wire [31:0] _decoder_T_52 = io_inst & 32'h90000010; // @[Decode.scala 14:65]
  wire  _decoder_T_55 = _decoder_T_52 == 32'h80000010; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_57 = io_inst & 32'ha0000010; // @[Decode.scala 14:65]
  wire  _decoder_T_58 = _decoder_T_57 == 32'h20000010; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_59 = io_inst & 32'hd0000010; // @[Decode.scala 14:65]
  wire  _decoder_T_60 = _decoder_T_59 == 32'h40000010; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_62 = io_inst & 32'h70000004; // @[Decode.scala 14:65]
  wire  _decoder_T_63 = _decoder_T_62 == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_64 = io_inst & 32'h68000004; // @[Decode.scala 14:65]
  wire  _decoder_T_65 = _decoder_T_64 == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_68 = io_inst & 32'h58000010; // @[Decode.scala 14:65]
  wire [31:0] _decoder_T_72 = io_inst & 32'h20000004; // @[Decode.scala 14:65]
  wire  _decoder_T_73 = _decoder_T_72 == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_74 = io_inst & 32'h8002000; // @[Decode.scala 14:65]
  wire  _decoder_T_75 = _decoder_T_74 == 32'h8000000; // @[Decode.scala 14:121]
  wire [31:0] _decoder_T_76 = io_inst & 32'hc0000004; // @[Decode.scala 14:65]
  wire  _decoder_T_77 = _decoder_T_76 == 32'h80000000; // @[Decode.scala 14:121]
  assign io_sigs_ldst = _decoder_T == 32'h0; // @[Decode.scala 14:121]
  assign io_sigs_wen = _decoder_T_3 | _decoder_T_5 | _decoder_T_7; // @[Decode.scala 15:30]
  assign io_sigs_ren1 = _decoder_T_11 | _decoder_T_13 | decoder_4; // @[Decode.scala 15:30]
  assign io_sigs_ren2 = _decoder_T_19 | _decoder_T_21 | decoder_4; // @[Decode.scala 15:30]
  assign io_sigs_ren3 = _decoder_T_14 == 32'h40; // @[Decode.scala 14:121]
  assign io_sigs_swap12 = _decoder_T == 32'h0; // @[Decode.scala 14:121]
  assign io_sigs_swap23 = _decoder_T_24 == 32'h10; // @[Decode.scala 14:121]
  assign io_sigs_typeTagIn = {{1'd0}, decoder_7}; // @[Decode.scala 15:30]
  assign io_sigs_typeTagOut = {{1'd0}, decoder_8}; // @[Decode.scala 15:30]
  assign io_sigs_fromint = _decoder_T_52 == 32'h90000010; // @[Decode.scala 14:121]
  assign io_sigs_toint = _decoder_T_21 | _decoder_T_55; // @[Decode.scala 15:30]
  assign io_sigs_fastpipe = _decoder_T_58 | _decoder_T_60; // @[Decode.scala 15:30]
  assign io_sigs_fma = _decoder_T_63 | _decoder_T_65 | decoder_4; // @[Decode.scala 15:30]
  assign io_sigs_div = _decoder_T_68 == 32'h18000010; // @[Decode.scala 14:121]
  assign io_sigs_sqrt = _decoder_T_59 == 32'h50000010; // @[Decode.scala 14:121]
  assign io_sigs_wflags = _decoder_T_73 | decoder_4 | _decoder_T_75 | _decoder_T_77; // @[Decode.scala 15:30]
endmodule
