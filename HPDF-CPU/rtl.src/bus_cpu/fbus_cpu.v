module fbus_cpu (/*AUTOARG*/
   // Outputs
   AXI4__RX__AWREADY, AXI4__RX__WREADY, AXI4__RX__BVALID,
   AXI4__RX__BID, AXI4__RX__BRESP, AXI4__RX__ARREADY,
   AXI4__RX__RVALID, AXI4__RX__RID, AXI4__RX__RDATA, AXI4__RX__RRESP,
   AXI4__RX__RLAST, AXI4__CBUS__CB2FB__AWREADY,
   AXI4__CBUS__CB2FB__WREADY, AXI4__CBUS__CB2FB__BVALID,
   AXI4__CBUS__CB2FB__BID, AXI4__CBUS__CB2FB__BRESP,
   AXI4__CBUS__CB2FB__ARREADY, AXI4__CBUS__CB2FB__RVALID,
   AXI4__CBUS__CB2FB__RID, AXI4__CBUS__CB2FB__RDATA,
   AXI4__CBUS__CB2FB__RRESP, AXI4__CBUS__CB2FB__RLAST,
   CPU_0__AXI4__CPU__FRONTPORT__AWID,
   CPU_0__AXI4__CPU__FRONTPORT__AWADDR,
   CPU_0__AXI4__CPU__FRONTPORT__AWLEN,
   CPU_0__AXI4__CPU__FRONTPORT__AWSIZE,
   CPU_0__AXI4__CPU__FRONTPORT__AWBURST,
   CPU_0__AXI4__CPU__FRONTPORT__AWLOCK,
   CPU_0__AXI4__CPU__FRONTPORT__AWCACHE,
   CPU_0__AXI4__CPU__FRONTPORT__AWPROT,
   CPU_0__AXI4__CPU__FRONTPORT__AWVALID,
   CPU_0__AXI4__CPU__FRONTPORT__WDATA,
   CPU_0__AXI4__CPU__FRONTPORT__WSTRB,
   CPU_0__AXI4__CPU__FRONTPORT__WLAST,
   CPU_0__AXI4__CPU__FRONTPORT__WVALID,
   CPU_0__AXI4__CPU__FRONTPORT__BREADY,
   CPU_0__AXI4__CPU__FRONTPORT__ARID,
   CPU_0__AXI4__CPU__FRONTPORT__ARADDR,
   CPU_0__AXI4__CPU__FRONTPORT__ARLEN,
   CPU_0__AXI4__CPU__FRONTPORT__ARSIZE,
   CPU_0__AXI4__CPU__FRONTPORT__ARBURST,
   CPU_0__AXI4__CPU__FRONTPORT__ARLOCK,
   CPU_0__AXI4__CPU__FRONTPORT__ARCACHE,
   CPU_0__AXI4__CPU__FRONTPORT__ARPROT,
   CPU_0__AXI4__CPU__FRONTPORT__ARVALID,
   CPU_0__AXI4__CPU__FRONTPORT__RREADY, ROM_0__AXI4__ROM__AWID,
   ROM_0__AXI4__ROM__AWADDR, ROM_0__AXI4__ROM__AWLEN,
   ROM_0__AXI4__ROM__AWSIZE, ROM_0__AXI4__ROM__AWBURST,
   ROM_0__AXI4__ROM__AWLOCK, ROM_0__AXI4__ROM__AWCACHE,
   ROM_0__AXI4__ROM__AWPROT, ROM_0__AXI4__ROM__AWVALID,
   ROM_0__AXI4__ROM__WDATA, ROM_0__AXI4__ROM__WSTRB,
   ROM_0__AXI4__ROM__WLAST, ROM_0__AXI4__ROM__WVALID,
   ROM_0__AXI4__ROM__BREADY, ROM_0__AXI4__ROM__ARID,
   ROM_0__AXI4__ROM__ARADDR, ROM_0__AXI4__ROM__ARLEN,
   ROM_0__AXI4__ROM__ARSIZE, ROM_0__AXI4__ROM__ARBURST,
   ROM_0__AXI4__ROM__ARLOCK, ROM_0__AXI4__ROM__ARCACHE,
   ROM_0__AXI4__ROM__ARPROT, ROM_0__AXI4__ROM__ARVALID,
   ROM_0__AXI4__ROM__RREADY, SRAM_0__AXI4__SRAM__AWID,
   SRAM_0__AXI4__SRAM__AWADDR, SRAM_0__AXI4__SRAM__AWLEN,
   SRAM_0__AXI4__SRAM__AWSIZE, SRAM_0__AXI4__SRAM__AWBURST,
   SRAM_0__AXI4__SRAM__AWLOCK, SRAM_0__AXI4__SRAM__AWCACHE,
   SRAM_0__AXI4__SRAM__AWPROT, SRAM_0__AXI4__SRAM__AWVALID,
   SRAM_0__AXI4__SRAM__WDATA, SRAM_0__AXI4__SRAM__WSTRB,
   SRAM_0__AXI4__SRAM__WLAST, SRAM_0__AXI4__SRAM__WVALID,
   SRAM_0__AXI4__SRAM__BREADY, SRAM_0__AXI4__SRAM__ARID,
   SRAM_0__AXI4__SRAM__ARADDR, SRAM_0__AXI4__SRAM__ARLEN,
   SRAM_0__AXI4__SRAM__ARSIZE, SRAM_0__AXI4__SRAM__ARBURST,
   SRAM_0__AXI4__SRAM__ARLOCK, SRAM_0__AXI4__SRAM__ARCACHE,
   SRAM_0__AXI4__SRAM__ARPROT, SRAM_0__AXI4__SRAM__ARVALID,
   SRAM_0__AXI4__SRAM__RREADY,
   // Inputs
   CLK__RX, RSTN__RX, CLK__FBUS, RSTN__FBUS, CLK__SRAM, RSTN__SRAM,
   CLK__ROM, RSTN__ROM, AXI4__RX__AWVALID, AXI4__RX__AWID,
   AXI4__RX__AWADDR, AXI4__RX__AWLEN, AXI4__RX__AWSIZE,
   AXI4__RX__AWBURST, AXI4__RX__AWLOCK, AXI4__RX__AWCACHE,
   AXI4__RX__AWPROT, AXI4__RX__AWUSER, AXI4__RX__WVALID,
   AXI4__RX__WDATA, AXI4__RX__WSTRB, AXI4__RX__WLAST,
   AXI4__RX__BREADY, AXI4__RX__ARVALID, AXI4__RX__ARID,
   AXI4__RX__ARADDR, AXI4__RX__ARLEN, AXI4__RX__ARSIZE,
   AXI4__RX__ARBURST, AXI4__RX__ARLOCK, AXI4__RX__ARCACHE,
   AXI4__RX__ARPROT, AXI4__RX__ARUSER, AXI4__RX__RREADY,
   AXI4__CBUS__CB2FB__AWVALID, AXI4__CBUS__CB2FB__AWID,
   AXI4__CBUS__CB2FB__AWADDR, AXI4__CBUS__CB2FB__AWLEN,
   AXI4__CBUS__CB2FB__AWSIZE, AXI4__CBUS__CB2FB__AWBURST,
   AXI4__CBUS__CB2FB__AWLOCK, AXI4__CBUS__CB2FB__AWCACHE,
   AXI4__CBUS__CB2FB__AWPROT, AXI4__CBUS__CB2FB__WVALID,
   AXI4__CBUS__CB2FB__WDATA, AXI4__CBUS__CB2FB__WSTRB,
   AXI4__CBUS__CB2FB__WLAST, AXI4__CBUS__CB2FB__BREADY,
   AXI4__CBUS__CB2FB__ARVALID, AXI4__CBUS__CB2FB__ARID,
   AXI4__CBUS__CB2FB__ARADDR, AXI4__CBUS__CB2FB__ARLEN,
   AXI4__CBUS__CB2FB__ARSIZE, AXI4__CBUS__CB2FB__ARBURST,
   AXI4__CBUS__CB2FB__ARLOCK, AXI4__CBUS__CB2FB__ARCACHE,
   AXI4__CBUS__CB2FB__ARPROT, AXI4__CBUS__CB2FB__RREADY,
   CPU_0__AXI4__CPU__FRONTPORT__AWREADY,
   CPU_0__AXI4__CPU__FRONTPORT__WREADY,
   CPU_0__AXI4__CPU__FRONTPORT__BID,
   CPU_0__AXI4__CPU__FRONTPORT__BVALID,
   CPU_0__AXI4__CPU__FRONTPORT__BRESP,
   CPU_0__AXI4__CPU__FRONTPORT__ARREADY,
   CPU_0__AXI4__CPU__FRONTPORT__RID,
   CPU_0__AXI4__CPU__FRONTPORT__RDATA,
   CPU_0__AXI4__CPU__FRONTPORT__RRESP,
   CPU_0__AXI4__CPU__FRONTPORT__RLAST,
   CPU_0__AXI4__CPU__FRONTPORT__RVALID, ROM_0__AXI4__ROM__AWREADY,
   ROM_0__AXI4__ROM__WREADY, ROM_0__AXI4__ROM__BID,
   ROM_0__AXI4__ROM__BVALID, ROM_0__AXI4__ROM__BRESP,
   ROM_0__AXI4__ROM__ARREADY, ROM_0__AXI4__ROM__RID,
   ROM_0__AXI4__ROM__RDATA, ROM_0__AXI4__ROM__RRESP,
   ROM_0__AXI4__ROM__RLAST, ROM_0__AXI4__ROM__RVALID,
   SRAM_0__AXI4__SRAM__AWREADY, SRAM_0__AXI4__SRAM__WREADY,
   SRAM_0__AXI4__SRAM__BID, SRAM_0__AXI4__SRAM__BVALID,
   SRAM_0__AXI4__SRAM__BRESP, SRAM_0__AXI4__SRAM__ARREADY,
   SRAM_0__AXI4__SRAM__RID, SRAM_0__AXI4__SRAM__RDATA,
   SRAM_0__AXI4__SRAM__RRESP, SRAM_0__AXI4__SRAM__RLAST,
   SRAM_0__AXI4__SRAM__RVALID
   );
    parameter   FBUS_AW = 32; // FBUS from Mainbus
    parameter   FBUS_DW = 64;
    parameter   FBUS_IW = 10;
    parameter   FBUS_UW = 10;
    parameter   CBUS_AW = 32; // FBUS from CBUS
    parameter   CBUS_DW = 64;
    parameter   CBUS_IW = 1;
    parameter   CBUS_UW = 0;
    parameter   FRONT_AW = 32; // To CPU Frontport
    parameter   FRONT_DW = 64;
    parameter   FRONT_IW = 11;
    parameter   FRONT_UW = 0;
    parameter   ROM_AW = 32; // ROM
    parameter   ROM_DW = 64;
    parameter   ROM_IW = 11;
    parameter   ROM_UW = 0;
    parameter   SRAM_AW = 32; // SRAM
    parameter   SRAM_DW = 64;
    parameter   SRAM_IW = 11;
    parameter   SRAM_UW = 0;

    localparam  FBUS_SW = FBUS_DW/8;
    localparam  CBUS_SW = CBUS_DW/8;
    localparam  FRONT_SW = FRONT_DW/8;
    localparam  ROM_SW = ROM_DW/8;
    localparam  SRAM_SW = SRAM_DW/8;

    input           CLK__RX;
    input           RSTN__RX;
    input           CLK__FBUS;
    input           RSTN__FBUS;
    input           CLK__SRAM;
    input           RSTN__SRAM;
    input           CLK__ROM;
    input           RSTN__ROM;

    // From Mainbus
    output              AXI4__RX__AWREADY;
    input               AXI4__RX__AWVALID;
    input [FBUS_IW-1:0]   AXI4__RX__AWID;
    input [FBUS_AW-1:0]   AXI4__RX__AWADDR;
    input [7:0]         AXI4__RX__AWLEN;
    input [2:0]         AXI4__RX__AWSIZE;
    input [1:0]         AXI4__RX__AWBURST;
    input [0:0]         AXI4__RX__AWLOCK;
    input [3:0]         AXI4__RX__AWCACHE;
    input [2:0]         AXI4__RX__AWPROT;
    input [FBUS_UW-1:0]   AXI4__RX__AWUSER;
    output              AXI4__RX__WREADY;
    input               AXI4__RX__WVALID;
    input [FBUS_DW-1:0]   AXI4__RX__WDATA;
    input [FBUS_SW-1:0]   AXI4__RX__WSTRB;
    input               AXI4__RX__WLAST;
    input               AXI4__RX__BREADY;
    output              AXI4__RX__BVALID;
    output  [FBUS_IW-1:0] AXI4__RX__BID;
    output  [1:0]       AXI4__RX__BRESP;
    output              AXI4__RX__ARREADY;
    input               AXI4__RX__ARVALID;
    input [FBUS_IW-1:0]   AXI4__RX__ARID;
    input [FBUS_AW-1:0]   AXI4__RX__ARADDR;
    input [7:0]         AXI4__RX__ARLEN;
    input [2:0]         AXI4__RX__ARSIZE;
    input [1:0]         AXI4__RX__ARBURST;
    input [0:0]         AXI4__RX__ARLOCK;
    input [3:0]         AXI4__RX__ARCACHE;
    input [2:0]         AXI4__RX__ARPROT;
    input [FBUS_UW-1:0]   AXI4__RX__ARUSER;
    input               AXI4__RX__RREADY;
    output              AXI4__RX__RVALID;
    output  [FBUS_IW-1:0] AXI4__RX__RID;
    output  [FBUS_DW-1:0] AXI4__RX__RDATA;
    output  [1:0]       AXI4__RX__RRESP;
    output              AXI4__RX__RLAST;

    // From CBUS
    output              AXI4__CBUS__CB2FB__AWREADY;
    input               AXI4__CBUS__CB2FB__AWVALID;
    input [CBUS_IW-1:0]   AXI4__CBUS__CB2FB__AWID;
    input [CBUS_AW-1:0]   AXI4__CBUS__CB2FB__AWADDR;
    input [7:0]         AXI4__CBUS__CB2FB__AWLEN;
    input [2:0]         AXI4__CBUS__CB2FB__AWSIZE;
    input [1:0]         AXI4__CBUS__CB2FB__AWBURST;
    input [0:0]         AXI4__CBUS__CB2FB__AWLOCK;
    input [3:0]         AXI4__CBUS__CB2FB__AWCACHE;
    input [2:0]         AXI4__CBUS__CB2FB__AWPROT;
    output              AXI4__CBUS__CB2FB__WREADY;
    input               AXI4__CBUS__CB2FB__WVALID;
    input [CBUS_DW-1:0]   AXI4__CBUS__CB2FB__WDATA;
    input [CBUS_SW-1:0]   AXI4__CBUS__CB2FB__WSTRB;
    input               AXI4__CBUS__CB2FB__WLAST;
    input               AXI4__CBUS__CB2FB__BREADY;
    output              AXI4__CBUS__CB2FB__BVALID;
    output  [CBUS_IW-1:0] AXI4__CBUS__CB2FB__BID;
    output  [1:0]       AXI4__CBUS__CB2FB__BRESP;
    output              AXI4__CBUS__CB2FB__ARREADY;
    input               AXI4__CBUS__CB2FB__ARVALID;
    input [CBUS_IW-1:0]   AXI4__CBUS__CB2FB__ARID;
    input [CBUS_AW-1:0]   AXI4__CBUS__CB2FB__ARADDR;
    input [7:0]         AXI4__CBUS__CB2FB__ARLEN;
    input [2:0]         AXI4__CBUS__CB2FB__ARSIZE;
    input [1:0]         AXI4__CBUS__CB2FB__ARBURST;
    input [0:0]         AXI4__CBUS__CB2FB__ARLOCK;
    input [3:0]         AXI4__CBUS__CB2FB__ARCACHE;
    input [2:0]         AXI4__CBUS__CB2FB__ARPROT;
    input               AXI4__CBUS__CB2FB__RREADY;
    output              AXI4__CBUS__CB2FB__RVALID;
    output  [CBUS_IW-1:0] AXI4__CBUS__CB2FB__RID;
    output  [CBUS_DW-1:0] AXI4__CBUS__CB2FB__RDATA;
    output  [1:0]       AXI4__CBUS__CB2FB__RRESP;
    output              AXI4__CBUS__CB2FB__RLAST;

    // To CPU
    output  [FRONT_IW-1:0] CPU_0__AXI4__CPU__FRONTPORT__AWID;
    output  [FRONT_AW-1:0] CPU_0__AXI4__CPU__FRONTPORT__AWADDR;
    output  [7:0]       CPU_0__AXI4__CPU__FRONTPORT__AWLEN;
    output  [2:0]       CPU_0__AXI4__CPU__FRONTPORT__AWSIZE;
    output  [1:0]       CPU_0__AXI4__CPU__FRONTPORT__AWBURST;
    output  [0:0]       CPU_0__AXI4__CPU__FRONTPORT__AWLOCK;
    output  [3:0]       CPU_0__AXI4__CPU__FRONTPORT__AWCACHE;
    output  [2:0]       CPU_0__AXI4__CPU__FRONTPORT__AWPROT;
    output              CPU_0__AXI4__CPU__FRONTPORT__AWVALID;
    input               CPU_0__AXI4__CPU__FRONTPORT__AWREADY;
    output  [FRONT_DW-1:0] CPU_0__AXI4__CPU__FRONTPORT__WDATA;
    output  [FRONT_SW-1:0] CPU_0__AXI4__CPU__FRONTPORT__WSTRB;
    output              CPU_0__AXI4__CPU__FRONTPORT__WLAST;
    output              CPU_0__AXI4__CPU__FRONTPORT__WVALID;
    input               CPU_0__AXI4__CPU__FRONTPORT__WREADY;
    input   [FRONT_IW-1:0] CPU_0__AXI4__CPU__FRONTPORT__BID;
    input               CPU_0__AXI4__CPU__FRONTPORT__BVALID;
    input   [1:0]       CPU_0__AXI4__CPU__FRONTPORT__BRESP;
    output              CPU_0__AXI4__CPU__FRONTPORT__BREADY;
    output  [FRONT_IW-1:0] CPU_0__AXI4__CPU__FRONTPORT__ARID;
    output  [FRONT_AW-1:0] CPU_0__AXI4__CPU__FRONTPORT__ARADDR;
    output  [7:0]       CPU_0__AXI4__CPU__FRONTPORT__ARLEN;
    output  [2:0]       CPU_0__AXI4__CPU__FRONTPORT__ARSIZE;
    output  [1:0]       CPU_0__AXI4__CPU__FRONTPORT__ARBURST;
    output  [0:0]       CPU_0__AXI4__CPU__FRONTPORT__ARLOCK;
    output  [3:0]       CPU_0__AXI4__CPU__FRONTPORT__ARCACHE;
    output  [2:0]       CPU_0__AXI4__CPU__FRONTPORT__ARPROT;
    output              CPU_0__AXI4__CPU__FRONTPORT__ARVALID;
    input               CPU_0__AXI4__CPU__FRONTPORT__ARREADY;
    input   [FRONT_IW-1:0] CPU_0__AXI4__CPU__FRONTPORT__RID;
    input   [FRONT_DW-1:0] CPU_0__AXI4__CPU__FRONTPORT__RDATA;
    input   [1:0]       CPU_0__AXI4__CPU__FRONTPORT__RRESP;
    input               CPU_0__AXI4__CPU__FRONTPORT__RLAST;
    input               CPU_0__AXI4__CPU__FRONTPORT__RVALID;
    output              CPU_0__AXI4__CPU__FRONTPORT__RREADY;

    // AXI4 Master to access ROM
    output  [ROM_IW-1:0] ROM_0__AXI4__ROM__AWID;
    output  [ROM_AW-1:0] ROM_0__AXI4__ROM__AWADDR;
    output  [7:0]       ROM_0__AXI4__ROM__AWLEN;
    output  [2:0]       ROM_0__AXI4__ROM__AWSIZE;
    output  [1:0]       ROM_0__AXI4__ROM__AWBURST;
    output  [0:0]       ROM_0__AXI4__ROM__AWLOCK;
    output  [3:0]       ROM_0__AXI4__ROM__AWCACHE;
    output  [2:0]       ROM_0__AXI4__ROM__AWPROT;
    output              ROM_0__AXI4__ROM__AWVALID;
    input               ROM_0__AXI4__ROM__AWREADY;
    output  [ROM_DW-1:0] ROM_0__AXI4__ROM__WDATA;
    output  [ROM_SW-1:0] ROM_0__AXI4__ROM__WSTRB;
    output              ROM_0__AXI4__ROM__WLAST;
    output              ROM_0__AXI4__ROM__WVALID;
    input               ROM_0__AXI4__ROM__WREADY;
    input   [ROM_IW-1:0] ROM_0__AXI4__ROM__BID;
    input               ROM_0__AXI4__ROM__BVALID;
    input   [1:0]       ROM_0__AXI4__ROM__BRESP;
    output              ROM_0__AXI4__ROM__BREADY;
    output  [ROM_IW-1:0] ROM_0__AXI4__ROM__ARID;
    output  [ROM_AW-1:0] ROM_0__AXI4__ROM__ARADDR;
    output  [7:0]       ROM_0__AXI4__ROM__ARLEN;
    output  [2:0]       ROM_0__AXI4__ROM__ARSIZE;
    output  [1:0]       ROM_0__AXI4__ROM__ARBURST;
    output  [0:0]       ROM_0__AXI4__ROM__ARLOCK;
    output  [3:0]       ROM_0__AXI4__ROM__ARCACHE;
    output  [2:0]       ROM_0__AXI4__ROM__ARPROT;
    output              ROM_0__AXI4__ROM__ARVALID;
    input               ROM_0__AXI4__ROM__ARREADY;
    input   [ROM_IW-1:0] ROM_0__AXI4__ROM__RID;
    input   [ROM_DW-1:0] ROM_0__AXI4__ROM__RDATA;
    input   [1:0]       ROM_0__AXI4__ROM__RRESP;
    input               ROM_0__AXI4__ROM__RLAST;
    input               ROM_0__AXI4__ROM__RVALID;
    output              ROM_0__AXI4__ROM__RREADY;

    // AXI4 Master to access SRAM
    output  [SRAM_IW-1:0] SRAM_0__AXI4__SRAM__AWID;
    output  [SRAM_AW-1:0] SRAM_0__AXI4__SRAM__AWADDR;
    output  [7:0]       SRAM_0__AXI4__SRAM__AWLEN;
    output  [2:0]       SRAM_0__AXI4__SRAM__AWSIZE;
    output  [1:0]       SRAM_0__AXI4__SRAM__AWBURST;
    output  [0:0]       SRAM_0__AXI4__SRAM__AWLOCK;
    output  [3:0]       SRAM_0__AXI4__SRAM__AWCACHE;
    output  [2:0]       SRAM_0__AXI4__SRAM__AWPROT;
    output              SRAM_0__AXI4__SRAM__AWVALID;
    input               SRAM_0__AXI4__SRAM__AWREADY;
    output  [SRAM_DW-1:0] SRAM_0__AXI4__SRAM__WDATA;
    output  [SRAM_SW-1:0] SRAM_0__AXI4__SRAM__WSTRB;
    output              SRAM_0__AXI4__SRAM__WLAST;
    output              SRAM_0__AXI4__SRAM__WVALID;
    input               SRAM_0__AXI4__SRAM__WREADY;
    input   [SRAM_IW-1:0] SRAM_0__AXI4__SRAM__BID;
    input               SRAM_0__AXI4__SRAM__BVALID;
    input   [1:0]       SRAM_0__AXI4__SRAM__BRESP;
    output              SRAM_0__AXI4__SRAM__BREADY;
    output  [SRAM_IW-1:0] SRAM_0__AXI4__SRAM__ARID;
    output  [SRAM_AW-1:0] SRAM_0__AXI4__SRAM__ARADDR;
    output  [7:0]       SRAM_0__AXI4__SRAM__ARLEN;
    output  [2:0]       SRAM_0__AXI4__SRAM__ARSIZE;
    output  [1:0]       SRAM_0__AXI4__SRAM__ARBURST;
    output  [0:0]       SRAM_0__AXI4__SRAM__ARLOCK;
    output  [3:0]       SRAM_0__AXI4__SRAM__ARCACHE;
    output  [2:0]       SRAM_0__AXI4__SRAM__ARPROT;
    output              SRAM_0__AXI4__SRAM__ARVALID;
    input               SRAM_0__AXI4__SRAM__ARREADY;
    input   [SRAM_IW-1:0] SRAM_0__AXI4__SRAM__RID;
    input   [SRAM_DW-1:0] SRAM_0__AXI4__SRAM__RDATA;
    input   [1:0]       SRAM_0__AXI4__SRAM__RRESP;
    input               SRAM_0__AXI4__SRAM__RLAST;
    input               SRAM_0__AXI4__SRAM__RVALID;
    output              SRAM_0__AXI4__SRAM__RREADY;

`include "./axi_2x3_cpu_fbus/axi_2x3_cpu_fbus_DW_axi_all_includes.vh"
`include "./axi2axi_cpu_sram/axi2axi_cpu_sram_DW_axi_x2x_all_includes.vh"
// Wire & Reg
// {{{
    wor [31:0] NC;
    /*AUTOWIRE*/
    // Beginning of automatic wires (for undeclared instantiated-module outputs)
    wire [ROM_AW-1:0]	AXI3__FBUS__TO_ROM__ARADDR;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [1:0]		AXI3__FBUS__TO_ROM__ARBURST;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [3:0]		AXI3__FBUS__TO_ROM__ARCACHE;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [ROM_IW-1:0]	AXI3__FBUS__TO_ROM__ARID;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [7:0]		AXI3__FBUS__TO_ROM__ARLEN;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [1:0]		AXI3__FBUS__TO_ROM__ARLOCK;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [2:0]		AXI3__FBUS__TO_ROM__ARPROT;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire		AXI3__FBUS__TO_ROM__ARREADY;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [2:0]		AXI3__FBUS__TO_ROM__ARSIZE;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire		AXI3__FBUS__TO_ROM__ARVALID;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [ROM_AW-1:0]	AXI3__FBUS__TO_ROM__AWADDR;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [1:0]		AXI3__FBUS__TO_ROM__AWBURST;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [3:0]		AXI3__FBUS__TO_ROM__AWCACHE;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [ROM_IW-1:0]	AXI3__FBUS__TO_ROM__AWID;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [7:0]		AXI3__FBUS__TO_ROM__AWLEN;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [1:0]		AXI3__FBUS__TO_ROM__AWLOCK;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [2:0]		AXI3__FBUS__TO_ROM__AWPROT;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire		AXI3__FBUS__TO_ROM__AWREADY;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [2:0]		AXI3__FBUS__TO_ROM__AWSIZE;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire		AXI3__FBUS__TO_ROM__AWVALID;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [`axi2axi_cpu_sram_X2X_MP_IDW-1:0] AXI3__FBUS__TO_ROM__BID;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__FBUS__TO_ROM__BREADY;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [`axi2axi_cpu_sram_X2X_BRW-1:0] AXI3__FBUS__TO_ROM__BRESP;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__FBUS__TO_ROM__BVALID;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_MP_DW-1:0] AXI3__FBUS__TO_ROM__RDATA;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_MP_IDW-1:0] AXI3__FBUS__TO_ROM__RID;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__FBUS__TO_ROM__RLAST;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__FBUS__TO_ROM__RREADY;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [`axi2axi_cpu_sram_X2X_RRW-1:0] AXI3__FBUS__TO_ROM__RRESP;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__FBUS__TO_ROM__RVALID;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [ROM_DW-1:0]	AXI3__FBUS__TO_ROM__WDATA;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [ROM_IW-1:0]	AXI3__FBUS__TO_ROM__WID;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire		AXI3__FBUS__TO_ROM__WLAST;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire		AXI3__FBUS__TO_ROM__WREADY;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [ROM_SW-1:0]	AXI3__FBUS__TO_ROM__WSTRB;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire		AXI3__FBUS__TO_ROM__WVALID;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [SRAM_AW-1:0]	AXI3__FBUS__TO_SRAM__ARADDR;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [1:0]		AXI3__FBUS__TO_SRAM__ARBURST;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [3:0]		AXI3__FBUS__TO_SRAM__ARCACHE;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [SRAM_IW-1:0]	AXI3__FBUS__TO_SRAM__ARID;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [7:0]		AXI3__FBUS__TO_SRAM__ARLEN;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [1:0]		AXI3__FBUS__TO_SRAM__ARLOCK;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [2:0]		AXI3__FBUS__TO_SRAM__ARPROT;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire		AXI3__FBUS__TO_SRAM__ARREADY;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [2:0]		AXI3__FBUS__TO_SRAM__ARSIZE;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire		AXI3__FBUS__TO_SRAM__ARVALID;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [SRAM_AW-1:0]	AXI3__FBUS__TO_SRAM__AWADDR;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [1:0]		AXI3__FBUS__TO_SRAM__AWBURST;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [3:0]		AXI3__FBUS__TO_SRAM__AWCACHE;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [SRAM_IW-1:0]	AXI3__FBUS__TO_SRAM__AWID;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [7:0]		AXI3__FBUS__TO_SRAM__AWLEN;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [1:0]		AXI3__FBUS__TO_SRAM__AWLOCK;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [2:0]		AXI3__FBUS__TO_SRAM__AWPROT;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire		AXI3__FBUS__TO_SRAM__AWREADY;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [2:0]		AXI3__FBUS__TO_SRAM__AWSIZE;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire		AXI3__FBUS__TO_SRAM__AWVALID;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [`axi2axi_cpu_sram_X2X_MP_IDW-1:0] AXI3__FBUS__TO_SRAM__BID;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__FBUS__TO_SRAM__BREADY;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [`axi2axi_cpu_sram_X2X_BRW-1:0] AXI3__FBUS__TO_SRAM__BRESP;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__FBUS__TO_SRAM__BVALID;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_MP_DW-1:0] AXI3__FBUS__TO_SRAM__RDATA;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_MP_IDW-1:0] AXI3__FBUS__TO_SRAM__RID;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__FBUS__TO_SRAM__RLAST;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__FBUS__TO_SRAM__RREADY;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [`axi2axi_cpu_sram_X2X_RRW-1:0] AXI3__FBUS__TO_SRAM__RRESP;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__FBUS__TO_SRAM__RVALID;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [SRAM_DW-1:0]	AXI3__FBUS__TO_SRAM__WDATA;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [SRAM_IW-1:0]	AXI3__FBUS__TO_SRAM__WID;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire		AXI3__FBUS__TO_SRAM__WLAST;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire		AXI3__FBUS__TO_SRAM__WREADY;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [SRAM_SW-1:0]	AXI3__FBUS__TO_SRAM__WSTRB;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire		AXI3__FBUS__TO_SRAM__WVALID;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [`axi2axi_cpu_sram_X2X_SP_AW-1:0] AXI3__ROM__ARADDR;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_BTW-1:0] AXI3__ROM__ARBURST;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_CTW-1:0] AXI3__ROM__ARCACHE;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_SP_IDW-1:0] AXI3__ROM__ARID;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_SP_BLW-1:0] AXI3__ROM__ARLEN;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_LTW-1:0] AXI3__ROM__ARLOCK;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_PTW-1:0] AXI3__ROM__ARPROT;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__ROM__ARREADY;	// From AXI3TO4_ROM_0 of axi3to4.v
    wire [`axi2axi_cpu_sram_X2X_BSW-1:0] AXI3__ROM__ARSIZE;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__ROM__ARVALID;	// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_SP_AW-1:0] AXI3__ROM__AWADDR;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_BTW-1:0] AXI3__ROM__AWBURST;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_CTW-1:0] AXI3__ROM__AWCACHE;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_SP_IDW-1:0] AXI3__ROM__AWID;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_SP_BLW-1:0] AXI3__ROM__AWLEN;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_LTW-1:0] AXI3__ROM__AWLOCK;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_PTW-1:0] AXI3__ROM__AWPROT;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__ROM__AWREADY;	// From AXI3TO4_ROM_0 of axi3to4.v
    wire [`axi2axi_cpu_sram_X2X_BSW-1:0] AXI3__ROM__AWSIZE;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__ROM__AWVALID;	// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [ROM_IW-1:0]	AXI3__ROM__BID;		// From AXI3TO4_ROM_0 of axi3to4.v
    wire		AXI3__ROM__BREADY;	// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [1:0]		AXI3__ROM__BRESP;	// From AXI3TO4_ROM_0 of axi3to4.v
    wire		AXI3__ROM__BVALID;	// From AXI3TO4_ROM_0 of axi3to4.v
    wire [ROM_DW-1:0]	AXI3__ROM__RDATA;	// From AXI3TO4_ROM_0 of axi3to4.v
    wire [ROM_IW-1:0]	AXI3__ROM__RID;		// From AXI3TO4_ROM_0 of axi3to4.v
    wire		AXI3__ROM__RLAST;	// From AXI3TO4_ROM_0 of axi3to4.v
    wire		AXI3__ROM__RREADY;	// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [1:0]		AXI3__ROM__RRESP;	// From AXI3TO4_ROM_0 of axi3to4.v
    wire		AXI3__ROM__RVALID;	// From AXI3TO4_ROM_0 of axi3to4.v
    wire [`axi2axi_cpu_sram_X2X_SP_DW-1:0] AXI3__ROM__WDATA;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_SP_IDW-1:0] AXI3__ROM__WID;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__ROM__WLAST;	// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__ROM__WREADY;	// From AXI3TO4_ROM_0 of axi3to4.v
    wire [`axi2axi_cpu_sram_X2X_SP_SW-1:0] AXI3__ROM__WSTRB;// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__ROM__WVALID;	// From AXI3X2X_ROM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_SP_AW-1:0] AXI3__SRAM__ARADDR;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_BTW-1:0] AXI3__SRAM__ARBURST;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_CTW-1:0] AXI3__SRAM__ARCACHE;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_SP_IDW-1:0] AXI3__SRAM__ARID;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_SP_BLW-1:0] AXI3__SRAM__ARLEN;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_LTW-1:0] AXI3__SRAM__ARLOCK;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_PTW-1:0] AXI3__SRAM__ARPROT;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__SRAM__ARREADY;	// From AXI3TO4_SRAM_0 of axi3to4.v
    wire [`axi2axi_cpu_sram_X2X_BSW-1:0] AXI3__SRAM__ARSIZE;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__SRAM__ARVALID;	// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_SP_AW-1:0] AXI3__SRAM__AWADDR;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_BTW-1:0] AXI3__SRAM__AWBURST;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_CTW-1:0] AXI3__SRAM__AWCACHE;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_SP_IDW-1:0] AXI3__SRAM__AWID;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_SP_BLW-1:0] AXI3__SRAM__AWLEN;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_LTW-1:0] AXI3__SRAM__AWLOCK;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_PTW-1:0] AXI3__SRAM__AWPROT;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__SRAM__AWREADY;	// From AXI3TO4_SRAM_0 of axi3to4.v
    wire [`axi2axi_cpu_sram_X2X_BSW-1:0] AXI3__SRAM__AWSIZE;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__SRAM__AWVALID;	// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [SRAM_IW-1:0]	AXI3__SRAM__BID;	// From AXI3TO4_SRAM_0 of axi3to4.v
    wire		AXI3__SRAM__BREADY;	// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [1:0]		AXI3__SRAM__BRESP;	// From AXI3TO4_SRAM_0 of axi3to4.v
    wire		AXI3__SRAM__BVALID;	// From AXI3TO4_SRAM_0 of axi3to4.v
    wire [SRAM_DW-1:0]	AXI3__SRAM__RDATA;	// From AXI3TO4_SRAM_0 of axi3to4.v
    wire [SRAM_IW-1:0]	AXI3__SRAM__RID;	// From AXI3TO4_SRAM_0 of axi3to4.v
    wire		AXI3__SRAM__RLAST;	// From AXI3TO4_SRAM_0 of axi3to4.v
    wire		AXI3__SRAM__RREADY;	// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [1:0]		AXI3__SRAM__RRESP;	// From AXI3TO4_SRAM_0 of axi3to4.v
    wire		AXI3__SRAM__RVALID;	// From AXI3TO4_SRAM_0 of axi3to4.v
    wire [`axi2axi_cpu_sram_X2X_SP_DW-1:0] AXI3__SRAM__WDATA;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi2axi_cpu_sram_X2X_SP_IDW-1:0] AXI3__SRAM__WID;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__SRAM__WLAST;	// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__SRAM__WREADY;	// From AXI3TO4_SRAM_0 of axi3to4.v
    wire [`axi2axi_cpu_sram_X2X_SP_SW-1:0] AXI3__SRAM__WSTRB;// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire		AXI3__SRAM__WVALID;	// From AXI3X2X_SRAM_0 of axi2axi_cpu_sram_DW_axi_x2x.v
    wire [`axi_2x3_cpu_fbus_AXI_AW-1:0] AXI4__FBUS__TO_ROM__ARADDR;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_BTW-1:0] AXI4__FBUS__TO_ROM__ARBURST;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_CTW-1:0] AXI4__FBUS__TO_ROM__ARCACHE;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_SIDW-1:0] AXI4__FBUS__TO_ROM__ARID;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_BLW-1:0] AXI4__FBUS__TO_ROM__ARLEN;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_LTW-1:0] AXI4__FBUS__TO_ROM__ARLOCK;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_PTW-1:0] AXI4__FBUS__TO_ROM__ARPROT;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire		AXI4__FBUS__TO_ROM__ARREADY;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [`axi_2x3_cpu_fbus_AXI_BSW-1:0] AXI4__FBUS__TO_ROM__ARSIZE;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire		AXI4__FBUS__TO_ROM__ARVALID;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_AW-1:0] AXI4__FBUS__TO_ROM__AWADDR;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_BTW-1:0] AXI4__FBUS__TO_ROM__AWBURST;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_CTW-1:0] AXI4__FBUS__TO_ROM__AWCACHE;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_SIDW-1:0] AXI4__FBUS__TO_ROM__AWID;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_BLW-1:0] AXI4__FBUS__TO_ROM__AWLEN;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_LTW-1:0] AXI4__FBUS__TO_ROM__AWLOCK;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_PTW-1:0] AXI4__FBUS__TO_ROM__AWPROT;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire		AXI4__FBUS__TO_ROM__AWREADY;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [`axi_2x3_cpu_fbus_AXI_BSW-1:0] AXI4__FBUS__TO_ROM__AWSIZE;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire		AXI4__FBUS__TO_ROM__AWVALID;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [ROM_IW-1:0]	AXI4__FBUS__TO_ROM__BID;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire		AXI4__FBUS__TO_ROM__BREADY;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [1:0]		AXI4__FBUS__TO_ROM__BRESP;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire		AXI4__FBUS__TO_ROM__BVALID;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [ROM_DW-1:0]	AXI4__FBUS__TO_ROM__RDATA;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [ROM_IW-1:0]	AXI4__FBUS__TO_ROM__RID;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire		AXI4__FBUS__TO_ROM__RLAST;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire		AXI4__FBUS__TO_ROM__RREADY;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [1:0]		AXI4__FBUS__TO_ROM__RRESP;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire		AXI4__FBUS__TO_ROM__RVALID;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [`axi_2x3_cpu_fbus_AXI_DW-1:0] AXI4__FBUS__TO_ROM__WDATA;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire		AXI4__FBUS__TO_ROM__WLAST;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire		AXI4__FBUS__TO_ROM__WREADY;// From AXI4TO3_FBUS_ROM_0 of axi4to3.v
    wire [`axi_2x3_cpu_fbus_AXI_SW-1:0] AXI4__FBUS__TO_ROM__WSTRB;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire		AXI4__FBUS__TO_ROM__WVALID;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_AW-1:0] AXI4__FBUS__TO_SRAM__ARADDR;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_BTW-1:0] AXI4__FBUS__TO_SRAM__ARBURST;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_CTW-1:0] AXI4__FBUS__TO_SRAM__ARCACHE;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_SIDW-1:0] AXI4__FBUS__TO_SRAM__ARID;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_BLW-1:0] AXI4__FBUS__TO_SRAM__ARLEN;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_LTW-1:0] AXI4__FBUS__TO_SRAM__ARLOCK;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_PTW-1:0] AXI4__FBUS__TO_SRAM__ARPROT;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire		AXI4__FBUS__TO_SRAM__ARREADY;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [`axi_2x3_cpu_fbus_AXI_BSW-1:0] AXI4__FBUS__TO_SRAM__ARSIZE;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire		AXI4__FBUS__TO_SRAM__ARVALID;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_AW-1:0] AXI4__FBUS__TO_SRAM__AWADDR;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_BTW-1:0] AXI4__FBUS__TO_SRAM__AWBURST;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_CTW-1:0] AXI4__FBUS__TO_SRAM__AWCACHE;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_SIDW-1:0] AXI4__FBUS__TO_SRAM__AWID;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_BLW-1:0] AXI4__FBUS__TO_SRAM__AWLEN;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_LTW-1:0] AXI4__FBUS__TO_SRAM__AWLOCK;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [`axi_2x3_cpu_fbus_AXI_PTW-1:0] AXI4__FBUS__TO_SRAM__AWPROT;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire		AXI4__FBUS__TO_SRAM__AWREADY;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [`axi_2x3_cpu_fbus_AXI_BSW-1:0] AXI4__FBUS__TO_SRAM__AWSIZE;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire		AXI4__FBUS__TO_SRAM__AWVALID;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [SRAM_IW-1:0]	AXI4__FBUS__TO_SRAM__BID;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire		AXI4__FBUS__TO_SRAM__BREADY;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [1:0]		AXI4__FBUS__TO_SRAM__BRESP;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire		AXI4__FBUS__TO_SRAM__BVALID;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [SRAM_DW-1:0]	AXI4__FBUS__TO_SRAM__RDATA;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [SRAM_IW-1:0]	AXI4__FBUS__TO_SRAM__RID;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire		AXI4__FBUS__TO_SRAM__RLAST;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire		AXI4__FBUS__TO_SRAM__RREADY;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire [1:0]		AXI4__FBUS__TO_SRAM__RRESP;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire		AXI4__FBUS__TO_SRAM__RVALID;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [`axi_2x3_cpu_fbus_AXI_DW-1:0] AXI4__FBUS__TO_SRAM__WDATA;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire		AXI4__FBUS__TO_SRAM__WLAST;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire		AXI4__FBUS__TO_SRAM__WREADY;// From AXI4TO3_FBUS_SRAM_0 of axi4to3.v
    wire [`axi_2x3_cpu_fbus_AXI_SW-1:0] AXI4__FBUS__TO_SRAM__WSTRB;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    wire		AXI4__FBUS__TO_SRAM__WVALID;// From AXI_XBAR_FBUS_CPU_0 of axi_2x3_cpu_fbus_DW_axi.v
    // End of automatics
    /*AUTOREG*/
// }}}

    /* axi_2x3_cpu_fbus_DW_axi AUTO_TEMPLATE (
        .aclk               (CLK__FBUS),
        .aresetn            (RSTN__FBUS),
        .\(.*\)_m1          (AXI4__RX__@"(upcase \\"\1\\")"[]),
        .arid_m2            ({9'h0,AXI4__CBUS__CB2FB__ARID}),
        .awid_m2            ({9'h0,AXI4__CBUS__CB2FB__AWID}),
        .rid_m2             ({NC[9:1],AXI4__CBUS__CB2FB__RID[0]}),
        .bid_m2             ({NC[9:1],AXI4__CBUS__CB2FB__BID[0]}),
        .\(.*\)_m2          (AXI4__CBUS__CB2FB__@"(upcase \\"\1\\")"[]),
        .\(.*\)_s1          (CPU_0__AXI4__CPU__FRONTPORT__@"(upcase \\"\1\\")"[]),
        .\(.*\)_s2          (AXI4__FBUS__TO_ROM__@"(upcase \\"\1\\")"[]),
        .\(.*\)_s3          (AXI4__FBUS__TO_SRAM__@"(upcase \\"\1\\")"[]),
        .dbg_.*             (),
    );*/
    axi_2x3_cpu_fbus_DW_axi AXI_XBAR_FBUS_CPU_0 (/*AUTOINST*/
						 // Outputs
						 .awready_m1		(AXI4__RX__AWREADY), // Templated
						 .wready_m1		(AXI4__RX__WREADY), // Templated
						 .bvalid_m1		(AXI4__RX__BVALID), // Templated
						 .bid_m1		(AXI4__RX__BID[`axi_2x3_cpu_fbus_AXI_IDW_M1-1:0]), // Templated
						 .bresp_m1		(AXI4__RX__BRESP[`axi_2x3_cpu_fbus_AXI_BRW-1:0]), // Templated
						 .arready_m1		(AXI4__RX__ARREADY), // Templated
						 .rvalid_m1		(AXI4__RX__RVALID), // Templated
						 .rid_m1		(AXI4__RX__RID[`axi_2x3_cpu_fbus_AXI_IDW_M1-1:0]), // Templated
						 .rdata_m1		(AXI4__RX__RDATA[`axi_2x3_cpu_fbus_AXI_DW-1:0]), // Templated
						 .rlast_m1		(AXI4__RX__RLAST), // Templated
						 .rresp_m1		(AXI4__RX__RRESP[`axi_2x3_cpu_fbus_AXI_RRW-1:0]), // Templated
						 .awready_m2		(AXI4__CBUS__CB2FB__AWREADY), // Templated
						 .wready_m2		(AXI4__CBUS__CB2FB__WREADY), // Templated
						 .bvalid_m2		(AXI4__CBUS__CB2FB__BVALID), // Templated
						 .bid_m2		({NC[9:1],AXI4__CBUS__CB2FB__BID[0]}), // Templated
						 .bresp_m2		(AXI4__CBUS__CB2FB__BRESP[`axi_2x3_cpu_fbus_AXI_BRW-1:0]), // Templated
						 .arready_m2		(AXI4__CBUS__CB2FB__ARREADY), // Templated
						 .rvalid_m2		(AXI4__CBUS__CB2FB__RVALID), // Templated
						 .rid_m2		({NC[9:1],AXI4__CBUS__CB2FB__RID[0]}), // Templated
						 .rdata_m2		(AXI4__CBUS__CB2FB__RDATA[`axi_2x3_cpu_fbus_AXI_DW-1:0]), // Templated
						 .rlast_m2		(AXI4__CBUS__CB2FB__RLAST), // Templated
						 .rresp_m2		(AXI4__CBUS__CB2FB__RRESP[`axi_2x3_cpu_fbus_AXI_RRW-1:0]), // Templated
						 .awvalid_s1		(CPU_0__AXI4__CPU__FRONTPORT__AWVALID), // Templated
						 .awaddr_s1		(CPU_0__AXI4__CPU__FRONTPORT__AWADDR[`axi_2x3_cpu_fbus_AXI_AW-1:0]), // Templated
						 .awid_s1		(CPU_0__AXI4__CPU__FRONTPORT__AWID[`axi_2x3_cpu_fbus_AXI_SIDW-1:0]), // Templated
						 .awlen_s1		(CPU_0__AXI4__CPU__FRONTPORT__AWLEN[`axi_2x3_cpu_fbus_AXI_BLW-1:0]), // Templated
						 .awsize_s1		(CPU_0__AXI4__CPU__FRONTPORT__AWSIZE[`axi_2x3_cpu_fbus_AXI_BSW-1:0]), // Templated
						 .awburst_s1		(CPU_0__AXI4__CPU__FRONTPORT__AWBURST[`axi_2x3_cpu_fbus_AXI_BTW-1:0]), // Templated
						 .awlock_s1		(CPU_0__AXI4__CPU__FRONTPORT__AWLOCK[`axi_2x3_cpu_fbus_AXI_LTW-1:0]), // Templated
						 .awcache_s1		(CPU_0__AXI4__CPU__FRONTPORT__AWCACHE[`axi_2x3_cpu_fbus_AXI_CTW-1:0]), // Templated
						 .awprot_s1		(CPU_0__AXI4__CPU__FRONTPORT__AWPROT[`axi_2x3_cpu_fbus_AXI_PTW-1:0]), // Templated
						 .wvalid_s1		(CPU_0__AXI4__CPU__FRONTPORT__WVALID), // Templated
						 .wdata_s1		(CPU_0__AXI4__CPU__FRONTPORT__WDATA[`axi_2x3_cpu_fbus_AXI_DW-1:0]), // Templated
						 .wstrb_s1		(CPU_0__AXI4__CPU__FRONTPORT__WSTRB[`axi_2x3_cpu_fbus_AXI_SW-1:0]), // Templated
						 .wlast_s1		(CPU_0__AXI4__CPU__FRONTPORT__WLAST), // Templated
						 .bready_s1		(CPU_0__AXI4__CPU__FRONTPORT__BREADY), // Templated
						 .arvalid_s1		(CPU_0__AXI4__CPU__FRONTPORT__ARVALID), // Templated
						 .arid_s1		(CPU_0__AXI4__CPU__FRONTPORT__ARID[`axi_2x3_cpu_fbus_AXI_SIDW-1:0]), // Templated
						 .araddr_s1		(CPU_0__AXI4__CPU__FRONTPORT__ARADDR[`axi_2x3_cpu_fbus_AXI_AW-1:0]), // Templated
						 .arlen_s1		(CPU_0__AXI4__CPU__FRONTPORT__ARLEN[`axi_2x3_cpu_fbus_AXI_BLW-1:0]), // Templated
						 .arsize_s1		(CPU_0__AXI4__CPU__FRONTPORT__ARSIZE[`axi_2x3_cpu_fbus_AXI_BSW-1:0]), // Templated
						 .arburst_s1		(CPU_0__AXI4__CPU__FRONTPORT__ARBURST[`axi_2x3_cpu_fbus_AXI_BTW-1:0]), // Templated
						 .arlock_s1		(CPU_0__AXI4__CPU__FRONTPORT__ARLOCK[`axi_2x3_cpu_fbus_AXI_LTW-1:0]), // Templated
						 .arcache_s1		(CPU_0__AXI4__CPU__FRONTPORT__ARCACHE[`axi_2x3_cpu_fbus_AXI_CTW-1:0]), // Templated
						 .arprot_s1		(CPU_0__AXI4__CPU__FRONTPORT__ARPROT[`axi_2x3_cpu_fbus_AXI_PTW-1:0]), // Templated
						 .rready_s1		(CPU_0__AXI4__CPU__FRONTPORT__RREADY), // Templated
						 .awvalid_s2		(AXI4__FBUS__TO_ROM__AWVALID), // Templated
						 .awaddr_s2		(AXI4__FBUS__TO_ROM__AWADDR[`axi_2x3_cpu_fbus_AXI_AW-1:0]), // Templated
						 .awid_s2		(AXI4__FBUS__TO_ROM__AWID[`axi_2x3_cpu_fbus_AXI_SIDW-1:0]), // Templated
						 .awlen_s2		(AXI4__FBUS__TO_ROM__AWLEN[`axi_2x3_cpu_fbus_AXI_BLW-1:0]), // Templated
						 .awsize_s2		(AXI4__FBUS__TO_ROM__AWSIZE[`axi_2x3_cpu_fbus_AXI_BSW-1:0]), // Templated
						 .awburst_s2		(AXI4__FBUS__TO_ROM__AWBURST[`axi_2x3_cpu_fbus_AXI_BTW-1:0]), // Templated
						 .awlock_s2		(AXI4__FBUS__TO_ROM__AWLOCK[`axi_2x3_cpu_fbus_AXI_LTW-1:0]), // Templated
						 .awcache_s2		(AXI4__FBUS__TO_ROM__AWCACHE[`axi_2x3_cpu_fbus_AXI_CTW-1:0]), // Templated
						 .awprot_s2		(AXI4__FBUS__TO_ROM__AWPROT[`axi_2x3_cpu_fbus_AXI_PTW-1:0]), // Templated
						 .wvalid_s2		(AXI4__FBUS__TO_ROM__WVALID), // Templated
						 .wdata_s2		(AXI4__FBUS__TO_ROM__WDATA[`axi_2x3_cpu_fbus_AXI_DW-1:0]), // Templated
						 .wstrb_s2		(AXI4__FBUS__TO_ROM__WSTRB[`axi_2x3_cpu_fbus_AXI_SW-1:0]), // Templated
						 .wlast_s2		(AXI4__FBUS__TO_ROM__WLAST), // Templated
						 .bready_s2		(AXI4__FBUS__TO_ROM__BREADY), // Templated
						 .arvalid_s2		(AXI4__FBUS__TO_ROM__ARVALID), // Templated
						 .arid_s2		(AXI4__FBUS__TO_ROM__ARID[`axi_2x3_cpu_fbus_AXI_SIDW-1:0]), // Templated
						 .araddr_s2		(AXI4__FBUS__TO_ROM__ARADDR[`axi_2x3_cpu_fbus_AXI_AW-1:0]), // Templated
						 .arlen_s2		(AXI4__FBUS__TO_ROM__ARLEN[`axi_2x3_cpu_fbus_AXI_BLW-1:0]), // Templated
						 .arsize_s2		(AXI4__FBUS__TO_ROM__ARSIZE[`axi_2x3_cpu_fbus_AXI_BSW-1:0]), // Templated
						 .arburst_s2		(AXI4__FBUS__TO_ROM__ARBURST[`axi_2x3_cpu_fbus_AXI_BTW-1:0]), // Templated
						 .arlock_s2		(AXI4__FBUS__TO_ROM__ARLOCK[`axi_2x3_cpu_fbus_AXI_LTW-1:0]), // Templated
						 .arcache_s2		(AXI4__FBUS__TO_ROM__ARCACHE[`axi_2x3_cpu_fbus_AXI_CTW-1:0]), // Templated
						 .arprot_s2		(AXI4__FBUS__TO_ROM__ARPROT[`axi_2x3_cpu_fbus_AXI_PTW-1:0]), // Templated
						 .rready_s2		(AXI4__FBUS__TO_ROM__RREADY), // Templated
						 .awvalid_s3		(AXI4__FBUS__TO_SRAM__AWVALID), // Templated
						 .awaddr_s3		(AXI4__FBUS__TO_SRAM__AWADDR[`axi_2x3_cpu_fbus_AXI_AW-1:0]), // Templated
						 .awid_s3		(AXI4__FBUS__TO_SRAM__AWID[`axi_2x3_cpu_fbus_AXI_SIDW-1:0]), // Templated
						 .awlen_s3		(AXI4__FBUS__TO_SRAM__AWLEN[`axi_2x3_cpu_fbus_AXI_BLW-1:0]), // Templated
						 .awsize_s3		(AXI4__FBUS__TO_SRAM__AWSIZE[`axi_2x3_cpu_fbus_AXI_BSW-1:0]), // Templated
						 .awburst_s3		(AXI4__FBUS__TO_SRAM__AWBURST[`axi_2x3_cpu_fbus_AXI_BTW-1:0]), // Templated
						 .awlock_s3		(AXI4__FBUS__TO_SRAM__AWLOCK[`axi_2x3_cpu_fbus_AXI_LTW-1:0]), // Templated
						 .awcache_s3		(AXI4__FBUS__TO_SRAM__AWCACHE[`axi_2x3_cpu_fbus_AXI_CTW-1:0]), // Templated
						 .awprot_s3		(AXI4__FBUS__TO_SRAM__AWPROT[`axi_2x3_cpu_fbus_AXI_PTW-1:0]), // Templated
						 .wvalid_s3		(AXI4__FBUS__TO_SRAM__WVALID), // Templated
						 .wdata_s3		(AXI4__FBUS__TO_SRAM__WDATA[`axi_2x3_cpu_fbus_AXI_DW-1:0]), // Templated
						 .wstrb_s3		(AXI4__FBUS__TO_SRAM__WSTRB[`axi_2x3_cpu_fbus_AXI_SW-1:0]), // Templated
						 .wlast_s3		(AXI4__FBUS__TO_SRAM__WLAST), // Templated
						 .bready_s3		(AXI4__FBUS__TO_SRAM__BREADY), // Templated
						 .arvalid_s3		(AXI4__FBUS__TO_SRAM__ARVALID), // Templated
						 .arid_s3		(AXI4__FBUS__TO_SRAM__ARID[`axi_2x3_cpu_fbus_AXI_SIDW-1:0]), // Templated
						 .araddr_s3		(AXI4__FBUS__TO_SRAM__ARADDR[`axi_2x3_cpu_fbus_AXI_AW-1:0]), // Templated
						 .arlen_s3		(AXI4__FBUS__TO_SRAM__ARLEN[`axi_2x3_cpu_fbus_AXI_BLW-1:0]), // Templated
						 .arsize_s3		(AXI4__FBUS__TO_SRAM__ARSIZE[`axi_2x3_cpu_fbus_AXI_BSW-1:0]), // Templated
						 .arburst_s3		(AXI4__FBUS__TO_SRAM__ARBURST[`axi_2x3_cpu_fbus_AXI_BTW-1:0]), // Templated
						 .arlock_s3		(AXI4__FBUS__TO_SRAM__ARLOCK[`axi_2x3_cpu_fbus_AXI_LTW-1:0]), // Templated
						 .arcache_s3		(AXI4__FBUS__TO_SRAM__ARCACHE[`axi_2x3_cpu_fbus_AXI_CTW-1:0]), // Templated
						 .arprot_s3		(AXI4__FBUS__TO_SRAM__ARPROT[`axi_2x3_cpu_fbus_AXI_PTW-1:0]), // Templated
						 .rready_s3		(AXI4__FBUS__TO_SRAM__RREADY), // Templated
						 .dbg_awid_s0		(),		 // Templated
						 .dbg_awaddr_s0		(),		 // Templated
						 .dbg_awlen_s0		(),		 // Templated
						 .dbg_awsize_s0		(),		 // Templated
						 .dbg_awburst_s0	(),		 // Templated
						 .dbg_awlock_s0		(),		 // Templated
						 .dbg_awcache_s0	(),		 // Templated
						 .dbg_awprot_s0		(),		 // Templated
						 .dbg_awvalid_s0	(),		 // Templated
						 .dbg_awready_s0	(),		 // Templated
						 .dbg_wid_s0		(),		 // Templated
						 .dbg_wdata_s0		(),		 // Templated
						 .dbg_wstrb_s0		(),		 // Templated
						 .dbg_wlast_s0		(),		 // Templated
						 .dbg_wvalid_s0		(),		 // Templated
						 .dbg_wready_s0		(),		 // Templated
						 .dbg_bid_s0		(),		 // Templated
						 .dbg_bresp_s0		(),		 // Templated
						 .dbg_bvalid_s0		(),		 // Templated
						 .dbg_bready_s0		(),		 // Templated
						 .dbg_arid_s0		(),		 // Templated
						 .dbg_araddr_s0		(),		 // Templated
						 .dbg_arlen_s0		(),		 // Templated
						 .dbg_arsize_s0		(),		 // Templated
						 .dbg_arburst_s0	(),		 // Templated
						 .dbg_arlock_s0		(),		 // Templated
						 .dbg_arcache_s0	(),		 // Templated
						 .dbg_arprot_s0		(),		 // Templated
						 .dbg_arvalid_s0	(),		 // Templated
						 .dbg_arready_s0	(),		 // Templated
						 .dbg_rid_s0		(),		 // Templated
						 .dbg_rdata_s0		(),		 // Templated
						 .dbg_rresp_s0		(),		 // Templated
						 .dbg_rvalid_s0		(),		 // Templated
						 .dbg_rlast_s0		(),		 // Templated
						 .dbg_rready_s0		(),		 // Templated
						 // Inputs
						 .aclk			(CLK__FBUS),	 // Templated
						 .aresetn		(RSTN__FBUS),	 // Templated
						 .awvalid_m1		(AXI4__RX__AWVALID), // Templated
						 .awaddr_m1		(AXI4__RX__AWADDR[`axi_2x3_cpu_fbus_AXI_AW-1:0]), // Templated
						 .awid_m1		(AXI4__RX__AWID[`axi_2x3_cpu_fbus_AXI_IDW_M1-1:0]), // Templated
						 .awlen_m1		(AXI4__RX__AWLEN[`axi_2x3_cpu_fbus_AXI_BLW-1:0]), // Templated
						 .awsize_m1		(AXI4__RX__AWSIZE[`axi_2x3_cpu_fbus_AXI_BSW-1:0]), // Templated
						 .awburst_m1		(AXI4__RX__AWBURST[`axi_2x3_cpu_fbus_AXI_BTW-1:0]), // Templated
						 .awlock_m1		(AXI4__RX__AWLOCK[`axi_2x3_cpu_fbus_AXI_LTW-1:0]), // Templated
						 .awcache_m1		(AXI4__RX__AWCACHE[`axi_2x3_cpu_fbus_AXI_CTW-1:0]), // Templated
						 .awprot_m1		(AXI4__RX__AWPROT[`axi_2x3_cpu_fbus_AXI_PTW-1:0]), // Templated
						 .wvalid_m1		(AXI4__RX__WVALID), // Templated
						 .wdata_m1		(AXI4__RX__WDATA[`axi_2x3_cpu_fbus_AXI_DW-1:0]), // Templated
						 .wstrb_m1		(AXI4__RX__WSTRB[`axi_2x3_cpu_fbus_AXI_SW-1:0]), // Templated
						 .wlast_m1		(AXI4__RX__WLAST), // Templated
						 .bready_m1		(AXI4__RX__BREADY), // Templated
						 .arvalid_m1		(AXI4__RX__ARVALID), // Templated
						 .arid_m1		(AXI4__RX__ARID[`axi_2x3_cpu_fbus_AXI_IDW_M1-1:0]), // Templated
						 .araddr_m1		(AXI4__RX__ARADDR[`axi_2x3_cpu_fbus_AXI_AW-1:0]), // Templated
						 .arlen_m1		(AXI4__RX__ARLEN[`axi_2x3_cpu_fbus_AXI_BLW-1:0]), // Templated
						 .arsize_m1		(AXI4__RX__ARSIZE[`axi_2x3_cpu_fbus_AXI_BSW-1:0]), // Templated
						 .arburst_m1		(AXI4__RX__ARBURST[`axi_2x3_cpu_fbus_AXI_BTW-1:0]), // Templated
						 .arlock_m1		(AXI4__RX__ARLOCK[`axi_2x3_cpu_fbus_AXI_LTW-1:0]), // Templated
						 .arcache_m1		(AXI4__RX__ARCACHE[`axi_2x3_cpu_fbus_AXI_CTW-1:0]), // Templated
						 .arprot_m1		(AXI4__RX__ARPROT[`axi_2x3_cpu_fbus_AXI_PTW-1:0]), // Templated
						 .rready_m1		(AXI4__RX__RREADY), // Templated
						 .awvalid_m2		(AXI4__CBUS__CB2FB__AWVALID), // Templated
						 .awaddr_m2		(AXI4__CBUS__CB2FB__AWADDR[`axi_2x3_cpu_fbus_AXI_AW-1:0]), // Templated
						 .awid_m2		({9'h0,AXI4__CBUS__CB2FB__AWID}), // Templated
						 .awlen_m2		(AXI4__CBUS__CB2FB__AWLEN[`axi_2x3_cpu_fbus_AXI_BLW-1:0]), // Templated
						 .awsize_m2		(AXI4__CBUS__CB2FB__AWSIZE[`axi_2x3_cpu_fbus_AXI_BSW-1:0]), // Templated
						 .awburst_m2		(AXI4__CBUS__CB2FB__AWBURST[`axi_2x3_cpu_fbus_AXI_BTW-1:0]), // Templated
						 .awlock_m2		(AXI4__CBUS__CB2FB__AWLOCK[`axi_2x3_cpu_fbus_AXI_LTW-1:0]), // Templated
						 .awcache_m2		(AXI4__CBUS__CB2FB__AWCACHE[`axi_2x3_cpu_fbus_AXI_CTW-1:0]), // Templated
						 .awprot_m2		(AXI4__CBUS__CB2FB__AWPROT[`axi_2x3_cpu_fbus_AXI_PTW-1:0]), // Templated
						 .wvalid_m2		(AXI4__CBUS__CB2FB__WVALID), // Templated
						 .wdata_m2		(AXI4__CBUS__CB2FB__WDATA[`axi_2x3_cpu_fbus_AXI_DW-1:0]), // Templated
						 .wstrb_m2		(AXI4__CBUS__CB2FB__WSTRB[`axi_2x3_cpu_fbus_AXI_SW-1:0]), // Templated
						 .wlast_m2		(AXI4__CBUS__CB2FB__WLAST), // Templated
						 .bready_m2		(AXI4__CBUS__CB2FB__BREADY), // Templated
						 .arvalid_m2		(AXI4__CBUS__CB2FB__ARVALID), // Templated
						 .arid_m2		({9'h0,AXI4__CBUS__CB2FB__ARID}), // Templated
						 .araddr_m2		(AXI4__CBUS__CB2FB__ARADDR[`axi_2x3_cpu_fbus_AXI_AW-1:0]), // Templated
						 .arlen_m2		(AXI4__CBUS__CB2FB__ARLEN[`axi_2x3_cpu_fbus_AXI_BLW-1:0]), // Templated
						 .arsize_m2		(AXI4__CBUS__CB2FB__ARSIZE[`axi_2x3_cpu_fbus_AXI_BSW-1:0]), // Templated
						 .arburst_m2		(AXI4__CBUS__CB2FB__ARBURST[`axi_2x3_cpu_fbus_AXI_BTW-1:0]), // Templated
						 .arlock_m2		(AXI4__CBUS__CB2FB__ARLOCK[`axi_2x3_cpu_fbus_AXI_LTW-1:0]), // Templated
						 .arcache_m2		(AXI4__CBUS__CB2FB__ARCACHE[`axi_2x3_cpu_fbus_AXI_CTW-1:0]), // Templated
						 .arprot_m2		(AXI4__CBUS__CB2FB__ARPROT[`axi_2x3_cpu_fbus_AXI_PTW-1:0]), // Templated
						 .rready_m2		(AXI4__CBUS__CB2FB__RREADY), // Templated
						 .awready_s1		(CPU_0__AXI4__CPU__FRONTPORT__AWREADY), // Templated
						 .wready_s1		(CPU_0__AXI4__CPU__FRONTPORT__WREADY), // Templated
						 .bvalid_s1		(CPU_0__AXI4__CPU__FRONTPORT__BVALID), // Templated
						 .bid_s1		(CPU_0__AXI4__CPU__FRONTPORT__BID[`axi_2x3_cpu_fbus_AXI_SIDW-1:0]), // Templated
						 .bresp_s1		(CPU_0__AXI4__CPU__FRONTPORT__BRESP[`axi_2x3_cpu_fbus_AXI_BRW-1:0]), // Templated
						 .arready_s1		(CPU_0__AXI4__CPU__FRONTPORT__ARREADY), // Templated
						 .rvalid_s1		(CPU_0__AXI4__CPU__FRONTPORT__RVALID), // Templated
						 .rid_s1		(CPU_0__AXI4__CPU__FRONTPORT__RID[`axi_2x3_cpu_fbus_AXI_SIDW-1:0]), // Templated
						 .rdata_s1		(CPU_0__AXI4__CPU__FRONTPORT__RDATA[`axi_2x3_cpu_fbus_AXI_DW-1:0]), // Templated
						 .rlast_s1		(CPU_0__AXI4__CPU__FRONTPORT__RLAST), // Templated
						 .rresp_s1		(CPU_0__AXI4__CPU__FRONTPORT__RRESP[`axi_2x3_cpu_fbus_AXI_RRW-1:0]), // Templated
						 .awready_s2		(AXI4__FBUS__TO_ROM__AWREADY), // Templated
						 .wready_s2		(AXI4__FBUS__TO_ROM__WREADY), // Templated
						 .bvalid_s2		(AXI4__FBUS__TO_ROM__BVALID), // Templated
						 .bid_s2		(AXI4__FBUS__TO_ROM__BID[`axi_2x3_cpu_fbus_AXI_SIDW-1:0]), // Templated
						 .bresp_s2		(AXI4__FBUS__TO_ROM__BRESP[`axi_2x3_cpu_fbus_AXI_BRW-1:0]), // Templated
						 .arready_s2		(AXI4__FBUS__TO_ROM__ARREADY), // Templated
						 .rvalid_s2		(AXI4__FBUS__TO_ROM__RVALID), // Templated
						 .rid_s2		(AXI4__FBUS__TO_ROM__RID[`axi_2x3_cpu_fbus_AXI_SIDW-1:0]), // Templated
						 .rdata_s2		(AXI4__FBUS__TO_ROM__RDATA[`axi_2x3_cpu_fbus_AXI_DW-1:0]), // Templated
						 .rlast_s2		(AXI4__FBUS__TO_ROM__RLAST), // Templated
						 .rresp_s2		(AXI4__FBUS__TO_ROM__RRESP[`axi_2x3_cpu_fbus_AXI_RRW-1:0]), // Templated
						 .awready_s3		(AXI4__FBUS__TO_SRAM__AWREADY), // Templated
						 .wready_s3		(AXI4__FBUS__TO_SRAM__WREADY), // Templated
						 .bvalid_s3		(AXI4__FBUS__TO_SRAM__BVALID), // Templated
						 .bid_s3		(AXI4__FBUS__TO_SRAM__BID[`axi_2x3_cpu_fbus_AXI_SIDW-1:0]), // Templated
						 .bresp_s3		(AXI4__FBUS__TO_SRAM__BRESP[`axi_2x3_cpu_fbus_AXI_BRW-1:0]), // Templated
						 .arready_s3		(AXI4__FBUS__TO_SRAM__ARREADY), // Templated
						 .rvalid_s3		(AXI4__FBUS__TO_SRAM__RVALID), // Templated
						 .rid_s3		(AXI4__FBUS__TO_SRAM__RID[`axi_2x3_cpu_fbus_AXI_SIDW-1:0]), // Templated
						 .rdata_s3		(AXI4__FBUS__TO_SRAM__RDATA[`axi_2x3_cpu_fbus_AXI_DW-1:0]), // Templated
						 .rlast_s3		(AXI4__FBUS__TO_SRAM__RLAST), // Templated
						 .rresp_s3		(AXI4__FBUS__TO_SRAM__RRESP[`axi_2x3_cpu_fbus_AXI_RRW-1:0])); // Templated

    /* axi4to3 AUTO_TEMPLATE "AXI4TO3_FBUS_\(.*\)_0" (
        .CLK__BUS  (CLK__FBUS),
        .RSTN__BUS  (RSTN__FBUS),
		.AXI4__BUS__.*USER	(),
        .AXI4__BUS__\(.*\)  (AXI4__FBUS__TO_@__\1[]),
		.AXI3__BUS__.*USER	(),
        .AXI3__BUS__\(.*\)  (AXI3__FBUS__TO_@__\1[]),
    );*/
    axi4to3
        #(.ADDR_WIDTH(SRAM_AW),
          .ID_WIDTH(SRAM_IW),
          .DATA_WIDTH(SRAM_DW),
          .QUEUE_DEPTH(16),
          .USER_WIDTH(SRAM_UW),
          .LEN_WIDTH(8),
          .STRB_WIDTH(SRAM_SW))
    AXI4TO3_FBUS_SRAM_0 (/*AUTOINST*/
			 // Outputs
			 .AXI4__BUS__AWREADY	(AXI4__FBUS__TO_SRAM__AWREADY), // Templated
			 .AXI4__BUS__WREADY	(AXI4__FBUS__TO_SRAM__WREADY), // Templated
			 .AXI4__BUS__BID	(AXI4__FBUS__TO_SRAM__BID[SRAM_IW-1:0]), // Templated
			 .AXI4__BUS__BVALID	(AXI4__FBUS__TO_SRAM__BVALID), // Templated
			 .AXI4__BUS__BRESP	(AXI4__FBUS__TO_SRAM__BRESP[1:0]), // Templated
			 .AXI4__BUS__ARREADY	(AXI4__FBUS__TO_SRAM__ARREADY), // Templated
			 .AXI4__BUS__RID	(AXI4__FBUS__TO_SRAM__RID[SRAM_IW-1:0]), // Templated
			 .AXI4__BUS__RDATA	(AXI4__FBUS__TO_SRAM__RDATA[SRAM_DW-1:0]), // Templated
			 .AXI4__BUS__RRESP	(AXI4__FBUS__TO_SRAM__RRESP[1:0]), // Templated
			 .AXI4__BUS__RLAST	(AXI4__FBUS__TO_SRAM__RLAST), // Templated
			 .AXI4__BUS__RVALID	(AXI4__FBUS__TO_SRAM__RVALID), // Templated
			 .AXI3__BUS__AWADDR	(AXI3__FBUS__TO_SRAM__AWADDR[SRAM_AW-1:0]), // Templated
			 .AXI3__BUS__AWID	(AXI3__FBUS__TO_SRAM__AWID[SRAM_IW-1:0]), // Templated
			 .AXI3__BUS__AWLEN	(AXI3__FBUS__TO_SRAM__AWLEN[7:0]), // Templated
			 .AXI3__BUS__AWSIZE	(AXI3__FBUS__TO_SRAM__AWSIZE[2:0]), // Templated
			 .AXI3__BUS__AWBURST	(AXI3__FBUS__TO_SRAM__AWBURST[1:0]), // Templated
			 .AXI3__BUS__AWLOCK	(AXI3__FBUS__TO_SRAM__AWLOCK[1:0]), // Templated
			 .AXI3__BUS__AWCACHE	(AXI3__FBUS__TO_SRAM__AWCACHE[3:0]), // Templated
			 .AXI3__BUS__AWPROT	(AXI3__FBUS__TO_SRAM__AWPROT[2:0]), // Templated
			 .AXI3__BUS__AWUSER	(),		 // Templated
			 .AXI3__BUS__AWVALID	(AXI3__FBUS__TO_SRAM__AWVALID), // Templated
			 .AXI3__BUS__WID	(AXI3__FBUS__TO_SRAM__WID[SRAM_IW-1:0]), // Templated
			 .AXI3__BUS__WDATA	(AXI3__FBUS__TO_SRAM__WDATA[SRAM_DW-1:0]), // Templated
			 .AXI3__BUS__WSTRB	(AXI3__FBUS__TO_SRAM__WSTRB[SRAM_SW-1:0]), // Templated
			 .AXI3__BUS__WLAST	(AXI3__FBUS__TO_SRAM__WLAST), // Templated
			 .AXI3__BUS__WVALID	(AXI3__FBUS__TO_SRAM__WVALID), // Templated
			 .AXI3__BUS__BREADY	(AXI3__FBUS__TO_SRAM__BREADY), // Templated
			 .AXI3__BUS__ARID	(AXI3__FBUS__TO_SRAM__ARID[SRAM_IW-1:0]), // Templated
			 .AXI3__BUS__ARADDR	(AXI3__FBUS__TO_SRAM__ARADDR[SRAM_AW-1:0]), // Templated
			 .AXI3__BUS__ARLEN	(AXI3__FBUS__TO_SRAM__ARLEN[7:0]), // Templated
			 .AXI3__BUS__ARSIZE	(AXI3__FBUS__TO_SRAM__ARSIZE[2:0]), // Templated
			 .AXI3__BUS__ARBURST	(AXI3__FBUS__TO_SRAM__ARBURST[1:0]), // Templated
			 .AXI3__BUS__ARLOCK	(AXI3__FBUS__TO_SRAM__ARLOCK[1:0]), // Templated
			 .AXI3__BUS__ARCACHE	(AXI3__FBUS__TO_SRAM__ARCACHE[3:0]), // Templated
			 .AXI3__BUS__ARPROT	(AXI3__FBUS__TO_SRAM__ARPROT[2:0]), // Templated
			 .AXI3__BUS__ARUSER	(),		 // Templated
			 .AXI3__BUS__ARVALID	(AXI3__FBUS__TO_SRAM__ARVALID), // Templated
			 .AXI3__BUS__RREADY	(AXI3__FBUS__TO_SRAM__RREADY), // Templated
			 // Inputs
			 .CLK__BUS		(CLK__FBUS),	 // Templated
			 .RSTN__BUS		(RSTN__FBUS),	 // Templated
			 .AXI4__BUS__AWADDR	(AXI4__FBUS__TO_SRAM__AWADDR[SRAM_AW-1:0]), // Templated
			 .AXI4__BUS__AWID	(AXI4__FBUS__TO_SRAM__AWID[SRAM_IW-1:0]), // Templated
			 .AXI4__BUS__AWLEN	(AXI4__FBUS__TO_SRAM__AWLEN[7:0]), // Templated
			 .AXI4__BUS__AWSIZE	(AXI4__FBUS__TO_SRAM__AWSIZE[2:0]), // Templated
			 .AXI4__BUS__AWBURST	(AXI4__FBUS__TO_SRAM__AWBURST[1:0]), // Templated
			 .AXI4__BUS__AWLOCK	(AXI4__FBUS__TO_SRAM__AWLOCK), // Templated
			 .AXI4__BUS__AWCACHE	(AXI4__FBUS__TO_SRAM__AWCACHE[3:0]), // Templated
			 .AXI4__BUS__AWPROT	(AXI4__FBUS__TO_SRAM__AWPROT[2:0]), // Templated
			 .AXI4__BUS__AWUSER	(),		 // Templated
			 .AXI4__BUS__AWVALID	(AXI4__FBUS__TO_SRAM__AWVALID), // Templated
			 .AXI4__BUS__WDATA	(AXI4__FBUS__TO_SRAM__WDATA[SRAM_DW-1:0]), // Templated
			 .AXI4__BUS__WSTRB	(AXI4__FBUS__TO_SRAM__WSTRB[SRAM_SW-1:0]), // Templated
			 .AXI4__BUS__WVALID	(AXI4__FBUS__TO_SRAM__WVALID), // Templated
			 .AXI4__BUS__WLAST	(AXI4__FBUS__TO_SRAM__WLAST), // Templated
			 .AXI4__BUS__BREADY	(AXI4__FBUS__TO_SRAM__BREADY), // Templated
			 .AXI4__BUS__ARID	(AXI4__FBUS__TO_SRAM__ARID[SRAM_IW-1:0]), // Templated
			 .AXI4__BUS__ARADDR	(AXI4__FBUS__TO_SRAM__ARADDR[SRAM_AW-1:0]), // Templated
			 .AXI4__BUS__ARLEN	(AXI4__FBUS__TO_SRAM__ARLEN[7:0]), // Templated
			 .AXI4__BUS__ARSIZE	(AXI4__FBUS__TO_SRAM__ARSIZE[2:0]), // Templated
			 .AXI4__BUS__ARBURST	(AXI4__FBUS__TO_SRAM__ARBURST[1:0]), // Templated
			 .AXI4__BUS__ARLOCK	(AXI4__FBUS__TO_SRAM__ARLOCK), // Templated
			 .AXI4__BUS__ARCACHE	(AXI4__FBUS__TO_SRAM__ARCACHE[3:0]), // Templated
			 .AXI4__BUS__ARPROT	(AXI4__FBUS__TO_SRAM__ARPROT[2:0]), // Templated
			 .AXI4__BUS__ARUSER	(),		 // Templated
			 .AXI4__BUS__ARVALID	(AXI4__FBUS__TO_SRAM__ARVALID), // Templated
			 .AXI4__BUS__RREADY	(AXI4__FBUS__TO_SRAM__RREADY), // Templated
			 .AXI3__BUS__AWREADY	(AXI3__FBUS__TO_SRAM__AWREADY), // Templated
			 .AXI3__BUS__WREADY	(AXI3__FBUS__TO_SRAM__WREADY), // Templated
			 .AXI3__BUS__BID	(AXI3__FBUS__TO_SRAM__BID[SRAM_IW-1:0]), // Templated
			 .AXI3__BUS__BRESP	(AXI3__FBUS__TO_SRAM__BRESP[1:0]), // Templated
			 .AXI3__BUS__BVALID	(AXI3__FBUS__TO_SRAM__BVALID), // Templated
			 .AXI3__BUS__ARREADY	(AXI3__FBUS__TO_SRAM__ARREADY), // Templated
			 .AXI3__BUS__RID	(AXI3__FBUS__TO_SRAM__RID[SRAM_IW-1:0]), // Templated
			 .AXI3__BUS__RDATA	(AXI3__FBUS__TO_SRAM__RDATA[SRAM_DW-1:0]), // Templated
			 .AXI3__BUS__RRESP	(AXI3__FBUS__TO_SRAM__RRESP[1:0]), // Templated
			 .AXI3__BUS__RLAST	(AXI3__FBUS__TO_SRAM__RLAST), // Templated
			 .AXI3__BUS__RVALID	(AXI3__FBUS__TO_SRAM__RVALID)); // Templated
    axi4to3
        #(.ADDR_WIDTH(ROM_AW),
          .ID_WIDTH(ROM_IW),
          .DATA_WIDTH(ROM_DW),
          .QUEUE_DEPTH(16),
          .USER_WIDTH(ROM_UW),
          .LEN_WIDTH(8),
          .STRB_WIDTH(ROM_SW))
    AXI4TO3_FBUS_ROM_0 (/*AUTOINST*/
			// Outputs
			.AXI4__BUS__AWREADY(AXI4__FBUS__TO_ROM__AWREADY), // Templated
			.AXI4__BUS__WREADY(AXI4__FBUS__TO_ROM__WREADY), // Templated
			.AXI4__BUS__BID	(AXI4__FBUS__TO_ROM__BID[ROM_IW-1:0]), // Templated
			.AXI4__BUS__BVALID(AXI4__FBUS__TO_ROM__BVALID), // Templated
			.AXI4__BUS__BRESP(AXI4__FBUS__TO_ROM__BRESP[1:0]), // Templated
			.AXI4__BUS__ARREADY(AXI4__FBUS__TO_ROM__ARREADY), // Templated
			.AXI4__BUS__RID	(AXI4__FBUS__TO_ROM__RID[ROM_IW-1:0]), // Templated
			.AXI4__BUS__RDATA(AXI4__FBUS__TO_ROM__RDATA[ROM_DW-1:0]), // Templated
			.AXI4__BUS__RRESP(AXI4__FBUS__TO_ROM__RRESP[1:0]), // Templated
			.AXI4__BUS__RLAST(AXI4__FBUS__TO_ROM__RLAST), // Templated
			.AXI4__BUS__RVALID(AXI4__FBUS__TO_ROM__RVALID), // Templated
			.AXI3__BUS__AWADDR(AXI3__FBUS__TO_ROM__AWADDR[ROM_AW-1:0]), // Templated
			.AXI3__BUS__AWID(AXI3__FBUS__TO_ROM__AWID[ROM_IW-1:0]), // Templated
			.AXI3__BUS__AWLEN(AXI3__FBUS__TO_ROM__AWLEN[7:0]), // Templated
			.AXI3__BUS__AWSIZE(AXI3__FBUS__TO_ROM__AWSIZE[2:0]), // Templated
			.AXI3__BUS__AWBURST(AXI3__FBUS__TO_ROM__AWBURST[1:0]), // Templated
			.AXI3__BUS__AWLOCK(AXI3__FBUS__TO_ROM__AWLOCK[1:0]), // Templated
			.AXI3__BUS__AWCACHE(AXI3__FBUS__TO_ROM__AWCACHE[3:0]), // Templated
			.AXI3__BUS__AWPROT(AXI3__FBUS__TO_ROM__AWPROT[2:0]), // Templated
			.AXI3__BUS__AWUSER(),			 // Templated
			.AXI3__BUS__AWVALID(AXI3__FBUS__TO_ROM__AWVALID), // Templated
			.AXI3__BUS__WID	(AXI3__FBUS__TO_ROM__WID[ROM_IW-1:0]), // Templated
			.AXI3__BUS__WDATA(AXI3__FBUS__TO_ROM__WDATA[ROM_DW-1:0]), // Templated
			.AXI3__BUS__WSTRB(AXI3__FBUS__TO_ROM__WSTRB[ROM_SW-1:0]), // Templated
			.AXI3__BUS__WLAST(AXI3__FBUS__TO_ROM__WLAST), // Templated
			.AXI3__BUS__WVALID(AXI3__FBUS__TO_ROM__WVALID), // Templated
			.AXI3__BUS__BREADY(AXI3__FBUS__TO_ROM__BREADY), // Templated
			.AXI3__BUS__ARID(AXI3__FBUS__TO_ROM__ARID[ROM_IW-1:0]), // Templated
			.AXI3__BUS__ARADDR(AXI3__FBUS__TO_ROM__ARADDR[ROM_AW-1:0]), // Templated
			.AXI3__BUS__ARLEN(AXI3__FBUS__TO_ROM__ARLEN[7:0]), // Templated
			.AXI3__BUS__ARSIZE(AXI3__FBUS__TO_ROM__ARSIZE[2:0]), // Templated
			.AXI3__BUS__ARBURST(AXI3__FBUS__TO_ROM__ARBURST[1:0]), // Templated
			.AXI3__BUS__ARLOCK(AXI3__FBUS__TO_ROM__ARLOCK[1:0]), // Templated
			.AXI3__BUS__ARCACHE(AXI3__FBUS__TO_ROM__ARCACHE[3:0]), // Templated
			.AXI3__BUS__ARPROT(AXI3__FBUS__TO_ROM__ARPROT[2:0]), // Templated
			.AXI3__BUS__ARUSER(),			 // Templated
			.AXI3__BUS__ARVALID(AXI3__FBUS__TO_ROM__ARVALID), // Templated
			.AXI3__BUS__RREADY(AXI3__FBUS__TO_ROM__RREADY), // Templated
			// Inputs
			.CLK__BUS	(CLK__FBUS),		 // Templated
			.RSTN__BUS	(RSTN__FBUS),		 // Templated
			.AXI4__BUS__AWADDR(AXI4__FBUS__TO_ROM__AWADDR[ROM_AW-1:0]), // Templated
			.AXI4__BUS__AWID(AXI4__FBUS__TO_ROM__AWID[ROM_IW-1:0]), // Templated
			.AXI4__BUS__AWLEN(AXI4__FBUS__TO_ROM__AWLEN[7:0]), // Templated
			.AXI4__BUS__AWSIZE(AXI4__FBUS__TO_ROM__AWSIZE[2:0]), // Templated
			.AXI4__BUS__AWBURST(AXI4__FBUS__TO_ROM__AWBURST[1:0]), // Templated
			.AXI4__BUS__AWLOCK(AXI4__FBUS__TO_ROM__AWLOCK), // Templated
			.AXI4__BUS__AWCACHE(AXI4__FBUS__TO_ROM__AWCACHE[3:0]), // Templated
			.AXI4__BUS__AWPROT(AXI4__FBUS__TO_ROM__AWPROT[2:0]), // Templated
			.AXI4__BUS__AWUSER(),			 // Templated
			.AXI4__BUS__AWVALID(AXI4__FBUS__TO_ROM__AWVALID), // Templated
			.AXI4__BUS__WDATA(AXI4__FBUS__TO_ROM__WDATA[ROM_DW-1:0]), // Templated
			.AXI4__BUS__WSTRB(AXI4__FBUS__TO_ROM__WSTRB[ROM_SW-1:0]), // Templated
			.AXI4__BUS__WVALID(AXI4__FBUS__TO_ROM__WVALID), // Templated
			.AXI4__BUS__WLAST(AXI4__FBUS__TO_ROM__WLAST), // Templated
			.AXI4__BUS__BREADY(AXI4__FBUS__TO_ROM__BREADY), // Templated
			.AXI4__BUS__ARID(AXI4__FBUS__TO_ROM__ARID[ROM_IW-1:0]), // Templated
			.AXI4__BUS__ARADDR(AXI4__FBUS__TO_ROM__ARADDR[ROM_AW-1:0]), // Templated
			.AXI4__BUS__ARLEN(AXI4__FBUS__TO_ROM__ARLEN[7:0]), // Templated
			.AXI4__BUS__ARSIZE(AXI4__FBUS__TO_ROM__ARSIZE[2:0]), // Templated
			.AXI4__BUS__ARBURST(AXI4__FBUS__TO_ROM__ARBURST[1:0]), // Templated
			.AXI4__BUS__ARLOCK(AXI4__FBUS__TO_ROM__ARLOCK), // Templated
			.AXI4__BUS__ARCACHE(AXI4__FBUS__TO_ROM__ARCACHE[3:0]), // Templated
			.AXI4__BUS__ARPROT(AXI4__FBUS__TO_ROM__ARPROT[2:0]), // Templated
			.AXI4__BUS__ARUSER(),			 // Templated
			.AXI4__BUS__ARVALID(AXI4__FBUS__TO_ROM__ARVALID), // Templated
			.AXI4__BUS__RREADY(AXI4__FBUS__TO_ROM__RREADY), // Templated
			.AXI3__BUS__AWREADY(AXI3__FBUS__TO_ROM__AWREADY), // Templated
			.AXI3__BUS__WREADY(AXI3__FBUS__TO_ROM__WREADY), // Templated
			.AXI3__BUS__BID	(AXI3__FBUS__TO_ROM__BID[ROM_IW-1:0]), // Templated
			.AXI3__BUS__BRESP(AXI3__FBUS__TO_ROM__BRESP[1:0]), // Templated
			.AXI3__BUS__BVALID(AXI3__FBUS__TO_ROM__BVALID), // Templated
			.AXI3__BUS__ARREADY(AXI3__FBUS__TO_ROM__ARREADY), // Templated
			.AXI3__BUS__RID	(AXI3__FBUS__TO_ROM__RID[ROM_IW-1:0]), // Templated
			.AXI3__BUS__RDATA(AXI3__FBUS__TO_ROM__RDATA[ROM_DW-1:0]), // Templated
			.AXI3__BUS__RRESP(AXI3__FBUS__TO_ROM__RRESP[1:0]), // Templated
			.AXI3__BUS__RLAST(AXI3__FBUS__TO_ROM__RLAST), // Templated
			.AXI3__BUS__RVALID(AXI3__FBUS__TO_ROM__RVALID)); // Templated

    /* axi2axi_cpu_sram_DW_axi_x2x AUTO_TEMPLATE "AXI3X2X_\(.*\)_0" (
        .aclk_m                 (CLK__FBUS),
        .aresetn_m              (RSTN__FBUS),
        .aclk_s                 (CLK__@),
        .aresetn_s              (RSTN__@),
        .\(.*\)_m               (AXI3__FBUS__TO_@__@"(upcase \\"\1\\")"[]),
        .\(.*\)_s               (AXI3__@__@"(upcase \\"\1\\")"[]),
        .\(.*\)_s1              (AXI3__@__@"(upcase \\"\1\\")"[]),
    );*/
    axi2axi_cpu_sram_DW_axi_x2x AXI3X2X_SRAM_0 (/*AUTOINST*/
						// Outputs
						.awready_m	(AXI3__FBUS__TO_SRAM__AWREADY), // Templated
						.wready_m	(AXI3__FBUS__TO_SRAM__WREADY), // Templated
						.bvalid_m	(AXI3__FBUS__TO_SRAM__BVALID), // Templated
						.bid_m		(AXI3__FBUS__TO_SRAM__BID[`axi2axi_cpu_sram_X2X_MP_IDW-1:0]), // Templated
						.bresp_m	(AXI3__FBUS__TO_SRAM__BRESP[`axi2axi_cpu_sram_X2X_BRW-1:0]), // Templated
						.arready_m	(AXI3__FBUS__TO_SRAM__ARREADY), // Templated
						.rvalid_m	(AXI3__FBUS__TO_SRAM__RVALID), // Templated
						.rid_m		(AXI3__FBUS__TO_SRAM__RID[`axi2axi_cpu_sram_X2X_MP_IDW-1:0]), // Templated
						.rdata_m	(AXI3__FBUS__TO_SRAM__RDATA[`axi2axi_cpu_sram_X2X_MP_DW-1:0]), // Templated
						.rlast_m	(AXI3__FBUS__TO_SRAM__RLAST), // Templated
						.rresp_m	(AXI3__FBUS__TO_SRAM__RRESP[`axi2axi_cpu_sram_X2X_RRW-1:0]), // Templated
						.awvalid_s1	(AXI3__SRAM__AWVALID), // Templated
						.awaddr_s1	(AXI3__SRAM__AWADDR[`axi2axi_cpu_sram_X2X_SP_AW-1:0]), // Templated
						.awid_s1	(AXI3__SRAM__AWID[`axi2axi_cpu_sram_X2X_SP_IDW-1:0]), // Templated
						.awlen_s1	(AXI3__SRAM__AWLEN[`axi2axi_cpu_sram_X2X_SP_BLW-1:0]), // Templated
						.awsize_s1	(AXI3__SRAM__AWSIZE[`axi2axi_cpu_sram_X2X_BSW-1:0]), // Templated
						.awburst_s1	(AXI3__SRAM__AWBURST[`axi2axi_cpu_sram_X2X_BTW-1:0]), // Templated
						.awlock_s1	(AXI3__SRAM__AWLOCK[`axi2axi_cpu_sram_X2X_LTW-1:0]), // Templated
						.awcache_s1	(AXI3__SRAM__AWCACHE[`axi2axi_cpu_sram_X2X_CTW-1:0]), // Templated
						.awprot_s1	(AXI3__SRAM__AWPROT[`axi2axi_cpu_sram_X2X_PTW-1:0]), // Templated
						.wvalid_s1	(AXI3__SRAM__WVALID), // Templated
						.wid_s1		(AXI3__SRAM__WID[`axi2axi_cpu_sram_X2X_SP_IDW-1:0]), // Templated
						.wdata_s1	(AXI3__SRAM__WDATA[`axi2axi_cpu_sram_X2X_SP_DW-1:0]), // Templated
						.wstrb_s1	(AXI3__SRAM__WSTRB[`axi2axi_cpu_sram_X2X_SP_SW-1:0]), // Templated
						.wlast_s1	(AXI3__SRAM__WLAST), // Templated
						.bready_s1	(AXI3__SRAM__BREADY), // Templated
						.arvalid_s	(AXI3__SRAM__ARVALID), // Templated
						.arid_s		(AXI3__SRAM__ARID[`axi2axi_cpu_sram_X2X_SP_IDW-1:0]), // Templated
						.araddr_s	(AXI3__SRAM__ARADDR[`axi2axi_cpu_sram_X2X_SP_AW-1:0]), // Templated
						.arlen_s	(AXI3__SRAM__ARLEN[`axi2axi_cpu_sram_X2X_SP_BLW-1:0]), // Templated
						.arsize_s	(AXI3__SRAM__ARSIZE[`axi2axi_cpu_sram_X2X_BSW-1:0]), // Templated
						.arburst_s	(AXI3__SRAM__ARBURST[`axi2axi_cpu_sram_X2X_BTW-1:0]), // Templated
						.arlock_s	(AXI3__SRAM__ARLOCK[`axi2axi_cpu_sram_X2X_LTW-1:0]), // Templated
						.arcache_s	(AXI3__SRAM__ARCACHE[`axi2axi_cpu_sram_X2X_CTW-1:0]), // Templated
						.arprot_s	(AXI3__SRAM__ARPROT[`axi2axi_cpu_sram_X2X_PTW-1:0]), // Templated
						.rready_s	(AXI3__SRAM__RREADY), // Templated
						// Inputs
						.aclk_m		(CLK__FBUS),	 // Templated
						.aresetn_m	(RSTN__FBUS),	 // Templated
						.awvalid_m	(AXI3__FBUS__TO_SRAM__AWVALID), // Templated
						.awaddr_m	(AXI3__FBUS__TO_SRAM__AWADDR[`axi2axi_cpu_sram_X2X_MP_AW-1:0]), // Templated
						.awid_m		(AXI3__FBUS__TO_SRAM__AWID[`axi2axi_cpu_sram_X2X_MP_IDW-1:0]), // Templated
						.awlen_m	(AXI3__FBUS__TO_SRAM__AWLEN[`axi2axi_cpu_sram_X2X_MP_BLW-1:0]), // Templated
						.awsize_m	(AXI3__FBUS__TO_SRAM__AWSIZE[`axi2axi_cpu_sram_X2X_BSW-1:0]), // Templated
						.awburst_m	(AXI3__FBUS__TO_SRAM__AWBURST[`axi2axi_cpu_sram_X2X_BTW-1:0]), // Templated
						.awlock_m	(AXI3__FBUS__TO_SRAM__AWLOCK[`axi2axi_cpu_sram_X2X_LTW-1:0]), // Templated
						.awcache_m	(AXI3__FBUS__TO_SRAM__AWCACHE[`axi2axi_cpu_sram_X2X_CTW-1:0]), // Templated
						.awprot_m	(AXI3__FBUS__TO_SRAM__AWPROT[`axi2axi_cpu_sram_X2X_PTW-1:0]), // Templated
						.wvalid_m	(AXI3__FBUS__TO_SRAM__WVALID), // Templated
						.wid_m		(AXI3__FBUS__TO_SRAM__WID[`axi2axi_cpu_sram_X2X_MP_IDW-1:0]), // Templated
						.wdata_m	(AXI3__FBUS__TO_SRAM__WDATA[`axi2axi_cpu_sram_X2X_MP_DW-1:0]), // Templated
						.wstrb_m	(AXI3__FBUS__TO_SRAM__WSTRB[`axi2axi_cpu_sram_X2X_MP_SW-1:0]), // Templated
						.wlast_m	(AXI3__FBUS__TO_SRAM__WLAST), // Templated
						.bready_m	(AXI3__FBUS__TO_SRAM__BREADY), // Templated
						.arvalid_m	(AXI3__FBUS__TO_SRAM__ARVALID), // Templated
						.arid_m		(AXI3__FBUS__TO_SRAM__ARID[`axi2axi_cpu_sram_X2X_MP_IDW-1:0]), // Templated
						.araddr_m	(AXI3__FBUS__TO_SRAM__ARADDR[`axi2axi_cpu_sram_X2X_MP_AW-1:0]), // Templated
						.arlen_m	(AXI3__FBUS__TO_SRAM__ARLEN[`axi2axi_cpu_sram_X2X_MP_BLW-1:0]), // Templated
						.arsize_m	(AXI3__FBUS__TO_SRAM__ARSIZE[`axi2axi_cpu_sram_X2X_BSW-1:0]), // Templated
						.arburst_m	(AXI3__FBUS__TO_SRAM__ARBURST[`axi2axi_cpu_sram_X2X_BTW-1:0]), // Templated
						.arlock_m	(AXI3__FBUS__TO_SRAM__ARLOCK[`axi2axi_cpu_sram_X2X_LTW-1:0]), // Templated
						.arcache_m	(AXI3__FBUS__TO_SRAM__ARCACHE[`axi2axi_cpu_sram_X2X_CTW-1:0]), // Templated
						.arprot_m	(AXI3__FBUS__TO_SRAM__ARPROT[`axi2axi_cpu_sram_X2X_PTW-1:0]), // Templated
						.rready_m	(AXI3__FBUS__TO_SRAM__RREADY), // Templated
						.aclk_s		(CLK__SRAM),	 // Templated
						.aresetn_s	(RSTN__SRAM),	 // Templated
						.awready_s1	(AXI3__SRAM__AWREADY), // Templated
						.wready_s1	(AXI3__SRAM__WREADY), // Templated
						.bvalid_s1	(AXI3__SRAM__BVALID), // Templated
						.bid_s1		(AXI3__SRAM__BID[`axi2axi_cpu_sram_X2X_SP_IDW-1:0]), // Templated
						.bresp_s1	(AXI3__SRAM__BRESP[`axi2axi_cpu_sram_X2X_BRW-1:0]), // Templated
						.arready_s	(AXI3__SRAM__ARREADY), // Templated
						.rvalid_s	(AXI3__SRAM__RVALID), // Templated
						.rid_s		(AXI3__SRAM__RID[`axi2axi_cpu_sram_X2X_SP_IDW-1:0]), // Templated
						.rdata_s	(AXI3__SRAM__RDATA[`axi2axi_cpu_sram_X2X_SP_DW-1:0]), // Templated
						.rlast_s	(AXI3__SRAM__RLAST), // Templated
						.rresp_s	(AXI3__SRAM__RRESP[`axi2axi_cpu_sram_X2X_RRW-1:0])); // Templated
    axi2axi_cpu_sram_DW_axi_x2x AXI3X2X_ROM_0 (/*AUTOINST*/
					       // Outputs
					       .awready_m	(AXI3__FBUS__TO_ROM__AWREADY), // Templated
					       .wready_m	(AXI3__FBUS__TO_ROM__WREADY), // Templated
					       .bvalid_m	(AXI3__FBUS__TO_ROM__BVALID), // Templated
					       .bid_m		(AXI3__FBUS__TO_ROM__BID[`axi2axi_cpu_sram_X2X_MP_IDW-1:0]), // Templated
					       .bresp_m		(AXI3__FBUS__TO_ROM__BRESP[`axi2axi_cpu_sram_X2X_BRW-1:0]), // Templated
					       .arready_m	(AXI3__FBUS__TO_ROM__ARREADY), // Templated
					       .rvalid_m	(AXI3__FBUS__TO_ROM__RVALID), // Templated
					       .rid_m		(AXI3__FBUS__TO_ROM__RID[`axi2axi_cpu_sram_X2X_MP_IDW-1:0]), // Templated
					       .rdata_m		(AXI3__FBUS__TO_ROM__RDATA[`axi2axi_cpu_sram_X2X_MP_DW-1:0]), // Templated
					       .rlast_m		(AXI3__FBUS__TO_ROM__RLAST), // Templated
					       .rresp_m		(AXI3__FBUS__TO_ROM__RRESP[`axi2axi_cpu_sram_X2X_RRW-1:0]), // Templated
					       .awvalid_s1	(AXI3__ROM__AWVALID), // Templated
					       .awaddr_s1	(AXI3__ROM__AWADDR[`axi2axi_cpu_sram_X2X_SP_AW-1:0]), // Templated
					       .awid_s1		(AXI3__ROM__AWID[`axi2axi_cpu_sram_X2X_SP_IDW-1:0]), // Templated
					       .awlen_s1	(AXI3__ROM__AWLEN[`axi2axi_cpu_sram_X2X_SP_BLW-1:0]), // Templated
					       .awsize_s1	(AXI3__ROM__AWSIZE[`axi2axi_cpu_sram_X2X_BSW-1:0]), // Templated
					       .awburst_s1	(AXI3__ROM__AWBURST[`axi2axi_cpu_sram_X2X_BTW-1:0]), // Templated
					       .awlock_s1	(AXI3__ROM__AWLOCK[`axi2axi_cpu_sram_X2X_LTW-1:0]), // Templated
					       .awcache_s1	(AXI3__ROM__AWCACHE[`axi2axi_cpu_sram_X2X_CTW-1:0]), // Templated
					       .awprot_s1	(AXI3__ROM__AWPROT[`axi2axi_cpu_sram_X2X_PTW-1:0]), // Templated
					       .wvalid_s1	(AXI3__ROM__WVALID), // Templated
					       .wid_s1		(AXI3__ROM__WID[`axi2axi_cpu_sram_X2X_SP_IDW-1:0]), // Templated
					       .wdata_s1	(AXI3__ROM__WDATA[`axi2axi_cpu_sram_X2X_SP_DW-1:0]), // Templated
					       .wstrb_s1	(AXI3__ROM__WSTRB[`axi2axi_cpu_sram_X2X_SP_SW-1:0]), // Templated
					       .wlast_s1	(AXI3__ROM__WLAST), // Templated
					       .bready_s1	(AXI3__ROM__BREADY), // Templated
					       .arvalid_s	(AXI3__ROM__ARVALID), // Templated
					       .arid_s		(AXI3__ROM__ARID[`axi2axi_cpu_sram_X2X_SP_IDW-1:0]), // Templated
					       .araddr_s	(AXI3__ROM__ARADDR[`axi2axi_cpu_sram_X2X_SP_AW-1:0]), // Templated
					       .arlen_s		(AXI3__ROM__ARLEN[`axi2axi_cpu_sram_X2X_SP_BLW-1:0]), // Templated
					       .arsize_s	(AXI3__ROM__ARSIZE[`axi2axi_cpu_sram_X2X_BSW-1:0]), // Templated
					       .arburst_s	(AXI3__ROM__ARBURST[`axi2axi_cpu_sram_X2X_BTW-1:0]), // Templated
					       .arlock_s	(AXI3__ROM__ARLOCK[`axi2axi_cpu_sram_X2X_LTW-1:0]), // Templated
					       .arcache_s	(AXI3__ROM__ARCACHE[`axi2axi_cpu_sram_X2X_CTW-1:0]), // Templated
					       .arprot_s	(AXI3__ROM__ARPROT[`axi2axi_cpu_sram_X2X_PTW-1:0]), // Templated
					       .rready_s	(AXI3__ROM__RREADY), // Templated
					       // Inputs
					       .aclk_m		(CLK__FBUS),	 // Templated
					       .aresetn_m	(RSTN__FBUS),	 // Templated
					       .awvalid_m	(AXI3__FBUS__TO_ROM__AWVALID), // Templated
					       .awaddr_m	(AXI3__FBUS__TO_ROM__AWADDR[`axi2axi_cpu_sram_X2X_MP_AW-1:0]), // Templated
					       .awid_m		(AXI3__FBUS__TO_ROM__AWID[`axi2axi_cpu_sram_X2X_MP_IDW-1:0]), // Templated
					       .awlen_m		(AXI3__FBUS__TO_ROM__AWLEN[`axi2axi_cpu_sram_X2X_MP_BLW-1:0]), // Templated
					       .awsize_m	(AXI3__FBUS__TO_ROM__AWSIZE[`axi2axi_cpu_sram_X2X_BSW-1:0]), // Templated
					       .awburst_m	(AXI3__FBUS__TO_ROM__AWBURST[`axi2axi_cpu_sram_X2X_BTW-1:0]), // Templated
					       .awlock_m	(AXI3__FBUS__TO_ROM__AWLOCK[`axi2axi_cpu_sram_X2X_LTW-1:0]), // Templated
					       .awcache_m	(AXI3__FBUS__TO_ROM__AWCACHE[`axi2axi_cpu_sram_X2X_CTW-1:0]), // Templated
					       .awprot_m	(AXI3__FBUS__TO_ROM__AWPROT[`axi2axi_cpu_sram_X2X_PTW-1:0]), // Templated
					       .wvalid_m	(AXI3__FBUS__TO_ROM__WVALID), // Templated
					       .wid_m		(AXI3__FBUS__TO_ROM__WID[`axi2axi_cpu_sram_X2X_MP_IDW-1:0]), // Templated
					       .wdata_m		(AXI3__FBUS__TO_ROM__WDATA[`axi2axi_cpu_sram_X2X_MP_DW-1:0]), // Templated
					       .wstrb_m		(AXI3__FBUS__TO_ROM__WSTRB[`axi2axi_cpu_sram_X2X_MP_SW-1:0]), // Templated
					       .wlast_m		(AXI3__FBUS__TO_ROM__WLAST), // Templated
					       .bready_m	(AXI3__FBUS__TO_ROM__BREADY), // Templated
					       .arvalid_m	(AXI3__FBUS__TO_ROM__ARVALID), // Templated
					       .arid_m		(AXI3__FBUS__TO_ROM__ARID[`axi2axi_cpu_sram_X2X_MP_IDW-1:0]), // Templated
					       .araddr_m	(AXI3__FBUS__TO_ROM__ARADDR[`axi2axi_cpu_sram_X2X_MP_AW-1:0]), // Templated
					       .arlen_m		(AXI3__FBUS__TO_ROM__ARLEN[`axi2axi_cpu_sram_X2X_MP_BLW-1:0]), // Templated
					       .arsize_m	(AXI3__FBUS__TO_ROM__ARSIZE[`axi2axi_cpu_sram_X2X_BSW-1:0]), // Templated
					       .arburst_m	(AXI3__FBUS__TO_ROM__ARBURST[`axi2axi_cpu_sram_X2X_BTW-1:0]), // Templated
					       .arlock_m	(AXI3__FBUS__TO_ROM__ARLOCK[`axi2axi_cpu_sram_X2X_LTW-1:0]), // Templated
					       .arcache_m	(AXI3__FBUS__TO_ROM__ARCACHE[`axi2axi_cpu_sram_X2X_CTW-1:0]), // Templated
					       .arprot_m	(AXI3__FBUS__TO_ROM__ARPROT[`axi2axi_cpu_sram_X2X_PTW-1:0]), // Templated
					       .rready_m	(AXI3__FBUS__TO_ROM__RREADY), // Templated
					       .aclk_s		(CLK__ROM),	 // Templated
					       .aresetn_s	(RSTN__ROM),	 // Templated
					       .awready_s1	(AXI3__ROM__AWREADY), // Templated
					       .wready_s1	(AXI3__ROM__WREADY), // Templated
					       .bvalid_s1	(AXI3__ROM__BVALID), // Templated
					       .bid_s1		(AXI3__ROM__BID[`axi2axi_cpu_sram_X2X_SP_IDW-1:0]), // Templated
					       .bresp_s1	(AXI3__ROM__BRESP[`axi2axi_cpu_sram_X2X_BRW-1:0]), // Templated
					       .arready_s	(AXI3__ROM__ARREADY), // Templated
					       .rvalid_s	(AXI3__ROM__RVALID), // Templated
					       .rid_s		(AXI3__ROM__RID[`axi2axi_cpu_sram_X2X_SP_IDW-1:0]), // Templated
					       .rdata_s		(AXI3__ROM__RDATA[`axi2axi_cpu_sram_X2X_SP_DW-1:0]), // Templated
					       .rlast_s		(AXI3__ROM__RLAST), // Templated
					       .rresp_s		(AXI3__ROM__RRESP[`axi2axi_cpu_sram_X2X_RRW-1:0])); // Templated

    /* axi3to4 AUTO_TEMPLATE "AXI3TO4_\(.*\)_0" (
        .CLK__BUS          (CLK__@),
        .RSTN__BUS          (RSTN__@),
        .AXI3__BUS__A.USER	 (),
        .AXI3__BUS__\(.*\)         (AXI3__@__\1[]),
        .AXI4__BUS__A.USER	 (),
        .AXI4__BUS__\(.*\)         (@_0__AXI4__@__\1[]),
    );*/
    axi3to4 #(.ADDR_WIDTH(SRAM_AW),
              .ID_WIDTH(SRAM_IW),
              .DATA_WIDTH(SRAM_DW),
              .USER_WIDTH(SRAM_UW),
              .LEN_WIDTH(8),
              .STRB_WIDTH(SRAM_SW)) AXI3TO4_SRAM_0 (/*AUTOINST*/
						    // Outputs
						    .AXI3__BUS__AWREADY	(AXI3__SRAM__AWREADY), // Templated
						    .AXI3__BUS__WREADY	(AXI3__SRAM__WREADY), // Templated
						    .AXI3__BUS__BID	(AXI3__SRAM__BID[SRAM_IW-1:0]), // Templated
						    .AXI3__BUS__BVALID	(AXI3__SRAM__BVALID), // Templated
						    .AXI3__BUS__BRESP	(AXI3__SRAM__BRESP[1:0]), // Templated
						    .AXI3__BUS__ARREADY	(AXI3__SRAM__ARREADY), // Templated
						    .AXI3__BUS__RID	(AXI3__SRAM__RID[SRAM_IW-1:0]), // Templated
						    .AXI3__BUS__RDATA	(AXI3__SRAM__RDATA[SRAM_DW-1:0]), // Templated
						    .AXI3__BUS__RRESP	(AXI3__SRAM__RRESP[1:0]), // Templated
						    .AXI3__BUS__RLAST	(AXI3__SRAM__RLAST), // Templated
						    .AXI3__BUS__RVALID	(AXI3__SRAM__RVALID), // Templated
						    .AXI4__BUS__AWADDR	(SRAM_0__AXI4__SRAM__AWADDR[SRAM_AW-1:0]), // Templated
						    .AXI4__BUS__AWID	(SRAM_0__AXI4__SRAM__AWID[SRAM_IW-1:0]), // Templated
						    .AXI4__BUS__AWLEN	(SRAM_0__AXI4__SRAM__AWLEN[7:0]), // Templated
						    .AXI4__BUS__AWSIZE	(SRAM_0__AXI4__SRAM__AWSIZE[2:0]), // Templated
						    .AXI4__BUS__AWBURST	(SRAM_0__AXI4__SRAM__AWBURST[1:0]), // Templated
						    .AXI4__BUS__AWLOCK	(SRAM_0__AXI4__SRAM__AWLOCK), // Templated
						    .AXI4__BUS__AWCACHE	(SRAM_0__AXI4__SRAM__AWCACHE[3:0]), // Templated
						    .AXI4__BUS__AWPROT	(SRAM_0__AXI4__SRAM__AWPROT[2:0]), // Templated
						    .AXI4__BUS__AWUSER	(),		 // Templated
						    .AXI4__BUS__AWVALID	(SRAM_0__AXI4__SRAM__AWVALID), // Templated
						    .AXI4__BUS__WDATA	(SRAM_0__AXI4__SRAM__WDATA[SRAM_DW-1:0]), // Templated
						    .AXI4__BUS__WSTRB	(SRAM_0__AXI4__SRAM__WSTRB[SRAM_SW-1:0]), // Templated
						    .AXI4__BUS__WLAST	(SRAM_0__AXI4__SRAM__WLAST), // Templated
						    .AXI4__BUS__WVALID	(SRAM_0__AXI4__SRAM__WVALID), // Templated
						    .AXI4__BUS__BREADY	(SRAM_0__AXI4__SRAM__BREADY), // Templated
						    .AXI4__BUS__ARID	(SRAM_0__AXI4__SRAM__ARID[SRAM_IW-1:0]), // Templated
						    .AXI4__BUS__ARADDR	(SRAM_0__AXI4__SRAM__ARADDR[SRAM_AW-1:0]), // Templated
						    .AXI4__BUS__ARLEN	(SRAM_0__AXI4__SRAM__ARLEN[7:0]), // Templated
						    .AXI4__BUS__ARSIZE	(SRAM_0__AXI4__SRAM__ARSIZE[2:0]), // Templated
						    .AXI4__BUS__ARBURST	(SRAM_0__AXI4__SRAM__ARBURST[1:0]), // Templated
						    .AXI4__BUS__ARLOCK	(SRAM_0__AXI4__SRAM__ARLOCK), // Templated
						    .AXI4__BUS__ARCACHE	(SRAM_0__AXI4__SRAM__ARCACHE[3:0]), // Templated
						    .AXI4__BUS__ARPROT	(SRAM_0__AXI4__SRAM__ARPROT[2:0]), // Templated
						    .AXI4__BUS__ARUSER	(),		 // Templated
						    .AXI4__BUS__ARVALID	(SRAM_0__AXI4__SRAM__ARVALID), // Templated
						    .AXI4__BUS__RREADY	(SRAM_0__AXI4__SRAM__RREADY), // Templated
						    // Inputs
						    .CLK__BUS		(CLK__SRAM),	 // Templated
						    .RSTN__BUS		(RSTN__SRAM),	 // Templated
						    .AXI3__BUS__AWADDR	(AXI3__SRAM__AWADDR[SRAM_AW-1:0]), // Templated
						    .AXI3__BUS__AWID	(AXI3__SRAM__AWID[SRAM_IW-1:0]), // Templated
						    .AXI3__BUS__AWLEN	(AXI3__SRAM__AWLEN[7:0]), // Templated
						    .AXI3__BUS__AWSIZE	(AXI3__SRAM__AWSIZE[2:0]), // Templated
						    .AXI3__BUS__AWBURST	(AXI3__SRAM__AWBURST[1:0]), // Templated
						    .AXI3__BUS__AWLOCK	(AXI3__SRAM__AWLOCK[1:0]), // Templated
						    .AXI3__BUS__AWCACHE	(AXI3__SRAM__AWCACHE[3:0]), // Templated
						    .AXI3__BUS__AWPROT	(AXI3__SRAM__AWPROT[2:0]), // Templated
						    .AXI3__BUS__AWUSER	(),		 // Templated
						    .AXI3__BUS__AWVALID	(AXI3__SRAM__AWVALID), // Templated
						    .AXI3__BUS__WID	(AXI3__SRAM__WID[SRAM_IW-1:0]), // Templated
						    .AXI3__BUS__WDATA	(AXI3__SRAM__WDATA[SRAM_DW-1:0]), // Templated
						    .AXI3__BUS__WSTRB	(AXI3__SRAM__WSTRB[SRAM_SW-1:0]), // Templated
						    .AXI3__BUS__WVALID	(AXI3__SRAM__WVALID), // Templated
						    .AXI3__BUS__WLAST	(AXI3__SRAM__WLAST), // Templated
						    .AXI3__BUS__BREADY	(AXI3__SRAM__BREADY), // Templated
						    .AXI3__BUS__ARID	(AXI3__SRAM__ARID[SRAM_IW-1:0]), // Templated
						    .AXI3__BUS__ARADDR	(AXI3__SRAM__ARADDR[SRAM_AW-1:0]), // Templated
						    .AXI3__BUS__ARLEN	(AXI3__SRAM__ARLEN[7:0]), // Templated
						    .AXI3__BUS__ARSIZE	(AXI3__SRAM__ARSIZE[2:0]), // Templated
						    .AXI3__BUS__ARBURST	(AXI3__SRAM__ARBURST[1:0]), // Templated
						    .AXI3__BUS__ARLOCK	(AXI3__SRAM__ARLOCK[1:0]), // Templated
						    .AXI3__BUS__ARCACHE	(AXI3__SRAM__ARCACHE[3:0]), // Templated
						    .AXI3__BUS__ARPROT	(AXI3__SRAM__ARPROT[2:0]), // Templated
						    .AXI3__BUS__ARUSER	(),		 // Templated
						    .AXI3__BUS__ARVALID	(AXI3__SRAM__ARVALID), // Templated
						    .AXI3__BUS__RREADY	(AXI3__SRAM__RREADY), // Templated
						    .AXI4__BUS__AWREADY	(SRAM_0__AXI4__SRAM__AWREADY), // Templated
						    .AXI4__BUS__WREADY	(SRAM_0__AXI4__SRAM__WREADY), // Templated
						    .AXI4__BUS__BID	(SRAM_0__AXI4__SRAM__BID[SRAM_IW-1:0]), // Templated
						    .AXI4__BUS__BRESP	(SRAM_0__AXI4__SRAM__BRESP[1:0]), // Templated
						    .AXI4__BUS__BVALID	(SRAM_0__AXI4__SRAM__BVALID), // Templated
						    .AXI4__BUS__ARREADY	(SRAM_0__AXI4__SRAM__ARREADY), // Templated
						    .AXI4__BUS__RID	(SRAM_0__AXI4__SRAM__RID[SRAM_IW-1:0]), // Templated
						    .AXI4__BUS__RDATA	(SRAM_0__AXI4__SRAM__RDATA[SRAM_DW-1:0]), // Templated
						    .AXI4__BUS__RRESP	(SRAM_0__AXI4__SRAM__RRESP[1:0]), // Templated
						    .AXI4__BUS__RLAST	(SRAM_0__AXI4__SRAM__RLAST), // Templated
						    .AXI4__BUS__RVALID	(SRAM_0__AXI4__SRAM__RVALID)); // Templated

    axi3to4 #(.ADDR_WIDTH(ROM_AW),
              .ID_WIDTH(ROM_IW),
              .DATA_WIDTH(ROM_DW),
              .USER_WIDTH(ROM_UW),
              .LEN_WIDTH(8),
              .STRB_WIDTH(ROM_SW)) AXI3TO4_ROM_0 (/*AUTOINST*/
						  // Outputs
						  .AXI3__BUS__AWREADY	(AXI3__ROM__AWREADY), // Templated
						  .AXI3__BUS__WREADY	(AXI3__ROM__WREADY), // Templated
						  .AXI3__BUS__BID	(AXI3__ROM__BID[ROM_IW-1:0]), // Templated
						  .AXI3__BUS__BVALID	(AXI3__ROM__BVALID), // Templated
						  .AXI3__BUS__BRESP	(AXI3__ROM__BRESP[1:0]), // Templated
						  .AXI3__BUS__ARREADY	(AXI3__ROM__ARREADY), // Templated
						  .AXI3__BUS__RID	(AXI3__ROM__RID[ROM_IW-1:0]), // Templated
						  .AXI3__BUS__RDATA	(AXI3__ROM__RDATA[ROM_DW-1:0]), // Templated
						  .AXI3__BUS__RRESP	(AXI3__ROM__RRESP[1:0]), // Templated
						  .AXI3__BUS__RLAST	(AXI3__ROM__RLAST), // Templated
						  .AXI3__BUS__RVALID	(AXI3__ROM__RVALID), // Templated
						  .AXI4__BUS__AWADDR	(ROM_0__AXI4__ROM__AWADDR[ROM_AW-1:0]), // Templated
						  .AXI4__BUS__AWID	(ROM_0__AXI4__ROM__AWID[ROM_IW-1:0]), // Templated
						  .AXI4__BUS__AWLEN	(ROM_0__AXI4__ROM__AWLEN[7:0]), // Templated
						  .AXI4__BUS__AWSIZE	(ROM_0__AXI4__ROM__AWSIZE[2:0]), // Templated
						  .AXI4__BUS__AWBURST	(ROM_0__AXI4__ROM__AWBURST[1:0]), // Templated
						  .AXI4__BUS__AWLOCK	(ROM_0__AXI4__ROM__AWLOCK), // Templated
						  .AXI4__BUS__AWCACHE	(ROM_0__AXI4__ROM__AWCACHE[3:0]), // Templated
						  .AXI4__BUS__AWPROT	(ROM_0__AXI4__ROM__AWPROT[2:0]), // Templated
						  .AXI4__BUS__AWUSER	(),		 // Templated
						  .AXI4__BUS__AWVALID	(ROM_0__AXI4__ROM__AWVALID), // Templated
						  .AXI4__BUS__WDATA	(ROM_0__AXI4__ROM__WDATA[ROM_DW-1:0]), // Templated
						  .AXI4__BUS__WSTRB	(ROM_0__AXI4__ROM__WSTRB[ROM_SW-1:0]), // Templated
						  .AXI4__BUS__WLAST	(ROM_0__AXI4__ROM__WLAST), // Templated
						  .AXI4__BUS__WVALID	(ROM_0__AXI4__ROM__WVALID), // Templated
						  .AXI4__BUS__BREADY	(ROM_0__AXI4__ROM__BREADY), // Templated
						  .AXI4__BUS__ARID	(ROM_0__AXI4__ROM__ARID[ROM_IW-1:0]), // Templated
						  .AXI4__BUS__ARADDR	(ROM_0__AXI4__ROM__ARADDR[ROM_AW-1:0]), // Templated
						  .AXI4__BUS__ARLEN	(ROM_0__AXI4__ROM__ARLEN[7:0]), // Templated
						  .AXI4__BUS__ARSIZE	(ROM_0__AXI4__ROM__ARSIZE[2:0]), // Templated
						  .AXI4__BUS__ARBURST	(ROM_0__AXI4__ROM__ARBURST[1:0]), // Templated
						  .AXI4__BUS__ARLOCK	(ROM_0__AXI4__ROM__ARLOCK), // Templated
						  .AXI4__BUS__ARCACHE	(ROM_0__AXI4__ROM__ARCACHE[3:0]), // Templated
						  .AXI4__BUS__ARPROT	(ROM_0__AXI4__ROM__ARPROT[2:0]), // Templated
						  .AXI4__BUS__ARUSER	(),		 // Templated
						  .AXI4__BUS__ARVALID	(ROM_0__AXI4__ROM__ARVALID), // Templated
						  .AXI4__BUS__RREADY	(ROM_0__AXI4__ROM__RREADY), // Templated
						  // Inputs
						  .CLK__BUS		(CLK__ROM),	 // Templated
						  .RSTN__BUS		(RSTN__ROM),	 // Templated
						  .AXI3__BUS__AWADDR	(AXI3__ROM__AWADDR[ROM_AW-1:0]), // Templated
						  .AXI3__BUS__AWID	(AXI3__ROM__AWID[ROM_IW-1:0]), // Templated
						  .AXI3__BUS__AWLEN	(AXI3__ROM__AWLEN[7:0]), // Templated
						  .AXI3__BUS__AWSIZE	(AXI3__ROM__AWSIZE[2:0]), // Templated
						  .AXI3__BUS__AWBURST	(AXI3__ROM__AWBURST[1:0]), // Templated
						  .AXI3__BUS__AWLOCK	(AXI3__ROM__AWLOCK[1:0]), // Templated
						  .AXI3__BUS__AWCACHE	(AXI3__ROM__AWCACHE[3:0]), // Templated
						  .AXI3__BUS__AWPROT	(AXI3__ROM__AWPROT[2:0]), // Templated
						  .AXI3__BUS__AWUSER	(),		 // Templated
						  .AXI3__BUS__AWVALID	(AXI3__ROM__AWVALID), // Templated
						  .AXI3__BUS__WID	(AXI3__ROM__WID[ROM_IW-1:0]), // Templated
						  .AXI3__BUS__WDATA	(AXI3__ROM__WDATA[ROM_DW-1:0]), // Templated
						  .AXI3__BUS__WSTRB	(AXI3__ROM__WSTRB[ROM_SW-1:0]), // Templated
						  .AXI3__BUS__WVALID	(AXI3__ROM__WVALID), // Templated
						  .AXI3__BUS__WLAST	(AXI3__ROM__WLAST), // Templated
						  .AXI3__BUS__BREADY	(AXI3__ROM__BREADY), // Templated
						  .AXI3__BUS__ARID	(AXI3__ROM__ARID[ROM_IW-1:0]), // Templated
						  .AXI3__BUS__ARADDR	(AXI3__ROM__ARADDR[ROM_AW-1:0]), // Templated
						  .AXI3__BUS__ARLEN	(AXI3__ROM__ARLEN[7:0]), // Templated
						  .AXI3__BUS__ARSIZE	(AXI3__ROM__ARSIZE[2:0]), // Templated
						  .AXI3__BUS__ARBURST	(AXI3__ROM__ARBURST[1:0]), // Templated
						  .AXI3__BUS__ARLOCK	(AXI3__ROM__ARLOCK[1:0]), // Templated
						  .AXI3__BUS__ARCACHE	(AXI3__ROM__ARCACHE[3:0]), // Templated
						  .AXI3__BUS__ARPROT	(AXI3__ROM__ARPROT[2:0]), // Templated
						  .AXI3__BUS__ARUSER	(),		 // Templated
						  .AXI3__BUS__ARVALID	(AXI3__ROM__ARVALID), // Templated
						  .AXI3__BUS__RREADY	(AXI3__ROM__RREADY), // Templated
						  .AXI4__BUS__AWREADY	(ROM_0__AXI4__ROM__AWREADY), // Templated
						  .AXI4__BUS__WREADY	(ROM_0__AXI4__ROM__WREADY), // Templated
						  .AXI4__BUS__BID	(ROM_0__AXI4__ROM__BID[ROM_IW-1:0]), // Templated
						  .AXI4__BUS__BRESP	(ROM_0__AXI4__ROM__BRESP[1:0]), // Templated
						  .AXI4__BUS__BVALID	(ROM_0__AXI4__ROM__BVALID), // Templated
						  .AXI4__BUS__ARREADY	(ROM_0__AXI4__ROM__ARREADY), // Templated
						  .AXI4__BUS__RID	(ROM_0__AXI4__ROM__RID[ROM_IW-1:0]), // Templated
						  .AXI4__BUS__RDATA	(ROM_0__AXI4__ROM__RDATA[ROM_DW-1:0]), // Templated
						  .AXI4__BUS__RRESP	(ROM_0__AXI4__ROM__RRESP[1:0]), // Templated
						  .AXI4__BUS__RLAST	(ROM_0__AXI4__ROM__RLAST), // Templated
						  .AXI4__BUS__RVALID	(ROM_0__AXI4__ROM__RVALID)); // Templated
`include "./axi_2x3_cpu_fbus/axi_2x3_cpu_fbus_DW_axi-undef.v"
`include "./axi2axi_cpu_sram/axi2axi_cpu_sram_DW_axi_x2x-undef.v"
endmodule
// Local Variables:
// verilog-library-directories:(".")
// verilog-auto-inst-param-value:t
// verilog-library-files:("../../../Shared/rtl.src/bus_components/axi_converter/axi3to4.v" "../../../Shared/rtl.src/bus_components/axi_converter/axi4to3.v" "axi2axi_cpu_sram/axi2axi_cpu_sram_DW_axi_x2x.v" "axi_2x3_cpu_fbus/axi_2x3_cpu_fbus_DW_axi.v")
// End:
// vim:tabstop=4:shiftwidth=4
