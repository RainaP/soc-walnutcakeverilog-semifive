`ifdef DDR_EMPTY
`else
module lpddr4_wrapper(
  LPDDR4_itf.port  LPDDR4
);

  pullup(LPDDR4.ZQ);

  LPDDRIV inst0(
    .clk_a      (LPDDR4.CK),
    .clkb_a     (LPDDR4.CKB),
    .cke_a      (LPDDR4.CKE[0]),
    .cs_a       (LPDDR4.CS[0]),
    .ca_a       (LPDDR4.ADCT[5:0]),
    .odt_ca_a   (LPDDR4.ODT[0]),
    .dq_a       (LPDDR4.DQ_A[15:0]),
    .dbi_a      (LPDDR4.DM_A[1:0]),
    .dqs_a      (LPDDR4.PDQS_A[1:0]),
    .dqsb_a     (LPDDR4.NDQS_A[1:0]),
    .clk_b      (LPDDR4.CK),
    .clkb_b     (LPDDR4.CKB),
    .cke_b      (LPDDR4.CKE[0]),
    .cs_b       (LPDDR4.CS[0]),
    .ca_b       (LPDDR4.ADCT[5:0]),
    .odt_ca_b   (LPDDR4.ODT[1]),
    .dq_b       (LPDDR4.DQ_B[15:0]),
    .dbi_b      (LPDDR4.DM_B[1:0]),
    .dqs_b      (LPDDR4.PDQS_B[1:0]),
    .dqsb_b     (LPDDR4.NDQS_B[1:0]),
    .resetb     (LPDDR4.RESET),
    .zq         (LPDDR4.ZQ)
  );

  LPDDRIV inst1(
    .clk_a      (LPDDR4.CK),
    .clkb_a     (LPDDR4.CKB),
    .cke_a      (LPDDR4.CKE[0]),
    .cs_a       (LPDDR4.CS[1]),
    .ca_a       (LPDDR4.ADCT[5:0]),
    .odt_ca_a   (LPDDR4.ODT[0]),
    .dq_a       (LPDDR4.DQ_A[15:0]),
    .dbi_a      (LPDDR4.DM_A[1:0]),
    .dqs_a      (LPDDR4.PDQS_A[1:0]),
    .dqsb_a     (LPDDR4.NDQS_A[1:0]),
    .clk_b      (LPDDR4.CK),
    .clkb_b     (LPDDR4.CKB),
    .cke_b      (LPDDR4.CKE[0]),
    .cs_b       (LPDDR4.CS[1]),
    .ca_b       (LPDDR4.ADCT[5:0]),
    .odt_ca_b   (LPDDR4.ODT[1]),
    .dq_b       (LPDDR4.DQ_B[15:0]),
    .dbi_b      (LPDDR4.DM_B[1:0]),
    .dqs_b      (LPDDR4.PDQS_B[1:0]),
    .dqsb_b     (LPDDR4.NDQS_B[1:0]),
    .resetb     (LPDDR4.RESET),
    .zq         (LPDDR4.ZQ)
  );

endmodule
`endif
