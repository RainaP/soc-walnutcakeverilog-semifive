module pll1426xe_wrap (/*AUTOARG*/
   // Outputs
   CLK__FOUT, TEST__PLL_OUT, REG__AFC_CODE, REG__LOCK, REG__FEED_OUT,
   // Inputs
   CLK__FIN, TEST__SCAN, TEST__PLL, TEST__PLL_IN, REG__P, REG__M,
   REG__S, REG__ICP, REG__RSEL, REG__EXTAFC, REG__LOCK_CON_DLY,
   REG__LOCK_CON_REV, REG__LOCK_CON_IN, REG__LOCK_CON_OUT,
   REG__RESETB, REG__BYPASS, REG__AFC_ENB, REG__FSEL, REG__FEED_EN,
   REG__LOCK_EN, REG__AFCINIT_SEL, REG__LRD_EN, REG__FOUT_MASK,
   REG__VCO_BOOST, REG__PBIAS_CTRL_EN, REG__PBIAS_CTRL
   );

   input        CLK__FIN;
   output       CLK__FOUT;
input           TEST__SCAN;
input           TEST__PLL;
input  [52:0]   TEST__PLL_IN;
output [7:0]    TEST__PLL_OUT;
input  [5:0]  REG__P; 
input  [9:0]  REG__M; 
input  [2:0]  REG__S; 
input  [1:0]  REG__ICP; 
input  [3:0]  REG__RSEL; 
input  [4:0]  REG__EXTAFC; 
input  [1:0]  REG__LOCK_CON_DLY; 
input  [1:0]  REG__LOCK_CON_REV; 
input  [1:0]  REG__LOCK_CON_IN; 
input  [1:0]  REG__LOCK_CON_OUT; 
input         REG__RESETB; 
input         REG__BYPASS; 
input         REG__AFC_ENB; 
input         REG__FSEL; 
input         REG__FEED_EN; 
input         REG__LOCK_EN; 
input         REG__AFCINIT_SEL; 
input         REG__LRD_EN; 
input         REG__FOUT_MASK; 
input         REG__VCO_BOOST; 
input         REG__PBIAS_CTRL_EN; 
input         REG__PBIAS_CTRL; 
output [4:0]  REG__AFC_CODE;
output        REG__LOCK;
output        REG__FEED_OUT;

/*AUTOWIRE*/
// Beginning of automatic wires (for undeclared instantiated-module outputs)
wire			PLL__AFCINIT_SEL;	// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire [4:0]		PLL__AFC_CODE;		// From uPLL of sf_pll1426xe_ln14lpp_32422_v.v
wire			PLL__AFC_ENB;		// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire			PLL__BYPASS;		// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire [4:0]		PLL__EXTAFC;		// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire			PLL__FEED_EN;		// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire			PLL__FEED_OUT;		// From uPLL of sf_pll1426xe_ln14lpp_32422_v.v
wire			PLL__FOUT_MASK;		// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire			PLL__FSEL;		// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire [1:0]		PLL__ICP;		// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire			PLL__LOCK;		// From uPLL of sf_pll1426xe_ln14lpp_32422_v.v
wire [1:0]		PLL__LOCK_CON_DLY;	// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire [1:0]		PLL__LOCK_CON_IN;	// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire [1:0]		PLL__LOCK_CON_OUT;	// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire [1:0]		PLL__LOCK_CON_REV;	// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire			PLL__LOCK_EN;		// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire			PLL__LRD_EN;		// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire [9:0]		PLL__M;			// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire [5:0]		PLL__P;			// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire			PLL__PBIAS_CTRL;	// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire			PLL__PBIAS_CTRL_EN;	// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire			PLL__RESETB;		// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire [3:0]		PLL__RSEL;		// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire [2:0]		PLL__S;			// From uTMUX_0 of pll1426xe_wrap_testmux.v
wire			PLL__VCO_BOOST;		// From uTMUX_0 of pll1426xe_wrap_testmux.v
// End of automatics
/*AUTOREG*/
/*pll1426xe_wrap_testmux AUTO_TEMPLATE (
    .TEST__OUT	(TEST__PLL_OUT[]),
    .TEST__IN 	(TEST__PLL_IN[]),
);
*/
pll1426xe_wrap_testmux uTMUX_0 (/*AUTOINST*/
				// Outputs
				.TEST__OUT	(TEST__PLL_OUT[7:0]), // Templated
				.REG__LOCK	(REG__LOCK),
				.REG__FEED_OUT	(REG__FEED_OUT),
				.REG__AFC_CODE	(REG__AFC_CODE[4:0]),
				.PLL__P		(PLL__P[5:0]),
				.PLL__M		(PLL__M[9:0]),
				.PLL__S		(PLL__S[2:0]),
				.PLL__ICP	(PLL__ICP[1:0]),
				.PLL__RSEL	(PLL__RSEL[3:0]),
				.PLL__EXTAFC	(PLL__EXTAFC[4:0]),
				.PLL__LOCK_CON_DLY(PLL__LOCK_CON_DLY[1:0]),
				.PLL__LOCK_CON_REV(PLL__LOCK_CON_REV[1:0]),
				.PLL__LOCK_CON_IN(PLL__LOCK_CON_IN[1:0]),
				.PLL__LOCK_CON_OUT(PLL__LOCK_CON_OUT[1:0]),
				.PLL__RESETB	(PLL__RESETB),
				.PLL__BYPASS	(PLL__BYPASS),
				.PLL__AFC_ENB	(PLL__AFC_ENB),
				.PLL__FSEL	(PLL__FSEL),
				.PLL__FEED_EN	(PLL__FEED_EN),
				.PLL__LOCK_EN	(PLL__LOCK_EN),
				.PLL__AFCINIT_SEL(PLL__AFCINIT_SEL),
				.PLL__LRD_EN	(PLL__LRD_EN),
				.PLL__FOUT_MASK	(PLL__FOUT_MASK),
				.PLL__VCO_BOOST	(PLL__VCO_BOOST),
				.PLL__PBIAS_CTRL_EN(PLL__PBIAS_CTRL_EN),
				.PLL__PBIAS_CTRL(PLL__PBIAS_CTRL),
				// Inputs
				.TEST__SCAN	(TEST__SCAN),
				.TEST__PLL	(TEST__PLL),
				.TEST__IN	(TEST__PLL_IN[52:0]), // Templated
				.REG__P		(REG__P[5:0]),
				.REG__M		(REG__M[9:0]),
				.REG__S		(REG__S[2:0]),
				.REG__ICP	(REG__ICP[1:0]),
				.REG__RSEL	(REG__RSEL[3:0]),
				.REG__EXTAFC	(REG__EXTAFC[4:0]),
				.REG__LOCK_CON_DLY(REG__LOCK_CON_DLY[1:0]),
				.REG__LOCK_CON_REV(REG__LOCK_CON_REV[1:0]),
				.REG__LOCK_CON_IN(REG__LOCK_CON_IN[1:0]),
				.REG__LOCK_CON_OUT(REG__LOCK_CON_OUT[1:0]),
				.REG__RESETB	(REG__RESETB),
				.REG__BYPASS	(REG__BYPASS),
				.REG__AFC_ENB	(REG__AFC_ENB),
				.REG__FSEL	(REG__FSEL),
				.REG__FEED_EN	(REG__FEED_EN),
				.REG__LOCK_EN	(REG__LOCK_EN),
				.REG__AFCINIT_SEL(REG__AFCINIT_SEL),
				.REG__LRD_EN	(REG__LRD_EN),
				.REG__FOUT_MASK	(REG__FOUT_MASK),
				.REG__VCO_BOOST	(REG__VCO_BOOST),
				.REG__PBIAS_CTRL_EN(REG__PBIAS_CTRL_EN),
				.REG__PBIAS_CTRL(REG__PBIAS_CTRL),
				.PLL__LOCK	(PLL__LOCK),
				.PLL__AFC_CODE	(PLL__AFC_CODE[4:0]),
				.PLL__FEED_OUT	(PLL__FEED_OUT),
				.CLK__FOUT	(CLK__FOUT));

`ifdef FPGA
assign PLL__AFC_CODE = 5'h0;
assign PLL__FEED_OUT = 1'b0;
assign PLL__LOCK = 1'b1;
assign CLK__FOUT = CLK__FIN;
`else

/*sf_pll1426xe_ln14lpp_32422_v  AUTO_TEMPLATE (
    .FIN                (CLK__FIN),
    .FOUT               (CLK__FOUT),
    .\(.*\)             (PLL__\1[]),
);
*/
sf_pll1426xe_ln14lpp_32422_v  uPLL (/*AUTOINST*/
				    // Outputs
				    .AFC_CODE		(PLL__AFC_CODE[4:0]), // Templated
				    .FEED_OUT		(PLL__FEED_OUT), // Templated
				    .LOCK		(PLL__LOCK),	 // Templated
				    .FOUT		(CLK__FOUT),	 // Templated
				    // Inputs
				    .P			(PLL__P[5:0]),	 // Templated
				    .M			(PLL__M[9:0]),	 // Templated
				    .S			(PLL__S[2:0]),	 // Templated
				    .ICP		(PLL__ICP[1:0]), // Templated
				    .RSEL		(PLL__RSEL[3:0]), // Templated
				    .EXTAFC		(PLL__EXTAFC[4:0]), // Templated
				    .LOCK_CON_DLY	(PLL__LOCK_CON_DLY[1:0]), // Templated
				    .LOCK_CON_REV	(PLL__LOCK_CON_REV[1:0]), // Templated
				    .LOCK_CON_IN	(PLL__LOCK_CON_IN[1:0]), // Templated
				    .LOCK_CON_OUT	(PLL__LOCK_CON_OUT[1:0]), // Templated
				    .FIN		(CLK__FIN),	 // Templated
				    .RESETB		(PLL__RESETB),	 // Templated
				    .BYPASS		(PLL__BYPASS),	 // Templated
				    .AFC_ENB		(PLL__AFC_ENB),	 // Templated
				    .FSEL		(PLL__FSEL),	 // Templated
				    .FEED_EN		(PLL__FEED_EN),	 // Templated
				    .LOCK_EN		(PLL__LOCK_EN),	 // Templated
				    .AFCINIT_SEL	(PLL__AFCINIT_SEL), // Templated
				    .LRD_EN		(PLL__LRD_EN),	 // Templated
				    .FOUT_MASK		(PLL__FOUT_MASK), // Templated
				    .VCO_BOOST		(PLL__VCO_BOOST), // Templated
				    .PBIAS_CTRL_EN	(PLL__PBIAS_CTRL_EN), // Templated
				    .PBIAS_CTRL		(PLL__PBIAS_CTRL)); // Templated
`endif

endmodule
// Local Variables:
// verilog-library-directories:(".")
// verilog-auto-inst-param-value:t
// verilog-library-files:("./pll1426xe_wrap_testmux.v" "../../mdl.src/sf_pll1426xe_ln14lpp_32422_v.vcs")
// End:
// vim:tabstop=4:shiftwidth=4
