//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__Queue_49(
  input         rf_reset,
  input         clock,
  input         reset,
  output        io_enq_ready,
  input         io_enq_valid,
  input  [31:0] io_enq_bits_addr,
  input  [7:0]  io_enq_bits_len,
  input  [2:0]  io_enq_bits_size,
  input  [3:0]  io_enq_bits_cache,
  input  [2:0]  io_enq_bits_prot,
  input  [3:0]  io_enq_bits_echo_tl_state_size,
  input  [10:0] io_enq_bits_echo_tl_state_source,
  input         io_enq_bits_wen,
  input         io_deq_ready,
  output        io_deq_valid,
  output [31:0] io_deq_bits_addr,
  output [7:0]  io_deq_bits_len,
  output [2:0]  io_deq_bits_size,
  output [3:0]  io_deq_bits_cache,
  output [2:0]  io_deq_bits_prot,
  output [3:0]  io_deq_bits_echo_tl_state_size,
  output [10:0] io_deq_bits_echo_tl_state_source,
  output        io_deq_bits_wen
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
`endif // RANDOMIZE_REG_INIT
  reg [31:0] ram_0_addr; // @[Decoupled.scala 218:16]
  reg [7:0] ram_0_len; // @[Decoupled.scala 218:16]
  reg [2:0] ram_0_size; // @[Decoupled.scala 218:16]
  reg [3:0] ram_0_cache; // @[Decoupled.scala 218:16]
  reg [2:0] ram_0_prot; // @[Decoupled.scala 218:16]
  reg [3:0] ram_0_echo_tl_state_size; // @[Decoupled.scala 218:16]
  reg [10:0] ram_0_echo_tl_state_source; // @[Decoupled.scala 218:16]
  reg  ram_0_wen; // @[Decoupled.scala 218:16]
  reg  maybe_full; // @[Decoupled.scala 221:27]
  wire  empty = ~maybe_full; // @[Decoupled.scala 224:28]
  wire  _do_enq_T = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  wire  _GEN_66 = io_deq_ready ? 1'h0 : _do_enq_T; // @[Decoupled.scala 249:27 Decoupled.scala 249:36]
  wire  do_enq = empty ? _GEN_66 : _do_enq_T; // @[Decoupled.scala 246:18]
  wire  _do_deq_T = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  wire  do_deq = empty ? 1'h0 : _do_deq_T; // @[Decoupled.scala 246:18 Decoupled.scala 248:14]
  assign io_enq_ready = ~maybe_full; // @[Decoupled.scala 241:19]
  assign io_deq_valid = io_enq_valid | ~empty; // @[Decoupled.scala 245:25 Decoupled.scala 245:40 Decoupled.scala 240:16]
  assign io_deq_bits_addr = empty ? io_enq_bits_addr : ram_0_addr; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_len = empty ? io_enq_bits_len : ram_0_len; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_size = empty ? io_enq_bits_size : ram_0_size; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_cache = empty ? io_enq_bits_cache : ram_0_cache; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_prot = empty ? io_enq_bits_prot : ram_0_prot; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_echo_tl_state_size = empty ? io_enq_bits_echo_tl_state_size : ram_0_echo_tl_state_size; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_echo_tl_state_source = empty ? io_enq_bits_echo_tl_state_source : ram_0_echo_tl_state_source; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  assign io_deq_bits_wen = empty ? io_enq_bits_wen : ram_0_wen; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_addr <= 32'h0;
    end else if (do_enq) begin
      ram_0_addr <= io_enq_bits_addr;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_len <= 8'h0;
    end else if (do_enq) begin
      ram_0_len <= io_enq_bits_len;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_size <= 3'h0;
    end else if (do_enq) begin
      ram_0_size <= io_enq_bits_size;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_cache <= 4'h0;
    end else if (do_enq) begin
      ram_0_cache <= io_enq_bits_cache;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_prot <= 3'h0;
    end else if (do_enq) begin
      ram_0_prot <= io_enq_bits_prot;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_echo_tl_state_size <= 4'h0;
    end else if (do_enq) begin
      ram_0_echo_tl_state_size <= io_enq_bits_echo_tl_state_size;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_echo_tl_state_source <= 11'h0;
    end else if (do_enq) begin
      ram_0_echo_tl_state_source <= io_enq_bits_echo_tl_state_source;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_wen <= 1'h0;
    end else if (do_enq) begin
      ram_0_wen <= io_enq_bits_wen;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      maybe_full <= 1'h0;
    end else if (do_enq != do_deq) begin
      if (empty) begin
        if (io_deq_ready) begin
          maybe_full <= 1'h0;
        end else begin
          maybe_full <= _do_enq_T;
        end
      end else begin
        maybe_full <= _do_enq_T;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ram_0_addr = _RAND_0[31:0];
  _RAND_1 = {1{`RANDOM}};
  ram_0_len = _RAND_1[7:0];
  _RAND_2 = {1{`RANDOM}};
  ram_0_size = _RAND_2[2:0];
  _RAND_3 = {1{`RANDOM}};
  ram_0_cache = _RAND_3[3:0];
  _RAND_4 = {1{`RANDOM}};
  ram_0_prot = _RAND_4[2:0];
  _RAND_5 = {1{`RANDOM}};
  ram_0_echo_tl_state_size = _RAND_5[3:0];
  _RAND_6 = {1{`RANDOM}};
  ram_0_echo_tl_state_source = _RAND_6[10:0];
  _RAND_7 = {1{`RANDOM}};
  ram_0_wen = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  maybe_full = _RAND_8[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ram_0_addr = 32'h0;
  end
  if (rf_reset) begin
    ram_0_len = 8'h0;
  end
  if (rf_reset) begin
    ram_0_size = 3'h0;
  end
  if (rf_reset) begin
    ram_0_cache = 4'h0;
  end
  if (rf_reset) begin
    ram_0_prot = 3'h0;
  end
  if (rf_reset) begin
    ram_0_echo_tl_state_size = 4'h0;
  end
  if (rf_reset) begin
    ram_0_echo_tl_state_source = 11'h0;
  end
  if (rf_reset) begin
    ram_0_wen = 1'h0;
  end
  if (reset) begin
    maybe_full = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
