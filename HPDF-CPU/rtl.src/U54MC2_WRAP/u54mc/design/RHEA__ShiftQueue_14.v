//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__ShiftQueue_14(
  input         rf_reset,
  input         clock,
  input         reset,
  output        io_enq_ready,
  input         io_enq_valid,
  input  [31:0] io_enq_bits,
  input         io_deq_ready,
  output        io_deq_valid,
  output [31:0] io_deq_bits
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
`endif // RANDOMIZE_REG_INIT
  reg  valid_0; // @[ShiftQueue.scala 21:30]
  reg  valid_1; // @[ShiftQueue.scala 21:30]
  reg  valid_2; // @[ShiftQueue.scala 21:30]
  reg  valid_3; // @[ShiftQueue.scala 21:30]
  reg  valid_4; // @[ShiftQueue.scala 21:30]
  reg  valid_5; // @[ShiftQueue.scala 21:30]
  reg  valid_6; // @[ShiftQueue.scala 21:30]
  reg  valid_7; // @[ShiftQueue.scala 21:30]
  reg [31:0] elts_0; // @[ShiftQueue.scala 22:25]
  reg [31:0] elts_1; // @[ShiftQueue.scala 22:25]
  reg [31:0] elts_2; // @[ShiftQueue.scala 22:25]
  reg [31:0] elts_3; // @[ShiftQueue.scala 22:25]
  reg [31:0] elts_4; // @[ShiftQueue.scala 22:25]
  reg [31:0] elts_5; // @[ShiftQueue.scala 22:25]
  reg [31:0] elts_6; // @[ShiftQueue.scala 22:25]
  reg [31:0] elts_7; // @[ShiftQueue.scala 22:25]
  wire  _wen_T = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  wire  _wen_T_3 = valid_1 | _wen_T; // @[ShiftQueue.scala 30:28]
  wire  _wen_T_7 = _wen_T & ~valid_0; // @[ShiftQueue.scala 31:45]
  wire  wen = io_deq_ready ? _wen_T_3 : _wen_T_7; // @[ShiftQueue.scala 29:10]
  wire  _valid_0_T_6 = _wen_T | valid_0; // @[ShiftQueue.scala 37:45]
  wire  _wen_T_10 = _wen_T & valid_1; // @[ShiftQueue.scala 30:45]
  wire  _wen_T_11 = valid_2 | _wen_T & valid_1; // @[ShiftQueue.scala 30:28]
  wire  _wen_T_13 = _wen_T & valid_0; // @[ShiftQueue.scala 31:25]
  wire  _wen_T_15 = _wen_T & valid_0 & ~valid_1; // @[ShiftQueue.scala 31:45]
  wire  wen_1 = io_deq_ready ? _wen_T_11 : _wen_T_15; // @[ShiftQueue.scala 29:10]
  wire  _valid_1_T_6 = _wen_T_13 | valid_1; // @[ShiftQueue.scala 37:45]
  wire  _wen_T_18 = _wen_T & valid_2; // @[ShiftQueue.scala 30:45]
  wire  _wen_T_19 = valid_3 | _wen_T & valid_2; // @[ShiftQueue.scala 30:28]
  wire  _wen_T_23 = _wen_T_10 & ~valid_2; // @[ShiftQueue.scala 31:45]
  wire  wen_2 = io_deq_ready ? _wen_T_19 : _wen_T_23; // @[ShiftQueue.scala 29:10]
  wire  _valid_2_T_6 = _wen_T_10 | valid_2; // @[ShiftQueue.scala 37:45]
  wire  _wen_T_26 = _wen_T & valid_3; // @[ShiftQueue.scala 30:45]
  wire  _wen_T_27 = valid_4 | _wen_T & valid_3; // @[ShiftQueue.scala 30:28]
  wire  _wen_T_31 = _wen_T_18 & ~valid_3; // @[ShiftQueue.scala 31:45]
  wire  wen_3 = io_deq_ready ? _wen_T_27 : _wen_T_31; // @[ShiftQueue.scala 29:10]
  wire  _valid_3_T_6 = _wen_T_18 | valid_3; // @[ShiftQueue.scala 37:45]
  wire  _wen_T_34 = _wen_T & valid_4; // @[ShiftQueue.scala 30:45]
  wire  _wen_T_35 = valid_5 | _wen_T & valid_4; // @[ShiftQueue.scala 30:28]
  wire  _wen_T_39 = _wen_T_26 & ~valid_4; // @[ShiftQueue.scala 31:45]
  wire  wen_4 = io_deq_ready ? _wen_T_35 : _wen_T_39; // @[ShiftQueue.scala 29:10]
  wire  _valid_4_T_6 = _wen_T_26 | valid_4; // @[ShiftQueue.scala 37:45]
  wire  _wen_T_42 = _wen_T & valid_5; // @[ShiftQueue.scala 30:45]
  wire  _wen_T_43 = valid_6 | _wen_T & valid_5; // @[ShiftQueue.scala 30:28]
  wire  _wen_T_47 = _wen_T_34 & ~valid_5; // @[ShiftQueue.scala 31:45]
  wire  wen_5 = io_deq_ready ? _wen_T_43 : _wen_T_47; // @[ShiftQueue.scala 29:10]
  wire  _valid_5_T_6 = _wen_T_34 | valid_5; // @[ShiftQueue.scala 37:45]
  wire  _wen_T_50 = _wen_T & valid_6; // @[ShiftQueue.scala 30:45]
  wire  _wen_T_51 = valid_7 | _wen_T & valid_6; // @[ShiftQueue.scala 30:28]
  wire  _wen_T_55 = _wen_T_42 & ~valid_6; // @[ShiftQueue.scala 31:45]
  wire  wen_6 = io_deq_ready ? _wen_T_51 : _wen_T_55; // @[ShiftQueue.scala 29:10]
  wire  _valid_6_T_6 = _wen_T_42 | valid_6; // @[ShiftQueue.scala 37:45]
  wire  _wen_T_58 = _wen_T & valid_7; // @[ShiftQueue.scala 30:45]
  wire  _wen_T_63 = _wen_T_50 & ~valid_7; // @[ShiftQueue.scala 31:45]
  wire  wen_7 = io_deq_ready ? _wen_T_58 : _wen_T_63; // @[ShiftQueue.scala 29:10]
  wire  _valid_7_T_6 = _wen_T_50 | valid_7; // @[ShiftQueue.scala 37:45]
  assign io_enq_ready = ~valid_7; // @[ShiftQueue.scala 40:19]
  assign io_deq_valid = valid_0; // @[ShiftQueue.scala 41:16]
  assign io_deq_bits = elts_0; // @[ShiftQueue.scala 42:15]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid_0 <= 1'h0;
    end else if (io_deq_ready) begin
      valid_0 <= _wen_T_3;
    end else begin
      valid_0 <= _valid_0_T_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid_1 <= 1'h0;
    end else if (io_deq_ready) begin
      valid_1 <= _wen_T_11;
    end else begin
      valid_1 <= _valid_1_T_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid_2 <= 1'h0;
    end else if (io_deq_ready) begin
      valid_2 <= _wen_T_19;
    end else begin
      valid_2 <= _valid_2_T_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid_3 <= 1'h0;
    end else if (io_deq_ready) begin
      valid_3 <= _wen_T_27;
    end else begin
      valid_3 <= _valid_3_T_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid_4 <= 1'h0;
    end else if (io_deq_ready) begin
      valid_4 <= _wen_T_35;
    end else begin
      valid_4 <= _valid_4_T_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid_5 <= 1'h0;
    end else if (io_deq_ready) begin
      valid_5 <= _wen_T_43;
    end else begin
      valid_5 <= _valid_5_T_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid_6 <= 1'h0;
    end else if (io_deq_ready) begin
      valid_6 <= _wen_T_51;
    end else begin
      valid_6 <= _valid_6_T_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid_7 <= 1'h0;
    end else if (io_deq_ready) begin
      valid_7 <= _wen_T_58;
    end else begin
      valid_7 <= _valid_7_T_6;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_0 <= 32'h0;
    end else if (wen) begin
      if (valid_1) begin
        elts_0 <= elts_1;
      end else begin
        elts_0 <= io_enq_bits;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_1 <= 32'h0;
    end else if (wen_1) begin
      if (valid_2) begin
        elts_1 <= elts_2;
      end else begin
        elts_1 <= io_enq_bits;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_2 <= 32'h0;
    end else if (wen_2) begin
      if (valid_3) begin
        elts_2 <= elts_3;
      end else begin
        elts_2 <= io_enq_bits;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_3 <= 32'h0;
    end else if (wen_3) begin
      if (valid_4) begin
        elts_3 <= elts_4;
      end else begin
        elts_3 <= io_enq_bits;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_4 <= 32'h0;
    end else if (wen_4) begin
      if (valid_5) begin
        elts_4 <= elts_5;
      end else begin
        elts_4 <= io_enq_bits;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_5 <= 32'h0;
    end else if (wen_5) begin
      if (valid_6) begin
        elts_5 <= elts_6;
      end else begin
        elts_5 <= io_enq_bits;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_6 <= 32'h0;
    end else if (wen_6) begin
      if (valid_7) begin
        elts_6 <= elts_7;
      end else begin
        elts_6 <= io_enq_bits;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_7 <= 32'h0;
    end else if (wen_7) begin
      elts_7 <= io_enq_bits;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  valid_0 = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  valid_1 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  valid_2 = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  valid_3 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  valid_4 = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  valid_5 = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  valid_6 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  valid_7 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  elts_0 = _RAND_8[31:0];
  _RAND_9 = {1{`RANDOM}};
  elts_1 = _RAND_9[31:0];
  _RAND_10 = {1{`RANDOM}};
  elts_2 = _RAND_10[31:0];
  _RAND_11 = {1{`RANDOM}};
  elts_3 = _RAND_11[31:0];
  _RAND_12 = {1{`RANDOM}};
  elts_4 = _RAND_12[31:0];
  _RAND_13 = {1{`RANDOM}};
  elts_5 = _RAND_13[31:0];
  _RAND_14 = {1{`RANDOM}};
  elts_6 = _RAND_14[31:0];
  _RAND_15 = {1{`RANDOM}};
  elts_7 = _RAND_15[31:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    valid_0 = 1'h0;
  end
  if (reset) begin
    valid_1 = 1'h0;
  end
  if (reset) begin
    valid_2 = 1'h0;
  end
  if (reset) begin
    valid_3 = 1'h0;
  end
  if (reset) begin
    valid_4 = 1'h0;
  end
  if (reset) begin
    valid_5 = 1'h0;
  end
  if (reset) begin
    valid_6 = 1'h0;
  end
  if (reset) begin
    valid_7 = 1'h0;
  end
  if (rf_reset) begin
    elts_0 = 32'h0;
  end
  if (rf_reset) begin
    elts_1 = 32'h0;
  end
  if (rf_reset) begin
    elts_2 = 32'h0;
  end
  if (rf_reset) begin
    elts_3 = 32'h0;
  end
  if (rf_reset) begin
    elts_4 = 32'h0;
  end
  if (rf_reset) begin
    elts_5 = 32'h0;
  end
  if (rf_reset) begin
    elts_6 = 32'h0;
  end
  if (rf_reset) begin
    elts_7 = 32'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
