//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_14_assert(
  input         clock,
  input         reset,
  input         io_in_a_ready,
  input         io_in_a_valid,
  input  [2:0]  io_in_a_bits_opcode,
  input  [3:0]  io_in_a_bits_size,
  input  [35:0] io_in_a_bits_address,
  input  [7:0]  io_in_a_bits_mask,
  input         io_in_d_ready,
  input         io_in_d_valid,
  input  [2:0]  io_in_d_bits_opcode,
  input  [1:0]  io_in_d_bits_param,
  input  [3:0]  io_in_d_bits_size,
  input  [3:0]  io_in_d_bits_sink,
  input         io_in_d_bits_denied,
  input         io_in_d_bits_corrupt
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire [22:0] _T_7; // @[package.scala 189:77]
  wire [7:0] _T_9; // @[package.scala 189:46]
  wire [35:0] _GEN_18; // @[Edges.scala 22:16]
  wire [35:0] _T_10; // @[Edges.scala 22:16]
  wire  _T_11; // @[Edges.scala 22:24]
  wire [3:0] _T_14; // @[OneHot.scala 65:12]
  wire [2:0] _T_16; // @[Misc.scala 201:81]
  wire  _T_17; // @[Misc.scala 205:21]
  wire  _T_20; // @[Misc.scala 210:20]
  wire  _T_22; // @[Misc.scala 214:38]
  wire  _T_23; // @[Misc.scala 214:29]
  wire  _T_25; // @[Misc.scala 214:38]
  wire  _T_26; // @[Misc.scala 214:29]
  wire  _T_29; // @[Misc.scala 210:20]
  wire  _T_30; // @[Misc.scala 213:27]
  wire  _T_31; // @[Misc.scala 214:38]
  wire  _T_32; // @[Misc.scala 214:29]
  wire  _T_33; // @[Misc.scala 213:27]
  wire  _T_34; // @[Misc.scala 214:38]
  wire  _T_35; // @[Misc.scala 214:29]
  wire  _T_36; // @[Misc.scala 213:27]
  wire  _T_37; // @[Misc.scala 214:38]
  wire  _T_38; // @[Misc.scala 214:29]
  wire  _T_39; // @[Misc.scala 213:27]
  wire  _T_40; // @[Misc.scala 214:38]
  wire  _T_41; // @[Misc.scala 214:29]
  wire  _T_44; // @[Misc.scala 210:20]
  wire  _T_45; // @[Misc.scala 213:27]
  wire  _T_46; // @[Misc.scala 214:38]
  wire  _T_47; // @[Misc.scala 214:29]
  wire  _T_48; // @[Misc.scala 213:27]
  wire  _T_49; // @[Misc.scala 214:38]
  wire  _T_50; // @[Misc.scala 214:29]
  wire  _T_51; // @[Misc.scala 213:27]
  wire  _T_52; // @[Misc.scala 214:38]
  wire  _T_53; // @[Misc.scala 214:29]
  wire  _T_54; // @[Misc.scala 213:27]
  wire  _T_55; // @[Misc.scala 214:38]
  wire  _T_56; // @[Misc.scala 214:29]
  wire  _T_57; // @[Misc.scala 213:27]
  wire  _T_58; // @[Misc.scala 214:38]
  wire  _T_59; // @[Misc.scala 214:29]
  wire  _T_60; // @[Misc.scala 213:27]
  wire  _T_61; // @[Misc.scala 214:38]
  wire  _T_62; // @[Misc.scala 214:29]
  wire  _T_63; // @[Misc.scala 213:27]
  wire  _T_64; // @[Misc.scala 214:38]
  wire  _T_65; // @[Misc.scala 214:29]
  wire  _T_66; // @[Misc.scala 213:27]
  wire  _T_67; // @[Misc.scala 214:38]
  wire  _T_68; // @[Misc.scala 214:29]
  wire [7:0] _T_75; // @[Cat.scala 29:58]
  wire [36:0] _T_79; // @[Parameters.scala 137:49]
  wire  _T_87; // @[Monitor.scala 79:25]
  wire [35:0] _T_89; // @[Parameters.scala 137:31]
  wire [36:0] _T_90; // @[Parameters.scala 137:49]
  wire [36:0] _T_92; // @[Parameters.scala 137:52]
  wire  _T_93; // @[Parameters.scala 137:67]
  wire [35:0] _T_94; // @[Parameters.scala 137:31]
  wire [36:0] _T_95; // @[Parameters.scala 137:49]
  wire [35:0] _T_99; // @[Parameters.scala 137:31]
  wire [36:0] _T_100; // @[Parameters.scala 137:49]
  wire [36:0] _T_102; // @[Parameters.scala 137:52]
  wire  _T_103; // @[Parameters.scala 137:67]
  wire [35:0] _T_104; // @[Parameters.scala 137:31]
  wire [36:0] _T_105; // @[Parameters.scala 137:49]
  wire [36:0] _T_107; // @[Parameters.scala 137:52]
  wire  _T_108; // @[Parameters.scala 137:67]
  wire [35:0] _T_109; // @[Parameters.scala 137:31]
  wire [36:0] _T_110; // @[Parameters.scala 137:49]
  wire [36:0] _T_112; // @[Parameters.scala 137:52]
  wire  _T_113; // @[Parameters.scala 137:67]
  wire [36:0] _T_117; // @[Parameters.scala 137:52]
  wire  _T_118; // @[Parameters.scala 137:67]
  wire [35:0] _T_119; // @[Parameters.scala 137:31]
  wire [36:0] _T_120; // @[Parameters.scala 137:49]
  wire [36:0] _T_122; // @[Parameters.scala 137:52]
  wire  _T_123; // @[Parameters.scala 137:67]
  wire [35:0] _T_124; // @[Parameters.scala 137:31]
  wire [36:0] _T_125; // @[Parameters.scala 137:49]
  wire [36:0] _T_127; // @[Parameters.scala 137:52]
  wire  _T_128; // @[Parameters.scala 137:67]
  wire [35:0] _T_129; // @[Parameters.scala 137:31]
  wire [36:0] _T_130; // @[Parameters.scala 137:49]
  wire [36:0] _T_132; // @[Parameters.scala 137:52]
  wire  _T_133; // @[Parameters.scala 137:67]
  wire [35:0] _T_134; // @[Parameters.scala 137:31]
  wire [36:0] _T_135; // @[Parameters.scala 137:49]
  wire [36:0] _T_137; // @[Parameters.scala 137:52]
  wire  _T_138; // @[Parameters.scala 137:67]
  wire  _T_149; // @[Parameters.scala 92:48]
  wire [35:0] _T_151; // @[Parameters.scala 137:31]
  wire [36:0] _T_152; // @[Parameters.scala 137:49]
  wire [36:0] _T_154; // @[Parameters.scala 137:52]
  wire  _T_155; // @[Parameters.scala 137:67]
  wire [35:0] _T_156; // @[Parameters.scala 137:31]
  wire [36:0] _T_157; // @[Parameters.scala 137:49]
  wire [36:0] _T_159; // @[Parameters.scala 137:52]
  wire  _T_160; // @[Parameters.scala 137:67]
  wire  _T_161; // @[Parameters.scala 552:42]
  wire  _T_162; // @[Parameters.scala 551:56]
  wire  _T_166; // @[Monitor.scala 44:11]
  wire  _T_167; // @[Monitor.scala 44:11]
  wire  _T_170; // @[Monitor.scala 44:11]
  wire  _T_176; // @[Monitor.scala 44:11]
  wire  _T_177; // @[Monitor.scala 44:11]
  wire  _T_179; // @[Monitor.scala 44:11]
  wire  _T_180; // @[Monitor.scala 44:11]
  wire [7:0] _T_185; // @[Monitor.scala 86:18]
  wire  _T_186; // @[Monitor.scala 86:31]
  wire  _T_188; // @[Monitor.scala 44:11]
  wire  _T_189; // @[Monitor.scala 44:11]
  wire  _T_194; // @[Monitor.scala 90:25]
  wire  _T_305; // @[Monitor.scala 102:25]
  wire  _T_307; // @[Parameters.scala 93:42]
  wire [36:0] _T_318; // @[Parameters.scala 137:52]
  wire  _T_319; // @[Parameters.scala 137:67]
  wire [36:0] _T_348; // @[Parameters.scala 137:52]
  wire  _T_349; // @[Parameters.scala 137:67]
  wire  _T_360; // @[Parameters.scala 552:42]
  wire  _T_361; // @[Parameters.scala 552:42]
  wire  _T_362; // @[Parameters.scala 552:42]
  wire  _T_363; // @[Parameters.scala 552:42]
  wire  _T_364; // @[Parameters.scala 552:42]
  wire  _T_365; // @[Parameters.scala 552:42]
  wire  _T_366; // @[Parameters.scala 552:42]
  wire  _T_367; // @[Parameters.scala 552:42]
  wire  _T_368; // @[Parameters.scala 552:42]
  wire  _T_369; // @[Parameters.scala 551:56]
  wire  _T_371; // @[Parameters.scala 93:42]
  wire [36:0] _T_377; // @[Parameters.scala 137:52]
  wire  _T_378; // @[Parameters.scala 137:67]
  wire  _T_379; // @[Parameters.scala 551:56]
  wire  _T_381; // @[Parameters.scala 553:30]
  wire  _T_383; // @[Monitor.scala 44:11]
  wire  _T_384; // @[Monitor.scala 44:11]
  wire  _T_395; // @[Monitor.scala 107:30]
  wire  _T_397; // @[Monitor.scala 44:11]
  wire  _T_398; // @[Monitor.scala 44:11]
  wire  _T_403; // @[Monitor.scala 111:25]
  wire  _T_405; // @[Parameters.scala 93:42]
  wire  _T_413; // @[Parameters.scala 551:56]
  wire  _T_463; // @[Parameters.scala 552:42]
  wire  _T_464; // @[Parameters.scala 552:42]
  wire  _T_465; // @[Parameters.scala 552:42]
  wire  _T_466; // @[Parameters.scala 552:42]
  wire  _T_467; // @[Parameters.scala 552:42]
  wire  _T_468; // @[Parameters.scala 552:42]
  wire  _T_469; // @[Parameters.scala 552:42]
  wire  _T_470; // @[Parameters.scala 552:42]
  wire  _T_471; // @[Parameters.scala 551:56]
  wire  _T_483; // @[Parameters.scala 553:30]
  wire  _T_484; // @[Parameters.scala 553:30]
  wire  _T_486; // @[Monitor.scala 44:11]
  wire  _T_487; // @[Monitor.scala 44:11]
  wire  _T_502; // @[Monitor.scala 119:25]
  wire [7:0] _T_597; // @[Monitor.scala 124:33]
  wire [7:0] _T_598; // @[Monitor.scala 124:31]
  wire  _T_599; // @[Monitor.scala 124:40]
  wire  _T_601; // @[Monitor.scala 44:11]
  wire  _T_602; // @[Monitor.scala 44:11]
  wire  _T_603; // @[Monitor.scala 127:25]
  wire  _T_612; // @[Parameters.scala 93:42]
  wire  _T_666; // @[Parameters.scala 552:42]
  wire  _T_667; // @[Parameters.scala 552:42]
  wire  _T_668; // @[Parameters.scala 552:42]
  wire  _T_669; // @[Parameters.scala 552:42]
  wire  _T_670; // @[Parameters.scala 552:42]
  wire  _T_671; // @[Parameters.scala 552:42]
  wire  _T_672; // @[Parameters.scala 552:42]
  wire  _T_673; // @[Parameters.scala 552:42]
  wire  _T_674; // @[Parameters.scala 551:56]
  wire  _T_678; // @[Monitor.scala 44:11]
  wire  _T_679; // @[Monitor.scala 44:11]
  wire  _T_694; // @[Monitor.scala 135:25]
  wire  _T_785; // @[Monitor.scala 143:25]
  wire  _T_856; // @[Parameters.scala 551:56]
  wire  _T_869; // @[Parameters.scala 553:30]
  wire  _T_871; // @[Monitor.scala 44:11]
  wire  _T_872; // @[Monitor.scala 44:11]
  wire  _T_891; // @[Bundles.scala 44:24]
  wire  _T_893; // @[Monitor.scala 51:11]
  wire  _T_894; // @[Monitor.scala 51:11]
  wire  _T_898; // @[Monitor.scala 307:25]
  wire  _T_902; // @[Monitor.scala 309:27]
  wire  _T_904; // @[Monitor.scala 51:11]
  wire  _T_905; // @[Monitor.scala 51:11]
  wire  _T_906; // @[Monitor.scala 310:28]
  wire  _T_908; // @[Monitor.scala 51:11]
  wire  _T_909; // @[Monitor.scala 51:11]
  wire  _T_910; // @[Monitor.scala 311:15]
  wire  _T_912; // @[Monitor.scala 51:11]
  wire  _T_913; // @[Monitor.scala 51:11]
  wire  _T_914; // @[Monitor.scala 312:15]
  wire  _T_916; // @[Monitor.scala 51:11]
  wire  _T_917; // @[Monitor.scala 51:11]
  wire  _T_918; // @[Monitor.scala 315:25]
  wire  _T_929; // @[Bundles.scala 104:26]
  wire  _T_931; // @[Monitor.scala 51:11]
  wire  _T_932; // @[Monitor.scala 51:11]
  wire  _T_933; // @[Monitor.scala 320:28]
  wire  _T_935; // @[Monitor.scala 51:11]
  wire  _T_936; // @[Monitor.scala 51:11]
  wire  _T_946; // @[Monitor.scala 325:25]
  wire  _T_966; // @[Monitor.scala 331:30]
  wire  _T_968; // @[Monitor.scala 51:11]
  wire  _T_969; // @[Monitor.scala 51:11]
  wire  _T_975; // @[Monitor.scala 335:25]
  wire  _T_992; // @[Monitor.scala 343:25]
  wire  _T_1010; // @[Monitor.scala 351:25]
  wire  _T_1042; // @[Decoupled.scala 40:37]
  wire  _T_1049; // @[Edges.scala 93:28]
  reg [4:0] _T_1051; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire [4:0] _T_1053; // @[Edges.scala 231:28]
  wire  _T_1054; // @[Edges.scala 232:25]
  reg [2:0] _T_1062; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [3:0] _T_1064; // @[Monitor.scala 383:22]
  reg [31:0] _RAND_2;
  reg [35:0] _T_1066; // @[Monitor.scala 385:22]
  reg [63:0] _RAND_3;
  wire  _T_1067; // @[Monitor.scala 386:22]
  wire  _T_1068; // @[Monitor.scala 386:19]
  wire  _T_1069; // @[Monitor.scala 387:32]
  wire  _T_1071; // @[Monitor.scala 44:11]
  wire  _T_1072; // @[Monitor.scala 44:11]
  wire  _T_1077; // @[Monitor.scala 389:32]
  wire  _T_1079; // @[Monitor.scala 44:11]
  wire  _T_1080; // @[Monitor.scala 44:11]
  wire  _T_1085; // @[Monitor.scala 391:32]
  wire  _T_1087; // @[Monitor.scala 44:11]
  wire  _T_1088; // @[Monitor.scala 44:11]
  wire  _T_1090; // @[Monitor.scala 393:20]
  wire  _T_1091; // @[Decoupled.scala 40:37]
  wire [22:0] _T_1093; // @[package.scala 189:77]
  wire [7:0] _T_1095; // @[package.scala 189:46]
  reg [4:0] _T_1099; // @[Edges.scala 230:27]
  reg [31:0] _RAND_4;
  wire [4:0] _T_1101; // @[Edges.scala 231:28]
  wire  _T_1102; // @[Edges.scala 232:25]
  reg [2:0] _T_1110; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_5;
  reg [1:0] _T_1111; // @[Monitor.scala 533:22]
  reg [31:0] _RAND_6;
  reg [3:0] _T_1112; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_7;
  reg [3:0] _T_1114; // @[Monitor.scala 536:22]
  reg [31:0] _RAND_8;
  reg  _T_1115; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_9;
  wire  _T_1116; // @[Monitor.scala 538:22]
  wire  _T_1117; // @[Monitor.scala 538:19]
  wire  _T_1118; // @[Monitor.scala 539:29]
  wire  _T_1120; // @[Monitor.scala 51:11]
  wire  _T_1121; // @[Monitor.scala 51:11]
  wire  _T_1122; // @[Monitor.scala 540:29]
  wire  _T_1124; // @[Monitor.scala 51:11]
  wire  _T_1125; // @[Monitor.scala 51:11]
  wire  _T_1126; // @[Monitor.scala 541:29]
  wire  _T_1128; // @[Monitor.scala 51:11]
  wire  _T_1129; // @[Monitor.scala 51:11]
  wire  _T_1134; // @[Monitor.scala 543:29]
  wire  _T_1136; // @[Monitor.scala 51:11]
  wire  _T_1137; // @[Monitor.scala 51:11]
  wire  _T_1138; // @[Monitor.scala 544:29]
  wire  _T_1140; // @[Monitor.scala 51:11]
  wire  _T_1141; // @[Monitor.scala 51:11]
  wire  _T_1143; // @[Monitor.scala 546:20]
  reg  _T_1144; // @[Monitor.scala 568:27]
  reg [31:0] _RAND_10;
  reg [4:0] _T_1154; // @[Edges.scala 230:27]
  reg [31:0] _RAND_11;
  wire [4:0] _T_1156; // @[Edges.scala 231:28]
  wire  _T_1157; // @[Edges.scala 232:25]
  reg [4:0] _T_1173; // @[Edges.scala 230:27]
  reg [31:0] _RAND_12;
  wire [4:0] _T_1175; // @[Edges.scala 231:28]
  wire  _T_1176; // @[Edges.scala 232:25]
  wire  _T_1186; // @[Monitor.scala 574:27]
  wire  _T_1191; // @[Monitor.scala 576:14]
  wire  _T_1193; // @[Monitor.scala 576:13]
  wire  _T_1194; // @[Monitor.scala 576:13]
  wire [1:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_1198; // @[Monitor.scala 581:27]
  wire  _T_1200; // @[Monitor.scala 581:75]
  wire  _T_1201; // @[Monitor.scala 581:72]
  wire  _T_1203; // @[Monitor.scala 583:21]
  wire  _T_1207; // @[Monitor.scala 51:11]
  wire  _T_1208; // @[Monitor.scala 51:11]
  wire [1:0] _GEN_16; // @[Monitor.scala 581:91]
  wire  _T_1209; // @[Monitor.scala 587:20]
  wire  _T_1211; // @[Monitor.scala 587:33]
  wire  _T_1212; // @[Monitor.scala 587:30]
  wire  _T_1214; // @[Monitor.scala 51:11]
  wire  _T_1215; // @[Monitor.scala 51:11]
  wire  _T_1216; // @[Monitor.scala 590:27]
  wire  _T_1217; // @[Monitor.scala 590:38]
  wire  _T_1218; // @[Monitor.scala 590:36]
  reg [31:0] _T_1219; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_13;
  wire  _T_1222; // @[Monitor.scala 595:36]
  wire  _T_1223; // @[Monitor.scala 595:27]
  wire  _T_1224; // @[Monitor.scala 595:56]
  wire  _T_1225; // @[Monitor.scala 595:44]
  wire  _T_1227; // @[Monitor.scala 595:12]
  wire  _T_1228; // @[Monitor.scala 595:12]
  wire [31:0] _T_1230; // @[Monitor.scala 597:26]
  wire  _T_1233; // @[Monitor.scala 598:27]
  wire  _GEN_19; // @[Monitor.scala 44:11]
  wire  _GEN_29; // @[Monitor.scala 44:11]
  wire  _GEN_41; // @[Monitor.scala 44:11]
  wire  _GEN_47; // @[Monitor.scala 44:11]
  wire  _GEN_53; // @[Monitor.scala 44:11]
  wire  _GEN_59; // @[Monitor.scala 44:11]
  wire  _GEN_65; // @[Monitor.scala 44:11]
  wire  _GEN_71; // @[Monitor.scala 44:11]
  wire  _GEN_77; // @[Monitor.scala 51:11]
  wire  _GEN_85; // @[Monitor.scala 51:11]
  wire  _GEN_93; // @[Monitor.scala 51:11]
  wire  _GEN_101; // @[Monitor.scala 51:11]
  wire  _GEN_105; // @[Monitor.scala 51:11]
  wire  _GEN_109; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_7 = 23'hff << io_in_a_bits_size; // @[package.scala 189:77]
  assign _T_9 = ~_T_7[7:0]; // @[package.scala 189:46]
  assign _GEN_18 = {{28'd0}, _T_9}; // @[Edges.scala 22:16]
  assign _T_10 = io_in_a_bits_address & _GEN_18; // @[Edges.scala 22:16]
  assign _T_11 = _T_10 == 36'h0; // @[Edges.scala 22:24]
  assign _T_14 = 4'h1 << io_in_a_bits_size[1:0]; // @[OneHot.scala 65:12]
  assign _T_16 = _T_14[2:0] | 3'h1; // @[Misc.scala 201:81]
  assign _T_17 = io_in_a_bits_size >= 4'h3; // @[Misc.scala 205:21]
  assign _T_20 = ~io_in_a_bits_address[2]; // @[Misc.scala 210:20]
  assign _T_22 = _T_16[2] & _T_20; // @[Misc.scala 214:38]
  assign _T_23 = _T_17 | _T_22; // @[Misc.scala 214:29]
  assign _T_25 = _T_16[2] & io_in_a_bits_address[2]; // @[Misc.scala 214:38]
  assign _T_26 = _T_17 | _T_25; // @[Misc.scala 214:29]
  assign _T_29 = ~io_in_a_bits_address[1]; // @[Misc.scala 210:20]
  assign _T_30 = _T_20 & _T_29; // @[Misc.scala 213:27]
  assign _T_31 = _T_16[1] & _T_30; // @[Misc.scala 214:38]
  assign _T_32 = _T_23 | _T_31; // @[Misc.scala 214:29]
  assign _T_33 = _T_20 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_34 = _T_16[1] & _T_33; // @[Misc.scala 214:38]
  assign _T_35 = _T_23 | _T_34; // @[Misc.scala 214:29]
  assign _T_36 = io_in_a_bits_address[2] & _T_29; // @[Misc.scala 213:27]
  assign _T_37 = _T_16[1] & _T_36; // @[Misc.scala 214:38]
  assign _T_38 = _T_26 | _T_37; // @[Misc.scala 214:29]
  assign _T_39 = io_in_a_bits_address[2] & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_40 = _T_16[1] & _T_39; // @[Misc.scala 214:38]
  assign _T_41 = _T_26 | _T_40; // @[Misc.scala 214:29]
  assign _T_44 = ~io_in_a_bits_address[0]; // @[Misc.scala 210:20]
  assign _T_45 = _T_30 & _T_44; // @[Misc.scala 213:27]
  assign _T_46 = _T_16[0] & _T_45; // @[Misc.scala 214:38]
  assign _T_47 = _T_32 | _T_46; // @[Misc.scala 214:29]
  assign _T_48 = _T_30 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_49 = _T_16[0] & _T_48; // @[Misc.scala 214:38]
  assign _T_50 = _T_32 | _T_49; // @[Misc.scala 214:29]
  assign _T_51 = _T_33 & _T_44; // @[Misc.scala 213:27]
  assign _T_52 = _T_16[0] & _T_51; // @[Misc.scala 214:38]
  assign _T_53 = _T_35 | _T_52; // @[Misc.scala 214:29]
  assign _T_54 = _T_33 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_55 = _T_16[0] & _T_54; // @[Misc.scala 214:38]
  assign _T_56 = _T_35 | _T_55; // @[Misc.scala 214:29]
  assign _T_57 = _T_36 & _T_44; // @[Misc.scala 213:27]
  assign _T_58 = _T_16[0] & _T_57; // @[Misc.scala 214:38]
  assign _T_59 = _T_38 | _T_58; // @[Misc.scala 214:29]
  assign _T_60 = _T_36 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_61 = _T_16[0] & _T_60; // @[Misc.scala 214:38]
  assign _T_62 = _T_38 | _T_61; // @[Misc.scala 214:29]
  assign _T_63 = _T_39 & _T_44; // @[Misc.scala 213:27]
  assign _T_64 = _T_16[0] & _T_63; // @[Misc.scala 214:38]
  assign _T_65 = _T_41 | _T_64; // @[Misc.scala 214:29]
  assign _T_66 = _T_39 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_67 = _T_16[0] & _T_66; // @[Misc.scala 214:38]
  assign _T_68 = _T_41 | _T_67; // @[Misc.scala 214:29]
  assign _T_75 = {_T_68,_T_65,_T_62,_T_59,_T_56,_T_53,_T_50,_T_47}; // @[Cat.scala 29:58]
  assign _T_79 = {1'b0,$signed(io_in_a_bits_address)}; // @[Parameters.scala 137:49]
  assign _T_87 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_89 = io_in_a_bits_address ^ 36'h400000000; // @[Parameters.scala 137:31]
  assign _T_90 = {1'b0,$signed(_T_89)}; // @[Parameters.scala 137:49]
  assign _T_92 = $signed(_T_90) & -37'sh400000000; // @[Parameters.scala 137:52]
  assign _T_93 = $signed(_T_92) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_94 = io_in_a_bits_address ^ 36'h8000000; // @[Parameters.scala 137:31]
  assign _T_95 = {1'b0,$signed(_T_94)}; // @[Parameters.scala 137:49]
  assign _T_99 = io_in_a_bits_address ^ 36'h3000; // @[Parameters.scala 137:31]
  assign _T_100 = {1'b0,$signed(_T_99)}; // @[Parameters.scala 137:49]
  assign _T_102 = $signed(_T_100) & -37'sh10001000; // @[Parameters.scala 137:52]
  assign _T_103 = $signed(_T_102) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_104 = io_in_a_bits_address ^ 36'hc000000; // @[Parameters.scala 137:31]
  assign _T_105 = {1'b0,$signed(_T_104)}; // @[Parameters.scala 137:49]
  assign _T_107 = $signed(_T_105) & -37'sh4000000; // @[Parameters.scala 137:52]
  assign _T_108 = $signed(_T_107) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_109 = io_in_a_bits_address ^ 36'h2000000; // @[Parameters.scala 137:31]
  assign _T_110 = {1'b0,$signed(_T_109)}; // @[Parameters.scala 137:49]
  assign _T_112 = $signed(_T_110) & -37'sh10000; // @[Parameters.scala 137:52]
  assign _T_113 = $signed(_T_112) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_117 = $signed(_T_79) & -37'sh10001000; // @[Parameters.scala 137:52]
  assign _T_118 = $signed(_T_117) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_119 = io_in_a_bits_address ^ 36'h20000000; // @[Parameters.scala 137:31]
  assign _T_120 = {1'b0,$signed(_T_119)}; // @[Parameters.scala 137:49]
  assign _T_122 = $signed(_T_120) & -37'sh20000000; // @[Parameters.scala 137:52]
  assign _T_123 = $signed(_T_122) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_124 = io_in_a_bits_address ^ 36'h10001000; // @[Parameters.scala 137:31]
  assign _T_125 = {1'b0,$signed(_T_124)}; // @[Parameters.scala 137:49]
  assign _T_127 = $signed(_T_125) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_128 = $signed(_T_127) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_129 = io_in_a_bits_address ^ 36'h4000; // @[Parameters.scala 137:31]
  assign _T_130 = {1'b0,$signed(_T_129)}; // @[Parameters.scala 137:49]
  assign _T_132 = $signed(_T_130) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_133 = $signed(_T_132) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_134 = io_in_a_bits_address ^ 36'h2010000; // @[Parameters.scala 137:31]
  assign _T_135 = {1'b0,$signed(_T_134)}; // @[Parameters.scala 137:49]
  assign _T_137 = $signed(_T_135) & -37'sh4000; // @[Parameters.scala 137:52]
  assign _T_138 = $signed(_T_137) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_149 = 4'h6 == io_in_a_bits_size; // @[Parameters.scala 92:48]
  assign _T_151 = io_in_a_bits_address ^ 36'ha000000; // @[Parameters.scala 137:31]
  assign _T_152 = {1'b0,$signed(_T_151)}; // @[Parameters.scala 137:49]
  assign _T_154 = $signed(_T_152) & -37'sh200000; // @[Parameters.scala 137:52]
  assign _T_155 = $signed(_T_154) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_156 = io_in_a_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  assign _T_157 = {1'b0,$signed(_T_156)}; // @[Parameters.scala 137:49]
  assign _T_159 = $signed(_T_157) & -37'sh800000000; // @[Parameters.scala 137:52]
  assign _T_160 = $signed(_T_159) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_161 = _T_155 | _T_160; // @[Parameters.scala 552:42]
  assign _T_162 = _T_149 & _T_161; // @[Parameters.scala 551:56]
  assign _T_166 = _T_162 | reset; // @[Monitor.scala 44:11]
  assign _T_167 = ~_T_166; // @[Monitor.scala 44:11]
  assign _T_170 = ~reset; // @[Monitor.scala 44:11]
  assign _T_176 = _T_17 | reset; // @[Monitor.scala 44:11]
  assign _T_177 = ~_T_176; // @[Monitor.scala 44:11]
  assign _T_179 = _T_11 | reset; // @[Monitor.scala 44:11]
  assign _T_180 = ~_T_179; // @[Monitor.scala 44:11]
  assign _T_185 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_186 = _T_185 == 8'h0; // @[Monitor.scala 86:31]
  assign _T_188 = _T_186 | reset; // @[Monitor.scala 44:11]
  assign _T_189 = ~_T_188; // @[Monitor.scala 44:11]
  assign _T_194 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_305 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_307 = io_in_a_bits_size <= 4'h6; // @[Parameters.scala 93:42]
  assign _T_318 = $signed(_T_95) & -37'sh2200000; // @[Parameters.scala 137:52]
  assign _T_319 = $signed(_T_318) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_348 = $signed(_T_125) & -37'sh3000; // @[Parameters.scala 137:52]
  assign _T_349 = $signed(_T_348) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_360 = _T_93 | _T_319; // @[Parameters.scala 552:42]
  assign _T_361 = _T_360 | _T_160; // @[Parameters.scala 552:42]
  assign _T_362 = _T_361 | _T_108; // @[Parameters.scala 552:42]
  assign _T_363 = _T_362 | _T_113; // @[Parameters.scala 552:42]
  assign _T_364 = _T_363 | _T_118; // @[Parameters.scala 552:42]
  assign _T_365 = _T_364 | _T_123; // @[Parameters.scala 552:42]
  assign _T_366 = _T_365 | _T_349; // @[Parameters.scala 552:42]
  assign _T_367 = _T_366 | _T_133; // @[Parameters.scala 552:42]
  assign _T_368 = _T_367 | _T_138; // @[Parameters.scala 552:42]
  assign _T_369 = _T_307 & _T_368; // @[Parameters.scala 551:56]
  assign _T_371 = io_in_a_bits_size <= 4'h8; // @[Parameters.scala 93:42]
  assign _T_377 = $signed(_T_100) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_378 = $signed(_T_377) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_379 = _T_371 & _T_378; // @[Parameters.scala 551:56]
  assign _T_381 = _T_369 | _T_379; // @[Parameters.scala 553:30]
  assign _T_383 = _T_381 | reset; // @[Monitor.scala 44:11]
  assign _T_384 = ~_T_383; // @[Monitor.scala 44:11]
  assign _T_395 = io_in_a_bits_mask == _T_75; // @[Monitor.scala 107:30]
  assign _T_397 = _T_395 | reset; // @[Monitor.scala 44:11]
  assign _T_398 = ~_T_397; // @[Monitor.scala 44:11]
  assign _T_403 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_405 = io_in_a_bits_size <= 4'h7; // @[Parameters.scala 93:42]
  assign _T_413 = _T_405 & _T_93; // @[Parameters.scala 551:56]
  assign _T_463 = _T_319 | _T_160; // @[Parameters.scala 552:42]
  assign _T_464 = _T_463 | _T_108; // @[Parameters.scala 552:42]
  assign _T_465 = _T_464 | _T_113; // @[Parameters.scala 552:42]
  assign _T_466 = _T_465 | _T_118; // @[Parameters.scala 552:42]
  assign _T_467 = _T_466 | _T_123; // @[Parameters.scala 552:42]
  assign _T_468 = _T_467 | _T_349; // @[Parameters.scala 552:42]
  assign _T_469 = _T_468 | _T_133; // @[Parameters.scala 552:42]
  assign _T_470 = _T_469 | _T_138; // @[Parameters.scala 552:42]
  assign _T_471 = _T_307 & _T_470; // @[Parameters.scala 551:56]
  assign _T_483 = _T_413 | _T_471; // @[Parameters.scala 553:30]
  assign _T_484 = _T_483 | _T_379; // @[Parameters.scala 553:30]
  assign _T_486 = _T_484 | reset; // @[Monitor.scala 44:11]
  assign _T_487 = ~_T_486; // @[Monitor.scala 44:11]
  assign _T_502 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_597 = ~_T_75; // @[Monitor.scala 124:33]
  assign _T_598 = io_in_a_bits_mask & _T_597; // @[Monitor.scala 124:31]
  assign _T_599 = _T_598 == 8'h0; // @[Monitor.scala 124:40]
  assign _T_601 = _T_599 | reset; // @[Monitor.scala 44:11]
  assign _T_602 = ~_T_601; // @[Monitor.scala 44:11]
  assign _T_603 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_612 = io_in_a_bits_size <= 4'h3; // @[Parameters.scala 93:42]
  assign _T_666 = _T_463 | _T_103; // @[Parameters.scala 552:42]
  assign _T_667 = _T_666 | _T_108; // @[Parameters.scala 552:42]
  assign _T_668 = _T_667 | _T_113; // @[Parameters.scala 552:42]
  assign _T_669 = _T_668 | _T_118; // @[Parameters.scala 552:42]
  assign _T_670 = _T_669 | _T_123; // @[Parameters.scala 552:42]
  assign _T_671 = _T_670 | _T_128; // @[Parameters.scala 552:42]
  assign _T_672 = _T_671 | _T_133; // @[Parameters.scala 552:42]
  assign _T_673 = _T_672 | _T_138; // @[Parameters.scala 552:42]
  assign _T_674 = _T_612 & _T_673; // @[Parameters.scala 551:56]
  assign _T_678 = _T_674 | reset; // @[Monitor.scala 44:11]
  assign _T_679 = ~_T_678; // @[Monitor.scala 44:11]
  assign _T_694 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_785 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_856 = _T_307 & _T_161; // @[Parameters.scala 551:56]
  assign _T_869 = _T_856 | _T_379; // @[Parameters.scala 553:30]
  assign _T_871 = _T_869 | reset; // @[Monitor.scala 44:11]
  assign _T_872 = ~_T_871; // @[Monitor.scala 44:11]
  assign _T_891 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_893 = _T_891 | reset; // @[Monitor.scala 51:11]
  assign _T_894 = ~_T_893; // @[Monitor.scala 51:11]
  assign _T_898 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_902 = io_in_d_bits_size >= 4'h3; // @[Monitor.scala 309:27]
  assign _T_904 = _T_902 | reset; // @[Monitor.scala 51:11]
  assign _T_905 = ~_T_904; // @[Monitor.scala 51:11]
  assign _T_906 = io_in_d_bits_param == 2'h0; // @[Monitor.scala 310:28]
  assign _T_908 = _T_906 | reset; // @[Monitor.scala 51:11]
  assign _T_909 = ~_T_908; // @[Monitor.scala 51:11]
  assign _T_910 = ~io_in_d_bits_corrupt; // @[Monitor.scala 311:15]
  assign _T_912 = _T_910 | reset; // @[Monitor.scala 51:11]
  assign _T_913 = ~_T_912; // @[Monitor.scala 51:11]
  assign _T_914 = ~io_in_d_bits_denied; // @[Monitor.scala 312:15]
  assign _T_916 = _T_914 | reset; // @[Monitor.scala 51:11]
  assign _T_917 = ~_T_916; // @[Monitor.scala 51:11]
  assign _T_918 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_929 = io_in_d_bits_param <= 2'h2; // @[Bundles.scala 104:26]
  assign _T_931 = _T_929 | reset; // @[Monitor.scala 51:11]
  assign _T_932 = ~_T_931; // @[Monitor.scala 51:11]
  assign _T_933 = io_in_d_bits_param != 2'h2; // @[Monitor.scala 320:28]
  assign _T_935 = _T_933 | reset; // @[Monitor.scala 51:11]
  assign _T_936 = ~_T_935; // @[Monitor.scala 51:11]
  assign _T_946 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_966 = _T_914 | io_in_d_bits_corrupt; // @[Monitor.scala 331:30]
  assign _T_968 = _T_966 | reset; // @[Monitor.scala 51:11]
  assign _T_969 = ~_T_968; // @[Monitor.scala 51:11]
  assign _T_975 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_992 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_1010 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_1042 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_1049 = ~io_in_a_bits_opcode[2]; // @[Edges.scala 93:28]
  assign _T_1053 = _T_1051 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1054 = _T_1051 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1067 = ~_T_1054; // @[Monitor.scala 386:22]
  assign _T_1068 = io_in_a_valid & _T_1067; // @[Monitor.scala 386:19]
  assign _T_1069 = io_in_a_bits_opcode == _T_1062; // @[Monitor.scala 387:32]
  assign _T_1071 = _T_1069 | reset; // @[Monitor.scala 44:11]
  assign _T_1072 = ~_T_1071; // @[Monitor.scala 44:11]
  assign _T_1077 = io_in_a_bits_size == _T_1064; // @[Monitor.scala 389:32]
  assign _T_1079 = _T_1077 | reset; // @[Monitor.scala 44:11]
  assign _T_1080 = ~_T_1079; // @[Monitor.scala 44:11]
  assign _T_1085 = io_in_a_bits_address == _T_1066; // @[Monitor.scala 391:32]
  assign _T_1087 = _T_1085 | reset; // @[Monitor.scala 44:11]
  assign _T_1088 = ~_T_1087; // @[Monitor.scala 44:11]
  assign _T_1090 = _T_1042 & _T_1054; // @[Monitor.scala 393:20]
  assign _T_1091 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_1093 = 23'hff << io_in_d_bits_size; // @[package.scala 189:77]
  assign _T_1095 = ~_T_1093[7:0]; // @[package.scala 189:46]
  assign _T_1101 = _T_1099 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1102 = _T_1099 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1116 = ~_T_1102; // @[Monitor.scala 538:22]
  assign _T_1117 = io_in_d_valid & _T_1116; // @[Monitor.scala 538:19]
  assign _T_1118 = io_in_d_bits_opcode == _T_1110; // @[Monitor.scala 539:29]
  assign _T_1120 = _T_1118 | reset; // @[Monitor.scala 51:11]
  assign _T_1121 = ~_T_1120; // @[Monitor.scala 51:11]
  assign _T_1122 = io_in_d_bits_param == _T_1111; // @[Monitor.scala 540:29]
  assign _T_1124 = _T_1122 | reset; // @[Monitor.scala 51:11]
  assign _T_1125 = ~_T_1124; // @[Monitor.scala 51:11]
  assign _T_1126 = io_in_d_bits_size == _T_1112; // @[Monitor.scala 541:29]
  assign _T_1128 = _T_1126 | reset; // @[Monitor.scala 51:11]
  assign _T_1129 = ~_T_1128; // @[Monitor.scala 51:11]
  assign _T_1134 = io_in_d_bits_sink == _T_1114; // @[Monitor.scala 543:29]
  assign _T_1136 = _T_1134 | reset; // @[Monitor.scala 51:11]
  assign _T_1137 = ~_T_1136; // @[Monitor.scala 51:11]
  assign _T_1138 = io_in_d_bits_denied == _T_1115; // @[Monitor.scala 544:29]
  assign _T_1140 = _T_1138 | reset; // @[Monitor.scala 51:11]
  assign _T_1141 = ~_T_1140; // @[Monitor.scala 51:11]
  assign _T_1143 = _T_1091 & _T_1102; // @[Monitor.scala 546:20]
  assign _T_1156 = _T_1154 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1157 = _T_1154 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1175 = _T_1173 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1176 = _T_1173 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1186 = _T_1042 & _T_1157; // @[Monitor.scala 574:27]
  assign _T_1191 = ~_T_1144; // @[Monitor.scala 576:14]
  assign _T_1193 = _T_1191 | reset; // @[Monitor.scala 576:13]
  assign _T_1194 = ~_T_1193; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_1186 ? 2'h1 : 2'h0; // @[Monitor.scala 574:72]
  assign _T_1198 = _T_1091 & _T_1176; // @[Monitor.scala 581:27]
  assign _T_1200 = ~_T_898; // @[Monitor.scala 581:75]
  assign _T_1201 = _T_1198 & _T_1200; // @[Monitor.scala 581:72]
  assign _T_1203 = _GEN_15[0] | _T_1144; // @[Monitor.scala 583:21]
  assign _T_1207 = _T_1203 | reset; // @[Monitor.scala 51:11]
  assign _T_1208 = ~_T_1207; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_1201 ? 2'h1 : 2'h0; // @[Monitor.scala 581:91]
  assign _T_1209 = _GEN_15[0] != _GEN_16[0]; // @[Monitor.scala 587:20]
  assign _T_1211 = ~_GEN_15[0]; // @[Monitor.scala 587:33]
  assign _T_1212 = _T_1209 | _T_1211; // @[Monitor.scala 587:30]
  assign _T_1214 = _T_1212 | reset; // @[Monitor.scala 51:11]
  assign _T_1215 = ~_T_1214; // @[Monitor.scala 51:11]
  assign _T_1216 = _T_1144 | _GEN_15[0]; // @[Monitor.scala 590:27]
  assign _T_1217 = ~_GEN_16[0]; // @[Monitor.scala 590:38]
  assign _T_1218 = _T_1216 & _T_1217; // @[Monitor.scala 590:36]
  assign _T_1222 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_1223 = _T_1191 | _T_1222; // @[Monitor.scala 595:27]
  assign _T_1224 = _T_1219 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_1225 = _T_1223 | _T_1224; // @[Monitor.scala 595:44]
  assign _T_1227 = _T_1225 | reset; // @[Monitor.scala 595:12]
  assign _T_1228 = ~_T_1227; // @[Monitor.scala 595:12]
  assign _T_1230 = _T_1219 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_1233 = _T_1042 | _T_1091; // @[Monitor.scala 598:27]
  assign _GEN_19 = io_in_a_valid & _T_87; // @[Monitor.scala 44:11]
  assign _GEN_29 = io_in_a_valid & _T_194; // @[Monitor.scala 44:11]
  assign _GEN_41 = io_in_a_valid & _T_305; // @[Monitor.scala 44:11]
  assign _GEN_47 = io_in_a_valid & _T_403; // @[Monitor.scala 44:11]
  assign _GEN_53 = io_in_a_valid & _T_502; // @[Monitor.scala 44:11]
  assign _GEN_59 = io_in_a_valid & _T_603; // @[Monitor.scala 44:11]
  assign _GEN_65 = io_in_a_valid & _T_694; // @[Monitor.scala 44:11]
  assign _GEN_71 = io_in_a_valid & _T_785; // @[Monitor.scala 44:11]
  assign _GEN_77 = io_in_d_valid & _T_898; // @[Monitor.scala 51:11]
  assign _GEN_85 = io_in_d_valid & _T_918; // @[Monitor.scala 51:11]
  assign _GEN_93 = io_in_d_valid & _T_946; // @[Monitor.scala 51:11]
  assign _GEN_101 = io_in_d_valid & _T_975; // @[Monitor.scala 51:11]
  assign _GEN_105 = io_in_d_valid & _T_992; // @[Monitor.scala 51:11]
  assign _GEN_109 = io_in_d_valid & _T_1010; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_1051 = _RAND_0[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_1062 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_1064 = _RAND_2[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {2{`RANDOM}};
  _T_1066 = _RAND_3[35:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_1099 = _RAND_4[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  _T_1110 = _RAND_5[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_1111 = _RAND_6[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_1112 = _RAND_7[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_1114 = _RAND_8[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_1115 = _RAND_9[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_1144 = _RAND_10[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_1154 = _RAND_11[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_1173 = _RAND_12[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  _T_1219 = _RAND_13[31:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_1051 <= 5'h0;
    end else if (_T_1042) begin
      if (_T_1054) begin
        if (_T_1049) begin
          _T_1051 <= _T_9[7:3];
        end else begin
          _T_1051 <= 5'h0;
        end
      end else begin
        _T_1051 <= _T_1053;
      end
    end
    if (_T_1090) begin
      _T_1062 <= io_in_a_bits_opcode;
    end
    if (_T_1090) begin
      _T_1064 <= io_in_a_bits_size;
    end
    if (_T_1090) begin
      _T_1066 <= io_in_a_bits_address;
    end
    if (reset) begin
      _T_1099 <= 5'h0;
    end else if (_T_1091) begin
      if (_T_1102) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1099 <= _T_1095[7:3];
        end else begin
          _T_1099 <= 5'h0;
        end
      end else begin
        _T_1099 <= _T_1101;
      end
    end
    if (_T_1143) begin
      _T_1110 <= io_in_d_bits_opcode;
    end
    if (_T_1143) begin
      _T_1111 <= io_in_d_bits_param;
    end
    if (_T_1143) begin
      _T_1112 <= io_in_d_bits_size;
    end
    if (_T_1143) begin
      _T_1114 <= io_in_d_bits_sink;
    end
    if (_T_1143) begin
      _T_1115 <= io_in_d_bits_denied;
    end
    if (reset) begin
      _T_1144 <= 1'h0;
    end else begin
      _T_1144 <= _T_1218;
    end
    if (reset) begin
      _T_1154 <= 5'h0;
    end else if (_T_1042) begin
      if (_T_1157) begin
        if (_T_1049) begin
          _T_1154 <= _T_9[7:3];
        end else begin
          _T_1154 <= 5'h0;
        end
      end else begin
        _T_1154 <= _T_1156;
      end
    end
    if (reset) begin
      _T_1173 <= 5'h0;
    end else if (_T_1091) begin
      if (_T_1176) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1173 <= _T_1095[7:3];
        end else begin
          _T_1173 <= 5'h0;
        end
      end else begin
        _T_1173 <= _T_1175;
      end
    end
    if (reset) begin
      _T_1219 <= 32'h0;
    end else if (_T_1233) begin
      _T_1219 <= 32'h0;
    end else begin
      _T_1219 <= _T_1230;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_167) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_167) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_170) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_170) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_177) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock smaller than a beat (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_177) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_180) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_180) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_189) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_189) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_29 & _T_167) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_29 & _T_167) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_29 & _T_170) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_29 & _T_170) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_29 & _T_177) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm smaller than a beat (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_29 & _T_177) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_29 & _T_180) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_29 & _T_180) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_29 & _T_170) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_29 & _T_170) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_29 & _T_189) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_29 & _T_189) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_41 & _T_384) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_41 & _T_384) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_41 & _T_180) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_41 & _T_180) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_41 & _T_398) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_41 & _T_398) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_47 & _T_487) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_47 & _T_487) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_47 & _T_180) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_47 & _T_180) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_47 & _T_398) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_47 & _T_398) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_487) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_487) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_180) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_180) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_602) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial contains invalid mask (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_602) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_59 & _T_679) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_59 & _T_679) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_59 & _T_180) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_59 & _T_180) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_59 & _T_398) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_59 & _T_398) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_679) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_679) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_180) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_180) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_398) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_398) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_71 & _T_872) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_71 & _T_872) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_71 & _T_180) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_71 & _T_180) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_71 & _T_398) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_71 & _T_398) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_894) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_894) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_77 & _T_905) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_77 & _T_905) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_77 & _T_909) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseeAck carries invalid param (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_77 & _T_909) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_77 & _T_913) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is corrupt (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_77 & _T_913) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_77 & _T_917) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is denied (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_77 & _T_917) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_905) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_905) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_932) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid cap param (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_932) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_936) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries toN param (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_936) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_913) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is corrupt (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_913) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_93 & _T_905) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_93 & _T_905) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_93 & _T_932) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid cap param (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_93 & _T_932) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_93 & _T_936) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries toN param (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_93 & _T_936) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_93 & _T_969) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied but not corrupt (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_93 & _T_969) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_101 & _T_909) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid param (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_101 & _T_909) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_101 & _T_913) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is corrupt (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_101 & _T_913) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_909) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid param (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_909) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_969) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied but not corrupt (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_969) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_109 & _T_909) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid param (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_109 & _T_909) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_109 & _T_913) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is corrupt (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_109 & _T_913) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1068 & _T_1072) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1068 & _T_1072) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1068 & _T_1080) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel size changed within multibeat operation (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1068 & _T_1080) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1068 & _T_1088) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1068 & _T_1088) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1117 & _T_1121) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1117 & _T_1121) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1117 & _T_1125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel param changed within multibeat operation (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1117 & _T_1125) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1117 & _T_1129) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1117 & _T_1129) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1117 & _T_1137) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel sink changed with multibeat operation (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1117 & _T_1137) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1117 & _T_1141) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1117 & _T_1141) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1186 & _T_1194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1186 & _T_1194) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1201 & _T_1208) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1201 & _T_1208) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1215) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' and 'D' concurrent, despite minlatency 4 (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1215) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1228) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at BusWrapper.scala:186:18)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1228) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
