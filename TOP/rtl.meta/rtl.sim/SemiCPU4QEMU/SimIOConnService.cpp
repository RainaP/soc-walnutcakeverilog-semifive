#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <execinfo.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include <errno.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>

#include "SemiCPU.h"

#ifdef DEBUG
static inline void printStackTrace() 
{
#define MAX_CALL_DEPTH  (64)
    void* frames[MAX_CALL_DEPTH]; 
    int frameDepth = backtrace(frames, MAX_CALL_DEPTH); 
    char** frameInfo = backtrace_symbols(frames, frameDepth); 

    for (int i = 0; i < frameDepth; ++i) 
        printf("%s\n", frameInfo[i]); 

    free(frameInfo); 
}
#endif

///////////////////////////////////////////////////////////////////////////////
// SemiCPU helper functinos
///////////////////////////////////////////////////////////////////////////////
extern "C" void simulationTick(void);

///////////////////////////////////////////////////////////////////////////////
// SimIO definitions
///////////////////////////////////////////////////////////////////////////////
#define TEST_BASE_ADDR  (0x400000000ULL)
#define TEST_SRAM_SIZE  (1024)

#ifndef SIMIO_PORT
#define SIMIO_PORT      (4444)
#endif
#ifndef SIMIO_HOST
#define SIMIO_HOST      "192.168.20.34"
#endif

#define WRITE_ACK       (0x0001)
#define WRITE_REQ       (0x0002)
#define READ_ACK        (0x0004)
#define READ_REQ        (0x0008)
#define IRQ_ACK         (0x0010)
#define IRQ_REQ         (0x0020)
#define STOP_SIMIO      (0x8000)

typedef struct pkt_info {
    uint16_t type;
    uint16_t size;
    uint32_t offset;
} req_info_t;

typedef struct simio_pkt {
    union {
        req_info_t info;
        uint64_t hdr;
    };
    uint64_t data;
} simio_pkt_t;

#define SIMIO_PACKET_SIZE   (sizeof(simio_pkt_t))

#define MAX_EVENTS_SIZE     (32)
#define REQ_INFO(d)         ((req_info_t *)&(d))
#define DOORBELL            (BASE_SIM_HOST + SIM_HOST_DOORBELL_OFFSET)
#define USEC_PER_MS         (1000)

static void setSocketNonBlock(int client_fd)
{
    /* Change the socket into Non-Blocking state */
    int flag = fcntl(client_fd, F_GETFL, 0);

    fcntl(client_fd, F_SETFL, flag | O_NONBLOCK);
}

static int gClientSocket;

static int getClientSocket(void)
{
    if (!gClientSocket) {
        struct sockaddr_in client_addr;

        gClientSocket = socket(PF_INET, SOCK_STREAM, 0);

        client_addr.sin_addr.s_addr = inet_addr(SIMIO_HOST);
        client_addr.sin_family = AF_INET;
        client_addr.sin_port = htons(SIMIO_PORT);

        if (connect(gClientSocket, (struct sockaddr *)&client_addr, sizeof(client_addr)) == -1) {
            printf("[%s] Cannot connect to %s:%d (error = %d)\n", __func__, SIMIO_HOST, SIMIO_PORT, errno);
            close(gClientSocket);
            return -1;
        }
    }

    return gClientSocket;
}

extern "C" void SimIORaiseIRQ(int irq_num)
{
    printf("[%s] Entering ...\n", __func__);

    int client_fd = getClientSocket();
    simio_pkt_t packet;

    packet.info.type = IRQ_REQ;
    packet.data = irq_num;
    send(client_fd, &packet, sizeof(packet), 0);

    printf("[%s] Exiting ...\n", __func__);
}

#ifdef MULTI_THREADED_TIME_CONSUMING
static pthread_cond_t consumeTimeCond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t consumeTimeMutex = PTHREAD_MUTEX_INITIALIZER;
static bool keepRunning = true;

static long getuSecPerClock(void)
{
    struct timeval start, finish;

    gettimeofday(&start, NULL);
    simulationTick();
    gettimeofday(&finish, NULL);

    long elapsed_usec = finish.tv_usec - start.tv_usec;
    long elapsed_sec = finish.tv_sec - start.tv_sec;
    long elapsed = (elapsed_sec * 1000000) + elapsed_usec;

    printf("[%s] A clock consumed %ld usec\n", __func__, elapsed);

    return elapsed;
}

static void *consumeTimeWrapper(void *opaque)
{
    pthread_mutex_lock(&consumeTimeMutex);

    while(keepRunning) {
        pthread_cond_wait(&consumeTimeCond, &consumeTimeMutex);
        simulationTick();
    }

    pthread_mutex_unlock(&consumeTimeMutex);
    return NULL;
}

#define STOP    (false)
#define EXEC    (true)

static void consumeTimeEvent(bool condition)
{
    pthread_mutex_lock(&consumeTimeMutex);

    keepRunning = condition;
    pthread_cond_signal(&consumeTimeCond);

    pthread_mutex_unlock(&consumeTimeMutex);
}
#endif

extern "C" int SimIOConnService(int flag)
{
    int client_fd, event_ready;
    int timeout_ms;
    ssize_t pkt_size;

    simio_pkt_t packet;
    uint64_t addr, data;
    uint16_t type, size;
#ifdef MULTI_THREADED_TIME_CONSUMING
    long usec_per_clock;
#endif

    (void)flag;

    printf("[%s] Entering ...\n", __func__);

    client_fd = getClientSocket();
    if (client_fd < 0) {
        printf("[%s] socket create error (error = %d)\n", __func__, errno);
        goto finish;
    }

    setSocketNonBlock(client_fd);

    printf("[%s] socket initialized\n", __func__);

#ifdef MULTI_THREADED_TIME_CONSUMING
    /*
     * EXPERIMENTAL: multi-threaded time consuming
     */
    pthread_t consumeTimeThread;
    if (pthread_create(&consumeTimeThread, NULL, consumeTimeWrapper, NULL)) {
        printf("[%s] pthread create error (error = %d)\n", __func__, errno);
        goto error;
    }
    /*
     * NOTE: It may need to consider edge cases:
     * usec_per_clock > 1000 (Consuming a clock takes over 1 sec)
     */
    usec_per_clock = getuSecPerClock();
    timeout_ms = (usec_per_clock + USEC_PER_MS - 1) / USEC_PER_MS;
#else
    timeout_ms = 0; // set pollfd as non-blocking
#endif

    /*
     * Set up a pollfd to poll() the client socket
     */
    pollfd poll_fd;
    poll_fd.fd      = client_fd;
    poll_fd.events  = POLLIN;

    while (true) {
#ifdef MULTI_THREADED_TIME_CONSUMING
        consumeTimeEvent(EXEC);
#else
        simulationTick();
#endif
        event_ready = poll(&poll_fd, 1, timeout_ms);

        if (event_ready < 0) {
            if (errno == EINTR)
                continue;

            printf("[%s] Error while polling socket! (error = %d)\n", __func__, errno);
            goto error;
        }
#ifdef MULTI_THREADED_TIME_CONSUMING
        else if (event_ready == 0) {
            printf("[%s] poll() timed out. Consuming clock ...\n", __func__);
            continue;
        }
#endif
        pkt_size = recv(client_fd, (void *)&packet, SIMIO_PACKET_SIZE, 0);

        if (pkt_size < 0) {
            if (errno == EAGAIN)
                continue;

            printf("[%s] recv() failed from %s:%d (errno = %d)\n", __func__, SIMIO_HOST, SIMIO_PORT, errno);
            goto error;
        }
        else if (pkt_size == SIMIO_PACKET_SIZE) {
            addr = packet.info.offset + TEST_BASE_ADDR;
            type = packet.info.type;
            size = packet.info.size;

            printf("[%s] recv() type=0x%04hx, size=%hd, offset=0x%016lx, data=0x%016lx from %s:%d\n", __func__,
                    type, size, addr, data, SIMIO_HOST, SIMIO_PORT);

            switch (type) {
                case STOP_SIMIO:
                {
                    goto error;
                }                    
                case READ_REQ:
                {
                    readDevice(&data, addr, size);
                    packet.data = data;
                    packet.info.type = READ_ACK;
                    break;
                }
                case WRITE_REQ:
                {
                    data = packet.data;
                    writeDevice(&data, addr, size);
                    packet.info.type = WRITE_ACK;
                    break;
                }                    
            }

            send(client_fd, &packet, sizeof(packet), 0);
        }
    }

error:
#ifdef MULTI_THREADED_TIME_CONSUMING
    consumeTimeEvent(STOP);
    pthread_join(consumeTimeThread, NULL);
#endif
    close(client_fd);
finish:
    printf("[%s] Exiting ...\n", __func__);

    return 0;
}
