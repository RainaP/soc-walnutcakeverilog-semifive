//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__BreakpointUnit(
  input         io_status_debug,
  input  [1:0]  io_status_prv,
  input  [2:0]  io_bp_0_control_action,
  input         io_bp_0_control_chain,
  input  [1:0]  io_bp_0_control_tmatch,
  input         io_bp_0_control_m,
  input         io_bp_0_control_s,
  input         io_bp_0_control_u,
  input         io_bp_0_control_x,
  input         io_bp_0_control_w,
  input         io_bp_0_control_r,
  input  [38:0] io_bp_0_address,
  input  [2:0]  io_bp_1_control_action,
  input         io_bp_1_control_chain,
  input  [1:0]  io_bp_1_control_tmatch,
  input         io_bp_1_control_m,
  input         io_bp_1_control_s,
  input         io_bp_1_control_u,
  input         io_bp_1_control_x,
  input         io_bp_1_control_w,
  input         io_bp_1_control_r,
  input  [38:0] io_bp_1_address,
  input  [2:0]  io_bp_2_control_action,
  input         io_bp_2_control_chain,
  input  [1:0]  io_bp_2_control_tmatch,
  input         io_bp_2_control_m,
  input         io_bp_2_control_s,
  input         io_bp_2_control_u,
  input         io_bp_2_control_x,
  input         io_bp_2_control_w,
  input         io_bp_2_control_r,
  input  [38:0] io_bp_2_address,
  input  [2:0]  io_bp_3_control_action,
  input         io_bp_3_control_chain,
  input  [1:0]  io_bp_3_control_tmatch,
  input         io_bp_3_control_m,
  input         io_bp_3_control_s,
  input         io_bp_3_control_u,
  input         io_bp_3_control_x,
  input         io_bp_3_control_w,
  input         io_bp_3_control_r,
  input  [38:0] io_bp_3_address,
  input  [2:0]  io_bp_4_control_action,
  input         io_bp_4_control_chain,
  input  [1:0]  io_bp_4_control_tmatch,
  input         io_bp_4_control_m,
  input         io_bp_4_control_s,
  input         io_bp_4_control_u,
  input         io_bp_4_control_x,
  input         io_bp_4_control_w,
  input         io_bp_4_control_r,
  input  [38:0] io_bp_4_address,
  input  [2:0]  io_bp_5_control_action,
  input         io_bp_5_control_chain,
  input  [1:0]  io_bp_5_control_tmatch,
  input         io_bp_5_control_m,
  input         io_bp_5_control_s,
  input         io_bp_5_control_u,
  input         io_bp_5_control_x,
  input         io_bp_5_control_w,
  input         io_bp_5_control_r,
  input  [38:0] io_bp_5_address,
  input  [2:0]  io_bp_6_control_action,
  input         io_bp_6_control_chain,
  input  [1:0]  io_bp_6_control_tmatch,
  input         io_bp_6_control_m,
  input         io_bp_6_control_s,
  input         io_bp_6_control_u,
  input         io_bp_6_control_x,
  input         io_bp_6_control_w,
  input         io_bp_6_control_r,
  input  [38:0] io_bp_6_address,
  input  [2:0]  io_bp_7_control_action,
  input         io_bp_7_control_chain,
  input  [1:0]  io_bp_7_control_tmatch,
  input         io_bp_7_control_m,
  input         io_bp_7_control_s,
  input         io_bp_7_control_u,
  input         io_bp_7_control_x,
  input         io_bp_7_control_w,
  input         io_bp_7_control_r,
  input  [38:0] io_bp_7_address,
  input  [2:0]  io_bp_8_control_action,
  input         io_bp_8_control_chain,
  input  [1:0]  io_bp_8_control_tmatch,
  input         io_bp_8_control_m,
  input         io_bp_8_control_s,
  input         io_bp_8_control_u,
  input         io_bp_8_control_x,
  input         io_bp_8_control_w,
  input         io_bp_8_control_r,
  input  [38:0] io_bp_8_address,
  input  [2:0]  io_bp_9_control_action,
  input         io_bp_9_control_chain,
  input  [1:0]  io_bp_9_control_tmatch,
  input         io_bp_9_control_m,
  input         io_bp_9_control_s,
  input         io_bp_9_control_u,
  input         io_bp_9_control_x,
  input         io_bp_9_control_w,
  input         io_bp_9_control_r,
  input  [38:0] io_bp_9_address,
  input  [2:0]  io_bp_10_control_action,
  input         io_bp_10_control_chain,
  input  [1:0]  io_bp_10_control_tmatch,
  input         io_bp_10_control_m,
  input         io_bp_10_control_s,
  input         io_bp_10_control_u,
  input         io_bp_10_control_x,
  input         io_bp_10_control_w,
  input         io_bp_10_control_r,
  input  [38:0] io_bp_10_address,
  input  [2:0]  io_bp_11_control_action,
  input         io_bp_11_control_chain,
  input  [1:0]  io_bp_11_control_tmatch,
  input         io_bp_11_control_m,
  input         io_bp_11_control_s,
  input         io_bp_11_control_u,
  input         io_bp_11_control_x,
  input         io_bp_11_control_w,
  input         io_bp_11_control_r,
  input  [38:0] io_bp_11_address,
  input  [2:0]  io_bp_12_control_action,
  input         io_bp_12_control_chain,
  input  [1:0]  io_bp_12_control_tmatch,
  input         io_bp_12_control_m,
  input         io_bp_12_control_s,
  input         io_bp_12_control_u,
  input         io_bp_12_control_x,
  input         io_bp_12_control_w,
  input         io_bp_12_control_r,
  input  [38:0] io_bp_12_address,
  input  [2:0]  io_bp_13_control_action,
  input         io_bp_13_control_chain,
  input  [1:0]  io_bp_13_control_tmatch,
  input         io_bp_13_control_m,
  input         io_bp_13_control_s,
  input         io_bp_13_control_u,
  input         io_bp_13_control_x,
  input         io_bp_13_control_w,
  input         io_bp_13_control_r,
  input  [38:0] io_bp_13_address,
  input  [2:0]  io_bp_14_control_action,
  input         io_bp_14_control_chain,
  input  [1:0]  io_bp_14_control_tmatch,
  input         io_bp_14_control_m,
  input         io_bp_14_control_s,
  input         io_bp_14_control_u,
  input         io_bp_14_control_x,
  input         io_bp_14_control_w,
  input         io_bp_14_control_r,
  input  [38:0] io_bp_14_address,
  input  [2:0]  io_bp_15_control_action,
  input  [1:0]  io_bp_15_control_tmatch,
  input         io_bp_15_control_m,
  input         io_bp_15_control_s,
  input         io_bp_15_control_u,
  input         io_bp_15_control_x,
  input         io_bp_15_control_w,
  input         io_bp_15_control_r,
  input  [38:0] io_bp_15_address,
  input  [38:0] io_pc,
  input  [38:0] io_ea,
  output        io_xcpt_if,
  output        io_xcpt_ld,
  output        io_xcpt_st,
  output        io_debug_if,
  output        io_debug_ld,
  output        io_debug_st,
  output        io_bpwatch_0_rvalid_0,
  output        io_bpwatch_0_wvalid_0,
  output        io_bpwatch_0_ivalid_0,
  output        io_bpwatch_1_rvalid_0,
  output        io_bpwatch_1_wvalid_0,
  output        io_bpwatch_1_ivalid_0,
  output        io_bpwatch_2_rvalid_0,
  output        io_bpwatch_2_wvalid_0,
  output        io_bpwatch_2_ivalid_0,
  output        io_bpwatch_3_rvalid_0,
  output        io_bpwatch_3_wvalid_0,
  output        io_bpwatch_3_ivalid_0,
  output        io_bpwatch_4_rvalid_0,
  output        io_bpwatch_4_wvalid_0,
  output        io_bpwatch_4_ivalid_0,
  output        io_bpwatch_5_rvalid_0,
  output        io_bpwatch_5_wvalid_0,
  output        io_bpwatch_5_ivalid_0,
  output        io_bpwatch_6_rvalid_0,
  output        io_bpwatch_6_wvalid_0,
  output        io_bpwatch_6_ivalid_0,
  output        io_bpwatch_7_rvalid_0,
  output        io_bpwatch_7_wvalid_0,
  output        io_bpwatch_7_ivalid_0,
  output        io_bpwatch_8_rvalid_0,
  output        io_bpwatch_8_wvalid_0,
  output        io_bpwatch_8_ivalid_0,
  output        io_bpwatch_9_rvalid_0,
  output        io_bpwatch_9_wvalid_0,
  output        io_bpwatch_9_ivalid_0,
  output        io_bpwatch_10_rvalid_0,
  output        io_bpwatch_10_wvalid_0,
  output        io_bpwatch_10_ivalid_0,
  output        io_bpwatch_11_rvalid_0,
  output        io_bpwatch_11_wvalid_0,
  output        io_bpwatch_11_ivalid_0,
  output        io_bpwatch_12_rvalid_0,
  output        io_bpwatch_12_wvalid_0,
  output        io_bpwatch_12_ivalid_0,
  output        io_bpwatch_13_rvalid_0,
  output        io_bpwatch_13_wvalid_0,
  output        io_bpwatch_13_ivalid_0,
  output        io_bpwatch_14_rvalid_0,
  output        io_bpwatch_14_wvalid_0,
  output        io_bpwatch_14_ivalid_0,
  output        io_bpwatch_15_rvalid_0,
  output        io_bpwatch_15_wvalid_0,
  output        io_bpwatch_15_ivalid_0
);
  wire [3:0] _en_T_1 = {io_bp_0_control_m,1'h0,io_bp_0_control_s,io_bp_0_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_2 = _en_T_1 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en = ~io_status_debug & _en_T_2[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_4 = io_ea >= io_bp_0_address ^ io_bp_0_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _r_T_5 = ~io_ea; // @[Breakpoint.scala 63:6]
  wire  r_lo_hi = io_bp_0_control_tmatch[0] & io_bp_0_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo = r_lo_hi & io_bp_0_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi = r_hi_lo & io_bp_0_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_9 = {r_hi_hi,r_hi_lo,r_lo_hi,io_bp_0_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_5 = {{35'd0}, _r_T_9}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_10 = _r_T_5 | _GEN_5; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_11 = ~io_bp_0_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_16 = _r_T_11 | _GEN_5; // @[Breakpoint.scala 63:33]
  wire  _r_T_17 = _r_T_10 == _r_T_16; // @[Breakpoint.scala 63:19]
  wire  _r_T_18 = io_bp_0_control_tmatch[1] ? _r_T_4 : _r_T_17; // @[Breakpoint.scala 69:8]
  wire  r = en & io_bp_0_control_r & _r_T_18; // @[Breakpoint.scala 107:32]
  wire  w = en & io_bp_0_control_w & _r_T_18; // @[Breakpoint.scala 108:32]
  wire  _x_T_4 = io_pc >= io_bp_0_address ^ io_bp_0_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_5 = ~io_pc; // @[Breakpoint.scala 63:6]
  wire [38:0] _x_T_10 = _x_T_5 | _GEN_5; // @[Breakpoint.scala 63:9]
  wire  _x_T_17 = _x_T_10 == _r_T_16; // @[Breakpoint.scala 63:19]
  wire  _x_T_18 = io_bp_0_control_tmatch[1] ? _x_T_4 : _x_T_17; // @[Breakpoint.scala 69:8]
  wire  x = en & io_bp_0_control_x & _x_T_18; // @[Breakpoint.scala 109:32]
  wire  end_ = ~io_bp_0_control_chain; // @[Breakpoint.scala 110:15]
  wire  _io_xcpt_ld_T = io_bp_0_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T = io_bp_0_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  wire  _GEN_0 = end_ & r & io_bp_0_control_action == 3'h0; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40 Breakpoint.scala 98:14]
  wire  _GEN_1 = end_ & r & io_bp_0_control_action == 3'h1; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73 Breakpoint.scala 101:15]
  wire  _GEN_3 = end_ & w & _io_xcpt_ld_T; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40 Breakpoint.scala 99:14]
  wire  _GEN_4 = end_ & w & _io_debug_ld_T; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73 Breakpoint.scala 102:15]
  wire  _GEN_7 = end_ & x & _io_xcpt_ld_T; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40 Breakpoint.scala 97:14]
  wire  _GEN_8 = end_ & x & _io_debug_ld_T; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73 Breakpoint.scala 100:15]
  wire  _T_6 = end_ | r; // @[Breakpoint.scala 123:10]
  wire  _T_7 = end_ | w; // @[Breakpoint.scala 123:20]
  wire  _T_8 = end_ | x; // @[Breakpoint.scala 123:30]
  wire [3:0] _en_T_5 = {io_bp_1_control_m,1'h0,io_bp_1_control_s,io_bp_1_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_6 = _en_T_5 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en_1 = ~io_status_debug & _en_T_6[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_24 = io_ea >= io_bp_1_address ^ io_bp_1_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire  r_lo_hi_2 = io_bp_1_control_tmatch[0] & io_bp_1_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo_2 = r_lo_hi_2 & io_bp_1_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi_2 = r_hi_lo_2 & io_bp_1_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_29 = {r_hi_hi_2,r_hi_lo_2,r_lo_hi_2,io_bp_1_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_38 = {{35'd0}, _r_T_29}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_30 = _r_T_5 | _GEN_38; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_31 = ~io_bp_1_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_36 = _r_T_31 | _GEN_38; // @[Breakpoint.scala 63:33]
  wire  _r_T_37 = _r_T_30 == _r_T_36; // @[Breakpoint.scala 63:19]
  wire  _r_T_38 = io_bp_1_control_tmatch[1] ? _r_T_24 : _r_T_37; // @[Breakpoint.scala 69:8]
  wire  r_1 = en_1 & io_bp_1_control_r & _r_T_38; // @[Breakpoint.scala 107:32]
  wire  w_1 = en_1 & io_bp_1_control_w & _r_T_38; // @[Breakpoint.scala 108:32]
  wire  _x_T_24 = io_pc >= io_bp_1_address ^ io_bp_1_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_30 = _x_T_5 | _GEN_38; // @[Breakpoint.scala 63:9]
  wire  _x_T_37 = _x_T_30 == _r_T_36; // @[Breakpoint.scala 63:19]
  wire  _x_T_38 = io_bp_1_control_tmatch[1] ? _x_T_24 : _x_T_37; // @[Breakpoint.scala 69:8]
  wire  x_1 = en_1 & io_bp_1_control_x & _x_T_38; // @[Breakpoint.scala 109:32]
  wire  end_1 = ~io_bp_1_control_chain; // @[Breakpoint.scala 110:15]
  wire  _io_xcpt_ld_T_1 = io_bp_1_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T_1 = io_bp_1_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  wire  _GEN_11 = end_1 & r_1 & _T_6 ? io_bp_1_control_action == 3'h0 : _GEN_0; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40]
  wire  _GEN_12 = end_1 & r_1 & _T_6 ? io_bp_1_control_action == 3'h1 : _GEN_1; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73]
  wire  _GEN_14 = end_1 & w_1 & _T_7 ? _io_xcpt_ld_T_1 : _GEN_3; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40]
  wire  _GEN_15 = end_1 & w_1 & _T_7 ? _io_debug_ld_T_1 : _GEN_4; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73]
  wire  _GEN_18 = end_1 & x_1 & _T_8 ? _io_xcpt_ld_T_1 : _GEN_7; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40]
  wire  _GEN_19 = end_1 & x_1 & _T_8 ? _io_debug_ld_T_1 : _GEN_8; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73]
  wire  _T_15 = end_1 | r_1; // @[Breakpoint.scala 123:10]
  wire  _T_16 = end_1 | w_1; // @[Breakpoint.scala 123:20]
  wire  _T_17 = end_1 | x_1; // @[Breakpoint.scala 123:30]
  wire [3:0] _en_T_9 = {io_bp_2_control_m,1'h0,io_bp_2_control_s,io_bp_2_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_10 = _en_T_9 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en_2 = ~io_status_debug & _en_T_10[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_44 = io_ea >= io_bp_2_address ^ io_bp_2_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire  r_lo_hi_4 = io_bp_2_control_tmatch[0] & io_bp_2_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo_4 = r_lo_hi_4 & io_bp_2_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi_4 = r_hi_lo_4 & io_bp_2_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_49 = {r_hi_hi_4,r_hi_lo_4,r_lo_hi_4,io_bp_2_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_71 = {{35'd0}, _r_T_49}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_50 = _r_T_5 | _GEN_71; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_51 = ~io_bp_2_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_56 = _r_T_51 | _GEN_71; // @[Breakpoint.scala 63:33]
  wire  _r_T_57 = _r_T_50 == _r_T_56; // @[Breakpoint.scala 63:19]
  wire  _r_T_58 = io_bp_2_control_tmatch[1] ? _r_T_44 : _r_T_57; // @[Breakpoint.scala 69:8]
  wire  r_2 = en_2 & io_bp_2_control_r & _r_T_58; // @[Breakpoint.scala 107:32]
  wire  w_2 = en_2 & io_bp_2_control_w & _r_T_58; // @[Breakpoint.scala 108:32]
  wire  _x_T_44 = io_pc >= io_bp_2_address ^ io_bp_2_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_50 = _x_T_5 | _GEN_71; // @[Breakpoint.scala 63:9]
  wire  _x_T_57 = _x_T_50 == _r_T_56; // @[Breakpoint.scala 63:19]
  wire  _x_T_58 = io_bp_2_control_tmatch[1] ? _x_T_44 : _x_T_57; // @[Breakpoint.scala 69:8]
  wire  x_2 = en_2 & io_bp_2_control_x & _x_T_58; // @[Breakpoint.scala 109:32]
  wire  end_2 = ~io_bp_2_control_chain; // @[Breakpoint.scala 110:15]
  wire  _io_xcpt_ld_T_2 = io_bp_2_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T_2 = io_bp_2_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  wire  _GEN_22 = end_2 & r_2 & _T_15 ? io_bp_2_control_action == 3'h0 : _GEN_11; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40]
  wire  _GEN_23 = end_2 & r_2 & _T_15 ? io_bp_2_control_action == 3'h1 : _GEN_12; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73]
  wire  _GEN_25 = end_2 & w_2 & _T_16 ? _io_xcpt_ld_T_2 : _GEN_14; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40]
  wire  _GEN_26 = end_2 & w_2 & _T_16 ? _io_debug_ld_T_2 : _GEN_15; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73]
  wire  _GEN_29 = end_2 & x_2 & _T_17 ? _io_xcpt_ld_T_2 : _GEN_18; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40]
  wire  _GEN_30 = end_2 & x_2 & _T_17 ? _io_debug_ld_T_2 : _GEN_19; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73]
  wire  _T_24 = end_2 | r_2; // @[Breakpoint.scala 123:10]
  wire  _T_25 = end_2 | w_2; // @[Breakpoint.scala 123:20]
  wire  _T_26 = end_2 | x_2; // @[Breakpoint.scala 123:30]
  wire [3:0] _en_T_13 = {io_bp_3_control_m,1'h0,io_bp_3_control_s,io_bp_3_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_14 = _en_T_13 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en_3 = ~io_status_debug & _en_T_14[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_64 = io_ea >= io_bp_3_address ^ io_bp_3_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire  r_lo_hi_6 = io_bp_3_control_tmatch[0] & io_bp_3_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo_6 = r_lo_hi_6 & io_bp_3_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi_6 = r_hi_lo_6 & io_bp_3_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_69 = {r_hi_hi_6,r_hi_lo_6,r_lo_hi_6,io_bp_3_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_104 = {{35'd0}, _r_T_69}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_70 = _r_T_5 | _GEN_104; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_71 = ~io_bp_3_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_76 = _r_T_71 | _GEN_104; // @[Breakpoint.scala 63:33]
  wire  _r_T_77 = _r_T_70 == _r_T_76; // @[Breakpoint.scala 63:19]
  wire  _r_T_78 = io_bp_3_control_tmatch[1] ? _r_T_64 : _r_T_77; // @[Breakpoint.scala 69:8]
  wire  r_3 = en_3 & io_bp_3_control_r & _r_T_78; // @[Breakpoint.scala 107:32]
  wire  w_3 = en_3 & io_bp_3_control_w & _r_T_78; // @[Breakpoint.scala 108:32]
  wire  _x_T_64 = io_pc >= io_bp_3_address ^ io_bp_3_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_70 = _x_T_5 | _GEN_104; // @[Breakpoint.scala 63:9]
  wire  _x_T_77 = _x_T_70 == _r_T_76; // @[Breakpoint.scala 63:19]
  wire  _x_T_78 = io_bp_3_control_tmatch[1] ? _x_T_64 : _x_T_77; // @[Breakpoint.scala 69:8]
  wire  x_3 = en_3 & io_bp_3_control_x & _x_T_78; // @[Breakpoint.scala 109:32]
  wire  end_3 = ~io_bp_3_control_chain; // @[Breakpoint.scala 110:15]
  wire  _io_xcpt_ld_T_3 = io_bp_3_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T_3 = io_bp_3_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  wire  _GEN_33 = end_3 & r_3 & _T_24 ? io_bp_3_control_action == 3'h0 : _GEN_22; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40]
  wire  _GEN_34 = end_3 & r_3 & _T_24 ? io_bp_3_control_action == 3'h1 : _GEN_23; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73]
  wire  _GEN_36 = end_3 & w_3 & _T_25 ? _io_xcpt_ld_T_3 : _GEN_25; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40]
  wire  _GEN_37 = end_3 & w_3 & _T_25 ? _io_debug_ld_T_3 : _GEN_26; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73]
  wire  _GEN_40 = end_3 & x_3 & _T_26 ? _io_xcpt_ld_T_3 : _GEN_29; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40]
  wire  _GEN_41 = end_3 & x_3 & _T_26 ? _io_debug_ld_T_3 : _GEN_30; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73]
  wire  _T_33 = end_3 | r_3; // @[Breakpoint.scala 123:10]
  wire  _T_34 = end_3 | w_3; // @[Breakpoint.scala 123:20]
  wire  _T_35 = end_3 | x_3; // @[Breakpoint.scala 123:30]
  wire [3:0] _en_T_17 = {io_bp_4_control_m,1'h0,io_bp_4_control_s,io_bp_4_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_18 = _en_T_17 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en_4 = ~io_status_debug & _en_T_18[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_84 = io_ea >= io_bp_4_address ^ io_bp_4_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire  r_lo_hi_8 = io_bp_4_control_tmatch[0] & io_bp_4_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo_8 = r_lo_hi_8 & io_bp_4_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi_8 = r_hi_lo_8 & io_bp_4_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_89 = {r_hi_hi_8,r_hi_lo_8,r_lo_hi_8,io_bp_4_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_137 = {{35'd0}, _r_T_89}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_90 = _r_T_5 | _GEN_137; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_91 = ~io_bp_4_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_96 = _r_T_91 | _GEN_137; // @[Breakpoint.scala 63:33]
  wire  _r_T_97 = _r_T_90 == _r_T_96; // @[Breakpoint.scala 63:19]
  wire  _r_T_98 = io_bp_4_control_tmatch[1] ? _r_T_84 : _r_T_97; // @[Breakpoint.scala 69:8]
  wire  r_4 = en_4 & io_bp_4_control_r & _r_T_98; // @[Breakpoint.scala 107:32]
  wire  w_4 = en_4 & io_bp_4_control_w & _r_T_98; // @[Breakpoint.scala 108:32]
  wire  _x_T_84 = io_pc >= io_bp_4_address ^ io_bp_4_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_90 = _x_T_5 | _GEN_137; // @[Breakpoint.scala 63:9]
  wire  _x_T_97 = _x_T_90 == _r_T_96; // @[Breakpoint.scala 63:19]
  wire  _x_T_98 = io_bp_4_control_tmatch[1] ? _x_T_84 : _x_T_97; // @[Breakpoint.scala 69:8]
  wire  x_4 = en_4 & io_bp_4_control_x & _x_T_98; // @[Breakpoint.scala 109:32]
  wire  end_4 = ~io_bp_4_control_chain; // @[Breakpoint.scala 110:15]
  wire  _io_xcpt_ld_T_4 = io_bp_4_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T_4 = io_bp_4_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  wire  _GEN_44 = end_4 & r_4 & _T_33 ? io_bp_4_control_action == 3'h0 : _GEN_33; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40]
  wire  _GEN_45 = end_4 & r_4 & _T_33 ? io_bp_4_control_action == 3'h1 : _GEN_34; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73]
  wire  _GEN_47 = end_4 & w_4 & _T_34 ? _io_xcpt_ld_T_4 : _GEN_36; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40]
  wire  _GEN_48 = end_4 & w_4 & _T_34 ? _io_debug_ld_T_4 : _GEN_37; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73]
  wire  _GEN_51 = end_4 & x_4 & _T_35 ? _io_xcpt_ld_T_4 : _GEN_40; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40]
  wire  _GEN_52 = end_4 & x_4 & _T_35 ? _io_debug_ld_T_4 : _GEN_41; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73]
  wire  _T_42 = end_4 | r_4; // @[Breakpoint.scala 123:10]
  wire  _T_43 = end_4 | w_4; // @[Breakpoint.scala 123:20]
  wire  _T_44 = end_4 | x_4; // @[Breakpoint.scala 123:30]
  wire [3:0] _en_T_21 = {io_bp_5_control_m,1'h0,io_bp_5_control_s,io_bp_5_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_22 = _en_T_21 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en_5 = ~io_status_debug & _en_T_22[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_104 = io_ea >= io_bp_5_address ^ io_bp_5_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire  r_lo_hi_10 = io_bp_5_control_tmatch[0] & io_bp_5_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo_10 = r_lo_hi_10 & io_bp_5_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi_10 = r_hi_lo_10 & io_bp_5_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_109 = {r_hi_hi_10,r_hi_lo_10,r_lo_hi_10,io_bp_5_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_170 = {{35'd0}, _r_T_109}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_110 = _r_T_5 | _GEN_170; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_111 = ~io_bp_5_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_116 = _r_T_111 | _GEN_170; // @[Breakpoint.scala 63:33]
  wire  _r_T_117 = _r_T_110 == _r_T_116; // @[Breakpoint.scala 63:19]
  wire  _r_T_118 = io_bp_5_control_tmatch[1] ? _r_T_104 : _r_T_117; // @[Breakpoint.scala 69:8]
  wire  r_5 = en_5 & io_bp_5_control_r & _r_T_118; // @[Breakpoint.scala 107:32]
  wire  w_5 = en_5 & io_bp_5_control_w & _r_T_118; // @[Breakpoint.scala 108:32]
  wire  _x_T_104 = io_pc >= io_bp_5_address ^ io_bp_5_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_110 = _x_T_5 | _GEN_170; // @[Breakpoint.scala 63:9]
  wire  _x_T_117 = _x_T_110 == _r_T_116; // @[Breakpoint.scala 63:19]
  wire  _x_T_118 = io_bp_5_control_tmatch[1] ? _x_T_104 : _x_T_117; // @[Breakpoint.scala 69:8]
  wire  x_5 = en_5 & io_bp_5_control_x & _x_T_118; // @[Breakpoint.scala 109:32]
  wire  end_5 = ~io_bp_5_control_chain; // @[Breakpoint.scala 110:15]
  wire  _io_xcpt_ld_T_5 = io_bp_5_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T_5 = io_bp_5_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  wire  _GEN_55 = end_5 & r_5 & _T_42 ? io_bp_5_control_action == 3'h0 : _GEN_44; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40]
  wire  _GEN_56 = end_5 & r_5 & _T_42 ? io_bp_5_control_action == 3'h1 : _GEN_45; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73]
  wire  _GEN_58 = end_5 & w_5 & _T_43 ? _io_xcpt_ld_T_5 : _GEN_47; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40]
  wire  _GEN_59 = end_5 & w_5 & _T_43 ? _io_debug_ld_T_5 : _GEN_48; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73]
  wire  _GEN_62 = end_5 & x_5 & _T_44 ? _io_xcpt_ld_T_5 : _GEN_51; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40]
  wire  _GEN_63 = end_5 & x_5 & _T_44 ? _io_debug_ld_T_5 : _GEN_52; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73]
  wire  _T_51 = end_5 | r_5; // @[Breakpoint.scala 123:10]
  wire  _T_52 = end_5 | w_5; // @[Breakpoint.scala 123:20]
  wire  _T_53 = end_5 | x_5; // @[Breakpoint.scala 123:30]
  wire [3:0] _en_T_25 = {io_bp_6_control_m,1'h0,io_bp_6_control_s,io_bp_6_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_26 = _en_T_25 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en_6 = ~io_status_debug & _en_T_26[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_124 = io_ea >= io_bp_6_address ^ io_bp_6_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire  r_lo_hi_12 = io_bp_6_control_tmatch[0] & io_bp_6_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo_12 = r_lo_hi_12 & io_bp_6_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi_12 = r_hi_lo_12 & io_bp_6_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_129 = {r_hi_hi_12,r_hi_lo_12,r_lo_hi_12,io_bp_6_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_180 = {{35'd0}, _r_T_129}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_130 = _r_T_5 | _GEN_180; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_131 = ~io_bp_6_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_136 = _r_T_131 | _GEN_180; // @[Breakpoint.scala 63:33]
  wire  _r_T_137 = _r_T_130 == _r_T_136; // @[Breakpoint.scala 63:19]
  wire  _r_T_138 = io_bp_6_control_tmatch[1] ? _r_T_124 : _r_T_137; // @[Breakpoint.scala 69:8]
  wire  r_6 = en_6 & io_bp_6_control_r & _r_T_138; // @[Breakpoint.scala 107:32]
  wire  w_6 = en_6 & io_bp_6_control_w & _r_T_138; // @[Breakpoint.scala 108:32]
  wire  _x_T_124 = io_pc >= io_bp_6_address ^ io_bp_6_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_130 = _x_T_5 | _GEN_180; // @[Breakpoint.scala 63:9]
  wire  _x_T_137 = _x_T_130 == _r_T_136; // @[Breakpoint.scala 63:19]
  wire  _x_T_138 = io_bp_6_control_tmatch[1] ? _x_T_124 : _x_T_137; // @[Breakpoint.scala 69:8]
  wire  x_6 = en_6 & io_bp_6_control_x & _x_T_138; // @[Breakpoint.scala 109:32]
  wire  end_6 = ~io_bp_6_control_chain; // @[Breakpoint.scala 110:15]
  wire  _io_xcpt_ld_T_6 = io_bp_6_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T_6 = io_bp_6_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  wire  _GEN_66 = end_6 & r_6 & _T_51 ? io_bp_6_control_action == 3'h0 : _GEN_55; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40]
  wire  _GEN_67 = end_6 & r_6 & _T_51 ? io_bp_6_control_action == 3'h1 : _GEN_56; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73]
  wire  _GEN_69 = end_6 & w_6 & _T_52 ? _io_xcpt_ld_T_6 : _GEN_58; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40]
  wire  _GEN_70 = end_6 & w_6 & _T_52 ? _io_debug_ld_T_6 : _GEN_59; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73]
  wire  _GEN_73 = end_6 & x_6 & _T_53 ? _io_xcpt_ld_T_6 : _GEN_62; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40]
  wire  _GEN_74 = end_6 & x_6 & _T_53 ? _io_debug_ld_T_6 : _GEN_63; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73]
  wire  _T_60 = end_6 | r_6; // @[Breakpoint.scala 123:10]
  wire  _T_61 = end_6 | w_6; // @[Breakpoint.scala 123:20]
  wire  _T_62 = end_6 | x_6; // @[Breakpoint.scala 123:30]
  wire [3:0] _en_T_29 = {io_bp_7_control_m,1'h0,io_bp_7_control_s,io_bp_7_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_30 = _en_T_29 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en_7 = ~io_status_debug & _en_T_30[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_144 = io_ea >= io_bp_7_address ^ io_bp_7_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire  r_lo_hi_14 = io_bp_7_control_tmatch[0] & io_bp_7_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo_14 = r_lo_hi_14 & io_bp_7_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi_14 = r_hi_lo_14 & io_bp_7_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_149 = {r_hi_hi_14,r_hi_lo_14,r_lo_hi_14,io_bp_7_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_186 = {{35'd0}, _r_T_149}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_150 = _r_T_5 | _GEN_186; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_151 = ~io_bp_7_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_156 = _r_T_151 | _GEN_186; // @[Breakpoint.scala 63:33]
  wire  _r_T_157 = _r_T_150 == _r_T_156; // @[Breakpoint.scala 63:19]
  wire  _r_T_158 = io_bp_7_control_tmatch[1] ? _r_T_144 : _r_T_157; // @[Breakpoint.scala 69:8]
  wire  r_7 = en_7 & io_bp_7_control_r & _r_T_158; // @[Breakpoint.scala 107:32]
  wire  w_7 = en_7 & io_bp_7_control_w & _r_T_158; // @[Breakpoint.scala 108:32]
  wire  _x_T_144 = io_pc >= io_bp_7_address ^ io_bp_7_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_150 = _x_T_5 | _GEN_186; // @[Breakpoint.scala 63:9]
  wire  _x_T_157 = _x_T_150 == _r_T_156; // @[Breakpoint.scala 63:19]
  wire  _x_T_158 = io_bp_7_control_tmatch[1] ? _x_T_144 : _x_T_157; // @[Breakpoint.scala 69:8]
  wire  x_7 = en_7 & io_bp_7_control_x & _x_T_158; // @[Breakpoint.scala 109:32]
  wire  end_7 = ~io_bp_7_control_chain; // @[Breakpoint.scala 110:15]
  wire  _io_xcpt_ld_T_7 = io_bp_7_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T_7 = io_bp_7_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  wire  _GEN_77 = end_7 & r_7 & _T_60 ? io_bp_7_control_action == 3'h0 : _GEN_66; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40]
  wire  _GEN_78 = end_7 & r_7 & _T_60 ? io_bp_7_control_action == 3'h1 : _GEN_67; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73]
  wire  _GEN_80 = end_7 & w_7 & _T_61 ? _io_xcpt_ld_T_7 : _GEN_69; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40]
  wire  _GEN_81 = end_7 & w_7 & _T_61 ? _io_debug_ld_T_7 : _GEN_70; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73]
  wire  _GEN_84 = end_7 & x_7 & _T_62 ? _io_xcpt_ld_T_7 : _GEN_73; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40]
  wire  _GEN_85 = end_7 & x_7 & _T_62 ? _io_debug_ld_T_7 : _GEN_74; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73]
  wire  _T_69 = end_7 | r_7; // @[Breakpoint.scala 123:10]
  wire  _T_70 = end_7 | w_7; // @[Breakpoint.scala 123:20]
  wire  _T_71 = end_7 | x_7; // @[Breakpoint.scala 123:30]
  wire [3:0] _en_T_33 = {io_bp_8_control_m,1'h0,io_bp_8_control_s,io_bp_8_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_34 = _en_T_33 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en_8 = ~io_status_debug & _en_T_34[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_164 = io_ea >= io_bp_8_address ^ io_bp_8_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire  r_lo_hi_16 = io_bp_8_control_tmatch[0] & io_bp_8_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo_16 = r_lo_hi_16 & io_bp_8_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi_16 = r_hi_lo_16 & io_bp_8_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_169 = {r_hi_hi_16,r_hi_lo_16,r_lo_hi_16,io_bp_8_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_192 = {{35'd0}, _r_T_169}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_170 = _r_T_5 | _GEN_192; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_171 = ~io_bp_8_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_176 = _r_T_171 | _GEN_192; // @[Breakpoint.scala 63:33]
  wire  _r_T_177 = _r_T_170 == _r_T_176; // @[Breakpoint.scala 63:19]
  wire  _r_T_178 = io_bp_8_control_tmatch[1] ? _r_T_164 : _r_T_177; // @[Breakpoint.scala 69:8]
  wire  r_8 = en_8 & io_bp_8_control_r & _r_T_178; // @[Breakpoint.scala 107:32]
  wire  w_8 = en_8 & io_bp_8_control_w & _r_T_178; // @[Breakpoint.scala 108:32]
  wire  _x_T_164 = io_pc >= io_bp_8_address ^ io_bp_8_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_170 = _x_T_5 | _GEN_192; // @[Breakpoint.scala 63:9]
  wire  _x_T_177 = _x_T_170 == _r_T_176; // @[Breakpoint.scala 63:19]
  wire  _x_T_178 = io_bp_8_control_tmatch[1] ? _x_T_164 : _x_T_177; // @[Breakpoint.scala 69:8]
  wire  x_8 = en_8 & io_bp_8_control_x & _x_T_178; // @[Breakpoint.scala 109:32]
  wire  end_8 = ~io_bp_8_control_chain; // @[Breakpoint.scala 110:15]
  wire  _io_xcpt_ld_T_8 = io_bp_8_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T_8 = io_bp_8_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  wire  _GEN_88 = end_8 & r_8 & _T_69 ? io_bp_8_control_action == 3'h0 : _GEN_77; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40]
  wire  _GEN_89 = end_8 & r_8 & _T_69 ? io_bp_8_control_action == 3'h1 : _GEN_78; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73]
  wire  _GEN_91 = end_8 & w_8 & _T_70 ? _io_xcpt_ld_T_8 : _GEN_80; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40]
  wire  _GEN_92 = end_8 & w_8 & _T_70 ? _io_debug_ld_T_8 : _GEN_81; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73]
  wire  _GEN_95 = end_8 & x_8 & _T_71 ? _io_xcpt_ld_T_8 : _GEN_84; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40]
  wire  _GEN_96 = end_8 & x_8 & _T_71 ? _io_debug_ld_T_8 : _GEN_85; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73]
  wire  _T_78 = end_8 | r_8; // @[Breakpoint.scala 123:10]
  wire  _T_79 = end_8 | w_8; // @[Breakpoint.scala 123:20]
  wire  _T_80 = end_8 | x_8; // @[Breakpoint.scala 123:30]
  wire [3:0] _en_T_37 = {io_bp_9_control_m,1'h0,io_bp_9_control_s,io_bp_9_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_38 = _en_T_37 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en_9 = ~io_status_debug & _en_T_38[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_184 = io_ea >= io_bp_9_address ^ io_bp_9_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire  r_lo_hi_18 = io_bp_9_control_tmatch[0] & io_bp_9_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo_18 = r_lo_hi_18 & io_bp_9_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi_18 = r_hi_lo_18 & io_bp_9_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_189 = {r_hi_hi_18,r_hi_lo_18,r_lo_hi_18,io_bp_9_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_198 = {{35'd0}, _r_T_189}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_190 = _r_T_5 | _GEN_198; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_191 = ~io_bp_9_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_196 = _r_T_191 | _GEN_198; // @[Breakpoint.scala 63:33]
  wire  _r_T_197 = _r_T_190 == _r_T_196; // @[Breakpoint.scala 63:19]
  wire  _r_T_198 = io_bp_9_control_tmatch[1] ? _r_T_184 : _r_T_197; // @[Breakpoint.scala 69:8]
  wire  r_9 = en_9 & io_bp_9_control_r & _r_T_198; // @[Breakpoint.scala 107:32]
  wire  w_9 = en_9 & io_bp_9_control_w & _r_T_198; // @[Breakpoint.scala 108:32]
  wire  _x_T_184 = io_pc >= io_bp_9_address ^ io_bp_9_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_190 = _x_T_5 | _GEN_198; // @[Breakpoint.scala 63:9]
  wire  _x_T_197 = _x_T_190 == _r_T_196; // @[Breakpoint.scala 63:19]
  wire  _x_T_198 = io_bp_9_control_tmatch[1] ? _x_T_184 : _x_T_197; // @[Breakpoint.scala 69:8]
  wire  x_9 = en_9 & io_bp_9_control_x & _x_T_198; // @[Breakpoint.scala 109:32]
  wire  end_9 = ~io_bp_9_control_chain; // @[Breakpoint.scala 110:15]
  wire  _io_xcpt_ld_T_9 = io_bp_9_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T_9 = io_bp_9_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  wire  _GEN_99 = end_9 & r_9 & _T_78 ? io_bp_9_control_action == 3'h0 : _GEN_88; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40]
  wire  _GEN_100 = end_9 & r_9 & _T_78 ? io_bp_9_control_action == 3'h1 : _GEN_89; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73]
  wire  _GEN_102 = end_9 & w_9 & _T_79 ? _io_xcpt_ld_T_9 : _GEN_91; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40]
  wire  _GEN_103 = end_9 & w_9 & _T_79 ? _io_debug_ld_T_9 : _GEN_92; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73]
  wire  _GEN_106 = end_9 & x_9 & _T_80 ? _io_xcpt_ld_T_9 : _GEN_95; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40]
  wire  _GEN_107 = end_9 & x_9 & _T_80 ? _io_debug_ld_T_9 : _GEN_96; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73]
  wire  _T_87 = end_9 | r_9; // @[Breakpoint.scala 123:10]
  wire  _T_88 = end_9 | w_9; // @[Breakpoint.scala 123:20]
  wire  _T_89 = end_9 | x_9; // @[Breakpoint.scala 123:30]
  wire [3:0] _en_T_41 = {io_bp_10_control_m,1'h0,io_bp_10_control_s,io_bp_10_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_42 = _en_T_41 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en_10 = ~io_status_debug & _en_T_42[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_204 = io_ea >= io_bp_10_address ^ io_bp_10_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire  r_lo_hi_20 = io_bp_10_control_tmatch[0] & io_bp_10_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo_20 = r_lo_hi_20 & io_bp_10_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi_20 = r_hi_lo_20 & io_bp_10_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_209 = {r_hi_hi_20,r_hi_lo_20,r_lo_hi_20,io_bp_10_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_204 = {{35'd0}, _r_T_209}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_210 = _r_T_5 | _GEN_204; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_211 = ~io_bp_10_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_216 = _r_T_211 | _GEN_204; // @[Breakpoint.scala 63:33]
  wire  _r_T_217 = _r_T_210 == _r_T_216; // @[Breakpoint.scala 63:19]
  wire  _r_T_218 = io_bp_10_control_tmatch[1] ? _r_T_204 : _r_T_217; // @[Breakpoint.scala 69:8]
  wire  r_10 = en_10 & io_bp_10_control_r & _r_T_218; // @[Breakpoint.scala 107:32]
  wire  w_10 = en_10 & io_bp_10_control_w & _r_T_218; // @[Breakpoint.scala 108:32]
  wire  _x_T_204 = io_pc >= io_bp_10_address ^ io_bp_10_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_210 = _x_T_5 | _GEN_204; // @[Breakpoint.scala 63:9]
  wire  _x_T_217 = _x_T_210 == _r_T_216; // @[Breakpoint.scala 63:19]
  wire  _x_T_218 = io_bp_10_control_tmatch[1] ? _x_T_204 : _x_T_217; // @[Breakpoint.scala 69:8]
  wire  x_10 = en_10 & io_bp_10_control_x & _x_T_218; // @[Breakpoint.scala 109:32]
  wire  end_10 = ~io_bp_10_control_chain; // @[Breakpoint.scala 110:15]
  wire  _io_xcpt_ld_T_10 = io_bp_10_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T_10 = io_bp_10_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  wire  _GEN_110 = end_10 & r_10 & _T_87 ? io_bp_10_control_action == 3'h0 : _GEN_99; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40]
  wire  _GEN_111 = end_10 & r_10 & _T_87 ? io_bp_10_control_action == 3'h1 : _GEN_100; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73]
  wire  _GEN_113 = end_10 & w_10 & _T_88 ? _io_xcpt_ld_T_10 : _GEN_102; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40]
  wire  _GEN_114 = end_10 & w_10 & _T_88 ? _io_debug_ld_T_10 : _GEN_103; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73]
  wire  _GEN_117 = end_10 & x_10 & _T_89 ? _io_xcpt_ld_T_10 : _GEN_106; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40]
  wire  _GEN_118 = end_10 & x_10 & _T_89 ? _io_debug_ld_T_10 : _GEN_107; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73]
  wire  _T_96 = end_10 | r_10; // @[Breakpoint.scala 123:10]
  wire  _T_97 = end_10 | w_10; // @[Breakpoint.scala 123:20]
  wire  _T_98 = end_10 | x_10; // @[Breakpoint.scala 123:30]
  wire [3:0] _en_T_45 = {io_bp_11_control_m,1'h0,io_bp_11_control_s,io_bp_11_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_46 = _en_T_45 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en_11 = ~io_status_debug & _en_T_46[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_224 = io_ea >= io_bp_11_address ^ io_bp_11_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire  r_lo_hi_22 = io_bp_11_control_tmatch[0] & io_bp_11_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo_22 = r_lo_hi_22 & io_bp_11_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi_22 = r_hi_lo_22 & io_bp_11_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_229 = {r_hi_hi_22,r_hi_lo_22,r_lo_hi_22,io_bp_11_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_210 = {{35'd0}, _r_T_229}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_230 = _r_T_5 | _GEN_210; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_231 = ~io_bp_11_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_236 = _r_T_231 | _GEN_210; // @[Breakpoint.scala 63:33]
  wire  _r_T_237 = _r_T_230 == _r_T_236; // @[Breakpoint.scala 63:19]
  wire  _r_T_238 = io_bp_11_control_tmatch[1] ? _r_T_224 : _r_T_237; // @[Breakpoint.scala 69:8]
  wire  r_11 = en_11 & io_bp_11_control_r & _r_T_238; // @[Breakpoint.scala 107:32]
  wire  w_11 = en_11 & io_bp_11_control_w & _r_T_238; // @[Breakpoint.scala 108:32]
  wire  _x_T_224 = io_pc >= io_bp_11_address ^ io_bp_11_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_230 = _x_T_5 | _GEN_210; // @[Breakpoint.scala 63:9]
  wire  _x_T_237 = _x_T_230 == _r_T_236; // @[Breakpoint.scala 63:19]
  wire  _x_T_238 = io_bp_11_control_tmatch[1] ? _x_T_224 : _x_T_237; // @[Breakpoint.scala 69:8]
  wire  x_11 = en_11 & io_bp_11_control_x & _x_T_238; // @[Breakpoint.scala 109:32]
  wire  end_11 = ~io_bp_11_control_chain; // @[Breakpoint.scala 110:15]
  wire  _io_xcpt_ld_T_11 = io_bp_11_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T_11 = io_bp_11_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  wire  _GEN_121 = end_11 & r_11 & _T_96 ? io_bp_11_control_action == 3'h0 : _GEN_110; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40]
  wire  _GEN_122 = end_11 & r_11 & _T_96 ? io_bp_11_control_action == 3'h1 : _GEN_111; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73]
  wire  _GEN_124 = end_11 & w_11 & _T_97 ? _io_xcpt_ld_T_11 : _GEN_113; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40]
  wire  _GEN_125 = end_11 & w_11 & _T_97 ? _io_debug_ld_T_11 : _GEN_114; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73]
  wire  _GEN_128 = end_11 & x_11 & _T_98 ? _io_xcpt_ld_T_11 : _GEN_117; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40]
  wire  _GEN_129 = end_11 & x_11 & _T_98 ? _io_debug_ld_T_11 : _GEN_118; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73]
  wire  _T_105 = end_11 | r_11; // @[Breakpoint.scala 123:10]
  wire  _T_106 = end_11 | w_11; // @[Breakpoint.scala 123:20]
  wire  _T_107 = end_11 | x_11; // @[Breakpoint.scala 123:30]
  wire [3:0] _en_T_49 = {io_bp_12_control_m,1'h0,io_bp_12_control_s,io_bp_12_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_50 = _en_T_49 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en_12 = ~io_status_debug & _en_T_50[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_244 = io_ea >= io_bp_12_address ^ io_bp_12_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire  r_lo_hi_24 = io_bp_12_control_tmatch[0] & io_bp_12_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo_24 = r_lo_hi_24 & io_bp_12_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi_24 = r_hi_lo_24 & io_bp_12_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_249 = {r_hi_hi_24,r_hi_lo_24,r_lo_hi_24,io_bp_12_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_216 = {{35'd0}, _r_T_249}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_250 = _r_T_5 | _GEN_216; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_251 = ~io_bp_12_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_256 = _r_T_251 | _GEN_216; // @[Breakpoint.scala 63:33]
  wire  _r_T_257 = _r_T_250 == _r_T_256; // @[Breakpoint.scala 63:19]
  wire  _r_T_258 = io_bp_12_control_tmatch[1] ? _r_T_244 : _r_T_257; // @[Breakpoint.scala 69:8]
  wire  r_12 = en_12 & io_bp_12_control_r & _r_T_258; // @[Breakpoint.scala 107:32]
  wire  w_12 = en_12 & io_bp_12_control_w & _r_T_258; // @[Breakpoint.scala 108:32]
  wire  _x_T_244 = io_pc >= io_bp_12_address ^ io_bp_12_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_250 = _x_T_5 | _GEN_216; // @[Breakpoint.scala 63:9]
  wire  _x_T_257 = _x_T_250 == _r_T_256; // @[Breakpoint.scala 63:19]
  wire  _x_T_258 = io_bp_12_control_tmatch[1] ? _x_T_244 : _x_T_257; // @[Breakpoint.scala 69:8]
  wire  x_12 = en_12 & io_bp_12_control_x & _x_T_258; // @[Breakpoint.scala 109:32]
  wire  end_12 = ~io_bp_12_control_chain; // @[Breakpoint.scala 110:15]
  wire  _io_xcpt_ld_T_12 = io_bp_12_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T_12 = io_bp_12_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  wire  _GEN_132 = end_12 & r_12 & _T_105 ? io_bp_12_control_action == 3'h0 : _GEN_121; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40]
  wire  _GEN_133 = end_12 & r_12 & _T_105 ? io_bp_12_control_action == 3'h1 : _GEN_122; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73]
  wire  _GEN_135 = end_12 & w_12 & _T_106 ? _io_xcpt_ld_T_12 : _GEN_124; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40]
  wire  _GEN_136 = end_12 & w_12 & _T_106 ? _io_debug_ld_T_12 : _GEN_125; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73]
  wire  _GEN_139 = end_12 & x_12 & _T_107 ? _io_xcpt_ld_T_12 : _GEN_128; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40]
  wire  _GEN_140 = end_12 & x_12 & _T_107 ? _io_debug_ld_T_12 : _GEN_129; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73]
  wire  _T_114 = end_12 | r_12; // @[Breakpoint.scala 123:10]
  wire  _T_115 = end_12 | w_12; // @[Breakpoint.scala 123:20]
  wire  _T_116 = end_12 | x_12; // @[Breakpoint.scala 123:30]
  wire [3:0] _en_T_53 = {io_bp_13_control_m,1'h0,io_bp_13_control_s,io_bp_13_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_54 = _en_T_53 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en_13 = ~io_status_debug & _en_T_54[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_264 = io_ea >= io_bp_13_address ^ io_bp_13_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire  r_lo_hi_26 = io_bp_13_control_tmatch[0] & io_bp_13_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo_26 = r_lo_hi_26 & io_bp_13_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi_26 = r_hi_lo_26 & io_bp_13_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_269 = {r_hi_hi_26,r_hi_lo_26,r_lo_hi_26,io_bp_13_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_222 = {{35'd0}, _r_T_269}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_270 = _r_T_5 | _GEN_222; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_271 = ~io_bp_13_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_276 = _r_T_271 | _GEN_222; // @[Breakpoint.scala 63:33]
  wire  _r_T_277 = _r_T_270 == _r_T_276; // @[Breakpoint.scala 63:19]
  wire  _r_T_278 = io_bp_13_control_tmatch[1] ? _r_T_264 : _r_T_277; // @[Breakpoint.scala 69:8]
  wire  r_13 = en_13 & io_bp_13_control_r & _r_T_278; // @[Breakpoint.scala 107:32]
  wire  w_13 = en_13 & io_bp_13_control_w & _r_T_278; // @[Breakpoint.scala 108:32]
  wire  _x_T_264 = io_pc >= io_bp_13_address ^ io_bp_13_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_270 = _x_T_5 | _GEN_222; // @[Breakpoint.scala 63:9]
  wire  _x_T_277 = _x_T_270 == _r_T_276; // @[Breakpoint.scala 63:19]
  wire  _x_T_278 = io_bp_13_control_tmatch[1] ? _x_T_264 : _x_T_277; // @[Breakpoint.scala 69:8]
  wire  x_13 = en_13 & io_bp_13_control_x & _x_T_278; // @[Breakpoint.scala 109:32]
  wire  end_13 = ~io_bp_13_control_chain; // @[Breakpoint.scala 110:15]
  wire  _io_xcpt_ld_T_13 = io_bp_13_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T_13 = io_bp_13_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  wire  _GEN_143 = end_13 & r_13 & _T_114 ? io_bp_13_control_action == 3'h0 : _GEN_132; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40]
  wire  _GEN_144 = end_13 & r_13 & _T_114 ? io_bp_13_control_action == 3'h1 : _GEN_133; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73]
  wire  _GEN_146 = end_13 & w_13 & _T_115 ? _io_xcpt_ld_T_13 : _GEN_135; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40]
  wire  _GEN_147 = end_13 & w_13 & _T_115 ? _io_debug_ld_T_13 : _GEN_136; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73]
  wire  _GEN_150 = end_13 & x_13 & _T_116 ? _io_xcpt_ld_T_13 : _GEN_139; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40]
  wire  _GEN_151 = end_13 & x_13 & _T_116 ? _io_debug_ld_T_13 : _GEN_140; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73]
  wire  _T_123 = end_13 | r_13; // @[Breakpoint.scala 123:10]
  wire  _T_124 = end_13 | w_13; // @[Breakpoint.scala 123:20]
  wire  _T_125 = end_13 | x_13; // @[Breakpoint.scala 123:30]
  wire [3:0] _en_T_57 = {io_bp_14_control_m,1'h0,io_bp_14_control_s,io_bp_14_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_58 = _en_T_57 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en_14 = ~io_status_debug & _en_T_58[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_284 = io_ea >= io_bp_14_address ^ io_bp_14_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire  r_lo_hi_28 = io_bp_14_control_tmatch[0] & io_bp_14_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo_28 = r_lo_hi_28 & io_bp_14_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi_28 = r_hi_lo_28 & io_bp_14_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_289 = {r_hi_hi_28,r_hi_lo_28,r_lo_hi_28,io_bp_14_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_228 = {{35'd0}, _r_T_289}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_290 = _r_T_5 | _GEN_228; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_291 = ~io_bp_14_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_296 = _r_T_291 | _GEN_228; // @[Breakpoint.scala 63:33]
  wire  _r_T_297 = _r_T_290 == _r_T_296; // @[Breakpoint.scala 63:19]
  wire  _r_T_298 = io_bp_14_control_tmatch[1] ? _r_T_284 : _r_T_297; // @[Breakpoint.scala 69:8]
  wire  r_14 = en_14 & io_bp_14_control_r & _r_T_298; // @[Breakpoint.scala 107:32]
  wire  w_14 = en_14 & io_bp_14_control_w & _r_T_298; // @[Breakpoint.scala 108:32]
  wire  _x_T_284 = io_pc >= io_bp_14_address ^ io_bp_14_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_290 = _x_T_5 | _GEN_228; // @[Breakpoint.scala 63:9]
  wire  _x_T_297 = _x_T_290 == _r_T_296; // @[Breakpoint.scala 63:19]
  wire  _x_T_298 = io_bp_14_control_tmatch[1] ? _x_T_284 : _x_T_297; // @[Breakpoint.scala 69:8]
  wire  x_14 = en_14 & io_bp_14_control_x & _x_T_298; // @[Breakpoint.scala 109:32]
  wire  end_14 = ~io_bp_14_control_chain; // @[Breakpoint.scala 110:15]
  wire  _io_xcpt_ld_T_14 = io_bp_14_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T_14 = io_bp_14_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  wire  _GEN_154 = end_14 & r_14 & _T_123 ? io_bp_14_control_action == 3'h0 : _GEN_143; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40]
  wire  _GEN_155 = end_14 & r_14 & _T_123 ? io_bp_14_control_action == 3'h1 : _GEN_144; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73]
  wire  _GEN_157 = end_14 & w_14 & _T_124 ? _io_xcpt_ld_T_14 : _GEN_146; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40]
  wire  _GEN_158 = end_14 & w_14 & _T_124 ? _io_debug_ld_T_14 : _GEN_147; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73]
  wire  _GEN_161 = end_14 & x_14 & _T_125 ? _io_xcpt_ld_T_14 : _GEN_150; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40]
  wire  _GEN_162 = end_14 & x_14 & _T_125 ? _io_debug_ld_T_14 : _GEN_151; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73]
  wire  _T_132 = end_14 | r_14; // @[Breakpoint.scala 123:10]
  wire  _T_133 = end_14 | w_14; // @[Breakpoint.scala 123:20]
  wire  _T_134 = end_14 | x_14; // @[Breakpoint.scala 123:30]
  wire [3:0] _en_T_61 = {io_bp_15_control_m,1'h0,io_bp_15_control_s,io_bp_15_control_u}; // @[Cat.scala 30:58]
  wire [3:0] _en_T_62 = _en_T_61 >> io_status_prv; // @[Breakpoint.scala 31:68]
  wire  en_15 = ~io_status_debug & _en_T_62[0]; // @[Breakpoint.scala 31:50]
  wire  _r_T_304 = io_ea >= io_bp_15_address ^ io_bp_15_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire  r_lo_hi_30 = io_bp_15_control_tmatch[0] & io_bp_15_address[0]; // @[Breakpoint.scala 60:73]
  wire  r_hi_lo_30 = r_lo_hi_30 & io_bp_15_address[1]; // @[Breakpoint.scala 60:73]
  wire  r_hi_hi_30 = r_hi_lo_30 & io_bp_15_address[2]; // @[Breakpoint.scala 60:73]
  wire [3:0] _r_T_309 = {r_hi_hi_30,r_hi_lo_30,r_lo_hi_30,io_bp_15_control_tmatch[0]}; // @[Cat.scala 30:58]
  wire [38:0] _GEN_234 = {{35'd0}, _r_T_309}; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_310 = _r_T_5 | _GEN_234; // @[Breakpoint.scala 63:9]
  wire [38:0] _r_T_311 = ~io_bp_15_address; // @[Breakpoint.scala 63:24]
  wire [38:0] _r_T_316 = _r_T_311 | _GEN_234; // @[Breakpoint.scala 63:33]
  wire  _r_T_317 = _r_T_310 == _r_T_316; // @[Breakpoint.scala 63:19]
  wire  _r_T_318 = io_bp_15_control_tmatch[1] ? _r_T_304 : _r_T_317; // @[Breakpoint.scala 69:8]
  wire  r_15 = en_15 & io_bp_15_control_r & _r_T_318; // @[Breakpoint.scala 107:32]
  wire  w_15 = en_15 & io_bp_15_control_w & _r_T_318; // @[Breakpoint.scala 108:32]
  wire  _x_T_304 = io_pc >= io_bp_15_address ^ io_bp_15_control_tmatch[0]; // @[Breakpoint.scala 66:20]
  wire [38:0] _x_T_310 = _x_T_5 | _GEN_234; // @[Breakpoint.scala 63:9]
  wire  _x_T_317 = _x_T_310 == _r_T_316; // @[Breakpoint.scala 63:19]
  wire  _x_T_318 = io_bp_15_control_tmatch[1] ? _x_T_304 : _x_T_317; // @[Breakpoint.scala 69:8]
  wire  x_15 = en_15 & io_bp_15_control_x & _x_T_318; // @[Breakpoint.scala 109:32]
  wire  _io_xcpt_ld_T_15 = io_bp_15_control_action == 3'h0; // @[Breakpoint.scala 119:51]
  wire  _io_debug_ld_T_15 = io_bp_15_control_action == 3'h1; // @[Breakpoint.scala 119:84]
  assign io_xcpt_if = x_15 & _T_134 ? _io_xcpt_ld_T_15 : _GEN_161; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:40]
  assign io_xcpt_ld = r_15 & _T_132 ? io_bp_15_control_action == 3'h0 : _GEN_154; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:40]
  assign io_xcpt_st = w_15 & _T_133 ? _io_xcpt_ld_T_15 : _GEN_157; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:40]
  assign io_debug_if = x_15 & _T_134 ? _io_debug_ld_T_15 : _GEN_162; // @[Breakpoint.scala 121:27 Breakpoint.scala 121:73]
  assign io_debug_ld = r_15 & _T_132 ? io_bp_15_control_action == 3'h1 : _GEN_155; // @[Breakpoint.scala 119:27 Breakpoint.scala 119:73]
  assign io_debug_st = w_15 & _T_133 ? _io_debug_ld_T_15 : _GEN_158; // @[Breakpoint.scala 120:27 Breakpoint.scala 120:73]
  assign io_bpwatch_0_rvalid_0 = end_ & r; // @[Breakpoint.scala 119:15]
  assign io_bpwatch_0_wvalid_0 = end_ & w; // @[Breakpoint.scala 120:15]
  assign io_bpwatch_0_ivalid_0 = end_ & x; // @[Breakpoint.scala 121:15]
  assign io_bpwatch_1_rvalid_0 = end_1 & r_1 & _T_6; // @[Breakpoint.scala 119:20]
  assign io_bpwatch_1_wvalid_0 = end_1 & w_1 & _T_7; // @[Breakpoint.scala 120:20]
  assign io_bpwatch_1_ivalid_0 = end_1 & x_1 & _T_8; // @[Breakpoint.scala 121:20]
  assign io_bpwatch_2_rvalid_0 = end_2 & r_2 & _T_15; // @[Breakpoint.scala 119:20]
  assign io_bpwatch_2_wvalid_0 = end_2 & w_2 & _T_16; // @[Breakpoint.scala 120:20]
  assign io_bpwatch_2_ivalid_0 = end_2 & x_2 & _T_17; // @[Breakpoint.scala 121:20]
  assign io_bpwatch_3_rvalid_0 = end_3 & r_3 & _T_24; // @[Breakpoint.scala 119:20]
  assign io_bpwatch_3_wvalid_0 = end_3 & w_3 & _T_25; // @[Breakpoint.scala 120:20]
  assign io_bpwatch_3_ivalid_0 = end_3 & x_3 & _T_26; // @[Breakpoint.scala 121:20]
  assign io_bpwatch_4_rvalid_0 = end_4 & r_4 & _T_33; // @[Breakpoint.scala 119:20]
  assign io_bpwatch_4_wvalid_0 = end_4 & w_4 & _T_34; // @[Breakpoint.scala 120:20]
  assign io_bpwatch_4_ivalid_0 = end_4 & x_4 & _T_35; // @[Breakpoint.scala 121:20]
  assign io_bpwatch_5_rvalid_0 = end_5 & r_5 & _T_42; // @[Breakpoint.scala 119:20]
  assign io_bpwatch_5_wvalid_0 = end_5 & w_5 & _T_43; // @[Breakpoint.scala 120:20]
  assign io_bpwatch_5_ivalid_0 = end_5 & x_5 & _T_44; // @[Breakpoint.scala 121:20]
  assign io_bpwatch_6_rvalid_0 = end_6 & r_6 & _T_51; // @[Breakpoint.scala 119:20]
  assign io_bpwatch_6_wvalid_0 = end_6 & w_6 & _T_52; // @[Breakpoint.scala 120:20]
  assign io_bpwatch_6_ivalid_0 = end_6 & x_6 & _T_53; // @[Breakpoint.scala 121:20]
  assign io_bpwatch_7_rvalid_0 = end_7 & r_7 & _T_60; // @[Breakpoint.scala 119:20]
  assign io_bpwatch_7_wvalid_0 = end_7 & w_7 & _T_61; // @[Breakpoint.scala 120:20]
  assign io_bpwatch_7_ivalid_0 = end_7 & x_7 & _T_62; // @[Breakpoint.scala 121:20]
  assign io_bpwatch_8_rvalid_0 = end_8 & r_8 & _T_69; // @[Breakpoint.scala 119:20]
  assign io_bpwatch_8_wvalid_0 = end_8 & w_8 & _T_70; // @[Breakpoint.scala 120:20]
  assign io_bpwatch_8_ivalid_0 = end_8 & x_8 & _T_71; // @[Breakpoint.scala 121:20]
  assign io_bpwatch_9_rvalid_0 = end_9 & r_9 & _T_78; // @[Breakpoint.scala 119:20]
  assign io_bpwatch_9_wvalid_0 = end_9 & w_9 & _T_79; // @[Breakpoint.scala 120:20]
  assign io_bpwatch_9_ivalid_0 = end_9 & x_9 & _T_80; // @[Breakpoint.scala 121:20]
  assign io_bpwatch_10_rvalid_0 = end_10 & r_10 & _T_87; // @[Breakpoint.scala 119:20]
  assign io_bpwatch_10_wvalid_0 = end_10 & w_10 & _T_88; // @[Breakpoint.scala 120:20]
  assign io_bpwatch_10_ivalid_0 = end_10 & x_10 & _T_89; // @[Breakpoint.scala 121:20]
  assign io_bpwatch_11_rvalid_0 = end_11 & r_11 & _T_96; // @[Breakpoint.scala 119:20]
  assign io_bpwatch_11_wvalid_0 = end_11 & w_11 & _T_97; // @[Breakpoint.scala 120:20]
  assign io_bpwatch_11_ivalid_0 = end_11 & x_11 & _T_98; // @[Breakpoint.scala 121:20]
  assign io_bpwatch_12_rvalid_0 = end_12 & r_12 & _T_105; // @[Breakpoint.scala 119:20]
  assign io_bpwatch_12_wvalid_0 = end_12 & w_12 & _T_106; // @[Breakpoint.scala 120:20]
  assign io_bpwatch_12_ivalid_0 = end_12 & x_12 & _T_107; // @[Breakpoint.scala 121:20]
  assign io_bpwatch_13_rvalid_0 = end_13 & r_13 & _T_114; // @[Breakpoint.scala 119:20]
  assign io_bpwatch_13_wvalid_0 = end_13 & w_13 & _T_115; // @[Breakpoint.scala 120:20]
  assign io_bpwatch_13_ivalid_0 = end_13 & x_13 & _T_116; // @[Breakpoint.scala 121:20]
  assign io_bpwatch_14_rvalid_0 = end_14 & r_14 & _T_123; // @[Breakpoint.scala 119:20]
  assign io_bpwatch_14_wvalid_0 = end_14 & w_14 & _T_124; // @[Breakpoint.scala 120:20]
  assign io_bpwatch_14_ivalid_0 = end_14 & x_14 & _T_125; // @[Breakpoint.scala 121:20]
  assign io_bpwatch_15_rvalid_0 = r_15 & _T_132; // @[Breakpoint.scala 119:20]
  assign io_bpwatch_15_wvalid_0 = w_15 & _T_133; // @[Breakpoint.scala 120:20]
  assign io_bpwatch_15_ivalid_0 = x_15 & _T_134; // @[Breakpoint.scala 121:20]
endmodule
