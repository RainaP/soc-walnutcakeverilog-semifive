//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88

module SCIEUnpipelined #(parameter XLEN = 32) (
    input  [31:0] insn,
    input  [XLEN-1:0] rs1,
    input  [XLEN-1:0] rs2,
    output [XLEN-1:0] rd);

  /* This example SCIE implementation provides the following instructions:

     Major opcode custom-0:
     Funct3 = 0: MIN (rd = rs1 < rs2 ? rs1 : rs2)
     Funct3 = 1: MAX (rd = rs1 > rs2 ? rs1 : rs2)

     Major opcode custom-1:
     Funct3 = 0: MINI (rd = rs1 < imm[11:0] ? rs1 : imm[11:0])
     Funct3 = 1: MAXI (rd = rs1 > imm[11:0] ? rs1 : imm[11:0])
  */

  /* Decode the instruction. */
  wire use_immediate = insn[5];
  wire pick_smaller = !insn[12];

  /* Mux the operands. */
  wire [XLEN-1:0] immediate = {{(XLEN-12){insn[31]}},  insn[31:20]};
  wire [XLEN-1:0] rhs = use_immediate ? immediate : rs2;
  wire [XLEN-1:0] lhs = rs1;

  /* Perform the computation. */
  wire lhs_smaller = $signed(lhs) < $signed(rhs);
  wire [XLEN-1:0] result = lhs_smaller == pick_smaller ? lhs : rhs;

  /* Drive the output. */
  assign rd = result;

endmodule
     