//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLWidthWidget_4(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_a_ready,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [3:0]  auto_in_a_bits_size,
  input  [36:0] auto_in_a_bits_address,
  input  [7:0]  auto_in_a_bits_data,
  input         auto_in_d_ready,
  output        auto_in_d_valid,
  output        auto_in_d_bits_denied,
  output [7:0]  auto_in_d_bits_data,
  output        auto_in_d_bits_corrupt,
  input         auto_out_a_ready,
  output        auto_out_a_valid,
  output [2:0]  auto_out_a_bits_opcode,
  output [3:0]  auto_out_a_bits_size,
  output [36:0] auto_out_a_bits_address,
  output [7:0]  auto_out_a_bits_mask,
  output [63:0] auto_out_a_bits_data,
  output        auto_out_d_ready,
  input         auto_out_d_valid,
  input  [2:0]  auto_out_d_bits_opcode,
  input  [1:0]  auto_out_d_bits_param,
  input  [3:0]  auto_out_d_bits_size,
  input  [4:0]  auto_out_d_bits_sink,
  input         auto_out_d_bits_denied,
  input  [63:0] auto_out_d_bits_data,
  input         auto_out_d_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
`endif // RANDOMIZE_REG_INIT
  wire  repeated_repeater_rf_reset; // @[Repeater.scala 35:26]
  wire  repeated_repeater_clock; // @[Repeater.scala 35:26]
  wire  repeated_repeater_reset; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_repeat; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_enq_ready; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_enq_valid; // @[Repeater.scala 35:26]
  wire [2:0] repeated_repeater_io_enq_bits_opcode; // @[Repeater.scala 35:26]
  wire [1:0] repeated_repeater_io_enq_bits_param; // @[Repeater.scala 35:26]
  wire [3:0] repeated_repeater_io_enq_bits_size; // @[Repeater.scala 35:26]
  wire [4:0] repeated_repeater_io_enq_bits_sink; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_enq_bits_denied; // @[Repeater.scala 35:26]
  wire [63:0] repeated_repeater_io_enq_bits_data; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_enq_bits_corrupt; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_deq_ready; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_deq_valid; // @[Repeater.scala 35:26]
  wire [2:0] repeated_repeater_io_deq_bits_opcode; // @[Repeater.scala 35:26]
  wire [1:0] repeated_repeater_io_deq_bits_param; // @[Repeater.scala 35:26]
  wire [3:0] repeated_repeater_io_deq_bits_size; // @[Repeater.scala 35:26]
  wire [4:0] repeated_repeater_io_deq_bits_sink; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_deq_bits_denied; // @[Repeater.scala 35:26]
  wire [63:0] repeated_repeater_io_deq_bits_data; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_deq_bits_corrupt; // @[Repeater.scala 35:26]
  wire  hasData = ~auto_in_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [17:0] _limit_T_1 = 18'h7 << auto_in_a_bits_size; // @[package.scala 234:77]
  wire [2:0] limit = ~_limit_T_1[2:0]; // @[package.scala 234:46]
  reg [2:0] count; // @[WidthWidget.scala 34:27]
  wire  last = count == limit | ~hasData; // @[WidthWidget.scala 36:36]
  wire [2:0] _enable_T_1 = count & limit; // @[WidthWidget.scala 37:63]
  wire  enable_0 = ~(|_enable_T_1); // @[WidthWidget.scala 37:47]
  wire [2:0] _enable_T_3 = count ^ 3'h1; // @[WidthWidget.scala 37:56]
  wire [2:0] _enable_T_4 = _enable_T_3 & limit; // @[WidthWidget.scala 37:63]
  wire  enable_1 = ~(|_enable_T_4); // @[WidthWidget.scala 37:47]
  wire [2:0] _enable_T_6 = count ^ 3'h2; // @[WidthWidget.scala 37:56]
  wire [2:0] _enable_T_7 = _enable_T_6 & limit; // @[WidthWidget.scala 37:63]
  wire  enable_2 = ~(|_enable_T_7); // @[WidthWidget.scala 37:47]
  wire [2:0] _enable_T_9 = count ^ 3'h3; // @[WidthWidget.scala 37:56]
  wire [2:0] _enable_T_10 = _enable_T_9 & limit; // @[WidthWidget.scala 37:63]
  wire  enable_3 = ~(|_enable_T_10); // @[WidthWidget.scala 37:47]
  wire [2:0] _enable_T_12 = count ^ 3'h4; // @[WidthWidget.scala 37:56]
  wire [2:0] _enable_T_13 = _enable_T_12 & limit; // @[WidthWidget.scala 37:63]
  wire  enable_4 = ~(|_enable_T_13); // @[WidthWidget.scala 37:47]
  wire [2:0] _enable_T_15 = count ^ 3'h5; // @[WidthWidget.scala 37:56]
  wire [2:0] _enable_T_16 = _enable_T_15 & limit; // @[WidthWidget.scala 37:63]
  wire  enable_5 = ~(|_enable_T_16); // @[WidthWidget.scala 37:47]
  wire [2:0] _enable_T_18 = count ^ 3'h6; // @[WidthWidget.scala 37:56]
  wire [2:0] _enable_T_19 = _enable_T_18 & limit; // @[WidthWidget.scala 37:63]
  wire  enable_6 = ~(|_enable_T_19); // @[WidthWidget.scala 37:47]
  wire  _bundleIn_0_a_ready_T = ~last; // @[WidthWidget.scala 70:32]
  wire  bundleIn_0_a_ready = auto_out_a_ready | _bundleIn_0_a_ready_T; // @[WidthWidget.scala 70:29]
  wire  _T = bundleIn_0_a_ready & auto_in_a_valid; // @[Decoupled.scala 40:37]
  wire [2:0] _count_T_1 = count + 3'h1; // @[WidthWidget.scala 44:24]
  reg  bundleOut_0_a_bits_data_rdata_written_once; // @[WidthWidget.scala 56:41]
  wire  bundleOut_0_a_bits_data_masked_enable_0 = enable_0 | ~bundleOut_0_a_bits_data_rdata_written_once; // @[WidthWidget.scala 57:42]
  wire  bundleOut_0_a_bits_data_masked_enable_1 = enable_1 | ~bundleOut_0_a_bits_data_rdata_written_once; // @[WidthWidget.scala 57:42]
  wire  bundleOut_0_a_bits_data_masked_enable_2 = enable_2 | ~bundleOut_0_a_bits_data_rdata_written_once; // @[WidthWidget.scala 57:42]
  wire  bundleOut_0_a_bits_data_masked_enable_3 = enable_3 | ~bundleOut_0_a_bits_data_rdata_written_once; // @[WidthWidget.scala 57:42]
  wire  bundleOut_0_a_bits_data_masked_enable_4 = enable_4 | ~bundleOut_0_a_bits_data_rdata_written_once; // @[WidthWidget.scala 57:42]
  wire  bundleOut_0_a_bits_data_masked_enable_5 = enable_5 | ~bundleOut_0_a_bits_data_rdata_written_once; // @[WidthWidget.scala 57:42]
  wire  bundleOut_0_a_bits_data_masked_enable_6 = enable_6 | ~bundleOut_0_a_bits_data_rdata_written_once; // @[WidthWidget.scala 57:42]
  reg [7:0] bundleOut_0_a_bits_data_rdata_0; // @[WidthWidget.scala 60:24]
  reg [7:0] bundleOut_0_a_bits_data_rdata_1; // @[WidthWidget.scala 60:24]
  reg [7:0] bundleOut_0_a_bits_data_rdata_2; // @[WidthWidget.scala 60:24]
  reg [7:0] bundleOut_0_a_bits_data_rdata_3; // @[WidthWidget.scala 60:24]
  reg [7:0] bundleOut_0_a_bits_data_rdata_4; // @[WidthWidget.scala 60:24]
  reg [7:0] bundleOut_0_a_bits_data_rdata_5; // @[WidthWidget.scala 60:24]
  reg [7:0] bundleOut_0_a_bits_data_rdata_6; // @[WidthWidget.scala 60:24]
  wire [7:0] bundleOut_0_a_bits_data_lo_lo_lo = bundleOut_0_a_bits_data_masked_enable_0 ? auto_in_a_bits_data :
    bundleOut_0_a_bits_data_rdata_0; // @[WidthWidget.scala 62:88]
  wire [7:0] bundleOut_0_a_bits_data_lo_lo_hi = bundleOut_0_a_bits_data_masked_enable_1 ? auto_in_a_bits_data :
    bundleOut_0_a_bits_data_rdata_1; // @[WidthWidget.scala 62:88]
  wire [7:0] bundleOut_0_a_bits_data_lo_hi_lo = bundleOut_0_a_bits_data_masked_enable_2 ? auto_in_a_bits_data :
    bundleOut_0_a_bits_data_rdata_2; // @[WidthWidget.scala 62:88]
  wire [7:0] bundleOut_0_a_bits_data_lo_hi_hi = bundleOut_0_a_bits_data_masked_enable_3 ? auto_in_a_bits_data :
    bundleOut_0_a_bits_data_rdata_3; // @[WidthWidget.scala 62:88]
  wire [7:0] bundleOut_0_a_bits_data_hi_lo_lo = bundleOut_0_a_bits_data_masked_enable_4 ? auto_in_a_bits_data :
    bundleOut_0_a_bits_data_rdata_4; // @[WidthWidget.scala 62:88]
  wire [7:0] bundleOut_0_a_bits_data_hi_lo_hi = bundleOut_0_a_bits_data_masked_enable_5 ? auto_in_a_bits_data :
    bundleOut_0_a_bits_data_rdata_5; // @[WidthWidget.scala 62:88]
  wire [7:0] bundleOut_0_a_bits_data_hi_hi_lo = bundleOut_0_a_bits_data_masked_enable_6 ? auto_in_a_bits_data :
    bundleOut_0_a_bits_data_rdata_6; // @[WidthWidget.scala 62:88]
  wire [31:0] bundleOut_0_a_bits_data_lo = {bundleOut_0_a_bits_data_lo_hi_hi,bundleOut_0_a_bits_data_lo_hi_lo,
    bundleOut_0_a_bits_data_lo_lo_hi,bundleOut_0_a_bits_data_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [31:0] bundleOut_0_a_bits_data_hi = {auto_in_a_bits_data,bundleOut_0_a_bits_data_hi_hi_lo,
    bundleOut_0_a_bits_data_hi_lo_hi,bundleOut_0_a_bits_data_hi_lo_lo}; // @[Cat.scala 30:58]
  wire [1:0] bundleOut_0_a_bits_mask_sizeOH_shiftAmount = auto_in_a_bits_size[1:0]; // @[OneHot.scala 64:49]
  wire [3:0] _bundleOut_0_a_bits_mask_sizeOH_T_1 = 4'h1 << bundleOut_0_a_bits_mask_sizeOH_shiftAmount; // @[OneHot.scala 65:12]
  wire [2:0] bundleOut_0_a_bits_mask_sizeOH = _bundleOut_0_a_bits_mask_sizeOH_T_1[2:0] | 3'h1; // @[Misc.scala 207:81]
  wire  _bundleOut_0_a_bits_mask_T = auto_in_a_bits_size >= 4'h3; // @[Misc.scala 211:21]
  wire  bundleOut_0_a_bits_mask_size = bundleOut_0_a_bits_mask_sizeOH[2]; // @[Misc.scala 214:26]
  wire  bundleOut_0_a_bits_mask_bit = auto_in_a_bits_address[2]; // @[Misc.scala 215:26]
  wire  bundleOut_0_a_bits_mask_nbit = ~bundleOut_0_a_bits_mask_bit; // @[Misc.scala 216:20]
  wire  bundleOut_0_a_bits_mask_acc = _bundleOut_0_a_bits_mask_T | bundleOut_0_a_bits_mask_size &
    bundleOut_0_a_bits_mask_nbit; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_acc_1 = _bundleOut_0_a_bits_mask_T | bundleOut_0_a_bits_mask_size &
    bundleOut_0_a_bits_mask_bit; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_size_1 = bundleOut_0_a_bits_mask_sizeOH[1]; // @[Misc.scala 214:26]
  wire  bundleOut_0_a_bits_mask_bit_1 = auto_in_a_bits_address[1]; // @[Misc.scala 215:26]
  wire  bundleOut_0_a_bits_mask_nbit_1 = ~bundleOut_0_a_bits_mask_bit_1; // @[Misc.scala 216:20]
  wire  bundleOut_0_a_bits_mask_eq_2 = bundleOut_0_a_bits_mask_nbit & bundleOut_0_a_bits_mask_nbit_1; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_2 = bundleOut_0_a_bits_mask_acc | bundleOut_0_a_bits_mask_size_1 &
    bundleOut_0_a_bits_mask_eq_2; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_3 = bundleOut_0_a_bits_mask_nbit & bundleOut_0_a_bits_mask_bit_1; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_3 = bundleOut_0_a_bits_mask_acc | bundleOut_0_a_bits_mask_size_1 &
    bundleOut_0_a_bits_mask_eq_3; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_4 = bundleOut_0_a_bits_mask_bit & bundleOut_0_a_bits_mask_nbit_1; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_4 = bundleOut_0_a_bits_mask_acc_1 | bundleOut_0_a_bits_mask_size_1 &
    bundleOut_0_a_bits_mask_eq_4; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_5 = bundleOut_0_a_bits_mask_bit & bundleOut_0_a_bits_mask_bit_1; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_5 = bundleOut_0_a_bits_mask_acc_1 | bundleOut_0_a_bits_mask_size_1 &
    bundleOut_0_a_bits_mask_eq_5; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_size_2 = bundleOut_0_a_bits_mask_sizeOH[0]; // @[Misc.scala 214:26]
  wire  bundleOut_0_a_bits_mask_bit_2 = auto_in_a_bits_address[0]; // @[Misc.scala 215:26]
  wire  bundleOut_0_a_bits_mask_nbit_2 = ~bundleOut_0_a_bits_mask_bit_2; // @[Misc.scala 216:20]
  wire  bundleOut_0_a_bits_mask_eq_6 = bundleOut_0_a_bits_mask_eq_2 & bundleOut_0_a_bits_mask_nbit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_lo_lo = bundleOut_0_a_bits_mask_acc_2 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_6; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_7 = bundleOut_0_a_bits_mask_eq_2 & bundleOut_0_a_bits_mask_bit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_lo_hi = bundleOut_0_a_bits_mask_acc_2 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_7; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_8 = bundleOut_0_a_bits_mask_eq_3 & bundleOut_0_a_bits_mask_nbit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_hi_lo = bundleOut_0_a_bits_mask_acc_3 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_8; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_9 = bundleOut_0_a_bits_mask_eq_3 & bundleOut_0_a_bits_mask_bit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_hi_hi = bundleOut_0_a_bits_mask_acc_3 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_9; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_10 = bundleOut_0_a_bits_mask_eq_4 & bundleOut_0_a_bits_mask_nbit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_lo_lo = bundleOut_0_a_bits_mask_acc_4 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_10; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_11 = bundleOut_0_a_bits_mask_eq_4 & bundleOut_0_a_bits_mask_bit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_lo_hi = bundleOut_0_a_bits_mask_acc_4 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_11; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_12 = bundleOut_0_a_bits_mask_eq_5 & bundleOut_0_a_bits_mask_nbit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_hi_lo = bundleOut_0_a_bits_mask_acc_5 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_12; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_13 = bundleOut_0_a_bits_mask_eq_5 & bundleOut_0_a_bits_mask_bit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_hi_hi = bundleOut_0_a_bits_mask_acc_5 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_13; // @[Misc.scala 220:29]
  wire [3:0] bundleOut_0_a_bits_mask_lo = {bundleOut_0_a_bits_mask_lo_hi_hi,bundleOut_0_a_bits_mask_lo_hi_lo,
    bundleOut_0_a_bits_mask_lo_lo_hi,bundleOut_0_a_bits_mask_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [3:0] bundleOut_0_a_bits_mask_hi = {bundleOut_0_a_bits_mask_hi_hi_hi,bundleOut_0_a_bits_mask_hi_hi_lo,
    bundleOut_0_a_bits_mask_hi_lo_hi,bundleOut_0_a_bits_mask_hi_lo_lo}; // @[Cat.scala 30:58]
  wire [55:0] cated_bits_data_hi = repeated_repeater_io_deq_bits_data[63:8]; // @[WidthWidget.scala 158:37]
  wire [7:0] cated_bits_data_lo = auto_out_d_bits_data[7:0]; // @[WidthWidget.scala 159:31]
  wire [63:0] cated_bits_data = {cated_bits_data_hi,cated_bits_data_lo}; // @[Cat.scala 30:58]
  wire [2:0] cated_bits_opcode = repeated_repeater_io_deq_bits_opcode; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  wire  repeat_hasData = cated_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [3:0] cated_bits_size = repeated_repeater_io_deq_bits_size; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  wire [17:0] _repeat_limit_T_1 = 18'h7 << cated_bits_size; // @[package.scala 234:77]
  wire [2:0] repeat_limit = ~_repeat_limit_T_1[2:0]; // @[package.scala 234:46]
  reg [2:0] repeat_count; // @[WidthWidget.scala 99:26]
  wire  repeat_first = repeat_count == 3'h0; // @[WidthWidget.scala 100:25]
  wire  repeat_last = repeat_count == repeat_limit | ~repeat_hasData; // @[WidthWidget.scala 101:35]
  wire  cated_valid = repeated_repeater_io_deq_valid; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  wire  _repeat_T = auto_in_d_ready & cated_valid; // @[Decoupled.scala 40:37]
  wire [2:0] _repeat_count_T_1 = repeat_count + 3'h1; // @[WidthWidget.scala 104:24]
  reg [2:0] repeat_sel_sel_sources_0; // @[WidthWidget.scala 180:27]
  wire [2:0] repeat_sel_sel_a_sel = auto_in_a_bits_address[2:0]; // @[WidthWidget.scala 181:38]
  reg [2:0] repeat_sel_hold_r; // @[Reg.scala 15:16]
  wire [2:0] _GEN_23 = repeat_first ? repeat_sel_sel_sources_0 : repeat_sel_hold_r; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire [2:0] _repeat_sel_T = ~repeat_limit; // @[WidthWidget.scala 116:18]
  wire [2:0] repeat_sel = _GEN_23 & _repeat_sel_T; // @[WidthWidget.scala 116:16]
  wire [2:0] repeat_index = repeat_sel | repeat_count; // @[WidthWidget.scala 120:24]
  wire [7:0] repeat_bundleIn_0_d_bits_data_mux_0 = cated_bits_data[7:0]; // @[WidthWidget.scala 122:55]
  wire [7:0] repeat_bundleIn_0_d_bits_data_mux_1 = cated_bits_data[15:8]; // @[WidthWidget.scala 122:55]
  wire [7:0] repeat_bundleIn_0_d_bits_data_mux_2 = cated_bits_data[23:16]; // @[WidthWidget.scala 122:55]
  wire [7:0] repeat_bundleIn_0_d_bits_data_mux_3 = cated_bits_data[31:24]; // @[WidthWidget.scala 122:55]
  wire [7:0] repeat_bundleIn_0_d_bits_data_mux_4 = cated_bits_data[39:32]; // @[WidthWidget.scala 122:55]
  wire [7:0] repeat_bundleIn_0_d_bits_data_mux_5 = cated_bits_data[47:40]; // @[WidthWidget.scala 122:55]
  wire [7:0] repeat_bundleIn_0_d_bits_data_mux_6 = cated_bits_data[55:48]; // @[WidthWidget.scala 122:55]
  wire [7:0] repeat_bundleIn_0_d_bits_data_mux_7 = cated_bits_data[63:56]; // @[WidthWidget.scala 122:55]
  wire [7:0] _GEN_25 = 3'h1 == repeat_index ? repeat_bundleIn_0_d_bits_data_mux_1 : repeat_bundleIn_0_d_bits_data_mux_0; // @[WidthWidget.scala 131:30 WidthWidget.scala 131:30]
  wire [7:0] _GEN_26 = 3'h2 == repeat_index ? repeat_bundleIn_0_d_bits_data_mux_2 : _GEN_25; // @[WidthWidget.scala 131:30 WidthWidget.scala 131:30]
  wire [7:0] _GEN_27 = 3'h3 == repeat_index ? repeat_bundleIn_0_d_bits_data_mux_3 : _GEN_26; // @[WidthWidget.scala 131:30 WidthWidget.scala 131:30]
  wire [7:0] _GEN_28 = 3'h4 == repeat_index ? repeat_bundleIn_0_d_bits_data_mux_4 : _GEN_27; // @[WidthWidget.scala 131:30 WidthWidget.scala 131:30]
  wire [7:0] _GEN_29 = 3'h5 == repeat_index ? repeat_bundleIn_0_d_bits_data_mux_5 : _GEN_28; // @[WidthWidget.scala 131:30 WidthWidget.scala 131:30]
  wire [7:0] _GEN_30 = 3'h6 == repeat_index ? repeat_bundleIn_0_d_bits_data_mux_6 : _GEN_29; // @[WidthWidget.scala 131:30 WidthWidget.scala 131:30]
  RHEA__Repeater_3 repeated_repeater ( // @[Repeater.scala 35:26]
    .rf_reset(repeated_repeater_rf_reset),
    .clock(repeated_repeater_clock),
    .reset(repeated_repeater_reset),
    .io_repeat(repeated_repeater_io_repeat),
    .io_enq_ready(repeated_repeater_io_enq_ready),
    .io_enq_valid(repeated_repeater_io_enq_valid),
    .io_enq_bits_opcode(repeated_repeater_io_enq_bits_opcode),
    .io_enq_bits_param(repeated_repeater_io_enq_bits_param),
    .io_enq_bits_size(repeated_repeater_io_enq_bits_size),
    .io_enq_bits_sink(repeated_repeater_io_enq_bits_sink),
    .io_enq_bits_denied(repeated_repeater_io_enq_bits_denied),
    .io_enq_bits_data(repeated_repeater_io_enq_bits_data),
    .io_enq_bits_corrupt(repeated_repeater_io_enq_bits_corrupt),
    .io_deq_ready(repeated_repeater_io_deq_ready),
    .io_deq_valid(repeated_repeater_io_deq_valid),
    .io_deq_bits_opcode(repeated_repeater_io_deq_bits_opcode),
    .io_deq_bits_param(repeated_repeater_io_deq_bits_param),
    .io_deq_bits_size(repeated_repeater_io_deq_bits_size),
    .io_deq_bits_sink(repeated_repeater_io_deq_bits_sink),
    .io_deq_bits_denied(repeated_repeater_io_deq_bits_denied),
    .io_deq_bits_data(repeated_repeater_io_deq_bits_data),
    .io_deq_bits_corrupt(repeated_repeater_io_deq_bits_corrupt)
  );
  assign repeated_repeater_rf_reset = rf_reset;
  assign auto_in_a_ready = auto_out_a_ready | _bundleIn_0_a_ready_T; // @[WidthWidget.scala 70:29]
  assign auto_in_d_valid = repeated_repeater_io_deq_valid; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  assign auto_in_d_bits_denied = repeated_repeater_io_deq_bits_denied; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  assign auto_in_d_bits_data = 3'h7 == repeat_index ? repeat_bundleIn_0_d_bits_data_mux_7 : _GEN_30; // @[WidthWidget.scala 131:30 WidthWidget.scala 131:30]
  assign auto_in_d_bits_corrupt = repeated_repeater_io_deq_bits_corrupt; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  assign auto_out_a_valid = auto_in_a_valid & last; // @[WidthWidget.scala 71:29]
  assign auto_out_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_size = auto_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_mask = {bundleOut_0_a_bits_mask_hi,bundleOut_0_a_bits_mask_lo}; // @[Cat.scala 30:58]
  assign auto_out_a_bits_data = {bundleOut_0_a_bits_data_hi,bundleOut_0_a_bits_data_lo}; // @[Cat.scala 30:58]
  assign auto_out_d_ready = repeated_repeater_io_enq_ready; // @[Nodes.scala 1529:13 Repeater.scala 37:21]
  assign repeated_repeater_clock = clock;
  assign repeated_repeater_reset = reset;
  assign repeated_repeater_io_repeat = ~repeat_last; // @[WidthWidget.scala 142:7]
  assign repeated_repeater_io_enq_valid = auto_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_param = auto_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_size = auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_sink = auto_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_denied = auto_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_data = auto_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_corrupt = auto_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_deq_ready = auto_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count <= 3'h0;
    end else if (_T) begin
      if (last) begin
        count <= 3'h0;
      end else begin
        count <= _count_T_1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      bundleOut_0_a_bits_data_rdata_written_once <= 1'h0;
    end else begin
      bundleOut_0_a_bits_data_rdata_written_once <= _T & _bundleIn_0_a_ready_T |
        bundleOut_0_a_bits_data_rdata_written_once;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleOut_0_a_bits_data_rdata_0 <= 8'h0;
    end else if (_T & _bundleIn_0_a_ready_T) begin
      if (bundleOut_0_a_bits_data_masked_enable_0) begin
        bundleOut_0_a_bits_data_rdata_0 <= auto_in_a_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleOut_0_a_bits_data_rdata_1 <= 8'h0;
    end else if (_T & _bundleIn_0_a_ready_T) begin
      if (bundleOut_0_a_bits_data_masked_enable_1) begin
        bundleOut_0_a_bits_data_rdata_1 <= auto_in_a_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleOut_0_a_bits_data_rdata_2 <= 8'h0;
    end else if (_T & _bundleIn_0_a_ready_T) begin
      if (bundleOut_0_a_bits_data_masked_enable_2) begin
        bundleOut_0_a_bits_data_rdata_2 <= auto_in_a_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleOut_0_a_bits_data_rdata_3 <= 8'h0;
    end else if (_T & _bundleIn_0_a_ready_T) begin
      if (bundleOut_0_a_bits_data_masked_enable_3) begin
        bundleOut_0_a_bits_data_rdata_3 <= auto_in_a_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleOut_0_a_bits_data_rdata_4 <= 8'h0;
    end else if (_T & _bundleIn_0_a_ready_T) begin
      if (bundleOut_0_a_bits_data_masked_enable_4) begin
        bundleOut_0_a_bits_data_rdata_4 <= auto_in_a_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleOut_0_a_bits_data_rdata_5 <= 8'h0;
    end else if (_T & _bundleIn_0_a_ready_T) begin
      if (bundleOut_0_a_bits_data_masked_enable_5) begin
        bundleOut_0_a_bits_data_rdata_5 <= auto_in_a_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleOut_0_a_bits_data_rdata_6 <= 8'h0;
    end else if (_T & _bundleIn_0_a_ready_T) begin
      if (bundleOut_0_a_bits_data_masked_enable_6) begin
        bundleOut_0_a_bits_data_rdata_6 <= auto_in_a_bits_data;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      repeat_count <= 3'h0;
    end else if (_repeat_T) begin
      if (repeat_last) begin
        repeat_count <= 3'h0;
      end else begin
        repeat_count <= _repeat_count_T_1;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_0 <= 3'h0;
    end else if (_T) begin
      repeat_sel_sel_sources_0 <= repeat_sel_sel_a_sel;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_hold_r <= 3'h0;
    end else if (repeat_first) begin
      repeat_sel_hold_r <= repeat_sel_sel_sources_0;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  count = _RAND_0[2:0];
  _RAND_1 = {1{`RANDOM}};
  bundleOut_0_a_bits_data_rdata_written_once = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  bundleOut_0_a_bits_data_rdata_0 = _RAND_2[7:0];
  _RAND_3 = {1{`RANDOM}};
  bundleOut_0_a_bits_data_rdata_1 = _RAND_3[7:0];
  _RAND_4 = {1{`RANDOM}};
  bundleOut_0_a_bits_data_rdata_2 = _RAND_4[7:0];
  _RAND_5 = {1{`RANDOM}};
  bundleOut_0_a_bits_data_rdata_3 = _RAND_5[7:0];
  _RAND_6 = {1{`RANDOM}};
  bundleOut_0_a_bits_data_rdata_4 = _RAND_6[7:0];
  _RAND_7 = {1{`RANDOM}};
  bundleOut_0_a_bits_data_rdata_5 = _RAND_7[7:0];
  _RAND_8 = {1{`RANDOM}};
  bundleOut_0_a_bits_data_rdata_6 = _RAND_8[7:0];
  _RAND_9 = {1{`RANDOM}};
  repeat_count = _RAND_9[2:0];
  _RAND_10 = {1{`RANDOM}};
  repeat_sel_sel_sources_0 = _RAND_10[2:0];
  _RAND_11 = {1{`RANDOM}};
  repeat_sel_hold_r = _RAND_11[2:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    count = 3'h0;
  end
  if (reset) begin
    bundleOut_0_a_bits_data_rdata_written_once = 1'h0;
  end
  if (rf_reset) begin
    bundleOut_0_a_bits_data_rdata_0 = 8'h0;
  end
  if (rf_reset) begin
    bundleOut_0_a_bits_data_rdata_1 = 8'h0;
  end
  if (rf_reset) begin
    bundleOut_0_a_bits_data_rdata_2 = 8'h0;
  end
  if (rf_reset) begin
    bundleOut_0_a_bits_data_rdata_3 = 8'h0;
  end
  if (rf_reset) begin
    bundleOut_0_a_bits_data_rdata_4 = 8'h0;
  end
  if (rf_reset) begin
    bundleOut_0_a_bits_data_rdata_5 = 8'h0;
  end
  if (rf_reset) begin
    bundleOut_0_a_bits_data_rdata_6 = 8'h0;
  end
  if (reset) begin
    repeat_count = 3'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_0 = 3'h0;
  end
  if (rf_reset) begin
    repeat_sel_hold_r = 3'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
