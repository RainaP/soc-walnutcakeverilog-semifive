//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_CaptureChain_assert(
  input   clock,
  input   reset,
  input   io_chainIn_shift,
  input   io_chainIn_capture,
  input   io_chainIn_update
);
  wire  _T_130; // @[JtagShifter.scala 118:31]
  wire  _T_131; // @[JtagShifter.scala 118:10]
  wire  _T_132; // @[JtagShifter.scala 119:31]
  wire  _T_133; // @[JtagShifter.scala 119:10]
  wire  _T_134; // @[JtagShifter.scala 119:7]
  wire  _T_135; // @[JtagShifter.scala 120:30]
  wire  _T_136; // @[JtagShifter.scala 120:10]
  wire  _T_137; // @[JtagShifter.scala 120:7]
  wire  _T_139; // @[JtagShifter.scala 118:9]
  wire  _T_140; // @[JtagShifter.scala 118:9]
  assign _T_130 = io_chainIn_capture & io_chainIn_update; // @[JtagShifter.scala 118:31]
  assign _T_131 = ~_T_130; // @[JtagShifter.scala 118:10]
  assign _T_132 = io_chainIn_capture & io_chainIn_shift; // @[JtagShifter.scala 119:31]
  assign _T_133 = ~_T_132; // @[JtagShifter.scala 119:10]
  assign _T_134 = _T_131 & _T_133; // @[JtagShifter.scala 119:7]
  assign _T_135 = io_chainIn_update & io_chainIn_shift; // @[JtagShifter.scala 120:30]
  assign _T_136 = ~_T_135; // @[JtagShifter.scala 120:10]
  assign _T_137 = _T_134 & _T_136; // @[JtagShifter.scala 120:7]
  assign _T_139 = _T_137 | reset; // @[JtagShifter.scala 118:9]
  assign _T_140 = ~_T_139; // @[JtagShifter.scala 118:9]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_140) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at JtagShifter.scala:118 assert(!(io.chainIn.capture && io.chainIn.update)\n"); // @[JtagShifter.scala 118:9]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_140) begin
          $fatal; // @[JtagShifter.scala 118:9]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
