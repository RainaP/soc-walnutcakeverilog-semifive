// Temporary PADs shell to integrate TOP
module pads_temp (
    // PAD
    input               PAD__RSTN,
    output              PAD__RSTN_OUT,
    input               PAD__PKG0,
    input               PAD__PKG1,
    input               PAD__PKG2,
    input               PAD__JTAG_TCK,
    input               PAD__JTAG_TMS,
    input               PAD__JTAG_TDI,
    output              PAD__JTAG_TDO,
    input               PAD__JTAG_TRST,
    inout               PAD__GPIOA_0,
    inout               PAD__GPIOA_1,
    inout               PAD__GPIOA_2,
    inout               PAD__GPIOA_3,
    inout               PAD__GPIOA_4,
    inout               PAD__GPIOA_5,
    inout               PAD__GPIOA_6,
    inout               PAD__GPIOA_7,
    inout               PAD__GPIOA_8,
    inout               PAD__GPIOA_9,
    inout               PAD__GPIOA_10,
    inout               PAD__GPIOA_11,
    inout               PAD__GPIOA_12,
    inout               PAD__GPIOA_13,
    inout               PAD__GPIOA_14,
    inout               PAD__GPIOA_15,
    inout               PAD__GPIOB_0,
    inout               PAD__GPIOB_1,
    inout               PAD__GPIOB_2,
    inout               PAD__GPIOB_3,
    inout               PAD__GPIOB_4,
    inout               PAD__GPIOB_5,
    inout               PAD__GPIOB_6,
    inout               PAD__GPIOB_7,
    inout               PAD__GPIOB_8,
    inout               PAD__GPIOB_9,
    inout               PAD__GPIOB_10,
    inout               PAD__GPIOB_11,
    inout               PAD__GPIOB_12,
    inout               PAD__GPIOB_13,
    inout               PAD__GPIOB_14,
    inout               PAD__GPIOB_15,
    inout               PAD__GPIOC_0,
    inout               PAD__GPIOC_1,
    inout               PAD__GPIOC_2,
    inout               PAD__GPIOC_3,
    inout               PAD__GPIOC_4,
    inout               PAD__GPIOC_5,
    inout               PAD__GPIOC_6,
    inout               PAD__GPIOC_7,
    inout               PAD__GPIOC_8,
    inout               PAD__GPIOC_9,
    inout               PAD__GPIOC_10,
    inout               PAD__GPIOC_11,
    inout               PAD__GPIOC_12,
    inout               PAD__GPIOC_13,
    inout               PAD__GPIOC_14,
    inout               PAD__GPIOC_15,
    inout               PAD__GPIOD_0,
    inout               PAD__GPIOD_1,
    inout               PAD__GPIOD_2,
    inout               PAD__GPIOD_3,
    inout               PAD__GPIOD_4,
    inout               PAD__GPIOD_5,
    inout               PAD__GPIOD_6,
    inout               PAD__GPIOD_7,
    inout               PAD__GPIOD_8,
    inout               PAD__GPIOD_9,
    inout               PAD__GPIOD_10,
    inout               PAD__GPIOD_11,
    inout               PAD__GPIOD_12,
    inout               PAD__GPIOD_13,
    inout               PAD__GPIOD_14,
    inout               PAD__GPIOD_15,
    inout               PAD__SDMMC_0,
    inout               PAD__SDMMC_1,
    inout               PAD__SDMMC_2,
    inout               PAD__SDMMC_3,
    inout               PAD__SDMMC_4,
    inout               PAD__SDMMC_5,
    inout               PAD__SDMMC_6,
    inout               PAD__SDMMC_7,
    inout               PAD__SDMMC_8,
    inout               PAD__SDMMC_9,
    inout               PAD__SDMMC_10,
    inout               PAD__SDMMC_11,
	inout               PAD__OTP_0_VPP,
	inout               PAD__OTP_0_VBG,
	inout               PAD__OTP_0_VTDO,
	inout               PAD__OTP_0_VREFM,
	inout               PAD__OTP_1_VPP,
	inout               PAD__OTP_1_VBG,
	inout               PAD__OTP_1_VTDO,
	inout               PAD__OTP_1_VREFM,

	inout               AY__OTP_0_VPP,
	inout               AY__OTP_0_VBG,
	inout               AY__OTP_0_VTDO,
	inout               AY__OTP_0_VREFM,
	inout               AY__OTP_1_VPP,
	inout               AY__OTP_1_VBG,
	inout               AY__OTP_1_VTDO,
	inout               AY__OTP_1_VREFM,

    output  PAD_OUT__XTAL,
    input   PAD_IN__XTAL,
    input   E0__XTAL,
    input   TE__XTAL,
    input   SP__XTAL,
    input   SF0__XTAL,
    input   SF1__XTAL,
    output  CK__XTAL,
    output  CK_IOV__XTAL,
    input   SNS__XTAL,
    output  PO__XTAL,
    input   POE__XTAL,
    input   RTO__XTAL,

    output  PAD_OUT__OSC,
    input   PAD_IN__OSC,
    input   E0__OSC,
    input   TE__OSC,
    input   SP__OSC,
    input   SF0__OSC,
    input   SF1__OSC,
    output  CK__OSC,
    output  CK_IOV__OSC,
    input   SNS__OSC,
    output  PO__OSC,
    input   POE__OSC,
    input   RTO__OSC,

    // CLKSEL
    input   PAD__CLKSEL,
    output  Y__CLKSEL,

    // A
    input               A__RSTN,
    input               A__RSTN_OUT,
    input               A__PKG0,
    input               A__PKG1,
    input               A__PKG2,
    input               A__JTAG_TCK,
    input               A__JTAG_TMS,
    input               A__JTAG_TDI,
    input               A__JTAG_TDO,
    input               A__JTAG_TRST,
    input               A__GPIOA_0,
    input               A__GPIOA_1,
    input               A__GPIOA_2,
    input               A__GPIOA_3,
    input               A__GPIOA_4,
    input               A__GPIOA_5,
    input               A__GPIOA_6,
    input               A__GPIOA_7,
    input               A__GPIOA_8,
    input               A__GPIOA_9,
    input               A__GPIOA_10,
    input               A__GPIOA_11,
    input               A__GPIOA_12,
    input               A__GPIOA_13,
    input               A__GPIOA_14,
    input               A__GPIOA_15,
    input               A__GPIOB_0,
    input               A__GPIOB_1,
    input               A__GPIOB_2,
    input               A__GPIOB_3,
    input               A__GPIOB_4,
    input               A__GPIOB_5,
    input               A__GPIOB_6,
    input               A__GPIOB_7,
    input               A__GPIOB_8,
    input               A__GPIOB_9,
    input               A__GPIOB_10,
    input               A__GPIOB_11,
    input               A__GPIOB_12,
    input               A__GPIOB_13,
    input               A__GPIOB_14,
    input               A__GPIOB_15,
    input               A__GPIOC_0,
    input               A__GPIOC_1,
    input               A__GPIOC_2,
    input               A__GPIOC_3,
    input               A__GPIOC_4,
    input               A__GPIOC_5,
    input               A__GPIOC_6,
    input               A__GPIOC_7,
    input               A__GPIOC_8,
    input               A__GPIOC_9,
    input               A__GPIOC_10,
    input               A__GPIOC_11,
    input               A__GPIOC_12,
    input               A__GPIOC_13,
    input               A__GPIOC_14,
    input               A__GPIOC_15,
    input               A__GPIOD_0,
    input               A__GPIOD_1,
    input               A__GPIOD_2,
    input               A__GPIOD_3,
    input               A__GPIOD_4,
    input               A__GPIOD_5,
    input               A__GPIOD_6,
    input               A__GPIOD_7,
    input               A__GPIOD_8,
    input               A__GPIOD_9,
    input               A__GPIOD_10,
    input               A__GPIOD_11,
    input               A__GPIOD_12,
    input               A__GPIOD_13,
    input               A__GPIOD_14,
    input               A__GPIOD_15,
    input               A__SDMMC_0,
    input               A__SDMMC_1,
    input               A__SDMMC_2,
    input               A__SDMMC_3,
    input               A__SDMMC_4,
    input               A__SDMMC_5,
    input               A__SDMMC_6,
    input               A__SDMMC_7,
    input               A__SDMMC_8,
    input               A__SDMMC_9,
    input               A__SDMMC_10,
    input               A__SDMMC_11,
    // Y
    output              Y__RSTN,
    output              Y__RSTN_OUT,
    output              Y__PKG0,
    output              Y__PKG1,
    output              Y__PKG2,
    output              Y__JTAG_TCK,
    output              Y__JTAG_TMS,
    output              Y__JTAG_TDI,
    output              Y__JTAG_TDO,
    output              Y__JTAG_TRST,
    output              Y__GPIOA_0,
    output              Y__GPIOA_1,
    output              Y__GPIOA_2,
    output              Y__GPIOA_3,
    output              Y__GPIOA_4,
    output              Y__GPIOA_5,
    output              Y__GPIOA_6,
    output              Y__GPIOA_7,
    output              Y__GPIOA_8,
    output              Y__GPIOA_9,
    output              Y__GPIOA_10,
    output              Y__GPIOA_11,
    output              Y__GPIOA_12,
    output              Y__GPIOA_13,
    output              Y__GPIOA_14,
    output              Y__GPIOA_15,
    output              Y__GPIOB_0,
    output              Y__GPIOB_1,
    output              Y__GPIOB_2,
    output              Y__GPIOB_3,
    output              Y__GPIOB_4,
    output              Y__GPIOB_5,
    output              Y__GPIOB_6,
    output              Y__GPIOB_7,
    output              Y__GPIOB_8,
    output              Y__GPIOB_9,
    output              Y__GPIOB_10,
    output              Y__GPIOB_11,
    output              Y__GPIOB_12,
    output              Y__GPIOB_13,
    output              Y__GPIOB_14,
    output              Y__GPIOB_15,
    output              Y__GPIOC_0,
    output              Y__GPIOC_1,
    output              Y__GPIOC_2,
    output              Y__GPIOC_3,
    output              Y__GPIOC_4,
    output              Y__GPIOC_5,
    output              Y__GPIOC_6,
    output              Y__GPIOC_7,
    output              Y__GPIOC_8,
    output              Y__GPIOC_9,
    output              Y__GPIOC_10,
    output              Y__GPIOC_11,
    output              Y__GPIOC_12,
    output              Y__GPIOC_13,
    output              Y__GPIOC_14,
    output              Y__GPIOC_15,
    output              Y__GPIOD_0,
    output              Y__GPIOD_1,
    output              Y__GPIOD_2,
    output              Y__GPIOD_3,
    output              Y__GPIOD_4,
    output              Y__GPIOD_5,
    output              Y__GPIOD_6,
    output              Y__GPIOD_7,
    output              Y__GPIOD_8,
    output              Y__GPIOD_9,
    output              Y__GPIOD_10,
    output              Y__GPIOD_11,
    output              Y__GPIOD_12,
    output              Y__GPIOD_13,
    output              Y__GPIOD_14,
    output              Y__GPIOD_15,
    output              Y__SDMMC_0,
    output              Y__SDMMC_1,
    output              Y__SDMMC_2,
    output              Y__SDMMC_3,
    output              Y__SDMMC_4,
    output              Y__SDMMC_5,
    output              Y__SDMMC_6,
    output              Y__SDMMC_7,
    output              Y__SDMMC_8,
    output              Y__SDMMC_9,
    output              Y__SDMMC_10,
    output              Y__SDMMC_11,
    // OE
    input               OE__RSTN,
    input               OE__RSTN_OUT,
    input               OE__PKG0,
    input               OE__PKG1,
    input               OE__PKG2,
    input               OE__JTAG_TCK,
    input               OE__JTAG_TMS,
    input               OE__JTAG_TDI,
    input               OE__JTAG_TDO,
    input               OE__JTAG_TRST,
    input               OE__GPIOA_0,
    input               OE__GPIOA_1,
    input               OE__GPIOA_2,
    input               OE__GPIOA_3,
    input               OE__GPIOA_4,
    input               OE__GPIOA_5,
    input               OE__GPIOA_6,
    input               OE__GPIOA_7,
    input               OE__GPIOA_8,
    input               OE__GPIOA_9,
    input               OE__GPIOA_10,
    input               OE__GPIOA_11,
    input               OE__GPIOA_12,
    input               OE__GPIOA_13,
    input               OE__GPIOA_14,
    input               OE__GPIOA_15,
    input               OE__GPIOB_0,
    input               OE__GPIOB_1,
    input               OE__GPIOB_2,
    input               OE__GPIOB_3,
    input               OE__GPIOB_4,
    input               OE__GPIOB_5,
    input               OE__GPIOB_6,
    input               OE__GPIOB_7,
    input               OE__GPIOB_8,
    input               OE__GPIOB_9,
    input               OE__GPIOB_10,
    input               OE__GPIOB_11,
    input               OE__GPIOB_12,
    input               OE__GPIOB_13,
    input               OE__GPIOB_14,
    input               OE__GPIOB_15,
    input               OE__GPIOC_0,
    input               OE__GPIOC_1,
    input               OE__GPIOC_2,
    input               OE__GPIOC_3,
    input               OE__GPIOC_4,
    input               OE__GPIOC_5,
    input               OE__GPIOC_6,
    input               OE__GPIOC_7,
    input               OE__GPIOC_8,
    input               OE__GPIOC_9,
    input               OE__GPIOC_10,
    input               OE__GPIOC_11,
    input               OE__GPIOC_12,
    input               OE__GPIOC_13,
    input               OE__GPIOC_14,
    input               OE__GPIOC_15,
    input               OE__GPIOD_0,
    input               OE__GPIOD_1,
    input               OE__GPIOD_2,
    input               OE__GPIOD_3,
    input               OE__GPIOD_4,
    input               OE__GPIOD_5,
    input               OE__GPIOD_6,
    input               OE__GPIOD_7,
    input               OE__GPIOD_8,
    input               OE__GPIOD_9,
    input               OE__GPIOD_10,
    input               OE__GPIOD_11,
    input               OE__GPIOD_12,
    input               OE__GPIOD_13,
    input               OE__GPIOD_14,
    input               OE__GPIOD_15,
    input               OE__SDMMC_0,
    input               OE__SDMMC_1,
    input               OE__SDMMC_2,
    input               OE__SDMMC_3,
    input               OE__SDMMC_4,
    input               OE__SDMMC_5,
    input               OE__SDMMC_6,
    input               OE__SDMMC_7,
    input               OE__SDMMC_8,
    input               OE__SDMMC_9,
    input               OE__SDMMC_10,
    input               OE__SDMMC_11,
    // IE
    input               IE__RSTN,
    input               IE__RSTN_OUT,
    input               IE__PKG0,
    input               IE__PKG1,
    input               IE__PKG2,
    input               IE__JTAG_TCK,
    input               IE__JTAG_TMS,
    input               IE__JTAG_TDI,
    input               IE__JTAG_TDO,
    input               IE__JTAG_TRST,
    input               IE__GPIOA_0,
    input               IE__GPIOA_1,
    input               IE__GPIOA_2,
    input               IE__GPIOA_3,
    input               IE__GPIOA_4,
    input               IE__GPIOA_5,
    input               IE__GPIOA_6,
    input               IE__GPIOA_7,
    input               IE__GPIOA_8,
    input               IE__GPIOA_9,
    input               IE__GPIOA_10,
    input               IE__GPIOA_11,
    input               IE__GPIOA_12,
    input               IE__GPIOA_13,
    input               IE__GPIOA_14,
    input               IE__GPIOA_15,
    input               IE__GPIOB_0,
    input               IE__GPIOB_1,
    input               IE__GPIOB_2,
    input               IE__GPIOB_3,
    input               IE__GPIOB_4,
    input               IE__GPIOB_5,
    input               IE__GPIOB_6,
    input               IE__GPIOB_7,
    input               IE__GPIOB_8,
    input               IE__GPIOB_9,
    input               IE__GPIOB_10,
    input               IE__GPIOB_11,
    input               IE__GPIOB_12,
    input               IE__GPIOB_13,
    input               IE__GPIOB_14,
    input               IE__GPIOB_15,
    input               IE__GPIOC_0,
    input               IE__GPIOC_1,
    input               IE__GPIOC_2,
    input               IE__GPIOC_3,
    input               IE__GPIOC_4,
    input               IE__GPIOC_5,
    input               IE__GPIOC_6,
    input               IE__GPIOC_7,
    input               IE__GPIOC_8,
    input               IE__GPIOC_9,
    input               IE__GPIOC_10,
    input               IE__GPIOC_11,
    input               IE__GPIOC_12,
    input               IE__GPIOC_13,
    input               IE__GPIOC_14,
    input               IE__GPIOC_15,
    input               IE__GPIOD_0,
    input               IE__GPIOD_1,
    input               IE__GPIOD_2,
    input               IE__GPIOD_3,
    input               IE__GPIOD_4,
    input               IE__GPIOD_5,
    input               IE__GPIOD_6,
    input               IE__GPIOD_7,
    input               IE__GPIOD_8,
    input               IE__GPIOD_9,
    input               IE__GPIOD_10,
    input               IE__GPIOD_11,
    input               IE__GPIOD_12,
    input               IE__GPIOD_13,
    input               IE__GPIOD_14,
    input               IE__GPIOD_15,
    input               IE__SDMMC_0,
    input               IE__SDMMC_1,
    input               IE__SDMMC_2,
    input               IE__SDMMC_3,
    input               IE__SDMMC_4,
    input               IE__SDMMC_5,
    input               IE__SDMMC_6,
    input               IE__SDMMC_7,
    input               IE__SDMMC_8,
    input               IE__SDMMC_9,
    input               IE__SDMMC_10,
    input               IE__SDMMC_11,
    // DS0
    input               DS0__RSTN,
    input               DS0__RSTN_OUT,
    input               DS0__PKG0,
    input               DS0__PKG1,
    input               DS0__PKG2,
    input               DS0__JTAG_TCK,
    input               DS0__JTAG_TMS,
    input               DS0__JTAG_TDI,
    input               DS0__JTAG_TDO,
    input               DS0__JTAG_TRST,
    input               DS0__GPIOA_0,
    input               DS0__GPIOA_1,
    input               DS0__GPIOA_2,
    input               DS0__GPIOA_3,
    input               DS0__GPIOA_4,
    input               DS0__GPIOA_5,
    input               DS0__GPIOA_6,
    input               DS0__GPIOA_7,
    input               DS0__GPIOA_8,
    input               DS0__GPIOA_9,
    input               DS0__GPIOA_10,
    input               DS0__GPIOA_11,
    input               DS0__GPIOA_12,
    input               DS0__GPIOA_13,
    input               DS0__GPIOA_14,
    input               DS0__GPIOA_15,
    input               DS0__GPIOB_0,
    input               DS0__GPIOB_1,
    input               DS0__GPIOB_2,
    input               DS0__GPIOB_3,
    input               DS0__GPIOB_4,
    input               DS0__GPIOB_5,
    input               DS0__GPIOB_6,
    input               DS0__GPIOB_7,
    input               DS0__GPIOB_8,
    input               DS0__GPIOB_9,
    input               DS0__GPIOB_10,
    input               DS0__GPIOB_11,
    input               DS0__GPIOB_12,
    input               DS0__GPIOB_13,
    input               DS0__GPIOB_14,
    input               DS0__GPIOB_15,
    input               DS0__GPIOC_0,
    input               DS0__GPIOC_1,
    input               DS0__GPIOC_2,
    input               DS0__GPIOC_3,
    input               DS0__GPIOC_4,
    input               DS0__GPIOC_5,
    input               DS0__GPIOC_6,
    input               DS0__GPIOC_7,
    input               DS0__GPIOC_8,
    input               DS0__GPIOC_9,
    input               DS0__GPIOC_10,
    input               DS0__GPIOC_11,
    input               DS0__GPIOC_12,
    input               DS0__GPIOC_13,
    input               DS0__GPIOC_14,
    input               DS0__GPIOC_15,
    input               DS0__GPIOD_0,
    input               DS0__GPIOD_1,
    input               DS0__GPIOD_2,
    input               DS0__GPIOD_3,
    input               DS0__GPIOD_4,
    input               DS0__GPIOD_5,
    input               DS0__GPIOD_6,
    input               DS0__GPIOD_7,
    input               DS0__GPIOD_8,
    input               DS0__GPIOD_9,
    input               DS0__GPIOD_10,
    input               DS0__GPIOD_11,
    input               DS0__GPIOD_12,
    input               DS0__GPIOD_13,
    input               DS0__GPIOD_14,
    input               DS0__GPIOD_15,
    input               DS0__SDMMC_0,
    input               DS0__SDMMC_1,
    input               DS0__SDMMC_2,
    input               DS0__SDMMC_3,
    input               DS0__SDMMC_4,
    input               DS0__SDMMC_5,
    input               DS0__SDMMC_6,
    input               DS0__SDMMC_7,
    input               DS0__SDMMC_8,
    input               DS0__SDMMC_9,
    input               DS0__SDMMC_10,
    input               DS0__SDMMC_11,
    // DS1
    input               DS1__RSTN,
    input               DS1__RSTN_OUT,
    input               DS1__PKG0,
    input               DS1__PKG1,
    input               DS1__PKG2,
    input               DS1__JTAG_TCK,
    input               DS1__JTAG_TMS,
    input               DS1__JTAG_TDI,
    input               DS1__JTAG_TDO,
    input               DS1__JTAG_TRST,
    input               DS1__GPIOA_0,
    input               DS1__GPIOA_1,
    input               DS1__GPIOA_2,
    input               DS1__GPIOA_3,
    input               DS1__GPIOA_4,
    input               DS1__GPIOA_5,
    input               DS1__GPIOA_6,
    input               DS1__GPIOA_7,
    input               DS1__GPIOA_8,
    input               DS1__GPIOA_9,
    input               DS1__GPIOA_10,
    input               DS1__GPIOA_11,
    input               DS1__GPIOA_12,
    input               DS1__GPIOA_13,
    input               DS1__GPIOA_14,
    input               DS1__GPIOA_15,
    input               DS1__GPIOB_0,
    input               DS1__GPIOB_1,
    input               DS1__GPIOB_2,
    input               DS1__GPIOB_3,
    input               DS1__GPIOB_4,
    input               DS1__GPIOB_5,
    input               DS1__GPIOB_6,
    input               DS1__GPIOB_7,
    input               DS1__GPIOB_8,
    input               DS1__GPIOB_9,
    input               DS1__GPIOB_10,
    input               DS1__GPIOB_11,
    input               DS1__GPIOB_12,
    input               DS1__GPIOB_13,
    input               DS1__GPIOB_14,
    input               DS1__GPIOB_15,
    input               DS1__GPIOC_0,
    input               DS1__GPIOC_1,
    input               DS1__GPIOC_2,
    input               DS1__GPIOC_3,
    input               DS1__GPIOC_4,
    input               DS1__GPIOC_5,
    input               DS1__GPIOC_6,
    input               DS1__GPIOC_7,
    input               DS1__GPIOC_8,
    input               DS1__GPIOC_9,
    input               DS1__GPIOC_10,
    input               DS1__GPIOC_11,
    input               DS1__GPIOC_12,
    input               DS1__GPIOC_13,
    input               DS1__GPIOC_14,
    input               DS1__GPIOC_15,
    input               DS1__GPIOD_0,
    input               DS1__GPIOD_1,
    input               DS1__GPIOD_2,
    input               DS1__GPIOD_3,
    input               DS1__GPIOD_4,
    input               DS1__GPIOD_5,
    input               DS1__GPIOD_6,
    input               DS1__GPIOD_7,
    input               DS1__GPIOD_8,
    input               DS1__GPIOD_9,
    input               DS1__GPIOD_10,
    input               DS1__GPIOD_11,
    input               DS1__GPIOD_12,
    input               DS1__GPIOD_13,
    input               DS1__GPIOD_14,
    input               DS1__GPIOD_15,
    input               DS1__SDMMC_0,
    input               DS1__SDMMC_1,
    input               DS1__SDMMC_2,
    input               DS1__SDMMC_3,
    input               DS1__SDMMC_4,
    input               DS1__SDMMC_5,
    input               DS1__SDMMC_6,
    input               DS1__SDMMC_7,
    input               DS1__SDMMC_8,
    input               DS1__SDMMC_9,
    input               DS1__SDMMC_10,
    input               DS1__SDMMC_11,
    // PE
    input               PE__RSTN,
    input               PE__RSTN_OUT,
    input               PE__PKG0,
    input               PE__PKG1,
    input               PE__PKG2,
    input               PE__JTAG_TCK,
    input               PE__JTAG_TMS,
    input               PE__JTAG_TDI,
    input               PE__JTAG_TDO,
    input               PE__JTAG_TRST,
    input               PE__GPIOA_0,
    input               PE__GPIOA_1,
    input               PE__GPIOA_2,
    input               PE__GPIOA_3,
    input               PE__GPIOA_4,
    input               PE__GPIOA_5,
    input               PE__GPIOA_6,
    input               PE__GPIOA_7,
    input               PE__GPIOA_8,
    input               PE__GPIOA_9,
    input               PE__GPIOA_10,
    input               PE__GPIOA_11,
    input               PE__GPIOA_12,
    input               PE__GPIOA_13,
    input               PE__GPIOA_14,
    input               PE__GPIOA_15,
    input               PE__GPIOB_0,
    input               PE__GPIOB_1,
    input               PE__GPIOB_2,
    input               PE__GPIOB_3,
    input               PE__GPIOB_4,
    input               PE__GPIOB_5,
    input               PE__GPIOB_6,
    input               PE__GPIOB_7,
    input               PE__GPIOB_8,
    input               PE__GPIOB_9,
    input               PE__GPIOB_10,
    input               PE__GPIOB_11,
    input               PE__GPIOB_12,
    input               PE__GPIOB_13,
    input               PE__GPIOB_14,
    input               PE__GPIOB_15,
    input               PE__GPIOC_0,
    input               PE__GPIOC_1,
    input               PE__GPIOC_2,
    input               PE__GPIOC_3,
    input               PE__GPIOC_4,
    input               PE__GPIOC_5,
    input               PE__GPIOC_6,
    input               PE__GPIOC_7,
    input               PE__GPIOC_8,
    input               PE__GPIOC_9,
    input               PE__GPIOC_10,
    input               PE__GPIOC_11,
    input               PE__GPIOC_12,
    input               PE__GPIOC_13,
    input               PE__GPIOC_14,
    input               PE__GPIOC_15,
    input               PE__GPIOD_0,
    input               PE__GPIOD_1,
    input               PE__GPIOD_2,
    input               PE__GPIOD_3,
    input               PE__GPIOD_4,
    input               PE__GPIOD_5,
    input               PE__GPIOD_6,
    input               PE__GPIOD_7,
    input               PE__GPIOD_8,
    input               PE__GPIOD_9,
    input               PE__GPIOD_10,
    input               PE__GPIOD_11,
    input               PE__GPIOD_12,
    input               PE__GPIOD_13,
    input               PE__GPIOD_14,
    input               PE__GPIOD_15,
    input               PE__SDMMC_0,
    input               PE__SDMMC_1,
    input               PE__SDMMC_2,
    input               PE__SDMMC_3,
    input               PE__SDMMC_4,
    input               PE__SDMMC_5,
    input               PE__SDMMC_6,
    input               PE__SDMMC_7,
    input               PE__SDMMC_8,
    input               PE__SDMMC_9,
    input               PE__SDMMC_10,
    input               PE__SDMMC_11,
    // PS
    input               PS__RSTN,
    input               PS__RSTN_OUT,
    input               PS__PKG0,
    input               PS__PKG1,
    input               PS__PKG2,
    input               PS__JTAG_TCK,
    input               PS__JTAG_TMS,
    input               PS__JTAG_TDI,
    input               PS__JTAG_TDO,
    input               PS__JTAG_TRST,
    input               PS__GPIOA_0,
    input               PS__GPIOA_1,
    input               PS__GPIOA_2,
    input               PS__GPIOA_3,
    input               PS__GPIOA_4,
    input               PS__GPIOA_5,
    input               PS__GPIOA_6,
    input               PS__GPIOA_7,
    input               PS__GPIOA_8,
    input               PS__GPIOA_9,
    input               PS__GPIOA_10,
    input               PS__GPIOA_11,
    input               PS__GPIOA_12,
    input               PS__GPIOA_13,
    input               PS__GPIOA_14,
    input               PS__GPIOA_15,
    input               PS__GPIOB_0,
    input               PS__GPIOB_1,
    input               PS__GPIOB_2,
    input               PS__GPIOB_3,
    input               PS__GPIOB_4,
    input               PS__GPIOB_5,
    input               PS__GPIOB_6,
    input               PS__GPIOB_7,
    input               PS__GPIOB_8,
    input               PS__GPIOB_9,
    input               PS__GPIOB_10,
    input               PS__GPIOB_11,
    input               PS__GPIOB_12,
    input               PS__GPIOB_13,
    input               PS__GPIOB_14,
    input               PS__GPIOB_15,
    input               PS__GPIOC_0,
    input               PS__GPIOC_1,
    input               PS__GPIOC_2,
    input               PS__GPIOC_3,
    input               PS__GPIOC_4,
    input               PS__GPIOC_5,
    input               PS__GPIOC_6,
    input               PS__GPIOC_7,
    input               PS__GPIOC_8,
    input               PS__GPIOC_9,
    input               PS__GPIOC_10,
    input               PS__GPIOC_11,
    input               PS__GPIOC_12,
    input               PS__GPIOC_13,
    input               PS__GPIOC_14,
    input               PS__GPIOC_15,
    input               PS__GPIOD_0,
    input               PS__GPIOD_1,
    input               PS__GPIOD_2,
    input               PS__GPIOD_3,
    input               PS__GPIOD_4,
    input               PS__GPIOD_5,
    input               PS__GPIOD_6,
    input               PS__GPIOD_7,
    input               PS__GPIOD_8,
    input               PS__GPIOD_9,
    input               PS__GPIOD_10,
    input               PS__GPIOD_11,
    input               PS__GPIOD_12,
    input               PS__GPIOD_13,
    input               PS__GPIOD_14,
    input               PS__GPIOD_15,
    input               PS__SDMMC_0,
    input               PS__SDMMC_1,
    input               PS__SDMMC_2,
    input               PS__SDMMC_3,
    input               PS__SDMMC_4,
    input               PS__SDMMC_5,
    input               PS__SDMMC_6,
    input               PS__SDMMC_7,
    input               PS__SDMMC_8,
    input               PS__SDMMC_9,
    input               PS__SDMMC_10,
    input               PS__SDMMC_11
);

// RSTN
assign Y__RSTN = PAD__RSTN;

// RSTN__OUT
assign PAD__RSTN_OUT = A__RSTN_OUT;

// XTAL
assign CK__XTAL= PAD_IN__XTAL;

// OSC
assign CK__OSC= PAD_IN__OSC;

// PKG0
assign Y__PKG0 = PAD__PKG0;

// PKG1
assign Y__PKG1 = PAD__PKG1;

// PKG2
assign Y__PKG2 = PAD__PKG2;

// JTAG_TCK
assign Y__JTAG_TCK = PAD__JTAG_TCK;

// JTAG_TMS
assign Y__JTAG_TMS = PAD__JTAG_TMS;

// JTAG_TDI
assign Y__JTAG_TDI = PAD__JTAG_TDI;

// JTAG_TDO
assign PAD__JTAG_TDO = OE__JTAG_TDO ? A__JTAG_TDO : 1'bz;

// JTAG_TRST
assign Y__JTAG_TRST = PAD__JTAG_TRST;

// GPIO
assign PAD__GPIOA_0 = OE__GPIOA_0 ? A__GPIOA_0 : 1'bz;
assign Y__GPIOA_0 = IE__GPIOA_0 ? PAD__GPIOA_0 : 1'b0;
assign PAD__GPIOA_1 = OE__GPIOA_1 ? A__GPIOA_1 : 1'bz;
assign Y__GPIOA_1 = IE__GPIOA_1 ? PAD__GPIOA_1 : 1'b0;
assign PAD__GPIOA_2 = OE__GPIOA_2 ? A__GPIOA_2 : 1'bz;
assign Y__GPIOA_2 = IE__GPIOA_2 ? PAD__GPIOA_2 : 1'b0;
assign PAD__GPIOA_3 = OE__GPIOA_3 ? A__GPIOA_3 : 1'bz;
assign Y__GPIOA_3 = IE__GPIOA_3 ? PAD__GPIOA_3 : 1'b0;
assign PAD__GPIOA_4 = OE__GPIOA_4 ? A__GPIOA_4 : 1'bz;
assign Y__GPIOA_4 = IE__GPIOA_4 ? PAD__GPIOA_4 : 1'b0;
assign PAD__GPIOA_5 = OE__GPIOA_5 ? A__GPIOA_5 : 1'bz;
assign Y__GPIOA_5 = IE__GPIOA_5 ? PAD__GPIOA_5 : 1'b0;
assign PAD__GPIOA_6 = OE__GPIOA_6 ? A__GPIOA_6 : 1'bz;
assign Y__GPIOA_6 = IE__GPIOA_6 ? PAD__GPIOA_6 : 1'b0;
assign PAD__GPIOA_7 = OE__GPIOA_7 ? A__GPIOA_7 : 1'bz;
assign Y__GPIOA_7 = IE__GPIOA_7 ? PAD__GPIOA_7 : 1'b0;
assign PAD__GPIOA_8 = OE__GPIOA_8 ? A__GPIOA_8 : 1'bz;
assign Y__GPIOA_8 = IE__GPIOA_8 ? PAD__GPIOA_8 : 1'b0;
assign PAD__GPIOA_9 = OE__GPIOA_9 ? A__GPIOA_9 : 1'bz;
assign Y__GPIOA_9 = IE__GPIOA_9 ? PAD__GPIOA_9 : 1'b0;
assign PAD__GPIOA_10 = OE__GPIOA_10 ? A__GPIOA_10 : 1'bz;
assign Y__GPIOA_10 = IE__GPIOA_10 ? PAD__GPIOA_10 : 1'b0;
assign PAD__GPIOA_11 = OE__GPIOA_11 ? A__GPIOA_11 : 1'bz;
assign Y__GPIOA_11 = IE__GPIOA_11 ? PAD__GPIOA_11 : 1'b0;
assign PAD__GPIOA_12 = OE__GPIOA_12 ? A__GPIOA_12 : 1'bz;
assign Y__GPIOA_12 = IE__GPIOA_12 ? PAD__GPIOA_12 : 1'b0;
assign PAD__GPIOA_13 = OE__GPIOA_13 ? A__GPIOA_13 : 1'bz;
assign Y__GPIOA_13 = IE__GPIOA_13 ? PAD__GPIOA_13 : 1'b0;
assign PAD__GPIOA_14 = OE__GPIOA_14 ? A__GPIOA_14 : 1'bz;
assign Y__GPIOA_14 = IE__GPIOA_14 ? PAD__GPIOA_14 : 1'b0;
assign PAD__GPIOA_15 = OE__GPIOA_15 ? A__GPIOA_15 : 1'bz;
assign Y__GPIOA_15 = IE__GPIOA_15 ? PAD__GPIOA_15 : 1'b0;
assign PAD__GPIOB_0 = OE__GPIOB_0 ? A__GPIOB_0 : 1'bz;
assign Y__GPIOB_0 = IE__GPIOB_0 ? PAD__GPIOB_0 : 1'b0;
assign PAD__GPIOB_1 = OE__GPIOB_1 ? A__GPIOB_1 : 1'bz;
assign Y__GPIOB_1 = IE__GPIOB_1 ? PAD__GPIOB_1 : 1'b0;
assign PAD__GPIOB_2 = OE__GPIOB_2 ? A__GPIOB_2 : 1'bz;
assign Y__GPIOB_2 = IE__GPIOB_2 ? PAD__GPIOB_2 : 1'b0;
assign PAD__GPIOB_3 = OE__GPIOB_3 ? A__GPIOB_3 : 1'bz;
assign Y__GPIOB_3 = IE__GPIOB_3 ? PAD__GPIOB_3 : 1'b0;
assign PAD__GPIOB_4 = OE__GPIOB_4 ? A__GPIOB_4 : 1'bz;
assign Y__GPIOB_4 = IE__GPIOB_4 ? PAD__GPIOB_4 : 1'b0;
assign PAD__GPIOB_5 = OE__GPIOB_5 ? A__GPIOB_5 : 1'bz;
assign Y__GPIOB_5 = IE__GPIOB_5 ? PAD__GPIOB_5 : 1'b0;
assign PAD__GPIOB_6 = OE__GPIOB_6 ? A__GPIOB_6 : 1'bz;
assign Y__GPIOB_6 = IE__GPIOB_6 ? PAD__GPIOB_6 : 1'b0;
assign PAD__GPIOB_7 = OE__GPIOB_7 ? A__GPIOB_7 : 1'bz;
assign Y__GPIOB_7 = IE__GPIOB_7 ? PAD__GPIOB_7 : 1'b0;
assign PAD__GPIOB_8 = OE__GPIOB_8 ? A__GPIOB_8 : 1'bz;
assign Y__GPIOB_8 = IE__GPIOB_8 ? PAD__GPIOB_8 : 1'b0;
assign PAD__GPIOB_9 = OE__GPIOB_9 ? A__GPIOB_9 : 1'bz;
assign Y__GPIOB_9 = IE__GPIOB_9 ? PAD__GPIOB_9 : 1'b0;
assign PAD__GPIOB_10 = OE__GPIOB_10 ? A__GPIOB_10 : 1'bz;
assign Y__GPIOB_10 = IE__GPIOB_10 ? PAD__GPIOB_10 : 1'b0;
assign PAD__GPIOB_11 = OE__GPIOB_11 ? A__GPIOB_11 : 1'bz;
assign Y__GPIOB_11 = IE__GPIOB_11 ? PAD__GPIOB_11 : 1'b0;
assign PAD__GPIOB_12 = OE__GPIOB_12 ? A__GPIOB_12 : 1'bz;
assign Y__GPIOB_12 = IE__GPIOB_12 ? PAD__GPIOB_12 : 1'b0;
assign PAD__GPIOB_13 = OE__GPIOB_13 ? A__GPIOB_13 : 1'bz;
assign Y__GPIOB_13 = IE__GPIOB_13 ? PAD__GPIOB_13 : 1'b0;
assign PAD__GPIOB_14 = OE__GPIOB_14 ? A__GPIOB_14 : 1'bz;
assign Y__GPIOB_14 = IE__GPIOB_14 ? PAD__GPIOB_14 : 1'b0;
assign PAD__GPIOB_15 = OE__GPIOB_15 ? A__GPIOB_15 : 1'bz;
assign Y__GPIOB_15 = IE__GPIOB_15 ? PAD__GPIOB_15 : 1'b0;
assign PAD__GPIOC_0 = OE__GPIOC_0 ? A__GPIOC_0 : 1'bz;
assign Y__GPIOC_0 = IE__GPIOC_0 ? PAD__GPIOC_0 : 1'b0;
assign PAD__GPIOC_1 = OE__GPIOC_1 ? A__GPIOC_1 : 1'bz;
assign Y__GPIOC_1 = IE__GPIOC_1 ? PAD__GPIOC_1 : 1'b0;
assign PAD__GPIOC_2 = OE__GPIOC_2 ? A__GPIOC_2 : 1'bz;
assign Y__GPIOC_2 = IE__GPIOC_2 ? PAD__GPIOC_2 : 1'b0;
assign PAD__GPIOC_3 = OE__GPIOC_3 ? A__GPIOC_3 : 1'bz;
assign Y__GPIOC_3 = IE__GPIOC_3 ? PAD__GPIOC_3 : 1'b0;
assign PAD__GPIOC_4 = OE__GPIOC_4 ? A__GPIOC_4 : 1'bz;
assign Y__GPIOC_4 = IE__GPIOC_4 ? PAD__GPIOC_4 : 1'b0;
assign PAD__GPIOC_5 = OE__GPIOC_5 ? A__GPIOC_5 : 1'bz;
assign Y__GPIOC_5 = IE__GPIOC_5 ? PAD__GPIOC_5 : 1'b0;
assign PAD__GPIOC_6 = OE__GPIOC_6 ? A__GPIOC_6 : 1'bz;
assign Y__GPIOC_6 = IE__GPIOC_6 ? PAD__GPIOC_6 : 1'b0;
assign PAD__GPIOC_7 = OE__GPIOC_7 ? A__GPIOC_7 : 1'bz;
assign Y__GPIOC_7 = IE__GPIOC_7 ? PAD__GPIOC_7 : 1'b0;
assign PAD__GPIOC_8 = OE__GPIOC_8 ? A__GPIOC_8 : 1'bz;
assign Y__GPIOC_8 = IE__GPIOC_8 ? PAD__GPIOC_8 : 1'b0;
assign PAD__GPIOC_9 = OE__GPIOC_9 ? A__GPIOC_9 : 1'bz;
assign Y__GPIOC_9 = IE__GPIOC_9 ? PAD__GPIOC_9 : 1'b0;
assign PAD__GPIOC_10 = OE__GPIOC_10 ? A__GPIOC_10 : 1'bz;
assign Y__GPIOC_10 = IE__GPIOC_10 ? PAD__GPIOC_10 : 1'b0;
assign PAD__GPIOC_11 = OE__GPIOC_11 ? A__GPIOC_11 : 1'bz;
assign Y__GPIOC_11 = IE__GPIOC_11 ? PAD__GPIOC_11 : 1'b0;
assign PAD__GPIOC_12 = OE__GPIOC_12 ? A__GPIOC_12 : 1'bz;
assign Y__GPIOC_12 = IE__GPIOC_12 ? PAD__GPIOC_12 : 1'b0;
assign PAD__GPIOC_13 = OE__GPIOC_13 ? A__GPIOC_13 : 1'bz;
assign Y__GPIOC_13 = IE__GPIOC_13 ? PAD__GPIOC_13 : 1'b0;
assign PAD__GPIOC_14 = OE__GPIOC_14 ? A__GPIOC_14 : 1'bz;
assign Y__GPIOC_14 = IE__GPIOC_14 ? PAD__GPIOC_14 : 1'b0;
assign PAD__GPIOC_15 = OE__GPIOC_15 ? A__GPIOC_15 : 1'bz;
assign Y__GPIOC_15 = IE__GPIOC_15 ? PAD__GPIOC_15 : 1'b0;
assign PAD__GPIOD_0 = OE__GPIOD_0 ? A__GPIOD_0 : 1'bz;
assign Y__GPIOD_0 = IE__GPIOD_0 ? PAD__GPIOD_0 : 1'b0;
assign PAD__GPIOD_1 = OE__GPIOD_1 ? A__GPIOD_1 : 1'bz;
assign Y__GPIOD_1 = IE__GPIOD_1 ? PAD__GPIOD_1 : 1'b0;
assign PAD__GPIOD_2 = OE__GPIOD_2 ? A__GPIOD_2 : 1'bz;
assign Y__GPIOD_2 = IE__GPIOD_2 ? PAD__GPIOD_2 : 1'b0;
assign PAD__GPIOD_3 = OE__GPIOD_3 ? A__GPIOD_3 : 1'bz;
assign Y__GPIOD_3 = IE__GPIOD_3 ? PAD__GPIOD_3 : 1'b0;
assign PAD__GPIOD_4 = OE__GPIOD_4 ? A__GPIOD_4 : 1'bz;
assign Y__GPIOD_4 = IE__GPIOD_4 ? PAD__GPIOD_4 : 1'b0;
assign PAD__GPIOD_5 = OE__GPIOD_5 ? A__GPIOD_5 : 1'bz;
assign Y__GPIOD_5 = IE__GPIOD_5 ? PAD__GPIOD_5 : 1'b0;
assign PAD__GPIOD_6 = OE__GPIOD_6 ? A__GPIOD_6 : 1'bz;
assign Y__GPIOD_6 = IE__GPIOD_6 ? PAD__GPIOD_6 : 1'b0;
assign PAD__GPIOD_7 = OE__GPIOD_7 ? A__GPIOD_7 : 1'bz;
assign Y__GPIOD_7 = IE__GPIOD_7 ? PAD__GPIOD_7 : 1'b0;
assign PAD__GPIOD_8 = OE__GPIOD_8 ? A__GPIOD_8 : 1'bz;
assign Y__GPIOD_8 = IE__GPIOD_8 ? PAD__GPIOD_8 : 1'b0;
assign PAD__GPIOD_9 = OE__GPIOD_9 ? A__GPIOD_9 : 1'bz;
assign Y__GPIOD_9 = IE__GPIOD_9 ? PAD__GPIOD_9 : 1'b0;
assign PAD__GPIOD_10 = OE__GPIOD_10 ? A__GPIOD_10 : 1'bz;
assign Y__GPIOD_10 = IE__GPIOD_10 ? PAD__GPIOD_10 : 1'b0;
assign PAD__GPIOD_11 = OE__GPIOD_11 ? A__GPIOD_11 : 1'bz;
assign Y__GPIOD_11 = IE__GPIOD_11 ? PAD__GPIOD_11 : 1'b0;
assign PAD__GPIOD_12 = OE__GPIOD_12 ? A__GPIOD_12 : 1'bz;
assign Y__GPIOD_12 = IE__GPIOD_12 ? PAD__GPIOD_12 : 1'b0;
assign PAD__GPIOD_13 = OE__GPIOD_13 ? A__GPIOD_13 : 1'bz;
assign Y__GPIOD_13 = IE__GPIOD_13 ? PAD__GPIOD_13 : 1'b0;
assign PAD__GPIOD_14 = OE__GPIOD_14 ? A__GPIOD_14 : 1'bz;
assign Y__GPIOD_14 = IE__GPIOD_14 ? PAD__GPIOD_14 : 1'b0;
assign PAD__GPIOD_15 = OE__GPIOD_15 ? A__GPIOD_15 : 1'bz;
assign Y__GPIOD_15 = IE__GPIOD_15 ? PAD__GPIOD_15 : 1'b0;

// SDMMC
assign PAD__SDMMC_0 = OE__SDMMC_0 ? A__SDMMC_0 : 1'bz;
assign Y__SDMMC_0 = IE__SDMMC_0 ? PAD__SDMMC_0 : 1'b0;
assign PAD__SDMMC_1 = OE__SDMMC_1 ? A__SDMMC_1 : 1'bz;
assign Y__SDMMC_1 = IE__SDMMC_1 ? PAD__SDMMC_1 : 1'b0;
assign PAD__SDMMC_2 = OE__SDMMC_2 ? A__SDMMC_2 : 1'bz;
assign Y__SDMMC_2 = IE__SDMMC_2 ? PAD__SDMMC_2 : 1'b0;
assign PAD__SDMMC_3 = OE__SDMMC_3 ? A__SDMMC_3 : 1'bz;
assign Y__SDMMC_3 = IE__SDMMC_3 ? PAD__SDMMC_3 : 1'b0;
assign PAD__SDMMC_4 = OE__SDMMC_4 ? A__SDMMC_4 : 1'bz;
assign Y__SDMMC_4 = IE__SDMMC_4 ? PAD__SDMMC_4 : 1'b0;
assign PAD__SDMMC_5 = OE__SDMMC_5 ? A__SDMMC_5 : 1'bz;
assign Y__SDMMC_5 = IE__SDMMC_5 ? PAD__SDMMC_5 : 1'b0;
assign PAD__SDMMC_6 = OE__SDMMC_6 ? A__SDMMC_6 : 1'bz;
assign Y__SDMMC_6 = IE__SDMMC_6 ? PAD__SDMMC_6 : 1'b0;
assign PAD__SDMMC_7 = OE__SDMMC_7 ? A__SDMMC_7 : 1'bz;
assign Y__SDMMC_7 = IE__SDMMC_7 ? PAD__SDMMC_7 : 1'b0;
assign PAD__SDMMC_8 = OE__SDMMC_8 ? A__SDMMC_8 : 1'bz;
assign Y__SDMMC_8 = IE__SDMMC_8 ? PAD__SDMMC_8 : 1'b0;
assign PAD__SDMMC_9 = OE__SDMMC_9 ? A__SDMMC_9 : 1'bz;
assign Y__SDMMC_9 = IE__SDMMC_9 ? PAD__SDMMC_9 : 1'b0;
assign PAD__SDMMC_10 = OE__SDMMC_10 ? A__SDMMC_10 : 1'bz;
assign Y__SDMMC_10 = IE__SDMMC_10 ? PAD__SDMMC_10 : 1'b0;
assign PAD__SDMMC_11 = OE__SDMMC_11 ? A__SDMMC_11 : 1'bz;
assign Y__SDMMC_11 = IE__SDMMC_11 ? PAD__SDMMC_11 : 1'b0;

// CLKSEL
assign Y__CLKSEL = PAD__CLKSEL;

// OTP
rnmos uOTP0VPP0 (AY__OTP_0_VPP,PAD__OTP_0_VPP,1'b1);
rnmos uOTP0VPP1 (PAD__OTP_0_VPP,AY__OTP_0_VPP,1'b1);
rnmos uOTP0VBG0 (AY__OTP_0_VBG,PAD__OTP_0_VBG,1'b1);
rnmos uOTP0VBG1 (PAD__OTP_0_VBG,AY__OTP_0_VBG,1'b1);
rnmos uOTP0VTDO0 (AY__OTP_0_VTDO,PAD__OTP_0_VTDO,1'b1);
rnmos uOTP0VTDO1 (PAD__OTP_0_VTDO,AY__OTP_0_VTDO,1'b1);
rnmos uOTP0VREFM0 (AY__OTP_0_VREFM,PAD__OTP_0_VREFM,1'b1);
rnmos uOTP0VREFM1 (PAD__OTP_0_VREFM,AY__OTP_0_VREFM,1'b1);
rnmos uOTP1VPP0 (AY__OTP_1_VPP,PAD__OTP_1_VPP,1'b1);
rnmos uOTP1VPP1 (PAD__OTP_1_VPP,AY__OTP_1_VPP,1'b1);
rnmos uOTP1VBG0 (AY__OTP_1_VBG,PAD__OTP_1_VBG,1'b1);
rnmos uOTP1VBG1 (PAD__OTP_1_VBG,AY__OTP_1_VBG,1'b1);
rnmos uOTP1VTDO0 (AY__OTP_1_VTDO,PAD__OTP_1_VTDO,1'b1);
rnmos uOTP1VTDO1 (PAD__OTP_1_VTDO,AY__OTP_1_VTDO,1'b1);
rnmos uOTP1VREFM0 (AY__OTP_1_VREFM,PAD__OTP_1_VREFM,1'b1);
rnmos uOTP1VREFM1 (PAD__OTP_1_VREFM,AY__OTP_1_VREFM,1'b1);
endmodule
