//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLToAXI4(
  input          rf_reset,
  input          clock,
  input          reset,
  output         auto_in_a_ready,
  input          auto_in_a_valid,
  input  [2:0]   auto_in_a_bits_opcode,
  input  [2:0]   auto_in_a_bits_param,
  input  [2:0]   auto_in_a_bits_size,
  input  [6:0]   auto_in_a_bits_source,
  input  [36:0]  auto_in_a_bits_address,
  input          auto_in_a_bits_user_amba_prot_bufferable,
  input          auto_in_a_bits_user_amba_prot_modifiable,
  input          auto_in_a_bits_user_amba_prot_readalloc,
  input          auto_in_a_bits_user_amba_prot_writealloc,
  input          auto_in_a_bits_user_amba_prot_privileged,
  input          auto_in_a_bits_user_amba_prot_secure,
  input          auto_in_a_bits_user_amba_prot_fetch,
  input  [15:0]  auto_in_a_bits_mask,
  input  [127:0] auto_in_a_bits_data,
  input          auto_in_a_bits_corrupt,
  input          auto_in_d_ready,
  output         auto_in_d_valid,
  output [2:0]   auto_in_d_bits_opcode,
  output [2:0]   auto_in_d_bits_size,
  output [6:0]   auto_in_d_bits_source,
  output         auto_in_d_bits_denied,
  output [127:0] auto_in_d_bits_data,
  output         auto_in_d_bits_corrupt,
  input          auto_out_aw_ready,
  output         auto_out_aw_valid,
  output [3:0]   auto_out_aw_bits_id,
  output [36:0]  auto_out_aw_bits_addr,
  output [7:0]   auto_out_aw_bits_len,
  output [2:0]   auto_out_aw_bits_size,
  output [3:0]   auto_out_aw_bits_cache,
  output [2:0]   auto_out_aw_bits_prot,
  output [3:0]   auto_out_aw_bits_echo_tl_state_size,
  output [6:0]   auto_out_aw_bits_echo_tl_state_source,
  input          auto_out_w_ready,
  output         auto_out_w_valid,
  output [127:0] auto_out_w_bits_data,
  output [15:0]  auto_out_w_bits_strb,
  output         auto_out_w_bits_last,
  output         auto_out_b_ready,
  input          auto_out_b_valid,
  input  [3:0]   auto_out_b_bits_id,
  input  [1:0]   auto_out_b_bits_resp,
  input  [3:0]   auto_out_b_bits_echo_tl_state_size,
  input  [6:0]   auto_out_b_bits_echo_tl_state_source,
  input          auto_out_ar_ready,
  output         auto_out_ar_valid,
  output [3:0]   auto_out_ar_bits_id,
  output [36:0]  auto_out_ar_bits_addr,
  output [7:0]   auto_out_ar_bits_len,
  output [2:0]   auto_out_ar_bits_size,
  output [3:0]   auto_out_ar_bits_cache,
  output [2:0]   auto_out_ar_bits_prot,
  output [3:0]   auto_out_ar_bits_echo_tl_state_size,
  output [6:0]   auto_out_ar_bits_echo_tl_state_source,
  output         auto_out_r_ready,
  input          auto_out_r_valid,
  input  [3:0]   auto_out_r_bits_id,
  input  [127:0] auto_out_r_bits_data,
  input  [1:0]   auto_out_r_bits_resp,
  input  [3:0]   auto_out_r_bits_echo_tl_state_size,
  input  [6:0]   auto_out_r_bits_echo_tl_state_source,
  input          auto_out_r_bits_last
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
`endif // RANDOMIZE_REG_INIT
  wire  deq_rf_reset; // @[Decoupled.scala 296:21]
  wire  deq_clock; // @[Decoupled.scala 296:21]
  wire  deq_reset; // @[Decoupled.scala 296:21]
  wire  deq_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  deq_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [127:0] deq_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire [15:0] deq_io_enq_bits_strb; // @[Decoupled.scala 296:21]
  wire  deq_io_enq_bits_last; // @[Decoupled.scala 296:21]
  wire  deq_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  deq_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [127:0] deq_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire [15:0] deq_io_deq_bits_strb; // @[Decoupled.scala 296:21]
  wire  deq_io_deq_bits_last; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_rf_reset; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_clock; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_reset; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [3:0] queue_arw_deq_io_enq_bits_id; // @[Decoupled.scala 296:21]
  wire [36:0] queue_arw_deq_io_enq_bits_addr; // @[Decoupled.scala 296:21]
  wire [7:0] queue_arw_deq_io_enq_bits_len; // @[Decoupled.scala 296:21]
  wire [2:0] queue_arw_deq_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [3:0] queue_arw_deq_io_enq_bits_cache; // @[Decoupled.scala 296:21]
  wire [2:0] queue_arw_deq_io_enq_bits_prot; // @[Decoupled.scala 296:21]
  wire [3:0] queue_arw_deq_io_enq_bits_echo_tl_state_size; // @[Decoupled.scala 296:21]
  wire [6:0] queue_arw_deq_io_enq_bits_echo_tl_state_source; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_io_enq_bits_wen; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [3:0] queue_arw_deq_io_deq_bits_id; // @[Decoupled.scala 296:21]
  wire [36:0] queue_arw_deq_io_deq_bits_addr; // @[Decoupled.scala 296:21]
  wire [7:0] queue_arw_deq_io_deq_bits_len; // @[Decoupled.scala 296:21]
  wire [2:0] queue_arw_deq_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [3:0] queue_arw_deq_io_deq_bits_cache; // @[Decoupled.scala 296:21]
  wire [2:0] queue_arw_deq_io_deq_bits_prot; // @[Decoupled.scala 296:21]
  wire [3:0] queue_arw_deq_io_deq_bits_echo_tl_state_size; // @[Decoupled.scala 296:21]
  wire [6:0] queue_arw_deq_io_deq_bits_echo_tl_state_source; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_io_deq_bits_wen; // @[Decoupled.scala 296:21]
  wire  a_isPut = ~auto_in_a_bits_opcode[2]; // @[Edges.scala 91:28]
  reg  count_5; // @[ToAXI4.scala 250:28]
  reg  count_4; // @[ToAXI4.scala 250:28]
  reg [2:0] count_12; // @[ToAXI4.scala 250:28]
  wire  idle_11 = count_12 == 3'h0; // @[ToAXI4.scala 252:26]
  reg  write_11; // @[ToAXI4.scala 251:24]
  wire  mismatch_6 = write_11 != a_isPut; // @[ToAXI4.scala 263:50]
  wire  idStall_11 = ~idle_11 & mismatch_6 | count_12 == 3'h7; // @[ToAXI4.scala 264:34]
  reg  count_16; // @[ToAXI4.scala 250:28]
  reg  count_15; // @[ToAXI4.scala 250:28]
  reg  count_3; // @[ToAXI4.scala 250:28]
  reg  count_2; // @[ToAXI4.scala 250:28]
  reg [2:0] count_11; // @[ToAXI4.scala 250:28]
  wire  idle_10 = count_11 == 3'h0; // @[ToAXI4.scala 252:26]
  reg  write_10; // @[ToAXI4.scala 251:24]
  wire  mismatch_5 = write_10 != a_isPut; // @[ToAXI4.scala 263:50]
  wire  idStall_10 = ~idle_10 & mismatch_5 | count_11 == 3'h7; // @[ToAXI4.scala 264:34]
  reg  count_14; // @[ToAXI4.scala 250:28]
  reg  count_13; // @[ToAXI4.scala 250:28]
  reg  count_1; // @[ToAXI4.scala 250:28]
  reg [2:0] count_10; // @[ToAXI4.scala 250:28]
  wire  idle_9 = count_10 == 3'h0; // @[ToAXI4.scala 252:26]
  reg  write_9; // @[ToAXI4.scala 251:24]
  wire  mismatch_4 = write_9 != a_isPut; // @[ToAXI4.scala 263:50]
  wire  idStall_9 = ~idle_9 & mismatch_4 | count_10 == 3'h4; // @[ToAXI4.scala 264:34]
  reg [3:0] count_9; // @[ToAXI4.scala 250:28]
  wire  idle_8 = count_9 == 4'h0; // @[ToAXI4.scala 252:26]
  reg  write_8; // @[ToAXI4.scala 251:24]
  wire  mismatch_3 = write_8 != a_isPut; // @[ToAXI4.scala 263:50]
  wire  idStall_8 = ~idle_8 & mismatch_3 | count_9 == 4'h8; // @[ToAXI4.scala 264:34]
  reg [3:0] count_8; // @[ToAXI4.scala 250:28]
  wire  idle_7 = count_8 == 4'h0; // @[ToAXI4.scala 252:26]
  reg  write_7; // @[ToAXI4.scala 251:24]
  wire  mismatch_2 = write_7 != a_isPut; // @[ToAXI4.scala 263:50]
  wire  idStall_7 = ~idle_7 & mismatch_2 | count_8 == 4'h8; // @[ToAXI4.scala 264:34]
  reg [3:0] count_7; // @[ToAXI4.scala 250:28]
  wire  idle_6 = count_7 == 4'h0; // @[ToAXI4.scala 252:26]
  reg  write_6; // @[ToAXI4.scala 251:24]
  wire  mismatch_1 = write_6 != a_isPut; // @[ToAXI4.scala 263:50]
  wire  idStall_6 = ~idle_6 & mismatch_1 | count_7 == 4'h8; // @[ToAXI4.scala 264:34]
  reg [3:0] count_6; // @[ToAXI4.scala 250:28]
  wire  idle_5 = count_6 == 4'h0; // @[ToAXI4.scala 252:26]
  reg  write_5; // @[ToAXI4.scala 251:24]
  wire  mismatch = write_5 != a_isPut; // @[ToAXI4.scala 263:50]
  wire  idStall_5 = ~idle_5 & mismatch | count_6 == 4'h8; // @[ToAXI4.scala 264:34]
  wire  _GEN_124 = 7'h8 == auto_in_a_bits_source ? idStall_6 : idStall_5; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_125 = 7'h9 == auto_in_a_bits_source ? idStall_6 : _GEN_124; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_126 = 7'ha == auto_in_a_bits_source ? idStall_6 : _GEN_125; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_127 = 7'hb == auto_in_a_bits_source ? idStall_6 : _GEN_126; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_128 = 7'hc == auto_in_a_bits_source ? idStall_6 : _GEN_127; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_129 = 7'hd == auto_in_a_bits_source ? idStall_6 : _GEN_128; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_130 = 7'he == auto_in_a_bits_source ? idStall_6 : _GEN_129; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_131 = 7'hf == auto_in_a_bits_source ? idStall_6 : _GEN_130; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_132 = 7'h10 == auto_in_a_bits_source ? idStall_7 : _GEN_131; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_133 = 7'h11 == auto_in_a_bits_source ? idStall_7 : _GEN_132; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_134 = 7'h12 == auto_in_a_bits_source ? idStall_7 : _GEN_133; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_135 = 7'h13 == auto_in_a_bits_source ? idStall_7 : _GEN_134; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_136 = 7'h14 == auto_in_a_bits_source ? idStall_7 : _GEN_135; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_137 = 7'h15 == auto_in_a_bits_source ? idStall_7 : _GEN_136; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_138 = 7'h16 == auto_in_a_bits_source ? idStall_7 : _GEN_137; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_139 = 7'h17 == auto_in_a_bits_source ? idStall_7 : _GEN_138; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_140 = 7'h18 == auto_in_a_bits_source ? idStall_8 : _GEN_139; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_141 = 7'h19 == auto_in_a_bits_source ? idStall_8 : _GEN_140; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_142 = 7'h1a == auto_in_a_bits_source ? idStall_8 : _GEN_141; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_143 = 7'h1b == auto_in_a_bits_source ? idStall_8 : _GEN_142; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_144 = 7'h1c == auto_in_a_bits_source ? idStall_8 : _GEN_143; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_145 = 7'h1d == auto_in_a_bits_source ? idStall_8 : _GEN_144; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_146 = 7'h1e == auto_in_a_bits_source ? idStall_8 : _GEN_145; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_147 = 7'h1f == auto_in_a_bits_source ? idStall_8 : _GEN_146; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_148 = 7'h20 == auto_in_a_bits_source ? idStall_9 : _GEN_147; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_149 = 7'h21 == auto_in_a_bits_source ? idStall_9 : _GEN_148; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_150 = 7'h22 == auto_in_a_bits_source ? idStall_9 : _GEN_149; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_151 = 7'h23 == auto_in_a_bits_source ? idStall_9 : _GEN_150; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_152 = 7'h24 == auto_in_a_bits_source ? count_1 : _GEN_151; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_153 = 7'h25 == auto_in_a_bits_source ? 1'h0 : _GEN_152; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_154 = 7'h26 == auto_in_a_bits_source ? 1'h0 : _GEN_153; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_155 = 7'h27 == auto_in_a_bits_source ? 1'h0 : _GEN_154; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_156 = 7'h28 == auto_in_a_bits_source ? 1'h0 : _GEN_155; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_157 = 7'h29 == auto_in_a_bits_source ? 1'h0 : _GEN_156; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_158 = 7'h2a == auto_in_a_bits_source ? 1'h0 : _GEN_157; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_159 = 7'h2b == auto_in_a_bits_source ? 1'h0 : _GEN_158; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_160 = 7'h2c == auto_in_a_bits_source ? 1'h0 : _GEN_159; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_161 = 7'h2d == auto_in_a_bits_source ? 1'h0 : _GEN_160; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_162 = 7'h2e == auto_in_a_bits_source ? 1'h0 : _GEN_161; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_163 = 7'h2f == auto_in_a_bits_source ? 1'h0 : _GEN_162; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_164 = 7'h30 == auto_in_a_bits_source ? 1'h0 : _GEN_163; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_165 = 7'h31 == auto_in_a_bits_source ? 1'h0 : _GEN_164; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_166 = 7'h32 == auto_in_a_bits_source ? 1'h0 : _GEN_165; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_167 = 7'h33 == auto_in_a_bits_source ? 1'h0 : _GEN_166; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_168 = 7'h34 == auto_in_a_bits_source ? 1'h0 : _GEN_167; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_169 = 7'h35 == auto_in_a_bits_source ? 1'h0 : _GEN_168; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_170 = 7'h36 == auto_in_a_bits_source ? 1'h0 : _GEN_169; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_171 = 7'h37 == auto_in_a_bits_source ? 1'h0 : _GEN_170; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_172 = 7'h38 == auto_in_a_bits_source ? 1'h0 : _GEN_171; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_173 = 7'h39 == auto_in_a_bits_source ? 1'h0 : _GEN_172; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_174 = 7'h3a == auto_in_a_bits_source ? 1'h0 : _GEN_173; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_175 = 7'h3b == auto_in_a_bits_source ? 1'h0 : _GEN_174; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_176 = 7'h3c == auto_in_a_bits_source ? 1'h0 : _GEN_175; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_177 = 7'h3d == auto_in_a_bits_source ? 1'h0 : _GEN_176; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_178 = 7'h3e == auto_in_a_bits_source ? 1'h0 : _GEN_177; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_179 = 7'h3f == auto_in_a_bits_source ? 1'h0 : _GEN_178; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_180 = 7'h40 == auto_in_a_bits_source ? count_13 : _GEN_179; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_181 = 7'h41 == auto_in_a_bits_source ? count_14 : _GEN_180; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_182 = 7'h42 == auto_in_a_bits_source ? idStall_10 : _GEN_181; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_183 = 7'h43 == auto_in_a_bits_source ? idStall_10 : _GEN_182; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_184 = 7'h44 == auto_in_a_bits_source ? idStall_10 : _GEN_183; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_185 = 7'h45 == auto_in_a_bits_source ? idStall_10 : _GEN_184; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_186 = 7'h46 == auto_in_a_bits_source ? idStall_10 : _GEN_185; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_187 = 7'h47 == auto_in_a_bits_source ? idStall_10 : _GEN_186; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_188 = 7'h48 == auto_in_a_bits_source ? idStall_10 : _GEN_187; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_189 = 7'h49 == auto_in_a_bits_source ? 1'h0 : _GEN_188; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_190 = 7'h4a == auto_in_a_bits_source ? 1'h0 : _GEN_189; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_191 = 7'h4b == auto_in_a_bits_source ? 1'h0 : _GEN_190; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_192 = 7'h4c == auto_in_a_bits_source ? 1'h0 : _GEN_191; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_193 = 7'h4d == auto_in_a_bits_source ? 1'h0 : _GEN_192; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_194 = 7'h4e == auto_in_a_bits_source ? 1'h0 : _GEN_193; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_195 = 7'h4f == auto_in_a_bits_source ? 1'h0 : _GEN_194; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_196 = 7'h50 == auto_in_a_bits_source ? count_2 : _GEN_195; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_197 = 7'h51 == auto_in_a_bits_source ? count_3 : _GEN_196; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_198 = 7'h52 == auto_in_a_bits_source ? 1'h0 : _GEN_197; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_199 = 7'h53 == auto_in_a_bits_source ? 1'h0 : _GEN_198; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_200 = 7'h54 == auto_in_a_bits_source ? 1'h0 : _GEN_199; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_201 = 7'h55 == auto_in_a_bits_source ? 1'h0 : _GEN_200; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_202 = 7'h56 == auto_in_a_bits_source ? 1'h0 : _GEN_201; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_203 = 7'h57 == auto_in_a_bits_source ? 1'h0 : _GEN_202; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_204 = 7'h58 == auto_in_a_bits_source ? 1'h0 : _GEN_203; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_205 = 7'h59 == auto_in_a_bits_source ? 1'h0 : _GEN_204; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_206 = 7'h5a == auto_in_a_bits_source ? 1'h0 : _GEN_205; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_207 = 7'h5b == auto_in_a_bits_source ? 1'h0 : _GEN_206; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_208 = 7'h5c == auto_in_a_bits_source ? 1'h0 : _GEN_207; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_209 = 7'h5d == auto_in_a_bits_source ? 1'h0 : _GEN_208; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_210 = 7'h5e == auto_in_a_bits_source ? 1'h0 : _GEN_209; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_211 = 7'h5f == auto_in_a_bits_source ? 1'h0 : _GEN_210; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_212 = 7'h60 == auto_in_a_bits_source ? count_15 : _GEN_211; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_213 = 7'h61 == auto_in_a_bits_source ? count_16 : _GEN_212; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_214 = 7'h62 == auto_in_a_bits_source ? idStall_11 : _GEN_213; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_215 = 7'h63 == auto_in_a_bits_source ? idStall_11 : _GEN_214; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_216 = 7'h64 == auto_in_a_bits_source ? idStall_11 : _GEN_215; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_217 = 7'h65 == auto_in_a_bits_source ? idStall_11 : _GEN_216; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_218 = 7'h66 == auto_in_a_bits_source ? idStall_11 : _GEN_217; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_219 = 7'h67 == auto_in_a_bits_source ? idStall_11 : _GEN_218; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_220 = 7'h68 == auto_in_a_bits_source ? idStall_11 : _GEN_219; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_221 = 7'h69 == auto_in_a_bits_source ? 1'h0 : _GEN_220; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_222 = 7'h6a == auto_in_a_bits_source ? 1'h0 : _GEN_221; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_223 = 7'h6b == auto_in_a_bits_source ? 1'h0 : _GEN_222; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_224 = 7'h6c == auto_in_a_bits_source ? 1'h0 : _GEN_223; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_225 = 7'h6d == auto_in_a_bits_source ? 1'h0 : _GEN_224; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_226 = 7'h6e == auto_in_a_bits_source ? 1'h0 : _GEN_225; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_227 = 7'h6f == auto_in_a_bits_source ? 1'h0 : _GEN_226; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_228 = 7'h70 == auto_in_a_bits_source ? count_4 : _GEN_227; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  wire  _GEN_229 = 7'h71 == auto_in_a_bits_source ? count_5 : _GEN_228; // @[ToAXI4.scala 197:49 ToAXI4.scala 197:49]
  reg [2:0] counter; // @[Edges.scala 228:27]
  wire  a_first = counter == 3'h0; // @[Edges.scala 230:25]
  wire  stall = _GEN_229 & a_first; // @[ToAXI4.scala 197:49]
  wire  _bundleIn_0_a_ready_T = ~stall; // @[ToAXI4.scala 198:21]
  reg  doneAW; // @[ToAXI4.scala 163:30]
  wire  out_arw_ready = queue_arw_deq_io_enq_ready; // @[ToAXI4.scala 149:25 Decoupled.scala 299:17]
  wire  _bundleIn_0_a_ready_T_1 = doneAW | out_arw_ready; // @[ToAXI4.scala 198:52]
  wire  out_w_ready = deq_io_enq_ready; // @[ToAXI4.scala 150:23 Decoupled.scala 299:17]
  wire  _bundleIn_0_a_ready_T_3 = a_isPut ? (doneAW | out_arw_ready) & out_w_ready : out_arw_ready; // @[ToAXI4.scala 198:34]
  wire  bundleIn_0_a_ready = _bundleIn_0_a_ready_T & _bundleIn_0_a_ready_T_3; // @[ToAXI4.scala 198:28]
  wire  _T = bundleIn_0_a_ready & auto_in_a_valid; // @[Decoupled.scala 40:37]
  wire [13:0] _beats1_decode_T_1 = 14'h7f << auto_in_a_bits_size; // @[package.scala 234:77]
  wire [6:0] _beats1_decode_T_3 = ~_beats1_decode_T_1[6:0]; // @[package.scala 234:46]
  wire [2:0] beats1_decode = _beats1_decode_T_3[6:4]; // @[Edges.scala 219:59]
  wire [2:0] beats1 = a_isPut ? beats1_decode : 3'h0; // @[Edges.scala 220:14]
  wire [2:0] counter1 = counter - 3'h1; // @[Edges.scala 229:28]
  wire  a_last = counter == 3'h1 | beats1 == 3'h0; // @[Edges.scala 231:37]
  wire  queue_arw_bits_wen = queue_arw_deq_io_deq_bits_wen; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  wire  queue_arw_valid = queue_arw_deq_io_deq_valid; // @[Decoupled.scala 317:19 Decoupled.scala 319:15]
  wire [3:0] _GEN_10 = 7'h8 == auto_in_a_bits_source ? 4'h6 : 4'h5; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_11 = 7'h9 == auto_in_a_bits_source ? 4'h6 : _GEN_10; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_12 = 7'ha == auto_in_a_bits_source ? 4'h6 : _GEN_11; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_13 = 7'hb == auto_in_a_bits_source ? 4'h6 : _GEN_12; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_14 = 7'hc == auto_in_a_bits_source ? 4'h6 : _GEN_13; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_15 = 7'hd == auto_in_a_bits_source ? 4'h6 : _GEN_14; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_16 = 7'he == auto_in_a_bits_source ? 4'h6 : _GEN_15; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_17 = 7'hf == auto_in_a_bits_source ? 4'h6 : _GEN_16; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_18 = 7'h10 == auto_in_a_bits_source ? 4'h7 : _GEN_17; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_19 = 7'h11 == auto_in_a_bits_source ? 4'h7 : _GEN_18; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_20 = 7'h12 == auto_in_a_bits_source ? 4'h7 : _GEN_19; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_21 = 7'h13 == auto_in_a_bits_source ? 4'h7 : _GEN_20; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_22 = 7'h14 == auto_in_a_bits_source ? 4'h7 : _GEN_21; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_23 = 7'h15 == auto_in_a_bits_source ? 4'h7 : _GEN_22; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_24 = 7'h16 == auto_in_a_bits_source ? 4'h7 : _GEN_23; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_25 = 7'h17 == auto_in_a_bits_source ? 4'h7 : _GEN_24; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_26 = 7'h18 == auto_in_a_bits_source ? 4'h8 : _GEN_25; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_27 = 7'h19 == auto_in_a_bits_source ? 4'h8 : _GEN_26; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_28 = 7'h1a == auto_in_a_bits_source ? 4'h8 : _GEN_27; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_29 = 7'h1b == auto_in_a_bits_source ? 4'h8 : _GEN_28; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_30 = 7'h1c == auto_in_a_bits_source ? 4'h8 : _GEN_29; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_31 = 7'h1d == auto_in_a_bits_source ? 4'h8 : _GEN_30; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_32 = 7'h1e == auto_in_a_bits_source ? 4'h8 : _GEN_31; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_33 = 7'h1f == auto_in_a_bits_source ? 4'h8 : _GEN_32; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_34 = 7'h20 == auto_in_a_bits_source ? 4'h9 : _GEN_33; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_35 = 7'h21 == auto_in_a_bits_source ? 4'h9 : _GEN_34; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_36 = 7'h22 == auto_in_a_bits_source ? 4'h9 : _GEN_35; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_37 = 7'h23 == auto_in_a_bits_source ? 4'h9 : _GEN_36; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_38 = 7'h24 == auto_in_a_bits_source ? 4'h0 : _GEN_37; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_39 = 7'h25 == auto_in_a_bits_source ? 4'h0 : _GEN_38; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_40 = 7'h26 == auto_in_a_bits_source ? 4'h0 : _GEN_39; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_41 = 7'h27 == auto_in_a_bits_source ? 4'h0 : _GEN_40; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_42 = 7'h28 == auto_in_a_bits_source ? 4'h0 : _GEN_41; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_43 = 7'h29 == auto_in_a_bits_source ? 4'h0 : _GEN_42; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_44 = 7'h2a == auto_in_a_bits_source ? 4'h0 : _GEN_43; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_45 = 7'h2b == auto_in_a_bits_source ? 4'h0 : _GEN_44; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_46 = 7'h2c == auto_in_a_bits_source ? 4'h0 : _GEN_45; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_47 = 7'h2d == auto_in_a_bits_source ? 4'h0 : _GEN_46; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_48 = 7'h2e == auto_in_a_bits_source ? 4'h0 : _GEN_47; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_49 = 7'h2f == auto_in_a_bits_source ? 4'h0 : _GEN_48; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_50 = 7'h30 == auto_in_a_bits_source ? 4'h0 : _GEN_49; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_51 = 7'h31 == auto_in_a_bits_source ? 4'h0 : _GEN_50; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_52 = 7'h32 == auto_in_a_bits_source ? 4'h0 : _GEN_51; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_53 = 7'h33 == auto_in_a_bits_source ? 4'h0 : _GEN_52; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_54 = 7'h34 == auto_in_a_bits_source ? 4'h0 : _GEN_53; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_55 = 7'h35 == auto_in_a_bits_source ? 4'h0 : _GEN_54; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_56 = 7'h36 == auto_in_a_bits_source ? 4'h0 : _GEN_55; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_57 = 7'h37 == auto_in_a_bits_source ? 4'h0 : _GEN_56; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_58 = 7'h38 == auto_in_a_bits_source ? 4'h0 : _GEN_57; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_59 = 7'h39 == auto_in_a_bits_source ? 4'h0 : _GEN_58; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_60 = 7'h3a == auto_in_a_bits_source ? 4'h0 : _GEN_59; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_61 = 7'h3b == auto_in_a_bits_source ? 4'h0 : _GEN_60; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_62 = 7'h3c == auto_in_a_bits_source ? 4'h0 : _GEN_61; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_63 = 7'h3d == auto_in_a_bits_source ? 4'h0 : _GEN_62; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_64 = 7'h3e == auto_in_a_bits_source ? 4'h0 : _GEN_63; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_65 = 7'h3f == auto_in_a_bits_source ? 4'h0 : _GEN_64; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_66 = 7'h40 == auto_in_a_bits_source ? 4'hc : _GEN_65; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_67 = 7'h41 == auto_in_a_bits_source ? 4'hd : _GEN_66; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_68 = 7'h42 == auto_in_a_bits_source ? 4'ha : _GEN_67; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_69 = 7'h43 == auto_in_a_bits_source ? 4'ha : _GEN_68; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_70 = 7'h44 == auto_in_a_bits_source ? 4'ha : _GEN_69; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_71 = 7'h45 == auto_in_a_bits_source ? 4'ha : _GEN_70; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_72 = 7'h46 == auto_in_a_bits_source ? 4'ha : _GEN_71; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_73 = 7'h47 == auto_in_a_bits_source ? 4'ha : _GEN_72; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_74 = 7'h48 == auto_in_a_bits_source ? 4'ha : _GEN_73; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_75 = 7'h49 == auto_in_a_bits_source ? 4'h0 : _GEN_74; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_76 = 7'h4a == auto_in_a_bits_source ? 4'h0 : _GEN_75; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_77 = 7'h4b == auto_in_a_bits_source ? 4'h0 : _GEN_76; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_78 = 7'h4c == auto_in_a_bits_source ? 4'h0 : _GEN_77; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_79 = 7'h4d == auto_in_a_bits_source ? 4'h0 : _GEN_78; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_80 = 7'h4e == auto_in_a_bits_source ? 4'h0 : _GEN_79; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_81 = 7'h4f == auto_in_a_bits_source ? 4'h0 : _GEN_80; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_82 = 7'h50 == auto_in_a_bits_source ? 4'h1 : _GEN_81; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_83 = 7'h51 == auto_in_a_bits_source ? 4'h2 : _GEN_82; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_84 = 7'h52 == auto_in_a_bits_source ? 4'h0 : _GEN_83; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_85 = 7'h53 == auto_in_a_bits_source ? 4'h0 : _GEN_84; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_86 = 7'h54 == auto_in_a_bits_source ? 4'h0 : _GEN_85; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_87 = 7'h55 == auto_in_a_bits_source ? 4'h0 : _GEN_86; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_88 = 7'h56 == auto_in_a_bits_source ? 4'h0 : _GEN_87; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_89 = 7'h57 == auto_in_a_bits_source ? 4'h0 : _GEN_88; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_90 = 7'h58 == auto_in_a_bits_source ? 4'h0 : _GEN_89; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_91 = 7'h59 == auto_in_a_bits_source ? 4'h0 : _GEN_90; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_92 = 7'h5a == auto_in_a_bits_source ? 4'h0 : _GEN_91; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_93 = 7'h5b == auto_in_a_bits_source ? 4'h0 : _GEN_92; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_94 = 7'h5c == auto_in_a_bits_source ? 4'h0 : _GEN_93; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_95 = 7'h5d == auto_in_a_bits_source ? 4'h0 : _GEN_94; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_96 = 7'h5e == auto_in_a_bits_source ? 4'h0 : _GEN_95; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_97 = 7'h5f == auto_in_a_bits_source ? 4'h0 : _GEN_96; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_98 = 7'h60 == auto_in_a_bits_source ? 4'he : _GEN_97; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_99 = 7'h61 == auto_in_a_bits_source ? 4'hf : _GEN_98; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_100 = 7'h62 == auto_in_a_bits_source ? 4'hb : _GEN_99; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_101 = 7'h63 == auto_in_a_bits_source ? 4'hb : _GEN_100; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_102 = 7'h64 == auto_in_a_bits_source ? 4'hb : _GEN_101; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_103 = 7'h65 == auto_in_a_bits_source ? 4'hb : _GEN_102; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_104 = 7'h66 == auto_in_a_bits_source ? 4'hb : _GEN_103; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_105 = 7'h67 == auto_in_a_bits_source ? 4'hb : _GEN_104; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_106 = 7'h68 == auto_in_a_bits_source ? 4'hb : _GEN_105; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_107 = 7'h69 == auto_in_a_bits_source ? 4'h0 : _GEN_106; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_108 = 7'h6a == auto_in_a_bits_source ? 4'h0 : _GEN_107; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_109 = 7'h6b == auto_in_a_bits_source ? 4'h0 : _GEN_108; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_110 = 7'h6c == auto_in_a_bits_source ? 4'h0 : _GEN_109; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_111 = 7'h6d == auto_in_a_bits_source ? 4'h0 : _GEN_110; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_112 = 7'h6e == auto_in_a_bits_source ? 4'h0 : _GEN_111; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_113 = 7'h6f == auto_in_a_bits_source ? 4'h0 : _GEN_112; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] _GEN_114 = 7'h70 == auto_in_a_bits_source ? 4'h3 : _GEN_113; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [3:0] out_arw_bits_id = 7'h71 == auto_in_a_bits_source ? 4'h4 : _GEN_114; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  wire [18:0] _out_arw_bits_len_T_1 = 19'hfff << auto_in_a_bits_size; // @[package.scala 234:77]
  wire [11:0] _out_arw_bits_len_T_3 = ~_out_arw_bits_len_T_1[11:0]; // @[package.scala 234:46]
  wire  prot_1 = ~auto_in_a_bits_user_amba_prot_secure; // @[ToAXI4.scala 187:20]
  wire [1:0] out_arw_bits_prot_hi = {auto_in_a_bits_user_amba_prot_fetch,prot_1}; // @[Cat.scala 30:58]
  wire [1:0] out_arw_bits_cache_lo = {auto_in_a_bits_user_amba_prot_modifiable,auto_in_a_bits_user_amba_prot_bufferable}
    ; // @[Cat.scala 30:58]
  wire [1:0] out_arw_bits_cache_hi = {auto_in_a_bits_user_amba_prot_writealloc,auto_in_a_bits_user_amba_prot_readalloc}; // @[Cat.scala 30:58]
  wire  _out_arw_valid_T_1 = _bundleIn_0_a_ready_T & auto_in_a_valid; // @[ToAXI4.scala 199:31]
  wire  _out_arw_valid_T_4 = a_isPut ? ~doneAW & out_w_ready : 1'h1; // @[ToAXI4.scala 199:51]
  wire  out_arw_valid = _bundleIn_0_a_ready_T & auto_in_a_valid & _out_arw_valid_T_4; // @[ToAXI4.scala 199:45]
  reg  r_holds_d; // @[ToAXI4.scala 208:30]
  wire  _T_2 = auto_in_d_ready & auto_out_r_valid; // @[Decoupled.scala 40:37]
  wire  r_wins = auto_out_r_valid | r_holds_d; // @[ToAXI4.scala 211:32]
  wire  bundleIn_0_d_valid = r_wins ? auto_out_r_valid : auto_out_b_valid; // @[ToAXI4.scala 215:24]
  reg  r_first; // @[ToAXI4.scala 220:28]
  wire  _r_denied_T = auto_out_r_bits_resp == 2'h3; // @[ToAXI4.scala 222:39]
  reg  r_denied_r; // @[Reg.scala 15:16]
  wire  _GEN_232 = r_first ? _r_denied_T : r_denied_r; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire  r_corrupt = auto_out_r_bits_resp != 2'h0; // @[ToAXI4.scala 223:39]
  wire  b_denied = auto_out_b_bits_resp != 2'h0; // @[ToAXI4.scala 224:39]
  wire  r_d_corrupt = r_corrupt | _GEN_232; // @[ToAXI4.scala 226:100]
  wire [2:0] r_d_size = auto_out_r_bits_echo_tl_state_size[2:0]; // @[Edges.scala 771:17 Edges.scala 774:15]
  wire [2:0] b_d_size = auto_out_b_bits_echo_tl_state_size[2:0]; // @[Edges.scala 755:17 Edges.scala 758:15]
  wire [15:0] _a_sel_T = 16'h1 << out_arw_bits_id; // @[OneHot.scala 65:12]
  wire  a_sel_0 = _a_sel_T[0]; // @[ToAXI4.scala 238:58]
  wire  a_sel_1 = _a_sel_T[1]; // @[ToAXI4.scala 238:58]
  wire  a_sel_2 = _a_sel_T[2]; // @[ToAXI4.scala 238:58]
  wire  a_sel_3 = _a_sel_T[3]; // @[ToAXI4.scala 238:58]
  wire  a_sel_4 = _a_sel_T[4]; // @[ToAXI4.scala 238:58]
  wire  a_sel_5 = _a_sel_T[5]; // @[ToAXI4.scala 238:58]
  wire  a_sel_6 = _a_sel_T[6]; // @[ToAXI4.scala 238:58]
  wire  a_sel_7 = _a_sel_T[7]; // @[ToAXI4.scala 238:58]
  wire  a_sel_8 = _a_sel_T[8]; // @[ToAXI4.scala 238:58]
  wire  a_sel_9 = _a_sel_T[9]; // @[ToAXI4.scala 238:58]
  wire  a_sel_10 = _a_sel_T[10]; // @[ToAXI4.scala 238:58]
  wire  a_sel_11 = _a_sel_T[11]; // @[ToAXI4.scala 238:58]
  wire  a_sel_12 = _a_sel_T[12]; // @[ToAXI4.scala 238:58]
  wire  a_sel_13 = _a_sel_T[13]; // @[ToAXI4.scala 238:58]
  wire  a_sel_14 = _a_sel_T[14]; // @[ToAXI4.scala 238:58]
  wire  a_sel_15 = _a_sel_T[15]; // @[ToAXI4.scala 238:58]
  wire [3:0] d_sel_shiftAmount = r_wins ? auto_out_r_bits_id : auto_out_b_bits_id; // @[ToAXI4.scala 239:31]
  wire [15:0] _d_sel_T_1 = 16'h1 << d_sel_shiftAmount; // @[OneHot.scala 65:12]
  wire  d_sel_0 = _d_sel_T_1[0]; // @[ToAXI4.scala 239:93]
  wire  d_sel_1 = _d_sel_T_1[1]; // @[ToAXI4.scala 239:93]
  wire  d_sel_2 = _d_sel_T_1[2]; // @[ToAXI4.scala 239:93]
  wire  d_sel_3 = _d_sel_T_1[3]; // @[ToAXI4.scala 239:93]
  wire  d_sel_4 = _d_sel_T_1[4]; // @[ToAXI4.scala 239:93]
  wire  d_sel_5 = _d_sel_T_1[5]; // @[ToAXI4.scala 239:93]
  wire  d_sel_6 = _d_sel_T_1[6]; // @[ToAXI4.scala 239:93]
  wire  d_sel_7 = _d_sel_T_1[7]; // @[ToAXI4.scala 239:93]
  wire  d_sel_8 = _d_sel_T_1[8]; // @[ToAXI4.scala 239:93]
  wire  d_sel_9 = _d_sel_T_1[9]; // @[ToAXI4.scala 239:93]
  wire  d_sel_10 = _d_sel_T_1[10]; // @[ToAXI4.scala 239:93]
  wire  d_sel_11 = _d_sel_T_1[11]; // @[ToAXI4.scala 239:93]
  wire  d_sel_12 = _d_sel_T_1[12]; // @[ToAXI4.scala 239:93]
  wire  d_sel_13 = _d_sel_T_1[13]; // @[ToAXI4.scala 239:93]
  wire  d_sel_14 = _d_sel_T_1[14]; // @[ToAXI4.scala 239:93]
  wire  d_sel_15 = _d_sel_T_1[15]; // @[ToAXI4.scala 239:93]
  wire  d_last = r_wins ? auto_out_r_bits_last : 1'h1; // @[ToAXI4.scala 240:23]
  wire  _inc_T = out_arw_ready & out_arw_valid; // @[Decoupled.scala 40:37]
  wire  inc = a_sel_0 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  _dec_T_1 = auto_in_d_ready & bundleIn_0_d_valid; // @[Decoupled.scala 40:37]
  wire  dec = d_sel_0 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire  _count_T_2 = count_1 + inc; // @[ToAXI4.scala 256:24]
  wire  inc_1 = a_sel_1 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  dec_1 = d_sel_1 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire  _count_T_6 = count_2 + inc_1; // @[ToAXI4.scala 256:24]
  wire  inc_2 = a_sel_2 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  dec_2 = d_sel_2 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire  _count_T_10 = count_3 + inc_2; // @[ToAXI4.scala 256:24]
  wire  inc_3 = a_sel_3 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  dec_3 = d_sel_3 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire  _count_T_14 = count_4 + inc_3; // @[ToAXI4.scala 256:24]
  wire  inc_4 = a_sel_4 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  dec_4 = d_sel_4 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire  _count_T_18 = count_5 + inc_4; // @[ToAXI4.scala 256:24]
  wire  inc_5 = a_sel_5 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  dec_5 = d_sel_5 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire [3:0] _GEN_233 = {{3'd0}, inc_5}; // @[ToAXI4.scala 256:24]
  wire [3:0] _count_T_22 = count_6 + _GEN_233; // @[ToAXI4.scala 256:24]
  wire [3:0] _GEN_234 = {{3'd0}, dec_5}; // @[ToAXI4.scala 256:37]
  wire  inc_6 = a_sel_6 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  dec_6 = d_sel_6 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire [3:0] _GEN_235 = {{3'd0}, inc_6}; // @[ToAXI4.scala 256:24]
  wire [3:0] _count_T_26 = count_7 + _GEN_235; // @[ToAXI4.scala 256:24]
  wire [3:0] _GEN_236 = {{3'd0}, dec_6}; // @[ToAXI4.scala 256:37]
  wire  inc_7 = a_sel_7 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  dec_7 = d_sel_7 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire [3:0] _GEN_237 = {{3'd0}, inc_7}; // @[ToAXI4.scala 256:24]
  wire [3:0] _count_T_30 = count_8 + _GEN_237; // @[ToAXI4.scala 256:24]
  wire [3:0] _GEN_245 = {{3'd0}, dec_7}; // @[ToAXI4.scala 256:37]
  wire  inc_8 = a_sel_8 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  dec_8 = d_sel_8 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire [3:0] _GEN_246 = {{3'd0}, inc_8}; // @[ToAXI4.scala 256:24]
  wire [3:0] _count_T_34 = count_9 + _GEN_246; // @[ToAXI4.scala 256:24]
  wire [3:0] _GEN_247 = {{3'd0}, dec_8}; // @[ToAXI4.scala 256:37]
  wire  inc_9 = a_sel_9 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  dec_9 = d_sel_9 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire [2:0] _GEN_248 = {{2'd0}, inc_9}; // @[ToAXI4.scala 256:24]
  wire [2:0] _count_T_38 = count_10 + _GEN_248; // @[ToAXI4.scala 256:24]
  wire [2:0] _GEN_249 = {{2'd0}, dec_9}; // @[ToAXI4.scala 256:37]
  wire  inc_10 = a_sel_10 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  dec_10 = d_sel_10 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire [2:0] _GEN_250 = {{2'd0}, inc_10}; // @[ToAXI4.scala 256:24]
  wire [2:0] _count_T_42 = count_11 + _GEN_250; // @[ToAXI4.scala 256:24]
  wire [2:0] _GEN_251 = {{2'd0}, dec_10}; // @[ToAXI4.scala 256:37]
  wire  inc_11 = a_sel_11 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  dec_11 = d_sel_11 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire [2:0] _GEN_252 = {{2'd0}, inc_11}; // @[ToAXI4.scala 256:24]
  wire [2:0] _count_T_46 = count_12 + _GEN_252; // @[ToAXI4.scala 256:24]
  wire [2:0] _GEN_253 = {{2'd0}, dec_11}; // @[ToAXI4.scala 256:37]
  wire  inc_12 = a_sel_12 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  dec_12 = d_sel_12 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire  _count_T_50 = count_13 + inc_12; // @[ToAXI4.scala 256:24]
  wire  inc_13 = a_sel_13 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  dec_13 = d_sel_13 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire  _count_T_54 = count_14 + inc_13; // @[ToAXI4.scala 256:24]
  wire  inc_14 = a_sel_14 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  dec_14 = d_sel_14 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire  _count_T_58 = count_15 + inc_14; // @[ToAXI4.scala 256:24]
  wire  inc_15 = a_sel_15 & _inc_T; // @[ToAXI4.scala 254:22]
  wire  dec_15 = d_sel_15 & d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire  _count_T_62 = count_16 + inc_15; // @[ToAXI4.scala 256:24]
  RHEA__Queue_22 deq ( // @[Decoupled.scala 296:21]
    .rf_reset(deq_rf_reset),
    .clock(deq_clock),
    .reset(deq_reset),
    .io_enq_ready(deq_io_enq_ready),
    .io_enq_valid(deq_io_enq_valid),
    .io_enq_bits_data(deq_io_enq_bits_data),
    .io_enq_bits_strb(deq_io_enq_bits_strb),
    .io_enq_bits_last(deq_io_enq_bits_last),
    .io_deq_ready(deq_io_deq_ready),
    .io_deq_valid(deq_io_deq_valid),
    .io_deq_bits_data(deq_io_deq_bits_data),
    .io_deq_bits_strb(deq_io_deq_bits_strb),
    .io_deq_bits_last(deq_io_deq_bits_last)
  );
  RHEA__Queue_23 queue_arw_deq ( // @[Decoupled.scala 296:21]
    .rf_reset(queue_arw_deq_rf_reset),
    .clock(queue_arw_deq_clock),
    .reset(queue_arw_deq_reset),
    .io_enq_ready(queue_arw_deq_io_enq_ready),
    .io_enq_valid(queue_arw_deq_io_enq_valid),
    .io_enq_bits_id(queue_arw_deq_io_enq_bits_id),
    .io_enq_bits_addr(queue_arw_deq_io_enq_bits_addr),
    .io_enq_bits_len(queue_arw_deq_io_enq_bits_len),
    .io_enq_bits_size(queue_arw_deq_io_enq_bits_size),
    .io_enq_bits_cache(queue_arw_deq_io_enq_bits_cache),
    .io_enq_bits_prot(queue_arw_deq_io_enq_bits_prot),
    .io_enq_bits_echo_tl_state_size(queue_arw_deq_io_enq_bits_echo_tl_state_size),
    .io_enq_bits_echo_tl_state_source(queue_arw_deq_io_enq_bits_echo_tl_state_source),
    .io_enq_bits_wen(queue_arw_deq_io_enq_bits_wen),
    .io_deq_ready(queue_arw_deq_io_deq_ready),
    .io_deq_valid(queue_arw_deq_io_deq_valid),
    .io_deq_bits_id(queue_arw_deq_io_deq_bits_id),
    .io_deq_bits_addr(queue_arw_deq_io_deq_bits_addr),
    .io_deq_bits_len(queue_arw_deq_io_deq_bits_len),
    .io_deq_bits_size(queue_arw_deq_io_deq_bits_size),
    .io_deq_bits_cache(queue_arw_deq_io_deq_bits_cache),
    .io_deq_bits_prot(queue_arw_deq_io_deq_bits_prot),
    .io_deq_bits_echo_tl_state_size(queue_arw_deq_io_deq_bits_echo_tl_state_size),
    .io_deq_bits_echo_tl_state_source(queue_arw_deq_io_deq_bits_echo_tl_state_source),
    .io_deq_bits_wen(queue_arw_deq_io_deq_bits_wen)
  );
  assign deq_rf_reset = rf_reset;
  assign queue_arw_deq_rf_reset = rf_reset;
  assign auto_in_a_ready = _bundleIn_0_a_ready_T & _bundleIn_0_a_ready_T_3; // @[ToAXI4.scala 198:28]
  assign auto_in_d_valid = r_wins ? auto_out_r_valid : auto_out_b_valid; // @[ToAXI4.scala 215:24]
  assign auto_in_d_bits_opcode = r_wins ? 3'h1 : 3'h0; // @[ToAXI4.scala 233:23]
  assign auto_in_d_bits_size = r_wins ? r_d_size : b_d_size; // @[ToAXI4.scala 233:23]
  assign auto_in_d_bits_source = r_wins ? auto_out_r_bits_echo_tl_state_source : auto_out_b_bits_echo_tl_state_source; // @[ToAXI4.scala 233:23]
  assign auto_in_d_bits_denied = r_wins ? _GEN_232 : b_denied; // @[ToAXI4.scala 233:23]
  assign auto_in_d_bits_data = auto_out_r_bits_data; // @[ToAXI4.scala 64:86 LazyModule.scala 390:12]
  assign auto_in_d_bits_corrupt = r_wins & r_d_corrupt; // @[ToAXI4.scala 233:23]
  assign auto_out_aw_valid = queue_arw_valid & queue_arw_bits_wen; // @[ToAXI4.scala 158:39]
  assign auto_out_aw_bits_id = queue_arw_deq_io_deq_bits_id; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_addr = queue_arw_deq_io_deq_bits_addr; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_len = queue_arw_deq_io_deq_bits_len; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_size = queue_arw_deq_io_deq_bits_size; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_cache = queue_arw_deq_io_deq_bits_cache; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_prot = queue_arw_deq_io_deq_bits_prot; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_echo_tl_state_size = queue_arw_deq_io_deq_bits_echo_tl_state_size; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_echo_tl_state_source = queue_arw_deq_io_deq_bits_echo_tl_state_source; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_w_valid = deq_io_deq_valid; // @[Decoupled.scala 317:19 Decoupled.scala 319:15]
  assign auto_out_w_bits_data = deq_io_deq_bits_data; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_w_bits_strb = deq_io_deq_bits_strb; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_w_bits_last = deq_io_deq_bits_last; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_b_ready = auto_in_d_ready & ~r_wins; // @[ToAXI4.scala 214:33]
  assign auto_out_ar_valid = queue_arw_valid & ~queue_arw_bits_wen; // @[ToAXI4.scala 157:39]
  assign auto_out_ar_bits_id = queue_arw_deq_io_deq_bits_id; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_addr = queue_arw_deq_io_deq_bits_addr; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_len = queue_arw_deq_io_deq_bits_len; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_size = queue_arw_deq_io_deq_bits_size; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_cache = queue_arw_deq_io_deq_bits_cache; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_prot = queue_arw_deq_io_deq_bits_prot; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_echo_tl_state_size = queue_arw_deq_io_deq_bits_echo_tl_state_size; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_echo_tl_state_source = queue_arw_deq_io_deq_bits_echo_tl_state_source; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_r_ready = auto_in_d_ready; // @[ToAXI4.scala 64:86 LazyModule.scala 388:16]
  assign deq_clock = clock;
  assign deq_reset = reset;
  assign deq_io_enq_valid = _out_arw_valid_T_1 & a_isPut & _bundleIn_0_a_ready_T_1; // @[ToAXI4.scala 201:54]
  assign deq_io_enq_bits_data = auto_in_a_bits_data; // @[ToAXI4.scala 64:86 LazyModule.scala 388:16]
  assign deq_io_enq_bits_strb = auto_in_a_bits_mask; // @[ToAXI4.scala 64:86 LazyModule.scala 388:16]
  assign deq_io_enq_bits_last = counter == 3'h1 | beats1 == 3'h0; // @[Edges.scala 231:37]
  assign deq_io_deq_ready = auto_out_w_ready; // @[ToAXI4.scala 64:86 LazyModule.scala 390:12]
  assign queue_arw_deq_clock = clock;
  assign queue_arw_deq_reset = reset;
  assign queue_arw_deq_io_enq_valid = _bundleIn_0_a_ready_T & auto_in_a_valid & _out_arw_valid_T_4; // @[ToAXI4.scala 199:45]
  assign queue_arw_deq_io_enq_bits_id = 7'h71 == auto_in_a_bits_source ? 4'h4 : _GEN_114; // @[ToAXI4.scala 168:17 ToAXI4.scala 168:17]
  assign queue_arw_deq_io_enq_bits_addr = auto_in_a_bits_address; // @[ToAXI4.scala 64:86 LazyModule.scala 388:16]
  assign queue_arw_deq_io_enq_bits_len = _out_arw_bits_len_T_3[11:4]; // @[ToAXI4.scala 170:84]
  assign queue_arw_deq_io_enq_bits_size = auto_in_a_bits_size >= 3'h4 ? 3'h4 : auto_in_a_bits_size; // @[ToAXI4.scala 171:23]
  assign queue_arw_deq_io_enq_bits_cache = {out_arw_bits_cache_hi,out_arw_bits_cache_lo}; // @[Cat.scala 30:58]
  assign queue_arw_deq_io_enq_bits_prot = {out_arw_bits_prot_hi,auto_in_a_bits_user_amba_prot_privileged}; // @[Cat.scala 30:58]
  assign queue_arw_deq_io_enq_bits_echo_tl_state_size = {{1'd0}, auto_in_a_bits_size}; // @[ToAXI4.scala 149:25 ToAXI4.scala 181:22]
  assign queue_arw_deq_io_enq_bits_echo_tl_state_source = auto_in_a_bits_source; // @[ToAXI4.scala 64:86 LazyModule.scala 388:16]
  assign queue_arw_deq_io_enq_bits_wen = ~auto_in_a_bits_opcode[2]; // @[Edges.scala 91:28]
  assign queue_arw_deq_io_deq_ready = queue_arw_bits_wen ? auto_out_aw_ready : auto_out_ar_ready; // @[ToAXI4.scala 159:29]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_5 <= 1'h0;
    end else begin
      count_5 <= _count_T_18 - dec_4;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_4 <= 1'h0;
    end else begin
      count_4 <= _count_T_14 - dec_3;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_12 <= 3'h0;
    end else begin
      count_12 <= _count_T_46 - _GEN_253;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      write_11 <= 1'h0;
    end else if (inc_11) begin
      write_11 <= a_isPut;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_16 <= 1'h0;
    end else begin
      count_16 <= _count_T_62 - dec_15;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_15 <= 1'h0;
    end else begin
      count_15 <= _count_T_58 - dec_14;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_3 <= 1'h0;
    end else begin
      count_3 <= _count_T_10 - dec_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_2 <= 1'h0;
    end else begin
      count_2 <= _count_T_6 - dec_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_11 <= 3'h0;
    end else begin
      count_11 <= _count_T_42 - _GEN_251;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      write_10 <= 1'h0;
    end else if (inc_10) begin
      write_10 <= a_isPut;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_14 <= 1'h0;
    end else begin
      count_14 <= _count_T_54 - dec_13;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_13 <= 1'h0;
    end else begin
      count_13 <= _count_T_50 - dec_12;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_1 <= 1'h0;
    end else begin
      count_1 <= _count_T_2 - dec;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_10 <= 3'h0;
    end else begin
      count_10 <= _count_T_38 - _GEN_249;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      write_9 <= 1'h0;
    end else if (inc_9) begin
      write_9 <= a_isPut;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_9 <= 4'h0;
    end else begin
      count_9 <= _count_T_34 - _GEN_247;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      write_8 <= 1'h0;
    end else if (inc_8) begin
      write_8 <= a_isPut;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_8 <= 4'h0;
    end else begin
      count_8 <= _count_T_30 - _GEN_245;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      write_7 <= 1'h0;
    end else if (inc_7) begin
      write_7 <= a_isPut;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_7 <= 4'h0;
    end else begin
      count_7 <= _count_T_26 - _GEN_236;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      write_6 <= 1'h0;
    end else if (inc_6) begin
      write_6 <= a_isPut;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_6 <= 4'h0;
    end else begin
      count_6 <= _count_T_22 - _GEN_234;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      write_5 <= 1'h0;
    end else if (inc_5) begin
      write_5 <= a_isPut;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      counter <= 3'h0;
    end else if (_T) begin
      if (a_first) begin
        if (a_isPut) begin
          counter <= beats1_decode;
        end else begin
          counter <= 3'h0;
        end
      end else begin
        counter <= counter1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      doneAW <= 1'h0;
    end else if (_T) begin
      doneAW <= ~a_last;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r_holds_d <= 1'h0;
    end else if (_T_2) begin
      r_holds_d <= ~auto_out_r_bits_last;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r_first <= 1'h1;
    end else if (_T_2) begin
      r_first <= auto_out_r_bits_last;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r_denied_r <= 1'h0;
    end else if (r_first) begin
      r_denied_r <= _r_denied_T;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  count_5 = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  count_4 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  count_12 = _RAND_2[2:0];
  _RAND_3 = {1{`RANDOM}};
  write_11 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  count_16 = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  count_15 = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  count_3 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  count_2 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  count_11 = _RAND_8[2:0];
  _RAND_9 = {1{`RANDOM}};
  write_10 = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  count_14 = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  count_13 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  count_1 = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  count_10 = _RAND_13[2:0];
  _RAND_14 = {1{`RANDOM}};
  write_9 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  count_9 = _RAND_15[3:0];
  _RAND_16 = {1{`RANDOM}};
  write_8 = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  count_8 = _RAND_17[3:0];
  _RAND_18 = {1{`RANDOM}};
  write_7 = _RAND_18[0:0];
  _RAND_19 = {1{`RANDOM}};
  count_7 = _RAND_19[3:0];
  _RAND_20 = {1{`RANDOM}};
  write_6 = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  count_6 = _RAND_21[3:0];
  _RAND_22 = {1{`RANDOM}};
  write_5 = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  counter = _RAND_23[2:0];
  _RAND_24 = {1{`RANDOM}};
  doneAW = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  r_holds_d = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  r_first = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  r_denied_r = _RAND_27[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    count_5 = 1'h0;
  end
  if (reset) begin
    count_4 = 1'h0;
  end
  if (reset) begin
    count_12 = 3'h0;
  end
  if (rf_reset) begin
    write_11 = 1'h0;
  end
  if (reset) begin
    count_16 = 1'h0;
  end
  if (reset) begin
    count_15 = 1'h0;
  end
  if (reset) begin
    count_3 = 1'h0;
  end
  if (reset) begin
    count_2 = 1'h0;
  end
  if (reset) begin
    count_11 = 3'h0;
  end
  if (rf_reset) begin
    write_10 = 1'h0;
  end
  if (reset) begin
    count_14 = 1'h0;
  end
  if (reset) begin
    count_13 = 1'h0;
  end
  if (reset) begin
    count_1 = 1'h0;
  end
  if (reset) begin
    count_10 = 3'h0;
  end
  if (rf_reset) begin
    write_9 = 1'h0;
  end
  if (reset) begin
    count_9 = 4'h0;
  end
  if (rf_reset) begin
    write_8 = 1'h0;
  end
  if (reset) begin
    count_8 = 4'h0;
  end
  if (rf_reset) begin
    write_7 = 1'h0;
  end
  if (reset) begin
    count_7 = 4'h0;
  end
  if (rf_reset) begin
    write_6 = 1'h0;
  end
  if (reset) begin
    count_6 = 4'h0;
  end
  if (rf_reset) begin
    write_5 = 1'h0;
  end
  if (reset) begin
    counter = 3'h0;
  end
  if (reset) begin
    doneAW = 1'h0;
  end
  if (reset) begin
    r_holds_d = 1'h0;
  end
  if (reset) begin
    r_first = 1'h1;
  end
  if (rf_reset) begin
    r_denied_r = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
