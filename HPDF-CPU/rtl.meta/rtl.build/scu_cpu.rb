
#-----------------------------------------------------------------------------------
DESIGN_NAME = "scu_cpu"
OUTPUT_PATH = "../../rtl.src/scu_cpu"
#-----------------------------------------------------------------------------------
INSTANTIATIONS = {
    'SCR_CPU        ' => ['scr_cpu_wrap                           ', "../../rtl.src/scu_cpu/scr_cpu/scr_cpu_wrap.v" ],
    'PLL_CPU        ' => ['pll1426xe_wrap                         ', "../../../Shared/rtl.src/pll1426xe_wrap/pll1426xe_wrap.v"],
    #Clock Domain : CPUCORE
    'GLCM_CPUCORE   ' => ['SemiGLCM                               ', "../../../../block-widgets-semifive/src/main/resources/vsrc/SemiGLCM.sv"],
    'CLKDIV_CPUCORE ' => ['ClockDivider                           ', "../../../../block-widgets-semifive/src/main/resources/vsrc/ClockDivider.sv",
        {'LOG2N' => 8}],
    'CRG_CPUCORE    ' => ['SemiCRG                                ', "../../../../block-widgets-semifive/src/main/resources/vsrc/SemiCRG.sv",
        {'LOG2N' => 8, 'NUM_RESET' => 3, 'NUM_CLK' => 3}],
    'CLKDIV_BUS     ' => ['ClockDivider                           ', "../../../../block-widgets-semifive/src/main/resources/vsrc/ClockDivider.sv",
        {'LOG2N' => 8}],
    'CRG_BUS        ' => ['SemiCRG                                ', "../../../../block-widgets-semifive/src/main/resources/vsrc/SemiCRG.sv",
        {'LOG2N' => 8, 'NUM_RESET' => 7, 'NUM_CLK' => 7}],
    'CLKDIV_MEM     ' => ['ClockDivider                           ', "../../../../block-widgets-semifive/src/main/resources/vsrc/ClockDivider.sv",
        {'LOG2N' => 8}],
    'CRG_MEM        ' => ['SemiCRG                                ', "../../../../block-widgets-semifive/src/main/resources/vsrc/SemiCRG.sv",
        {'LOG2N' => 8, 'NUM_RESET' => 2, 'NUM_CLK' => 2}],
    #Clock Domain : DEBUG
    'GLCM_DEBUG         ' => ['SemiGLCM                               ', "../../../../block-widgets-semifive/src/main/resources/vsrc/SemiGLCM.sv"],
    'CLKDIV_DEBUG       ' => ['ClockDivider                           ', "../../../../block-widgets-semifive/src/main/resources/vsrc/ClockDivider.sv",
        {'LOG2N' => 8}],
    'CRG_DEBUG          ' => ['SemiCRG                                ', "../../../../block-widgets-semifive/src/main/resources/vsrc/SemiCRG.sv",
        {'LOG2N' => 8, 'NUM_RESET' => 4, 'NUM_CLK' => 4}],
    #Clock Domain : TS
    'GLCM_TS            ' => ['SemiGLCM                               ', "../../../../block-widgets-semifive/src/main/resources/vsrc/SemiGLCM.sv"],
    'CLKDIV_TS          ' => ['ClockDivider                           ', "../../../../block-widgets-semifive/src/main/resources/vsrc/ClockDivider.sv",
        {'LOG2N' => 8}],
    'CRG_TS             ' => ['SemiCRG                                ', "../../../../block-widgets-semifive/src/main/resources/vsrc/SemiCRG.sv",
        {'LOG2N' => 8, 'NUM_RESET' => 1, 'NUM_CLK' => 1}],
}
#-----------------------------------------------------------------------------------
IO_INSTANTIATIONS = [
    'input  CLK__OSC',
    'input  CLK__SUBSYSTEM',
    'input  RSTN__SUBSYSTEM',
    'output CLK__CPU',
    'output RSTN__CPU',
    'output CLK__CPUCORE0',
    'output RSTN__CPUCORE0',
    'output CLK__CPUCORE1',
    'output RSTN__CPUCORE1',
    'output CLK__CBUS',
    'output RSTN__CBUS',
    'output CLK__DBUS',
    'output RSTN__DBUS',
    'output CLK__FBUS',
    'output RSTN__FBUS',
    'output CLK__ROM',
    'output RSTN__ROM',
    'output CLK__SRAM',
    'output RSTN__SRAM',
    'output CLK__DEBUG',
    'output RSTN__DEBUG',
    'output CLK__TRACE_COM',
    'output RSTN__TRACE_COM',
    'output CLK__TRACE_0',
    'output RSTN__TRACE_0',
    'output CLK__TRACE_1',
    'output RSTN__TRACE_1',
    'output CLK__TS',
    'output RSTN__TS',
    'output CLK__FBUS_RX',
    'output CLK__DBUS_TX',
    'output CLK__CBUS_TX',
    'output RSTN__FBUS_RX',
    'output RSTN__DBUS_TX',
    'output RSTN__CBUS_TX',
	'input  [31:0] APB3__CBUS__PADDR',
    'input  DFT__SCAN_CLK',
]
#-----------------------------------------------------------------------------------
CONNECTIONS = [
    # Clock & Reset
    ['<top>             ', 'CLK__OSC                ', 'PLL_CPU         ', 'CLK__FIN'],
    ['GLCM_.*           ', 'CLK__IN0                ', '<top>           ', 'CLK__SUBSYSTEM'],
    ['GLCM_.*           ', 'RSTN                    ', '<top>           ', 'RSTN__SUBSYSTEM'],
    ['CRG_.*            ', 'RSTN__SCU               ', '<top>           ', 'RSTN__SUBSYSTEM'],
    ['GLCM_.*           ', 'CLK__IN1                ', 'PLL_CPU         ', 'CLK__FOUT'],
    ['GLCM_CPUCORE      ', 'CLK__OUT                ', 'CLKDIV_CPUCORE  ', 'CLK__IN'],
    ['CLKDIV_CPUCORE    ', 'CLK__OUT                ', 'CRG_CPUCORE     ', 'CLK__IN'],
    ['CRG_CPUCORE       ', 'CLK__OUT\[0\]           ', 'CLKDIV_BUS      ', 'CLK__IN'],
    ['CRG_CPUCORE       ', 'CLK__OUT\[0\]           ', 'CLKDIV_MEM      ', 'CLK__IN'],
    ['CRG_CPUCORE       ', '(CLK|RSTN)__OUT\[1\]    ', '<top>           ', '\1__CPUCORE0'],
    ['CRG_CPUCORE       ', '(CLK|RSTN)__OUT\[2\]    ', '<top>           ', '\1__CPUCORE1'],
    ['CLKDIV_BUS        ', 'CLK__OUT                ', 'CRG_BUS         ', 'CLK__IN'],
    ['CRG_BUS           ', 'CLK__OUT\[0\]           ', 'SCR_CPU         ', 'PCLK'],
    ['CRG_BUS           ', 'RSTN__OUT\[0\]          ', 'SCR_CPU         ', 'PRESETn'],
    ['CRG_BUS           ', '(CLK|RSTN)__OUT\[0\]    ', '<top>           ', '\1__CBUS'],
    ['CLKDIV_.*         ', '(CLK|RSTN)__SFR         ', 'CRG_BUS         ', '\1__OUT[0]'],
    ['CRG_.*            ', '(CLK|RSTN)__SFR         ', 'CRG_BUS         ', '\1__OUT[0]'],
    ['CRG_BUS           ', '(CLK|RSTN)__OUT\[1\]    ', '<top>           ', '\1__CPU'],
    ['CRG_BUS           ', '(CLK|RSTN)__OUT\[2\]    ', '<top>           ', '\1__CBUS_TX'],
    ['CRG_BUS           ', '(CLK|RSTN)__OUT\[3\]    ', '<top>           ', '\1__DBUS'],
    ['CRG_BUS           ', '(CLK|RSTN)__OUT\[4\]    ', '<top>           ', '\1__DBUS_TX'],
    ['CRG_BUS           ', '(CLK|RSTN)__OUT\[5\]    ', '<top>           ', '\1__FBUS'],
    ['CRG_BUS           ', '(CLK|RSTN)__OUT\[6\]    ', '<top>           ', '\1__FBUS_RX'],
    ['CLKDIV_MEM        ', 'CLK__OUT                ', 'CRG_MEM         ', 'CLK__IN'],
    ['CRG_MEM           ', '(CLK|RSTN)__OUT\[0\]    ', '<top>           ', '\1__ROM'],
    ['CRG_MEM           ', '(CLK|RSTN)__OUT\[1\]    ', '<top>           ', '\1__SRAM'],
    ['GLCM_DEBUG        ', 'CLK__OUT                ', 'CLKDIV_DEBUG    ', 'CLK__IN'],
    ['CLKDIV_DEBUG      ', 'CLK__OUT                ', 'CRG_DEBUG       ', 'CLK__IN'],
    ['CRG_DEBUG         ', '(CLK|RSTN)__OUT\[0\]    ', '<top>           ', '\1__DEBUG'],
    ['CRG_DEBUG         ', '(CLK|RSTN)__OUT\[1\]    ', '<top>           ', '\1__TRACE_COM'],
    ['CRG_DEBUG         ', '(CLK|RSTN)__OUT\[2\]    ', '<top>           ', '\1__TRACE_0'],
    ['CRG_DEBUG         ', '(CLK|RSTN)__OUT\[3\]    ', '<top>           ', '\1__TRACE_1'],
    ['GLCM_TS           ', 'CLK__OUT                ', 'CLKDIV_TS       ', 'CLK__IN'],
    ['CLKDIV_TS         ', 'CLK__OUT                ', 'CRG_TS          ', 'CLK__IN'],
    ['CRG_TS            ', '(CLK|RSTN)__OUT\[0\]    ', '<top>           ', '\1__TS'],
    #SCR
    ['SCR_CPU           ', 'SCR__PLL_CPU_(.*)       ', 'PLL_CPU         ', 'REG__\1'],
    ['SCR_CPU           ', 'SCR__CLKSEL0            ', 'GLCM_CPUCORE    ', 'CLKSEL'],
    ['SCR_CPU           ', 'SCR__CLKSEL1            ', 'GLCM_DEBUG      ', 'CLKSEL'],
    ['SCR_CPU           ', 'SCR__CLKSEL2            ', 'GLCM_TS         ', 'CLKSEL'],
    ['SCR_CPU           ', 'SCR__CLKDIV0            ', 'CLKDIV_CPUCORE  ', 'SFR__DIV'],
    ['SCR_CPU           ', 'SCR__CLKDIV1            ', 'CLKDIV_DEBUG    ', 'SFR__DIV'],
    ['SCR_CPU           ', 'SCR__CLKDIV2            ', 'CLKDIV_TS       ', 'SFR__DIV'],
    ['SCR_CPU           ', 'SCR__CLKDIS0\[2:0\]     ', 'CRG_CPUCORE     ', 'SFR__SCU__CLKDIS'],
    ['SCR_CPU           ', 'SCR__RSTREQN0\[2:0\]    ', 'CRG_CPUCORE     ', 'SFR__SCU__RSTREQN'],
    ['SCR_CPU           ', 'SCR__RSTCYCLE0          ', 'CRG_CPUCORE     ', 'SFR__SCU__RSTCYCLE'],
    #['SCR_CPU           ', 'SCR__CLKDIS0\[9:3\]     ', 'CRG_BUS         ', 'SFR__SCU__CLKDIS'],
    ['SCR_CPU           ', 'SCR__CLKDIS0\[9:4\]     ', 'CRG_BUS         ', 'SFR__SCU__CLKDIS\[6:1\]'],
    ['SCR_CPU           ', 'SCR__RSTREQN0\[9:3\]    ', 'CRG_BUS         ', 'SFR__SCU__RSTREQN'],
    ['SCR_CPU           ', 'SCR__RSTCYCLE1          ', 'CRG_BUS         ', 'SFR__SCU__RSTCYCLE'],
    ['SCR_CPU           ', 'SCR__CLKDIS0\[11:10\]   ', 'CRG_MEM         ', 'SFR__SCU__CLKDIS'],
    ['SCR_CPU           ', 'SCR__RSTREQN0\[11:10\]  ', 'CRG_MEM         ', 'SFR__SCU__RSTREQN'],
    ['SCR_CPU           ', 'SCR__RSTCYCLE2          ', 'CRG_MEM         ', 'SFR__SCU__RSTCYCLE'],
    ['SCR_CPU           ', 'SCR__CLKDIS0\[15:12\]   ', 'CRG_DEBUG       ', 'SFR__SCU__CLKDIS'],
    ['SCR_CPU           ', 'SCR__RSTREQN0\[15:12\]  ', 'CRG_DEBUG       ', 'SFR__SCU__RSTREQN'],
    ['SCR_CPU           ', 'SCR__RSTCYCLE3          ', 'CRG_DEBUG       ', 'SFR__SCU__RSTCYCLE'],
    ['SCR_CPU           ', 'SCR__CLKDIS0\[16\]      ', 'CRG_TS          ', 'SFR__SCU__CLKDIS'],
    ['SCR_CPU           ', 'SCR__RSTREQN0\[16\]     ', 'CRG_TS          ', 'SFR__SCU__RSTREQN'],
    ['SCR_CPU           ', 'SCR__RSTCYCLE4          ', 'CRG_TS          ', 'SFR__SCU__RSTCYCLE'],
    #SFR
    ['SCR_CPU           ', 'SFR__(.*)               ', '<top>           ', 'SFR__\1'],
    #DFT
    ['.*                ', 'DFT__SCAN_(:not(DIV))   ', '<top>           ', 'DFT__SCAN_\1'],
    ['.*                ', 'DFT__BIST_MODE          ', '<top>           ', 'DFT__BIST_MODE'],
    ['PLL_CPU           ', 'TEST__PLL(.*)           ', '<top>           ', 'DFT__PLL_CPU\1'],
    ['PLL_CPU           ', 'TEST__SCAN              ', '<top>           ', 'DFT__SCAN_MODE'],
    #APB
    ['SCR_CPU           ', 'P(:not(CLK|RESETn|STRB|PROT|ADDR))', '<top>           ', 'APB3__CBUS__P\1'],
    ['<top>             ', 'APB3__CBUS__PADDR\[11:0\]', 'SCR_CPU        ', 'PADDR'],
    #RSTACK
    ['SCR_CPU           ', 'RSTACK\[0\]             ', 'CRG_CPUCORE     ', 'RSTACK__IN'],
    ['SCR_CPU           ', 'RSTACK\[1\]             ', 'CRG_BUS         ', 'RSTACK__IN'],
    ['SCR_CPU           ', 'RSTACK\[2\]             ', 'CRG_MEM         ', 'RSTACK__IN'],
    ['SCR_CPU           ', 'RSTACK\[3\]             ', 'CRG_DEBUG       ', 'RSTACK__IN'],
    ['SCR_CPU           ', 'RSTACK\[4\]             ', 'CRG_TS          ', 'RSTACK__IN'],
]
#-----------------------------------------------------------------------------------
OPENED_OR_TIED = [
    ['CRG_BUS           ', 'SFR__SCU__CLKDIS\[0\]', '0'],
    ['CLKDIV_.*         ', 'DFT__SCAN_DIV', '8\'h0'],
    ['.*                ', 'DFT__BIST_CLK', '0'],
    ['CLKDIV_BUS        ', 'SFR__DIV', '8\'h2'],
    ['CLKDIV_MEM        ', 'SFR__DIV', '8\'h4'],
    ['PLL_CPU           ', 'REG__FEED_OUT', '0'],
    ['SCR_CPU           ', 'PSTRB        ', '4\'hf'],
    ['SCR_CPU           ', 'PPROT', '3\'h0'],
    ['SCR_CPU           ', 'SCR__VERSION.', '32\'h0'],
    ['<top>             ', 'DFT__SCAN_CLK', '0'],
    ['<top>             ', 'APB3__CBUS__PADDR\[31:12\]' ,'0'],
]
#-----------------------------------------------------------------------------------
CONFIRMED_UNCONNECTED = [
#    ['<top>', 'GPIOA_.*       ', '0'],
    ['CRG_CPUCORE', 'RSTN__OUT\[0\]', '0'],
]
