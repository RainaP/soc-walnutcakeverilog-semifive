module axi_x2p_32_32_1 (
    input   CLK,
    input   RSTN,

    input   [31:0]  AXI4__AWADDR,
    input   [0:0]   AXI4__AWID,
    input   [7:0]   AXI4__AWLEN,
    input   [2:0]   AXI4__AWSIZE,
    input   [1:0]   AXI4__AWBURST,
    input   [0:0]   AXI4__AWLOCK,
    input   [3:0]   AXI4__AWCACHE,
    input   [2:0]   AXI4__AWPROT,
    input           AXI4__AWVALID,
    output          AXI4__AWREADY,
    input   [31:0]  AXI4__WDATA,
    input   [3:0]   AXI4__WSTRB,
    input           AXI4__WVALID,
    input           AXI4__WLAST,
    output          AXI4__WREADY,
    output  [0:0]   AXI4__BID,
    output          AXI4__BVALID,
    output  [1:0]   AXI4__BRESP,
    input           AXI4__BREADY,
    input   [0:0]   AXI4__ARID,
    input   [31:0]  AXI4__ARADDR,
    input   [7:0]   AXI4__ARLEN,
    input   [2:0]   AXI4__ARSIZE,
    input   [1:0]   AXI4__ARBURST,
    input   [0:0]   AXI4__ARLOCK,
    input   [3:0]   AXI4__ARCACHE,
    input   [2:0]   AXI4__ARPROT,
    input           AXI4__ARVALID,
    output          AXI4__ARREADY,
    output  [0:0]   AXI4__RID,
    output  [31:0]  AXI4__RDATA,
    output  [1:0]   AXI4__RRESP,
    output          AXI4__RLAST,
    output          AXI4__RVALID,
    input           AXI4__RREADY,

    output [31:0]   APB3__PADDR,
    output          APB3__PENABLE,
    output          APB3__PSEL,
    output [31:0]	APB3__PWDATA,
    output          APB3__PWRITE,
    input  [31:0]   APB3__PRDATA,
    input           APB3__PREADY,
    input           APB3__PSLVERR

);
 `include "axi_x2p_32_32_1_DW_axi_x2p_all_includes.vh"

 /*AUTOWIRE*/
 /*AUTOREG*/

    /* axi_x2p_32_32_1_DW_axi_x2p AUTO_TEMPLATE (
        .aclk           (CLK),
        .aresetn        (RSTN),
        .p\(.*\)_s0     (APB3__P@"(upcase \\"\1\\")"[]),
        .p\(.*\)        (APB3__P@"(upcase \\"\1\\")"[]),
        .\(.*\)         (AXI4__@"(upcase \\"\1\\")"[]),
    );*/
    axi_x2p_32_32_1_DW_axi_x2p AXI2APB (/*AUTOINST*/
					// Outputs
					.arready	(AXI4__ARREADY), // Templated
					.awready	(AXI4__AWREADY), // Templated
					.bid		(AXI4__BID[`axi_x2p_32_32_1_X2P_AXI_SIDW-1:0]), // Templated
					.bresp		(AXI4__BRESP[1:0]), // Templated
					.bvalid		(AXI4__BVALID),	 // Templated
					.rdata		(AXI4__RDATA[`axi_x2p_32_32_1_X2P_AXI_DW-1:0]), // Templated
					.rid		(AXI4__RID[`axi_x2p_32_32_1_X2P_AXI_SIDW-1:0]), // Templated
					.rlast		(AXI4__RLAST),	 // Templated
					.rresp		(AXI4__RRESP[1:0]), // Templated
					.rvalid		(AXI4__RVALID),	 // Templated
					.wready		(AXI4__WREADY),	 // Templated
					.paddr		(APB3__PADDR[`axi_x2p_32_32_1_X2P_APB_ADDR_WIDTH-1:0]), // Templated
					.penable	(APB3__PENABLE), // Templated
					.psel_s0	(APB3__PSEL),	 // Templated
					.pwdata		(APB3__PWDATA[`axi_x2p_32_32_1_X2P_APB_DATA_WIDTH-1:0]), // Templated
					.pwrite		(APB3__PWRITE),	 // Templated
					// Inputs
					.aclk		(CLK),		 // Templated
					.aresetn	(RSTN),		 // Templated
					.araddr		(AXI4__ARADDR[`axi_x2p_32_32_1_X2P_AXI_AW-1:0]), // Templated
					.awaddr		(AXI4__AWADDR[`axi_x2p_32_32_1_X2P_AXI_AW-1:0]), // Templated
					.arburst	(AXI4__ARBURST[1:0]), // Templated
					.arid		(AXI4__ARID[`axi_x2p_32_32_1_X2P_AXI_SIDW-1:0]), // Templated
					.arlen		(AXI4__ARLEN[`axi_x2p_32_32_1_LEN_WIDTH-1:0]), // Templated
					.arsize		(AXI4__ARSIZE[2:0]), // Templated
					.arvalid	(AXI4__ARVALID), // Templated
					.awburst	(AXI4__AWBURST[1:0]), // Templated
					.awid		(AXI4__AWID[`axi_x2p_32_32_1_X2P_AXI_SIDW-1:0]), // Templated
					.awlen		(AXI4__AWLEN[`axi_x2p_32_32_1_LEN_WIDTH-1:0]), // Templated
					.arcache	(AXI4__ARCACHE[3:0]), // Templated
					.arprot		(AXI4__ARPROT[2:0]), // Templated
					.arlock		(AXI4__ARLOCK[`axi_x2p_32_32_1_X2P_AXI_LTW-1:0]), // Templated
					.awcache	(AXI4__AWCACHE[3:0]), // Templated
					.awprot		(AXI4__AWPROT[2:0]), // Templated
					.awlock		(AXI4__AWLOCK[`axi_x2p_32_32_1_X2P_AXI_LTW-1:0]), // Templated
					.awsize		(AXI4__AWSIZE[2:0]), // Templated
					.awvalid	(AXI4__AWVALID), // Templated
					.bready		(AXI4__BREADY),	 // Templated
					.rready		(AXI4__RREADY),	 // Templated
					.wdata		(AXI4__WDATA[`axi_x2p_32_32_1_X2P_AXI_DW-1:0]), // Templated
					.wlast		(AXI4__WLAST),	 // Templated
					.wstrb		(AXI4__WSTRB[`axi_x2p_32_32_1_X2P_AXI_WSTRB_WIDTH-1:0]), // Templated
					.wvalid		(AXI4__WVALID),	 // Templated
					.prdata_s0	(APB3__PRDATA[`axi_x2p_32_32_1_X2P_APB_DATA_WIDTH-1:0]), // Templated
					.pready_s0	(APB3__PREADY),	 // Templated
					.pslverr_s0	(APB3__PSLVERR)); // Templated
 `include "axi_x2p_32_32_1_DW_axi_x2p-undef.v"


endmodule
// Local Variables:
// verilog-library-directories:(".")
// verilog-auto-inst-param-value:t
// verilog-library-files:("axi_x2p_32_32_1_DW_axi_x2p.v")
// End:
// vim:tabstop=4:shiftwidth=4
