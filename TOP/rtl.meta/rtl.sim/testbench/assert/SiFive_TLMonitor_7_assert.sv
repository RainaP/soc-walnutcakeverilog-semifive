//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_7_assert(
  input         clock,
  input         reset,
  input         io_in_a_ready,
  input         io_in_a_valid,
  input  [2:0]  io_in_a_bits_opcode,
  input  [2:0]  io_in_a_bits_param,
  input  [2:0]  io_in_a_bits_size,
  input  [6:0]  io_in_a_bits_source,
  input  [34:0] io_in_a_bits_address,
  input  [7:0]  io_in_a_bits_mask,
  input         io_in_a_bits_corrupt,
  input         io_in_d_ready,
  input         io_in_d_valid,
  input  [2:0]  io_in_d_bits_opcode,
  input  [2:0]  io_in_d_bits_size,
  input  [6:0]  io_in_d_bits_source,
  input         io_in_d_bits_denied,
  input         io_in_d_bits_corrupt
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire  _T_4; // @[Parameters.scala 47:9]
  wire  _T_5; // @[Parameters.scala 47:9]
  wire  _T_9; // @[Parameters.scala 55:32]
  wire  _T_10; // @[Parameters.scala 57:34]
  wire  _T_11; // @[Parameters.scala 55:69]
  wire  _T_12; // @[Parameters.scala 58:20]
  wire  _T_13; // @[Parameters.scala 57:50]
  wire  _T_17; // @[Parameters.scala 55:32]
  wire  _T_22; // @[Parameters.scala 47:9]
  wire  _T_23; // @[Parameters.scala 47:9]
  wire  _T_27; // @[Parameters.scala 55:32]
  wire  _T_29; // @[Parameters.scala 55:69]
  wire  _T_31; // @[Parameters.scala 57:50]
  wire  _T_35; // @[Parameters.scala 55:32]
  wire  _T_40; // @[Parameters.scala 47:9]
  wire  _T_44; // @[Parameters.scala 55:32]
  wire  _T_52; // @[Parameters.scala 55:32]
  wire  _T_60; // @[Parameters.scala 55:32]
  wire  _T_68; // @[Parameters.scala 55:32]
  wire  _T_76; // @[Parameters.scala 55:32]
  wire  _T_82; // @[Parameters.scala 924:46]
  wire  _T_83; // @[Parameters.scala 924:46]
  wire  _T_84; // @[Parameters.scala 924:46]
  wire  _T_85; // @[Parameters.scala 924:46]
  wire  _T_86; // @[Parameters.scala 924:46]
  wire  _T_87; // @[Parameters.scala 924:46]
  wire  _T_88; // @[Parameters.scala 924:46]
  wire  _T_89; // @[Parameters.scala 924:46]
  wire  _T_90; // @[Parameters.scala 924:46]
  wire  _T_91; // @[Parameters.scala 924:46]
  wire  _T_92; // @[Parameters.scala 924:46]
  wire  _T_93; // @[Parameters.scala 924:46]
  wire  _T_94; // @[Parameters.scala 924:46]
  wire [13:0] _T_96; // @[package.scala 189:77]
  wire [6:0] _T_98; // @[package.scala 189:46]
  wire [34:0] _GEN_18; // @[Edges.scala 22:16]
  wire [34:0] _T_99; // @[Edges.scala 22:16]
  wire  _T_100; // @[Edges.scala 22:24]
  wire [3:0] _T_103; // @[OneHot.scala 65:12]
  wire [2:0] _T_105; // @[Misc.scala 201:81]
  wire  _T_106; // @[Misc.scala 205:21]
  wire  _T_109; // @[Misc.scala 210:20]
  wire  _T_111; // @[Misc.scala 214:38]
  wire  _T_112; // @[Misc.scala 214:29]
  wire  _T_114; // @[Misc.scala 214:38]
  wire  _T_115; // @[Misc.scala 214:29]
  wire  _T_118; // @[Misc.scala 210:20]
  wire  _T_119; // @[Misc.scala 213:27]
  wire  _T_120; // @[Misc.scala 214:38]
  wire  _T_121; // @[Misc.scala 214:29]
  wire  _T_122; // @[Misc.scala 213:27]
  wire  _T_123; // @[Misc.scala 214:38]
  wire  _T_124; // @[Misc.scala 214:29]
  wire  _T_125; // @[Misc.scala 213:27]
  wire  _T_126; // @[Misc.scala 214:38]
  wire  _T_127; // @[Misc.scala 214:29]
  wire  _T_128; // @[Misc.scala 213:27]
  wire  _T_129; // @[Misc.scala 214:38]
  wire  _T_130; // @[Misc.scala 214:29]
  wire  _T_133; // @[Misc.scala 210:20]
  wire  _T_134; // @[Misc.scala 213:27]
  wire  _T_135; // @[Misc.scala 214:38]
  wire  _T_136; // @[Misc.scala 214:29]
  wire  _T_137; // @[Misc.scala 213:27]
  wire  _T_138; // @[Misc.scala 214:38]
  wire  _T_139; // @[Misc.scala 214:29]
  wire  _T_140; // @[Misc.scala 213:27]
  wire  _T_141; // @[Misc.scala 214:38]
  wire  _T_142; // @[Misc.scala 214:29]
  wire  _T_143; // @[Misc.scala 213:27]
  wire  _T_144; // @[Misc.scala 214:38]
  wire  _T_145; // @[Misc.scala 214:29]
  wire  _T_146; // @[Misc.scala 213:27]
  wire  _T_147; // @[Misc.scala 214:38]
  wire  _T_148; // @[Misc.scala 214:29]
  wire  _T_149; // @[Misc.scala 213:27]
  wire  _T_150; // @[Misc.scala 214:38]
  wire  _T_151; // @[Misc.scala 214:29]
  wire  _T_152; // @[Misc.scala 213:27]
  wire  _T_153; // @[Misc.scala 214:38]
  wire  _T_154; // @[Misc.scala 214:29]
  wire  _T_155; // @[Misc.scala 213:27]
  wire  _T_156; // @[Misc.scala 214:38]
  wire  _T_157; // @[Misc.scala 214:29]
  wire [7:0] _T_164; // @[Cat.scala 29:58]
  wire  _T_356; // @[Monitor.scala 79:25]
  wire [34:0] _T_358; // @[Parameters.scala 137:31]
  wire [35:0] _T_359; // @[Parameters.scala 137:49]
  wire [35:0] _T_361; // @[Parameters.scala 137:52]
  wire  _T_362; // @[Parameters.scala 137:67]
  wire  _T_367; // @[Monitor.scala 44:11]
  wire  _T_446; // @[Parameters.scala 92:48]
  wire  _T_450; // @[Mux.scala 27:72]
  wire  _T_451; // @[Mux.scala 27:72]
  wire  _T_454; // @[Mux.scala 27:72]
  wire  _T_455; // @[Mux.scala 27:72]
  wire  _T_464; // @[Mux.scala 27:72]
  wire  _T_467; // @[Mux.scala 27:72]
  wire  _T_468; // @[Mux.scala 27:72]
  wire  _T_479; // @[Monitor.scala 44:11]
  wire  _T_480; // @[Monitor.scala 44:11]
  wire  _T_482; // @[Monitor.scala 44:11]
  wire  _T_483; // @[Monitor.scala 44:11]
  wire  _T_486; // @[Monitor.scala 44:11]
  wire  _T_487; // @[Monitor.scala 44:11]
  wire  _T_489; // @[Monitor.scala 44:11]
  wire  _T_490; // @[Monitor.scala 44:11]
  wire  _T_491; // @[Bundles.scala 110:27]
  wire  _T_493; // @[Monitor.scala 44:11]
  wire  _T_494; // @[Monitor.scala 44:11]
  wire [7:0] _T_495; // @[Monitor.scala 86:18]
  wire  _T_496; // @[Monitor.scala 86:31]
  wire  _T_498; // @[Monitor.scala 44:11]
  wire  _T_499; // @[Monitor.scala 44:11]
  wire  _T_500; // @[Monitor.scala 87:18]
  wire  _T_502; // @[Monitor.scala 44:11]
  wire  _T_503; // @[Monitor.scala 44:11]
  wire  _T_504; // @[Monitor.scala 90:25]
  wire  _T_643; // @[Monitor.scala 97:31]
  wire  _T_645; // @[Monitor.scala 44:11]
  wire  _T_646; // @[Monitor.scala 44:11]
  wire  _T_656; // @[Monitor.scala 102:25]
  wire  _T_658; // @[Parameters.scala 93:42]
  wire  _T_666; // @[Parameters.scala 551:56]
  wire  _T_669; // @[Monitor.scala 44:11]
  wire  _T_670; // @[Monitor.scala 44:11]
  wire  _T_677; // @[Monitor.scala 106:31]
  wire  _T_679; // @[Monitor.scala 44:11]
  wire  _T_680; // @[Monitor.scala 44:11]
  wire  _T_681; // @[Monitor.scala 107:30]
  wire  _T_683; // @[Monitor.scala 44:11]
  wire  _T_684; // @[Monitor.scala 44:11]
  wire  _T_689; // @[Monitor.scala 111:25]
  wire  _T_702; // @[Monitor.scala 44:11]
  wire  _T_703; // @[Monitor.scala 44:11]
  wire  _T_718; // @[Monitor.scala 119:25]
  wire [7:0] _T_743; // @[Monitor.scala 124:33]
  wire [7:0] _T_744; // @[Monitor.scala 124:31]
  wire  _T_745; // @[Monitor.scala 124:40]
  wire  _T_747; // @[Monitor.scala 44:11]
  wire  _T_748; // @[Monitor.scala 44:11]
  wire  _T_749; // @[Monitor.scala 127:25]
  wire  _T_767; // @[Bundles.scala 140:33]
  wire  _T_769; // @[Monitor.scala 44:11]
  wire  _T_770; // @[Monitor.scala 44:11]
  wire  _T_775; // @[Monitor.scala 135:25]
  wire  _T_793; // @[Bundles.scala 147:30]
  wire  _T_795; // @[Monitor.scala 44:11]
  wire  _T_796; // @[Monitor.scala 44:11]
  wire  _T_801; // @[Monitor.scala 143:25]
  wire  _T_819; // @[Bundles.scala 160:28]
  wire  _T_821; // @[Monitor.scala 44:11]
  wire  _T_822; // @[Monitor.scala 44:11]
  wire  _T_831; // @[Bundles.scala 44:24]
  wire  _T_833; // @[Monitor.scala 51:11]
  wire  _T_834; // @[Monitor.scala 51:11]
  wire  _T_835; // @[Parameters.scala 47:9]
  wire  _T_836; // @[Parameters.scala 47:9]
  wire  _T_840; // @[Parameters.scala 55:32]
  wire  _T_841; // @[Parameters.scala 57:34]
  wire  _T_842; // @[Parameters.scala 55:69]
  wire  _T_843; // @[Parameters.scala 58:20]
  wire  _T_844; // @[Parameters.scala 57:50]
  wire  _T_848; // @[Parameters.scala 55:32]
  wire  _T_853; // @[Parameters.scala 47:9]
  wire  _T_854; // @[Parameters.scala 47:9]
  wire  _T_858; // @[Parameters.scala 55:32]
  wire  _T_860; // @[Parameters.scala 55:69]
  wire  _T_862; // @[Parameters.scala 57:50]
  wire  _T_866; // @[Parameters.scala 55:32]
  wire  _T_871; // @[Parameters.scala 47:9]
  wire  _T_875; // @[Parameters.scala 55:32]
  wire  _T_883; // @[Parameters.scala 55:32]
  wire  _T_891; // @[Parameters.scala 55:32]
  wire  _T_899; // @[Parameters.scala 55:32]
  wire  _T_907; // @[Parameters.scala 55:32]
  wire  _T_913; // @[Parameters.scala 924:46]
  wire  _T_914; // @[Parameters.scala 924:46]
  wire  _T_915; // @[Parameters.scala 924:46]
  wire  _T_916; // @[Parameters.scala 924:46]
  wire  _T_917; // @[Parameters.scala 924:46]
  wire  _T_918; // @[Parameters.scala 924:46]
  wire  _T_919; // @[Parameters.scala 924:46]
  wire  _T_920; // @[Parameters.scala 924:46]
  wire  _T_921; // @[Parameters.scala 924:46]
  wire  _T_922; // @[Parameters.scala 924:46]
  wire  _T_923; // @[Parameters.scala 924:46]
  wire  _T_924; // @[Parameters.scala 924:46]
  wire  _T_925; // @[Parameters.scala 924:46]
  wire  _T_927; // @[Monitor.scala 307:25]
  wire  _T_929; // @[Monitor.scala 51:11]
  wire  _T_930; // @[Monitor.scala 51:11]
  wire  _T_931; // @[Monitor.scala 309:27]
  wire  _T_933; // @[Monitor.scala 51:11]
  wire  _T_934; // @[Monitor.scala 51:11]
  wire  _T_939; // @[Monitor.scala 311:15]
  wire  _T_941; // @[Monitor.scala 51:11]
  wire  _T_942; // @[Monitor.scala 51:11]
  wire  _T_943; // @[Monitor.scala 312:15]
  wire  _T_945; // @[Monitor.scala 51:11]
  wire  _T_946; // @[Monitor.scala 51:11]
  wire  _T_947; // @[Monitor.scala 315:25]
  wire  _T_975; // @[Monitor.scala 325:25]
  wire  _T_995; // @[Monitor.scala 331:30]
  wire  _T_997; // @[Monitor.scala 51:11]
  wire  _T_998; // @[Monitor.scala 51:11]
  wire  _T_1004; // @[Monitor.scala 335:25]
  wire  _T_1021; // @[Monitor.scala 343:25]
  wire  _T_1039; // @[Monitor.scala 351:25]
  wire  _T_1071; // @[Decoupled.scala 40:37]
  wire  _T_1078; // @[Edges.scala 93:28]
  reg [3:0] _T_1080; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire [3:0] _T_1082; // @[Edges.scala 231:28]
  wire  _T_1083; // @[Edges.scala 232:25]
  reg [2:0] _T_1091; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [2:0] _T_1092; // @[Monitor.scala 382:22]
  reg [31:0] _RAND_2;
  reg [2:0] _T_1093; // @[Monitor.scala 383:22]
  reg [31:0] _RAND_3;
  reg [6:0] _T_1094; // @[Monitor.scala 384:22]
  reg [31:0] _RAND_4;
  reg [34:0] _T_1095; // @[Monitor.scala 385:22]
  reg [63:0] _RAND_5;
  wire  _T_1096; // @[Monitor.scala 386:22]
  wire  _T_1097; // @[Monitor.scala 386:19]
  wire  _T_1098; // @[Monitor.scala 387:32]
  wire  _T_1100; // @[Monitor.scala 44:11]
  wire  _T_1101; // @[Monitor.scala 44:11]
  wire  _T_1102; // @[Monitor.scala 388:32]
  wire  _T_1104; // @[Monitor.scala 44:11]
  wire  _T_1105; // @[Monitor.scala 44:11]
  wire  _T_1106; // @[Monitor.scala 389:32]
  wire  _T_1108; // @[Monitor.scala 44:11]
  wire  _T_1109; // @[Monitor.scala 44:11]
  wire  _T_1110; // @[Monitor.scala 390:32]
  wire  _T_1112; // @[Monitor.scala 44:11]
  wire  _T_1113; // @[Monitor.scala 44:11]
  wire  _T_1114; // @[Monitor.scala 391:32]
  wire  _T_1116; // @[Monitor.scala 44:11]
  wire  _T_1117; // @[Monitor.scala 44:11]
  wire  _T_1119; // @[Monitor.scala 393:20]
  wire  _T_1120; // @[Decoupled.scala 40:37]
  wire [13:0] _T_1122; // @[package.scala 189:77]
  wire [6:0] _T_1124; // @[package.scala 189:46]
  reg [3:0] _T_1128; // @[Edges.scala 230:27]
  reg [31:0] _RAND_6;
  wire [3:0] _T_1130; // @[Edges.scala 231:28]
  wire  _T_1131; // @[Edges.scala 232:25]
  reg [2:0] _T_1139; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_7;
  reg [2:0] _T_1141; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_8;
  reg [6:0] _T_1142; // @[Monitor.scala 535:22]
  reg [31:0] _RAND_9;
  reg  _T_1144; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_10;
  wire  _T_1145; // @[Monitor.scala 538:22]
  wire  _T_1146; // @[Monitor.scala 538:19]
  wire  _T_1147; // @[Monitor.scala 539:29]
  wire  _T_1149; // @[Monitor.scala 51:11]
  wire  _T_1150; // @[Monitor.scala 51:11]
  wire  _T_1155; // @[Monitor.scala 541:29]
  wire  _T_1157; // @[Monitor.scala 51:11]
  wire  _T_1158; // @[Monitor.scala 51:11]
  wire  _T_1159; // @[Monitor.scala 542:29]
  wire  _T_1161; // @[Monitor.scala 51:11]
  wire  _T_1162; // @[Monitor.scala 51:11]
  wire  _T_1167; // @[Monitor.scala 544:29]
  wire  _T_1169; // @[Monitor.scala 51:11]
  wire  _T_1170; // @[Monitor.scala 51:11]
  wire  _T_1172; // @[Monitor.scala 546:20]
  reg [113:0] _T_1173; // @[Monitor.scala 568:27]
  reg [127:0] _RAND_11;
  reg [3:0] _T_1183; // @[Edges.scala 230:27]
  reg [31:0] _RAND_12;
  wire [3:0] _T_1185; // @[Edges.scala 231:28]
  wire  _T_1186; // @[Edges.scala 232:25]
  reg [3:0] _T_1202; // @[Edges.scala 230:27]
  reg [31:0] _RAND_13;
  wire [3:0] _T_1204; // @[Edges.scala 231:28]
  wire  _T_1205; // @[Edges.scala 232:25]
  wire  _T_1215; // @[Monitor.scala 574:27]
  wire [127:0] _T_1217; // @[OneHot.scala 58:35]
  wire [113:0] _T_1218; // @[Monitor.scala 576:23]
  wire  _T_1220; // @[Monitor.scala 576:14]
  wire  _T_1222; // @[Monitor.scala 576:13]
  wire  _T_1223; // @[Monitor.scala 576:13]
  wire [127:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_1227; // @[Monitor.scala 581:27]
  wire  _T_1229; // @[Monitor.scala 581:75]
  wire  _T_1230; // @[Monitor.scala 581:72]
  wire [127:0] _T_1231; // @[OneHot.scala 58:35]
  wire [113:0] _T_1232; // @[Monitor.scala 583:21]
  wire [113:0] _T_1233; // @[Monitor.scala 583:32]
  wire  _T_1236; // @[Monitor.scala 51:11]
  wire  _T_1237; // @[Monitor.scala 51:11]
  wire [127:0] _GEN_16; // @[Monitor.scala 581:91]
  wire  _T_1238; // @[Monitor.scala 587:20]
  wire  _T_1239; // @[Monitor.scala 587:40]
  wire  _T_1240; // @[Monitor.scala 587:33]
  wire  _T_1241; // @[Monitor.scala 587:30]
  wire  _T_1243; // @[Monitor.scala 51:11]
  wire  _T_1244; // @[Monitor.scala 51:11]
  wire [113:0] _T_1245; // @[Monitor.scala 590:27]
  wire [113:0] _T_1246; // @[Monitor.scala 590:38]
  wire [113:0] _T_1247; // @[Monitor.scala 590:36]
  reg [31:0] _T_1248; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_14;
  wire  _T_1249; // @[Monitor.scala 595:23]
  wire  _T_1250; // @[Monitor.scala 595:13]
  wire  _T_1251; // @[Monitor.scala 595:36]
  wire  _T_1252; // @[Monitor.scala 595:27]
  wire  _T_1253; // @[Monitor.scala 595:56]
  wire  _T_1254; // @[Monitor.scala 595:44]
  wire  _T_1256; // @[Monitor.scala 595:12]
  wire  _T_1257; // @[Monitor.scala 595:12]
  wire [31:0] _T_1259; // @[Monitor.scala 597:26]
  wire  _T_1262; // @[Monitor.scala 598:27]
  wire  _GEN_19; // @[Monitor.scala 44:11]
  wire  _GEN_35; // @[Monitor.scala 44:11]
  wire  _GEN_53; // @[Monitor.scala 44:11]
  wire  _GEN_65; // @[Monitor.scala 44:11]
  wire  _GEN_75; // @[Monitor.scala 44:11]
  wire  _GEN_85; // @[Monitor.scala 44:11]
  wire  _GEN_95; // @[Monitor.scala 44:11]
  wire  _GEN_105; // @[Monitor.scala 44:11]
  wire  _GEN_117; // @[Monitor.scala 51:11]
  wire  _GEN_125; // @[Monitor.scala 51:11]
  wire  _GEN_133; // @[Monitor.scala 51:11]
  wire  _GEN_141; // @[Monitor.scala 51:11]
  wire  _GEN_145; // @[Monitor.scala 51:11]
  wire  _GEN_149; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_4 = io_in_a_bits_source == 7'h60; // @[Parameters.scala 47:9]
  assign _T_5 = io_in_a_bits_source == 7'h61; // @[Parameters.scala 47:9]
  assign _T_9 = io_in_a_bits_source[6:4] == 3'h6; // @[Parameters.scala 55:32]
  assign _T_10 = 4'h2 <= io_in_a_bits_source[3:0]; // @[Parameters.scala 57:34]
  assign _T_11 = _T_9 & _T_10; // @[Parameters.scala 55:69]
  assign _T_12 = io_in_a_bits_source[3:0] <= 4'h8; // @[Parameters.scala 58:20]
  assign _T_13 = _T_11 & _T_12; // @[Parameters.scala 57:50]
  assign _T_17 = io_in_a_bits_source[6:1] == 6'h38; // @[Parameters.scala 55:32]
  assign _T_22 = io_in_a_bits_source == 7'h40; // @[Parameters.scala 47:9]
  assign _T_23 = io_in_a_bits_source == 7'h41; // @[Parameters.scala 47:9]
  assign _T_27 = io_in_a_bits_source[6:4] == 3'h4; // @[Parameters.scala 55:32]
  assign _T_29 = _T_27 & _T_10; // @[Parameters.scala 55:69]
  assign _T_31 = _T_29 & _T_12; // @[Parameters.scala 57:50]
  assign _T_35 = io_in_a_bits_source[6:1] == 6'h28; // @[Parameters.scala 55:32]
  assign _T_40 = io_in_a_bits_source == 7'h24; // @[Parameters.scala 47:9]
  assign _T_44 = io_in_a_bits_source[6:3] == 4'h0; // @[Parameters.scala 55:32]
  assign _T_52 = io_in_a_bits_source[6:3] == 4'h1; // @[Parameters.scala 55:32]
  assign _T_60 = io_in_a_bits_source[6:3] == 4'h2; // @[Parameters.scala 55:32]
  assign _T_68 = io_in_a_bits_source[6:3] == 4'h3; // @[Parameters.scala 55:32]
  assign _T_76 = io_in_a_bits_source[6:2] == 5'h8; // @[Parameters.scala 55:32]
  assign _T_82 = _T_4 | _T_5; // @[Parameters.scala 924:46]
  assign _T_83 = _T_82 | _T_13; // @[Parameters.scala 924:46]
  assign _T_84 = _T_83 | _T_17; // @[Parameters.scala 924:46]
  assign _T_85 = _T_84 | _T_22; // @[Parameters.scala 924:46]
  assign _T_86 = _T_85 | _T_23; // @[Parameters.scala 924:46]
  assign _T_87 = _T_86 | _T_31; // @[Parameters.scala 924:46]
  assign _T_88 = _T_87 | _T_35; // @[Parameters.scala 924:46]
  assign _T_89 = _T_88 | _T_40; // @[Parameters.scala 924:46]
  assign _T_90 = _T_89 | _T_44; // @[Parameters.scala 924:46]
  assign _T_91 = _T_90 | _T_52; // @[Parameters.scala 924:46]
  assign _T_92 = _T_91 | _T_60; // @[Parameters.scala 924:46]
  assign _T_93 = _T_92 | _T_68; // @[Parameters.scala 924:46]
  assign _T_94 = _T_93 | _T_76; // @[Parameters.scala 924:46]
  assign _T_96 = 14'h7f << io_in_a_bits_size; // @[package.scala 189:77]
  assign _T_98 = ~_T_96[6:0]; // @[package.scala 189:46]
  assign _GEN_18 = {{28'd0}, _T_98}; // @[Edges.scala 22:16]
  assign _T_99 = io_in_a_bits_address & _GEN_18; // @[Edges.scala 22:16]
  assign _T_100 = _T_99 == 35'h0; // @[Edges.scala 22:24]
  assign _T_103 = 4'h1 << io_in_a_bits_size[1:0]; // @[OneHot.scala 65:12]
  assign _T_105 = _T_103[2:0] | 3'h1; // @[Misc.scala 201:81]
  assign _T_106 = io_in_a_bits_size >= 3'h3; // @[Misc.scala 205:21]
  assign _T_109 = ~io_in_a_bits_address[2]; // @[Misc.scala 210:20]
  assign _T_111 = _T_105[2] & _T_109; // @[Misc.scala 214:38]
  assign _T_112 = _T_106 | _T_111; // @[Misc.scala 214:29]
  assign _T_114 = _T_105[2] & io_in_a_bits_address[2]; // @[Misc.scala 214:38]
  assign _T_115 = _T_106 | _T_114; // @[Misc.scala 214:29]
  assign _T_118 = ~io_in_a_bits_address[1]; // @[Misc.scala 210:20]
  assign _T_119 = _T_109 & _T_118; // @[Misc.scala 213:27]
  assign _T_120 = _T_105[1] & _T_119; // @[Misc.scala 214:38]
  assign _T_121 = _T_112 | _T_120; // @[Misc.scala 214:29]
  assign _T_122 = _T_109 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_123 = _T_105[1] & _T_122; // @[Misc.scala 214:38]
  assign _T_124 = _T_112 | _T_123; // @[Misc.scala 214:29]
  assign _T_125 = io_in_a_bits_address[2] & _T_118; // @[Misc.scala 213:27]
  assign _T_126 = _T_105[1] & _T_125; // @[Misc.scala 214:38]
  assign _T_127 = _T_115 | _T_126; // @[Misc.scala 214:29]
  assign _T_128 = io_in_a_bits_address[2] & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_129 = _T_105[1] & _T_128; // @[Misc.scala 214:38]
  assign _T_130 = _T_115 | _T_129; // @[Misc.scala 214:29]
  assign _T_133 = ~io_in_a_bits_address[0]; // @[Misc.scala 210:20]
  assign _T_134 = _T_119 & _T_133; // @[Misc.scala 213:27]
  assign _T_135 = _T_105[0] & _T_134; // @[Misc.scala 214:38]
  assign _T_136 = _T_121 | _T_135; // @[Misc.scala 214:29]
  assign _T_137 = _T_119 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_138 = _T_105[0] & _T_137; // @[Misc.scala 214:38]
  assign _T_139 = _T_121 | _T_138; // @[Misc.scala 214:29]
  assign _T_140 = _T_122 & _T_133; // @[Misc.scala 213:27]
  assign _T_141 = _T_105[0] & _T_140; // @[Misc.scala 214:38]
  assign _T_142 = _T_124 | _T_141; // @[Misc.scala 214:29]
  assign _T_143 = _T_122 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_144 = _T_105[0] & _T_143; // @[Misc.scala 214:38]
  assign _T_145 = _T_124 | _T_144; // @[Misc.scala 214:29]
  assign _T_146 = _T_125 & _T_133; // @[Misc.scala 213:27]
  assign _T_147 = _T_105[0] & _T_146; // @[Misc.scala 214:38]
  assign _T_148 = _T_127 | _T_147; // @[Misc.scala 214:29]
  assign _T_149 = _T_125 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_150 = _T_105[0] & _T_149; // @[Misc.scala 214:38]
  assign _T_151 = _T_127 | _T_150; // @[Misc.scala 214:29]
  assign _T_152 = _T_128 & _T_133; // @[Misc.scala 213:27]
  assign _T_153 = _T_105[0] & _T_152; // @[Misc.scala 214:38]
  assign _T_154 = _T_130 | _T_153; // @[Misc.scala 214:29]
  assign _T_155 = _T_128 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_156 = _T_105[0] & _T_155; // @[Misc.scala 214:38]
  assign _T_157 = _T_130 | _T_156; // @[Misc.scala 214:29]
  assign _T_164 = {_T_157,_T_154,_T_151,_T_148,_T_145,_T_142,_T_139,_T_136}; // @[Cat.scala 29:58]
  assign _T_356 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_358 = io_in_a_bits_address ^ 35'h400000000; // @[Parameters.scala 137:31]
  assign _T_359 = {1'b0,$signed(_T_358)}; // @[Parameters.scala 137:49]
  assign _T_361 = $signed(_T_359) & -36'sh400000000; // @[Parameters.scala 137:52]
  assign _T_362 = $signed(_T_361) == 36'sh0; // @[Parameters.scala 137:67]
  assign _T_367 = ~reset; // @[Monitor.scala 44:11]
  assign _T_446 = 3'h6 == io_in_a_bits_size; // @[Parameters.scala 92:48]
  assign _T_450 = _T_4 & _T_446; // @[Mux.scala 27:72]
  assign _T_451 = _T_5 & _T_446; // @[Mux.scala 27:72]
  assign _T_454 = _T_22 & _T_446; // @[Mux.scala 27:72]
  assign _T_455 = _T_23 & _T_446; // @[Mux.scala 27:72]
  assign _T_464 = _T_450 | _T_451; // @[Mux.scala 27:72]
  assign _T_467 = _T_464 | _T_454; // @[Mux.scala 27:72]
  assign _T_468 = _T_467 | _T_455; // @[Mux.scala 27:72]
  assign _T_479 = _T_468 | reset; // @[Monitor.scala 44:11]
  assign _T_480 = ~_T_479; // @[Monitor.scala 44:11]
  assign _T_482 = _T_94 | reset; // @[Monitor.scala 44:11]
  assign _T_483 = ~_T_482; // @[Monitor.scala 44:11]
  assign _T_486 = _T_106 | reset; // @[Monitor.scala 44:11]
  assign _T_487 = ~_T_486; // @[Monitor.scala 44:11]
  assign _T_489 = _T_100 | reset; // @[Monitor.scala 44:11]
  assign _T_490 = ~_T_489; // @[Monitor.scala 44:11]
  assign _T_491 = io_in_a_bits_param <= 3'h2; // @[Bundles.scala 110:27]
  assign _T_493 = _T_491 | reset; // @[Monitor.scala 44:11]
  assign _T_494 = ~_T_493; // @[Monitor.scala 44:11]
  assign _T_495 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_496 = _T_495 == 8'h0; // @[Monitor.scala 86:31]
  assign _T_498 = _T_496 | reset; // @[Monitor.scala 44:11]
  assign _T_499 = ~_T_498; // @[Monitor.scala 44:11]
  assign _T_500 = ~io_in_a_bits_corrupt; // @[Monitor.scala 87:18]
  assign _T_502 = _T_500 | reset; // @[Monitor.scala 44:11]
  assign _T_503 = ~_T_502; // @[Monitor.scala 44:11]
  assign _T_504 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_643 = io_in_a_bits_param != 3'h0; // @[Monitor.scala 97:31]
  assign _T_645 = _T_643 | reset; // @[Monitor.scala 44:11]
  assign _T_646 = ~_T_645; // @[Monitor.scala 44:11]
  assign _T_656 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_658 = io_in_a_bits_size <= 3'h6; // @[Parameters.scala 93:42]
  assign _T_666 = _T_658 & _T_362; // @[Parameters.scala 551:56]
  assign _T_669 = _T_666 | reset; // @[Monitor.scala 44:11]
  assign _T_670 = ~_T_669; // @[Monitor.scala 44:11]
  assign _T_677 = io_in_a_bits_param == 3'h0; // @[Monitor.scala 106:31]
  assign _T_679 = _T_677 | reset; // @[Monitor.scala 44:11]
  assign _T_680 = ~_T_679; // @[Monitor.scala 44:11]
  assign _T_681 = io_in_a_bits_mask == _T_164; // @[Monitor.scala 107:30]
  assign _T_683 = _T_681 | reset; // @[Monitor.scala 44:11]
  assign _T_684 = ~_T_683; // @[Monitor.scala 44:11]
  assign _T_689 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_702 = _T_362 | reset; // @[Monitor.scala 44:11]
  assign _T_703 = ~_T_702; // @[Monitor.scala 44:11]
  assign _T_718 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_743 = ~_T_164; // @[Monitor.scala 124:33]
  assign _T_744 = io_in_a_bits_mask & _T_743; // @[Monitor.scala 124:31]
  assign _T_745 = _T_744 == 8'h0; // @[Monitor.scala 124:40]
  assign _T_747 = _T_745 | reset; // @[Monitor.scala 44:11]
  assign _T_748 = ~_T_747; // @[Monitor.scala 44:11]
  assign _T_749 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_767 = io_in_a_bits_param <= 3'h4; // @[Bundles.scala 140:33]
  assign _T_769 = _T_767 | reset; // @[Monitor.scala 44:11]
  assign _T_770 = ~_T_769; // @[Monitor.scala 44:11]
  assign _T_775 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_793 = io_in_a_bits_param <= 3'h3; // @[Bundles.scala 147:30]
  assign _T_795 = _T_793 | reset; // @[Monitor.scala 44:11]
  assign _T_796 = ~_T_795; // @[Monitor.scala 44:11]
  assign _T_801 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_819 = io_in_a_bits_param <= 3'h1; // @[Bundles.scala 160:28]
  assign _T_821 = _T_819 | reset; // @[Monitor.scala 44:11]
  assign _T_822 = ~_T_821; // @[Monitor.scala 44:11]
  assign _T_831 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_833 = _T_831 | reset; // @[Monitor.scala 51:11]
  assign _T_834 = ~_T_833; // @[Monitor.scala 51:11]
  assign _T_835 = io_in_d_bits_source == 7'h60; // @[Parameters.scala 47:9]
  assign _T_836 = io_in_d_bits_source == 7'h61; // @[Parameters.scala 47:9]
  assign _T_840 = io_in_d_bits_source[6:4] == 3'h6; // @[Parameters.scala 55:32]
  assign _T_841 = 4'h2 <= io_in_d_bits_source[3:0]; // @[Parameters.scala 57:34]
  assign _T_842 = _T_840 & _T_841; // @[Parameters.scala 55:69]
  assign _T_843 = io_in_d_bits_source[3:0] <= 4'h8; // @[Parameters.scala 58:20]
  assign _T_844 = _T_842 & _T_843; // @[Parameters.scala 57:50]
  assign _T_848 = io_in_d_bits_source[6:1] == 6'h38; // @[Parameters.scala 55:32]
  assign _T_853 = io_in_d_bits_source == 7'h40; // @[Parameters.scala 47:9]
  assign _T_854 = io_in_d_bits_source == 7'h41; // @[Parameters.scala 47:9]
  assign _T_858 = io_in_d_bits_source[6:4] == 3'h4; // @[Parameters.scala 55:32]
  assign _T_860 = _T_858 & _T_841; // @[Parameters.scala 55:69]
  assign _T_862 = _T_860 & _T_843; // @[Parameters.scala 57:50]
  assign _T_866 = io_in_d_bits_source[6:1] == 6'h28; // @[Parameters.scala 55:32]
  assign _T_871 = io_in_d_bits_source == 7'h24; // @[Parameters.scala 47:9]
  assign _T_875 = io_in_d_bits_source[6:3] == 4'h0; // @[Parameters.scala 55:32]
  assign _T_883 = io_in_d_bits_source[6:3] == 4'h1; // @[Parameters.scala 55:32]
  assign _T_891 = io_in_d_bits_source[6:3] == 4'h2; // @[Parameters.scala 55:32]
  assign _T_899 = io_in_d_bits_source[6:3] == 4'h3; // @[Parameters.scala 55:32]
  assign _T_907 = io_in_d_bits_source[6:2] == 5'h8; // @[Parameters.scala 55:32]
  assign _T_913 = _T_835 | _T_836; // @[Parameters.scala 924:46]
  assign _T_914 = _T_913 | _T_844; // @[Parameters.scala 924:46]
  assign _T_915 = _T_914 | _T_848; // @[Parameters.scala 924:46]
  assign _T_916 = _T_915 | _T_853; // @[Parameters.scala 924:46]
  assign _T_917 = _T_916 | _T_854; // @[Parameters.scala 924:46]
  assign _T_918 = _T_917 | _T_862; // @[Parameters.scala 924:46]
  assign _T_919 = _T_918 | _T_866; // @[Parameters.scala 924:46]
  assign _T_920 = _T_919 | _T_871; // @[Parameters.scala 924:46]
  assign _T_921 = _T_920 | _T_875; // @[Parameters.scala 924:46]
  assign _T_922 = _T_921 | _T_883; // @[Parameters.scala 924:46]
  assign _T_923 = _T_922 | _T_891; // @[Parameters.scala 924:46]
  assign _T_924 = _T_923 | _T_899; // @[Parameters.scala 924:46]
  assign _T_925 = _T_924 | _T_907; // @[Parameters.scala 924:46]
  assign _T_927 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_929 = _T_925 | reset; // @[Monitor.scala 51:11]
  assign _T_930 = ~_T_929; // @[Monitor.scala 51:11]
  assign _T_931 = io_in_d_bits_size >= 3'h3; // @[Monitor.scala 309:27]
  assign _T_933 = _T_931 | reset; // @[Monitor.scala 51:11]
  assign _T_934 = ~_T_933; // @[Monitor.scala 51:11]
  assign _T_939 = ~io_in_d_bits_corrupt; // @[Monitor.scala 311:15]
  assign _T_941 = _T_939 | reset; // @[Monitor.scala 51:11]
  assign _T_942 = ~_T_941; // @[Monitor.scala 51:11]
  assign _T_943 = ~io_in_d_bits_denied; // @[Monitor.scala 312:15]
  assign _T_945 = _T_943 | reset; // @[Monitor.scala 51:11]
  assign _T_946 = ~_T_945; // @[Monitor.scala 51:11]
  assign _T_947 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_975 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_995 = _T_943 | io_in_d_bits_corrupt; // @[Monitor.scala 331:30]
  assign _T_997 = _T_995 | reset; // @[Monitor.scala 51:11]
  assign _T_998 = ~_T_997; // @[Monitor.scala 51:11]
  assign _T_1004 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_1021 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_1039 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_1071 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_1078 = ~io_in_a_bits_opcode[2]; // @[Edges.scala 93:28]
  assign _T_1082 = _T_1080 - 4'h1; // @[Edges.scala 231:28]
  assign _T_1083 = _T_1080 == 4'h0; // @[Edges.scala 232:25]
  assign _T_1096 = ~_T_1083; // @[Monitor.scala 386:22]
  assign _T_1097 = io_in_a_valid & _T_1096; // @[Monitor.scala 386:19]
  assign _T_1098 = io_in_a_bits_opcode == _T_1091; // @[Monitor.scala 387:32]
  assign _T_1100 = _T_1098 | reset; // @[Monitor.scala 44:11]
  assign _T_1101 = ~_T_1100; // @[Monitor.scala 44:11]
  assign _T_1102 = io_in_a_bits_param == _T_1092; // @[Monitor.scala 388:32]
  assign _T_1104 = _T_1102 | reset; // @[Monitor.scala 44:11]
  assign _T_1105 = ~_T_1104; // @[Monitor.scala 44:11]
  assign _T_1106 = io_in_a_bits_size == _T_1093; // @[Monitor.scala 389:32]
  assign _T_1108 = _T_1106 | reset; // @[Monitor.scala 44:11]
  assign _T_1109 = ~_T_1108; // @[Monitor.scala 44:11]
  assign _T_1110 = io_in_a_bits_source == _T_1094; // @[Monitor.scala 390:32]
  assign _T_1112 = _T_1110 | reset; // @[Monitor.scala 44:11]
  assign _T_1113 = ~_T_1112; // @[Monitor.scala 44:11]
  assign _T_1114 = io_in_a_bits_address == _T_1095; // @[Monitor.scala 391:32]
  assign _T_1116 = _T_1114 | reset; // @[Monitor.scala 44:11]
  assign _T_1117 = ~_T_1116; // @[Monitor.scala 44:11]
  assign _T_1119 = _T_1071 & _T_1083; // @[Monitor.scala 393:20]
  assign _T_1120 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_1122 = 14'h7f << io_in_d_bits_size; // @[package.scala 189:77]
  assign _T_1124 = ~_T_1122[6:0]; // @[package.scala 189:46]
  assign _T_1130 = _T_1128 - 4'h1; // @[Edges.scala 231:28]
  assign _T_1131 = _T_1128 == 4'h0; // @[Edges.scala 232:25]
  assign _T_1145 = ~_T_1131; // @[Monitor.scala 538:22]
  assign _T_1146 = io_in_d_valid & _T_1145; // @[Monitor.scala 538:19]
  assign _T_1147 = io_in_d_bits_opcode == _T_1139; // @[Monitor.scala 539:29]
  assign _T_1149 = _T_1147 | reset; // @[Monitor.scala 51:11]
  assign _T_1150 = ~_T_1149; // @[Monitor.scala 51:11]
  assign _T_1155 = io_in_d_bits_size == _T_1141; // @[Monitor.scala 541:29]
  assign _T_1157 = _T_1155 | reset; // @[Monitor.scala 51:11]
  assign _T_1158 = ~_T_1157; // @[Monitor.scala 51:11]
  assign _T_1159 = io_in_d_bits_source == _T_1142; // @[Monitor.scala 542:29]
  assign _T_1161 = _T_1159 | reset; // @[Monitor.scala 51:11]
  assign _T_1162 = ~_T_1161; // @[Monitor.scala 51:11]
  assign _T_1167 = io_in_d_bits_denied == _T_1144; // @[Monitor.scala 544:29]
  assign _T_1169 = _T_1167 | reset; // @[Monitor.scala 51:11]
  assign _T_1170 = ~_T_1169; // @[Monitor.scala 51:11]
  assign _T_1172 = _T_1120 & _T_1131; // @[Monitor.scala 546:20]
  assign _T_1185 = _T_1183 - 4'h1; // @[Edges.scala 231:28]
  assign _T_1186 = _T_1183 == 4'h0; // @[Edges.scala 232:25]
  assign _T_1204 = _T_1202 - 4'h1; // @[Edges.scala 231:28]
  assign _T_1205 = _T_1202 == 4'h0; // @[Edges.scala 232:25]
  assign _T_1215 = _T_1071 & _T_1186; // @[Monitor.scala 574:27]
  assign _T_1217 = 128'h1 << io_in_a_bits_source; // @[OneHot.scala 58:35]
  assign _T_1218 = _T_1173 >> io_in_a_bits_source; // @[Monitor.scala 576:23]
  assign _T_1220 = ~_T_1218[0]; // @[Monitor.scala 576:14]
  assign _T_1222 = _T_1220 | reset; // @[Monitor.scala 576:13]
  assign _T_1223 = ~_T_1222; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_1215 ? _T_1217 : 128'h0; // @[Monitor.scala 574:72]
  assign _T_1227 = _T_1120 & _T_1205; // @[Monitor.scala 581:27]
  assign _T_1229 = ~_T_927; // @[Monitor.scala 581:75]
  assign _T_1230 = _T_1227 & _T_1229; // @[Monitor.scala 581:72]
  assign _T_1231 = 128'h1 << io_in_d_bits_source; // @[OneHot.scala 58:35]
  assign _T_1232 = _GEN_15[113:0] | _T_1173; // @[Monitor.scala 583:21]
  assign _T_1233 = _T_1232 >> io_in_d_bits_source; // @[Monitor.scala 583:32]
  assign _T_1236 = _T_1233[0] | reset; // @[Monitor.scala 51:11]
  assign _T_1237 = ~_T_1236; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_1230 ? _T_1231 : 128'h0; // @[Monitor.scala 581:91]
  assign _T_1238 = _GEN_15[113:0] != _GEN_16[113:0]; // @[Monitor.scala 587:20]
  assign _T_1239 = _GEN_15[113:0] != 114'h0; // @[Monitor.scala 587:40]
  assign _T_1240 = ~_T_1239; // @[Monitor.scala 587:33]
  assign _T_1241 = _T_1238 | _T_1240; // @[Monitor.scala 587:30]
  assign _T_1243 = _T_1241 | reset; // @[Monitor.scala 51:11]
  assign _T_1244 = ~_T_1243; // @[Monitor.scala 51:11]
  assign _T_1245 = _T_1173 | _GEN_15[113:0]; // @[Monitor.scala 590:27]
  assign _T_1246 = ~_GEN_16[113:0]; // @[Monitor.scala 590:38]
  assign _T_1247 = _T_1245 & _T_1246; // @[Monitor.scala 590:36]
  assign _T_1249 = _T_1173 != 114'h0; // @[Monitor.scala 595:23]
  assign _T_1250 = ~_T_1249; // @[Monitor.scala 595:13]
  assign _T_1251 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_1252 = _T_1250 | _T_1251; // @[Monitor.scala 595:27]
  assign _T_1253 = _T_1248 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_1254 = _T_1252 | _T_1253; // @[Monitor.scala 595:44]
  assign _T_1256 = _T_1254 | reset; // @[Monitor.scala 595:12]
  assign _T_1257 = ~_T_1256; // @[Monitor.scala 595:12]
  assign _T_1259 = _T_1248 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_1262 = _T_1071 | _T_1120; // @[Monitor.scala 598:27]
  assign _GEN_19 = io_in_a_valid & _T_356; // @[Monitor.scala 44:11]
  assign _GEN_35 = io_in_a_valid & _T_504; // @[Monitor.scala 44:11]
  assign _GEN_53 = io_in_a_valid & _T_656; // @[Monitor.scala 44:11]
  assign _GEN_65 = io_in_a_valid & _T_689; // @[Monitor.scala 44:11]
  assign _GEN_75 = io_in_a_valid & _T_718; // @[Monitor.scala 44:11]
  assign _GEN_85 = io_in_a_valid & _T_749; // @[Monitor.scala 44:11]
  assign _GEN_95 = io_in_a_valid & _T_775; // @[Monitor.scala 44:11]
  assign _GEN_105 = io_in_a_valid & _T_801; // @[Monitor.scala 44:11]
  assign _GEN_117 = io_in_d_valid & _T_927; // @[Monitor.scala 51:11]
  assign _GEN_125 = io_in_d_valid & _T_947; // @[Monitor.scala 51:11]
  assign _GEN_133 = io_in_d_valid & _T_975; // @[Monitor.scala 51:11]
  assign _GEN_141 = io_in_d_valid & _T_1004; // @[Monitor.scala 51:11]
  assign _GEN_145 = io_in_d_valid & _T_1021; // @[Monitor.scala 51:11]
  assign _GEN_149 = io_in_d_valid & _T_1039; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_1080 = _RAND_0[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_1091 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_1092 = _RAND_2[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_1093 = _RAND_3[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_1094 = _RAND_4[6:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {2{`RANDOM}};
  _T_1095 = _RAND_5[34:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_1128 = _RAND_6[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_1139 = _RAND_7[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_1141 = _RAND_8[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_1142 = _RAND_9[6:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_1144 = _RAND_10[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {4{`RANDOM}};
  _T_1173 = _RAND_11[113:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_1183 = _RAND_12[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  _T_1202 = _RAND_13[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_14 = {1{`RANDOM}};
  _T_1248 = _RAND_14[31:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_1080 <= 4'h0;
    end else if (_T_1071) begin
      if (_T_1083) begin
        if (_T_1078) begin
          _T_1080 <= _T_98[6:3];
        end else begin
          _T_1080 <= 4'h0;
        end
      end else begin
        _T_1080 <= _T_1082;
      end
    end
    if (_T_1119) begin
      _T_1091 <= io_in_a_bits_opcode;
    end
    if (_T_1119) begin
      _T_1092 <= io_in_a_bits_param;
    end
    if (_T_1119) begin
      _T_1093 <= io_in_a_bits_size;
    end
    if (_T_1119) begin
      _T_1094 <= io_in_a_bits_source;
    end
    if (_T_1119) begin
      _T_1095 <= io_in_a_bits_address;
    end
    if (reset) begin
      _T_1128 <= 4'h0;
    end else if (_T_1120) begin
      if (_T_1131) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1128 <= _T_1124[6:3];
        end else begin
          _T_1128 <= 4'h0;
        end
      end else begin
        _T_1128 <= _T_1130;
      end
    end
    if (_T_1172) begin
      _T_1139 <= io_in_d_bits_opcode;
    end
    if (_T_1172) begin
      _T_1141 <= io_in_d_bits_size;
    end
    if (_T_1172) begin
      _T_1142 <= io_in_d_bits_source;
    end
    if (_T_1172) begin
      _T_1144 <= io_in_d_bits_denied;
    end
    if (reset) begin
      _T_1173 <= 114'h0;
    end else begin
      _T_1173 <= _T_1247;
    end
    if (reset) begin
      _T_1183 <= 4'h0;
    end else if (_T_1071) begin
      if (_T_1186) begin
        if (_T_1078) begin
          _T_1183 <= _T_98[6:3];
        end else begin
          _T_1183 <= 4'h0;
        end
      end else begin
        _T_1183 <= _T_1185;
      end
    end
    if (reset) begin
      _T_1202 <= 4'h0;
    end else if (_T_1120) begin
      if (_T_1205) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1202 <= _T_1124[6:3];
        end else begin
          _T_1202 <= 4'h0;
        end
      end else begin
        _T_1202 <= _T_1204;
      end
    end
    if (reset) begin
      _T_1248 <= 32'h0;
    end else if (_T_1262) begin
      _T_1248 <= 32'h0;
    end else begin
      _T_1248 <= _T_1259;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_367) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_367) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_480) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_480) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_483) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid source ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_483) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_487) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock smaller than a beat (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_487) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_490) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_490) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_494) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid grow param (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_494) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_499) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_499) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_503) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock is corrupt (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_503) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_367) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_367) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_480) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_480) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_483) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid source ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_483) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_487) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm smaller than a beat (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_487) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_490) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_490) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_494) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid grow param (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_494) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_646) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_646) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_499) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_499) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_503) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm is corrupt (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_503) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_670) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_670) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_483) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid source ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_483) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_490) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_490) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_680) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid param (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_680) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_684) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_684) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_503) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get is corrupt (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_503) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_703) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_703) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_483) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid source ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_483) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_490) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_490) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_680) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid param (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_680) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_684) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_684) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_703) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_703) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_483) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid source ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_483) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_490) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_490) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_680) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid param (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_680) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_748) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial contains invalid mask (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_748) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_367) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_367) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_483) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid source ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_483) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_490) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_490) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_770) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid opcode param (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_770) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_684) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_684) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_367) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_367) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_483) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid source ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_483) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_490) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_490) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_796) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid opcode param (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_796) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_684) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_684) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_367) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_367) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_483) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid source ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_483) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_490) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_490) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_822) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid opcode param (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_822) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_684) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_684) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_503) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint is corrupt (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_503) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_834) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_834) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_930) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck carries invalid source ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_930) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_934) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_934) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_942) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is corrupt (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_942) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_946) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is denied (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_946) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_125 & _T_930) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid source ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_125 & _T_930) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_125 & _T_367) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid sink ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_125 & _T_367) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_125 & _T_934) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_125 & _T_934) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_125 & _T_942) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is corrupt (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_125 & _T_942) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_133 & _T_930) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid source ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_133 & _T_930) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_133 & _T_367) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid sink ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_133 & _T_367) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_133 & _T_934) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_133 & _T_934) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_133 & _T_998) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied but not corrupt (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_133 & _T_998) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_141 & _T_930) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid source ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_141 & _T_930) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_141 & _T_942) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is corrupt (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_141 & _T_942) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_145 & _T_930) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid source ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_145 & _T_930) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_145 & _T_998) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied but not corrupt (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_145 & _T_998) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_149 & _T_930) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid source ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_149 & _T_930) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_149 & _T_942) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is corrupt (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_149 & _T_942) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1097 & _T_1101) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1097 & _T_1101) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1097 & _T_1105) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel param changed within multibeat operation (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1097 & _T_1105) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1097 & _T_1109) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel size changed within multibeat operation (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1097 & _T_1109) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1097 & _T_1113) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel source changed within multibeat operation (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1097 & _T_1113) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1097 & _T_1117) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1097 & _T_1117) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1146 & _T_1150) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1146 & _T_1150) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1146 & _T_1158) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1146 & _T_1158) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1146 & _T_1162) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel source changed within multibeat operation (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1146 & _T_1162) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1146 & _T_1170) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1146 & _T_1170) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1215 & _T_1223) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1215 & _T_1223) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1230 & _T_1237) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1230 & _T_1237) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1244) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' and 'D' concurrent, despite minlatency 2 (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1244) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1257) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at AXI4Bridge.scala:102:9)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1257) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
