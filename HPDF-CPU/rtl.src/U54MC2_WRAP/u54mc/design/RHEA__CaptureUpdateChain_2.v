//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__CaptureUpdateChain_2(
  input        rf_reset,
  input        clock,
  input        reset,
  input        io_chainIn_shift,
  input        io_chainIn_data,
  input        io_chainIn_capture,
  input        io_chainIn_update,
  output       io_chainOut_data,
  output [4:0] io_update_bits
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
`endif // RANDOMIZE_REG_INIT
  reg  regs_0; // @[JtagShifter.scala 155:39]
  reg  regs_1; // @[JtagShifter.scala 155:39]
  reg  regs_2; // @[JtagShifter.scala 155:39]
  reg  regs_3; // @[JtagShifter.scala 155:39]
  reg  regs_4; // @[JtagShifter.scala 155:39]
  wire [1:0] updateBits_lo = {regs_1,regs_0}; // @[Cat.scala 30:58]
  wire [2:0] updateBits_hi = {regs_4,regs_3,regs_2}; // @[Cat.scala 30:58]
  wire  _GEN_1 = io_chainIn_shift ? regs_1 : regs_0; // @[JtagShifter.scala 175:34 JtagShifter.scala 177:37 JtagShifter.scala 155:39]
  wire  _GEN_9 = io_chainIn_update ? regs_0 : _GEN_1; // @[JtagShifter.scala 172:35 JtagShifter.scala 155:39]
  assign io_chainOut_data = regs_0; // @[JtagShifter.scala 157:20]
  assign io_update_bits = {updateBits_hi,updateBits_lo}; // @[Cat.scala 30:58]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regs_0 <= 1'h0;
    end else begin
      regs_0 <= io_chainIn_capture | _GEN_9;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regs_1 <= 1'h0;
    end else if (io_chainIn_capture) begin
      regs_1 <= 1'h0;
    end else if (!(io_chainIn_update)) begin
      if (io_chainIn_shift) begin
        regs_1 <= regs_2;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regs_2 <= 1'h0;
    end else if (io_chainIn_capture) begin
      regs_2 <= 1'h0;
    end else if (!(io_chainIn_update)) begin
      if (io_chainIn_shift) begin
        regs_2 <= regs_3;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regs_3 <= 1'h0;
    end else if (io_chainIn_capture) begin
      regs_3 <= 1'h0;
    end else if (!(io_chainIn_update)) begin
      if (io_chainIn_shift) begin
        regs_3 <= regs_4;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regs_4 <= 1'h0;
    end else if (io_chainIn_capture) begin
      regs_4 <= 1'h0;
    end else if (!(io_chainIn_update)) begin
      if (io_chainIn_shift) begin
        regs_4 <= io_chainIn_data;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  regs_0 = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  regs_1 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  regs_2 = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  regs_3 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  regs_4 = _RAND_4[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    regs_0 = 1'h0;
  end
  if (rf_reset) begin
    regs_1 = 1'h0;
  end
  if (rf_reset) begin
    regs_2 = 1'h0;
  end
  if (rf_reset) begin
    regs_3 = 1'h0;
  end
  if (rf_reset) begin
    regs_4 = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
