//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_DebugTransportModuleJTAG_assert(
  input         io_jtag_clock,
  input         io_jtag_reset,
  input  [10:0] io_jtag_mfr_id,
  input  [15:0] io_jtag_part_number,
  input  [3:0]  io_jtag_version,
  input         stickyBusyReg,
  input         _T_21,
  input         _T_28,
  input         dmiAccessChain_io_update_valid
);
  wire  _GEN_14; // @[DebugTransport.scala 214:97]
  wire  _GEN_19; // @[DebugTransport.scala 212:26]
  wire  dmiReqValidCheck; // @[DebugTransport.scala 211:41]
  wire  _T_22; // @[DebugTransport.scala 209:29]
  wire  _T_23; // @[DebugTransport.scala 209:10]
  wire  _T_25; // @[DebugTransport.scala 209:9]
  wire  _T_26; // @[DebugTransport.scala 209:9]
  wire [31:0] _T_59; // @[JtagTap.scala 188:45]
  wire [31:0] _GEN_0; // @[JtagTap.scala 189:18]
  wire [1:0] _T_60; // @[JtagTap.scala 189:18]
  wire  _T_61; // @[JtagTap.scala 189:24]
  wire  _T_63; // @[JtagTap.scala 189:15]
  wire  _T_64; // @[JtagTap.scala 189:15]
  wire [11:0] _T_68; // @[JtagTap.scala 190:41]
  wire [30:0] _GEN_59; // @[JtagTap.scala 190:26]
  wire [30:0] _T_69; // @[JtagTap.scala 190:26]
  wire  _T_70; // @[JtagTap.scala 190:49]
  wire  _T_72; // @[JtagTap.scala 190:15]
  wire  _T_73; // @[JtagTap.scala 190:15]
  assign _GEN_14 = _T_28 ? 1'h0 : 1'h1; // @[DebugTransport.scala 214:97]
  assign _GEN_19 = stickyBusyReg ? 1'h0 : _GEN_14; // @[DebugTransport.scala 212:26]
  assign dmiReqValidCheck = dmiAccessChain_io_update_valid & _GEN_19; // @[DebugTransport.scala 211:41]
  assign _T_22 = dmiReqValidCheck & _T_21; // @[DebugTransport.scala 209:29]
  assign _T_23 = ~_T_22; // @[DebugTransport.scala 209:10]
  assign _T_25 = _T_23 | io_jtag_reset; // @[DebugTransport.scala 209:9]
  assign _T_26 = ~_T_25; // @[DebugTransport.scala 209:9]
  assign _T_59 = {io_jtag_version,io_jtag_part_number,io_jtag_mfr_id,1'h1}; // @[JtagTap.scala 188:45]
  assign _GEN_0 = _T_59 % 32'h2; // @[JtagTap.scala 189:18]
  assign _T_60 = _GEN_0[1:0]; // @[JtagTap.scala 189:18]
  assign _T_61 = _T_60 == 2'h1; // @[JtagTap.scala 189:24]
  assign _T_63 = _T_61 | io_jtag_reset; // @[JtagTap.scala 189:15]
  assign _T_64 = ~_T_63; // @[JtagTap.scala 189:15]
  assign _T_68 = 12'h800 - 12'h1; // @[JtagTap.scala 190:41]
  assign _GEN_59 = {{19'd0}, _T_68}; // @[JtagTap.scala 190:26]
  assign _T_69 = _T_59[31:1] & _GEN_59; // @[JtagTap.scala 190:26]
  assign _T_70 = _T_69 != 31'h7f; // @[JtagTap.scala 190:49]
  assign _T_72 = _T_70 | io_jtag_reset; // @[JtagTap.scala 190:15]
  assign _T_73 = ~_T_72; // @[JtagTap.scala 190:15]
  always @(posedge io_jtag_clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_26) begin
          $fwrite(32'h80000002,"Assertion Failed: Conflicting updates for dmiReqValidReg, should not happen.\n    at DebugTransport.scala:209 assert(!(dmiReqValidCheck && io.dmi.req.fire()), \"Conflicting updates for dmiReqValidReg, should not happen.\");\n"); // @[DebugTransport.scala 209:9]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_26) begin
          $fatal; // @[DebugTransport.scala 209:9]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_64) begin
          $fwrite(32'h80000002,"Assertion Failed: LSB must be set in IDCODE, see 12.1.1d\n    at JtagTap.scala:189 assert(i %% 2.U === 1.U, \"LSB must be set in IDCODE, see 12.1.1d\")\n"); // @[JtagTap.scala 189:15]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_64) begin
          $fatal; // @[JtagTap.scala 189:15]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: IDCODE must not have 0b00001111111 as manufacturer identity, see 12.2.1b\n    at JtagTap.scala:190 assert(((i >> 1) & ((1.U << 11) - 1.U)) =/= JtagIdcode.dummyMfrId.U,\n"); // @[JtagTap.scala 190:15]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_73) begin
          $fatal; // @[JtagTap.scala 190:15]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
