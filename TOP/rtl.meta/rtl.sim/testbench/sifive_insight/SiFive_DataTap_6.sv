//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_DataTap_6(
  output [35:0] _5,
  output [63:0] _4,
  output [1:0]  _3,
  output        _2_debug,
  output        _2_cease,
  output        _2_wfi,
  output [31:0] _2_isa,
  output [1:0]  _2_dprv,
  output [1:0]  _2_prv,
  output        _2_sd,
  output [26:0] _2_zero2,
  output [1:0]  _2_sxl,
  output [1:0]  _2_uxl,
  output        _2_sd_rv32,
  output [7:0]  _2_zero1,
  output        _2_tsr,
  output        _2_tw,
  output        _2_tvm,
  output        _2_mxr,
  output        _2_sum,
  output        _2_mprv,
  output [1:0]  _2_xs,
  output [1:0]  _2_fs,
  output [1:0]  _2_mpp,
  output [1:0]  _2_vs,
  output        _2_spp,
  output        _2_mpie,
  output        _2_hpie,
  output        _2_spie,
  output        _2_upie,
  output        _2_mie,
  output        _2_hie,
  output        _2_sie,
  output        _2_uie,
  output [63:0] _1,
  output [1:0]  _0
);
  assign _5 = rocket.core.csr.reg_mtvec;
  assign _4 = rocket.core.csr.io_cause;
  assign _3 = rocket.core.csr.reg_mstatus_prv;
  assign _2_debug = rocket.core.csr.io_status_debug;
  assign _2_cease = rocket.core.csr.io_status_cease;
  assign _2_wfi = rocket.core.csr.io_status_wfi;
  assign _2_isa = rocket.core.csr.io_status_isa;
  assign _2_dprv = rocket.core.csr.io_status_dprv;
  assign _2_prv = rocket.core.csr.io_status_prv;
  assign _2_sd = rocket.core.csr.io_status_sd;
  assign _2_zero2 = rocket.core.csr.io_status_zero2;
  assign _2_sxl = rocket.core.csr.io_status_sxl;
  assign _2_uxl = rocket.core.csr.io_status_uxl;
  assign _2_sd_rv32 = rocket.core.csr.io_status_sd_rv32;
  assign _2_zero1 = rocket.core.csr.io_status_zero1;
  assign _2_tsr = rocket.core.csr.io_status_tsr;
  assign _2_tw = rocket.core.csr.io_status_tw;
  assign _2_tvm = rocket.core.csr.io_status_tvm;
  assign _2_mxr = rocket.core.csr.io_status_mxr;
  assign _2_sum = rocket.core.csr.io_status_sum;
  assign _2_mprv = rocket.core.csr.io_status_mprv;
  assign _2_xs = rocket.core.csr.io_status_xs;
  assign _2_fs = rocket.core.csr.io_status_fs;
  assign _2_mpp = rocket.core.csr.io_status_mpp;
  assign _2_vs = rocket.core.csr.io_status_vs;
  assign _2_spp = rocket.core.csr.io_status_spp;
  assign _2_mpie = rocket.core.csr.io_status_mpie;
  assign _2_hpie = rocket.core.csr.io_status_hpie;
  assign _2_spie = rocket.core.csr.io_status_spie;
  assign _2_upie = rocket.core.csr.io_status_upie;
  assign _2_mie = rocket.core.csr.io_status_mie;
  assign _2_hie = rocket.core.csr.io_status_hie;
  assign _2_sie = rocket.core.csr.io_status_sie;
  assign _2_uie = rocket.core.csr.io_status_uie;
  assign _1 = rocket.core.csr.io_time;
  assign _0 = rocket.core.csr.io_hartid;
endmodule
