//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TraceTimestamp(
  output [39:0] auto_timestamp_out,
  input  [39:0] io_extTimestampIn,
  output [39:0] io_timestamp,
  input  [1:0]  io_tsControlIn_tsBranch,
  input         io_tsControlIn_tsEnable,
  output [1:0]  io_tsControl_tsBranch,
  output        io_tsControl_tsEnable
);
  wire [39:0] timestamp_auto_in;
  wire [39:0] timestamp_auto_out;
  assign timestamp_auto_out = timestamp_auto_in; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_timestamp_out = timestamp_auto_out; // @[LazyModule.scala 390:12]
  assign io_timestamp = io_extTimestampIn; // @[TraceTimestamp.scala 219:49]
  assign io_tsControl_tsBranch = io_tsControlIn_tsBranch; // @[TraceTimestamp.scala 176:18]
  assign io_tsControl_tsEnable = io_tsControlIn_tsEnable; // @[TraceTimestamp.scala 176:18]
  assign timestamp_auto_in = io_timestamp; // @[BundleBridge.scala 66:109 TraceTimestamp.scala 225:32]
endmodule
