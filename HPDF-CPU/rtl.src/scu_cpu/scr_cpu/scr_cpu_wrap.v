module scr_cpu_wrap (/*AUTOARG*/
   // Outputs
   SFR__SYSPORT_AWUSER, SFR__SYSPORT_ARUSER,
   SFR__SUPPRESS_STRB_ON_CORRUPT, SFR__RTC_TOGGLE,
   SFR__RESETCTRL_HARTISINRESET_1, SFR__RESETCTRL_HARTISINRESET_0,
   SFR__PSD_TEST_CLOCK_ENABLE, SFR__MEMPORT_AWUSER,
   SFR__MEMPORT_ARUSER, SFR__DEBUG_SYSTEMJTAG_VERSION,
   SFR__DEBUG_SYSTEMJTAG_PART_NUMBER, SFR__DEBUG_SYSTEMJTAG_MFR_ID,
   SFR__DEBUG_DMACTIVEACK, SCR__RSTREQN0, SCR__RSTCYCLE4,
   SCR__RSTCYCLE3, SCR__RSTCYCLE2, SCR__RSTCYCLE1, SCR__RSTCYCLE0,
   SCR__PLL_CPU_VCO_BOOST, SCR__PLL_CPU_S, SCR__PLL_CPU_RSEL,
   SCR__PLL_CPU_RESETB, SCR__PLL_CPU_PBIAS_CTRL_EN,
   SCR__PLL_CPU_PBIAS_CTRL, SCR__PLL_CPU_P, SCR__PLL_CPU_M,
   SCR__PLL_CPU_LRD_EN, SCR__PLL_CPU_LOCK_EN,
   SCR__PLL_CPU_LOCK_CON_REV, SCR__PLL_CPU_LOCK_CON_OUT,
   SCR__PLL_CPU_LOCK_CON_IN, SCR__PLL_CPU_LOCK_CON_DLY,
   SCR__PLL_CPU_ICP, SCR__PLL_CPU_FSEL, SCR__PLL_CPU_FOUT_MASK,
   SCR__PLL_CPU_FEED_EN, SCR__PLL_CPU_EXTAFC, SCR__PLL_CPU_BYPASS,
   SCR__PLL_CPU_AFC_ENB, SCR__PLL_CPU_AFCINIT_SEL, SCR__CLKSEL2,
   SCR__CLKSEL1, SCR__CLKSEL0, SCR__CLKDIV2, SCR__CLKDIV1,
   SCR__CLKDIV0, SCR__CLKDIS0, PSLVERR, PRDATA, PREADY,
   // Inputs
   SFR__WFI_FROM_TILE_1, SFR__WFI_FROM_TILE_0, SFR__HALT_FROM_TILE_1,
   SFR__HALT_FROM_TILE_0, SFR__DEBUG_NDRESET, SFR__DEBUG_FROM_TILE_1,
   SFR__DEBUG_FROM_TILE_0, SFR__DEBUG_DMACTIVE,
   SFR__CEASE_FROM_TILE_1, SFR__CEASE_FROM_TILE_0, SCR__VERSION3,
   SCR__VERSION2, SCR__VERSION1, SCR__VERSION0, SCR__PLL_CPU_LOCK,
   SCR__PLL_CPU_AFC_CODE, PWRITE, PWDATA, PSTRB, PSEL, PRESETn, PPROT,
   PENABLE, PCLK, PADDR, RSTACK
   );

    parameter NUM_CRG = 5;
    parameter NUM_RESET = 17;
    parameter AW=12;
    localparam DW=32;

    input [NUM_CRG-1:0] RSTACK;
    output              PREADY;
    /*AUTOINPUT*/
    // Beginning of automatic inputs (from unused autoinst inputs)
    input [AW-1:0]	PADDR;			// To x_scr_cpu of scr_cpu.v
    input		PCLK;			// To x_scr_cpu of scr_cpu.v
    input		PENABLE;		// To x_scr_cpu of scr_cpu.v
    input [2:0]		PPROT;			// To x_scr_cpu of scr_cpu.v
    input		PRESETn;		// To x_scr_cpu of scr_cpu.v
    input		PSEL;			// To x_scr_cpu of scr_cpu.v
    input [(DW/8)-1:0]	PSTRB;			// To x_scr_cpu of scr_cpu.v
    input [DW-1:0]	PWDATA;			// To x_scr_cpu of scr_cpu.v
    input		PWRITE;			// To x_scr_cpu of scr_cpu.v
    input [4:0]		SCR__PLL_CPU_AFC_CODE;	// To x_scr_cpu of scr_cpu.v
    input		SCR__PLL_CPU_LOCK;	// To x_scr_cpu of scr_cpu.v
    input [31:0]	SCR__VERSION0;		// To x_scr_cpu of scr_cpu.v
    input [31:0]	SCR__VERSION1;		// To x_scr_cpu of scr_cpu.v
    input [31:0]	SCR__VERSION2;		// To x_scr_cpu of scr_cpu.v
    input [31:0]	SCR__VERSION3;		// To x_scr_cpu of scr_cpu.v
    input		SFR__CEASE_FROM_TILE_0;	// To x_scr_cpu of scr_cpu.v
    input		SFR__CEASE_FROM_TILE_1;	// To x_scr_cpu of scr_cpu.v
    input		SFR__DEBUG_DMACTIVE;	// To x_scr_cpu of scr_cpu.v
    input		SFR__DEBUG_FROM_TILE_0;	// To x_scr_cpu of scr_cpu.v
    input		SFR__DEBUG_FROM_TILE_1;	// To x_scr_cpu of scr_cpu.v
    input		SFR__DEBUG_NDRESET;	// To x_scr_cpu of scr_cpu.v
    input		SFR__HALT_FROM_TILE_0;	// To x_scr_cpu of scr_cpu.v
    input		SFR__HALT_FROM_TILE_1;	// To x_scr_cpu of scr_cpu.v
    input		SFR__WFI_FROM_TILE_0;	// To x_scr_cpu of scr_cpu.v
    input		SFR__WFI_FROM_TILE_1;	// To x_scr_cpu of scr_cpu.v
    // End of automatics
    /*AUTOOUTPUT*/
    // Beginning of automatic outputs (from unused autoinst outputs)
    output [DW-1:0]	PRDATA;			// From x_scr_cpu of scr_cpu.v
    output		PSLVERR;		// From x_scr_cpu of scr_cpu.v
    output [16:0]	SCR__CLKDIS0;		// From x_scr_cpu of scr_cpu.v
    output [7:0]	SCR__CLKDIV0;		// From x_scr_cpu of scr_cpu.v
    output [7:0]	SCR__CLKDIV1;		// From x_scr_cpu of scr_cpu.v
    output [7:0]	SCR__CLKDIV2;		// From x_scr_cpu of scr_cpu.v
    output		SCR__CLKSEL0;		// From x_scr_cpu of scr_cpu.v
    output		SCR__CLKSEL1;		// From x_scr_cpu of scr_cpu.v
    output		SCR__CLKSEL2;		// From x_scr_cpu of scr_cpu.v
    output		SCR__PLL_CPU_AFCINIT_SEL;// From x_scr_cpu of scr_cpu.v
    output		SCR__PLL_CPU_AFC_ENB;	// From x_scr_cpu of scr_cpu.v
    output		SCR__PLL_CPU_BYPASS;	// From x_scr_cpu of scr_cpu.v
    output [4:0]	SCR__PLL_CPU_EXTAFC;	// From x_scr_cpu of scr_cpu.v
    output		SCR__PLL_CPU_FEED_EN;	// From x_scr_cpu of scr_cpu.v
    output		SCR__PLL_CPU_FOUT_MASK;	// From x_scr_cpu of scr_cpu.v
    output		SCR__PLL_CPU_FSEL;	// From x_scr_cpu of scr_cpu.v
    output [1:0]	SCR__PLL_CPU_ICP;	// From x_scr_cpu of scr_cpu.v
    output [1:0]	SCR__PLL_CPU_LOCK_CON_DLY;// From x_scr_cpu of scr_cpu.v
    output [1:0]	SCR__PLL_CPU_LOCK_CON_IN;// From x_scr_cpu of scr_cpu.v
    output [1:0]	SCR__PLL_CPU_LOCK_CON_OUT;// From x_scr_cpu of scr_cpu.v
    output [1:0]	SCR__PLL_CPU_LOCK_CON_REV;// From x_scr_cpu of scr_cpu.v
    output		SCR__PLL_CPU_LOCK_EN;	// From x_scr_cpu of scr_cpu.v
    output		SCR__PLL_CPU_LRD_EN;	// From x_scr_cpu of scr_cpu.v
    output [9:0]	SCR__PLL_CPU_M;		// From x_scr_cpu of scr_cpu.v
    output [5:0]	SCR__PLL_CPU_P;		// From x_scr_cpu of scr_cpu.v
    output		SCR__PLL_CPU_PBIAS_CTRL;// From x_scr_cpu of scr_cpu.v
    output		SCR__PLL_CPU_PBIAS_CTRL_EN;// From x_scr_cpu of scr_cpu.v
    output		SCR__PLL_CPU_RESETB;	// From x_scr_cpu of scr_cpu.v
    output [3:0]	SCR__PLL_CPU_RSEL;	// From x_scr_cpu of scr_cpu.v
    output [2:0]	SCR__PLL_CPU_S;		// From x_scr_cpu of scr_cpu.v
    output		SCR__PLL_CPU_VCO_BOOST;	// From x_scr_cpu of scr_cpu.v
    output [7:0]	SCR__RSTCYCLE0;		// From x_scr_cpu of scr_cpu.v
    output [7:0]	SCR__RSTCYCLE1;		// From x_scr_cpu of scr_cpu.v
    output [7:0]	SCR__RSTCYCLE2;		// From x_scr_cpu of scr_cpu.v
    output [7:0]	SCR__RSTCYCLE3;		// From x_scr_cpu of scr_cpu.v
    output [7:0]	SCR__RSTCYCLE4;		// From x_scr_cpu of scr_cpu.v
    output [16:0]	SCR__RSTREQN0;		// From x_scr_cpu of scr_cpu.v
    output		SFR__DEBUG_DMACTIVEACK;	// From x_scr_cpu of scr_cpu.v
    output [10:0]	SFR__DEBUG_SYSTEMJTAG_MFR_ID;// From x_scr_cpu of scr_cpu.v
    output [15:0]	SFR__DEBUG_SYSTEMJTAG_PART_NUMBER;// From x_scr_cpu of scr_cpu.v
    output [3:0]	SFR__DEBUG_SYSTEMJTAG_VERSION;// From x_scr_cpu of scr_cpu.v
    output [9:0]	SFR__MEMPORT_ARUSER;	// From x_scr_cpu of scr_cpu.v
    output [9:0]	SFR__MEMPORT_AWUSER;	// From x_scr_cpu of scr_cpu.v
    output		SFR__PSD_TEST_CLOCK_ENABLE;// From x_scr_cpu of scr_cpu.v
    output		SFR__RESETCTRL_HARTISINRESET_0;// From x_scr_cpu of scr_cpu.v
    output		SFR__RESETCTRL_HARTISINRESET_1;// From x_scr_cpu of scr_cpu.v
    output		SFR__RTC_TOGGLE;	// From x_scr_cpu of scr_cpu.v
    output		SFR__SUPPRESS_STRB_ON_CORRUPT;// From x_scr_cpu of scr_cpu.v
    output [9:0]	SFR__SYSPORT_ARUSER;	// From x_scr_cpu of scr_cpu.v
    output [9:0]	SFR__SYSPORT_AWUSER;	// From x_scr_cpu of scr_cpu.v
    // End of automatics
    wire [16:0] SCR__RSTREQN0;
    wire [31:0] SCR__RSTREQN1;
    wire [31:0] SCR__RSTREQN2;
    wire [31:0] SCR__RSTREQN3;
    /*AUTOREG*/
    // Beginning of automatic regs (for this module's undeclared outputs)
    reg			PREADY;
    // End of automatics
    /*AUTOWIRE*/

    /*scr_cpu AUTO_TEMPLATE (
	    .PREADY		(),
    );*/
    scr_cpu x_scr_cpu (/*AUTOINST*/
		       // Outputs
		       .PRDATA		(PRDATA[DW-1:0]),
		       .PREADY		(),			 // Templated
		       .PSLVERR		(PSLVERR),
		       .SCR__RSTREQN0	(SCR__RSTREQN0[16:0]),
		       .SCR__CLKDIS0	(SCR__CLKDIS0[16:0]),
		       .SCR__CLKSEL0	(SCR__CLKSEL0),
		       .SCR__CLKSEL1	(SCR__CLKSEL1),
		       .SCR__CLKSEL2	(SCR__CLKSEL2),
		       .SCR__CLKDIV0	(SCR__CLKDIV0[7:0]),
		       .SCR__CLKDIV1	(SCR__CLKDIV1[7:0]),
		       .SCR__CLKDIV2	(SCR__CLKDIV2[7:0]),
		       .SCR__RSTCYCLE0	(SCR__RSTCYCLE0[7:0]),
		       .SCR__RSTCYCLE1	(SCR__RSTCYCLE1[7:0]),
		       .SCR__RSTCYCLE2	(SCR__RSTCYCLE2[7:0]),
		       .SCR__RSTCYCLE3	(SCR__RSTCYCLE3[7:0]),
		       .SCR__RSTCYCLE4	(SCR__RSTCYCLE4[7:0]),
		       .SCR__PLL_CPU_RESETB(SCR__PLL_CPU_RESETB),
		       .SCR__PLL_CPU_P	(SCR__PLL_CPU_P[5:0]),
		       .SCR__PLL_CPU_M	(SCR__PLL_CPU_M[9:0]),
		       .SCR__PLL_CPU_BYPASS(SCR__PLL_CPU_BYPASS),
		       .SCR__PLL_CPU_S	(SCR__PLL_CPU_S[2:0]),
		       .SCR__PLL_CPU_FOUT_MASK(SCR__PLL_CPU_FOUT_MASK),
		       .SCR__PLL_CPU_LOCK_EN(SCR__PLL_CPU_LOCK_EN),
		       .SCR__PLL_CPU_LOCK_CON_IN(SCR__PLL_CPU_LOCK_CON_IN[1:0]),
		       .SCR__PLL_CPU_LOCK_CON_OUT(SCR__PLL_CPU_LOCK_CON_OUT[1:0]),
		       .SCR__PLL_CPU_LOCK_CON_DLY(SCR__PLL_CPU_LOCK_CON_DLY[1:0]),
		       .SCR__PLL_CPU_LOCK_CON_REV(SCR__PLL_CPU_LOCK_CON_REV[1:0]),
		       .SCR__PLL_CPU_FEED_EN(SCR__PLL_CPU_FEED_EN),
		       .SCR__PLL_CPU_FSEL(SCR__PLL_CPU_FSEL),
		       .SCR__PLL_CPU_RSEL(SCR__PLL_CPU_RSEL[3:0]),
		       .SCR__PLL_CPU_AFC_ENB(SCR__PLL_CPU_AFC_ENB),
		       .SCR__PLL_CPU_AFCINIT_SEL(SCR__PLL_CPU_AFCINIT_SEL),
		       .SCR__PLL_CPU_LRD_EN(SCR__PLL_CPU_LRD_EN),
		       .SCR__PLL_CPU_EXTAFC(SCR__PLL_CPU_EXTAFC[4:0]),
		       .SCR__PLL_CPU_ICP(SCR__PLL_CPU_ICP[1:0]),
		       .SCR__PLL_CPU_PBIAS_CTRL(SCR__PLL_CPU_PBIAS_CTRL),
		       .SCR__PLL_CPU_PBIAS_CTRL_EN(SCR__PLL_CPU_PBIAS_CTRL_EN),
		       .SCR__PLL_CPU_VCO_BOOST(SCR__PLL_CPU_VCO_BOOST),
		       .SFR__RTC_TOGGLE	(SFR__RTC_TOGGLE),
		       .SFR__PSD_TEST_CLOCK_ENABLE(SFR__PSD_TEST_CLOCK_ENABLE),
		       .SFR__SUPPRESS_STRB_ON_CORRUPT(SFR__SUPPRESS_STRB_ON_CORRUPT),
		       .SFR__RESETCTRL_HARTISINRESET_0(SFR__RESETCTRL_HARTISINRESET_0),
		       .SFR__RESETCTRL_HARTISINRESET_1(SFR__RESETCTRL_HARTISINRESET_1),
		       .SFR__DEBUG_DMACTIVEACK(SFR__DEBUG_DMACTIVEACK),
		       .SFR__DEBUG_SYSTEMJTAG_MFR_ID(SFR__DEBUG_SYSTEMJTAG_MFR_ID[10:0]),
		       .SFR__DEBUG_SYSTEMJTAG_VERSION(SFR__DEBUG_SYSTEMJTAG_VERSION[3:0]),
		       .SFR__DEBUG_SYSTEMJTAG_PART_NUMBER(SFR__DEBUG_SYSTEMJTAG_PART_NUMBER[15:0]),
		       .SFR__SYSPORT_AWUSER(SFR__SYSPORT_AWUSER[9:0]),
		       .SFR__SYSPORT_ARUSER(SFR__SYSPORT_ARUSER[9:0]),
		       .SFR__MEMPORT_AWUSER(SFR__MEMPORT_AWUSER[9:0]),
		       .SFR__MEMPORT_ARUSER(SFR__MEMPORT_ARUSER[9:0]),
		       // Inputs
		       .PCLK		(PCLK),
		       .PRESETn		(PRESETn),
		       .PADDR		(PADDR[AW-1:0]),
		       .PENABLE		(PENABLE),
		       .PSEL		(PSEL),
		       .PSTRB		(PSTRB[(DW/8)-1:0]),
		       .PPROT		(PPROT[02:00]),
		       .PWDATA		(PWDATA[DW-1:0]),
		       .PWRITE		(PWRITE),
		       .SCR__VERSION0	(SCR__VERSION0[31:0]),
		       .SCR__VERSION1	(SCR__VERSION1[31:0]),
		       .SCR__VERSION2	(SCR__VERSION2[31:0]),
		       .SCR__VERSION3	(SCR__VERSION3[31:0]),
		       .SCR__PLL_CPU_LOCK(SCR__PLL_CPU_LOCK),
		       .SCR__PLL_CPU_AFC_CODE(SCR__PLL_CPU_AFC_CODE[4:0]),
		       .SFR__DEBUG_NDRESET(SFR__DEBUG_NDRESET),
		       .SFR__DEBUG_DMACTIVE(SFR__DEBUG_DMACTIVE),
		       .SFR__HALT_FROM_TILE_0(SFR__HALT_FROM_TILE_0),
		       .SFR__HALT_FROM_TILE_1(SFR__HALT_FROM_TILE_1),
		       .SFR__WFI_FROM_TILE_0(SFR__WFI_FROM_TILE_0),
		       .SFR__WFI_FROM_TILE_1(SFR__WFI_FROM_TILE_1),
		       .SFR__CEASE_FROM_TILE_0(SFR__CEASE_FROM_TILE_0),
		       .SFR__CEASE_FROM_TILE_1(SFR__CEASE_FROM_TILE_1),
		       .SFR__DEBUG_FROM_TILE_0(SFR__DEBUG_FROM_TILE_0),
		       .SFR__DEBUG_FROM_TILE_1(SFR__DEBUG_FROM_TILE_1));

    reg [4:0] RSTACK_d0;
    reg [4:0] RSTACK_d1;
    reg [4:0] RSTACK_d2;
    reg [4:0] RSTACK_d3;
    always @(posedge PCLK or negedge PRESETn) begin
        if (~PRESETn) begin
            RSTACK_d0 <= {(5){1'b1}};
            RSTACK_d1 <= {(5){1'b1}};
            RSTACK_d2 <= {(5){1'b1}};
            RSTACK_d3 <= {(5){1'b1}};
        end
        else begin
            RSTACK_d0 <= RSTACK;
            RSTACK_d1 <= RSTACK_d0;
            RSTACK_d2 <= RSTACK_d1;
            RSTACK_d3 <= RSTACK_d2;
        end
    end

    wire RSTACK_RE = &RSTACK_d2 & ~&RSTACK_d3;

    wire [32*4-1:0] RSTREQN;
    reg  [32*4-1:0] RSTREQN_d;

    genvar i;
    generate
        for (i=0;i<(32*4);i=i+1) begin
            if (i<NUM_RESET) begin
                if (i<(32*1)) begin
                    assign RSTREQN[i] = SCR__RSTREQN0[i];
                end
                else if (i<(32*2)) begin
                    assign RSTREQN[i] = SCR__RSTREQN1[i-32];
                end
                else if (i<(32*3)) begin
                    assign RSTREQN[i] = SCR__RSTREQN2[i-(32*2)];
                end
                else begin
                    assign RSTREQN[i] = SCR__RSTREQN3[i-(32*3)];
                end
            end
            else begin
                assign RSTREQN[i] = 1;
            end
        end
    endgenerate
        
    wire [3:0] WE_RESET;
    reg  [3:0] WE_RESET_d;
    assign WE_RESET[0] = (PADDR[AW-1:0] == 12'h10) & PSEL & PWRITE;
    assign WE_RESET[1] = (PADDR[AW-1:0] == 12'h14) & PSEL & PWRITE;
    assign WE_RESET[2] = (PADDR[AW-1:0] == 12'h18) & PSEL & PWRITE;
    assign WE_RESET[3] = (PADDR[AW-1:0] == 12'h1C) & PSEL & PWRITE;

    // --------------------------------------------
    // PREADY Control 
    // --------------------------------------------
    // FSM
    localparam ST_IDLE = 3'd0; // Indicates FSM is stable (= progress finished)
    localparam ST_CMD  = 3'd1;// Indicates APB Command phase
    localparam ST_DATA = 3'd2;// Indicates APB write phase
    localparam ST_CMP  = 3'd3;// Indicates RSTREQN compare phase
    localparam ST_DONE = 3'd4;// Indicates transaction end phase
    reg [2:0] st_nxt, r_st;
    reg RSTREQ_changed;
    always @(posedge PCLK or negedge PRESETn) begin
        if (!PRESETn)
            r_st <= ST_IDLE;
        else
            r_st <= st_nxt;
    end
    always @(*) begin
        st_nxt = r_st;
        case(r_st)
            ST_IDLE : begin
                PREADY = 1;
                if (|WE_RESET)
                    st_nxt = ST_CMD;
            end
            ST_CMD : begin
                PREADY = 0;
                if (PENABLE)
                    st_nxt = ST_DATA;
            end
            ST_DATA : begin
                PREADY = 0;
                if (RSTREQ_changed)
                    st_nxt = ST_CMP;
                else
                    st_nxt = ST_DONE;
            end
            ST_CMP : begin
                PREADY = 0;
                if (RSTACK_RE)
                    st_nxt = ST_DONE;
            end
            ST_DONE : begin
                PREADY = 1;
                st_nxt = ST_IDLE;
            end
        endcase
    end

    // RSTREQ_changed
    always @(*) begin
        RSTREQ_changed = 1'b0;
        if (r_st==ST_DATA) begin
            if (RSTREQN_d==RSTREQN) begin
                RSTREQ_changed = 1'b0;
            end
            else begin
                RSTREQ_changed = 1'b1;
            end
        end
        else begin
            RSTREQ_changed = 1'b0;
        end
    end

    // RSTREQ backup
    always @(posedge PCLK or negedge PRESETn) begin
        if (~PRESETn) begin
            RSTREQN_d <= '1;
        end
        else if (r_st==ST_DATA) begin
            RSTREQN_d <= RSTREQN;
        end
    end
endmodule
    
// Local Variables:
// verilog-library-directories:(".")
// verilog-auto-inst-param-value:t
// verilog-library-files:("./scr_cpu.v")
// End:
// vim:tabstop=4:shiftwidth=4
