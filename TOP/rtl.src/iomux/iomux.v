module iomux_temp (
    // pad
    input  [15:0]   PAD_GPIOA_Y,
    output [15:0]   PAD_GPIOA_A,
    output [15:0]   PAD_GPIOA_OE,
    output [15:0]   PAD_GPIOA_DS0,
    output [15:0]   PAD_GPIOA_DS1,
    output [15:0]   PAD_GPIOA_IE,
    output [15:0]   PAD_GPIOA_PE,
    output [15:0]   PAD_GPIOA_PS,
    input  [15:0]   PAD_GPIOB_Y,
    output [15:0]   PAD_GPIOB_A,
    output [15:0]   PAD_GPIOB_OE,
    output [15:0]   PAD_GPIOB_DS0,
    output [15:0]   PAD_GPIOB_DS1,
    output [15:0]   PAD_GPIOB_IE,
    output [15:0]   PAD_GPIOB_PE,
    output [15:0]   PAD_GPIOB_PS,
    input  [15:0]   PAD_GPIOC_Y,
    output [15:0]   PAD_GPIOC_A,
    output [15:0]   PAD_GPIOC_OE,
    output [15:0]   PAD_GPIOC_DS0,
    output [15:0]   PAD_GPIOC_DS1,
    output [15:0]   PAD_GPIOC_IE,
    output [15:0]   PAD_GPIOC_PE,
    output [15:0]   PAD_GPIOC_PS,
    input  [15:0]   PAD_GPIOD_Y,
    output [15:0]   PAD_GPIOD_A,
    output [15:0]   PAD_GPIOD_OE,
    output [15:0]   PAD_GPIOD_DS0,
    output [15:0]   PAD_GPIOD_DS1,
    output [15:0]   PAD_GPIOD_IE,
    output [15:0]   PAD_GPIOD_PE,
    output [15:0]   PAD_GPIOD_PS,
    input  [11:0]   PAD_SDMMC_Y,
    output [11:0]   PAD_SDMMC_A,
    output [11:0]   PAD_SDMMC_OE,
    output [11:0]   PAD_SDMMC_DS0,
    output [11:0]   PAD_SDMMC_DS1,
    output [11:0]   PAD_SDMMC_IE,
    output [11:0]   PAD_SDMMC_PE,
    output [11:0]   PAD_SDMMC_PS,
    // core
    input  [15:0]   CORE_GPIOA_OE,
    input  [15:0]   CORE_GPIOA_DS0,
    input  [15:0]   CORE_GPIOA_DS1,
    input  [15:0]   CORE_GPIOA_IE,
    input  [15:0]   CORE_GPIOA_PE,
    input  [15:0]   CORE_GPIOA_PS,
    output [15:0]   CORE_GPIOA_Y,
    input  [15:0]   CORE_GPIOA_A,
    input  [15:0]   CORE_GPIOA_FNC,
    input  [15:0]   CORE_GPIOB_OE,
    input  [15:0]   CORE_GPIOB_DS0,
    input  [15:0]   CORE_GPIOB_DS1,
    input  [15:0]   CORE_GPIOB_IE,
    input  [15:0]   CORE_GPIOB_PE,
    input  [15:0]   CORE_GPIOB_PS,
    output [15:0]   CORE_GPIOB_Y,
    input  [15:0]   CORE_GPIOB_A,
    input  [15:0]   CORE_GPIOB_FNC,
    input  [15:0]   CORE_GPIOC_OE,
    input  [15:0]   CORE_GPIOC_DS0,
    input  [15:0]   CORE_GPIOC_DS1,
    input  [15:0]   CORE_GPIOC_IE,
    input  [15:0]   CORE_GPIOC_PE,
    input  [15:0]   CORE_GPIOC_PS,
    output [15:0]   CORE_GPIOC_Y,
    input  [15:0]   CORE_GPIOC_A,
    input  [15:0]   CORE_GPIOC_FNC,
    input  [15:0]   CORE_GPIOD_OE,
    input  [15:0]   CORE_GPIOD_DS0,
    input  [15:0]   CORE_GPIOD_DS1,
    input  [15:0]   CORE_GPIOD_IE,
    input  [15:0]   CORE_GPIOD_PE,
    input  [15:0]   CORE_GPIOD_PS,
    output [15:0]   CORE_GPIOD_Y,
    input  [15:0]   CORE_GPIOD_A,
    input  [15:0]   CORE_GPIOD_FNC,
    // Function IO
    output          UART_0_RX,
    input           UART_0_TX,
    output          UART_1_RX,
    input           UART_1_TX,
    output          I2C_0_SCLI,
    input           I2C_0_SCLOE,
    output          I2C_0_SDIN,
    input           I2C_0_SDOE,
    output          I2C_1_SCLI,
    input           I2C_1_SCLOE,
    output          I2C_1_SDIN,
    input           I2C_1_SDOE,
    output          I2C_2_SCLI,
    input           I2C_2_SCLOE,
    output          I2C_2_SDIN,
    input           I2C_2_SDOE,
    output          I2C_3_SCLI,
    input           I2C_3_SCLOE,
    output          I2C_3_SDIN,
    input           I2C_3_SDOE,
    input           SPI_0_SCK,
    input           SPI_0_SCS,
    input  [3:0]    SPI_0_SDO,
    output [3:0]    SPI_0_SDI,
    input  [3:0]    SPI_0_OEN,
    input           SPI_1_SCK,
    input           SPI_1_SCS,
    input  [3:0]    SPI_1_SDO,
    output [3:0]    SPI_1_SDI,
    input  [3:0]    SPI_1_OEN,
    input           SPI_2_SCK,
    input           SPI_2_SCS,
    input  [3:0]    SPI_2_SDO,
    output [3:0]    SPI_2_SDI,
    input  [3:0]    SPI_2_OEN,
    input           SPI_3_SCK,
    input           SPI_3_SCS,
    input  [3:0]    SPI_3_SDO,
    output [3:0]    SPI_3_SDI,
    input  [3:0]    SPI_3_OEN,
    input           SDMMC_0_CLK,
    input           SDMMC_0_CMD,
    output          SDMMC_0_RSP,
    input           SDMMC_0_CMDOEN,
    input  [7:0]    SDMMC_0_DOUT,
    output [7:0]    SDMMC_0_DIN,
    input  [7:0]    SDMMC_0_DOEN,
    output          SDMMC_0_DQS,
    input           SDMMC_0_RSTN
);

// default input pullup
parameter DFLT_OE = 1'b0; //output disable
parameter DFLT_IE = 1'b1; // input enable
parameter DFLT_PE = 1'b1; // pullup/down enable
parameter DFLT_PS = 1'b1; // pullup select
parameter DFLT_DS0 = 1'b1;
parameter DFLT_DS1 = 1'b1; // most strong drive strength


// GPIOA
assign CORE_GPIOA_Y = PAD_GPIOA_Y;
// GPIOA[0] : I2C_SCL
assign PAD_GPIOA_A[0]   = CORE_GPIOA_FNC[0] ? 1'b1       : CORE_GPIOA_A[0];
assign PAD_GPIOA_OE[0]  = CORE_GPIOA_FNC[0] ? I2C_0_SCLOE: CORE_GPIOA_OE[0];
assign PAD_GPIOA_IE[0]  = CORE_GPIOA_FNC[0] ? 1'b1       : CORE_GPIOA_IE[0];
assign PAD_GPIOA_PE[0]  = CORE_GPIOA_FNC[0] ? 1'b0       : CORE_GPIOA_PE[0];
assign PAD_GPIOA_PS[0]  = CORE_GPIOA_FNC[0] ? 1'b0       : CORE_GPIOA_PS[0];
assign PAD_GPIOA_DS0[0] = CORE_GPIOA_FNC[0] ? 1'b1       : CORE_GPIOA_DS0[0];
assign PAD_GPIOA_DS1[0] = CORE_GPIOA_FNC[0] ? 1'b1       : CORE_GPIOA_DS1[0];
assign I2C_0_SCLI = PAD_GPIOA_Y[0];

// GPIOA[1] : I2C_SDA
assign PAD_GPIOA_A[1]   = CORE_GPIOA_FNC[1] ? 1'b1       : CORE_GPIOA_A[1];
assign PAD_GPIOA_OE[1]  = CORE_GPIOA_FNC[1] ? I2C_0_SDOE : CORE_GPIOA_OE[1];
assign PAD_GPIOA_IE[1]  = CORE_GPIOA_FNC[1] ? 1'b1       : CORE_GPIOA_IE[1];
assign PAD_GPIOA_PE[1]  = CORE_GPIOA_FNC[1] ? 1'b0       : CORE_GPIOA_PE[1];
assign PAD_GPIOA_PS[1]  = CORE_GPIOA_FNC[1] ? 1'b0       : CORE_GPIOA_PS[1];
assign PAD_GPIOA_DS0[1] = CORE_GPIOA_FNC[1] ? 1'b1       : CORE_GPIOA_DS0[1];
assign PAD_GPIOA_DS1[1] = CORE_GPIOA_FNC[1] ? 1'b1       : CORE_GPIOA_DS1[1];
assign I2C_0_SDIN = PAD_GPIOA_Y[1];

// GPIOA[2] : I2C_SCL
assign PAD_GPIOA_A[2]   = CORE_GPIOA_FNC[2] ? 1'b1       : CORE_GPIOA_A[2];
assign PAD_GPIOA_OE[2]  = CORE_GPIOA_FNC[2] ? I2C_1_SCLOE: CORE_GPIOA_OE[2];
assign PAD_GPIOA_IE[2]  = CORE_GPIOA_FNC[2] ? 1'b1       : CORE_GPIOA_IE[2];
assign PAD_GPIOA_PE[2]  = CORE_GPIOA_FNC[2] ? 1'b0       : CORE_GPIOA_PE[2];
assign PAD_GPIOA_PS[2]  = CORE_GPIOA_FNC[2] ? 1'b0       : CORE_GPIOA_PS[2];
assign PAD_GPIOA_DS0[2] = CORE_GPIOA_FNC[2] ? 1'b1       : CORE_GPIOA_DS0[2];
assign PAD_GPIOA_DS1[2] = CORE_GPIOA_FNC[2] ? 1'b1       : CORE_GPIOA_DS1[2];
assign I2C_1_SCLI = PAD_GPIOA_Y[2];

// GPIOA[3] : I2C_SDA
assign PAD_GPIOA_A[3]   = CORE_GPIOA_FNC[3] ? 1'b1       : CORE_GPIOA_A[3];
assign PAD_GPIOA_OE[3]  = CORE_GPIOA_FNC[3] ? I2C_1_SDOE : CORE_GPIOA_OE[3];
assign PAD_GPIOA_IE[3]  = CORE_GPIOA_FNC[3] ? 1'b1       : CORE_GPIOA_IE[3];
assign PAD_GPIOA_PE[3]  = CORE_GPIOA_FNC[3] ? 1'b0       : CORE_GPIOA_PE[3];
assign PAD_GPIOA_PS[3]  = CORE_GPIOA_FNC[3] ? 1'b0       : CORE_GPIOA_PS[3];
assign PAD_GPIOA_DS0[3] = CORE_GPIOA_FNC[3] ? 1'b1       : CORE_GPIOA_DS0[3];
assign PAD_GPIOA_DS1[3] = CORE_GPIOA_FNC[3] ? 1'b1       : CORE_GPIOA_DS1[3];
assign I2C_1_SDIN = PAD_GPIOA_Y[3];

// GPIOA[4] : QSPI_SCK
assign PAD_GPIOA_A[4]   = CORE_GPIOA_FNC[4] ? SPI_0_SCK  : CORE_GPIOA_A[4];
assign PAD_GPIOA_OE[4]  = CORE_GPIOA_FNC[4] ? 1'b1      : CORE_GPIOA_OE[4];
assign PAD_GPIOA_IE[4]  = CORE_GPIOA_FNC[4] ? 1'b0      : CORE_GPIOA_IE[4];
assign PAD_GPIOA_PE[4]  = CORE_GPIOA_FNC[4] ? 1'b0      : CORE_GPIOA_PE[4];
assign PAD_GPIOA_PS[4]  = CORE_GPIOA_FNC[4] ? 1'b0      : CORE_GPIOA_PS[4];
assign PAD_GPIOA_DS0[4] = CORE_GPIOA_FNC[4] ? 1'b1      : CORE_GPIOA_DS0[4];
assign PAD_GPIOA_DS1[4] = CORE_GPIOA_FNC[4] ? 1'b1      : CORE_GPIOA_DS1[4];

// GPIOA[5] : QSPI_SCS
assign PAD_GPIOA_A[5]   = CORE_GPIOA_FNC[5] ? SPI_0_SCS  : CORE_GPIOA_A[5];
assign PAD_GPIOA_OE[5]  = CORE_GPIOA_FNC[5] ? 1'b1      : CORE_GPIOA_OE[5];
assign PAD_GPIOA_IE[5]  = CORE_GPIOA_FNC[5] ? 1'b0      : CORE_GPIOA_IE[5];
assign PAD_GPIOA_PE[5]  = CORE_GPIOA_FNC[5] ? 1'b0      : CORE_GPIOA_PE[5];
assign PAD_GPIOA_PS[5]  = CORE_GPIOA_FNC[5] ? 1'b0      : CORE_GPIOA_PS[5];
assign PAD_GPIOA_DS0[5] = CORE_GPIOA_FNC[5] ? 1'b1      : CORE_GPIOA_DS0[5];
assign PAD_GPIOA_DS1[5] = CORE_GPIOA_FNC[5] ? 1'b1      : CORE_GPIOA_DS1[5];

// GPIOA[6] : QSPI_SDIO
assign PAD_GPIOA_A[6]   = CORE_GPIOA_FNC[6] ? SPI_0_SDO[0]   : CORE_GPIOA_A[6];
assign PAD_GPIOA_OE[6]  = CORE_GPIOA_FNC[6] ? SPI_0_OEN[0]   : CORE_GPIOA_OE[6];
assign PAD_GPIOA_IE[6]  = CORE_GPIOA_FNC[6] ? 1'b1          : CORE_GPIOA_IE[6];
assign PAD_GPIOA_PE[6]  = CORE_GPIOA_FNC[6] ? 1'b0          : CORE_GPIOA_PE[6];
assign PAD_GPIOA_PS[6]  = CORE_GPIOA_FNC[6] ? 1'b0          : CORE_GPIOA_PS[6];
assign PAD_GPIOA_DS0[6] = CORE_GPIOA_FNC[6] ? 1'b1          : CORE_GPIOA_DS0[6];
assign PAD_GPIOA_DS1[6] = CORE_GPIOA_FNC[6] ? 1'b1          : CORE_GPIOA_DS1[6];
assign SPI_0_SDI[0] = PAD_GPIOA_Y[6];

// GPIOA[7] : QSPI_SDIO
assign PAD_GPIOA_A[7]   = CORE_GPIOA_FNC[7] ? SPI_0_SDO[1]   : CORE_GPIOA_A[7];
assign PAD_GPIOA_OE[7]  = CORE_GPIOA_FNC[7] ? SPI_0_OEN[1]   : CORE_GPIOA_OE[7];
assign PAD_GPIOA_IE[7]  = CORE_GPIOA_FNC[7] ? 1'b1          : CORE_GPIOA_IE[7];
assign PAD_GPIOA_PE[7]  = CORE_GPIOA_FNC[7] ? 1'b0          : CORE_GPIOA_PE[7];
assign PAD_GPIOA_PS[7]  = CORE_GPIOA_FNC[7] ? 1'b0          : CORE_GPIOA_PS[7];
assign PAD_GPIOA_DS0[7] = CORE_GPIOA_FNC[7] ? 1'b1          : CORE_GPIOA_DS0[7];
assign PAD_GPIOA_DS1[7] = CORE_GPIOA_FNC[7] ? 1'b1          : CORE_GPIOA_DS1[7];
assign SPI_0_SDI[1] = PAD_GPIOA_Y[7];

// GPIOA[8] : QSPI_SDIO
assign PAD_GPIOA_A[8]   = CORE_GPIOA_FNC[8] ? SPI_0_SDO[2]   : CORE_GPIOA_A[8];
assign PAD_GPIOA_OE[8]  = CORE_GPIOA_FNC[8] ? SPI_0_OEN[2]   : CORE_GPIOA_OE[8];
assign PAD_GPIOA_IE[8]  = CORE_GPIOA_FNC[8] ? 1'b1          : CORE_GPIOA_IE[8];
assign PAD_GPIOA_PE[8]  = CORE_GPIOA_FNC[8] ? 1'b0          : CORE_GPIOA_PE[8];
assign PAD_GPIOA_PS[8]  = CORE_GPIOA_FNC[8] ? 1'b0          : CORE_GPIOA_PS[8];
assign PAD_GPIOA_DS0[8] = CORE_GPIOA_FNC[8] ? 1'b1          : CORE_GPIOA_DS0[8];
assign PAD_GPIOA_DS1[8] = CORE_GPIOA_FNC[8] ? 1'b1          : CORE_GPIOA_DS1[8];
assign SPI_0_SDI[2] = PAD_GPIOA_Y[8];

// GPIOA[9] : QSPI_SDIO
assign PAD_GPIOA_A[9]   = CORE_GPIOA_FNC[9] ? SPI_0_SDO[3]   : CORE_GPIOA_A[9];
assign PAD_GPIOA_OE[9]  = CORE_GPIOA_FNC[9] ? SPI_0_OEN[3]   : CORE_GPIOA_OE[9];
assign PAD_GPIOA_IE[9]  = CORE_GPIOA_FNC[9] ? 1'b1          : CORE_GPIOA_IE[9];
assign PAD_GPIOA_PE[9]  = CORE_GPIOA_FNC[9] ? 1'b0          : CORE_GPIOA_PE[9];
assign PAD_GPIOA_PS[9]  = CORE_GPIOA_FNC[9] ? 1'b0          : CORE_GPIOA_PS[9];
assign PAD_GPIOA_DS0[9] = CORE_GPIOA_FNC[9] ? 1'b1          : CORE_GPIOA_DS0[9];
assign PAD_GPIOA_DS1[9] = CORE_GPIOA_FNC[9] ? 1'b1          : CORE_GPIOA_DS1[9];
assign SPI_0_SDI[3] = PAD_GPIOA_Y[9];

// GPIOA[10] : SPI_SCK
assign PAD_GPIOA_A[10]   = CORE_GPIOA_FNC[10] ? SPI_1_SCK  : CORE_GPIOA_A[10];
assign PAD_GPIOA_OE[10]  = CORE_GPIOA_FNC[10] ? 1'b1      : CORE_GPIOA_OE[10];
assign PAD_GPIOA_IE[10]  = CORE_GPIOA_FNC[10] ? 1'b0      : CORE_GPIOA_IE[10];
assign PAD_GPIOA_PE[10]  = CORE_GPIOA_FNC[10] ? 1'b0      : CORE_GPIOA_PE[10];
assign PAD_GPIOA_PS[10]  = CORE_GPIOA_FNC[10] ? 1'b0      : CORE_GPIOA_PS[10];
assign PAD_GPIOA_DS0[10] = CORE_GPIOA_FNC[10] ? 1'b1      : CORE_GPIOA_DS0[10];
assign PAD_GPIOA_DS1[10] = CORE_GPIOA_FNC[10] ? 1'b1      : CORE_GPIOA_DS1[10];

// GPIOA[11] : SPI_SCS
assign PAD_GPIOA_A[11]   = CORE_GPIOA_FNC[11] ? SPI_1_SCS  : CORE_GPIOA_A[11];
assign PAD_GPIOA_OE[11]  = CORE_GPIOA_FNC[11] ? 1'b1      : CORE_GPIOA_OE[11];
assign PAD_GPIOA_IE[11]  = CORE_GPIOA_FNC[11] ? 1'b0      : CORE_GPIOA_IE[11];
assign PAD_GPIOA_PE[11]  = CORE_GPIOA_FNC[11] ? 1'b0      : CORE_GPIOA_PE[11];
assign PAD_GPIOA_PS[11]  = CORE_GPIOA_FNC[11] ? 1'b0      : CORE_GPIOA_PS[11];
assign PAD_GPIOA_DS0[11] = CORE_GPIOA_FNC[11] ? 1'b1      : CORE_GPIOA_DS0[11];
assign PAD_GPIOA_DS1[11] = CORE_GPIOA_FNC[11] ? 1'b1      : CORE_GPIOA_DS1[11];

// GPIOA[12] : SPI_SDO 
assign PAD_GPIOA_A[12]   = CORE_GPIOA_FNC[12] ? SPI_1_SDO[0]   : CORE_GPIOA_A[12];
assign PAD_GPIOA_OE[12]  = CORE_GPIOA_FNC[12] ? SPI_1_OEN[0]  : CORE_GPIOA_OE[12];
assign PAD_GPIOA_IE[12]  = CORE_GPIOA_FNC[12] ? 1'b1          : CORE_GPIOA_IE[12];
assign PAD_GPIOA_PE[12]  = CORE_GPIOA_FNC[12] ? 1'b0          : CORE_GPIOA_PE[12];
assign PAD_GPIOA_PS[12]  = CORE_GPIOA_FNC[12] ? 1'b0          : CORE_GPIOA_PS[12];
assign PAD_GPIOA_DS0[12] = CORE_GPIOA_FNC[12] ? 1'b1          : CORE_GPIOA_DS0[12];
assign PAD_GPIOA_DS1[12] = CORE_GPIOA_FNC[12] ? 1'b1          : CORE_GPIOA_DS1[12];

// GPIOA[13] : SPI_SDI
assign PAD_GPIOA_A[13]   = CORE_GPIOA_FNC[13] ? 1'b0          : CORE_GPIOA_A[13];
assign PAD_GPIOA_OE[13]  = CORE_GPIOA_FNC[13] ? 1'b0          : CORE_GPIOA_OE[13];
assign PAD_GPIOA_IE[13]  = CORE_GPIOA_FNC[13] ? 1'b1          : CORE_GPIOA_IE[13];
assign PAD_GPIOA_PE[13]  = CORE_GPIOA_FNC[13] ? 1'b0          : CORE_GPIOA_PE[13];
assign PAD_GPIOA_PS[13]  = CORE_GPIOA_FNC[13] ? 1'b0          : CORE_GPIOA_PS[13];
assign PAD_GPIOA_DS0[13] = CORE_GPIOA_FNC[13] ? 1'b1          : CORE_GPIOA_DS0[13];
assign PAD_GPIOA_DS1[13] = CORE_GPIOA_FNC[13] ? 1'b1          : CORE_GPIOA_DS1[13];
assign SPI_1_SDI[0] = PAD_GPIOA_Y[13];
assign SPI_1_SDI[3:1] = 0;

// GPIOA[14] : UART_TX
assign PAD_GPIOA_A[14]   = CORE_GPIOA_FNC[14] ? UART_0_TX      : CORE_GPIOA_A[14];
assign PAD_GPIOA_OE[14]  = CORE_GPIOA_FNC[14] ? 1'b1          : CORE_GPIOA_OE[14];
assign PAD_GPIOA_IE[14]  = CORE_GPIOA_FNC[14] ? 1'b0          : CORE_GPIOA_IE[14];
assign PAD_GPIOA_PE[14]  = CORE_GPIOA_FNC[14] ? 1'b0          : CORE_GPIOA_PE[14];
assign PAD_GPIOA_PS[14]  = CORE_GPIOA_FNC[14] ? 1'b0          : CORE_GPIOA_PS[14];
assign PAD_GPIOA_DS0[14] = CORE_GPIOA_FNC[14] ? 1'b1          : CORE_GPIOA_DS0[14];
assign PAD_GPIOA_DS1[14] = CORE_GPIOA_FNC[14] ? 1'b1          : CORE_GPIOA_DS1[14];

// GPIOA[15] : UART_RX
assign PAD_GPIOA_A[15]   = CORE_GPIOA_FNC[15] ? 1'b0          : CORE_GPIOA_A[15];
assign PAD_GPIOA_OE[15]  = CORE_GPIOA_FNC[15] ? 1'b0          : CORE_GPIOA_OE[15];
assign PAD_GPIOA_IE[15]  = CORE_GPIOA_FNC[15] ? 1'b1          : CORE_GPIOA_IE[15];
assign PAD_GPIOA_PE[15]  = CORE_GPIOA_FNC[15] ? 1'b0          : CORE_GPIOA_PE[15];
assign PAD_GPIOA_PS[15]  = CORE_GPIOA_FNC[15] ? 1'b0          : CORE_GPIOA_PS[15];
assign PAD_GPIOA_DS0[15] = CORE_GPIOA_FNC[15] ? 1'b1          : CORE_GPIOA_DS0[15];
assign PAD_GPIOA_DS1[15] = CORE_GPIOA_FNC[15] ? 1'b1          : CORE_GPIOA_DS1[15];
assign UART_0_RX = PAD_GPIOA_Y[15];

// GPIOB[0] : UART_TX
assign PAD_GPIOB_A[0]   = CORE_GPIOB_FNC[0] ? UART_1_TX      : CORE_GPIOB_A[0];
assign PAD_GPIOB_OE[0]  = CORE_GPIOB_FNC[0] ? 1'b1          : CORE_GPIOB_OE[0];
assign PAD_GPIOB_IE[0]  = CORE_GPIOB_FNC[0] ? 1'b0          : CORE_GPIOB_IE[0];
assign PAD_GPIOB_PE[0]  = CORE_GPIOB_FNC[0] ? 1'b0          : CORE_GPIOB_PE[0];
assign PAD_GPIOB_PS[0]  = CORE_GPIOB_FNC[0] ? 1'b0          : CORE_GPIOB_PS[0];
assign PAD_GPIOB_DS0[0] = CORE_GPIOB_FNC[0] ? 1'b1          : CORE_GPIOB_DS0[0];
assign PAD_GPIOB_DS1[0] = CORE_GPIOB_FNC[0] ? 1'b1          : CORE_GPIOB_DS1[0];

// GPIOB[1] : UART_RX
assign PAD_GPIOB_A[1]   = CORE_GPIOB_FNC[1] ? 1'b0          : CORE_GPIOB_A[1];
assign PAD_GPIOB_OE[1]  = CORE_GPIOB_FNC[1] ? 1'b0          : CORE_GPIOB_OE[1];
assign PAD_GPIOB_IE[1]  = CORE_GPIOB_FNC[1] ? 1'b1          : CORE_GPIOB_IE[1];
assign PAD_GPIOB_PE[1]  = CORE_GPIOB_FNC[1] ? 1'b0          : CORE_GPIOB_PE[1];
assign PAD_GPIOB_PS[1]  = CORE_GPIOB_FNC[1] ? 1'b0          : CORE_GPIOB_PS[1];
assign PAD_GPIOB_DS0[1] = CORE_GPIOB_FNC[1] ? 1'b1          : CORE_GPIOB_DS0[1];
assign PAD_GPIOB_DS1[1] = CORE_GPIOB_FNC[1] ? 1'b1          : CORE_GPIOB_DS1[1];
assign UART_1_RX = PAD_GPIOB_Y[1];

// GPIOB[2] : SPI_SCK
assign PAD_GPIOB_A[2]   = CORE_GPIOB_FNC[2] ? SPI_2_SCK  : CORE_GPIOB_A[2];
assign PAD_GPIOB_OE[2]  = CORE_GPIOB_FNC[2] ? 1'b1      : CORE_GPIOB_OE[2];
assign PAD_GPIOB_IE[2]  = CORE_GPIOB_FNC[2] ? 1'b0      : CORE_GPIOB_IE[2];
assign PAD_GPIOB_PE[2]  = CORE_GPIOB_FNC[2] ? 1'b0      : CORE_GPIOB_PE[2];
assign PAD_GPIOB_PS[2]  = CORE_GPIOB_FNC[2] ? 1'b0      : CORE_GPIOB_PS[2];
assign PAD_GPIOB_DS0[2] = CORE_GPIOB_FNC[2] ? 1'b1      : CORE_GPIOB_DS0[2];
assign PAD_GPIOB_DS1[2] = CORE_GPIOB_FNC[2] ? 1'b1      : CORE_GPIOB_DS1[2];

// GPIOB[3] : SPI_SCS
assign PAD_GPIOB_A[3]   = CORE_GPIOB_FNC[3] ? SPI_2_SCS  : CORE_GPIOB_A[3];
assign PAD_GPIOB_OE[3]  = CORE_GPIOB_FNC[3] ? 1'b1      : CORE_GPIOB_OE[3];
assign PAD_GPIOB_IE[3]  = CORE_GPIOB_FNC[3] ? 1'b0      : CORE_GPIOB_IE[3];
assign PAD_GPIOB_PE[3]  = CORE_GPIOB_FNC[3] ? 1'b0      : CORE_GPIOB_PE[3];
assign PAD_GPIOB_PS[3]  = CORE_GPIOB_FNC[3] ? 1'b0      : CORE_GPIOB_PS[3];
assign PAD_GPIOB_DS0[3] = CORE_GPIOB_FNC[3] ? 1'b1      : CORE_GPIOB_DS0[3];
assign PAD_GPIOB_DS1[3] = CORE_GPIOB_FNC[3] ? 1'b1      : CORE_GPIOB_DS1[3];

// GPIOB[4] : SPI_SDO 
assign PAD_GPIOB_A[4]   = CORE_GPIOB_FNC[4] ? SPI_2_SDO[0]  : CORE_GPIOB_A[4];
assign PAD_GPIOB_OE[4]  = CORE_GPIOB_FNC[4] ? SPI_2_OEN[0]  : CORE_GPIOB_OE[4];
assign PAD_GPIOB_IE[4]  = CORE_GPIOB_FNC[4] ? 1'b0          : CORE_GPIOB_IE[4];
assign PAD_GPIOB_PE[4]  = CORE_GPIOB_FNC[4] ? 1'b0          : CORE_GPIOB_PE[4];
assign PAD_GPIOB_PS[4]  = CORE_GPIOB_FNC[4] ? 1'b0          : CORE_GPIOB_PS[4];
assign PAD_GPIOB_DS0[4] = CORE_GPIOB_FNC[4] ? 1'b1          : CORE_GPIOB_DS0[4];
assign PAD_GPIOB_DS1[4] = CORE_GPIOB_FNC[4] ? 1'b1          : CORE_GPIOB_DS1[4];

// GPIOB[5] : SPI_SDI
assign PAD_GPIOB_A[5]   = CORE_GPIOB_FNC[5] ? 1'b0          : CORE_GPIOB_A[5];
assign PAD_GPIOB_OE[5]  = CORE_GPIOB_FNC[5] ? 1'b0          : CORE_GPIOB_OE[5];
assign PAD_GPIOB_IE[5]  = CORE_GPIOB_FNC[5] ? 1'b1          : CORE_GPIOB_IE[5];
assign PAD_GPIOB_PE[5]  = CORE_GPIOB_FNC[5] ? 1'b0          : CORE_GPIOB_PE[5];
assign PAD_GPIOB_PS[5]  = CORE_GPIOB_FNC[5] ? 1'b0          : CORE_GPIOB_PS[5];
assign PAD_GPIOB_DS0[5] = CORE_GPIOB_FNC[5] ? 1'b1          : CORE_GPIOB_DS0[5];
assign PAD_GPIOB_DS1[5] = CORE_GPIOB_FNC[5] ? 1'b1          : CORE_GPIOB_DS1[5];
assign SPI_2_SDI[0] = PAD_GPIOB_Y[5];
assign SPI_2_SDI[3:1] = 0;

// GPIOB[6] : SPI_SCK
assign PAD_GPIOB_A[6]   = CORE_GPIOB_FNC[6] ? SPI_3_SCK  : CORE_GPIOB_A[6];
assign PAD_GPIOB_OE[6]  = CORE_GPIOB_FNC[6] ? 1'b1      : CORE_GPIOB_OE[6];
assign PAD_GPIOB_IE[6]  = CORE_GPIOB_FNC[6] ? 1'b0      : CORE_GPIOB_IE[6];
assign PAD_GPIOB_PE[6]  = CORE_GPIOB_FNC[6] ? 1'b0      : CORE_GPIOB_PE[6];
assign PAD_GPIOB_PS[6]  = CORE_GPIOB_FNC[6] ? 1'b0      : CORE_GPIOB_PS[6];
assign PAD_GPIOB_DS0[6] = CORE_GPIOB_FNC[6] ? 1'b1      : CORE_GPIOB_DS0[6];
assign PAD_GPIOB_DS1[6] = CORE_GPIOB_FNC[6] ? 1'b1      : CORE_GPIOB_DS1[6];

// GPIOB[7] : SPI_SCS
assign PAD_GPIOB_A[7]   = CORE_GPIOB_FNC[7] ? SPI_3_SCS  : CORE_GPIOB_A[7];
assign PAD_GPIOB_OE[7]  = CORE_GPIOB_FNC[7] ? 1'b1      : CORE_GPIOB_OE[7];
assign PAD_GPIOB_IE[7]  = CORE_GPIOB_FNC[7] ? 1'b0      : CORE_GPIOB_IE[7];
assign PAD_GPIOB_PE[7]  = CORE_GPIOB_FNC[7] ? 1'b0      : CORE_GPIOB_PE[7];
assign PAD_GPIOB_PS[7]  = CORE_GPIOB_FNC[7] ? 1'b0      : CORE_GPIOB_PS[7];
assign PAD_GPIOB_DS0[7] = CORE_GPIOB_FNC[7] ? 1'b1      : CORE_GPIOB_DS0[7];
assign PAD_GPIOB_DS1[7] = CORE_GPIOB_FNC[7] ? 1'b1      : CORE_GPIOB_DS1[7];

// GPIOB[8] : SPI_SDO 
assign PAD_GPIOB_A[8]   = CORE_GPIOB_FNC[8] ? SPI_3_SDO[0]  : CORE_GPIOB_A[8];
assign PAD_GPIOB_OE[8]  = CORE_GPIOB_FNC[8] ? SPI_3_OEN[0]  : CORE_GPIOB_OE[8];
assign PAD_GPIOB_IE[8]  = CORE_GPIOB_FNC[8] ? 1'b0          : CORE_GPIOB_IE[8];
assign PAD_GPIOB_PE[8]  = CORE_GPIOB_FNC[8] ? 1'b0          : CORE_GPIOB_PE[8];
assign PAD_GPIOB_PS[8]  = CORE_GPIOB_FNC[8] ? 1'b0          : CORE_GPIOB_PS[8];
assign PAD_GPIOB_DS0[8] = CORE_GPIOB_FNC[8] ? 1'b1          : CORE_GPIOB_DS0[8];
assign PAD_GPIOB_DS1[8] = CORE_GPIOB_FNC[8] ? 1'b1          : CORE_GPIOB_DS1[8];

// GPIOB[9] : SPI_SDI
assign PAD_GPIOB_A[9]   = CORE_GPIOB_FNC[9] ? 1'b0          : CORE_GPIOB_A[9];
assign PAD_GPIOB_OE[9]  = CORE_GPIOB_FNC[9] ? 1'b0          : CORE_GPIOB_OE[9];
assign PAD_GPIOB_IE[9]  = CORE_GPIOB_FNC[9] ? 1'b1          : CORE_GPIOB_IE[9];
assign PAD_GPIOB_PE[9]  = CORE_GPIOB_FNC[9] ? 1'b0          : CORE_GPIOB_PE[9];
assign PAD_GPIOB_PS[9]  = CORE_GPIOB_FNC[9] ? 1'b0          : CORE_GPIOB_PS[9];
assign PAD_GPIOB_DS0[9] = CORE_GPIOB_FNC[9] ? 1'b1          : CORE_GPIOB_DS0[9];
assign PAD_GPIOB_DS1[9] = CORE_GPIOB_FNC[9] ? 1'b1          : CORE_GPIOB_DS1[9];
assign SPI_3_SDI[0] = PAD_GPIOB_Y[9];
assign SPI_3_SDI[3:1] = 0;

// GPIOB[10] : I2C_SCL
assign PAD_GPIOB_A[10]   = CORE_GPIOB_FNC[10] ? 1'b1        : CORE_GPIOB_A[10];
assign PAD_GPIOB_OE[10]  = CORE_GPIOB_FNC[10] ? I2C_2_SCLOE : CORE_GPIOB_OE[10];
assign PAD_GPIOB_IE[10]  = CORE_GPIOB_FNC[10] ? 1'b1        : CORE_GPIOB_IE[10];
assign PAD_GPIOB_PE[10]  = CORE_GPIOB_FNC[10] ? 1'b0        : CORE_GPIOB_PE[10];
assign PAD_GPIOB_PS[10]  = CORE_GPIOB_FNC[10] ? 1'b0        : CORE_GPIOB_PS[10];
assign PAD_GPIOB_DS0[10] = CORE_GPIOB_FNC[10] ? 1'b1        : CORE_GPIOB_DS0[10];
assign PAD_GPIOB_DS1[10] = CORE_GPIOB_FNC[10] ? 1'b1        : CORE_GPIOB_DS1[10];
assign I2C_2_SCLI = PAD_GPIOB_Y[10];

// GPIOB[11] : I2C_SDA
assign PAD_GPIOB_A[11]   = CORE_GPIOB_FNC[11] ? 1'b1        : CORE_GPIOB_A[11];
assign PAD_GPIOB_OE[11]  = CORE_GPIOB_FNC[11] ? I2C_2_SDOE  : CORE_GPIOB_OE[11];
assign PAD_GPIOB_IE[11]  = CORE_GPIOB_FNC[11] ? 1'b1        : CORE_GPIOB_IE[11];
assign PAD_GPIOB_PE[11]  = CORE_GPIOB_FNC[11] ? 1'b0        : CORE_GPIOB_PE[11];
assign PAD_GPIOB_PS[11]  = CORE_GPIOB_FNC[11] ? 1'b0        : CORE_GPIOB_PS[11];
assign PAD_GPIOB_DS0[11] = CORE_GPIOB_FNC[11] ? 1'b1        : CORE_GPIOB_DS0[11];
assign PAD_GPIOB_DS1[11] = CORE_GPIOB_FNC[11] ? 1'b1        : CORE_GPIOB_DS1[11];
assign I2C_2_SDIN = PAD_GPIOB_Y[11];

// GPIOB[12] : I2C_SCL
assign PAD_GPIOB_A[12]   = CORE_GPIOB_FNC[12] ? 1'b1        : CORE_GPIOB_A[12];
assign PAD_GPIOB_OE[12]  = CORE_GPIOB_FNC[12] ? I2C_3_SCLOE : CORE_GPIOB_OE[12];
assign PAD_GPIOB_IE[12]  = CORE_GPIOB_FNC[12] ? 1'b1        : CORE_GPIOB_IE[12];
assign PAD_GPIOB_PE[12]  = CORE_GPIOB_FNC[12] ? 1'b0        : CORE_GPIOB_PE[12];
assign PAD_GPIOB_PS[12]  = CORE_GPIOB_FNC[12] ? 1'b0        : CORE_GPIOB_PS[12];
assign PAD_GPIOB_DS0[12] = CORE_GPIOB_FNC[12] ? 1'b1        : CORE_GPIOB_DS0[12];
assign PAD_GPIOB_DS1[12] = CORE_GPIOB_FNC[12] ? 1'b1        : CORE_GPIOB_DS1[12];
assign I2C_3_SCLI = PAD_GPIOB_Y[12];

// GPIOB[13] : I2C_SDA
assign PAD_GPIOB_A[13]   = CORE_GPIOB_FNC[13] ? 1'b1        : CORE_GPIOB_A[13];
assign PAD_GPIOB_OE[13]  = CORE_GPIOB_FNC[13] ? I2C_3_SDOE  : CORE_GPIOB_OE[13];
assign PAD_GPIOB_IE[13]  = CORE_GPIOB_FNC[13] ? 1'b1        : CORE_GPIOB_IE[13];
assign PAD_GPIOB_PE[13]  = CORE_GPIOB_FNC[13] ? 1'b0        : CORE_GPIOB_PE[13];
assign PAD_GPIOB_PS[13]  = CORE_GPIOB_FNC[13] ? 1'b0        : CORE_GPIOB_PS[13];
assign PAD_GPIOB_DS0[13] = CORE_GPIOB_FNC[13] ? 1'b1        : CORE_GPIOB_DS0[13];
assign PAD_GPIOB_DS1[13] = CORE_GPIOB_FNC[13] ? 1'b1        : CORE_GPIOB_DS1[13];
assign I2C_3_SDIN = PAD_GPIOB_Y[13];

// GPIOB[15:14] : GPIO
assign PAD_GPIOB_A[15:14]   = CORE_GPIOB_A[15:14];
assign PAD_GPIOB_OE[15:14]  = CORE_GPIOB_OE[15:14];
assign PAD_GPIOB_IE[15:14]  = CORE_GPIOB_IE[15:14];
assign PAD_GPIOB_PE[15:14]  = CORE_GPIOB_PE[15:14];
assign PAD_GPIOB_PS[15:14]  = CORE_GPIOB_PS[15:14];
assign PAD_GPIOB_DS0[15:14] = CORE_GPIOB_DS0[15:14];
assign PAD_GPIOB_DS1[15:14] = CORE_GPIOB_DS1[15:14];

// GPIOC[15:0] : GPIO
assign PAD_GPIOC_A[15:0]   = CORE_GPIOC_A[15:0];
assign PAD_GPIOC_OE[15:0]  = CORE_GPIOC_OE[15:0];
assign PAD_GPIOC_IE[15:0]  = CORE_GPIOC_IE[15:0];
assign PAD_GPIOC_PE[15:0]  = CORE_GPIOC_PE[15:0];
assign PAD_GPIOC_PS[15:0]  = CORE_GPIOC_PS[15:0];
assign PAD_GPIOC_DS0[15:0] = CORE_GPIOC_DS0[15:0];
assign PAD_GPIOC_DS1[15:0] = CORE_GPIOC_DS1[15:0];

// GPIOD[15:0] : GPIO
assign PAD_GPIOD_A[15:0]   = CORE_GPIOD_A[15:0];
assign PAD_GPIOD_OE[15:0]  = CORE_GPIOD_OE[15:0];
assign PAD_GPIOD_IE[15:0]  = CORE_GPIOD_IE[15:0];
assign PAD_GPIOD_PE[15:0]  = CORE_GPIOD_PE[15:0];
assign PAD_GPIOD_PS[15:0]  = CORE_GPIOD_PS[15:0];
assign PAD_GPIOD_DS0[15:0] = CORE_GPIOD_DS0[15:0];
assign PAD_GPIOD_DS1[15:0] = CORE_GPIOD_DS1[15:0];

// SDMMC[0] : CLK
assign PAD_SDMMC_A[0]   = SDMMC_0_CLK;
assign PAD_SDMMC_OE[0]  = 1'b1;
assign PAD_SDMMC_IE[0]  = 1'b0;
assign PAD_SDMMC_PE[0]  = 1'b0;
assign PAD_SDMMC_PS[0]  = 1'b0;
assign PAD_SDMMC_DS0[0] = 1'b1;
assign PAD_SDMMC_DS1[0] = 1'b1;

// SDMMC[1] : CMD/RSP
assign PAD_SDMMC_A[1]   = SDMMC_0_CMD;
assign PAD_SDMMC_OE[1]  = SDMMC_0_CMDOEN;
assign PAD_SDMMC_IE[1]  = 1'b1;
assign PAD_SDMMC_PE[1]  = 1'b0;
assign PAD_SDMMC_PS[1]  = 1'b0;
assign PAD_SDMMC_DS0[1] = 1'b1;
assign PAD_SDMMC_DS1[1] = 1'b1;
assign SDMMC_0_RSP = PAD_SDMMC_Y[1];

// SDMMC[2] : SD0
assign PAD_SDMMC_A[2]   = SDMMC_0_DOUT[0];
assign PAD_SDMMC_OE[2]  = SDMMC_0_DOEN[0];
assign PAD_SDMMC_IE[2]  = 1'b1;
assign PAD_SDMMC_PE[2]  = 1'b0;
assign PAD_SDMMC_PS[2]  = 1'b0;
assign PAD_SDMMC_DS0[2] = 1'b1;
assign PAD_SDMMC_DS1[2] = 1'b1;
assign SDMMC_0_DIN[0] = PAD_SDMMC_Y[2];

// SDMMC[3] : SD1
assign PAD_SDMMC_A[3]   = SDMMC_0_DOUT[1];
assign PAD_SDMMC_OE[3]  = SDMMC_0_DOEN[1];
assign PAD_SDMMC_IE[3]  = 1'b1;
assign PAD_SDMMC_PE[3]  = 1'b0;
assign PAD_SDMMC_PS[3]  = 1'b0;
assign PAD_SDMMC_DS0[3] = 1'b1;
assign PAD_SDMMC_DS1[3] = 1'b1;
assign SDMMC_0_DIN[1] = PAD_SDMMC_Y[3];

// SDMMC[4] : SD2
assign PAD_SDMMC_A[4]   = SDMMC_0_DOUT[2];
assign PAD_SDMMC_OE[4]  = SDMMC_0_DOEN[2];
assign PAD_SDMMC_IE[4]  = 1'b1;
assign PAD_SDMMC_PE[4]  = 1'b0;
assign PAD_SDMMC_PS[4]  = 1'b0;
assign PAD_SDMMC_DS0[4] = 1'b1;
assign PAD_SDMMC_DS1[4] = 1'b1;
assign SDMMC_0_DIN[2] = PAD_SDMMC_Y[4];

// SDMMC[5] : SD3
assign PAD_SDMMC_A[5]   = SDMMC_0_DOUT[3];
assign PAD_SDMMC_OE[5]  = SDMMC_0_DOEN[3];
assign PAD_SDMMC_IE[5]  = 1'b1;
assign PAD_SDMMC_PE[5]  = 1'b0;
assign PAD_SDMMC_PS[5]  = 1'b0;
assign PAD_SDMMC_DS0[5] = 1'b1;
assign PAD_SDMMC_DS1[5] = 1'b1;
assign SDMMC_0_DIN[3] = PAD_SDMMC_Y[5];

// SDMMC[6] : SD4
assign PAD_SDMMC_A[6]   = SDMMC_0_DOUT[4];
assign PAD_SDMMC_OE[6]  = SDMMC_0_DOEN[4];
assign PAD_SDMMC_IE[6]  = 1'b1;
assign PAD_SDMMC_PE[6]  = 1'b0;
assign PAD_SDMMC_PS[6]  = 1'b0;
assign PAD_SDMMC_DS0[6] = 1'b1;
assign PAD_SDMMC_DS1[6] = 1'b1;
assign SDMMC_0_DIN[4] = PAD_SDMMC_Y[6];

// SDMMC[7] : SD5
assign PAD_SDMMC_A[7]   = SDMMC_0_DOUT[5];
assign PAD_SDMMC_OE[7]  = SDMMC_0_DOEN[5];
assign PAD_SDMMC_IE[7]  = 1'b1;
assign PAD_SDMMC_PE[7]  = 1'b0;
assign PAD_SDMMC_PS[7]  = 1'b0;
assign PAD_SDMMC_DS0[7] = 1'b1;
assign PAD_SDMMC_DS1[7] = 1'b1;
assign SDMMC_0_DIN[5] = PAD_SDMMC_Y[7];

// SDMMC[8] : SD6
assign PAD_SDMMC_A[8]   = SDMMC_0_DOUT[6];
assign PAD_SDMMC_OE[8]  = SDMMC_0_DOEN[6];
assign PAD_SDMMC_IE[8]  = 1'b1;
assign PAD_SDMMC_PE[8]  = 1'b0;
assign PAD_SDMMC_PS[8]  = 1'b0;
assign PAD_SDMMC_DS0[8] = 1'b1;
assign PAD_SDMMC_DS1[8] = 1'b1;
assign SDMMC_0_DIN[6] = PAD_SDMMC_Y[8];

// SDMMC[9] : SD7
assign PAD_SDMMC_A[9]   = SDMMC_0_DOUT[7];
assign PAD_SDMMC_OE[9]  = SDMMC_0_DOEN[7];
assign PAD_SDMMC_IE[9]  = 1'b1;
assign PAD_SDMMC_PE[9]  = 1'b0;
assign PAD_SDMMC_PS[9]  = 1'b0;
assign PAD_SDMMC_DS0[9] = 1'b1;
assign PAD_SDMMC_DS1[9] = 1'b1;
assign SDMMC_0_DIN[7] = PAD_SDMMC_Y[9];

// SDMMC[10] : DQS
assign PAD_SDMMC_A[10]   = 1'b0;
assign PAD_SDMMC_OE[10]  = 1'b0;
assign PAD_SDMMC_IE[10]  = 1'b1;
assign PAD_SDMMC_PE[10]  = 1'b0;
assign PAD_SDMMC_PS[10]  = 1'b0;
assign PAD_SDMMC_DS0[10] = 1'b1;
assign PAD_SDMMC_DS1[10] = 1'b1;
assign SDMMC_0_DQS = PAD_SDMMC_Y[10];

// SDMMC[11] : RSTN
assign PAD_SDMMC_A[11]   = SDMMC_0_RSTN;
assign PAD_SDMMC_OE[11]  = 1'b1;
assign PAD_SDMMC_IE[11]  = 1'b0;
assign PAD_SDMMC_PE[11]  = 1'b0;
assign PAD_SDMMC_PS[11]  = 1'b0;
assign PAD_SDMMC_DS0[11] = 1'b1;
assign PAD_SDMMC_DS1[11] = 1'b1;

endmodule
