//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_83_assert(
  input         clock,
  input         io_in_a_ready,
  input         io_in_a_valid,
  input  [2:0]  io_in_a_bits_opcode,
  input  [2:0]  io_in_a_bits_param,
  input  [1:0]  io_in_a_bits_size,
  input  [11:0] io_in_a_bits_source,
  input  [14:0] io_in_a_bits_address,
  input  [3:0]  io_in_a_bits_mask,
  input         io_in_a_bits_corrupt,
  input         io_in_d_ready,
  input         io_in_d_valid,
  input  [2:0]  io_in_d_bits_opcode,
  input  [1:0]  io_in_d_bits_size,
  input  [11:0] io_in_d_bits_source
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire  _T_10; // @[Parameters.scala 58:20]
  wire [4:0] _T_14; // @[package.scala 189:77]
  wire [1:0] _T_16; // @[package.scala 189:46]
  wire [14:0] _GEN_18; // @[Edges.scala 22:16]
  wire [14:0] _T_17; // @[Edges.scala 22:16]
  wire  _T_18; // @[Edges.scala 22:24]
  wire [1:0] _T_21; // @[OneHot.scala 65:12]
  wire [1:0] _T_23; // @[Misc.scala 201:81]
  wire  _T_24; // @[Misc.scala 205:21]
  wire  _T_27; // @[Misc.scala 210:20]
  wire  _T_29; // @[Misc.scala 214:38]
  wire  _T_30; // @[Misc.scala 214:29]
  wire  _T_32; // @[Misc.scala 214:38]
  wire  _T_33; // @[Misc.scala 214:29]
  wire  _T_36; // @[Misc.scala 210:20]
  wire  _T_37; // @[Misc.scala 213:27]
  wire  _T_38; // @[Misc.scala 214:38]
  wire  _T_39; // @[Misc.scala 214:29]
  wire  _T_40; // @[Misc.scala 213:27]
  wire  _T_41; // @[Misc.scala 214:38]
  wire  _T_42; // @[Misc.scala 214:29]
  wire  _T_43; // @[Misc.scala 213:27]
  wire  _T_44; // @[Misc.scala 214:38]
  wire  _T_45; // @[Misc.scala 214:29]
  wire  _T_46; // @[Misc.scala 213:27]
  wire  _T_47; // @[Misc.scala 214:38]
  wire  _T_48; // @[Misc.scala 214:29]
  wire [3:0] _T_51; // @[Cat.scala 29:58]
  wire  _T_60; // @[Monitor.scala 65:7]
  wire  _T_70; // @[Monitor.scala 79:25]
  wire [14:0] _T_72; // @[Parameters.scala 137:31]
  wire [15:0] _T_73; // @[Parameters.scala 137:49]
  wire [15:0] _T_75; // @[Parameters.scala 137:52]
  wire  _T_76; // @[Parameters.scala 137:67]
  wire  _T_91; // @[Monitor.scala 44:11]
  wire  _T_94; // @[Monitor.scala 44:11]
  wire  _T_95; // @[Bundles.scala 110:27]
  wire  _T_98; // @[Monitor.scala 44:11]
  wire [3:0] _T_99; // @[Monitor.scala 86:18]
  wire  _T_100; // @[Monitor.scala 86:31]
  wire  _T_103; // @[Monitor.scala 44:11]
  wire  _T_104; // @[Monitor.scala 87:18]
  wire  _T_107; // @[Monitor.scala 44:11]
  wire  _T_108; // @[Monitor.scala 90:25]
  wire  _T_137; // @[Monitor.scala 97:31]
  wire  _T_140; // @[Monitor.scala 44:11]
  wire  _T_150; // @[Monitor.scala 102:25]
  wire  _T_152; // @[Parameters.scala 93:42]
  wire  _T_160; // @[Parameters.scala 551:56]
  wire  _T_164; // @[Monitor.scala 44:11]
  wire  _T_171; // @[Monitor.scala 106:31]
  wire  _T_174; // @[Monitor.scala 44:11]
  wire  _T_175; // @[Monitor.scala 107:30]
  wire  _T_178; // @[Monitor.scala 44:11]
  wire  _T_183; // @[Monitor.scala 111:25]
  wire  _T_212; // @[Monitor.scala 119:25]
  wire [3:0] _T_237; // @[Monitor.scala 124:33]
  wire [3:0] _T_238; // @[Monitor.scala 124:31]
  wire  _T_239; // @[Monitor.scala 124:40]
  wire  _T_242; // @[Monitor.scala 44:11]
  wire  _T_243; // @[Monitor.scala 127:25]
  wire  _T_261; // @[Bundles.scala 140:33]
  wire  _T_264; // @[Monitor.scala 44:11]
  wire  _T_269; // @[Monitor.scala 135:25]
  wire  _T_287; // @[Bundles.scala 147:30]
  wire  _T_290; // @[Monitor.scala 44:11]
  wire  _T_295; // @[Monitor.scala 143:25]
  wire  _T_313; // @[Bundles.scala 160:28]
  wire  _T_316; // @[Monitor.scala 44:11]
  wire  _T_325; // @[Bundles.scala 44:24]
  wire  _T_328; // @[Monitor.scala 51:11]
  wire  _T_335; // @[Parameters.scala 58:20]
  wire  _T_339; // @[Monitor.scala 307:25]
  wire  _T_342; // @[Monitor.scala 51:11]
  wire  _T_343; // @[Monitor.scala 309:27]
  wire  _T_346; // @[Monitor.scala 51:11]
  wire  _T_359; // @[Monitor.scala 315:25]
  wire  _T_387; // @[Monitor.scala 325:25]
  wire  _T_416; // @[Monitor.scala 335:25]
  wire  _T_433; // @[Monitor.scala 343:25]
  wire  _T_451; // @[Monitor.scala 351:25]
  wire  _T_483; // @[Decoupled.scala 40:37]
  reg  _T_492; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire  _T_494; // @[Edges.scala 231:28]
  wire  _T_495; // @[Edges.scala 232:25]
  reg [2:0] _T_503; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [2:0] _T_504; // @[Monitor.scala 382:22]
  reg [31:0] _RAND_2;
  reg [1:0] _T_505; // @[Monitor.scala 383:22]
  reg [31:0] _RAND_3;
  reg [11:0] _T_506; // @[Monitor.scala 384:22]
  reg [31:0] _RAND_4;
  reg [14:0] _T_507; // @[Monitor.scala 385:22]
  reg [31:0] _RAND_5;
  wire  _T_508; // @[Monitor.scala 386:22]
  wire  _T_509; // @[Monitor.scala 386:19]
  wire  _T_510; // @[Monitor.scala 387:32]
  wire  _T_513; // @[Monitor.scala 44:11]
  wire  _T_514; // @[Monitor.scala 388:32]
  wire  _T_517; // @[Monitor.scala 44:11]
  wire  _T_518; // @[Monitor.scala 389:32]
  wire  _T_521; // @[Monitor.scala 44:11]
  wire  _T_522; // @[Monitor.scala 390:32]
  wire  _T_525; // @[Monitor.scala 44:11]
  wire  _T_526; // @[Monitor.scala 391:32]
  wire  _T_529; // @[Monitor.scala 44:11]
  wire  _T_531; // @[Monitor.scala 393:20]
  wire  _T_532; // @[Decoupled.scala 40:37]
  reg  _T_540; // @[Edges.scala 230:27]
  reg [31:0] _RAND_6;
  wire  _T_542; // @[Edges.scala 231:28]
  wire  _T_543; // @[Edges.scala 232:25]
  reg [2:0] _T_551; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_7;
  reg [1:0] _T_553; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_8;
  reg [11:0] _T_554; // @[Monitor.scala 535:22]
  reg [31:0] _RAND_9;
  wire  _T_557; // @[Monitor.scala 538:22]
  wire  _T_558; // @[Monitor.scala 538:19]
  wire  _T_559; // @[Monitor.scala 539:29]
  wire  _T_562; // @[Monitor.scala 51:11]
  wire  _T_567; // @[Monitor.scala 541:29]
  wire  _T_570; // @[Monitor.scala 51:11]
  wire  _T_571; // @[Monitor.scala 542:29]
  wire  _T_574; // @[Monitor.scala 51:11]
  wire  _T_584; // @[Monitor.scala 546:20]
  reg [3647:0] _T_585; // @[Monitor.scala 568:27]
  reg [3647:0] _RAND_10;
  reg  _T_595; // @[Edges.scala 230:27]
  reg [31:0] _RAND_11;
  wire  _T_597; // @[Edges.scala 231:28]
  wire  _T_598; // @[Edges.scala 232:25]
  reg  _T_614; // @[Edges.scala 230:27]
  reg [31:0] _RAND_12;
  wire  _T_616; // @[Edges.scala 231:28]
  wire  _T_617; // @[Edges.scala 232:25]
  wire  _T_627; // @[Monitor.scala 574:27]
  wire [4095:0] _T_629; // @[OneHot.scala 58:35]
  wire [3647:0] _T_630; // @[Monitor.scala 576:23]
  wire  _T_632; // @[Monitor.scala 576:14]
  wire  _T_635; // @[Monitor.scala 576:13]
  wire [4095:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_639; // @[Monitor.scala 581:27]
  wire  _T_641; // @[Monitor.scala 581:75]
  wire  _T_642; // @[Monitor.scala 581:72]
  wire [4095:0] _T_643; // @[OneHot.scala 58:35]
  wire [3647:0] _T_644; // @[Monitor.scala 583:21]
  wire [3647:0] _T_645; // @[Monitor.scala 583:32]
  wire  _T_649; // @[Monitor.scala 51:11]
  wire [4095:0] _GEN_16; // @[Monitor.scala 581:91]
  wire  _T_650; // @[Monitor.scala 587:20]
  wire  _T_651; // @[Monitor.scala 587:40]
  wire  _T_652; // @[Monitor.scala 587:33]
  wire  _T_653; // @[Monitor.scala 587:30]
  wire  _T_656; // @[Monitor.scala 51:11]
  wire [3647:0] _T_657; // @[Monitor.scala 590:27]
  wire [3647:0] _T_658; // @[Monitor.scala 590:38]
  reg [31:0] _T_660; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_13;
  wire  _T_661; // @[Monitor.scala 595:23]
  wire  _T_662; // @[Monitor.scala 595:13]
  wire  _T_663; // @[Monitor.scala 595:36]
  wire  _T_664; // @[Monitor.scala 595:27]
  wire  _T_665; // @[Monitor.scala 595:56]
  wire  _T_666; // @[Monitor.scala 595:44]
  wire  _T_669; // @[Monitor.scala 595:12]
  wire [31:0] _T_671; // @[Monitor.scala 597:26]
  wire  _T_674; // @[Monitor.scala 598:27]
  wire  _GEN_19; // @[Monitor.scala 44:11]
  wire  _GEN_31; // @[Monitor.scala 44:11]
  wire  _GEN_45; // @[Monitor.scala 44:11]
  wire  _GEN_57; // @[Monitor.scala 44:11]
  wire  _GEN_67; // @[Monitor.scala 44:11]
  wire  _GEN_77; // @[Monitor.scala 44:11]
  wire  _GEN_85; // @[Monitor.scala 44:11]
  wire  _GEN_93; // @[Monitor.scala 44:11]
  wire  _GEN_103; // @[Monitor.scala 51:11]
  wire  _GEN_107; // @[Monitor.scala 51:11]
  wire  _GEN_111; // @[Monitor.scala 51:11]
  wire  _GEN_115; // @[Monitor.scala 51:11]
  wire  _GEN_117; // @[Monitor.scala 51:11]
  wire  _GEN_119; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_10 = io_in_a_bits_source <= 12'he3f; // @[Parameters.scala 58:20]
  assign _T_14 = 5'h3 << io_in_a_bits_size; // @[package.scala 189:77]
  assign _T_16 = ~_T_14[1:0]; // @[package.scala 189:46]
  assign _GEN_18 = {{13'd0}, _T_16}; // @[Edges.scala 22:16]
  assign _T_17 = io_in_a_bits_address & _GEN_18; // @[Edges.scala 22:16]
  assign _T_18 = _T_17 == 15'h0; // @[Edges.scala 22:24]
  assign _T_21 = 2'h1 << io_in_a_bits_size[0]; // @[OneHot.scala 65:12]
  assign _T_23 = _T_21 | 2'h1; // @[Misc.scala 201:81]
  assign _T_24 = io_in_a_bits_size >= 2'h2; // @[Misc.scala 205:21]
  assign _T_27 = ~io_in_a_bits_address[1]; // @[Misc.scala 210:20]
  assign _T_29 = _T_23[1] & _T_27; // @[Misc.scala 214:38]
  assign _T_30 = _T_24 | _T_29; // @[Misc.scala 214:29]
  assign _T_32 = _T_23[1] & io_in_a_bits_address[1]; // @[Misc.scala 214:38]
  assign _T_33 = _T_24 | _T_32; // @[Misc.scala 214:29]
  assign _T_36 = ~io_in_a_bits_address[0]; // @[Misc.scala 210:20]
  assign _T_37 = _T_27 & _T_36; // @[Misc.scala 213:27]
  assign _T_38 = _T_23[0] & _T_37; // @[Misc.scala 214:38]
  assign _T_39 = _T_30 | _T_38; // @[Misc.scala 214:29]
  assign _T_40 = _T_27 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_41 = _T_23[0] & _T_40; // @[Misc.scala 214:38]
  assign _T_42 = _T_30 | _T_41; // @[Misc.scala 214:29]
  assign _T_43 = io_in_a_bits_address[1] & _T_36; // @[Misc.scala 213:27]
  assign _T_44 = _T_23[0] & _T_43; // @[Misc.scala 214:38]
  assign _T_45 = _T_33 | _T_44; // @[Misc.scala 214:29]
  assign _T_46 = io_in_a_bits_address[1] & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_47 = _T_23[0] & _T_46; // @[Misc.scala 214:38]
  assign _T_48 = _T_33 | _T_47; // @[Misc.scala 214:29]
  assign _T_51 = {_T_48,_T_45,_T_42,_T_39}; // @[Cat.scala 29:58]
  assign _T_60 = ~_T_10; // @[Monitor.scala 65:7]
  assign _T_70 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_72 = io_in_a_bits_address ^ 15'h4000; // @[Parameters.scala 137:31]
  assign _T_73 = {1'b0,$signed(_T_72)}; // @[Parameters.scala 137:49]
  assign _T_75 = $signed(_T_73) & -16'sh1000; // @[Parameters.scala 137:52]
  assign _T_76 = $signed(_T_75) == 16'sh0; // @[Parameters.scala 137:67]
  assign _T_91 = ~_T_24; // @[Monitor.scala 44:11]
  assign _T_94 = ~_T_18; // @[Monitor.scala 44:11]
  assign _T_95 = io_in_a_bits_param <= 3'h2; // @[Bundles.scala 110:27]
  assign _T_98 = ~_T_95; // @[Monitor.scala 44:11]
  assign _T_99 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_100 = _T_99 == 4'h0; // @[Monitor.scala 86:31]
  assign _T_103 = ~_T_100; // @[Monitor.scala 44:11]
  assign _T_104 = ~io_in_a_bits_corrupt; // @[Monitor.scala 87:18]
  assign _T_107 = ~_T_104; // @[Monitor.scala 44:11]
  assign _T_108 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_137 = io_in_a_bits_param != 3'h0; // @[Monitor.scala 97:31]
  assign _T_140 = ~_T_137; // @[Monitor.scala 44:11]
  assign _T_150 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_152 = io_in_a_bits_size <= 2'h2; // @[Parameters.scala 93:42]
  assign _T_160 = _T_152 & _T_76; // @[Parameters.scala 551:56]
  assign _T_164 = ~_T_160; // @[Monitor.scala 44:11]
  assign _T_171 = io_in_a_bits_param == 3'h0; // @[Monitor.scala 106:31]
  assign _T_174 = ~_T_171; // @[Monitor.scala 44:11]
  assign _T_175 = io_in_a_bits_mask == _T_51; // @[Monitor.scala 107:30]
  assign _T_178 = ~_T_175; // @[Monitor.scala 44:11]
  assign _T_183 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_212 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_237 = ~_T_51; // @[Monitor.scala 124:33]
  assign _T_238 = io_in_a_bits_mask & _T_237; // @[Monitor.scala 124:31]
  assign _T_239 = _T_238 == 4'h0; // @[Monitor.scala 124:40]
  assign _T_242 = ~_T_239; // @[Monitor.scala 44:11]
  assign _T_243 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_261 = io_in_a_bits_param <= 3'h4; // @[Bundles.scala 140:33]
  assign _T_264 = ~_T_261; // @[Monitor.scala 44:11]
  assign _T_269 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_287 = io_in_a_bits_param <= 3'h3; // @[Bundles.scala 147:30]
  assign _T_290 = ~_T_287; // @[Monitor.scala 44:11]
  assign _T_295 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_313 = io_in_a_bits_param <= 3'h1; // @[Bundles.scala 160:28]
  assign _T_316 = ~_T_313; // @[Monitor.scala 44:11]
  assign _T_325 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_328 = ~_T_325; // @[Monitor.scala 51:11]
  assign _T_335 = io_in_d_bits_source <= 12'he3f; // @[Parameters.scala 58:20]
  assign _T_339 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_342 = ~_T_335; // @[Monitor.scala 51:11]
  assign _T_343 = io_in_d_bits_size >= 2'h2; // @[Monitor.scala 309:27]
  assign _T_346 = ~_T_343; // @[Monitor.scala 51:11]
  assign _T_359 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_387 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_416 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_433 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_451 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_483 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_494 = _T_492 - 1'h1; // @[Edges.scala 231:28]
  assign _T_495 = ~_T_492; // @[Edges.scala 232:25]
  assign _T_508 = ~_T_495; // @[Monitor.scala 386:22]
  assign _T_509 = io_in_a_valid & _T_508; // @[Monitor.scala 386:19]
  assign _T_510 = io_in_a_bits_opcode == _T_503; // @[Monitor.scala 387:32]
  assign _T_513 = ~_T_510; // @[Monitor.scala 44:11]
  assign _T_514 = io_in_a_bits_param == _T_504; // @[Monitor.scala 388:32]
  assign _T_517 = ~_T_514; // @[Monitor.scala 44:11]
  assign _T_518 = io_in_a_bits_size == _T_505; // @[Monitor.scala 389:32]
  assign _T_521 = ~_T_518; // @[Monitor.scala 44:11]
  assign _T_522 = io_in_a_bits_source == _T_506; // @[Monitor.scala 390:32]
  assign _T_525 = ~_T_522; // @[Monitor.scala 44:11]
  assign _T_526 = io_in_a_bits_address == _T_507; // @[Monitor.scala 391:32]
  assign _T_529 = ~_T_526; // @[Monitor.scala 44:11]
  assign _T_531 = _T_483 & _T_495; // @[Monitor.scala 393:20]
  assign _T_532 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_542 = _T_540 - 1'h1; // @[Edges.scala 231:28]
  assign _T_543 = ~_T_540; // @[Edges.scala 232:25]
  assign _T_557 = ~_T_543; // @[Monitor.scala 538:22]
  assign _T_558 = io_in_d_valid & _T_557; // @[Monitor.scala 538:19]
  assign _T_559 = io_in_d_bits_opcode == _T_551; // @[Monitor.scala 539:29]
  assign _T_562 = ~_T_559; // @[Monitor.scala 51:11]
  assign _T_567 = io_in_d_bits_size == _T_553; // @[Monitor.scala 541:29]
  assign _T_570 = ~_T_567; // @[Monitor.scala 51:11]
  assign _T_571 = io_in_d_bits_source == _T_554; // @[Monitor.scala 542:29]
  assign _T_574 = ~_T_571; // @[Monitor.scala 51:11]
  assign _T_584 = _T_532 & _T_543; // @[Monitor.scala 546:20]
  assign _T_597 = _T_595 - 1'h1; // @[Edges.scala 231:28]
  assign _T_598 = ~_T_595; // @[Edges.scala 232:25]
  assign _T_616 = _T_614 - 1'h1; // @[Edges.scala 231:28]
  assign _T_617 = ~_T_614; // @[Edges.scala 232:25]
  assign _T_627 = _T_483 & _T_598; // @[Monitor.scala 574:27]
  assign _T_629 = 4096'h1 << io_in_a_bits_source; // @[OneHot.scala 58:35]
  assign _T_630 = _T_585 >> io_in_a_bits_source; // @[Monitor.scala 576:23]
  assign _T_632 = ~_T_630[0]; // @[Monitor.scala 576:14]
  assign _T_635 = ~_T_632; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_627 ? _T_629 : 4096'h0; // @[Monitor.scala 574:72]
  assign _T_639 = _T_532 & _T_617; // @[Monitor.scala 581:27]
  assign _T_641 = ~_T_339; // @[Monitor.scala 581:75]
  assign _T_642 = _T_639 & _T_641; // @[Monitor.scala 581:72]
  assign _T_643 = 4096'h1 << io_in_d_bits_source; // @[OneHot.scala 58:35]
  assign _T_644 = _GEN_15[3647:0] | _T_585; // @[Monitor.scala 583:21]
  assign _T_645 = _T_644 >> io_in_d_bits_source; // @[Monitor.scala 583:32]
  assign _T_649 = ~_T_645[0]; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_642 ? _T_643 : 4096'h0; // @[Monitor.scala 581:91]
  assign _T_650 = _GEN_15[3647:0] != _GEN_16[3647:0]; // @[Monitor.scala 587:20]
  assign _T_651 = _GEN_15[3647:0] != 3648'h0; // @[Monitor.scala 587:40]
  assign _T_652 = ~_T_651; // @[Monitor.scala 587:33]
  assign _T_653 = _T_650 | _T_652; // @[Monitor.scala 587:30]
  assign _T_656 = ~_T_653; // @[Monitor.scala 51:11]
  assign _T_657 = _T_585 | _GEN_15[3647:0]; // @[Monitor.scala 590:27]
  assign _T_658 = ~_GEN_16[3647:0]; // @[Monitor.scala 590:38]
  assign _T_661 = _T_585 != 3648'h0; // @[Monitor.scala 595:23]
  assign _T_662 = ~_T_661; // @[Monitor.scala 595:13]
  assign _T_663 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_664 = _T_662 | _T_663; // @[Monitor.scala 595:27]
  assign _T_665 = _T_660 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_666 = _T_664 | _T_665; // @[Monitor.scala 595:44]
  assign _T_669 = ~_T_666; // @[Monitor.scala 595:12]
  assign _T_671 = _T_660 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_674 = _T_483 | _T_532; // @[Monitor.scala 598:27]
  assign _GEN_19 = io_in_a_valid & _T_70; // @[Monitor.scala 44:11]
  assign _GEN_31 = io_in_a_valid & _T_108; // @[Monitor.scala 44:11]
  assign _GEN_45 = io_in_a_valid & _T_150; // @[Monitor.scala 44:11]
  assign _GEN_57 = io_in_a_valid & _T_183; // @[Monitor.scala 44:11]
  assign _GEN_67 = io_in_a_valid & _T_212; // @[Monitor.scala 44:11]
  assign _GEN_77 = io_in_a_valid & _T_243; // @[Monitor.scala 44:11]
  assign _GEN_85 = io_in_a_valid & _T_269; // @[Monitor.scala 44:11]
  assign _GEN_93 = io_in_a_valid & _T_295; // @[Monitor.scala 44:11]
  assign _GEN_103 = io_in_d_valid & _T_339; // @[Monitor.scala 51:11]
  assign _GEN_107 = io_in_d_valid & _T_359; // @[Monitor.scala 51:11]
  assign _GEN_111 = io_in_d_valid & _T_387; // @[Monitor.scala 51:11]
  assign _GEN_115 = io_in_d_valid & _T_416; // @[Monitor.scala 51:11]
  assign _GEN_117 = io_in_d_valid & _T_433; // @[Monitor.scala 51:11]
  assign _GEN_119 = io_in_d_valid & _T_451; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_492 = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_503 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_504 = _RAND_2[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_505 = _RAND_3[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_506 = _RAND_4[11:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  _T_507 = _RAND_5[14:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_540 = _RAND_6[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_551 = _RAND_7[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_553 = _RAND_8[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_554 = _RAND_9[11:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {114{`RANDOM}};
  _T_585 = _RAND_10[3647:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_595 = _RAND_11[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_614 = _RAND_12[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  _T_660 = _RAND_13[31:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (_T_483) begin
      if (_T_495) begin
        _T_492 <= 1'h0;
      end else begin
        _T_492 <= _T_494;
      end
    end
    if (_T_531) begin
      _T_503 <= io_in_a_bits_opcode;
    end
    if (_T_531) begin
      _T_504 <= io_in_a_bits_param;
    end
    if (_T_531) begin
      _T_505 <= io_in_a_bits_size;
    end
    if (_T_531) begin
      _T_506 <= io_in_a_bits_source;
    end
    if (_T_531) begin
      _T_507 <= io_in_a_bits_address;
    end
    if (_T_532) begin
      if (_T_543) begin
        _T_540 <= 1'h0;
      end else begin
        _T_540 <= _T_542;
      end
    end
    if (_T_584) begin
      _T_551 <= io_in_d_bits_opcode;
    end
    if (_T_584) begin
      _T_553 <= io_in_d_bits_size;
    end
    if (_T_584) begin
      _T_554 <= io_in_d_bits_source;
    end
    _T_585 <= _T_657 & _T_658;
    if (_T_483) begin
      if (_T_598) begin
        _T_595 <= 1'h0;
      end else begin
        _T_595 <= _T_597;
      end
    end
    if (_T_532) begin
      if (_T_617) begin
        _T_614 <= 1'h0;
      end else begin
        _T_614 <= _T_616;
      end
    end
    if (_T_674) begin
      _T_660 <= 32'h0;
    end else begin
      _T_660 <= _T_671;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_a_valid & _T_70) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_a_valid & _T_70) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_a_valid & _T_70) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_a_valid & _T_70) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_60) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid source ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_60) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_91) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock smaller than a beat (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_91) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_94) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_94) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_98) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid grow param (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_98) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_103) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_103) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_107) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock is corrupt (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_107) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_a_valid & _T_108) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_a_valid & _T_108) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_a_valid & _T_108) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_a_valid & _T_108) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_31 & _T_60) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid source ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_31 & _T_60) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_31 & _T_91) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm smaller than a beat (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_31 & _T_91) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_31 & _T_94) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_31 & _T_94) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_31 & _T_98) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid grow param (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_31 & _T_98) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_31 & _T_140) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_31 & _T_140) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_31 & _T_103) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_31 & _T_103) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_31 & _T_107) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm is corrupt (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_31 & _T_107) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_45 & _T_164) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_45 & _T_164) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_45 & _T_60) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid source ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_45 & _T_60) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_45 & _T_94) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_45 & _T_94) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_45 & _T_174) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid param (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_45 & _T_174) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_45 & _T_178) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_45 & _T_178) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_45 & _T_107) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get is corrupt (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_45 & _T_107) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_57 & _T_164) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_57 & _T_164) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_57 & _T_60) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid source ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_57 & _T_60) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_57 & _T_94) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_57 & _T_94) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_57 & _T_174) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid param (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_57 & _T_174) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_57 & _T_178) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_57 & _T_178) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_67 & _T_164) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_67 & _T_164) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_67 & _T_60) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid source ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_67 & _T_60) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_67 & _T_94) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_67 & _T_94) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_67 & _T_174) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid param (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_67 & _T_174) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_67 & _T_242) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial contains invalid mask (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_67 & _T_242) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_a_valid & _T_243) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_a_valid & _T_243) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_77 & _T_60) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid source ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_77 & _T_60) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_77 & _T_94) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_77 & _T_94) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_77 & _T_264) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid opcode param (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_77 & _T_264) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_77 & _T_178) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_77 & _T_178) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_a_valid & _T_269) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_a_valid & _T_269) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_60) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid source ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_60) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_94) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_94) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_290) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid opcode param (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_290) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_178) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_178) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_a_valid & _T_295) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_a_valid & _T_295) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_93 & _T_60) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid source ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_93 & _T_60) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_93 & _T_94) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_93 & _T_94) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_93 & _T_316) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid opcode param (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_93 & _T_316) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_93 & _T_178) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_93 & _T_178) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_93 & _T_107) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint is corrupt (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_93 & _T_107) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_328) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_328) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_103 & _T_342) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck carries invalid source ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_103 & _T_342) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_103 & _T_346) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_103 & _T_346) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_107 & _T_342) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid source ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_107 & _T_342) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_359) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid sink ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_359) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_107 & _T_346) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_107 & _T_346) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_111 & _T_342) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid source ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_111 & _T_342) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_387) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid sink ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_387) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_111 & _T_346) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_111 & _T_346) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_115 & _T_342) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid source ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_115 & _T_342) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_342) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid source ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_342) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_119 & _T_342) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid source ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_119 & _T_342) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_509 & _T_513) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_509 & _T_513) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_509 & _T_517) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel param changed within multibeat operation (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_509 & _T_517) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_509 & _T_521) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel size changed within multibeat operation (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_509 & _T_521) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_509 & _T_525) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel source changed within multibeat operation (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_509 & _T_525) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_509 & _T_529) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_509 & _T_529) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_558 & _T_562) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_558 & _T_562) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_558 & _T_570) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_558 & _T_570) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_558 & _T_574) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel source changed within multibeat operation (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_558 & _T_574) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_627 & _T_635) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_627 & _T_635) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_642 & _T_649) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_642 & _T_649) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_656) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' and 'D' concurrent, despite minlatency 1 (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_656) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_669) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at CrossingHelper.scala:17:44)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_669) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
