//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__QueueCompatibility_170(
  input          rf_reset,
  input          clock,
  input          reset,
  output         io_enq_ready,
  input          io_enq_valid,
  input  [2:0]   io_enq_bits_opcode,
  input  [2:0]   io_enq_bits_size,
  input  [9:0]   io_enq_bits_source,
  input  [127:0] io_enq_bits_data,
  input          io_enq_bits_corrupt,
  input          io_deq_ready,
  output         io_deq_valid,
  output [2:0]   io_deq_bits_opcode,
  output [2:0]   io_deq_bits_size,
  output [9:0]   io_deq_bits_source,
  output [127:0] io_deq_bits_data,
  output         io_deq_bits_corrupt,
  output [2:0]   io_count
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [127:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [127:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [127:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [127:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
`endif // RANDOMIZE_REG_INIT
  reg [2:0] ram_0_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_0_size; // @[Decoupled.scala 218:16]
  reg [9:0] ram_0_source; // @[Decoupled.scala 218:16]
  reg [127:0] ram_0_data; // @[Decoupled.scala 218:16]
  reg  ram_0_corrupt; // @[Decoupled.scala 218:16]
  reg [2:0] ram_1_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_1_size; // @[Decoupled.scala 218:16]
  reg [9:0] ram_1_source; // @[Decoupled.scala 218:16]
  reg [127:0] ram_1_data; // @[Decoupled.scala 218:16]
  reg  ram_1_corrupt; // @[Decoupled.scala 218:16]
  reg [2:0] ram_2_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_2_size; // @[Decoupled.scala 218:16]
  reg [9:0] ram_2_source; // @[Decoupled.scala 218:16]
  reg [127:0] ram_2_data; // @[Decoupled.scala 218:16]
  reg  ram_2_corrupt; // @[Decoupled.scala 218:16]
  reg [2:0] ram_3_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_3_size; // @[Decoupled.scala 218:16]
  reg [9:0] ram_3_source; // @[Decoupled.scala 218:16]
  reg [127:0] ram_3_data; // @[Decoupled.scala 218:16]
  reg  ram_3_corrupt; // @[Decoupled.scala 218:16]
  reg [1:0] deq_ptr_value; // @[Counter.scala 60:40]
  wire [2:0] _GEN_8 = 2'h1 == deq_ptr_value ? ram_1_opcode : ram_0_opcode; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_10 = 2'h1 == deq_ptr_value ? ram_1_size : ram_0_size; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [9:0] _GEN_11 = 2'h1 == deq_ptr_value ? ram_1_source : ram_0_source; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [127:0] _GEN_14 = 2'h1 == deq_ptr_value ? ram_1_data : ram_0_data; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire  _GEN_15 = 2'h1 == deq_ptr_value ? ram_1_corrupt : ram_0_corrupt; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_16 = 2'h2 == deq_ptr_value ? ram_2_opcode : _GEN_8; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_18 = 2'h2 == deq_ptr_value ? ram_2_size : _GEN_10; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [9:0] _GEN_19 = 2'h2 == deq_ptr_value ? ram_2_source : _GEN_11; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [127:0] _GEN_22 = 2'h2 == deq_ptr_value ? ram_2_data : _GEN_14; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire  _GEN_23 = 2'h2 == deq_ptr_value ? ram_2_corrupt : _GEN_15; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire  do_enq = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  reg [1:0] enq_ptr_value; // @[Counter.scala 60:40]
  reg  maybe_full; // @[Decoupled.scala 221:27]
  wire  ptr_match = enq_ptr_value == deq_ptr_value; // @[Decoupled.scala 223:33]
  wire  empty = ptr_match & ~maybe_full; // @[Decoupled.scala 224:25]
  wire  full = ptr_match & maybe_full; // @[Decoupled.scala 225:24]
  wire  do_deq = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  wire [1:0] _value_T_1 = enq_ptr_value + 2'h1; // @[Counter.scala 76:24]
  wire [1:0] _value_T_3 = deq_ptr_value + 2'h1; // @[Counter.scala 76:24]
  wire [1:0] ptr_diff = enq_ptr_value - deq_ptr_value; // @[Decoupled.scala 257:32]
  wire [2:0] _io_count_T_1 = maybe_full & ptr_match ? 3'h4 : 3'h0; // @[Decoupled.scala 259:20]
  wire [2:0] _GEN_129 = {{1'd0}, ptr_diff}; // @[Decoupled.scala 259:62]
  assign io_enq_ready = ~full; // @[Decoupled.scala 241:19]
  assign io_deq_valid = ~empty; // @[Decoupled.scala 240:19]
  assign io_deq_bits_opcode = 2'h3 == deq_ptr_value ? ram_3_opcode : _GEN_16; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_size = 2'h3 == deq_ptr_value ? ram_3_size : _GEN_18; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_source = 2'h3 == deq_ptr_value ? ram_3_source : _GEN_19; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_data = 2'h3 == deq_ptr_value ? ram_3_data : _GEN_22; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_corrupt = 2'h3 == deq_ptr_value ? ram_3_corrupt : _GEN_23; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_count = _io_count_T_1 | _GEN_129; // @[Decoupled.scala 259:62]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_opcode <= 3'h0;
    end else if (do_enq) begin
      if (2'h0 == enq_ptr_value) begin
        ram_0_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_size <= 3'h0;
    end else if (do_enq) begin
      if (2'h0 == enq_ptr_value) begin
        ram_0_size <= io_enq_bits_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_source <= 10'h0;
    end else if (do_enq) begin
      if (2'h0 == enq_ptr_value) begin
        ram_0_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_data <= 128'h0;
    end else if (do_enq) begin
      if (2'h0 == enq_ptr_value) begin
        ram_0_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_corrupt <= 1'h0;
    end else if (do_enq) begin
      if (2'h0 == enq_ptr_value) begin
        ram_0_corrupt <= io_enq_bits_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_opcode <= 3'h0;
    end else if (do_enq) begin
      if (2'h1 == enq_ptr_value) begin
        ram_1_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_size <= 3'h0;
    end else if (do_enq) begin
      if (2'h1 == enq_ptr_value) begin
        ram_1_size <= io_enq_bits_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_source <= 10'h0;
    end else if (do_enq) begin
      if (2'h1 == enq_ptr_value) begin
        ram_1_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_data <= 128'h0;
    end else if (do_enq) begin
      if (2'h1 == enq_ptr_value) begin
        ram_1_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_corrupt <= 1'h0;
    end else if (do_enq) begin
      if (2'h1 == enq_ptr_value) begin
        ram_1_corrupt <= io_enq_bits_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_opcode <= 3'h0;
    end else if (do_enq) begin
      if (2'h2 == enq_ptr_value) begin
        ram_2_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_size <= 3'h0;
    end else if (do_enq) begin
      if (2'h2 == enq_ptr_value) begin
        ram_2_size <= io_enq_bits_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_source <= 10'h0;
    end else if (do_enq) begin
      if (2'h2 == enq_ptr_value) begin
        ram_2_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_data <= 128'h0;
    end else if (do_enq) begin
      if (2'h2 == enq_ptr_value) begin
        ram_2_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_corrupt <= 1'h0;
    end else if (do_enq) begin
      if (2'h2 == enq_ptr_value) begin
        ram_2_corrupt <= io_enq_bits_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_opcode <= 3'h0;
    end else if (do_enq) begin
      if (2'h3 == enq_ptr_value) begin
        ram_3_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_size <= 3'h0;
    end else if (do_enq) begin
      if (2'h3 == enq_ptr_value) begin
        ram_3_size <= io_enq_bits_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_source <= 10'h0;
    end else if (do_enq) begin
      if (2'h3 == enq_ptr_value) begin
        ram_3_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_data <= 128'h0;
    end else if (do_enq) begin
      if (2'h3 == enq_ptr_value) begin
        ram_3_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_corrupt <= 1'h0;
    end else if (do_enq) begin
      if (2'h3 == enq_ptr_value) begin
        ram_3_corrupt <= io_enq_bits_corrupt;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      deq_ptr_value <= 2'h0;
    end else if (do_deq) begin
      deq_ptr_value <= _value_T_3;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      enq_ptr_value <= 2'h0;
    end else if (do_enq) begin
      enq_ptr_value <= _value_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      maybe_full <= 1'h0;
    end else if (do_enq != do_deq) begin
      maybe_full <= do_enq;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ram_0_opcode = _RAND_0[2:0];
  _RAND_1 = {1{`RANDOM}};
  ram_0_size = _RAND_1[2:0];
  _RAND_2 = {1{`RANDOM}};
  ram_0_source = _RAND_2[9:0];
  _RAND_3 = {4{`RANDOM}};
  ram_0_data = _RAND_3[127:0];
  _RAND_4 = {1{`RANDOM}};
  ram_0_corrupt = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  ram_1_opcode = _RAND_5[2:0];
  _RAND_6 = {1{`RANDOM}};
  ram_1_size = _RAND_6[2:0];
  _RAND_7 = {1{`RANDOM}};
  ram_1_source = _RAND_7[9:0];
  _RAND_8 = {4{`RANDOM}};
  ram_1_data = _RAND_8[127:0];
  _RAND_9 = {1{`RANDOM}};
  ram_1_corrupt = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  ram_2_opcode = _RAND_10[2:0];
  _RAND_11 = {1{`RANDOM}};
  ram_2_size = _RAND_11[2:0];
  _RAND_12 = {1{`RANDOM}};
  ram_2_source = _RAND_12[9:0];
  _RAND_13 = {4{`RANDOM}};
  ram_2_data = _RAND_13[127:0];
  _RAND_14 = {1{`RANDOM}};
  ram_2_corrupt = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  ram_3_opcode = _RAND_15[2:0];
  _RAND_16 = {1{`RANDOM}};
  ram_3_size = _RAND_16[2:0];
  _RAND_17 = {1{`RANDOM}};
  ram_3_source = _RAND_17[9:0];
  _RAND_18 = {4{`RANDOM}};
  ram_3_data = _RAND_18[127:0];
  _RAND_19 = {1{`RANDOM}};
  ram_3_corrupt = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  deq_ptr_value = _RAND_20[1:0];
  _RAND_21 = {1{`RANDOM}};
  enq_ptr_value = _RAND_21[1:0];
  _RAND_22 = {1{`RANDOM}};
  maybe_full = _RAND_22[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ram_0_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_0_size = 3'h0;
  end
  if (rf_reset) begin
    ram_0_source = 10'h0;
  end
  if (rf_reset) begin
    ram_0_data = 128'h0;
  end
  if (rf_reset) begin
    ram_0_corrupt = 1'h0;
  end
  if (rf_reset) begin
    ram_1_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_1_size = 3'h0;
  end
  if (rf_reset) begin
    ram_1_source = 10'h0;
  end
  if (rf_reset) begin
    ram_1_data = 128'h0;
  end
  if (rf_reset) begin
    ram_1_corrupt = 1'h0;
  end
  if (rf_reset) begin
    ram_2_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_2_size = 3'h0;
  end
  if (rf_reset) begin
    ram_2_source = 10'h0;
  end
  if (rf_reset) begin
    ram_2_data = 128'h0;
  end
  if (rf_reset) begin
    ram_2_corrupt = 1'h0;
  end
  if (rf_reset) begin
    ram_3_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_3_size = 3'h0;
  end
  if (rf_reset) begin
    ram_3_source = 10'h0;
  end
  if (rf_reset) begin
    ram_3_data = 128'h0;
  end
  if (rf_reset) begin
    ram_3_corrupt = 1'h0;
  end
  if (reset) begin
    deq_ptr_value = 2'h0;
  end
  if (reset) begin
    enq_ptr_value = 2'h0;
  end
  if (reset) begin
    maybe_full = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
