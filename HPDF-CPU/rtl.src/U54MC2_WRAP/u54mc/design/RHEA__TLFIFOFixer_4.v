//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLFIFOFixer_4(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_a_ready,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [2:0]  auto_in_a_bits_param,
  input  [3:0]  auto_in_a_bits_size,
  input  [6:0]  auto_in_a_bits_source,
  input  [31:0] auto_in_a_bits_address,
  input         auto_in_a_bits_user_amba_prot_bufferable,
  input         auto_in_a_bits_user_amba_prot_modifiable,
  input         auto_in_a_bits_user_amba_prot_readalloc,
  input         auto_in_a_bits_user_amba_prot_writealloc,
  input         auto_in_a_bits_user_amba_prot_privileged,
  input         auto_in_a_bits_user_amba_prot_secure,
  input         auto_in_a_bits_user_amba_prot_fetch,
  input  [7:0]  auto_in_a_bits_mask,
  input  [63:0] auto_in_a_bits_data,
  input         auto_in_a_bits_corrupt,
  input         auto_in_d_ready,
  output        auto_in_d_valid,
  output [2:0]  auto_in_d_bits_opcode,
  output [1:0]  auto_in_d_bits_param,
  output [3:0]  auto_in_d_bits_size,
  output [6:0]  auto_in_d_bits_source,
  output        auto_in_d_bits_sink,
  output        auto_in_d_bits_denied,
  output [63:0] auto_in_d_bits_data,
  output        auto_in_d_bits_corrupt,
  input         auto_out_a_ready,
  output        auto_out_a_valid,
  output [2:0]  auto_out_a_bits_opcode,
  output [2:0]  auto_out_a_bits_param,
  output [3:0]  auto_out_a_bits_size,
  output [6:0]  auto_out_a_bits_source,
  output [31:0] auto_out_a_bits_address,
  output        auto_out_a_bits_user_amba_prot_bufferable,
  output        auto_out_a_bits_user_amba_prot_modifiable,
  output        auto_out_a_bits_user_amba_prot_readalloc,
  output        auto_out_a_bits_user_amba_prot_writealloc,
  output        auto_out_a_bits_user_amba_prot_privileged,
  output        auto_out_a_bits_user_amba_prot_secure,
  output        auto_out_a_bits_user_amba_prot_fetch,
  output [7:0]  auto_out_a_bits_mask,
  output [63:0] auto_out_a_bits_data,
  output        auto_out_a_bits_corrupt,
  output        auto_out_d_ready,
  input         auto_out_d_valid,
  input  [2:0]  auto_out_d_bits_opcode,
  input  [1:0]  auto_out_d_bits_param,
  input  [3:0]  auto_out_d_bits_size,
  input  [6:0]  auto_out_d_bits_source,
  input         auto_out_d_bits_sink,
  input         auto_out_d_bits_denied,
  input  [63:0] auto_out_d_bits_data,
  input         auto_out_d_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [31:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
`endif // RANDOMIZE_REG_INIT
  wire [31:0] _a_id_T = auto_in_a_bits_address ^ 32'h3000; // @[Parameters.scala 137:31]
  wire [32:0] _a_id_T_1 = {1'b0,$signed(_a_id_T)}; // @[Parameters.scala 137:49]
  wire [32:0] _a_id_T_3 = $signed(_a_id_T_1) & 33'sh9a017000; // @[Parameters.scala 137:52]
  wire  _a_id_T_4 = $signed(_a_id_T_3) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _a_id_T_5 = auto_in_a_bits_address ^ 32'h2010000; // @[Parameters.scala 137:31]
  wire [32:0] _a_id_T_6 = {1'b0,$signed(_a_id_T_5)}; // @[Parameters.scala 137:49]
  wire [32:0] _a_id_T_8 = $signed(_a_id_T_6) & 33'sh9a014000; // @[Parameters.scala 137:52]
  wire  _a_id_T_9 = $signed(_a_id_T_8) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _a_id_T_10 = auto_in_a_bits_address ^ 32'h8000000; // @[Parameters.scala 137:31]
  wire [32:0] _a_id_T_11 = {1'b0,$signed(_a_id_T_10)}; // @[Parameters.scala 137:49]
  wire [32:0] _a_id_T_13 = $signed(_a_id_T_11) & 33'sh98000000; // @[Parameters.scala 137:52]
  wire  _a_id_T_14 = $signed(_a_id_T_13) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _a_id_T_15 = auto_in_a_bits_address ^ 32'h2000000; // @[Parameters.scala 137:31]
  wire [32:0] _a_id_T_16 = {1'b0,$signed(_a_id_T_15)}; // @[Parameters.scala 137:49]
  wire [32:0] _a_id_T_18 = $signed(_a_id_T_16) & 33'sh9a010000; // @[Parameters.scala 137:52]
  wire  _a_id_T_19 = $signed(_a_id_T_18) == 33'sh0; // @[Parameters.scala 137:67]
  wire [32:0] _a_id_T_21 = {1'b0,$signed(auto_in_a_bits_address)}; // @[Parameters.scala 137:49]
  wire [32:0] _a_id_T_23 = $signed(_a_id_T_21) & 33'sh9a017000; // @[Parameters.scala 137:52]
  wire  _a_id_T_24 = $signed(_a_id_T_23) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _a_id_T_25 = auto_in_a_bits_address ^ 32'h80000000; // @[Parameters.scala 137:31]
  wire [32:0] _a_id_T_26 = {1'b0,$signed(_a_id_T_25)}; // @[Parameters.scala 137:49]
  wire [32:0] _a_id_T_28 = $signed(_a_id_T_26) & 33'sh80000000; // @[Parameters.scala 137:52]
  wire  _a_id_T_29 = $signed(_a_id_T_28) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _a_id_T_30 = auto_in_a_bits_address ^ 32'h10000000; // @[Parameters.scala 137:31]
  wire [32:0] _a_id_T_31 = {1'b0,$signed(_a_id_T_30)}; // @[Parameters.scala 137:49]
  wire [32:0] _a_id_T_33 = $signed(_a_id_T_31) & 33'sh9a017000; // @[Parameters.scala 137:52]
  wire  _a_id_T_34 = $signed(_a_id_T_33) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _a_id_T_35 = auto_in_a_bits_address ^ 32'h10001000; // @[Parameters.scala 137:31]
  wire [32:0] _a_id_T_36 = {1'b0,$signed(_a_id_T_35)}; // @[Parameters.scala 137:49]
  wire [32:0] _a_id_T_38 = $signed(_a_id_T_36) & 33'sh9a017000; // @[Parameters.scala 137:52]
  wire  _a_id_T_39 = $signed(_a_id_T_38) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _a_id_T_40 = auto_in_a_bits_address ^ 32'h10003000; // @[Parameters.scala 137:31]
  wire [32:0] _a_id_T_41 = {1'b0,$signed(_a_id_T_40)}; // @[Parameters.scala 137:49]
  wire [32:0] _a_id_T_43 = $signed(_a_id_T_41) & 33'sh9a017000; // @[Parameters.scala 137:52]
  wire  _a_id_T_44 = $signed(_a_id_T_43) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _a_id_T_45 = auto_in_a_bits_address ^ 32'h4000; // @[Parameters.scala 137:31]
  wire [32:0] _a_id_T_46 = {1'b0,$signed(_a_id_T_45)}; // @[Parameters.scala 137:49]
  wire [32:0] _a_id_T_48 = $signed(_a_id_T_46) & 33'sh9a017000; // @[Parameters.scala 137:52]
  wire  _a_id_T_49 = $signed(_a_id_T_48) == 33'sh0; // @[Parameters.scala 137:67]
  wire [1:0] _a_id_T_51 = _a_id_T_9 ? 2'h3 : 2'h0; // @[Mux.scala 27:72]
  wire [2:0] _a_id_T_52 = _a_id_T_14 ? 3'h6 : 3'h0; // @[Mux.scala 27:72]
  wire [3:0] _a_id_T_53 = _a_id_T_19 ? 4'h8 : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _a_id_T_54 = _a_id_T_24 ? 4'ha : 4'h0; // @[Mux.scala 27:72]
  wire [1:0] _a_id_T_55 = _a_id_T_29 ? 2'h2 : 2'h0; // @[Mux.scala 27:72]
  wire [2:0] _a_id_T_56 = _a_id_T_34 ? 3'h4 : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _a_id_T_57 = _a_id_T_39 ? 3'h7 : 3'h0; // @[Mux.scala 27:72]
  wire [3:0] _a_id_T_58 = _a_id_T_44 ? 4'h9 : 4'h0; // @[Mux.scala 27:72]
  wire [2:0] _a_id_T_59 = _a_id_T_49 ? 3'h5 : 3'h0; // @[Mux.scala 27:72]
  wire [1:0] _GEN_467 = {{1'd0}, _a_id_T_4}; // @[Mux.scala 27:72]
  wire [1:0] _a_id_T_60 = _GEN_467 | _a_id_T_51; // @[Mux.scala 27:72]
  wire [2:0] _GEN_468 = {{1'd0}, _a_id_T_60}; // @[Mux.scala 27:72]
  wire [2:0] _a_id_T_61 = _GEN_468 | _a_id_T_52; // @[Mux.scala 27:72]
  wire [3:0] _GEN_469 = {{1'd0}, _a_id_T_61}; // @[Mux.scala 27:72]
  wire [3:0] _a_id_T_62 = _GEN_469 | _a_id_T_53; // @[Mux.scala 27:72]
  wire [3:0] _a_id_T_63 = _a_id_T_62 | _a_id_T_54; // @[Mux.scala 27:72]
  wire [3:0] _GEN_470 = {{2'd0}, _a_id_T_55}; // @[Mux.scala 27:72]
  wire [3:0] _a_id_T_64 = _a_id_T_63 | _GEN_470; // @[Mux.scala 27:72]
  wire [3:0] _GEN_471 = {{1'd0}, _a_id_T_56}; // @[Mux.scala 27:72]
  wire [3:0] _a_id_T_65 = _a_id_T_64 | _GEN_471; // @[Mux.scala 27:72]
  wire [3:0] _GEN_472 = {{1'd0}, _a_id_T_57}; // @[Mux.scala 27:72]
  wire [3:0] _a_id_T_66 = _a_id_T_65 | _GEN_472; // @[Mux.scala 27:72]
  wire [3:0] _a_id_T_67 = _a_id_T_66 | _a_id_T_58; // @[Mux.scala 27:72]
  wire [3:0] _GEN_473 = {{1'd0}, _a_id_T_59}; // @[Mux.scala 27:72]
  wire [3:0] a_id = _a_id_T_67 | _GEN_473; // @[Mux.scala 27:72]
  wire  a_noDomain = a_id == 4'h0; // @[FIFOFixer.scala 55:29]
  wire  stalls_a_sel = auto_in_a_bits_source[6:3] == 4'h0; // @[Parameters.scala 54:32]
  reg [4:0] a_first_counter; // @[Edges.scala 228:27]
  wire  a_first = a_first_counter == 5'h0; // @[Edges.scala 230:25]
  reg  flight_0; // @[FIFOFixer.scala 71:27]
  reg  flight_1; // @[FIFOFixer.scala 71:27]
  reg  flight_2; // @[FIFOFixer.scala 71:27]
  reg  flight_3; // @[FIFOFixer.scala 71:27]
  reg  flight_4; // @[FIFOFixer.scala 71:27]
  reg  flight_5; // @[FIFOFixer.scala 71:27]
  reg  flight_6; // @[FIFOFixer.scala 71:27]
  reg  flight_7; // @[FIFOFixer.scala 71:27]
  wire  _stalls_T_7 = flight_0 | flight_1 | flight_2 | flight_3 | flight_4 | flight_5 | flight_6 | flight_7; // @[FIFOFixer.scala 80:44]
  reg [3:0] stalls_id; // @[Reg.scala 15:16]
  wire  stalls_0 = stalls_a_sel & a_first & _stalls_T_7 & (a_noDomain | stalls_id != a_id); // @[FIFOFixer.scala 80:50]
  wire  stalls_a_sel_1 = auto_in_a_bits_source[6:3] == 4'h1; // @[Parameters.scala 54:32]
  reg  flight_8; // @[FIFOFixer.scala 71:27]
  reg  flight_9; // @[FIFOFixer.scala 71:27]
  reg  flight_10; // @[FIFOFixer.scala 71:27]
  reg  flight_11; // @[FIFOFixer.scala 71:27]
  reg  flight_12; // @[FIFOFixer.scala 71:27]
  reg  flight_13; // @[FIFOFixer.scala 71:27]
  reg  flight_14; // @[FIFOFixer.scala 71:27]
  reg  flight_15; // @[FIFOFixer.scala 71:27]
  reg [3:0] stalls_id_1; // @[Reg.scala 15:16]
  wire  stalls_1 = stalls_a_sel_1 & a_first & (flight_8 | flight_9 | flight_10 | flight_11 | flight_12 | flight_13 |
    flight_14 | flight_15) & (a_noDomain | stalls_id_1 != a_id); // @[FIFOFixer.scala 80:50]
  wire  stalls_a_sel_2 = auto_in_a_bits_source[6:3] == 4'h2; // @[Parameters.scala 54:32]
  reg  flight_16; // @[FIFOFixer.scala 71:27]
  reg  flight_17; // @[FIFOFixer.scala 71:27]
  reg  flight_18; // @[FIFOFixer.scala 71:27]
  reg  flight_19; // @[FIFOFixer.scala 71:27]
  reg  flight_20; // @[FIFOFixer.scala 71:27]
  reg  flight_21; // @[FIFOFixer.scala 71:27]
  reg  flight_22; // @[FIFOFixer.scala 71:27]
  reg  flight_23; // @[FIFOFixer.scala 71:27]
  reg [3:0] stalls_id_2; // @[Reg.scala 15:16]
  wire  stalls_2 = stalls_a_sel_2 & a_first & (flight_16 | flight_17 | flight_18 | flight_19 | flight_20 | flight_21 |
    flight_22 | flight_23) & (a_noDomain | stalls_id_2 != a_id); // @[FIFOFixer.scala 80:50]
  wire  stalls_a_sel_3 = auto_in_a_bits_source[6:3] == 4'h3; // @[Parameters.scala 54:32]
  reg  flight_24; // @[FIFOFixer.scala 71:27]
  reg  flight_25; // @[FIFOFixer.scala 71:27]
  reg  flight_26; // @[FIFOFixer.scala 71:27]
  reg  flight_27; // @[FIFOFixer.scala 71:27]
  reg  flight_28; // @[FIFOFixer.scala 71:27]
  reg  flight_29; // @[FIFOFixer.scala 71:27]
  reg  flight_30; // @[FIFOFixer.scala 71:27]
  reg  flight_31; // @[FIFOFixer.scala 71:27]
  reg [3:0] stalls_id_3; // @[Reg.scala 15:16]
  wire  stalls_3 = stalls_a_sel_3 & a_first & (flight_24 | flight_25 | flight_26 | flight_27 | flight_28 | flight_29 |
    flight_30 | flight_31) & (a_noDomain | stalls_id_3 != a_id); // @[FIFOFixer.scala 80:50]
  wire  stalls_a_sel_4 = auto_in_a_bits_source[6:2] == 5'h8; // @[Parameters.scala 54:32]
  reg  flight_32; // @[FIFOFixer.scala 71:27]
  reg  flight_33; // @[FIFOFixer.scala 71:27]
  reg  flight_34; // @[FIFOFixer.scala 71:27]
  reg  flight_35; // @[FIFOFixer.scala 71:27]
  reg [3:0] stalls_id_4; // @[Reg.scala 15:16]
  wire  stalls_4 = stalls_a_sel_4 & a_first & (flight_32 | flight_33 | flight_34 | flight_35) & (a_noDomain |
    stalls_id_4 != a_id); // @[FIFOFixer.scala 80:50]
  wire [3:0] stalls_a_sel_uncommonBits_5 = auto_in_a_bits_source[3:0]; // @[Parameters.scala 52:64]
  wire  _stalls_a_sel_T_27 = 4'h2 <= stalls_a_sel_uncommonBits_5; // @[Parameters.scala 56:34]
  wire  _stalls_a_sel_T_28 = auto_in_a_bits_source[6:4] == 3'h6 & _stalls_a_sel_T_27; // @[Parameters.scala 54:69]
  wire  _stalls_a_sel_T_29 = stalls_a_sel_uncommonBits_5 <= 4'h8; // @[Parameters.scala 57:20]
  wire  stalls_a_sel_5 = _stalls_a_sel_T_28 & _stalls_a_sel_T_29; // @[Parameters.scala 56:50]
  reg  flight_98; // @[FIFOFixer.scala 71:27]
  reg  flight_99; // @[FIFOFixer.scala 71:27]
  reg  flight_100; // @[FIFOFixer.scala 71:27]
  reg  flight_101; // @[FIFOFixer.scala 71:27]
  reg  flight_102; // @[FIFOFixer.scala 71:27]
  reg  flight_103; // @[FIFOFixer.scala 71:27]
  reg  flight_104; // @[FIFOFixer.scala 71:27]
  reg [3:0] stalls_id_5; // @[Reg.scala 15:16]
  wire  stalls_5 = stalls_a_sel_5 & a_first & (flight_98 | flight_99 | flight_100 | flight_101 | flight_102 | flight_103
     | flight_104) & (a_noDomain | stalls_id_5 != a_id); // @[FIFOFixer.scala 80:50]
  wire  _stalls_a_sel_T_33 = auto_in_a_bits_source[6:4] == 3'h4 & _stalls_a_sel_T_27; // @[Parameters.scala 54:69]
  wire  stalls_a_sel_6 = _stalls_a_sel_T_33 & _stalls_a_sel_T_29; // @[Parameters.scala 56:50]
  reg  flight_66; // @[FIFOFixer.scala 71:27]
  reg  flight_67; // @[FIFOFixer.scala 71:27]
  reg  flight_68; // @[FIFOFixer.scala 71:27]
  reg  flight_69; // @[FIFOFixer.scala 71:27]
  reg  flight_70; // @[FIFOFixer.scala 71:27]
  reg  flight_71; // @[FIFOFixer.scala 71:27]
  reg  flight_72; // @[FIFOFixer.scala 71:27]
  reg [3:0] stalls_id_6; // @[Reg.scala 15:16]
  wire  stalls_6 = stalls_a_sel_6 & a_first & (flight_66 | flight_67 | flight_68 | flight_69 | flight_70 | flight_71 |
    flight_72) & (a_noDomain | stalls_id_6 != a_id); // @[FIFOFixer.scala 80:50]
  wire  stall = stalls_0 | stalls_1 | stalls_2 | stalls_3 | stalls_4 | stalls_5 | stalls_6; // @[FIFOFixer.scala 83:49]
  wire  _bundleIn_0_a_ready_T = ~stall; // @[FIFOFixer.scala 88:50]
  wire  bundleIn_0_a_ready = auto_out_a_ready & _bundleIn_0_a_ready_T; // @[FIFOFixer.scala 88:33]
  wire  _a_first_T = bundleIn_0_a_ready & auto_in_a_valid; // @[Decoupled.scala 40:37]
  wire [22:0] _a_first_beats1_decode_T_1 = 23'hff << auto_in_a_bits_size; // @[package.scala 234:77]
  wire [7:0] _a_first_beats1_decode_T_3 = ~_a_first_beats1_decode_T_1[7:0]; // @[package.scala 234:46]
  wire [4:0] a_first_beats1_decode = _a_first_beats1_decode_T_3[7:3]; // @[Edges.scala 219:59]
  wire  a_first_beats1_opdata = ~auto_in_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [4:0] a_first_counter1 = a_first_counter - 5'h1; // @[Edges.scala 229:28]
  wire  _d_first_T = auto_in_d_ready & auto_out_d_valid; // @[Decoupled.scala 40:37]
  wire [22:0] _d_first_beats1_decode_T_1 = 23'hff << auto_out_d_bits_size; // @[package.scala 234:77]
  wire [7:0] _d_first_beats1_decode_T_3 = ~_d_first_beats1_decode_T_1[7:0]; // @[package.scala 234:46]
  wire [4:0] d_first_beats1_decode = _d_first_beats1_decode_T_3[7:3]; // @[Edges.scala 219:59]
  wire  d_first_beats1_opdata = auto_out_d_bits_opcode[0]; // @[Edges.scala 105:36]
  reg [4:0] d_first_counter; // @[Edges.scala 228:27]
  wire [4:0] d_first_counter1 = d_first_counter - 5'h1; // @[Edges.scala 229:28]
  wire  d_first_first = d_first_counter == 5'h0; // @[Edges.scala 230:25]
  wire  d_first = d_first_first & auto_out_d_bits_opcode != 3'h6; // @[FIFOFixer.scala 67:42]
  wire  _T_1 = a_first & _a_first_T; // @[FIFOFixer.scala 72:21]
  wire  _GEN_116 = _T_1 ? 7'h0 == auto_in_a_bits_source | flight_0 : flight_0; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_117 = _T_1 ? 7'h1 == auto_in_a_bits_source | flight_1 : flight_1; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_118 = _T_1 ? 7'h2 == auto_in_a_bits_source | flight_2 : flight_2; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_119 = _T_1 ? 7'h3 == auto_in_a_bits_source | flight_3 : flight_3; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_120 = _T_1 ? 7'h4 == auto_in_a_bits_source | flight_4 : flight_4; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_121 = _T_1 ? 7'h5 == auto_in_a_bits_source | flight_5 : flight_5; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_122 = _T_1 ? 7'h6 == auto_in_a_bits_source | flight_6 : flight_6; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_123 = _T_1 ? 7'h7 == auto_in_a_bits_source | flight_7 : flight_7; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_124 = _T_1 ? 7'h8 == auto_in_a_bits_source | flight_8 : flight_8; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_125 = _T_1 ? 7'h9 == auto_in_a_bits_source | flight_9 : flight_9; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_126 = _T_1 ? 7'ha == auto_in_a_bits_source | flight_10 : flight_10; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_127 = _T_1 ? 7'hb == auto_in_a_bits_source | flight_11 : flight_11; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_128 = _T_1 ? 7'hc == auto_in_a_bits_source | flight_12 : flight_12; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_129 = _T_1 ? 7'hd == auto_in_a_bits_source | flight_13 : flight_13; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_130 = _T_1 ? 7'he == auto_in_a_bits_source | flight_14 : flight_14; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_131 = _T_1 ? 7'hf == auto_in_a_bits_source | flight_15 : flight_15; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_132 = _T_1 ? 7'h10 == auto_in_a_bits_source | flight_16 : flight_16; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_133 = _T_1 ? 7'h11 == auto_in_a_bits_source | flight_17 : flight_17; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_134 = _T_1 ? 7'h12 == auto_in_a_bits_source | flight_18 : flight_18; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_135 = _T_1 ? 7'h13 == auto_in_a_bits_source | flight_19 : flight_19; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_136 = _T_1 ? 7'h14 == auto_in_a_bits_source | flight_20 : flight_20; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_137 = _T_1 ? 7'h15 == auto_in_a_bits_source | flight_21 : flight_21; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_138 = _T_1 ? 7'h16 == auto_in_a_bits_source | flight_22 : flight_22; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_139 = _T_1 ? 7'h17 == auto_in_a_bits_source | flight_23 : flight_23; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_140 = _T_1 ? 7'h18 == auto_in_a_bits_source | flight_24 : flight_24; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_141 = _T_1 ? 7'h19 == auto_in_a_bits_source | flight_25 : flight_25; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_142 = _T_1 ? 7'h1a == auto_in_a_bits_source | flight_26 : flight_26; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_143 = _T_1 ? 7'h1b == auto_in_a_bits_source | flight_27 : flight_27; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_144 = _T_1 ? 7'h1c == auto_in_a_bits_source | flight_28 : flight_28; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_145 = _T_1 ? 7'h1d == auto_in_a_bits_source | flight_29 : flight_29; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_146 = _T_1 ? 7'h1e == auto_in_a_bits_source | flight_30 : flight_30; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_147 = _T_1 ? 7'h1f == auto_in_a_bits_source | flight_31 : flight_31; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_148 = _T_1 ? 7'h20 == auto_in_a_bits_source | flight_32 : flight_32; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_149 = _T_1 ? 7'h21 == auto_in_a_bits_source | flight_33 : flight_33; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_150 = _T_1 ? 7'h22 == auto_in_a_bits_source | flight_34 : flight_34; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_151 = _T_1 ? 7'h23 == auto_in_a_bits_source | flight_35 : flight_35; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_182 = _T_1 ? 7'h42 == auto_in_a_bits_source | flight_66 : flight_66; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_183 = _T_1 ? 7'h43 == auto_in_a_bits_source | flight_67 : flight_67; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_184 = _T_1 ? 7'h44 == auto_in_a_bits_source | flight_68 : flight_68; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_185 = _T_1 ? 7'h45 == auto_in_a_bits_source | flight_69 : flight_69; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_186 = _T_1 ? 7'h46 == auto_in_a_bits_source | flight_70 : flight_70; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_187 = _T_1 ? 7'h47 == auto_in_a_bits_source | flight_71 : flight_71; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_188 = _T_1 ? 7'h48 == auto_in_a_bits_source | flight_72 : flight_72; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_214 = _T_1 ? 7'h62 == auto_in_a_bits_source | flight_98 : flight_98; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_215 = _T_1 ? 7'h63 == auto_in_a_bits_source | flight_99 : flight_99; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_216 = _T_1 ? 7'h64 == auto_in_a_bits_source | flight_100 : flight_100; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_217 = _T_1 ? 7'h65 == auto_in_a_bits_source | flight_101 : flight_101; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_218 = _T_1 ? 7'h66 == auto_in_a_bits_source | flight_102 : flight_102; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_219 = _T_1 ? 7'h67 == auto_in_a_bits_source | flight_103 : flight_103; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_220 = _T_1 ? 7'h68 == auto_in_a_bits_source | flight_104 : flight_104; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _T_3 = d_first & _d_first_T; // @[FIFOFixer.scala 73:21]
  wire  _stalls_id_T_1 = _a_first_T & stalls_a_sel; // @[FIFOFixer.scala 77:49]
  wire  _stalls_id_T_5 = _a_first_T & stalls_a_sel_1; // @[FIFOFixer.scala 77:49]
  wire  _stalls_id_T_9 = _a_first_T & stalls_a_sel_2; // @[FIFOFixer.scala 77:49]
  wire  _stalls_id_T_13 = _a_first_T & stalls_a_sel_3; // @[FIFOFixer.scala 77:49]
  wire  _stalls_id_T_17 = _a_first_T & stalls_a_sel_4; // @[FIFOFixer.scala 77:49]
  wire  _stalls_id_T_21 = _a_first_T & stalls_a_sel_5; // @[FIFOFixer.scala 77:49]
  wire  _stalls_id_T_25 = _a_first_T & stalls_a_sel_6; // @[FIFOFixer.scala 77:49]
  assign auto_in_a_ready = auto_out_a_ready & _bundleIn_0_a_ready_T; // @[FIFOFixer.scala 88:33]
  assign auto_in_d_valid = auto_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_param = auto_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_size = auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_source = auto_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_sink = auto_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_denied = auto_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_data = auto_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_corrupt = auto_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_out_a_valid = auto_in_a_valid & _bundleIn_0_a_ready_T; // @[FIFOFixer.scala 87:33]
  assign auto_out_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_size = auto_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_source = auto_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_bufferable = auto_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_modifiable = auto_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_readalloc = auto_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_writealloc = auto_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_privileged = auto_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_secure = auto_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_fetch = auto_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_data = auto_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_d_ready = auto_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      a_first_counter <= 5'h0;
    end else if (_a_first_T) begin
      if (a_first) begin
        if (a_first_beats1_opdata) begin
          a_first_counter <= a_first_beats1_decode;
        end else begin
          a_first_counter <= 5'h0;
        end
      end else begin
        a_first_counter <= a_first_counter1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_0 <= 1'h0;
    end else if (_T_3) begin
      if (7'h0 == auto_out_d_bits_source) begin
        flight_0 <= 1'h0;
      end else begin
        flight_0 <= _GEN_116;
      end
    end else begin
      flight_0 <= _GEN_116;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_1 <= 1'h0;
    end else if (_T_3) begin
      if (7'h1 == auto_out_d_bits_source) begin
        flight_1 <= 1'h0;
      end else begin
        flight_1 <= _GEN_117;
      end
    end else begin
      flight_1 <= _GEN_117;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_2 <= 1'h0;
    end else if (_T_3) begin
      if (7'h2 == auto_out_d_bits_source) begin
        flight_2 <= 1'h0;
      end else begin
        flight_2 <= _GEN_118;
      end
    end else begin
      flight_2 <= _GEN_118;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_3 <= 1'h0;
    end else if (_T_3) begin
      if (7'h3 == auto_out_d_bits_source) begin
        flight_3 <= 1'h0;
      end else begin
        flight_3 <= _GEN_119;
      end
    end else begin
      flight_3 <= _GEN_119;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_4 <= 1'h0;
    end else if (_T_3) begin
      if (7'h4 == auto_out_d_bits_source) begin
        flight_4 <= 1'h0;
      end else begin
        flight_4 <= _GEN_120;
      end
    end else begin
      flight_4 <= _GEN_120;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_5 <= 1'h0;
    end else if (_T_3) begin
      if (7'h5 == auto_out_d_bits_source) begin
        flight_5 <= 1'h0;
      end else begin
        flight_5 <= _GEN_121;
      end
    end else begin
      flight_5 <= _GEN_121;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_6 <= 1'h0;
    end else if (_T_3) begin
      if (7'h6 == auto_out_d_bits_source) begin
        flight_6 <= 1'h0;
      end else begin
        flight_6 <= _GEN_122;
      end
    end else begin
      flight_6 <= _GEN_122;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_7 <= 1'h0;
    end else if (_T_3) begin
      if (7'h7 == auto_out_d_bits_source) begin
        flight_7 <= 1'h0;
      end else begin
        flight_7 <= _GEN_123;
      end
    end else begin
      flight_7 <= _GEN_123;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id <= 4'h0;
    end else if (_stalls_id_T_1) begin
      stalls_id <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_8 <= 1'h0;
    end else if (_T_3) begin
      if (7'h8 == auto_out_d_bits_source) begin
        flight_8 <= 1'h0;
      end else begin
        flight_8 <= _GEN_124;
      end
    end else begin
      flight_8 <= _GEN_124;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_9 <= 1'h0;
    end else if (_T_3) begin
      if (7'h9 == auto_out_d_bits_source) begin
        flight_9 <= 1'h0;
      end else begin
        flight_9 <= _GEN_125;
      end
    end else begin
      flight_9 <= _GEN_125;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_10 <= 1'h0;
    end else if (_T_3) begin
      if (7'ha == auto_out_d_bits_source) begin
        flight_10 <= 1'h0;
      end else begin
        flight_10 <= _GEN_126;
      end
    end else begin
      flight_10 <= _GEN_126;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_11 <= 1'h0;
    end else if (_T_3) begin
      if (7'hb == auto_out_d_bits_source) begin
        flight_11 <= 1'h0;
      end else begin
        flight_11 <= _GEN_127;
      end
    end else begin
      flight_11 <= _GEN_127;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_12 <= 1'h0;
    end else if (_T_3) begin
      if (7'hc == auto_out_d_bits_source) begin
        flight_12 <= 1'h0;
      end else begin
        flight_12 <= _GEN_128;
      end
    end else begin
      flight_12 <= _GEN_128;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_13 <= 1'h0;
    end else if (_T_3) begin
      if (7'hd == auto_out_d_bits_source) begin
        flight_13 <= 1'h0;
      end else begin
        flight_13 <= _GEN_129;
      end
    end else begin
      flight_13 <= _GEN_129;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_14 <= 1'h0;
    end else if (_T_3) begin
      if (7'he == auto_out_d_bits_source) begin
        flight_14 <= 1'h0;
      end else begin
        flight_14 <= _GEN_130;
      end
    end else begin
      flight_14 <= _GEN_130;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_15 <= 1'h0;
    end else if (_T_3) begin
      if (7'hf == auto_out_d_bits_source) begin
        flight_15 <= 1'h0;
      end else begin
        flight_15 <= _GEN_131;
      end
    end else begin
      flight_15 <= _GEN_131;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id_1 <= 4'h0;
    end else if (_stalls_id_T_5) begin
      stalls_id_1 <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_16 <= 1'h0;
    end else if (_T_3) begin
      if (7'h10 == auto_out_d_bits_source) begin
        flight_16 <= 1'h0;
      end else begin
        flight_16 <= _GEN_132;
      end
    end else begin
      flight_16 <= _GEN_132;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_17 <= 1'h0;
    end else if (_T_3) begin
      if (7'h11 == auto_out_d_bits_source) begin
        flight_17 <= 1'h0;
      end else begin
        flight_17 <= _GEN_133;
      end
    end else begin
      flight_17 <= _GEN_133;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_18 <= 1'h0;
    end else if (_T_3) begin
      if (7'h12 == auto_out_d_bits_source) begin
        flight_18 <= 1'h0;
      end else begin
        flight_18 <= _GEN_134;
      end
    end else begin
      flight_18 <= _GEN_134;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_19 <= 1'h0;
    end else if (_T_3) begin
      if (7'h13 == auto_out_d_bits_source) begin
        flight_19 <= 1'h0;
      end else begin
        flight_19 <= _GEN_135;
      end
    end else begin
      flight_19 <= _GEN_135;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_20 <= 1'h0;
    end else if (_T_3) begin
      if (7'h14 == auto_out_d_bits_source) begin
        flight_20 <= 1'h0;
      end else begin
        flight_20 <= _GEN_136;
      end
    end else begin
      flight_20 <= _GEN_136;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_21 <= 1'h0;
    end else if (_T_3) begin
      if (7'h15 == auto_out_d_bits_source) begin
        flight_21 <= 1'h0;
      end else begin
        flight_21 <= _GEN_137;
      end
    end else begin
      flight_21 <= _GEN_137;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_22 <= 1'h0;
    end else if (_T_3) begin
      if (7'h16 == auto_out_d_bits_source) begin
        flight_22 <= 1'h0;
      end else begin
        flight_22 <= _GEN_138;
      end
    end else begin
      flight_22 <= _GEN_138;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_23 <= 1'h0;
    end else if (_T_3) begin
      if (7'h17 == auto_out_d_bits_source) begin
        flight_23 <= 1'h0;
      end else begin
        flight_23 <= _GEN_139;
      end
    end else begin
      flight_23 <= _GEN_139;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id_2 <= 4'h0;
    end else if (_stalls_id_T_9) begin
      stalls_id_2 <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_24 <= 1'h0;
    end else if (_T_3) begin
      if (7'h18 == auto_out_d_bits_source) begin
        flight_24 <= 1'h0;
      end else begin
        flight_24 <= _GEN_140;
      end
    end else begin
      flight_24 <= _GEN_140;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_25 <= 1'h0;
    end else if (_T_3) begin
      if (7'h19 == auto_out_d_bits_source) begin
        flight_25 <= 1'h0;
      end else begin
        flight_25 <= _GEN_141;
      end
    end else begin
      flight_25 <= _GEN_141;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_26 <= 1'h0;
    end else if (_T_3) begin
      if (7'h1a == auto_out_d_bits_source) begin
        flight_26 <= 1'h0;
      end else begin
        flight_26 <= _GEN_142;
      end
    end else begin
      flight_26 <= _GEN_142;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_27 <= 1'h0;
    end else if (_T_3) begin
      if (7'h1b == auto_out_d_bits_source) begin
        flight_27 <= 1'h0;
      end else begin
        flight_27 <= _GEN_143;
      end
    end else begin
      flight_27 <= _GEN_143;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_28 <= 1'h0;
    end else if (_T_3) begin
      if (7'h1c == auto_out_d_bits_source) begin
        flight_28 <= 1'h0;
      end else begin
        flight_28 <= _GEN_144;
      end
    end else begin
      flight_28 <= _GEN_144;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_29 <= 1'h0;
    end else if (_T_3) begin
      if (7'h1d == auto_out_d_bits_source) begin
        flight_29 <= 1'h0;
      end else begin
        flight_29 <= _GEN_145;
      end
    end else begin
      flight_29 <= _GEN_145;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_30 <= 1'h0;
    end else if (_T_3) begin
      if (7'h1e == auto_out_d_bits_source) begin
        flight_30 <= 1'h0;
      end else begin
        flight_30 <= _GEN_146;
      end
    end else begin
      flight_30 <= _GEN_146;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_31 <= 1'h0;
    end else if (_T_3) begin
      if (7'h1f == auto_out_d_bits_source) begin
        flight_31 <= 1'h0;
      end else begin
        flight_31 <= _GEN_147;
      end
    end else begin
      flight_31 <= _GEN_147;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id_3 <= 4'h0;
    end else if (_stalls_id_T_13) begin
      stalls_id_3 <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_32 <= 1'h0;
    end else if (_T_3) begin
      if (7'h20 == auto_out_d_bits_source) begin
        flight_32 <= 1'h0;
      end else begin
        flight_32 <= _GEN_148;
      end
    end else begin
      flight_32 <= _GEN_148;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_33 <= 1'h0;
    end else if (_T_3) begin
      if (7'h21 == auto_out_d_bits_source) begin
        flight_33 <= 1'h0;
      end else begin
        flight_33 <= _GEN_149;
      end
    end else begin
      flight_33 <= _GEN_149;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_34 <= 1'h0;
    end else if (_T_3) begin
      if (7'h22 == auto_out_d_bits_source) begin
        flight_34 <= 1'h0;
      end else begin
        flight_34 <= _GEN_150;
      end
    end else begin
      flight_34 <= _GEN_150;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_35 <= 1'h0;
    end else if (_T_3) begin
      if (7'h23 == auto_out_d_bits_source) begin
        flight_35 <= 1'h0;
      end else begin
        flight_35 <= _GEN_151;
      end
    end else begin
      flight_35 <= _GEN_151;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id_4 <= 4'h0;
    end else if (_stalls_id_T_17) begin
      stalls_id_4 <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_98 <= 1'h0;
    end else if (_T_3) begin
      if (7'h62 == auto_out_d_bits_source) begin
        flight_98 <= 1'h0;
      end else begin
        flight_98 <= _GEN_214;
      end
    end else begin
      flight_98 <= _GEN_214;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_99 <= 1'h0;
    end else if (_T_3) begin
      if (7'h63 == auto_out_d_bits_source) begin
        flight_99 <= 1'h0;
      end else begin
        flight_99 <= _GEN_215;
      end
    end else begin
      flight_99 <= _GEN_215;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_100 <= 1'h0;
    end else if (_T_3) begin
      if (7'h64 == auto_out_d_bits_source) begin
        flight_100 <= 1'h0;
      end else begin
        flight_100 <= _GEN_216;
      end
    end else begin
      flight_100 <= _GEN_216;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_101 <= 1'h0;
    end else if (_T_3) begin
      if (7'h65 == auto_out_d_bits_source) begin
        flight_101 <= 1'h0;
      end else begin
        flight_101 <= _GEN_217;
      end
    end else begin
      flight_101 <= _GEN_217;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_102 <= 1'h0;
    end else if (_T_3) begin
      if (7'h66 == auto_out_d_bits_source) begin
        flight_102 <= 1'h0;
      end else begin
        flight_102 <= _GEN_218;
      end
    end else begin
      flight_102 <= _GEN_218;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_103 <= 1'h0;
    end else if (_T_3) begin
      if (7'h67 == auto_out_d_bits_source) begin
        flight_103 <= 1'h0;
      end else begin
        flight_103 <= _GEN_219;
      end
    end else begin
      flight_103 <= _GEN_219;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_104 <= 1'h0;
    end else if (_T_3) begin
      if (7'h68 == auto_out_d_bits_source) begin
        flight_104 <= 1'h0;
      end else begin
        flight_104 <= _GEN_220;
      end
    end else begin
      flight_104 <= _GEN_220;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id_5 <= 4'h0;
    end else if (_stalls_id_T_21) begin
      stalls_id_5 <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_66 <= 1'h0;
    end else if (_T_3) begin
      if (7'h42 == auto_out_d_bits_source) begin
        flight_66 <= 1'h0;
      end else begin
        flight_66 <= _GEN_182;
      end
    end else begin
      flight_66 <= _GEN_182;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_67 <= 1'h0;
    end else if (_T_3) begin
      if (7'h43 == auto_out_d_bits_source) begin
        flight_67 <= 1'h0;
      end else begin
        flight_67 <= _GEN_183;
      end
    end else begin
      flight_67 <= _GEN_183;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_68 <= 1'h0;
    end else if (_T_3) begin
      if (7'h44 == auto_out_d_bits_source) begin
        flight_68 <= 1'h0;
      end else begin
        flight_68 <= _GEN_184;
      end
    end else begin
      flight_68 <= _GEN_184;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_69 <= 1'h0;
    end else if (_T_3) begin
      if (7'h45 == auto_out_d_bits_source) begin
        flight_69 <= 1'h0;
      end else begin
        flight_69 <= _GEN_185;
      end
    end else begin
      flight_69 <= _GEN_185;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_70 <= 1'h0;
    end else if (_T_3) begin
      if (7'h46 == auto_out_d_bits_source) begin
        flight_70 <= 1'h0;
      end else begin
        flight_70 <= _GEN_186;
      end
    end else begin
      flight_70 <= _GEN_186;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_71 <= 1'h0;
    end else if (_T_3) begin
      if (7'h47 == auto_out_d_bits_source) begin
        flight_71 <= 1'h0;
      end else begin
        flight_71 <= _GEN_187;
      end
    end else begin
      flight_71 <= _GEN_187;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_72 <= 1'h0;
    end else if (_T_3) begin
      if (7'h48 == auto_out_d_bits_source) begin
        flight_72 <= 1'h0;
      end else begin
        flight_72 <= _GEN_188;
      end
    end else begin
      flight_72 <= _GEN_188;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id_6 <= 4'h0;
    end else if (_stalls_id_T_25) begin
      stalls_id_6 <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      d_first_counter <= 5'h0;
    end else if (_d_first_T) begin
      if (d_first_first) begin
        if (d_first_beats1_opdata) begin
          d_first_counter <= d_first_beats1_decode;
        end else begin
          d_first_counter <= 5'h0;
        end
      end else begin
        d_first_counter <= d_first_counter1;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  a_first_counter = _RAND_0[4:0];
  _RAND_1 = {1{`RANDOM}};
  flight_0 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  flight_1 = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  flight_2 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  flight_3 = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  flight_4 = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  flight_5 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  flight_6 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  flight_7 = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  stalls_id = _RAND_9[3:0];
  _RAND_10 = {1{`RANDOM}};
  flight_8 = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  flight_9 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  flight_10 = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  flight_11 = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  flight_12 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  flight_13 = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  flight_14 = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  flight_15 = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  stalls_id_1 = _RAND_18[3:0];
  _RAND_19 = {1{`RANDOM}};
  flight_16 = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  flight_17 = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  flight_18 = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  flight_19 = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  flight_20 = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  flight_21 = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  flight_22 = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  flight_23 = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  stalls_id_2 = _RAND_27[3:0];
  _RAND_28 = {1{`RANDOM}};
  flight_24 = _RAND_28[0:0];
  _RAND_29 = {1{`RANDOM}};
  flight_25 = _RAND_29[0:0];
  _RAND_30 = {1{`RANDOM}};
  flight_26 = _RAND_30[0:0];
  _RAND_31 = {1{`RANDOM}};
  flight_27 = _RAND_31[0:0];
  _RAND_32 = {1{`RANDOM}};
  flight_28 = _RAND_32[0:0];
  _RAND_33 = {1{`RANDOM}};
  flight_29 = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  flight_30 = _RAND_34[0:0];
  _RAND_35 = {1{`RANDOM}};
  flight_31 = _RAND_35[0:0];
  _RAND_36 = {1{`RANDOM}};
  stalls_id_3 = _RAND_36[3:0];
  _RAND_37 = {1{`RANDOM}};
  flight_32 = _RAND_37[0:0];
  _RAND_38 = {1{`RANDOM}};
  flight_33 = _RAND_38[0:0];
  _RAND_39 = {1{`RANDOM}};
  flight_34 = _RAND_39[0:0];
  _RAND_40 = {1{`RANDOM}};
  flight_35 = _RAND_40[0:0];
  _RAND_41 = {1{`RANDOM}};
  stalls_id_4 = _RAND_41[3:0];
  _RAND_42 = {1{`RANDOM}};
  flight_98 = _RAND_42[0:0];
  _RAND_43 = {1{`RANDOM}};
  flight_99 = _RAND_43[0:0];
  _RAND_44 = {1{`RANDOM}};
  flight_100 = _RAND_44[0:0];
  _RAND_45 = {1{`RANDOM}};
  flight_101 = _RAND_45[0:0];
  _RAND_46 = {1{`RANDOM}};
  flight_102 = _RAND_46[0:0];
  _RAND_47 = {1{`RANDOM}};
  flight_103 = _RAND_47[0:0];
  _RAND_48 = {1{`RANDOM}};
  flight_104 = _RAND_48[0:0];
  _RAND_49 = {1{`RANDOM}};
  stalls_id_5 = _RAND_49[3:0];
  _RAND_50 = {1{`RANDOM}};
  flight_66 = _RAND_50[0:0];
  _RAND_51 = {1{`RANDOM}};
  flight_67 = _RAND_51[0:0];
  _RAND_52 = {1{`RANDOM}};
  flight_68 = _RAND_52[0:0];
  _RAND_53 = {1{`RANDOM}};
  flight_69 = _RAND_53[0:0];
  _RAND_54 = {1{`RANDOM}};
  flight_70 = _RAND_54[0:0];
  _RAND_55 = {1{`RANDOM}};
  flight_71 = _RAND_55[0:0];
  _RAND_56 = {1{`RANDOM}};
  flight_72 = _RAND_56[0:0];
  _RAND_57 = {1{`RANDOM}};
  stalls_id_6 = _RAND_57[3:0];
  _RAND_58 = {1{`RANDOM}};
  d_first_counter = _RAND_58[4:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    a_first_counter = 5'h0;
  end
  if (reset) begin
    flight_0 = 1'h0;
  end
  if (reset) begin
    flight_1 = 1'h0;
  end
  if (reset) begin
    flight_2 = 1'h0;
  end
  if (reset) begin
    flight_3 = 1'h0;
  end
  if (reset) begin
    flight_4 = 1'h0;
  end
  if (reset) begin
    flight_5 = 1'h0;
  end
  if (reset) begin
    flight_6 = 1'h0;
  end
  if (reset) begin
    flight_7 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id = 4'h0;
  end
  if (reset) begin
    flight_8 = 1'h0;
  end
  if (reset) begin
    flight_9 = 1'h0;
  end
  if (reset) begin
    flight_10 = 1'h0;
  end
  if (reset) begin
    flight_11 = 1'h0;
  end
  if (reset) begin
    flight_12 = 1'h0;
  end
  if (reset) begin
    flight_13 = 1'h0;
  end
  if (reset) begin
    flight_14 = 1'h0;
  end
  if (reset) begin
    flight_15 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id_1 = 4'h0;
  end
  if (reset) begin
    flight_16 = 1'h0;
  end
  if (reset) begin
    flight_17 = 1'h0;
  end
  if (reset) begin
    flight_18 = 1'h0;
  end
  if (reset) begin
    flight_19 = 1'h0;
  end
  if (reset) begin
    flight_20 = 1'h0;
  end
  if (reset) begin
    flight_21 = 1'h0;
  end
  if (reset) begin
    flight_22 = 1'h0;
  end
  if (reset) begin
    flight_23 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id_2 = 4'h0;
  end
  if (reset) begin
    flight_24 = 1'h0;
  end
  if (reset) begin
    flight_25 = 1'h0;
  end
  if (reset) begin
    flight_26 = 1'h0;
  end
  if (reset) begin
    flight_27 = 1'h0;
  end
  if (reset) begin
    flight_28 = 1'h0;
  end
  if (reset) begin
    flight_29 = 1'h0;
  end
  if (reset) begin
    flight_30 = 1'h0;
  end
  if (reset) begin
    flight_31 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id_3 = 4'h0;
  end
  if (reset) begin
    flight_32 = 1'h0;
  end
  if (reset) begin
    flight_33 = 1'h0;
  end
  if (reset) begin
    flight_34 = 1'h0;
  end
  if (reset) begin
    flight_35 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id_4 = 4'h0;
  end
  if (reset) begin
    flight_98 = 1'h0;
  end
  if (reset) begin
    flight_99 = 1'h0;
  end
  if (reset) begin
    flight_100 = 1'h0;
  end
  if (reset) begin
    flight_101 = 1'h0;
  end
  if (reset) begin
    flight_102 = 1'h0;
  end
  if (reset) begin
    flight_103 = 1'h0;
  end
  if (reset) begin
    flight_104 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id_5 = 4'h0;
  end
  if (reset) begin
    flight_66 = 1'h0;
  end
  if (reset) begin
    flight_67 = 1'h0;
  end
  if (reset) begin
    flight_68 = 1'h0;
  end
  if (reset) begin
    flight_69 = 1'h0;
  end
  if (reset) begin
    flight_70 = 1'h0;
  end
  if (reset) begin
    flight_71 = 1'h0;
  end
  if (reset) begin
    flight_72 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id_6 = 4'h0;
  end
  if (reset) begin
    d_first_counter = 5'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
