//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
interface SiFive_Insight_MStatus(
);
  logic [31:0] isa;
  logic [1:0] dprv;
  logic  sd;
  logic [26:0] zero2;
  logic [1:0] sxl;
  logic [1:0] uxl;
  logic  sd_rv32;
  logic [7:0] zero1;
  logic  tsr;
  logic  tw;
  logic  tvm;
  logic  mxr;
  logic  sum;
  logic  mprv;
  logic [1:0] xs;
  logic [1:0] fs;
  logic [1:0] mpp;
  logic [1:0] vs;
  logic  spp;
  logic  mpie;
  logic  hpie;
  logic  spie;
  logic  upie;
  logic  mie;
  logic  hie;
  logic  sie;
  logic  uie;
endinterface
