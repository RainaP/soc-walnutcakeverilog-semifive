//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_BankedStore_assert(
  input   clock,
  input   reset,
  input   io_side_adr_valid,
  input   io_side_adr_bits_write,
  input   io_side_wdat_poison,
  input   io_sinkC_adr_valid,
  input   io_sinkC_adr_bits_noop,
  input   io_sinkC_dat_poison,
  input   io_sourceD_wadr_valid,
  input   io_sourceD_wdat_poison
);
  wire  _T_2; // @[BankedStore.scala 147:46]
  wire  _T_3; // @[BankedStore.scala 147:55]
  wire  _T_6; // @[BankedStore.scala 147:37]
  wire  _T_8; // @[BankedStore.scala 147:36]
  wire  _T_9; // @[BankedStore.scala 147:36]
  wire  _T_171; // @[BankedStore.scala 147:55]
  wire  _T_172; // @[BankedStore.scala 147:69]
  wire  _T_173; // @[BankedStore.scala 147:66]
  wire  _T_174; // @[BankedStore.scala 147:37]
  wire  _T_176; // @[BankedStore.scala 147:36]
  wire  _T_177; // @[BankedStore.scala 147:36]
  wire  _T_782; // @[BankedStore.scala 147:55]
  wire  _T_785; // @[BankedStore.scala 147:37]
  wire  _T_787; // @[BankedStore.scala 147:36]
  wire  _T_788; // @[BankedStore.scala 147:36]
  assign _T_2 = io_side_wdat_poison & io_side_adr_bits_write; // @[BankedStore.scala 147:46]
  assign _T_3 = _T_2 & io_side_adr_valid; // @[BankedStore.scala 147:55]
  assign _T_6 = ~_T_3; // @[BankedStore.scala 147:37]
  assign _T_8 = _T_6 | reset; // @[BankedStore.scala 147:36]
  assign _T_9 = ~_T_8; // @[BankedStore.scala 147:36]
  assign _T_171 = io_sinkC_dat_poison & io_sinkC_adr_valid; // @[BankedStore.scala 147:55]
  assign _T_172 = ~io_sinkC_adr_bits_noop; // @[BankedStore.scala 147:69]
  assign _T_173 = _T_171 & _T_172; // @[BankedStore.scala 147:66]
  assign _T_174 = ~_T_173; // @[BankedStore.scala 147:37]
  assign _T_176 = _T_174 | reset; // @[BankedStore.scala 147:36]
  assign _T_177 = ~_T_176; // @[BankedStore.scala 147:36]
  assign _T_782 = io_sourceD_wdat_poison & io_sourceD_wadr_valid; // @[BankedStore.scala 147:55]
  assign _T_785 = ~_T_782; // @[BankedStore.scala 147:37]
  assign _T_787 = _T_785 | reset; // @[BankedStore.scala 147:36]
  assign _T_788 = ~_T_787; // @[BankedStore.scala 147:36]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_9) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at BankedStore.scala:147 if (!code.canDetect) { assert(!(poison && write && b.valid && !b.bits.noop)) }\n"); // @[BankedStore.scala 147:36]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_9) begin
          $fatal; // @[BankedStore.scala 147:36]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_177) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at BankedStore.scala:147 if (!code.canDetect) { assert(!(poison && write && b.valid && !b.bits.noop)) }\n"); // @[BankedStore.scala 147:36]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_177) begin
          $fatal; // @[BankedStore.scala 147:36]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_788) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at BankedStore.scala:147 if (!code.canDetect) { assert(!(poison && write && b.valid && !b.bits.noop)) }\n"); // @[BankedStore.scala 147:36]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_788) begin
          $fatal; // @[BankedStore.scala 147:36]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
