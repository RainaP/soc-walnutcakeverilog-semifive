//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_RationalCrossingSource_4_assert(
  input   clock,
  input   reset,
  input   equal,
  input   _T_3,
  input   _T_4
);
  wire  _T_14; // @[RationalCrossing.scala 99:50]
  wire  _T_15; // @[RationalCrossing.scala 99:38]
  wire  _T_17; // @[RationalCrossing.scala 99:31]
  wire  _T_18; // @[RationalCrossing.scala 99:31]
  assign _T_14 = _T_3 == _T_4; // @[RationalCrossing.scala 99:50]
  assign _T_15 = equal | _T_14; // @[RationalCrossing.scala 99:38]
  assign _T_17 = _T_15 | reset; // @[RationalCrossing.scala 99:31]
  assign _T_18 = ~_T_17; // @[RationalCrossing.scala 99:31]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_18) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at RationalCrossing.scala:99 case FastToSlow => assert (equal || count(1) === deq.sink(0))\n"); // @[RationalCrossing.scala 99:31]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_18) begin
          $fatal; // @[RationalCrossing.scala 99:31]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
