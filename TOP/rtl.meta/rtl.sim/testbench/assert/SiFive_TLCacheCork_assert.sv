//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLCacheCork_assert(
  input          clock,
  input          reset,
  input          auto_in_a_valid,
  input  [2:0]   auto_in_a_bits_opcode,
  input  [2:0]   auto_in_a_bits_param,
  input  [2:0]   auto_in_a_bits_size,
  input  [2:0]   auto_in_a_bits_source,
  input  [35:0]  auto_in_a_bits_address,
  input  [15:0]  auto_in_a_bits_mask,
  input          auto_in_a_bits_corrupt,
  input          auto_in_c_valid,
  input  [2:0]   auto_in_c_bits_opcode,
  input  [2:0]   auto_in_c_bits_param,
  input  [2:0]   auto_in_c_bits_size,
  input  [2:0]   auto_in_c_bits_source,
  input  [35:0]  auto_in_c_bits_address,
  input          auto_in_c_bits_corrupt,
  input          auto_in_d_ready,
  input          auto_in_e_valid,
  input  [2:0]   auto_in_e_bits_sink,
  input          _T_11,
  input          _T_3_ready,
  input          _T_26,
  input          _T_310,
  input          _T_165,
  input          _T_164_ready,
  input          _T_309,
  input          _T_197,
  input          _T_391,
  input          _T_392,
  input          _T_393,
  input  [143:0] _T_471,
  input          _T_219,
  input          _T_438,
  input          _T_445,
  input  [2:0]   _T_221,
  input          _T_276,
  input          _T_277,
  input          _T_291,
  input  [2:0]   IDPool_io_alloc_bits
);
  wire  TLMonitor_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [15:0] TLMonitor_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_c_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_c_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_c_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_c_bits_param; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_c_bits_size; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_c_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_io_in_c_bits_address; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_c_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_d_bits_param; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_e_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_e_bits_sink; // @[Nodes.scala 25:25]
  wire  _T_168; // @[CacheCork.scala 110:17]
  wire  _T_170; // @[CacheCork.scala 110:29]
  wire  _T_172; // @[CacheCork.scala 110:61]
  wire  _T_174; // @[CacheCork.scala 110:16]
  wire  _T_175; // @[CacheCork.scala 110:16]
  wire  _T_280; // @[Arbiter.scala 74:52]
  wire  _T_282; // @[Arbiter.scala 75:62]
  wire  _T_285; // @[Arbiter.scala 75:62]
  wire  _T_286; // @[Arbiter.scala 75:59]
  wire  _T_289; // @[Arbiter.scala 75:13]
  wire  _T_290; // @[Arbiter.scala 75:13]
  wire  _T_292; // @[Arbiter.scala 77:15]
  wire  _T_294; // @[Arbiter.scala 77:36]
  wire  _T_296; // @[Arbiter.scala 77:14]
  wire  _T_297; // @[Arbiter.scala 77:14]
  wire  _T_396; // @[Arbiter.scala 74:52]
  wire  _T_397; // @[Arbiter.scala 74:52]
  wire  _T_399; // @[Arbiter.scala 75:62]
  wire  _T_402; // @[Arbiter.scala 75:62]
  wire  _T_403; // @[Arbiter.scala 75:59]
  wire  _T_404; // @[Arbiter.scala 75:56]
  wire  _T_405; // @[Arbiter.scala 75:62]
  wire  _T_406; // @[Arbiter.scala 75:59]
  wire  _T_408; // @[Arbiter.scala 75:77]
  wire  _T_410; // @[Arbiter.scala 75:13]
  wire  _T_411; // @[Arbiter.scala 75:13]
  wire  _T_414; // @[Arbiter.scala 77:15]
  wire  _T_417; // @[Arbiter.scala 77:36]
  wire  _T_419; // @[Arbiter.scala 77:14]
  wire  _T_420; // @[Arbiter.scala 77:14]
  SiFive_TLMonitor_91_assert TLMonitor ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_clock),
    .reset(TLMonitor_reset),
    .io_in_a_ready(TLMonitor_io_in_a_ready),
    .io_in_a_valid(TLMonitor_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_io_in_a_bits_corrupt),
    .io_in_c_ready(TLMonitor_io_in_c_ready),
    .io_in_c_valid(TLMonitor_io_in_c_valid),
    .io_in_c_bits_opcode(TLMonitor_io_in_c_bits_opcode),
    .io_in_c_bits_param(TLMonitor_io_in_c_bits_param),
    .io_in_c_bits_size(TLMonitor_io_in_c_bits_size),
    .io_in_c_bits_source(TLMonitor_io_in_c_bits_source),
    .io_in_c_bits_address(TLMonitor_io_in_c_bits_address),
    .io_in_c_bits_corrupt(TLMonitor_io_in_c_bits_corrupt),
    .io_in_d_ready(TLMonitor_io_in_d_ready),
    .io_in_d_valid(TLMonitor_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_io_in_d_bits_opcode),
    .io_in_d_bits_param(TLMonitor_io_in_d_bits_param),
    .io_in_d_bits_size(TLMonitor_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_io_in_d_bits_source),
    .io_in_d_bits_sink(TLMonitor_io_in_d_bits_sink),
    .io_in_d_bits_denied(TLMonitor_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_io_in_d_bits_corrupt),
    .io_in_e_valid(TLMonitor_io_in_e_valid),
    .io_in_e_bits_sink(TLMonitor_io_in_e_bits_sink)
  );
  assign _T_168 = ~auto_in_c_valid; // @[CacheCork.scala 110:17]
  assign _T_170 = _T_168 | _T_165; // @[CacheCork.scala 110:29]
  assign _T_172 = _T_170 | _T_26; // @[CacheCork.scala 110:61]
  assign _T_174 = _T_172 | reset; // @[CacheCork.scala 110:16]
  assign _T_175 = ~_T_174; // @[CacheCork.scala 110:16]
  assign _T_280 = _T_276 | _T_277; // @[Arbiter.scala 74:52]
  assign _T_282 = ~_T_276; // @[Arbiter.scala 75:62]
  assign _T_285 = ~_T_277; // @[Arbiter.scala 75:62]
  assign _T_286 = _T_282 | _T_285; // @[Arbiter.scala 75:59]
  assign _T_289 = _T_286 | reset; // @[Arbiter.scala 75:13]
  assign _T_290 = ~_T_289; // @[Arbiter.scala 75:13]
  assign _T_292 = ~_T_291; // @[Arbiter.scala 77:15]
  assign _T_294 = _T_292 | _T_280; // @[Arbiter.scala 77:36]
  assign _T_296 = _T_294 | reset; // @[Arbiter.scala 77:14]
  assign _T_297 = ~_T_296; // @[Arbiter.scala 77:14]
  assign _T_396 = _T_391 | _T_392; // @[Arbiter.scala 74:52]
  assign _T_397 = _T_396 | _T_393; // @[Arbiter.scala 74:52]
  assign _T_399 = ~_T_391; // @[Arbiter.scala 75:62]
  assign _T_402 = ~_T_392; // @[Arbiter.scala 75:62]
  assign _T_403 = _T_399 | _T_402; // @[Arbiter.scala 75:59]
  assign _T_404 = ~_T_396; // @[Arbiter.scala 75:56]
  assign _T_405 = ~_T_393; // @[Arbiter.scala 75:62]
  assign _T_406 = _T_404 | _T_405; // @[Arbiter.scala 75:59]
  assign _T_408 = _T_403 & _T_406; // @[Arbiter.scala 75:77]
  assign _T_410 = _T_408 | reset; // @[Arbiter.scala 75:13]
  assign _T_411 = ~_T_410; // @[Arbiter.scala 75:13]
  assign _T_414 = ~_T_438; // @[Arbiter.scala 77:15]
  assign _T_417 = _T_414 | _T_397; // @[Arbiter.scala 77:36]
  assign _T_419 = _T_417 | reset; // @[Arbiter.scala 77:14]
  assign _T_420 = ~_T_419; // @[Arbiter.scala 77:14]
  assign TLMonitor_clock = clock;
  assign TLMonitor_reset = reset;
  assign TLMonitor_io_in_a_ready = _T_11 ? _T_3_ready : _T_310; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_valid = auto_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_size = auto_in_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_source = auto_in_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_ready = _T_165 ? _T_164_ready : _T_309; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_valid = auto_in_c_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_opcode = auto_in_c_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_param = auto_in_c_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_size = auto_in_c_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_source = auto_in_c_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_address = auto_in_c_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_corrupt = auto_in_c_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_ready = auto_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_valid = _T_445 & _T_219; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_opcode = _T_471[143:141]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_param = _T_471[140:139]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_size = _T_471[138:136]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_source = _T_471[135:133]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_sink = _T_197 ? IDPool_io_alloc_bits : _T_221; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_denied = _T_471[129]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_corrupt = _T_471[0]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_e_valid = auto_in_e_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_e_bits_sink = auto_in_e_bits_sink; // @[Nodes.scala 26:19]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_175) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at CacheCork.scala:110 assert (!in.c.valid || in.c.bits.opcode === Release || in.c.bits.opcode === ReleaseData)\n"); // @[CacheCork.scala 110:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_175) begin
          $fatal; // @[CacheCork.scala 110:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_290) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_290) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_297) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_297) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_411) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_411) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_420) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_420) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
