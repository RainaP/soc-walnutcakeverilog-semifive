//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_AXI4ToTL_assert(
  input         clock,
  input         reset,
  input         auto_in_aw_valid,
  input  [7:0]  auto_in_aw_bits_len,
  input  [2:0]  auto_in_aw_bits_size,
  input         auto_in_ar_valid,
  input  [14:0] _T_5,
  input  [3:0]  _T_26,
  input  [14:0] _T_284,
  input  [3:0]  _T_305,
  input         _T_552,
  input         _T_553,
  input         _T_567
);
  wire  _T_111; // @[ToTL.scala 98:15]
  wire [29:0] _T_113; // @[package.scala 189:77]
  wire [14:0] _T_115; // @[package.scala 189:46]
  wire  _T_116; // @[ToTL.scala 98:39]
  wire  _T_117; // @[ToTL.scala 98:28]
  wire  _T_119; // @[ToTL.scala 98:14]
  wire  _T_120; // @[ToTL.scala 98:14]
  wire  _T_395; // @[ToTL.scala 131:15]
  wire [29:0] _T_397; // @[package.scala 189:77]
  wire [14:0] _T_399; // @[package.scala 189:46]
  wire  _T_400; // @[ToTL.scala 131:39]
  wire  _T_401; // @[ToTL.scala 131:28]
  wire  _T_403; // @[ToTL.scala 131:14]
  wire  _T_404; // @[ToTL.scala 131:14]
  wire  _T_406; // @[ToTL.scala 132:46]
  wire  _T_407; // @[ToTL.scala 132:28]
  wire  _T_408; // @[ToTL.scala 132:77]
  wire  _T_409; // @[ToTL.scala 132:58]
  wire  _T_411; // @[ToTL.scala 132:14]
  wire  _T_412; // @[ToTL.scala 132:14]
  wire  _T_556; // @[Arbiter.scala 74:52]
  wire  _T_558; // @[Arbiter.scala 75:62]
  wire  _T_561; // @[Arbiter.scala 75:62]
  wire  _T_562; // @[Arbiter.scala 75:59]
  wire  _T_565; // @[Arbiter.scala 75:13]
  wire  _T_566; // @[Arbiter.scala 75:13]
  wire  _T_568; // @[Arbiter.scala 77:15]
  wire  _T_570; // @[Arbiter.scala 77:36]
  wire  _T_572; // @[Arbiter.scala 77:14]
  wire  _T_573; // @[Arbiter.scala 77:14]
  assign _T_111 = ~auto_in_ar_valid; // @[ToTL.scala 98:15]
  assign _T_113 = 30'h7fff << _T_26; // @[package.scala 189:77]
  assign _T_115 = ~_T_113[14:0]; // @[package.scala 189:46]
  assign _T_116 = _T_5 == _T_115; // @[ToTL.scala 98:39]
  assign _T_117 = _T_111 | _T_116; // @[ToTL.scala 98:28]
  assign _T_119 = _T_117 | reset; // @[ToTL.scala 98:14]
  assign _T_120 = ~_T_119; // @[ToTL.scala 98:14]
  assign _T_395 = ~auto_in_aw_valid; // @[ToTL.scala 131:15]
  assign _T_397 = 30'h7fff << _T_305; // @[package.scala 189:77]
  assign _T_399 = ~_T_397[14:0]; // @[package.scala 189:46]
  assign _T_400 = _T_284 == _T_399; // @[ToTL.scala 131:39]
  assign _T_401 = _T_395 | _T_400; // @[ToTL.scala 131:28]
  assign _T_403 = _T_401 | reset; // @[ToTL.scala 131:14]
  assign _T_404 = ~_T_403; // @[ToTL.scala 131:14]
  assign _T_406 = auto_in_aw_bits_len == 8'h0; // @[ToTL.scala 132:46]
  assign _T_407 = _T_395 | _T_406; // @[ToTL.scala 132:28]
  assign _T_408 = auto_in_aw_bits_size == 3'h3; // @[ToTL.scala 132:77]
  assign _T_409 = _T_407 | _T_408; // @[ToTL.scala 132:58]
  assign _T_411 = _T_409 | reset; // @[ToTL.scala 132:14]
  assign _T_412 = ~_T_411; // @[ToTL.scala 132:14]
  assign _T_556 = _T_552 | _T_553; // @[Arbiter.scala 74:52]
  assign _T_558 = ~_T_552; // @[Arbiter.scala 75:62]
  assign _T_561 = ~_T_553; // @[Arbiter.scala 75:62]
  assign _T_562 = _T_558 | _T_561; // @[Arbiter.scala 75:59]
  assign _T_565 = _T_562 | reset; // @[Arbiter.scala 75:13]
  assign _T_566 = ~_T_565; // @[Arbiter.scala 75:13]
  assign _T_568 = ~_T_567; // @[Arbiter.scala 77:15]
  assign _T_570 = _T_568 | _T_556; // @[Arbiter.scala 77:36]
  assign _T_572 = _T_570 | reset; // @[Arbiter.scala 77:14]
  assign _T_573 = ~_T_572; // @[Arbiter.scala 77:14]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_120) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToTL.scala:98 assert (!in.ar.valid || r_size1 === UIntToOH1(r_size, beatCountBits)) // because aligned\n"); // @[ToTL.scala 98:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_120) begin
          $fatal; // @[ToTL.scala 98:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_404) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToTL.scala:131 assert (!in.aw.valid || w_size1 === UIntToOH1(w_size, beatCountBits)) // because aligned\n"); // @[ToTL.scala 131:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_404) begin
          $fatal; // @[ToTL.scala 131:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_412) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToTL.scala:132 assert (!in.aw.valid || in.aw.bits.len === UInt(0) || in.aw.bits.size === UInt(log2Ceil(beatBytes))) // because aligned\n"); // @[ToTL.scala 132:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_412) begin
          $fatal; // @[ToTL.scala 132:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_566) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_566) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_573) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_573) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
