//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__Queue_172(
  input         rf_reset,
  input         clock,
  input         reset,
  output        io_enq_ready,
  input         io_enq_valid,
  input         io_enq_bits_read,
  input  [9:0]  io_enq_bits_index,
  input  [31:0] io_enq_bits_data,
  input  [3:0]  io_enq_bits_mask,
  input  [11:0] io_enq_bits_extra_tlrr_extra_source,
  input  [1:0]  io_enq_bits_extra_tlrr_extra_size,
  input         io_deq_ready,
  output        io_deq_valid,
  output        io_deq_bits_read,
  output [9:0]  io_deq_bits_index,
  output [31:0] io_deq_bits_data,
  output [3:0]  io_deq_bits_mask,
  output [11:0] io_deq_bits_extra_tlrr_extra_source,
  output [1:0]  io_deq_bits_extra_tlrr_extra_size
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
`endif // RANDOMIZE_REG_INIT
  reg  ram_0_read; // @[Decoupled.scala 218:16]
  reg [9:0] ram_0_index; // @[Decoupled.scala 218:16]
  reg [31:0] ram_0_data; // @[Decoupled.scala 218:16]
  reg [3:0] ram_0_mask; // @[Decoupled.scala 218:16]
  reg [11:0] ram_0_extra_tlrr_extra_source; // @[Decoupled.scala 218:16]
  reg [1:0] ram_0_extra_tlrr_extra_size; // @[Decoupled.scala 218:16]
  wire  do_enq = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  reg  maybe_full; // @[Decoupled.scala 221:27]
  wire  empty = ~maybe_full; // @[Decoupled.scala 224:28]
  wire  do_deq = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  assign io_enq_ready = ~maybe_full; // @[Decoupled.scala 241:19]
  assign io_deq_valid = ~empty; // @[Decoupled.scala 240:19]
  assign io_deq_bits_read = ram_0_read; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_index = ram_0_index; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_data = ram_0_data; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_mask = ram_0_mask; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_extra_tlrr_extra_source = ram_0_extra_tlrr_extra_source; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_extra_tlrr_extra_size = ram_0_extra_tlrr_extra_size; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_read <= 1'h0;
    end else if (do_enq) begin
      ram_0_read <= io_enq_bits_read;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_index <= 10'h0;
    end else if (do_enq) begin
      ram_0_index <= io_enq_bits_index;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_data <= 32'h0;
    end else if (do_enq) begin
      ram_0_data <= io_enq_bits_data;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_mask <= 4'h0;
    end else if (do_enq) begin
      ram_0_mask <= io_enq_bits_mask;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_extra_tlrr_extra_source <= 12'h0;
    end else if (do_enq) begin
      ram_0_extra_tlrr_extra_source <= io_enq_bits_extra_tlrr_extra_source;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_extra_tlrr_extra_size <= 2'h0;
    end else if (do_enq) begin
      ram_0_extra_tlrr_extra_size <= io_enq_bits_extra_tlrr_extra_size;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      maybe_full <= 1'h0;
    end else if (do_enq != do_deq) begin
      maybe_full <= do_enq;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ram_0_read = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  ram_0_index = _RAND_1[9:0];
  _RAND_2 = {1{`RANDOM}};
  ram_0_data = _RAND_2[31:0];
  _RAND_3 = {1{`RANDOM}};
  ram_0_mask = _RAND_3[3:0];
  _RAND_4 = {1{`RANDOM}};
  ram_0_extra_tlrr_extra_source = _RAND_4[11:0];
  _RAND_5 = {1{`RANDOM}};
  ram_0_extra_tlrr_extra_size = _RAND_5[1:0];
  _RAND_6 = {1{`RANDOM}};
  maybe_full = _RAND_6[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ram_0_read = 1'h0;
  end
  if (rf_reset) begin
    ram_0_index = 10'h0;
  end
  if (rf_reset) begin
    ram_0_data = 32'h0;
  end
  if (rf_reset) begin
    ram_0_mask = 4'h0;
  end
  if (rf_reset) begin
    ram_0_extra_tlrr_extra_source = 12'h0;
  end
  if (rf_reset) begin
    ram_0_extra_tlrr_extra_size = 2'h0;
  end
  if (reset) begin
    maybe_full = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
