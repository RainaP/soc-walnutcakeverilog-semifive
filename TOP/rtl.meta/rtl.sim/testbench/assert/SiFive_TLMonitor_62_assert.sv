//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_62_assert(
  input        clock,
  input        reset,
  input        io_in_a_ready,
  input        io_in_a_valid,
  input  [2:0] io_in_a_bits_opcode,
  input  [6:0] io_in_a_bits_address,
  input  [3:0] io_in_a_bits_mask,
  input        io_in_d_ready,
  input        io_in_d_valid,
  input  [2:0] io_in_d_bits_opcode
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire [6:0] _T_10; // @[Edges.scala 22:16]
  wire  _T_11; // @[Edges.scala 22:24]
  wire  _T_56; // @[Monitor.scala 79:25]
  wire [6:0] _T_58; // @[Parameters.scala 137:31]
  wire [7:0] _T_59; // @[Parameters.scala 137:49]
  wire [7:0] _T_61; // @[Parameters.scala 137:52]
  wire  _T_62; // @[Parameters.scala 137:67]
  wire [6:0] _T_63; // @[Parameters.scala 137:31]
  wire [7:0] _T_64; // @[Parameters.scala 137:49]
  wire [7:0] _T_66; // @[Parameters.scala 137:52]
  wire  _T_67; // @[Parameters.scala 137:67]
  wire  _T_68; // @[Parameters.scala 552:42]
  wire  _T_73; // @[Monitor.scala 44:11]
  wire  _T_85; // @[Monitor.scala 44:11]
  wire  _T_86; // @[Monitor.scala 44:11]
  wire [3:0] _T_91; // @[Monitor.scala 86:18]
  wire  _T_92; // @[Monitor.scala 86:31]
  wire  _T_94; // @[Monitor.scala 44:11]
  wire  _T_95; // @[Monitor.scala 44:11]
  wire  _T_100; // @[Monitor.scala 90:25]
  wire  _T_148; // @[Monitor.scala 102:25]
  wire  _T_167; // @[Monitor.scala 44:11]
  wire  _T_168; // @[Monitor.scala 44:11]
  wire  _T_179; // @[Monitor.scala 107:30]
  wire  _T_181; // @[Monitor.scala 44:11]
  wire  _T_182; // @[Monitor.scala 44:11]
  wire  _T_187; // @[Monitor.scala 111:25]
  wire  _T_222; // @[Monitor.scala 119:25]
  wire  _T_259; // @[Monitor.scala 127:25]
  wire  _T_291; // @[Monitor.scala 135:25]
  wire  _T_323; // @[Monitor.scala 143:25]
  wire  _T_359; // @[Bundles.scala 44:24]
  wire  _T_361; // @[Monitor.scala 51:11]
  wire  _T_362; // @[Monitor.scala 51:11]
  wire  _T_366; // @[Monitor.scala 307:25]
  wire  _T_386; // @[Monitor.scala 315:25]
  wire  _T_414; // @[Monitor.scala 325:25]
  wire  _T_510; // @[Decoupled.scala 40:37]
  reg  _T_519; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire  _T_521; // @[Edges.scala 231:28]
  wire  _T_522; // @[Edges.scala 232:25]
  reg [2:0] _T_530; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [6:0] _T_534; // @[Monitor.scala 385:22]
  reg [31:0] _RAND_2;
  wire  _T_535; // @[Monitor.scala 386:22]
  wire  _T_536; // @[Monitor.scala 386:19]
  wire  _T_537; // @[Monitor.scala 387:32]
  wire  _T_539; // @[Monitor.scala 44:11]
  wire  _T_540; // @[Monitor.scala 44:11]
  wire  _T_553; // @[Monitor.scala 391:32]
  wire  _T_555; // @[Monitor.scala 44:11]
  wire  _T_556; // @[Monitor.scala 44:11]
  wire  _T_558; // @[Monitor.scala 393:20]
  wire  _T_559; // @[Decoupled.scala 40:37]
  reg  _T_567; // @[Edges.scala 230:27]
  reg [31:0] _RAND_3;
  wire  _T_569; // @[Edges.scala 231:28]
  wire  _T_570; // @[Edges.scala 232:25]
  reg [2:0] _T_578; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_4;
  wire  _T_584; // @[Monitor.scala 538:22]
  wire  _T_585; // @[Monitor.scala 538:19]
  wire  _T_586; // @[Monitor.scala 539:29]
  wire  _T_588; // @[Monitor.scala 51:11]
  wire  _T_589; // @[Monitor.scala 51:11]
  wire  _T_611; // @[Monitor.scala 546:20]
  reg  _T_612; // @[Monitor.scala 568:27]
  reg [31:0] _RAND_5;
  reg  _T_622; // @[Edges.scala 230:27]
  reg [31:0] _RAND_6;
  wire  _T_624; // @[Edges.scala 231:28]
  wire  _T_625; // @[Edges.scala 232:25]
  reg  _T_641; // @[Edges.scala 230:27]
  reg [31:0] _RAND_7;
  wire  _T_643; // @[Edges.scala 231:28]
  wire  _T_644; // @[Edges.scala 232:25]
  wire  _T_654; // @[Monitor.scala 574:27]
  wire  _T_659; // @[Monitor.scala 576:14]
  wire  _T_661; // @[Monitor.scala 576:13]
  wire  _T_662; // @[Monitor.scala 576:13]
  wire [1:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_666; // @[Monitor.scala 581:27]
  wire  _T_668; // @[Monitor.scala 581:75]
  wire  _T_669; // @[Monitor.scala 581:72]
  wire  _T_671; // @[Monitor.scala 583:21]
  wire  _T_675; // @[Monitor.scala 51:11]
  wire  _T_676; // @[Monitor.scala 51:11]
  wire [1:0] _GEN_16; // @[Monitor.scala 581:91]
  wire  _T_677; // @[Monitor.scala 590:27]
  wire  _T_678; // @[Monitor.scala 590:38]
  reg [31:0] _T_680; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_8;
  wire  _T_683; // @[Monitor.scala 595:36]
  wire  _T_684; // @[Monitor.scala 595:27]
  wire  _T_685; // @[Monitor.scala 595:56]
  wire  _T_686; // @[Monitor.scala 595:44]
  wire  _T_688; // @[Monitor.scala 595:12]
  wire  _T_689; // @[Monitor.scala 595:12]
  wire [31:0] _T_691; // @[Monitor.scala 597:26]
  wire  _T_694; // @[Monitor.scala 598:27]
  wire  _GEN_18; // @[Monitor.scala 44:11]
  wire  _GEN_26; // @[Monitor.scala 44:11]
  wire  _GEN_36; // @[Monitor.scala 44:11]
  wire  _GEN_42; // @[Monitor.scala 44:11]
  wire  _GEN_48; // @[Monitor.scala 44:11]
  wire  _GEN_52; // @[Monitor.scala 44:11]
  wire  _GEN_58; // @[Monitor.scala 44:11]
  wire  _GEN_64; // @[Monitor.scala 44:11]
  wire  _GEN_70; // @[Monitor.scala 51:11]
  wire  _GEN_72; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_10 = io_in_a_bits_address & 7'h3; // @[Edges.scala 22:16]
  assign _T_11 = _T_10 == 7'h0; // @[Edges.scala 22:24]
  assign _T_56 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_58 = io_in_a_bits_address ^ 7'h40; // @[Parameters.scala 137:31]
  assign _T_59 = {1'b0,$signed(_T_58)}; // @[Parameters.scala 137:49]
  assign _T_61 = $signed(_T_59) & -8'sh14; // @[Parameters.scala 137:52]
  assign _T_62 = $signed(_T_61) == 8'sh0; // @[Parameters.scala 137:67]
  assign _T_63 = io_in_a_bits_address ^ 7'h54; // @[Parameters.scala 137:31]
  assign _T_64 = {1'b0,$signed(_T_63)}; // @[Parameters.scala 137:49]
  assign _T_66 = $signed(_T_64) & -8'sh4; // @[Parameters.scala 137:52]
  assign _T_67 = $signed(_T_66) == 8'sh0; // @[Parameters.scala 137:67]
  assign _T_68 = _T_62 | _T_67; // @[Parameters.scala 552:42]
  assign _T_73 = ~reset; // @[Monitor.scala 44:11]
  assign _T_85 = _T_11 | reset; // @[Monitor.scala 44:11]
  assign _T_86 = ~_T_85; // @[Monitor.scala 44:11]
  assign _T_91 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_92 = _T_91 == 4'h0; // @[Monitor.scala 86:31]
  assign _T_94 = _T_92 | reset; // @[Monitor.scala 44:11]
  assign _T_95 = ~_T_94; // @[Monitor.scala 44:11]
  assign _T_100 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_148 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_167 = _T_68 | reset; // @[Monitor.scala 44:11]
  assign _T_168 = ~_T_167; // @[Monitor.scala 44:11]
  assign _T_179 = io_in_a_bits_mask == 4'hf; // @[Monitor.scala 107:30]
  assign _T_181 = _T_179 | reset; // @[Monitor.scala 44:11]
  assign _T_182 = ~_T_181; // @[Monitor.scala 44:11]
  assign _T_187 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_222 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_259 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_291 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_323 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_359 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_361 = _T_359 | reset; // @[Monitor.scala 51:11]
  assign _T_362 = ~_T_361; // @[Monitor.scala 51:11]
  assign _T_366 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_386 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_414 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_510 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_521 = _T_519 - 1'h1; // @[Edges.scala 231:28]
  assign _T_522 = ~_T_519; // @[Edges.scala 232:25]
  assign _T_535 = ~_T_522; // @[Monitor.scala 386:22]
  assign _T_536 = io_in_a_valid & _T_535; // @[Monitor.scala 386:19]
  assign _T_537 = io_in_a_bits_opcode == _T_530; // @[Monitor.scala 387:32]
  assign _T_539 = _T_537 | reset; // @[Monitor.scala 44:11]
  assign _T_540 = ~_T_539; // @[Monitor.scala 44:11]
  assign _T_553 = io_in_a_bits_address == _T_534; // @[Monitor.scala 391:32]
  assign _T_555 = _T_553 | reset; // @[Monitor.scala 44:11]
  assign _T_556 = ~_T_555; // @[Monitor.scala 44:11]
  assign _T_558 = _T_510 & _T_522; // @[Monitor.scala 393:20]
  assign _T_559 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_569 = _T_567 - 1'h1; // @[Edges.scala 231:28]
  assign _T_570 = ~_T_567; // @[Edges.scala 232:25]
  assign _T_584 = ~_T_570; // @[Monitor.scala 538:22]
  assign _T_585 = io_in_d_valid & _T_584; // @[Monitor.scala 538:19]
  assign _T_586 = io_in_d_bits_opcode == _T_578; // @[Monitor.scala 539:29]
  assign _T_588 = _T_586 | reset; // @[Monitor.scala 51:11]
  assign _T_589 = ~_T_588; // @[Monitor.scala 51:11]
  assign _T_611 = _T_559 & _T_570; // @[Monitor.scala 546:20]
  assign _T_624 = _T_622 - 1'h1; // @[Edges.scala 231:28]
  assign _T_625 = ~_T_622; // @[Edges.scala 232:25]
  assign _T_643 = _T_641 - 1'h1; // @[Edges.scala 231:28]
  assign _T_644 = ~_T_641; // @[Edges.scala 232:25]
  assign _T_654 = _T_510 & _T_625; // @[Monitor.scala 574:27]
  assign _T_659 = ~_T_612; // @[Monitor.scala 576:14]
  assign _T_661 = _T_659 | reset; // @[Monitor.scala 576:13]
  assign _T_662 = ~_T_661; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_654 ? 2'h1 : 2'h0; // @[Monitor.scala 574:72]
  assign _T_666 = _T_559 & _T_644; // @[Monitor.scala 581:27]
  assign _T_668 = ~_T_366; // @[Monitor.scala 581:75]
  assign _T_669 = _T_666 & _T_668; // @[Monitor.scala 581:72]
  assign _T_671 = _GEN_15[0] | _T_612; // @[Monitor.scala 583:21]
  assign _T_675 = _T_671 | reset; // @[Monitor.scala 51:11]
  assign _T_676 = ~_T_675; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_669 ? 2'h1 : 2'h0; // @[Monitor.scala 581:91]
  assign _T_677 = _T_612 | _GEN_15[0]; // @[Monitor.scala 590:27]
  assign _T_678 = ~_GEN_16[0]; // @[Monitor.scala 590:38]
  assign _T_683 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_684 = _T_659 | _T_683; // @[Monitor.scala 595:27]
  assign _T_685 = _T_680 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_686 = _T_684 | _T_685; // @[Monitor.scala 595:44]
  assign _T_688 = _T_686 | reset; // @[Monitor.scala 595:12]
  assign _T_689 = ~_T_688; // @[Monitor.scala 595:12]
  assign _T_691 = _T_680 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_694 = _T_510 | _T_559; // @[Monitor.scala 598:27]
  assign _GEN_18 = io_in_a_valid & _T_56; // @[Monitor.scala 44:11]
  assign _GEN_26 = io_in_a_valid & _T_100; // @[Monitor.scala 44:11]
  assign _GEN_36 = io_in_a_valid & _T_148; // @[Monitor.scala 44:11]
  assign _GEN_42 = io_in_a_valid & _T_187; // @[Monitor.scala 44:11]
  assign _GEN_48 = io_in_a_valid & _T_222; // @[Monitor.scala 44:11]
  assign _GEN_52 = io_in_a_valid & _T_259; // @[Monitor.scala 44:11]
  assign _GEN_58 = io_in_a_valid & _T_291; // @[Monitor.scala 44:11]
  assign _GEN_64 = io_in_a_valid & _T_323; // @[Monitor.scala 44:11]
  assign _GEN_70 = io_in_d_valid & _T_386; // @[Monitor.scala 51:11]
  assign _GEN_72 = io_in_d_valid & _T_414; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_519 = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_530 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_534 = _RAND_2[6:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_567 = _RAND_3[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_578 = _RAND_4[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  _T_612 = _RAND_5[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_622 = _RAND_6[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_641 = _RAND_7[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_680 = _RAND_8[31:0];
  `endif // RANDOMIZE_REG_INIT
  if (reset) begin
    _T_519 = 1'h0;
  end
  if (reset) begin
    _T_567 = 1'h0;
  end
  if (reset) begin
    _T_612 = 1'h0;
  end
  if (reset) begin
    _T_622 = 1'h0;
  end
  if (reset) begin
    _T_641 = 1'h0;
  end
  if (reset) begin
    _T_680 = 32'h0;
  end
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (_T_558) begin
      _T_530 <= io_in_a_bits_opcode;
    end
    if (_T_558) begin
      _T_534 <= io_in_a_bits_address;
    end
    if (_T_611) begin
      _T_578 <= io_in_d_bits_opcode;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_73) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_73) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_86) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_86) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_95) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_95) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_73) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_73) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_86) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_86) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_73) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_95) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_95) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_36 & _T_168) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_36 & _T_168) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_36 & _T_86) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_36 & _T_86) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_36 & _T_182) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_36 & _T_182) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_42 & _T_168) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_42 & _T_168) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_42 & _T_86) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_42 & _T_86) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_42 & _T_182) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_42 & _T_182) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_48 & _T_168) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_48 & _T_168) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_48 & _T_86) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_48 & _T_86) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_52 & _T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_52 & _T_73) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_52 & _T_86) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_52 & _T_86) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_52 & _T_182) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_52 & _T_182) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_58 & _T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_58 & _T_73) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_58 & _T_86) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_58 & _T_86) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_58 & _T_182) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_58 & _T_182) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_73) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_86) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_86) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_182) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_182) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_362) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at Debug.scala:632:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_362) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_70 & _T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid sink ID (connected at Debug.scala:632:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_70 & _T_73) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_72 & _T_73) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid sink ID (connected at Debug.scala:632:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_72 & _T_73) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_536 & _T_540) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_536 & _T_540) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_536 & _T_556) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at Debug.scala:632:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_536 & _T_556) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_585 & _T_589) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at Debug.scala:632:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_585 & _T_589) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_654 & _T_662) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at Debug.scala:632:19)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_654 & _T_662) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_669 & _T_676) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at Debug.scala:632:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_669 & _T_676) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_689) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at Debug.scala:632:19)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_689) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_519 <= 1'h0;
    end else if (_T_510) begin
      if (_T_522) begin
        _T_519 <= 1'h0;
      end else begin
        _T_519 <= _T_521;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_567 <= 1'h0;
    end else if (_T_559) begin
      if (_T_570) begin
        _T_567 <= 1'h0;
      end else begin
        _T_567 <= _T_569;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_612 <= 1'h0;
    end else begin
      _T_612 <= _T_677 & _T_678;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_622 <= 1'h0;
    end else if (_T_510) begin
      if (_T_625) begin
        _T_622 <= 1'h0;
      end else begin
        _T_622 <= _T_624;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_641 <= 1'h0;
    end else if (_T_559) begin
      if (_T_644) begin
        _T_641 <= 1'h0;
      end else begin
        _T_641 <= _T_643;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      _T_680 <= 32'h0;
    end else if (_T_694) begin
      _T_680 <= 32'h0;
    end else begin
      _T_680 <= _T_691;
    end
  end

endmodule
