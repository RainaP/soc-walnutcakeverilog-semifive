//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_CaptureUpdateChain_1_assert(
  input   clock,
  input   reset,
  input   io_chainIn_shift,
  input   io_chainIn_capture,
  input   io_chainIn_update
);
  wire  _T_91; // @[JtagShifter.scala 184:31]
  wire  _T_92; // @[JtagShifter.scala 184:10]
  wire  _T_93; // @[JtagShifter.scala 185:31]
  wire  _T_94; // @[JtagShifter.scala 185:10]
  wire  _T_95; // @[JtagShifter.scala 185:7]
  wire  _T_96; // @[JtagShifter.scala 186:30]
  wire  _T_97; // @[JtagShifter.scala 186:10]
  wire  _T_98; // @[JtagShifter.scala 186:7]
  wire  _T_100; // @[JtagShifter.scala 184:9]
  wire  _T_101; // @[JtagShifter.scala 184:9]
  assign _T_91 = io_chainIn_capture & io_chainIn_update; // @[JtagShifter.scala 184:31]
  assign _T_92 = ~_T_91; // @[JtagShifter.scala 184:10]
  assign _T_93 = io_chainIn_capture & io_chainIn_shift; // @[JtagShifter.scala 185:31]
  assign _T_94 = ~_T_93; // @[JtagShifter.scala 185:10]
  assign _T_95 = _T_92 & _T_94; // @[JtagShifter.scala 185:7]
  assign _T_96 = io_chainIn_update & io_chainIn_shift; // @[JtagShifter.scala 186:30]
  assign _T_97 = ~_T_96; // @[JtagShifter.scala 186:10]
  assign _T_98 = _T_95 & _T_97; // @[JtagShifter.scala 186:7]
  assign _T_100 = _T_98 | reset; // @[JtagShifter.scala 184:9]
  assign _T_101 = ~_T_100; // @[JtagShifter.scala 184:9]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_101) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at JtagShifter.scala:184 assert(!(io.chainIn.capture && io.chainIn.update)\n"); // @[JtagShifter.scala 184:9]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_101) begin
          $fatal; // @[JtagShifter.scala 184:9]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
