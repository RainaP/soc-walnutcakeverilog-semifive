//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_ICache_assert(
  input   clock,
  input   reset,
  input   s1_valid,
  input   s1_tag_hit_0,
  input   s1_tag_hit_1,
  input   s1_tag_hit_2,
  input   s1_tag_hit_3
);
  wire  _T_215; // @[ICache.scala 260:10]
  wire [1:0] _T_224; // @[Bitwise.scala 47:55]
  wire [1:0] _T_226; // @[Bitwise.scala 47:55]
  wire [2:0] _T_228; // @[Bitwise.scala 47:55]
  wire  _T_230; // @[ICache.scala 260:115]
  wire  _T_231; // @[ICache.scala 260:39]
  wire  _T_233; // @[ICache.scala 260:9]
  wire  _T_234; // @[ICache.scala 260:9]
  assign _T_215 = ~s1_valid; // @[ICache.scala 260:10]
  assign _T_224 = s1_tag_hit_0 + s1_tag_hit_1; // @[Bitwise.scala 47:55]
  assign _T_226 = s1_tag_hit_2 + s1_tag_hit_3; // @[Bitwise.scala 47:55]
  assign _T_228 = _T_224 + _T_226; // @[Bitwise.scala 47:55]
  assign _T_230 = _T_228 <= 3'h1; // @[ICache.scala 260:115]
  assign _T_231 = _T_215 | _T_230; // @[ICache.scala 260:39]
  assign _T_233 = _T_231 | reset; // @[ICache.scala 260:9]
  assign _T_234 = ~_T_233; // @[ICache.scala 260:9]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_234) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ICache.scala:260 assert(!(s1_valid || s1_slaveValid) || PopCount(s1_tag_hit zip s1_tag_disparity map { case (h, d) => h && !d }) <= 1)\n"); // @[ICache.scala 260:9]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_234) begin
          $fatal; // @[ICache.scala 260:9]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
