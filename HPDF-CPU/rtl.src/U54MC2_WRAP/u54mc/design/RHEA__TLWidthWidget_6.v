//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLWidthWidget_6(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_a_ready,
  input         auto_in_a_valid,
  input  [1:0]  auto_in_a_bits_source,
  input  [36:0] auto_in_a_bits_address,
  input  [31:0] auto_in_a_bits_data,
  output        auto_in_d_valid,
  output [1:0]  auto_in_d_bits_source,
  output        auto_in_d_bits_denied,
  input         auto_out_a_ready,
  output        auto_out_a_valid,
  output [1:0]  auto_out_a_bits_source,
  output [36:0] auto_out_a_bits_address,
  output [7:0]  auto_out_a_bits_mask,
  output [63:0] auto_out_a_bits_data,
  output        auto_out_d_ready,
  input         auto_out_d_valid,
  input  [2:0]  auto_out_d_bits_opcode,
  input  [1:0]  auto_out_d_bits_param,
  input  [3:0]  auto_out_d_bits_size,
  input  [1:0]  auto_out_d_bits_source,
  input  [4:0]  auto_out_d_bits_sink,
  input         auto_out_d_bits_denied,
  input         auto_out_d_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
`endif // RANDOMIZE_REG_INIT
  wire  repeated_repeater_rf_reset; // @[Repeater.scala 35:26]
  wire  repeated_repeater_clock; // @[Repeater.scala 35:26]
  wire  repeated_repeater_reset; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_repeat; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_enq_ready; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_enq_valid; // @[Repeater.scala 35:26]
  wire [2:0] repeated_repeater_io_enq_bits_opcode; // @[Repeater.scala 35:26]
  wire [1:0] repeated_repeater_io_enq_bits_param; // @[Repeater.scala 35:26]
  wire [3:0] repeated_repeater_io_enq_bits_size; // @[Repeater.scala 35:26]
  wire [1:0] repeated_repeater_io_enq_bits_source; // @[Repeater.scala 35:26]
  wire [4:0] repeated_repeater_io_enq_bits_sink; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_enq_bits_denied; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_enq_bits_corrupt; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_deq_valid; // @[Repeater.scala 35:26]
  wire [2:0] repeated_repeater_io_deq_bits_opcode; // @[Repeater.scala 35:26]
  wire [1:0] repeated_repeater_io_deq_bits_param; // @[Repeater.scala 35:26]
  wire [3:0] repeated_repeater_io_deq_bits_size; // @[Repeater.scala 35:26]
  wire [1:0] repeated_repeater_io_deq_bits_source; // @[Repeater.scala 35:26]
  wire [4:0] repeated_repeater_io_deq_bits_sink; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_deq_bits_denied; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_deq_bits_corrupt; // @[Repeater.scala 35:26]
  reg  count; // @[WidthWidget.scala 34:27]
  wire  last = ~count; // @[WidthWidget.scala 36:26]
  wire  _bundleIn_0_a_ready_T = ~last; // @[WidthWidget.scala 70:32]
  wire  bundleIn_0_a_ready = auto_out_a_ready | _bundleIn_0_a_ready_T; // @[WidthWidget.scala 70:29]
  wire  _T = bundleIn_0_a_ready & auto_in_a_valid; // @[Decoupled.scala 40:37]
  wire  bundleOut_0_a_bits_mask_bit = auto_in_a_bits_address[2]; // @[Misc.scala 215:26]
  wire  bundleOut_0_a_bits_mask_nbit = ~bundleOut_0_a_bits_mask_bit; // @[Misc.scala 216:20]
  wire  bundleOut_0_a_bits_mask_bit_1 = auto_in_a_bits_address[1]; // @[Misc.scala 215:26]
  wire  bundleOut_0_a_bits_mask_nbit_1 = ~bundleOut_0_a_bits_mask_bit_1; // @[Misc.scala 216:20]
  wire  bundleOut_0_a_bits_mask_eq_2 = bundleOut_0_a_bits_mask_nbit & bundleOut_0_a_bits_mask_nbit_1; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_eq_3 = bundleOut_0_a_bits_mask_nbit & bundleOut_0_a_bits_mask_bit_1; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_eq_4 = bundleOut_0_a_bits_mask_bit & bundleOut_0_a_bits_mask_nbit_1; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_eq_5 = bundleOut_0_a_bits_mask_bit & bundleOut_0_a_bits_mask_bit_1; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_bit_2 = auto_in_a_bits_address[0]; // @[Misc.scala 215:26]
  wire  bundleOut_0_a_bits_mask_nbit_2 = ~bundleOut_0_a_bits_mask_bit_2; // @[Misc.scala 216:20]
  wire  bundleOut_0_a_bits_mask_eq_6 = bundleOut_0_a_bits_mask_eq_2 & bundleOut_0_a_bits_mask_nbit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_lo_lo = bundleOut_0_a_bits_mask_nbit | bundleOut_0_a_bits_mask_eq_6; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_7 = bundleOut_0_a_bits_mask_eq_2 & bundleOut_0_a_bits_mask_bit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_lo_hi = bundleOut_0_a_bits_mask_nbit | bundleOut_0_a_bits_mask_eq_7; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_8 = bundleOut_0_a_bits_mask_eq_3 & bundleOut_0_a_bits_mask_nbit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_hi_lo = bundleOut_0_a_bits_mask_nbit | bundleOut_0_a_bits_mask_eq_8; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_9 = bundleOut_0_a_bits_mask_eq_3 & bundleOut_0_a_bits_mask_bit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_hi_hi = bundleOut_0_a_bits_mask_nbit | bundleOut_0_a_bits_mask_eq_9; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_10 = bundleOut_0_a_bits_mask_eq_4 & bundleOut_0_a_bits_mask_nbit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_lo_lo = bundleOut_0_a_bits_mask_bit | bundleOut_0_a_bits_mask_eq_10; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_11 = bundleOut_0_a_bits_mask_eq_4 & bundleOut_0_a_bits_mask_bit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_lo_hi = bundleOut_0_a_bits_mask_bit | bundleOut_0_a_bits_mask_eq_11; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_12 = bundleOut_0_a_bits_mask_eq_5 & bundleOut_0_a_bits_mask_nbit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_hi_lo = bundleOut_0_a_bits_mask_bit | bundleOut_0_a_bits_mask_eq_12; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_13 = bundleOut_0_a_bits_mask_eq_5 & bundleOut_0_a_bits_mask_bit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_hi_hi = bundleOut_0_a_bits_mask_bit | bundleOut_0_a_bits_mask_eq_13; // @[Misc.scala 220:29]
  wire [3:0] bundleOut_0_a_bits_mask_lo = {bundleOut_0_a_bits_mask_lo_hi_hi,bundleOut_0_a_bits_mask_lo_hi_lo,
    bundleOut_0_a_bits_mask_lo_lo_hi,bundleOut_0_a_bits_mask_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [3:0] bundleOut_0_a_bits_mask_hi = {bundleOut_0_a_bits_mask_hi_hi_hi,bundleOut_0_a_bits_mask_hi_hi_lo,
    bundleOut_0_a_bits_mask_hi_lo_hi,bundleOut_0_a_bits_mask_hi_lo_lo}; // @[Cat.scala 30:58]
  wire [2:0] cated_bits_opcode = repeated_repeater_io_deq_bits_opcode; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  wire  repeat_hasData = cated_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [3:0] cated_bits_size = repeated_repeater_io_deq_bits_size; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  wire [17:0] _repeat_limit_T_1 = 18'h7 << cated_bits_size; // @[package.scala 234:77]
  wire [2:0] _repeat_limit_T_3 = ~_repeat_limit_T_1[2:0]; // @[package.scala 234:46]
  wire  repeat_limit = _repeat_limit_T_3[2]; // @[WidthWidget.scala 97:47]
  reg  repeat_count; // @[WidthWidget.scala 99:26]
  wire  repeat_last = repeat_count == repeat_limit | ~repeat_hasData; // @[WidthWidget.scala 101:35]
  wire  cated_valid = repeated_repeater_io_deq_valid; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  RHEA__Repeater_4 repeated_repeater ( // @[Repeater.scala 35:26]
    .rf_reset(repeated_repeater_rf_reset),
    .clock(repeated_repeater_clock),
    .reset(repeated_repeater_reset),
    .io_repeat(repeated_repeater_io_repeat),
    .io_enq_ready(repeated_repeater_io_enq_ready),
    .io_enq_valid(repeated_repeater_io_enq_valid),
    .io_enq_bits_opcode(repeated_repeater_io_enq_bits_opcode),
    .io_enq_bits_param(repeated_repeater_io_enq_bits_param),
    .io_enq_bits_size(repeated_repeater_io_enq_bits_size),
    .io_enq_bits_source(repeated_repeater_io_enq_bits_source),
    .io_enq_bits_sink(repeated_repeater_io_enq_bits_sink),
    .io_enq_bits_denied(repeated_repeater_io_enq_bits_denied),
    .io_enq_bits_corrupt(repeated_repeater_io_enq_bits_corrupt),
    .io_deq_valid(repeated_repeater_io_deq_valid),
    .io_deq_bits_opcode(repeated_repeater_io_deq_bits_opcode),
    .io_deq_bits_param(repeated_repeater_io_deq_bits_param),
    .io_deq_bits_size(repeated_repeater_io_deq_bits_size),
    .io_deq_bits_source(repeated_repeater_io_deq_bits_source),
    .io_deq_bits_sink(repeated_repeater_io_deq_bits_sink),
    .io_deq_bits_denied(repeated_repeater_io_deq_bits_denied),
    .io_deq_bits_corrupt(repeated_repeater_io_deq_bits_corrupt)
  );
  assign repeated_repeater_rf_reset = rf_reset;
  assign auto_in_a_ready = auto_out_a_ready | _bundleIn_0_a_ready_T; // @[WidthWidget.scala 70:29]
  assign auto_in_d_valid = repeated_repeater_io_deq_valid; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  assign auto_in_d_bits_source = repeated_repeater_io_deq_bits_source; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  assign auto_in_d_bits_denied = repeated_repeater_io_deq_bits_denied; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  assign auto_out_a_valid = auto_in_a_valid & last; // @[WidthWidget.scala 71:29]
  assign auto_out_a_bits_source = auto_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_mask = {bundleOut_0_a_bits_mask_hi,bundleOut_0_a_bits_mask_lo}; // @[Cat.scala 30:58]
  assign auto_out_a_bits_data = {auto_in_a_bits_data,auto_in_a_bits_data}; // @[Cat.scala 30:58]
  assign auto_out_d_ready = repeated_repeater_io_enq_ready; // @[Nodes.scala 1529:13 Repeater.scala 37:21]
  assign repeated_repeater_clock = clock;
  assign repeated_repeater_reset = reset;
  assign repeated_repeater_io_repeat = ~repeat_last; // @[WidthWidget.scala 142:7]
  assign repeated_repeater_io_enq_valid = auto_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_param = auto_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_size = auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_source = auto_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_sink = auto_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_denied = auto_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_corrupt = auto_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count <= 1'h0;
    end else if (_T) begin
      if (last) begin
        count <= 1'h0;
      end else begin
        count <= count + 1'h1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      repeat_count <= 1'h0;
    end else if (cated_valid) begin
      if (repeat_last) begin
        repeat_count <= 1'h0;
      end else begin
        repeat_count <= repeat_count + 1'h1;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  count = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  repeat_count = _RAND_1[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    count = 1'h0;
  end
  if (reset) begin
    repeat_count = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
