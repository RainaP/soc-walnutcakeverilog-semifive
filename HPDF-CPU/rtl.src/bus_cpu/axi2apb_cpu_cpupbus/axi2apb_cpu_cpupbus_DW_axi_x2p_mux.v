/*
 ------------------------------------------------------------------------
--
// ------------------------------------------------------------------------------
// 
// Copyright 2001 - 2020 Synopsys, INC.
// 
// This Synopsys IP and all associated documentation are proprietary to
// Synopsys, Inc. and may only be used pursuant to the terms and conditions of a
// written license agreement with Synopsys, Inc. All other use, reproduction,
// modification, or distribution of the Synopsys IP or the associated
// documentation is strictly prohibited.
// 
// Component Name   : DW_axi_x2p
// Component Version: 2.04a
// Release Type     : GA
// ------------------------------------------------------------------------------

// 
// Release version :  2.04a
// File Version     :        $Revision: #8 $ 
// Revision: $Id: //dwh/DW_ocb/DW_axi_x2p/amba_dev/src/DW_axi_x2p_mux.v#8 $ 
*/
//-----------------------------------------------------------------------------
//
//
// Description : Output multiplexor.
//-----------------------------------------------------------------------------

`include "axi2apb_cpu_cpupbus_DW_axi_x2p_all_includes.vh"

module axi2apb_cpu_cpupbus_DW_axi_x2p_mux (/*AUTOARG*/
  // Outputs
  prdata, 
                       pready,
                       pslverr, 
                       prdata_s1_s,
                       prdata_s2_s,
                       prdata_s3_s, 
                       prdata_s4_s, 
                       prdata_s5_s, 
                       pready_s0_s, 
                       pready_s1_s, 
                       pready_s2_s, 
                       pready_s3_s, 
                       pready_s4_s,
                       pready_s5_s, 
                       pslverr_s0_s,
                       pslverr_s1_s,
                       pslverr_s2_s,
                       pslverr_s3_s, 
                       pslverr_s4_s,
                       pslverr_s5_s, 
                       // Inputs
                       psel, 
                       prdata_s0_s
                       );
  
  input [`axi2apb_cpu_cpupbus_X2P_NUM_APB_SLAVES-1:0]        psel;
  input [`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0]        prdata_s0_s;
  input [`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0]        prdata_s1_s;
  input [`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0]        prdata_s2_s;
  input [`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0]        prdata_s3_s;
  input [`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0]        prdata_s4_s;
  input [`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0]        prdata_s5_s;
  
  input                                  pready_s0_s;
  input                                  pready_s1_s;  
  input                                  pready_s2_s;
  input                                  pready_s3_s;  
  input                                  pready_s4_s;
  input                                  pready_s5_s;  

  input                                  pslverr_s0_s;
  input                                  pslverr_s1_s;  
  input                                  pslverr_s2_s;
  input                                  pslverr_s3_s;  
  input                                  pslverr_s4_s;
  input                                  pslverr_s5_s;  
  
  output [`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0]       prdata;
  output                                 pready;
  output                                 pslverr;

  reg [`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0]          prdata_s;
  reg                                    pready_s;
  reg                                    pslverr_s;

  assign                                 prdata = prdata_s;
  assign                                 pready = pready_s;
  assign                                 pslverr = pslverr_s;
  
    always@(*)
        begin: PRDATA_PROC
            case (psel)
              1:     prdata_s = prdata_s0_s;
              2:     prdata_s = prdata_s1_s;
              4:     prdata_s = prdata_s2_s;
              8:     prdata_s = prdata_s3_s;
              16:    prdata_s = prdata_s4_s;
              32:    prdata_s = prdata_s5_s;
              default:  prdata_s = {`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH{1'b0}};
            endcase // case(psel)
        end // always@ (prdata_s0 or prdata_s1 or prdata_s10...


   always @(*)
     begin:PREADY_S_PROC
     case(psel)
          1:     pready_s = pready_s0_s;
          2:     pready_s = pready_s1_s;
          4:     pready_s = pready_s2_s;
          8:     pready_s = pready_s3_s;
          16:    pready_s = pready_s4_s;
          32:    pready_s = pready_s5_s;
        default: pready_s = 1'b0;
     endcase // case(psel)
   end // always @ (...   

   always @(*)
     begin: PSLVERR_S_PROC
     case(psel)
          1:     pslverr_s = pslverr_s0_s;
          2:     pslverr_s = pslverr_s1_s;
          4:     pslverr_s = pslverr_s2_s;
          8:     pslverr_s = pslverr_s3_s;
          16:    pslverr_s = pslverr_s4_s;
          32:    pslverr_s = pslverr_s5_s;
       default: pslverr_s = 1'b0;
     endcase // case(psel)
   end // always @ (...  
  
//       
endmodule // DW_axi_x2p_mux
