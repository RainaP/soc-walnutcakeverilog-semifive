//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__Queue_24(
  input         rf_reset,
  input         clock,
  input         reset,
  output        io_enq_ready,
  input         io_enq_valid,
  input  [2:0]  io_enq_bits_opcode,
  input  [3:0]  io_enq_bits_size,
  input  [5:0]  io_enq_bits_source,
  input  [36:0] io_enq_bits_address,
  input         io_enq_bits_user_amba_prot_bufferable,
  input         io_enq_bits_user_amba_prot_modifiable,
  input         io_enq_bits_user_amba_prot_readalloc,
  input         io_enq_bits_user_amba_prot_writealloc,
  input         io_enq_bits_user_amba_prot_privileged,
  input         io_enq_bits_user_amba_prot_secure,
  input         io_enq_bits_user_amba_prot_fetch,
  input  [7:0]  io_enq_bits_mask,
  input  [63:0] io_enq_bits_data,
  input         io_deq_ready,
  output        io_deq_valid,
  output [2:0]  io_deq_bits_opcode,
  output [3:0]  io_deq_bits_size,
  output [5:0]  io_deq_bits_source,
  output [36:0] io_deq_bits_address,
  output        io_deq_bits_user_amba_prot_bufferable,
  output        io_deq_bits_user_amba_prot_modifiable,
  output        io_deq_bits_user_amba_prot_readalloc,
  output        io_deq_bits_user_amba_prot_writealloc,
  output        io_deq_bits_user_amba_prot_privileged,
  output        io_deq_bits_user_amba_prot_secure,
  output        io_deq_bits_user_amba_prot_fetch,
  output [7:0]  io_deq_bits_mask,
  output [63:0] io_deq_bits_data
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [63:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [63:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [63:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [63:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
`endif // RANDOMIZE_REG_INIT
  reg [2:0] ram_0_opcode; // @[Decoupled.scala 218:16]
  reg [3:0] ram_0_size; // @[Decoupled.scala 218:16]
  reg [5:0] ram_0_source; // @[Decoupled.scala 218:16]
  reg [36:0] ram_0_address; // @[Decoupled.scala 218:16]
  reg  ram_0_user_amba_prot_bufferable; // @[Decoupled.scala 218:16]
  reg  ram_0_user_amba_prot_modifiable; // @[Decoupled.scala 218:16]
  reg  ram_0_user_amba_prot_readalloc; // @[Decoupled.scala 218:16]
  reg  ram_0_user_amba_prot_writealloc; // @[Decoupled.scala 218:16]
  reg  ram_0_user_amba_prot_privileged; // @[Decoupled.scala 218:16]
  reg  ram_0_user_amba_prot_secure; // @[Decoupled.scala 218:16]
  reg  ram_0_user_amba_prot_fetch; // @[Decoupled.scala 218:16]
  reg [7:0] ram_0_mask; // @[Decoupled.scala 218:16]
  reg [63:0] ram_0_data; // @[Decoupled.scala 218:16]
  reg [2:0] ram_1_opcode; // @[Decoupled.scala 218:16]
  reg [3:0] ram_1_size; // @[Decoupled.scala 218:16]
  reg [5:0] ram_1_source; // @[Decoupled.scala 218:16]
  reg [36:0] ram_1_address; // @[Decoupled.scala 218:16]
  reg  ram_1_user_amba_prot_bufferable; // @[Decoupled.scala 218:16]
  reg  ram_1_user_amba_prot_modifiable; // @[Decoupled.scala 218:16]
  reg  ram_1_user_amba_prot_readalloc; // @[Decoupled.scala 218:16]
  reg  ram_1_user_amba_prot_writealloc; // @[Decoupled.scala 218:16]
  reg  ram_1_user_amba_prot_privileged; // @[Decoupled.scala 218:16]
  reg  ram_1_user_amba_prot_secure; // @[Decoupled.scala 218:16]
  reg  ram_1_user_amba_prot_fetch; // @[Decoupled.scala 218:16]
  reg [7:0] ram_1_mask; // @[Decoupled.scala 218:16]
  reg [63:0] ram_1_data; // @[Decoupled.scala 218:16]
  reg  value_1; // @[Counter.scala 60:40]
  wire  do_enq = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  reg  value; // @[Counter.scala 60:40]
  reg  maybe_full; // @[Decoupled.scala 221:27]
  wire  ptr_match = value == value_1; // @[Decoupled.scala 223:33]
  wire  empty = ptr_match & ~maybe_full; // @[Decoupled.scala 224:25]
  wire  full = ptr_match & maybe_full; // @[Decoupled.scala 225:24]
  wire  do_deq = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  assign io_enq_ready = ~full; // @[Decoupled.scala 241:19]
  assign io_deq_valid = ~empty; // @[Decoupled.scala 240:19]
  assign io_deq_bits_opcode = value_1 ? ram_1_opcode : ram_0_opcode; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_size = value_1 ? ram_1_size : ram_0_size; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_source = value_1 ? ram_1_source : ram_0_source; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_address = value_1 ? ram_1_address : ram_0_address; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_user_amba_prot_bufferable = value_1 ? ram_1_user_amba_prot_bufferable :
    ram_0_user_amba_prot_bufferable; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_user_amba_prot_modifiable = value_1 ? ram_1_user_amba_prot_modifiable :
    ram_0_user_amba_prot_modifiable; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_user_amba_prot_readalloc = value_1 ? ram_1_user_amba_prot_readalloc :
    ram_0_user_amba_prot_readalloc; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_user_amba_prot_writealloc = value_1 ? ram_1_user_amba_prot_writealloc :
    ram_0_user_amba_prot_writealloc; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_user_amba_prot_privileged = value_1 ? ram_1_user_amba_prot_privileged :
    ram_0_user_amba_prot_privileged; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_user_amba_prot_secure = value_1 ? ram_1_user_amba_prot_secure : ram_0_user_amba_prot_secure; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_user_amba_prot_fetch = value_1 ? ram_1_user_amba_prot_fetch : ram_0_user_amba_prot_fetch; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_mask = value_1 ? ram_1_mask : ram_0_mask; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_data = value_1 ? ram_1_data : ram_0_data; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_opcode <= 3'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_size <= 4'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_size <= io_enq_bits_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_source <= 6'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_address <= 37'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_user_amba_prot_bufferable <= 1'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_user_amba_prot_bufferable <= io_enq_bits_user_amba_prot_bufferable;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_user_amba_prot_modifiable <= 1'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_user_amba_prot_modifiable <= io_enq_bits_user_amba_prot_modifiable;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_user_amba_prot_readalloc <= 1'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_user_amba_prot_readalloc <= io_enq_bits_user_amba_prot_readalloc;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_user_amba_prot_writealloc <= 1'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_user_amba_prot_writealloc <= io_enq_bits_user_amba_prot_writealloc;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_user_amba_prot_privileged <= 1'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_user_amba_prot_privileged <= io_enq_bits_user_amba_prot_privileged;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_user_amba_prot_secure <= 1'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_user_amba_prot_secure <= io_enq_bits_user_amba_prot_secure;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_user_amba_prot_fetch <= 1'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_user_amba_prot_fetch <= io_enq_bits_user_amba_prot_fetch;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_mask <= 8'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_mask <= io_enq_bits_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_data <= 64'h0;
    end else if (do_enq) begin
      if (~value) begin
        ram_0_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_opcode <= 3'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_size <= 4'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_size <= io_enq_bits_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_source <= 6'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_address <= 37'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_user_amba_prot_bufferable <= 1'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_user_amba_prot_bufferable <= io_enq_bits_user_amba_prot_bufferable;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_user_amba_prot_modifiable <= 1'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_user_amba_prot_modifiable <= io_enq_bits_user_amba_prot_modifiable;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_user_amba_prot_readalloc <= 1'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_user_amba_prot_readalloc <= io_enq_bits_user_amba_prot_readalloc;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_user_amba_prot_writealloc <= 1'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_user_amba_prot_writealloc <= io_enq_bits_user_amba_prot_writealloc;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_user_amba_prot_privileged <= 1'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_user_amba_prot_privileged <= io_enq_bits_user_amba_prot_privileged;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_user_amba_prot_secure <= 1'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_user_amba_prot_secure <= io_enq_bits_user_amba_prot_secure;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_user_amba_prot_fetch <= 1'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_user_amba_prot_fetch <= io_enq_bits_user_amba_prot_fetch;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_mask <= 8'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_mask <= io_enq_bits_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_data <= 64'h0;
    end else if (do_enq) begin
      if (value) begin
        ram_1_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      value_1 <= 1'h0;
    end else if (do_deq) begin
      value_1 <= value_1 + 1'h1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      value <= 1'h0;
    end else if (do_enq) begin
      value <= value + 1'h1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      maybe_full <= 1'h0;
    end else if (do_enq != do_deq) begin
      maybe_full <= do_enq;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ram_0_opcode = _RAND_0[2:0];
  _RAND_1 = {1{`RANDOM}};
  ram_0_size = _RAND_1[3:0];
  _RAND_2 = {1{`RANDOM}};
  ram_0_source = _RAND_2[5:0];
  _RAND_3 = {2{`RANDOM}};
  ram_0_address = _RAND_3[36:0];
  _RAND_4 = {1{`RANDOM}};
  ram_0_user_amba_prot_bufferable = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  ram_0_user_amba_prot_modifiable = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  ram_0_user_amba_prot_readalloc = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  ram_0_user_amba_prot_writealloc = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  ram_0_user_amba_prot_privileged = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  ram_0_user_amba_prot_secure = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  ram_0_user_amba_prot_fetch = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  ram_0_mask = _RAND_11[7:0];
  _RAND_12 = {2{`RANDOM}};
  ram_0_data = _RAND_12[63:0];
  _RAND_13 = {1{`RANDOM}};
  ram_1_opcode = _RAND_13[2:0];
  _RAND_14 = {1{`RANDOM}};
  ram_1_size = _RAND_14[3:0];
  _RAND_15 = {1{`RANDOM}};
  ram_1_source = _RAND_15[5:0];
  _RAND_16 = {2{`RANDOM}};
  ram_1_address = _RAND_16[36:0];
  _RAND_17 = {1{`RANDOM}};
  ram_1_user_amba_prot_bufferable = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  ram_1_user_amba_prot_modifiable = _RAND_18[0:0];
  _RAND_19 = {1{`RANDOM}};
  ram_1_user_amba_prot_readalloc = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  ram_1_user_amba_prot_writealloc = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  ram_1_user_amba_prot_privileged = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  ram_1_user_amba_prot_secure = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  ram_1_user_amba_prot_fetch = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  ram_1_mask = _RAND_24[7:0];
  _RAND_25 = {2{`RANDOM}};
  ram_1_data = _RAND_25[63:0];
  _RAND_26 = {1{`RANDOM}};
  value_1 = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  value = _RAND_27[0:0];
  _RAND_28 = {1{`RANDOM}};
  maybe_full = _RAND_28[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ram_0_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_0_size = 4'h0;
  end
  if (rf_reset) begin
    ram_0_source = 6'h0;
  end
  if (rf_reset) begin
    ram_0_address = 37'h0;
  end
  if (rf_reset) begin
    ram_0_user_amba_prot_bufferable = 1'h0;
  end
  if (rf_reset) begin
    ram_0_user_amba_prot_modifiable = 1'h0;
  end
  if (rf_reset) begin
    ram_0_user_amba_prot_readalloc = 1'h0;
  end
  if (rf_reset) begin
    ram_0_user_amba_prot_writealloc = 1'h0;
  end
  if (rf_reset) begin
    ram_0_user_amba_prot_privileged = 1'h0;
  end
  if (rf_reset) begin
    ram_0_user_amba_prot_secure = 1'h0;
  end
  if (rf_reset) begin
    ram_0_user_amba_prot_fetch = 1'h0;
  end
  if (rf_reset) begin
    ram_0_mask = 8'h0;
  end
  if (rf_reset) begin
    ram_0_data = 64'h0;
  end
  if (rf_reset) begin
    ram_1_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_1_size = 4'h0;
  end
  if (rf_reset) begin
    ram_1_source = 6'h0;
  end
  if (rf_reset) begin
    ram_1_address = 37'h0;
  end
  if (rf_reset) begin
    ram_1_user_amba_prot_bufferable = 1'h0;
  end
  if (rf_reset) begin
    ram_1_user_amba_prot_modifiable = 1'h0;
  end
  if (rf_reset) begin
    ram_1_user_amba_prot_readalloc = 1'h0;
  end
  if (rf_reset) begin
    ram_1_user_amba_prot_writealloc = 1'h0;
  end
  if (rf_reset) begin
    ram_1_user_amba_prot_privileged = 1'h0;
  end
  if (rf_reset) begin
    ram_1_user_amba_prot_secure = 1'h0;
  end
  if (rf_reset) begin
    ram_1_user_amba_prot_fetch = 1'h0;
  end
  if (rf_reset) begin
    ram_1_mask = 8'h0;
  end
  if (rf_reset) begin
    ram_1_data = 64'h0;
  end
  if (reset) begin
    value_1 = 1'h0;
  end
  if (reset) begin
    value = 1'h0;
  end
  if (reset) begin
    maybe_full = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
