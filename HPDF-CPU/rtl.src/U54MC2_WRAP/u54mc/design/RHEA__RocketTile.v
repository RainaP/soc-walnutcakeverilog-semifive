//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__RocketTile(
  input          rf_reset,
  input          clock,
  input          reset,
  output         auto_broadcast_out_0_valid,
  output [39:0]  auto_broadcast_out_0_iaddr,
  output [31:0]  auto_broadcast_out_0_insn,
  output [2:0]   auto_broadcast_out_0_priv,
  output         auto_broadcast_out_0_exception,
  output         auto_broadcast_out_0_interrupt,
  output         auto_debug_out_0,
  output         auto_wfi_out_0,
  output         auto_cease_out_0,
  input          auto_int_sink_in_3_0,
  input          auto_int_sink_in_3_1,
  input          auto_int_sink_in_3_2,
  input          auto_int_sink_in_3_3,
  input          auto_int_sink_in_3_4,
  input          auto_int_sink_in_3_5,
  input          auto_int_sink_in_3_6,
  input          auto_int_sink_in_3_7,
  input          auto_int_sink_in_3_8,
  input          auto_int_sink_in_3_9,
  input          auto_int_sink_in_3_10,
  input          auto_int_sink_in_3_11,
  input          auto_int_sink_in_3_12,
  input          auto_int_sink_in_3_13,
  input          auto_int_sink_in_3_14,
  input          auto_int_sink_in_3_15,
  input          auto_int_sink_in_2_0,
  input          auto_int_sink_in_2_1,
  input          auto_int_sink_in_1_0,
  input          auto_int_sink_in_1_1,
  input          auto_int_sink_in_0_0,
  output         auto_bpwatch_out_0_valid_0,
  output [2:0]   auto_bpwatch_out_0_action,
  output         auto_bpwatch_out_1_valid_0,
  output [2:0]   auto_bpwatch_out_1_action,
  output         auto_bpwatch_out_2_valid_0,
  output [2:0]   auto_bpwatch_out_2_action,
  output         auto_bpwatch_out_3_valid_0,
  output [2:0]   auto_bpwatch_out_3_action,
  output         auto_bpwatch_out_4_valid_0,
  output [2:0]   auto_bpwatch_out_4_action,
  output         auto_bpwatch_out_5_valid_0,
  output [2:0]   auto_bpwatch_out_5_action,
  output         auto_bpwatch_out_6_valid_0,
  output [2:0]   auto_bpwatch_out_6_action,
  output         auto_bpwatch_out_7_valid_0,
  output [2:0]   auto_bpwatch_out_7_action,
  output         auto_bpwatch_out_8_valid_0,
  output [2:0]   auto_bpwatch_out_8_action,
  output         auto_bpwatch_out_9_valid_0,
  output [2:0]   auto_bpwatch_out_9_action,
  output         auto_bpwatch_out_10_valid_0,
  output [2:0]   auto_bpwatch_out_10_action,
  output         auto_bpwatch_out_11_valid_0,
  output [2:0]   auto_bpwatch_out_11_action,
  output         auto_bpwatch_out_12_valid_0,
  output [2:0]   auto_bpwatch_out_12_action,
  output         auto_bpwatch_out_13_valid_0,
  output [2:0]   auto_bpwatch_out_13_action,
  output         auto_bpwatch_out_14_valid_0,
  output [2:0]   auto_bpwatch_out_14_action,
  output         auto_bpwatch_out_15_valid_0,
  output [2:0]   auto_bpwatch_out_15_action,
  input          auto_trace_aux_in_stall,
  input          auto_nmi_in_rnmi,
  input  [36:0]  auto_nmi_in_rnmi_interrupt_vector,
  input  [36:0]  auto_nmi_in_rnmi_exception_vector,
  input  [36:0]  auto_reset_vector_in,
  input  [1:0]   auto_hartid_in,
  input          auto_tl_other_masters_out_a_ready,
  output         auto_tl_other_masters_out_a_valid,
  output [2:0]   auto_tl_other_masters_out_a_bits_opcode,
  output [2:0]   auto_tl_other_masters_out_a_bits_param,
  output [3:0]   auto_tl_other_masters_out_a_bits_size,
  output [4:0]   auto_tl_other_masters_out_a_bits_source,
  output [36:0]  auto_tl_other_masters_out_a_bits_address,
  output         auto_tl_other_masters_out_a_bits_user_amba_prot_bufferable,
  output         auto_tl_other_masters_out_a_bits_user_amba_prot_modifiable,
  output         auto_tl_other_masters_out_a_bits_user_amba_prot_readalloc,
  output         auto_tl_other_masters_out_a_bits_user_amba_prot_writealloc,
  output         auto_tl_other_masters_out_a_bits_user_amba_prot_privileged,
  output         auto_tl_other_masters_out_a_bits_user_amba_prot_secure,
  output         auto_tl_other_masters_out_a_bits_user_amba_prot_fetch,
  output [15:0]  auto_tl_other_masters_out_a_bits_mask,
  output [127:0] auto_tl_other_masters_out_a_bits_data,
  output         auto_tl_other_masters_out_a_bits_corrupt,
  output         auto_tl_other_masters_out_b_ready,
  input          auto_tl_other_masters_out_b_valid,
  input  [2:0]   auto_tl_other_masters_out_b_bits_opcode,
  input  [1:0]   auto_tl_other_masters_out_b_bits_param,
  input  [3:0]   auto_tl_other_masters_out_b_bits_size,
  input  [4:0]   auto_tl_other_masters_out_b_bits_source,
  input  [36:0]  auto_tl_other_masters_out_b_bits_address,
  input  [15:0]  auto_tl_other_masters_out_b_bits_mask,
  input  [127:0] auto_tl_other_masters_out_b_bits_data,
  input          auto_tl_other_masters_out_b_bits_corrupt,
  input          auto_tl_other_masters_out_c_ready,
  output         auto_tl_other_masters_out_c_valid,
  output [2:0]   auto_tl_other_masters_out_c_bits_opcode,
  output [2:0]   auto_tl_other_masters_out_c_bits_param,
  output [3:0]   auto_tl_other_masters_out_c_bits_size,
  output [4:0]   auto_tl_other_masters_out_c_bits_source,
  output [36:0]  auto_tl_other_masters_out_c_bits_address,
  output [127:0] auto_tl_other_masters_out_c_bits_data,
  output         auto_tl_other_masters_out_c_bits_corrupt,
  output         auto_tl_other_masters_out_d_ready,
  input          auto_tl_other_masters_out_d_valid,
  input  [2:0]   auto_tl_other_masters_out_d_bits_opcode,
  input  [1:0]   auto_tl_other_masters_out_d_bits_param,
  input  [3:0]   auto_tl_other_masters_out_d_bits_size,
  input  [4:0]   auto_tl_other_masters_out_d_bits_source,
  input  [4:0]   auto_tl_other_masters_out_d_bits_sink,
  input          auto_tl_other_masters_out_d_bits_denied,
  input  [127:0] auto_tl_other_masters_out_d_bits_data,
  input          auto_tl_other_masters_out_d_bits_corrupt,
  input          auto_tl_other_masters_out_e_ready,
  output         auto_tl_other_masters_out_e_valid,
  output [4:0]   auto_tl_other_masters_out_e_bits_sink,
  input          psd_test_clock_enable
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
`endif // RANDOMIZE_REG_INIT
  wire  tlMasterXbar_rf_reset; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_clock; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_reset; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_1_a_ready; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_1_a_valid; // @[BaseTile.scala 207:32]
  wire [2:0] tlMasterXbar_auto_in_1_a_bits_opcode; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_1_a_bits_source; // @[BaseTile.scala 207:32]
  wire [36:0] tlMasterXbar_auto_in_1_a_bits_address; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_1_d_valid; // @[BaseTile.scala 207:32]
  wire [2:0] tlMasterXbar_auto_in_1_d_bits_opcode; // @[BaseTile.scala 207:32]
  wire [3:0] tlMasterXbar_auto_in_1_d_bits_size; // @[BaseTile.scala 207:32]
  wire [127:0] tlMasterXbar_auto_in_1_d_bits_data; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_1_d_bits_corrupt; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_a_ready; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_a_valid; // @[BaseTile.scala 207:32]
  wire [2:0] tlMasterXbar_auto_in_0_a_bits_opcode; // @[BaseTile.scala 207:32]
  wire [2:0] tlMasterXbar_auto_in_0_a_bits_param; // @[BaseTile.scala 207:32]
  wire [3:0] tlMasterXbar_auto_in_0_a_bits_size; // @[BaseTile.scala 207:32]
  wire [3:0] tlMasterXbar_auto_in_0_a_bits_source; // @[BaseTile.scala 207:32]
  wire [36:0] tlMasterXbar_auto_in_0_a_bits_address; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_a_bits_user_amba_prot_bufferable; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_a_bits_user_amba_prot_modifiable; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_a_bits_user_amba_prot_readalloc; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_a_bits_user_amba_prot_writealloc; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_a_bits_user_amba_prot_privileged; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_a_bits_user_amba_prot_secure; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_a_bits_user_amba_prot_fetch; // @[BaseTile.scala 207:32]
  wire [15:0] tlMasterXbar_auto_in_0_a_bits_mask; // @[BaseTile.scala 207:32]
  wire [127:0] tlMasterXbar_auto_in_0_a_bits_data; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_a_bits_corrupt; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_b_ready; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_b_valid; // @[BaseTile.scala 207:32]
  wire [2:0] tlMasterXbar_auto_in_0_b_bits_opcode; // @[BaseTile.scala 207:32]
  wire [1:0] tlMasterXbar_auto_in_0_b_bits_param; // @[BaseTile.scala 207:32]
  wire [3:0] tlMasterXbar_auto_in_0_b_bits_size; // @[BaseTile.scala 207:32]
  wire [3:0] tlMasterXbar_auto_in_0_b_bits_source; // @[BaseTile.scala 207:32]
  wire [36:0] tlMasterXbar_auto_in_0_b_bits_address; // @[BaseTile.scala 207:32]
  wire [15:0] tlMasterXbar_auto_in_0_b_bits_mask; // @[BaseTile.scala 207:32]
  wire [127:0] tlMasterXbar_auto_in_0_b_bits_data; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_b_bits_corrupt; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_c_ready; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_c_valid; // @[BaseTile.scala 207:32]
  wire [2:0] tlMasterXbar_auto_in_0_c_bits_opcode; // @[BaseTile.scala 207:32]
  wire [2:0] tlMasterXbar_auto_in_0_c_bits_param; // @[BaseTile.scala 207:32]
  wire [3:0] tlMasterXbar_auto_in_0_c_bits_size; // @[BaseTile.scala 207:32]
  wire [3:0] tlMasterXbar_auto_in_0_c_bits_source; // @[BaseTile.scala 207:32]
  wire [36:0] tlMasterXbar_auto_in_0_c_bits_address; // @[BaseTile.scala 207:32]
  wire [127:0] tlMasterXbar_auto_in_0_c_bits_data; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_c_bits_corrupt; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_d_ready; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_d_valid; // @[BaseTile.scala 207:32]
  wire [2:0] tlMasterXbar_auto_in_0_d_bits_opcode; // @[BaseTile.scala 207:32]
  wire [1:0] tlMasterXbar_auto_in_0_d_bits_param; // @[BaseTile.scala 207:32]
  wire [3:0] tlMasterXbar_auto_in_0_d_bits_size; // @[BaseTile.scala 207:32]
  wire [3:0] tlMasterXbar_auto_in_0_d_bits_source; // @[BaseTile.scala 207:32]
  wire [4:0] tlMasterXbar_auto_in_0_d_bits_sink; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_d_bits_denied; // @[BaseTile.scala 207:32]
  wire [127:0] tlMasterXbar_auto_in_0_d_bits_data; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_d_bits_corrupt; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_e_ready; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_in_0_e_valid; // @[BaseTile.scala 207:32]
  wire [4:0] tlMasterXbar_auto_in_0_e_bits_sink; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_a_ready; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_a_valid; // @[BaseTile.scala 207:32]
  wire [2:0] tlMasterXbar_auto_out_a_bits_opcode; // @[BaseTile.scala 207:32]
  wire [2:0] tlMasterXbar_auto_out_a_bits_param; // @[BaseTile.scala 207:32]
  wire [3:0] tlMasterXbar_auto_out_a_bits_size; // @[BaseTile.scala 207:32]
  wire [4:0] tlMasterXbar_auto_out_a_bits_source; // @[BaseTile.scala 207:32]
  wire [36:0] tlMasterXbar_auto_out_a_bits_address; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_a_bits_user_amba_prot_bufferable; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_a_bits_user_amba_prot_modifiable; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_a_bits_user_amba_prot_readalloc; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_a_bits_user_amba_prot_writealloc; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_a_bits_user_amba_prot_privileged; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_a_bits_user_amba_prot_secure; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_a_bits_user_amba_prot_fetch; // @[BaseTile.scala 207:32]
  wire [15:0] tlMasterXbar_auto_out_a_bits_mask; // @[BaseTile.scala 207:32]
  wire [127:0] tlMasterXbar_auto_out_a_bits_data; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_a_bits_corrupt; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_b_ready; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_b_valid; // @[BaseTile.scala 207:32]
  wire [2:0] tlMasterXbar_auto_out_b_bits_opcode; // @[BaseTile.scala 207:32]
  wire [1:0] tlMasterXbar_auto_out_b_bits_param; // @[BaseTile.scala 207:32]
  wire [3:0] tlMasterXbar_auto_out_b_bits_size; // @[BaseTile.scala 207:32]
  wire [4:0] tlMasterXbar_auto_out_b_bits_source; // @[BaseTile.scala 207:32]
  wire [36:0] tlMasterXbar_auto_out_b_bits_address; // @[BaseTile.scala 207:32]
  wire [15:0] tlMasterXbar_auto_out_b_bits_mask; // @[BaseTile.scala 207:32]
  wire [127:0] tlMasterXbar_auto_out_b_bits_data; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_b_bits_corrupt; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_c_ready; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_c_valid; // @[BaseTile.scala 207:32]
  wire [2:0] tlMasterXbar_auto_out_c_bits_opcode; // @[BaseTile.scala 207:32]
  wire [2:0] tlMasterXbar_auto_out_c_bits_param; // @[BaseTile.scala 207:32]
  wire [3:0] tlMasterXbar_auto_out_c_bits_size; // @[BaseTile.scala 207:32]
  wire [4:0] tlMasterXbar_auto_out_c_bits_source; // @[BaseTile.scala 207:32]
  wire [36:0] tlMasterXbar_auto_out_c_bits_address; // @[BaseTile.scala 207:32]
  wire [127:0] tlMasterXbar_auto_out_c_bits_data; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_c_bits_corrupt; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_d_ready; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_d_valid; // @[BaseTile.scala 207:32]
  wire [2:0] tlMasterXbar_auto_out_d_bits_opcode; // @[BaseTile.scala 207:32]
  wire [1:0] tlMasterXbar_auto_out_d_bits_param; // @[BaseTile.scala 207:32]
  wire [3:0] tlMasterXbar_auto_out_d_bits_size; // @[BaseTile.scala 207:32]
  wire [4:0] tlMasterXbar_auto_out_d_bits_source; // @[BaseTile.scala 207:32]
  wire [4:0] tlMasterXbar_auto_out_d_bits_sink; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_d_bits_denied; // @[BaseTile.scala 207:32]
  wire [127:0] tlMasterXbar_auto_out_d_bits_data; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_d_bits_corrupt; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_e_ready; // @[BaseTile.scala 207:32]
  wire  tlMasterXbar_auto_out_e_valid; // @[BaseTile.scala 207:32]
  wire [4:0] tlMasterXbar_auto_out_e_bits_sink; // @[BaseTile.scala 207:32]
  wire [1:0] broadcast_auto_in;
  wire [1:0] broadcast_auto_out;
  wire [36:0] broadcast_1_auto_in;
  wire [36:0] broadcast_1_auto_out_1;
  wire  broadcast_3_auto_in_rnmi;
  wire [36:0] broadcast_3_auto_in_rnmi_interrupt_vector;
  wire [36:0] broadcast_3_auto_in_rnmi_exception_vector;
  wire  broadcast_3_auto_out_rnmi;
  wire [36:0] broadcast_3_auto_out_rnmi_interrupt_vector;
  wire [36:0] broadcast_3_auto_out_rnmi_exception_vector;
  wire  broadcast_4_auto_in_0_valid;
  wire [39:0] broadcast_4_auto_in_0_iaddr;
  wire [31:0] broadcast_4_auto_in_0_insn;
  wire [2:0] broadcast_4_auto_in_0_priv;
  wire  broadcast_4_auto_in_0_exception;
  wire  broadcast_4_auto_in_0_interrupt;
  wire  broadcast_4_auto_out_0_valid;
  wire [39:0] broadcast_4_auto_out_0_iaddr;
  wire [31:0] broadcast_4_auto_out_0_insn;
  wire [2:0] broadcast_4_auto_out_0_priv;
  wire  broadcast_4_auto_out_0_exception;
  wire  broadcast_4_auto_out_0_interrupt;
  wire  nexus_1_auto_in_stall;
  wire  nexus_1_auto_out_stall;
  wire  broadcast_5_auto_in_0_valid_0;
  wire [2:0] broadcast_5_auto_in_0_action;
  wire  broadcast_5_auto_in_1_valid_0;
  wire [2:0] broadcast_5_auto_in_1_action;
  wire  broadcast_5_auto_in_2_valid_0;
  wire [2:0] broadcast_5_auto_in_2_action;
  wire  broadcast_5_auto_in_3_valid_0;
  wire [2:0] broadcast_5_auto_in_3_action;
  wire  broadcast_5_auto_in_4_valid_0;
  wire [2:0] broadcast_5_auto_in_4_action;
  wire  broadcast_5_auto_in_5_valid_0;
  wire [2:0] broadcast_5_auto_in_5_action;
  wire  broadcast_5_auto_in_6_valid_0;
  wire [2:0] broadcast_5_auto_in_6_action;
  wire  broadcast_5_auto_in_7_valid_0;
  wire [2:0] broadcast_5_auto_in_7_action;
  wire  broadcast_5_auto_in_8_valid_0;
  wire [2:0] broadcast_5_auto_in_8_action;
  wire  broadcast_5_auto_in_9_valid_0;
  wire [2:0] broadcast_5_auto_in_9_action;
  wire  broadcast_5_auto_in_10_valid_0;
  wire [2:0] broadcast_5_auto_in_10_action;
  wire  broadcast_5_auto_in_11_valid_0;
  wire [2:0] broadcast_5_auto_in_11_action;
  wire  broadcast_5_auto_in_12_valid_0;
  wire [2:0] broadcast_5_auto_in_12_action;
  wire  broadcast_5_auto_in_13_valid_0;
  wire [2:0] broadcast_5_auto_in_13_action;
  wire  broadcast_5_auto_in_14_valid_0;
  wire [2:0] broadcast_5_auto_in_14_action;
  wire  broadcast_5_auto_in_15_valid_0;
  wire [2:0] broadcast_5_auto_in_15_action;
  wire  broadcast_5_auto_out_0_valid_0;
  wire [2:0] broadcast_5_auto_out_0_action;
  wire  broadcast_5_auto_out_1_valid_0;
  wire [2:0] broadcast_5_auto_out_1_action;
  wire  broadcast_5_auto_out_2_valid_0;
  wire [2:0] broadcast_5_auto_out_2_action;
  wire  broadcast_5_auto_out_3_valid_0;
  wire [2:0] broadcast_5_auto_out_3_action;
  wire  broadcast_5_auto_out_4_valid_0;
  wire [2:0] broadcast_5_auto_out_4_action;
  wire  broadcast_5_auto_out_5_valid_0;
  wire [2:0] broadcast_5_auto_out_5_action;
  wire  broadcast_5_auto_out_6_valid_0;
  wire [2:0] broadcast_5_auto_out_6_action;
  wire  broadcast_5_auto_out_7_valid_0;
  wire [2:0] broadcast_5_auto_out_7_action;
  wire  broadcast_5_auto_out_8_valid_0;
  wire [2:0] broadcast_5_auto_out_8_action;
  wire  broadcast_5_auto_out_9_valid_0;
  wire [2:0] broadcast_5_auto_out_9_action;
  wire  broadcast_5_auto_out_10_valid_0;
  wire [2:0] broadcast_5_auto_out_10_action;
  wire  broadcast_5_auto_out_11_valid_0;
  wire [2:0] broadcast_5_auto_out_11_action;
  wire  broadcast_5_auto_out_12_valid_0;
  wire [2:0] broadcast_5_auto_out_12_action;
  wire  broadcast_5_auto_out_13_valid_0;
  wire [2:0] broadcast_5_auto_out_13_action;
  wire  broadcast_5_auto_out_14_valid_0;
  wire [2:0] broadcast_5_auto_out_14_action;
  wire  broadcast_5_auto_out_15_valid_0;
  wire [2:0] broadcast_5_auto_out_15_action;
  wire  dcache_rf_reset; // @[HellaCache.scala 291:43]
  wire  dcache_clock; // @[HellaCache.scala 291:43]
  wire  dcache_reset; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_a_ready; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_a_valid; // @[HellaCache.scala 291:43]
  wire [2:0] dcache_auto_out_a_bits_opcode; // @[HellaCache.scala 291:43]
  wire [2:0] dcache_auto_out_a_bits_param; // @[HellaCache.scala 291:43]
  wire [3:0] dcache_auto_out_a_bits_size; // @[HellaCache.scala 291:43]
  wire [3:0] dcache_auto_out_a_bits_source; // @[HellaCache.scala 291:43]
  wire [36:0] dcache_auto_out_a_bits_address; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_a_bits_user_amba_prot_bufferable; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_a_bits_user_amba_prot_modifiable; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_a_bits_user_amba_prot_readalloc; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_a_bits_user_amba_prot_writealloc; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_a_bits_user_amba_prot_privileged; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_a_bits_user_amba_prot_secure; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_a_bits_user_amba_prot_fetch; // @[HellaCache.scala 291:43]
  wire [15:0] dcache_auto_out_a_bits_mask; // @[HellaCache.scala 291:43]
  wire [127:0] dcache_auto_out_a_bits_data; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_a_bits_corrupt; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_b_ready; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_b_valid; // @[HellaCache.scala 291:43]
  wire [2:0] dcache_auto_out_b_bits_opcode; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_auto_out_b_bits_param; // @[HellaCache.scala 291:43]
  wire [3:0] dcache_auto_out_b_bits_size; // @[HellaCache.scala 291:43]
  wire [3:0] dcache_auto_out_b_bits_source; // @[HellaCache.scala 291:43]
  wire [36:0] dcache_auto_out_b_bits_address; // @[HellaCache.scala 291:43]
  wire [15:0] dcache_auto_out_b_bits_mask; // @[HellaCache.scala 291:43]
  wire [127:0] dcache_auto_out_b_bits_data; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_b_bits_corrupt; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_c_ready; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_c_valid; // @[HellaCache.scala 291:43]
  wire [2:0] dcache_auto_out_c_bits_opcode; // @[HellaCache.scala 291:43]
  wire [2:0] dcache_auto_out_c_bits_param; // @[HellaCache.scala 291:43]
  wire [3:0] dcache_auto_out_c_bits_size; // @[HellaCache.scala 291:43]
  wire [3:0] dcache_auto_out_c_bits_source; // @[HellaCache.scala 291:43]
  wire [36:0] dcache_auto_out_c_bits_address; // @[HellaCache.scala 291:43]
  wire [127:0] dcache_auto_out_c_bits_data; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_c_bits_corrupt; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_d_ready; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_d_valid; // @[HellaCache.scala 291:43]
  wire [2:0] dcache_auto_out_d_bits_opcode; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_auto_out_d_bits_param; // @[HellaCache.scala 291:43]
  wire [3:0] dcache_auto_out_d_bits_size; // @[HellaCache.scala 291:43]
  wire [3:0] dcache_auto_out_d_bits_source; // @[HellaCache.scala 291:43]
  wire [4:0] dcache_auto_out_d_bits_sink; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_d_bits_denied; // @[HellaCache.scala 291:43]
  wire [127:0] dcache_auto_out_d_bits_data; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_d_bits_corrupt; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_e_ready; // @[HellaCache.scala 291:43]
  wire  dcache_auto_out_e_valid; // @[HellaCache.scala 291:43]
  wire [4:0] dcache_auto_out_e_bits_sink; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_req_ready; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_req_valid; // @[HellaCache.scala 291:43]
  wire [39:0] dcache_io_cpu_req_bits_addr; // @[HellaCache.scala 291:43]
  wire [39:0] dcache_io_cpu_req_bits_idx; // @[HellaCache.scala 291:43]
  wire [6:0] dcache_io_cpu_req_bits_tag; // @[HellaCache.scala 291:43]
  wire [4:0] dcache_io_cpu_req_bits_cmd; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_io_cpu_req_bits_size; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_req_bits_signed; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_io_cpu_req_bits_dprv; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_req_bits_phys; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_req_bits_no_alloc; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_req_bits_no_xcpt; // @[HellaCache.scala 291:43]
  wire [7:0] dcache_io_cpu_req_bits_mask; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_s1_kill; // @[HellaCache.scala 291:43]
  wire [63:0] dcache_io_cpu_s1_data_data; // @[HellaCache.scala 291:43]
  wire [7:0] dcache_io_cpu_s1_data_mask; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_s2_nack; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_resp_valid; // @[HellaCache.scala 291:43]
  wire [39:0] dcache_io_cpu_resp_bits_addr; // @[HellaCache.scala 291:43]
  wire [39:0] dcache_io_cpu_resp_bits_idx; // @[HellaCache.scala 291:43]
  wire [6:0] dcache_io_cpu_resp_bits_tag; // @[HellaCache.scala 291:43]
  wire [4:0] dcache_io_cpu_resp_bits_cmd; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_io_cpu_resp_bits_size; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_resp_bits_signed; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_io_cpu_resp_bits_dprv; // @[HellaCache.scala 291:43]
  wire [63:0] dcache_io_cpu_resp_bits_data; // @[HellaCache.scala 291:43]
  wire [7:0] dcache_io_cpu_resp_bits_mask; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_resp_bits_replay; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_resp_bits_has_data; // @[HellaCache.scala 291:43]
  wire [63:0] dcache_io_cpu_resp_bits_data_word_bypass; // @[HellaCache.scala 291:43]
  wire [63:0] dcache_io_cpu_resp_bits_data_raw; // @[HellaCache.scala 291:43]
  wire [63:0] dcache_io_cpu_resp_bits_store_data; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_replay_next; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_s2_xcpt_ma_ld; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_s2_xcpt_ma_st; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_s2_xcpt_pf_ld; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_s2_xcpt_pf_st; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_s2_xcpt_ae_ld; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_s2_xcpt_ae_st; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_ordered; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_perf_acquire; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_perf_release; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_perf_grant; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_perf_tlbMiss; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_perf_blocked; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_keep_clock_enabled; // @[HellaCache.scala 291:43]
  wire  dcache_io_cpu_clock_enabled; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_req_ready; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_req_valid; // @[HellaCache.scala 291:43]
  wire [26:0] dcache_io_ptw_req_bits_bits_addr; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_resp_valid; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_resp_bits_ae; // @[HellaCache.scala 291:43]
  wire [53:0] dcache_io_ptw_resp_bits_pte_ppn; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_resp_bits_pte_d; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_resp_bits_pte_a; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_resp_bits_pte_g; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_resp_bits_pte_u; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_resp_bits_pte_x; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_resp_bits_pte_w; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_resp_bits_pte_r; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_resp_bits_pte_v; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_io_ptw_resp_bits_level; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_resp_bits_homogeneous; // @[HellaCache.scala 291:43]
  wire [3:0] dcache_io_ptw_ptbr_mode; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_status_debug; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_io_ptw_status_dprv; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_status_mxr; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_status_sum; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_0_cfg_l; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_io_ptw_pmp_0_cfg_a; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_0_cfg_x; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_0_cfg_w; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_0_cfg_r; // @[HellaCache.scala 291:43]
  wire [34:0] dcache_io_ptw_pmp_0_addr; // @[HellaCache.scala 291:43]
  wire [36:0] dcache_io_ptw_pmp_0_mask; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_1_cfg_l; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_io_ptw_pmp_1_cfg_a; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_1_cfg_x; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_1_cfg_w; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_1_cfg_r; // @[HellaCache.scala 291:43]
  wire [34:0] dcache_io_ptw_pmp_1_addr; // @[HellaCache.scala 291:43]
  wire [36:0] dcache_io_ptw_pmp_1_mask; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_2_cfg_l; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_io_ptw_pmp_2_cfg_a; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_2_cfg_x; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_2_cfg_w; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_2_cfg_r; // @[HellaCache.scala 291:43]
  wire [34:0] dcache_io_ptw_pmp_2_addr; // @[HellaCache.scala 291:43]
  wire [36:0] dcache_io_ptw_pmp_2_mask; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_3_cfg_l; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_io_ptw_pmp_3_cfg_a; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_3_cfg_x; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_3_cfg_w; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_3_cfg_r; // @[HellaCache.scala 291:43]
  wire [34:0] dcache_io_ptw_pmp_3_addr; // @[HellaCache.scala 291:43]
  wire [36:0] dcache_io_ptw_pmp_3_mask; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_4_cfg_l; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_io_ptw_pmp_4_cfg_a; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_4_cfg_x; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_4_cfg_w; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_4_cfg_r; // @[HellaCache.scala 291:43]
  wire [34:0] dcache_io_ptw_pmp_4_addr; // @[HellaCache.scala 291:43]
  wire [36:0] dcache_io_ptw_pmp_4_mask; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_5_cfg_l; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_io_ptw_pmp_5_cfg_a; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_5_cfg_x; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_5_cfg_w; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_5_cfg_r; // @[HellaCache.scala 291:43]
  wire [34:0] dcache_io_ptw_pmp_5_addr; // @[HellaCache.scala 291:43]
  wire [36:0] dcache_io_ptw_pmp_5_mask; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_6_cfg_l; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_io_ptw_pmp_6_cfg_a; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_6_cfg_x; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_6_cfg_w; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_6_cfg_r; // @[HellaCache.scala 291:43]
  wire [34:0] dcache_io_ptw_pmp_6_addr; // @[HellaCache.scala 291:43]
  wire [36:0] dcache_io_ptw_pmp_6_mask; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_7_cfg_l; // @[HellaCache.scala 291:43]
  wire [1:0] dcache_io_ptw_pmp_7_cfg_a; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_7_cfg_x; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_7_cfg_w; // @[HellaCache.scala 291:43]
  wire  dcache_io_ptw_pmp_7_cfg_r; // @[HellaCache.scala 291:43]
  wire [34:0] dcache_io_ptw_pmp_7_addr; // @[HellaCache.scala 291:43]
  wire [36:0] dcache_io_ptw_pmp_7_mask; // @[HellaCache.scala 291:43]
  wire [63:0] dcache_io_ptw_customCSRs_csrs_1_value; // @[HellaCache.scala 291:43]
  wire  dcache_io_errors_bus_valid; // @[HellaCache.scala 291:43]
  wire  dcache_psd_test_clock_enable; // @[HellaCache.scala 291:43]
  wire  frontend_rf_reset; // @[Frontend.scala 353:28]
  wire  frontend_clock; // @[Frontend.scala 353:28]
  wire  frontend_reset; // @[Frontend.scala 353:28]
  wire  frontend_auto_icache_master_out_a_ready; // @[Frontend.scala 353:28]
  wire  frontend_auto_icache_master_out_a_valid; // @[Frontend.scala 353:28]
  wire [2:0] frontend_auto_icache_master_out_a_bits_opcode; // @[Frontend.scala 353:28]
  wire  frontend_auto_icache_master_out_a_bits_source; // @[Frontend.scala 353:28]
  wire [36:0] frontend_auto_icache_master_out_a_bits_address; // @[Frontend.scala 353:28]
  wire  frontend_auto_icache_master_out_d_valid; // @[Frontend.scala 353:28]
  wire [2:0] frontend_auto_icache_master_out_d_bits_opcode; // @[Frontend.scala 353:28]
  wire [3:0] frontend_auto_icache_master_out_d_bits_size; // @[Frontend.scala 353:28]
  wire [127:0] frontend_auto_icache_master_out_d_bits_data; // @[Frontend.scala 353:28]
  wire  frontend_auto_icache_master_out_d_bits_corrupt; // @[Frontend.scala 353:28]
  wire [36:0] frontend_auto_reset_vector_sink_in; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_might_request; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_clock_enabled; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_req_valid; // @[Frontend.scala 353:28]
  wire [39:0] frontend_io_cpu_req_bits_pc; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_req_bits_speculative; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_sfence_valid; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_sfence_bits_rs1; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_sfence_bits_rs2; // @[Frontend.scala 353:28]
  wire [38:0] frontend_io_cpu_sfence_bits_addr; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_sfence_bits_asid; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_resp_ready; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_resp_valid; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_resp_bits_btb_taken; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_resp_bits_btb_bridx; // @[Frontend.scala 353:28]
  wire [4:0] frontend_io_cpu_resp_bits_btb_entry; // @[Frontend.scala 353:28]
  wire [7:0] frontend_io_cpu_resp_bits_btb_bht_history; // @[Frontend.scala 353:28]
  wire [39:0] frontend_io_cpu_resp_bits_pc; // @[Frontend.scala 353:28]
  wire [31:0] frontend_io_cpu_resp_bits_data; // @[Frontend.scala 353:28]
  wire [1:0] frontend_io_cpu_resp_bits_mask; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_resp_bits_xcpt_pf_inst; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_resp_bits_xcpt_ae_inst; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_resp_bits_replay; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_btb_update_valid; // @[Frontend.scala 353:28]
  wire [4:0] frontend_io_cpu_btb_update_bits_prediction_entry; // @[Frontend.scala 353:28]
  wire [38:0] frontend_io_cpu_btb_update_bits_pc; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_btb_update_bits_isValid; // @[Frontend.scala 353:28]
  wire [38:0] frontend_io_cpu_btb_update_bits_br_pc; // @[Frontend.scala 353:28]
  wire [1:0] frontend_io_cpu_btb_update_bits_cfiType; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_bht_update_valid; // @[Frontend.scala 353:28]
  wire [7:0] frontend_io_cpu_bht_update_bits_prediction_history; // @[Frontend.scala 353:28]
  wire [38:0] frontend_io_cpu_bht_update_bits_pc; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_bht_update_bits_branch; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_bht_update_bits_taken; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_bht_update_bits_mispredict; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_flush_icache; // @[Frontend.scala 353:28]
  wire [39:0] frontend_io_cpu_npc; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_perf_acquire; // @[Frontend.scala 353:28]
  wire  frontend_io_cpu_perf_tlbMiss; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_req_ready; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_req_valid; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_req_bits_valid; // @[Frontend.scala 353:28]
  wire [26:0] frontend_io_ptw_req_bits_bits_addr; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_resp_valid; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_resp_bits_ae; // @[Frontend.scala 353:28]
  wire [53:0] frontend_io_ptw_resp_bits_pte_ppn; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_resp_bits_pte_d; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_resp_bits_pte_a; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_resp_bits_pte_g; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_resp_bits_pte_u; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_resp_bits_pte_x; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_resp_bits_pte_w; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_resp_bits_pte_r; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_resp_bits_pte_v; // @[Frontend.scala 353:28]
  wire [1:0] frontend_io_ptw_resp_bits_level; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_resp_bits_homogeneous; // @[Frontend.scala 353:28]
  wire [3:0] frontend_io_ptw_ptbr_mode; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_status_debug; // @[Frontend.scala 353:28]
  wire [1:0] frontend_io_ptw_status_prv; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_0_cfg_l; // @[Frontend.scala 353:28]
  wire [1:0] frontend_io_ptw_pmp_0_cfg_a; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_0_cfg_x; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_0_cfg_w; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_0_cfg_r; // @[Frontend.scala 353:28]
  wire [34:0] frontend_io_ptw_pmp_0_addr; // @[Frontend.scala 353:28]
  wire [36:0] frontend_io_ptw_pmp_0_mask; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_1_cfg_l; // @[Frontend.scala 353:28]
  wire [1:0] frontend_io_ptw_pmp_1_cfg_a; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_1_cfg_x; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_1_cfg_w; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_1_cfg_r; // @[Frontend.scala 353:28]
  wire [34:0] frontend_io_ptw_pmp_1_addr; // @[Frontend.scala 353:28]
  wire [36:0] frontend_io_ptw_pmp_1_mask; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_2_cfg_l; // @[Frontend.scala 353:28]
  wire [1:0] frontend_io_ptw_pmp_2_cfg_a; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_2_cfg_x; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_2_cfg_w; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_2_cfg_r; // @[Frontend.scala 353:28]
  wire [34:0] frontend_io_ptw_pmp_2_addr; // @[Frontend.scala 353:28]
  wire [36:0] frontend_io_ptw_pmp_2_mask; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_3_cfg_l; // @[Frontend.scala 353:28]
  wire [1:0] frontend_io_ptw_pmp_3_cfg_a; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_3_cfg_x; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_3_cfg_w; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_3_cfg_r; // @[Frontend.scala 353:28]
  wire [34:0] frontend_io_ptw_pmp_3_addr; // @[Frontend.scala 353:28]
  wire [36:0] frontend_io_ptw_pmp_3_mask; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_4_cfg_l; // @[Frontend.scala 353:28]
  wire [1:0] frontend_io_ptw_pmp_4_cfg_a; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_4_cfg_x; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_4_cfg_w; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_4_cfg_r; // @[Frontend.scala 353:28]
  wire [34:0] frontend_io_ptw_pmp_4_addr; // @[Frontend.scala 353:28]
  wire [36:0] frontend_io_ptw_pmp_4_mask; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_5_cfg_l; // @[Frontend.scala 353:28]
  wire [1:0] frontend_io_ptw_pmp_5_cfg_a; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_5_cfg_x; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_5_cfg_w; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_5_cfg_r; // @[Frontend.scala 353:28]
  wire [34:0] frontend_io_ptw_pmp_5_addr; // @[Frontend.scala 353:28]
  wire [36:0] frontend_io_ptw_pmp_5_mask; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_6_cfg_l; // @[Frontend.scala 353:28]
  wire [1:0] frontend_io_ptw_pmp_6_cfg_a; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_6_cfg_x; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_6_cfg_w; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_6_cfg_r; // @[Frontend.scala 353:28]
  wire [34:0] frontend_io_ptw_pmp_6_addr; // @[Frontend.scala 353:28]
  wire [36:0] frontend_io_ptw_pmp_6_mask; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_7_cfg_l; // @[Frontend.scala 353:28]
  wire [1:0] frontend_io_ptw_pmp_7_cfg_a; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_7_cfg_x; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_7_cfg_w; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_pmp_7_cfg_r; // @[Frontend.scala 353:28]
  wire [34:0] frontend_io_ptw_pmp_7_addr; // @[Frontend.scala 353:28]
  wire [36:0] frontend_io_ptw_pmp_7_mask; // @[Frontend.scala 353:28]
  wire  frontend_io_ptw_customCSRs_csrs_0_wen; // @[Frontend.scala 353:28]
  wire [63:0] frontend_io_ptw_customCSRs_csrs_0_value; // @[Frontend.scala 353:28]
  wire [63:0] frontend_io_ptw_customCSRs_csrs_1_value; // @[Frontend.scala 353:28]
  wire  frontend_psd_test_clock_enable; // @[Frontend.scala 353:28]
  wire  fpuOpt_rf_reset; // @[RocketTile.scala 224:62]
  wire  fpuOpt_clock; // @[RocketTile.scala 224:62]
  wire  fpuOpt_reset; // @[RocketTile.scala 224:62]
  wire [31:0] fpuOpt_io_inst; // @[RocketTile.scala 224:62]
  wire [63:0] fpuOpt_io_fromint_data; // @[RocketTile.scala 224:62]
  wire [2:0] fpuOpt_io_fcsr_rm; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_fcsr_flags_valid; // @[RocketTile.scala 224:62]
  wire [4:0] fpuOpt_io_fcsr_flags_bits; // @[RocketTile.scala 224:62]
  wire [63:0] fpuOpt_io_store_data; // @[RocketTile.scala 224:62]
  wire [63:0] fpuOpt_io_toint_data; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_dmem_resp_val; // @[RocketTile.scala 224:62]
  wire [2:0] fpuOpt_io_dmem_resp_type; // @[RocketTile.scala 224:62]
  wire [4:0] fpuOpt_io_dmem_resp_tag; // @[RocketTile.scala 224:62]
  wire [63:0] fpuOpt_io_dmem_resp_data; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_valid; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_fcsr_rdy; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_nack_mem; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_illegal_rm; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_killx; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_killm; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_dec_ldst; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_dec_wen; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_dec_ren1; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_dec_ren2; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_dec_ren3; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_dec_swap23; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_dec_fma; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_dec_div; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_dec_sqrt; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_sboard_set; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_sboard_clr; // @[RocketTile.scala 224:62]
  wire [4:0] fpuOpt_io_sboard_clra; // @[RocketTile.scala 224:62]
  wire  fpuOpt_io_keep_clock_enabled; // @[RocketTile.scala 224:62]
  wire  fpuOpt_psd_test_clock_enable; // @[RocketTile.scala 224:62]
  wire  dcacheArb_rf_reset; // @[HellaCache.scala 302:25]
  wire  dcacheArb_clock; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_0_req_ready; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_0_req_valid; // @[HellaCache.scala 302:25]
  wire [39:0] dcacheArb_io_requestor_0_req_bits_addr; // @[HellaCache.scala 302:25]
  wire [39:0] dcacheArb_io_requestor_0_req_bits_idx; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_0_s1_kill; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_0_s2_nack; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_0_resp_valid; // @[HellaCache.scala 302:25]
  wire [63:0] dcacheArb_io_requestor_0_resp_bits_data; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_0_s2_xcpt_ae_ld; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_req_ready; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_req_valid; // @[HellaCache.scala 302:25]
  wire [39:0] dcacheArb_io_requestor_1_req_bits_addr; // @[HellaCache.scala 302:25]
  wire [39:0] dcacheArb_io_requestor_1_req_bits_idx; // @[HellaCache.scala 302:25]
  wire [6:0] dcacheArb_io_requestor_1_req_bits_tag; // @[HellaCache.scala 302:25]
  wire [4:0] dcacheArb_io_requestor_1_req_bits_cmd; // @[HellaCache.scala 302:25]
  wire [1:0] dcacheArb_io_requestor_1_req_bits_size; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_req_bits_signed; // @[HellaCache.scala 302:25]
  wire [1:0] dcacheArb_io_requestor_1_req_bits_dprv; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_req_bits_phys; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_req_bits_no_alloc; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_req_bits_no_xcpt; // @[HellaCache.scala 302:25]
  wire [7:0] dcacheArb_io_requestor_1_req_bits_mask; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_s1_kill; // @[HellaCache.scala 302:25]
  wire [63:0] dcacheArb_io_requestor_1_s1_data_data; // @[HellaCache.scala 302:25]
  wire [7:0] dcacheArb_io_requestor_1_s1_data_mask; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_s2_nack; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_resp_valid; // @[HellaCache.scala 302:25]
  wire [39:0] dcacheArb_io_requestor_1_resp_bits_addr; // @[HellaCache.scala 302:25]
  wire [6:0] dcacheArb_io_requestor_1_resp_bits_tag; // @[HellaCache.scala 302:25]
  wire [4:0] dcacheArb_io_requestor_1_resp_bits_cmd; // @[HellaCache.scala 302:25]
  wire [1:0] dcacheArb_io_requestor_1_resp_bits_size; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_resp_bits_signed; // @[HellaCache.scala 302:25]
  wire [63:0] dcacheArb_io_requestor_1_resp_bits_data; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_resp_bits_replay; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_resp_bits_has_data; // @[HellaCache.scala 302:25]
  wire [63:0] dcacheArb_io_requestor_1_resp_bits_data_word_bypass; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_replay_next; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_s2_xcpt_ma_ld; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_s2_xcpt_ma_st; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_s2_xcpt_pf_ld; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_s2_xcpt_pf_st; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_s2_xcpt_ae_ld; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_s2_xcpt_ae_st; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_ordered; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_perf_acquire; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_perf_release; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_perf_grant; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_perf_tlbMiss; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_perf_blocked; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_keep_clock_enabled; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_requestor_1_clock_enabled; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_req_ready; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_req_valid; // @[HellaCache.scala 302:25]
  wire [39:0] dcacheArb_io_mem_req_bits_addr; // @[HellaCache.scala 302:25]
  wire [39:0] dcacheArb_io_mem_req_bits_idx; // @[HellaCache.scala 302:25]
  wire [6:0] dcacheArb_io_mem_req_bits_tag; // @[HellaCache.scala 302:25]
  wire [4:0] dcacheArb_io_mem_req_bits_cmd; // @[HellaCache.scala 302:25]
  wire [1:0] dcacheArb_io_mem_req_bits_size; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_req_bits_signed; // @[HellaCache.scala 302:25]
  wire [1:0] dcacheArb_io_mem_req_bits_dprv; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_req_bits_phys; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_req_bits_no_alloc; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_req_bits_no_xcpt; // @[HellaCache.scala 302:25]
  wire [7:0] dcacheArb_io_mem_req_bits_mask; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_s1_kill; // @[HellaCache.scala 302:25]
  wire [63:0] dcacheArb_io_mem_s1_data_data; // @[HellaCache.scala 302:25]
  wire [7:0] dcacheArb_io_mem_s1_data_mask; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_s2_nack; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_resp_valid; // @[HellaCache.scala 302:25]
  wire [39:0] dcacheArb_io_mem_resp_bits_addr; // @[HellaCache.scala 302:25]
  wire [6:0] dcacheArb_io_mem_resp_bits_tag; // @[HellaCache.scala 302:25]
  wire [4:0] dcacheArb_io_mem_resp_bits_cmd; // @[HellaCache.scala 302:25]
  wire [1:0] dcacheArb_io_mem_resp_bits_size; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_resp_bits_signed; // @[HellaCache.scala 302:25]
  wire [63:0] dcacheArb_io_mem_resp_bits_data; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_resp_bits_replay; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_resp_bits_has_data; // @[HellaCache.scala 302:25]
  wire [63:0] dcacheArb_io_mem_resp_bits_data_word_bypass; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_replay_next; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_s2_xcpt_ma_ld; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_s2_xcpt_ma_st; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_s2_xcpt_pf_ld; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_s2_xcpt_pf_st; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_s2_xcpt_ae_ld; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_s2_xcpt_ae_st; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_ordered; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_perf_acquire; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_perf_release; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_perf_grant; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_perf_tlbMiss; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_perf_blocked; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_keep_clock_enabled; // @[HellaCache.scala 302:25]
  wire  dcacheArb_io_mem_clock_enabled; // @[HellaCache.scala 302:25]
  wire  ptw_rf_reset; // @[PTW.scala 445:19]
  wire  ptw_clock; // @[PTW.scala 445:19]
  wire  ptw_reset; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_req_ready; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_req_valid; // @[PTW.scala 445:19]
  wire [26:0] ptw_io_requestor_0_req_bits_bits_addr; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_resp_valid; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_resp_bits_ae; // @[PTW.scala 445:19]
  wire [53:0] ptw_io_requestor_0_resp_bits_pte_ppn; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_resp_bits_pte_d; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_resp_bits_pte_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_resp_bits_pte_g; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_resp_bits_pte_u; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_resp_bits_pte_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_resp_bits_pte_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_resp_bits_pte_r; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_resp_bits_pte_v; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_0_resp_bits_level; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_resp_bits_homogeneous; // @[PTW.scala 445:19]
  wire [3:0] ptw_io_requestor_0_ptbr_mode; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_status_debug; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_0_status_dprv; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_status_mxr; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_status_sum; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_0_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_0_pmp_0_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_0_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_0_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_0_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_0_pmp_0_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_0_pmp_0_mask; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_1_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_0_pmp_1_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_1_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_1_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_1_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_0_pmp_1_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_0_pmp_1_mask; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_2_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_0_pmp_2_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_2_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_2_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_2_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_0_pmp_2_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_0_pmp_2_mask; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_3_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_0_pmp_3_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_3_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_3_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_3_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_0_pmp_3_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_0_pmp_3_mask; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_4_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_0_pmp_4_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_4_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_4_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_4_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_0_pmp_4_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_0_pmp_4_mask; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_5_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_0_pmp_5_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_5_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_5_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_5_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_0_pmp_5_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_0_pmp_5_mask; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_6_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_0_pmp_6_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_6_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_6_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_6_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_0_pmp_6_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_0_pmp_6_mask; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_7_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_0_pmp_7_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_7_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_7_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_0_pmp_7_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_0_pmp_7_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_0_pmp_7_mask; // @[PTW.scala 445:19]
  wire [63:0] ptw_io_requestor_0_customCSRs_csrs_1_value; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_req_ready; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_req_valid; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_req_bits_valid; // @[PTW.scala 445:19]
  wire [26:0] ptw_io_requestor_1_req_bits_bits_addr; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_resp_valid; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_resp_bits_ae; // @[PTW.scala 445:19]
  wire [53:0] ptw_io_requestor_1_resp_bits_pte_ppn; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_resp_bits_pte_d; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_resp_bits_pte_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_resp_bits_pte_g; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_resp_bits_pte_u; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_resp_bits_pte_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_resp_bits_pte_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_resp_bits_pte_r; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_resp_bits_pte_v; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_1_resp_bits_level; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_resp_bits_homogeneous; // @[PTW.scala 445:19]
  wire [3:0] ptw_io_requestor_1_ptbr_mode; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_status_debug; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_1_status_prv; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_0_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_1_pmp_0_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_0_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_0_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_0_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_1_pmp_0_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_1_pmp_0_mask; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_1_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_1_pmp_1_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_1_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_1_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_1_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_1_pmp_1_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_1_pmp_1_mask; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_2_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_1_pmp_2_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_2_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_2_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_2_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_1_pmp_2_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_1_pmp_2_mask; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_3_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_1_pmp_3_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_3_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_3_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_3_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_1_pmp_3_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_1_pmp_3_mask; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_4_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_1_pmp_4_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_4_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_4_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_4_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_1_pmp_4_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_1_pmp_4_mask; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_5_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_1_pmp_5_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_5_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_5_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_5_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_1_pmp_5_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_1_pmp_5_mask; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_6_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_1_pmp_6_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_6_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_6_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_6_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_1_pmp_6_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_1_pmp_6_mask; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_7_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_requestor_1_pmp_7_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_7_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_7_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_pmp_7_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_requestor_1_pmp_7_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_requestor_1_pmp_7_mask; // @[PTW.scala 445:19]
  wire  ptw_io_requestor_1_customCSRs_csrs_0_wen; // @[PTW.scala 445:19]
  wire [63:0] ptw_io_requestor_1_customCSRs_csrs_0_value; // @[PTW.scala 445:19]
  wire [63:0] ptw_io_requestor_1_customCSRs_csrs_1_value; // @[PTW.scala 445:19]
  wire  ptw_io_mem_req_ready; // @[PTW.scala 445:19]
  wire  ptw_io_mem_req_valid; // @[PTW.scala 445:19]
  wire [39:0] ptw_io_mem_req_bits_addr; // @[PTW.scala 445:19]
  wire [39:0] ptw_io_mem_req_bits_idx; // @[PTW.scala 445:19]
  wire  ptw_io_mem_s1_kill; // @[PTW.scala 445:19]
  wire  ptw_io_mem_s2_nack; // @[PTW.scala 445:19]
  wire  ptw_io_mem_resp_valid; // @[PTW.scala 445:19]
  wire [63:0] ptw_io_mem_resp_bits_data; // @[PTW.scala 445:19]
  wire  ptw_io_mem_s2_xcpt_ae_ld; // @[PTW.scala 445:19]
  wire [3:0] ptw_io_dpath_ptbr_mode; // @[PTW.scala 445:19]
  wire [43:0] ptw_io_dpath_ptbr_ppn; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_sfence_valid; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_sfence_bits_rs1; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_sfence_bits_rs2; // @[PTW.scala 445:19]
  wire [38:0] ptw_io_dpath_sfence_bits_addr; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_status_debug; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_dpath_status_dprv; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_dpath_status_prv; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_status_mxr; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_status_sum; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_0_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_dpath_pmp_0_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_0_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_0_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_0_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_dpath_pmp_0_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_dpath_pmp_0_mask; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_1_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_dpath_pmp_1_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_1_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_1_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_1_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_dpath_pmp_1_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_dpath_pmp_1_mask; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_2_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_dpath_pmp_2_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_2_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_2_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_2_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_dpath_pmp_2_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_dpath_pmp_2_mask; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_3_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_dpath_pmp_3_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_3_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_3_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_3_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_dpath_pmp_3_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_dpath_pmp_3_mask; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_4_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_dpath_pmp_4_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_4_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_4_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_4_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_dpath_pmp_4_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_dpath_pmp_4_mask; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_5_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_dpath_pmp_5_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_5_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_5_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_5_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_dpath_pmp_5_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_dpath_pmp_5_mask; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_6_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_dpath_pmp_6_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_6_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_6_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_6_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_dpath_pmp_6_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_dpath_pmp_6_mask; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_7_cfg_l; // @[PTW.scala 445:19]
  wire [1:0] ptw_io_dpath_pmp_7_cfg_a; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_7_cfg_x; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_7_cfg_w; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_pmp_7_cfg_r; // @[PTW.scala 445:19]
  wire [34:0] ptw_io_dpath_pmp_7_addr; // @[PTW.scala 445:19]
  wire [36:0] ptw_io_dpath_pmp_7_mask; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_perf_l2miss; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_perf_l2hit; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_perf_pte_miss; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_perf_pte_hit; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_customCSRs_csrs_0_wen; // @[PTW.scala 445:19]
  wire [63:0] ptw_io_dpath_customCSRs_csrs_0_value; // @[PTW.scala 445:19]
  wire [63:0] ptw_io_dpath_customCSRs_csrs_1_value; // @[PTW.scala 445:19]
  wire  ptw_io_dpath_clock_enabled; // @[PTW.scala 445:19]
  wire  ptw_psd_test_clock_enable; // @[PTW.scala 445:19]
  wire  core_rf_reset; // @[RocketTile.scala 157:20]
  wire  core_clock; // @[RocketTile.scala 157:20]
  wire  core_reset; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_hartid; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_debug; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_mtip; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_msip; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_meip; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_seip; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_0; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_1; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_2; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_3; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_4; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_5; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_6; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_7; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_8; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_9; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_10; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_11; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_12; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_13; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_14; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_lip_15; // @[RocketTile.scala 157:20]
  wire  core_io_interrupts_nmi_rnmi; // @[RocketTile.scala 157:20]
  wire [36:0] core_io_interrupts_nmi_rnmi_interrupt_vector; // @[RocketTile.scala 157:20]
  wire [36:0] core_io_interrupts_nmi_rnmi_exception_vector; // @[RocketTile.scala 157:20]
  wire  core_io_imem_might_request; // @[RocketTile.scala 157:20]
  wire  core_io_imem_req_valid; // @[RocketTile.scala 157:20]
  wire [39:0] core_io_imem_req_bits_pc; // @[RocketTile.scala 157:20]
  wire  core_io_imem_req_bits_speculative; // @[RocketTile.scala 157:20]
  wire  core_io_imem_sfence_valid; // @[RocketTile.scala 157:20]
  wire  core_io_imem_sfence_bits_rs1; // @[RocketTile.scala 157:20]
  wire  core_io_imem_sfence_bits_rs2; // @[RocketTile.scala 157:20]
  wire [38:0] core_io_imem_sfence_bits_addr; // @[RocketTile.scala 157:20]
  wire  core_io_imem_sfence_bits_asid; // @[RocketTile.scala 157:20]
  wire  core_io_imem_resp_ready; // @[RocketTile.scala 157:20]
  wire  core_io_imem_resp_valid; // @[RocketTile.scala 157:20]
  wire  core_io_imem_resp_bits_btb_taken; // @[RocketTile.scala 157:20]
  wire  core_io_imem_resp_bits_btb_bridx; // @[RocketTile.scala 157:20]
  wire [4:0] core_io_imem_resp_bits_btb_entry; // @[RocketTile.scala 157:20]
  wire [7:0] core_io_imem_resp_bits_btb_bht_history; // @[RocketTile.scala 157:20]
  wire [39:0] core_io_imem_resp_bits_pc; // @[RocketTile.scala 157:20]
  wire [31:0] core_io_imem_resp_bits_data; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_imem_resp_bits_mask; // @[RocketTile.scala 157:20]
  wire  core_io_imem_resp_bits_xcpt_pf_inst; // @[RocketTile.scala 157:20]
  wire  core_io_imem_resp_bits_xcpt_ae_inst; // @[RocketTile.scala 157:20]
  wire  core_io_imem_resp_bits_replay; // @[RocketTile.scala 157:20]
  wire  core_io_imem_btb_update_valid; // @[RocketTile.scala 157:20]
  wire [4:0] core_io_imem_btb_update_bits_prediction_entry; // @[RocketTile.scala 157:20]
  wire [38:0] core_io_imem_btb_update_bits_pc; // @[RocketTile.scala 157:20]
  wire  core_io_imem_btb_update_bits_isValid; // @[RocketTile.scala 157:20]
  wire [38:0] core_io_imem_btb_update_bits_br_pc; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_imem_btb_update_bits_cfiType; // @[RocketTile.scala 157:20]
  wire  core_io_imem_bht_update_valid; // @[RocketTile.scala 157:20]
  wire [7:0] core_io_imem_bht_update_bits_prediction_history; // @[RocketTile.scala 157:20]
  wire [38:0] core_io_imem_bht_update_bits_pc; // @[RocketTile.scala 157:20]
  wire  core_io_imem_bht_update_bits_branch; // @[RocketTile.scala 157:20]
  wire  core_io_imem_bht_update_bits_taken; // @[RocketTile.scala 157:20]
  wire  core_io_imem_bht_update_bits_mispredict; // @[RocketTile.scala 157:20]
  wire  core_io_imem_flush_icache; // @[RocketTile.scala 157:20]
  wire  core_io_imem_perf_acquire; // @[RocketTile.scala 157:20]
  wire  core_io_imem_perf_tlbMiss; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_req_ready; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_req_valid; // @[RocketTile.scala 157:20]
  wire [39:0] core_io_dmem_req_bits_addr; // @[RocketTile.scala 157:20]
  wire [39:0] core_io_dmem_req_bits_idx; // @[RocketTile.scala 157:20]
  wire [6:0] core_io_dmem_req_bits_tag; // @[RocketTile.scala 157:20]
  wire [4:0] core_io_dmem_req_bits_cmd; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_dmem_req_bits_size; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_req_bits_signed; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_dmem_req_bits_dprv; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_req_bits_phys; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_req_bits_no_alloc; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_req_bits_no_xcpt; // @[RocketTile.scala 157:20]
  wire [63:0] core_io_dmem_req_bits_data; // @[RocketTile.scala 157:20]
  wire [7:0] core_io_dmem_req_bits_mask; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_s1_kill; // @[RocketTile.scala 157:20]
  wire [63:0] core_io_dmem_s1_data_data; // @[RocketTile.scala 157:20]
  wire [7:0] core_io_dmem_s1_data_mask; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_s2_nack; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_resp_valid; // @[RocketTile.scala 157:20]
  wire [39:0] core_io_dmem_resp_bits_addr; // @[RocketTile.scala 157:20]
  wire [6:0] core_io_dmem_resp_bits_tag; // @[RocketTile.scala 157:20]
  wire [4:0] core_io_dmem_resp_bits_cmd; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_dmem_resp_bits_size; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_resp_bits_signed; // @[RocketTile.scala 157:20]
  wire [63:0] core_io_dmem_resp_bits_data; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_resp_bits_replay; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_resp_bits_has_data; // @[RocketTile.scala 157:20]
  wire [63:0] core_io_dmem_resp_bits_data_word_bypass; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_replay_next; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_s2_xcpt_ma_ld; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_s2_xcpt_ma_st; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_s2_xcpt_pf_ld; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_s2_xcpt_pf_st; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_s2_xcpt_ae_ld; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_s2_xcpt_ae_st; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_ordered; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_perf_acquire; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_perf_release; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_perf_grant; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_perf_tlbMiss; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_perf_blocked; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_keep_clock_enabled; // @[RocketTile.scala 157:20]
  wire  core_io_dmem_clock_enabled; // @[RocketTile.scala 157:20]
  wire [3:0] core_io_ptw_ptbr_mode; // @[RocketTile.scala 157:20]
  wire [43:0] core_io_ptw_ptbr_ppn; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_sfence_valid; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_sfence_bits_rs1; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_sfence_bits_rs2; // @[RocketTile.scala 157:20]
  wire [38:0] core_io_ptw_sfence_bits_addr; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_status_debug; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_ptw_status_dprv; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_ptw_status_prv; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_status_mxr; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_status_sum; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_0_cfg_l; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_ptw_pmp_0_cfg_a; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_0_cfg_x; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_0_cfg_w; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_0_cfg_r; // @[RocketTile.scala 157:20]
  wire [34:0] core_io_ptw_pmp_0_addr; // @[RocketTile.scala 157:20]
  wire [36:0] core_io_ptw_pmp_0_mask; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_1_cfg_l; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_ptw_pmp_1_cfg_a; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_1_cfg_x; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_1_cfg_w; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_1_cfg_r; // @[RocketTile.scala 157:20]
  wire [34:0] core_io_ptw_pmp_1_addr; // @[RocketTile.scala 157:20]
  wire [36:0] core_io_ptw_pmp_1_mask; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_2_cfg_l; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_ptw_pmp_2_cfg_a; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_2_cfg_x; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_2_cfg_w; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_2_cfg_r; // @[RocketTile.scala 157:20]
  wire [34:0] core_io_ptw_pmp_2_addr; // @[RocketTile.scala 157:20]
  wire [36:0] core_io_ptw_pmp_2_mask; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_3_cfg_l; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_ptw_pmp_3_cfg_a; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_3_cfg_x; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_3_cfg_w; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_3_cfg_r; // @[RocketTile.scala 157:20]
  wire [34:0] core_io_ptw_pmp_3_addr; // @[RocketTile.scala 157:20]
  wire [36:0] core_io_ptw_pmp_3_mask; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_4_cfg_l; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_ptw_pmp_4_cfg_a; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_4_cfg_x; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_4_cfg_w; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_4_cfg_r; // @[RocketTile.scala 157:20]
  wire [34:0] core_io_ptw_pmp_4_addr; // @[RocketTile.scala 157:20]
  wire [36:0] core_io_ptw_pmp_4_mask; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_5_cfg_l; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_ptw_pmp_5_cfg_a; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_5_cfg_x; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_5_cfg_w; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_5_cfg_r; // @[RocketTile.scala 157:20]
  wire [34:0] core_io_ptw_pmp_5_addr; // @[RocketTile.scala 157:20]
  wire [36:0] core_io_ptw_pmp_5_mask; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_6_cfg_l; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_ptw_pmp_6_cfg_a; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_6_cfg_x; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_6_cfg_w; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_6_cfg_r; // @[RocketTile.scala 157:20]
  wire [34:0] core_io_ptw_pmp_6_addr; // @[RocketTile.scala 157:20]
  wire [36:0] core_io_ptw_pmp_6_mask; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_7_cfg_l; // @[RocketTile.scala 157:20]
  wire [1:0] core_io_ptw_pmp_7_cfg_a; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_7_cfg_x; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_7_cfg_w; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_pmp_7_cfg_r; // @[RocketTile.scala 157:20]
  wire [34:0] core_io_ptw_pmp_7_addr; // @[RocketTile.scala 157:20]
  wire [36:0] core_io_ptw_pmp_7_mask; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_perf_l2miss; // @[RocketTile.scala 157:20]
  wire  core_io_ptw_customCSRs_csrs_0_wen; // @[RocketTile.scala 157:20]
  wire [63:0] core_io_ptw_customCSRs_csrs_0_value; // @[RocketTile.scala 157:20]
  wire [63:0] core_io_ptw_customCSRs_csrs_1_value; // @[RocketTile.scala 157:20]
  wire [31:0] core_io_fpu_inst; // @[RocketTile.scala 157:20]
  wire [63:0] core_io_fpu_fromint_data; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_fpu_fcsr_rm; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_fcsr_flags_valid; // @[RocketTile.scala 157:20]
  wire [4:0] core_io_fpu_fcsr_flags_bits; // @[RocketTile.scala 157:20]
  wire [63:0] core_io_fpu_store_data; // @[RocketTile.scala 157:20]
  wire [63:0] core_io_fpu_toint_data; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_dmem_resp_val; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_fpu_dmem_resp_type; // @[RocketTile.scala 157:20]
  wire [4:0] core_io_fpu_dmem_resp_tag; // @[RocketTile.scala 157:20]
  wire [63:0] core_io_fpu_dmem_resp_data; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_valid; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_fcsr_rdy; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_nack_mem; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_illegal_rm; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_killx; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_killm; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_dec_ldst; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_dec_wen; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_dec_ren1; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_dec_ren2; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_dec_ren3; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_dec_swap23; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_dec_fma; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_dec_div; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_dec_sqrt; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_sboard_set; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_sboard_clr; // @[RocketTile.scala 157:20]
  wire [4:0] core_io_fpu_sboard_clra; // @[RocketTile.scala 157:20]
  wire  core_io_fpu_keep_clock_enabled; // @[RocketTile.scala 157:20]
  wire  core_io_trace_0_valid; // @[RocketTile.scala 157:20]
  wire [39:0] core_io_trace_0_iaddr; // @[RocketTile.scala 157:20]
  wire [31:0] core_io_trace_0_insn; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_trace_0_priv; // @[RocketTile.scala 157:20]
  wire  core_io_trace_0_exception; // @[RocketTile.scala 157:20]
  wire  core_io_trace_0_interrupt; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_0_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_0_action; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_1_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_1_action; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_2_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_2_action; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_3_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_3_action; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_4_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_4_action; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_5_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_5_action; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_6_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_6_action; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_7_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_7_action; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_8_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_8_action; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_9_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_9_action; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_10_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_10_action; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_11_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_11_action; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_12_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_12_action; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_13_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_13_action; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_14_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_14_action; // @[RocketTile.scala 157:20]
  wire  core_io_bpwatch_15_valid_0; // @[RocketTile.scala 157:20]
  wire [2:0] core_io_bpwatch_15_action; // @[RocketTile.scala 157:20]
  wire  core_io_cease; // @[RocketTile.scala 157:20]
  wire  core_io_wfi; // @[RocketTile.scala 157:20]
  wire  core_io_debug; // @[RocketTile.scala 157:20]
  wire  core_io_traceStall; // @[RocketTile.scala 157:20]
  wire  core_psd_test_clock_enable; // @[RocketTile.scala 157:20]
  wire  _T_1 = ~frontend_io_cpu_clock_enabled; // @[RocketTile.scala 165:5]
  wire  _T_2 = ~dcache_io_cpu_clock_enabled & _T_1; // @[RocketTile.scala 164:47]
  wire  _T_3 = ~ptw_io_dpath_clock_enabled; // @[RocketTile.scala 166:5]
  wire  _T_4 = _T_2 & _T_3; // @[RocketTile.scala 165:49]
  wire  _T_5 = _T_4 & core_io_cease; // @[RocketTile.scala 166:33]
  reg [3:0] bundleOut_0_0_cease_count; // @[Interrupts.scala 97:26]
  wire  bundleOut_0_0_cease = bundleOut_0_0_cease_count >= 4'h8; // @[Interrupts.scala 98:29]
  wire  _bundleOut_0_0_cease_T_1 = ~bundleOut_0_0_cease; // @[Interrupts.scala 100:22]
  wire [3:0] _bundleOut_0_0_cease_count_T_1 = bundleOut_0_0_cease_count + 4'h1; // @[Interrupts.scala 100:51]
  reg  bundleOut_0_0_REG; // @[Interrupts.scala 118:36]
  RHEA__TLXbar_7 tlMasterXbar ( // @[BaseTile.scala 207:32]
    .rf_reset(tlMasterXbar_rf_reset),
    .clock(tlMasterXbar_clock),
    .reset(tlMasterXbar_reset),
    .auto_in_1_a_ready(tlMasterXbar_auto_in_1_a_ready),
    .auto_in_1_a_valid(tlMasterXbar_auto_in_1_a_valid),
    .auto_in_1_a_bits_opcode(tlMasterXbar_auto_in_1_a_bits_opcode),
    .auto_in_1_a_bits_source(tlMasterXbar_auto_in_1_a_bits_source),
    .auto_in_1_a_bits_address(tlMasterXbar_auto_in_1_a_bits_address),
    .auto_in_1_d_valid(tlMasterXbar_auto_in_1_d_valid),
    .auto_in_1_d_bits_opcode(tlMasterXbar_auto_in_1_d_bits_opcode),
    .auto_in_1_d_bits_size(tlMasterXbar_auto_in_1_d_bits_size),
    .auto_in_1_d_bits_data(tlMasterXbar_auto_in_1_d_bits_data),
    .auto_in_1_d_bits_corrupt(tlMasterXbar_auto_in_1_d_bits_corrupt),
    .auto_in_0_a_ready(tlMasterXbar_auto_in_0_a_ready),
    .auto_in_0_a_valid(tlMasterXbar_auto_in_0_a_valid),
    .auto_in_0_a_bits_opcode(tlMasterXbar_auto_in_0_a_bits_opcode),
    .auto_in_0_a_bits_param(tlMasterXbar_auto_in_0_a_bits_param),
    .auto_in_0_a_bits_size(tlMasterXbar_auto_in_0_a_bits_size),
    .auto_in_0_a_bits_source(tlMasterXbar_auto_in_0_a_bits_source),
    .auto_in_0_a_bits_address(tlMasterXbar_auto_in_0_a_bits_address),
    .auto_in_0_a_bits_user_amba_prot_bufferable(tlMasterXbar_auto_in_0_a_bits_user_amba_prot_bufferable),
    .auto_in_0_a_bits_user_amba_prot_modifiable(tlMasterXbar_auto_in_0_a_bits_user_amba_prot_modifiable),
    .auto_in_0_a_bits_user_amba_prot_readalloc(tlMasterXbar_auto_in_0_a_bits_user_amba_prot_readalloc),
    .auto_in_0_a_bits_user_amba_prot_writealloc(tlMasterXbar_auto_in_0_a_bits_user_amba_prot_writealloc),
    .auto_in_0_a_bits_user_amba_prot_privileged(tlMasterXbar_auto_in_0_a_bits_user_amba_prot_privileged),
    .auto_in_0_a_bits_user_amba_prot_secure(tlMasterXbar_auto_in_0_a_bits_user_amba_prot_secure),
    .auto_in_0_a_bits_user_amba_prot_fetch(tlMasterXbar_auto_in_0_a_bits_user_amba_prot_fetch),
    .auto_in_0_a_bits_mask(tlMasterXbar_auto_in_0_a_bits_mask),
    .auto_in_0_a_bits_data(tlMasterXbar_auto_in_0_a_bits_data),
    .auto_in_0_a_bits_corrupt(tlMasterXbar_auto_in_0_a_bits_corrupt),
    .auto_in_0_b_ready(tlMasterXbar_auto_in_0_b_ready),
    .auto_in_0_b_valid(tlMasterXbar_auto_in_0_b_valid),
    .auto_in_0_b_bits_opcode(tlMasterXbar_auto_in_0_b_bits_opcode),
    .auto_in_0_b_bits_param(tlMasterXbar_auto_in_0_b_bits_param),
    .auto_in_0_b_bits_size(tlMasterXbar_auto_in_0_b_bits_size),
    .auto_in_0_b_bits_source(tlMasterXbar_auto_in_0_b_bits_source),
    .auto_in_0_b_bits_address(tlMasterXbar_auto_in_0_b_bits_address),
    .auto_in_0_b_bits_mask(tlMasterXbar_auto_in_0_b_bits_mask),
    .auto_in_0_b_bits_data(tlMasterXbar_auto_in_0_b_bits_data),
    .auto_in_0_b_bits_corrupt(tlMasterXbar_auto_in_0_b_bits_corrupt),
    .auto_in_0_c_ready(tlMasterXbar_auto_in_0_c_ready),
    .auto_in_0_c_valid(tlMasterXbar_auto_in_0_c_valid),
    .auto_in_0_c_bits_opcode(tlMasterXbar_auto_in_0_c_bits_opcode),
    .auto_in_0_c_bits_param(tlMasterXbar_auto_in_0_c_bits_param),
    .auto_in_0_c_bits_size(tlMasterXbar_auto_in_0_c_bits_size),
    .auto_in_0_c_bits_source(tlMasterXbar_auto_in_0_c_bits_source),
    .auto_in_0_c_bits_address(tlMasterXbar_auto_in_0_c_bits_address),
    .auto_in_0_c_bits_data(tlMasterXbar_auto_in_0_c_bits_data),
    .auto_in_0_c_bits_corrupt(tlMasterXbar_auto_in_0_c_bits_corrupt),
    .auto_in_0_d_ready(tlMasterXbar_auto_in_0_d_ready),
    .auto_in_0_d_valid(tlMasterXbar_auto_in_0_d_valid),
    .auto_in_0_d_bits_opcode(tlMasterXbar_auto_in_0_d_bits_opcode),
    .auto_in_0_d_bits_param(tlMasterXbar_auto_in_0_d_bits_param),
    .auto_in_0_d_bits_size(tlMasterXbar_auto_in_0_d_bits_size),
    .auto_in_0_d_bits_source(tlMasterXbar_auto_in_0_d_bits_source),
    .auto_in_0_d_bits_sink(tlMasterXbar_auto_in_0_d_bits_sink),
    .auto_in_0_d_bits_denied(tlMasterXbar_auto_in_0_d_bits_denied),
    .auto_in_0_d_bits_data(tlMasterXbar_auto_in_0_d_bits_data),
    .auto_in_0_d_bits_corrupt(tlMasterXbar_auto_in_0_d_bits_corrupt),
    .auto_in_0_e_ready(tlMasterXbar_auto_in_0_e_ready),
    .auto_in_0_e_valid(tlMasterXbar_auto_in_0_e_valid),
    .auto_in_0_e_bits_sink(tlMasterXbar_auto_in_0_e_bits_sink),
    .auto_out_a_ready(tlMasterXbar_auto_out_a_ready),
    .auto_out_a_valid(tlMasterXbar_auto_out_a_valid),
    .auto_out_a_bits_opcode(tlMasterXbar_auto_out_a_bits_opcode),
    .auto_out_a_bits_param(tlMasterXbar_auto_out_a_bits_param),
    .auto_out_a_bits_size(tlMasterXbar_auto_out_a_bits_size),
    .auto_out_a_bits_source(tlMasterXbar_auto_out_a_bits_source),
    .auto_out_a_bits_address(tlMasterXbar_auto_out_a_bits_address),
    .auto_out_a_bits_user_amba_prot_bufferable(tlMasterXbar_auto_out_a_bits_user_amba_prot_bufferable),
    .auto_out_a_bits_user_amba_prot_modifiable(tlMasterXbar_auto_out_a_bits_user_amba_prot_modifiable),
    .auto_out_a_bits_user_amba_prot_readalloc(tlMasterXbar_auto_out_a_bits_user_amba_prot_readalloc),
    .auto_out_a_bits_user_amba_prot_writealloc(tlMasterXbar_auto_out_a_bits_user_amba_prot_writealloc),
    .auto_out_a_bits_user_amba_prot_privileged(tlMasterXbar_auto_out_a_bits_user_amba_prot_privileged),
    .auto_out_a_bits_user_amba_prot_secure(tlMasterXbar_auto_out_a_bits_user_amba_prot_secure),
    .auto_out_a_bits_user_amba_prot_fetch(tlMasterXbar_auto_out_a_bits_user_amba_prot_fetch),
    .auto_out_a_bits_mask(tlMasterXbar_auto_out_a_bits_mask),
    .auto_out_a_bits_data(tlMasterXbar_auto_out_a_bits_data),
    .auto_out_a_bits_corrupt(tlMasterXbar_auto_out_a_bits_corrupt),
    .auto_out_b_ready(tlMasterXbar_auto_out_b_ready),
    .auto_out_b_valid(tlMasterXbar_auto_out_b_valid),
    .auto_out_b_bits_opcode(tlMasterXbar_auto_out_b_bits_opcode),
    .auto_out_b_bits_param(tlMasterXbar_auto_out_b_bits_param),
    .auto_out_b_bits_size(tlMasterXbar_auto_out_b_bits_size),
    .auto_out_b_bits_source(tlMasterXbar_auto_out_b_bits_source),
    .auto_out_b_bits_address(tlMasterXbar_auto_out_b_bits_address),
    .auto_out_b_bits_mask(tlMasterXbar_auto_out_b_bits_mask),
    .auto_out_b_bits_data(tlMasterXbar_auto_out_b_bits_data),
    .auto_out_b_bits_corrupt(tlMasterXbar_auto_out_b_bits_corrupt),
    .auto_out_c_ready(tlMasterXbar_auto_out_c_ready),
    .auto_out_c_valid(tlMasterXbar_auto_out_c_valid),
    .auto_out_c_bits_opcode(tlMasterXbar_auto_out_c_bits_opcode),
    .auto_out_c_bits_param(tlMasterXbar_auto_out_c_bits_param),
    .auto_out_c_bits_size(tlMasterXbar_auto_out_c_bits_size),
    .auto_out_c_bits_source(tlMasterXbar_auto_out_c_bits_source),
    .auto_out_c_bits_address(tlMasterXbar_auto_out_c_bits_address),
    .auto_out_c_bits_data(tlMasterXbar_auto_out_c_bits_data),
    .auto_out_c_bits_corrupt(tlMasterXbar_auto_out_c_bits_corrupt),
    .auto_out_d_ready(tlMasterXbar_auto_out_d_ready),
    .auto_out_d_valid(tlMasterXbar_auto_out_d_valid),
    .auto_out_d_bits_opcode(tlMasterXbar_auto_out_d_bits_opcode),
    .auto_out_d_bits_param(tlMasterXbar_auto_out_d_bits_param),
    .auto_out_d_bits_size(tlMasterXbar_auto_out_d_bits_size),
    .auto_out_d_bits_source(tlMasterXbar_auto_out_d_bits_source),
    .auto_out_d_bits_sink(tlMasterXbar_auto_out_d_bits_sink),
    .auto_out_d_bits_denied(tlMasterXbar_auto_out_d_bits_denied),
    .auto_out_d_bits_data(tlMasterXbar_auto_out_d_bits_data),
    .auto_out_d_bits_corrupt(tlMasterXbar_auto_out_d_bits_corrupt),
    .auto_out_e_ready(tlMasterXbar_auto_out_e_ready),
    .auto_out_e_valid(tlMasterXbar_auto_out_e_valid),
    .auto_out_e_bits_sink(tlMasterXbar_auto_out_e_bits_sink)
  );
  RHEA__DCache dcache ( // @[HellaCache.scala 291:43]
    .rf_reset(dcache_rf_reset),
    .clock(dcache_clock),
    .reset(dcache_reset),
    .auto_out_a_ready(dcache_auto_out_a_ready),
    .auto_out_a_valid(dcache_auto_out_a_valid),
    .auto_out_a_bits_opcode(dcache_auto_out_a_bits_opcode),
    .auto_out_a_bits_param(dcache_auto_out_a_bits_param),
    .auto_out_a_bits_size(dcache_auto_out_a_bits_size),
    .auto_out_a_bits_source(dcache_auto_out_a_bits_source),
    .auto_out_a_bits_address(dcache_auto_out_a_bits_address),
    .auto_out_a_bits_user_amba_prot_bufferable(dcache_auto_out_a_bits_user_amba_prot_bufferable),
    .auto_out_a_bits_user_amba_prot_modifiable(dcache_auto_out_a_bits_user_amba_prot_modifiable),
    .auto_out_a_bits_user_amba_prot_readalloc(dcache_auto_out_a_bits_user_amba_prot_readalloc),
    .auto_out_a_bits_user_amba_prot_writealloc(dcache_auto_out_a_bits_user_amba_prot_writealloc),
    .auto_out_a_bits_user_amba_prot_privileged(dcache_auto_out_a_bits_user_amba_prot_privileged),
    .auto_out_a_bits_user_amba_prot_secure(dcache_auto_out_a_bits_user_amba_prot_secure),
    .auto_out_a_bits_user_amba_prot_fetch(dcache_auto_out_a_bits_user_amba_prot_fetch),
    .auto_out_a_bits_mask(dcache_auto_out_a_bits_mask),
    .auto_out_a_bits_data(dcache_auto_out_a_bits_data),
    .auto_out_a_bits_corrupt(dcache_auto_out_a_bits_corrupt),
    .auto_out_b_ready(dcache_auto_out_b_ready),
    .auto_out_b_valid(dcache_auto_out_b_valid),
    .auto_out_b_bits_opcode(dcache_auto_out_b_bits_opcode),
    .auto_out_b_bits_param(dcache_auto_out_b_bits_param),
    .auto_out_b_bits_size(dcache_auto_out_b_bits_size),
    .auto_out_b_bits_source(dcache_auto_out_b_bits_source),
    .auto_out_b_bits_address(dcache_auto_out_b_bits_address),
    .auto_out_b_bits_mask(dcache_auto_out_b_bits_mask),
    .auto_out_b_bits_data(dcache_auto_out_b_bits_data),
    .auto_out_b_bits_corrupt(dcache_auto_out_b_bits_corrupt),
    .auto_out_c_ready(dcache_auto_out_c_ready),
    .auto_out_c_valid(dcache_auto_out_c_valid),
    .auto_out_c_bits_opcode(dcache_auto_out_c_bits_opcode),
    .auto_out_c_bits_param(dcache_auto_out_c_bits_param),
    .auto_out_c_bits_size(dcache_auto_out_c_bits_size),
    .auto_out_c_bits_source(dcache_auto_out_c_bits_source),
    .auto_out_c_bits_address(dcache_auto_out_c_bits_address),
    .auto_out_c_bits_data(dcache_auto_out_c_bits_data),
    .auto_out_c_bits_corrupt(dcache_auto_out_c_bits_corrupt),
    .auto_out_d_ready(dcache_auto_out_d_ready),
    .auto_out_d_valid(dcache_auto_out_d_valid),
    .auto_out_d_bits_opcode(dcache_auto_out_d_bits_opcode),
    .auto_out_d_bits_param(dcache_auto_out_d_bits_param),
    .auto_out_d_bits_size(dcache_auto_out_d_bits_size),
    .auto_out_d_bits_source(dcache_auto_out_d_bits_source),
    .auto_out_d_bits_sink(dcache_auto_out_d_bits_sink),
    .auto_out_d_bits_denied(dcache_auto_out_d_bits_denied),
    .auto_out_d_bits_data(dcache_auto_out_d_bits_data),
    .auto_out_d_bits_corrupt(dcache_auto_out_d_bits_corrupt),
    .auto_out_e_ready(dcache_auto_out_e_ready),
    .auto_out_e_valid(dcache_auto_out_e_valid),
    .auto_out_e_bits_sink(dcache_auto_out_e_bits_sink),
    .io_cpu_req_ready(dcache_io_cpu_req_ready),
    .io_cpu_req_valid(dcache_io_cpu_req_valid),
    .io_cpu_req_bits_addr(dcache_io_cpu_req_bits_addr),
    .io_cpu_req_bits_idx(dcache_io_cpu_req_bits_idx),
    .io_cpu_req_bits_tag(dcache_io_cpu_req_bits_tag),
    .io_cpu_req_bits_cmd(dcache_io_cpu_req_bits_cmd),
    .io_cpu_req_bits_size(dcache_io_cpu_req_bits_size),
    .io_cpu_req_bits_signed(dcache_io_cpu_req_bits_signed),
    .io_cpu_req_bits_dprv(dcache_io_cpu_req_bits_dprv),
    .io_cpu_req_bits_phys(dcache_io_cpu_req_bits_phys),
    .io_cpu_req_bits_no_alloc(dcache_io_cpu_req_bits_no_alloc),
    .io_cpu_req_bits_no_xcpt(dcache_io_cpu_req_bits_no_xcpt),
    .io_cpu_req_bits_mask(dcache_io_cpu_req_bits_mask),
    .io_cpu_s1_kill(dcache_io_cpu_s1_kill),
    .io_cpu_s1_data_data(dcache_io_cpu_s1_data_data),
    .io_cpu_s1_data_mask(dcache_io_cpu_s1_data_mask),
    .io_cpu_s2_nack(dcache_io_cpu_s2_nack),
    .io_cpu_resp_valid(dcache_io_cpu_resp_valid),
    .io_cpu_resp_bits_addr(dcache_io_cpu_resp_bits_addr),
    .io_cpu_resp_bits_idx(dcache_io_cpu_resp_bits_idx),
    .io_cpu_resp_bits_tag(dcache_io_cpu_resp_bits_tag),
    .io_cpu_resp_bits_cmd(dcache_io_cpu_resp_bits_cmd),
    .io_cpu_resp_bits_size(dcache_io_cpu_resp_bits_size),
    .io_cpu_resp_bits_signed(dcache_io_cpu_resp_bits_signed),
    .io_cpu_resp_bits_dprv(dcache_io_cpu_resp_bits_dprv),
    .io_cpu_resp_bits_data(dcache_io_cpu_resp_bits_data),
    .io_cpu_resp_bits_mask(dcache_io_cpu_resp_bits_mask),
    .io_cpu_resp_bits_replay(dcache_io_cpu_resp_bits_replay),
    .io_cpu_resp_bits_has_data(dcache_io_cpu_resp_bits_has_data),
    .io_cpu_resp_bits_data_word_bypass(dcache_io_cpu_resp_bits_data_word_bypass),
    .io_cpu_resp_bits_data_raw(dcache_io_cpu_resp_bits_data_raw),
    .io_cpu_resp_bits_store_data(dcache_io_cpu_resp_bits_store_data),
    .io_cpu_replay_next(dcache_io_cpu_replay_next),
    .io_cpu_s2_xcpt_ma_ld(dcache_io_cpu_s2_xcpt_ma_ld),
    .io_cpu_s2_xcpt_ma_st(dcache_io_cpu_s2_xcpt_ma_st),
    .io_cpu_s2_xcpt_pf_ld(dcache_io_cpu_s2_xcpt_pf_ld),
    .io_cpu_s2_xcpt_pf_st(dcache_io_cpu_s2_xcpt_pf_st),
    .io_cpu_s2_xcpt_ae_ld(dcache_io_cpu_s2_xcpt_ae_ld),
    .io_cpu_s2_xcpt_ae_st(dcache_io_cpu_s2_xcpt_ae_st),
    .io_cpu_ordered(dcache_io_cpu_ordered),
    .io_cpu_perf_acquire(dcache_io_cpu_perf_acquire),
    .io_cpu_perf_release(dcache_io_cpu_perf_release),
    .io_cpu_perf_grant(dcache_io_cpu_perf_grant),
    .io_cpu_perf_tlbMiss(dcache_io_cpu_perf_tlbMiss),
    .io_cpu_perf_blocked(dcache_io_cpu_perf_blocked),
    .io_cpu_keep_clock_enabled(dcache_io_cpu_keep_clock_enabled),
    .io_cpu_clock_enabled(dcache_io_cpu_clock_enabled),
    .io_ptw_req_ready(dcache_io_ptw_req_ready),
    .io_ptw_req_valid(dcache_io_ptw_req_valid),
    .io_ptw_req_bits_bits_addr(dcache_io_ptw_req_bits_bits_addr),
    .io_ptw_resp_valid(dcache_io_ptw_resp_valid),
    .io_ptw_resp_bits_ae(dcache_io_ptw_resp_bits_ae),
    .io_ptw_resp_bits_pte_ppn(dcache_io_ptw_resp_bits_pte_ppn),
    .io_ptw_resp_bits_pte_d(dcache_io_ptw_resp_bits_pte_d),
    .io_ptw_resp_bits_pte_a(dcache_io_ptw_resp_bits_pte_a),
    .io_ptw_resp_bits_pte_g(dcache_io_ptw_resp_bits_pte_g),
    .io_ptw_resp_bits_pte_u(dcache_io_ptw_resp_bits_pte_u),
    .io_ptw_resp_bits_pte_x(dcache_io_ptw_resp_bits_pte_x),
    .io_ptw_resp_bits_pte_w(dcache_io_ptw_resp_bits_pte_w),
    .io_ptw_resp_bits_pte_r(dcache_io_ptw_resp_bits_pte_r),
    .io_ptw_resp_bits_pte_v(dcache_io_ptw_resp_bits_pte_v),
    .io_ptw_resp_bits_level(dcache_io_ptw_resp_bits_level),
    .io_ptw_resp_bits_homogeneous(dcache_io_ptw_resp_bits_homogeneous),
    .io_ptw_ptbr_mode(dcache_io_ptw_ptbr_mode),
    .io_ptw_status_debug(dcache_io_ptw_status_debug),
    .io_ptw_status_dprv(dcache_io_ptw_status_dprv),
    .io_ptw_status_mxr(dcache_io_ptw_status_mxr),
    .io_ptw_status_sum(dcache_io_ptw_status_sum),
    .io_ptw_pmp_0_cfg_l(dcache_io_ptw_pmp_0_cfg_l),
    .io_ptw_pmp_0_cfg_a(dcache_io_ptw_pmp_0_cfg_a),
    .io_ptw_pmp_0_cfg_x(dcache_io_ptw_pmp_0_cfg_x),
    .io_ptw_pmp_0_cfg_w(dcache_io_ptw_pmp_0_cfg_w),
    .io_ptw_pmp_0_cfg_r(dcache_io_ptw_pmp_0_cfg_r),
    .io_ptw_pmp_0_addr(dcache_io_ptw_pmp_0_addr),
    .io_ptw_pmp_0_mask(dcache_io_ptw_pmp_0_mask),
    .io_ptw_pmp_1_cfg_l(dcache_io_ptw_pmp_1_cfg_l),
    .io_ptw_pmp_1_cfg_a(dcache_io_ptw_pmp_1_cfg_a),
    .io_ptw_pmp_1_cfg_x(dcache_io_ptw_pmp_1_cfg_x),
    .io_ptw_pmp_1_cfg_w(dcache_io_ptw_pmp_1_cfg_w),
    .io_ptw_pmp_1_cfg_r(dcache_io_ptw_pmp_1_cfg_r),
    .io_ptw_pmp_1_addr(dcache_io_ptw_pmp_1_addr),
    .io_ptw_pmp_1_mask(dcache_io_ptw_pmp_1_mask),
    .io_ptw_pmp_2_cfg_l(dcache_io_ptw_pmp_2_cfg_l),
    .io_ptw_pmp_2_cfg_a(dcache_io_ptw_pmp_2_cfg_a),
    .io_ptw_pmp_2_cfg_x(dcache_io_ptw_pmp_2_cfg_x),
    .io_ptw_pmp_2_cfg_w(dcache_io_ptw_pmp_2_cfg_w),
    .io_ptw_pmp_2_cfg_r(dcache_io_ptw_pmp_2_cfg_r),
    .io_ptw_pmp_2_addr(dcache_io_ptw_pmp_2_addr),
    .io_ptw_pmp_2_mask(dcache_io_ptw_pmp_2_mask),
    .io_ptw_pmp_3_cfg_l(dcache_io_ptw_pmp_3_cfg_l),
    .io_ptw_pmp_3_cfg_a(dcache_io_ptw_pmp_3_cfg_a),
    .io_ptw_pmp_3_cfg_x(dcache_io_ptw_pmp_3_cfg_x),
    .io_ptw_pmp_3_cfg_w(dcache_io_ptw_pmp_3_cfg_w),
    .io_ptw_pmp_3_cfg_r(dcache_io_ptw_pmp_3_cfg_r),
    .io_ptw_pmp_3_addr(dcache_io_ptw_pmp_3_addr),
    .io_ptw_pmp_3_mask(dcache_io_ptw_pmp_3_mask),
    .io_ptw_pmp_4_cfg_l(dcache_io_ptw_pmp_4_cfg_l),
    .io_ptw_pmp_4_cfg_a(dcache_io_ptw_pmp_4_cfg_a),
    .io_ptw_pmp_4_cfg_x(dcache_io_ptw_pmp_4_cfg_x),
    .io_ptw_pmp_4_cfg_w(dcache_io_ptw_pmp_4_cfg_w),
    .io_ptw_pmp_4_cfg_r(dcache_io_ptw_pmp_4_cfg_r),
    .io_ptw_pmp_4_addr(dcache_io_ptw_pmp_4_addr),
    .io_ptw_pmp_4_mask(dcache_io_ptw_pmp_4_mask),
    .io_ptw_pmp_5_cfg_l(dcache_io_ptw_pmp_5_cfg_l),
    .io_ptw_pmp_5_cfg_a(dcache_io_ptw_pmp_5_cfg_a),
    .io_ptw_pmp_5_cfg_x(dcache_io_ptw_pmp_5_cfg_x),
    .io_ptw_pmp_5_cfg_w(dcache_io_ptw_pmp_5_cfg_w),
    .io_ptw_pmp_5_cfg_r(dcache_io_ptw_pmp_5_cfg_r),
    .io_ptw_pmp_5_addr(dcache_io_ptw_pmp_5_addr),
    .io_ptw_pmp_5_mask(dcache_io_ptw_pmp_5_mask),
    .io_ptw_pmp_6_cfg_l(dcache_io_ptw_pmp_6_cfg_l),
    .io_ptw_pmp_6_cfg_a(dcache_io_ptw_pmp_6_cfg_a),
    .io_ptw_pmp_6_cfg_x(dcache_io_ptw_pmp_6_cfg_x),
    .io_ptw_pmp_6_cfg_w(dcache_io_ptw_pmp_6_cfg_w),
    .io_ptw_pmp_6_cfg_r(dcache_io_ptw_pmp_6_cfg_r),
    .io_ptw_pmp_6_addr(dcache_io_ptw_pmp_6_addr),
    .io_ptw_pmp_6_mask(dcache_io_ptw_pmp_6_mask),
    .io_ptw_pmp_7_cfg_l(dcache_io_ptw_pmp_7_cfg_l),
    .io_ptw_pmp_7_cfg_a(dcache_io_ptw_pmp_7_cfg_a),
    .io_ptw_pmp_7_cfg_x(dcache_io_ptw_pmp_7_cfg_x),
    .io_ptw_pmp_7_cfg_w(dcache_io_ptw_pmp_7_cfg_w),
    .io_ptw_pmp_7_cfg_r(dcache_io_ptw_pmp_7_cfg_r),
    .io_ptw_pmp_7_addr(dcache_io_ptw_pmp_7_addr),
    .io_ptw_pmp_7_mask(dcache_io_ptw_pmp_7_mask),
    .io_ptw_customCSRs_csrs_1_value(dcache_io_ptw_customCSRs_csrs_1_value),
    .io_errors_bus_valid(dcache_io_errors_bus_valid),
    .psd_test_clock_enable(dcache_psd_test_clock_enable)
  );
  RHEA__Frontend frontend ( // @[Frontend.scala 353:28]
    .rf_reset(frontend_rf_reset),
    .clock(frontend_clock),
    .reset(frontend_reset),
    .auto_icache_master_out_a_ready(frontend_auto_icache_master_out_a_ready),
    .auto_icache_master_out_a_valid(frontend_auto_icache_master_out_a_valid),
    .auto_icache_master_out_a_bits_opcode(frontend_auto_icache_master_out_a_bits_opcode),
    .auto_icache_master_out_a_bits_source(frontend_auto_icache_master_out_a_bits_source),
    .auto_icache_master_out_a_bits_address(frontend_auto_icache_master_out_a_bits_address),
    .auto_icache_master_out_d_valid(frontend_auto_icache_master_out_d_valid),
    .auto_icache_master_out_d_bits_opcode(frontend_auto_icache_master_out_d_bits_opcode),
    .auto_icache_master_out_d_bits_size(frontend_auto_icache_master_out_d_bits_size),
    .auto_icache_master_out_d_bits_data(frontend_auto_icache_master_out_d_bits_data),
    .auto_icache_master_out_d_bits_corrupt(frontend_auto_icache_master_out_d_bits_corrupt),
    .auto_reset_vector_sink_in(frontend_auto_reset_vector_sink_in),
    .io_cpu_might_request(frontend_io_cpu_might_request),
    .io_cpu_clock_enabled(frontend_io_cpu_clock_enabled),
    .io_cpu_req_valid(frontend_io_cpu_req_valid),
    .io_cpu_req_bits_pc(frontend_io_cpu_req_bits_pc),
    .io_cpu_req_bits_speculative(frontend_io_cpu_req_bits_speculative),
    .io_cpu_sfence_valid(frontend_io_cpu_sfence_valid),
    .io_cpu_sfence_bits_rs1(frontend_io_cpu_sfence_bits_rs1),
    .io_cpu_sfence_bits_rs2(frontend_io_cpu_sfence_bits_rs2),
    .io_cpu_sfence_bits_addr(frontend_io_cpu_sfence_bits_addr),
    .io_cpu_sfence_bits_asid(frontend_io_cpu_sfence_bits_asid),
    .io_cpu_resp_ready(frontend_io_cpu_resp_ready),
    .io_cpu_resp_valid(frontend_io_cpu_resp_valid),
    .io_cpu_resp_bits_btb_taken(frontend_io_cpu_resp_bits_btb_taken),
    .io_cpu_resp_bits_btb_bridx(frontend_io_cpu_resp_bits_btb_bridx),
    .io_cpu_resp_bits_btb_entry(frontend_io_cpu_resp_bits_btb_entry),
    .io_cpu_resp_bits_btb_bht_history(frontend_io_cpu_resp_bits_btb_bht_history),
    .io_cpu_resp_bits_pc(frontend_io_cpu_resp_bits_pc),
    .io_cpu_resp_bits_data(frontend_io_cpu_resp_bits_data),
    .io_cpu_resp_bits_mask(frontend_io_cpu_resp_bits_mask),
    .io_cpu_resp_bits_xcpt_pf_inst(frontend_io_cpu_resp_bits_xcpt_pf_inst),
    .io_cpu_resp_bits_xcpt_ae_inst(frontend_io_cpu_resp_bits_xcpt_ae_inst),
    .io_cpu_resp_bits_replay(frontend_io_cpu_resp_bits_replay),
    .io_cpu_btb_update_valid(frontend_io_cpu_btb_update_valid),
    .io_cpu_btb_update_bits_prediction_entry(frontend_io_cpu_btb_update_bits_prediction_entry),
    .io_cpu_btb_update_bits_pc(frontend_io_cpu_btb_update_bits_pc),
    .io_cpu_btb_update_bits_isValid(frontend_io_cpu_btb_update_bits_isValid),
    .io_cpu_btb_update_bits_br_pc(frontend_io_cpu_btb_update_bits_br_pc),
    .io_cpu_btb_update_bits_cfiType(frontend_io_cpu_btb_update_bits_cfiType),
    .io_cpu_bht_update_valid(frontend_io_cpu_bht_update_valid),
    .io_cpu_bht_update_bits_prediction_history(frontend_io_cpu_bht_update_bits_prediction_history),
    .io_cpu_bht_update_bits_pc(frontend_io_cpu_bht_update_bits_pc),
    .io_cpu_bht_update_bits_branch(frontend_io_cpu_bht_update_bits_branch),
    .io_cpu_bht_update_bits_taken(frontend_io_cpu_bht_update_bits_taken),
    .io_cpu_bht_update_bits_mispredict(frontend_io_cpu_bht_update_bits_mispredict),
    .io_cpu_flush_icache(frontend_io_cpu_flush_icache),
    .io_cpu_npc(frontend_io_cpu_npc),
    .io_cpu_perf_acquire(frontend_io_cpu_perf_acquire),
    .io_cpu_perf_tlbMiss(frontend_io_cpu_perf_tlbMiss),
    .io_ptw_req_ready(frontend_io_ptw_req_ready),
    .io_ptw_req_valid(frontend_io_ptw_req_valid),
    .io_ptw_req_bits_valid(frontend_io_ptw_req_bits_valid),
    .io_ptw_req_bits_bits_addr(frontend_io_ptw_req_bits_bits_addr),
    .io_ptw_resp_valid(frontend_io_ptw_resp_valid),
    .io_ptw_resp_bits_ae(frontend_io_ptw_resp_bits_ae),
    .io_ptw_resp_bits_pte_ppn(frontend_io_ptw_resp_bits_pte_ppn),
    .io_ptw_resp_bits_pte_d(frontend_io_ptw_resp_bits_pte_d),
    .io_ptw_resp_bits_pte_a(frontend_io_ptw_resp_bits_pte_a),
    .io_ptw_resp_bits_pte_g(frontend_io_ptw_resp_bits_pte_g),
    .io_ptw_resp_bits_pte_u(frontend_io_ptw_resp_bits_pte_u),
    .io_ptw_resp_bits_pte_x(frontend_io_ptw_resp_bits_pte_x),
    .io_ptw_resp_bits_pte_w(frontend_io_ptw_resp_bits_pte_w),
    .io_ptw_resp_bits_pte_r(frontend_io_ptw_resp_bits_pte_r),
    .io_ptw_resp_bits_pte_v(frontend_io_ptw_resp_bits_pte_v),
    .io_ptw_resp_bits_level(frontend_io_ptw_resp_bits_level),
    .io_ptw_resp_bits_homogeneous(frontend_io_ptw_resp_bits_homogeneous),
    .io_ptw_ptbr_mode(frontend_io_ptw_ptbr_mode),
    .io_ptw_status_debug(frontend_io_ptw_status_debug),
    .io_ptw_status_prv(frontend_io_ptw_status_prv),
    .io_ptw_pmp_0_cfg_l(frontend_io_ptw_pmp_0_cfg_l),
    .io_ptw_pmp_0_cfg_a(frontend_io_ptw_pmp_0_cfg_a),
    .io_ptw_pmp_0_cfg_x(frontend_io_ptw_pmp_0_cfg_x),
    .io_ptw_pmp_0_cfg_w(frontend_io_ptw_pmp_0_cfg_w),
    .io_ptw_pmp_0_cfg_r(frontend_io_ptw_pmp_0_cfg_r),
    .io_ptw_pmp_0_addr(frontend_io_ptw_pmp_0_addr),
    .io_ptw_pmp_0_mask(frontend_io_ptw_pmp_0_mask),
    .io_ptw_pmp_1_cfg_l(frontend_io_ptw_pmp_1_cfg_l),
    .io_ptw_pmp_1_cfg_a(frontend_io_ptw_pmp_1_cfg_a),
    .io_ptw_pmp_1_cfg_x(frontend_io_ptw_pmp_1_cfg_x),
    .io_ptw_pmp_1_cfg_w(frontend_io_ptw_pmp_1_cfg_w),
    .io_ptw_pmp_1_cfg_r(frontend_io_ptw_pmp_1_cfg_r),
    .io_ptw_pmp_1_addr(frontend_io_ptw_pmp_1_addr),
    .io_ptw_pmp_1_mask(frontend_io_ptw_pmp_1_mask),
    .io_ptw_pmp_2_cfg_l(frontend_io_ptw_pmp_2_cfg_l),
    .io_ptw_pmp_2_cfg_a(frontend_io_ptw_pmp_2_cfg_a),
    .io_ptw_pmp_2_cfg_x(frontend_io_ptw_pmp_2_cfg_x),
    .io_ptw_pmp_2_cfg_w(frontend_io_ptw_pmp_2_cfg_w),
    .io_ptw_pmp_2_cfg_r(frontend_io_ptw_pmp_2_cfg_r),
    .io_ptw_pmp_2_addr(frontend_io_ptw_pmp_2_addr),
    .io_ptw_pmp_2_mask(frontend_io_ptw_pmp_2_mask),
    .io_ptw_pmp_3_cfg_l(frontend_io_ptw_pmp_3_cfg_l),
    .io_ptw_pmp_3_cfg_a(frontend_io_ptw_pmp_3_cfg_a),
    .io_ptw_pmp_3_cfg_x(frontend_io_ptw_pmp_3_cfg_x),
    .io_ptw_pmp_3_cfg_w(frontend_io_ptw_pmp_3_cfg_w),
    .io_ptw_pmp_3_cfg_r(frontend_io_ptw_pmp_3_cfg_r),
    .io_ptw_pmp_3_addr(frontend_io_ptw_pmp_3_addr),
    .io_ptw_pmp_3_mask(frontend_io_ptw_pmp_3_mask),
    .io_ptw_pmp_4_cfg_l(frontend_io_ptw_pmp_4_cfg_l),
    .io_ptw_pmp_4_cfg_a(frontend_io_ptw_pmp_4_cfg_a),
    .io_ptw_pmp_4_cfg_x(frontend_io_ptw_pmp_4_cfg_x),
    .io_ptw_pmp_4_cfg_w(frontend_io_ptw_pmp_4_cfg_w),
    .io_ptw_pmp_4_cfg_r(frontend_io_ptw_pmp_4_cfg_r),
    .io_ptw_pmp_4_addr(frontend_io_ptw_pmp_4_addr),
    .io_ptw_pmp_4_mask(frontend_io_ptw_pmp_4_mask),
    .io_ptw_pmp_5_cfg_l(frontend_io_ptw_pmp_5_cfg_l),
    .io_ptw_pmp_5_cfg_a(frontend_io_ptw_pmp_5_cfg_a),
    .io_ptw_pmp_5_cfg_x(frontend_io_ptw_pmp_5_cfg_x),
    .io_ptw_pmp_5_cfg_w(frontend_io_ptw_pmp_5_cfg_w),
    .io_ptw_pmp_5_cfg_r(frontend_io_ptw_pmp_5_cfg_r),
    .io_ptw_pmp_5_addr(frontend_io_ptw_pmp_5_addr),
    .io_ptw_pmp_5_mask(frontend_io_ptw_pmp_5_mask),
    .io_ptw_pmp_6_cfg_l(frontend_io_ptw_pmp_6_cfg_l),
    .io_ptw_pmp_6_cfg_a(frontend_io_ptw_pmp_6_cfg_a),
    .io_ptw_pmp_6_cfg_x(frontend_io_ptw_pmp_6_cfg_x),
    .io_ptw_pmp_6_cfg_w(frontend_io_ptw_pmp_6_cfg_w),
    .io_ptw_pmp_6_cfg_r(frontend_io_ptw_pmp_6_cfg_r),
    .io_ptw_pmp_6_addr(frontend_io_ptw_pmp_6_addr),
    .io_ptw_pmp_6_mask(frontend_io_ptw_pmp_6_mask),
    .io_ptw_pmp_7_cfg_l(frontend_io_ptw_pmp_7_cfg_l),
    .io_ptw_pmp_7_cfg_a(frontend_io_ptw_pmp_7_cfg_a),
    .io_ptw_pmp_7_cfg_x(frontend_io_ptw_pmp_7_cfg_x),
    .io_ptw_pmp_7_cfg_w(frontend_io_ptw_pmp_7_cfg_w),
    .io_ptw_pmp_7_cfg_r(frontend_io_ptw_pmp_7_cfg_r),
    .io_ptw_pmp_7_addr(frontend_io_ptw_pmp_7_addr),
    .io_ptw_pmp_7_mask(frontend_io_ptw_pmp_7_mask),
    .io_ptw_customCSRs_csrs_0_wen(frontend_io_ptw_customCSRs_csrs_0_wen),
    .io_ptw_customCSRs_csrs_0_value(frontend_io_ptw_customCSRs_csrs_0_value),
    .io_ptw_customCSRs_csrs_1_value(frontend_io_ptw_customCSRs_csrs_1_value),
    .psd_test_clock_enable(frontend_psd_test_clock_enable)
  );
  RHEA__FPU fpuOpt ( // @[RocketTile.scala 224:62]
    .rf_reset(fpuOpt_rf_reset),
    .clock(fpuOpt_clock),
    .reset(fpuOpt_reset),
    .io_inst(fpuOpt_io_inst),
    .io_fromint_data(fpuOpt_io_fromint_data),
    .io_fcsr_rm(fpuOpt_io_fcsr_rm),
    .io_fcsr_flags_valid(fpuOpt_io_fcsr_flags_valid),
    .io_fcsr_flags_bits(fpuOpt_io_fcsr_flags_bits),
    .io_store_data(fpuOpt_io_store_data),
    .io_toint_data(fpuOpt_io_toint_data),
    .io_dmem_resp_val(fpuOpt_io_dmem_resp_val),
    .io_dmem_resp_type(fpuOpt_io_dmem_resp_type),
    .io_dmem_resp_tag(fpuOpt_io_dmem_resp_tag),
    .io_dmem_resp_data(fpuOpt_io_dmem_resp_data),
    .io_valid(fpuOpt_io_valid),
    .io_fcsr_rdy(fpuOpt_io_fcsr_rdy),
    .io_nack_mem(fpuOpt_io_nack_mem),
    .io_illegal_rm(fpuOpt_io_illegal_rm),
    .io_killx(fpuOpt_io_killx),
    .io_killm(fpuOpt_io_killm),
    .io_dec_ldst(fpuOpt_io_dec_ldst),
    .io_dec_wen(fpuOpt_io_dec_wen),
    .io_dec_ren1(fpuOpt_io_dec_ren1),
    .io_dec_ren2(fpuOpt_io_dec_ren2),
    .io_dec_ren3(fpuOpt_io_dec_ren3),
    .io_dec_swap23(fpuOpt_io_dec_swap23),
    .io_dec_fma(fpuOpt_io_dec_fma),
    .io_dec_div(fpuOpt_io_dec_div),
    .io_dec_sqrt(fpuOpt_io_dec_sqrt),
    .io_sboard_set(fpuOpt_io_sboard_set),
    .io_sboard_clr(fpuOpt_io_sboard_clr),
    .io_sboard_clra(fpuOpt_io_sboard_clra),
    .io_keep_clock_enabled(fpuOpt_io_keep_clock_enabled),
    .psd_test_clock_enable(fpuOpt_psd_test_clock_enable)
  );
  RHEA__HellaCacheArbiter dcacheArb ( // @[HellaCache.scala 302:25]
    .rf_reset(dcacheArb_rf_reset),
    .clock(dcacheArb_clock),
    .io_requestor_0_req_ready(dcacheArb_io_requestor_0_req_ready),
    .io_requestor_0_req_valid(dcacheArb_io_requestor_0_req_valid),
    .io_requestor_0_req_bits_addr(dcacheArb_io_requestor_0_req_bits_addr),
    .io_requestor_0_req_bits_idx(dcacheArb_io_requestor_0_req_bits_idx),
    .io_requestor_0_s1_kill(dcacheArb_io_requestor_0_s1_kill),
    .io_requestor_0_s2_nack(dcacheArb_io_requestor_0_s2_nack),
    .io_requestor_0_resp_valid(dcacheArb_io_requestor_0_resp_valid),
    .io_requestor_0_resp_bits_data(dcacheArb_io_requestor_0_resp_bits_data),
    .io_requestor_0_s2_xcpt_ae_ld(dcacheArb_io_requestor_0_s2_xcpt_ae_ld),
    .io_requestor_1_req_ready(dcacheArb_io_requestor_1_req_ready),
    .io_requestor_1_req_valid(dcacheArb_io_requestor_1_req_valid),
    .io_requestor_1_req_bits_addr(dcacheArb_io_requestor_1_req_bits_addr),
    .io_requestor_1_req_bits_idx(dcacheArb_io_requestor_1_req_bits_idx),
    .io_requestor_1_req_bits_tag(dcacheArb_io_requestor_1_req_bits_tag),
    .io_requestor_1_req_bits_cmd(dcacheArb_io_requestor_1_req_bits_cmd),
    .io_requestor_1_req_bits_size(dcacheArb_io_requestor_1_req_bits_size),
    .io_requestor_1_req_bits_signed(dcacheArb_io_requestor_1_req_bits_signed),
    .io_requestor_1_req_bits_dprv(dcacheArb_io_requestor_1_req_bits_dprv),
    .io_requestor_1_req_bits_phys(dcacheArb_io_requestor_1_req_bits_phys),
    .io_requestor_1_req_bits_no_alloc(dcacheArb_io_requestor_1_req_bits_no_alloc),
    .io_requestor_1_req_bits_no_xcpt(dcacheArb_io_requestor_1_req_bits_no_xcpt),
    .io_requestor_1_req_bits_mask(dcacheArb_io_requestor_1_req_bits_mask),
    .io_requestor_1_s1_kill(dcacheArb_io_requestor_1_s1_kill),
    .io_requestor_1_s1_data_data(dcacheArb_io_requestor_1_s1_data_data),
    .io_requestor_1_s1_data_mask(dcacheArb_io_requestor_1_s1_data_mask),
    .io_requestor_1_s2_nack(dcacheArb_io_requestor_1_s2_nack),
    .io_requestor_1_resp_valid(dcacheArb_io_requestor_1_resp_valid),
    .io_requestor_1_resp_bits_addr(dcacheArb_io_requestor_1_resp_bits_addr),
    .io_requestor_1_resp_bits_tag(dcacheArb_io_requestor_1_resp_bits_tag),
    .io_requestor_1_resp_bits_cmd(dcacheArb_io_requestor_1_resp_bits_cmd),
    .io_requestor_1_resp_bits_size(dcacheArb_io_requestor_1_resp_bits_size),
    .io_requestor_1_resp_bits_signed(dcacheArb_io_requestor_1_resp_bits_signed),
    .io_requestor_1_resp_bits_data(dcacheArb_io_requestor_1_resp_bits_data),
    .io_requestor_1_resp_bits_replay(dcacheArb_io_requestor_1_resp_bits_replay),
    .io_requestor_1_resp_bits_has_data(dcacheArb_io_requestor_1_resp_bits_has_data),
    .io_requestor_1_resp_bits_data_word_bypass(dcacheArb_io_requestor_1_resp_bits_data_word_bypass),
    .io_requestor_1_replay_next(dcacheArb_io_requestor_1_replay_next),
    .io_requestor_1_s2_xcpt_ma_ld(dcacheArb_io_requestor_1_s2_xcpt_ma_ld),
    .io_requestor_1_s2_xcpt_ma_st(dcacheArb_io_requestor_1_s2_xcpt_ma_st),
    .io_requestor_1_s2_xcpt_pf_ld(dcacheArb_io_requestor_1_s2_xcpt_pf_ld),
    .io_requestor_1_s2_xcpt_pf_st(dcacheArb_io_requestor_1_s2_xcpt_pf_st),
    .io_requestor_1_s2_xcpt_ae_ld(dcacheArb_io_requestor_1_s2_xcpt_ae_ld),
    .io_requestor_1_s2_xcpt_ae_st(dcacheArb_io_requestor_1_s2_xcpt_ae_st),
    .io_requestor_1_ordered(dcacheArb_io_requestor_1_ordered),
    .io_requestor_1_perf_acquire(dcacheArb_io_requestor_1_perf_acquire),
    .io_requestor_1_perf_release(dcacheArb_io_requestor_1_perf_release),
    .io_requestor_1_perf_grant(dcacheArb_io_requestor_1_perf_grant),
    .io_requestor_1_perf_tlbMiss(dcacheArb_io_requestor_1_perf_tlbMiss),
    .io_requestor_1_perf_blocked(dcacheArb_io_requestor_1_perf_blocked),
    .io_requestor_1_keep_clock_enabled(dcacheArb_io_requestor_1_keep_clock_enabled),
    .io_requestor_1_clock_enabled(dcacheArb_io_requestor_1_clock_enabled),
    .io_mem_req_ready(dcacheArb_io_mem_req_ready),
    .io_mem_req_valid(dcacheArb_io_mem_req_valid),
    .io_mem_req_bits_addr(dcacheArb_io_mem_req_bits_addr),
    .io_mem_req_bits_idx(dcacheArb_io_mem_req_bits_idx),
    .io_mem_req_bits_tag(dcacheArb_io_mem_req_bits_tag),
    .io_mem_req_bits_cmd(dcacheArb_io_mem_req_bits_cmd),
    .io_mem_req_bits_size(dcacheArb_io_mem_req_bits_size),
    .io_mem_req_bits_signed(dcacheArb_io_mem_req_bits_signed),
    .io_mem_req_bits_dprv(dcacheArb_io_mem_req_bits_dprv),
    .io_mem_req_bits_phys(dcacheArb_io_mem_req_bits_phys),
    .io_mem_req_bits_no_alloc(dcacheArb_io_mem_req_bits_no_alloc),
    .io_mem_req_bits_no_xcpt(dcacheArb_io_mem_req_bits_no_xcpt),
    .io_mem_req_bits_mask(dcacheArb_io_mem_req_bits_mask),
    .io_mem_s1_kill(dcacheArb_io_mem_s1_kill),
    .io_mem_s1_data_data(dcacheArb_io_mem_s1_data_data),
    .io_mem_s1_data_mask(dcacheArb_io_mem_s1_data_mask),
    .io_mem_s2_nack(dcacheArb_io_mem_s2_nack),
    .io_mem_resp_valid(dcacheArb_io_mem_resp_valid),
    .io_mem_resp_bits_addr(dcacheArb_io_mem_resp_bits_addr),
    .io_mem_resp_bits_tag(dcacheArb_io_mem_resp_bits_tag),
    .io_mem_resp_bits_cmd(dcacheArb_io_mem_resp_bits_cmd),
    .io_mem_resp_bits_size(dcacheArb_io_mem_resp_bits_size),
    .io_mem_resp_bits_signed(dcacheArb_io_mem_resp_bits_signed),
    .io_mem_resp_bits_data(dcacheArb_io_mem_resp_bits_data),
    .io_mem_resp_bits_replay(dcacheArb_io_mem_resp_bits_replay),
    .io_mem_resp_bits_has_data(dcacheArb_io_mem_resp_bits_has_data),
    .io_mem_resp_bits_data_word_bypass(dcacheArb_io_mem_resp_bits_data_word_bypass),
    .io_mem_replay_next(dcacheArb_io_mem_replay_next),
    .io_mem_s2_xcpt_ma_ld(dcacheArb_io_mem_s2_xcpt_ma_ld),
    .io_mem_s2_xcpt_ma_st(dcacheArb_io_mem_s2_xcpt_ma_st),
    .io_mem_s2_xcpt_pf_ld(dcacheArb_io_mem_s2_xcpt_pf_ld),
    .io_mem_s2_xcpt_pf_st(dcacheArb_io_mem_s2_xcpt_pf_st),
    .io_mem_s2_xcpt_ae_ld(dcacheArb_io_mem_s2_xcpt_ae_ld),
    .io_mem_s2_xcpt_ae_st(dcacheArb_io_mem_s2_xcpt_ae_st),
    .io_mem_ordered(dcacheArb_io_mem_ordered),
    .io_mem_perf_acquire(dcacheArb_io_mem_perf_acquire),
    .io_mem_perf_release(dcacheArb_io_mem_perf_release),
    .io_mem_perf_grant(dcacheArb_io_mem_perf_grant),
    .io_mem_perf_tlbMiss(dcacheArb_io_mem_perf_tlbMiss),
    .io_mem_perf_blocked(dcacheArb_io_mem_perf_blocked),
    .io_mem_keep_clock_enabled(dcacheArb_io_mem_keep_clock_enabled),
    .io_mem_clock_enabled(dcacheArb_io_mem_clock_enabled)
  );
  RHEA__PTW ptw ( // @[PTW.scala 445:19]
    .rf_reset(ptw_rf_reset),
    .clock(ptw_clock),
    .reset(ptw_reset),
    .io_requestor_0_req_ready(ptw_io_requestor_0_req_ready),
    .io_requestor_0_req_valid(ptw_io_requestor_0_req_valid),
    .io_requestor_0_req_bits_bits_addr(ptw_io_requestor_0_req_bits_bits_addr),
    .io_requestor_0_resp_valid(ptw_io_requestor_0_resp_valid),
    .io_requestor_0_resp_bits_ae(ptw_io_requestor_0_resp_bits_ae),
    .io_requestor_0_resp_bits_pte_ppn(ptw_io_requestor_0_resp_bits_pte_ppn),
    .io_requestor_0_resp_bits_pte_d(ptw_io_requestor_0_resp_bits_pte_d),
    .io_requestor_0_resp_bits_pte_a(ptw_io_requestor_0_resp_bits_pte_a),
    .io_requestor_0_resp_bits_pte_g(ptw_io_requestor_0_resp_bits_pte_g),
    .io_requestor_0_resp_bits_pte_u(ptw_io_requestor_0_resp_bits_pte_u),
    .io_requestor_0_resp_bits_pte_x(ptw_io_requestor_0_resp_bits_pte_x),
    .io_requestor_0_resp_bits_pte_w(ptw_io_requestor_0_resp_bits_pte_w),
    .io_requestor_0_resp_bits_pte_r(ptw_io_requestor_0_resp_bits_pte_r),
    .io_requestor_0_resp_bits_pte_v(ptw_io_requestor_0_resp_bits_pte_v),
    .io_requestor_0_resp_bits_level(ptw_io_requestor_0_resp_bits_level),
    .io_requestor_0_resp_bits_homogeneous(ptw_io_requestor_0_resp_bits_homogeneous),
    .io_requestor_0_ptbr_mode(ptw_io_requestor_0_ptbr_mode),
    .io_requestor_0_status_debug(ptw_io_requestor_0_status_debug),
    .io_requestor_0_status_dprv(ptw_io_requestor_0_status_dprv),
    .io_requestor_0_status_mxr(ptw_io_requestor_0_status_mxr),
    .io_requestor_0_status_sum(ptw_io_requestor_0_status_sum),
    .io_requestor_0_pmp_0_cfg_l(ptw_io_requestor_0_pmp_0_cfg_l),
    .io_requestor_0_pmp_0_cfg_a(ptw_io_requestor_0_pmp_0_cfg_a),
    .io_requestor_0_pmp_0_cfg_x(ptw_io_requestor_0_pmp_0_cfg_x),
    .io_requestor_0_pmp_0_cfg_w(ptw_io_requestor_0_pmp_0_cfg_w),
    .io_requestor_0_pmp_0_cfg_r(ptw_io_requestor_0_pmp_0_cfg_r),
    .io_requestor_0_pmp_0_addr(ptw_io_requestor_0_pmp_0_addr),
    .io_requestor_0_pmp_0_mask(ptw_io_requestor_0_pmp_0_mask),
    .io_requestor_0_pmp_1_cfg_l(ptw_io_requestor_0_pmp_1_cfg_l),
    .io_requestor_0_pmp_1_cfg_a(ptw_io_requestor_0_pmp_1_cfg_a),
    .io_requestor_0_pmp_1_cfg_x(ptw_io_requestor_0_pmp_1_cfg_x),
    .io_requestor_0_pmp_1_cfg_w(ptw_io_requestor_0_pmp_1_cfg_w),
    .io_requestor_0_pmp_1_cfg_r(ptw_io_requestor_0_pmp_1_cfg_r),
    .io_requestor_0_pmp_1_addr(ptw_io_requestor_0_pmp_1_addr),
    .io_requestor_0_pmp_1_mask(ptw_io_requestor_0_pmp_1_mask),
    .io_requestor_0_pmp_2_cfg_l(ptw_io_requestor_0_pmp_2_cfg_l),
    .io_requestor_0_pmp_2_cfg_a(ptw_io_requestor_0_pmp_2_cfg_a),
    .io_requestor_0_pmp_2_cfg_x(ptw_io_requestor_0_pmp_2_cfg_x),
    .io_requestor_0_pmp_2_cfg_w(ptw_io_requestor_0_pmp_2_cfg_w),
    .io_requestor_0_pmp_2_cfg_r(ptw_io_requestor_0_pmp_2_cfg_r),
    .io_requestor_0_pmp_2_addr(ptw_io_requestor_0_pmp_2_addr),
    .io_requestor_0_pmp_2_mask(ptw_io_requestor_0_pmp_2_mask),
    .io_requestor_0_pmp_3_cfg_l(ptw_io_requestor_0_pmp_3_cfg_l),
    .io_requestor_0_pmp_3_cfg_a(ptw_io_requestor_0_pmp_3_cfg_a),
    .io_requestor_0_pmp_3_cfg_x(ptw_io_requestor_0_pmp_3_cfg_x),
    .io_requestor_0_pmp_3_cfg_w(ptw_io_requestor_0_pmp_3_cfg_w),
    .io_requestor_0_pmp_3_cfg_r(ptw_io_requestor_0_pmp_3_cfg_r),
    .io_requestor_0_pmp_3_addr(ptw_io_requestor_0_pmp_3_addr),
    .io_requestor_0_pmp_3_mask(ptw_io_requestor_0_pmp_3_mask),
    .io_requestor_0_pmp_4_cfg_l(ptw_io_requestor_0_pmp_4_cfg_l),
    .io_requestor_0_pmp_4_cfg_a(ptw_io_requestor_0_pmp_4_cfg_a),
    .io_requestor_0_pmp_4_cfg_x(ptw_io_requestor_0_pmp_4_cfg_x),
    .io_requestor_0_pmp_4_cfg_w(ptw_io_requestor_0_pmp_4_cfg_w),
    .io_requestor_0_pmp_4_cfg_r(ptw_io_requestor_0_pmp_4_cfg_r),
    .io_requestor_0_pmp_4_addr(ptw_io_requestor_0_pmp_4_addr),
    .io_requestor_0_pmp_4_mask(ptw_io_requestor_0_pmp_4_mask),
    .io_requestor_0_pmp_5_cfg_l(ptw_io_requestor_0_pmp_5_cfg_l),
    .io_requestor_0_pmp_5_cfg_a(ptw_io_requestor_0_pmp_5_cfg_a),
    .io_requestor_0_pmp_5_cfg_x(ptw_io_requestor_0_pmp_5_cfg_x),
    .io_requestor_0_pmp_5_cfg_w(ptw_io_requestor_0_pmp_5_cfg_w),
    .io_requestor_0_pmp_5_cfg_r(ptw_io_requestor_0_pmp_5_cfg_r),
    .io_requestor_0_pmp_5_addr(ptw_io_requestor_0_pmp_5_addr),
    .io_requestor_0_pmp_5_mask(ptw_io_requestor_0_pmp_5_mask),
    .io_requestor_0_pmp_6_cfg_l(ptw_io_requestor_0_pmp_6_cfg_l),
    .io_requestor_0_pmp_6_cfg_a(ptw_io_requestor_0_pmp_6_cfg_a),
    .io_requestor_0_pmp_6_cfg_x(ptw_io_requestor_0_pmp_6_cfg_x),
    .io_requestor_0_pmp_6_cfg_w(ptw_io_requestor_0_pmp_6_cfg_w),
    .io_requestor_0_pmp_6_cfg_r(ptw_io_requestor_0_pmp_6_cfg_r),
    .io_requestor_0_pmp_6_addr(ptw_io_requestor_0_pmp_6_addr),
    .io_requestor_0_pmp_6_mask(ptw_io_requestor_0_pmp_6_mask),
    .io_requestor_0_pmp_7_cfg_l(ptw_io_requestor_0_pmp_7_cfg_l),
    .io_requestor_0_pmp_7_cfg_a(ptw_io_requestor_0_pmp_7_cfg_a),
    .io_requestor_0_pmp_7_cfg_x(ptw_io_requestor_0_pmp_7_cfg_x),
    .io_requestor_0_pmp_7_cfg_w(ptw_io_requestor_0_pmp_7_cfg_w),
    .io_requestor_0_pmp_7_cfg_r(ptw_io_requestor_0_pmp_7_cfg_r),
    .io_requestor_0_pmp_7_addr(ptw_io_requestor_0_pmp_7_addr),
    .io_requestor_0_pmp_7_mask(ptw_io_requestor_0_pmp_7_mask),
    .io_requestor_0_customCSRs_csrs_1_value(ptw_io_requestor_0_customCSRs_csrs_1_value),
    .io_requestor_1_req_ready(ptw_io_requestor_1_req_ready),
    .io_requestor_1_req_valid(ptw_io_requestor_1_req_valid),
    .io_requestor_1_req_bits_valid(ptw_io_requestor_1_req_bits_valid),
    .io_requestor_1_req_bits_bits_addr(ptw_io_requestor_1_req_bits_bits_addr),
    .io_requestor_1_resp_valid(ptw_io_requestor_1_resp_valid),
    .io_requestor_1_resp_bits_ae(ptw_io_requestor_1_resp_bits_ae),
    .io_requestor_1_resp_bits_pte_ppn(ptw_io_requestor_1_resp_bits_pte_ppn),
    .io_requestor_1_resp_bits_pte_d(ptw_io_requestor_1_resp_bits_pte_d),
    .io_requestor_1_resp_bits_pte_a(ptw_io_requestor_1_resp_bits_pte_a),
    .io_requestor_1_resp_bits_pte_g(ptw_io_requestor_1_resp_bits_pte_g),
    .io_requestor_1_resp_bits_pte_u(ptw_io_requestor_1_resp_bits_pte_u),
    .io_requestor_1_resp_bits_pte_x(ptw_io_requestor_1_resp_bits_pte_x),
    .io_requestor_1_resp_bits_pte_w(ptw_io_requestor_1_resp_bits_pte_w),
    .io_requestor_1_resp_bits_pte_r(ptw_io_requestor_1_resp_bits_pte_r),
    .io_requestor_1_resp_bits_pte_v(ptw_io_requestor_1_resp_bits_pte_v),
    .io_requestor_1_resp_bits_level(ptw_io_requestor_1_resp_bits_level),
    .io_requestor_1_resp_bits_homogeneous(ptw_io_requestor_1_resp_bits_homogeneous),
    .io_requestor_1_ptbr_mode(ptw_io_requestor_1_ptbr_mode),
    .io_requestor_1_status_debug(ptw_io_requestor_1_status_debug),
    .io_requestor_1_status_prv(ptw_io_requestor_1_status_prv),
    .io_requestor_1_pmp_0_cfg_l(ptw_io_requestor_1_pmp_0_cfg_l),
    .io_requestor_1_pmp_0_cfg_a(ptw_io_requestor_1_pmp_0_cfg_a),
    .io_requestor_1_pmp_0_cfg_x(ptw_io_requestor_1_pmp_0_cfg_x),
    .io_requestor_1_pmp_0_cfg_w(ptw_io_requestor_1_pmp_0_cfg_w),
    .io_requestor_1_pmp_0_cfg_r(ptw_io_requestor_1_pmp_0_cfg_r),
    .io_requestor_1_pmp_0_addr(ptw_io_requestor_1_pmp_0_addr),
    .io_requestor_1_pmp_0_mask(ptw_io_requestor_1_pmp_0_mask),
    .io_requestor_1_pmp_1_cfg_l(ptw_io_requestor_1_pmp_1_cfg_l),
    .io_requestor_1_pmp_1_cfg_a(ptw_io_requestor_1_pmp_1_cfg_a),
    .io_requestor_1_pmp_1_cfg_x(ptw_io_requestor_1_pmp_1_cfg_x),
    .io_requestor_1_pmp_1_cfg_w(ptw_io_requestor_1_pmp_1_cfg_w),
    .io_requestor_1_pmp_1_cfg_r(ptw_io_requestor_1_pmp_1_cfg_r),
    .io_requestor_1_pmp_1_addr(ptw_io_requestor_1_pmp_1_addr),
    .io_requestor_1_pmp_1_mask(ptw_io_requestor_1_pmp_1_mask),
    .io_requestor_1_pmp_2_cfg_l(ptw_io_requestor_1_pmp_2_cfg_l),
    .io_requestor_1_pmp_2_cfg_a(ptw_io_requestor_1_pmp_2_cfg_a),
    .io_requestor_1_pmp_2_cfg_x(ptw_io_requestor_1_pmp_2_cfg_x),
    .io_requestor_1_pmp_2_cfg_w(ptw_io_requestor_1_pmp_2_cfg_w),
    .io_requestor_1_pmp_2_cfg_r(ptw_io_requestor_1_pmp_2_cfg_r),
    .io_requestor_1_pmp_2_addr(ptw_io_requestor_1_pmp_2_addr),
    .io_requestor_1_pmp_2_mask(ptw_io_requestor_1_pmp_2_mask),
    .io_requestor_1_pmp_3_cfg_l(ptw_io_requestor_1_pmp_3_cfg_l),
    .io_requestor_1_pmp_3_cfg_a(ptw_io_requestor_1_pmp_3_cfg_a),
    .io_requestor_1_pmp_3_cfg_x(ptw_io_requestor_1_pmp_3_cfg_x),
    .io_requestor_1_pmp_3_cfg_w(ptw_io_requestor_1_pmp_3_cfg_w),
    .io_requestor_1_pmp_3_cfg_r(ptw_io_requestor_1_pmp_3_cfg_r),
    .io_requestor_1_pmp_3_addr(ptw_io_requestor_1_pmp_3_addr),
    .io_requestor_1_pmp_3_mask(ptw_io_requestor_1_pmp_3_mask),
    .io_requestor_1_pmp_4_cfg_l(ptw_io_requestor_1_pmp_4_cfg_l),
    .io_requestor_1_pmp_4_cfg_a(ptw_io_requestor_1_pmp_4_cfg_a),
    .io_requestor_1_pmp_4_cfg_x(ptw_io_requestor_1_pmp_4_cfg_x),
    .io_requestor_1_pmp_4_cfg_w(ptw_io_requestor_1_pmp_4_cfg_w),
    .io_requestor_1_pmp_4_cfg_r(ptw_io_requestor_1_pmp_4_cfg_r),
    .io_requestor_1_pmp_4_addr(ptw_io_requestor_1_pmp_4_addr),
    .io_requestor_1_pmp_4_mask(ptw_io_requestor_1_pmp_4_mask),
    .io_requestor_1_pmp_5_cfg_l(ptw_io_requestor_1_pmp_5_cfg_l),
    .io_requestor_1_pmp_5_cfg_a(ptw_io_requestor_1_pmp_5_cfg_a),
    .io_requestor_1_pmp_5_cfg_x(ptw_io_requestor_1_pmp_5_cfg_x),
    .io_requestor_1_pmp_5_cfg_w(ptw_io_requestor_1_pmp_5_cfg_w),
    .io_requestor_1_pmp_5_cfg_r(ptw_io_requestor_1_pmp_5_cfg_r),
    .io_requestor_1_pmp_5_addr(ptw_io_requestor_1_pmp_5_addr),
    .io_requestor_1_pmp_5_mask(ptw_io_requestor_1_pmp_5_mask),
    .io_requestor_1_pmp_6_cfg_l(ptw_io_requestor_1_pmp_6_cfg_l),
    .io_requestor_1_pmp_6_cfg_a(ptw_io_requestor_1_pmp_6_cfg_a),
    .io_requestor_1_pmp_6_cfg_x(ptw_io_requestor_1_pmp_6_cfg_x),
    .io_requestor_1_pmp_6_cfg_w(ptw_io_requestor_1_pmp_6_cfg_w),
    .io_requestor_1_pmp_6_cfg_r(ptw_io_requestor_1_pmp_6_cfg_r),
    .io_requestor_1_pmp_6_addr(ptw_io_requestor_1_pmp_6_addr),
    .io_requestor_1_pmp_6_mask(ptw_io_requestor_1_pmp_6_mask),
    .io_requestor_1_pmp_7_cfg_l(ptw_io_requestor_1_pmp_7_cfg_l),
    .io_requestor_1_pmp_7_cfg_a(ptw_io_requestor_1_pmp_7_cfg_a),
    .io_requestor_1_pmp_7_cfg_x(ptw_io_requestor_1_pmp_7_cfg_x),
    .io_requestor_1_pmp_7_cfg_w(ptw_io_requestor_1_pmp_7_cfg_w),
    .io_requestor_1_pmp_7_cfg_r(ptw_io_requestor_1_pmp_7_cfg_r),
    .io_requestor_1_pmp_7_addr(ptw_io_requestor_1_pmp_7_addr),
    .io_requestor_1_pmp_7_mask(ptw_io_requestor_1_pmp_7_mask),
    .io_requestor_1_customCSRs_csrs_0_wen(ptw_io_requestor_1_customCSRs_csrs_0_wen),
    .io_requestor_1_customCSRs_csrs_0_value(ptw_io_requestor_1_customCSRs_csrs_0_value),
    .io_requestor_1_customCSRs_csrs_1_value(ptw_io_requestor_1_customCSRs_csrs_1_value),
    .io_mem_req_ready(ptw_io_mem_req_ready),
    .io_mem_req_valid(ptw_io_mem_req_valid),
    .io_mem_req_bits_addr(ptw_io_mem_req_bits_addr),
    .io_mem_req_bits_idx(ptw_io_mem_req_bits_idx),
    .io_mem_s1_kill(ptw_io_mem_s1_kill),
    .io_mem_s2_nack(ptw_io_mem_s2_nack),
    .io_mem_resp_valid(ptw_io_mem_resp_valid),
    .io_mem_resp_bits_data(ptw_io_mem_resp_bits_data),
    .io_mem_s2_xcpt_ae_ld(ptw_io_mem_s2_xcpt_ae_ld),
    .io_dpath_ptbr_mode(ptw_io_dpath_ptbr_mode),
    .io_dpath_ptbr_ppn(ptw_io_dpath_ptbr_ppn),
    .io_dpath_sfence_valid(ptw_io_dpath_sfence_valid),
    .io_dpath_sfence_bits_rs1(ptw_io_dpath_sfence_bits_rs1),
    .io_dpath_sfence_bits_rs2(ptw_io_dpath_sfence_bits_rs2),
    .io_dpath_sfence_bits_addr(ptw_io_dpath_sfence_bits_addr),
    .io_dpath_status_debug(ptw_io_dpath_status_debug),
    .io_dpath_status_dprv(ptw_io_dpath_status_dprv),
    .io_dpath_status_prv(ptw_io_dpath_status_prv),
    .io_dpath_status_mxr(ptw_io_dpath_status_mxr),
    .io_dpath_status_sum(ptw_io_dpath_status_sum),
    .io_dpath_pmp_0_cfg_l(ptw_io_dpath_pmp_0_cfg_l),
    .io_dpath_pmp_0_cfg_a(ptw_io_dpath_pmp_0_cfg_a),
    .io_dpath_pmp_0_cfg_x(ptw_io_dpath_pmp_0_cfg_x),
    .io_dpath_pmp_0_cfg_w(ptw_io_dpath_pmp_0_cfg_w),
    .io_dpath_pmp_0_cfg_r(ptw_io_dpath_pmp_0_cfg_r),
    .io_dpath_pmp_0_addr(ptw_io_dpath_pmp_0_addr),
    .io_dpath_pmp_0_mask(ptw_io_dpath_pmp_0_mask),
    .io_dpath_pmp_1_cfg_l(ptw_io_dpath_pmp_1_cfg_l),
    .io_dpath_pmp_1_cfg_a(ptw_io_dpath_pmp_1_cfg_a),
    .io_dpath_pmp_1_cfg_x(ptw_io_dpath_pmp_1_cfg_x),
    .io_dpath_pmp_1_cfg_w(ptw_io_dpath_pmp_1_cfg_w),
    .io_dpath_pmp_1_cfg_r(ptw_io_dpath_pmp_1_cfg_r),
    .io_dpath_pmp_1_addr(ptw_io_dpath_pmp_1_addr),
    .io_dpath_pmp_1_mask(ptw_io_dpath_pmp_1_mask),
    .io_dpath_pmp_2_cfg_l(ptw_io_dpath_pmp_2_cfg_l),
    .io_dpath_pmp_2_cfg_a(ptw_io_dpath_pmp_2_cfg_a),
    .io_dpath_pmp_2_cfg_x(ptw_io_dpath_pmp_2_cfg_x),
    .io_dpath_pmp_2_cfg_w(ptw_io_dpath_pmp_2_cfg_w),
    .io_dpath_pmp_2_cfg_r(ptw_io_dpath_pmp_2_cfg_r),
    .io_dpath_pmp_2_addr(ptw_io_dpath_pmp_2_addr),
    .io_dpath_pmp_2_mask(ptw_io_dpath_pmp_2_mask),
    .io_dpath_pmp_3_cfg_l(ptw_io_dpath_pmp_3_cfg_l),
    .io_dpath_pmp_3_cfg_a(ptw_io_dpath_pmp_3_cfg_a),
    .io_dpath_pmp_3_cfg_x(ptw_io_dpath_pmp_3_cfg_x),
    .io_dpath_pmp_3_cfg_w(ptw_io_dpath_pmp_3_cfg_w),
    .io_dpath_pmp_3_cfg_r(ptw_io_dpath_pmp_3_cfg_r),
    .io_dpath_pmp_3_addr(ptw_io_dpath_pmp_3_addr),
    .io_dpath_pmp_3_mask(ptw_io_dpath_pmp_3_mask),
    .io_dpath_pmp_4_cfg_l(ptw_io_dpath_pmp_4_cfg_l),
    .io_dpath_pmp_4_cfg_a(ptw_io_dpath_pmp_4_cfg_a),
    .io_dpath_pmp_4_cfg_x(ptw_io_dpath_pmp_4_cfg_x),
    .io_dpath_pmp_4_cfg_w(ptw_io_dpath_pmp_4_cfg_w),
    .io_dpath_pmp_4_cfg_r(ptw_io_dpath_pmp_4_cfg_r),
    .io_dpath_pmp_4_addr(ptw_io_dpath_pmp_4_addr),
    .io_dpath_pmp_4_mask(ptw_io_dpath_pmp_4_mask),
    .io_dpath_pmp_5_cfg_l(ptw_io_dpath_pmp_5_cfg_l),
    .io_dpath_pmp_5_cfg_a(ptw_io_dpath_pmp_5_cfg_a),
    .io_dpath_pmp_5_cfg_x(ptw_io_dpath_pmp_5_cfg_x),
    .io_dpath_pmp_5_cfg_w(ptw_io_dpath_pmp_5_cfg_w),
    .io_dpath_pmp_5_cfg_r(ptw_io_dpath_pmp_5_cfg_r),
    .io_dpath_pmp_5_addr(ptw_io_dpath_pmp_5_addr),
    .io_dpath_pmp_5_mask(ptw_io_dpath_pmp_5_mask),
    .io_dpath_pmp_6_cfg_l(ptw_io_dpath_pmp_6_cfg_l),
    .io_dpath_pmp_6_cfg_a(ptw_io_dpath_pmp_6_cfg_a),
    .io_dpath_pmp_6_cfg_x(ptw_io_dpath_pmp_6_cfg_x),
    .io_dpath_pmp_6_cfg_w(ptw_io_dpath_pmp_6_cfg_w),
    .io_dpath_pmp_6_cfg_r(ptw_io_dpath_pmp_6_cfg_r),
    .io_dpath_pmp_6_addr(ptw_io_dpath_pmp_6_addr),
    .io_dpath_pmp_6_mask(ptw_io_dpath_pmp_6_mask),
    .io_dpath_pmp_7_cfg_l(ptw_io_dpath_pmp_7_cfg_l),
    .io_dpath_pmp_7_cfg_a(ptw_io_dpath_pmp_7_cfg_a),
    .io_dpath_pmp_7_cfg_x(ptw_io_dpath_pmp_7_cfg_x),
    .io_dpath_pmp_7_cfg_w(ptw_io_dpath_pmp_7_cfg_w),
    .io_dpath_pmp_7_cfg_r(ptw_io_dpath_pmp_7_cfg_r),
    .io_dpath_pmp_7_addr(ptw_io_dpath_pmp_7_addr),
    .io_dpath_pmp_7_mask(ptw_io_dpath_pmp_7_mask),
    .io_dpath_perf_l2miss(ptw_io_dpath_perf_l2miss),
    .io_dpath_perf_l2hit(ptw_io_dpath_perf_l2hit),
    .io_dpath_perf_pte_miss(ptw_io_dpath_perf_pte_miss),
    .io_dpath_perf_pte_hit(ptw_io_dpath_perf_pte_hit),
    .io_dpath_customCSRs_csrs_0_wen(ptw_io_dpath_customCSRs_csrs_0_wen),
    .io_dpath_customCSRs_csrs_0_value(ptw_io_dpath_customCSRs_csrs_0_value),
    .io_dpath_customCSRs_csrs_1_value(ptw_io_dpath_customCSRs_csrs_1_value),
    .io_dpath_clock_enabled(ptw_io_dpath_clock_enabled),
    .psd_test_clock_enable(ptw_psd_test_clock_enable)
  );
  RHEA__Rocket core ( // @[RocketTile.scala 157:20]
    .rf_reset(core_rf_reset),
    .clock(core_clock),
    .reset(core_reset),
    .io_hartid(core_io_hartid),
    .io_interrupts_debug(core_io_interrupts_debug),
    .io_interrupts_mtip(core_io_interrupts_mtip),
    .io_interrupts_msip(core_io_interrupts_msip),
    .io_interrupts_meip(core_io_interrupts_meip),
    .io_interrupts_seip(core_io_interrupts_seip),
    .io_interrupts_lip_0(core_io_interrupts_lip_0),
    .io_interrupts_lip_1(core_io_interrupts_lip_1),
    .io_interrupts_lip_2(core_io_interrupts_lip_2),
    .io_interrupts_lip_3(core_io_interrupts_lip_3),
    .io_interrupts_lip_4(core_io_interrupts_lip_4),
    .io_interrupts_lip_5(core_io_interrupts_lip_5),
    .io_interrupts_lip_6(core_io_interrupts_lip_6),
    .io_interrupts_lip_7(core_io_interrupts_lip_7),
    .io_interrupts_lip_8(core_io_interrupts_lip_8),
    .io_interrupts_lip_9(core_io_interrupts_lip_9),
    .io_interrupts_lip_10(core_io_interrupts_lip_10),
    .io_interrupts_lip_11(core_io_interrupts_lip_11),
    .io_interrupts_lip_12(core_io_interrupts_lip_12),
    .io_interrupts_lip_13(core_io_interrupts_lip_13),
    .io_interrupts_lip_14(core_io_interrupts_lip_14),
    .io_interrupts_lip_15(core_io_interrupts_lip_15),
    .io_interrupts_nmi_rnmi(core_io_interrupts_nmi_rnmi),
    .io_interrupts_nmi_rnmi_interrupt_vector(core_io_interrupts_nmi_rnmi_interrupt_vector),
    .io_interrupts_nmi_rnmi_exception_vector(core_io_interrupts_nmi_rnmi_exception_vector),
    .io_imem_might_request(core_io_imem_might_request),
    .io_imem_req_valid(core_io_imem_req_valid),
    .io_imem_req_bits_pc(core_io_imem_req_bits_pc),
    .io_imem_req_bits_speculative(core_io_imem_req_bits_speculative),
    .io_imem_sfence_valid(core_io_imem_sfence_valid),
    .io_imem_sfence_bits_rs1(core_io_imem_sfence_bits_rs1),
    .io_imem_sfence_bits_rs2(core_io_imem_sfence_bits_rs2),
    .io_imem_sfence_bits_addr(core_io_imem_sfence_bits_addr),
    .io_imem_sfence_bits_asid(core_io_imem_sfence_bits_asid),
    .io_imem_resp_ready(core_io_imem_resp_ready),
    .io_imem_resp_valid(core_io_imem_resp_valid),
    .io_imem_resp_bits_btb_taken(core_io_imem_resp_bits_btb_taken),
    .io_imem_resp_bits_btb_bridx(core_io_imem_resp_bits_btb_bridx),
    .io_imem_resp_bits_btb_entry(core_io_imem_resp_bits_btb_entry),
    .io_imem_resp_bits_btb_bht_history(core_io_imem_resp_bits_btb_bht_history),
    .io_imem_resp_bits_pc(core_io_imem_resp_bits_pc),
    .io_imem_resp_bits_data(core_io_imem_resp_bits_data),
    .io_imem_resp_bits_mask(core_io_imem_resp_bits_mask),
    .io_imem_resp_bits_xcpt_pf_inst(core_io_imem_resp_bits_xcpt_pf_inst),
    .io_imem_resp_bits_xcpt_ae_inst(core_io_imem_resp_bits_xcpt_ae_inst),
    .io_imem_resp_bits_replay(core_io_imem_resp_bits_replay),
    .io_imem_btb_update_valid(core_io_imem_btb_update_valid),
    .io_imem_btb_update_bits_prediction_entry(core_io_imem_btb_update_bits_prediction_entry),
    .io_imem_btb_update_bits_pc(core_io_imem_btb_update_bits_pc),
    .io_imem_btb_update_bits_isValid(core_io_imem_btb_update_bits_isValid),
    .io_imem_btb_update_bits_br_pc(core_io_imem_btb_update_bits_br_pc),
    .io_imem_btb_update_bits_cfiType(core_io_imem_btb_update_bits_cfiType),
    .io_imem_bht_update_valid(core_io_imem_bht_update_valid),
    .io_imem_bht_update_bits_prediction_history(core_io_imem_bht_update_bits_prediction_history),
    .io_imem_bht_update_bits_pc(core_io_imem_bht_update_bits_pc),
    .io_imem_bht_update_bits_branch(core_io_imem_bht_update_bits_branch),
    .io_imem_bht_update_bits_taken(core_io_imem_bht_update_bits_taken),
    .io_imem_bht_update_bits_mispredict(core_io_imem_bht_update_bits_mispredict),
    .io_imem_flush_icache(core_io_imem_flush_icache),
    .io_imem_perf_acquire(core_io_imem_perf_acquire),
    .io_imem_perf_tlbMiss(core_io_imem_perf_tlbMiss),
    .io_dmem_req_ready(core_io_dmem_req_ready),
    .io_dmem_req_valid(core_io_dmem_req_valid),
    .io_dmem_req_bits_addr(core_io_dmem_req_bits_addr),
    .io_dmem_req_bits_idx(core_io_dmem_req_bits_idx),
    .io_dmem_req_bits_tag(core_io_dmem_req_bits_tag),
    .io_dmem_req_bits_cmd(core_io_dmem_req_bits_cmd),
    .io_dmem_req_bits_size(core_io_dmem_req_bits_size),
    .io_dmem_req_bits_signed(core_io_dmem_req_bits_signed),
    .io_dmem_req_bits_dprv(core_io_dmem_req_bits_dprv),
    .io_dmem_req_bits_phys(core_io_dmem_req_bits_phys),
    .io_dmem_req_bits_no_alloc(core_io_dmem_req_bits_no_alloc),
    .io_dmem_req_bits_no_xcpt(core_io_dmem_req_bits_no_xcpt),
    .io_dmem_req_bits_data(core_io_dmem_req_bits_data),
    .io_dmem_req_bits_mask(core_io_dmem_req_bits_mask),
    .io_dmem_s1_kill(core_io_dmem_s1_kill),
    .io_dmem_s1_data_data(core_io_dmem_s1_data_data),
    .io_dmem_s1_data_mask(core_io_dmem_s1_data_mask),
    .io_dmem_s2_nack(core_io_dmem_s2_nack),
    .io_dmem_resp_valid(core_io_dmem_resp_valid),
    .io_dmem_resp_bits_addr(core_io_dmem_resp_bits_addr),
    .io_dmem_resp_bits_tag(core_io_dmem_resp_bits_tag),
    .io_dmem_resp_bits_cmd(core_io_dmem_resp_bits_cmd),
    .io_dmem_resp_bits_size(core_io_dmem_resp_bits_size),
    .io_dmem_resp_bits_signed(core_io_dmem_resp_bits_signed),
    .io_dmem_resp_bits_data(core_io_dmem_resp_bits_data),
    .io_dmem_resp_bits_replay(core_io_dmem_resp_bits_replay),
    .io_dmem_resp_bits_has_data(core_io_dmem_resp_bits_has_data),
    .io_dmem_resp_bits_data_word_bypass(core_io_dmem_resp_bits_data_word_bypass),
    .io_dmem_replay_next(core_io_dmem_replay_next),
    .io_dmem_s2_xcpt_ma_ld(core_io_dmem_s2_xcpt_ma_ld),
    .io_dmem_s2_xcpt_ma_st(core_io_dmem_s2_xcpt_ma_st),
    .io_dmem_s2_xcpt_pf_ld(core_io_dmem_s2_xcpt_pf_ld),
    .io_dmem_s2_xcpt_pf_st(core_io_dmem_s2_xcpt_pf_st),
    .io_dmem_s2_xcpt_ae_ld(core_io_dmem_s2_xcpt_ae_ld),
    .io_dmem_s2_xcpt_ae_st(core_io_dmem_s2_xcpt_ae_st),
    .io_dmem_ordered(core_io_dmem_ordered),
    .io_dmem_perf_acquire(core_io_dmem_perf_acquire),
    .io_dmem_perf_release(core_io_dmem_perf_release),
    .io_dmem_perf_grant(core_io_dmem_perf_grant),
    .io_dmem_perf_tlbMiss(core_io_dmem_perf_tlbMiss),
    .io_dmem_perf_blocked(core_io_dmem_perf_blocked),
    .io_dmem_keep_clock_enabled(core_io_dmem_keep_clock_enabled),
    .io_dmem_clock_enabled(core_io_dmem_clock_enabled),
    .io_ptw_ptbr_mode(core_io_ptw_ptbr_mode),
    .io_ptw_ptbr_ppn(core_io_ptw_ptbr_ppn),
    .io_ptw_sfence_valid(core_io_ptw_sfence_valid),
    .io_ptw_sfence_bits_rs1(core_io_ptw_sfence_bits_rs1),
    .io_ptw_sfence_bits_rs2(core_io_ptw_sfence_bits_rs2),
    .io_ptw_sfence_bits_addr(core_io_ptw_sfence_bits_addr),
    .io_ptw_status_debug(core_io_ptw_status_debug),
    .io_ptw_status_dprv(core_io_ptw_status_dprv),
    .io_ptw_status_prv(core_io_ptw_status_prv),
    .io_ptw_status_mxr(core_io_ptw_status_mxr),
    .io_ptw_status_sum(core_io_ptw_status_sum),
    .io_ptw_pmp_0_cfg_l(core_io_ptw_pmp_0_cfg_l),
    .io_ptw_pmp_0_cfg_a(core_io_ptw_pmp_0_cfg_a),
    .io_ptw_pmp_0_cfg_x(core_io_ptw_pmp_0_cfg_x),
    .io_ptw_pmp_0_cfg_w(core_io_ptw_pmp_0_cfg_w),
    .io_ptw_pmp_0_cfg_r(core_io_ptw_pmp_0_cfg_r),
    .io_ptw_pmp_0_addr(core_io_ptw_pmp_0_addr),
    .io_ptw_pmp_0_mask(core_io_ptw_pmp_0_mask),
    .io_ptw_pmp_1_cfg_l(core_io_ptw_pmp_1_cfg_l),
    .io_ptw_pmp_1_cfg_a(core_io_ptw_pmp_1_cfg_a),
    .io_ptw_pmp_1_cfg_x(core_io_ptw_pmp_1_cfg_x),
    .io_ptw_pmp_1_cfg_w(core_io_ptw_pmp_1_cfg_w),
    .io_ptw_pmp_1_cfg_r(core_io_ptw_pmp_1_cfg_r),
    .io_ptw_pmp_1_addr(core_io_ptw_pmp_1_addr),
    .io_ptw_pmp_1_mask(core_io_ptw_pmp_1_mask),
    .io_ptw_pmp_2_cfg_l(core_io_ptw_pmp_2_cfg_l),
    .io_ptw_pmp_2_cfg_a(core_io_ptw_pmp_2_cfg_a),
    .io_ptw_pmp_2_cfg_x(core_io_ptw_pmp_2_cfg_x),
    .io_ptw_pmp_2_cfg_w(core_io_ptw_pmp_2_cfg_w),
    .io_ptw_pmp_2_cfg_r(core_io_ptw_pmp_2_cfg_r),
    .io_ptw_pmp_2_addr(core_io_ptw_pmp_2_addr),
    .io_ptw_pmp_2_mask(core_io_ptw_pmp_2_mask),
    .io_ptw_pmp_3_cfg_l(core_io_ptw_pmp_3_cfg_l),
    .io_ptw_pmp_3_cfg_a(core_io_ptw_pmp_3_cfg_a),
    .io_ptw_pmp_3_cfg_x(core_io_ptw_pmp_3_cfg_x),
    .io_ptw_pmp_3_cfg_w(core_io_ptw_pmp_3_cfg_w),
    .io_ptw_pmp_3_cfg_r(core_io_ptw_pmp_3_cfg_r),
    .io_ptw_pmp_3_addr(core_io_ptw_pmp_3_addr),
    .io_ptw_pmp_3_mask(core_io_ptw_pmp_3_mask),
    .io_ptw_pmp_4_cfg_l(core_io_ptw_pmp_4_cfg_l),
    .io_ptw_pmp_4_cfg_a(core_io_ptw_pmp_4_cfg_a),
    .io_ptw_pmp_4_cfg_x(core_io_ptw_pmp_4_cfg_x),
    .io_ptw_pmp_4_cfg_w(core_io_ptw_pmp_4_cfg_w),
    .io_ptw_pmp_4_cfg_r(core_io_ptw_pmp_4_cfg_r),
    .io_ptw_pmp_4_addr(core_io_ptw_pmp_4_addr),
    .io_ptw_pmp_4_mask(core_io_ptw_pmp_4_mask),
    .io_ptw_pmp_5_cfg_l(core_io_ptw_pmp_5_cfg_l),
    .io_ptw_pmp_5_cfg_a(core_io_ptw_pmp_5_cfg_a),
    .io_ptw_pmp_5_cfg_x(core_io_ptw_pmp_5_cfg_x),
    .io_ptw_pmp_5_cfg_w(core_io_ptw_pmp_5_cfg_w),
    .io_ptw_pmp_5_cfg_r(core_io_ptw_pmp_5_cfg_r),
    .io_ptw_pmp_5_addr(core_io_ptw_pmp_5_addr),
    .io_ptw_pmp_5_mask(core_io_ptw_pmp_5_mask),
    .io_ptw_pmp_6_cfg_l(core_io_ptw_pmp_6_cfg_l),
    .io_ptw_pmp_6_cfg_a(core_io_ptw_pmp_6_cfg_a),
    .io_ptw_pmp_6_cfg_x(core_io_ptw_pmp_6_cfg_x),
    .io_ptw_pmp_6_cfg_w(core_io_ptw_pmp_6_cfg_w),
    .io_ptw_pmp_6_cfg_r(core_io_ptw_pmp_6_cfg_r),
    .io_ptw_pmp_6_addr(core_io_ptw_pmp_6_addr),
    .io_ptw_pmp_6_mask(core_io_ptw_pmp_6_mask),
    .io_ptw_pmp_7_cfg_l(core_io_ptw_pmp_7_cfg_l),
    .io_ptw_pmp_7_cfg_a(core_io_ptw_pmp_7_cfg_a),
    .io_ptw_pmp_7_cfg_x(core_io_ptw_pmp_7_cfg_x),
    .io_ptw_pmp_7_cfg_w(core_io_ptw_pmp_7_cfg_w),
    .io_ptw_pmp_7_cfg_r(core_io_ptw_pmp_7_cfg_r),
    .io_ptw_pmp_7_addr(core_io_ptw_pmp_7_addr),
    .io_ptw_pmp_7_mask(core_io_ptw_pmp_7_mask),
    .io_ptw_perf_l2miss(core_io_ptw_perf_l2miss),
    .io_ptw_customCSRs_csrs_0_wen(core_io_ptw_customCSRs_csrs_0_wen),
    .io_ptw_customCSRs_csrs_0_value(core_io_ptw_customCSRs_csrs_0_value),
    .io_ptw_customCSRs_csrs_1_value(core_io_ptw_customCSRs_csrs_1_value),
    .io_fpu_inst(core_io_fpu_inst),
    .io_fpu_fromint_data(core_io_fpu_fromint_data),
    .io_fpu_fcsr_rm(core_io_fpu_fcsr_rm),
    .io_fpu_fcsr_flags_valid(core_io_fpu_fcsr_flags_valid),
    .io_fpu_fcsr_flags_bits(core_io_fpu_fcsr_flags_bits),
    .io_fpu_store_data(core_io_fpu_store_data),
    .io_fpu_toint_data(core_io_fpu_toint_data),
    .io_fpu_dmem_resp_val(core_io_fpu_dmem_resp_val),
    .io_fpu_dmem_resp_type(core_io_fpu_dmem_resp_type),
    .io_fpu_dmem_resp_tag(core_io_fpu_dmem_resp_tag),
    .io_fpu_dmem_resp_data(core_io_fpu_dmem_resp_data),
    .io_fpu_valid(core_io_fpu_valid),
    .io_fpu_fcsr_rdy(core_io_fpu_fcsr_rdy),
    .io_fpu_nack_mem(core_io_fpu_nack_mem),
    .io_fpu_illegal_rm(core_io_fpu_illegal_rm),
    .io_fpu_killx(core_io_fpu_killx),
    .io_fpu_killm(core_io_fpu_killm),
    .io_fpu_dec_ldst(core_io_fpu_dec_ldst),
    .io_fpu_dec_wen(core_io_fpu_dec_wen),
    .io_fpu_dec_ren1(core_io_fpu_dec_ren1),
    .io_fpu_dec_ren2(core_io_fpu_dec_ren2),
    .io_fpu_dec_ren3(core_io_fpu_dec_ren3),
    .io_fpu_dec_swap23(core_io_fpu_dec_swap23),
    .io_fpu_dec_fma(core_io_fpu_dec_fma),
    .io_fpu_dec_div(core_io_fpu_dec_div),
    .io_fpu_dec_sqrt(core_io_fpu_dec_sqrt),
    .io_fpu_sboard_set(core_io_fpu_sboard_set),
    .io_fpu_sboard_clr(core_io_fpu_sboard_clr),
    .io_fpu_sboard_clra(core_io_fpu_sboard_clra),
    .io_fpu_keep_clock_enabled(core_io_fpu_keep_clock_enabled),
    .io_trace_0_valid(core_io_trace_0_valid),
    .io_trace_0_iaddr(core_io_trace_0_iaddr),
    .io_trace_0_insn(core_io_trace_0_insn),
    .io_trace_0_priv(core_io_trace_0_priv),
    .io_trace_0_exception(core_io_trace_0_exception),
    .io_trace_0_interrupt(core_io_trace_0_interrupt),
    .io_bpwatch_0_valid_0(core_io_bpwatch_0_valid_0),
    .io_bpwatch_0_action(core_io_bpwatch_0_action),
    .io_bpwatch_1_valid_0(core_io_bpwatch_1_valid_0),
    .io_bpwatch_1_action(core_io_bpwatch_1_action),
    .io_bpwatch_2_valid_0(core_io_bpwatch_2_valid_0),
    .io_bpwatch_2_action(core_io_bpwatch_2_action),
    .io_bpwatch_3_valid_0(core_io_bpwatch_3_valid_0),
    .io_bpwatch_3_action(core_io_bpwatch_3_action),
    .io_bpwatch_4_valid_0(core_io_bpwatch_4_valid_0),
    .io_bpwatch_4_action(core_io_bpwatch_4_action),
    .io_bpwatch_5_valid_0(core_io_bpwatch_5_valid_0),
    .io_bpwatch_5_action(core_io_bpwatch_5_action),
    .io_bpwatch_6_valid_0(core_io_bpwatch_6_valid_0),
    .io_bpwatch_6_action(core_io_bpwatch_6_action),
    .io_bpwatch_7_valid_0(core_io_bpwatch_7_valid_0),
    .io_bpwatch_7_action(core_io_bpwatch_7_action),
    .io_bpwatch_8_valid_0(core_io_bpwatch_8_valid_0),
    .io_bpwatch_8_action(core_io_bpwatch_8_action),
    .io_bpwatch_9_valid_0(core_io_bpwatch_9_valid_0),
    .io_bpwatch_9_action(core_io_bpwatch_9_action),
    .io_bpwatch_10_valid_0(core_io_bpwatch_10_valid_0),
    .io_bpwatch_10_action(core_io_bpwatch_10_action),
    .io_bpwatch_11_valid_0(core_io_bpwatch_11_valid_0),
    .io_bpwatch_11_action(core_io_bpwatch_11_action),
    .io_bpwatch_12_valid_0(core_io_bpwatch_12_valid_0),
    .io_bpwatch_12_action(core_io_bpwatch_12_action),
    .io_bpwatch_13_valid_0(core_io_bpwatch_13_valid_0),
    .io_bpwatch_13_action(core_io_bpwatch_13_action),
    .io_bpwatch_14_valid_0(core_io_bpwatch_14_valid_0),
    .io_bpwatch_14_action(core_io_bpwatch_14_action),
    .io_bpwatch_15_valid_0(core_io_bpwatch_15_valid_0),
    .io_bpwatch_15_action(core_io_bpwatch_15_action),
    .io_cease(core_io_cease),
    .io_wfi(core_io_wfi),
    .io_debug(core_io_debug),
    .io_traceStall(core_io_traceStall),
    .psd_test_clock_enable(core_psd_test_clock_enable)
  );
  assign tlMasterXbar_rf_reset = rf_reset;
  assign broadcast_auto_out = broadcast_auto_in; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_1_auto_out_1 = broadcast_1_auto_in; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_3_auto_out_rnmi = broadcast_3_auto_in_rnmi; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_3_auto_out_rnmi_interrupt_vector = broadcast_3_auto_in_rnmi_interrupt_vector; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_3_auto_out_rnmi_exception_vector = broadcast_3_auto_in_rnmi_exception_vector; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_4_auto_out_0_valid = broadcast_4_auto_in_0_valid; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_4_auto_out_0_iaddr = broadcast_4_auto_in_0_iaddr; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_4_auto_out_0_insn = broadcast_4_auto_in_0_insn; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_4_auto_out_0_priv = broadcast_4_auto_in_0_priv; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_4_auto_out_0_exception = broadcast_4_auto_in_0_exception; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_4_auto_out_0_interrupt = broadcast_4_auto_in_0_interrupt; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign nexus_1_auto_out_stall = nexus_1_auto_in_stall; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_0_valid_0 = broadcast_5_auto_in_0_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_0_action = broadcast_5_auto_in_0_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_1_valid_0 = broadcast_5_auto_in_1_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_1_action = broadcast_5_auto_in_1_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_2_valid_0 = broadcast_5_auto_in_2_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_2_action = broadcast_5_auto_in_2_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_3_valid_0 = broadcast_5_auto_in_3_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_3_action = broadcast_5_auto_in_3_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_4_valid_0 = broadcast_5_auto_in_4_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_4_action = broadcast_5_auto_in_4_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_5_valid_0 = broadcast_5_auto_in_5_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_5_action = broadcast_5_auto_in_5_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_6_valid_0 = broadcast_5_auto_in_6_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_6_action = broadcast_5_auto_in_6_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_7_valid_0 = broadcast_5_auto_in_7_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_7_action = broadcast_5_auto_in_7_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_8_valid_0 = broadcast_5_auto_in_8_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_8_action = broadcast_5_auto_in_8_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_9_valid_0 = broadcast_5_auto_in_9_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_9_action = broadcast_5_auto_in_9_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_10_valid_0 = broadcast_5_auto_in_10_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_10_action = broadcast_5_auto_in_10_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_11_valid_0 = broadcast_5_auto_in_11_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_11_action = broadcast_5_auto_in_11_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_12_valid_0 = broadcast_5_auto_in_12_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_12_action = broadcast_5_auto_in_12_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_13_valid_0 = broadcast_5_auto_in_13_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_13_action = broadcast_5_auto_in_13_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_14_valid_0 = broadcast_5_auto_in_14_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_14_action = broadcast_5_auto_in_14_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_15_valid_0 = broadcast_5_auto_in_15_valid_0; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_out_15_action = broadcast_5_auto_in_15_action; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign dcache_rf_reset = rf_reset;
  assign frontend_rf_reset = rf_reset;
  assign fpuOpt_rf_reset = rf_reset;
  assign dcacheArb_rf_reset = rf_reset;
  assign ptw_rf_reset = rf_reset;
  assign core_rf_reset = rf_reset;
  assign auto_broadcast_out_0_valid = broadcast_4_auto_out_0_valid; // @[LazyModule.scala 390:12]
  assign auto_broadcast_out_0_iaddr = broadcast_4_auto_out_0_iaddr; // @[LazyModule.scala 390:12]
  assign auto_broadcast_out_0_insn = broadcast_4_auto_out_0_insn; // @[LazyModule.scala 390:12]
  assign auto_broadcast_out_0_priv = broadcast_4_auto_out_0_priv; // @[LazyModule.scala 390:12]
  assign auto_broadcast_out_0_exception = broadcast_4_auto_out_0_exception; // @[LazyModule.scala 390:12]
  assign auto_broadcast_out_0_interrupt = broadcast_4_auto_out_0_interrupt; // @[LazyModule.scala 390:12]
  assign auto_debug_out_0 = core_io_debug; // @[Nodes.scala 27:103 Interrupts.scala 126:14]
  assign auto_wfi_out_0 = bundleOut_0_0_REG; // @[Nodes.scala 27:103 Interrupts.scala 118:12]
  assign auto_cease_out_0 = bundleOut_0_0_cease_count >= 4'h8; // @[Interrupts.scala 98:29]
  assign auto_bpwatch_out_0_valid_0 = broadcast_5_auto_out_0_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_0_action = broadcast_5_auto_out_0_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_1_valid_0 = broadcast_5_auto_out_1_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_1_action = broadcast_5_auto_out_1_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_2_valid_0 = broadcast_5_auto_out_2_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_2_action = broadcast_5_auto_out_2_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_3_valid_0 = broadcast_5_auto_out_3_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_3_action = broadcast_5_auto_out_3_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_4_valid_0 = broadcast_5_auto_out_4_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_4_action = broadcast_5_auto_out_4_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_5_valid_0 = broadcast_5_auto_out_5_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_5_action = broadcast_5_auto_out_5_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_6_valid_0 = broadcast_5_auto_out_6_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_6_action = broadcast_5_auto_out_6_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_7_valid_0 = broadcast_5_auto_out_7_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_7_action = broadcast_5_auto_out_7_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_8_valid_0 = broadcast_5_auto_out_8_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_8_action = broadcast_5_auto_out_8_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_9_valid_0 = broadcast_5_auto_out_9_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_9_action = broadcast_5_auto_out_9_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_10_valid_0 = broadcast_5_auto_out_10_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_10_action = broadcast_5_auto_out_10_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_11_valid_0 = broadcast_5_auto_out_11_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_11_action = broadcast_5_auto_out_11_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_12_valid_0 = broadcast_5_auto_out_12_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_12_action = broadcast_5_auto_out_12_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_13_valid_0 = broadcast_5_auto_out_13_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_13_action = broadcast_5_auto_out_13_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_14_valid_0 = broadcast_5_auto_out_14_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_14_action = broadcast_5_auto_out_14_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_15_valid_0 = broadcast_5_auto_out_15_valid_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bpwatch_out_15_action = broadcast_5_auto_out_15_action; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_valid = tlMasterXbar_auto_out_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_bits_opcode = tlMasterXbar_auto_out_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_bits_param = tlMasterXbar_auto_out_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_bits_size = tlMasterXbar_auto_out_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_bits_source = tlMasterXbar_auto_out_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_bits_address = tlMasterXbar_auto_out_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_bits_user_amba_prot_bufferable =
    tlMasterXbar_auto_out_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_bits_user_amba_prot_modifiable =
    tlMasterXbar_auto_out_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_bits_user_amba_prot_readalloc =
    tlMasterXbar_auto_out_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_bits_user_amba_prot_writealloc =
    tlMasterXbar_auto_out_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_bits_user_amba_prot_privileged =
    tlMasterXbar_auto_out_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_bits_user_amba_prot_secure = tlMasterXbar_auto_out_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_bits_user_amba_prot_fetch = tlMasterXbar_auto_out_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_bits_mask = tlMasterXbar_auto_out_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_bits_data = tlMasterXbar_auto_out_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_a_bits_corrupt = tlMasterXbar_auto_out_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_b_ready = tlMasterXbar_auto_out_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_c_valid = tlMasterXbar_auto_out_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_c_bits_opcode = tlMasterXbar_auto_out_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_c_bits_param = tlMasterXbar_auto_out_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_c_bits_size = tlMasterXbar_auto_out_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_c_bits_source = tlMasterXbar_auto_out_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_c_bits_address = tlMasterXbar_auto_out_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_c_bits_data = tlMasterXbar_auto_out_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_c_bits_corrupt = tlMasterXbar_auto_out_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_d_ready = tlMasterXbar_auto_out_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_e_valid = tlMasterXbar_auto_out_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_other_masters_out_e_bits_sink = tlMasterXbar_auto_out_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tlMasterXbar_clock = clock;
  assign tlMasterXbar_reset = reset;
  assign tlMasterXbar_auto_in_1_a_valid = frontend_auto_icache_master_out_a_valid; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_1_a_bits_opcode = frontend_auto_icache_master_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_1_a_bits_source = frontend_auto_icache_master_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_1_a_bits_address = frontend_auto_icache_master_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_valid = dcache_auto_out_a_valid; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_bits_opcode = dcache_auto_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_bits_param = dcache_auto_out_a_bits_param; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_bits_size = dcache_auto_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_bits_source = dcache_auto_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_bits_address = dcache_auto_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_bits_user_amba_prot_bufferable = dcache_auto_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_bits_user_amba_prot_modifiable = dcache_auto_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_bits_user_amba_prot_readalloc = dcache_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_bits_user_amba_prot_writealloc = dcache_auto_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_bits_user_amba_prot_privileged = dcache_auto_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_bits_user_amba_prot_secure = dcache_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_bits_user_amba_prot_fetch = dcache_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_bits_mask = dcache_auto_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_bits_data = dcache_auto_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_a_bits_corrupt = dcache_auto_out_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_b_ready = dcache_auto_out_b_ready; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_c_valid = dcache_auto_out_c_valid; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_c_bits_opcode = dcache_auto_out_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_c_bits_param = dcache_auto_out_c_bits_param; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_c_bits_size = dcache_auto_out_c_bits_size; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_c_bits_source = dcache_auto_out_c_bits_source; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_c_bits_address = dcache_auto_out_c_bits_address; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_c_bits_data = dcache_auto_out_c_bits_data; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_c_bits_corrupt = dcache_auto_out_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_d_ready = dcache_auto_out_d_ready; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_e_valid = dcache_auto_out_e_valid; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_in_0_e_bits_sink = dcache_auto_out_e_bits_sink; // @[LazyModule.scala 375:16]
  assign tlMasterXbar_auto_out_a_ready = auto_tl_other_masters_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_b_valid = auto_tl_other_masters_out_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_b_bits_opcode = auto_tl_other_masters_out_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_b_bits_param = auto_tl_other_masters_out_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_b_bits_size = auto_tl_other_masters_out_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_b_bits_source = auto_tl_other_masters_out_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_b_bits_address = auto_tl_other_masters_out_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_b_bits_mask = auto_tl_other_masters_out_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_b_bits_data = auto_tl_other_masters_out_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_b_bits_corrupt = auto_tl_other_masters_out_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_c_ready = auto_tl_other_masters_out_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_d_valid = auto_tl_other_masters_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_d_bits_opcode = auto_tl_other_masters_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_d_bits_param = auto_tl_other_masters_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_d_bits_size = auto_tl_other_masters_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_d_bits_source = auto_tl_other_masters_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_d_bits_sink = auto_tl_other_masters_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_d_bits_denied = auto_tl_other_masters_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_d_bits_data = auto_tl_other_masters_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_d_bits_corrupt = auto_tl_other_masters_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tlMasterXbar_auto_out_e_ready = auto_tl_other_masters_out_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign broadcast_auto_in = auto_hartid_in; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign broadcast_1_auto_in = auto_reset_vector_in; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign broadcast_3_auto_in_rnmi = auto_nmi_in_rnmi; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign broadcast_3_auto_in_rnmi_interrupt_vector = auto_nmi_in_rnmi_interrupt_vector; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign broadcast_3_auto_in_rnmi_exception_vector = auto_nmi_in_rnmi_exception_vector; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign broadcast_4_auto_in_0_valid = core_io_trace_0_valid; // @[BundleBridge.scala 66:109 RocketTile.scala 187:32]
  assign broadcast_4_auto_in_0_iaddr = core_io_trace_0_iaddr; // @[BundleBridge.scala 66:109 RocketTile.scala 187:32]
  assign broadcast_4_auto_in_0_insn = core_io_trace_0_insn; // @[BundleBridge.scala 66:109 RocketTile.scala 187:32]
  assign broadcast_4_auto_in_0_priv = core_io_trace_0_priv; // @[BundleBridge.scala 66:109 RocketTile.scala 187:32]
  assign broadcast_4_auto_in_0_exception = core_io_trace_0_exception; // @[BundleBridge.scala 66:109 RocketTile.scala 187:32]
  assign broadcast_4_auto_in_0_interrupt = core_io_trace_0_interrupt; // @[BundleBridge.scala 66:109 RocketTile.scala 187:32]
  assign nexus_1_auto_in_stall = auto_trace_aux_in_stall; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign broadcast_5_auto_in_0_valid_0 = core_io_bpwatch_0_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_0_action = core_io_bpwatch_0_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_1_valid_0 = core_io_bpwatch_1_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_1_action = core_io_bpwatch_1_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_2_valid_0 = core_io_bpwatch_2_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_2_action = core_io_bpwatch_2_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_3_valid_0 = core_io_bpwatch_3_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_3_action = core_io_bpwatch_3_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_4_valid_0 = core_io_bpwatch_4_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_4_action = core_io_bpwatch_4_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_5_valid_0 = core_io_bpwatch_5_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_5_action = core_io_bpwatch_5_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_6_valid_0 = core_io_bpwatch_6_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_6_action = core_io_bpwatch_6_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_7_valid_0 = core_io_bpwatch_7_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_7_action = core_io_bpwatch_7_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_8_valid_0 = core_io_bpwatch_8_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_8_action = core_io_bpwatch_8_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_9_valid_0 = core_io_bpwatch_9_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_9_action = core_io_bpwatch_9_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_10_valid_0 = core_io_bpwatch_10_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_10_action = core_io_bpwatch_10_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_11_valid_0 = core_io_bpwatch_11_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_11_action = core_io_bpwatch_11_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_12_valid_0 = core_io_bpwatch_12_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_12_action = core_io_bpwatch_12_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_13_valid_0 = core_io_bpwatch_13_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_13_action = core_io_bpwatch_13_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_14_valid_0 = core_io_bpwatch_14_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_14_action = core_io_bpwatch_14_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_15_valid_0 = core_io_bpwatch_15_valid_0; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign broadcast_5_auto_in_15_action = core_io_bpwatch_15_action; // @[BundleBridge.scala 66:109 RocketTile.scala 189:34]
  assign dcache_clock = clock;
  assign dcache_reset = reset;
  assign dcache_auto_out_a_ready = tlMasterXbar_auto_in_0_a_ready; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_b_valid = tlMasterXbar_auto_in_0_b_valid; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_b_bits_opcode = tlMasterXbar_auto_in_0_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_b_bits_param = tlMasterXbar_auto_in_0_b_bits_param; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_b_bits_size = tlMasterXbar_auto_in_0_b_bits_size; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_b_bits_source = tlMasterXbar_auto_in_0_b_bits_source; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_b_bits_address = tlMasterXbar_auto_in_0_b_bits_address; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_b_bits_mask = tlMasterXbar_auto_in_0_b_bits_mask; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_b_bits_data = tlMasterXbar_auto_in_0_b_bits_data; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_b_bits_corrupt = tlMasterXbar_auto_in_0_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_c_ready = tlMasterXbar_auto_in_0_c_ready; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_d_valid = tlMasterXbar_auto_in_0_d_valid; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_d_bits_opcode = tlMasterXbar_auto_in_0_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_d_bits_param = tlMasterXbar_auto_in_0_d_bits_param; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_d_bits_size = tlMasterXbar_auto_in_0_d_bits_size; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_d_bits_source = tlMasterXbar_auto_in_0_d_bits_source; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_d_bits_sink = tlMasterXbar_auto_in_0_d_bits_sink; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_d_bits_denied = tlMasterXbar_auto_in_0_d_bits_denied; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_d_bits_data = tlMasterXbar_auto_in_0_d_bits_data; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_d_bits_corrupt = tlMasterXbar_auto_in_0_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign dcache_auto_out_e_ready = tlMasterXbar_auto_in_0_e_ready; // @[LazyModule.scala 375:16]
  assign dcache_io_cpu_req_valid = dcacheArb_io_mem_req_valid; // @[HellaCache.scala 303:30]
  assign dcache_io_cpu_req_bits_addr = dcacheArb_io_mem_req_bits_addr; // @[HellaCache.scala 303:30]
  assign dcache_io_cpu_req_bits_idx = dcacheArb_io_mem_req_bits_idx; // @[HellaCache.scala 303:30]
  assign dcache_io_cpu_req_bits_tag = dcacheArb_io_mem_req_bits_tag; // @[HellaCache.scala 303:30]
  assign dcache_io_cpu_req_bits_cmd = dcacheArb_io_mem_req_bits_cmd; // @[HellaCache.scala 303:30]
  assign dcache_io_cpu_req_bits_size = dcacheArb_io_mem_req_bits_size; // @[HellaCache.scala 303:30]
  assign dcache_io_cpu_req_bits_signed = dcacheArb_io_mem_req_bits_signed; // @[HellaCache.scala 303:30]
  assign dcache_io_cpu_req_bits_dprv = dcacheArb_io_mem_req_bits_dprv; // @[HellaCache.scala 303:30]
  assign dcache_io_cpu_req_bits_phys = dcacheArb_io_mem_req_bits_phys; // @[HellaCache.scala 303:30]
  assign dcache_io_cpu_req_bits_no_alloc = dcacheArb_io_mem_req_bits_no_alloc; // @[HellaCache.scala 303:30]
  assign dcache_io_cpu_req_bits_no_xcpt = dcacheArb_io_mem_req_bits_no_xcpt; // @[HellaCache.scala 303:30]
  assign dcache_io_cpu_req_bits_mask = dcacheArb_io_mem_req_bits_mask; // @[HellaCache.scala 303:30]
  assign dcache_io_cpu_s1_kill = dcacheArb_io_mem_s1_kill; // @[HellaCache.scala 303:30]
  assign dcache_io_cpu_s1_data_data = dcacheArb_io_mem_s1_data_data; // @[HellaCache.scala 303:30]
  assign dcache_io_cpu_s1_data_mask = dcacheArb_io_mem_s1_data_mask; // @[HellaCache.scala 303:30]
  assign dcache_io_cpu_keep_clock_enabled = dcacheArb_io_mem_keep_clock_enabled; // @[HellaCache.scala 303:30]
  assign dcache_io_ptw_req_ready = ptw_io_requestor_0_req_ready; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_resp_valid = ptw_io_requestor_0_resp_valid; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_resp_bits_ae = ptw_io_requestor_0_resp_bits_ae; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_resp_bits_pte_ppn = ptw_io_requestor_0_resp_bits_pte_ppn; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_resp_bits_pte_d = ptw_io_requestor_0_resp_bits_pte_d; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_resp_bits_pte_a = ptw_io_requestor_0_resp_bits_pte_a; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_resp_bits_pte_g = ptw_io_requestor_0_resp_bits_pte_g; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_resp_bits_pte_u = ptw_io_requestor_0_resp_bits_pte_u; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_resp_bits_pte_x = ptw_io_requestor_0_resp_bits_pte_x; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_resp_bits_pte_w = ptw_io_requestor_0_resp_bits_pte_w; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_resp_bits_pte_r = ptw_io_requestor_0_resp_bits_pte_r; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_resp_bits_pte_v = ptw_io_requestor_0_resp_bits_pte_v; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_resp_bits_level = ptw_io_requestor_0_resp_bits_level; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_resp_bits_homogeneous = ptw_io_requestor_0_resp_bits_homogeneous; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_ptbr_mode = ptw_io_requestor_0_ptbr_mode; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_status_debug = ptw_io_requestor_0_status_debug; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_status_dprv = ptw_io_requestor_0_status_dprv; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_status_mxr = ptw_io_requestor_0_status_mxr; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_status_sum = ptw_io_requestor_0_status_sum; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_0_cfg_l = ptw_io_requestor_0_pmp_0_cfg_l; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_0_cfg_a = ptw_io_requestor_0_pmp_0_cfg_a; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_0_cfg_x = ptw_io_requestor_0_pmp_0_cfg_x; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_0_cfg_w = ptw_io_requestor_0_pmp_0_cfg_w; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_0_cfg_r = ptw_io_requestor_0_pmp_0_cfg_r; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_0_addr = ptw_io_requestor_0_pmp_0_addr; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_0_mask = ptw_io_requestor_0_pmp_0_mask; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_1_cfg_l = ptw_io_requestor_0_pmp_1_cfg_l; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_1_cfg_a = ptw_io_requestor_0_pmp_1_cfg_a; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_1_cfg_x = ptw_io_requestor_0_pmp_1_cfg_x; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_1_cfg_w = ptw_io_requestor_0_pmp_1_cfg_w; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_1_cfg_r = ptw_io_requestor_0_pmp_1_cfg_r; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_1_addr = ptw_io_requestor_0_pmp_1_addr; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_1_mask = ptw_io_requestor_0_pmp_1_mask; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_2_cfg_l = ptw_io_requestor_0_pmp_2_cfg_l; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_2_cfg_a = ptw_io_requestor_0_pmp_2_cfg_a; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_2_cfg_x = ptw_io_requestor_0_pmp_2_cfg_x; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_2_cfg_w = ptw_io_requestor_0_pmp_2_cfg_w; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_2_cfg_r = ptw_io_requestor_0_pmp_2_cfg_r; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_2_addr = ptw_io_requestor_0_pmp_2_addr; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_2_mask = ptw_io_requestor_0_pmp_2_mask; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_3_cfg_l = ptw_io_requestor_0_pmp_3_cfg_l; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_3_cfg_a = ptw_io_requestor_0_pmp_3_cfg_a; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_3_cfg_x = ptw_io_requestor_0_pmp_3_cfg_x; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_3_cfg_w = ptw_io_requestor_0_pmp_3_cfg_w; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_3_cfg_r = ptw_io_requestor_0_pmp_3_cfg_r; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_3_addr = ptw_io_requestor_0_pmp_3_addr; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_3_mask = ptw_io_requestor_0_pmp_3_mask; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_4_cfg_l = ptw_io_requestor_0_pmp_4_cfg_l; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_4_cfg_a = ptw_io_requestor_0_pmp_4_cfg_a; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_4_cfg_x = ptw_io_requestor_0_pmp_4_cfg_x; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_4_cfg_w = ptw_io_requestor_0_pmp_4_cfg_w; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_4_cfg_r = ptw_io_requestor_0_pmp_4_cfg_r; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_4_addr = ptw_io_requestor_0_pmp_4_addr; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_4_mask = ptw_io_requestor_0_pmp_4_mask; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_5_cfg_l = ptw_io_requestor_0_pmp_5_cfg_l; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_5_cfg_a = ptw_io_requestor_0_pmp_5_cfg_a; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_5_cfg_x = ptw_io_requestor_0_pmp_5_cfg_x; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_5_cfg_w = ptw_io_requestor_0_pmp_5_cfg_w; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_5_cfg_r = ptw_io_requestor_0_pmp_5_cfg_r; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_5_addr = ptw_io_requestor_0_pmp_5_addr; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_5_mask = ptw_io_requestor_0_pmp_5_mask; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_6_cfg_l = ptw_io_requestor_0_pmp_6_cfg_l; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_6_cfg_a = ptw_io_requestor_0_pmp_6_cfg_a; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_6_cfg_x = ptw_io_requestor_0_pmp_6_cfg_x; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_6_cfg_w = ptw_io_requestor_0_pmp_6_cfg_w; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_6_cfg_r = ptw_io_requestor_0_pmp_6_cfg_r; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_6_addr = ptw_io_requestor_0_pmp_6_addr; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_6_mask = ptw_io_requestor_0_pmp_6_mask; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_7_cfg_l = ptw_io_requestor_0_pmp_7_cfg_l; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_7_cfg_a = ptw_io_requestor_0_pmp_7_cfg_a; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_7_cfg_x = ptw_io_requestor_0_pmp_7_cfg_x; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_7_cfg_w = ptw_io_requestor_0_pmp_7_cfg_w; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_7_cfg_r = ptw_io_requestor_0_pmp_7_cfg_r; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_7_addr = ptw_io_requestor_0_pmp_7_addr; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_pmp_7_mask = ptw_io_requestor_0_pmp_7_mask; // @[RocketTile.scala 220:20]
  assign dcache_io_ptw_customCSRs_csrs_1_value = ptw_io_requestor_0_customCSRs_csrs_1_value; // @[RocketTile.scala 220:20]
  assign frontend_clock = clock;
  assign frontend_reset = reset;
  assign frontend_auto_icache_master_out_a_ready = tlMasterXbar_auto_in_1_a_ready; // @[LazyModule.scala 375:16]
  assign frontend_auto_icache_master_out_d_valid = tlMasterXbar_auto_in_1_d_valid; // @[LazyModule.scala 375:16]
  assign frontend_auto_icache_master_out_d_bits_opcode = tlMasterXbar_auto_in_1_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign frontend_auto_icache_master_out_d_bits_size = tlMasterXbar_auto_in_1_d_bits_size; // @[LazyModule.scala 375:16]
  assign frontend_auto_icache_master_out_d_bits_data = tlMasterXbar_auto_in_1_d_bits_data; // @[LazyModule.scala 375:16]
  assign frontend_auto_icache_master_out_d_bits_corrupt = tlMasterXbar_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign frontend_auto_reset_vector_sink_in = broadcast_1_auto_out_1; // @[LazyModule.scala 377:16]
  assign frontend_io_cpu_might_request = core_io_imem_might_request; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_req_valid = core_io_imem_req_valid; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_req_bits_pc = core_io_imem_req_bits_pc; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_req_bits_speculative = core_io_imem_req_bits_speculative; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_sfence_valid = core_io_imem_sfence_valid; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_sfence_bits_rs1 = core_io_imem_sfence_bits_rs1; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_sfence_bits_rs2 = core_io_imem_sfence_bits_rs2; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_sfence_bits_addr = core_io_imem_sfence_bits_addr; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_sfence_bits_asid = core_io_imem_sfence_bits_asid; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_resp_ready = core_io_imem_resp_ready; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_btb_update_valid = core_io_imem_btb_update_valid; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_btb_update_bits_prediction_entry = core_io_imem_btb_update_bits_prediction_entry; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_btb_update_bits_pc = core_io_imem_btb_update_bits_pc; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_btb_update_bits_isValid = core_io_imem_btb_update_bits_isValid; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_btb_update_bits_br_pc = core_io_imem_btb_update_bits_br_pc; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_btb_update_bits_cfiType = core_io_imem_btb_update_bits_cfiType; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_bht_update_valid = core_io_imem_bht_update_valid; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_bht_update_bits_prediction_history = core_io_imem_bht_update_bits_prediction_history; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_bht_update_bits_pc = core_io_imem_bht_update_bits_pc; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_bht_update_bits_branch = core_io_imem_bht_update_bits_branch; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_bht_update_bits_taken = core_io_imem_bht_update_bits_taken; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_bht_update_bits_mispredict = core_io_imem_bht_update_bits_mispredict; // @[RocketTile.scala 195:32]
  assign frontend_io_cpu_flush_icache = core_io_imem_flush_icache; // @[RocketTile.scala 195:32]
  assign frontend_io_ptw_req_ready = ptw_io_requestor_1_req_ready; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_resp_valid = ptw_io_requestor_1_resp_valid; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_resp_bits_ae = ptw_io_requestor_1_resp_bits_ae; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_resp_bits_pte_ppn = ptw_io_requestor_1_resp_bits_pte_ppn; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_resp_bits_pte_d = ptw_io_requestor_1_resp_bits_pte_d; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_resp_bits_pte_a = ptw_io_requestor_1_resp_bits_pte_a; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_resp_bits_pte_g = ptw_io_requestor_1_resp_bits_pte_g; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_resp_bits_pte_u = ptw_io_requestor_1_resp_bits_pte_u; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_resp_bits_pte_x = ptw_io_requestor_1_resp_bits_pte_x; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_resp_bits_pte_w = ptw_io_requestor_1_resp_bits_pte_w; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_resp_bits_pte_r = ptw_io_requestor_1_resp_bits_pte_r; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_resp_bits_pte_v = ptw_io_requestor_1_resp_bits_pte_v; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_resp_bits_level = ptw_io_requestor_1_resp_bits_level; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_resp_bits_homogeneous = ptw_io_requestor_1_resp_bits_homogeneous; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_ptbr_mode = ptw_io_requestor_1_ptbr_mode; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_status_debug = ptw_io_requestor_1_status_debug; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_status_prv = ptw_io_requestor_1_status_prv; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_0_cfg_l = ptw_io_requestor_1_pmp_0_cfg_l; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_0_cfg_a = ptw_io_requestor_1_pmp_0_cfg_a; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_0_cfg_x = ptw_io_requestor_1_pmp_0_cfg_x; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_0_cfg_w = ptw_io_requestor_1_pmp_0_cfg_w; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_0_cfg_r = ptw_io_requestor_1_pmp_0_cfg_r; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_0_addr = ptw_io_requestor_1_pmp_0_addr; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_0_mask = ptw_io_requestor_1_pmp_0_mask; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_1_cfg_l = ptw_io_requestor_1_pmp_1_cfg_l; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_1_cfg_a = ptw_io_requestor_1_pmp_1_cfg_a; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_1_cfg_x = ptw_io_requestor_1_pmp_1_cfg_x; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_1_cfg_w = ptw_io_requestor_1_pmp_1_cfg_w; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_1_cfg_r = ptw_io_requestor_1_pmp_1_cfg_r; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_1_addr = ptw_io_requestor_1_pmp_1_addr; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_1_mask = ptw_io_requestor_1_pmp_1_mask; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_2_cfg_l = ptw_io_requestor_1_pmp_2_cfg_l; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_2_cfg_a = ptw_io_requestor_1_pmp_2_cfg_a; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_2_cfg_x = ptw_io_requestor_1_pmp_2_cfg_x; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_2_cfg_w = ptw_io_requestor_1_pmp_2_cfg_w; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_2_cfg_r = ptw_io_requestor_1_pmp_2_cfg_r; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_2_addr = ptw_io_requestor_1_pmp_2_addr; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_2_mask = ptw_io_requestor_1_pmp_2_mask; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_3_cfg_l = ptw_io_requestor_1_pmp_3_cfg_l; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_3_cfg_a = ptw_io_requestor_1_pmp_3_cfg_a; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_3_cfg_x = ptw_io_requestor_1_pmp_3_cfg_x; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_3_cfg_w = ptw_io_requestor_1_pmp_3_cfg_w; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_3_cfg_r = ptw_io_requestor_1_pmp_3_cfg_r; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_3_addr = ptw_io_requestor_1_pmp_3_addr; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_3_mask = ptw_io_requestor_1_pmp_3_mask; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_4_cfg_l = ptw_io_requestor_1_pmp_4_cfg_l; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_4_cfg_a = ptw_io_requestor_1_pmp_4_cfg_a; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_4_cfg_x = ptw_io_requestor_1_pmp_4_cfg_x; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_4_cfg_w = ptw_io_requestor_1_pmp_4_cfg_w; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_4_cfg_r = ptw_io_requestor_1_pmp_4_cfg_r; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_4_addr = ptw_io_requestor_1_pmp_4_addr; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_4_mask = ptw_io_requestor_1_pmp_4_mask; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_5_cfg_l = ptw_io_requestor_1_pmp_5_cfg_l; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_5_cfg_a = ptw_io_requestor_1_pmp_5_cfg_a; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_5_cfg_x = ptw_io_requestor_1_pmp_5_cfg_x; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_5_cfg_w = ptw_io_requestor_1_pmp_5_cfg_w; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_5_cfg_r = ptw_io_requestor_1_pmp_5_cfg_r; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_5_addr = ptw_io_requestor_1_pmp_5_addr; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_5_mask = ptw_io_requestor_1_pmp_5_mask; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_6_cfg_l = ptw_io_requestor_1_pmp_6_cfg_l; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_6_cfg_a = ptw_io_requestor_1_pmp_6_cfg_a; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_6_cfg_x = ptw_io_requestor_1_pmp_6_cfg_x; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_6_cfg_w = ptw_io_requestor_1_pmp_6_cfg_w; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_6_cfg_r = ptw_io_requestor_1_pmp_6_cfg_r; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_6_addr = ptw_io_requestor_1_pmp_6_addr; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_6_mask = ptw_io_requestor_1_pmp_6_mask; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_7_cfg_l = ptw_io_requestor_1_pmp_7_cfg_l; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_7_cfg_a = ptw_io_requestor_1_pmp_7_cfg_a; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_7_cfg_x = ptw_io_requestor_1_pmp_7_cfg_x; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_7_cfg_w = ptw_io_requestor_1_pmp_7_cfg_w; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_7_cfg_r = ptw_io_requestor_1_pmp_7_cfg_r; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_7_addr = ptw_io_requestor_1_pmp_7_addr; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_pmp_7_mask = ptw_io_requestor_1_pmp_7_mask; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_customCSRs_csrs_0_wen = ptw_io_requestor_1_customCSRs_csrs_0_wen; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_customCSRs_csrs_0_value = ptw_io_requestor_1_customCSRs_csrs_0_value; // @[RocketTile.scala 220:20]
  assign frontend_io_ptw_customCSRs_csrs_1_value = ptw_io_requestor_1_customCSRs_csrs_1_value; // @[RocketTile.scala 220:20]
  assign fpuOpt_clock = clock;
  assign fpuOpt_reset = reset;
  assign fpuOpt_io_inst = core_io_fpu_inst; // @[RocketTile.scala 197:39]
  assign fpuOpt_io_fromint_data = core_io_fpu_fromint_data; // @[RocketTile.scala 197:39]
  assign fpuOpt_io_fcsr_rm = core_io_fpu_fcsr_rm; // @[RocketTile.scala 197:39]
  assign fpuOpt_io_dmem_resp_val = core_io_fpu_dmem_resp_val; // @[RocketTile.scala 197:39]
  assign fpuOpt_io_dmem_resp_type = core_io_fpu_dmem_resp_type; // @[RocketTile.scala 197:39]
  assign fpuOpt_io_dmem_resp_tag = core_io_fpu_dmem_resp_tag; // @[RocketTile.scala 197:39]
  assign fpuOpt_io_dmem_resp_data = core_io_fpu_dmem_resp_data; // @[RocketTile.scala 197:39]
  assign fpuOpt_io_valid = core_io_fpu_valid; // @[RocketTile.scala 197:39]
  assign fpuOpt_io_killx = core_io_fpu_killx; // @[RocketTile.scala 197:39]
  assign fpuOpt_io_killm = core_io_fpu_killm; // @[RocketTile.scala 197:39]
  assign fpuOpt_io_keep_clock_enabled = core_io_fpu_keep_clock_enabled; // @[RocketTile.scala 197:39]
  assign dcacheArb_clock = clock;
  assign dcacheArb_io_requestor_0_req_valid = ptw_io_mem_req_valid; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_0_req_bits_addr = ptw_io_mem_req_bits_addr; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_0_req_bits_idx = ptw_io_mem_req_bits_idx; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_0_s1_kill = ptw_io_mem_s1_kill; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_req_valid = core_io_dmem_req_valid; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_req_bits_addr = core_io_dmem_req_bits_addr; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_req_bits_idx = core_io_dmem_req_bits_idx; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_req_bits_tag = core_io_dmem_req_bits_tag; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_req_bits_cmd = core_io_dmem_req_bits_cmd; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_req_bits_size = core_io_dmem_req_bits_size; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_req_bits_signed = core_io_dmem_req_bits_signed; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_req_bits_dprv = core_io_dmem_req_bits_dprv; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_req_bits_phys = core_io_dmem_req_bits_phys; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_req_bits_no_alloc = core_io_dmem_req_bits_no_alloc; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_req_bits_no_xcpt = core_io_dmem_req_bits_no_xcpt; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_req_bits_mask = core_io_dmem_req_bits_mask; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_s1_kill = core_io_dmem_s1_kill; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_s1_data_data = core_io_dmem_s1_data_data; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_s1_data_mask = core_io_dmem_s1_data_mask; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_requestor_1_keep_clock_enabled = core_io_dmem_keep_clock_enabled; // @[RocketTile.scala 219:26]
  assign dcacheArb_io_mem_req_ready = dcache_io_cpu_req_ready; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_s2_nack = dcache_io_cpu_s2_nack; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_resp_valid = dcache_io_cpu_resp_valid; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_resp_bits_addr = dcache_io_cpu_resp_bits_addr; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_resp_bits_tag = dcache_io_cpu_resp_bits_tag; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_resp_bits_cmd = dcache_io_cpu_resp_bits_cmd; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_resp_bits_size = dcache_io_cpu_resp_bits_size; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_resp_bits_signed = dcache_io_cpu_resp_bits_signed; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_resp_bits_data = dcache_io_cpu_resp_bits_data; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_resp_bits_replay = dcache_io_cpu_resp_bits_replay; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_resp_bits_has_data = dcache_io_cpu_resp_bits_has_data; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_resp_bits_data_word_bypass = dcache_io_cpu_resp_bits_data_word_bypass; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_replay_next = dcache_io_cpu_replay_next; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_s2_xcpt_ma_ld = dcache_io_cpu_s2_xcpt_ma_ld; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_s2_xcpt_ma_st = dcache_io_cpu_s2_xcpt_ma_st; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_s2_xcpt_pf_ld = dcache_io_cpu_s2_xcpt_pf_ld; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_s2_xcpt_pf_st = dcache_io_cpu_s2_xcpt_pf_st; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_s2_xcpt_ae_ld = dcache_io_cpu_s2_xcpt_ae_ld; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_s2_xcpt_ae_st = dcache_io_cpu_s2_xcpt_ae_st; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_ordered = dcache_io_cpu_ordered; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_perf_acquire = dcache_io_cpu_perf_acquire; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_perf_release = dcache_io_cpu_perf_release; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_perf_grant = dcache_io_cpu_perf_grant; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_perf_tlbMiss = dcache_io_cpu_perf_tlbMiss; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_perf_blocked = dcache_io_cpu_perf_blocked; // @[HellaCache.scala 303:30]
  assign dcacheArb_io_mem_clock_enabled = dcache_io_cpu_clock_enabled; // @[HellaCache.scala 303:30]
  assign ptw_clock = clock;
  assign ptw_reset = reset;
  assign ptw_io_requestor_0_req_valid = dcache_io_ptw_req_valid; // @[RocketTile.scala 220:20]
  assign ptw_io_requestor_0_req_bits_bits_addr = dcache_io_ptw_req_bits_bits_addr; // @[RocketTile.scala 220:20]
  assign ptw_io_requestor_1_req_valid = frontend_io_ptw_req_valid; // @[RocketTile.scala 220:20]
  assign ptw_io_requestor_1_req_bits_valid = frontend_io_ptw_req_bits_valid; // @[RocketTile.scala 220:20]
  assign ptw_io_requestor_1_req_bits_bits_addr = frontend_io_ptw_req_bits_bits_addr; // @[RocketTile.scala 220:20]
  assign ptw_io_mem_req_ready = dcacheArb_io_requestor_0_req_ready; // @[RocketTile.scala 219:26]
  assign ptw_io_mem_s2_nack = dcacheArb_io_requestor_0_s2_nack; // @[RocketTile.scala 219:26]
  assign ptw_io_mem_resp_valid = dcacheArb_io_requestor_0_resp_valid; // @[RocketTile.scala 219:26]
  assign ptw_io_mem_resp_bits_data = dcacheArb_io_requestor_0_resp_bits_data; // @[RocketTile.scala 219:26]
  assign ptw_io_mem_s2_xcpt_ae_ld = dcacheArb_io_requestor_0_s2_xcpt_ae_ld; // @[RocketTile.scala 219:26]
  assign ptw_io_dpath_ptbr_mode = core_io_ptw_ptbr_mode; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_ptbr_ppn = core_io_ptw_ptbr_ppn; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_sfence_valid = core_io_ptw_sfence_valid; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_sfence_bits_rs1 = core_io_ptw_sfence_bits_rs1; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_sfence_bits_rs2 = core_io_ptw_sfence_bits_rs2; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_sfence_bits_addr = core_io_ptw_sfence_bits_addr; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_status_debug = core_io_ptw_status_debug; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_status_dprv = core_io_ptw_status_dprv; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_status_prv = core_io_ptw_status_prv; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_status_mxr = core_io_ptw_status_mxr; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_status_sum = core_io_ptw_status_sum; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_0_cfg_l = core_io_ptw_pmp_0_cfg_l; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_0_cfg_a = core_io_ptw_pmp_0_cfg_a; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_0_cfg_x = core_io_ptw_pmp_0_cfg_x; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_0_cfg_w = core_io_ptw_pmp_0_cfg_w; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_0_cfg_r = core_io_ptw_pmp_0_cfg_r; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_0_addr = core_io_ptw_pmp_0_addr; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_0_mask = core_io_ptw_pmp_0_mask; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_1_cfg_l = core_io_ptw_pmp_1_cfg_l; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_1_cfg_a = core_io_ptw_pmp_1_cfg_a; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_1_cfg_x = core_io_ptw_pmp_1_cfg_x; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_1_cfg_w = core_io_ptw_pmp_1_cfg_w; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_1_cfg_r = core_io_ptw_pmp_1_cfg_r; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_1_addr = core_io_ptw_pmp_1_addr; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_1_mask = core_io_ptw_pmp_1_mask; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_2_cfg_l = core_io_ptw_pmp_2_cfg_l; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_2_cfg_a = core_io_ptw_pmp_2_cfg_a; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_2_cfg_x = core_io_ptw_pmp_2_cfg_x; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_2_cfg_w = core_io_ptw_pmp_2_cfg_w; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_2_cfg_r = core_io_ptw_pmp_2_cfg_r; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_2_addr = core_io_ptw_pmp_2_addr; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_2_mask = core_io_ptw_pmp_2_mask; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_3_cfg_l = core_io_ptw_pmp_3_cfg_l; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_3_cfg_a = core_io_ptw_pmp_3_cfg_a; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_3_cfg_x = core_io_ptw_pmp_3_cfg_x; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_3_cfg_w = core_io_ptw_pmp_3_cfg_w; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_3_cfg_r = core_io_ptw_pmp_3_cfg_r; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_3_addr = core_io_ptw_pmp_3_addr; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_3_mask = core_io_ptw_pmp_3_mask; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_4_cfg_l = core_io_ptw_pmp_4_cfg_l; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_4_cfg_a = core_io_ptw_pmp_4_cfg_a; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_4_cfg_x = core_io_ptw_pmp_4_cfg_x; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_4_cfg_w = core_io_ptw_pmp_4_cfg_w; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_4_cfg_r = core_io_ptw_pmp_4_cfg_r; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_4_addr = core_io_ptw_pmp_4_addr; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_4_mask = core_io_ptw_pmp_4_mask; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_5_cfg_l = core_io_ptw_pmp_5_cfg_l; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_5_cfg_a = core_io_ptw_pmp_5_cfg_a; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_5_cfg_x = core_io_ptw_pmp_5_cfg_x; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_5_cfg_w = core_io_ptw_pmp_5_cfg_w; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_5_cfg_r = core_io_ptw_pmp_5_cfg_r; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_5_addr = core_io_ptw_pmp_5_addr; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_5_mask = core_io_ptw_pmp_5_mask; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_6_cfg_l = core_io_ptw_pmp_6_cfg_l; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_6_cfg_a = core_io_ptw_pmp_6_cfg_a; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_6_cfg_x = core_io_ptw_pmp_6_cfg_x; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_6_cfg_w = core_io_ptw_pmp_6_cfg_w; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_6_cfg_r = core_io_ptw_pmp_6_cfg_r; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_6_addr = core_io_ptw_pmp_6_addr; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_6_mask = core_io_ptw_pmp_6_mask; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_7_cfg_l = core_io_ptw_pmp_7_cfg_l; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_7_cfg_a = core_io_ptw_pmp_7_cfg_a; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_7_cfg_x = core_io_ptw_pmp_7_cfg_x; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_7_cfg_w = core_io_ptw_pmp_7_cfg_w; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_7_cfg_r = core_io_ptw_pmp_7_cfg_r; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_7_addr = core_io_ptw_pmp_7_addr; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_pmp_7_mask = core_io_ptw_pmp_7_mask; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_customCSRs_csrs_0_wen = core_io_ptw_customCSRs_csrs_0_wen; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_customCSRs_csrs_0_value = core_io_ptw_customCSRs_csrs_0_value; // @[RocketTile.scala 198:15]
  assign ptw_io_dpath_customCSRs_csrs_1_value = core_io_ptw_customCSRs_csrs_1_value; // @[RocketTile.scala 198:15]
  assign core_clock = clock;
  assign core_reset = reset;
  assign core_io_hartid = broadcast_auto_out; // @[BundleBridge.scala 43:11 LazyModule.scala 375:16]
  assign core_io_interrupts_debug = auto_int_sink_in_0_0; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_mtip = auto_int_sink_in_1_1; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_msip = auto_int_sink_in_1_0; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_meip = auto_int_sink_in_2_0; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_seip = auto_int_sink_in_2_1; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_0 = auto_int_sink_in_3_0; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_1 = auto_int_sink_in_3_1; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_2 = auto_int_sink_in_3_2; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_3 = auto_int_sink_in_3_3; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_4 = auto_int_sink_in_3_4; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_5 = auto_int_sink_in_3_5; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_6 = auto_int_sink_in_3_6; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_7 = auto_int_sink_in_3_7; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_8 = auto_int_sink_in_3_8; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_9 = auto_int_sink_in_3_9; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_10 = auto_int_sink_in_3_10; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_11 = auto_int_sink_in_3_11; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_12 = auto_int_sink_in_3_12; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_13 = auto_int_sink_in_3_13; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_14 = auto_int_sink_in_3_14; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_lip_15 = auto_int_sink_in_3_15; // @[Nodes.scala 28:99 LazyModule.scala 388:16]
  assign core_io_interrupts_nmi_rnmi = broadcast_3_auto_out_rnmi; // @[BundleBridge.scala 43:11 LazyModule.scala 375:16]
  assign core_io_interrupts_nmi_rnmi_interrupt_vector = broadcast_3_auto_out_rnmi_interrupt_vector; // @[BundleBridge.scala 43:11 LazyModule.scala 375:16]
  assign core_io_interrupts_nmi_rnmi_exception_vector = broadcast_3_auto_out_rnmi_exception_vector; // @[BundleBridge.scala 43:11 LazyModule.scala 375:16]
  assign core_io_imem_resp_valid = frontend_io_cpu_resp_valid; // @[RocketTile.scala 195:32]
  assign core_io_imem_resp_bits_btb_taken = frontend_io_cpu_resp_bits_btb_taken; // @[RocketTile.scala 195:32]
  assign core_io_imem_resp_bits_btb_bridx = frontend_io_cpu_resp_bits_btb_bridx; // @[RocketTile.scala 195:32]
  assign core_io_imem_resp_bits_btb_entry = frontend_io_cpu_resp_bits_btb_entry; // @[RocketTile.scala 195:32]
  assign core_io_imem_resp_bits_btb_bht_history = frontend_io_cpu_resp_bits_btb_bht_history; // @[RocketTile.scala 195:32]
  assign core_io_imem_resp_bits_pc = frontend_io_cpu_resp_bits_pc; // @[RocketTile.scala 195:32]
  assign core_io_imem_resp_bits_data = frontend_io_cpu_resp_bits_data; // @[RocketTile.scala 195:32]
  assign core_io_imem_resp_bits_mask = frontend_io_cpu_resp_bits_mask; // @[RocketTile.scala 195:32]
  assign core_io_imem_resp_bits_xcpt_pf_inst = frontend_io_cpu_resp_bits_xcpt_pf_inst; // @[RocketTile.scala 195:32]
  assign core_io_imem_resp_bits_xcpt_ae_inst = frontend_io_cpu_resp_bits_xcpt_ae_inst; // @[RocketTile.scala 195:32]
  assign core_io_imem_resp_bits_replay = frontend_io_cpu_resp_bits_replay; // @[RocketTile.scala 195:32]
  assign core_io_imem_perf_acquire = frontend_io_cpu_perf_acquire; // @[RocketTile.scala 195:32]
  assign core_io_imem_perf_tlbMiss = frontend_io_cpu_perf_tlbMiss; // @[RocketTile.scala 195:32]
  assign core_io_dmem_req_ready = dcacheArb_io_requestor_1_req_ready; // @[RocketTile.scala 219:26]
  assign core_io_dmem_s2_nack = dcacheArb_io_requestor_1_s2_nack; // @[RocketTile.scala 219:26]
  assign core_io_dmem_resp_valid = dcacheArb_io_requestor_1_resp_valid; // @[RocketTile.scala 219:26]
  assign core_io_dmem_resp_bits_addr = dcacheArb_io_requestor_1_resp_bits_addr; // @[RocketTile.scala 219:26]
  assign core_io_dmem_resp_bits_tag = dcacheArb_io_requestor_1_resp_bits_tag; // @[RocketTile.scala 219:26]
  assign core_io_dmem_resp_bits_cmd = dcacheArb_io_requestor_1_resp_bits_cmd; // @[RocketTile.scala 219:26]
  assign core_io_dmem_resp_bits_size = dcacheArb_io_requestor_1_resp_bits_size; // @[RocketTile.scala 219:26]
  assign core_io_dmem_resp_bits_signed = dcacheArb_io_requestor_1_resp_bits_signed; // @[RocketTile.scala 219:26]
  assign core_io_dmem_resp_bits_data = dcacheArb_io_requestor_1_resp_bits_data; // @[RocketTile.scala 219:26]
  assign core_io_dmem_resp_bits_replay = dcacheArb_io_requestor_1_resp_bits_replay; // @[RocketTile.scala 219:26]
  assign core_io_dmem_resp_bits_has_data = dcacheArb_io_requestor_1_resp_bits_has_data; // @[RocketTile.scala 219:26]
  assign core_io_dmem_resp_bits_data_word_bypass = dcacheArb_io_requestor_1_resp_bits_data_word_bypass; // @[RocketTile.scala 219:26]
  assign core_io_dmem_replay_next = dcacheArb_io_requestor_1_replay_next; // @[RocketTile.scala 219:26]
  assign core_io_dmem_s2_xcpt_ma_ld = dcacheArb_io_requestor_1_s2_xcpt_ma_ld; // @[RocketTile.scala 219:26]
  assign core_io_dmem_s2_xcpt_ma_st = dcacheArb_io_requestor_1_s2_xcpt_ma_st; // @[RocketTile.scala 219:26]
  assign core_io_dmem_s2_xcpt_pf_ld = dcacheArb_io_requestor_1_s2_xcpt_pf_ld; // @[RocketTile.scala 219:26]
  assign core_io_dmem_s2_xcpt_pf_st = dcacheArb_io_requestor_1_s2_xcpt_pf_st; // @[RocketTile.scala 219:26]
  assign core_io_dmem_s2_xcpt_ae_ld = dcacheArb_io_requestor_1_s2_xcpt_ae_ld; // @[RocketTile.scala 219:26]
  assign core_io_dmem_s2_xcpt_ae_st = dcacheArb_io_requestor_1_s2_xcpt_ae_st; // @[RocketTile.scala 219:26]
  assign core_io_dmem_ordered = dcacheArb_io_requestor_1_ordered; // @[RocketTile.scala 219:26]
  assign core_io_dmem_perf_acquire = dcacheArb_io_requestor_1_perf_acquire; // @[RocketTile.scala 219:26]
  assign core_io_dmem_perf_release = dcacheArb_io_requestor_1_perf_release; // @[RocketTile.scala 219:26]
  assign core_io_dmem_perf_grant = dcacheArb_io_requestor_1_perf_grant; // @[RocketTile.scala 219:26]
  assign core_io_dmem_perf_tlbMiss = dcacheArb_io_requestor_1_perf_tlbMiss; // @[RocketTile.scala 219:26]
  assign core_io_dmem_perf_blocked = dcacheArb_io_requestor_1_perf_blocked; // @[RocketTile.scala 219:26]
  assign core_io_dmem_clock_enabled = dcacheArb_io_requestor_1_clock_enabled; // @[RocketTile.scala 219:26]
  assign core_io_ptw_perf_l2miss = ptw_io_dpath_perf_l2miss; // @[RocketTile.scala 198:15]
  assign core_io_fpu_fcsr_flags_valid = fpuOpt_io_fcsr_flags_valid; // @[RocketTile.scala 197:39]
  assign core_io_fpu_fcsr_flags_bits = fpuOpt_io_fcsr_flags_bits; // @[RocketTile.scala 197:39]
  assign core_io_fpu_store_data = fpuOpt_io_store_data; // @[RocketTile.scala 197:39]
  assign core_io_fpu_toint_data = fpuOpt_io_toint_data; // @[RocketTile.scala 197:39]
  assign core_io_fpu_fcsr_rdy = fpuOpt_io_fcsr_rdy; // @[RocketTile.scala 197:39]
  assign core_io_fpu_nack_mem = fpuOpt_io_nack_mem; // @[RocketTile.scala 197:39]
  assign core_io_fpu_illegal_rm = fpuOpt_io_illegal_rm; // @[RocketTile.scala 197:39]
  assign core_io_fpu_dec_ldst = fpuOpt_io_dec_ldst; // @[RocketTile.scala 197:39]
  assign core_io_fpu_dec_wen = fpuOpt_io_dec_wen; // @[RocketTile.scala 197:39]
  assign core_io_fpu_dec_ren1 = fpuOpt_io_dec_ren1; // @[RocketTile.scala 197:39]
  assign core_io_fpu_dec_ren2 = fpuOpt_io_dec_ren2; // @[RocketTile.scala 197:39]
  assign core_io_fpu_dec_ren3 = fpuOpt_io_dec_ren3; // @[RocketTile.scala 197:39]
  assign core_io_fpu_dec_swap23 = fpuOpt_io_dec_swap23; // @[RocketTile.scala 197:39]
  assign core_io_fpu_dec_fma = fpuOpt_io_dec_fma; // @[RocketTile.scala 197:39]
  assign core_io_fpu_dec_div = fpuOpt_io_dec_div; // @[RocketTile.scala 197:39]
  assign core_io_fpu_dec_sqrt = fpuOpt_io_dec_sqrt; // @[RocketTile.scala 197:39]
  assign core_io_fpu_sboard_set = fpuOpt_io_sboard_set; // @[RocketTile.scala 197:39]
  assign core_io_fpu_sboard_clr = fpuOpt_io_sboard_clr; // @[RocketTile.scala 197:39]
  assign core_io_fpu_sboard_clra = fpuOpt_io_sboard_clra; // @[RocketTile.scala 197:39]
  assign core_io_traceStall = nexus_1_auto_out_stall; // @[BundleBridge.scala 43:11 LazyModule.scala 375:16]
  assign dcache_psd_test_clock_enable = psd_test_clock_enable;
  assign frontend_psd_test_clock_enable = psd_test_clock_enable;
  assign fpuOpt_psd_test_clock_enable = psd_test_clock_enable;
  assign ptw_psd_test_clock_enable = psd_test_clock_enable;
  assign core_psd_test_clock_enable = psd_test_clock_enable;
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleOut_0_0_cease_count <= 4'h0;
    end else if (reset) begin
      bundleOut_0_0_cease_count <= 4'h0;
    end else if (_T_5 & _bundleOut_0_0_cease_T_1) begin
      bundleOut_0_0_cease_count <= _bundleOut_0_0_cease_count_T_1;
    end else if (~_T_5) begin
      bundleOut_0_0_cease_count <= 4'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleOut_0_0_REG <= 1'h0;
    end else if (reset) begin
      bundleOut_0_0_REG <= 1'h0;
    end else begin
      bundleOut_0_0_REG <= core_io_wfi;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  bundleOut_0_0_cease_count = _RAND_0[3:0];
  _RAND_1 = {1{`RANDOM}};
  bundleOut_0_0_REG = _RAND_1[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    bundleOut_0_0_cease_count = 4'h0;
  end
  if (rf_reset) begin
    bundleOut_0_0_REG = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
