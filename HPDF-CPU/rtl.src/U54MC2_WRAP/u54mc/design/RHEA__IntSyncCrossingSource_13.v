//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__IntSyncCrossingSource_13(
  input   auto_in_0,
  input   auto_in_1,
  input   auto_in_2,
  input   auto_in_3,
  input   auto_in_4,
  input   auto_in_5,
  input   auto_in_6,
  input   auto_in_7,
  input   auto_in_8,
  input   auto_in_9,
  input   auto_in_10,
  input   auto_in_11,
  input   auto_in_12,
  input   auto_in_13,
  input   auto_in_14,
  input   auto_in_15,
  output  auto_out_sync_0,
  output  auto_out_sync_1,
  output  auto_out_sync_2,
  output  auto_out_sync_3,
  output  auto_out_sync_4,
  output  auto_out_sync_5,
  output  auto_out_sync_6,
  output  auto_out_sync_7,
  output  auto_out_sync_8,
  output  auto_out_sync_9,
  output  auto_out_sync_10,
  output  auto_out_sync_11,
  output  auto_out_sync_12,
  output  auto_out_sync_13,
  output  auto_out_sync_14,
  output  auto_out_sync_15
);
  assign auto_out_sync_0 = auto_in_0; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
  assign auto_out_sync_1 = auto_in_1; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
  assign auto_out_sync_2 = auto_in_2; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
  assign auto_out_sync_3 = auto_in_3; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
  assign auto_out_sync_4 = auto_in_4; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
  assign auto_out_sync_5 = auto_in_5; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
  assign auto_out_sync_6 = auto_in_6; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
  assign auto_out_sync_7 = auto_in_7; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
  assign auto_out_sync_8 = auto_in_8; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
  assign auto_out_sync_9 = auto_in_9; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
  assign auto_out_sync_10 = auto_in_10; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
  assign auto_out_sync_11 = auto_in_11; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
  assign auto_out_sync_12 = auto_in_12; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
  assign auto_out_sync_13 = auto_in_13; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
  assign auto_out_sync_14 = auto_in_14; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
  assign auto_out_sync_15 = auto_in_15; // @[Nodes.scala 76:11 LazyModule.scala 388:16]
endmodule
