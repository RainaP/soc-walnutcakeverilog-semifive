//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLWidthWidget_1(
  input          rf_reset,
  input          clock,
  input          reset,
  output         auto_in_a_ready,
  input          auto_in_a_valid,
  input  [2:0]   auto_in_a_bits_opcode,
  input  [3:0]   auto_in_a_bits_size,
  input  [5:0]   auto_in_a_bits_source,
  input  [36:0]  auto_in_a_bits_address,
  input          auto_in_a_bits_user_amba_prot_bufferable,
  input          auto_in_a_bits_user_amba_prot_modifiable,
  input          auto_in_a_bits_user_amba_prot_readalloc,
  input          auto_in_a_bits_user_amba_prot_writealloc,
  input          auto_in_a_bits_user_amba_prot_privileged,
  input          auto_in_a_bits_user_amba_prot_secure,
  input          auto_in_a_bits_user_amba_prot_fetch,
  input  [7:0]   auto_in_a_bits_mask,
  input  [63:0]  auto_in_a_bits_data,
  input          auto_in_d_ready,
  output         auto_in_d_valid,
  output [2:0]   auto_in_d_bits_opcode,
  output [1:0]   auto_in_d_bits_param,
  output [3:0]   auto_in_d_bits_size,
  output [5:0]   auto_in_d_bits_source,
  output [4:0]   auto_in_d_bits_sink,
  output         auto_in_d_bits_denied,
  output [63:0]  auto_in_d_bits_data,
  output         auto_in_d_bits_corrupt,
  input          auto_out_a_ready,
  output         auto_out_a_valid,
  output [2:0]   auto_out_a_bits_opcode,
  output [3:0]   auto_out_a_bits_size,
  output [5:0]   auto_out_a_bits_source,
  output [36:0]  auto_out_a_bits_address,
  output         auto_out_a_bits_user_amba_prot_bufferable,
  output         auto_out_a_bits_user_amba_prot_modifiable,
  output         auto_out_a_bits_user_amba_prot_readalloc,
  output         auto_out_a_bits_user_amba_prot_writealloc,
  output         auto_out_a_bits_user_amba_prot_privileged,
  output         auto_out_a_bits_user_amba_prot_secure,
  output         auto_out_a_bits_user_amba_prot_fetch,
  output [15:0]  auto_out_a_bits_mask,
  output [127:0] auto_out_a_bits_data,
  output         auto_out_d_ready,
  input          auto_out_d_valid,
  input  [2:0]   auto_out_d_bits_opcode,
  input  [1:0]   auto_out_d_bits_param,
  input  [3:0]   auto_out_d_bits_size,
  input  [5:0]   auto_out_d_bits_source,
  input  [4:0]   auto_out_d_bits_sink,
  input          auto_out_d_bits_denied,
  input  [127:0] auto_out_d_bits_data,
  input          auto_out_d_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [63:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
`endif // RANDOMIZE_REG_INIT
  wire  repeated_repeater_rf_reset; // @[Repeater.scala 35:26]
  wire  repeated_repeater_clock; // @[Repeater.scala 35:26]
  wire  repeated_repeater_reset; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_repeat; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_enq_ready; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_enq_valid; // @[Repeater.scala 35:26]
  wire [2:0] repeated_repeater_io_enq_bits_opcode; // @[Repeater.scala 35:26]
  wire [1:0] repeated_repeater_io_enq_bits_param; // @[Repeater.scala 35:26]
  wire [3:0] repeated_repeater_io_enq_bits_size; // @[Repeater.scala 35:26]
  wire [5:0] repeated_repeater_io_enq_bits_source; // @[Repeater.scala 35:26]
  wire [4:0] repeated_repeater_io_enq_bits_sink; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_enq_bits_denied; // @[Repeater.scala 35:26]
  wire [127:0] repeated_repeater_io_enq_bits_data; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_enq_bits_corrupt; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_deq_ready; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_deq_valid; // @[Repeater.scala 35:26]
  wire [2:0] repeated_repeater_io_deq_bits_opcode; // @[Repeater.scala 35:26]
  wire [1:0] repeated_repeater_io_deq_bits_param; // @[Repeater.scala 35:26]
  wire [3:0] repeated_repeater_io_deq_bits_size; // @[Repeater.scala 35:26]
  wire [5:0] repeated_repeater_io_deq_bits_source; // @[Repeater.scala 35:26]
  wire [4:0] repeated_repeater_io_deq_bits_sink; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_deq_bits_denied; // @[Repeater.scala 35:26]
  wire [127:0] repeated_repeater_io_deq_bits_data; // @[Repeater.scala 35:26]
  wire  repeated_repeater_io_deq_bits_corrupt; // @[Repeater.scala 35:26]
  wire  hasData = ~auto_in_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [18:0] _limit_T_1 = 19'hf << auto_in_a_bits_size; // @[package.scala 234:77]
  wire [3:0] _limit_T_3 = ~_limit_T_1[3:0]; // @[package.scala 234:46]
  wire  limit = _limit_T_3[3]; // @[WidthWidget.scala 32:47]
  reg  count; // @[WidthWidget.scala 34:27]
  wire  last = count == limit | ~hasData; // @[WidthWidget.scala 36:36]
  wire  enable_0 = ~(|(count & limit)); // @[WidthWidget.scala 37:47]
  wire  _bundleIn_0_a_ready_T = ~last; // @[WidthWidget.scala 70:32]
  wire  bundleIn_0_a_ready = auto_out_a_ready | _bundleIn_0_a_ready_T; // @[WidthWidget.scala 70:29]
  wire  _T = bundleIn_0_a_ready & auto_in_a_valid; // @[Decoupled.scala 40:37]
  reg  bundleOut_0_a_bits_data_rdata_written_once; // @[WidthWidget.scala 56:41]
  wire  bundleOut_0_a_bits_data_masked_enable_0 = enable_0 | ~bundleOut_0_a_bits_data_rdata_written_once; // @[WidthWidget.scala 57:42]
  reg [63:0] bundleOut_0_a_bits_data_rdata_0; // @[WidthWidget.scala 60:24]
  wire [63:0] bundleOut_0_a_bits_data_lo = bundleOut_0_a_bits_data_masked_enable_0 ? auto_in_a_bits_data :
    bundleOut_0_a_bits_data_rdata_0; // @[WidthWidget.scala 62:88]
  wire [1:0] bundleOut_0_a_bits_mask_sizeOH_shiftAmount = auto_in_a_bits_size[1:0]; // @[OneHot.scala 64:49]
  wire [3:0] _bundleOut_0_a_bits_mask_sizeOH_T_1 = 4'h1 << bundleOut_0_a_bits_mask_sizeOH_shiftAmount; // @[OneHot.scala 65:12]
  wire [3:0] bundleOut_0_a_bits_mask_sizeOH = _bundleOut_0_a_bits_mask_sizeOH_T_1 | 4'h1; // @[Misc.scala 207:81]
  wire  _bundleOut_0_a_bits_mask_T = auto_in_a_bits_size >= 4'h4; // @[Misc.scala 211:21]
  wire  bundleOut_0_a_bits_mask_size = bundleOut_0_a_bits_mask_sizeOH[3]; // @[Misc.scala 214:26]
  wire  bundleOut_0_a_bits_mask_bit = auto_in_a_bits_address[3]; // @[Misc.scala 215:26]
  wire  bundleOut_0_a_bits_mask_nbit = ~bundleOut_0_a_bits_mask_bit; // @[Misc.scala 216:20]
  wire  bundleOut_0_a_bits_mask_acc = _bundleOut_0_a_bits_mask_T | bundleOut_0_a_bits_mask_size &
    bundleOut_0_a_bits_mask_nbit; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_acc_1 = _bundleOut_0_a_bits_mask_T | bundleOut_0_a_bits_mask_size &
    bundleOut_0_a_bits_mask_bit; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_size_1 = bundleOut_0_a_bits_mask_sizeOH[2]; // @[Misc.scala 214:26]
  wire  bundleOut_0_a_bits_mask_bit_1 = auto_in_a_bits_address[2]; // @[Misc.scala 215:26]
  wire  bundleOut_0_a_bits_mask_nbit_1 = ~bundleOut_0_a_bits_mask_bit_1; // @[Misc.scala 216:20]
  wire  bundleOut_0_a_bits_mask_eq_2 = bundleOut_0_a_bits_mask_nbit & bundleOut_0_a_bits_mask_nbit_1; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_2 = bundleOut_0_a_bits_mask_acc | bundleOut_0_a_bits_mask_size_1 &
    bundleOut_0_a_bits_mask_eq_2; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_3 = bundleOut_0_a_bits_mask_nbit & bundleOut_0_a_bits_mask_bit_1; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_3 = bundleOut_0_a_bits_mask_acc | bundleOut_0_a_bits_mask_size_1 &
    bundleOut_0_a_bits_mask_eq_3; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_4 = bundleOut_0_a_bits_mask_bit & bundleOut_0_a_bits_mask_nbit_1; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_4 = bundleOut_0_a_bits_mask_acc_1 | bundleOut_0_a_bits_mask_size_1 &
    bundleOut_0_a_bits_mask_eq_4; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_5 = bundleOut_0_a_bits_mask_bit & bundleOut_0_a_bits_mask_bit_1; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_5 = bundleOut_0_a_bits_mask_acc_1 | bundleOut_0_a_bits_mask_size_1 &
    bundleOut_0_a_bits_mask_eq_5; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_size_2 = bundleOut_0_a_bits_mask_sizeOH[1]; // @[Misc.scala 214:26]
  wire  bundleOut_0_a_bits_mask_bit_2 = auto_in_a_bits_address[1]; // @[Misc.scala 215:26]
  wire  bundleOut_0_a_bits_mask_nbit_2 = ~bundleOut_0_a_bits_mask_bit_2; // @[Misc.scala 216:20]
  wire  bundleOut_0_a_bits_mask_eq_6 = bundleOut_0_a_bits_mask_eq_2 & bundleOut_0_a_bits_mask_nbit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_6 = bundleOut_0_a_bits_mask_acc_2 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_6; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_7 = bundleOut_0_a_bits_mask_eq_2 & bundleOut_0_a_bits_mask_bit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_7 = bundleOut_0_a_bits_mask_acc_2 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_7; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_8 = bundleOut_0_a_bits_mask_eq_3 & bundleOut_0_a_bits_mask_nbit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_8 = bundleOut_0_a_bits_mask_acc_3 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_8; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_9 = bundleOut_0_a_bits_mask_eq_3 & bundleOut_0_a_bits_mask_bit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_9 = bundleOut_0_a_bits_mask_acc_3 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_9; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_10 = bundleOut_0_a_bits_mask_eq_4 & bundleOut_0_a_bits_mask_nbit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_10 = bundleOut_0_a_bits_mask_acc_4 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_10; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_11 = bundleOut_0_a_bits_mask_eq_4 & bundleOut_0_a_bits_mask_bit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_11 = bundleOut_0_a_bits_mask_acc_4 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_11; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_12 = bundleOut_0_a_bits_mask_eq_5 & bundleOut_0_a_bits_mask_nbit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_12 = bundleOut_0_a_bits_mask_acc_5 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_12; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_13 = bundleOut_0_a_bits_mask_eq_5 & bundleOut_0_a_bits_mask_bit_2; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_acc_13 = bundleOut_0_a_bits_mask_acc_5 | bundleOut_0_a_bits_mask_size_2 &
    bundleOut_0_a_bits_mask_eq_13; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_size_3 = bundleOut_0_a_bits_mask_sizeOH[0]; // @[Misc.scala 214:26]
  wire  bundleOut_0_a_bits_mask_bit_3 = auto_in_a_bits_address[0]; // @[Misc.scala 215:26]
  wire  bundleOut_0_a_bits_mask_nbit_3 = ~bundleOut_0_a_bits_mask_bit_3; // @[Misc.scala 216:20]
  wire  bundleOut_0_a_bits_mask_eq_14 = bundleOut_0_a_bits_mask_eq_6 & bundleOut_0_a_bits_mask_nbit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_lo_lo_lo = bundleOut_0_a_bits_mask_acc_6 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_14; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_15 = bundleOut_0_a_bits_mask_eq_6 & bundleOut_0_a_bits_mask_bit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_lo_lo_hi = bundleOut_0_a_bits_mask_acc_6 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_15; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_16 = bundleOut_0_a_bits_mask_eq_7 & bundleOut_0_a_bits_mask_nbit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_lo_hi_lo = bundleOut_0_a_bits_mask_acc_7 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_16; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_17 = bundleOut_0_a_bits_mask_eq_7 & bundleOut_0_a_bits_mask_bit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_lo_hi_hi = bundleOut_0_a_bits_mask_acc_7 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_17; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_18 = bundleOut_0_a_bits_mask_eq_8 & bundleOut_0_a_bits_mask_nbit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_hi_lo_lo = bundleOut_0_a_bits_mask_acc_8 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_18; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_19 = bundleOut_0_a_bits_mask_eq_8 & bundleOut_0_a_bits_mask_bit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_hi_lo_hi = bundleOut_0_a_bits_mask_acc_8 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_19; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_20 = bundleOut_0_a_bits_mask_eq_9 & bundleOut_0_a_bits_mask_nbit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_hi_hi_lo = bundleOut_0_a_bits_mask_acc_9 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_20; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_21 = bundleOut_0_a_bits_mask_eq_9 & bundleOut_0_a_bits_mask_bit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_lo_hi_hi_hi = bundleOut_0_a_bits_mask_acc_9 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_21; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_22 = bundleOut_0_a_bits_mask_eq_10 & bundleOut_0_a_bits_mask_nbit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_lo_lo_lo = bundleOut_0_a_bits_mask_acc_10 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_22; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_23 = bundleOut_0_a_bits_mask_eq_10 & bundleOut_0_a_bits_mask_bit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_lo_lo_hi = bundleOut_0_a_bits_mask_acc_10 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_23; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_24 = bundleOut_0_a_bits_mask_eq_11 & bundleOut_0_a_bits_mask_nbit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_lo_hi_lo = bundleOut_0_a_bits_mask_acc_11 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_24; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_25 = bundleOut_0_a_bits_mask_eq_11 & bundleOut_0_a_bits_mask_bit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_lo_hi_hi = bundleOut_0_a_bits_mask_acc_11 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_25; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_26 = bundleOut_0_a_bits_mask_eq_12 & bundleOut_0_a_bits_mask_nbit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_hi_lo_lo = bundleOut_0_a_bits_mask_acc_12 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_26; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_27 = bundleOut_0_a_bits_mask_eq_12 & bundleOut_0_a_bits_mask_bit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_hi_lo_hi = bundleOut_0_a_bits_mask_acc_12 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_27; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_28 = bundleOut_0_a_bits_mask_eq_13 & bundleOut_0_a_bits_mask_nbit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_hi_hi_lo = bundleOut_0_a_bits_mask_acc_13 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_28; // @[Misc.scala 220:29]
  wire  bundleOut_0_a_bits_mask_eq_29 = bundleOut_0_a_bits_mask_eq_13 & bundleOut_0_a_bits_mask_bit_3; // @[Misc.scala 219:27]
  wire  bundleOut_0_a_bits_mask_hi_hi_hi_hi = bundleOut_0_a_bits_mask_acc_13 | bundleOut_0_a_bits_mask_size_3 &
    bundleOut_0_a_bits_mask_eq_29; // @[Misc.scala 220:29]
  wire [7:0] bundleOut_0_a_bits_mask_lo = {bundleOut_0_a_bits_mask_lo_hi_hi_hi,bundleOut_0_a_bits_mask_lo_hi_hi_lo,
    bundleOut_0_a_bits_mask_lo_hi_lo_hi,bundleOut_0_a_bits_mask_lo_hi_lo_lo,bundleOut_0_a_bits_mask_lo_lo_hi_hi,
    bundleOut_0_a_bits_mask_lo_lo_hi_lo,bundleOut_0_a_bits_mask_lo_lo_lo_hi,bundleOut_0_a_bits_mask_lo_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [15:0] _bundleOut_0_a_bits_mask_T_1 = {bundleOut_0_a_bits_mask_hi_hi_hi_hi,bundleOut_0_a_bits_mask_hi_hi_hi_lo,
    bundleOut_0_a_bits_mask_hi_hi_lo_hi,bundleOut_0_a_bits_mask_hi_hi_lo_lo,bundleOut_0_a_bits_mask_hi_lo_hi_hi,
    bundleOut_0_a_bits_mask_hi_lo_hi_lo,bundleOut_0_a_bits_mask_hi_lo_lo_hi,bundleOut_0_a_bits_mask_hi_lo_lo_lo,
    bundleOut_0_a_bits_mask_lo}; // @[Cat.scala 30:58]
  reg  bundleOut_0_a_bits_mask_rdata_written_once; // @[WidthWidget.scala 56:41]
  wire  bundleOut_0_a_bits_mask_masked_enable_0 = enable_0 | ~bundleOut_0_a_bits_mask_rdata_written_once; // @[WidthWidget.scala 57:42]
  reg [7:0] bundleOut_0_a_bits_mask_rdata_0; // @[WidthWidget.scala 60:24]
  wire [7:0] bundleOut_0_a_bits_mask_lo_1 = bundleOut_0_a_bits_mask_masked_enable_0 ? auto_in_a_bits_mask :
    bundleOut_0_a_bits_mask_rdata_0; // @[WidthWidget.scala 62:88]
  wire [15:0] _bundleOut_0_a_bits_mask_T_5 = {auto_in_a_bits_mask,bundleOut_0_a_bits_mask_lo_1}; // @[Cat.scala 30:58]
  wire [15:0] _bundleOut_0_a_bits_mask_T_7 = hasData ? _bundleOut_0_a_bits_mask_T_5 : 16'hffff; // @[WidthWidget.scala 79:93]
  wire [63:0] cated_bits_data_hi = repeated_repeater_io_deq_bits_data[127:64]; // @[WidthWidget.scala 158:37]
  wire [63:0] cated_bits_data_lo = auto_out_d_bits_data[63:0]; // @[WidthWidget.scala 159:31]
  wire [127:0] cated_bits_data = {cated_bits_data_hi,cated_bits_data_lo}; // @[Cat.scala 30:58]
  wire [2:0] cated_bits_opcode = repeated_repeater_io_deq_bits_opcode; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  wire  repeat_hasData = cated_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [3:0] cated_bits_size = repeated_repeater_io_deq_bits_size; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  wire [18:0] _repeat_limit_T_1 = 19'hf << cated_bits_size; // @[package.scala 234:77]
  wire [3:0] _repeat_limit_T_3 = ~_repeat_limit_T_1[3:0]; // @[package.scala 234:46]
  wire  repeat_limit = _repeat_limit_T_3[3]; // @[WidthWidget.scala 97:47]
  reg  repeat_count; // @[WidthWidget.scala 99:26]
  wire  repeat_first = ~repeat_count; // @[WidthWidget.scala 100:25]
  wire  repeat_last = repeat_count == repeat_limit | ~repeat_hasData; // @[WidthWidget.scala 101:35]
  wire  cated_valid = repeated_repeater_io_deq_valid; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  wire  _repeat_T = auto_in_d_ready & cated_valid; // @[Decoupled.scala 40:37]
  reg  repeat_sel_sel_sources_0; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_1; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_2; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_3; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_4; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_5; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_6; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_7; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_8; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_9; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_10; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_11; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_12; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_13; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_14; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_15; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_16; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_17; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_18; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_19; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_20; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_21; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_22; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_23; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_24; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_25; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_26; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_27; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_28; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_29; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_30; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_31; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_32; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_33; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_34; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_35; // @[WidthWidget.scala 180:27]
  reg  repeat_sel_sel_sources_36; // @[WidthWidget.scala 180:27]
  wire [5:0] cated_bits_source = repeated_repeater_io_deq_bits_source; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  reg  repeat_sel_hold_r; // @[Reg.scala 15:16]
  wire  _GEN_85 = 6'h1 == cated_bits_source ? repeat_sel_sel_sources_1 : repeat_sel_sel_sources_0; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_86 = 6'h2 == cated_bits_source ? repeat_sel_sel_sources_2 : _GEN_85; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_87 = 6'h3 == cated_bits_source ? repeat_sel_sel_sources_3 : _GEN_86; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_88 = 6'h4 == cated_bits_source ? repeat_sel_sel_sources_4 : _GEN_87; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_89 = 6'h5 == cated_bits_source ? repeat_sel_sel_sources_5 : _GEN_88; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_90 = 6'h6 == cated_bits_source ? repeat_sel_sel_sources_6 : _GEN_89; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_91 = 6'h7 == cated_bits_source ? repeat_sel_sel_sources_7 : _GEN_90; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_92 = 6'h8 == cated_bits_source ? repeat_sel_sel_sources_8 : _GEN_91; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_93 = 6'h9 == cated_bits_source ? repeat_sel_sel_sources_9 : _GEN_92; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_94 = 6'ha == cated_bits_source ? repeat_sel_sel_sources_10 : _GEN_93; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_95 = 6'hb == cated_bits_source ? repeat_sel_sel_sources_11 : _GEN_94; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_96 = 6'hc == cated_bits_source ? repeat_sel_sel_sources_12 : _GEN_95; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_97 = 6'hd == cated_bits_source ? repeat_sel_sel_sources_13 : _GEN_96; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_98 = 6'he == cated_bits_source ? repeat_sel_sel_sources_14 : _GEN_97; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_99 = 6'hf == cated_bits_source ? repeat_sel_sel_sources_15 : _GEN_98; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_100 = 6'h10 == cated_bits_source ? repeat_sel_sel_sources_16 : _GEN_99; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_101 = 6'h11 == cated_bits_source ? repeat_sel_sel_sources_17 : _GEN_100; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_102 = 6'h12 == cated_bits_source ? repeat_sel_sel_sources_18 : _GEN_101; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_103 = 6'h13 == cated_bits_source ? repeat_sel_sel_sources_19 : _GEN_102; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_104 = 6'h14 == cated_bits_source ? repeat_sel_sel_sources_20 : _GEN_103; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_105 = 6'h15 == cated_bits_source ? repeat_sel_sel_sources_21 : _GEN_104; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_106 = 6'h16 == cated_bits_source ? repeat_sel_sel_sources_22 : _GEN_105; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_107 = 6'h17 == cated_bits_source ? repeat_sel_sel_sources_23 : _GEN_106; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_108 = 6'h18 == cated_bits_source ? repeat_sel_sel_sources_24 : _GEN_107; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_109 = 6'h19 == cated_bits_source ? repeat_sel_sel_sources_25 : _GEN_108; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_110 = 6'h1a == cated_bits_source ? repeat_sel_sel_sources_26 : _GEN_109; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_111 = 6'h1b == cated_bits_source ? repeat_sel_sel_sources_27 : _GEN_110; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_112 = 6'h1c == cated_bits_source ? repeat_sel_sel_sources_28 : _GEN_111; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_113 = 6'h1d == cated_bits_source ? repeat_sel_sel_sources_29 : _GEN_112; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_114 = 6'h1e == cated_bits_source ? repeat_sel_sel_sources_30 : _GEN_113; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_115 = 6'h1f == cated_bits_source ? repeat_sel_sel_sources_31 : _GEN_114; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_116 = 6'h20 == cated_bits_source ? repeat_sel_sel_sources_32 : _GEN_115; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_117 = 6'h21 == cated_bits_source ? repeat_sel_sel_sources_33 : _GEN_116; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_118 = 6'h22 == cated_bits_source ? repeat_sel_sel_sources_34 : _GEN_117; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_119 = 6'h23 == cated_bits_source ? repeat_sel_sel_sources_35 : _GEN_118; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_120 = 6'h24 == cated_bits_source ? repeat_sel_sel_sources_36 : _GEN_119; // @[Reg.scala 16:23 Reg.scala 16:23]
  wire  _GEN_121 = repeat_first ? _GEN_120 : repeat_sel_hold_r; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire  repeat_sel = _GEN_121 & ~repeat_limit; // @[WidthWidget.scala 116:16]
  wire  repeat_index = repeat_sel | repeat_count; // @[WidthWidget.scala 120:24]
  wire [63:0] repeat_bundleIn_0_d_bits_data_mux_0 = cated_bits_data[63:0]; // @[WidthWidget.scala 122:55]
  wire [63:0] repeat_bundleIn_0_d_bits_data_mux_1 = cated_bits_data[127:64]; // @[WidthWidget.scala 122:55]
  RHEA__Repeater_2 repeated_repeater ( // @[Repeater.scala 35:26]
    .rf_reset(repeated_repeater_rf_reset),
    .clock(repeated_repeater_clock),
    .reset(repeated_repeater_reset),
    .io_repeat(repeated_repeater_io_repeat),
    .io_enq_ready(repeated_repeater_io_enq_ready),
    .io_enq_valid(repeated_repeater_io_enq_valid),
    .io_enq_bits_opcode(repeated_repeater_io_enq_bits_opcode),
    .io_enq_bits_param(repeated_repeater_io_enq_bits_param),
    .io_enq_bits_size(repeated_repeater_io_enq_bits_size),
    .io_enq_bits_source(repeated_repeater_io_enq_bits_source),
    .io_enq_bits_sink(repeated_repeater_io_enq_bits_sink),
    .io_enq_bits_denied(repeated_repeater_io_enq_bits_denied),
    .io_enq_bits_data(repeated_repeater_io_enq_bits_data),
    .io_enq_bits_corrupt(repeated_repeater_io_enq_bits_corrupt),
    .io_deq_ready(repeated_repeater_io_deq_ready),
    .io_deq_valid(repeated_repeater_io_deq_valid),
    .io_deq_bits_opcode(repeated_repeater_io_deq_bits_opcode),
    .io_deq_bits_param(repeated_repeater_io_deq_bits_param),
    .io_deq_bits_size(repeated_repeater_io_deq_bits_size),
    .io_deq_bits_source(repeated_repeater_io_deq_bits_source),
    .io_deq_bits_sink(repeated_repeater_io_deq_bits_sink),
    .io_deq_bits_denied(repeated_repeater_io_deq_bits_denied),
    .io_deq_bits_data(repeated_repeater_io_deq_bits_data),
    .io_deq_bits_corrupt(repeated_repeater_io_deq_bits_corrupt)
  );
  assign repeated_repeater_rf_reset = rf_reset;
  assign auto_in_a_ready = auto_out_a_ready | _bundleIn_0_a_ready_T; // @[WidthWidget.scala 70:29]
  assign auto_in_d_valid = repeated_repeater_io_deq_valid; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  assign auto_in_d_bits_opcode = repeated_repeater_io_deq_bits_opcode; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  assign auto_in_d_bits_param = repeated_repeater_io_deq_bits_param; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  assign auto_in_d_bits_size = repeated_repeater_io_deq_bits_size; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  assign auto_in_d_bits_source = repeated_repeater_io_deq_bits_source; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  assign auto_in_d_bits_sink = repeated_repeater_io_deq_bits_sink; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  assign auto_in_d_bits_denied = repeated_repeater_io_deq_bits_denied; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  assign auto_in_d_bits_data = repeat_index ? repeat_bundleIn_0_d_bits_data_mux_1 : repeat_bundleIn_0_d_bits_data_mux_0; // @[WidthWidget.scala 131:30 WidthWidget.scala 131:30]
  assign auto_in_d_bits_corrupt = repeated_repeater_io_deq_bits_corrupt; // @[WidthWidget.scala 155:25 WidthWidget.scala 156:15]
  assign auto_out_a_valid = auto_in_a_valid & last; // @[WidthWidget.scala 71:29]
  assign auto_out_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_size = auto_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_source = auto_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_bufferable = auto_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_modifiable = auto_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_readalloc = auto_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_writealloc = auto_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_privileged = auto_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_secure = auto_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_user_amba_prot_fetch = auto_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_mask = _bundleOut_0_a_bits_mask_T_1 & _bundleOut_0_a_bits_mask_T_7; // @[WidthWidget.scala 79:88]
  assign auto_out_a_bits_data = {auto_in_a_bits_data,bundleOut_0_a_bits_data_lo}; // @[Cat.scala 30:58]
  assign auto_out_d_ready = repeated_repeater_io_enq_ready; // @[Nodes.scala 1529:13 Repeater.scala 37:21]
  assign repeated_repeater_clock = clock;
  assign repeated_repeater_reset = reset;
  assign repeated_repeater_io_repeat = ~repeat_last; // @[WidthWidget.scala 142:7]
  assign repeated_repeater_io_enq_valid = auto_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_param = auto_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_size = auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_source = auto_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_sink = auto_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_denied = auto_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_data = auto_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_enq_bits_corrupt = auto_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign repeated_repeater_io_deq_ready = auto_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count <= 1'h0;
    end else if (_T) begin
      if (last) begin
        count <= 1'h0;
      end else begin
        count <= count + 1'h1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      bundleOut_0_a_bits_data_rdata_written_once <= 1'h0;
    end else begin
      bundleOut_0_a_bits_data_rdata_written_once <= _T & _bundleIn_0_a_ready_T |
        bundleOut_0_a_bits_data_rdata_written_once;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleOut_0_a_bits_data_rdata_0 <= 64'h0;
    end else if (_T & _bundleIn_0_a_ready_T) begin
      if (bundleOut_0_a_bits_data_masked_enable_0) begin
        bundleOut_0_a_bits_data_rdata_0 <= auto_in_a_bits_data;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      bundleOut_0_a_bits_mask_rdata_written_once <= 1'h0;
    end else begin
      bundleOut_0_a_bits_mask_rdata_written_once <= _T & _bundleIn_0_a_ready_T |
        bundleOut_0_a_bits_mask_rdata_written_once;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleOut_0_a_bits_mask_rdata_0 <= 8'h0;
    end else if (_T & _bundleIn_0_a_ready_T) begin
      if (bundleOut_0_a_bits_mask_masked_enable_0) begin
        bundleOut_0_a_bits_mask_rdata_0 <= auto_in_a_bits_mask;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      repeat_count <= 1'h0;
    end else if (_repeat_T) begin
      if (repeat_last) begin
        repeat_count <= 1'h0;
      end else begin
        repeat_count <= repeat_count + 1'h1;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_0 <= 1'h0;
    end else if (_T) begin
      if (6'h0 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_0 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_1 <= 1'h0;
    end else if (_T) begin
      if (6'h1 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_1 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_2 <= 1'h0;
    end else if (_T) begin
      if (6'h2 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_2 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_3 <= 1'h0;
    end else if (_T) begin
      if (6'h3 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_3 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_4 <= 1'h0;
    end else if (_T) begin
      if (6'h4 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_4 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_5 <= 1'h0;
    end else if (_T) begin
      if (6'h5 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_5 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_6 <= 1'h0;
    end else if (_T) begin
      if (6'h6 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_6 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_7 <= 1'h0;
    end else if (_T) begin
      if (6'h7 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_7 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_8 <= 1'h0;
    end else if (_T) begin
      if (6'h8 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_8 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_9 <= 1'h0;
    end else if (_T) begin
      if (6'h9 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_9 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_10 <= 1'h0;
    end else if (_T) begin
      if (6'ha == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_10 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_11 <= 1'h0;
    end else if (_T) begin
      if (6'hb == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_11 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_12 <= 1'h0;
    end else if (_T) begin
      if (6'hc == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_12 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_13 <= 1'h0;
    end else if (_T) begin
      if (6'hd == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_13 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_14 <= 1'h0;
    end else if (_T) begin
      if (6'he == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_14 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_15 <= 1'h0;
    end else if (_T) begin
      if (6'hf == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_15 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_16 <= 1'h0;
    end else if (_T) begin
      if (6'h10 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_16 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_17 <= 1'h0;
    end else if (_T) begin
      if (6'h11 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_17 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_18 <= 1'h0;
    end else if (_T) begin
      if (6'h12 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_18 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_19 <= 1'h0;
    end else if (_T) begin
      if (6'h13 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_19 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_20 <= 1'h0;
    end else if (_T) begin
      if (6'h14 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_20 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_21 <= 1'h0;
    end else if (_T) begin
      if (6'h15 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_21 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_22 <= 1'h0;
    end else if (_T) begin
      if (6'h16 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_22 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_23 <= 1'h0;
    end else if (_T) begin
      if (6'h17 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_23 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_24 <= 1'h0;
    end else if (_T) begin
      if (6'h18 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_24 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_25 <= 1'h0;
    end else if (_T) begin
      if (6'h19 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_25 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_26 <= 1'h0;
    end else if (_T) begin
      if (6'h1a == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_26 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_27 <= 1'h0;
    end else if (_T) begin
      if (6'h1b == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_27 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_28 <= 1'h0;
    end else if (_T) begin
      if (6'h1c == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_28 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_29 <= 1'h0;
    end else if (_T) begin
      if (6'h1d == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_29 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_30 <= 1'h0;
    end else if (_T) begin
      if (6'h1e == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_30 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_31 <= 1'h0;
    end else if (_T) begin
      if (6'h1f == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_31 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_32 <= 1'h0;
    end else if (_T) begin
      if (6'h20 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_32 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_33 <= 1'h0;
    end else if (_T) begin
      if (6'h21 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_33 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_34 <= 1'h0;
    end else if (_T) begin
      if (6'h22 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_34 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_35 <= 1'h0;
    end else if (_T) begin
      if (6'h23 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_35 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_sel_sources_36 <= 1'h0;
    end else if (_T) begin
      if (6'h24 == auto_in_a_bits_source) begin
        repeat_sel_sel_sources_36 <= bundleOut_0_a_bits_mask_bit;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      repeat_sel_hold_r <= 1'h0;
    end else if (repeat_first) begin
      if (6'h24 == cated_bits_source) begin
        repeat_sel_hold_r <= repeat_sel_sel_sources_36;
      end else if (6'h23 == cated_bits_source) begin
        repeat_sel_hold_r <= repeat_sel_sel_sources_35;
      end else if (6'h22 == cated_bits_source) begin
        repeat_sel_hold_r <= repeat_sel_sel_sources_34;
      end else begin
        repeat_sel_hold_r <= _GEN_117;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  count = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  bundleOut_0_a_bits_data_rdata_written_once = _RAND_1[0:0];
  _RAND_2 = {2{`RANDOM}};
  bundleOut_0_a_bits_data_rdata_0 = _RAND_2[63:0];
  _RAND_3 = {1{`RANDOM}};
  bundleOut_0_a_bits_mask_rdata_written_once = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  bundleOut_0_a_bits_mask_rdata_0 = _RAND_4[7:0];
  _RAND_5 = {1{`RANDOM}};
  repeat_count = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  repeat_sel_sel_sources_0 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  repeat_sel_sel_sources_1 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  repeat_sel_sel_sources_2 = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  repeat_sel_sel_sources_3 = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  repeat_sel_sel_sources_4 = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  repeat_sel_sel_sources_5 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  repeat_sel_sel_sources_6 = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  repeat_sel_sel_sources_7 = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  repeat_sel_sel_sources_8 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  repeat_sel_sel_sources_9 = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  repeat_sel_sel_sources_10 = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  repeat_sel_sel_sources_11 = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  repeat_sel_sel_sources_12 = _RAND_18[0:0];
  _RAND_19 = {1{`RANDOM}};
  repeat_sel_sel_sources_13 = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  repeat_sel_sel_sources_14 = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  repeat_sel_sel_sources_15 = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  repeat_sel_sel_sources_16 = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  repeat_sel_sel_sources_17 = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  repeat_sel_sel_sources_18 = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  repeat_sel_sel_sources_19 = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  repeat_sel_sel_sources_20 = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  repeat_sel_sel_sources_21 = _RAND_27[0:0];
  _RAND_28 = {1{`RANDOM}};
  repeat_sel_sel_sources_22 = _RAND_28[0:0];
  _RAND_29 = {1{`RANDOM}};
  repeat_sel_sel_sources_23 = _RAND_29[0:0];
  _RAND_30 = {1{`RANDOM}};
  repeat_sel_sel_sources_24 = _RAND_30[0:0];
  _RAND_31 = {1{`RANDOM}};
  repeat_sel_sel_sources_25 = _RAND_31[0:0];
  _RAND_32 = {1{`RANDOM}};
  repeat_sel_sel_sources_26 = _RAND_32[0:0];
  _RAND_33 = {1{`RANDOM}};
  repeat_sel_sel_sources_27 = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  repeat_sel_sel_sources_28 = _RAND_34[0:0];
  _RAND_35 = {1{`RANDOM}};
  repeat_sel_sel_sources_29 = _RAND_35[0:0];
  _RAND_36 = {1{`RANDOM}};
  repeat_sel_sel_sources_30 = _RAND_36[0:0];
  _RAND_37 = {1{`RANDOM}};
  repeat_sel_sel_sources_31 = _RAND_37[0:0];
  _RAND_38 = {1{`RANDOM}};
  repeat_sel_sel_sources_32 = _RAND_38[0:0];
  _RAND_39 = {1{`RANDOM}};
  repeat_sel_sel_sources_33 = _RAND_39[0:0];
  _RAND_40 = {1{`RANDOM}};
  repeat_sel_sel_sources_34 = _RAND_40[0:0];
  _RAND_41 = {1{`RANDOM}};
  repeat_sel_sel_sources_35 = _RAND_41[0:0];
  _RAND_42 = {1{`RANDOM}};
  repeat_sel_sel_sources_36 = _RAND_42[0:0];
  _RAND_43 = {1{`RANDOM}};
  repeat_sel_hold_r = _RAND_43[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    count = 1'h0;
  end
  if (reset) begin
    bundleOut_0_a_bits_data_rdata_written_once = 1'h0;
  end
  if (rf_reset) begin
    bundleOut_0_a_bits_data_rdata_0 = 64'h0;
  end
  if (reset) begin
    bundleOut_0_a_bits_mask_rdata_written_once = 1'h0;
  end
  if (rf_reset) begin
    bundleOut_0_a_bits_mask_rdata_0 = 8'h0;
  end
  if (reset) begin
    repeat_count = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_0 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_1 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_2 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_3 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_4 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_5 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_6 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_7 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_8 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_9 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_10 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_11 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_12 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_13 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_14 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_15 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_16 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_17 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_18 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_19 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_20 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_21 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_22 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_23 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_24 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_25 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_26 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_27 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_28 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_29 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_30 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_31 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_32 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_33 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_34 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_35 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_sel_sources_36 = 1'h0;
  end
  if (rf_reset) begin
    repeat_sel_hold_r = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
