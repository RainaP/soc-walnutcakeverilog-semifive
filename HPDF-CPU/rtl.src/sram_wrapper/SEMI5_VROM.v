`define FFD
module SEMI5_VROM
#( parameter ABIT = 13 // address bits
    		,WORD = 8192 // number of data words
    		,DBIT = 64 // number of data bits
)
(
     input              clock
    ,input              enable
    ,input   [ABIT-1:0] address
    ,input              readenable
    ,output  [DBIT-1:0] readdata
);
	wire [DBIT-1:0] unknown = {(DBIT){1'bx}};
    reg  [DBIT-1:0] Mem [WORD-1:0];
    reg  [DBIT-1:0] DOUT          ;

    always@(posedge clock) 
        if      (~enable            ) DOUT <=`FFD unknown;
		else if (enable & readenable) DOUT <=`FFD Mem[address] ; 


    assign readdata = DOUT  ;

`ifdef ENV_MEMORY_REPORT_DEFAULT_MODEL
	initial	begin
	    #10;
        $display("*W, SEMI5_VROM   : ABIT:%2d, WORD:%4d, DBIT:%4d           : %m", ABIT, WORD, DBIT);
	end
`endif
`ifdef ENV_MEMORY_REPORT_DEFAULT_MODEL_CSV
	initial	begin
	    #10;
        $display("VROM;%4d;%4d;500;;;sync;%m", WORD, DBIT);
	end
`endif

endmodule



