// SPDX-License-Identifier: Apache-2.0

//----------------------------------------------------------------------------------------
//  VIP interface
//----------------------------------------------------------------------------------------
`ifndef SYNTHESIS
interface axi_vmaster_driver_if #(
    parameter ID= 4, 
              A=36, 
              D=128, 
              MAX_BURST = 16, 
              MAX_OUTSTANDING = 16 
)(
    input bit CLK,
    input bit RSTN
);
    logic              ENABLE;
    logic  [ID-1:0]    AWID;
    logic  [A-1:0]     AWADDR;
    logic  [7:0]       AWLEN;
    logic  [2:0]       AWSIZE;
    logic  [1:0]       AWBURST;
    logic              AWLOCK;
    logic  [3:0]       AWCACHE;
    logic  [2:0]       AWPROT;
    logic              AWVALID;
    logic              AWREADY;
    logic  [D-1:0]     WDATA;
    logic  [(D/8)-1:0] WSTRB;
    logic              WLAST;
    logic              WVALID;
    logic              WREADY;
    logic  [ID-1:0]    BID;
    logic              BVALID;
    logic  [1:0]       BRESP;
    logic              BREADY;
    logic  [ID-1:0]    ARID;
    logic  [A-1:0]     ARADDR;
    logic  [7:0]       ARLEN;
    logic  [2:0]       ARSIZE;
    logic  [1:0]       ARBURST;
    logic              ARLOCK;
    logic  [3:0]       ARCACHE;
    logic  [2:0]       ARPROT;
    logic              ARVALID;
    logic              ARREADY;
    logic  [ID-1:0]    RID;
    logic  [D-1:0]     RDATA;
    logic  [1:0]       RRESP;
    logic              RLAST;
    logic              RVALID;
    logic              RREADY;
  
    //--------------------------------------------------------------------------------
    //
    //--------------------------------------------------------------------------------
    parameter L = $clog2(D/8);

    class AXI_TRANSACTION_A;
        reg              VALID;
        reg              WRITE;
        reg  [A-1:0]     ADDR ;
        reg  [ 31:0]     LEN  ;
        reg  [  2:0]     SIZE ;
    endclass

    class AXI_TRANSACTION_D;
        reg  [D-1:0]     DATA [$];
        reg  [(D/8)-1:0] STRB [$];
    endclass

    //--------------------------------------------------------------------------------
    AXI_TRANSACTION_A  transaction_aw_queue[$];
    AXI_TRANSACTION_A  transaction_ar_queue[$];
    AXI_TRANSACTION_D  transaction_wd_queue[$];
    AXI_TRANSACTION_D  transaction_rd_queue[$];
    reg[7:0]           transaction_read[$];
    logic [31:0] outstandings_r;
    logic [31:0] outstandings_w;

    AXI_TRANSACTION_A  cur_transaction_a;
    AXI_TRANSACTION_D  cur_transaction_d;
    logic [31:0]       cur_bytes;
    logic              cur_WRITE;
    logic [A-1:0]      cur_ADDR_begin ;
    logic [A-1:0]      cur_ADDR_end   ;
    logic [(D/8)-1:0]  cur_STRB ;
    logic [D-1:0]      cur_DATA ;

    initial begin
        ENABLE = 0;
        cur_transaction_a = new();
        cur_transaction_d = new();
        cur_transaction_a.VALID=0;
    end

    always@(posedge CLK or negedge RSTN)
        if(!RSTN) begin
            AWID      <= 0;
            AWBURST   <= 1;
            AWLOCK    <= 0;
            AWCACHE   <= 0;
            AWPROT    <= 0;
            BREADY    <= 1;
            ARID      <= 0;
            ARBURST   <= 1;
            ARLOCK    <= 0;
            ARCACHE   <= 0;
            ARPROT    <= 0;
            RREADY    <= 1;
            transaction_aw_queue.delete();
            transaction_ar_queue.delete();
            transaction_wd_queue.delete();
            transaction_rd_queue.delete();
            transaction_read.delete();
            cur_transaction_a.VALID=0;
            cur_bytes <= 0;
            cur_STRB  <= 0;
        end

    //--------------------------------------------------------------------------------
    //  driver core
    //--------------------------------------------------------------------------------
    task collect_axi(logic in_WRITE, logic[63:0] in_ADDR, logic[7:0] in_DATA);
        integer g;
        if( (0!=cur_bytes) && ((cur_ADDR_end[A-1:L] !== in_ADDR[A-1:L]) || (cur_WRITE != in_WRITE)) ) begin
            push_single();
            if( ( cur_ADDR_end[A-1:L]+1 !== in_ADDR[A-1:L] )
                || (cur_transaction_d.DATA.size()==MAX_BURST) 
                || ({cur_ADDR_end[11:L],{L{1'b1}}}==4095)
                || (cur_WRITE != in_WRITE) ) begin
                flush_axi();
            end
        end
        if((0==cur_bytes) && (0==cur_transaction_a.VALID)) begin            
            cur_ADDR_begin = in_ADDR;
        end
        cur_bytes      = cur_bytes+1;
        cur_WRITE      = in_WRITE;
        cur_ADDR_end   = {in_ADDR[A-1:L],{L{1'b0}}};
        cur_STRB       = cur_STRB | (1 << in_ADDR[L-1:0]);        
        for( g=0; g<8; g=g+1 ) begin
            cur_DATA[ in_ADDR[L-1:0]*8 + g ] = in_DATA[g];
        end
    endtask

    task push_single;
        if( (0!=cur_bytes) ) begin
            cur_transaction_a.VALID = 1;
            cur_transaction_a.WRITE = cur_WRITE;
            cur_transaction_a.ADDR  = cur_ADDR_begin;
            cur_transaction_a.LEN   = cur_bytes;
            cur_transaction_d.DATA.push_back(cur_DATA);
            cur_transaction_d.STRB.push_back(cur_STRB);
            cur_bytes= 0;
            cur_STRB = 0;
        end
    endtask

    task flush_axi;
        push_single();
        if( cur_transaction_a.VALID ) begin
            @(negedge CLK );
            if((1==cur_transaction_a.LEN) && (1==cur_transaction_d.DATA.size())) begin
                cur_transaction_a.SIZE= 0;
            end else if((2==cur_transaction_a.LEN) && (0==cur_transaction_a.ADDR[0]) && (1==cur_transaction_d.DATA.size())) begin
                cur_transaction_a.SIZE= 1;
            end else if((4==cur_transaction_a.LEN) && (0==cur_transaction_a.ADDR[1:0]) && (1==cur_transaction_d.DATA.size())) begin
                cur_transaction_a.SIZE= 2;
            end else if((8==cur_transaction_a.LEN) && (0==cur_transaction_a.ADDR[2:0]) && (1==cur_transaction_d.DATA.size())) begin
                cur_transaction_a.SIZE= 3;
            end else begin
                cur_transaction_a.ADDR= {cur_transaction_a.ADDR[A-1:L],{L{1'b0}}};
                cur_transaction_a.SIZE= L;
            end
            cur_transaction_a.LEN = cur_transaction_d.DATA.size()-1;
            if( cur_transaction_a.WRITE ) begin
                transaction_aw_queue.push_back(cur_transaction_a);
                transaction_wd_queue.push_back(cur_transaction_d);
            end else begin
                transaction_ar_queue.push_back(cur_transaction_a);
                transaction_rd_queue.push_back(cur_transaction_d);
            end
            cur_transaction_a = new();
            cur_transaction_d = new();
            cur_transaction_a.VALID=0;
        end
    endtask

    function [7:0] read_data;
        assert( 0!=transaction_read.size() );
        return transaction_read.pop_front();        
    endfunction

    task wait_for_w;
        flush_axi();
        @(negedge CLK );            
        while(0<outstandings_w || 0<transaction_aw_queue.size() || AWVALID || WVALID ) @(negedge CLK );
        @(posedge CLK );
    endtask

    task wait_for_r;
        flush_axi();
        @(negedge CLK );            
        while(0<outstandings_r || 0<transaction_ar_queue.size() || ARVALID || RVALID ) @(negedge CLK );
        @(posedge CLK );
    endtask

    always@(posedge CLK or negedge RSTN)
        if(!RSTN) begin
            AWADDR  <= 0;
            AWLEN   <= 0;
            AWSIZE  <= 0;
            AWVALID <= 0;
        end else if(((0==AWVALID) || AWREADY ) && (0<transaction_aw_queue.size())) begin
            AXI_TRANSACTION_A  a_transaction;
            a_transaction = transaction_aw_queue.pop_front();
            AWADDR  <= a_transaction.ADDR ;
            AWLEN   <= a_transaction.LEN  ;
            AWSIZE  <= a_transaction.SIZE ;
            AWVALID <= 1;
        end else if( AWREADY ) begin
            AWADDR  <= {A{1'bx}};
            AWLEN   <= 8'bx;
            AWSIZE  <= 3'bx;
            AWVALID <= 0;
        end

    always@(posedge CLK or negedge RSTN)
        if(!RSTN) begin
            ARADDR  <= 0;
            ARLEN   <= 0;
            ARSIZE  <= 0;
            ARVALID <= 0;
        end else if(((0==ARVALID) || ARREADY ) && (0<transaction_ar_queue.size())) begin
            AXI_TRANSACTION_A  a_transaction;
            a_transaction = transaction_ar_queue.pop_front();
            ARADDR  <= a_transaction.ADDR ;
            ARLEN   <= a_transaction.LEN  ;
            ARSIZE  <= a_transaction.SIZE ;
            ARVALID <= 1;
        end else if( ARREADY ) begin
            ARADDR  <= {A{1'bx}};
            ARLEN   <= 8'bx;
            ARSIZE  <= 3'bx;
            ARVALID <= 0;
        end

    always@(posedge CLK or negedge RSTN)
        if(!RSTN) begin
            WDATA  <= 0;
            WSTRB  <= 0;
            WLAST  <= 0;
            WVALID <= 0;
        end else if(( (0==WVALID) || WREADY ) && (0<transaction_wd_queue.size())) begin
            AXI_TRANSACTION_D a_transaction;
            a_transaction = transaction_wd_queue[0];
            WDATA  <= a_transaction.DATA.pop_front();
            WSTRB  <= a_transaction.STRB.pop_front();
            WLAST  <= (0==a_transaction.DATA.size());
            WVALID <= 1;
            if(0==a_transaction.DATA.size()) transaction_wd_queue.pop_front();
        end else if( WREADY ) begin
            WDATA  <= {D{1'bx}};
            WSTRB  <= {(D/8){1'bx}};
            WLAST  <= 1'bx;
            WVALID <= 0;
        end

    always@(posedge CLK or negedge RSTN)
        if(!RSTN) outstandings_r <= 0;
        else if(ARVALID || RVALID) begin
            outstandings_r <= outstandings_r + (ARVALID && ARREADY) - (RVALID && RREADY && RLAST);
        end

    always@(posedge CLK or negedge RSTN)
        if(!RSTN) outstandings_w <= 0;
        else if(AWVALID || BVALID) begin
            outstandings_w <= outstandings_w + (AWVALID && AWREADY) - (BVALID && BREADY);
        end

    always@(posedge CLK or negedge RSTN)
        if(RSTN && RVALID && RREADY) begin
            integer g;
            AXI_TRANSACTION_D item;
            reg  [(D/8)-1:0]  strb;
            assert( 0 < transaction_rd_queue.size() );
            item = transaction_rd_queue[0];
            assert( 0 < item.STRB.size() );
            strb = item.STRB.pop_front();
            for( g=0; g<D; g=g+1 ) begin
                if( strb[g] ) begin
                    reg[7:0] ritem;
                    ritem = (RDATA >> (g*8));
                    transaction_read.push_back(ritem);
                end
            end
            if( 0==item.STRB.size() ) transaction_rd_queue.pop_front();
        end
endinterface

//----------------------------------------------------------------------------------------
//  VIPs
//----------------------------------------------------------------------------------------
class axi_vmaster_driver #(
    parameter ID= 36 ,
              A = 36 ,
              D = 128,
              MAX_BURST = 64,
              MAX_OUTSTANDING = 64 ) extends vmaster_driver;

    typedef virtual axi_vmaster_driver_if#(.ID(ID),.A(A),.D(D),.MAX_BURST(MAX_BURST),.MAX_OUTSTANDING(MAX_OUTSTANDING)) vif_type;
    vif_type vif;
    function new(string name,vif_type vif);
        this.vif = vif;
        semifive_hookup_config#(vmaster_driver,string)::set(name,this);
    endfunction

    //--------------------------------------------------------------------------------
    //  Service functions
    //--------------------------------------------------------------------------------
    virtual task write_req( input logic[63:0] ADDR, input logic[7:0] DATA );
        vif.collect_axi( 1, ADDR, DATA );
    endtask

    virtual task write_flush;
        vif.wait_for_w();
    endtask

    virtual task read_req( input logic[63:0] ADDR );
        vif.collect_axi( 0, ADDR, 8'hXX );
    endtask

    virtual task read_flush;
        vif.wait_for_r();
    endtask

    virtual function [7:0] get_read_data;
        return vif.read_data();
    endfunction

    virtual task hookup( input logic enable );
        vif.ENABLE = enable;
    endtask

endclass

`endif

//----------------------------------------------------------------------------------------
//  VIP RTL wrapper
//----------------------------------------------------------------------------------------
module axi_vmaster_binder #(
    parameter string name="TBD",
              int ID=  4 ,
              A = 36 ,
              D = 128,
              MAX_BURST = 64, 
              MAX_OUTSTANDING = 64 
)(
    input               CLK__BUS,
    input               RSTN__BUS, 

    input   [ID-1:0]    AXI4__BUS__SLV__AWID,
    input   [A-1:0]     AXI4__BUS__SLV__AWADDR,
    input   [7:0]       AXI4__BUS__SLV__AWLEN,
    input   [2:0]       AXI4__BUS__SLV__AWSIZE,
    input   [1:0]       AXI4__BUS__SLV__AWBURST,
    input               AXI4__BUS__SLV__AWLOCK,
    input   [3:0]       AXI4__BUS__SLV__AWCACHE,
    input   [2:0]       AXI4__BUS__SLV__AWPROT,
    input               AXI4__BUS__SLV__AWVALID,
    output              AXI4__BUS__SLV__AWREADY,
    input   [D-1:0]     AXI4__BUS__SLV__WDATA,
    input   [(D/8)-1:0] AXI4__BUS__SLV__WSTRB,
    input               AXI4__BUS__SLV__WLAST,
    input               AXI4__BUS__SLV__WVALID,
    output              AXI4__BUS__SLV__WREADY,
    output  [ID-1:0]    AXI4__BUS__SLV__BID,
    output              AXI4__BUS__SLV__BVALID,
    output  [1:0]       AXI4__BUS__SLV__BRESP,
    input               AXI4__BUS__SLV__BREADY,
    input   [ID-1:0]    AXI4__BUS__SLV__ARID,
    input   [A-1:0]     AXI4__BUS__SLV__ARADDR,
    input   [7:0]       AXI4__BUS__SLV__ARLEN,
    input   [2:0]       AXI4__BUS__SLV__ARSIZE,
    input   [1:0]       AXI4__BUS__SLV__ARBURST,
    input               AXI4__BUS__SLV__ARLOCK,
    input   [3:0]       AXI4__BUS__SLV__ARCACHE,
    input   [2:0]       AXI4__BUS__SLV__ARPROT,
    input               AXI4__BUS__SLV__ARVALID,
    output              AXI4__BUS__SLV__ARREADY,
    output  [ID-1:0]    AXI4__BUS__SLV__RID,
    output  [D-1:0]     AXI4__BUS__SLV__RDATA,
    output  [1:0]       AXI4__BUS__SLV__RRESP,
    output              AXI4__BUS__SLV__RLAST,
    output              AXI4__BUS__SLV__RVALID,
    input               AXI4__BUS__SLV__RREADY,

    output  [ID-1:0]    AXI4__BUS__MST__AWID,
    output  [A-1:0]     AXI4__BUS__MST__AWADDR,
    output  [7:0]       AXI4__BUS__MST__AWLEN,
    output  [2:0]       AXI4__BUS__MST__AWSIZE,
    output  [1:0]       AXI4__BUS__MST__AWBURST,
    output              AXI4__BUS__MST__AWLOCK,
    output  [3:0]       AXI4__BUS__MST__AWCACHE,
    output  [2:0]       AXI4__BUS__MST__AWPROT,
    output              AXI4__BUS__MST__AWVALID,
    input               AXI4__BUS__MST__AWREADY,
    output  [D-1:0]     AXI4__BUS__MST__WDATA,
    output  [(D/8)-1:0] AXI4__BUS__MST__WSTRB,
    output              AXI4__BUS__MST__WLAST,
    output              AXI4__BUS__MST__WVALID,
    input               AXI4__BUS__MST__WREADY,
    input   [ID-1:0]    AXI4__BUS__MST__BID,
    input               AXI4__BUS__MST__BVALID,
    input   [1:0]       AXI4__BUS__MST__BRESP,
    output              AXI4__BUS__MST__BREADY,
    output  [ID-1:0]    AXI4__BUS__MST__ARID,
    output  [A-1:0]     AXI4__BUS__MST__ARADDR,
    output  [7:0]       AXI4__BUS__MST__ARLEN,
    output  [2:0]       AXI4__BUS__MST__ARSIZE,
    output  [1:0]       AXI4__BUS__MST__ARBURST,
    output              AXI4__BUS__MST__ARLOCK,
    output  [3:0]       AXI4__BUS__MST__ARCACHE,
    output  [2:0]       AXI4__BUS__MST__ARPROT,
    output              AXI4__BUS__MST__ARVALID,
    input               AXI4__BUS__MST__ARREADY,
    input   [ID-1:0]    AXI4__BUS__MST__RID,
    input   [D-1:0]     AXI4__BUS__MST__RDATA,
    input   [1:0]       AXI4__BUS__MST__RRESP,
    input               AXI4__BUS__MST__RLAST,
    input               AXI4__BUS__MST__RVALID,
    output              AXI4__BUS__MST__RREADY
);
`ifndef SYNTHESIS
    axi_vmaster_driver_if #(.ID(ID),.A(A),.D(D),.MAX_BURST(MAX_BURST),.MAX_OUTSTANDING(MAX_OUTSTANDING)) axi_if(CLK__BUS,RSTN__BUS);              
    axi_vmaster_driver #(.ID(ID),.A(A),.D(D),.MAX_BURST(MAX_BURST),.MAX_OUTSTANDING(MAX_OUTSTANDING)) driver;
    initial driver = new(name, axi_if);

    //-----------------------------------------------------
    assign AXI4__BUS__MST__AWID    = axi_if.ENABLE ? axi_if.AWID    : AXI4__BUS__SLV__AWID    ;
    assign AXI4__BUS__MST__AWADDR  = axi_if.ENABLE ? axi_if.AWADDR  : AXI4__BUS__SLV__AWADDR  ;
    assign AXI4__BUS__MST__AWLEN   = axi_if.ENABLE ? axi_if.AWLEN   : AXI4__BUS__SLV__AWLEN   ;
    assign AXI4__BUS__MST__AWSIZE  = axi_if.ENABLE ? axi_if.AWSIZE  : AXI4__BUS__SLV__AWSIZE  ;
    assign AXI4__BUS__MST__AWBURST = axi_if.ENABLE ? axi_if.AWBURST : AXI4__BUS__SLV__AWBURST ;
    assign AXI4__BUS__MST__AWLOCK  = axi_if.ENABLE ? axi_if.AWLOCK  : AXI4__BUS__SLV__AWLOCK  ;
    assign AXI4__BUS__MST__AWCACHE = axi_if.ENABLE ? axi_if.AWCACHE : AXI4__BUS__SLV__AWCACHE ;
    assign AXI4__BUS__MST__AWPROT  = axi_if.ENABLE ? axi_if.AWPROT  : AXI4__BUS__SLV__AWPROT  ;
    assign AXI4__BUS__MST__AWVALID = axi_if.ENABLE ? axi_if.AWVALID : AXI4__BUS__SLV__AWVALID ;
    assign AXI4__BUS__MST__WDATA   = axi_if.ENABLE ? axi_if.WDATA   : AXI4__BUS__SLV__WDATA   ;
    assign AXI4__BUS__MST__WSTRB   = axi_if.ENABLE ? axi_if.WSTRB   : AXI4__BUS__SLV__WSTRB   ;
    assign AXI4__BUS__MST__WLAST   = axi_if.ENABLE ? axi_if.WLAST   : AXI4__BUS__SLV__WLAST   ;
    assign AXI4__BUS__MST__WVALID  = axi_if.ENABLE ? axi_if.WVALID  : AXI4__BUS__SLV__WVALID  ;
    assign AXI4__BUS__MST__BREADY  = axi_if.ENABLE ? axi_if.BREADY  : AXI4__BUS__SLV__BREADY  ;
    assign AXI4__BUS__MST__ARID    = axi_if.ENABLE ? axi_if.ARID    : AXI4__BUS__SLV__ARID    ;
    assign AXI4__BUS__MST__ARADDR  = axi_if.ENABLE ? axi_if.ARADDR  : AXI4__BUS__SLV__ARADDR  ;
    assign AXI4__BUS__MST__ARLEN   = axi_if.ENABLE ? axi_if.ARLEN   : AXI4__BUS__SLV__ARLEN   ;
    assign AXI4__BUS__MST__ARSIZE  = axi_if.ENABLE ? axi_if.ARSIZE  : AXI4__BUS__SLV__ARSIZE  ;
    assign AXI4__BUS__MST__ARBURST = axi_if.ENABLE ? axi_if.ARBURST : AXI4__BUS__SLV__ARBURST ;
    assign AXI4__BUS__MST__ARLOCK  = axi_if.ENABLE ? axi_if.ARLOCK  : AXI4__BUS__SLV__ARLOCK  ;
    assign AXI4__BUS__MST__ARCACHE = axi_if.ENABLE ? axi_if.ARCACHE : AXI4__BUS__SLV__ARCACHE ;
    assign AXI4__BUS__MST__ARPROT  = axi_if.ENABLE ? axi_if.ARPROT  : AXI4__BUS__SLV__ARPROT  ;
    assign AXI4__BUS__MST__ARVALID = axi_if.ENABLE ? axi_if.ARVALID : AXI4__BUS__SLV__ARVALID ;
    assign AXI4__BUS__MST__RREADY  = axi_if.ENABLE ? axi_if.RREADY  : AXI4__BUS__SLV__RREADY  ;

    assign  AXI4__BUS__SLV__AWREADY = AXI4__BUS__MST__AWREADY && (0==axi_if.ENABLE);
    assign  AXI4__BUS__SLV__WREADY  = AXI4__BUS__MST__WREADY  && (0==axi_if.ENABLE);
    assign  AXI4__BUS__SLV__BID     = AXI4__BUS__MST__BID     ;
    assign  AXI4__BUS__SLV__BVALID  = AXI4__BUS__MST__BVALID  && (0==axi_if.ENABLE);
    assign  AXI4__BUS__SLV__BRESP   = AXI4__BUS__MST__BRESP   ;
    assign  AXI4__BUS__SLV__ARREADY = AXI4__BUS__MST__ARREADY && (0==axi_if.ENABLE);
    assign  AXI4__BUS__SLV__RID     = AXI4__BUS__MST__RID     ;
    assign  AXI4__BUS__SLV__RDATA   = AXI4__BUS__MST__RDATA   ;
    assign  AXI4__BUS__SLV__RRESP   = AXI4__BUS__MST__RRESP   ;
    assign  AXI4__BUS__SLV__RLAST   = AXI4__BUS__MST__RLAST   ;
    assign  AXI4__BUS__SLV__RVALID  = AXI4__BUS__MST__RVALID  && (0==axi_if.ENABLE);

    assign  axi_if.AWREADY = AXI4__BUS__MST__AWREADY && (1==axi_if.ENABLE);
    assign  axi_if.WREADY  = AXI4__BUS__MST__WREADY  && (1==axi_if.ENABLE);
    assign  axi_if.BID     = AXI4__BUS__MST__BID     ;
    assign  axi_if.BVALID  = AXI4__BUS__MST__BVALID  && (1==axi_if.ENABLE);
    assign  axi_if.BRESP   = AXI4__BUS__MST__BRESP   ;
    assign  axi_if.ARREADY = AXI4__BUS__MST__ARREADY && (1==axi_if.ENABLE);
    assign  axi_if.RID     = AXI4__BUS__MST__RID     ;
    assign  axi_if.RDATA   = AXI4__BUS__MST__RDATA   ;
    assign  axi_if.RRESP   = AXI4__BUS__MST__RRESP   ;
    assign  axi_if.RLAST   = AXI4__BUS__MST__RLAST   ;
    assign  axi_if.RVALID  = AXI4__BUS__MST__RVALID  && (1==axi_if.ENABLE);
`else
    //-----------------------------------------------------
    assign AXI4__BUS__MST__AWID    = AXI4__BUS__SLV__AWID    ;
    assign AXI4__BUS__MST__AWADDR  = AXI4__BUS__SLV__AWADDR  ;
    assign AXI4__BUS__MST__AWLEN   = AXI4__BUS__SLV__AWLEN   ;
    assign AXI4__BUS__MST__AWSIZE  = AXI4__BUS__SLV__AWSIZE  ;
    assign AXI4__BUS__MST__AWBURST = AXI4__BUS__SLV__AWBURST ;
    assign AXI4__BUS__MST__AWLOCK  = AXI4__BUS__SLV__AWLOCK  ;
    assign AXI4__BUS__MST__AWCACHE = AXI4__BUS__SLV__AWCACHE ;
    assign AXI4__BUS__MST__AWPROT  = AXI4__BUS__SLV__AWPROT  ;
    assign AXI4__BUS__MST__AWVALID = AXI4__BUS__SLV__AWVALID ;
    assign AXI4__BUS__MST__WDATA   = AXI4__BUS__SLV__WDATA   ;
    assign AXI4__BUS__MST__WSTRB   = AXI4__BUS__SLV__WSTRB   ;
    assign AXI4__BUS__MST__WLAST   = AXI4__BUS__SLV__WLAST   ;
    assign AXI4__BUS__MST__WVALID  = AXI4__BUS__SLV__WVALID  ;
    assign AXI4__BUS__MST__BREADY  = AXI4__BUS__SLV__BREADY  ;
    assign AXI4__BUS__MST__ARID    = AXI4__BUS__SLV__ARID    ;
    assign AXI4__BUS__MST__ARADDR  = AXI4__BUS__SLV__ARADDR  ;
    assign AXI4__BUS__MST__ARLEN   = AXI4__BUS__SLV__ARLEN   ;
    assign AXI4__BUS__MST__ARSIZE  = AXI4__BUS__SLV__ARSIZE  ;
    assign AXI4__BUS__MST__ARBURST = AXI4__BUS__SLV__ARBURST ;
    assign AXI4__BUS__MST__ARLOCK  = AXI4__BUS__SLV__ARLOCK  ;
    assign AXI4__BUS__MST__ARCACHE = AXI4__BUS__SLV__ARCACHE ;
    assign AXI4__BUS__MST__ARPROT  = AXI4__BUS__SLV__ARPROT  ;
    assign AXI4__BUS__MST__ARVALID = AXI4__BUS__SLV__ARVALID ;
    assign AXI4__BUS__MST__RREADY  = AXI4__BUS__SLV__RREADY  ;

    assign  AXI4__BUS__SLV__AWREADY = AXI4__BUS__MST__AWREADY ;
    assign  AXI4__BUS__SLV__WREADY  = AXI4__BUS__MST__WREADY  ;
    assign  AXI4__BUS__SLV__BID     = AXI4__BUS__MST__BID     ;
    assign  AXI4__BUS__SLV__BVALID  = AXI4__BUS__MST__BVALID  ;
    assign  AXI4__BUS__SLV__BRESP   = AXI4__BUS__MST__BRESP   ;
    assign  AXI4__BUS__SLV__ARREADY = AXI4__BUS__MST__ARREADY ;
    assign  AXI4__BUS__SLV__RID     = AXI4__BUS__MST__RID     ;
    assign  AXI4__BUS__SLV__RDATA   = AXI4__BUS__MST__RDATA   ;
    assign  AXI4__BUS__SLV__RRESP   = AXI4__BUS__MST__RRESP   ;
    assign  AXI4__BUS__SLV__RLAST   = AXI4__BUS__MST__RLAST   ;
    assign  AXI4__BUS__SLV__RVALID  = AXI4__BUS__MST__RVALID  ;
`endif
endmodule
