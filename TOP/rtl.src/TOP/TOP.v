//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade 
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module TOP (
	 inout         USB3_0__VBUS
	,inout         USB3_0__DM
	,inout         USB3_0__DP
	,input         USB3_0__ID
	,inout         USB3_0__RESREF
	,output        USB3_0__TXM
	,output        USB3_0__TXP
	,input         USB3_0__RXM
	,input         USB3_0__RXP
	,input         USB3_0__REFPADCLKM
	,input         USB3_0__REFPADCLKP
	,output [ 5:0] LPDDR4_0_ADCT
	,inout         LPDDR4_0_CK
	,inout         LPDDR4_0_CKB
	,output [ 1:0] LPDDR4_0_CKE
	,output [ 1:0] LPDDR4_0_CS
	,inout  [ 1:0] LPDDR4_0_DM_A
	,inout  [ 1:0] LPDDR4_0_DM_B
	,inout  [15:0] LPDDR4_0_DQ_A
	,inout  [15:0] LPDDR4_0_DQ_B
	,inout  [ 1:0] LPDDR4_0_NDQS_A
	,inout  [ 1:0] LPDDR4_0_NDQS_B
	,output [ 1:0] LPDDR4_0_ODT
	,inout  [ 1:0] LPDDR4_0_PDQS_A
	,inout  [ 1:0] LPDDR4_0_PDQS_B
	,output        LPDDR4_0_RESET
	,inout         LPDDR4_0_ZQ
	,output [ 5:0] LPDDR4_1_ADCT
	,inout         LPDDR4_1_CK
	,inout         LPDDR4_1_CKB
	,output [ 1:0] LPDDR4_1_CKE
	,output [ 1:0] LPDDR4_1_CS
	,inout  [ 1:0] LPDDR4_1_DM_A
	,inout  [ 1:0] LPDDR4_1_DM_B
	,inout  [15:0] LPDDR4_1_DQ_A
	,inout  [15:0] LPDDR4_1_DQ_B
	,inout  [ 1:0] LPDDR4_1_NDQS_A
	,inout  [ 1:0] LPDDR4_1_NDQS_B
	,output [ 1:0] LPDDR4_1_ODT
	,inout  [ 1:0] LPDDR4_1_PDQS_A
	,inout  [ 1:0] LPDDR4_1_PDQS_B
	,output        LPDDR4_1_RESET
	,inout         LPDDR4_1_ZQ
	,input         RSTN
	,output        RSTN_OUT
	,input         PKG0
	,input         PKG1
	,input         PKG2
	,input         JTAG_TCK
	,input         JTAG_TMS
	,input         JTAG_TDI
	,output        JTAG_TDO
	,input         JTAG_TRST
	,inout         GPIOA_0
	,inout         GPIOA_1
	,inout         GPIOA_2
	,inout         GPIOA_3
	,inout         GPIOA_4
	,inout         GPIOA_5
	,inout         GPIOA_6
	,inout         GPIOA_7
	,inout         GPIOA_8
	,inout         GPIOA_9
	,inout         GPIOA_10
	,inout         GPIOA_11
	,inout         GPIOA_12
	,inout         GPIOA_13
	,inout         GPIOA_14
	,inout         GPIOA_15
	,inout         GPIOB_0
	,inout         GPIOB_1
	,inout         GPIOB_2
	,inout         GPIOB_3
	,inout         GPIOB_4
	,inout         GPIOB_5
	,inout         GPIOB_6
	,inout         GPIOB_7
	,inout         GPIOB_8
	,inout         GPIOB_9
	,inout         GPIOB_10
	,inout         GPIOB_11
	,inout         GPIOB_12
	,inout         GPIOB_13
	,inout         GPIOB_14
	,inout         GPIOB_15
	,inout         GPIOC_0
	,inout         GPIOC_1
	,inout         GPIOC_2
	,inout         GPIOC_3
	,inout         GPIOC_4
	,inout         GPIOC_5
	,inout         GPIOC_6
	,inout         GPIOC_7
	,inout         GPIOC_8
	,inout         GPIOC_9
	,inout         GPIOC_10
	,inout         GPIOC_11
	,inout         GPIOC_12
	,inout         GPIOC_13
	,inout         GPIOC_14
	,inout         GPIOC_15
	,inout         GPIOD_0
	,inout         GPIOD_1
	,inout         GPIOD_2
	,inout         GPIOD_3
	,inout         GPIOD_4
	,inout         GPIOD_5
	,inout         GPIOD_6
	,inout         GPIOD_7
	,inout         GPIOD_8
	,inout         GPIOD_9
	,inout         GPIOD_10
	,inout         GPIOD_11
	,inout         GPIOD_12
	,inout         GPIOD_13
	,inout         GPIOD_14
	,inout         GPIOD_15
	,inout         SDMMC_0
	,inout         SDMMC_1
	,inout         SDMMC_2
	,inout         SDMMC_3
	,inout         SDMMC_4
	,inout         SDMMC_5
	,inout         SDMMC_6
	,inout         SDMMC_7
	,inout         SDMMC_8
	,inout         SDMMC_9
	,inout         SDMMC_10
	,inout         SDMMC_11
	,inout         OTP_0_VPP
	,inout         OTP_0_VBG
	,inout         OTP_0_VTDO
	,inout         OTP_0_VREFM
	,inout         OTP_1_VPP
	,inout         OTP_1_VBG
	,inout         OTP_1_VTDO
	,inout         OTP_1_VREFM
	,input         CLKSEL
	,output        XTALO
	,input         XTALI
	,input         OSC
);
	wire [15:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_A__IO__A;
	wire [15:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_B__IO__A;
	wire [15:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_C__IO__A;
	wire [15:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_D__IO__A;
	wire [15:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_A__IO__Y;
	wire [15:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_B__IO__Y;
	wire [15:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_C__IO__Y;
	wire [15:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_D__IO__Y;
	wire [15:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_A__IO__OE;
	wire [15:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_B__IO__OE;
	wire [15:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_C__IO__OE;
	wire [15:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_D__IO__OE;
	wire        CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TCK;
	wire        CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TMS;
	wire        CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TDI;
	wire        CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TDO;
	wire        CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TDOEN;
	wire        CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TRST;
	wire        CORE_0__PADS_0__PKG__NPU0_DISABLE;
	wire        CORE_0__PADS_0__PKG__NPU1_DISABLE;
	wire        CORE_0__PADS_0__PKG__NPU2_DISABLE;
	wire        CORE_0__PADS_0__IO__RSTNOUT;
	wire        CORE_0__PADS_0__IO__CLK__OSC;
	wire        CORE_0__PADS_0__IO__CLK__XTAL;
	wire        CORE_0__PADS_0__IO__CLKSEL;
	wire        CORE_0__PADS_0__IO__RSTN;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_0__IO__SCLI;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_0__IO__SCLOE;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_0__IO__SDIN;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_0__IO__SDOE;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_1__IO__SCLI;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_1__IO__SCLOE;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_1__IO__SDIN;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_1__IO__SDOE;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_2__IO__SCLI;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_2__IO__SCLOE;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_2__IO__SDIN;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_2__IO__SDOE;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_3__IO__SCLI;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_3__IO__SCLOE;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_3__IO__SDIN;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_3__IO__SDOE;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_0__IO__SCK;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_0__IO__SCS;
	wire [ 3:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_0__IO__SDO;
	wire [ 3:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_0__IO__SDI;
	wire [ 3:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_0__IO__OEN;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_1__IO__SCK;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_1__IO__SCS;
	wire [ 3:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_1__IO__SDO;
	wire [ 3:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_1__IO__SDI;
	wire [ 3:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_1__IO__OEN;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_2__IO__SCK;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_2__IO__SCS;
	wire [ 3:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_2__IO__SDO;
	wire [ 3:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_2__IO__SDI;
	wire [ 3:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_2__IO__OEN;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_3__IO__SCK;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_3__IO__SCS;
	wire [ 3:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_3__IO__SDO;
	wire [ 3:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_3__IO__SDI;
	wire [ 3:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_3__IO__OEN;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__UART_0__IO__RX;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__UART_0__IO__TX;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__UART_1__IO__RX;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__UART_1__IO__TX;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__CLK;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__CMD;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__RSP;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__CMDOEN;
	wire [ 7:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__DOUT;
	wire [ 7:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__DIN;
	wire [ 7:0] CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__DOEN;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__DQS;
	wire        CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__RSTN;
	wire        IOMUX_0__PADS_0__Y__GPIOA_0;
	wire        IOMUX_0__PADS_0__A__GPIOA_0;
	wire        IOMUX_0__PADS_0__OE__GPIOA_0;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_0;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_0;
	wire        IOMUX_0__PADS_0__IE__GPIOA_0;
	wire        IOMUX_0__PADS_0__PE__GPIOA_0;
	wire        IOMUX_0__PADS_0__PS__GPIOA_0;
	wire        IOMUX_0__PADS_0__Y__GPIOB_0;
	wire        IOMUX_0__PADS_0__A__GPIOB_0;
	wire        IOMUX_0__PADS_0__OE__GPIOB_0;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_0;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_0;
	wire        IOMUX_0__PADS_0__IE__GPIOB_0;
	wire        IOMUX_0__PADS_0__PE__GPIOB_0;
	wire        IOMUX_0__PADS_0__PS__GPIOB_0;
	wire        IOMUX_0__PADS_0__Y__GPIOC_0;
	wire        IOMUX_0__PADS_0__A__GPIOC_0;
	wire        IOMUX_0__PADS_0__OE__GPIOC_0;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_0;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_0;
	wire        IOMUX_0__PADS_0__IE__GPIOC_0;
	wire        IOMUX_0__PADS_0__PE__GPIOC_0;
	wire        IOMUX_0__PADS_0__PS__GPIOC_0;
	wire        IOMUX_0__PADS_0__Y__GPIOD_0;
	wire        IOMUX_0__PADS_0__A__GPIOD_0;
	wire        IOMUX_0__PADS_0__OE__GPIOD_0;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_0;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_0;
	wire        IOMUX_0__PADS_0__IE__GPIOD_0;
	wire        IOMUX_0__PADS_0__PE__GPIOD_0;
	wire        IOMUX_0__PADS_0__PS__GPIOD_0;
	wire        IOMUX_0__PADS_0__Y__GPIOA_1;
	wire        IOMUX_0__PADS_0__A__GPIOA_1;
	wire        IOMUX_0__PADS_0__OE__GPIOA_1;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_1;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_1;
	wire        IOMUX_0__PADS_0__IE__GPIOA_1;
	wire        IOMUX_0__PADS_0__PE__GPIOA_1;
	wire        IOMUX_0__PADS_0__PS__GPIOA_1;
	wire        IOMUX_0__PADS_0__Y__GPIOB_1;
	wire        IOMUX_0__PADS_0__A__GPIOB_1;
	wire        IOMUX_0__PADS_0__OE__GPIOB_1;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_1;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_1;
	wire        IOMUX_0__PADS_0__IE__GPIOB_1;
	wire        IOMUX_0__PADS_0__PE__GPIOB_1;
	wire        IOMUX_0__PADS_0__PS__GPIOB_1;
	wire        IOMUX_0__PADS_0__Y__GPIOC_1;
	wire        IOMUX_0__PADS_0__A__GPIOC_1;
	wire        IOMUX_0__PADS_0__OE__GPIOC_1;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_1;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_1;
	wire        IOMUX_0__PADS_0__IE__GPIOC_1;
	wire        IOMUX_0__PADS_0__PE__GPIOC_1;
	wire        IOMUX_0__PADS_0__PS__GPIOC_1;
	wire        IOMUX_0__PADS_0__Y__GPIOD_1;
	wire        IOMUX_0__PADS_0__A__GPIOD_1;
	wire        IOMUX_0__PADS_0__OE__GPIOD_1;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_1;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_1;
	wire        IOMUX_0__PADS_0__IE__GPIOD_1;
	wire        IOMUX_0__PADS_0__PE__GPIOD_1;
	wire        IOMUX_0__PADS_0__PS__GPIOD_1;
	wire        IOMUX_0__PADS_0__Y__GPIOA_2;
	wire        IOMUX_0__PADS_0__A__GPIOA_2;
	wire        IOMUX_0__PADS_0__OE__GPIOA_2;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_2;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_2;
	wire        IOMUX_0__PADS_0__IE__GPIOA_2;
	wire        IOMUX_0__PADS_0__PE__GPIOA_2;
	wire        IOMUX_0__PADS_0__PS__GPIOA_2;
	wire        IOMUX_0__PADS_0__Y__GPIOB_2;
	wire        IOMUX_0__PADS_0__A__GPIOB_2;
	wire        IOMUX_0__PADS_0__OE__GPIOB_2;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_2;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_2;
	wire        IOMUX_0__PADS_0__IE__GPIOB_2;
	wire        IOMUX_0__PADS_0__PE__GPIOB_2;
	wire        IOMUX_0__PADS_0__PS__GPIOB_2;
	wire        IOMUX_0__PADS_0__Y__GPIOC_2;
	wire        IOMUX_0__PADS_0__A__GPIOC_2;
	wire        IOMUX_0__PADS_0__OE__GPIOC_2;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_2;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_2;
	wire        IOMUX_0__PADS_0__IE__GPIOC_2;
	wire        IOMUX_0__PADS_0__PE__GPIOC_2;
	wire        IOMUX_0__PADS_0__PS__GPIOC_2;
	wire        IOMUX_0__PADS_0__Y__GPIOD_2;
	wire        IOMUX_0__PADS_0__A__GPIOD_2;
	wire        IOMUX_0__PADS_0__OE__GPIOD_2;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_2;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_2;
	wire        IOMUX_0__PADS_0__IE__GPIOD_2;
	wire        IOMUX_0__PADS_0__PE__GPIOD_2;
	wire        IOMUX_0__PADS_0__PS__GPIOD_2;
	wire        IOMUX_0__PADS_0__Y__GPIOA_3;
	wire        IOMUX_0__PADS_0__A__GPIOA_3;
	wire        IOMUX_0__PADS_0__OE__GPIOA_3;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_3;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_3;
	wire        IOMUX_0__PADS_0__IE__GPIOA_3;
	wire        IOMUX_0__PADS_0__PE__GPIOA_3;
	wire        IOMUX_0__PADS_0__PS__GPIOA_3;
	wire        IOMUX_0__PADS_0__Y__GPIOB_3;
	wire        IOMUX_0__PADS_0__A__GPIOB_3;
	wire        IOMUX_0__PADS_0__OE__GPIOB_3;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_3;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_3;
	wire        IOMUX_0__PADS_0__IE__GPIOB_3;
	wire        IOMUX_0__PADS_0__PE__GPIOB_3;
	wire        IOMUX_0__PADS_0__PS__GPIOB_3;
	wire        IOMUX_0__PADS_0__Y__GPIOC_3;
	wire        IOMUX_0__PADS_0__A__GPIOC_3;
	wire        IOMUX_0__PADS_0__OE__GPIOC_3;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_3;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_3;
	wire        IOMUX_0__PADS_0__IE__GPIOC_3;
	wire        IOMUX_0__PADS_0__PE__GPIOC_3;
	wire        IOMUX_0__PADS_0__PS__GPIOC_3;
	wire        IOMUX_0__PADS_0__Y__GPIOD_3;
	wire        IOMUX_0__PADS_0__A__GPIOD_3;
	wire        IOMUX_0__PADS_0__OE__GPIOD_3;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_3;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_3;
	wire        IOMUX_0__PADS_0__IE__GPIOD_3;
	wire        IOMUX_0__PADS_0__PE__GPIOD_3;
	wire        IOMUX_0__PADS_0__PS__GPIOD_3;
	wire        IOMUX_0__PADS_0__Y__GPIOA_4;
	wire        IOMUX_0__PADS_0__A__GPIOA_4;
	wire        IOMUX_0__PADS_0__OE__GPIOA_4;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_4;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_4;
	wire        IOMUX_0__PADS_0__IE__GPIOA_4;
	wire        IOMUX_0__PADS_0__PE__GPIOA_4;
	wire        IOMUX_0__PADS_0__PS__GPIOA_4;
	wire        IOMUX_0__PADS_0__Y__GPIOB_4;
	wire        IOMUX_0__PADS_0__A__GPIOB_4;
	wire        IOMUX_0__PADS_0__OE__GPIOB_4;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_4;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_4;
	wire        IOMUX_0__PADS_0__IE__GPIOB_4;
	wire        IOMUX_0__PADS_0__PE__GPIOB_4;
	wire        IOMUX_0__PADS_0__PS__GPIOB_4;
	wire        IOMUX_0__PADS_0__Y__GPIOC_4;
	wire        IOMUX_0__PADS_0__A__GPIOC_4;
	wire        IOMUX_0__PADS_0__OE__GPIOC_4;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_4;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_4;
	wire        IOMUX_0__PADS_0__IE__GPIOC_4;
	wire        IOMUX_0__PADS_0__PE__GPIOC_4;
	wire        IOMUX_0__PADS_0__PS__GPIOC_4;
	wire        IOMUX_0__PADS_0__Y__GPIOD_4;
	wire        IOMUX_0__PADS_0__A__GPIOD_4;
	wire        IOMUX_0__PADS_0__OE__GPIOD_4;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_4;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_4;
	wire        IOMUX_0__PADS_0__IE__GPIOD_4;
	wire        IOMUX_0__PADS_0__PE__GPIOD_4;
	wire        IOMUX_0__PADS_0__PS__GPIOD_4;
	wire        IOMUX_0__PADS_0__Y__GPIOA_5;
	wire        IOMUX_0__PADS_0__A__GPIOA_5;
	wire        IOMUX_0__PADS_0__OE__GPIOA_5;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_5;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_5;
	wire        IOMUX_0__PADS_0__IE__GPIOA_5;
	wire        IOMUX_0__PADS_0__PE__GPIOA_5;
	wire        IOMUX_0__PADS_0__PS__GPIOA_5;
	wire        IOMUX_0__PADS_0__Y__GPIOB_5;
	wire        IOMUX_0__PADS_0__A__GPIOB_5;
	wire        IOMUX_0__PADS_0__OE__GPIOB_5;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_5;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_5;
	wire        IOMUX_0__PADS_0__IE__GPIOB_5;
	wire        IOMUX_0__PADS_0__PE__GPIOB_5;
	wire        IOMUX_0__PADS_0__PS__GPIOB_5;
	wire        IOMUX_0__PADS_0__Y__GPIOC_5;
	wire        IOMUX_0__PADS_0__A__GPIOC_5;
	wire        IOMUX_0__PADS_0__OE__GPIOC_5;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_5;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_5;
	wire        IOMUX_0__PADS_0__IE__GPIOC_5;
	wire        IOMUX_0__PADS_0__PE__GPIOC_5;
	wire        IOMUX_0__PADS_0__PS__GPIOC_5;
	wire        IOMUX_0__PADS_0__Y__GPIOD_5;
	wire        IOMUX_0__PADS_0__A__GPIOD_5;
	wire        IOMUX_0__PADS_0__OE__GPIOD_5;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_5;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_5;
	wire        IOMUX_0__PADS_0__IE__GPIOD_5;
	wire        IOMUX_0__PADS_0__PE__GPIOD_5;
	wire        IOMUX_0__PADS_0__PS__GPIOD_5;
	wire        IOMUX_0__PADS_0__Y__GPIOA_6;
	wire        IOMUX_0__PADS_0__A__GPIOA_6;
	wire        IOMUX_0__PADS_0__OE__GPIOA_6;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_6;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_6;
	wire        IOMUX_0__PADS_0__IE__GPIOA_6;
	wire        IOMUX_0__PADS_0__PE__GPIOA_6;
	wire        IOMUX_0__PADS_0__PS__GPIOA_6;
	wire        IOMUX_0__PADS_0__Y__GPIOB_6;
	wire        IOMUX_0__PADS_0__A__GPIOB_6;
	wire        IOMUX_0__PADS_0__OE__GPIOB_6;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_6;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_6;
	wire        IOMUX_0__PADS_0__IE__GPIOB_6;
	wire        IOMUX_0__PADS_0__PE__GPIOB_6;
	wire        IOMUX_0__PADS_0__PS__GPIOB_6;
	wire        IOMUX_0__PADS_0__Y__GPIOC_6;
	wire        IOMUX_0__PADS_0__A__GPIOC_6;
	wire        IOMUX_0__PADS_0__OE__GPIOC_6;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_6;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_6;
	wire        IOMUX_0__PADS_0__IE__GPIOC_6;
	wire        IOMUX_0__PADS_0__PE__GPIOC_6;
	wire        IOMUX_0__PADS_0__PS__GPIOC_6;
	wire        IOMUX_0__PADS_0__Y__GPIOD_6;
	wire        IOMUX_0__PADS_0__A__GPIOD_6;
	wire        IOMUX_0__PADS_0__OE__GPIOD_6;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_6;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_6;
	wire        IOMUX_0__PADS_0__IE__GPIOD_6;
	wire        IOMUX_0__PADS_0__PE__GPIOD_6;
	wire        IOMUX_0__PADS_0__PS__GPIOD_6;
	wire        IOMUX_0__PADS_0__Y__GPIOA_7;
	wire        IOMUX_0__PADS_0__A__GPIOA_7;
	wire        IOMUX_0__PADS_0__OE__GPIOA_7;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_7;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_7;
	wire        IOMUX_0__PADS_0__IE__GPIOA_7;
	wire        IOMUX_0__PADS_0__PE__GPIOA_7;
	wire        IOMUX_0__PADS_0__PS__GPIOA_7;
	wire        IOMUX_0__PADS_0__Y__GPIOB_7;
	wire        IOMUX_0__PADS_0__A__GPIOB_7;
	wire        IOMUX_0__PADS_0__OE__GPIOB_7;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_7;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_7;
	wire        IOMUX_0__PADS_0__IE__GPIOB_7;
	wire        IOMUX_0__PADS_0__PE__GPIOB_7;
	wire        IOMUX_0__PADS_0__PS__GPIOB_7;
	wire        IOMUX_0__PADS_0__Y__GPIOC_7;
	wire        IOMUX_0__PADS_0__A__GPIOC_7;
	wire        IOMUX_0__PADS_0__OE__GPIOC_7;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_7;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_7;
	wire        IOMUX_0__PADS_0__IE__GPIOC_7;
	wire        IOMUX_0__PADS_0__PE__GPIOC_7;
	wire        IOMUX_0__PADS_0__PS__GPIOC_7;
	wire        IOMUX_0__PADS_0__Y__GPIOD_7;
	wire        IOMUX_0__PADS_0__A__GPIOD_7;
	wire        IOMUX_0__PADS_0__OE__GPIOD_7;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_7;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_7;
	wire        IOMUX_0__PADS_0__IE__GPIOD_7;
	wire        IOMUX_0__PADS_0__PE__GPIOD_7;
	wire        IOMUX_0__PADS_0__PS__GPIOD_7;
	wire        IOMUX_0__PADS_0__Y__GPIOA_8;
	wire        IOMUX_0__PADS_0__A__GPIOA_8;
	wire        IOMUX_0__PADS_0__OE__GPIOA_8;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_8;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_8;
	wire        IOMUX_0__PADS_0__IE__GPIOA_8;
	wire        IOMUX_0__PADS_0__PE__GPIOA_8;
	wire        IOMUX_0__PADS_0__PS__GPIOA_8;
	wire        IOMUX_0__PADS_0__Y__GPIOB_8;
	wire        IOMUX_0__PADS_0__A__GPIOB_8;
	wire        IOMUX_0__PADS_0__OE__GPIOB_8;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_8;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_8;
	wire        IOMUX_0__PADS_0__IE__GPIOB_8;
	wire        IOMUX_0__PADS_0__PE__GPIOB_8;
	wire        IOMUX_0__PADS_0__PS__GPIOB_8;
	wire        IOMUX_0__PADS_0__Y__GPIOC_8;
	wire        IOMUX_0__PADS_0__A__GPIOC_8;
	wire        IOMUX_0__PADS_0__OE__GPIOC_8;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_8;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_8;
	wire        IOMUX_0__PADS_0__IE__GPIOC_8;
	wire        IOMUX_0__PADS_0__PE__GPIOC_8;
	wire        IOMUX_0__PADS_0__PS__GPIOC_8;
	wire        IOMUX_0__PADS_0__Y__GPIOD_8;
	wire        IOMUX_0__PADS_0__A__GPIOD_8;
	wire        IOMUX_0__PADS_0__OE__GPIOD_8;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_8;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_8;
	wire        IOMUX_0__PADS_0__IE__GPIOD_8;
	wire        IOMUX_0__PADS_0__PE__GPIOD_8;
	wire        IOMUX_0__PADS_0__PS__GPIOD_8;
	wire        IOMUX_0__PADS_0__Y__GPIOA_9;
	wire        IOMUX_0__PADS_0__A__GPIOA_9;
	wire        IOMUX_0__PADS_0__OE__GPIOA_9;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_9;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_9;
	wire        IOMUX_0__PADS_0__IE__GPIOA_9;
	wire        IOMUX_0__PADS_0__PE__GPIOA_9;
	wire        IOMUX_0__PADS_0__PS__GPIOA_9;
	wire        IOMUX_0__PADS_0__Y__GPIOB_9;
	wire        IOMUX_0__PADS_0__A__GPIOB_9;
	wire        IOMUX_0__PADS_0__OE__GPIOB_9;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_9;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_9;
	wire        IOMUX_0__PADS_0__IE__GPIOB_9;
	wire        IOMUX_0__PADS_0__PE__GPIOB_9;
	wire        IOMUX_0__PADS_0__PS__GPIOB_9;
	wire        IOMUX_0__PADS_0__Y__GPIOC_9;
	wire        IOMUX_0__PADS_0__A__GPIOC_9;
	wire        IOMUX_0__PADS_0__OE__GPIOC_9;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_9;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_9;
	wire        IOMUX_0__PADS_0__IE__GPIOC_9;
	wire        IOMUX_0__PADS_0__PE__GPIOC_9;
	wire        IOMUX_0__PADS_0__PS__GPIOC_9;
	wire        IOMUX_0__PADS_0__Y__GPIOD_9;
	wire        IOMUX_0__PADS_0__A__GPIOD_9;
	wire        IOMUX_0__PADS_0__OE__GPIOD_9;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_9;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_9;
	wire        IOMUX_0__PADS_0__IE__GPIOD_9;
	wire        IOMUX_0__PADS_0__PE__GPIOD_9;
	wire        IOMUX_0__PADS_0__PS__GPIOD_9;
	wire        IOMUX_0__PADS_0__Y__GPIOA_10;
	wire        IOMUX_0__PADS_0__A__GPIOA_10;
	wire        IOMUX_0__PADS_0__OE__GPIOA_10;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_10;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_10;
	wire        IOMUX_0__PADS_0__IE__GPIOA_10;
	wire        IOMUX_0__PADS_0__PE__GPIOA_10;
	wire        IOMUX_0__PADS_0__PS__GPIOA_10;
	wire        IOMUX_0__PADS_0__Y__GPIOB_10;
	wire        IOMUX_0__PADS_0__A__GPIOB_10;
	wire        IOMUX_0__PADS_0__OE__GPIOB_10;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_10;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_10;
	wire        IOMUX_0__PADS_0__IE__GPIOB_10;
	wire        IOMUX_0__PADS_0__PE__GPIOB_10;
	wire        IOMUX_0__PADS_0__PS__GPIOB_10;
	wire        IOMUX_0__PADS_0__Y__GPIOC_10;
	wire        IOMUX_0__PADS_0__A__GPIOC_10;
	wire        IOMUX_0__PADS_0__OE__GPIOC_10;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_10;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_10;
	wire        IOMUX_0__PADS_0__IE__GPIOC_10;
	wire        IOMUX_0__PADS_0__PE__GPIOC_10;
	wire        IOMUX_0__PADS_0__PS__GPIOC_10;
	wire        IOMUX_0__PADS_0__Y__GPIOD_10;
	wire        IOMUX_0__PADS_0__A__GPIOD_10;
	wire        IOMUX_0__PADS_0__OE__GPIOD_10;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_10;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_10;
	wire        IOMUX_0__PADS_0__IE__GPIOD_10;
	wire        IOMUX_0__PADS_0__PE__GPIOD_10;
	wire        IOMUX_0__PADS_0__PS__GPIOD_10;
	wire        IOMUX_0__PADS_0__Y__GPIOA_11;
	wire        IOMUX_0__PADS_0__A__GPIOA_11;
	wire        IOMUX_0__PADS_0__OE__GPIOA_11;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_11;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_11;
	wire        IOMUX_0__PADS_0__IE__GPIOA_11;
	wire        IOMUX_0__PADS_0__PE__GPIOA_11;
	wire        IOMUX_0__PADS_0__PS__GPIOA_11;
	wire        IOMUX_0__PADS_0__Y__GPIOB_11;
	wire        IOMUX_0__PADS_0__A__GPIOB_11;
	wire        IOMUX_0__PADS_0__OE__GPIOB_11;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_11;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_11;
	wire        IOMUX_0__PADS_0__IE__GPIOB_11;
	wire        IOMUX_0__PADS_0__PE__GPIOB_11;
	wire        IOMUX_0__PADS_0__PS__GPIOB_11;
	wire        IOMUX_0__PADS_0__Y__GPIOC_11;
	wire        IOMUX_0__PADS_0__A__GPIOC_11;
	wire        IOMUX_0__PADS_0__OE__GPIOC_11;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_11;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_11;
	wire        IOMUX_0__PADS_0__IE__GPIOC_11;
	wire        IOMUX_0__PADS_0__PE__GPIOC_11;
	wire        IOMUX_0__PADS_0__PS__GPIOC_11;
	wire        IOMUX_0__PADS_0__Y__GPIOD_11;
	wire        IOMUX_0__PADS_0__A__GPIOD_11;
	wire        IOMUX_0__PADS_0__OE__GPIOD_11;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_11;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_11;
	wire        IOMUX_0__PADS_0__IE__GPIOD_11;
	wire        IOMUX_0__PADS_0__PE__GPIOD_11;
	wire        IOMUX_0__PADS_0__PS__GPIOD_11;
	wire        IOMUX_0__PADS_0__Y__GPIOA_12;
	wire        IOMUX_0__PADS_0__A__GPIOA_12;
	wire        IOMUX_0__PADS_0__OE__GPIOA_12;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_12;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_12;
	wire        IOMUX_0__PADS_0__IE__GPIOA_12;
	wire        IOMUX_0__PADS_0__PE__GPIOA_12;
	wire        IOMUX_0__PADS_0__PS__GPIOA_12;
	wire        IOMUX_0__PADS_0__Y__GPIOB_12;
	wire        IOMUX_0__PADS_0__A__GPIOB_12;
	wire        IOMUX_0__PADS_0__OE__GPIOB_12;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_12;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_12;
	wire        IOMUX_0__PADS_0__IE__GPIOB_12;
	wire        IOMUX_0__PADS_0__PE__GPIOB_12;
	wire        IOMUX_0__PADS_0__PS__GPIOB_12;
	wire        IOMUX_0__PADS_0__Y__GPIOC_12;
	wire        IOMUX_0__PADS_0__A__GPIOC_12;
	wire        IOMUX_0__PADS_0__OE__GPIOC_12;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_12;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_12;
	wire        IOMUX_0__PADS_0__IE__GPIOC_12;
	wire        IOMUX_0__PADS_0__PE__GPIOC_12;
	wire        IOMUX_0__PADS_0__PS__GPIOC_12;
	wire        IOMUX_0__PADS_0__Y__GPIOD_12;
	wire        IOMUX_0__PADS_0__A__GPIOD_12;
	wire        IOMUX_0__PADS_0__OE__GPIOD_12;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_12;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_12;
	wire        IOMUX_0__PADS_0__IE__GPIOD_12;
	wire        IOMUX_0__PADS_0__PE__GPIOD_12;
	wire        IOMUX_0__PADS_0__PS__GPIOD_12;
	wire        IOMUX_0__PADS_0__Y__GPIOA_13;
	wire        IOMUX_0__PADS_0__A__GPIOA_13;
	wire        IOMUX_0__PADS_0__OE__GPIOA_13;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_13;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_13;
	wire        IOMUX_0__PADS_0__IE__GPIOA_13;
	wire        IOMUX_0__PADS_0__PE__GPIOA_13;
	wire        IOMUX_0__PADS_0__PS__GPIOA_13;
	wire        IOMUX_0__PADS_0__Y__GPIOB_13;
	wire        IOMUX_0__PADS_0__A__GPIOB_13;
	wire        IOMUX_0__PADS_0__OE__GPIOB_13;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_13;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_13;
	wire        IOMUX_0__PADS_0__IE__GPIOB_13;
	wire        IOMUX_0__PADS_0__PE__GPIOB_13;
	wire        IOMUX_0__PADS_0__PS__GPIOB_13;
	wire        IOMUX_0__PADS_0__Y__GPIOC_13;
	wire        IOMUX_0__PADS_0__A__GPIOC_13;
	wire        IOMUX_0__PADS_0__OE__GPIOC_13;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_13;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_13;
	wire        IOMUX_0__PADS_0__IE__GPIOC_13;
	wire        IOMUX_0__PADS_0__PE__GPIOC_13;
	wire        IOMUX_0__PADS_0__PS__GPIOC_13;
	wire        IOMUX_0__PADS_0__Y__GPIOD_13;
	wire        IOMUX_0__PADS_0__A__GPIOD_13;
	wire        IOMUX_0__PADS_0__OE__GPIOD_13;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_13;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_13;
	wire        IOMUX_0__PADS_0__IE__GPIOD_13;
	wire        IOMUX_0__PADS_0__PE__GPIOD_13;
	wire        IOMUX_0__PADS_0__PS__GPIOD_13;
	wire        IOMUX_0__PADS_0__Y__GPIOA_14;
	wire        IOMUX_0__PADS_0__A__GPIOA_14;
	wire        IOMUX_0__PADS_0__OE__GPIOA_14;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_14;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_14;
	wire        IOMUX_0__PADS_0__IE__GPIOA_14;
	wire        IOMUX_0__PADS_0__PE__GPIOA_14;
	wire        IOMUX_0__PADS_0__PS__GPIOA_14;
	wire        IOMUX_0__PADS_0__Y__GPIOB_14;
	wire        IOMUX_0__PADS_0__A__GPIOB_14;
	wire        IOMUX_0__PADS_0__OE__GPIOB_14;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_14;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_14;
	wire        IOMUX_0__PADS_0__IE__GPIOB_14;
	wire        IOMUX_0__PADS_0__PE__GPIOB_14;
	wire        IOMUX_0__PADS_0__PS__GPIOB_14;
	wire        IOMUX_0__PADS_0__Y__GPIOC_14;
	wire        IOMUX_0__PADS_0__A__GPIOC_14;
	wire        IOMUX_0__PADS_0__OE__GPIOC_14;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_14;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_14;
	wire        IOMUX_0__PADS_0__IE__GPIOC_14;
	wire        IOMUX_0__PADS_0__PE__GPIOC_14;
	wire        IOMUX_0__PADS_0__PS__GPIOC_14;
	wire        IOMUX_0__PADS_0__Y__GPIOD_14;
	wire        IOMUX_0__PADS_0__A__GPIOD_14;
	wire        IOMUX_0__PADS_0__OE__GPIOD_14;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_14;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_14;
	wire        IOMUX_0__PADS_0__IE__GPIOD_14;
	wire        IOMUX_0__PADS_0__PE__GPIOD_14;
	wire        IOMUX_0__PADS_0__PS__GPIOD_14;
	wire        IOMUX_0__PADS_0__Y__GPIOA_15;
	wire        IOMUX_0__PADS_0__A__GPIOA_15;
	wire        IOMUX_0__PADS_0__OE__GPIOA_15;
	wire        IOMUX_0__PADS_0__DS0__GPIOA_15;
	wire        IOMUX_0__PADS_0__DS1__GPIOA_15;
	wire        IOMUX_0__PADS_0__IE__GPIOA_15;
	wire        IOMUX_0__PADS_0__PE__GPIOA_15;
	wire        IOMUX_0__PADS_0__PS__GPIOA_15;
	wire        IOMUX_0__PADS_0__Y__GPIOB_15;
	wire        IOMUX_0__PADS_0__A__GPIOB_15;
	wire        IOMUX_0__PADS_0__OE__GPIOB_15;
	wire        IOMUX_0__PADS_0__DS0__GPIOB_15;
	wire        IOMUX_0__PADS_0__DS1__GPIOB_15;
	wire        IOMUX_0__PADS_0__IE__GPIOB_15;
	wire        IOMUX_0__PADS_0__PE__GPIOB_15;
	wire        IOMUX_0__PADS_0__PS__GPIOB_15;
	wire        IOMUX_0__PADS_0__Y__GPIOC_15;
	wire        IOMUX_0__PADS_0__A__GPIOC_15;
	wire        IOMUX_0__PADS_0__OE__GPIOC_15;
	wire        IOMUX_0__PADS_0__DS0__GPIOC_15;
	wire        IOMUX_0__PADS_0__DS1__GPIOC_15;
	wire        IOMUX_0__PADS_0__IE__GPIOC_15;
	wire        IOMUX_0__PADS_0__PE__GPIOC_15;
	wire        IOMUX_0__PADS_0__PS__GPIOC_15;
	wire        IOMUX_0__PADS_0__Y__GPIOD_15;
	wire        IOMUX_0__PADS_0__A__GPIOD_15;
	wire        IOMUX_0__PADS_0__OE__GPIOD_15;
	wire        IOMUX_0__PADS_0__DS0__GPIOD_15;
	wire        IOMUX_0__PADS_0__DS1__GPIOD_15;
	wire        IOMUX_0__PADS_0__IE__GPIOD_15;
	wire        IOMUX_0__PADS_0__PE__GPIOD_15;
	wire        IOMUX_0__PADS_0__PS__GPIOD_15;
	wire        IOMUX_0__PADS_0__Y__SDMMC_0;
	wire        IOMUX_0__PADS_0__A__SDMMC_0;
	wire        IOMUX_0__PADS_0__OE__SDMMC_0;
	wire        IOMUX_0__PADS_0__DS0__SDMMC_0;
	wire        IOMUX_0__PADS_0__DS1__SDMMC_0;
	wire        IOMUX_0__PADS_0__IE__SDMMC_0;
	wire        IOMUX_0__PADS_0__PE__SDMMC_0;
	wire        IOMUX_0__PADS_0__PS__SDMMC_0;
	wire        IOMUX_0__PADS_0__Y__SDMMC_1;
	wire        IOMUX_0__PADS_0__A__SDMMC_1;
	wire        IOMUX_0__PADS_0__OE__SDMMC_1;
	wire        IOMUX_0__PADS_0__DS0__SDMMC_1;
	wire        IOMUX_0__PADS_0__DS1__SDMMC_1;
	wire        IOMUX_0__PADS_0__IE__SDMMC_1;
	wire        IOMUX_0__PADS_0__PE__SDMMC_1;
	wire        IOMUX_0__PADS_0__PS__SDMMC_1;
	wire        IOMUX_0__PADS_0__Y__SDMMC_2;
	wire        IOMUX_0__PADS_0__A__SDMMC_2;
	wire        IOMUX_0__PADS_0__OE__SDMMC_2;
	wire        IOMUX_0__PADS_0__DS0__SDMMC_2;
	wire        IOMUX_0__PADS_0__DS1__SDMMC_2;
	wire        IOMUX_0__PADS_0__IE__SDMMC_2;
	wire        IOMUX_0__PADS_0__PE__SDMMC_2;
	wire        IOMUX_0__PADS_0__PS__SDMMC_2;
	wire        IOMUX_0__PADS_0__Y__SDMMC_3;
	wire        IOMUX_0__PADS_0__A__SDMMC_3;
	wire        IOMUX_0__PADS_0__OE__SDMMC_3;
	wire        IOMUX_0__PADS_0__DS0__SDMMC_3;
	wire        IOMUX_0__PADS_0__DS1__SDMMC_3;
	wire        IOMUX_0__PADS_0__IE__SDMMC_3;
	wire        IOMUX_0__PADS_0__PE__SDMMC_3;
	wire        IOMUX_0__PADS_0__PS__SDMMC_3;
	wire        IOMUX_0__PADS_0__Y__SDMMC_4;
	wire        IOMUX_0__PADS_0__A__SDMMC_4;
	wire        IOMUX_0__PADS_0__OE__SDMMC_4;
	wire        IOMUX_0__PADS_0__DS0__SDMMC_4;
	wire        IOMUX_0__PADS_0__DS1__SDMMC_4;
	wire        IOMUX_0__PADS_0__IE__SDMMC_4;
	wire        IOMUX_0__PADS_0__PE__SDMMC_4;
	wire        IOMUX_0__PADS_0__PS__SDMMC_4;
	wire        IOMUX_0__PADS_0__Y__SDMMC_5;
	wire        IOMUX_0__PADS_0__A__SDMMC_5;
	wire        IOMUX_0__PADS_0__OE__SDMMC_5;
	wire        IOMUX_0__PADS_0__DS0__SDMMC_5;
	wire        IOMUX_0__PADS_0__DS1__SDMMC_5;
	wire        IOMUX_0__PADS_0__IE__SDMMC_5;
	wire        IOMUX_0__PADS_0__PE__SDMMC_5;
	wire        IOMUX_0__PADS_0__PS__SDMMC_5;
	wire        IOMUX_0__PADS_0__Y__SDMMC_6;
	wire        IOMUX_0__PADS_0__A__SDMMC_6;
	wire        IOMUX_0__PADS_0__OE__SDMMC_6;
	wire        IOMUX_0__PADS_0__DS0__SDMMC_6;
	wire        IOMUX_0__PADS_0__DS1__SDMMC_6;
	wire        IOMUX_0__PADS_0__IE__SDMMC_6;
	wire        IOMUX_0__PADS_0__PE__SDMMC_6;
	wire        IOMUX_0__PADS_0__PS__SDMMC_6;
	wire        IOMUX_0__PADS_0__Y__SDMMC_7;
	wire        IOMUX_0__PADS_0__A__SDMMC_7;
	wire        IOMUX_0__PADS_0__OE__SDMMC_7;
	wire        IOMUX_0__PADS_0__DS0__SDMMC_7;
	wire        IOMUX_0__PADS_0__DS1__SDMMC_7;
	wire        IOMUX_0__PADS_0__IE__SDMMC_7;
	wire        IOMUX_0__PADS_0__PE__SDMMC_7;
	wire        IOMUX_0__PADS_0__PS__SDMMC_7;
	wire        IOMUX_0__PADS_0__Y__SDMMC_8;
	wire        IOMUX_0__PADS_0__A__SDMMC_8;
	wire        IOMUX_0__PADS_0__OE__SDMMC_8;
	wire        IOMUX_0__PADS_0__DS0__SDMMC_8;
	wire        IOMUX_0__PADS_0__DS1__SDMMC_8;
	wire        IOMUX_0__PADS_0__IE__SDMMC_8;
	wire        IOMUX_0__PADS_0__PE__SDMMC_8;
	wire        IOMUX_0__PADS_0__PS__SDMMC_8;
	wire        IOMUX_0__PADS_0__Y__SDMMC_9;
	wire        IOMUX_0__PADS_0__A__SDMMC_9;
	wire        IOMUX_0__PADS_0__OE__SDMMC_9;
	wire        IOMUX_0__PADS_0__DS0__SDMMC_9;
	wire        IOMUX_0__PADS_0__DS1__SDMMC_9;
	wire        IOMUX_0__PADS_0__IE__SDMMC_9;
	wire        IOMUX_0__PADS_0__PE__SDMMC_9;
	wire        IOMUX_0__PADS_0__PS__SDMMC_9;
	wire        IOMUX_0__PADS_0__Y__SDMMC_10;
	wire        IOMUX_0__PADS_0__A__SDMMC_10;
	wire        IOMUX_0__PADS_0__OE__SDMMC_10;
	wire        IOMUX_0__PADS_0__DS0__SDMMC_10;
	wire        IOMUX_0__PADS_0__DS1__SDMMC_10;
	wire        IOMUX_0__PADS_0__IE__SDMMC_10;
	wire        IOMUX_0__PADS_0__PE__SDMMC_10;
	wire        IOMUX_0__PADS_0__PS__SDMMC_10;
	wire        IOMUX_0__PADS_0__Y__SDMMC_11;
	wire        IOMUX_0__PADS_0__A__SDMMC_11;
	wire        IOMUX_0__PADS_0__OE__SDMMC_11;
	wire        IOMUX_0__PADS_0__DS0__SDMMC_11;
	wire        IOMUX_0__PADS_0__DS1__SDMMC_11;
	wire        IOMUX_0__PADS_0__IE__SDMMC_11;
	wire        IOMUX_0__PADS_0__PE__SDMMC_11;
	wire        IOMUX_0__PADS_0__PS__SDMMC_11;

	core
	CORE_0 (
		 .PKG__NPU2_DISABLE                                     (CORE_0__PADS_0__PKG__NPU2_DISABLE)
		,.PKG__NPU0_DISABLE                                     (CORE_0__PADS_0__PKG__NPU0_DISABLE)
		,.PKG__NPU1_DISABLE                                     (CORE_0__PADS_0__PKG__NPU1_DISABLE)
		,.IO__RSTNOUT                                           (CORE_0__PADS_0__IO__RSTNOUT)
		,.IO__CLK__OSC                                          (CORE_0__PADS_0__IO__CLK__OSC)
		,.IO__CLK__XTAL                                         (CORE_0__PADS_0__IO__CLK__XTAL)
		,.IO__CLKSEL                                            (CORE_0__PADS_0__IO__CLKSEL)
		,.IO__RSTN                                              (CORE_0__PADS_0__IO__RSTN)
		,.CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TCK                  (CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TCK)
		,.CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TRST                 (CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TRST)
		,.CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TMS                  (CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TMS)
		,.CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TDI                  (CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TDI)
		,.CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TDO                  (CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TDO)
		,.CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TDOEN                (CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TDOEN)
		,.CPU_SUBSYSTEM_0__CPU_0__IO__TRACE_TREF                ()
		,.CPU_SUBSYSTEM_0__CPU_0__IO__TRACE_TDATA               ()
		,.PERI_SUBSYSTEM_0__I2C_0__IO__SCLI                     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_0__IO__SCLI)
		,.PERI_SUBSYSTEM_0__I2C_0__IO__SCLOE                    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_0__IO__SCLOE)
		,.PERI_SUBSYSTEM_0__I2C_0__IO__SDIN                     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_0__IO__SDIN)
		,.PERI_SUBSYSTEM_0__I2C_0__IO__SDOE                     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_0__IO__SDOE)
		,.PERI_SUBSYSTEM_0__I2C_1__IO__SCLI                     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_1__IO__SCLI)
		,.PERI_SUBSYSTEM_0__I2C_1__IO__SCLOE                    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_1__IO__SCLOE)
		,.PERI_SUBSYSTEM_0__I2C_1__IO__SDIN                     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_1__IO__SDIN)
		,.PERI_SUBSYSTEM_0__I2C_1__IO__SDOE                     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_1__IO__SDOE)
		,.PERI_SUBSYSTEM_0__I2C_2__IO__SCLI                     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_2__IO__SCLI)
		,.PERI_SUBSYSTEM_0__I2C_2__IO__SCLOE                    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_2__IO__SCLOE)
		,.PERI_SUBSYSTEM_0__I2C_2__IO__SDIN                     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_2__IO__SDIN)
		,.PERI_SUBSYSTEM_0__I2C_2__IO__SDOE                     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_2__IO__SDOE)
		,.PERI_SUBSYSTEM_0__I2C_3__IO__SCLI                     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_3__IO__SCLI)
		,.PERI_SUBSYSTEM_0__I2C_3__IO__SCLOE                    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_3__IO__SCLOE)
		,.PERI_SUBSYSTEM_0__I2C_3__IO__SDIN                     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_3__IO__SDIN)
		,.PERI_SUBSYSTEM_0__I2C_3__IO__SDOE                     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_3__IO__SDOE)
		,.PERI_SUBSYSTEM_0__SPI_0__IO__SCK                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_0__IO__SCK)
		,.PERI_SUBSYSTEM_0__SPI_0__IO__SCS                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_0__IO__SCS)
		,.PERI_SUBSYSTEM_0__SPI_0__IO__SDO                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_0__IO__SDO)
		,.PERI_SUBSYSTEM_0__SPI_0__IO__SDI                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_0__IO__SDI)
		,.PERI_SUBSYSTEM_0__SPI_0__IO__OEN                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_0__IO__OEN)
		,.PERI_SUBSYSTEM_0__SPI_1__IO__SCK                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_1__IO__SCK)
		,.PERI_SUBSYSTEM_0__SPI_1__IO__SCS                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_1__IO__SCS)
		,.PERI_SUBSYSTEM_0__SPI_1__IO__SDO                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_1__IO__SDO)
		,.PERI_SUBSYSTEM_0__SPI_1__IO__SDI                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_1__IO__SDI)
		,.PERI_SUBSYSTEM_0__SPI_1__IO__OEN                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_1__IO__OEN)
		,.PERI_SUBSYSTEM_0__SPI_2__IO__SCK                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_2__IO__SCK)
		,.PERI_SUBSYSTEM_0__SPI_2__IO__SCS                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_2__IO__SCS)
		,.PERI_SUBSYSTEM_0__SPI_2__IO__SDO                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_2__IO__SDO)
		,.PERI_SUBSYSTEM_0__SPI_2__IO__SDI                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_2__IO__SDI)
		,.PERI_SUBSYSTEM_0__SPI_2__IO__OEN                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_2__IO__OEN)
		,.PERI_SUBSYSTEM_0__SPI_3__IO__SCK                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_3__IO__SCK)
		,.PERI_SUBSYSTEM_0__SPI_3__IO__SCS                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_3__IO__SCS)
		,.PERI_SUBSYSTEM_0__SPI_3__IO__SDO                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_3__IO__SDO)
		,.PERI_SUBSYSTEM_0__SPI_3__IO__SDI                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_3__IO__SDI)
		,.PERI_SUBSYSTEM_0__SPI_3__IO__OEN                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_3__IO__OEN)
		,.PERI_SUBSYSTEM_0__SDMMC_0__IO__CLK                    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__CLK)
		,.PERI_SUBSYSTEM_0__SDMMC_0__IO__CMD                    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__CMD)
		,.PERI_SUBSYSTEM_0__SDMMC_0__IO__RSP                    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__RSP)
		,.PERI_SUBSYSTEM_0__SDMMC_0__IO__CMDOEN                 (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__CMDOEN)
		,.PERI_SUBSYSTEM_0__SDMMC_0__IO__DOUT                   (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__DOUT)
		,.PERI_SUBSYSTEM_0__SDMMC_0__IO__DIN                    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__DIN)
		,.PERI_SUBSYSTEM_0__SDMMC_0__IO__DOEN                   (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__DOEN)
		,.PERI_SUBSYSTEM_0__SDMMC_0__IO__DQS                    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__DQS)
		,.PERI_SUBSYSTEM_0__SDMMC_0__IO__RSTN                   (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__RSTN)
		,.PERI_SUBSYSTEM_0__OTP_0__IO__ADD                      (15'h0)
		,.PERI_SUBSYSTEM_0__OTP_0__IO__CEB                      (1'h0)
		,.PERI_SUBSYSTEM_0__OTP_0__IO__CLE                      (1'h0)
		,.PERI_SUBSYSTEM_0__OTP_0__IO__DLE                      (1'h0)
		,.PERI_SUBSYSTEM_0__OTP_0__IO__PGMEN                    (1'h0)
		,.PERI_SUBSYSTEM_0__OTP_0__IO__READEN                   (1'h0)
		,.PERI_SUBSYSTEM_0__OTP_0__IO__RSTB                     (1'h0)
		,.PERI_SUBSYSTEM_0__OTP_0__IO__WEB                      (1'h0)
		,.PERI_SUBSYSTEM_0__OTP_0__IO__CPUMPEN                  (1'h0)
		,.PERI_SUBSYSTEM_0__OTP_0__IO__DIN                      (1'h0)
		,.PERI_SUBSYSTEM_0__OTP_0__IO__DOUT                     ()
		,.PERI_SUBSYSTEM_0__OTP_0__IO__VGB_ANALOG               ()
		,.PERI_SUBSYSTEM_0__OTP_0__IO__VTDO_ANALOG              ()
		,.PERI_SUBSYSTEM_0__OTP_0__IO__VREFM_ANALOG             ()
		,.PERI_SUBSYSTEM_0__OTP_0__IO__VPP_ANALOG               ()
		,.PERI_SUBSYSTEM_0__PVT_0__IO__VSENSE_TS_ANALOG         ()
		,.PERI_SUBSYSTEM_0__PVT_0__IO__VOL_TS_ANALOG            ()
		,.PERI_SUBSYSTEM_0__PVT_0__IO__VREFT_FLAG_TS            (1'h0)
		,.PERI_SUBSYSTEM_0__PVT_0__IO__VBE_FLAG_TS              (1'h0)
		,.PERI_SUBSYSTEM_0__PVT_0__IO__IBIAS_TS_ANALOG          ()
		,.PERI_SUBSYSTEM_0__PVT_0__IO__TEST_OUT_TS_ANALOG       ()
		,.PERI_SUBSYSTEM_0__GPIO_A__IO__Y                       (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_A__IO__Y)
		,.PERI_SUBSYSTEM_0__GPIO_A__IO__A                       (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_A__IO__A)
		,.PERI_SUBSYSTEM_0__GPIO_A__IO__OE                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_A__IO__OE)
		,.PERI_SUBSYSTEM_0__GPIO_B__IO__Y                       (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_B__IO__Y)
		,.PERI_SUBSYSTEM_0__GPIO_B__IO__A                       (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_B__IO__A)
		,.PERI_SUBSYSTEM_0__GPIO_B__IO__OE                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_B__IO__OE)
		,.PERI_SUBSYSTEM_0__GPIO_C__IO__Y                       (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_C__IO__Y)
		,.PERI_SUBSYSTEM_0__GPIO_C__IO__A                       (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_C__IO__A)
		,.PERI_SUBSYSTEM_0__GPIO_C__IO__OE                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_C__IO__OE)
		,.PERI_SUBSYSTEM_0__GPIO_D__IO__Y                       (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_D__IO__Y)
		,.PERI_SUBSYSTEM_0__GPIO_D__IO__A                       (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_D__IO__A)
		,.PERI_SUBSYSTEM_0__GPIO_D__IO__OE                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_D__IO__OE)
		,.PERI_SUBSYSTEM_0__UART_0__IO__RX                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__UART_0__IO__RX)
		,.PERI_SUBSYSTEM_0__UART_0__IO__TX                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__UART_0__IO__TX)
		,.PERI_SUBSYSTEM_0__UART_0__IO__CTS                     (1'h0)
		,.PERI_SUBSYSTEM_0__UART_0__IO__RTS                     ()
		,.PERI_SUBSYSTEM_0__UART_1__IO__RX                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__UART_1__IO__RX)
		,.PERI_SUBSYSTEM_0__UART_1__IO__TX                      (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__UART_1__IO__TX)
		,.PERI_SUBSYSTEM_0__UART_1__IO__CTS                     (1'h0)
		,.PERI_SUBSYSTEM_0__UART_1__IO__RTS                     ()
		,.USB3_SUBSYSTEM_0__USB3_0__IO__VBUS                    (USB3_0__VBUS)
		,.USB3_SUBSYSTEM_0__USB3_0__IO__DM                      (USB3_0__DM)
		,.USB3_SUBSYSTEM_0__USB3_0__IO__DP                      (USB3_0__DP)
		,.USB3_SUBSYSTEM_0__USB3_0__IO__ID                      (USB3_0__ID)
		,.USB3_SUBSYSTEM_0__USB3_0__IO__RESREF                  (USB3_0__RESREF)
		,.USB3_SUBSYSTEM_0__USB3_0__IO__TXM                     (USB3_0__TXM)
		,.USB3_SUBSYSTEM_0__USB3_0__IO__TXP                     (USB3_0__TXP)
		,.USB3_SUBSYSTEM_0__USB3_0__IO__RXM                     (USB3_0__RXM)
		,.USB3_SUBSYSTEM_0__USB3_0__IO__RXP                     (USB3_0__RXP)
		,.USB3_SUBSYSTEM_0__USB3_0__IO__REFPADCLKM              (USB3_0__REFPADCLKM)
		,.USB3_SUBSYSTEM_0__USB3_0__IO__REFPADCLKP              (USB3_0__REFPADCLKP)
		,.USB3_SUBSYSTEM_0__USB3_0__IO__TEST__USB3PHY_MODE      (1'h0)
		,.USB3_SUBSYSTEM_0__USB3_0__IO__TEST__USB3PHY_IN        (100'h0)
		,.USB3_SUBSYSTEM_0__USB3_0__IO__TEST__USB3PHY_OUT       ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_TX_CLK        ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_TX_DATA       ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_TX_DATAK      ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_PCLK          (1'h0)
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_RX_DATA       (16'h0)
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_RX_DATAK      (2'h0)
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_RX_VALID      (1'h0)
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_PHY_RESETN    ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_TX_DETRX_LPBK ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_TX_ELECIDLE   ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_RX_ELECIDLE   ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_RX_STATUS     (3'h0)
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_POWER_DOWN    ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_PHY_STATUS    (1'h0)
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_PWRPRESENT    (1'h0)
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_TX_ONESZEROS  ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_TX_DEEMPH     ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_TX_MARGIN     ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_TX_SWING      ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_RX_POLARITY   ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_RX_TERMINATION()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_RATE          ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__PIPE3_ELAS_BUF_MODE ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__ULPI_CLK            (1'h0)
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__ULPI_DATAOUT        ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__ULPI_DATAIN         (8'h0)
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__ULPI_DIR            (1'h0)
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__ULPI_STP            ()
		,.USB3_SUBSYSTEM_0__USB3_0__FPGAIO__ULPI_NXT            (1'h0)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__ADCT                (LPDDR4_0_ADCT)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__CK                  (LPDDR4_0_CK)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__CKB                 (LPDDR4_0_CKB)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__CKE                 (LPDDR4_0_CKE)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__CS                  (LPDDR4_0_CS)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__DM_A                (LPDDR4_0_DM_A)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__DM_B                (LPDDR4_0_DM_B)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__DQ_A                (LPDDR4_0_DQ_A)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__DQ_B                (LPDDR4_0_DQ_B)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__NDQS_A              (LPDDR4_0_NDQS_A)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__NDQS_B              (LPDDR4_0_NDQS_B)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__ODT                 (LPDDR4_0_ODT)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__PDQS_A              (LPDDR4_0_PDQS_A)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__PDQS_B              (LPDDR4_0_PDQS_B)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__RESET               (LPDDR4_0_RESET)
		,.LPDDR4_SUBSYSTEM_0__LPDDR4_0__IO__ZQ                  (LPDDR4_0_ZQ)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__ADCT                (LPDDR4_1_ADCT)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__CK                  (LPDDR4_1_CK)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__CKB                 (LPDDR4_1_CKB)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__CKE                 (LPDDR4_1_CKE)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__CS                  (LPDDR4_1_CS)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__DM_A                (LPDDR4_1_DM_A)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__DM_B                (LPDDR4_1_DM_B)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__DQ_A                (LPDDR4_1_DQ_A)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__DQ_B                (LPDDR4_1_DQ_B)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__NDQS_A              (LPDDR4_1_NDQS_A)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__NDQS_B              (LPDDR4_1_NDQS_B)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__ODT                 (LPDDR4_1_ODT)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__PDQS_A              (LPDDR4_1_PDQS_A)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__PDQS_B              (LPDDR4_1_PDQS_B)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__RESET               (LPDDR4_1_RESET)
		,.LPDDR4_SUBSYSTEM_1__LPDDR4_0__IO__ZQ                  (LPDDR4_1_ZQ)
	);

	pads_temp
	PADS_0 (
		 .PAD__RSTN       (RSTN)
		,.PAD__RSTN_OUT   (RSTN_OUT)
		,.PAD__PKG0       (PKG0)
		,.PAD__PKG1       (PKG1)
		,.PAD__PKG2       (PKG2)
		,.PAD__JTAG_TCK   (JTAG_TCK)
		,.PAD__JTAG_TMS   (JTAG_TMS)
		,.PAD__JTAG_TDI   (JTAG_TDI)
		,.PAD__JTAG_TDO   (JTAG_TDO)
		,.PAD__JTAG_TRST  (JTAG_TRST)
		,.PAD__GPIOA_0    (GPIOA_0)
		,.PAD__GPIOA_1    (GPIOA_1)
		,.PAD__GPIOA_2    (GPIOA_2)
		,.PAD__GPIOA_3    (GPIOA_3)
		,.PAD__GPIOA_4    (GPIOA_4)
		,.PAD__GPIOA_5    (GPIOA_5)
		,.PAD__GPIOA_6    (GPIOA_6)
		,.PAD__GPIOA_7    (GPIOA_7)
		,.PAD__GPIOA_8    (GPIOA_8)
		,.PAD__GPIOA_9    (GPIOA_9)
		,.PAD__GPIOA_10   (GPIOA_10)
		,.PAD__GPIOA_11   (GPIOA_11)
		,.PAD__GPIOA_12   (GPIOA_12)
		,.PAD__GPIOA_13   (GPIOA_13)
		,.PAD__GPIOA_14   (GPIOA_14)
		,.PAD__GPIOA_15   (GPIOA_15)
		,.PAD__GPIOB_0    (GPIOB_0)
		,.PAD__GPIOB_1    (GPIOB_1)
		,.PAD__GPIOB_2    (GPIOB_2)
		,.PAD__GPIOB_3    (GPIOB_3)
		,.PAD__GPIOB_4    (GPIOB_4)
		,.PAD__GPIOB_5    (GPIOB_5)
		,.PAD__GPIOB_6    (GPIOB_6)
		,.PAD__GPIOB_7    (GPIOB_7)
		,.PAD__GPIOB_8    (GPIOB_8)
		,.PAD__GPIOB_9    (GPIOB_9)
		,.PAD__GPIOB_10   (GPIOB_10)
		,.PAD__GPIOB_11   (GPIOB_11)
		,.PAD__GPIOB_12   (GPIOB_12)
		,.PAD__GPIOB_13   (GPIOB_13)
		,.PAD__GPIOB_14   (GPIOB_14)
		,.PAD__GPIOB_15   (GPIOB_15)
		,.PAD__GPIOC_0    (GPIOC_0)
		,.PAD__GPIOC_1    (GPIOC_1)
		,.PAD__GPIOC_2    (GPIOC_2)
		,.PAD__GPIOC_3    (GPIOC_3)
		,.PAD__GPIOC_4    (GPIOC_4)
		,.PAD__GPIOC_5    (GPIOC_5)
		,.PAD__GPIOC_6    (GPIOC_6)
		,.PAD__GPIOC_7    (GPIOC_7)
		,.PAD__GPIOC_8    (GPIOC_8)
		,.PAD__GPIOC_9    (GPIOC_9)
		,.PAD__GPIOC_10   (GPIOC_10)
		,.PAD__GPIOC_11   (GPIOC_11)
		,.PAD__GPIOC_12   (GPIOC_12)
		,.PAD__GPIOC_13   (GPIOC_13)
		,.PAD__GPIOC_14   (GPIOC_14)
		,.PAD__GPIOC_15   (GPIOC_15)
		,.PAD__GPIOD_0    (GPIOD_0)
		,.PAD__GPIOD_1    (GPIOD_1)
		,.PAD__GPIOD_2    (GPIOD_2)
		,.PAD__GPIOD_3    (GPIOD_3)
		,.PAD__GPIOD_4    (GPIOD_4)
		,.PAD__GPIOD_5    (GPIOD_5)
		,.PAD__GPIOD_6    (GPIOD_6)
		,.PAD__GPIOD_7    (GPIOD_7)
		,.PAD__GPIOD_8    (GPIOD_8)
		,.PAD__GPIOD_9    (GPIOD_9)
		,.PAD__GPIOD_10   (GPIOD_10)
		,.PAD__GPIOD_11   (GPIOD_11)
		,.PAD__GPIOD_12   (GPIOD_12)
		,.PAD__GPIOD_13   (GPIOD_13)
		,.PAD__GPIOD_14   (GPIOD_14)
		,.PAD__GPIOD_15   (GPIOD_15)
		,.PAD__SDMMC_0    (SDMMC_0)
		,.PAD__SDMMC_1    (SDMMC_1)
		,.PAD__SDMMC_2    (SDMMC_2)
		,.PAD__SDMMC_3    (SDMMC_3)
		,.PAD__SDMMC_4    (SDMMC_4)
		,.PAD__SDMMC_5    (SDMMC_5)
		,.PAD__SDMMC_6    (SDMMC_6)
		,.PAD__SDMMC_7    (SDMMC_7)
		,.PAD__SDMMC_8    (SDMMC_8)
		,.PAD__SDMMC_9    (SDMMC_9)
		,.PAD__SDMMC_10   (SDMMC_10)
		,.PAD__SDMMC_11   (SDMMC_11)
		,.PAD__OTP_0_VPP  (OTP_0_VPP)
		,.PAD__OTP_0_VBG  (OTP_0_VBG)
		,.PAD__OTP_0_VTDO (OTP_0_VTDO)
		,.PAD__OTP_0_VREFM(OTP_0_VREFM)
		,.PAD__OTP_1_VPP  (OTP_1_VPP)
		,.PAD__OTP_1_VBG  (OTP_1_VBG)
		,.PAD__OTP_1_VTDO (OTP_1_VTDO)
		,.PAD__OTP_1_VREFM(OTP_1_VREFM)
		,.AY__OTP_0_VPP   ()
		,.AY__OTP_0_VBG   ()
		,.AY__OTP_0_VTDO  ()
		,.AY__OTP_0_VREFM ()
		,.AY__OTP_1_VPP   ()
		,.AY__OTP_1_VBG   ()
		,.AY__OTP_1_VTDO  ()
		,.AY__OTP_1_VREFM ()
		,.PAD_OUT__XTAL   (XTALO)
		,.PAD_IN__XTAL    (XTALI)
		,.E0__XTAL        (1'h0)
		,.TE__XTAL        (1'h0)
		,.SP__XTAL        (1'h0)
		,.SF0__XTAL       (1'h0)
		,.SF1__XTAL       (1'h0)
		,.CK__XTAL        (CORE_0__PADS_0__IO__CLK__XTAL)
		,.CK_IOV__XTAL    ()
		,.SNS__XTAL       (1'h0)
		,.PO__XTAL        ()
		,.POE__XTAL       (1'h0)
		,.RTO__XTAL       (1'h0)
		,.PAD_OUT__OSC    ()
		,.PAD_IN__OSC     (OSC)
		,.E0__OSC         (1'h0)
		,.TE__OSC         (1'h0)
		,.SP__OSC         (1'h0)
		,.SF0__OSC        (1'h0)
		,.SF1__OSC        (1'h0)
		,.CK__OSC         (CORE_0__PADS_0__IO__CLK__OSC)
		,.CK_IOV__OSC     ()
		,.SNS__OSC        (1'h0)
		,.PO__OSC         ()
		,.POE__OSC        (1'h0)
		,.RTO__OSC        (1'h0)
		,.PAD__CLKSEL     (CLKSEL)
		,.Y__CLKSEL       (CORE_0__PADS_0__IO__CLKSEL)
		,.A__RSTN         (1'h0)
		,.A__RSTN_OUT     (CORE_0__PADS_0__IO__RSTNOUT)
		,.A__PKG0         (1'h0)
		,.A__PKG1         (1'h0)
		,.A__PKG2         (1'h0)
		,.A__JTAG_TCK     (1'h0)
		,.A__JTAG_TMS     (1'h0)
		,.A__JTAG_TDI     (1'h0)
		,.A__JTAG_TDO     (CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TDO)
		,.A__JTAG_TRST    (1'h0)
		,.A__GPIOA_0      (IOMUX_0__PADS_0__A__GPIOA_0)
		,.A__GPIOA_1      (IOMUX_0__PADS_0__A__GPIOA_1)
		,.A__GPIOA_2      (IOMUX_0__PADS_0__A__GPIOA_2)
		,.A__GPIOA_3      (IOMUX_0__PADS_0__A__GPIOA_3)
		,.A__GPIOA_4      (IOMUX_0__PADS_0__A__GPIOA_4)
		,.A__GPIOA_5      (IOMUX_0__PADS_0__A__GPIOA_5)
		,.A__GPIOA_6      (IOMUX_0__PADS_0__A__GPIOA_6)
		,.A__GPIOA_7      (IOMUX_0__PADS_0__A__GPIOA_7)
		,.A__GPIOA_8      (IOMUX_0__PADS_0__A__GPIOA_8)
		,.A__GPIOA_9      (IOMUX_0__PADS_0__A__GPIOA_9)
		,.A__GPIOA_10     (IOMUX_0__PADS_0__A__GPIOA_10)
		,.A__GPIOA_11     (IOMUX_0__PADS_0__A__GPIOA_11)
		,.A__GPIOA_12     (IOMUX_0__PADS_0__A__GPIOA_12)
		,.A__GPIOA_13     (IOMUX_0__PADS_0__A__GPIOA_13)
		,.A__GPIOA_14     (IOMUX_0__PADS_0__A__GPIOA_14)
		,.A__GPIOA_15     (IOMUX_0__PADS_0__A__GPIOA_15)
		,.A__GPIOB_0      (IOMUX_0__PADS_0__A__GPIOB_0)
		,.A__GPIOB_1      (IOMUX_0__PADS_0__A__GPIOB_1)
		,.A__GPIOB_2      (IOMUX_0__PADS_0__A__GPIOB_2)
		,.A__GPIOB_3      (IOMUX_0__PADS_0__A__GPIOB_3)
		,.A__GPIOB_4      (IOMUX_0__PADS_0__A__GPIOB_4)
		,.A__GPIOB_5      (IOMUX_0__PADS_0__A__GPIOB_5)
		,.A__GPIOB_6      (IOMUX_0__PADS_0__A__GPIOB_6)
		,.A__GPIOB_7      (IOMUX_0__PADS_0__A__GPIOB_7)
		,.A__GPIOB_8      (IOMUX_0__PADS_0__A__GPIOB_8)
		,.A__GPIOB_9      (IOMUX_0__PADS_0__A__GPIOB_9)
		,.A__GPIOB_10     (IOMUX_0__PADS_0__A__GPIOB_10)
		,.A__GPIOB_11     (IOMUX_0__PADS_0__A__GPIOB_11)
		,.A__GPIOB_12     (IOMUX_0__PADS_0__A__GPIOB_12)
		,.A__GPIOB_13     (IOMUX_0__PADS_0__A__GPIOB_13)
		,.A__GPIOB_14     (IOMUX_0__PADS_0__A__GPIOB_14)
		,.A__GPIOB_15     (IOMUX_0__PADS_0__A__GPIOB_15)
		,.A__GPIOC_0      (IOMUX_0__PADS_0__A__GPIOC_0)
		,.A__GPIOC_1      (IOMUX_0__PADS_0__A__GPIOC_1)
		,.A__GPIOC_2      (IOMUX_0__PADS_0__A__GPIOC_2)
		,.A__GPIOC_3      (IOMUX_0__PADS_0__A__GPIOC_3)
		,.A__GPIOC_4      (IOMUX_0__PADS_0__A__GPIOC_4)
		,.A__GPIOC_5      (IOMUX_0__PADS_0__A__GPIOC_5)
		,.A__GPIOC_6      (IOMUX_0__PADS_0__A__GPIOC_6)
		,.A__GPIOC_7      (IOMUX_0__PADS_0__A__GPIOC_7)
		,.A__GPIOC_8      (IOMUX_0__PADS_0__A__GPIOC_8)
		,.A__GPIOC_9      (IOMUX_0__PADS_0__A__GPIOC_9)
		,.A__GPIOC_10     (IOMUX_0__PADS_0__A__GPIOC_10)
		,.A__GPIOC_11     (IOMUX_0__PADS_0__A__GPIOC_11)
		,.A__GPIOC_12     (IOMUX_0__PADS_0__A__GPIOC_12)
		,.A__GPIOC_13     (IOMUX_0__PADS_0__A__GPIOC_13)
		,.A__GPIOC_14     (IOMUX_0__PADS_0__A__GPIOC_14)
		,.A__GPIOC_15     (IOMUX_0__PADS_0__A__GPIOC_15)
		,.A__GPIOD_0      (IOMUX_0__PADS_0__A__GPIOD_0)
		,.A__GPIOD_1      (IOMUX_0__PADS_0__A__GPIOD_1)
		,.A__GPIOD_2      (IOMUX_0__PADS_0__A__GPIOD_2)
		,.A__GPIOD_3      (IOMUX_0__PADS_0__A__GPIOD_3)
		,.A__GPIOD_4      (IOMUX_0__PADS_0__A__GPIOD_4)
		,.A__GPIOD_5      (IOMUX_0__PADS_0__A__GPIOD_5)
		,.A__GPIOD_6      (IOMUX_0__PADS_0__A__GPIOD_6)
		,.A__GPIOD_7      (IOMUX_0__PADS_0__A__GPIOD_7)
		,.A__GPIOD_8      (IOMUX_0__PADS_0__A__GPIOD_8)
		,.A__GPIOD_9      (IOMUX_0__PADS_0__A__GPIOD_9)
		,.A__GPIOD_10     (IOMUX_0__PADS_0__A__GPIOD_10)
		,.A__GPIOD_11     (IOMUX_0__PADS_0__A__GPIOD_11)
		,.A__GPIOD_12     (IOMUX_0__PADS_0__A__GPIOD_12)
		,.A__GPIOD_13     (IOMUX_0__PADS_0__A__GPIOD_13)
		,.A__GPIOD_14     (IOMUX_0__PADS_0__A__GPIOD_14)
		,.A__GPIOD_15     (IOMUX_0__PADS_0__A__GPIOD_15)
		,.A__SDMMC_0      (IOMUX_0__PADS_0__A__SDMMC_0)
		,.A__SDMMC_1      (IOMUX_0__PADS_0__A__SDMMC_1)
		,.A__SDMMC_2      (IOMUX_0__PADS_0__A__SDMMC_2)
		,.A__SDMMC_3      (IOMUX_0__PADS_0__A__SDMMC_3)
		,.A__SDMMC_4      (IOMUX_0__PADS_0__A__SDMMC_4)
		,.A__SDMMC_5      (IOMUX_0__PADS_0__A__SDMMC_5)
		,.A__SDMMC_6      (IOMUX_0__PADS_0__A__SDMMC_6)
		,.A__SDMMC_7      (IOMUX_0__PADS_0__A__SDMMC_7)
		,.A__SDMMC_8      (IOMUX_0__PADS_0__A__SDMMC_8)
		,.A__SDMMC_9      (IOMUX_0__PADS_0__A__SDMMC_9)
		,.A__SDMMC_10     (IOMUX_0__PADS_0__A__SDMMC_10)
		,.A__SDMMC_11     (IOMUX_0__PADS_0__A__SDMMC_11)
		,.Y__RSTN         (CORE_0__PADS_0__IO__RSTN)
		,.Y__RSTN_OUT     ()
		,.Y__PKG0         (CORE_0__PADS_0__PKG__NPU0_DISABLE)
		,.Y__PKG1         (CORE_0__PADS_0__PKG__NPU1_DISABLE)
		,.Y__PKG2         (CORE_0__PADS_0__PKG__NPU2_DISABLE)
		,.Y__JTAG_TCK     (CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TCK)
		,.Y__JTAG_TMS     (CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TMS)
		,.Y__JTAG_TDI     (CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TDI)
		,.Y__JTAG_TDO     ()
		,.Y__JTAG_TRST    (CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TRST)
		,.Y__GPIOA_0      (IOMUX_0__PADS_0__Y__GPIOA_0)
		,.Y__GPIOA_1      (IOMUX_0__PADS_0__Y__GPIOA_1)
		,.Y__GPIOA_2      (IOMUX_0__PADS_0__Y__GPIOA_2)
		,.Y__GPIOA_3      (IOMUX_0__PADS_0__Y__GPIOA_3)
		,.Y__GPIOA_4      (IOMUX_0__PADS_0__Y__GPIOA_4)
		,.Y__GPIOA_5      (IOMUX_0__PADS_0__Y__GPIOA_5)
		,.Y__GPIOA_6      (IOMUX_0__PADS_0__Y__GPIOA_6)
		,.Y__GPIOA_7      (IOMUX_0__PADS_0__Y__GPIOA_7)
		,.Y__GPIOA_8      (IOMUX_0__PADS_0__Y__GPIOA_8)
		,.Y__GPIOA_9      (IOMUX_0__PADS_0__Y__GPIOA_9)
		,.Y__GPIOA_10     (IOMUX_0__PADS_0__Y__GPIOA_10)
		,.Y__GPIOA_11     (IOMUX_0__PADS_0__Y__GPIOA_11)
		,.Y__GPIOA_12     (IOMUX_0__PADS_0__Y__GPIOA_12)
		,.Y__GPIOA_13     (IOMUX_0__PADS_0__Y__GPIOA_13)
		,.Y__GPIOA_14     (IOMUX_0__PADS_0__Y__GPIOA_14)
		,.Y__GPIOA_15     (IOMUX_0__PADS_0__Y__GPIOA_15)
		,.Y__GPIOB_0      (IOMUX_0__PADS_0__Y__GPIOB_0)
		,.Y__GPIOB_1      (IOMUX_0__PADS_0__Y__GPIOB_1)
		,.Y__GPIOB_2      (IOMUX_0__PADS_0__Y__GPIOB_2)
		,.Y__GPIOB_3      (IOMUX_0__PADS_0__Y__GPIOB_3)
		,.Y__GPIOB_4      (IOMUX_0__PADS_0__Y__GPIOB_4)
		,.Y__GPIOB_5      (IOMUX_0__PADS_0__Y__GPIOB_5)
		,.Y__GPIOB_6      (IOMUX_0__PADS_0__Y__GPIOB_6)
		,.Y__GPIOB_7      (IOMUX_0__PADS_0__Y__GPIOB_7)
		,.Y__GPIOB_8      (IOMUX_0__PADS_0__Y__GPIOB_8)
		,.Y__GPIOB_9      (IOMUX_0__PADS_0__Y__GPIOB_9)
		,.Y__GPIOB_10     (IOMUX_0__PADS_0__Y__GPIOB_10)
		,.Y__GPIOB_11     (IOMUX_0__PADS_0__Y__GPIOB_11)
		,.Y__GPIOB_12     (IOMUX_0__PADS_0__Y__GPIOB_12)
		,.Y__GPIOB_13     (IOMUX_0__PADS_0__Y__GPIOB_13)
		,.Y__GPIOB_14     (IOMUX_0__PADS_0__Y__GPIOB_14)
		,.Y__GPIOB_15     (IOMUX_0__PADS_0__Y__GPIOB_15)
		,.Y__GPIOC_0      (IOMUX_0__PADS_0__Y__GPIOC_0)
		,.Y__GPIOC_1      (IOMUX_0__PADS_0__Y__GPIOC_1)
		,.Y__GPIOC_2      (IOMUX_0__PADS_0__Y__GPIOC_2)
		,.Y__GPIOC_3      (IOMUX_0__PADS_0__Y__GPIOC_3)
		,.Y__GPIOC_4      (IOMUX_0__PADS_0__Y__GPIOC_4)
		,.Y__GPIOC_5      (IOMUX_0__PADS_0__Y__GPIOC_5)
		,.Y__GPIOC_6      (IOMUX_0__PADS_0__Y__GPIOC_6)
		,.Y__GPIOC_7      (IOMUX_0__PADS_0__Y__GPIOC_7)
		,.Y__GPIOC_8      (IOMUX_0__PADS_0__Y__GPIOC_8)
		,.Y__GPIOC_9      (IOMUX_0__PADS_0__Y__GPIOC_9)
		,.Y__GPIOC_10     (IOMUX_0__PADS_0__Y__GPIOC_10)
		,.Y__GPIOC_11     (IOMUX_0__PADS_0__Y__GPIOC_11)
		,.Y__GPIOC_12     (IOMUX_0__PADS_0__Y__GPIOC_12)
		,.Y__GPIOC_13     (IOMUX_0__PADS_0__Y__GPIOC_13)
		,.Y__GPIOC_14     (IOMUX_0__PADS_0__Y__GPIOC_14)
		,.Y__GPIOC_15     (IOMUX_0__PADS_0__Y__GPIOC_15)
		,.Y__GPIOD_0      (IOMUX_0__PADS_0__Y__GPIOD_0)
		,.Y__GPIOD_1      (IOMUX_0__PADS_0__Y__GPIOD_1)
		,.Y__GPIOD_2      (IOMUX_0__PADS_0__Y__GPIOD_2)
		,.Y__GPIOD_3      (IOMUX_0__PADS_0__Y__GPIOD_3)
		,.Y__GPIOD_4      (IOMUX_0__PADS_0__Y__GPIOD_4)
		,.Y__GPIOD_5      (IOMUX_0__PADS_0__Y__GPIOD_5)
		,.Y__GPIOD_6      (IOMUX_0__PADS_0__Y__GPIOD_6)
		,.Y__GPIOD_7      (IOMUX_0__PADS_0__Y__GPIOD_7)
		,.Y__GPIOD_8      (IOMUX_0__PADS_0__Y__GPIOD_8)
		,.Y__GPIOD_9      (IOMUX_0__PADS_0__Y__GPIOD_9)
		,.Y__GPIOD_10     (IOMUX_0__PADS_0__Y__GPIOD_10)
		,.Y__GPIOD_11     (IOMUX_0__PADS_0__Y__GPIOD_11)
		,.Y__GPIOD_12     (IOMUX_0__PADS_0__Y__GPIOD_12)
		,.Y__GPIOD_13     (IOMUX_0__PADS_0__Y__GPIOD_13)
		,.Y__GPIOD_14     (IOMUX_0__PADS_0__Y__GPIOD_14)
		,.Y__GPIOD_15     (IOMUX_0__PADS_0__Y__GPIOD_15)
		,.Y__SDMMC_0      (IOMUX_0__PADS_0__Y__SDMMC_0)
		,.Y__SDMMC_1      (IOMUX_0__PADS_0__Y__SDMMC_1)
		,.Y__SDMMC_2      (IOMUX_0__PADS_0__Y__SDMMC_2)
		,.Y__SDMMC_3      (IOMUX_0__PADS_0__Y__SDMMC_3)
		,.Y__SDMMC_4      (IOMUX_0__PADS_0__Y__SDMMC_4)
		,.Y__SDMMC_5      (IOMUX_0__PADS_0__Y__SDMMC_5)
		,.Y__SDMMC_6      (IOMUX_0__PADS_0__Y__SDMMC_6)
		,.Y__SDMMC_7      (IOMUX_0__PADS_0__Y__SDMMC_7)
		,.Y__SDMMC_8      (IOMUX_0__PADS_0__Y__SDMMC_8)
		,.Y__SDMMC_9      (IOMUX_0__PADS_0__Y__SDMMC_9)
		,.Y__SDMMC_10     (IOMUX_0__PADS_0__Y__SDMMC_10)
		,.Y__SDMMC_11     (IOMUX_0__PADS_0__Y__SDMMC_11)
		,.OE__RSTN        (1'h0)
		,.OE__RSTN_OUT    (1'h1)
		,.OE__PKG0        (1'h0)
		,.OE__PKG1        (1'h0)
		,.OE__PKG2        (1'h0)
		,.OE__JTAG_TCK    (1'h0)
		,.OE__JTAG_TMS    (1'h0)
		,.OE__JTAG_TDI    (1'h0)
		,.OE__JTAG_TDO    (CORE_0__PADS_0__CPU_SUBSYSTEM_0__CPU_0__IO__JTAG_TDOEN)
		,.OE__JTAG_TRST   (1'h0)
		,.OE__GPIOA_0     (IOMUX_0__PADS_0__OE__GPIOA_0)
		,.OE__GPIOA_1     (IOMUX_0__PADS_0__OE__GPIOA_1)
		,.OE__GPIOA_2     (IOMUX_0__PADS_0__OE__GPIOA_2)
		,.OE__GPIOA_3     (IOMUX_0__PADS_0__OE__GPIOA_3)
		,.OE__GPIOA_4     (IOMUX_0__PADS_0__OE__GPIOA_4)
		,.OE__GPIOA_5     (IOMUX_0__PADS_0__OE__GPIOA_5)
		,.OE__GPIOA_6     (IOMUX_0__PADS_0__OE__GPIOA_6)
		,.OE__GPIOA_7     (IOMUX_0__PADS_0__OE__GPIOA_7)
		,.OE__GPIOA_8     (IOMUX_0__PADS_0__OE__GPIOA_8)
		,.OE__GPIOA_9     (IOMUX_0__PADS_0__OE__GPIOA_9)
		,.OE__GPIOA_10    (IOMUX_0__PADS_0__OE__GPIOA_10)
		,.OE__GPIOA_11    (IOMUX_0__PADS_0__OE__GPIOA_11)
		,.OE__GPIOA_12    (IOMUX_0__PADS_0__OE__GPIOA_12)
		,.OE__GPIOA_13    (IOMUX_0__PADS_0__OE__GPIOA_13)
		,.OE__GPIOA_14    (IOMUX_0__PADS_0__OE__GPIOA_14)
		,.OE__GPIOA_15    (IOMUX_0__PADS_0__OE__GPIOA_15)
		,.OE__GPIOB_0     (IOMUX_0__PADS_0__OE__GPIOB_0)
		,.OE__GPIOB_1     (IOMUX_0__PADS_0__OE__GPIOB_1)
		,.OE__GPIOB_2     (IOMUX_0__PADS_0__OE__GPIOB_2)
		,.OE__GPIOB_3     (IOMUX_0__PADS_0__OE__GPIOB_3)
		,.OE__GPIOB_4     (IOMUX_0__PADS_0__OE__GPIOB_4)
		,.OE__GPIOB_5     (IOMUX_0__PADS_0__OE__GPIOB_5)
		,.OE__GPIOB_6     (IOMUX_0__PADS_0__OE__GPIOB_6)
		,.OE__GPIOB_7     (IOMUX_0__PADS_0__OE__GPIOB_7)
		,.OE__GPIOB_8     (IOMUX_0__PADS_0__OE__GPIOB_8)
		,.OE__GPIOB_9     (IOMUX_0__PADS_0__OE__GPIOB_9)
		,.OE__GPIOB_10    (IOMUX_0__PADS_0__OE__GPIOB_10)
		,.OE__GPIOB_11    (IOMUX_0__PADS_0__OE__GPIOB_11)
		,.OE__GPIOB_12    (IOMUX_0__PADS_0__OE__GPIOB_12)
		,.OE__GPIOB_13    (IOMUX_0__PADS_0__OE__GPIOB_13)
		,.OE__GPIOB_14    (IOMUX_0__PADS_0__OE__GPIOB_14)
		,.OE__GPIOB_15    (IOMUX_0__PADS_0__OE__GPIOB_15)
		,.OE__GPIOC_0     (IOMUX_0__PADS_0__OE__GPIOC_0)
		,.OE__GPIOC_1     (IOMUX_0__PADS_0__OE__GPIOC_1)
		,.OE__GPIOC_2     (IOMUX_0__PADS_0__OE__GPIOC_2)
		,.OE__GPIOC_3     (IOMUX_0__PADS_0__OE__GPIOC_3)
		,.OE__GPIOC_4     (IOMUX_0__PADS_0__OE__GPIOC_4)
		,.OE__GPIOC_5     (IOMUX_0__PADS_0__OE__GPIOC_5)
		,.OE__GPIOC_6     (IOMUX_0__PADS_0__OE__GPIOC_6)
		,.OE__GPIOC_7     (IOMUX_0__PADS_0__OE__GPIOC_7)
		,.OE__GPIOC_8     (IOMUX_0__PADS_0__OE__GPIOC_8)
		,.OE__GPIOC_9     (IOMUX_0__PADS_0__OE__GPIOC_9)
		,.OE__GPIOC_10    (IOMUX_0__PADS_0__OE__GPIOC_10)
		,.OE__GPIOC_11    (IOMUX_0__PADS_0__OE__GPIOC_11)
		,.OE__GPIOC_12    (IOMUX_0__PADS_0__OE__GPIOC_12)
		,.OE__GPIOC_13    (IOMUX_0__PADS_0__OE__GPIOC_13)
		,.OE__GPIOC_14    (IOMUX_0__PADS_0__OE__GPIOC_14)
		,.OE__GPIOC_15    (IOMUX_0__PADS_0__OE__GPIOC_15)
		,.OE__GPIOD_0     (IOMUX_0__PADS_0__OE__GPIOD_0)
		,.OE__GPIOD_1     (IOMUX_0__PADS_0__OE__GPIOD_1)
		,.OE__GPIOD_2     (IOMUX_0__PADS_0__OE__GPIOD_2)
		,.OE__GPIOD_3     (IOMUX_0__PADS_0__OE__GPIOD_3)
		,.OE__GPIOD_4     (IOMUX_0__PADS_0__OE__GPIOD_4)
		,.OE__GPIOD_5     (IOMUX_0__PADS_0__OE__GPIOD_5)
		,.OE__GPIOD_6     (IOMUX_0__PADS_0__OE__GPIOD_6)
		,.OE__GPIOD_7     (IOMUX_0__PADS_0__OE__GPIOD_7)
		,.OE__GPIOD_8     (IOMUX_0__PADS_0__OE__GPIOD_8)
		,.OE__GPIOD_9     (IOMUX_0__PADS_0__OE__GPIOD_9)
		,.OE__GPIOD_10    (IOMUX_0__PADS_0__OE__GPIOD_10)
		,.OE__GPIOD_11    (IOMUX_0__PADS_0__OE__GPIOD_11)
		,.OE__GPIOD_12    (IOMUX_0__PADS_0__OE__GPIOD_12)
		,.OE__GPIOD_13    (IOMUX_0__PADS_0__OE__GPIOD_13)
		,.OE__GPIOD_14    (IOMUX_0__PADS_0__OE__GPIOD_14)
		,.OE__GPIOD_15    (IOMUX_0__PADS_0__OE__GPIOD_15)
		,.OE__SDMMC_0     (IOMUX_0__PADS_0__OE__SDMMC_0)
		,.OE__SDMMC_1     (IOMUX_0__PADS_0__OE__SDMMC_1)
		,.OE__SDMMC_2     (IOMUX_0__PADS_0__OE__SDMMC_2)
		,.OE__SDMMC_3     (IOMUX_0__PADS_0__OE__SDMMC_3)
		,.OE__SDMMC_4     (IOMUX_0__PADS_0__OE__SDMMC_4)
		,.OE__SDMMC_5     (IOMUX_0__PADS_0__OE__SDMMC_5)
		,.OE__SDMMC_6     (IOMUX_0__PADS_0__OE__SDMMC_6)
		,.OE__SDMMC_7     (IOMUX_0__PADS_0__OE__SDMMC_7)
		,.OE__SDMMC_8     (IOMUX_0__PADS_0__OE__SDMMC_8)
		,.OE__SDMMC_9     (IOMUX_0__PADS_0__OE__SDMMC_9)
		,.OE__SDMMC_10    (IOMUX_0__PADS_0__OE__SDMMC_10)
		,.OE__SDMMC_11    (IOMUX_0__PADS_0__OE__SDMMC_11)
		,.IE__RSTN        (1'h1)
		,.IE__RSTN_OUT    (1'h1)
		,.IE__PKG0        (1'h1)
		,.IE__PKG1        (1'h1)
		,.IE__PKG2        (1'h1)
		,.IE__JTAG_TCK    (1'h1)
		,.IE__JTAG_TMS    (1'h1)
		,.IE__JTAG_TDI    (1'h1)
		,.IE__JTAG_TDO    (1'h1)
		,.IE__JTAG_TRST   (1'h1)
		,.IE__GPIOA_0     (IOMUX_0__PADS_0__IE__GPIOA_0)
		,.IE__GPIOA_1     (IOMUX_0__PADS_0__IE__GPIOA_1)
		,.IE__GPIOA_2     (IOMUX_0__PADS_0__IE__GPIOA_2)
		,.IE__GPIOA_3     (IOMUX_0__PADS_0__IE__GPIOA_3)
		,.IE__GPIOA_4     (IOMUX_0__PADS_0__IE__GPIOA_4)
		,.IE__GPIOA_5     (IOMUX_0__PADS_0__IE__GPIOA_5)
		,.IE__GPIOA_6     (IOMUX_0__PADS_0__IE__GPIOA_6)
		,.IE__GPIOA_7     (IOMUX_0__PADS_0__IE__GPIOA_7)
		,.IE__GPIOA_8     (IOMUX_0__PADS_0__IE__GPIOA_8)
		,.IE__GPIOA_9     (IOMUX_0__PADS_0__IE__GPIOA_9)
		,.IE__GPIOA_10    (IOMUX_0__PADS_0__IE__GPIOA_10)
		,.IE__GPIOA_11    (IOMUX_0__PADS_0__IE__GPIOA_11)
		,.IE__GPIOA_12    (IOMUX_0__PADS_0__IE__GPIOA_12)
		,.IE__GPIOA_13    (IOMUX_0__PADS_0__IE__GPIOA_13)
		,.IE__GPIOA_14    (IOMUX_0__PADS_0__IE__GPIOA_14)
		,.IE__GPIOA_15    (IOMUX_0__PADS_0__IE__GPIOA_15)
		,.IE__GPIOB_0     (IOMUX_0__PADS_0__IE__GPIOB_0)
		,.IE__GPIOB_1     (IOMUX_0__PADS_0__IE__GPIOB_1)
		,.IE__GPIOB_2     (IOMUX_0__PADS_0__IE__GPIOB_2)
		,.IE__GPIOB_3     (IOMUX_0__PADS_0__IE__GPIOB_3)
		,.IE__GPIOB_4     (IOMUX_0__PADS_0__IE__GPIOB_4)
		,.IE__GPIOB_5     (IOMUX_0__PADS_0__IE__GPIOB_5)
		,.IE__GPIOB_6     (IOMUX_0__PADS_0__IE__GPIOB_6)
		,.IE__GPIOB_7     (IOMUX_0__PADS_0__IE__GPIOB_7)
		,.IE__GPIOB_8     (IOMUX_0__PADS_0__IE__GPIOB_8)
		,.IE__GPIOB_9     (IOMUX_0__PADS_0__IE__GPIOB_9)
		,.IE__GPIOB_10    (IOMUX_0__PADS_0__IE__GPIOB_10)
		,.IE__GPIOB_11    (IOMUX_0__PADS_0__IE__GPIOB_11)
		,.IE__GPIOB_12    (IOMUX_0__PADS_0__IE__GPIOB_12)
		,.IE__GPIOB_13    (IOMUX_0__PADS_0__IE__GPIOB_13)
		,.IE__GPIOB_14    (IOMUX_0__PADS_0__IE__GPIOB_14)
		,.IE__GPIOB_15    (IOMUX_0__PADS_0__IE__GPIOB_15)
		,.IE__GPIOC_0     (IOMUX_0__PADS_0__IE__GPIOC_0)
		,.IE__GPIOC_1     (IOMUX_0__PADS_0__IE__GPIOC_1)
		,.IE__GPIOC_2     (IOMUX_0__PADS_0__IE__GPIOC_2)
		,.IE__GPIOC_3     (IOMUX_0__PADS_0__IE__GPIOC_3)
		,.IE__GPIOC_4     (IOMUX_0__PADS_0__IE__GPIOC_4)
		,.IE__GPIOC_5     (IOMUX_0__PADS_0__IE__GPIOC_5)
		,.IE__GPIOC_6     (IOMUX_0__PADS_0__IE__GPIOC_6)
		,.IE__GPIOC_7     (IOMUX_0__PADS_0__IE__GPIOC_7)
		,.IE__GPIOC_8     (IOMUX_0__PADS_0__IE__GPIOC_8)
		,.IE__GPIOC_9     (IOMUX_0__PADS_0__IE__GPIOC_9)
		,.IE__GPIOC_10    (IOMUX_0__PADS_0__IE__GPIOC_10)
		,.IE__GPIOC_11    (IOMUX_0__PADS_0__IE__GPIOC_11)
		,.IE__GPIOC_12    (IOMUX_0__PADS_0__IE__GPIOC_12)
		,.IE__GPIOC_13    (IOMUX_0__PADS_0__IE__GPIOC_13)
		,.IE__GPIOC_14    (IOMUX_0__PADS_0__IE__GPIOC_14)
		,.IE__GPIOC_15    (IOMUX_0__PADS_0__IE__GPIOC_15)
		,.IE__GPIOD_0     (IOMUX_0__PADS_0__IE__GPIOD_0)
		,.IE__GPIOD_1     (IOMUX_0__PADS_0__IE__GPIOD_1)
		,.IE__GPIOD_2     (IOMUX_0__PADS_0__IE__GPIOD_2)
		,.IE__GPIOD_3     (IOMUX_0__PADS_0__IE__GPIOD_3)
		,.IE__GPIOD_4     (IOMUX_0__PADS_0__IE__GPIOD_4)
		,.IE__GPIOD_5     (IOMUX_0__PADS_0__IE__GPIOD_5)
		,.IE__GPIOD_6     (IOMUX_0__PADS_0__IE__GPIOD_6)
		,.IE__GPIOD_7     (IOMUX_0__PADS_0__IE__GPIOD_7)
		,.IE__GPIOD_8     (IOMUX_0__PADS_0__IE__GPIOD_8)
		,.IE__GPIOD_9     (IOMUX_0__PADS_0__IE__GPIOD_9)
		,.IE__GPIOD_10    (IOMUX_0__PADS_0__IE__GPIOD_10)
		,.IE__GPIOD_11    (IOMUX_0__PADS_0__IE__GPIOD_11)
		,.IE__GPIOD_12    (IOMUX_0__PADS_0__IE__GPIOD_12)
		,.IE__GPIOD_13    (IOMUX_0__PADS_0__IE__GPIOD_13)
		,.IE__GPIOD_14    (IOMUX_0__PADS_0__IE__GPIOD_14)
		,.IE__GPIOD_15    (IOMUX_0__PADS_0__IE__GPIOD_15)
		,.IE__SDMMC_0     (IOMUX_0__PADS_0__IE__SDMMC_0)
		,.IE__SDMMC_1     (IOMUX_0__PADS_0__IE__SDMMC_1)
		,.IE__SDMMC_2     (IOMUX_0__PADS_0__IE__SDMMC_2)
		,.IE__SDMMC_3     (IOMUX_0__PADS_0__IE__SDMMC_3)
		,.IE__SDMMC_4     (IOMUX_0__PADS_0__IE__SDMMC_4)
		,.IE__SDMMC_5     (IOMUX_0__PADS_0__IE__SDMMC_5)
		,.IE__SDMMC_6     (IOMUX_0__PADS_0__IE__SDMMC_6)
		,.IE__SDMMC_7     (IOMUX_0__PADS_0__IE__SDMMC_7)
		,.IE__SDMMC_8     (IOMUX_0__PADS_0__IE__SDMMC_8)
		,.IE__SDMMC_9     (IOMUX_0__PADS_0__IE__SDMMC_9)
		,.IE__SDMMC_10    (IOMUX_0__PADS_0__IE__SDMMC_10)
		,.IE__SDMMC_11    (IOMUX_0__PADS_0__IE__SDMMC_11)
		,.DS0__RSTN       (1'h1)
		,.DS0__RSTN_OUT   (1'h1)
		,.DS0__PKG0       (1'h1)
		,.DS0__PKG1       (1'h1)
		,.DS0__PKG2       (1'h1)
		,.DS0__JTAG_TCK   (1'h1)
		,.DS0__JTAG_TMS   (1'h1)
		,.DS0__JTAG_TDI   (1'h1)
		,.DS0__JTAG_TDO   (1'h1)
		,.DS0__JTAG_TRST  (1'h1)
		,.DS0__GPIOA_0    (IOMUX_0__PADS_0__DS0__GPIOA_0)
		,.DS0__GPIOA_1    (IOMUX_0__PADS_0__DS0__GPIOA_1)
		,.DS0__GPIOA_2    (IOMUX_0__PADS_0__DS0__GPIOA_2)
		,.DS0__GPIOA_3    (IOMUX_0__PADS_0__DS0__GPIOA_3)
		,.DS0__GPIOA_4    (IOMUX_0__PADS_0__DS0__GPIOA_4)
		,.DS0__GPIOA_5    (IOMUX_0__PADS_0__DS0__GPIOA_5)
		,.DS0__GPIOA_6    (IOMUX_0__PADS_0__DS0__GPIOA_6)
		,.DS0__GPIOA_7    (IOMUX_0__PADS_0__DS0__GPIOA_7)
		,.DS0__GPIOA_8    (IOMUX_0__PADS_0__DS0__GPIOA_8)
		,.DS0__GPIOA_9    (IOMUX_0__PADS_0__DS0__GPIOA_9)
		,.DS0__GPIOA_10   (IOMUX_0__PADS_0__DS0__GPIOA_10)
		,.DS0__GPIOA_11   (IOMUX_0__PADS_0__DS0__GPIOA_11)
		,.DS0__GPIOA_12   (IOMUX_0__PADS_0__DS0__GPIOA_12)
		,.DS0__GPIOA_13   (IOMUX_0__PADS_0__DS0__GPIOA_13)
		,.DS0__GPIOA_14   (IOMUX_0__PADS_0__DS0__GPIOA_14)
		,.DS0__GPIOA_15   (IOMUX_0__PADS_0__DS0__GPIOA_15)
		,.DS0__GPIOB_0    (IOMUX_0__PADS_0__DS0__GPIOB_0)
		,.DS0__GPIOB_1    (IOMUX_0__PADS_0__DS0__GPIOB_1)
		,.DS0__GPIOB_2    (IOMUX_0__PADS_0__DS0__GPIOB_2)
		,.DS0__GPIOB_3    (IOMUX_0__PADS_0__DS0__GPIOB_3)
		,.DS0__GPIOB_4    (IOMUX_0__PADS_0__DS0__GPIOB_4)
		,.DS0__GPIOB_5    (IOMUX_0__PADS_0__DS0__GPIOB_5)
		,.DS0__GPIOB_6    (IOMUX_0__PADS_0__DS0__GPIOB_6)
		,.DS0__GPIOB_7    (IOMUX_0__PADS_0__DS0__GPIOB_7)
		,.DS0__GPIOB_8    (IOMUX_0__PADS_0__DS0__GPIOB_8)
		,.DS0__GPIOB_9    (IOMUX_0__PADS_0__DS0__GPIOB_9)
		,.DS0__GPIOB_10   (IOMUX_0__PADS_0__DS0__GPIOB_10)
		,.DS0__GPIOB_11   (IOMUX_0__PADS_0__DS0__GPIOB_11)
		,.DS0__GPIOB_12   (IOMUX_0__PADS_0__DS0__GPIOB_12)
		,.DS0__GPIOB_13   (IOMUX_0__PADS_0__DS0__GPIOB_13)
		,.DS0__GPIOB_14   (IOMUX_0__PADS_0__DS0__GPIOB_14)
		,.DS0__GPIOB_15   (IOMUX_0__PADS_0__DS0__GPIOB_15)
		,.DS0__GPIOC_0    (IOMUX_0__PADS_0__DS0__GPIOC_0)
		,.DS0__GPIOC_1    (IOMUX_0__PADS_0__DS0__GPIOC_1)
		,.DS0__GPIOC_2    (IOMUX_0__PADS_0__DS0__GPIOC_2)
		,.DS0__GPIOC_3    (IOMUX_0__PADS_0__DS0__GPIOC_3)
		,.DS0__GPIOC_4    (IOMUX_0__PADS_0__DS0__GPIOC_4)
		,.DS0__GPIOC_5    (IOMUX_0__PADS_0__DS0__GPIOC_5)
		,.DS0__GPIOC_6    (IOMUX_0__PADS_0__DS0__GPIOC_6)
		,.DS0__GPIOC_7    (IOMUX_0__PADS_0__DS0__GPIOC_7)
		,.DS0__GPIOC_8    (IOMUX_0__PADS_0__DS0__GPIOC_8)
		,.DS0__GPIOC_9    (IOMUX_0__PADS_0__DS0__GPIOC_9)
		,.DS0__GPIOC_10   (IOMUX_0__PADS_0__DS0__GPIOC_10)
		,.DS0__GPIOC_11   (IOMUX_0__PADS_0__DS0__GPIOC_11)
		,.DS0__GPIOC_12   (IOMUX_0__PADS_0__DS0__GPIOC_12)
		,.DS0__GPIOC_13   (IOMUX_0__PADS_0__DS0__GPIOC_13)
		,.DS0__GPIOC_14   (IOMUX_0__PADS_0__DS0__GPIOC_14)
		,.DS0__GPIOC_15   (IOMUX_0__PADS_0__DS0__GPIOC_15)
		,.DS0__GPIOD_0    (IOMUX_0__PADS_0__DS0__GPIOD_0)
		,.DS0__GPIOD_1    (IOMUX_0__PADS_0__DS0__GPIOD_1)
		,.DS0__GPIOD_2    (IOMUX_0__PADS_0__DS0__GPIOD_2)
		,.DS0__GPIOD_3    (IOMUX_0__PADS_0__DS0__GPIOD_3)
		,.DS0__GPIOD_4    (IOMUX_0__PADS_0__DS0__GPIOD_4)
		,.DS0__GPIOD_5    (IOMUX_0__PADS_0__DS0__GPIOD_5)
		,.DS0__GPIOD_6    (IOMUX_0__PADS_0__DS0__GPIOD_6)
		,.DS0__GPIOD_7    (IOMUX_0__PADS_0__DS0__GPIOD_7)
		,.DS0__GPIOD_8    (IOMUX_0__PADS_0__DS0__GPIOD_8)
		,.DS0__GPIOD_9    (IOMUX_0__PADS_0__DS0__GPIOD_9)
		,.DS0__GPIOD_10   (IOMUX_0__PADS_0__DS0__GPIOD_10)
		,.DS0__GPIOD_11   (IOMUX_0__PADS_0__DS0__GPIOD_11)
		,.DS0__GPIOD_12   (IOMUX_0__PADS_0__DS0__GPIOD_12)
		,.DS0__GPIOD_13   (IOMUX_0__PADS_0__DS0__GPIOD_13)
		,.DS0__GPIOD_14   (IOMUX_0__PADS_0__DS0__GPIOD_14)
		,.DS0__GPIOD_15   (IOMUX_0__PADS_0__DS0__GPIOD_15)
		,.DS0__SDMMC_0    (IOMUX_0__PADS_0__DS0__SDMMC_0)
		,.DS0__SDMMC_1    (IOMUX_0__PADS_0__DS0__SDMMC_1)
		,.DS0__SDMMC_2    (IOMUX_0__PADS_0__DS0__SDMMC_2)
		,.DS0__SDMMC_3    (IOMUX_0__PADS_0__DS0__SDMMC_3)
		,.DS0__SDMMC_4    (IOMUX_0__PADS_0__DS0__SDMMC_4)
		,.DS0__SDMMC_5    (IOMUX_0__PADS_0__DS0__SDMMC_5)
		,.DS0__SDMMC_6    (IOMUX_0__PADS_0__DS0__SDMMC_6)
		,.DS0__SDMMC_7    (IOMUX_0__PADS_0__DS0__SDMMC_7)
		,.DS0__SDMMC_8    (IOMUX_0__PADS_0__DS0__SDMMC_8)
		,.DS0__SDMMC_9    (IOMUX_0__PADS_0__DS0__SDMMC_9)
		,.DS0__SDMMC_10   (IOMUX_0__PADS_0__DS0__SDMMC_10)
		,.DS0__SDMMC_11   (IOMUX_0__PADS_0__DS0__SDMMC_11)
		,.DS1__RSTN       (1'h1)
		,.DS1__RSTN_OUT   (1'h1)
		,.DS1__PKG0       (1'h1)
		,.DS1__PKG1       (1'h1)
		,.DS1__PKG2       (1'h1)
		,.DS1__JTAG_TCK   (1'h1)
		,.DS1__JTAG_TMS   (1'h1)
		,.DS1__JTAG_TDI   (1'h1)
		,.DS1__JTAG_TDO   (1'h1)
		,.DS1__JTAG_TRST  (1'h1)
		,.DS1__GPIOA_0    (IOMUX_0__PADS_0__DS1__GPIOA_0)
		,.DS1__GPIOA_1    (IOMUX_0__PADS_0__DS1__GPIOA_1)
		,.DS1__GPIOA_2    (IOMUX_0__PADS_0__DS1__GPIOA_2)
		,.DS1__GPIOA_3    (IOMUX_0__PADS_0__DS1__GPIOA_3)
		,.DS1__GPIOA_4    (IOMUX_0__PADS_0__DS1__GPIOA_4)
		,.DS1__GPIOA_5    (IOMUX_0__PADS_0__DS1__GPIOA_5)
		,.DS1__GPIOA_6    (IOMUX_0__PADS_0__DS1__GPIOA_6)
		,.DS1__GPIOA_7    (IOMUX_0__PADS_0__DS1__GPIOA_7)
		,.DS1__GPIOA_8    (IOMUX_0__PADS_0__DS1__GPIOA_8)
		,.DS1__GPIOA_9    (IOMUX_0__PADS_0__DS1__GPIOA_9)
		,.DS1__GPIOA_10   (IOMUX_0__PADS_0__DS1__GPIOA_10)
		,.DS1__GPIOA_11   (IOMUX_0__PADS_0__DS1__GPIOA_11)
		,.DS1__GPIOA_12   (IOMUX_0__PADS_0__DS1__GPIOA_12)
		,.DS1__GPIOA_13   (IOMUX_0__PADS_0__DS1__GPIOA_13)
		,.DS1__GPIOA_14   (IOMUX_0__PADS_0__DS1__GPIOA_14)
		,.DS1__GPIOA_15   (IOMUX_0__PADS_0__DS1__GPIOA_15)
		,.DS1__GPIOB_0    (IOMUX_0__PADS_0__DS1__GPIOB_0)
		,.DS1__GPIOB_1    (IOMUX_0__PADS_0__DS1__GPIOB_1)
		,.DS1__GPIOB_2    (IOMUX_0__PADS_0__DS1__GPIOB_2)
		,.DS1__GPIOB_3    (IOMUX_0__PADS_0__DS1__GPIOB_3)
		,.DS1__GPIOB_4    (IOMUX_0__PADS_0__DS1__GPIOB_4)
		,.DS1__GPIOB_5    (IOMUX_0__PADS_0__DS1__GPIOB_5)
		,.DS1__GPIOB_6    (IOMUX_0__PADS_0__DS1__GPIOB_6)
		,.DS1__GPIOB_7    (IOMUX_0__PADS_0__DS1__GPIOB_7)
		,.DS1__GPIOB_8    (IOMUX_0__PADS_0__DS1__GPIOB_8)
		,.DS1__GPIOB_9    (IOMUX_0__PADS_0__DS1__GPIOB_9)
		,.DS1__GPIOB_10   (IOMUX_0__PADS_0__DS1__GPIOB_10)
		,.DS1__GPIOB_11   (IOMUX_0__PADS_0__DS1__GPIOB_11)
		,.DS1__GPIOB_12   (IOMUX_0__PADS_0__DS1__GPIOB_12)
		,.DS1__GPIOB_13   (IOMUX_0__PADS_0__DS1__GPIOB_13)
		,.DS1__GPIOB_14   (IOMUX_0__PADS_0__DS1__GPIOB_14)
		,.DS1__GPIOB_15   (IOMUX_0__PADS_0__DS1__GPIOB_15)
		,.DS1__GPIOC_0    (IOMUX_0__PADS_0__DS1__GPIOC_0)
		,.DS1__GPIOC_1    (IOMUX_0__PADS_0__DS1__GPIOC_1)
		,.DS1__GPIOC_2    (IOMUX_0__PADS_0__DS1__GPIOC_2)
		,.DS1__GPIOC_3    (IOMUX_0__PADS_0__DS1__GPIOC_3)
		,.DS1__GPIOC_4    (IOMUX_0__PADS_0__DS1__GPIOC_4)
		,.DS1__GPIOC_5    (IOMUX_0__PADS_0__DS1__GPIOC_5)
		,.DS1__GPIOC_6    (IOMUX_0__PADS_0__DS1__GPIOC_6)
		,.DS1__GPIOC_7    (IOMUX_0__PADS_0__DS1__GPIOC_7)
		,.DS1__GPIOC_8    (IOMUX_0__PADS_0__DS1__GPIOC_8)
		,.DS1__GPIOC_9    (IOMUX_0__PADS_0__DS1__GPIOC_9)
		,.DS1__GPIOC_10   (IOMUX_0__PADS_0__DS1__GPIOC_10)
		,.DS1__GPIOC_11   (IOMUX_0__PADS_0__DS1__GPIOC_11)
		,.DS1__GPIOC_12   (IOMUX_0__PADS_0__DS1__GPIOC_12)
		,.DS1__GPIOC_13   (IOMUX_0__PADS_0__DS1__GPIOC_13)
		,.DS1__GPIOC_14   (IOMUX_0__PADS_0__DS1__GPIOC_14)
		,.DS1__GPIOC_15   (IOMUX_0__PADS_0__DS1__GPIOC_15)
		,.DS1__GPIOD_0    (IOMUX_0__PADS_0__DS1__GPIOD_0)
		,.DS1__GPIOD_1    (IOMUX_0__PADS_0__DS1__GPIOD_1)
		,.DS1__GPIOD_2    (IOMUX_0__PADS_0__DS1__GPIOD_2)
		,.DS1__GPIOD_3    (IOMUX_0__PADS_0__DS1__GPIOD_3)
		,.DS1__GPIOD_4    (IOMUX_0__PADS_0__DS1__GPIOD_4)
		,.DS1__GPIOD_5    (IOMUX_0__PADS_0__DS1__GPIOD_5)
		,.DS1__GPIOD_6    (IOMUX_0__PADS_0__DS1__GPIOD_6)
		,.DS1__GPIOD_7    (IOMUX_0__PADS_0__DS1__GPIOD_7)
		,.DS1__GPIOD_8    (IOMUX_0__PADS_0__DS1__GPIOD_8)
		,.DS1__GPIOD_9    (IOMUX_0__PADS_0__DS1__GPIOD_9)
		,.DS1__GPIOD_10   (IOMUX_0__PADS_0__DS1__GPIOD_10)
		,.DS1__GPIOD_11   (IOMUX_0__PADS_0__DS1__GPIOD_11)
		,.DS1__GPIOD_12   (IOMUX_0__PADS_0__DS1__GPIOD_12)
		,.DS1__GPIOD_13   (IOMUX_0__PADS_0__DS1__GPIOD_13)
		,.DS1__GPIOD_14   (IOMUX_0__PADS_0__DS1__GPIOD_14)
		,.DS1__GPIOD_15   (IOMUX_0__PADS_0__DS1__GPIOD_15)
		,.DS1__SDMMC_0    (IOMUX_0__PADS_0__DS1__SDMMC_0)
		,.DS1__SDMMC_1    (IOMUX_0__PADS_0__DS1__SDMMC_1)
		,.DS1__SDMMC_2    (IOMUX_0__PADS_0__DS1__SDMMC_2)
		,.DS1__SDMMC_3    (IOMUX_0__PADS_0__DS1__SDMMC_3)
		,.DS1__SDMMC_4    (IOMUX_0__PADS_0__DS1__SDMMC_4)
		,.DS1__SDMMC_5    (IOMUX_0__PADS_0__DS1__SDMMC_5)
		,.DS1__SDMMC_6    (IOMUX_0__PADS_0__DS1__SDMMC_6)
		,.DS1__SDMMC_7    (IOMUX_0__PADS_0__DS1__SDMMC_7)
		,.DS1__SDMMC_8    (IOMUX_0__PADS_0__DS1__SDMMC_8)
		,.DS1__SDMMC_9    (IOMUX_0__PADS_0__DS1__SDMMC_9)
		,.DS1__SDMMC_10   (IOMUX_0__PADS_0__DS1__SDMMC_10)
		,.DS1__SDMMC_11   (IOMUX_0__PADS_0__DS1__SDMMC_11)
		,.PE__RSTN        (1'h1)
		,.PE__RSTN_OUT    (1'h1)
		,.PE__PKG0        (1'h1)
		,.PE__PKG1        (1'h1)
		,.PE__PKG2        (1'h1)
		,.PE__JTAG_TCK    (1'h1)
		,.PE__JTAG_TMS    (1'h1)
		,.PE__JTAG_TDI    (1'h1)
		,.PE__JTAG_TDO    (1'h1)
		,.PE__JTAG_TRST   (1'h1)
		,.PE__GPIOA_0     (IOMUX_0__PADS_0__PE__GPIOA_0)
		,.PE__GPIOA_1     (IOMUX_0__PADS_0__PE__GPIOA_1)
		,.PE__GPIOA_2     (IOMUX_0__PADS_0__PE__GPIOA_2)
		,.PE__GPIOA_3     (IOMUX_0__PADS_0__PE__GPIOA_3)
		,.PE__GPIOA_4     (IOMUX_0__PADS_0__PE__GPIOA_4)
		,.PE__GPIOA_5     (IOMUX_0__PADS_0__PE__GPIOA_5)
		,.PE__GPIOA_6     (IOMUX_0__PADS_0__PE__GPIOA_6)
		,.PE__GPIOA_7     (IOMUX_0__PADS_0__PE__GPIOA_7)
		,.PE__GPIOA_8     (IOMUX_0__PADS_0__PE__GPIOA_8)
		,.PE__GPIOA_9     (IOMUX_0__PADS_0__PE__GPIOA_9)
		,.PE__GPIOA_10    (IOMUX_0__PADS_0__PE__GPIOA_10)
		,.PE__GPIOA_11    (IOMUX_0__PADS_0__PE__GPIOA_11)
		,.PE__GPIOA_12    (IOMUX_0__PADS_0__PE__GPIOA_12)
		,.PE__GPIOA_13    (IOMUX_0__PADS_0__PE__GPIOA_13)
		,.PE__GPIOA_14    (IOMUX_0__PADS_0__PE__GPIOA_14)
		,.PE__GPIOA_15    (IOMUX_0__PADS_0__PE__GPIOA_15)
		,.PE__GPIOB_0     (IOMUX_0__PADS_0__PE__GPIOB_0)
		,.PE__GPIOB_1     (IOMUX_0__PADS_0__PE__GPIOB_1)
		,.PE__GPIOB_2     (IOMUX_0__PADS_0__PE__GPIOB_2)
		,.PE__GPIOB_3     (IOMUX_0__PADS_0__PE__GPIOB_3)
		,.PE__GPIOB_4     (IOMUX_0__PADS_0__PE__GPIOB_4)
		,.PE__GPIOB_5     (IOMUX_0__PADS_0__PE__GPIOB_5)
		,.PE__GPIOB_6     (IOMUX_0__PADS_0__PE__GPIOB_6)
		,.PE__GPIOB_7     (IOMUX_0__PADS_0__PE__GPIOB_7)
		,.PE__GPIOB_8     (IOMUX_0__PADS_0__PE__GPIOB_8)
		,.PE__GPIOB_9     (IOMUX_0__PADS_0__PE__GPIOB_9)
		,.PE__GPIOB_10    (IOMUX_0__PADS_0__PE__GPIOB_10)
		,.PE__GPIOB_11    (IOMUX_0__PADS_0__PE__GPIOB_11)
		,.PE__GPIOB_12    (IOMUX_0__PADS_0__PE__GPIOB_12)
		,.PE__GPIOB_13    (IOMUX_0__PADS_0__PE__GPIOB_13)
		,.PE__GPIOB_14    (IOMUX_0__PADS_0__PE__GPIOB_14)
		,.PE__GPIOB_15    (IOMUX_0__PADS_0__PE__GPIOB_15)
		,.PE__GPIOC_0     (IOMUX_0__PADS_0__PE__GPIOC_0)
		,.PE__GPIOC_1     (IOMUX_0__PADS_0__PE__GPIOC_1)
		,.PE__GPIOC_2     (IOMUX_0__PADS_0__PE__GPIOC_2)
		,.PE__GPIOC_3     (IOMUX_0__PADS_0__PE__GPIOC_3)
		,.PE__GPIOC_4     (IOMUX_0__PADS_0__PE__GPIOC_4)
		,.PE__GPIOC_5     (IOMUX_0__PADS_0__PE__GPIOC_5)
		,.PE__GPIOC_6     (IOMUX_0__PADS_0__PE__GPIOC_6)
		,.PE__GPIOC_7     (IOMUX_0__PADS_0__PE__GPIOC_7)
		,.PE__GPIOC_8     (IOMUX_0__PADS_0__PE__GPIOC_8)
		,.PE__GPIOC_9     (IOMUX_0__PADS_0__PE__GPIOC_9)
		,.PE__GPIOC_10    (IOMUX_0__PADS_0__PE__GPIOC_10)
		,.PE__GPIOC_11    (IOMUX_0__PADS_0__PE__GPIOC_11)
		,.PE__GPIOC_12    (IOMUX_0__PADS_0__PE__GPIOC_12)
		,.PE__GPIOC_13    (IOMUX_0__PADS_0__PE__GPIOC_13)
		,.PE__GPIOC_14    (IOMUX_0__PADS_0__PE__GPIOC_14)
		,.PE__GPIOC_15    (IOMUX_0__PADS_0__PE__GPIOC_15)
		,.PE__GPIOD_0     (IOMUX_0__PADS_0__PE__GPIOD_0)
		,.PE__GPIOD_1     (IOMUX_0__PADS_0__PE__GPIOD_1)
		,.PE__GPIOD_2     (IOMUX_0__PADS_0__PE__GPIOD_2)
		,.PE__GPIOD_3     (IOMUX_0__PADS_0__PE__GPIOD_3)
		,.PE__GPIOD_4     (IOMUX_0__PADS_0__PE__GPIOD_4)
		,.PE__GPIOD_5     (IOMUX_0__PADS_0__PE__GPIOD_5)
		,.PE__GPIOD_6     (IOMUX_0__PADS_0__PE__GPIOD_6)
		,.PE__GPIOD_7     (IOMUX_0__PADS_0__PE__GPIOD_7)
		,.PE__GPIOD_8     (IOMUX_0__PADS_0__PE__GPIOD_8)
		,.PE__GPIOD_9     (IOMUX_0__PADS_0__PE__GPIOD_9)
		,.PE__GPIOD_10    (IOMUX_0__PADS_0__PE__GPIOD_10)
		,.PE__GPIOD_11    (IOMUX_0__PADS_0__PE__GPIOD_11)
		,.PE__GPIOD_12    (IOMUX_0__PADS_0__PE__GPIOD_12)
		,.PE__GPIOD_13    (IOMUX_0__PADS_0__PE__GPIOD_13)
		,.PE__GPIOD_14    (IOMUX_0__PADS_0__PE__GPIOD_14)
		,.PE__GPIOD_15    (IOMUX_0__PADS_0__PE__GPIOD_15)
		,.PE__SDMMC_0     (IOMUX_0__PADS_0__PE__SDMMC_0)
		,.PE__SDMMC_1     (IOMUX_0__PADS_0__PE__SDMMC_1)
		,.PE__SDMMC_2     (IOMUX_0__PADS_0__PE__SDMMC_2)
		,.PE__SDMMC_3     (IOMUX_0__PADS_0__PE__SDMMC_3)
		,.PE__SDMMC_4     (IOMUX_0__PADS_0__PE__SDMMC_4)
		,.PE__SDMMC_5     (IOMUX_0__PADS_0__PE__SDMMC_5)
		,.PE__SDMMC_6     (IOMUX_0__PADS_0__PE__SDMMC_6)
		,.PE__SDMMC_7     (IOMUX_0__PADS_0__PE__SDMMC_7)
		,.PE__SDMMC_8     (IOMUX_0__PADS_0__PE__SDMMC_8)
		,.PE__SDMMC_9     (IOMUX_0__PADS_0__PE__SDMMC_9)
		,.PE__SDMMC_10    (IOMUX_0__PADS_0__PE__SDMMC_10)
		,.PE__SDMMC_11    (IOMUX_0__PADS_0__PE__SDMMC_11)
		,.PS__RSTN        (1'h1)
		,.PS__RSTN_OUT    (1'h1)
		,.PS__PKG0        (1'h1)
		,.PS__PKG1        (1'h1)
		,.PS__PKG2        (1'h1)
		,.PS__JTAG_TCK    (1'h1)
		,.PS__JTAG_TMS    (1'h1)
		,.PS__JTAG_TDI    (1'h1)
		,.PS__JTAG_TDO    (1'h1)
		,.PS__JTAG_TRST   (1'h1)
		,.PS__GPIOA_0     (IOMUX_0__PADS_0__PS__GPIOA_0)
		,.PS__GPIOA_1     (IOMUX_0__PADS_0__PS__GPIOA_1)
		,.PS__GPIOA_2     (IOMUX_0__PADS_0__PS__GPIOA_2)
		,.PS__GPIOA_3     (IOMUX_0__PADS_0__PS__GPIOA_3)
		,.PS__GPIOA_4     (IOMUX_0__PADS_0__PS__GPIOA_4)
		,.PS__GPIOA_5     (IOMUX_0__PADS_0__PS__GPIOA_5)
		,.PS__GPIOA_6     (IOMUX_0__PADS_0__PS__GPIOA_6)
		,.PS__GPIOA_7     (IOMUX_0__PADS_0__PS__GPIOA_7)
		,.PS__GPIOA_8     (IOMUX_0__PADS_0__PS__GPIOA_8)
		,.PS__GPIOA_9     (IOMUX_0__PADS_0__PS__GPIOA_9)
		,.PS__GPIOA_10    (IOMUX_0__PADS_0__PS__GPIOA_10)
		,.PS__GPIOA_11    (IOMUX_0__PADS_0__PS__GPIOA_11)
		,.PS__GPIOA_12    (IOMUX_0__PADS_0__PS__GPIOA_12)
		,.PS__GPIOA_13    (IOMUX_0__PADS_0__PS__GPIOA_13)
		,.PS__GPIOA_14    (IOMUX_0__PADS_0__PS__GPIOA_14)
		,.PS__GPIOA_15    (IOMUX_0__PADS_0__PS__GPIOA_15)
		,.PS__GPIOB_0     (IOMUX_0__PADS_0__PS__GPIOB_0)
		,.PS__GPIOB_1     (IOMUX_0__PADS_0__PS__GPIOB_1)
		,.PS__GPIOB_2     (IOMUX_0__PADS_0__PS__GPIOB_2)
		,.PS__GPIOB_3     (IOMUX_0__PADS_0__PS__GPIOB_3)
		,.PS__GPIOB_4     (IOMUX_0__PADS_0__PS__GPIOB_4)
		,.PS__GPIOB_5     (IOMUX_0__PADS_0__PS__GPIOB_5)
		,.PS__GPIOB_6     (IOMUX_0__PADS_0__PS__GPIOB_6)
		,.PS__GPIOB_7     (IOMUX_0__PADS_0__PS__GPIOB_7)
		,.PS__GPIOB_8     (IOMUX_0__PADS_0__PS__GPIOB_8)
		,.PS__GPIOB_9     (IOMUX_0__PADS_0__PS__GPIOB_9)
		,.PS__GPIOB_10    (IOMUX_0__PADS_0__PS__GPIOB_10)
		,.PS__GPIOB_11    (IOMUX_0__PADS_0__PS__GPIOB_11)
		,.PS__GPIOB_12    (IOMUX_0__PADS_0__PS__GPIOB_12)
		,.PS__GPIOB_13    (IOMUX_0__PADS_0__PS__GPIOB_13)
		,.PS__GPIOB_14    (IOMUX_0__PADS_0__PS__GPIOB_14)
		,.PS__GPIOB_15    (IOMUX_0__PADS_0__PS__GPIOB_15)
		,.PS__GPIOC_0     (IOMUX_0__PADS_0__PS__GPIOC_0)
		,.PS__GPIOC_1     (IOMUX_0__PADS_0__PS__GPIOC_1)
		,.PS__GPIOC_2     (IOMUX_0__PADS_0__PS__GPIOC_2)
		,.PS__GPIOC_3     (IOMUX_0__PADS_0__PS__GPIOC_3)
		,.PS__GPIOC_4     (IOMUX_0__PADS_0__PS__GPIOC_4)
		,.PS__GPIOC_5     (IOMUX_0__PADS_0__PS__GPIOC_5)
		,.PS__GPIOC_6     (IOMUX_0__PADS_0__PS__GPIOC_6)
		,.PS__GPIOC_7     (IOMUX_0__PADS_0__PS__GPIOC_7)
		,.PS__GPIOC_8     (IOMUX_0__PADS_0__PS__GPIOC_8)
		,.PS__GPIOC_9     (IOMUX_0__PADS_0__PS__GPIOC_9)
		,.PS__GPIOC_10    (IOMUX_0__PADS_0__PS__GPIOC_10)
		,.PS__GPIOC_11    (IOMUX_0__PADS_0__PS__GPIOC_11)
		,.PS__GPIOC_12    (IOMUX_0__PADS_0__PS__GPIOC_12)
		,.PS__GPIOC_13    (IOMUX_0__PADS_0__PS__GPIOC_13)
		,.PS__GPIOC_14    (IOMUX_0__PADS_0__PS__GPIOC_14)
		,.PS__GPIOC_15    (IOMUX_0__PADS_0__PS__GPIOC_15)
		,.PS__GPIOD_0     (IOMUX_0__PADS_0__PS__GPIOD_0)
		,.PS__GPIOD_1     (IOMUX_0__PADS_0__PS__GPIOD_1)
		,.PS__GPIOD_2     (IOMUX_0__PADS_0__PS__GPIOD_2)
		,.PS__GPIOD_3     (IOMUX_0__PADS_0__PS__GPIOD_3)
		,.PS__GPIOD_4     (IOMUX_0__PADS_0__PS__GPIOD_4)
		,.PS__GPIOD_5     (IOMUX_0__PADS_0__PS__GPIOD_5)
		,.PS__GPIOD_6     (IOMUX_0__PADS_0__PS__GPIOD_6)
		,.PS__GPIOD_7     (IOMUX_0__PADS_0__PS__GPIOD_7)
		,.PS__GPIOD_8     (IOMUX_0__PADS_0__PS__GPIOD_8)
		,.PS__GPIOD_9     (IOMUX_0__PADS_0__PS__GPIOD_9)
		,.PS__GPIOD_10    (IOMUX_0__PADS_0__PS__GPIOD_10)
		,.PS__GPIOD_11    (IOMUX_0__PADS_0__PS__GPIOD_11)
		,.PS__GPIOD_12    (IOMUX_0__PADS_0__PS__GPIOD_12)
		,.PS__GPIOD_13    (IOMUX_0__PADS_0__PS__GPIOD_13)
		,.PS__GPIOD_14    (IOMUX_0__PADS_0__PS__GPIOD_14)
		,.PS__GPIOD_15    (IOMUX_0__PADS_0__PS__GPIOD_15)
		,.PS__SDMMC_0     (IOMUX_0__PADS_0__PS__SDMMC_0)
		,.PS__SDMMC_1     (IOMUX_0__PADS_0__PS__SDMMC_1)
		,.PS__SDMMC_2     (IOMUX_0__PADS_0__PS__SDMMC_2)
		,.PS__SDMMC_3     (IOMUX_0__PADS_0__PS__SDMMC_3)
		,.PS__SDMMC_4     (IOMUX_0__PADS_0__PS__SDMMC_4)
		,.PS__SDMMC_5     (IOMUX_0__PADS_0__PS__SDMMC_5)
		,.PS__SDMMC_6     (IOMUX_0__PADS_0__PS__SDMMC_6)
		,.PS__SDMMC_7     (IOMUX_0__PADS_0__PS__SDMMC_7)
		,.PS__SDMMC_8     (IOMUX_0__PADS_0__PS__SDMMC_8)
		,.PS__SDMMC_9     (IOMUX_0__PADS_0__PS__SDMMC_9)
		,.PS__SDMMC_10    (IOMUX_0__PADS_0__PS__SDMMC_10)
		,.PS__SDMMC_11    (IOMUX_0__PADS_0__PS__SDMMC_11)
	);

	iomux_temp
	IOMUX_0 (
		 .PAD_GPIOA_Y   ({
			IOMUX_0__PADS_0__Y__GPIOA_15
			,IOMUX_0__PADS_0__Y__GPIOA_14
			,IOMUX_0__PADS_0__Y__GPIOA_13
			,IOMUX_0__PADS_0__Y__GPIOA_12
			,IOMUX_0__PADS_0__Y__GPIOA_11
			,IOMUX_0__PADS_0__Y__GPIOA_10
			,IOMUX_0__PADS_0__Y__GPIOA_9
			,IOMUX_0__PADS_0__Y__GPIOA_8
			,IOMUX_0__PADS_0__Y__GPIOA_7
			,IOMUX_0__PADS_0__Y__GPIOA_6
			,IOMUX_0__PADS_0__Y__GPIOA_5
			,IOMUX_0__PADS_0__Y__GPIOA_4
			,IOMUX_0__PADS_0__Y__GPIOA_3
			,IOMUX_0__PADS_0__Y__GPIOA_2
			,IOMUX_0__PADS_0__Y__GPIOA_1
			,IOMUX_0__PADS_0__Y__GPIOA_0})
		,.PAD_GPIOA_A   ({
			IOMUX_0__PADS_0__A__GPIOA_15
			,IOMUX_0__PADS_0__A__GPIOA_14
			,IOMUX_0__PADS_0__A__GPIOA_13
			,IOMUX_0__PADS_0__A__GPIOA_12
			,IOMUX_0__PADS_0__A__GPIOA_11
			,IOMUX_0__PADS_0__A__GPIOA_10
			,IOMUX_0__PADS_0__A__GPIOA_9
			,IOMUX_0__PADS_0__A__GPIOA_8
			,IOMUX_0__PADS_0__A__GPIOA_7
			,IOMUX_0__PADS_0__A__GPIOA_6
			,IOMUX_0__PADS_0__A__GPIOA_5
			,IOMUX_0__PADS_0__A__GPIOA_4
			,IOMUX_0__PADS_0__A__GPIOA_3
			,IOMUX_0__PADS_0__A__GPIOA_2
			,IOMUX_0__PADS_0__A__GPIOA_1
			,IOMUX_0__PADS_0__A__GPIOA_0})
		,.PAD_GPIOA_OE  ({
			IOMUX_0__PADS_0__OE__GPIOA_15
			,IOMUX_0__PADS_0__OE__GPIOA_14
			,IOMUX_0__PADS_0__OE__GPIOA_13
			,IOMUX_0__PADS_0__OE__GPIOA_12
			,IOMUX_0__PADS_0__OE__GPIOA_11
			,IOMUX_0__PADS_0__OE__GPIOA_10
			,IOMUX_0__PADS_0__OE__GPIOA_9
			,IOMUX_0__PADS_0__OE__GPIOA_8
			,IOMUX_0__PADS_0__OE__GPIOA_7
			,IOMUX_0__PADS_0__OE__GPIOA_6
			,IOMUX_0__PADS_0__OE__GPIOA_5
			,IOMUX_0__PADS_0__OE__GPIOA_4
			,IOMUX_0__PADS_0__OE__GPIOA_3
			,IOMUX_0__PADS_0__OE__GPIOA_2
			,IOMUX_0__PADS_0__OE__GPIOA_1
			,IOMUX_0__PADS_0__OE__GPIOA_0})
		,.PAD_GPIOA_DS0 ({
			IOMUX_0__PADS_0__DS0__GPIOA_15
			,IOMUX_0__PADS_0__DS0__GPIOA_14
			,IOMUX_0__PADS_0__DS0__GPIOA_13
			,IOMUX_0__PADS_0__DS0__GPIOA_12
			,IOMUX_0__PADS_0__DS0__GPIOA_11
			,IOMUX_0__PADS_0__DS0__GPIOA_10
			,IOMUX_0__PADS_0__DS0__GPIOA_9
			,IOMUX_0__PADS_0__DS0__GPIOA_8
			,IOMUX_0__PADS_0__DS0__GPIOA_7
			,IOMUX_0__PADS_0__DS0__GPIOA_6
			,IOMUX_0__PADS_0__DS0__GPIOA_5
			,IOMUX_0__PADS_0__DS0__GPIOA_4
			,IOMUX_0__PADS_0__DS0__GPIOA_3
			,IOMUX_0__PADS_0__DS0__GPIOA_2
			,IOMUX_0__PADS_0__DS0__GPIOA_1
			,IOMUX_0__PADS_0__DS0__GPIOA_0})
		,.PAD_GPIOA_DS1 ({
			IOMUX_0__PADS_0__DS1__GPIOA_15
			,IOMUX_0__PADS_0__DS1__GPIOA_14
			,IOMUX_0__PADS_0__DS1__GPIOA_13
			,IOMUX_0__PADS_0__DS1__GPIOA_12
			,IOMUX_0__PADS_0__DS1__GPIOA_11
			,IOMUX_0__PADS_0__DS1__GPIOA_10
			,IOMUX_0__PADS_0__DS1__GPIOA_9
			,IOMUX_0__PADS_0__DS1__GPIOA_8
			,IOMUX_0__PADS_0__DS1__GPIOA_7
			,IOMUX_0__PADS_0__DS1__GPIOA_6
			,IOMUX_0__PADS_0__DS1__GPIOA_5
			,IOMUX_0__PADS_0__DS1__GPIOA_4
			,IOMUX_0__PADS_0__DS1__GPIOA_3
			,IOMUX_0__PADS_0__DS1__GPIOA_2
			,IOMUX_0__PADS_0__DS1__GPIOA_1
			,IOMUX_0__PADS_0__DS1__GPIOA_0})
		,.PAD_GPIOA_IE  ({
			IOMUX_0__PADS_0__IE__GPIOA_15
			,IOMUX_0__PADS_0__IE__GPIOA_14
			,IOMUX_0__PADS_0__IE__GPIOA_13
			,IOMUX_0__PADS_0__IE__GPIOA_12
			,IOMUX_0__PADS_0__IE__GPIOA_11
			,IOMUX_0__PADS_0__IE__GPIOA_10
			,IOMUX_0__PADS_0__IE__GPIOA_9
			,IOMUX_0__PADS_0__IE__GPIOA_8
			,IOMUX_0__PADS_0__IE__GPIOA_7
			,IOMUX_0__PADS_0__IE__GPIOA_6
			,IOMUX_0__PADS_0__IE__GPIOA_5
			,IOMUX_0__PADS_0__IE__GPIOA_4
			,IOMUX_0__PADS_0__IE__GPIOA_3
			,IOMUX_0__PADS_0__IE__GPIOA_2
			,IOMUX_0__PADS_0__IE__GPIOA_1
			,IOMUX_0__PADS_0__IE__GPIOA_0})
		,.PAD_GPIOA_PE  ({
			IOMUX_0__PADS_0__PE__GPIOA_15
			,IOMUX_0__PADS_0__PE__GPIOA_14
			,IOMUX_0__PADS_0__PE__GPIOA_13
			,IOMUX_0__PADS_0__PE__GPIOA_12
			,IOMUX_0__PADS_0__PE__GPIOA_11
			,IOMUX_0__PADS_0__PE__GPIOA_10
			,IOMUX_0__PADS_0__PE__GPIOA_9
			,IOMUX_0__PADS_0__PE__GPIOA_8
			,IOMUX_0__PADS_0__PE__GPIOA_7
			,IOMUX_0__PADS_0__PE__GPIOA_6
			,IOMUX_0__PADS_0__PE__GPIOA_5
			,IOMUX_0__PADS_0__PE__GPIOA_4
			,IOMUX_0__PADS_0__PE__GPIOA_3
			,IOMUX_0__PADS_0__PE__GPIOA_2
			,IOMUX_0__PADS_0__PE__GPIOA_1
			,IOMUX_0__PADS_0__PE__GPIOA_0})
		,.PAD_GPIOA_PS  ({
			IOMUX_0__PADS_0__PS__GPIOA_15
			,IOMUX_0__PADS_0__PS__GPIOA_14
			,IOMUX_0__PADS_0__PS__GPIOA_13
			,IOMUX_0__PADS_0__PS__GPIOA_12
			,IOMUX_0__PADS_0__PS__GPIOA_11
			,IOMUX_0__PADS_0__PS__GPIOA_10
			,IOMUX_0__PADS_0__PS__GPIOA_9
			,IOMUX_0__PADS_0__PS__GPIOA_8
			,IOMUX_0__PADS_0__PS__GPIOA_7
			,IOMUX_0__PADS_0__PS__GPIOA_6
			,IOMUX_0__PADS_0__PS__GPIOA_5
			,IOMUX_0__PADS_0__PS__GPIOA_4
			,IOMUX_0__PADS_0__PS__GPIOA_3
			,IOMUX_0__PADS_0__PS__GPIOA_2
			,IOMUX_0__PADS_0__PS__GPIOA_1
			,IOMUX_0__PADS_0__PS__GPIOA_0})
		,.PAD_GPIOB_Y   ({
			IOMUX_0__PADS_0__Y__GPIOB_15
			,IOMUX_0__PADS_0__Y__GPIOB_14
			,IOMUX_0__PADS_0__Y__GPIOB_13
			,IOMUX_0__PADS_0__Y__GPIOB_12
			,IOMUX_0__PADS_0__Y__GPIOB_11
			,IOMUX_0__PADS_0__Y__GPIOB_10
			,IOMUX_0__PADS_0__Y__GPIOB_9
			,IOMUX_0__PADS_0__Y__GPIOB_8
			,IOMUX_0__PADS_0__Y__GPIOB_7
			,IOMUX_0__PADS_0__Y__GPIOB_6
			,IOMUX_0__PADS_0__Y__GPIOB_5
			,IOMUX_0__PADS_0__Y__GPIOB_4
			,IOMUX_0__PADS_0__Y__GPIOB_3
			,IOMUX_0__PADS_0__Y__GPIOB_2
			,IOMUX_0__PADS_0__Y__GPIOB_1
			,IOMUX_0__PADS_0__Y__GPIOB_0})
		,.PAD_GPIOB_A   ({
			IOMUX_0__PADS_0__A__GPIOB_15
			,IOMUX_0__PADS_0__A__GPIOB_14
			,IOMUX_0__PADS_0__A__GPIOB_13
			,IOMUX_0__PADS_0__A__GPIOB_12
			,IOMUX_0__PADS_0__A__GPIOB_11
			,IOMUX_0__PADS_0__A__GPIOB_10
			,IOMUX_0__PADS_0__A__GPIOB_9
			,IOMUX_0__PADS_0__A__GPIOB_8
			,IOMUX_0__PADS_0__A__GPIOB_7
			,IOMUX_0__PADS_0__A__GPIOB_6
			,IOMUX_0__PADS_0__A__GPIOB_5
			,IOMUX_0__PADS_0__A__GPIOB_4
			,IOMUX_0__PADS_0__A__GPIOB_3
			,IOMUX_0__PADS_0__A__GPIOB_2
			,IOMUX_0__PADS_0__A__GPIOB_1
			,IOMUX_0__PADS_0__A__GPIOB_0})
		,.PAD_GPIOB_OE  ({
			IOMUX_0__PADS_0__OE__GPIOB_15
			,IOMUX_0__PADS_0__OE__GPIOB_14
			,IOMUX_0__PADS_0__OE__GPIOB_13
			,IOMUX_0__PADS_0__OE__GPIOB_12
			,IOMUX_0__PADS_0__OE__GPIOB_11
			,IOMUX_0__PADS_0__OE__GPIOB_10
			,IOMUX_0__PADS_0__OE__GPIOB_9
			,IOMUX_0__PADS_0__OE__GPIOB_8
			,IOMUX_0__PADS_0__OE__GPIOB_7
			,IOMUX_0__PADS_0__OE__GPIOB_6
			,IOMUX_0__PADS_0__OE__GPIOB_5
			,IOMUX_0__PADS_0__OE__GPIOB_4
			,IOMUX_0__PADS_0__OE__GPIOB_3
			,IOMUX_0__PADS_0__OE__GPIOB_2
			,IOMUX_0__PADS_0__OE__GPIOB_1
			,IOMUX_0__PADS_0__OE__GPIOB_0})
		,.PAD_GPIOB_DS0 ({
			IOMUX_0__PADS_0__DS0__GPIOB_15
			,IOMUX_0__PADS_0__DS0__GPIOB_14
			,IOMUX_0__PADS_0__DS0__GPIOB_13
			,IOMUX_0__PADS_0__DS0__GPIOB_12
			,IOMUX_0__PADS_0__DS0__GPIOB_11
			,IOMUX_0__PADS_0__DS0__GPIOB_10
			,IOMUX_0__PADS_0__DS0__GPIOB_9
			,IOMUX_0__PADS_0__DS0__GPIOB_8
			,IOMUX_0__PADS_0__DS0__GPIOB_7
			,IOMUX_0__PADS_0__DS0__GPIOB_6
			,IOMUX_0__PADS_0__DS0__GPIOB_5
			,IOMUX_0__PADS_0__DS0__GPIOB_4
			,IOMUX_0__PADS_0__DS0__GPIOB_3
			,IOMUX_0__PADS_0__DS0__GPIOB_2
			,IOMUX_0__PADS_0__DS0__GPIOB_1
			,IOMUX_0__PADS_0__DS0__GPIOB_0})
		,.PAD_GPIOB_DS1 ({
			IOMUX_0__PADS_0__DS1__GPIOB_15
			,IOMUX_0__PADS_0__DS1__GPIOB_14
			,IOMUX_0__PADS_0__DS1__GPIOB_13
			,IOMUX_0__PADS_0__DS1__GPIOB_12
			,IOMUX_0__PADS_0__DS1__GPIOB_11
			,IOMUX_0__PADS_0__DS1__GPIOB_10
			,IOMUX_0__PADS_0__DS1__GPIOB_9
			,IOMUX_0__PADS_0__DS1__GPIOB_8
			,IOMUX_0__PADS_0__DS1__GPIOB_7
			,IOMUX_0__PADS_0__DS1__GPIOB_6
			,IOMUX_0__PADS_0__DS1__GPIOB_5
			,IOMUX_0__PADS_0__DS1__GPIOB_4
			,IOMUX_0__PADS_0__DS1__GPIOB_3
			,IOMUX_0__PADS_0__DS1__GPIOB_2
			,IOMUX_0__PADS_0__DS1__GPIOB_1
			,IOMUX_0__PADS_0__DS1__GPIOB_0})
		,.PAD_GPIOB_IE  ({
			IOMUX_0__PADS_0__IE__GPIOB_15
			,IOMUX_0__PADS_0__IE__GPIOB_14
			,IOMUX_0__PADS_0__IE__GPIOB_13
			,IOMUX_0__PADS_0__IE__GPIOB_12
			,IOMUX_0__PADS_0__IE__GPIOB_11
			,IOMUX_0__PADS_0__IE__GPIOB_10
			,IOMUX_0__PADS_0__IE__GPIOB_9
			,IOMUX_0__PADS_0__IE__GPIOB_8
			,IOMUX_0__PADS_0__IE__GPIOB_7
			,IOMUX_0__PADS_0__IE__GPIOB_6
			,IOMUX_0__PADS_0__IE__GPIOB_5
			,IOMUX_0__PADS_0__IE__GPIOB_4
			,IOMUX_0__PADS_0__IE__GPIOB_3
			,IOMUX_0__PADS_0__IE__GPIOB_2
			,IOMUX_0__PADS_0__IE__GPIOB_1
			,IOMUX_0__PADS_0__IE__GPIOB_0})
		,.PAD_GPIOB_PE  ({
			IOMUX_0__PADS_0__PE__GPIOB_15
			,IOMUX_0__PADS_0__PE__GPIOB_14
			,IOMUX_0__PADS_0__PE__GPIOB_13
			,IOMUX_0__PADS_0__PE__GPIOB_12
			,IOMUX_0__PADS_0__PE__GPIOB_11
			,IOMUX_0__PADS_0__PE__GPIOB_10
			,IOMUX_0__PADS_0__PE__GPIOB_9
			,IOMUX_0__PADS_0__PE__GPIOB_8
			,IOMUX_0__PADS_0__PE__GPIOB_7
			,IOMUX_0__PADS_0__PE__GPIOB_6
			,IOMUX_0__PADS_0__PE__GPIOB_5
			,IOMUX_0__PADS_0__PE__GPIOB_4
			,IOMUX_0__PADS_0__PE__GPIOB_3
			,IOMUX_0__PADS_0__PE__GPIOB_2
			,IOMUX_0__PADS_0__PE__GPIOB_1
			,IOMUX_0__PADS_0__PE__GPIOB_0})
		,.PAD_GPIOB_PS  ({
			IOMUX_0__PADS_0__PS__GPIOB_15
			,IOMUX_0__PADS_0__PS__GPIOB_14
			,IOMUX_0__PADS_0__PS__GPIOB_13
			,IOMUX_0__PADS_0__PS__GPIOB_12
			,IOMUX_0__PADS_0__PS__GPIOB_11
			,IOMUX_0__PADS_0__PS__GPIOB_10
			,IOMUX_0__PADS_0__PS__GPIOB_9
			,IOMUX_0__PADS_0__PS__GPIOB_8
			,IOMUX_0__PADS_0__PS__GPIOB_7
			,IOMUX_0__PADS_0__PS__GPIOB_6
			,IOMUX_0__PADS_0__PS__GPIOB_5
			,IOMUX_0__PADS_0__PS__GPIOB_4
			,IOMUX_0__PADS_0__PS__GPIOB_3
			,IOMUX_0__PADS_0__PS__GPIOB_2
			,IOMUX_0__PADS_0__PS__GPIOB_1
			,IOMUX_0__PADS_0__PS__GPIOB_0})
		,.PAD_GPIOC_Y   ({
			IOMUX_0__PADS_0__Y__GPIOC_15
			,IOMUX_0__PADS_0__Y__GPIOC_14
			,IOMUX_0__PADS_0__Y__GPIOC_13
			,IOMUX_0__PADS_0__Y__GPIOC_12
			,IOMUX_0__PADS_0__Y__GPIOC_11
			,IOMUX_0__PADS_0__Y__GPIOC_10
			,IOMUX_0__PADS_0__Y__GPIOC_9
			,IOMUX_0__PADS_0__Y__GPIOC_8
			,IOMUX_0__PADS_0__Y__GPIOC_7
			,IOMUX_0__PADS_0__Y__GPIOC_6
			,IOMUX_0__PADS_0__Y__GPIOC_5
			,IOMUX_0__PADS_0__Y__GPIOC_4
			,IOMUX_0__PADS_0__Y__GPIOC_3
			,IOMUX_0__PADS_0__Y__GPIOC_2
			,IOMUX_0__PADS_0__Y__GPIOC_1
			,IOMUX_0__PADS_0__Y__GPIOC_0})
		,.PAD_GPIOC_A   ({
			IOMUX_0__PADS_0__A__GPIOC_15
			,IOMUX_0__PADS_0__A__GPIOC_14
			,IOMUX_0__PADS_0__A__GPIOC_13
			,IOMUX_0__PADS_0__A__GPIOC_12
			,IOMUX_0__PADS_0__A__GPIOC_11
			,IOMUX_0__PADS_0__A__GPIOC_10
			,IOMUX_0__PADS_0__A__GPIOC_9
			,IOMUX_0__PADS_0__A__GPIOC_8
			,IOMUX_0__PADS_0__A__GPIOC_7
			,IOMUX_0__PADS_0__A__GPIOC_6
			,IOMUX_0__PADS_0__A__GPIOC_5
			,IOMUX_0__PADS_0__A__GPIOC_4
			,IOMUX_0__PADS_0__A__GPIOC_3
			,IOMUX_0__PADS_0__A__GPIOC_2
			,IOMUX_0__PADS_0__A__GPIOC_1
			,IOMUX_0__PADS_0__A__GPIOC_0})
		,.PAD_GPIOC_OE  ({
			IOMUX_0__PADS_0__OE__GPIOC_15
			,IOMUX_0__PADS_0__OE__GPIOC_14
			,IOMUX_0__PADS_0__OE__GPIOC_13
			,IOMUX_0__PADS_0__OE__GPIOC_12
			,IOMUX_0__PADS_0__OE__GPIOC_11
			,IOMUX_0__PADS_0__OE__GPIOC_10
			,IOMUX_0__PADS_0__OE__GPIOC_9
			,IOMUX_0__PADS_0__OE__GPIOC_8
			,IOMUX_0__PADS_0__OE__GPIOC_7
			,IOMUX_0__PADS_0__OE__GPIOC_6
			,IOMUX_0__PADS_0__OE__GPIOC_5
			,IOMUX_0__PADS_0__OE__GPIOC_4
			,IOMUX_0__PADS_0__OE__GPIOC_3
			,IOMUX_0__PADS_0__OE__GPIOC_2
			,IOMUX_0__PADS_0__OE__GPIOC_1
			,IOMUX_0__PADS_0__OE__GPIOC_0})
		,.PAD_GPIOC_DS0 ({
			IOMUX_0__PADS_0__DS0__GPIOC_15
			,IOMUX_0__PADS_0__DS0__GPIOC_14
			,IOMUX_0__PADS_0__DS0__GPIOC_13
			,IOMUX_0__PADS_0__DS0__GPIOC_12
			,IOMUX_0__PADS_0__DS0__GPIOC_11
			,IOMUX_0__PADS_0__DS0__GPIOC_10
			,IOMUX_0__PADS_0__DS0__GPIOC_9
			,IOMUX_0__PADS_0__DS0__GPIOC_8
			,IOMUX_0__PADS_0__DS0__GPIOC_7
			,IOMUX_0__PADS_0__DS0__GPIOC_6
			,IOMUX_0__PADS_0__DS0__GPIOC_5
			,IOMUX_0__PADS_0__DS0__GPIOC_4
			,IOMUX_0__PADS_0__DS0__GPIOC_3
			,IOMUX_0__PADS_0__DS0__GPIOC_2
			,IOMUX_0__PADS_0__DS0__GPIOC_1
			,IOMUX_0__PADS_0__DS0__GPIOC_0})
		,.PAD_GPIOC_DS1 ({
			IOMUX_0__PADS_0__DS1__GPIOC_15
			,IOMUX_0__PADS_0__DS1__GPIOC_14
			,IOMUX_0__PADS_0__DS1__GPIOC_13
			,IOMUX_0__PADS_0__DS1__GPIOC_12
			,IOMUX_0__PADS_0__DS1__GPIOC_11
			,IOMUX_0__PADS_0__DS1__GPIOC_10
			,IOMUX_0__PADS_0__DS1__GPIOC_9
			,IOMUX_0__PADS_0__DS1__GPIOC_8
			,IOMUX_0__PADS_0__DS1__GPIOC_7
			,IOMUX_0__PADS_0__DS1__GPIOC_6
			,IOMUX_0__PADS_0__DS1__GPIOC_5
			,IOMUX_0__PADS_0__DS1__GPIOC_4
			,IOMUX_0__PADS_0__DS1__GPIOC_3
			,IOMUX_0__PADS_0__DS1__GPIOC_2
			,IOMUX_0__PADS_0__DS1__GPIOC_1
			,IOMUX_0__PADS_0__DS1__GPIOC_0})
		,.PAD_GPIOC_IE  ({
			IOMUX_0__PADS_0__IE__GPIOC_15
			,IOMUX_0__PADS_0__IE__GPIOC_14
			,IOMUX_0__PADS_0__IE__GPIOC_13
			,IOMUX_0__PADS_0__IE__GPIOC_12
			,IOMUX_0__PADS_0__IE__GPIOC_11
			,IOMUX_0__PADS_0__IE__GPIOC_10
			,IOMUX_0__PADS_0__IE__GPIOC_9
			,IOMUX_0__PADS_0__IE__GPIOC_8
			,IOMUX_0__PADS_0__IE__GPIOC_7
			,IOMUX_0__PADS_0__IE__GPIOC_6
			,IOMUX_0__PADS_0__IE__GPIOC_5
			,IOMUX_0__PADS_0__IE__GPIOC_4
			,IOMUX_0__PADS_0__IE__GPIOC_3
			,IOMUX_0__PADS_0__IE__GPIOC_2
			,IOMUX_0__PADS_0__IE__GPIOC_1
			,IOMUX_0__PADS_0__IE__GPIOC_0})
		,.PAD_GPIOC_PE  ({
			IOMUX_0__PADS_0__PE__GPIOC_15
			,IOMUX_0__PADS_0__PE__GPIOC_14
			,IOMUX_0__PADS_0__PE__GPIOC_13
			,IOMUX_0__PADS_0__PE__GPIOC_12
			,IOMUX_0__PADS_0__PE__GPIOC_11
			,IOMUX_0__PADS_0__PE__GPIOC_10
			,IOMUX_0__PADS_0__PE__GPIOC_9
			,IOMUX_0__PADS_0__PE__GPIOC_8
			,IOMUX_0__PADS_0__PE__GPIOC_7
			,IOMUX_0__PADS_0__PE__GPIOC_6
			,IOMUX_0__PADS_0__PE__GPIOC_5
			,IOMUX_0__PADS_0__PE__GPIOC_4
			,IOMUX_0__PADS_0__PE__GPIOC_3
			,IOMUX_0__PADS_0__PE__GPIOC_2
			,IOMUX_0__PADS_0__PE__GPIOC_1
			,IOMUX_0__PADS_0__PE__GPIOC_0})
		,.PAD_GPIOC_PS  ({
			IOMUX_0__PADS_0__PS__GPIOC_15
			,IOMUX_0__PADS_0__PS__GPIOC_14
			,IOMUX_0__PADS_0__PS__GPIOC_13
			,IOMUX_0__PADS_0__PS__GPIOC_12
			,IOMUX_0__PADS_0__PS__GPIOC_11
			,IOMUX_0__PADS_0__PS__GPIOC_10
			,IOMUX_0__PADS_0__PS__GPIOC_9
			,IOMUX_0__PADS_0__PS__GPIOC_8
			,IOMUX_0__PADS_0__PS__GPIOC_7
			,IOMUX_0__PADS_0__PS__GPIOC_6
			,IOMUX_0__PADS_0__PS__GPIOC_5
			,IOMUX_0__PADS_0__PS__GPIOC_4
			,IOMUX_0__PADS_0__PS__GPIOC_3
			,IOMUX_0__PADS_0__PS__GPIOC_2
			,IOMUX_0__PADS_0__PS__GPIOC_1
			,IOMUX_0__PADS_0__PS__GPIOC_0})
		,.PAD_GPIOD_Y   ({
			IOMUX_0__PADS_0__Y__GPIOD_15
			,IOMUX_0__PADS_0__Y__GPIOD_14
			,IOMUX_0__PADS_0__Y__GPIOD_13
			,IOMUX_0__PADS_0__Y__GPIOD_12
			,IOMUX_0__PADS_0__Y__GPIOD_11
			,IOMUX_0__PADS_0__Y__GPIOD_10
			,IOMUX_0__PADS_0__Y__GPIOD_9
			,IOMUX_0__PADS_0__Y__GPIOD_8
			,IOMUX_0__PADS_0__Y__GPIOD_7
			,IOMUX_0__PADS_0__Y__GPIOD_6
			,IOMUX_0__PADS_0__Y__GPIOD_5
			,IOMUX_0__PADS_0__Y__GPIOD_4
			,IOMUX_0__PADS_0__Y__GPIOD_3
			,IOMUX_0__PADS_0__Y__GPIOD_2
			,IOMUX_0__PADS_0__Y__GPIOD_1
			,IOMUX_0__PADS_0__Y__GPIOD_0})
		,.PAD_GPIOD_A   ({
			IOMUX_0__PADS_0__A__GPIOD_15
			,IOMUX_0__PADS_0__A__GPIOD_14
			,IOMUX_0__PADS_0__A__GPIOD_13
			,IOMUX_0__PADS_0__A__GPIOD_12
			,IOMUX_0__PADS_0__A__GPIOD_11
			,IOMUX_0__PADS_0__A__GPIOD_10
			,IOMUX_0__PADS_0__A__GPIOD_9
			,IOMUX_0__PADS_0__A__GPIOD_8
			,IOMUX_0__PADS_0__A__GPIOD_7
			,IOMUX_0__PADS_0__A__GPIOD_6
			,IOMUX_0__PADS_0__A__GPIOD_5
			,IOMUX_0__PADS_0__A__GPIOD_4
			,IOMUX_0__PADS_0__A__GPIOD_3
			,IOMUX_0__PADS_0__A__GPIOD_2
			,IOMUX_0__PADS_0__A__GPIOD_1
			,IOMUX_0__PADS_0__A__GPIOD_0})
		,.PAD_GPIOD_OE  ({
			IOMUX_0__PADS_0__OE__GPIOD_15
			,IOMUX_0__PADS_0__OE__GPIOD_14
			,IOMUX_0__PADS_0__OE__GPIOD_13
			,IOMUX_0__PADS_0__OE__GPIOD_12
			,IOMUX_0__PADS_0__OE__GPIOD_11
			,IOMUX_0__PADS_0__OE__GPIOD_10
			,IOMUX_0__PADS_0__OE__GPIOD_9
			,IOMUX_0__PADS_0__OE__GPIOD_8
			,IOMUX_0__PADS_0__OE__GPIOD_7
			,IOMUX_0__PADS_0__OE__GPIOD_6
			,IOMUX_0__PADS_0__OE__GPIOD_5
			,IOMUX_0__PADS_0__OE__GPIOD_4
			,IOMUX_0__PADS_0__OE__GPIOD_3
			,IOMUX_0__PADS_0__OE__GPIOD_2
			,IOMUX_0__PADS_0__OE__GPIOD_1
			,IOMUX_0__PADS_0__OE__GPIOD_0})
		,.PAD_GPIOD_DS0 ({
			IOMUX_0__PADS_0__DS0__GPIOD_15
			,IOMUX_0__PADS_0__DS0__GPIOD_14
			,IOMUX_0__PADS_0__DS0__GPIOD_13
			,IOMUX_0__PADS_0__DS0__GPIOD_12
			,IOMUX_0__PADS_0__DS0__GPIOD_11
			,IOMUX_0__PADS_0__DS0__GPIOD_10
			,IOMUX_0__PADS_0__DS0__GPIOD_9
			,IOMUX_0__PADS_0__DS0__GPIOD_8
			,IOMUX_0__PADS_0__DS0__GPIOD_7
			,IOMUX_0__PADS_0__DS0__GPIOD_6
			,IOMUX_0__PADS_0__DS0__GPIOD_5
			,IOMUX_0__PADS_0__DS0__GPIOD_4
			,IOMUX_0__PADS_0__DS0__GPIOD_3
			,IOMUX_0__PADS_0__DS0__GPIOD_2
			,IOMUX_0__PADS_0__DS0__GPIOD_1
			,IOMUX_0__PADS_0__DS0__GPIOD_0})
		,.PAD_GPIOD_DS1 ({
			IOMUX_0__PADS_0__DS1__GPIOD_15
			,IOMUX_0__PADS_0__DS1__GPIOD_14
			,IOMUX_0__PADS_0__DS1__GPIOD_13
			,IOMUX_0__PADS_0__DS1__GPIOD_12
			,IOMUX_0__PADS_0__DS1__GPIOD_11
			,IOMUX_0__PADS_0__DS1__GPIOD_10
			,IOMUX_0__PADS_0__DS1__GPIOD_9
			,IOMUX_0__PADS_0__DS1__GPIOD_8
			,IOMUX_0__PADS_0__DS1__GPIOD_7
			,IOMUX_0__PADS_0__DS1__GPIOD_6
			,IOMUX_0__PADS_0__DS1__GPIOD_5
			,IOMUX_0__PADS_0__DS1__GPIOD_4
			,IOMUX_0__PADS_0__DS1__GPIOD_3
			,IOMUX_0__PADS_0__DS1__GPIOD_2
			,IOMUX_0__PADS_0__DS1__GPIOD_1
			,IOMUX_0__PADS_0__DS1__GPIOD_0})
		,.PAD_GPIOD_IE  ({
			IOMUX_0__PADS_0__IE__GPIOD_15
			,IOMUX_0__PADS_0__IE__GPIOD_14
			,IOMUX_0__PADS_0__IE__GPIOD_13
			,IOMUX_0__PADS_0__IE__GPIOD_12
			,IOMUX_0__PADS_0__IE__GPIOD_11
			,IOMUX_0__PADS_0__IE__GPIOD_10
			,IOMUX_0__PADS_0__IE__GPIOD_9
			,IOMUX_0__PADS_0__IE__GPIOD_8
			,IOMUX_0__PADS_0__IE__GPIOD_7
			,IOMUX_0__PADS_0__IE__GPIOD_6
			,IOMUX_0__PADS_0__IE__GPIOD_5
			,IOMUX_0__PADS_0__IE__GPIOD_4
			,IOMUX_0__PADS_0__IE__GPIOD_3
			,IOMUX_0__PADS_0__IE__GPIOD_2
			,IOMUX_0__PADS_0__IE__GPIOD_1
			,IOMUX_0__PADS_0__IE__GPIOD_0})
		,.PAD_GPIOD_PE  ({
			IOMUX_0__PADS_0__PE__GPIOD_15
			,IOMUX_0__PADS_0__PE__GPIOD_14
			,IOMUX_0__PADS_0__PE__GPIOD_13
			,IOMUX_0__PADS_0__PE__GPIOD_12
			,IOMUX_0__PADS_0__PE__GPIOD_11
			,IOMUX_0__PADS_0__PE__GPIOD_10
			,IOMUX_0__PADS_0__PE__GPIOD_9
			,IOMUX_0__PADS_0__PE__GPIOD_8
			,IOMUX_0__PADS_0__PE__GPIOD_7
			,IOMUX_0__PADS_0__PE__GPIOD_6
			,IOMUX_0__PADS_0__PE__GPIOD_5
			,IOMUX_0__PADS_0__PE__GPIOD_4
			,IOMUX_0__PADS_0__PE__GPIOD_3
			,IOMUX_0__PADS_0__PE__GPIOD_2
			,IOMUX_0__PADS_0__PE__GPIOD_1
			,IOMUX_0__PADS_0__PE__GPIOD_0})
		,.PAD_GPIOD_PS  ({
			IOMUX_0__PADS_0__PS__GPIOD_15
			,IOMUX_0__PADS_0__PS__GPIOD_14
			,IOMUX_0__PADS_0__PS__GPIOD_13
			,IOMUX_0__PADS_0__PS__GPIOD_12
			,IOMUX_0__PADS_0__PS__GPIOD_11
			,IOMUX_0__PADS_0__PS__GPIOD_10
			,IOMUX_0__PADS_0__PS__GPIOD_9
			,IOMUX_0__PADS_0__PS__GPIOD_8
			,IOMUX_0__PADS_0__PS__GPIOD_7
			,IOMUX_0__PADS_0__PS__GPIOD_6
			,IOMUX_0__PADS_0__PS__GPIOD_5
			,IOMUX_0__PADS_0__PS__GPIOD_4
			,IOMUX_0__PADS_0__PS__GPIOD_3
			,IOMUX_0__PADS_0__PS__GPIOD_2
			,IOMUX_0__PADS_0__PS__GPIOD_1
			,IOMUX_0__PADS_0__PS__GPIOD_0})
		,.PAD_SDMMC_Y   ({
			IOMUX_0__PADS_0__Y__SDMMC_11
			,IOMUX_0__PADS_0__Y__SDMMC_10
			,IOMUX_0__PADS_0__Y__SDMMC_9
			,IOMUX_0__PADS_0__Y__SDMMC_8
			,IOMUX_0__PADS_0__Y__SDMMC_7
			,IOMUX_0__PADS_0__Y__SDMMC_6
			,IOMUX_0__PADS_0__Y__SDMMC_5
			,IOMUX_0__PADS_0__Y__SDMMC_4
			,IOMUX_0__PADS_0__Y__SDMMC_3
			,IOMUX_0__PADS_0__Y__SDMMC_2
			,IOMUX_0__PADS_0__Y__SDMMC_1
			,IOMUX_0__PADS_0__Y__SDMMC_0})
		,.PAD_SDMMC_A   ({
			IOMUX_0__PADS_0__A__SDMMC_11
			,IOMUX_0__PADS_0__A__SDMMC_10
			,IOMUX_0__PADS_0__A__SDMMC_9
			,IOMUX_0__PADS_0__A__SDMMC_8
			,IOMUX_0__PADS_0__A__SDMMC_7
			,IOMUX_0__PADS_0__A__SDMMC_6
			,IOMUX_0__PADS_0__A__SDMMC_5
			,IOMUX_0__PADS_0__A__SDMMC_4
			,IOMUX_0__PADS_0__A__SDMMC_3
			,IOMUX_0__PADS_0__A__SDMMC_2
			,IOMUX_0__PADS_0__A__SDMMC_1
			,IOMUX_0__PADS_0__A__SDMMC_0})
		,.PAD_SDMMC_OE  ({
			IOMUX_0__PADS_0__OE__SDMMC_11
			,IOMUX_0__PADS_0__OE__SDMMC_10
			,IOMUX_0__PADS_0__OE__SDMMC_9
			,IOMUX_0__PADS_0__OE__SDMMC_8
			,IOMUX_0__PADS_0__OE__SDMMC_7
			,IOMUX_0__PADS_0__OE__SDMMC_6
			,IOMUX_0__PADS_0__OE__SDMMC_5
			,IOMUX_0__PADS_0__OE__SDMMC_4
			,IOMUX_0__PADS_0__OE__SDMMC_3
			,IOMUX_0__PADS_0__OE__SDMMC_2
			,IOMUX_0__PADS_0__OE__SDMMC_1
			,IOMUX_0__PADS_0__OE__SDMMC_0})
		,.PAD_SDMMC_DS0 ({
			IOMUX_0__PADS_0__DS0__SDMMC_11
			,IOMUX_0__PADS_0__DS0__SDMMC_10
			,IOMUX_0__PADS_0__DS0__SDMMC_9
			,IOMUX_0__PADS_0__DS0__SDMMC_8
			,IOMUX_0__PADS_0__DS0__SDMMC_7
			,IOMUX_0__PADS_0__DS0__SDMMC_6
			,IOMUX_0__PADS_0__DS0__SDMMC_5
			,IOMUX_0__PADS_0__DS0__SDMMC_4
			,IOMUX_0__PADS_0__DS0__SDMMC_3
			,IOMUX_0__PADS_0__DS0__SDMMC_2
			,IOMUX_0__PADS_0__DS0__SDMMC_1
			,IOMUX_0__PADS_0__DS0__SDMMC_0})
		,.PAD_SDMMC_DS1 ({
			IOMUX_0__PADS_0__DS1__SDMMC_11
			,IOMUX_0__PADS_0__DS1__SDMMC_10
			,IOMUX_0__PADS_0__DS1__SDMMC_9
			,IOMUX_0__PADS_0__DS1__SDMMC_8
			,IOMUX_0__PADS_0__DS1__SDMMC_7
			,IOMUX_0__PADS_0__DS1__SDMMC_6
			,IOMUX_0__PADS_0__DS1__SDMMC_5
			,IOMUX_0__PADS_0__DS1__SDMMC_4
			,IOMUX_0__PADS_0__DS1__SDMMC_3
			,IOMUX_0__PADS_0__DS1__SDMMC_2
			,IOMUX_0__PADS_0__DS1__SDMMC_1
			,IOMUX_0__PADS_0__DS1__SDMMC_0})
		,.PAD_SDMMC_IE  ({
			IOMUX_0__PADS_0__IE__SDMMC_11
			,IOMUX_0__PADS_0__IE__SDMMC_10
			,IOMUX_0__PADS_0__IE__SDMMC_9
			,IOMUX_0__PADS_0__IE__SDMMC_8
			,IOMUX_0__PADS_0__IE__SDMMC_7
			,IOMUX_0__PADS_0__IE__SDMMC_6
			,IOMUX_0__PADS_0__IE__SDMMC_5
			,IOMUX_0__PADS_0__IE__SDMMC_4
			,IOMUX_0__PADS_0__IE__SDMMC_3
			,IOMUX_0__PADS_0__IE__SDMMC_2
			,IOMUX_0__PADS_0__IE__SDMMC_1
			,IOMUX_0__PADS_0__IE__SDMMC_0})
		,.PAD_SDMMC_PE  ({
			IOMUX_0__PADS_0__PE__SDMMC_11
			,IOMUX_0__PADS_0__PE__SDMMC_10
			,IOMUX_0__PADS_0__PE__SDMMC_9
			,IOMUX_0__PADS_0__PE__SDMMC_8
			,IOMUX_0__PADS_0__PE__SDMMC_7
			,IOMUX_0__PADS_0__PE__SDMMC_6
			,IOMUX_0__PADS_0__PE__SDMMC_5
			,IOMUX_0__PADS_0__PE__SDMMC_4
			,IOMUX_0__PADS_0__PE__SDMMC_3
			,IOMUX_0__PADS_0__PE__SDMMC_2
			,IOMUX_0__PADS_0__PE__SDMMC_1
			,IOMUX_0__PADS_0__PE__SDMMC_0})
		,.PAD_SDMMC_PS  ({
			IOMUX_0__PADS_0__PS__SDMMC_11
			,IOMUX_0__PADS_0__PS__SDMMC_10
			,IOMUX_0__PADS_0__PS__SDMMC_9
			,IOMUX_0__PADS_0__PS__SDMMC_8
			,IOMUX_0__PADS_0__PS__SDMMC_7
			,IOMUX_0__PADS_0__PS__SDMMC_6
			,IOMUX_0__PADS_0__PS__SDMMC_5
			,IOMUX_0__PADS_0__PS__SDMMC_4
			,IOMUX_0__PADS_0__PS__SDMMC_3
			,IOMUX_0__PADS_0__PS__SDMMC_2
			,IOMUX_0__PADS_0__PS__SDMMC_1
			,IOMUX_0__PADS_0__PS__SDMMC_0})
		,.CORE_GPIOA_OE (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_A__IO__OE)
		,.CORE_GPIOA_DS0(16'hffff)
		,.CORE_GPIOA_DS1(16'hffff)
		,.CORE_GPIOA_IE (16'hffff)
		,.CORE_GPIOA_PE (16'h0)
		,.CORE_GPIOA_PS (16'h0)
		,.CORE_GPIOA_Y  (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_A__IO__Y)
		,.CORE_GPIOA_A  (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_A__IO__A)
		,.CORE_GPIOA_FNC(16'hffff)
		,.CORE_GPIOB_OE (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_B__IO__OE)
		,.CORE_GPIOB_DS0(16'hffff)
		,.CORE_GPIOB_DS1(16'hffff)
		,.CORE_GPIOB_IE (16'hffff)
		,.CORE_GPIOB_PE (16'h0)
		,.CORE_GPIOB_PS (16'h0)
		,.CORE_GPIOB_Y  (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_B__IO__Y)
		,.CORE_GPIOB_A  (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_B__IO__A)
		,.CORE_GPIOB_FNC(16'hffff)
		,.CORE_GPIOC_OE (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_C__IO__OE)
		,.CORE_GPIOC_DS0(16'hffff)
		,.CORE_GPIOC_DS1(16'hffff)
		,.CORE_GPIOC_IE (16'hffff)
		,.CORE_GPIOC_PE (16'h0)
		,.CORE_GPIOC_PS (16'h0)
		,.CORE_GPIOC_Y  (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_C__IO__Y)
		,.CORE_GPIOC_A  (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_C__IO__A)
		,.CORE_GPIOC_FNC(16'hffff)
		,.CORE_GPIOD_OE (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_D__IO__OE)
		,.CORE_GPIOD_DS0(16'hffff)
		,.CORE_GPIOD_DS1(16'hffff)
		,.CORE_GPIOD_IE (16'hffff)
		,.CORE_GPIOD_PE (16'h0)
		,.CORE_GPIOD_PS (16'h0)
		,.CORE_GPIOD_Y  (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_D__IO__Y)
		,.CORE_GPIOD_A  (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__GPIO_D__IO__A)
		,.CORE_GPIOD_FNC(16'hffff)
		,.UART_0_RX     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__UART_0__IO__RX)
		,.UART_0_TX     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__UART_0__IO__TX)
		,.UART_1_RX     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__UART_1__IO__RX)
		,.UART_1_TX     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__UART_1__IO__TX)
		,.I2C_0_SCLI    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_0__IO__SCLI)
		,.I2C_0_SCLOE   (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_0__IO__SCLOE)
		,.I2C_0_SDIN    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_0__IO__SDIN)
		,.I2C_0_SDOE    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_0__IO__SDOE)
		,.I2C_1_SCLI    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_1__IO__SCLI)
		,.I2C_1_SCLOE   (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_1__IO__SCLOE)
		,.I2C_1_SDIN    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_1__IO__SDIN)
		,.I2C_1_SDOE    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_1__IO__SDOE)
		,.I2C_2_SCLI    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_2__IO__SCLI)
		,.I2C_2_SCLOE   (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_2__IO__SCLOE)
		,.I2C_2_SDIN    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_2__IO__SDIN)
		,.I2C_2_SDOE    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_2__IO__SDOE)
		,.I2C_3_SCLI    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_3__IO__SCLI)
		,.I2C_3_SCLOE   (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_3__IO__SCLOE)
		,.I2C_3_SDIN    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_3__IO__SDIN)
		,.I2C_3_SDOE    (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__I2C_3__IO__SDOE)
		,.SPI_0_SCK     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_0__IO__SCK)
		,.SPI_0_SCS     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_0__IO__SCS)
		,.SPI_0_SDO     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_0__IO__SDO)
		,.SPI_0_SDI     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_0__IO__SDI)
		,.SPI_0_OEN     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_0__IO__OEN)
		,.SPI_1_SCK     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_1__IO__SCK)
		,.SPI_1_SCS     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_1__IO__SCS)
		,.SPI_1_SDO     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_1__IO__SDO)
		,.SPI_1_SDI     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_1__IO__SDI)
		,.SPI_1_OEN     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_1__IO__OEN)
		,.SPI_2_SCK     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_2__IO__SCK)
		,.SPI_2_SCS     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_2__IO__SCS)
		,.SPI_2_SDO     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_2__IO__SDO)
		,.SPI_2_SDI     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_2__IO__SDI)
		,.SPI_2_OEN     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_2__IO__OEN)
		,.SPI_3_SCK     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_3__IO__SCK)
		,.SPI_3_SCS     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_3__IO__SCS)
		,.SPI_3_SDO     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_3__IO__SDO)
		,.SPI_3_SDI     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_3__IO__SDI)
		,.SPI_3_OEN     (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SPI_3__IO__OEN)
		,.SDMMC_0_CLK   (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__CLK)
		,.SDMMC_0_CMD   (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__CMD)
		,.SDMMC_0_RSP   (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__RSP)
		,.SDMMC_0_CMDOEN(CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__CMDOEN)
		,.SDMMC_0_DOUT  (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__DOUT)
		,.SDMMC_0_DIN   (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__DIN)
		,.SDMMC_0_DOEN  (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__DOEN)
		,.SDMMC_0_DQS   (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__DQS)
		,.SDMMC_0_RSTN  (CORE_0__IOMUX_0__PERI_SUBSYSTEM_0__SDMMC_0__IO__RSTN)
	);

endmodule 
