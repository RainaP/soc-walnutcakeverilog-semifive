//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__MSHR_6(
  input         rf_reset,
  input         clock,
  input         reset,
  input         io_allocate_valid,
  input         io_allocate_bits_prio_2,
  input         io_allocate_bits_control,
  input  [2:0]  io_allocate_bits_opcode,
  input  [2:0]  io_allocate_bits_param,
  input  [2:0]  io_allocate_bits_size,
  input  [6:0]  io_allocate_bits_source,
  input  [17:0] io_allocate_bits_tag,
  input  [5:0]  io_allocate_bits_offset,
  input  [4:0]  io_allocate_bits_put,
  input  [9:0]  io_allocate_bits_set,
  input         io_allocate_bits_repeat,
  input         io_directory_valid,
  input         io_directory_bits_dirty,
  input  [1:0]  io_directory_bits_state,
  input  [3:0]  io_directory_bits_clients,
  input  [17:0] io_directory_bits_tag,
  input         io_directory_bits_hit,
  input  [2:0]  io_directory_bits_way,
  output        io_status_valid,
  output [9:0]  io_status_bits_set,
  output [17:0] io_status_bits_tag,
  output [2:0]  io_status_bits_way,
  output        io_status_bits_blockB,
  output        io_status_bits_nestB,
  output        io_status_bits_blockC,
  output        io_status_bits_nestC,
  input         io_schedule_ready,
  output        io_schedule_valid,
  output        io_schedule_bits_a_valid,
  output [17:0] io_schedule_bits_a_bits_tag,
  output [9:0]  io_schedule_bits_a_bits_set,
  output [2:0]  io_schedule_bits_a_bits_param,
  output        io_schedule_bits_a_bits_block,
  output        io_schedule_bits_b_valid,
  output [2:0]  io_schedule_bits_b_bits_param,
  output [17:0] io_schedule_bits_b_bits_tag,
  output [9:0]  io_schedule_bits_b_bits_set,
  output [3:0]  io_schedule_bits_b_bits_clients,
  output        io_schedule_bits_c_valid,
  output [2:0]  io_schedule_bits_c_bits_opcode,
  output [2:0]  io_schedule_bits_c_bits_param,
  output [17:0] io_schedule_bits_c_bits_tag,
  output [9:0]  io_schedule_bits_c_bits_set,
  output [2:0]  io_schedule_bits_c_bits_way,
  output        io_schedule_bits_c_bits_dirty,
  output        io_schedule_bits_d_valid,
  output        io_schedule_bits_d_bits_prio_2,
  output [2:0]  io_schedule_bits_d_bits_opcode,
  output [2:0]  io_schedule_bits_d_bits_param,
  output [2:0]  io_schedule_bits_d_bits_size,
  output [6:0]  io_schedule_bits_d_bits_source,
  output [5:0]  io_schedule_bits_d_bits_offset,
  output [4:0]  io_schedule_bits_d_bits_put,
  output [9:0]  io_schedule_bits_d_bits_set,
  output [2:0]  io_schedule_bits_d_bits_way,
  output        io_schedule_bits_d_bits_bad,
  output        io_schedule_bits_d_bits_user_hit_cache,
  output        io_schedule_bits_e_valid,
  output [2:0]  io_schedule_bits_e_bits_sink,
  output        io_schedule_bits_x_valid,
  output        io_schedule_bits_dir_valid,
  output [9:0]  io_schedule_bits_dir_bits_set,
  output [2:0]  io_schedule_bits_dir_bits_way,
  output        io_schedule_bits_dir_bits_data_dirty,
  output [1:0]  io_schedule_bits_dir_bits_data_state,
  output [3:0]  io_schedule_bits_dir_bits_data_clients,
  output [17:0] io_schedule_bits_dir_bits_data_tag,
  output        io_schedule_bits_reload,
  input         io_sinkc_valid,
  input         io_sinkc_bits_last,
  input  [17:0] io_sinkc_bits_tag,
  input  [6:0]  io_sinkc_bits_source,
  input  [2:0]  io_sinkc_bits_param,
  input         io_sinkc_bits_data,
  input         io_sinkd_valid,
  input         io_sinkd_bits_last,
  input  [2:0]  io_sinkd_bits_opcode,
  input  [2:0]  io_sinkd_bits_param,
  input  [2:0]  io_sinkd_bits_sink,
  input         io_sinkd_bits_denied,
  input         io_sinke_valid,
  input  [9:0]  io_nestedwb_set,
  input  [17:0] io_nestedwb_tag,
  input         io_nestedwb_b_toN,
  input         io_nestedwb_b_toB,
  input         io_nestedwb_b_clr_dirty,
  input         io_nestedwb_c_set_dirty
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
`endif // RANDOMIZE_REG_INIT
  reg  request_valid; // @[MSHR.scala 98:30]
  reg  request_prio_2; // @[MSHR.scala 99:20]
  reg  request_control; // @[MSHR.scala 99:20]
  reg [2:0] request_opcode; // @[MSHR.scala 99:20]
  reg [2:0] request_param; // @[MSHR.scala 99:20]
  reg [2:0] request_size; // @[MSHR.scala 99:20]
  reg [6:0] request_source; // @[MSHR.scala 99:20]
  reg [17:0] request_tag; // @[MSHR.scala 99:20]
  reg [5:0] request_offset; // @[MSHR.scala 99:20]
  reg [4:0] request_put; // @[MSHR.scala 99:20]
  reg [9:0] request_set; // @[MSHR.scala 99:20]
  reg  meta_valid; // @[MSHR.scala 100:27]
  reg  meta_dirty; // @[MSHR.scala 101:17]
  reg [1:0] meta_state; // @[MSHR.scala 101:17]
  reg [3:0] meta_clients; // @[MSHR.scala 101:17]
  reg [17:0] meta_tag; // @[MSHR.scala 101:17]
  reg  meta_hit; // @[MSHR.scala 101:17]
  reg [2:0] meta_way; // @[MSHR.scala 101:17]
  wire  _T_10 = meta_state == 2'h1; // @[MSHR.scala 110:22]
  wire  _T_15 = meta_state == 2'h2; // @[MSHR.scala 113:22]
  reg  s_rprobe; // @[MSHR.scala 123:33]
  reg  w_rprobeackfirst; // @[MSHR.scala 124:33]
  reg  w_rprobeacklast; // @[MSHR.scala 125:33]
  reg  s_release; // @[MSHR.scala 126:33]
  reg  w_releaseack; // @[MSHR.scala 127:33]
  reg  s_pprobe; // @[MSHR.scala 128:33]
  reg  s_acquire; // @[MSHR.scala 129:33]
  reg  s_flush; // @[MSHR.scala 130:33]
  reg  w_grantfirst; // @[MSHR.scala 131:33]
  reg  w_grantlast; // @[MSHR.scala 132:33]
  reg  w_grant; // @[MSHR.scala 133:33]
  reg  w_pprobeackfirst; // @[MSHR.scala 134:33]
  reg  w_pprobeacklast; // @[MSHR.scala 135:33]
  reg  w_pprobeack; // @[MSHR.scala 136:33]
  reg  s_grantack; // @[MSHR.scala 138:33]
  reg  s_execute; // @[MSHR.scala 139:33]
  reg  w_grantack; // @[MSHR.scala 140:33]
  reg  s_writeback; // @[MSHR.scala 141:33]
  reg [2:0] sink; // @[MSHR.scala 149:17]
  reg  gotT; // @[MSHR.scala 150:17]
  reg  bad_grant; // @[MSHR.scala 151:22]
  reg [3:0] probes_done; // @[MSHR.scala 152:24]
  reg [3:0] probes_toN; // @[MSHR.scala 153:23]
  wire  _T_28 = meta_state != 2'h0; // @[MSHR.scala 157:34]
  wire  _T_30 = io_nestedwb_set == request_set; // @[MSHR.scala 158:25]
  wire  _T_31 = meta_valid & meta_state != 2'h0 & _T_30; // @[MSHR.scala 157:46]
  wire  _GEN_0 = io_nestedwb_b_clr_dirty ? 1'h0 : meta_dirty; // @[MSHR.scala 159:36 MSHR.scala 159:49 MSHR.scala 101:17]
  wire  _GEN_1 = io_nestedwb_c_set_dirty | _GEN_0; // @[MSHR.scala 160:36 MSHR.scala 160:49]
  wire  _GEN_4 = _T_31 & io_nestedwb_tag == meta_tag ? _GEN_1 : meta_dirty; // @[MSHR.scala 158:74 MSHR.scala 101:17]
  wire  _io_status_bits_blockB_T = ~meta_valid; // @[MSHR.scala 170:28]
  wire  _io_status_bits_blockB_T_6 = ~w_grantfirst; // @[MSHR.scala 170:103]
  wire  no_wait = w_rprobeacklast & w_releaseack & w_grantlast & w_pprobeacklast & w_grantack; // @[MSHR.scala 185:83]
  wire  _request_is_hint_T = request_opcode == 3'h5; // @[MSHR.scala 188:59]
  wire  execute_ready = w_pprobeack & w_grant; // @[MSHR.scala 189:55]
  wire  acquire_ready = s_pprobe & s_release; // @[MSHR.scala 190:32]
  wire  _io_schedule_bits_b_valid_T = ~s_rprobe; // @[MSHR.scala 193:31]
  wire  _io_schedule_bits_c_valid_T = ~s_release; // @[MSHR.scala 194:32]
  wire  _io_schedule_bits_c_valid_T_1 = ~s_release & w_rprobeackfirst; // @[MSHR.scala 194:43]
  wire  _io_schedule_bits_dir_valid_T_3 = ~s_writeback & no_wait; // @[MSHR.scala 198:83]
  wire  _io_schedule_valid_T_2 = io_schedule_bits_a_valid | io_schedule_bits_b_valid | io_schedule_bits_c_valid |
    io_schedule_bits_d_valid; // @[MSHR.scala 200:105]
  wire  _GEN_7 = w_rprobeackfirst | s_release; // @[MSHR.scala 207:35 MSHR.scala 207:50 MSHR.scala 126:33]
  wire  _GEN_8 = acquire_ready | s_acquire; // @[MSHR.scala 209:35 MSHR.scala 209:50 MSHR.scala 129:33]
  wire  _GEN_9 = w_releaseack | s_flush; // @[MSHR.scala 210:35 MSHR.scala 210:50 MSHR.scala 130:33]
  wire  _GEN_11 = w_grantfirst | s_grantack; // @[MSHR.scala 212:35 MSHR.scala 212:50 MSHR.scala 138:33]
  wire  _GEN_12 = execute_ready | s_execute; // @[MSHR.scala 213:35 MSHR.scala 213:50 MSHR.scala 139:33]
  wire  _GEN_13 = no_wait | s_writeback; // @[MSHR.scala 214:35 MSHR.scala 214:50 MSHR.scala 141:33]
  wire  _GEN_14 = no_wait ? 1'h0 : request_valid; // @[MSHR.scala 216:20 MSHR.scala 217:21 MSHR.scala 98:30]
  wire  _GEN_15 = no_wait ? 1'h0 : meta_valid; // @[MSHR.scala 216:20 MSHR.scala 218:18 MSHR.scala 100:27]
  wire  _GEN_16 = io_schedule_ready | s_rprobe; // @[MSHR.scala 205:28 MSHR.scala 206:50 MSHR.scala 123:33]
  wire  _GEN_18 = io_schedule_ready | s_pprobe; // @[MSHR.scala 205:28 MSHR.scala 208:50 MSHR.scala 128:33]
  wire  _GEN_25 = io_schedule_ready ? _GEN_14 : request_valid; // @[MSHR.scala 205:28 MSHR.scala 98:30]
  wire  _GEN_26 = io_schedule_ready ? _GEN_15 : meta_valid; // @[MSHR.scala 205:28 MSHR.scala 100:27]
  wire  req_clientBit_lo_lo = request_source == 7'h60; // @[Parameters.scala 46:9]
  wire  req_clientBit_lo_hi = request_source == 7'h61; // @[Parameters.scala 46:9]
  wire  req_clientBit_hi_lo = request_source == 7'h40; // @[Parameters.scala 46:9]
  wire  req_clientBit_hi_hi = request_source == 7'h41; // @[Parameters.scala 46:9]
  wire [3:0] req_clientBit = {req_clientBit_hi_hi,req_clientBit_hi_lo,req_clientBit_lo_hi,req_clientBit_lo_lo}; // @[Cat.scala 30:58]
  wire  _req_needT_T_1 = ~request_opcode[2]; // @[Parameters.scala 281:5]
  wire  _req_needT_T_3 = request_param == 3'h1; // @[Parameters.scala 282:42]
  wire  _req_needT_T_4 = _request_is_hint_T & request_param == 3'h1; // @[Parameters.scala 282:33]
  wire  _req_needT_T_5 = ~request_opcode[2] | _req_needT_T_4; // @[Parameters.scala 281:16]
  wire  _req_needT_T_7 = request_opcode == 3'h7; // @[Parameters.scala 283:52]
  wire  _req_needT_T_8 = request_opcode == 3'h6 | request_opcode == 3'h7; // @[Parameters.scala 283:42]
  wire  _req_needT_T_10 = (request_opcode == 3'h6 | request_opcode == 3'h7) & request_param != 3'h0; // @[Parameters.scala 283:80]
  wire  req_needT = _req_needT_T_5 | _req_needT_T_10; // @[Parameters.scala 282:70]
  wire [3:0] _no_other_clients_T = ~probes_toN; // @[MSHR.scala 228:42]
  wire [3:0] _no_other_clients_T_1 = meta_clients & _no_other_clients_T; // @[MSHR.scala 228:40]
  wire [3:0] _no_other_clients_T_2 = ~req_clientBit; // @[MSHR.scala 228:56]
  wire [3:0] _no_other_clients_T_3 = _no_other_clients_T_1 & _no_other_clients_T_2; // @[MSHR.scala 228:54]
  wire  no_other_clients = _no_other_clients_T_3 == 4'h0; // @[MSHR.scala 228:72]
  wire  req_haveT = meta_hit ? no_other_clients & (_T_15 | meta_state == 2'h3 | gotT) : gotT; // @[MSHR.scala 229:22]
  wire [3:0] req_proveN = request_opcode == 3'h4 ? req_clientBit : 4'h0; // @[MSHR.scala 230:23]
  wire [1:0] _final_meta_writeback_state_T_3 = request_param != 3'h3 & _T_15 ? 2'h3 : meta_state; // @[MSHR.scala 234:40]
  wire  _final_meta_writeback_clients_T_4 = _req_needT_T_3 | request_param == 3'h2 | request_param == 3'h5; // @[Parameters.scala 286:66]
  wire [3:0] _final_meta_writeback_clients_T_5 = _final_meta_writeback_clients_T_4 ? req_clientBit : 4'h0; // @[MSHR.scala 235:56]
  wire [3:0] _final_meta_writeback_clients_T_6 = ~_final_meta_writeback_clients_T_5; // @[MSHR.scala 235:52]
  wire [3:0] _final_meta_writeback_clients_T_7 = meta_clients & _final_meta_writeback_clients_T_6; // @[MSHR.scala 235:50]
  wire  _GEN_27 = meta_hit ? 1'h0 : meta_dirty; // @[MSHR.scala 238:21 MSHR.scala 239:36]
  wire [3:0] _GEN_29 = meta_hit ? _no_other_clients_T_1 : meta_clients; // @[MSHR.scala 238:21 MSHR.scala 244:36]
  wire [1:0] _GEN_30 = meta_hit ? 2'h0 : meta_state; // @[MSHR.scala 248:21 MSHR.scala 250:36]
  wire  _final_meta_writeback_dirty_T_2 = meta_hit & meta_dirty; // @[MSHR.scala 257:45]
  wire [1:0] _final_meta_writeback_state_T_16 = meta_hit ? meta_state : 2'h0; // @[MSHR.scala 260:22]
  wire [1:0] _final_meta_writeback_state_T_17 = gotT ? 2'h3 : 2'h1; // @[MSHR.scala 261:25]
  wire [1:0] _final_meta_writeback_state_T_21 = 2'h1 == _final_meta_writeback_state_T_16 ?
    _final_meta_writeback_state_T_17 : _final_meta_writeback_state_T_17; // @[Mux.scala 80:57]
  wire [1:0] _final_meta_writeback_state_T_23 = 2'h3 == _final_meta_writeback_state_T_16 ? 2'h3 :
    _final_meta_writeback_state_T_21; // @[Mux.scala 80:57]
  wire [1:0] _final_meta_writeback_state_T_25 = 2'h2 == _final_meta_writeback_state_T_16 ? 2'h3 :
    _final_meta_writeback_state_T_23; // @[Mux.scala 80:57]
  wire [1:0] _final_meta_writeback_state_T_26 = _req_needT_T_8 & req_haveT ? 2'h2 : _final_meta_writeback_state_T_25; // @[MSHR.scala 259:10]
  wire [3:0] _final_meta_writeback_clients_T_14 = ~req_proveN; // @[MSHR.scala 266:50]
  wire [3:0] _final_meta_writeback_clients_T_15 = _no_other_clients_T_1 & _final_meta_writeback_clients_T_14; // @[MSHR.scala 266:48]
  wire [3:0] _final_meta_writeback_clients_T_16 = meta_hit ? _final_meta_writeback_clients_T_15 : 4'h0; // @[MSHR.scala 266:10]
  wire [3:0] _final_meta_writeback_clients_T_17 = _req_needT_T_8 ? req_clientBit : 4'h0; // @[MSHR.scala 267:10]
  wire [3:0] _final_meta_writeback_clients_T_18 = _final_meta_writeback_clients_T_16 |
    _final_meta_writeback_clients_T_17; // @[MSHR.scala 266:72]
  wire  _GEN_32 = request_control ? _GEN_27 : meta_hit & meta_dirty | _req_needT_T_1; // @[MSHR.scala 247:57 MSHR.scala 257:32]
  wire [1:0] _GEN_33 = request_control ? _GEN_30 : _final_meta_writeback_state_T_26; // @[MSHR.scala 247:57 MSHR.scala 258:32]
  wire [3:0] _GEN_34 = request_control ? _GEN_29 : _final_meta_writeback_clients_T_18; // @[MSHR.scala 247:57 MSHR.scala 265:34]
  wire  _GEN_35 = request_control ? 1'h0 : 1'h1; // @[MSHR.scala 247:57 MSHR.scala 253:30 MSHR.scala 269:30]
  wire [17:0] _GEN_36 = request_control ? meta_tag : request_tag; // @[MSHR.scala 247:57 MSHR.scala 268:30]
  wire  _GEN_42 = request_prio_2 ? meta_dirty | request_opcode[0] : _GEN_32; // @[MSHR.scala 232:54 MSHR.scala 233:34]
  wire [1:0] _GEN_43 = request_prio_2 ? _final_meta_writeback_state_T_3 : _GEN_33; // @[MSHR.scala 232:54 MSHR.scala 234:34]
  wire [3:0] _GEN_44 = request_prio_2 ? _final_meta_writeback_clients_T_7 : _GEN_34; // @[MSHR.scala 232:54 MSHR.scala 235:34]
  wire  _GEN_45 = request_prio_2 | _GEN_35; // @[MSHR.scala 232:54 MSHR.scala 236:34]
  wire [17:0] final_meta_writeback_tag = request_prio_2 ? meta_tag : _GEN_36; // @[MSHR.scala 232:54]
  wire [1:0] _GEN_49 = meta_hit ? 2'h1 : 2'h0; // @[MSHR.scala 274:21 MSHR.scala 279:36 MSHR.scala 285:36]
  wire [3:0] _GEN_50 = meta_hit ? _no_other_clients_T_1 : 4'h0; // @[MSHR.scala 274:21 MSHR.scala 280:36 MSHR.scala 286:36]
  wire  final_meta_writeback_hit = bad_grant ? meta_hit : _GEN_45; // @[MSHR.scala 273:20]
  wire  final_meta_writeback_dirty = bad_grant ? 1'h0 : _GEN_42; // @[MSHR.scala 273:20]
  wire [1:0] final_meta_writeback_state = bad_grant ? _GEN_49 : _GEN_43; // @[MSHR.scala 273:20]
  wire [3:0] final_meta_writeback_clients = bad_grant ? _GEN_50 : _GEN_44; // @[MSHR.scala 273:20]
  wire [3:0] _honour_BtoT_T = meta_clients & req_clientBit; // @[MSHR.scala 298:47]
  wire  honour_BtoT = meta_hit & |_honour_BtoT_T; // @[MSHR.scala 298:30]
  wire [1:0] _io_schedule_bits_a_bits_param_T = meta_hit ? 2'h2 : 2'h1; // @[MSHR.scala 304:56]
  wire [1:0] _io_schedule_bits_a_bits_param_T_1 = req_needT ? _io_schedule_bits_a_bits_param_T : 2'h0; // @[MSHR.scala 304:41]
  wire  _io_schedule_bits_a_bits_block_T_4 = ~(request_opcode == 3'h0 | _req_needT_T_7); // @[MSHR.scala 306:38]
  wire [1:0] _io_schedule_bits_b_bits_param_T_1 = req_needT ? 2'h2 : 2'h1; // @[MSHR.scala 309:97]
  wire [2:0] _io_schedule_bits_b_bits_param_T_2 = {{1'd0}, _io_schedule_bits_b_bits_param_T_1}; // @[MSHR.scala 309:61]
  wire [2:0] _io_schedule_bits_c_bits_opcode_T_1 = meta_dirty ? 3'h7 : 3'h6; // @[MSHR.scala 313:57]
  wire [2:0] _io_schedule_bits_c_bits_opcode_T_3 = _final_meta_writeback_dirty_T_2 ? 3'h5 : 3'h4; // @[MSHR.scala 313:96]
  wire [2:0] _io_schedule_bits_c_bits_param_T_2 = _T_10 ? 3'h2 : 3'h1; // @[MSHR.scala 314:57]
  wire  _io_schedule_bits_c_bits_param_T_3 = ~meta_hit; // @[MSHR.scala 316:53]
  wire [2:0] _io_schedule_bits_c_bits_param_T_5 = _T_10 ? 3'h4 : 3'h3; // @[MSHR.scala 316:73]
  wire [2:0] _io_schedule_bits_c_bits_param_T_6 = _io_schedule_bits_c_bits_param_T_3 ? 3'h5 :
    _io_schedule_bits_c_bits_param_T_5; // @[MSHR.scala 316:52]
  wire [2:0] _io_schedule_bits_c_bits_param_T_9 = _T_10 ? 3'h4 : 3'h0; // @[MSHR.scala 317:73]
  wire [2:0] _io_schedule_bits_c_bits_param_T_10 = _io_schedule_bits_c_bits_param_T_3 ? 3'h5 :
    _io_schedule_bits_c_bits_param_T_9; // @[MSHR.scala 317:52]
  wire [2:0] _io_schedule_bits_c_bits_param_T_14 = _io_schedule_bits_c_bits_param_T_3 ? 3'h5 :
    _io_schedule_bits_c_bits_param_T_2; // @[MSHR.scala 318:52]
  wire [2:0] _io_schedule_bits_c_bits_param_T_16 = 3'h0 == request_param ? _io_schedule_bits_c_bits_param_T_6 : 3'h0; // @[Mux.scala 80:57]
  wire [2:0] _io_schedule_bits_c_bits_param_T_18 = 3'h1 == request_param ? _io_schedule_bits_c_bits_param_T_10 :
    _io_schedule_bits_c_bits_param_T_16; // @[Mux.scala 80:57]
  wire [2:0] _io_schedule_bits_c_bits_param_T_20 = 3'h2 == request_param ? _io_schedule_bits_c_bits_param_T_14 :
    _io_schedule_bits_c_bits_param_T_18; // @[Mux.scala 80:57]
  wire [1:0] _io_schedule_bits_d_bits_param_T_1 = req_haveT ? 2'h1 : 2'h0; // @[MSHR.scala 328:53]
  wire [1:0] _io_schedule_bits_d_bits_param_T_2 = honour_BtoT ? 2'h2 : 2'h1; // @[MSHR.scala 329:53]
  wire [2:0] _io_schedule_bits_d_bits_param_T_4 = 3'h0 == request_param ? {{1'd0}, _io_schedule_bits_d_bits_param_T_1}
     : 3'h0; // @[Mux.scala 80:57]
  wire [2:0] _io_schedule_bits_d_bits_param_T_6 = 3'h2 == request_param ? {{1'd0}, _io_schedule_bits_d_bits_param_T_2}
     : _io_schedule_bits_d_bits_param_T_4; // @[Mux.scala 80:57]
  wire [2:0] _io_schedule_bits_d_bits_param_T_8 = 3'h1 == request_param ? 3'h1 : _io_schedule_bits_d_bits_param_T_6; // @[Mux.scala 80:57]
  wire  probe_bit_lo_lo = io_sinkc_bits_source == 7'h60; // @[Parameters.scala 46:9]
  wire  probe_bit_lo_hi = io_sinkc_bits_source == 7'h61; // @[Parameters.scala 46:9]
  wire  probe_bit_hi_lo = io_sinkc_bits_source == 7'h40; // @[Parameters.scala 46:9]
  wire  probe_bit_hi_hi = io_sinkc_bits_source == 7'h41; // @[Parameters.scala 46:9]
  wire [3:0] probe_bit = {probe_bit_hi_hi,probe_bit_hi_lo,probe_bit_lo_hi,probe_bit_lo_lo}; // @[Cat.scala 30:58]
  wire [3:0] _last_probe_T = probes_done | probe_bit; // @[MSHR.scala 499:33]
  wire  last_probe = _last_probe_T == meta_clients; // @[MSHR.scala 499:46]
  wire  probe_toN = io_sinkc_bits_param == 3'h1 | io_sinkc_bits_param == 3'h2 | io_sinkc_bits_param == 3'h5; // @[Parameters.scala 286:66]
  wire [3:0] _probes_toN_T = probe_toN ? probe_bit : 4'h0; // @[MSHR.scala 510:35]
  wire [3:0] _probes_toN_T_1 = probes_toN | _probes_toN_T; // @[MSHR.scala 510:30]
  wire  _w_rprobeacklast_T = last_probe & io_sinkc_bits_last; // @[MSHR.scala 513:55]
  wire  _set_pprobeack_T = request_offset == 6'h0; // @[MSHR.scala 517:77]
  wire  set_pprobeack = last_probe & (io_sinkc_bits_last | request_offset == 6'h0); // @[MSHR.scala 517:36]
  wire  _GEN_70 = _T_28 & io_sinkc_bits_tag == meta_tag & io_sinkc_bits_data | _GEN_4; // @[MSHR.scala 522:91 MSHR.scala 522:104]
  wire  _GEN_80 = io_sinkd_bits_opcode == 3'h6 | w_releaseack; // @[MSHR.scala 541:53 MSHR.scala 542:20 MSHR.scala 127:33]
  wire  _GEN_82 = io_sinkd_bits_opcode == 3'h4 | io_sinkd_bits_opcode == 3'h5 | w_grantfirst; // @[MSHR.scala 525:81 MSHR.scala 527:20 MSHR.scala 131:33]
  wire  _GEN_95 = io_sinke_valid | w_grantack; // @[MSHR.scala 545:25 MSHR.scala 546:16 MSHR.scala 140:33]
  wire  _new_meta_T = io_allocate_valid & io_allocate_bits_repeat; // @[MSHR.scala 551:40]
  wire  new_meta_dirty = _new_meta_T ? final_meta_writeback_dirty : io_directory_bits_dirty; // @[MSHR.scala 551:21]
  wire [1:0] new_meta_state = _new_meta_T ? final_meta_writeback_state : io_directory_bits_state; // @[MSHR.scala 551:21]
  wire [3:0] new_meta_clients = _new_meta_T ? final_meta_writeback_clients : io_directory_bits_clients; // @[MSHR.scala 551:21]
  wire  new_meta_hit = _new_meta_T ? final_meta_writeback_hit : io_directory_bits_hit; // @[MSHR.scala 551:21]
  wire  new_request_prio_2 = io_allocate_valid ? io_allocate_bits_prio_2 : request_prio_2; // @[MSHR.scala 552:24]
  wire  new_request_control = io_allocate_valid ? io_allocate_bits_control : request_control; // @[MSHR.scala 552:24]
  wire [2:0] new_request_opcode = io_allocate_valid ? io_allocate_bits_opcode : request_opcode; // @[MSHR.scala 552:24]
  wire [2:0] new_request_param = io_allocate_valid ? io_allocate_bits_param : request_param; // @[MSHR.scala 552:24]
  wire [6:0] new_request_source = io_allocate_valid ? io_allocate_bits_source : request_source; // @[MSHR.scala 552:24]
  wire  _new_needT_T_1 = ~new_request_opcode[2]; // @[Parameters.scala 281:5]
  wire  _new_needT_T_3 = new_request_param == 3'h1; // @[Parameters.scala 282:42]
  wire  _new_needT_T_4 = new_request_opcode == 3'h5 & new_request_param == 3'h1; // @[Parameters.scala 282:33]
  wire  _new_needT_T_5 = ~new_request_opcode[2] | _new_needT_T_4; // @[Parameters.scala 281:16]
  wire  _new_needT_T_8 = new_request_opcode == 3'h6 | new_request_opcode == 3'h7; // @[Parameters.scala 283:42]
  wire  _new_needT_T_10 = (new_request_opcode == 3'h6 | new_request_opcode == 3'h7) & new_request_param != 3'h0; // @[Parameters.scala 283:80]
  wire  new_needT = _new_needT_T_5 | _new_needT_T_10; // @[Parameters.scala 282:70]
  wire  new_clientBit_lo_lo = new_request_source == 7'h60; // @[Parameters.scala 46:9]
  wire  new_clientBit_lo_hi = new_request_source == 7'h61; // @[Parameters.scala 46:9]
  wire  new_clientBit_hi_lo = new_request_source == 7'h40; // @[Parameters.scala 46:9]
  wire  new_clientBit_hi_hi = new_request_source == 7'h41; // @[Parameters.scala 46:9]
  wire [3:0] new_clientBit = {new_clientBit_hi_hi,new_clientBit_hi_lo,new_clientBit_lo_hi,new_clientBit_lo_lo}; // @[Cat.scala 30:58]
  wire  _T_693 = io_directory_valid | _new_meta_T; // @[MSHR.scala 587:28]
  wire  _T_696 = ~new_meta_dirty; // @[MSHR.scala 622:38]
  wire  _GEN_115 = new_request_opcode[0] & ~new_meta_dirty ? 1'h0 : 1'h1; // @[MSHR.scala 622:55 MSHR.scala 623:21 MSHR.scala 616:22]
  wire  _T_700 = new_request_param == 3'h0 | new_request_param == 3'h4; // @[Parameters.scala 289:34]
  wire  _T_701 = new_meta_state == 2'h2; // @[MSHR.scala 626:56]
  wire  _T_707 = _new_needT_T_3 | new_request_param == 3'h2 | new_request_param == 3'h5; // @[Parameters.scala 286:66]
  wire [3:0] _T_708 = new_meta_clients & new_clientBit; // @[MSHR.scala 630:59]
  wire  _T_716 = new_meta_clients != 4'h0; // @[MSHR.scala 640:62]
  wire  _GEN_123 = _T_716 ? 1'h0 : 1'h1; // @[MSHR.scala 667:75 MSHR.scala 668:20 MSHR.scala 598:22]
  wire  _GEN_124 = new_meta_hit ? 1'h0 : 1'h1; // @[MSHR.scala 663:27 MSHR.scala 664:19 MSHR.scala 601:22]
  wire  _GEN_125 = new_meta_hit ? _GEN_123 : 1'h1; // @[MSHR.scala 663:27 MSHR.scala 598:22]
  wire  _T_728 = ~new_meta_hit; // @[MSHR.scala 678:13]
  wire  _GEN_127 = ~new_meta_hit & new_meta_state != 2'h0 ? 1'h0 : 1'h1; // @[MSHR.scala 678:58 MSHR.scala 679:19 MSHR.scala 601:22]
  wire  _GEN_128 = ~new_meta_hit & new_meta_state != 2'h0 ? _GEN_123 : 1'h1; // @[MSHR.scala 678:58 MSHR.scala 598:22]
  wire  _GEN_129 = _T_728 | new_meta_state == 2'h1 & new_needT ? 1'h0 : 1'h1; // @[MSHR.scala 689:72 MSHR.scala 690:19 MSHR.scala 604:22]
  wire  _T_738 = new_needT | _T_701; // @[MSHR.scala 699:24]
  wire  _T_739 = new_meta_hit & _T_738; // @[MSHR.scala 698:55]
  wire  _T_740 = new_request_opcode != 3'h5; // @[MSHR.scala 700:32]
  wire  _T_741 = _T_739 & _T_740; // @[MSHR.scala 699:53]
  wire [3:0] _T_742 = ~new_clientBit; // @[MSHR.scala 701:33]
  wire [3:0] _T_743 = new_meta_clients & _T_742; // @[MSHR.scala 701:31]
  wire  _T_744 = _T_743 != 4'h0; // @[MSHR.scala 701:49]
  wire  _T_745 = _T_741 & _T_744; // @[MSHR.scala 700:52]
  wire  _GEN_130 = _T_745 ? 1'h0 : 1'h1; // @[MSHR.scala 701:63 MSHR.scala 702:18 MSHR.scala 603:22]
  wire  _GEN_131 = _T_745 ? 1'h0 : _GEN_129; // @[MSHR.scala 701:63 MSHR.scala 706:21]
  wire  _GEN_132 = _new_needT_T_8 ? 1'h0 : 1'h1; // @[MSHR.scala 709:88 MSHR.scala 710:20 MSHR.scala 615:22]
  wire  _GEN_133 = _new_needT_T_8 ? 1'h0 : _GEN_131; // @[MSHR.scala 709:88 MSHR.scala 711:21]
  wire  _GEN_134 = _new_needT_T_1 & new_meta_hit & _T_696 ? 1'h0 : _GEN_133; // @[MSHR.scala 714:72 MSHR.scala 715:21]
  wire  _GEN_135 = new_request_control ? 1'h0 : 1'h1; // @[MSHR.scala 660:61 MSHR.scala 661:15 MSHR.scala 605:22]
  wire  _GEN_136 = new_request_control ? _GEN_124 : _GEN_127; // @[MSHR.scala 660:61]
  wire  _GEN_137 = new_request_control ? _GEN_125 : _GEN_128; // @[MSHR.scala 660:61]
  wire  _GEN_139 = new_request_control | _GEN_129; // @[MSHR.scala 660:61 MSHR.scala 604:22]
  wire  _GEN_140 = new_request_control | _GEN_134; // @[MSHR.scala 660:61 MSHR.scala 616:22]
  wire  _GEN_141 = new_request_control | _GEN_130; // @[MSHR.scala 660:61 MSHR.scala 603:22]
  wire  _GEN_142 = new_request_control | _GEN_132; // @[MSHR.scala 660:61 MSHR.scala 615:22]
  wire  _GEN_155 = new_request_prio_2 | _GEN_141; // @[MSHR.scala 619:60 MSHR.scala 603:22]
  wire  _GEN_156 = new_request_prio_2 | _GEN_135; // @[MSHR.scala 619:60 MSHR.scala 605:22]
  wire  _GEN_157 = new_request_prio_2 | _GEN_136; // @[MSHR.scala 619:60 MSHR.scala 601:22]
  wire  _GEN_158 = new_request_prio_2 | _GEN_137; // @[MSHR.scala 619:60 MSHR.scala 598:22]
  wire  _GEN_159 = new_request_prio_2 | _GEN_139; // @[MSHR.scala 619:60 MSHR.scala 604:22]
  wire  _GEN_160 = new_request_prio_2 | _GEN_142; // @[MSHR.scala 619:60 MSHR.scala 615:22]
  assign io_status_valid = request_valid; // @[MSHR.scala 166:19]
  assign io_status_bits_set = request_set; // @[MSHR.scala 167:25]
  assign io_status_bits_tag = request_tag; // @[MSHR.scala 168:25]
  assign io_status_bits_way = meta_way; // @[MSHR.scala 169:25]
  assign io_status_bits_blockB = _io_status_bits_blockB_T | (~w_releaseack | ~w_rprobeacklast | ~w_pprobeacklast) & ~
    w_grantfirst; // @[MSHR.scala 170:40]
  assign io_status_bits_nestB = meta_valid & w_releaseack & w_rprobeacklast & w_pprobeacklast &
    _io_status_bits_blockB_T_6; // @[MSHR.scala 171:93]
  assign io_status_bits_blockC = ~meta_valid; // @[MSHR.scala 174:28]
  assign io_status_bits_nestC = meta_valid & (~w_rprobeackfirst | ~w_pprobeackfirst | _io_status_bits_blockB_T_6); // @[MSHR.scala 175:39]
  assign io_schedule_valid = _io_schedule_valid_T_2 | io_schedule_bits_e_valid | io_schedule_bits_x_valid |
    io_schedule_bits_dir_valid; // @[MSHR.scala 201:105]
  assign io_schedule_bits_a_valid = ~s_acquire & acquire_ready; // @[MSHR.scala 192:42]
  assign io_schedule_bits_a_bits_tag = request_tag; // @[MSHR.scala 302:35]
  assign io_schedule_bits_a_bits_set = request_set; // @[MSHR.scala 303:35]
  assign io_schedule_bits_a_bits_param = {{1'd0}, _io_schedule_bits_a_bits_param_T_1}; // @[MSHR.scala 304:41]
  assign io_schedule_bits_a_bits_block = request_size != 3'h6 | _io_schedule_bits_a_bits_block_T_4; // @[MSHR.scala 305:95]
  assign io_schedule_bits_b_valid = ~s_rprobe | ~s_pprobe; // @[MSHR.scala 193:41]
  assign io_schedule_bits_b_bits_param = _io_schedule_bits_b_valid_T ? 3'h2 : _io_schedule_bits_b_bits_param_T_2; // @[MSHR.scala 309:41]
  assign io_schedule_bits_b_bits_tag = _io_schedule_bits_b_valid_T ? meta_tag : request_tag; // @[MSHR.scala 310:41]
  assign io_schedule_bits_b_bits_set = request_set; // @[MSHR.scala 311:35]
  assign io_schedule_bits_b_bits_clients = meta_clients; // @[MSHR.scala 312:51]
  assign io_schedule_bits_c_valid = ~s_release & w_rprobeackfirst; // @[MSHR.scala 194:43]
  assign io_schedule_bits_c_bits_opcode = _io_schedule_bits_c_valid_T ? _io_schedule_bits_c_bits_opcode_T_1 :
    _io_schedule_bits_c_bits_opcode_T_3; // @[MSHR.scala 313:41]
  assign io_schedule_bits_c_bits_param = _io_schedule_bits_c_valid_T ? _io_schedule_bits_c_bits_param_T_2 :
    _io_schedule_bits_c_bits_param_T_20; // @[MSHR.scala 314:41]
  assign io_schedule_bits_c_bits_tag = _io_schedule_bits_c_valid_T ? meta_tag : request_tag; // @[MSHR.scala 321:41]
  assign io_schedule_bits_c_bits_set = request_set; // @[MSHR.scala 322:35]
  assign io_schedule_bits_c_bits_way = meta_way; // @[MSHR.scala 323:35]
  assign io_schedule_bits_c_bits_dirty = _io_schedule_bits_c_valid_T ? meta_dirty : _final_meta_writeback_dirty_T_2; // @[MSHR.scala 324:41]
  assign io_schedule_bits_d_valid = ~s_execute & execute_ready; // @[MSHR.scala 195:42]
  assign io_schedule_bits_d_bits_prio_2 = request_prio_2; // @[MSHR.scala 325:35]
  assign io_schedule_bits_d_bits_opcode = request_opcode; // @[MSHR.scala 325:35]
  assign io_schedule_bits_d_bits_param = ~_req_needT_T_8 ? request_param : _io_schedule_bits_d_bits_param_T_8; // @[MSHR.scala 326:41]
  assign io_schedule_bits_d_bits_size = request_size; // @[MSHR.scala 325:35]
  assign io_schedule_bits_d_bits_source = request_source; // @[MSHR.scala 325:35]
  assign io_schedule_bits_d_bits_offset = request_offset; // @[MSHR.scala 325:35]
  assign io_schedule_bits_d_bits_put = request_put; // @[MSHR.scala 325:35]
  assign io_schedule_bits_d_bits_set = request_set; // @[MSHR.scala 325:35]
  assign io_schedule_bits_d_bits_way = meta_way; // @[MSHR.scala 332:35]
  assign io_schedule_bits_d_bits_bad = bad_grant; // @[MSHR.scala 333:35]
  assign io_schedule_bits_d_bits_user_hit_cache = meta_hit; // @[MSHR.scala 335:47]
  assign io_schedule_bits_e_valid = ~s_grantack & w_grantfirst; // @[MSHR.scala 196:43]
  assign io_schedule_bits_e_bits_sink = sink; // @[MSHR.scala 339:35]
  assign io_schedule_bits_x_valid = ~s_flush & w_releaseack & w_rprobeacklast; // @[MSHR.scala 197:56]
  assign io_schedule_bits_dir_valid = _io_schedule_bits_c_valid_T_1 | _io_schedule_bits_dir_valid_T_3; // @[MSHR.scala 198:66]
  assign io_schedule_bits_dir_bits_set = request_set; // @[MSHR.scala 341:35]
  assign io_schedule_bits_dir_bits_way = meta_way; // @[MSHR.scala 342:35]
  assign io_schedule_bits_dir_bits_data_dirty = _io_schedule_bits_c_valid_T ? 1'h0 : final_meta_writeback_dirty; // @[MSHR.scala 343:41]
  assign io_schedule_bits_dir_bits_data_state = _io_schedule_bits_c_valid_T ? 2'h0 : final_meta_writeback_state; // @[MSHR.scala 343:41]
  assign io_schedule_bits_dir_bits_data_clients = _io_schedule_bits_c_valid_T ? 4'h0 : final_meta_writeback_clients; // @[MSHR.scala 343:41]
  assign io_schedule_bits_dir_bits_data_tag = _io_schedule_bits_c_valid_T ? 18'h0 : final_meta_writeback_tag; // @[MSHR.scala 343:41]
  assign io_schedule_bits_reload = w_rprobeacklast & w_releaseack & w_grantlast & w_pprobeacklast & w_grantack; // @[MSHR.scala 185:83]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      request_valid <= 1'h0;
    end else begin
      request_valid <= io_allocate_valid | _GEN_25;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      request_prio_2 <= 1'h0;
    end else if (io_allocate_valid) begin
      request_prio_2 <= io_allocate_bits_prio_2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      request_control <= 1'h0;
    end else if (io_allocate_valid) begin
      request_control <= io_allocate_bits_control;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      request_opcode <= 3'h0;
    end else if (io_allocate_valid) begin
      request_opcode <= io_allocate_bits_opcode;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      request_param <= 3'h0;
    end else if (io_allocate_valid) begin
      request_param <= io_allocate_bits_param;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      request_size <= 3'h0;
    end else if (io_allocate_valid) begin
      request_size <= io_allocate_bits_size;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      request_source <= 7'h0;
    end else if (io_allocate_valid) begin
      request_source <= io_allocate_bits_source;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      request_tag <= 18'h0;
    end else if (io_allocate_valid) begin
      request_tag <= io_allocate_bits_tag;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      request_offset <= 6'h0;
    end else if (io_allocate_valid) begin
      request_offset <= io_allocate_bits_offset;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      request_put <= 5'h0;
    end else if (io_allocate_valid) begin
      request_put <= io_allocate_bits_put;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      request_set <= 10'h0;
    end else if (io_allocate_valid) begin
      request_set <= io_allocate_bits_set;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      meta_valid <= 1'h0;
    end else begin
      meta_valid <= _T_693 | _GEN_26;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      meta_dirty <= 1'h0;
    end else if (_T_693) begin
      if (_new_meta_T) begin
        if (bad_grant) begin
          meta_dirty <= 1'h0;
        end else if (request_prio_2) begin
          meta_dirty <= meta_dirty | request_opcode[0];
        end else begin
          meta_dirty <= _GEN_32;
        end
      end else begin
        meta_dirty <= io_directory_bits_dirty;
      end
    end else if (io_sinkc_valid) begin
      meta_dirty <= _GEN_70;
    end else if (_T_31 & io_nestedwb_tag == meta_tag) begin
      meta_dirty <= _GEN_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      meta_state <= 2'h0;
    end else if (_T_693) begin
      if (_new_meta_T) begin
        if (bad_grant) begin
          if (meta_hit) begin
            meta_state <= 2'h1;
          end else begin
            meta_state <= 2'h0;
          end
        end else if (request_prio_2) begin
          meta_state <= _final_meta_writeback_state_T_3;
        end else begin
          meta_state <= _GEN_33;
        end
      end else begin
        meta_state <= io_directory_bits_state;
      end
    end else if (_T_31 & io_nestedwb_tag == meta_tag) begin
      if (io_nestedwb_b_toB) begin
        meta_state <= 2'h1;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      meta_clients <= 4'h0;
    end else if (_T_693) begin
      if (_new_meta_T) begin
        if (bad_grant) begin
          if (meta_hit) begin
            meta_clients <= _no_other_clients_T_1;
          end else begin
            meta_clients <= 4'h0;
          end
        end else if (request_prio_2) begin
          meta_clients <= _final_meta_writeback_clients_T_7;
        end else begin
          meta_clients <= _GEN_34;
        end
      end else begin
        meta_clients <= io_directory_bits_clients;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      meta_tag <= 18'h0;
    end else if (_T_693) begin
      if (_new_meta_T) begin
        if (!(request_prio_2)) begin
          if (!(request_control)) begin
            meta_tag <= request_tag;
          end
        end
      end else begin
        meta_tag <= io_directory_bits_tag;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      meta_hit <= 1'h0;
    end else if (_T_693) begin
      if (_new_meta_T) begin
        if (!(bad_grant)) begin
          meta_hit <= _GEN_45;
        end
      end else begin
        meta_hit <= io_directory_bits_hit;
      end
    end else if (_T_31 & io_nestedwb_tag == meta_tag) begin
      if (io_nestedwb_b_toN) begin
        meta_hit <= 1'h0;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      meta_way <= 3'h0;
    end else if (_T_693) begin
      if (!(_new_meta_T)) begin
        meta_way <= io_directory_bits_way;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s_rprobe <= 1'h1;
    end else if (_T_693) begin
      s_rprobe <= _GEN_158;
    end else begin
      s_rprobe <= _GEN_16;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      w_rprobeackfirst <= 1'h1;
    end else if (_T_693) begin
      w_rprobeackfirst <= _GEN_158;
    end else if (io_sinkc_valid) begin
      w_rprobeackfirst <= w_rprobeackfirst | last_probe;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      w_rprobeacklast <= 1'h1;
    end else if (_T_693) begin
      w_rprobeacklast <= _GEN_158;
    end else if (io_sinkc_valid) begin
      w_rprobeacklast <= w_rprobeacklast | last_probe & io_sinkc_bits_last;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s_release <= 1'h1;
    end else if (_T_693) begin
      s_release <= _GEN_157;
    end else if (io_schedule_ready) begin
      s_release <= _GEN_7;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      w_releaseack <= 1'h1;
    end else if (_T_693) begin
      w_releaseack <= _GEN_157;
    end else if (io_sinkd_valid) begin
      if (!(io_sinkd_bits_opcode == 3'h4 | io_sinkd_bits_opcode == 3'h5)) begin
        w_releaseack <= _GEN_80;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s_pprobe <= 1'h1;
    end else if (_T_693) begin
      s_pprobe <= _GEN_155;
    end else begin
      s_pprobe <= _GEN_18;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s_acquire <= 1'h1;
    end else if (_T_693) begin
      s_acquire <= _GEN_159;
    end else if (io_schedule_ready) begin
      s_acquire <= _GEN_8;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s_flush <= 1'h1;
    end else if (_T_693) begin
      s_flush <= _GEN_156;
    end else if (io_schedule_ready) begin
      s_flush <= _GEN_9;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      w_grantfirst <= 1'h1;
    end else if (_T_693) begin
      w_grantfirst <= _GEN_159;
    end else if (io_sinkd_valid) begin
      w_grantfirst <= _GEN_82;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      w_grantlast <= 1'h1;
    end else if (_T_693) begin
      w_grantlast <= _GEN_159;
    end else if (io_sinkd_valid) begin
      if (io_sinkd_bits_opcode == 3'h4 | io_sinkd_bits_opcode == 3'h5) begin
        w_grantlast <= io_sinkd_bits_last;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      w_grant <= 1'h1;
    end else if (_T_693) begin
      w_grant <= _GEN_159;
    end else if (io_sinkd_valid) begin
      if (io_sinkd_bits_opcode == 3'h4 | io_sinkd_bits_opcode == 3'h5) begin
        w_grant <= _set_pprobeack_T | io_sinkd_bits_last;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      w_pprobeackfirst <= 1'h1;
    end else if (_T_693) begin
      w_pprobeackfirst <= _GEN_155;
    end else if (io_sinkc_valid) begin
      w_pprobeackfirst <= w_pprobeackfirst | last_probe;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      w_pprobeacklast <= 1'h1;
    end else if (_T_693) begin
      w_pprobeacklast <= _GEN_155;
    end else if (io_sinkc_valid) begin
      w_pprobeacklast <= w_pprobeacklast | _w_rprobeacklast_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      w_pprobeack <= 1'h1;
    end else if (_T_693) begin
      w_pprobeack <= _GEN_155;
    end else if (io_sinkc_valid) begin
      w_pprobeack <= w_pprobeack | set_pprobeack;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s_grantack <= 1'h1;
    end else if (_T_693) begin
      s_grantack <= _GEN_159;
    end else if (io_schedule_ready) begin
      s_grantack <= _GEN_11;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s_execute <= 1'h1;
    end else if (_T_693) begin
      if (new_request_prio_2) begin
        s_execute <= 1'h0;
      end else if (io_allocate_valid) begin
        s_execute <= io_allocate_bits_control;
      end else begin
        s_execute <= request_control;
      end
    end else if (io_schedule_ready) begin
      s_execute <= _GEN_12;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      w_grantack <= 1'h1;
    end else if (_T_693) begin
      w_grantack <= _GEN_160;
    end else begin
      w_grantack <= _GEN_95;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s_writeback <= 1'h1;
    end else if (_T_693) begin
      if (new_request_prio_2) begin
        if (_T_707 & _T_708 != 4'h0) begin
          s_writeback <= 1'h0;
        end else if (_T_700 & new_meta_state == 2'h2) begin
          s_writeback <= 1'h0;
        end else begin
          s_writeback <= _GEN_115;
        end
      end else begin
        s_writeback <= _GEN_140;
      end
    end else if (io_schedule_ready) begin
      s_writeback <= _GEN_13;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      sink <= 3'h0;
    end else if (io_sinkd_valid) begin
      if (io_sinkd_bits_opcode == 3'h4 | io_sinkd_bits_opcode == 3'h5) begin
        sink <= io_sinkd_bits_sink;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      gotT <= 1'h0;
    end else if (_T_693) begin
      gotT <= 1'h0;
    end else if (io_sinkd_valid) begin
      if (io_sinkd_bits_opcode == 3'h4 | io_sinkd_bits_opcode == 3'h5) begin
        gotT <= io_sinkd_bits_param == 3'h0;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bad_grant <= 1'h0;
    end else if (_T_693) begin
      bad_grant <= 1'h0;
    end else if (io_sinkd_valid) begin
      if (io_sinkd_bits_opcode == 3'h4 | io_sinkd_bits_opcode == 3'h5) begin
        bad_grant <= io_sinkd_bits_denied;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      probes_done <= 4'h0;
    end else if (_T_693) begin
      probes_done <= 4'h0;
    end else if (io_sinkc_valid) begin
      probes_done <= _last_probe_T;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      probes_toN <= 4'h0;
    end else if (_T_693) begin
      probes_toN <= 4'h0;
    end else if (io_sinkc_valid) begin
      probes_toN <= _probes_toN_T_1;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  request_valid = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  request_prio_2 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  request_control = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  request_opcode = _RAND_3[2:0];
  _RAND_4 = {1{`RANDOM}};
  request_param = _RAND_4[2:0];
  _RAND_5 = {1{`RANDOM}};
  request_size = _RAND_5[2:0];
  _RAND_6 = {1{`RANDOM}};
  request_source = _RAND_6[6:0];
  _RAND_7 = {1{`RANDOM}};
  request_tag = _RAND_7[17:0];
  _RAND_8 = {1{`RANDOM}};
  request_offset = _RAND_8[5:0];
  _RAND_9 = {1{`RANDOM}};
  request_put = _RAND_9[4:0];
  _RAND_10 = {1{`RANDOM}};
  request_set = _RAND_10[9:0];
  _RAND_11 = {1{`RANDOM}};
  meta_valid = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  meta_dirty = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  meta_state = _RAND_13[1:0];
  _RAND_14 = {1{`RANDOM}};
  meta_clients = _RAND_14[3:0];
  _RAND_15 = {1{`RANDOM}};
  meta_tag = _RAND_15[17:0];
  _RAND_16 = {1{`RANDOM}};
  meta_hit = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  meta_way = _RAND_17[2:0];
  _RAND_18 = {1{`RANDOM}};
  s_rprobe = _RAND_18[0:0];
  _RAND_19 = {1{`RANDOM}};
  w_rprobeackfirst = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  w_rprobeacklast = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  s_release = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  w_releaseack = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  s_pprobe = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  s_acquire = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  s_flush = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  w_grantfirst = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  w_grantlast = _RAND_27[0:0];
  _RAND_28 = {1{`RANDOM}};
  w_grant = _RAND_28[0:0];
  _RAND_29 = {1{`RANDOM}};
  w_pprobeackfirst = _RAND_29[0:0];
  _RAND_30 = {1{`RANDOM}};
  w_pprobeacklast = _RAND_30[0:0];
  _RAND_31 = {1{`RANDOM}};
  w_pprobeack = _RAND_31[0:0];
  _RAND_32 = {1{`RANDOM}};
  s_grantack = _RAND_32[0:0];
  _RAND_33 = {1{`RANDOM}};
  s_execute = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  w_grantack = _RAND_34[0:0];
  _RAND_35 = {1{`RANDOM}};
  s_writeback = _RAND_35[0:0];
  _RAND_36 = {1{`RANDOM}};
  sink = _RAND_36[2:0];
  _RAND_37 = {1{`RANDOM}};
  gotT = _RAND_37[0:0];
  _RAND_38 = {1{`RANDOM}};
  bad_grant = _RAND_38[0:0];
  _RAND_39 = {1{`RANDOM}};
  probes_done = _RAND_39[3:0];
  _RAND_40 = {1{`RANDOM}};
  probes_toN = _RAND_40[3:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    request_valid = 1'h0;
  end
  if (rf_reset) begin
    request_prio_2 = 1'h0;
  end
  if (rf_reset) begin
    request_control = 1'h0;
  end
  if (rf_reset) begin
    request_opcode = 3'h0;
  end
  if (rf_reset) begin
    request_param = 3'h0;
  end
  if (rf_reset) begin
    request_size = 3'h0;
  end
  if (rf_reset) begin
    request_source = 7'h0;
  end
  if (rf_reset) begin
    request_tag = 18'h0;
  end
  if (rf_reset) begin
    request_offset = 6'h0;
  end
  if (rf_reset) begin
    request_put = 5'h0;
  end
  if (rf_reset) begin
    request_set = 10'h0;
  end
  if (reset) begin
    meta_valid = 1'h0;
  end
  if (rf_reset) begin
    meta_dirty = 1'h0;
  end
  if (rf_reset) begin
    meta_state = 2'h0;
  end
  if (rf_reset) begin
    meta_clients = 4'h0;
  end
  if (rf_reset) begin
    meta_tag = 18'h0;
  end
  if (rf_reset) begin
    meta_hit = 1'h0;
  end
  if (rf_reset) begin
    meta_way = 3'h0;
  end
  if (reset) begin
    s_rprobe = 1'h1;
  end
  if (reset) begin
    w_rprobeackfirst = 1'h1;
  end
  if (reset) begin
    w_rprobeacklast = 1'h1;
  end
  if (reset) begin
    s_release = 1'h1;
  end
  if (reset) begin
    w_releaseack = 1'h1;
  end
  if (reset) begin
    s_pprobe = 1'h1;
  end
  if (reset) begin
    s_acquire = 1'h1;
  end
  if (reset) begin
    s_flush = 1'h1;
  end
  if (reset) begin
    w_grantfirst = 1'h1;
  end
  if (reset) begin
    w_grantlast = 1'h1;
  end
  if (reset) begin
    w_grant = 1'h1;
  end
  if (reset) begin
    w_pprobeackfirst = 1'h1;
  end
  if (reset) begin
    w_pprobeacklast = 1'h1;
  end
  if (reset) begin
    w_pprobeack = 1'h1;
  end
  if (reset) begin
    s_grantack = 1'h1;
  end
  if (reset) begin
    s_execute = 1'h1;
  end
  if (reset) begin
    w_grantack = 1'h1;
  end
  if (reset) begin
    s_writeback = 1'h1;
  end
  if (rf_reset) begin
    sink = 3'h0;
  end
  if (rf_reset) begin
    gotT = 1'h0;
  end
  if (rf_reset) begin
    bad_grant = 1'h0;
  end
  if (rf_reset) begin
    probes_done = 4'h0;
  end
  if (rf_reset) begin
    probes_toN = 4'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
