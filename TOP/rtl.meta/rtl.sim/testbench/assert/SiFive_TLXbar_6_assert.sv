//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLXbar_6_assert(
  input         clock,
  input         reset,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [2:0]  auto_in_a_bits_param,
  input  [3:0]  auto_in_a_bits_size,
  input  [6:0]  auto_in_a_bits_source,
  input  [29:0] auto_in_a_bits_address,
  input  [7:0]  auto_in_a_bits_mask,
  input         auto_in_a_bits_corrupt,
  input         auto_in_d_ready,
  input         _T_786,
  input         _T_850,
  input         _T_851,
  input         _T_852,
  input         _T_853,
  input         _T_854,
  input         _T_855,
  input         _T_856,
  input         _T_857,
  input         _T_858,
  input         _T_859,
  input  [82:0] _T_1091,
  input         _T_599,
  input         _T_607,
  input         _T_921,
  input         _T_1000
);
  wire  TLMonitor_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [6:0] TLMonitor_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [29:0] TLMonitor_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_d_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [6:0] TLMonitor_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  _T_862; // @[Arbiter.scala 74:52]
  wire  _T_863; // @[Arbiter.scala 74:52]
  wire  _T_864; // @[Arbiter.scala 74:52]
  wire  _T_865; // @[Arbiter.scala 74:52]
  wire  _T_866; // @[Arbiter.scala 74:52]
  wire  _T_867; // @[Arbiter.scala 74:52]
  wire  _T_868; // @[Arbiter.scala 74:52]
  wire  _T_869; // @[Arbiter.scala 74:52]
  wire  _T_870; // @[Arbiter.scala 74:52]
  wire  _T_872; // @[Arbiter.scala 75:62]
  wire  _T_875; // @[Arbiter.scala 75:62]
  wire  _T_876; // @[Arbiter.scala 75:59]
  wire  _T_877; // @[Arbiter.scala 75:56]
  wire  _T_878; // @[Arbiter.scala 75:62]
  wire  _T_879; // @[Arbiter.scala 75:59]
  wire  _T_880; // @[Arbiter.scala 75:56]
  wire  _T_881; // @[Arbiter.scala 75:62]
  wire  _T_882; // @[Arbiter.scala 75:59]
  wire  _T_883; // @[Arbiter.scala 75:56]
  wire  _T_884; // @[Arbiter.scala 75:62]
  wire  _T_885; // @[Arbiter.scala 75:59]
  wire  _T_886; // @[Arbiter.scala 75:56]
  wire  _T_887; // @[Arbiter.scala 75:62]
  wire  _T_888; // @[Arbiter.scala 75:59]
  wire  _T_889; // @[Arbiter.scala 75:56]
  wire  _T_890; // @[Arbiter.scala 75:62]
  wire  _T_891; // @[Arbiter.scala 75:59]
  wire  _T_892; // @[Arbiter.scala 75:56]
  wire  _T_893; // @[Arbiter.scala 75:62]
  wire  _T_894; // @[Arbiter.scala 75:59]
  wire  _T_895; // @[Arbiter.scala 75:56]
  wire  _T_896; // @[Arbiter.scala 75:62]
  wire  _T_897; // @[Arbiter.scala 75:59]
  wire  _T_898; // @[Arbiter.scala 75:56]
  wire  _T_899; // @[Arbiter.scala 75:62]
  wire  _T_900; // @[Arbiter.scala 75:59]
  wire  _T_902; // @[Arbiter.scala 75:77]
  wire  _T_903; // @[Arbiter.scala 75:77]
  wire  _T_904; // @[Arbiter.scala 75:77]
  wire  _T_905; // @[Arbiter.scala 75:77]
  wire  _T_906; // @[Arbiter.scala 75:77]
  wire  _T_907; // @[Arbiter.scala 75:77]
  wire  _T_908; // @[Arbiter.scala 75:77]
  wire  _T_909; // @[Arbiter.scala 75:77]
  wire  _T_911; // @[Arbiter.scala 75:13]
  wire  _T_912; // @[Arbiter.scala 75:13]
  wire  _T_922; // @[Arbiter.scala 77:15]
  wire  _T_932; // @[Arbiter.scala 77:36]
  wire  _T_934; // @[Arbiter.scala 77:14]
  wire  _T_935; // @[Arbiter.scala 77:14]
  SiFive_TLMonitor_39_assert TLMonitor ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_clock),
    .reset(TLMonitor_reset),
    .io_in_a_ready(TLMonitor_io_in_a_ready),
    .io_in_a_valid(TLMonitor_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_io_in_a_bits_corrupt),
    .io_in_d_ready(TLMonitor_io_in_d_ready),
    .io_in_d_valid(TLMonitor_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_io_in_d_bits_opcode),
    .io_in_d_bits_param(TLMonitor_io_in_d_bits_param),
    .io_in_d_bits_size(TLMonitor_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_io_in_d_bits_source),
    .io_in_d_bits_sink(TLMonitor_io_in_d_bits_sink),
    .io_in_d_bits_denied(TLMonitor_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_io_in_d_bits_corrupt)
  );
  assign _T_862 = _T_850 | _T_851; // @[Arbiter.scala 74:52]
  assign _T_863 = _T_862 | _T_852; // @[Arbiter.scala 74:52]
  assign _T_864 = _T_863 | _T_853; // @[Arbiter.scala 74:52]
  assign _T_865 = _T_864 | _T_854; // @[Arbiter.scala 74:52]
  assign _T_866 = _T_865 | _T_855; // @[Arbiter.scala 74:52]
  assign _T_867 = _T_866 | _T_856; // @[Arbiter.scala 74:52]
  assign _T_868 = _T_867 | _T_857; // @[Arbiter.scala 74:52]
  assign _T_869 = _T_868 | _T_858; // @[Arbiter.scala 74:52]
  assign _T_870 = _T_869 | _T_859; // @[Arbiter.scala 74:52]
  assign _T_872 = ~_T_850; // @[Arbiter.scala 75:62]
  assign _T_875 = ~_T_851; // @[Arbiter.scala 75:62]
  assign _T_876 = _T_872 | _T_875; // @[Arbiter.scala 75:59]
  assign _T_877 = ~_T_862; // @[Arbiter.scala 75:56]
  assign _T_878 = ~_T_852; // @[Arbiter.scala 75:62]
  assign _T_879 = _T_877 | _T_878; // @[Arbiter.scala 75:59]
  assign _T_880 = ~_T_863; // @[Arbiter.scala 75:56]
  assign _T_881 = ~_T_853; // @[Arbiter.scala 75:62]
  assign _T_882 = _T_880 | _T_881; // @[Arbiter.scala 75:59]
  assign _T_883 = ~_T_864; // @[Arbiter.scala 75:56]
  assign _T_884 = ~_T_854; // @[Arbiter.scala 75:62]
  assign _T_885 = _T_883 | _T_884; // @[Arbiter.scala 75:59]
  assign _T_886 = ~_T_865; // @[Arbiter.scala 75:56]
  assign _T_887 = ~_T_855; // @[Arbiter.scala 75:62]
  assign _T_888 = _T_886 | _T_887; // @[Arbiter.scala 75:59]
  assign _T_889 = ~_T_866; // @[Arbiter.scala 75:56]
  assign _T_890 = ~_T_856; // @[Arbiter.scala 75:62]
  assign _T_891 = _T_889 | _T_890; // @[Arbiter.scala 75:59]
  assign _T_892 = ~_T_867; // @[Arbiter.scala 75:56]
  assign _T_893 = ~_T_857; // @[Arbiter.scala 75:62]
  assign _T_894 = _T_892 | _T_893; // @[Arbiter.scala 75:59]
  assign _T_895 = ~_T_868; // @[Arbiter.scala 75:56]
  assign _T_896 = ~_T_858; // @[Arbiter.scala 75:62]
  assign _T_897 = _T_895 | _T_896; // @[Arbiter.scala 75:59]
  assign _T_898 = ~_T_869; // @[Arbiter.scala 75:56]
  assign _T_899 = ~_T_859; // @[Arbiter.scala 75:62]
  assign _T_900 = _T_898 | _T_899; // @[Arbiter.scala 75:59]
  assign _T_902 = _T_876 & _T_879; // @[Arbiter.scala 75:77]
  assign _T_903 = _T_902 & _T_882; // @[Arbiter.scala 75:77]
  assign _T_904 = _T_903 & _T_885; // @[Arbiter.scala 75:77]
  assign _T_905 = _T_904 & _T_888; // @[Arbiter.scala 75:77]
  assign _T_906 = _T_905 & _T_891; // @[Arbiter.scala 75:77]
  assign _T_907 = _T_906 & _T_894; // @[Arbiter.scala 75:77]
  assign _T_908 = _T_907 & _T_897; // @[Arbiter.scala 75:77]
  assign _T_909 = _T_908 & _T_900; // @[Arbiter.scala 75:77]
  assign _T_911 = _T_909 | reset; // @[Arbiter.scala 75:13]
  assign _T_912 = ~_T_911; // @[Arbiter.scala 75:13]
  assign _T_922 = ~_T_921; // @[Arbiter.scala 77:15]
  assign _T_932 = _T_922 | _T_870; // @[Arbiter.scala 77:36]
  assign _T_934 = _T_932 | reset; // @[Arbiter.scala 77:14]
  assign _T_935 = ~_T_934; // @[Arbiter.scala 77:14]
  assign TLMonitor_clock = clock;
  assign TLMonitor_reset = reset;
  assign TLMonitor_io_in_a_ready = _T_607 | _T_599; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_valid = auto_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_size = auto_in_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_source = auto_in_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_ready = auto_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_valid = _T_786 ? _T_921 : _T_1000; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_opcode = _T_1091[82:80]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_param = _T_1091[79:78]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_size = _T_1091[77:74]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_source = _T_1091[73:67]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_sink = _T_1091[66]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_denied = _T_1091[65]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_corrupt = _T_1091[0]; // @[Nodes.scala 26:19]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_912) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_912) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_935) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_935) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
