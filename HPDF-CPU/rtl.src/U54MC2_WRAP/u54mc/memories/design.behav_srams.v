//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88

module RHEA__cc_dir_ext(
  input RW0_clk,
  input [9:0] RW0_addr,
  input RW0_en,
  input RW0_wmode,
  input [7:0] RW0_wmask,
  input [199:0] RW0_wdata,
  output [199:0] RW0_rdata
);
    SEMIFIVE_SRAM_SP_W #(.UID("RHEA__cc_dir_ext"),.ABIT(10), .WORD(1024), .DBIT(200), .GBIT(25)) MEM_0 (
        .clock          (RW0_clk),
        .enable         (RW0_en),
        .address        (RW0_addr),
        .readenable     (~RW0_wmode),
        .readdata       (RW0_rdata),
        .writeenable    (RW0_wmode),
        .writedata      (RW0_wdata),
        .writestrobe    (RW0_wmask));
endmodule

module RHEA__cc_banks_0_ext(
  input RW0_clk,
  input [11:0] RW0_addr,
  input RW0_en,
  input RW0_wmode,
  input [63:0] RW0_wdata,
  output [63:0] RW0_rdata
);
    SEMIFIVE_SRAM_SP #(.UID("RHEAccbanks0ext"),.ABIT(12), .WORD(4096), .DBIT(64)) MEM_0 (
        .clock          (RW0_clk),
        .enable         (RW0_en),
        .address        (RW0_addr),
        .readenable     (~RW0_wmode),
        .readdata       (RW0_rdata),
        .writeenable    (RW0_wmode),
        .writedata      (RW0_wdata));
endmodule

module RHEA__data_arrays_0_ext(
  input RW0_clk,
  input [8:0] RW0_addr,
  input RW0_en,
  input RW0_wmode,
  input [31:0] RW0_wmask,
  input [255:0] RW0_wdata,
  output [255:0] RW0_rdata
);
    SEMIFIVE_SRAM_SP_W #(.UID("RHEAdataarrays0ext"),.ABIT(9), .WORD(512), .DBIT(256), .GBIT(8)) MEM_0 (
        .clock          (RW0_clk),
        .enable         (RW0_en),
        .address        (RW0_addr),
        .readenable     (~RW0_wmode),
        .readdata       (RW0_rdata),
        .writeenable    (RW0_wmode),
        .writedata      (RW0_wdata),
        .writestrobe    (RW0_wmask));
endmodule

module RHEA__tag_array_ext(
  input RW0_clk,
  input [6:0] RW0_addr,
  input RW0_en,
  input RW0_wmode,
  input [3:0] RW0_wmask,
  input [107:0] RW0_wdata,
  output [107:0] RW0_rdata
);
    SEMIFIVE_SRAM_SP_W #(.UID("RHEA__tag_array_ext"),.ABIT(7), .WORD(128), .DBIT(108), .GBIT(27)) MEM_0 (
        .clock          (RW0_clk),
        .enable         (RW0_en),
        .address        (RW0_addr),
        .readenable     (~RW0_wmode),
        .readdata       (RW0_rdata),
        .writeenable    (RW0_wmode),
        .writedata      (RW0_wdata),
        .writestrobe    (RW0_wmask));
endmodule

module RHEA__tag_array_0_ext(
  input RW0_clk,
  input [6:0] RW0_addr,
  input RW0_en,
  input RW0_wmode,
  input [3:0] RW0_wmask,
  input [103:0] RW0_wdata,
  output [103:0] RW0_rdata
  );
    SEMIFIVE_SRAM_SP_W #(.UID("RHEA__tag_array_0_ext"),.ABIT(7), .WORD(128), .DBIT(104), .GBIT(26)) MEM_0 (
        .clock          (RW0_clk),
        .enable         (RW0_en),
        .address        (RW0_addr),
        .readenable     (~RW0_wmode),
        .readdata       (RW0_rdata),
        .writeenable    (RW0_wmode),
        .writedata      (RW0_wdata),
        .writestrobe    (RW0_wmask));
endmodule

module RHEA__data_arrays_0_0_ext(
  input RW0_clk,
  input [8:0] RW0_addr,
  input RW0_en,
  input RW0_wmode,
  input [3:0] RW0_wmask,
  input [127:0] RW0_wdata,
  output [127:0] RW0_rdata
);
    SEMIFIVE_SRAM_SP_W #(.UID("RHEA__data_arays_0_0_ext"),.ABIT(9), .WORD(512), .DBIT(128), .GBIT(32)) MEM_0 (
        .clock          (RW0_clk),
        .enable         (RW0_en),
        .address        (RW0_addr),
        .readenable     (~RW0_wmode),
        .readdata       (RW0_rdata),
        .writeenable    (RW0_wmode),
        .writedata      (RW0_wdata),
        .writestrobe    (RW0_wmask));
endmodule

module RHEA__l2_tlb_ram_ext(
  input RW0_clk,
  input [6:0] RW0_addr,
  input RW0_en,
  input RW0_wmode,
  input [51:0] RW0_wdata,
  output [51:0] RW0_rdata
);
    SEMIFIVE_SRAM_SP #(.UID("RHEAl2tlbramext"),.ABIT(7), .WORD(128), .DBIT(52)) MEM_0 (
        .clock          (RW0_clk),
        .enable         (RW0_en),
        .address        (RW0_addr),
        .readenable     (~RW0_wmode),
        .readdata       (RW0_rdata),
        .writeenable    (RW0_wmode),
        .writedata      (RW0_wdata));
endmodule

module RHEA__TraceSRAM_0_ext(
  input RW0_clk,
  input [5:0] RW0_addr,
  input RW0_en,
  input RW0_wmode,
  input [3:0] RW0_wmask,
  input [31:0] RW0_wdata,
  output [31:0] RW0_rdata
);
    SEMIFIVE_SRAM_SP_W #(.UID("RHEA__TraceSRAM_0_ext"),.ABIT(6), .WORD(64), .DBIT(32), .GBIT(8)) MEM_0 (
        .clock          (RW0_clk),
        .enable         (RW0_en),
        .address        (RW0_addr),
        .readenable     (~RW0_wmode),
        .readdata       (RW0_rdata),
        .writeenable    (RW0_wmode),
        .writedata      (RW0_wdata),
        .writestrobe    (RW0_wmask));
endmodule

module testharness_ext(
  input W0_clk,
  input [25:0] W0_addr,
  input W0_en,
  input [63:0] W0_data,
  input [7:0] W0_mask,
  input R0_clk,
  input [25:0] R0_addr,
  input R0_en,
  output [63:0] R0_data
);

  reg reg_R0_ren;
  reg [25:0] reg_R0_addr;
  reg [63:0] ram [67108863:0];
  `ifdef RANDOMIZE_MEM_INIT
    integer initvar;
    initial begin
      #`RANDOMIZE_DELAY begin end
      for (initvar = 0; initvar < 67108864; initvar = initvar+1)
        ram[initvar] = {2 {$random}};
      reg_R0_addr = {1 {$random}};
    end
  `endif
  integer i;
  always @(posedge R0_clk)
    reg_R0_ren <= R0_en;
  always @(posedge R0_clk)
    if (R0_en) reg_R0_addr <= R0_addr;
  always @(posedge W0_clk)
    if (W0_en) begin
      if (W0_mask[0]) ram[W0_addr][7:0] <= W0_data[7:0];
      if (W0_mask[1]) ram[W0_addr][15:8] <= W0_data[15:8];
      if (W0_mask[2]) ram[W0_addr][23:16] <= W0_data[23:16];
      if (W0_mask[3]) ram[W0_addr][31:24] <= W0_data[31:24];
      if (W0_mask[4]) ram[W0_addr][39:32] <= W0_data[39:32];
      if (W0_mask[5]) ram[W0_addr][47:40] <= W0_data[47:40];
      if (W0_mask[6]) ram[W0_addr][55:48] <= W0_data[55:48];
      if (W0_mask[7]) ram[W0_addr][63:56] <= W0_data[63:56];
    end
  `ifdef RANDOMIZE_GARBAGE_ASSIGN
  reg [63:0] R0_random;
  `ifdef RANDOMIZE_MEM_INIT
    initial begin
      #`RANDOMIZE_DELAY begin end
      R0_random = {$random, $random};
      reg_R0_ren = R0_random[0];
    end
  `endif
  always @(posedge R0_clk) R0_random <= {$random, $random};
  assign R0_data = reg_R0_ren ? ram[reg_R0_addr] : R0_random[63:0];
  `else
  assign R0_data = ram[reg_R0_addr];
  `endif

endmodule
module testharness_0_ext(
  input W0_clk,
  input [24:0] W0_addr,
  input W0_en,
  input [127:0] W0_data,
  input [15:0] W0_mask,
  input R0_clk,
  input [24:0] R0_addr,
  input R0_en,
  output [127:0] R0_data
);

  reg reg_R0_ren;
  reg [24:0] reg_R0_addr;
  reg [127:0] ram [33554431:0];
  `ifdef RANDOMIZE_MEM_INIT
    integer initvar;
    initial begin
      #`RANDOMIZE_DELAY begin end
      for (initvar = 0; initvar < 33554432; initvar = initvar+1)
        ram[initvar] = {4 {$random}};
      reg_R0_addr = {1 {$random}};
    end
  `endif
  integer i;
  always @(posedge R0_clk)
    reg_R0_ren <= R0_en;
  always @(posedge R0_clk)
    if (R0_en) reg_R0_addr <= R0_addr;
  always @(posedge W0_clk)
    if (W0_en) begin
      if (W0_mask[0]) ram[W0_addr][7:0] <= W0_data[7:0];
      if (W0_mask[1]) ram[W0_addr][15:8] <= W0_data[15:8];
      if (W0_mask[2]) ram[W0_addr][23:16] <= W0_data[23:16];
      if (W0_mask[3]) ram[W0_addr][31:24] <= W0_data[31:24];
      if (W0_mask[4]) ram[W0_addr][39:32] <= W0_data[39:32];
      if (W0_mask[5]) ram[W0_addr][47:40] <= W0_data[47:40];
      if (W0_mask[6]) ram[W0_addr][55:48] <= W0_data[55:48];
      if (W0_mask[7]) ram[W0_addr][63:56] <= W0_data[63:56];
      if (W0_mask[8]) ram[W0_addr][71:64] <= W0_data[71:64];
      if (W0_mask[9]) ram[W0_addr][79:72] <= W0_data[79:72];
      if (W0_mask[10]) ram[W0_addr][87:80] <= W0_data[87:80];
      if (W0_mask[11]) ram[W0_addr][95:88] <= W0_data[95:88];
      if (W0_mask[12]) ram[W0_addr][103:96] <= W0_data[103:96];
      if (W0_mask[13]) ram[W0_addr][111:104] <= W0_data[111:104];
      if (W0_mask[14]) ram[W0_addr][119:112] <= W0_data[119:112];
      if (W0_mask[15]) ram[W0_addr][127:120] <= W0_data[127:120];
    end
  `ifdef RANDOMIZE_GARBAGE_ASSIGN
  reg [127:0] R0_random;
  `ifdef RANDOMIZE_MEM_INIT
    initial begin
      #`RANDOMIZE_DELAY begin end
      R0_random = {$random, $random, $random, $random};
      reg_R0_ren = R0_random[0];
    end
  `endif
  always @(posedge R0_clk) R0_random <= {$random, $random, $random, $random};
  assign R0_data = reg_R0_ren ? ram[reg_R0_addr] : R0_random[127:0];
  `else
  assign R0_data = ram[reg_R0_addr];
  `endif

endmodule
module testharness_1_ext(
  input W0_clk,
  input [24:0] W0_addr,
  input W0_en,
  input [127:0] W0_data,
  input [15:0] W0_mask,
  input R0_clk,
  input [24:0] R0_addr,
  input R0_en,
  output [127:0] R0_data
);

  reg reg_R0_ren;
  reg [24:0] reg_R0_addr;
  reg [127:0] ram [33554431:0];
  `ifdef RANDOMIZE_MEM_INIT
    integer initvar;
    initial begin
      #`RANDOMIZE_DELAY begin end
      for (initvar = 0; initvar < 33554432; initvar = initvar+1)
        ram[initvar] = {4 {$random}};
      reg_R0_addr = {1 {$random}};
    end
  `endif
  integer i;
  always @(posedge R0_clk)
    reg_R0_ren <= R0_en;
  always @(posedge R0_clk)
    if (R0_en) reg_R0_addr <= R0_addr;
  always @(posedge W0_clk)
    if (W0_en) begin
      if (W0_mask[0]) ram[W0_addr][7:0] <= W0_data[7:0];
      if (W0_mask[1]) ram[W0_addr][15:8] <= W0_data[15:8];
      if (W0_mask[2]) ram[W0_addr][23:16] <= W0_data[23:16];
      if (W0_mask[3]) ram[W0_addr][31:24] <= W0_data[31:24];
      if (W0_mask[4]) ram[W0_addr][39:32] <= W0_data[39:32];
      if (W0_mask[5]) ram[W0_addr][47:40] <= W0_data[47:40];
      if (W0_mask[6]) ram[W0_addr][55:48] <= W0_data[55:48];
      if (W0_mask[7]) ram[W0_addr][63:56] <= W0_data[63:56];
      if (W0_mask[8]) ram[W0_addr][71:64] <= W0_data[71:64];
      if (W0_mask[9]) ram[W0_addr][79:72] <= W0_data[79:72];
      if (W0_mask[10]) ram[W0_addr][87:80] <= W0_data[87:80];
      if (W0_mask[11]) ram[W0_addr][95:88] <= W0_data[95:88];
      if (W0_mask[12]) ram[W0_addr][103:96] <= W0_data[103:96];
      if (W0_mask[13]) ram[W0_addr][111:104] <= W0_data[111:104];
      if (W0_mask[14]) ram[W0_addr][119:112] <= W0_data[119:112];
      if (W0_mask[15]) ram[W0_addr][127:120] <= W0_data[127:120];
    end
  `ifdef RANDOMIZE_GARBAGE_ASSIGN
  reg [127:0] R0_random;
  `ifdef RANDOMIZE_MEM_INIT
    initial begin
      #`RANDOMIZE_DELAY begin end
      R0_random = {$random, $random, $random, $random};
      reg_R0_ren = R0_random[0];
    end
  `endif
  always @(posedge R0_clk) R0_random <= {$random, $random, $random, $random};
  assign R0_data = reg_R0_ren ? ram[reg_R0_addr] : R0_random[127:0];
  `else
  assign R0_data = ram[reg_R0_addr];
  `endif

endmodule
