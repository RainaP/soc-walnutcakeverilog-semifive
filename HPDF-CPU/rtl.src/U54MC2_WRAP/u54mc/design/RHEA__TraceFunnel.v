//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TraceFunnel(
  input         clock,
  input         reset,
  input         auto_traceSink_sba_out_a_ready,
  output        auto_traceSink_sba_out_a_valid,
  output [1:0]  auto_traceSink_sba_out_a_bits_source,
  output [36:0] auto_traceSink_sba_out_a_bits_address,
  output [31:0] auto_traceSink_sba_out_a_bits_data,
  input         auto_traceSink_sba_out_d_valid,
  input  [1:0]  auto_traceSink_sba_out_d_bits_source,
  input         auto_traceSink_sba_out_d_bits_denied,
  output        auto_sink_in_1_a_ready,
  input         auto_sink_in_1_a_valid,
  input  [31:0] auto_sink_in_1_a_bits_address,
  input  [31:0] auto_sink_in_1_a_bits_data,
  output        auto_sink_in_1_d_valid,
  output        auto_sink_in_1_d_bits_denied,
  output        auto_sink_in_0_a_ready,
  input         auto_sink_in_0_a_valid,
  input  [31:0] auto_sink_in_0_a_bits_address,
  input  [31:0] auto_sink_in_0_a_bits_data,
  output        auto_sink_in_0_d_valid,
  output        auto_sink_in_0_d_bits_denied,
  output        auto_reg_in_a_ready,
  input         auto_reg_in_a_valid,
  input  [2:0]  auto_reg_in_a_bits_opcode,
  input  [2:0]  auto_reg_in_a_bits_param,
  input  [1:0]  auto_reg_in_a_bits_size,
  input  [10:0] auto_reg_in_a_bits_source,
  input  [28:0] auto_reg_in_a_bits_address,
  input  [7:0]  auto_reg_in_a_bits_mask,
  input  [63:0] auto_reg_in_a_bits_data,
  input         auto_reg_in_a_bits_corrupt,
  input         auto_reg_in_d_ready,
  output        auto_reg_in_d_valid,
  output [2:0]  auto_reg_in_d_bits_opcode,
  output [1:0]  auto_reg_in_d_bits_size,
  output [10:0] auto_reg_in_d_bits_source,
  output [63:0] auto_reg_in_d_bits_data,
  output        io_pib_tref,
  output [3:0]  io_pib_tdata
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
`endif // RANDOMIZE_REG_INIT
  wire  rf_reset;
  wire  traceSink_clock; // @[TraceFunnel.scala 56:34]
  wire  traceSink_reset; // @[TraceFunnel.scala 56:34]
  wire  traceSink_auto_sba_out_a_ready; // @[TraceFunnel.scala 56:34]
  wire  traceSink_auto_sba_out_a_valid; // @[TraceFunnel.scala 56:34]
  wire [1:0] traceSink_auto_sba_out_a_bits_source; // @[TraceFunnel.scala 56:34]
  wire [36:0] traceSink_auto_sba_out_a_bits_address; // @[TraceFunnel.scala 56:34]
  wire [31:0] traceSink_auto_sba_out_a_bits_data; // @[TraceFunnel.scala 56:34]
  wire  traceSink_auto_sba_out_d_valid; // @[TraceFunnel.scala 56:34]
  wire [1:0] traceSink_auto_sba_out_d_bits_source; // @[TraceFunnel.scala 56:34]
  wire  traceSink_auto_sba_out_d_bits_denied; // @[TraceFunnel.scala 56:34]
  wire  traceSink_auto_sink_out_a_ready; // @[TraceFunnel.scala 56:34]
  wire  traceSink_auto_sink_out_a_valid; // @[TraceFunnel.scala 56:34]
  wire [2:0] traceSink_auto_sink_out_a_bits_opcode; // @[TraceFunnel.scala 56:34]
  wire  traceSink_auto_sink_out_a_bits_source; // @[TraceFunnel.scala 56:34]
  wire [30:0] traceSink_auto_sink_out_a_bits_address; // @[TraceFunnel.scala 56:34]
  wire [31:0] traceSink_auto_sink_out_a_bits_data; // @[TraceFunnel.scala 56:34]
  wire  traceSink_auto_sink_out_d_valid; // @[TraceFunnel.scala 56:34]
  wire  traceSink_auto_sink_out_d_bits_denied; // @[TraceFunnel.scala 56:34]
  wire [31:0] traceSink_auto_sink_out_d_bits_data; // @[TraceFunnel.scala 56:34]
  wire  traceSink_io_teActive; // @[TraceFunnel.scala 56:34]
  wire [3:0] traceSink_io_teControl_teSink; // @[TraceFunnel.scala 56:34]
  wire  traceSink_io_teControl_teStopOnWrap; // @[TraceFunnel.scala 56:34]
  wire  traceSink_io_stream_ready; // @[TraceFunnel.scala 56:34]
  wire  traceSink_io_stream_valid; // @[TraceFunnel.scala 56:34]
  wire [31:0] traceSink_io_stream_bits_data; // @[TraceFunnel.scala 56:34]
  wire [31:0] traceSink_io_streamBaseH; // @[TraceFunnel.scala 56:34]
  wire [29:0] traceSink_io_streamWP; // @[TraceFunnel.scala 56:34]
  wire  traceSink_io_streamWPInc; // @[TraceFunnel.scala 56:34]
  wire  traceSink_io_streamWPWrap; // @[TraceFunnel.scala 56:34]
  wire  traceSink_io_busWrEn; // @[TraceFunnel.scala 56:34]
  wire  traceSink_io_busRdEn; // @[TraceFunnel.scala 56:34]
  wire  traceSink_io_busReady; // @[TraceFunnel.scala 56:34]
  wire [5:0] traceSink_io_busAP; // @[TraceFunnel.scala 56:34]
  wire [31:0] traceSink_io_busWrData; // @[TraceFunnel.scala 56:34]
  wire [31:0] traceSink_io_busRdData; // @[TraceFunnel.scala 56:34]
  wire  traceSink_io_sinkError; // @[TraceFunnel.scala 56:34]
  wire  tracePIB_rf_reset; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_clock; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_reset; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_auto_sink_in_a_ready; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_auto_sink_in_a_valid; // @[TraceFunnel.scala 58:56]
  wire [2:0] tracePIB_auto_sink_in_a_bits_opcode; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_auto_sink_in_a_bits_source; // @[TraceFunnel.scala 58:56]
  wire [30:0] tracePIB_auto_sink_in_a_bits_address; // @[TraceFunnel.scala 58:56]
  wire [31:0] tracePIB_auto_sink_in_a_bits_data; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_auto_sink_in_d_ready; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_auto_sink_in_d_valid; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_auto_sink_in_d_bits_source; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_auto_sink_in_d_bits_denied; // @[TraceFunnel.scala 58:56]
  wire [2:0] tracePIB_io_pibControl_bits_reserved0; // @[TraceFunnel.scala 58:56]
  wire [12:0] tracePIB_io_pibControl_bits_pibDivider; // @[TraceFunnel.scala 58:56]
  wire [5:0] tracePIB_io_pibControl_bits_reserved1; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_io_pibControl_bits_pibCalibrate; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_io_pibControl_bits_pibRefCenter; // @[TraceFunnel.scala 58:56]
  wire [3:0] tracePIB_io_pibControl_bits_pibMode; // @[TraceFunnel.scala 58:56]
  wire [1:0] tracePIB_io_pibControl_bits_reserved2; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_io_pibControl_bits_pibEnable; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_io_pibControl_bits_pibActive; // @[TraceFunnel.scala 58:56]
  wire [12:0] tracePIB_io_pibControlSync_pibDivider; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_io_pibControlSync_pibCalibrate; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_io_pibControlSync_pibRefCenter; // @[TraceFunnel.scala 58:56]
  wire [3:0] tracePIB_io_pibControlSync_pibMode; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_io_pibControlSync_pibEnable; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_io_pibControlSync_pibActive; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_io_streamOut_ready; // @[TraceFunnel.scala 58:56]
  wire  tracePIB_io_streamOut_valid; // @[TraceFunnel.scala 58:56]
  wire [31:0] tracePIB_io_streamOut_bits; // @[TraceFunnel.scala 58:56]
  wire  teSinkXbar_rf_reset; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_clock; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_reset; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_in_a_ready; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_in_a_valid; // @[TraceFunnel.scala 83:53]
  wire [2:0] teSinkXbar_auto_in_a_bits_opcode; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_in_a_bits_source; // @[TraceFunnel.scala 83:53]
  wire [30:0] teSinkXbar_auto_in_a_bits_address; // @[TraceFunnel.scala 83:53]
  wire [31:0] teSinkXbar_auto_in_a_bits_data; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_in_d_valid; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_in_d_bits_denied; // @[TraceFunnel.scala 83:53]
  wire [31:0] teSinkXbar_auto_in_d_bits_data; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_out_1_a_ready; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_out_1_a_valid; // @[TraceFunnel.scala 83:53]
  wire [2:0] teSinkXbar_auto_out_1_a_bits_opcode; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_out_1_a_bits_source; // @[TraceFunnel.scala 83:53]
  wire [30:0] teSinkXbar_auto_out_1_a_bits_address; // @[TraceFunnel.scala 83:53]
  wire [31:0] teSinkXbar_auto_out_1_a_bits_data; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_out_1_d_ready; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_out_1_d_valid; // @[TraceFunnel.scala 83:53]
  wire [2:0] teSinkXbar_auto_out_1_d_bits_opcode; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_out_1_d_bits_source; // @[TraceFunnel.scala 83:53]
  wire [31:0] teSinkXbar_auto_out_1_d_bits_data; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_out_0_a_ready; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_out_0_a_valid; // @[TraceFunnel.scala 83:53]
  wire [2:0] teSinkXbar_auto_out_0_a_bits_opcode; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_out_0_a_bits_source; // @[TraceFunnel.scala 83:53]
  wire [30:0] teSinkXbar_auto_out_0_a_bits_address; // @[TraceFunnel.scala 83:53]
  wire [31:0] teSinkXbar_auto_out_0_a_bits_data; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_out_0_d_ready; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_out_0_d_valid; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_out_0_d_bits_source; // @[TraceFunnel.scala 83:53]
  wire  teSinkXbar_auto_out_0_d_bits_denied; // @[TraceFunnel.scala 83:53]
  wire  teSRAMSink_rf_reset; // @[TraceFunnel.scala 90:55]
  wire  teSRAMSink_clock; // @[TraceFunnel.scala 90:55]
  wire  teSRAMSink_reset; // @[TraceFunnel.scala 90:55]
  wire  teSRAMSink_auto_in_a_ready; // @[TraceFunnel.scala 90:55]
  wire  teSRAMSink_auto_in_a_valid; // @[TraceFunnel.scala 90:55]
  wire [2:0] teSRAMSink_auto_in_a_bits_opcode; // @[TraceFunnel.scala 90:55]
  wire  teSRAMSink_auto_in_a_bits_source; // @[TraceFunnel.scala 90:55]
  wire [30:0] teSRAMSink_auto_in_a_bits_address; // @[TraceFunnel.scala 90:55]
  wire [31:0] teSRAMSink_auto_in_a_bits_data; // @[TraceFunnel.scala 90:55]
  wire  teSRAMSink_auto_in_d_ready; // @[TraceFunnel.scala 90:55]
  wire  teSRAMSink_auto_in_d_valid; // @[TraceFunnel.scala 90:55]
  wire [2:0] teSRAMSink_auto_in_d_bits_opcode; // @[TraceFunnel.scala 90:55]
  wire  teSRAMSink_auto_in_d_bits_source; // @[TraceFunnel.scala 90:55]
  wire [31:0] teSRAMSink_auto_in_d_bits_data; // @[TraceFunnel.scala 90:55]
  wire  tracePacker_clock; // @[TraceFunnel.scala 110:31]
  wire  tracePacker_reset; // @[TraceFunnel.scala 110:31]
  wire  tracePacker_io_teActive; // @[TraceFunnel.scala 110:31]
  wire  tracePacker_io_btm_ready; // @[TraceFunnel.scala 110:31]
  wire  tracePacker_io_btm_valid; // @[TraceFunnel.scala 110:31]
  wire [4:0] tracePacker_io_btm_bits_length; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_0_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_0_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_1_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_1_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_2_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_2_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_3_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_3_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_4_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_4_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_5_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_5_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_6_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_6_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_7_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_7_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_8_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_8_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_9_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_9_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_10_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_10_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_11_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_11_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_12_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_12_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_13_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_13_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_14_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_14_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_15_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_15_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_16_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_16_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_17_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_17_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_18_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_18_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_19_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_19_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_20_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_20_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_21_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_21_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_22_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_22_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_23_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_23_mseo; // @[TraceFunnel.scala 110:31]
  wire [5:0] tracePacker_io_btm_bits_slices_24_data; // @[TraceFunnel.scala 110:31]
  wire [1:0] tracePacker_io_btm_bits_slices_24_mseo; // @[TraceFunnel.scala 110:31]
  wire  tracePacker_io_stream_ready; // @[TraceFunnel.scala 110:31]
  wire  tracePacker_io_stream_valid; // @[TraceFunnel.scala 110:31]
  wire [31:0] tracePacker_io_stream_bits_data; // @[TraceFunnel.scala 110:31]
  wire  traceGapper_clock; // @[TraceFunnel.scala 111:31]
  wire  traceGapper_reset; // @[TraceFunnel.scala 111:31]
  wire  traceGapper_io_teActive; // @[TraceFunnel.scala 111:31]
  wire  traceGapper_io_disableGapper; // @[TraceFunnel.scala 111:31]
  wire  traceGapper_io_streamIn_ready; // @[TraceFunnel.scala 111:31]
  wire  traceGapper_io_streamIn_valid; // @[TraceFunnel.scala 111:31]
  wire [31:0] traceGapper_io_streamIn_bits_data; // @[TraceFunnel.scala 111:31]
  wire  traceGapper_io_streamOut_ready; // @[TraceFunnel.scala 111:31]
  wire  traceGapper_io_streamOut_valid; // @[TraceFunnel.scala 111:31]
  wire [31:0] traceGapper_io_streamOut_bits_data; // @[TraceFunnel.scala 111:31]
  wire  traceGapper_io_fifoFlush; // @[TraceFunnel.scala 111:31]
  wire  traceGapper_io_fifoEmpty; // @[TraceFunnel.scala 111:31]
  wire  sinkArbiter_clock; // @[TraceFunnel.scala 255:29]
  wire  sinkArbiter_reset; // @[TraceFunnel.scala 255:29]
  wire  sinkArbiter_io_in_0_ready; // @[TraceFunnel.scala 255:29]
  wire  sinkArbiter_io_in_0_valid; // @[TraceFunnel.scala 255:29]
  wire [4:0] sinkArbiter_io_in_0_bits_length; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_0_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_0_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_1_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_1_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_2_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_2_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_3_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_3_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_4_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_4_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_5_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_5_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_6_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_6_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_7_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_7_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_8_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_8_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_9_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_9_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_10_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_10_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_11_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_11_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_12_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_12_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_13_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_13_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_14_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_14_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_15_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_15_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_16_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_16_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_17_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_17_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_18_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_18_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_19_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_19_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_20_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_20_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_21_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_21_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_22_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_22_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_23_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_23_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_0_bits_slices_24_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_0_bits_slices_24_mseo; // @[TraceFunnel.scala 255:29]
  wire  sinkArbiter_io_in_1_ready; // @[TraceFunnel.scala 255:29]
  wire  sinkArbiter_io_in_1_valid; // @[TraceFunnel.scala 255:29]
  wire [4:0] sinkArbiter_io_in_1_bits_length; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_0_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_0_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_1_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_1_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_2_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_2_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_3_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_3_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_4_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_4_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_5_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_5_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_6_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_6_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_7_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_7_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_8_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_8_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_9_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_9_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_10_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_10_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_11_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_11_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_12_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_12_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_13_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_13_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_14_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_14_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_15_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_15_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_16_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_16_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_17_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_17_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_18_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_18_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_19_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_19_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_20_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_20_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_21_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_21_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_22_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_22_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_23_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_23_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_in_1_bits_slices_24_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_in_1_bits_slices_24_mseo; // @[TraceFunnel.scala 255:29]
  wire  sinkArbiter_io_out_ready; // @[TraceFunnel.scala 255:29]
  wire  sinkArbiter_io_out_valid; // @[TraceFunnel.scala 255:29]
  wire [4:0] sinkArbiter_io_out_bits_length; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_0_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_0_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_1_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_1_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_2_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_2_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_3_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_3_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_4_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_4_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_5_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_5_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_6_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_6_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_7_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_7_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_8_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_8_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_9_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_9_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_10_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_10_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_11_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_11_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_12_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_12_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_13_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_13_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_14_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_14_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_15_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_15_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_16_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_16_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_17_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_17_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_18_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_18_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_19_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_19_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_20_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_20_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_21_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_21_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_22_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_22_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_23_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_23_mseo; // @[TraceFunnel.scala 255:29]
  wire [5:0] sinkArbiter_io_out_bits_slices_24_data; // @[TraceFunnel.scala 255:29]
  wire [1:0] sinkArbiter_io_out_bits_slices_24_mseo; // @[TraceFunnel.scala 255:29]
  wire  traceUnpacker_rf_reset; // @[TraceFunnel.scala 270:33]
  wire  traceUnpacker_clock; // @[TraceFunnel.scala 270:33]
  wire  traceUnpacker_reset; // @[TraceFunnel.scala 270:33]
  wire  traceUnpacker_io_teActive; // @[TraceFunnel.scala 270:33]
  wire  traceUnpacker_io_stream_ready; // @[TraceFunnel.scala 270:33]
  wire  traceUnpacker_io_stream_valid; // @[TraceFunnel.scala 270:33]
  wire [31:0] traceUnpacker_io_stream_bits; // @[TraceFunnel.scala 270:33]
  wire  traceUnpacker_io_btm_ready; // @[TraceFunnel.scala 270:33]
  wire  traceUnpacker_io_btm_valid; // @[TraceFunnel.scala 270:33]
  wire [4:0] traceUnpacker_io_btm_bits_length; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_0_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_0_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_1_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_1_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_2_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_2_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_3_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_3_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_4_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_4_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_5_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_5_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_6_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_6_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_7_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_7_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_8_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_8_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_9_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_9_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_10_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_10_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_11_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_11_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_12_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_12_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_13_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_13_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_14_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_14_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_15_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_15_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_16_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_16_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_17_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_17_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_18_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_18_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_19_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_19_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_20_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_20_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_21_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_21_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_22_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_22_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_23_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_23_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_io_btm_bits_slices_24_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_io_btm_bits_slices_24_mseo; // @[TraceFunnel.scala 270:33]
  wire  traceUnpacker_1_rf_reset; // @[TraceFunnel.scala 270:33]
  wire  traceUnpacker_1_clock; // @[TraceFunnel.scala 270:33]
  wire  traceUnpacker_1_reset; // @[TraceFunnel.scala 270:33]
  wire  traceUnpacker_1_io_teActive; // @[TraceFunnel.scala 270:33]
  wire  traceUnpacker_1_io_stream_ready; // @[TraceFunnel.scala 270:33]
  wire  traceUnpacker_1_io_stream_valid; // @[TraceFunnel.scala 270:33]
  wire [31:0] traceUnpacker_1_io_stream_bits; // @[TraceFunnel.scala 270:33]
  wire  traceUnpacker_1_io_btm_ready; // @[TraceFunnel.scala 270:33]
  wire  traceUnpacker_1_io_btm_valid; // @[TraceFunnel.scala 270:33]
  wire [4:0] traceUnpacker_1_io_btm_bits_length; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_0_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_0_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_1_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_1_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_2_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_2_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_3_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_3_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_4_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_4_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_5_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_5_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_6_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_6_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_7_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_7_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_8_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_8_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_9_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_9_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_10_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_10_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_11_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_11_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_12_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_12_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_13_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_13_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_14_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_14_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_15_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_15_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_16_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_16_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_17_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_17_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_18_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_18_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_19_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_19_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_20_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_20_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_21_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_21_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_22_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_22_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_23_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_23_mseo; // @[TraceFunnel.scala 270:33]
  wire [5:0] traceUnpacker_1_io_btm_bits_slices_24_data; // @[TraceFunnel.scala 270:33]
  wire [1:0] traceUnpacker_1_io_btm_bits_slices_24_mseo; // @[TraceFunnel.scala 270:33]
  wire  pibOuter_clock; // @[TraceFunnel.scala 295:48]
  wire  pibOuter_reset; // @[TraceFunnel.scala 295:48]
  wire [12:0] pibOuter_io_pibControlSync_pibDivider; // @[TraceFunnel.scala 295:48]
  wire  pibOuter_io_pibControlSync_pibCalibrate; // @[TraceFunnel.scala 295:48]
  wire  pibOuter_io_pibControlSync_pibRefCenter; // @[TraceFunnel.scala 295:48]
  wire [3:0] pibOuter_io_pibControlSync_pibMode; // @[TraceFunnel.scala 295:48]
  wire  pibOuter_io_pibControlSync_pibEnable; // @[TraceFunnel.scala 295:48]
  wire  pibOuter_io_pibControlSync_pibActive; // @[TraceFunnel.scala 295:48]
  wire  pibOuter_io_streamIn_ready; // @[TraceFunnel.scala 295:48]
  wire  pibOuter_io_streamIn_valid; // @[TraceFunnel.scala 295:48]
  wire [31:0] pibOuter_io_streamIn_bits; // @[TraceFunnel.scala 295:48]
  wire  pibOuter_io_pib_tref; // @[TraceFunnel.scala 295:48]
  wire [3:0] pibOuter_io_pib_tdata; // @[TraceFunnel.scala 295:48]
  wire  pibOuter_io_pibEmpty; // @[TraceFunnel.scala 295:48]
  reg [3:0] teControlReg_teSink; // @[TraceFunnel.scala 120:37]
  reg  teControlReg_teSinkError; // @[TraceFunnel.scala 120:37]
  reg  teControlReg_teStopOnWrap; // @[TraceFunnel.scala 120:37]
  reg  teControlReg_teEnable; // @[TraceFunnel.scala 120:37]
  reg  teControlReg_teActive; // @[TraceFunnel.scala 120:37]
  wire  teControlWrData_teActive = auto_reg_in_a_bits_data[0]; // @[TraceFunnel.scala 49:31]
  wire  _T = ~teControlWrData_teActive; // @[TraceFunnel.scala 139:26]
  wire  in_bits_read = auto_reg_in_a_bits_opcode == 3'h4; // @[TraceFunnel.scala 49:31]
  wire [8:0] in_bits_index = auto_reg_in_a_bits_address[11:3]; // @[TraceFunnel.scala 49:31 TraceFunnel.scala 49:31]
  wire  out_iindex_hi_hi = in_bits_index[8]; // @[TraceFunnel.scala 49:31]
  wire  out_iindex_hi_lo = in_bits_index[2]; // @[TraceFunnel.scala 49:31]
  wire  out_iindex_lo_hi = in_bits_index[1]; // @[TraceFunnel.scala 49:31]
  wire  out_iindex_lo_lo = in_bits_index[0]; // @[TraceFunnel.scala 49:31]
  wire [3:0] out_iindex = {out_iindex_hi_hi,out_iindex_hi_lo,out_iindex_lo_hi,out_iindex_lo_lo}; // @[Cat.scala 30:58]
  wire [8:0] out_findex = in_bits_index & 9'hf8; // @[TraceFunnel.scala 49:31]
  wire  _out_T_2 = out_findex == 9'he0; // @[TraceFunnel.scala 49:31]
  wire  _out_T_8 = out_findex == 9'h0; // @[TraceFunnel.scala 49:31]
  wire  _out_wofireMux_T_1 = ~in_bits_read; // @[TraceFunnel.scala 49:31]
  wire [15:0] _out_backSel_T = 16'h1 << out_iindex; // @[OneHot.scala 58:35]
  wire  out_backSel_0 = _out_backSel_T[0]; // @[TraceFunnel.scala 49:31]
  wire  out_woready_0 = auto_reg_in_a_valid & auto_reg_in_d_ready & _out_wofireMux_T_1 & out_backSel_0 & _out_T_8; // @[TraceFunnel.scala 49:31]
  wire [7:0] out_backMask_hi_hi_hi = auto_reg_in_a_bits_mask[7] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_hi_lo = auto_reg_in_a_bits_mask[6] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_lo_hi = auto_reg_in_a_bits_mask[5] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_lo_lo = auto_reg_in_a_bits_mask[4] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_hi_hi = auto_reg_in_a_bits_mask[3] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_hi_lo = auto_reg_in_a_bits_mask[2] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_lo_hi = auto_reg_in_a_bits_mask[1] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_lo_lo = auto_reg_in_a_bits_mask[0] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [63:0] out_backMask = {out_backMask_hi_hi_hi,out_backMask_hi_hi_lo,out_backMask_hi_lo_hi,out_backMask_hi_lo_lo,
    out_backMask_lo_hi_hi,out_backMask_lo_hi_lo,out_backMask_lo_lo_hi,out_backMask_lo_lo_lo}; // @[Cat.scala 30:58]
  wire  _out_womask_T = out_backMask[0]; // @[TraceFunnel.scala 49:31]
  wire  out_womask = &out_backMask[0]; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready = out_woready_0 & out_womask; // @[TraceFunnel.scala 49:31]
  wire  _out_womask_T_1 = out_backMask[1]; // @[TraceFunnel.scala 49:31]
  wire  out_womask_1 = &out_backMask[1]; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_1 = out_woready_0 & out_womask_1; // @[TraceFunnel.scala 49:31]
  wire  teControlWrData_teEnable = auto_reg_in_a_bits_data[1]; // @[TraceFunnel.scala 49:31]
  wire  _out_womask_T_7 = out_backMask[27]; // @[TraceFunnel.scala 49:31]
  wire  out_womask_7 = &out_backMask[27]; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_7 = out_woready_0 & out_womask_7; // @[TraceFunnel.scala 49:31]
  wire  teControlWrData_teSinkError = auto_reg_in_a_bits_data[27]; // @[TraceFunnel.scala 49:31]
  wire  _GEN_2 = traceSink_io_sinkError | teControlReg_teSinkError; // @[TraceFunnel.scala 145:49 TraceFunnel.scala 145:76 TraceFunnel.scala 120:37]
  wire [3:0] teControlWrData_teSink = auto_reg_in_a_bits_data[31:28]; // @[TraceFunnel.scala 49:31]
  wire [3:0] _out_womask_T_8 = out_backMask[31:28]; // @[TraceFunnel.scala 49:31]
  wire  out_womask_8 = &out_backMask[31:28]; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_8 = out_woready_0 & out_womask_8; // @[TraceFunnel.scala 49:31]
  wire  _T_9 = teControlReg_teStopOnWrap & traceSink_io_streamWPWrap; // @[TraceFunnel.scala 199:52]
  wire  _T_18 = teControlReg_teSink == 4'h6; // @[TraceFunnel.scala 207:78]
  wire  _sinkReady_T = ~teControlReg_teEnable; // @[TraceFunnel.scala 263:43]
  wire  inDecoupled_ready = traceUnpacker_io_stream_ready; // @[TraceFunnel.scala 262:29 TraceFunnel.scala 272:31]
  wire  _sinkReady_T_1 = inDecoupled_ready | ~teControlReg_teEnable; // @[TraceFunnel.scala 263:41]
  wire  _sinkReady_T_2 = ~teControlReg_teActive; // @[TraceFunnel.scala 263:68]
  wire  sinkReady = _sinkReady_T_1 | _sinkReady_T_2; // @[TraceFunnel.scala 263:66]
  wire  bundleIn_0_1_d_valid = auto_sink_in_0_a_valid & sinkReady; // @[TraceFunnel.scala 267:32]
  wire  _denied_T_4 = _sinkReady_T & ~teControlReg_teStopOnWrap | _sinkReady_T_2; // @[TraceFunnel.scala 268:85]
  wire  inDecoupled_1_ready = traceUnpacker_1_io_stream_ready; // @[TraceFunnel.scala 262:29 TraceFunnel.scala 272:31]
  wire  _sinkReady_T_4 = inDecoupled_1_ready | ~teControlReg_teEnable; // @[TraceFunnel.scala 263:41]
  wire  sinkReady_1 = _sinkReady_T_4 | _sinkReady_T_2; // @[TraceFunnel.scala 263:66]
  wire  bundleIn_0_2_d_valid = auto_sink_in_1_a_valid & sinkReady_1; // @[TraceFunnel.scala 267:32]
  wire  teEmpty = ~tracePacker_io_stream_valid & traceGapper_io_fifoEmpty; // @[TraceFunnel.scala 306:45]
  wire  usingSRAM = traceSink_io_teControl_teSink == 4'h4; // @[TraceSink.scala 25:62]
  wire  usingSBA = traceSink_io_teControl_teSink == 4'h7; // @[TraceSink.scala 26:62]
  reg [29:0] teSinkBaseReg_teSinkBase; // @[TraceSink.scala 38:40]
  wire [29:0] teSinkBaseWrData_teSinkBase = auto_reg_in_a_bits_data[31:2]; // @[TraceFunnel.scala 49:31]
  wire [30:0] _teSinkBaseReg_teSinkBase_T = {{1'd0}, teSinkBaseWrData_teSinkBase}; // @[TraceSink.scala 45:65]
  wire  out_backSel_2 = _out_backSel_T[2]; // @[TraceFunnel.scala 49:31]
  wire  out_woready_27 = auto_reg_in_a_valid & auto_reg_in_d_ready & _out_wofireMux_T_1 & out_backSel_2 & _out_T_8; // @[TraceFunnel.scala 49:31]
  wire [29:0] _out_womask_T_27 = out_backMask[31:2]; // @[TraceFunnel.scala 49:31]
  wire  out_womask_27 = &out_backMask[31:2]; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_27 = out_woready_27 & out_womask_27; // @[TraceFunnel.scala 49:31]
  wire [30:0] _GEN_24 = out_f_woready_27 ? _teSinkBaseReg_teSinkBase_T : {{1'd0}, teSinkBaseReg_teSinkBase}; // @[TraceSink.scala 44:35 TraceSink.scala 45:34 TraceSink.scala 38:40]
  wire [30:0] _GEN_25 = _sinkReady_T_2 ? 31'h0 : _GEN_24; // @[TraceSink.scala 42:24 TraceSink.scala 43:34]
  wire [29:0] teSinkBase = usingSBA ? teSinkBaseReg_teSinkBase : 30'h0; // @[TraceSink.scala 48:23 TraceSink.scala 48:36]
  reg [31:0] teSinkBaseHReg_teSinkBaseH; // @[TraceSink.scala 63:40]
  wire [31:0] teSinkBaseHWrData_teSinkBaseH = {{27'd0}, auto_reg_in_a_bits_data[36:32]};
  wire [31:0] _teSinkBaseHReg_teSinkBaseH_T = teSinkBaseHWrData_teSinkBaseH & 32'h1f; // @[TraceSink.scala 70:69]
  wire [4:0] _out_womask_T_28 = out_backMask[36:32]; // @[TraceFunnel.scala 49:31]
  wire  out_womask_28 = &out_backMask[36:32]; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_28 = out_woready_27 & out_womask_28; // @[TraceFunnel.scala 49:31]
  reg [29:0] teSinkLimitReg_teSinkLimit; // @[TraceSink.scala 89:41]
  wire  out_backSel_3 = _out_backSel_T[3]; // @[TraceFunnel.scala 49:31]
  wire  out_woready_30 = auto_reg_in_a_valid & auto_reg_in_d_ready & _out_wofireMux_T_1 & out_backSel_3 & _out_T_8; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_30 = out_woready_30 & out_womask_27; // @[TraceFunnel.scala 49:31]
  wire [30:0] _GEN_29 = out_f_woready_30 ? _teSinkBaseReg_teSinkBase_T : {{1'd0}, teSinkLimitReg_teSinkLimit}; // @[TraceSink.scala 95:36 TraceSink.scala 96:36 TraceSink.scala 89:41]
  wire [30:0] _GEN_30 = _sinkReady_T_2 ? 31'h0 : _GEN_29; // @[TraceSink.scala 93:24 TraceSink.scala 94:36]
  wire [29:0] teSinkLimit = usingSBA ? teSinkLimitReg_teSinkLimit : 30'h3f; // @[TraceSink.scala 99:23 TraceSink.scala 99:37 TraceSink.scala 23:19]
  reg [5:0] teSinkRPReg_teSinkRP; // @[TraceSink.scala 122:38]
  wire  _out_rofireMux_T_1 = auto_reg_in_a_valid & auto_reg_in_d_ready & in_bits_read; // @[TraceFunnel.scala 49:31]
  wire  out_backSel_4 = _out_backSel_T[4]; // @[TraceFunnel.scala 49:31]
  wire  out_roready_36 = _out_rofireMux_T_1 & out_backSel_4 & _out_T_8; // @[TraceFunnel.scala 49:31]
  wire  out_romask_36 = |out_backMask[63:32]; // @[TraceFunnel.scala 49:31]
  wire  out_f_roready_36 = out_roready_36 & out_romask_36; // @[TraceFunnel.scala 49:31]
  wire  out_woready_36 = auto_reg_in_a_valid & auto_reg_in_d_ready & _out_wofireMux_T_1 & out_backSel_4 & _out_T_8; // @[TraceFunnel.scala 49:31]
  wire  out_womask_36 = &out_backMask[63:32]; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_36 = out_woready_36 & out_womask_36; // @[TraceFunnel.scala 49:31]
  wire  teSinkReady = _sinkReady_T_2 | ~usingSRAM | traceSink_io_busReady; // @[TraceSink.scala 139:46]
  wire [5:0] _teSinkRPReg_teSinkRP_T_1 = teSinkRPReg_teSinkRP + 6'h1; // @[TraceSink.scala 131:54]
  wire  out_wofireMux_out_4 = teSinkReady | ~out_womask_36; // @[TraceFunnel.scala 49:31]
  wire  out_wofireMux_all = out_woready_36 & out_wofireMux_out_4; // @[ReduceOthers.scala 47:21]
  wire [5:0] _out_womask_T_35 = out_backMask[7:2]; // @[TraceFunnel.scala 49:31]
  wire  out_womask_35 = &out_backMask[7:2]; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_35 = out_wofireMux_all & out_womask_35; // @[TraceFunnel.scala 49:31]
  wire [5:0] teSinkRPWrData_teSinkRP = auto_reg_in_a_bits_data[7:2]; // @[TraceFunnel.scala 49:31]
  reg [29:0] teSinkWPReg_teSinkWP; // @[TraceSink.scala 165:38]
  reg  teSinkWPReg_teSinkWrapped; // @[TraceSink.scala 165:38]
  wire [29:0] _teSinkWPNext_teSinkWP_T_1 = teSinkWPReg_teSinkWP + 30'h1; // @[TraceSink.scala 181:57]
  wire  _GEN_35 = teSinkWPReg_teSinkWP == teSinkLimit | teSinkWPReg_teSinkWrapped; // @[TraceSink.scala 177:53 TraceSink.scala 178:38 TraceSink.scala 172:20]
  wire [29:0] _GEN_36 = teSinkWPReg_teSinkWP == teSinkLimit ? teSinkBase : _teSinkWPNext_teSinkWP_T_1; // @[TraceSink.scala 177:53 TraceSink.scala 179:33 TraceSink.scala 181:33]
  wire  _GEN_37 = traceSink_io_streamWPInc ? _GEN_35 : teSinkWPReg_teSinkWrapped; // @[TraceSink.scala 176:47 TraceSink.scala 172:20]
  wire [29:0] _GEN_38 = traceSink_io_streamWPInc ? _GEN_36 : teSinkWPReg_teSinkWP; // @[TraceSink.scala 176:47 TraceSink.scala 172:20]
  wire [29:0] _out_womask_T_33 = out_backMask[63:34]; // @[TraceFunnel.scala 49:31]
  wire  out_womask_33 = &out_backMask[63:34]; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_33 = out_woready_30 & out_womask_33; // @[TraceFunnel.scala 49:31]
  wire  teSinkWPWrData_teSinkWrapped = auto_reg_in_a_bits_data[32]; // @[TraceFunnel.scala 49:31]
  wire  teSinkWPNext_teSinkWrapped = out_f_woready_33 ? teSinkWPWrData_teSinkWrapped : _GEN_37; // @[TraceSink.scala 173:35 TraceSink.scala 174:36]
  wire [29:0] teSinkWPWrData_teSinkWP = auto_reg_in_a_bits_data[63:34]; // @[TraceFunnel.scala 49:31]
  wire [29:0] teSinkWPNext_teSinkWP = out_f_woready_33 ? teSinkWPWrData_teSinkWP : _GEN_38; // @[TraceSink.scala 173:35 TraceSink.scala 175:31]
  wire [30:0] _teSinkWPReg_teSinkWP_T = {{1'd0}, teSinkWPNext_teSinkWP}; // @[TraceSink.scala 191:57]
  wire [29:0] _teSinkWPReg_teSinkWP_T_1 = teSinkWPNext_teSinkWP & teSinkLimit; // @[TraceSink.scala 194:55]
  wire [29:0] _GEN_41 = usingSRAM ? _teSinkWPReg_teSinkWP_T_1 : 30'h0; // @[TraceSink.scala 193:30 TraceSink.scala 194:30 TraceSink.scala 196:30]
  wire  _GEN_42 = usingSRAM & teSinkWPNext_teSinkWrapped; // @[TraceSink.scala 193:30 TraceSink.scala 185:19 TraceSink.scala 197:35]
  wire [30:0] _GEN_43 = usingSBA ? _teSinkWPReg_teSinkWP_T : {{1'd0}, _GEN_41}; // @[TraceSink.scala 189:29 TraceSink.scala 191:32]
  wire [30:0] _GEN_45 = _sinkReady_T_2 ? 31'h0 : _GEN_43; // @[TraceSink.scala 186:24 TraceSink.scala 187:30]
  reg [12:0] pibControlReg_pibDivider; // @[TracePIB.scala 52:35]
  reg  pibControlReg_pibCalibrate; // @[TracePIB.scala 52:35]
  reg  pibControlReg_pibRefCenter; // @[TracePIB.scala 52:35]
  reg [3:0] pibControlReg_pibMode; // @[TracePIB.scala 52:35]
  reg  pibControlReg_pibEnable; // @[TracePIB.scala 52:35]
  reg  pibControlReg_pibActive; // @[TracePIB.scala 52:35]
  wire  out_backSel_8 = _out_backSel_T[8]; // @[TraceFunnel.scala 49:31]
  wire  out_woready_17 = auto_reg_in_a_valid & auto_reg_in_d_ready & _out_wofireMux_T_1 & out_backSel_8 & _out_T_2; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_17 = out_woready_17 & out_womask; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_18 = out_woready_17 & out_womask_1; // @[TraceFunnel.scala 49:31]
  wire [3:0] pibControlWrData_pibMode = auto_reg_in_a_bits_data[7:4]; // @[TraceFunnel.scala 49:31]
  wire [15:0] _T_37 = 16'h1 << pibControlWrData_pibMode; // @[TracePIB.scala 72:30]
  wire [15:0] _T_38 = 16'h731 & _T_37; // @[TracePIB.scala 72:23]
  wire  pibControlWrData_pibRefCenter = auto_reg_in_a_bits_data[8]; // @[TraceFunnel.scala 49:31]
  wire  _T_42 = pibControlWrData_pibMode[3] | ~pibControlWrData_pibRefCenter; // @[TracePIB.scala 73:64]
  wire  _T_43 = _T_38 != 16'h0 & _T_42; // @[TracePIB.scala 72:69]
  wire [3:0] _out_womask_T_21 = out_backMask[7:4]; // @[TraceFunnel.scala 49:31]
  wire  out_womask_21 = &out_backMask[7:4]; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_21 = out_woready_17 & out_womask_21; // @[TraceFunnel.scala 49:31]
  wire  _out_womask_T_22 = out_backMask[8]; // @[TraceFunnel.scala 49:31]
  wire  out_womask_22 = &out_backMask[8]; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_22 = out_woready_17 & out_womask_22; // @[TraceFunnel.scala 49:31]
  wire  _out_womask_T_23 = out_backMask[9]; // @[TraceFunnel.scala 49:31]
  wire  out_womask_23 = &out_backMask[9]; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_23 = out_woready_17 & out_womask_23; // @[TraceFunnel.scala 49:31]
  wire  pibControlWrData_pibCalibrate = auto_reg_in_a_bits_data[9]; // @[TraceFunnel.scala 49:31]
  wire [12:0] _out_womask_T_25 = out_backMask[28:16]; // @[TraceFunnel.scala 49:31]
  wire  out_womask_25 = &out_backMask[28:16]; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_25 = out_woready_17 & out_womask_25; // @[TraceFunnel.scala 49:31]
  wire [12:0] pibControlWrData_pibDivider = auto_reg_in_a_bits_data[28:16]; // @[TraceFunnel.scala 49:31]
  wire  out_rofireMux_out_4 = teSinkReady | ~out_romask_36; // @[TraceFunnel.scala 49:31]
  wire  _out_rofireMux_T_21 = out_rofireMux_out_4 | ~_out_T_8; // @[TraceFunnel.scala 49:31]
  wire  _GEN_109 = 4'h4 == out_iindex ? _out_rofireMux_T_21 : 1'h1; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_12 = 4'h8 == out_iindex; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  out_rofireMux = 4'hf == out_iindex | (4'he == out_iindex | (4'hd == out_iindex | (4'hc == out_iindex | (4'hb ==
    out_iindex | (4'ha == out_iindex | (4'h9 == out_iindex | (4'h8 == out_iindex | (4'h7 == out_iindex | (4'h6 ==
    out_iindex | (4'h5 == out_iindex | _GEN_109)))))))))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _out_wofireMux_T_22 = out_wofireMux_out_4 | ~_out_T_8; // @[TraceFunnel.scala 49:31]
  wire  _GEN_125 = 4'h4 == out_iindex ? _out_wofireMux_T_22 : 1'h1; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  out_wofireMux = 4'hf == out_iindex | (4'he == out_iindex | (4'hd == out_iindex | (4'hc == out_iindex | (4'hb ==
    out_iindex | (4'ha == out_iindex | (4'h9 == out_iindex | (4'h8 == out_iindex | (4'h7 == out_iindex | (4'h6 ==
    out_iindex | (4'h5 == out_iindex | _GEN_125)))))))))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  out_oready = in_bits_read ? out_rofireMux : out_wofireMux; // @[TraceFunnel.scala 49:31]
  wire [4:0] out_prepend_3 = {1'h0,teEmpty,1'h0,teControlReg_teEnable,teControlReg_teActive}; // @[Cat.scala 30:58]
  wire [13:0] out_prepend_lo_4 = {{9'd0}, out_prepend_3}; // @[TraceFunnel.scala 49:31]
  wire  _out_rimask_T_5 = out_backMask[14]; // @[TraceFunnel.scala 49:31]
  wire  out_wimask_5 = &out_backMask[14]; // @[TraceFunnel.scala 49:31]
  wire  out_f_woready_5 = out_woready_0 & out_wimask_5; // @[TraceFunnel.scala 49:31]
  wire [15:0] out_prepend_5 = {1'h0,teControlReg_teStopOnWrap,out_prepend_lo_4}; // @[Cat.scala 30:58]
  wire [26:0] out_prepend_lo_6 = {{11'd0}, out_prepend_5}; // @[TraceFunnel.scala 49:31]
  wire [41:0] out_prepend_14 = {1'h0,1'h0,1'h1,1'h1,1'h0,1'h1,4'h0,teControlReg_teSink,teControlReg_teSinkError,
    out_prepend_lo_6}; // @[Cat.scala 30:58]
  wire [47:0] out_prepend_lo_15 = {{6'd0}, out_prepend_14}; // @[TraceFunnel.scala 49:31]
  wire [49:0] out_prepend_15 = {2'h0,out_prepend_lo_15}; // @[Cat.scala 30:58]
  wire [10:0] out_prepend_22 = {1'h0,pibControlReg_pibCalibrate,pibControlReg_pibRefCenter,pibControlReg_pibMode,
    pibOuter_io_pibEmpty,1'h0,pibControlReg_pibEnable,pibControlReg_pibActive}; // @[Cat.scala 30:58]
  wire [15:0] out_prepend_lo_23 = {{5'd0}, out_prepend_22}; // @[TraceFunnel.scala 49:31]
  wire [28:0] out_prepend_23 = {pibControlReg_pibDivider,out_prepend_lo_23}; // @[Cat.scala 30:58]
  wire [63:0] out_prepend_25 = {teSinkBaseHReg_teSinkBaseH,teSinkBaseReg_teSinkBase,2'h0}; // @[Cat.scala 30:58]
  wire [63:0] out_prepend_29 = {teSinkWPReg_teSinkWP,1'h0,teSinkWPReg_teSinkWrapped,teSinkLimitReg_teSinkLimit,2'h0}; // @[Cat.scala 30:58]
  wire [7:0] out_prepend_30 = {teSinkRPReg_teSinkRP,2'h0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_31 = {{24'd0}, out_prepend_30}; // @[TraceFunnel.scala 49:31]
  wire [31:0] out_prepend_hi = traceSink_io_busRdData;
  wire [63:0] out_prepend_31 = {out_prepend_hi,out_prepend_lo_31}; // @[Cat.scala 30:58]
  wire  _out_out_bits_data_T = 4'h0 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_1 = 4'h2 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_2 = 4'h3 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_3 = 4'h4 == out_iindex; // @[Conditional.scala 37:30]
  wire  _GEN_137 = _GEN_12 ? _out_T_2 : 1'h1; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_138 = _out_out_bits_data_T_3 ? _out_T_8 : _GEN_137; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_139 = _out_out_bits_data_T_2 ? _out_T_8 : _GEN_138; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_140 = _out_out_bits_data_T_1 ? _out_T_8 : _GEN_139; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  out_out_bits_data_out = _out_out_bits_data_T ? _out_T_8 : _GEN_140; // @[Conditional.scala 40:58 MuxLiteral.scala 53:32]
  wire [28:0] _GEN_142 = _GEN_12 ? out_prepend_23 : 29'h0; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_143 = _out_out_bits_data_T_3 ? out_prepend_31 : {{35'd0}, _GEN_142}; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_144 = _out_out_bits_data_T_2 ? out_prepend_29 : _GEN_143; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_145 = _out_out_bits_data_T_1 ? {{27'd0}, out_prepend_25[36:0]} : _GEN_144; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] out_out_bits_data_out_1 = _out_out_bits_data_T ? {{14'd0}, out_prepend_15} : _GEN_145; // @[Conditional.scala 40:58 MuxLiteral.scala 53:32]
  RHEA__TLTraceSink_2 traceSink ( // @[TraceFunnel.scala 56:34]
    .clock(traceSink_clock),
    .reset(traceSink_reset),
    .auto_sba_out_a_ready(traceSink_auto_sba_out_a_ready),
    .auto_sba_out_a_valid(traceSink_auto_sba_out_a_valid),
    .auto_sba_out_a_bits_source(traceSink_auto_sba_out_a_bits_source),
    .auto_sba_out_a_bits_address(traceSink_auto_sba_out_a_bits_address),
    .auto_sba_out_a_bits_data(traceSink_auto_sba_out_a_bits_data),
    .auto_sba_out_d_valid(traceSink_auto_sba_out_d_valid),
    .auto_sba_out_d_bits_source(traceSink_auto_sba_out_d_bits_source),
    .auto_sba_out_d_bits_denied(traceSink_auto_sba_out_d_bits_denied),
    .auto_sink_out_a_ready(traceSink_auto_sink_out_a_ready),
    .auto_sink_out_a_valid(traceSink_auto_sink_out_a_valid),
    .auto_sink_out_a_bits_opcode(traceSink_auto_sink_out_a_bits_opcode),
    .auto_sink_out_a_bits_source(traceSink_auto_sink_out_a_bits_source),
    .auto_sink_out_a_bits_address(traceSink_auto_sink_out_a_bits_address),
    .auto_sink_out_a_bits_data(traceSink_auto_sink_out_a_bits_data),
    .auto_sink_out_d_valid(traceSink_auto_sink_out_d_valid),
    .auto_sink_out_d_bits_denied(traceSink_auto_sink_out_d_bits_denied),
    .auto_sink_out_d_bits_data(traceSink_auto_sink_out_d_bits_data),
    .io_teActive(traceSink_io_teActive),
    .io_teControl_teSink(traceSink_io_teControl_teSink),
    .io_teControl_teStopOnWrap(traceSink_io_teControl_teStopOnWrap),
    .io_stream_ready(traceSink_io_stream_ready),
    .io_stream_valid(traceSink_io_stream_valid),
    .io_stream_bits_data(traceSink_io_stream_bits_data),
    .io_streamBaseH(traceSink_io_streamBaseH),
    .io_streamWP(traceSink_io_streamWP),
    .io_streamWPInc(traceSink_io_streamWPInc),
    .io_streamWPWrap(traceSink_io_streamWPWrap),
    .io_busWrEn(traceSink_io_busWrEn),
    .io_busRdEn(traceSink_io_busRdEn),
    .io_busReady(traceSink_io_busReady),
    .io_busAP(traceSink_io_busAP),
    .io_busWrData(traceSink_io_busWrData),
    .io_busRdData(traceSink_io_busRdData),
    .io_sinkError(traceSink_io_sinkError)
  );
  RHEA__TracePIB tracePIB ( // @[TraceFunnel.scala 58:56]
    .rf_reset(tracePIB_rf_reset),
    .clock(tracePIB_clock),
    .reset(tracePIB_reset),
    .auto_sink_in_a_ready(tracePIB_auto_sink_in_a_ready),
    .auto_sink_in_a_valid(tracePIB_auto_sink_in_a_valid),
    .auto_sink_in_a_bits_opcode(tracePIB_auto_sink_in_a_bits_opcode),
    .auto_sink_in_a_bits_source(tracePIB_auto_sink_in_a_bits_source),
    .auto_sink_in_a_bits_address(tracePIB_auto_sink_in_a_bits_address),
    .auto_sink_in_a_bits_data(tracePIB_auto_sink_in_a_bits_data),
    .auto_sink_in_d_ready(tracePIB_auto_sink_in_d_ready),
    .auto_sink_in_d_valid(tracePIB_auto_sink_in_d_valid),
    .auto_sink_in_d_bits_source(tracePIB_auto_sink_in_d_bits_source),
    .auto_sink_in_d_bits_denied(tracePIB_auto_sink_in_d_bits_denied),
    .io_pibControl_bits_reserved0(tracePIB_io_pibControl_bits_reserved0),
    .io_pibControl_bits_pibDivider(tracePIB_io_pibControl_bits_pibDivider),
    .io_pibControl_bits_reserved1(tracePIB_io_pibControl_bits_reserved1),
    .io_pibControl_bits_pibCalibrate(tracePIB_io_pibControl_bits_pibCalibrate),
    .io_pibControl_bits_pibRefCenter(tracePIB_io_pibControl_bits_pibRefCenter),
    .io_pibControl_bits_pibMode(tracePIB_io_pibControl_bits_pibMode),
    .io_pibControl_bits_reserved2(tracePIB_io_pibControl_bits_reserved2),
    .io_pibControl_bits_pibEnable(tracePIB_io_pibControl_bits_pibEnable),
    .io_pibControl_bits_pibActive(tracePIB_io_pibControl_bits_pibActive),
    .io_pibControlSync_pibDivider(tracePIB_io_pibControlSync_pibDivider),
    .io_pibControlSync_pibCalibrate(tracePIB_io_pibControlSync_pibCalibrate),
    .io_pibControlSync_pibRefCenter(tracePIB_io_pibControlSync_pibRefCenter),
    .io_pibControlSync_pibMode(tracePIB_io_pibControlSync_pibMode),
    .io_pibControlSync_pibEnable(tracePIB_io_pibControlSync_pibEnable),
    .io_pibControlSync_pibActive(tracePIB_io_pibControlSync_pibActive),
    .io_streamOut_ready(tracePIB_io_streamOut_ready),
    .io_streamOut_valid(tracePIB_io_streamOut_valid),
    .io_streamOut_bits(tracePIB_io_streamOut_bits)
  );
  RHEA__TLXbar_14 teSinkXbar ( // @[TraceFunnel.scala 83:53]
    .rf_reset(teSinkXbar_rf_reset),
    .clock(teSinkXbar_clock),
    .reset(teSinkXbar_reset),
    .auto_in_a_ready(teSinkXbar_auto_in_a_ready),
    .auto_in_a_valid(teSinkXbar_auto_in_a_valid),
    .auto_in_a_bits_opcode(teSinkXbar_auto_in_a_bits_opcode),
    .auto_in_a_bits_source(teSinkXbar_auto_in_a_bits_source),
    .auto_in_a_bits_address(teSinkXbar_auto_in_a_bits_address),
    .auto_in_a_bits_data(teSinkXbar_auto_in_a_bits_data),
    .auto_in_d_valid(teSinkXbar_auto_in_d_valid),
    .auto_in_d_bits_denied(teSinkXbar_auto_in_d_bits_denied),
    .auto_in_d_bits_data(teSinkXbar_auto_in_d_bits_data),
    .auto_out_1_a_ready(teSinkXbar_auto_out_1_a_ready),
    .auto_out_1_a_valid(teSinkXbar_auto_out_1_a_valid),
    .auto_out_1_a_bits_opcode(teSinkXbar_auto_out_1_a_bits_opcode),
    .auto_out_1_a_bits_source(teSinkXbar_auto_out_1_a_bits_source),
    .auto_out_1_a_bits_address(teSinkXbar_auto_out_1_a_bits_address),
    .auto_out_1_a_bits_data(teSinkXbar_auto_out_1_a_bits_data),
    .auto_out_1_d_ready(teSinkXbar_auto_out_1_d_ready),
    .auto_out_1_d_valid(teSinkXbar_auto_out_1_d_valid),
    .auto_out_1_d_bits_opcode(teSinkXbar_auto_out_1_d_bits_opcode),
    .auto_out_1_d_bits_source(teSinkXbar_auto_out_1_d_bits_source),
    .auto_out_1_d_bits_data(teSinkXbar_auto_out_1_d_bits_data),
    .auto_out_0_a_ready(teSinkXbar_auto_out_0_a_ready),
    .auto_out_0_a_valid(teSinkXbar_auto_out_0_a_valid),
    .auto_out_0_a_bits_opcode(teSinkXbar_auto_out_0_a_bits_opcode),
    .auto_out_0_a_bits_source(teSinkXbar_auto_out_0_a_bits_source),
    .auto_out_0_a_bits_address(teSinkXbar_auto_out_0_a_bits_address),
    .auto_out_0_a_bits_data(teSinkXbar_auto_out_0_a_bits_data),
    .auto_out_0_d_ready(teSinkXbar_auto_out_0_d_ready),
    .auto_out_0_d_valid(teSinkXbar_auto_out_0_d_valid),
    .auto_out_0_d_bits_source(teSinkXbar_auto_out_0_d_bits_source),
    .auto_out_0_d_bits_denied(teSinkXbar_auto_out_0_d_bits_denied)
  );
  RHEA__TLRAM teSRAMSink ( // @[TraceFunnel.scala 90:55]
    .rf_reset(teSRAMSink_rf_reset),
    .clock(teSRAMSink_clock),
    .reset(teSRAMSink_reset),
    .auto_in_a_ready(teSRAMSink_auto_in_a_ready),
    .auto_in_a_valid(teSRAMSink_auto_in_a_valid),
    .auto_in_a_bits_opcode(teSRAMSink_auto_in_a_bits_opcode),
    .auto_in_a_bits_source(teSRAMSink_auto_in_a_bits_source),
    .auto_in_a_bits_address(teSRAMSink_auto_in_a_bits_address),
    .auto_in_a_bits_data(teSRAMSink_auto_in_a_bits_data),
    .auto_in_d_ready(teSRAMSink_auto_in_d_ready),
    .auto_in_d_valid(teSRAMSink_auto_in_d_valid),
    .auto_in_d_bits_opcode(teSRAMSink_auto_in_d_bits_opcode),
    .auto_in_d_bits_source(teSRAMSink_auto_in_d_bits_source),
    .auto_in_d_bits_data(teSRAMSink_auto_in_d_bits_data)
  );
  RHEA__TracePacker_2 tracePacker ( // @[TraceFunnel.scala 110:31]
    .clock(tracePacker_clock),
    .reset(tracePacker_reset),
    .io_teActive(tracePacker_io_teActive),
    .io_btm_ready(tracePacker_io_btm_ready),
    .io_btm_valid(tracePacker_io_btm_valid),
    .io_btm_bits_length(tracePacker_io_btm_bits_length),
    .io_btm_bits_slices_0_data(tracePacker_io_btm_bits_slices_0_data),
    .io_btm_bits_slices_0_mseo(tracePacker_io_btm_bits_slices_0_mseo),
    .io_btm_bits_slices_1_data(tracePacker_io_btm_bits_slices_1_data),
    .io_btm_bits_slices_1_mseo(tracePacker_io_btm_bits_slices_1_mseo),
    .io_btm_bits_slices_2_data(tracePacker_io_btm_bits_slices_2_data),
    .io_btm_bits_slices_2_mseo(tracePacker_io_btm_bits_slices_2_mseo),
    .io_btm_bits_slices_3_data(tracePacker_io_btm_bits_slices_3_data),
    .io_btm_bits_slices_3_mseo(tracePacker_io_btm_bits_slices_3_mseo),
    .io_btm_bits_slices_4_data(tracePacker_io_btm_bits_slices_4_data),
    .io_btm_bits_slices_4_mseo(tracePacker_io_btm_bits_slices_4_mseo),
    .io_btm_bits_slices_5_data(tracePacker_io_btm_bits_slices_5_data),
    .io_btm_bits_slices_5_mseo(tracePacker_io_btm_bits_slices_5_mseo),
    .io_btm_bits_slices_6_data(tracePacker_io_btm_bits_slices_6_data),
    .io_btm_bits_slices_6_mseo(tracePacker_io_btm_bits_slices_6_mseo),
    .io_btm_bits_slices_7_data(tracePacker_io_btm_bits_slices_7_data),
    .io_btm_bits_slices_7_mseo(tracePacker_io_btm_bits_slices_7_mseo),
    .io_btm_bits_slices_8_data(tracePacker_io_btm_bits_slices_8_data),
    .io_btm_bits_slices_8_mseo(tracePacker_io_btm_bits_slices_8_mseo),
    .io_btm_bits_slices_9_data(tracePacker_io_btm_bits_slices_9_data),
    .io_btm_bits_slices_9_mseo(tracePacker_io_btm_bits_slices_9_mseo),
    .io_btm_bits_slices_10_data(tracePacker_io_btm_bits_slices_10_data),
    .io_btm_bits_slices_10_mseo(tracePacker_io_btm_bits_slices_10_mseo),
    .io_btm_bits_slices_11_data(tracePacker_io_btm_bits_slices_11_data),
    .io_btm_bits_slices_11_mseo(tracePacker_io_btm_bits_slices_11_mseo),
    .io_btm_bits_slices_12_data(tracePacker_io_btm_bits_slices_12_data),
    .io_btm_bits_slices_12_mseo(tracePacker_io_btm_bits_slices_12_mseo),
    .io_btm_bits_slices_13_data(tracePacker_io_btm_bits_slices_13_data),
    .io_btm_bits_slices_13_mseo(tracePacker_io_btm_bits_slices_13_mseo),
    .io_btm_bits_slices_14_data(tracePacker_io_btm_bits_slices_14_data),
    .io_btm_bits_slices_14_mseo(tracePacker_io_btm_bits_slices_14_mseo),
    .io_btm_bits_slices_15_data(tracePacker_io_btm_bits_slices_15_data),
    .io_btm_bits_slices_15_mseo(tracePacker_io_btm_bits_slices_15_mseo),
    .io_btm_bits_slices_16_data(tracePacker_io_btm_bits_slices_16_data),
    .io_btm_bits_slices_16_mseo(tracePacker_io_btm_bits_slices_16_mseo),
    .io_btm_bits_slices_17_data(tracePacker_io_btm_bits_slices_17_data),
    .io_btm_bits_slices_17_mseo(tracePacker_io_btm_bits_slices_17_mseo),
    .io_btm_bits_slices_18_data(tracePacker_io_btm_bits_slices_18_data),
    .io_btm_bits_slices_18_mseo(tracePacker_io_btm_bits_slices_18_mseo),
    .io_btm_bits_slices_19_data(tracePacker_io_btm_bits_slices_19_data),
    .io_btm_bits_slices_19_mseo(tracePacker_io_btm_bits_slices_19_mseo),
    .io_btm_bits_slices_20_data(tracePacker_io_btm_bits_slices_20_data),
    .io_btm_bits_slices_20_mseo(tracePacker_io_btm_bits_slices_20_mseo),
    .io_btm_bits_slices_21_data(tracePacker_io_btm_bits_slices_21_data),
    .io_btm_bits_slices_21_mseo(tracePacker_io_btm_bits_slices_21_mseo),
    .io_btm_bits_slices_22_data(tracePacker_io_btm_bits_slices_22_data),
    .io_btm_bits_slices_22_mseo(tracePacker_io_btm_bits_slices_22_mseo),
    .io_btm_bits_slices_23_data(tracePacker_io_btm_bits_slices_23_data),
    .io_btm_bits_slices_23_mseo(tracePacker_io_btm_bits_slices_23_mseo),
    .io_btm_bits_slices_24_data(tracePacker_io_btm_bits_slices_24_data),
    .io_btm_bits_slices_24_mseo(tracePacker_io_btm_bits_slices_24_mseo),
    .io_stream_ready(tracePacker_io_stream_ready),
    .io_stream_valid(tracePacker_io_stream_valid),
    .io_stream_bits_data(tracePacker_io_stream_bits_data)
  );
  RHEA__TraceGapper_2 traceGapper ( // @[TraceFunnel.scala 111:31]
    .clock(traceGapper_clock),
    .reset(traceGapper_reset),
    .io_teActive(traceGapper_io_teActive),
    .io_disableGapper(traceGapper_io_disableGapper),
    .io_streamIn_ready(traceGapper_io_streamIn_ready),
    .io_streamIn_valid(traceGapper_io_streamIn_valid),
    .io_streamIn_bits_data(traceGapper_io_streamIn_bits_data),
    .io_streamOut_ready(traceGapper_io_streamOut_ready),
    .io_streamOut_valid(traceGapper_io_streamOut_valid),
    .io_streamOut_bits_data(traceGapper_io_streamOut_bits_data),
    .io_fifoFlush(traceGapper_io_fifoFlush),
    .io_fifoEmpty(traceGapper_io_fifoEmpty)
  );
  RHEA__HellaPeekingArbiter sinkArbiter ( // @[TraceFunnel.scala 255:29]
    .clock(sinkArbiter_clock),
    .reset(sinkArbiter_reset),
    .io_in_0_ready(sinkArbiter_io_in_0_ready),
    .io_in_0_valid(sinkArbiter_io_in_0_valid),
    .io_in_0_bits_length(sinkArbiter_io_in_0_bits_length),
    .io_in_0_bits_slices_0_data(sinkArbiter_io_in_0_bits_slices_0_data),
    .io_in_0_bits_slices_0_mseo(sinkArbiter_io_in_0_bits_slices_0_mseo),
    .io_in_0_bits_slices_1_data(sinkArbiter_io_in_0_bits_slices_1_data),
    .io_in_0_bits_slices_1_mseo(sinkArbiter_io_in_0_bits_slices_1_mseo),
    .io_in_0_bits_slices_2_data(sinkArbiter_io_in_0_bits_slices_2_data),
    .io_in_0_bits_slices_2_mseo(sinkArbiter_io_in_0_bits_slices_2_mseo),
    .io_in_0_bits_slices_3_data(sinkArbiter_io_in_0_bits_slices_3_data),
    .io_in_0_bits_slices_3_mseo(sinkArbiter_io_in_0_bits_slices_3_mseo),
    .io_in_0_bits_slices_4_data(sinkArbiter_io_in_0_bits_slices_4_data),
    .io_in_0_bits_slices_4_mseo(sinkArbiter_io_in_0_bits_slices_4_mseo),
    .io_in_0_bits_slices_5_data(sinkArbiter_io_in_0_bits_slices_5_data),
    .io_in_0_bits_slices_5_mseo(sinkArbiter_io_in_0_bits_slices_5_mseo),
    .io_in_0_bits_slices_6_data(sinkArbiter_io_in_0_bits_slices_6_data),
    .io_in_0_bits_slices_6_mseo(sinkArbiter_io_in_0_bits_slices_6_mseo),
    .io_in_0_bits_slices_7_data(sinkArbiter_io_in_0_bits_slices_7_data),
    .io_in_0_bits_slices_7_mseo(sinkArbiter_io_in_0_bits_slices_7_mseo),
    .io_in_0_bits_slices_8_data(sinkArbiter_io_in_0_bits_slices_8_data),
    .io_in_0_bits_slices_8_mseo(sinkArbiter_io_in_0_bits_slices_8_mseo),
    .io_in_0_bits_slices_9_data(sinkArbiter_io_in_0_bits_slices_9_data),
    .io_in_0_bits_slices_9_mseo(sinkArbiter_io_in_0_bits_slices_9_mseo),
    .io_in_0_bits_slices_10_data(sinkArbiter_io_in_0_bits_slices_10_data),
    .io_in_0_bits_slices_10_mseo(sinkArbiter_io_in_0_bits_slices_10_mseo),
    .io_in_0_bits_slices_11_data(sinkArbiter_io_in_0_bits_slices_11_data),
    .io_in_0_bits_slices_11_mseo(sinkArbiter_io_in_0_bits_slices_11_mseo),
    .io_in_0_bits_slices_12_data(sinkArbiter_io_in_0_bits_slices_12_data),
    .io_in_0_bits_slices_12_mseo(sinkArbiter_io_in_0_bits_slices_12_mseo),
    .io_in_0_bits_slices_13_data(sinkArbiter_io_in_0_bits_slices_13_data),
    .io_in_0_bits_slices_13_mseo(sinkArbiter_io_in_0_bits_slices_13_mseo),
    .io_in_0_bits_slices_14_data(sinkArbiter_io_in_0_bits_slices_14_data),
    .io_in_0_bits_slices_14_mseo(sinkArbiter_io_in_0_bits_slices_14_mseo),
    .io_in_0_bits_slices_15_data(sinkArbiter_io_in_0_bits_slices_15_data),
    .io_in_0_bits_slices_15_mseo(sinkArbiter_io_in_0_bits_slices_15_mseo),
    .io_in_0_bits_slices_16_data(sinkArbiter_io_in_0_bits_slices_16_data),
    .io_in_0_bits_slices_16_mseo(sinkArbiter_io_in_0_bits_slices_16_mseo),
    .io_in_0_bits_slices_17_data(sinkArbiter_io_in_0_bits_slices_17_data),
    .io_in_0_bits_slices_17_mseo(sinkArbiter_io_in_0_bits_slices_17_mseo),
    .io_in_0_bits_slices_18_data(sinkArbiter_io_in_0_bits_slices_18_data),
    .io_in_0_bits_slices_18_mseo(sinkArbiter_io_in_0_bits_slices_18_mseo),
    .io_in_0_bits_slices_19_data(sinkArbiter_io_in_0_bits_slices_19_data),
    .io_in_0_bits_slices_19_mseo(sinkArbiter_io_in_0_bits_slices_19_mseo),
    .io_in_0_bits_slices_20_data(sinkArbiter_io_in_0_bits_slices_20_data),
    .io_in_0_bits_slices_20_mseo(sinkArbiter_io_in_0_bits_slices_20_mseo),
    .io_in_0_bits_slices_21_data(sinkArbiter_io_in_0_bits_slices_21_data),
    .io_in_0_bits_slices_21_mseo(sinkArbiter_io_in_0_bits_slices_21_mseo),
    .io_in_0_bits_slices_22_data(sinkArbiter_io_in_0_bits_slices_22_data),
    .io_in_0_bits_slices_22_mseo(sinkArbiter_io_in_0_bits_slices_22_mseo),
    .io_in_0_bits_slices_23_data(sinkArbiter_io_in_0_bits_slices_23_data),
    .io_in_0_bits_slices_23_mseo(sinkArbiter_io_in_0_bits_slices_23_mseo),
    .io_in_0_bits_slices_24_data(sinkArbiter_io_in_0_bits_slices_24_data),
    .io_in_0_bits_slices_24_mseo(sinkArbiter_io_in_0_bits_slices_24_mseo),
    .io_in_1_ready(sinkArbiter_io_in_1_ready),
    .io_in_1_valid(sinkArbiter_io_in_1_valid),
    .io_in_1_bits_length(sinkArbiter_io_in_1_bits_length),
    .io_in_1_bits_slices_0_data(sinkArbiter_io_in_1_bits_slices_0_data),
    .io_in_1_bits_slices_0_mseo(sinkArbiter_io_in_1_bits_slices_0_mseo),
    .io_in_1_bits_slices_1_data(sinkArbiter_io_in_1_bits_slices_1_data),
    .io_in_1_bits_slices_1_mseo(sinkArbiter_io_in_1_bits_slices_1_mseo),
    .io_in_1_bits_slices_2_data(sinkArbiter_io_in_1_bits_slices_2_data),
    .io_in_1_bits_slices_2_mseo(sinkArbiter_io_in_1_bits_slices_2_mseo),
    .io_in_1_bits_slices_3_data(sinkArbiter_io_in_1_bits_slices_3_data),
    .io_in_1_bits_slices_3_mseo(sinkArbiter_io_in_1_bits_slices_3_mseo),
    .io_in_1_bits_slices_4_data(sinkArbiter_io_in_1_bits_slices_4_data),
    .io_in_1_bits_slices_4_mseo(sinkArbiter_io_in_1_bits_slices_4_mseo),
    .io_in_1_bits_slices_5_data(sinkArbiter_io_in_1_bits_slices_5_data),
    .io_in_1_bits_slices_5_mseo(sinkArbiter_io_in_1_bits_slices_5_mseo),
    .io_in_1_bits_slices_6_data(sinkArbiter_io_in_1_bits_slices_6_data),
    .io_in_1_bits_slices_6_mseo(sinkArbiter_io_in_1_bits_slices_6_mseo),
    .io_in_1_bits_slices_7_data(sinkArbiter_io_in_1_bits_slices_7_data),
    .io_in_1_bits_slices_7_mseo(sinkArbiter_io_in_1_bits_slices_7_mseo),
    .io_in_1_bits_slices_8_data(sinkArbiter_io_in_1_bits_slices_8_data),
    .io_in_1_bits_slices_8_mseo(sinkArbiter_io_in_1_bits_slices_8_mseo),
    .io_in_1_bits_slices_9_data(sinkArbiter_io_in_1_bits_slices_9_data),
    .io_in_1_bits_slices_9_mseo(sinkArbiter_io_in_1_bits_slices_9_mseo),
    .io_in_1_bits_slices_10_data(sinkArbiter_io_in_1_bits_slices_10_data),
    .io_in_1_bits_slices_10_mseo(sinkArbiter_io_in_1_bits_slices_10_mseo),
    .io_in_1_bits_slices_11_data(sinkArbiter_io_in_1_bits_slices_11_data),
    .io_in_1_bits_slices_11_mseo(sinkArbiter_io_in_1_bits_slices_11_mseo),
    .io_in_1_bits_slices_12_data(sinkArbiter_io_in_1_bits_slices_12_data),
    .io_in_1_bits_slices_12_mseo(sinkArbiter_io_in_1_bits_slices_12_mseo),
    .io_in_1_bits_slices_13_data(sinkArbiter_io_in_1_bits_slices_13_data),
    .io_in_1_bits_slices_13_mseo(sinkArbiter_io_in_1_bits_slices_13_mseo),
    .io_in_1_bits_slices_14_data(sinkArbiter_io_in_1_bits_slices_14_data),
    .io_in_1_bits_slices_14_mseo(sinkArbiter_io_in_1_bits_slices_14_mseo),
    .io_in_1_bits_slices_15_data(sinkArbiter_io_in_1_bits_slices_15_data),
    .io_in_1_bits_slices_15_mseo(sinkArbiter_io_in_1_bits_slices_15_mseo),
    .io_in_1_bits_slices_16_data(sinkArbiter_io_in_1_bits_slices_16_data),
    .io_in_1_bits_slices_16_mseo(sinkArbiter_io_in_1_bits_slices_16_mseo),
    .io_in_1_bits_slices_17_data(sinkArbiter_io_in_1_bits_slices_17_data),
    .io_in_1_bits_slices_17_mseo(sinkArbiter_io_in_1_bits_slices_17_mseo),
    .io_in_1_bits_slices_18_data(sinkArbiter_io_in_1_bits_slices_18_data),
    .io_in_1_bits_slices_18_mseo(sinkArbiter_io_in_1_bits_slices_18_mseo),
    .io_in_1_bits_slices_19_data(sinkArbiter_io_in_1_bits_slices_19_data),
    .io_in_1_bits_slices_19_mseo(sinkArbiter_io_in_1_bits_slices_19_mseo),
    .io_in_1_bits_slices_20_data(sinkArbiter_io_in_1_bits_slices_20_data),
    .io_in_1_bits_slices_20_mseo(sinkArbiter_io_in_1_bits_slices_20_mseo),
    .io_in_1_bits_slices_21_data(sinkArbiter_io_in_1_bits_slices_21_data),
    .io_in_1_bits_slices_21_mseo(sinkArbiter_io_in_1_bits_slices_21_mseo),
    .io_in_1_bits_slices_22_data(sinkArbiter_io_in_1_bits_slices_22_data),
    .io_in_1_bits_slices_22_mseo(sinkArbiter_io_in_1_bits_slices_22_mseo),
    .io_in_1_bits_slices_23_data(sinkArbiter_io_in_1_bits_slices_23_data),
    .io_in_1_bits_slices_23_mseo(sinkArbiter_io_in_1_bits_slices_23_mseo),
    .io_in_1_bits_slices_24_data(sinkArbiter_io_in_1_bits_slices_24_data),
    .io_in_1_bits_slices_24_mseo(sinkArbiter_io_in_1_bits_slices_24_mseo),
    .io_out_ready(sinkArbiter_io_out_ready),
    .io_out_valid(sinkArbiter_io_out_valid),
    .io_out_bits_length(sinkArbiter_io_out_bits_length),
    .io_out_bits_slices_0_data(sinkArbiter_io_out_bits_slices_0_data),
    .io_out_bits_slices_0_mseo(sinkArbiter_io_out_bits_slices_0_mseo),
    .io_out_bits_slices_1_data(sinkArbiter_io_out_bits_slices_1_data),
    .io_out_bits_slices_1_mseo(sinkArbiter_io_out_bits_slices_1_mseo),
    .io_out_bits_slices_2_data(sinkArbiter_io_out_bits_slices_2_data),
    .io_out_bits_slices_2_mseo(sinkArbiter_io_out_bits_slices_2_mseo),
    .io_out_bits_slices_3_data(sinkArbiter_io_out_bits_slices_3_data),
    .io_out_bits_slices_3_mseo(sinkArbiter_io_out_bits_slices_3_mseo),
    .io_out_bits_slices_4_data(sinkArbiter_io_out_bits_slices_4_data),
    .io_out_bits_slices_4_mseo(sinkArbiter_io_out_bits_slices_4_mseo),
    .io_out_bits_slices_5_data(sinkArbiter_io_out_bits_slices_5_data),
    .io_out_bits_slices_5_mseo(sinkArbiter_io_out_bits_slices_5_mseo),
    .io_out_bits_slices_6_data(sinkArbiter_io_out_bits_slices_6_data),
    .io_out_bits_slices_6_mseo(sinkArbiter_io_out_bits_slices_6_mseo),
    .io_out_bits_slices_7_data(sinkArbiter_io_out_bits_slices_7_data),
    .io_out_bits_slices_7_mseo(sinkArbiter_io_out_bits_slices_7_mseo),
    .io_out_bits_slices_8_data(sinkArbiter_io_out_bits_slices_8_data),
    .io_out_bits_slices_8_mseo(sinkArbiter_io_out_bits_slices_8_mseo),
    .io_out_bits_slices_9_data(sinkArbiter_io_out_bits_slices_9_data),
    .io_out_bits_slices_9_mseo(sinkArbiter_io_out_bits_slices_9_mseo),
    .io_out_bits_slices_10_data(sinkArbiter_io_out_bits_slices_10_data),
    .io_out_bits_slices_10_mseo(sinkArbiter_io_out_bits_slices_10_mseo),
    .io_out_bits_slices_11_data(sinkArbiter_io_out_bits_slices_11_data),
    .io_out_bits_slices_11_mseo(sinkArbiter_io_out_bits_slices_11_mseo),
    .io_out_bits_slices_12_data(sinkArbiter_io_out_bits_slices_12_data),
    .io_out_bits_slices_12_mseo(sinkArbiter_io_out_bits_slices_12_mseo),
    .io_out_bits_slices_13_data(sinkArbiter_io_out_bits_slices_13_data),
    .io_out_bits_slices_13_mseo(sinkArbiter_io_out_bits_slices_13_mseo),
    .io_out_bits_slices_14_data(sinkArbiter_io_out_bits_slices_14_data),
    .io_out_bits_slices_14_mseo(sinkArbiter_io_out_bits_slices_14_mseo),
    .io_out_bits_slices_15_data(sinkArbiter_io_out_bits_slices_15_data),
    .io_out_bits_slices_15_mseo(sinkArbiter_io_out_bits_slices_15_mseo),
    .io_out_bits_slices_16_data(sinkArbiter_io_out_bits_slices_16_data),
    .io_out_bits_slices_16_mseo(sinkArbiter_io_out_bits_slices_16_mseo),
    .io_out_bits_slices_17_data(sinkArbiter_io_out_bits_slices_17_data),
    .io_out_bits_slices_17_mseo(sinkArbiter_io_out_bits_slices_17_mseo),
    .io_out_bits_slices_18_data(sinkArbiter_io_out_bits_slices_18_data),
    .io_out_bits_slices_18_mseo(sinkArbiter_io_out_bits_slices_18_mseo),
    .io_out_bits_slices_19_data(sinkArbiter_io_out_bits_slices_19_data),
    .io_out_bits_slices_19_mseo(sinkArbiter_io_out_bits_slices_19_mseo),
    .io_out_bits_slices_20_data(sinkArbiter_io_out_bits_slices_20_data),
    .io_out_bits_slices_20_mseo(sinkArbiter_io_out_bits_slices_20_mseo),
    .io_out_bits_slices_21_data(sinkArbiter_io_out_bits_slices_21_data),
    .io_out_bits_slices_21_mseo(sinkArbiter_io_out_bits_slices_21_mseo),
    .io_out_bits_slices_22_data(sinkArbiter_io_out_bits_slices_22_data),
    .io_out_bits_slices_22_mseo(sinkArbiter_io_out_bits_slices_22_mseo),
    .io_out_bits_slices_23_data(sinkArbiter_io_out_bits_slices_23_data),
    .io_out_bits_slices_23_mseo(sinkArbiter_io_out_bits_slices_23_mseo),
    .io_out_bits_slices_24_data(sinkArbiter_io_out_bits_slices_24_data),
    .io_out_bits_slices_24_mseo(sinkArbiter_io_out_bits_slices_24_mseo)
  );
  RHEA__TraceUnpacker traceUnpacker ( // @[TraceFunnel.scala 270:33]
    .rf_reset(traceUnpacker_rf_reset),
    .clock(traceUnpacker_clock),
    .reset(traceUnpacker_reset),
    .io_teActive(traceUnpacker_io_teActive),
    .io_stream_ready(traceUnpacker_io_stream_ready),
    .io_stream_valid(traceUnpacker_io_stream_valid),
    .io_stream_bits(traceUnpacker_io_stream_bits),
    .io_btm_ready(traceUnpacker_io_btm_ready),
    .io_btm_valid(traceUnpacker_io_btm_valid),
    .io_btm_bits_length(traceUnpacker_io_btm_bits_length),
    .io_btm_bits_slices_0_data(traceUnpacker_io_btm_bits_slices_0_data),
    .io_btm_bits_slices_0_mseo(traceUnpacker_io_btm_bits_slices_0_mseo),
    .io_btm_bits_slices_1_data(traceUnpacker_io_btm_bits_slices_1_data),
    .io_btm_bits_slices_1_mseo(traceUnpacker_io_btm_bits_slices_1_mseo),
    .io_btm_bits_slices_2_data(traceUnpacker_io_btm_bits_slices_2_data),
    .io_btm_bits_slices_2_mseo(traceUnpacker_io_btm_bits_slices_2_mseo),
    .io_btm_bits_slices_3_data(traceUnpacker_io_btm_bits_slices_3_data),
    .io_btm_bits_slices_3_mseo(traceUnpacker_io_btm_bits_slices_3_mseo),
    .io_btm_bits_slices_4_data(traceUnpacker_io_btm_bits_slices_4_data),
    .io_btm_bits_slices_4_mseo(traceUnpacker_io_btm_bits_slices_4_mseo),
    .io_btm_bits_slices_5_data(traceUnpacker_io_btm_bits_slices_5_data),
    .io_btm_bits_slices_5_mseo(traceUnpacker_io_btm_bits_slices_5_mseo),
    .io_btm_bits_slices_6_data(traceUnpacker_io_btm_bits_slices_6_data),
    .io_btm_bits_slices_6_mseo(traceUnpacker_io_btm_bits_slices_6_mseo),
    .io_btm_bits_slices_7_data(traceUnpacker_io_btm_bits_slices_7_data),
    .io_btm_bits_slices_7_mseo(traceUnpacker_io_btm_bits_slices_7_mseo),
    .io_btm_bits_slices_8_data(traceUnpacker_io_btm_bits_slices_8_data),
    .io_btm_bits_slices_8_mseo(traceUnpacker_io_btm_bits_slices_8_mseo),
    .io_btm_bits_slices_9_data(traceUnpacker_io_btm_bits_slices_9_data),
    .io_btm_bits_slices_9_mseo(traceUnpacker_io_btm_bits_slices_9_mseo),
    .io_btm_bits_slices_10_data(traceUnpacker_io_btm_bits_slices_10_data),
    .io_btm_bits_slices_10_mseo(traceUnpacker_io_btm_bits_slices_10_mseo),
    .io_btm_bits_slices_11_data(traceUnpacker_io_btm_bits_slices_11_data),
    .io_btm_bits_slices_11_mseo(traceUnpacker_io_btm_bits_slices_11_mseo),
    .io_btm_bits_slices_12_data(traceUnpacker_io_btm_bits_slices_12_data),
    .io_btm_bits_slices_12_mseo(traceUnpacker_io_btm_bits_slices_12_mseo),
    .io_btm_bits_slices_13_data(traceUnpacker_io_btm_bits_slices_13_data),
    .io_btm_bits_slices_13_mseo(traceUnpacker_io_btm_bits_slices_13_mseo),
    .io_btm_bits_slices_14_data(traceUnpacker_io_btm_bits_slices_14_data),
    .io_btm_bits_slices_14_mseo(traceUnpacker_io_btm_bits_slices_14_mseo),
    .io_btm_bits_slices_15_data(traceUnpacker_io_btm_bits_slices_15_data),
    .io_btm_bits_slices_15_mseo(traceUnpacker_io_btm_bits_slices_15_mseo),
    .io_btm_bits_slices_16_data(traceUnpacker_io_btm_bits_slices_16_data),
    .io_btm_bits_slices_16_mseo(traceUnpacker_io_btm_bits_slices_16_mseo),
    .io_btm_bits_slices_17_data(traceUnpacker_io_btm_bits_slices_17_data),
    .io_btm_bits_slices_17_mseo(traceUnpacker_io_btm_bits_slices_17_mseo),
    .io_btm_bits_slices_18_data(traceUnpacker_io_btm_bits_slices_18_data),
    .io_btm_bits_slices_18_mseo(traceUnpacker_io_btm_bits_slices_18_mseo),
    .io_btm_bits_slices_19_data(traceUnpacker_io_btm_bits_slices_19_data),
    .io_btm_bits_slices_19_mseo(traceUnpacker_io_btm_bits_slices_19_mseo),
    .io_btm_bits_slices_20_data(traceUnpacker_io_btm_bits_slices_20_data),
    .io_btm_bits_slices_20_mseo(traceUnpacker_io_btm_bits_slices_20_mseo),
    .io_btm_bits_slices_21_data(traceUnpacker_io_btm_bits_slices_21_data),
    .io_btm_bits_slices_21_mseo(traceUnpacker_io_btm_bits_slices_21_mseo),
    .io_btm_bits_slices_22_data(traceUnpacker_io_btm_bits_slices_22_data),
    .io_btm_bits_slices_22_mseo(traceUnpacker_io_btm_bits_slices_22_mseo),
    .io_btm_bits_slices_23_data(traceUnpacker_io_btm_bits_slices_23_data),
    .io_btm_bits_slices_23_mseo(traceUnpacker_io_btm_bits_slices_23_mseo),
    .io_btm_bits_slices_24_data(traceUnpacker_io_btm_bits_slices_24_data),
    .io_btm_bits_slices_24_mseo(traceUnpacker_io_btm_bits_slices_24_mseo)
  );
  RHEA__TraceUnpacker traceUnpacker_1 ( // @[TraceFunnel.scala 270:33]
    .rf_reset(traceUnpacker_1_rf_reset),
    .clock(traceUnpacker_1_clock),
    .reset(traceUnpacker_1_reset),
    .io_teActive(traceUnpacker_1_io_teActive),
    .io_stream_ready(traceUnpacker_1_io_stream_ready),
    .io_stream_valid(traceUnpacker_1_io_stream_valid),
    .io_stream_bits(traceUnpacker_1_io_stream_bits),
    .io_btm_ready(traceUnpacker_1_io_btm_ready),
    .io_btm_valid(traceUnpacker_1_io_btm_valid),
    .io_btm_bits_length(traceUnpacker_1_io_btm_bits_length),
    .io_btm_bits_slices_0_data(traceUnpacker_1_io_btm_bits_slices_0_data),
    .io_btm_bits_slices_0_mseo(traceUnpacker_1_io_btm_bits_slices_0_mseo),
    .io_btm_bits_slices_1_data(traceUnpacker_1_io_btm_bits_slices_1_data),
    .io_btm_bits_slices_1_mseo(traceUnpacker_1_io_btm_bits_slices_1_mseo),
    .io_btm_bits_slices_2_data(traceUnpacker_1_io_btm_bits_slices_2_data),
    .io_btm_bits_slices_2_mseo(traceUnpacker_1_io_btm_bits_slices_2_mseo),
    .io_btm_bits_slices_3_data(traceUnpacker_1_io_btm_bits_slices_3_data),
    .io_btm_bits_slices_3_mseo(traceUnpacker_1_io_btm_bits_slices_3_mseo),
    .io_btm_bits_slices_4_data(traceUnpacker_1_io_btm_bits_slices_4_data),
    .io_btm_bits_slices_4_mseo(traceUnpacker_1_io_btm_bits_slices_4_mseo),
    .io_btm_bits_slices_5_data(traceUnpacker_1_io_btm_bits_slices_5_data),
    .io_btm_bits_slices_5_mseo(traceUnpacker_1_io_btm_bits_slices_5_mseo),
    .io_btm_bits_slices_6_data(traceUnpacker_1_io_btm_bits_slices_6_data),
    .io_btm_bits_slices_6_mseo(traceUnpacker_1_io_btm_bits_slices_6_mseo),
    .io_btm_bits_slices_7_data(traceUnpacker_1_io_btm_bits_slices_7_data),
    .io_btm_bits_slices_7_mseo(traceUnpacker_1_io_btm_bits_slices_7_mseo),
    .io_btm_bits_slices_8_data(traceUnpacker_1_io_btm_bits_slices_8_data),
    .io_btm_bits_slices_8_mseo(traceUnpacker_1_io_btm_bits_slices_8_mseo),
    .io_btm_bits_slices_9_data(traceUnpacker_1_io_btm_bits_slices_9_data),
    .io_btm_bits_slices_9_mseo(traceUnpacker_1_io_btm_bits_slices_9_mseo),
    .io_btm_bits_slices_10_data(traceUnpacker_1_io_btm_bits_slices_10_data),
    .io_btm_bits_slices_10_mseo(traceUnpacker_1_io_btm_bits_slices_10_mseo),
    .io_btm_bits_slices_11_data(traceUnpacker_1_io_btm_bits_slices_11_data),
    .io_btm_bits_slices_11_mseo(traceUnpacker_1_io_btm_bits_slices_11_mseo),
    .io_btm_bits_slices_12_data(traceUnpacker_1_io_btm_bits_slices_12_data),
    .io_btm_bits_slices_12_mseo(traceUnpacker_1_io_btm_bits_slices_12_mseo),
    .io_btm_bits_slices_13_data(traceUnpacker_1_io_btm_bits_slices_13_data),
    .io_btm_bits_slices_13_mseo(traceUnpacker_1_io_btm_bits_slices_13_mseo),
    .io_btm_bits_slices_14_data(traceUnpacker_1_io_btm_bits_slices_14_data),
    .io_btm_bits_slices_14_mseo(traceUnpacker_1_io_btm_bits_slices_14_mseo),
    .io_btm_bits_slices_15_data(traceUnpacker_1_io_btm_bits_slices_15_data),
    .io_btm_bits_slices_15_mseo(traceUnpacker_1_io_btm_bits_slices_15_mseo),
    .io_btm_bits_slices_16_data(traceUnpacker_1_io_btm_bits_slices_16_data),
    .io_btm_bits_slices_16_mseo(traceUnpacker_1_io_btm_bits_slices_16_mseo),
    .io_btm_bits_slices_17_data(traceUnpacker_1_io_btm_bits_slices_17_data),
    .io_btm_bits_slices_17_mseo(traceUnpacker_1_io_btm_bits_slices_17_mseo),
    .io_btm_bits_slices_18_data(traceUnpacker_1_io_btm_bits_slices_18_data),
    .io_btm_bits_slices_18_mseo(traceUnpacker_1_io_btm_bits_slices_18_mseo),
    .io_btm_bits_slices_19_data(traceUnpacker_1_io_btm_bits_slices_19_data),
    .io_btm_bits_slices_19_mseo(traceUnpacker_1_io_btm_bits_slices_19_mseo),
    .io_btm_bits_slices_20_data(traceUnpacker_1_io_btm_bits_slices_20_data),
    .io_btm_bits_slices_20_mseo(traceUnpacker_1_io_btm_bits_slices_20_mseo),
    .io_btm_bits_slices_21_data(traceUnpacker_1_io_btm_bits_slices_21_data),
    .io_btm_bits_slices_21_mseo(traceUnpacker_1_io_btm_bits_slices_21_mseo),
    .io_btm_bits_slices_22_data(traceUnpacker_1_io_btm_bits_slices_22_data),
    .io_btm_bits_slices_22_mseo(traceUnpacker_1_io_btm_bits_slices_22_mseo),
    .io_btm_bits_slices_23_data(traceUnpacker_1_io_btm_bits_slices_23_data),
    .io_btm_bits_slices_23_mseo(traceUnpacker_1_io_btm_bits_slices_23_mseo),
    .io_btm_bits_slices_24_data(traceUnpacker_1_io_btm_bits_slices_24_data),
    .io_btm_bits_slices_24_mseo(traceUnpacker_1_io_btm_bits_slices_24_mseo)
  );
  RHEA__TracePIBOuter pibOuter ( // @[TraceFunnel.scala 295:48]
    .clock(pibOuter_clock),
    .reset(pibOuter_reset),
    .io_pibControlSync_pibDivider(pibOuter_io_pibControlSync_pibDivider),
    .io_pibControlSync_pibCalibrate(pibOuter_io_pibControlSync_pibCalibrate),
    .io_pibControlSync_pibRefCenter(pibOuter_io_pibControlSync_pibRefCenter),
    .io_pibControlSync_pibMode(pibOuter_io_pibControlSync_pibMode),
    .io_pibControlSync_pibEnable(pibOuter_io_pibControlSync_pibEnable),
    .io_pibControlSync_pibActive(pibOuter_io_pibControlSync_pibActive),
    .io_streamIn_ready(pibOuter_io_streamIn_ready),
    .io_streamIn_valid(pibOuter_io_streamIn_valid),
    .io_streamIn_bits(pibOuter_io_streamIn_bits),
    .io_pib_tref(pibOuter_io_pib_tref),
    .io_pib_tdata(pibOuter_io_pib_tdata),
    .io_pibEmpty(pibOuter_io_pibEmpty)
  );
  assign tracePIB_rf_reset = rf_reset;
  assign teSinkXbar_rf_reset = rf_reset;
  assign teSRAMSink_rf_reset = rf_reset;
  assign traceUnpacker_rf_reset = rf_reset;
  assign traceUnpacker_1_rf_reset = rf_reset;
  assign rf_reset = reset;
  assign auto_traceSink_sba_out_a_valid = traceSink_auto_sba_out_a_valid; // @[LazyModule.scala 390:12]
  assign auto_traceSink_sba_out_a_bits_source = traceSink_auto_sba_out_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_traceSink_sba_out_a_bits_address = traceSink_auto_sba_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_traceSink_sba_out_a_bits_data = traceSink_auto_sba_out_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_sink_in_1_a_ready = _sinkReady_T_4 | _sinkReady_T_2; // @[TraceFunnel.scala 263:66]
  assign auto_sink_in_1_d_valid = auto_sink_in_1_a_valid & sinkReady_1; // @[TraceFunnel.scala 267:32]
  assign auto_sink_in_1_d_bits_denied = bundleIn_0_2_d_valid & _denied_T_4; // @[TraceFunnel.scala 268:28]
  assign auto_sink_in_0_a_ready = _sinkReady_T_1 | _sinkReady_T_2; // @[TraceFunnel.scala 263:66]
  assign auto_sink_in_0_d_valid = auto_sink_in_0_a_valid & sinkReady; // @[TraceFunnel.scala 267:32]
  assign auto_sink_in_0_d_bits_denied = bundleIn_0_1_d_valid & _denied_T_4; // @[TraceFunnel.scala 268:28]
  assign auto_reg_in_a_ready = auto_reg_in_d_ready & out_oready; // @[TraceFunnel.scala 49:31]
  assign auto_reg_in_d_valid = auto_reg_in_a_valid & out_oready; // @[TraceFunnel.scala 49:31]
  assign auto_reg_in_d_bits_opcode = {{2'd0}, in_bits_read}; // @[TraceFunnel.scala 49:31 TraceFunnel.scala 49:31]
  assign auto_reg_in_d_bits_size = auto_reg_in_a_bits_size; // @[TraceFunnel.scala 49:31 LazyModule.scala 388:16]
  assign auto_reg_in_d_bits_source = auto_reg_in_a_bits_source; // @[TraceFunnel.scala 49:31 LazyModule.scala 388:16]
  assign auto_reg_in_d_bits_data = out_out_bits_data_out ? out_out_bits_data_out_1 : 64'h0; // @[TraceFunnel.scala 49:31]
  assign io_pib_tref = pibOuter_io_pib_tref; // @[TraceFunnel.scala 303:18]
  assign io_pib_tdata = pibOuter_io_pib_tdata; // @[TraceFunnel.scala 303:18]
  assign traceSink_clock = clock;
  assign traceSink_reset = reset;
  assign traceSink_auto_sba_out_a_ready = auto_traceSink_sba_out_a_ready; // @[LazyModule.scala 390:12]
  assign traceSink_auto_sba_out_d_valid = auto_traceSink_sba_out_d_valid; // @[LazyModule.scala 390:12]
  assign traceSink_auto_sba_out_d_bits_source = auto_traceSink_sba_out_d_bits_source; // @[LazyModule.scala 390:12]
  assign traceSink_auto_sba_out_d_bits_denied = auto_traceSink_sba_out_d_bits_denied; // @[LazyModule.scala 390:12]
  assign traceSink_auto_sink_out_a_ready = teSinkXbar_auto_in_a_ready; // @[LazyModule.scala 377:16]
  assign traceSink_auto_sink_out_d_valid = teSinkXbar_auto_in_d_valid; // @[LazyModule.scala 377:16]
  assign traceSink_auto_sink_out_d_bits_denied = teSinkXbar_auto_in_d_bits_denied; // @[LazyModule.scala 377:16]
  assign traceSink_auto_sink_out_d_bits_data = teSinkXbar_auto_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign traceSink_io_teActive = teControlReg_teActive; // @[TraceFunnel.scala 289:34]
  assign traceSink_io_teControl_teSink = teControlReg_teSink; // @[TraceFunnel.scala 290:35]
  assign traceSink_io_teControl_teStopOnWrap = teControlReg_teStopOnWrap; // @[TraceFunnel.scala 290:35]
  assign traceSink_io_stream_valid = traceGapper_io_streamOut_valid; // @[TraceFunnel.scala 291:32]
  assign traceSink_io_stream_bits_data = traceGapper_io_streamOut_bits_data; // @[TraceFunnel.scala 291:32]
  assign traceSink_io_streamBaseH = teSinkBaseHReg_teSinkBaseH; // @[TraceSink.scala 73:40]
  assign traceSink_io_streamWP = teSinkWPReg_teSinkWP; // @[TraceSink.scala 200:35]
  assign traceSink_io_streamWPWrap = teSinkWPReg_teSinkWrapped; // @[TraceSink.scala 201:35]
  assign traceSink_io_busWrEn = usingSRAM & out_f_woready_36; // @[TraceSink.scala 134:50]
  assign traceSink_io_busRdEn = usingSRAM & out_f_roready_36; // @[TraceSink.scala 135:50]
  assign traceSink_io_busAP = teSinkRPReg_teSinkRP; // @[TraceSink.scala 136:37]
  assign traceSink_io_busWrData = out_f_woready_36 ? auto_reg_in_a_bits_data[63:32] : 32'h0; // @[TraceEncoder.scala 186:24 TraceEncoder.scala 186:30]
  assign tracePIB_clock = clock;
  assign tracePIB_reset = reset;
  assign tracePIB_auto_sink_in_a_valid = teSinkXbar_auto_out_0_a_valid; // @[LazyModule.scala 375:16]
  assign tracePIB_auto_sink_in_a_bits_opcode = teSinkXbar_auto_out_0_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign tracePIB_auto_sink_in_a_bits_source = teSinkXbar_auto_out_0_a_bits_source; // @[LazyModule.scala 375:16]
  assign tracePIB_auto_sink_in_a_bits_address = teSinkXbar_auto_out_0_a_bits_address; // @[LazyModule.scala 375:16]
  assign tracePIB_auto_sink_in_a_bits_data = teSinkXbar_auto_out_0_a_bits_data; // @[LazyModule.scala 375:16]
  assign tracePIB_auto_sink_in_d_ready = teSinkXbar_auto_out_0_d_ready; // @[LazyModule.scala 375:16]
  assign tracePIB_io_pibControl_bits_reserved0 = 3'h0; // @[TracePIB.scala 77:35]
  assign tracePIB_io_pibControl_bits_pibDivider = pibControlReg_pibDivider; // @[TracePIB.scala 77:35]
  assign tracePIB_io_pibControl_bits_reserved1 = 6'h0; // @[TracePIB.scala 77:35]
  assign tracePIB_io_pibControl_bits_pibCalibrate = pibControlReg_pibCalibrate; // @[TracePIB.scala 77:35]
  assign tracePIB_io_pibControl_bits_pibRefCenter = pibControlReg_pibRefCenter; // @[TracePIB.scala 77:35]
  assign tracePIB_io_pibControl_bits_pibMode = pibControlReg_pibMode; // @[TracePIB.scala 77:35]
  assign tracePIB_io_pibControl_bits_reserved2 = 2'h0; // @[TracePIB.scala 77:35]
  assign tracePIB_io_pibControl_bits_pibEnable = pibControlReg_pibEnable; // @[TracePIB.scala 77:35]
  assign tracePIB_io_pibControl_bits_pibActive = pibControlReg_pibActive; // @[TracePIB.scala 77:35]
  assign tracePIB_io_streamOut_ready = pibOuter_io_streamIn_ready; // @[TraceFunnel.scala 302:32]
  assign teSinkXbar_clock = clock;
  assign teSinkXbar_reset = reset;
  assign teSinkXbar_auto_in_a_valid = traceSink_auto_sink_out_a_valid; // @[LazyModule.scala 377:16]
  assign teSinkXbar_auto_in_a_bits_opcode = traceSink_auto_sink_out_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign teSinkXbar_auto_in_a_bits_source = traceSink_auto_sink_out_a_bits_source; // @[LazyModule.scala 377:16]
  assign teSinkXbar_auto_in_a_bits_address = traceSink_auto_sink_out_a_bits_address; // @[LazyModule.scala 377:16]
  assign teSinkXbar_auto_in_a_bits_data = traceSink_auto_sink_out_a_bits_data; // @[LazyModule.scala 377:16]
  assign teSinkXbar_auto_out_1_a_ready = teSRAMSink_auto_in_a_ready; // @[LazyModule.scala 377:16]
  assign teSinkXbar_auto_out_1_d_valid = teSRAMSink_auto_in_d_valid; // @[LazyModule.scala 377:16]
  assign teSinkXbar_auto_out_1_d_bits_opcode = teSRAMSink_auto_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign teSinkXbar_auto_out_1_d_bits_source = teSRAMSink_auto_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign teSinkXbar_auto_out_1_d_bits_data = teSRAMSink_auto_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign teSinkXbar_auto_out_0_a_ready = tracePIB_auto_sink_in_a_ready; // @[LazyModule.scala 375:16]
  assign teSinkXbar_auto_out_0_d_valid = tracePIB_auto_sink_in_d_valid; // @[LazyModule.scala 375:16]
  assign teSinkXbar_auto_out_0_d_bits_source = tracePIB_auto_sink_in_d_bits_source; // @[LazyModule.scala 375:16]
  assign teSinkXbar_auto_out_0_d_bits_denied = tracePIB_auto_sink_in_d_bits_denied; // @[LazyModule.scala 375:16]
  assign teSRAMSink_clock = clock;
  assign teSRAMSink_reset = reset;
  assign teSRAMSink_auto_in_a_valid = teSinkXbar_auto_out_1_a_valid; // @[LazyModule.scala 377:16]
  assign teSRAMSink_auto_in_a_bits_opcode = teSinkXbar_auto_out_1_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign teSRAMSink_auto_in_a_bits_source = teSinkXbar_auto_out_1_a_bits_source; // @[LazyModule.scala 377:16]
  assign teSRAMSink_auto_in_a_bits_address = teSinkXbar_auto_out_1_a_bits_address; // @[LazyModule.scala 377:16]
  assign teSRAMSink_auto_in_a_bits_data = teSinkXbar_auto_out_1_a_bits_data; // @[LazyModule.scala 377:16]
  assign teSRAMSink_auto_in_d_ready = teSinkXbar_auto_out_1_d_ready; // @[LazyModule.scala 377:16]
  assign tracePacker_clock = clock;
  assign tracePacker_reset = reset;
  assign tracePacker_io_teActive = teControlReg_teActive; // @[TraceFunnel.scala 277:29]
  assign tracePacker_io_btm_valid = sinkArbiter_io_out_valid; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_length = sinkArbiter_io_out_bits_length; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_0_data = sinkArbiter_io_out_bits_slices_0_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_0_mseo = sinkArbiter_io_out_bits_slices_0_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_1_data = sinkArbiter_io_out_bits_slices_1_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_1_mseo = sinkArbiter_io_out_bits_slices_1_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_2_data = sinkArbiter_io_out_bits_slices_2_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_2_mseo = sinkArbiter_io_out_bits_slices_2_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_3_data = sinkArbiter_io_out_bits_slices_3_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_3_mseo = sinkArbiter_io_out_bits_slices_3_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_4_data = sinkArbiter_io_out_bits_slices_4_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_4_mseo = sinkArbiter_io_out_bits_slices_4_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_5_data = sinkArbiter_io_out_bits_slices_5_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_5_mseo = sinkArbiter_io_out_bits_slices_5_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_6_data = sinkArbiter_io_out_bits_slices_6_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_6_mseo = sinkArbiter_io_out_bits_slices_6_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_7_data = sinkArbiter_io_out_bits_slices_7_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_7_mseo = sinkArbiter_io_out_bits_slices_7_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_8_data = sinkArbiter_io_out_bits_slices_8_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_8_mseo = sinkArbiter_io_out_bits_slices_8_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_9_data = sinkArbiter_io_out_bits_slices_9_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_9_mseo = sinkArbiter_io_out_bits_slices_9_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_10_data = sinkArbiter_io_out_bits_slices_10_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_10_mseo = sinkArbiter_io_out_bits_slices_10_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_11_data = sinkArbiter_io_out_bits_slices_11_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_11_mseo = sinkArbiter_io_out_bits_slices_11_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_12_data = sinkArbiter_io_out_bits_slices_12_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_12_mseo = sinkArbiter_io_out_bits_slices_12_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_13_data = sinkArbiter_io_out_bits_slices_13_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_13_mseo = sinkArbiter_io_out_bits_slices_13_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_14_data = sinkArbiter_io_out_bits_slices_14_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_14_mseo = sinkArbiter_io_out_bits_slices_14_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_15_data = sinkArbiter_io_out_bits_slices_15_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_15_mseo = sinkArbiter_io_out_bits_slices_15_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_16_data = sinkArbiter_io_out_bits_slices_16_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_16_mseo = sinkArbiter_io_out_bits_slices_16_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_17_data = sinkArbiter_io_out_bits_slices_17_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_17_mseo = sinkArbiter_io_out_bits_slices_17_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_18_data = sinkArbiter_io_out_bits_slices_18_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_18_mseo = sinkArbiter_io_out_bits_slices_18_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_19_data = sinkArbiter_io_out_bits_slices_19_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_19_mseo = sinkArbiter_io_out_bits_slices_19_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_20_data = sinkArbiter_io_out_bits_slices_20_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_20_mseo = sinkArbiter_io_out_bits_slices_20_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_21_data = sinkArbiter_io_out_bits_slices_21_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_21_mseo = sinkArbiter_io_out_bits_slices_21_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_22_data = sinkArbiter_io_out_bits_slices_22_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_22_mseo = sinkArbiter_io_out_bits_slices_22_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_23_data = sinkArbiter_io_out_bits_slices_23_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_23_mseo = sinkArbiter_io_out_bits_slices_23_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_24_data = sinkArbiter_io_out_bits_slices_24_data; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_btm_bits_slices_24_mseo = sinkArbiter_io_out_bits_slices_24_mseo; // @[TraceFunnel.scala 278:24]
  assign tracePacker_io_stream_ready = traceGapper_io_streamIn_ready; // @[TraceFunnel.scala 287:29]
  assign traceGapper_clock = clock;
  assign traceGapper_reset = reset;
  assign traceGapper_io_teActive = teControlReg_teActive; // @[TraceFunnel.scala 281:29]
  assign traceGapper_io_disableGapper = teControlReg_teSink == 4'h8 | _T_18; // @[TraceFunnel.scala 285:60]
  assign traceGapper_io_streamIn_valid = tracePacker_io_stream_valid; // @[TraceFunnel.scala 287:29]
  assign traceGapper_io_streamIn_bits_data = tracePacker_io_stream_bits_data; // @[TraceFunnel.scala 287:29]
  assign traceGapper_io_streamOut_ready = traceSink_io_stream_ready; // @[TraceFunnel.scala 291:32]
  assign traceGapper_io_fifoFlush = ~teControlReg_teEnable; // @[TraceFunnel.scala 286:33]
  assign sinkArbiter_clock = clock;
  assign sinkArbiter_reset = reset;
  assign sinkArbiter_io_in_0_valid = traceUnpacker_io_btm_valid; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_length = traceUnpacker_io_btm_bits_length; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_0_data = traceUnpacker_io_btm_bits_slices_0_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_0_mseo = traceUnpacker_io_btm_bits_slices_0_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_1_data = traceUnpacker_io_btm_bits_slices_1_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_1_mseo = traceUnpacker_io_btm_bits_slices_1_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_2_data = traceUnpacker_io_btm_bits_slices_2_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_2_mseo = traceUnpacker_io_btm_bits_slices_2_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_3_data = traceUnpacker_io_btm_bits_slices_3_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_3_mseo = traceUnpacker_io_btm_bits_slices_3_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_4_data = traceUnpacker_io_btm_bits_slices_4_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_4_mseo = traceUnpacker_io_btm_bits_slices_4_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_5_data = traceUnpacker_io_btm_bits_slices_5_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_5_mseo = traceUnpacker_io_btm_bits_slices_5_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_6_data = traceUnpacker_io_btm_bits_slices_6_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_6_mseo = traceUnpacker_io_btm_bits_slices_6_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_7_data = traceUnpacker_io_btm_bits_slices_7_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_7_mseo = traceUnpacker_io_btm_bits_slices_7_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_8_data = traceUnpacker_io_btm_bits_slices_8_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_8_mseo = traceUnpacker_io_btm_bits_slices_8_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_9_data = traceUnpacker_io_btm_bits_slices_9_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_9_mseo = traceUnpacker_io_btm_bits_slices_9_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_10_data = traceUnpacker_io_btm_bits_slices_10_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_10_mseo = traceUnpacker_io_btm_bits_slices_10_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_11_data = traceUnpacker_io_btm_bits_slices_11_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_11_mseo = traceUnpacker_io_btm_bits_slices_11_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_12_data = traceUnpacker_io_btm_bits_slices_12_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_12_mseo = traceUnpacker_io_btm_bits_slices_12_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_13_data = traceUnpacker_io_btm_bits_slices_13_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_13_mseo = traceUnpacker_io_btm_bits_slices_13_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_14_data = traceUnpacker_io_btm_bits_slices_14_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_14_mseo = traceUnpacker_io_btm_bits_slices_14_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_15_data = traceUnpacker_io_btm_bits_slices_15_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_15_mseo = traceUnpacker_io_btm_bits_slices_15_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_16_data = traceUnpacker_io_btm_bits_slices_16_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_16_mseo = traceUnpacker_io_btm_bits_slices_16_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_17_data = traceUnpacker_io_btm_bits_slices_17_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_17_mseo = traceUnpacker_io_btm_bits_slices_17_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_18_data = traceUnpacker_io_btm_bits_slices_18_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_18_mseo = traceUnpacker_io_btm_bits_slices_18_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_19_data = traceUnpacker_io_btm_bits_slices_19_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_19_mseo = traceUnpacker_io_btm_bits_slices_19_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_20_data = traceUnpacker_io_btm_bits_slices_20_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_20_mseo = traceUnpacker_io_btm_bits_slices_20_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_21_data = traceUnpacker_io_btm_bits_slices_21_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_21_mseo = traceUnpacker_io_btm_bits_slices_21_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_22_data = traceUnpacker_io_btm_bits_slices_22_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_22_mseo = traceUnpacker_io_btm_bits_slices_22_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_23_data = traceUnpacker_io_btm_bits_slices_23_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_23_mseo = traceUnpacker_io_btm_bits_slices_23_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_24_data = traceUnpacker_io_btm_bits_slices_24_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_0_bits_slices_24_mseo = traceUnpacker_io_btm_bits_slices_24_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_valid = traceUnpacker_1_io_btm_valid; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_length = traceUnpacker_1_io_btm_bits_length; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_0_data = traceUnpacker_1_io_btm_bits_slices_0_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_0_mseo = traceUnpacker_1_io_btm_bits_slices_0_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_1_data = traceUnpacker_1_io_btm_bits_slices_1_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_1_mseo = traceUnpacker_1_io_btm_bits_slices_1_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_2_data = traceUnpacker_1_io_btm_bits_slices_2_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_2_mseo = traceUnpacker_1_io_btm_bits_slices_2_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_3_data = traceUnpacker_1_io_btm_bits_slices_3_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_3_mseo = traceUnpacker_1_io_btm_bits_slices_3_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_4_data = traceUnpacker_1_io_btm_bits_slices_4_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_4_mseo = traceUnpacker_1_io_btm_bits_slices_4_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_5_data = traceUnpacker_1_io_btm_bits_slices_5_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_5_mseo = traceUnpacker_1_io_btm_bits_slices_5_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_6_data = traceUnpacker_1_io_btm_bits_slices_6_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_6_mseo = traceUnpacker_1_io_btm_bits_slices_6_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_7_data = traceUnpacker_1_io_btm_bits_slices_7_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_7_mseo = traceUnpacker_1_io_btm_bits_slices_7_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_8_data = traceUnpacker_1_io_btm_bits_slices_8_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_8_mseo = traceUnpacker_1_io_btm_bits_slices_8_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_9_data = traceUnpacker_1_io_btm_bits_slices_9_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_9_mseo = traceUnpacker_1_io_btm_bits_slices_9_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_10_data = traceUnpacker_1_io_btm_bits_slices_10_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_10_mseo = traceUnpacker_1_io_btm_bits_slices_10_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_11_data = traceUnpacker_1_io_btm_bits_slices_11_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_11_mseo = traceUnpacker_1_io_btm_bits_slices_11_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_12_data = traceUnpacker_1_io_btm_bits_slices_12_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_12_mseo = traceUnpacker_1_io_btm_bits_slices_12_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_13_data = traceUnpacker_1_io_btm_bits_slices_13_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_13_mseo = traceUnpacker_1_io_btm_bits_slices_13_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_14_data = traceUnpacker_1_io_btm_bits_slices_14_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_14_mseo = traceUnpacker_1_io_btm_bits_slices_14_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_15_data = traceUnpacker_1_io_btm_bits_slices_15_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_15_mseo = traceUnpacker_1_io_btm_bits_slices_15_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_16_data = traceUnpacker_1_io_btm_bits_slices_16_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_16_mseo = traceUnpacker_1_io_btm_bits_slices_16_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_17_data = traceUnpacker_1_io_btm_bits_slices_17_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_17_mseo = traceUnpacker_1_io_btm_bits_slices_17_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_18_data = traceUnpacker_1_io_btm_bits_slices_18_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_18_mseo = traceUnpacker_1_io_btm_bits_slices_18_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_19_data = traceUnpacker_1_io_btm_bits_slices_19_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_19_mseo = traceUnpacker_1_io_btm_bits_slices_19_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_20_data = traceUnpacker_1_io_btm_bits_slices_20_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_20_mseo = traceUnpacker_1_io_btm_bits_slices_20_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_21_data = traceUnpacker_1_io_btm_bits_slices_21_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_21_mseo = traceUnpacker_1_io_btm_bits_slices_21_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_22_data = traceUnpacker_1_io_btm_bits_slices_22_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_22_mseo = traceUnpacker_1_io_btm_bits_slices_22_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_23_data = traceUnpacker_1_io_btm_bits_slices_23_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_23_mseo = traceUnpacker_1_io_btm_bits_slices_23_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_24_data = traceUnpacker_1_io_btm_bits_slices_24_data; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_in_1_bits_slices_24_mseo = traceUnpacker_1_io_btm_bits_slices_24_mseo; // @[TraceFunnel.scala 274:28]
  assign sinkArbiter_io_out_ready = tracePacker_io_btm_ready; // @[TraceFunnel.scala 278:24]
  assign traceUnpacker_clock = clock;
  assign traceUnpacker_reset = reset;
  assign traceUnpacker_io_teActive = teControlReg_teActive; // @[TraceFunnel.scala 271:33]
  assign traceUnpacker_io_stream_valid = auto_sink_in_0_a_valid & teControlReg_teEnable & teControlReg_teActive; // @[TraceFunnel.scala 265:76]
  assign traceUnpacker_io_stream_bits = auto_sink_in_0_a_bits_data; // @[TraceFunnel.scala 64:81 LazyModule.scala 388:16]
  assign traceUnpacker_io_btm_ready = sinkArbiter_io_in_0_ready; // @[TraceFunnel.scala 274:28]
  assign traceUnpacker_1_clock = clock;
  assign traceUnpacker_1_reset = reset;
  assign traceUnpacker_1_io_teActive = teControlReg_teActive; // @[TraceFunnel.scala 271:33]
  assign traceUnpacker_1_io_stream_valid = auto_sink_in_1_a_valid & teControlReg_teEnable & teControlReg_teActive; // @[TraceFunnel.scala 265:76]
  assign traceUnpacker_1_io_stream_bits = auto_sink_in_1_a_bits_data; // @[TraceFunnel.scala 64:81 LazyModule.scala 388:16]
  assign traceUnpacker_1_io_btm_ready = sinkArbiter_io_in_1_ready; // @[TraceFunnel.scala 274:28]
  assign pibOuter_clock = clock; // @[TraceFunnel.scala 299:26]
  assign pibOuter_reset = reset; // @[TraceFunnel.scala 300:26]
  assign pibOuter_io_pibControlSync_pibDivider = tracePIB_io_pibControlSync_pibDivider; // @[TraceFunnel.scala 301:38]
  assign pibOuter_io_pibControlSync_pibCalibrate = tracePIB_io_pibControlSync_pibCalibrate; // @[TraceFunnel.scala 301:38]
  assign pibOuter_io_pibControlSync_pibRefCenter = tracePIB_io_pibControlSync_pibRefCenter; // @[TraceFunnel.scala 301:38]
  assign pibOuter_io_pibControlSync_pibMode = tracePIB_io_pibControlSync_pibMode; // @[TraceFunnel.scala 301:38]
  assign pibOuter_io_pibControlSync_pibEnable = tracePIB_io_pibControlSync_pibEnable; // @[TraceFunnel.scala 301:38]
  assign pibOuter_io_pibControlSync_pibActive = tracePIB_io_pibControlSync_pibActive; // @[TraceFunnel.scala 301:38]
  assign pibOuter_io_streamIn_valid = tracePIB_io_streamOut_valid; // @[TraceFunnel.scala 302:32]
  assign pibOuter_io_streamIn_bits = tracePIB_io_streamOut_bits; // @[TraceFunnel.scala 302:32]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teSink <= 4'h4;
    end else if (out_f_woready & ~teControlWrData_teActive) begin
      teControlReg_teSink <= 4'h4;
    end else if (out_f_woready_8 & teControlWrData_teSink == 4'h7) begin
      teControlReg_teSink <= teControlWrData_teSink;
    end else if (out_f_woready_8 & teControlWrData_teSink == 4'h6) begin
      teControlReg_teSink <= teControlWrData_teSink;
    end else if (out_f_woready_8 & teControlWrData_teSink == 4'h4) begin
      teControlReg_teSink <= teControlWrData_teSink;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teSinkError <= 1'h0;
    end else if (out_f_woready & ~teControlWrData_teActive) begin
      teControlReg_teSinkError <= 1'h0;
    end else if (out_f_woready_7 & teControlWrData_teSinkError) begin
      teControlReg_teSinkError <= 1'h0;
    end else begin
      teControlReg_teSinkError <= _GEN_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teStopOnWrap <= 1'h0;
    end else if (out_f_woready_5) begin
      teControlReg_teStopOnWrap <= auto_reg_in_a_bits_data[14];
    end else if (out_f_woready & ~teControlWrData_teActive) begin
      teControlReg_teStopOnWrap <= 1'h0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teEnable <= 1'h0;
    end else if (_T_9) begin
      teControlReg_teEnable <= 1'h0;
    end else if (out_f_woready & ~teControlWrData_teActive) begin
      teControlReg_teEnable <= 1'h0;
    end else if (out_f_woready_1) begin
      teControlReg_teEnable <= teControlWrData_teEnable;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teActive <= 1'h0;
    end else if (out_f_woready & ~teControlWrData_teActive) begin
      teControlReg_teActive <= 1'h0;
    end else if (out_f_woready) begin
      teControlReg_teActive <= teControlWrData_teActive;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teSinkBaseReg_teSinkBase <= 30'h0;
    end else begin
      teSinkBaseReg_teSinkBase <= _GEN_25[29:0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teSinkBaseHReg_teSinkBaseH <= 32'h0;
    end else if (_sinkReady_T_2) begin
      teSinkBaseHReg_teSinkBaseH <= 32'h0;
    end else if (out_f_woready_28) begin
      teSinkBaseHReg_teSinkBaseH <= _teSinkBaseHReg_teSinkBaseH_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teSinkLimitReg_teSinkLimit <= 30'h0;
    end else begin
      teSinkLimitReg_teSinkLimit <= _GEN_30[29:0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teSinkRPReg_teSinkRP <= 6'h0;
    end else if (_sinkReady_T_2) begin
      teSinkRPReg_teSinkRP <= 6'h0;
    end else if (out_f_woready_35) begin
      teSinkRPReg_teSinkRP <= teSinkRPWrData_teSinkRP;
    end else if ((out_f_roready_36 | out_f_woready_36) & teSinkReady) begin
      teSinkRPReg_teSinkRP <= _teSinkRPReg_teSinkRP_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teSinkWPReg_teSinkWP <= 30'h0;
    end else begin
      teSinkWPReg_teSinkWP <= _GEN_45[29:0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teSinkWPReg_teSinkWrapped <= 1'h0;
    end else if (_sinkReady_T_2) begin
      teSinkWPReg_teSinkWrapped <= 1'h0;
    end else if (usingSBA) begin
      if (out_f_woready_33) begin
        teSinkWPReg_teSinkWrapped <= teSinkWPWrData_teSinkWrapped;
      end else if (traceSink_io_streamWPInc) begin
        teSinkWPReg_teSinkWrapped <= _GEN_35;
      end
    end else begin
      teSinkWPReg_teSinkWrapped <= _GEN_42;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pibControlReg_pibDivider <= 13'h0;
    end else if (_sinkReady_T_2) begin
      pibControlReg_pibDivider <= 13'h0;
    end else if (out_f_woready_17 & _T) begin
      pibControlReg_pibDivider <= 13'h0;
    end else if (out_f_woready_25) begin
      pibControlReg_pibDivider <= pibControlWrData_pibDivider;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pibControlReg_pibCalibrate <= 1'h0;
    end else if (_sinkReady_T_2) begin
      pibControlReg_pibCalibrate <= 1'h0;
    end else if (out_f_woready_17 & _T) begin
      pibControlReg_pibCalibrate <= 1'h0;
    end else if (out_f_woready_23) begin
      pibControlReg_pibCalibrate <= pibControlWrData_pibCalibrate;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pibControlReg_pibRefCenter <= 1'h0;
    end else if (_sinkReady_T_2) begin
      pibControlReg_pibRefCenter <= 1'h0;
    end else if (out_f_woready_17 & _T) begin
      pibControlReg_pibRefCenter <= 1'h0;
    end else if (out_f_woready_22 & _T_43) begin
      pibControlReg_pibRefCenter <= pibControlWrData_pibRefCenter;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pibControlReg_pibMode <= 4'h0;
    end else if (_sinkReady_T_2) begin
      pibControlReg_pibMode <= 4'h0;
    end else if (out_f_woready_17 & _T) begin
      pibControlReg_pibMode <= 4'h0;
    end else if (out_f_woready_21 & _T_43) begin
      pibControlReg_pibMode <= pibControlWrData_pibMode;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pibControlReg_pibEnable <= 1'h0;
    end else if (_sinkReady_T_2) begin
      pibControlReg_pibEnable <= 1'h0;
    end else if (out_f_woready_17 & _T) begin
      pibControlReg_pibEnable <= 1'h0;
    end else if (out_f_woready_18) begin
      pibControlReg_pibEnable <= teControlWrData_teEnable;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pibControlReg_pibActive <= 1'h0;
    end else if (_sinkReady_T_2) begin
      pibControlReg_pibActive <= 1'h0;
    end else if (out_f_woready_17 & _T) begin
      pibControlReg_pibActive <= 1'h0;
    end else if (out_f_woready_17) begin
      pibControlReg_pibActive <= teControlWrData_teActive;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  teControlReg_teSink = _RAND_0[3:0];
  _RAND_1 = {1{`RANDOM}};
  teControlReg_teSinkError = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  teControlReg_teStopOnWrap = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  teControlReg_teEnable = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  teControlReg_teActive = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  teSinkBaseReg_teSinkBase = _RAND_5[29:0];
  _RAND_6 = {1{`RANDOM}};
  teSinkBaseHReg_teSinkBaseH = _RAND_6[31:0];
  _RAND_7 = {1{`RANDOM}};
  teSinkLimitReg_teSinkLimit = _RAND_7[29:0];
  _RAND_8 = {1{`RANDOM}};
  teSinkRPReg_teSinkRP = _RAND_8[5:0];
  _RAND_9 = {1{`RANDOM}};
  teSinkWPReg_teSinkWP = _RAND_9[29:0];
  _RAND_10 = {1{`RANDOM}};
  teSinkWPReg_teSinkWrapped = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  pibControlReg_pibDivider = _RAND_11[12:0];
  _RAND_12 = {1{`RANDOM}};
  pibControlReg_pibCalibrate = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  pibControlReg_pibRefCenter = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  pibControlReg_pibMode = _RAND_14[3:0];
  _RAND_15 = {1{`RANDOM}};
  pibControlReg_pibEnable = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  pibControlReg_pibActive = _RAND_16[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    teControlReg_teSink = 4'h4;
  end
  if (reset) begin
    teControlReg_teSinkError = 1'h0;
  end
  if (reset) begin
    teControlReg_teStopOnWrap = 1'h0;
  end
  if (reset) begin
    teControlReg_teEnable = 1'h0;
  end
  if (reset) begin
    teControlReg_teActive = 1'h0;
  end
  if (reset) begin
    teSinkBaseReg_teSinkBase = 30'h0;
  end
  if (reset) begin
    teSinkBaseHReg_teSinkBaseH = 32'h0;
  end
  if (reset) begin
    teSinkLimitReg_teSinkLimit = 30'h0;
  end
  if (reset) begin
    teSinkRPReg_teSinkRP = 6'h0;
  end
  if (reset) begin
    teSinkWPReg_teSinkWP = 30'h0;
  end
  if (reset) begin
    teSinkWPReg_teSinkWrapped = 1'h0;
  end
  if (reset) begin
    pibControlReg_pibDivider = 13'h0;
  end
  if (reset) begin
    pibControlReg_pibCalibrate = 1'h0;
  end
  if (reset) begin
    pibControlReg_pibRefCenter = 1'h0;
  end
  if (reset) begin
    pibControlReg_pibMode = 4'h0;
  end
  if (reset) begin
    pibControlReg_pibEnable = 1'h0;
  end
  if (reset) begin
    pibControlReg_pibActive = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
