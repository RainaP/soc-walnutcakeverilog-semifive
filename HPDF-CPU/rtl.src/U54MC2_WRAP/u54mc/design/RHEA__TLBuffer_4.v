//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLBuffer_4(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_a_ready,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [3:0]  auto_in_a_bits_size,
  input  [5:0]  auto_in_a_bits_source,
  input  [36:0] auto_in_a_bits_address,
  input         auto_in_a_bits_user_amba_prot_bufferable,
  input         auto_in_a_bits_user_amba_prot_modifiable,
  input         auto_in_a_bits_user_amba_prot_readalloc,
  input         auto_in_a_bits_user_amba_prot_writealloc,
  input         auto_in_a_bits_user_amba_prot_privileged,
  input         auto_in_a_bits_user_amba_prot_secure,
  input         auto_in_a_bits_user_amba_prot_fetch,
  input  [7:0]  auto_in_a_bits_mask,
  input  [63:0] auto_in_a_bits_data,
  input         auto_in_d_ready,
  output        auto_in_d_valid,
  output [2:0]  auto_in_d_bits_opcode,
  output [1:0]  auto_in_d_bits_param,
  output [3:0]  auto_in_d_bits_size,
  output [5:0]  auto_in_d_bits_source,
  output [4:0]  auto_in_d_bits_sink,
  output        auto_in_d_bits_denied,
  output [63:0] auto_in_d_bits_data,
  output        auto_in_d_bits_corrupt,
  input         auto_out_a_ready,
  output        auto_out_a_valid,
  output [2:0]  auto_out_a_bits_opcode,
  output [3:0]  auto_out_a_bits_size,
  output [5:0]  auto_out_a_bits_source,
  output [36:0] auto_out_a_bits_address,
  output        auto_out_a_bits_user_amba_prot_bufferable,
  output        auto_out_a_bits_user_amba_prot_modifiable,
  output        auto_out_a_bits_user_amba_prot_readalloc,
  output        auto_out_a_bits_user_amba_prot_writealloc,
  output        auto_out_a_bits_user_amba_prot_privileged,
  output        auto_out_a_bits_user_amba_prot_secure,
  output        auto_out_a_bits_user_amba_prot_fetch,
  output [7:0]  auto_out_a_bits_mask,
  output [63:0] auto_out_a_bits_data,
  output        auto_out_d_ready,
  input         auto_out_d_valid,
  input  [2:0]  auto_out_d_bits_opcode,
  input  [1:0]  auto_out_d_bits_param,
  input  [3:0]  auto_out_d_bits_size,
  input  [5:0]  auto_out_d_bits_source,
  input  [4:0]  auto_out_d_bits_sink,
  input         auto_out_d_bits_denied,
  input  [63:0] auto_out_d_bits_data,
  input         auto_out_d_bits_corrupt
);
  wire  bundleOut_0_a_q_rf_reset; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_clock; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_reset; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_0_a_q_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [3:0] bundleOut_0_a_q_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [5:0] bundleOut_0_a_q_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [36:0] bundleOut_0_a_q_io_enq_bits_address; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_enq_bits_user_amba_prot_bufferable; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_enq_bits_user_amba_prot_modifiable; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_enq_bits_user_amba_prot_readalloc; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_enq_bits_user_amba_prot_writealloc; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_enq_bits_user_amba_prot_privileged; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_enq_bits_user_amba_prot_secure; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_enq_bits_user_amba_prot_fetch; // @[Decoupled.scala 296:21]
  wire [7:0] bundleOut_0_a_q_io_enq_bits_mask; // @[Decoupled.scala 296:21]
  wire [63:0] bundleOut_0_a_q_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_0_a_q_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [3:0] bundleOut_0_a_q_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [5:0] bundleOut_0_a_q_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [36:0] bundleOut_0_a_q_io_deq_bits_address; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_deq_bits_user_amba_prot_bufferable; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_deq_bits_user_amba_prot_modifiable; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_deq_bits_user_amba_prot_readalloc; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_deq_bits_user_amba_prot_writealloc; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_deq_bits_user_amba_prot_privileged; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_deq_bits_user_amba_prot_secure; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_deq_bits_user_amba_prot_fetch; // @[Decoupled.scala 296:21]
  wire [7:0] bundleOut_0_a_q_io_deq_bits_mask; // @[Decoupled.scala 296:21]
  wire [63:0] bundleOut_0_a_q_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_rf_reset; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_clock; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_reset; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_0_d_q_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] bundleIn_0_d_q_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [3:0] bundleIn_0_d_q_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [5:0] bundleIn_0_d_q_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [4:0] bundleIn_0_d_q_io_enq_bits_sink; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_enq_bits_denied; // @[Decoupled.scala 296:21]
  wire [63:0] bundleIn_0_d_q_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_0_d_q_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] bundleIn_0_d_q_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [3:0] bundleIn_0_d_q_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [5:0] bundleIn_0_d_q_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [4:0] bundleIn_0_d_q_io_deq_bits_sink; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_deq_bits_denied; // @[Decoupled.scala 296:21]
  wire [63:0] bundleIn_0_d_q_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  RHEA__Queue_24 bundleOut_0_a_q ( // @[Decoupled.scala 296:21]
    .rf_reset(bundleOut_0_a_q_rf_reset),
    .clock(bundleOut_0_a_q_clock),
    .reset(bundleOut_0_a_q_reset),
    .io_enq_ready(bundleOut_0_a_q_io_enq_ready),
    .io_enq_valid(bundleOut_0_a_q_io_enq_valid),
    .io_enq_bits_opcode(bundleOut_0_a_q_io_enq_bits_opcode),
    .io_enq_bits_size(bundleOut_0_a_q_io_enq_bits_size),
    .io_enq_bits_source(bundleOut_0_a_q_io_enq_bits_source),
    .io_enq_bits_address(bundleOut_0_a_q_io_enq_bits_address),
    .io_enq_bits_user_amba_prot_bufferable(bundleOut_0_a_q_io_enq_bits_user_amba_prot_bufferable),
    .io_enq_bits_user_amba_prot_modifiable(bundleOut_0_a_q_io_enq_bits_user_amba_prot_modifiable),
    .io_enq_bits_user_amba_prot_readalloc(bundleOut_0_a_q_io_enq_bits_user_amba_prot_readalloc),
    .io_enq_bits_user_amba_prot_writealloc(bundleOut_0_a_q_io_enq_bits_user_amba_prot_writealloc),
    .io_enq_bits_user_amba_prot_privileged(bundleOut_0_a_q_io_enq_bits_user_amba_prot_privileged),
    .io_enq_bits_user_amba_prot_secure(bundleOut_0_a_q_io_enq_bits_user_amba_prot_secure),
    .io_enq_bits_user_amba_prot_fetch(bundleOut_0_a_q_io_enq_bits_user_amba_prot_fetch),
    .io_enq_bits_mask(bundleOut_0_a_q_io_enq_bits_mask),
    .io_enq_bits_data(bundleOut_0_a_q_io_enq_bits_data),
    .io_deq_ready(bundleOut_0_a_q_io_deq_ready),
    .io_deq_valid(bundleOut_0_a_q_io_deq_valid),
    .io_deq_bits_opcode(bundleOut_0_a_q_io_deq_bits_opcode),
    .io_deq_bits_size(bundleOut_0_a_q_io_deq_bits_size),
    .io_deq_bits_source(bundleOut_0_a_q_io_deq_bits_source),
    .io_deq_bits_address(bundleOut_0_a_q_io_deq_bits_address),
    .io_deq_bits_user_amba_prot_bufferable(bundleOut_0_a_q_io_deq_bits_user_amba_prot_bufferable),
    .io_deq_bits_user_amba_prot_modifiable(bundleOut_0_a_q_io_deq_bits_user_amba_prot_modifiable),
    .io_deq_bits_user_amba_prot_readalloc(bundleOut_0_a_q_io_deq_bits_user_amba_prot_readalloc),
    .io_deq_bits_user_amba_prot_writealloc(bundleOut_0_a_q_io_deq_bits_user_amba_prot_writealloc),
    .io_deq_bits_user_amba_prot_privileged(bundleOut_0_a_q_io_deq_bits_user_amba_prot_privileged),
    .io_deq_bits_user_amba_prot_secure(bundleOut_0_a_q_io_deq_bits_user_amba_prot_secure),
    .io_deq_bits_user_amba_prot_fetch(bundleOut_0_a_q_io_deq_bits_user_amba_prot_fetch),
    .io_deq_bits_mask(bundleOut_0_a_q_io_deq_bits_mask),
    .io_deq_bits_data(bundleOut_0_a_q_io_deq_bits_data)
  );
  RHEA__Queue_25 bundleIn_0_d_q ( // @[Decoupled.scala 296:21]
    .rf_reset(bundleIn_0_d_q_rf_reset),
    .clock(bundleIn_0_d_q_clock),
    .reset(bundleIn_0_d_q_reset),
    .io_enq_ready(bundleIn_0_d_q_io_enq_ready),
    .io_enq_valid(bundleIn_0_d_q_io_enq_valid),
    .io_enq_bits_opcode(bundleIn_0_d_q_io_enq_bits_opcode),
    .io_enq_bits_param(bundleIn_0_d_q_io_enq_bits_param),
    .io_enq_bits_size(bundleIn_0_d_q_io_enq_bits_size),
    .io_enq_bits_source(bundleIn_0_d_q_io_enq_bits_source),
    .io_enq_bits_sink(bundleIn_0_d_q_io_enq_bits_sink),
    .io_enq_bits_denied(bundleIn_0_d_q_io_enq_bits_denied),
    .io_enq_bits_data(bundleIn_0_d_q_io_enq_bits_data),
    .io_enq_bits_corrupt(bundleIn_0_d_q_io_enq_bits_corrupt),
    .io_deq_ready(bundleIn_0_d_q_io_deq_ready),
    .io_deq_valid(bundleIn_0_d_q_io_deq_valid),
    .io_deq_bits_opcode(bundleIn_0_d_q_io_deq_bits_opcode),
    .io_deq_bits_param(bundleIn_0_d_q_io_deq_bits_param),
    .io_deq_bits_size(bundleIn_0_d_q_io_deq_bits_size),
    .io_deq_bits_source(bundleIn_0_d_q_io_deq_bits_source),
    .io_deq_bits_sink(bundleIn_0_d_q_io_deq_bits_sink),
    .io_deq_bits_denied(bundleIn_0_d_q_io_deq_bits_denied),
    .io_deq_bits_data(bundleIn_0_d_q_io_deq_bits_data),
    .io_deq_bits_corrupt(bundleIn_0_d_q_io_deq_bits_corrupt)
  );
  assign bundleOut_0_a_q_rf_reset = rf_reset;
  assign bundleIn_0_d_q_rf_reset = rf_reset;
  assign auto_in_a_ready = bundleOut_0_a_q_io_enq_ready; // @[Nodes.scala 1529:13 Decoupled.scala 299:17]
  assign auto_in_d_valid = bundleIn_0_d_q_io_deq_valid; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_d_bits_opcode = bundleIn_0_d_q_io_deq_bits_opcode; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_d_bits_param = bundleIn_0_d_q_io_deq_bits_param; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_d_bits_size = bundleIn_0_d_q_io_deq_bits_size; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_d_bits_source = bundleIn_0_d_q_io_deq_bits_source; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_d_bits_sink = bundleIn_0_d_q_io_deq_bits_sink; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_d_bits_denied = bundleIn_0_d_q_io_deq_bits_denied; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_d_bits_data = bundleIn_0_d_q_io_deq_bits_data; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_d_bits_corrupt = bundleIn_0_d_q_io_deq_bits_corrupt; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_out_a_valid = bundleOut_0_a_q_io_deq_valid; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_opcode = bundleOut_0_a_q_io_deq_bits_opcode; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_size = bundleOut_0_a_q_io_deq_bits_size; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_source = bundleOut_0_a_q_io_deq_bits_source; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_address = bundleOut_0_a_q_io_deq_bits_address; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_user_amba_prot_bufferable = bundleOut_0_a_q_io_deq_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_user_amba_prot_modifiable = bundleOut_0_a_q_io_deq_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_user_amba_prot_readalloc = bundleOut_0_a_q_io_deq_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_user_amba_prot_writealloc = bundleOut_0_a_q_io_deq_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_user_amba_prot_privileged = bundleOut_0_a_q_io_deq_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_user_amba_prot_secure = bundleOut_0_a_q_io_deq_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_user_amba_prot_fetch = bundleOut_0_a_q_io_deq_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_mask = bundleOut_0_a_q_io_deq_bits_mask; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_data = bundleOut_0_a_q_io_deq_bits_data; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_d_ready = bundleIn_0_d_q_io_enq_ready; // @[Nodes.scala 1529:13 Decoupled.scala 299:17]
  assign bundleOut_0_a_q_clock = clock;
  assign bundleOut_0_a_q_reset = reset;
  assign bundleOut_0_a_q_io_enq_valid = auto_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_size = auto_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_source = auto_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_address = auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_user_amba_prot_bufferable = auto_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_user_amba_prot_modifiable = auto_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_user_amba_prot_readalloc = auto_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_user_amba_prot_writealloc = auto_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_user_amba_prot_privileged = auto_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_user_amba_prot_secure = auto_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_user_amba_prot_fetch = auto_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_data = auto_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_deq_ready = auto_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_clock = clock;
  assign bundleIn_0_d_q_reset = reset;
  assign bundleIn_0_d_q_io_enq_valid = auto_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_param = auto_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_size = auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_source = auto_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_sink = auto_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_denied = auto_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_data = auto_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_corrupt = auto_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_deq_ready = auto_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
endmodule
