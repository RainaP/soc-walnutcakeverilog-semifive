//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__Queue_124(
  input        rf_reset,
  input        clock,
  input        reset,
  output       io_enq_ready,
  input        io_enq_valid,
  input        io_enq_bits_noop,
  input  [2:0] io_enq_bits_way,
  input  [9:0] io_enq_bits_set,
  input  [1:0] io_enq_bits_beat,
  input        io_deq_ready,
  output       io_deq_valid,
  output       io_deq_bits_noop,
  output [2:0] io_deq_bits_way,
  output [9:0] io_deq_bits_set,
  output [1:0] io_deq_bits_beat
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
`endif // RANDOMIZE_REG_INIT
  reg  ram_0_noop; // @[Decoupled.scala 218:16]
  reg [2:0] ram_0_way; // @[Decoupled.scala 218:16]
  reg [9:0] ram_0_set; // @[Decoupled.scala 218:16]
  reg [1:0] ram_0_beat; // @[Decoupled.scala 218:16]
  wire  do_enq = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  reg  maybe_full; // @[Decoupled.scala 221:27]
  wire  empty = ~maybe_full; // @[Decoupled.scala 224:28]
  wire  do_deq = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  assign io_enq_ready = io_deq_ready | empty; // @[Decoupled.scala 254:25 Decoupled.scala 254:40 Decoupled.scala 241:16]
  assign io_deq_valid = ~empty; // @[Decoupled.scala 240:19]
  assign io_deq_bits_noop = ram_0_noop; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_way = ram_0_way; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_set = ram_0_set; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_beat = ram_0_beat; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_noop <= 1'h0;
    end else if (do_enq) begin
      ram_0_noop <= io_enq_bits_noop;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_way <= 3'h0;
    end else if (do_enq) begin
      ram_0_way <= io_enq_bits_way;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_set <= 10'h0;
    end else if (do_enq) begin
      ram_0_set <= io_enq_bits_set;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_beat <= 2'h0;
    end else if (do_enq) begin
      ram_0_beat <= io_enq_bits_beat;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      maybe_full <= 1'h0;
    end else if (do_enq != do_deq) begin
      maybe_full <= do_enq;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ram_0_noop = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  ram_0_way = _RAND_1[2:0];
  _RAND_2 = {1{`RANDOM}};
  ram_0_set = _RAND_2[9:0];
  _RAND_3 = {1{`RANDOM}};
  ram_0_beat = _RAND_3[1:0];
  _RAND_4 = {1{`RANDOM}};
  maybe_full = _RAND_4[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ram_0_noop = 1'h0;
  end
  if (rf_reset) begin
    ram_0_way = 3'h0;
  end
  if (rf_reset) begin
    ram_0_set = 10'h0;
  end
  if (rf_reset) begin
    ram_0_beat = 2'h0;
  end
  if (reset) begin
    maybe_full = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
