//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade 
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module lpddr4_subsystem (
	 input          SCU_LPDDR4_0__CLK__OSC
	,input          SCU_LPDDR4_0__CLK__SUBSYSTEM
	,input          SCU_LPDDR4_0__RSTN__SUBSYSTEM
	,output         PBUS_LPDDR4_0__CLK__RX
	,output         PBUS_LPDDR4_0__RSTN__RX
	,output         MBUS_LPDDR4_0__CLK__RX
	,output         MBUS_LPDDR4_0__RSTN__RX
	,output         PBUS_LPDDR4_0__AXI4__RX__ARREADY
	,output         PBUS_LPDDR4_0__AXI4__RX__AWREADY
	,output         PBUS_LPDDR4_0__AXI4__RX__BID
	,output [  1:0] PBUS_LPDDR4_0__AXI4__RX__BRESP
	,output         PBUS_LPDDR4_0__AXI4__RX__BVALID
	,output [ 31:0] PBUS_LPDDR4_0__AXI4__RX__RDATA
	,output         PBUS_LPDDR4_0__AXI4__RX__RID
	,output         PBUS_LPDDR4_0__AXI4__RX__RLAST
	,output [  1:0] PBUS_LPDDR4_0__AXI4__RX__RRESP
	,output         PBUS_LPDDR4_0__AXI4__RX__RVALID
	,output         PBUS_LPDDR4_0__AXI4__RX__WREADY
	,input  [ 31:0] PBUS_LPDDR4_0__AXI4__RX__ARADDR
	,input  [  1:0] PBUS_LPDDR4_0__AXI4__RX__ARBURST
	,input  [  3:0] PBUS_LPDDR4_0__AXI4__RX__ARCACHE
	,input          PBUS_LPDDR4_0__AXI4__RX__ARID
	,input  [  7:0] PBUS_LPDDR4_0__AXI4__RX__ARLEN
	,input          PBUS_LPDDR4_0__AXI4__RX__ARLOCK
	,input  [  2:0] PBUS_LPDDR4_0__AXI4__RX__ARPROT
	,input  [  2:0] PBUS_LPDDR4_0__AXI4__RX__ARSIZE
	,input          PBUS_LPDDR4_0__AXI4__RX__ARVALID
	,input  [ 31:0] PBUS_LPDDR4_0__AXI4__RX__AWADDR
	,input  [  1:0] PBUS_LPDDR4_0__AXI4__RX__AWBURST
	,input  [  3:0] PBUS_LPDDR4_0__AXI4__RX__AWCACHE
	,input          PBUS_LPDDR4_0__AXI4__RX__AWID
	,input  [  7:0] PBUS_LPDDR4_0__AXI4__RX__AWLEN
	,input          PBUS_LPDDR4_0__AXI4__RX__AWLOCK
	,input  [  2:0] PBUS_LPDDR4_0__AXI4__RX__AWPROT
	,input  [  2:0] PBUS_LPDDR4_0__AXI4__RX__AWSIZE
	,input          PBUS_LPDDR4_0__AXI4__RX__AWVALID
	,input          PBUS_LPDDR4_0__AXI4__RX__BREADY
	,input          PBUS_LPDDR4_0__AXI4__RX__RREADY
	,input  [ 31:0] PBUS_LPDDR4_0__AXI4__RX__WDATA
	,input          PBUS_LPDDR4_0__AXI4__RX__WLAST
	,input  [  3:0] PBUS_LPDDR4_0__AXI4__RX__WSTRB
	,input          PBUS_LPDDR4_0__AXI4__RX__WVALID
	,output         MBUS_LPDDR4_0__AXI4__RX__ARREADY
	,output         MBUS_LPDDR4_0__AXI4__RX__AWREADY
	,output [ 11:0] MBUS_LPDDR4_0__AXI4__RX__BID
	,output [  1:0] MBUS_LPDDR4_0__AXI4__RX__BRESP
	,output         MBUS_LPDDR4_0__AXI4__RX__BVALID
	,output [255:0] MBUS_LPDDR4_0__AXI4__RX__RDATA
	,output [ 11:0] MBUS_LPDDR4_0__AXI4__RX__RID
	,output         MBUS_LPDDR4_0__AXI4__RX__RLAST
	,output [  1:0] MBUS_LPDDR4_0__AXI4__RX__RRESP
	,output         MBUS_LPDDR4_0__AXI4__RX__RVALID
	,output         MBUS_LPDDR4_0__AXI4__RX__WREADY
	,input  [ 35:0] MBUS_LPDDR4_0__AXI4__RX__ARADDR
	,input  [  1:0] MBUS_LPDDR4_0__AXI4__RX__ARBURST
	,input  [  3:0] MBUS_LPDDR4_0__AXI4__RX__ARCACHE
	,input  [ 11:0] MBUS_LPDDR4_0__AXI4__RX__ARID
	,input  [  7:0] MBUS_LPDDR4_0__AXI4__RX__ARLEN
	,input          MBUS_LPDDR4_0__AXI4__RX__ARLOCK
	,input  [  2:0] MBUS_LPDDR4_0__AXI4__RX__ARPROT
	,input  [  9:0] MBUS_LPDDR4_0__AXI4__RX__ARUSER
	,input  [  2:0] MBUS_LPDDR4_0__AXI4__RX__ARSIZE
	,input          MBUS_LPDDR4_0__AXI4__RX__ARVALID
	,input  [ 35:0] MBUS_LPDDR4_0__AXI4__RX__AWADDR
	,input  [  1:0] MBUS_LPDDR4_0__AXI4__RX__AWBURST
	,input  [  3:0] MBUS_LPDDR4_0__AXI4__RX__AWCACHE
	,input  [ 11:0] MBUS_LPDDR4_0__AXI4__RX__AWID
	,input  [  7:0] MBUS_LPDDR4_0__AXI4__RX__AWLEN
	,input          MBUS_LPDDR4_0__AXI4__RX__AWLOCK
	,input  [  2:0] MBUS_LPDDR4_0__AXI4__RX__AWPROT
	,input  [  9:0] MBUS_LPDDR4_0__AXI4__RX__AWUSER
	,input  [  2:0] MBUS_LPDDR4_0__AXI4__RX__AWSIZE
	,input          MBUS_LPDDR4_0__AXI4__RX__AWVALID
	,input          MBUS_LPDDR4_0__AXI4__RX__BREADY
	,input          MBUS_LPDDR4_0__AXI4__RX__RREADY
	,input  [255:0] MBUS_LPDDR4_0__AXI4__RX__WDATA
	,input          MBUS_LPDDR4_0__AXI4__RX__WLAST
	,input  [ 31:0] MBUS_LPDDR4_0__AXI4__RX__WSTRB
	,input          MBUS_LPDDR4_0__AXI4__RX__WVALID
	,output         LPDDR4_0__IRQ__cooldown
	,output         LPDDR4_0__IRQ__hightemp
	,output         LPDDR4_0__IRQ__overtemp
	,output [  5:0] LPDDR4_0__IO__ADCT
	,inout          LPDDR4_0__IO__CK
	,inout          LPDDR4_0__IO__CKB
	,output [  1:0] LPDDR4_0__IO__CKE
	,output [  1:0] LPDDR4_0__IO__CS
	,inout  [  1:0] LPDDR4_0__IO__DM_A
	,inout  [  1:0] LPDDR4_0__IO__DM_B
	,inout  [ 15:0] LPDDR4_0__IO__DQ_A
	,inout  [ 15:0] LPDDR4_0__IO__DQ_B
	,inout  [  1:0] LPDDR4_0__IO__NDQS_A
	,inout  [  1:0] LPDDR4_0__IO__NDQS_B
	,output [  1:0] LPDDR4_0__IO__ODT
	,inout  [  1:0] LPDDR4_0__IO__PDQS_A
	,inout  [  1:0] LPDDR4_0__IO__PDQS_B
	,output         LPDDR4_0__IO__RESET
	,inout          LPDDR4_0__IO__ZQ
	,input          LPDDR4_0__RET_OFF
	,input          LPDDR4_0__TEST__MODE
	,input          LPDDR4_0__TEST__PHY__ND_DDR_START
	,output         LPDDR4_0__TEST__PHY__ND_DDR_END
	,output [  3:0] LPDDR4_0__TEST__PHY__DDRPHY_DTB
	,output         LPDDR4_0__TEST__PLL__CLK_SEL_OUT
	,output [  4:0] LPDDR4_0__TEST__PLL__AFC_CODE
	,output         LPDDR4_0__TEST__PLL__LOCK
	,input          DFT__SCAN_CLK
	,input          DFT__SCAN_RSTN
	,input          DFT__SCAN_MODE
);
	assign PBUS_LPDDR4_0__AXI4__RX__ARREADY = 1'h1;
	assign PBUS_LPDDR4_0__AXI4__RX__AWREADY = 1'h1;
	assign PBUS_LPDDR4_0__AXI4__RX__WREADY = 1'h1;
	assign MBUS_LPDDR4_0__AXI4__RX__ARREADY = 1'h1;
	assign MBUS_LPDDR4_0__AXI4__RX__AWREADY = 1'h1;
	assign MBUS_LPDDR4_0__AXI4__RX__WREADY = 1'h1;
	assign PBUS_LPDDR4_0__CLK__RX = 1'h0;
	assign PBUS_LPDDR4_0__RSTN__RX = 1'h0;
	assign MBUS_LPDDR4_0__CLK__RX = 1'h0;
	assign MBUS_LPDDR4_0__RSTN__RX = 1'h0;
	assign PBUS_LPDDR4_0__AXI4__RX__BID = 1'h0;
	assign PBUS_LPDDR4_0__AXI4__RX__BRESP = 2'h0;
	assign PBUS_LPDDR4_0__AXI4__RX__BVALID = 1'h0;
	assign PBUS_LPDDR4_0__AXI4__RX__RDATA = 32'h0;
	assign PBUS_LPDDR4_0__AXI4__RX__RID = 1'h0;
	assign PBUS_LPDDR4_0__AXI4__RX__RLAST = 1'h0;
	assign PBUS_LPDDR4_0__AXI4__RX__RRESP = 2'h0;
	assign PBUS_LPDDR4_0__AXI4__RX__RVALID = 1'h0;
	assign MBUS_LPDDR4_0__AXI4__RX__BID = 12'h0;
	assign MBUS_LPDDR4_0__AXI4__RX__BRESP = 2'h0;
	assign MBUS_LPDDR4_0__AXI4__RX__BVALID = 1'h0;
	assign MBUS_LPDDR4_0__AXI4__RX__RDATA = 256'h0;
	assign MBUS_LPDDR4_0__AXI4__RX__RID = 12'h0;
	assign MBUS_LPDDR4_0__AXI4__RX__RLAST = 1'h0;
	assign MBUS_LPDDR4_0__AXI4__RX__RRESP = 2'h0;
	assign MBUS_LPDDR4_0__AXI4__RX__RVALID = 1'h0;
	assign LPDDR4_0__IRQ__cooldown = 1'h0;
	assign LPDDR4_0__IRQ__hightemp = 1'h0;
	assign LPDDR4_0__IRQ__overtemp = 1'h0;
	assign LPDDR4_0__IO__ADCT = 6'h0;
	assign LPDDR4_0__IO__CKE = 2'h0;
	assign LPDDR4_0__IO__CS = 2'h0;
	assign LPDDR4_0__IO__ODT = 2'h0;
	assign LPDDR4_0__IO__RESET = 1'h0;
	assign LPDDR4_0__TEST__PHY__ND_DDR_END = 1'h0;
	assign LPDDR4_0__TEST__PHY__DDRPHY_DTB = 4'h0;
	assign LPDDR4_0__TEST__PLL__CLK_SEL_OUT = 1'h0;
	assign LPDDR4_0__TEST__PLL__AFC_CODE = 5'h0;
	assign LPDDR4_0__TEST__PLL__LOCK = 1'h0;

endmodule 
