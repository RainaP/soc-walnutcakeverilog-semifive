//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_CSRFile_assert(
  input   clock,
  input   reset,
  input   io_exception,
  input   io_retire,
  input   insn_ret,
  input   insn_call,
  input   insn_break,
  input   reg_singleStepped
);
  wire [1:0] _T_1219; // @[Bitwise.scala 47:55]
  wire [1:0] _T_1221; // @[Bitwise.scala 47:55]
  wire [2:0] _T_1223; // @[Bitwise.scala 47:55]
  wire  _T_1225; // @[CSR.scala 659:79]
  wire  _T_1227; // @[CSR.scala 659:9]
  wire  _T_1228; // @[CSR.scala 659:9]
  wire  _T_1245; // @[CSR.scala 667:10]
  wire  _T_1246; // @[CSR.scala 667:42]
  wire  _T_1247; // @[CSR.scala 667:29]
  wire  _T_1249; // @[CSR.scala 667:9]
  wire  _T_1250; // @[CSR.scala 667:9]
  assign _T_1219 = insn_ret + insn_call; // @[Bitwise.scala 47:55]
  assign _T_1221 = insn_break + io_exception; // @[Bitwise.scala 47:55]
  assign _T_1223 = _T_1219 + _T_1221; // @[Bitwise.scala 47:55]
  assign _T_1225 = _T_1223 <= 3'h1; // @[CSR.scala 659:79]
  assign _T_1227 = _T_1225 | reset; // @[CSR.scala 659:9]
  assign _T_1228 = ~_T_1227; // @[CSR.scala 659:9]
  assign _T_1245 = ~reg_singleStepped; // @[CSR.scala 667:10]
  assign _T_1246 = ~io_retire; // @[CSR.scala 667:42]
  assign _T_1247 = _T_1245 | _T_1246; // @[CSR.scala 667:29]
  assign _T_1249 = _T_1247 | reset; // @[CSR.scala 667:9]
  assign _T_1250 = ~_T_1249; // @[CSR.scala 667:9]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1228) begin
          $fwrite(32'h80000002,"Assertion Failed: these conditions must be mutually exclusive\n    at CSR.scala:659 assert(PopCount(insn_ret :: insn_call :: insn_break :: io.exception :: Nil) <= 1, \"these conditions must be mutually exclusive\")\n"); // @[CSR.scala 659:9]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1228) begin
          $fatal; // @[CSR.scala 659:9]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1250) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at CSR.scala:667 assert(!reg_singleStepped || io.retire === UInt(0))\n"); // @[CSR.scala 667:9]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1250) begin
          $fatal; // @[CSR.scala 667:9]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
