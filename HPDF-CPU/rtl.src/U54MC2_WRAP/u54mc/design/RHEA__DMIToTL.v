//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__DMIToTL(
  input         auto_out_a_ready,
  output        auto_out_a_valid,
  output [2:0]  auto_out_a_bits_opcode,
  output [8:0]  auto_out_a_bits_address,
  output [31:0] auto_out_a_bits_data,
  output        auto_out_d_ready,
  input         auto_out_d_valid,
  input         auto_out_d_bits_denied,
  input  [31:0] auto_out_d_bits_data,
  input         auto_out_d_bits_corrupt,
  output        io_dmi_req_ready,
  input         io_dmi_req_valid,
  input  [6:0]  io_dmi_req_bits_addr,
  input  [31:0] io_dmi_req_bits_data,
  input  [1:0]  io_dmi_req_bits_op,
  input         io_dmi_resp_ready,
  output        io_dmi_resp_valid,
  output [31:0] io_dmi_resp_bits_data,
  output [1:0]  io_dmi_resp_bits_resp
);
  wire [8:0] addr = {io_dmi_req_bits_addr, 2'h0}; // @[DMI.scala 95:46]
  wire [8:0] _GEN_3 = io_dmi_req_bits_op == 2'h1 ? addr : 9'h48; // @[DMI.scala 110:64 DMI.scala 110:76 DMI.scala 111:76]
  wire [2:0] _GEN_7 = io_dmi_req_bits_op == 2'h1 ? 3'h4 : 3'h0; // @[DMI.scala 110:64 DMI.scala 110:76 DMI.scala 111:76]
  wire  _io_dmi_resp_bits_resp_T = auto_out_d_bits_corrupt | auto_out_d_bits_denied; // @[DMI.scala 119:53]
  assign auto_out_a_valid = io_dmi_req_valid; // @[DMI.scala 81:26 DMI.scala 114:22]
  assign auto_out_a_bits_opcode = io_dmi_req_bits_op == 2'h2 ? 3'h0 : _GEN_7; // @[DMI.scala 109:64 DMI.scala 109:76]
  assign auto_out_a_bits_address = io_dmi_req_bits_op == 2'h2 ? addr : _GEN_3; // @[DMI.scala 109:64 DMI.scala 109:76]
  assign auto_out_a_bits_data = io_dmi_req_bits_op == 2'h2 ? io_dmi_req_bits_data : 32'h0; // @[DMI.scala 109:64 DMI.scala 109:76]
  assign auto_out_d_ready = io_dmi_resp_ready; // @[DMI.scala 81:26 DMI.scala 118:28]
  assign io_dmi_req_ready = auto_out_a_ready; // @[DMI.scala 81:26 LazyModule.scala 390:12]
  assign io_dmi_resp_valid = auto_out_d_valid; // @[DMI.scala 81:26 LazyModule.scala 390:12]
  assign io_dmi_resp_bits_data = auto_out_d_bits_data; // @[DMI.scala 81:26 LazyModule.scala 390:12]
  assign io_dmi_resp_bits_resp = {{1'd0}, _io_dmi_resp_bits_resp_T}; // @[DMI.scala 119:53]
endmodule
