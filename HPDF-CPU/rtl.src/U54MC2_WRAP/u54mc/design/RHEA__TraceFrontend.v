//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TraceFrontend(
  input         clock,
  input         reset,
  input         auto_traceTrigBlock_bpwatch_in_0_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_0_action,
  input         auto_traceTrigBlock_bpwatch_in_1_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_1_action,
  input         auto_traceTrigBlock_bpwatch_in_2_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_2_action,
  input         auto_traceTrigBlock_bpwatch_in_3_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_3_action,
  input         auto_traceTrigBlock_bpwatch_in_4_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_4_action,
  input         auto_traceTrigBlock_bpwatch_in_5_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_5_action,
  input         auto_traceTrigBlock_bpwatch_in_6_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_6_action,
  input         auto_traceTrigBlock_bpwatch_in_7_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_7_action,
  input         auto_traceTrigBlock_bpwatch_in_8_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_8_action,
  input         auto_traceTrigBlock_bpwatch_in_9_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_9_action,
  input         auto_traceTrigBlock_bpwatch_in_10_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_10_action,
  input         auto_traceTrigBlock_bpwatch_in_11_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_11_action,
  input         auto_traceTrigBlock_bpwatch_in_12_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_12_action,
  input         auto_traceTrigBlock_bpwatch_in_13_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_13_action,
  input         auto_traceTrigBlock_bpwatch_in_14_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_14_action,
  input         auto_traceTrigBlock_bpwatch_in_15_valid_0,
  input  [2:0]  auto_traceTrigBlock_bpwatch_in_15_action,
  input         auto_traceInBlock_trace_legacy_in_0_valid,
  input  [39:0] auto_traceInBlock_trace_legacy_in_0_iaddr,
  input  [31:0] auto_traceInBlock_trace_legacy_in_0_insn,
  input  [2:0]  auto_traceInBlock_trace_legacy_in_0_priv,
  input         auto_traceInBlock_trace_legacy_in_0_exception,
  input         auto_traceInBlock_trace_legacy_in_0_interrupt,
  output        auto_trace_aux_out_stall,
  input         io_teActive,
  input         io_tlClock,
  input         io_tlReset,
  input         io_hartIsInReset,
  input         io_ingControlAsync_mem_0_teEnable,
  input         io_ingControlAsync_mem_0_teTracing,
  input  [2:0]  io_ingControlAsync_mem_0_teInstruction,
  input         io_ingControlAsync_mem_0_teStallEnable,
  input  [3:0]  io_ingControlAsync_mem_0_teSyncMaxInst,
  input  [3:0]  io_ingControlAsync_mem_0_teSyncMaxBTM,
  output        io_ingControlAsync_ridx,
  input         io_ingControlAsync_widx,
  output        io_traceOn,
  input         io_teIdle,
  output        io_traceStall,
  input         io_pcActive,
  output        io_pcCapture_valid,
  output [38:0] io_pcCapture_bits,
  output        io_pcSample_valid,
  output [38:0] io_pcSample_bits,
  input         io_btmEvent_ready,
  output        io_btmEvent_valid,
  output [4:0]  io_btmEvent_bits_reason,
  output [38:0] io_btmEvent_bits_iaddr,
  output [13:0] io_btmEvent_bits_icnt,
  output [31:0] io_btmEvent_bits_hist,
  output [1:0]  io_btmEvent_bits_hisv,
  input         io_wpControl_mem_0_0,
  input         io_wpControl_mem_0_1,
  input         io_wpControl_mem_0_2,
  input         io_wpControl_mem_0_3,
  input         io_wpControl_mem_0_4,
  input         io_wpControl_mem_0_5,
  input         io_wpControl_mem_0_6,
  input         io_wpControl_mem_0_7,
  input         io_wpControl_mem_0_8,
  input         io_wpControl_mem_0_9,
  input         io_wpControl_mem_0_10,
  input         io_wpControl_mem_0_11,
  input         io_wpControl_mem_0_12,
  input         io_wpControl_mem_0_13,
  input         io_wpControl_mem_0_14,
  input         io_wpControl_mem_0_15
);
  wire  rf_reset;
  wire  traceInBlock_rf_reset; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_clock; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_reset; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_auto_trace_legacy_in_0_valid; // @[TraceFrontend.scala 161:52]
  wire [39:0] traceInBlock_auto_trace_legacy_in_0_iaddr; // @[TraceFrontend.scala 161:52]
  wire [31:0] traceInBlock_auto_trace_legacy_in_0_insn; // @[TraceFrontend.scala 161:52]
  wire [2:0] traceInBlock_auto_trace_legacy_in_0_priv; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_auto_trace_legacy_in_0_exception; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_auto_trace_legacy_in_0_interrupt; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_teActive; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_teActiveCore; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_tlClock; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_tlReset; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_hartIsInReset; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_ingControlAsync_mem_0_teEnable; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_ingControlAsync_mem_0_teTracing; // @[TraceFrontend.scala 161:52]
  wire [2:0] traceInBlock_io_ingControlAsync_mem_0_teInstruction; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_ingControlAsync_mem_0_teStallEnable; // @[TraceFrontend.scala 161:52]
  wire [3:0] traceInBlock_io_ingControlAsync_mem_0_teSyncMaxInst; // @[TraceFrontend.scala 161:52]
  wire [3:0] traceInBlock_io_ingControlAsync_mem_0_teSyncMaxBTM; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_ingControlAsync_ridx; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_ingControlAsync_widx; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_traceOn; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_iretire_0; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_ivalid_0; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_ingValid; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_ingTrigger_0_traceon; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_ingTrigger_0_traceoff; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_ingTrigger_0_wpsync; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_teIdle; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_btmEvent_ready; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_btmEvent_valid; // @[TraceFrontend.scala 161:52]
  wire [4:0] traceInBlock_io_btmEvent_bits_reason; // @[TraceFrontend.scala 161:52]
  wire [38:0] traceInBlock_io_btmEvent_bits_iaddr; // @[TraceFrontend.scala 161:52]
  wire [13:0] traceInBlock_io_btmEvent_bits_icnt; // @[TraceFrontend.scala 161:52]
  wire [31:0] traceInBlock_io_btmEvent_bits_hist; // @[TraceFrontend.scala 161:52]
  wire [1:0] traceInBlock_io_btmEvent_bits_hisv; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_traceStall; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_pcActive; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_pcCapture_valid; // @[TraceFrontend.scala 161:52]
  wire [38:0] traceInBlock_io_pcCapture_bits; // @[TraceFrontend.scala 161:52]
  wire  traceInBlock_io_pcSample_valid; // @[TraceFrontend.scala 161:52]
  wire [38:0] traceInBlock_io_pcSample_bits; // @[TraceFrontend.scala 161:52]
  wire  traceTrigBlock_rf_reset; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_clock; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_reset; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_0_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_0_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_1_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_1_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_2_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_2_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_3_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_3_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_4_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_4_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_5_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_5_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_6_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_6_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_7_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_7_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_8_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_8_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_9_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_9_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_10_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_10_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_11_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_11_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_12_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_12_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_13_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_13_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_14_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_14_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_auto_bpwatch_in_15_valid_0; // @[TraceFrontend.scala 162:52]
  wire [2:0] traceTrigBlock_auto_bpwatch_in_15_action; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_teActiveCore; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_0; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_1; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_2; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_3; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_4; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_5; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_6; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_7; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_8; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_9; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_10; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_11; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_12; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_13; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_14; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_wpControl_mem_0_15; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_iretire_0; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_ivalid_0; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_ingValid; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_ingTrigger_0_traceon; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_ingTrigger_0_traceoff; // @[TraceFrontend.scala 162:52]
  wire  traceTrigBlock_io_ingTrigger_0_wpsync; // @[TraceFrontend.scala 162:52]
  RHEA__TraceInBlock traceInBlock ( // @[TraceFrontend.scala 161:52]
    .rf_reset(traceInBlock_rf_reset),
    .clock(traceInBlock_clock),
    .reset(traceInBlock_reset),
    .auto_trace_legacy_in_0_valid(traceInBlock_auto_trace_legacy_in_0_valid),
    .auto_trace_legacy_in_0_iaddr(traceInBlock_auto_trace_legacy_in_0_iaddr),
    .auto_trace_legacy_in_0_insn(traceInBlock_auto_trace_legacy_in_0_insn),
    .auto_trace_legacy_in_0_priv(traceInBlock_auto_trace_legacy_in_0_priv),
    .auto_trace_legacy_in_0_exception(traceInBlock_auto_trace_legacy_in_0_exception),
    .auto_trace_legacy_in_0_interrupt(traceInBlock_auto_trace_legacy_in_0_interrupt),
    .io_teActive(traceInBlock_io_teActive),
    .io_teActiveCore(traceInBlock_io_teActiveCore),
    .io_tlClock(traceInBlock_io_tlClock),
    .io_tlReset(traceInBlock_io_tlReset),
    .io_hartIsInReset(traceInBlock_io_hartIsInReset),
    .io_ingControlAsync_mem_0_teEnable(traceInBlock_io_ingControlAsync_mem_0_teEnable),
    .io_ingControlAsync_mem_0_teTracing(traceInBlock_io_ingControlAsync_mem_0_teTracing),
    .io_ingControlAsync_mem_0_teInstruction(traceInBlock_io_ingControlAsync_mem_0_teInstruction),
    .io_ingControlAsync_mem_0_teStallEnable(traceInBlock_io_ingControlAsync_mem_0_teStallEnable),
    .io_ingControlAsync_mem_0_teSyncMaxInst(traceInBlock_io_ingControlAsync_mem_0_teSyncMaxInst),
    .io_ingControlAsync_mem_0_teSyncMaxBTM(traceInBlock_io_ingControlAsync_mem_0_teSyncMaxBTM),
    .io_ingControlAsync_ridx(traceInBlock_io_ingControlAsync_ridx),
    .io_ingControlAsync_widx(traceInBlock_io_ingControlAsync_widx),
    .io_traceOn(traceInBlock_io_traceOn),
    .io_iretire_0(traceInBlock_io_iretire_0),
    .io_ivalid_0(traceInBlock_io_ivalid_0),
    .io_ingValid(traceInBlock_io_ingValid),
    .io_ingTrigger_0_traceon(traceInBlock_io_ingTrigger_0_traceon),
    .io_ingTrigger_0_traceoff(traceInBlock_io_ingTrigger_0_traceoff),
    .io_ingTrigger_0_wpsync(traceInBlock_io_ingTrigger_0_wpsync),
    .io_teIdle(traceInBlock_io_teIdle),
    .io_btmEvent_ready(traceInBlock_io_btmEvent_ready),
    .io_btmEvent_valid(traceInBlock_io_btmEvent_valid),
    .io_btmEvent_bits_reason(traceInBlock_io_btmEvent_bits_reason),
    .io_btmEvent_bits_iaddr(traceInBlock_io_btmEvent_bits_iaddr),
    .io_btmEvent_bits_icnt(traceInBlock_io_btmEvent_bits_icnt),
    .io_btmEvent_bits_hist(traceInBlock_io_btmEvent_bits_hist),
    .io_btmEvent_bits_hisv(traceInBlock_io_btmEvent_bits_hisv),
    .io_traceStall(traceInBlock_io_traceStall),
    .io_pcActive(traceInBlock_io_pcActive),
    .io_pcCapture_valid(traceInBlock_io_pcCapture_valid),
    .io_pcCapture_bits(traceInBlock_io_pcCapture_bits),
    .io_pcSample_valid(traceInBlock_io_pcSample_valid),
    .io_pcSample_bits(traceInBlock_io_pcSample_bits)
  );
  RHEA__TraceTrigBlock traceTrigBlock ( // @[TraceFrontend.scala 162:52]
    .rf_reset(traceTrigBlock_rf_reset),
    .clock(traceTrigBlock_clock),
    .reset(traceTrigBlock_reset),
    .auto_bpwatch_in_0_valid_0(traceTrigBlock_auto_bpwatch_in_0_valid_0),
    .auto_bpwatch_in_0_action(traceTrigBlock_auto_bpwatch_in_0_action),
    .auto_bpwatch_in_1_valid_0(traceTrigBlock_auto_bpwatch_in_1_valid_0),
    .auto_bpwatch_in_1_action(traceTrigBlock_auto_bpwatch_in_1_action),
    .auto_bpwatch_in_2_valid_0(traceTrigBlock_auto_bpwatch_in_2_valid_0),
    .auto_bpwatch_in_2_action(traceTrigBlock_auto_bpwatch_in_2_action),
    .auto_bpwatch_in_3_valid_0(traceTrigBlock_auto_bpwatch_in_3_valid_0),
    .auto_bpwatch_in_3_action(traceTrigBlock_auto_bpwatch_in_3_action),
    .auto_bpwatch_in_4_valid_0(traceTrigBlock_auto_bpwatch_in_4_valid_0),
    .auto_bpwatch_in_4_action(traceTrigBlock_auto_bpwatch_in_4_action),
    .auto_bpwatch_in_5_valid_0(traceTrigBlock_auto_bpwatch_in_5_valid_0),
    .auto_bpwatch_in_5_action(traceTrigBlock_auto_bpwatch_in_5_action),
    .auto_bpwatch_in_6_valid_0(traceTrigBlock_auto_bpwatch_in_6_valid_0),
    .auto_bpwatch_in_6_action(traceTrigBlock_auto_bpwatch_in_6_action),
    .auto_bpwatch_in_7_valid_0(traceTrigBlock_auto_bpwatch_in_7_valid_0),
    .auto_bpwatch_in_7_action(traceTrigBlock_auto_bpwatch_in_7_action),
    .auto_bpwatch_in_8_valid_0(traceTrigBlock_auto_bpwatch_in_8_valid_0),
    .auto_bpwatch_in_8_action(traceTrigBlock_auto_bpwatch_in_8_action),
    .auto_bpwatch_in_9_valid_0(traceTrigBlock_auto_bpwatch_in_9_valid_0),
    .auto_bpwatch_in_9_action(traceTrigBlock_auto_bpwatch_in_9_action),
    .auto_bpwatch_in_10_valid_0(traceTrigBlock_auto_bpwatch_in_10_valid_0),
    .auto_bpwatch_in_10_action(traceTrigBlock_auto_bpwatch_in_10_action),
    .auto_bpwatch_in_11_valid_0(traceTrigBlock_auto_bpwatch_in_11_valid_0),
    .auto_bpwatch_in_11_action(traceTrigBlock_auto_bpwatch_in_11_action),
    .auto_bpwatch_in_12_valid_0(traceTrigBlock_auto_bpwatch_in_12_valid_0),
    .auto_bpwatch_in_12_action(traceTrigBlock_auto_bpwatch_in_12_action),
    .auto_bpwatch_in_13_valid_0(traceTrigBlock_auto_bpwatch_in_13_valid_0),
    .auto_bpwatch_in_13_action(traceTrigBlock_auto_bpwatch_in_13_action),
    .auto_bpwatch_in_14_valid_0(traceTrigBlock_auto_bpwatch_in_14_valid_0),
    .auto_bpwatch_in_14_action(traceTrigBlock_auto_bpwatch_in_14_action),
    .auto_bpwatch_in_15_valid_0(traceTrigBlock_auto_bpwatch_in_15_valid_0),
    .auto_bpwatch_in_15_action(traceTrigBlock_auto_bpwatch_in_15_action),
    .io_teActiveCore(traceTrigBlock_io_teActiveCore),
    .io_wpControl_mem_0_0(traceTrigBlock_io_wpControl_mem_0_0),
    .io_wpControl_mem_0_1(traceTrigBlock_io_wpControl_mem_0_1),
    .io_wpControl_mem_0_2(traceTrigBlock_io_wpControl_mem_0_2),
    .io_wpControl_mem_0_3(traceTrigBlock_io_wpControl_mem_0_3),
    .io_wpControl_mem_0_4(traceTrigBlock_io_wpControl_mem_0_4),
    .io_wpControl_mem_0_5(traceTrigBlock_io_wpControl_mem_0_5),
    .io_wpControl_mem_0_6(traceTrigBlock_io_wpControl_mem_0_6),
    .io_wpControl_mem_0_7(traceTrigBlock_io_wpControl_mem_0_7),
    .io_wpControl_mem_0_8(traceTrigBlock_io_wpControl_mem_0_8),
    .io_wpControl_mem_0_9(traceTrigBlock_io_wpControl_mem_0_9),
    .io_wpControl_mem_0_10(traceTrigBlock_io_wpControl_mem_0_10),
    .io_wpControl_mem_0_11(traceTrigBlock_io_wpControl_mem_0_11),
    .io_wpControl_mem_0_12(traceTrigBlock_io_wpControl_mem_0_12),
    .io_wpControl_mem_0_13(traceTrigBlock_io_wpControl_mem_0_13),
    .io_wpControl_mem_0_14(traceTrigBlock_io_wpControl_mem_0_14),
    .io_wpControl_mem_0_15(traceTrigBlock_io_wpControl_mem_0_15),
    .io_iretire_0(traceTrigBlock_io_iretire_0),
    .io_ivalid_0(traceTrigBlock_io_ivalid_0),
    .io_ingValid(traceTrigBlock_io_ingValid),
    .io_ingTrigger_0_traceon(traceTrigBlock_io_ingTrigger_0_traceon),
    .io_ingTrigger_0_traceoff(traceTrigBlock_io_ingTrigger_0_traceoff),
    .io_ingTrigger_0_wpsync(traceTrigBlock_io_ingTrigger_0_wpsync)
  );
  assign traceInBlock_rf_reset = rf_reset;
  assign traceTrigBlock_rf_reset = rf_reset;
  assign rf_reset = reset;
  assign auto_trace_aux_out_stall = traceInBlock_io_traceStall; // @[BundleBridge.scala 66:109 TraceFrontend.scala 256:33]
  assign io_ingControlAsync_ridx = traceInBlock_io_ingControlAsync_ridx; // @[TraceFrontend.scala 244:84]
  assign io_traceOn = traceInBlock_io_traceOn; // @[TraceFrontend.scala 246:18]
  assign io_traceStall = traceInBlock_io_traceStall; // @[TraceFrontend.scala 254:21]
  assign io_pcCapture_valid = traceInBlock_io_pcCapture_valid; // @[TraceFrontend.scala 250:20]
  assign io_pcCapture_bits = traceInBlock_io_pcCapture_bits; // @[TraceFrontend.scala 250:20]
  assign io_pcSample_valid = traceInBlock_io_pcSample_valid; // @[TraceFrontend.scala 251:20]
  assign io_pcSample_bits = traceInBlock_io_pcSample_bits; // @[TraceFrontend.scala 251:20]
  assign io_btmEvent_valid = traceInBlock_io_btmEvent_valid; // @[TraceFrontend.scala 255:19]
  assign io_btmEvent_bits_reason = traceInBlock_io_btmEvent_bits_reason; // @[TraceFrontend.scala 255:19]
  assign io_btmEvent_bits_iaddr = traceInBlock_io_btmEvent_bits_iaddr; // @[TraceFrontend.scala 255:19]
  assign io_btmEvent_bits_icnt = traceInBlock_io_btmEvent_bits_icnt; // @[TraceFrontend.scala 255:19]
  assign io_btmEvent_bits_hist = traceInBlock_io_btmEvent_bits_hist; // @[TraceFrontend.scala 255:19]
  assign io_btmEvent_bits_hisv = traceInBlock_io_btmEvent_bits_hisv; // @[TraceFrontend.scala 255:19]
  assign traceInBlock_clock = clock;
  assign traceInBlock_reset = reset;
  assign traceInBlock_auto_trace_legacy_in_0_valid = auto_traceInBlock_trace_legacy_in_0_valid; // @[LazyModule.scala 388:16]
  assign traceInBlock_auto_trace_legacy_in_0_iaddr = auto_traceInBlock_trace_legacy_in_0_iaddr; // @[LazyModule.scala 388:16]
  assign traceInBlock_auto_trace_legacy_in_0_insn = auto_traceInBlock_trace_legacy_in_0_insn; // @[LazyModule.scala 388:16]
  assign traceInBlock_auto_trace_legacy_in_0_priv = auto_traceInBlock_trace_legacy_in_0_priv; // @[LazyModule.scala 388:16]
  assign traceInBlock_auto_trace_legacy_in_0_exception = auto_traceInBlock_trace_legacy_in_0_exception; // @[LazyModule.scala 388:16]
  assign traceInBlock_auto_trace_legacy_in_0_interrupt = auto_traceInBlock_trace_legacy_in_0_interrupt; // @[LazyModule.scala 388:16]
  assign traceInBlock_io_teActive = io_teActive; // @[TraceFrontend.scala 242:34]
  assign traceInBlock_io_tlClock = io_tlClock; // @[TraceFrontend.scala 240:33]
  assign traceInBlock_io_tlReset = io_tlReset; // @[TraceFrontend.scala 241:33]
  assign traceInBlock_io_hartIsInReset = io_hartIsInReset; // @[TraceFrontend.scala 243:39]
  assign traceInBlock_io_ingControlAsync_mem_0_teEnable = io_ingControlAsync_mem_0_teEnable; // @[TraceFrontend.scala 244:84]
  assign traceInBlock_io_ingControlAsync_mem_0_teTracing = io_ingControlAsync_mem_0_teTracing; // @[TraceFrontend.scala 244:84]
  assign traceInBlock_io_ingControlAsync_mem_0_teInstruction = io_ingControlAsync_mem_0_teInstruction; // @[TraceFrontend.scala 244:84]
  assign traceInBlock_io_ingControlAsync_mem_0_teStallEnable = io_ingControlAsync_mem_0_teStallEnable; // @[TraceFrontend.scala 244:84]
  assign traceInBlock_io_ingControlAsync_mem_0_teSyncMaxInst = io_ingControlAsync_mem_0_teSyncMaxInst; // @[TraceFrontend.scala 244:84]
  assign traceInBlock_io_ingControlAsync_mem_0_teSyncMaxBTM = io_ingControlAsync_mem_0_teSyncMaxBTM; // @[TraceFrontend.scala 244:84]
  assign traceInBlock_io_ingControlAsync_widx = io_ingControlAsync_widx; // @[TraceFrontend.scala 244:84]
  assign traceInBlock_io_ingTrigger_0_traceon = traceTrigBlock_io_ingTrigger_0_traceon; // @[TraceFrontend.scala 272:36]
  assign traceInBlock_io_ingTrigger_0_traceoff = traceTrigBlock_io_ingTrigger_0_traceoff; // @[TraceFrontend.scala 272:36]
  assign traceInBlock_io_ingTrigger_0_wpsync = traceTrigBlock_io_ingTrigger_0_wpsync; // @[TraceFrontend.scala 272:36]
  assign traceInBlock_io_teIdle = io_teIdle; // @[TraceFrontend.scala 248:32]
  assign traceInBlock_io_btmEvent_ready = io_btmEvent_ready; // @[TraceFrontend.scala 255:19]
  assign traceInBlock_io_pcActive = io_pcActive; // @[TraceFrontend.scala 249:34]
  assign traceTrigBlock_clock = clock;
  assign traceTrigBlock_reset = reset;
  assign traceTrigBlock_auto_bpwatch_in_0_valid_0 = auto_traceTrigBlock_bpwatch_in_0_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_0_action = auto_traceTrigBlock_bpwatch_in_0_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_1_valid_0 = auto_traceTrigBlock_bpwatch_in_1_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_1_action = auto_traceTrigBlock_bpwatch_in_1_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_2_valid_0 = auto_traceTrigBlock_bpwatch_in_2_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_2_action = auto_traceTrigBlock_bpwatch_in_2_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_3_valid_0 = auto_traceTrigBlock_bpwatch_in_3_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_3_action = auto_traceTrigBlock_bpwatch_in_3_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_4_valid_0 = auto_traceTrigBlock_bpwatch_in_4_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_4_action = auto_traceTrigBlock_bpwatch_in_4_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_5_valid_0 = auto_traceTrigBlock_bpwatch_in_5_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_5_action = auto_traceTrigBlock_bpwatch_in_5_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_6_valid_0 = auto_traceTrigBlock_bpwatch_in_6_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_6_action = auto_traceTrigBlock_bpwatch_in_6_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_7_valid_0 = auto_traceTrigBlock_bpwatch_in_7_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_7_action = auto_traceTrigBlock_bpwatch_in_7_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_8_valid_0 = auto_traceTrigBlock_bpwatch_in_8_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_8_action = auto_traceTrigBlock_bpwatch_in_8_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_9_valid_0 = auto_traceTrigBlock_bpwatch_in_9_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_9_action = auto_traceTrigBlock_bpwatch_in_9_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_10_valid_0 = auto_traceTrigBlock_bpwatch_in_10_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_10_action = auto_traceTrigBlock_bpwatch_in_10_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_11_valid_0 = auto_traceTrigBlock_bpwatch_in_11_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_11_action = auto_traceTrigBlock_bpwatch_in_11_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_12_valid_0 = auto_traceTrigBlock_bpwatch_in_12_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_12_action = auto_traceTrigBlock_bpwatch_in_12_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_13_valid_0 = auto_traceTrigBlock_bpwatch_in_13_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_13_action = auto_traceTrigBlock_bpwatch_in_13_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_14_valid_0 = auto_traceTrigBlock_bpwatch_in_14_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_14_action = auto_traceTrigBlock_bpwatch_in_14_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_15_valid_0 = auto_traceTrigBlock_bpwatch_in_15_valid_0; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_auto_bpwatch_in_15_action = auto_traceTrigBlock_bpwatch_in_15_action; // @[LazyModule.scala 388:16]
  assign traceTrigBlock_io_teActiveCore = traceInBlock_io_teActiveCore; // @[TraceFrontend.scala 259:38]
  assign traceTrigBlock_io_wpControl_mem_0_0 = io_wpControl_mem_0_0; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_wpControl_mem_0_1 = io_wpControl_mem_0_1; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_wpControl_mem_0_2 = io_wpControl_mem_0_2; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_wpControl_mem_0_3 = io_wpControl_mem_0_3; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_wpControl_mem_0_4 = io_wpControl_mem_0_4; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_wpControl_mem_0_5 = io_wpControl_mem_0_5; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_wpControl_mem_0_6 = io_wpControl_mem_0_6; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_wpControl_mem_0_7 = io_wpControl_mem_0_7; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_wpControl_mem_0_8 = io_wpControl_mem_0_8; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_wpControl_mem_0_9 = io_wpControl_mem_0_9; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_wpControl_mem_0_10 = io_wpControl_mem_0_10; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_wpControl_mem_0_11 = io_wpControl_mem_0_11; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_wpControl_mem_0_12 = io_wpControl_mem_0_12; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_wpControl_mem_0_13 = io_wpControl_mem_0_13; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_wpControl_mem_0_14 = io_wpControl_mem_0_14; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_wpControl_mem_0_15 = io_wpControl_mem_0_15; // @[TraceFrontend.scala 264:21]
  assign traceTrigBlock_io_iretire_0 = traceInBlock_io_iretire_0; // @[TraceFrontend.scala 268:33]
  assign traceTrigBlock_io_ivalid_0 = traceInBlock_io_ivalid_0; // @[TraceFrontend.scala 269:32]
  assign traceTrigBlock_io_ingValid = traceInBlock_io_ingValid; // @[TraceFrontend.scala 270:34]
endmodule
