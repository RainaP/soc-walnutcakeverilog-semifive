//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLToAXI4BridgeContents_1(
  input         rf_reset,
  input         clock,
  input         reset,
  input         auto_axi4_out_aw_ready,
  output        auto_axi4_out_aw_valid,
  output [31:0] auto_axi4_out_aw_bits_addr,
  output [7:0]  auto_axi4_out_aw_bits_len,
  output [2:0]  auto_axi4_out_aw_bits_size,
  output [3:0]  auto_axi4_out_aw_bits_cache,
  output [2:0]  auto_axi4_out_aw_bits_prot,
  input         auto_axi4_out_w_ready,
  output        auto_axi4_out_w_valid,
  output [63:0] auto_axi4_out_w_bits_data,
  output [7:0]  auto_axi4_out_w_bits_strb,
  output        auto_axi4_out_w_bits_last,
  output        auto_axi4_out_b_ready,
  input         auto_axi4_out_b_valid,
  input  [1:0]  auto_axi4_out_b_bits_resp,
  input         auto_axi4_out_ar_ready,
  output        auto_axi4_out_ar_valid,
  output [31:0] auto_axi4_out_ar_bits_addr,
  output [7:0]  auto_axi4_out_ar_bits_len,
  output [2:0]  auto_axi4_out_ar_bits_size,
  output [3:0]  auto_axi4_out_ar_bits_cache,
  output [2:0]  auto_axi4_out_ar_bits_prot,
  output        auto_axi4_out_r_ready,
  input         auto_axi4_out_r_valid,
  input  [63:0] auto_axi4_out_r_bits_data,
  input  [1:0]  auto_axi4_out_r_bits_resp,
  input         auto_axi4_out_r_bits_last,
  output        auto_tl_in_a_ready,
  input         auto_tl_in_a_valid,
  input  [2:0]  auto_tl_in_a_bits_opcode,
  input  [2:0]  auto_tl_in_a_bits_param,
  input  [2:0]  auto_tl_in_a_bits_size,
  input  [6:0]  auto_tl_in_a_bits_source,
  input  [31:0] auto_tl_in_a_bits_address,
  input         auto_tl_in_a_bits_user_amba_prot_bufferable,
  input         auto_tl_in_a_bits_user_amba_prot_modifiable,
  input         auto_tl_in_a_bits_user_amba_prot_readalloc,
  input         auto_tl_in_a_bits_user_amba_prot_writealloc,
  input         auto_tl_in_a_bits_user_amba_prot_privileged,
  input         auto_tl_in_a_bits_user_amba_prot_secure,
  input         auto_tl_in_a_bits_user_amba_prot_fetch,
  input  [7:0]  auto_tl_in_a_bits_mask,
  input  [63:0] auto_tl_in_a_bits_data,
  input         auto_tl_in_a_bits_corrupt,
  input         auto_tl_in_d_ready,
  output        auto_tl_in_d_valid,
  output [2:0]  auto_tl_in_d_bits_opcode,
  output [2:0]  auto_tl_in_d_bits_size,
  output [6:0]  auto_tl_in_d_bits_source,
  output        auto_tl_in_d_bits_denied,
  output [63:0] auto_tl_in_d_bits_data,
  output        auto_tl_in_d_bits_corrupt
);
  wire  fragmenter_rf_reset; // @[Fragmenter.scala 333:34]
  wire  fragmenter_clock; // @[Fragmenter.scala 333:34]
  wire  fragmenter_reset; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_a_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_a_bits_param; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_a_bits_size; // @[Fragmenter.scala 333:34]
  wire [6:0] fragmenter_auto_in_a_bits_source; // @[Fragmenter.scala 333:34]
  wire [31:0] fragmenter_auto_in_a_bits_address; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_bits_user_amba_prot_bufferable; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_bits_user_amba_prot_modifiable; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_bits_user_amba_prot_readalloc; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_bits_user_amba_prot_writealloc; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_bits_user_amba_prot_privileged; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_bits_user_amba_prot_secure; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_bits_user_amba_prot_fetch; // @[Fragmenter.scala 333:34]
  wire [7:0] fragmenter_auto_in_a_bits_mask; // @[Fragmenter.scala 333:34]
  wire [63:0] fragmenter_auto_in_a_bits_data; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_bits_corrupt; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_d_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_d_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_d_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_d_bits_size; // @[Fragmenter.scala 333:34]
  wire [6:0] fragmenter_auto_in_d_bits_source; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_d_bits_denied; // @[Fragmenter.scala 333:34]
  wire [63:0] fragmenter_auto_in_d_bits_data; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_d_bits_corrupt; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_a_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_a_bits_param; // @[Fragmenter.scala 333:34]
  wire [1:0] fragmenter_auto_out_a_bits_size; // @[Fragmenter.scala 333:34]
  wire [10:0] fragmenter_auto_out_a_bits_source; // @[Fragmenter.scala 333:34]
  wire [31:0] fragmenter_auto_out_a_bits_address; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_bits_user_amba_prot_bufferable; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_bits_user_amba_prot_modifiable; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_bits_user_amba_prot_readalloc; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_bits_user_amba_prot_writealloc; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_bits_user_amba_prot_privileged; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_bits_user_amba_prot_secure; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_bits_user_amba_prot_fetch; // @[Fragmenter.scala 333:34]
  wire [7:0] fragmenter_auto_out_a_bits_mask; // @[Fragmenter.scala 333:34]
  wire [63:0] fragmenter_auto_out_a_bits_data; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_bits_corrupt; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_d_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_d_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_d_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [1:0] fragmenter_auto_out_d_bits_size; // @[Fragmenter.scala 333:34]
  wire [10:0] fragmenter_auto_out_d_bits_source; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_d_bits_denied; // @[Fragmenter.scala 333:34]
  wire [63:0] fragmenter_auto_out_d_bits_data; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_d_bits_corrupt; // @[Fragmenter.scala 333:34]
  wire  axi4buf_rf_reset; // @[Buffer.scala 58:29]
  wire  axi4buf_clock; // @[Buffer.scala 58:29]
  wire  axi4buf_reset; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_aw_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_aw_valid; // @[Buffer.scala 58:29]
  wire [31:0] axi4buf_auto_in_aw_bits_addr; // @[Buffer.scala 58:29]
  wire [7:0] axi4buf_auto_in_aw_bits_len; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_in_aw_bits_size; // @[Buffer.scala 58:29]
  wire [3:0] axi4buf_auto_in_aw_bits_cache; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_in_aw_bits_prot; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_w_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_w_valid; // @[Buffer.scala 58:29]
  wire [63:0] axi4buf_auto_in_w_bits_data; // @[Buffer.scala 58:29]
  wire [7:0] axi4buf_auto_in_w_bits_strb; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_w_bits_last; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_b_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_b_valid; // @[Buffer.scala 58:29]
  wire [1:0] axi4buf_auto_in_b_bits_resp; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_ar_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_ar_valid; // @[Buffer.scala 58:29]
  wire [31:0] axi4buf_auto_in_ar_bits_addr; // @[Buffer.scala 58:29]
  wire [7:0] axi4buf_auto_in_ar_bits_len; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_in_ar_bits_size; // @[Buffer.scala 58:29]
  wire [3:0] axi4buf_auto_in_ar_bits_cache; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_in_ar_bits_prot; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_r_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_r_valid; // @[Buffer.scala 58:29]
  wire [63:0] axi4buf_auto_in_r_bits_data; // @[Buffer.scala 58:29]
  wire [1:0] axi4buf_auto_in_r_bits_resp; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_r_bits_last; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_aw_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_aw_valid; // @[Buffer.scala 58:29]
  wire [31:0] axi4buf_auto_out_aw_bits_addr; // @[Buffer.scala 58:29]
  wire [7:0] axi4buf_auto_out_aw_bits_len; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_out_aw_bits_size; // @[Buffer.scala 58:29]
  wire [3:0] axi4buf_auto_out_aw_bits_cache; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_out_aw_bits_prot; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_w_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_w_valid; // @[Buffer.scala 58:29]
  wire [63:0] axi4buf_auto_out_w_bits_data; // @[Buffer.scala 58:29]
  wire [7:0] axi4buf_auto_out_w_bits_strb; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_w_bits_last; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_b_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_b_valid; // @[Buffer.scala 58:29]
  wire [1:0] axi4buf_auto_out_b_bits_resp; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_ar_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_ar_valid; // @[Buffer.scala 58:29]
  wire [31:0] axi4buf_auto_out_ar_bits_addr; // @[Buffer.scala 58:29]
  wire [7:0] axi4buf_auto_out_ar_bits_len; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_out_ar_bits_size; // @[Buffer.scala 58:29]
  wire [3:0] axi4buf_auto_out_ar_bits_cache; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_out_ar_bits_prot; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_r_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_r_valid; // @[Buffer.scala 58:29]
  wire [63:0] axi4buf_auto_out_r_bits_data; // @[Buffer.scala 58:29]
  wire [1:0] axi4buf_auto_out_r_bits_resp; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_r_bits_last; // @[Buffer.scala 58:29]
  wire  axi4yank_rf_reset; // @[UserYanker.scala 113:30]
  wire  axi4yank_clock; // @[UserYanker.scala 113:30]
  wire  axi4yank_reset; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_aw_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_aw_valid; // @[UserYanker.scala 113:30]
  wire [31:0] axi4yank_auto_in_aw_bits_addr; // @[UserYanker.scala 113:30]
  wire [7:0] axi4yank_auto_in_aw_bits_len; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_in_aw_bits_size; // @[UserYanker.scala 113:30]
  wire [3:0] axi4yank_auto_in_aw_bits_cache; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_in_aw_bits_prot; // @[UserYanker.scala 113:30]
  wire [3:0] axi4yank_auto_in_aw_bits_echo_tl_state_size; // @[UserYanker.scala 113:30]
  wire [10:0] axi4yank_auto_in_aw_bits_echo_tl_state_source; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_w_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_w_valid; // @[UserYanker.scala 113:30]
  wire [63:0] axi4yank_auto_in_w_bits_data; // @[UserYanker.scala 113:30]
  wire [7:0] axi4yank_auto_in_w_bits_strb; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_w_bits_last; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_b_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_b_valid; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_in_b_bits_resp; // @[UserYanker.scala 113:30]
  wire [3:0] axi4yank_auto_in_b_bits_echo_tl_state_size; // @[UserYanker.scala 113:30]
  wire [10:0] axi4yank_auto_in_b_bits_echo_tl_state_source; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_ar_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_ar_valid; // @[UserYanker.scala 113:30]
  wire [31:0] axi4yank_auto_in_ar_bits_addr; // @[UserYanker.scala 113:30]
  wire [7:0] axi4yank_auto_in_ar_bits_len; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_in_ar_bits_size; // @[UserYanker.scala 113:30]
  wire [3:0] axi4yank_auto_in_ar_bits_cache; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_in_ar_bits_prot; // @[UserYanker.scala 113:30]
  wire [3:0] axi4yank_auto_in_ar_bits_echo_tl_state_size; // @[UserYanker.scala 113:30]
  wire [10:0] axi4yank_auto_in_ar_bits_echo_tl_state_source; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_r_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_r_valid; // @[UserYanker.scala 113:30]
  wire [63:0] axi4yank_auto_in_r_bits_data; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_in_r_bits_resp; // @[UserYanker.scala 113:30]
  wire [3:0] axi4yank_auto_in_r_bits_echo_tl_state_size; // @[UserYanker.scala 113:30]
  wire [10:0] axi4yank_auto_in_r_bits_echo_tl_state_source; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_r_bits_last; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_aw_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_aw_valid; // @[UserYanker.scala 113:30]
  wire [31:0] axi4yank_auto_out_aw_bits_addr; // @[UserYanker.scala 113:30]
  wire [7:0] axi4yank_auto_out_aw_bits_len; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_out_aw_bits_size; // @[UserYanker.scala 113:30]
  wire [3:0] axi4yank_auto_out_aw_bits_cache; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_out_aw_bits_prot; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_w_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_w_valid; // @[UserYanker.scala 113:30]
  wire [63:0] axi4yank_auto_out_w_bits_data; // @[UserYanker.scala 113:30]
  wire [7:0] axi4yank_auto_out_w_bits_strb; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_w_bits_last; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_b_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_b_valid; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_out_b_bits_resp; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_ar_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_ar_valid; // @[UserYanker.scala 113:30]
  wire [31:0] axi4yank_auto_out_ar_bits_addr; // @[UserYanker.scala 113:30]
  wire [7:0] axi4yank_auto_out_ar_bits_len; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_out_ar_bits_size; // @[UserYanker.scala 113:30]
  wire [3:0] axi4yank_auto_out_ar_bits_cache; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_out_ar_bits_prot; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_r_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_r_valid; // @[UserYanker.scala 113:30]
  wire [63:0] axi4yank_auto_out_r_bits_data; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_out_r_bits_resp; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_r_bits_last; // @[UserYanker.scala 113:30]
  wire  axi4index_auto_in_aw_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_aw_valid; // @[IdIndexer.scala 107:31]
  wire [31:0] axi4index_auto_in_aw_bits_addr; // @[IdIndexer.scala 107:31]
  wire [7:0] axi4index_auto_in_aw_bits_len; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_in_aw_bits_size; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_in_aw_bits_cache; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_in_aw_bits_prot; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_in_aw_bits_echo_tl_state_size; // @[IdIndexer.scala 107:31]
  wire [10:0] axi4index_auto_in_aw_bits_echo_tl_state_source; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_w_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_w_valid; // @[IdIndexer.scala 107:31]
  wire [63:0] axi4index_auto_in_w_bits_data; // @[IdIndexer.scala 107:31]
  wire [7:0] axi4index_auto_in_w_bits_strb; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_w_bits_last; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_b_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_b_valid; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_in_b_bits_resp; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_in_b_bits_echo_tl_state_size; // @[IdIndexer.scala 107:31]
  wire [10:0] axi4index_auto_in_b_bits_echo_tl_state_source; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_ar_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_ar_valid; // @[IdIndexer.scala 107:31]
  wire [31:0] axi4index_auto_in_ar_bits_addr; // @[IdIndexer.scala 107:31]
  wire [7:0] axi4index_auto_in_ar_bits_len; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_in_ar_bits_size; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_in_ar_bits_cache; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_in_ar_bits_prot; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_in_ar_bits_echo_tl_state_size; // @[IdIndexer.scala 107:31]
  wire [10:0] axi4index_auto_in_ar_bits_echo_tl_state_source; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_r_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_r_valid; // @[IdIndexer.scala 107:31]
  wire [63:0] axi4index_auto_in_r_bits_data; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_in_r_bits_resp; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_in_r_bits_echo_tl_state_size; // @[IdIndexer.scala 107:31]
  wire [10:0] axi4index_auto_in_r_bits_echo_tl_state_source; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_r_bits_last; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_aw_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_aw_valid; // @[IdIndexer.scala 107:31]
  wire [31:0] axi4index_auto_out_aw_bits_addr; // @[IdIndexer.scala 107:31]
  wire [7:0] axi4index_auto_out_aw_bits_len; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_out_aw_bits_size; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_out_aw_bits_cache; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_out_aw_bits_prot; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_out_aw_bits_echo_tl_state_size; // @[IdIndexer.scala 107:31]
  wire [10:0] axi4index_auto_out_aw_bits_echo_tl_state_source; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_w_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_w_valid; // @[IdIndexer.scala 107:31]
  wire [63:0] axi4index_auto_out_w_bits_data; // @[IdIndexer.scala 107:31]
  wire [7:0] axi4index_auto_out_w_bits_strb; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_w_bits_last; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_b_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_b_valid; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_out_b_bits_resp; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_out_b_bits_echo_tl_state_size; // @[IdIndexer.scala 107:31]
  wire [10:0] axi4index_auto_out_b_bits_echo_tl_state_source; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_ar_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_ar_valid; // @[IdIndexer.scala 107:31]
  wire [31:0] axi4index_auto_out_ar_bits_addr; // @[IdIndexer.scala 107:31]
  wire [7:0] axi4index_auto_out_ar_bits_len; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_out_ar_bits_size; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_out_ar_bits_cache; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_out_ar_bits_prot; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_out_ar_bits_echo_tl_state_size; // @[IdIndexer.scala 107:31]
  wire [10:0] axi4index_auto_out_ar_bits_echo_tl_state_source; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_r_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_r_valid; // @[IdIndexer.scala 107:31]
  wire [63:0] axi4index_auto_out_r_bits_data; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_out_r_bits_resp; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_out_r_bits_echo_tl_state_size; // @[IdIndexer.scala 107:31]
  wire [10:0] axi4index_auto_out_r_bits_echo_tl_state_source; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_r_bits_last; // @[IdIndexer.scala 107:31]
  wire  tl2axi4_rf_reset; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_clock; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_reset; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_in_a_ready; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_in_a_valid; // @[ToAXI4.scala 279:29]
  wire [2:0] tl2axi4_auto_in_a_bits_opcode; // @[ToAXI4.scala 279:29]
  wire [2:0] tl2axi4_auto_in_a_bits_param; // @[ToAXI4.scala 279:29]
  wire [1:0] tl2axi4_auto_in_a_bits_size; // @[ToAXI4.scala 279:29]
  wire [10:0] tl2axi4_auto_in_a_bits_source; // @[ToAXI4.scala 279:29]
  wire [31:0] tl2axi4_auto_in_a_bits_address; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_in_a_bits_user_amba_prot_bufferable; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_in_a_bits_user_amba_prot_modifiable; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_in_a_bits_user_amba_prot_readalloc; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_in_a_bits_user_amba_prot_writealloc; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_in_a_bits_user_amba_prot_privileged; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_in_a_bits_user_amba_prot_secure; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_in_a_bits_user_amba_prot_fetch; // @[ToAXI4.scala 279:29]
  wire [7:0] tl2axi4_auto_in_a_bits_mask; // @[ToAXI4.scala 279:29]
  wire [63:0] tl2axi4_auto_in_a_bits_data; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_in_a_bits_corrupt; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_in_d_ready; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_in_d_valid; // @[ToAXI4.scala 279:29]
  wire [2:0] tl2axi4_auto_in_d_bits_opcode; // @[ToAXI4.scala 279:29]
  wire [1:0] tl2axi4_auto_in_d_bits_size; // @[ToAXI4.scala 279:29]
  wire [10:0] tl2axi4_auto_in_d_bits_source; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_in_d_bits_denied; // @[ToAXI4.scala 279:29]
  wire [63:0] tl2axi4_auto_in_d_bits_data; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_in_d_bits_corrupt; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_out_aw_ready; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_out_aw_valid; // @[ToAXI4.scala 279:29]
  wire [31:0] tl2axi4_auto_out_aw_bits_addr; // @[ToAXI4.scala 279:29]
  wire [7:0] tl2axi4_auto_out_aw_bits_len; // @[ToAXI4.scala 279:29]
  wire [2:0] tl2axi4_auto_out_aw_bits_size; // @[ToAXI4.scala 279:29]
  wire [3:0] tl2axi4_auto_out_aw_bits_cache; // @[ToAXI4.scala 279:29]
  wire [2:0] tl2axi4_auto_out_aw_bits_prot; // @[ToAXI4.scala 279:29]
  wire [3:0] tl2axi4_auto_out_aw_bits_echo_tl_state_size; // @[ToAXI4.scala 279:29]
  wire [10:0] tl2axi4_auto_out_aw_bits_echo_tl_state_source; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_out_w_ready; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_out_w_valid; // @[ToAXI4.scala 279:29]
  wire [63:0] tl2axi4_auto_out_w_bits_data; // @[ToAXI4.scala 279:29]
  wire [7:0] tl2axi4_auto_out_w_bits_strb; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_out_w_bits_last; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_out_b_ready; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_out_b_valid; // @[ToAXI4.scala 279:29]
  wire [1:0] tl2axi4_auto_out_b_bits_resp; // @[ToAXI4.scala 279:29]
  wire [3:0] tl2axi4_auto_out_b_bits_echo_tl_state_size; // @[ToAXI4.scala 279:29]
  wire [10:0] tl2axi4_auto_out_b_bits_echo_tl_state_source; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_out_ar_ready; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_out_ar_valid; // @[ToAXI4.scala 279:29]
  wire [31:0] tl2axi4_auto_out_ar_bits_addr; // @[ToAXI4.scala 279:29]
  wire [7:0] tl2axi4_auto_out_ar_bits_len; // @[ToAXI4.scala 279:29]
  wire [2:0] tl2axi4_auto_out_ar_bits_size; // @[ToAXI4.scala 279:29]
  wire [3:0] tl2axi4_auto_out_ar_bits_cache; // @[ToAXI4.scala 279:29]
  wire [2:0] tl2axi4_auto_out_ar_bits_prot; // @[ToAXI4.scala 279:29]
  wire [3:0] tl2axi4_auto_out_ar_bits_echo_tl_state_size; // @[ToAXI4.scala 279:29]
  wire [10:0] tl2axi4_auto_out_ar_bits_echo_tl_state_source; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_out_r_ready; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_out_r_valid; // @[ToAXI4.scala 279:29]
  wire [63:0] tl2axi4_auto_out_r_bits_data; // @[ToAXI4.scala 279:29]
  wire [1:0] tl2axi4_auto_out_r_bits_resp; // @[ToAXI4.scala 279:29]
  wire [3:0] tl2axi4_auto_out_r_bits_echo_tl_state_size; // @[ToAXI4.scala 279:29]
  wire [10:0] tl2axi4_auto_out_r_bits_echo_tl_state_source; // @[ToAXI4.scala 279:29]
  wire  tl2axi4_auto_out_r_bits_last; // @[ToAXI4.scala 279:29]
  wire  widget_auto_in_a_ready;
  wire  widget_auto_in_a_valid;
  wire [2:0] widget_auto_in_a_bits_opcode;
  wire [2:0] widget_auto_in_a_bits_param;
  wire [2:0] widget_auto_in_a_bits_size;
  wire [6:0] widget_auto_in_a_bits_source;
  wire [31:0] widget_auto_in_a_bits_address;
  wire  widget_auto_in_a_bits_user_amba_prot_bufferable;
  wire  widget_auto_in_a_bits_user_amba_prot_modifiable;
  wire  widget_auto_in_a_bits_user_amba_prot_readalloc;
  wire  widget_auto_in_a_bits_user_amba_prot_writealloc;
  wire  widget_auto_in_a_bits_user_amba_prot_privileged;
  wire  widget_auto_in_a_bits_user_amba_prot_secure;
  wire  widget_auto_in_a_bits_user_amba_prot_fetch;
  wire [7:0] widget_auto_in_a_bits_mask;
  wire [63:0] widget_auto_in_a_bits_data;
  wire  widget_auto_in_a_bits_corrupt;
  wire  widget_auto_in_d_ready;
  wire  widget_auto_in_d_valid;
  wire [2:0] widget_auto_in_d_bits_opcode;
  wire [2:0] widget_auto_in_d_bits_size;
  wire [6:0] widget_auto_in_d_bits_source;
  wire  widget_auto_in_d_bits_denied;
  wire [63:0] widget_auto_in_d_bits_data;
  wire  widget_auto_in_d_bits_corrupt;
  wire  widget_auto_out_a_ready;
  wire  widget_auto_out_a_valid;
  wire [2:0] widget_auto_out_a_bits_opcode;
  wire [2:0] widget_auto_out_a_bits_param;
  wire [2:0] widget_auto_out_a_bits_size;
  wire [6:0] widget_auto_out_a_bits_source;
  wire [31:0] widget_auto_out_a_bits_address;
  wire  widget_auto_out_a_bits_user_amba_prot_bufferable;
  wire  widget_auto_out_a_bits_user_amba_prot_modifiable;
  wire  widget_auto_out_a_bits_user_amba_prot_readalloc;
  wire  widget_auto_out_a_bits_user_amba_prot_writealloc;
  wire  widget_auto_out_a_bits_user_amba_prot_privileged;
  wire  widget_auto_out_a_bits_user_amba_prot_secure;
  wire  widget_auto_out_a_bits_user_amba_prot_fetch;
  wire [7:0] widget_auto_out_a_bits_mask;
  wire [63:0] widget_auto_out_a_bits_data;
  wire  widget_auto_out_a_bits_corrupt;
  wire  widget_auto_out_d_ready;
  wire  widget_auto_out_d_valid;
  wire [2:0] widget_auto_out_d_bits_opcode;
  wire [2:0] widget_auto_out_d_bits_size;
  wire [6:0] widget_auto_out_d_bits_source;
  wire  widget_auto_out_d_bits_denied;
  wire [63:0] widget_auto_out_d_bits_data;
  wire  widget_auto_out_d_bits_corrupt;
  wire  buffer_auto_in_a_ready;
  wire  buffer_auto_in_a_valid;
  wire [2:0] buffer_auto_in_a_bits_opcode;
  wire [2:0] buffer_auto_in_a_bits_param;
  wire [2:0] buffer_auto_in_a_bits_size;
  wire [6:0] buffer_auto_in_a_bits_source;
  wire [31:0] buffer_auto_in_a_bits_address;
  wire  buffer_auto_in_a_bits_user_amba_prot_bufferable;
  wire  buffer_auto_in_a_bits_user_amba_prot_modifiable;
  wire  buffer_auto_in_a_bits_user_amba_prot_readalloc;
  wire  buffer_auto_in_a_bits_user_amba_prot_writealloc;
  wire  buffer_auto_in_a_bits_user_amba_prot_privileged;
  wire  buffer_auto_in_a_bits_user_amba_prot_secure;
  wire  buffer_auto_in_a_bits_user_amba_prot_fetch;
  wire [7:0] buffer_auto_in_a_bits_mask;
  wire [63:0] buffer_auto_in_a_bits_data;
  wire  buffer_auto_in_a_bits_corrupt;
  wire  buffer_auto_in_d_ready;
  wire  buffer_auto_in_d_valid;
  wire [2:0] buffer_auto_in_d_bits_opcode;
  wire [2:0] buffer_auto_in_d_bits_size;
  wire [6:0] buffer_auto_in_d_bits_source;
  wire  buffer_auto_in_d_bits_denied;
  wire [63:0] buffer_auto_in_d_bits_data;
  wire  buffer_auto_in_d_bits_corrupt;
  wire  buffer_auto_out_a_ready;
  wire  buffer_auto_out_a_valid;
  wire [2:0] buffer_auto_out_a_bits_opcode;
  wire [2:0] buffer_auto_out_a_bits_param;
  wire [2:0] buffer_auto_out_a_bits_size;
  wire [6:0] buffer_auto_out_a_bits_source;
  wire [31:0] buffer_auto_out_a_bits_address;
  wire  buffer_auto_out_a_bits_user_amba_prot_bufferable;
  wire  buffer_auto_out_a_bits_user_amba_prot_modifiable;
  wire  buffer_auto_out_a_bits_user_amba_prot_readalloc;
  wire  buffer_auto_out_a_bits_user_amba_prot_writealloc;
  wire  buffer_auto_out_a_bits_user_amba_prot_privileged;
  wire  buffer_auto_out_a_bits_user_amba_prot_secure;
  wire  buffer_auto_out_a_bits_user_amba_prot_fetch;
  wire [7:0] buffer_auto_out_a_bits_mask;
  wire [63:0] buffer_auto_out_a_bits_data;
  wire  buffer_auto_out_a_bits_corrupt;
  wire  buffer_auto_out_d_ready;
  wire  buffer_auto_out_d_valid;
  wire [2:0] buffer_auto_out_d_bits_opcode;
  wire [2:0] buffer_auto_out_d_bits_size;
  wire [6:0] buffer_auto_out_d_bits_source;
  wire  buffer_auto_out_d_bits_denied;
  wire [63:0] buffer_auto_out_d_bits_data;
  wire  buffer_auto_out_d_bits_corrupt;
  RHEA__TLFragmenter_5 fragmenter ( // @[Fragmenter.scala 333:34]
    .rf_reset(fragmenter_rf_reset),
    .clock(fragmenter_clock),
    .reset(fragmenter_reset),
    .auto_in_a_ready(fragmenter_auto_in_a_ready),
    .auto_in_a_valid(fragmenter_auto_in_a_valid),
    .auto_in_a_bits_opcode(fragmenter_auto_in_a_bits_opcode),
    .auto_in_a_bits_param(fragmenter_auto_in_a_bits_param),
    .auto_in_a_bits_size(fragmenter_auto_in_a_bits_size),
    .auto_in_a_bits_source(fragmenter_auto_in_a_bits_source),
    .auto_in_a_bits_address(fragmenter_auto_in_a_bits_address),
    .auto_in_a_bits_user_amba_prot_bufferable(fragmenter_auto_in_a_bits_user_amba_prot_bufferable),
    .auto_in_a_bits_user_amba_prot_modifiable(fragmenter_auto_in_a_bits_user_amba_prot_modifiable),
    .auto_in_a_bits_user_amba_prot_readalloc(fragmenter_auto_in_a_bits_user_amba_prot_readalloc),
    .auto_in_a_bits_user_amba_prot_writealloc(fragmenter_auto_in_a_bits_user_amba_prot_writealloc),
    .auto_in_a_bits_user_amba_prot_privileged(fragmenter_auto_in_a_bits_user_amba_prot_privileged),
    .auto_in_a_bits_user_amba_prot_secure(fragmenter_auto_in_a_bits_user_amba_prot_secure),
    .auto_in_a_bits_user_amba_prot_fetch(fragmenter_auto_in_a_bits_user_amba_prot_fetch),
    .auto_in_a_bits_mask(fragmenter_auto_in_a_bits_mask),
    .auto_in_a_bits_data(fragmenter_auto_in_a_bits_data),
    .auto_in_a_bits_corrupt(fragmenter_auto_in_a_bits_corrupt),
    .auto_in_d_ready(fragmenter_auto_in_d_ready),
    .auto_in_d_valid(fragmenter_auto_in_d_valid),
    .auto_in_d_bits_opcode(fragmenter_auto_in_d_bits_opcode),
    .auto_in_d_bits_size(fragmenter_auto_in_d_bits_size),
    .auto_in_d_bits_source(fragmenter_auto_in_d_bits_source),
    .auto_in_d_bits_denied(fragmenter_auto_in_d_bits_denied),
    .auto_in_d_bits_data(fragmenter_auto_in_d_bits_data),
    .auto_in_d_bits_corrupt(fragmenter_auto_in_d_bits_corrupt),
    .auto_out_a_ready(fragmenter_auto_out_a_ready),
    .auto_out_a_valid(fragmenter_auto_out_a_valid),
    .auto_out_a_bits_opcode(fragmenter_auto_out_a_bits_opcode),
    .auto_out_a_bits_param(fragmenter_auto_out_a_bits_param),
    .auto_out_a_bits_size(fragmenter_auto_out_a_bits_size),
    .auto_out_a_bits_source(fragmenter_auto_out_a_bits_source),
    .auto_out_a_bits_address(fragmenter_auto_out_a_bits_address),
    .auto_out_a_bits_user_amba_prot_bufferable(fragmenter_auto_out_a_bits_user_amba_prot_bufferable),
    .auto_out_a_bits_user_amba_prot_modifiable(fragmenter_auto_out_a_bits_user_amba_prot_modifiable),
    .auto_out_a_bits_user_amba_prot_readalloc(fragmenter_auto_out_a_bits_user_amba_prot_readalloc),
    .auto_out_a_bits_user_amba_prot_writealloc(fragmenter_auto_out_a_bits_user_amba_prot_writealloc),
    .auto_out_a_bits_user_amba_prot_privileged(fragmenter_auto_out_a_bits_user_amba_prot_privileged),
    .auto_out_a_bits_user_amba_prot_secure(fragmenter_auto_out_a_bits_user_amba_prot_secure),
    .auto_out_a_bits_user_amba_prot_fetch(fragmenter_auto_out_a_bits_user_amba_prot_fetch),
    .auto_out_a_bits_mask(fragmenter_auto_out_a_bits_mask),
    .auto_out_a_bits_data(fragmenter_auto_out_a_bits_data),
    .auto_out_a_bits_corrupt(fragmenter_auto_out_a_bits_corrupt),
    .auto_out_d_ready(fragmenter_auto_out_d_ready),
    .auto_out_d_valid(fragmenter_auto_out_d_valid),
    .auto_out_d_bits_opcode(fragmenter_auto_out_d_bits_opcode),
    .auto_out_d_bits_size(fragmenter_auto_out_d_bits_size),
    .auto_out_d_bits_source(fragmenter_auto_out_d_bits_source),
    .auto_out_d_bits_denied(fragmenter_auto_out_d_bits_denied),
    .auto_out_d_bits_data(fragmenter_auto_out_d_bits_data),
    .auto_out_d_bits_corrupt(fragmenter_auto_out_d_bits_corrupt)
  );
  RHEA__AXI4Buffer_2 axi4buf ( // @[Buffer.scala 58:29]
    .rf_reset(axi4buf_rf_reset),
    .clock(axi4buf_clock),
    .reset(axi4buf_reset),
    .auto_in_aw_ready(axi4buf_auto_in_aw_ready),
    .auto_in_aw_valid(axi4buf_auto_in_aw_valid),
    .auto_in_aw_bits_addr(axi4buf_auto_in_aw_bits_addr),
    .auto_in_aw_bits_len(axi4buf_auto_in_aw_bits_len),
    .auto_in_aw_bits_size(axi4buf_auto_in_aw_bits_size),
    .auto_in_aw_bits_cache(axi4buf_auto_in_aw_bits_cache),
    .auto_in_aw_bits_prot(axi4buf_auto_in_aw_bits_prot),
    .auto_in_w_ready(axi4buf_auto_in_w_ready),
    .auto_in_w_valid(axi4buf_auto_in_w_valid),
    .auto_in_w_bits_data(axi4buf_auto_in_w_bits_data),
    .auto_in_w_bits_strb(axi4buf_auto_in_w_bits_strb),
    .auto_in_w_bits_last(axi4buf_auto_in_w_bits_last),
    .auto_in_b_ready(axi4buf_auto_in_b_ready),
    .auto_in_b_valid(axi4buf_auto_in_b_valid),
    .auto_in_b_bits_resp(axi4buf_auto_in_b_bits_resp),
    .auto_in_ar_ready(axi4buf_auto_in_ar_ready),
    .auto_in_ar_valid(axi4buf_auto_in_ar_valid),
    .auto_in_ar_bits_addr(axi4buf_auto_in_ar_bits_addr),
    .auto_in_ar_bits_len(axi4buf_auto_in_ar_bits_len),
    .auto_in_ar_bits_size(axi4buf_auto_in_ar_bits_size),
    .auto_in_ar_bits_cache(axi4buf_auto_in_ar_bits_cache),
    .auto_in_ar_bits_prot(axi4buf_auto_in_ar_bits_prot),
    .auto_in_r_ready(axi4buf_auto_in_r_ready),
    .auto_in_r_valid(axi4buf_auto_in_r_valid),
    .auto_in_r_bits_data(axi4buf_auto_in_r_bits_data),
    .auto_in_r_bits_resp(axi4buf_auto_in_r_bits_resp),
    .auto_in_r_bits_last(axi4buf_auto_in_r_bits_last),
    .auto_out_aw_ready(axi4buf_auto_out_aw_ready),
    .auto_out_aw_valid(axi4buf_auto_out_aw_valid),
    .auto_out_aw_bits_addr(axi4buf_auto_out_aw_bits_addr),
    .auto_out_aw_bits_len(axi4buf_auto_out_aw_bits_len),
    .auto_out_aw_bits_size(axi4buf_auto_out_aw_bits_size),
    .auto_out_aw_bits_cache(axi4buf_auto_out_aw_bits_cache),
    .auto_out_aw_bits_prot(axi4buf_auto_out_aw_bits_prot),
    .auto_out_w_ready(axi4buf_auto_out_w_ready),
    .auto_out_w_valid(axi4buf_auto_out_w_valid),
    .auto_out_w_bits_data(axi4buf_auto_out_w_bits_data),
    .auto_out_w_bits_strb(axi4buf_auto_out_w_bits_strb),
    .auto_out_w_bits_last(axi4buf_auto_out_w_bits_last),
    .auto_out_b_ready(axi4buf_auto_out_b_ready),
    .auto_out_b_valid(axi4buf_auto_out_b_valid),
    .auto_out_b_bits_resp(axi4buf_auto_out_b_bits_resp),
    .auto_out_ar_ready(axi4buf_auto_out_ar_ready),
    .auto_out_ar_valid(axi4buf_auto_out_ar_valid),
    .auto_out_ar_bits_addr(axi4buf_auto_out_ar_bits_addr),
    .auto_out_ar_bits_len(axi4buf_auto_out_ar_bits_len),
    .auto_out_ar_bits_size(axi4buf_auto_out_ar_bits_size),
    .auto_out_ar_bits_cache(axi4buf_auto_out_ar_bits_cache),
    .auto_out_ar_bits_prot(axi4buf_auto_out_ar_bits_prot),
    .auto_out_r_ready(axi4buf_auto_out_r_ready),
    .auto_out_r_valid(axi4buf_auto_out_r_valid),
    .auto_out_r_bits_data(axi4buf_auto_out_r_bits_data),
    .auto_out_r_bits_resp(axi4buf_auto_out_r_bits_resp),
    .auto_out_r_bits_last(axi4buf_auto_out_r_bits_last)
  );
  RHEA__AXI4UserYanker_2 axi4yank ( // @[UserYanker.scala 113:30]
    .rf_reset(axi4yank_rf_reset),
    .clock(axi4yank_clock),
    .reset(axi4yank_reset),
    .auto_in_aw_ready(axi4yank_auto_in_aw_ready),
    .auto_in_aw_valid(axi4yank_auto_in_aw_valid),
    .auto_in_aw_bits_addr(axi4yank_auto_in_aw_bits_addr),
    .auto_in_aw_bits_len(axi4yank_auto_in_aw_bits_len),
    .auto_in_aw_bits_size(axi4yank_auto_in_aw_bits_size),
    .auto_in_aw_bits_cache(axi4yank_auto_in_aw_bits_cache),
    .auto_in_aw_bits_prot(axi4yank_auto_in_aw_bits_prot),
    .auto_in_aw_bits_echo_tl_state_size(axi4yank_auto_in_aw_bits_echo_tl_state_size),
    .auto_in_aw_bits_echo_tl_state_source(axi4yank_auto_in_aw_bits_echo_tl_state_source),
    .auto_in_w_ready(axi4yank_auto_in_w_ready),
    .auto_in_w_valid(axi4yank_auto_in_w_valid),
    .auto_in_w_bits_data(axi4yank_auto_in_w_bits_data),
    .auto_in_w_bits_strb(axi4yank_auto_in_w_bits_strb),
    .auto_in_w_bits_last(axi4yank_auto_in_w_bits_last),
    .auto_in_b_ready(axi4yank_auto_in_b_ready),
    .auto_in_b_valid(axi4yank_auto_in_b_valid),
    .auto_in_b_bits_resp(axi4yank_auto_in_b_bits_resp),
    .auto_in_b_bits_echo_tl_state_size(axi4yank_auto_in_b_bits_echo_tl_state_size),
    .auto_in_b_bits_echo_tl_state_source(axi4yank_auto_in_b_bits_echo_tl_state_source),
    .auto_in_ar_ready(axi4yank_auto_in_ar_ready),
    .auto_in_ar_valid(axi4yank_auto_in_ar_valid),
    .auto_in_ar_bits_addr(axi4yank_auto_in_ar_bits_addr),
    .auto_in_ar_bits_len(axi4yank_auto_in_ar_bits_len),
    .auto_in_ar_bits_size(axi4yank_auto_in_ar_bits_size),
    .auto_in_ar_bits_cache(axi4yank_auto_in_ar_bits_cache),
    .auto_in_ar_bits_prot(axi4yank_auto_in_ar_bits_prot),
    .auto_in_ar_bits_echo_tl_state_size(axi4yank_auto_in_ar_bits_echo_tl_state_size),
    .auto_in_ar_bits_echo_tl_state_source(axi4yank_auto_in_ar_bits_echo_tl_state_source),
    .auto_in_r_ready(axi4yank_auto_in_r_ready),
    .auto_in_r_valid(axi4yank_auto_in_r_valid),
    .auto_in_r_bits_data(axi4yank_auto_in_r_bits_data),
    .auto_in_r_bits_resp(axi4yank_auto_in_r_bits_resp),
    .auto_in_r_bits_echo_tl_state_size(axi4yank_auto_in_r_bits_echo_tl_state_size),
    .auto_in_r_bits_echo_tl_state_source(axi4yank_auto_in_r_bits_echo_tl_state_source),
    .auto_in_r_bits_last(axi4yank_auto_in_r_bits_last),
    .auto_out_aw_ready(axi4yank_auto_out_aw_ready),
    .auto_out_aw_valid(axi4yank_auto_out_aw_valid),
    .auto_out_aw_bits_addr(axi4yank_auto_out_aw_bits_addr),
    .auto_out_aw_bits_len(axi4yank_auto_out_aw_bits_len),
    .auto_out_aw_bits_size(axi4yank_auto_out_aw_bits_size),
    .auto_out_aw_bits_cache(axi4yank_auto_out_aw_bits_cache),
    .auto_out_aw_bits_prot(axi4yank_auto_out_aw_bits_prot),
    .auto_out_w_ready(axi4yank_auto_out_w_ready),
    .auto_out_w_valid(axi4yank_auto_out_w_valid),
    .auto_out_w_bits_data(axi4yank_auto_out_w_bits_data),
    .auto_out_w_bits_strb(axi4yank_auto_out_w_bits_strb),
    .auto_out_w_bits_last(axi4yank_auto_out_w_bits_last),
    .auto_out_b_ready(axi4yank_auto_out_b_ready),
    .auto_out_b_valid(axi4yank_auto_out_b_valid),
    .auto_out_b_bits_resp(axi4yank_auto_out_b_bits_resp),
    .auto_out_ar_ready(axi4yank_auto_out_ar_ready),
    .auto_out_ar_valid(axi4yank_auto_out_ar_valid),
    .auto_out_ar_bits_addr(axi4yank_auto_out_ar_bits_addr),
    .auto_out_ar_bits_len(axi4yank_auto_out_ar_bits_len),
    .auto_out_ar_bits_size(axi4yank_auto_out_ar_bits_size),
    .auto_out_ar_bits_cache(axi4yank_auto_out_ar_bits_cache),
    .auto_out_ar_bits_prot(axi4yank_auto_out_ar_bits_prot),
    .auto_out_r_ready(axi4yank_auto_out_r_ready),
    .auto_out_r_valid(axi4yank_auto_out_r_valid),
    .auto_out_r_bits_data(axi4yank_auto_out_r_bits_data),
    .auto_out_r_bits_resp(axi4yank_auto_out_r_bits_resp),
    .auto_out_r_bits_last(axi4yank_auto_out_r_bits_last)
  );
  RHEA__AXI4IdIndexer_2 axi4index ( // @[IdIndexer.scala 107:31]
    .auto_in_aw_ready(axi4index_auto_in_aw_ready),
    .auto_in_aw_valid(axi4index_auto_in_aw_valid),
    .auto_in_aw_bits_addr(axi4index_auto_in_aw_bits_addr),
    .auto_in_aw_bits_len(axi4index_auto_in_aw_bits_len),
    .auto_in_aw_bits_size(axi4index_auto_in_aw_bits_size),
    .auto_in_aw_bits_cache(axi4index_auto_in_aw_bits_cache),
    .auto_in_aw_bits_prot(axi4index_auto_in_aw_bits_prot),
    .auto_in_aw_bits_echo_tl_state_size(axi4index_auto_in_aw_bits_echo_tl_state_size),
    .auto_in_aw_bits_echo_tl_state_source(axi4index_auto_in_aw_bits_echo_tl_state_source),
    .auto_in_w_ready(axi4index_auto_in_w_ready),
    .auto_in_w_valid(axi4index_auto_in_w_valid),
    .auto_in_w_bits_data(axi4index_auto_in_w_bits_data),
    .auto_in_w_bits_strb(axi4index_auto_in_w_bits_strb),
    .auto_in_w_bits_last(axi4index_auto_in_w_bits_last),
    .auto_in_b_ready(axi4index_auto_in_b_ready),
    .auto_in_b_valid(axi4index_auto_in_b_valid),
    .auto_in_b_bits_resp(axi4index_auto_in_b_bits_resp),
    .auto_in_b_bits_echo_tl_state_size(axi4index_auto_in_b_bits_echo_tl_state_size),
    .auto_in_b_bits_echo_tl_state_source(axi4index_auto_in_b_bits_echo_tl_state_source),
    .auto_in_ar_ready(axi4index_auto_in_ar_ready),
    .auto_in_ar_valid(axi4index_auto_in_ar_valid),
    .auto_in_ar_bits_addr(axi4index_auto_in_ar_bits_addr),
    .auto_in_ar_bits_len(axi4index_auto_in_ar_bits_len),
    .auto_in_ar_bits_size(axi4index_auto_in_ar_bits_size),
    .auto_in_ar_bits_cache(axi4index_auto_in_ar_bits_cache),
    .auto_in_ar_bits_prot(axi4index_auto_in_ar_bits_prot),
    .auto_in_ar_bits_echo_tl_state_size(axi4index_auto_in_ar_bits_echo_tl_state_size),
    .auto_in_ar_bits_echo_tl_state_source(axi4index_auto_in_ar_bits_echo_tl_state_source),
    .auto_in_r_ready(axi4index_auto_in_r_ready),
    .auto_in_r_valid(axi4index_auto_in_r_valid),
    .auto_in_r_bits_data(axi4index_auto_in_r_bits_data),
    .auto_in_r_bits_resp(axi4index_auto_in_r_bits_resp),
    .auto_in_r_bits_echo_tl_state_size(axi4index_auto_in_r_bits_echo_tl_state_size),
    .auto_in_r_bits_echo_tl_state_source(axi4index_auto_in_r_bits_echo_tl_state_source),
    .auto_in_r_bits_last(axi4index_auto_in_r_bits_last),
    .auto_out_aw_ready(axi4index_auto_out_aw_ready),
    .auto_out_aw_valid(axi4index_auto_out_aw_valid),
    .auto_out_aw_bits_addr(axi4index_auto_out_aw_bits_addr),
    .auto_out_aw_bits_len(axi4index_auto_out_aw_bits_len),
    .auto_out_aw_bits_size(axi4index_auto_out_aw_bits_size),
    .auto_out_aw_bits_cache(axi4index_auto_out_aw_bits_cache),
    .auto_out_aw_bits_prot(axi4index_auto_out_aw_bits_prot),
    .auto_out_aw_bits_echo_tl_state_size(axi4index_auto_out_aw_bits_echo_tl_state_size),
    .auto_out_aw_bits_echo_tl_state_source(axi4index_auto_out_aw_bits_echo_tl_state_source),
    .auto_out_w_ready(axi4index_auto_out_w_ready),
    .auto_out_w_valid(axi4index_auto_out_w_valid),
    .auto_out_w_bits_data(axi4index_auto_out_w_bits_data),
    .auto_out_w_bits_strb(axi4index_auto_out_w_bits_strb),
    .auto_out_w_bits_last(axi4index_auto_out_w_bits_last),
    .auto_out_b_ready(axi4index_auto_out_b_ready),
    .auto_out_b_valid(axi4index_auto_out_b_valid),
    .auto_out_b_bits_resp(axi4index_auto_out_b_bits_resp),
    .auto_out_b_bits_echo_tl_state_size(axi4index_auto_out_b_bits_echo_tl_state_size),
    .auto_out_b_bits_echo_tl_state_source(axi4index_auto_out_b_bits_echo_tl_state_source),
    .auto_out_ar_ready(axi4index_auto_out_ar_ready),
    .auto_out_ar_valid(axi4index_auto_out_ar_valid),
    .auto_out_ar_bits_addr(axi4index_auto_out_ar_bits_addr),
    .auto_out_ar_bits_len(axi4index_auto_out_ar_bits_len),
    .auto_out_ar_bits_size(axi4index_auto_out_ar_bits_size),
    .auto_out_ar_bits_cache(axi4index_auto_out_ar_bits_cache),
    .auto_out_ar_bits_prot(axi4index_auto_out_ar_bits_prot),
    .auto_out_ar_bits_echo_tl_state_size(axi4index_auto_out_ar_bits_echo_tl_state_size),
    .auto_out_ar_bits_echo_tl_state_source(axi4index_auto_out_ar_bits_echo_tl_state_source),
    .auto_out_r_ready(axi4index_auto_out_r_ready),
    .auto_out_r_valid(axi4index_auto_out_r_valid),
    .auto_out_r_bits_data(axi4index_auto_out_r_bits_data),
    .auto_out_r_bits_resp(axi4index_auto_out_r_bits_resp),
    .auto_out_r_bits_echo_tl_state_size(axi4index_auto_out_r_bits_echo_tl_state_size),
    .auto_out_r_bits_echo_tl_state_source(axi4index_auto_out_r_bits_echo_tl_state_source),
    .auto_out_r_bits_last(axi4index_auto_out_r_bits_last)
  );
  RHEA__TLToAXI4_1 tl2axi4 ( // @[ToAXI4.scala 279:29]
    .rf_reset(tl2axi4_rf_reset),
    .clock(tl2axi4_clock),
    .reset(tl2axi4_reset),
    .auto_in_a_ready(tl2axi4_auto_in_a_ready),
    .auto_in_a_valid(tl2axi4_auto_in_a_valid),
    .auto_in_a_bits_opcode(tl2axi4_auto_in_a_bits_opcode),
    .auto_in_a_bits_param(tl2axi4_auto_in_a_bits_param),
    .auto_in_a_bits_size(tl2axi4_auto_in_a_bits_size),
    .auto_in_a_bits_source(tl2axi4_auto_in_a_bits_source),
    .auto_in_a_bits_address(tl2axi4_auto_in_a_bits_address),
    .auto_in_a_bits_user_amba_prot_bufferable(tl2axi4_auto_in_a_bits_user_amba_prot_bufferable),
    .auto_in_a_bits_user_amba_prot_modifiable(tl2axi4_auto_in_a_bits_user_amba_prot_modifiable),
    .auto_in_a_bits_user_amba_prot_readalloc(tl2axi4_auto_in_a_bits_user_amba_prot_readalloc),
    .auto_in_a_bits_user_amba_prot_writealloc(tl2axi4_auto_in_a_bits_user_amba_prot_writealloc),
    .auto_in_a_bits_user_amba_prot_privileged(tl2axi4_auto_in_a_bits_user_amba_prot_privileged),
    .auto_in_a_bits_user_amba_prot_secure(tl2axi4_auto_in_a_bits_user_amba_prot_secure),
    .auto_in_a_bits_user_amba_prot_fetch(tl2axi4_auto_in_a_bits_user_amba_prot_fetch),
    .auto_in_a_bits_mask(tl2axi4_auto_in_a_bits_mask),
    .auto_in_a_bits_data(tl2axi4_auto_in_a_bits_data),
    .auto_in_a_bits_corrupt(tl2axi4_auto_in_a_bits_corrupt),
    .auto_in_d_ready(tl2axi4_auto_in_d_ready),
    .auto_in_d_valid(tl2axi4_auto_in_d_valid),
    .auto_in_d_bits_opcode(tl2axi4_auto_in_d_bits_opcode),
    .auto_in_d_bits_size(tl2axi4_auto_in_d_bits_size),
    .auto_in_d_bits_source(tl2axi4_auto_in_d_bits_source),
    .auto_in_d_bits_denied(tl2axi4_auto_in_d_bits_denied),
    .auto_in_d_bits_data(tl2axi4_auto_in_d_bits_data),
    .auto_in_d_bits_corrupt(tl2axi4_auto_in_d_bits_corrupt),
    .auto_out_aw_ready(tl2axi4_auto_out_aw_ready),
    .auto_out_aw_valid(tl2axi4_auto_out_aw_valid),
    .auto_out_aw_bits_addr(tl2axi4_auto_out_aw_bits_addr),
    .auto_out_aw_bits_len(tl2axi4_auto_out_aw_bits_len),
    .auto_out_aw_bits_size(tl2axi4_auto_out_aw_bits_size),
    .auto_out_aw_bits_cache(tl2axi4_auto_out_aw_bits_cache),
    .auto_out_aw_bits_prot(tl2axi4_auto_out_aw_bits_prot),
    .auto_out_aw_bits_echo_tl_state_size(tl2axi4_auto_out_aw_bits_echo_tl_state_size),
    .auto_out_aw_bits_echo_tl_state_source(tl2axi4_auto_out_aw_bits_echo_tl_state_source),
    .auto_out_w_ready(tl2axi4_auto_out_w_ready),
    .auto_out_w_valid(tl2axi4_auto_out_w_valid),
    .auto_out_w_bits_data(tl2axi4_auto_out_w_bits_data),
    .auto_out_w_bits_strb(tl2axi4_auto_out_w_bits_strb),
    .auto_out_w_bits_last(tl2axi4_auto_out_w_bits_last),
    .auto_out_b_ready(tl2axi4_auto_out_b_ready),
    .auto_out_b_valid(tl2axi4_auto_out_b_valid),
    .auto_out_b_bits_resp(tl2axi4_auto_out_b_bits_resp),
    .auto_out_b_bits_echo_tl_state_size(tl2axi4_auto_out_b_bits_echo_tl_state_size),
    .auto_out_b_bits_echo_tl_state_source(tl2axi4_auto_out_b_bits_echo_tl_state_source),
    .auto_out_ar_ready(tl2axi4_auto_out_ar_ready),
    .auto_out_ar_valid(tl2axi4_auto_out_ar_valid),
    .auto_out_ar_bits_addr(tl2axi4_auto_out_ar_bits_addr),
    .auto_out_ar_bits_len(tl2axi4_auto_out_ar_bits_len),
    .auto_out_ar_bits_size(tl2axi4_auto_out_ar_bits_size),
    .auto_out_ar_bits_cache(tl2axi4_auto_out_ar_bits_cache),
    .auto_out_ar_bits_prot(tl2axi4_auto_out_ar_bits_prot),
    .auto_out_ar_bits_echo_tl_state_size(tl2axi4_auto_out_ar_bits_echo_tl_state_size),
    .auto_out_ar_bits_echo_tl_state_source(tl2axi4_auto_out_ar_bits_echo_tl_state_source),
    .auto_out_r_ready(tl2axi4_auto_out_r_ready),
    .auto_out_r_valid(tl2axi4_auto_out_r_valid),
    .auto_out_r_bits_data(tl2axi4_auto_out_r_bits_data),
    .auto_out_r_bits_resp(tl2axi4_auto_out_r_bits_resp),
    .auto_out_r_bits_echo_tl_state_size(tl2axi4_auto_out_r_bits_echo_tl_state_size),
    .auto_out_r_bits_echo_tl_state_source(tl2axi4_auto_out_r_bits_echo_tl_state_source),
    .auto_out_r_bits_last(tl2axi4_auto_out_r_bits_last)
  );
  assign fragmenter_rf_reset = rf_reset;
  assign axi4buf_rf_reset = rf_reset;
  assign axi4yank_rf_reset = rf_reset;
  assign tl2axi4_rf_reset = rf_reset;
  assign widget_auto_in_a_ready = widget_auto_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_in_d_valid = widget_auto_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_in_d_bits_opcode = widget_auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_in_d_bits_size = widget_auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_in_d_bits_source = widget_auto_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_in_d_bits_denied = widget_auto_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_in_d_bits_data = widget_auto_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_in_d_bits_corrupt = widget_auto_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_out_a_valid = widget_auto_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_opcode = widget_auto_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_param = widget_auto_in_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_size = widget_auto_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_source = widget_auto_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_address = widget_auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_user_amba_prot_bufferable = widget_auto_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_user_amba_prot_modifiable = widget_auto_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_user_amba_prot_readalloc = widget_auto_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_user_amba_prot_writealloc = widget_auto_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_user_amba_prot_privileged = widget_auto_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_user_amba_prot_secure = widget_auto_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_user_amba_prot_fetch = widget_auto_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_mask = widget_auto_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_data = widget_auto_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_corrupt = widget_auto_in_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_d_ready = widget_auto_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_ready = buffer_auto_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_valid = buffer_auto_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_opcode = buffer_auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_size = buffer_auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_source = buffer_auto_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_denied = buffer_auto_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_data = buffer_auto_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_corrupt = buffer_auto_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_a_valid = buffer_auto_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_opcode = buffer_auto_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_param = buffer_auto_in_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_size = buffer_auto_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_source = buffer_auto_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_address = buffer_auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_bufferable = buffer_auto_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_modifiable = buffer_auto_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_readalloc = buffer_auto_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_writealloc = buffer_auto_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_privileged = buffer_auto_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_secure = buffer_auto_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_fetch = buffer_auto_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_mask = buffer_auto_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_data = buffer_auto_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_corrupt = buffer_auto_in_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_d_ready = buffer_auto_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_axi4_out_aw_valid = axi4buf_auto_out_aw_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_aw_bits_addr = axi4buf_auto_out_aw_bits_addr; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_aw_bits_len = axi4buf_auto_out_aw_bits_len; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_aw_bits_size = axi4buf_auto_out_aw_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_aw_bits_cache = axi4buf_auto_out_aw_bits_cache; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_aw_bits_prot = axi4buf_auto_out_aw_bits_prot; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_w_valid = axi4buf_auto_out_w_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_w_bits_data = axi4buf_auto_out_w_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_w_bits_strb = axi4buf_auto_out_w_bits_strb; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_w_bits_last = axi4buf_auto_out_w_bits_last; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_b_ready = axi4buf_auto_out_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_ar_valid = axi4buf_auto_out_ar_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_ar_bits_addr = axi4buf_auto_out_ar_bits_addr; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_ar_bits_len = axi4buf_auto_out_ar_bits_len; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_ar_bits_size = axi4buf_auto_out_ar_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_ar_bits_cache = axi4buf_auto_out_ar_bits_cache; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_ar_bits_prot = axi4buf_auto_out_ar_bits_prot; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_out_r_ready = axi4buf_auto_out_r_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_in_a_ready = buffer_auto_in_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_valid = buffer_auto_in_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_opcode = buffer_auto_in_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_size = buffer_auto_in_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_source = buffer_auto_in_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_denied = buffer_auto_in_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_data = buffer_auto_in_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_corrupt = buffer_auto_in_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign fragmenter_clock = clock;
  assign fragmenter_reset = reset;
  assign fragmenter_auto_in_a_valid = widget_auto_out_a_valid; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_opcode = widget_auto_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_param = widget_auto_out_a_bits_param; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_size = widget_auto_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_source = widget_auto_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_address = widget_auto_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_user_amba_prot_bufferable = widget_auto_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_user_amba_prot_modifiable = widget_auto_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_user_amba_prot_readalloc = widget_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_user_amba_prot_writealloc = widget_auto_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_user_amba_prot_privileged = widget_auto_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_user_amba_prot_secure = widget_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_user_amba_prot_fetch = widget_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_mask = widget_auto_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_data = widget_auto_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_corrupt = widget_auto_out_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_d_ready = widget_auto_out_d_ready; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_out_a_ready = tl2axi4_auto_in_a_ready; // @[LazyModule.scala 377:16]
  assign fragmenter_auto_out_d_valid = tl2axi4_auto_in_d_valid; // @[LazyModule.scala 377:16]
  assign fragmenter_auto_out_d_bits_opcode = tl2axi4_auto_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign fragmenter_auto_out_d_bits_size = tl2axi4_auto_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign fragmenter_auto_out_d_bits_source = tl2axi4_auto_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign fragmenter_auto_out_d_bits_denied = tl2axi4_auto_in_d_bits_denied; // @[LazyModule.scala 377:16]
  assign fragmenter_auto_out_d_bits_data = tl2axi4_auto_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign fragmenter_auto_out_d_bits_corrupt = tl2axi4_auto_in_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign axi4buf_clock = clock;
  assign axi4buf_reset = reset;
  assign axi4buf_auto_in_aw_valid = axi4yank_auto_out_aw_valid; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_aw_bits_addr = axi4yank_auto_out_aw_bits_addr; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_aw_bits_len = axi4yank_auto_out_aw_bits_len; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_aw_bits_size = axi4yank_auto_out_aw_bits_size; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_aw_bits_cache = axi4yank_auto_out_aw_bits_cache; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_aw_bits_prot = axi4yank_auto_out_aw_bits_prot; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_w_valid = axi4yank_auto_out_w_valid; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_w_bits_data = axi4yank_auto_out_w_bits_data; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_w_bits_strb = axi4yank_auto_out_w_bits_strb; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_w_bits_last = axi4yank_auto_out_w_bits_last; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_b_ready = axi4yank_auto_out_b_ready; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_ar_valid = axi4yank_auto_out_ar_valid; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_ar_bits_addr = axi4yank_auto_out_ar_bits_addr; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_ar_bits_len = axi4yank_auto_out_ar_bits_len; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_ar_bits_size = axi4yank_auto_out_ar_bits_size; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_ar_bits_cache = axi4yank_auto_out_ar_bits_cache; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_ar_bits_prot = axi4yank_auto_out_ar_bits_prot; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_in_r_ready = axi4yank_auto_out_r_ready; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_out_aw_ready = auto_axi4_out_aw_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign axi4buf_auto_out_w_ready = auto_axi4_out_w_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign axi4buf_auto_out_b_valid = auto_axi4_out_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign axi4buf_auto_out_b_bits_resp = auto_axi4_out_b_bits_resp; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign axi4buf_auto_out_ar_ready = auto_axi4_out_ar_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign axi4buf_auto_out_r_valid = auto_axi4_out_r_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign axi4buf_auto_out_r_bits_data = auto_axi4_out_r_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign axi4buf_auto_out_r_bits_resp = auto_axi4_out_r_bits_resp; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign axi4buf_auto_out_r_bits_last = auto_axi4_out_r_bits_last; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign axi4yank_clock = clock;
  assign axi4yank_reset = reset;
  assign axi4yank_auto_in_aw_valid = axi4index_auto_out_aw_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_aw_bits_addr = axi4index_auto_out_aw_bits_addr; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_aw_bits_len = axi4index_auto_out_aw_bits_len; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_aw_bits_size = axi4index_auto_out_aw_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_aw_bits_cache = axi4index_auto_out_aw_bits_cache; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_aw_bits_prot = axi4index_auto_out_aw_bits_prot; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_aw_bits_echo_tl_state_size = axi4index_auto_out_aw_bits_echo_tl_state_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_aw_bits_echo_tl_state_source = axi4index_auto_out_aw_bits_echo_tl_state_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_w_valid = axi4index_auto_out_w_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_w_bits_data = axi4index_auto_out_w_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_w_bits_strb = axi4index_auto_out_w_bits_strb; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_w_bits_last = axi4index_auto_out_w_bits_last; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_b_ready = axi4index_auto_out_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_valid = axi4index_auto_out_ar_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_bits_addr = axi4index_auto_out_ar_bits_addr; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_bits_len = axi4index_auto_out_ar_bits_len; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_bits_size = axi4index_auto_out_ar_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_bits_cache = axi4index_auto_out_ar_bits_cache; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_bits_prot = axi4index_auto_out_ar_bits_prot; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_bits_echo_tl_state_size = axi4index_auto_out_ar_bits_echo_tl_state_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_bits_echo_tl_state_source = axi4index_auto_out_ar_bits_echo_tl_state_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_in_r_ready = axi4index_auto_out_r_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign axi4yank_auto_out_aw_ready = axi4buf_auto_in_aw_ready; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_w_ready = axi4buf_auto_in_w_ready; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_b_valid = axi4buf_auto_in_b_valid; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_b_bits_resp = axi4buf_auto_in_b_bits_resp; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_ar_ready = axi4buf_auto_in_ar_ready; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_r_valid = axi4buf_auto_in_r_valid; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_r_bits_data = axi4buf_auto_in_r_bits_data; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_r_bits_resp = axi4buf_auto_in_r_bits_resp; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_r_bits_last = axi4buf_auto_in_r_bits_last; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_valid = tl2axi4_auto_out_aw_valid; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_bits_addr = tl2axi4_auto_out_aw_bits_addr; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_bits_len = tl2axi4_auto_out_aw_bits_len; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_bits_size = tl2axi4_auto_out_aw_bits_size; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_bits_cache = tl2axi4_auto_out_aw_bits_cache; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_bits_prot = tl2axi4_auto_out_aw_bits_prot; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_bits_echo_tl_state_size = tl2axi4_auto_out_aw_bits_echo_tl_state_size; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_bits_echo_tl_state_source = tl2axi4_auto_out_aw_bits_echo_tl_state_source; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_w_valid = tl2axi4_auto_out_w_valid; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_w_bits_data = tl2axi4_auto_out_w_bits_data; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_w_bits_strb = tl2axi4_auto_out_w_bits_strb; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_w_bits_last = tl2axi4_auto_out_w_bits_last; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_b_ready = tl2axi4_auto_out_b_ready; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_valid = tl2axi4_auto_out_ar_valid; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_bits_addr = tl2axi4_auto_out_ar_bits_addr; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_bits_len = tl2axi4_auto_out_ar_bits_len; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_bits_size = tl2axi4_auto_out_ar_bits_size; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_bits_cache = tl2axi4_auto_out_ar_bits_cache; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_bits_prot = tl2axi4_auto_out_ar_bits_prot; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_bits_echo_tl_state_size = tl2axi4_auto_out_ar_bits_echo_tl_state_size; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_bits_echo_tl_state_source = tl2axi4_auto_out_ar_bits_echo_tl_state_source; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_r_ready = tl2axi4_auto_out_r_ready; // @[LazyModule.scala 375:16]
  assign axi4index_auto_out_aw_ready = axi4yank_auto_in_aw_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign axi4index_auto_out_w_ready = axi4yank_auto_in_w_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign axi4index_auto_out_b_valid = axi4yank_auto_in_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign axi4index_auto_out_b_bits_resp = axi4yank_auto_in_b_bits_resp; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign axi4index_auto_out_b_bits_echo_tl_state_size = axi4yank_auto_in_b_bits_echo_tl_state_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign axi4index_auto_out_b_bits_echo_tl_state_source = axi4yank_auto_in_b_bits_echo_tl_state_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign axi4index_auto_out_ar_ready = axi4yank_auto_in_ar_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign axi4index_auto_out_r_valid = axi4yank_auto_in_r_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign axi4index_auto_out_r_bits_data = axi4yank_auto_in_r_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign axi4index_auto_out_r_bits_resp = axi4yank_auto_in_r_bits_resp; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign axi4index_auto_out_r_bits_echo_tl_state_size = axi4yank_auto_in_r_bits_echo_tl_state_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign axi4index_auto_out_r_bits_echo_tl_state_source = axi4yank_auto_in_r_bits_echo_tl_state_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign axi4index_auto_out_r_bits_last = axi4yank_auto_in_r_bits_last; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign tl2axi4_clock = clock;
  assign tl2axi4_reset = reset;
  assign tl2axi4_auto_in_a_valid = fragmenter_auto_out_a_valid; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_a_bits_opcode = fragmenter_auto_out_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_a_bits_param = fragmenter_auto_out_a_bits_param; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_a_bits_size = fragmenter_auto_out_a_bits_size; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_a_bits_source = fragmenter_auto_out_a_bits_source; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_a_bits_address = fragmenter_auto_out_a_bits_address; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_a_bits_user_amba_prot_bufferable = fragmenter_auto_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_a_bits_user_amba_prot_modifiable = fragmenter_auto_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_a_bits_user_amba_prot_readalloc = fragmenter_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_a_bits_user_amba_prot_writealloc = fragmenter_auto_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_a_bits_user_amba_prot_privileged = fragmenter_auto_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_a_bits_user_amba_prot_secure = fragmenter_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_a_bits_user_amba_prot_fetch = fragmenter_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_a_bits_mask = fragmenter_auto_out_a_bits_mask; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_a_bits_data = fragmenter_auto_out_a_bits_data; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_a_bits_corrupt = fragmenter_auto_out_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_in_d_ready = fragmenter_auto_out_d_ready; // @[LazyModule.scala 377:16]
  assign tl2axi4_auto_out_aw_ready = axi4index_auto_in_aw_ready; // @[LazyModule.scala 375:16]
  assign tl2axi4_auto_out_w_ready = axi4index_auto_in_w_ready; // @[LazyModule.scala 375:16]
  assign tl2axi4_auto_out_b_valid = axi4index_auto_in_b_valid; // @[LazyModule.scala 375:16]
  assign tl2axi4_auto_out_b_bits_resp = axi4index_auto_in_b_bits_resp; // @[LazyModule.scala 375:16]
  assign tl2axi4_auto_out_b_bits_echo_tl_state_size = axi4index_auto_in_b_bits_echo_tl_state_size; // @[LazyModule.scala 375:16]
  assign tl2axi4_auto_out_b_bits_echo_tl_state_source = axi4index_auto_in_b_bits_echo_tl_state_source; // @[LazyModule.scala 375:16]
  assign tl2axi4_auto_out_ar_ready = axi4index_auto_in_ar_ready; // @[LazyModule.scala 375:16]
  assign tl2axi4_auto_out_r_valid = axi4index_auto_in_r_valid; // @[LazyModule.scala 375:16]
  assign tl2axi4_auto_out_r_bits_data = axi4index_auto_in_r_bits_data; // @[LazyModule.scala 375:16]
  assign tl2axi4_auto_out_r_bits_resp = axi4index_auto_in_r_bits_resp; // @[LazyModule.scala 375:16]
  assign tl2axi4_auto_out_r_bits_echo_tl_state_size = axi4index_auto_in_r_bits_echo_tl_state_size; // @[LazyModule.scala 375:16]
  assign tl2axi4_auto_out_r_bits_echo_tl_state_source = axi4index_auto_in_r_bits_echo_tl_state_source; // @[LazyModule.scala 375:16]
  assign tl2axi4_auto_out_r_bits_last = axi4index_auto_in_r_bits_last; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_valid = buffer_auto_out_a_valid; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_opcode = buffer_auto_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_param = buffer_auto_out_a_bits_param; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_size = buffer_auto_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_source = buffer_auto_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_address = buffer_auto_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_user_amba_prot_bufferable = buffer_auto_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_user_amba_prot_modifiable = buffer_auto_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_user_amba_prot_readalloc = buffer_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_user_amba_prot_writealloc = buffer_auto_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_user_amba_prot_privileged = buffer_auto_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_user_amba_prot_secure = buffer_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_user_amba_prot_fetch = buffer_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_mask = buffer_auto_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_data = buffer_auto_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_corrupt = buffer_auto_out_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign widget_auto_in_d_ready = buffer_auto_out_d_ready; // @[LazyModule.scala 375:16]
  assign widget_auto_out_a_ready = fragmenter_auto_in_a_ready; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_valid = fragmenter_auto_in_d_valid; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_opcode = fragmenter_auto_in_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_size = fragmenter_auto_in_d_bits_size; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_source = fragmenter_auto_in_d_bits_source; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_denied = fragmenter_auto_in_d_bits_denied; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_data = fragmenter_auto_in_d_bits_data; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_corrupt = fragmenter_auto_in_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_valid = auto_tl_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_opcode = auto_tl_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_param = auto_tl_in_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_size = auto_tl_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_source = auto_tl_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_address = auto_tl_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_user_amba_prot_bufferable = auto_tl_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_user_amba_prot_modifiable = auto_tl_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_user_amba_prot_readalloc = auto_tl_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_user_amba_prot_writealloc = auto_tl_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_user_amba_prot_privileged = auto_tl_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_user_amba_prot_secure = auto_tl_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_user_amba_prot_fetch = auto_tl_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_mask = auto_tl_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_data = auto_tl_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_corrupt = auto_tl_in_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_d_ready = auto_tl_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_ready = widget_auto_in_a_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_valid = widget_auto_in_d_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_opcode = widget_auto_in_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_size = widget_auto_in_d_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_source = widget_auto_in_d_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_denied = widget_auto_in_d_bits_denied; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_data = widget_auto_in_d_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_corrupt = widget_auto_in_d_bits_corrupt; // @[LazyModule.scala 375:16]
endmodule
