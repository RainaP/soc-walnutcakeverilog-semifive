// SPDX-License-Identifier: Apache-2.0

class semifive_hookup_config #(type T,type KEY);
    static T configs[KEY];
    static function void set(input KEY key, input T value);
        configs[key]= value;
    endfunction
    static function T get(input KEY key);
        return configs[key];
    endfunction
    static function int size();
        return configs.size();
    endfunction
endclass

virtual class interrupt_vslave_driver;
    pure virtual task hookup( input logic enable );
    pure virtual function logic get_irq;
endclass

virtual class vmaster_driver;
    pure virtual task write_req( input logic[63:0] ADDR, input logic[7:0] DATA );
    pure virtual task write_flush;
    pure virtual task read_req( input logic[63:0] ADDR );
    pure virtual task read_flush;
    pure virtual function [7:0] get_read_data;
    pure virtual task hookup( input logic enable );
endclass
