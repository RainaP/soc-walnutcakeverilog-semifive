//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLXbar_9_assert(
  input         clock,
  input         reset,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [8:0]  auto_in_a_bits_address,
  input  [3:0]  auto_in_a_bits_mask,
  input         auto_in_d_ready,
  input         _T_149,
  input         _T_150,
  input         _T_193,
  input         _T_226,
  input         _T_227,
  input         _T_241,
  input         _T_264,
  input  [42:0] _T_283
);
  wire  TLMonitor_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [8:0] TLMonitor_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_d_bits_param; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  _T_230; // @[Arbiter.scala 74:52]
  wire  _T_232; // @[Arbiter.scala 75:62]
  wire  _T_235; // @[Arbiter.scala 75:62]
  wire  _T_236; // @[Arbiter.scala 75:59]
  wire  _T_239; // @[Arbiter.scala 75:13]
  wire  _T_240; // @[Arbiter.scala 75:13]
  wire  _T_242; // @[Arbiter.scala 77:15]
  wire  _T_244; // @[Arbiter.scala 77:36]
  wire  _T_246; // @[Arbiter.scala 77:14]
  wire  _T_247; // @[Arbiter.scala 77:14]
  SiFive_TLMonitor_61_assert TLMonitor ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_clock),
    .reset(TLMonitor_reset),
    .io_in_a_ready(TLMonitor_io_in_a_ready),
    .io_in_a_valid(TLMonitor_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_io_in_a_bits_opcode),
    .io_in_a_bits_address(TLMonitor_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_io_in_a_bits_mask),
    .io_in_d_ready(TLMonitor_io_in_d_ready),
    .io_in_d_valid(TLMonitor_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_io_in_d_bits_opcode),
    .io_in_d_bits_param(TLMonitor_io_in_d_bits_param),
    .io_in_d_bits_size(TLMonitor_io_in_d_bits_size),
    .io_in_d_bits_sink(TLMonitor_io_in_d_bits_sink),
    .io_in_d_bits_denied(TLMonitor_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_io_in_d_bits_corrupt)
  );
  assign _T_230 = _T_226 | _T_227; // @[Arbiter.scala 74:52]
  assign _T_232 = ~_T_226; // @[Arbiter.scala 75:62]
  assign _T_235 = ~_T_227; // @[Arbiter.scala 75:62]
  assign _T_236 = _T_232 | _T_235; // @[Arbiter.scala 75:59]
  assign _T_239 = _T_236 | reset; // @[Arbiter.scala 75:13]
  assign _T_240 = ~_T_239; // @[Arbiter.scala 75:13]
  assign _T_242 = ~_T_241; // @[Arbiter.scala 77:15]
  assign _T_244 = _T_242 | _T_230; // @[Arbiter.scala 77:36]
  assign _T_246 = _T_244 | reset; // @[Arbiter.scala 77:14]
  assign _T_247 = ~_T_246; // @[Arbiter.scala 77:14]
  assign TLMonitor_clock = clock;
  assign TLMonitor_reset = reset;
  assign TLMonitor_io_in_a_ready = _T_149 | _T_150; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_valid = auto_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_ready = auto_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_valid = _T_193 ? _T_241 : _T_264; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_opcode = _T_283[42:40]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_param = _T_283[39:38]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_size = _T_283[37:36]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_sink = _T_283[34]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_denied = _T_283[33]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_corrupt = _T_283[0]; // @[Nodes.scala 26:19]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_240) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_240) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_247) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_247) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
