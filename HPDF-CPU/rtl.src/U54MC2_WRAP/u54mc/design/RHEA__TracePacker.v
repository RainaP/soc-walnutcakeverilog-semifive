//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TracePacker(
  input         clock,
  input         reset,
  input         io_teActive,
  output        io_btm_ready,
  input         io_btm_valid,
  input  [4:0]  io_btm_bits_length,
  input  [1:0]  io_btm_bits_timestamp,
  input  [5:0]  io_btm_bits_slices_0_data,
  input  [1:0]  io_btm_bits_slices_0_mseo,
  input  [5:0]  io_btm_bits_slices_1_data,
  input  [1:0]  io_btm_bits_slices_1_mseo,
  input  [5:0]  io_btm_bits_slices_2_data,
  input  [1:0]  io_btm_bits_slices_2_mseo,
  input  [5:0]  io_btm_bits_slices_3_data,
  input  [1:0]  io_btm_bits_slices_3_mseo,
  input  [5:0]  io_btm_bits_slices_4_data,
  input  [1:0]  io_btm_bits_slices_4_mseo,
  input  [5:0]  io_btm_bits_slices_5_data,
  input  [1:0]  io_btm_bits_slices_5_mseo,
  input  [5:0]  io_btm_bits_slices_6_data,
  input  [1:0]  io_btm_bits_slices_6_mseo,
  input  [5:0]  io_btm_bits_slices_7_data,
  input  [1:0]  io_btm_bits_slices_7_mseo,
  input  [5:0]  io_btm_bits_slices_8_data,
  input  [1:0]  io_btm_bits_slices_8_mseo,
  input  [5:0]  io_btm_bits_slices_9_data,
  input  [1:0]  io_btm_bits_slices_9_mseo,
  input  [5:0]  io_btm_bits_slices_10_data,
  input  [1:0]  io_btm_bits_slices_10_mseo,
  input  [5:0]  io_btm_bits_slices_11_data,
  input  [1:0]  io_btm_bits_slices_11_mseo,
  input  [5:0]  io_btm_bits_slices_12_data,
  input  [1:0]  io_btm_bits_slices_12_mseo,
  input  [5:0]  io_btm_bits_slices_13_data,
  input  [1:0]  io_btm_bits_slices_13_mseo,
  input  [5:0]  io_btm_bits_slices_14_data,
  input  [1:0]  io_btm_bits_slices_14_mseo,
  input  [5:0]  io_btm_bits_slices_15_data,
  input  [1:0]  io_btm_bits_slices_15_mseo,
  input  [5:0]  io_btm_bits_slices_16_data,
  input  [1:0]  io_btm_bits_slices_16_mseo,
  input  [5:0]  io_btm_bits_slices_17_data,
  input  [1:0]  io_btm_bits_slices_17_mseo,
  input  [5:0]  io_btm_bits_slices_18_data,
  input  [1:0]  io_btm_bits_slices_18_mseo,
  input  [5:0]  io_btm_bits_slices_19_data,
  input  [1:0]  io_btm_bits_slices_19_mseo,
  input  [5:0]  io_btm_bits_slices_20_data,
  input  [1:0]  io_btm_bits_slices_20_mseo,
  input  [5:0]  io_btm_bits_slices_21_data,
  input  [1:0]  io_btm_bits_slices_21_mseo,
  input  [5:0]  io_btm_bits_slices_22_data,
  input  [1:0]  io_btm_bits_slices_22_mseo,
  input  [5:0]  io_btm_bits_slices_23_data,
  input  [1:0]  io_btm_bits_slices_23_mseo,
  input  [5:0]  io_btm_bits_slices_24_data,
  input  [1:0]  io_btm_bits_slices_24_mseo,
  input  [39:0] io_timestamp,
  input         io_stream_ready,
  output        io_stream_valid,
  output [31:0] io_stream_bits_data
);
`ifdef RANDOMIZE_REG_INIT
  reg [63:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [31:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [31:0] _RAND_59;
  reg [31:0] _RAND_60;
  reg [31:0] _RAND_61;
  reg [31:0] _RAND_62;
  reg [31:0] _RAND_63;
  reg [31:0] _RAND_64;
  reg [31:0] _RAND_65;
`endif // RANDOMIZE_REG_INIT
  reg [39:0] rtimestamp; // @[TracePacker.scala 93:29]
  wire [39:0] utimestamp = io_timestamp ^ rtimestamp; // @[TracePacker.scala 94:42]
  wire [39:0] timestamp = io_btm_bits_timestamp[0] ? io_timestamp : utimestamp; // @[TracePacker.scala 97:21]
  wire [41:0] _btm_length_lastOnes_WIRE_1 = {{2'd0}, timestamp};
  wire [1:0] _btm_length_lastOnes_T_15 = |_btm_length_lastOnes_WIRE_1[17:12] ? 2'h2 : {{1'd0}, |
    _btm_length_lastOnes_WIRE_1[11:6]}; // @[TraceBTM.scala 71:97]
  wire [1:0] _btm_length_lastOnes_T_16 = |_btm_length_lastOnes_WIRE_1[23:18] ? 2'h3 : _btm_length_lastOnes_T_15; // @[TraceBTM.scala 71:97]
  wire [2:0] _btm_length_lastOnes_T_17 = |_btm_length_lastOnes_WIRE_1[29:24] ? 3'h4 : {{1'd0}, _btm_length_lastOnes_T_16
    }; // @[TraceBTM.scala 71:97]
  wire [2:0] _btm_length_lastOnes_T_18 = |_btm_length_lastOnes_WIRE_1[35:30] ? 3'h5 : _btm_length_lastOnes_T_17; // @[TraceBTM.scala 71:97]
  wire [2:0] btm_length_lastOnes = |_btm_length_lastOnes_WIRE_1[41:36] ? 3'h6 : _btm_length_lastOnes_T_18; // @[TraceBTM.scala 71:97]
  wire [3:0] _btm_length_T = btm_length_lastOnes + 3'h1; // @[TraceBTM.scala 72:14]
  wire [4:0] _GEN_196 = {{1'd0}, _btm_length_T}; // @[TracePacker.scala 99:39]
  wire [4:0] _btm_length_T_2 = io_btm_bits_length + _GEN_196; // @[TracePacker.scala 99:39]
  wire [4:0] _btm_slices_0_data_T_1 = 5'h0 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_0_data_T_2 = _btm_slices_0_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_0_data_T_3 = timestamp >> _btm_slices_0_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_0 = 5'h0 >= io_btm_bits_length ? _btm_slices_0_data_T_3 : {{34'd0}, io_btm_bits_slices_0_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_1 = 5'h0 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_0_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_1_data_T_1 = 5'h1 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_1_data_T_2 = _btm_slices_1_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_1_data_T_3 = timestamp >> _btm_slices_1_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_2 = 5'h1 >= io_btm_bits_length ? _btm_slices_1_data_T_3 : {{34'd0}, io_btm_bits_slices_1_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_3 = 5'h1 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_1_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_2_data_T_1 = 5'h2 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_2_data_T_2 = _btm_slices_2_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_2_data_T_3 = timestamp >> _btm_slices_2_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_4 = 5'h2 >= io_btm_bits_length ? _btm_slices_2_data_T_3 : {{34'd0}, io_btm_bits_slices_2_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_5 = 5'h2 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_2_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_3_data_T_1 = 5'h3 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_3_data_T_2 = _btm_slices_3_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_3_data_T_3 = timestamp >> _btm_slices_3_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_6 = 5'h3 >= io_btm_bits_length ? _btm_slices_3_data_T_3 : {{34'd0}, io_btm_bits_slices_3_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_7 = 5'h3 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_3_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_4_data_T_1 = 5'h4 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_4_data_T_2 = _btm_slices_4_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_4_data_T_3 = timestamp >> _btm_slices_4_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_8 = 5'h4 >= io_btm_bits_length ? _btm_slices_4_data_T_3 : {{34'd0}, io_btm_bits_slices_4_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_9 = 5'h4 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_4_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_5_data_T_1 = 5'h5 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_5_data_T_2 = _btm_slices_5_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_5_data_T_3 = timestamp >> _btm_slices_5_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_10 = 5'h5 >= io_btm_bits_length ? _btm_slices_5_data_T_3 : {{34'd0}, io_btm_bits_slices_5_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_11 = 5'h5 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_5_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_6_data_T_1 = 5'h6 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_6_data_T_2 = _btm_slices_6_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_6_data_T_3 = timestamp >> _btm_slices_6_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_12 = 5'h6 >= io_btm_bits_length ? _btm_slices_6_data_T_3 : {{34'd0}, io_btm_bits_slices_6_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_13 = 5'h6 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_6_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_7_data_T_1 = 5'h7 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_7_data_T_2 = _btm_slices_7_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_7_data_T_3 = timestamp >> _btm_slices_7_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_14 = 5'h7 >= io_btm_bits_length ? _btm_slices_7_data_T_3 : {{34'd0}, io_btm_bits_slices_7_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_15 = 5'h7 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_7_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_8_data_T_1 = 5'h8 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_8_data_T_2 = _btm_slices_8_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_8_data_T_3 = timestamp >> _btm_slices_8_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_16 = 5'h8 >= io_btm_bits_length ? _btm_slices_8_data_T_3 : {{34'd0}, io_btm_bits_slices_8_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_17 = 5'h8 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_8_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_9_data_T_1 = 5'h9 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_9_data_T_2 = _btm_slices_9_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_9_data_T_3 = timestamp >> _btm_slices_9_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_18 = 5'h9 >= io_btm_bits_length ? _btm_slices_9_data_T_3 : {{34'd0}, io_btm_bits_slices_9_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_19 = 5'h9 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_9_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_10_data_T_1 = 5'ha - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_10_data_T_2 = _btm_slices_10_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_10_data_T_3 = timestamp >> _btm_slices_10_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_20 = 5'ha >= io_btm_bits_length ? _btm_slices_10_data_T_3 : {{34'd0}, io_btm_bits_slices_10_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_21 = 5'ha >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_10_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_11_data_T_1 = 5'hb - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_11_data_T_2 = _btm_slices_11_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_11_data_T_3 = timestamp >> _btm_slices_11_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_22 = 5'hb >= io_btm_bits_length ? _btm_slices_11_data_T_3 : {{34'd0}, io_btm_bits_slices_11_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_23 = 5'hb >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_11_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_12_data_T_1 = 5'hc - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_12_data_T_2 = _btm_slices_12_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_12_data_T_3 = timestamp >> _btm_slices_12_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_24 = 5'hc >= io_btm_bits_length ? _btm_slices_12_data_T_3 : {{34'd0}, io_btm_bits_slices_12_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_25 = 5'hc >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_12_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_13_data_T_1 = 5'hd - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_13_data_T_2 = _btm_slices_13_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_13_data_T_3 = timestamp >> _btm_slices_13_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_26 = 5'hd >= io_btm_bits_length ? _btm_slices_13_data_T_3 : {{34'd0}, io_btm_bits_slices_13_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_27 = 5'hd >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_13_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_14_data_T_1 = 5'he - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_14_data_T_2 = _btm_slices_14_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_14_data_T_3 = timestamp >> _btm_slices_14_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_28 = 5'he >= io_btm_bits_length ? _btm_slices_14_data_T_3 : {{34'd0}, io_btm_bits_slices_14_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_29 = 5'he >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_14_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_15_data_T_1 = 5'hf - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_15_data_T_2 = _btm_slices_15_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_15_data_T_3 = timestamp >> _btm_slices_15_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_30 = 5'hf >= io_btm_bits_length ? _btm_slices_15_data_T_3 : {{34'd0}, io_btm_bits_slices_15_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_31 = 5'hf >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_15_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_16_data_T_1 = 5'h10 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_16_data_T_2 = _btm_slices_16_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_16_data_T_3 = timestamp >> _btm_slices_16_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_32 = 5'h10 >= io_btm_bits_length ? _btm_slices_16_data_T_3 : {{34'd0}, io_btm_bits_slices_16_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_33 = 5'h10 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_16_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_17_data_T_1 = 5'h11 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_17_data_T_2 = _btm_slices_17_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_17_data_T_3 = timestamp >> _btm_slices_17_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_34 = 5'h11 >= io_btm_bits_length ? _btm_slices_17_data_T_3 : {{34'd0}, io_btm_bits_slices_17_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_35 = 5'h11 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_17_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_18_data_T_1 = 5'h12 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_18_data_T_2 = _btm_slices_18_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_18_data_T_3 = timestamp >> _btm_slices_18_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_36 = 5'h12 >= io_btm_bits_length ? _btm_slices_18_data_T_3 : {{34'd0}, io_btm_bits_slices_18_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_37 = 5'h12 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_18_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_19_data_T_1 = 5'h13 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_19_data_T_2 = _btm_slices_19_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_19_data_T_3 = timestamp >> _btm_slices_19_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_38 = 5'h13 >= io_btm_bits_length ? _btm_slices_19_data_T_3 : {{34'd0}, io_btm_bits_slices_19_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_39 = 5'h13 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_19_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_20_data_T_1 = 5'h14 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_20_data_T_2 = _btm_slices_20_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_20_data_T_3 = timestamp >> _btm_slices_20_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_40 = 5'h14 >= io_btm_bits_length ? _btm_slices_20_data_T_3 : {{34'd0}, io_btm_bits_slices_20_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_41 = 5'h14 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_20_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_21_data_T_1 = 5'h15 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_21_data_T_2 = _btm_slices_21_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_21_data_T_3 = timestamp >> _btm_slices_21_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_42 = 5'h15 >= io_btm_bits_length ? _btm_slices_21_data_T_3 : {{34'd0}, io_btm_bits_slices_21_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_43 = 5'h15 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_21_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_22_data_T_1 = 5'h16 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_22_data_T_2 = _btm_slices_22_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_22_data_T_3 = timestamp >> _btm_slices_22_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_44 = 5'h16 >= io_btm_bits_length ? _btm_slices_22_data_T_3 : {{34'd0}, io_btm_bits_slices_22_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_45 = 5'h16 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_22_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_23_data_T_1 = 5'h17 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_23_data_T_2 = _btm_slices_23_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_23_data_T_3 = timestamp >> _btm_slices_23_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_46 = 5'h17 >= io_btm_bits_length ? _btm_slices_23_data_T_3 : {{34'd0}, io_btm_bits_slices_23_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_47 = 5'h17 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_23_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] _btm_slices_24_data_T_1 = 5'h18 - io_btm_bits_length; // @[TracePacker.scala 102:60]
  wire [7:0] _btm_slices_24_data_T_2 = _btm_slices_24_data_T_1 * 3'h6; // @[TracePacker.scala 102:81]
  wire [39:0] _btm_slices_24_data_T_3 = timestamp >> _btm_slices_24_data_T_2; // @[TracePacker.scala 102:47]
  wire [39:0] _GEN_48 = 5'h18 >= io_btm_bits_length ? _btm_slices_24_data_T_3 : {{34'd0}, io_btm_bits_slices_24_data}; // @[TracePacker.scala 101:45 TracePacker.scala 102:34]
  wire [1:0] _GEN_49 = 5'h18 >= io_btm_bits_length ? 2'h0 : io_btm_bits_slices_24_mseo; // @[TracePacker.scala 101:45 TracePacker.scala 103:34]
  wire [4:0] btm_length = io_btm_bits_timestamp[1] ? _btm_length_T_2 : io_btm_bits_length; // @[TracePacker.scala 98:29 TracePacker.scala 99:18]
  wire [4:0] _T_27 = btm_length - 5'h1; // @[TracePacker.scala 106:29]
  wire [1:0] _GEN_50 = 5'h0 == _T_27 ? 2'h3 : _GEN_1; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_51 = 5'h1 == _T_27 ? 2'h3 : _GEN_3; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_52 = 5'h2 == _T_27 ? 2'h3 : _GEN_5; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_53 = 5'h3 == _T_27 ? 2'h3 : _GEN_7; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_54 = 5'h4 == _T_27 ? 2'h3 : _GEN_9; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_55 = 5'h5 == _T_27 ? 2'h3 : _GEN_11; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_56 = 5'h6 == _T_27 ? 2'h3 : _GEN_13; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_57 = 5'h7 == _T_27 ? 2'h3 : _GEN_15; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_58 = 5'h8 == _T_27 ? 2'h3 : _GEN_17; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_59 = 5'h9 == _T_27 ? 2'h3 : _GEN_19; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_60 = 5'ha == _T_27 ? 2'h3 : _GEN_21; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_61 = 5'hb == _T_27 ? 2'h3 : _GEN_23; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_62 = 5'hc == _T_27 ? 2'h3 : _GEN_25; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_63 = 5'hd == _T_27 ? 2'h3 : _GEN_27; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_64 = 5'he == _T_27 ? 2'h3 : _GEN_29; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_65 = 5'hf == _T_27 ? 2'h3 : _GEN_31; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_66 = 5'h10 == _T_27 ? 2'h3 : _GEN_33; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_67 = 5'h11 == _T_27 ? 2'h3 : _GEN_35; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_68 = 5'h12 == _T_27 ? 2'h3 : _GEN_37; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_69 = 5'h13 == _T_27 ? 2'h3 : _GEN_39; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_70 = 5'h14 == _T_27 ? 2'h3 : _GEN_41; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_71 = 5'h15 == _T_27 ? 2'h3 : _GEN_43; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_72 = 5'h16 == _T_27 ? 2'h3 : _GEN_45; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_73 = 5'h17 == _T_27 ? 2'h3 : _GEN_47; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  wire [1:0] _GEN_74 = 5'h18 == _T_27 ? 2'h3 : _GEN_49; // @[TracePacker.scala 106:41 TracePacker.scala 106:41]
  reg [5:0] fifoWP; // @[TracePacker.scala 126:30]
  wire  deq = io_stream_ready & io_stream_valid; // @[Decoupled.scala 40:37]
  wire [2:0] _GEN_129 = deq ? 3'h4 : 3'h0; // @[TracePacker.scala 139:14 TracePacker.scala 140:13 TracePacker.scala 142:13]
  wire [5:0] fifoDec = {{3'd0}, _GEN_129};
  wire [5:0] fifoWPNow = fifoWP - fifoDec; // @[TracePacker.scala 148:24]
  wire [4:0] _x_T_1 = 5'h0 - btm_length; // @[TracePacker.scala 145:8]
  wire [1:0] x = _x_T_1[1:0]; // @[TracePacker.scala 144:15 TracePacker.scala 145:5]
  wire [4:0] _GEN_197 = {{3'd0}, x}; // @[TracePacker.scala 146:25]
  wire [5:0] fifoInc = btm_length + _GEN_197; // @[TracePacker.scala 146:25]
  wire [5:0] fifoWPNext = fifoWPNow + fifoInc; // @[TracePacker.scala 149:27]
  wire  fifoOverflow = fifoWPNext > 6'h20; // @[TracePacker.scala 150:31]
  wire  btmin_ready = ~fifoOverflow; // @[TracePacker.scala 151:18]
  wire  _T_28 = btmin_ready & io_btm_valid; // @[Decoupled.scala 40:37]
  wire [39:0] _GEN_77 = io_btm_bits_timestamp[1] ? _GEN_0 : {{34'd0}, io_btm_bits_slices_0_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_0_mseo = io_btm_bits_timestamp[1] ? _GEN_50 : io_btm_bits_slices_0_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_79 = io_btm_bits_timestamp[1] ? _GEN_2 : {{34'd0}, io_btm_bits_slices_1_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_1_mseo = io_btm_bits_timestamp[1] ? _GEN_51 : io_btm_bits_slices_1_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_81 = io_btm_bits_timestamp[1] ? _GEN_4 : {{34'd0}, io_btm_bits_slices_2_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_2_mseo = io_btm_bits_timestamp[1] ? _GEN_52 : io_btm_bits_slices_2_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_83 = io_btm_bits_timestamp[1] ? _GEN_6 : {{34'd0}, io_btm_bits_slices_3_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_3_mseo = io_btm_bits_timestamp[1] ? _GEN_53 : io_btm_bits_slices_3_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_85 = io_btm_bits_timestamp[1] ? _GEN_8 : {{34'd0}, io_btm_bits_slices_4_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_4_mseo = io_btm_bits_timestamp[1] ? _GEN_54 : io_btm_bits_slices_4_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_87 = io_btm_bits_timestamp[1] ? _GEN_10 : {{34'd0}, io_btm_bits_slices_5_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_5_mseo = io_btm_bits_timestamp[1] ? _GEN_55 : io_btm_bits_slices_5_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_89 = io_btm_bits_timestamp[1] ? _GEN_12 : {{34'd0}, io_btm_bits_slices_6_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_6_mseo = io_btm_bits_timestamp[1] ? _GEN_56 : io_btm_bits_slices_6_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_91 = io_btm_bits_timestamp[1] ? _GEN_14 : {{34'd0}, io_btm_bits_slices_7_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_7_mseo = io_btm_bits_timestamp[1] ? _GEN_57 : io_btm_bits_slices_7_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_93 = io_btm_bits_timestamp[1] ? _GEN_16 : {{34'd0}, io_btm_bits_slices_8_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_8_mseo = io_btm_bits_timestamp[1] ? _GEN_58 : io_btm_bits_slices_8_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_95 = io_btm_bits_timestamp[1] ? _GEN_18 : {{34'd0}, io_btm_bits_slices_9_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_9_mseo = io_btm_bits_timestamp[1] ? _GEN_59 : io_btm_bits_slices_9_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_97 = io_btm_bits_timestamp[1] ? _GEN_20 : {{34'd0}, io_btm_bits_slices_10_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_10_mseo = io_btm_bits_timestamp[1] ? _GEN_60 : io_btm_bits_slices_10_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_99 = io_btm_bits_timestamp[1] ? _GEN_22 : {{34'd0}, io_btm_bits_slices_11_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_11_mseo = io_btm_bits_timestamp[1] ? _GEN_61 : io_btm_bits_slices_11_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_101 = io_btm_bits_timestamp[1] ? _GEN_24 : {{34'd0}, io_btm_bits_slices_12_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_12_mseo = io_btm_bits_timestamp[1] ? _GEN_62 : io_btm_bits_slices_12_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_103 = io_btm_bits_timestamp[1] ? _GEN_26 : {{34'd0}, io_btm_bits_slices_13_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_13_mseo = io_btm_bits_timestamp[1] ? _GEN_63 : io_btm_bits_slices_13_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_105 = io_btm_bits_timestamp[1] ? _GEN_28 : {{34'd0}, io_btm_bits_slices_14_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_14_mseo = io_btm_bits_timestamp[1] ? _GEN_64 : io_btm_bits_slices_14_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_107 = io_btm_bits_timestamp[1] ? _GEN_30 : {{34'd0}, io_btm_bits_slices_15_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_15_mseo = io_btm_bits_timestamp[1] ? _GEN_65 : io_btm_bits_slices_15_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_109 = io_btm_bits_timestamp[1] ? _GEN_32 : {{34'd0}, io_btm_bits_slices_16_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_16_mseo = io_btm_bits_timestamp[1] ? _GEN_66 : io_btm_bits_slices_16_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_111 = io_btm_bits_timestamp[1] ? _GEN_34 : {{34'd0}, io_btm_bits_slices_17_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_17_mseo = io_btm_bits_timestamp[1] ? _GEN_67 : io_btm_bits_slices_17_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_113 = io_btm_bits_timestamp[1] ? _GEN_36 : {{34'd0}, io_btm_bits_slices_18_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_18_mseo = io_btm_bits_timestamp[1] ? _GEN_68 : io_btm_bits_slices_18_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_115 = io_btm_bits_timestamp[1] ? _GEN_38 : {{34'd0}, io_btm_bits_slices_19_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_19_mseo = io_btm_bits_timestamp[1] ? _GEN_69 : io_btm_bits_slices_19_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_117 = io_btm_bits_timestamp[1] ? _GEN_40 : {{34'd0}, io_btm_bits_slices_20_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_20_mseo = io_btm_bits_timestamp[1] ? _GEN_70 : io_btm_bits_slices_20_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_119 = io_btm_bits_timestamp[1] ? _GEN_42 : {{34'd0}, io_btm_bits_slices_21_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_21_mseo = io_btm_bits_timestamp[1] ? _GEN_71 : io_btm_bits_slices_21_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_121 = io_btm_bits_timestamp[1] ? _GEN_44 : {{34'd0}, io_btm_bits_slices_22_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_22_mseo = io_btm_bits_timestamp[1] ? _GEN_72 : io_btm_bits_slices_22_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_123 = io_btm_bits_timestamp[1] ? _GEN_46 : {{34'd0}, io_btm_bits_slices_23_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_23_mseo = io_btm_bits_timestamp[1] ? _GEN_73 : io_btm_bits_slices_23_mseo; // @[TracePacker.scala 98:29]
  wire [39:0] _GEN_125 = io_btm_bits_timestamp[1] ? _GEN_48 : {{34'd0}, io_btm_bits_slices_24_data}; // @[TracePacker.scala 98:29]
  wire [1:0] btm_slices_24_mseo = io_btm_bits_timestamp[1] ? _GEN_74 : io_btm_bits_slices_24_mseo; // @[TracePacker.scala 98:29]
  wire  _T_29 = ~io_teActive; // @[TracePacker.scala 111:11]
  reg [5:0] fifo_0_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_0_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_1_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_1_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_2_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_2_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_3_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_3_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_4_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_4_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_5_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_5_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_6_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_6_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_7_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_7_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_8_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_8_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_9_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_9_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_10_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_10_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_11_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_11_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_12_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_12_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_13_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_13_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_14_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_14_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_15_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_15_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_16_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_16_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_17_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_17_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_18_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_18_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_19_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_19_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_20_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_20_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_21_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_21_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_22_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_22_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_23_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_23_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_24_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_24_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_25_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_25_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_26_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_26_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_27_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_27_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_28_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_28_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_29_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_29_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_30_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_30_mseo; // @[TracePacker.scala 125:30]
  reg [5:0] fifo_31_data; // @[TracePacker.scala 125:30]
  reg [1:0] fifo_31_mseo; // @[TracePacker.scala 125:30]
  wire [5:0] deq4_0_data = deq ? fifo_4_data : fifo_0_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_0_mseo = deq ? fifo_4_mseo : fifo_0_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_1_data = deq ? fifo_5_data : fifo_1_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_1_mseo = deq ? fifo_5_mseo : fifo_1_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_2_data = deq ? fifo_6_data : fifo_2_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_2_mseo = deq ? fifo_6_mseo : fifo_2_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_3_data = deq ? fifo_7_data : fifo_3_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_3_mseo = deq ? fifo_7_mseo : fifo_3_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_4_data = deq ? fifo_8_data : fifo_4_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_4_mseo = deq ? fifo_8_mseo : fifo_4_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_5_data = deq ? fifo_9_data : fifo_5_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_5_mseo = deq ? fifo_9_mseo : fifo_5_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_6_data = deq ? fifo_10_data : fifo_6_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_6_mseo = deq ? fifo_10_mseo : fifo_6_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_7_data = deq ? fifo_11_data : fifo_7_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_7_mseo = deq ? fifo_11_mseo : fifo_7_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_8_data = deq ? fifo_12_data : fifo_8_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_8_mseo = deq ? fifo_12_mseo : fifo_8_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_9_data = deq ? fifo_13_data : fifo_9_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_9_mseo = deq ? fifo_13_mseo : fifo_9_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_10_data = deq ? fifo_14_data : fifo_10_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_10_mseo = deq ? fifo_14_mseo : fifo_10_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_11_data = deq ? fifo_15_data : fifo_11_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_11_mseo = deq ? fifo_15_mseo : fifo_11_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_12_data = deq ? fifo_16_data : fifo_12_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_12_mseo = deq ? fifo_16_mseo : fifo_12_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_13_data = deq ? fifo_17_data : fifo_13_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_13_mseo = deq ? fifo_17_mseo : fifo_13_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_14_data = deq ? fifo_18_data : fifo_14_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_14_mseo = deq ? fifo_18_mseo : fifo_14_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_15_data = deq ? fifo_19_data : fifo_15_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_15_mseo = deq ? fifo_19_mseo : fifo_15_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_16_data = deq ? fifo_20_data : fifo_16_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_16_mseo = deq ? fifo_20_mseo : fifo_16_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_17_data = deq ? fifo_21_data : fifo_17_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_17_mseo = deq ? fifo_21_mseo : fifo_17_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_18_data = deq ? fifo_22_data : fifo_18_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_18_mseo = deq ? fifo_22_mseo : fifo_18_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_19_data = deq ? fifo_23_data : fifo_19_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_19_mseo = deq ? fifo_23_mseo : fifo_19_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_20_data = deq ? fifo_24_data : fifo_20_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_20_mseo = deq ? fifo_24_mseo : fifo_20_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_21_data = deq ? fifo_25_data : fifo_21_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_21_mseo = deq ? fifo_25_mseo : fifo_21_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_22_data = deq ? fifo_26_data : fifo_22_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_22_mseo = deq ? fifo_26_mseo : fifo_22_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_23_data = deq ? fifo_27_data : fifo_23_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_23_mseo = deq ? fifo_27_mseo : fifo_23_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_24_data = deq ? fifo_28_data : fifo_24_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_24_mseo = deq ? fifo_28_mseo : fifo_24_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_25_data = deq ? fifo_29_data : fifo_25_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_25_mseo = deq ? fifo_29_mseo : fifo_25_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_26_data = deq ? fifo_30_data : fifo_26_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_26_mseo = deq ? fifo_30_mseo : fifo_26_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_27_data = deq ? fifo_31_data : fifo_27_data; // @[TracePacker.scala 178:25]
  wire [1:0] deq4_27_mseo = deq ? fifo_31_mseo : fifo_27_mseo; // @[TracePacker.scala 178:25]
  wire [5:0] deq4_28_data = deq ? 6'h0 : fifo_28_data; // @[TracePacker.scala 180:25]
  wire [1:0] deq4_28_mseo = deq ? 2'h0 : fifo_28_mseo; // @[TracePacker.scala 180:25]
  wire [5:0] deq4_29_data = deq ? 6'h0 : fifo_29_data; // @[TracePacker.scala 180:25]
  wire [1:0] deq4_29_mseo = deq ? 2'h0 : fifo_29_mseo; // @[TracePacker.scala 180:25]
  wire [5:0] deq4_30_data = deq ? 6'h0 : fifo_30_data; // @[TracePacker.scala 180:25]
  wire [1:0] deq4_30_mseo = deq ? 2'h0 : fifo_30_mseo; // @[TracePacker.scala 180:25]
  wire [5:0] deq4_31_data = deq ? 6'h0 : fifo_31_data; // @[TracePacker.scala 180:25]
  wire [1:0] deq4_31_mseo = deq ? 2'h0 : fifo_31_mseo; // @[TracePacker.scala 180:25]
  wire [5:0] btm_slices_0_data = _GEN_77[5:0];
  wire [5:0] btmshift_0_0_data = _T_28 & btmin_ready ? btm_slices_0_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_0_mseo = _T_28 & btmin_ready ? btm_slices_0_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_1_data = _GEN_79[5:0];
  wire [5:0] btmshift_0_1_data = _T_28 & btmin_ready ? btm_slices_1_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_1_mseo = _T_28 & btmin_ready ? btm_slices_1_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_2_data = _GEN_81[5:0];
  wire [5:0] btmshift_0_2_data = _T_28 & btmin_ready ? btm_slices_2_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_2_mseo = _T_28 & btmin_ready ? btm_slices_2_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_3_data = _GEN_83[5:0];
  wire [5:0] btmshift_0_3_data = _T_28 & btmin_ready ? btm_slices_3_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_3_mseo = _T_28 & btmin_ready ? btm_slices_3_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_4_data = _GEN_85[5:0];
  wire [5:0] btmshift_0_4_data = _T_28 & btmin_ready ? btm_slices_4_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_4_mseo = _T_28 & btmin_ready ? btm_slices_4_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_5_data = _GEN_87[5:0];
  wire [5:0] btmshift_0_5_data = _T_28 & btmin_ready ? btm_slices_5_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_5_mseo = _T_28 & btmin_ready ? btm_slices_5_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_6_data = _GEN_89[5:0];
  wire [5:0] btmshift_0_6_data = _T_28 & btmin_ready ? btm_slices_6_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_6_mseo = _T_28 & btmin_ready ? btm_slices_6_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_7_data = _GEN_91[5:0];
  wire [5:0] btmshift_0_7_data = _T_28 & btmin_ready ? btm_slices_7_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_7_mseo = _T_28 & btmin_ready ? btm_slices_7_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_8_data = _GEN_93[5:0];
  wire [5:0] btmshift_0_8_data = _T_28 & btmin_ready ? btm_slices_8_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_8_mseo = _T_28 & btmin_ready ? btm_slices_8_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_9_data = _GEN_95[5:0];
  wire [5:0] btmshift_0_9_data = _T_28 & btmin_ready ? btm_slices_9_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_9_mseo = _T_28 & btmin_ready ? btm_slices_9_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_10_data = _GEN_97[5:0];
  wire [5:0] btmshift_0_10_data = _T_28 & btmin_ready ? btm_slices_10_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_10_mseo = _T_28 & btmin_ready ? btm_slices_10_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_11_data = _GEN_99[5:0];
  wire [5:0] btmshift_0_11_data = _T_28 & btmin_ready ? btm_slices_11_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_11_mseo = _T_28 & btmin_ready ? btm_slices_11_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_12_data = _GEN_101[5:0];
  wire [5:0] btmshift_0_12_data = _T_28 & btmin_ready ? btm_slices_12_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_12_mseo = _T_28 & btmin_ready ? btm_slices_12_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_13_data = _GEN_103[5:0];
  wire [5:0] btmshift_0_13_data = _T_28 & btmin_ready ? btm_slices_13_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_13_mseo = _T_28 & btmin_ready ? btm_slices_13_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_14_data = _GEN_105[5:0];
  wire [5:0] btmshift_0_14_data = _T_28 & btmin_ready ? btm_slices_14_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_14_mseo = _T_28 & btmin_ready ? btm_slices_14_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_15_data = _GEN_107[5:0];
  wire [5:0] btmshift_0_15_data = _T_28 & btmin_ready ? btm_slices_15_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_15_mseo = _T_28 & btmin_ready ? btm_slices_15_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_16_data = _GEN_109[5:0];
  wire [5:0] btmshift_0_16_data = _T_28 & btmin_ready ? btm_slices_16_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_16_mseo = _T_28 & btmin_ready ? btm_slices_16_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_17_data = _GEN_111[5:0];
  wire [5:0] btmshift_0_17_data = _T_28 & btmin_ready ? btm_slices_17_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_17_mseo = _T_28 & btmin_ready ? btm_slices_17_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_18_data = _GEN_113[5:0];
  wire [5:0] btmshift_0_18_data = _T_28 & btmin_ready ? btm_slices_18_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_18_mseo = _T_28 & btmin_ready ? btm_slices_18_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_19_data = _GEN_115[5:0];
  wire [5:0] btmshift_0_19_data = _T_28 & btmin_ready ? btm_slices_19_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_19_mseo = _T_28 & btmin_ready ? btm_slices_19_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_20_data = _GEN_117[5:0];
  wire [5:0] btmshift_0_20_data = _T_28 & btmin_ready ? btm_slices_20_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_20_mseo = _T_28 & btmin_ready ? btm_slices_20_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_21_data = _GEN_119[5:0];
  wire [5:0] btmshift_0_21_data = _T_28 & btmin_ready ? btm_slices_21_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_21_mseo = _T_28 & btmin_ready ? btm_slices_21_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_22_data = _GEN_121[5:0];
  wire [5:0] btmshift_0_22_data = _T_28 & btmin_ready ? btm_slices_22_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_22_mseo = _T_28 & btmin_ready ? btm_slices_22_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_23_data = _GEN_123[5:0];
  wire [5:0] btmshift_0_23_data = _T_28 & btmin_ready ? btm_slices_23_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_23_mseo = _T_28 & btmin_ready ? btm_slices_23_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btm_slices_24_data = _GEN_125[5:0];
  wire [5:0] btmshift_0_24_data = _T_28 & btmin_ready ? btm_slices_24_data : 6'h0; // @[TracePacker.scala 187:50]
  wire [1:0] btmshift_0_24_mseo = _T_28 & btmin_ready ? btm_slices_24_mseo : 2'h0; // @[TracePacker.scala 187:50]
  wire [5:0] btmshift_1_0_data = fifoWPNow[2] ? 6'h0 : btmshift_0_0_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_1_0_mseo = fifoWPNow[2] ? 2'h0 : btmshift_0_0_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_1_1_data = fifoWPNow[2] ? 6'h0 : btmshift_0_1_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_1_1_mseo = fifoWPNow[2] ? 2'h0 : btmshift_0_1_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_1_2_data = fifoWPNow[2] ? 6'h0 : btmshift_0_2_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_1_2_mseo = fifoWPNow[2] ? 2'h0 : btmshift_0_2_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_1_3_data = fifoWPNow[2] ? 6'h0 : btmshift_0_3_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_1_3_mseo = fifoWPNow[2] ? 2'h0 : btmshift_0_3_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_1_4_data = fifoWPNow[2] ? btmshift_0_0_data : btmshift_0_4_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_4_mseo = fifoWPNow[2] ? btmshift_0_0_mseo : btmshift_0_4_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_5_data = fifoWPNow[2] ? btmshift_0_1_data : btmshift_0_5_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_5_mseo = fifoWPNow[2] ? btmshift_0_1_mseo : btmshift_0_5_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_6_data = fifoWPNow[2] ? btmshift_0_2_data : btmshift_0_6_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_6_mseo = fifoWPNow[2] ? btmshift_0_2_mseo : btmshift_0_6_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_7_data = fifoWPNow[2] ? btmshift_0_3_data : btmshift_0_7_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_7_mseo = fifoWPNow[2] ? btmshift_0_3_mseo : btmshift_0_7_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_8_data = fifoWPNow[2] ? btmshift_0_4_data : btmshift_0_8_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_8_mseo = fifoWPNow[2] ? btmshift_0_4_mseo : btmshift_0_8_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_9_data = fifoWPNow[2] ? btmshift_0_5_data : btmshift_0_9_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_9_mseo = fifoWPNow[2] ? btmshift_0_5_mseo : btmshift_0_9_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_10_data = fifoWPNow[2] ? btmshift_0_6_data : btmshift_0_10_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_10_mseo = fifoWPNow[2] ? btmshift_0_6_mseo : btmshift_0_10_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_11_data = fifoWPNow[2] ? btmshift_0_7_data : btmshift_0_11_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_11_mseo = fifoWPNow[2] ? btmshift_0_7_mseo : btmshift_0_11_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_12_data = fifoWPNow[2] ? btmshift_0_8_data : btmshift_0_12_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_12_mseo = fifoWPNow[2] ? btmshift_0_8_mseo : btmshift_0_12_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_13_data = fifoWPNow[2] ? btmshift_0_9_data : btmshift_0_13_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_13_mseo = fifoWPNow[2] ? btmshift_0_9_mseo : btmshift_0_13_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_14_data = fifoWPNow[2] ? btmshift_0_10_data : btmshift_0_14_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_14_mseo = fifoWPNow[2] ? btmshift_0_10_mseo : btmshift_0_14_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_15_data = fifoWPNow[2] ? btmshift_0_11_data : btmshift_0_15_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_15_mseo = fifoWPNow[2] ? btmshift_0_11_mseo : btmshift_0_15_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_16_data = fifoWPNow[2] ? btmshift_0_12_data : btmshift_0_16_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_16_mseo = fifoWPNow[2] ? btmshift_0_12_mseo : btmshift_0_16_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_17_data = fifoWPNow[2] ? btmshift_0_13_data : btmshift_0_17_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_17_mseo = fifoWPNow[2] ? btmshift_0_13_mseo : btmshift_0_17_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_18_data = fifoWPNow[2] ? btmshift_0_14_data : btmshift_0_18_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_18_mseo = fifoWPNow[2] ? btmshift_0_14_mseo : btmshift_0_18_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_19_data = fifoWPNow[2] ? btmshift_0_15_data : btmshift_0_19_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_19_mseo = fifoWPNow[2] ? btmshift_0_15_mseo : btmshift_0_19_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_20_data = fifoWPNow[2] ? btmshift_0_16_data : btmshift_0_20_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_20_mseo = fifoWPNow[2] ? btmshift_0_16_mseo : btmshift_0_20_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_21_data = fifoWPNow[2] ? btmshift_0_17_data : btmshift_0_21_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_21_mseo = fifoWPNow[2] ? btmshift_0_17_mseo : btmshift_0_21_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_22_data = fifoWPNow[2] ? btmshift_0_18_data : btmshift_0_22_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_22_mseo = fifoWPNow[2] ? btmshift_0_18_mseo : btmshift_0_22_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_23_data = fifoWPNow[2] ? btmshift_0_19_data : btmshift_0_23_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_23_mseo = fifoWPNow[2] ? btmshift_0_19_mseo : btmshift_0_23_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_24_data = fifoWPNow[2] ? btmshift_0_20_data : btmshift_0_24_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_24_mseo = fifoWPNow[2] ? btmshift_0_20_mseo : btmshift_0_24_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_25_data = fifoWPNow[2] ? btmshift_0_21_data : 6'h0; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_25_mseo = fifoWPNow[2] ? btmshift_0_21_mseo : 2'h0; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_26_data = fifoWPNow[2] ? btmshift_0_22_data : 6'h0; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_26_mseo = fifoWPNow[2] ? btmshift_0_22_mseo : 2'h0; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_27_data = fifoWPNow[2] ? btmshift_0_23_data : 6'h0; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_27_mseo = fifoWPNow[2] ? btmshift_0_23_mseo : 2'h0; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_1_28_data = fifoWPNow[2] ? btmshift_0_24_data : 6'h0; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_1_28_mseo = fifoWPNow[2] ? btmshift_0_24_mseo : 2'h0; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_0_data = fifoWPNow[3] ? 6'h0 : btmshift_1_0_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_2_0_mseo = fifoWPNow[3] ? 2'h0 : btmshift_1_0_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_2_1_data = fifoWPNow[3] ? 6'h0 : btmshift_1_1_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_2_1_mseo = fifoWPNow[3] ? 2'h0 : btmshift_1_1_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_2_2_data = fifoWPNow[3] ? 6'h0 : btmshift_1_2_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_2_2_mseo = fifoWPNow[3] ? 2'h0 : btmshift_1_2_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_2_3_data = fifoWPNow[3] ? 6'h0 : btmshift_1_3_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_2_3_mseo = fifoWPNow[3] ? 2'h0 : btmshift_1_3_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_2_4_data = fifoWPNow[3] ? 6'h0 : btmshift_1_4_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_2_4_mseo = fifoWPNow[3] ? 2'h0 : btmshift_1_4_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_2_5_data = fifoWPNow[3] ? 6'h0 : btmshift_1_5_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_2_5_mseo = fifoWPNow[3] ? 2'h0 : btmshift_1_5_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_2_6_data = fifoWPNow[3] ? 6'h0 : btmshift_1_6_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_2_6_mseo = fifoWPNow[3] ? 2'h0 : btmshift_1_6_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_2_7_data = fifoWPNow[3] ? 6'h0 : btmshift_1_7_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_2_7_mseo = fifoWPNow[3] ? 2'h0 : btmshift_1_7_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_2_8_data = fifoWPNow[3] ? btmshift_1_0_data : btmshift_1_8_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_8_mseo = fifoWPNow[3] ? btmshift_1_0_mseo : btmshift_1_8_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_9_data = fifoWPNow[3] ? btmshift_1_1_data : btmshift_1_9_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_9_mseo = fifoWPNow[3] ? btmshift_1_1_mseo : btmshift_1_9_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_10_data = fifoWPNow[3] ? btmshift_1_2_data : btmshift_1_10_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_10_mseo = fifoWPNow[3] ? btmshift_1_2_mseo : btmshift_1_10_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_11_data = fifoWPNow[3] ? btmshift_1_3_data : btmshift_1_11_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_11_mseo = fifoWPNow[3] ? btmshift_1_3_mseo : btmshift_1_11_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_12_data = fifoWPNow[3] ? btmshift_1_4_data : btmshift_1_12_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_12_mseo = fifoWPNow[3] ? btmshift_1_4_mseo : btmshift_1_12_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_13_data = fifoWPNow[3] ? btmshift_1_5_data : btmshift_1_13_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_13_mseo = fifoWPNow[3] ? btmshift_1_5_mseo : btmshift_1_13_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_14_data = fifoWPNow[3] ? btmshift_1_6_data : btmshift_1_14_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_14_mseo = fifoWPNow[3] ? btmshift_1_6_mseo : btmshift_1_14_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_15_data = fifoWPNow[3] ? btmshift_1_7_data : btmshift_1_15_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_15_mseo = fifoWPNow[3] ? btmshift_1_7_mseo : btmshift_1_15_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_16_data = fifoWPNow[3] ? btmshift_1_8_data : btmshift_1_16_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_16_mseo = fifoWPNow[3] ? btmshift_1_8_mseo : btmshift_1_16_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_17_data = fifoWPNow[3] ? btmshift_1_9_data : btmshift_1_17_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_17_mseo = fifoWPNow[3] ? btmshift_1_9_mseo : btmshift_1_17_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_18_data = fifoWPNow[3] ? btmshift_1_10_data : btmshift_1_18_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_18_mseo = fifoWPNow[3] ? btmshift_1_10_mseo : btmshift_1_18_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_19_data = fifoWPNow[3] ? btmshift_1_11_data : btmshift_1_19_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_19_mseo = fifoWPNow[3] ? btmshift_1_11_mseo : btmshift_1_19_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_20_data = fifoWPNow[3] ? btmshift_1_12_data : btmshift_1_20_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_20_mseo = fifoWPNow[3] ? btmshift_1_12_mseo : btmshift_1_20_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_21_data = fifoWPNow[3] ? btmshift_1_13_data : btmshift_1_21_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_21_mseo = fifoWPNow[3] ? btmshift_1_13_mseo : btmshift_1_21_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_22_data = fifoWPNow[3] ? btmshift_1_14_data : btmshift_1_22_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_22_mseo = fifoWPNow[3] ? btmshift_1_14_mseo : btmshift_1_22_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_23_data = fifoWPNow[3] ? btmshift_1_15_data : btmshift_1_23_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_23_mseo = fifoWPNow[3] ? btmshift_1_15_mseo : btmshift_1_23_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_24_data = fifoWPNow[3] ? btmshift_1_16_data : btmshift_1_24_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_24_mseo = fifoWPNow[3] ? btmshift_1_16_mseo : btmshift_1_24_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_25_data = fifoWPNow[3] ? btmshift_1_17_data : btmshift_1_25_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_25_mseo = fifoWPNow[3] ? btmshift_1_17_mseo : btmshift_1_25_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_26_data = fifoWPNow[3] ? btmshift_1_18_data : btmshift_1_26_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_26_mseo = fifoWPNow[3] ? btmshift_1_18_mseo : btmshift_1_26_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_27_data = fifoWPNow[3] ? btmshift_1_19_data : btmshift_1_27_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_27_mseo = fifoWPNow[3] ? btmshift_1_19_mseo : btmshift_1_27_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_28_data = fifoWPNow[3] ? btmshift_1_20_data : btmshift_1_28_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_28_mseo = fifoWPNow[3] ? btmshift_1_20_mseo : btmshift_1_28_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_29_data = fifoWPNow[3] ? btmshift_1_21_data : 6'h0; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_29_mseo = fifoWPNow[3] ? btmshift_1_21_mseo : 2'h0; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_30_data = fifoWPNow[3] ? btmshift_1_22_data : 6'h0; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_30_mseo = fifoWPNow[3] ? btmshift_1_22_mseo : 2'h0; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_2_31_data = fifoWPNow[3] ? btmshift_1_23_data : 6'h0; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_2_31_mseo = fifoWPNow[3] ? btmshift_1_23_mseo : 2'h0; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_0_data = fifoWPNow[4] ? 6'h0 : btmshift_2_0_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_0_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_0_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_1_data = fifoWPNow[4] ? 6'h0 : btmshift_2_1_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_1_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_1_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_2_data = fifoWPNow[4] ? 6'h0 : btmshift_2_2_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_2_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_2_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_3_data = fifoWPNow[4] ? 6'h0 : btmshift_2_3_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_3_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_3_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_4_data = fifoWPNow[4] ? 6'h0 : btmshift_2_4_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_4_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_4_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_5_data = fifoWPNow[4] ? 6'h0 : btmshift_2_5_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_5_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_5_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_6_data = fifoWPNow[4] ? 6'h0 : btmshift_2_6_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_6_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_6_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_7_data = fifoWPNow[4] ? 6'h0 : btmshift_2_7_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_7_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_7_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_8_data = fifoWPNow[4] ? 6'h0 : btmshift_2_8_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_8_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_8_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_9_data = fifoWPNow[4] ? 6'h0 : btmshift_2_9_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_9_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_9_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_10_data = fifoWPNow[4] ? 6'h0 : btmshift_2_10_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_10_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_10_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_11_data = fifoWPNow[4] ? 6'h0 : btmshift_2_11_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_11_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_11_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_12_data = fifoWPNow[4] ? 6'h0 : btmshift_2_12_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_12_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_12_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_13_data = fifoWPNow[4] ? 6'h0 : btmshift_2_13_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_13_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_13_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_14_data = fifoWPNow[4] ? 6'h0 : btmshift_2_14_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_14_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_14_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_15_data = fifoWPNow[4] ? 6'h0 : btmshift_2_15_data; // @[TracePacker.scala 195:40]
  wire [1:0] btmshift_3_15_mseo = fifoWPNow[4] ? 2'h0 : btmshift_2_15_mseo; // @[TracePacker.scala 195:40]
  wire [5:0] btmshift_3_16_data = fifoWPNow[4] ? btmshift_2_0_data : btmshift_2_16_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_16_mseo = fifoWPNow[4] ? btmshift_2_0_mseo : btmshift_2_16_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_17_data = fifoWPNow[4] ? btmshift_2_1_data : btmshift_2_17_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_17_mseo = fifoWPNow[4] ? btmshift_2_1_mseo : btmshift_2_17_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_18_data = fifoWPNow[4] ? btmshift_2_2_data : btmshift_2_18_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_18_mseo = fifoWPNow[4] ? btmshift_2_2_mseo : btmshift_2_18_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_19_data = fifoWPNow[4] ? btmshift_2_3_data : btmshift_2_19_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_19_mseo = fifoWPNow[4] ? btmshift_2_3_mseo : btmshift_2_19_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_20_data = fifoWPNow[4] ? btmshift_2_4_data : btmshift_2_20_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_20_mseo = fifoWPNow[4] ? btmshift_2_4_mseo : btmshift_2_20_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_21_data = fifoWPNow[4] ? btmshift_2_5_data : btmshift_2_21_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_21_mseo = fifoWPNow[4] ? btmshift_2_5_mseo : btmshift_2_21_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_22_data = fifoWPNow[4] ? btmshift_2_6_data : btmshift_2_22_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_22_mseo = fifoWPNow[4] ? btmshift_2_6_mseo : btmshift_2_22_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_23_data = fifoWPNow[4] ? btmshift_2_7_data : btmshift_2_23_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_23_mseo = fifoWPNow[4] ? btmshift_2_7_mseo : btmshift_2_23_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_24_data = fifoWPNow[4] ? btmshift_2_8_data : btmshift_2_24_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_24_mseo = fifoWPNow[4] ? btmshift_2_8_mseo : btmshift_2_24_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_25_data = fifoWPNow[4] ? btmshift_2_9_data : btmshift_2_25_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_25_mseo = fifoWPNow[4] ? btmshift_2_9_mseo : btmshift_2_25_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_26_data = fifoWPNow[4] ? btmshift_2_10_data : btmshift_2_26_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_26_mseo = fifoWPNow[4] ? btmshift_2_10_mseo : btmshift_2_26_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_27_data = fifoWPNow[4] ? btmshift_2_11_data : btmshift_2_27_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_27_mseo = fifoWPNow[4] ? btmshift_2_11_mseo : btmshift_2_27_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_28_data = fifoWPNow[4] ? btmshift_2_12_data : btmshift_2_28_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_28_mseo = fifoWPNow[4] ? btmshift_2_12_mseo : btmshift_2_28_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_29_data = fifoWPNow[4] ? btmshift_2_13_data : btmshift_2_29_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_29_mseo = fifoWPNow[4] ? btmshift_2_13_mseo : btmshift_2_29_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_30_data = fifoWPNow[4] ? btmshift_2_14_data : btmshift_2_30_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_30_mseo = fifoWPNow[4] ? btmshift_2_14_mseo : btmshift_2_30_mseo; // @[TracePacker.scala 197:40]
  wire [5:0] btmshift_3_31_data = fifoWPNow[4] ? btmshift_2_15_data : btmshift_2_31_data; // @[TracePacker.scala 197:40]
  wire [1:0] btmshift_3_31_mseo = fifoWPNow[4] ? btmshift_2_15_mseo : btmshift_2_31_mseo; // @[TracePacker.scala 197:40]
  wire [1:0] _fifo_0_mseo_T = deq4_0_mseo | btmshift_3_0_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_0_data_T = deq4_0_data | btmshift_3_0_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_1_mseo_T = deq4_1_mseo | btmshift_3_1_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_1_data_T = deq4_1_data | btmshift_3_1_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_2_mseo_T = deq4_2_mseo | btmshift_3_2_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_2_data_T = deq4_2_data | btmshift_3_2_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_3_mseo_T = deq4_3_mseo | btmshift_3_3_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_3_data_T = deq4_3_data | btmshift_3_3_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_4_mseo_T = deq4_4_mseo | btmshift_3_4_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_4_data_T = deq4_4_data | btmshift_3_4_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_5_mseo_T = deq4_5_mseo | btmshift_3_5_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_5_data_T = deq4_5_data | btmshift_3_5_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_6_mseo_T = deq4_6_mseo | btmshift_3_6_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_6_data_T = deq4_6_data | btmshift_3_6_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_7_mseo_T = deq4_7_mseo | btmshift_3_7_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_7_data_T = deq4_7_data | btmshift_3_7_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_8_mseo_T = deq4_8_mseo | btmshift_3_8_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_8_data_T = deq4_8_data | btmshift_3_8_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_9_mseo_T = deq4_9_mseo | btmshift_3_9_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_9_data_T = deq4_9_data | btmshift_3_9_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_10_mseo_T = deq4_10_mseo | btmshift_3_10_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_10_data_T = deq4_10_data | btmshift_3_10_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_11_mseo_T = deq4_11_mseo | btmshift_3_11_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_11_data_T = deq4_11_data | btmshift_3_11_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_12_mseo_T = deq4_12_mseo | btmshift_3_12_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_12_data_T = deq4_12_data | btmshift_3_12_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_13_mseo_T = deq4_13_mseo | btmshift_3_13_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_13_data_T = deq4_13_data | btmshift_3_13_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_14_mseo_T = deq4_14_mseo | btmshift_3_14_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_14_data_T = deq4_14_data | btmshift_3_14_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_15_mseo_T = deq4_15_mseo | btmshift_3_15_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_15_data_T = deq4_15_data | btmshift_3_15_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_16_mseo_T = deq4_16_mseo | btmshift_3_16_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_16_data_T = deq4_16_data | btmshift_3_16_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_17_mseo_T = deq4_17_mseo | btmshift_3_17_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_17_data_T = deq4_17_data | btmshift_3_17_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_18_mseo_T = deq4_18_mseo | btmshift_3_18_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_18_data_T = deq4_18_data | btmshift_3_18_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_19_mseo_T = deq4_19_mseo | btmshift_3_19_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_19_data_T = deq4_19_data | btmshift_3_19_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_20_mseo_T = deq4_20_mseo | btmshift_3_20_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_20_data_T = deq4_20_data | btmshift_3_20_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_21_mseo_T = deq4_21_mseo | btmshift_3_21_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_21_data_T = deq4_21_data | btmshift_3_21_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_22_mseo_T = deq4_22_mseo | btmshift_3_22_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_22_data_T = deq4_22_data | btmshift_3_22_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_23_mseo_T = deq4_23_mseo | btmshift_3_23_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_23_data_T = deq4_23_data | btmshift_3_23_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_24_mseo_T = deq4_24_mseo | btmshift_3_24_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_24_data_T = deq4_24_data | btmshift_3_24_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_25_mseo_T = deq4_25_mseo | btmshift_3_25_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_25_data_T = deq4_25_data | btmshift_3_25_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_26_mseo_T = deq4_26_mseo | btmshift_3_26_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_26_data_T = deq4_26_data | btmshift_3_26_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_27_mseo_T = deq4_27_mseo | btmshift_3_27_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_27_data_T = deq4_27_data | btmshift_3_27_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_28_mseo_T = deq4_28_mseo | btmshift_3_28_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_28_data_T = deq4_28_data | btmshift_3_28_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_29_mseo_T = deq4_29_mseo | btmshift_3_29_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_29_data_T = deq4_29_data | btmshift_3_29_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_30_mseo_T = deq4_30_mseo | btmshift_3_30_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_30_data_T = deq4_30_data | btmshift_3_30_data; // @[TracePacker.scala 205:42]
  wire [1:0] _fifo_31_mseo_T = deq4_31_mseo | btmshift_3_31_mseo; // @[TracePacker.scala 204:42]
  wire [5:0] _fifo_31_data_T = deq4_31_data | btmshift_3_31_data; // @[TracePacker.scala 205:42]
  wire [31:0] io_stream_bits_data_lo_lo_lo = {fifo_3_data,fifo_3_mseo,fifo_2_data,fifo_2_mseo,fifo_1_data,fifo_1_mseo,
    fifo_0_data,fifo_0_mseo}; // @[TracePacker.scala 212:31]
  wire [63:0] io_stream_bits_data_lo_lo = {fifo_7_data,fifo_7_mseo,fifo_6_data,fifo_6_mseo,fifo_5_data,fifo_5_mseo,
    fifo_4_data,fifo_4_mseo,io_stream_bits_data_lo_lo_lo}; // @[TracePacker.scala 212:31]
  wire [31:0] io_stream_bits_data_lo_hi_lo = {fifo_11_data,fifo_11_mseo,fifo_10_data,fifo_10_mseo,fifo_9_data,
    fifo_9_mseo,fifo_8_data,fifo_8_mseo}; // @[TracePacker.scala 212:31]
  wire [127:0] io_stream_bits_data_lo = {fifo_15_data,fifo_15_mseo,fifo_14_data,fifo_14_mseo,fifo_13_data,fifo_13_mseo,
    fifo_12_data,fifo_12_mseo,io_stream_bits_data_lo_hi_lo,io_stream_bits_data_lo_lo}; // @[TracePacker.scala 212:31]
  wire [31:0] io_stream_bits_data_hi_lo_lo = {fifo_19_data,fifo_19_mseo,fifo_18_data,fifo_18_mseo,fifo_17_data,
    fifo_17_mseo,fifo_16_data,fifo_16_mseo}; // @[TracePacker.scala 212:31]
  wire [63:0] io_stream_bits_data_hi_lo = {fifo_23_data,fifo_23_mseo,fifo_22_data,fifo_22_mseo,fifo_21_data,fifo_21_mseo
    ,fifo_20_data,fifo_20_mseo,io_stream_bits_data_hi_lo_lo}; // @[TracePacker.scala 212:31]
  wire [31:0] io_stream_bits_data_hi_hi_lo = {fifo_27_data,fifo_27_mseo,fifo_26_data,fifo_26_mseo,fifo_25_data,
    fifo_25_mseo,fifo_24_data,fifo_24_mseo}; // @[TracePacker.scala 212:31]
  wire [127:0] io_stream_bits_data_hi = {fifo_31_data,fifo_31_mseo,fifo_30_data,fifo_30_mseo,fifo_29_data,fifo_29_mseo,
    fifo_28_data,fifo_28_mseo,io_stream_bits_data_hi_hi_lo,io_stream_bits_data_hi_lo}; // @[TracePacker.scala 212:31]
  wire [255:0] _io_stream_bits_data_T = {io_stream_bits_data_hi,io_stream_bits_data_lo}; // @[TracePacker.scala 212:31]
  assign io_btm_ready = ~fifoOverflow; // @[TracePacker.scala 151:18]
  assign io_stream_valid = |fifoWP; // @[TracePacker.scala 211:29]
  assign io_stream_bits_data = _io_stream_bits_data_T[31:0]; // @[TracePacker.scala 212:23]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      rtimestamp <= 40'h0;
    end else if (~io_teActive) begin
      rtimestamp <= 40'h0;
    end else if (io_btm_bits_timestamp[1]) begin
      if (_T_28) begin
        rtimestamp <= io_timestamp;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifoWP <= 6'h0;
    end else if (_T_29) begin
      fifoWP <= 6'h0;
    end else if (_T_28) begin
      fifoWP <= fifoWPNext;
    end else begin
      fifoWP <= fifoWPNow;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_0_data <= 6'h0;
    end else if (_T_29) begin
      fifo_0_data <= 6'h0;
    end else begin
      fifo_0_data <= _fifo_0_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_0_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_0_mseo <= 2'h0;
    end else begin
      fifo_0_mseo <= _fifo_0_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_1_data <= 6'h0;
    end else if (_T_29) begin
      fifo_1_data <= 6'h0;
    end else begin
      fifo_1_data <= _fifo_1_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_1_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_1_mseo <= 2'h0;
    end else begin
      fifo_1_mseo <= _fifo_1_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_2_data <= 6'h0;
    end else if (_T_29) begin
      fifo_2_data <= 6'h0;
    end else begin
      fifo_2_data <= _fifo_2_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_2_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_2_mseo <= 2'h0;
    end else begin
      fifo_2_mseo <= _fifo_2_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_3_data <= 6'h0;
    end else if (_T_29) begin
      fifo_3_data <= 6'h0;
    end else begin
      fifo_3_data <= _fifo_3_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_3_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_3_mseo <= 2'h0;
    end else begin
      fifo_3_mseo <= _fifo_3_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_4_data <= 6'h0;
    end else if (_T_29) begin
      fifo_4_data <= 6'h0;
    end else begin
      fifo_4_data <= _fifo_4_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_4_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_4_mseo <= 2'h0;
    end else begin
      fifo_4_mseo <= _fifo_4_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_5_data <= 6'h0;
    end else if (_T_29) begin
      fifo_5_data <= 6'h0;
    end else begin
      fifo_5_data <= _fifo_5_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_5_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_5_mseo <= 2'h0;
    end else begin
      fifo_5_mseo <= _fifo_5_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_6_data <= 6'h0;
    end else if (_T_29) begin
      fifo_6_data <= 6'h0;
    end else begin
      fifo_6_data <= _fifo_6_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_6_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_6_mseo <= 2'h0;
    end else begin
      fifo_6_mseo <= _fifo_6_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_7_data <= 6'h0;
    end else if (_T_29) begin
      fifo_7_data <= 6'h0;
    end else begin
      fifo_7_data <= _fifo_7_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_7_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_7_mseo <= 2'h0;
    end else begin
      fifo_7_mseo <= _fifo_7_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_8_data <= 6'h0;
    end else if (_T_29) begin
      fifo_8_data <= 6'h0;
    end else begin
      fifo_8_data <= _fifo_8_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_8_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_8_mseo <= 2'h0;
    end else begin
      fifo_8_mseo <= _fifo_8_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_9_data <= 6'h0;
    end else if (_T_29) begin
      fifo_9_data <= 6'h0;
    end else begin
      fifo_9_data <= _fifo_9_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_9_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_9_mseo <= 2'h0;
    end else begin
      fifo_9_mseo <= _fifo_9_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_10_data <= 6'h0;
    end else if (_T_29) begin
      fifo_10_data <= 6'h0;
    end else begin
      fifo_10_data <= _fifo_10_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_10_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_10_mseo <= 2'h0;
    end else begin
      fifo_10_mseo <= _fifo_10_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_11_data <= 6'h0;
    end else if (_T_29) begin
      fifo_11_data <= 6'h0;
    end else begin
      fifo_11_data <= _fifo_11_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_11_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_11_mseo <= 2'h0;
    end else begin
      fifo_11_mseo <= _fifo_11_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_12_data <= 6'h0;
    end else if (_T_29) begin
      fifo_12_data <= 6'h0;
    end else begin
      fifo_12_data <= _fifo_12_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_12_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_12_mseo <= 2'h0;
    end else begin
      fifo_12_mseo <= _fifo_12_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_13_data <= 6'h0;
    end else if (_T_29) begin
      fifo_13_data <= 6'h0;
    end else begin
      fifo_13_data <= _fifo_13_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_13_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_13_mseo <= 2'h0;
    end else begin
      fifo_13_mseo <= _fifo_13_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_14_data <= 6'h0;
    end else if (_T_29) begin
      fifo_14_data <= 6'h0;
    end else begin
      fifo_14_data <= _fifo_14_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_14_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_14_mseo <= 2'h0;
    end else begin
      fifo_14_mseo <= _fifo_14_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_15_data <= 6'h0;
    end else if (_T_29) begin
      fifo_15_data <= 6'h0;
    end else begin
      fifo_15_data <= _fifo_15_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_15_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_15_mseo <= 2'h0;
    end else begin
      fifo_15_mseo <= _fifo_15_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_16_data <= 6'h0;
    end else if (_T_29) begin
      fifo_16_data <= 6'h0;
    end else begin
      fifo_16_data <= _fifo_16_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_16_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_16_mseo <= 2'h0;
    end else begin
      fifo_16_mseo <= _fifo_16_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_17_data <= 6'h0;
    end else if (_T_29) begin
      fifo_17_data <= 6'h0;
    end else begin
      fifo_17_data <= _fifo_17_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_17_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_17_mseo <= 2'h0;
    end else begin
      fifo_17_mseo <= _fifo_17_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_18_data <= 6'h0;
    end else if (_T_29) begin
      fifo_18_data <= 6'h0;
    end else begin
      fifo_18_data <= _fifo_18_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_18_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_18_mseo <= 2'h0;
    end else begin
      fifo_18_mseo <= _fifo_18_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_19_data <= 6'h0;
    end else if (_T_29) begin
      fifo_19_data <= 6'h0;
    end else begin
      fifo_19_data <= _fifo_19_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_19_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_19_mseo <= 2'h0;
    end else begin
      fifo_19_mseo <= _fifo_19_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_20_data <= 6'h0;
    end else if (_T_29) begin
      fifo_20_data <= 6'h0;
    end else begin
      fifo_20_data <= _fifo_20_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_20_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_20_mseo <= 2'h0;
    end else begin
      fifo_20_mseo <= _fifo_20_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_21_data <= 6'h0;
    end else if (_T_29) begin
      fifo_21_data <= 6'h0;
    end else begin
      fifo_21_data <= _fifo_21_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_21_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_21_mseo <= 2'h0;
    end else begin
      fifo_21_mseo <= _fifo_21_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_22_data <= 6'h0;
    end else if (_T_29) begin
      fifo_22_data <= 6'h0;
    end else begin
      fifo_22_data <= _fifo_22_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_22_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_22_mseo <= 2'h0;
    end else begin
      fifo_22_mseo <= _fifo_22_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_23_data <= 6'h0;
    end else if (_T_29) begin
      fifo_23_data <= 6'h0;
    end else begin
      fifo_23_data <= _fifo_23_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_23_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_23_mseo <= 2'h0;
    end else begin
      fifo_23_mseo <= _fifo_23_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_24_data <= 6'h0;
    end else if (_T_29) begin
      fifo_24_data <= 6'h0;
    end else begin
      fifo_24_data <= _fifo_24_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_24_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_24_mseo <= 2'h0;
    end else begin
      fifo_24_mseo <= _fifo_24_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_25_data <= 6'h0;
    end else if (_T_29) begin
      fifo_25_data <= 6'h0;
    end else begin
      fifo_25_data <= _fifo_25_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_25_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_25_mseo <= 2'h0;
    end else begin
      fifo_25_mseo <= _fifo_25_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_26_data <= 6'h0;
    end else if (_T_29) begin
      fifo_26_data <= 6'h0;
    end else begin
      fifo_26_data <= _fifo_26_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_26_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_26_mseo <= 2'h0;
    end else begin
      fifo_26_mseo <= _fifo_26_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_27_data <= 6'h0;
    end else if (_T_29) begin
      fifo_27_data <= 6'h0;
    end else begin
      fifo_27_data <= _fifo_27_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_27_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_27_mseo <= 2'h0;
    end else begin
      fifo_27_mseo <= _fifo_27_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_28_data <= 6'h0;
    end else if (_T_29) begin
      fifo_28_data <= 6'h0;
    end else begin
      fifo_28_data <= _fifo_28_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_28_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_28_mseo <= 2'h0;
    end else begin
      fifo_28_mseo <= _fifo_28_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_29_data <= 6'h0;
    end else if (_T_29) begin
      fifo_29_data <= 6'h0;
    end else begin
      fifo_29_data <= _fifo_29_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_29_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_29_mseo <= 2'h0;
    end else begin
      fifo_29_mseo <= _fifo_29_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_30_data <= 6'h0;
    end else if (_T_29) begin
      fifo_30_data <= 6'h0;
    end else begin
      fifo_30_data <= _fifo_30_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_30_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_30_mseo <= 2'h0;
    end else begin
      fifo_30_mseo <= _fifo_30_mseo_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_31_data <= 6'h0;
    end else if (_T_29) begin
      fifo_31_data <= 6'h0;
    end else begin
      fifo_31_data <= _fifo_31_data_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      fifo_31_mseo <= 2'h0;
    end else if (_T_29) begin
      fifo_31_mseo <= 2'h0;
    end else begin
      fifo_31_mseo <= _fifo_31_mseo_T;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {2{`RANDOM}};
  rtimestamp = _RAND_0[39:0];
  _RAND_1 = {1{`RANDOM}};
  fifoWP = _RAND_1[5:0];
  _RAND_2 = {1{`RANDOM}};
  fifo_0_data = _RAND_2[5:0];
  _RAND_3 = {1{`RANDOM}};
  fifo_0_mseo = _RAND_3[1:0];
  _RAND_4 = {1{`RANDOM}};
  fifo_1_data = _RAND_4[5:0];
  _RAND_5 = {1{`RANDOM}};
  fifo_1_mseo = _RAND_5[1:0];
  _RAND_6 = {1{`RANDOM}};
  fifo_2_data = _RAND_6[5:0];
  _RAND_7 = {1{`RANDOM}};
  fifo_2_mseo = _RAND_7[1:0];
  _RAND_8 = {1{`RANDOM}};
  fifo_3_data = _RAND_8[5:0];
  _RAND_9 = {1{`RANDOM}};
  fifo_3_mseo = _RAND_9[1:0];
  _RAND_10 = {1{`RANDOM}};
  fifo_4_data = _RAND_10[5:0];
  _RAND_11 = {1{`RANDOM}};
  fifo_4_mseo = _RAND_11[1:0];
  _RAND_12 = {1{`RANDOM}};
  fifo_5_data = _RAND_12[5:0];
  _RAND_13 = {1{`RANDOM}};
  fifo_5_mseo = _RAND_13[1:0];
  _RAND_14 = {1{`RANDOM}};
  fifo_6_data = _RAND_14[5:0];
  _RAND_15 = {1{`RANDOM}};
  fifo_6_mseo = _RAND_15[1:0];
  _RAND_16 = {1{`RANDOM}};
  fifo_7_data = _RAND_16[5:0];
  _RAND_17 = {1{`RANDOM}};
  fifo_7_mseo = _RAND_17[1:0];
  _RAND_18 = {1{`RANDOM}};
  fifo_8_data = _RAND_18[5:0];
  _RAND_19 = {1{`RANDOM}};
  fifo_8_mseo = _RAND_19[1:0];
  _RAND_20 = {1{`RANDOM}};
  fifo_9_data = _RAND_20[5:0];
  _RAND_21 = {1{`RANDOM}};
  fifo_9_mseo = _RAND_21[1:0];
  _RAND_22 = {1{`RANDOM}};
  fifo_10_data = _RAND_22[5:0];
  _RAND_23 = {1{`RANDOM}};
  fifo_10_mseo = _RAND_23[1:0];
  _RAND_24 = {1{`RANDOM}};
  fifo_11_data = _RAND_24[5:0];
  _RAND_25 = {1{`RANDOM}};
  fifo_11_mseo = _RAND_25[1:0];
  _RAND_26 = {1{`RANDOM}};
  fifo_12_data = _RAND_26[5:0];
  _RAND_27 = {1{`RANDOM}};
  fifo_12_mseo = _RAND_27[1:0];
  _RAND_28 = {1{`RANDOM}};
  fifo_13_data = _RAND_28[5:0];
  _RAND_29 = {1{`RANDOM}};
  fifo_13_mseo = _RAND_29[1:0];
  _RAND_30 = {1{`RANDOM}};
  fifo_14_data = _RAND_30[5:0];
  _RAND_31 = {1{`RANDOM}};
  fifo_14_mseo = _RAND_31[1:0];
  _RAND_32 = {1{`RANDOM}};
  fifo_15_data = _RAND_32[5:0];
  _RAND_33 = {1{`RANDOM}};
  fifo_15_mseo = _RAND_33[1:0];
  _RAND_34 = {1{`RANDOM}};
  fifo_16_data = _RAND_34[5:0];
  _RAND_35 = {1{`RANDOM}};
  fifo_16_mseo = _RAND_35[1:0];
  _RAND_36 = {1{`RANDOM}};
  fifo_17_data = _RAND_36[5:0];
  _RAND_37 = {1{`RANDOM}};
  fifo_17_mseo = _RAND_37[1:0];
  _RAND_38 = {1{`RANDOM}};
  fifo_18_data = _RAND_38[5:0];
  _RAND_39 = {1{`RANDOM}};
  fifo_18_mseo = _RAND_39[1:0];
  _RAND_40 = {1{`RANDOM}};
  fifo_19_data = _RAND_40[5:0];
  _RAND_41 = {1{`RANDOM}};
  fifo_19_mseo = _RAND_41[1:0];
  _RAND_42 = {1{`RANDOM}};
  fifo_20_data = _RAND_42[5:0];
  _RAND_43 = {1{`RANDOM}};
  fifo_20_mseo = _RAND_43[1:0];
  _RAND_44 = {1{`RANDOM}};
  fifo_21_data = _RAND_44[5:0];
  _RAND_45 = {1{`RANDOM}};
  fifo_21_mseo = _RAND_45[1:0];
  _RAND_46 = {1{`RANDOM}};
  fifo_22_data = _RAND_46[5:0];
  _RAND_47 = {1{`RANDOM}};
  fifo_22_mseo = _RAND_47[1:0];
  _RAND_48 = {1{`RANDOM}};
  fifo_23_data = _RAND_48[5:0];
  _RAND_49 = {1{`RANDOM}};
  fifo_23_mseo = _RAND_49[1:0];
  _RAND_50 = {1{`RANDOM}};
  fifo_24_data = _RAND_50[5:0];
  _RAND_51 = {1{`RANDOM}};
  fifo_24_mseo = _RAND_51[1:0];
  _RAND_52 = {1{`RANDOM}};
  fifo_25_data = _RAND_52[5:0];
  _RAND_53 = {1{`RANDOM}};
  fifo_25_mseo = _RAND_53[1:0];
  _RAND_54 = {1{`RANDOM}};
  fifo_26_data = _RAND_54[5:0];
  _RAND_55 = {1{`RANDOM}};
  fifo_26_mseo = _RAND_55[1:0];
  _RAND_56 = {1{`RANDOM}};
  fifo_27_data = _RAND_56[5:0];
  _RAND_57 = {1{`RANDOM}};
  fifo_27_mseo = _RAND_57[1:0];
  _RAND_58 = {1{`RANDOM}};
  fifo_28_data = _RAND_58[5:0];
  _RAND_59 = {1{`RANDOM}};
  fifo_28_mseo = _RAND_59[1:0];
  _RAND_60 = {1{`RANDOM}};
  fifo_29_data = _RAND_60[5:0];
  _RAND_61 = {1{`RANDOM}};
  fifo_29_mseo = _RAND_61[1:0];
  _RAND_62 = {1{`RANDOM}};
  fifo_30_data = _RAND_62[5:0];
  _RAND_63 = {1{`RANDOM}};
  fifo_30_mseo = _RAND_63[1:0];
  _RAND_64 = {1{`RANDOM}};
  fifo_31_data = _RAND_64[5:0];
  _RAND_65 = {1{`RANDOM}};
  fifo_31_mseo = _RAND_65[1:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    rtimestamp = 40'h0;
  end
  if (reset) begin
    fifoWP = 6'h0;
  end
  if (reset) begin
    fifo_0_data = 6'h0;
  end
  if (reset) begin
    fifo_0_mseo = 2'h0;
  end
  if (reset) begin
    fifo_1_data = 6'h0;
  end
  if (reset) begin
    fifo_1_mseo = 2'h0;
  end
  if (reset) begin
    fifo_2_data = 6'h0;
  end
  if (reset) begin
    fifo_2_mseo = 2'h0;
  end
  if (reset) begin
    fifo_3_data = 6'h0;
  end
  if (reset) begin
    fifo_3_mseo = 2'h0;
  end
  if (reset) begin
    fifo_4_data = 6'h0;
  end
  if (reset) begin
    fifo_4_mseo = 2'h0;
  end
  if (reset) begin
    fifo_5_data = 6'h0;
  end
  if (reset) begin
    fifo_5_mseo = 2'h0;
  end
  if (reset) begin
    fifo_6_data = 6'h0;
  end
  if (reset) begin
    fifo_6_mseo = 2'h0;
  end
  if (reset) begin
    fifo_7_data = 6'h0;
  end
  if (reset) begin
    fifo_7_mseo = 2'h0;
  end
  if (reset) begin
    fifo_8_data = 6'h0;
  end
  if (reset) begin
    fifo_8_mseo = 2'h0;
  end
  if (reset) begin
    fifo_9_data = 6'h0;
  end
  if (reset) begin
    fifo_9_mseo = 2'h0;
  end
  if (reset) begin
    fifo_10_data = 6'h0;
  end
  if (reset) begin
    fifo_10_mseo = 2'h0;
  end
  if (reset) begin
    fifo_11_data = 6'h0;
  end
  if (reset) begin
    fifo_11_mseo = 2'h0;
  end
  if (reset) begin
    fifo_12_data = 6'h0;
  end
  if (reset) begin
    fifo_12_mseo = 2'h0;
  end
  if (reset) begin
    fifo_13_data = 6'h0;
  end
  if (reset) begin
    fifo_13_mseo = 2'h0;
  end
  if (reset) begin
    fifo_14_data = 6'h0;
  end
  if (reset) begin
    fifo_14_mseo = 2'h0;
  end
  if (reset) begin
    fifo_15_data = 6'h0;
  end
  if (reset) begin
    fifo_15_mseo = 2'h0;
  end
  if (reset) begin
    fifo_16_data = 6'h0;
  end
  if (reset) begin
    fifo_16_mseo = 2'h0;
  end
  if (reset) begin
    fifo_17_data = 6'h0;
  end
  if (reset) begin
    fifo_17_mseo = 2'h0;
  end
  if (reset) begin
    fifo_18_data = 6'h0;
  end
  if (reset) begin
    fifo_18_mseo = 2'h0;
  end
  if (reset) begin
    fifo_19_data = 6'h0;
  end
  if (reset) begin
    fifo_19_mseo = 2'h0;
  end
  if (reset) begin
    fifo_20_data = 6'h0;
  end
  if (reset) begin
    fifo_20_mseo = 2'h0;
  end
  if (reset) begin
    fifo_21_data = 6'h0;
  end
  if (reset) begin
    fifo_21_mseo = 2'h0;
  end
  if (reset) begin
    fifo_22_data = 6'h0;
  end
  if (reset) begin
    fifo_22_mseo = 2'h0;
  end
  if (reset) begin
    fifo_23_data = 6'h0;
  end
  if (reset) begin
    fifo_23_mseo = 2'h0;
  end
  if (reset) begin
    fifo_24_data = 6'h0;
  end
  if (reset) begin
    fifo_24_mseo = 2'h0;
  end
  if (reset) begin
    fifo_25_data = 6'h0;
  end
  if (reset) begin
    fifo_25_mseo = 2'h0;
  end
  if (reset) begin
    fifo_26_data = 6'h0;
  end
  if (reset) begin
    fifo_26_mseo = 2'h0;
  end
  if (reset) begin
    fifo_27_data = 6'h0;
  end
  if (reset) begin
    fifo_27_mseo = 2'h0;
  end
  if (reset) begin
    fifo_28_data = 6'h0;
  end
  if (reset) begin
    fifo_28_mseo = 2'h0;
  end
  if (reset) begin
    fifo_29_data = 6'h0;
  end
  if (reset) begin
    fifo_29_mseo = 2'h0;
  end
  if (reset) begin
    fifo_30_data = 6'h0;
  end
  if (reset) begin
    fifo_30_mseo = 2'h0;
  end
  if (reset) begin
    fifo_31_data = 6'h0;
  end
  if (reset) begin
    fifo_31_mseo = 2'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
