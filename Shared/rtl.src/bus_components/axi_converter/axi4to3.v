// Protocol Converter AXI4 to AXI3
// Limits :
//  1. AXI4 Master limit : WVALID does not set before AWVALID. WVALID may set with
//  AWVALID at the same clock cycle
//  Temporary for RHEA FPGA

module axi4to3
#(parameter ADDR_WIDTH = 38,
  parameter ID_WIDTH = 10,
  parameter DATA_WIDTH = 64,
  parameter USER_WIDTH = 64,
  parameter QUEUE_DEPTH = 64,
  parameter LEN_WIDTH = 8,
  parameter STRB_WIDTH = DATA_WIDTH/8)
(
    input                   CLK__BUS,
    input                   RSTN__BUS,
    input [ADDR_WIDTH-1:0]  AXI4__BUS__AWADDR,
    input [ID_WIDTH-1:0]    AXI4__BUS__AWID,
    input [LEN_WIDTH-1:0]   AXI4__BUS__AWLEN,
    input [2:0]             AXI4__BUS__AWSIZE,
    input [1:0]             AXI4__BUS__AWBURST,
    input                   AXI4__BUS__AWLOCK,
    input [3:0]             AXI4__BUS__AWCACHE,
    input [2:0]             AXI4__BUS__AWPROT,
    input [USER_WIDTH-1:0]  AXI4__BUS__AWUSER,
    input                   AXI4__BUS__AWVALID,
    output                  AXI4__BUS__AWREADY,
    input [DATA_WIDTH-1:0]  AXI4__BUS__WDATA,
    input [STRB_WIDTH-1:0]  AXI4__BUS__WSTRB,
    input                   AXI4__BUS__WVALID,
    input                   AXI4__BUS__WLAST,
    output                  AXI4__BUS__WREADY,
    output [ID_WIDTH-1:0]   AXI4__BUS__BID,
    output                  AXI4__BUS__BVALID,
    output [1:0]            AXI4__BUS__BRESP,
    input                   AXI4__BUS__BREADY,
    input [ID_WIDTH-1:0]    AXI4__BUS__ARID,
    input [ADDR_WIDTH-1:0]  AXI4__BUS__ARADDR,
    input [LEN_WIDTH-1:0]   AXI4__BUS__ARLEN,
    input [2:0]             AXI4__BUS__ARSIZE,
    input [1:0]             AXI4__BUS__ARBURST,
    input                   AXI4__BUS__ARLOCK,
    input [3:0]             AXI4__BUS__ARCACHE,
    input [2:0]             AXI4__BUS__ARPROT,
    input [USER_WIDTH-1:0]  AXI4__BUS__ARUSER,
    input                   AXI4__BUS__ARVALID,
    output                  AXI4__BUS__ARREADY,
    output [ID_WIDTH-1:0]   AXI4__BUS__RID,
    output [DATA_WIDTH-1:0] AXI4__BUS__RDATA,
    output [1:0]            AXI4__BUS__RRESP,
    output                  AXI4__BUS__RLAST,
    output                  AXI4__BUS__RVALID,
    input                   AXI4__BUS__RREADY,

    output [ADDR_WIDTH-1:0] AXI3__BUS__AWADDR,
    output [ID_WIDTH-1:0]   AXI3__BUS__AWID,
    output [LEN_WIDTH-1:0]  AXI3__BUS__AWLEN,
    output [2:0]            AXI3__BUS__AWSIZE,
    output [1:0]            AXI3__BUS__AWBURST,
    output [1:0]            AXI3__BUS__AWLOCK,
    output [3:0]            AXI3__BUS__AWCACHE,
    output [2:0]            AXI3__BUS__AWPROT,
    output [USER_WIDTH-1:0] AXI3__BUS__AWUSER,
    output                  AXI3__BUS__AWVALID,
    input                   AXI3__BUS__AWREADY,
    output [ID_WIDTH-1:0]   AXI3__BUS__WID,
    output [DATA_WIDTH-1:0] AXI3__BUS__WDATA,
    output [STRB_WIDTH-1:0] AXI3__BUS__WSTRB,
    output                  AXI3__BUS__WLAST,
    output                  AXI3__BUS__WVALID,
    input                   AXI3__BUS__WREADY,
    input [ID_WIDTH-1:0]    AXI3__BUS__BID,
    input [1:0]             AXI3__BUS__BRESP,
    input                   AXI3__BUS__BVALID,
    output                  AXI3__BUS__BREADY,
    output [ID_WIDTH-1:0]   AXI3__BUS__ARID,
    output [ADDR_WIDTH-1:0] AXI3__BUS__ARADDR,
    output [LEN_WIDTH-1:0]  AXI3__BUS__ARLEN,
    output [2:0]            AXI3__BUS__ARSIZE,
    output [1:0]            AXI3__BUS__ARBURST,
    output [1:0]            AXI3__BUS__ARLOCK,
    output [3:0]            AXI3__BUS__ARCACHE,
    output [2:0]            AXI3__BUS__ARPROT,
    output [USER_WIDTH-1:0] AXI3__BUS__ARUSER,
    output                  AXI3__BUS__ARVALID,
    input                   AXI3__BUS__ARREADY,
    input [ID_WIDTH-1:0]    AXI3__BUS__RID,
    input [DATA_WIDTH-1:0]  AXI3__BUS__RDATA,
    input [1:0]             AXI3__BUS__RRESP,
    input                   AXI3__BUS__RLAST,
    input                   AXI3__BUS__RVALID,
    output                  AXI3__BUS__RREADY
);

localparam QUEUE_DEPTH_ = QUEUE_DEPTH > 4 ? QUEUE_DEPTH : 4;
localparam PTR_WIDTH = $clog2(QUEUE_DEPTH_);

reg [PTR_WIDTH-1:0] wptr;
reg [PTR_WIDTH-1:0] rptr;
wire empty;
wire full;
wire wr_en;
wire rd_en;

assign wr_en = !full & AXI4__BUS__AWVALID & AXI4__BUS__AWREADY;
assign rd_en = !empty & AXI4__BUS__WVALID & AXI4__BUS__WLAST & AXI4__BUS__WREADY;

always @(posedge CLK__BUS or negedge RSTN__BUS) begin
    if (~RSTN__BUS) begin
        wptr <= {PTR_WIDTH{1'b0}};
    end
    else if (wr_en) begin
        wptr <= wptr+1;
    end
end
always @(posedge CLK__BUS or negedge RSTN__BUS) begin
    if (~RSTN__BUS) begin
        rptr <= {PTR_WIDTH{1'b0}};
    end
    else if (rd_en) begin
        rptr <= rptr+1;
    end
end

assign full = (wptr[PTR_WIDTH-2:0] == rptr[PTR_WIDTH-2:0]) & (wptr[PTR_WIDTH-1:0] != rptr[PTR_WIDTH-1:0]);
assign empty = (wptr[PTR_WIDTH-2:0] == rptr[PTR_WIDTH-2:0]) & (wptr[PTR_WIDTH-1:0] == rptr[PTR_WIDTH-1:0]);

reg [ID_WIDTH-1:0] awid_queue[0:QUEUE_DEPTH_-1];
genvar n;
generate
    for (n=0;n<QUEUE_DEPTH_;n=n+1) begin
        always @(posedge CLK__BUS or negedge RSTN__BUS) begin
            if (~RSTN__BUS) begin
                awid_queue[n] <= {ID_WIDTH{1'b0}};
            end
            else if (wr_en) begin
                awid_queue[n] <= AXI4__BUS__AWID; 
            end
        end
    end
endgenerate

assign AXI4__BUS__AWREADY = !full & AXI3__BUS__AWREADY;
assign AXI4__BUS__WREADY = !empty & AXI3__BUS__WREADY; // prevent WVALID before AWVALID
assign AXI3__BUS__WVALID = !empty & AXI4__BUS__WVALID; // prevent WVALID before AWVALID
assign AXI3__BUS__WID = awid_queue[rptr];

assign AXI4__BUS__BID           = AXI3__BUS__BID;
assign AXI4__BUS__BVALID        = AXI3__BUS__BVALID;
assign AXI4__BUS__BRESP         = AXI3__BUS__BRESP;
assign AXI4__BUS__ARREADY       = AXI3__BUS__ARREADY;
assign AXI4__BUS__RID           = AXI3__BUS__RID;
assign AXI4__BUS__RDATA         = AXI3__BUS__RDATA;
assign AXI4__BUS__RRESP         = AXI3__BUS__RRESP;
assign AXI4__BUS__RLAST         = AXI3__BUS__RLAST;
assign AXI4__BUS__RVALID        = AXI3__BUS__RVALID;
assign AXI3__BUS__AWADDR        = AXI4__BUS__AWADDR;
assign AXI3__BUS__AWID          = AXI4__BUS__AWID;
assign AXI3__BUS__AWLEN         = AXI4__BUS__AWLEN;
assign AXI3__BUS__AWSIZE        = AXI4__BUS__AWSIZE;
assign AXI3__BUS__AWBURST       = AXI4__BUS__AWBURST;
assign AXI3__BUS__AWLOCK        = {1'b0,AXI4__BUS__AWLOCK};
assign AXI3__BUS__AWCACHE       = AXI4__BUS__AWCACHE;
assign AXI3__BUS__AWPROT        = AXI4__BUS__AWPROT;
assign AXI3__BUS__AWUSER        = AXI4__BUS__AWUSER;
assign AXI3__BUS__AWVALID       = AXI4__BUS__AWVALID;
assign AXI3__BUS__WDATA         = AXI4__BUS__WDATA;
assign AXI3__BUS__WSTRB         = AXI4__BUS__WSTRB;
assign AXI3__BUS__WLAST         = AXI4__BUS__WLAST;
assign AXI3__BUS__BREADY        = AXI4__BUS__BREADY;
assign AXI3__BUS__ARID          = AXI4__BUS__ARID;
assign AXI3__BUS__ARADDR        = AXI4__BUS__ARADDR;
assign AXI3__BUS__ARLEN         = AXI4__BUS__ARLEN;
assign AXI3__BUS__ARSIZE        = AXI4__BUS__ARSIZE;
assign AXI3__BUS__ARBURST       = AXI4__BUS__ARBURST;
assign AXI3__BUS__ARLOCK        = {1'b0,AXI4__BUS__ARLOCK};
assign AXI3__BUS__ARCACHE       = AXI4__BUS__ARCACHE;
assign AXI3__BUS__ARPROT        = AXI4__BUS__ARPROT;
assign AXI3__BUS__ARUSER        = AXI4__BUS__ARUSER;
assign AXI3__BUS__ARVALID       = AXI4__BUS__ARVALID;
assign AXI3__BUS__RREADY        = AXI4__BUS__RREADY;

endmodule
// vim:tabstop=4:shiftwidth=4
