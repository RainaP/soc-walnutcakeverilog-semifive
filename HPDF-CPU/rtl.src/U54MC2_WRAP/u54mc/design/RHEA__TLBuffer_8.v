//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLBuffer_8(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_a_ready,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [2:0]  auto_in_a_bits_param,
  input  [1:0]  auto_in_a_bits_size,
  input  [10:0] auto_in_a_bits_source,
  input  [25:0] auto_in_a_bits_address,
  input  [7:0]  auto_in_a_bits_mask,
  input  [63:0] auto_in_a_bits_data,
  input         auto_in_a_bits_corrupt,
  input         auto_in_d_ready,
  output        auto_in_d_valid,
  output [2:0]  auto_in_d_bits_opcode,
  output [1:0]  auto_in_d_bits_size,
  output [10:0] auto_in_d_bits_source,
  output [63:0] auto_in_d_bits_data,
  input         auto_out_a_ready,
  output        auto_out_a_valid,
  output [2:0]  auto_out_a_bits_opcode,
  output [2:0]  auto_out_a_bits_param,
  output [1:0]  auto_out_a_bits_size,
  output [10:0] auto_out_a_bits_source,
  output [25:0] auto_out_a_bits_address,
  output [7:0]  auto_out_a_bits_mask,
  output [63:0] auto_out_a_bits_data,
  output        auto_out_a_bits_corrupt,
  output        auto_out_d_ready,
  input         auto_out_d_valid,
  input  [2:0]  auto_out_d_bits_opcode,
  input  [1:0]  auto_out_d_bits_size,
  input  [10:0] auto_out_d_bits_source,
  input  [63:0] auto_out_d_bits_data
);
  wire  bundleOut_0_a_q_rf_reset; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_clock; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_reset; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_0_a_q_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_0_a_q_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [1:0] bundleOut_0_a_q_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [10:0] bundleOut_0_a_q_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [25:0] bundleOut_0_a_q_io_enq_bits_address; // @[Decoupled.scala 296:21]
  wire [7:0] bundleOut_0_a_q_io_enq_bits_mask; // @[Decoupled.scala 296:21]
  wire [63:0] bundleOut_0_a_q_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_0_a_q_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] bundleOut_0_a_q_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [1:0] bundleOut_0_a_q_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [10:0] bundleOut_0_a_q_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [25:0] bundleOut_0_a_q_io_deq_bits_address; // @[Decoupled.scala 296:21]
  wire [7:0] bundleOut_0_a_q_io_deq_bits_mask; // @[Decoupled.scala 296:21]
  wire [63:0] bundleOut_0_a_q_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleOut_0_a_q_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_rf_reset; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_clock; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_reset; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_0_d_q_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] bundleIn_0_d_q_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [10:0] bundleIn_0_d_q_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [63:0] bundleIn_0_d_q_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  bundleIn_0_d_q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] bundleIn_0_d_q_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] bundleIn_0_d_q_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [10:0] bundleIn_0_d_q_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [63:0] bundleIn_0_d_q_io_deq_bits_data; // @[Decoupled.scala 296:21]
  RHEA__Queue_41 bundleOut_0_a_q ( // @[Decoupled.scala 296:21]
    .rf_reset(bundleOut_0_a_q_rf_reset),
    .clock(bundleOut_0_a_q_clock),
    .reset(bundleOut_0_a_q_reset),
    .io_enq_ready(bundleOut_0_a_q_io_enq_ready),
    .io_enq_valid(bundleOut_0_a_q_io_enq_valid),
    .io_enq_bits_opcode(bundleOut_0_a_q_io_enq_bits_opcode),
    .io_enq_bits_param(bundleOut_0_a_q_io_enq_bits_param),
    .io_enq_bits_size(bundleOut_0_a_q_io_enq_bits_size),
    .io_enq_bits_source(bundleOut_0_a_q_io_enq_bits_source),
    .io_enq_bits_address(bundleOut_0_a_q_io_enq_bits_address),
    .io_enq_bits_mask(bundleOut_0_a_q_io_enq_bits_mask),
    .io_enq_bits_data(bundleOut_0_a_q_io_enq_bits_data),
    .io_enq_bits_corrupt(bundleOut_0_a_q_io_enq_bits_corrupt),
    .io_deq_ready(bundleOut_0_a_q_io_deq_ready),
    .io_deq_valid(bundleOut_0_a_q_io_deq_valid),
    .io_deq_bits_opcode(bundleOut_0_a_q_io_deq_bits_opcode),
    .io_deq_bits_param(bundleOut_0_a_q_io_deq_bits_param),
    .io_deq_bits_size(bundleOut_0_a_q_io_deq_bits_size),
    .io_deq_bits_source(bundleOut_0_a_q_io_deq_bits_source),
    .io_deq_bits_address(bundleOut_0_a_q_io_deq_bits_address),
    .io_deq_bits_mask(bundleOut_0_a_q_io_deq_bits_mask),
    .io_deq_bits_data(bundleOut_0_a_q_io_deq_bits_data),
    .io_deq_bits_corrupt(bundleOut_0_a_q_io_deq_bits_corrupt)
  );
  RHEA__Queue_42 bundleIn_0_d_q ( // @[Decoupled.scala 296:21]
    .rf_reset(bundleIn_0_d_q_rf_reset),
    .clock(bundleIn_0_d_q_clock),
    .reset(bundleIn_0_d_q_reset),
    .io_enq_ready(bundleIn_0_d_q_io_enq_ready),
    .io_enq_valid(bundleIn_0_d_q_io_enq_valid),
    .io_enq_bits_opcode(bundleIn_0_d_q_io_enq_bits_opcode),
    .io_enq_bits_size(bundleIn_0_d_q_io_enq_bits_size),
    .io_enq_bits_source(bundleIn_0_d_q_io_enq_bits_source),
    .io_enq_bits_data(bundleIn_0_d_q_io_enq_bits_data),
    .io_deq_ready(bundleIn_0_d_q_io_deq_ready),
    .io_deq_valid(bundleIn_0_d_q_io_deq_valid),
    .io_deq_bits_opcode(bundleIn_0_d_q_io_deq_bits_opcode),
    .io_deq_bits_size(bundleIn_0_d_q_io_deq_bits_size),
    .io_deq_bits_source(bundleIn_0_d_q_io_deq_bits_source),
    .io_deq_bits_data(bundleIn_0_d_q_io_deq_bits_data)
  );
  assign bundleOut_0_a_q_rf_reset = rf_reset;
  assign bundleIn_0_d_q_rf_reset = rf_reset;
  assign auto_in_a_ready = bundleOut_0_a_q_io_enq_ready; // @[Nodes.scala 1529:13 Decoupled.scala 299:17]
  assign auto_in_d_valid = bundleIn_0_d_q_io_deq_valid; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_d_bits_opcode = bundleIn_0_d_q_io_deq_bits_opcode; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_d_bits_size = bundleIn_0_d_q_io_deq_bits_size; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_d_bits_source = bundleIn_0_d_q_io_deq_bits_source; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_in_d_bits_data = bundleIn_0_d_q_io_deq_bits_data; // @[Nodes.scala 1529:13 Buffer.scala 38:13]
  assign auto_out_a_valid = bundleOut_0_a_q_io_deq_valid; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_opcode = bundleOut_0_a_q_io_deq_bits_opcode; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_param = bundleOut_0_a_q_io_deq_bits_param; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_size = bundleOut_0_a_q_io_deq_bits_size; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_source = bundleOut_0_a_q_io_deq_bits_source; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_address = bundleOut_0_a_q_io_deq_bits_address; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_mask = bundleOut_0_a_q_io_deq_bits_mask; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_data = bundleOut_0_a_q_io_deq_bits_data; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_a_bits_corrupt = bundleOut_0_a_q_io_deq_bits_corrupt; // @[Nodes.scala 1529:13 Buffer.scala 37:13]
  assign auto_out_d_ready = bundleIn_0_d_q_io_enq_ready; // @[Nodes.scala 1529:13 Decoupled.scala 299:17]
  assign bundleOut_0_a_q_clock = clock;
  assign bundleOut_0_a_q_reset = reset;
  assign bundleOut_0_a_q_io_enq_valid = auto_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_param = auto_in_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_size = auto_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_source = auto_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_address = auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_data = auto_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_enq_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bundleOut_0_a_q_io_deq_ready = auto_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_clock = clock;
  assign bundleIn_0_d_q_reset = reset;
  assign bundleIn_0_d_q_io_enq_valid = auto_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_size = auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_source = auto_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_enq_bits_data = auto_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign bundleIn_0_d_q_io_deq_ready = auto_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
endmodule
