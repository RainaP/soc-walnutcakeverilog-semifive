module cpu_rom_wrapper #(
    parameter AW = 32,
    parameter DW = 64,
    parameter IW = 11,
    parameter UW = 0,
    localparam SW = DW/8)
(
    input              CLK__ROM,
    input              RSTN__ROM,

    input   [IW-1:0]   AXI4__ROM__AWID,
    input   [AW-1:0]   AXI4__ROM__AWADDR,
    input   [7:0]      AXI4__ROM__AWLEN,
    input   [2:0]      AXI4__ROM__AWSIZE,
    input   [1:0]      AXI4__ROM__AWBURST,
    input              AXI4__ROM__AWLOCK,
    input   [3:0]      AXI4__ROM__AWCACHE,
    input   [2:0]      AXI4__ROM__AWPROT,
    input   [UW-1:0]   AXI4__ROM__AWUSER,
    input              AXI4__ROM__AWVALID,
    output             AXI4__ROM__AWREADY,
    input   [DW-1:0]   AXI4__ROM__WDATA,
    input   [SW-1:0]   AXI4__ROM__WSTRB,
    input              AXI4__ROM__WLAST,
    input              AXI4__ROM__WVALID,
    output             AXI4__ROM__WREADY,
    output  [IW-1:0]   AXI4__ROM__BID,
    output             AXI4__ROM__BVALID,
    output  [1:0]      AXI4__ROM__BRESP,
    input              AXI4__ROM__BREADY,
    input   [IW-1:0]   AXI4__ROM__ARID,
    input   [AW-1:0]   AXI4__ROM__ARADDR,
    input   [7:0]      AXI4__ROM__ARLEN,
    input   [2:0]      AXI4__ROM__ARSIZE,
    input   [1:0]      AXI4__ROM__ARBURST,
    input              AXI4__ROM__ARLOCK,
    input   [3:0]      AXI4__ROM__ARCACHE,
    input   [2:0]      AXI4__ROM__ARPROT,
    input   [UW-1:0]   AXI4__ROM__ARUSER,
    input              AXI4__ROM__ARVALID,
    output             AXI4__ROM__ARREADY,
    output  [IW-1:0]   AXI4__ROM__RID,
    output  [DW-1:0]   AXI4__ROM__RDATA,
    output  [1:0]      AXI4__ROM__RRESP,
    output             AXI4__ROM__RLAST,
    output             AXI4__ROM__RVALID,
    input              AXI4__ROM__RREADY

);

wire [AW-1:0] gs_maddr;
wire        gs_mread;
wire        gs_mwrite;
wire [DW-1:0] gs_sdata;
reg  [1:0]  gs_sresp;

wire [12:0] rom_address;
wire        rom_enable;
wire        rom_readenable;
wire [DW-1:0] rom_readdata;

axi_gs_64_11_DW_axi_gs uPERI_ROM_0 (
    .aclk          (CLK__ROM), // input
    .aresetn       (RSTN__ROM), // input
    .bid           (AXI4__ROM__BID), // output [IW-1:0]
    .bresp         (AXI4__ROM__BRESP), // output [1:0]
    .rid           (AXI4__ROM__RID), // output [IW-1:0]
    .rdata         (AXI4__ROM__RDATA), // output [63:0]
    .rresp         (AXI4__ROM__RRESP), // output [1:0]
    .awid          (AXI4__ROM__AWID), // input [IW-1:0]
    .awaddr        (AXI4__ROM__AWADDR), // input [AW-1:0]
    .awlen         (AXI4__ROM__AWLEN), // input [3:0]
    .awsize        (AXI4__ROM__AWSIZE), // input [2:0]
    .awburst       (AXI4__ROM__AWBURST), // input [1:0]
    .awlock        (AXI4__ROM__AWLOCK), // input [1:0]
    .awcache       (AXI4__ROM__AWCACHE), // input [3:0]
    .awprot        (AXI4__ROM__AWPROT), // input [2:0]
    .wdata         (AXI4__ROM__WDATA), // input [63:0]
    .wstrb         (AXI4__ROM__WSTRB), // input [7:0]
    .arid          (AXI4__ROM__ARID), // input [IW-1:0]
    .araddr        (AXI4__ROM__ARADDR), // input [AW-1:0]
    .arlen         (AXI4__ROM__ARLEN), // input [3:0]
    .arsize        (AXI4__ROM__ARSIZE), // input [2:0]
    .arburst       (AXI4__ROM__ARBURST), // input [1:0]
    .arlock        (AXI4__ROM__ARLOCK), // input [1:0]
    .arcache       (AXI4__ROM__ARCACHE), // input [3:0]
    .arprot        (AXI4__ROM__ARPROT), // input [2:0]
    .awvalid       (AXI4__ROM__AWVALID), // input
    .wlast         (AXI4__ROM__WLAST), // input
    .wvalid        (AXI4__ROM__WVALID), // input
    .bready        (AXI4__ROM__BREADY), // input
    .arvalid       (AXI4__ROM__ARVALID), // input
    .rready        (AXI4__ROM__RREADY), // input
    .awready       (AXI4__ROM__AWREADY), // output
    .wready        (AXI4__ROM__WREADY), // output
    .bvalid        (AXI4__ROM__BVALID), // output
    .arready       (AXI4__ROM__ARREADY), // output
    .rlast         (AXI4__ROM__RLAST), // output
    .rvalid        (AXI4__ROM__RVALID), // output
    .gclken        (1'b1), // input
    .sdata         (gs_sdata), // input [63:0]
    .sresp         (gs_sresp), // input [1:0]
    .saccept       (1'b1), // input
    .svalid        (1'b1), // input
    .maddr         (gs_maddr), // output [AW-1:0]
    .msize         (), // output [2:0]
    .mburst        (), // output [1:0]
    .mlen          (), // output [3:0]
    .mdata         (), // output [63:0]
    .mwstrb        (), // output [7:0]
    .mread         (gs_mread), // output
    .mwrite        (gs_mwrite), // output
    .mlast         (), // output
    .mready        ()  // output
);


assign rom_enable = gs_mread;
assign rom_readenable = gs_mread;
assign rom_address = gs_maddr[15:0]>>3;

assign gs_sdata = rom_readdata;

always @(posedge CLK__ROM or negedge RSTN__ROM) begin
    if (~RSTN__ROM)
        gs_sresp <= 2'b00;
    else if (gs_mwrite)
        gs_sresp <= 2'b01;
    else if (gs_mread)
        gs_sresp <= 2'b00;
end

SEMI5_VROM #(.ABIT(13), .WORD(8192), .DBIT(64)) CPU_ROM_0 (
    .clock      (CLK__ROM),
    .enable     (rom_enable),
    .address    (rom_address),
    .readenable (rom_readenable),
    .readdata   (rom_readdata)
);

endmodule
