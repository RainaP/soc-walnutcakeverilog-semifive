//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__IntSyncCrossingSource_11(
  input   clock,
  input   reset,
  input   auto_in_0,
  input   auto_in_1,
  output  auto_out_sync_0,
  output  auto_out_sync_1
);
  wire  reg__clock; // @[AsyncResetReg.scala 89:21]
  wire  reg__reset; // @[AsyncResetReg.scala 89:21]
  wire [1:0] reg__io_d; // @[AsyncResetReg.scala 89:21]
  wire [1:0] reg__io_q; // @[AsyncResetReg.scala 89:21]
  RHEA__AsyncResetRegVec_w2_i0 reg_ ( // @[AsyncResetReg.scala 89:21]
    .clock(reg__clock),
    .reset(reg__reset),
    .io_d(reg__io_d),
    .io_q(reg__io_q)
  );
  assign auto_out_sync_0 = reg__io_q[0]; // @[Crossing.scala 42:52]
  assign auto_out_sync_1 = reg__io_q[1]; // @[Crossing.scala 42:52]
  assign reg__clock = clock;
  assign reg__reset = reset;
  assign reg__io_d = {auto_in_1,auto_in_0}; // @[Cat.scala 30:58]
endmodule
