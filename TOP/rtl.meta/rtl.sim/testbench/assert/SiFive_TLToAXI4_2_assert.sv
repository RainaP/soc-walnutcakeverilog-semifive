//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLToAXI4_2_assert(
  input         clock,
  input         reset,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [2:0]  auto_in_a_bits_param,
  input  [1:0]  auto_in_a_bits_size,
  input  [10:0] auto_in_a_bits_source,
  input  [29:0] auto_in_a_bits_address,
  input  [7:0]  auto_in_a_bits_mask,
  input         auto_in_a_bits_corrupt,
  input         auto_in_d_ready,
  input         auto_out_b_valid,
  input  [10:0] auto_out_b_bits_echo_tl_state_source,
  input         auto_out_r_valid,
  input  [10:0] auto_out_r_bits_echo_tl_state_source,
  input  [10:0] _T_91,
  input         _T_55,
  input         _T_58,
  input         _T_74,
  input         _GEN_3652,
  input         _T_84,
  input         _T_85,
  input  [1:0]  _T_86_size,
  input  [1:0]  _T_87_size,
  input         _T_94,
  input         _T_98
);
  wire  TLMonitor_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [10:0] TLMonitor_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [29:0] TLMonitor_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [10:0] TLMonitor_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  _T_103; // @[ToAXI4.scala 255:17]
  wire  _T_104; // @[ToAXI4.scala 255:31]
  wire  _T_105; // @[ToAXI4.scala 255:22]
  wire  _T_107; // @[ToAXI4.scala 255:16]
  wire  _T_108; // @[ToAXI4.scala 255:16]
  wire  _T_109; // @[ToAXI4.scala 256:17]
  wire  _T_110; // @[ToAXI4.scala 256:31]
  wire  _T_111; // @[ToAXI4.scala 256:22]
  wire  _T_113; // @[ToAXI4.scala 256:16]
  wire  _T_114; // @[ToAXI4.scala 256:16]
  SiFive_TLMonitor_48_assert TLMonitor ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_clock),
    .reset(TLMonitor_reset),
    .io_in_a_ready(TLMonitor_io_in_a_ready),
    .io_in_a_valid(TLMonitor_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_io_in_a_bits_corrupt),
    .io_in_d_ready(TLMonitor_io_in_d_ready),
    .io_in_d_valid(TLMonitor_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_io_in_d_bits_opcode),
    .io_in_d_bits_size(TLMonitor_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_io_in_d_bits_source),
    .io_in_d_bits_denied(TLMonitor_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_io_in_d_bits_corrupt)
  );
  assign _T_103 = ~_T_98; // @[ToAXI4.scala 255:17]
  assign _T_104 = _T_91 != 11'h0; // @[ToAXI4.scala 255:31]
  assign _T_105 = _T_103 | _T_104; // @[ToAXI4.scala 255:22]
  assign _T_107 = _T_105 | reset; // @[ToAXI4.scala 255:16]
  assign _T_108 = ~_T_107; // @[ToAXI4.scala 255:16]
  assign _T_109 = ~_T_94; // @[ToAXI4.scala 256:17]
  assign _T_110 = _T_91 != 11'h720; // @[ToAXI4.scala 256:31]
  assign _T_111 = _T_109 | _T_110; // @[ToAXI4.scala 256:22]
  assign _T_113 = _T_111 | reset; // @[ToAXI4.scala 256:16]
  assign _T_114 = ~_T_113; // @[ToAXI4.scala 256:16]
  assign TLMonitor_clock = clock;
  assign TLMonitor_reset = reset;
  assign TLMonitor_io_in_a_ready = _T_55 & _T_58; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_valid = auto_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_size = auto_in_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_source = auto_in_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_ready = auto_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_valid = _T_74 ? auto_out_r_valid : auto_out_b_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_opcode = _T_74 ? 3'h1 : 3'h0; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_size = _T_74 ? _T_86_size : _T_87_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_source = _T_74 ? auto_out_r_bits_echo_tl_state_source : auto_out_b_bits_echo_tl_state_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_denied = _T_74 ? _GEN_3652 : _T_84; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_corrupt = _T_74 & _T_85; // @[Nodes.scala 26:19]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_108) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:255 assert (!dec || count =/= UInt(0))        // underflow\n"); // @[ToAXI4.scala 255:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_108) begin
          $fatal; // @[ToAXI4.scala 255:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_114) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at ToAXI4.scala:256 assert (!inc || count =/= UInt(maxCount)) // overflow\n"); // @[ToAXI4.scala 256:16]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_114) begin
          $fatal; // @[ToAXI4.scala 256:16]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
