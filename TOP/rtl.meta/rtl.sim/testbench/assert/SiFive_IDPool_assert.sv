//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_IDPool_assert(
  input        clock,
  input        reset,
  input        io_free_valid,
  input  [2:0] io_free_bits,
  input  [7:0] _T_4
);
  wire  _T_32; // @[IDPool.scala 43:11]
  wire [7:0] _T_35; // @[IDPool.scala 43:47]
  wire  _T_37; // @[IDPool.scala 43:29]
  wire  _T_38; // @[IDPool.scala 43:26]
  wire  _T_40; // @[IDPool.scala 43:10]
  wire  _T_41; // @[IDPool.scala 43:10]
  assign _T_32 = ~io_free_valid; // @[IDPool.scala 43:11]
  assign _T_35 = _T_4 >> io_free_bits; // @[IDPool.scala 43:47]
  assign _T_37 = ~_T_35[0]; // @[IDPool.scala 43:29]
  assign _T_38 = _T_32 | _T_37; // @[IDPool.scala 43:26]
  assign _T_40 = _T_38 | reset; // @[IDPool.scala 43:10]
  assign _T_41 = ~_T_40; // @[IDPool.scala 43:10]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_41) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at IDPool.scala:43 assert (!io.free.valid || !(bitmap & ~taken)(io.free.bits))\n"); // @[IDPool.scala 43:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_41) begin
          $fatal; // @[IDPool.scala 43:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
