module dbus_cpu(/*AUTOARG*/
   // Outputs
   CPU_0__AXI4__CPU__SYSPORT__AWREADY,
   CPU_0__AXI4__CPU__SYSPORT__WREADY,
   CPU_0__AXI4__CPU__SYSPORT__BVALID, CPU_0__AXI4__CPU__SYSPORT__BID,
   CPU_0__AXI4__CPU__SYSPORT__BRESP,
   CPU_0__AXI4__CPU__SYSPORT__ARREADY,
   CPU_0__AXI4__CPU__SYSPORT__RVALID, CPU_0__AXI4__CPU__SYSPORT__RID,
   CPU_0__AXI4__CPU__SYSPORT__RDATA, CPU_0__AXI4__CPU__SYSPORT__RRESP,
   CPU_0__AXI4__CPU__SYSPORT__RLAST,
   CPU_0__AXI4__CPU__MEMPORT__AWREADY,
   CPU_0__AXI4__CPU__MEMPORT__WREADY,
   CPU_0__AXI4__CPU__MEMPORT__BVALID, CPU_0__AXI4__CPU__MEMPORT__BID,
   CPU_0__AXI4__CPU__MEMPORT__BRESP,
   CPU_0__AXI4__CPU__MEMPORT__ARREADY,
   CPU_0__AXI4__CPU__MEMPORT__RVALID, CPU_0__AXI4__CPU__MEMPORT__RID,
   CPU_0__AXI4__CPU__MEMPORT__RDATA, CPU_0__AXI4__CPU__MEMPORT__RRESP,
   CPU_0__AXI4__CPU__MEMPORT__RLAST, AXI4__TX__ARID, AXI4__TX__ARADDR,
   AXI4__TX__ARLEN, AXI4__TX__ARSIZE, AXI4__TX__ARBURST,
   AXI4__TX__ARLOCK, AXI4__TX__ARCACHE, AXI4__TX__ARPROT,
   AXI4__TX__ARUSER, AXI4__TX__ARVALID, AXI4__TX__RREADY,
   AXI4__TX__AWADDR, AXI4__TX__AWID, AXI4__TX__AWLEN,
   AXI4__TX__AWSIZE, AXI4__TX__AWBURST, AXI4__TX__AWLOCK,
   AXI4__TX__AWCACHE, AXI4__TX__AWPROT, AXI4__TX__AWUSER,
   AXI4__TX__AWVALID, AXI4__TX__WLAST, AXI4__TX__WDATA,
   AXI4__TX__WSTRB, AXI4__TX__WVALID, AXI4__TX__BREADY,
   // Inputs
   CLK__DBUS, RSTN__DBUS, CLK__TX, RSTN__TX,
   CPU_0__AXI4__CPU__SYSPORT__AWVALID,
   CPU_0__AXI4__CPU__SYSPORT__AWID, CPU_0__AXI4__CPU__SYSPORT__AWADDR,
   CPU_0__AXI4__CPU__SYSPORT__AWLEN,
   CPU_0__AXI4__CPU__SYSPORT__AWSIZE,
   CPU_0__AXI4__CPU__SYSPORT__AWBURST,
   CPU_0__AXI4__CPU__SYSPORT__AWLOCK,
   CPU_0__AXI4__CPU__SYSPORT__AWCACHE,
   CPU_0__AXI4__CPU__SYSPORT__AWPROT,
   CPU_0__AXI4__CPU__SYSPORT__AWUSER,
   CPU_0__AXI4__CPU__SYSPORT__WVALID,
   CPU_0__AXI4__CPU__SYSPORT__WDATA, CPU_0__AXI4__CPU__SYSPORT__WSTRB,
   CPU_0__AXI4__CPU__SYSPORT__WLAST,
   CPU_0__AXI4__CPU__SYSPORT__BREADY,
   CPU_0__AXI4__CPU__SYSPORT__ARVALID,
   CPU_0__AXI4__CPU__SYSPORT__ARID, CPU_0__AXI4__CPU__SYSPORT__ARADDR,
   CPU_0__AXI4__CPU__SYSPORT__ARLEN,
   CPU_0__AXI4__CPU__SYSPORT__ARSIZE,
   CPU_0__AXI4__CPU__SYSPORT__ARBURST,
   CPU_0__AXI4__CPU__SYSPORT__ARLOCK,
   CPU_0__AXI4__CPU__SYSPORT__ARCACHE,
   CPU_0__AXI4__CPU__SYSPORT__ARPROT,
   CPU_0__AXI4__CPU__SYSPORT__ARUSER,
   CPU_0__AXI4__CPU__SYSPORT__RREADY,
   CPU_0__AXI4__CPU__MEMPORT__AWVALID,
   CPU_0__AXI4__CPU__MEMPORT__AWID, CPU_0__AXI4__CPU__MEMPORT__AWADDR,
   CPU_0__AXI4__CPU__MEMPORT__AWLEN,
   CPU_0__AXI4__CPU__MEMPORT__AWSIZE,
   CPU_0__AXI4__CPU__MEMPORT__AWBURST,
   CPU_0__AXI4__CPU__MEMPORT__AWLOCK,
   CPU_0__AXI4__CPU__MEMPORT__AWCACHE,
   CPU_0__AXI4__CPU__MEMPORT__AWPROT,
   CPU_0__AXI4__CPU__MEMPORT__AWUSER,
   CPU_0__AXI4__CPU__MEMPORT__WVALID,
   CPU_0__AXI4__CPU__MEMPORT__WDATA, CPU_0__AXI4__CPU__MEMPORT__WSTRB,
   CPU_0__AXI4__CPU__MEMPORT__WLAST,
   CPU_0__AXI4__CPU__MEMPORT__BREADY,
   CPU_0__AXI4__CPU__MEMPORT__ARVALID,
   CPU_0__AXI4__CPU__MEMPORT__ARID, CPU_0__AXI4__CPU__MEMPORT__ARADDR,
   CPU_0__AXI4__CPU__MEMPORT__ARLEN,
   CPU_0__AXI4__CPU__MEMPORT__ARSIZE,
   CPU_0__AXI4__CPU__MEMPORT__ARBURST,
   CPU_0__AXI4__CPU__MEMPORT__ARLOCK,
   CPU_0__AXI4__CPU__MEMPORT__ARCACHE,
   CPU_0__AXI4__CPU__MEMPORT__ARPROT,
   CPU_0__AXI4__CPU__MEMPORT__ARUSER,
   CPU_0__AXI4__CPU__MEMPORT__RREADY, AXI4__TX__ARREADY,
   AXI4__TX__RID, AXI4__TX__RDATA, AXI4__TX__RRESP, AXI4__TX__RLAST,
   AXI4__TX__RVALID, AXI4__TX__AWREADY, AXI4__TX__WREADY,
   AXI4__TX__BID, AXI4__TX__BRESP, AXI4__TX__BVALID
   );
    parameter   SYS_AW = 36; // SYSPORT
    parameter   SYS_DW = 128;
    parameter   SYS_IW = 8;
    parameter   SYS_UW = 10;
    parameter   MEM_AW = 36; // MEMPORT
    parameter   MEM_DW = 128;
    parameter   MEM_IW = 8;
    parameter   MEM_UW = 10;
    parameter   DBUS_AW = 36; // DBUS
    parameter   DBUS_DW = 128;
    parameter   DBUS_IW = 9;
    parameter   DBUS_UW = 10;

    localparam  SYS_SW = SYS_DW/8;
    localparam  MEM_SW = MEM_DW/8;
    localparam  DBUS_SW = DBUS_DW/8;

    input           CLK__DBUS;
    input           RSTN__DBUS;
    input           CLK__TX;
    input           RSTN__TX;

    // System Port from CPU
    output              CPU_0__AXI4__CPU__SYSPORT__AWREADY;
    input               CPU_0__AXI4__CPU__SYSPORT__AWVALID;
    input [SYS_IW-1:0]   CPU_0__AXI4__CPU__SYSPORT__AWID;
    input [SYS_AW-1:0]   CPU_0__AXI4__CPU__SYSPORT__AWADDR;
    input [7:0]         CPU_0__AXI4__CPU__SYSPORT__AWLEN;
    input [2:0]         CPU_0__AXI4__CPU__SYSPORT__AWSIZE;
    input [1:0]         CPU_0__AXI4__CPU__SYSPORT__AWBURST;
    input [0:0]         CPU_0__AXI4__CPU__SYSPORT__AWLOCK;
    input [3:0]         CPU_0__AXI4__CPU__SYSPORT__AWCACHE;
    input [2:0]         CPU_0__AXI4__CPU__SYSPORT__AWPROT;
    input [SYS_UW-1:0]   CPU_0__AXI4__CPU__SYSPORT__AWUSER;
    output              CPU_0__AXI4__CPU__SYSPORT__WREADY;
    input               CPU_0__AXI4__CPU__SYSPORT__WVALID;
    input [SYS_DW-1:0]   CPU_0__AXI4__CPU__SYSPORT__WDATA;
    input [SYS_SW-1:0]   CPU_0__AXI4__CPU__SYSPORT__WSTRB;
    input               CPU_0__AXI4__CPU__SYSPORT__WLAST;
    input               CPU_0__AXI4__CPU__SYSPORT__BREADY;
    output              CPU_0__AXI4__CPU__SYSPORT__BVALID;
    output  [SYS_IW-1:0] CPU_0__AXI4__CPU__SYSPORT__BID;
    output  [1:0]       CPU_0__AXI4__CPU__SYSPORT__BRESP;
    output              CPU_0__AXI4__CPU__SYSPORT__ARREADY;
    input               CPU_0__AXI4__CPU__SYSPORT__ARVALID;
    input [SYS_IW-1:0]   CPU_0__AXI4__CPU__SYSPORT__ARID;
    input [SYS_AW-1:0]   CPU_0__AXI4__CPU__SYSPORT__ARADDR;
    input [7:0]         CPU_0__AXI4__CPU__SYSPORT__ARLEN;
    input [2:0]         CPU_0__AXI4__CPU__SYSPORT__ARSIZE;
    input [1:0]         CPU_0__AXI4__CPU__SYSPORT__ARBURST;
    input [0:0]         CPU_0__AXI4__CPU__SYSPORT__ARLOCK;
    input [3:0]         CPU_0__AXI4__CPU__SYSPORT__ARCACHE;
    input [2:0]         CPU_0__AXI4__CPU__SYSPORT__ARPROT;
    input [SYS_UW-1:0]   CPU_0__AXI4__CPU__SYSPORT__ARUSER;
    input               CPU_0__AXI4__CPU__SYSPORT__RREADY;
    output              CPU_0__AXI4__CPU__SYSPORT__RVALID;
    output  [SYS_IW-1:0] CPU_0__AXI4__CPU__SYSPORT__RID;
    output  [SYS_DW-1:0] CPU_0__AXI4__CPU__SYSPORT__RDATA;
    output  [1:0]       CPU_0__AXI4__CPU__SYSPORT__RRESP;
    output              CPU_0__AXI4__CPU__SYSPORT__RLAST;

    // Memory port from CPU
    output              CPU_0__AXI4__CPU__MEMPORT__AWREADY;
    input               CPU_0__AXI4__CPU__MEMPORT__AWVALID;
    input [MEM_IW-1:0]   CPU_0__AXI4__CPU__MEMPORT__AWID;
    input [MEM_AW-1:0]   CPU_0__AXI4__CPU__MEMPORT__AWADDR;
    input [7:0]         CPU_0__AXI4__CPU__MEMPORT__AWLEN;
    input [2:0]         CPU_0__AXI4__CPU__MEMPORT__AWSIZE;
    input [1:0]         CPU_0__AXI4__CPU__MEMPORT__AWBURST;
    input [0:0]         CPU_0__AXI4__CPU__MEMPORT__AWLOCK;
    input [3:0]         CPU_0__AXI4__CPU__MEMPORT__AWCACHE;
    input [2:0]         CPU_0__AXI4__CPU__MEMPORT__AWPROT;
    input [MEM_UW-1:0]   CPU_0__AXI4__CPU__MEMPORT__AWUSER;
    output              CPU_0__AXI4__CPU__MEMPORT__WREADY;
    input               CPU_0__AXI4__CPU__MEMPORT__WVALID;
    input [MEM_DW-1:0]   CPU_0__AXI4__CPU__MEMPORT__WDATA;
    input [MEM_SW-1:0]   CPU_0__AXI4__CPU__MEMPORT__WSTRB;
    input               CPU_0__AXI4__CPU__MEMPORT__WLAST;
    input               CPU_0__AXI4__CPU__MEMPORT__BREADY;
    output              CPU_0__AXI4__CPU__MEMPORT__BVALID;
    output  [MEM_IW-1:0] CPU_0__AXI4__CPU__MEMPORT__BID;
    output  [1:0]       CPU_0__AXI4__CPU__MEMPORT__BRESP;
    output              CPU_0__AXI4__CPU__MEMPORT__ARREADY;
    input               CPU_0__AXI4__CPU__MEMPORT__ARVALID;
    input [MEM_IW-1:0]   CPU_0__AXI4__CPU__MEMPORT__ARID;
    input [MEM_AW-1:0]   CPU_0__AXI4__CPU__MEMPORT__ARADDR;
    input [7:0]         CPU_0__AXI4__CPU__MEMPORT__ARLEN;
    input [2:0]         CPU_0__AXI4__CPU__MEMPORT__ARSIZE;
    input [1:0]         CPU_0__AXI4__CPU__MEMPORT__ARBURST;
    input [0:0]         CPU_0__AXI4__CPU__MEMPORT__ARLOCK;
    input [3:0]         CPU_0__AXI4__CPU__MEMPORT__ARCACHE;
    input [2:0]         CPU_0__AXI4__CPU__MEMPORT__ARPROT;
    input [MEM_UW-1:0]   CPU_0__AXI4__CPU__MEMPORT__ARUSER;
    input               CPU_0__AXI4__CPU__MEMPORT__RREADY;
    output              CPU_0__AXI4__CPU__MEMPORT__RVALID;
    output  [MEM_IW-1:0] CPU_0__AXI4__CPU__MEMPORT__RID;
    output  [MEM_DW-1:0] CPU_0__AXI4__CPU__MEMPORT__RDATA;
    output  [1:0]       CPU_0__AXI4__CPU__MEMPORT__RRESP;
    output              CPU_0__AXI4__CPU__MEMPORT__RLAST;

    // AXI4 Master to DBUS
    output [DBUS_IW-1:0]  AXI4__TX__ARID;
    output [DBUS_AW-1:0]  AXI4__TX__ARADDR;
    output [7:0]        AXI4__TX__ARLEN;
    output [2:0]        AXI4__TX__ARSIZE;
    output [1:0]        AXI4__TX__ARBURST;
    output [0:0]        AXI4__TX__ARLOCK;
    output [3:0]        AXI4__TX__ARCACHE;
    output [2:0]        AXI4__TX__ARPROT;
    output [DBUS_UW-1:0]  AXI4__TX__ARUSER;
    output              AXI4__TX__ARVALID;
    input               AXI4__TX__ARREADY;
    input [DBUS_IW-1:0]   AXI4__TX__RID;
    input [DBUS_DW-1:0]   AXI4__TX__RDATA;
    input [1:0]         AXI4__TX__RRESP;
    input               AXI4__TX__RLAST;
    input               AXI4__TX__RVALID;
    output              AXI4__TX__RREADY;
    output [DBUS_AW-1:0]  AXI4__TX__AWADDR;
    output [DBUS_IW-1:0]  AXI4__TX__AWID;
    output [7:0]        AXI4__TX__AWLEN;
    output [2:0]        AXI4__TX__AWSIZE;
    output [1:0]        AXI4__TX__AWBURST;
    output [0:0]        AXI4__TX__AWLOCK;
    output [3:0]        AXI4__TX__AWCACHE;
    output [2:0]        AXI4__TX__AWPROT;
    output [DBUS_UW-1:0]  AXI4__TX__AWUSER;
    output              AXI4__TX__AWVALID;
    input               AXI4__TX__AWREADY;
    output              AXI4__TX__WLAST;
    output [DBUS_DW-1:0]  AXI4__TX__WDATA;
    output [DBUS_SW-1:0]  AXI4__TX__WSTRB;
    output              AXI4__TX__WVALID;
    input               AXI4__TX__WREADY;
    input [DBUS_IW-1:0]   AXI4__TX__BID;
    input [1:0]         AXI4__TX__BRESP;
    input               AXI4__TX__BVALID;
    output              AXI4__TX__BREADY;

`include "./axi_2x1_cpu_mem/axi_2x1_cpu_mem_DW_axi_all_includes.vh"
// Wire & Reg
// {{{
    wor [31:0] NC;
    /*AUTOWIRE*/
    /*AUTOREG*/
// }}}

    /* axi_2x1_cpu_mem_DW_axi AUTO_TEMPLATE (
        .aclk               (CLK__DBUS),
        .aresetn            (RSTN__DBUS),
        .mst_priority_m1    (1'b1),
		.awsideband_m1	    (CPU_0__AXI4__CPU__MEMPORT__AWUSER[]),
		.arsideband_m1	    (CPU_0__AXI4__CPU__MEMPORT__ARUSER[]),
        .\(.*\)_m1          (CPU_0__AXI4__CPU__MEMPORT__@"(upcase \\"\1\\")"[]),
        .mst_priority_m2    (1'b0),
		.awsideband_m2	    (CPU_0__AXI4__CPU__SYSPORT__AWUSER[]),
		.arsideband_m2	    (CPU_0__AXI4__CPU__SYSPORT__ARUSER[]),
        .\(.*\)_m2          (CPU_0__AXI4__CPU__SYSPORT__@"(upcase \\"\1\\")"[]),
        .slv_priority_s1    (1'b0),
		.awsideband_s1	    (AXI4__TX__AWUSER[]),
		.arsideband_s1	    (AXI4__TX__ARUSER[]),
        .\(.*\)_s1          (AXI4__TX__@"(upcase \\"\1\\")"[]),
        .csysreq            (1'b0),
        .csysack            (),
        .cactive            (),
        .dbg_.*             (),
    );*/
    axi_2x1_cpu_mem_DW_axi AXI_XBAR_CPU_MEM_0 (/*AUTOINST*/
					       // Outputs
					       .awready_m1	(CPU_0__AXI4__CPU__MEMPORT__AWREADY), // Templated
					       .wready_m1	(CPU_0__AXI4__CPU__MEMPORT__WREADY), // Templated
					       .bvalid_m1	(CPU_0__AXI4__CPU__MEMPORT__BVALID), // Templated
					       .bid_m1		(CPU_0__AXI4__CPU__MEMPORT__BID[`axi_2x1_cpu_mem_AXI_IDW_M1-1:0]), // Templated
					       .bresp_m1	(CPU_0__AXI4__CPU__MEMPORT__BRESP[`axi_2x1_cpu_mem_AXI_BRW-1:0]), // Templated
					       .arready_m1	(CPU_0__AXI4__CPU__MEMPORT__ARREADY), // Templated
					       .rvalid_m1	(CPU_0__AXI4__CPU__MEMPORT__RVALID), // Templated
					       .rid_m1		(CPU_0__AXI4__CPU__MEMPORT__RID[`axi_2x1_cpu_mem_AXI_IDW_M1-1:0]), // Templated
					       .rdata_m1	(CPU_0__AXI4__CPU__MEMPORT__RDATA[`axi_2x1_cpu_mem_AXI_DW-1:0]), // Templated
					       .rlast_m1	(CPU_0__AXI4__CPU__MEMPORT__RLAST), // Templated
					       .rresp_m1	(CPU_0__AXI4__CPU__MEMPORT__RRESP[`axi_2x1_cpu_mem_AXI_RRW-1:0]), // Templated
					       .awready_m2	(CPU_0__AXI4__CPU__SYSPORT__AWREADY), // Templated
					       .wready_m2	(CPU_0__AXI4__CPU__SYSPORT__WREADY), // Templated
					       .bvalid_m2	(CPU_0__AXI4__CPU__SYSPORT__BVALID), // Templated
					       .bid_m2		(CPU_0__AXI4__CPU__SYSPORT__BID[`axi_2x1_cpu_mem_AXI_IDW_M2-1:0]), // Templated
					       .bresp_m2	(CPU_0__AXI4__CPU__SYSPORT__BRESP[`axi_2x1_cpu_mem_AXI_BRW-1:0]), // Templated
					       .arready_m2	(CPU_0__AXI4__CPU__SYSPORT__ARREADY), // Templated
					       .rvalid_m2	(CPU_0__AXI4__CPU__SYSPORT__RVALID), // Templated
					       .rid_m2		(CPU_0__AXI4__CPU__SYSPORT__RID[`axi_2x1_cpu_mem_AXI_IDW_M2-1:0]), // Templated
					       .rdata_m2	(CPU_0__AXI4__CPU__SYSPORT__RDATA[`axi_2x1_cpu_mem_AXI_DW-1:0]), // Templated
					       .rlast_m2	(CPU_0__AXI4__CPU__SYSPORT__RLAST), // Templated
					       .rresp_m2	(CPU_0__AXI4__CPU__SYSPORT__RRESP[`axi_2x1_cpu_mem_AXI_RRW-1:0]), // Templated
					       .awvalid_s1	(AXI4__TX__AWVALID), // Templated
					       .awaddr_s1	(AXI4__TX__AWADDR[`axi_2x1_cpu_mem_AXI_AW-1:0]), // Templated
					       .awid_s1		(AXI4__TX__AWID[`axi_2x1_cpu_mem_AXI_SIDW-1:0]), // Templated
					       .awlen_s1	(AXI4__TX__AWLEN[`axi_2x1_cpu_mem_AXI_BLW-1:0]), // Templated
					       .awsize_s1	(AXI4__TX__AWSIZE[`axi_2x1_cpu_mem_AXI_BSW-1:0]), // Templated
					       .awburst_s1	(AXI4__TX__AWBURST[`axi_2x1_cpu_mem_AXI_BTW-1:0]), // Templated
					       .awlock_s1	(AXI4__TX__AWLOCK[`axi_2x1_cpu_mem_AXI_LTW-1:0]), // Templated
					       .awcache_s1	(AXI4__TX__AWCACHE[`axi_2x1_cpu_mem_AXI_CTW-1:0]), // Templated
					       .awprot_s1	(AXI4__TX__AWPROT[`axi_2x1_cpu_mem_AXI_PTW-1:0]), // Templated
					       .awuser_s1	(AXI4__TX__AWUSER[`axi_2x1_cpu_mem_AXI_AW_SBW-1:0]), // Templated
					       .wvalid_s1	(AXI4__TX__WVALID), // Templated
					       .wdata_s1	(AXI4__TX__WDATA[`axi_2x1_cpu_mem_AXI_DW-1:0]), // Templated
					       .wstrb_s1	(AXI4__TX__WSTRB[`axi_2x1_cpu_mem_AXI_SW-1:0]), // Templated
					       .wlast_s1	(AXI4__TX__WLAST), // Templated
					       .bready_s1	(AXI4__TX__BREADY), // Templated
					       .arvalid_s1	(AXI4__TX__ARVALID), // Templated
					       .arid_s1		(AXI4__TX__ARID[`axi_2x1_cpu_mem_AXI_SIDW-1:0]), // Templated
					       .araddr_s1	(AXI4__TX__ARADDR[`axi_2x1_cpu_mem_AXI_AW-1:0]), // Templated
					       .arlen_s1	(AXI4__TX__ARLEN[`axi_2x1_cpu_mem_AXI_BLW-1:0]), // Templated
					       .arsize_s1	(AXI4__TX__ARSIZE[`axi_2x1_cpu_mem_AXI_BSW-1:0]), // Templated
					       .arburst_s1	(AXI4__TX__ARBURST[`axi_2x1_cpu_mem_AXI_BTW-1:0]), // Templated
					       .arlock_s1	(AXI4__TX__ARLOCK[`axi_2x1_cpu_mem_AXI_LTW-1:0]), // Templated
					       .arcache_s1	(AXI4__TX__ARCACHE[`axi_2x1_cpu_mem_AXI_CTW-1:0]), // Templated
					       .arprot_s1	(AXI4__TX__ARPROT[`axi_2x1_cpu_mem_AXI_PTW-1:0]), // Templated
					       .aruser_s1	(AXI4__TX__ARUSER[`axi_2x1_cpu_mem_AXI_AR_SBW-1:0]), // Templated
					       .rready_s1	(AXI4__TX__RREADY), // Templated
					       .dbg_awid_s0	(),		 // Templated
					       .dbg_awaddr_s0	(),		 // Templated
					       .dbg_awlen_s0	(),		 // Templated
					       .dbg_awsize_s0	(),		 // Templated
					       .dbg_awburst_s0	(),		 // Templated
					       .dbg_awlock_s0	(),		 // Templated
					       .dbg_awcache_s0	(),		 // Templated
					       .dbg_awprot_s0	(),		 // Templated
					       .dbg_awvalid_s0	(),		 // Templated
					       .dbg_awready_s0	(),		 // Templated
					       .dbg_wid_s0	(),		 // Templated
					       .dbg_wdata_s0	(),		 // Templated
					       .dbg_wstrb_s0	(),		 // Templated
					       .dbg_wlast_s0	(),		 // Templated
					       .dbg_wvalid_s0	(),		 // Templated
					       .dbg_wready_s0	(),		 // Templated
					       .dbg_bid_s0	(),		 // Templated
					       .dbg_bresp_s0	(),		 // Templated
					       .dbg_bvalid_s0	(),		 // Templated
					       .dbg_bready_s0	(),		 // Templated
					       .dbg_arid_s0	(),		 // Templated
					       .dbg_araddr_s0	(),		 // Templated
					       .dbg_arlen_s0	(),		 // Templated
					       .dbg_arsize_s0	(),		 // Templated
					       .dbg_arburst_s0	(),		 // Templated
					       .dbg_arlock_s0	(),		 // Templated
					       .dbg_arcache_s0	(),		 // Templated
					       .dbg_arprot_s0	(),		 // Templated
					       .dbg_arvalid_s0	(),		 // Templated
					       .dbg_arready_s0	(),		 // Templated
					       .dbg_rid_s0	(),		 // Templated
					       .dbg_rdata_s0	(),		 // Templated
					       .dbg_rresp_s0	(),		 // Templated
					       .dbg_rvalid_s0	(),		 // Templated
					       .dbg_rlast_s0	(),		 // Templated
					       .dbg_rready_s0	(),		 // Templated
					       // Inputs
					       .aclk		(CLK__DBUS),	 // Templated
					       .aresetn		(RSTN__DBUS),	 // Templated
					       .awvalid_m1	(CPU_0__AXI4__CPU__MEMPORT__AWVALID), // Templated
					       .awaddr_m1	(CPU_0__AXI4__CPU__MEMPORT__AWADDR[`axi_2x1_cpu_mem_AXI_AW-1:0]), // Templated
					       .awid_m1		(CPU_0__AXI4__CPU__MEMPORT__AWID[`axi_2x1_cpu_mem_AXI_IDW_M1-1:0]), // Templated
					       .awlen_m1	(CPU_0__AXI4__CPU__MEMPORT__AWLEN[`axi_2x1_cpu_mem_AXI_BLW-1:0]), // Templated
					       .awsize_m1	(CPU_0__AXI4__CPU__MEMPORT__AWSIZE[`axi_2x1_cpu_mem_AXI_BSW-1:0]), // Templated
					       .awburst_m1	(CPU_0__AXI4__CPU__MEMPORT__AWBURST[`axi_2x1_cpu_mem_AXI_BTW-1:0]), // Templated
					       .awlock_m1	(CPU_0__AXI4__CPU__MEMPORT__AWLOCK[`axi_2x1_cpu_mem_AXI_LTW-1:0]), // Templated
					       .awcache_m1	(CPU_0__AXI4__CPU__MEMPORT__AWCACHE[`axi_2x1_cpu_mem_AXI_CTW-1:0]), // Templated
					       .awprot_m1	(CPU_0__AXI4__CPU__MEMPORT__AWPROT[`axi_2x1_cpu_mem_AXI_PTW-1:0]), // Templated
					       .awuser_m1	(CPU_0__AXI4__CPU__MEMPORT__AWUSER[`axi_2x1_cpu_mem_AXI_AW_SBW-1:0]), // Templated
					       .wvalid_m1	(CPU_0__AXI4__CPU__MEMPORT__WVALID), // Templated
					       .wdata_m1	(CPU_0__AXI4__CPU__MEMPORT__WDATA[`axi_2x1_cpu_mem_AXI_DW-1:0]), // Templated
					       .wstrb_m1	(CPU_0__AXI4__CPU__MEMPORT__WSTRB[`axi_2x1_cpu_mem_AXI_SW-1:0]), // Templated
					       .wlast_m1	(CPU_0__AXI4__CPU__MEMPORT__WLAST), // Templated
					       .bready_m1	(CPU_0__AXI4__CPU__MEMPORT__BREADY), // Templated
					       .arvalid_m1	(CPU_0__AXI4__CPU__MEMPORT__ARVALID), // Templated
					       .arid_m1		(CPU_0__AXI4__CPU__MEMPORT__ARID[`axi_2x1_cpu_mem_AXI_IDW_M1-1:0]), // Templated
					       .araddr_m1	(CPU_0__AXI4__CPU__MEMPORT__ARADDR[`axi_2x1_cpu_mem_AXI_AW-1:0]), // Templated
					       .arlen_m1	(CPU_0__AXI4__CPU__MEMPORT__ARLEN[`axi_2x1_cpu_mem_AXI_BLW-1:0]), // Templated
					       .arsize_m1	(CPU_0__AXI4__CPU__MEMPORT__ARSIZE[`axi_2x1_cpu_mem_AXI_BSW-1:0]), // Templated
					       .arburst_m1	(CPU_0__AXI4__CPU__MEMPORT__ARBURST[`axi_2x1_cpu_mem_AXI_BTW-1:0]), // Templated
					       .arlock_m1	(CPU_0__AXI4__CPU__MEMPORT__ARLOCK[`axi_2x1_cpu_mem_AXI_LTW-1:0]), // Templated
					       .arcache_m1	(CPU_0__AXI4__CPU__MEMPORT__ARCACHE[`axi_2x1_cpu_mem_AXI_CTW-1:0]), // Templated
					       .arprot_m1	(CPU_0__AXI4__CPU__MEMPORT__ARPROT[`axi_2x1_cpu_mem_AXI_PTW-1:0]), // Templated
					       .aruser_m1	(CPU_0__AXI4__CPU__MEMPORT__ARUSER[`axi_2x1_cpu_mem_AXI_AR_SBW-1:0]), // Templated
					       .rready_m1	(CPU_0__AXI4__CPU__MEMPORT__RREADY), // Templated
					       .mst_priority_m1	(1'b1),		 // Templated
					       .awvalid_m2	(CPU_0__AXI4__CPU__SYSPORT__AWVALID), // Templated
					       .awaddr_m2	(CPU_0__AXI4__CPU__SYSPORT__AWADDR[`axi_2x1_cpu_mem_AXI_AW-1:0]), // Templated
					       .awid_m2		(CPU_0__AXI4__CPU__SYSPORT__AWID[`axi_2x1_cpu_mem_AXI_IDW_M2-1:0]), // Templated
					       .awlen_m2	(CPU_0__AXI4__CPU__SYSPORT__AWLEN[`axi_2x1_cpu_mem_AXI_BLW-1:0]), // Templated
					       .awsize_m2	(CPU_0__AXI4__CPU__SYSPORT__AWSIZE[`axi_2x1_cpu_mem_AXI_BSW-1:0]), // Templated
					       .awburst_m2	(CPU_0__AXI4__CPU__SYSPORT__AWBURST[`axi_2x1_cpu_mem_AXI_BTW-1:0]), // Templated
					       .awlock_m2	(CPU_0__AXI4__CPU__SYSPORT__AWLOCK[`axi_2x1_cpu_mem_AXI_LTW-1:0]), // Templated
					       .awcache_m2	(CPU_0__AXI4__CPU__SYSPORT__AWCACHE[`axi_2x1_cpu_mem_AXI_CTW-1:0]), // Templated
					       .awprot_m2	(CPU_0__AXI4__CPU__SYSPORT__AWPROT[`axi_2x1_cpu_mem_AXI_PTW-1:0]), // Templated
					       .awuser_m2	(CPU_0__AXI4__CPU__SYSPORT__AWUSER[`axi_2x1_cpu_mem_AXI_AW_SBW-1:0]), // Templated
					       .wvalid_m2	(CPU_0__AXI4__CPU__SYSPORT__WVALID), // Templated
					       .wdata_m2	(CPU_0__AXI4__CPU__SYSPORT__WDATA[`axi_2x1_cpu_mem_AXI_DW-1:0]), // Templated
					       .wstrb_m2	(CPU_0__AXI4__CPU__SYSPORT__WSTRB[`axi_2x1_cpu_mem_AXI_SW-1:0]), // Templated
					       .wlast_m2	(CPU_0__AXI4__CPU__SYSPORT__WLAST), // Templated
					       .bready_m2	(CPU_0__AXI4__CPU__SYSPORT__BREADY), // Templated
					       .arvalid_m2	(CPU_0__AXI4__CPU__SYSPORT__ARVALID), // Templated
					       .arid_m2		(CPU_0__AXI4__CPU__SYSPORT__ARID[`axi_2x1_cpu_mem_AXI_IDW_M2-1:0]), // Templated
					       .araddr_m2	(CPU_0__AXI4__CPU__SYSPORT__ARADDR[`axi_2x1_cpu_mem_AXI_AW-1:0]), // Templated
					       .arlen_m2	(CPU_0__AXI4__CPU__SYSPORT__ARLEN[`axi_2x1_cpu_mem_AXI_BLW-1:0]), // Templated
					       .arsize_m2	(CPU_0__AXI4__CPU__SYSPORT__ARSIZE[`axi_2x1_cpu_mem_AXI_BSW-1:0]), // Templated
					       .arburst_m2	(CPU_0__AXI4__CPU__SYSPORT__ARBURST[`axi_2x1_cpu_mem_AXI_BTW-1:0]), // Templated
					       .arlock_m2	(CPU_0__AXI4__CPU__SYSPORT__ARLOCK[`axi_2x1_cpu_mem_AXI_LTW-1:0]), // Templated
					       .arcache_m2	(CPU_0__AXI4__CPU__SYSPORT__ARCACHE[`axi_2x1_cpu_mem_AXI_CTW-1:0]), // Templated
					       .arprot_m2	(CPU_0__AXI4__CPU__SYSPORT__ARPROT[`axi_2x1_cpu_mem_AXI_PTW-1:0]), // Templated
					       .aruser_m2	(CPU_0__AXI4__CPU__SYSPORT__ARUSER[`axi_2x1_cpu_mem_AXI_AR_SBW-1:0]), // Templated
					       .rready_m2	(CPU_0__AXI4__CPU__SYSPORT__RREADY), // Templated
					       .mst_priority_m2	(1'b0),		 // Templated
					       .awready_s1	(AXI4__TX__AWREADY), // Templated
					       .wready_s1	(AXI4__TX__WREADY), // Templated
					       .bvalid_s1	(AXI4__TX__BVALID), // Templated
					       .bid_s1		(AXI4__TX__BID[`axi_2x1_cpu_mem_AXI_SIDW-1:0]), // Templated
					       .bresp_s1	(AXI4__TX__BRESP[`axi_2x1_cpu_mem_AXI_BRW-1:0]), // Templated
					       .arready_s1	(AXI4__TX__ARREADY), // Templated
					       .rvalid_s1	(AXI4__TX__RVALID), // Templated
					       .rid_s1		(AXI4__TX__RID[`axi_2x1_cpu_mem_AXI_SIDW-1:0]), // Templated
					       .rdata_s1	(AXI4__TX__RDATA[`axi_2x1_cpu_mem_AXI_DW-1:0]), // Templated
					       .rlast_s1	(AXI4__TX__RLAST), // Templated
					       .rresp_s1	(AXI4__TX__RRESP[`axi_2x1_cpu_mem_AXI_RRW-1:0]), // Templated
					       .slv_priority_s1	(1'b0));		 // Templated

`include "./axi_2x1_cpu_mem/axi_2x1_cpu_mem_DW_axi-undef.v"
endmodule
// Local Variables:
// verilog-library-directories:(".")
// verilog-auto-inst-param-value:t
// verilog-library-files:("./axi_2x1_cpu_mem/axi_2x1_cpu_mem_DW_axi.v")
// End:
// vim:tabstop=4:shiftwidth=4
