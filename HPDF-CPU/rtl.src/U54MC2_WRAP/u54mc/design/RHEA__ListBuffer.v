//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__ListBuffer(
  input          rf_reset,
  input          clock,
  input          reset,
  output         io_push_ready,
  input          io_push_valid,
  input  [4:0]   io_push_bits_index,
  input  [127:0] io_push_bits_data_data,
  input  [15:0]  io_push_bits_data_mask,
  input          io_push_bits_data_corrupt,
  output [23:0]  io_valid,
  input          io_pop_valid,
  input  [4:0]   io_pop_bits,
  output [127:0] io_data_data,
  output [15:0]  io_data_mask,
  output         io_data_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [31:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [31:0] _RAND_59;
  reg [31:0] _RAND_60;
  reg [31:0] _RAND_61;
  reg [31:0] _RAND_62;
  reg [31:0] _RAND_63;
  reg [31:0] _RAND_64;
  reg [31:0] _RAND_65;
  reg [31:0] _RAND_66;
  reg [31:0] _RAND_67;
  reg [31:0] _RAND_68;
  reg [31:0] _RAND_69;
  reg [31:0] _RAND_70;
  reg [31:0] _RAND_71;
  reg [31:0] _RAND_72;
  reg [31:0] _RAND_73;
  reg [127:0] _RAND_74;
  reg [31:0] _RAND_75;
  reg [31:0] _RAND_76;
  reg [127:0] _RAND_77;
  reg [31:0] _RAND_78;
  reg [31:0] _RAND_79;
  reg [127:0] _RAND_80;
  reg [31:0] _RAND_81;
  reg [31:0] _RAND_82;
  reg [127:0] _RAND_83;
  reg [31:0] _RAND_84;
  reg [31:0] _RAND_85;
  reg [127:0] _RAND_86;
  reg [31:0] _RAND_87;
  reg [31:0] _RAND_88;
  reg [127:0] _RAND_89;
  reg [31:0] _RAND_90;
  reg [31:0] _RAND_91;
  reg [127:0] _RAND_92;
  reg [31:0] _RAND_93;
  reg [31:0] _RAND_94;
  reg [127:0] _RAND_95;
  reg [31:0] _RAND_96;
  reg [31:0] _RAND_97;
  reg [127:0] _RAND_98;
  reg [31:0] _RAND_99;
  reg [31:0] _RAND_100;
  reg [127:0] _RAND_101;
  reg [31:0] _RAND_102;
  reg [31:0] _RAND_103;
  reg [127:0] _RAND_104;
  reg [31:0] _RAND_105;
  reg [31:0] _RAND_106;
  reg [127:0] _RAND_107;
  reg [31:0] _RAND_108;
  reg [31:0] _RAND_109;
  reg [127:0] _RAND_110;
  reg [31:0] _RAND_111;
  reg [31:0] _RAND_112;
  reg [127:0] _RAND_113;
  reg [31:0] _RAND_114;
  reg [31:0] _RAND_115;
  reg [127:0] _RAND_116;
  reg [31:0] _RAND_117;
  reg [31:0] _RAND_118;
  reg [127:0] _RAND_119;
  reg [31:0] _RAND_120;
  reg [31:0] _RAND_121;
  reg [127:0] _RAND_122;
  reg [31:0] _RAND_123;
  reg [31:0] _RAND_124;
  reg [127:0] _RAND_125;
  reg [31:0] _RAND_126;
  reg [31:0] _RAND_127;
  reg [127:0] _RAND_128;
  reg [31:0] _RAND_129;
  reg [31:0] _RAND_130;
  reg [127:0] _RAND_131;
  reg [31:0] _RAND_132;
  reg [31:0] _RAND_133;
  reg [127:0] _RAND_134;
  reg [31:0] _RAND_135;
  reg [31:0] _RAND_136;
  reg [127:0] _RAND_137;
  reg [31:0] _RAND_138;
  reg [31:0] _RAND_139;
  reg [127:0] _RAND_140;
  reg [31:0] _RAND_141;
  reg [31:0] _RAND_142;
  reg [127:0] _RAND_143;
  reg [31:0] _RAND_144;
  reg [31:0] _RAND_145;
`endif // RANDOMIZE_REG_INIT
  reg [23:0] valid; // @[ListBuffer.scala 49:22]
  reg [4:0] head_0; // @[ListBuffer.scala 50:18]
  reg [4:0] head_1; // @[ListBuffer.scala 50:18]
  reg [4:0] head_2; // @[ListBuffer.scala 50:18]
  reg [4:0] head_3; // @[ListBuffer.scala 50:18]
  reg [4:0] head_4; // @[ListBuffer.scala 50:18]
  reg [4:0] head_5; // @[ListBuffer.scala 50:18]
  reg [4:0] head_6; // @[ListBuffer.scala 50:18]
  reg [4:0] head_7; // @[ListBuffer.scala 50:18]
  reg [4:0] head_8; // @[ListBuffer.scala 50:18]
  reg [4:0] head_9; // @[ListBuffer.scala 50:18]
  reg [4:0] head_10; // @[ListBuffer.scala 50:18]
  reg [4:0] head_11; // @[ListBuffer.scala 50:18]
  reg [4:0] head_12; // @[ListBuffer.scala 50:18]
  reg [4:0] head_13; // @[ListBuffer.scala 50:18]
  reg [4:0] head_14; // @[ListBuffer.scala 50:18]
  reg [4:0] head_15; // @[ListBuffer.scala 50:18]
  reg [4:0] head_16; // @[ListBuffer.scala 50:18]
  reg [4:0] head_17; // @[ListBuffer.scala 50:18]
  reg [4:0] head_18; // @[ListBuffer.scala 50:18]
  reg [4:0] head_19; // @[ListBuffer.scala 50:18]
  reg [4:0] head_20; // @[ListBuffer.scala 50:18]
  reg [4:0] head_21; // @[ListBuffer.scala 50:18]
  reg [4:0] head_22; // @[ListBuffer.scala 50:18]
  reg [4:0] head_23; // @[ListBuffer.scala 50:18]
  wire [4:0] _GEN_1 = 5'h1 == io_pop_bits ? head_1 : head_0; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_2 = 5'h2 == io_pop_bits ? head_2 : _GEN_1; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_3 = 5'h3 == io_pop_bits ? head_3 : _GEN_2; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_4 = 5'h4 == io_pop_bits ? head_4 : _GEN_3; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_5 = 5'h5 == io_pop_bits ? head_5 : _GEN_4; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_6 = 5'h6 == io_pop_bits ? head_6 : _GEN_5; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_7 = 5'h7 == io_pop_bits ? head_7 : _GEN_6; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_8 = 5'h8 == io_pop_bits ? head_8 : _GEN_7; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_9 = 5'h9 == io_pop_bits ? head_9 : _GEN_8; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_10 = 5'ha == io_pop_bits ? head_10 : _GEN_9; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_11 = 5'hb == io_pop_bits ? head_11 : _GEN_10; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_12 = 5'hc == io_pop_bits ? head_12 : _GEN_11; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_13 = 5'hd == io_pop_bits ? head_13 : _GEN_12; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_14 = 5'he == io_pop_bits ? head_14 : _GEN_13; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_15 = 5'hf == io_pop_bits ? head_15 : _GEN_14; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_16 = 5'h10 == io_pop_bits ? head_16 : _GEN_15; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_17 = 5'h11 == io_pop_bits ? head_17 : _GEN_16; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_18 = 5'h12 == io_pop_bits ? head_18 : _GEN_17; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_19 = 5'h13 == io_pop_bits ? head_19 : _GEN_18; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_20 = 5'h14 == io_pop_bits ? head_20 : _GEN_19; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_21 = 5'h15 == io_pop_bits ? head_21 : _GEN_20; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_22 = 5'h16 == io_pop_bits ? head_22 : _GEN_21; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] headIntf_pop_head_data = 5'h17 == io_pop_bits ? head_23 : _GEN_22; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire  tailIntf_MPORT_3_en = io_push_ready & io_push_valid; // @[Decoupled.scala 40:37]
  wire [23:0] _push_valid_T = valid >> io_push_bits_index; // @[ListBuffer.scala 65:25]
  wire  push_valid = _push_valid_T[0]; // @[ListBuffer.scala 65:25]
  reg [23:0] used; // @[ListBuffer.scala 52:22]
  wire [23:0] _freeOH_T = ~used; // @[ListBuffer.scala 56:25]
  wire [24:0] _freeOH_T_1 = {_freeOH_T, 1'h0}; // @[package.scala 244:48]
  wire [23:0] _freeOH_T_3 = _freeOH_T | _freeOH_T_1[23:0]; // @[package.scala 244:43]
  wire [25:0] _freeOH_T_4 = {_freeOH_T_3, 2'h0}; // @[package.scala 244:48]
  wire [23:0] _freeOH_T_6 = _freeOH_T_3 | _freeOH_T_4[23:0]; // @[package.scala 244:43]
  wire [27:0] _freeOH_T_7 = {_freeOH_T_6, 4'h0}; // @[package.scala 244:48]
  wire [23:0] _freeOH_T_9 = _freeOH_T_6 | _freeOH_T_7[23:0]; // @[package.scala 244:43]
  wire [31:0] _freeOH_T_10 = {_freeOH_T_9, 8'h0}; // @[package.scala 244:48]
  wire [23:0] _freeOH_T_12 = _freeOH_T_9 | _freeOH_T_10[23:0]; // @[package.scala 244:43]
  wire [39:0] _freeOH_T_13 = {_freeOH_T_12, 16'h0}; // @[package.scala 244:48]
  wire [23:0] _freeOH_T_15 = _freeOH_T_12 | _freeOH_T_13[23:0]; // @[package.scala 244:43]
  wire [24:0] _freeOH_T_17 = {_freeOH_T_15, 1'h0}; // @[ListBuffer.scala 56:32]
  wire [24:0] _freeOH_T_18 = ~_freeOH_T_17; // @[ListBuffer.scala 56:16]
  wire [24:0] _GEN_673 = {{1'd0}, _freeOH_T}; // @[ListBuffer.scala 56:38]
  wire [24:0] freeOH = _freeOH_T_18 & _GEN_673; // @[ListBuffer.scala 56:38]
  wire [8:0] freeIdx_hi = freeOH[24:16]; // @[OneHot.scala 30:18]
  wire  freeIdx_hi_1 = |freeIdx_hi; // @[OneHot.scala 32:14]
  wire [15:0] freeIdx_lo = freeOH[15:0]; // @[OneHot.scala 31:18]
  wire [15:0] _GEN_678 = {{7'd0}, freeIdx_hi}; // @[OneHot.scala 32:28]
  wire [15:0] _freeIdx_T = _GEN_678 | freeIdx_lo; // @[OneHot.scala 32:28]
  wire [7:0] freeIdx_hi_2 = _freeIdx_T[15:8]; // @[OneHot.scala 30:18]
  wire  freeIdx_hi_3 = |freeIdx_hi_2; // @[OneHot.scala 32:14]
  wire [7:0] freeIdx_lo_1 = _freeIdx_T[7:0]; // @[OneHot.scala 31:18]
  wire [7:0] _freeIdx_T_1 = freeIdx_hi_2 | freeIdx_lo_1; // @[OneHot.scala 32:28]
  wire [3:0] freeIdx_hi_4 = _freeIdx_T_1[7:4]; // @[OneHot.scala 30:18]
  wire  freeIdx_hi_5 = |freeIdx_hi_4; // @[OneHot.scala 32:14]
  wire [3:0] freeIdx_lo_2 = _freeIdx_T_1[3:0]; // @[OneHot.scala 31:18]
  wire [3:0] _freeIdx_T_2 = freeIdx_hi_4 | freeIdx_lo_2; // @[OneHot.scala 32:28]
  wire [1:0] freeIdx_hi_6 = _freeIdx_T_2[3:2]; // @[OneHot.scala 30:18]
  wire  freeIdx_hi_7 = |freeIdx_hi_6; // @[OneHot.scala 32:14]
  wire [1:0] freeIdx_lo_3 = _freeIdx_T_2[1:0]; // @[OneHot.scala 31:18]
  wire [1:0] _freeIdx_T_3 = freeIdx_hi_6 | freeIdx_lo_3; // @[OneHot.scala 32:28]
  wire  freeIdx_lo_4 = _freeIdx_T_3[1]; // @[CircuitMath.scala 30:8]
  wire [4:0] freeIdx = {freeIdx_hi_1,freeIdx_hi_3,freeIdx_hi_5,freeIdx_hi_7,freeIdx_lo_4}; // @[Cat.scala 30:58]
  wire [4:0] _GEN_24 = 5'h0 == io_push_bits_index ? freeIdx : head_0; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_25 = 5'h1 == io_push_bits_index ? freeIdx : head_1; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_26 = 5'h2 == io_push_bits_index ? freeIdx : head_2; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_27 = 5'h3 == io_push_bits_index ? freeIdx : head_3; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_28 = 5'h4 == io_push_bits_index ? freeIdx : head_4; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_29 = 5'h5 == io_push_bits_index ? freeIdx : head_5; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_30 = 5'h6 == io_push_bits_index ? freeIdx : head_6; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_31 = 5'h7 == io_push_bits_index ? freeIdx : head_7; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_32 = 5'h8 == io_push_bits_index ? freeIdx : head_8; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_33 = 5'h9 == io_push_bits_index ? freeIdx : head_9; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_34 = 5'ha == io_push_bits_index ? freeIdx : head_10; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_35 = 5'hb == io_push_bits_index ? freeIdx : head_11; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_36 = 5'hc == io_push_bits_index ? freeIdx : head_12; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_37 = 5'hd == io_push_bits_index ? freeIdx : head_13; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_38 = 5'he == io_push_bits_index ? freeIdx : head_14; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_39 = 5'hf == io_push_bits_index ? freeIdx : head_15; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_40 = 5'h10 == io_push_bits_index ? freeIdx : head_16; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_41 = 5'h11 == io_push_bits_index ? freeIdx : head_17; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_42 = 5'h12 == io_push_bits_index ? freeIdx : head_18; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_43 = 5'h13 == io_push_bits_index ? freeIdx : head_19; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_44 = 5'h14 == io_push_bits_index ? freeIdx : head_20; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_45 = 5'h15 == io_push_bits_index ? freeIdx : head_21; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_46 = 5'h16 == io_push_bits_index ? freeIdx : head_22; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_47 = 5'h17 == io_push_bits_index ? freeIdx : head_23; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire  _GEN_679 = push_valid ? 1'h0 : 1'h1; // @[ListBuffer.scala 72:23 ListBuffer.scala 50:18]
  wire  headIntf_MPORT_2_en = tailIntf_MPORT_3_en & _GEN_679; // @[ListBuffer.scala 68:25 ListBuffer.scala 50:18]
  wire [4:0] _GEN_72 = headIntf_MPORT_2_en ? _GEN_24 : head_0; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_73 = headIntf_MPORT_2_en ? _GEN_25 : head_1; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_74 = headIntf_MPORT_2_en ? _GEN_26 : head_2; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_75 = headIntf_MPORT_2_en ? _GEN_27 : head_3; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_76 = headIntf_MPORT_2_en ? _GEN_28 : head_4; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_77 = headIntf_MPORT_2_en ? _GEN_29 : head_5; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_78 = headIntf_MPORT_2_en ? _GEN_30 : head_6; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_79 = headIntf_MPORT_2_en ? _GEN_31 : head_7; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_80 = headIntf_MPORT_2_en ? _GEN_32 : head_8; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_81 = headIntf_MPORT_2_en ? _GEN_33 : head_9; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_82 = headIntf_MPORT_2_en ? _GEN_34 : head_10; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_83 = headIntf_MPORT_2_en ? _GEN_35 : head_11; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_84 = headIntf_MPORT_2_en ? _GEN_36 : head_12; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_85 = headIntf_MPORT_2_en ? _GEN_37 : head_13; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_86 = headIntf_MPORT_2_en ? _GEN_38 : head_14; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_87 = headIntf_MPORT_2_en ? _GEN_39 : head_15; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_88 = headIntf_MPORT_2_en ? _GEN_40 : head_16; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_89 = headIntf_MPORT_2_en ? _GEN_41 : head_17; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_90 = headIntf_MPORT_2_en ? _GEN_42 : head_18; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_91 = headIntf_MPORT_2_en ? _GEN_43 : head_19; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_92 = headIntf_MPORT_2_en ? _GEN_44 : head_20; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_93 = headIntf_MPORT_2_en ? _GEN_45 : head_21; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_94 = headIntf_MPORT_2_en ? _GEN_46 : head_22; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [4:0] _GEN_95 = headIntf_MPORT_2_en ? _GEN_47 : head_23; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire  _T_10 = tailIntf_MPORT_3_en & push_valid; // @[ListBuffer.scala 95:48]
  reg [4:0] tail_23; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_22; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_21; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_20; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_19; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_18; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_17; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_16; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_15; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_14; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_13; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_12; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_11; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_10; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_9; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_8; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_7; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_6; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_5; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_4; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_3; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_2; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_1; // @[ListBuffer.scala 51:18]
  reg [4:0] tail_0; // @[ListBuffer.scala 51:18]
  wire [4:0] _GEN_169 = 5'h1 == io_push_bits_index ? tail_1 : tail_0; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_170 = 5'h2 == io_push_bits_index ? tail_2 : _GEN_169; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_171 = 5'h3 == io_push_bits_index ? tail_3 : _GEN_170; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_172 = 5'h4 == io_push_bits_index ? tail_4 : _GEN_171; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_173 = 5'h5 == io_push_bits_index ? tail_5 : _GEN_172; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_174 = 5'h6 == io_push_bits_index ? tail_6 : _GEN_173; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_175 = 5'h7 == io_push_bits_index ? tail_7 : _GEN_174; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_176 = 5'h8 == io_push_bits_index ? tail_8 : _GEN_175; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_177 = 5'h9 == io_push_bits_index ? tail_9 : _GEN_176; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_178 = 5'ha == io_push_bits_index ? tail_10 : _GEN_177; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_179 = 5'hb == io_push_bits_index ? tail_11 : _GEN_178; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_180 = 5'hc == io_push_bits_index ? tail_12 : _GEN_179; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_181 = 5'hd == io_push_bits_index ? tail_13 : _GEN_180; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_182 = 5'he == io_push_bits_index ? tail_14 : _GEN_181; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_183 = 5'hf == io_push_bits_index ? tail_15 : _GEN_182; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_184 = 5'h10 == io_push_bits_index ? tail_16 : _GEN_183; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_185 = 5'h11 == io_push_bits_index ? tail_17 : _GEN_184; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_186 = 5'h12 == io_push_bits_index ? tail_18 : _GEN_185; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_187 = 5'h13 == io_push_bits_index ? tail_19 : _GEN_186; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_188 = 5'h14 == io_push_bits_index ? tail_20 : _GEN_187; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_189 = 5'h15 == io_push_bits_index ? tail_21 : _GEN_188; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_190 = 5'h16 == io_push_bits_index ? tail_22 : _GEN_189; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] tailIntf_push_tail_data = 5'h17 == io_push_bits_index ? tail_23 : _GEN_190; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  reg [4:0] next_23; // @[ListBuffer.scala 53:18]
  reg [4:0] next_22; // @[ListBuffer.scala 53:18]
  reg [4:0] next_21; // @[ListBuffer.scala 53:18]
  reg [4:0] next_20; // @[ListBuffer.scala 53:18]
  reg [4:0] next_19; // @[ListBuffer.scala 53:18]
  reg [4:0] next_18; // @[ListBuffer.scala 53:18]
  reg [4:0] next_17; // @[ListBuffer.scala 53:18]
  reg [4:0] next_16; // @[ListBuffer.scala 53:18]
  reg [4:0] next_15; // @[ListBuffer.scala 53:18]
  reg [4:0] next_14; // @[ListBuffer.scala 53:18]
  reg [4:0] next_13; // @[ListBuffer.scala 53:18]
  reg [4:0] next_12; // @[ListBuffer.scala 53:18]
  reg [4:0] next_11; // @[ListBuffer.scala 53:18]
  reg [4:0] next_10; // @[ListBuffer.scala 53:18]
  reg [4:0] next_9; // @[ListBuffer.scala 53:18]
  reg [4:0] next_8; // @[ListBuffer.scala 53:18]
  reg [4:0] next_7; // @[ListBuffer.scala 53:18]
  reg [4:0] next_6; // @[ListBuffer.scala 53:18]
  reg [4:0] next_5; // @[ListBuffer.scala 53:18]
  reg [4:0] next_4; // @[ListBuffer.scala 53:18]
  reg [4:0] next_3; // @[ListBuffer.scala 53:18]
  reg [4:0] next_2; // @[ListBuffer.scala 53:18]
  reg [4:0] next_1; // @[ListBuffer.scala 53:18]
  reg [4:0] next_0; // @[ListBuffer.scala 53:18]
  wire [4:0] _GEN_289 = 5'h1 == headIntf_pop_head_data ? next_1 : next_0; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_290 = 5'h2 == headIntf_pop_head_data ? next_2 : _GEN_289; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_291 = 5'h3 == headIntf_pop_head_data ? next_3 : _GEN_290; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_292 = 5'h4 == headIntf_pop_head_data ? next_4 : _GEN_291; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_293 = 5'h5 == headIntf_pop_head_data ? next_5 : _GEN_292; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_294 = 5'h6 == headIntf_pop_head_data ? next_6 : _GEN_293; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_295 = 5'h7 == headIntf_pop_head_data ? next_7 : _GEN_294; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_296 = 5'h8 == headIntf_pop_head_data ? next_8 : _GEN_295; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_297 = 5'h9 == headIntf_pop_head_data ? next_9 : _GEN_296; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_298 = 5'ha == headIntf_pop_head_data ? next_10 : _GEN_297; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_299 = 5'hb == headIntf_pop_head_data ? next_11 : _GEN_298; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_300 = 5'hc == headIntf_pop_head_data ? next_12 : _GEN_299; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_301 = 5'hd == headIntf_pop_head_data ? next_13 : _GEN_300; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_302 = 5'he == headIntf_pop_head_data ? next_14 : _GEN_301; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_303 = 5'hf == headIntf_pop_head_data ? next_15 : _GEN_302; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_304 = 5'h10 == headIntf_pop_head_data ? next_16 : _GEN_303; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_305 = 5'h11 == headIntf_pop_head_data ? next_17 : _GEN_304; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_306 = 5'h12 == headIntf_pop_head_data ? next_18 : _GEN_305; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_307 = 5'h13 == headIntf_pop_head_data ? next_19 : _GEN_306; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_308 = 5'h14 == headIntf_pop_head_data ? next_20 : _GEN_307; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_309 = 5'h15 == headIntf_pop_head_data ? next_21 : _GEN_308; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_310 = 5'h16 == headIntf_pop_head_data ? next_22 : _GEN_309; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [4:0] _GEN_193 = 5'h1 == io_pop_bits ? tail_1 : tail_0; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_194 = 5'h2 == io_pop_bits ? tail_2 : _GEN_193; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_195 = 5'h3 == io_pop_bits ? tail_3 : _GEN_194; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_196 = 5'h4 == io_pop_bits ? tail_4 : _GEN_195; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_197 = 5'h5 == io_pop_bits ? tail_5 : _GEN_196; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_198 = 5'h6 == io_pop_bits ? tail_6 : _GEN_197; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_199 = 5'h7 == io_pop_bits ? tail_7 : _GEN_198; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_200 = 5'h8 == io_pop_bits ? tail_8 : _GEN_199; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_201 = 5'h9 == io_pop_bits ? tail_9 : _GEN_200; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_202 = 5'ha == io_pop_bits ? tail_10 : _GEN_201; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_203 = 5'hb == io_pop_bits ? tail_11 : _GEN_202; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_204 = 5'hc == io_pop_bits ? tail_12 : _GEN_203; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_205 = 5'hd == io_pop_bits ? tail_13 : _GEN_204; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_206 = 5'he == io_pop_bits ? tail_14 : _GEN_205; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_207 = 5'hf == io_pop_bits ? tail_15 : _GEN_206; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_208 = 5'h10 == io_pop_bits ? tail_16 : _GEN_207; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_209 = 5'h11 == io_pop_bits ? tail_17 : _GEN_208; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_210 = 5'h12 == io_pop_bits ? tail_18 : _GEN_209; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_211 = 5'h13 == io_pop_bits ? tail_19 : _GEN_210; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_212 = 5'h14 == io_pop_bits ? tail_20 : _GEN_211; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_213 = 5'h15 == io_pop_bits ? tail_21 : _GEN_212; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] _GEN_214 = 5'h16 == io_pop_bits ? tail_22 : _GEN_213; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  wire [4:0] tailIntf_MPORT_4_data = 5'h17 == io_pop_bits ? tail_23 : _GEN_214; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  reg [127:0] data_0_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_0_mask; // @[ListBuffer.scala 54:18]
  reg  data_0_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_1_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_1_mask; // @[ListBuffer.scala 54:18]
  reg  data_1_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_2_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_2_mask; // @[ListBuffer.scala 54:18]
  reg  data_2_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_3_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_3_mask; // @[ListBuffer.scala 54:18]
  reg  data_3_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_4_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_4_mask; // @[ListBuffer.scala 54:18]
  reg  data_4_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_5_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_5_mask; // @[ListBuffer.scala 54:18]
  reg  data_5_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_6_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_6_mask; // @[ListBuffer.scala 54:18]
  reg  data_6_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_7_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_7_mask; // @[ListBuffer.scala 54:18]
  reg  data_7_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_8_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_8_mask; // @[ListBuffer.scala 54:18]
  reg  data_8_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_9_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_9_mask; // @[ListBuffer.scala 54:18]
  reg  data_9_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_10_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_10_mask; // @[ListBuffer.scala 54:18]
  reg  data_10_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_11_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_11_mask; // @[ListBuffer.scala 54:18]
  reg  data_11_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_12_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_12_mask; // @[ListBuffer.scala 54:18]
  reg  data_12_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_13_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_13_mask; // @[ListBuffer.scala 54:18]
  reg  data_13_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_14_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_14_mask; // @[ListBuffer.scala 54:18]
  reg  data_14_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_15_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_15_mask; // @[ListBuffer.scala 54:18]
  reg  data_15_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_16_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_16_mask; // @[ListBuffer.scala 54:18]
  reg  data_16_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_17_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_17_mask; // @[ListBuffer.scala 54:18]
  reg  data_17_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_18_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_18_mask; // @[ListBuffer.scala 54:18]
  reg  data_18_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_19_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_19_mask; // @[ListBuffer.scala 54:18]
  reg  data_19_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_20_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_20_mask; // @[ListBuffer.scala 54:18]
  reg  data_20_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_21_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_21_mask; // @[ListBuffer.scala 54:18]
  reg  data_21_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_22_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_22_mask; // @[ListBuffer.scala 54:18]
  reg  data_22_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_23_data; // @[ListBuffer.scala 54:18]
  reg [15:0] data_23_mask; // @[ListBuffer.scala 54:18]
  reg  data_23_corrupt; // @[ListBuffer.scala 54:18]
  wire [127:0] _GEN_387 = 5'h1 == headIntf_pop_head_data ? data_1_data : data_0_data; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_388 = 5'h1 == headIntf_pop_head_data ? data_1_mask : data_0_mask; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_389 = 5'h1 == headIntf_pop_head_data ? data_1_corrupt : data_0_corrupt; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_390 = 5'h2 == headIntf_pop_head_data ? data_2_data : _GEN_387; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_391 = 5'h2 == headIntf_pop_head_data ? data_2_mask : _GEN_388; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_392 = 5'h2 == headIntf_pop_head_data ? data_2_corrupt : _GEN_389; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_393 = 5'h3 == headIntf_pop_head_data ? data_3_data : _GEN_390; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_394 = 5'h3 == headIntf_pop_head_data ? data_3_mask : _GEN_391; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_395 = 5'h3 == headIntf_pop_head_data ? data_3_corrupt : _GEN_392; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_396 = 5'h4 == headIntf_pop_head_data ? data_4_data : _GEN_393; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_397 = 5'h4 == headIntf_pop_head_data ? data_4_mask : _GEN_394; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_398 = 5'h4 == headIntf_pop_head_data ? data_4_corrupt : _GEN_395; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_399 = 5'h5 == headIntf_pop_head_data ? data_5_data : _GEN_396; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_400 = 5'h5 == headIntf_pop_head_data ? data_5_mask : _GEN_397; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_401 = 5'h5 == headIntf_pop_head_data ? data_5_corrupt : _GEN_398; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_402 = 5'h6 == headIntf_pop_head_data ? data_6_data : _GEN_399; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_403 = 5'h6 == headIntf_pop_head_data ? data_6_mask : _GEN_400; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_404 = 5'h6 == headIntf_pop_head_data ? data_6_corrupt : _GEN_401; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_405 = 5'h7 == headIntf_pop_head_data ? data_7_data : _GEN_402; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_406 = 5'h7 == headIntf_pop_head_data ? data_7_mask : _GEN_403; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_407 = 5'h7 == headIntf_pop_head_data ? data_7_corrupt : _GEN_404; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_408 = 5'h8 == headIntf_pop_head_data ? data_8_data : _GEN_405; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_409 = 5'h8 == headIntf_pop_head_data ? data_8_mask : _GEN_406; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_410 = 5'h8 == headIntf_pop_head_data ? data_8_corrupt : _GEN_407; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_411 = 5'h9 == headIntf_pop_head_data ? data_9_data : _GEN_408; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_412 = 5'h9 == headIntf_pop_head_data ? data_9_mask : _GEN_409; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_413 = 5'h9 == headIntf_pop_head_data ? data_9_corrupt : _GEN_410; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_414 = 5'ha == headIntf_pop_head_data ? data_10_data : _GEN_411; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_415 = 5'ha == headIntf_pop_head_data ? data_10_mask : _GEN_412; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_416 = 5'ha == headIntf_pop_head_data ? data_10_corrupt : _GEN_413; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_417 = 5'hb == headIntf_pop_head_data ? data_11_data : _GEN_414; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_418 = 5'hb == headIntf_pop_head_data ? data_11_mask : _GEN_415; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_419 = 5'hb == headIntf_pop_head_data ? data_11_corrupt : _GEN_416; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_420 = 5'hc == headIntf_pop_head_data ? data_12_data : _GEN_417; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_421 = 5'hc == headIntf_pop_head_data ? data_12_mask : _GEN_418; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_422 = 5'hc == headIntf_pop_head_data ? data_12_corrupt : _GEN_419; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_423 = 5'hd == headIntf_pop_head_data ? data_13_data : _GEN_420; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_424 = 5'hd == headIntf_pop_head_data ? data_13_mask : _GEN_421; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_425 = 5'hd == headIntf_pop_head_data ? data_13_corrupt : _GEN_422; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_426 = 5'he == headIntf_pop_head_data ? data_14_data : _GEN_423; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_427 = 5'he == headIntf_pop_head_data ? data_14_mask : _GEN_424; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_428 = 5'he == headIntf_pop_head_data ? data_14_corrupt : _GEN_425; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_429 = 5'hf == headIntf_pop_head_data ? data_15_data : _GEN_426; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_430 = 5'hf == headIntf_pop_head_data ? data_15_mask : _GEN_427; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_431 = 5'hf == headIntf_pop_head_data ? data_15_corrupt : _GEN_428; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_432 = 5'h10 == headIntf_pop_head_data ? data_16_data : _GEN_429; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_433 = 5'h10 == headIntf_pop_head_data ? data_16_mask : _GEN_430; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_434 = 5'h10 == headIntf_pop_head_data ? data_16_corrupt : _GEN_431; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_435 = 5'h11 == headIntf_pop_head_data ? data_17_data : _GEN_432; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_436 = 5'h11 == headIntf_pop_head_data ? data_17_mask : _GEN_433; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_437 = 5'h11 == headIntf_pop_head_data ? data_17_corrupt : _GEN_434; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_438 = 5'h12 == headIntf_pop_head_data ? data_18_data : _GEN_435; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_439 = 5'h12 == headIntf_pop_head_data ? data_18_mask : _GEN_436; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_440 = 5'h12 == headIntf_pop_head_data ? data_18_corrupt : _GEN_437; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_441 = 5'h13 == headIntf_pop_head_data ? data_19_data : _GEN_438; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_442 = 5'h13 == headIntf_pop_head_data ? data_19_mask : _GEN_439; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_443 = 5'h13 == headIntf_pop_head_data ? data_19_corrupt : _GEN_440; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_444 = 5'h14 == headIntf_pop_head_data ? data_20_data : _GEN_441; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_445 = 5'h14 == headIntf_pop_head_data ? data_20_mask : _GEN_442; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_446 = 5'h14 == headIntf_pop_head_data ? data_20_corrupt : _GEN_443; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_447 = 5'h15 == headIntf_pop_head_data ? data_21_data : _GEN_444; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_448 = 5'h15 == headIntf_pop_head_data ? data_21_mask : _GEN_445; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_449 = 5'h15 == headIntf_pop_head_data ? data_21_corrupt : _GEN_446; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_450 = 5'h16 == headIntf_pop_head_data ? data_22_data : _GEN_447; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [15:0] _GEN_451 = 5'h16 == headIntf_pop_head_data ? data_22_mask : _GEN_448; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_452 = 5'h16 == headIntf_pop_head_data ? data_22_corrupt : _GEN_449; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [31:0] _valid_set_T = 32'h1 << io_push_bits_index; // @[OneHot.scala 65:12]
  wire [23:0] valid_set = tailIntf_MPORT_3_en ? _valid_set_T[23:0] : 24'h0; // @[ListBuffer.scala 68:25 ListBuffer.scala 69:15]
  wire [24:0] _GEN_683 = tailIntf_MPORT_3_en ? freeOH : 25'h0; // @[ListBuffer.scala 68:25 ListBuffer.scala 70:14]
  wire [31:0] _used_clr_T = 32'h1 << headIntf_pop_head_data; // @[OneHot.scala 65:12]
  wire [31:0] _valid_clr_T = 32'h1 << io_pop_bits; // @[OneHot.scala 65:12]
  wire [23:0] _GEN_702 = headIntf_pop_head_data == tailIntf_MPORT_4_data ? _valid_clr_T[23:0] : 24'h0; // @[ListBuffer.scala 92:48 ListBuffer.scala 93:17]
  wire [23:0] used_clr = io_pop_valid ? _used_clr_T[23:0] : 24'h0; // @[ListBuffer.scala 90:24 ListBuffer.scala 91:14]
  wire [23:0] valid_clr = io_pop_valid ? _GEN_702 : 24'h0; // @[ListBuffer.scala 90:24]
  wire [23:0] _used_T = ~used_clr; // @[ListBuffer.scala 100:23]
  wire [23:0] _used_T_1 = used & _used_T; // @[ListBuffer.scala 100:21]
  wire [23:0] used_set = _GEN_683[23:0];
  wire [23:0] _valid_T = ~valid_clr; // @[ListBuffer.scala 101:23]
  wire [23:0] _valid_T_1 = valid & _valid_T; // @[ListBuffer.scala 101:21]
  assign io_push_ready = ~(&used); // @[ListBuffer.scala 67:20]
  assign io_valid = valid; // @[ListBuffer.scala 85:12]
  assign io_data_data = 5'h17 == headIntf_pop_head_data ? data_23_data : _GEN_450; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  assign io_data_mask = 5'h17 == headIntf_pop_head_data ? data_23_mask : _GEN_451; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  assign io_data_corrupt = 5'h17 == headIntf_pop_head_data ? data_23_corrupt : _GEN_452; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid <= 24'h0;
    end else begin
      valid <= _valid_T_1 | valid_set;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_0 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h0 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_0 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_0 <= next_23;
        end else begin
          head_0 <= _GEN_310;
        end
      end else begin
        head_0 <= _GEN_72;
      end
    end else begin
      head_0 <= _GEN_72;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_1 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h1 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_1 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_1 <= next_23;
        end else begin
          head_1 <= _GEN_310;
        end
      end else begin
        head_1 <= _GEN_73;
      end
    end else begin
      head_1 <= _GEN_73;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_2 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h2 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_2 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_2 <= next_23;
        end else begin
          head_2 <= _GEN_310;
        end
      end else begin
        head_2 <= _GEN_74;
      end
    end else begin
      head_2 <= _GEN_74;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_3 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h3 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_3 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_3 <= next_23;
        end else begin
          head_3 <= _GEN_310;
        end
      end else begin
        head_3 <= _GEN_75;
      end
    end else begin
      head_3 <= _GEN_75;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_4 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h4 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_4 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_4 <= next_23;
        end else begin
          head_4 <= _GEN_310;
        end
      end else begin
        head_4 <= _GEN_76;
      end
    end else begin
      head_4 <= _GEN_76;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_5 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h5 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_5 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_5 <= next_23;
        end else begin
          head_5 <= _GEN_310;
        end
      end else begin
        head_5 <= _GEN_77;
      end
    end else begin
      head_5 <= _GEN_77;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_6 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h6 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_6 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_6 <= next_23;
        end else begin
          head_6 <= _GEN_310;
        end
      end else begin
        head_6 <= _GEN_78;
      end
    end else begin
      head_6 <= _GEN_78;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_7 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h7 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_7 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_7 <= next_23;
        end else begin
          head_7 <= _GEN_310;
        end
      end else begin
        head_7 <= _GEN_79;
      end
    end else begin
      head_7 <= _GEN_79;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_8 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h8 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_8 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_8 <= next_23;
        end else begin
          head_8 <= _GEN_310;
        end
      end else begin
        head_8 <= _GEN_80;
      end
    end else begin
      head_8 <= _GEN_80;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_9 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h9 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_9 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_9 <= next_23;
        end else begin
          head_9 <= _GEN_310;
        end
      end else begin
        head_9 <= _GEN_81;
      end
    end else begin
      head_9 <= _GEN_81;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_10 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'ha == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_10 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_10 <= next_23;
        end else begin
          head_10 <= _GEN_310;
        end
      end else begin
        head_10 <= _GEN_82;
      end
    end else begin
      head_10 <= _GEN_82;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_11 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'hb == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_11 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_11 <= next_23;
        end else begin
          head_11 <= _GEN_310;
        end
      end else begin
        head_11 <= _GEN_83;
      end
    end else begin
      head_11 <= _GEN_83;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_12 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'hc == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_12 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_12 <= next_23;
        end else begin
          head_12 <= _GEN_310;
        end
      end else begin
        head_12 <= _GEN_84;
      end
    end else begin
      head_12 <= _GEN_84;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_13 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'hd == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_13 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_13 <= next_23;
        end else begin
          head_13 <= _GEN_310;
        end
      end else begin
        head_13 <= _GEN_85;
      end
    end else begin
      head_13 <= _GEN_85;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_14 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'he == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_14 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_14 <= next_23;
        end else begin
          head_14 <= _GEN_310;
        end
      end else begin
        head_14 <= _GEN_86;
      end
    end else begin
      head_14 <= _GEN_86;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_15 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'hf == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_15 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_15 <= next_23;
        end else begin
          head_15 <= _GEN_310;
        end
      end else begin
        head_15 <= _GEN_87;
      end
    end else begin
      head_15 <= _GEN_87;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_16 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h10 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_16 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_16 <= next_23;
        end else begin
          head_16 <= _GEN_310;
        end
      end else begin
        head_16 <= _GEN_88;
      end
    end else begin
      head_16 <= _GEN_88;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_17 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h11 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_17 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_17 <= next_23;
        end else begin
          head_17 <= _GEN_310;
        end
      end else begin
        head_17 <= _GEN_89;
      end
    end else begin
      head_17 <= _GEN_89;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_18 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h12 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_18 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_18 <= next_23;
        end else begin
          head_18 <= _GEN_310;
        end
      end else begin
        head_18 <= _GEN_90;
      end
    end else begin
      head_18 <= _GEN_90;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_19 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h13 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_19 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_19 <= next_23;
        end else begin
          head_19 <= _GEN_310;
        end
      end else begin
        head_19 <= _GEN_91;
      end
    end else begin
      head_19 <= _GEN_91;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_20 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h14 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_20 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_20 <= next_23;
        end else begin
          head_20 <= _GEN_310;
        end
      end else begin
        head_20 <= _GEN_92;
      end
    end else begin
      head_20 <= _GEN_92;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_21 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h15 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_21 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_21 <= next_23;
        end else begin
          head_21 <= _GEN_310;
        end
      end else begin
        head_21 <= _GEN_93;
      end
    end else begin
      head_21 <= _GEN_93;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_22 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h16 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_22 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_22 <= next_23;
        end else begin
          head_22 <= _GEN_310;
        end
      end else begin
        head_22 <= _GEN_94;
      end
    end else begin
      head_22 <= _GEN_94;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_23 <= 5'h0;
    end else if (io_pop_valid) begin
      if (5'h17 == io_pop_bits) begin
        if (tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data) begin
          head_23 <= freeIdx;
        end else if (5'h17 == headIntf_pop_head_data) begin
          head_23 <= next_23;
        end else begin
          head_23 <= _GEN_310;
        end
      end else begin
        head_23 <= _GEN_95;
      end
    end else begin
      head_23 <= _GEN_95;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      used <= 24'h0;
    end else begin
      used <= _used_T_1 | used_set;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_23 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h17 == io_push_bits_index) begin
        tail_23 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_22 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h16 == io_push_bits_index) begin
        tail_22 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_21 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h15 == io_push_bits_index) begin
        tail_21 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_20 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h14 == io_push_bits_index) begin
        tail_20 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_19 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h13 == io_push_bits_index) begin
        tail_19 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_18 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h12 == io_push_bits_index) begin
        tail_18 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_17 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h11 == io_push_bits_index) begin
        tail_17 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_16 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h10 == io_push_bits_index) begin
        tail_16 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_15 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hf == io_push_bits_index) begin
        tail_15 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_14 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'he == io_push_bits_index) begin
        tail_14 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_13 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hd == io_push_bits_index) begin
        tail_13 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_12 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hc == io_push_bits_index) begin
        tail_12 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_11 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hb == io_push_bits_index) begin
        tail_11 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_10 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'ha == io_push_bits_index) begin
        tail_10 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_9 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h9 == io_push_bits_index) begin
        tail_9 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_8 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h8 == io_push_bits_index) begin
        tail_8 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_7 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h7 == io_push_bits_index) begin
        tail_7 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_6 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h6 == io_push_bits_index) begin
        tail_6 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_5 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h5 == io_push_bits_index) begin
        tail_5 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_4 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h4 == io_push_bits_index) begin
        tail_4 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_3 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h3 == io_push_bits_index) begin
        tail_3 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_2 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h2 == io_push_bits_index) begin
        tail_2 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_1 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h1 == io_push_bits_index) begin
        tail_1 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_0 <= 5'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h0 == io_push_bits_index) begin
        tail_0 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_23 <= 5'h0;
    end else if (_T_10) begin
      if (5'h17 == tailIntf_push_tail_data) begin
        next_23 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_22 <= 5'h0;
    end else if (_T_10) begin
      if (5'h16 == tailIntf_push_tail_data) begin
        next_22 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_21 <= 5'h0;
    end else if (_T_10) begin
      if (5'h15 == tailIntf_push_tail_data) begin
        next_21 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_20 <= 5'h0;
    end else if (_T_10) begin
      if (5'h14 == tailIntf_push_tail_data) begin
        next_20 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_19 <= 5'h0;
    end else if (_T_10) begin
      if (5'h13 == tailIntf_push_tail_data) begin
        next_19 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_18 <= 5'h0;
    end else if (_T_10) begin
      if (5'h12 == tailIntf_push_tail_data) begin
        next_18 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_17 <= 5'h0;
    end else if (_T_10) begin
      if (5'h11 == tailIntf_push_tail_data) begin
        next_17 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_16 <= 5'h0;
    end else if (_T_10) begin
      if (5'h10 == tailIntf_push_tail_data) begin
        next_16 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_15 <= 5'h0;
    end else if (_T_10) begin
      if (5'hf == tailIntf_push_tail_data) begin
        next_15 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_14 <= 5'h0;
    end else if (_T_10) begin
      if (5'he == tailIntf_push_tail_data) begin
        next_14 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_13 <= 5'h0;
    end else if (_T_10) begin
      if (5'hd == tailIntf_push_tail_data) begin
        next_13 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_12 <= 5'h0;
    end else if (_T_10) begin
      if (5'hc == tailIntf_push_tail_data) begin
        next_12 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_11 <= 5'h0;
    end else if (_T_10) begin
      if (5'hb == tailIntf_push_tail_data) begin
        next_11 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_10 <= 5'h0;
    end else if (_T_10) begin
      if (5'ha == tailIntf_push_tail_data) begin
        next_10 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_9 <= 5'h0;
    end else if (_T_10) begin
      if (5'h9 == tailIntf_push_tail_data) begin
        next_9 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_8 <= 5'h0;
    end else if (_T_10) begin
      if (5'h8 == tailIntf_push_tail_data) begin
        next_8 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_7 <= 5'h0;
    end else if (_T_10) begin
      if (5'h7 == tailIntf_push_tail_data) begin
        next_7 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_6 <= 5'h0;
    end else if (_T_10) begin
      if (5'h6 == tailIntf_push_tail_data) begin
        next_6 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_5 <= 5'h0;
    end else if (_T_10) begin
      if (5'h5 == tailIntf_push_tail_data) begin
        next_5 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_4 <= 5'h0;
    end else if (_T_10) begin
      if (5'h4 == tailIntf_push_tail_data) begin
        next_4 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_3 <= 5'h0;
    end else if (_T_10) begin
      if (5'h3 == tailIntf_push_tail_data) begin
        next_3 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_2 <= 5'h0;
    end else if (_T_10) begin
      if (5'h2 == tailIntf_push_tail_data) begin
        next_2 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_1 <= 5'h0;
    end else if (_T_10) begin
      if (5'h1 == tailIntf_push_tail_data) begin
        next_1 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_0 <= 5'h0;
    end else if (_T_10) begin
      if (5'h0 == tailIntf_push_tail_data) begin
        next_0 <= freeIdx;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_0_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h0 == freeIdx) begin
        data_0_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_0_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h0 == freeIdx) begin
        data_0_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_0_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h0 == freeIdx) begin
        data_0_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_1_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h1 == freeIdx) begin
        data_1_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_1_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h1 == freeIdx) begin
        data_1_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_1_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h1 == freeIdx) begin
        data_1_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_2_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h2 == freeIdx) begin
        data_2_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_2_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h2 == freeIdx) begin
        data_2_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_2_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h2 == freeIdx) begin
        data_2_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_3_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h3 == freeIdx) begin
        data_3_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_3_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h3 == freeIdx) begin
        data_3_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_3_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h3 == freeIdx) begin
        data_3_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_4_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h4 == freeIdx) begin
        data_4_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_4_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h4 == freeIdx) begin
        data_4_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_4_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h4 == freeIdx) begin
        data_4_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_5_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h5 == freeIdx) begin
        data_5_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_5_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h5 == freeIdx) begin
        data_5_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_5_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h5 == freeIdx) begin
        data_5_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_6_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h6 == freeIdx) begin
        data_6_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_6_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h6 == freeIdx) begin
        data_6_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_6_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h6 == freeIdx) begin
        data_6_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_7_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h7 == freeIdx) begin
        data_7_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_7_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h7 == freeIdx) begin
        data_7_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_7_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h7 == freeIdx) begin
        data_7_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_8_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h8 == freeIdx) begin
        data_8_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_8_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h8 == freeIdx) begin
        data_8_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_8_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h8 == freeIdx) begin
        data_8_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_9_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h9 == freeIdx) begin
        data_9_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_9_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h9 == freeIdx) begin
        data_9_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_9_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h9 == freeIdx) begin
        data_9_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_10_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'ha == freeIdx) begin
        data_10_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_10_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'ha == freeIdx) begin
        data_10_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_10_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'ha == freeIdx) begin
        data_10_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_11_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hb == freeIdx) begin
        data_11_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_11_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hb == freeIdx) begin
        data_11_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_11_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hb == freeIdx) begin
        data_11_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_12_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hc == freeIdx) begin
        data_12_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_12_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hc == freeIdx) begin
        data_12_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_12_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hc == freeIdx) begin
        data_12_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_13_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hd == freeIdx) begin
        data_13_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_13_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hd == freeIdx) begin
        data_13_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_13_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hd == freeIdx) begin
        data_13_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_14_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'he == freeIdx) begin
        data_14_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_14_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'he == freeIdx) begin
        data_14_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_14_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'he == freeIdx) begin
        data_14_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_15_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hf == freeIdx) begin
        data_15_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_15_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hf == freeIdx) begin
        data_15_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_15_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'hf == freeIdx) begin
        data_15_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_16_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h10 == freeIdx) begin
        data_16_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_16_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h10 == freeIdx) begin
        data_16_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_16_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h10 == freeIdx) begin
        data_16_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_17_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h11 == freeIdx) begin
        data_17_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_17_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h11 == freeIdx) begin
        data_17_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_17_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h11 == freeIdx) begin
        data_17_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_18_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h12 == freeIdx) begin
        data_18_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_18_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h12 == freeIdx) begin
        data_18_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_18_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h12 == freeIdx) begin
        data_18_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_19_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h13 == freeIdx) begin
        data_19_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_19_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h13 == freeIdx) begin
        data_19_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_19_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h13 == freeIdx) begin
        data_19_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_20_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h14 == freeIdx) begin
        data_20_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_20_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h14 == freeIdx) begin
        data_20_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_20_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h14 == freeIdx) begin
        data_20_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_21_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h15 == freeIdx) begin
        data_21_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_21_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h15 == freeIdx) begin
        data_21_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_21_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h15 == freeIdx) begin
        data_21_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_22_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h16 == freeIdx) begin
        data_22_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_22_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h16 == freeIdx) begin
        data_22_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_22_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h16 == freeIdx) begin
        data_22_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_23_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h17 == freeIdx) begin
        data_23_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_23_mask <= 16'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h17 == freeIdx) begin
        data_23_mask <= io_push_bits_data_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_23_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (5'h17 == freeIdx) begin
        data_23_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  valid = _RAND_0[23:0];
  _RAND_1 = {1{`RANDOM}};
  head_0 = _RAND_1[4:0];
  _RAND_2 = {1{`RANDOM}};
  head_1 = _RAND_2[4:0];
  _RAND_3 = {1{`RANDOM}};
  head_2 = _RAND_3[4:0];
  _RAND_4 = {1{`RANDOM}};
  head_3 = _RAND_4[4:0];
  _RAND_5 = {1{`RANDOM}};
  head_4 = _RAND_5[4:0];
  _RAND_6 = {1{`RANDOM}};
  head_5 = _RAND_6[4:0];
  _RAND_7 = {1{`RANDOM}};
  head_6 = _RAND_7[4:0];
  _RAND_8 = {1{`RANDOM}};
  head_7 = _RAND_8[4:0];
  _RAND_9 = {1{`RANDOM}};
  head_8 = _RAND_9[4:0];
  _RAND_10 = {1{`RANDOM}};
  head_9 = _RAND_10[4:0];
  _RAND_11 = {1{`RANDOM}};
  head_10 = _RAND_11[4:0];
  _RAND_12 = {1{`RANDOM}};
  head_11 = _RAND_12[4:0];
  _RAND_13 = {1{`RANDOM}};
  head_12 = _RAND_13[4:0];
  _RAND_14 = {1{`RANDOM}};
  head_13 = _RAND_14[4:0];
  _RAND_15 = {1{`RANDOM}};
  head_14 = _RAND_15[4:0];
  _RAND_16 = {1{`RANDOM}};
  head_15 = _RAND_16[4:0];
  _RAND_17 = {1{`RANDOM}};
  head_16 = _RAND_17[4:0];
  _RAND_18 = {1{`RANDOM}};
  head_17 = _RAND_18[4:0];
  _RAND_19 = {1{`RANDOM}};
  head_18 = _RAND_19[4:0];
  _RAND_20 = {1{`RANDOM}};
  head_19 = _RAND_20[4:0];
  _RAND_21 = {1{`RANDOM}};
  head_20 = _RAND_21[4:0];
  _RAND_22 = {1{`RANDOM}};
  head_21 = _RAND_22[4:0];
  _RAND_23 = {1{`RANDOM}};
  head_22 = _RAND_23[4:0];
  _RAND_24 = {1{`RANDOM}};
  head_23 = _RAND_24[4:0];
  _RAND_25 = {1{`RANDOM}};
  used = _RAND_25[23:0];
  _RAND_26 = {1{`RANDOM}};
  tail_23 = _RAND_26[4:0];
  _RAND_27 = {1{`RANDOM}};
  tail_22 = _RAND_27[4:0];
  _RAND_28 = {1{`RANDOM}};
  tail_21 = _RAND_28[4:0];
  _RAND_29 = {1{`RANDOM}};
  tail_20 = _RAND_29[4:0];
  _RAND_30 = {1{`RANDOM}};
  tail_19 = _RAND_30[4:0];
  _RAND_31 = {1{`RANDOM}};
  tail_18 = _RAND_31[4:0];
  _RAND_32 = {1{`RANDOM}};
  tail_17 = _RAND_32[4:0];
  _RAND_33 = {1{`RANDOM}};
  tail_16 = _RAND_33[4:0];
  _RAND_34 = {1{`RANDOM}};
  tail_15 = _RAND_34[4:0];
  _RAND_35 = {1{`RANDOM}};
  tail_14 = _RAND_35[4:0];
  _RAND_36 = {1{`RANDOM}};
  tail_13 = _RAND_36[4:0];
  _RAND_37 = {1{`RANDOM}};
  tail_12 = _RAND_37[4:0];
  _RAND_38 = {1{`RANDOM}};
  tail_11 = _RAND_38[4:0];
  _RAND_39 = {1{`RANDOM}};
  tail_10 = _RAND_39[4:0];
  _RAND_40 = {1{`RANDOM}};
  tail_9 = _RAND_40[4:0];
  _RAND_41 = {1{`RANDOM}};
  tail_8 = _RAND_41[4:0];
  _RAND_42 = {1{`RANDOM}};
  tail_7 = _RAND_42[4:0];
  _RAND_43 = {1{`RANDOM}};
  tail_6 = _RAND_43[4:0];
  _RAND_44 = {1{`RANDOM}};
  tail_5 = _RAND_44[4:0];
  _RAND_45 = {1{`RANDOM}};
  tail_4 = _RAND_45[4:0];
  _RAND_46 = {1{`RANDOM}};
  tail_3 = _RAND_46[4:0];
  _RAND_47 = {1{`RANDOM}};
  tail_2 = _RAND_47[4:0];
  _RAND_48 = {1{`RANDOM}};
  tail_1 = _RAND_48[4:0];
  _RAND_49 = {1{`RANDOM}};
  tail_0 = _RAND_49[4:0];
  _RAND_50 = {1{`RANDOM}};
  next_23 = _RAND_50[4:0];
  _RAND_51 = {1{`RANDOM}};
  next_22 = _RAND_51[4:0];
  _RAND_52 = {1{`RANDOM}};
  next_21 = _RAND_52[4:0];
  _RAND_53 = {1{`RANDOM}};
  next_20 = _RAND_53[4:0];
  _RAND_54 = {1{`RANDOM}};
  next_19 = _RAND_54[4:0];
  _RAND_55 = {1{`RANDOM}};
  next_18 = _RAND_55[4:0];
  _RAND_56 = {1{`RANDOM}};
  next_17 = _RAND_56[4:0];
  _RAND_57 = {1{`RANDOM}};
  next_16 = _RAND_57[4:0];
  _RAND_58 = {1{`RANDOM}};
  next_15 = _RAND_58[4:0];
  _RAND_59 = {1{`RANDOM}};
  next_14 = _RAND_59[4:0];
  _RAND_60 = {1{`RANDOM}};
  next_13 = _RAND_60[4:0];
  _RAND_61 = {1{`RANDOM}};
  next_12 = _RAND_61[4:0];
  _RAND_62 = {1{`RANDOM}};
  next_11 = _RAND_62[4:0];
  _RAND_63 = {1{`RANDOM}};
  next_10 = _RAND_63[4:0];
  _RAND_64 = {1{`RANDOM}};
  next_9 = _RAND_64[4:0];
  _RAND_65 = {1{`RANDOM}};
  next_8 = _RAND_65[4:0];
  _RAND_66 = {1{`RANDOM}};
  next_7 = _RAND_66[4:0];
  _RAND_67 = {1{`RANDOM}};
  next_6 = _RAND_67[4:0];
  _RAND_68 = {1{`RANDOM}};
  next_5 = _RAND_68[4:0];
  _RAND_69 = {1{`RANDOM}};
  next_4 = _RAND_69[4:0];
  _RAND_70 = {1{`RANDOM}};
  next_3 = _RAND_70[4:0];
  _RAND_71 = {1{`RANDOM}};
  next_2 = _RAND_71[4:0];
  _RAND_72 = {1{`RANDOM}};
  next_1 = _RAND_72[4:0];
  _RAND_73 = {1{`RANDOM}};
  next_0 = _RAND_73[4:0];
  _RAND_74 = {4{`RANDOM}};
  data_0_data = _RAND_74[127:0];
  _RAND_75 = {1{`RANDOM}};
  data_0_mask = _RAND_75[15:0];
  _RAND_76 = {1{`RANDOM}};
  data_0_corrupt = _RAND_76[0:0];
  _RAND_77 = {4{`RANDOM}};
  data_1_data = _RAND_77[127:0];
  _RAND_78 = {1{`RANDOM}};
  data_1_mask = _RAND_78[15:0];
  _RAND_79 = {1{`RANDOM}};
  data_1_corrupt = _RAND_79[0:0];
  _RAND_80 = {4{`RANDOM}};
  data_2_data = _RAND_80[127:0];
  _RAND_81 = {1{`RANDOM}};
  data_2_mask = _RAND_81[15:0];
  _RAND_82 = {1{`RANDOM}};
  data_2_corrupt = _RAND_82[0:0];
  _RAND_83 = {4{`RANDOM}};
  data_3_data = _RAND_83[127:0];
  _RAND_84 = {1{`RANDOM}};
  data_3_mask = _RAND_84[15:0];
  _RAND_85 = {1{`RANDOM}};
  data_3_corrupt = _RAND_85[0:0];
  _RAND_86 = {4{`RANDOM}};
  data_4_data = _RAND_86[127:0];
  _RAND_87 = {1{`RANDOM}};
  data_4_mask = _RAND_87[15:0];
  _RAND_88 = {1{`RANDOM}};
  data_4_corrupt = _RAND_88[0:0];
  _RAND_89 = {4{`RANDOM}};
  data_5_data = _RAND_89[127:0];
  _RAND_90 = {1{`RANDOM}};
  data_5_mask = _RAND_90[15:0];
  _RAND_91 = {1{`RANDOM}};
  data_5_corrupt = _RAND_91[0:0];
  _RAND_92 = {4{`RANDOM}};
  data_6_data = _RAND_92[127:0];
  _RAND_93 = {1{`RANDOM}};
  data_6_mask = _RAND_93[15:0];
  _RAND_94 = {1{`RANDOM}};
  data_6_corrupt = _RAND_94[0:0];
  _RAND_95 = {4{`RANDOM}};
  data_7_data = _RAND_95[127:0];
  _RAND_96 = {1{`RANDOM}};
  data_7_mask = _RAND_96[15:0];
  _RAND_97 = {1{`RANDOM}};
  data_7_corrupt = _RAND_97[0:0];
  _RAND_98 = {4{`RANDOM}};
  data_8_data = _RAND_98[127:0];
  _RAND_99 = {1{`RANDOM}};
  data_8_mask = _RAND_99[15:0];
  _RAND_100 = {1{`RANDOM}};
  data_8_corrupt = _RAND_100[0:0];
  _RAND_101 = {4{`RANDOM}};
  data_9_data = _RAND_101[127:0];
  _RAND_102 = {1{`RANDOM}};
  data_9_mask = _RAND_102[15:0];
  _RAND_103 = {1{`RANDOM}};
  data_9_corrupt = _RAND_103[0:0];
  _RAND_104 = {4{`RANDOM}};
  data_10_data = _RAND_104[127:0];
  _RAND_105 = {1{`RANDOM}};
  data_10_mask = _RAND_105[15:0];
  _RAND_106 = {1{`RANDOM}};
  data_10_corrupt = _RAND_106[0:0];
  _RAND_107 = {4{`RANDOM}};
  data_11_data = _RAND_107[127:0];
  _RAND_108 = {1{`RANDOM}};
  data_11_mask = _RAND_108[15:0];
  _RAND_109 = {1{`RANDOM}};
  data_11_corrupt = _RAND_109[0:0];
  _RAND_110 = {4{`RANDOM}};
  data_12_data = _RAND_110[127:0];
  _RAND_111 = {1{`RANDOM}};
  data_12_mask = _RAND_111[15:0];
  _RAND_112 = {1{`RANDOM}};
  data_12_corrupt = _RAND_112[0:0];
  _RAND_113 = {4{`RANDOM}};
  data_13_data = _RAND_113[127:0];
  _RAND_114 = {1{`RANDOM}};
  data_13_mask = _RAND_114[15:0];
  _RAND_115 = {1{`RANDOM}};
  data_13_corrupt = _RAND_115[0:0];
  _RAND_116 = {4{`RANDOM}};
  data_14_data = _RAND_116[127:0];
  _RAND_117 = {1{`RANDOM}};
  data_14_mask = _RAND_117[15:0];
  _RAND_118 = {1{`RANDOM}};
  data_14_corrupt = _RAND_118[0:0];
  _RAND_119 = {4{`RANDOM}};
  data_15_data = _RAND_119[127:0];
  _RAND_120 = {1{`RANDOM}};
  data_15_mask = _RAND_120[15:0];
  _RAND_121 = {1{`RANDOM}};
  data_15_corrupt = _RAND_121[0:0];
  _RAND_122 = {4{`RANDOM}};
  data_16_data = _RAND_122[127:0];
  _RAND_123 = {1{`RANDOM}};
  data_16_mask = _RAND_123[15:0];
  _RAND_124 = {1{`RANDOM}};
  data_16_corrupt = _RAND_124[0:0];
  _RAND_125 = {4{`RANDOM}};
  data_17_data = _RAND_125[127:0];
  _RAND_126 = {1{`RANDOM}};
  data_17_mask = _RAND_126[15:0];
  _RAND_127 = {1{`RANDOM}};
  data_17_corrupt = _RAND_127[0:0];
  _RAND_128 = {4{`RANDOM}};
  data_18_data = _RAND_128[127:0];
  _RAND_129 = {1{`RANDOM}};
  data_18_mask = _RAND_129[15:0];
  _RAND_130 = {1{`RANDOM}};
  data_18_corrupt = _RAND_130[0:0];
  _RAND_131 = {4{`RANDOM}};
  data_19_data = _RAND_131[127:0];
  _RAND_132 = {1{`RANDOM}};
  data_19_mask = _RAND_132[15:0];
  _RAND_133 = {1{`RANDOM}};
  data_19_corrupt = _RAND_133[0:0];
  _RAND_134 = {4{`RANDOM}};
  data_20_data = _RAND_134[127:0];
  _RAND_135 = {1{`RANDOM}};
  data_20_mask = _RAND_135[15:0];
  _RAND_136 = {1{`RANDOM}};
  data_20_corrupt = _RAND_136[0:0];
  _RAND_137 = {4{`RANDOM}};
  data_21_data = _RAND_137[127:0];
  _RAND_138 = {1{`RANDOM}};
  data_21_mask = _RAND_138[15:0];
  _RAND_139 = {1{`RANDOM}};
  data_21_corrupt = _RAND_139[0:0];
  _RAND_140 = {4{`RANDOM}};
  data_22_data = _RAND_140[127:0];
  _RAND_141 = {1{`RANDOM}};
  data_22_mask = _RAND_141[15:0];
  _RAND_142 = {1{`RANDOM}};
  data_22_corrupt = _RAND_142[0:0];
  _RAND_143 = {4{`RANDOM}};
  data_23_data = _RAND_143[127:0];
  _RAND_144 = {1{`RANDOM}};
  data_23_mask = _RAND_144[15:0];
  _RAND_145 = {1{`RANDOM}};
  data_23_corrupt = _RAND_145[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    valid = 24'h0;
  end
  if (rf_reset) begin
    head_0 = 5'h0;
  end
  if (rf_reset) begin
    head_1 = 5'h0;
  end
  if (rf_reset) begin
    head_2 = 5'h0;
  end
  if (rf_reset) begin
    head_3 = 5'h0;
  end
  if (rf_reset) begin
    head_4 = 5'h0;
  end
  if (rf_reset) begin
    head_5 = 5'h0;
  end
  if (rf_reset) begin
    head_6 = 5'h0;
  end
  if (rf_reset) begin
    head_7 = 5'h0;
  end
  if (rf_reset) begin
    head_8 = 5'h0;
  end
  if (rf_reset) begin
    head_9 = 5'h0;
  end
  if (rf_reset) begin
    head_10 = 5'h0;
  end
  if (rf_reset) begin
    head_11 = 5'h0;
  end
  if (rf_reset) begin
    head_12 = 5'h0;
  end
  if (rf_reset) begin
    head_13 = 5'h0;
  end
  if (rf_reset) begin
    head_14 = 5'h0;
  end
  if (rf_reset) begin
    head_15 = 5'h0;
  end
  if (rf_reset) begin
    head_16 = 5'h0;
  end
  if (rf_reset) begin
    head_17 = 5'h0;
  end
  if (rf_reset) begin
    head_18 = 5'h0;
  end
  if (rf_reset) begin
    head_19 = 5'h0;
  end
  if (rf_reset) begin
    head_20 = 5'h0;
  end
  if (rf_reset) begin
    head_21 = 5'h0;
  end
  if (rf_reset) begin
    head_22 = 5'h0;
  end
  if (rf_reset) begin
    head_23 = 5'h0;
  end
  if (reset) begin
    used = 24'h0;
  end
  if (rf_reset) begin
    tail_23 = 5'h0;
  end
  if (rf_reset) begin
    tail_22 = 5'h0;
  end
  if (rf_reset) begin
    tail_21 = 5'h0;
  end
  if (rf_reset) begin
    tail_20 = 5'h0;
  end
  if (rf_reset) begin
    tail_19 = 5'h0;
  end
  if (rf_reset) begin
    tail_18 = 5'h0;
  end
  if (rf_reset) begin
    tail_17 = 5'h0;
  end
  if (rf_reset) begin
    tail_16 = 5'h0;
  end
  if (rf_reset) begin
    tail_15 = 5'h0;
  end
  if (rf_reset) begin
    tail_14 = 5'h0;
  end
  if (rf_reset) begin
    tail_13 = 5'h0;
  end
  if (rf_reset) begin
    tail_12 = 5'h0;
  end
  if (rf_reset) begin
    tail_11 = 5'h0;
  end
  if (rf_reset) begin
    tail_10 = 5'h0;
  end
  if (rf_reset) begin
    tail_9 = 5'h0;
  end
  if (rf_reset) begin
    tail_8 = 5'h0;
  end
  if (rf_reset) begin
    tail_7 = 5'h0;
  end
  if (rf_reset) begin
    tail_6 = 5'h0;
  end
  if (rf_reset) begin
    tail_5 = 5'h0;
  end
  if (rf_reset) begin
    tail_4 = 5'h0;
  end
  if (rf_reset) begin
    tail_3 = 5'h0;
  end
  if (rf_reset) begin
    tail_2 = 5'h0;
  end
  if (rf_reset) begin
    tail_1 = 5'h0;
  end
  if (rf_reset) begin
    tail_0 = 5'h0;
  end
  if (rf_reset) begin
    next_23 = 5'h0;
  end
  if (rf_reset) begin
    next_22 = 5'h0;
  end
  if (rf_reset) begin
    next_21 = 5'h0;
  end
  if (rf_reset) begin
    next_20 = 5'h0;
  end
  if (rf_reset) begin
    next_19 = 5'h0;
  end
  if (rf_reset) begin
    next_18 = 5'h0;
  end
  if (rf_reset) begin
    next_17 = 5'h0;
  end
  if (rf_reset) begin
    next_16 = 5'h0;
  end
  if (rf_reset) begin
    next_15 = 5'h0;
  end
  if (rf_reset) begin
    next_14 = 5'h0;
  end
  if (rf_reset) begin
    next_13 = 5'h0;
  end
  if (rf_reset) begin
    next_12 = 5'h0;
  end
  if (rf_reset) begin
    next_11 = 5'h0;
  end
  if (rf_reset) begin
    next_10 = 5'h0;
  end
  if (rf_reset) begin
    next_9 = 5'h0;
  end
  if (rf_reset) begin
    next_8 = 5'h0;
  end
  if (rf_reset) begin
    next_7 = 5'h0;
  end
  if (rf_reset) begin
    next_6 = 5'h0;
  end
  if (rf_reset) begin
    next_5 = 5'h0;
  end
  if (rf_reset) begin
    next_4 = 5'h0;
  end
  if (rf_reset) begin
    next_3 = 5'h0;
  end
  if (rf_reset) begin
    next_2 = 5'h0;
  end
  if (rf_reset) begin
    next_1 = 5'h0;
  end
  if (rf_reset) begin
    next_0 = 5'h0;
  end
  if (rf_reset) begin
    data_0_data = 128'h0;
  end
  if (rf_reset) begin
    data_0_mask = 16'h0;
  end
  if (rf_reset) begin
    data_0_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_1_data = 128'h0;
  end
  if (rf_reset) begin
    data_1_mask = 16'h0;
  end
  if (rf_reset) begin
    data_1_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_2_data = 128'h0;
  end
  if (rf_reset) begin
    data_2_mask = 16'h0;
  end
  if (rf_reset) begin
    data_2_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_3_data = 128'h0;
  end
  if (rf_reset) begin
    data_3_mask = 16'h0;
  end
  if (rf_reset) begin
    data_3_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_4_data = 128'h0;
  end
  if (rf_reset) begin
    data_4_mask = 16'h0;
  end
  if (rf_reset) begin
    data_4_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_5_data = 128'h0;
  end
  if (rf_reset) begin
    data_5_mask = 16'h0;
  end
  if (rf_reset) begin
    data_5_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_6_data = 128'h0;
  end
  if (rf_reset) begin
    data_6_mask = 16'h0;
  end
  if (rf_reset) begin
    data_6_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_7_data = 128'h0;
  end
  if (rf_reset) begin
    data_7_mask = 16'h0;
  end
  if (rf_reset) begin
    data_7_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_8_data = 128'h0;
  end
  if (rf_reset) begin
    data_8_mask = 16'h0;
  end
  if (rf_reset) begin
    data_8_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_9_data = 128'h0;
  end
  if (rf_reset) begin
    data_9_mask = 16'h0;
  end
  if (rf_reset) begin
    data_9_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_10_data = 128'h0;
  end
  if (rf_reset) begin
    data_10_mask = 16'h0;
  end
  if (rf_reset) begin
    data_10_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_11_data = 128'h0;
  end
  if (rf_reset) begin
    data_11_mask = 16'h0;
  end
  if (rf_reset) begin
    data_11_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_12_data = 128'h0;
  end
  if (rf_reset) begin
    data_12_mask = 16'h0;
  end
  if (rf_reset) begin
    data_12_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_13_data = 128'h0;
  end
  if (rf_reset) begin
    data_13_mask = 16'h0;
  end
  if (rf_reset) begin
    data_13_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_14_data = 128'h0;
  end
  if (rf_reset) begin
    data_14_mask = 16'h0;
  end
  if (rf_reset) begin
    data_14_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_15_data = 128'h0;
  end
  if (rf_reset) begin
    data_15_mask = 16'h0;
  end
  if (rf_reset) begin
    data_15_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_16_data = 128'h0;
  end
  if (rf_reset) begin
    data_16_mask = 16'h0;
  end
  if (rf_reset) begin
    data_16_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_17_data = 128'h0;
  end
  if (rf_reset) begin
    data_17_mask = 16'h0;
  end
  if (rf_reset) begin
    data_17_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_18_data = 128'h0;
  end
  if (rf_reset) begin
    data_18_mask = 16'h0;
  end
  if (rf_reset) begin
    data_18_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_19_data = 128'h0;
  end
  if (rf_reset) begin
    data_19_mask = 16'h0;
  end
  if (rf_reset) begin
    data_19_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_20_data = 128'h0;
  end
  if (rf_reset) begin
    data_20_mask = 16'h0;
  end
  if (rf_reset) begin
    data_20_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_21_data = 128'h0;
  end
  if (rf_reset) begin
    data_21_mask = 16'h0;
  end
  if (rf_reset) begin
    data_21_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_22_data = 128'h0;
  end
  if (rf_reset) begin
    data_22_mask = 16'h0;
  end
  if (rf_reset) begin
    data_22_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_23_data = 128'h0;
  end
  if (rf_reset) begin
    data_23_mask = 16'h0;
  end
  if (rf_reset) begin
    data_23_corrupt = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
