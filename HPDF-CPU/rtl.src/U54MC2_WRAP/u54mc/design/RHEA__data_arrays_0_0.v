//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__data_arrays_0_0(
  input  [8:0]  RW0_addr,
  input         RW0_en,
  input         RW0_clk,
  input         RW0_wmode,
  input  [31:0] RW0_wdata_0,
  input  [31:0] RW0_wdata_1,
  input  [31:0] RW0_wdata_2,
  input  [31:0] RW0_wdata_3,
  output [31:0] RW0_rdata_0,
  output [31:0] RW0_rdata_1,
  output [31:0] RW0_rdata_2,
  output [31:0] RW0_rdata_3,
  input         RW0_wmask_0,
  input         RW0_wmask_1,
  input         RW0_wmask_2,
  input         RW0_wmask_3
);
  wire [8:0] data_arrays_0_0_ext_RW0_addr;
  wire  data_arrays_0_0_ext_RW0_en;
  wire  data_arrays_0_0_ext_RW0_clk;
  wire  data_arrays_0_0_ext_RW0_wmode;
  wire [127:0] data_arrays_0_0_ext_RW0_wdata;
  wire [127:0] data_arrays_0_0_ext_RW0_rdata;
  wire [3:0] data_arrays_0_0_ext_RW0_wmask;
  wire [63:0] _GEN_0 = {RW0_wdata_3,RW0_wdata_2};
  wire [63:0] _GEN_1 = {RW0_wdata_1,RW0_wdata_0};
  wire [1:0] _GEN_2 = {RW0_wmask_3,RW0_wmask_2};
  wire [1:0] _GEN_3 = {RW0_wmask_1,RW0_wmask_0};
  RHEA__data_arrays_0_0_ext data_arrays_0_0_ext (
    .RW0_addr(data_arrays_0_0_ext_RW0_addr),
    .RW0_en(data_arrays_0_0_ext_RW0_en),
    .RW0_clk(data_arrays_0_0_ext_RW0_clk),
    .RW0_wmode(data_arrays_0_0_ext_RW0_wmode),
    .RW0_wdata(data_arrays_0_0_ext_RW0_wdata),
    .RW0_rdata(data_arrays_0_0_ext_RW0_rdata),
    .RW0_wmask(data_arrays_0_0_ext_RW0_wmask)
  );
  assign data_arrays_0_0_ext_RW0_clk = RW0_clk;
  assign data_arrays_0_0_ext_RW0_en = RW0_en;
  assign data_arrays_0_0_ext_RW0_addr = RW0_addr;
  assign RW0_rdata_0 = data_arrays_0_0_ext_RW0_rdata[31:0];
  assign RW0_rdata_1 = data_arrays_0_0_ext_RW0_rdata[63:32];
  assign RW0_rdata_2 = data_arrays_0_0_ext_RW0_rdata[95:64];
  assign RW0_rdata_3 = data_arrays_0_0_ext_RW0_rdata[127:96];
  assign data_arrays_0_0_ext_RW0_wmode = RW0_wmode;
  assign data_arrays_0_0_ext_RW0_wdata = {_GEN_0,_GEN_1};
  assign data_arrays_0_0_ext_RW0_wmask = {_GEN_2,_GEN_3};
endmodule
