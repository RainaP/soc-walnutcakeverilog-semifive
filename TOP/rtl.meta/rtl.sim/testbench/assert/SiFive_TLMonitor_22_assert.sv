//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_22_assert(
  input         clock,
  input         reset,
  input         io_in_a_ready,
  input         io_in_a_valid,
  input  [2:0]  io_in_a_bits_opcode,
  input  [2:0]  io_in_a_bits_param,
  input  [3:0]  io_in_a_bits_size,
  input  [4:0]  io_in_a_bits_source,
  input  [35:0] io_in_a_bits_address,
  input  [7:0]  io_in_a_bits_mask,
  input         io_in_a_bits_corrupt,
  input         io_in_d_ready,
  input         io_in_d_valid,
  input  [2:0]  io_in_d_bits_opcode,
  input  [1:0]  io_in_d_bits_param,
  input  [3:0]  io_in_d_bits_size,
  input  [4:0]  io_in_d_bits_source,
  input  [3:0]  io_in_d_bits_sink,
  input         io_in_d_bits_denied,
  input         io_in_d_bits_corrupt
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire  _T_7; // @[Parameters.scala 55:32]
  wire  _T_15; // @[Parameters.scala 55:32]
  wire  _T_23; // @[Parameters.scala 55:32]
  wire  _T_31; // @[Parameters.scala 55:32]
  wire  _T_37; // @[Parameters.scala 924:46]
  wire  _T_38; // @[Parameters.scala 924:46]
  wire  _T_39; // @[Parameters.scala 924:46]
  wire [22:0] _T_41; // @[package.scala 189:77]
  wire [7:0] _T_43; // @[package.scala 189:46]
  wire [35:0] _GEN_18; // @[Edges.scala 22:16]
  wire [35:0] _T_44; // @[Edges.scala 22:16]
  wire  _T_45; // @[Edges.scala 22:24]
  wire [3:0] _T_48; // @[OneHot.scala 65:12]
  wire [2:0] _T_50; // @[Misc.scala 201:81]
  wire  _T_51; // @[Misc.scala 205:21]
  wire  _T_54; // @[Misc.scala 210:20]
  wire  _T_56; // @[Misc.scala 214:38]
  wire  _T_57; // @[Misc.scala 214:29]
  wire  _T_59; // @[Misc.scala 214:38]
  wire  _T_60; // @[Misc.scala 214:29]
  wire  _T_63; // @[Misc.scala 210:20]
  wire  _T_64; // @[Misc.scala 213:27]
  wire  _T_65; // @[Misc.scala 214:38]
  wire  _T_66; // @[Misc.scala 214:29]
  wire  _T_67; // @[Misc.scala 213:27]
  wire  _T_68; // @[Misc.scala 214:38]
  wire  _T_69; // @[Misc.scala 214:29]
  wire  _T_70; // @[Misc.scala 213:27]
  wire  _T_71; // @[Misc.scala 214:38]
  wire  _T_72; // @[Misc.scala 214:29]
  wire  _T_73; // @[Misc.scala 213:27]
  wire  _T_74; // @[Misc.scala 214:38]
  wire  _T_75; // @[Misc.scala 214:29]
  wire  _T_78; // @[Misc.scala 210:20]
  wire  _T_79; // @[Misc.scala 213:27]
  wire  _T_80; // @[Misc.scala 214:38]
  wire  _T_81; // @[Misc.scala 214:29]
  wire  _T_82; // @[Misc.scala 213:27]
  wire  _T_83; // @[Misc.scala 214:38]
  wire  _T_84; // @[Misc.scala 214:29]
  wire  _T_85; // @[Misc.scala 213:27]
  wire  _T_86; // @[Misc.scala 214:38]
  wire  _T_87; // @[Misc.scala 214:29]
  wire  _T_88; // @[Misc.scala 213:27]
  wire  _T_89; // @[Misc.scala 214:38]
  wire  _T_90; // @[Misc.scala 214:29]
  wire  _T_91; // @[Misc.scala 213:27]
  wire  _T_92; // @[Misc.scala 214:38]
  wire  _T_93; // @[Misc.scala 214:29]
  wire  _T_94; // @[Misc.scala 213:27]
  wire  _T_95; // @[Misc.scala 214:38]
  wire  _T_96; // @[Misc.scala 214:29]
  wire  _T_97; // @[Misc.scala 213:27]
  wire  _T_98; // @[Misc.scala 214:38]
  wire  _T_99; // @[Misc.scala 214:29]
  wire  _T_100; // @[Misc.scala 213:27]
  wire  _T_101; // @[Misc.scala 214:38]
  wire  _T_102; // @[Misc.scala 214:29]
  wire [7:0] _T_109; // @[Cat.scala 29:58]
  wire [36:0] _T_120; // @[Parameters.scala 137:49]
  wire  _T_176; // @[Monitor.scala 79:25]
  wire [35:0] _T_178; // @[Parameters.scala 137:31]
  wire [36:0] _T_179; // @[Parameters.scala 137:49]
  wire [36:0] _T_181; // @[Parameters.scala 137:52]
  wire  _T_182; // @[Parameters.scala 137:67]
  wire [35:0] _T_183; // @[Parameters.scala 137:31]
  wire [36:0] _T_184; // @[Parameters.scala 137:49]
  wire [35:0] _T_188; // @[Parameters.scala 137:31]
  wire [36:0] _T_189; // @[Parameters.scala 137:49]
  wire [36:0] _T_191; // @[Parameters.scala 137:52]
  wire  _T_192; // @[Parameters.scala 137:67]
  wire [35:0] _T_193; // @[Parameters.scala 137:31]
  wire [36:0] _T_194; // @[Parameters.scala 137:49]
  wire [36:0] _T_196; // @[Parameters.scala 137:52]
  wire  _T_197; // @[Parameters.scala 137:67]
  wire [35:0] _T_198; // @[Parameters.scala 137:31]
  wire [36:0] _T_199; // @[Parameters.scala 137:49]
  wire [36:0] _T_201; // @[Parameters.scala 137:52]
  wire  _T_202; // @[Parameters.scala 137:67]
  wire [36:0] _T_206; // @[Parameters.scala 137:52]
  wire  _T_207; // @[Parameters.scala 137:67]
  wire [35:0] _T_208; // @[Parameters.scala 137:31]
  wire [36:0] _T_209; // @[Parameters.scala 137:49]
  wire [36:0] _T_211; // @[Parameters.scala 137:52]
  wire  _T_212; // @[Parameters.scala 137:67]
  wire [35:0] _T_213; // @[Parameters.scala 137:31]
  wire [36:0] _T_214; // @[Parameters.scala 137:49]
  wire [36:0] _T_216; // @[Parameters.scala 137:52]
  wire  _T_217; // @[Parameters.scala 137:67]
  wire [35:0] _T_218; // @[Parameters.scala 137:31]
  wire [36:0] _T_219; // @[Parameters.scala 137:49]
  wire [36:0] _T_221; // @[Parameters.scala 137:52]
  wire  _T_222; // @[Parameters.scala 137:67]
  wire [35:0] _T_223; // @[Parameters.scala 137:31]
  wire [36:0] _T_224; // @[Parameters.scala 137:49]
  wire [36:0] _T_226; // @[Parameters.scala 137:52]
  wire  _T_227; // @[Parameters.scala 137:67]
  wire  _T_238; // @[Parameters.scala 92:48]
  wire [35:0] _T_240; // @[Parameters.scala 137:31]
  wire [36:0] _T_241; // @[Parameters.scala 137:49]
  wire [36:0] _T_243; // @[Parameters.scala 137:52]
  wire  _T_244; // @[Parameters.scala 137:67]
  wire [35:0] _T_245; // @[Parameters.scala 137:31]
  wire [36:0] _T_246; // @[Parameters.scala 137:49]
  wire [36:0] _T_248; // @[Parameters.scala 137:52]
  wire  _T_249; // @[Parameters.scala 137:67]
  wire  _T_250; // @[Parameters.scala 552:42]
  wire  _T_251; // @[Parameters.scala 551:56]
  wire  _T_255; // @[Monitor.scala 44:11]
  wire  _T_256; // @[Monitor.scala 44:11]
  wire  _T_259; // @[Monitor.scala 44:11]
  wire  _T_261; // @[Monitor.scala 44:11]
  wire  _T_262; // @[Monitor.scala 44:11]
  wire  _T_265; // @[Monitor.scala 44:11]
  wire  _T_266; // @[Monitor.scala 44:11]
  wire  _T_268; // @[Monitor.scala 44:11]
  wire  _T_269; // @[Monitor.scala 44:11]
  wire  _T_270; // @[Bundles.scala 110:27]
  wire  _T_272; // @[Monitor.scala 44:11]
  wire  _T_273; // @[Monitor.scala 44:11]
  wire [7:0] _T_274; // @[Monitor.scala 86:18]
  wire  _T_275; // @[Monitor.scala 86:31]
  wire  _T_277; // @[Monitor.scala 44:11]
  wire  _T_278; // @[Monitor.scala 44:11]
  wire  _T_279; // @[Monitor.scala 87:18]
  wire  _T_281; // @[Monitor.scala 44:11]
  wire  _T_282; // @[Monitor.scala 44:11]
  wire  _T_283; // @[Monitor.scala 90:25]
  wire  _T_381; // @[Monitor.scala 97:31]
  wire  _T_383; // @[Monitor.scala 44:11]
  wire  _T_384; // @[Monitor.scala 44:11]
  wire  _T_394; // @[Monitor.scala 102:25]
  wire  _T_396; // @[Parameters.scala 93:42]
  wire [36:0] _T_407; // @[Parameters.scala 137:52]
  wire  _T_408; // @[Parameters.scala 137:67]
  wire [36:0] _T_437; // @[Parameters.scala 137:52]
  wire  _T_438; // @[Parameters.scala 137:67]
  wire  _T_449; // @[Parameters.scala 552:42]
  wire  _T_450; // @[Parameters.scala 552:42]
  wire  _T_451; // @[Parameters.scala 552:42]
  wire  _T_452; // @[Parameters.scala 552:42]
  wire  _T_453; // @[Parameters.scala 552:42]
  wire  _T_454; // @[Parameters.scala 552:42]
  wire  _T_455; // @[Parameters.scala 552:42]
  wire  _T_456; // @[Parameters.scala 552:42]
  wire  _T_457; // @[Parameters.scala 552:42]
  wire  _T_458; // @[Parameters.scala 551:56]
  wire  _T_460; // @[Parameters.scala 93:42]
  wire [36:0] _T_466; // @[Parameters.scala 137:52]
  wire  _T_467; // @[Parameters.scala 137:67]
  wire  _T_468; // @[Parameters.scala 551:56]
  wire  _T_470; // @[Parameters.scala 553:30]
  wire  _T_472; // @[Monitor.scala 44:11]
  wire  _T_473; // @[Monitor.scala 44:11]
  wire  _T_480; // @[Monitor.scala 106:31]
  wire  _T_482; // @[Monitor.scala 44:11]
  wire  _T_483; // @[Monitor.scala 44:11]
  wire  _T_484; // @[Monitor.scala 107:30]
  wire  _T_486; // @[Monitor.scala 44:11]
  wire  _T_487; // @[Monitor.scala 44:11]
  wire  _T_492; // @[Monitor.scala 111:25]
  wire  _T_494; // @[Parameters.scala 93:42]
  wire  _T_502; // @[Parameters.scala 551:56]
  wire  _T_552; // @[Parameters.scala 552:42]
  wire  _T_553; // @[Parameters.scala 552:42]
  wire  _T_554; // @[Parameters.scala 552:42]
  wire  _T_555; // @[Parameters.scala 552:42]
  wire  _T_556; // @[Parameters.scala 552:42]
  wire  _T_557; // @[Parameters.scala 552:42]
  wire  _T_558; // @[Parameters.scala 552:42]
  wire  _T_559; // @[Parameters.scala 552:42]
  wire  _T_560; // @[Parameters.scala 551:56]
  wire  _T_572; // @[Parameters.scala 553:30]
  wire  _T_573; // @[Parameters.scala 553:30]
  wire  _T_575; // @[Monitor.scala 44:11]
  wire  _T_576; // @[Monitor.scala 44:11]
  wire  _T_591; // @[Monitor.scala 119:25]
  wire [7:0] _T_686; // @[Monitor.scala 124:33]
  wire [7:0] _T_687; // @[Monitor.scala 124:31]
  wire  _T_688; // @[Monitor.scala 124:40]
  wire  _T_690; // @[Monitor.scala 44:11]
  wire  _T_691; // @[Monitor.scala 44:11]
  wire  _T_692; // @[Monitor.scala 127:25]
  wire  _T_701; // @[Parameters.scala 93:42]
  wire  _T_755; // @[Parameters.scala 552:42]
  wire  _T_756; // @[Parameters.scala 552:42]
  wire  _T_757; // @[Parameters.scala 552:42]
  wire  _T_758; // @[Parameters.scala 552:42]
  wire  _T_759; // @[Parameters.scala 552:42]
  wire  _T_760; // @[Parameters.scala 552:42]
  wire  _T_761; // @[Parameters.scala 552:42]
  wire  _T_762; // @[Parameters.scala 552:42]
  wire  _T_763; // @[Parameters.scala 551:56]
  wire  _T_767; // @[Monitor.scala 44:11]
  wire  _T_768; // @[Monitor.scala 44:11]
  wire  _T_775; // @[Bundles.scala 140:33]
  wire  _T_777; // @[Monitor.scala 44:11]
  wire  _T_778; // @[Monitor.scala 44:11]
  wire  _T_783; // @[Monitor.scala 135:25]
  wire  _T_866; // @[Bundles.scala 147:30]
  wire  _T_868; // @[Monitor.scala 44:11]
  wire  _T_869; // @[Monitor.scala 44:11]
  wire  _T_874; // @[Monitor.scala 143:25]
  wire  _T_945; // @[Parameters.scala 551:56]
  wire  _T_958; // @[Parameters.scala 553:30]
  wire  _T_960; // @[Monitor.scala 44:11]
  wire  _T_961; // @[Monitor.scala 44:11]
  wire  _T_968; // @[Bundles.scala 160:28]
  wire  _T_970; // @[Monitor.scala 44:11]
  wire  _T_971; // @[Monitor.scala 44:11]
  wire  _T_980; // @[Bundles.scala 44:24]
  wire  _T_982; // @[Monitor.scala 51:11]
  wire  _T_983; // @[Monitor.scala 51:11]
  wire  _T_987; // @[Parameters.scala 55:32]
  wire  _T_995; // @[Parameters.scala 55:32]
  wire  _T_1003; // @[Parameters.scala 55:32]
  wire  _T_1011; // @[Parameters.scala 55:32]
  wire  _T_1017; // @[Parameters.scala 924:46]
  wire  _T_1018; // @[Parameters.scala 924:46]
  wire  _T_1019; // @[Parameters.scala 924:46]
  wire  _T_1021; // @[Monitor.scala 307:25]
  wire  _T_1023; // @[Monitor.scala 51:11]
  wire  _T_1024; // @[Monitor.scala 51:11]
  wire  _T_1025; // @[Monitor.scala 309:27]
  wire  _T_1027; // @[Monitor.scala 51:11]
  wire  _T_1028; // @[Monitor.scala 51:11]
  wire  _T_1029; // @[Monitor.scala 310:28]
  wire  _T_1031; // @[Monitor.scala 51:11]
  wire  _T_1032; // @[Monitor.scala 51:11]
  wire  _T_1033; // @[Monitor.scala 311:15]
  wire  _T_1035; // @[Monitor.scala 51:11]
  wire  _T_1036; // @[Monitor.scala 51:11]
  wire  _T_1037; // @[Monitor.scala 312:15]
  wire  _T_1039; // @[Monitor.scala 51:11]
  wire  _T_1040; // @[Monitor.scala 51:11]
  wire  _T_1041; // @[Monitor.scala 315:25]
  wire  _T_1052; // @[Bundles.scala 104:26]
  wire  _T_1054; // @[Monitor.scala 51:11]
  wire  _T_1055; // @[Monitor.scala 51:11]
  wire  _T_1056; // @[Monitor.scala 320:28]
  wire  _T_1058; // @[Monitor.scala 51:11]
  wire  _T_1059; // @[Monitor.scala 51:11]
  wire  _T_1069; // @[Monitor.scala 325:25]
  wire  _T_1089; // @[Monitor.scala 331:30]
  wire  _T_1091; // @[Monitor.scala 51:11]
  wire  _T_1092; // @[Monitor.scala 51:11]
  wire  _T_1098; // @[Monitor.scala 335:25]
  wire  _T_1115; // @[Monitor.scala 343:25]
  wire  _T_1133; // @[Monitor.scala 351:25]
  wire  _T_1165; // @[Decoupled.scala 40:37]
  wire  _T_1172; // @[Edges.scala 93:28]
  reg [4:0] _T_1174; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire [4:0] _T_1176; // @[Edges.scala 231:28]
  wire  _T_1177; // @[Edges.scala 232:25]
  reg [2:0] _T_1185; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [2:0] _T_1186; // @[Monitor.scala 382:22]
  reg [31:0] _RAND_2;
  reg [3:0] _T_1187; // @[Monitor.scala 383:22]
  reg [31:0] _RAND_3;
  reg [4:0] _T_1188; // @[Monitor.scala 384:22]
  reg [31:0] _RAND_4;
  reg [35:0] _T_1189; // @[Monitor.scala 385:22]
  reg [63:0] _RAND_5;
  wire  _T_1190; // @[Monitor.scala 386:22]
  wire  _T_1191; // @[Monitor.scala 386:19]
  wire  _T_1192; // @[Monitor.scala 387:32]
  wire  _T_1194; // @[Monitor.scala 44:11]
  wire  _T_1195; // @[Monitor.scala 44:11]
  wire  _T_1196; // @[Monitor.scala 388:32]
  wire  _T_1198; // @[Monitor.scala 44:11]
  wire  _T_1199; // @[Monitor.scala 44:11]
  wire  _T_1200; // @[Monitor.scala 389:32]
  wire  _T_1202; // @[Monitor.scala 44:11]
  wire  _T_1203; // @[Monitor.scala 44:11]
  wire  _T_1204; // @[Monitor.scala 390:32]
  wire  _T_1206; // @[Monitor.scala 44:11]
  wire  _T_1207; // @[Monitor.scala 44:11]
  wire  _T_1208; // @[Monitor.scala 391:32]
  wire  _T_1210; // @[Monitor.scala 44:11]
  wire  _T_1211; // @[Monitor.scala 44:11]
  wire  _T_1213; // @[Monitor.scala 393:20]
  wire  _T_1214; // @[Decoupled.scala 40:37]
  wire [22:0] _T_1216; // @[package.scala 189:77]
  wire [7:0] _T_1218; // @[package.scala 189:46]
  reg [4:0] _T_1222; // @[Edges.scala 230:27]
  reg [31:0] _RAND_6;
  wire [4:0] _T_1224; // @[Edges.scala 231:28]
  wire  _T_1225; // @[Edges.scala 232:25]
  reg [2:0] _T_1233; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_7;
  reg [1:0] _T_1234; // @[Monitor.scala 533:22]
  reg [31:0] _RAND_8;
  reg [3:0] _T_1235; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_9;
  reg [4:0] _T_1236; // @[Monitor.scala 535:22]
  reg [31:0] _RAND_10;
  reg [3:0] _T_1237; // @[Monitor.scala 536:22]
  reg [31:0] _RAND_11;
  reg  _T_1238; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_12;
  wire  _T_1239; // @[Monitor.scala 538:22]
  wire  _T_1240; // @[Monitor.scala 538:19]
  wire  _T_1241; // @[Monitor.scala 539:29]
  wire  _T_1243; // @[Monitor.scala 51:11]
  wire  _T_1244; // @[Monitor.scala 51:11]
  wire  _T_1245; // @[Monitor.scala 540:29]
  wire  _T_1247; // @[Monitor.scala 51:11]
  wire  _T_1248; // @[Monitor.scala 51:11]
  wire  _T_1249; // @[Monitor.scala 541:29]
  wire  _T_1251; // @[Monitor.scala 51:11]
  wire  _T_1252; // @[Monitor.scala 51:11]
  wire  _T_1253; // @[Monitor.scala 542:29]
  wire  _T_1255; // @[Monitor.scala 51:11]
  wire  _T_1256; // @[Monitor.scala 51:11]
  wire  _T_1257; // @[Monitor.scala 543:29]
  wire  _T_1259; // @[Monitor.scala 51:11]
  wire  _T_1260; // @[Monitor.scala 51:11]
  wire  _T_1261; // @[Monitor.scala 544:29]
  wire  _T_1263; // @[Monitor.scala 51:11]
  wire  _T_1264; // @[Monitor.scala 51:11]
  wire  _T_1266; // @[Monitor.scala 546:20]
  reg [31:0] _T_1267; // @[Monitor.scala 568:27]
  reg [31:0] _RAND_13;
  reg [4:0] _T_1277; // @[Edges.scala 230:27]
  reg [31:0] _RAND_14;
  wire [4:0] _T_1279; // @[Edges.scala 231:28]
  wire  _T_1280; // @[Edges.scala 232:25]
  reg [4:0] _T_1296; // @[Edges.scala 230:27]
  reg [31:0] _RAND_15;
  wire [4:0] _T_1298; // @[Edges.scala 231:28]
  wire  _T_1299; // @[Edges.scala 232:25]
  wire  _T_1309; // @[Monitor.scala 574:27]
  wire [31:0] _T_1311; // @[OneHot.scala 58:35]
  wire [31:0] _T_1312; // @[Monitor.scala 576:23]
  wire  _T_1314; // @[Monitor.scala 576:14]
  wire  _T_1316; // @[Monitor.scala 576:13]
  wire  _T_1317; // @[Monitor.scala 576:13]
  wire [31:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_1321; // @[Monitor.scala 581:27]
  wire  _T_1323; // @[Monitor.scala 581:75]
  wire  _T_1324; // @[Monitor.scala 581:72]
  wire [31:0] _T_1325; // @[OneHot.scala 58:35]
  wire [31:0] _T_1326; // @[Monitor.scala 583:21]
  wire [31:0] _T_1327; // @[Monitor.scala 583:32]
  wire  _T_1330; // @[Monitor.scala 51:11]
  wire  _T_1331; // @[Monitor.scala 51:11]
  wire [31:0] _GEN_16; // @[Monitor.scala 581:91]
  wire  _T_1332; // @[Monitor.scala 587:20]
  wire  _T_1333; // @[Monitor.scala 587:40]
  wire  _T_1334; // @[Monitor.scala 587:33]
  wire  _T_1335; // @[Monitor.scala 587:30]
  wire  _T_1337; // @[Monitor.scala 51:11]
  wire  _T_1338; // @[Monitor.scala 51:11]
  wire [31:0] _T_1339; // @[Monitor.scala 590:27]
  wire [31:0] _T_1340; // @[Monitor.scala 590:38]
  wire [31:0] _T_1341; // @[Monitor.scala 590:36]
  reg [31:0] _T_1342; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_16;
  wire  _T_1343; // @[Monitor.scala 595:23]
  wire  _T_1344; // @[Monitor.scala 595:13]
  wire  _T_1345; // @[Monitor.scala 595:36]
  wire  _T_1346; // @[Monitor.scala 595:27]
  wire  _T_1347; // @[Monitor.scala 595:56]
  wire  _T_1348; // @[Monitor.scala 595:44]
  wire  _T_1350; // @[Monitor.scala 595:12]
  wire  _T_1351; // @[Monitor.scala 595:12]
  wire [31:0] _T_1353; // @[Monitor.scala 597:26]
  wire  _T_1356; // @[Monitor.scala 598:27]
  wire  _GEN_19; // @[Monitor.scala 44:11]
  wire  _GEN_35; // @[Monitor.scala 44:11]
  wire  _GEN_53; // @[Monitor.scala 44:11]
  wire  _GEN_65; // @[Monitor.scala 44:11]
  wire  _GEN_75; // @[Monitor.scala 44:11]
  wire  _GEN_85; // @[Monitor.scala 44:11]
  wire  _GEN_95; // @[Monitor.scala 44:11]
  wire  _GEN_105; // @[Monitor.scala 44:11]
  wire  _GEN_117; // @[Monitor.scala 51:11]
  wire  _GEN_127; // @[Monitor.scala 51:11]
  wire  _GEN_137; // @[Monitor.scala 51:11]
  wire  _GEN_147; // @[Monitor.scala 51:11]
  wire  _GEN_153; // @[Monitor.scala 51:11]
  wire  _GEN_159; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_7 = io_in_a_bits_source[4:3] == 2'h0; // @[Parameters.scala 55:32]
  assign _T_15 = io_in_a_bits_source[4:3] == 2'h1; // @[Parameters.scala 55:32]
  assign _T_23 = io_in_a_bits_source[4:3] == 2'h2; // @[Parameters.scala 55:32]
  assign _T_31 = io_in_a_bits_source[4:3] == 2'h3; // @[Parameters.scala 55:32]
  assign _T_37 = _T_7 | _T_15; // @[Parameters.scala 924:46]
  assign _T_38 = _T_37 | _T_23; // @[Parameters.scala 924:46]
  assign _T_39 = _T_38 | _T_31; // @[Parameters.scala 924:46]
  assign _T_41 = 23'hff << io_in_a_bits_size; // @[package.scala 189:77]
  assign _T_43 = ~_T_41[7:0]; // @[package.scala 189:46]
  assign _GEN_18 = {{28'd0}, _T_43}; // @[Edges.scala 22:16]
  assign _T_44 = io_in_a_bits_address & _GEN_18; // @[Edges.scala 22:16]
  assign _T_45 = _T_44 == 36'h0; // @[Edges.scala 22:24]
  assign _T_48 = 4'h1 << io_in_a_bits_size[1:0]; // @[OneHot.scala 65:12]
  assign _T_50 = _T_48[2:0] | 3'h1; // @[Misc.scala 201:81]
  assign _T_51 = io_in_a_bits_size >= 4'h3; // @[Misc.scala 205:21]
  assign _T_54 = ~io_in_a_bits_address[2]; // @[Misc.scala 210:20]
  assign _T_56 = _T_50[2] & _T_54; // @[Misc.scala 214:38]
  assign _T_57 = _T_51 | _T_56; // @[Misc.scala 214:29]
  assign _T_59 = _T_50[2] & io_in_a_bits_address[2]; // @[Misc.scala 214:38]
  assign _T_60 = _T_51 | _T_59; // @[Misc.scala 214:29]
  assign _T_63 = ~io_in_a_bits_address[1]; // @[Misc.scala 210:20]
  assign _T_64 = _T_54 & _T_63; // @[Misc.scala 213:27]
  assign _T_65 = _T_50[1] & _T_64; // @[Misc.scala 214:38]
  assign _T_66 = _T_57 | _T_65; // @[Misc.scala 214:29]
  assign _T_67 = _T_54 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_68 = _T_50[1] & _T_67; // @[Misc.scala 214:38]
  assign _T_69 = _T_57 | _T_68; // @[Misc.scala 214:29]
  assign _T_70 = io_in_a_bits_address[2] & _T_63; // @[Misc.scala 213:27]
  assign _T_71 = _T_50[1] & _T_70; // @[Misc.scala 214:38]
  assign _T_72 = _T_60 | _T_71; // @[Misc.scala 214:29]
  assign _T_73 = io_in_a_bits_address[2] & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_74 = _T_50[1] & _T_73; // @[Misc.scala 214:38]
  assign _T_75 = _T_60 | _T_74; // @[Misc.scala 214:29]
  assign _T_78 = ~io_in_a_bits_address[0]; // @[Misc.scala 210:20]
  assign _T_79 = _T_64 & _T_78; // @[Misc.scala 213:27]
  assign _T_80 = _T_50[0] & _T_79; // @[Misc.scala 214:38]
  assign _T_81 = _T_66 | _T_80; // @[Misc.scala 214:29]
  assign _T_82 = _T_64 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_83 = _T_50[0] & _T_82; // @[Misc.scala 214:38]
  assign _T_84 = _T_66 | _T_83; // @[Misc.scala 214:29]
  assign _T_85 = _T_67 & _T_78; // @[Misc.scala 213:27]
  assign _T_86 = _T_50[0] & _T_85; // @[Misc.scala 214:38]
  assign _T_87 = _T_69 | _T_86; // @[Misc.scala 214:29]
  assign _T_88 = _T_67 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_89 = _T_50[0] & _T_88; // @[Misc.scala 214:38]
  assign _T_90 = _T_69 | _T_89; // @[Misc.scala 214:29]
  assign _T_91 = _T_70 & _T_78; // @[Misc.scala 213:27]
  assign _T_92 = _T_50[0] & _T_91; // @[Misc.scala 214:38]
  assign _T_93 = _T_72 | _T_92; // @[Misc.scala 214:29]
  assign _T_94 = _T_70 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_95 = _T_50[0] & _T_94; // @[Misc.scala 214:38]
  assign _T_96 = _T_72 | _T_95; // @[Misc.scala 214:29]
  assign _T_97 = _T_73 & _T_78; // @[Misc.scala 213:27]
  assign _T_98 = _T_50[0] & _T_97; // @[Misc.scala 214:38]
  assign _T_99 = _T_75 | _T_98; // @[Misc.scala 214:29]
  assign _T_100 = _T_73 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_101 = _T_50[0] & _T_100; // @[Misc.scala 214:38]
  assign _T_102 = _T_75 | _T_101; // @[Misc.scala 214:29]
  assign _T_109 = {_T_102,_T_99,_T_96,_T_93,_T_90,_T_87,_T_84,_T_81}; // @[Cat.scala 29:58]
  assign _T_120 = {1'b0,$signed(io_in_a_bits_address)}; // @[Parameters.scala 137:49]
  assign _T_176 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_178 = io_in_a_bits_address ^ 36'h400000000; // @[Parameters.scala 137:31]
  assign _T_179 = {1'b0,$signed(_T_178)}; // @[Parameters.scala 137:49]
  assign _T_181 = $signed(_T_179) & -37'sh400000000; // @[Parameters.scala 137:52]
  assign _T_182 = $signed(_T_181) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_183 = io_in_a_bits_address ^ 36'h8000000; // @[Parameters.scala 137:31]
  assign _T_184 = {1'b0,$signed(_T_183)}; // @[Parameters.scala 137:49]
  assign _T_188 = io_in_a_bits_address ^ 36'h3000; // @[Parameters.scala 137:31]
  assign _T_189 = {1'b0,$signed(_T_188)}; // @[Parameters.scala 137:49]
  assign _T_191 = $signed(_T_189) & -37'sh10001000; // @[Parameters.scala 137:52]
  assign _T_192 = $signed(_T_191) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_193 = io_in_a_bits_address ^ 36'hc000000; // @[Parameters.scala 137:31]
  assign _T_194 = {1'b0,$signed(_T_193)}; // @[Parameters.scala 137:49]
  assign _T_196 = $signed(_T_194) & -37'sh4000000; // @[Parameters.scala 137:52]
  assign _T_197 = $signed(_T_196) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_198 = io_in_a_bits_address ^ 36'h2000000; // @[Parameters.scala 137:31]
  assign _T_199 = {1'b0,$signed(_T_198)}; // @[Parameters.scala 137:49]
  assign _T_201 = $signed(_T_199) & -37'sh10000; // @[Parameters.scala 137:52]
  assign _T_202 = $signed(_T_201) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_206 = $signed(_T_120) & -37'sh10001000; // @[Parameters.scala 137:52]
  assign _T_207 = $signed(_T_206) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_208 = io_in_a_bits_address ^ 36'h20000000; // @[Parameters.scala 137:31]
  assign _T_209 = {1'b0,$signed(_T_208)}; // @[Parameters.scala 137:49]
  assign _T_211 = $signed(_T_209) & -37'sh20000000; // @[Parameters.scala 137:52]
  assign _T_212 = $signed(_T_211) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_213 = io_in_a_bits_address ^ 36'h10001000; // @[Parameters.scala 137:31]
  assign _T_214 = {1'b0,$signed(_T_213)}; // @[Parameters.scala 137:49]
  assign _T_216 = $signed(_T_214) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_217 = $signed(_T_216) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_218 = io_in_a_bits_address ^ 36'h4000; // @[Parameters.scala 137:31]
  assign _T_219 = {1'b0,$signed(_T_218)}; // @[Parameters.scala 137:49]
  assign _T_221 = $signed(_T_219) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_222 = $signed(_T_221) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_223 = io_in_a_bits_address ^ 36'h2010000; // @[Parameters.scala 137:31]
  assign _T_224 = {1'b0,$signed(_T_223)}; // @[Parameters.scala 137:49]
  assign _T_226 = $signed(_T_224) & -37'sh4000; // @[Parameters.scala 137:52]
  assign _T_227 = $signed(_T_226) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_238 = 4'h6 == io_in_a_bits_size; // @[Parameters.scala 92:48]
  assign _T_240 = io_in_a_bits_address ^ 36'ha000000; // @[Parameters.scala 137:31]
  assign _T_241 = {1'b0,$signed(_T_240)}; // @[Parameters.scala 137:49]
  assign _T_243 = $signed(_T_241) & -37'sh200000; // @[Parameters.scala 137:52]
  assign _T_244 = $signed(_T_243) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_245 = io_in_a_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  assign _T_246 = {1'b0,$signed(_T_245)}; // @[Parameters.scala 137:49]
  assign _T_248 = $signed(_T_246) & -37'sh800000000; // @[Parameters.scala 137:52]
  assign _T_249 = $signed(_T_248) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_250 = _T_244 | _T_249; // @[Parameters.scala 552:42]
  assign _T_251 = _T_238 & _T_250; // @[Parameters.scala 551:56]
  assign _T_255 = _T_251 | reset; // @[Monitor.scala 44:11]
  assign _T_256 = ~_T_255; // @[Monitor.scala 44:11]
  assign _T_259 = ~reset; // @[Monitor.scala 44:11]
  assign _T_261 = _T_39 | reset; // @[Monitor.scala 44:11]
  assign _T_262 = ~_T_261; // @[Monitor.scala 44:11]
  assign _T_265 = _T_51 | reset; // @[Monitor.scala 44:11]
  assign _T_266 = ~_T_265; // @[Monitor.scala 44:11]
  assign _T_268 = _T_45 | reset; // @[Monitor.scala 44:11]
  assign _T_269 = ~_T_268; // @[Monitor.scala 44:11]
  assign _T_270 = io_in_a_bits_param <= 3'h2; // @[Bundles.scala 110:27]
  assign _T_272 = _T_270 | reset; // @[Monitor.scala 44:11]
  assign _T_273 = ~_T_272; // @[Monitor.scala 44:11]
  assign _T_274 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_275 = _T_274 == 8'h0; // @[Monitor.scala 86:31]
  assign _T_277 = _T_275 | reset; // @[Monitor.scala 44:11]
  assign _T_278 = ~_T_277; // @[Monitor.scala 44:11]
  assign _T_279 = ~io_in_a_bits_corrupt; // @[Monitor.scala 87:18]
  assign _T_281 = _T_279 | reset; // @[Monitor.scala 44:11]
  assign _T_282 = ~_T_281; // @[Monitor.scala 44:11]
  assign _T_283 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_381 = io_in_a_bits_param != 3'h0; // @[Monitor.scala 97:31]
  assign _T_383 = _T_381 | reset; // @[Monitor.scala 44:11]
  assign _T_384 = ~_T_383; // @[Monitor.scala 44:11]
  assign _T_394 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_396 = io_in_a_bits_size <= 4'h6; // @[Parameters.scala 93:42]
  assign _T_407 = $signed(_T_184) & -37'sh2200000; // @[Parameters.scala 137:52]
  assign _T_408 = $signed(_T_407) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_437 = $signed(_T_214) & -37'sh3000; // @[Parameters.scala 137:52]
  assign _T_438 = $signed(_T_437) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_449 = _T_182 | _T_408; // @[Parameters.scala 552:42]
  assign _T_450 = _T_449 | _T_249; // @[Parameters.scala 552:42]
  assign _T_451 = _T_450 | _T_197; // @[Parameters.scala 552:42]
  assign _T_452 = _T_451 | _T_202; // @[Parameters.scala 552:42]
  assign _T_453 = _T_452 | _T_207; // @[Parameters.scala 552:42]
  assign _T_454 = _T_453 | _T_212; // @[Parameters.scala 552:42]
  assign _T_455 = _T_454 | _T_438; // @[Parameters.scala 552:42]
  assign _T_456 = _T_455 | _T_222; // @[Parameters.scala 552:42]
  assign _T_457 = _T_456 | _T_227; // @[Parameters.scala 552:42]
  assign _T_458 = _T_396 & _T_457; // @[Parameters.scala 551:56]
  assign _T_460 = io_in_a_bits_size <= 4'h8; // @[Parameters.scala 93:42]
  assign _T_466 = $signed(_T_189) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_467 = $signed(_T_466) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_468 = _T_460 & _T_467; // @[Parameters.scala 551:56]
  assign _T_470 = _T_458 | _T_468; // @[Parameters.scala 553:30]
  assign _T_472 = _T_470 | reset; // @[Monitor.scala 44:11]
  assign _T_473 = ~_T_472; // @[Monitor.scala 44:11]
  assign _T_480 = io_in_a_bits_param == 3'h0; // @[Monitor.scala 106:31]
  assign _T_482 = _T_480 | reset; // @[Monitor.scala 44:11]
  assign _T_483 = ~_T_482; // @[Monitor.scala 44:11]
  assign _T_484 = io_in_a_bits_mask == _T_109; // @[Monitor.scala 107:30]
  assign _T_486 = _T_484 | reset; // @[Monitor.scala 44:11]
  assign _T_487 = ~_T_486; // @[Monitor.scala 44:11]
  assign _T_492 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_494 = io_in_a_bits_size <= 4'h7; // @[Parameters.scala 93:42]
  assign _T_502 = _T_494 & _T_182; // @[Parameters.scala 551:56]
  assign _T_552 = _T_408 | _T_249; // @[Parameters.scala 552:42]
  assign _T_553 = _T_552 | _T_197; // @[Parameters.scala 552:42]
  assign _T_554 = _T_553 | _T_202; // @[Parameters.scala 552:42]
  assign _T_555 = _T_554 | _T_207; // @[Parameters.scala 552:42]
  assign _T_556 = _T_555 | _T_212; // @[Parameters.scala 552:42]
  assign _T_557 = _T_556 | _T_438; // @[Parameters.scala 552:42]
  assign _T_558 = _T_557 | _T_222; // @[Parameters.scala 552:42]
  assign _T_559 = _T_558 | _T_227; // @[Parameters.scala 552:42]
  assign _T_560 = _T_396 & _T_559; // @[Parameters.scala 551:56]
  assign _T_572 = _T_502 | _T_560; // @[Parameters.scala 553:30]
  assign _T_573 = _T_572 | _T_468; // @[Parameters.scala 553:30]
  assign _T_575 = _T_573 | reset; // @[Monitor.scala 44:11]
  assign _T_576 = ~_T_575; // @[Monitor.scala 44:11]
  assign _T_591 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_686 = ~_T_109; // @[Monitor.scala 124:33]
  assign _T_687 = io_in_a_bits_mask & _T_686; // @[Monitor.scala 124:31]
  assign _T_688 = _T_687 == 8'h0; // @[Monitor.scala 124:40]
  assign _T_690 = _T_688 | reset; // @[Monitor.scala 44:11]
  assign _T_691 = ~_T_690; // @[Monitor.scala 44:11]
  assign _T_692 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_701 = io_in_a_bits_size <= 4'h3; // @[Parameters.scala 93:42]
  assign _T_755 = _T_552 | _T_192; // @[Parameters.scala 552:42]
  assign _T_756 = _T_755 | _T_197; // @[Parameters.scala 552:42]
  assign _T_757 = _T_756 | _T_202; // @[Parameters.scala 552:42]
  assign _T_758 = _T_757 | _T_207; // @[Parameters.scala 552:42]
  assign _T_759 = _T_758 | _T_212; // @[Parameters.scala 552:42]
  assign _T_760 = _T_759 | _T_217; // @[Parameters.scala 552:42]
  assign _T_761 = _T_760 | _T_222; // @[Parameters.scala 552:42]
  assign _T_762 = _T_761 | _T_227; // @[Parameters.scala 552:42]
  assign _T_763 = _T_701 & _T_762; // @[Parameters.scala 551:56]
  assign _T_767 = _T_763 | reset; // @[Monitor.scala 44:11]
  assign _T_768 = ~_T_767; // @[Monitor.scala 44:11]
  assign _T_775 = io_in_a_bits_param <= 3'h4; // @[Bundles.scala 140:33]
  assign _T_777 = _T_775 | reset; // @[Monitor.scala 44:11]
  assign _T_778 = ~_T_777; // @[Monitor.scala 44:11]
  assign _T_783 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_866 = io_in_a_bits_param <= 3'h3; // @[Bundles.scala 147:30]
  assign _T_868 = _T_866 | reset; // @[Monitor.scala 44:11]
  assign _T_869 = ~_T_868; // @[Monitor.scala 44:11]
  assign _T_874 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_945 = _T_396 & _T_250; // @[Parameters.scala 551:56]
  assign _T_958 = _T_945 | _T_468; // @[Parameters.scala 553:30]
  assign _T_960 = _T_958 | reset; // @[Monitor.scala 44:11]
  assign _T_961 = ~_T_960; // @[Monitor.scala 44:11]
  assign _T_968 = io_in_a_bits_param <= 3'h1; // @[Bundles.scala 160:28]
  assign _T_970 = _T_968 | reset; // @[Monitor.scala 44:11]
  assign _T_971 = ~_T_970; // @[Monitor.scala 44:11]
  assign _T_980 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_982 = _T_980 | reset; // @[Monitor.scala 51:11]
  assign _T_983 = ~_T_982; // @[Monitor.scala 51:11]
  assign _T_987 = io_in_d_bits_source[4:3] == 2'h0; // @[Parameters.scala 55:32]
  assign _T_995 = io_in_d_bits_source[4:3] == 2'h1; // @[Parameters.scala 55:32]
  assign _T_1003 = io_in_d_bits_source[4:3] == 2'h2; // @[Parameters.scala 55:32]
  assign _T_1011 = io_in_d_bits_source[4:3] == 2'h3; // @[Parameters.scala 55:32]
  assign _T_1017 = _T_987 | _T_995; // @[Parameters.scala 924:46]
  assign _T_1018 = _T_1017 | _T_1003; // @[Parameters.scala 924:46]
  assign _T_1019 = _T_1018 | _T_1011; // @[Parameters.scala 924:46]
  assign _T_1021 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_1023 = _T_1019 | reset; // @[Monitor.scala 51:11]
  assign _T_1024 = ~_T_1023; // @[Monitor.scala 51:11]
  assign _T_1025 = io_in_d_bits_size >= 4'h3; // @[Monitor.scala 309:27]
  assign _T_1027 = _T_1025 | reset; // @[Monitor.scala 51:11]
  assign _T_1028 = ~_T_1027; // @[Monitor.scala 51:11]
  assign _T_1029 = io_in_d_bits_param == 2'h0; // @[Monitor.scala 310:28]
  assign _T_1031 = _T_1029 | reset; // @[Monitor.scala 51:11]
  assign _T_1032 = ~_T_1031; // @[Monitor.scala 51:11]
  assign _T_1033 = ~io_in_d_bits_corrupt; // @[Monitor.scala 311:15]
  assign _T_1035 = _T_1033 | reset; // @[Monitor.scala 51:11]
  assign _T_1036 = ~_T_1035; // @[Monitor.scala 51:11]
  assign _T_1037 = ~io_in_d_bits_denied; // @[Monitor.scala 312:15]
  assign _T_1039 = _T_1037 | reset; // @[Monitor.scala 51:11]
  assign _T_1040 = ~_T_1039; // @[Monitor.scala 51:11]
  assign _T_1041 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_1052 = io_in_d_bits_param <= 2'h2; // @[Bundles.scala 104:26]
  assign _T_1054 = _T_1052 | reset; // @[Monitor.scala 51:11]
  assign _T_1055 = ~_T_1054; // @[Monitor.scala 51:11]
  assign _T_1056 = io_in_d_bits_param != 2'h2; // @[Monitor.scala 320:28]
  assign _T_1058 = _T_1056 | reset; // @[Monitor.scala 51:11]
  assign _T_1059 = ~_T_1058; // @[Monitor.scala 51:11]
  assign _T_1069 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_1089 = _T_1037 | io_in_d_bits_corrupt; // @[Monitor.scala 331:30]
  assign _T_1091 = _T_1089 | reset; // @[Monitor.scala 51:11]
  assign _T_1092 = ~_T_1091; // @[Monitor.scala 51:11]
  assign _T_1098 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_1115 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_1133 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_1165 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_1172 = ~io_in_a_bits_opcode[2]; // @[Edges.scala 93:28]
  assign _T_1176 = _T_1174 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1177 = _T_1174 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1190 = ~_T_1177; // @[Monitor.scala 386:22]
  assign _T_1191 = io_in_a_valid & _T_1190; // @[Monitor.scala 386:19]
  assign _T_1192 = io_in_a_bits_opcode == _T_1185; // @[Monitor.scala 387:32]
  assign _T_1194 = _T_1192 | reset; // @[Monitor.scala 44:11]
  assign _T_1195 = ~_T_1194; // @[Monitor.scala 44:11]
  assign _T_1196 = io_in_a_bits_param == _T_1186; // @[Monitor.scala 388:32]
  assign _T_1198 = _T_1196 | reset; // @[Monitor.scala 44:11]
  assign _T_1199 = ~_T_1198; // @[Monitor.scala 44:11]
  assign _T_1200 = io_in_a_bits_size == _T_1187; // @[Monitor.scala 389:32]
  assign _T_1202 = _T_1200 | reset; // @[Monitor.scala 44:11]
  assign _T_1203 = ~_T_1202; // @[Monitor.scala 44:11]
  assign _T_1204 = io_in_a_bits_source == _T_1188; // @[Monitor.scala 390:32]
  assign _T_1206 = _T_1204 | reset; // @[Monitor.scala 44:11]
  assign _T_1207 = ~_T_1206; // @[Monitor.scala 44:11]
  assign _T_1208 = io_in_a_bits_address == _T_1189; // @[Monitor.scala 391:32]
  assign _T_1210 = _T_1208 | reset; // @[Monitor.scala 44:11]
  assign _T_1211 = ~_T_1210; // @[Monitor.scala 44:11]
  assign _T_1213 = _T_1165 & _T_1177; // @[Monitor.scala 393:20]
  assign _T_1214 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_1216 = 23'hff << io_in_d_bits_size; // @[package.scala 189:77]
  assign _T_1218 = ~_T_1216[7:0]; // @[package.scala 189:46]
  assign _T_1224 = _T_1222 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1225 = _T_1222 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1239 = ~_T_1225; // @[Monitor.scala 538:22]
  assign _T_1240 = io_in_d_valid & _T_1239; // @[Monitor.scala 538:19]
  assign _T_1241 = io_in_d_bits_opcode == _T_1233; // @[Monitor.scala 539:29]
  assign _T_1243 = _T_1241 | reset; // @[Monitor.scala 51:11]
  assign _T_1244 = ~_T_1243; // @[Monitor.scala 51:11]
  assign _T_1245 = io_in_d_bits_param == _T_1234; // @[Monitor.scala 540:29]
  assign _T_1247 = _T_1245 | reset; // @[Monitor.scala 51:11]
  assign _T_1248 = ~_T_1247; // @[Monitor.scala 51:11]
  assign _T_1249 = io_in_d_bits_size == _T_1235; // @[Monitor.scala 541:29]
  assign _T_1251 = _T_1249 | reset; // @[Monitor.scala 51:11]
  assign _T_1252 = ~_T_1251; // @[Monitor.scala 51:11]
  assign _T_1253 = io_in_d_bits_source == _T_1236; // @[Monitor.scala 542:29]
  assign _T_1255 = _T_1253 | reset; // @[Monitor.scala 51:11]
  assign _T_1256 = ~_T_1255; // @[Monitor.scala 51:11]
  assign _T_1257 = io_in_d_bits_sink == _T_1237; // @[Monitor.scala 543:29]
  assign _T_1259 = _T_1257 | reset; // @[Monitor.scala 51:11]
  assign _T_1260 = ~_T_1259; // @[Monitor.scala 51:11]
  assign _T_1261 = io_in_d_bits_denied == _T_1238; // @[Monitor.scala 544:29]
  assign _T_1263 = _T_1261 | reset; // @[Monitor.scala 51:11]
  assign _T_1264 = ~_T_1263; // @[Monitor.scala 51:11]
  assign _T_1266 = _T_1214 & _T_1225; // @[Monitor.scala 546:20]
  assign _T_1279 = _T_1277 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1280 = _T_1277 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1298 = _T_1296 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1299 = _T_1296 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1309 = _T_1165 & _T_1280; // @[Monitor.scala 574:27]
  assign _T_1311 = 32'h1 << io_in_a_bits_source; // @[OneHot.scala 58:35]
  assign _T_1312 = _T_1267 >> io_in_a_bits_source; // @[Monitor.scala 576:23]
  assign _T_1314 = ~_T_1312[0]; // @[Monitor.scala 576:14]
  assign _T_1316 = _T_1314 | reset; // @[Monitor.scala 576:13]
  assign _T_1317 = ~_T_1316; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_1309 ? _T_1311 : 32'h0; // @[Monitor.scala 574:72]
  assign _T_1321 = _T_1214 & _T_1299; // @[Monitor.scala 581:27]
  assign _T_1323 = ~_T_1021; // @[Monitor.scala 581:75]
  assign _T_1324 = _T_1321 & _T_1323; // @[Monitor.scala 581:72]
  assign _T_1325 = 32'h1 << io_in_d_bits_source; // @[OneHot.scala 58:35]
  assign _T_1326 = _GEN_15 | _T_1267; // @[Monitor.scala 583:21]
  assign _T_1327 = _T_1326 >> io_in_d_bits_source; // @[Monitor.scala 583:32]
  assign _T_1330 = _T_1327[0] | reset; // @[Monitor.scala 51:11]
  assign _T_1331 = ~_T_1330; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_1324 ? _T_1325 : 32'h0; // @[Monitor.scala 581:91]
  assign _T_1332 = _GEN_15 != _GEN_16; // @[Monitor.scala 587:20]
  assign _T_1333 = _GEN_15 != 32'h0; // @[Monitor.scala 587:40]
  assign _T_1334 = ~_T_1333; // @[Monitor.scala 587:33]
  assign _T_1335 = _T_1332 | _T_1334; // @[Monitor.scala 587:30]
  assign _T_1337 = _T_1335 | reset; // @[Monitor.scala 51:11]
  assign _T_1338 = ~_T_1337; // @[Monitor.scala 51:11]
  assign _T_1339 = _T_1267 | _GEN_15; // @[Monitor.scala 590:27]
  assign _T_1340 = ~_GEN_16; // @[Monitor.scala 590:38]
  assign _T_1341 = _T_1339 & _T_1340; // @[Monitor.scala 590:36]
  assign _T_1343 = _T_1267 != 32'h0; // @[Monitor.scala 595:23]
  assign _T_1344 = ~_T_1343; // @[Monitor.scala 595:13]
  assign _T_1345 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_1346 = _T_1344 | _T_1345; // @[Monitor.scala 595:27]
  assign _T_1347 = _T_1342 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_1348 = _T_1346 | _T_1347; // @[Monitor.scala 595:44]
  assign _T_1350 = _T_1348 | reset; // @[Monitor.scala 595:12]
  assign _T_1351 = ~_T_1350; // @[Monitor.scala 595:12]
  assign _T_1353 = _T_1342 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_1356 = _T_1165 | _T_1214; // @[Monitor.scala 598:27]
  assign _GEN_19 = io_in_a_valid & _T_176; // @[Monitor.scala 44:11]
  assign _GEN_35 = io_in_a_valid & _T_283; // @[Monitor.scala 44:11]
  assign _GEN_53 = io_in_a_valid & _T_394; // @[Monitor.scala 44:11]
  assign _GEN_65 = io_in_a_valid & _T_492; // @[Monitor.scala 44:11]
  assign _GEN_75 = io_in_a_valid & _T_591; // @[Monitor.scala 44:11]
  assign _GEN_85 = io_in_a_valid & _T_692; // @[Monitor.scala 44:11]
  assign _GEN_95 = io_in_a_valid & _T_783; // @[Monitor.scala 44:11]
  assign _GEN_105 = io_in_a_valid & _T_874; // @[Monitor.scala 44:11]
  assign _GEN_117 = io_in_d_valid & _T_1021; // @[Monitor.scala 51:11]
  assign _GEN_127 = io_in_d_valid & _T_1041; // @[Monitor.scala 51:11]
  assign _GEN_137 = io_in_d_valid & _T_1069; // @[Monitor.scala 51:11]
  assign _GEN_147 = io_in_d_valid & _T_1098; // @[Monitor.scala 51:11]
  assign _GEN_153 = io_in_d_valid & _T_1115; // @[Monitor.scala 51:11]
  assign _GEN_159 = io_in_d_valid & _T_1133; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_1174 = _RAND_0[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_1185 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_1186 = _RAND_2[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_1187 = _RAND_3[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_1188 = _RAND_4[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {2{`RANDOM}};
  _T_1189 = _RAND_5[35:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_1222 = _RAND_6[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_1233 = _RAND_7[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_1234 = _RAND_8[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_1235 = _RAND_9[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_1236 = _RAND_10[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_1237 = _RAND_11[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_1238 = _RAND_12[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  _T_1267 = _RAND_13[31:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_14 = {1{`RANDOM}};
  _T_1277 = _RAND_14[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_15 = {1{`RANDOM}};
  _T_1296 = _RAND_15[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_16 = {1{`RANDOM}};
  _T_1342 = _RAND_16[31:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_1174 <= 5'h0;
    end else if (_T_1165) begin
      if (_T_1177) begin
        if (_T_1172) begin
          _T_1174 <= _T_43[7:3];
        end else begin
          _T_1174 <= 5'h0;
        end
      end else begin
        _T_1174 <= _T_1176;
      end
    end
    if (_T_1213) begin
      _T_1185 <= io_in_a_bits_opcode;
    end
    if (_T_1213) begin
      _T_1186 <= io_in_a_bits_param;
    end
    if (_T_1213) begin
      _T_1187 <= io_in_a_bits_size;
    end
    if (_T_1213) begin
      _T_1188 <= io_in_a_bits_source;
    end
    if (_T_1213) begin
      _T_1189 <= io_in_a_bits_address;
    end
    if (reset) begin
      _T_1222 <= 5'h0;
    end else if (_T_1214) begin
      if (_T_1225) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1222 <= _T_1218[7:3];
        end else begin
          _T_1222 <= 5'h0;
        end
      end else begin
        _T_1222 <= _T_1224;
      end
    end
    if (_T_1266) begin
      _T_1233 <= io_in_d_bits_opcode;
    end
    if (_T_1266) begin
      _T_1234 <= io_in_d_bits_param;
    end
    if (_T_1266) begin
      _T_1235 <= io_in_d_bits_size;
    end
    if (_T_1266) begin
      _T_1236 <= io_in_d_bits_source;
    end
    if (_T_1266) begin
      _T_1237 <= io_in_d_bits_sink;
    end
    if (_T_1266) begin
      _T_1238 <= io_in_d_bits_denied;
    end
    if (reset) begin
      _T_1267 <= 32'h0;
    end else begin
      _T_1267 <= _T_1341;
    end
    if (reset) begin
      _T_1277 <= 5'h0;
    end else if (_T_1165) begin
      if (_T_1280) begin
        if (_T_1172) begin
          _T_1277 <= _T_43[7:3];
        end else begin
          _T_1277 <= 5'h0;
        end
      end else begin
        _T_1277 <= _T_1279;
      end
    end
    if (reset) begin
      _T_1296 <= 5'h0;
    end else if (_T_1214) begin
      if (_T_1299) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1296 <= _T_1218[7:3];
        end else begin
          _T_1296 <= 5'h0;
        end
      end else begin
        _T_1296 <= _T_1298;
      end
    end
    if (reset) begin
      _T_1342 <= 32'h0;
    end else if (_T_1356) begin
      _T_1342 <= 32'h0;
    end else begin
      _T_1342 <= _T_1353;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_256) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_256) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_259) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_259) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_262) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid source ID (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_262) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_266) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock smaller than a beat (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_266) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_269) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_269) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_273) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid grow param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_273) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_278) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_278) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_282) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock is corrupt (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_282) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_256) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_256) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_259) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_259) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_262) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid source ID (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_262) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_266) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm smaller than a beat (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_266) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_269) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_269) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_273) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid grow param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_273) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_384) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_384) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_278) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_278) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_282) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm is corrupt (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_282) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_473) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_473) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_262) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid source ID (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_262) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_269) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_269) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_483) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_483) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_487) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_487) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_282) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get is corrupt (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_282) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_576) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_576) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_262) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid source ID (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_262) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_269) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_269) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_483) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_483) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_487) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_487) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_576) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_576) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_262) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid source ID (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_262) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_269) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_269) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_483) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_483) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_691) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial contains invalid mask (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_691) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_768) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_768) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_262) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid source ID (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_262) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_269) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_269) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_778) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid opcode param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_778) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_487) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_487) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_768) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_768) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_262) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid source ID (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_262) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_269) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_269) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_869) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid opcode param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_869) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_487) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_487) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_961) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_961) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_262) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid source ID (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_262) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_269) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_269) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_971) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid opcode param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_971) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_487) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_487) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_282) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint is corrupt (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_282) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_983) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_983) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_1024) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck carries invalid source ID (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_1024) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_1028) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_1028) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_1032) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseeAck carries invalid param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_1032) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_1036) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is corrupt (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_1036) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_1040) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is denied (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_1040) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_1024) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid source ID (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_1024) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_1028) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_1028) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_1055) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid cap param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_1055) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_1059) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries toN param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_1059) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_1036) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is corrupt (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_1036) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_137 & _T_1024) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid source ID (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_137 & _T_1024) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_137 & _T_1028) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_137 & _T_1028) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_137 & _T_1055) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid cap param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_137 & _T_1055) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_137 & _T_1059) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries toN param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_137 & _T_1059) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_137 & _T_1092) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied but not corrupt (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_137 & _T_1092) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_147 & _T_1024) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid source ID (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_147 & _T_1024) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_147 & _T_1032) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_147 & _T_1032) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_147 & _T_1036) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is corrupt (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_147 & _T_1036) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_153 & _T_1024) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid source ID (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_153 & _T_1024) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_153 & _T_1032) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_153 & _T_1032) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_153 & _T_1092) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied but not corrupt (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_153 & _T_1092) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_159 & _T_1024) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid source ID (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_159 & _T_1024) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_159 & _T_1032) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid param (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_159 & _T_1032) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_159 & _T_1036) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is corrupt (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_159 & _T_1036) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1191 & _T_1195) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1191 & _T_1195) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1191 & _T_1199) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel param changed within multibeat operation (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1191 & _T_1199) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1191 & _T_1203) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel size changed within multibeat operation (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1191 & _T_1203) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1191 & _T_1207) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel source changed within multibeat operation (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1191 & _T_1207) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1191 & _T_1211) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1191 & _T_1211) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1240 & _T_1244) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1240 & _T_1244) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1240 & _T_1248) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel param changed within multibeat operation (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1240 & _T_1248) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1240 & _T_1252) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1240 & _T_1252) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1240 & _T_1256) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel source changed within multibeat operation (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1240 & _T_1256) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1240 & _T_1260) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel sink changed with multibeat operation (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1240 & _T_1260) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1240 & _T_1264) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1240 & _T_1264) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1309 & _T_1317) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1309 & _T_1317) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1324 & _T_1331) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1324 & _T_1331) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1338) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' and 'D' concurrent, despite minlatency 4 (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1338) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1351) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at AXI4Bridge.scala:129:9)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1351) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
