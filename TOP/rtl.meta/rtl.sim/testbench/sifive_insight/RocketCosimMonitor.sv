//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159

`ifdef USE_COSIM_DPI
`ifndef SYNTHESIS
`include "cosim/cosim_monitor_cfg.svh"
`endif
`endif

module RocketCosimMonitor
#(parameter XLEN=64, FLEN=64, COMMIT_WIDTH=4, INST_WIDTH=32, WDATA_WIDTH=64)(
  input bit                                    clock,
  input bit                                    reset,
  input bit [(XLEN)-1:0]                       hartid,
  input bit [(COMMIT_WIDTH)-1:0]               valid,
  input bit [(COMMIT_WIDTH*32)-1:0]            timer,
  input bit [(COMMIT_WIDTH*XLEN)-1:0]          pc,
  input bit [(COMMIT_WIDTH*INST_WIDTH)-1:0]    inst,
  input bit [(COMMIT_WIDTH)-1:0]               instxd,
  input bit [(COMMIT_WIDTH)-1:0]               instfd,
  input bit [(COMMIT_WIDTH*WDATA_WIDTH)-1:0]   wdata,
  input bit [(COMMIT_WIDTH*5)-1:0]             wrdst,
  input bit [(COMMIT_WIDTH)-1:0]               wrenx,
  input bit [(COMMIT_WIDTH)-1:0]               wrenf,
  input bit [(XLEN)-1:0]                       excphartid,
  input bit [31:0]                             excptimer,
  input bit                                    exception,
  input bit                                    interrupt,
  input bit [(XLEN)-1:0]                       mcause,
  input bit [(XLEN)-1:0]                       mtvec,
  input bit [(XLEN)-1:0]                       mstatus,
  input bit [31:0]                             prv
);
`ifdef USE_DPI
`ifdef USE_COSIM_DPI
`ifndef SYNTHESIS
  sifive_objections_pkg::run_phase_t current_phase = sifive_objections_pkg::invalid_e;
  sifive_objections_pkg::run_phase_t next_phase;
  CosimScoreboardCfg   scoreboard_cfg;
  string               instance_path = $sformatf("%m");
  string               cosim_cfg_file = "";
  int                  objection_id;
  int                  result_id;
  bit                  objection_dropped;
  bit                  cosim_en = 1'b0;
  bit                  fail_seen;
  logic [(XLEN)-1:0]   prev_mcause;
  //Store the dut information and send it to sb 1 cycle later since csr updates are delayed by a cycle
  DutInstInfo          prev_dut_inst[COMMIT_WIDTH];
  DutExcpInfo          prev_dut_excp;
  DutCsrInfo           csrinfo;

  initial begin
    int cosim_scoreboard_enable_check;
    int cosim_scoreboard_verbosity;
    int cosim_scoreboard_wrdata_check;
    int cosim_scoreboard_error_count;
    int cosim_scoreboard_stall_cycle_count;

    // Main arg to enable/disable the cosim scoreboard environment
    if ($test$plusargs("enable_spike_cosim")) cosim_en = 1'b1;
    else cosim_en = 1'b0;

    // Config file used for spike-cosim
    if (!$value$plusargs("cosim_cfg_file=%s", cosim_cfg_file)) cosim_cfg_file="";

    // Cosim scoreboard configs:
    // cosim_scoreboard_enable_check: 0 = do not error out on mismatch, 1 = fail on mismatch
    // cosim_scoreboard_verbosity: 0 = none, 1 = low, 2 = med, 3 = high
    if (!$value$plusargs("cosim_scoreboard_enable_check=%d", cosim_scoreboard_enable_check)) cosim_scoreboard_enable_check=1;
    if (!$value$plusargs("cosim_scoreboard_verbosity=%d", cosim_scoreboard_verbosity)) cosim_scoreboard_verbosity = 0;
    if (!$value$plusargs("cosim_scoreboard_wrdata_check=%d", cosim_scoreboard_wrdata_check)) cosim_scoreboard_wrdata_check=1;
    if (!$value$plusargs("cosim_scoreboard_error_count=%d", cosim_scoreboard_error_count)) cosim_scoreboard_error_count=1;
    if (!$value$plusargs("cosim_scoreboard_stall_cycle_count=%d", cosim_scoreboard_stall_cycle_count)) cosim_scoreboard_stall_cycle_count=10000;
    scoreboard_cfg.stall_cycle_count = cosim_scoreboard_stall_cycle_count;
    scoreboard_cfg.enable_check = cosim_scoreboard_enable_check;
    scoreboard_cfg.verbosity = cosim_scoreboard_verbosity;
    scoreboard_cfg.enable_wrdata_check = cosim_scoreboard_wrdata_check;
    scoreboard_cfg.error_count = cosim_scoreboard_error_count;
    scoreboard_cfg.xLen = XLEN;
    scoreboard_cfg.fLen = FLEN;
    scoreboard_cfg.vLen = 0;
    scoreboard_cfg.vec_reg_count = 0;

    if (!create_cosim_scoreboard_env(cosim_cfg_file, scoreboard_cfg)) begin
      $display("FAILED to initialize a cosim object!");
      $fatal;
    end

    result_id = sifive_test_results_pkg::register(instance_path);
    objection_id = sifive_objections_pkg::register(instance_path);
    sifive_objections_pkg::raise_objection(objection_id, sifive_objections_pkg::main_e);
  end

  // Set phase for cosim scoreboard
  always @ (posedge clock) begin
    next_phase = objection_dropped ? sifive_objections_pkg::shutdown_e : sifive_objections_pkg::get_current_phase();
    if (current_phase != next_phase) begin
       current_phase <= next_phase;
       set_current_phase(next_phase, next_phase.name());
    end
  end

  always @ (negedge clock) begin
    if (!reset && cosim_en && !objection_dropped) begin
      // Send CSR info to scoreboard every clock
      csrinfo.dut_hartid = hartid;
      csrinfo.dut_time = $time;
      csrinfo.dut_csr_timer = timer[31:0];
      csrinfo.dut_prv = prv;
      csrinfo.dut_mstatus = mstatus;
      csrinfo.dut_mtvec = mtvec;
      csrinfo.dut_mcause = prev_mcause;
      csrinfo.dut_vstart = 0;
      csrinfo.dut_vl = 0;
      if (!put_dut_csr_info(csrinfo)) begin
        $display("Cosim Monitor could not send packet to Scoreboard!");
        $fatal;
      end

      for (int i = 0; i < COMMIT_WIDTH; i = i + 1) begin
        if (prev_dut_inst[i].dut_valid || prev_dut_inst[i].dut_wrenx || prev_dut_inst[i].dut_wrenf) begin
          if (!put_dut_inst_info(prev_dut_inst[i])) begin
            $display("Cosim Monitor could not send packet to Scoreboard!");
            $fatal;
          end
          prev_dut_inst[i] = '0;
        end
      end

      if (prev_dut_excp.dut_exception || prev_dut_excp.dut_interrupt) begin
        if (!put_dut_excp_info(prev_dut_excp)) begin
          $display("Cosim Monitor could not send packet to Scoreboard!");
          $fatal;
        end
        prev_dut_excp = '0;
      end

      for (int i = 0; i < COMMIT_WIDTH; i = i + 1) begin
        if (valid[i] || wrenx[i] || wrenf[i]) begin
          DutInstInfo info;
          info.dut_hartid = hartid;
          info.dut_time = $time;
          info.dut_valid = valid[i];
          info.dut_pc = pc[(i*XLEN) +: XLEN];
          info.dut_insn = inst[(i*INST_WIDTH) +: INST_WIDTH];
          info.dut_insnxd = instxd[i];
          info.dut_insnfd = instfd[i];
          info.dut_insnvd = 0;
          info.dut_insnfused = 0;
          info.dut_wdata_s = wdata[(i*WDATA_WIDTH) +: WDATA_WIDTH];
          info.dut_wrdst_s = wrdst[(i*5) +: 5];
          info.dut_wrenx = wrenx[i];
          info.dut_wrenf = wrenf[i];
          prev_dut_inst[i] = info;
        end
      end

      if (exception) begin
        DutExcpInfo info;
        info.dut_hartid = excphartid;
        info.dut_time = $time;
        info.dut_exception = exception;
        info.dut_interrupt = interrupt;
        prev_dut_excp = info;
      end
      prev_mcause = mcause;
    end
    else begin
      prev_dut_excp = '0;
      for (int i = 0; i < COMMIT_WIDTH; i = i + 1) begin
        prev_dut_inst[i] = '0;
      end
    end

    fail_seen = check_fail();
    if (!objection_dropped && (fail_seen || sifive_objections_pkg::get_test_finisher_done())) begin
      sifive_test_results_pkg::result_set(result_id, fail_seen);
      sifive_objections_pkg::drop_objection(objection_id, sifive_objections_pkg::main_e);
      objection_dropped = 1'b1;
    end
  end
`endif
`endif
`endif
endmodule