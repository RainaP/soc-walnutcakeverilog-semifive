//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLXbar_3_assert(
  input         clock,
  input         reset,
  input         auto_in_2_a_valid,
  input  [1:0]  auto_in_2_a_bits_source,
  input  [35:0] auto_in_2_a_bits_address,
  input  [7:0]  auto_in_2_a_bits_mask,
  input         auto_in_2_d_ready,
  input         auto_in_1_a_valid,
  input  [2:0]  auto_in_1_a_bits_opcode,
  input  [2:0]  auto_in_1_a_bits_param,
  input  [3:0]  auto_in_1_a_bits_size,
  input  [4:0]  auto_in_1_a_bits_source,
  input  [35:0] auto_in_1_a_bits_address,
  input  [7:0]  auto_in_1_a_bits_mask,
  input         auto_in_1_a_bits_corrupt,
  input         auto_in_1_d_ready,
  input         auto_in_0_a_valid,
  input  [2:0]  auto_in_0_a_bits_opcode,
  input  [3:0]  auto_in_0_a_bits_size,
  input  [35:0] auto_in_0_a_bits_address,
  input  [7:0]  auto_in_0_a_bits_mask,
  input         auto_in_0_d_ready,
  input         auto_out_a_ready,
  input         auto_out_d_valid,
  input  [2:0]  auto_out_d_bits_opcode,
  input  [1:0]  auto_out_d_bits_param,
  input  [3:0]  auto_out_d_bits_size,
  input  [5:0]  auto_out_d_bits_source,
  input  [3:0]  auto_out_d_bits_sink,
  input         auto_out_d_bits_denied,
  input         auto_out_d_bits_corrupt,
  input         _T_95,
  input         _T_99,
  input         _T_107,
  input         _T_307,
  input         _T_308,
  input         _T_309,
  input         _T_329,
  input         _T_349_0,
  input         _T_349_1,
  input         _T_349_2
);
  wire  TLMonitor_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_d_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_d_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_1_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [4:0] TLMonitor_1_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_1_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_1_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_1_io_in_d_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_1_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [4:0] TLMonitor_1_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_1_io_in_d_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_2_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_2_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_2_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_2_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_2_io_in_d_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_2_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_2_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_2_io_in_d_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  _T_312; // @[Arbiter.scala 74:52]
  wire  _T_313; // @[Arbiter.scala 74:52]
  wire  _T_315; // @[Arbiter.scala 75:62]
  wire  _T_318; // @[Arbiter.scala 75:62]
  wire  _T_319; // @[Arbiter.scala 75:59]
  wire  _T_320; // @[Arbiter.scala 75:56]
  wire  _T_321; // @[Arbiter.scala 75:62]
  wire  _T_322; // @[Arbiter.scala 75:59]
  wire  _T_324; // @[Arbiter.scala 75:77]
  wire  _T_326; // @[Arbiter.scala 75:13]
  wire  _T_327; // @[Arbiter.scala 75:13]
  wire  _T_330; // @[Arbiter.scala 77:15]
  wire  _T_333; // @[Arbiter.scala 77:36]
  wire  _T_335; // @[Arbiter.scala 77:14]
  wire  _T_336; // @[Arbiter.scala 77:14]
  SiFive_TLMonitor_14_assert TLMonitor ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_clock),
    .reset(TLMonitor_reset),
    .io_in_a_ready(TLMonitor_io_in_a_ready),
    .io_in_a_valid(TLMonitor_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_io_in_a_bits_opcode),
    .io_in_a_bits_size(TLMonitor_io_in_a_bits_size),
    .io_in_a_bits_address(TLMonitor_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_io_in_a_bits_mask),
    .io_in_d_ready(TLMonitor_io_in_d_ready),
    .io_in_d_valid(TLMonitor_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_io_in_d_bits_opcode),
    .io_in_d_bits_param(TLMonitor_io_in_d_bits_param),
    .io_in_d_bits_size(TLMonitor_io_in_d_bits_size),
    .io_in_d_bits_sink(TLMonitor_io_in_d_bits_sink),
    .io_in_d_bits_denied(TLMonitor_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_io_in_d_bits_corrupt)
  );
  SiFive_TLMonitor_15_assert TLMonitor_1 ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_1_clock),
    .reset(TLMonitor_1_reset),
    .io_in_a_ready(TLMonitor_1_io_in_a_ready),
    .io_in_a_valid(TLMonitor_1_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_1_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_1_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_1_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_1_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_1_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_1_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_1_io_in_a_bits_corrupt),
    .io_in_d_ready(TLMonitor_1_io_in_d_ready),
    .io_in_d_valid(TLMonitor_1_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_1_io_in_d_bits_opcode),
    .io_in_d_bits_param(TLMonitor_1_io_in_d_bits_param),
    .io_in_d_bits_size(TLMonitor_1_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_1_io_in_d_bits_source),
    .io_in_d_bits_sink(TLMonitor_1_io_in_d_bits_sink),
    .io_in_d_bits_denied(TLMonitor_1_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_1_io_in_d_bits_corrupt)
  );
  SiFive_TLMonitor_16_assert TLMonitor_2 ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_2_clock),
    .reset(TLMonitor_2_reset),
    .io_in_a_ready(TLMonitor_2_io_in_a_ready),
    .io_in_a_valid(TLMonitor_2_io_in_a_valid),
    .io_in_a_bits_source(TLMonitor_2_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_2_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_2_io_in_a_bits_mask),
    .io_in_d_ready(TLMonitor_2_io_in_d_ready),
    .io_in_d_valid(TLMonitor_2_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_2_io_in_d_bits_opcode),
    .io_in_d_bits_param(TLMonitor_2_io_in_d_bits_param),
    .io_in_d_bits_size(TLMonitor_2_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_2_io_in_d_bits_source),
    .io_in_d_bits_sink(TLMonitor_2_io_in_d_bits_sink),
    .io_in_d_bits_denied(TLMonitor_2_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_2_io_in_d_bits_corrupt)
  );
  assign _T_312 = _T_307 | _T_308; // @[Arbiter.scala 74:52]
  assign _T_313 = _T_312 | _T_309; // @[Arbiter.scala 74:52]
  assign _T_315 = ~_T_307; // @[Arbiter.scala 75:62]
  assign _T_318 = ~_T_308; // @[Arbiter.scala 75:62]
  assign _T_319 = _T_315 | _T_318; // @[Arbiter.scala 75:59]
  assign _T_320 = ~_T_312; // @[Arbiter.scala 75:56]
  assign _T_321 = ~_T_309; // @[Arbiter.scala 75:62]
  assign _T_322 = _T_320 | _T_321; // @[Arbiter.scala 75:59]
  assign _T_324 = _T_319 & _T_322; // @[Arbiter.scala 75:77]
  assign _T_326 = _T_324 | reset; // @[Arbiter.scala 75:13]
  assign _T_327 = ~_T_326; // @[Arbiter.scala 75:13]
  assign _T_330 = ~_T_329; // @[Arbiter.scala 77:15]
  assign _T_333 = _T_330 | _T_313; // @[Arbiter.scala 77:36]
  assign _T_335 = _T_333 | reset; // @[Arbiter.scala 77:14]
  assign _T_336 = ~_T_335; // @[Arbiter.scala 77:14]
  assign TLMonitor_clock = clock;
  assign TLMonitor_reset = reset;
  assign TLMonitor_io_in_a_ready = auto_out_a_ready & _T_349_0; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_valid = auto_in_0_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_opcode = auto_in_0_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_size = auto_in_0_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_address = auto_in_0_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_mask = auto_in_0_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_ready = auto_in_0_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_valid = auto_out_d_valid & _T_95; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_param = auto_out_d_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_size = auto_out_d_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_sink = auto_out_d_bits_sink; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_denied = auto_out_d_bits_denied; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_corrupt = auto_out_d_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_1_clock = clock;
  assign TLMonitor_1_reset = reset;
  assign TLMonitor_1_io_in_a_ready = auto_out_a_ready & _T_349_1; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_valid = auto_in_1_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_opcode = auto_in_1_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_param = auto_in_1_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_size = auto_in_1_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_source = auto_in_1_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_address = auto_in_1_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_mask = auto_in_1_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_corrupt = auto_in_1_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_ready = auto_in_1_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_valid = auto_out_d_valid & _T_99; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_param = auto_out_d_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_size = auto_out_d_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_source = auto_out_d_bits_source[4:0]; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_sink = auto_out_d_bits_sink; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_denied = auto_out_d_bits_denied; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_corrupt = auto_out_d_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_2_clock = clock;
  assign TLMonitor_2_reset = reset;
  assign TLMonitor_2_io_in_a_ready = auto_out_a_ready & _T_349_2; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_valid = auto_in_2_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_source = auto_in_2_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_address = auto_in_2_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_mask = auto_in_2_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_ready = auto_in_2_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_valid = auto_out_d_valid & _T_107; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_param = auto_out_d_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_size = auto_out_d_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_source = auto_out_d_bits_source[1:0]; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_sink = auto_out_d_bits_sink; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_denied = auto_out_d_bits_denied; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_corrupt = auto_out_d_bits_corrupt; // @[Nodes.scala 26:19]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_327) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_327) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_336) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_336) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
