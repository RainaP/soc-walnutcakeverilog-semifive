//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__Repeater(
  input         rf_reset,
  input         clock,
  input         reset,
  input         io_repeat,
  output        io_full,
  output        io_enq_ready,
  input         io_enq_valid,
  input  [2:0]  io_enq_bits_opcode,
  input  [2:0]  io_enq_bits_param,
  input  [2:0]  io_enq_bits_size,
  input  [6:0]  io_enq_bits_source,
  input  [27:0] io_enq_bits_address,
  input  [15:0] io_enq_bits_mask,
  input         io_enq_bits_corrupt,
  input         io_deq_ready,
  output        io_deq_valid,
  output [2:0]  io_deq_bits_opcode,
  output [2:0]  io_deq_bits_param,
  output [2:0]  io_deq_bits_size,
  output [6:0]  io_deq_bits_source,
  output [27:0] io_deq_bits_address,
  output [15:0] io_deq_bits_mask,
  output        io_deq_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
`endif // RANDOMIZE_REG_INIT
  reg  full; // @[Repeater.scala 19:21]
  reg [2:0] saved_opcode; // @[Repeater.scala 20:18]
  reg [2:0] saved_param; // @[Repeater.scala 20:18]
  reg [2:0] saved_size; // @[Repeater.scala 20:18]
  reg [6:0] saved_source; // @[Repeater.scala 20:18]
  reg [27:0] saved_address; // @[Repeater.scala 20:18]
  reg [15:0] saved_mask; // @[Repeater.scala 20:18]
  reg  saved_corrupt; // @[Repeater.scala 20:18]
  wire  _T = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  wire  _GEN_0 = _T & io_repeat | full; // @[Repeater.scala 28:38 Repeater.scala 28:45 Repeater.scala 19:21]
  wire  _T_2 = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  assign io_full = full; // @[Repeater.scala 26:11]
  assign io_enq_ready = io_deq_ready & ~full; // @[Repeater.scala 24:32]
  assign io_deq_valid = io_enq_valid | full; // @[Repeater.scala 23:32]
  assign io_deq_bits_opcode = full ? saved_opcode : io_enq_bits_opcode; // @[Repeater.scala 25:21]
  assign io_deq_bits_param = full ? saved_param : io_enq_bits_param; // @[Repeater.scala 25:21]
  assign io_deq_bits_size = full ? saved_size : io_enq_bits_size; // @[Repeater.scala 25:21]
  assign io_deq_bits_source = full ? saved_source : io_enq_bits_source; // @[Repeater.scala 25:21]
  assign io_deq_bits_address = full ? saved_address : io_enq_bits_address; // @[Repeater.scala 25:21]
  assign io_deq_bits_mask = full ? saved_mask : io_enq_bits_mask; // @[Repeater.scala 25:21]
  assign io_deq_bits_corrupt = full ? saved_corrupt : io_enq_bits_corrupt; // @[Repeater.scala 25:21]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      full <= 1'h0;
    end else if (_T_2 & ~io_repeat) begin
      full <= 1'h0;
    end else begin
      full <= _GEN_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      saved_opcode <= 3'h0;
    end else if (_T & io_repeat) begin
      saved_opcode <= io_enq_bits_opcode;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      saved_param <= 3'h0;
    end else if (_T & io_repeat) begin
      saved_param <= io_enq_bits_param;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      saved_size <= 3'h0;
    end else if (_T & io_repeat) begin
      saved_size <= io_enq_bits_size;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      saved_source <= 7'h0;
    end else if (_T & io_repeat) begin
      saved_source <= io_enq_bits_source;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      saved_address <= 28'h0;
    end else if (_T & io_repeat) begin
      saved_address <= io_enq_bits_address;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      saved_mask <= 16'h0;
    end else if (_T & io_repeat) begin
      saved_mask <= io_enq_bits_mask;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      saved_corrupt <= 1'h0;
    end else if (_T & io_repeat) begin
      saved_corrupt <= io_enq_bits_corrupt;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  full = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  saved_opcode = _RAND_1[2:0];
  _RAND_2 = {1{`RANDOM}};
  saved_param = _RAND_2[2:0];
  _RAND_3 = {1{`RANDOM}};
  saved_size = _RAND_3[2:0];
  _RAND_4 = {1{`RANDOM}};
  saved_source = _RAND_4[6:0];
  _RAND_5 = {1{`RANDOM}};
  saved_address = _RAND_5[27:0];
  _RAND_6 = {1{`RANDOM}};
  saved_mask = _RAND_6[15:0];
  _RAND_7 = {1{`RANDOM}};
  saved_corrupt = _RAND_7[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    full = 1'h0;
  end
  if (rf_reset) begin
    saved_opcode = 3'h0;
  end
  if (rf_reset) begin
    saved_param = 3'h0;
  end
  if (rf_reset) begin
    saved_size = 3'h0;
  end
  if (rf_reset) begin
    saved_source = 7'h0;
  end
  if (rf_reset) begin
    saved_address = 28'h0;
  end
  if (rf_reset) begin
    saved_mask = 16'h0;
  end
  if (rf_reset) begin
    saved_corrupt = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
