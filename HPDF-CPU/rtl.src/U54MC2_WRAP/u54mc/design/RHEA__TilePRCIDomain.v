//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TilePRCIDomain(
  input          auto_intsink_in_sync_0,
  output         auto_tile_reset_domain_tile_bpwatch_out_0_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_0_action,
  output         auto_tile_reset_domain_tile_bpwatch_out_1_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_1_action,
  output         auto_tile_reset_domain_tile_bpwatch_out_2_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_2_action,
  output         auto_tile_reset_domain_tile_bpwatch_out_3_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_3_action,
  output         auto_tile_reset_domain_tile_bpwatch_out_4_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_4_action,
  output         auto_tile_reset_domain_tile_bpwatch_out_5_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_5_action,
  output         auto_tile_reset_domain_tile_bpwatch_out_6_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_6_action,
  output         auto_tile_reset_domain_tile_bpwatch_out_7_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_7_action,
  output         auto_tile_reset_domain_tile_bpwatch_out_8_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_8_action,
  output         auto_tile_reset_domain_tile_bpwatch_out_9_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_9_action,
  output         auto_tile_reset_domain_tile_bpwatch_out_10_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_10_action,
  output         auto_tile_reset_domain_tile_bpwatch_out_11_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_11_action,
  output         auto_tile_reset_domain_tile_bpwatch_out_12_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_12_action,
  output         auto_tile_reset_domain_tile_bpwatch_out_13_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_13_action,
  output         auto_tile_reset_domain_tile_bpwatch_out_14_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_14_action,
  output         auto_tile_reset_domain_tile_bpwatch_out_15_valid_0,
  output [2:0]   auto_tile_reset_domain_tile_bpwatch_out_15_action,
  input          auto_tile_reset_domain_tile_trace_aux_in_stall,
  input          auto_tile_reset_domain_tile_nmi_in_rnmi,
  input  [36:0]  auto_tile_reset_domain_tile_nmi_in_rnmi_interrupt_vector,
  input  [36:0]  auto_tile_reset_domain_tile_nmi_in_rnmi_exception_vector,
  input  [36:0]  auto_tile_reset_domain_tile_reset_vector_in,
  input  [1:0]   auto_tile_reset_domain_tile_hartid_in,
  output         auto_int_out_clock_xing_out_3_sync_0,
  output         auto_int_out_clock_xing_out_2_sync_0,
  output         auto_int_out_clock_xing_out_1_sync_0,
  output         auto_int_out_clock_xing_out_0_sync_0,
  input          auto_int_in_clock_xing_in_2_sync_0,
  input          auto_int_in_clock_xing_in_2_sync_1,
  input          auto_int_in_clock_xing_in_2_sync_2,
  input          auto_int_in_clock_xing_in_2_sync_3,
  input          auto_int_in_clock_xing_in_2_sync_4,
  input          auto_int_in_clock_xing_in_2_sync_5,
  input          auto_int_in_clock_xing_in_2_sync_6,
  input          auto_int_in_clock_xing_in_2_sync_7,
  input          auto_int_in_clock_xing_in_2_sync_8,
  input          auto_int_in_clock_xing_in_2_sync_9,
  input          auto_int_in_clock_xing_in_2_sync_10,
  input          auto_int_in_clock_xing_in_2_sync_11,
  input          auto_int_in_clock_xing_in_2_sync_12,
  input          auto_int_in_clock_xing_in_2_sync_13,
  input          auto_int_in_clock_xing_in_2_sync_14,
  input          auto_int_in_clock_xing_in_2_sync_15,
  input          auto_int_in_clock_xing_in_1_sync_0,
  input          auto_int_in_clock_xing_in_1_sync_1,
  input          auto_int_in_clock_xing_in_0_sync_0,
  input          auto_int_in_clock_xing_in_0_sync_1,
  output [2:0]   auto_tl_master_clock_xing_out_a_bits0_opcode,
  output [2:0]   auto_tl_master_clock_xing_out_a_bits0_param,
  output [3:0]   auto_tl_master_clock_xing_out_a_bits0_size,
  output [4:0]   auto_tl_master_clock_xing_out_a_bits0_source,
  output [36:0]  auto_tl_master_clock_xing_out_a_bits0_address,
  output         auto_tl_master_clock_xing_out_a_bits0_user_amba_prot_bufferable,
  output         auto_tl_master_clock_xing_out_a_bits0_user_amba_prot_modifiable,
  output         auto_tl_master_clock_xing_out_a_bits0_user_amba_prot_readalloc,
  output         auto_tl_master_clock_xing_out_a_bits0_user_amba_prot_writealloc,
  output         auto_tl_master_clock_xing_out_a_bits0_user_amba_prot_privileged,
  output         auto_tl_master_clock_xing_out_a_bits0_user_amba_prot_secure,
  output         auto_tl_master_clock_xing_out_a_bits0_user_amba_prot_fetch,
  output [15:0]  auto_tl_master_clock_xing_out_a_bits0_mask,
  output [127:0] auto_tl_master_clock_xing_out_a_bits0_data,
  output         auto_tl_master_clock_xing_out_a_bits0_corrupt,
  output [2:0]   auto_tl_master_clock_xing_out_a_bits1_opcode,
  output [2:0]   auto_tl_master_clock_xing_out_a_bits1_param,
  output [3:0]   auto_tl_master_clock_xing_out_a_bits1_size,
  output [4:0]   auto_tl_master_clock_xing_out_a_bits1_source,
  output [36:0]  auto_tl_master_clock_xing_out_a_bits1_address,
  output         auto_tl_master_clock_xing_out_a_bits1_user_amba_prot_bufferable,
  output         auto_tl_master_clock_xing_out_a_bits1_user_amba_prot_modifiable,
  output         auto_tl_master_clock_xing_out_a_bits1_user_amba_prot_readalloc,
  output         auto_tl_master_clock_xing_out_a_bits1_user_amba_prot_writealloc,
  output         auto_tl_master_clock_xing_out_a_bits1_user_amba_prot_privileged,
  output         auto_tl_master_clock_xing_out_a_bits1_user_amba_prot_secure,
  output         auto_tl_master_clock_xing_out_a_bits1_user_amba_prot_fetch,
  output [15:0]  auto_tl_master_clock_xing_out_a_bits1_mask,
  output [127:0] auto_tl_master_clock_xing_out_a_bits1_data,
  output         auto_tl_master_clock_xing_out_a_bits1_corrupt,
  output         auto_tl_master_clock_xing_out_a_valid,
  output [1:0]   auto_tl_master_clock_xing_out_a_source,
  input          auto_tl_master_clock_xing_out_a_ready,
  input  [1:0]   auto_tl_master_clock_xing_out_a_sink,
  input  [2:0]   auto_tl_master_clock_xing_out_b_bits0_opcode,
  input  [1:0]   auto_tl_master_clock_xing_out_b_bits0_param,
  input  [3:0]   auto_tl_master_clock_xing_out_b_bits0_size,
  input  [4:0]   auto_tl_master_clock_xing_out_b_bits0_source,
  input  [36:0]  auto_tl_master_clock_xing_out_b_bits0_address,
  input  [15:0]  auto_tl_master_clock_xing_out_b_bits0_mask,
  input  [127:0] auto_tl_master_clock_xing_out_b_bits0_data,
  input          auto_tl_master_clock_xing_out_b_bits0_corrupt,
  input  [2:0]   auto_tl_master_clock_xing_out_b_bits1_opcode,
  input  [1:0]   auto_tl_master_clock_xing_out_b_bits1_param,
  input  [3:0]   auto_tl_master_clock_xing_out_b_bits1_size,
  input  [4:0]   auto_tl_master_clock_xing_out_b_bits1_source,
  input  [36:0]  auto_tl_master_clock_xing_out_b_bits1_address,
  input  [15:0]  auto_tl_master_clock_xing_out_b_bits1_mask,
  input  [127:0] auto_tl_master_clock_xing_out_b_bits1_data,
  input          auto_tl_master_clock_xing_out_b_bits1_corrupt,
  input          auto_tl_master_clock_xing_out_b_valid,
  input  [1:0]   auto_tl_master_clock_xing_out_b_source,
  output         auto_tl_master_clock_xing_out_b_ready,
  output [1:0]   auto_tl_master_clock_xing_out_b_sink,
  output [2:0]   auto_tl_master_clock_xing_out_c_bits0_opcode,
  output [2:0]   auto_tl_master_clock_xing_out_c_bits0_param,
  output [3:0]   auto_tl_master_clock_xing_out_c_bits0_size,
  output [4:0]   auto_tl_master_clock_xing_out_c_bits0_source,
  output [36:0]  auto_tl_master_clock_xing_out_c_bits0_address,
  output [127:0] auto_tl_master_clock_xing_out_c_bits0_data,
  output         auto_tl_master_clock_xing_out_c_bits0_corrupt,
  output [2:0]   auto_tl_master_clock_xing_out_c_bits1_opcode,
  output [2:0]   auto_tl_master_clock_xing_out_c_bits1_param,
  output [3:0]   auto_tl_master_clock_xing_out_c_bits1_size,
  output [4:0]   auto_tl_master_clock_xing_out_c_bits1_source,
  output [36:0]  auto_tl_master_clock_xing_out_c_bits1_address,
  output [127:0] auto_tl_master_clock_xing_out_c_bits1_data,
  output         auto_tl_master_clock_xing_out_c_bits1_corrupt,
  output         auto_tl_master_clock_xing_out_c_valid,
  output [1:0]   auto_tl_master_clock_xing_out_c_source,
  input          auto_tl_master_clock_xing_out_c_ready,
  input  [1:0]   auto_tl_master_clock_xing_out_c_sink,
  input  [2:0]   auto_tl_master_clock_xing_out_d_bits0_opcode,
  input  [1:0]   auto_tl_master_clock_xing_out_d_bits0_param,
  input  [3:0]   auto_tl_master_clock_xing_out_d_bits0_size,
  input  [4:0]   auto_tl_master_clock_xing_out_d_bits0_source,
  input  [4:0]   auto_tl_master_clock_xing_out_d_bits0_sink,
  input          auto_tl_master_clock_xing_out_d_bits0_denied,
  input  [127:0] auto_tl_master_clock_xing_out_d_bits0_data,
  input          auto_tl_master_clock_xing_out_d_bits0_corrupt,
  input  [2:0]   auto_tl_master_clock_xing_out_d_bits1_opcode,
  input  [1:0]   auto_tl_master_clock_xing_out_d_bits1_param,
  input  [3:0]   auto_tl_master_clock_xing_out_d_bits1_size,
  input  [4:0]   auto_tl_master_clock_xing_out_d_bits1_source,
  input  [4:0]   auto_tl_master_clock_xing_out_d_bits1_sink,
  input          auto_tl_master_clock_xing_out_d_bits1_denied,
  input  [127:0] auto_tl_master_clock_xing_out_d_bits1_data,
  input          auto_tl_master_clock_xing_out_d_bits1_corrupt,
  input          auto_tl_master_clock_xing_out_d_valid,
  input  [1:0]   auto_tl_master_clock_xing_out_d_source,
  output         auto_tl_master_clock_xing_out_d_ready,
  output [1:0]   auto_tl_master_clock_xing_out_d_sink,
  output [4:0]   auto_tl_master_clock_xing_out_e_bits0_sink,
  output [4:0]   auto_tl_master_clock_xing_out_e_bits1_sink,
  output         auto_tl_master_clock_xing_out_e_valid,
  output [1:0]   auto_tl_master_clock_xing_out_e_source,
  input          auto_tl_master_clock_xing_out_e_ready,
  input  [1:0]   auto_tl_master_clock_xing_out_e_sink,
  output         auto_trace_out_0_valid,
  output [39:0]  auto_trace_out_0_iaddr,
  output [31:0]  auto_trace_out_0_insn,
  output [2:0]   auto_trace_out_0_priv,
  output         auto_trace_out_0_exception,
  output         auto_trace_out_0_interrupt,
  input          auto_tap_clock_in_clock,
  input          auto_tap_clock_in_reset,
  input          psd_test_clock_enable
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
`endif // RANDOMIZE_REG_INIT
  wire  rf_reset;
  wire  tile_reset_domain_auto_tile_broadcast_out_0_valid;
  wire [39:0] tile_reset_domain_auto_tile_broadcast_out_0_iaddr;
  wire [31:0] tile_reset_domain_auto_tile_broadcast_out_0_insn;
  wire [2:0] tile_reset_domain_auto_tile_broadcast_out_0_priv;
  wire  tile_reset_domain_auto_tile_broadcast_out_0_exception;
  wire  tile_reset_domain_auto_tile_broadcast_out_0_interrupt;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_0;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_1;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_2;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_3;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_4;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_5;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_6;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_7;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_8;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_9;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_10;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_11;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_12;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_13;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_14;
  wire  tile_reset_domain_auto_tile_int_sink_in_3_15;
  wire  tile_reset_domain_auto_tile_int_sink_in_2_0;
  wire  tile_reset_domain_auto_tile_int_sink_in_2_1;
  wire  tile_reset_domain_auto_tile_int_sink_in_1_0;
  wire  tile_reset_domain_auto_tile_int_sink_in_1_1;
  wire  tile_reset_domain_auto_tile_int_sink_in_0_0;
  wire  tile_reset_domain_auto_tile_bpwatch_out_0_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_0_action;
  wire  tile_reset_domain_auto_tile_bpwatch_out_1_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_1_action;
  wire  tile_reset_domain_auto_tile_bpwatch_out_2_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_2_action;
  wire  tile_reset_domain_auto_tile_bpwatch_out_3_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_3_action;
  wire  tile_reset_domain_auto_tile_bpwatch_out_4_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_4_action;
  wire  tile_reset_domain_auto_tile_bpwatch_out_5_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_5_action;
  wire  tile_reset_domain_auto_tile_bpwatch_out_6_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_6_action;
  wire  tile_reset_domain_auto_tile_bpwatch_out_7_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_7_action;
  wire  tile_reset_domain_auto_tile_bpwatch_out_8_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_8_action;
  wire  tile_reset_domain_auto_tile_bpwatch_out_9_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_9_action;
  wire  tile_reset_domain_auto_tile_bpwatch_out_10_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_10_action;
  wire  tile_reset_domain_auto_tile_bpwatch_out_11_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_11_action;
  wire  tile_reset_domain_auto_tile_bpwatch_out_12_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_12_action;
  wire  tile_reset_domain_auto_tile_bpwatch_out_13_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_13_action;
  wire  tile_reset_domain_auto_tile_bpwatch_out_14_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_14_action;
  wire  tile_reset_domain_auto_tile_bpwatch_out_15_valid_0;
  wire [2:0] tile_reset_domain_auto_tile_bpwatch_out_15_action;
  wire  tile_reset_domain_auto_tile_trace_aux_in_stall;
  wire  tile_reset_domain_auto_tile_nmi_in_rnmi;
  wire [36:0] tile_reset_domain_auto_tile_nmi_in_rnmi_interrupt_vector;
  wire [36:0] tile_reset_domain_auto_tile_nmi_in_rnmi_exception_vector;
  wire [36:0] tile_reset_domain_auto_tile_reset_vector_in;
  wire [1:0] tile_reset_domain_auto_tile_hartid_in;
  wire  tile_reset_domain_auto_int_out_xing_out_3_0;
  wire  tile_reset_domain_auto_int_out_xing_out_2_0;
  wire  tile_reset_domain_auto_int_out_xing_out_1_0;
  wire  tile_reset_domain_auto_tl_out_xing_out_a_ready;
  wire  tile_reset_domain_auto_tl_out_xing_out_a_valid;
  wire [2:0] tile_reset_domain_auto_tl_out_xing_out_a_bits_opcode;
  wire [2:0] tile_reset_domain_auto_tl_out_xing_out_a_bits_param;
  wire [3:0] tile_reset_domain_auto_tl_out_xing_out_a_bits_size;
  wire [4:0] tile_reset_domain_auto_tl_out_xing_out_a_bits_source;
  wire [36:0] tile_reset_domain_auto_tl_out_xing_out_a_bits_address;
  wire  tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_bufferable;
  wire  tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_modifiable;
  wire  tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_readalloc;
  wire  tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_writealloc;
  wire  tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_privileged;
  wire  tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_secure;
  wire  tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_fetch;
  wire [15:0] tile_reset_domain_auto_tl_out_xing_out_a_bits_mask;
  wire [127:0] tile_reset_domain_auto_tl_out_xing_out_a_bits_data;
  wire  tile_reset_domain_auto_tl_out_xing_out_a_bits_corrupt;
  wire  tile_reset_domain_auto_tl_out_xing_out_b_ready;
  wire  tile_reset_domain_auto_tl_out_xing_out_b_valid;
  wire [2:0] tile_reset_domain_auto_tl_out_xing_out_b_bits_opcode;
  wire [1:0] tile_reset_domain_auto_tl_out_xing_out_b_bits_param;
  wire [3:0] tile_reset_domain_auto_tl_out_xing_out_b_bits_size;
  wire [4:0] tile_reset_domain_auto_tl_out_xing_out_b_bits_source;
  wire [36:0] tile_reset_domain_auto_tl_out_xing_out_b_bits_address;
  wire [15:0] tile_reset_domain_auto_tl_out_xing_out_b_bits_mask;
  wire [127:0] tile_reset_domain_auto_tl_out_xing_out_b_bits_data;
  wire  tile_reset_domain_auto_tl_out_xing_out_b_bits_corrupt;
  wire  tile_reset_domain_auto_tl_out_xing_out_c_ready;
  wire  tile_reset_domain_auto_tl_out_xing_out_c_valid;
  wire [2:0] tile_reset_domain_auto_tl_out_xing_out_c_bits_opcode;
  wire [2:0] tile_reset_domain_auto_tl_out_xing_out_c_bits_param;
  wire [3:0] tile_reset_domain_auto_tl_out_xing_out_c_bits_size;
  wire [4:0] tile_reset_domain_auto_tl_out_xing_out_c_bits_source;
  wire [36:0] tile_reset_domain_auto_tl_out_xing_out_c_bits_address;
  wire [127:0] tile_reset_domain_auto_tl_out_xing_out_c_bits_data;
  wire  tile_reset_domain_auto_tl_out_xing_out_c_bits_corrupt;
  wire  tile_reset_domain_auto_tl_out_xing_out_d_ready;
  wire  tile_reset_domain_auto_tl_out_xing_out_d_valid;
  wire [2:0] tile_reset_domain_auto_tl_out_xing_out_d_bits_opcode;
  wire [1:0] tile_reset_domain_auto_tl_out_xing_out_d_bits_param;
  wire [3:0] tile_reset_domain_auto_tl_out_xing_out_d_bits_size;
  wire [4:0] tile_reset_domain_auto_tl_out_xing_out_d_bits_source;
  wire [4:0] tile_reset_domain_auto_tl_out_xing_out_d_bits_sink;
  wire  tile_reset_domain_auto_tl_out_xing_out_d_bits_denied;
  wire [127:0] tile_reset_domain_auto_tl_out_xing_out_d_bits_data;
  wire  tile_reset_domain_auto_tl_out_xing_out_d_bits_corrupt;
  wire  tile_reset_domain_auto_tl_out_xing_out_e_ready;
  wire  tile_reset_domain_auto_tl_out_xing_out_e_valid;
  wire [4:0] tile_reset_domain_auto_tl_out_xing_out_e_bits_sink;
  wire  tile_reset_domain_auto_clock_in_clock;
  wire  tile_reset_domain_auto_clock_in_reset;
  wire  tile_reset_domain_psd_test_clock_enable;
  wire  tile_rf_reset; // @[HasTiles.scala 274:53]
  wire  tile_clock; // @[HasTiles.scala 274:53]
  wire  tile_reset; // @[HasTiles.scala 274:53]
  wire  tile_auto_broadcast_out_0_valid; // @[HasTiles.scala 274:53]
  wire [39:0] tile_auto_broadcast_out_0_iaddr; // @[HasTiles.scala 274:53]
  wire [31:0] tile_auto_broadcast_out_0_insn; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_broadcast_out_0_priv; // @[HasTiles.scala 274:53]
  wire  tile_auto_broadcast_out_0_exception; // @[HasTiles.scala 274:53]
  wire  tile_auto_broadcast_out_0_interrupt; // @[HasTiles.scala 274:53]
  wire  tile_auto_debug_out_0; // @[HasTiles.scala 274:53]
  wire  tile_auto_wfi_out_0; // @[HasTiles.scala 274:53]
  wire  tile_auto_cease_out_0; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_0; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_1; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_2; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_3; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_4; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_5; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_6; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_7; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_8; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_9; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_10; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_11; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_12; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_13; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_14; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_3_15; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_2_0; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_2_1; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_1_0; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_1_1; // @[HasTiles.scala 274:53]
  wire  tile_auto_int_sink_in_0_0; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_0_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_0_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_1_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_1_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_2_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_2_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_3_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_3_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_4_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_4_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_5_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_5_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_6_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_6_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_7_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_7_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_8_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_8_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_9_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_9_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_10_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_10_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_11_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_11_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_12_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_12_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_13_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_13_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_14_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_14_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_bpwatch_out_15_valid_0; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_bpwatch_out_15_action; // @[HasTiles.scala 274:53]
  wire  tile_auto_trace_aux_in_stall; // @[HasTiles.scala 274:53]
  wire  tile_auto_nmi_in_rnmi; // @[HasTiles.scala 274:53]
  wire [36:0] tile_auto_nmi_in_rnmi_interrupt_vector; // @[HasTiles.scala 274:53]
  wire [36:0] tile_auto_nmi_in_rnmi_exception_vector; // @[HasTiles.scala 274:53]
  wire [36:0] tile_auto_reset_vector_in; // @[HasTiles.scala 274:53]
  wire [1:0] tile_auto_hartid_in; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_a_ready; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_a_valid; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_tl_other_masters_out_a_bits_opcode; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_tl_other_masters_out_a_bits_param; // @[HasTiles.scala 274:53]
  wire [3:0] tile_auto_tl_other_masters_out_a_bits_size; // @[HasTiles.scala 274:53]
  wire [4:0] tile_auto_tl_other_masters_out_a_bits_source; // @[HasTiles.scala 274:53]
  wire [36:0] tile_auto_tl_other_masters_out_a_bits_address; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_a_bits_user_amba_prot_bufferable; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_a_bits_user_amba_prot_modifiable; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_a_bits_user_amba_prot_readalloc; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_a_bits_user_amba_prot_writealloc; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_a_bits_user_amba_prot_privileged; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_a_bits_user_amba_prot_secure; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_a_bits_user_amba_prot_fetch; // @[HasTiles.scala 274:53]
  wire [15:0] tile_auto_tl_other_masters_out_a_bits_mask; // @[HasTiles.scala 274:53]
  wire [127:0] tile_auto_tl_other_masters_out_a_bits_data; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_a_bits_corrupt; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_b_ready; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_b_valid; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_tl_other_masters_out_b_bits_opcode; // @[HasTiles.scala 274:53]
  wire [1:0] tile_auto_tl_other_masters_out_b_bits_param; // @[HasTiles.scala 274:53]
  wire [3:0] tile_auto_tl_other_masters_out_b_bits_size; // @[HasTiles.scala 274:53]
  wire [4:0] tile_auto_tl_other_masters_out_b_bits_source; // @[HasTiles.scala 274:53]
  wire [36:0] tile_auto_tl_other_masters_out_b_bits_address; // @[HasTiles.scala 274:53]
  wire [15:0] tile_auto_tl_other_masters_out_b_bits_mask; // @[HasTiles.scala 274:53]
  wire [127:0] tile_auto_tl_other_masters_out_b_bits_data; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_b_bits_corrupt; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_c_ready; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_c_valid; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_tl_other_masters_out_c_bits_opcode; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_tl_other_masters_out_c_bits_param; // @[HasTiles.scala 274:53]
  wire [3:0] tile_auto_tl_other_masters_out_c_bits_size; // @[HasTiles.scala 274:53]
  wire [4:0] tile_auto_tl_other_masters_out_c_bits_source; // @[HasTiles.scala 274:53]
  wire [36:0] tile_auto_tl_other_masters_out_c_bits_address; // @[HasTiles.scala 274:53]
  wire [127:0] tile_auto_tl_other_masters_out_c_bits_data; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_c_bits_corrupt; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_d_ready; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_d_valid; // @[HasTiles.scala 274:53]
  wire [2:0] tile_auto_tl_other_masters_out_d_bits_opcode; // @[HasTiles.scala 274:53]
  wire [1:0] tile_auto_tl_other_masters_out_d_bits_param; // @[HasTiles.scala 274:53]
  wire [3:0] tile_auto_tl_other_masters_out_d_bits_size; // @[HasTiles.scala 274:53]
  wire [4:0] tile_auto_tl_other_masters_out_d_bits_source; // @[HasTiles.scala 274:53]
  wire [4:0] tile_auto_tl_other_masters_out_d_bits_sink; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_d_bits_denied; // @[HasTiles.scala 274:53]
  wire [127:0] tile_auto_tl_other_masters_out_d_bits_data; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_d_bits_corrupt; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_e_ready; // @[HasTiles.scala 274:53]
  wire  tile_auto_tl_other_masters_out_e_valid; // @[HasTiles.scala 274:53]
  wire [4:0] tile_auto_tl_other_masters_out_e_bits_sink; // @[HasTiles.scala 274:53]
  wire  tile_psd_test_clock_enable; // @[HasTiles.scala 274:53]
  wire  clockNode_auto_in_clock;
  wire  clockNode_auto_in_reset;
  wire  clockNode_auto_out_clock;
  wire  clockNode_auto_out_reset;
  wire  buffer_auto_in_a_ready;
  wire  buffer_auto_in_a_valid;
  wire [2:0] buffer_auto_in_a_bits_opcode;
  wire [2:0] buffer_auto_in_a_bits_param;
  wire [3:0] buffer_auto_in_a_bits_size;
  wire [4:0] buffer_auto_in_a_bits_source;
  wire [36:0] buffer_auto_in_a_bits_address;
  wire  buffer_auto_in_a_bits_user_amba_prot_bufferable;
  wire  buffer_auto_in_a_bits_user_amba_prot_modifiable;
  wire  buffer_auto_in_a_bits_user_amba_prot_readalloc;
  wire  buffer_auto_in_a_bits_user_amba_prot_writealloc;
  wire  buffer_auto_in_a_bits_user_amba_prot_privileged;
  wire  buffer_auto_in_a_bits_user_amba_prot_secure;
  wire  buffer_auto_in_a_bits_user_amba_prot_fetch;
  wire [15:0] buffer_auto_in_a_bits_mask;
  wire [127:0] buffer_auto_in_a_bits_data;
  wire  buffer_auto_in_a_bits_corrupt;
  wire  buffer_auto_in_b_ready;
  wire  buffer_auto_in_b_valid;
  wire [2:0] buffer_auto_in_b_bits_opcode;
  wire [1:0] buffer_auto_in_b_bits_param;
  wire [3:0] buffer_auto_in_b_bits_size;
  wire [4:0] buffer_auto_in_b_bits_source;
  wire [36:0] buffer_auto_in_b_bits_address;
  wire [15:0] buffer_auto_in_b_bits_mask;
  wire [127:0] buffer_auto_in_b_bits_data;
  wire  buffer_auto_in_b_bits_corrupt;
  wire  buffer_auto_in_c_ready;
  wire  buffer_auto_in_c_valid;
  wire [2:0] buffer_auto_in_c_bits_opcode;
  wire [2:0] buffer_auto_in_c_bits_param;
  wire [3:0] buffer_auto_in_c_bits_size;
  wire [4:0] buffer_auto_in_c_bits_source;
  wire [36:0] buffer_auto_in_c_bits_address;
  wire [127:0] buffer_auto_in_c_bits_data;
  wire  buffer_auto_in_c_bits_corrupt;
  wire  buffer_auto_in_d_ready;
  wire  buffer_auto_in_d_valid;
  wire [2:0] buffer_auto_in_d_bits_opcode;
  wire [1:0] buffer_auto_in_d_bits_param;
  wire [3:0] buffer_auto_in_d_bits_size;
  wire [4:0] buffer_auto_in_d_bits_source;
  wire [4:0] buffer_auto_in_d_bits_sink;
  wire  buffer_auto_in_d_bits_denied;
  wire [127:0] buffer_auto_in_d_bits_data;
  wire  buffer_auto_in_d_bits_corrupt;
  wire  buffer_auto_in_e_ready;
  wire  buffer_auto_in_e_valid;
  wire [4:0] buffer_auto_in_e_bits_sink;
  wire  buffer_auto_out_a_ready;
  wire  buffer_auto_out_a_valid;
  wire [2:0] buffer_auto_out_a_bits_opcode;
  wire [2:0] buffer_auto_out_a_bits_param;
  wire [3:0] buffer_auto_out_a_bits_size;
  wire [4:0] buffer_auto_out_a_bits_source;
  wire [36:0] buffer_auto_out_a_bits_address;
  wire  buffer_auto_out_a_bits_user_amba_prot_bufferable;
  wire  buffer_auto_out_a_bits_user_amba_prot_modifiable;
  wire  buffer_auto_out_a_bits_user_amba_prot_readalloc;
  wire  buffer_auto_out_a_bits_user_amba_prot_writealloc;
  wire  buffer_auto_out_a_bits_user_amba_prot_privileged;
  wire  buffer_auto_out_a_bits_user_amba_prot_secure;
  wire  buffer_auto_out_a_bits_user_amba_prot_fetch;
  wire [15:0] buffer_auto_out_a_bits_mask;
  wire [127:0] buffer_auto_out_a_bits_data;
  wire  buffer_auto_out_a_bits_corrupt;
  wire  buffer_auto_out_b_ready;
  wire  buffer_auto_out_b_valid;
  wire [2:0] buffer_auto_out_b_bits_opcode;
  wire [1:0] buffer_auto_out_b_bits_param;
  wire [3:0] buffer_auto_out_b_bits_size;
  wire [4:0] buffer_auto_out_b_bits_source;
  wire [36:0] buffer_auto_out_b_bits_address;
  wire [15:0] buffer_auto_out_b_bits_mask;
  wire [127:0] buffer_auto_out_b_bits_data;
  wire  buffer_auto_out_b_bits_corrupt;
  wire  buffer_auto_out_c_ready;
  wire  buffer_auto_out_c_valid;
  wire [2:0] buffer_auto_out_c_bits_opcode;
  wire [2:0] buffer_auto_out_c_bits_param;
  wire [3:0] buffer_auto_out_c_bits_size;
  wire [4:0] buffer_auto_out_c_bits_source;
  wire [36:0] buffer_auto_out_c_bits_address;
  wire [127:0] buffer_auto_out_c_bits_data;
  wire  buffer_auto_out_c_bits_corrupt;
  wire  buffer_auto_out_d_ready;
  wire  buffer_auto_out_d_valid;
  wire [2:0] buffer_auto_out_d_bits_opcode;
  wire [1:0] buffer_auto_out_d_bits_param;
  wire [3:0] buffer_auto_out_d_bits_size;
  wire [4:0] buffer_auto_out_d_bits_source;
  wire [4:0] buffer_auto_out_d_bits_sink;
  wire  buffer_auto_out_d_bits_denied;
  wire [127:0] buffer_auto_out_d_bits_data;
  wire  buffer_auto_out_d_bits_corrupt;
  wire  buffer_auto_out_e_ready;
  wire  buffer_auto_out_e_valid;
  wire [4:0] buffer_auto_out_e_bits_sink;
  wire  block_during_reset_clock;
  wire  block_during_reset_reset;
  wire  block_during_reset_auto_in_a_ready;
  wire  block_during_reset_auto_in_a_valid;
  wire [2:0] block_during_reset_auto_in_a_bits_opcode;
  wire [2:0] block_during_reset_auto_in_a_bits_param;
  wire [3:0] block_during_reset_auto_in_a_bits_size;
  wire [4:0] block_during_reset_auto_in_a_bits_source;
  wire [36:0] block_during_reset_auto_in_a_bits_address;
  wire  block_during_reset_auto_in_a_bits_user_amba_prot_bufferable;
  wire  block_during_reset_auto_in_a_bits_user_amba_prot_modifiable;
  wire  block_during_reset_auto_in_a_bits_user_amba_prot_readalloc;
  wire  block_during_reset_auto_in_a_bits_user_amba_prot_writealloc;
  wire  block_during_reset_auto_in_a_bits_user_amba_prot_privileged;
  wire  block_during_reset_auto_in_a_bits_user_amba_prot_secure;
  wire  block_during_reset_auto_in_a_bits_user_amba_prot_fetch;
  wire [15:0] block_during_reset_auto_in_a_bits_mask;
  wire [127:0] block_during_reset_auto_in_a_bits_data;
  wire  block_during_reset_auto_in_a_bits_corrupt;
  wire  block_during_reset_auto_in_b_ready;
  wire  block_during_reset_auto_in_b_valid;
  wire [2:0] block_during_reset_auto_in_b_bits_opcode;
  wire [1:0] block_during_reset_auto_in_b_bits_param;
  wire [3:0] block_during_reset_auto_in_b_bits_size;
  wire [4:0] block_during_reset_auto_in_b_bits_source;
  wire [36:0] block_during_reset_auto_in_b_bits_address;
  wire [15:0] block_during_reset_auto_in_b_bits_mask;
  wire [127:0] block_during_reset_auto_in_b_bits_data;
  wire  block_during_reset_auto_in_b_bits_corrupt;
  wire  block_during_reset_auto_in_c_ready;
  wire  block_during_reset_auto_in_c_valid;
  wire [2:0] block_during_reset_auto_in_c_bits_opcode;
  wire [2:0] block_during_reset_auto_in_c_bits_param;
  wire [3:0] block_during_reset_auto_in_c_bits_size;
  wire [4:0] block_during_reset_auto_in_c_bits_source;
  wire [36:0] block_during_reset_auto_in_c_bits_address;
  wire [127:0] block_during_reset_auto_in_c_bits_data;
  wire  block_during_reset_auto_in_c_bits_corrupt;
  wire  block_during_reset_auto_in_d_ready;
  wire  block_during_reset_auto_in_d_valid;
  wire [2:0] block_during_reset_auto_in_d_bits_opcode;
  wire [1:0] block_during_reset_auto_in_d_bits_param;
  wire [3:0] block_during_reset_auto_in_d_bits_size;
  wire [4:0] block_during_reset_auto_in_d_bits_source;
  wire [4:0] block_during_reset_auto_in_d_bits_sink;
  wire  block_during_reset_auto_in_d_bits_denied;
  wire [127:0] block_during_reset_auto_in_d_bits_data;
  wire  block_during_reset_auto_in_d_bits_corrupt;
  wire  block_during_reset_auto_in_e_ready;
  wire  block_during_reset_auto_in_e_valid;
  wire [4:0] block_during_reset_auto_in_e_bits_sink;
  wire  block_during_reset_auto_out_a_ready;
  wire  block_during_reset_auto_out_a_valid;
  wire [2:0] block_during_reset_auto_out_a_bits_opcode;
  wire [2:0] block_during_reset_auto_out_a_bits_param;
  wire [3:0] block_during_reset_auto_out_a_bits_size;
  wire [4:0] block_during_reset_auto_out_a_bits_source;
  wire [36:0] block_during_reset_auto_out_a_bits_address;
  wire  block_during_reset_auto_out_a_bits_user_amba_prot_bufferable;
  wire  block_during_reset_auto_out_a_bits_user_amba_prot_modifiable;
  wire  block_during_reset_auto_out_a_bits_user_amba_prot_readalloc;
  wire  block_during_reset_auto_out_a_bits_user_amba_prot_writealloc;
  wire  block_during_reset_auto_out_a_bits_user_amba_prot_privileged;
  wire  block_during_reset_auto_out_a_bits_user_amba_prot_secure;
  wire  block_during_reset_auto_out_a_bits_user_amba_prot_fetch;
  wire [15:0] block_during_reset_auto_out_a_bits_mask;
  wire [127:0] block_during_reset_auto_out_a_bits_data;
  wire  block_during_reset_auto_out_a_bits_corrupt;
  wire  block_during_reset_auto_out_b_ready;
  wire  block_during_reset_auto_out_b_valid;
  wire [2:0] block_during_reset_auto_out_b_bits_opcode;
  wire [1:0] block_during_reset_auto_out_b_bits_param;
  wire [3:0] block_during_reset_auto_out_b_bits_size;
  wire [4:0] block_during_reset_auto_out_b_bits_source;
  wire [36:0] block_during_reset_auto_out_b_bits_address;
  wire [15:0] block_during_reset_auto_out_b_bits_mask;
  wire [127:0] block_during_reset_auto_out_b_bits_data;
  wire  block_during_reset_auto_out_b_bits_corrupt;
  wire  block_during_reset_auto_out_c_ready;
  wire  block_during_reset_auto_out_c_valid;
  wire [2:0] block_during_reset_auto_out_c_bits_opcode;
  wire [2:0] block_during_reset_auto_out_c_bits_param;
  wire [3:0] block_during_reset_auto_out_c_bits_size;
  wire [4:0] block_during_reset_auto_out_c_bits_source;
  wire [36:0] block_during_reset_auto_out_c_bits_address;
  wire [127:0] block_during_reset_auto_out_c_bits_data;
  wire  block_during_reset_auto_out_c_bits_corrupt;
  wire  block_during_reset_auto_out_d_ready;
  wire  block_during_reset_auto_out_d_valid;
  wire [2:0] block_during_reset_auto_out_d_bits_opcode;
  wire [1:0] block_during_reset_auto_out_d_bits_param;
  wire [3:0] block_during_reset_auto_out_d_bits_size;
  wire [4:0] block_during_reset_auto_out_d_bits_source;
  wire [4:0] block_during_reset_auto_out_d_bits_sink;
  wire  block_during_reset_auto_out_d_bits_denied;
  wire [127:0] block_during_reset_auto_out_d_bits_data;
  wire  block_during_reset_auto_out_d_bits_corrupt;
  wire  block_during_reset_auto_out_e_ready;
  wire  block_during_reset_auto_out_e_valid;
  wire [4:0] block_during_reset_auto_out_e_bits_sink;
  reg [3:0] block_during_reset_value; // @[Counter.scala 60:40]
  wire  block_during_reset_wrap_wrap = block_during_reset_value == 4'hf; // @[Counter.scala 72:24]
  reg  block_during_reset_r; // @[Reg.scala 27:20]
  wire  block_during_reset__T = ~block_during_reset_r; // @[BlockDuringReset.scala 17:41]
  reg [3:0] block_during_reset_value_1; // @[Counter.scala 60:40]
  wire  block_during_reset_wrap_wrap_1 = block_during_reset_value_1 == 4'hf; // @[Counter.scala 72:24]
  reg  block_during_reset_r_1; // @[Reg.scala 27:20]
  wire  block_during_reset__T_1 = ~block_during_reset_r_1; // @[BlockDuringReset.scala 17:41]
  reg [3:0] block_during_reset_value_2; // @[Counter.scala 60:40]
  wire  block_during_reset_wrap_wrap_2 = block_during_reset_value_2 == 4'hf; // @[Counter.scala 72:24]
  reg  block_during_reset_r_2; // @[Reg.scala 27:20]
  wire  block_during_reset__T_2 = ~block_during_reset_r_2; // @[BlockDuringReset.scala 17:41]
  reg [3:0] block_during_reset_value_3; // @[Counter.scala 60:40]
  wire  block_during_reset_wrap_wrap_3 = block_during_reset_value_3 == 4'hf; // @[Counter.scala 72:24]
  reg  block_during_reset_r_3; // @[Reg.scala 27:20]
  wire  block_during_reset__T_3 = ~block_during_reset_r_3; // @[BlockDuringReset.scala 17:41]
  reg [3:0] block_during_reset_value_4; // @[Counter.scala 60:40]
  wire  block_during_reset_wrap_wrap_4 = block_during_reset_value_4 == 4'hf; // @[Counter.scala 72:24]
  reg  block_during_reset_r_4; // @[Reg.scala 27:20]
  wire  block_during_reset__T_4 = ~block_during_reset_r_4; // @[BlockDuringReset.scala 17:41]
  wire  rsource_rf_reset; // @[RationalCrossing.scala 83:29]
  wire  rsource_clock; // @[RationalCrossing.scala 83:29]
  wire  rsource_reset; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_a_ready; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_a_valid; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_in_a_bits_opcode; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_in_a_bits_param; // @[RationalCrossing.scala 83:29]
  wire [3:0] rsource_auto_in_a_bits_size; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_in_a_bits_source; // @[RationalCrossing.scala 83:29]
  wire [36:0] rsource_auto_in_a_bits_address; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_a_bits_user_amba_prot_bufferable; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_a_bits_user_amba_prot_modifiable; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_a_bits_user_amba_prot_readalloc; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_a_bits_user_amba_prot_writealloc; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_a_bits_user_amba_prot_privileged; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_a_bits_user_amba_prot_secure; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_a_bits_user_amba_prot_fetch; // @[RationalCrossing.scala 83:29]
  wire [15:0] rsource_auto_in_a_bits_mask; // @[RationalCrossing.scala 83:29]
  wire [127:0] rsource_auto_in_a_bits_data; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_a_bits_corrupt; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_b_ready; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_b_valid; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_in_b_bits_opcode; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_in_b_bits_param; // @[RationalCrossing.scala 83:29]
  wire [3:0] rsource_auto_in_b_bits_size; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_in_b_bits_source; // @[RationalCrossing.scala 83:29]
  wire [36:0] rsource_auto_in_b_bits_address; // @[RationalCrossing.scala 83:29]
  wire [15:0] rsource_auto_in_b_bits_mask; // @[RationalCrossing.scala 83:29]
  wire [127:0] rsource_auto_in_b_bits_data; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_b_bits_corrupt; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_c_ready; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_c_valid; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_in_c_bits_opcode; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_in_c_bits_param; // @[RationalCrossing.scala 83:29]
  wire [3:0] rsource_auto_in_c_bits_size; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_in_c_bits_source; // @[RationalCrossing.scala 83:29]
  wire [36:0] rsource_auto_in_c_bits_address; // @[RationalCrossing.scala 83:29]
  wire [127:0] rsource_auto_in_c_bits_data; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_c_bits_corrupt; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_d_ready; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_d_valid; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_in_d_bits_opcode; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_in_d_bits_param; // @[RationalCrossing.scala 83:29]
  wire [3:0] rsource_auto_in_d_bits_size; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_in_d_bits_source; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_in_d_bits_sink; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_d_bits_denied; // @[RationalCrossing.scala 83:29]
  wire [127:0] rsource_auto_in_d_bits_data; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_d_bits_corrupt; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_e_ready; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_in_e_valid; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_in_e_bits_sink; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_out_a_bits0_opcode; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_out_a_bits0_param; // @[RationalCrossing.scala 83:29]
  wire [3:0] rsource_auto_out_a_bits0_size; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_out_a_bits0_source; // @[RationalCrossing.scala 83:29]
  wire [36:0] rsource_auto_out_a_bits0_address; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits0_user_amba_prot_bufferable; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits0_user_amba_prot_modifiable; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits0_user_amba_prot_readalloc; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits0_user_amba_prot_writealloc; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits0_user_amba_prot_privileged; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits0_user_amba_prot_secure; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits0_user_amba_prot_fetch; // @[RationalCrossing.scala 83:29]
  wire [15:0] rsource_auto_out_a_bits0_mask; // @[RationalCrossing.scala 83:29]
  wire [127:0] rsource_auto_out_a_bits0_data; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits0_corrupt; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_out_a_bits1_opcode; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_out_a_bits1_param; // @[RationalCrossing.scala 83:29]
  wire [3:0] rsource_auto_out_a_bits1_size; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_out_a_bits1_source; // @[RationalCrossing.scala 83:29]
  wire [36:0] rsource_auto_out_a_bits1_address; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits1_user_amba_prot_bufferable; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits1_user_amba_prot_modifiable; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits1_user_amba_prot_readalloc; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits1_user_amba_prot_writealloc; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits1_user_amba_prot_privileged; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits1_user_amba_prot_secure; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits1_user_amba_prot_fetch; // @[RationalCrossing.scala 83:29]
  wire [15:0] rsource_auto_out_a_bits1_mask; // @[RationalCrossing.scala 83:29]
  wire [127:0] rsource_auto_out_a_bits1_data; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_bits1_corrupt; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_valid; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_out_a_source; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_a_ready; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_out_a_sink; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_out_b_bits0_opcode; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_out_b_bits0_param; // @[RationalCrossing.scala 83:29]
  wire [3:0] rsource_auto_out_b_bits0_size; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_out_b_bits0_source; // @[RationalCrossing.scala 83:29]
  wire [36:0] rsource_auto_out_b_bits0_address; // @[RationalCrossing.scala 83:29]
  wire [15:0] rsource_auto_out_b_bits0_mask; // @[RationalCrossing.scala 83:29]
  wire [127:0] rsource_auto_out_b_bits0_data; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_b_bits0_corrupt; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_out_b_bits1_opcode; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_out_b_bits1_param; // @[RationalCrossing.scala 83:29]
  wire [3:0] rsource_auto_out_b_bits1_size; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_out_b_bits1_source; // @[RationalCrossing.scala 83:29]
  wire [36:0] rsource_auto_out_b_bits1_address; // @[RationalCrossing.scala 83:29]
  wire [15:0] rsource_auto_out_b_bits1_mask; // @[RationalCrossing.scala 83:29]
  wire [127:0] rsource_auto_out_b_bits1_data; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_b_bits1_corrupt; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_b_valid; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_out_b_source; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_b_ready; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_out_b_sink; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_out_c_bits0_opcode; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_out_c_bits0_param; // @[RationalCrossing.scala 83:29]
  wire [3:0] rsource_auto_out_c_bits0_size; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_out_c_bits0_source; // @[RationalCrossing.scala 83:29]
  wire [36:0] rsource_auto_out_c_bits0_address; // @[RationalCrossing.scala 83:29]
  wire [127:0] rsource_auto_out_c_bits0_data; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_c_bits0_corrupt; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_out_c_bits1_opcode; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_out_c_bits1_param; // @[RationalCrossing.scala 83:29]
  wire [3:0] rsource_auto_out_c_bits1_size; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_out_c_bits1_source; // @[RationalCrossing.scala 83:29]
  wire [36:0] rsource_auto_out_c_bits1_address; // @[RationalCrossing.scala 83:29]
  wire [127:0] rsource_auto_out_c_bits1_data; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_c_bits1_corrupt; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_c_valid; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_out_c_source; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_c_ready; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_out_c_sink; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_out_d_bits0_opcode; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_out_d_bits0_param; // @[RationalCrossing.scala 83:29]
  wire [3:0] rsource_auto_out_d_bits0_size; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_out_d_bits0_source; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_out_d_bits0_sink; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_d_bits0_denied; // @[RationalCrossing.scala 83:29]
  wire [127:0] rsource_auto_out_d_bits0_data; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_d_bits0_corrupt; // @[RationalCrossing.scala 83:29]
  wire [2:0] rsource_auto_out_d_bits1_opcode; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_out_d_bits1_param; // @[RationalCrossing.scala 83:29]
  wire [3:0] rsource_auto_out_d_bits1_size; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_out_d_bits1_source; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_out_d_bits1_sink; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_d_bits1_denied; // @[RationalCrossing.scala 83:29]
  wire [127:0] rsource_auto_out_d_bits1_data; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_d_bits1_corrupt; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_d_valid; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_out_d_source; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_d_ready; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_out_d_sink; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_out_e_bits0_sink; // @[RationalCrossing.scala 83:29]
  wire [4:0] rsource_auto_out_e_bits1_sink; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_e_valid; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_out_e_source; // @[RationalCrossing.scala 83:29]
  wire  rsource_auto_out_e_ready; // @[RationalCrossing.scala 83:29]
  wire [1:0] rsource_auto_out_e_sink; // @[RationalCrossing.scala 83:29]
  wire  intsink_rf_reset; // @[Crossing.scala 75:29]
  wire  intsink_clock; // @[Crossing.scala 75:29]
  wire  intsink_auto_in_sync_0; // @[Crossing.scala 75:29]
  wire  intsink_auto_out_0; // @[Crossing.scala 75:29]
  wire  intsink_1_clock; // @[Crossing.scala 115:29]
  wire  intsink_1_reset; // @[Crossing.scala 115:29]
  wire  intsink_1_auto_in_sync_0; // @[Crossing.scala 115:29]
  wire  intsink_1_auto_in_sync_1; // @[Crossing.scala 115:29]
  wire  intsink_1_auto_out_0; // @[Crossing.scala 115:29]
  wire  intsink_1_auto_out_1; // @[Crossing.scala 115:29]
  wire  intsink_2_clock; // @[Crossing.scala 115:29]
  wire  intsink_2_reset; // @[Crossing.scala 115:29]
  wire  intsink_2_auto_in_sync_0; // @[Crossing.scala 115:29]
  wire  intsink_2_auto_in_sync_1; // @[Crossing.scala 115:29]
  wire  intsink_2_auto_out_0; // @[Crossing.scala 115:29]
  wire  intsink_2_auto_out_1; // @[Crossing.scala 115:29]
  wire  intsink_3_auto_in_sync_0; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_in_sync_1; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_in_sync_2; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_in_sync_3; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_in_sync_4; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_in_sync_5; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_in_sync_6; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_in_sync_7; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_in_sync_8; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_in_sync_9; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_in_sync_10; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_in_sync_11; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_in_sync_12; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_in_sync_13; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_in_sync_14; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_in_sync_15; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_0; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_1; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_2; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_3; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_4; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_5; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_6; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_7; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_8; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_9; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_10; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_11; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_12; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_13; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_14; // @[Crossing.scala 95:29]
  wire  intsink_3_auto_out_15; // @[Crossing.scala 95:29]
  wire  rs_auto_reset_stretcher_in_clock; // @[ResetCrossingType.scala 21:24]
  wire  rs_auto_reset_stretcher_in_reset; // @[ResetCrossingType.scala 21:24]
  wire  rs_auto_reset_stretcher_out_clock; // @[ResetCrossingType.scala 21:24]
  wire  rs_auto_reset_stretcher_out_reset; // @[ResetCrossingType.scala 21:24]
  wire  intsource_1_auto_in_0; // @[Crossing.scala 27:31]
  wire  intsource_1_auto_out_sync_0; // @[Crossing.scala 27:31]
  wire  block_during_reset_4_clock;
  wire  block_during_reset_4_reset;
  wire  block_during_reset_4_auto_int_in_0;
  wire  block_during_reset_4_auto_int_out_0;
  reg [3:0] block_during_reset_4_bundleOut_0_0_value; // @[Counter.scala 60:40]
  wire  block_during_reset_4_bundleOut_0_0_wrap_wrap = block_during_reset_4_bundleOut_0_0_value == 4'hf; // @[Counter.scala 72:24]
  reg  block_during_reset_4_bundleOut_0_0_r; // @[Reg.scala 27:20]
  wire  block_during_reset_4__bundleOut_0_0_T = ~block_during_reset_4_bundleOut_0_0_r; // @[BlockDuringReset.scala 17:41]
  wire  block_during_reset_4__bundleOut_0_0_T_1 = ~block_during_reset_4__bundleOut_0_0_T; // @[Blockable.scala 17:65]
  wire  intsource_2_auto_in_0; // @[Crossing.scala 27:31]
  wire  intsource_2_auto_out_sync_0; // @[Crossing.scala 27:31]
  wire  block_during_reset_5_clock;
  wire  block_during_reset_5_reset;
  wire  block_during_reset_5_auto_int_in_0;
  wire  block_during_reset_5_auto_int_out_0;
  reg [3:0] block_during_reset_5_bundleOut_0_0_value; // @[Counter.scala 60:40]
  wire  block_during_reset_5_bundleOut_0_0_wrap_wrap = block_during_reset_5_bundleOut_0_0_value == 4'hf; // @[Counter.scala 72:24]
  reg  block_during_reset_5_bundleOut_0_0_r; // @[Reg.scala 27:20]
  wire  block_during_reset_5__bundleOut_0_0_T = ~block_during_reset_5_bundleOut_0_0_r; // @[BlockDuringReset.scala 17:41]
  wire  block_during_reset_5__bundleOut_0_0_T_1 = ~block_during_reset_5__bundleOut_0_0_T; // @[Blockable.scala 17:65]
  wire  intsource_3_auto_in_0; // @[Crossing.scala 27:31]
  wire  intsource_3_auto_out_sync_0; // @[Crossing.scala 27:31]
  wire  block_during_reset_6_clock;
  wire  block_during_reset_6_reset;
  wire  block_during_reset_6_auto_int_in_0;
  wire  block_during_reset_6_auto_int_out_0;
  reg [3:0] block_during_reset_6_bundleOut_0_0_value; // @[Counter.scala 60:40]
  wire  block_during_reset_6_bundleOut_0_0_wrap_wrap = block_during_reset_6_bundleOut_0_0_value == 4'hf; // @[Counter.scala 72:24]
  reg  block_during_reset_6_bundleOut_0_0_r; // @[Reg.scala 27:20]
  wire  block_during_reset_6__bundleOut_0_0_T = ~block_during_reset_6_bundleOut_0_0_r; // @[BlockDuringReset.scala 17:41]
  wire  block_during_reset_6__bundleOut_0_0_T_1 = ~block_during_reset_6__bundleOut_0_0_T; // @[Blockable.scala 17:65]
  wire  intsource_4_auto_in_0; // @[Crossing.scala 27:31]
  wire  intsource_4_auto_out_sync_0; // @[Crossing.scala 27:31]
  wire  trace_clock;
  wire  trace_reset;
  wire  trace_auto_in_0_valid;
  wire [39:0] trace_auto_in_0_iaddr;
  wire [31:0] trace_auto_in_0_insn;
  wire [2:0] trace_auto_in_0_priv;
  wire  trace_auto_in_0_exception;
  wire  trace_auto_in_0_interrupt;
  wire  trace_auto_out_0_valid;
  wire [39:0] trace_auto_out_0_iaddr;
  wire [31:0] trace_auto_out_0_insn;
  wire [2:0] trace_auto_out_0_priv;
  wire  trace_auto_out_0_exception;
  wire  trace_auto_out_0_interrupt;
  reg [3:0] trace_outputs_broadcast_value; // @[Counter.scala 60:40]
  wire  trace_outputs_broadcast_wrap_wrap = trace_outputs_broadcast_value == 4'hf; // @[Counter.scala 72:24]
  reg  trace_outputs_broadcast_r; // @[Reg.scala 27:20]
  wire  trace__outputs_broadcast_T = ~trace_outputs_broadcast_r; // @[BlockDuringReset.scala 17:41]
  RHEA__RocketTile tile ( // @[HasTiles.scala 274:53]
    .rf_reset(tile_rf_reset),
    .clock(tile_clock),
    .reset(tile_reset),
    .auto_broadcast_out_0_valid(tile_auto_broadcast_out_0_valid),
    .auto_broadcast_out_0_iaddr(tile_auto_broadcast_out_0_iaddr),
    .auto_broadcast_out_0_insn(tile_auto_broadcast_out_0_insn),
    .auto_broadcast_out_0_priv(tile_auto_broadcast_out_0_priv),
    .auto_broadcast_out_0_exception(tile_auto_broadcast_out_0_exception),
    .auto_broadcast_out_0_interrupt(tile_auto_broadcast_out_0_interrupt),
    .auto_debug_out_0(tile_auto_debug_out_0),
    .auto_wfi_out_0(tile_auto_wfi_out_0),
    .auto_cease_out_0(tile_auto_cease_out_0),
    .auto_int_sink_in_3_0(tile_auto_int_sink_in_3_0),
    .auto_int_sink_in_3_1(tile_auto_int_sink_in_3_1),
    .auto_int_sink_in_3_2(tile_auto_int_sink_in_3_2),
    .auto_int_sink_in_3_3(tile_auto_int_sink_in_3_3),
    .auto_int_sink_in_3_4(tile_auto_int_sink_in_3_4),
    .auto_int_sink_in_3_5(tile_auto_int_sink_in_3_5),
    .auto_int_sink_in_3_6(tile_auto_int_sink_in_3_6),
    .auto_int_sink_in_3_7(tile_auto_int_sink_in_3_7),
    .auto_int_sink_in_3_8(tile_auto_int_sink_in_3_8),
    .auto_int_sink_in_3_9(tile_auto_int_sink_in_3_9),
    .auto_int_sink_in_3_10(tile_auto_int_sink_in_3_10),
    .auto_int_sink_in_3_11(tile_auto_int_sink_in_3_11),
    .auto_int_sink_in_3_12(tile_auto_int_sink_in_3_12),
    .auto_int_sink_in_3_13(tile_auto_int_sink_in_3_13),
    .auto_int_sink_in_3_14(tile_auto_int_sink_in_3_14),
    .auto_int_sink_in_3_15(tile_auto_int_sink_in_3_15),
    .auto_int_sink_in_2_0(tile_auto_int_sink_in_2_0),
    .auto_int_sink_in_2_1(tile_auto_int_sink_in_2_1),
    .auto_int_sink_in_1_0(tile_auto_int_sink_in_1_0),
    .auto_int_sink_in_1_1(tile_auto_int_sink_in_1_1),
    .auto_int_sink_in_0_0(tile_auto_int_sink_in_0_0),
    .auto_bpwatch_out_0_valid_0(tile_auto_bpwatch_out_0_valid_0),
    .auto_bpwatch_out_0_action(tile_auto_bpwatch_out_0_action),
    .auto_bpwatch_out_1_valid_0(tile_auto_bpwatch_out_1_valid_0),
    .auto_bpwatch_out_1_action(tile_auto_bpwatch_out_1_action),
    .auto_bpwatch_out_2_valid_0(tile_auto_bpwatch_out_2_valid_0),
    .auto_bpwatch_out_2_action(tile_auto_bpwatch_out_2_action),
    .auto_bpwatch_out_3_valid_0(tile_auto_bpwatch_out_3_valid_0),
    .auto_bpwatch_out_3_action(tile_auto_bpwatch_out_3_action),
    .auto_bpwatch_out_4_valid_0(tile_auto_bpwatch_out_4_valid_0),
    .auto_bpwatch_out_4_action(tile_auto_bpwatch_out_4_action),
    .auto_bpwatch_out_5_valid_0(tile_auto_bpwatch_out_5_valid_0),
    .auto_bpwatch_out_5_action(tile_auto_bpwatch_out_5_action),
    .auto_bpwatch_out_6_valid_0(tile_auto_bpwatch_out_6_valid_0),
    .auto_bpwatch_out_6_action(tile_auto_bpwatch_out_6_action),
    .auto_bpwatch_out_7_valid_0(tile_auto_bpwatch_out_7_valid_0),
    .auto_bpwatch_out_7_action(tile_auto_bpwatch_out_7_action),
    .auto_bpwatch_out_8_valid_0(tile_auto_bpwatch_out_8_valid_0),
    .auto_bpwatch_out_8_action(tile_auto_bpwatch_out_8_action),
    .auto_bpwatch_out_9_valid_0(tile_auto_bpwatch_out_9_valid_0),
    .auto_bpwatch_out_9_action(tile_auto_bpwatch_out_9_action),
    .auto_bpwatch_out_10_valid_0(tile_auto_bpwatch_out_10_valid_0),
    .auto_bpwatch_out_10_action(tile_auto_bpwatch_out_10_action),
    .auto_bpwatch_out_11_valid_0(tile_auto_bpwatch_out_11_valid_0),
    .auto_bpwatch_out_11_action(tile_auto_bpwatch_out_11_action),
    .auto_bpwatch_out_12_valid_0(tile_auto_bpwatch_out_12_valid_0),
    .auto_bpwatch_out_12_action(tile_auto_bpwatch_out_12_action),
    .auto_bpwatch_out_13_valid_0(tile_auto_bpwatch_out_13_valid_0),
    .auto_bpwatch_out_13_action(tile_auto_bpwatch_out_13_action),
    .auto_bpwatch_out_14_valid_0(tile_auto_bpwatch_out_14_valid_0),
    .auto_bpwatch_out_14_action(tile_auto_bpwatch_out_14_action),
    .auto_bpwatch_out_15_valid_0(tile_auto_bpwatch_out_15_valid_0),
    .auto_bpwatch_out_15_action(tile_auto_bpwatch_out_15_action),
    .auto_trace_aux_in_stall(tile_auto_trace_aux_in_stall),
    .auto_nmi_in_rnmi(tile_auto_nmi_in_rnmi),
    .auto_nmi_in_rnmi_interrupt_vector(tile_auto_nmi_in_rnmi_interrupt_vector),
    .auto_nmi_in_rnmi_exception_vector(tile_auto_nmi_in_rnmi_exception_vector),
    .auto_reset_vector_in(tile_auto_reset_vector_in),
    .auto_hartid_in(tile_auto_hartid_in),
    .auto_tl_other_masters_out_a_ready(tile_auto_tl_other_masters_out_a_ready),
    .auto_tl_other_masters_out_a_valid(tile_auto_tl_other_masters_out_a_valid),
    .auto_tl_other_masters_out_a_bits_opcode(tile_auto_tl_other_masters_out_a_bits_opcode),
    .auto_tl_other_masters_out_a_bits_param(tile_auto_tl_other_masters_out_a_bits_param),
    .auto_tl_other_masters_out_a_bits_size(tile_auto_tl_other_masters_out_a_bits_size),
    .auto_tl_other_masters_out_a_bits_source(tile_auto_tl_other_masters_out_a_bits_source),
    .auto_tl_other_masters_out_a_bits_address(tile_auto_tl_other_masters_out_a_bits_address),
    .auto_tl_other_masters_out_a_bits_user_amba_prot_bufferable(
      tile_auto_tl_other_masters_out_a_bits_user_amba_prot_bufferable),
    .auto_tl_other_masters_out_a_bits_user_amba_prot_modifiable(
      tile_auto_tl_other_masters_out_a_bits_user_amba_prot_modifiable),
    .auto_tl_other_masters_out_a_bits_user_amba_prot_readalloc(
      tile_auto_tl_other_masters_out_a_bits_user_amba_prot_readalloc),
    .auto_tl_other_masters_out_a_bits_user_amba_prot_writealloc(
      tile_auto_tl_other_masters_out_a_bits_user_amba_prot_writealloc),
    .auto_tl_other_masters_out_a_bits_user_amba_prot_privileged(
      tile_auto_tl_other_masters_out_a_bits_user_amba_prot_privileged),
    .auto_tl_other_masters_out_a_bits_user_amba_prot_secure(tile_auto_tl_other_masters_out_a_bits_user_amba_prot_secure)
      ,
    .auto_tl_other_masters_out_a_bits_user_amba_prot_fetch(tile_auto_tl_other_masters_out_a_bits_user_amba_prot_fetch),
    .auto_tl_other_masters_out_a_bits_mask(tile_auto_tl_other_masters_out_a_bits_mask),
    .auto_tl_other_masters_out_a_bits_data(tile_auto_tl_other_masters_out_a_bits_data),
    .auto_tl_other_masters_out_a_bits_corrupt(tile_auto_tl_other_masters_out_a_bits_corrupt),
    .auto_tl_other_masters_out_b_ready(tile_auto_tl_other_masters_out_b_ready),
    .auto_tl_other_masters_out_b_valid(tile_auto_tl_other_masters_out_b_valid),
    .auto_tl_other_masters_out_b_bits_opcode(tile_auto_tl_other_masters_out_b_bits_opcode),
    .auto_tl_other_masters_out_b_bits_param(tile_auto_tl_other_masters_out_b_bits_param),
    .auto_tl_other_masters_out_b_bits_size(tile_auto_tl_other_masters_out_b_bits_size),
    .auto_tl_other_masters_out_b_bits_source(tile_auto_tl_other_masters_out_b_bits_source),
    .auto_tl_other_masters_out_b_bits_address(tile_auto_tl_other_masters_out_b_bits_address),
    .auto_tl_other_masters_out_b_bits_mask(tile_auto_tl_other_masters_out_b_bits_mask),
    .auto_tl_other_masters_out_b_bits_data(tile_auto_tl_other_masters_out_b_bits_data),
    .auto_tl_other_masters_out_b_bits_corrupt(tile_auto_tl_other_masters_out_b_bits_corrupt),
    .auto_tl_other_masters_out_c_ready(tile_auto_tl_other_masters_out_c_ready),
    .auto_tl_other_masters_out_c_valid(tile_auto_tl_other_masters_out_c_valid),
    .auto_tl_other_masters_out_c_bits_opcode(tile_auto_tl_other_masters_out_c_bits_opcode),
    .auto_tl_other_masters_out_c_bits_param(tile_auto_tl_other_masters_out_c_bits_param),
    .auto_tl_other_masters_out_c_bits_size(tile_auto_tl_other_masters_out_c_bits_size),
    .auto_tl_other_masters_out_c_bits_source(tile_auto_tl_other_masters_out_c_bits_source),
    .auto_tl_other_masters_out_c_bits_address(tile_auto_tl_other_masters_out_c_bits_address),
    .auto_tl_other_masters_out_c_bits_data(tile_auto_tl_other_masters_out_c_bits_data),
    .auto_tl_other_masters_out_c_bits_corrupt(tile_auto_tl_other_masters_out_c_bits_corrupt),
    .auto_tl_other_masters_out_d_ready(tile_auto_tl_other_masters_out_d_ready),
    .auto_tl_other_masters_out_d_valid(tile_auto_tl_other_masters_out_d_valid),
    .auto_tl_other_masters_out_d_bits_opcode(tile_auto_tl_other_masters_out_d_bits_opcode),
    .auto_tl_other_masters_out_d_bits_param(tile_auto_tl_other_masters_out_d_bits_param),
    .auto_tl_other_masters_out_d_bits_size(tile_auto_tl_other_masters_out_d_bits_size),
    .auto_tl_other_masters_out_d_bits_source(tile_auto_tl_other_masters_out_d_bits_source),
    .auto_tl_other_masters_out_d_bits_sink(tile_auto_tl_other_masters_out_d_bits_sink),
    .auto_tl_other_masters_out_d_bits_denied(tile_auto_tl_other_masters_out_d_bits_denied),
    .auto_tl_other_masters_out_d_bits_data(tile_auto_tl_other_masters_out_d_bits_data),
    .auto_tl_other_masters_out_d_bits_corrupt(tile_auto_tl_other_masters_out_d_bits_corrupt),
    .auto_tl_other_masters_out_e_ready(tile_auto_tl_other_masters_out_e_ready),
    .auto_tl_other_masters_out_e_valid(tile_auto_tl_other_masters_out_e_valid),
    .auto_tl_other_masters_out_e_bits_sink(tile_auto_tl_other_masters_out_e_bits_sink),
    .psd_test_clock_enable(tile_psd_test_clock_enable)
  );
  RHEA__TLRationalCrossingSource_2 rsource ( // @[RationalCrossing.scala 83:29]
    .rf_reset(rsource_rf_reset),
    .clock(rsource_clock),
    .reset(rsource_reset),
    .auto_in_a_ready(rsource_auto_in_a_ready),
    .auto_in_a_valid(rsource_auto_in_a_valid),
    .auto_in_a_bits_opcode(rsource_auto_in_a_bits_opcode),
    .auto_in_a_bits_param(rsource_auto_in_a_bits_param),
    .auto_in_a_bits_size(rsource_auto_in_a_bits_size),
    .auto_in_a_bits_source(rsource_auto_in_a_bits_source),
    .auto_in_a_bits_address(rsource_auto_in_a_bits_address),
    .auto_in_a_bits_user_amba_prot_bufferable(rsource_auto_in_a_bits_user_amba_prot_bufferable),
    .auto_in_a_bits_user_amba_prot_modifiable(rsource_auto_in_a_bits_user_amba_prot_modifiable),
    .auto_in_a_bits_user_amba_prot_readalloc(rsource_auto_in_a_bits_user_amba_prot_readalloc),
    .auto_in_a_bits_user_amba_prot_writealloc(rsource_auto_in_a_bits_user_amba_prot_writealloc),
    .auto_in_a_bits_user_amba_prot_privileged(rsource_auto_in_a_bits_user_amba_prot_privileged),
    .auto_in_a_bits_user_amba_prot_secure(rsource_auto_in_a_bits_user_amba_prot_secure),
    .auto_in_a_bits_user_amba_prot_fetch(rsource_auto_in_a_bits_user_amba_prot_fetch),
    .auto_in_a_bits_mask(rsource_auto_in_a_bits_mask),
    .auto_in_a_bits_data(rsource_auto_in_a_bits_data),
    .auto_in_a_bits_corrupt(rsource_auto_in_a_bits_corrupt),
    .auto_in_b_ready(rsource_auto_in_b_ready),
    .auto_in_b_valid(rsource_auto_in_b_valid),
    .auto_in_b_bits_opcode(rsource_auto_in_b_bits_opcode),
    .auto_in_b_bits_param(rsource_auto_in_b_bits_param),
    .auto_in_b_bits_size(rsource_auto_in_b_bits_size),
    .auto_in_b_bits_source(rsource_auto_in_b_bits_source),
    .auto_in_b_bits_address(rsource_auto_in_b_bits_address),
    .auto_in_b_bits_mask(rsource_auto_in_b_bits_mask),
    .auto_in_b_bits_data(rsource_auto_in_b_bits_data),
    .auto_in_b_bits_corrupt(rsource_auto_in_b_bits_corrupt),
    .auto_in_c_ready(rsource_auto_in_c_ready),
    .auto_in_c_valid(rsource_auto_in_c_valid),
    .auto_in_c_bits_opcode(rsource_auto_in_c_bits_opcode),
    .auto_in_c_bits_param(rsource_auto_in_c_bits_param),
    .auto_in_c_bits_size(rsource_auto_in_c_bits_size),
    .auto_in_c_bits_source(rsource_auto_in_c_bits_source),
    .auto_in_c_bits_address(rsource_auto_in_c_bits_address),
    .auto_in_c_bits_data(rsource_auto_in_c_bits_data),
    .auto_in_c_bits_corrupt(rsource_auto_in_c_bits_corrupt),
    .auto_in_d_ready(rsource_auto_in_d_ready),
    .auto_in_d_valid(rsource_auto_in_d_valid),
    .auto_in_d_bits_opcode(rsource_auto_in_d_bits_opcode),
    .auto_in_d_bits_param(rsource_auto_in_d_bits_param),
    .auto_in_d_bits_size(rsource_auto_in_d_bits_size),
    .auto_in_d_bits_source(rsource_auto_in_d_bits_source),
    .auto_in_d_bits_sink(rsource_auto_in_d_bits_sink),
    .auto_in_d_bits_denied(rsource_auto_in_d_bits_denied),
    .auto_in_d_bits_data(rsource_auto_in_d_bits_data),
    .auto_in_d_bits_corrupt(rsource_auto_in_d_bits_corrupt),
    .auto_in_e_ready(rsource_auto_in_e_ready),
    .auto_in_e_valid(rsource_auto_in_e_valid),
    .auto_in_e_bits_sink(rsource_auto_in_e_bits_sink),
    .auto_out_a_bits0_opcode(rsource_auto_out_a_bits0_opcode),
    .auto_out_a_bits0_param(rsource_auto_out_a_bits0_param),
    .auto_out_a_bits0_size(rsource_auto_out_a_bits0_size),
    .auto_out_a_bits0_source(rsource_auto_out_a_bits0_source),
    .auto_out_a_bits0_address(rsource_auto_out_a_bits0_address),
    .auto_out_a_bits0_user_amba_prot_bufferable(rsource_auto_out_a_bits0_user_amba_prot_bufferable),
    .auto_out_a_bits0_user_amba_prot_modifiable(rsource_auto_out_a_bits0_user_amba_prot_modifiable),
    .auto_out_a_bits0_user_amba_prot_readalloc(rsource_auto_out_a_bits0_user_amba_prot_readalloc),
    .auto_out_a_bits0_user_amba_prot_writealloc(rsource_auto_out_a_bits0_user_amba_prot_writealloc),
    .auto_out_a_bits0_user_amba_prot_privileged(rsource_auto_out_a_bits0_user_amba_prot_privileged),
    .auto_out_a_bits0_user_amba_prot_secure(rsource_auto_out_a_bits0_user_amba_prot_secure),
    .auto_out_a_bits0_user_amba_prot_fetch(rsource_auto_out_a_bits0_user_amba_prot_fetch),
    .auto_out_a_bits0_mask(rsource_auto_out_a_bits0_mask),
    .auto_out_a_bits0_data(rsource_auto_out_a_bits0_data),
    .auto_out_a_bits0_corrupt(rsource_auto_out_a_bits0_corrupt),
    .auto_out_a_bits1_opcode(rsource_auto_out_a_bits1_opcode),
    .auto_out_a_bits1_param(rsource_auto_out_a_bits1_param),
    .auto_out_a_bits1_size(rsource_auto_out_a_bits1_size),
    .auto_out_a_bits1_source(rsource_auto_out_a_bits1_source),
    .auto_out_a_bits1_address(rsource_auto_out_a_bits1_address),
    .auto_out_a_bits1_user_amba_prot_bufferable(rsource_auto_out_a_bits1_user_amba_prot_bufferable),
    .auto_out_a_bits1_user_amba_prot_modifiable(rsource_auto_out_a_bits1_user_amba_prot_modifiable),
    .auto_out_a_bits1_user_amba_prot_readalloc(rsource_auto_out_a_bits1_user_amba_prot_readalloc),
    .auto_out_a_bits1_user_amba_prot_writealloc(rsource_auto_out_a_bits1_user_amba_prot_writealloc),
    .auto_out_a_bits1_user_amba_prot_privileged(rsource_auto_out_a_bits1_user_amba_prot_privileged),
    .auto_out_a_bits1_user_amba_prot_secure(rsource_auto_out_a_bits1_user_amba_prot_secure),
    .auto_out_a_bits1_user_amba_prot_fetch(rsource_auto_out_a_bits1_user_amba_prot_fetch),
    .auto_out_a_bits1_mask(rsource_auto_out_a_bits1_mask),
    .auto_out_a_bits1_data(rsource_auto_out_a_bits1_data),
    .auto_out_a_bits1_corrupt(rsource_auto_out_a_bits1_corrupt),
    .auto_out_a_valid(rsource_auto_out_a_valid),
    .auto_out_a_source(rsource_auto_out_a_source),
    .auto_out_a_ready(rsource_auto_out_a_ready),
    .auto_out_a_sink(rsource_auto_out_a_sink),
    .auto_out_b_bits0_opcode(rsource_auto_out_b_bits0_opcode),
    .auto_out_b_bits0_param(rsource_auto_out_b_bits0_param),
    .auto_out_b_bits0_size(rsource_auto_out_b_bits0_size),
    .auto_out_b_bits0_source(rsource_auto_out_b_bits0_source),
    .auto_out_b_bits0_address(rsource_auto_out_b_bits0_address),
    .auto_out_b_bits0_mask(rsource_auto_out_b_bits0_mask),
    .auto_out_b_bits0_data(rsource_auto_out_b_bits0_data),
    .auto_out_b_bits0_corrupt(rsource_auto_out_b_bits0_corrupt),
    .auto_out_b_bits1_opcode(rsource_auto_out_b_bits1_opcode),
    .auto_out_b_bits1_param(rsource_auto_out_b_bits1_param),
    .auto_out_b_bits1_size(rsource_auto_out_b_bits1_size),
    .auto_out_b_bits1_source(rsource_auto_out_b_bits1_source),
    .auto_out_b_bits1_address(rsource_auto_out_b_bits1_address),
    .auto_out_b_bits1_mask(rsource_auto_out_b_bits1_mask),
    .auto_out_b_bits1_data(rsource_auto_out_b_bits1_data),
    .auto_out_b_bits1_corrupt(rsource_auto_out_b_bits1_corrupt),
    .auto_out_b_valid(rsource_auto_out_b_valid),
    .auto_out_b_source(rsource_auto_out_b_source),
    .auto_out_b_ready(rsource_auto_out_b_ready),
    .auto_out_b_sink(rsource_auto_out_b_sink),
    .auto_out_c_bits0_opcode(rsource_auto_out_c_bits0_opcode),
    .auto_out_c_bits0_param(rsource_auto_out_c_bits0_param),
    .auto_out_c_bits0_size(rsource_auto_out_c_bits0_size),
    .auto_out_c_bits0_source(rsource_auto_out_c_bits0_source),
    .auto_out_c_bits0_address(rsource_auto_out_c_bits0_address),
    .auto_out_c_bits0_data(rsource_auto_out_c_bits0_data),
    .auto_out_c_bits0_corrupt(rsource_auto_out_c_bits0_corrupt),
    .auto_out_c_bits1_opcode(rsource_auto_out_c_bits1_opcode),
    .auto_out_c_bits1_param(rsource_auto_out_c_bits1_param),
    .auto_out_c_bits1_size(rsource_auto_out_c_bits1_size),
    .auto_out_c_bits1_source(rsource_auto_out_c_bits1_source),
    .auto_out_c_bits1_address(rsource_auto_out_c_bits1_address),
    .auto_out_c_bits1_data(rsource_auto_out_c_bits1_data),
    .auto_out_c_bits1_corrupt(rsource_auto_out_c_bits1_corrupt),
    .auto_out_c_valid(rsource_auto_out_c_valid),
    .auto_out_c_source(rsource_auto_out_c_source),
    .auto_out_c_ready(rsource_auto_out_c_ready),
    .auto_out_c_sink(rsource_auto_out_c_sink),
    .auto_out_d_bits0_opcode(rsource_auto_out_d_bits0_opcode),
    .auto_out_d_bits0_param(rsource_auto_out_d_bits0_param),
    .auto_out_d_bits0_size(rsource_auto_out_d_bits0_size),
    .auto_out_d_bits0_source(rsource_auto_out_d_bits0_source),
    .auto_out_d_bits0_sink(rsource_auto_out_d_bits0_sink),
    .auto_out_d_bits0_denied(rsource_auto_out_d_bits0_denied),
    .auto_out_d_bits0_data(rsource_auto_out_d_bits0_data),
    .auto_out_d_bits0_corrupt(rsource_auto_out_d_bits0_corrupt),
    .auto_out_d_bits1_opcode(rsource_auto_out_d_bits1_opcode),
    .auto_out_d_bits1_param(rsource_auto_out_d_bits1_param),
    .auto_out_d_bits1_size(rsource_auto_out_d_bits1_size),
    .auto_out_d_bits1_source(rsource_auto_out_d_bits1_source),
    .auto_out_d_bits1_sink(rsource_auto_out_d_bits1_sink),
    .auto_out_d_bits1_denied(rsource_auto_out_d_bits1_denied),
    .auto_out_d_bits1_data(rsource_auto_out_d_bits1_data),
    .auto_out_d_bits1_corrupt(rsource_auto_out_d_bits1_corrupt),
    .auto_out_d_valid(rsource_auto_out_d_valid),
    .auto_out_d_source(rsource_auto_out_d_source),
    .auto_out_d_ready(rsource_auto_out_d_ready),
    .auto_out_d_sink(rsource_auto_out_d_sink),
    .auto_out_e_bits0_sink(rsource_auto_out_e_bits0_sink),
    .auto_out_e_bits1_sink(rsource_auto_out_e_bits1_sink),
    .auto_out_e_valid(rsource_auto_out_e_valid),
    .auto_out_e_source(rsource_auto_out_e_source),
    .auto_out_e_ready(rsource_auto_out_e_ready),
    .auto_out_e_sink(rsource_auto_out_e_sink)
  );
  RHEA__IntSyncAsyncCrossingSink intsink ( // @[Crossing.scala 75:29]
    .rf_reset(intsink_rf_reset),
    .clock(intsink_clock),
    .auto_in_sync_0(intsink_auto_in_sync_0),
    .auto_out_0(intsink_auto_out_0)
  );
  RHEA__IntSyncRationalCrossingSink intsink_1 ( // @[Crossing.scala 115:29]
    .clock(intsink_1_clock),
    .reset(intsink_1_reset),
    .auto_in_sync_0(intsink_1_auto_in_sync_0),
    .auto_in_sync_1(intsink_1_auto_in_sync_1),
    .auto_out_0(intsink_1_auto_out_0),
    .auto_out_1(intsink_1_auto_out_1)
  );
  RHEA__IntSyncRationalCrossingSink intsink_2 ( // @[Crossing.scala 115:29]
    .clock(intsink_2_clock),
    .reset(intsink_2_reset),
    .auto_in_sync_0(intsink_2_auto_in_sync_0),
    .auto_in_sync_1(intsink_2_auto_in_sync_1),
    .auto_out_0(intsink_2_auto_out_0),
    .auto_out_1(intsink_2_auto_out_1)
  );
  RHEA__IntSyncSyncCrossingSink intsink_3 ( // @[Crossing.scala 95:29]
    .auto_in_sync_0(intsink_3_auto_in_sync_0),
    .auto_in_sync_1(intsink_3_auto_in_sync_1),
    .auto_in_sync_2(intsink_3_auto_in_sync_2),
    .auto_in_sync_3(intsink_3_auto_in_sync_3),
    .auto_in_sync_4(intsink_3_auto_in_sync_4),
    .auto_in_sync_5(intsink_3_auto_in_sync_5),
    .auto_in_sync_6(intsink_3_auto_in_sync_6),
    .auto_in_sync_7(intsink_3_auto_in_sync_7),
    .auto_in_sync_8(intsink_3_auto_in_sync_8),
    .auto_in_sync_9(intsink_3_auto_in_sync_9),
    .auto_in_sync_10(intsink_3_auto_in_sync_10),
    .auto_in_sync_11(intsink_3_auto_in_sync_11),
    .auto_in_sync_12(intsink_3_auto_in_sync_12),
    .auto_in_sync_13(intsink_3_auto_in_sync_13),
    .auto_in_sync_14(intsink_3_auto_in_sync_14),
    .auto_in_sync_15(intsink_3_auto_in_sync_15),
    .auto_out_0(intsink_3_auto_out_0),
    .auto_out_1(intsink_3_auto_out_1),
    .auto_out_2(intsink_3_auto_out_2),
    .auto_out_3(intsink_3_auto_out_3),
    .auto_out_4(intsink_3_auto_out_4),
    .auto_out_5(intsink_3_auto_out_5),
    .auto_out_6(intsink_3_auto_out_6),
    .auto_out_7(intsink_3_auto_out_7),
    .auto_out_8(intsink_3_auto_out_8),
    .auto_out_9(intsink_3_auto_out_9),
    .auto_out_10(intsink_3_auto_out_10),
    .auto_out_11(intsink_3_auto_out_11),
    .auto_out_12(intsink_3_auto_out_12),
    .auto_out_13(intsink_3_auto_out_13),
    .auto_out_14(intsink_3_auto_out_14),
    .auto_out_15(intsink_3_auto_out_15)
  );
  RHEA__ResetStretcher rs ( // @[ResetCrossingType.scala 21:24]
    .auto_reset_stretcher_in_clock(rs_auto_reset_stretcher_in_clock),
    .auto_reset_stretcher_in_reset(rs_auto_reset_stretcher_in_reset),
    .auto_reset_stretcher_out_clock(rs_auto_reset_stretcher_out_clock),
    .auto_reset_stretcher_out_reset(rs_auto_reset_stretcher_out_reset)
  );
  RHEA__IntSyncCrossingSource_1 intsource_1 ( // @[Crossing.scala 27:31]
    .auto_in_0(intsource_1_auto_in_0),
    .auto_out_sync_0(intsource_1_auto_out_sync_0)
  );
  RHEA__IntSyncCrossingSource_1 intsource_2 ( // @[Crossing.scala 27:31]
    .auto_in_0(intsource_2_auto_in_0),
    .auto_out_sync_0(intsource_2_auto_out_sync_0)
  );
  RHEA__IntSyncCrossingSource_1 intsource_3 ( // @[Crossing.scala 27:31]
    .auto_in_0(intsource_3_auto_in_0),
    .auto_out_sync_0(intsource_3_auto_out_sync_0)
  );
  RHEA__IntSyncCrossingSource_1 intsource_4 ( // @[Crossing.scala 27:31]
    .auto_in_0(intsource_4_auto_in_0),
    .auto_out_sync_0(intsource_4_auto_out_sync_0)
  );
  assign tile_rf_reset = rf_reset;
  assign tile_reset_domain_auto_tile_broadcast_out_0_valid = tile_auto_broadcast_out_0_valid; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_broadcast_out_0_iaddr = tile_auto_broadcast_out_0_iaddr; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_broadcast_out_0_insn = tile_auto_broadcast_out_0_insn; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_broadcast_out_0_priv = tile_auto_broadcast_out_0_priv; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_broadcast_out_0_exception = tile_auto_broadcast_out_0_exception; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_broadcast_out_0_interrupt = tile_auto_broadcast_out_0_interrupt; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_0_valid_0 = tile_auto_bpwatch_out_0_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_0_action = tile_auto_bpwatch_out_0_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_1_valid_0 = tile_auto_bpwatch_out_1_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_1_action = tile_auto_bpwatch_out_1_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_2_valid_0 = tile_auto_bpwatch_out_2_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_2_action = tile_auto_bpwatch_out_2_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_3_valid_0 = tile_auto_bpwatch_out_3_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_3_action = tile_auto_bpwatch_out_3_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_4_valid_0 = tile_auto_bpwatch_out_4_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_4_action = tile_auto_bpwatch_out_4_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_5_valid_0 = tile_auto_bpwatch_out_5_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_5_action = tile_auto_bpwatch_out_5_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_6_valid_0 = tile_auto_bpwatch_out_6_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_6_action = tile_auto_bpwatch_out_6_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_7_valid_0 = tile_auto_bpwatch_out_7_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_7_action = tile_auto_bpwatch_out_7_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_8_valid_0 = tile_auto_bpwatch_out_8_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_8_action = tile_auto_bpwatch_out_8_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_9_valid_0 = tile_auto_bpwatch_out_9_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_9_action = tile_auto_bpwatch_out_9_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_10_valid_0 = tile_auto_bpwatch_out_10_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_10_action = tile_auto_bpwatch_out_10_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_11_valid_0 = tile_auto_bpwatch_out_11_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_11_action = tile_auto_bpwatch_out_11_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_12_valid_0 = tile_auto_bpwatch_out_12_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_12_action = tile_auto_bpwatch_out_12_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_13_valid_0 = tile_auto_bpwatch_out_13_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_13_action = tile_auto_bpwatch_out_13_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_14_valid_0 = tile_auto_bpwatch_out_14_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_14_action = tile_auto_bpwatch_out_14_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_15_valid_0 = tile_auto_bpwatch_out_15_valid_0; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_tile_bpwatch_out_15_action = tile_auto_bpwatch_out_15_action; // @[LazyModule.scala 390:12]
  assign tile_reset_domain_auto_int_out_xing_out_3_0 = tile_auto_debug_out_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_int_out_xing_out_2_0 = tile_auto_cease_out_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_int_out_xing_out_1_0 = tile_auto_wfi_out_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_valid = tile_auto_tl_other_masters_out_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_bits_opcode = tile_auto_tl_other_masters_out_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_bits_param = tile_auto_tl_other_masters_out_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_bits_size = tile_auto_tl_other_masters_out_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_bits_source = tile_auto_tl_other_masters_out_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_bits_address = tile_auto_tl_other_masters_out_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_bufferable =
    tile_auto_tl_other_masters_out_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_modifiable =
    tile_auto_tl_other_masters_out_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_readalloc =
    tile_auto_tl_other_masters_out_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_writealloc =
    tile_auto_tl_other_masters_out_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_privileged =
    tile_auto_tl_other_masters_out_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_secure =
    tile_auto_tl_other_masters_out_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_fetch =
    tile_auto_tl_other_masters_out_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_bits_mask = tile_auto_tl_other_masters_out_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_bits_data = tile_auto_tl_other_masters_out_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_bits_corrupt = tile_auto_tl_other_masters_out_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_b_ready = tile_auto_tl_other_masters_out_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_c_valid = tile_auto_tl_other_masters_out_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_c_bits_opcode = tile_auto_tl_other_masters_out_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_c_bits_param = tile_auto_tl_other_masters_out_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_c_bits_size = tile_auto_tl_other_masters_out_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_c_bits_source = tile_auto_tl_other_masters_out_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_c_bits_address = tile_auto_tl_other_masters_out_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_c_bits_data = tile_auto_tl_other_masters_out_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_c_bits_corrupt = tile_auto_tl_other_masters_out_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_d_ready = tile_auto_tl_other_masters_out_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_e_valid = tile_auto_tl_other_masters_out_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tl_out_xing_out_e_bits_sink = tile_auto_tl_other_masters_out_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_clock = tile_reset_domain_auto_clock_in_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 388:16]
  assign tile_reset = tile_reset_domain_auto_clock_in_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_0 = tile_reset_domain_auto_tile_int_sink_in_3_0; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_1 = tile_reset_domain_auto_tile_int_sink_in_3_1; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_2 = tile_reset_domain_auto_tile_int_sink_in_3_2; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_3 = tile_reset_domain_auto_tile_int_sink_in_3_3; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_4 = tile_reset_domain_auto_tile_int_sink_in_3_4; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_5 = tile_reset_domain_auto_tile_int_sink_in_3_5; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_6 = tile_reset_domain_auto_tile_int_sink_in_3_6; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_7 = tile_reset_domain_auto_tile_int_sink_in_3_7; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_8 = tile_reset_domain_auto_tile_int_sink_in_3_8; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_9 = tile_reset_domain_auto_tile_int_sink_in_3_9; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_10 = tile_reset_domain_auto_tile_int_sink_in_3_10; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_11 = tile_reset_domain_auto_tile_int_sink_in_3_11; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_12 = tile_reset_domain_auto_tile_int_sink_in_3_12; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_13 = tile_reset_domain_auto_tile_int_sink_in_3_13; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_14 = tile_reset_domain_auto_tile_int_sink_in_3_14; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_3_15 = tile_reset_domain_auto_tile_int_sink_in_3_15; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_2_0 = tile_reset_domain_auto_tile_int_sink_in_2_0; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_2_1 = tile_reset_domain_auto_tile_int_sink_in_2_1; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_1_0 = tile_reset_domain_auto_tile_int_sink_in_1_0; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_1_1 = tile_reset_domain_auto_tile_int_sink_in_1_1; // @[LazyModule.scala 388:16]
  assign tile_auto_int_sink_in_0_0 = tile_reset_domain_auto_tile_int_sink_in_0_0; // @[LazyModule.scala 388:16]
  assign tile_auto_trace_aux_in_stall = tile_reset_domain_auto_tile_trace_aux_in_stall; // @[LazyModule.scala 388:16]
  assign tile_auto_nmi_in_rnmi = tile_reset_domain_auto_tile_nmi_in_rnmi; // @[LazyModule.scala 388:16]
  assign tile_auto_nmi_in_rnmi_interrupt_vector = tile_reset_domain_auto_tile_nmi_in_rnmi_interrupt_vector; // @[LazyModule.scala 388:16]
  assign tile_auto_nmi_in_rnmi_exception_vector = tile_reset_domain_auto_tile_nmi_in_rnmi_exception_vector; // @[LazyModule.scala 388:16]
  assign tile_auto_reset_vector_in = tile_reset_domain_auto_tile_reset_vector_in; // @[LazyModule.scala 388:16]
  assign tile_auto_hartid_in = tile_reset_domain_auto_tile_hartid_in; // @[LazyModule.scala 388:16]
  assign tile_auto_tl_other_masters_out_a_ready = tile_reset_domain_auto_tl_out_xing_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_b_valid = tile_reset_domain_auto_tl_out_xing_out_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_b_bits_opcode = tile_reset_domain_auto_tl_out_xing_out_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_b_bits_param = tile_reset_domain_auto_tl_out_xing_out_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_b_bits_size = tile_reset_domain_auto_tl_out_xing_out_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_b_bits_source = tile_reset_domain_auto_tl_out_xing_out_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_b_bits_address = tile_reset_domain_auto_tl_out_xing_out_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_b_bits_mask = tile_reset_domain_auto_tl_out_xing_out_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_b_bits_data = tile_reset_domain_auto_tl_out_xing_out_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_b_bits_corrupt = tile_reset_domain_auto_tl_out_xing_out_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_c_ready = tile_reset_domain_auto_tl_out_xing_out_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_d_valid = tile_reset_domain_auto_tl_out_xing_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_d_bits_opcode = tile_reset_domain_auto_tl_out_xing_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_d_bits_param = tile_reset_domain_auto_tl_out_xing_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_d_bits_size = tile_reset_domain_auto_tl_out_xing_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_d_bits_source = tile_reset_domain_auto_tl_out_xing_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_d_bits_sink = tile_reset_domain_auto_tl_out_xing_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_d_bits_denied = tile_reset_domain_auto_tl_out_xing_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_d_bits_data = tile_reset_domain_auto_tl_out_xing_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_d_bits_corrupt = tile_reset_domain_auto_tl_out_xing_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_auto_tl_other_masters_out_e_ready = tile_reset_domain_auto_tl_out_xing_out_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign tile_psd_test_clock_enable = tile_reset_domain_psd_test_clock_enable;
  assign clockNode_auto_out_clock = clockNode_auto_in_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign clockNode_auto_out_reset = clockNode_auto_in_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_ready = buffer_auto_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_b_valid = buffer_auto_out_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_b_bits_opcode = buffer_auto_out_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_b_bits_param = buffer_auto_out_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_b_bits_size = buffer_auto_out_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_b_bits_source = buffer_auto_out_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_b_bits_address = buffer_auto_out_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_b_bits_mask = buffer_auto_out_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_b_bits_data = buffer_auto_out_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_b_bits_corrupt = buffer_auto_out_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_c_ready = buffer_auto_out_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_valid = buffer_auto_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_opcode = buffer_auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_param = buffer_auto_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_size = buffer_auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_source = buffer_auto_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_sink = buffer_auto_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_denied = buffer_auto_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_data = buffer_auto_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_corrupt = buffer_auto_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_e_ready = buffer_auto_out_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_a_valid = buffer_auto_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_opcode = buffer_auto_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_param = buffer_auto_in_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_size = buffer_auto_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_source = buffer_auto_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_address = buffer_auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_bufferable = buffer_auto_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_modifiable = buffer_auto_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_readalloc = buffer_auto_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_writealloc = buffer_auto_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_privileged = buffer_auto_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_secure = buffer_auto_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_fetch = buffer_auto_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_mask = buffer_auto_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_data = buffer_auto_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_corrupt = buffer_auto_in_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_b_ready = buffer_auto_in_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_c_valid = buffer_auto_in_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_c_bits_opcode = buffer_auto_in_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_c_bits_param = buffer_auto_in_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_c_bits_size = buffer_auto_in_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_c_bits_source = buffer_auto_in_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_c_bits_address = buffer_auto_in_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_c_bits_data = buffer_auto_in_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_c_bits_corrupt = buffer_auto_in_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_d_ready = buffer_auto_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_e_valid = buffer_auto_in_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_e_bits_sink = buffer_auto_in_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_in_a_ready = block_during_reset__T ? 1'h0 : block_during_reset_auto_out_a_ready; // @[Blockable.scala 35:30 Blockable.scala 37:20 Blockable.scala 33:18]
  assign block_during_reset_auto_in_b_valid = block_during_reset__T_2 ? 1'h0 : block_during_reset_auto_out_b_valid; // @[Blockable.scala 35:30 Blockable.scala 36:20 Blockable.scala 32:18]
  assign block_during_reset_auto_in_b_bits_opcode = block_during_reset_auto_out_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_b_bits_param = block_during_reset_auto_out_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_b_bits_size = block_during_reset_auto_out_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_b_bits_source = block_during_reset_auto_out_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_b_bits_address = block_during_reset_auto_out_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_b_bits_mask = block_during_reset_auto_out_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_b_bits_data = block_during_reset_auto_out_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_b_bits_corrupt = block_during_reset_auto_out_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_c_ready = block_during_reset__T_3 ? 1'h0 : block_during_reset_auto_out_c_ready; // @[Blockable.scala 35:30 Blockable.scala 37:20 Blockable.scala 33:18]
  assign block_during_reset_auto_in_d_valid = block_during_reset__T_1 ? 1'h0 : block_during_reset_auto_out_d_valid; // @[Blockable.scala 35:30 Blockable.scala 36:20 Blockable.scala 32:18]
  assign block_during_reset_auto_in_d_bits_opcode = block_during_reset_auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_d_bits_param = block_during_reset_auto_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_d_bits_size = block_during_reset_auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_d_bits_source = block_during_reset_auto_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_d_bits_sink = block_during_reset_auto_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_d_bits_denied = block_during_reset_auto_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_d_bits_data = block_during_reset_auto_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_d_bits_corrupt = block_during_reset_auto_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign block_during_reset_auto_in_e_ready = block_during_reset__T_4 ? 1'h0 : block_during_reset_auto_out_e_ready; // @[Blockable.scala 35:30 Blockable.scala 37:20 Blockable.scala 33:18]
  assign block_during_reset_auto_out_a_valid = block_during_reset__T ? 1'h0 : block_during_reset_auto_in_a_valid; // @[Blockable.scala 35:30 Blockable.scala 36:20 Blockable.scala 32:18]
  assign block_during_reset_auto_out_a_bits_opcode = block_during_reset_auto_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_a_bits_param = block_during_reset_auto_in_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_a_bits_size = block_during_reset_auto_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_a_bits_source = block_during_reset_auto_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_a_bits_address = block_during_reset_auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_a_bits_user_amba_prot_bufferable =
    block_during_reset_auto_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_a_bits_user_amba_prot_modifiable =
    block_during_reset_auto_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_a_bits_user_amba_prot_readalloc =
    block_during_reset_auto_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_a_bits_user_amba_prot_writealloc =
    block_during_reset_auto_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_a_bits_user_amba_prot_privileged =
    block_during_reset_auto_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_a_bits_user_amba_prot_secure =
    block_during_reset_auto_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_a_bits_user_amba_prot_fetch =
    block_during_reset_auto_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_a_bits_mask = block_during_reset_auto_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_a_bits_data = block_during_reset_auto_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_a_bits_corrupt = block_during_reset_auto_in_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_b_ready = block_during_reset__T_2 ? 1'h0 : block_during_reset_auto_in_b_ready; // @[Blockable.scala 35:30 Blockable.scala 37:20 Blockable.scala 33:18]
  assign block_during_reset_auto_out_c_valid = block_during_reset__T_3 ? 1'h0 : block_during_reset_auto_in_c_valid; // @[Blockable.scala 35:30 Blockable.scala 36:20 Blockable.scala 32:18]
  assign block_during_reset_auto_out_c_bits_opcode = block_during_reset_auto_in_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_c_bits_param = block_during_reset_auto_in_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_c_bits_size = block_during_reset_auto_in_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_c_bits_source = block_during_reset_auto_in_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_c_bits_address = block_during_reset_auto_in_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_c_bits_data = block_during_reset_auto_in_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_c_bits_corrupt = block_during_reset_auto_in_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_out_d_ready = block_during_reset__T_1 ? 1'h0 : block_during_reset_auto_in_d_ready; // @[Blockable.scala 35:30 Blockable.scala 37:20 Blockable.scala 33:18]
  assign block_during_reset_auto_out_e_valid = block_during_reset__T_4 ? 1'h0 : block_during_reset_auto_in_e_valid; // @[Blockable.scala 35:30 Blockable.scala 36:20 Blockable.scala 32:18]
  assign block_during_reset_auto_out_e_bits_sink = block_during_reset_auto_in_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsource_rf_reset = rf_reset;
  assign intsink_rf_reset = rf_reset;
  assign block_during_reset_4_auto_int_out_0 = block_during_reset_4_auto_int_in_0 &
    block_during_reset_4__bundleOut_0_0_T_1; // @[Blockable.scala 17:62]
  assign block_during_reset_5_auto_int_out_0 = block_during_reset_5_auto_int_in_0 &
    block_during_reset_5__bundleOut_0_0_T_1; // @[Blockable.scala 17:62]
  assign block_during_reset_6_auto_int_out_0 = block_during_reset_6_auto_int_in_0 &
    block_during_reset_6__bundleOut_0_0_T_1; // @[Blockable.scala 17:62]
  assign trace_auto_out_0_valid = trace__outputs_broadcast_T ? 1'h0 : trace_auto_in_0_valid; // @[Blockable.scala 24:30 Blockable.scala 24:46 Blockable.scala 23:15]
  assign trace_auto_out_0_iaddr = trace_auto_in_0_iaddr; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign trace_auto_out_0_insn = trace_auto_in_0_insn; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign trace_auto_out_0_priv = trace_auto_in_0_priv; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign trace_auto_out_0_exception = trace_auto_in_0_exception; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign trace_auto_out_0_interrupt = trace_auto_in_0_interrupt; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign rf_reset = auto_tap_clock_in_reset; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_tile_reset_domain_tile_bpwatch_out_0_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_0_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_0_action = tile_reset_domain_auto_tile_bpwatch_out_0_action; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_1_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_1_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_1_action = tile_reset_domain_auto_tile_bpwatch_out_1_action; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_2_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_2_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_2_action = tile_reset_domain_auto_tile_bpwatch_out_2_action; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_3_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_3_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_3_action = tile_reset_domain_auto_tile_bpwatch_out_3_action; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_4_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_4_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_4_action = tile_reset_domain_auto_tile_bpwatch_out_4_action; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_5_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_5_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_5_action = tile_reset_domain_auto_tile_bpwatch_out_5_action; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_6_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_6_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_6_action = tile_reset_domain_auto_tile_bpwatch_out_6_action; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_7_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_7_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_7_action = tile_reset_domain_auto_tile_bpwatch_out_7_action; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_8_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_8_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_8_action = tile_reset_domain_auto_tile_bpwatch_out_8_action; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_9_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_9_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_9_action = tile_reset_domain_auto_tile_bpwatch_out_9_action; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_10_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_10_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_10_action = tile_reset_domain_auto_tile_bpwatch_out_10_action; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_11_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_11_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_11_action = tile_reset_domain_auto_tile_bpwatch_out_11_action; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_12_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_12_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_12_action = tile_reset_domain_auto_tile_bpwatch_out_12_action; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_13_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_13_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_13_action = tile_reset_domain_auto_tile_bpwatch_out_13_action; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_14_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_14_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_14_action = tile_reset_domain_auto_tile_bpwatch_out_14_action; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_15_valid_0 = tile_reset_domain_auto_tile_bpwatch_out_15_valid_0; // @[LazyModule.scala 390:12]
  assign auto_tile_reset_domain_tile_bpwatch_out_15_action = tile_reset_domain_auto_tile_bpwatch_out_15_action; // @[LazyModule.scala 390:12]
  assign auto_int_out_clock_xing_out_3_sync_0 = intsource_4_auto_out_sync_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_int_out_clock_xing_out_2_sync_0 = intsource_3_auto_out_sync_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_int_out_clock_xing_out_1_sync_0 = intsource_2_auto_out_sync_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_int_out_clock_xing_out_0_sync_0 = intsource_1_auto_out_sync_0; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits0_opcode = rsource_auto_out_a_bits0_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits0_param = rsource_auto_out_a_bits0_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits0_size = rsource_auto_out_a_bits0_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits0_source = rsource_auto_out_a_bits0_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits0_address = rsource_auto_out_a_bits0_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits0_user_amba_prot_bufferable =
    rsource_auto_out_a_bits0_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits0_user_amba_prot_modifiable =
    rsource_auto_out_a_bits0_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits0_user_amba_prot_readalloc =
    rsource_auto_out_a_bits0_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits0_user_amba_prot_writealloc =
    rsource_auto_out_a_bits0_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits0_user_amba_prot_privileged =
    rsource_auto_out_a_bits0_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits0_user_amba_prot_secure = rsource_auto_out_a_bits0_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits0_user_amba_prot_fetch = rsource_auto_out_a_bits0_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits0_mask = rsource_auto_out_a_bits0_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits0_data = rsource_auto_out_a_bits0_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits0_corrupt = rsource_auto_out_a_bits0_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits1_opcode = rsource_auto_out_a_bits1_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits1_param = rsource_auto_out_a_bits1_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits1_size = rsource_auto_out_a_bits1_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits1_source = rsource_auto_out_a_bits1_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits1_address = rsource_auto_out_a_bits1_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits1_user_amba_prot_bufferable =
    rsource_auto_out_a_bits1_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits1_user_amba_prot_modifiable =
    rsource_auto_out_a_bits1_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits1_user_amba_prot_readalloc =
    rsource_auto_out_a_bits1_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits1_user_amba_prot_writealloc =
    rsource_auto_out_a_bits1_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits1_user_amba_prot_privileged =
    rsource_auto_out_a_bits1_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits1_user_amba_prot_secure = rsource_auto_out_a_bits1_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits1_user_amba_prot_fetch = rsource_auto_out_a_bits1_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits1_mask = rsource_auto_out_a_bits1_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits1_data = rsource_auto_out_a_bits1_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_bits1_corrupt = rsource_auto_out_a_bits1_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_valid = rsource_auto_out_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_a_source = rsource_auto_out_a_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_b_ready = rsource_auto_out_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_b_sink = rsource_auto_out_b_sink; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_bits0_opcode = rsource_auto_out_c_bits0_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_bits0_param = rsource_auto_out_c_bits0_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_bits0_size = rsource_auto_out_c_bits0_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_bits0_source = rsource_auto_out_c_bits0_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_bits0_address = rsource_auto_out_c_bits0_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_bits0_data = rsource_auto_out_c_bits0_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_bits0_corrupt = rsource_auto_out_c_bits0_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_bits1_opcode = rsource_auto_out_c_bits1_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_bits1_param = rsource_auto_out_c_bits1_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_bits1_size = rsource_auto_out_c_bits1_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_bits1_source = rsource_auto_out_c_bits1_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_bits1_address = rsource_auto_out_c_bits1_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_bits1_data = rsource_auto_out_c_bits1_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_bits1_corrupt = rsource_auto_out_c_bits1_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_valid = rsource_auto_out_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_c_source = rsource_auto_out_c_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_d_ready = rsource_auto_out_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_d_sink = rsource_auto_out_d_sink; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_e_bits0_sink = rsource_auto_out_e_bits0_sink; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_e_bits1_sink = rsource_auto_out_e_bits1_sink; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_e_valid = rsource_auto_out_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_master_clock_xing_out_e_source = rsource_auto_out_e_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_trace_out_0_valid = trace_auto_out_0_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_trace_out_0_iaddr = trace_auto_out_0_iaddr; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_trace_out_0_insn = trace_auto_out_0_insn; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_trace_out_0_priv = trace_auto_out_0_priv; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_trace_out_0_exception = trace_auto_out_0_exception; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_trace_out_0_interrupt = trace_auto_out_0_interrupt; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_0 = intsink_3_auto_out_0; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_1 = intsink_3_auto_out_1; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_2 = intsink_3_auto_out_2; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_3 = intsink_3_auto_out_3; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_4 = intsink_3_auto_out_4; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_5 = intsink_3_auto_out_5; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_6 = intsink_3_auto_out_6; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_7 = intsink_3_auto_out_7; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_8 = intsink_3_auto_out_8; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_9 = intsink_3_auto_out_9; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_10 = intsink_3_auto_out_10; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_11 = intsink_3_auto_out_11; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_12 = intsink_3_auto_out_12; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_13 = intsink_3_auto_out_13; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_14 = intsink_3_auto_out_14; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_3_15 = intsink_3_auto_out_15; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_2_0 = intsink_2_auto_out_0; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_2_1 = intsink_2_auto_out_1; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_1_0 = intsink_1_auto_out_0; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_1_1 = intsink_1_auto_out_1; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_int_sink_in_0_0 = intsink_auto_out_0; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_tile_trace_aux_in_stall = auto_tile_reset_domain_tile_trace_aux_in_stall; // @[LazyModule.scala 388:16]
  assign tile_reset_domain_auto_tile_nmi_in_rnmi = auto_tile_reset_domain_tile_nmi_in_rnmi; // @[LazyModule.scala 388:16]
  assign tile_reset_domain_auto_tile_nmi_in_rnmi_interrupt_vector =
    auto_tile_reset_domain_tile_nmi_in_rnmi_interrupt_vector; // @[LazyModule.scala 388:16]
  assign tile_reset_domain_auto_tile_nmi_in_rnmi_exception_vector =
    auto_tile_reset_domain_tile_nmi_in_rnmi_exception_vector; // @[LazyModule.scala 388:16]
  assign tile_reset_domain_auto_tile_reset_vector_in = auto_tile_reset_domain_tile_reset_vector_in; // @[LazyModule.scala 388:16]
  assign tile_reset_domain_auto_tile_hartid_in = auto_tile_reset_domain_tile_hartid_in; // @[LazyModule.scala 388:16]
  assign tile_reset_domain_auto_tl_out_xing_out_a_ready = block_during_reset_auto_in_a_ready; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_b_valid = block_during_reset_auto_in_b_valid; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_b_bits_opcode = block_during_reset_auto_in_b_bits_opcode; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_b_bits_param = block_during_reset_auto_in_b_bits_param; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_b_bits_size = block_during_reset_auto_in_b_bits_size; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_b_bits_source = block_during_reset_auto_in_b_bits_source; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_b_bits_address = block_during_reset_auto_in_b_bits_address; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_b_bits_mask = block_during_reset_auto_in_b_bits_mask; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_b_bits_data = block_during_reset_auto_in_b_bits_data; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_b_bits_corrupt = block_during_reset_auto_in_b_bits_corrupt; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_c_ready = block_during_reset_auto_in_c_ready; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_d_valid = block_during_reset_auto_in_d_valid; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_d_bits_opcode = block_during_reset_auto_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_d_bits_param = block_during_reset_auto_in_d_bits_param; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_d_bits_size = block_during_reset_auto_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_d_bits_source = block_during_reset_auto_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_d_bits_sink = block_during_reset_auto_in_d_bits_sink; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_d_bits_denied = block_during_reset_auto_in_d_bits_denied; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_d_bits_data = block_during_reset_auto_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_d_bits_corrupt = block_during_reset_auto_in_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_tl_out_xing_out_e_ready = block_during_reset_auto_in_e_ready; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_auto_clock_in_clock = rs_auto_reset_stretcher_out_clock; // @[LazyModule.scala 375:16]
  assign tile_reset_domain_auto_clock_in_reset = rs_auto_reset_stretcher_out_reset; // @[LazyModule.scala 375:16]
  assign clockNode_auto_in_clock = auto_tap_clock_in_clock; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign clockNode_auto_in_reset = auto_tap_clock_in_reset; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_valid = block_during_reset_auto_out_a_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_opcode = block_during_reset_auto_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_param = block_during_reset_auto_out_a_bits_param; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_size = block_during_reset_auto_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_source = block_during_reset_auto_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_address = block_during_reset_auto_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_bufferable = block_during_reset_auto_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_modifiable = block_during_reset_auto_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_readalloc = block_during_reset_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_writealloc = block_during_reset_auto_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_privileged = block_during_reset_auto_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_secure = block_during_reset_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_fetch = block_during_reset_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_mask = block_during_reset_auto_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_data = block_during_reset_auto_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_corrupt = block_during_reset_auto_out_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_b_ready = block_during_reset_auto_out_b_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_c_valid = block_during_reset_auto_out_c_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_c_bits_opcode = block_during_reset_auto_out_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_c_bits_param = block_during_reset_auto_out_c_bits_param; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_c_bits_size = block_during_reset_auto_out_c_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_c_bits_source = block_during_reset_auto_out_c_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_c_bits_address = block_during_reset_auto_out_c_bits_address; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_c_bits_data = block_during_reset_auto_out_c_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_c_bits_corrupt = block_during_reset_auto_out_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_d_ready = block_during_reset_auto_out_d_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_e_valid = block_during_reset_auto_out_e_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_e_bits_sink = block_during_reset_auto_out_e_bits_sink; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_a_ready = rsource_auto_in_a_ready; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_b_valid = rsource_auto_in_b_valid; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_b_bits_opcode = rsource_auto_in_b_bits_opcode; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_b_bits_param = rsource_auto_in_b_bits_param; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_b_bits_size = rsource_auto_in_b_bits_size; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_b_bits_source = rsource_auto_in_b_bits_source; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_b_bits_address = rsource_auto_in_b_bits_address; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_b_bits_mask = rsource_auto_in_b_bits_mask; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_b_bits_data = rsource_auto_in_b_bits_data; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_b_bits_corrupt = rsource_auto_in_b_bits_corrupt; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_c_ready = rsource_auto_in_c_ready; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_d_valid = rsource_auto_in_d_valid; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_d_bits_opcode = rsource_auto_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_d_bits_param = rsource_auto_in_d_bits_param; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_d_bits_size = rsource_auto_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_d_bits_source = rsource_auto_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_d_bits_sink = rsource_auto_in_d_bits_sink; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_d_bits_denied = rsource_auto_in_d_bits_denied; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_d_bits_data = rsource_auto_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_d_bits_corrupt = rsource_auto_in_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_e_ready = rsource_auto_in_e_ready; // @[LazyModule.scala 377:16]
  assign block_during_reset_clock = auto_tap_clock_in_clock; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_reset = auto_tap_clock_in_reset; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_auto_in_a_valid = tile_reset_domain_auto_tl_out_xing_out_a_valid; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_a_bits_opcode = tile_reset_domain_auto_tl_out_xing_out_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_a_bits_param = tile_reset_domain_auto_tl_out_xing_out_a_bits_param; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_a_bits_size = tile_reset_domain_auto_tl_out_xing_out_a_bits_size; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_a_bits_source = tile_reset_domain_auto_tl_out_xing_out_a_bits_source; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_a_bits_address = tile_reset_domain_auto_tl_out_xing_out_a_bits_address; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_a_bits_user_amba_prot_bufferable =
    tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_a_bits_user_amba_prot_modifiable =
    tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_a_bits_user_amba_prot_readalloc =
    tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_a_bits_user_amba_prot_writealloc =
    tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_a_bits_user_amba_prot_privileged =
    tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_a_bits_user_amba_prot_secure =
    tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_a_bits_user_amba_prot_fetch =
    tile_reset_domain_auto_tl_out_xing_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_a_bits_mask = tile_reset_domain_auto_tl_out_xing_out_a_bits_mask; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_a_bits_data = tile_reset_domain_auto_tl_out_xing_out_a_bits_data; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_a_bits_corrupt = tile_reset_domain_auto_tl_out_xing_out_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_b_ready = tile_reset_domain_auto_tl_out_xing_out_b_ready; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_c_valid = tile_reset_domain_auto_tl_out_xing_out_c_valid; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_c_bits_opcode = tile_reset_domain_auto_tl_out_xing_out_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_c_bits_param = tile_reset_domain_auto_tl_out_xing_out_c_bits_param; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_c_bits_size = tile_reset_domain_auto_tl_out_xing_out_c_bits_size; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_c_bits_source = tile_reset_domain_auto_tl_out_xing_out_c_bits_source; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_c_bits_address = tile_reset_domain_auto_tl_out_xing_out_c_bits_address; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_c_bits_data = tile_reset_domain_auto_tl_out_xing_out_c_bits_data; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_c_bits_corrupt = tile_reset_domain_auto_tl_out_xing_out_c_bits_corrupt; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_d_ready = tile_reset_domain_auto_tl_out_xing_out_d_ready; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_e_valid = tile_reset_domain_auto_tl_out_xing_out_e_valid; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_in_e_bits_sink = tile_reset_domain_auto_tl_out_xing_out_e_bits_sink; // @[LazyModule.scala 377:16]
  assign block_during_reset_auto_out_a_ready = buffer_auto_in_a_ready; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_b_valid = buffer_auto_in_b_valid; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_b_bits_opcode = buffer_auto_in_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_b_bits_param = buffer_auto_in_b_bits_param; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_b_bits_size = buffer_auto_in_b_bits_size; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_b_bits_source = buffer_auto_in_b_bits_source; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_b_bits_address = buffer_auto_in_b_bits_address; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_b_bits_mask = buffer_auto_in_b_bits_mask; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_b_bits_data = buffer_auto_in_b_bits_data; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_b_bits_corrupt = buffer_auto_in_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_c_ready = buffer_auto_in_c_ready; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_d_valid = buffer_auto_in_d_valid; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_d_bits_opcode = buffer_auto_in_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_d_bits_param = buffer_auto_in_d_bits_param; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_d_bits_size = buffer_auto_in_d_bits_size; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_d_bits_source = buffer_auto_in_d_bits_source; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_d_bits_sink = buffer_auto_in_d_bits_sink; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_d_bits_denied = buffer_auto_in_d_bits_denied; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_d_bits_data = buffer_auto_in_d_bits_data; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_d_bits_corrupt = buffer_auto_in_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign block_during_reset_auto_out_e_ready = buffer_auto_in_e_ready; // @[LazyModule.scala 375:16]
  assign rsource_clock = auto_tap_clock_in_clock; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsource_reset = auto_tap_clock_in_reset; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rsource_auto_in_a_valid = buffer_auto_out_a_valid; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_a_bits_opcode = buffer_auto_out_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_a_bits_param = buffer_auto_out_a_bits_param; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_a_bits_size = buffer_auto_out_a_bits_size; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_a_bits_source = buffer_auto_out_a_bits_source; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_a_bits_address = buffer_auto_out_a_bits_address; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_a_bits_user_amba_prot_bufferable = buffer_auto_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_a_bits_user_amba_prot_modifiable = buffer_auto_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_a_bits_user_amba_prot_readalloc = buffer_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_a_bits_user_amba_prot_writealloc = buffer_auto_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_a_bits_user_amba_prot_privileged = buffer_auto_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_a_bits_user_amba_prot_secure = buffer_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_a_bits_user_amba_prot_fetch = buffer_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_a_bits_mask = buffer_auto_out_a_bits_mask; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_a_bits_data = buffer_auto_out_a_bits_data; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_a_bits_corrupt = buffer_auto_out_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_b_ready = buffer_auto_out_b_ready; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_c_valid = buffer_auto_out_c_valid; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_c_bits_opcode = buffer_auto_out_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_c_bits_param = buffer_auto_out_c_bits_param; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_c_bits_size = buffer_auto_out_c_bits_size; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_c_bits_source = buffer_auto_out_c_bits_source; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_c_bits_address = buffer_auto_out_c_bits_address; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_c_bits_data = buffer_auto_out_c_bits_data; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_c_bits_corrupt = buffer_auto_out_c_bits_corrupt; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_d_ready = buffer_auto_out_d_ready; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_e_valid = buffer_auto_out_e_valid; // @[LazyModule.scala 377:16]
  assign rsource_auto_in_e_bits_sink = buffer_auto_out_e_bits_sink; // @[LazyModule.scala 377:16]
  assign rsource_auto_out_a_ready = auto_tl_master_clock_xing_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_a_sink = auto_tl_master_clock_xing_out_a_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits0_opcode = auto_tl_master_clock_xing_out_b_bits0_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits0_param = auto_tl_master_clock_xing_out_b_bits0_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits0_size = auto_tl_master_clock_xing_out_b_bits0_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits0_source = auto_tl_master_clock_xing_out_b_bits0_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits0_address = auto_tl_master_clock_xing_out_b_bits0_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits0_mask = auto_tl_master_clock_xing_out_b_bits0_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits0_data = auto_tl_master_clock_xing_out_b_bits0_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits0_corrupt = auto_tl_master_clock_xing_out_b_bits0_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits1_opcode = auto_tl_master_clock_xing_out_b_bits1_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits1_param = auto_tl_master_clock_xing_out_b_bits1_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits1_size = auto_tl_master_clock_xing_out_b_bits1_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits1_source = auto_tl_master_clock_xing_out_b_bits1_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits1_address = auto_tl_master_clock_xing_out_b_bits1_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits1_mask = auto_tl_master_clock_xing_out_b_bits1_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits1_data = auto_tl_master_clock_xing_out_b_bits1_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_bits1_corrupt = auto_tl_master_clock_xing_out_b_bits1_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_valid = auto_tl_master_clock_xing_out_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_b_source = auto_tl_master_clock_xing_out_b_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_c_ready = auto_tl_master_clock_xing_out_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_c_sink = auto_tl_master_clock_xing_out_c_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits0_opcode = auto_tl_master_clock_xing_out_d_bits0_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits0_param = auto_tl_master_clock_xing_out_d_bits0_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits0_size = auto_tl_master_clock_xing_out_d_bits0_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits0_source = auto_tl_master_clock_xing_out_d_bits0_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits0_sink = auto_tl_master_clock_xing_out_d_bits0_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits0_denied = auto_tl_master_clock_xing_out_d_bits0_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits0_data = auto_tl_master_clock_xing_out_d_bits0_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits0_corrupt = auto_tl_master_clock_xing_out_d_bits0_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits1_opcode = auto_tl_master_clock_xing_out_d_bits1_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits1_param = auto_tl_master_clock_xing_out_d_bits1_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits1_size = auto_tl_master_clock_xing_out_d_bits1_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits1_source = auto_tl_master_clock_xing_out_d_bits1_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits1_sink = auto_tl_master_clock_xing_out_d_bits1_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits1_denied = auto_tl_master_clock_xing_out_d_bits1_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits1_data = auto_tl_master_clock_xing_out_d_bits1_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_bits1_corrupt = auto_tl_master_clock_xing_out_d_bits1_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_valid = auto_tl_master_clock_xing_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_d_source = auto_tl_master_clock_xing_out_d_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_e_ready = auto_tl_master_clock_xing_out_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign rsource_auto_out_e_sink = auto_tl_master_clock_xing_out_e_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign intsink_clock = auto_tap_clock_in_clock; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_auto_in_sync_0 = auto_intsink_in_sync_0; // @[LazyModule.scala 388:16]
  assign intsink_1_clock = auto_tap_clock_in_clock; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_1_reset = auto_tap_clock_in_reset; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_1_auto_in_sync_0 = auto_int_in_clock_xing_in_0_sync_0; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_1_auto_in_sync_1 = auto_int_in_clock_xing_in_0_sync_1; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_2_clock = auto_tap_clock_in_clock; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_2_reset = auto_tap_clock_in_reset; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_2_auto_in_sync_0 = auto_int_in_clock_xing_in_1_sync_0; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_2_auto_in_sync_1 = auto_int_in_clock_xing_in_1_sync_1; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_0 = auto_int_in_clock_xing_in_2_sync_0; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_1 = auto_int_in_clock_xing_in_2_sync_1; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_2 = auto_int_in_clock_xing_in_2_sync_2; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_3 = auto_int_in_clock_xing_in_2_sync_3; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_4 = auto_int_in_clock_xing_in_2_sync_4; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_5 = auto_int_in_clock_xing_in_2_sync_5; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_6 = auto_int_in_clock_xing_in_2_sync_6; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_7 = auto_int_in_clock_xing_in_2_sync_7; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_8 = auto_int_in_clock_xing_in_2_sync_8; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_9 = auto_int_in_clock_xing_in_2_sync_9; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_10 = auto_int_in_clock_xing_in_2_sync_10; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_11 = auto_int_in_clock_xing_in_2_sync_11; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_12 = auto_int_in_clock_xing_in_2_sync_12; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_13 = auto_int_in_clock_xing_in_2_sync_13; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_14 = auto_int_in_clock_xing_in_2_sync_14; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign intsink_3_auto_in_sync_15 = auto_int_in_clock_xing_in_2_sync_15; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign rs_auto_reset_stretcher_in_clock = clockNode_auto_out_clock; // @[LazyModule.scala 377:16]
  assign rs_auto_reset_stretcher_in_reset = clockNode_auto_out_reset; // @[LazyModule.scala 377:16]
  assign intsource_1_auto_in_0 = 1'h0; // @[LazyModule.scala 377:16]
  assign block_during_reset_4_clock = auto_tap_clock_in_clock; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_4_reset = auto_tap_clock_in_reset; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_4_auto_int_in_0 = tile_reset_domain_auto_int_out_xing_out_1_0; // @[LazyModule.scala 377:16]
  assign intsource_2_auto_in_0 = block_during_reset_4_auto_int_out_0; // @[LazyModule.scala 377:16]
  assign block_during_reset_5_clock = auto_tap_clock_in_clock; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_5_reset = auto_tap_clock_in_reset; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_5_auto_int_in_0 = tile_reset_domain_auto_int_out_xing_out_2_0; // @[LazyModule.scala 377:16]
  assign intsource_3_auto_in_0 = block_during_reset_5_auto_int_out_0; // @[LazyModule.scala 377:16]
  assign block_during_reset_6_clock = auto_tap_clock_in_clock; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_6_reset = auto_tap_clock_in_reset; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign block_during_reset_6_auto_int_in_0 = tile_reset_domain_auto_int_out_xing_out_3_0; // @[LazyModule.scala 377:16]
  assign intsource_4_auto_in_0 = block_during_reset_6_auto_int_out_0; // @[LazyModule.scala 377:16]
  assign trace_clock = auto_tap_clock_in_clock; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign trace_reset = auto_tap_clock_in_reset; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign trace_auto_in_0_valid = tile_reset_domain_auto_tile_broadcast_out_0_valid; // @[LazyModule.scala 377:16]
  assign trace_auto_in_0_iaddr = tile_reset_domain_auto_tile_broadcast_out_0_iaddr; // @[LazyModule.scala 377:16]
  assign trace_auto_in_0_insn = tile_reset_domain_auto_tile_broadcast_out_0_insn; // @[LazyModule.scala 377:16]
  assign trace_auto_in_0_priv = tile_reset_domain_auto_tile_broadcast_out_0_priv; // @[LazyModule.scala 377:16]
  assign trace_auto_in_0_exception = tile_reset_domain_auto_tile_broadcast_out_0_exception; // @[LazyModule.scala 377:16]
  assign trace_auto_in_0_interrupt = tile_reset_domain_auto_tile_broadcast_out_0_interrupt; // @[LazyModule.scala 377:16]
  assign tile_reset_domain_psd_test_clock_enable = psd_test_clock_enable;
  always @(posedge block_during_reset_clock or posedge block_during_reset_reset) begin
    if (block_during_reset_reset) begin
      block_during_reset_value <= 4'h0;
    end else begin
      block_during_reset_value <= block_during_reset_value + 4'h1;
    end
  end
  always @(posedge block_during_reset_clock or posedge block_during_reset_reset) begin
    if (block_during_reset_reset) begin
      block_during_reset_r <= 1'h0;
    end else begin
      block_during_reset_r <= block_during_reset_wrap_wrap | block_during_reset_r;
    end
  end
  always @(posedge block_during_reset_clock or posedge block_during_reset_reset) begin
    if (block_during_reset_reset) begin
      block_during_reset_value_1 <= 4'h0;
    end else begin
      block_during_reset_value_1 <= block_during_reset_value_1 + 4'h1;
    end
  end
  always @(posedge block_during_reset_clock or posedge block_during_reset_reset) begin
    if (block_during_reset_reset) begin
      block_during_reset_r_1 <= 1'h0;
    end else begin
      block_during_reset_r_1 <= block_during_reset_wrap_wrap_1 | block_during_reset_r_1;
    end
  end
  always @(posedge block_during_reset_clock or posedge block_during_reset_reset) begin
    if (block_during_reset_reset) begin
      block_during_reset_value_2 <= 4'h0;
    end else begin
      block_during_reset_value_2 <= block_during_reset_value_2 + 4'h1;
    end
  end
  always @(posedge block_during_reset_clock or posedge block_during_reset_reset) begin
    if (block_during_reset_reset) begin
      block_during_reset_r_2 <= 1'h0;
    end else begin
      block_during_reset_r_2 <= block_during_reset_wrap_wrap_2 | block_during_reset_r_2;
    end
  end
  always @(posedge block_during_reset_clock or posedge block_during_reset_reset) begin
    if (block_during_reset_reset) begin
      block_during_reset_value_3 <= 4'h0;
    end else begin
      block_during_reset_value_3 <= block_during_reset_value_3 + 4'h1;
    end
  end
  always @(posedge block_during_reset_clock or posedge block_during_reset_reset) begin
    if (block_during_reset_reset) begin
      block_during_reset_r_3 <= 1'h0;
    end else begin
      block_during_reset_r_3 <= block_during_reset_wrap_wrap_3 | block_during_reset_r_3;
    end
  end
  always @(posedge block_during_reset_clock or posedge block_during_reset_reset) begin
    if (block_during_reset_reset) begin
      block_during_reset_value_4 <= 4'h0;
    end else begin
      block_during_reset_value_4 <= block_during_reset_value_4 + 4'h1;
    end
  end
  always @(posedge block_during_reset_clock or posedge block_during_reset_reset) begin
    if (block_during_reset_reset) begin
      block_during_reset_r_4 <= 1'h0;
    end else begin
      block_during_reset_r_4 <= block_during_reset_wrap_wrap_4 | block_during_reset_r_4;
    end
  end
  always @(posedge block_during_reset_4_clock or posedge block_during_reset_4_reset) begin
    if (block_during_reset_4_reset) begin
      block_during_reset_4_bundleOut_0_0_value <= 4'h0;
    end else begin
      block_during_reset_4_bundleOut_0_0_value <= block_during_reset_4_bundleOut_0_0_value + 4'h1;
    end
  end
  always @(posedge block_during_reset_4_clock or posedge block_during_reset_4_reset) begin
    if (block_during_reset_4_reset) begin
      block_during_reset_4_bundleOut_0_0_r <= 1'h0;
    end else begin
      block_during_reset_4_bundleOut_0_0_r <= block_during_reset_4_bundleOut_0_0_wrap_wrap |
        block_during_reset_4_bundleOut_0_0_r;
    end
  end
  always @(posedge block_during_reset_5_clock or posedge block_during_reset_5_reset) begin
    if (block_during_reset_5_reset) begin
      block_during_reset_5_bundleOut_0_0_value <= 4'h0;
    end else begin
      block_during_reset_5_bundleOut_0_0_value <= block_during_reset_5_bundleOut_0_0_value + 4'h1;
    end
  end
  always @(posedge block_during_reset_5_clock or posedge block_during_reset_5_reset) begin
    if (block_during_reset_5_reset) begin
      block_during_reset_5_bundleOut_0_0_r <= 1'h0;
    end else begin
      block_during_reset_5_bundleOut_0_0_r <= block_during_reset_5_bundleOut_0_0_wrap_wrap |
        block_during_reset_5_bundleOut_0_0_r;
    end
  end
  always @(posedge block_during_reset_6_clock or posedge block_during_reset_6_reset) begin
    if (block_during_reset_6_reset) begin
      block_during_reset_6_bundleOut_0_0_value <= 4'h0;
    end else begin
      block_during_reset_6_bundleOut_0_0_value <= block_during_reset_6_bundleOut_0_0_value + 4'h1;
    end
  end
  always @(posedge block_during_reset_6_clock or posedge block_during_reset_6_reset) begin
    if (block_during_reset_6_reset) begin
      block_during_reset_6_bundleOut_0_0_r <= 1'h0;
    end else begin
      block_during_reset_6_bundleOut_0_0_r <= block_during_reset_6_bundleOut_0_0_wrap_wrap |
        block_during_reset_6_bundleOut_0_0_r;
    end
  end
  always @(posedge trace_clock or posedge trace_reset) begin
    if (trace_reset) begin
      trace_outputs_broadcast_value <= 4'h0;
    end else begin
      trace_outputs_broadcast_value <= trace_outputs_broadcast_value + 4'h1;
    end
  end
  always @(posedge trace_clock or posedge trace_reset) begin
    if (trace_reset) begin
      trace_outputs_broadcast_r <= 1'h0;
    end else begin
      trace_outputs_broadcast_r <= trace_outputs_broadcast_wrap_wrap | trace_outputs_broadcast_r;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  block_during_reset_value = _RAND_0[3:0];
  _RAND_1 = {1{`RANDOM}};
  block_during_reset_r = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  block_during_reset_value_1 = _RAND_2[3:0];
  _RAND_3 = {1{`RANDOM}};
  block_during_reset_r_1 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  block_during_reset_value_2 = _RAND_4[3:0];
  _RAND_5 = {1{`RANDOM}};
  block_during_reset_r_2 = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  block_during_reset_value_3 = _RAND_6[3:0];
  _RAND_7 = {1{`RANDOM}};
  block_during_reset_r_3 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  block_during_reset_value_4 = _RAND_8[3:0];
  _RAND_9 = {1{`RANDOM}};
  block_during_reset_r_4 = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  block_during_reset_4_bundleOut_0_0_value = _RAND_10[3:0];
  _RAND_11 = {1{`RANDOM}};
  block_during_reset_4_bundleOut_0_0_r = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  block_during_reset_5_bundleOut_0_0_value = _RAND_12[3:0];
  _RAND_13 = {1{`RANDOM}};
  block_during_reset_5_bundleOut_0_0_r = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  block_during_reset_6_bundleOut_0_0_value = _RAND_14[3:0];
  _RAND_15 = {1{`RANDOM}};
  block_during_reset_6_bundleOut_0_0_r = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  trace_outputs_broadcast_value = _RAND_16[3:0];
  _RAND_17 = {1{`RANDOM}};
  trace_outputs_broadcast_r = _RAND_17[0:0];
`endif // RANDOMIZE_REG_INIT
  if (block_during_reset_reset) begin
    block_during_reset_value = 4'h0;
  end
  if (block_during_reset_reset) begin
    block_during_reset_r = 1'h0;
  end
  if (block_during_reset_reset) begin
    block_during_reset_value_1 = 4'h0;
  end
  if (block_during_reset_reset) begin
    block_during_reset_r_1 = 1'h0;
  end
  if (block_during_reset_reset) begin
    block_during_reset_value_2 = 4'h0;
  end
  if (block_during_reset_reset) begin
    block_during_reset_r_2 = 1'h0;
  end
  if (block_during_reset_reset) begin
    block_during_reset_value_3 = 4'h0;
  end
  if (block_during_reset_reset) begin
    block_during_reset_r_3 = 1'h0;
  end
  if (block_during_reset_reset) begin
    block_during_reset_value_4 = 4'h0;
  end
  if (block_during_reset_reset) begin
    block_during_reset_r_4 = 1'h0;
  end
  if (block_during_reset_4_reset) begin
    block_during_reset_4_bundleOut_0_0_value = 4'h0;
  end
  if (block_during_reset_4_reset) begin
    block_during_reset_4_bundleOut_0_0_r = 1'h0;
  end
  if (block_during_reset_5_reset) begin
    block_during_reset_5_bundleOut_0_0_value = 4'h0;
  end
  if (block_during_reset_5_reset) begin
    block_during_reset_5_bundleOut_0_0_r = 1'h0;
  end
  if (block_during_reset_6_reset) begin
    block_during_reset_6_bundleOut_0_0_value = 4'h0;
  end
  if (block_during_reset_6_reset) begin
    block_during_reset_6_bundleOut_0_0_r = 1'h0;
  end
  if (trace_reset) begin
    trace_outputs_broadcast_value = 4'h0;
  end
  if (trace_reset) begin
    trace_outputs_broadcast_r = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
