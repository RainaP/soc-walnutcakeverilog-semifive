//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_FPU_assert(
  input         reset,
  input         clock_gate_0_out,
  input         load_wb,
  input  [64:0] wdata,
  input         _T_262,
  input  [64:0] wdata_1,
  input         _T_798,
  input         _T_809
);
  wire  _T_263; // @[FPU.scala 333:19]
  wire  _T_266; // @[FPU.scala 333:96]
  wire  _T_267; // @[FPU.scala 333:55]
  wire  _T_268; // @[FPU.scala 333:31]
  wire  _T_271; // @[FPU.scala 752:11]
  wire  _T_272; // @[FPU.scala 752:11]
  wire  _T_810; // @[FPU.scala 333:19]
  wire  _T_813; // @[FPU.scala 333:96]
  wire  _T_814; // @[FPU.scala 333:55]
  wire  _T_815; // @[FPU.scala 333:31]
  wire  _T_818; // @[FPU.scala 883:11]
  wire  _T_819; // @[FPU.scala 883:11]
  assign _T_263 = ~_T_262; // @[FPU.scala 333:19]
  assign _T_266 = wdata[51:32] == 20'hfffff; // @[FPU.scala 333:96]
  assign _T_267 = wdata[60] == _T_266; // @[FPU.scala 333:55]
  assign _T_268 = _T_263 | _T_267; // @[FPU.scala 333:31]
  assign _T_271 = _T_268 | reset; // @[FPU.scala 752:11]
  assign _T_272 = ~_T_271; // @[FPU.scala 752:11]
  assign _T_810 = ~_T_809; // @[FPU.scala 333:19]
  assign _T_813 = wdata_1[51:32] == 20'hfffff; // @[FPU.scala 333:96]
  assign _T_814 = wdata_1[60] == _T_813; // @[FPU.scala 333:55]
  assign _T_815 = _T_810 | _T_814; // @[FPU.scala 333:31]
  assign _T_818 = _T_815 | reset; // @[FPU.scala 883:11]
  assign _T_819 = ~_T_818; // @[FPU.scala 883:11]
  always @(posedge clock_gate_0_out) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (load_wb & _T_272) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at FPU.scala:752 assert(consistent(wdata))\n"); // @[FPU.scala 752:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (load_wb & _T_272) begin
          $fatal; // @[FPU.scala 752:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_798 & _T_819) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at FPU.scala:883 assert(consistent(wdata))\n"); // @[FPU.scala 883:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_798 & _T_819) begin
          $fatal; // @[FPU.scala 883:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
