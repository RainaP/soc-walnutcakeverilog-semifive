//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__Rocket(
  input         rf_reset,
  input         clock,
  input         reset,
  input  [1:0]  io_hartid,
  input         io_interrupts_debug,
  input         io_interrupts_mtip,
  input         io_interrupts_msip,
  input         io_interrupts_meip,
  input         io_interrupts_seip,
  input         io_interrupts_lip_0,
  input         io_interrupts_lip_1,
  input         io_interrupts_lip_2,
  input         io_interrupts_lip_3,
  input         io_interrupts_lip_4,
  input         io_interrupts_lip_5,
  input         io_interrupts_lip_6,
  input         io_interrupts_lip_7,
  input         io_interrupts_lip_8,
  input         io_interrupts_lip_9,
  input         io_interrupts_lip_10,
  input         io_interrupts_lip_11,
  input         io_interrupts_lip_12,
  input         io_interrupts_lip_13,
  input         io_interrupts_lip_14,
  input         io_interrupts_lip_15,
  input         io_interrupts_nmi_rnmi,
  input  [36:0] io_interrupts_nmi_rnmi_interrupt_vector,
  input  [36:0] io_interrupts_nmi_rnmi_exception_vector,
  output        io_imem_might_request,
  output        io_imem_req_valid,
  output [39:0] io_imem_req_bits_pc,
  output        io_imem_req_bits_speculative,
  output        io_imem_sfence_valid,
  output        io_imem_sfence_bits_rs1,
  output        io_imem_sfence_bits_rs2,
  output [38:0] io_imem_sfence_bits_addr,
  output        io_imem_sfence_bits_asid,
  output        io_imem_resp_ready,
  input         io_imem_resp_valid,
  input         io_imem_resp_bits_btb_taken,
  input         io_imem_resp_bits_btb_bridx,
  input  [4:0]  io_imem_resp_bits_btb_entry,
  input  [7:0]  io_imem_resp_bits_btb_bht_history,
  input  [39:0] io_imem_resp_bits_pc,
  input  [31:0] io_imem_resp_bits_data,
  input  [1:0]  io_imem_resp_bits_mask,
  input         io_imem_resp_bits_xcpt_pf_inst,
  input         io_imem_resp_bits_xcpt_ae_inst,
  input         io_imem_resp_bits_replay,
  output        io_imem_btb_update_valid,
  output [4:0]  io_imem_btb_update_bits_prediction_entry,
  output [38:0] io_imem_btb_update_bits_pc,
  output        io_imem_btb_update_bits_isValid,
  output [38:0] io_imem_btb_update_bits_br_pc,
  output [1:0]  io_imem_btb_update_bits_cfiType,
  output        io_imem_bht_update_valid,
  output [7:0]  io_imem_bht_update_bits_prediction_history,
  output [38:0] io_imem_bht_update_bits_pc,
  output        io_imem_bht_update_bits_branch,
  output        io_imem_bht_update_bits_taken,
  output        io_imem_bht_update_bits_mispredict,
  output        io_imem_flush_icache,
  input         io_imem_perf_acquire,
  input         io_imem_perf_tlbMiss,
  input         io_dmem_req_ready,
  output        io_dmem_req_valid,
  output [39:0] io_dmem_req_bits_addr,
  output [39:0] io_dmem_req_bits_idx,
  output [6:0]  io_dmem_req_bits_tag,
  output [4:0]  io_dmem_req_bits_cmd,
  output [1:0]  io_dmem_req_bits_size,
  output        io_dmem_req_bits_signed,
  output [1:0]  io_dmem_req_bits_dprv,
  output        io_dmem_req_bits_phys,
  output        io_dmem_req_bits_no_alloc,
  output        io_dmem_req_bits_no_xcpt,
  output [63:0] io_dmem_req_bits_data,
  output [7:0]  io_dmem_req_bits_mask,
  output        io_dmem_s1_kill,
  output [63:0] io_dmem_s1_data_data,
  output [7:0]  io_dmem_s1_data_mask,
  input         io_dmem_s2_nack,
  input         io_dmem_resp_valid,
  input  [39:0] io_dmem_resp_bits_addr,
  input  [6:0]  io_dmem_resp_bits_tag,
  input  [4:0]  io_dmem_resp_bits_cmd,
  input  [1:0]  io_dmem_resp_bits_size,
  input         io_dmem_resp_bits_signed,
  input  [63:0] io_dmem_resp_bits_data,
  input         io_dmem_resp_bits_replay,
  input         io_dmem_resp_bits_has_data,
  input  [63:0] io_dmem_resp_bits_data_word_bypass,
  input         io_dmem_replay_next,
  input         io_dmem_s2_xcpt_ma_ld,
  input         io_dmem_s2_xcpt_ma_st,
  input         io_dmem_s2_xcpt_pf_ld,
  input         io_dmem_s2_xcpt_pf_st,
  input         io_dmem_s2_xcpt_ae_ld,
  input         io_dmem_s2_xcpt_ae_st,
  input         io_dmem_ordered,
  input         io_dmem_perf_acquire,
  input         io_dmem_perf_release,
  input         io_dmem_perf_grant,
  input         io_dmem_perf_tlbMiss,
  input         io_dmem_perf_blocked,
  output        io_dmem_keep_clock_enabled,
  input         io_dmem_clock_enabled,
  output [3:0]  io_ptw_ptbr_mode,
  output [43:0] io_ptw_ptbr_ppn,
  output        io_ptw_sfence_valid,
  output        io_ptw_sfence_bits_rs1,
  output        io_ptw_sfence_bits_rs2,
  output [38:0] io_ptw_sfence_bits_addr,
  output        io_ptw_status_debug,
  output [1:0]  io_ptw_status_dprv,
  output [1:0]  io_ptw_status_prv,
  output        io_ptw_status_mxr,
  output        io_ptw_status_sum,
  output        io_ptw_pmp_0_cfg_l,
  output [1:0]  io_ptw_pmp_0_cfg_a,
  output        io_ptw_pmp_0_cfg_x,
  output        io_ptw_pmp_0_cfg_w,
  output        io_ptw_pmp_0_cfg_r,
  output [34:0] io_ptw_pmp_0_addr,
  output [36:0] io_ptw_pmp_0_mask,
  output        io_ptw_pmp_1_cfg_l,
  output [1:0]  io_ptw_pmp_1_cfg_a,
  output        io_ptw_pmp_1_cfg_x,
  output        io_ptw_pmp_1_cfg_w,
  output        io_ptw_pmp_1_cfg_r,
  output [34:0] io_ptw_pmp_1_addr,
  output [36:0] io_ptw_pmp_1_mask,
  output        io_ptw_pmp_2_cfg_l,
  output [1:0]  io_ptw_pmp_2_cfg_a,
  output        io_ptw_pmp_2_cfg_x,
  output        io_ptw_pmp_2_cfg_w,
  output        io_ptw_pmp_2_cfg_r,
  output [34:0] io_ptw_pmp_2_addr,
  output [36:0] io_ptw_pmp_2_mask,
  output        io_ptw_pmp_3_cfg_l,
  output [1:0]  io_ptw_pmp_3_cfg_a,
  output        io_ptw_pmp_3_cfg_x,
  output        io_ptw_pmp_3_cfg_w,
  output        io_ptw_pmp_3_cfg_r,
  output [34:0] io_ptw_pmp_3_addr,
  output [36:0] io_ptw_pmp_3_mask,
  output        io_ptw_pmp_4_cfg_l,
  output [1:0]  io_ptw_pmp_4_cfg_a,
  output        io_ptw_pmp_4_cfg_x,
  output        io_ptw_pmp_4_cfg_w,
  output        io_ptw_pmp_4_cfg_r,
  output [34:0] io_ptw_pmp_4_addr,
  output [36:0] io_ptw_pmp_4_mask,
  output        io_ptw_pmp_5_cfg_l,
  output [1:0]  io_ptw_pmp_5_cfg_a,
  output        io_ptw_pmp_5_cfg_x,
  output        io_ptw_pmp_5_cfg_w,
  output        io_ptw_pmp_5_cfg_r,
  output [34:0] io_ptw_pmp_5_addr,
  output [36:0] io_ptw_pmp_5_mask,
  output        io_ptw_pmp_6_cfg_l,
  output [1:0]  io_ptw_pmp_6_cfg_a,
  output        io_ptw_pmp_6_cfg_x,
  output        io_ptw_pmp_6_cfg_w,
  output        io_ptw_pmp_6_cfg_r,
  output [34:0] io_ptw_pmp_6_addr,
  output [36:0] io_ptw_pmp_6_mask,
  output        io_ptw_pmp_7_cfg_l,
  output [1:0]  io_ptw_pmp_7_cfg_a,
  output        io_ptw_pmp_7_cfg_x,
  output        io_ptw_pmp_7_cfg_w,
  output        io_ptw_pmp_7_cfg_r,
  output [34:0] io_ptw_pmp_7_addr,
  output [36:0] io_ptw_pmp_7_mask,
  input         io_ptw_perf_l2miss,
  output        io_ptw_customCSRs_csrs_0_wen,
  output [63:0] io_ptw_customCSRs_csrs_0_value,
  output [63:0] io_ptw_customCSRs_csrs_1_value,
  output [31:0] io_fpu_inst,
  output [63:0] io_fpu_fromint_data,
  output [2:0]  io_fpu_fcsr_rm,
  input         io_fpu_fcsr_flags_valid,
  input  [4:0]  io_fpu_fcsr_flags_bits,
  input  [63:0] io_fpu_store_data,
  input  [63:0] io_fpu_toint_data,
  output        io_fpu_dmem_resp_val,
  output [2:0]  io_fpu_dmem_resp_type,
  output [4:0]  io_fpu_dmem_resp_tag,
  output [63:0] io_fpu_dmem_resp_data,
  output        io_fpu_valid,
  input         io_fpu_fcsr_rdy,
  input         io_fpu_nack_mem,
  input         io_fpu_illegal_rm,
  output        io_fpu_killx,
  output        io_fpu_killm,
  input         io_fpu_dec_ldst,
  input         io_fpu_dec_wen,
  input         io_fpu_dec_ren1,
  input         io_fpu_dec_ren2,
  input         io_fpu_dec_ren3,
  input         io_fpu_dec_swap23,
  input         io_fpu_dec_fma,
  input         io_fpu_dec_div,
  input         io_fpu_dec_sqrt,
  input         io_fpu_sboard_set,
  input         io_fpu_sboard_clr,
  input  [4:0]  io_fpu_sboard_clra,
  output        io_fpu_keep_clock_enabled,
  output        io_trace_0_valid,
  output [39:0] io_trace_0_iaddr,
  output [31:0] io_trace_0_insn,
  output [2:0]  io_trace_0_priv,
  output        io_trace_0_exception,
  output        io_trace_0_interrupt,
  output        io_bpwatch_0_valid_0,
  output [2:0]  io_bpwatch_0_action,
  output        io_bpwatch_1_valid_0,
  output [2:0]  io_bpwatch_1_action,
  output        io_bpwatch_2_valid_0,
  output [2:0]  io_bpwatch_2_action,
  output        io_bpwatch_3_valid_0,
  output [2:0]  io_bpwatch_3_action,
  output        io_bpwatch_4_valid_0,
  output [2:0]  io_bpwatch_4_action,
  output        io_bpwatch_5_valid_0,
  output [2:0]  io_bpwatch_5_action,
  output        io_bpwatch_6_valid_0,
  output [2:0]  io_bpwatch_6_action,
  output        io_bpwatch_7_valid_0,
  output [2:0]  io_bpwatch_7_action,
  output        io_bpwatch_8_valid_0,
  output [2:0]  io_bpwatch_8_action,
  output        io_bpwatch_9_valid_0,
  output [2:0]  io_bpwatch_9_action,
  output        io_bpwatch_10_valid_0,
  output [2:0]  io_bpwatch_10_action,
  output        io_bpwatch_11_valid_0,
  output [2:0]  io_bpwatch_11_action,
  output        io_bpwatch_12_valid_0,
  output [2:0]  io_bpwatch_12_action,
  output        io_bpwatch_13_valid_0,
  output [2:0]  io_bpwatch_13_action,
  output        io_bpwatch_14_valid_0,
  output [2:0]  io_bpwatch_14_action,
  output        io_bpwatch_15_valid_0,
  output [2:0]  io_bpwatch_15_action,
  output        io_cease,
  output        io_wfi,
  output        io_debug,
  input         io_traceStall,
  input         psd_test_clock_enable
);
`ifdef RANDOMIZE_GARBAGE_ASSIGN
  reg [63:0] _RAND_1;
  reg [63:0] _RAND_2;
`endif // RANDOMIZE_GARBAGE_ASSIGN
`ifdef RANDOMIZE_MEM_INIT
  reg [63:0] _RAND_0;
`endif // RANDOMIZE_MEM_INIT
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [63:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [63:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [31:0] _RAND_59;
  reg [31:0] _RAND_60;
  reg [31:0] _RAND_61;
  reg [31:0] _RAND_62;
  reg [31:0] _RAND_63;
  reg [31:0] _RAND_64;
  reg [31:0] _RAND_65;
  reg [31:0] _RAND_66;
  reg [31:0] _RAND_67;
  reg [31:0] _RAND_68;
  reg [31:0] _RAND_69;
  reg [31:0] _RAND_70;
  reg [31:0] _RAND_71;
  reg [31:0] _RAND_72;
  reg [31:0] _RAND_73;
  reg [31:0] _RAND_74;
  reg [31:0] _RAND_75;
  reg [31:0] _RAND_76;
  reg [31:0] _RAND_77;
  reg [31:0] _RAND_78;
  reg [31:0] _RAND_79;
  reg [31:0] _RAND_80;
  reg [31:0] _RAND_81;
  reg [31:0] _RAND_82;
  reg [31:0] _RAND_83;
  reg [31:0] _RAND_84;
  reg [31:0] _RAND_85;
  reg [63:0] _RAND_86;
  reg [31:0] _RAND_87;
  reg [31:0] _RAND_88;
  reg [31:0] _RAND_89;
  reg [31:0] _RAND_90;
  reg [63:0] _RAND_91;
  reg [31:0] _RAND_92;
  reg [31:0] _RAND_93;
  reg [31:0] _RAND_94;
  reg [31:0] _RAND_95;
  reg [63:0] _RAND_96;
  reg [63:0] _RAND_97;
  reg [31:0] _RAND_98;
  reg [31:0] _RAND_99;
  reg [31:0] _RAND_100;
  reg [31:0] _RAND_101;
  reg [31:0] _RAND_102;
  reg [31:0] _RAND_103;
  reg [31:0] _RAND_104;
  reg [31:0] _RAND_105;
  reg [31:0] _RAND_106;
  reg [31:0] _RAND_107;
  reg [31:0] _RAND_108;
  reg [31:0] _RAND_109;
  reg [31:0] _RAND_110;
  reg [31:0] _RAND_111;
  reg [31:0] _RAND_112;
  reg [31:0] _RAND_113;
  reg [31:0] _RAND_114;
  reg [31:0] _RAND_115;
  reg [31:0] _RAND_116;
  reg [31:0] _RAND_117;
  reg [31:0] _RAND_118;
  reg [63:0] _RAND_119;
  reg [31:0] _RAND_120;
  reg [63:0] _RAND_121;
  reg [31:0] _RAND_122;
  reg [31:0] _RAND_123;
  reg [31:0] _RAND_124;
  reg [63:0] _RAND_125;
  reg [63:0] _RAND_126;
  reg [31:0] _RAND_127;
  reg [31:0] _RAND_128;
  reg [31:0] _RAND_129;
  reg [31:0] _RAND_130;
  reg [31:0] _RAND_131;
  reg [31:0] _RAND_132;
  reg [31:0] _RAND_133;
  reg [31:0] _RAND_134;
  reg [31:0] _RAND_135;
  reg [31:0] _RAND_136;
  reg [31:0] _RAND_137;
  reg [31:0] _RAND_138;
  reg [31:0] _RAND_139;
  reg [31:0] _RAND_140;
  reg [31:0] _RAND_141;
  reg [31:0] _RAND_142;
  reg [31:0] _RAND_143;
  reg [31:0] _RAND_144;
  reg [31:0] _RAND_145;
  reg [31:0] _RAND_146;
  reg [31:0] _RAND_147;
  reg [63:0] _RAND_148;
  reg [63:0] _RAND_149;
  reg [31:0] _RAND_150;
  reg [31:0] _RAND_151;
  reg [31:0] _RAND_152;
  reg [31:0] _RAND_153;
  reg [31:0] _RAND_154;
  reg [31:0] _RAND_155;
  reg [31:0] _RAND_156;
  reg [31:0] _RAND_157;
  reg [31:0] _RAND_158;
  reg [31:0] _RAND_159;
  reg [31:0] _RAND_160;
  reg [31:0] _RAND_161;
  reg [31:0] _RAND_162;
  reg [31:0] _RAND_163;
  reg [31:0] _RAND_164;
  reg [31:0] _RAND_165;
  reg [31:0] _RAND_166;
  reg [31:0] _RAND_167;
  reg [31:0] _RAND_168;
  reg [31:0] _RAND_169;
  reg [31:0] _RAND_170;
  reg [63:0] _RAND_171;
  reg [63:0] _RAND_172;
  reg [63:0] _RAND_173;
  reg [63:0] _RAND_174;
`endif // RANDOMIZE_REG_INIT
  wire  gated_clock_rocket_clock_gate_in; // @[ClockGate.scala 24:20]
  wire  gated_clock_rocket_clock_gate_test_en; // @[ClockGate.scala 24:20]
  wire  gated_clock_rocket_clock_gate_en; // @[ClockGate.scala 24:20]
  wire  gated_clock_rocket_clock_gate_out; // @[ClockGate.scala 24:20]
  wire  ibuf_rf_reset; // @[RocketCore.scala 255:20]
  wire  ibuf_clock; // @[RocketCore.scala 255:20]
  wire  ibuf_reset; // @[RocketCore.scala 255:20]
  wire  ibuf_io_imem_ready; // @[RocketCore.scala 255:20]
  wire  ibuf_io_imem_valid; // @[RocketCore.scala 255:20]
  wire  ibuf_io_imem_bits_btb_taken; // @[RocketCore.scala 255:20]
  wire  ibuf_io_imem_bits_btb_bridx; // @[RocketCore.scala 255:20]
  wire [4:0] ibuf_io_imem_bits_btb_entry; // @[RocketCore.scala 255:20]
  wire [7:0] ibuf_io_imem_bits_btb_bht_history; // @[RocketCore.scala 255:20]
  wire [39:0] ibuf_io_imem_bits_pc; // @[RocketCore.scala 255:20]
  wire [31:0] ibuf_io_imem_bits_data; // @[RocketCore.scala 255:20]
  wire  ibuf_io_imem_bits_xcpt_pf_inst; // @[RocketCore.scala 255:20]
  wire  ibuf_io_imem_bits_xcpt_ae_inst; // @[RocketCore.scala 255:20]
  wire  ibuf_io_imem_bits_replay; // @[RocketCore.scala 255:20]
  wire  ibuf_io_kill; // @[RocketCore.scala 255:20]
  wire [39:0] ibuf_io_pc; // @[RocketCore.scala 255:20]
  wire  ibuf_io_btb_resp_taken; // @[RocketCore.scala 255:20]
  wire [4:0] ibuf_io_btb_resp_entry; // @[RocketCore.scala 255:20]
  wire [7:0] ibuf_io_btb_resp_bht_history; // @[RocketCore.scala 255:20]
  wire  ibuf_io_inst_0_ready; // @[RocketCore.scala 255:20]
  wire  ibuf_io_inst_0_valid; // @[RocketCore.scala 255:20]
  wire  ibuf_io_inst_0_bits_xcpt0_pf_inst; // @[RocketCore.scala 255:20]
  wire  ibuf_io_inst_0_bits_xcpt0_ae_inst; // @[RocketCore.scala 255:20]
  wire  ibuf_io_inst_0_bits_xcpt1_pf_inst; // @[RocketCore.scala 255:20]
  wire  ibuf_io_inst_0_bits_xcpt1_ae_inst; // @[RocketCore.scala 255:20]
  wire  ibuf_io_inst_0_bits_replay; // @[RocketCore.scala 255:20]
  wire  ibuf_io_inst_0_bits_rvc; // @[RocketCore.scala 255:20]
  wire [31:0] ibuf_io_inst_0_bits_inst_bits; // @[RocketCore.scala 255:20]
  wire [4:0] ibuf_io_inst_0_bits_inst_rd; // @[RocketCore.scala 255:20]
  wire [4:0] ibuf_io_inst_0_bits_inst_rs1; // @[RocketCore.scala 255:20]
  wire [4:0] ibuf_io_inst_0_bits_inst_rs2; // @[RocketCore.scala 255:20]
  wire [4:0] ibuf_io_inst_0_bits_inst_rs3; // @[RocketCore.scala 255:20]
  wire [31:0] ibuf_io_inst_0_bits_raw; // @[RocketCore.scala 255:20]
  reg [63:0] rf [0:30]; // @[RocketCore.scala 1017:15]
  wire [63:0] rf_id_rs_MPORT_data; // @[RocketCore.scala 1017:15]
  wire [4:0] rf_id_rs_MPORT_addr; // @[RocketCore.scala 1017:15]
  wire [63:0] rf_id_rs_MPORT_1_data; // @[RocketCore.scala 1017:15]
  wire [4:0] rf_id_rs_MPORT_1_addr; // @[RocketCore.scala 1017:15]
  wire [63:0] rf_MPORT_data; // @[RocketCore.scala 1017:15]
  wire [4:0] rf_MPORT_addr; // @[RocketCore.scala 1017:15]
  wire  rf_MPORT_mask; // @[RocketCore.scala 1017:15]
  wire  rf_MPORT_en; // @[RocketCore.scala 1017:15]
  wire  csr_rf_reset; // @[RocketCore.scala 283:19]
  wire  csr_clock; // @[RocketCore.scala 283:19]
  wire  csr_reset; // @[RocketCore.scala 283:19]
  wire  csr_io_ungated_clock; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_debug; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_mtip; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_msip; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_meip; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_seip; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_0; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_1; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_2; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_3; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_4; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_5; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_6; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_7; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_8; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_9; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_10; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_11; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_12; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_13; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_14; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_lip_15; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupts_nmi_rnmi; // @[RocketCore.scala 283:19]
  wire [36:0] csr_io_interrupts_nmi_rnmi_interrupt_vector; // @[RocketCore.scala 283:19]
  wire [36:0] csr_io_interrupts_nmi_rnmi_exception_vector; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_hartid; // @[RocketCore.scala 283:19]
  wire [11:0] csr_io_rw_addr; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_rw_cmd; // @[RocketCore.scala 283:19]
  wire [63:0] csr_io_rw_rdata; // @[RocketCore.scala 283:19]
  wire [63:0] csr_io_rw_wdata; // @[RocketCore.scala 283:19]
  wire [11:0] csr_io_decode_0_csr; // @[RocketCore.scala 283:19]
  wire  csr_io_decode_0_fp_illegal; // @[RocketCore.scala 283:19]
  wire  csr_io_decode_0_fp_csr; // @[RocketCore.scala 283:19]
  wire  csr_io_decode_0_read_illegal; // @[RocketCore.scala 283:19]
  wire  csr_io_decode_0_write_illegal; // @[RocketCore.scala 283:19]
  wire  csr_io_decode_0_write_flush; // @[RocketCore.scala 283:19]
  wire  csr_io_decode_0_system_illegal; // @[RocketCore.scala 283:19]
  wire  csr_io_csr_stall; // @[RocketCore.scala 283:19]
  wire  csr_io_eret; // @[RocketCore.scala 283:19]
  wire  csr_io_singleStep; // @[RocketCore.scala 283:19]
  wire  csr_io_status_debug; // @[RocketCore.scala 283:19]
  wire  csr_io_status_cease; // @[RocketCore.scala 283:19]
  wire  csr_io_status_wfi; // @[RocketCore.scala 283:19]
  wire [31:0] csr_io_status_isa; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_status_dprv; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_status_prv; // @[RocketCore.scala 283:19]
  wire  csr_io_status_sd; // @[RocketCore.scala 283:19]
  wire [26:0] csr_io_status_zero2; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_status_sxl; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_status_uxl; // @[RocketCore.scala 283:19]
  wire  csr_io_status_sd_rv32; // @[RocketCore.scala 283:19]
  wire [7:0] csr_io_status_zero1; // @[RocketCore.scala 283:19]
  wire  csr_io_status_tsr; // @[RocketCore.scala 283:19]
  wire  csr_io_status_tw; // @[RocketCore.scala 283:19]
  wire  csr_io_status_tvm; // @[RocketCore.scala 283:19]
  wire  csr_io_status_mxr; // @[RocketCore.scala 283:19]
  wire  csr_io_status_sum; // @[RocketCore.scala 283:19]
  wire  csr_io_status_mprv; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_status_xs; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_status_fs; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_status_mpp; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_status_vs; // @[RocketCore.scala 283:19]
  wire  csr_io_status_spp; // @[RocketCore.scala 283:19]
  wire  csr_io_status_mpie; // @[RocketCore.scala 283:19]
  wire  csr_io_status_hpie; // @[RocketCore.scala 283:19]
  wire  csr_io_status_spie; // @[RocketCore.scala 283:19]
  wire  csr_io_status_upie; // @[RocketCore.scala 283:19]
  wire  csr_io_status_mie; // @[RocketCore.scala 283:19]
  wire  csr_io_status_hie; // @[RocketCore.scala 283:19]
  wire  csr_io_status_sie; // @[RocketCore.scala 283:19]
  wire  csr_io_status_uie; // @[RocketCore.scala 283:19]
  wire [3:0] csr_io_ptbr_mode; // @[RocketCore.scala 283:19]
  wire [43:0] csr_io_ptbr_ppn; // @[RocketCore.scala 283:19]
  wire [39:0] csr_io_evec; // @[RocketCore.scala 283:19]
  wire  csr_io_exception; // @[RocketCore.scala 283:19]
  wire  csr_io_retire; // @[RocketCore.scala 283:19]
  wire [63:0] csr_io_cause; // @[RocketCore.scala 283:19]
  wire [39:0] csr_io_pc; // @[RocketCore.scala 283:19]
  wire [39:0] csr_io_tval; // @[RocketCore.scala 283:19]
  wire [63:0] csr_io_time; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_fcsr_rm; // @[RocketCore.scala 283:19]
  wire  csr_io_fcsr_flags_valid; // @[RocketCore.scala 283:19]
  wire [4:0] csr_io_fcsr_flags_bits; // @[RocketCore.scala 283:19]
  wire  csr_io_interrupt; // @[RocketCore.scala 283:19]
  wire [63:0] csr_io_interrupt_cause; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_0_control_action; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_0_control_chain; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_0_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_0_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_0_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_0_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_0_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_0_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_0_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_0_address; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_1_control_action; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_1_control_chain; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_1_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_1_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_1_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_1_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_1_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_1_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_1_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_1_address; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_2_control_action; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_2_control_chain; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_2_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_2_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_2_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_2_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_2_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_2_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_2_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_2_address; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_3_control_action; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_3_control_chain; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_3_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_3_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_3_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_3_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_3_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_3_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_3_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_3_address; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_4_control_action; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_4_control_chain; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_4_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_4_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_4_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_4_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_4_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_4_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_4_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_4_address; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_5_control_action; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_5_control_chain; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_5_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_5_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_5_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_5_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_5_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_5_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_5_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_5_address; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_6_control_action; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_6_control_chain; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_6_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_6_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_6_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_6_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_6_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_6_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_6_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_6_address; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_7_control_action; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_7_control_chain; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_7_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_7_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_7_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_7_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_7_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_7_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_7_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_7_address; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_8_control_action; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_8_control_chain; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_8_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_8_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_8_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_8_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_8_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_8_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_8_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_8_address; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_9_control_action; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_9_control_chain; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_9_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_9_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_9_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_9_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_9_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_9_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_9_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_9_address; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_10_control_action; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_10_control_chain; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_10_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_10_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_10_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_10_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_10_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_10_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_10_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_10_address; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_11_control_action; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_11_control_chain; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_11_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_11_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_11_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_11_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_11_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_11_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_11_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_11_address; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_12_control_action; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_12_control_chain; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_12_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_12_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_12_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_12_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_12_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_12_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_12_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_12_address; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_13_control_action; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_13_control_chain; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_13_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_13_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_13_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_13_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_13_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_13_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_13_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_13_address; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_14_control_action; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_14_control_chain; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_14_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_14_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_14_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_14_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_14_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_14_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_14_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_14_address; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_bp_15_control_action; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_bp_15_control_tmatch; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_15_control_m; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_15_control_s; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_15_control_u; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_15_control_x; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_15_control_w; // @[RocketCore.scala 283:19]
  wire  csr_io_bp_15_control_r; // @[RocketCore.scala 283:19]
  wire [38:0] csr_io_bp_15_address; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_0_cfg_l; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_pmp_0_cfg_a; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_0_cfg_x; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_0_cfg_w; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_0_cfg_r; // @[RocketCore.scala 283:19]
  wire [34:0] csr_io_pmp_0_addr; // @[RocketCore.scala 283:19]
  wire [36:0] csr_io_pmp_0_mask; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_1_cfg_l; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_pmp_1_cfg_a; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_1_cfg_x; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_1_cfg_w; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_1_cfg_r; // @[RocketCore.scala 283:19]
  wire [34:0] csr_io_pmp_1_addr; // @[RocketCore.scala 283:19]
  wire [36:0] csr_io_pmp_1_mask; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_2_cfg_l; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_pmp_2_cfg_a; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_2_cfg_x; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_2_cfg_w; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_2_cfg_r; // @[RocketCore.scala 283:19]
  wire [34:0] csr_io_pmp_2_addr; // @[RocketCore.scala 283:19]
  wire [36:0] csr_io_pmp_2_mask; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_3_cfg_l; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_pmp_3_cfg_a; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_3_cfg_x; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_3_cfg_w; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_3_cfg_r; // @[RocketCore.scala 283:19]
  wire [34:0] csr_io_pmp_3_addr; // @[RocketCore.scala 283:19]
  wire [36:0] csr_io_pmp_3_mask; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_4_cfg_l; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_pmp_4_cfg_a; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_4_cfg_x; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_4_cfg_w; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_4_cfg_r; // @[RocketCore.scala 283:19]
  wire [34:0] csr_io_pmp_4_addr; // @[RocketCore.scala 283:19]
  wire [36:0] csr_io_pmp_4_mask; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_5_cfg_l; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_pmp_5_cfg_a; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_5_cfg_x; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_5_cfg_w; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_5_cfg_r; // @[RocketCore.scala 283:19]
  wire [34:0] csr_io_pmp_5_addr; // @[RocketCore.scala 283:19]
  wire [36:0] csr_io_pmp_5_mask; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_6_cfg_l; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_pmp_6_cfg_a; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_6_cfg_x; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_6_cfg_w; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_6_cfg_r; // @[RocketCore.scala 283:19]
  wire [34:0] csr_io_pmp_6_addr; // @[RocketCore.scala 283:19]
  wire [36:0] csr_io_pmp_6_mask; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_7_cfg_l; // @[RocketCore.scala 283:19]
  wire [1:0] csr_io_pmp_7_cfg_a; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_7_cfg_x; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_7_cfg_w; // @[RocketCore.scala 283:19]
  wire  csr_io_pmp_7_cfg_r; // @[RocketCore.scala 283:19]
  wire [34:0] csr_io_pmp_7_addr; // @[RocketCore.scala 283:19]
  wire [36:0] csr_io_pmp_7_mask; // @[RocketCore.scala 283:19]
  wire [63:0] csr_io_counters_0_eventSel; // @[RocketCore.scala 283:19]
  wire  csr_io_counters_0_inc; // @[RocketCore.scala 283:19]
  wire [63:0] csr_io_counters_1_eventSel; // @[RocketCore.scala 283:19]
  wire  csr_io_counters_1_inc; // @[RocketCore.scala 283:19]
  wire [63:0] csr_io_counters_2_eventSel; // @[RocketCore.scala 283:19]
  wire  csr_io_counters_2_inc; // @[RocketCore.scala 283:19]
  wire [63:0] csr_io_counters_3_eventSel; // @[RocketCore.scala 283:19]
  wire  csr_io_counters_3_inc; // @[RocketCore.scala 283:19]
  wire  csr_io_inhibit_cycle; // @[RocketCore.scala 283:19]
  wire [31:0] csr_io_inst_0; // @[RocketCore.scala 283:19]
  wire  csr_io_trace_0_valid; // @[RocketCore.scala 283:19]
  wire [39:0] csr_io_trace_0_iaddr; // @[RocketCore.scala 283:19]
  wire [31:0] csr_io_trace_0_insn; // @[RocketCore.scala 283:19]
  wire [2:0] csr_io_trace_0_priv; // @[RocketCore.scala 283:19]
  wire  csr_io_trace_0_exception; // @[RocketCore.scala 283:19]
  wire  csr_io_trace_0_interrupt; // @[RocketCore.scala 283:19]
  wire [63:0] csr_io_trace_0_cause; // @[RocketCore.scala 283:19]
  wire  csr_io_customCSRs_0_wen; // @[RocketCore.scala 283:19]
  wire [63:0] csr_io_customCSRs_0_value; // @[RocketCore.scala 283:19]
  wire [63:0] csr_io_customCSRs_1_value; // @[RocketCore.scala 283:19]
  wire [31:0] d_insn; // @[RocketCore.scala 292:19]
  wire  d_unpipelined; // @[RocketCore.scala 292:19]
  wire  d_pipelined; // @[RocketCore.scala 292:19]
  wire  d_multicycle; // @[RocketCore.scala 292:19]
  wire  bpu_io_status_debug; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_status_prv; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_0_control_action; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_0_control_chain; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_0_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_0_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_0_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_0_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_0_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_0_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_0_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_0_address; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_1_control_action; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_1_control_chain; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_1_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_1_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_1_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_1_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_1_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_1_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_1_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_1_address; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_2_control_action; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_2_control_chain; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_2_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_2_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_2_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_2_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_2_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_2_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_2_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_2_address; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_3_control_action; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_3_control_chain; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_3_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_3_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_3_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_3_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_3_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_3_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_3_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_3_address; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_4_control_action; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_4_control_chain; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_4_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_4_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_4_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_4_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_4_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_4_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_4_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_4_address; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_5_control_action; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_5_control_chain; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_5_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_5_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_5_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_5_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_5_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_5_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_5_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_5_address; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_6_control_action; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_6_control_chain; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_6_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_6_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_6_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_6_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_6_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_6_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_6_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_6_address; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_7_control_action; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_7_control_chain; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_7_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_7_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_7_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_7_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_7_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_7_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_7_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_7_address; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_8_control_action; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_8_control_chain; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_8_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_8_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_8_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_8_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_8_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_8_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_8_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_8_address; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_9_control_action; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_9_control_chain; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_9_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_9_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_9_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_9_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_9_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_9_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_9_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_9_address; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_10_control_action; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_10_control_chain; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_10_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_10_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_10_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_10_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_10_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_10_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_10_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_10_address; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_11_control_action; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_11_control_chain; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_11_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_11_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_11_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_11_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_11_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_11_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_11_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_11_address; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_12_control_action; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_12_control_chain; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_12_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_12_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_12_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_12_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_12_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_12_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_12_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_12_address; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_13_control_action; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_13_control_chain; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_13_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_13_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_13_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_13_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_13_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_13_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_13_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_13_address; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_14_control_action; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_14_control_chain; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_14_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_14_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_14_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_14_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_14_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_14_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_14_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_14_address; // @[RocketCore.scala 324:19]
  wire [2:0] bpu_io_bp_15_control_action; // @[RocketCore.scala 324:19]
  wire [1:0] bpu_io_bp_15_control_tmatch; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_15_control_m; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_15_control_s; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_15_control_u; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_15_control_x; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_15_control_w; // @[RocketCore.scala 324:19]
  wire  bpu_io_bp_15_control_r; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_bp_15_address; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_pc; // @[RocketCore.scala 324:19]
  wire [38:0] bpu_io_ea; // @[RocketCore.scala 324:19]
  wire  bpu_io_xcpt_if; // @[RocketCore.scala 324:19]
  wire  bpu_io_xcpt_ld; // @[RocketCore.scala 324:19]
  wire  bpu_io_xcpt_st; // @[RocketCore.scala 324:19]
  wire  bpu_io_debug_if; // @[RocketCore.scala 324:19]
  wire  bpu_io_debug_ld; // @[RocketCore.scala 324:19]
  wire  bpu_io_debug_st; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_0_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_0_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_0_ivalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_1_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_1_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_1_ivalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_2_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_2_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_2_ivalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_3_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_3_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_3_ivalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_4_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_4_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_4_ivalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_5_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_5_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_5_ivalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_6_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_6_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_6_ivalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_7_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_7_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_7_ivalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_8_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_8_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_8_ivalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_9_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_9_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_9_ivalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_10_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_10_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_10_ivalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_11_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_11_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_11_ivalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_12_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_12_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_12_ivalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_13_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_13_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_13_ivalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_14_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_14_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_14_ivalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_15_rvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_15_wvalid_0; // @[RocketCore.scala 324:19]
  wire  bpu_io_bpwatch_15_ivalid_0; // @[RocketCore.scala 324:19]
  wire  alu_io_dw; // @[RocketCore.scala 386:19]
  wire [3:0] alu_io_fn; // @[RocketCore.scala 386:19]
  wire [63:0] alu_io_in2; // @[RocketCore.scala 386:19]
  wire [63:0] alu_io_in1; // @[RocketCore.scala 386:19]
  wire [63:0] alu_io_out; // @[RocketCore.scala 386:19]
  wire [63:0] alu_io_adder_out; // @[RocketCore.scala 386:19]
  wire  alu_io_cmp_out; // @[RocketCore.scala 386:19]
  wire [31:0] u_insn; // @[RocketCore.scala 393:19]
  wire [63:0] u_rs1; // @[RocketCore.scala 393:19]
  wire [63:0] u_rs2; // @[RocketCore.scala 393:19]
  wire [63:0] u_rd; // @[RocketCore.scala 393:19]
  wire  u_1_clock; // @[RocketCore.scala 400:19]
  wire  u_1_valid; // @[RocketCore.scala 400:19]
  wire [31:0] u_1_insn; // @[RocketCore.scala 400:19]
  wire [63:0] u_1_rs1; // @[RocketCore.scala 400:19]
  wire [63:0] u_1_rs2; // @[RocketCore.scala 400:19]
  wire [63:0] u_1_rd; // @[RocketCore.scala 400:19]
  wire  div_rf_reset; // @[RocketCore.scala 410:19]
  wire  div_clock; // @[RocketCore.scala 410:19]
  wire  div_reset; // @[RocketCore.scala 410:19]
  wire  div_io_req_ready; // @[RocketCore.scala 410:19]
  wire  div_io_req_valid; // @[RocketCore.scala 410:19]
  wire [3:0] div_io_req_bits_fn; // @[RocketCore.scala 410:19]
  wire  div_io_req_bits_dw; // @[RocketCore.scala 410:19]
  wire [63:0] div_io_req_bits_in1; // @[RocketCore.scala 410:19]
  wire [63:0] div_io_req_bits_in2; // @[RocketCore.scala 410:19]
  wire [4:0] div_io_req_bits_tag; // @[RocketCore.scala 410:19]
  wire  div_io_kill; // @[RocketCore.scala 410:19]
  wire  div_io_resp_ready; // @[RocketCore.scala 410:19]
  wire  div_io_resp_valid; // @[RocketCore.scala 410:19]
  wire [63:0] div_io_resp_bits_data; // @[RocketCore.scala 410:19]
  wire [4:0] div_io_resp_bits_tag; // @[RocketCore.scala 410:19]
  wire  m_rf_reset; // @[RocketCore.scala 418:19]
  wire  m_clock; // @[RocketCore.scala 418:19]
  wire  m_reset; // @[RocketCore.scala 418:19]
  wire  m_io_req_valid; // @[RocketCore.scala 418:19]
  wire [3:0] m_io_req_bits_fn; // @[RocketCore.scala 418:19]
  wire  m_io_req_bits_dw; // @[RocketCore.scala 418:19]
  wire [63:0] m_io_req_bits_in1; // @[RocketCore.scala 418:19]
  wire [63:0] m_io_req_bits_in2; // @[RocketCore.scala 418:19]
  wire [63:0] m_io_resp_bits_data; // @[RocketCore.scala 418:19]
  reg  clock_en_reg; // @[RocketCore.scala 113:29]
  reg  long_latency_stall; // @[RocketCore.scala 114:31]
  reg  id_reg_pause; // @[RocketCore.scala 115:25]
  reg  imem_might_request_reg; // @[RocketCore.scala 116:35]
  reg  ex_ctrl_fp; // @[RocketCore.scala 191:20]
  reg  ex_ctrl_branch; // @[RocketCore.scala 191:20]
  reg  ex_ctrl_jal; // @[RocketCore.scala 191:20]
  reg  ex_ctrl_jalr; // @[RocketCore.scala 191:20]
  reg  ex_ctrl_rxs2; // @[RocketCore.scala 191:20]
  reg [1:0] ex_ctrl_sel_alu2; // @[RocketCore.scala 191:20]
  reg [1:0] ex_ctrl_sel_alu1; // @[RocketCore.scala 191:20]
  reg [2:0] ex_ctrl_sel_imm; // @[RocketCore.scala 191:20]
  reg  ex_ctrl_alu_dw; // @[RocketCore.scala 191:20]
  reg [3:0] ex_ctrl_alu_fn; // @[RocketCore.scala 191:20]
  reg  ex_ctrl_mem; // @[RocketCore.scala 191:20]
  reg [4:0] ex_ctrl_mem_cmd; // @[RocketCore.scala 191:20]
  reg  ex_ctrl_wfd; // @[RocketCore.scala 191:20]
  reg  ex_ctrl_mul; // @[RocketCore.scala 191:20]
  reg  ex_ctrl_div; // @[RocketCore.scala 191:20]
  reg  ex_ctrl_wxd; // @[RocketCore.scala 191:20]
  reg [2:0] ex_ctrl_csr; // @[RocketCore.scala 191:20]
  reg  ex_ctrl_fence_i; // @[RocketCore.scala 191:20]
  reg  mem_ctrl_fp; // @[RocketCore.scala 192:21]
  reg  mem_ctrl_branch; // @[RocketCore.scala 192:21]
  reg  mem_ctrl_jal; // @[RocketCore.scala 192:21]
  reg  mem_ctrl_jalr; // @[RocketCore.scala 192:21]
  reg  mem_ctrl_mem; // @[RocketCore.scala 192:21]
  reg  mem_ctrl_wfd; // @[RocketCore.scala 192:21]
  reg  mem_ctrl_mul; // @[RocketCore.scala 192:21]
  reg  mem_ctrl_div; // @[RocketCore.scala 192:21]
  reg  mem_ctrl_wxd; // @[RocketCore.scala 192:21]
  reg [2:0] mem_ctrl_csr; // @[RocketCore.scala 192:21]
  reg  mem_ctrl_fence_i; // @[RocketCore.scala 192:21]
  reg  wb_ctrl_fp; // @[RocketCore.scala 193:20]
  reg  wb_ctrl_mem; // @[RocketCore.scala 193:20]
  reg  wb_ctrl_wfd; // @[RocketCore.scala 193:20]
  reg  wb_ctrl_mul; // @[RocketCore.scala 193:20]
  reg  wb_ctrl_div; // @[RocketCore.scala 193:20]
  reg  wb_ctrl_wxd; // @[RocketCore.scala 193:20]
  reg [2:0] wb_ctrl_csr; // @[RocketCore.scala 193:20]
  reg  wb_ctrl_fence_i; // @[RocketCore.scala 193:20]
  reg  ex_reg_xcpt_interrupt; // @[RocketCore.scala 195:35]
  reg  ex_reg_valid; // @[RocketCore.scala 196:35]
  reg  ex_reg_rvc; // @[RocketCore.scala 197:35]
  reg  ex_reg_btb_resp_taken; // @[RocketCore.scala 198:35]
  reg [4:0] ex_reg_btb_resp_entry; // @[RocketCore.scala 198:35]
  reg [7:0] ex_reg_btb_resp_bht_history; // @[RocketCore.scala 198:35]
  reg  ex_reg_xcpt; // @[RocketCore.scala 199:35]
  reg  ex_reg_flush_pipe; // @[RocketCore.scala 200:35]
  reg  ex_reg_load_use; // @[RocketCore.scala 201:35]
  reg [63:0] ex_reg_cause; // @[RocketCore.scala 202:35]
  reg  ex_reg_replay; // @[RocketCore.scala 203:26]
  reg [39:0] ex_reg_pc; // @[RocketCore.scala 204:22]
  reg [1:0] ex_reg_mem_size; // @[RocketCore.scala 205:28]
  reg [31:0] ex_reg_inst; // @[RocketCore.scala 206:24]
  reg [31:0] ex_reg_raw_inst; // @[RocketCore.scala 207:28]
  reg  ex_scie_unpipelined; // @[RocketCore.scala 208:32]
  reg  ex_scie_pipelined; // @[RocketCore.scala 209:30]
  reg  ex_reg_wphit_0; // @[RocketCore.scala 210:36]
  reg  ex_reg_wphit_1; // @[RocketCore.scala 210:36]
  reg  ex_reg_wphit_2; // @[RocketCore.scala 210:36]
  reg  ex_reg_wphit_3; // @[RocketCore.scala 210:36]
  reg  ex_reg_wphit_4; // @[RocketCore.scala 210:36]
  reg  ex_reg_wphit_5; // @[RocketCore.scala 210:36]
  reg  ex_reg_wphit_6; // @[RocketCore.scala 210:36]
  reg  ex_reg_wphit_7; // @[RocketCore.scala 210:36]
  reg  ex_reg_wphit_8; // @[RocketCore.scala 210:36]
  reg  ex_reg_wphit_9; // @[RocketCore.scala 210:36]
  reg  ex_reg_wphit_10; // @[RocketCore.scala 210:36]
  reg  ex_reg_wphit_11; // @[RocketCore.scala 210:36]
  reg  ex_reg_wphit_12; // @[RocketCore.scala 210:36]
  reg  ex_reg_wphit_13; // @[RocketCore.scala 210:36]
  reg  ex_reg_wphit_14; // @[RocketCore.scala 210:36]
  reg  ex_reg_wphit_15; // @[RocketCore.scala 210:36]
  reg  mem_reg_xcpt_interrupt; // @[RocketCore.scala 212:36]
  reg  mem_reg_valid; // @[RocketCore.scala 213:36]
  reg  mem_reg_rvc; // @[RocketCore.scala 214:36]
  reg  mem_reg_btb_resp_taken; // @[RocketCore.scala 215:36]
  reg [4:0] mem_reg_btb_resp_entry; // @[RocketCore.scala 215:36]
  reg [7:0] mem_reg_btb_resp_bht_history; // @[RocketCore.scala 215:36]
  reg  mem_reg_xcpt; // @[RocketCore.scala 216:36]
  reg  mem_reg_replay; // @[RocketCore.scala 217:36]
  reg  mem_reg_flush_pipe; // @[RocketCore.scala 218:36]
  reg [63:0] mem_reg_cause; // @[RocketCore.scala 219:36]
  reg  mem_reg_slow_bypass; // @[RocketCore.scala 220:36]
  reg  mem_reg_load; // @[RocketCore.scala 221:36]
  reg  mem_reg_store; // @[RocketCore.scala 222:36]
  reg  mem_reg_sfence; // @[RocketCore.scala 223:27]
  reg [39:0] mem_reg_pc; // @[RocketCore.scala 224:23]
  reg [31:0] mem_reg_inst; // @[RocketCore.scala 225:25]
  reg [1:0] mem_reg_mem_size; // @[RocketCore.scala 226:29]
  reg [31:0] mem_reg_raw_inst; // @[RocketCore.scala 227:29]
  reg  mem_scie_pipelined; // @[RocketCore.scala 229:31]
  reg [63:0] mem_reg_wdata; // @[RocketCore.scala 230:26]
  reg [63:0] mem_reg_rs2; // @[RocketCore.scala 231:24]
  reg  mem_br_taken; // @[RocketCore.scala 232:25]
  reg  mem_reg_wphit_0; // @[RocketCore.scala 234:35]
  reg  mem_reg_wphit_1; // @[RocketCore.scala 234:35]
  reg  mem_reg_wphit_2; // @[RocketCore.scala 234:35]
  reg  mem_reg_wphit_3; // @[RocketCore.scala 234:35]
  reg  mem_reg_wphit_4; // @[RocketCore.scala 234:35]
  reg  mem_reg_wphit_5; // @[RocketCore.scala 234:35]
  reg  mem_reg_wphit_6; // @[RocketCore.scala 234:35]
  reg  mem_reg_wphit_7; // @[RocketCore.scala 234:35]
  reg  mem_reg_wphit_8; // @[RocketCore.scala 234:35]
  reg  mem_reg_wphit_9; // @[RocketCore.scala 234:35]
  reg  mem_reg_wphit_10; // @[RocketCore.scala 234:35]
  reg  mem_reg_wphit_11; // @[RocketCore.scala 234:35]
  reg  mem_reg_wphit_12; // @[RocketCore.scala 234:35]
  reg  mem_reg_wphit_13; // @[RocketCore.scala 234:35]
  reg  mem_reg_wphit_14; // @[RocketCore.scala 234:35]
  reg  mem_reg_wphit_15; // @[RocketCore.scala 234:35]
  reg  wb_reg_valid; // @[RocketCore.scala 236:35]
  reg  wb_reg_xcpt; // @[RocketCore.scala 237:35]
  reg  wb_reg_replay; // @[RocketCore.scala 238:35]
  reg  wb_reg_flush_pipe; // @[RocketCore.scala 239:35]
  reg [63:0] wb_reg_cause; // @[RocketCore.scala 240:35]
  reg  wb_reg_sfence; // @[RocketCore.scala 241:26]
  reg [39:0] wb_reg_pc; // @[RocketCore.scala 242:22]
  reg [1:0] wb_reg_mem_size; // @[RocketCore.scala 243:28]
  reg [31:0] wb_reg_inst; // @[RocketCore.scala 244:24]
  reg [31:0] wb_reg_raw_inst; // @[RocketCore.scala 245:28]
  reg [63:0] wb_reg_wdata; // @[RocketCore.scala 246:25]
  reg [63:0] wb_reg_rs2; // @[RocketCore.scala 247:23]
  reg  wb_reg_wphit_0; // @[RocketCore.scala 249:35]
  reg  wb_reg_wphit_1; // @[RocketCore.scala 249:35]
  reg  wb_reg_wphit_2; // @[RocketCore.scala 249:35]
  reg  wb_reg_wphit_3; // @[RocketCore.scala 249:35]
  reg  wb_reg_wphit_4; // @[RocketCore.scala 249:35]
  reg  wb_reg_wphit_5; // @[RocketCore.scala 249:35]
  reg  wb_reg_wphit_6; // @[RocketCore.scala 249:35]
  reg  wb_reg_wphit_7; // @[RocketCore.scala 249:35]
  reg  wb_reg_wphit_8; // @[RocketCore.scala 249:35]
  reg  wb_reg_wphit_9; // @[RocketCore.scala 249:35]
  reg  wb_reg_wphit_10; // @[RocketCore.scala 249:35]
  reg  wb_reg_wphit_11; // @[RocketCore.scala 249:35]
  reg  wb_reg_wphit_12; // @[RocketCore.scala 249:35]
  reg  wb_reg_wphit_13; // @[RocketCore.scala 249:35]
  reg  wb_reg_wphit_14; // @[RocketCore.scala 249:35]
  reg  wb_reg_wphit_15; // @[RocketCore.scala 249:35]
  wire  replay_wb_common = io_dmem_s2_nack | wb_reg_replay; // @[RocketCore.scala 638:42]
  wire  _T_175 = wb_reg_valid & wb_ctrl_mem; // @[RocketCore.scala 616:19]
  wire  _T_176 = wb_reg_valid & wb_ctrl_mem & io_dmem_s2_xcpt_ma_st; // @[RocketCore.scala 616:34]
  wire  _T_178 = _T_175 & io_dmem_s2_xcpt_ma_ld; // @[RocketCore.scala 617:34]
  wire  _T_180 = _T_175 & io_dmem_s2_xcpt_pf_st; // @[RocketCore.scala 618:34]
  wire  _T_182 = _T_175 & io_dmem_s2_xcpt_pf_ld; // @[RocketCore.scala 619:34]
  wire  _T_184 = _T_175 & io_dmem_s2_xcpt_ae_st; // @[RocketCore.scala 620:34]
  wire  _T_186 = _T_175 & io_dmem_s2_xcpt_ae_ld; // @[RocketCore.scala 621:34]
  wire  wb_xcpt = wb_reg_xcpt | _T_176 | _T_178 | _T_180 | _T_182 | _T_184 | _T_186; // @[RocketCore.scala 977:26]
  wire  take_pc_wb = replay_wb_common | wb_xcpt | csr_io_eret | wb_reg_flush_pipe; // @[RocketCore.scala 641:53]
  wire  ex_pc_valid = ex_reg_valid | ex_reg_replay | ex_reg_xcpt_interrupt; // @[RocketCore.scala 491:51]
  wire [63:0] _mem_npc_a_T = mem_reg_wdata; // @[RocketCore.scala 991:16]
  wire [24:0] a = _mem_npc_a_T[63:39]; // @[RocketCore.scala 991:23]
  wire  msb = $signed(a) == 25'sh0 | $signed(a) == -25'sh1 ? mem_reg_wdata[39] : ~mem_reg_wdata[38]; // @[RocketCore.scala 992:18]
  wire [38:0] mem_npc_lo = mem_reg_wdata[38:0]; // @[RocketCore.scala 993:16]
  wire [39:0] _mem_npc_T_2 = {msb,mem_npc_lo}; // @[RocketCore.scala 514:106]
  wire  _mem_br_target_T_1 = mem_ctrl_branch & mem_br_taken; // @[RocketCore.scala 511:25]
  wire  mem_br_target_sign = mem_reg_inst[31]; // @[RocketCore.scala 1039:53]
  wire  mem_br_target_hi_hi_hi = mem_reg_inst[31]; // @[Cat.scala 30:58]
  wire [10:0] mem_br_target_hi_hi_lo = {11{mem_br_target_sign}}; // @[Cat.scala 30:58]
  wire [7:0] mem_br_target_hi_lo_hi = {8{mem_br_target_sign}}; // @[Cat.scala 30:58]
  wire  mem_br_target_hi_lo_lo = mem_reg_inst[7]; // @[Cat.scala 30:58]
  wire [5:0] mem_br_target_lo_hi_hi = mem_reg_inst[30:25]; // @[RocketCore.scala 1045:66]
  wire [3:0] mem_br_target_lo_hi_lo = mem_reg_inst[11:8]; // @[RocketCore.scala 1047:57]
  wire [31:0] _mem_br_target_T_3 = {mem_br_target_hi_hi_hi,mem_br_target_hi_hi_lo,mem_br_target_hi_lo_hi,
    mem_br_target_hi_lo_lo,mem_br_target_lo_hi_hi,mem_br_target_lo_hi_lo,1'h0}; // @[RocketCore.scala 1053:53]
  wire [7:0] mem_br_target_hi_lo_hi_1 = mem_reg_inst[19:12]; // @[Cat.scala 30:58]
  wire  mem_br_target_hi_lo_lo_1 = mem_reg_inst[20]; // @[Cat.scala 30:58]
  wire [31:0] _mem_br_target_T_5 = {mem_br_target_hi_hi_hi,mem_br_target_hi_hi_lo,mem_br_target_hi_lo_hi_1,
    mem_br_target_hi_lo_lo_1,mem_br_target_lo_hi_hi,mem_reg_inst[24:21],1'h0}; // @[RocketCore.scala 1053:53]
  wire [3:0] _mem_br_target_T_6 = mem_reg_rvc ? $signed(4'sh2) : $signed(4'sh4); // @[RocketCore.scala 513:8]
  wire [31:0] _mem_br_target_T_7 = mem_ctrl_jal ? $signed(_mem_br_target_T_5) : $signed({{28{_mem_br_target_T_6[3]}},
    _mem_br_target_T_6}); // @[RocketCore.scala 512:8]
  wire [31:0] _mem_br_target_T_8 = mem_ctrl_branch & mem_br_taken ? $signed(_mem_br_target_T_3) : $signed(
    _mem_br_target_T_7); // @[RocketCore.scala 511:8]
  wire [39:0] _GEN_21 = {{8{_mem_br_target_T_8[31]}},_mem_br_target_T_8}; // @[RocketCore.scala 510:41]
  wire [39:0] mem_br_target = $signed(mem_reg_pc) + $signed(_GEN_21); // @[RocketCore.scala 510:41]
  wire [39:0] _mem_npc_T_3 = mem_ctrl_jalr | mem_reg_sfence ? $signed(_mem_npc_T_2) : $signed(mem_br_target); // @[RocketCore.scala 514:21]
  wire [39:0] mem_npc = $signed(_mem_npc_T_3) & -40'sh2; // @[RocketCore.scala 514:141]
  wire  _mem_wrong_npc_T_3 = ibuf_io_inst_0_valid | ibuf_io_imem_valid ? mem_npc != ibuf_io_pc : 1'h1; // @[RocketCore.scala 517:8]
  wire  mem_wrong_npc = ex_pc_valid ? mem_npc != ex_reg_pc : _mem_wrong_npc_T_3; // @[RocketCore.scala 516:8]
  wire  take_pc_mem = mem_reg_valid & (mem_wrong_npc | mem_reg_sfence); // @[RocketCore.scala 524:32]
  wire  take_pc_mem_wb = take_pc_wb | take_pc_mem; // @[RocketCore.scala 251:35]
  wire [31:0] _id_ctrl_decoder_bit_T = ibuf_io_inst_0_bits_inst_bits & 32'hfe00707f; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_bit_T_1 = _id_ctrl_decoder_bit_T == 32'h2000033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_3 = _id_ctrl_decoder_bit_T == 32'h2001033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_5 = _id_ctrl_decoder_bit_T == 32'h2003033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_7 = _id_ctrl_decoder_bit_T == 32'h2002033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_9 = _id_ctrl_decoder_bit_T == 32'h2004033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_11 = _id_ctrl_decoder_bit_T == 32'h2005033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_13 = _id_ctrl_decoder_bit_T == 32'h2006033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_15 = _id_ctrl_decoder_bit_T == 32'h2007033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_17 = _id_ctrl_decoder_bit_T == 32'h200003b; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_19 = _id_ctrl_decoder_bit_T == 32'h200403b; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_21 = _id_ctrl_decoder_bit_T == 32'h200503b; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_23 = _id_ctrl_decoder_bit_T == 32'h200603b; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_25 = _id_ctrl_decoder_bit_T == 32'h200703b; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_bit_T_26 = ibuf_io_inst_0_bits_inst_bits & 32'hf800707f; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_bit_T_27 = _id_ctrl_decoder_bit_T_26 == 32'h202f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_29 = _id_ctrl_decoder_bit_T_26 == 32'h2000202f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_31 = _id_ctrl_decoder_bit_T_26 == 32'h800202f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_33 = _id_ctrl_decoder_bit_T_26 == 32'h6000202f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_35 = _id_ctrl_decoder_bit_T_26 == 32'h4000202f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_37 = _id_ctrl_decoder_bit_T_26 == 32'h8000202f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_39 = _id_ctrl_decoder_bit_T_26 == 32'hc000202f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_41 = _id_ctrl_decoder_bit_T_26 == 32'ha000202f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_43 = _id_ctrl_decoder_bit_T_26 == 32'he000202f; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_bit_T_44 = ibuf_io_inst_0_bits_inst_bits & 32'hf9f0707f; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_bit_T_45 = _id_ctrl_decoder_bit_T_44 == 32'h1000202f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_47 = _id_ctrl_decoder_bit_T_26 == 32'h1800202f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_49 = _id_ctrl_decoder_bit_T_26 == 32'h302f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_51 = _id_ctrl_decoder_bit_T_26 == 32'h800302f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_53 = _id_ctrl_decoder_bit_T_26 == 32'h2000302f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_55 = _id_ctrl_decoder_bit_T_26 == 32'h6000302f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_57 = _id_ctrl_decoder_bit_T_26 == 32'h4000302f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_59 = _id_ctrl_decoder_bit_T_26 == 32'h8000302f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_61 = _id_ctrl_decoder_bit_T_26 == 32'hc000302f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_63 = _id_ctrl_decoder_bit_T_26 == 32'ha000302f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_65 = _id_ctrl_decoder_bit_T_26 == 32'he000302f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_67 = _id_ctrl_decoder_bit_T_44 == 32'h1000302f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_69 = _id_ctrl_decoder_bit_T_26 == 32'h1800302f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_71 = _id_ctrl_decoder_bit_T == 32'h20000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_73 = _id_ctrl_decoder_bit_T == 32'h20002053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_75 = _id_ctrl_decoder_bit_T == 32'h20001053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_77 = _id_ctrl_decoder_bit_T == 32'h28000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_79 = _id_ctrl_decoder_bit_T == 32'h28001053; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_bit_T_80 = ibuf_io_inst_0_bits_inst_bits & 32'hfe00007f; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_bit_T_81 = _id_ctrl_decoder_bit_T_80 == 32'h53; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_83 = _id_ctrl_decoder_bit_T_80 == 32'h8000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_85 = _id_ctrl_decoder_bit_T_80 == 32'h10000053; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_bit_T_86 = ibuf_io_inst_0_bits_inst_bits & 32'h600007f; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_bit_T_87 = _id_ctrl_decoder_bit_T_86 == 32'h43; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_89 = _id_ctrl_decoder_bit_T_86 == 32'h47; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_91 = _id_ctrl_decoder_bit_T_86 == 32'h4f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_93 = _id_ctrl_decoder_bit_T_86 == 32'h4b; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_bit_T_94 = ibuf_io_inst_0_bits_inst_bits & 32'hfff0707f; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_bit_T_95 = _id_ctrl_decoder_bit_T_94 == 32'he0001053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_97 = _id_ctrl_decoder_bit_T_94 == 32'he0000053; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_bit_T_98 = ibuf_io_inst_0_bits_inst_bits & 32'hfff0007f; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_bit_T_99 = _id_ctrl_decoder_bit_T_98 == 32'hc0000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_101 = _id_ctrl_decoder_bit_T_98 == 32'hc0100053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_103 = _id_ctrl_decoder_bit_T == 32'ha0002053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_105 = _id_ctrl_decoder_bit_T == 32'ha0001053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_107 = _id_ctrl_decoder_bit_T == 32'ha0000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_109 = _id_ctrl_decoder_bit_T_94 == 32'hf0000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_111 = _id_ctrl_decoder_bit_T_98 == 32'hd0000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_113 = _id_ctrl_decoder_bit_T_98 == 32'hd0100053; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_bit_T_114 = ibuf_io_inst_0_bits_inst_bits & 32'h707f; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_bit_T_115 = _id_ctrl_decoder_bit_T_114 == 32'h2007; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_117 = _id_ctrl_decoder_bit_T_114 == 32'h2027; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_119 = _id_ctrl_decoder_bit_T_80 == 32'h18000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_121 = _id_ctrl_decoder_bit_T_98 == 32'h58000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_123 = _id_ctrl_decoder_bit_T_98 == 32'hc0200053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_125 = _id_ctrl_decoder_bit_T_98 == 32'hc0300053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_127 = _id_ctrl_decoder_bit_T_98 == 32'hd0200053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_129 = _id_ctrl_decoder_bit_T_98 == 32'hd0300053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_131 = _id_ctrl_decoder_bit_T_98 == 32'h40100053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_133 = _id_ctrl_decoder_bit_T_98 == 32'h42000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_135 = _id_ctrl_decoder_bit_T == 32'h22000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_137 = _id_ctrl_decoder_bit_T == 32'h22002053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_139 = _id_ctrl_decoder_bit_T == 32'h22001053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_141 = _id_ctrl_decoder_bit_T == 32'h2a000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_143 = _id_ctrl_decoder_bit_T == 32'h2a001053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_145 = _id_ctrl_decoder_bit_T_80 == 32'h2000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_147 = _id_ctrl_decoder_bit_T_80 == 32'ha000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_149 = _id_ctrl_decoder_bit_T_80 == 32'h12000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_151 = _id_ctrl_decoder_bit_T_86 == 32'h2000043; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_153 = _id_ctrl_decoder_bit_T_86 == 32'h2000047; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_155 = _id_ctrl_decoder_bit_T_86 == 32'h200004f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_157 = _id_ctrl_decoder_bit_T_86 == 32'h200004b; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_159 = _id_ctrl_decoder_bit_T_94 == 32'he2001053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_161 = _id_ctrl_decoder_bit_T_98 == 32'hc2000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_163 = _id_ctrl_decoder_bit_T_98 == 32'hc2100053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_165 = _id_ctrl_decoder_bit_T == 32'ha2002053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_167 = _id_ctrl_decoder_bit_T == 32'ha2001053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_169 = _id_ctrl_decoder_bit_T == 32'ha2000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_171 = _id_ctrl_decoder_bit_T_98 == 32'hd2000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_173 = _id_ctrl_decoder_bit_T_98 == 32'hd2100053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_175 = _id_ctrl_decoder_bit_T_114 == 32'h3007; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_177 = _id_ctrl_decoder_bit_T_114 == 32'h3027; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_179 = _id_ctrl_decoder_bit_T_80 == 32'h1a000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_181 = _id_ctrl_decoder_bit_T_98 == 32'h5a000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_183 = _id_ctrl_decoder_bit_T_94 == 32'he2000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_185 = _id_ctrl_decoder_bit_T_98 == 32'hc2200053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_187 = _id_ctrl_decoder_bit_T_98 == 32'hc2300053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_189 = _id_ctrl_decoder_bit_T_94 == 32'hf2000053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_191 = _id_ctrl_decoder_bit_T_98 == 32'hd2200053; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_193 = _id_ctrl_decoder_bit_T_98 == 32'hd2300053; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_bit_T_194 = ibuf_io_inst_0_bits_inst_bits & 32'h5f; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_bit_T_195 = _id_ctrl_decoder_bit_T_194 == 32'hb; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_197 = _id_ctrl_decoder_bit_T_114 == 32'h3003; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_199 = _id_ctrl_decoder_bit_T_114 == 32'h6003; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_201 = _id_ctrl_decoder_bit_T_114 == 32'h3023; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_bit_T_202 = ibuf_io_inst_0_bits_inst_bits & 32'hfc00707f; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_bit_T_203 = _id_ctrl_decoder_bit_T_202 == 32'h1013; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_205 = _id_ctrl_decoder_bit_T_202 == 32'h5013; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_207 = _id_ctrl_decoder_bit_T_202 == 32'h40005013; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_209 = _id_ctrl_decoder_bit_T_114 == 32'h1b; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_211 = _id_ctrl_decoder_bit_T == 32'h101b; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_213 = _id_ctrl_decoder_bit_T == 32'h501b; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_215 = _id_ctrl_decoder_bit_T == 32'h4000501b; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_217 = _id_ctrl_decoder_bit_T == 32'h3b; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_219 = _id_ctrl_decoder_bit_T == 32'h4000003b; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_221 = _id_ctrl_decoder_bit_T == 32'h103b; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_223 = _id_ctrl_decoder_bit_T == 32'h503b; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_225 = _id_ctrl_decoder_bit_T == 32'h4000503b; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_bit_T_226 = ibuf_io_inst_0_bits_inst_bits & 32'hfe007fff; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_bit_T_227 = _id_ctrl_decoder_bit_T_226 == 32'h12000073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_228 = ibuf_io_inst_0_bits_inst_bits == 32'h10200073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_229 = ibuf_io_inst_0_bits_inst_bits == 32'h7b200073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_230 = ibuf_io_inst_0_bits_inst_bits == 32'h70200073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_232 = _id_ctrl_decoder_bit_T_114 == 32'h100f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_233 = ibuf_io_inst_0_bits_inst_bits == 32'hfc000073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_234 = ibuf_io_inst_0_bits_inst_bits == 32'hfc200073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_236 = _id_ctrl_decoder_bit_T_114 == 32'h1063; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_238 = _id_ctrl_decoder_bit_T_114 == 32'h63; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_240 = _id_ctrl_decoder_bit_T_114 == 32'h4063; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_242 = _id_ctrl_decoder_bit_T_114 == 32'h6063; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_244 = _id_ctrl_decoder_bit_T_114 == 32'h5063; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_246 = _id_ctrl_decoder_bit_T_114 == 32'h7063; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_bit_T_247 = ibuf_io_inst_0_bits_inst_bits & 32'h7f; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_bit_T_248 = _id_ctrl_decoder_bit_T_247 == 32'h6f; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_250 = _id_ctrl_decoder_bit_T_114 == 32'h67; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_252 = _id_ctrl_decoder_bit_T_247 == 32'h17; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_254 = _id_ctrl_decoder_bit_T_114 == 32'h3; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_256 = _id_ctrl_decoder_bit_T_114 == 32'h1003; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_258 = _id_ctrl_decoder_bit_T_114 == 32'h2003; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_260 = _id_ctrl_decoder_bit_T_114 == 32'h4003; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_262 = _id_ctrl_decoder_bit_T_114 == 32'h5003; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_264 = _id_ctrl_decoder_bit_T_114 == 32'h23; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_266 = _id_ctrl_decoder_bit_T_114 == 32'h1023; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_268 = _id_ctrl_decoder_bit_T_114 == 32'h2023; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_270 = _id_ctrl_decoder_bit_T_247 == 32'h37; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_272 = _id_ctrl_decoder_bit_T_114 == 32'h13; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_274 = _id_ctrl_decoder_bit_T_114 == 32'h2013; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_276 = _id_ctrl_decoder_bit_T_114 == 32'h3013; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_278 = _id_ctrl_decoder_bit_T_114 == 32'h7013; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_280 = _id_ctrl_decoder_bit_T_114 == 32'h6013; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_282 = _id_ctrl_decoder_bit_T_114 == 32'h4013; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_284 = _id_ctrl_decoder_bit_T == 32'h33; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_286 = _id_ctrl_decoder_bit_T == 32'h40000033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_288 = _id_ctrl_decoder_bit_T == 32'h2033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_290 = _id_ctrl_decoder_bit_T == 32'h3033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_292 = _id_ctrl_decoder_bit_T == 32'h7033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_294 = _id_ctrl_decoder_bit_T == 32'h6033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_296 = _id_ctrl_decoder_bit_T == 32'h4033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_298 = _id_ctrl_decoder_bit_T == 32'h1033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_300 = _id_ctrl_decoder_bit_T == 32'h5033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_302 = _id_ctrl_decoder_bit_T == 32'h40005033; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_304 = _id_ctrl_decoder_bit_T_114 == 32'hf; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_305 = ibuf_io_inst_0_bits_inst_bits == 32'h73; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_306 = ibuf_io_inst_0_bits_inst_bits == 32'h100073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_307 = ibuf_io_inst_0_bits_inst_bits == 32'h30200073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_308 = ibuf_io_inst_0_bits_inst_bits == 32'h10500073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_309 = ibuf_io_inst_0_bits_inst_bits == 32'h30500073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_311 = _id_ctrl_decoder_bit_T_114 == 32'h1073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_313 = _id_ctrl_decoder_bit_T_114 == 32'h2073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_315 = _id_ctrl_decoder_bit_T_114 == 32'h3073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_317 = _id_ctrl_decoder_bit_T_114 == 32'h5073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_319 = _id_ctrl_decoder_bit_T_114 == 32'h6073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_321 = _id_ctrl_decoder_bit_T_114 == 32'h7073; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_bit_T_352 = _id_ctrl_decoder_bit_T_1 | _id_ctrl_decoder_bit_T_3 | _id_ctrl_decoder_bit_T_5 |
    _id_ctrl_decoder_bit_T_7 | _id_ctrl_decoder_bit_T_9 | _id_ctrl_decoder_bit_T_11 | _id_ctrl_decoder_bit_T_13 |
    _id_ctrl_decoder_bit_T_15 | _id_ctrl_decoder_bit_T_17 | _id_ctrl_decoder_bit_T_19 | _id_ctrl_decoder_bit_T_21 |
    _id_ctrl_decoder_bit_T_23 | _id_ctrl_decoder_bit_T_25 | _id_ctrl_decoder_bit_T_27 | _id_ctrl_decoder_bit_T_29 |
    _id_ctrl_decoder_bit_T_31 | _id_ctrl_decoder_bit_T_33 | _id_ctrl_decoder_bit_T_35 | _id_ctrl_decoder_bit_T_37 |
    _id_ctrl_decoder_bit_T_39 | _id_ctrl_decoder_bit_T_41 | _id_ctrl_decoder_bit_T_43 | _id_ctrl_decoder_bit_T_45 |
    _id_ctrl_decoder_bit_T_47 | _id_ctrl_decoder_bit_T_49 | _id_ctrl_decoder_bit_T_51 | _id_ctrl_decoder_bit_T_53 |
    _id_ctrl_decoder_bit_T_55 | _id_ctrl_decoder_bit_T_57 | _id_ctrl_decoder_bit_T_59 | _id_ctrl_decoder_bit_T_61; // @[Decode.scala 15:30]
  wire  _id_ctrl_decoder_bit_T_382 = _id_ctrl_decoder_bit_T_352 | _id_ctrl_decoder_bit_T_63 | _id_ctrl_decoder_bit_T_65
     | _id_ctrl_decoder_bit_T_67 | _id_ctrl_decoder_bit_T_69 | _id_ctrl_decoder_bit_T_71 | _id_ctrl_decoder_bit_T_73 |
    _id_ctrl_decoder_bit_T_75 | _id_ctrl_decoder_bit_T_77 | _id_ctrl_decoder_bit_T_79 | _id_ctrl_decoder_bit_T_81 |
    _id_ctrl_decoder_bit_T_83 | _id_ctrl_decoder_bit_T_85 | _id_ctrl_decoder_bit_T_87 | _id_ctrl_decoder_bit_T_89 |
    _id_ctrl_decoder_bit_T_91 | _id_ctrl_decoder_bit_T_93 | _id_ctrl_decoder_bit_T_95 | _id_ctrl_decoder_bit_T_97 |
    _id_ctrl_decoder_bit_T_99 | _id_ctrl_decoder_bit_T_101 | _id_ctrl_decoder_bit_T_103 | _id_ctrl_decoder_bit_T_105 |
    _id_ctrl_decoder_bit_T_107 | _id_ctrl_decoder_bit_T_109 | _id_ctrl_decoder_bit_T_111 | _id_ctrl_decoder_bit_T_113 |
    _id_ctrl_decoder_bit_T_115 | _id_ctrl_decoder_bit_T_117 | _id_ctrl_decoder_bit_T_119 | _id_ctrl_decoder_bit_T_121; // @[Decode.scala 15:30]
  wire  _id_ctrl_decoder_bit_T_412 = _id_ctrl_decoder_bit_T_382 | _id_ctrl_decoder_bit_T_123 |
    _id_ctrl_decoder_bit_T_125 | _id_ctrl_decoder_bit_T_127 | _id_ctrl_decoder_bit_T_129 | _id_ctrl_decoder_bit_T_131 |
    _id_ctrl_decoder_bit_T_133 | _id_ctrl_decoder_bit_T_135 | _id_ctrl_decoder_bit_T_137 | _id_ctrl_decoder_bit_T_139 |
    _id_ctrl_decoder_bit_T_141 | _id_ctrl_decoder_bit_T_143 | _id_ctrl_decoder_bit_T_145 | _id_ctrl_decoder_bit_T_147 |
    _id_ctrl_decoder_bit_T_149 | _id_ctrl_decoder_bit_T_151 | _id_ctrl_decoder_bit_T_153 | _id_ctrl_decoder_bit_T_155 |
    _id_ctrl_decoder_bit_T_157 | _id_ctrl_decoder_bit_T_159 | _id_ctrl_decoder_bit_T_161 | _id_ctrl_decoder_bit_T_163 |
    _id_ctrl_decoder_bit_T_165 | _id_ctrl_decoder_bit_T_167 | _id_ctrl_decoder_bit_T_169 | _id_ctrl_decoder_bit_T_171 |
    _id_ctrl_decoder_bit_T_173 | _id_ctrl_decoder_bit_T_175 | _id_ctrl_decoder_bit_T_177 | _id_ctrl_decoder_bit_T_179 |
    _id_ctrl_decoder_bit_T_181; // @[Decode.scala 15:30]
  wire  _id_ctrl_decoder_bit_T_442 = _id_ctrl_decoder_bit_T_412 | _id_ctrl_decoder_bit_T_183 |
    _id_ctrl_decoder_bit_T_185 | _id_ctrl_decoder_bit_T_187 | _id_ctrl_decoder_bit_T_189 | _id_ctrl_decoder_bit_T_191 |
    _id_ctrl_decoder_bit_T_193 | _id_ctrl_decoder_bit_T_195 | _id_ctrl_decoder_bit_T_197 | _id_ctrl_decoder_bit_T_199 |
    _id_ctrl_decoder_bit_T_201 | _id_ctrl_decoder_bit_T_203 | _id_ctrl_decoder_bit_T_205 | _id_ctrl_decoder_bit_T_207 |
    _id_ctrl_decoder_bit_T_209 | _id_ctrl_decoder_bit_T_211 | _id_ctrl_decoder_bit_T_213 | _id_ctrl_decoder_bit_T_215 |
    _id_ctrl_decoder_bit_T_217 | _id_ctrl_decoder_bit_T_219 | _id_ctrl_decoder_bit_T_221 | _id_ctrl_decoder_bit_T_223 |
    _id_ctrl_decoder_bit_T_225 | _id_ctrl_decoder_bit_T_227 | _id_ctrl_decoder_bit_T_228 | _id_ctrl_decoder_bit_T_229 |
    _id_ctrl_decoder_bit_T_230 | _id_ctrl_decoder_bit_T_232 | _id_ctrl_decoder_bit_T_233 | _id_ctrl_decoder_bit_T_234 |
    _id_ctrl_decoder_bit_T_236; // @[Decode.scala 15:30]
  wire  _id_ctrl_decoder_bit_T_472 = _id_ctrl_decoder_bit_T_442 | _id_ctrl_decoder_bit_T_238 |
    _id_ctrl_decoder_bit_T_240 | _id_ctrl_decoder_bit_T_242 | _id_ctrl_decoder_bit_T_244 | _id_ctrl_decoder_bit_T_246 |
    _id_ctrl_decoder_bit_T_248 | _id_ctrl_decoder_bit_T_250 | _id_ctrl_decoder_bit_T_252 | _id_ctrl_decoder_bit_T_254 |
    _id_ctrl_decoder_bit_T_256 | _id_ctrl_decoder_bit_T_258 | _id_ctrl_decoder_bit_T_260 | _id_ctrl_decoder_bit_T_262 |
    _id_ctrl_decoder_bit_T_264 | _id_ctrl_decoder_bit_T_266 | _id_ctrl_decoder_bit_T_268 | _id_ctrl_decoder_bit_T_270 |
    _id_ctrl_decoder_bit_T_272 | _id_ctrl_decoder_bit_T_274 | _id_ctrl_decoder_bit_T_276 | _id_ctrl_decoder_bit_T_278 |
    _id_ctrl_decoder_bit_T_280 | _id_ctrl_decoder_bit_T_282 | _id_ctrl_decoder_bit_T_284 | _id_ctrl_decoder_bit_T_286 |
    _id_ctrl_decoder_bit_T_288 | _id_ctrl_decoder_bit_T_290 | _id_ctrl_decoder_bit_T_292 | _id_ctrl_decoder_bit_T_294 |
    _id_ctrl_decoder_bit_T_296; // @[Decode.scala 15:30]
  wire  id_ctrl_decoder_0 = _id_ctrl_decoder_bit_T_472 | _id_ctrl_decoder_bit_T_298 | _id_ctrl_decoder_bit_T_300 |
    _id_ctrl_decoder_bit_T_302 | _id_ctrl_decoder_bit_T_304 | _id_ctrl_decoder_bit_T_305 | _id_ctrl_decoder_bit_T_306 |
    _id_ctrl_decoder_bit_T_307 | _id_ctrl_decoder_bit_T_308 | _id_ctrl_decoder_bit_T_309 | _id_ctrl_decoder_bit_T_311 |
    _id_ctrl_decoder_bit_T_313 | _id_ctrl_decoder_bit_T_315 | _id_ctrl_decoder_bit_T_317 | _id_ctrl_decoder_bit_T_319 |
    _id_ctrl_decoder_bit_T_321; // @[Decode.scala 15:30]
  wire [31:0] _id_ctrl_decoder_T = ibuf_io_inst_0_bits_inst_bits & 32'h5c; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_1 = _id_ctrl_decoder_T == 32'h4; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_2 = ibuf_io_inst_0_bits_inst_bits & 32'h60; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_3 = _id_ctrl_decoder_T_2 == 32'h40; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_1 = _id_ctrl_decoder_T_1 | _id_ctrl_decoder_T_3; // @[Decode.scala 15:30]
  wire [31:0] _id_ctrl_decoder_T_5 = ibuf_io_inst_0_bits_inst_bits & 32'h74; // @[Decode.scala 14:65]
  wire  id_ctrl_decoder_3 = _id_ctrl_decoder_T_5 == 32'h60; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_7 = ibuf_io_inst_0_bits_inst_bits & 32'h68; // @[Decode.scala 14:65]
  wire  id_ctrl_decoder_4 = _id_ctrl_decoder_T_7 == 32'h68; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_9 = ibuf_io_inst_0_bits_inst_bits & 32'h203c; // @[Decode.scala 14:65]
  wire  id_ctrl_decoder_5 = _id_ctrl_decoder_T_9 == 32'h24; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_8 = _id_ctrl_decoder_T == 32'h8; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_13 = ibuf_io_inst_0_bits_inst_bits & 32'h64; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_14 = _id_ctrl_decoder_T_13 == 32'h20; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_15 = ibuf_io_inst_0_bits_inst_bits & 32'h34; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_16 = _id_ctrl_decoder_T_15 == 32'h20; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_17 = ibuf_io_inst_0_bits_inst_bits & 32'h2048; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_18 = _id_ctrl_decoder_T_17 == 32'h2008; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_19 = ibuf_io_inst_0_bits_inst_bits & 32'h42003024; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_20 = _id_ctrl_decoder_T_19 == 32'h2000020; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_6 = id_ctrl_decoder_8 | _id_ctrl_decoder_T_14 | _id_ctrl_decoder_T_16 | _id_ctrl_decoder_T_18 |
    _id_ctrl_decoder_T_20; // @[Decode.scala 15:30]
  wire [31:0] _id_ctrl_decoder_T_25 = ibuf_io_inst_0_bits_inst_bits & 32'h44; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_26 = _id_ctrl_decoder_T_25 == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_27 = ibuf_io_inst_0_bits_inst_bits & 32'h4024; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_28 = _id_ctrl_decoder_T_27 == 32'h20; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_29 = ibuf_io_inst_0_bits_inst_bits & 32'h38; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_30 = _id_ctrl_decoder_T_29 == 32'h20; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_31 = ibuf_io_inst_0_bits_inst_bits & 32'h2050; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_32 = _id_ctrl_decoder_T_31 == 32'h2000; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_33 = ibuf_io_inst_0_bits_inst_bits & 32'h90000034; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_34 = _id_ctrl_decoder_T_33 == 32'h90000010; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_7 = _id_ctrl_decoder_T_26 | _id_ctrl_decoder_T_28 | _id_ctrl_decoder_T_30 |
    _id_ctrl_decoder_T_32 | _id_ctrl_decoder_T_34; // @[Decode.scala 15:30]
  wire [31:0] _id_ctrl_decoder_T_39 = ibuf_io_inst_0_bits_inst_bits & 32'h58; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_40 = _id_ctrl_decoder_T_39 == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_41 = ibuf_io_inst_0_bits_inst_bits & 32'hc; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_42 = _id_ctrl_decoder_T_41 == 32'h4; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_43 = ibuf_io_inst_0_bits_inst_bits & 32'h30; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_44 = _id_ctrl_decoder_T_43 == 32'h10; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_45 = ibuf_io_inst_0_bits_inst_bits & 32'h48; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_46 = _id_ctrl_decoder_T_45 == 32'h48; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_47 = ibuf_io_inst_0_bits_inst_bits & 32'h4050; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_48 = _id_ctrl_decoder_T_47 == 32'h4050; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_lo = _id_ctrl_decoder_T_40 | _id_ctrl_decoder_T_42 | _id_ctrl_decoder_T_44 |
    _id_ctrl_decoder_T_46 | _id_ctrl_decoder_T_48; // @[Decode.scala 15:30]
  wire [31:0] _id_ctrl_decoder_T_53 = ibuf_io_inst_0_bits_inst_bits & 32'h18; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_54 = _id_ctrl_decoder_T_53 == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_55 = ibuf_io_inst_0_bits_inst_bits & 32'h50; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_56 = _id_ctrl_decoder_T_55 == 32'h10; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_57 = ibuf_io_inst_0_bits_inst_bits & 32'h4008; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_58 = _id_ctrl_decoder_T_57 == 32'h4000; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_hi = _id_ctrl_decoder_T_54 | _id_ctrl_decoder_T_56 | _id_ctrl_decoder_T_58; // @[Decode.scala 15:30]
  wire [1:0] id_ctrl_decoder_9 = {id_ctrl_decoder_hi,id_ctrl_decoder_lo}; // @[Cat.scala 30:58]
  wire [31:0] _id_ctrl_decoder_T_61 = ibuf_io_inst_0_bits_inst_bits & 32'h4004; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_62 = _id_ctrl_decoder_T_61 == 32'h0; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_T_64 = _id_ctrl_decoder_T_55 == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_65 = ibuf_io_inst_0_bits_inst_bits & 32'h24; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_66 = _id_ctrl_decoder_T_65 == 32'h0; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_lo_1 = _id_ctrl_decoder_T_62 | _id_ctrl_decoder_T_64 | _id_ctrl_decoder_T_26 |
    _id_ctrl_decoder_T_66 | _id_ctrl_decoder_T_54; // @[Decode.scala 15:30]
  wire  _id_ctrl_decoder_T_72 = _id_ctrl_decoder_T_15 == 32'h14; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_hi_1 = _id_ctrl_decoder_T_72 | _id_ctrl_decoder_T_46; // @[Decode.scala 15:30]
  wire [1:0] id_ctrl_decoder_10 = {id_ctrl_decoder_hi_1,id_ctrl_decoder_lo_1}; // @[Cat.scala 30:58]
  wire  _id_ctrl_decoder_T_75 = _id_ctrl_decoder_T_53 == 32'h8; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_T_77 = _id_ctrl_decoder_T_25 == 32'h40; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_lo_2 = _id_ctrl_decoder_T_75 | _id_ctrl_decoder_T_77; // @[Decode.scala 15:30]
  wire [31:0] _id_ctrl_decoder_T_79 = ibuf_io_inst_0_bits_inst_bits & 32'h14; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_80 = _id_ctrl_decoder_T_79 == 32'h14; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_hi_lo = _id_ctrl_decoder_T_75 | _id_ctrl_decoder_T_80; // @[Decode.scala 15:30]
  wire  _id_ctrl_decoder_T_83 = _id_ctrl_decoder_T_43 == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_84 = ibuf_io_inst_0_bits_inst_bits & 32'h201c; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_85 = _id_ctrl_decoder_T_84 == 32'h4; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_T_87 = _id_ctrl_decoder_T_79 == 32'h10; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_hi_hi = _id_ctrl_decoder_T_83 | _id_ctrl_decoder_T_85 | _id_ctrl_decoder_T_87; // @[Decode.scala 15:30]
  wire [2:0] id_ctrl_decoder_11 = {id_ctrl_decoder_hi_hi,id_ctrl_decoder_hi_lo,id_ctrl_decoder_lo_2}; // @[Cat.scala 30:58]
  wire [31:0] _id_ctrl_decoder_T_90 = ibuf_io_inst_0_bits_inst_bits & 32'h10; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_91 = _id_ctrl_decoder_T_90 == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_92 = ibuf_io_inst_0_bits_inst_bits & 32'h8; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_93 = _id_ctrl_decoder_T_92 == 32'h0; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_12 = _id_ctrl_decoder_T_91 | _id_ctrl_decoder_T_93; // @[Decode.scala 15:30]
  wire [31:0] _id_ctrl_decoder_T_95 = ibuf_io_inst_0_bits_inst_bits & 32'h3054; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_96 = _id_ctrl_decoder_T_95 == 32'h1010; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_97 = ibuf_io_inst_0_bits_inst_bits & 32'h1058; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_98 = _id_ctrl_decoder_T_97 == 32'h1040; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_99 = ibuf_io_inst_0_bits_inst_bits & 32'h7044; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_100 = _id_ctrl_decoder_T_99 == 32'h7000; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_101 = ibuf_io_inst_0_bits_inst_bits & 32'h2001074; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_102 = _id_ctrl_decoder_T_101 == 32'h2001030; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_lo_lo = _id_ctrl_decoder_T_96 | _id_ctrl_decoder_T_98 | _id_ctrl_decoder_T_100 |
    _id_ctrl_decoder_T_102; // @[Decode.scala 15:30]
  wire [31:0] _id_ctrl_decoder_T_106 = ibuf_io_inst_0_bits_inst_bits & 32'h4054; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_107 = _id_ctrl_decoder_T_106 == 32'h40; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_108 = ibuf_io_inst_0_bits_inst_bits & 32'h2058; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_109 = _id_ctrl_decoder_T_108 == 32'h2040; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_T_111 = _id_ctrl_decoder_T_95 == 32'h3010; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_112 = ibuf_io_inst_0_bits_inst_bits & 32'h6054; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_113 = _id_ctrl_decoder_T_112 == 32'h6010; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_114 = ibuf_io_inst_0_bits_inst_bits & 32'h2002074; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_115 = _id_ctrl_decoder_T_114 == 32'h2002030; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_116 = ibuf_io_inst_0_bits_inst_bits & 32'hc0003034; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_117 = _id_ctrl_decoder_T_116 == 32'h40000030; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_118 = ibuf_io_inst_0_bits_inst_bits & 32'h40001054; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_119 = _id_ctrl_decoder_T_118 == 32'h40001010; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_lo_hi = _id_ctrl_decoder_T_107 | _id_ctrl_decoder_T_109 | _id_ctrl_decoder_T_111 |
    _id_ctrl_decoder_T_113 | _id_ctrl_decoder_T_115 | _id_ctrl_decoder_T_117 | _id_ctrl_decoder_T_119; // @[Decode.scala 15:30]
  wire [31:0] _id_ctrl_decoder_T_126 = ibuf_io_inst_0_bits_inst_bits & 32'h2002054; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_127 = _id_ctrl_decoder_T_126 == 32'h2010; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_128 = ibuf_io_inst_0_bits_inst_bits & 32'h2034; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_129 = _id_ctrl_decoder_T_128 == 32'h2010; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_130 = ibuf_io_inst_0_bits_inst_bits & 32'h40004054; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_131 = _id_ctrl_decoder_T_130 == 32'h4010; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_132 = ibuf_io_inst_0_bits_inst_bits & 32'h5054; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_133 = _id_ctrl_decoder_T_132 == 32'h4010; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_134 = ibuf_io_inst_0_bits_inst_bits & 32'h4058; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_135 = _id_ctrl_decoder_T_134 == 32'h4040; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_hi_lo_1 = _id_ctrl_decoder_T_127 | _id_ctrl_decoder_T_129 | _id_ctrl_decoder_T_131 |
    _id_ctrl_decoder_T_133 | _id_ctrl_decoder_T_135; // @[Decode.scala 15:30]
  wire [31:0] _id_ctrl_decoder_T_140 = ibuf_io_inst_0_bits_inst_bits & 32'h2006054; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_141 = _id_ctrl_decoder_T_140 == 32'h2010; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_142 = ibuf_io_inst_0_bits_inst_bits & 32'h6034; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_143 = _id_ctrl_decoder_T_142 == 32'h2010; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_144 = ibuf_io_inst_0_bits_inst_bits & 32'h40003054; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_145 = _id_ctrl_decoder_T_144 == 32'h40001010; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_hi_hi_1 = _id_ctrl_decoder_T_141 | _id_ctrl_decoder_T_143 | _id_ctrl_decoder_T_135 |
    _id_ctrl_decoder_T_117 | _id_ctrl_decoder_T_145; // @[Decode.scala 15:30]
  wire [3:0] id_ctrl_decoder_13 = {id_ctrl_decoder_hi_hi_1,id_ctrl_decoder_hi_lo_1,id_ctrl_decoder_lo_hi,
    id_ctrl_decoder_lo_lo}; // @[Cat.scala 30:58]
  wire  _id_ctrl_decoder_bit_T_517 = _id_ctrl_decoder_bit_T_27 | _id_ctrl_decoder_bit_T_29 | _id_ctrl_decoder_bit_T_31
     | _id_ctrl_decoder_bit_T_33 | _id_ctrl_decoder_bit_T_35 | _id_ctrl_decoder_bit_T_37 | _id_ctrl_decoder_bit_T_39 |
    _id_ctrl_decoder_bit_T_41 | _id_ctrl_decoder_bit_T_43 | _id_ctrl_decoder_bit_T_45 | _id_ctrl_decoder_bit_T_47 |
    _id_ctrl_decoder_bit_T_49 | _id_ctrl_decoder_bit_T_51 | _id_ctrl_decoder_bit_T_53 | _id_ctrl_decoder_bit_T_55 |
    _id_ctrl_decoder_bit_T_57 | _id_ctrl_decoder_bit_T_59 | _id_ctrl_decoder_bit_T_61 | _id_ctrl_decoder_bit_T_63 |
    _id_ctrl_decoder_bit_T_65 | _id_ctrl_decoder_bit_T_67 | _id_ctrl_decoder_bit_T_69 | _id_ctrl_decoder_bit_T_115 |
    _id_ctrl_decoder_bit_T_117 | _id_ctrl_decoder_bit_T_175 | _id_ctrl_decoder_bit_T_177 | _id_ctrl_decoder_bit_T_197 |
    _id_ctrl_decoder_bit_T_199 | _id_ctrl_decoder_bit_T_201 | _id_ctrl_decoder_bit_T_227 | _id_ctrl_decoder_bit_T_233; // @[Decode.scala 15:30]
  wire  id_ctrl_decoder_14 = _id_ctrl_decoder_bit_T_517 | _id_ctrl_decoder_bit_T_234 | _id_ctrl_decoder_bit_T_254 |
    _id_ctrl_decoder_bit_T_256 | _id_ctrl_decoder_bit_T_258 | _id_ctrl_decoder_bit_T_260 | _id_ctrl_decoder_bit_T_262 |
    _id_ctrl_decoder_bit_T_264 | _id_ctrl_decoder_bit_T_266 | _id_ctrl_decoder_bit_T_268; // @[Decode.scala 15:30]
  wire  _id_ctrl_decoder_T_151 = _id_ctrl_decoder_T_7 == 32'h20; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_152 = ibuf_io_inst_0_bits_inst_bits & 32'h18000020; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_153 = _id_ctrl_decoder_T_152 == 32'h18000020; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_154 = ibuf_io_inst_0_bits_inst_bits & 32'h20000020; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_155 = _id_ctrl_decoder_T_154 == 32'h20000020; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_lo_lo_1 = _id_ctrl_decoder_T_151 | _id_ctrl_decoder_T_153 | _id_ctrl_decoder_T_155; // @[Decode.scala 15:30]
  wire [31:0] _id_ctrl_decoder_T_158 = ibuf_io_inst_0_bits_inst_bits & 32'h10000008; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_159 = _id_ctrl_decoder_T_158 == 32'h10000008; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_160 = ibuf_io_inst_0_bits_inst_bits & 32'h40000008; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_161 = _id_ctrl_decoder_T_160 == 32'h40000008; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_lo_hi_1 = _id_ctrl_decoder_T_159 | _id_ctrl_decoder_T_161; // @[Decode.scala 15:30]
  wire [31:0] _id_ctrl_decoder_T_163 = ibuf_io_inst_0_bits_inst_bits & 32'h40; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_164 = _id_ctrl_decoder_T_163 == 32'h40; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_165 = ibuf_io_inst_0_bits_inst_bits & 32'h8000008; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_166 = _id_ctrl_decoder_T_165 == 32'h8000008; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_167 = ibuf_io_inst_0_bits_inst_bits & 32'h80000008; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_168 = _id_ctrl_decoder_T_167 == 32'h80000008; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_hi_lo_2 = _id_ctrl_decoder_T_164 | _id_ctrl_decoder_T_166 | _id_ctrl_decoder_T_159 |
    _id_ctrl_decoder_T_168; // @[Decode.scala 15:30]
  wire [31:0] _id_ctrl_decoder_T_172 = ibuf_io_inst_0_bits_inst_bits & 32'h18000008; // @[Decode.scala 14:65]
  wire  id_ctrl_decoder_hi_hi_lo = _id_ctrl_decoder_T_172 == 32'h8; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_174 = ibuf_io_inst_0_bits_inst_bits & 32'h80000040; // @[Decode.scala 14:65]
  wire  id_ctrl_decoder_hi_hi_hi = _id_ctrl_decoder_T_174 == 32'h40; // @[Decode.scala 14:121]
  wire [4:0] id_ctrl_decoder_15 = {id_ctrl_decoder_hi_hi_hi,id_ctrl_decoder_hi_hi_lo,id_ctrl_decoder_hi_lo_2,
    id_ctrl_decoder_lo_hi_1,id_ctrl_decoder_lo_lo_1}; // @[Cat.scala 30:58]
  wire [31:0] _id_ctrl_decoder_T_176 = ibuf_io_inst_0_bits_inst_bits & 32'h80000060; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_177 = _id_ctrl_decoder_T_176 == 32'h40; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_178 = ibuf_io_inst_0_bits_inst_bits & 32'h10000060; // @[Decode.scala 14:65]
  wire [31:0] _id_ctrl_decoder_T_180 = ibuf_io_inst_0_bits_inst_bits & 32'h70; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_181 = _id_ctrl_decoder_T_180 == 32'h40; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_193 = ibuf_io_inst_0_bits_inst_bits & 32'h3c; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_194 = _id_ctrl_decoder_T_193 == 32'h4; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_T_196 = _id_ctrl_decoder_T_178 == 32'h10000040; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_19 = _id_ctrl_decoder_T_194 | _id_ctrl_decoder_T_177 | _id_ctrl_decoder_T_181 |
    _id_ctrl_decoder_T_196; // @[Decode.scala 15:30]
  wire [31:0] _id_ctrl_decoder_T_200 = ibuf_io_inst_0_bits_inst_bits & 32'h2004074; // @[Decode.scala 14:65]
  wire  id_ctrl_decoder_20 = _id_ctrl_decoder_T_200 == 32'h2000030; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_21 = _id_ctrl_decoder_T_200 == 32'h2004030; // @[Decode.scala 14:121]
  wire  _id_ctrl_decoder_T_205 = _id_ctrl_decoder_T_13 == 32'h0; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_206 = ibuf_io_inst_0_bits_inst_bits & 32'h4c; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_207 = _id_ctrl_decoder_T_206 == 32'h8; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_208 = ibuf_io_inst_0_bits_inst_bits & 32'h2024; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_209 = _id_ctrl_decoder_T_208 == 32'h24; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_210 = ibuf_io_inst_0_bits_inst_bits & 32'h28; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_211 = _id_ctrl_decoder_T_210 == 32'h28; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_212 = ibuf_io_inst_0_bits_inst_bits & 32'h1030; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_213 = _id_ctrl_decoder_T_212 == 32'h1030; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_214 = ibuf_io_inst_0_bits_inst_bits & 32'h2030; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_215 = _id_ctrl_decoder_T_214 == 32'h2030; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_216 = ibuf_io_inst_0_bits_inst_bits & 32'h90000010; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_217 = _id_ctrl_decoder_T_216 == 32'h80000010; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_22 = _id_ctrl_decoder_T_205 | _id_ctrl_decoder_T_207 | _id_ctrl_decoder_T_56 |
    _id_ctrl_decoder_T_209 | _id_ctrl_decoder_T_211 | _id_ctrl_decoder_T_213 | _id_ctrl_decoder_T_215 |
    _id_ctrl_decoder_T_217; // @[Decode.scala 15:30]
  wire [31:0] _id_ctrl_decoder_T_225 = ibuf_io_inst_0_bits_inst_bits & 32'h1070; // @[Decode.scala 14:65]
  wire  id_ctrl_decoder_lo_5 = _id_ctrl_decoder_T_225 == 32'h1070; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_227 = ibuf_io_inst_0_bits_inst_bits & 32'h2070; // @[Decode.scala 14:65]
  wire  id_ctrl_decoder_hi_lo_3 = _id_ctrl_decoder_T_227 == 32'h2070; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_229 = ibuf_io_inst_0_bits_inst_bits & 32'h10000070; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_230 = _id_ctrl_decoder_T_229 == 32'h70; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_231 = ibuf_io_inst_0_bits_inst_bits & 32'h12000034; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_232 = _id_ctrl_decoder_T_231 == 32'h10000030; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_233 = ibuf_io_inst_0_bits_inst_bits & 32'hb0000050; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_234 = _id_ctrl_decoder_T_233 == 32'h30000050; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_hi_hi_3 = _id_ctrl_decoder_T_230 | id_ctrl_decoder_lo_5 | id_ctrl_decoder_hi_lo_3 |
    _id_ctrl_decoder_T_232 | _id_ctrl_decoder_T_234; // @[Decode.scala 15:30]
  wire [2:0] id_ctrl_decoder_23 = {id_ctrl_decoder_hi_hi_3,id_ctrl_decoder_hi_lo_3,id_ctrl_decoder_lo_5}; // @[Cat.scala 30:58]
  wire  id_ctrl_decoder_24 = _id_ctrl_decoder_T_95 == 32'h1004; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_241 = ibuf_io_inst_0_bits_inst_bits & 32'h2054; // @[Decode.scala 14:65]
  wire  id_ctrl_decoder_25 = _id_ctrl_decoder_T_241 == 32'h4; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_243 = ibuf_io_inst_0_bits_inst_bits & 32'h204c; // @[Decode.scala 14:65]
  wire  id_ctrl_decoder_26 = _id_ctrl_decoder_T_243 == 32'h200c; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_245 = ibuf_io_inst_0_bits_inst_bits & 32'h105c; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_246 = _id_ctrl_decoder_T_245 == 32'h1004; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_247 = ibuf_io_inst_0_bits_inst_bits & 32'h2000060; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_248 = _id_ctrl_decoder_T_247 == 32'h2000040; // @[Decode.scala 14:121]
  wire [31:0] _id_ctrl_decoder_T_249 = ibuf_io_inst_0_bits_inst_bits & 32'hd0000070; // @[Decode.scala 14:65]
  wire  _id_ctrl_decoder_T_250 = _id_ctrl_decoder_T_249 == 32'h40000050; // @[Decode.scala 14:121]
  wire  id_ctrl_decoder_27 = _id_ctrl_decoder_T_246 | _id_ctrl_decoder_T_248 | _id_ctrl_decoder_T_250; // @[Decode.scala 15:30]
  wire [4:0] id_raddr3 = ibuf_io_inst_0_bits_inst_rs3; // @[RocketCore.scala 268:72]
  wire [4:0] id_raddr2 = ibuf_io_inst_0_bits_inst_rs2; // @[RocketCore.scala 268:72]
  wire [4:0] id_raddr1 = ibuf_io_inst_0_bits_inst_rs1; // @[RocketCore.scala 268:72]
  wire [4:0] id_waddr = ibuf_io_inst_0_bits_inst_rd; // @[RocketCore.scala 268:72]
  reg  id_reg_fence; // @[RocketCore.scala 275:25]
  wire [63:0] _id_rs_T_4 = rf_id_rs_MPORT_data; // @[RocketCore.scala 1024:25]
  wire [63:0] _id_rs_T_9 = rf_id_rs_MPORT_1_data; // @[RocketCore.scala 1024:25]
  wire  _id_csr_en_T = id_ctrl_decoder_23 == 3'h6; // @[package.scala 15:47]
  wire  _id_csr_en_T_1 = id_ctrl_decoder_23 == 3'h7; // @[package.scala 15:47]
  wire  _id_csr_en_T_2 = id_ctrl_decoder_23 == 3'h5; // @[package.scala 15:47]
  wire  _id_csr_en_T_3 = _id_csr_en_T | _id_csr_en_T_1; // @[package.scala 72:59]
  wire  id_csr_en = _id_csr_en_T | _id_csr_en_T_1 | _id_csr_en_T_2; // @[package.scala 72:59]
  wire  id_system_insn = id_ctrl_decoder_23 == 3'h4; // @[RocketCore.scala 285:36]
  wire  id_csr_ren = _id_csr_en_T_3 & ibuf_io_inst_0_bits_inst_rs1 == 5'h0; // @[RocketCore.scala 286:54]
  wire  _id_sfence_T = id_ctrl_decoder_15 == 5'h14; // @[RocketCore.scala 288:50]
  wire  id_sfence = id_ctrl_decoder_14 & id_ctrl_decoder_15 == 5'h14; // @[RocketCore.scala 288:31]
  wire  _id_csr_flush_T = id_sfence | id_system_insn; // @[RocketCore.scala 289:32]
  wire  _id_csr_flush_T_1 = ~id_csr_ren; // @[RocketCore.scala 289:67]
  wire  id_csr_flush = id_sfence | id_system_insn | id_csr_en & ~id_csr_ren & csr_io_decode_0_write_flush; // @[RocketCore.scala 289:50]
  wire  _id_illegal_insn_T_4 = (id_ctrl_decoder_20 | id_ctrl_decoder_21) & ~csr_io_status_isa[12]; // @[RocketCore.scala 298:34]
  wire  _id_illegal_insn_T_5 = ~id_ctrl_decoder_0 | _id_illegal_insn_T_4; // @[RocketCore.scala 297:40]
  wire  _id_illegal_insn_T_8 = id_ctrl_decoder_26 & ~csr_io_status_isa[0]; // @[RocketCore.scala 299:17]
  wire  _id_illegal_insn_T_9 = _id_illegal_insn_T_5 | _id_illegal_insn_T_8; // @[RocketCore.scala 298:65]
  wire  _id_illegal_insn_T_11 = id_ctrl_decoder_1 & (csr_io_decode_0_fp_illegal | io_fpu_illegal_rm); // @[RocketCore.scala 300:16]
  wire  _id_illegal_insn_T_12 = _id_illegal_insn_T_9 | _id_illegal_insn_T_11; // @[RocketCore.scala 299:48]
  wire  _id_illegal_insn_T_15 = id_ctrl_decoder_27 & ~csr_io_status_isa[3]; // @[RocketCore.scala 301:16]
  wire  _id_illegal_insn_T_16 = _id_illegal_insn_T_12 | _id_illegal_insn_T_15; // @[RocketCore.scala 300:70]
  wire  _id_illegal_insn_T_18 = ~csr_io_status_isa[2]; // @[RocketCore.scala 302:33]
  wire  _id_illegal_insn_T_19 = ibuf_io_inst_0_bits_rvc & ~csr_io_status_isa[2]; // @[RocketCore.scala 302:30]
  wire  _id_illegal_insn_T_20 = _id_illegal_insn_T_16 | _id_illegal_insn_T_19; // @[RocketCore.scala 301:47]
  wire  _id_illegal_insn_T_37 = id_ctrl_decoder_8 & ~(d_unpipelined | d_pipelined); // @[RocketCore.scala 307:18]
  wire  _id_illegal_insn_T_38 = _id_illegal_insn_T_20 | _id_illegal_insn_T_37; // @[RocketCore.scala 306:51]
  wire  _id_illegal_insn_T_42 = id_csr_en & (csr_io_decode_0_read_illegal | _id_csr_flush_T_1 &
    csr_io_decode_0_write_illegal); // @[RocketCore.scala 308:15]
  wire  _id_illegal_insn_T_43 = _id_illegal_insn_T_38 | _id_illegal_insn_T_42; // @[RocketCore.scala 307:81]
  wire  _id_illegal_insn_T_47 = ~ibuf_io_inst_0_bits_rvc & (_id_csr_flush_T & csr_io_decode_0_system_illegal); // @[RocketCore.scala 309:31]
  wire  id_illegal_insn = _id_illegal_insn_T_43 | _id_illegal_insn_T_47; // @[RocketCore.scala 308:99]
  wire  id_amo_aq = ibuf_io_inst_0_bits_inst_bits[26]; // @[RocketCore.scala 311:29]
  wire  id_amo_rl = ibuf_io_inst_0_bits_inst_bits[25]; // @[RocketCore.scala 312:29]
  wire [3:0] id_fence_succ = ibuf_io_inst_0_bits_inst_bits[23:20]; // @[RocketCore.scala 314:33]
  wire  id_fence_next = id_ctrl_decoder_25 | id_ctrl_decoder_26 & id_amo_aq; // @[RocketCore.scala 315:37]
  wire  id_mem_busy = ~io_dmem_ordered | io_dmem_req_valid; // @[RocketCore.scala 316:38]
  wire  _GEN_0 = ~id_mem_busy ? 1'h0 : id_reg_fence; // @[RocketCore.scala 317:23 RocketCore.scala 317:38 RocketCore.scala 275:25]
  wire  id_do_fence_x8 = id_mem_busy & (id_ctrl_decoder_26 & id_amo_rl | id_ctrl_decoder_24 | id_reg_fence &
    id_ctrl_decoder_14); // @[RocketCore.scala 322:17]
  wire  _id_do_fence_x8_T_6 = id_do_fence_x8; // @[RocketCore.scala 322:17]
  wire  id_xcpt = csr_io_interrupt | bpu_io_debug_if | bpu_io_xcpt_if | ibuf_io_inst_0_bits_xcpt0_pf_inst |
    ibuf_io_inst_0_bits_xcpt0_ae_inst | ibuf_io_inst_0_bits_xcpt1_pf_inst | ibuf_io_inst_0_bits_xcpt1_ae_inst |
    id_illegal_insn; // @[RocketCore.scala 977:26]
  wire [1:0] _T_7 = ibuf_io_inst_0_bits_xcpt1_ae_inst ? 2'h1 : 2'h2; // @[Mux.scala 47:69]
  wire [3:0] _T_8 = ibuf_io_inst_0_bits_xcpt1_pf_inst ? 4'hc : {{2'd0}, _T_7}; // @[Mux.scala 47:69]
  wire [3:0] _T_9 = ibuf_io_inst_0_bits_xcpt0_ae_inst ? 4'h1 : _T_8; // @[Mux.scala 47:69]
  wire [3:0] _T_10 = ibuf_io_inst_0_bits_xcpt0_pf_inst ? 4'hc : _T_9; // @[Mux.scala 47:69]
  wire [3:0] _T_11 = bpu_io_xcpt_if ? 4'h3 : _T_10; // @[Mux.scala 47:69]
  wire [3:0] _T_12 = bpu_io_debug_if ? 4'he : _T_11; // @[Mux.scala 47:69]
  wire [63:0] id_cause = csr_io_interrupt ? csr_io_interrupt_cause : {{60'd0}, _T_12}; // @[Mux.scala 47:69]
  wire [4:0] ex_waddr = ex_reg_inst[11:7]; // @[RocketCore.scala 360:29]
  wire [4:0] mem_waddr = mem_reg_inst[11:7]; // @[RocketCore.scala 361:31]
  wire [4:0] wb_waddr = wb_reg_inst[11:7]; // @[RocketCore.scala 362:29]
  wire  _T_33 = ex_reg_valid & ex_ctrl_wxd; // @[RocketCore.scala 365:19]
  wire  _T_34 = mem_reg_valid & mem_ctrl_wxd; // @[RocketCore.scala 366:20]
  wire  _T_36 = mem_reg_valid & mem_ctrl_wxd & ~mem_ctrl_mem; // @[RocketCore.scala 366:36]
  wire  id_bypass_src_0_0 = 5'h0 == id_raddr1; // @[RocketCore.scala 368:82]
  wire  id_bypass_src_0_1 = _T_33 & ex_waddr == id_raddr1; // @[RocketCore.scala 368:74]
  wire  id_bypass_src_0_2 = _T_36 & mem_waddr == id_raddr1; // @[RocketCore.scala 368:74]
  wire  id_bypass_src_0_3 = _T_34 & mem_waddr == id_raddr1; // @[RocketCore.scala 368:74]
  wire  id_bypass_src_1_0 = 5'h0 == id_raddr2; // @[RocketCore.scala 368:82]
  wire  id_bypass_src_1_1 = _T_33 & ex_waddr == id_raddr2; // @[RocketCore.scala 368:74]
  wire  id_bypass_src_1_2 = _T_36 & mem_waddr == id_raddr2; // @[RocketCore.scala 368:74]
  wire  id_bypass_src_1_3 = _T_34 & mem_waddr == id_raddr2; // @[RocketCore.scala 368:74]
  reg  ex_reg_rs_bypass_0; // @[RocketCore.scala 372:29]
  reg  ex_reg_rs_bypass_1; // @[RocketCore.scala 372:29]
  reg [1:0] ex_reg_rs_lsb_0; // @[RocketCore.scala 373:26]
  reg [1:0] ex_reg_rs_lsb_1; // @[RocketCore.scala 373:26]
  reg [61:0] ex_reg_rs_msb_0; // @[RocketCore.scala 374:26]
  reg [61:0] ex_reg_rs_msb_1; // @[RocketCore.scala 374:26]
  wire [63:0] _ex_rs_T_1 = ex_reg_rs_lsb_0 == 2'h1 ? mem_reg_wdata : 64'h0; // @[package.scala 32:76]
  wire [63:0] _ex_rs_T_3 = ex_reg_rs_lsb_0 == 2'h2 ? wb_reg_wdata : _ex_rs_T_1; // @[package.scala 32:76]
  wire [63:0] _ex_rs_T_5 = ex_reg_rs_lsb_0 == 2'h3 ? io_dmem_resp_bits_data_word_bypass : _ex_rs_T_3; // @[package.scala 32:76]
  wire [63:0] _ex_rs_T_6 = {ex_reg_rs_msb_0,ex_reg_rs_lsb_0}; // @[Cat.scala 30:58]
  wire [63:0] _ex_rs_T_8 = ex_reg_rs_lsb_1 == 2'h1 ? mem_reg_wdata : 64'h0; // @[package.scala 32:76]
  wire [63:0] _ex_rs_T_10 = ex_reg_rs_lsb_1 == 2'h2 ? wb_reg_wdata : _ex_rs_T_8; // @[package.scala 32:76]
  wire [63:0] _ex_rs_T_12 = ex_reg_rs_lsb_1 == 2'h3 ? io_dmem_resp_bits_data_word_bypass : _ex_rs_T_10; // @[package.scala 32:76]
  wire [63:0] _ex_rs_T_13 = {ex_reg_rs_msb_1,ex_reg_rs_lsb_1}; // @[Cat.scala 30:58]
  wire [63:0] ex_rs_1 = ex_reg_rs_bypass_1 ? _ex_rs_T_12 : _ex_rs_T_13; // @[RocketCore.scala 376:14]
  wire  _ex_imm_sign_T = ex_ctrl_sel_imm == 3'h5; // @[RocketCore.scala 1039:24]
  wire  _ex_imm_sign_T_2 = ex_reg_inst[31]; // @[RocketCore.scala 1039:53]
  wire  ex_imm_sign = ex_ctrl_sel_imm == 3'h5 ? $signed(1'sh0) : $signed(_ex_imm_sign_T_2); // @[RocketCore.scala 1039:19]
  wire  _ex_imm_b30_20_T = ex_ctrl_sel_imm == 3'h2; // @[RocketCore.scala 1040:26]
  wire [10:0] _ex_imm_b30_20_T_2 = ex_reg_inst[30:20]; // @[RocketCore.scala 1040:49]
  wire [7:0] _ex_imm_b19_12_T_4 = ex_reg_inst[19:12]; // @[RocketCore.scala 1041:73]
  wire  _ex_imm_b11_T_2 = _ex_imm_b30_20_T | _ex_imm_sign_T; // @[RocketCore.scala 1042:33]
  wire  _ex_imm_b11_T_5 = ex_reg_inst[20]; // @[RocketCore.scala 1043:44]
  wire  _ex_imm_b11_T_6 = ex_ctrl_sel_imm == 3'h1; // @[RocketCore.scala 1044:23]
  wire  _ex_imm_b11_T_8 = ex_reg_inst[7]; // @[RocketCore.scala 1044:43]
  wire  _ex_imm_b11_T_9 = ex_ctrl_sel_imm == 3'h1 ? $signed(_ex_imm_b11_T_8) : $signed(ex_imm_sign); // @[RocketCore.scala 1044:18]
  wire  _ex_imm_b11_T_10 = ex_ctrl_sel_imm == 3'h3 ? $signed(_ex_imm_b11_T_5) : $signed(_ex_imm_b11_T_9); // @[RocketCore.scala 1043:18]
  wire [5:0] ex_imm_lo_hi_hi = _ex_imm_b11_T_2 ? 6'h0 : ex_reg_inst[30:25]; // @[RocketCore.scala 1045:20]
  wire  _ex_imm_b4_1_T_1 = ex_ctrl_sel_imm == 3'h0; // @[RocketCore.scala 1047:24]
  wire [3:0] _ex_imm_b4_1_T_8 = _ex_imm_sign_T ? ex_reg_inst[19:16] : ex_reg_inst[24:21]; // @[RocketCore.scala 1048:19]
  wire [3:0] _ex_imm_b4_1_T_9 = ex_ctrl_sel_imm == 3'h0 | _ex_imm_b11_T_6 ? ex_reg_inst[11:8] : _ex_imm_b4_1_T_8; // @[RocketCore.scala 1047:19]
  wire [3:0] ex_imm_lo_hi_lo = _ex_imm_b30_20_T ? 4'h0 : _ex_imm_b4_1_T_9; // @[RocketCore.scala 1046:19]
  wire  _ex_imm_b0_T_6 = _ex_imm_sign_T & ex_reg_inst[15]; // @[RocketCore.scala 1051:17]
  wire  _ex_imm_b0_T_7 = ex_ctrl_sel_imm == 3'h4 ? ex_reg_inst[20] : _ex_imm_b0_T_6; // @[RocketCore.scala 1050:17]
  wire  ex_imm_lo_lo = _ex_imm_b4_1_T_1 ? ex_reg_inst[7] : _ex_imm_b0_T_7; // @[RocketCore.scala 1049:17]
  wire  ex_imm_hi_lo_lo = _ex_imm_b30_20_T | _ex_imm_sign_T ? $signed(1'sh0) : $signed(_ex_imm_b11_T_10); // @[Cat.scala 30:58]
  wire [7:0] ex_imm_hi_lo_hi = ex_ctrl_sel_imm != 3'h2 & ex_ctrl_sel_imm != 3'h3 ? $signed({8{ex_imm_sign}}) : $signed(
    _ex_imm_b19_12_T_4); // @[Cat.scala 30:58]
  wire [10:0] ex_imm_hi_hi_lo = ex_ctrl_sel_imm == 3'h2 ? $signed(_ex_imm_b30_20_T_2) : $signed({11{ex_imm_sign}}); // @[Cat.scala 30:58]
  wire  ex_imm_hi_hi_hi = ex_ctrl_sel_imm == 3'h5 ? $signed(1'sh0) : $signed(_ex_imm_sign_T_2); // @[Cat.scala 30:58]
  wire [31:0] ex_imm = {ex_imm_hi_hi_hi,ex_imm_hi_hi_lo,ex_imm_hi_lo_hi,ex_imm_hi_lo_lo,ex_imm_lo_hi_hi,ex_imm_lo_hi_lo,
    ex_imm_lo_lo}; // @[RocketCore.scala 1053:53]
  wire [63:0] _ex_op1_T = ex_reg_rs_bypass_0 ? _ex_rs_T_5 : _ex_rs_T_6; // @[RocketCore.scala 379:24]
  wire [39:0] _ex_op1_T_1 = ex_reg_pc; // @[RocketCore.scala 380:24]
  wire [63:0] _ex_op1_T_3 = 2'h1 == ex_ctrl_sel_alu1 ? $signed(_ex_op1_T) : $signed(64'sh0); // @[Mux.scala 80:57]
  wire [63:0] _ex_op2_T = ex_reg_rs_bypass_1 ? _ex_rs_T_12 : _ex_rs_T_13; // @[RocketCore.scala 382:24]
  wire [3:0] _ex_op2_T_1 = ex_reg_rvc ? $signed(4'sh2) : $signed(4'sh4); // @[RocketCore.scala 384:19]
  wire [63:0] _ex_op2_T_3 = 2'h2 == ex_ctrl_sel_alu2 ? $signed(_ex_op2_T) : $signed(64'sh0); // @[Mux.scala 80:57]
  wire [63:0] _ex_op2_T_5 = 2'h3 == ex_ctrl_sel_alu2 ? $signed({{32{ex_imm[31]}},ex_imm}) : $signed(_ex_op2_T_3); // @[Mux.scala 80:57]
  wire  _T_227 = id_raddr1 != 5'h0; // @[RocketCore.scala 715:55]
  wire  _T_228 = id_ctrl_decoder_7 & id_raddr1 != 5'h0; // @[RocketCore.scala 715:42]
  wire  _data_hazard_ex_T = id_raddr1 == ex_waddr; // @[RocketCore.scala 735:70]
  wire  _T_229 = id_raddr2 != 5'h0; // @[RocketCore.scala 716:55]
  wire  _T_230 = id_ctrl_decoder_6 & id_raddr2 != 5'h0; // @[RocketCore.scala 716:42]
  wire  _data_hazard_ex_T_2 = id_raddr2 == ex_waddr; // @[RocketCore.scala 735:70]
  wire  _T_232 = id_ctrl_decoder_22 & id_waddr != 5'h0; // @[RocketCore.scala 717:42]
  wire  _data_hazard_ex_T_4 = id_waddr == ex_waddr; // @[RocketCore.scala 735:70]
  wire  _data_hazard_ex_T_7 = _T_228 & _data_hazard_ex_T | _T_230 & _data_hazard_ex_T_2 | _T_232 & _data_hazard_ex_T_4; // @[RocketCore.scala 986:50]
  wire  data_hazard_ex = ex_ctrl_wxd & _data_hazard_ex_T_7; // @[RocketCore.scala 735:36]
  wire  _ex_cannot_bypass_T = ex_ctrl_csr != 3'h0; // @[RocketCore.scala 734:38]
  wire  ex_cannot_bypass = ex_ctrl_csr != 3'h0 | ex_ctrl_jalr | ex_ctrl_mem | ex_ctrl_mul | ex_ctrl_div | ex_ctrl_fp |
    ex_scie_pipelined; // @[RocketCore.scala 734:139]
  wire  _fp_data_hazard_ex_T_4 = id_raddr3 == ex_waddr; // @[RocketCore.scala 736:76]
  wire  _fp_data_hazard_ex_T_10 = io_fpu_dec_ren1 & _data_hazard_ex_T | io_fpu_dec_ren2 & _data_hazard_ex_T_2 |
    io_fpu_dec_ren3 & _fp_data_hazard_ex_T_4 | io_fpu_dec_wen & _data_hazard_ex_T_4; // @[RocketCore.scala 986:50]
  wire  fp_data_hazard_ex = ex_ctrl_wfd & _fp_data_hazard_ex_T_10; // @[RocketCore.scala 736:39]
  wire  id_ex_hazard = ex_reg_valid & (data_hazard_ex & ex_cannot_bypass | fp_data_hazard_ex); // @[RocketCore.scala 737:35]
  wire  _data_hazard_mem_T = id_raddr1 == mem_waddr; // @[RocketCore.scala 744:72]
  wire  _data_hazard_mem_T_2 = id_raddr2 == mem_waddr; // @[RocketCore.scala 744:72]
  wire  _data_hazard_mem_T_4 = id_waddr == mem_waddr; // @[RocketCore.scala 744:72]
  wire  _data_hazard_mem_T_7 = _T_228 & _data_hazard_mem_T | _T_230 & _data_hazard_mem_T_2 | _T_232 &
    _data_hazard_mem_T_4; // @[RocketCore.scala 986:50]
  wire  data_hazard_mem = mem_ctrl_wxd & _data_hazard_mem_T_7; // @[RocketCore.scala 744:38]
  wire  _mem_cannot_bypass_T = mem_ctrl_csr != 3'h0; // @[RocketCore.scala 743:40]
  wire  mem_cannot_bypass = mem_ctrl_csr != 3'h0 | mem_ctrl_mem & mem_reg_slow_bypass | mem_ctrl_mul | mem_ctrl_div |
    mem_ctrl_fp; // @[RocketCore.scala 743:116]
  wire  _fp_data_hazard_mem_T_4 = id_raddr3 == mem_waddr; // @[RocketCore.scala 745:78]
  wire  _fp_data_hazard_mem_T_10 = io_fpu_dec_ren1 & _data_hazard_mem_T | io_fpu_dec_ren2 & _data_hazard_mem_T_2 |
    io_fpu_dec_ren3 & _fp_data_hazard_mem_T_4 | io_fpu_dec_wen & _data_hazard_mem_T_4; // @[RocketCore.scala 986:50]
  wire  fp_data_hazard_mem = mem_ctrl_wfd & _fp_data_hazard_mem_T_10; // @[RocketCore.scala 745:41]
  wire  id_mem_hazard = mem_reg_valid & (data_hazard_mem & mem_cannot_bypass | fp_data_hazard_mem); // @[RocketCore.scala 746:37]
  wire  _data_hazard_wb_T = id_raddr1 == wb_waddr; // @[RocketCore.scala 750:70]
  wire  _data_hazard_wb_T_2 = id_raddr2 == wb_waddr; // @[RocketCore.scala 750:70]
  wire  _data_hazard_wb_T_4 = id_waddr == wb_waddr; // @[RocketCore.scala 750:70]
  wire  _data_hazard_wb_T_7 = _T_228 & _data_hazard_wb_T | _T_230 & _data_hazard_wb_T_2 | _T_232 & _data_hazard_wb_T_4; // @[RocketCore.scala 986:50]
  wire  data_hazard_wb = wb_ctrl_wxd & _data_hazard_wb_T_7; // @[RocketCore.scala 750:36]
  wire  wb_dcache_miss = wb_ctrl_mem & ~io_dmem_resp_valid; // @[RocketCore.scala 492:36]
  wire  wb_set_sboard = wb_ctrl_div | wb_dcache_miss; // @[RocketCore.scala 637:35]
  wire  _fp_data_hazard_wb_T_4 = id_raddr3 == wb_waddr; // @[RocketCore.scala 751:76]
  wire  _fp_data_hazard_wb_T_10 = io_fpu_dec_ren1 & _data_hazard_wb_T | io_fpu_dec_ren2 & _data_hazard_wb_T_2 |
    io_fpu_dec_ren3 & _fp_data_hazard_wb_T_4 | io_fpu_dec_wen & _data_hazard_wb_T_4; // @[RocketCore.scala 986:50]
  wire  fp_data_hazard_wb = wb_ctrl_wfd & _fp_data_hazard_wb_T_10; // @[RocketCore.scala 751:39]
  wire  id_wb_hazard = wb_reg_valid & (data_hazard_wb & wb_set_sboard | fp_data_hazard_wb); // @[RocketCore.scala 752:35]
  reg [31:0] state; // @[RocketCore.scala 1003:28]
  wire [31:0] r = {state[31:1], 1'h0}; // @[RocketCore.scala 1004:43]
  wire [31:0] _id_sboard_hazard_T = r >> id_raddr1; // @[RocketCore.scala 1000:35]
  wire  dmem_resp_valid = io_dmem_resp_valid & io_dmem_resp_bits_has_data; // @[RocketCore.scala 647:44]
  wire  dmem_resp_replay = dmem_resp_valid & io_dmem_resp_bits_replay; // @[RocketCore.scala 648:42]
  wire  dmem_resp_xpu = ~io_dmem_resp_bits_tag[0]; // @[RocketCore.scala 644:23]
  wire  ll_wen_x1 = div_io_resp_ready & div_io_resp_valid; // @[Decoupled.scala 40:37]
  wire  ll_wen = dmem_resp_replay & dmem_resp_xpu | ll_wen_x1; // @[RocketCore.scala 663:44 RocketCore.scala 668:12]
  wire [4:0] dmem_resp_waddr = io_dmem_resp_bits_tag[5:1]; // @[RocketCore.scala 646:46]
  wire [4:0] ll_waddr = dmem_resp_replay & dmem_resp_xpu ? dmem_resp_waddr : div_io_resp_bits_tag; // @[RocketCore.scala 663:44 RocketCore.scala 667:14]
  wire  _id_sboard_hazard_T_3 = ll_wen & ll_waddr == id_raddr1; // @[RocketCore.scala 727:58]
  wire  _id_sboard_hazard_T_5 = _id_sboard_hazard_T[0] & ~_id_sboard_hazard_T_3; // @[RocketCore.scala 730:77]
  wire [31:0] _id_sboard_hazard_T_7 = r >> id_raddr2; // @[RocketCore.scala 1000:35]
  wire  _id_sboard_hazard_T_10 = ll_wen & ll_waddr == id_raddr2; // @[RocketCore.scala 727:58]
  wire  _id_sboard_hazard_T_12 = _id_sboard_hazard_T_7[0] & ~_id_sboard_hazard_T_10; // @[RocketCore.scala 730:77]
  wire [31:0] _id_sboard_hazard_T_14 = r >> id_waddr; // @[RocketCore.scala 1000:35]
  wire  _id_sboard_hazard_T_17 = ll_wen & ll_waddr == id_waddr; // @[RocketCore.scala 727:58]
  wire  _id_sboard_hazard_T_19 = _id_sboard_hazard_T_14[0] & ~_id_sboard_hazard_T_17; // @[RocketCore.scala 730:77]
  wire  id_sboard_hazard = _T_228 & _id_sboard_hazard_T_5 | _T_230 & _id_sboard_hazard_T_12 | _T_232 &
    _id_sboard_hazard_T_19; // @[RocketCore.scala 986:50]
  wire  _ctrl_stalld_T_5 = csr_io_singleStep & (ex_reg_valid | mem_reg_valid | wb_reg_valid); // @[RocketCore.scala 774:23]
  wire  _ctrl_stalld_T_6 = id_ex_hazard | id_mem_hazard | id_wb_hazard | id_sboard_hazard | _ctrl_stalld_T_5; // @[RocketCore.scala 773:71]
  wire  _ctrl_stalld_T_8 = ~io_fpu_fcsr_rdy; // @[RocketCore.scala 775:45]
  wire  _ctrl_stalld_T_9 = id_csr_en & csr_io_decode_0_fp_csr & ~io_fpu_fcsr_rdy; // @[RocketCore.scala 775:42]
  wire  _ctrl_stalld_T_10 = _ctrl_stalld_T_6 | _ctrl_stalld_T_9; // @[RocketCore.scala 774:74]
  reg [31:0] id_stall_fpu_state; // @[RocketCore.scala 1003:28]
  wire [31:0] _id_stall_fpu_T_18 = id_stall_fpu_state >> id_raddr1; // @[RocketCore.scala 1000:35]
  wire [31:0] _id_stall_fpu_T_21 = id_stall_fpu_state >> id_raddr2; // @[RocketCore.scala 1000:35]
  wire [31:0] _id_stall_fpu_T_24 = id_stall_fpu_state >> id_raddr3; // @[RocketCore.scala 1000:35]
  wire [31:0] _id_stall_fpu_T_27 = id_stall_fpu_state >> id_waddr; // @[RocketCore.scala 1000:35]
  wire  id_stall_fpu = io_fpu_dec_ren1 & _id_stall_fpu_T_18[0] | io_fpu_dec_ren2 & _id_stall_fpu_T_21[0] |
    io_fpu_dec_ren3 & _id_stall_fpu_T_24[0] | io_fpu_dec_wen & _id_stall_fpu_T_27[0]; // @[RocketCore.scala 986:50]
  wire  _ctrl_stalld_T_11 = id_ctrl_decoder_1 & id_stall_fpu; // @[RocketCore.scala 776:16]
  wire  _ctrl_stalld_T_12 = _ctrl_stalld_T_10 | _ctrl_stalld_T_11; // @[RocketCore.scala 775:62]
  reg  blocked; // @[RocketCore.scala 765:22]
  wire  _dcache_blocked_T = ~io_dmem_perf_grant; // @[RocketCore.scala 767:16]
  wire  dcache_blocked = blocked & ~io_dmem_perf_grant; // @[RocketCore.scala 767:13]
  wire  _ctrl_stalld_T_13 = id_ctrl_decoder_14 & dcache_blocked; // @[RocketCore.scala 777:17]
  wire  _ctrl_stalld_T_14 = _ctrl_stalld_T_12 | _ctrl_stalld_T_13; // @[RocketCore.scala 776:32]
  wire  wb_wxd = wb_reg_valid & wb_ctrl_wxd; // @[RocketCore.scala 636:29]
  wire  _ctrl_stalld_T_17 = ~wb_wxd; // @[RocketCore.scala 779:65]
  wire  _ctrl_stalld_T_22 = id_ctrl_decoder_21 & (~(div_io_req_ready | div_io_resp_valid & ~wb_wxd) | div_io_req_valid); // @[RocketCore.scala 779:17]
  wire  _ctrl_stalld_T_23 = _ctrl_stalld_T_14 | _ctrl_stalld_T_22; // @[RocketCore.scala 778:34]
  wire  _clock_en_T_1 = ~long_latency_stall; // @[RocketCore.scala 869:49]
  wire  clock_en = clock_en_reg | ex_pc_valid | ~long_latency_stall & io_imem_resp_valid; // @[RocketCore.scala 869:45]
  wire  _ctrl_stalld_T_24 = ~clock_en; // @[RocketCore.scala 780:5]
  wire  _ctrl_stalld_T_25 = _ctrl_stalld_T_23 | _ctrl_stalld_T_24; // @[RocketCore.scala 779:96]
  wire  id_do_fence = id_do_fence_x8; // @[RocketCore.scala 322:17]
  wire  _ctrl_stalld_T_26 = _ctrl_stalld_T_25 | _id_do_fence_x8_T_6; // @[RocketCore.scala 780:15]
  wire  _ctrl_stalld_T_27 = _ctrl_stalld_T_26 | csr_io_csr_stall; // @[RocketCore.scala 781:17]
  wire  _ctrl_stalld_T_28 = _ctrl_stalld_T_27 | id_reg_pause; // @[RocketCore.scala 782:22]
  wire  ctrl_stalld = _ctrl_stalld_T_28 | io_traceStall; // @[RocketCore.scala 783:18]
  wire  ctrl_killd = ~ibuf_io_inst_0_valid | ibuf_io_inst_0_bits_replay | take_pc_mem_wb | ctrl_stalld |
    csr_io_interrupt; // @[RocketCore.scala 785:104]
  wire  _ex_reg_valid_T = ~ctrl_killd; // @[RocketCore.scala 424:19]
  wire  _ex_reg_replay_T = ~take_pc_mem_wb; // @[RocketCore.scala 425:20]
  wire  _ex_reg_replay_T_1 = ~take_pc_mem_wb & ibuf_io_inst_0_valid; // @[RocketCore.scala 425:29]
  wire  _GEN_1 = id_ctrl_decoder_25 & id_fence_succ == 4'h0 | id_reg_pause; // @[RocketCore.scala 435:49 RocketCore.scala 435:64 RocketCore.scala 115:25]
  wire  _GEN_2 = id_fence_next | _GEN_0; // @[RocketCore.scala 436:26 RocketCore.scala 436:41]
  wire [1:0] _T_41 = {ibuf_io_inst_0_bits_xcpt1_pf_inst,ibuf_io_inst_0_bits_xcpt1_ae_inst}; // @[RocketCore.scala 442:22]
  wire  _GEN_5 = |_T_41 | ibuf_io_inst_0_bits_rvc; // @[RocketCore.scala 442:34 RocketCore.scala 445:20 RocketCore.scala 431:16]
  wire [1:0] _T_43 = {ibuf_io_inst_0_bits_xcpt0_pf_inst,ibuf_io_inst_0_bits_xcpt0_ae_inst}; // @[RocketCore.scala 447:40]
  wire  _GEN_9 = id_xcpt | id_ctrl_decoder_12; // @[RocketCore.scala 437:20 RocketCore.scala 439:22 RocketCore.scala 430:13]
  wire  _T_47 = id_ctrl_decoder_15 == 5'h5; // @[package.scala 15:47]
  wire  _T_48 = _id_sfence_T | _T_47; // @[package.scala 72:59]
  wire [1:0] _ex_reg_mem_size_T_1 = {_T_229,_T_227}; // @[Cat.scala 30:58]
  wire  do_bypass = id_bypass_src_0_0 | id_bypass_src_0_1 | id_bypass_src_0_2 | id_bypass_src_0_3; // @[RocketCore.scala 465:48]
  wire [1:0] _bypass_src_T = id_bypass_src_0_2 ? 2'h2 : 2'h3; // @[Mux.scala 47:69]
  wire [1:0] _bypass_src_T_1 = id_bypass_src_0_1 ? 2'h1 : _bypass_src_T; // @[Mux.scala 47:69]
  wire  wb_valid = wb_reg_valid & ~replay_wb_common & ~wb_xcpt; // @[RocketCore.scala 671:45]
  wire  wb_wen = wb_valid & wb_ctrl_wxd; // @[RocketCore.scala 672:25]
  wire  rf_wen = wb_wen | ll_wen; // @[RocketCore.scala 673:23]
  wire [4:0] rf_waddr = ll_wen ? ll_waddr : wb_waddr; // @[RocketCore.scala 674:21]
  wire  _T_222 = rf_waddr != 5'h0; // @[RocketCore.scala 1029:16]
  wire  _rf_wdata_T = dmem_resp_valid & dmem_resp_xpu; // @[RocketCore.scala 675:38]
  wire [63:0] ll_wdata = div_io_resp_bits_data;
  wire  _rf_wdata_T_2 = wb_ctrl_csr != 3'h0; // @[RocketCore.scala 677:34]
  wire [63:0] _rf_wdata_T_3 = wb_ctrl_mul ? m_io_resp_bits_data : wb_reg_wdata; // @[RocketCore.scala 678:21]
  wire [63:0] _rf_wdata_T_4 = wb_ctrl_csr != 3'h0 ? csr_io_rw_rdata : _rf_wdata_T_3; // @[RocketCore.scala 677:21]
  wire [63:0] _rf_wdata_T_5 = ll_wen ? ll_wdata : _rf_wdata_T_4; // @[RocketCore.scala 676:21]
  wire [63:0] rf_wdata = dmem_resp_valid & dmem_resp_xpu ? io_dmem_resp_bits_data : _rf_wdata_T_5; // @[RocketCore.scala 675:21]
  wire [63:0] _GEN_286 = rf_waddr == id_raddr1 ? rf_wdata : _id_rs_T_4; // @[RocketCore.scala 1032:31 RocketCore.scala 1032:39 RocketCore.scala 1024:19]
  wire [63:0] _GEN_293 = rf_waddr != 5'h0 ? _GEN_286 : _id_rs_T_4; // @[RocketCore.scala 1029:29 RocketCore.scala 1024:19]
  wire [63:0] id_rs_0 = rf_wen ? _GEN_293 : _id_rs_T_4; // @[RocketCore.scala 680:17 RocketCore.scala 1024:19]
  wire  do_bypass_1 = id_bypass_src_1_0 | id_bypass_src_1_1 | id_bypass_src_1_2 | id_bypass_src_1_3; // @[RocketCore.scala 465:48]
  wire [1:0] _bypass_src_T_2 = id_bypass_src_1_2 ? 2'h2 : 2'h3; // @[Mux.scala 47:69]
  wire [63:0] _GEN_287 = rf_waddr == id_raddr2 ? rf_wdata : _id_rs_T_9; // @[RocketCore.scala 1032:31 RocketCore.scala 1032:39 RocketCore.scala 1024:19]
  wire [63:0] _GEN_294 = rf_waddr != 5'h0 ? _GEN_287 : _id_rs_T_9; // @[RocketCore.scala 1029:29 RocketCore.scala 1024:19]
  wire [63:0] id_rs_1 = rf_wen ? _GEN_294 : _id_rs_T_9; // @[RocketCore.scala 680:17 RocketCore.scala 1024:19]
  wire [31:0] inst = ibuf_io_inst_0_bits_rvc ? {{16'd0}, ibuf_io_inst_0_bits_raw[15:0]} : ibuf_io_inst_0_bits_raw; // @[RocketCore.scala 475:21]
  wire  id_load_use = mem_reg_valid & data_hazard_mem & mem_ctrl_mem; // @[RocketCore.scala 747:51]
  wire  _replay_ex_structural_T = ~io_dmem_req_ready; // @[RocketCore.scala 493:45]
  wire  _replay_ex_structural_T_2 = ~div_io_req_ready; // @[RocketCore.scala 494:45]
  wire  _replay_ex_structural_T_3 = ex_ctrl_div & ~div_io_req_ready; // @[RocketCore.scala 494:42]
  wire  replay_ex_structural = ex_ctrl_mem & ~io_dmem_req_ready | _replay_ex_structural_T_3; // @[RocketCore.scala 493:64]
  wire  replay_ex_load_use = wb_dcache_miss & ex_reg_load_use; // @[RocketCore.scala 495:43]
  wire  replay_ex = ex_reg_replay | ex_reg_valid & (replay_ex_structural | replay_ex_load_use); // @[RocketCore.scala 496:33]
  wire  ctrl_killx = take_pc_mem_wb | replay_ex | ~ex_reg_valid; // @[RocketCore.scala 497:48]
  wire  _ex_slow_bypass_T = ex_ctrl_mem_cmd == 5'h7; // @[RocketCore.scala 499:40]
  wire  ex_slow_bypass = ex_ctrl_mem_cmd == 5'h7 | ex_reg_mem_size < 2'h2; // @[RocketCore.scala 499:50]
  wire  ex_sfence = ex_ctrl_mem & ex_ctrl_mem_cmd == 5'h14; // @[RocketCore.scala 500:48]
  wire  ex_xcpt = ex_reg_xcpt_interrupt | ex_reg_xcpt; // @[RocketCore.scala 503:28]
  wire  mem_pc_valid = mem_reg_valid | mem_reg_replay | mem_reg_xcpt_interrupt; // @[RocketCore.scala 509:54]
  wire  mem_npc_misaligned = _id_illegal_insn_T_18 & mem_npc[1] & ~mem_reg_sfence; // @[RocketCore.scala 518:70]
  wire  _mem_int_wdata_T = ~mem_reg_xcpt; // @[RocketCore.scala 519:27]
  wire [63:0] mem_int_wdata = ~mem_reg_xcpt & (mem_ctrl_jalr ^ mem_npc_misaligned) ? $signed({{24{mem_br_target[39]}},
    mem_br_target}) : $signed(mem_reg_wdata); // @[RocketCore.scala 519:119]
  wire  mem_cfi = mem_ctrl_branch | mem_ctrl_jalr | mem_ctrl_jal; // @[RocketCore.scala 520:50]
  wire  mem_cfi_taken = _mem_br_target_T_1 | mem_ctrl_jalr | mem_ctrl_jal; // @[RocketCore.scala 521:74]
  wire  mem_direction_misprediction = mem_ctrl_branch & mem_br_taken != mem_reg_btb_resp_taken; // @[RocketCore.scala 522:53]
  wire  _mem_reg_valid_T = ~ctrl_killx; // @[RocketCore.scala 526:20]
  wire  _T_76 = mem_reg_valid & mem_reg_flush_pipe; // @[RocketCore.scala 533:23]
  wire  _mem_reg_load_T_5 = ex_ctrl_mem_cmd == 5'h4; // @[package.scala 15:47]
  wire  _mem_reg_load_T_6 = ex_ctrl_mem_cmd == 5'h9; // @[package.scala 15:47]
  wire  _mem_reg_load_T_7 = ex_ctrl_mem_cmd == 5'ha; // @[package.scala 15:47]
  wire  _mem_reg_load_T_8 = ex_ctrl_mem_cmd == 5'hb; // @[package.scala 15:47]
  wire  _mem_reg_load_T_11 = _mem_reg_load_T_5 | _mem_reg_load_T_6 | _mem_reg_load_T_7 | _mem_reg_load_T_8; // @[package.scala 72:59]
  wire  _mem_reg_load_T_12 = ex_ctrl_mem_cmd == 5'h8; // @[package.scala 15:47]
  wire  _mem_reg_load_T_13 = ex_ctrl_mem_cmd == 5'hc; // @[package.scala 15:47]
  wire  _mem_reg_load_T_14 = ex_ctrl_mem_cmd == 5'hd; // @[package.scala 15:47]
  wire  _mem_reg_load_T_15 = ex_ctrl_mem_cmd == 5'he; // @[package.scala 15:47]
  wire  _mem_reg_load_T_16 = ex_ctrl_mem_cmd == 5'hf; // @[package.scala 15:47]
  wire  _mem_reg_load_T_20 = _mem_reg_load_T_12 | _mem_reg_load_T_13 | _mem_reg_load_T_14 | _mem_reg_load_T_15 |
    _mem_reg_load_T_16; // @[package.scala 72:59]
  wire  _mem_reg_load_T_21 = _mem_reg_load_T_11 | _mem_reg_load_T_20; // @[Consts.scala 79:44]
  wire  _mem_reg_load_T_22 = ex_ctrl_mem_cmd == 5'h0 | ex_ctrl_mem_cmd == 5'h6 | _ex_slow_bypass_T | _mem_reg_load_T_21; // @[Consts.scala 81:75]
  wire  _mem_reg_store_T_22 = ex_ctrl_mem_cmd == 5'h1 | ex_ctrl_mem_cmd == 5'h11 | _ex_slow_bypass_T |
    _mem_reg_load_T_21; // @[Consts.scala 82:76]
  wire [7:0] mem_reg_rs2_hi = ex_rs_1[7:0]; // @[AMOALU.scala 26:66]
  wire [63:0] _mem_reg_rs2_T_1 = {mem_reg_rs2_hi,mem_reg_rs2_hi,mem_reg_rs2_hi,mem_reg_rs2_hi,mem_reg_rs2_hi,
    mem_reg_rs2_hi,mem_reg_rs2_hi,mem_reg_rs2_hi}; // @[Cat.scala 30:58]
  wire [15:0] mem_reg_rs2_hi_3 = ex_rs_1[15:0]; // @[AMOALU.scala 26:66]
  wire [63:0] _mem_reg_rs2_T_3 = {mem_reg_rs2_hi_3,mem_reg_rs2_hi_3,mem_reg_rs2_hi_3,mem_reg_rs2_hi_3}; // @[Cat.scala 30:58]
  wire [31:0] mem_reg_rs2_hi_5 = ex_rs_1[31:0]; // @[AMOALU.scala 26:66]
  wire [63:0] _mem_reg_rs2_T_5 = {mem_reg_rs2_hi_5,mem_reg_rs2_hi_5}; // @[Cat.scala 30:58]
  wire [63:0] _mem_reg_rs2_T_6 = ex_reg_mem_size == 2'h2 ? _mem_reg_rs2_T_5 : ex_rs_1; // @[AMOALU.scala 26:13]
  wire [63:0] _mem_reg_rs2_T_7 = ex_reg_mem_size == 2'h1 ? _mem_reg_rs2_T_3 : _mem_reg_rs2_T_6; // @[AMOALU.scala 26:13]
  wire  _GEN_92 = ex_ctrl_jalr & csr_io_status_debug | ex_ctrl_fence_i; // @[RocketCore.scala 560:48 RocketCore.scala 562:24 RocketCore.scala 536:14]
  wire  _GEN_93 = ex_ctrl_jalr & csr_io_status_debug | ex_reg_flush_pipe; // @[RocketCore.scala 560:48 RocketCore.scala 563:26 RocketCore.scala 544:24]
  wire  mem_breakpoint = mem_reg_load & bpu_io_xcpt_ld | mem_reg_store & bpu_io_xcpt_st; // @[RocketCore.scala 567:57]
  wire  mem_debug_breakpoint = mem_reg_load & bpu_io_debug_ld | mem_reg_store & bpu_io_debug_st; // @[RocketCore.scala 568:64]
  wire  mem_ldst_xcpt = mem_debug_breakpoint | mem_breakpoint; // @[RocketCore.scala 977:26]
  wire [3:0] mem_ldst_cause = mem_debug_breakpoint ? 4'he : 4'h3; // @[Mux.scala 47:69]
  wire  _T_81 = mem_reg_xcpt_interrupt | mem_reg_xcpt; // @[RocketCore.scala 574:29]
  wire  _T_82 = mem_reg_valid & mem_npc_misaligned; // @[RocketCore.scala 575:20]
  wire  _T_83 = mem_reg_valid & mem_ldst_xcpt; // @[RocketCore.scala 576:20]
  wire  mem_xcpt = _T_81 | _T_82 | _T_83; // @[RocketCore.scala 977:26]
  wire [3:0] _T_85 = _T_82 ? 4'h0 : mem_ldst_cause; // @[Mux.scala 47:69]
  wire [63:0] mem_cause = _T_81 ? mem_reg_cause : {{60'd0}, _T_85}; // @[Mux.scala 47:69]
  wire  dcache_kill_mem = _T_34 & io_dmem_replay_next; // @[RocketCore.scala 585:55]
  wire  fpu_kill_mem = mem_reg_valid & mem_ctrl_fp & io_fpu_nack_mem; // @[RocketCore.scala 586:51]
  wire  replay_mem = dcache_kill_mem | mem_reg_replay | fpu_kill_mem; // @[RocketCore.scala 587:55]
  wire  killm_common = dcache_kill_mem | take_pc_wb | mem_reg_xcpt | ~mem_reg_valid; // @[RocketCore.scala 588:68]
  reg  div_io_kill_REG; // @[RocketCore.scala 589:37]
  wire  ctrl_killm = killm_common | mem_xcpt | fpu_kill_mem; // @[RocketCore.scala 590:45]
  wire  _wb_reg_valid_T = ~ctrl_killm; // @[RocketCore.scala 593:19]
  wire  _wb_reg_replay_T = ~take_pc_wb; // @[RocketCore.scala 594:34]
  wire  _T_113 = bpu_io_bpwatch_0_rvalid_0 & mem_reg_load | bpu_io_bpwatch_0_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_116 = bpu_io_bpwatch_1_rvalid_0 & mem_reg_load | bpu_io_bpwatch_1_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_119 = bpu_io_bpwatch_2_rvalid_0 & mem_reg_load | bpu_io_bpwatch_2_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_122 = bpu_io_bpwatch_3_rvalid_0 & mem_reg_load | bpu_io_bpwatch_3_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_125 = bpu_io_bpwatch_4_rvalid_0 & mem_reg_load | bpu_io_bpwatch_4_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_128 = bpu_io_bpwatch_5_rvalid_0 & mem_reg_load | bpu_io_bpwatch_5_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_131 = bpu_io_bpwatch_6_rvalid_0 & mem_reg_load | bpu_io_bpwatch_6_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_134 = bpu_io_bpwatch_7_rvalid_0 & mem_reg_load | bpu_io_bpwatch_7_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_137 = bpu_io_bpwatch_8_rvalid_0 & mem_reg_load | bpu_io_bpwatch_8_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_140 = bpu_io_bpwatch_9_rvalid_0 & mem_reg_load | bpu_io_bpwatch_9_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_143 = bpu_io_bpwatch_10_rvalid_0 & mem_reg_load | bpu_io_bpwatch_10_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_146 = bpu_io_bpwatch_11_rvalid_0 & mem_reg_load | bpu_io_bpwatch_11_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_149 = bpu_io_bpwatch_12_rvalid_0 & mem_reg_load | bpu_io_bpwatch_12_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_152 = bpu_io_bpwatch_13_rvalid_0 & mem_reg_load | bpu_io_bpwatch_13_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_155 = bpu_io_bpwatch_14_rvalid_0 & mem_reg_load | bpu_io_bpwatch_14_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_158 = bpu_io_bpwatch_15_rvalid_0 & mem_reg_load | bpu_io_bpwatch_15_wvalid_0 & mem_reg_store; // @[RocketCore.scala 610:97]
  wire  _T_159 = mem_reg_wphit_0 | _T_113; // @[package.scala 66:75]
  wire  _T_160 = mem_reg_wphit_1 | _T_116; // @[package.scala 66:75]
  wire  _T_161 = mem_reg_wphit_2 | _T_119; // @[package.scala 66:75]
  wire  _T_162 = mem_reg_wphit_3 | _T_122; // @[package.scala 66:75]
  wire  _T_163 = mem_reg_wphit_4 | _T_125; // @[package.scala 66:75]
  wire  _T_164 = mem_reg_wphit_5 | _T_128; // @[package.scala 66:75]
  wire  _T_165 = mem_reg_wphit_6 | _T_131; // @[package.scala 66:75]
  wire  _T_166 = mem_reg_wphit_7 | _T_134; // @[package.scala 66:75]
  wire  _T_167 = mem_reg_wphit_8 | _T_137; // @[package.scala 66:75]
  wire  _T_168 = mem_reg_wphit_9 | _T_140; // @[package.scala 66:75]
  wire  _T_169 = mem_reg_wphit_10 | _T_143; // @[package.scala 66:75]
  wire  _T_170 = mem_reg_wphit_11 | _T_146; // @[package.scala 66:75]
  wire  _T_171 = mem_reg_wphit_12 | _T_149; // @[package.scala 66:75]
  wire  _T_172 = mem_reg_wphit_13 | _T_152; // @[package.scala 66:75]
  wire  _T_173 = mem_reg_wphit_14 | _T_155; // @[package.scala 66:75]
  wire  _T_174 = mem_reg_wphit_15 | _T_158; // @[package.scala 66:75]
  wire [2:0] _T_192 = _T_184 ? 3'h7 : 3'h5; // @[Mux.scala 47:69]
  wire [3:0] _T_193 = _T_182 ? 4'hd : {{1'd0}, _T_192}; // @[Mux.scala 47:69]
  wire [3:0] _T_194 = _T_180 ? 4'hf : _T_193; // @[Mux.scala 47:69]
  wire [3:0] _T_195 = _T_178 ? 4'h4 : _T_194; // @[Mux.scala 47:69]
  wire [3:0] _T_196 = _T_176 ? 4'h6 : _T_195; // @[Mux.scala 47:69]
  wire [63:0] wb_cause = wb_reg_xcpt ? wb_reg_cause : {{60'd0}, _T_196}; // @[Mux.scala 47:69]
  wire  _T_197 = wb_cause == 64'h6; // @[RocketCore.scala 981:38]
  wire  _T_201 = wb_cause == 64'h4; // @[RocketCore.scala 981:38]
  wire  _T_205 = wb_cause == 64'h7; // @[RocketCore.scala 981:38]
  wire  _T_209 = wb_cause == 64'h5; // @[RocketCore.scala 981:38]
  wire  _T_213 = wb_cause == 64'hf; // @[RocketCore.scala 981:38]
  wire  _T_217 = wb_cause == 64'hd; // @[RocketCore.scala 981:38]
  wire  wb_pc_valid = wb_reg_valid | wb_reg_replay | wb_reg_xcpt; // @[RocketCore.scala 635:51]
  wire [15:0] csr_io_inst_0_hi = &wb_reg_raw_inst[1:0] ? wb_reg_inst[31:16] : 16'h0; // @[RocketCore.scala 688:50]
  wire [15:0] csr_io_inst_0_lo = wb_reg_raw_inst[15:0]; // @[RocketCore.scala 688:119]
  wire  _tval_valid_T = wb_cause == 64'h2; // @[package.scala 15:47]
  wire  _tval_valid_T_1 = wb_cause == 64'h3; // @[package.scala 15:47]
  wire  _tval_valid_T_6 = wb_cause == 64'h1; // @[package.scala 15:47]
  wire  _tval_valid_T_9 = wb_cause == 64'hc; // @[package.scala 15:47]
  wire  _tval_valid_T_18 = _tval_valid_T | _tval_valid_T_1 | _T_201 | _T_197 | _T_209 | _T_205 | _tval_valid_T_6 |
    _T_217 | _T_213 | _tval_valid_T_9; // @[package.scala 72:59]
  wire  tval_valid = wb_xcpt & _tval_valid_T_18; // @[RocketCore.scala 697:28]
  wire [63:0] _csr_io_tval_a_T = wb_reg_wdata; // @[RocketCore.scala 991:16]
  wire [24:0] a_1 = _csr_io_tval_a_T[63:39]; // @[RocketCore.scala 991:23]
  wire  msb_1 = $signed(a_1) == 25'sh0 | $signed(a_1) == -25'sh1 ? wb_reg_wdata[39] : ~wb_reg_wdata[38]; // @[RocketCore.scala 992:18]
  wire [38:0] csr_io_tval_lo = wb_reg_wdata[38:0]; // @[RocketCore.scala 993:16]
  wire [39:0] _csr_io_tval_T = {msb_1,csr_io_tval_lo}; // @[Cat.scala 30:58]
  wire [2:0] _csr_io_rw_cmd_T = wb_reg_valid ? 3'h0 : 3'h4; // @[CSR.scala 132:15]
  wire [2:0] _csr_io_rw_cmd_T_1 = ~_csr_io_rw_cmd_T; // @[CSR.scala 132:11]
  wire [31:0] _T_233 = 32'h1 << ll_waddr; // @[RocketCore.scala 1007:62]
  wire [31:0] _T_234 = ll_wen ? _T_233 : 32'h0; // @[RocketCore.scala 1007:49]
  wire [31:0] _T_235 = ~_T_234; // @[RocketCore.scala 999:69]
  wire [31:0] _T_236 = r & _T_235; // @[RocketCore.scala 999:67]
  wire  _T_238 = wb_set_sboard & wb_wen; // @[RocketCore.scala 731:28]
  wire [31:0] _T_239 = 32'h1 << wb_waddr; // @[RocketCore.scala 1007:62]
  wire [31:0] _T_240 = _T_238 ? _T_239 : 32'h0; // @[RocketCore.scala 1007:49]
  wire [31:0] _T_241 = _T_236 | _T_240; // @[RocketCore.scala 998:65]
  wire  _T_242 = ll_wen | _T_238; // @[RocketCore.scala 1010:17]
  wire  _id_stall_fpu_T_2 = (wb_dcache_miss & wb_ctrl_wfd | io_fpu_sboard_set) & wb_valid; // @[RocketCore.scala 756:72]
  wire [31:0] _id_stall_fpu_T_4 = _id_stall_fpu_T_2 ? _T_239 : 32'h0; // @[RocketCore.scala 1007:49]
  wire [31:0] _id_stall_fpu_T_5 = id_stall_fpu_state | _id_stall_fpu_T_4; // @[RocketCore.scala 998:65]
  wire  _id_stall_fpu_T_7 = dmem_resp_replay & io_dmem_resp_bits_tag[0]; // @[RocketCore.scala 757:38]
  wire [31:0] _id_stall_fpu_T_8 = 32'h1 << dmem_resp_waddr; // @[RocketCore.scala 1007:62]
  wire [31:0] _id_stall_fpu_T_9 = _id_stall_fpu_T_7 ? _id_stall_fpu_T_8 : 32'h0; // @[RocketCore.scala 1007:49]
  wire [31:0] _id_stall_fpu_T_10 = ~_id_stall_fpu_T_9; // @[RocketCore.scala 999:69]
  wire [31:0] _id_stall_fpu_T_11 = _id_stall_fpu_T_5 & _id_stall_fpu_T_10; // @[RocketCore.scala 999:67]
  wire  _id_stall_fpu_T_12 = _id_stall_fpu_T_2 | _id_stall_fpu_T_7; // @[RocketCore.scala 1010:17]
  wire [31:0] _id_stall_fpu_T_13 = 32'h1 << io_fpu_sboard_clra; // @[RocketCore.scala 1007:62]
  wire [31:0] _id_stall_fpu_T_14 = io_fpu_sboard_clr ? _id_stall_fpu_T_13 : 32'h0; // @[RocketCore.scala 1007:49]
  wire [31:0] _id_stall_fpu_T_15 = ~_id_stall_fpu_T_14; // @[RocketCore.scala 999:69]
  wire [31:0] _id_stall_fpu_T_16 = _id_stall_fpu_T_11 & _id_stall_fpu_T_15; // @[RocketCore.scala 999:67]
  wire  _id_stall_fpu_T_17 = _id_stall_fpu_T_2 | _id_stall_fpu_T_7 | io_fpu_sboard_clr; // @[RocketCore.scala 1010:17]
  wire [39:0] _io_imem_req_bits_pc_T_1 = replay_wb_common ? wb_reg_pc : mem_npc; // @[RocketCore.scala 791:8]
  wire  _io_imem_might_request_imem_might_request_reg_T = ex_pc_valid | mem_pc_valid; // @[RocketCore.scala 795:43]
  wire  _io_imem_btb_update_bits_cfiType_T = mem_ctrl_jal | mem_ctrl_jalr; // @[RocketCore.scala 810:23]
  wire [4:0] _io_imem_btb_update_bits_cfiType_T_5 = mem_reg_inst[19:15] & 5'h1b; // @[RocketCore.scala 811:62]
  wire [1:0] _io_imem_btb_update_bits_cfiType_T_10 = mem_ctrl_jalr & 5'h1 == _io_imem_btb_update_bits_cfiType_T_5 ? 2'h3
     : {{1'd0}, _io_imem_btb_update_bits_cfiType_T}; // @[RocketCore.scala 811:8]
  wire [1:0] _io_imem_btb_update_bits_br_pc_T = mem_reg_rvc ? 2'h0 : 2'h2; // @[RocketCore.scala 815:74]
  wire [39:0] _GEN_39 = {{38'd0}, _io_imem_btb_update_bits_br_pc_T}; // @[RocketCore.scala 815:69]
  wire [39:0] _io_imem_btb_update_bits_br_pc_T_2 = mem_reg_pc + _GEN_39; // @[RocketCore.scala 815:69]
  wire [38:0] _io_imem_btb_update_bits_pc_T = ~io_imem_btb_update_bits_br_pc; // @[RocketCore.scala 816:35]
  wire [38:0] _io_imem_btb_update_bits_pc_T_1 = _io_imem_btb_update_bits_pc_T | 39'h3; // @[RocketCore.scala 816:66]
  wire [5:0] ex_dcache_tag = {ex_waddr,ex_ctrl_fp}; // @[Cat.scala 30:58]
  wire [24:0] a_2 = _ex_op1_T[63:39]; // @[RocketCore.scala 991:23]
  wire  msb_2 = $signed(a_2) == 25'sh0 | $signed(a_2) == -25'sh1 ? alu_io_adder_out[39] : ~alu_io_adder_out[38]; // @[RocketCore.scala 992:18]
  wire [38:0] io_dmem_req_bits_addr_lo = alu_io_adder_out[38:0]; // @[RocketCore.scala 993:16]
  wire  unpause = csr_io_time[4:0] == 5'h0 | csr_io_inhibit_cycle | io_dmem_perf_release | take_pc_mem_wb; // @[RocketCore.scala 862:116]
  wire  _clock_en_reg_T_1 = _io_imem_might_request_imem_might_request_reg_T | wb_pc_valid; // @[RocketCore.scala 871:35]
  wire  _clock_en_reg_T_3 = _clock_en_reg_T_1 | io_ptw_customCSRs_csrs_1_value[2]; // @[RocketCore.scala 871:50]
  wire  _clock_en_reg_T_5 = _clock_en_reg_T_3 | _replay_ex_structural_T_2; // @[RocketCore.scala 872:46]
  wire  _clock_en_reg_T_8 = _clock_en_reg_T_5 | _ctrl_stalld_T_8; // @[RocketCore.scala 873:25]
  wire  _clock_en_reg_T_9 = _clock_en_reg_T_8 | io_dmem_replay_next; // @[RocketCore.scala 874:36]
  wire  _clock_en_reg_T_12 = _clock_en_T_1 & (ibuf_io_inst_0_valid | io_imem_resp_valid); // @[RocketCore.scala 876:28]
  wire  _clock_en_reg_T_13 = _clock_en_reg_T_9 | _clock_en_reg_T_12; // @[RocketCore.scala 875:27]
  reg  icache_blocked_REG; // @[RocketCore.scala 882:55]
  wire  icache_blocked = ~(io_imem_resp_valid | icache_blocked_REG); // @[RocketCore.scala 882:24]
  wire [1:0] csr_io_counters_0_inc_set = csr_io_counters_0_eventSel[1:0]; // @[Events.scala 39:13]
  wire [55:0] csr_io_counters_0_inc_mask = csr_io_counters_0_eventSel[63:8]; // @[Events.scala 39:44]
  wire  _csr_io_counters_3_inc_sets_T_2 = ~id_ctrl_decoder_1; // @[RocketCore.scala 130:66]
  wire  hits__1 = id_ctrl_decoder_14 & id_ctrl_decoder_15 == 5'h0 & ~id_ctrl_decoder_1; // @[RocketCore.scala 130:63]
  wire  _csr_io_counters_3_inc_sets_T_9 = id_ctrl_decoder_15 == 5'h4; // @[package.scala 15:47]
  wire  _csr_io_counters_3_inc_sets_T_10 = id_ctrl_decoder_15 == 5'h9; // @[package.scala 15:47]
  wire  _csr_io_counters_3_inc_sets_T_11 = id_ctrl_decoder_15 == 5'ha; // @[package.scala 15:47]
  wire  _csr_io_counters_3_inc_sets_T_12 = id_ctrl_decoder_15 == 5'hb; // @[package.scala 15:47]
  wire  _csr_io_counters_3_inc_sets_T_15 = _csr_io_counters_3_inc_sets_T_9 | _csr_io_counters_3_inc_sets_T_10 |
    _csr_io_counters_3_inc_sets_T_11 | _csr_io_counters_3_inc_sets_T_12; // @[package.scala 72:59]
  wire  _csr_io_counters_3_inc_sets_T_16 = id_ctrl_decoder_15 == 5'h8; // @[package.scala 15:47]
  wire  _csr_io_counters_3_inc_sets_T_17 = id_ctrl_decoder_15 == 5'hc; // @[package.scala 15:47]
  wire  _csr_io_counters_3_inc_sets_T_18 = id_ctrl_decoder_15 == 5'hd; // @[package.scala 15:47]
  wire  _csr_io_counters_3_inc_sets_T_19 = id_ctrl_decoder_15 == 5'he; // @[package.scala 15:47]
  wire  _csr_io_counters_3_inc_sets_T_20 = id_ctrl_decoder_15 == 5'hf; // @[package.scala 15:47]
  wire  _csr_io_counters_3_inc_sets_T_24 = _csr_io_counters_3_inc_sets_T_16 | _csr_io_counters_3_inc_sets_T_17 |
    _csr_io_counters_3_inc_sets_T_18 | _csr_io_counters_3_inc_sets_T_19 | _csr_io_counters_3_inc_sets_T_20; // @[package.scala 72:59]
  wire  _csr_io_counters_3_inc_sets_T_25 = _csr_io_counters_3_inc_sets_T_15 | _csr_io_counters_3_inc_sets_T_24; // @[Consts.scala 79:44]
  wire  _csr_io_counters_3_inc_sets_T_26 = id_ctrl_decoder_15 == 5'h6; // @[package.scala 15:47]
  wire  _csr_io_counters_3_inc_sets_T_27 = id_ctrl_decoder_15 == 5'h7; // @[package.scala 15:47]
  wire  _csr_io_counters_3_inc_sets_T_28 = _csr_io_counters_3_inc_sets_T_26 | _csr_io_counters_3_inc_sets_T_27; // @[package.scala 72:59]
  wire  hits__3 = id_ctrl_decoder_14 & (_csr_io_counters_3_inc_sets_T_25 | _csr_io_counters_3_inc_sets_T_28); // @[RocketCore.scala 132:55]
  wire  hits__2 = id_ctrl_decoder_14 & id_ctrl_decoder_15 == 5'h1 & _csr_io_counters_3_inc_sets_T_2; // @[RocketCore.scala 131:64]
  wire  _csr_io_counters_3_inc_sets_T_37 = id_ctrl_decoder_23 != 3'h0; // @[RocketCore.scala 134:142]
  wire  hits__5 = id_ctrl_decoder_22 & ~(id_ctrl_decoder_4 | id_ctrl_decoder_5 | id_ctrl_decoder_14 | id_ctrl_decoder_1
     | id_ctrl_decoder_20 | id_ctrl_decoder_21 | id_ctrl_decoder_23 != 3'h0); // @[RocketCore.scala 134:35]
  wire [8:0] csr_io_counters_0_inc_sets_lo = {id_ctrl_decoder_5,id_ctrl_decoder_4,id_ctrl_decoder_3,hits__5,
    _csr_io_counters_3_inc_sets_T_37,hits__3,hits__2,hits__1,1'h0}; // @[Events.scala 15:21]
  wire  _csr_io_counters_3_inc_sets_T_43 = id_ctrl_decoder_1 & io_fpu_dec_ldst; // @[RocketCore.scala 143:39]
  wire  hits__12 = id_ctrl_decoder_1 & io_fpu_dec_ldst & ~io_fpu_dec_wen; // @[RocketCore.scala 143:58]
  wire  hits__11 = _csr_io_counters_3_inc_sets_T_43 & io_fpu_dec_wen; // @[RocketCore.scala 142:57]
  wire  _csr_io_counters_3_inc_sets_T_48 = id_ctrl_decoder_1 & io_fpu_dec_fma; // @[RocketCore.scala 145:37]
  wire  hits__14 = id_ctrl_decoder_1 & io_fpu_dec_fma & ~io_fpu_dec_swap23 & ~io_fpu_dec_ren3; // @[RocketCore.scala 145:77]
  wire  hits__13 = _csr_io_counters_3_inc_sets_T_48 & io_fpu_dec_swap23; // @[RocketCore.scala 144:55]
  wire  hits__17 = id_ctrl_decoder_1 & ~(io_fpu_dec_ldst | io_fpu_dec_fma | io_fpu_dec_div | io_fpu_dec_sqrt); // @[RocketCore.scala 148:39]
  wire  hits__16 = id_ctrl_decoder_1 & (io_fpu_dec_div | io_fpu_dec_sqrt); // @[RocketCore.scala 147:42]
  wire  hits__15 = _csr_io_counters_3_inc_sets_T_48 & io_fpu_dec_ren3; // @[RocketCore.scala 146:59]
  wire [17:0] _csr_io_counters_0_inc_sets_T_62 = {hits__17,hits__16,hits__15,hits__14,hits__13,hits__12,hits__11,
    id_ctrl_decoder_21,id_ctrl_decoder_20,csr_io_counters_0_inc_sets_lo}; // @[Events.scala 15:21]
  wire [55:0] _GEN_46 = {{38'd0}, _csr_io_counters_0_inc_sets_T_62}; // @[RocketCore.scala 128:89]
  wire [55:0] _csr_io_counters_0_inc_sets_T_64 = csr_io_counters_0_inc_mask & _GEN_46; // @[RocketCore.scala 128:89]
  wire  _csr_io_counters_0_inc_sets_T_65 = |_csr_io_counters_0_inc_sets_T_64; // @[RocketCore.scala 128:97]
  reg  csr_io_counters_0_inc_sets_r; // @[Reg.scala 15:16]
  reg  csr_io_counters_0_inc_sets_r_1; // @[Reg.scala 15:16]
  reg  csr_io_counters_0_inc_sets_r_2; // @[Reg.scala 15:16]
  wire  hits_1_0 = id_ex_hazard & ex_ctrl_mem | id_mem_hazard & mem_ctrl_mem | id_wb_hazard & wb_ctrl_mem; // @[RocketCore.scala 150:97]
  wire  hits_1_2 = id_ex_hazard & _ex_cannot_bypass_T | id_mem_hazard & _mem_cannot_bypass_T | id_wb_hazard &
    _rf_wdata_T_2; // @[RocketCore.scala 152:112]
  wire [4:0] csr_io_counters_0_inc_sets_lo_1 = {_ctrl_stalld_T_13,icache_blocked,hits_1_2,id_sboard_hazard,hits_1_0}; // @[Events.scala 15:21]
  wire  hits_1_6 = take_pc_mem & mem_wrong_npc & mem_cfi & ~mem_direction_misprediction & ~icache_blocked; // @[RocketCore.scala 156:127]
  wire  hits_1_5 = take_pc_mem & mem_direction_misprediction; // @[RocketCore.scala 155:50]
  wire  hits_1_10 = id_ex_hazard & ex_ctrl_fp | id_mem_hazard & mem_ctrl_fp | id_wb_hazard & wb_ctrl_fp |
    _ctrl_stalld_T_11; // @[RocketCore.scala 162:121]
  wire  hits_1_9 = id_ex_hazard & (ex_ctrl_mul | ex_ctrl_div) | id_mem_hazard & (mem_ctrl_mul | mem_ctrl_div) |
    id_wb_hazard & wb_ctrl_div; // @[RocketCore.scala 160:133]
  wire [10:0] _csr_io_counters_0_inc_sets_T_103 = {hits_1_10,hits_1_9,replay_wb_common,wb_reg_flush_pipe,hits_1_6,
    hits_1_5,csr_io_counters_0_inc_sets_lo_1}; // @[Events.scala 15:21]
  wire [55:0] _GEN_47 = {{45'd0}, _csr_io_counters_0_inc_sets_T_103}; // @[RocketCore.scala 149:40]
  wire [55:0] _csr_io_counters_0_inc_sets_T_104 = csr_io_counters_0_inc_mask & _GEN_47; // @[RocketCore.scala 149:40]
  wire  csr_io_counters_0_inc_sets_1 = |_csr_io_counters_0_inc_sets_T_104; // @[RocketCore.scala 149:48]
  wire [5:0] _csr_io_counters_0_inc_sets_T_105 = {io_ptw_perf_l2miss,io_dmem_perf_tlbMiss,io_imem_perf_tlbMiss,
    io_dmem_perf_release,io_dmem_perf_acquire,io_imem_perf_acquire}; // @[Events.scala 15:21]
  wire [55:0] _GEN_48 = {{50'd0}, _csr_io_counters_0_inc_sets_T_105}; // @[RocketCore.scala 163:40]
  wire [55:0] _csr_io_counters_0_inc_sets_T_106 = csr_io_counters_0_inc_mask & _GEN_48; // @[RocketCore.scala 163:40]
  wire  csr_io_counters_0_inc_sets_2 = |_csr_io_counters_0_inc_sets_T_106; // @[RocketCore.scala 163:48]
  reg  csr_io_counters_0_inc_REG; // @[RocketCore.scala 883:50]
  wire [1:0] csr_io_counters_1_inc_set = csr_io_counters_1_eventSel[1:0]; // @[Events.scala 39:13]
  wire [55:0] csr_io_counters_1_inc_mask = csr_io_counters_1_eventSel[63:8]; // @[Events.scala 39:44]
  wire [55:0] _csr_io_counters_1_inc_sets_T_64 = csr_io_counters_1_inc_mask & _GEN_46; // @[RocketCore.scala 128:89]
  wire  _csr_io_counters_1_inc_sets_T_65 = |_csr_io_counters_1_inc_sets_T_64; // @[RocketCore.scala 128:97]
  reg  csr_io_counters_1_inc_sets_r; // @[Reg.scala 15:16]
  reg  csr_io_counters_1_inc_sets_r_1; // @[Reg.scala 15:16]
  reg  csr_io_counters_1_inc_sets_r_2; // @[Reg.scala 15:16]
  wire [55:0] _csr_io_counters_1_inc_sets_T_104 = csr_io_counters_1_inc_mask & _GEN_47; // @[RocketCore.scala 149:40]
  wire  csr_io_counters_1_inc_sets_1 = |_csr_io_counters_1_inc_sets_T_104; // @[RocketCore.scala 149:48]
  wire [55:0] _csr_io_counters_1_inc_sets_T_106 = csr_io_counters_1_inc_mask & _GEN_48; // @[RocketCore.scala 163:40]
  wire  csr_io_counters_1_inc_sets_2 = |_csr_io_counters_1_inc_sets_T_106; // @[RocketCore.scala 163:48]
  reg  csr_io_counters_1_inc_REG; // @[RocketCore.scala 883:50]
  wire [1:0] csr_io_counters_2_inc_set = csr_io_counters_2_eventSel[1:0]; // @[Events.scala 39:13]
  wire [55:0] csr_io_counters_2_inc_mask = csr_io_counters_2_eventSel[63:8]; // @[Events.scala 39:44]
  wire [55:0] _csr_io_counters_2_inc_sets_T_64 = csr_io_counters_2_inc_mask & _GEN_46; // @[RocketCore.scala 128:89]
  wire  _csr_io_counters_2_inc_sets_T_65 = |_csr_io_counters_2_inc_sets_T_64; // @[RocketCore.scala 128:97]
  reg  csr_io_counters_2_inc_sets_r; // @[Reg.scala 15:16]
  reg  csr_io_counters_2_inc_sets_r_1; // @[Reg.scala 15:16]
  reg  csr_io_counters_2_inc_sets_r_2; // @[Reg.scala 15:16]
  wire [55:0] _csr_io_counters_2_inc_sets_T_104 = csr_io_counters_2_inc_mask & _GEN_47; // @[RocketCore.scala 149:40]
  wire  csr_io_counters_2_inc_sets_1 = |_csr_io_counters_2_inc_sets_T_104; // @[RocketCore.scala 149:48]
  wire [55:0] _csr_io_counters_2_inc_sets_T_106 = csr_io_counters_2_inc_mask & _GEN_48; // @[RocketCore.scala 163:40]
  wire  csr_io_counters_2_inc_sets_2 = |_csr_io_counters_2_inc_sets_T_106; // @[RocketCore.scala 163:48]
  reg  csr_io_counters_2_inc_REG; // @[RocketCore.scala 883:50]
  wire [1:0] csr_io_counters_3_inc_set = csr_io_counters_3_eventSel[1:0]; // @[Events.scala 39:13]
  wire [55:0] csr_io_counters_3_inc_mask = csr_io_counters_3_eventSel[63:8]; // @[Events.scala 39:44]
  wire [55:0] _csr_io_counters_3_inc_sets_T_64 = csr_io_counters_3_inc_mask & _GEN_46; // @[RocketCore.scala 128:89]
  wire  _csr_io_counters_3_inc_sets_T_65 = |_csr_io_counters_3_inc_sets_T_64; // @[RocketCore.scala 128:97]
  reg  csr_io_counters_3_inc_sets_r; // @[Reg.scala 15:16]
  reg  csr_io_counters_3_inc_sets_r_1; // @[Reg.scala 15:16]
  reg  csr_io_counters_3_inc_sets_r_2; // @[Reg.scala 15:16]
  wire [55:0] _csr_io_counters_3_inc_sets_T_104 = csr_io_counters_3_inc_mask & _GEN_47; // @[RocketCore.scala 149:40]
  wire  csr_io_counters_3_inc_sets_1 = |_csr_io_counters_3_inc_sets_T_104; // @[RocketCore.scala 149:48]
  wire [55:0] _csr_io_counters_3_inc_sets_T_106 = csr_io_counters_3_inc_mask & _GEN_48; // @[RocketCore.scala 163:40]
  wire  csr_io_counters_3_inc_sets_2 = |_csr_io_counters_3_inc_sets_T_106; // @[RocketCore.scala 163:48]
  reg  csr_io_counters_3_inc_REG; // @[RocketCore.scala 883:50]
  wire [39:0] coreMonitorBundle_pc_lo = csr_io_trace_0_iaddr; // @[RocketCore.scala 892:48]
  wire [23:0] coreMonitorBundle_pc_hi = coreMonitorBundle_pc_lo[39] ? 24'hffffff : 24'h0; // @[Bitwise.scala 72:12]
  reg [63:0] coreMonitorBundle_rd0val_x20; // @[RocketCore.scala 898:43]
  reg [63:0] coreMonitorBundle_rd0val_REG; // @[RocketCore.scala 898:34]
  reg [63:0] coreMonitorBundle_rd1val_x26; // @[RocketCore.scala 900:43]
  reg [63:0] coreMonitorBundle_rd1val_REG; // @[RocketCore.scala 900:34]
  wire [4:0] coreMonitorBundle_wrdst = wb_waddr; // @[RocketCore.scala 362:29]
  wire  coreMonitorBundle_wrenx = wb_wen & ~wb_set_sboard; // @[RocketCore.scala 893:37]
  wire [63:0] coreMonitorBundle_wrdata = rf_wdata; // @[RocketCore.scala 675:21]
  wire [4:0] coreMonitorBundle_rd0src = wb_reg_inst[19:15]; // @[RocketCore.scala 897:42]
  wire [63:0] coreMonitorBundle_rd0val = coreMonitorBundle_rd0val_REG; // @[RocketCore.scala 885:31 RocketCore.scala 898:28]
  wire [4:0] coreMonitorBundle_rd1src = wb_reg_inst[24:20]; // @[RocketCore.scala 899:42]
  wire [63:0] coreMonitorBundle_rd1val = coreMonitorBundle_rd1val_REG; // @[RocketCore.scala 885:31 RocketCore.scala 900:28]
  wire  coreMonitorBundle_clock = clock; // @[RocketCore.scala 885:31 RocketCore.scala 887:27]
  wire  coreMonitorBundle_reset = reset; // @[RocketCore.scala 885:31 RocketCore.scala 888:27]
  wire  coreMonitorBundle_excpt = csr_io_trace_0_exception; // @[RocketCore.scala 885:31 RocketCore.scala 902:27]
  wire [2:0] coreMonitorBundle_priv_mode = csr_io_trace_0_priv; // @[RocketCore.scala 885:31 RocketCore.scala 903:31]
  wire [63:0] coreMonitorBundle_hartid = {{62'd0}, io_hartid}; // @[RocketCore.scala 885:31 RocketCore.scala 889:28]
  wire [31:0] coreMonitorBundle_timer = csr_io_time[31:0]; // @[RocketCore.scala 890:41]
  wire  coreMonitorBundle_valid = csr_io_trace_0_valid & ~csr_io_trace_0_exception; // @[RocketCore.scala 891:52]
  wire [63:0] coreMonitorBundle_pc = {coreMonitorBundle_pc_hi,coreMonitorBundle_pc_lo}; // @[Cat.scala 30:58]
  wire  coreMonitorBundle_wrenf = 1'h0; // @[RocketCore.scala 885:31 RocketCore.scala 894:27]
  wire [31:0] coreMonitorBundle_inst = csr_io_trace_0_insn; // @[RocketCore.scala 885:31 RocketCore.scala 901:26]
  EICG_wrapper gated_clock_rocket_clock_gate ( // @[ClockGate.scala 24:20]
    .in(gated_clock_rocket_clock_gate_in),
    .test_en(gated_clock_rocket_clock_gate_test_en),
    .en(gated_clock_rocket_clock_gate_en),
    .out(gated_clock_rocket_clock_gate_out)
  );
  RHEA__IBuf ibuf ( // @[RocketCore.scala 255:20]
    .rf_reset(ibuf_rf_reset),
    .clock(ibuf_clock),
    .reset(ibuf_reset),
    .io_imem_ready(ibuf_io_imem_ready),
    .io_imem_valid(ibuf_io_imem_valid),
    .io_imem_bits_btb_taken(ibuf_io_imem_bits_btb_taken),
    .io_imem_bits_btb_bridx(ibuf_io_imem_bits_btb_bridx),
    .io_imem_bits_btb_entry(ibuf_io_imem_bits_btb_entry),
    .io_imem_bits_btb_bht_history(ibuf_io_imem_bits_btb_bht_history),
    .io_imem_bits_pc(ibuf_io_imem_bits_pc),
    .io_imem_bits_data(ibuf_io_imem_bits_data),
    .io_imem_bits_xcpt_pf_inst(ibuf_io_imem_bits_xcpt_pf_inst),
    .io_imem_bits_xcpt_ae_inst(ibuf_io_imem_bits_xcpt_ae_inst),
    .io_imem_bits_replay(ibuf_io_imem_bits_replay),
    .io_kill(ibuf_io_kill),
    .io_pc(ibuf_io_pc),
    .io_btb_resp_taken(ibuf_io_btb_resp_taken),
    .io_btb_resp_entry(ibuf_io_btb_resp_entry),
    .io_btb_resp_bht_history(ibuf_io_btb_resp_bht_history),
    .io_inst_0_ready(ibuf_io_inst_0_ready),
    .io_inst_0_valid(ibuf_io_inst_0_valid),
    .io_inst_0_bits_xcpt0_pf_inst(ibuf_io_inst_0_bits_xcpt0_pf_inst),
    .io_inst_0_bits_xcpt0_ae_inst(ibuf_io_inst_0_bits_xcpt0_ae_inst),
    .io_inst_0_bits_xcpt1_pf_inst(ibuf_io_inst_0_bits_xcpt1_pf_inst),
    .io_inst_0_bits_xcpt1_ae_inst(ibuf_io_inst_0_bits_xcpt1_ae_inst),
    .io_inst_0_bits_replay(ibuf_io_inst_0_bits_replay),
    .io_inst_0_bits_rvc(ibuf_io_inst_0_bits_rvc),
    .io_inst_0_bits_inst_bits(ibuf_io_inst_0_bits_inst_bits),
    .io_inst_0_bits_inst_rd(ibuf_io_inst_0_bits_inst_rd),
    .io_inst_0_bits_inst_rs1(ibuf_io_inst_0_bits_inst_rs1),
    .io_inst_0_bits_inst_rs2(ibuf_io_inst_0_bits_inst_rs2),
    .io_inst_0_bits_inst_rs3(ibuf_io_inst_0_bits_inst_rs3),
    .io_inst_0_bits_raw(ibuf_io_inst_0_bits_raw)
  );
  RHEA__CSRFile csr ( // @[RocketCore.scala 283:19]
    .rf_reset(csr_rf_reset),
    .clock(csr_clock),
    .reset(csr_reset),
    .io_ungated_clock(csr_io_ungated_clock),
    .io_interrupts_debug(csr_io_interrupts_debug),
    .io_interrupts_mtip(csr_io_interrupts_mtip),
    .io_interrupts_msip(csr_io_interrupts_msip),
    .io_interrupts_meip(csr_io_interrupts_meip),
    .io_interrupts_seip(csr_io_interrupts_seip),
    .io_interrupts_lip_0(csr_io_interrupts_lip_0),
    .io_interrupts_lip_1(csr_io_interrupts_lip_1),
    .io_interrupts_lip_2(csr_io_interrupts_lip_2),
    .io_interrupts_lip_3(csr_io_interrupts_lip_3),
    .io_interrupts_lip_4(csr_io_interrupts_lip_4),
    .io_interrupts_lip_5(csr_io_interrupts_lip_5),
    .io_interrupts_lip_6(csr_io_interrupts_lip_6),
    .io_interrupts_lip_7(csr_io_interrupts_lip_7),
    .io_interrupts_lip_8(csr_io_interrupts_lip_8),
    .io_interrupts_lip_9(csr_io_interrupts_lip_9),
    .io_interrupts_lip_10(csr_io_interrupts_lip_10),
    .io_interrupts_lip_11(csr_io_interrupts_lip_11),
    .io_interrupts_lip_12(csr_io_interrupts_lip_12),
    .io_interrupts_lip_13(csr_io_interrupts_lip_13),
    .io_interrupts_lip_14(csr_io_interrupts_lip_14),
    .io_interrupts_lip_15(csr_io_interrupts_lip_15),
    .io_interrupts_nmi_rnmi(csr_io_interrupts_nmi_rnmi),
    .io_interrupts_nmi_rnmi_interrupt_vector(csr_io_interrupts_nmi_rnmi_interrupt_vector),
    .io_interrupts_nmi_rnmi_exception_vector(csr_io_interrupts_nmi_rnmi_exception_vector),
    .io_hartid(csr_io_hartid),
    .io_rw_addr(csr_io_rw_addr),
    .io_rw_cmd(csr_io_rw_cmd),
    .io_rw_rdata(csr_io_rw_rdata),
    .io_rw_wdata(csr_io_rw_wdata),
    .io_decode_0_csr(csr_io_decode_0_csr),
    .io_decode_0_fp_illegal(csr_io_decode_0_fp_illegal),
    .io_decode_0_fp_csr(csr_io_decode_0_fp_csr),
    .io_decode_0_read_illegal(csr_io_decode_0_read_illegal),
    .io_decode_0_write_illegal(csr_io_decode_0_write_illegal),
    .io_decode_0_write_flush(csr_io_decode_0_write_flush),
    .io_decode_0_system_illegal(csr_io_decode_0_system_illegal),
    .io_csr_stall(csr_io_csr_stall),
    .io_eret(csr_io_eret),
    .io_singleStep(csr_io_singleStep),
    .io_status_debug(csr_io_status_debug),
    .io_status_cease(csr_io_status_cease),
    .io_status_wfi(csr_io_status_wfi),
    .io_status_isa(csr_io_status_isa),
    .io_status_dprv(csr_io_status_dprv),
    .io_status_prv(csr_io_status_prv),
    .io_status_sd(csr_io_status_sd),
    .io_status_zero2(csr_io_status_zero2),
    .io_status_sxl(csr_io_status_sxl),
    .io_status_uxl(csr_io_status_uxl),
    .io_status_sd_rv32(csr_io_status_sd_rv32),
    .io_status_zero1(csr_io_status_zero1),
    .io_status_tsr(csr_io_status_tsr),
    .io_status_tw(csr_io_status_tw),
    .io_status_tvm(csr_io_status_tvm),
    .io_status_mxr(csr_io_status_mxr),
    .io_status_sum(csr_io_status_sum),
    .io_status_mprv(csr_io_status_mprv),
    .io_status_xs(csr_io_status_xs),
    .io_status_fs(csr_io_status_fs),
    .io_status_mpp(csr_io_status_mpp),
    .io_status_vs(csr_io_status_vs),
    .io_status_spp(csr_io_status_spp),
    .io_status_mpie(csr_io_status_mpie),
    .io_status_hpie(csr_io_status_hpie),
    .io_status_spie(csr_io_status_spie),
    .io_status_upie(csr_io_status_upie),
    .io_status_mie(csr_io_status_mie),
    .io_status_hie(csr_io_status_hie),
    .io_status_sie(csr_io_status_sie),
    .io_status_uie(csr_io_status_uie),
    .io_ptbr_mode(csr_io_ptbr_mode),
    .io_ptbr_ppn(csr_io_ptbr_ppn),
    .io_evec(csr_io_evec),
    .io_exception(csr_io_exception),
    .io_retire(csr_io_retire),
    .io_cause(csr_io_cause),
    .io_pc(csr_io_pc),
    .io_tval(csr_io_tval),
    .io_time(csr_io_time),
    .io_fcsr_rm(csr_io_fcsr_rm),
    .io_fcsr_flags_valid(csr_io_fcsr_flags_valid),
    .io_fcsr_flags_bits(csr_io_fcsr_flags_bits),
    .io_interrupt(csr_io_interrupt),
    .io_interrupt_cause(csr_io_interrupt_cause),
    .io_bp_0_control_action(csr_io_bp_0_control_action),
    .io_bp_0_control_chain(csr_io_bp_0_control_chain),
    .io_bp_0_control_tmatch(csr_io_bp_0_control_tmatch),
    .io_bp_0_control_m(csr_io_bp_0_control_m),
    .io_bp_0_control_s(csr_io_bp_0_control_s),
    .io_bp_0_control_u(csr_io_bp_0_control_u),
    .io_bp_0_control_x(csr_io_bp_0_control_x),
    .io_bp_0_control_w(csr_io_bp_0_control_w),
    .io_bp_0_control_r(csr_io_bp_0_control_r),
    .io_bp_0_address(csr_io_bp_0_address),
    .io_bp_1_control_action(csr_io_bp_1_control_action),
    .io_bp_1_control_chain(csr_io_bp_1_control_chain),
    .io_bp_1_control_tmatch(csr_io_bp_1_control_tmatch),
    .io_bp_1_control_m(csr_io_bp_1_control_m),
    .io_bp_1_control_s(csr_io_bp_1_control_s),
    .io_bp_1_control_u(csr_io_bp_1_control_u),
    .io_bp_1_control_x(csr_io_bp_1_control_x),
    .io_bp_1_control_w(csr_io_bp_1_control_w),
    .io_bp_1_control_r(csr_io_bp_1_control_r),
    .io_bp_1_address(csr_io_bp_1_address),
    .io_bp_2_control_action(csr_io_bp_2_control_action),
    .io_bp_2_control_chain(csr_io_bp_2_control_chain),
    .io_bp_2_control_tmatch(csr_io_bp_2_control_tmatch),
    .io_bp_2_control_m(csr_io_bp_2_control_m),
    .io_bp_2_control_s(csr_io_bp_2_control_s),
    .io_bp_2_control_u(csr_io_bp_2_control_u),
    .io_bp_2_control_x(csr_io_bp_2_control_x),
    .io_bp_2_control_w(csr_io_bp_2_control_w),
    .io_bp_2_control_r(csr_io_bp_2_control_r),
    .io_bp_2_address(csr_io_bp_2_address),
    .io_bp_3_control_action(csr_io_bp_3_control_action),
    .io_bp_3_control_chain(csr_io_bp_3_control_chain),
    .io_bp_3_control_tmatch(csr_io_bp_3_control_tmatch),
    .io_bp_3_control_m(csr_io_bp_3_control_m),
    .io_bp_3_control_s(csr_io_bp_3_control_s),
    .io_bp_3_control_u(csr_io_bp_3_control_u),
    .io_bp_3_control_x(csr_io_bp_3_control_x),
    .io_bp_3_control_w(csr_io_bp_3_control_w),
    .io_bp_3_control_r(csr_io_bp_3_control_r),
    .io_bp_3_address(csr_io_bp_3_address),
    .io_bp_4_control_action(csr_io_bp_4_control_action),
    .io_bp_4_control_chain(csr_io_bp_4_control_chain),
    .io_bp_4_control_tmatch(csr_io_bp_4_control_tmatch),
    .io_bp_4_control_m(csr_io_bp_4_control_m),
    .io_bp_4_control_s(csr_io_bp_4_control_s),
    .io_bp_4_control_u(csr_io_bp_4_control_u),
    .io_bp_4_control_x(csr_io_bp_4_control_x),
    .io_bp_4_control_w(csr_io_bp_4_control_w),
    .io_bp_4_control_r(csr_io_bp_4_control_r),
    .io_bp_4_address(csr_io_bp_4_address),
    .io_bp_5_control_action(csr_io_bp_5_control_action),
    .io_bp_5_control_chain(csr_io_bp_5_control_chain),
    .io_bp_5_control_tmatch(csr_io_bp_5_control_tmatch),
    .io_bp_5_control_m(csr_io_bp_5_control_m),
    .io_bp_5_control_s(csr_io_bp_5_control_s),
    .io_bp_5_control_u(csr_io_bp_5_control_u),
    .io_bp_5_control_x(csr_io_bp_5_control_x),
    .io_bp_5_control_w(csr_io_bp_5_control_w),
    .io_bp_5_control_r(csr_io_bp_5_control_r),
    .io_bp_5_address(csr_io_bp_5_address),
    .io_bp_6_control_action(csr_io_bp_6_control_action),
    .io_bp_6_control_chain(csr_io_bp_6_control_chain),
    .io_bp_6_control_tmatch(csr_io_bp_6_control_tmatch),
    .io_bp_6_control_m(csr_io_bp_6_control_m),
    .io_bp_6_control_s(csr_io_bp_6_control_s),
    .io_bp_6_control_u(csr_io_bp_6_control_u),
    .io_bp_6_control_x(csr_io_bp_6_control_x),
    .io_bp_6_control_w(csr_io_bp_6_control_w),
    .io_bp_6_control_r(csr_io_bp_6_control_r),
    .io_bp_6_address(csr_io_bp_6_address),
    .io_bp_7_control_action(csr_io_bp_7_control_action),
    .io_bp_7_control_chain(csr_io_bp_7_control_chain),
    .io_bp_7_control_tmatch(csr_io_bp_7_control_tmatch),
    .io_bp_7_control_m(csr_io_bp_7_control_m),
    .io_bp_7_control_s(csr_io_bp_7_control_s),
    .io_bp_7_control_u(csr_io_bp_7_control_u),
    .io_bp_7_control_x(csr_io_bp_7_control_x),
    .io_bp_7_control_w(csr_io_bp_7_control_w),
    .io_bp_7_control_r(csr_io_bp_7_control_r),
    .io_bp_7_address(csr_io_bp_7_address),
    .io_bp_8_control_action(csr_io_bp_8_control_action),
    .io_bp_8_control_chain(csr_io_bp_8_control_chain),
    .io_bp_8_control_tmatch(csr_io_bp_8_control_tmatch),
    .io_bp_8_control_m(csr_io_bp_8_control_m),
    .io_bp_8_control_s(csr_io_bp_8_control_s),
    .io_bp_8_control_u(csr_io_bp_8_control_u),
    .io_bp_8_control_x(csr_io_bp_8_control_x),
    .io_bp_8_control_w(csr_io_bp_8_control_w),
    .io_bp_8_control_r(csr_io_bp_8_control_r),
    .io_bp_8_address(csr_io_bp_8_address),
    .io_bp_9_control_action(csr_io_bp_9_control_action),
    .io_bp_9_control_chain(csr_io_bp_9_control_chain),
    .io_bp_9_control_tmatch(csr_io_bp_9_control_tmatch),
    .io_bp_9_control_m(csr_io_bp_9_control_m),
    .io_bp_9_control_s(csr_io_bp_9_control_s),
    .io_bp_9_control_u(csr_io_bp_9_control_u),
    .io_bp_9_control_x(csr_io_bp_9_control_x),
    .io_bp_9_control_w(csr_io_bp_9_control_w),
    .io_bp_9_control_r(csr_io_bp_9_control_r),
    .io_bp_9_address(csr_io_bp_9_address),
    .io_bp_10_control_action(csr_io_bp_10_control_action),
    .io_bp_10_control_chain(csr_io_bp_10_control_chain),
    .io_bp_10_control_tmatch(csr_io_bp_10_control_tmatch),
    .io_bp_10_control_m(csr_io_bp_10_control_m),
    .io_bp_10_control_s(csr_io_bp_10_control_s),
    .io_bp_10_control_u(csr_io_bp_10_control_u),
    .io_bp_10_control_x(csr_io_bp_10_control_x),
    .io_bp_10_control_w(csr_io_bp_10_control_w),
    .io_bp_10_control_r(csr_io_bp_10_control_r),
    .io_bp_10_address(csr_io_bp_10_address),
    .io_bp_11_control_action(csr_io_bp_11_control_action),
    .io_bp_11_control_chain(csr_io_bp_11_control_chain),
    .io_bp_11_control_tmatch(csr_io_bp_11_control_tmatch),
    .io_bp_11_control_m(csr_io_bp_11_control_m),
    .io_bp_11_control_s(csr_io_bp_11_control_s),
    .io_bp_11_control_u(csr_io_bp_11_control_u),
    .io_bp_11_control_x(csr_io_bp_11_control_x),
    .io_bp_11_control_w(csr_io_bp_11_control_w),
    .io_bp_11_control_r(csr_io_bp_11_control_r),
    .io_bp_11_address(csr_io_bp_11_address),
    .io_bp_12_control_action(csr_io_bp_12_control_action),
    .io_bp_12_control_chain(csr_io_bp_12_control_chain),
    .io_bp_12_control_tmatch(csr_io_bp_12_control_tmatch),
    .io_bp_12_control_m(csr_io_bp_12_control_m),
    .io_bp_12_control_s(csr_io_bp_12_control_s),
    .io_bp_12_control_u(csr_io_bp_12_control_u),
    .io_bp_12_control_x(csr_io_bp_12_control_x),
    .io_bp_12_control_w(csr_io_bp_12_control_w),
    .io_bp_12_control_r(csr_io_bp_12_control_r),
    .io_bp_12_address(csr_io_bp_12_address),
    .io_bp_13_control_action(csr_io_bp_13_control_action),
    .io_bp_13_control_chain(csr_io_bp_13_control_chain),
    .io_bp_13_control_tmatch(csr_io_bp_13_control_tmatch),
    .io_bp_13_control_m(csr_io_bp_13_control_m),
    .io_bp_13_control_s(csr_io_bp_13_control_s),
    .io_bp_13_control_u(csr_io_bp_13_control_u),
    .io_bp_13_control_x(csr_io_bp_13_control_x),
    .io_bp_13_control_w(csr_io_bp_13_control_w),
    .io_bp_13_control_r(csr_io_bp_13_control_r),
    .io_bp_13_address(csr_io_bp_13_address),
    .io_bp_14_control_action(csr_io_bp_14_control_action),
    .io_bp_14_control_chain(csr_io_bp_14_control_chain),
    .io_bp_14_control_tmatch(csr_io_bp_14_control_tmatch),
    .io_bp_14_control_m(csr_io_bp_14_control_m),
    .io_bp_14_control_s(csr_io_bp_14_control_s),
    .io_bp_14_control_u(csr_io_bp_14_control_u),
    .io_bp_14_control_x(csr_io_bp_14_control_x),
    .io_bp_14_control_w(csr_io_bp_14_control_w),
    .io_bp_14_control_r(csr_io_bp_14_control_r),
    .io_bp_14_address(csr_io_bp_14_address),
    .io_bp_15_control_action(csr_io_bp_15_control_action),
    .io_bp_15_control_tmatch(csr_io_bp_15_control_tmatch),
    .io_bp_15_control_m(csr_io_bp_15_control_m),
    .io_bp_15_control_s(csr_io_bp_15_control_s),
    .io_bp_15_control_u(csr_io_bp_15_control_u),
    .io_bp_15_control_x(csr_io_bp_15_control_x),
    .io_bp_15_control_w(csr_io_bp_15_control_w),
    .io_bp_15_control_r(csr_io_bp_15_control_r),
    .io_bp_15_address(csr_io_bp_15_address),
    .io_pmp_0_cfg_l(csr_io_pmp_0_cfg_l),
    .io_pmp_0_cfg_a(csr_io_pmp_0_cfg_a),
    .io_pmp_0_cfg_x(csr_io_pmp_0_cfg_x),
    .io_pmp_0_cfg_w(csr_io_pmp_0_cfg_w),
    .io_pmp_0_cfg_r(csr_io_pmp_0_cfg_r),
    .io_pmp_0_addr(csr_io_pmp_0_addr),
    .io_pmp_0_mask(csr_io_pmp_0_mask),
    .io_pmp_1_cfg_l(csr_io_pmp_1_cfg_l),
    .io_pmp_1_cfg_a(csr_io_pmp_1_cfg_a),
    .io_pmp_1_cfg_x(csr_io_pmp_1_cfg_x),
    .io_pmp_1_cfg_w(csr_io_pmp_1_cfg_w),
    .io_pmp_1_cfg_r(csr_io_pmp_1_cfg_r),
    .io_pmp_1_addr(csr_io_pmp_1_addr),
    .io_pmp_1_mask(csr_io_pmp_1_mask),
    .io_pmp_2_cfg_l(csr_io_pmp_2_cfg_l),
    .io_pmp_2_cfg_a(csr_io_pmp_2_cfg_a),
    .io_pmp_2_cfg_x(csr_io_pmp_2_cfg_x),
    .io_pmp_2_cfg_w(csr_io_pmp_2_cfg_w),
    .io_pmp_2_cfg_r(csr_io_pmp_2_cfg_r),
    .io_pmp_2_addr(csr_io_pmp_2_addr),
    .io_pmp_2_mask(csr_io_pmp_2_mask),
    .io_pmp_3_cfg_l(csr_io_pmp_3_cfg_l),
    .io_pmp_3_cfg_a(csr_io_pmp_3_cfg_a),
    .io_pmp_3_cfg_x(csr_io_pmp_3_cfg_x),
    .io_pmp_3_cfg_w(csr_io_pmp_3_cfg_w),
    .io_pmp_3_cfg_r(csr_io_pmp_3_cfg_r),
    .io_pmp_3_addr(csr_io_pmp_3_addr),
    .io_pmp_3_mask(csr_io_pmp_3_mask),
    .io_pmp_4_cfg_l(csr_io_pmp_4_cfg_l),
    .io_pmp_4_cfg_a(csr_io_pmp_4_cfg_a),
    .io_pmp_4_cfg_x(csr_io_pmp_4_cfg_x),
    .io_pmp_4_cfg_w(csr_io_pmp_4_cfg_w),
    .io_pmp_4_cfg_r(csr_io_pmp_4_cfg_r),
    .io_pmp_4_addr(csr_io_pmp_4_addr),
    .io_pmp_4_mask(csr_io_pmp_4_mask),
    .io_pmp_5_cfg_l(csr_io_pmp_5_cfg_l),
    .io_pmp_5_cfg_a(csr_io_pmp_5_cfg_a),
    .io_pmp_5_cfg_x(csr_io_pmp_5_cfg_x),
    .io_pmp_5_cfg_w(csr_io_pmp_5_cfg_w),
    .io_pmp_5_cfg_r(csr_io_pmp_5_cfg_r),
    .io_pmp_5_addr(csr_io_pmp_5_addr),
    .io_pmp_5_mask(csr_io_pmp_5_mask),
    .io_pmp_6_cfg_l(csr_io_pmp_6_cfg_l),
    .io_pmp_6_cfg_a(csr_io_pmp_6_cfg_a),
    .io_pmp_6_cfg_x(csr_io_pmp_6_cfg_x),
    .io_pmp_6_cfg_w(csr_io_pmp_6_cfg_w),
    .io_pmp_6_cfg_r(csr_io_pmp_6_cfg_r),
    .io_pmp_6_addr(csr_io_pmp_6_addr),
    .io_pmp_6_mask(csr_io_pmp_6_mask),
    .io_pmp_7_cfg_l(csr_io_pmp_7_cfg_l),
    .io_pmp_7_cfg_a(csr_io_pmp_7_cfg_a),
    .io_pmp_7_cfg_x(csr_io_pmp_7_cfg_x),
    .io_pmp_7_cfg_w(csr_io_pmp_7_cfg_w),
    .io_pmp_7_cfg_r(csr_io_pmp_7_cfg_r),
    .io_pmp_7_addr(csr_io_pmp_7_addr),
    .io_pmp_7_mask(csr_io_pmp_7_mask),
    .io_counters_0_eventSel(csr_io_counters_0_eventSel),
    .io_counters_0_inc(csr_io_counters_0_inc),
    .io_counters_1_eventSel(csr_io_counters_1_eventSel),
    .io_counters_1_inc(csr_io_counters_1_inc),
    .io_counters_2_eventSel(csr_io_counters_2_eventSel),
    .io_counters_2_inc(csr_io_counters_2_inc),
    .io_counters_3_eventSel(csr_io_counters_3_eventSel),
    .io_counters_3_inc(csr_io_counters_3_inc),
    .io_inhibit_cycle(csr_io_inhibit_cycle),
    .io_inst_0(csr_io_inst_0),
    .io_trace_0_valid(csr_io_trace_0_valid),
    .io_trace_0_iaddr(csr_io_trace_0_iaddr),
    .io_trace_0_insn(csr_io_trace_0_insn),
    .io_trace_0_priv(csr_io_trace_0_priv),
    .io_trace_0_exception(csr_io_trace_0_exception),
    .io_trace_0_interrupt(csr_io_trace_0_interrupt),
    .io_trace_0_cause(csr_io_trace_0_cause),
    .io_customCSRs_0_wen(csr_io_customCSRs_0_wen),
    .io_customCSRs_0_value(csr_io_customCSRs_0_value),
    .io_customCSRs_1_value(csr_io_customCSRs_1_value)
  );
  SCIEDecoder d ( // @[RocketCore.scala 292:19]
    .insn(d_insn),
    .unpipelined(d_unpipelined),
    .pipelined(d_pipelined),
    .multicycle(d_multicycle)
  );
  RHEA__BreakpointUnit bpu ( // @[RocketCore.scala 324:19]
    .io_status_debug(bpu_io_status_debug),
    .io_status_prv(bpu_io_status_prv),
    .io_bp_0_control_action(bpu_io_bp_0_control_action),
    .io_bp_0_control_chain(bpu_io_bp_0_control_chain),
    .io_bp_0_control_tmatch(bpu_io_bp_0_control_tmatch),
    .io_bp_0_control_m(bpu_io_bp_0_control_m),
    .io_bp_0_control_s(bpu_io_bp_0_control_s),
    .io_bp_0_control_u(bpu_io_bp_0_control_u),
    .io_bp_0_control_x(bpu_io_bp_0_control_x),
    .io_bp_0_control_w(bpu_io_bp_0_control_w),
    .io_bp_0_control_r(bpu_io_bp_0_control_r),
    .io_bp_0_address(bpu_io_bp_0_address),
    .io_bp_1_control_action(bpu_io_bp_1_control_action),
    .io_bp_1_control_chain(bpu_io_bp_1_control_chain),
    .io_bp_1_control_tmatch(bpu_io_bp_1_control_tmatch),
    .io_bp_1_control_m(bpu_io_bp_1_control_m),
    .io_bp_1_control_s(bpu_io_bp_1_control_s),
    .io_bp_1_control_u(bpu_io_bp_1_control_u),
    .io_bp_1_control_x(bpu_io_bp_1_control_x),
    .io_bp_1_control_w(bpu_io_bp_1_control_w),
    .io_bp_1_control_r(bpu_io_bp_1_control_r),
    .io_bp_1_address(bpu_io_bp_1_address),
    .io_bp_2_control_action(bpu_io_bp_2_control_action),
    .io_bp_2_control_chain(bpu_io_bp_2_control_chain),
    .io_bp_2_control_tmatch(bpu_io_bp_2_control_tmatch),
    .io_bp_2_control_m(bpu_io_bp_2_control_m),
    .io_bp_2_control_s(bpu_io_bp_2_control_s),
    .io_bp_2_control_u(bpu_io_bp_2_control_u),
    .io_bp_2_control_x(bpu_io_bp_2_control_x),
    .io_bp_2_control_w(bpu_io_bp_2_control_w),
    .io_bp_2_control_r(bpu_io_bp_2_control_r),
    .io_bp_2_address(bpu_io_bp_2_address),
    .io_bp_3_control_action(bpu_io_bp_3_control_action),
    .io_bp_3_control_chain(bpu_io_bp_3_control_chain),
    .io_bp_3_control_tmatch(bpu_io_bp_3_control_tmatch),
    .io_bp_3_control_m(bpu_io_bp_3_control_m),
    .io_bp_3_control_s(bpu_io_bp_3_control_s),
    .io_bp_3_control_u(bpu_io_bp_3_control_u),
    .io_bp_3_control_x(bpu_io_bp_3_control_x),
    .io_bp_3_control_w(bpu_io_bp_3_control_w),
    .io_bp_3_control_r(bpu_io_bp_3_control_r),
    .io_bp_3_address(bpu_io_bp_3_address),
    .io_bp_4_control_action(bpu_io_bp_4_control_action),
    .io_bp_4_control_chain(bpu_io_bp_4_control_chain),
    .io_bp_4_control_tmatch(bpu_io_bp_4_control_tmatch),
    .io_bp_4_control_m(bpu_io_bp_4_control_m),
    .io_bp_4_control_s(bpu_io_bp_4_control_s),
    .io_bp_4_control_u(bpu_io_bp_4_control_u),
    .io_bp_4_control_x(bpu_io_bp_4_control_x),
    .io_bp_4_control_w(bpu_io_bp_4_control_w),
    .io_bp_4_control_r(bpu_io_bp_4_control_r),
    .io_bp_4_address(bpu_io_bp_4_address),
    .io_bp_5_control_action(bpu_io_bp_5_control_action),
    .io_bp_5_control_chain(bpu_io_bp_5_control_chain),
    .io_bp_5_control_tmatch(bpu_io_bp_5_control_tmatch),
    .io_bp_5_control_m(bpu_io_bp_5_control_m),
    .io_bp_5_control_s(bpu_io_bp_5_control_s),
    .io_bp_5_control_u(bpu_io_bp_5_control_u),
    .io_bp_5_control_x(bpu_io_bp_5_control_x),
    .io_bp_5_control_w(bpu_io_bp_5_control_w),
    .io_bp_5_control_r(bpu_io_bp_5_control_r),
    .io_bp_5_address(bpu_io_bp_5_address),
    .io_bp_6_control_action(bpu_io_bp_6_control_action),
    .io_bp_6_control_chain(bpu_io_bp_6_control_chain),
    .io_bp_6_control_tmatch(bpu_io_bp_6_control_tmatch),
    .io_bp_6_control_m(bpu_io_bp_6_control_m),
    .io_bp_6_control_s(bpu_io_bp_6_control_s),
    .io_bp_6_control_u(bpu_io_bp_6_control_u),
    .io_bp_6_control_x(bpu_io_bp_6_control_x),
    .io_bp_6_control_w(bpu_io_bp_6_control_w),
    .io_bp_6_control_r(bpu_io_bp_6_control_r),
    .io_bp_6_address(bpu_io_bp_6_address),
    .io_bp_7_control_action(bpu_io_bp_7_control_action),
    .io_bp_7_control_chain(bpu_io_bp_7_control_chain),
    .io_bp_7_control_tmatch(bpu_io_bp_7_control_tmatch),
    .io_bp_7_control_m(bpu_io_bp_7_control_m),
    .io_bp_7_control_s(bpu_io_bp_7_control_s),
    .io_bp_7_control_u(bpu_io_bp_7_control_u),
    .io_bp_7_control_x(bpu_io_bp_7_control_x),
    .io_bp_7_control_w(bpu_io_bp_7_control_w),
    .io_bp_7_control_r(bpu_io_bp_7_control_r),
    .io_bp_7_address(bpu_io_bp_7_address),
    .io_bp_8_control_action(bpu_io_bp_8_control_action),
    .io_bp_8_control_chain(bpu_io_bp_8_control_chain),
    .io_bp_8_control_tmatch(bpu_io_bp_8_control_tmatch),
    .io_bp_8_control_m(bpu_io_bp_8_control_m),
    .io_bp_8_control_s(bpu_io_bp_8_control_s),
    .io_bp_8_control_u(bpu_io_bp_8_control_u),
    .io_bp_8_control_x(bpu_io_bp_8_control_x),
    .io_bp_8_control_w(bpu_io_bp_8_control_w),
    .io_bp_8_control_r(bpu_io_bp_8_control_r),
    .io_bp_8_address(bpu_io_bp_8_address),
    .io_bp_9_control_action(bpu_io_bp_9_control_action),
    .io_bp_9_control_chain(bpu_io_bp_9_control_chain),
    .io_bp_9_control_tmatch(bpu_io_bp_9_control_tmatch),
    .io_bp_9_control_m(bpu_io_bp_9_control_m),
    .io_bp_9_control_s(bpu_io_bp_9_control_s),
    .io_bp_9_control_u(bpu_io_bp_9_control_u),
    .io_bp_9_control_x(bpu_io_bp_9_control_x),
    .io_bp_9_control_w(bpu_io_bp_9_control_w),
    .io_bp_9_control_r(bpu_io_bp_9_control_r),
    .io_bp_9_address(bpu_io_bp_9_address),
    .io_bp_10_control_action(bpu_io_bp_10_control_action),
    .io_bp_10_control_chain(bpu_io_bp_10_control_chain),
    .io_bp_10_control_tmatch(bpu_io_bp_10_control_tmatch),
    .io_bp_10_control_m(bpu_io_bp_10_control_m),
    .io_bp_10_control_s(bpu_io_bp_10_control_s),
    .io_bp_10_control_u(bpu_io_bp_10_control_u),
    .io_bp_10_control_x(bpu_io_bp_10_control_x),
    .io_bp_10_control_w(bpu_io_bp_10_control_w),
    .io_bp_10_control_r(bpu_io_bp_10_control_r),
    .io_bp_10_address(bpu_io_bp_10_address),
    .io_bp_11_control_action(bpu_io_bp_11_control_action),
    .io_bp_11_control_chain(bpu_io_bp_11_control_chain),
    .io_bp_11_control_tmatch(bpu_io_bp_11_control_tmatch),
    .io_bp_11_control_m(bpu_io_bp_11_control_m),
    .io_bp_11_control_s(bpu_io_bp_11_control_s),
    .io_bp_11_control_u(bpu_io_bp_11_control_u),
    .io_bp_11_control_x(bpu_io_bp_11_control_x),
    .io_bp_11_control_w(bpu_io_bp_11_control_w),
    .io_bp_11_control_r(bpu_io_bp_11_control_r),
    .io_bp_11_address(bpu_io_bp_11_address),
    .io_bp_12_control_action(bpu_io_bp_12_control_action),
    .io_bp_12_control_chain(bpu_io_bp_12_control_chain),
    .io_bp_12_control_tmatch(bpu_io_bp_12_control_tmatch),
    .io_bp_12_control_m(bpu_io_bp_12_control_m),
    .io_bp_12_control_s(bpu_io_bp_12_control_s),
    .io_bp_12_control_u(bpu_io_bp_12_control_u),
    .io_bp_12_control_x(bpu_io_bp_12_control_x),
    .io_bp_12_control_w(bpu_io_bp_12_control_w),
    .io_bp_12_control_r(bpu_io_bp_12_control_r),
    .io_bp_12_address(bpu_io_bp_12_address),
    .io_bp_13_control_action(bpu_io_bp_13_control_action),
    .io_bp_13_control_chain(bpu_io_bp_13_control_chain),
    .io_bp_13_control_tmatch(bpu_io_bp_13_control_tmatch),
    .io_bp_13_control_m(bpu_io_bp_13_control_m),
    .io_bp_13_control_s(bpu_io_bp_13_control_s),
    .io_bp_13_control_u(bpu_io_bp_13_control_u),
    .io_bp_13_control_x(bpu_io_bp_13_control_x),
    .io_bp_13_control_w(bpu_io_bp_13_control_w),
    .io_bp_13_control_r(bpu_io_bp_13_control_r),
    .io_bp_13_address(bpu_io_bp_13_address),
    .io_bp_14_control_action(bpu_io_bp_14_control_action),
    .io_bp_14_control_chain(bpu_io_bp_14_control_chain),
    .io_bp_14_control_tmatch(bpu_io_bp_14_control_tmatch),
    .io_bp_14_control_m(bpu_io_bp_14_control_m),
    .io_bp_14_control_s(bpu_io_bp_14_control_s),
    .io_bp_14_control_u(bpu_io_bp_14_control_u),
    .io_bp_14_control_x(bpu_io_bp_14_control_x),
    .io_bp_14_control_w(bpu_io_bp_14_control_w),
    .io_bp_14_control_r(bpu_io_bp_14_control_r),
    .io_bp_14_address(bpu_io_bp_14_address),
    .io_bp_15_control_action(bpu_io_bp_15_control_action),
    .io_bp_15_control_tmatch(bpu_io_bp_15_control_tmatch),
    .io_bp_15_control_m(bpu_io_bp_15_control_m),
    .io_bp_15_control_s(bpu_io_bp_15_control_s),
    .io_bp_15_control_u(bpu_io_bp_15_control_u),
    .io_bp_15_control_x(bpu_io_bp_15_control_x),
    .io_bp_15_control_w(bpu_io_bp_15_control_w),
    .io_bp_15_control_r(bpu_io_bp_15_control_r),
    .io_bp_15_address(bpu_io_bp_15_address),
    .io_pc(bpu_io_pc),
    .io_ea(bpu_io_ea),
    .io_xcpt_if(bpu_io_xcpt_if),
    .io_xcpt_ld(bpu_io_xcpt_ld),
    .io_xcpt_st(bpu_io_xcpt_st),
    .io_debug_if(bpu_io_debug_if),
    .io_debug_ld(bpu_io_debug_ld),
    .io_debug_st(bpu_io_debug_st),
    .io_bpwatch_0_rvalid_0(bpu_io_bpwatch_0_rvalid_0),
    .io_bpwatch_0_wvalid_0(bpu_io_bpwatch_0_wvalid_0),
    .io_bpwatch_0_ivalid_0(bpu_io_bpwatch_0_ivalid_0),
    .io_bpwatch_1_rvalid_0(bpu_io_bpwatch_1_rvalid_0),
    .io_bpwatch_1_wvalid_0(bpu_io_bpwatch_1_wvalid_0),
    .io_bpwatch_1_ivalid_0(bpu_io_bpwatch_1_ivalid_0),
    .io_bpwatch_2_rvalid_0(bpu_io_bpwatch_2_rvalid_0),
    .io_bpwatch_2_wvalid_0(bpu_io_bpwatch_2_wvalid_0),
    .io_bpwatch_2_ivalid_0(bpu_io_bpwatch_2_ivalid_0),
    .io_bpwatch_3_rvalid_0(bpu_io_bpwatch_3_rvalid_0),
    .io_bpwatch_3_wvalid_0(bpu_io_bpwatch_3_wvalid_0),
    .io_bpwatch_3_ivalid_0(bpu_io_bpwatch_3_ivalid_0),
    .io_bpwatch_4_rvalid_0(bpu_io_bpwatch_4_rvalid_0),
    .io_bpwatch_4_wvalid_0(bpu_io_bpwatch_4_wvalid_0),
    .io_bpwatch_4_ivalid_0(bpu_io_bpwatch_4_ivalid_0),
    .io_bpwatch_5_rvalid_0(bpu_io_bpwatch_5_rvalid_0),
    .io_bpwatch_5_wvalid_0(bpu_io_bpwatch_5_wvalid_0),
    .io_bpwatch_5_ivalid_0(bpu_io_bpwatch_5_ivalid_0),
    .io_bpwatch_6_rvalid_0(bpu_io_bpwatch_6_rvalid_0),
    .io_bpwatch_6_wvalid_0(bpu_io_bpwatch_6_wvalid_0),
    .io_bpwatch_6_ivalid_0(bpu_io_bpwatch_6_ivalid_0),
    .io_bpwatch_7_rvalid_0(bpu_io_bpwatch_7_rvalid_0),
    .io_bpwatch_7_wvalid_0(bpu_io_bpwatch_7_wvalid_0),
    .io_bpwatch_7_ivalid_0(bpu_io_bpwatch_7_ivalid_0),
    .io_bpwatch_8_rvalid_0(bpu_io_bpwatch_8_rvalid_0),
    .io_bpwatch_8_wvalid_0(bpu_io_bpwatch_8_wvalid_0),
    .io_bpwatch_8_ivalid_0(bpu_io_bpwatch_8_ivalid_0),
    .io_bpwatch_9_rvalid_0(bpu_io_bpwatch_9_rvalid_0),
    .io_bpwatch_9_wvalid_0(bpu_io_bpwatch_9_wvalid_0),
    .io_bpwatch_9_ivalid_0(bpu_io_bpwatch_9_ivalid_0),
    .io_bpwatch_10_rvalid_0(bpu_io_bpwatch_10_rvalid_0),
    .io_bpwatch_10_wvalid_0(bpu_io_bpwatch_10_wvalid_0),
    .io_bpwatch_10_ivalid_0(bpu_io_bpwatch_10_ivalid_0),
    .io_bpwatch_11_rvalid_0(bpu_io_bpwatch_11_rvalid_0),
    .io_bpwatch_11_wvalid_0(bpu_io_bpwatch_11_wvalid_0),
    .io_bpwatch_11_ivalid_0(bpu_io_bpwatch_11_ivalid_0),
    .io_bpwatch_12_rvalid_0(bpu_io_bpwatch_12_rvalid_0),
    .io_bpwatch_12_wvalid_0(bpu_io_bpwatch_12_wvalid_0),
    .io_bpwatch_12_ivalid_0(bpu_io_bpwatch_12_ivalid_0),
    .io_bpwatch_13_rvalid_0(bpu_io_bpwatch_13_rvalid_0),
    .io_bpwatch_13_wvalid_0(bpu_io_bpwatch_13_wvalid_0),
    .io_bpwatch_13_ivalid_0(bpu_io_bpwatch_13_ivalid_0),
    .io_bpwatch_14_rvalid_0(bpu_io_bpwatch_14_rvalid_0),
    .io_bpwatch_14_wvalid_0(bpu_io_bpwatch_14_wvalid_0),
    .io_bpwatch_14_ivalid_0(bpu_io_bpwatch_14_ivalid_0),
    .io_bpwatch_15_rvalid_0(bpu_io_bpwatch_15_rvalid_0),
    .io_bpwatch_15_wvalid_0(bpu_io_bpwatch_15_wvalid_0),
    .io_bpwatch_15_ivalid_0(bpu_io_bpwatch_15_ivalid_0)
  );
  RHEA__ALU alu ( // @[RocketCore.scala 386:19]
    .io_dw(alu_io_dw),
    .io_fn(alu_io_fn),
    .io_in2(alu_io_in2),
    .io_in1(alu_io_in1),
    .io_out(alu_io_out),
    .io_adder_out(alu_io_adder_out),
    .io_cmp_out(alu_io_cmp_out)
  );
  SCIEUnpipelined #(.XLEN(64)) u ( // @[RocketCore.scala 393:19]
    .insn(u_insn),
    .rs1(u_rs1),
    .rs2(u_rs2),
    .rd(u_rd)
  );
  SCIEPipelined #(.XLEN(64)) u_1 ( // @[RocketCore.scala 400:19]
    .clock(u_1_clock),
    .valid(u_1_valid),
    .insn(u_1_insn),
    .rs1(u_1_rs1),
    .rs2(u_1_rs2),
    .rd(u_1_rd)
  );
  RHEA__MulDiv div ( // @[RocketCore.scala 410:19]
    .rf_reset(div_rf_reset),
    .clock(div_clock),
    .reset(div_reset),
    .io_req_ready(div_io_req_ready),
    .io_req_valid(div_io_req_valid),
    .io_req_bits_fn(div_io_req_bits_fn),
    .io_req_bits_dw(div_io_req_bits_dw),
    .io_req_bits_in1(div_io_req_bits_in1),
    .io_req_bits_in2(div_io_req_bits_in2),
    .io_req_bits_tag(div_io_req_bits_tag),
    .io_kill(div_io_kill),
    .io_resp_ready(div_io_resp_ready),
    .io_resp_valid(div_io_resp_valid),
    .io_resp_bits_data(div_io_resp_bits_data),
    .io_resp_bits_tag(div_io_resp_bits_tag)
  );
  RHEA__PipelinedMultiplier m ( // @[RocketCore.scala 418:19]
    .rf_reset(m_rf_reset),
    .clock(m_clock),
    .reset(m_reset),
    .io_req_valid(m_io_req_valid),
    .io_req_bits_fn(m_io_req_bits_fn),
    .io_req_bits_dw(m_io_req_bits_dw),
    .io_req_bits_in1(m_io_req_bits_in1),
    .io_req_bits_in2(m_io_req_bits_in2),
    .io_resp_bits_data(m_io_resp_bits_data)
  );
  assign ibuf_rf_reset = rf_reset;
  assign rf_id_rs_MPORT_addr = ~id_raddr1;
  `ifndef RANDOMIZE_GARBAGE_ASSIGN
  assign rf_id_rs_MPORT_data = rf[rf_id_rs_MPORT_addr]; // @[RocketCore.scala 1017:15]
  `else
  assign rf_id_rs_MPORT_data = rf_id_rs_MPORT_addr >= 5'h1f ? _RAND_1[63:0] : rf[rf_id_rs_MPORT_addr]; // @[RocketCore.scala 1017:15]
  `endif // RANDOMIZE_GARBAGE_ASSIGN
  assign rf_id_rs_MPORT_1_addr = ~id_raddr2;
  `ifndef RANDOMIZE_GARBAGE_ASSIGN
  assign rf_id_rs_MPORT_1_data = rf[rf_id_rs_MPORT_1_addr]; // @[RocketCore.scala 1017:15]
  `else
  assign rf_id_rs_MPORT_1_data = rf_id_rs_MPORT_1_addr >= 5'h1f ? _RAND_2[63:0] : rf[rf_id_rs_MPORT_1_addr]; // @[RocketCore.scala 1017:15]
  `endif // RANDOMIZE_GARBAGE_ASSIGN
  assign rf_MPORT_data = _rf_wdata_T ? io_dmem_resp_bits_data : _rf_wdata_T_5;
  assign rf_MPORT_addr = ~rf_waddr;
  assign rf_MPORT_mask = 1'h1;
  assign rf_MPORT_en = rf_wen & _T_222;
  assign csr_rf_reset = rf_reset;
  assign div_rf_reset = rf_reset;
  assign m_rf_reset = rf_reset;
  assign io_imem_might_request = imem_might_request_reg; // @[RocketCore.scala 794:25]
  assign io_imem_req_valid = take_pc_wb | take_pc_mem; // @[RocketCore.scala 251:35]
  assign io_imem_req_bits_pc = wb_xcpt | csr_io_eret ? csr_io_evec : _io_imem_req_bits_pc_T_1; // @[RocketCore.scala 790:8]
  assign io_imem_req_bits_speculative = ~take_pc_wb; // @[RocketCore.scala 788:35]
  assign io_imem_sfence_valid = wb_reg_valid & wb_reg_sfence; // @[RocketCore.scala 798:40]
  assign io_imem_sfence_bits_rs1 = wb_reg_mem_size[0]; // @[RocketCore.scala 799:45]
  assign io_imem_sfence_bits_rs2 = wb_reg_mem_size[1]; // @[RocketCore.scala 800:45]
  assign io_imem_sfence_bits_addr = wb_reg_wdata[38:0]; // @[RocketCore.scala 801:28]
  assign io_imem_sfence_bits_asid = wb_reg_rs2[0]; // @[RocketCore.scala 802:28]
  assign io_imem_resp_ready = ibuf_io_imem_ready; // @[RocketCore.scala 259:16]
  assign io_imem_btb_update_valid = mem_reg_valid & _wb_reg_replay_T & mem_wrong_npc & (~mem_cfi | mem_cfi_taken); // @[RocketCore.scala 807:77]
  assign io_imem_btb_update_bits_prediction_entry = mem_reg_btb_resp_entry; // @[RocketCore.scala 817:38]
  assign io_imem_btb_update_bits_pc = ~_io_imem_btb_update_bits_pc_T_1; // @[RocketCore.scala 816:33]
  assign io_imem_btb_update_bits_isValid = mem_ctrl_branch | mem_ctrl_jalr | mem_ctrl_jal; // @[RocketCore.scala 520:50]
  assign io_imem_btb_update_bits_br_pc = _io_imem_btb_update_bits_br_pc_T_2[38:0]; // @[RocketCore.scala 815:33]
  assign io_imem_btb_update_bits_cfiType = (mem_ctrl_jal | mem_ctrl_jalr) & mem_waddr[0] ? 2'h2 :
    _io_imem_btb_update_bits_cfiType_T_10; // @[RocketCore.scala 810:8]
  assign io_imem_bht_update_valid = mem_reg_valid & _wb_reg_replay_T; // @[RocketCore.scala 819:45]
  assign io_imem_bht_update_bits_prediction_history = mem_reg_btb_resp_bht_history; // @[RocketCore.scala 824:38]
  assign io_imem_bht_update_bits_pc = io_imem_btb_update_bits_pc; // @[RocketCore.scala 820:30]
  assign io_imem_bht_update_bits_branch = mem_ctrl_branch; // @[RocketCore.scala 823:34]
  assign io_imem_bht_update_bits_taken = mem_br_taken; // @[RocketCore.scala 821:33]
  assign io_imem_bht_update_bits_mispredict = ex_pc_valid ? mem_npc != ex_reg_pc : _mem_wrong_npc_T_3; // @[RocketCore.scala 516:8]
  assign io_imem_flush_icache = wb_reg_valid & wb_ctrl_fence_i & ~io_dmem_s2_nack; // @[RocketCore.scala 793:59]
  assign io_dmem_req_valid = ex_reg_valid & ex_ctrl_mem; // @[RocketCore.scala 837:41]
  assign io_dmem_req_bits_addr = {msb_2,io_dmem_req_bits_addr_lo}; // @[Cat.scala 30:58]
  assign io_dmem_req_bits_idx = io_dmem_req_bits_addr; // @[RocketCore.scala 846:34]
  assign io_dmem_req_bits_tag = {{1'd0}, ex_dcache_tag}; // @[Cat.scala 30:58]
  assign io_dmem_req_bits_cmd = ex_ctrl_mem_cmd; // @[RocketCore.scala 841:25]
  assign io_dmem_req_bits_size = ex_reg_mem_size; // @[RocketCore.scala 842:25]
  assign io_dmem_req_bits_signed = ~ex_reg_inst[14]; // @[RocketCore.scala 843:30]
  assign io_dmem_req_bits_dprv = csr_io_status_dprv; // @[RocketCore.scala 847:25]
  assign io_dmem_req_bits_phys = 1'h0; // @[RocketCore.scala 844:25]
  assign io_dmem_req_bits_no_alloc = 1'h0;
  assign io_dmem_req_bits_no_xcpt = 1'h0;
  assign io_dmem_req_bits_data = 64'h0;
  assign io_dmem_req_bits_mask = 8'h0;
  assign io_dmem_s1_kill = killm_common | mem_ldst_xcpt | fpu_kill_mem; // @[RocketCore.scala 849:52]
  assign io_dmem_s1_data_data = mem_ctrl_fp ? io_fpu_store_data : mem_reg_rs2; // @[RocketCore.scala 848:63]
  assign io_dmem_s1_data_mask = 8'h0;
  assign io_dmem_keep_clock_enabled = ibuf_io_inst_0_valid & id_ctrl_decoder_14 & ~csr_io_csr_stall; // @[RocketCore.scala 852:70]
  assign io_ptw_ptbr_mode = csr_io_ptbr_mode; // @[RocketCore.scala 702:15]
  assign io_ptw_ptbr_ppn = csr_io_ptbr_ppn; // @[RocketCore.scala 702:15]
  assign io_ptw_sfence_valid = io_imem_sfence_valid; // @[RocketCore.scala 803:17]
  assign io_ptw_sfence_bits_rs1 = io_imem_sfence_bits_rs1; // @[RocketCore.scala 803:17]
  assign io_ptw_sfence_bits_rs2 = io_imem_sfence_bits_rs2; // @[RocketCore.scala 803:17]
  assign io_ptw_sfence_bits_addr = io_imem_sfence_bits_addr; // @[RocketCore.scala 803:17]
  assign io_ptw_status_debug = csr_io_status_debug; // @[RocketCore.scala 704:17]
  assign io_ptw_status_dprv = csr_io_status_dprv; // @[RocketCore.scala 704:17]
  assign io_ptw_status_prv = csr_io_status_prv; // @[RocketCore.scala 704:17]
  assign io_ptw_status_mxr = csr_io_status_mxr; // @[RocketCore.scala 704:17]
  assign io_ptw_status_sum = csr_io_status_sum; // @[RocketCore.scala 704:17]
  assign io_ptw_pmp_0_cfg_l = csr_io_pmp_0_cfg_l; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_0_cfg_a = csr_io_pmp_0_cfg_a; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_0_cfg_x = csr_io_pmp_0_cfg_x; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_0_cfg_w = csr_io_pmp_0_cfg_w; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_0_cfg_r = csr_io_pmp_0_cfg_r; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_0_addr = csr_io_pmp_0_addr; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_0_mask = csr_io_pmp_0_mask; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_1_cfg_l = csr_io_pmp_1_cfg_l; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_1_cfg_a = csr_io_pmp_1_cfg_a; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_1_cfg_x = csr_io_pmp_1_cfg_x; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_1_cfg_w = csr_io_pmp_1_cfg_w; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_1_cfg_r = csr_io_pmp_1_cfg_r; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_1_addr = csr_io_pmp_1_addr; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_1_mask = csr_io_pmp_1_mask; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_2_cfg_l = csr_io_pmp_2_cfg_l; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_2_cfg_a = csr_io_pmp_2_cfg_a; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_2_cfg_x = csr_io_pmp_2_cfg_x; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_2_cfg_w = csr_io_pmp_2_cfg_w; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_2_cfg_r = csr_io_pmp_2_cfg_r; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_2_addr = csr_io_pmp_2_addr; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_2_mask = csr_io_pmp_2_mask; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_3_cfg_l = csr_io_pmp_3_cfg_l; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_3_cfg_a = csr_io_pmp_3_cfg_a; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_3_cfg_x = csr_io_pmp_3_cfg_x; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_3_cfg_w = csr_io_pmp_3_cfg_w; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_3_cfg_r = csr_io_pmp_3_cfg_r; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_3_addr = csr_io_pmp_3_addr; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_3_mask = csr_io_pmp_3_mask; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_4_cfg_l = csr_io_pmp_4_cfg_l; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_4_cfg_a = csr_io_pmp_4_cfg_a; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_4_cfg_x = csr_io_pmp_4_cfg_x; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_4_cfg_w = csr_io_pmp_4_cfg_w; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_4_cfg_r = csr_io_pmp_4_cfg_r; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_4_addr = csr_io_pmp_4_addr; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_4_mask = csr_io_pmp_4_mask; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_5_cfg_l = csr_io_pmp_5_cfg_l; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_5_cfg_a = csr_io_pmp_5_cfg_a; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_5_cfg_x = csr_io_pmp_5_cfg_x; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_5_cfg_w = csr_io_pmp_5_cfg_w; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_5_cfg_r = csr_io_pmp_5_cfg_r; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_5_addr = csr_io_pmp_5_addr; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_5_mask = csr_io_pmp_5_mask; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_6_cfg_l = csr_io_pmp_6_cfg_l; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_6_cfg_a = csr_io_pmp_6_cfg_a; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_6_cfg_x = csr_io_pmp_6_cfg_x; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_6_cfg_w = csr_io_pmp_6_cfg_w; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_6_cfg_r = csr_io_pmp_6_cfg_r; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_6_addr = csr_io_pmp_6_addr; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_6_mask = csr_io_pmp_6_mask; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_7_cfg_l = csr_io_pmp_7_cfg_l; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_7_cfg_a = csr_io_pmp_7_cfg_a; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_7_cfg_x = csr_io_pmp_7_cfg_x; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_7_cfg_w = csr_io_pmp_7_cfg_w; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_7_cfg_r = csr_io_pmp_7_cfg_r; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_7_addr = csr_io_pmp_7_addr; // @[RocketCore.scala 705:14]
  assign io_ptw_pmp_7_mask = csr_io_pmp_7_mask; // @[RocketCore.scala 705:14]
  assign io_ptw_customCSRs_csrs_0_wen = csr_io_customCSRs_0_wen; // @[RocketCore.scala 703:79]
  assign io_ptw_customCSRs_csrs_0_value = csr_io_customCSRs_0_value; // @[RocketCore.scala 703:79]
  assign io_ptw_customCSRs_csrs_1_value = csr_io_customCSRs_1_value; // @[RocketCore.scala 703:79]
  assign io_fpu_inst = ibuf_io_inst_0_bits_inst_bits; // @[RocketCore.scala 829:15]
  assign io_fpu_fromint_data = ex_reg_rs_bypass_0 ? _ex_rs_T_5 : _ex_rs_T_6; // @[RocketCore.scala 376:14]
  assign io_fpu_fcsr_rm = csr_io_fcsr_rm; // @[RocketCore.scala 691:18]
  assign io_fpu_dmem_resp_val = dmem_resp_valid & io_dmem_resp_bits_tag[0]; // @[RocketCore.scala 831:43]
  assign io_fpu_dmem_resp_type = {{1'd0}, io_dmem_resp_bits_size}; // @[RocketCore.scala 833:25]
  assign io_fpu_dmem_resp_tag = io_dmem_resp_bits_tag[5:1]; // @[RocketCore.scala 646:46]
  assign io_fpu_dmem_resp_data = io_dmem_resp_bits_data_word_bypass; // @[RocketCore.scala 832:25]
  assign io_fpu_valid = _ex_reg_valid_T & id_ctrl_decoder_1; // @[RocketCore.scala 826:31]
  assign io_fpu_killx = take_pc_mem_wb | replay_ex | ~ex_reg_valid; // @[RocketCore.scala 497:48]
  assign io_fpu_killm = dcache_kill_mem | take_pc_wb | mem_reg_xcpt | ~mem_reg_valid; // @[RocketCore.scala 588:68]
  assign io_fpu_keep_clock_enabled = io_ptw_customCSRs_csrs_1_value[2]; // @[CustomCSRs.scala 39:59]
  assign io_trace_0_valid = csr_io_trace_0_valid; // @[RocketCore.scala 709:12]
  assign io_trace_0_iaddr = csr_io_trace_0_iaddr; // @[RocketCore.scala 709:12]
  assign io_trace_0_insn = csr_io_trace_0_insn; // @[RocketCore.scala 709:12]
  assign io_trace_0_priv = csr_io_trace_0_priv; // @[RocketCore.scala 709:12]
  assign io_trace_0_exception = csr_io_trace_0_exception; // @[RocketCore.scala 709:12]
  assign io_trace_0_interrupt = csr_io_trace_0_interrupt; // @[RocketCore.scala 709:12]
  assign io_bpwatch_0_valid_0 = wb_reg_wphit_0; // @[RocketCore.scala 711:20]
  assign io_bpwatch_0_action = csr_io_bp_0_control_action; // @[RocketCore.scala 712:18]
  assign io_bpwatch_1_valid_0 = wb_reg_wphit_1; // @[RocketCore.scala 711:20]
  assign io_bpwatch_1_action = csr_io_bp_1_control_action; // @[RocketCore.scala 712:18]
  assign io_bpwatch_2_valid_0 = wb_reg_wphit_2; // @[RocketCore.scala 711:20]
  assign io_bpwatch_2_action = csr_io_bp_2_control_action; // @[RocketCore.scala 712:18]
  assign io_bpwatch_3_valid_0 = wb_reg_wphit_3; // @[RocketCore.scala 711:20]
  assign io_bpwatch_3_action = csr_io_bp_3_control_action; // @[RocketCore.scala 712:18]
  assign io_bpwatch_4_valid_0 = wb_reg_wphit_4; // @[RocketCore.scala 711:20]
  assign io_bpwatch_4_action = csr_io_bp_4_control_action; // @[RocketCore.scala 712:18]
  assign io_bpwatch_5_valid_0 = wb_reg_wphit_5; // @[RocketCore.scala 711:20]
  assign io_bpwatch_5_action = csr_io_bp_5_control_action; // @[RocketCore.scala 712:18]
  assign io_bpwatch_6_valid_0 = wb_reg_wphit_6; // @[RocketCore.scala 711:20]
  assign io_bpwatch_6_action = csr_io_bp_6_control_action; // @[RocketCore.scala 712:18]
  assign io_bpwatch_7_valid_0 = wb_reg_wphit_7; // @[RocketCore.scala 711:20]
  assign io_bpwatch_7_action = csr_io_bp_7_control_action; // @[RocketCore.scala 712:18]
  assign io_bpwatch_8_valid_0 = wb_reg_wphit_8; // @[RocketCore.scala 711:20]
  assign io_bpwatch_8_action = csr_io_bp_8_control_action; // @[RocketCore.scala 712:18]
  assign io_bpwatch_9_valid_0 = wb_reg_wphit_9; // @[RocketCore.scala 711:20]
  assign io_bpwatch_9_action = csr_io_bp_9_control_action; // @[RocketCore.scala 712:18]
  assign io_bpwatch_10_valid_0 = wb_reg_wphit_10; // @[RocketCore.scala 711:20]
  assign io_bpwatch_10_action = csr_io_bp_10_control_action; // @[RocketCore.scala 712:18]
  assign io_bpwatch_11_valid_0 = wb_reg_wphit_11; // @[RocketCore.scala 711:20]
  assign io_bpwatch_11_action = csr_io_bp_11_control_action; // @[RocketCore.scala 712:18]
  assign io_bpwatch_12_valid_0 = wb_reg_wphit_12; // @[RocketCore.scala 711:20]
  assign io_bpwatch_12_action = csr_io_bp_12_control_action; // @[RocketCore.scala 712:18]
  assign io_bpwatch_13_valid_0 = wb_reg_wphit_13; // @[RocketCore.scala 711:20]
  assign io_bpwatch_13_action = csr_io_bp_13_control_action; // @[RocketCore.scala 712:18]
  assign io_bpwatch_14_valid_0 = wb_reg_wphit_14; // @[RocketCore.scala 711:20]
  assign io_bpwatch_14_action = csr_io_bp_14_control_action; // @[RocketCore.scala 712:18]
  assign io_bpwatch_15_valid_0 = wb_reg_wphit_15; // @[RocketCore.scala 711:20]
  assign io_bpwatch_15_action = csr_io_bp_15_control_action; // @[RocketCore.scala 712:18]
  assign io_cease = csr_io_status_cease & ~clock_en_reg; // @[RocketCore.scala 864:35]
  assign io_wfi = csr_io_status_wfi; // @[RocketCore.scala 865:10]
  assign io_debug = csr_io_status_debug; // @[RocketCore.scala 866:12]
  assign gated_clock_rocket_clock_gate_in = clock; // @[ClockGate.scala 26:14]
  assign gated_clock_rocket_clock_gate_test_en = psd_test_clock_enable;
  assign gated_clock_rocket_clock_gate_en = clock_en_reg | ex_pc_valid | ~long_latency_stall & io_imem_resp_valid; // @[RocketCore.scala 869:45]
  assign ibuf_clock = gated_clock_rocket_clock_gate_out;
  assign ibuf_reset = reset;
  assign ibuf_io_imem_valid = io_imem_resp_valid; // @[RocketCore.scala 259:16]
  assign ibuf_io_imem_bits_btb_taken = io_imem_resp_bits_btb_taken; // @[RocketCore.scala 259:16]
  assign ibuf_io_imem_bits_btb_bridx = io_imem_resp_bits_btb_bridx; // @[RocketCore.scala 259:16]
  assign ibuf_io_imem_bits_btb_entry = io_imem_resp_bits_btb_entry; // @[RocketCore.scala 259:16]
  assign ibuf_io_imem_bits_btb_bht_history = io_imem_resp_bits_btb_bht_history; // @[RocketCore.scala 259:16]
  assign ibuf_io_imem_bits_pc = io_imem_resp_bits_pc; // @[RocketCore.scala 259:16]
  assign ibuf_io_imem_bits_data = io_imem_resp_bits_data; // @[RocketCore.scala 259:16]
  assign ibuf_io_imem_bits_xcpt_pf_inst = io_imem_resp_bits_xcpt_pf_inst; // @[RocketCore.scala 259:16]
  assign ibuf_io_imem_bits_xcpt_ae_inst = io_imem_resp_bits_xcpt_ae_inst; // @[RocketCore.scala 259:16]
  assign ibuf_io_imem_bits_replay = io_imem_resp_bits_replay; // @[RocketCore.scala 259:16]
  assign ibuf_io_kill = take_pc_wb | take_pc_mem; // @[RocketCore.scala 251:35]
  assign ibuf_io_inst_0_ready = ~ctrl_stalld; // @[RocketCore.scala 805:28]
  assign csr_clock = gated_clock_rocket_clock_gate_out;
  assign csr_reset = reset;
  assign csr_io_ungated_clock = clock; // @[RocketCore.scala 683:24]
  assign csr_io_interrupts_debug = io_interrupts_debug; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_mtip = io_interrupts_mtip; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_msip = io_interrupts_msip; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_meip = io_interrupts_meip; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_seip = io_interrupts_seip; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_0 = io_interrupts_lip_0; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_1 = io_interrupts_lip_1; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_2 = io_interrupts_lip_2; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_3 = io_interrupts_lip_3; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_4 = io_interrupts_lip_4; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_5 = io_interrupts_lip_5; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_6 = io_interrupts_lip_6; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_7 = io_interrupts_lip_7; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_8 = io_interrupts_lip_8; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_9 = io_interrupts_lip_9; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_10 = io_interrupts_lip_10; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_11 = io_interrupts_lip_11; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_12 = io_interrupts_lip_12; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_13 = io_interrupts_lip_13; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_14 = io_interrupts_lip_14; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_lip_15 = io_interrupts_lip_15; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_nmi_rnmi = io_interrupts_nmi_rnmi; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_nmi_rnmi_interrupt_vector = io_interrupts_nmi_rnmi_interrupt_vector; // @[RocketCore.scala 689:21]
  assign csr_io_interrupts_nmi_rnmi_exception_vector = io_interrupts_nmi_rnmi_exception_vector; // @[RocketCore.scala 689:21]
  assign csr_io_hartid = io_hartid; // @[RocketCore.scala 690:17]
  assign csr_io_rw_addr = wb_reg_inst[31:20]; // @[RocketCore.scala 706:32]
  assign csr_io_rw_cmd = wb_ctrl_csr & _csr_io_rw_cmd_T_1; // @[CSR.scala 132:9]
  assign csr_io_rw_wdata = wb_reg_wdata; // @[RocketCore.scala 708:19]
  assign csr_io_decode_0_csr = ibuf_io_inst_0_bits_raw[31:20]; // @[RocketCore.scala 684:41]
  assign csr_io_exception = wb_reg_xcpt | _T_176 | _T_178 | _T_180 | _T_182 | _T_184 | _T_186; // @[RocketCore.scala 977:26]
  assign csr_io_retire = wb_reg_valid & ~replay_wb_common & ~wb_xcpt; // @[RocketCore.scala 671:45]
  assign csr_io_cause = wb_reg_xcpt ? wb_reg_cause : {{60'd0}, _T_196}; // @[Mux.scala 47:69]
  assign csr_io_pc = wb_reg_pc; // @[RocketCore.scala 696:13]
  assign csr_io_tval = tval_valid ? _csr_io_tval_T : 40'h0; // @[RocketCore.scala 701:21]
  assign csr_io_fcsr_flags_valid = io_fpu_fcsr_flags_valid; // @[RocketCore.scala 692:21]
  assign csr_io_fcsr_flags_bits = io_fpu_fcsr_flags_bits; // @[RocketCore.scala 692:21]
  assign csr_io_counters_0_inc = csr_io_counters_0_inc_REG; // @[RocketCore.scala 883:40]
  assign csr_io_counters_1_inc = csr_io_counters_1_inc_REG; // @[RocketCore.scala 883:40]
  assign csr_io_counters_2_inc = csr_io_counters_2_inc_REG; // @[RocketCore.scala 883:40]
  assign csr_io_counters_3_inc = csr_io_counters_3_inc_REG; // @[RocketCore.scala 883:40]
  assign csr_io_inst_0 = {csr_io_inst_0_hi,csr_io_inst_0_lo}; // @[Cat.scala 30:58]
  assign d_insn = ibuf_io_inst_0_bits_raw; // @[RocketCore.scala 294:15]
  assign bpu_io_status_debug = csr_io_status_debug; // @[RocketCore.scala 325:17]
  assign bpu_io_status_prv = csr_io_status_prv; // @[RocketCore.scala 325:17]
  assign bpu_io_bp_0_control_action = csr_io_bp_0_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_0_control_chain = csr_io_bp_0_control_chain; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_0_control_tmatch = csr_io_bp_0_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_0_control_m = csr_io_bp_0_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_0_control_s = csr_io_bp_0_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_0_control_u = csr_io_bp_0_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_0_control_x = csr_io_bp_0_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_0_control_w = csr_io_bp_0_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_0_control_r = csr_io_bp_0_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_0_address = csr_io_bp_0_address; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_1_control_action = csr_io_bp_1_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_1_control_chain = csr_io_bp_1_control_chain; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_1_control_tmatch = csr_io_bp_1_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_1_control_m = csr_io_bp_1_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_1_control_s = csr_io_bp_1_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_1_control_u = csr_io_bp_1_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_1_control_x = csr_io_bp_1_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_1_control_w = csr_io_bp_1_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_1_control_r = csr_io_bp_1_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_1_address = csr_io_bp_1_address; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_2_control_action = csr_io_bp_2_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_2_control_chain = csr_io_bp_2_control_chain; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_2_control_tmatch = csr_io_bp_2_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_2_control_m = csr_io_bp_2_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_2_control_s = csr_io_bp_2_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_2_control_u = csr_io_bp_2_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_2_control_x = csr_io_bp_2_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_2_control_w = csr_io_bp_2_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_2_control_r = csr_io_bp_2_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_2_address = csr_io_bp_2_address; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_3_control_action = csr_io_bp_3_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_3_control_chain = csr_io_bp_3_control_chain; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_3_control_tmatch = csr_io_bp_3_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_3_control_m = csr_io_bp_3_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_3_control_s = csr_io_bp_3_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_3_control_u = csr_io_bp_3_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_3_control_x = csr_io_bp_3_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_3_control_w = csr_io_bp_3_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_3_control_r = csr_io_bp_3_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_3_address = csr_io_bp_3_address; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_4_control_action = csr_io_bp_4_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_4_control_chain = csr_io_bp_4_control_chain; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_4_control_tmatch = csr_io_bp_4_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_4_control_m = csr_io_bp_4_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_4_control_s = csr_io_bp_4_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_4_control_u = csr_io_bp_4_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_4_control_x = csr_io_bp_4_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_4_control_w = csr_io_bp_4_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_4_control_r = csr_io_bp_4_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_4_address = csr_io_bp_4_address; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_5_control_action = csr_io_bp_5_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_5_control_chain = csr_io_bp_5_control_chain; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_5_control_tmatch = csr_io_bp_5_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_5_control_m = csr_io_bp_5_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_5_control_s = csr_io_bp_5_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_5_control_u = csr_io_bp_5_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_5_control_x = csr_io_bp_5_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_5_control_w = csr_io_bp_5_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_5_control_r = csr_io_bp_5_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_5_address = csr_io_bp_5_address; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_6_control_action = csr_io_bp_6_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_6_control_chain = csr_io_bp_6_control_chain; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_6_control_tmatch = csr_io_bp_6_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_6_control_m = csr_io_bp_6_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_6_control_s = csr_io_bp_6_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_6_control_u = csr_io_bp_6_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_6_control_x = csr_io_bp_6_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_6_control_w = csr_io_bp_6_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_6_control_r = csr_io_bp_6_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_6_address = csr_io_bp_6_address; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_7_control_action = csr_io_bp_7_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_7_control_chain = csr_io_bp_7_control_chain; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_7_control_tmatch = csr_io_bp_7_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_7_control_m = csr_io_bp_7_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_7_control_s = csr_io_bp_7_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_7_control_u = csr_io_bp_7_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_7_control_x = csr_io_bp_7_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_7_control_w = csr_io_bp_7_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_7_control_r = csr_io_bp_7_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_7_address = csr_io_bp_7_address; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_8_control_action = csr_io_bp_8_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_8_control_chain = csr_io_bp_8_control_chain; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_8_control_tmatch = csr_io_bp_8_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_8_control_m = csr_io_bp_8_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_8_control_s = csr_io_bp_8_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_8_control_u = csr_io_bp_8_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_8_control_x = csr_io_bp_8_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_8_control_w = csr_io_bp_8_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_8_control_r = csr_io_bp_8_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_8_address = csr_io_bp_8_address; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_9_control_action = csr_io_bp_9_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_9_control_chain = csr_io_bp_9_control_chain; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_9_control_tmatch = csr_io_bp_9_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_9_control_m = csr_io_bp_9_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_9_control_s = csr_io_bp_9_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_9_control_u = csr_io_bp_9_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_9_control_x = csr_io_bp_9_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_9_control_w = csr_io_bp_9_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_9_control_r = csr_io_bp_9_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_9_address = csr_io_bp_9_address; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_10_control_action = csr_io_bp_10_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_10_control_chain = csr_io_bp_10_control_chain; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_10_control_tmatch = csr_io_bp_10_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_10_control_m = csr_io_bp_10_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_10_control_s = csr_io_bp_10_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_10_control_u = csr_io_bp_10_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_10_control_x = csr_io_bp_10_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_10_control_w = csr_io_bp_10_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_10_control_r = csr_io_bp_10_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_10_address = csr_io_bp_10_address; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_11_control_action = csr_io_bp_11_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_11_control_chain = csr_io_bp_11_control_chain; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_11_control_tmatch = csr_io_bp_11_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_11_control_m = csr_io_bp_11_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_11_control_s = csr_io_bp_11_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_11_control_u = csr_io_bp_11_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_11_control_x = csr_io_bp_11_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_11_control_w = csr_io_bp_11_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_11_control_r = csr_io_bp_11_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_11_address = csr_io_bp_11_address; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_12_control_action = csr_io_bp_12_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_12_control_chain = csr_io_bp_12_control_chain; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_12_control_tmatch = csr_io_bp_12_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_12_control_m = csr_io_bp_12_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_12_control_s = csr_io_bp_12_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_12_control_u = csr_io_bp_12_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_12_control_x = csr_io_bp_12_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_12_control_w = csr_io_bp_12_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_12_control_r = csr_io_bp_12_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_12_address = csr_io_bp_12_address; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_13_control_action = csr_io_bp_13_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_13_control_chain = csr_io_bp_13_control_chain; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_13_control_tmatch = csr_io_bp_13_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_13_control_m = csr_io_bp_13_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_13_control_s = csr_io_bp_13_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_13_control_u = csr_io_bp_13_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_13_control_x = csr_io_bp_13_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_13_control_w = csr_io_bp_13_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_13_control_r = csr_io_bp_13_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_13_address = csr_io_bp_13_address; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_14_control_action = csr_io_bp_14_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_14_control_chain = csr_io_bp_14_control_chain; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_14_control_tmatch = csr_io_bp_14_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_14_control_m = csr_io_bp_14_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_14_control_s = csr_io_bp_14_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_14_control_u = csr_io_bp_14_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_14_control_x = csr_io_bp_14_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_14_control_w = csr_io_bp_14_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_14_control_r = csr_io_bp_14_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_14_address = csr_io_bp_14_address; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_15_control_action = csr_io_bp_15_control_action; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_15_control_tmatch = csr_io_bp_15_control_tmatch; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_15_control_m = csr_io_bp_15_control_m; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_15_control_s = csr_io_bp_15_control_s; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_15_control_u = csr_io_bp_15_control_u; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_15_control_x = csr_io_bp_15_control_x; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_15_control_w = csr_io_bp_15_control_w; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_15_control_r = csr_io_bp_15_control_r; // @[RocketCore.scala 326:13]
  assign bpu_io_bp_15_address = csr_io_bp_15_address; // @[RocketCore.scala 326:13]
  assign bpu_io_pc = ibuf_io_pc[38:0]; // @[RocketCore.scala 327:13]
  assign bpu_io_ea = mem_reg_wdata[38:0]; // @[RocketCore.scala 328:13]
  assign alu_io_dw = ex_ctrl_alu_dw; // @[RocketCore.scala 387:13]
  assign alu_io_fn = ex_ctrl_alu_fn; // @[RocketCore.scala 388:13]
  assign alu_io_in2 = 2'h1 == ex_ctrl_sel_alu2 ? $signed({{60{_ex_op2_T_1[3]}},_ex_op2_T_1}) : $signed(_ex_op2_T_5); // @[RocketCore.scala 389:24]
  assign alu_io_in1 = 2'h2 == ex_ctrl_sel_alu1 ? $signed({{24{_ex_op1_T_1[39]}},_ex_op1_T_1}) : $signed(_ex_op1_T_3); // @[RocketCore.scala 390:24]
  assign u_insn = ex_reg_inst; // @[RocketCore.scala 394:15]
  assign u_rs1 = ex_reg_rs_bypass_0 ? _ex_rs_T_5 : _ex_rs_T_6; // @[RocketCore.scala 376:14]
  assign u_rs2 = ex_reg_rs_bypass_1 ? _ex_rs_T_12 : _ex_rs_T_13; // @[RocketCore.scala 376:14]
  assign u_1_clock = gated_clock_rocket_clock_gate_out; // @[RocketCore.scala 401:16]
  assign u_1_valid = ex_reg_valid & ex_scie_pipelined; // @[RocketCore.scala 402:32]
  assign u_1_insn = ex_reg_inst; // @[RocketCore.scala 403:15]
  assign u_1_rs1 = ex_reg_rs_bypass_0 ? _ex_rs_T_5 : _ex_rs_T_6; // @[RocketCore.scala 376:14]
  assign u_1_rs2 = ex_reg_rs_bypass_1 ? _ex_rs_T_12 : _ex_rs_T_13; // @[RocketCore.scala 376:14]
  assign div_clock = gated_clock_rocket_clock_gate_out;
  assign div_reset = reset;
  assign div_io_req_valid = ex_reg_valid & ex_ctrl_div; // @[RocketCore.scala 411:36]
  assign div_io_req_bits_fn = ex_ctrl_alu_fn; // @[RocketCore.scala 413:22]
  assign div_io_req_bits_dw = ex_ctrl_alu_dw; // @[RocketCore.scala 412:22]
  assign div_io_req_bits_in1 = ex_reg_rs_bypass_0 ? _ex_rs_T_5 : _ex_rs_T_6; // @[RocketCore.scala 376:14]
  assign div_io_req_bits_in2 = ex_reg_rs_bypass_1 ? _ex_rs_T_12 : _ex_rs_T_13; // @[RocketCore.scala 376:14]
  assign div_io_req_bits_tag = ex_reg_inst[11:7]; // @[RocketCore.scala 360:29]
  assign div_io_kill = killm_common & div_io_kill_REG; // @[RocketCore.scala 589:31]
  assign div_io_resp_ready = dmem_resp_replay & dmem_resp_xpu ? 1'h0 : _ctrl_stalld_T_17; // @[RocketCore.scala 663:44 RocketCore.scala 664:23 RocketCore.scala 650:21]
  assign m_clock = gated_clock_rocket_clock_gate_out;
  assign m_reset = reset;
  assign m_io_req_valid = ex_reg_valid & ex_ctrl_mul; // @[RocketCore.scala 419:36]
  assign m_io_req_bits_fn = div_io_req_bits_fn; // @[RocketCore.scala 420:19]
  assign m_io_req_bits_dw = div_io_req_bits_dw; // @[RocketCore.scala 420:19]
  assign m_io_req_bits_in1 = div_io_req_bits_in1; // @[RocketCore.scala 420:19]
  assign m_io_req_bits_in2 = div_io_req_bits_in2; // @[RocketCore.scala 420:19]
  always @(posedge gated_clock_rocket_clock_gate_out) begin
    if(rf_MPORT_en & rf_MPORT_mask) begin
      rf[rf_MPORT_addr] <= rf_MPORT_data; // @[RocketCore.scala 1017:15]
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      clock_en_reg <= 1'h0;
    end else begin
      clock_en_reg <= reset | _clock_en_reg_T_13;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      long_latency_stall <= 1'h0;
    end else begin
      long_latency_stall <= csr_io_csr_stall | io_dmem_perf_blocked | id_reg_pause & ~unpause;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      id_reg_pause <= 1'h0;
    end else if (unpause) begin
      id_reg_pause <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      id_reg_pause <= _GEN_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      imem_might_request_reg <= 1'h0;
    end else begin
      imem_might_request_reg <= ex_pc_valid | mem_pc_valid | io_ptw_customCSRs_csrs_1_value[1];
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_fp <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_ctrl_fp <= id_ctrl_decoder_1;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_branch <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_ctrl_branch <= id_ctrl_decoder_3;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_jal <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_ctrl_jal <= id_ctrl_decoder_4;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_jalr <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_ctrl_jalr <= id_ctrl_decoder_5;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_rxs2 <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_ctrl_rxs2 <= id_ctrl_decoder_6;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_sel_alu2 <= 2'h0;
    end else if (_ex_reg_valid_T) begin
      if (id_xcpt) begin
        if (bpu_io_xcpt_if | |_T_43) begin
          ex_ctrl_sel_alu2 <= 2'h0;
        end else if (|_T_41) begin
          ex_ctrl_sel_alu2 <= 2'h1;
        end else begin
          ex_ctrl_sel_alu2 <= 2'h0;
        end
      end else begin
        ex_ctrl_sel_alu2 <= id_ctrl_decoder_9;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_sel_alu1 <= 2'h0;
    end else if (_ex_reg_valid_T) begin
      if (id_xcpt) begin
        if (bpu_io_xcpt_if | |_T_43) begin
          ex_ctrl_sel_alu1 <= 2'h2;
        end else if (|_T_41) begin
          ex_ctrl_sel_alu1 <= 2'h2;
        end else begin
          ex_ctrl_sel_alu1 <= 2'h1;
        end
      end else begin
        ex_ctrl_sel_alu1 <= id_ctrl_decoder_10;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_sel_imm <= 3'h0;
    end else if (_ex_reg_valid_T) begin
      ex_ctrl_sel_imm <= id_ctrl_decoder_11;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_alu_dw <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_ctrl_alu_dw <= _GEN_9;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_alu_fn <= 4'h0;
    end else if (_ex_reg_valid_T) begin
      if (id_xcpt) begin
        ex_ctrl_alu_fn <= 4'h0;
      end else begin
        ex_ctrl_alu_fn <= id_ctrl_decoder_13;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_mem <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_ctrl_mem <= id_ctrl_decoder_14;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_mem_cmd <= 5'h0;
    end else if (_ex_reg_valid_T) begin
      ex_ctrl_mem_cmd <= id_ctrl_decoder_15;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_wfd <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_ctrl_wfd <= id_ctrl_decoder_19;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_mul <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_ctrl_mul <= id_ctrl_decoder_20;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_div <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_ctrl_div <= id_ctrl_decoder_21;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_wxd <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_ctrl_wxd <= id_ctrl_decoder_22;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_csr <= 3'h0;
    end else if (_ex_reg_valid_T) begin
      if (id_csr_ren) begin
        ex_ctrl_csr <= 3'h2;
      end else begin
        ex_ctrl_csr <= id_ctrl_decoder_23;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_ctrl_fence_i <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_ctrl_fence_i <= id_ctrl_decoder_24;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_ctrl_fp <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_ctrl_fp <= ex_ctrl_fp;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_ctrl_branch <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_ctrl_branch <= ex_ctrl_branch;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_ctrl_jal <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_ctrl_jal <= ex_ctrl_jal;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_ctrl_jalr <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_ctrl_jalr <= ex_ctrl_jalr;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_ctrl_mem <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_ctrl_mem <= ex_ctrl_mem;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_ctrl_wfd <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_ctrl_wfd <= ex_ctrl_wfd;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_ctrl_mul <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_ctrl_mul <= ex_ctrl_mul;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_ctrl_div <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_ctrl_div <= ex_ctrl_div;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_ctrl_wxd <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_ctrl_wxd <= ex_ctrl_wxd;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_ctrl_csr <= 3'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_ctrl_csr <= ex_ctrl_csr;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_ctrl_fence_i <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_ctrl_fence_i <= _GEN_92;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_ctrl_fp <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_ctrl_fp <= mem_ctrl_fp;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_ctrl_mem <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_ctrl_mem <= mem_ctrl_mem;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_ctrl_wfd <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_ctrl_wfd <= mem_ctrl_wfd;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_ctrl_mul <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_ctrl_mul <= mem_ctrl_mul;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_ctrl_div <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_ctrl_div <= mem_ctrl_div;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_ctrl_wxd <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_ctrl_wxd <= mem_ctrl_wxd;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_ctrl_csr <= 3'h0;
    end else if (mem_pc_valid) begin
      wb_ctrl_csr <= mem_ctrl_csr;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_ctrl_fence_i <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_ctrl_fence_i <= mem_ctrl_fence_i;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_xcpt_interrupt <= 1'h0;
    end else begin
      ex_reg_xcpt_interrupt <= _ex_reg_replay_T_1 & csr_io_interrupt;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_valid <= 1'h0;
    end else begin
      ex_reg_valid <= ~ctrl_killd;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_rvc <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      if (id_xcpt) begin
        ex_reg_rvc <= _GEN_5;
      end else begin
        ex_reg_rvc <= ibuf_io_inst_0_bits_rvc;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_btb_resp_taken <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_btb_resp_taken <= ibuf_io_btb_resp_taken;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_btb_resp_entry <= 5'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_btb_resp_entry <= ibuf_io_btb_resp_entry;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_btb_resp_bht_history <= 8'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_btb_resp_bht_history <= ibuf_io_btb_resp_bht_history;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_xcpt <= 1'h0;
    end else begin
      ex_reg_xcpt <= _ex_reg_valid_T & id_xcpt;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_flush_pipe <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_reg_flush_pipe <= id_ctrl_decoder_24 | id_csr_flush;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_load_use <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_reg_load_use <= id_load_use;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_cause <= 64'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      if (csr_io_interrupt) begin
        ex_reg_cause <= csr_io_interrupt_cause;
      end else begin
        ex_reg_cause <= {{60'd0}, _T_12};
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_replay <= 1'h0;
    end else begin
      ex_reg_replay <= ~take_pc_mem_wb & ibuf_io_inst_0_valid & ibuf_io_inst_0_bits_replay;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_pc <= 40'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_pc <= ibuf_io_pc;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_mem_size <= 2'h0;
    end else if (_ex_reg_valid_T) begin
      if (_T_48) begin
        ex_reg_mem_size <= _ex_reg_mem_size_T_1;
      end else begin
        ex_reg_mem_size <= ibuf_io_inst_0_bits_inst_bits[13:12];
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_inst <= 32'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_inst <= ibuf_io_inst_0_bits_inst_bits;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_raw_inst <= 32'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_raw_inst <= ibuf_io_inst_0_bits_raw;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_scie_unpipelined <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_scie_unpipelined <= id_ctrl_decoder_8 & d_unpipelined;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_scie_pipelined <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_scie_pipelined <= id_ctrl_decoder_8 & d_pipelined;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_0 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_0 <= bpu_io_bpwatch_0_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_1 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_1 <= bpu_io_bpwatch_1_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_2 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_2 <= bpu_io_bpwatch_2_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_3 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_3 <= bpu_io_bpwatch_3_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_4 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_4 <= bpu_io_bpwatch_4_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_5 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_5 <= bpu_io_bpwatch_5_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_6 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_6 <= bpu_io_bpwatch_6_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_7 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_7 <= bpu_io_bpwatch_7_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_8 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_8 <= bpu_io_bpwatch_8_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_9 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_9 <= bpu_io_bpwatch_9_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_10 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_10 <= bpu_io_bpwatch_10_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_11 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_11 <= bpu_io_bpwatch_11_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_12 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_12 <= bpu_io_bpwatch_12_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_13 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_13 <= bpu_io_bpwatch_13_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_14 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_14 <= bpu_io_bpwatch_14_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_wphit_15 <= 1'h0;
    end else if (_ex_reg_valid_T | csr_io_interrupt | ibuf_io_inst_0_bits_replay) begin
      ex_reg_wphit_15 <= bpu_io_bpwatch_15_ivalid_0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_xcpt_interrupt <= 1'h0;
    end else begin
      mem_reg_xcpt_interrupt <= _ex_reg_replay_T & ex_reg_xcpt_interrupt;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_valid <= 1'h0;
    end else begin
      mem_reg_valid <= ~ctrl_killx;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_rvc <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_rvc <= ex_reg_rvc;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_btb_resp_taken <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_btb_resp_taken <= ex_reg_btb_resp_taken;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_btb_resp_entry <= 5'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_btb_resp_entry <= ex_reg_btb_resp_entry;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_btb_resp_bht_history <= 8'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_btb_resp_bht_history <= ex_reg_btb_resp_bht_history;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_xcpt <= 1'h0;
    end else begin
      mem_reg_xcpt <= _mem_reg_valid_T & ex_xcpt;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_replay <= 1'h0;
    end else begin
      mem_reg_replay <= _ex_reg_replay_T & replay_ex;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_flush_pipe <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_flush_pipe <= _GEN_93;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_cause <= 64'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_cause <= ex_reg_cause;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_slow_bypass <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_slow_bypass <= ex_slow_bypass;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_load <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_load <= ex_ctrl_mem & _mem_reg_load_T_22;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_store <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_store <= ex_ctrl_mem & _mem_reg_store_T_22;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_sfence <= 1'h0;
    end else if (_T_76) begin
      mem_reg_sfence <= 1'h0;
    end else if (ex_pc_valid) begin
      mem_reg_sfence <= ex_sfence;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_pc <= 40'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_pc <= ex_reg_pc;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_inst <= 32'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_inst <= ex_reg_inst;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_mem_size <= 2'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_mem_size <= ex_reg_mem_size;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_raw_inst <= 32'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_raw_inst <= ex_reg_raw_inst;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_scie_pipelined <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_scie_pipelined <= ex_scie_pipelined;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wdata <= 64'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        if (ex_scie_unpipelined) begin
          mem_reg_wdata <= u_rd;
        end else begin
          mem_reg_wdata <= alu_io_out;
        end
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_rs2 <= 64'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        if (ex_ctrl_rxs2 & (ex_ctrl_mem | ex_sfence)) begin
          if (ex_reg_mem_size == 2'h0) begin
            mem_reg_rs2 <= _mem_reg_rs2_T_1;
          end else begin
            mem_reg_rs2 <= _mem_reg_rs2_T_7;
          end
        end
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_br_taken <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_br_taken <= alu_io_cmp_out;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_0 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_0 <= ex_reg_wphit_0;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_1 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_1 <= ex_reg_wphit_1;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_2 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_2 <= ex_reg_wphit_2;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_3 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_3 <= ex_reg_wphit_3;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_4 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_4 <= ex_reg_wphit_4;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_5 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_5 <= ex_reg_wphit_5;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_6 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_6 <= ex_reg_wphit_6;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_7 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_7 <= ex_reg_wphit_7;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_8 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_8 <= ex_reg_wphit_8;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_9 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_9 <= ex_reg_wphit_9;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_10 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_10 <= ex_reg_wphit_10;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_11 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_11 <= ex_reg_wphit_11;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_12 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_12 <= ex_reg_wphit_12;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_13 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_13 <= ex_reg_wphit_13;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_14 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_14 <= ex_reg_wphit_14;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_reg_wphit_15 <= 1'h0;
    end else if (!(_T_76)) begin
      if (ex_pc_valid) begin
        mem_reg_wphit_15 <= ex_reg_wphit_15;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_valid <= 1'h0;
    end else begin
      wb_reg_valid <= ~ctrl_killm;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_xcpt <= 1'h0;
    end else begin
      wb_reg_xcpt <= mem_xcpt & _wb_reg_replay_T;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_replay <= 1'h0;
    end else begin
      wb_reg_replay <= replay_mem & ~take_pc_wb;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_flush_pipe <= 1'h0;
    end else begin
      wb_reg_flush_pipe <= _wb_reg_valid_T & mem_reg_flush_pipe;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_cause <= 64'h0;
    end else if (mem_pc_valid) begin
      if (_T_81) begin
        wb_reg_cause <= mem_reg_cause;
      end else begin
        wb_reg_cause <= {{60'd0}, _T_85};
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_sfence <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_sfence <= mem_reg_sfence;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_pc <= 40'h0;
    end else if (mem_pc_valid) begin
      wb_reg_pc <= mem_reg_pc;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_mem_size <= 2'h0;
    end else if (mem_pc_valid) begin
      wb_reg_mem_size <= mem_reg_mem_size;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_inst <= 32'h0;
    end else if (mem_pc_valid) begin
      wb_reg_inst <= mem_reg_inst;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_raw_inst <= 32'h0;
    end else if (mem_pc_valid) begin
      wb_reg_raw_inst <= mem_reg_raw_inst;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wdata <= 64'h0;
    end else if (mem_pc_valid) begin
      if (mem_scie_pipelined) begin
        wb_reg_wdata <= u_1_rd;
      end else if (_mem_int_wdata_T & mem_ctrl_fp & mem_ctrl_wxd) begin
        wb_reg_wdata <= io_fpu_toint_data;
      end else begin
        wb_reg_wdata <= mem_int_wdata;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_rs2 <= 64'h0;
    end else if (mem_pc_valid) begin
      if (mem_reg_sfence) begin
        wb_reg_rs2 <= mem_reg_rs2;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_0 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_0 <= _T_159;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_1 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_1 <= _T_160;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_2 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_2 <= _T_161;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_3 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_3 <= _T_162;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_4 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_4 <= _T_163;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_5 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_5 <= _T_164;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_6 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_6 <= _T_165;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_7 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_7 <= _T_166;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_8 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_8 <= _T_167;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_9 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_9 <= _T_168;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_10 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_10 <= _T_169;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_11 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_11 <= _T_170;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_12 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_12 <= _T_171;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_13 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_13 <= _T_172;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_14 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_14 <= _T_173;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      wb_reg_wphit_15 <= 1'h0;
    end else if (mem_pc_valid) begin
      wb_reg_wphit_15 <= _T_174;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      id_reg_fence <= 1'h0;
    end else if (reset) begin
      id_reg_fence <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      id_reg_fence <= _GEN_2;
    end else if (~id_mem_busy) begin
      id_reg_fence <= 1'h0;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_rs_bypass_0 <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      if (id_illegal_insn) begin
        ex_reg_rs_bypass_0 <= 1'h0;
      end else begin
        ex_reg_rs_bypass_0 <= do_bypass;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_rs_bypass_1 <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      ex_reg_rs_bypass_1 <= do_bypass_1;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_rs_lsb_0 <= 2'h0;
    end else if (_ex_reg_valid_T) begin
      if (id_illegal_insn) begin
        ex_reg_rs_lsb_0 <= inst[1:0];
      end else if (id_ctrl_decoder_7 & ~do_bypass) begin
        ex_reg_rs_lsb_0 <= id_rs_0[1:0];
      end else if (id_bypass_src_0_0) begin
        ex_reg_rs_lsb_0 <= 2'h0;
      end else begin
        ex_reg_rs_lsb_0 <= _bypass_src_T_1;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_rs_lsb_1 <= 2'h0;
    end else if (_ex_reg_valid_T) begin
      if (id_ctrl_decoder_6 & ~do_bypass_1) begin
        ex_reg_rs_lsb_1 <= id_rs_1[1:0];
      end else if (id_bypass_src_1_0) begin
        ex_reg_rs_lsb_1 <= 2'h0;
      end else if (id_bypass_src_1_1) begin
        ex_reg_rs_lsb_1 <= 2'h1;
      end else begin
        ex_reg_rs_lsb_1 <= _bypass_src_T_2;
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_rs_msb_0 <= 62'h0;
    end else if (_ex_reg_valid_T) begin
      if (id_illegal_insn) begin
        ex_reg_rs_msb_0 <= {{32'd0}, inst[31:2]};
      end else if (id_ctrl_decoder_7 & ~do_bypass) begin
        ex_reg_rs_msb_0 <= id_rs_0[63:2];
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      ex_reg_rs_msb_1 <= 62'h0;
    end else if (_ex_reg_valid_T) begin
      if (id_ctrl_decoder_6 & ~do_bypass_1) begin
        ex_reg_rs_msb_1 <= id_rs_1[63:2];
      end
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      state <= 32'h0;
    end else if (reset) begin
      state <= 32'h0;
    end else if (_T_242) begin
      state <= _T_241;
    end else if (ll_wen) begin
      state <= _T_236;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      id_stall_fpu_state <= 32'h0;
    end else if (reset) begin
      id_stall_fpu_state <= 32'h0;
    end else if (_id_stall_fpu_T_17) begin
      id_stall_fpu_state <= _id_stall_fpu_T_16;
    end else if (_id_stall_fpu_T_12) begin
      id_stall_fpu_state <= _id_stall_fpu_T_11;
    end else if (_id_stall_fpu_T_2) begin
      id_stall_fpu_state <= _id_stall_fpu_T_5;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      blocked <= 1'h0;
    end else begin
      blocked <= _replay_ex_structural_T & io_dmem_clock_enabled & _dcache_blocked_T & (blocked | io_dmem_req_valid |
        io_dmem_s2_nack);
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      div_io_kill_REG <= 1'h0;
    end else begin
      div_io_kill_REG <= div_io_req_ready & div_io_req_valid;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      icache_blocked_REG <= 1'h0;
    end else begin
      icache_blocked_REG <= io_imem_resp_valid;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_0_inc_sets_r <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      csr_io_counters_0_inc_sets_r <= _csr_io_counters_0_inc_sets_T_65;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_0_inc_sets_r_1 <= 1'h0;
    end else if (ex_pc_valid) begin
      csr_io_counters_0_inc_sets_r_1 <= csr_io_counters_0_inc_sets_r;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_0_inc_sets_r_2 <= 1'h0;
    end else if (mem_pc_valid) begin
      csr_io_counters_0_inc_sets_r_2 <= csr_io_counters_0_inc_sets_r_1;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_0_inc_REG <= 1'h0;
    end else if (csr_io_counters_0_inc_set == 2'h3) begin
      csr_io_counters_0_inc_REG <= csr_io_counters_0_inc_sets_2;
    end else if (csr_io_counters_0_inc_set == 2'h2) begin
      csr_io_counters_0_inc_REG <= csr_io_counters_0_inc_sets_2;
    end else if (csr_io_counters_0_inc_set == 2'h1) begin
      csr_io_counters_0_inc_REG <= csr_io_counters_0_inc_sets_1;
    end else if (wb_xcpt) begin
      csr_io_counters_0_inc_REG <= csr_io_counters_0_inc_mask[0];
    end else begin
      csr_io_counters_0_inc_REG <= wb_valid & csr_io_counters_0_inc_sets_r_2;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_1_inc_sets_r <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      csr_io_counters_1_inc_sets_r <= _csr_io_counters_1_inc_sets_T_65;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_1_inc_sets_r_1 <= 1'h0;
    end else if (ex_pc_valid) begin
      csr_io_counters_1_inc_sets_r_1 <= csr_io_counters_1_inc_sets_r;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_1_inc_sets_r_2 <= 1'h0;
    end else if (mem_pc_valid) begin
      csr_io_counters_1_inc_sets_r_2 <= csr_io_counters_1_inc_sets_r_1;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_1_inc_REG <= 1'h0;
    end else if (csr_io_counters_1_inc_set == 2'h3) begin
      csr_io_counters_1_inc_REG <= csr_io_counters_1_inc_sets_2;
    end else if (csr_io_counters_1_inc_set == 2'h2) begin
      csr_io_counters_1_inc_REG <= csr_io_counters_1_inc_sets_2;
    end else if (csr_io_counters_1_inc_set == 2'h1) begin
      csr_io_counters_1_inc_REG <= csr_io_counters_1_inc_sets_1;
    end else if (wb_xcpt) begin
      csr_io_counters_1_inc_REG <= csr_io_counters_1_inc_mask[0];
    end else begin
      csr_io_counters_1_inc_REG <= wb_valid & csr_io_counters_1_inc_sets_r_2;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_2_inc_sets_r <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      csr_io_counters_2_inc_sets_r <= _csr_io_counters_2_inc_sets_T_65;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_2_inc_sets_r_1 <= 1'h0;
    end else if (ex_pc_valid) begin
      csr_io_counters_2_inc_sets_r_1 <= csr_io_counters_2_inc_sets_r;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_2_inc_sets_r_2 <= 1'h0;
    end else if (mem_pc_valid) begin
      csr_io_counters_2_inc_sets_r_2 <= csr_io_counters_2_inc_sets_r_1;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_2_inc_REG <= 1'h0;
    end else if (csr_io_counters_2_inc_set == 2'h3) begin
      csr_io_counters_2_inc_REG <= csr_io_counters_2_inc_sets_2;
    end else if (csr_io_counters_2_inc_set == 2'h2) begin
      csr_io_counters_2_inc_REG <= csr_io_counters_2_inc_sets_2;
    end else if (csr_io_counters_2_inc_set == 2'h1) begin
      csr_io_counters_2_inc_REG <= csr_io_counters_2_inc_sets_1;
    end else if (wb_xcpt) begin
      csr_io_counters_2_inc_REG <= csr_io_counters_2_inc_mask[0];
    end else begin
      csr_io_counters_2_inc_REG <= wb_valid & csr_io_counters_2_inc_sets_r_2;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_3_inc_sets_r <= 1'h0;
    end else if (_ex_reg_valid_T) begin
      csr_io_counters_3_inc_sets_r <= _csr_io_counters_3_inc_sets_T_65;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_3_inc_sets_r_1 <= 1'h0;
    end else if (ex_pc_valid) begin
      csr_io_counters_3_inc_sets_r_1 <= csr_io_counters_3_inc_sets_r;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_3_inc_sets_r_2 <= 1'h0;
    end else if (mem_pc_valid) begin
      csr_io_counters_3_inc_sets_r_2 <= csr_io_counters_3_inc_sets_r_1;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      csr_io_counters_3_inc_REG <= 1'h0;
    end else if (csr_io_counters_3_inc_set == 2'h3) begin
      csr_io_counters_3_inc_REG <= csr_io_counters_3_inc_sets_2;
    end else if (csr_io_counters_3_inc_set == 2'h2) begin
      csr_io_counters_3_inc_REG <= csr_io_counters_3_inc_sets_2;
    end else if (csr_io_counters_3_inc_set == 2'h1) begin
      csr_io_counters_3_inc_REG <= csr_io_counters_3_inc_sets_1;
    end else if (wb_xcpt) begin
      csr_io_counters_3_inc_REG <= csr_io_counters_3_inc_mask[0];
    end else begin
      csr_io_counters_3_inc_REG <= wb_valid & csr_io_counters_3_inc_sets_r_2;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      coreMonitorBundle_rd0val_x20 <= 64'h0;
    end else if (ex_reg_rs_bypass_0) begin
      if (ex_reg_rs_lsb_0 == 2'h3) begin
        coreMonitorBundle_rd0val_x20 <= io_dmem_resp_bits_data_word_bypass;
      end else if (ex_reg_rs_lsb_0 == 2'h2) begin
        coreMonitorBundle_rd0val_x20 <= wb_reg_wdata;
      end else if (ex_reg_rs_lsb_0 == 2'h1) begin
        coreMonitorBundle_rd0val_x20 <= mem_reg_wdata;
      end else begin
        coreMonitorBundle_rd0val_x20 <= 64'h0;
      end
    end else begin
      coreMonitorBundle_rd0val_x20 <= _ex_rs_T_6;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      coreMonitorBundle_rd0val_REG <= 64'h0;
    end else begin
      coreMonitorBundle_rd0val_REG <= coreMonitorBundle_rd0val_x20;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      coreMonitorBundle_rd1val_x26 <= 64'h0;
    end else if (ex_reg_rs_bypass_1) begin
      if (ex_reg_rs_lsb_1 == 2'h3) begin
        coreMonitorBundle_rd1val_x26 <= io_dmem_resp_bits_data_word_bypass;
      end else if (ex_reg_rs_lsb_1 == 2'h2) begin
        coreMonitorBundle_rd1val_x26 <= wb_reg_wdata;
      end else if (ex_reg_rs_lsb_1 == 2'h1) begin
        coreMonitorBundle_rd1val_x26 <= mem_reg_wdata;
      end else begin
        coreMonitorBundle_rd1val_x26 <= 64'h0;
      end
    end else begin
      coreMonitorBundle_rd1val_x26 <= _ex_rs_T_13;
    end
  end
  always @(posedge gated_clock_rocket_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      coreMonitorBundle_rd1val_REG <= 64'h0;
    end else begin
      coreMonitorBundle_rd1val_REG <= coreMonitorBundle_rd1val_x26;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_GARBAGE_ASSIGN
  _RAND_1 = {2{`RANDOM}};
  _RAND_2 = {2{`RANDOM}};
`endif // RANDOMIZE_GARBAGE_ASSIGN
`ifdef RANDOMIZE_MEM_INIT
  _RAND_0 = {2{`RANDOM}};
  for (initvar = 0; initvar < 31; initvar = initvar+1)
    rf[initvar] = _RAND_0[63:0];
`endif // RANDOMIZE_MEM_INIT
`ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  clock_en_reg = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  long_latency_stall = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  id_reg_pause = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  imem_might_request_reg = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  ex_ctrl_fp = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  ex_ctrl_branch = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  ex_ctrl_jal = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  ex_ctrl_jalr = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  ex_ctrl_rxs2 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  ex_ctrl_sel_alu2 = _RAND_12[1:0];
  _RAND_13 = {1{`RANDOM}};
  ex_ctrl_sel_alu1 = _RAND_13[1:0];
  _RAND_14 = {1{`RANDOM}};
  ex_ctrl_sel_imm = _RAND_14[2:0];
  _RAND_15 = {1{`RANDOM}};
  ex_ctrl_alu_dw = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  ex_ctrl_alu_fn = _RAND_16[3:0];
  _RAND_17 = {1{`RANDOM}};
  ex_ctrl_mem = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  ex_ctrl_mem_cmd = _RAND_18[4:0];
  _RAND_19 = {1{`RANDOM}};
  ex_ctrl_wfd = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  ex_ctrl_mul = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  ex_ctrl_div = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  ex_ctrl_wxd = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  ex_ctrl_csr = _RAND_23[2:0];
  _RAND_24 = {1{`RANDOM}};
  ex_ctrl_fence_i = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  mem_ctrl_fp = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  mem_ctrl_branch = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  mem_ctrl_jal = _RAND_27[0:0];
  _RAND_28 = {1{`RANDOM}};
  mem_ctrl_jalr = _RAND_28[0:0];
  _RAND_29 = {1{`RANDOM}};
  mem_ctrl_mem = _RAND_29[0:0];
  _RAND_30 = {1{`RANDOM}};
  mem_ctrl_wfd = _RAND_30[0:0];
  _RAND_31 = {1{`RANDOM}};
  mem_ctrl_mul = _RAND_31[0:0];
  _RAND_32 = {1{`RANDOM}};
  mem_ctrl_div = _RAND_32[0:0];
  _RAND_33 = {1{`RANDOM}};
  mem_ctrl_wxd = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  mem_ctrl_csr = _RAND_34[2:0];
  _RAND_35 = {1{`RANDOM}};
  mem_ctrl_fence_i = _RAND_35[0:0];
  _RAND_36 = {1{`RANDOM}};
  wb_ctrl_fp = _RAND_36[0:0];
  _RAND_37 = {1{`RANDOM}};
  wb_ctrl_mem = _RAND_37[0:0];
  _RAND_38 = {1{`RANDOM}};
  wb_ctrl_wfd = _RAND_38[0:0];
  _RAND_39 = {1{`RANDOM}};
  wb_ctrl_mul = _RAND_39[0:0];
  _RAND_40 = {1{`RANDOM}};
  wb_ctrl_div = _RAND_40[0:0];
  _RAND_41 = {1{`RANDOM}};
  wb_ctrl_wxd = _RAND_41[0:0];
  _RAND_42 = {1{`RANDOM}};
  wb_ctrl_csr = _RAND_42[2:0];
  _RAND_43 = {1{`RANDOM}};
  wb_ctrl_fence_i = _RAND_43[0:0];
  _RAND_44 = {1{`RANDOM}};
  ex_reg_xcpt_interrupt = _RAND_44[0:0];
  _RAND_45 = {1{`RANDOM}};
  ex_reg_valid = _RAND_45[0:0];
  _RAND_46 = {1{`RANDOM}};
  ex_reg_rvc = _RAND_46[0:0];
  _RAND_47 = {1{`RANDOM}};
  ex_reg_btb_resp_taken = _RAND_47[0:0];
  _RAND_48 = {1{`RANDOM}};
  ex_reg_btb_resp_entry = _RAND_48[4:0];
  _RAND_49 = {1{`RANDOM}};
  ex_reg_btb_resp_bht_history = _RAND_49[7:0];
  _RAND_50 = {1{`RANDOM}};
  ex_reg_xcpt = _RAND_50[0:0];
  _RAND_51 = {1{`RANDOM}};
  ex_reg_flush_pipe = _RAND_51[0:0];
  _RAND_52 = {1{`RANDOM}};
  ex_reg_load_use = _RAND_52[0:0];
  _RAND_53 = {2{`RANDOM}};
  ex_reg_cause = _RAND_53[63:0];
  _RAND_54 = {1{`RANDOM}};
  ex_reg_replay = _RAND_54[0:0];
  _RAND_55 = {2{`RANDOM}};
  ex_reg_pc = _RAND_55[39:0];
  _RAND_56 = {1{`RANDOM}};
  ex_reg_mem_size = _RAND_56[1:0];
  _RAND_57 = {1{`RANDOM}};
  ex_reg_inst = _RAND_57[31:0];
  _RAND_58 = {1{`RANDOM}};
  ex_reg_raw_inst = _RAND_58[31:0];
  _RAND_59 = {1{`RANDOM}};
  ex_scie_unpipelined = _RAND_59[0:0];
  _RAND_60 = {1{`RANDOM}};
  ex_scie_pipelined = _RAND_60[0:0];
  _RAND_61 = {1{`RANDOM}};
  ex_reg_wphit_0 = _RAND_61[0:0];
  _RAND_62 = {1{`RANDOM}};
  ex_reg_wphit_1 = _RAND_62[0:0];
  _RAND_63 = {1{`RANDOM}};
  ex_reg_wphit_2 = _RAND_63[0:0];
  _RAND_64 = {1{`RANDOM}};
  ex_reg_wphit_3 = _RAND_64[0:0];
  _RAND_65 = {1{`RANDOM}};
  ex_reg_wphit_4 = _RAND_65[0:0];
  _RAND_66 = {1{`RANDOM}};
  ex_reg_wphit_5 = _RAND_66[0:0];
  _RAND_67 = {1{`RANDOM}};
  ex_reg_wphit_6 = _RAND_67[0:0];
  _RAND_68 = {1{`RANDOM}};
  ex_reg_wphit_7 = _RAND_68[0:0];
  _RAND_69 = {1{`RANDOM}};
  ex_reg_wphit_8 = _RAND_69[0:0];
  _RAND_70 = {1{`RANDOM}};
  ex_reg_wphit_9 = _RAND_70[0:0];
  _RAND_71 = {1{`RANDOM}};
  ex_reg_wphit_10 = _RAND_71[0:0];
  _RAND_72 = {1{`RANDOM}};
  ex_reg_wphit_11 = _RAND_72[0:0];
  _RAND_73 = {1{`RANDOM}};
  ex_reg_wphit_12 = _RAND_73[0:0];
  _RAND_74 = {1{`RANDOM}};
  ex_reg_wphit_13 = _RAND_74[0:0];
  _RAND_75 = {1{`RANDOM}};
  ex_reg_wphit_14 = _RAND_75[0:0];
  _RAND_76 = {1{`RANDOM}};
  ex_reg_wphit_15 = _RAND_76[0:0];
  _RAND_77 = {1{`RANDOM}};
  mem_reg_xcpt_interrupt = _RAND_77[0:0];
  _RAND_78 = {1{`RANDOM}};
  mem_reg_valid = _RAND_78[0:0];
  _RAND_79 = {1{`RANDOM}};
  mem_reg_rvc = _RAND_79[0:0];
  _RAND_80 = {1{`RANDOM}};
  mem_reg_btb_resp_taken = _RAND_80[0:0];
  _RAND_81 = {1{`RANDOM}};
  mem_reg_btb_resp_entry = _RAND_81[4:0];
  _RAND_82 = {1{`RANDOM}};
  mem_reg_btb_resp_bht_history = _RAND_82[7:0];
  _RAND_83 = {1{`RANDOM}};
  mem_reg_xcpt = _RAND_83[0:0];
  _RAND_84 = {1{`RANDOM}};
  mem_reg_replay = _RAND_84[0:0];
  _RAND_85 = {1{`RANDOM}};
  mem_reg_flush_pipe = _RAND_85[0:0];
  _RAND_86 = {2{`RANDOM}};
  mem_reg_cause = _RAND_86[63:0];
  _RAND_87 = {1{`RANDOM}};
  mem_reg_slow_bypass = _RAND_87[0:0];
  _RAND_88 = {1{`RANDOM}};
  mem_reg_load = _RAND_88[0:0];
  _RAND_89 = {1{`RANDOM}};
  mem_reg_store = _RAND_89[0:0];
  _RAND_90 = {1{`RANDOM}};
  mem_reg_sfence = _RAND_90[0:0];
  _RAND_91 = {2{`RANDOM}};
  mem_reg_pc = _RAND_91[39:0];
  _RAND_92 = {1{`RANDOM}};
  mem_reg_inst = _RAND_92[31:0];
  _RAND_93 = {1{`RANDOM}};
  mem_reg_mem_size = _RAND_93[1:0];
  _RAND_94 = {1{`RANDOM}};
  mem_reg_raw_inst = _RAND_94[31:0];
  _RAND_95 = {1{`RANDOM}};
  mem_scie_pipelined = _RAND_95[0:0];
  _RAND_96 = {2{`RANDOM}};
  mem_reg_wdata = _RAND_96[63:0];
  _RAND_97 = {2{`RANDOM}};
  mem_reg_rs2 = _RAND_97[63:0];
  _RAND_98 = {1{`RANDOM}};
  mem_br_taken = _RAND_98[0:0];
  _RAND_99 = {1{`RANDOM}};
  mem_reg_wphit_0 = _RAND_99[0:0];
  _RAND_100 = {1{`RANDOM}};
  mem_reg_wphit_1 = _RAND_100[0:0];
  _RAND_101 = {1{`RANDOM}};
  mem_reg_wphit_2 = _RAND_101[0:0];
  _RAND_102 = {1{`RANDOM}};
  mem_reg_wphit_3 = _RAND_102[0:0];
  _RAND_103 = {1{`RANDOM}};
  mem_reg_wphit_4 = _RAND_103[0:0];
  _RAND_104 = {1{`RANDOM}};
  mem_reg_wphit_5 = _RAND_104[0:0];
  _RAND_105 = {1{`RANDOM}};
  mem_reg_wphit_6 = _RAND_105[0:0];
  _RAND_106 = {1{`RANDOM}};
  mem_reg_wphit_7 = _RAND_106[0:0];
  _RAND_107 = {1{`RANDOM}};
  mem_reg_wphit_8 = _RAND_107[0:0];
  _RAND_108 = {1{`RANDOM}};
  mem_reg_wphit_9 = _RAND_108[0:0];
  _RAND_109 = {1{`RANDOM}};
  mem_reg_wphit_10 = _RAND_109[0:0];
  _RAND_110 = {1{`RANDOM}};
  mem_reg_wphit_11 = _RAND_110[0:0];
  _RAND_111 = {1{`RANDOM}};
  mem_reg_wphit_12 = _RAND_111[0:0];
  _RAND_112 = {1{`RANDOM}};
  mem_reg_wphit_13 = _RAND_112[0:0];
  _RAND_113 = {1{`RANDOM}};
  mem_reg_wphit_14 = _RAND_113[0:0];
  _RAND_114 = {1{`RANDOM}};
  mem_reg_wphit_15 = _RAND_114[0:0];
  _RAND_115 = {1{`RANDOM}};
  wb_reg_valid = _RAND_115[0:0];
  _RAND_116 = {1{`RANDOM}};
  wb_reg_xcpt = _RAND_116[0:0];
  _RAND_117 = {1{`RANDOM}};
  wb_reg_replay = _RAND_117[0:0];
  _RAND_118 = {1{`RANDOM}};
  wb_reg_flush_pipe = _RAND_118[0:0];
  _RAND_119 = {2{`RANDOM}};
  wb_reg_cause = _RAND_119[63:0];
  _RAND_120 = {1{`RANDOM}};
  wb_reg_sfence = _RAND_120[0:0];
  _RAND_121 = {2{`RANDOM}};
  wb_reg_pc = _RAND_121[39:0];
  _RAND_122 = {1{`RANDOM}};
  wb_reg_mem_size = _RAND_122[1:0];
  _RAND_123 = {1{`RANDOM}};
  wb_reg_inst = _RAND_123[31:0];
  _RAND_124 = {1{`RANDOM}};
  wb_reg_raw_inst = _RAND_124[31:0];
  _RAND_125 = {2{`RANDOM}};
  wb_reg_wdata = _RAND_125[63:0];
  _RAND_126 = {2{`RANDOM}};
  wb_reg_rs2 = _RAND_126[63:0];
  _RAND_127 = {1{`RANDOM}};
  wb_reg_wphit_0 = _RAND_127[0:0];
  _RAND_128 = {1{`RANDOM}};
  wb_reg_wphit_1 = _RAND_128[0:0];
  _RAND_129 = {1{`RANDOM}};
  wb_reg_wphit_2 = _RAND_129[0:0];
  _RAND_130 = {1{`RANDOM}};
  wb_reg_wphit_3 = _RAND_130[0:0];
  _RAND_131 = {1{`RANDOM}};
  wb_reg_wphit_4 = _RAND_131[0:0];
  _RAND_132 = {1{`RANDOM}};
  wb_reg_wphit_5 = _RAND_132[0:0];
  _RAND_133 = {1{`RANDOM}};
  wb_reg_wphit_6 = _RAND_133[0:0];
  _RAND_134 = {1{`RANDOM}};
  wb_reg_wphit_7 = _RAND_134[0:0];
  _RAND_135 = {1{`RANDOM}};
  wb_reg_wphit_8 = _RAND_135[0:0];
  _RAND_136 = {1{`RANDOM}};
  wb_reg_wphit_9 = _RAND_136[0:0];
  _RAND_137 = {1{`RANDOM}};
  wb_reg_wphit_10 = _RAND_137[0:0];
  _RAND_138 = {1{`RANDOM}};
  wb_reg_wphit_11 = _RAND_138[0:0];
  _RAND_139 = {1{`RANDOM}};
  wb_reg_wphit_12 = _RAND_139[0:0];
  _RAND_140 = {1{`RANDOM}};
  wb_reg_wphit_13 = _RAND_140[0:0];
  _RAND_141 = {1{`RANDOM}};
  wb_reg_wphit_14 = _RAND_141[0:0];
  _RAND_142 = {1{`RANDOM}};
  wb_reg_wphit_15 = _RAND_142[0:0];
  _RAND_143 = {1{`RANDOM}};
  id_reg_fence = _RAND_143[0:0];
  _RAND_144 = {1{`RANDOM}};
  ex_reg_rs_bypass_0 = _RAND_144[0:0];
  _RAND_145 = {1{`RANDOM}};
  ex_reg_rs_bypass_1 = _RAND_145[0:0];
  _RAND_146 = {1{`RANDOM}};
  ex_reg_rs_lsb_0 = _RAND_146[1:0];
  _RAND_147 = {1{`RANDOM}};
  ex_reg_rs_lsb_1 = _RAND_147[1:0];
  _RAND_148 = {2{`RANDOM}};
  ex_reg_rs_msb_0 = _RAND_148[61:0];
  _RAND_149 = {2{`RANDOM}};
  ex_reg_rs_msb_1 = _RAND_149[61:0];
  _RAND_150 = {1{`RANDOM}};
  state = _RAND_150[31:0];
  _RAND_151 = {1{`RANDOM}};
  id_stall_fpu_state = _RAND_151[31:0];
  _RAND_152 = {1{`RANDOM}};
  blocked = _RAND_152[0:0];
  _RAND_153 = {1{`RANDOM}};
  div_io_kill_REG = _RAND_153[0:0];
  _RAND_154 = {1{`RANDOM}};
  icache_blocked_REG = _RAND_154[0:0];
  _RAND_155 = {1{`RANDOM}};
  csr_io_counters_0_inc_sets_r = _RAND_155[0:0];
  _RAND_156 = {1{`RANDOM}};
  csr_io_counters_0_inc_sets_r_1 = _RAND_156[0:0];
  _RAND_157 = {1{`RANDOM}};
  csr_io_counters_0_inc_sets_r_2 = _RAND_157[0:0];
  _RAND_158 = {1{`RANDOM}};
  csr_io_counters_0_inc_REG = _RAND_158[0:0];
  _RAND_159 = {1{`RANDOM}};
  csr_io_counters_1_inc_sets_r = _RAND_159[0:0];
  _RAND_160 = {1{`RANDOM}};
  csr_io_counters_1_inc_sets_r_1 = _RAND_160[0:0];
  _RAND_161 = {1{`RANDOM}};
  csr_io_counters_1_inc_sets_r_2 = _RAND_161[0:0];
  _RAND_162 = {1{`RANDOM}};
  csr_io_counters_1_inc_REG = _RAND_162[0:0];
  _RAND_163 = {1{`RANDOM}};
  csr_io_counters_2_inc_sets_r = _RAND_163[0:0];
  _RAND_164 = {1{`RANDOM}};
  csr_io_counters_2_inc_sets_r_1 = _RAND_164[0:0];
  _RAND_165 = {1{`RANDOM}};
  csr_io_counters_2_inc_sets_r_2 = _RAND_165[0:0];
  _RAND_166 = {1{`RANDOM}};
  csr_io_counters_2_inc_REG = _RAND_166[0:0];
  _RAND_167 = {1{`RANDOM}};
  csr_io_counters_3_inc_sets_r = _RAND_167[0:0];
  _RAND_168 = {1{`RANDOM}};
  csr_io_counters_3_inc_sets_r_1 = _RAND_168[0:0];
  _RAND_169 = {1{`RANDOM}};
  csr_io_counters_3_inc_sets_r_2 = _RAND_169[0:0];
  _RAND_170 = {1{`RANDOM}};
  csr_io_counters_3_inc_REG = _RAND_170[0:0];
  _RAND_171 = {2{`RANDOM}};
  coreMonitorBundle_rd0val_x20 = _RAND_171[63:0];
  _RAND_172 = {2{`RANDOM}};
  coreMonitorBundle_rd0val_REG = _RAND_172[63:0];
  _RAND_173 = {2{`RANDOM}};
  coreMonitorBundle_rd1val_x26 = _RAND_173[63:0];
  _RAND_174 = {2{`RANDOM}};
  coreMonitorBundle_rd1val_REG = _RAND_174[63:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    clock_en_reg = 1'h0;
  end
  if (rf_reset) begin
    long_latency_stall = 1'h0;
  end
  if (rf_reset) begin
    id_reg_pause = 1'h0;
  end
  if (rf_reset) begin
    imem_might_request_reg = 1'h0;
  end
  if (rf_reset) begin
    ex_ctrl_fp = 1'h0;
  end
  if (rf_reset) begin
    ex_ctrl_branch = 1'h0;
  end
  if (rf_reset) begin
    ex_ctrl_jal = 1'h0;
  end
  if (rf_reset) begin
    ex_ctrl_jalr = 1'h0;
  end
  if (rf_reset) begin
    ex_ctrl_rxs2 = 1'h0;
  end
  if (rf_reset) begin
    ex_ctrl_sel_alu2 = 2'h0;
  end
  if (rf_reset) begin
    ex_ctrl_sel_alu1 = 2'h0;
  end
  if (rf_reset) begin
    ex_ctrl_sel_imm = 3'h0;
  end
  if (rf_reset) begin
    ex_ctrl_alu_dw = 1'h0;
  end
  if (rf_reset) begin
    ex_ctrl_alu_fn = 4'h0;
  end
  if (rf_reset) begin
    ex_ctrl_mem = 1'h0;
  end
  if (rf_reset) begin
    ex_ctrl_mem_cmd = 5'h0;
  end
  if (rf_reset) begin
    ex_ctrl_wfd = 1'h0;
  end
  if (rf_reset) begin
    ex_ctrl_mul = 1'h0;
  end
  if (rf_reset) begin
    ex_ctrl_div = 1'h0;
  end
  if (rf_reset) begin
    ex_ctrl_wxd = 1'h0;
  end
  if (rf_reset) begin
    ex_ctrl_csr = 3'h0;
  end
  if (rf_reset) begin
    ex_ctrl_fence_i = 1'h0;
  end
  if (rf_reset) begin
    mem_ctrl_fp = 1'h0;
  end
  if (rf_reset) begin
    mem_ctrl_branch = 1'h0;
  end
  if (rf_reset) begin
    mem_ctrl_jal = 1'h0;
  end
  if (rf_reset) begin
    mem_ctrl_jalr = 1'h0;
  end
  if (rf_reset) begin
    mem_ctrl_mem = 1'h0;
  end
  if (rf_reset) begin
    mem_ctrl_wfd = 1'h0;
  end
  if (rf_reset) begin
    mem_ctrl_mul = 1'h0;
  end
  if (rf_reset) begin
    mem_ctrl_div = 1'h0;
  end
  if (rf_reset) begin
    mem_ctrl_wxd = 1'h0;
  end
  if (rf_reset) begin
    mem_ctrl_csr = 3'h0;
  end
  if (rf_reset) begin
    mem_ctrl_fence_i = 1'h0;
  end
  if (rf_reset) begin
    wb_ctrl_fp = 1'h0;
  end
  if (rf_reset) begin
    wb_ctrl_mem = 1'h0;
  end
  if (rf_reset) begin
    wb_ctrl_wfd = 1'h0;
  end
  if (rf_reset) begin
    wb_ctrl_mul = 1'h0;
  end
  if (rf_reset) begin
    wb_ctrl_div = 1'h0;
  end
  if (rf_reset) begin
    wb_ctrl_wxd = 1'h0;
  end
  if (rf_reset) begin
    wb_ctrl_csr = 3'h0;
  end
  if (rf_reset) begin
    wb_ctrl_fence_i = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_xcpt_interrupt = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_valid = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_rvc = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_btb_resp_taken = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_btb_resp_entry = 5'h0;
  end
  if (rf_reset) begin
    ex_reg_btb_resp_bht_history = 8'h0;
  end
  if (rf_reset) begin
    ex_reg_xcpt = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_flush_pipe = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_load_use = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_cause = 64'h0;
  end
  if (rf_reset) begin
    ex_reg_replay = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_pc = 40'h0;
  end
  if (rf_reset) begin
    ex_reg_mem_size = 2'h0;
  end
  if (rf_reset) begin
    ex_reg_inst = 32'h0;
  end
  if (rf_reset) begin
    ex_reg_raw_inst = 32'h0;
  end
  if (rf_reset) begin
    ex_scie_unpipelined = 1'h0;
  end
  if (rf_reset) begin
    ex_scie_pipelined = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_0 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_1 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_2 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_3 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_4 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_5 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_6 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_7 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_8 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_9 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_10 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_11 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_12 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_13 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_14 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_wphit_15 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_xcpt_interrupt = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_valid = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_rvc = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_btb_resp_taken = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_btb_resp_entry = 5'h0;
  end
  if (rf_reset) begin
    mem_reg_btb_resp_bht_history = 8'h0;
  end
  if (rf_reset) begin
    mem_reg_xcpt = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_replay = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_flush_pipe = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_cause = 64'h0;
  end
  if (rf_reset) begin
    mem_reg_slow_bypass = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_load = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_store = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_sfence = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_pc = 40'h0;
  end
  if (rf_reset) begin
    mem_reg_inst = 32'h0;
  end
  if (rf_reset) begin
    mem_reg_mem_size = 2'h0;
  end
  if (rf_reset) begin
    mem_reg_raw_inst = 32'h0;
  end
  if (rf_reset) begin
    mem_scie_pipelined = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wdata = 64'h0;
  end
  if (rf_reset) begin
    mem_reg_rs2 = 64'h0;
  end
  if (rf_reset) begin
    mem_br_taken = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_0 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_1 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_2 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_3 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_4 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_5 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_6 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_7 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_8 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_9 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_10 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_11 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_12 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_13 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_14 = 1'h0;
  end
  if (rf_reset) begin
    mem_reg_wphit_15 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_valid = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_xcpt = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_replay = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_flush_pipe = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_cause = 64'h0;
  end
  if (rf_reset) begin
    wb_reg_sfence = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_pc = 40'h0;
  end
  if (rf_reset) begin
    wb_reg_mem_size = 2'h0;
  end
  if (rf_reset) begin
    wb_reg_inst = 32'h0;
  end
  if (rf_reset) begin
    wb_reg_raw_inst = 32'h0;
  end
  if (rf_reset) begin
    wb_reg_wdata = 64'h0;
  end
  if (rf_reset) begin
    wb_reg_rs2 = 64'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_0 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_1 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_2 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_3 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_4 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_5 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_6 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_7 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_8 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_9 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_10 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_11 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_12 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_13 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_14 = 1'h0;
  end
  if (rf_reset) begin
    wb_reg_wphit_15 = 1'h0;
  end
  if (rf_reset) begin
    id_reg_fence = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_rs_bypass_0 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_rs_bypass_1 = 1'h0;
  end
  if (rf_reset) begin
    ex_reg_rs_lsb_0 = 2'h0;
  end
  if (rf_reset) begin
    ex_reg_rs_lsb_1 = 2'h0;
  end
  if (rf_reset) begin
    ex_reg_rs_msb_0 = 62'h0;
  end
  if (rf_reset) begin
    ex_reg_rs_msb_1 = 62'h0;
  end
  if (rf_reset) begin
    state = 32'h0;
  end
  if (rf_reset) begin
    id_stall_fpu_state = 32'h0;
  end
  if (rf_reset) begin
    blocked = 1'h0;
  end
  if (rf_reset) begin
    div_io_kill_REG = 1'h0;
  end
  if (rf_reset) begin
    icache_blocked_REG = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_0_inc_sets_r = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_0_inc_sets_r_1 = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_0_inc_sets_r_2 = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_0_inc_REG = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_1_inc_sets_r = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_1_inc_sets_r_1 = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_1_inc_sets_r_2 = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_1_inc_REG = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_2_inc_sets_r = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_2_inc_sets_r_1 = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_2_inc_sets_r_2 = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_2_inc_REG = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_3_inc_sets_r = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_3_inc_sets_r_1 = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_3_inc_sets_r_2 = 1'h0;
  end
  if (rf_reset) begin
    csr_io_counters_3_inc_REG = 1'h0;
  end
  if (rf_reset) begin
    coreMonitorBundle_rd0val_x20 = 64'h0;
  end
  if (rf_reset) begin
    coreMonitorBundle_rd0val_REG = 64'h0;
  end
  if (rf_reset) begin
    coreMonitorBundle_rd1val_x26 = 64'h0;
  end
  if (rf_reset) begin
    coreMonitorBundle_rd1val_REG = 64'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
