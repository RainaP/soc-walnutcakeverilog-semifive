//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_69_assert(
  input         clock,
  input         reset,
  input         io_in_a_ready,
  input         io_in_a_valid,
  input  [2:0]  io_in_a_bits_opcode,
  input         io_in_a_bits_source,
  input  [35:0] io_in_a_bits_address,
  input         io_in_d_valid,
  input  [2:0]  io_in_d_bits_opcode,
  input  [1:0]  io_in_d_bits_param,
  input  [3:0]  io_in_d_bits_size,
  input         io_in_d_bits_source,
  input  [3:0]  io_in_d_bits_sink,
  input         io_in_d_bits_denied,
  input         io_in_d_bits_corrupt
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire [35:0] _T_17; // @[Edges.scala 22:16]
  wire  _T_18; // @[Edges.scala 22:24]
  wire [36:0] _T_93; // @[Parameters.scala 137:49]
  wire  _T_101; // @[Monitor.scala 79:25]
  wire [35:0] _T_103; // @[Parameters.scala 137:31]
  wire [36:0] _T_104; // @[Parameters.scala 137:49]
  wire [36:0] _T_106; // @[Parameters.scala 137:52]
  wire  _T_107; // @[Parameters.scala 137:67]
  wire [35:0] _T_108; // @[Parameters.scala 137:31]
  wire [36:0] _T_109; // @[Parameters.scala 137:49]
  wire [35:0] _T_113; // @[Parameters.scala 137:31]
  wire [36:0] _T_114; // @[Parameters.scala 137:49]
  wire [35:0] _T_118; // @[Parameters.scala 137:31]
  wire [36:0] _T_119; // @[Parameters.scala 137:49]
  wire [36:0] _T_121; // @[Parameters.scala 137:52]
  wire  _T_122; // @[Parameters.scala 137:67]
  wire [35:0] _T_123; // @[Parameters.scala 137:31]
  wire [36:0] _T_124; // @[Parameters.scala 137:49]
  wire [36:0] _T_126; // @[Parameters.scala 137:52]
  wire  _T_127; // @[Parameters.scala 137:67]
  wire [36:0] _T_131; // @[Parameters.scala 137:52]
  wire  _T_132; // @[Parameters.scala 137:67]
  wire [35:0] _T_133; // @[Parameters.scala 137:31]
  wire [36:0] _T_134; // @[Parameters.scala 137:49]
  wire [36:0] _T_136; // @[Parameters.scala 137:52]
  wire  _T_137; // @[Parameters.scala 137:67]
  wire [35:0] _T_138; // @[Parameters.scala 137:31]
  wire [36:0] _T_139; // @[Parameters.scala 137:49]
  wire [35:0] _T_143; // @[Parameters.scala 137:31]
  wire [36:0] _T_144; // @[Parameters.scala 137:49]
  wire [36:0] _T_146; // @[Parameters.scala 137:52]
  wire  _T_147; // @[Parameters.scala 137:67]
  wire [35:0] _T_148; // @[Parameters.scala 137:31]
  wire [36:0] _T_149; // @[Parameters.scala 137:49]
  wire [36:0] _T_151; // @[Parameters.scala 137:52]
  wire  _T_152; // @[Parameters.scala 137:67]
  wire [35:0] _T_165; // @[Parameters.scala 137:31]
  wire [36:0] _T_166; // @[Parameters.scala 137:49]
  wire [36:0] _T_168; // @[Parameters.scala 137:52]
  wire  _T_169; // @[Parameters.scala 137:67]
  wire [35:0] _T_170; // @[Parameters.scala 137:31]
  wire [36:0] _T_171; // @[Parameters.scala 137:49]
  wire [36:0] _T_173; // @[Parameters.scala 137:52]
  wire  _T_174; // @[Parameters.scala 137:67]
  wire  _T_175; // @[Parameters.scala 552:42]
  wire  _T_180; // @[Monitor.scala 44:11]
  wire  _T_181; // @[Monitor.scala 44:11]
  wire  _T_184; // @[Monitor.scala 44:11]
  wire  _T_193; // @[Monitor.scala 44:11]
  wire  _T_194; // @[Monitor.scala 44:11]
  wire  _T_208; // @[Monitor.scala 90:25]
  wire  _T_319; // @[Monitor.scala 102:25]
  wire [36:0] _T_332; // @[Parameters.scala 137:52]
  wire  _T_333; // @[Parameters.scala 137:67]
  wire [36:0] _T_362; // @[Parameters.scala 137:52]
  wire  _T_363; // @[Parameters.scala 137:67]
  wire  _T_374; // @[Parameters.scala 552:42]
  wire  _T_375; // @[Parameters.scala 552:42]
  wire  _T_376; // @[Parameters.scala 552:42]
  wire  _T_377; // @[Parameters.scala 552:42]
  wire  _T_378; // @[Parameters.scala 552:42]
  wire  _T_379; // @[Parameters.scala 552:42]
  wire  _T_380; // @[Parameters.scala 552:42]
  wire  _T_381; // @[Parameters.scala 552:42]
  wire  _T_382; // @[Parameters.scala 552:42]
  wire [36:0] _T_391; // @[Parameters.scala 137:52]
  wire  _T_392; // @[Parameters.scala 137:67]
  wire  _T_395; // @[Parameters.scala 553:30]
  wire  _T_397; // @[Monitor.scala 44:11]
  wire  _T_398; // @[Monitor.scala 44:11]
  wire  _T_417; // @[Monitor.scala 111:25]
  wire  _T_477; // @[Parameters.scala 552:42]
  wire  _T_478; // @[Parameters.scala 552:42]
  wire  _T_479; // @[Parameters.scala 552:42]
  wire  _T_480; // @[Parameters.scala 552:42]
  wire  _T_481; // @[Parameters.scala 552:42]
  wire  _T_482; // @[Parameters.scala 552:42]
  wire  _T_483; // @[Parameters.scala 552:42]
  wire  _T_484; // @[Parameters.scala 552:42]
  wire  _T_497; // @[Parameters.scala 553:30]
  wire  _T_498; // @[Parameters.scala 553:30]
  wire  _T_500; // @[Monitor.scala 44:11]
  wire  _T_501; // @[Monitor.scala 44:11]
  wire  _T_516; // @[Monitor.scala 119:25]
  wire  _T_617; // @[Monitor.scala 127:25]
  wire  _T_708; // @[Monitor.scala 135:25]
  wire  _T_799; // @[Monitor.scala 143:25]
  wire  _T_883; // @[Parameters.scala 553:30]
  wire  _T_885; // @[Monitor.scala 44:11]
  wire  _T_886; // @[Monitor.scala 44:11]
  wire  _T_905; // @[Bundles.scala 44:24]
  wire  _T_907; // @[Monitor.scala 51:11]
  wire  _T_908; // @[Monitor.scala 51:11]
  wire  _T_919; // @[Monitor.scala 307:25]
  wire  _T_923; // @[Monitor.scala 309:27]
  wire  _T_925; // @[Monitor.scala 51:11]
  wire  _T_926; // @[Monitor.scala 51:11]
  wire  _T_927; // @[Monitor.scala 310:28]
  wire  _T_929; // @[Monitor.scala 51:11]
  wire  _T_930; // @[Monitor.scala 51:11]
  wire  _T_931; // @[Monitor.scala 311:15]
  wire  _T_933; // @[Monitor.scala 51:11]
  wire  _T_934; // @[Monitor.scala 51:11]
  wire  _T_935; // @[Monitor.scala 312:15]
  wire  _T_937; // @[Monitor.scala 51:11]
  wire  _T_938; // @[Monitor.scala 51:11]
  wire  _T_939; // @[Monitor.scala 315:25]
  wire  _T_950; // @[Bundles.scala 104:26]
  wire  _T_952; // @[Monitor.scala 51:11]
  wire  _T_953; // @[Monitor.scala 51:11]
  wire  _T_954; // @[Monitor.scala 320:28]
  wire  _T_956; // @[Monitor.scala 51:11]
  wire  _T_957; // @[Monitor.scala 51:11]
  wire  _T_967; // @[Monitor.scala 325:25]
  wire  _T_987; // @[Monitor.scala 331:30]
  wire  _T_989; // @[Monitor.scala 51:11]
  wire  _T_990; // @[Monitor.scala 51:11]
  wire  _T_996; // @[Monitor.scala 335:25]
  wire  _T_1013; // @[Monitor.scala 343:25]
  wire  _T_1031; // @[Monitor.scala 351:25]
  wire  _T_1063; // @[Decoupled.scala 40:37]
  wire  _T_1070; // @[Edges.scala 93:28]
  reg [4:0] _T_1072; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire [4:0] _T_1074; // @[Edges.scala 231:28]
  wire  _T_1075; // @[Edges.scala 232:25]
  reg [2:0] _T_1083; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg  _T_1086; // @[Monitor.scala 384:22]
  reg [31:0] _RAND_2;
  reg [35:0] _T_1087; // @[Monitor.scala 385:22]
  reg [63:0] _RAND_3;
  wire  _T_1088; // @[Monitor.scala 386:22]
  wire  _T_1089; // @[Monitor.scala 386:19]
  wire  _T_1090; // @[Monitor.scala 387:32]
  wire  _T_1092; // @[Monitor.scala 44:11]
  wire  _T_1093; // @[Monitor.scala 44:11]
  wire  _T_1102; // @[Monitor.scala 390:32]
  wire  _T_1104; // @[Monitor.scala 44:11]
  wire  _T_1105; // @[Monitor.scala 44:11]
  wire  _T_1106; // @[Monitor.scala 391:32]
  wire  _T_1108; // @[Monitor.scala 44:11]
  wire  _T_1109; // @[Monitor.scala 44:11]
  wire  _T_1111; // @[Monitor.scala 393:20]
  wire [22:0] _T_1114; // @[package.scala 189:77]
  wire [7:0] _T_1116; // @[package.scala 189:46]
  reg [4:0] _T_1120; // @[Edges.scala 230:27]
  reg [31:0] _RAND_4;
  wire [4:0] _T_1122; // @[Edges.scala 231:28]
  wire  _T_1123; // @[Edges.scala 232:25]
  reg [2:0] _T_1131; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_5;
  reg [1:0] _T_1132; // @[Monitor.scala 533:22]
  reg [31:0] _RAND_6;
  reg [3:0] _T_1133; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_7;
  reg  _T_1134; // @[Monitor.scala 535:22]
  reg [31:0] _RAND_8;
  reg [3:0] _T_1135; // @[Monitor.scala 536:22]
  reg [31:0] _RAND_9;
  reg  _T_1136; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_10;
  wire  _T_1137; // @[Monitor.scala 538:22]
  wire  _T_1138; // @[Monitor.scala 538:19]
  wire  _T_1139; // @[Monitor.scala 539:29]
  wire  _T_1141; // @[Monitor.scala 51:11]
  wire  _T_1142; // @[Monitor.scala 51:11]
  wire  _T_1143; // @[Monitor.scala 540:29]
  wire  _T_1145; // @[Monitor.scala 51:11]
  wire  _T_1146; // @[Monitor.scala 51:11]
  wire  _T_1147; // @[Monitor.scala 541:29]
  wire  _T_1149; // @[Monitor.scala 51:11]
  wire  _T_1150; // @[Monitor.scala 51:11]
  wire  _T_1151; // @[Monitor.scala 542:29]
  wire  _T_1153; // @[Monitor.scala 51:11]
  wire  _T_1154; // @[Monitor.scala 51:11]
  wire  _T_1155; // @[Monitor.scala 543:29]
  wire  _T_1157; // @[Monitor.scala 51:11]
  wire  _T_1158; // @[Monitor.scala 51:11]
  wire  _T_1159; // @[Monitor.scala 544:29]
  wire  _T_1161; // @[Monitor.scala 51:11]
  wire  _T_1162; // @[Monitor.scala 51:11]
  wire  _T_1164; // @[Monitor.scala 546:20]
  reg [1:0] _T_1165; // @[Monitor.scala 568:27]
  reg [31:0] _RAND_11;
  reg [4:0] _T_1175; // @[Edges.scala 230:27]
  reg [31:0] _RAND_12;
  wire [4:0] _T_1177; // @[Edges.scala 231:28]
  wire  _T_1178; // @[Edges.scala 232:25]
  reg [4:0] _T_1194; // @[Edges.scala 230:27]
  reg [31:0] _RAND_13;
  wire [4:0] _T_1196; // @[Edges.scala 231:28]
  wire  _T_1197; // @[Edges.scala 232:25]
  wire  _T_1207; // @[Monitor.scala 574:27]
  wire [1:0] _T_1209; // @[OneHot.scala 58:35]
  wire [1:0] _T_1210; // @[Monitor.scala 576:23]
  wire  _T_1212; // @[Monitor.scala 576:14]
  wire  _T_1214; // @[Monitor.scala 576:13]
  wire  _T_1215; // @[Monitor.scala 576:13]
  wire [1:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_1219; // @[Monitor.scala 581:27]
  wire  _T_1221; // @[Monitor.scala 581:75]
  wire  _T_1222; // @[Monitor.scala 581:72]
  wire [1:0] _T_1223; // @[OneHot.scala 58:35]
  wire [1:0] _T_1224; // @[Monitor.scala 583:21]
  wire [1:0] _T_1225; // @[Monitor.scala 583:32]
  wire  _T_1228; // @[Monitor.scala 51:11]
  wire  _T_1229; // @[Monitor.scala 51:11]
  wire [1:0] _GEN_16; // @[Monitor.scala 581:91]
  wire  _T_1230; // @[Monitor.scala 587:20]
  wire  _T_1231; // @[Monitor.scala 587:40]
  wire  _T_1232; // @[Monitor.scala 587:33]
  wire  _T_1233; // @[Monitor.scala 587:30]
  wire  _T_1235; // @[Monitor.scala 51:11]
  wire  _T_1236; // @[Monitor.scala 51:11]
  wire [1:0] _T_1237; // @[Monitor.scala 590:27]
  wire [1:0] _T_1238; // @[Monitor.scala 590:38]
  wire [1:0] _T_1239; // @[Monitor.scala 590:36]
  reg [31:0] _T_1240; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_14;
  wire  _T_1241; // @[Monitor.scala 595:23]
  wire  _T_1242; // @[Monitor.scala 595:13]
  wire  _T_1243; // @[Monitor.scala 595:36]
  wire  _T_1244; // @[Monitor.scala 595:27]
  wire  _T_1245; // @[Monitor.scala 595:56]
  wire  _T_1246; // @[Monitor.scala 595:44]
  wire  _T_1248; // @[Monitor.scala 595:12]
  wire  _T_1249; // @[Monitor.scala 595:12]
  wire [31:0] _T_1251; // @[Monitor.scala 597:26]
  wire  _T_1254; // @[Monitor.scala 598:27]
  wire  _GEN_18; // @[Monitor.scala 44:11]
  wire  _GEN_24; // @[Monitor.scala 44:11]
  wire  _GEN_32; // @[Monitor.scala 44:11]
  wire  _GEN_36; // @[Monitor.scala 44:11]
  wire  _GEN_40; // @[Monitor.scala 44:11]
  wire  _GEN_44; // @[Monitor.scala 44:11]
  wire  _GEN_48; // @[Monitor.scala 44:11]
  wire  _GEN_52; // @[Monitor.scala 44:11]
  wire  _GEN_56; // @[Monitor.scala 51:11]
  wire  _GEN_64; // @[Monitor.scala 51:11]
  wire  _GEN_72; // @[Monitor.scala 51:11]
  wire  _GEN_80; // @[Monitor.scala 51:11]
  wire  _GEN_84; // @[Monitor.scala 51:11]
  wire  _GEN_88; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_17 = io_in_a_bits_address & 36'h3f; // @[Edges.scala 22:16]
  assign _T_18 = _T_17 == 36'h0; // @[Edges.scala 22:24]
  assign _T_93 = {1'b0,$signed(io_in_a_bits_address)}; // @[Parameters.scala 137:49]
  assign _T_101 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_103 = io_in_a_bits_address ^ 36'h400000000; // @[Parameters.scala 137:31]
  assign _T_104 = {1'b0,$signed(_T_103)}; // @[Parameters.scala 137:49]
  assign _T_106 = $signed(_T_104) & -37'sh400000000; // @[Parameters.scala 137:52]
  assign _T_107 = $signed(_T_106) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_108 = io_in_a_bits_address ^ 36'h8000000; // @[Parameters.scala 137:31]
  assign _T_109 = {1'b0,$signed(_T_108)}; // @[Parameters.scala 137:49]
  assign _T_113 = io_in_a_bits_address ^ 36'h3000; // @[Parameters.scala 137:31]
  assign _T_114 = {1'b0,$signed(_T_113)}; // @[Parameters.scala 137:49]
  assign _T_118 = io_in_a_bits_address ^ 36'hc000000; // @[Parameters.scala 137:31]
  assign _T_119 = {1'b0,$signed(_T_118)}; // @[Parameters.scala 137:49]
  assign _T_121 = $signed(_T_119) & -37'sh4000000; // @[Parameters.scala 137:52]
  assign _T_122 = $signed(_T_121) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_123 = io_in_a_bits_address ^ 36'h2000000; // @[Parameters.scala 137:31]
  assign _T_124 = {1'b0,$signed(_T_123)}; // @[Parameters.scala 137:49]
  assign _T_126 = $signed(_T_124) & -37'sh10000; // @[Parameters.scala 137:52]
  assign _T_127 = $signed(_T_126) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_131 = $signed(_T_93) & -37'sh10001000; // @[Parameters.scala 137:52]
  assign _T_132 = $signed(_T_131) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_133 = io_in_a_bits_address ^ 36'h20000000; // @[Parameters.scala 137:31]
  assign _T_134 = {1'b0,$signed(_T_133)}; // @[Parameters.scala 137:49]
  assign _T_136 = $signed(_T_134) & -37'sh20000000; // @[Parameters.scala 137:52]
  assign _T_137 = $signed(_T_136) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_138 = io_in_a_bits_address ^ 36'h10001000; // @[Parameters.scala 137:31]
  assign _T_139 = {1'b0,$signed(_T_138)}; // @[Parameters.scala 137:49]
  assign _T_143 = io_in_a_bits_address ^ 36'h4000; // @[Parameters.scala 137:31]
  assign _T_144 = {1'b0,$signed(_T_143)}; // @[Parameters.scala 137:49]
  assign _T_146 = $signed(_T_144) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_147 = $signed(_T_146) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_148 = io_in_a_bits_address ^ 36'h2010000; // @[Parameters.scala 137:31]
  assign _T_149 = {1'b0,$signed(_T_148)}; // @[Parameters.scala 137:49]
  assign _T_151 = $signed(_T_149) & -37'sh4000; // @[Parameters.scala 137:52]
  assign _T_152 = $signed(_T_151) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_165 = io_in_a_bits_address ^ 36'ha000000; // @[Parameters.scala 137:31]
  assign _T_166 = {1'b0,$signed(_T_165)}; // @[Parameters.scala 137:49]
  assign _T_168 = $signed(_T_166) & -37'sh200000; // @[Parameters.scala 137:52]
  assign _T_169 = $signed(_T_168) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_170 = io_in_a_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  assign _T_171 = {1'b0,$signed(_T_170)}; // @[Parameters.scala 137:49]
  assign _T_173 = $signed(_T_171) & -37'sh800000000; // @[Parameters.scala 137:52]
  assign _T_174 = $signed(_T_173) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_175 = _T_169 | _T_174; // @[Parameters.scala 552:42]
  assign _T_180 = _T_175 | reset; // @[Monitor.scala 44:11]
  assign _T_181 = ~_T_180; // @[Monitor.scala 44:11]
  assign _T_184 = ~reset; // @[Monitor.scala 44:11]
  assign _T_193 = _T_18 | reset; // @[Monitor.scala 44:11]
  assign _T_194 = ~_T_193; // @[Monitor.scala 44:11]
  assign _T_208 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_319 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_332 = $signed(_T_109) & -37'sh2200000; // @[Parameters.scala 137:52]
  assign _T_333 = $signed(_T_332) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_362 = $signed(_T_139) & -37'sh3000; // @[Parameters.scala 137:52]
  assign _T_363 = $signed(_T_362) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_374 = _T_107 | _T_333; // @[Parameters.scala 552:42]
  assign _T_375 = _T_374 | _T_174; // @[Parameters.scala 552:42]
  assign _T_376 = _T_375 | _T_122; // @[Parameters.scala 552:42]
  assign _T_377 = _T_376 | _T_127; // @[Parameters.scala 552:42]
  assign _T_378 = _T_377 | _T_132; // @[Parameters.scala 552:42]
  assign _T_379 = _T_378 | _T_137; // @[Parameters.scala 552:42]
  assign _T_380 = _T_379 | _T_363; // @[Parameters.scala 552:42]
  assign _T_381 = _T_380 | _T_147; // @[Parameters.scala 552:42]
  assign _T_382 = _T_381 | _T_152; // @[Parameters.scala 552:42]
  assign _T_391 = $signed(_T_114) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_392 = $signed(_T_391) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_395 = _T_382 | _T_392; // @[Parameters.scala 553:30]
  assign _T_397 = _T_395 | reset; // @[Monitor.scala 44:11]
  assign _T_398 = ~_T_397; // @[Monitor.scala 44:11]
  assign _T_417 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_477 = _T_333 | _T_174; // @[Parameters.scala 552:42]
  assign _T_478 = _T_477 | _T_122; // @[Parameters.scala 552:42]
  assign _T_479 = _T_478 | _T_127; // @[Parameters.scala 552:42]
  assign _T_480 = _T_479 | _T_132; // @[Parameters.scala 552:42]
  assign _T_481 = _T_480 | _T_137; // @[Parameters.scala 552:42]
  assign _T_482 = _T_481 | _T_363; // @[Parameters.scala 552:42]
  assign _T_483 = _T_482 | _T_147; // @[Parameters.scala 552:42]
  assign _T_484 = _T_483 | _T_152; // @[Parameters.scala 552:42]
  assign _T_497 = _T_107 | _T_484; // @[Parameters.scala 553:30]
  assign _T_498 = _T_497 | _T_392; // @[Parameters.scala 553:30]
  assign _T_500 = _T_498 | reset; // @[Monitor.scala 44:11]
  assign _T_501 = ~_T_500; // @[Monitor.scala 44:11]
  assign _T_516 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_617 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_708 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_799 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_883 = _T_175 | _T_392; // @[Parameters.scala 553:30]
  assign _T_885 = _T_883 | reset; // @[Monitor.scala 44:11]
  assign _T_886 = ~_T_885; // @[Monitor.scala 44:11]
  assign _T_905 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_907 = _T_905 | reset; // @[Monitor.scala 51:11]
  assign _T_908 = ~_T_907; // @[Monitor.scala 51:11]
  assign _T_919 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_923 = io_in_d_bits_size >= 4'h3; // @[Monitor.scala 309:27]
  assign _T_925 = _T_923 | reset; // @[Monitor.scala 51:11]
  assign _T_926 = ~_T_925; // @[Monitor.scala 51:11]
  assign _T_927 = io_in_d_bits_param == 2'h0; // @[Monitor.scala 310:28]
  assign _T_929 = _T_927 | reset; // @[Monitor.scala 51:11]
  assign _T_930 = ~_T_929; // @[Monitor.scala 51:11]
  assign _T_931 = ~io_in_d_bits_corrupt; // @[Monitor.scala 311:15]
  assign _T_933 = _T_931 | reset; // @[Monitor.scala 51:11]
  assign _T_934 = ~_T_933; // @[Monitor.scala 51:11]
  assign _T_935 = ~io_in_d_bits_denied; // @[Monitor.scala 312:15]
  assign _T_937 = _T_935 | reset; // @[Monitor.scala 51:11]
  assign _T_938 = ~_T_937; // @[Monitor.scala 51:11]
  assign _T_939 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_950 = io_in_d_bits_param <= 2'h2; // @[Bundles.scala 104:26]
  assign _T_952 = _T_950 | reset; // @[Monitor.scala 51:11]
  assign _T_953 = ~_T_952; // @[Monitor.scala 51:11]
  assign _T_954 = io_in_d_bits_param != 2'h2; // @[Monitor.scala 320:28]
  assign _T_956 = _T_954 | reset; // @[Monitor.scala 51:11]
  assign _T_957 = ~_T_956; // @[Monitor.scala 51:11]
  assign _T_967 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_987 = _T_935 | io_in_d_bits_corrupt; // @[Monitor.scala 331:30]
  assign _T_989 = _T_987 | reset; // @[Monitor.scala 51:11]
  assign _T_990 = ~_T_989; // @[Monitor.scala 51:11]
  assign _T_996 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_1013 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_1031 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_1063 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_1070 = ~io_in_a_bits_opcode[2]; // @[Edges.scala 93:28]
  assign _T_1074 = _T_1072 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1075 = _T_1072 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1088 = ~_T_1075; // @[Monitor.scala 386:22]
  assign _T_1089 = io_in_a_valid & _T_1088; // @[Monitor.scala 386:19]
  assign _T_1090 = io_in_a_bits_opcode == _T_1083; // @[Monitor.scala 387:32]
  assign _T_1092 = _T_1090 | reset; // @[Monitor.scala 44:11]
  assign _T_1093 = ~_T_1092; // @[Monitor.scala 44:11]
  assign _T_1102 = io_in_a_bits_source == _T_1086; // @[Monitor.scala 390:32]
  assign _T_1104 = _T_1102 | reset; // @[Monitor.scala 44:11]
  assign _T_1105 = ~_T_1104; // @[Monitor.scala 44:11]
  assign _T_1106 = io_in_a_bits_address == _T_1087; // @[Monitor.scala 391:32]
  assign _T_1108 = _T_1106 | reset; // @[Monitor.scala 44:11]
  assign _T_1109 = ~_T_1108; // @[Monitor.scala 44:11]
  assign _T_1111 = _T_1063 & _T_1075; // @[Monitor.scala 393:20]
  assign _T_1114 = 23'hff << io_in_d_bits_size; // @[package.scala 189:77]
  assign _T_1116 = ~_T_1114[7:0]; // @[package.scala 189:46]
  assign _T_1122 = _T_1120 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1123 = _T_1120 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1137 = ~_T_1123; // @[Monitor.scala 538:22]
  assign _T_1138 = io_in_d_valid & _T_1137; // @[Monitor.scala 538:19]
  assign _T_1139 = io_in_d_bits_opcode == _T_1131; // @[Monitor.scala 539:29]
  assign _T_1141 = _T_1139 | reset; // @[Monitor.scala 51:11]
  assign _T_1142 = ~_T_1141; // @[Monitor.scala 51:11]
  assign _T_1143 = io_in_d_bits_param == _T_1132; // @[Monitor.scala 540:29]
  assign _T_1145 = _T_1143 | reset; // @[Monitor.scala 51:11]
  assign _T_1146 = ~_T_1145; // @[Monitor.scala 51:11]
  assign _T_1147 = io_in_d_bits_size == _T_1133; // @[Monitor.scala 541:29]
  assign _T_1149 = _T_1147 | reset; // @[Monitor.scala 51:11]
  assign _T_1150 = ~_T_1149; // @[Monitor.scala 51:11]
  assign _T_1151 = io_in_d_bits_source == _T_1134; // @[Monitor.scala 542:29]
  assign _T_1153 = _T_1151 | reset; // @[Monitor.scala 51:11]
  assign _T_1154 = ~_T_1153; // @[Monitor.scala 51:11]
  assign _T_1155 = io_in_d_bits_sink == _T_1135; // @[Monitor.scala 543:29]
  assign _T_1157 = _T_1155 | reset; // @[Monitor.scala 51:11]
  assign _T_1158 = ~_T_1157; // @[Monitor.scala 51:11]
  assign _T_1159 = io_in_d_bits_denied == _T_1136; // @[Monitor.scala 544:29]
  assign _T_1161 = _T_1159 | reset; // @[Monitor.scala 51:11]
  assign _T_1162 = ~_T_1161; // @[Monitor.scala 51:11]
  assign _T_1164 = io_in_d_valid & _T_1123; // @[Monitor.scala 546:20]
  assign _T_1177 = _T_1175 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1178 = _T_1175 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1196 = _T_1194 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1197 = _T_1194 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1207 = _T_1063 & _T_1178; // @[Monitor.scala 574:27]
  assign _T_1209 = 2'h1 << io_in_a_bits_source; // @[OneHot.scala 58:35]
  assign _T_1210 = _T_1165 >> io_in_a_bits_source; // @[Monitor.scala 576:23]
  assign _T_1212 = ~_T_1210[0]; // @[Monitor.scala 576:14]
  assign _T_1214 = _T_1212 | reset; // @[Monitor.scala 576:13]
  assign _T_1215 = ~_T_1214; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_1207 ? _T_1209 : 2'h0; // @[Monitor.scala 574:72]
  assign _T_1219 = io_in_d_valid & _T_1197; // @[Monitor.scala 581:27]
  assign _T_1221 = ~_T_919; // @[Monitor.scala 581:75]
  assign _T_1222 = _T_1219 & _T_1221; // @[Monitor.scala 581:72]
  assign _T_1223 = 2'h1 << io_in_d_bits_source; // @[OneHot.scala 58:35]
  assign _T_1224 = _GEN_15 | _T_1165; // @[Monitor.scala 583:21]
  assign _T_1225 = _T_1224 >> io_in_d_bits_source; // @[Monitor.scala 583:32]
  assign _T_1228 = _T_1225[0] | reset; // @[Monitor.scala 51:11]
  assign _T_1229 = ~_T_1228; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_1222 ? _T_1223 : 2'h0; // @[Monitor.scala 581:91]
  assign _T_1230 = _GEN_15 != _GEN_16; // @[Monitor.scala 587:20]
  assign _T_1231 = _GEN_15 != 2'h0; // @[Monitor.scala 587:40]
  assign _T_1232 = ~_T_1231; // @[Monitor.scala 587:33]
  assign _T_1233 = _T_1230 | _T_1232; // @[Monitor.scala 587:30]
  assign _T_1235 = _T_1233 | reset; // @[Monitor.scala 51:11]
  assign _T_1236 = ~_T_1235; // @[Monitor.scala 51:11]
  assign _T_1237 = _T_1165 | _GEN_15; // @[Monitor.scala 590:27]
  assign _T_1238 = ~_GEN_16; // @[Monitor.scala 590:38]
  assign _T_1239 = _T_1237 & _T_1238; // @[Monitor.scala 590:36]
  assign _T_1241 = _T_1165 != 2'h0; // @[Monitor.scala 595:23]
  assign _T_1242 = ~_T_1241; // @[Monitor.scala 595:13]
  assign _T_1243 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_1244 = _T_1242 | _T_1243; // @[Monitor.scala 595:27]
  assign _T_1245 = _T_1240 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_1246 = _T_1244 | _T_1245; // @[Monitor.scala 595:44]
  assign _T_1248 = _T_1246 | reset; // @[Monitor.scala 595:12]
  assign _T_1249 = ~_T_1248; // @[Monitor.scala 595:12]
  assign _T_1251 = _T_1240 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_1254 = _T_1063 | io_in_d_valid; // @[Monitor.scala 598:27]
  assign _GEN_18 = io_in_a_valid & _T_101; // @[Monitor.scala 44:11]
  assign _GEN_24 = io_in_a_valid & _T_208; // @[Monitor.scala 44:11]
  assign _GEN_32 = io_in_a_valid & _T_319; // @[Monitor.scala 44:11]
  assign _GEN_36 = io_in_a_valid & _T_417; // @[Monitor.scala 44:11]
  assign _GEN_40 = io_in_a_valid & _T_516; // @[Monitor.scala 44:11]
  assign _GEN_44 = io_in_a_valid & _T_617; // @[Monitor.scala 44:11]
  assign _GEN_48 = io_in_a_valid & _T_708; // @[Monitor.scala 44:11]
  assign _GEN_52 = io_in_a_valid & _T_799; // @[Monitor.scala 44:11]
  assign _GEN_56 = io_in_d_valid & _T_919; // @[Monitor.scala 51:11]
  assign _GEN_64 = io_in_d_valid & _T_939; // @[Monitor.scala 51:11]
  assign _GEN_72 = io_in_d_valid & _T_967; // @[Monitor.scala 51:11]
  assign _GEN_80 = io_in_d_valid & _T_996; // @[Monitor.scala 51:11]
  assign _GEN_84 = io_in_d_valid & _T_1013; // @[Monitor.scala 51:11]
  assign _GEN_88 = io_in_d_valid & _T_1031; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_1072 = _RAND_0[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_1083 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_1086 = _RAND_2[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {2{`RANDOM}};
  _T_1087 = _RAND_3[35:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_1120 = _RAND_4[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  _T_1131 = _RAND_5[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_1132 = _RAND_6[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_1133 = _RAND_7[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_1134 = _RAND_8[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_1135 = _RAND_9[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_1136 = _RAND_10[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_1165 = _RAND_11[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_1175 = _RAND_12[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  _T_1194 = _RAND_13[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_14 = {1{`RANDOM}};
  _T_1240 = _RAND_14[31:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_1072 <= 5'h0;
    end else if (_T_1063) begin
      if (_T_1075) begin
        if (_T_1070) begin
          _T_1072 <= 5'h7;
        end else begin
          _T_1072 <= 5'h0;
        end
      end else begin
        _T_1072 <= _T_1074;
      end
    end
    if (_T_1111) begin
      _T_1083 <= io_in_a_bits_opcode;
    end
    if (_T_1111) begin
      _T_1086 <= io_in_a_bits_source;
    end
    if (_T_1111) begin
      _T_1087 <= io_in_a_bits_address;
    end
    if (reset) begin
      _T_1120 <= 5'h0;
    end else if (io_in_d_valid) begin
      if (_T_1123) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1120 <= _T_1116[7:3];
        end else begin
          _T_1120 <= 5'h0;
        end
      end else begin
        _T_1120 <= _T_1122;
      end
    end
    if (_T_1164) begin
      _T_1131 <= io_in_d_bits_opcode;
    end
    if (_T_1164) begin
      _T_1132 <= io_in_d_bits_param;
    end
    if (_T_1164) begin
      _T_1133 <= io_in_d_bits_size;
    end
    if (_T_1164) begin
      _T_1134 <= io_in_d_bits_source;
    end
    if (_T_1164) begin
      _T_1135 <= io_in_d_bits_sink;
    end
    if (_T_1164) begin
      _T_1136 <= io_in_d_bits_denied;
    end
    if (reset) begin
      _T_1165 <= 2'h0;
    end else begin
      _T_1165 <= _T_1239;
    end
    if (reset) begin
      _T_1175 <= 5'h0;
    end else if (_T_1063) begin
      if (_T_1178) begin
        if (_T_1070) begin
          _T_1175 <= 5'h7;
        end else begin
          _T_1175 <= 5'h0;
        end
      end else begin
        _T_1175 <= _T_1177;
      end
    end
    if (reset) begin
      _T_1194 <= 5'h0;
    end else if (io_in_d_valid) begin
      if (_T_1197) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1194 <= _T_1116[7:3];
        end else begin
          _T_1194 <= 5'h0;
        end
      end else begin
        _T_1194 <= _T_1196;
      end
    end
    if (reset) begin
      _T_1240 <= 32'h0;
    end else if (_T_1254) begin
      _T_1240 <= 32'h0;
    end else begin
      _T_1240 <= _T_1251;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_181) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_181) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_184) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_24 & _T_181) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_24 & _T_181) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_24 & _T_184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_24 & _T_184) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_24 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_24 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_24 & _T_184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_24 & _T_184) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_32 & _T_398) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_32 & _T_398) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_32 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_32 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_36 & _T_501) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_36 & _T_501) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_36 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_36 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_40 & _T_501) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_40 & _T_501) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_40 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_40 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_44 & _T_184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_44 & _T_184) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_44 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_44 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_48 & _T_184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_48 & _T_184) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_48 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_48 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_52 & _T_886) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_52 & _T_886) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_52 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_52 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_908) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_908) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_56 & _T_926) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_56 & _T_926) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_56 & _T_930) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseeAck carries invalid param (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_56 & _T_930) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_56 & _T_934) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is corrupt (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_56 & _T_934) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_56 & _T_938) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is denied (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_56 & _T_938) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_926) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_926) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_953) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid cap param (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_953) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_957) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries toN param (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_957) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_64 & _T_934) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is corrupt (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_64 & _T_934) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_72 & _T_926) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_72 & _T_926) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_72 & _T_953) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid cap param (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_72 & _T_953) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_72 & _T_957) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries toN param (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_72 & _T_957) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_72 & _T_990) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied but not corrupt (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_72 & _T_990) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_80 & _T_930) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid param (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_80 & _T_930) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_80 & _T_934) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is corrupt (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_80 & _T_934) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_84 & _T_930) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid param (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_84 & _T_930) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_84 & _T_990) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied but not corrupt (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_84 & _T_990) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_88 & _T_930) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid param (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_88 & _T_930) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_88 & _T_934) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is corrupt (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_88 & _T_934) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1089 & _T_1093) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1089 & _T_1093) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1089 & _T_1105) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel source changed within multibeat operation (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1089 & _T_1105) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1089 & _T_1109) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at Frontend.scala:353:21)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1089 & _T_1109) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1138 & _T_1142) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1138 & _T_1142) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1138 & _T_1146) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel param changed within multibeat operation (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1138 & _T_1146) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1138 & _T_1150) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1138 & _T_1150) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1138 & _T_1154) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel source changed within multibeat operation (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1138 & _T_1154) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1138 & _T_1158) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel sink changed with multibeat operation (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1138 & _T_1158) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1138 & _T_1162) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1138 & _T_1162) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1207 & _T_1215) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at Frontend.scala:353:21)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1207 & _T_1215) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1222 & _T_1229) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1222 & _T_1229) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1236) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' and 'D' concurrent, despite minlatency 1 (connected at Frontend.scala:353:21)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1236) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1249) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at Frontend.scala:353:21)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1249) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
