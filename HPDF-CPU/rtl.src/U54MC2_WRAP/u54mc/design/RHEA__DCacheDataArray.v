//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__DCacheDataArray(
  input          clock,
  input          io_req_valid,
  input  [12:0]  io_req_bits_addr,
  input          io_req_bits_write,
  input  [127:0] io_req_bits_wdata,
  input  [1:0]   io_req_bits_wordMask,
  input  [7:0]   io_req_bits_eccMask,
  input  [3:0]   io_req_bits_way_en,
  output [127:0] io_resp_0,
  output [127:0] io_resp_1,
  output [127:0] io_resp_2,
  output [127:0] io_resp_3
);
  wire [8:0] data_arrays_0_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_0; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_1; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_2; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_3; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_4; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_5; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_6; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_7; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_8; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_9; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_10; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_11; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_12; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_13; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_14; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_15; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_16; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_17; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_18; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_19; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_20; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_21; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_22; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_23; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_24; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_25; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_26; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_27; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_28; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_29; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_30; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_wdata_31; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_0; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_1; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_2; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_3; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_4; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_5; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_6; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_7; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_8; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_9; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_10; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_11; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_12; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_13; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_14; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_15; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_16; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_17; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_18; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_19; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_20; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_21; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_22; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_23; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_24; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_25; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_26; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_27; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_28; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_29; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_30; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_0_RW0_rdata_31; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_0; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_1; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_2; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_3; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_4; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_5; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_6; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_7; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_8; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_9; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_10; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_11; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_12; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_13; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_14; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_15; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_16; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_17; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_18; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_19; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_20; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_21; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_22; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_23; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_24; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_25; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_26; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_27; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_28; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_29; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_30; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_31; // @[DescribedSRAM.scala 18:26]
  wire [8:0] data_arrays_1_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_0; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_1; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_2; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_3; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_4; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_5; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_6; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_7; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_8; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_9; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_10; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_11; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_12; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_13; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_14; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_15; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_16; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_17; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_18; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_19; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_20; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_21; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_22; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_23; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_24; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_25; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_26; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_27; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_28; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_29; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_30; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_wdata_31; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_0; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_1; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_2; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_3; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_4; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_5; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_6; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_7; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_8; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_9; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_10; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_11; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_12; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_13; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_14; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_15; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_16; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_17; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_18; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_19; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_20; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_21; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_22; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_23; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_24; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_25; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_26; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_27; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_28; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_29; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_30; // @[DescribedSRAM.scala 18:26]
  wire [7:0] data_arrays_1_RW0_rdata_31; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_0; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_1; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_2; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_3; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_4; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_5; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_6; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_7; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_8; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_9; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_10; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_11; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_12; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_13; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_14; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_15; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_16; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_17; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_18; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_19; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_20; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_21; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_22; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_23; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_24; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_25; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_26; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_27; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_28; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_29; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_30; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_31; // @[DescribedSRAM.scala 18:26]
  wire  eccMask_0 = io_req_bits_eccMask[0]; // @[DCache.scala 52:82]
  wire  eccMask_1 = io_req_bits_eccMask[1]; // @[DCache.scala 52:82]
  wire  eccMask_2 = io_req_bits_eccMask[2]; // @[DCache.scala 52:82]
  wire  eccMask_3 = io_req_bits_eccMask[3]; // @[DCache.scala 52:82]
  wire  eccMask_4 = io_req_bits_eccMask[4]; // @[DCache.scala 52:82]
  wire  eccMask_5 = io_req_bits_eccMask[5]; // @[DCache.scala 52:82]
  wire  eccMask_6 = io_req_bits_eccMask[6]; // @[DCache.scala 52:82]
  wire  eccMask_7 = io_req_bits_eccMask[7]; // @[DCache.scala 52:82]
  wire [63:0] wWords_0 = io_req_bits_wdata[63:0]; // @[package.scala 202:50]
  wire [63:0] wWords_1 = io_req_bits_wdata[127:64]; // @[package.scala 202:50]
  wire  rdata_valid = io_req_valid & io_req_bits_wordMask[0]; // @[DCache.scala 67:30]
  wire  _rdata_T = rdata_valid & io_req_bits_write; // @[DCache.scala 68:17]
  wire  _rdata_data_T_1 = rdata_valid & ~io_req_bits_write; // @[DCache.scala 73:39]
  wire [63:0] rdata_0_0 = {data_arrays_0_RW0_rdata_7,data_arrays_0_RW0_rdata_6,data_arrays_0_RW0_rdata_5,
    data_arrays_0_RW0_rdata_4,data_arrays_0_RW0_rdata_3,data_arrays_0_RW0_rdata_2,data_arrays_0_RW0_rdata_1,
    data_arrays_0_RW0_rdata_0}; // @[Cat.scala 30:58]
  wire  rdata_valid_1 = io_req_valid & io_req_bits_wordMask[1]; // @[DCache.scala 67:30]
  wire  _rdata_T_1 = rdata_valid_1 & io_req_bits_write; // @[DCache.scala 68:17]
  wire  _rdata_data_T_5 = rdata_valid_1 & ~io_req_bits_write; // @[DCache.scala 73:39]
  wire [63:0] rdata_1_0 = {data_arrays_1_RW0_rdata_7,data_arrays_1_RW0_rdata_6,data_arrays_1_RW0_rdata_5,
    data_arrays_1_RW0_rdata_4,data_arrays_1_RW0_rdata_3,data_arrays_1_RW0_rdata_2,data_arrays_1_RW0_rdata_1,
    data_arrays_1_RW0_rdata_0}; // @[Cat.scala 30:58]
  wire [63:0] rdata_0_1 = {data_arrays_0_RW0_rdata_15,data_arrays_0_RW0_rdata_14,data_arrays_0_RW0_rdata_13,
    data_arrays_0_RW0_rdata_12,data_arrays_0_RW0_rdata_11,data_arrays_0_RW0_rdata_10,data_arrays_0_RW0_rdata_9,
    data_arrays_0_RW0_rdata_8}; // @[Cat.scala 30:58]
  wire [63:0] rdata_0_2 = {data_arrays_0_RW0_rdata_23,data_arrays_0_RW0_rdata_22,data_arrays_0_RW0_rdata_21,
    data_arrays_0_RW0_rdata_20,data_arrays_0_RW0_rdata_19,data_arrays_0_RW0_rdata_18,data_arrays_0_RW0_rdata_17,
    data_arrays_0_RW0_rdata_16}; // @[Cat.scala 30:58]
  wire [63:0] rdata_0_3 = {data_arrays_0_RW0_rdata_31,data_arrays_0_RW0_rdata_30,data_arrays_0_RW0_rdata_29,
    data_arrays_0_RW0_rdata_28,data_arrays_0_RW0_rdata_27,data_arrays_0_RW0_rdata_26,data_arrays_0_RW0_rdata_25,
    data_arrays_0_RW0_rdata_24}; // @[Cat.scala 30:58]
  wire [63:0] rdata_1_1 = {data_arrays_1_RW0_rdata_15,data_arrays_1_RW0_rdata_14,data_arrays_1_RW0_rdata_13,
    data_arrays_1_RW0_rdata_12,data_arrays_1_RW0_rdata_11,data_arrays_1_RW0_rdata_10,data_arrays_1_RW0_rdata_9,
    data_arrays_1_RW0_rdata_8}; // @[Cat.scala 30:58]
  wire [63:0] rdata_1_2 = {data_arrays_1_RW0_rdata_23,data_arrays_1_RW0_rdata_22,data_arrays_1_RW0_rdata_21,
    data_arrays_1_RW0_rdata_20,data_arrays_1_RW0_rdata_19,data_arrays_1_RW0_rdata_18,data_arrays_1_RW0_rdata_17,
    data_arrays_1_RW0_rdata_16}; // @[Cat.scala 30:58]
  wire [63:0] rdata_1_3 = {data_arrays_1_RW0_rdata_31,data_arrays_1_RW0_rdata_30,data_arrays_1_RW0_rdata_29,
    data_arrays_1_RW0_rdata_28,data_arrays_1_RW0_rdata_27,data_arrays_1_RW0_rdata_26,data_arrays_1_RW0_rdata_25,
    data_arrays_1_RW0_rdata_24}; // @[Cat.scala 30:58]
  RHEA__data_arrays_0 data_arrays_0 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(data_arrays_0_RW0_addr),
    .RW0_en(data_arrays_0_RW0_en),
    .RW0_clk(data_arrays_0_RW0_clk),
    .RW0_wmode(data_arrays_0_RW0_wmode),
    .RW0_wdata_0(data_arrays_0_RW0_wdata_0),
    .RW0_wdata_1(data_arrays_0_RW0_wdata_1),
    .RW0_wdata_2(data_arrays_0_RW0_wdata_2),
    .RW0_wdata_3(data_arrays_0_RW0_wdata_3),
    .RW0_wdata_4(data_arrays_0_RW0_wdata_4),
    .RW0_wdata_5(data_arrays_0_RW0_wdata_5),
    .RW0_wdata_6(data_arrays_0_RW0_wdata_6),
    .RW0_wdata_7(data_arrays_0_RW0_wdata_7),
    .RW0_wdata_8(data_arrays_0_RW0_wdata_8),
    .RW0_wdata_9(data_arrays_0_RW0_wdata_9),
    .RW0_wdata_10(data_arrays_0_RW0_wdata_10),
    .RW0_wdata_11(data_arrays_0_RW0_wdata_11),
    .RW0_wdata_12(data_arrays_0_RW0_wdata_12),
    .RW0_wdata_13(data_arrays_0_RW0_wdata_13),
    .RW0_wdata_14(data_arrays_0_RW0_wdata_14),
    .RW0_wdata_15(data_arrays_0_RW0_wdata_15),
    .RW0_wdata_16(data_arrays_0_RW0_wdata_16),
    .RW0_wdata_17(data_arrays_0_RW0_wdata_17),
    .RW0_wdata_18(data_arrays_0_RW0_wdata_18),
    .RW0_wdata_19(data_arrays_0_RW0_wdata_19),
    .RW0_wdata_20(data_arrays_0_RW0_wdata_20),
    .RW0_wdata_21(data_arrays_0_RW0_wdata_21),
    .RW0_wdata_22(data_arrays_0_RW0_wdata_22),
    .RW0_wdata_23(data_arrays_0_RW0_wdata_23),
    .RW0_wdata_24(data_arrays_0_RW0_wdata_24),
    .RW0_wdata_25(data_arrays_0_RW0_wdata_25),
    .RW0_wdata_26(data_arrays_0_RW0_wdata_26),
    .RW0_wdata_27(data_arrays_0_RW0_wdata_27),
    .RW0_wdata_28(data_arrays_0_RW0_wdata_28),
    .RW0_wdata_29(data_arrays_0_RW0_wdata_29),
    .RW0_wdata_30(data_arrays_0_RW0_wdata_30),
    .RW0_wdata_31(data_arrays_0_RW0_wdata_31),
    .RW0_rdata_0(data_arrays_0_RW0_rdata_0),
    .RW0_rdata_1(data_arrays_0_RW0_rdata_1),
    .RW0_rdata_2(data_arrays_0_RW0_rdata_2),
    .RW0_rdata_3(data_arrays_0_RW0_rdata_3),
    .RW0_rdata_4(data_arrays_0_RW0_rdata_4),
    .RW0_rdata_5(data_arrays_0_RW0_rdata_5),
    .RW0_rdata_6(data_arrays_0_RW0_rdata_6),
    .RW0_rdata_7(data_arrays_0_RW0_rdata_7),
    .RW0_rdata_8(data_arrays_0_RW0_rdata_8),
    .RW0_rdata_9(data_arrays_0_RW0_rdata_9),
    .RW0_rdata_10(data_arrays_0_RW0_rdata_10),
    .RW0_rdata_11(data_arrays_0_RW0_rdata_11),
    .RW0_rdata_12(data_arrays_0_RW0_rdata_12),
    .RW0_rdata_13(data_arrays_0_RW0_rdata_13),
    .RW0_rdata_14(data_arrays_0_RW0_rdata_14),
    .RW0_rdata_15(data_arrays_0_RW0_rdata_15),
    .RW0_rdata_16(data_arrays_0_RW0_rdata_16),
    .RW0_rdata_17(data_arrays_0_RW0_rdata_17),
    .RW0_rdata_18(data_arrays_0_RW0_rdata_18),
    .RW0_rdata_19(data_arrays_0_RW0_rdata_19),
    .RW0_rdata_20(data_arrays_0_RW0_rdata_20),
    .RW0_rdata_21(data_arrays_0_RW0_rdata_21),
    .RW0_rdata_22(data_arrays_0_RW0_rdata_22),
    .RW0_rdata_23(data_arrays_0_RW0_rdata_23),
    .RW0_rdata_24(data_arrays_0_RW0_rdata_24),
    .RW0_rdata_25(data_arrays_0_RW0_rdata_25),
    .RW0_rdata_26(data_arrays_0_RW0_rdata_26),
    .RW0_rdata_27(data_arrays_0_RW0_rdata_27),
    .RW0_rdata_28(data_arrays_0_RW0_rdata_28),
    .RW0_rdata_29(data_arrays_0_RW0_rdata_29),
    .RW0_rdata_30(data_arrays_0_RW0_rdata_30),
    .RW0_rdata_31(data_arrays_0_RW0_rdata_31),
    .RW0_wmask_0(data_arrays_0_RW0_wmask_0),
    .RW0_wmask_1(data_arrays_0_RW0_wmask_1),
    .RW0_wmask_2(data_arrays_0_RW0_wmask_2),
    .RW0_wmask_3(data_arrays_0_RW0_wmask_3),
    .RW0_wmask_4(data_arrays_0_RW0_wmask_4),
    .RW0_wmask_5(data_arrays_0_RW0_wmask_5),
    .RW0_wmask_6(data_arrays_0_RW0_wmask_6),
    .RW0_wmask_7(data_arrays_0_RW0_wmask_7),
    .RW0_wmask_8(data_arrays_0_RW0_wmask_8),
    .RW0_wmask_9(data_arrays_0_RW0_wmask_9),
    .RW0_wmask_10(data_arrays_0_RW0_wmask_10),
    .RW0_wmask_11(data_arrays_0_RW0_wmask_11),
    .RW0_wmask_12(data_arrays_0_RW0_wmask_12),
    .RW0_wmask_13(data_arrays_0_RW0_wmask_13),
    .RW0_wmask_14(data_arrays_0_RW0_wmask_14),
    .RW0_wmask_15(data_arrays_0_RW0_wmask_15),
    .RW0_wmask_16(data_arrays_0_RW0_wmask_16),
    .RW0_wmask_17(data_arrays_0_RW0_wmask_17),
    .RW0_wmask_18(data_arrays_0_RW0_wmask_18),
    .RW0_wmask_19(data_arrays_0_RW0_wmask_19),
    .RW0_wmask_20(data_arrays_0_RW0_wmask_20),
    .RW0_wmask_21(data_arrays_0_RW0_wmask_21),
    .RW0_wmask_22(data_arrays_0_RW0_wmask_22),
    .RW0_wmask_23(data_arrays_0_RW0_wmask_23),
    .RW0_wmask_24(data_arrays_0_RW0_wmask_24),
    .RW0_wmask_25(data_arrays_0_RW0_wmask_25),
    .RW0_wmask_26(data_arrays_0_RW0_wmask_26),
    .RW0_wmask_27(data_arrays_0_RW0_wmask_27),
    .RW0_wmask_28(data_arrays_0_RW0_wmask_28),
    .RW0_wmask_29(data_arrays_0_RW0_wmask_29),
    .RW0_wmask_30(data_arrays_0_RW0_wmask_30),
    .RW0_wmask_31(data_arrays_0_RW0_wmask_31)
  );
  RHEA__data_arrays_0 data_arrays_1 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(data_arrays_1_RW0_addr),
    .RW0_en(data_arrays_1_RW0_en),
    .RW0_clk(data_arrays_1_RW0_clk),
    .RW0_wmode(data_arrays_1_RW0_wmode),
    .RW0_wdata_0(data_arrays_1_RW0_wdata_0),
    .RW0_wdata_1(data_arrays_1_RW0_wdata_1),
    .RW0_wdata_2(data_arrays_1_RW0_wdata_2),
    .RW0_wdata_3(data_arrays_1_RW0_wdata_3),
    .RW0_wdata_4(data_arrays_1_RW0_wdata_4),
    .RW0_wdata_5(data_arrays_1_RW0_wdata_5),
    .RW0_wdata_6(data_arrays_1_RW0_wdata_6),
    .RW0_wdata_7(data_arrays_1_RW0_wdata_7),
    .RW0_wdata_8(data_arrays_1_RW0_wdata_8),
    .RW0_wdata_9(data_arrays_1_RW0_wdata_9),
    .RW0_wdata_10(data_arrays_1_RW0_wdata_10),
    .RW0_wdata_11(data_arrays_1_RW0_wdata_11),
    .RW0_wdata_12(data_arrays_1_RW0_wdata_12),
    .RW0_wdata_13(data_arrays_1_RW0_wdata_13),
    .RW0_wdata_14(data_arrays_1_RW0_wdata_14),
    .RW0_wdata_15(data_arrays_1_RW0_wdata_15),
    .RW0_wdata_16(data_arrays_1_RW0_wdata_16),
    .RW0_wdata_17(data_arrays_1_RW0_wdata_17),
    .RW0_wdata_18(data_arrays_1_RW0_wdata_18),
    .RW0_wdata_19(data_arrays_1_RW0_wdata_19),
    .RW0_wdata_20(data_arrays_1_RW0_wdata_20),
    .RW0_wdata_21(data_arrays_1_RW0_wdata_21),
    .RW0_wdata_22(data_arrays_1_RW0_wdata_22),
    .RW0_wdata_23(data_arrays_1_RW0_wdata_23),
    .RW0_wdata_24(data_arrays_1_RW0_wdata_24),
    .RW0_wdata_25(data_arrays_1_RW0_wdata_25),
    .RW0_wdata_26(data_arrays_1_RW0_wdata_26),
    .RW0_wdata_27(data_arrays_1_RW0_wdata_27),
    .RW0_wdata_28(data_arrays_1_RW0_wdata_28),
    .RW0_wdata_29(data_arrays_1_RW0_wdata_29),
    .RW0_wdata_30(data_arrays_1_RW0_wdata_30),
    .RW0_wdata_31(data_arrays_1_RW0_wdata_31),
    .RW0_rdata_0(data_arrays_1_RW0_rdata_0),
    .RW0_rdata_1(data_arrays_1_RW0_rdata_1),
    .RW0_rdata_2(data_arrays_1_RW0_rdata_2),
    .RW0_rdata_3(data_arrays_1_RW0_rdata_3),
    .RW0_rdata_4(data_arrays_1_RW0_rdata_4),
    .RW0_rdata_5(data_arrays_1_RW0_rdata_5),
    .RW0_rdata_6(data_arrays_1_RW0_rdata_6),
    .RW0_rdata_7(data_arrays_1_RW0_rdata_7),
    .RW0_rdata_8(data_arrays_1_RW0_rdata_8),
    .RW0_rdata_9(data_arrays_1_RW0_rdata_9),
    .RW0_rdata_10(data_arrays_1_RW0_rdata_10),
    .RW0_rdata_11(data_arrays_1_RW0_rdata_11),
    .RW0_rdata_12(data_arrays_1_RW0_rdata_12),
    .RW0_rdata_13(data_arrays_1_RW0_rdata_13),
    .RW0_rdata_14(data_arrays_1_RW0_rdata_14),
    .RW0_rdata_15(data_arrays_1_RW0_rdata_15),
    .RW0_rdata_16(data_arrays_1_RW0_rdata_16),
    .RW0_rdata_17(data_arrays_1_RW0_rdata_17),
    .RW0_rdata_18(data_arrays_1_RW0_rdata_18),
    .RW0_rdata_19(data_arrays_1_RW0_rdata_19),
    .RW0_rdata_20(data_arrays_1_RW0_rdata_20),
    .RW0_rdata_21(data_arrays_1_RW0_rdata_21),
    .RW0_rdata_22(data_arrays_1_RW0_rdata_22),
    .RW0_rdata_23(data_arrays_1_RW0_rdata_23),
    .RW0_rdata_24(data_arrays_1_RW0_rdata_24),
    .RW0_rdata_25(data_arrays_1_RW0_rdata_25),
    .RW0_rdata_26(data_arrays_1_RW0_rdata_26),
    .RW0_rdata_27(data_arrays_1_RW0_rdata_27),
    .RW0_rdata_28(data_arrays_1_RW0_rdata_28),
    .RW0_rdata_29(data_arrays_1_RW0_rdata_29),
    .RW0_rdata_30(data_arrays_1_RW0_rdata_30),
    .RW0_rdata_31(data_arrays_1_RW0_rdata_31),
    .RW0_wmask_0(data_arrays_1_RW0_wmask_0),
    .RW0_wmask_1(data_arrays_1_RW0_wmask_1),
    .RW0_wmask_2(data_arrays_1_RW0_wmask_2),
    .RW0_wmask_3(data_arrays_1_RW0_wmask_3),
    .RW0_wmask_4(data_arrays_1_RW0_wmask_4),
    .RW0_wmask_5(data_arrays_1_RW0_wmask_5),
    .RW0_wmask_6(data_arrays_1_RW0_wmask_6),
    .RW0_wmask_7(data_arrays_1_RW0_wmask_7),
    .RW0_wmask_8(data_arrays_1_RW0_wmask_8),
    .RW0_wmask_9(data_arrays_1_RW0_wmask_9),
    .RW0_wmask_10(data_arrays_1_RW0_wmask_10),
    .RW0_wmask_11(data_arrays_1_RW0_wmask_11),
    .RW0_wmask_12(data_arrays_1_RW0_wmask_12),
    .RW0_wmask_13(data_arrays_1_RW0_wmask_13),
    .RW0_wmask_14(data_arrays_1_RW0_wmask_14),
    .RW0_wmask_15(data_arrays_1_RW0_wmask_15),
    .RW0_wmask_16(data_arrays_1_RW0_wmask_16),
    .RW0_wmask_17(data_arrays_1_RW0_wmask_17),
    .RW0_wmask_18(data_arrays_1_RW0_wmask_18),
    .RW0_wmask_19(data_arrays_1_RW0_wmask_19),
    .RW0_wmask_20(data_arrays_1_RW0_wmask_20),
    .RW0_wmask_21(data_arrays_1_RW0_wmask_21),
    .RW0_wmask_22(data_arrays_1_RW0_wmask_22),
    .RW0_wmask_23(data_arrays_1_RW0_wmask_23),
    .RW0_wmask_24(data_arrays_1_RW0_wmask_24),
    .RW0_wmask_25(data_arrays_1_RW0_wmask_25),
    .RW0_wmask_26(data_arrays_1_RW0_wmask_26),
    .RW0_wmask_27(data_arrays_1_RW0_wmask_27),
    .RW0_wmask_28(data_arrays_1_RW0_wmask_28),
    .RW0_wmask_29(data_arrays_1_RW0_wmask_29),
    .RW0_wmask_30(data_arrays_1_RW0_wmask_30),
    .RW0_wmask_31(data_arrays_1_RW0_wmask_31)
  );
  assign io_resp_0 = {rdata_1_0,rdata_0_0}; // @[Cat.scala 30:58]
  assign io_resp_1 = {rdata_1_1,rdata_0_1}; // @[Cat.scala 30:58]
  assign io_resp_2 = {rdata_1_2,rdata_0_2}; // @[Cat.scala 30:58]
  assign io_resp_3 = {rdata_1_3,rdata_0_3}; // @[Cat.scala 30:58]
  assign data_arrays_0_RW0_wdata_0 = wWords_0[7:0]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_1 = wWords_0[15:8]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_2 = wWords_0[23:16]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_3 = wWords_0[31:24]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_4 = wWords_0[39:32]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_5 = wWords_0[47:40]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_6 = wWords_0[55:48]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_7 = wWords_0[63:56]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_8 = wWords_0[7:0]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_9 = wWords_0[15:8]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_10 = wWords_0[23:16]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_11 = wWords_0[31:24]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_12 = wWords_0[39:32]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_13 = wWords_0[47:40]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_14 = wWords_0[55:48]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_15 = wWords_0[63:56]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_16 = wWords_0[7:0]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_17 = wWords_0[15:8]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_18 = wWords_0[23:16]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_19 = wWords_0[31:24]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_20 = wWords_0[39:32]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_21 = wWords_0[47:40]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_22 = wWords_0[55:48]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_23 = wWords_0[63:56]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_24 = wWords_0[7:0]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_25 = wWords_0[15:8]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_26 = wWords_0[23:16]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_27 = wWords_0[31:24]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_28 = wWords_0[39:32]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_29 = wWords_0[47:40]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_30 = wWords_0[55:48]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wdata_31 = wWords_0[63:56]; // @[package.scala 202:50]
  assign data_arrays_0_RW0_wmask_0 = eccMask_0 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_1 = eccMask_1 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_2 = eccMask_2 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_3 = eccMask_3 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_4 = eccMask_4 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_5 = eccMask_5 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_6 = eccMask_6 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_7 = eccMask_7 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_8 = eccMask_0 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_9 = eccMask_1 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_10 = eccMask_2 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_11 = eccMask_3 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_12 = eccMask_4 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_13 = eccMask_5 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_14 = eccMask_6 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_15 = eccMask_7 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_16 = eccMask_0 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_17 = eccMask_1 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_18 = eccMask_2 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_19 = eccMask_3 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_20 = eccMask_4 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_21 = eccMask_5 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_22 = eccMask_6 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_23 = eccMask_7 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_24 = eccMask_0 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_25 = eccMask_1 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_26 = eccMask_2 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_27 = eccMask_3 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_28 = eccMask_4 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_29 = eccMask_5 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_30 = eccMask_6 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmask_31 = eccMask_7 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wdata_0 = wWords_1[7:0]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_1 = wWords_1[15:8]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_2 = wWords_1[23:16]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_3 = wWords_1[31:24]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_4 = wWords_1[39:32]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_5 = wWords_1[47:40]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_6 = wWords_1[55:48]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_7 = wWords_1[63:56]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_8 = wWords_1[7:0]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_9 = wWords_1[15:8]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_10 = wWords_1[23:16]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_11 = wWords_1[31:24]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_12 = wWords_1[39:32]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_13 = wWords_1[47:40]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_14 = wWords_1[55:48]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_15 = wWords_1[63:56]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_16 = wWords_1[7:0]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_17 = wWords_1[15:8]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_18 = wWords_1[23:16]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_19 = wWords_1[31:24]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_20 = wWords_1[39:32]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_21 = wWords_1[47:40]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_22 = wWords_1[55:48]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_23 = wWords_1[63:56]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_24 = wWords_1[7:0]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_25 = wWords_1[15:8]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_26 = wWords_1[23:16]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_27 = wWords_1[31:24]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_28 = wWords_1[39:32]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_29 = wWords_1[47:40]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_30 = wWords_1[55:48]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wdata_31 = wWords_1[63:56]; // @[package.scala 202:50]
  assign data_arrays_1_RW0_wmask_0 = eccMask_0 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_1 = eccMask_1 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_2 = eccMask_2 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_3 = eccMask_3 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_4 = eccMask_4 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_5 = eccMask_5 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_6 = eccMask_6 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_7 = eccMask_7 & io_req_bits_way_en[0]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_8 = eccMask_0 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_9 = eccMask_1 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_10 = eccMask_2 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_11 = eccMask_3 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_12 = eccMask_4 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_13 = eccMask_5 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_14 = eccMask_6 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_15 = eccMask_7 & io_req_bits_way_en[1]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_16 = eccMask_0 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_17 = eccMask_1 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_18 = eccMask_2 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_19 = eccMask_3 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_20 = eccMask_4 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_21 = eccMask_5 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_22 = eccMask_6 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_23 = eccMask_7 & io_req_bits_way_en[2]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_24 = eccMask_0 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_25 = eccMask_1 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_26 = eccMask_2 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_27 = eccMask_3 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_28 = eccMask_4 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_29 = eccMask_5 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_30 = eccMask_6 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_1_RW0_wmask_31 = eccMask_7 & io_req_bits_way_en[3]; // @[DCache.scala 53:87]
  assign data_arrays_0_RW0_wmode = io_req_bits_write;
  assign data_arrays_0_RW0_clk = clock;
  assign data_arrays_0_RW0_en = _rdata_data_T_1 | _rdata_T;
  assign data_arrays_0_RW0_addr = io_req_bits_addr[12:4]; // @[DCache.scala 55:31]
  assign data_arrays_1_RW0_wmode = io_req_bits_write;
  assign data_arrays_1_RW0_clk = clock;
  assign data_arrays_1_RW0_en = _rdata_data_T_5 | _rdata_T_1;
  assign data_arrays_1_RW0_addr = io_req_bits_addr[12:4]; // @[DCache.scala 55:31]
endmodule
