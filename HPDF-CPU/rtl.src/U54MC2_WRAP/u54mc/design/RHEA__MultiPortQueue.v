//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__MultiPortQueue(
  input         rf_reset,
  input         clock,
  input         reset,
  output        io_enq_0_ready,
  input         io_enq_0_valid,
  input  [4:0]  io_enq_0_bits_reason,
  input  [38:0] io_enq_0_bits_iaddr,
  input  [13:0] io_enq_0_bits_icnt,
  input  [31:0] io_enq_0_bits_hist,
  input  [1:0]  io_enq_0_bits_hisv,
  output        io_enq_1_ready,
  input         io_enq_1_valid,
  input  [4:0]  io_enq_1_bits_reason,
  input  [38:0] io_enq_1_bits_iaddr,
  input  [13:0] io_enq_1_bits_icnt,
  input  [31:0] io_enq_1_bits_hist,
  input  [1:0]  io_enq_1_bits_hisv,
  input         io_deq_0_ready,
  output        io_deq_0_valid,
  output [4:0]  io_deq_0_bits_reason,
  output [38:0] io_deq_0_bits_iaddr,
  output [13:0] io_deq_0_bits_icnt,
  output [31:0] io_deq_0_bits_hist,
  output [1:0]  io_deq_0_bits_hisv
);
  wire  queue_rf_reset; // @[MultiPortQueue.scala 15:21]
  wire  queue_clock; // @[MultiPortQueue.scala 15:21]
  wire  queue_reset; // @[MultiPortQueue.scala 15:21]
  wire [3:0] queue_io_enq_ready; // @[MultiPortQueue.scala 15:21]
  wire [1:0] queue_io_enq_valid; // @[MultiPortQueue.scala 15:21]
  wire [4:0] queue_io_enq_bits_0_reason; // @[MultiPortQueue.scala 15:21]
  wire [38:0] queue_io_enq_bits_0_iaddr; // @[MultiPortQueue.scala 15:21]
  wire [13:0] queue_io_enq_bits_0_icnt; // @[MultiPortQueue.scala 15:21]
  wire [31:0] queue_io_enq_bits_0_hist; // @[MultiPortQueue.scala 15:21]
  wire [1:0] queue_io_enq_bits_0_hisv; // @[MultiPortQueue.scala 15:21]
  wire [4:0] queue_io_enq_bits_1_reason; // @[MultiPortQueue.scala 15:21]
  wire [38:0] queue_io_enq_bits_1_iaddr; // @[MultiPortQueue.scala 15:21]
  wire [13:0] queue_io_enq_bits_1_icnt; // @[MultiPortQueue.scala 15:21]
  wire [31:0] queue_io_enq_bits_1_hist; // @[MultiPortQueue.scala 15:21]
  wire [1:0] queue_io_enq_bits_1_hisv; // @[MultiPortQueue.scala 15:21]
  wire [1:0] queue_io_deq_ready; // @[MultiPortQueue.scala 15:21]
  wire [3:0] queue_io_deq_valid; // @[MultiPortQueue.scala 15:21]
  wire [4:0] queue_io_deq_bits_0_reason; // @[MultiPortQueue.scala 15:21]
  wire [38:0] queue_io_deq_bits_0_iaddr; // @[MultiPortQueue.scala 15:21]
  wire [13:0] queue_io_deq_bits_0_icnt; // @[MultiPortQueue.scala 15:21]
  wire [31:0] queue_io_deq_bits_0_hist; // @[MultiPortQueue.scala 15:21]
  wire [1:0] queue_io_deq_bits_0_hisv; // @[MultiPortQueue.scala 15:21]
  wire [4:0] queue_io_deq_bits_1_reason; // @[MultiPortQueue.scala 15:21]
  wire [38:0] queue_io_deq_bits_1_iaddr; // @[MultiPortQueue.scala 15:21]
  wire [13:0] queue_io_deq_bits_1_icnt; // @[MultiPortQueue.scala 15:21]
  wire [31:0] queue_io_deq_bits_1_hist; // @[MultiPortQueue.scala 15:21]
  wire [1:0] queue_io_deq_bits_1_hisv; // @[MultiPortQueue.scala 15:21]
  wire  queue_io_enq_0_lane; // @[MultiPortQueue.scala 15:21]
  wire  queue_io_deq_0_lane; // @[MultiPortQueue.scala 15:21]
  wire [1:0] enq_valid_1 = io_enq_0_valid + io_enq_1_valid; // @[MultiPortQueue.scala 24:66]
  wire [3:0] _GEN_0 = {{2'd0}, enq_valid_1}; // @[MultiPortQueue.scala 28:39]
  wire [3:0] _queue_io_enq_valid_T_1 = _GEN_0 <= queue_io_enq_ready ? {{2'd0}, enq_valid_1} : queue_io_enq_ready; // @[MultiPortQueue.scala 28:23]
  wire [3:0] _GEN_1 = {{3'd0}, io_enq_0_valid}; // @[MultiPortQueue.scala 29:75]
  wire [1:0] _GEN_2 = {{1'd0}, queue_io_enq_0_lane}; // @[MultiPortQueue.scala 33:45]
  wire [1:0] lowHoles = 2'h2 - _GEN_2; // @[MultiPortQueue.scala 33:45]
  wire  _highHoles_T = ~io_enq_0_valid; // @[MultiPortQueue.scala 34:63]
  wire [1:0] highHoles_0 = {{1'd0}, _highHoles_T};
  wire [1:0] _enq_dense_T_1 = lowHoles + highHoles_0; // @[MultiPortQueue.scala 37:85]
  wire [4:0] _enq_dense_T_9_reason = lowHoles[0] ? io_enq_0_bits_reason : 5'h0; // @[ScatterGather.scala 29:16]
  wire [38:0] _enq_dense_T_9_iaddr = lowHoles[0] ? io_enq_0_bits_iaddr : 39'h0; // @[ScatterGather.scala 29:16]
  wire [13:0] _enq_dense_T_9_icnt = lowHoles[0] ? io_enq_0_bits_icnt : 14'h0; // @[ScatterGather.scala 29:16]
  wire [31:0] _enq_dense_T_9_hist = lowHoles[0] ? io_enq_0_bits_hist : 32'h0; // @[ScatterGather.scala 29:16]
  wire [1:0] _enq_dense_T_9_hisv = lowHoles[0] ? io_enq_0_bits_hisv : 2'h0; // @[ScatterGather.scala 29:16]
  wire [4:0] enq_dense_2_reason = _enq_dense_T_1[0] ? io_enq_1_bits_reason : io_enq_0_bits_reason; // @[ScatterGather.scala 29:16]
  wire [38:0] enq_dense_2_iaddr = _enq_dense_T_1[0] ? io_enq_1_bits_iaddr : io_enq_0_bits_iaddr; // @[ScatterGather.scala 29:16]
  wire [13:0] enq_dense_2_icnt = _enq_dense_T_1[0] ? io_enq_1_bits_icnt : io_enq_0_bits_icnt; // @[ScatterGather.scala 29:16]
  wire [31:0] enq_dense_2_hist = _enq_dense_T_1[0] ? io_enq_1_bits_hist : io_enq_0_bits_hist; // @[ScatterGather.scala 29:16]
  wire [1:0] enq_dense_2_hisv = _enq_dense_T_1[0] ? io_enq_1_bits_hisv : io_enq_0_bits_hisv; // @[ScatterGather.scala 29:16]
  wire [4:0] enq_dense_0_reason = lowHoles[1] ? enq_dense_2_reason : 5'h0; // @[ScatterGather.scala 29:16]
  wire [38:0] enq_dense_0_iaddr = lowHoles[1] ? enq_dense_2_iaddr : 39'h0; // @[ScatterGather.scala 29:16]
  wire [13:0] enq_dense_0_icnt = lowHoles[1] ? enq_dense_2_icnt : 14'h0; // @[ScatterGather.scala 29:16]
  wire [31:0] enq_dense_0_hist = lowHoles[1] ? enq_dense_2_hist : 32'h0; // @[ScatterGather.scala 29:16]
  wire [1:0] enq_dense_0_hisv = lowHoles[1] ? enq_dense_2_hisv : 2'h0; // @[ScatterGather.scala 29:16]
  wire [4:0] enq_dense_1_reason = _enq_dense_T_1[1] ? io_enq_1_bits_reason : _enq_dense_T_9_reason; // @[ScatterGather.scala 29:16]
  wire [38:0] enq_dense_1_iaddr = _enq_dense_T_1[1] ? io_enq_1_bits_iaddr : _enq_dense_T_9_iaddr; // @[ScatterGather.scala 29:16]
  wire [13:0] enq_dense_1_icnt = _enq_dense_T_1[1] ? io_enq_1_bits_icnt : _enq_dense_T_9_icnt; // @[ScatterGather.scala 29:16]
  wire [31:0] enq_dense_1_hist = _enq_dense_T_1[1] ? io_enq_1_bits_hist : _enq_dense_T_9_hist; // @[ScatterGather.scala 29:16]
  wire [1:0] enq_dense_1_hisv = _enq_dense_T_1[1] ? io_enq_1_bits_hisv : _enq_dense_T_9_hisv; // @[ScatterGather.scala 29:16]
  wire [2:0] _enq_1hot_T_1 = 3'h3 << queue_io_enq_0_lane; // @[package.scala 234:77]
  wire [1:0] enq_1hot = ~_enq_1hot_T_1[1:0]; // @[package.scala 234:46]
  wire [3:0] _GEN_3 = {{3'd0}, io_deq_0_ready}; // @[MultiPortQueue.scala 55:39]
  wire [3:0] _queue_io_deq_ready_T_1 = _GEN_3 <= queue_io_deq_valid ? {{3'd0}, io_deq_0_ready} : queue_io_deq_valid; // @[MultiPortQueue.scala 55:23]
  wire [1:0] _GEN_4 = {{1'd0}, queue_io_deq_0_lane}; // @[MultiPortQueue.scala 61:45]
  wire [1:0] lowHoles_1 = 2'h2 - _GEN_4; // @[MultiPortQueue.scala 61:45]
  wire [4:0] _deq_sparse_T_3_reason = queue_io_deq_bits_0_reason; // @[ScatterGather.scala 60:16]
  wire [38:0] _deq_sparse_T_3_iaddr = queue_io_deq_bits_0_iaddr; // @[ScatterGather.scala 60:16]
  wire [13:0] _deq_sparse_T_3_icnt = queue_io_deq_bits_0_icnt; // @[ScatterGather.scala 60:16]
  wire [31:0] _deq_sparse_T_3_hist = queue_io_deq_bits_0_hist; // @[ScatterGather.scala 60:16]
  wire [1:0] _deq_sparse_T_3_hisv = queue_io_deq_bits_0_hisv; // @[ScatterGather.scala 60:16]
  RHEA__FloppedLanePositionedQueueModule queue ( // @[MultiPortQueue.scala 15:21]
    .rf_reset(queue_rf_reset),
    .clock(queue_clock),
    .reset(queue_reset),
    .io_enq_ready(queue_io_enq_ready),
    .io_enq_valid(queue_io_enq_valid),
    .io_enq_bits_0_reason(queue_io_enq_bits_0_reason),
    .io_enq_bits_0_iaddr(queue_io_enq_bits_0_iaddr),
    .io_enq_bits_0_icnt(queue_io_enq_bits_0_icnt),
    .io_enq_bits_0_hist(queue_io_enq_bits_0_hist),
    .io_enq_bits_0_hisv(queue_io_enq_bits_0_hisv),
    .io_enq_bits_1_reason(queue_io_enq_bits_1_reason),
    .io_enq_bits_1_iaddr(queue_io_enq_bits_1_iaddr),
    .io_enq_bits_1_icnt(queue_io_enq_bits_1_icnt),
    .io_enq_bits_1_hist(queue_io_enq_bits_1_hist),
    .io_enq_bits_1_hisv(queue_io_enq_bits_1_hisv),
    .io_deq_ready(queue_io_deq_ready),
    .io_deq_valid(queue_io_deq_valid),
    .io_deq_bits_0_reason(queue_io_deq_bits_0_reason),
    .io_deq_bits_0_iaddr(queue_io_deq_bits_0_iaddr),
    .io_deq_bits_0_icnt(queue_io_deq_bits_0_icnt),
    .io_deq_bits_0_hist(queue_io_deq_bits_0_hist),
    .io_deq_bits_0_hisv(queue_io_deq_bits_0_hisv),
    .io_deq_bits_1_reason(queue_io_deq_bits_1_reason),
    .io_deq_bits_1_iaddr(queue_io_deq_bits_1_iaddr),
    .io_deq_bits_1_icnt(queue_io_deq_bits_1_icnt),
    .io_deq_bits_1_hist(queue_io_deq_bits_1_hist),
    .io_deq_bits_1_hisv(queue_io_deq_bits_1_hisv),
    .io_enq_0_lane(queue_io_enq_0_lane),
    .io_deq_0_lane(queue_io_deq_0_lane)
  );
  assign queue_rf_reset = rf_reset;
  assign io_enq_0_ready = 4'h0 < queue_io_enq_ready; // @[MultiPortQueue.scala 29:75]
  assign io_enq_1_ready = _GEN_1 < queue_io_enq_ready; // @[MultiPortQueue.scala 29:75]
  assign io_deq_0_valid = 4'h0 < queue_io_deq_valid; // @[MultiPortQueue.scala 56:75]
  assign io_deq_0_bits_reason = lowHoles_1[0] ? queue_io_deq_bits_1_reason : _deq_sparse_T_3_reason; // @[ScatterGather.scala 60:16]
  assign io_deq_0_bits_iaddr = lowHoles_1[0] ? queue_io_deq_bits_1_iaddr : _deq_sparse_T_3_iaddr; // @[ScatterGather.scala 60:16]
  assign io_deq_0_bits_icnt = lowHoles_1[0] ? queue_io_deq_bits_1_icnt : _deq_sparse_T_3_icnt; // @[ScatterGather.scala 60:16]
  assign io_deq_0_bits_hist = lowHoles_1[0] ? queue_io_deq_bits_1_hist : _deq_sparse_T_3_hist; // @[ScatterGather.scala 60:16]
  assign io_deq_0_bits_hisv = lowHoles_1[0] ? queue_io_deq_bits_1_hisv : _deq_sparse_T_3_hisv; // @[ScatterGather.scala 60:16]
  assign queue_clock = clock;
  assign queue_reset = reset;
  assign queue_io_enq_valid = _queue_io_enq_valid_T_1[1:0]; // @[MultiPortQueue.scala 28:17]
  assign queue_io_enq_bits_0_reason = enq_1hot[0] ? enq_dense_2_reason : enq_dense_0_reason; // @[MultiPortQueue.scala 42:20]
  assign queue_io_enq_bits_0_iaddr = enq_1hot[0] ? enq_dense_2_iaddr : enq_dense_0_iaddr; // @[MultiPortQueue.scala 42:20]
  assign queue_io_enq_bits_0_icnt = enq_1hot[0] ? enq_dense_2_icnt : enq_dense_0_icnt; // @[MultiPortQueue.scala 42:20]
  assign queue_io_enq_bits_0_hist = enq_1hot[0] ? enq_dense_2_hist : enq_dense_0_hist; // @[MultiPortQueue.scala 42:20]
  assign queue_io_enq_bits_0_hisv = enq_1hot[0] ? enq_dense_2_hisv : enq_dense_0_hisv; // @[MultiPortQueue.scala 42:20]
  assign queue_io_enq_bits_1_reason = enq_1hot[1] ? io_enq_1_bits_reason : enq_dense_1_reason; // @[MultiPortQueue.scala 42:20]
  assign queue_io_enq_bits_1_iaddr = enq_1hot[1] ? io_enq_1_bits_iaddr : enq_dense_1_iaddr; // @[MultiPortQueue.scala 42:20]
  assign queue_io_enq_bits_1_icnt = enq_1hot[1] ? io_enq_1_bits_icnt : enq_dense_1_icnt; // @[MultiPortQueue.scala 42:20]
  assign queue_io_enq_bits_1_hist = enq_1hot[1] ? io_enq_1_bits_hist : enq_dense_1_hist; // @[MultiPortQueue.scala 42:20]
  assign queue_io_enq_bits_1_hisv = enq_1hot[1] ? io_enq_1_bits_hisv : enq_dense_1_hisv; // @[MultiPortQueue.scala 42:20]
  assign queue_io_deq_ready = _queue_io_deq_ready_T_1[1:0]; // @[MultiPortQueue.scala 55:17]
endmodule
