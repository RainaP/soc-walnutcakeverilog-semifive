module cpu_sram_wrapper #(
    parameter AW = 32,
    parameter DW = 64,
    parameter IW = 11,
    parameter UW = 0,
    localparam SW = DW/8)
(
    input              CLK__SRAM,
    input              RSTN__SRAM,

    input   [IW-1:0]      AXI4__SRAM__AWID,
    input   [AW-1:0]     AXI4__SRAM__AWADDR,
    input   [7:0]      AXI4__SRAM__AWLEN,
    input   [2:0]      AXI4__SRAM__AWSIZE,
    input   [1:0]      AXI4__SRAM__AWBURST,
    input              AXI4__SRAM__AWLOCK,
    input   [3:0]      AXI4__SRAM__AWCACHE,
    input   [2:0]      AXI4__SRAM__AWPROT,
    input   [UW-1:0]      AXI4__SRAM__AWUSER,
    input              AXI4__SRAM__AWVALID,
    output             AXI4__SRAM__AWREADY,
    input   [DW-1:0]     AXI4__SRAM__WDATA,
    input   [SW-1:0]      AXI4__SRAM__WSTRB,
    input              AXI4__SRAM__WLAST,
    input              AXI4__SRAM__WVALID,
    output             AXI4__SRAM__WREADY,
    output  [IW-1:0]      AXI4__SRAM__BID,
    output             AXI4__SRAM__BVALID,
    output  [1:0]      AXI4__SRAM__BRESP,
    input              AXI4__SRAM__BREADY,
    input   [IW-1:0]      AXI4__SRAM__ARID,
    input   [AW-1:0]     AXI4__SRAM__ARADDR,
    input   [7:0]      AXI4__SRAM__ARLEN,
    input   [2:0]      AXI4__SRAM__ARSIZE,
    input   [1:0]      AXI4__SRAM__ARBURST,
    input              AXI4__SRAM__ARLOCK,
    input   [3:0]      AXI4__SRAM__ARCACHE,
    input   [2:0]      AXI4__SRAM__ARPROT,
    input   [UW-1:0]      AXI4__SRAM__ARUSER,
    input              AXI4__SRAM__ARVALID,
    output             AXI4__SRAM__ARREADY,
    output  [IW-1:0]      AXI4__SRAM__RID,
    output  [DW-1:0]     AXI4__SRAM__RDATA,
    output  [1:0]      AXI4__SRAM__RRESP,
    output             AXI4__SRAM__RLAST,
    output             AXI4__SRAM__RVALID,
    input              AXI4__SRAM__RREADY

);

wire         gclken = 1'b1; // TODO
wire [AW-1:0]  sram_maddr;
wire [DW-1:0]  sram_mdata;
wire [SW-1:0]   sram_mwstrb;
wire         sram_mread;
wire         sram_mwrite;
wire [DW-1:0]  sram_sdata;
wire         sram_saccept = 1'b1;
reg          sram_svalid;
reg  [1:0]   sram_sresp;

// temporary
axi_gs_64_11_DW_axi_gs uPERI_SRAM_0 (
    .aclk          (CLK__SRAM), // input
    .aresetn       (RSTN__SRAM), // input
    .bid           (AXI4__SRAM__BID), // output [IW-1:0]
    .bresp         (AXI4__SRAM__BRESP), // output [1:0]
    .rid           (AXI4__SRAM__RID), // output [IW-1:0]
    .rdata         (AXI4__SRAM__RDATA), // output [DW-1:0]
    .rresp         (AXI4__SRAM__RRESP), // output [1:0]
    .awid          (AXI4__SRAM__AWID), // input [IW-1:0]
    .awaddr        (AXI4__SRAM__AWADDR), // input [AW-1:0]
    .awlen         (AXI4__SRAM__AWLEN), // input [3:0]
    .awsize        (AXI4__SRAM__AWSIZE), // input [2:0]
    .awburst       (AXI4__SRAM__AWBURST), // input [1:0]
    .awlock        (AXI4__SRAM__AWLOCK), // input [1:0]
    .awcache       (AXI4__SRAM__AWCACHE), // input [3:0]
    .awprot        (AXI4__SRAM__AWPROT), // input [2:0]
    .wdata         (AXI4__SRAM__WDATA), // input [DW-1:0]
    .wstrb         (AXI4__SRAM__WSTRB), // input [SW-1:0]
    .arid          (AXI4__SRAM__ARID), // input [IW-1:0]
    .araddr        (AXI4__SRAM__ARADDR), // input [AW-1:0]
    .arlen         (AXI4__SRAM__ARLEN), // input [3:0]
    .arsize        (AXI4__SRAM__ARSIZE), // input [2:0]
    .arburst       (AXI4__SRAM__ARBURST), // input [1:0]
    .arlock        (AXI4__SRAM__ARLOCK), // input [1:0]
    .arcache       (AXI4__SRAM__ARCACHE), // input [3:0]
    .arprot        (AXI4__SRAM__ARPROT), // input [2:0]
    .awvalid       (AXI4__SRAM__AWVALID), // input
    .wlast         (AXI4__SRAM__WLAST), // input
    .wvalid        (AXI4__SRAM__WVALID), // input
    .bready        (AXI4__SRAM__BREADY), // input
    .arvalid       (AXI4__SRAM__ARVALID), // input
    .rready        (AXI4__SRAM__RREADY), // input
    .awready       (AXI4__SRAM__AWREADY), // output
    .wready        (AXI4__SRAM__WREADY), // output
    .bvalid        (AXI4__SRAM__BVALID), // output
    .arready       (AXI4__SRAM__ARREADY), // output
    .rlast         (AXI4__SRAM__RLAST), // output
    .rvalid        (AXI4__SRAM__RVALID), // output
    .gclken        (gclken), // input
    .sdata         (sram_sdata), // input [DW-1:0]
    .sresp         (sram_sresp), // input [1:0]
    .saccept       (sram_saccept), // input
    .svalid        (1'b1), // input
    .maddr         (sram_maddr), // output [AW-1:0]
    .msize         (), // output [2:0]
    .mburst        (), // output [1:0]
    .mlen          (), // output [3:0]
    .mdata         (sram_mdata), // output [DW-1:0]
    .mwstrb        (sram_mwstrb), // output [SW-1:0]
    .mread         (sram_mread), // output
    .mwrite        (sram_mwrite), // output
    .mlast         (), // output
    .mready        ()  // output
);

wire [15:0] sram_addr;
wire sram_ce;
assign sram_ce = sram_mread | sram_mwrite;
assign sram_addr = sram_maddr[18:0]>>3;

always @(posedge CLK__SRAM or negedge RSTN__SRAM) begin
    if (~RSTN__SRAM)
        sram_sresp <= 2'b00;
    else if (sram_mwrite)
        sram_sresp <= 2'b01;
    else if (sram_mread)
        sram_sresp <= 2'b00;
end

    SEMIFIVE_SRAM_SP_W #(.ABIT(16), .WORD(65536), .DBIT(64), .GBIT(8)) MEM_0 (
        .clock          (CLK__SRAM),
        .enable         (sram_ce),
        .address        (sram_addr),
        .readenable     (sram_mread),
        .readdata       (sram_sdata),
        .writeenable    (sram_mwrite),
        .writedata      (sram_mdata),
        .writestrobe    (sram_mwstrb));

endmodule
