//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade 
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module usb3_subsystem (
	 input         SCU_USB3_0__CLK__OSC
	,input         SCU_USB3_0__CLK__SUBSYSTEM
	,input         SCU_USB3_0__RSTN__SUBSYSTEM
	,output        MBUS_USB3_0__CLK__TX
	,output        MBUS_USB3_0__RSTN__TX
	,output        PBUS_USB3_0__CLK__RX
	,output        PBUS_USB3_0__RSTN__RX
	,output        USB3_0__IRQ
	,input  [31:0] PBUS_USB3_0__AXI4__RX__AWADDR
	,input         PBUS_USB3_0__AXI4__RX__AWID
	,input  [ 7:0] PBUS_USB3_0__AXI4__RX__AWLEN
	,input  [ 2:0] PBUS_USB3_0__AXI4__RX__AWSIZE
	,input  [ 1:0] PBUS_USB3_0__AXI4__RX__AWBURST
	,input         PBUS_USB3_0__AXI4__RX__AWLOCK
	,input  [ 3:0] PBUS_USB3_0__AXI4__RX__AWCACHE
	,input  [ 2:0] PBUS_USB3_0__AXI4__RX__AWPROT
	,input  [31:0] PBUS_USB3_0__AXI4__RX__WDATA
	,input  [ 3:0] PBUS_USB3_0__AXI4__RX__WSTRB
	,output        PBUS_USB3_0__AXI4__RX__BID
	,output [ 1:0] PBUS_USB3_0__AXI4__RX__BRESP
	,input         PBUS_USB3_0__AXI4__RX__ARID
	,input  [31:0] PBUS_USB3_0__AXI4__RX__ARADDR
	,input  [ 7:0] PBUS_USB3_0__AXI4__RX__ARLEN
	,input  [ 2:0] PBUS_USB3_0__AXI4__RX__ARSIZE
	,input  [ 1:0] PBUS_USB3_0__AXI4__RX__ARBURST
	,input         PBUS_USB3_0__AXI4__RX__ARLOCK
	,input  [ 3:0] PBUS_USB3_0__AXI4__RX__ARCACHE
	,input  [ 2:0] PBUS_USB3_0__AXI4__RX__ARPROT
	,output        PBUS_USB3_0__AXI4__RX__RID
	,output [31:0] PBUS_USB3_0__AXI4__RX__RDATA
	,output [ 1:0] PBUS_USB3_0__AXI4__RX__RRESP
	,output        PBUS_USB3_0__AXI4__RX__AWREADY
	,output        PBUS_USB3_0__AXI4__RX__WREADY
	,output        PBUS_USB3_0__AXI4__RX__BVALID
	,output        PBUS_USB3_0__AXI4__RX__ARREADY
	,output        PBUS_USB3_0__AXI4__RX__RVALID
	,output        PBUS_USB3_0__AXI4__RX__RLAST
	,input         PBUS_USB3_0__AXI4__RX__AWVALID
	,input         PBUS_USB3_0__AXI4__RX__WVALID
	,input         PBUS_USB3_0__AXI4__RX__WLAST
	,input         PBUS_USB3_0__AXI4__RX__BREADY
	,input         PBUS_USB3_0__AXI4__RX__ARVALID
	,input         PBUS_USB3_0__AXI4__RX__RREADY
	,output [ 5:0] MBUS_USB3_0__AXI4__TX__AWID
	,output [35:0] MBUS_USB3_0__AXI4__TX__AWADDR
	,output [ 7:0] MBUS_USB3_0__AXI4__TX__AWLEN
	,output [ 2:0] MBUS_USB3_0__AXI4__TX__AWSIZE
	,output [ 1:0] MBUS_USB3_0__AXI4__TX__AWBURST
	,output        MBUS_USB3_0__AXI4__TX__AWLOCK
	,output [ 3:0] MBUS_USB3_0__AXI4__TX__AWCACHE
	,output [ 2:0] MBUS_USB3_0__AXI4__TX__AWPROT
	,output [ 9:0] MBUS_USB3_0__AXI4__TX__AWUSER
	,output        MBUS_USB3_0__AXI4__TX__AWVALID
	,input         MBUS_USB3_0__AXI4__TX__AWREADY
	,output [63:0] MBUS_USB3_0__AXI4__TX__WDATA
	,output [ 7:0] MBUS_USB3_0__AXI4__TX__WSTRB
	,output        MBUS_USB3_0__AXI4__TX__WLAST
	,output        MBUS_USB3_0__AXI4__TX__WVALID
	,input         MBUS_USB3_0__AXI4__TX__WREADY
	,input  [ 5:0] MBUS_USB3_0__AXI4__TX__BID
	,input         MBUS_USB3_0__AXI4__TX__BVALID
	,input  [ 1:0] MBUS_USB3_0__AXI4__TX__BRESP
	,output        MBUS_USB3_0__AXI4__TX__BREADY
	,output [ 5:0] MBUS_USB3_0__AXI4__TX__ARID
	,output [35:0] MBUS_USB3_0__AXI4__TX__ARADDR
	,output [ 7:0] MBUS_USB3_0__AXI4__TX__ARLEN
	,output [ 2:0] MBUS_USB3_0__AXI4__TX__ARSIZE
	,output [ 1:0] MBUS_USB3_0__AXI4__TX__ARBURST
	,output        MBUS_USB3_0__AXI4__TX__ARLOCK
	,output [ 3:0] MBUS_USB3_0__AXI4__TX__ARCACHE
	,output [ 2:0] MBUS_USB3_0__AXI4__TX__ARPROT
	,output [ 9:0] MBUS_USB3_0__AXI4__TX__ARUSER
	,output        MBUS_USB3_0__AXI4__TX__ARVALID
	,input         MBUS_USB3_0__AXI4__TX__ARREADY
	,input  [ 5:0] MBUS_USB3_0__AXI4__TX__RID
	,input  [63:0] MBUS_USB3_0__AXI4__TX__RDATA
	,input  [ 1:0] MBUS_USB3_0__AXI4__TX__RRESP
	,input         MBUS_USB3_0__AXI4__TX__RLAST
	,input         MBUS_USB3_0__AXI4__TX__RVALID
	,output        MBUS_USB3_0__AXI4__TX__RREADY
	,inout         USB3_0__IO__VBUS
	,inout         USB3_0__IO__DM
	,inout         USB3_0__IO__DP
	,input         USB3_0__IO__ID
	,inout         USB3_0__IO__RESREF
	,output        USB3_0__IO__TXM
	,output        USB3_0__IO__TXP
	,input         USB3_0__IO__RXM
	,input         USB3_0__IO__RXP
	,input         USB3_0__IO__REFPADCLKM
	,input         USB3_0__IO__REFPADCLKP
	,input         USB3_0__IO__TEST__USB3PHY_MODE
	,input  [99:0] USB3_0__IO__TEST__USB3PHY_IN
	,output [99:0] USB3_0__IO__TEST__USB3PHY_OUT
	,output        USB3_0__FPGAIO__PIPE3_TX_CLK
	,output [15:0] USB3_0__FPGAIO__PIPE3_TX_DATA
	,output [ 1:0] USB3_0__FPGAIO__PIPE3_TX_DATAK
	,input         USB3_0__FPGAIO__PIPE3_PCLK
	,input  [15:0] USB3_0__FPGAIO__PIPE3_RX_DATA
	,input  [ 1:0] USB3_0__FPGAIO__PIPE3_RX_DATAK
	,input         USB3_0__FPGAIO__PIPE3_RX_VALID
	,output        USB3_0__FPGAIO__PIPE3_PHY_RESETN
	,output        USB3_0__FPGAIO__PIPE3_TX_DETRX_LPBK
	,output        USB3_0__FPGAIO__PIPE3_TX_ELECIDLE
	,output        USB3_0__FPGAIO__PIPE3_RX_ELECIDLE
	,input  [ 2:0] USB3_0__FPGAIO__PIPE3_RX_STATUS
	,output [ 1:0] USB3_0__FPGAIO__PIPE3_POWER_DOWN
	,input         USB3_0__FPGAIO__PIPE3_PHY_STATUS
	,input         USB3_0__FPGAIO__PIPE3_PWRPRESENT
	,output        USB3_0__FPGAIO__PIPE3_TX_ONESZEROS
	,output [ 1:0] USB3_0__FPGAIO__PIPE3_TX_DEEMPH
	,output [ 2:0] USB3_0__FPGAIO__PIPE3_TX_MARGIN
	,output        USB3_0__FPGAIO__PIPE3_TX_SWING
	,output        USB3_0__FPGAIO__PIPE3_RX_POLARITY
	,output        USB3_0__FPGAIO__PIPE3_RX_TERMINATION
	,output        USB3_0__FPGAIO__PIPE3_RATE
	,output        USB3_0__FPGAIO__PIPE3_ELAS_BUF_MODE
	,input         USB3_0__FPGAIO__ULPI_CLK
	,output [ 7:0] USB3_0__FPGAIO__ULPI_DATAOUT
	,input  [ 7:0] USB3_0__FPGAIO__ULPI_DATAIN
	,input         USB3_0__FPGAIO__ULPI_DIR
	,output        USB3_0__FPGAIO__ULPI_STP
	,input         USB3_0__FPGAIO__ULPI_NXT
	,input         DFT__SCAN_CLK
	,input         DFT__SCAN_RSTN
	,input         DFT__SCAN_MODE
);
	assign PBUS_USB3_0__AXI4__RX__AWREADY = 1'h1;
	assign PBUS_USB3_0__AXI4__RX__WREADY = 1'h1;
	assign PBUS_USB3_0__AXI4__RX__ARREADY = 1'h1;
	assign MBUS_USB3_0__AXI4__TX__BREADY = 1'h1;
	assign MBUS_USB3_0__AXI4__TX__RREADY = 1'h1;
	assign MBUS_USB3_0__CLK__TX = 1'h0;
	assign MBUS_USB3_0__RSTN__TX = 1'h0;
	assign PBUS_USB3_0__CLK__RX = 1'h0;
	assign PBUS_USB3_0__RSTN__RX = 1'h0;
	assign USB3_0__IRQ = 1'h0;
	assign PBUS_USB3_0__AXI4__RX__BID = 1'h0;
	assign PBUS_USB3_0__AXI4__RX__BRESP = 2'h0;
	assign PBUS_USB3_0__AXI4__RX__RID = 1'h0;
	assign PBUS_USB3_0__AXI4__RX__RDATA = 32'h0;
	assign PBUS_USB3_0__AXI4__RX__RRESP = 2'h0;
	assign PBUS_USB3_0__AXI4__RX__BVALID = 1'h0;
	assign PBUS_USB3_0__AXI4__RX__RVALID = 1'h0;
	assign PBUS_USB3_0__AXI4__RX__RLAST = 1'h0;
	assign MBUS_USB3_0__AXI4__TX__AWID = 6'h0;
	assign MBUS_USB3_0__AXI4__TX__AWADDR = 36'h0;
	assign MBUS_USB3_0__AXI4__TX__AWLEN = 8'h0;
	assign MBUS_USB3_0__AXI4__TX__AWSIZE = 3'h0;
	assign MBUS_USB3_0__AXI4__TX__AWBURST = 2'h0;
	assign MBUS_USB3_0__AXI4__TX__AWLOCK = 1'h0;
	assign MBUS_USB3_0__AXI4__TX__AWCACHE = 4'h0;
	assign MBUS_USB3_0__AXI4__TX__AWPROT = 3'h0;
	assign MBUS_USB3_0__AXI4__TX__AWUSER = 10'h0;
	assign MBUS_USB3_0__AXI4__TX__AWVALID = 1'h0;
	assign MBUS_USB3_0__AXI4__TX__WDATA = 64'h0;
	assign MBUS_USB3_0__AXI4__TX__WSTRB = 8'h0;
	assign MBUS_USB3_0__AXI4__TX__WLAST = 1'h0;
	assign MBUS_USB3_0__AXI4__TX__WVALID = 1'h0;
	assign MBUS_USB3_0__AXI4__TX__ARID = 6'h0;
	assign MBUS_USB3_0__AXI4__TX__ARADDR = 36'h0;
	assign MBUS_USB3_0__AXI4__TX__ARLEN = 8'h0;
	assign MBUS_USB3_0__AXI4__TX__ARSIZE = 3'h0;
	assign MBUS_USB3_0__AXI4__TX__ARBURST = 2'h0;
	assign MBUS_USB3_0__AXI4__TX__ARLOCK = 1'h0;
	assign MBUS_USB3_0__AXI4__TX__ARCACHE = 4'h0;
	assign MBUS_USB3_0__AXI4__TX__ARPROT = 3'h0;
	assign MBUS_USB3_0__AXI4__TX__ARUSER = 10'h0;
	assign MBUS_USB3_0__AXI4__TX__ARVALID = 1'h0;
	assign USB3_0__IO__TXM = 1'h0;
	assign USB3_0__IO__TXP = 1'h0;
	assign USB3_0__IO__TEST__USB3PHY_OUT = 100'h0;
	assign USB3_0__FPGAIO__PIPE3_TX_CLK = 1'h0;
	assign USB3_0__FPGAIO__PIPE3_TX_DATA = 16'h0;
	assign USB3_0__FPGAIO__PIPE3_TX_DATAK = 2'h0;
	assign USB3_0__FPGAIO__PIPE3_PHY_RESETN = 1'h0;
	assign USB3_0__FPGAIO__PIPE3_TX_DETRX_LPBK = 1'h0;
	assign USB3_0__FPGAIO__PIPE3_TX_ELECIDLE = 1'h0;
	assign USB3_0__FPGAIO__PIPE3_RX_ELECIDLE = 1'h0;
	assign USB3_0__FPGAIO__PIPE3_POWER_DOWN = 2'h0;
	assign USB3_0__FPGAIO__PIPE3_TX_ONESZEROS = 1'h0;
	assign USB3_0__FPGAIO__PIPE3_TX_DEEMPH = 2'h0;
	assign USB3_0__FPGAIO__PIPE3_TX_MARGIN = 3'h0;
	assign USB3_0__FPGAIO__PIPE3_TX_SWING = 1'h0;
	assign USB3_0__FPGAIO__PIPE3_RX_POLARITY = 1'h0;
	assign USB3_0__FPGAIO__PIPE3_RX_TERMINATION = 1'h0;
	assign USB3_0__FPGAIO__PIPE3_RATE = 1'h0;
	assign USB3_0__FPGAIO__PIPE3_ELAS_BUF_MODE = 1'h0;
	assign USB3_0__FPGAIO__ULPI_DATAOUT = 8'h0;
	assign USB3_0__FPGAIO__ULPI_STP = 1'h0;

endmodule 
