//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLDebugModuleInner_assert(
  input         clock,
  input         reset,
  input         auto_tl_in_a_valid,
  input  [2:0]  auto_tl_in_a_bits_opcode,
  input  [2:0]  auto_tl_in_a_bits_param,
  input  [1:0]  auto_tl_in_a_bits_size,
  input  [10:0] auto_tl_in_a_bits_source,
  input  [11:0] auto_tl_in_a_bits_address,
  input  [7:0]  auto_tl_in_a_bits_mask,
  input  [63:0] auto_tl_in_a_bits_data,
  input         auto_tl_in_a_bits_corrupt,
  input         auto_tl_in_d_ready,
  input         auto_dmi_in_a_valid,
  input  [2:0]  auto_dmi_in_a_bits_opcode,
  input  [2:0]  auto_dmi_in_a_bits_param,
  input  [1:0]  auto_dmi_in_a_bits_size,
  input         auto_dmi_in_a_bits_source,
  input  [8:0]  auto_dmi_in_a_bits_address,
  input  [3:0]  auto_dmi_in_a_bits_mask,
  input         auto_dmi_in_a_bits_corrupt,
  input         auto_dmi_in_d_ready,
  input         _T_7,
  input         _T_1389,
  input         _T_7335,
  input  [1:0]  ctrlStateReg,
  input         _T_18142,
  input         _T_18143,
  input         hartExceptionWrEn,
  input         _T_18109,
  input         hartGoingWrEn,
  input         goAbstract
);
  wire  TLMonitor_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [8:0] TLMonitor_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_1_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [10:0] TLMonitor_1_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [11:0] TLMonitor_1_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_1_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_1_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [10:0] TLMonitor_1_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire  _T_1254; // @[SBA.scala 247:10]
  wire [9:0] hartGoingId; // @[RegisterRouter.scala 80:24]
  wire  _T_7255; // @[Debug.scala 1389:28]
  wire  _T_7257; // @[Debug.scala 1389:15]
  wire  _T_7258; // @[Debug.scala 1389:15]
  wire  _T_18152; // @[Debug.scala 1657:30]
  wire  _T_18161; // @[Debug.scala 1671:30]
  wire  _T_18162; // @[Debug.scala 1671:27]
  wire  _T_18164; // @[Debug.scala 1671:49]
  wire  _T_18166; // @[Debug.scala 1671:12]
  wire  _T_18167; // @[Debug.scala 1671:12]
  wire  _GEN_1986; // @[Debug.scala 1389:15]
  wire  _GEN_1987; // @[Debug.scala 1389:15]
  wire  _GEN_1988; // @[Debug.scala 1389:15]
  wire  _GEN_1989; // @[Debug.scala 1389:15]
  wire  _GEN_1995; // @[Debug.scala 1653:15]
  wire  _GEN_1996; // @[Debug.scala 1653:15]
  wire  _GEN_1997; // @[Debug.scala 1653:15]
  wire  _GEN_1998; // @[Debug.scala 1653:15]
  wire  _GEN_2007; // @[Debug.scala 1658:13]
  wire  _GEN_2008; // @[Debug.scala 1658:13]
  wire  _GEN_2009; // @[Debug.scala 1658:13]
  SiFive_TLMonitor_66_assert TLMonitor ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_clock),
    .reset(TLMonitor_reset),
    .io_in_a_ready(TLMonitor_io_in_a_ready),
    .io_in_a_valid(TLMonitor_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_io_in_a_bits_corrupt),
    .io_in_d_ready(TLMonitor_io_in_d_ready),
    .io_in_d_valid(TLMonitor_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_io_in_d_bits_opcode),
    .io_in_d_bits_size(TLMonitor_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_io_in_d_bits_source)
  );
  SiFive_TLMonitor_67_assert TLMonitor_1 ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_1_clock),
    .reset(TLMonitor_1_reset),
    .io_in_a_ready(TLMonitor_1_io_in_a_ready),
    .io_in_a_valid(TLMonitor_1_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_1_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_1_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_1_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_1_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_1_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_1_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_1_io_in_a_bits_corrupt),
    .io_in_d_ready(TLMonitor_1_io_in_d_ready),
    .io_in_d_valid(TLMonitor_1_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_1_io_in_d_bits_opcode),
    .io_in_d_bits_size(TLMonitor_1_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_1_io_in_d_bits_source)
  );
  assign _T_1254 = ~reset; // @[SBA.scala 247:10]
  assign hartGoingId = auto_tl_in_a_bits_data[41:32]; // @[RegisterRouter.scala 80:24]
  assign _T_7255 = hartGoingId == 10'h0; // @[Debug.scala 1389:28]
  assign _T_7257 = _T_7255 | reset; // @[Debug.scala 1389:15]
  assign _T_7258 = ~_T_7257; // @[Debug.scala 1389:15]
  assign _T_18152 = ctrlStateReg == 2'h3; // @[Debug.scala 1657:30]
  assign _T_18161 = ~hartExceptionWrEn; // @[Debug.scala 1671:30]
  assign _T_18162 = _T_7 | _T_18161; // @[Debug.scala 1671:27]
  assign _T_18164 = _T_18162 | _T_18143; // @[Debug.scala 1671:49]
  assign _T_18166 = _T_18164 | reset; // @[Debug.scala 1671:12]
  assign _T_18167 = ~_T_18166; // @[Debug.scala 1671:12]
  assign TLMonitor_clock = clock;
  assign TLMonitor_reset = reset;
  assign TLMonitor_io_in_a_ready = auto_dmi_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_valid = auto_dmi_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_opcode = auto_dmi_in_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_param = auto_dmi_in_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_size = auto_dmi_in_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_source = auto_dmi_in_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_address = auto_dmi_in_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_mask = auto_dmi_in_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_corrupt = auto_dmi_in_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_ready = auto_dmi_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_valid = auto_dmi_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_opcode = {{2'd0}, _T_1389}; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_size = auto_dmi_in_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_source = auto_dmi_in_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_1_clock = clock;
  assign TLMonitor_1_reset = reset;
  assign TLMonitor_1_io_in_a_ready = auto_tl_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_valid = auto_tl_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_opcode = auto_tl_in_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_param = auto_tl_in_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_size = auto_tl_in_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_source = auto_tl_in_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_address = auto_tl_in_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_mask = auto_tl_in_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_corrupt = auto_tl_in_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_ready = auto_tl_in_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_valid = auto_tl_in_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_opcode = {{2'd0}, _T_7335}; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_size = auto_tl_in_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_source = auto_tl_in_a_bits_source; // @[Nodes.scala 26:19]
  assign _GEN_1986 = ~_T_7; // @[Debug.scala 1389:15]
  assign _GEN_1987 = ~goAbstract; // @[Debug.scala 1389:15]
  assign _GEN_1988 = _GEN_1986 & _GEN_1987; // @[Debug.scala 1389:15]
  assign _GEN_1989 = _GEN_1988 & hartGoingWrEn; // @[Debug.scala 1389:15]
  assign _GEN_1995 = ~_T_18142; // @[Debug.scala 1653:15]
  assign _GEN_1996 = _T_18109 & _GEN_1995; // @[Debug.scala 1653:15]
  assign _GEN_1997 = _GEN_1996 & _T_18143; // @[Debug.scala 1653:15]
  assign _GEN_1998 = _GEN_1997 & hartExceptionWrEn; // @[Debug.scala 1653:15]
  assign _GEN_2007 = ~_T_18143; // @[Debug.scala 1658:13]
  assign _GEN_2008 = _GEN_1996 & _GEN_2007; // @[Debug.scala 1658:13]
  assign _GEN_2009 = _GEN_2008 & _T_18152; // @[Debug.scala 1658:13]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_1989 & _T_7258) begin
          $fwrite(32'h80000002,"Assertion Failed: Unexpected 'GOING' hart.\n    at Debug.scala:1389 assert(hartGoingId === 0.U, \"Unexpected 'GOING' hart.\")//Chisel3 #540 %%x, expected %%x\", hartGoingId, 0.U)\n"); // @[Debug.scala 1389:15]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_1989 & _T_7258) begin
          $fatal; // @[Debug.scala 1389:15]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_1998 & _T_7258) begin
          $fwrite(32'h80000002,"Assertion Failed: Unexpected 'EXCEPTION' hart\n    at Debug.scala:1653 assert(hartExceptionId === 0.U, \"Unexpected 'EXCEPTION' hart\")//Chisel3 #540, %%x, expected %%x\", hartExceptionId, 0.U)\n"); // @[Debug.scala 1653:15]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_1998 & _T_7258) begin
          $fatal; // @[Debug.scala 1653:15]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_2009 & _T_1254) begin
          $fwrite(32'h80000002,"Assertion Failed: Should not be in custom state unless we need it.\n    at Debug.scala:1658 assert(needCustom.B, \"Should not be in custom state unless we need it.\")\n"); // @[Debug.scala 1658:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_2009 & _T_1254) begin
          $fatal; // @[Debug.scala 1658:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_18167) begin
          $fwrite(32'h80000002,"Assertion Failed: Unexpected EXCEPTION write: should only get it in Debug Module EXEC state\n    at Debug.scala:1671 assert ((!io.dmactive || !hartExceptionWrEn || ctrlStateReg === CtrlState(Exec)),\n"); // @[Debug.scala 1671:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_18167) begin
          $fatal; // @[Debug.scala 1671:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
