//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__IntSyncAsyncCrossingSink(
  input   rf_reset,
  input   clock,
  input   auto_in_sync_0,
  output  auto_out_0
);
  wire  chain_rf_reset; // @[ShiftReg.scala 45:23]
  wire  chain_clock; // @[ShiftReg.scala 45:23]
  wire  chain_io_d; // @[ShiftReg.scala 45:23]
  wire  chain_io_q; // @[ShiftReg.scala 45:23]
  RHEA__SynchronizerShiftReg_w1_d3 chain ( // @[ShiftReg.scala 45:23]
    .rf_reset(chain_rf_reset),
    .clock(chain_clock),
    .io_d(chain_io_d),
    .io_q(chain_io_q)
  );
  assign chain_rf_reset = rf_reset;
  assign auto_out_0 = chain_io_q; // @[ShiftReg.scala 48:24]
  assign chain_clock = clock;
  assign chain_io_d = auto_in_sync_0; // @[Nodes.scala 84:11 LazyModule.scala 388:16]
endmodule
