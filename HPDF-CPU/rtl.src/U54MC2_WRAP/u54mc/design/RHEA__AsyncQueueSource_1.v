//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__AsyncQueueSource_1(
  input        rf_reset,
  input        clock,
  input        reset,
  output       io_enq_ready,
  input        io_enq_valid,
  input        io_enq_bits_resumereq,
  input  [9:0] io_enq_bits_hartsel,
  input        io_enq_bits_ackhavereset,
  input        io_enq_bits_hasel,
  input        io_enq_bits_hamask_0,
  input        io_enq_bits_hamask_1,
  input        io_enq_bits_hrmask_0,
  input        io_enq_bits_hrmask_1,
  output       io_async_mem_0_resumereq,
  output [9:0] io_async_mem_0_hartsel,
  output       io_async_mem_0_ackhavereset,
  output       io_async_mem_0_hasel,
  output       io_async_mem_0_hamask_0,
  output       io_async_mem_0_hamask_1,
  output       io_async_mem_0_hrmask_0,
  output       io_async_mem_0_hrmask_1,
  input        io_async_ridx,
  output       io_async_widx
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
`endif // RANDOMIZE_REG_INIT
  wire  ridx_ridx_gray_clock; // @[ShiftReg.scala 45:23]
  wire  ridx_ridx_gray_reset; // @[ShiftReg.scala 45:23]
  wire  ridx_ridx_gray_io_d; // @[ShiftReg.scala 45:23]
  wire  ridx_ridx_gray_io_q; // @[ShiftReg.scala 45:23]
  reg  mem_0_resumereq; // @[AsyncQueue.scala 80:16]
  reg [9:0] mem_0_hartsel; // @[AsyncQueue.scala 80:16]
  reg  mem_0_ackhavereset; // @[AsyncQueue.scala 80:16]
  reg  mem_0_hasel; // @[AsyncQueue.scala 80:16]
  reg  mem_0_hamask_0; // @[AsyncQueue.scala 80:16]
  reg  mem_0_hamask_1; // @[AsyncQueue.scala 80:16]
  reg  mem_0_hrmask_0; // @[AsyncQueue.scala 80:16]
  reg  mem_0_hrmask_1; // @[AsyncQueue.scala 80:16]
  wire  _widx_T_1 = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  reg  widx_widx_bin; // @[AsyncQueue.scala 52:25]
  wire  widx_incremented = widx_widx_bin + _widx_T_1; // @[AsyncQueue.scala 53:43]
  wire  ridx = ridx_ridx_gray_io_q; // @[ShiftReg.scala 48:24 ShiftReg.scala 48:24]
  reg  ready_reg; // @[AsyncQueue.scala 88:56]
  reg  widx_gray; // @[AsyncQueue.scala 91:55]
  RHEA__AsyncResetSynchronizerShiftReg_w1_d3_i0 ridx_ridx_gray ( // @[ShiftReg.scala 45:23]
    .clock(ridx_ridx_gray_clock),
    .reset(ridx_ridx_gray_reset),
    .io_d(ridx_ridx_gray_io_d),
    .io_q(ridx_ridx_gray_io_q)
  );
  assign io_enq_ready = ready_reg; // @[AsyncQueue.scala 89:29]
  assign io_async_mem_0_resumereq = mem_0_resumereq; // @[AsyncQueue.scala 96:31]
  assign io_async_mem_0_hartsel = mem_0_hartsel; // @[AsyncQueue.scala 96:31]
  assign io_async_mem_0_ackhavereset = mem_0_ackhavereset; // @[AsyncQueue.scala 96:31]
  assign io_async_mem_0_hasel = mem_0_hasel; // @[AsyncQueue.scala 96:31]
  assign io_async_mem_0_hamask_0 = mem_0_hamask_0; // @[AsyncQueue.scala 96:31]
  assign io_async_mem_0_hamask_1 = mem_0_hamask_1; // @[AsyncQueue.scala 96:31]
  assign io_async_mem_0_hrmask_0 = mem_0_hrmask_0; // @[AsyncQueue.scala 96:31]
  assign io_async_mem_0_hrmask_1 = mem_0_hrmask_1; // @[AsyncQueue.scala 96:31]
  assign io_async_widx = widx_gray; // @[AsyncQueue.scala 92:17]
  assign ridx_ridx_gray_clock = clock;
  assign ridx_ridx_gray_reset = reset;
  assign ridx_ridx_gray_io_d = io_async_ridx; // @[ShiftReg.scala 47:16]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      mem_0_resumereq <= 1'h0;
    end else if (_widx_T_1) begin
      mem_0_resumereq <= io_enq_bits_resumereq;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      mem_0_hartsel <= 10'h0;
    end else if (_widx_T_1) begin
      mem_0_hartsel <= io_enq_bits_hartsel;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      mem_0_ackhavereset <= 1'h0;
    end else if (_widx_T_1) begin
      mem_0_ackhavereset <= io_enq_bits_ackhavereset;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      mem_0_hasel <= 1'h0;
    end else if (_widx_T_1) begin
      mem_0_hasel <= io_enq_bits_hasel;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      mem_0_hamask_0 <= 1'h0;
    end else if (_widx_T_1) begin
      mem_0_hamask_0 <= io_enq_bits_hamask_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      mem_0_hamask_1 <= 1'h0;
    end else if (_widx_T_1) begin
      mem_0_hamask_1 <= io_enq_bits_hamask_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      mem_0_hrmask_0 <= 1'h0;
    end else if (_widx_T_1) begin
      mem_0_hrmask_0 <= io_enq_bits_hrmask_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      mem_0_hrmask_1 <= 1'h0;
    end else if (_widx_T_1) begin
      mem_0_hrmask_1 <= io_enq_bits_hrmask_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      widx_widx_bin <= 1'h0;
    end else begin
      widx_widx_bin <= widx_widx_bin + _widx_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      ready_reg <= 1'h0;
    end else begin
      ready_reg <= widx_incremented != (ridx ^ 1'h1);
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      widx_gray <= 1'h0;
    end else begin
      widx_gray <= widx_widx_bin + _widx_T_1;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  mem_0_resumereq = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  mem_0_hartsel = _RAND_1[9:0];
  _RAND_2 = {1{`RANDOM}};
  mem_0_ackhavereset = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  mem_0_hasel = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  mem_0_hamask_0 = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  mem_0_hamask_1 = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  mem_0_hrmask_0 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  mem_0_hrmask_1 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  widx_widx_bin = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  ready_reg = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  widx_gray = _RAND_10[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    mem_0_resumereq = 1'h0;
  end
  if (rf_reset) begin
    mem_0_hartsel = 10'h0;
  end
  if (rf_reset) begin
    mem_0_ackhavereset = 1'h0;
  end
  if (rf_reset) begin
    mem_0_hasel = 1'h0;
  end
  if (rf_reset) begin
    mem_0_hamask_0 = 1'h0;
  end
  if (rf_reset) begin
    mem_0_hamask_1 = 1'h0;
  end
  if (rf_reset) begin
    mem_0_hrmask_0 = 1'h0;
  end
  if (rf_reset) begin
    mem_0_hrmask_1 = 1'h0;
  end
  if (reset) begin
    widx_widx_bin = 1'h0;
  end
  if (reset) begin
    ready_reg = 1'h0;
  end
  if (reset) begin
    widx_gray = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
