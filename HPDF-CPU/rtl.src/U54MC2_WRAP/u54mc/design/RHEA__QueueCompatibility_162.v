//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__QueueCompatibility_162(
  input          rf_reset,
  input          clock,
  input          reset,
  output         io_enq_ready,
  input          io_enq_valid,
  input  [2:0]   io_enq_bits_opcode,
  input  [2:0]   io_enq_bits_param,
  input  [2:0]   io_enq_bits_source,
  input  [35:0]  io_enq_bits_address,
  input  [127:0] io_enq_bits_data,
  input          io_deq_ready,
  output         io_deq_valid,
  output [2:0]   io_deq_bits_opcode,
  output [2:0]   io_deq_bits_param,
  output [2:0]   io_deq_bits_source,
  output [35:0]  io_deq_bits_address,
  output [127:0] io_deq_bits_data,
  output [3:0]   io_count
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [63:0] _RAND_3;
  reg [127:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [63:0] _RAND_8;
  reg [127:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [63:0] _RAND_13;
  reg [127:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [63:0] _RAND_18;
  reg [127:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [63:0] _RAND_23;
  reg [127:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [63:0] _RAND_28;
  reg [127:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [63:0] _RAND_33;
  reg [127:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [63:0] _RAND_38;
  reg [127:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
`endif // RANDOMIZE_REG_INIT
  reg [2:0] ram_0_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_0_param; // @[Decoupled.scala 218:16]
  reg [2:0] ram_0_source; // @[Decoupled.scala 218:16]
  reg [35:0] ram_0_address; // @[Decoupled.scala 218:16]
  reg [127:0] ram_0_data; // @[Decoupled.scala 218:16]
  reg [2:0] ram_1_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_1_param; // @[Decoupled.scala 218:16]
  reg [2:0] ram_1_source; // @[Decoupled.scala 218:16]
  reg [35:0] ram_1_address; // @[Decoupled.scala 218:16]
  reg [127:0] ram_1_data; // @[Decoupled.scala 218:16]
  reg [2:0] ram_2_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_2_param; // @[Decoupled.scala 218:16]
  reg [2:0] ram_2_source; // @[Decoupled.scala 218:16]
  reg [35:0] ram_2_address; // @[Decoupled.scala 218:16]
  reg [127:0] ram_2_data; // @[Decoupled.scala 218:16]
  reg [2:0] ram_3_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_3_param; // @[Decoupled.scala 218:16]
  reg [2:0] ram_3_source; // @[Decoupled.scala 218:16]
  reg [35:0] ram_3_address; // @[Decoupled.scala 218:16]
  reg [127:0] ram_3_data; // @[Decoupled.scala 218:16]
  reg [2:0] ram_4_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_4_param; // @[Decoupled.scala 218:16]
  reg [2:0] ram_4_source; // @[Decoupled.scala 218:16]
  reg [35:0] ram_4_address; // @[Decoupled.scala 218:16]
  reg [127:0] ram_4_data; // @[Decoupled.scala 218:16]
  reg [2:0] ram_5_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_5_param; // @[Decoupled.scala 218:16]
  reg [2:0] ram_5_source; // @[Decoupled.scala 218:16]
  reg [35:0] ram_5_address; // @[Decoupled.scala 218:16]
  reg [127:0] ram_5_data; // @[Decoupled.scala 218:16]
  reg [2:0] ram_6_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_6_param; // @[Decoupled.scala 218:16]
  reg [2:0] ram_6_source; // @[Decoupled.scala 218:16]
  reg [35:0] ram_6_address; // @[Decoupled.scala 218:16]
  reg [127:0] ram_6_data; // @[Decoupled.scala 218:16]
  reg [2:0] ram_7_opcode; // @[Decoupled.scala 218:16]
  reg [2:0] ram_7_param; // @[Decoupled.scala 218:16]
  reg [2:0] ram_7_source; // @[Decoupled.scala 218:16]
  reg [35:0] ram_7_address; // @[Decoupled.scala 218:16]
  reg [127:0] ram_7_data; // @[Decoupled.scala 218:16]
  reg [2:0] deq_ptr_value; // @[Counter.scala 60:40]
  wire [2:0] _GEN_14 = 3'h1 == deq_ptr_value ? ram_1_opcode : ram_0_opcode; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_15 = 3'h1 == deq_ptr_value ? ram_1_param : ram_0_param; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_17 = 3'h1 == deq_ptr_value ? ram_1_source : ram_0_source; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [35:0] _GEN_18 = 3'h1 == deq_ptr_value ? ram_1_address : ram_0_address; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [127:0] _GEN_26 = 3'h1 == deq_ptr_value ? ram_1_data : ram_0_data; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_28 = 3'h2 == deq_ptr_value ? ram_2_opcode : _GEN_14; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_29 = 3'h2 == deq_ptr_value ? ram_2_param : _GEN_15; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_31 = 3'h2 == deq_ptr_value ? ram_2_source : _GEN_17; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [35:0] _GEN_32 = 3'h2 == deq_ptr_value ? ram_2_address : _GEN_18; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [127:0] _GEN_40 = 3'h2 == deq_ptr_value ? ram_2_data : _GEN_26; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_42 = 3'h3 == deq_ptr_value ? ram_3_opcode : _GEN_28; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_43 = 3'h3 == deq_ptr_value ? ram_3_param : _GEN_29; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_45 = 3'h3 == deq_ptr_value ? ram_3_source : _GEN_31; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [35:0] _GEN_46 = 3'h3 == deq_ptr_value ? ram_3_address : _GEN_32; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [127:0] _GEN_54 = 3'h3 == deq_ptr_value ? ram_3_data : _GEN_40; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_56 = 3'h4 == deq_ptr_value ? ram_4_opcode : _GEN_42; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_57 = 3'h4 == deq_ptr_value ? ram_4_param : _GEN_43; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_59 = 3'h4 == deq_ptr_value ? ram_4_source : _GEN_45; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [35:0] _GEN_60 = 3'h4 == deq_ptr_value ? ram_4_address : _GEN_46; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [127:0] _GEN_68 = 3'h4 == deq_ptr_value ? ram_4_data : _GEN_54; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_70 = 3'h5 == deq_ptr_value ? ram_5_opcode : _GEN_56; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_71 = 3'h5 == deq_ptr_value ? ram_5_param : _GEN_57; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_73 = 3'h5 == deq_ptr_value ? ram_5_source : _GEN_59; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [35:0] _GEN_74 = 3'h5 == deq_ptr_value ? ram_5_address : _GEN_60; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [127:0] _GEN_82 = 3'h5 == deq_ptr_value ? ram_5_data : _GEN_68; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_84 = 3'h6 == deq_ptr_value ? ram_6_opcode : _GEN_70; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_85 = 3'h6 == deq_ptr_value ? ram_6_param : _GEN_71; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [2:0] _GEN_87 = 3'h6 == deq_ptr_value ? ram_6_source : _GEN_73; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [35:0] _GEN_88 = 3'h6 == deq_ptr_value ? ram_6_address : _GEN_74; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [127:0] _GEN_96 = 3'h6 == deq_ptr_value ? ram_6_data : _GEN_82; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire  do_enq = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  reg [2:0] enq_ptr_value; // @[Counter.scala 60:40]
  reg  maybe_full; // @[Decoupled.scala 221:27]
  wire  ptr_match = enq_ptr_value == deq_ptr_value; // @[Decoupled.scala 223:33]
  wire  empty = ptr_match & ~maybe_full; // @[Decoupled.scala 224:25]
  wire  full = ptr_match & maybe_full; // @[Decoupled.scala 225:24]
  wire  do_deq = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  wire [2:0] _value_T_1 = enq_ptr_value + 3'h1; // @[Counter.scala 76:24]
  wire [2:0] _value_T_3 = deq_ptr_value + 3'h1; // @[Counter.scala 76:24]
  wire [2:0] ptr_diff = enq_ptr_value - deq_ptr_value; // @[Decoupled.scala 257:32]
  wire [3:0] _io_count_T_1 = maybe_full & ptr_match ? 4'h8 : 4'h0; // @[Decoupled.scala 259:20]
  wire [3:0] _GEN_449 = {{1'd0}, ptr_diff}; // @[Decoupled.scala 259:62]
  assign io_enq_ready = ~full; // @[Decoupled.scala 241:19]
  assign io_deq_valid = ~empty; // @[Decoupled.scala 240:19]
  assign io_deq_bits_opcode = 3'h7 == deq_ptr_value ? ram_7_opcode : _GEN_84; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_param = 3'h7 == deq_ptr_value ? ram_7_param : _GEN_85; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_source = 3'h7 == deq_ptr_value ? ram_7_source : _GEN_87; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_address = 3'h7 == deq_ptr_value ? ram_7_address : _GEN_88; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_data = 3'h7 == deq_ptr_value ? ram_7_data : _GEN_96; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_count = _io_count_T_1 | _GEN_449; // @[Decoupled.scala 259:62]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_opcode <= 3'h0;
    end else if (do_enq) begin
      if (3'h0 == enq_ptr_value) begin
        ram_0_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_param <= 3'h0;
    end else if (do_enq) begin
      if (3'h0 == enq_ptr_value) begin
        ram_0_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_source <= 3'h0;
    end else if (do_enq) begin
      if (3'h0 == enq_ptr_value) begin
        ram_0_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_address <= 36'h0;
    end else if (do_enq) begin
      if (3'h0 == enq_ptr_value) begin
        ram_0_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_data <= 128'h0;
    end else if (do_enq) begin
      if (3'h0 == enq_ptr_value) begin
        ram_0_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_opcode <= 3'h0;
    end else if (do_enq) begin
      if (3'h1 == enq_ptr_value) begin
        ram_1_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_param <= 3'h0;
    end else if (do_enq) begin
      if (3'h1 == enq_ptr_value) begin
        ram_1_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_source <= 3'h0;
    end else if (do_enq) begin
      if (3'h1 == enq_ptr_value) begin
        ram_1_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_address <= 36'h0;
    end else if (do_enq) begin
      if (3'h1 == enq_ptr_value) begin
        ram_1_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_data <= 128'h0;
    end else if (do_enq) begin
      if (3'h1 == enq_ptr_value) begin
        ram_1_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_opcode <= 3'h0;
    end else if (do_enq) begin
      if (3'h2 == enq_ptr_value) begin
        ram_2_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_param <= 3'h0;
    end else if (do_enq) begin
      if (3'h2 == enq_ptr_value) begin
        ram_2_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_source <= 3'h0;
    end else if (do_enq) begin
      if (3'h2 == enq_ptr_value) begin
        ram_2_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_address <= 36'h0;
    end else if (do_enq) begin
      if (3'h2 == enq_ptr_value) begin
        ram_2_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_data <= 128'h0;
    end else if (do_enq) begin
      if (3'h2 == enq_ptr_value) begin
        ram_2_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_opcode <= 3'h0;
    end else if (do_enq) begin
      if (3'h3 == enq_ptr_value) begin
        ram_3_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_param <= 3'h0;
    end else if (do_enq) begin
      if (3'h3 == enq_ptr_value) begin
        ram_3_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_source <= 3'h0;
    end else if (do_enq) begin
      if (3'h3 == enq_ptr_value) begin
        ram_3_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_address <= 36'h0;
    end else if (do_enq) begin
      if (3'h3 == enq_ptr_value) begin
        ram_3_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_data <= 128'h0;
    end else if (do_enq) begin
      if (3'h3 == enq_ptr_value) begin
        ram_3_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_4_opcode <= 3'h0;
    end else if (do_enq) begin
      if (3'h4 == enq_ptr_value) begin
        ram_4_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_4_param <= 3'h0;
    end else if (do_enq) begin
      if (3'h4 == enq_ptr_value) begin
        ram_4_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_4_source <= 3'h0;
    end else if (do_enq) begin
      if (3'h4 == enq_ptr_value) begin
        ram_4_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_4_address <= 36'h0;
    end else if (do_enq) begin
      if (3'h4 == enq_ptr_value) begin
        ram_4_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_4_data <= 128'h0;
    end else if (do_enq) begin
      if (3'h4 == enq_ptr_value) begin
        ram_4_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_5_opcode <= 3'h0;
    end else if (do_enq) begin
      if (3'h5 == enq_ptr_value) begin
        ram_5_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_5_param <= 3'h0;
    end else if (do_enq) begin
      if (3'h5 == enq_ptr_value) begin
        ram_5_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_5_source <= 3'h0;
    end else if (do_enq) begin
      if (3'h5 == enq_ptr_value) begin
        ram_5_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_5_address <= 36'h0;
    end else if (do_enq) begin
      if (3'h5 == enq_ptr_value) begin
        ram_5_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_5_data <= 128'h0;
    end else if (do_enq) begin
      if (3'h5 == enq_ptr_value) begin
        ram_5_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_6_opcode <= 3'h0;
    end else if (do_enq) begin
      if (3'h6 == enq_ptr_value) begin
        ram_6_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_6_param <= 3'h0;
    end else if (do_enq) begin
      if (3'h6 == enq_ptr_value) begin
        ram_6_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_6_source <= 3'h0;
    end else if (do_enq) begin
      if (3'h6 == enq_ptr_value) begin
        ram_6_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_6_address <= 36'h0;
    end else if (do_enq) begin
      if (3'h6 == enq_ptr_value) begin
        ram_6_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_6_data <= 128'h0;
    end else if (do_enq) begin
      if (3'h6 == enq_ptr_value) begin
        ram_6_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_7_opcode <= 3'h0;
    end else if (do_enq) begin
      if (3'h7 == enq_ptr_value) begin
        ram_7_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_7_param <= 3'h0;
    end else if (do_enq) begin
      if (3'h7 == enq_ptr_value) begin
        ram_7_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_7_source <= 3'h0;
    end else if (do_enq) begin
      if (3'h7 == enq_ptr_value) begin
        ram_7_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_7_address <= 36'h0;
    end else if (do_enq) begin
      if (3'h7 == enq_ptr_value) begin
        ram_7_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_7_data <= 128'h0;
    end else if (do_enq) begin
      if (3'h7 == enq_ptr_value) begin
        ram_7_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      deq_ptr_value <= 3'h0;
    end else if (do_deq) begin
      deq_ptr_value <= _value_T_3;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      enq_ptr_value <= 3'h0;
    end else if (do_enq) begin
      enq_ptr_value <= _value_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      maybe_full <= 1'h0;
    end else if (do_enq != do_deq) begin
      maybe_full <= do_enq;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ram_0_opcode = _RAND_0[2:0];
  _RAND_1 = {1{`RANDOM}};
  ram_0_param = _RAND_1[2:0];
  _RAND_2 = {1{`RANDOM}};
  ram_0_source = _RAND_2[2:0];
  _RAND_3 = {2{`RANDOM}};
  ram_0_address = _RAND_3[35:0];
  _RAND_4 = {4{`RANDOM}};
  ram_0_data = _RAND_4[127:0];
  _RAND_5 = {1{`RANDOM}};
  ram_1_opcode = _RAND_5[2:0];
  _RAND_6 = {1{`RANDOM}};
  ram_1_param = _RAND_6[2:0];
  _RAND_7 = {1{`RANDOM}};
  ram_1_source = _RAND_7[2:0];
  _RAND_8 = {2{`RANDOM}};
  ram_1_address = _RAND_8[35:0];
  _RAND_9 = {4{`RANDOM}};
  ram_1_data = _RAND_9[127:0];
  _RAND_10 = {1{`RANDOM}};
  ram_2_opcode = _RAND_10[2:0];
  _RAND_11 = {1{`RANDOM}};
  ram_2_param = _RAND_11[2:0];
  _RAND_12 = {1{`RANDOM}};
  ram_2_source = _RAND_12[2:0];
  _RAND_13 = {2{`RANDOM}};
  ram_2_address = _RAND_13[35:0];
  _RAND_14 = {4{`RANDOM}};
  ram_2_data = _RAND_14[127:0];
  _RAND_15 = {1{`RANDOM}};
  ram_3_opcode = _RAND_15[2:0];
  _RAND_16 = {1{`RANDOM}};
  ram_3_param = _RAND_16[2:0];
  _RAND_17 = {1{`RANDOM}};
  ram_3_source = _RAND_17[2:0];
  _RAND_18 = {2{`RANDOM}};
  ram_3_address = _RAND_18[35:0];
  _RAND_19 = {4{`RANDOM}};
  ram_3_data = _RAND_19[127:0];
  _RAND_20 = {1{`RANDOM}};
  ram_4_opcode = _RAND_20[2:0];
  _RAND_21 = {1{`RANDOM}};
  ram_4_param = _RAND_21[2:0];
  _RAND_22 = {1{`RANDOM}};
  ram_4_source = _RAND_22[2:0];
  _RAND_23 = {2{`RANDOM}};
  ram_4_address = _RAND_23[35:0];
  _RAND_24 = {4{`RANDOM}};
  ram_4_data = _RAND_24[127:0];
  _RAND_25 = {1{`RANDOM}};
  ram_5_opcode = _RAND_25[2:0];
  _RAND_26 = {1{`RANDOM}};
  ram_5_param = _RAND_26[2:0];
  _RAND_27 = {1{`RANDOM}};
  ram_5_source = _RAND_27[2:0];
  _RAND_28 = {2{`RANDOM}};
  ram_5_address = _RAND_28[35:0];
  _RAND_29 = {4{`RANDOM}};
  ram_5_data = _RAND_29[127:0];
  _RAND_30 = {1{`RANDOM}};
  ram_6_opcode = _RAND_30[2:0];
  _RAND_31 = {1{`RANDOM}};
  ram_6_param = _RAND_31[2:0];
  _RAND_32 = {1{`RANDOM}};
  ram_6_source = _RAND_32[2:0];
  _RAND_33 = {2{`RANDOM}};
  ram_6_address = _RAND_33[35:0];
  _RAND_34 = {4{`RANDOM}};
  ram_6_data = _RAND_34[127:0];
  _RAND_35 = {1{`RANDOM}};
  ram_7_opcode = _RAND_35[2:0];
  _RAND_36 = {1{`RANDOM}};
  ram_7_param = _RAND_36[2:0];
  _RAND_37 = {1{`RANDOM}};
  ram_7_source = _RAND_37[2:0];
  _RAND_38 = {2{`RANDOM}};
  ram_7_address = _RAND_38[35:0];
  _RAND_39 = {4{`RANDOM}};
  ram_7_data = _RAND_39[127:0];
  _RAND_40 = {1{`RANDOM}};
  deq_ptr_value = _RAND_40[2:0];
  _RAND_41 = {1{`RANDOM}};
  enq_ptr_value = _RAND_41[2:0];
  _RAND_42 = {1{`RANDOM}};
  maybe_full = _RAND_42[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ram_0_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_0_param = 3'h0;
  end
  if (rf_reset) begin
    ram_0_source = 3'h0;
  end
  if (rf_reset) begin
    ram_0_address = 36'h0;
  end
  if (rf_reset) begin
    ram_0_data = 128'h0;
  end
  if (rf_reset) begin
    ram_1_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_1_param = 3'h0;
  end
  if (rf_reset) begin
    ram_1_source = 3'h0;
  end
  if (rf_reset) begin
    ram_1_address = 36'h0;
  end
  if (rf_reset) begin
    ram_1_data = 128'h0;
  end
  if (rf_reset) begin
    ram_2_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_2_param = 3'h0;
  end
  if (rf_reset) begin
    ram_2_source = 3'h0;
  end
  if (rf_reset) begin
    ram_2_address = 36'h0;
  end
  if (rf_reset) begin
    ram_2_data = 128'h0;
  end
  if (rf_reset) begin
    ram_3_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_3_param = 3'h0;
  end
  if (rf_reset) begin
    ram_3_source = 3'h0;
  end
  if (rf_reset) begin
    ram_3_address = 36'h0;
  end
  if (rf_reset) begin
    ram_3_data = 128'h0;
  end
  if (rf_reset) begin
    ram_4_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_4_param = 3'h0;
  end
  if (rf_reset) begin
    ram_4_source = 3'h0;
  end
  if (rf_reset) begin
    ram_4_address = 36'h0;
  end
  if (rf_reset) begin
    ram_4_data = 128'h0;
  end
  if (rf_reset) begin
    ram_5_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_5_param = 3'h0;
  end
  if (rf_reset) begin
    ram_5_source = 3'h0;
  end
  if (rf_reset) begin
    ram_5_address = 36'h0;
  end
  if (rf_reset) begin
    ram_5_data = 128'h0;
  end
  if (rf_reset) begin
    ram_6_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_6_param = 3'h0;
  end
  if (rf_reset) begin
    ram_6_source = 3'h0;
  end
  if (rf_reset) begin
    ram_6_address = 36'h0;
  end
  if (rf_reset) begin
    ram_6_data = 128'h0;
  end
  if (rf_reset) begin
    ram_7_opcode = 3'h0;
  end
  if (rf_reset) begin
    ram_7_param = 3'h0;
  end
  if (rf_reset) begin
    ram_7_source = 3'h0;
  end
  if (rf_reset) begin
    ram_7_address = 36'h0;
  end
  if (rf_reset) begin
    ram_7_data = 128'h0;
  end
  if (reset) begin
    deq_ptr_value = 3'h0;
  end
  if (reset) begin
    enq_ptr_value = 3'h0;
  end
  if (reset) begin
    maybe_full = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
