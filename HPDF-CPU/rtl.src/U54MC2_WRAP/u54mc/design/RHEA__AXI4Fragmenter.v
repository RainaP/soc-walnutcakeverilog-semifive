//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__AXI4Fragmenter(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_aw_ready,
  input         auto_in_aw_valid,
  input  [1:0]  auto_in_aw_bits_id,
  input  [36:0] auto_in_aw_bits_addr,
  input  [7:0]  auto_in_aw_bits_len,
  input  [2:0]  auto_in_aw_bits_size,
  input  [1:0]  auto_in_aw_bits_burst,
  input  [3:0]  auto_in_aw_bits_cache,
  input  [2:0]  auto_in_aw_bits_prot,
  input  [13:0] auto_in_aw_bits_echo_extra_id,
  output        auto_in_w_ready,
  input         auto_in_w_valid,
  input  [63:0] auto_in_w_bits_data,
  input  [7:0]  auto_in_w_bits_strb,
  input         auto_in_w_bits_last,
  input         auto_in_b_ready,
  output        auto_in_b_valid,
  output [1:0]  auto_in_b_bits_id,
  output [1:0]  auto_in_b_bits_resp,
  output [13:0] auto_in_b_bits_echo_extra_id,
  output        auto_in_ar_ready,
  input         auto_in_ar_valid,
  input  [1:0]  auto_in_ar_bits_id,
  input  [36:0] auto_in_ar_bits_addr,
  input  [7:0]  auto_in_ar_bits_len,
  input  [2:0]  auto_in_ar_bits_size,
  input  [1:0]  auto_in_ar_bits_burst,
  input  [3:0]  auto_in_ar_bits_cache,
  input  [2:0]  auto_in_ar_bits_prot,
  input  [13:0] auto_in_ar_bits_echo_extra_id,
  input         auto_in_r_ready,
  output        auto_in_r_valid,
  output [1:0]  auto_in_r_bits_id,
  output [63:0] auto_in_r_bits_data,
  output [1:0]  auto_in_r_bits_resp,
  output [13:0] auto_in_r_bits_echo_extra_id,
  output        auto_in_r_bits_last,
  input         auto_out_aw_ready,
  output        auto_out_aw_valid,
  output [1:0]  auto_out_aw_bits_id,
  output [36:0] auto_out_aw_bits_addr,
  output [7:0]  auto_out_aw_bits_len,
  output [2:0]  auto_out_aw_bits_size,
  output [3:0]  auto_out_aw_bits_cache,
  output [2:0]  auto_out_aw_bits_prot,
  output [13:0] auto_out_aw_bits_echo_extra_id,
  output        auto_out_aw_bits_echo_real_last,
  input         auto_out_w_ready,
  output        auto_out_w_valid,
  output [63:0] auto_out_w_bits_data,
  output [7:0]  auto_out_w_bits_strb,
  output        auto_out_w_bits_last,
  output        auto_out_b_ready,
  input         auto_out_b_valid,
  input  [1:0]  auto_out_b_bits_id,
  input  [1:0]  auto_out_b_bits_resp,
  input  [13:0] auto_out_b_bits_echo_extra_id,
  input         auto_out_b_bits_echo_real_last,
  input         auto_out_ar_ready,
  output        auto_out_ar_valid,
  output [1:0]  auto_out_ar_bits_id,
  output [36:0] auto_out_ar_bits_addr,
  output [7:0]  auto_out_ar_bits_len,
  output [2:0]  auto_out_ar_bits_size,
  output [3:0]  auto_out_ar_bits_cache,
  output [2:0]  auto_out_ar_bits_prot,
  output [13:0] auto_out_ar_bits_echo_extra_id,
  output        auto_out_ar_bits_echo_real_last,
  output        auto_out_r_ready,
  input         auto_out_r_valid,
  input  [1:0]  auto_out_r_bits_id,
  input  [63:0] auto_out_r_bits_data,
  input  [1:0]  auto_out_r_bits_resp,
  input  [13:0] auto_out_r_bits_echo_extra_id,
  input         auto_out_r_bits_echo_real_last,
  input         auto_out_r_bits_last
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [63:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [63:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
`endif // RANDOMIZE_REG_INIT
  wire  deq_rf_reset; // @[Decoupled.scala 296:21]
  wire  deq_clock; // @[Decoupled.scala 296:21]
  wire  deq_reset; // @[Decoupled.scala 296:21]
  wire  deq_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  deq_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [1:0] deq_io_enq_bits_id; // @[Decoupled.scala 296:21]
  wire [36:0] deq_io_enq_bits_addr; // @[Decoupled.scala 296:21]
  wire [7:0] deq_io_enq_bits_len; // @[Decoupled.scala 296:21]
  wire [2:0] deq_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [1:0] deq_io_enq_bits_burst; // @[Decoupled.scala 296:21]
  wire [3:0] deq_io_enq_bits_cache; // @[Decoupled.scala 296:21]
  wire [2:0] deq_io_enq_bits_prot; // @[Decoupled.scala 296:21]
  wire [13:0] deq_io_enq_bits_echo_extra_id; // @[Decoupled.scala 296:21]
  wire  deq_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  deq_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [1:0] deq_io_deq_bits_id; // @[Decoupled.scala 296:21]
  wire [36:0] deq_io_deq_bits_addr; // @[Decoupled.scala 296:21]
  wire [7:0] deq_io_deq_bits_len; // @[Decoupled.scala 296:21]
  wire [2:0] deq_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [1:0] deq_io_deq_bits_burst; // @[Decoupled.scala 296:21]
  wire [3:0] deq_io_deq_bits_cache; // @[Decoupled.scala 296:21]
  wire [2:0] deq_io_deq_bits_prot; // @[Decoupled.scala 296:21]
  wire [13:0] deq_io_deq_bits_echo_extra_id; // @[Decoupled.scala 296:21]
  wire  deq_1_rf_reset; // @[Decoupled.scala 296:21]
  wire  deq_1_clock; // @[Decoupled.scala 296:21]
  wire  deq_1_reset; // @[Decoupled.scala 296:21]
  wire  deq_1_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  deq_1_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [1:0] deq_1_io_enq_bits_id; // @[Decoupled.scala 296:21]
  wire [36:0] deq_1_io_enq_bits_addr; // @[Decoupled.scala 296:21]
  wire [7:0] deq_1_io_enq_bits_len; // @[Decoupled.scala 296:21]
  wire [2:0] deq_1_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [1:0] deq_1_io_enq_bits_burst; // @[Decoupled.scala 296:21]
  wire [3:0] deq_1_io_enq_bits_cache; // @[Decoupled.scala 296:21]
  wire [2:0] deq_1_io_enq_bits_prot; // @[Decoupled.scala 296:21]
  wire [13:0] deq_1_io_enq_bits_echo_extra_id; // @[Decoupled.scala 296:21]
  wire  deq_1_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  deq_1_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [1:0] deq_1_io_deq_bits_id; // @[Decoupled.scala 296:21]
  wire [36:0] deq_1_io_deq_bits_addr; // @[Decoupled.scala 296:21]
  wire [7:0] deq_1_io_deq_bits_len; // @[Decoupled.scala 296:21]
  wire [2:0] deq_1_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [1:0] deq_1_io_deq_bits_burst; // @[Decoupled.scala 296:21]
  wire [3:0] deq_1_io_deq_bits_cache; // @[Decoupled.scala 296:21]
  wire [2:0] deq_1_io_deq_bits_prot; // @[Decoupled.scala 296:21]
  wire [13:0] deq_1_io_deq_bits_echo_extra_id; // @[Decoupled.scala 296:21]
  wire  in_w_deq_rf_reset; // @[Decoupled.scala 296:21]
  wire  in_w_deq_clock; // @[Decoupled.scala 296:21]
  wire  in_w_deq_reset; // @[Decoupled.scala 296:21]
  wire  in_w_deq_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  in_w_deq_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [63:0] in_w_deq_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire [7:0] in_w_deq_io_enq_bits_strb; // @[Decoupled.scala 296:21]
  wire  in_w_deq_io_enq_bits_last; // @[Decoupled.scala 296:21]
  wire  in_w_deq_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  in_w_deq_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [63:0] in_w_deq_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire [7:0] in_w_deq_io_deq_bits_strb; // @[Decoupled.scala 296:21]
  wire  in_w_deq_io_deq_bits_last; // @[Decoupled.scala 296:21]
  reg  busy; // @[Fragmenter.scala 60:29]
  reg [36:0] r_addr; // @[Fragmenter.scala 61:25]
  reg [7:0] r_len; // @[Fragmenter.scala 62:25]
  wire [7:0] irr_bits_len = deq_io_deq_bits_len; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  wire [7:0] len = busy ? r_len : irr_bits_len; // @[Fragmenter.scala 64:23]
  wire [36:0] irr_bits_addr = deq_io_deq_bits_addr; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  wire [36:0] addr = busy ? r_addr : irr_bits_addr; // @[Fragmenter.scala 65:23]
  wire [7:0] alignment = addr[10:3]; // @[Fragmenter.scala 69:29]
  wire [36:0] _support1_T = addr ^ 37'h2000; // @[Parameters.scala 137:31]
  wire [37:0] _support1_T_1 = {1'b0,$signed(_support1_T)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_3 = $signed(_support1_T_1) & 38'sh189a002000; // @[Parameters.scala 137:52]
  wire  _support1_T_4 = $signed(_support1_T_3) == 38'sh0; // @[Parameters.scala 137:67]
  wire [37:0] _support1_T_6 = {1'b0,$signed(addr)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_8 = $signed(_support1_T_6) & 38'sh189a002000; // @[Parameters.scala 137:52]
  wire  _support1_T_9 = $signed(_support1_T_8) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _support1_T_10 = addr ^ 37'h2000000; // @[Parameters.scala 137:31]
  wire [37:0] _support1_T_11 = {1'b0,$signed(_support1_T_10)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_13 = $signed(_support1_T_11) & 38'sh189a000000; // @[Parameters.scala 137:52]
  wire  _support1_T_14 = $signed(_support1_T_13) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _support1_T_15 = addr ^ 37'h8000000; // @[Parameters.scala 137:31]
  wire [37:0] _support1_T_16 = {1'b0,$signed(_support1_T_15)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_18 = $signed(_support1_T_16) & 38'sh1898000000; // @[Parameters.scala 137:52]
  wire  _support1_T_19 = $signed(_support1_T_18) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _support1_T_20 = addr ^ 37'h10000000; // @[Parameters.scala 137:31]
  wire [37:0] _support1_T_21 = {1'b0,$signed(_support1_T_20)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_23 = $signed(_support1_T_21) & 38'sh189a000000; // @[Parameters.scala 137:52]
  wire  _support1_T_24 = $signed(_support1_T_23) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _support1_T_25 = addr ^ 37'h80000000; // @[Parameters.scala 137:31]
  wire [37:0] _support1_T_26 = {1'b0,$signed(_support1_T_25)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_28 = $signed(_support1_T_26) & 38'sh1880000000; // @[Parameters.scala 137:52]
  wire  _support1_T_29 = $signed(_support1_T_28) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _support1_T_30 = addr ^ 37'h800000000; // @[Parameters.scala 137:31]
  wire [37:0] _support1_T_31 = {1'b0,$signed(_support1_T_30)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_33 = $signed(_support1_T_31) & 38'sh1800000000; // @[Parameters.scala 137:52]
  wire  _support1_T_34 = $signed(_support1_T_33) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _support1_T_35 = addr ^ 37'h1000000000; // @[Parameters.scala 137:31]
  wire [37:0] _support1_T_36 = {1'b0,$signed(_support1_T_35)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_38 = $signed(_support1_T_36) & 38'sh1000000000; // @[Parameters.scala 137:52]
  wire  _support1_T_39 = $signed(_support1_T_38) == 38'sh0; // @[Parameters.scala 137:67]
  wire  _support1_T_45 = _support1_T_9 | _support1_T_14 | _support1_T_19 | _support1_T_24 | _support1_T_29 |
    _support1_T_34 | _support1_T_39; // @[Fragmenter.scala 76:100]
  wire [4:0] _support1_T_46 = _support1_T_4 ? 5'h1f : 5'h0; // @[Mux.scala 27:72]
  wire [2:0] _support1_T_47 = _support1_T_45 ? 3'h7 : 3'h0; // @[Mux.scala 27:72]
  wire [4:0] _GEN_20 = {{2'd0}, _support1_T_47}; // @[Mux.scala 27:72]
  wire [4:0] support1 = _support1_T_46 | _GEN_20; // @[Mux.scala 27:72]
  wire [7:0] _GEN_21 = {{1'd0}, len[7:1]}; // @[package.scala 253:43]
  wire [7:0] _fillLow_T_1 = len | _GEN_21; // @[package.scala 253:43]
  wire [7:0] _GEN_22 = {{2'd0}, _fillLow_T_1[7:2]}; // @[package.scala 253:43]
  wire [7:0] _fillLow_T_3 = _fillLow_T_1 | _GEN_22; // @[package.scala 253:43]
  wire [7:0] _GEN_23 = {{4'd0}, _fillLow_T_3[7:4]}; // @[package.scala 253:43]
  wire [7:0] _fillLow_T_5 = _fillLow_T_3 | _GEN_23; // @[package.scala 253:43]
  wire [6:0] fillLow = _fillLow_T_5[7:1]; // @[Fragmenter.scala 85:37]
  wire [7:0] _wipeHigh_T = ~len; // @[Fragmenter.scala 86:32]
  wire [8:0] _wipeHigh_T_1 = {_wipeHigh_T, 1'h0}; // @[package.scala 244:48]
  wire [7:0] _wipeHigh_T_3 = _wipeHigh_T | _wipeHigh_T_1[7:0]; // @[package.scala 244:43]
  wire [9:0] _wipeHigh_T_4 = {_wipeHigh_T_3, 2'h0}; // @[package.scala 244:48]
  wire [7:0] _wipeHigh_T_6 = _wipeHigh_T_3 | _wipeHigh_T_4[7:0]; // @[package.scala 244:43]
  wire [11:0] _wipeHigh_T_7 = {_wipeHigh_T_6, 4'h0}; // @[package.scala 244:48]
  wire [7:0] _wipeHigh_T_9 = _wipeHigh_T_6 | _wipeHigh_T_7[7:0]; // @[package.scala 244:43]
  wire [7:0] wipeHigh = ~_wipeHigh_T_9; // @[Fragmenter.scala 86:24]
  wire [7:0] _GEN_24 = {{1'd0}, fillLow}; // @[Fragmenter.scala 87:32]
  wire [7:0] remain1 = _GEN_24 | wipeHigh; // @[Fragmenter.scala 87:32]
  wire [8:0] _align1_T = {alignment, 1'h0}; // @[package.scala 244:48]
  wire [7:0] _align1_T_2 = alignment | _align1_T[7:0]; // @[package.scala 244:43]
  wire [9:0] _align1_T_3 = {_align1_T_2, 2'h0}; // @[package.scala 244:48]
  wire [7:0] _align1_T_5 = _align1_T_2 | _align1_T_3[7:0]; // @[package.scala 244:43]
  wire [11:0] _align1_T_6 = {_align1_T_5, 4'h0}; // @[package.scala 244:48]
  wire [7:0] _align1_T_8 = _align1_T_5 | _align1_T_6[7:0]; // @[package.scala 244:43]
  wire [7:0] align1 = ~_align1_T_8; // @[Fragmenter.scala 88:24]
  wire [7:0] _maxSupported1_T = remain1 & align1; // @[Fragmenter.scala 89:37]
  wire [7:0] _GEN_25 = {{3'd0}, support1}; // @[Fragmenter.scala 89:46]
  wire [7:0] maxSupported1 = _maxSupported1_T & _GEN_25; // @[Fragmenter.scala 89:46]
  wire [1:0] irr_bits_burst = deq_io_deq_bits_burst; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  wire  fixed = irr_bits_burst == 2'h0; // @[Fragmenter.scala 92:34]
  wire [2:0] irr_bits_size = deq_io_deq_bits_size; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  wire  narrow = irr_bits_size != 3'h3; // @[Fragmenter.scala 93:34]
  wire  bad = fixed | narrow; // @[Fragmenter.scala 94:25]
  wire [7:0] beats_lo = bad ? 8'h0 : maxSupported1; // @[Fragmenter.scala 97:25]
  wire [8:0] _beats_T = {beats_lo, 1'h0}; // @[package.scala 232:35]
  wire [8:0] _beats_T_1 = _beats_T | 9'h1; // @[package.scala 232:40]
  wire [8:0] _beats_T_2 = {1'h0,beats_lo}; // @[Cat.scala 30:58]
  wire [8:0] _beats_T_3 = ~_beats_T_2; // @[package.scala 232:53]
  wire [8:0] beats = _beats_T_1 & _beats_T_3; // @[package.scala 232:51]
  wire [15:0] _GEN_26 = {{7'd0}, beats}; // @[Fragmenter.scala 100:38]
  wire [15:0] _inc_addr_T = _GEN_26 << irr_bits_size; // @[Fragmenter.scala 100:38]
  wire [36:0] _GEN_27 = {{21'd0}, _inc_addr_T}; // @[Fragmenter.scala 100:29]
  wire [36:0] inc_addr = addr + _GEN_27; // @[Fragmenter.scala 100:29]
  wire [15:0] _wrapMask_T = {irr_bits_len,8'hff}; // @[Cat.scala 30:58]
  wire [22:0] _GEN_28 = {{7'd0}, _wrapMask_T}; // @[Bundles.scala 33:21]
  wire [22:0] _wrapMask_T_1 = _GEN_28 << irr_bits_size; // @[Bundles.scala 33:21]
  wire [14:0] wrapMask = _wrapMask_T_1[22:8]; // @[Bundles.scala 33:30]
  wire [36:0] _GEN_29 = {{22'd0}, wrapMask}; // @[Fragmenter.scala 104:33]
  wire [36:0] _mux_addr_T = inc_addr & _GEN_29; // @[Fragmenter.scala 104:33]
  wire [36:0] _mux_addr_T_1 = ~irr_bits_addr; // @[Fragmenter.scala 104:49]
  wire [36:0] _mux_addr_T_2 = _mux_addr_T_1 | _GEN_29; // @[Fragmenter.scala 104:62]
  wire [36:0] _mux_addr_T_3 = ~_mux_addr_T_2; // @[Fragmenter.scala 104:47]
  wire [36:0] _mux_addr_T_4 = _mux_addr_T | _mux_addr_T_3; // @[Fragmenter.scala 104:45]
  wire  ar_last = beats_lo == len; // @[Fragmenter.scala 110:27]
  wire [36:0] _out_bits_addr_T = ~addr; // @[Fragmenter.scala 122:28]
  wire [9:0] _out_bits_addr_T_2 = 10'h7 << irr_bits_size; // @[package.scala 234:77]
  wire [2:0] _out_bits_addr_T_4 = ~_out_bits_addr_T_2[2:0]; // @[package.scala 234:46]
  wire [36:0] _GEN_31 = {{34'd0}, _out_bits_addr_T_4}; // @[Fragmenter.scala 122:34]
  wire [36:0] _out_bits_addr_T_5 = _out_bits_addr_T | _GEN_31; // @[Fragmenter.scala 122:34]
  wire  irr_valid = deq_io_deq_valid; // @[Decoupled.scala 317:19 Decoupled.scala 319:15]
  wire  _T_2 = auto_out_ar_ready & irr_valid; // @[Decoupled.scala 40:37]
  wire [8:0] _GEN_32 = {{1'd0}, len}; // @[Fragmenter.scala 127:25]
  wire [8:0] _r_len_T_1 = _GEN_32 - beats; // @[Fragmenter.scala 127:25]
  wire [8:0] _GEN_4 = _T_2 ? _r_len_T_1 : {{1'd0}, r_len}; // @[Fragmenter.scala 124:27 Fragmenter.scala 127:18 Fragmenter.scala 62:25]
  reg  busy_1; // @[Fragmenter.scala 60:29]
  reg [36:0] r_addr_1; // @[Fragmenter.scala 61:25]
  reg [7:0] r_len_1; // @[Fragmenter.scala 62:25]
  wire [7:0] irr_1_bits_len = deq_1_io_deq_bits_len; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  wire [7:0] len_1 = busy_1 ? r_len_1 : irr_1_bits_len; // @[Fragmenter.scala 64:23]
  wire [36:0] irr_1_bits_addr = deq_1_io_deq_bits_addr; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  wire [36:0] addr_1 = busy_1 ? r_addr_1 : irr_1_bits_addr; // @[Fragmenter.scala 65:23]
  wire [7:0] alignment_1 = addr_1[10:3]; // @[Fragmenter.scala 69:29]
  wire [36:0] _support1_T_49 = addr_1 ^ 37'h2000; // @[Parameters.scala 137:31]
  wire [37:0] _support1_T_50 = {1'b0,$signed(_support1_T_49)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_52 = $signed(_support1_T_50) & 38'sh189a002000; // @[Parameters.scala 137:52]
  wire  _support1_T_53 = $signed(_support1_T_52) == 38'sh0; // @[Parameters.scala 137:67]
  wire [37:0] _support1_T_55 = {1'b0,$signed(addr_1)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_57 = $signed(_support1_T_55) & 38'sh189a002000; // @[Parameters.scala 137:52]
  wire  _support1_T_58 = $signed(_support1_T_57) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _support1_T_59 = addr_1 ^ 37'h2000000; // @[Parameters.scala 137:31]
  wire [37:0] _support1_T_60 = {1'b0,$signed(_support1_T_59)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_62 = $signed(_support1_T_60) & 38'sh189a000000; // @[Parameters.scala 137:52]
  wire  _support1_T_63 = $signed(_support1_T_62) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _support1_T_64 = addr_1 ^ 37'h8000000; // @[Parameters.scala 137:31]
  wire [37:0] _support1_T_65 = {1'b0,$signed(_support1_T_64)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_67 = $signed(_support1_T_65) & 38'sh1898000000; // @[Parameters.scala 137:52]
  wire  _support1_T_68 = $signed(_support1_T_67) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _support1_T_69 = addr_1 ^ 37'h10000000; // @[Parameters.scala 137:31]
  wire [37:0] _support1_T_70 = {1'b0,$signed(_support1_T_69)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_72 = $signed(_support1_T_70) & 38'sh189a000000; // @[Parameters.scala 137:52]
  wire  _support1_T_73 = $signed(_support1_T_72) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _support1_T_74 = addr_1 ^ 37'h80000000; // @[Parameters.scala 137:31]
  wire [37:0] _support1_T_75 = {1'b0,$signed(_support1_T_74)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_77 = $signed(_support1_T_75) & 38'sh1880000000; // @[Parameters.scala 137:52]
  wire  _support1_T_78 = $signed(_support1_T_77) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _support1_T_79 = addr_1 ^ 37'h800000000; // @[Parameters.scala 137:31]
  wire [37:0] _support1_T_80 = {1'b0,$signed(_support1_T_79)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_82 = $signed(_support1_T_80) & 38'sh1800000000; // @[Parameters.scala 137:52]
  wire  _support1_T_83 = $signed(_support1_T_82) == 38'sh0; // @[Parameters.scala 137:67]
  wire  _support1_T_88 = _support1_T_58 | _support1_T_63 | _support1_T_68 | _support1_T_73 | _support1_T_78 |
    _support1_T_83; // @[Fragmenter.scala 76:100]
  wire [36:0] _support1_T_89 = addr_1 ^ 37'h1000000000; // @[Parameters.scala 137:31]
  wire [37:0] _support1_T_90 = {1'b0,$signed(_support1_T_89)}; // @[Parameters.scala 137:49]
  wire [37:0] _support1_T_92 = $signed(_support1_T_90) & 38'sh1000000000; // @[Parameters.scala 137:52]
  wire  _support1_T_93 = $signed(_support1_T_92) == 38'sh0; // @[Parameters.scala 137:67]
  wire [4:0] _support1_T_94 = _support1_T_53 ? 5'h1f : 5'h0; // @[Mux.scala 27:72]
  wire [2:0] _support1_T_95 = _support1_T_88 ? 3'h7 : 3'h0; // @[Mux.scala 27:72]
  wire [3:0] _support1_T_96 = _support1_T_93 ? 4'hf : 4'h0; // @[Mux.scala 27:72]
  wire [4:0] _GEN_33 = {{2'd0}, _support1_T_95}; // @[Mux.scala 27:72]
  wire [4:0] _support1_T_97 = _support1_T_94 | _GEN_33; // @[Mux.scala 27:72]
  wire [4:0] _GEN_34 = {{1'd0}, _support1_T_96}; // @[Mux.scala 27:72]
  wire [4:0] support1_1 = _support1_T_97 | _GEN_34; // @[Mux.scala 27:72]
  wire [7:0] _GEN_35 = {{1'd0}, len_1[7:1]}; // @[package.scala 253:43]
  wire [7:0] _fillLow_T_8 = len_1 | _GEN_35; // @[package.scala 253:43]
  wire [7:0] _GEN_36 = {{2'd0}, _fillLow_T_8[7:2]}; // @[package.scala 253:43]
  wire [7:0] _fillLow_T_10 = _fillLow_T_8 | _GEN_36; // @[package.scala 253:43]
  wire [7:0] _GEN_37 = {{4'd0}, _fillLow_T_10[7:4]}; // @[package.scala 253:43]
  wire [7:0] _fillLow_T_12 = _fillLow_T_10 | _GEN_37; // @[package.scala 253:43]
  wire [6:0] fillLow_1 = _fillLow_T_12[7:1]; // @[Fragmenter.scala 85:37]
  wire [7:0] _wipeHigh_T_11 = ~len_1; // @[Fragmenter.scala 86:32]
  wire [8:0] _wipeHigh_T_12 = {_wipeHigh_T_11, 1'h0}; // @[package.scala 244:48]
  wire [7:0] _wipeHigh_T_14 = _wipeHigh_T_11 | _wipeHigh_T_12[7:0]; // @[package.scala 244:43]
  wire [9:0] _wipeHigh_T_15 = {_wipeHigh_T_14, 2'h0}; // @[package.scala 244:48]
  wire [7:0] _wipeHigh_T_17 = _wipeHigh_T_14 | _wipeHigh_T_15[7:0]; // @[package.scala 244:43]
  wire [11:0] _wipeHigh_T_18 = {_wipeHigh_T_17, 4'h0}; // @[package.scala 244:48]
  wire [7:0] _wipeHigh_T_20 = _wipeHigh_T_17 | _wipeHigh_T_18[7:0]; // @[package.scala 244:43]
  wire [7:0] wipeHigh_1 = ~_wipeHigh_T_20; // @[Fragmenter.scala 86:24]
  wire [7:0] _GEN_38 = {{1'd0}, fillLow_1}; // @[Fragmenter.scala 87:32]
  wire [7:0] remain1_1 = _GEN_38 | wipeHigh_1; // @[Fragmenter.scala 87:32]
  wire [8:0] _align1_T_10 = {alignment_1, 1'h0}; // @[package.scala 244:48]
  wire [7:0] _align1_T_12 = alignment_1 | _align1_T_10[7:0]; // @[package.scala 244:43]
  wire [9:0] _align1_T_13 = {_align1_T_12, 2'h0}; // @[package.scala 244:48]
  wire [7:0] _align1_T_15 = _align1_T_12 | _align1_T_13[7:0]; // @[package.scala 244:43]
  wire [11:0] _align1_T_16 = {_align1_T_15, 4'h0}; // @[package.scala 244:48]
  wire [7:0] _align1_T_18 = _align1_T_15 | _align1_T_16[7:0]; // @[package.scala 244:43]
  wire [7:0] align1_1 = ~_align1_T_18; // @[Fragmenter.scala 88:24]
  wire [7:0] _maxSupported1_T_1 = remain1_1 & align1_1; // @[Fragmenter.scala 89:37]
  wire [7:0] _GEN_39 = {{3'd0}, support1_1}; // @[Fragmenter.scala 89:46]
  wire [7:0] maxSupported1_1 = _maxSupported1_T_1 & _GEN_39; // @[Fragmenter.scala 89:46]
  wire [1:0] irr_1_bits_burst = deq_1_io_deq_bits_burst; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  wire  fixed_1 = irr_1_bits_burst == 2'h0; // @[Fragmenter.scala 92:34]
  wire [2:0] irr_1_bits_size = deq_1_io_deq_bits_size; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  wire  narrow_1 = irr_1_bits_size != 3'h3; // @[Fragmenter.scala 93:34]
  wire  bad_1 = fixed_1 | narrow_1; // @[Fragmenter.scala 94:25]
  wire [7:0] beats_lo_1 = bad_1 ? 8'h0 : maxSupported1_1; // @[Fragmenter.scala 97:25]
  wire [8:0] _beats_T_4 = {beats_lo_1, 1'h0}; // @[package.scala 232:35]
  wire [8:0] _beats_T_5 = _beats_T_4 | 9'h1; // @[package.scala 232:40]
  wire [8:0] _beats_T_6 = {1'h0,beats_lo_1}; // @[Cat.scala 30:58]
  wire [8:0] _beats_T_7 = ~_beats_T_6; // @[package.scala 232:53]
  wire [8:0] w_beats = _beats_T_5 & _beats_T_7; // @[package.scala 232:51]
  wire [15:0] _GEN_40 = {{7'd0}, w_beats}; // @[Fragmenter.scala 100:38]
  wire [15:0] _inc_addr_T_2 = _GEN_40 << irr_1_bits_size; // @[Fragmenter.scala 100:38]
  wire [36:0] _GEN_41 = {{21'd0}, _inc_addr_T_2}; // @[Fragmenter.scala 100:29]
  wire [36:0] inc_addr_1 = addr_1 + _GEN_41; // @[Fragmenter.scala 100:29]
  wire [15:0] _wrapMask_T_2 = {irr_1_bits_len,8'hff}; // @[Cat.scala 30:58]
  wire [22:0] _GEN_42 = {{7'd0}, _wrapMask_T_2}; // @[Bundles.scala 33:21]
  wire [22:0] _wrapMask_T_3 = _GEN_42 << irr_1_bits_size; // @[Bundles.scala 33:21]
  wire [14:0] wrapMask_1 = _wrapMask_T_3[22:8]; // @[Bundles.scala 33:30]
  wire [36:0] _GEN_43 = {{22'd0}, wrapMask_1}; // @[Fragmenter.scala 104:33]
  wire [36:0] _mux_addr_T_5 = inc_addr_1 & _GEN_43; // @[Fragmenter.scala 104:33]
  wire [36:0] _mux_addr_T_6 = ~irr_1_bits_addr; // @[Fragmenter.scala 104:49]
  wire [36:0] _mux_addr_T_7 = _mux_addr_T_6 | _GEN_43; // @[Fragmenter.scala 104:62]
  wire [36:0] _mux_addr_T_8 = ~_mux_addr_T_7; // @[Fragmenter.scala 104:47]
  wire [36:0] _mux_addr_T_9 = _mux_addr_T_5 | _mux_addr_T_8; // @[Fragmenter.scala 104:45]
  wire  aw_last = beats_lo_1 == len_1; // @[Fragmenter.scala 110:27]
  reg [8:0] w_counter; // @[Fragmenter.scala 164:30]
  wire  w_idle = w_counter == 9'h0; // @[Fragmenter.scala 165:30]
  reg  wbeats_latched; // @[Fragmenter.scala 150:35]
  wire  _in_aw_ready_T = w_idle | wbeats_latched; // @[Fragmenter.scala 158:52]
  wire  in_aw_ready = auto_out_aw_ready & (w_idle | wbeats_latched); // @[Fragmenter.scala 158:35]
  wire [36:0] _out_bits_addr_T_7 = ~addr_1; // @[Fragmenter.scala 122:28]
  wire [9:0] _out_bits_addr_T_9 = 10'h7 << irr_1_bits_size; // @[package.scala 234:77]
  wire [2:0] _out_bits_addr_T_11 = ~_out_bits_addr_T_9[2:0]; // @[package.scala 234:46]
  wire [36:0] _GEN_45 = {{34'd0}, _out_bits_addr_T_11}; // @[Fragmenter.scala 122:34]
  wire [36:0] _out_bits_addr_T_12 = _out_bits_addr_T_7 | _GEN_45; // @[Fragmenter.scala 122:34]
  wire  irr_1_valid = deq_1_io_deq_valid; // @[Decoupled.scala 317:19 Decoupled.scala 319:15]
  wire  _T_5 = in_aw_ready & irr_1_valid; // @[Decoupled.scala 40:37]
  wire [8:0] _GEN_46 = {{1'd0}, len_1}; // @[Fragmenter.scala 127:25]
  wire [8:0] _r_len_T_3 = _GEN_46 - w_beats; // @[Fragmenter.scala 127:25]
  wire [8:0] _GEN_9 = _T_5 ? _r_len_T_3 : {{1'd0}, r_len_1}; // @[Fragmenter.scala 124:27 Fragmenter.scala 127:18 Fragmenter.scala 62:25]
  wire  wbeats_valid = irr_1_valid & ~wbeats_latched; // @[Fragmenter.scala 159:35]
  wire  _GEN_10 = wbeats_valid & w_idle | wbeats_latched; // @[Fragmenter.scala 153:43 Fragmenter.scala 153:60 Fragmenter.scala 150:35]
  wire  bundleOut_0_aw_valid = irr_1_valid & _in_aw_ready_T; // @[Fragmenter.scala 157:35]
  wire  _T_7 = auto_out_aw_ready & bundleOut_0_aw_valid; // @[Decoupled.scala 40:37]
  wire [8:0] _w_todo_T = wbeats_valid ? w_beats : 9'h0; // @[Fragmenter.scala 166:35]
  wire [8:0] w_todo = w_idle ? _w_todo_T : w_counter; // @[Fragmenter.scala 166:23]
  wire  in_w_valid = in_w_deq_io_deq_valid; // @[Decoupled.scala 317:19 Decoupled.scala 319:15]
  wire  _bundleOut_0_w_valid_T_1 = ~w_idle | wbeats_valid; // @[Fragmenter.scala 173:51]
  wire  bundleOut_0_w_valid = in_w_valid & (~w_idle | wbeats_valid); // @[Fragmenter.scala 173:33]
  wire  _w_counter_T = auto_out_w_ready & bundleOut_0_w_valid; // @[Decoupled.scala 40:37]
  wire [8:0] _GEN_47 = {{8'd0}, _w_counter_T}; // @[Fragmenter.scala 168:27]
  wire  bundleOut_0_b_ready = auto_in_b_ready | ~auto_out_b_bits_echo_real_last; // @[Fragmenter.scala 189:33]
  reg [1:0] error_0; // @[Fragmenter.scala 192:26]
  reg [1:0] error_1; // @[Fragmenter.scala 192:26]
  reg [1:0] error_2; // @[Fragmenter.scala 192:26]
  reg [1:0] error_3; // @[Fragmenter.scala 192:26]
  wire [1:0] _GEN_13 = 2'h1 == auto_out_b_bits_id ? error_1 : error_0; // @[Fragmenter.scala 193:41 Fragmenter.scala 193:41]
  wire [1:0] _GEN_14 = 2'h2 == auto_out_b_bits_id ? error_2 : _GEN_13; // @[Fragmenter.scala 193:41 Fragmenter.scala 193:41]
  wire [1:0] _GEN_15 = 2'h3 == auto_out_b_bits_id ? error_3 : _GEN_14; // @[Fragmenter.scala 193:41 Fragmenter.scala 193:41]
  wire [3:0] _T_22 = 4'h1 << auto_out_b_bits_id; // @[OneHot.scala 65:12]
  wire  _T_28 = bundleOut_0_b_ready & auto_out_b_valid; // @[Decoupled.scala 40:37]
  wire [1:0] _error_0_T = error_0 | auto_out_b_bits_resp; // @[Fragmenter.scala 195:70]
  wire [1:0] _error_1_T = error_1 | auto_out_b_bits_resp; // @[Fragmenter.scala 195:70]
  wire [1:0] _error_2_T = error_2 | auto_out_b_bits_resp; // @[Fragmenter.scala 195:70]
  wire [1:0] _error_3_T = error_3 | auto_out_b_bits_resp; // @[Fragmenter.scala 195:70]
  RHEA__Queue_28 deq ( // @[Decoupled.scala 296:21]
    .rf_reset(deq_rf_reset),
    .clock(deq_clock),
    .reset(deq_reset),
    .io_enq_ready(deq_io_enq_ready),
    .io_enq_valid(deq_io_enq_valid),
    .io_enq_bits_id(deq_io_enq_bits_id),
    .io_enq_bits_addr(deq_io_enq_bits_addr),
    .io_enq_bits_len(deq_io_enq_bits_len),
    .io_enq_bits_size(deq_io_enq_bits_size),
    .io_enq_bits_burst(deq_io_enq_bits_burst),
    .io_enq_bits_cache(deq_io_enq_bits_cache),
    .io_enq_bits_prot(deq_io_enq_bits_prot),
    .io_enq_bits_echo_extra_id(deq_io_enq_bits_echo_extra_id),
    .io_deq_ready(deq_io_deq_ready),
    .io_deq_valid(deq_io_deq_valid),
    .io_deq_bits_id(deq_io_deq_bits_id),
    .io_deq_bits_addr(deq_io_deq_bits_addr),
    .io_deq_bits_len(deq_io_deq_bits_len),
    .io_deq_bits_size(deq_io_deq_bits_size),
    .io_deq_bits_burst(deq_io_deq_bits_burst),
    .io_deq_bits_cache(deq_io_deq_bits_cache),
    .io_deq_bits_prot(deq_io_deq_bits_prot),
    .io_deq_bits_echo_extra_id(deq_io_deq_bits_echo_extra_id)
  );
  RHEA__Queue_28 deq_1 ( // @[Decoupled.scala 296:21]
    .rf_reset(deq_1_rf_reset),
    .clock(deq_1_clock),
    .reset(deq_1_reset),
    .io_enq_ready(deq_1_io_enq_ready),
    .io_enq_valid(deq_1_io_enq_valid),
    .io_enq_bits_id(deq_1_io_enq_bits_id),
    .io_enq_bits_addr(deq_1_io_enq_bits_addr),
    .io_enq_bits_len(deq_1_io_enq_bits_len),
    .io_enq_bits_size(deq_1_io_enq_bits_size),
    .io_enq_bits_burst(deq_1_io_enq_bits_burst),
    .io_enq_bits_cache(deq_1_io_enq_bits_cache),
    .io_enq_bits_prot(deq_1_io_enq_bits_prot),
    .io_enq_bits_echo_extra_id(deq_1_io_enq_bits_echo_extra_id),
    .io_deq_ready(deq_1_io_deq_ready),
    .io_deq_valid(deq_1_io_deq_valid),
    .io_deq_bits_id(deq_1_io_deq_bits_id),
    .io_deq_bits_addr(deq_1_io_deq_bits_addr),
    .io_deq_bits_len(deq_1_io_deq_bits_len),
    .io_deq_bits_size(deq_1_io_deq_bits_size),
    .io_deq_bits_burst(deq_1_io_deq_bits_burst),
    .io_deq_bits_cache(deq_1_io_deq_bits_cache),
    .io_deq_bits_prot(deq_1_io_deq_bits_prot),
    .io_deq_bits_echo_extra_id(deq_1_io_deq_bits_echo_extra_id)
  );
  RHEA__Queue_30 in_w_deq ( // @[Decoupled.scala 296:21]
    .rf_reset(in_w_deq_rf_reset),
    .clock(in_w_deq_clock),
    .reset(in_w_deq_reset),
    .io_enq_ready(in_w_deq_io_enq_ready),
    .io_enq_valid(in_w_deq_io_enq_valid),
    .io_enq_bits_data(in_w_deq_io_enq_bits_data),
    .io_enq_bits_strb(in_w_deq_io_enq_bits_strb),
    .io_enq_bits_last(in_w_deq_io_enq_bits_last),
    .io_deq_ready(in_w_deq_io_deq_ready),
    .io_deq_valid(in_w_deq_io_deq_valid),
    .io_deq_bits_data(in_w_deq_io_deq_bits_data),
    .io_deq_bits_strb(in_w_deq_io_deq_bits_strb),
    .io_deq_bits_last(in_w_deq_io_deq_bits_last)
  );
  assign deq_rf_reset = rf_reset;
  assign deq_1_rf_reset = rf_reset;
  assign in_w_deq_rf_reset = rf_reset;
  assign auto_in_aw_ready = deq_1_io_enq_ready; // @[Nodes.scala 1529:13 Decoupled.scala 299:17]
  assign auto_in_w_ready = in_w_deq_io_enq_ready; // @[Nodes.scala 1529:13 Decoupled.scala 299:17]
  assign auto_in_b_valid = auto_out_b_valid & auto_out_b_bits_echo_real_last; // @[Fragmenter.scala 188:33]
  assign auto_in_b_bits_id = auto_out_b_bits_id; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_b_bits_resp = auto_out_b_bits_resp | _GEN_15; // @[Fragmenter.scala 193:41]
  assign auto_in_b_bits_echo_extra_id = auto_out_b_bits_echo_extra_id; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_ar_ready = deq_io_enq_ready; // @[Nodes.scala 1529:13 Decoupled.scala 299:17]
  assign auto_in_r_valid = auto_out_r_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_bits_id = auto_out_r_bits_id; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_bits_data = auto_out_r_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_bits_resp = auto_out_r_bits_resp; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_bits_echo_extra_id = auto_out_r_bits_echo_extra_id; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_bits_last = auto_out_r_bits_last & auto_out_r_bits_echo_real_last; // @[Fragmenter.scala 183:41]
  assign auto_out_aw_valid = irr_1_valid & _in_aw_ready_T; // @[Fragmenter.scala 157:35]
  assign auto_out_aw_bits_id = deq_1_io_deq_bits_id; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_addr = ~_out_bits_addr_T_12; // @[Fragmenter.scala 122:26]
  assign auto_out_aw_bits_len = bad_1 ? 8'h0 : maxSupported1_1; // @[Fragmenter.scala 97:25]
  assign auto_out_aw_bits_size = deq_1_io_deq_bits_size; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_cache = deq_1_io_deq_bits_cache; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_prot = deq_1_io_deq_bits_prot; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_echo_extra_id = deq_1_io_deq_bits_echo_extra_id; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_echo_real_last = beats_lo_1 == len_1; // @[Fragmenter.scala 110:27]
  assign auto_out_w_valid = in_w_valid & (~w_idle | wbeats_valid); // @[Fragmenter.scala 173:33]
  assign auto_out_w_bits_data = in_w_deq_io_deq_bits_data; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_w_bits_strb = in_w_deq_io_deq_bits_strb; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_w_bits_last = w_todo == 9'h1; // @[Fragmenter.scala 167:27]
  assign auto_out_b_ready = auto_in_b_ready | ~auto_out_b_bits_echo_real_last; // @[Fragmenter.scala 189:33]
  assign auto_out_ar_valid = deq_io_deq_valid; // @[Decoupled.scala 317:19 Decoupled.scala 319:15]
  assign auto_out_ar_bits_id = deq_io_deq_bits_id; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_addr = ~_out_bits_addr_T_5; // @[Fragmenter.scala 122:26]
  assign auto_out_ar_bits_len = bad ? 8'h0 : maxSupported1; // @[Fragmenter.scala 97:25]
  assign auto_out_ar_bits_size = deq_io_deq_bits_size; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_cache = deq_io_deq_bits_cache; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_prot = deq_io_deq_bits_prot; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_echo_extra_id = deq_io_deq_bits_echo_extra_id; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_echo_real_last = beats_lo == len; // @[Fragmenter.scala 110:27]
  assign auto_out_r_ready = auto_in_r_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_clock = clock;
  assign deq_reset = reset;
  assign deq_io_enq_valid = auto_in_ar_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_io_enq_bits_id = auto_in_ar_bits_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_io_enq_bits_addr = auto_in_ar_bits_addr; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_io_enq_bits_len = auto_in_ar_bits_len; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_io_enq_bits_size = auto_in_ar_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_io_enq_bits_burst = auto_in_ar_bits_burst; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_io_enq_bits_cache = auto_in_ar_bits_cache; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_io_enq_bits_prot = auto_in_ar_bits_prot; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_io_enq_bits_echo_extra_id = auto_in_ar_bits_echo_extra_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_io_deq_ready = auto_out_ar_ready & ar_last; // @[Fragmenter.scala 111:30]
  assign deq_1_clock = clock;
  assign deq_1_reset = reset;
  assign deq_1_io_enq_valid = auto_in_aw_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_1_io_enq_bits_id = auto_in_aw_bits_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_1_io_enq_bits_addr = auto_in_aw_bits_addr; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_1_io_enq_bits_len = auto_in_aw_bits_len; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_1_io_enq_bits_size = auto_in_aw_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_1_io_enq_bits_burst = auto_in_aw_bits_burst; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_1_io_enq_bits_cache = auto_in_aw_bits_cache; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_1_io_enq_bits_prot = auto_in_aw_bits_prot; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_1_io_enq_bits_echo_extra_id = auto_in_aw_bits_echo_extra_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign deq_1_io_deq_ready = in_aw_ready & aw_last; // @[Fragmenter.scala 111:30]
  assign in_w_deq_clock = clock;
  assign in_w_deq_reset = reset;
  assign in_w_deq_io_enq_valid = auto_in_w_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign in_w_deq_io_enq_bits_data = auto_in_w_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign in_w_deq_io_enq_bits_strb = auto_in_w_bits_strb; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign in_w_deq_io_enq_bits_last = auto_in_w_bits_last; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign in_w_deq_io_deq_ready = auto_out_w_ready & _bundleOut_0_w_valid_T_1; // @[Fragmenter.scala 174:33]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      busy <= 1'h0;
    end else if (_T_2) begin
      busy <= ~ar_last;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r_addr <= 37'h0;
    end else if (_T_2) begin
      if (fixed) begin
        r_addr <= irr_bits_addr;
      end else if (irr_bits_burst == 2'h2) begin
        r_addr <= _mux_addr_T_4;
      end else begin
        r_addr <= inc_addr;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r_len <= 8'h0;
    end else begin
      r_len <= _GEN_4[7:0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      busy_1 <= 1'h0;
    end else if (_T_5) begin
      busy_1 <= ~aw_last;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r_addr_1 <= 37'h0;
    end else if (_T_5) begin
      if (fixed_1) begin
        r_addr_1 <= irr_1_bits_addr;
      end else if (irr_1_bits_burst == 2'h2) begin
        r_addr_1 <= _mux_addr_T_9;
      end else begin
        r_addr_1 <= inc_addr_1;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r_len_1 <= 8'h0;
    end else begin
      r_len_1 <= _GEN_9[7:0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      w_counter <= 9'h0;
    end else begin
      w_counter <= w_todo - _GEN_47;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wbeats_latched <= 1'h0;
    end else if (_T_7) begin
      wbeats_latched <= 1'h0;
    end else begin
      wbeats_latched <= _GEN_10;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      error_0 <= 2'h0;
    end else if (_T_22[0] & _T_28) begin
      if (auto_out_b_bits_echo_real_last) begin
        error_0 <= 2'h0;
      end else begin
        error_0 <= _error_0_T;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      error_1 <= 2'h0;
    end else if (_T_22[1] & _T_28) begin
      if (auto_out_b_bits_echo_real_last) begin
        error_1 <= 2'h0;
      end else begin
        error_1 <= _error_1_T;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      error_2 <= 2'h0;
    end else if (_T_22[2] & _T_28) begin
      if (auto_out_b_bits_echo_real_last) begin
        error_2 <= 2'h0;
      end else begin
        error_2 <= _error_2_T;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      error_3 <= 2'h0;
    end else if (_T_22[3] & _T_28) begin
      if (auto_out_b_bits_echo_real_last) begin
        error_3 <= 2'h0;
      end else begin
        error_3 <= _error_3_T;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  busy = _RAND_0[0:0];
  _RAND_1 = {2{`RANDOM}};
  r_addr = _RAND_1[36:0];
  _RAND_2 = {1{`RANDOM}};
  r_len = _RAND_2[7:0];
  _RAND_3 = {1{`RANDOM}};
  busy_1 = _RAND_3[0:0];
  _RAND_4 = {2{`RANDOM}};
  r_addr_1 = _RAND_4[36:0];
  _RAND_5 = {1{`RANDOM}};
  r_len_1 = _RAND_5[7:0];
  _RAND_6 = {1{`RANDOM}};
  w_counter = _RAND_6[8:0];
  _RAND_7 = {1{`RANDOM}};
  wbeats_latched = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  error_0 = _RAND_8[1:0];
  _RAND_9 = {1{`RANDOM}};
  error_1 = _RAND_9[1:0];
  _RAND_10 = {1{`RANDOM}};
  error_2 = _RAND_10[1:0];
  _RAND_11 = {1{`RANDOM}};
  error_3 = _RAND_11[1:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    busy = 1'h0;
  end
  if (rf_reset) begin
    r_addr = 37'h0;
  end
  if (rf_reset) begin
    r_len = 8'h0;
  end
  if (reset) begin
    busy_1 = 1'h0;
  end
  if (rf_reset) begin
    r_addr_1 = 37'h0;
  end
  if (rf_reset) begin
    r_len_1 = 8'h0;
  end
  if (reset) begin
    w_counter = 9'h0;
  end
  if (reset) begin
    wbeats_latched = 1'h0;
  end
  if (reset) begin
    error_0 = 2'h0;
  end
  if (reset) begin
    error_1 = 2'h0;
  end
  if (reset) begin
    error_2 = 2'h0;
  end
  if (reset) begin
    error_3 = 2'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
