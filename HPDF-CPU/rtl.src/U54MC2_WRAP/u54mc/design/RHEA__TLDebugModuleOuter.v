//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLDebugModuleOuter(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_dmi_in_a_ready,
  input         auto_dmi_in_a_valid,
  input  [2:0]  auto_dmi_in_a_bits_opcode,
  input  [6:0]  auto_dmi_in_a_bits_address,
  input  [31:0] auto_dmi_in_a_bits_data,
  input         auto_dmi_in_d_ready,
  output        auto_dmi_in_d_valid,
  output [2:0]  auto_dmi_in_d_bits_opcode,
  output [31:0] auto_dmi_in_d_bits_data,
  output        auto_int_out_1_0,
  output        auto_int_out_0_0,
  output        io_ctrl_ndreset,
  output        io_ctrl_dmactive,
  input         io_ctrl_dmactiveAck,
  input         io_innerCtrl_ready,
  output        io_innerCtrl_valid,
  output        io_innerCtrl_bits_resumereq,
  output [9:0]  io_innerCtrl_bits_hartsel,
  output        io_innerCtrl_bits_ackhavereset,
  output        io_innerCtrl_bits_hasel,
  output        io_innerCtrl_bits_hamask_0,
  output        io_innerCtrl_bits_hamask_1,
  output        io_innerCtrl_bits_hrmask_0,
  output        io_innerCtrl_bits_hrmask_1,
  input         io_hgDebugInt_0,
  input         io_hgDebugInt_1
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
`endif // RANDOMIZE_REG_INIT
  reg  DMCONTROLReg_haltreq; // @[Debug.scala 345:31]
  reg  DMCONTROLReg_hasel; // @[Debug.scala 345:31]
  reg [9:0] DMCONTROLReg_hartsello; // @[Debug.scala 345:31]
  reg  DMCONTROLReg_ndmreset; // @[Debug.scala 345:31]
  reg  DMCONTROLReg_dmactive; // @[Debug.scala 345:31]
  wire  _T_1 = ~DMCONTROLReg_dmactive; // @[Debug.scala 363:11]
  wire  in_bits_read = auto_dmi_in_a_bits_opcode == 3'h4; // @[Debug.scala 305:32]
  wire [2:0] in_bits_index = auto_dmi_in_a_bits_address[4:2]; // @[Debug.scala 305:32 Debug.scala 305:32]
  wire  out_iindex_hi = in_bits_index[2]; // @[Debug.scala 305:32]
  wire  out_iindex_lo = in_bits_index[1]; // @[Debug.scala 305:32]
  wire [1:0] out_iindex = {out_iindex_hi,out_iindex_lo}; // @[Cat.scala 30:58]
  wire [2:0] out_findex = in_bits_index & 3'h1; // @[Debug.scala 305:32]
  wire  _out_T_2 = out_findex == 3'h1; // @[Debug.scala 305:32]
  wire  _out_T = out_findex == 3'h0; // @[Debug.scala 305:32]
  wire  _out_wofireMux_T = auto_dmi_in_a_valid & auto_dmi_in_d_ready; // @[Debug.scala 305:32]
  wire [3:0] _out_backSel_T = 4'h1 << out_iindex; // @[OneHot.scala 58:35]
  wire  out_backSel_0 = _out_backSel_T[0]; // @[Debug.scala 305:32]
  wire  out_woready_7 = _out_wofireMux_T & ~in_bits_read & out_backSel_0 & _out_T; // @[Debug.scala 305:32]
  wire  DMCONTROLWrData_ndmreset = auto_dmi_in_a_bits_data[1]; // @[Debug.scala 305:32]
  wire [9:0] DMCONTROLWrData_hartsello = {{8'd0}, auto_dmi_in_a_bits_data[17:16]};
  wire [9:0] _DMCONTROLNxt_hartsello_T = DMCONTROLWrData_hartsello & 10'h3; // @[Debug.scala 367:104]
  wire  DMCONTROLWrData_hasel = auto_dmi_in_a_bits_data[26]; // @[Debug.scala 305:32]
  wire  DMCONTROLWrData_haltreq = auto_dmi_in_a_bits_data[31]; // @[Debug.scala 305:32]
  wire  DMCONTROLWrData_dmactive = auto_dmi_in_a_bits_data[0]; // @[Debug.scala 305:32]
  reg [31:0] HAMASKReg_maskdata; // @[Debug.scala 440:32]
  wire [31:0] HAWINDOWRdData_maskdata = HAMASKReg_maskdata & 32'h3; // @[Debug.scala 443:55]
  wire  out_backSel_2 = _out_backSel_T[2]; // @[Debug.scala 305:32]
  wire  out_woready_5 = _out_wofireMux_T & ~in_bits_read & out_backSel_2 & _out_T_2; // @[Debug.scala 305:32]
  wire [31:0] HAWINDOWWrData_maskdata = {{30'd0}, auto_dmi_in_a_bits_data[1:0]};
  wire  tempWrData_0 = HAWINDOWWrData_maskdata[0]; // @[Debug.scala 458:54]
  wire  tempMaskReg_0 = HAMASKReg_maskdata[0]; // @[Debug.scala 459:48]
  wire  hamask_0 = out_woready_5 ? tempWrData_0 : tempMaskReg_0; // @[Debug.scala 460:74 Debug.scala 461:44 Debug.scala 463:44]
  wire  tempWrData_1_1 = HAWINDOWWrData_maskdata[1]; // @[Debug.scala 458:54]
  wire  tempMaskReg_1_1 = HAMASKReg_maskdata[1]; // @[Debug.scala 459:48]
  wire  hamask_1 = out_woready_5 ? tempWrData_1_1 : tempMaskReg_1_1; // @[Debug.scala 460:74 Debug.scala 461:44 Debug.scala 463:44]
  reg  hrmaskReg_0; // @[Debug.scala 480:28]
  reg  hrmaskReg_1; // @[Debug.scala 480:28]
  wire  DMCONTROLWrData_clrresethaltreq = auto_dmi_in_a_bits_data[2]; // @[Debug.scala 305:32]
  wire  _T_25 = io_innerCtrl_bits_hasel & io_innerCtrl_bits_hamask_0; // @[Debug.scala 410:56]
  wire  _T_26 = io_innerCtrl_bits_hartsel == 10'h0 | _T_25; // @[Debug.scala 409:47]
  wire  DMCONTROLWrData_setresethaltreq = auto_dmi_in_a_bits_data[3]; // @[Debug.scala 305:32]
  wire  _GEN_32 = out_woready_7 & DMCONTROLWrData_setresethaltreq & _T_26 | hrmaskReg_0; // @[Debug.scala 488:102 Debug.scala 489:30 Debug.scala 482:15]
  wire  _GEN_33 = out_woready_7 & DMCONTROLWrData_clrresethaltreq & _T_26 ? 1'h0 : _GEN_32; // @[Debug.scala 486:102 Debug.scala 487:30]
  wire  _T_38 = io_innerCtrl_bits_hasel & io_innerCtrl_bits_hamask_1; // @[Debug.scala 410:56]
  wire  _T_39 = io_innerCtrl_bits_hartsel == 10'h1 | _T_38; // @[Debug.scala 409:47]
  wire  _GEN_35 = out_woready_7 & DMCONTROLWrData_setresethaltreq & _T_39 | hrmaskReg_1; // @[Debug.scala 488:102 Debug.scala 489:30 Debug.scala 482:15]
  wire  _GEN_36 = out_woready_7 & DMCONTROLWrData_clrresethaltreq & _T_39 ? 1'h0 : _GEN_35; // @[Debug.scala 486:102 Debug.scala 487:30]
  wire  out_prepend_lo_4 = DMCONTROLReg_dmactive & io_ctrl_dmactiveAck; // @[Debug.scala 496:43]
  wire [4:0] out_prepend_7 = {1'h0,1'h0,1'h0,DMCONTROLReg_ndmreset,out_prepend_lo_4}; // @[Cat.scala 30:58]
  wire [15:0] out_prepend_lo_8 = {{11'd0}, out_prepend_7}; // @[Debug.scala 305:32]
  wire [25:0] out_prepend_8 = {DMCONTROLReg_hartsello,out_prepend_lo_8}; // @[Cat.scala 30:58]
  wire [17:0] out_prepend_lo_9 = out_prepend_8[17:0]; // @[Debug.scala 305:32]
  wire [18:0] out_prepend_9 = {1'h0,out_prepend_lo_9}; // @[Cat.scala 30:58]
  wire [25:0] out_prepend_lo_10 = {{7'd0}, out_prepend_9}; // @[Debug.scala 305:32]
  wire  DMCONTROLWrData_ackhavereset = auto_dmi_in_a_bits_data[28]; // @[Debug.scala 305:32]
  wire  DMCONTROLWrData_resumereq = auto_dmi_in_a_bits_data[30]; // @[Debug.scala 305:32]
  wire [31:0] out_prepend_15 = {DMCONTROLReg_haltreq,1'h0,1'h0,1'h0,1'h0,DMCONTROLReg_hasel,out_prepend_lo_10}; // @[Cat.scala 30:58]
  wire  _GEN_55 = 2'h1 == out_iindex ? _out_T : _out_T; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_56 = 2'h2 == out_iindex ? _out_T_2 : _GEN_55; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_57 = 2'h3 == out_iindex | _GEN_56; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire [31:0] _GEN_59 = 2'h1 == out_iindex ? 32'h112380 : out_prepend_15; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire [31:0] _out_out_bits_data_WIRE_1_2 = {{30'd0}, HAWINDOWRdData_maskdata[1:0]}; // @[MuxLiteral.scala 48:48 MuxLiteral.scala 48:48]
  wire [31:0] _GEN_60 = 2'h2 == out_iindex ? _out_out_bits_data_WIRE_1_2 : _GEN_59; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire [31:0] _GEN_61 = 2'h3 == out_iindex ? 32'h0 : _GEN_60; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  reg  debugIntRegs_0; // @[Debug.scala 561:31]
  reg  debugIntRegs_1; // @[Debug.scala 561:31]
  wire  _T_50 = DMCONTROLWrData_hartsello == 10'h0; // @[Debug.scala 580:58]
  wire  _T_52 = _T_50 | DMCONTROLWrData_hasel & hamask_0; // @[Debug.scala 581:11]
  wire  _T_53 = out_woready_7 & _T_52; // @[Debug.scala 580:27]
  wire  _T_57 = DMCONTROLWrData_hartsello == 10'h1; // @[Debug.scala 580:58]
  wire  _T_59 = _T_57 | DMCONTROLWrData_hasel & hamask_1; // @[Debug.scala 581:11]
  wire  _T_60 = out_woready_7 & _T_59; // @[Debug.scala 580:27]
  reg  innerCtrlValidReg; // @[Debug.scala 592:36]
  reg  innerCtrlResumeReqReg; // @[Debug.scala 593:40]
  reg  innerCtrlAckHaveResetReg; // @[Debug.scala 594:43]
  wire  innerCtrlValid = out_woready_7 | out_woready_5; // @[Debug.scala 596:128]
  wire  _innerCtrlValidReg_T = ~io_innerCtrl_ready; // @[Debug.scala 599:54]
  assign auto_dmi_in_a_ready = auto_dmi_in_d_ready; // @[Debug.scala 305:32]
  assign auto_dmi_in_d_valid = auto_dmi_in_a_valid; // @[Debug.scala 305:32]
  assign auto_dmi_in_d_bits_opcode = {{2'd0}, in_bits_read}; // @[Debug.scala 305:32 Debug.scala 305:32]
  assign auto_dmi_in_d_bits_data = _GEN_57 ? _GEN_61 : 32'h0; // @[Debug.scala 305:32]
  assign auto_int_out_1_0 = debugIntRegs_1 | io_hgDebugInt_1; // @[Debug.scala 567:80]
  assign auto_int_out_0_0 = debugIntRegs_0 | io_hgDebugInt_0; // @[Debug.scala 567:80]
  assign io_ctrl_ndreset = DMCONTROLReg_ndmreset; // @[Debug.scala 613:21]
  assign io_ctrl_dmactive = DMCONTROLReg_dmactive; // @[Debug.scala 614:22]
  assign io_innerCtrl_valid = innerCtrlValid | innerCtrlValidReg; // @[Debug.scala 603:54]
  assign io_innerCtrl_bits_resumereq = out_woready_7 & DMCONTROLWrData_resumereq | innerCtrlResumeReqReg; // @[Debug.scala 605:83]
  assign io_innerCtrl_bits_hartsel = out_woready_7 ? DMCONTROLWrData_hartsello : DMCONTROLReg_hartsello; // @[Debug.scala 604:42]
  assign io_innerCtrl_bits_ackhavereset = out_woready_7 & DMCONTROLWrData_ackhavereset | innerCtrlAckHaveResetReg; // @[Debug.scala 606:89]
  assign io_innerCtrl_bits_hasel = out_woready_7 ? DMCONTROLWrData_hasel : DMCONTROLReg_hasel; // @[Debug.scala 609:42]
  assign io_innerCtrl_bits_hamask_0 = out_woready_5 ? tempWrData_0 : tempMaskReg_0; // @[Debug.scala 460:74 Debug.scala 461:44 Debug.scala 463:44]
  assign io_innerCtrl_bits_hamask_1 = out_woready_5 ? tempWrData_1_1 : tempMaskReg_1_1; // @[Debug.scala 460:74 Debug.scala 461:44 Debug.scala 463:44]
  assign io_innerCtrl_bits_hrmask_0 = _T_1 ? 1'h0 : _GEN_33; // @[Debug.scala 484:44 Debug.scala 485:30]
  assign io_innerCtrl_bits_hrmask_1 = _T_1 ? 1'h0 : _GEN_36; // @[Debug.scala 484:44 Debug.scala 485:30]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      DMCONTROLReg_haltreq <= 1'h0;
    end else if (~DMCONTROLReg_dmactive) begin
      DMCONTROLReg_haltreq <= 1'h0;
    end else if (out_woready_7) begin
      DMCONTROLReg_haltreq <= DMCONTROLWrData_haltreq;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      DMCONTROLReg_hasel <= 1'h0;
    end else if (~DMCONTROLReg_dmactive) begin
      DMCONTROLReg_hasel <= 1'h0;
    end else if (out_woready_7) begin
      DMCONTROLReg_hasel <= DMCONTROLWrData_hasel;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      DMCONTROLReg_hartsello <= 10'h0;
    end else if (~DMCONTROLReg_dmactive) begin
      DMCONTROLReg_hartsello <= 10'h0;
    end else if (out_woready_7) begin
      DMCONTROLReg_hartsello <= _DMCONTROLNxt_hartsello_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      DMCONTROLReg_ndmreset <= 1'h0;
    end else if (~DMCONTROLReg_dmactive) begin
      DMCONTROLReg_ndmreset <= 1'h0;
    end else if (out_woready_7) begin
      DMCONTROLReg_ndmreset <= DMCONTROLWrData_ndmreset;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      DMCONTROLReg_dmactive <= 1'h0;
    end else if (out_woready_7) begin
      DMCONTROLReg_dmactive <= DMCONTROLWrData_dmactive;
    end else if (~DMCONTROLReg_dmactive) begin
      DMCONTROLReg_dmactive <= 1'h0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      HAMASKReg_maskdata <= 32'h0;
    end else if (_T_1) begin
      HAMASKReg_maskdata <= 32'h0;
    end else if (out_woready_5) begin
      HAMASKReg_maskdata <= HAWINDOWWrData_maskdata;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      hrmaskReg_0 <= 1'h0;
    end else if (_T_1) begin
      hrmaskReg_0 <= 1'h0;
    end else if (out_woready_7 & DMCONTROLWrData_clrresethaltreq & _T_26) begin
      hrmaskReg_0 <= 1'h0;
    end else begin
      hrmaskReg_0 <= _GEN_32;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      hrmaskReg_1 <= 1'h0;
    end else if (_T_1) begin
      hrmaskReg_1 <= 1'h0;
    end else if (out_woready_7 & DMCONTROLWrData_clrresethaltreq & _T_39) begin
      hrmaskReg_1 <= 1'h0;
    end else begin
      hrmaskReg_1 <= _GEN_35;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      debugIntRegs_0 <= 1'h0;
    end else if (_T_1) begin
      debugIntRegs_0 <= 1'h0;
    end else if (_T_53) begin
      debugIntRegs_0 <= DMCONTROLWrData_haltreq;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      debugIntRegs_1 <= 1'h0;
    end else if (_T_1) begin
      debugIntRegs_1 <= 1'h0;
    end else if (_T_60) begin
      debugIntRegs_1 <= DMCONTROLWrData_haltreq;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      innerCtrlValidReg <= 1'h0;
    end else begin
      innerCtrlValidReg <= io_innerCtrl_valid & ~io_innerCtrl_ready;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      innerCtrlResumeReqReg <= 1'h0;
    end else begin
      innerCtrlResumeReqReg <= io_innerCtrl_bits_resumereq & _innerCtrlValidReg_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      innerCtrlAckHaveResetReg <= 1'h0;
    end else begin
      innerCtrlAckHaveResetReg <= io_innerCtrl_bits_ackhavereset & _innerCtrlValidReg_T;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  DMCONTROLReg_haltreq = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  DMCONTROLReg_hasel = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  DMCONTROLReg_hartsello = _RAND_2[9:0];
  _RAND_3 = {1{`RANDOM}};
  DMCONTROLReg_ndmreset = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  DMCONTROLReg_dmactive = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  HAMASKReg_maskdata = _RAND_5[31:0];
  _RAND_6 = {1{`RANDOM}};
  hrmaskReg_0 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  hrmaskReg_1 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  debugIntRegs_0 = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  debugIntRegs_1 = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  innerCtrlValidReg = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  innerCtrlResumeReqReg = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  innerCtrlAckHaveResetReg = _RAND_12[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    DMCONTROLReg_haltreq = 1'h0;
  end
  if (reset) begin
    DMCONTROLReg_hasel = 1'h0;
  end
  if (reset) begin
    DMCONTROLReg_hartsello = 10'h0;
  end
  if (reset) begin
    DMCONTROLReg_ndmreset = 1'h0;
  end
  if (reset) begin
    DMCONTROLReg_dmactive = 1'h0;
  end
  if (reset) begin
    HAMASKReg_maskdata = 32'h0;
  end
  if (reset) begin
    hrmaskReg_0 = 1'h0;
  end
  if (reset) begin
    hrmaskReg_1 = 1'h0;
  end
  if (reset) begin
    debugIntRegs_0 = 1'h0;
  end
  if (reset) begin
    debugIntRegs_1 = 1'h0;
  end
  if (reset) begin
    innerCtrlValidReg = 1'h0;
  end
  if (reset) begin
    innerCtrlResumeReqReg = 1'h0;
  end
  if (reset) begin
    innerCtrlAckHaveResetReg = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
