//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__ShiftQueue_4(
  input        rf_reset,
  input        clock,
  input        reset,
  output       io_enq_ready,
  input        io_enq_valid,
  input  [4:0] io_enq_bits_sink,
  input        io_deq_ready,
  output       io_deq_valid,
  output [4:0] io_deq_bits_sink
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
`endif // RANDOMIZE_REG_INIT
  reg  valid_0; // @[ShiftQueue.scala 21:30]
  reg  valid_1; // @[ShiftQueue.scala 21:30]
  reg [4:0] elts_0_sink; // @[ShiftQueue.scala 22:25]
  reg [4:0] elts_1_sink; // @[ShiftQueue.scala 22:25]
  wire  _wen_T = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  wire  _wen_T_3 = valid_1 | _wen_T; // @[ShiftQueue.scala 30:28]
  wire  _wen_T_7 = _wen_T & ~valid_0; // @[ShiftQueue.scala 31:45]
  wire  wen = io_deq_ready ? _wen_T_3 : _wen_T_7; // @[ShiftQueue.scala 29:10]
  wire  _valid_0_T_6 = _wen_T | valid_0; // @[ShiftQueue.scala 37:45]
  wire  _wen_T_10 = _wen_T & valid_1; // @[ShiftQueue.scala 30:45]
  wire  _wen_T_13 = _wen_T & valid_0; // @[ShiftQueue.scala 31:25]
  wire  _wen_T_15 = _wen_T & valid_0 & ~valid_1; // @[ShiftQueue.scala 31:45]
  wire  wen_1 = io_deq_ready ? _wen_T_10 : _wen_T_15; // @[ShiftQueue.scala 29:10]
  wire  _valid_1_T_6 = _wen_T_13 | valid_1; // @[ShiftQueue.scala 37:45]
  assign io_enq_ready = ~valid_1; // @[ShiftQueue.scala 40:19]
  assign io_deq_valid = valid_0; // @[ShiftQueue.scala 41:16]
  assign io_deq_bits_sink = elts_0_sink; // @[ShiftQueue.scala 42:15]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid_0 <= 1'h0;
    end else if (io_deq_ready) begin
      valid_0 <= _wen_T_3;
    end else begin
      valid_0 <= _valid_0_T_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid_1 <= 1'h0;
    end else if (io_deq_ready) begin
      valid_1 <= _wen_T_10;
    end else begin
      valid_1 <= _valid_1_T_6;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_0_sink <= 5'h0;
    end else if (wen) begin
      if (valid_1) begin
        elts_0_sink <= elts_1_sink;
      end else begin
        elts_0_sink <= io_enq_bits_sink;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_1_sink <= 5'h0;
    end else if (wen_1) begin
      elts_1_sink <= io_enq_bits_sink;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  valid_0 = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  valid_1 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  elts_0_sink = _RAND_2[4:0];
  _RAND_3 = {1{`RANDOM}};
  elts_1_sink = _RAND_3[4:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    valid_0 = 1'h0;
  end
  if (reset) begin
    valid_1 = 1'h0;
  end
  if (rf_reset) begin
    elts_0_sink = 5'h0;
  end
  if (rf_reset) begin
    elts_1_sink = 5'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
