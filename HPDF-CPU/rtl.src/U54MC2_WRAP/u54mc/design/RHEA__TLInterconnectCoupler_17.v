//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLInterconnectCoupler_17(
  input         rf_reset,
  input         clock,
  input         reset,
  input         auto_bridge_for_axi4_periph_port_axi4_out_aw_ready,
  output        auto_bridge_for_axi4_periph_port_axi4_out_aw_valid,
  output [31:0] auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_addr,
  output [7:0]  auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_len,
  output [2:0]  auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_size,
  output [3:0]  auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_cache,
  output [2:0]  auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_prot,
  input         auto_bridge_for_axi4_periph_port_axi4_out_w_ready,
  output        auto_bridge_for_axi4_periph_port_axi4_out_w_valid,
  output [63:0] auto_bridge_for_axi4_periph_port_axi4_out_w_bits_data,
  output [7:0]  auto_bridge_for_axi4_periph_port_axi4_out_w_bits_strb,
  output        auto_bridge_for_axi4_periph_port_axi4_out_w_bits_last,
  output        auto_bridge_for_axi4_periph_port_axi4_out_b_ready,
  input         auto_bridge_for_axi4_periph_port_axi4_out_b_valid,
  input  [1:0]  auto_bridge_for_axi4_periph_port_axi4_out_b_bits_resp,
  input         auto_bridge_for_axi4_periph_port_axi4_out_ar_ready,
  output        auto_bridge_for_axi4_periph_port_axi4_out_ar_valid,
  output [31:0] auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_addr,
  output [7:0]  auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_len,
  output [2:0]  auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_size,
  output [3:0]  auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_cache,
  output [2:0]  auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_prot,
  output        auto_bridge_for_axi4_periph_port_axi4_out_r_ready,
  input         auto_bridge_for_axi4_periph_port_axi4_out_r_valid,
  input  [63:0] auto_bridge_for_axi4_periph_port_axi4_out_r_bits_data,
  input  [1:0]  auto_bridge_for_axi4_periph_port_axi4_out_r_bits_resp,
  input         auto_bridge_for_axi4_periph_port_axi4_out_r_bits_last,
  output        auto_tl_in_a_ready,
  input         auto_tl_in_a_valid,
  input  [2:0]  auto_tl_in_a_bits_opcode,
  input  [2:0]  auto_tl_in_a_bits_param,
  input  [2:0]  auto_tl_in_a_bits_size,
  input  [6:0]  auto_tl_in_a_bits_source,
  input  [31:0] auto_tl_in_a_bits_address,
  input         auto_tl_in_a_bits_user_amba_prot_bufferable,
  input         auto_tl_in_a_bits_user_amba_prot_modifiable,
  input         auto_tl_in_a_bits_user_amba_prot_readalloc,
  input         auto_tl_in_a_bits_user_amba_prot_writealloc,
  input         auto_tl_in_a_bits_user_amba_prot_privileged,
  input         auto_tl_in_a_bits_user_amba_prot_secure,
  input         auto_tl_in_a_bits_user_amba_prot_fetch,
  input  [7:0]  auto_tl_in_a_bits_mask,
  input  [63:0] auto_tl_in_a_bits_data,
  input         auto_tl_in_a_bits_corrupt,
  input         auto_tl_in_d_ready,
  output        auto_tl_in_d_valid,
  output [2:0]  auto_tl_in_d_bits_opcode,
  output [2:0]  auto_tl_in_d_bits_size,
  output [6:0]  auto_tl_in_d_bits_source,
  output        auto_tl_in_d_bits_denied,
  output [63:0] auto_tl_in_d_bits_data,
  output        auto_tl_in_d_bits_corrupt
);
  wire  bridge_for_axi4_periph_port_rf_reset; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_clock; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_reset; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_axi4_out_aw_ready; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_axi4_out_aw_valid; // @[LazyModule.scala 524:27]
  wire [31:0] bridge_for_axi4_periph_port_auto_axi4_out_aw_bits_addr; // @[LazyModule.scala 524:27]
  wire [7:0] bridge_for_axi4_periph_port_auto_axi4_out_aw_bits_len; // @[LazyModule.scala 524:27]
  wire [2:0] bridge_for_axi4_periph_port_auto_axi4_out_aw_bits_size; // @[LazyModule.scala 524:27]
  wire [3:0] bridge_for_axi4_periph_port_auto_axi4_out_aw_bits_cache; // @[LazyModule.scala 524:27]
  wire [2:0] bridge_for_axi4_periph_port_auto_axi4_out_aw_bits_prot; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_axi4_out_w_ready; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_axi4_out_w_valid; // @[LazyModule.scala 524:27]
  wire [63:0] bridge_for_axi4_periph_port_auto_axi4_out_w_bits_data; // @[LazyModule.scala 524:27]
  wire [7:0] bridge_for_axi4_periph_port_auto_axi4_out_w_bits_strb; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_axi4_out_w_bits_last; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_axi4_out_b_ready; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_axi4_out_b_valid; // @[LazyModule.scala 524:27]
  wire [1:0] bridge_for_axi4_periph_port_auto_axi4_out_b_bits_resp; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_axi4_out_ar_ready; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_axi4_out_ar_valid; // @[LazyModule.scala 524:27]
  wire [31:0] bridge_for_axi4_periph_port_auto_axi4_out_ar_bits_addr; // @[LazyModule.scala 524:27]
  wire [7:0] bridge_for_axi4_periph_port_auto_axi4_out_ar_bits_len; // @[LazyModule.scala 524:27]
  wire [2:0] bridge_for_axi4_periph_port_auto_axi4_out_ar_bits_size; // @[LazyModule.scala 524:27]
  wire [3:0] bridge_for_axi4_periph_port_auto_axi4_out_ar_bits_cache; // @[LazyModule.scala 524:27]
  wire [2:0] bridge_for_axi4_periph_port_auto_axi4_out_ar_bits_prot; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_axi4_out_r_ready; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_axi4_out_r_valid; // @[LazyModule.scala 524:27]
  wire [63:0] bridge_for_axi4_periph_port_auto_axi4_out_r_bits_data; // @[LazyModule.scala 524:27]
  wire [1:0] bridge_for_axi4_periph_port_auto_axi4_out_r_bits_resp; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_axi4_out_r_bits_last; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_tl_in_a_ready; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_tl_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] bridge_for_axi4_periph_port_auto_tl_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] bridge_for_axi4_periph_port_auto_tl_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [2:0] bridge_for_axi4_periph_port_auto_tl_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] bridge_for_axi4_periph_port_auto_tl_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [31:0] bridge_for_axi4_periph_port_auto_tl_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_secure; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 524:27]
  wire [7:0] bridge_for_axi4_periph_port_auto_tl_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] bridge_for_axi4_periph_port_auto_tl_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_tl_in_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_tl_in_d_ready; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_tl_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] bridge_for_axi4_periph_port_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] bridge_for_axi4_periph_port_auto_tl_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] bridge_for_axi4_periph_port_auto_tl_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_tl_in_d_bits_denied; // @[LazyModule.scala 524:27]
  wire [63:0] bridge_for_axi4_periph_port_auto_tl_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  bridge_for_axi4_periph_port_auto_tl_in_d_bits_corrupt; // @[LazyModule.scala 524:27]
  RHEA__TLToAXI4BridgeContents_1 bridge_for_axi4_periph_port ( // @[LazyModule.scala 524:27]
    .rf_reset(bridge_for_axi4_periph_port_rf_reset),
    .clock(bridge_for_axi4_periph_port_clock),
    .reset(bridge_for_axi4_periph_port_reset),
    .auto_axi4_out_aw_ready(bridge_for_axi4_periph_port_auto_axi4_out_aw_ready),
    .auto_axi4_out_aw_valid(bridge_for_axi4_periph_port_auto_axi4_out_aw_valid),
    .auto_axi4_out_aw_bits_addr(bridge_for_axi4_periph_port_auto_axi4_out_aw_bits_addr),
    .auto_axi4_out_aw_bits_len(bridge_for_axi4_periph_port_auto_axi4_out_aw_bits_len),
    .auto_axi4_out_aw_bits_size(bridge_for_axi4_periph_port_auto_axi4_out_aw_bits_size),
    .auto_axi4_out_aw_bits_cache(bridge_for_axi4_periph_port_auto_axi4_out_aw_bits_cache),
    .auto_axi4_out_aw_bits_prot(bridge_for_axi4_periph_port_auto_axi4_out_aw_bits_prot),
    .auto_axi4_out_w_ready(bridge_for_axi4_periph_port_auto_axi4_out_w_ready),
    .auto_axi4_out_w_valid(bridge_for_axi4_periph_port_auto_axi4_out_w_valid),
    .auto_axi4_out_w_bits_data(bridge_for_axi4_periph_port_auto_axi4_out_w_bits_data),
    .auto_axi4_out_w_bits_strb(bridge_for_axi4_periph_port_auto_axi4_out_w_bits_strb),
    .auto_axi4_out_w_bits_last(bridge_for_axi4_periph_port_auto_axi4_out_w_bits_last),
    .auto_axi4_out_b_ready(bridge_for_axi4_periph_port_auto_axi4_out_b_ready),
    .auto_axi4_out_b_valid(bridge_for_axi4_periph_port_auto_axi4_out_b_valid),
    .auto_axi4_out_b_bits_resp(bridge_for_axi4_periph_port_auto_axi4_out_b_bits_resp),
    .auto_axi4_out_ar_ready(bridge_for_axi4_periph_port_auto_axi4_out_ar_ready),
    .auto_axi4_out_ar_valid(bridge_for_axi4_periph_port_auto_axi4_out_ar_valid),
    .auto_axi4_out_ar_bits_addr(bridge_for_axi4_periph_port_auto_axi4_out_ar_bits_addr),
    .auto_axi4_out_ar_bits_len(bridge_for_axi4_periph_port_auto_axi4_out_ar_bits_len),
    .auto_axi4_out_ar_bits_size(bridge_for_axi4_periph_port_auto_axi4_out_ar_bits_size),
    .auto_axi4_out_ar_bits_cache(bridge_for_axi4_periph_port_auto_axi4_out_ar_bits_cache),
    .auto_axi4_out_ar_bits_prot(bridge_for_axi4_periph_port_auto_axi4_out_ar_bits_prot),
    .auto_axi4_out_r_ready(bridge_for_axi4_periph_port_auto_axi4_out_r_ready),
    .auto_axi4_out_r_valid(bridge_for_axi4_periph_port_auto_axi4_out_r_valid),
    .auto_axi4_out_r_bits_data(bridge_for_axi4_periph_port_auto_axi4_out_r_bits_data),
    .auto_axi4_out_r_bits_resp(bridge_for_axi4_periph_port_auto_axi4_out_r_bits_resp),
    .auto_axi4_out_r_bits_last(bridge_for_axi4_periph_port_auto_axi4_out_r_bits_last),
    .auto_tl_in_a_ready(bridge_for_axi4_periph_port_auto_tl_in_a_ready),
    .auto_tl_in_a_valid(bridge_for_axi4_periph_port_auto_tl_in_a_valid),
    .auto_tl_in_a_bits_opcode(bridge_for_axi4_periph_port_auto_tl_in_a_bits_opcode),
    .auto_tl_in_a_bits_param(bridge_for_axi4_periph_port_auto_tl_in_a_bits_param),
    .auto_tl_in_a_bits_size(bridge_for_axi4_periph_port_auto_tl_in_a_bits_size),
    .auto_tl_in_a_bits_source(bridge_for_axi4_periph_port_auto_tl_in_a_bits_source),
    .auto_tl_in_a_bits_address(bridge_for_axi4_periph_port_auto_tl_in_a_bits_address),
    .auto_tl_in_a_bits_user_amba_prot_bufferable(bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_bufferable
      ),
    .auto_tl_in_a_bits_user_amba_prot_modifiable(bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_modifiable
      ),
    .auto_tl_in_a_bits_user_amba_prot_readalloc(bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_readalloc),
    .auto_tl_in_a_bits_user_amba_prot_writealloc(bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_writealloc
      ),
    .auto_tl_in_a_bits_user_amba_prot_privileged(bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_privileged
      ),
    .auto_tl_in_a_bits_user_amba_prot_secure(bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_secure),
    .auto_tl_in_a_bits_user_amba_prot_fetch(bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_fetch),
    .auto_tl_in_a_bits_mask(bridge_for_axi4_periph_port_auto_tl_in_a_bits_mask),
    .auto_tl_in_a_bits_data(bridge_for_axi4_periph_port_auto_tl_in_a_bits_data),
    .auto_tl_in_a_bits_corrupt(bridge_for_axi4_periph_port_auto_tl_in_a_bits_corrupt),
    .auto_tl_in_d_ready(bridge_for_axi4_periph_port_auto_tl_in_d_ready),
    .auto_tl_in_d_valid(bridge_for_axi4_periph_port_auto_tl_in_d_valid),
    .auto_tl_in_d_bits_opcode(bridge_for_axi4_periph_port_auto_tl_in_d_bits_opcode),
    .auto_tl_in_d_bits_size(bridge_for_axi4_periph_port_auto_tl_in_d_bits_size),
    .auto_tl_in_d_bits_source(bridge_for_axi4_periph_port_auto_tl_in_d_bits_source),
    .auto_tl_in_d_bits_denied(bridge_for_axi4_periph_port_auto_tl_in_d_bits_denied),
    .auto_tl_in_d_bits_data(bridge_for_axi4_periph_port_auto_tl_in_d_bits_data),
    .auto_tl_in_d_bits_corrupt(bridge_for_axi4_periph_port_auto_tl_in_d_bits_corrupt)
  );
  assign bridge_for_axi4_periph_port_rf_reset = rf_reset;
  assign auto_bridge_for_axi4_periph_port_axi4_out_aw_valid = bridge_for_axi4_periph_port_auto_axi4_out_aw_valid; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_addr = bridge_for_axi4_periph_port_auto_axi4_out_aw_bits_addr
    ; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_len = bridge_for_axi4_periph_port_auto_axi4_out_aw_bits_len; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_size = bridge_for_axi4_periph_port_auto_axi4_out_aw_bits_size
    ; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_cache =
    bridge_for_axi4_periph_port_auto_axi4_out_aw_bits_cache; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_prot = bridge_for_axi4_periph_port_auto_axi4_out_aw_bits_prot
    ; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_w_valid = bridge_for_axi4_periph_port_auto_axi4_out_w_valid; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_w_bits_data = bridge_for_axi4_periph_port_auto_axi4_out_w_bits_data; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_w_bits_strb = bridge_for_axi4_periph_port_auto_axi4_out_w_bits_strb; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_w_bits_last = bridge_for_axi4_periph_port_auto_axi4_out_w_bits_last; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_b_ready = bridge_for_axi4_periph_port_auto_axi4_out_b_ready; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_ar_valid = bridge_for_axi4_periph_port_auto_axi4_out_ar_valid; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_addr = bridge_for_axi4_periph_port_auto_axi4_out_ar_bits_addr
    ; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_len = bridge_for_axi4_periph_port_auto_axi4_out_ar_bits_len; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_size = bridge_for_axi4_periph_port_auto_axi4_out_ar_bits_size
    ; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_cache =
    bridge_for_axi4_periph_port_auto_axi4_out_ar_bits_cache; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_prot = bridge_for_axi4_periph_port_auto_axi4_out_ar_bits_prot
    ; // @[LazyModule.scala 390:12]
  assign auto_bridge_for_axi4_periph_port_axi4_out_r_ready = bridge_for_axi4_periph_port_auto_axi4_out_r_ready; // @[LazyModule.scala 390:12]
  assign auto_tl_in_a_ready = bridge_for_axi4_periph_port_auto_tl_in_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_valid = bridge_for_axi4_periph_port_auto_tl_in_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_opcode = bridge_for_axi4_periph_port_auto_tl_in_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_size = bridge_for_axi4_periph_port_auto_tl_in_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_source = bridge_for_axi4_periph_port_auto_tl_in_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_denied = bridge_for_axi4_periph_port_auto_tl_in_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_data = bridge_for_axi4_periph_port_auto_tl_in_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_corrupt = bridge_for_axi4_periph_port_auto_tl_in_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign bridge_for_axi4_periph_port_clock = clock;
  assign bridge_for_axi4_periph_port_reset = reset;
  assign bridge_for_axi4_periph_port_auto_axi4_out_aw_ready = auto_bridge_for_axi4_periph_port_axi4_out_aw_ready; // @[LazyModule.scala 390:12]
  assign bridge_for_axi4_periph_port_auto_axi4_out_w_ready = auto_bridge_for_axi4_periph_port_axi4_out_w_ready; // @[LazyModule.scala 390:12]
  assign bridge_for_axi4_periph_port_auto_axi4_out_b_valid = auto_bridge_for_axi4_periph_port_axi4_out_b_valid; // @[LazyModule.scala 390:12]
  assign bridge_for_axi4_periph_port_auto_axi4_out_b_bits_resp = auto_bridge_for_axi4_periph_port_axi4_out_b_bits_resp; // @[LazyModule.scala 390:12]
  assign bridge_for_axi4_periph_port_auto_axi4_out_ar_ready = auto_bridge_for_axi4_periph_port_axi4_out_ar_ready; // @[LazyModule.scala 390:12]
  assign bridge_for_axi4_periph_port_auto_axi4_out_r_valid = auto_bridge_for_axi4_periph_port_axi4_out_r_valid; // @[LazyModule.scala 390:12]
  assign bridge_for_axi4_periph_port_auto_axi4_out_r_bits_data = auto_bridge_for_axi4_periph_port_axi4_out_r_bits_data; // @[LazyModule.scala 390:12]
  assign bridge_for_axi4_periph_port_auto_axi4_out_r_bits_resp = auto_bridge_for_axi4_periph_port_axi4_out_r_bits_resp; // @[LazyModule.scala 390:12]
  assign bridge_for_axi4_periph_port_auto_axi4_out_r_bits_last = auto_bridge_for_axi4_periph_port_axi4_out_r_bits_last; // @[LazyModule.scala 390:12]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_valid = auto_tl_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_bits_opcode = auto_tl_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_bits_param = auto_tl_in_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_bits_size = auto_tl_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_bits_source = auto_tl_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_bits_address = auto_tl_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_bufferable =
    auto_tl_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_modifiable =
    auto_tl_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_readalloc =
    auto_tl_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_writealloc =
    auto_tl_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_privileged =
    auto_tl_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_secure = auto_tl_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_fetch = auto_tl_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_bits_mask = auto_tl_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_bits_data = auto_tl_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_a_bits_corrupt = auto_tl_in_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign bridge_for_axi4_periph_port_auto_tl_in_d_ready = auto_tl_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
endmodule
