/* ---------------------------------------------------------------------
**
// ------------------------------------------------------------------------------
// 
// Copyright 2006 - 2020 Synopsys, INC.
// 
// This Synopsys IP and all associated documentation are proprietary to
// Synopsys, Inc. and may only be used pursuant to the terms and conditions of a
// written license agreement with Synopsys, Inc. All other use, reproduction,
// modification, or distribution of the Synopsys IP or the associated
// documentation is strictly prohibited.
// 
// Component Name   : DW_axi_x2x
// Component Version: 1.08a
// Release Type     : GA
// ------------------------------------------------------------------------------

// 
// Release version :  1.08a
// File Version     :        $Revision: #1 $ 
// Revision: $Id: //dwh/DW_ocb/DW_axi_x2x/amba_dev/src/DW_axi_x2x_constants.vh#1 $ 
**
** ---------------------------------------------------------------------
**
** File     : DW_axi_x2x_constants.vh
** Abstract : Some static macro's for DW_axi_x2x.
**
** ---------------------------------------------------------------------
*/

//==============================================================================
// Start Guard: prevent re-compilation of includes
//==============================================================================
`define axi2axi_cpu_pbus___GUARD__DW_AXI_X2X_CONSTANTS__VH__

// Max data width
`define axi2axi_cpu_pbus_X2X_MAX_DW 512

// Max strobe width
`define axi2axi_cpu_pbus_X2X_MAX_SW  64

// Log 2 max strobe width

`define axi2axi_cpu_pbus_X2X_LOG2_MAX_SW 6

// Burst Size Width
`define axi2axi_cpu_pbus_X2X_BSW 3
// Burst Type Width
`define axi2axi_cpu_pbus_X2X_BTW 2
// Locked Type Width
`define axi2axi_cpu_pbus_X2X_LTW 2 
// Cache Type Width
`define axi2axi_cpu_pbus_X2X_CTW 4
// Protection Type Width
`define axi2axi_cpu_pbus_X2X_PTW 3
// Buffered Response Width
`define axi2axi_cpu_pbus_X2X_BRW 2
// Read Response Width
`define axi2axi_cpu_pbus_X2X_RRW 2

// Locked type field macros.
`define axi2axi_cpu_pbus_X2X_LT_NORM  2'b00
`define axi2axi_cpu_pbus_X2X_LT_EX    2'b01
`define axi2axi_cpu_pbus_X2X_LT_LOCK  2'b10

// Protection type field macros.
`define axi2axi_cpu_pbus_X2X_PT_PRVLGD    3'bxx1
`define axi2axi_cpu_pbus_X2X_PT_NORM      3'bxx0
`define axi2axi_cpu_pbus_X2X_PT_SECURE    3'bx1x
`define axi2axi_cpu_pbus_X2X_PT_NSECURE   3'bx0x
`define axi2axi_cpu_pbus_X2X_PT_INSTRUCT  3'b1xx
`define axi2axi_cpu_pbus_X2X_PT_DATA      3'b0xx

`define axi2axi_cpu_pbus_X2X_PT_PRVLGD_BIT   0
`define axi2axi_cpu_pbus_X2X_PT_SECURE_BIT   1
`define axi2axi_cpu_pbus_X2X_PT_INSTRUCT_BIT 2

// Encoding definition of RESP signals.
`define axi2axi_cpu_pbus_X2X_RESP_OKAY     2'b00
`define axi2axi_cpu_pbus_X2X_RESP_EXOKAY   2'b01
`define axi2axi_cpu_pbus_X2X_RESP_SLVERR   2'b10
`define axi2axi_cpu_pbus_X2X_RESP_DECERR   2'b11

// Burst type macros.
`define axi2axi_cpu_pbus_X2X_BT_FIXED      2'b00
`define axi2axi_cpu_pbus_X2X_BT_INCR       2'b01
`define axi2axi_cpu_pbus_X2X_BT_WRAP       2'b10
`define axi2axi_cpu_pbus_X2X_BT_X          2'bxx


// Macros used as parameter inputs to blocks,
`define axi2axi_cpu_pbus_X2X_NOREQ_LOCKING  0 // No locking functionality required.
`define axi2axi_cpu_pbus_X2X_REQ_LOCKING    1 // Locking functionality required.

// Some blocks need to implement different logic depending
// on what type of channel they are implementing, these macros
// are used for that purpose.
`define axi2axi_cpu_pbus_X2X_W_CH 1      // This channel is a write data channel.
`define axi2axi_cpu_pbus_X2X_NOT_W_CH  0 // This channel is not a write data channel.

`define axi2axi_cpu_pbus_X2X_AW_CH 1      // This channel is a write address channel.
`define axi2axi_cpu_pbus_X2X_NOT_AW_CH  0 // This channel is not a write address channel.

`define axi2axi_cpu_pbus_X2X_R_CH 1      // This channel is a read data channel.
`define axi2axi_cpu_pbus_X2X_NOT_R_CH  0 // This channel is not a read data channel.

`define axi2axi_cpu_pbus_X2X_ADDR_CH 1     // This channel is an address channel.
`define axi2axi_cpu_pbus_X2X_NOT_ADDR_CH 0 // This channel is not an address channel.

// Macros to define the various levels of t/x altering the X2X is
// required to do.
`define axi2axi_cpu_pbus_X2X_TX_NO_ALTER        0 // No t/x altering.
`define axi2axi_cpu_pbus_X2X_LEN_ONLY_ALTER     1 // T/x length altering only.
`define axi2axi_cpu_pbus_X2X_MP_LRGR_ALTER      2 // MP DW largest altering.
`define axi2axi_cpu_pbus_X2X_SP_LRGR_ALTER      3 // SP DW largest altering, no upsizing.
`define axi2axi_cpu_pbus_X2X_SP_LRGR_US_ALTER   4 // SP DW largest altering, upsizing.

// Macros to define alock access types for resizer use
`define axi2axi_cpu_pbus_NORMAL                 2'b00 //normal access
`define axi2axi_cpu_pbus_EXCLUS                 2'b01 //exclusive access
`define axi2axi_cpu_pbus_LOCKED                 2'b10 //locked access

// Macros to define different cases in resizer (upsize --> US)
`define axi2axi_cpu_pbus_NO_ACTION               3'b000 //No action
`define axi2axi_cpu_pbus_AS_IS_OR_US_INLAST      3'b001 //pass as is or US from last
`define axi2axi_cpu_pbus_MUL_XACT_OR_US_INLAST   3'b010 //mul xacts or US from last 
`define axi2axi_cpu_pbus_MUL_XACT_OR_US_NOTLAST  3'b011 //mul xacts or US not from last
`define axi2axi_cpu_pbus_SINGLE_XACT_INLAST      3'b100 //single xact from last xact
`define axi2axi_cpu_pbus_MUL_XACT_INLAST         3'b101 //mul xacts from last xact
`define axi2axi_cpu_pbus_MUL_XACT_NOTLAST        3'b110 //mul xacts not from last xact

// Macros to define controls in resizer (MUL_XACT_OR_US_INLAST -> MXUI)
`define axi2axi_cpu_pbus_IDLE_CTRL               4'b0000 //idle
`define axi2axi_cpu_pbus_MUX_OVERONE             4'b0001 //over one xact for *_NOTLAST
`define axi2axi_cpu_pbus_MUX_WRAP_BOUND          4'b0010 //wrap boundary for *_NOTLAST
`define axi2axi_cpu_pbus_MUX_WRAP                4'b0011 //from boundary for *_NOTLAST
`define axi2axi_cpu_pbus_MUX_CROSS               4'b0100 //cross boundary for *_NOTLAST
`define axi2axi_cpu_pbus_MUX_FI_MUL              4'b0101 //mul xact of *_NOTLAST, fix/incr
`define axi2axi_cpu_pbus_MUX_FI_LAST             4'b0110 //last mutil xact for *_NOTLAST
`define axi2axi_cpu_pbus_MXI_WRAP                4'b0111 //wrap for MUL_XACT_INLAST
`define axi2axi_cpu_pbus_MXI_FI                  4'b1000 //fix/incr for MUL_XACT_INLAST
`define axi2axi_cpu_pbus_MXUI_SINGLE             4'b1001 //single xact for MXUI
`define axi2axi_cpu_pbus_MXUI_NO_CROSS           4'b1010 //no cross boundary for MXUI
`define axi2axi_cpu_pbus_MXUI_CROSS              4'b1011 //cross boundary for MXUI
`define axi2axi_cpu_pbus_MXUI_DS                 4'b1100 //downsize for MXUI
`define axi2axi_cpu_pbus_MXUI_FI_SINGLE          4'b1101 //fix/incr single xact for MXUI
`define axi2axi_cpu_pbus_MXUI_FI_MUL             4'b1110 //fix/incr mul xacts for MXUI
`define axi2axi_cpu_pbus_MXUI_FI_DS              4'b1111 //fix/incr downsize for MXUI

  // Width of bus containing aw channel payloads for all slave ports.
  `define axi2axi_cpu_pbus_BUS_AWPYLD_W_SP (`axi2axi_cpu_pbus_X2X_NUM_W_PORTS*`axi2axi_cpu_pbus_X2X_AWPYLD_W_SP)
  
  // Width of bus containing w channel payloads for all slave ports.
  // Sized for the master port.
  `define axi2axi_cpu_pbus_BUS_WPYLD_W_MP (`axi2axi_cpu_pbus_X2X_NUM_W_PORTS*`axi2axi_cpu_pbus_X2X_WPYLD_W_MP)
  
  // Width of bus containing w channel payloads for all slave ports.
  // Sized for the slave port.
  `define axi2axi_cpu_pbus_BUS_WPYLD_W_SP (`axi2axi_cpu_pbus_X2X_NUM_W_PORTS*`axi2axi_cpu_pbus_X2X_WPYLD_W_SP)

  // Width of bus containing b channel payloads for all slave ports.
  `define axi2axi_cpu_pbus_BUS_BPYLD_W_SP (`axi2axi_cpu_pbus_X2X_NUM_W_PORTS*`axi2axi_cpu_pbus_X2X_BPYLD_W_SP)


//==============================================================================
// End Guard
//==============================================================================
