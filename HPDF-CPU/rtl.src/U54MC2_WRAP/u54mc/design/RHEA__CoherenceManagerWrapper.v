//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__CoherenceManagerWrapper(
  input          rf_reset,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_ready,
  output         auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_opcode,
  output [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_param,
  output [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_size,
  output [3:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_source,
  output [35:0]  auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_address,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_user_amba_prot_bufferable,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_user_amba_prot_modifiable,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_user_amba_prot_readalloc,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_user_amba_prot_writealloc,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_user_amba_prot_privileged,
  output         auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_user_amba_prot_secure,
  output [15:0]  auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_mask,
  output [127:0] auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_data,
  output         auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_ready,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_valid,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_bits_opcode,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_bits_size,
  input  [3:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_bits_source,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_bits_denied,
  input  [127:0] auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_bits_data,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_bits_corrupt,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_ready,
  output         auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_opcode,
  output [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_param,
  output [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_size,
  output [3:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_source,
  output [35:0]  auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_address,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_user_amba_prot_bufferable,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_user_amba_prot_modifiable,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_user_amba_prot_readalloc,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_user_amba_prot_writealloc,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_user_amba_prot_privileged,
  output         auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_user_amba_prot_secure,
  output [15:0]  auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_mask,
  output [127:0] auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_data,
  output         auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_ready,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_valid,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_bits_opcode,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_bits_size,
  input  [3:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_bits_source,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_bits_denied,
  input  [127:0] auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_bits_data,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_bits_corrupt,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_ready,
  output         auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_opcode,
  output [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_param,
  output [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_size,
  output [3:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_source,
  output [35:0]  auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_address,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_user_amba_prot_bufferable,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_user_amba_prot_modifiable,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_user_amba_prot_readalloc,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_user_amba_prot_writealloc,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_user_amba_prot_privileged,
  output         auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_user_amba_prot_secure,
  output [15:0]  auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_mask,
  output [127:0] auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_data,
  output         auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_ready,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_valid,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_bits_opcode,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_bits_size,
  input  [3:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_bits_source,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_bits_denied,
  input  [127:0] auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_bits_data,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_bits_corrupt,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_ready,
  output         auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_valid,
  output [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_opcode,
  output [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_param,
  output [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_size,
  output [3:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_source,
  output [35:0]  auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_address,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_user_amba_prot_bufferable,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_user_amba_prot_modifiable,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_user_amba_prot_readalloc,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_user_amba_prot_writealloc,
  output
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_user_amba_prot_privileged,
  output         auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_user_amba_prot_secure,
  output [15:0]  auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_mask,
  output [127:0] auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_data,
  output         auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_ready,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_valid,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_bits_opcode,
  input  [2:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_bits_size,
  input  [3:0]   auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_bits_source,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_bits_denied,
  input  [127:0] auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_bits_data,
  input          auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_bits_corrupt,
  output         auto_l2_in_a_ready,
  input          auto_l2_in_a_valid,
  input  [2:0]   auto_l2_in_a_bits_opcode,
  input  [2:0]   auto_l2_in_a_bits_param,
  input  [1:0]   auto_l2_in_a_bits_size,
  input  [10:0]  auto_l2_in_a_bits_source,
  input  [25:0]  auto_l2_in_a_bits_address,
  input  [7:0]   auto_l2_in_a_bits_mask,
  input  [63:0]  auto_l2_in_a_bits_data,
  input          auto_l2_in_a_bits_corrupt,
  input          auto_l2_in_d_ready,
  output         auto_l2_in_d_valid,
  output [2:0]   auto_l2_in_d_bits_opcode,
  output [1:0]   auto_l2_in_d_bits_size,
  output [10:0]  auto_l2_in_d_bits_source,
  output [63:0]  auto_l2_in_d_bits_data,
  output         auto_l2_side_in_a_ready,
  input          auto_l2_side_in_a_valid,
  input  [2:0]   auto_l2_side_in_a_bits_opcode,
  input  [2:0]   auto_l2_side_in_a_bits_param,
  input  [2:0]   auto_l2_side_in_a_bits_size,
  input  [9:0]   auto_l2_side_in_a_bits_source,
  input  [27:0]  auto_l2_side_in_a_bits_address,
  input  [15:0]  auto_l2_side_in_a_bits_mask,
  input  [127:0] auto_l2_side_in_a_bits_data,
  input          auto_l2_side_in_a_bits_corrupt,
  input          auto_l2_side_in_d_ready,
  output         auto_l2_side_in_d_valid,
  output [2:0]   auto_l2_side_in_d_bits_opcode,
  output [2:0]   auto_l2_side_in_d_bits_size,
  output [9:0]   auto_l2_side_in_d_bits_source,
  output [127:0] auto_l2_side_in_d_bits_data,
  output         auto_l2_side_in_d_bits_corrupt,
  input          auto_subsystem_l2_clock_groups_in_member_subsystem_l2_2_clock,
  input          auto_subsystem_l2_clock_groups_in_member_subsystem_l2_2_reset,
  input          auto_subsystem_l2_clock_groups_in_member_subsystem_l2_0_clock,
  input          auto_subsystem_l2_clock_groups_in_member_subsystem_l2_0_reset,
  output         auto_subsystem_l2_clock_groups_out_member_subsystem_mbus_address_adjuster_1_clock,
  output         auto_subsystem_l2_clock_groups_out_member_subsystem_mbus_address_adjuster_1_reset,
  output         auto_bus_xing_in_3_a_ready,
  input          auto_bus_xing_in_3_a_valid,
  input  [2:0]   auto_bus_xing_in_3_a_bits_opcode,
  input  [2:0]   auto_bus_xing_in_3_a_bits_param,
  input  [2:0]   auto_bus_xing_in_3_a_bits_size,
  input  [6:0]   auto_bus_xing_in_3_a_bits_source,
  input  [35:0]  auto_bus_xing_in_3_a_bits_address,
  input  [15:0]  auto_bus_xing_in_3_a_bits_mask,
  input  [127:0] auto_bus_xing_in_3_a_bits_data,
  input          auto_bus_xing_in_3_a_bits_corrupt,
  input          auto_bus_xing_in_3_b_ready,
  output         auto_bus_xing_in_3_b_valid,
  output [2:0]   auto_bus_xing_in_3_b_bits_opcode,
  output [1:0]   auto_bus_xing_in_3_b_bits_param,
  output [2:0]   auto_bus_xing_in_3_b_bits_size,
  output [6:0]   auto_bus_xing_in_3_b_bits_source,
  output [35:0]  auto_bus_xing_in_3_b_bits_address,
  output [15:0]  auto_bus_xing_in_3_b_bits_mask,
  output [127:0] auto_bus_xing_in_3_b_bits_data,
  output         auto_bus_xing_in_3_b_bits_corrupt,
  output         auto_bus_xing_in_3_c_ready,
  input          auto_bus_xing_in_3_c_valid,
  input  [2:0]   auto_bus_xing_in_3_c_bits_opcode,
  input  [2:0]   auto_bus_xing_in_3_c_bits_param,
  input  [2:0]   auto_bus_xing_in_3_c_bits_size,
  input  [6:0]   auto_bus_xing_in_3_c_bits_source,
  input  [35:0]  auto_bus_xing_in_3_c_bits_address,
  input  [127:0] auto_bus_xing_in_3_c_bits_data,
  input          auto_bus_xing_in_3_c_bits_corrupt,
  input          auto_bus_xing_in_3_d_ready,
  output         auto_bus_xing_in_3_d_valid,
  output [2:0]   auto_bus_xing_in_3_d_bits_opcode,
  output [1:0]   auto_bus_xing_in_3_d_bits_param,
  output [2:0]   auto_bus_xing_in_3_d_bits_size,
  output [6:0]   auto_bus_xing_in_3_d_bits_source,
  output [2:0]   auto_bus_xing_in_3_d_bits_sink,
  output         auto_bus_xing_in_3_d_bits_denied,
  output [127:0] auto_bus_xing_in_3_d_bits_data,
  output         auto_bus_xing_in_3_d_bits_corrupt,
  output         auto_bus_xing_in_3_e_ready,
  input          auto_bus_xing_in_3_e_valid,
  input  [2:0]   auto_bus_xing_in_3_e_bits_sink,
  output         auto_bus_xing_in_2_a_ready,
  input          auto_bus_xing_in_2_a_valid,
  input  [2:0]   auto_bus_xing_in_2_a_bits_opcode,
  input  [2:0]   auto_bus_xing_in_2_a_bits_param,
  input  [2:0]   auto_bus_xing_in_2_a_bits_size,
  input  [6:0]   auto_bus_xing_in_2_a_bits_source,
  input  [35:0]  auto_bus_xing_in_2_a_bits_address,
  input  [15:0]  auto_bus_xing_in_2_a_bits_mask,
  input  [127:0] auto_bus_xing_in_2_a_bits_data,
  input          auto_bus_xing_in_2_a_bits_corrupt,
  input          auto_bus_xing_in_2_b_ready,
  output         auto_bus_xing_in_2_b_valid,
  output [2:0]   auto_bus_xing_in_2_b_bits_opcode,
  output [1:0]   auto_bus_xing_in_2_b_bits_param,
  output [2:0]   auto_bus_xing_in_2_b_bits_size,
  output [6:0]   auto_bus_xing_in_2_b_bits_source,
  output [35:0]  auto_bus_xing_in_2_b_bits_address,
  output [15:0]  auto_bus_xing_in_2_b_bits_mask,
  output [127:0] auto_bus_xing_in_2_b_bits_data,
  output         auto_bus_xing_in_2_b_bits_corrupt,
  output         auto_bus_xing_in_2_c_ready,
  input          auto_bus_xing_in_2_c_valid,
  input  [2:0]   auto_bus_xing_in_2_c_bits_opcode,
  input  [2:0]   auto_bus_xing_in_2_c_bits_param,
  input  [2:0]   auto_bus_xing_in_2_c_bits_size,
  input  [6:0]   auto_bus_xing_in_2_c_bits_source,
  input  [35:0]  auto_bus_xing_in_2_c_bits_address,
  input  [127:0] auto_bus_xing_in_2_c_bits_data,
  input          auto_bus_xing_in_2_c_bits_corrupt,
  input          auto_bus_xing_in_2_d_ready,
  output         auto_bus_xing_in_2_d_valid,
  output [2:0]   auto_bus_xing_in_2_d_bits_opcode,
  output [1:0]   auto_bus_xing_in_2_d_bits_param,
  output [2:0]   auto_bus_xing_in_2_d_bits_size,
  output [6:0]   auto_bus_xing_in_2_d_bits_source,
  output [2:0]   auto_bus_xing_in_2_d_bits_sink,
  output         auto_bus_xing_in_2_d_bits_denied,
  output [127:0] auto_bus_xing_in_2_d_bits_data,
  output         auto_bus_xing_in_2_d_bits_corrupt,
  output         auto_bus_xing_in_2_e_ready,
  input          auto_bus_xing_in_2_e_valid,
  input  [2:0]   auto_bus_xing_in_2_e_bits_sink,
  output         auto_bus_xing_in_1_a_ready,
  input          auto_bus_xing_in_1_a_valid,
  input  [2:0]   auto_bus_xing_in_1_a_bits_opcode,
  input  [2:0]   auto_bus_xing_in_1_a_bits_param,
  input  [2:0]   auto_bus_xing_in_1_a_bits_size,
  input  [6:0]   auto_bus_xing_in_1_a_bits_source,
  input  [35:0]  auto_bus_xing_in_1_a_bits_address,
  input  [15:0]  auto_bus_xing_in_1_a_bits_mask,
  input  [127:0] auto_bus_xing_in_1_a_bits_data,
  input          auto_bus_xing_in_1_a_bits_corrupt,
  input          auto_bus_xing_in_1_b_ready,
  output         auto_bus_xing_in_1_b_valid,
  output [2:0]   auto_bus_xing_in_1_b_bits_opcode,
  output [1:0]   auto_bus_xing_in_1_b_bits_param,
  output [2:0]   auto_bus_xing_in_1_b_bits_size,
  output [6:0]   auto_bus_xing_in_1_b_bits_source,
  output [35:0]  auto_bus_xing_in_1_b_bits_address,
  output [15:0]  auto_bus_xing_in_1_b_bits_mask,
  output [127:0] auto_bus_xing_in_1_b_bits_data,
  output         auto_bus_xing_in_1_b_bits_corrupt,
  output         auto_bus_xing_in_1_c_ready,
  input          auto_bus_xing_in_1_c_valid,
  input  [2:0]   auto_bus_xing_in_1_c_bits_opcode,
  input  [2:0]   auto_bus_xing_in_1_c_bits_param,
  input  [2:0]   auto_bus_xing_in_1_c_bits_size,
  input  [6:0]   auto_bus_xing_in_1_c_bits_source,
  input  [35:0]  auto_bus_xing_in_1_c_bits_address,
  input  [127:0] auto_bus_xing_in_1_c_bits_data,
  input          auto_bus_xing_in_1_c_bits_corrupt,
  input          auto_bus_xing_in_1_d_ready,
  output         auto_bus_xing_in_1_d_valid,
  output [2:0]   auto_bus_xing_in_1_d_bits_opcode,
  output [1:0]   auto_bus_xing_in_1_d_bits_param,
  output [2:0]   auto_bus_xing_in_1_d_bits_size,
  output [6:0]   auto_bus_xing_in_1_d_bits_source,
  output [2:0]   auto_bus_xing_in_1_d_bits_sink,
  output         auto_bus_xing_in_1_d_bits_denied,
  output [127:0] auto_bus_xing_in_1_d_bits_data,
  output         auto_bus_xing_in_1_d_bits_corrupt,
  output         auto_bus_xing_in_1_e_ready,
  input          auto_bus_xing_in_1_e_valid,
  input  [2:0]   auto_bus_xing_in_1_e_bits_sink,
  output         auto_bus_xing_in_0_a_ready,
  input          auto_bus_xing_in_0_a_valid,
  input  [2:0]   auto_bus_xing_in_0_a_bits_opcode,
  input  [2:0]   auto_bus_xing_in_0_a_bits_param,
  input  [2:0]   auto_bus_xing_in_0_a_bits_size,
  input  [6:0]   auto_bus_xing_in_0_a_bits_source,
  input  [35:0]  auto_bus_xing_in_0_a_bits_address,
  input  [15:0]  auto_bus_xing_in_0_a_bits_mask,
  input  [127:0] auto_bus_xing_in_0_a_bits_data,
  input          auto_bus_xing_in_0_a_bits_corrupt,
  input          auto_bus_xing_in_0_b_ready,
  output         auto_bus_xing_in_0_b_valid,
  output [2:0]   auto_bus_xing_in_0_b_bits_opcode,
  output [1:0]   auto_bus_xing_in_0_b_bits_param,
  output [2:0]   auto_bus_xing_in_0_b_bits_size,
  output [6:0]   auto_bus_xing_in_0_b_bits_source,
  output [35:0]  auto_bus_xing_in_0_b_bits_address,
  output [15:0]  auto_bus_xing_in_0_b_bits_mask,
  output [127:0] auto_bus_xing_in_0_b_bits_data,
  output         auto_bus_xing_in_0_b_bits_corrupt,
  output         auto_bus_xing_in_0_c_ready,
  input          auto_bus_xing_in_0_c_valid,
  input  [2:0]   auto_bus_xing_in_0_c_bits_opcode,
  input  [2:0]   auto_bus_xing_in_0_c_bits_param,
  input  [2:0]   auto_bus_xing_in_0_c_bits_size,
  input  [6:0]   auto_bus_xing_in_0_c_bits_source,
  input  [35:0]  auto_bus_xing_in_0_c_bits_address,
  input  [127:0] auto_bus_xing_in_0_c_bits_data,
  input          auto_bus_xing_in_0_c_bits_corrupt,
  input          auto_bus_xing_in_0_d_ready,
  output         auto_bus_xing_in_0_d_valid,
  output [2:0]   auto_bus_xing_in_0_d_bits_opcode,
  output [1:0]   auto_bus_xing_in_0_d_bits_param,
  output [2:0]   auto_bus_xing_in_0_d_bits_size,
  output [6:0]   auto_bus_xing_in_0_d_bits_source,
  output [2:0]   auto_bus_xing_in_0_d_bits_sink,
  output         auto_bus_xing_in_0_d_bits_denied,
  output [127:0] auto_bus_xing_in_0_d_bits_data,
  output         auto_bus_xing_in_0_d_bits_corrupt,
  output         auto_bus_xing_in_0_e_ready,
  input          auto_bus_xing_in_0_e_valid,
  input  [2:0]   auto_bus_xing_in_0_e_bits_sink
);
  wire  subsystem_l2_clock_groups_auto_in_member_subsystem_l2_2_clock;
  wire  subsystem_l2_clock_groups_auto_in_member_subsystem_l2_2_reset;
  wire  subsystem_l2_clock_groups_auto_in_member_subsystem_l2_0_clock;
  wire  subsystem_l2_clock_groups_auto_in_member_subsystem_l2_0_reset;
  wire  subsystem_l2_clock_groups_auto_out_1_member_subsystem_mbus_address_adjuster_1_clock;
  wire  subsystem_l2_clock_groups_auto_out_1_member_subsystem_mbus_address_adjuster_1_reset;
  wire  subsystem_l2_clock_groups_auto_out_0_member_subsystem_l2_0_clock;
  wire  subsystem_l2_clock_groups_auto_out_0_member_subsystem_l2_0_reset;
  wire  clockGroup_auto_in_member_subsystem_l2_0_clock;
  wire  clockGroup_auto_in_member_subsystem_l2_0_reset;
  wire  clockGroup_auto_out_clock;
  wire  clockGroup_auto_out_reset;
  wire  fixedClockNode_auto_in_clock;
  wire  fixedClockNode_auto_in_reset;
  wire  fixedClockNode_auto_out_clock;
  wire  fixedClockNode_auto_out_reset;
  wire  l2_rf_reset; // @[Configs.scala 210:24]
  wire  l2_clock; // @[Configs.scala 210:24]
  wire  l2_reset; // @[Configs.scala 210:24]
  wire  l2_auto_in_4_a_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_4_a_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_4_a_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_4_a_bits_param; // @[Configs.scala 210:24]
  wire [1:0] l2_auto_in_4_a_bits_size; // @[Configs.scala 210:24]
  wire [10:0] l2_auto_in_4_a_bits_source; // @[Configs.scala 210:24]
  wire [25:0] l2_auto_in_4_a_bits_address; // @[Configs.scala 210:24]
  wire [7:0] l2_auto_in_4_a_bits_mask; // @[Configs.scala 210:24]
  wire [63:0] l2_auto_in_4_a_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_4_a_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_4_d_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_4_d_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_4_d_bits_opcode; // @[Configs.scala 210:24]
  wire [1:0] l2_auto_in_4_d_bits_size; // @[Configs.scala 210:24]
  wire [10:0] l2_auto_in_4_d_bits_source; // @[Configs.scala 210:24]
  wire [63:0] l2_auto_in_4_d_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_side_in_a_ready; // @[Configs.scala 210:24]
  wire  l2_auto_side_in_a_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_side_in_a_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_side_in_a_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_side_in_a_bits_size; // @[Configs.scala 210:24]
  wire [9:0] l2_auto_side_in_a_bits_source; // @[Configs.scala 210:24]
  wire [27:0] l2_auto_side_in_a_bits_address; // @[Configs.scala 210:24]
  wire [15:0] l2_auto_side_in_a_bits_mask; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_side_in_a_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_side_in_a_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_side_in_d_ready; // @[Configs.scala 210:24]
  wire  l2_auto_side_in_d_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_side_in_d_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_side_in_d_bits_size; // @[Configs.scala 210:24]
  wire [9:0] l2_auto_side_in_d_bits_source; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_side_in_d_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_side_in_d_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_3_a_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_3_a_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_3_a_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_3_a_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_3_a_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_3_a_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_in_3_a_bits_address; // @[Configs.scala 210:24]
  wire [15:0] l2_auto_in_3_a_bits_mask; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_3_a_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_3_a_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_3_b_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_3_b_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_3_b_bits_opcode; // @[Configs.scala 210:24]
  wire [1:0] l2_auto_in_3_b_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_3_b_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_3_b_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_in_3_b_bits_address; // @[Configs.scala 210:24]
  wire [15:0] l2_auto_in_3_b_bits_mask; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_3_b_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_3_b_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_3_c_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_3_c_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_3_c_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_3_c_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_3_c_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_3_c_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_in_3_c_bits_address; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_3_c_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_3_c_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_3_d_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_3_d_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_3_d_bits_opcode; // @[Configs.scala 210:24]
  wire [1:0] l2_auto_in_3_d_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_3_d_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_3_d_bits_source; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_3_d_bits_sink; // @[Configs.scala 210:24]
  wire  l2_auto_in_3_d_bits_denied; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_3_d_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_3_d_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_3_e_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_3_e_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_3_e_bits_sink; // @[Configs.scala 210:24]
  wire  l2_auto_in_2_a_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_2_a_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_2_a_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_2_a_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_2_a_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_2_a_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_in_2_a_bits_address; // @[Configs.scala 210:24]
  wire [15:0] l2_auto_in_2_a_bits_mask; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_2_a_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_2_a_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_2_b_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_2_b_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_2_b_bits_opcode; // @[Configs.scala 210:24]
  wire [1:0] l2_auto_in_2_b_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_2_b_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_2_b_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_in_2_b_bits_address; // @[Configs.scala 210:24]
  wire [15:0] l2_auto_in_2_b_bits_mask; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_2_b_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_2_b_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_2_c_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_2_c_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_2_c_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_2_c_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_2_c_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_2_c_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_in_2_c_bits_address; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_2_c_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_2_c_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_2_d_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_2_d_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_2_d_bits_opcode; // @[Configs.scala 210:24]
  wire [1:0] l2_auto_in_2_d_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_2_d_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_2_d_bits_source; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_2_d_bits_sink; // @[Configs.scala 210:24]
  wire  l2_auto_in_2_d_bits_denied; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_2_d_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_2_d_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_2_e_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_2_e_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_2_e_bits_sink; // @[Configs.scala 210:24]
  wire  l2_auto_in_1_a_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_1_a_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_1_a_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_1_a_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_1_a_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_1_a_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_in_1_a_bits_address; // @[Configs.scala 210:24]
  wire [15:0] l2_auto_in_1_a_bits_mask; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_1_a_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_1_a_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_1_b_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_1_b_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_1_b_bits_opcode; // @[Configs.scala 210:24]
  wire [1:0] l2_auto_in_1_b_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_1_b_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_1_b_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_in_1_b_bits_address; // @[Configs.scala 210:24]
  wire [15:0] l2_auto_in_1_b_bits_mask; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_1_b_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_1_b_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_1_c_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_1_c_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_1_c_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_1_c_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_1_c_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_1_c_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_in_1_c_bits_address; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_1_c_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_1_c_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_1_d_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_1_d_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_1_d_bits_opcode; // @[Configs.scala 210:24]
  wire [1:0] l2_auto_in_1_d_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_1_d_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_1_d_bits_source; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_1_d_bits_sink; // @[Configs.scala 210:24]
  wire  l2_auto_in_1_d_bits_denied; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_1_d_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_1_d_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_1_e_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_1_e_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_1_e_bits_sink; // @[Configs.scala 210:24]
  wire  l2_auto_in_0_a_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_0_a_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_0_a_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_0_a_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_0_a_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_0_a_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_in_0_a_bits_address; // @[Configs.scala 210:24]
  wire [15:0] l2_auto_in_0_a_bits_mask; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_0_a_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_0_a_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_0_b_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_0_b_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_0_b_bits_opcode; // @[Configs.scala 210:24]
  wire [1:0] l2_auto_in_0_b_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_0_b_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_0_b_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_in_0_b_bits_address; // @[Configs.scala 210:24]
  wire [15:0] l2_auto_in_0_b_bits_mask; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_0_b_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_0_b_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_0_c_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_0_c_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_0_c_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_0_c_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_0_c_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_0_c_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_in_0_c_bits_address; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_0_c_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_0_c_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_0_d_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_0_d_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_0_d_bits_opcode; // @[Configs.scala 210:24]
  wire [1:0] l2_auto_in_0_d_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_0_d_bits_size; // @[Configs.scala 210:24]
  wire [6:0] l2_auto_in_0_d_bits_source; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_0_d_bits_sink; // @[Configs.scala 210:24]
  wire  l2_auto_in_0_d_bits_denied; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_in_0_d_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_in_0_d_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_in_0_e_ready; // @[Configs.scala 210:24]
  wire  l2_auto_in_0_e_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_in_0_e_bits_sink; // @[Configs.scala 210:24]
  wire  l2_auto_out_3_a_ready; // @[Configs.scala 210:24]
  wire  l2_auto_out_3_a_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_3_a_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_3_a_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_3_a_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_out_3_a_bits_address; // @[Configs.scala 210:24]
  wire  l2_auto_out_3_c_ready; // @[Configs.scala 210:24]
  wire  l2_auto_out_3_c_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_3_c_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_3_c_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_3_c_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_out_3_c_bits_address; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_out_3_c_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_out_3_d_ready; // @[Configs.scala 210:24]
  wire  l2_auto_out_3_d_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_3_d_bits_opcode; // @[Configs.scala 210:24]
  wire [1:0] l2_auto_out_3_d_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_3_d_bits_size; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_3_d_bits_source; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_3_d_bits_sink; // @[Configs.scala 210:24]
  wire  l2_auto_out_3_d_bits_denied; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_out_3_d_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_out_3_d_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_out_3_e_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_3_e_bits_sink; // @[Configs.scala 210:24]
  wire  l2_auto_out_2_a_ready; // @[Configs.scala 210:24]
  wire  l2_auto_out_2_a_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_2_a_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_2_a_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_2_a_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_out_2_a_bits_address; // @[Configs.scala 210:24]
  wire  l2_auto_out_2_c_ready; // @[Configs.scala 210:24]
  wire  l2_auto_out_2_c_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_2_c_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_2_c_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_2_c_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_out_2_c_bits_address; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_out_2_c_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_out_2_d_ready; // @[Configs.scala 210:24]
  wire  l2_auto_out_2_d_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_2_d_bits_opcode; // @[Configs.scala 210:24]
  wire [1:0] l2_auto_out_2_d_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_2_d_bits_size; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_2_d_bits_source; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_2_d_bits_sink; // @[Configs.scala 210:24]
  wire  l2_auto_out_2_d_bits_denied; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_out_2_d_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_out_2_d_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_out_2_e_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_2_e_bits_sink; // @[Configs.scala 210:24]
  wire  l2_auto_out_1_a_ready; // @[Configs.scala 210:24]
  wire  l2_auto_out_1_a_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_1_a_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_1_a_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_1_a_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_out_1_a_bits_address; // @[Configs.scala 210:24]
  wire  l2_auto_out_1_c_ready; // @[Configs.scala 210:24]
  wire  l2_auto_out_1_c_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_1_c_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_1_c_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_1_c_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_out_1_c_bits_address; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_out_1_c_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_out_1_d_ready; // @[Configs.scala 210:24]
  wire  l2_auto_out_1_d_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_1_d_bits_opcode; // @[Configs.scala 210:24]
  wire [1:0] l2_auto_out_1_d_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_1_d_bits_size; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_1_d_bits_source; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_1_d_bits_sink; // @[Configs.scala 210:24]
  wire  l2_auto_out_1_d_bits_denied; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_out_1_d_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_out_1_d_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_out_1_e_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_1_e_bits_sink; // @[Configs.scala 210:24]
  wire  l2_auto_out_0_a_ready; // @[Configs.scala 210:24]
  wire  l2_auto_out_0_a_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_0_a_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_0_a_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_0_a_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_out_0_a_bits_address; // @[Configs.scala 210:24]
  wire  l2_auto_out_0_c_ready; // @[Configs.scala 210:24]
  wire  l2_auto_out_0_c_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_0_c_bits_opcode; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_0_c_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_0_c_bits_source; // @[Configs.scala 210:24]
  wire [35:0] l2_auto_out_0_c_bits_address; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_out_0_c_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_out_0_d_ready; // @[Configs.scala 210:24]
  wire  l2_auto_out_0_d_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_0_d_bits_opcode; // @[Configs.scala 210:24]
  wire [1:0] l2_auto_out_0_d_bits_param; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_0_d_bits_size; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_0_d_bits_source; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_0_d_bits_sink; // @[Configs.scala 210:24]
  wire  l2_auto_out_0_d_bits_denied; // @[Configs.scala 210:24]
  wire [127:0] l2_auto_out_0_d_bits_data; // @[Configs.scala 210:24]
  wire  l2_auto_out_0_d_bits_corrupt; // @[Configs.scala 210:24]
  wire  l2_auto_out_0_e_valid; // @[Configs.scala 210:24]
  wire [2:0] l2_auto_out_0_e_bits_sink; // @[Configs.scala 210:24]
  wire  filter_auto_in_3_a_ready;
  wire  filter_auto_in_3_a_valid;
  wire [2:0] filter_auto_in_3_a_bits_opcode;
  wire [2:0] filter_auto_in_3_a_bits_param;
  wire [2:0] filter_auto_in_3_a_bits_size;
  wire [6:0] filter_auto_in_3_a_bits_source;
  wire [35:0] filter_auto_in_3_a_bits_address;
  wire [15:0] filter_auto_in_3_a_bits_mask;
  wire [127:0] filter_auto_in_3_a_bits_data;
  wire  filter_auto_in_3_a_bits_corrupt;
  wire  filter_auto_in_3_b_ready;
  wire  filter_auto_in_3_b_valid;
  wire [2:0] filter_auto_in_3_b_bits_opcode;
  wire [1:0] filter_auto_in_3_b_bits_param;
  wire [2:0] filter_auto_in_3_b_bits_size;
  wire [6:0] filter_auto_in_3_b_bits_source;
  wire [35:0] filter_auto_in_3_b_bits_address;
  wire [15:0] filter_auto_in_3_b_bits_mask;
  wire [127:0] filter_auto_in_3_b_bits_data;
  wire  filter_auto_in_3_b_bits_corrupt;
  wire  filter_auto_in_3_c_ready;
  wire  filter_auto_in_3_c_valid;
  wire [2:0] filter_auto_in_3_c_bits_opcode;
  wire [2:0] filter_auto_in_3_c_bits_param;
  wire [2:0] filter_auto_in_3_c_bits_size;
  wire [6:0] filter_auto_in_3_c_bits_source;
  wire [35:0] filter_auto_in_3_c_bits_address;
  wire [127:0] filter_auto_in_3_c_bits_data;
  wire  filter_auto_in_3_c_bits_corrupt;
  wire  filter_auto_in_3_d_ready;
  wire  filter_auto_in_3_d_valid;
  wire [2:0] filter_auto_in_3_d_bits_opcode;
  wire [1:0] filter_auto_in_3_d_bits_param;
  wire [2:0] filter_auto_in_3_d_bits_size;
  wire [6:0] filter_auto_in_3_d_bits_source;
  wire [2:0] filter_auto_in_3_d_bits_sink;
  wire  filter_auto_in_3_d_bits_denied;
  wire [127:0] filter_auto_in_3_d_bits_data;
  wire  filter_auto_in_3_d_bits_corrupt;
  wire  filter_auto_in_3_e_ready;
  wire  filter_auto_in_3_e_valid;
  wire [2:0] filter_auto_in_3_e_bits_sink;
  wire  filter_auto_in_2_a_ready;
  wire  filter_auto_in_2_a_valid;
  wire [2:0] filter_auto_in_2_a_bits_opcode;
  wire [2:0] filter_auto_in_2_a_bits_param;
  wire [2:0] filter_auto_in_2_a_bits_size;
  wire [6:0] filter_auto_in_2_a_bits_source;
  wire [35:0] filter_auto_in_2_a_bits_address;
  wire [15:0] filter_auto_in_2_a_bits_mask;
  wire [127:0] filter_auto_in_2_a_bits_data;
  wire  filter_auto_in_2_a_bits_corrupt;
  wire  filter_auto_in_2_b_ready;
  wire  filter_auto_in_2_b_valid;
  wire [2:0] filter_auto_in_2_b_bits_opcode;
  wire [1:0] filter_auto_in_2_b_bits_param;
  wire [2:0] filter_auto_in_2_b_bits_size;
  wire [6:0] filter_auto_in_2_b_bits_source;
  wire [35:0] filter_auto_in_2_b_bits_address;
  wire [15:0] filter_auto_in_2_b_bits_mask;
  wire [127:0] filter_auto_in_2_b_bits_data;
  wire  filter_auto_in_2_b_bits_corrupt;
  wire  filter_auto_in_2_c_ready;
  wire  filter_auto_in_2_c_valid;
  wire [2:0] filter_auto_in_2_c_bits_opcode;
  wire [2:0] filter_auto_in_2_c_bits_param;
  wire [2:0] filter_auto_in_2_c_bits_size;
  wire [6:0] filter_auto_in_2_c_bits_source;
  wire [35:0] filter_auto_in_2_c_bits_address;
  wire [127:0] filter_auto_in_2_c_bits_data;
  wire  filter_auto_in_2_c_bits_corrupt;
  wire  filter_auto_in_2_d_ready;
  wire  filter_auto_in_2_d_valid;
  wire [2:0] filter_auto_in_2_d_bits_opcode;
  wire [1:0] filter_auto_in_2_d_bits_param;
  wire [2:0] filter_auto_in_2_d_bits_size;
  wire [6:0] filter_auto_in_2_d_bits_source;
  wire [2:0] filter_auto_in_2_d_bits_sink;
  wire  filter_auto_in_2_d_bits_denied;
  wire [127:0] filter_auto_in_2_d_bits_data;
  wire  filter_auto_in_2_d_bits_corrupt;
  wire  filter_auto_in_2_e_ready;
  wire  filter_auto_in_2_e_valid;
  wire [2:0] filter_auto_in_2_e_bits_sink;
  wire  filter_auto_in_1_a_ready;
  wire  filter_auto_in_1_a_valid;
  wire [2:0] filter_auto_in_1_a_bits_opcode;
  wire [2:0] filter_auto_in_1_a_bits_param;
  wire [2:0] filter_auto_in_1_a_bits_size;
  wire [6:0] filter_auto_in_1_a_bits_source;
  wire [35:0] filter_auto_in_1_a_bits_address;
  wire [15:0] filter_auto_in_1_a_bits_mask;
  wire [127:0] filter_auto_in_1_a_bits_data;
  wire  filter_auto_in_1_a_bits_corrupt;
  wire  filter_auto_in_1_b_ready;
  wire  filter_auto_in_1_b_valid;
  wire [2:0] filter_auto_in_1_b_bits_opcode;
  wire [1:0] filter_auto_in_1_b_bits_param;
  wire [2:0] filter_auto_in_1_b_bits_size;
  wire [6:0] filter_auto_in_1_b_bits_source;
  wire [35:0] filter_auto_in_1_b_bits_address;
  wire [15:0] filter_auto_in_1_b_bits_mask;
  wire [127:0] filter_auto_in_1_b_bits_data;
  wire  filter_auto_in_1_b_bits_corrupt;
  wire  filter_auto_in_1_c_ready;
  wire  filter_auto_in_1_c_valid;
  wire [2:0] filter_auto_in_1_c_bits_opcode;
  wire [2:0] filter_auto_in_1_c_bits_param;
  wire [2:0] filter_auto_in_1_c_bits_size;
  wire [6:0] filter_auto_in_1_c_bits_source;
  wire [35:0] filter_auto_in_1_c_bits_address;
  wire [127:0] filter_auto_in_1_c_bits_data;
  wire  filter_auto_in_1_c_bits_corrupt;
  wire  filter_auto_in_1_d_ready;
  wire  filter_auto_in_1_d_valid;
  wire [2:0] filter_auto_in_1_d_bits_opcode;
  wire [1:0] filter_auto_in_1_d_bits_param;
  wire [2:0] filter_auto_in_1_d_bits_size;
  wire [6:0] filter_auto_in_1_d_bits_source;
  wire [2:0] filter_auto_in_1_d_bits_sink;
  wire  filter_auto_in_1_d_bits_denied;
  wire [127:0] filter_auto_in_1_d_bits_data;
  wire  filter_auto_in_1_d_bits_corrupt;
  wire  filter_auto_in_1_e_ready;
  wire  filter_auto_in_1_e_valid;
  wire [2:0] filter_auto_in_1_e_bits_sink;
  wire  filter_auto_in_0_a_ready;
  wire  filter_auto_in_0_a_valid;
  wire [2:0] filter_auto_in_0_a_bits_opcode;
  wire [2:0] filter_auto_in_0_a_bits_param;
  wire [2:0] filter_auto_in_0_a_bits_size;
  wire [6:0] filter_auto_in_0_a_bits_source;
  wire [35:0] filter_auto_in_0_a_bits_address;
  wire [15:0] filter_auto_in_0_a_bits_mask;
  wire [127:0] filter_auto_in_0_a_bits_data;
  wire  filter_auto_in_0_a_bits_corrupt;
  wire  filter_auto_in_0_b_ready;
  wire  filter_auto_in_0_b_valid;
  wire [2:0] filter_auto_in_0_b_bits_opcode;
  wire [1:0] filter_auto_in_0_b_bits_param;
  wire [2:0] filter_auto_in_0_b_bits_size;
  wire [6:0] filter_auto_in_0_b_bits_source;
  wire [35:0] filter_auto_in_0_b_bits_address;
  wire [15:0] filter_auto_in_0_b_bits_mask;
  wire [127:0] filter_auto_in_0_b_bits_data;
  wire  filter_auto_in_0_b_bits_corrupt;
  wire  filter_auto_in_0_c_ready;
  wire  filter_auto_in_0_c_valid;
  wire [2:0] filter_auto_in_0_c_bits_opcode;
  wire [2:0] filter_auto_in_0_c_bits_param;
  wire [2:0] filter_auto_in_0_c_bits_size;
  wire [6:0] filter_auto_in_0_c_bits_source;
  wire [35:0] filter_auto_in_0_c_bits_address;
  wire [127:0] filter_auto_in_0_c_bits_data;
  wire  filter_auto_in_0_c_bits_corrupt;
  wire  filter_auto_in_0_d_ready;
  wire  filter_auto_in_0_d_valid;
  wire [2:0] filter_auto_in_0_d_bits_opcode;
  wire [1:0] filter_auto_in_0_d_bits_param;
  wire [2:0] filter_auto_in_0_d_bits_size;
  wire [6:0] filter_auto_in_0_d_bits_source;
  wire [2:0] filter_auto_in_0_d_bits_sink;
  wire  filter_auto_in_0_d_bits_denied;
  wire [127:0] filter_auto_in_0_d_bits_data;
  wire  filter_auto_in_0_d_bits_corrupt;
  wire  filter_auto_in_0_e_ready;
  wire  filter_auto_in_0_e_valid;
  wire [2:0] filter_auto_in_0_e_bits_sink;
  wire  filter_auto_out_3_a_ready;
  wire  filter_auto_out_3_a_valid;
  wire [2:0] filter_auto_out_3_a_bits_opcode;
  wire [2:0] filter_auto_out_3_a_bits_param;
  wire [2:0] filter_auto_out_3_a_bits_size;
  wire [6:0] filter_auto_out_3_a_bits_source;
  wire [35:0] filter_auto_out_3_a_bits_address;
  wire [15:0] filter_auto_out_3_a_bits_mask;
  wire [127:0] filter_auto_out_3_a_bits_data;
  wire  filter_auto_out_3_a_bits_corrupt;
  wire  filter_auto_out_3_b_ready;
  wire  filter_auto_out_3_b_valid;
  wire [2:0] filter_auto_out_3_b_bits_opcode;
  wire [1:0] filter_auto_out_3_b_bits_param;
  wire [2:0] filter_auto_out_3_b_bits_size;
  wire [6:0] filter_auto_out_3_b_bits_source;
  wire [35:0] filter_auto_out_3_b_bits_address;
  wire [15:0] filter_auto_out_3_b_bits_mask;
  wire [127:0] filter_auto_out_3_b_bits_data;
  wire  filter_auto_out_3_b_bits_corrupt;
  wire  filter_auto_out_3_c_ready;
  wire  filter_auto_out_3_c_valid;
  wire [2:0] filter_auto_out_3_c_bits_opcode;
  wire [2:0] filter_auto_out_3_c_bits_param;
  wire [2:0] filter_auto_out_3_c_bits_size;
  wire [6:0] filter_auto_out_3_c_bits_source;
  wire [35:0] filter_auto_out_3_c_bits_address;
  wire [127:0] filter_auto_out_3_c_bits_data;
  wire  filter_auto_out_3_c_bits_corrupt;
  wire  filter_auto_out_3_d_ready;
  wire  filter_auto_out_3_d_valid;
  wire [2:0] filter_auto_out_3_d_bits_opcode;
  wire [1:0] filter_auto_out_3_d_bits_param;
  wire [2:0] filter_auto_out_3_d_bits_size;
  wire [6:0] filter_auto_out_3_d_bits_source;
  wire [2:0] filter_auto_out_3_d_bits_sink;
  wire  filter_auto_out_3_d_bits_denied;
  wire [127:0] filter_auto_out_3_d_bits_data;
  wire  filter_auto_out_3_d_bits_corrupt;
  wire  filter_auto_out_3_e_ready;
  wire  filter_auto_out_3_e_valid;
  wire [2:0] filter_auto_out_3_e_bits_sink;
  wire  filter_auto_out_2_a_ready;
  wire  filter_auto_out_2_a_valid;
  wire [2:0] filter_auto_out_2_a_bits_opcode;
  wire [2:0] filter_auto_out_2_a_bits_param;
  wire [2:0] filter_auto_out_2_a_bits_size;
  wire [6:0] filter_auto_out_2_a_bits_source;
  wire [35:0] filter_auto_out_2_a_bits_address;
  wire [15:0] filter_auto_out_2_a_bits_mask;
  wire [127:0] filter_auto_out_2_a_bits_data;
  wire  filter_auto_out_2_a_bits_corrupt;
  wire  filter_auto_out_2_b_ready;
  wire  filter_auto_out_2_b_valid;
  wire [2:0] filter_auto_out_2_b_bits_opcode;
  wire [1:0] filter_auto_out_2_b_bits_param;
  wire [2:0] filter_auto_out_2_b_bits_size;
  wire [6:0] filter_auto_out_2_b_bits_source;
  wire [35:0] filter_auto_out_2_b_bits_address;
  wire [15:0] filter_auto_out_2_b_bits_mask;
  wire [127:0] filter_auto_out_2_b_bits_data;
  wire  filter_auto_out_2_b_bits_corrupt;
  wire  filter_auto_out_2_c_ready;
  wire  filter_auto_out_2_c_valid;
  wire [2:0] filter_auto_out_2_c_bits_opcode;
  wire [2:0] filter_auto_out_2_c_bits_param;
  wire [2:0] filter_auto_out_2_c_bits_size;
  wire [6:0] filter_auto_out_2_c_bits_source;
  wire [35:0] filter_auto_out_2_c_bits_address;
  wire [127:0] filter_auto_out_2_c_bits_data;
  wire  filter_auto_out_2_c_bits_corrupt;
  wire  filter_auto_out_2_d_ready;
  wire  filter_auto_out_2_d_valid;
  wire [2:0] filter_auto_out_2_d_bits_opcode;
  wire [1:0] filter_auto_out_2_d_bits_param;
  wire [2:0] filter_auto_out_2_d_bits_size;
  wire [6:0] filter_auto_out_2_d_bits_source;
  wire [2:0] filter_auto_out_2_d_bits_sink;
  wire  filter_auto_out_2_d_bits_denied;
  wire [127:0] filter_auto_out_2_d_bits_data;
  wire  filter_auto_out_2_d_bits_corrupt;
  wire  filter_auto_out_2_e_ready;
  wire  filter_auto_out_2_e_valid;
  wire [2:0] filter_auto_out_2_e_bits_sink;
  wire  filter_auto_out_1_a_ready;
  wire  filter_auto_out_1_a_valid;
  wire [2:0] filter_auto_out_1_a_bits_opcode;
  wire [2:0] filter_auto_out_1_a_bits_param;
  wire [2:0] filter_auto_out_1_a_bits_size;
  wire [6:0] filter_auto_out_1_a_bits_source;
  wire [35:0] filter_auto_out_1_a_bits_address;
  wire [15:0] filter_auto_out_1_a_bits_mask;
  wire [127:0] filter_auto_out_1_a_bits_data;
  wire  filter_auto_out_1_a_bits_corrupt;
  wire  filter_auto_out_1_b_ready;
  wire  filter_auto_out_1_b_valid;
  wire [2:0] filter_auto_out_1_b_bits_opcode;
  wire [1:0] filter_auto_out_1_b_bits_param;
  wire [2:0] filter_auto_out_1_b_bits_size;
  wire [6:0] filter_auto_out_1_b_bits_source;
  wire [35:0] filter_auto_out_1_b_bits_address;
  wire [15:0] filter_auto_out_1_b_bits_mask;
  wire [127:0] filter_auto_out_1_b_bits_data;
  wire  filter_auto_out_1_b_bits_corrupt;
  wire  filter_auto_out_1_c_ready;
  wire  filter_auto_out_1_c_valid;
  wire [2:0] filter_auto_out_1_c_bits_opcode;
  wire [2:0] filter_auto_out_1_c_bits_param;
  wire [2:0] filter_auto_out_1_c_bits_size;
  wire [6:0] filter_auto_out_1_c_bits_source;
  wire [35:0] filter_auto_out_1_c_bits_address;
  wire [127:0] filter_auto_out_1_c_bits_data;
  wire  filter_auto_out_1_c_bits_corrupt;
  wire  filter_auto_out_1_d_ready;
  wire  filter_auto_out_1_d_valid;
  wire [2:0] filter_auto_out_1_d_bits_opcode;
  wire [1:0] filter_auto_out_1_d_bits_param;
  wire [2:0] filter_auto_out_1_d_bits_size;
  wire [6:0] filter_auto_out_1_d_bits_source;
  wire [2:0] filter_auto_out_1_d_bits_sink;
  wire  filter_auto_out_1_d_bits_denied;
  wire [127:0] filter_auto_out_1_d_bits_data;
  wire  filter_auto_out_1_d_bits_corrupt;
  wire  filter_auto_out_1_e_ready;
  wire  filter_auto_out_1_e_valid;
  wire [2:0] filter_auto_out_1_e_bits_sink;
  wire  filter_auto_out_0_a_ready;
  wire  filter_auto_out_0_a_valid;
  wire [2:0] filter_auto_out_0_a_bits_opcode;
  wire [2:0] filter_auto_out_0_a_bits_param;
  wire [2:0] filter_auto_out_0_a_bits_size;
  wire [6:0] filter_auto_out_0_a_bits_source;
  wire [35:0] filter_auto_out_0_a_bits_address;
  wire [15:0] filter_auto_out_0_a_bits_mask;
  wire [127:0] filter_auto_out_0_a_bits_data;
  wire  filter_auto_out_0_a_bits_corrupt;
  wire  filter_auto_out_0_b_ready;
  wire  filter_auto_out_0_b_valid;
  wire [2:0] filter_auto_out_0_b_bits_opcode;
  wire [1:0] filter_auto_out_0_b_bits_param;
  wire [2:0] filter_auto_out_0_b_bits_size;
  wire [6:0] filter_auto_out_0_b_bits_source;
  wire [35:0] filter_auto_out_0_b_bits_address;
  wire [15:0] filter_auto_out_0_b_bits_mask;
  wire [127:0] filter_auto_out_0_b_bits_data;
  wire  filter_auto_out_0_b_bits_corrupt;
  wire  filter_auto_out_0_c_ready;
  wire  filter_auto_out_0_c_valid;
  wire [2:0] filter_auto_out_0_c_bits_opcode;
  wire [2:0] filter_auto_out_0_c_bits_param;
  wire [2:0] filter_auto_out_0_c_bits_size;
  wire [6:0] filter_auto_out_0_c_bits_source;
  wire [35:0] filter_auto_out_0_c_bits_address;
  wire [127:0] filter_auto_out_0_c_bits_data;
  wire  filter_auto_out_0_c_bits_corrupt;
  wire  filter_auto_out_0_d_ready;
  wire  filter_auto_out_0_d_valid;
  wire [2:0] filter_auto_out_0_d_bits_opcode;
  wire [1:0] filter_auto_out_0_d_bits_param;
  wire [2:0] filter_auto_out_0_d_bits_size;
  wire [6:0] filter_auto_out_0_d_bits_source;
  wire [2:0] filter_auto_out_0_d_bits_sink;
  wire  filter_auto_out_0_d_bits_denied;
  wire [127:0] filter_auto_out_0_d_bits_data;
  wire  filter_auto_out_0_d_bits_corrupt;
  wire  filter_auto_out_0_e_ready;
  wire  filter_auto_out_0_e_valid;
  wire [2:0] filter_auto_out_0_e_bits_sink;
  wire  ComposableCache_inner_TLBuffer_rf_reset; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_clock; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_reset; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_3_a_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_3_a_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_3_a_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_3_a_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_3_a_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_3_a_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_in_3_a_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_in_3_a_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_3_a_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_3_a_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_3_b_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_3_b_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_3_b_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_in_3_b_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_3_b_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_3_b_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_in_3_b_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_in_3_b_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_3_b_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_3_b_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_3_c_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_3_c_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_3_c_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_3_c_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_3_c_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_3_c_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_in_3_c_bits_address; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_3_c_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_3_c_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_3_d_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_3_d_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_3_d_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_in_3_d_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_3_d_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_3_d_bits_source; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_3_d_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_3_d_bits_denied; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_3_d_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_3_d_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_3_e_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_3_e_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_3_e_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_2_a_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_2_a_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_2_a_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_2_a_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_2_a_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_2_a_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_in_2_a_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_in_2_a_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_2_a_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_2_a_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_2_b_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_2_b_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_2_b_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_in_2_b_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_2_b_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_2_b_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_in_2_b_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_in_2_b_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_2_b_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_2_b_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_2_c_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_2_c_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_2_c_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_2_c_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_2_c_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_2_c_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_in_2_c_bits_address; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_2_c_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_2_c_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_2_d_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_2_d_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_2_d_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_in_2_d_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_2_d_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_2_d_bits_source; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_2_d_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_2_d_bits_denied; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_2_d_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_2_d_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_2_e_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_2_e_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_2_e_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_1_a_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_1_a_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_1_a_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_1_a_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_1_a_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_1_a_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_in_1_a_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_in_1_a_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_1_a_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_1_a_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_1_b_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_1_b_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_1_b_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_in_1_b_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_1_b_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_1_b_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_in_1_b_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_in_1_b_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_1_b_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_1_b_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_1_c_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_1_c_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_1_c_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_1_c_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_1_c_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_1_c_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_in_1_c_bits_address; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_1_c_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_1_c_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_1_d_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_1_d_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_1_d_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_in_1_d_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_1_d_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_1_d_bits_source; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_1_d_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_1_d_bits_denied; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_1_d_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_1_d_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_1_e_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_1_e_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_1_e_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_0_a_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_0_a_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_0_a_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_0_a_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_0_a_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_0_a_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_in_0_a_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_in_0_a_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_0_a_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_0_a_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_0_b_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_0_b_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_0_b_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_in_0_b_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_0_b_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_0_b_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_in_0_b_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_in_0_b_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_0_b_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_0_b_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_0_c_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_0_c_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_0_c_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_0_c_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_0_c_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_0_c_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_in_0_c_bits_address; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_0_c_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_0_c_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_0_d_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_0_d_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_0_d_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_in_0_d_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_0_d_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_in_0_d_bits_source; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_0_d_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_0_d_bits_denied; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_in_0_d_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_0_d_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_0_e_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_in_0_e_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_in_0_e_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_3_a_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_3_a_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_3_a_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_3_a_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_3_a_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_3_a_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_out_3_a_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_out_3_a_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_3_a_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_3_a_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_3_b_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_3_b_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_3_b_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_out_3_b_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_3_b_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_3_b_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_out_3_b_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_out_3_b_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_3_b_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_3_b_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_3_c_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_3_c_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_3_c_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_3_c_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_3_c_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_3_c_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_out_3_c_bits_address; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_3_c_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_3_c_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_3_d_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_3_d_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_3_d_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_out_3_d_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_3_d_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_3_d_bits_source; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_3_d_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_3_d_bits_denied; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_3_d_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_3_d_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_3_e_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_3_e_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_3_e_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_2_a_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_2_a_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_2_a_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_2_a_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_2_a_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_2_a_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_out_2_a_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_out_2_a_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_2_a_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_2_a_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_2_b_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_2_b_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_2_b_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_out_2_b_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_2_b_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_2_b_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_out_2_b_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_out_2_b_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_2_b_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_2_b_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_2_c_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_2_c_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_2_c_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_2_c_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_2_c_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_2_c_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_out_2_c_bits_address; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_2_c_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_2_c_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_2_d_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_2_d_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_2_d_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_out_2_d_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_2_d_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_2_d_bits_source; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_2_d_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_2_d_bits_denied; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_2_d_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_2_d_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_2_e_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_2_e_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_2_e_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_1_a_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_1_a_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_1_a_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_1_a_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_1_a_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_1_a_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_out_1_a_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_out_1_a_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_1_a_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_1_a_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_1_b_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_1_b_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_1_b_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_out_1_b_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_1_b_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_1_b_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_out_1_b_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_out_1_b_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_1_b_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_1_b_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_1_c_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_1_c_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_1_c_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_1_c_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_1_c_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_1_c_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_out_1_c_bits_address; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_1_c_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_1_c_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_1_d_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_1_d_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_1_d_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_out_1_d_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_1_d_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_1_d_bits_source; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_1_d_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_1_d_bits_denied; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_1_d_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_1_d_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_1_e_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_1_e_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_1_e_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_0_a_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_0_a_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_0_a_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_0_a_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_0_a_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_0_a_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_out_0_a_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_out_0_a_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_0_a_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_0_a_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_0_b_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_0_b_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_0_b_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_out_0_b_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_0_b_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_0_b_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_out_0_b_bits_address; // @[Parameters.scala 57:69]
  wire [15:0] ComposableCache_inner_TLBuffer_auto_out_0_b_bits_mask; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_0_b_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_0_b_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_0_c_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_0_c_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_0_c_bits_opcode; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_0_c_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_0_c_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_0_c_bits_source; // @[Parameters.scala 57:69]
  wire [35:0] ComposableCache_inner_TLBuffer_auto_out_0_c_bits_address; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_0_c_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_0_c_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_0_d_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_0_d_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_0_d_bits_opcode; // @[Parameters.scala 57:69]
  wire [1:0] ComposableCache_inner_TLBuffer_auto_out_0_d_bits_param; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_0_d_bits_size; // @[Parameters.scala 57:69]
  wire [6:0] ComposableCache_inner_TLBuffer_auto_out_0_d_bits_source; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_0_d_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_0_d_bits_denied; // @[Parameters.scala 57:69]
  wire [127:0] ComposableCache_inner_TLBuffer_auto_out_0_d_bits_data; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_0_d_bits_corrupt; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_0_e_ready; // @[Parameters.scala 57:69]
  wire  ComposableCache_inner_TLBuffer_auto_out_0_e_valid; // @[Parameters.scala 57:69]
  wire [2:0] ComposableCache_inner_TLBuffer_auto_out_0_e_bits_sink; // @[Parameters.scala 57:69]
  wire  ComposableCache_outer_TLBuffer_auto_in_3_a_ready;
  wire  ComposableCache_outer_TLBuffer_auto_in_3_a_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_3_a_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_3_a_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_3_a_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_in_3_a_bits_address;
  wire  ComposableCache_outer_TLBuffer_auto_in_3_c_ready;
  wire  ComposableCache_outer_TLBuffer_auto_in_3_c_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_3_c_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_3_c_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_3_c_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_in_3_c_bits_address;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_in_3_c_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_in_3_d_ready;
  wire  ComposableCache_outer_TLBuffer_auto_in_3_d_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_3_d_bits_opcode;
  wire [1:0] ComposableCache_outer_TLBuffer_auto_in_3_d_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_3_d_bits_size;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_3_d_bits_source;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_3_d_bits_sink;
  wire  ComposableCache_outer_TLBuffer_auto_in_3_d_bits_denied;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_in_3_d_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_in_3_d_bits_corrupt;
  wire  ComposableCache_outer_TLBuffer_auto_in_3_e_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_3_e_bits_sink;
  wire  ComposableCache_outer_TLBuffer_auto_in_2_a_ready;
  wire  ComposableCache_outer_TLBuffer_auto_in_2_a_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_2_a_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_2_a_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_2_a_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_in_2_a_bits_address;
  wire  ComposableCache_outer_TLBuffer_auto_in_2_c_ready;
  wire  ComposableCache_outer_TLBuffer_auto_in_2_c_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_2_c_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_2_c_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_2_c_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_in_2_c_bits_address;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_in_2_c_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_in_2_d_ready;
  wire  ComposableCache_outer_TLBuffer_auto_in_2_d_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_2_d_bits_opcode;
  wire [1:0] ComposableCache_outer_TLBuffer_auto_in_2_d_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_2_d_bits_size;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_2_d_bits_source;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_2_d_bits_sink;
  wire  ComposableCache_outer_TLBuffer_auto_in_2_d_bits_denied;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_in_2_d_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_in_2_d_bits_corrupt;
  wire  ComposableCache_outer_TLBuffer_auto_in_2_e_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_2_e_bits_sink;
  wire  ComposableCache_outer_TLBuffer_auto_in_1_a_ready;
  wire  ComposableCache_outer_TLBuffer_auto_in_1_a_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_1_a_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_1_a_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_1_a_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_in_1_a_bits_address;
  wire  ComposableCache_outer_TLBuffer_auto_in_1_c_ready;
  wire  ComposableCache_outer_TLBuffer_auto_in_1_c_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_1_c_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_1_c_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_1_c_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_in_1_c_bits_address;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_in_1_c_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_in_1_d_ready;
  wire  ComposableCache_outer_TLBuffer_auto_in_1_d_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_1_d_bits_opcode;
  wire [1:0] ComposableCache_outer_TLBuffer_auto_in_1_d_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_1_d_bits_size;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_1_d_bits_source;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_1_d_bits_sink;
  wire  ComposableCache_outer_TLBuffer_auto_in_1_d_bits_denied;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_in_1_d_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_in_1_d_bits_corrupt;
  wire  ComposableCache_outer_TLBuffer_auto_in_1_e_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_1_e_bits_sink;
  wire  ComposableCache_outer_TLBuffer_auto_in_0_a_ready;
  wire  ComposableCache_outer_TLBuffer_auto_in_0_a_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_0_a_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_0_a_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_0_a_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_in_0_a_bits_address;
  wire  ComposableCache_outer_TLBuffer_auto_in_0_c_ready;
  wire  ComposableCache_outer_TLBuffer_auto_in_0_c_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_0_c_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_0_c_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_0_c_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_in_0_c_bits_address;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_in_0_c_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_in_0_d_ready;
  wire  ComposableCache_outer_TLBuffer_auto_in_0_d_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_0_d_bits_opcode;
  wire [1:0] ComposableCache_outer_TLBuffer_auto_in_0_d_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_0_d_bits_size;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_0_d_bits_source;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_0_d_bits_sink;
  wire  ComposableCache_outer_TLBuffer_auto_in_0_d_bits_denied;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_in_0_d_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_in_0_d_bits_corrupt;
  wire  ComposableCache_outer_TLBuffer_auto_in_0_e_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_in_0_e_bits_sink;
  wire  ComposableCache_outer_TLBuffer_auto_out_3_a_ready;
  wire  ComposableCache_outer_TLBuffer_auto_out_3_a_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_3_a_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_3_a_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_3_a_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_out_3_a_bits_address;
  wire  ComposableCache_outer_TLBuffer_auto_out_3_c_ready;
  wire  ComposableCache_outer_TLBuffer_auto_out_3_c_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_3_c_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_3_c_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_3_c_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_out_3_c_bits_address;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_out_3_c_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_out_3_d_ready;
  wire  ComposableCache_outer_TLBuffer_auto_out_3_d_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_3_d_bits_opcode;
  wire [1:0] ComposableCache_outer_TLBuffer_auto_out_3_d_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_3_d_bits_size;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_3_d_bits_source;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_3_d_bits_sink;
  wire  ComposableCache_outer_TLBuffer_auto_out_3_d_bits_denied;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_out_3_d_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_out_3_d_bits_corrupt;
  wire  ComposableCache_outer_TLBuffer_auto_out_3_e_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_3_e_bits_sink;
  wire  ComposableCache_outer_TLBuffer_auto_out_2_a_ready;
  wire  ComposableCache_outer_TLBuffer_auto_out_2_a_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_2_a_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_2_a_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_2_a_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_out_2_a_bits_address;
  wire  ComposableCache_outer_TLBuffer_auto_out_2_c_ready;
  wire  ComposableCache_outer_TLBuffer_auto_out_2_c_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_2_c_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_2_c_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_2_c_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_out_2_c_bits_address;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_out_2_c_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_out_2_d_ready;
  wire  ComposableCache_outer_TLBuffer_auto_out_2_d_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_2_d_bits_opcode;
  wire [1:0] ComposableCache_outer_TLBuffer_auto_out_2_d_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_2_d_bits_size;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_2_d_bits_source;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_2_d_bits_sink;
  wire  ComposableCache_outer_TLBuffer_auto_out_2_d_bits_denied;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_out_2_d_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_out_2_d_bits_corrupt;
  wire  ComposableCache_outer_TLBuffer_auto_out_2_e_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_2_e_bits_sink;
  wire  ComposableCache_outer_TLBuffer_auto_out_1_a_ready;
  wire  ComposableCache_outer_TLBuffer_auto_out_1_a_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_1_a_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_1_a_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_1_a_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_out_1_a_bits_address;
  wire  ComposableCache_outer_TLBuffer_auto_out_1_c_ready;
  wire  ComposableCache_outer_TLBuffer_auto_out_1_c_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_1_c_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_1_c_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_1_c_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_out_1_c_bits_address;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_out_1_c_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_out_1_d_ready;
  wire  ComposableCache_outer_TLBuffer_auto_out_1_d_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_1_d_bits_opcode;
  wire [1:0] ComposableCache_outer_TLBuffer_auto_out_1_d_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_1_d_bits_size;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_1_d_bits_source;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_1_d_bits_sink;
  wire  ComposableCache_outer_TLBuffer_auto_out_1_d_bits_denied;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_out_1_d_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_out_1_d_bits_corrupt;
  wire  ComposableCache_outer_TLBuffer_auto_out_1_e_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_1_e_bits_sink;
  wire  ComposableCache_outer_TLBuffer_auto_out_0_a_ready;
  wire  ComposableCache_outer_TLBuffer_auto_out_0_a_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_0_a_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_0_a_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_0_a_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_out_0_a_bits_address;
  wire  ComposableCache_outer_TLBuffer_auto_out_0_c_ready;
  wire  ComposableCache_outer_TLBuffer_auto_out_0_c_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_0_c_bits_opcode;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_0_c_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_0_c_bits_source;
  wire [35:0] ComposableCache_outer_TLBuffer_auto_out_0_c_bits_address;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_out_0_c_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_out_0_d_ready;
  wire  ComposableCache_outer_TLBuffer_auto_out_0_d_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_0_d_bits_opcode;
  wire [1:0] ComposableCache_outer_TLBuffer_auto_out_0_d_bits_param;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_0_d_bits_size;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_0_d_bits_source;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_0_d_bits_sink;
  wire  ComposableCache_outer_TLBuffer_auto_out_0_d_bits_denied;
  wire [127:0] ComposableCache_outer_TLBuffer_auto_out_0_d_bits_data;
  wire  ComposableCache_outer_TLBuffer_auto_out_0_d_bits_corrupt;
  wire  ComposableCache_outer_TLBuffer_auto_out_0_e_valid;
  wire [2:0] ComposableCache_outer_TLBuffer_auto_out_0_e_bits_sink;
  wire  cork_rf_reset; // @[Configs.scala 246:26]
  wire  cork_clock; // @[Configs.scala 246:26]
  wire  cork_reset; // @[Configs.scala 246:26]
  wire  cork_auto_in_3_a_ready; // @[Configs.scala 246:26]
  wire  cork_auto_in_3_a_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_3_a_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_3_a_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_3_a_bits_source; // @[Configs.scala 246:26]
  wire [35:0] cork_auto_in_3_a_bits_address; // @[Configs.scala 246:26]
  wire  cork_auto_in_3_c_ready; // @[Configs.scala 246:26]
  wire  cork_auto_in_3_c_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_3_c_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_3_c_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_3_c_bits_source; // @[Configs.scala 246:26]
  wire [35:0] cork_auto_in_3_c_bits_address; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_in_3_c_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_in_3_d_ready; // @[Configs.scala 246:26]
  wire  cork_auto_in_3_d_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_3_d_bits_opcode; // @[Configs.scala 246:26]
  wire [1:0] cork_auto_in_3_d_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_3_d_bits_size; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_3_d_bits_source; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_3_d_bits_sink; // @[Configs.scala 246:26]
  wire  cork_auto_in_3_d_bits_denied; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_in_3_d_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_in_3_d_bits_corrupt; // @[Configs.scala 246:26]
  wire  cork_auto_in_3_e_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_3_e_bits_sink; // @[Configs.scala 246:26]
  wire  cork_auto_in_2_a_ready; // @[Configs.scala 246:26]
  wire  cork_auto_in_2_a_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_2_a_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_2_a_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_2_a_bits_source; // @[Configs.scala 246:26]
  wire [35:0] cork_auto_in_2_a_bits_address; // @[Configs.scala 246:26]
  wire  cork_auto_in_2_c_ready; // @[Configs.scala 246:26]
  wire  cork_auto_in_2_c_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_2_c_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_2_c_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_2_c_bits_source; // @[Configs.scala 246:26]
  wire [35:0] cork_auto_in_2_c_bits_address; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_in_2_c_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_in_2_d_ready; // @[Configs.scala 246:26]
  wire  cork_auto_in_2_d_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_2_d_bits_opcode; // @[Configs.scala 246:26]
  wire [1:0] cork_auto_in_2_d_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_2_d_bits_size; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_2_d_bits_source; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_2_d_bits_sink; // @[Configs.scala 246:26]
  wire  cork_auto_in_2_d_bits_denied; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_in_2_d_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_in_2_d_bits_corrupt; // @[Configs.scala 246:26]
  wire  cork_auto_in_2_e_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_2_e_bits_sink; // @[Configs.scala 246:26]
  wire  cork_auto_in_1_a_ready; // @[Configs.scala 246:26]
  wire  cork_auto_in_1_a_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_1_a_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_1_a_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_1_a_bits_source; // @[Configs.scala 246:26]
  wire [35:0] cork_auto_in_1_a_bits_address; // @[Configs.scala 246:26]
  wire  cork_auto_in_1_c_ready; // @[Configs.scala 246:26]
  wire  cork_auto_in_1_c_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_1_c_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_1_c_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_1_c_bits_source; // @[Configs.scala 246:26]
  wire [35:0] cork_auto_in_1_c_bits_address; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_in_1_c_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_in_1_d_ready; // @[Configs.scala 246:26]
  wire  cork_auto_in_1_d_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_1_d_bits_opcode; // @[Configs.scala 246:26]
  wire [1:0] cork_auto_in_1_d_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_1_d_bits_size; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_1_d_bits_source; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_1_d_bits_sink; // @[Configs.scala 246:26]
  wire  cork_auto_in_1_d_bits_denied; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_in_1_d_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_in_1_d_bits_corrupt; // @[Configs.scala 246:26]
  wire  cork_auto_in_1_e_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_1_e_bits_sink; // @[Configs.scala 246:26]
  wire  cork_auto_in_0_a_ready; // @[Configs.scala 246:26]
  wire  cork_auto_in_0_a_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_0_a_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_0_a_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_0_a_bits_source; // @[Configs.scala 246:26]
  wire [35:0] cork_auto_in_0_a_bits_address; // @[Configs.scala 246:26]
  wire  cork_auto_in_0_c_ready; // @[Configs.scala 246:26]
  wire  cork_auto_in_0_c_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_0_c_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_0_c_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_0_c_bits_source; // @[Configs.scala 246:26]
  wire [35:0] cork_auto_in_0_c_bits_address; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_in_0_c_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_in_0_d_ready; // @[Configs.scala 246:26]
  wire  cork_auto_in_0_d_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_0_d_bits_opcode; // @[Configs.scala 246:26]
  wire [1:0] cork_auto_in_0_d_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_0_d_bits_size; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_0_d_bits_source; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_0_d_bits_sink; // @[Configs.scala 246:26]
  wire  cork_auto_in_0_d_bits_denied; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_in_0_d_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_in_0_d_bits_corrupt; // @[Configs.scala 246:26]
  wire  cork_auto_in_0_e_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_in_0_e_bits_sink; // @[Configs.scala 246:26]
  wire  cork_auto_out_3_a_ready; // @[Configs.scala 246:26]
  wire  cork_auto_out_3_a_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_3_a_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_3_a_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_3_a_bits_size; // @[Configs.scala 246:26]
  wire [3:0] cork_auto_out_3_a_bits_source; // @[Configs.scala 246:26]
  wire [35:0] cork_auto_out_3_a_bits_address; // @[Configs.scala 246:26]
  wire  cork_auto_out_3_a_bits_user_amba_prot_bufferable; // @[Configs.scala 246:26]
  wire  cork_auto_out_3_a_bits_user_amba_prot_modifiable; // @[Configs.scala 246:26]
  wire  cork_auto_out_3_a_bits_user_amba_prot_readalloc; // @[Configs.scala 246:26]
  wire  cork_auto_out_3_a_bits_user_amba_prot_writealloc; // @[Configs.scala 246:26]
  wire  cork_auto_out_3_a_bits_user_amba_prot_privileged; // @[Configs.scala 246:26]
  wire  cork_auto_out_3_a_bits_user_amba_prot_secure; // @[Configs.scala 246:26]
  wire [15:0] cork_auto_out_3_a_bits_mask; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_out_3_a_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_out_3_d_ready; // @[Configs.scala 246:26]
  wire  cork_auto_out_3_d_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_3_d_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_3_d_bits_size; // @[Configs.scala 246:26]
  wire [3:0] cork_auto_out_3_d_bits_source; // @[Configs.scala 246:26]
  wire  cork_auto_out_3_d_bits_denied; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_out_3_d_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_out_3_d_bits_corrupt; // @[Configs.scala 246:26]
  wire  cork_auto_out_2_a_ready; // @[Configs.scala 246:26]
  wire  cork_auto_out_2_a_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_2_a_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_2_a_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_2_a_bits_size; // @[Configs.scala 246:26]
  wire [3:0] cork_auto_out_2_a_bits_source; // @[Configs.scala 246:26]
  wire [35:0] cork_auto_out_2_a_bits_address; // @[Configs.scala 246:26]
  wire  cork_auto_out_2_a_bits_user_amba_prot_bufferable; // @[Configs.scala 246:26]
  wire  cork_auto_out_2_a_bits_user_amba_prot_modifiable; // @[Configs.scala 246:26]
  wire  cork_auto_out_2_a_bits_user_amba_prot_readalloc; // @[Configs.scala 246:26]
  wire  cork_auto_out_2_a_bits_user_amba_prot_writealloc; // @[Configs.scala 246:26]
  wire  cork_auto_out_2_a_bits_user_amba_prot_privileged; // @[Configs.scala 246:26]
  wire  cork_auto_out_2_a_bits_user_amba_prot_secure; // @[Configs.scala 246:26]
  wire [15:0] cork_auto_out_2_a_bits_mask; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_out_2_a_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_out_2_d_ready; // @[Configs.scala 246:26]
  wire  cork_auto_out_2_d_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_2_d_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_2_d_bits_size; // @[Configs.scala 246:26]
  wire [3:0] cork_auto_out_2_d_bits_source; // @[Configs.scala 246:26]
  wire  cork_auto_out_2_d_bits_denied; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_out_2_d_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_out_2_d_bits_corrupt; // @[Configs.scala 246:26]
  wire  cork_auto_out_1_a_ready; // @[Configs.scala 246:26]
  wire  cork_auto_out_1_a_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_1_a_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_1_a_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_1_a_bits_size; // @[Configs.scala 246:26]
  wire [3:0] cork_auto_out_1_a_bits_source; // @[Configs.scala 246:26]
  wire [35:0] cork_auto_out_1_a_bits_address; // @[Configs.scala 246:26]
  wire  cork_auto_out_1_a_bits_user_amba_prot_bufferable; // @[Configs.scala 246:26]
  wire  cork_auto_out_1_a_bits_user_amba_prot_modifiable; // @[Configs.scala 246:26]
  wire  cork_auto_out_1_a_bits_user_amba_prot_readalloc; // @[Configs.scala 246:26]
  wire  cork_auto_out_1_a_bits_user_amba_prot_writealloc; // @[Configs.scala 246:26]
  wire  cork_auto_out_1_a_bits_user_amba_prot_privileged; // @[Configs.scala 246:26]
  wire  cork_auto_out_1_a_bits_user_amba_prot_secure; // @[Configs.scala 246:26]
  wire [15:0] cork_auto_out_1_a_bits_mask; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_out_1_a_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_out_1_d_ready; // @[Configs.scala 246:26]
  wire  cork_auto_out_1_d_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_1_d_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_1_d_bits_size; // @[Configs.scala 246:26]
  wire [3:0] cork_auto_out_1_d_bits_source; // @[Configs.scala 246:26]
  wire  cork_auto_out_1_d_bits_denied; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_out_1_d_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_out_1_d_bits_corrupt; // @[Configs.scala 246:26]
  wire  cork_auto_out_0_a_ready; // @[Configs.scala 246:26]
  wire  cork_auto_out_0_a_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_0_a_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_0_a_bits_param; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_0_a_bits_size; // @[Configs.scala 246:26]
  wire [3:0] cork_auto_out_0_a_bits_source; // @[Configs.scala 246:26]
  wire [35:0] cork_auto_out_0_a_bits_address; // @[Configs.scala 246:26]
  wire  cork_auto_out_0_a_bits_user_amba_prot_bufferable; // @[Configs.scala 246:26]
  wire  cork_auto_out_0_a_bits_user_amba_prot_modifiable; // @[Configs.scala 246:26]
  wire  cork_auto_out_0_a_bits_user_amba_prot_readalloc; // @[Configs.scala 246:26]
  wire  cork_auto_out_0_a_bits_user_amba_prot_writealloc; // @[Configs.scala 246:26]
  wire  cork_auto_out_0_a_bits_user_amba_prot_privileged; // @[Configs.scala 246:26]
  wire  cork_auto_out_0_a_bits_user_amba_prot_secure; // @[Configs.scala 246:26]
  wire [15:0] cork_auto_out_0_a_bits_mask; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_out_0_a_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_out_0_d_ready; // @[Configs.scala 246:26]
  wire  cork_auto_out_0_d_valid; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_0_d_bits_opcode; // @[Configs.scala 246:26]
  wire [2:0] cork_auto_out_0_d_bits_size; // @[Configs.scala 246:26]
  wire [3:0] cork_auto_out_0_d_bits_source; // @[Configs.scala 246:26]
  wire  cork_auto_out_0_d_bits_denied; // @[Configs.scala 246:26]
  wire [127:0] cork_auto_out_0_d_bits_data; // @[Configs.scala 246:26]
  wire  cork_auto_out_0_d_bits_corrupt; // @[Configs.scala 246:26]
  wire  coherent_jbar_auto_in_3_a_ready;
  wire  coherent_jbar_auto_in_3_a_valid;
  wire [2:0] coherent_jbar_auto_in_3_a_bits_opcode;
  wire [2:0] coherent_jbar_auto_in_3_a_bits_param;
  wire [2:0] coherent_jbar_auto_in_3_a_bits_size;
  wire [6:0] coherent_jbar_auto_in_3_a_bits_source;
  wire [35:0] coherent_jbar_auto_in_3_a_bits_address;
  wire [15:0] coherent_jbar_auto_in_3_a_bits_mask;
  wire [127:0] coherent_jbar_auto_in_3_a_bits_data;
  wire  coherent_jbar_auto_in_3_a_bits_corrupt;
  wire  coherent_jbar_auto_in_3_b_ready;
  wire  coherent_jbar_auto_in_3_b_valid;
  wire [2:0] coherent_jbar_auto_in_3_b_bits_opcode;
  wire [1:0] coherent_jbar_auto_in_3_b_bits_param;
  wire [2:0] coherent_jbar_auto_in_3_b_bits_size;
  wire [6:0] coherent_jbar_auto_in_3_b_bits_source;
  wire [35:0] coherent_jbar_auto_in_3_b_bits_address;
  wire [15:0] coherent_jbar_auto_in_3_b_bits_mask;
  wire [127:0] coherent_jbar_auto_in_3_b_bits_data;
  wire  coherent_jbar_auto_in_3_b_bits_corrupt;
  wire  coherent_jbar_auto_in_3_c_ready;
  wire  coherent_jbar_auto_in_3_c_valid;
  wire [2:0] coherent_jbar_auto_in_3_c_bits_opcode;
  wire [2:0] coherent_jbar_auto_in_3_c_bits_param;
  wire [2:0] coherent_jbar_auto_in_3_c_bits_size;
  wire [6:0] coherent_jbar_auto_in_3_c_bits_source;
  wire [35:0] coherent_jbar_auto_in_3_c_bits_address;
  wire [127:0] coherent_jbar_auto_in_3_c_bits_data;
  wire  coherent_jbar_auto_in_3_c_bits_corrupt;
  wire  coherent_jbar_auto_in_3_d_ready;
  wire  coherent_jbar_auto_in_3_d_valid;
  wire [2:0] coherent_jbar_auto_in_3_d_bits_opcode;
  wire [1:0] coherent_jbar_auto_in_3_d_bits_param;
  wire [2:0] coherent_jbar_auto_in_3_d_bits_size;
  wire [6:0] coherent_jbar_auto_in_3_d_bits_source;
  wire [2:0] coherent_jbar_auto_in_3_d_bits_sink;
  wire  coherent_jbar_auto_in_3_d_bits_denied;
  wire [127:0] coherent_jbar_auto_in_3_d_bits_data;
  wire  coherent_jbar_auto_in_3_d_bits_corrupt;
  wire  coherent_jbar_auto_in_3_e_ready;
  wire  coherent_jbar_auto_in_3_e_valid;
  wire [2:0] coherent_jbar_auto_in_3_e_bits_sink;
  wire  coherent_jbar_auto_in_2_a_ready;
  wire  coherent_jbar_auto_in_2_a_valid;
  wire [2:0] coherent_jbar_auto_in_2_a_bits_opcode;
  wire [2:0] coherent_jbar_auto_in_2_a_bits_param;
  wire [2:0] coherent_jbar_auto_in_2_a_bits_size;
  wire [6:0] coherent_jbar_auto_in_2_a_bits_source;
  wire [35:0] coherent_jbar_auto_in_2_a_bits_address;
  wire [15:0] coherent_jbar_auto_in_2_a_bits_mask;
  wire [127:0] coherent_jbar_auto_in_2_a_bits_data;
  wire  coherent_jbar_auto_in_2_a_bits_corrupt;
  wire  coherent_jbar_auto_in_2_b_ready;
  wire  coherent_jbar_auto_in_2_b_valid;
  wire [2:0] coherent_jbar_auto_in_2_b_bits_opcode;
  wire [1:0] coherent_jbar_auto_in_2_b_bits_param;
  wire [2:0] coherent_jbar_auto_in_2_b_bits_size;
  wire [6:0] coherent_jbar_auto_in_2_b_bits_source;
  wire [35:0] coherent_jbar_auto_in_2_b_bits_address;
  wire [15:0] coherent_jbar_auto_in_2_b_bits_mask;
  wire [127:0] coherent_jbar_auto_in_2_b_bits_data;
  wire  coherent_jbar_auto_in_2_b_bits_corrupt;
  wire  coherent_jbar_auto_in_2_c_ready;
  wire  coherent_jbar_auto_in_2_c_valid;
  wire [2:0] coherent_jbar_auto_in_2_c_bits_opcode;
  wire [2:0] coherent_jbar_auto_in_2_c_bits_param;
  wire [2:0] coherent_jbar_auto_in_2_c_bits_size;
  wire [6:0] coherent_jbar_auto_in_2_c_bits_source;
  wire [35:0] coherent_jbar_auto_in_2_c_bits_address;
  wire [127:0] coherent_jbar_auto_in_2_c_bits_data;
  wire  coherent_jbar_auto_in_2_c_bits_corrupt;
  wire  coherent_jbar_auto_in_2_d_ready;
  wire  coherent_jbar_auto_in_2_d_valid;
  wire [2:0] coherent_jbar_auto_in_2_d_bits_opcode;
  wire [1:0] coherent_jbar_auto_in_2_d_bits_param;
  wire [2:0] coherent_jbar_auto_in_2_d_bits_size;
  wire [6:0] coherent_jbar_auto_in_2_d_bits_source;
  wire [2:0] coherent_jbar_auto_in_2_d_bits_sink;
  wire  coherent_jbar_auto_in_2_d_bits_denied;
  wire [127:0] coherent_jbar_auto_in_2_d_bits_data;
  wire  coherent_jbar_auto_in_2_d_bits_corrupt;
  wire  coherent_jbar_auto_in_2_e_ready;
  wire  coherent_jbar_auto_in_2_e_valid;
  wire [2:0] coherent_jbar_auto_in_2_e_bits_sink;
  wire  coherent_jbar_auto_in_1_a_ready;
  wire  coherent_jbar_auto_in_1_a_valid;
  wire [2:0] coherent_jbar_auto_in_1_a_bits_opcode;
  wire [2:0] coherent_jbar_auto_in_1_a_bits_param;
  wire [2:0] coherent_jbar_auto_in_1_a_bits_size;
  wire [6:0] coherent_jbar_auto_in_1_a_bits_source;
  wire [35:0] coherent_jbar_auto_in_1_a_bits_address;
  wire [15:0] coherent_jbar_auto_in_1_a_bits_mask;
  wire [127:0] coherent_jbar_auto_in_1_a_bits_data;
  wire  coherent_jbar_auto_in_1_a_bits_corrupt;
  wire  coherent_jbar_auto_in_1_b_ready;
  wire  coherent_jbar_auto_in_1_b_valid;
  wire [2:0] coherent_jbar_auto_in_1_b_bits_opcode;
  wire [1:0] coherent_jbar_auto_in_1_b_bits_param;
  wire [2:0] coherent_jbar_auto_in_1_b_bits_size;
  wire [6:0] coherent_jbar_auto_in_1_b_bits_source;
  wire [35:0] coherent_jbar_auto_in_1_b_bits_address;
  wire [15:0] coherent_jbar_auto_in_1_b_bits_mask;
  wire [127:0] coherent_jbar_auto_in_1_b_bits_data;
  wire  coherent_jbar_auto_in_1_b_bits_corrupt;
  wire  coherent_jbar_auto_in_1_c_ready;
  wire  coherent_jbar_auto_in_1_c_valid;
  wire [2:0] coherent_jbar_auto_in_1_c_bits_opcode;
  wire [2:0] coherent_jbar_auto_in_1_c_bits_param;
  wire [2:0] coherent_jbar_auto_in_1_c_bits_size;
  wire [6:0] coherent_jbar_auto_in_1_c_bits_source;
  wire [35:0] coherent_jbar_auto_in_1_c_bits_address;
  wire [127:0] coherent_jbar_auto_in_1_c_bits_data;
  wire  coherent_jbar_auto_in_1_c_bits_corrupt;
  wire  coherent_jbar_auto_in_1_d_ready;
  wire  coherent_jbar_auto_in_1_d_valid;
  wire [2:0] coherent_jbar_auto_in_1_d_bits_opcode;
  wire [1:0] coherent_jbar_auto_in_1_d_bits_param;
  wire [2:0] coherent_jbar_auto_in_1_d_bits_size;
  wire [6:0] coherent_jbar_auto_in_1_d_bits_source;
  wire [2:0] coherent_jbar_auto_in_1_d_bits_sink;
  wire  coherent_jbar_auto_in_1_d_bits_denied;
  wire [127:0] coherent_jbar_auto_in_1_d_bits_data;
  wire  coherent_jbar_auto_in_1_d_bits_corrupt;
  wire  coherent_jbar_auto_in_1_e_ready;
  wire  coherent_jbar_auto_in_1_e_valid;
  wire [2:0] coherent_jbar_auto_in_1_e_bits_sink;
  wire  coherent_jbar_auto_in_0_a_ready;
  wire  coherent_jbar_auto_in_0_a_valid;
  wire [2:0] coherent_jbar_auto_in_0_a_bits_opcode;
  wire [2:0] coherent_jbar_auto_in_0_a_bits_param;
  wire [2:0] coherent_jbar_auto_in_0_a_bits_size;
  wire [6:0] coherent_jbar_auto_in_0_a_bits_source;
  wire [35:0] coherent_jbar_auto_in_0_a_bits_address;
  wire [15:0] coherent_jbar_auto_in_0_a_bits_mask;
  wire [127:0] coherent_jbar_auto_in_0_a_bits_data;
  wire  coherent_jbar_auto_in_0_a_bits_corrupt;
  wire  coherent_jbar_auto_in_0_b_ready;
  wire  coherent_jbar_auto_in_0_b_valid;
  wire [2:0] coherent_jbar_auto_in_0_b_bits_opcode;
  wire [1:0] coherent_jbar_auto_in_0_b_bits_param;
  wire [2:0] coherent_jbar_auto_in_0_b_bits_size;
  wire [6:0] coherent_jbar_auto_in_0_b_bits_source;
  wire [35:0] coherent_jbar_auto_in_0_b_bits_address;
  wire [15:0] coherent_jbar_auto_in_0_b_bits_mask;
  wire [127:0] coherent_jbar_auto_in_0_b_bits_data;
  wire  coherent_jbar_auto_in_0_b_bits_corrupt;
  wire  coherent_jbar_auto_in_0_c_ready;
  wire  coherent_jbar_auto_in_0_c_valid;
  wire [2:0] coherent_jbar_auto_in_0_c_bits_opcode;
  wire [2:0] coherent_jbar_auto_in_0_c_bits_param;
  wire [2:0] coherent_jbar_auto_in_0_c_bits_size;
  wire [6:0] coherent_jbar_auto_in_0_c_bits_source;
  wire [35:0] coherent_jbar_auto_in_0_c_bits_address;
  wire [127:0] coherent_jbar_auto_in_0_c_bits_data;
  wire  coherent_jbar_auto_in_0_c_bits_corrupt;
  wire  coherent_jbar_auto_in_0_d_ready;
  wire  coherent_jbar_auto_in_0_d_valid;
  wire [2:0] coherent_jbar_auto_in_0_d_bits_opcode;
  wire [1:0] coherent_jbar_auto_in_0_d_bits_param;
  wire [2:0] coherent_jbar_auto_in_0_d_bits_size;
  wire [6:0] coherent_jbar_auto_in_0_d_bits_source;
  wire [2:0] coherent_jbar_auto_in_0_d_bits_sink;
  wire  coherent_jbar_auto_in_0_d_bits_denied;
  wire [127:0] coherent_jbar_auto_in_0_d_bits_data;
  wire  coherent_jbar_auto_in_0_d_bits_corrupt;
  wire  coherent_jbar_auto_in_0_e_ready;
  wire  coherent_jbar_auto_in_0_e_valid;
  wire [2:0] coherent_jbar_auto_in_0_e_bits_sink;
  wire  coherent_jbar_auto_out_3_a_ready;
  wire  coherent_jbar_auto_out_3_a_valid;
  wire [2:0] coherent_jbar_auto_out_3_a_bits_opcode;
  wire [2:0] coherent_jbar_auto_out_3_a_bits_param;
  wire [2:0] coherent_jbar_auto_out_3_a_bits_size;
  wire [6:0] coherent_jbar_auto_out_3_a_bits_source;
  wire [35:0] coherent_jbar_auto_out_3_a_bits_address;
  wire [15:0] coherent_jbar_auto_out_3_a_bits_mask;
  wire [127:0] coherent_jbar_auto_out_3_a_bits_data;
  wire  coherent_jbar_auto_out_3_a_bits_corrupt;
  wire  coherent_jbar_auto_out_3_b_ready;
  wire  coherent_jbar_auto_out_3_b_valid;
  wire [2:0] coherent_jbar_auto_out_3_b_bits_opcode;
  wire [1:0] coherent_jbar_auto_out_3_b_bits_param;
  wire [2:0] coherent_jbar_auto_out_3_b_bits_size;
  wire [6:0] coherent_jbar_auto_out_3_b_bits_source;
  wire [35:0] coherent_jbar_auto_out_3_b_bits_address;
  wire [15:0] coherent_jbar_auto_out_3_b_bits_mask;
  wire [127:0] coherent_jbar_auto_out_3_b_bits_data;
  wire  coherent_jbar_auto_out_3_b_bits_corrupt;
  wire  coherent_jbar_auto_out_3_c_ready;
  wire  coherent_jbar_auto_out_3_c_valid;
  wire [2:0] coherent_jbar_auto_out_3_c_bits_opcode;
  wire [2:0] coherent_jbar_auto_out_3_c_bits_param;
  wire [2:0] coherent_jbar_auto_out_3_c_bits_size;
  wire [6:0] coherent_jbar_auto_out_3_c_bits_source;
  wire [35:0] coherent_jbar_auto_out_3_c_bits_address;
  wire [127:0] coherent_jbar_auto_out_3_c_bits_data;
  wire  coherent_jbar_auto_out_3_c_bits_corrupt;
  wire  coherent_jbar_auto_out_3_d_ready;
  wire  coherent_jbar_auto_out_3_d_valid;
  wire [2:0] coherent_jbar_auto_out_3_d_bits_opcode;
  wire [1:0] coherent_jbar_auto_out_3_d_bits_param;
  wire [2:0] coherent_jbar_auto_out_3_d_bits_size;
  wire [6:0] coherent_jbar_auto_out_3_d_bits_source;
  wire [2:0] coherent_jbar_auto_out_3_d_bits_sink;
  wire  coherent_jbar_auto_out_3_d_bits_denied;
  wire [127:0] coherent_jbar_auto_out_3_d_bits_data;
  wire  coherent_jbar_auto_out_3_d_bits_corrupt;
  wire  coherent_jbar_auto_out_3_e_ready;
  wire  coherent_jbar_auto_out_3_e_valid;
  wire [2:0] coherent_jbar_auto_out_3_e_bits_sink;
  wire  coherent_jbar_auto_out_2_a_ready;
  wire  coherent_jbar_auto_out_2_a_valid;
  wire [2:0] coherent_jbar_auto_out_2_a_bits_opcode;
  wire [2:0] coherent_jbar_auto_out_2_a_bits_param;
  wire [2:0] coherent_jbar_auto_out_2_a_bits_size;
  wire [6:0] coherent_jbar_auto_out_2_a_bits_source;
  wire [35:0] coherent_jbar_auto_out_2_a_bits_address;
  wire [15:0] coherent_jbar_auto_out_2_a_bits_mask;
  wire [127:0] coherent_jbar_auto_out_2_a_bits_data;
  wire  coherent_jbar_auto_out_2_a_bits_corrupt;
  wire  coherent_jbar_auto_out_2_b_ready;
  wire  coherent_jbar_auto_out_2_b_valid;
  wire [2:0] coherent_jbar_auto_out_2_b_bits_opcode;
  wire [1:0] coherent_jbar_auto_out_2_b_bits_param;
  wire [2:0] coherent_jbar_auto_out_2_b_bits_size;
  wire [6:0] coherent_jbar_auto_out_2_b_bits_source;
  wire [35:0] coherent_jbar_auto_out_2_b_bits_address;
  wire [15:0] coherent_jbar_auto_out_2_b_bits_mask;
  wire [127:0] coherent_jbar_auto_out_2_b_bits_data;
  wire  coherent_jbar_auto_out_2_b_bits_corrupt;
  wire  coherent_jbar_auto_out_2_c_ready;
  wire  coherent_jbar_auto_out_2_c_valid;
  wire [2:0] coherent_jbar_auto_out_2_c_bits_opcode;
  wire [2:0] coherent_jbar_auto_out_2_c_bits_param;
  wire [2:0] coherent_jbar_auto_out_2_c_bits_size;
  wire [6:0] coherent_jbar_auto_out_2_c_bits_source;
  wire [35:0] coherent_jbar_auto_out_2_c_bits_address;
  wire [127:0] coherent_jbar_auto_out_2_c_bits_data;
  wire  coherent_jbar_auto_out_2_c_bits_corrupt;
  wire  coherent_jbar_auto_out_2_d_ready;
  wire  coherent_jbar_auto_out_2_d_valid;
  wire [2:0] coherent_jbar_auto_out_2_d_bits_opcode;
  wire [1:0] coherent_jbar_auto_out_2_d_bits_param;
  wire [2:0] coherent_jbar_auto_out_2_d_bits_size;
  wire [6:0] coherent_jbar_auto_out_2_d_bits_source;
  wire [2:0] coherent_jbar_auto_out_2_d_bits_sink;
  wire  coherent_jbar_auto_out_2_d_bits_denied;
  wire [127:0] coherent_jbar_auto_out_2_d_bits_data;
  wire  coherent_jbar_auto_out_2_d_bits_corrupt;
  wire  coherent_jbar_auto_out_2_e_ready;
  wire  coherent_jbar_auto_out_2_e_valid;
  wire [2:0] coherent_jbar_auto_out_2_e_bits_sink;
  wire  coherent_jbar_auto_out_1_a_ready;
  wire  coherent_jbar_auto_out_1_a_valid;
  wire [2:0] coherent_jbar_auto_out_1_a_bits_opcode;
  wire [2:0] coherent_jbar_auto_out_1_a_bits_param;
  wire [2:0] coherent_jbar_auto_out_1_a_bits_size;
  wire [6:0] coherent_jbar_auto_out_1_a_bits_source;
  wire [35:0] coherent_jbar_auto_out_1_a_bits_address;
  wire [15:0] coherent_jbar_auto_out_1_a_bits_mask;
  wire [127:0] coherent_jbar_auto_out_1_a_bits_data;
  wire  coherent_jbar_auto_out_1_a_bits_corrupt;
  wire  coherent_jbar_auto_out_1_b_ready;
  wire  coherent_jbar_auto_out_1_b_valid;
  wire [2:0] coherent_jbar_auto_out_1_b_bits_opcode;
  wire [1:0] coherent_jbar_auto_out_1_b_bits_param;
  wire [2:0] coherent_jbar_auto_out_1_b_bits_size;
  wire [6:0] coherent_jbar_auto_out_1_b_bits_source;
  wire [35:0] coherent_jbar_auto_out_1_b_bits_address;
  wire [15:0] coherent_jbar_auto_out_1_b_bits_mask;
  wire [127:0] coherent_jbar_auto_out_1_b_bits_data;
  wire  coherent_jbar_auto_out_1_b_bits_corrupt;
  wire  coherent_jbar_auto_out_1_c_ready;
  wire  coherent_jbar_auto_out_1_c_valid;
  wire [2:0] coherent_jbar_auto_out_1_c_bits_opcode;
  wire [2:0] coherent_jbar_auto_out_1_c_bits_param;
  wire [2:0] coherent_jbar_auto_out_1_c_bits_size;
  wire [6:0] coherent_jbar_auto_out_1_c_bits_source;
  wire [35:0] coherent_jbar_auto_out_1_c_bits_address;
  wire [127:0] coherent_jbar_auto_out_1_c_bits_data;
  wire  coherent_jbar_auto_out_1_c_bits_corrupt;
  wire  coherent_jbar_auto_out_1_d_ready;
  wire  coherent_jbar_auto_out_1_d_valid;
  wire [2:0] coherent_jbar_auto_out_1_d_bits_opcode;
  wire [1:0] coherent_jbar_auto_out_1_d_bits_param;
  wire [2:0] coherent_jbar_auto_out_1_d_bits_size;
  wire [6:0] coherent_jbar_auto_out_1_d_bits_source;
  wire [2:0] coherent_jbar_auto_out_1_d_bits_sink;
  wire  coherent_jbar_auto_out_1_d_bits_denied;
  wire [127:0] coherent_jbar_auto_out_1_d_bits_data;
  wire  coherent_jbar_auto_out_1_d_bits_corrupt;
  wire  coherent_jbar_auto_out_1_e_ready;
  wire  coherent_jbar_auto_out_1_e_valid;
  wire [2:0] coherent_jbar_auto_out_1_e_bits_sink;
  wire  coherent_jbar_auto_out_0_a_ready;
  wire  coherent_jbar_auto_out_0_a_valid;
  wire [2:0] coherent_jbar_auto_out_0_a_bits_opcode;
  wire [2:0] coherent_jbar_auto_out_0_a_bits_param;
  wire [2:0] coherent_jbar_auto_out_0_a_bits_size;
  wire [6:0] coherent_jbar_auto_out_0_a_bits_source;
  wire [35:0] coherent_jbar_auto_out_0_a_bits_address;
  wire [15:0] coherent_jbar_auto_out_0_a_bits_mask;
  wire [127:0] coherent_jbar_auto_out_0_a_bits_data;
  wire  coherent_jbar_auto_out_0_a_bits_corrupt;
  wire  coherent_jbar_auto_out_0_b_ready;
  wire  coherent_jbar_auto_out_0_b_valid;
  wire [2:0] coherent_jbar_auto_out_0_b_bits_opcode;
  wire [1:0] coherent_jbar_auto_out_0_b_bits_param;
  wire [2:0] coherent_jbar_auto_out_0_b_bits_size;
  wire [6:0] coherent_jbar_auto_out_0_b_bits_source;
  wire [35:0] coherent_jbar_auto_out_0_b_bits_address;
  wire [15:0] coherent_jbar_auto_out_0_b_bits_mask;
  wire [127:0] coherent_jbar_auto_out_0_b_bits_data;
  wire  coherent_jbar_auto_out_0_b_bits_corrupt;
  wire  coherent_jbar_auto_out_0_c_ready;
  wire  coherent_jbar_auto_out_0_c_valid;
  wire [2:0] coherent_jbar_auto_out_0_c_bits_opcode;
  wire [2:0] coherent_jbar_auto_out_0_c_bits_param;
  wire [2:0] coherent_jbar_auto_out_0_c_bits_size;
  wire [6:0] coherent_jbar_auto_out_0_c_bits_source;
  wire [35:0] coherent_jbar_auto_out_0_c_bits_address;
  wire [127:0] coherent_jbar_auto_out_0_c_bits_data;
  wire  coherent_jbar_auto_out_0_c_bits_corrupt;
  wire  coherent_jbar_auto_out_0_d_ready;
  wire  coherent_jbar_auto_out_0_d_valid;
  wire [2:0] coherent_jbar_auto_out_0_d_bits_opcode;
  wire [1:0] coherent_jbar_auto_out_0_d_bits_param;
  wire [2:0] coherent_jbar_auto_out_0_d_bits_size;
  wire [6:0] coherent_jbar_auto_out_0_d_bits_source;
  wire [2:0] coherent_jbar_auto_out_0_d_bits_sink;
  wire  coherent_jbar_auto_out_0_d_bits_denied;
  wire [127:0] coherent_jbar_auto_out_0_d_bits_data;
  wire  coherent_jbar_auto_out_0_d_bits_corrupt;
  wire  coherent_jbar_auto_out_0_e_ready;
  wire  coherent_jbar_auto_out_0_e_valid;
  wire [2:0] coherent_jbar_auto_out_0_e_bits_sink;
  wire  binder_rf_reset; // @[BankBinder.scala 67:28]
  wire  binder_clock; // @[BankBinder.scala 67:28]
  wire  binder_reset; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_3_a_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_3_a_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_3_a_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_3_a_bits_param; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_3_a_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_in_3_a_bits_source; // @[BankBinder.scala 67:28]
  wire [35:0] binder_auto_in_3_a_bits_address; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_3_a_bits_user_amba_prot_bufferable; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_3_a_bits_user_amba_prot_modifiable; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_3_a_bits_user_amba_prot_readalloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_3_a_bits_user_amba_prot_writealloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_3_a_bits_user_amba_prot_privileged; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_3_a_bits_user_amba_prot_secure; // @[BankBinder.scala 67:28]
  wire [15:0] binder_auto_in_3_a_bits_mask; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_in_3_a_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_3_d_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_3_d_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_3_d_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_3_d_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_in_3_d_bits_source; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_3_d_bits_denied; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_in_3_d_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_3_d_bits_corrupt; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_2_a_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_2_a_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_2_a_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_2_a_bits_param; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_2_a_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_in_2_a_bits_source; // @[BankBinder.scala 67:28]
  wire [35:0] binder_auto_in_2_a_bits_address; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_2_a_bits_user_amba_prot_bufferable; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_2_a_bits_user_amba_prot_modifiable; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_2_a_bits_user_amba_prot_readalloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_2_a_bits_user_amba_prot_writealloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_2_a_bits_user_amba_prot_privileged; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_2_a_bits_user_amba_prot_secure; // @[BankBinder.scala 67:28]
  wire [15:0] binder_auto_in_2_a_bits_mask; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_in_2_a_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_2_d_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_2_d_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_2_d_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_2_d_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_in_2_d_bits_source; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_2_d_bits_denied; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_in_2_d_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_2_d_bits_corrupt; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_1_a_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_1_a_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_1_a_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_1_a_bits_param; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_1_a_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_in_1_a_bits_source; // @[BankBinder.scala 67:28]
  wire [35:0] binder_auto_in_1_a_bits_address; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_1_a_bits_user_amba_prot_bufferable; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_1_a_bits_user_amba_prot_modifiable; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_1_a_bits_user_amba_prot_readalloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_1_a_bits_user_amba_prot_writealloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_1_a_bits_user_amba_prot_privileged; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_1_a_bits_user_amba_prot_secure; // @[BankBinder.scala 67:28]
  wire [15:0] binder_auto_in_1_a_bits_mask; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_in_1_a_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_1_d_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_1_d_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_1_d_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_1_d_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_in_1_d_bits_source; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_1_d_bits_denied; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_in_1_d_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_1_d_bits_corrupt; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_0_a_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_0_a_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_0_a_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_0_a_bits_param; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_0_a_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_in_0_a_bits_source; // @[BankBinder.scala 67:28]
  wire [35:0] binder_auto_in_0_a_bits_address; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_0_a_bits_user_amba_prot_bufferable; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_0_a_bits_user_amba_prot_modifiable; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_0_a_bits_user_amba_prot_readalloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_0_a_bits_user_amba_prot_writealloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_0_a_bits_user_amba_prot_privileged; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_0_a_bits_user_amba_prot_secure; // @[BankBinder.scala 67:28]
  wire [15:0] binder_auto_in_0_a_bits_mask; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_in_0_a_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_0_d_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_0_d_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_0_d_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_in_0_d_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_in_0_d_bits_source; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_0_d_bits_denied; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_in_0_d_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_in_0_d_bits_corrupt; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_3_a_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_3_a_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_3_a_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_3_a_bits_param; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_3_a_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_out_3_a_bits_source; // @[BankBinder.scala 67:28]
  wire [35:0] binder_auto_out_3_a_bits_address; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_3_a_bits_user_amba_prot_bufferable; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_3_a_bits_user_amba_prot_modifiable; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_3_a_bits_user_amba_prot_readalloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_3_a_bits_user_amba_prot_writealloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_3_a_bits_user_amba_prot_privileged; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_3_a_bits_user_amba_prot_secure; // @[BankBinder.scala 67:28]
  wire [15:0] binder_auto_out_3_a_bits_mask; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_out_3_a_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_3_d_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_3_d_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_3_d_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_3_d_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_out_3_d_bits_source; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_3_d_bits_denied; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_out_3_d_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_3_d_bits_corrupt; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_2_a_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_2_a_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_2_a_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_2_a_bits_param; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_2_a_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_out_2_a_bits_source; // @[BankBinder.scala 67:28]
  wire [35:0] binder_auto_out_2_a_bits_address; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_2_a_bits_user_amba_prot_bufferable; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_2_a_bits_user_amba_prot_modifiable; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_2_a_bits_user_amba_prot_readalloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_2_a_bits_user_amba_prot_writealloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_2_a_bits_user_amba_prot_privileged; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_2_a_bits_user_amba_prot_secure; // @[BankBinder.scala 67:28]
  wire [15:0] binder_auto_out_2_a_bits_mask; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_out_2_a_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_2_d_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_2_d_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_2_d_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_2_d_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_out_2_d_bits_source; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_2_d_bits_denied; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_out_2_d_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_2_d_bits_corrupt; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_1_a_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_1_a_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_1_a_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_1_a_bits_param; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_1_a_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_out_1_a_bits_source; // @[BankBinder.scala 67:28]
  wire [35:0] binder_auto_out_1_a_bits_address; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_1_a_bits_user_amba_prot_bufferable; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_1_a_bits_user_amba_prot_modifiable; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_1_a_bits_user_amba_prot_readalloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_1_a_bits_user_amba_prot_writealloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_1_a_bits_user_amba_prot_privileged; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_1_a_bits_user_amba_prot_secure; // @[BankBinder.scala 67:28]
  wire [15:0] binder_auto_out_1_a_bits_mask; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_out_1_a_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_1_d_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_1_d_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_1_d_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_1_d_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_out_1_d_bits_source; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_1_d_bits_denied; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_out_1_d_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_1_d_bits_corrupt; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_0_a_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_0_a_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_0_a_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_0_a_bits_param; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_0_a_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_out_0_a_bits_source; // @[BankBinder.scala 67:28]
  wire [35:0] binder_auto_out_0_a_bits_address; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_0_a_bits_user_amba_prot_bufferable; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_0_a_bits_user_amba_prot_modifiable; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_0_a_bits_user_amba_prot_readalloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_0_a_bits_user_amba_prot_writealloc; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_0_a_bits_user_amba_prot_privileged; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_0_a_bits_user_amba_prot_secure; // @[BankBinder.scala 67:28]
  wire [15:0] binder_auto_out_0_a_bits_mask; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_out_0_a_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_0_d_ready; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_0_d_valid; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_0_d_bits_opcode; // @[BankBinder.scala 67:28]
  wire [2:0] binder_auto_out_0_d_bits_size; // @[BankBinder.scala 67:28]
  wire [3:0] binder_auto_out_0_d_bits_source; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_0_d_bits_denied; // @[BankBinder.scala 67:28]
  wire [127:0] binder_auto_out_0_d_bits_data; // @[BankBinder.scala 67:28]
  wire  binder_auto_out_0_d_bits_corrupt; // @[BankBinder.scala 67:28]
  wire  buffer_auto_in_3_a_ready;
  wire  buffer_auto_in_3_a_valid;
  wire [2:0] buffer_auto_in_3_a_bits_opcode;
  wire [2:0] buffer_auto_in_3_a_bits_param;
  wire [2:0] buffer_auto_in_3_a_bits_size;
  wire [6:0] buffer_auto_in_3_a_bits_source;
  wire [35:0] buffer_auto_in_3_a_bits_address;
  wire [15:0] buffer_auto_in_3_a_bits_mask;
  wire [127:0] buffer_auto_in_3_a_bits_data;
  wire  buffer_auto_in_3_a_bits_corrupt;
  wire  buffer_auto_in_3_b_ready;
  wire  buffer_auto_in_3_b_valid;
  wire [2:0] buffer_auto_in_3_b_bits_opcode;
  wire [1:0] buffer_auto_in_3_b_bits_param;
  wire [2:0] buffer_auto_in_3_b_bits_size;
  wire [6:0] buffer_auto_in_3_b_bits_source;
  wire [35:0] buffer_auto_in_3_b_bits_address;
  wire [15:0] buffer_auto_in_3_b_bits_mask;
  wire [127:0] buffer_auto_in_3_b_bits_data;
  wire  buffer_auto_in_3_b_bits_corrupt;
  wire  buffer_auto_in_3_c_ready;
  wire  buffer_auto_in_3_c_valid;
  wire [2:0] buffer_auto_in_3_c_bits_opcode;
  wire [2:0] buffer_auto_in_3_c_bits_param;
  wire [2:0] buffer_auto_in_3_c_bits_size;
  wire [6:0] buffer_auto_in_3_c_bits_source;
  wire [35:0] buffer_auto_in_3_c_bits_address;
  wire [127:0] buffer_auto_in_3_c_bits_data;
  wire  buffer_auto_in_3_c_bits_corrupt;
  wire  buffer_auto_in_3_d_ready;
  wire  buffer_auto_in_3_d_valid;
  wire [2:0] buffer_auto_in_3_d_bits_opcode;
  wire [1:0] buffer_auto_in_3_d_bits_param;
  wire [2:0] buffer_auto_in_3_d_bits_size;
  wire [6:0] buffer_auto_in_3_d_bits_source;
  wire [2:0] buffer_auto_in_3_d_bits_sink;
  wire  buffer_auto_in_3_d_bits_denied;
  wire [127:0] buffer_auto_in_3_d_bits_data;
  wire  buffer_auto_in_3_d_bits_corrupt;
  wire  buffer_auto_in_3_e_ready;
  wire  buffer_auto_in_3_e_valid;
  wire [2:0] buffer_auto_in_3_e_bits_sink;
  wire  buffer_auto_in_2_a_ready;
  wire  buffer_auto_in_2_a_valid;
  wire [2:0] buffer_auto_in_2_a_bits_opcode;
  wire [2:0] buffer_auto_in_2_a_bits_param;
  wire [2:0] buffer_auto_in_2_a_bits_size;
  wire [6:0] buffer_auto_in_2_a_bits_source;
  wire [35:0] buffer_auto_in_2_a_bits_address;
  wire [15:0] buffer_auto_in_2_a_bits_mask;
  wire [127:0] buffer_auto_in_2_a_bits_data;
  wire  buffer_auto_in_2_a_bits_corrupt;
  wire  buffer_auto_in_2_b_ready;
  wire  buffer_auto_in_2_b_valid;
  wire [2:0] buffer_auto_in_2_b_bits_opcode;
  wire [1:0] buffer_auto_in_2_b_bits_param;
  wire [2:0] buffer_auto_in_2_b_bits_size;
  wire [6:0] buffer_auto_in_2_b_bits_source;
  wire [35:0] buffer_auto_in_2_b_bits_address;
  wire [15:0] buffer_auto_in_2_b_bits_mask;
  wire [127:0] buffer_auto_in_2_b_bits_data;
  wire  buffer_auto_in_2_b_bits_corrupt;
  wire  buffer_auto_in_2_c_ready;
  wire  buffer_auto_in_2_c_valid;
  wire [2:0] buffer_auto_in_2_c_bits_opcode;
  wire [2:0] buffer_auto_in_2_c_bits_param;
  wire [2:0] buffer_auto_in_2_c_bits_size;
  wire [6:0] buffer_auto_in_2_c_bits_source;
  wire [35:0] buffer_auto_in_2_c_bits_address;
  wire [127:0] buffer_auto_in_2_c_bits_data;
  wire  buffer_auto_in_2_c_bits_corrupt;
  wire  buffer_auto_in_2_d_ready;
  wire  buffer_auto_in_2_d_valid;
  wire [2:0] buffer_auto_in_2_d_bits_opcode;
  wire [1:0] buffer_auto_in_2_d_bits_param;
  wire [2:0] buffer_auto_in_2_d_bits_size;
  wire [6:0] buffer_auto_in_2_d_bits_source;
  wire [2:0] buffer_auto_in_2_d_bits_sink;
  wire  buffer_auto_in_2_d_bits_denied;
  wire [127:0] buffer_auto_in_2_d_bits_data;
  wire  buffer_auto_in_2_d_bits_corrupt;
  wire  buffer_auto_in_2_e_ready;
  wire  buffer_auto_in_2_e_valid;
  wire [2:0] buffer_auto_in_2_e_bits_sink;
  wire  buffer_auto_in_1_a_ready;
  wire  buffer_auto_in_1_a_valid;
  wire [2:0] buffer_auto_in_1_a_bits_opcode;
  wire [2:0] buffer_auto_in_1_a_bits_param;
  wire [2:0] buffer_auto_in_1_a_bits_size;
  wire [6:0] buffer_auto_in_1_a_bits_source;
  wire [35:0] buffer_auto_in_1_a_bits_address;
  wire [15:0] buffer_auto_in_1_a_bits_mask;
  wire [127:0] buffer_auto_in_1_a_bits_data;
  wire  buffer_auto_in_1_a_bits_corrupt;
  wire  buffer_auto_in_1_b_ready;
  wire  buffer_auto_in_1_b_valid;
  wire [2:0] buffer_auto_in_1_b_bits_opcode;
  wire [1:0] buffer_auto_in_1_b_bits_param;
  wire [2:0] buffer_auto_in_1_b_bits_size;
  wire [6:0] buffer_auto_in_1_b_bits_source;
  wire [35:0] buffer_auto_in_1_b_bits_address;
  wire [15:0] buffer_auto_in_1_b_bits_mask;
  wire [127:0] buffer_auto_in_1_b_bits_data;
  wire  buffer_auto_in_1_b_bits_corrupt;
  wire  buffer_auto_in_1_c_ready;
  wire  buffer_auto_in_1_c_valid;
  wire [2:0] buffer_auto_in_1_c_bits_opcode;
  wire [2:0] buffer_auto_in_1_c_bits_param;
  wire [2:0] buffer_auto_in_1_c_bits_size;
  wire [6:0] buffer_auto_in_1_c_bits_source;
  wire [35:0] buffer_auto_in_1_c_bits_address;
  wire [127:0] buffer_auto_in_1_c_bits_data;
  wire  buffer_auto_in_1_c_bits_corrupt;
  wire  buffer_auto_in_1_d_ready;
  wire  buffer_auto_in_1_d_valid;
  wire [2:0] buffer_auto_in_1_d_bits_opcode;
  wire [1:0] buffer_auto_in_1_d_bits_param;
  wire [2:0] buffer_auto_in_1_d_bits_size;
  wire [6:0] buffer_auto_in_1_d_bits_source;
  wire [2:0] buffer_auto_in_1_d_bits_sink;
  wire  buffer_auto_in_1_d_bits_denied;
  wire [127:0] buffer_auto_in_1_d_bits_data;
  wire  buffer_auto_in_1_d_bits_corrupt;
  wire  buffer_auto_in_1_e_ready;
  wire  buffer_auto_in_1_e_valid;
  wire [2:0] buffer_auto_in_1_e_bits_sink;
  wire  buffer_auto_in_0_a_ready;
  wire  buffer_auto_in_0_a_valid;
  wire [2:0] buffer_auto_in_0_a_bits_opcode;
  wire [2:0] buffer_auto_in_0_a_bits_param;
  wire [2:0] buffer_auto_in_0_a_bits_size;
  wire [6:0] buffer_auto_in_0_a_bits_source;
  wire [35:0] buffer_auto_in_0_a_bits_address;
  wire [15:0] buffer_auto_in_0_a_bits_mask;
  wire [127:0] buffer_auto_in_0_a_bits_data;
  wire  buffer_auto_in_0_a_bits_corrupt;
  wire  buffer_auto_in_0_b_ready;
  wire  buffer_auto_in_0_b_valid;
  wire [2:0] buffer_auto_in_0_b_bits_opcode;
  wire [1:0] buffer_auto_in_0_b_bits_param;
  wire [2:0] buffer_auto_in_0_b_bits_size;
  wire [6:0] buffer_auto_in_0_b_bits_source;
  wire [35:0] buffer_auto_in_0_b_bits_address;
  wire [15:0] buffer_auto_in_0_b_bits_mask;
  wire [127:0] buffer_auto_in_0_b_bits_data;
  wire  buffer_auto_in_0_b_bits_corrupt;
  wire  buffer_auto_in_0_c_ready;
  wire  buffer_auto_in_0_c_valid;
  wire [2:0] buffer_auto_in_0_c_bits_opcode;
  wire [2:0] buffer_auto_in_0_c_bits_param;
  wire [2:0] buffer_auto_in_0_c_bits_size;
  wire [6:0] buffer_auto_in_0_c_bits_source;
  wire [35:0] buffer_auto_in_0_c_bits_address;
  wire [127:0] buffer_auto_in_0_c_bits_data;
  wire  buffer_auto_in_0_c_bits_corrupt;
  wire  buffer_auto_in_0_d_ready;
  wire  buffer_auto_in_0_d_valid;
  wire [2:0] buffer_auto_in_0_d_bits_opcode;
  wire [1:0] buffer_auto_in_0_d_bits_param;
  wire [2:0] buffer_auto_in_0_d_bits_size;
  wire [6:0] buffer_auto_in_0_d_bits_source;
  wire [2:0] buffer_auto_in_0_d_bits_sink;
  wire  buffer_auto_in_0_d_bits_denied;
  wire [127:0] buffer_auto_in_0_d_bits_data;
  wire  buffer_auto_in_0_d_bits_corrupt;
  wire  buffer_auto_in_0_e_ready;
  wire  buffer_auto_in_0_e_valid;
  wire [2:0] buffer_auto_in_0_e_bits_sink;
  wire  buffer_auto_out_3_a_ready;
  wire  buffer_auto_out_3_a_valid;
  wire [2:0] buffer_auto_out_3_a_bits_opcode;
  wire [2:0] buffer_auto_out_3_a_bits_param;
  wire [2:0] buffer_auto_out_3_a_bits_size;
  wire [6:0] buffer_auto_out_3_a_bits_source;
  wire [35:0] buffer_auto_out_3_a_bits_address;
  wire [15:0] buffer_auto_out_3_a_bits_mask;
  wire [127:0] buffer_auto_out_3_a_bits_data;
  wire  buffer_auto_out_3_a_bits_corrupt;
  wire  buffer_auto_out_3_b_ready;
  wire  buffer_auto_out_3_b_valid;
  wire [2:0] buffer_auto_out_3_b_bits_opcode;
  wire [1:0] buffer_auto_out_3_b_bits_param;
  wire [2:0] buffer_auto_out_3_b_bits_size;
  wire [6:0] buffer_auto_out_3_b_bits_source;
  wire [35:0] buffer_auto_out_3_b_bits_address;
  wire [15:0] buffer_auto_out_3_b_bits_mask;
  wire [127:0] buffer_auto_out_3_b_bits_data;
  wire  buffer_auto_out_3_b_bits_corrupt;
  wire  buffer_auto_out_3_c_ready;
  wire  buffer_auto_out_3_c_valid;
  wire [2:0] buffer_auto_out_3_c_bits_opcode;
  wire [2:0] buffer_auto_out_3_c_bits_param;
  wire [2:0] buffer_auto_out_3_c_bits_size;
  wire [6:0] buffer_auto_out_3_c_bits_source;
  wire [35:0] buffer_auto_out_3_c_bits_address;
  wire [127:0] buffer_auto_out_3_c_bits_data;
  wire  buffer_auto_out_3_c_bits_corrupt;
  wire  buffer_auto_out_3_d_ready;
  wire  buffer_auto_out_3_d_valid;
  wire [2:0] buffer_auto_out_3_d_bits_opcode;
  wire [1:0] buffer_auto_out_3_d_bits_param;
  wire [2:0] buffer_auto_out_3_d_bits_size;
  wire [6:0] buffer_auto_out_3_d_bits_source;
  wire [2:0] buffer_auto_out_3_d_bits_sink;
  wire  buffer_auto_out_3_d_bits_denied;
  wire [127:0] buffer_auto_out_3_d_bits_data;
  wire  buffer_auto_out_3_d_bits_corrupt;
  wire  buffer_auto_out_3_e_ready;
  wire  buffer_auto_out_3_e_valid;
  wire [2:0] buffer_auto_out_3_e_bits_sink;
  wire  buffer_auto_out_2_a_ready;
  wire  buffer_auto_out_2_a_valid;
  wire [2:0] buffer_auto_out_2_a_bits_opcode;
  wire [2:0] buffer_auto_out_2_a_bits_param;
  wire [2:0] buffer_auto_out_2_a_bits_size;
  wire [6:0] buffer_auto_out_2_a_bits_source;
  wire [35:0] buffer_auto_out_2_a_bits_address;
  wire [15:0] buffer_auto_out_2_a_bits_mask;
  wire [127:0] buffer_auto_out_2_a_bits_data;
  wire  buffer_auto_out_2_a_bits_corrupt;
  wire  buffer_auto_out_2_b_ready;
  wire  buffer_auto_out_2_b_valid;
  wire [2:0] buffer_auto_out_2_b_bits_opcode;
  wire [1:0] buffer_auto_out_2_b_bits_param;
  wire [2:0] buffer_auto_out_2_b_bits_size;
  wire [6:0] buffer_auto_out_2_b_bits_source;
  wire [35:0] buffer_auto_out_2_b_bits_address;
  wire [15:0] buffer_auto_out_2_b_bits_mask;
  wire [127:0] buffer_auto_out_2_b_bits_data;
  wire  buffer_auto_out_2_b_bits_corrupt;
  wire  buffer_auto_out_2_c_ready;
  wire  buffer_auto_out_2_c_valid;
  wire [2:0] buffer_auto_out_2_c_bits_opcode;
  wire [2:0] buffer_auto_out_2_c_bits_param;
  wire [2:0] buffer_auto_out_2_c_bits_size;
  wire [6:0] buffer_auto_out_2_c_bits_source;
  wire [35:0] buffer_auto_out_2_c_bits_address;
  wire [127:0] buffer_auto_out_2_c_bits_data;
  wire  buffer_auto_out_2_c_bits_corrupt;
  wire  buffer_auto_out_2_d_ready;
  wire  buffer_auto_out_2_d_valid;
  wire [2:0] buffer_auto_out_2_d_bits_opcode;
  wire [1:0] buffer_auto_out_2_d_bits_param;
  wire [2:0] buffer_auto_out_2_d_bits_size;
  wire [6:0] buffer_auto_out_2_d_bits_source;
  wire [2:0] buffer_auto_out_2_d_bits_sink;
  wire  buffer_auto_out_2_d_bits_denied;
  wire [127:0] buffer_auto_out_2_d_bits_data;
  wire  buffer_auto_out_2_d_bits_corrupt;
  wire  buffer_auto_out_2_e_ready;
  wire  buffer_auto_out_2_e_valid;
  wire [2:0] buffer_auto_out_2_e_bits_sink;
  wire  buffer_auto_out_1_a_ready;
  wire  buffer_auto_out_1_a_valid;
  wire [2:0] buffer_auto_out_1_a_bits_opcode;
  wire [2:0] buffer_auto_out_1_a_bits_param;
  wire [2:0] buffer_auto_out_1_a_bits_size;
  wire [6:0] buffer_auto_out_1_a_bits_source;
  wire [35:0] buffer_auto_out_1_a_bits_address;
  wire [15:0] buffer_auto_out_1_a_bits_mask;
  wire [127:0] buffer_auto_out_1_a_bits_data;
  wire  buffer_auto_out_1_a_bits_corrupt;
  wire  buffer_auto_out_1_b_ready;
  wire  buffer_auto_out_1_b_valid;
  wire [2:0] buffer_auto_out_1_b_bits_opcode;
  wire [1:0] buffer_auto_out_1_b_bits_param;
  wire [2:0] buffer_auto_out_1_b_bits_size;
  wire [6:0] buffer_auto_out_1_b_bits_source;
  wire [35:0] buffer_auto_out_1_b_bits_address;
  wire [15:0] buffer_auto_out_1_b_bits_mask;
  wire [127:0] buffer_auto_out_1_b_bits_data;
  wire  buffer_auto_out_1_b_bits_corrupt;
  wire  buffer_auto_out_1_c_ready;
  wire  buffer_auto_out_1_c_valid;
  wire [2:0] buffer_auto_out_1_c_bits_opcode;
  wire [2:0] buffer_auto_out_1_c_bits_param;
  wire [2:0] buffer_auto_out_1_c_bits_size;
  wire [6:0] buffer_auto_out_1_c_bits_source;
  wire [35:0] buffer_auto_out_1_c_bits_address;
  wire [127:0] buffer_auto_out_1_c_bits_data;
  wire  buffer_auto_out_1_c_bits_corrupt;
  wire  buffer_auto_out_1_d_ready;
  wire  buffer_auto_out_1_d_valid;
  wire [2:0] buffer_auto_out_1_d_bits_opcode;
  wire [1:0] buffer_auto_out_1_d_bits_param;
  wire [2:0] buffer_auto_out_1_d_bits_size;
  wire [6:0] buffer_auto_out_1_d_bits_source;
  wire [2:0] buffer_auto_out_1_d_bits_sink;
  wire  buffer_auto_out_1_d_bits_denied;
  wire [127:0] buffer_auto_out_1_d_bits_data;
  wire  buffer_auto_out_1_d_bits_corrupt;
  wire  buffer_auto_out_1_e_ready;
  wire  buffer_auto_out_1_e_valid;
  wire [2:0] buffer_auto_out_1_e_bits_sink;
  wire  buffer_auto_out_0_a_ready;
  wire  buffer_auto_out_0_a_valid;
  wire [2:0] buffer_auto_out_0_a_bits_opcode;
  wire [2:0] buffer_auto_out_0_a_bits_param;
  wire [2:0] buffer_auto_out_0_a_bits_size;
  wire [6:0] buffer_auto_out_0_a_bits_source;
  wire [35:0] buffer_auto_out_0_a_bits_address;
  wire [15:0] buffer_auto_out_0_a_bits_mask;
  wire [127:0] buffer_auto_out_0_a_bits_data;
  wire  buffer_auto_out_0_a_bits_corrupt;
  wire  buffer_auto_out_0_b_ready;
  wire  buffer_auto_out_0_b_valid;
  wire [2:0] buffer_auto_out_0_b_bits_opcode;
  wire [1:0] buffer_auto_out_0_b_bits_param;
  wire [2:0] buffer_auto_out_0_b_bits_size;
  wire [6:0] buffer_auto_out_0_b_bits_source;
  wire [35:0] buffer_auto_out_0_b_bits_address;
  wire [15:0] buffer_auto_out_0_b_bits_mask;
  wire [127:0] buffer_auto_out_0_b_bits_data;
  wire  buffer_auto_out_0_b_bits_corrupt;
  wire  buffer_auto_out_0_c_ready;
  wire  buffer_auto_out_0_c_valid;
  wire [2:0] buffer_auto_out_0_c_bits_opcode;
  wire [2:0] buffer_auto_out_0_c_bits_param;
  wire [2:0] buffer_auto_out_0_c_bits_size;
  wire [6:0] buffer_auto_out_0_c_bits_source;
  wire [35:0] buffer_auto_out_0_c_bits_address;
  wire [127:0] buffer_auto_out_0_c_bits_data;
  wire  buffer_auto_out_0_c_bits_corrupt;
  wire  buffer_auto_out_0_d_ready;
  wire  buffer_auto_out_0_d_valid;
  wire [2:0] buffer_auto_out_0_d_bits_opcode;
  wire [1:0] buffer_auto_out_0_d_bits_param;
  wire [2:0] buffer_auto_out_0_d_bits_size;
  wire [6:0] buffer_auto_out_0_d_bits_source;
  wire [2:0] buffer_auto_out_0_d_bits_sink;
  wire  buffer_auto_out_0_d_bits_denied;
  wire [127:0] buffer_auto_out_0_d_bits_data;
  wire  buffer_auto_out_0_d_bits_corrupt;
  wire  buffer_auto_out_0_e_ready;
  wire  buffer_auto_out_0_e_valid;
  wire [2:0] buffer_auto_out_0_e_bits_sink;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_corrupt;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_param;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_source;
  wire [35:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_address;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_bufferable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_modifiable;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_readalloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_writealloc;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_privileged;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_secure;
  wire [15:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_mask;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_ready;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_valid;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_opcode;
  wire [2:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_size;
  wire [3:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_source;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_denied;
  wire [127:0] coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_data;
  wire  coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_corrupt;
  RHEA__ComposableCache l2 ( // @[Configs.scala 210:24]
    .rf_reset(l2_rf_reset),
    .clock(l2_clock),
    .reset(l2_reset),
    .auto_in_4_a_ready(l2_auto_in_4_a_ready),
    .auto_in_4_a_valid(l2_auto_in_4_a_valid),
    .auto_in_4_a_bits_opcode(l2_auto_in_4_a_bits_opcode),
    .auto_in_4_a_bits_param(l2_auto_in_4_a_bits_param),
    .auto_in_4_a_bits_size(l2_auto_in_4_a_bits_size),
    .auto_in_4_a_bits_source(l2_auto_in_4_a_bits_source),
    .auto_in_4_a_bits_address(l2_auto_in_4_a_bits_address),
    .auto_in_4_a_bits_mask(l2_auto_in_4_a_bits_mask),
    .auto_in_4_a_bits_data(l2_auto_in_4_a_bits_data),
    .auto_in_4_a_bits_corrupt(l2_auto_in_4_a_bits_corrupt),
    .auto_in_4_d_ready(l2_auto_in_4_d_ready),
    .auto_in_4_d_valid(l2_auto_in_4_d_valid),
    .auto_in_4_d_bits_opcode(l2_auto_in_4_d_bits_opcode),
    .auto_in_4_d_bits_size(l2_auto_in_4_d_bits_size),
    .auto_in_4_d_bits_source(l2_auto_in_4_d_bits_source),
    .auto_in_4_d_bits_data(l2_auto_in_4_d_bits_data),
    .auto_side_in_a_ready(l2_auto_side_in_a_ready),
    .auto_side_in_a_valid(l2_auto_side_in_a_valid),
    .auto_side_in_a_bits_opcode(l2_auto_side_in_a_bits_opcode),
    .auto_side_in_a_bits_param(l2_auto_side_in_a_bits_param),
    .auto_side_in_a_bits_size(l2_auto_side_in_a_bits_size),
    .auto_side_in_a_bits_source(l2_auto_side_in_a_bits_source),
    .auto_side_in_a_bits_address(l2_auto_side_in_a_bits_address),
    .auto_side_in_a_bits_mask(l2_auto_side_in_a_bits_mask),
    .auto_side_in_a_bits_data(l2_auto_side_in_a_bits_data),
    .auto_side_in_a_bits_corrupt(l2_auto_side_in_a_bits_corrupt),
    .auto_side_in_d_ready(l2_auto_side_in_d_ready),
    .auto_side_in_d_valid(l2_auto_side_in_d_valid),
    .auto_side_in_d_bits_opcode(l2_auto_side_in_d_bits_opcode),
    .auto_side_in_d_bits_size(l2_auto_side_in_d_bits_size),
    .auto_side_in_d_bits_source(l2_auto_side_in_d_bits_source),
    .auto_side_in_d_bits_data(l2_auto_side_in_d_bits_data),
    .auto_side_in_d_bits_corrupt(l2_auto_side_in_d_bits_corrupt),
    .auto_in_3_a_ready(l2_auto_in_3_a_ready),
    .auto_in_3_a_valid(l2_auto_in_3_a_valid),
    .auto_in_3_a_bits_opcode(l2_auto_in_3_a_bits_opcode),
    .auto_in_3_a_bits_param(l2_auto_in_3_a_bits_param),
    .auto_in_3_a_bits_size(l2_auto_in_3_a_bits_size),
    .auto_in_3_a_bits_source(l2_auto_in_3_a_bits_source),
    .auto_in_3_a_bits_address(l2_auto_in_3_a_bits_address),
    .auto_in_3_a_bits_mask(l2_auto_in_3_a_bits_mask),
    .auto_in_3_a_bits_data(l2_auto_in_3_a_bits_data),
    .auto_in_3_a_bits_corrupt(l2_auto_in_3_a_bits_corrupt),
    .auto_in_3_b_ready(l2_auto_in_3_b_ready),
    .auto_in_3_b_valid(l2_auto_in_3_b_valid),
    .auto_in_3_b_bits_opcode(l2_auto_in_3_b_bits_opcode),
    .auto_in_3_b_bits_param(l2_auto_in_3_b_bits_param),
    .auto_in_3_b_bits_size(l2_auto_in_3_b_bits_size),
    .auto_in_3_b_bits_source(l2_auto_in_3_b_bits_source),
    .auto_in_3_b_bits_address(l2_auto_in_3_b_bits_address),
    .auto_in_3_b_bits_mask(l2_auto_in_3_b_bits_mask),
    .auto_in_3_b_bits_data(l2_auto_in_3_b_bits_data),
    .auto_in_3_b_bits_corrupt(l2_auto_in_3_b_bits_corrupt),
    .auto_in_3_c_ready(l2_auto_in_3_c_ready),
    .auto_in_3_c_valid(l2_auto_in_3_c_valid),
    .auto_in_3_c_bits_opcode(l2_auto_in_3_c_bits_opcode),
    .auto_in_3_c_bits_param(l2_auto_in_3_c_bits_param),
    .auto_in_3_c_bits_size(l2_auto_in_3_c_bits_size),
    .auto_in_3_c_bits_source(l2_auto_in_3_c_bits_source),
    .auto_in_3_c_bits_address(l2_auto_in_3_c_bits_address),
    .auto_in_3_c_bits_data(l2_auto_in_3_c_bits_data),
    .auto_in_3_c_bits_corrupt(l2_auto_in_3_c_bits_corrupt),
    .auto_in_3_d_ready(l2_auto_in_3_d_ready),
    .auto_in_3_d_valid(l2_auto_in_3_d_valid),
    .auto_in_3_d_bits_opcode(l2_auto_in_3_d_bits_opcode),
    .auto_in_3_d_bits_param(l2_auto_in_3_d_bits_param),
    .auto_in_3_d_bits_size(l2_auto_in_3_d_bits_size),
    .auto_in_3_d_bits_source(l2_auto_in_3_d_bits_source),
    .auto_in_3_d_bits_sink(l2_auto_in_3_d_bits_sink),
    .auto_in_3_d_bits_denied(l2_auto_in_3_d_bits_denied),
    .auto_in_3_d_bits_data(l2_auto_in_3_d_bits_data),
    .auto_in_3_d_bits_corrupt(l2_auto_in_3_d_bits_corrupt),
    .auto_in_3_e_ready(l2_auto_in_3_e_ready),
    .auto_in_3_e_valid(l2_auto_in_3_e_valid),
    .auto_in_3_e_bits_sink(l2_auto_in_3_e_bits_sink),
    .auto_in_2_a_ready(l2_auto_in_2_a_ready),
    .auto_in_2_a_valid(l2_auto_in_2_a_valid),
    .auto_in_2_a_bits_opcode(l2_auto_in_2_a_bits_opcode),
    .auto_in_2_a_bits_param(l2_auto_in_2_a_bits_param),
    .auto_in_2_a_bits_size(l2_auto_in_2_a_bits_size),
    .auto_in_2_a_bits_source(l2_auto_in_2_a_bits_source),
    .auto_in_2_a_bits_address(l2_auto_in_2_a_bits_address),
    .auto_in_2_a_bits_mask(l2_auto_in_2_a_bits_mask),
    .auto_in_2_a_bits_data(l2_auto_in_2_a_bits_data),
    .auto_in_2_a_bits_corrupt(l2_auto_in_2_a_bits_corrupt),
    .auto_in_2_b_ready(l2_auto_in_2_b_ready),
    .auto_in_2_b_valid(l2_auto_in_2_b_valid),
    .auto_in_2_b_bits_opcode(l2_auto_in_2_b_bits_opcode),
    .auto_in_2_b_bits_param(l2_auto_in_2_b_bits_param),
    .auto_in_2_b_bits_size(l2_auto_in_2_b_bits_size),
    .auto_in_2_b_bits_source(l2_auto_in_2_b_bits_source),
    .auto_in_2_b_bits_address(l2_auto_in_2_b_bits_address),
    .auto_in_2_b_bits_mask(l2_auto_in_2_b_bits_mask),
    .auto_in_2_b_bits_data(l2_auto_in_2_b_bits_data),
    .auto_in_2_b_bits_corrupt(l2_auto_in_2_b_bits_corrupt),
    .auto_in_2_c_ready(l2_auto_in_2_c_ready),
    .auto_in_2_c_valid(l2_auto_in_2_c_valid),
    .auto_in_2_c_bits_opcode(l2_auto_in_2_c_bits_opcode),
    .auto_in_2_c_bits_param(l2_auto_in_2_c_bits_param),
    .auto_in_2_c_bits_size(l2_auto_in_2_c_bits_size),
    .auto_in_2_c_bits_source(l2_auto_in_2_c_bits_source),
    .auto_in_2_c_bits_address(l2_auto_in_2_c_bits_address),
    .auto_in_2_c_bits_data(l2_auto_in_2_c_bits_data),
    .auto_in_2_c_bits_corrupt(l2_auto_in_2_c_bits_corrupt),
    .auto_in_2_d_ready(l2_auto_in_2_d_ready),
    .auto_in_2_d_valid(l2_auto_in_2_d_valid),
    .auto_in_2_d_bits_opcode(l2_auto_in_2_d_bits_opcode),
    .auto_in_2_d_bits_param(l2_auto_in_2_d_bits_param),
    .auto_in_2_d_bits_size(l2_auto_in_2_d_bits_size),
    .auto_in_2_d_bits_source(l2_auto_in_2_d_bits_source),
    .auto_in_2_d_bits_sink(l2_auto_in_2_d_bits_sink),
    .auto_in_2_d_bits_denied(l2_auto_in_2_d_bits_denied),
    .auto_in_2_d_bits_data(l2_auto_in_2_d_bits_data),
    .auto_in_2_d_bits_corrupt(l2_auto_in_2_d_bits_corrupt),
    .auto_in_2_e_ready(l2_auto_in_2_e_ready),
    .auto_in_2_e_valid(l2_auto_in_2_e_valid),
    .auto_in_2_e_bits_sink(l2_auto_in_2_e_bits_sink),
    .auto_in_1_a_ready(l2_auto_in_1_a_ready),
    .auto_in_1_a_valid(l2_auto_in_1_a_valid),
    .auto_in_1_a_bits_opcode(l2_auto_in_1_a_bits_opcode),
    .auto_in_1_a_bits_param(l2_auto_in_1_a_bits_param),
    .auto_in_1_a_bits_size(l2_auto_in_1_a_bits_size),
    .auto_in_1_a_bits_source(l2_auto_in_1_a_bits_source),
    .auto_in_1_a_bits_address(l2_auto_in_1_a_bits_address),
    .auto_in_1_a_bits_mask(l2_auto_in_1_a_bits_mask),
    .auto_in_1_a_bits_data(l2_auto_in_1_a_bits_data),
    .auto_in_1_a_bits_corrupt(l2_auto_in_1_a_bits_corrupt),
    .auto_in_1_b_ready(l2_auto_in_1_b_ready),
    .auto_in_1_b_valid(l2_auto_in_1_b_valid),
    .auto_in_1_b_bits_opcode(l2_auto_in_1_b_bits_opcode),
    .auto_in_1_b_bits_param(l2_auto_in_1_b_bits_param),
    .auto_in_1_b_bits_size(l2_auto_in_1_b_bits_size),
    .auto_in_1_b_bits_source(l2_auto_in_1_b_bits_source),
    .auto_in_1_b_bits_address(l2_auto_in_1_b_bits_address),
    .auto_in_1_b_bits_mask(l2_auto_in_1_b_bits_mask),
    .auto_in_1_b_bits_data(l2_auto_in_1_b_bits_data),
    .auto_in_1_b_bits_corrupt(l2_auto_in_1_b_bits_corrupt),
    .auto_in_1_c_ready(l2_auto_in_1_c_ready),
    .auto_in_1_c_valid(l2_auto_in_1_c_valid),
    .auto_in_1_c_bits_opcode(l2_auto_in_1_c_bits_opcode),
    .auto_in_1_c_bits_param(l2_auto_in_1_c_bits_param),
    .auto_in_1_c_bits_size(l2_auto_in_1_c_bits_size),
    .auto_in_1_c_bits_source(l2_auto_in_1_c_bits_source),
    .auto_in_1_c_bits_address(l2_auto_in_1_c_bits_address),
    .auto_in_1_c_bits_data(l2_auto_in_1_c_bits_data),
    .auto_in_1_c_bits_corrupt(l2_auto_in_1_c_bits_corrupt),
    .auto_in_1_d_ready(l2_auto_in_1_d_ready),
    .auto_in_1_d_valid(l2_auto_in_1_d_valid),
    .auto_in_1_d_bits_opcode(l2_auto_in_1_d_bits_opcode),
    .auto_in_1_d_bits_param(l2_auto_in_1_d_bits_param),
    .auto_in_1_d_bits_size(l2_auto_in_1_d_bits_size),
    .auto_in_1_d_bits_source(l2_auto_in_1_d_bits_source),
    .auto_in_1_d_bits_sink(l2_auto_in_1_d_bits_sink),
    .auto_in_1_d_bits_denied(l2_auto_in_1_d_bits_denied),
    .auto_in_1_d_bits_data(l2_auto_in_1_d_bits_data),
    .auto_in_1_d_bits_corrupt(l2_auto_in_1_d_bits_corrupt),
    .auto_in_1_e_ready(l2_auto_in_1_e_ready),
    .auto_in_1_e_valid(l2_auto_in_1_e_valid),
    .auto_in_1_e_bits_sink(l2_auto_in_1_e_bits_sink),
    .auto_in_0_a_ready(l2_auto_in_0_a_ready),
    .auto_in_0_a_valid(l2_auto_in_0_a_valid),
    .auto_in_0_a_bits_opcode(l2_auto_in_0_a_bits_opcode),
    .auto_in_0_a_bits_param(l2_auto_in_0_a_bits_param),
    .auto_in_0_a_bits_size(l2_auto_in_0_a_bits_size),
    .auto_in_0_a_bits_source(l2_auto_in_0_a_bits_source),
    .auto_in_0_a_bits_address(l2_auto_in_0_a_bits_address),
    .auto_in_0_a_bits_mask(l2_auto_in_0_a_bits_mask),
    .auto_in_0_a_bits_data(l2_auto_in_0_a_bits_data),
    .auto_in_0_a_bits_corrupt(l2_auto_in_0_a_bits_corrupt),
    .auto_in_0_b_ready(l2_auto_in_0_b_ready),
    .auto_in_0_b_valid(l2_auto_in_0_b_valid),
    .auto_in_0_b_bits_opcode(l2_auto_in_0_b_bits_opcode),
    .auto_in_0_b_bits_param(l2_auto_in_0_b_bits_param),
    .auto_in_0_b_bits_size(l2_auto_in_0_b_bits_size),
    .auto_in_0_b_bits_source(l2_auto_in_0_b_bits_source),
    .auto_in_0_b_bits_address(l2_auto_in_0_b_bits_address),
    .auto_in_0_b_bits_mask(l2_auto_in_0_b_bits_mask),
    .auto_in_0_b_bits_data(l2_auto_in_0_b_bits_data),
    .auto_in_0_b_bits_corrupt(l2_auto_in_0_b_bits_corrupt),
    .auto_in_0_c_ready(l2_auto_in_0_c_ready),
    .auto_in_0_c_valid(l2_auto_in_0_c_valid),
    .auto_in_0_c_bits_opcode(l2_auto_in_0_c_bits_opcode),
    .auto_in_0_c_bits_param(l2_auto_in_0_c_bits_param),
    .auto_in_0_c_bits_size(l2_auto_in_0_c_bits_size),
    .auto_in_0_c_bits_source(l2_auto_in_0_c_bits_source),
    .auto_in_0_c_bits_address(l2_auto_in_0_c_bits_address),
    .auto_in_0_c_bits_data(l2_auto_in_0_c_bits_data),
    .auto_in_0_c_bits_corrupt(l2_auto_in_0_c_bits_corrupt),
    .auto_in_0_d_ready(l2_auto_in_0_d_ready),
    .auto_in_0_d_valid(l2_auto_in_0_d_valid),
    .auto_in_0_d_bits_opcode(l2_auto_in_0_d_bits_opcode),
    .auto_in_0_d_bits_param(l2_auto_in_0_d_bits_param),
    .auto_in_0_d_bits_size(l2_auto_in_0_d_bits_size),
    .auto_in_0_d_bits_source(l2_auto_in_0_d_bits_source),
    .auto_in_0_d_bits_sink(l2_auto_in_0_d_bits_sink),
    .auto_in_0_d_bits_denied(l2_auto_in_0_d_bits_denied),
    .auto_in_0_d_bits_data(l2_auto_in_0_d_bits_data),
    .auto_in_0_d_bits_corrupt(l2_auto_in_0_d_bits_corrupt),
    .auto_in_0_e_ready(l2_auto_in_0_e_ready),
    .auto_in_0_e_valid(l2_auto_in_0_e_valid),
    .auto_in_0_e_bits_sink(l2_auto_in_0_e_bits_sink),
    .auto_out_3_a_ready(l2_auto_out_3_a_ready),
    .auto_out_3_a_valid(l2_auto_out_3_a_valid),
    .auto_out_3_a_bits_opcode(l2_auto_out_3_a_bits_opcode),
    .auto_out_3_a_bits_param(l2_auto_out_3_a_bits_param),
    .auto_out_3_a_bits_source(l2_auto_out_3_a_bits_source),
    .auto_out_3_a_bits_address(l2_auto_out_3_a_bits_address),
    .auto_out_3_c_ready(l2_auto_out_3_c_ready),
    .auto_out_3_c_valid(l2_auto_out_3_c_valid),
    .auto_out_3_c_bits_opcode(l2_auto_out_3_c_bits_opcode),
    .auto_out_3_c_bits_param(l2_auto_out_3_c_bits_param),
    .auto_out_3_c_bits_source(l2_auto_out_3_c_bits_source),
    .auto_out_3_c_bits_address(l2_auto_out_3_c_bits_address),
    .auto_out_3_c_bits_data(l2_auto_out_3_c_bits_data),
    .auto_out_3_d_ready(l2_auto_out_3_d_ready),
    .auto_out_3_d_valid(l2_auto_out_3_d_valid),
    .auto_out_3_d_bits_opcode(l2_auto_out_3_d_bits_opcode),
    .auto_out_3_d_bits_param(l2_auto_out_3_d_bits_param),
    .auto_out_3_d_bits_size(l2_auto_out_3_d_bits_size),
    .auto_out_3_d_bits_source(l2_auto_out_3_d_bits_source),
    .auto_out_3_d_bits_sink(l2_auto_out_3_d_bits_sink),
    .auto_out_3_d_bits_denied(l2_auto_out_3_d_bits_denied),
    .auto_out_3_d_bits_data(l2_auto_out_3_d_bits_data),
    .auto_out_3_d_bits_corrupt(l2_auto_out_3_d_bits_corrupt),
    .auto_out_3_e_valid(l2_auto_out_3_e_valid),
    .auto_out_3_e_bits_sink(l2_auto_out_3_e_bits_sink),
    .auto_out_2_a_ready(l2_auto_out_2_a_ready),
    .auto_out_2_a_valid(l2_auto_out_2_a_valid),
    .auto_out_2_a_bits_opcode(l2_auto_out_2_a_bits_opcode),
    .auto_out_2_a_bits_param(l2_auto_out_2_a_bits_param),
    .auto_out_2_a_bits_source(l2_auto_out_2_a_bits_source),
    .auto_out_2_a_bits_address(l2_auto_out_2_a_bits_address),
    .auto_out_2_c_ready(l2_auto_out_2_c_ready),
    .auto_out_2_c_valid(l2_auto_out_2_c_valid),
    .auto_out_2_c_bits_opcode(l2_auto_out_2_c_bits_opcode),
    .auto_out_2_c_bits_param(l2_auto_out_2_c_bits_param),
    .auto_out_2_c_bits_source(l2_auto_out_2_c_bits_source),
    .auto_out_2_c_bits_address(l2_auto_out_2_c_bits_address),
    .auto_out_2_c_bits_data(l2_auto_out_2_c_bits_data),
    .auto_out_2_d_ready(l2_auto_out_2_d_ready),
    .auto_out_2_d_valid(l2_auto_out_2_d_valid),
    .auto_out_2_d_bits_opcode(l2_auto_out_2_d_bits_opcode),
    .auto_out_2_d_bits_param(l2_auto_out_2_d_bits_param),
    .auto_out_2_d_bits_size(l2_auto_out_2_d_bits_size),
    .auto_out_2_d_bits_source(l2_auto_out_2_d_bits_source),
    .auto_out_2_d_bits_sink(l2_auto_out_2_d_bits_sink),
    .auto_out_2_d_bits_denied(l2_auto_out_2_d_bits_denied),
    .auto_out_2_d_bits_data(l2_auto_out_2_d_bits_data),
    .auto_out_2_d_bits_corrupt(l2_auto_out_2_d_bits_corrupt),
    .auto_out_2_e_valid(l2_auto_out_2_e_valid),
    .auto_out_2_e_bits_sink(l2_auto_out_2_e_bits_sink),
    .auto_out_1_a_ready(l2_auto_out_1_a_ready),
    .auto_out_1_a_valid(l2_auto_out_1_a_valid),
    .auto_out_1_a_bits_opcode(l2_auto_out_1_a_bits_opcode),
    .auto_out_1_a_bits_param(l2_auto_out_1_a_bits_param),
    .auto_out_1_a_bits_source(l2_auto_out_1_a_bits_source),
    .auto_out_1_a_bits_address(l2_auto_out_1_a_bits_address),
    .auto_out_1_c_ready(l2_auto_out_1_c_ready),
    .auto_out_1_c_valid(l2_auto_out_1_c_valid),
    .auto_out_1_c_bits_opcode(l2_auto_out_1_c_bits_opcode),
    .auto_out_1_c_bits_param(l2_auto_out_1_c_bits_param),
    .auto_out_1_c_bits_source(l2_auto_out_1_c_bits_source),
    .auto_out_1_c_bits_address(l2_auto_out_1_c_bits_address),
    .auto_out_1_c_bits_data(l2_auto_out_1_c_bits_data),
    .auto_out_1_d_ready(l2_auto_out_1_d_ready),
    .auto_out_1_d_valid(l2_auto_out_1_d_valid),
    .auto_out_1_d_bits_opcode(l2_auto_out_1_d_bits_opcode),
    .auto_out_1_d_bits_param(l2_auto_out_1_d_bits_param),
    .auto_out_1_d_bits_size(l2_auto_out_1_d_bits_size),
    .auto_out_1_d_bits_source(l2_auto_out_1_d_bits_source),
    .auto_out_1_d_bits_sink(l2_auto_out_1_d_bits_sink),
    .auto_out_1_d_bits_denied(l2_auto_out_1_d_bits_denied),
    .auto_out_1_d_bits_data(l2_auto_out_1_d_bits_data),
    .auto_out_1_d_bits_corrupt(l2_auto_out_1_d_bits_corrupt),
    .auto_out_1_e_valid(l2_auto_out_1_e_valid),
    .auto_out_1_e_bits_sink(l2_auto_out_1_e_bits_sink),
    .auto_out_0_a_ready(l2_auto_out_0_a_ready),
    .auto_out_0_a_valid(l2_auto_out_0_a_valid),
    .auto_out_0_a_bits_opcode(l2_auto_out_0_a_bits_opcode),
    .auto_out_0_a_bits_param(l2_auto_out_0_a_bits_param),
    .auto_out_0_a_bits_source(l2_auto_out_0_a_bits_source),
    .auto_out_0_a_bits_address(l2_auto_out_0_a_bits_address),
    .auto_out_0_c_ready(l2_auto_out_0_c_ready),
    .auto_out_0_c_valid(l2_auto_out_0_c_valid),
    .auto_out_0_c_bits_opcode(l2_auto_out_0_c_bits_opcode),
    .auto_out_0_c_bits_param(l2_auto_out_0_c_bits_param),
    .auto_out_0_c_bits_source(l2_auto_out_0_c_bits_source),
    .auto_out_0_c_bits_address(l2_auto_out_0_c_bits_address),
    .auto_out_0_c_bits_data(l2_auto_out_0_c_bits_data),
    .auto_out_0_d_ready(l2_auto_out_0_d_ready),
    .auto_out_0_d_valid(l2_auto_out_0_d_valid),
    .auto_out_0_d_bits_opcode(l2_auto_out_0_d_bits_opcode),
    .auto_out_0_d_bits_param(l2_auto_out_0_d_bits_param),
    .auto_out_0_d_bits_size(l2_auto_out_0_d_bits_size),
    .auto_out_0_d_bits_source(l2_auto_out_0_d_bits_source),
    .auto_out_0_d_bits_sink(l2_auto_out_0_d_bits_sink),
    .auto_out_0_d_bits_denied(l2_auto_out_0_d_bits_denied),
    .auto_out_0_d_bits_data(l2_auto_out_0_d_bits_data),
    .auto_out_0_d_bits_corrupt(l2_auto_out_0_d_bits_corrupt),
    .auto_out_0_e_valid(l2_auto_out_0_e_valid),
    .auto_out_0_e_bits_sink(l2_auto_out_0_e_bits_sink)
  );
  RHEA__TLBuffer_16 ComposableCache_inner_TLBuffer ( // @[Parameters.scala 57:69]
    .rf_reset(ComposableCache_inner_TLBuffer_rf_reset),
    .clock(ComposableCache_inner_TLBuffer_clock),
    .reset(ComposableCache_inner_TLBuffer_reset),
    .auto_in_3_a_ready(ComposableCache_inner_TLBuffer_auto_in_3_a_ready),
    .auto_in_3_a_valid(ComposableCache_inner_TLBuffer_auto_in_3_a_valid),
    .auto_in_3_a_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_3_a_bits_opcode),
    .auto_in_3_a_bits_param(ComposableCache_inner_TLBuffer_auto_in_3_a_bits_param),
    .auto_in_3_a_bits_size(ComposableCache_inner_TLBuffer_auto_in_3_a_bits_size),
    .auto_in_3_a_bits_source(ComposableCache_inner_TLBuffer_auto_in_3_a_bits_source),
    .auto_in_3_a_bits_address(ComposableCache_inner_TLBuffer_auto_in_3_a_bits_address),
    .auto_in_3_a_bits_mask(ComposableCache_inner_TLBuffer_auto_in_3_a_bits_mask),
    .auto_in_3_a_bits_data(ComposableCache_inner_TLBuffer_auto_in_3_a_bits_data),
    .auto_in_3_a_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_3_a_bits_corrupt),
    .auto_in_3_b_ready(ComposableCache_inner_TLBuffer_auto_in_3_b_ready),
    .auto_in_3_b_valid(ComposableCache_inner_TLBuffer_auto_in_3_b_valid),
    .auto_in_3_b_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_3_b_bits_opcode),
    .auto_in_3_b_bits_param(ComposableCache_inner_TLBuffer_auto_in_3_b_bits_param),
    .auto_in_3_b_bits_size(ComposableCache_inner_TLBuffer_auto_in_3_b_bits_size),
    .auto_in_3_b_bits_source(ComposableCache_inner_TLBuffer_auto_in_3_b_bits_source),
    .auto_in_3_b_bits_address(ComposableCache_inner_TLBuffer_auto_in_3_b_bits_address),
    .auto_in_3_b_bits_mask(ComposableCache_inner_TLBuffer_auto_in_3_b_bits_mask),
    .auto_in_3_b_bits_data(ComposableCache_inner_TLBuffer_auto_in_3_b_bits_data),
    .auto_in_3_b_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_3_b_bits_corrupt),
    .auto_in_3_c_ready(ComposableCache_inner_TLBuffer_auto_in_3_c_ready),
    .auto_in_3_c_valid(ComposableCache_inner_TLBuffer_auto_in_3_c_valid),
    .auto_in_3_c_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_3_c_bits_opcode),
    .auto_in_3_c_bits_param(ComposableCache_inner_TLBuffer_auto_in_3_c_bits_param),
    .auto_in_3_c_bits_size(ComposableCache_inner_TLBuffer_auto_in_3_c_bits_size),
    .auto_in_3_c_bits_source(ComposableCache_inner_TLBuffer_auto_in_3_c_bits_source),
    .auto_in_3_c_bits_address(ComposableCache_inner_TLBuffer_auto_in_3_c_bits_address),
    .auto_in_3_c_bits_data(ComposableCache_inner_TLBuffer_auto_in_3_c_bits_data),
    .auto_in_3_c_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_3_c_bits_corrupt),
    .auto_in_3_d_ready(ComposableCache_inner_TLBuffer_auto_in_3_d_ready),
    .auto_in_3_d_valid(ComposableCache_inner_TLBuffer_auto_in_3_d_valid),
    .auto_in_3_d_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_3_d_bits_opcode),
    .auto_in_3_d_bits_param(ComposableCache_inner_TLBuffer_auto_in_3_d_bits_param),
    .auto_in_3_d_bits_size(ComposableCache_inner_TLBuffer_auto_in_3_d_bits_size),
    .auto_in_3_d_bits_source(ComposableCache_inner_TLBuffer_auto_in_3_d_bits_source),
    .auto_in_3_d_bits_sink(ComposableCache_inner_TLBuffer_auto_in_3_d_bits_sink),
    .auto_in_3_d_bits_denied(ComposableCache_inner_TLBuffer_auto_in_3_d_bits_denied),
    .auto_in_3_d_bits_data(ComposableCache_inner_TLBuffer_auto_in_3_d_bits_data),
    .auto_in_3_d_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_3_d_bits_corrupt),
    .auto_in_3_e_ready(ComposableCache_inner_TLBuffer_auto_in_3_e_ready),
    .auto_in_3_e_valid(ComposableCache_inner_TLBuffer_auto_in_3_e_valid),
    .auto_in_3_e_bits_sink(ComposableCache_inner_TLBuffer_auto_in_3_e_bits_sink),
    .auto_in_2_a_ready(ComposableCache_inner_TLBuffer_auto_in_2_a_ready),
    .auto_in_2_a_valid(ComposableCache_inner_TLBuffer_auto_in_2_a_valid),
    .auto_in_2_a_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_2_a_bits_opcode),
    .auto_in_2_a_bits_param(ComposableCache_inner_TLBuffer_auto_in_2_a_bits_param),
    .auto_in_2_a_bits_size(ComposableCache_inner_TLBuffer_auto_in_2_a_bits_size),
    .auto_in_2_a_bits_source(ComposableCache_inner_TLBuffer_auto_in_2_a_bits_source),
    .auto_in_2_a_bits_address(ComposableCache_inner_TLBuffer_auto_in_2_a_bits_address),
    .auto_in_2_a_bits_mask(ComposableCache_inner_TLBuffer_auto_in_2_a_bits_mask),
    .auto_in_2_a_bits_data(ComposableCache_inner_TLBuffer_auto_in_2_a_bits_data),
    .auto_in_2_a_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_2_a_bits_corrupt),
    .auto_in_2_b_ready(ComposableCache_inner_TLBuffer_auto_in_2_b_ready),
    .auto_in_2_b_valid(ComposableCache_inner_TLBuffer_auto_in_2_b_valid),
    .auto_in_2_b_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_2_b_bits_opcode),
    .auto_in_2_b_bits_param(ComposableCache_inner_TLBuffer_auto_in_2_b_bits_param),
    .auto_in_2_b_bits_size(ComposableCache_inner_TLBuffer_auto_in_2_b_bits_size),
    .auto_in_2_b_bits_source(ComposableCache_inner_TLBuffer_auto_in_2_b_bits_source),
    .auto_in_2_b_bits_address(ComposableCache_inner_TLBuffer_auto_in_2_b_bits_address),
    .auto_in_2_b_bits_mask(ComposableCache_inner_TLBuffer_auto_in_2_b_bits_mask),
    .auto_in_2_b_bits_data(ComposableCache_inner_TLBuffer_auto_in_2_b_bits_data),
    .auto_in_2_b_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_2_b_bits_corrupt),
    .auto_in_2_c_ready(ComposableCache_inner_TLBuffer_auto_in_2_c_ready),
    .auto_in_2_c_valid(ComposableCache_inner_TLBuffer_auto_in_2_c_valid),
    .auto_in_2_c_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_2_c_bits_opcode),
    .auto_in_2_c_bits_param(ComposableCache_inner_TLBuffer_auto_in_2_c_bits_param),
    .auto_in_2_c_bits_size(ComposableCache_inner_TLBuffer_auto_in_2_c_bits_size),
    .auto_in_2_c_bits_source(ComposableCache_inner_TLBuffer_auto_in_2_c_bits_source),
    .auto_in_2_c_bits_address(ComposableCache_inner_TLBuffer_auto_in_2_c_bits_address),
    .auto_in_2_c_bits_data(ComposableCache_inner_TLBuffer_auto_in_2_c_bits_data),
    .auto_in_2_c_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_2_c_bits_corrupt),
    .auto_in_2_d_ready(ComposableCache_inner_TLBuffer_auto_in_2_d_ready),
    .auto_in_2_d_valid(ComposableCache_inner_TLBuffer_auto_in_2_d_valid),
    .auto_in_2_d_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_2_d_bits_opcode),
    .auto_in_2_d_bits_param(ComposableCache_inner_TLBuffer_auto_in_2_d_bits_param),
    .auto_in_2_d_bits_size(ComposableCache_inner_TLBuffer_auto_in_2_d_bits_size),
    .auto_in_2_d_bits_source(ComposableCache_inner_TLBuffer_auto_in_2_d_bits_source),
    .auto_in_2_d_bits_sink(ComposableCache_inner_TLBuffer_auto_in_2_d_bits_sink),
    .auto_in_2_d_bits_denied(ComposableCache_inner_TLBuffer_auto_in_2_d_bits_denied),
    .auto_in_2_d_bits_data(ComposableCache_inner_TLBuffer_auto_in_2_d_bits_data),
    .auto_in_2_d_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_2_d_bits_corrupt),
    .auto_in_2_e_ready(ComposableCache_inner_TLBuffer_auto_in_2_e_ready),
    .auto_in_2_e_valid(ComposableCache_inner_TLBuffer_auto_in_2_e_valid),
    .auto_in_2_e_bits_sink(ComposableCache_inner_TLBuffer_auto_in_2_e_bits_sink),
    .auto_in_1_a_ready(ComposableCache_inner_TLBuffer_auto_in_1_a_ready),
    .auto_in_1_a_valid(ComposableCache_inner_TLBuffer_auto_in_1_a_valid),
    .auto_in_1_a_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_1_a_bits_opcode),
    .auto_in_1_a_bits_param(ComposableCache_inner_TLBuffer_auto_in_1_a_bits_param),
    .auto_in_1_a_bits_size(ComposableCache_inner_TLBuffer_auto_in_1_a_bits_size),
    .auto_in_1_a_bits_source(ComposableCache_inner_TLBuffer_auto_in_1_a_bits_source),
    .auto_in_1_a_bits_address(ComposableCache_inner_TLBuffer_auto_in_1_a_bits_address),
    .auto_in_1_a_bits_mask(ComposableCache_inner_TLBuffer_auto_in_1_a_bits_mask),
    .auto_in_1_a_bits_data(ComposableCache_inner_TLBuffer_auto_in_1_a_bits_data),
    .auto_in_1_a_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_1_a_bits_corrupt),
    .auto_in_1_b_ready(ComposableCache_inner_TLBuffer_auto_in_1_b_ready),
    .auto_in_1_b_valid(ComposableCache_inner_TLBuffer_auto_in_1_b_valid),
    .auto_in_1_b_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_1_b_bits_opcode),
    .auto_in_1_b_bits_param(ComposableCache_inner_TLBuffer_auto_in_1_b_bits_param),
    .auto_in_1_b_bits_size(ComposableCache_inner_TLBuffer_auto_in_1_b_bits_size),
    .auto_in_1_b_bits_source(ComposableCache_inner_TLBuffer_auto_in_1_b_bits_source),
    .auto_in_1_b_bits_address(ComposableCache_inner_TLBuffer_auto_in_1_b_bits_address),
    .auto_in_1_b_bits_mask(ComposableCache_inner_TLBuffer_auto_in_1_b_bits_mask),
    .auto_in_1_b_bits_data(ComposableCache_inner_TLBuffer_auto_in_1_b_bits_data),
    .auto_in_1_b_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_1_b_bits_corrupt),
    .auto_in_1_c_ready(ComposableCache_inner_TLBuffer_auto_in_1_c_ready),
    .auto_in_1_c_valid(ComposableCache_inner_TLBuffer_auto_in_1_c_valid),
    .auto_in_1_c_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_1_c_bits_opcode),
    .auto_in_1_c_bits_param(ComposableCache_inner_TLBuffer_auto_in_1_c_bits_param),
    .auto_in_1_c_bits_size(ComposableCache_inner_TLBuffer_auto_in_1_c_bits_size),
    .auto_in_1_c_bits_source(ComposableCache_inner_TLBuffer_auto_in_1_c_bits_source),
    .auto_in_1_c_bits_address(ComposableCache_inner_TLBuffer_auto_in_1_c_bits_address),
    .auto_in_1_c_bits_data(ComposableCache_inner_TLBuffer_auto_in_1_c_bits_data),
    .auto_in_1_c_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_1_c_bits_corrupt),
    .auto_in_1_d_ready(ComposableCache_inner_TLBuffer_auto_in_1_d_ready),
    .auto_in_1_d_valid(ComposableCache_inner_TLBuffer_auto_in_1_d_valid),
    .auto_in_1_d_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_1_d_bits_opcode),
    .auto_in_1_d_bits_param(ComposableCache_inner_TLBuffer_auto_in_1_d_bits_param),
    .auto_in_1_d_bits_size(ComposableCache_inner_TLBuffer_auto_in_1_d_bits_size),
    .auto_in_1_d_bits_source(ComposableCache_inner_TLBuffer_auto_in_1_d_bits_source),
    .auto_in_1_d_bits_sink(ComposableCache_inner_TLBuffer_auto_in_1_d_bits_sink),
    .auto_in_1_d_bits_denied(ComposableCache_inner_TLBuffer_auto_in_1_d_bits_denied),
    .auto_in_1_d_bits_data(ComposableCache_inner_TLBuffer_auto_in_1_d_bits_data),
    .auto_in_1_d_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_1_d_bits_corrupt),
    .auto_in_1_e_ready(ComposableCache_inner_TLBuffer_auto_in_1_e_ready),
    .auto_in_1_e_valid(ComposableCache_inner_TLBuffer_auto_in_1_e_valid),
    .auto_in_1_e_bits_sink(ComposableCache_inner_TLBuffer_auto_in_1_e_bits_sink),
    .auto_in_0_a_ready(ComposableCache_inner_TLBuffer_auto_in_0_a_ready),
    .auto_in_0_a_valid(ComposableCache_inner_TLBuffer_auto_in_0_a_valid),
    .auto_in_0_a_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_0_a_bits_opcode),
    .auto_in_0_a_bits_param(ComposableCache_inner_TLBuffer_auto_in_0_a_bits_param),
    .auto_in_0_a_bits_size(ComposableCache_inner_TLBuffer_auto_in_0_a_bits_size),
    .auto_in_0_a_bits_source(ComposableCache_inner_TLBuffer_auto_in_0_a_bits_source),
    .auto_in_0_a_bits_address(ComposableCache_inner_TLBuffer_auto_in_0_a_bits_address),
    .auto_in_0_a_bits_mask(ComposableCache_inner_TLBuffer_auto_in_0_a_bits_mask),
    .auto_in_0_a_bits_data(ComposableCache_inner_TLBuffer_auto_in_0_a_bits_data),
    .auto_in_0_a_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_0_a_bits_corrupt),
    .auto_in_0_b_ready(ComposableCache_inner_TLBuffer_auto_in_0_b_ready),
    .auto_in_0_b_valid(ComposableCache_inner_TLBuffer_auto_in_0_b_valid),
    .auto_in_0_b_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_0_b_bits_opcode),
    .auto_in_0_b_bits_param(ComposableCache_inner_TLBuffer_auto_in_0_b_bits_param),
    .auto_in_0_b_bits_size(ComposableCache_inner_TLBuffer_auto_in_0_b_bits_size),
    .auto_in_0_b_bits_source(ComposableCache_inner_TLBuffer_auto_in_0_b_bits_source),
    .auto_in_0_b_bits_address(ComposableCache_inner_TLBuffer_auto_in_0_b_bits_address),
    .auto_in_0_b_bits_mask(ComposableCache_inner_TLBuffer_auto_in_0_b_bits_mask),
    .auto_in_0_b_bits_data(ComposableCache_inner_TLBuffer_auto_in_0_b_bits_data),
    .auto_in_0_b_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_0_b_bits_corrupt),
    .auto_in_0_c_ready(ComposableCache_inner_TLBuffer_auto_in_0_c_ready),
    .auto_in_0_c_valid(ComposableCache_inner_TLBuffer_auto_in_0_c_valid),
    .auto_in_0_c_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_0_c_bits_opcode),
    .auto_in_0_c_bits_param(ComposableCache_inner_TLBuffer_auto_in_0_c_bits_param),
    .auto_in_0_c_bits_size(ComposableCache_inner_TLBuffer_auto_in_0_c_bits_size),
    .auto_in_0_c_bits_source(ComposableCache_inner_TLBuffer_auto_in_0_c_bits_source),
    .auto_in_0_c_bits_address(ComposableCache_inner_TLBuffer_auto_in_0_c_bits_address),
    .auto_in_0_c_bits_data(ComposableCache_inner_TLBuffer_auto_in_0_c_bits_data),
    .auto_in_0_c_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_0_c_bits_corrupt),
    .auto_in_0_d_ready(ComposableCache_inner_TLBuffer_auto_in_0_d_ready),
    .auto_in_0_d_valid(ComposableCache_inner_TLBuffer_auto_in_0_d_valid),
    .auto_in_0_d_bits_opcode(ComposableCache_inner_TLBuffer_auto_in_0_d_bits_opcode),
    .auto_in_0_d_bits_param(ComposableCache_inner_TLBuffer_auto_in_0_d_bits_param),
    .auto_in_0_d_bits_size(ComposableCache_inner_TLBuffer_auto_in_0_d_bits_size),
    .auto_in_0_d_bits_source(ComposableCache_inner_TLBuffer_auto_in_0_d_bits_source),
    .auto_in_0_d_bits_sink(ComposableCache_inner_TLBuffer_auto_in_0_d_bits_sink),
    .auto_in_0_d_bits_denied(ComposableCache_inner_TLBuffer_auto_in_0_d_bits_denied),
    .auto_in_0_d_bits_data(ComposableCache_inner_TLBuffer_auto_in_0_d_bits_data),
    .auto_in_0_d_bits_corrupt(ComposableCache_inner_TLBuffer_auto_in_0_d_bits_corrupt),
    .auto_in_0_e_ready(ComposableCache_inner_TLBuffer_auto_in_0_e_ready),
    .auto_in_0_e_valid(ComposableCache_inner_TLBuffer_auto_in_0_e_valid),
    .auto_in_0_e_bits_sink(ComposableCache_inner_TLBuffer_auto_in_0_e_bits_sink),
    .auto_out_3_a_ready(ComposableCache_inner_TLBuffer_auto_out_3_a_ready),
    .auto_out_3_a_valid(ComposableCache_inner_TLBuffer_auto_out_3_a_valid),
    .auto_out_3_a_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_3_a_bits_opcode),
    .auto_out_3_a_bits_param(ComposableCache_inner_TLBuffer_auto_out_3_a_bits_param),
    .auto_out_3_a_bits_size(ComposableCache_inner_TLBuffer_auto_out_3_a_bits_size),
    .auto_out_3_a_bits_source(ComposableCache_inner_TLBuffer_auto_out_3_a_bits_source),
    .auto_out_3_a_bits_address(ComposableCache_inner_TLBuffer_auto_out_3_a_bits_address),
    .auto_out_3_a_bits_mask(ComposableCache_inner_TLBuffer_auto_out_3_a_bits_mask),
    .auto_out_3_a_bits_data(ComposableCache_inner_TLBuffer_auto_out_3_a_bits_data),
    .auto_out_3_a_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_3_a_bits_corrupt),
    .auto_out_3_b_ready(ComposableCache_inner_TLBuffer_auto_out_3_b_ready),
    .auto_out_3_b_valid(ComposableCache_inner_TLBuffer_auto_out_3_b_valid),
    .auto_out_3_b_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_3_b_bits_opcode),
    .auto_out_3_b_bits_param(ComposableCache_inner_TLBuffer_auto_out_3_b_bits_param),
    .auto_out_3_b_bits_size(ComposableCache_inner_TLBuffer_auto_out_3_b_bits_size),
    .auto_out_3_b_bits_source(ComposableCache_inner_TLBuffer_auto_out_3_b_bits_source),
    .auto_out_3_b_bits_address(ComposableCache_inner_TLBuffer_auto_out_3_b_bits_address),
    .auto_out_3_b_bits_mask(ComposableCache_inner_TLBuffer_auto_out_3_b_bits_mask),
    .auto_out_3_b_bits_data(ComposableCache_inner_TLBuffer_auto_out_3_b_bits_data),
    .auto_out_3_b_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_3_b_bits_corrupt),
    .auto_out_3_c_ready(ComposableCache_inner_TLBuffer_auto_out_3_c_ready),
    .auto_out_3_c_valid(ComposableCache_inner_TLBuffer_auto_out_3_c_valid),
    .auto_out_3_c_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_3_c_bits_opcode),
    .auto_out_3_c_bits_param(ComposableCache_inner_TLBuffer_auto_out_3_c_bits_param),
    .auto_out_3_c_bits_size(ComposableCache_inner_TLBuffer_auto_out_3_c_bits_size),
    .auto_out_3_c_bits_source(ComposableCache_inner_TLBuffer_auto_out_3_c_bits_source),
    .auto_out_3_c_bits_address(ComposableCache_inner_TLBuffer_auto_out_3_c_bits_address),
    .auto_out_3_c_bits_data(ComposableCache_inner_TLBuffer_auto_out_3_c_bits_data),
    .auto_out_3_c_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_3_c_bits_corrupt),
    .auto_out_3_d_ready(ComposableCache_inner_TLBuffer_auto_out_3_d_ready),
    .auto_out_3_d_valid(ComposableCache_inner_TLBuffer_auto_out_3_d_valid),
    .auto_out_3_d_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_3_d_bits_opcode),
    .auto_out_3_d_bits_param(ComposableCache_inner_TLBuffer_auto_out_3_d_bits_param),
    .auto_out_3_d_bits_size(ComposableCache_inner_TLBuffer_auto_out_3_d_bits_size),
    .auto_out_3_d_bits_source(ComposableCache_inner_TLBuffer_auto_out_3_d_bits_source),
    .auto_out_3_d_bits_sink(ComposableCache_inner_TLBuffer_auto_out_3_d_bits_sink),
    .auto_out_3_d_bits_denied(ComposableCache_inner_TLBuffer_auto_out_3_d_bits_denied),
    .auto_out_3_d_bits_data(ComposableCache_inner_TLBuffer_auto_out_3_d_bits_data),
    .auto_out_3_d_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_3_d_bits_corrupt),
    .auto_out_3_e_ready(ComposableCache_inner_TLBuffer_auto_out_3_e_ready),
    .auto_out_3_e_valid(ComposableCache_inner_TLBuffer_auto_out_3_e_valid),
    .auto_out_3_e_bits_sink(ComposableCache_inner_TLBuffer_auto_out_3_e_bits_sink),
    .auto_out_2_a_ready(ComposableCache_inner_TLBuffer_auto_out_2_a_ready),
    .auto_out_2_a_valid(ComposableCache_inner_TLBuffer_auto_out_2_a_valid),
    .auto_out_2_a_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_2_a_bits_opcode),
    .auto_out_2_a_bits_param(ComposableCache_inner_TLBuffer_auto_out_2_a_bits_param),
    .auto_out_2_a_bits_size(ComposableCache_inner_TLBuffer_auto_out_2_a_bits_size),
    .auto_out_2_a_bits_source(ComposableCache_inner_TLBuffer_auto_out_2_a_bits_source),
    .auto_out_2_a_bits_address(ComposableCache_inner_TLBuffer_auto_out_2_a_bits_address),
    .auto_out_2_a_bits_mask(ComposableCache_inner_TLBuffer_auto_out_2_a_bits_mask),
    .auto_out_2_a_bits_data(ComposableCache_inner_TLBuffer_auto_out_2_a_bits_data),
    .auto_out_2_a_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_2_a_bits_corrupt),
    .auto_out_2_b_ready(ComposableCache_inner_TLBuffer_auto_out_2_b_ready),
    .auto_out_2_b_valid(ComposableCache_inner_TLBuffer_auto_out_2_b_valid),
    .auto_out_2_b_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_2_b_bits_opcode),
    .auto_out_2_b_bits_param(ComposableCache_inner_TLBuffer_auto_out_2_b_bits_param),
    .auto_out_2_b_bits_size(ComposableCache_inner_TLBuffer_auto_out_2_b_bits_size),
    .auto_out_2_b_bits_source(ComposableCache_inner_TLBuffer_auto_out_2_b_bits_source),
    .auto_out_2_b_bits_address(ComposableCache_inner_TLBuffer_auto_out_2_b_bits_address),
    .auto_out_2_b_bits_mask(ComposableCache_inner_TLBuffer_auto_out_2_b_bits_mask),
    .auto_out_2_b_bits_data(ComposableCache_inner_TLBuffer_auto_out_2_b_bits_data),
    .auto_out_2_b_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_2_b_bits_corrupt),
    .auto_out_2_c_ready(ComposableCache_inner_TLBuffer_auto_out_2_c_ready),
    .auto_out_2_c_valid(ComposableCache_inner_TLBuffer_auto_out_2_c_valid),
    .auto_out_2_c_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_2_c_bits_opcode),
    .auto_out_2_c_bits_param(ComposableCache_inner_TLBuffer_auto_out_2_c_bits_param),
    .auto_out_2_c_bits_size(ComposableCache_inner_TLBuffer_auto_out_2_c_bits_size),
    .auto_out_2_c_bits_source(ComposableCache_inner_TLBuffer_auto_out_2_c_bits_source),
    .auto_out_2_c_bits_address(ComposableCache_inner_TLBuffer_auto_out_2_c_bits_address),
    .auto_out_2_c_bits_data(ComposableCache_inner_TLBuffer_auto_out_2_c_bits_data),
    .auto_out_2_c_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_2_c_bits_corrupt),
    .auto_out_2_d_ready(ComposableCache_inner_TLBuffer_auto_out_2_d_ready),
    .auto_out_2_d_valid(ComposableCache_inner_TLBuffer_auto_out_2_d_valid),
    .auto_out_2_d_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_2_d_bits_opcode),
    .auto_out_2_d_bits_param(ComposableCache_inner_TLBuffer_auto_out_2_d_bits_param),
    .auto_out_2_d_bits_size(ComposableCache_inner_TLBuffer_auto_out_2_d_bits_size),
    .auto_out_2_d_bits_source(ComposableCache_inner_TLBuffer_auto_out_2_d_bits_source),
    .auto_out_2_d_bits_sink(ComposableCache_inner_TLBuffer_auto_out_2_d_bits_sink),
    .auto_out_2_d_bits_denied(ComposableCache_inner_TLBuffer_auto_out_2_d_bits_denied),
    .auto_out_2_d_bits_data(ComposableCache_inner_TLBuffer_auto_out_2_d_bits_data),
    .auto_out_2_d_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_2_d_bits_corrupt),
    .auto_out_2_e_ready(ComposableCache_inner_TLBuffer_auto_out_2_e_ready),
    .auto_out_2_e_valid(ComposableCache_inner_TLBuffer_auto_out_2_e_valid),
    .auto_out_2_e_bits_sink(ComposableCache_inner_TLBuffer_auto_out_2_e_bits_sink),
    .auto_out_1_a_ready(ComposableCache_inner_TLBuffer_auto_out_1_a_ready),
    .auto_out_1_a_valid(ComposableCache_inner_TLBuffer_auto_out_1_a_valid),
    .auto_out_1_a_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_1_a_bits_opcode),
    .auto_out_1_a_bits_param(ComposableCache_inner_TLBuffer_auto_out_1_a_bits_param),
    .auto_out_1_a_bits_size(ComposableCache_inner_TLBuffer_auto_out_1_a_bits_size),
    .auto_out_1_a_bits_source(ComposableCache_inner_TLBuffer_auto_out_1_a_bits_source),
    .auto_out_1_a_bits_address(ComposableCache_inner_TLBuffer_auto_out_1_a_bits_address),
    .auto_out_1_a_bits_mask(ComposableCache_inner_TLBuffer_auto_out_1_a_bits_mask),
    .auto_out_1_a_bits_data(ComposableCache_inner_TLBuffer_auto_out_1_a_bits_data),
    .auto_out_1_a_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_1_a_bits_corrupt),
    .auto_out_1_b_ready(ComposableCache_inner_TLBuffer_auto_out_1_b_ready),
    .auto_out_1_b_valid(ComposableCache_inner_TLBuffer_auto_out_1_b_valid),
    .auto_out_1_b_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_1_b_bits_opcode),
    .auto_out_1_b_bits_param(ComposableCache_inner_TLBuffer_auto_out_1_b_bits_param),
    .auto_out_1_b_bits_size(ComposableCache_inner_TLBuffer_auto_out_1_b_bits_size),
    .auto_out_1_b_bits_source(ComposableCache_inner_TLBuffer_auto_out_1_b_bits_source),
    .auto_out_1_b_bits_address(ComposableCache_inner_TLBuffer_auto_out_1_b_bits_address),
    .auto_out_1_b_bits_mask(ComposableCache_inner_TLBuffer_auto_out_1_b_bits_mask),
    .auto_out_1_b_bits_data(ComposableCache_inner_TLBuffer_auto_out_1_b_bits_data),
    .auto_out_1_b_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_1_b_bits_corrupt),
    .auto_out_1_c_ready(ComposableCache_inner_TLBuffer_auto_out_1_c_ready),
    .auto_out_1_c_valid(ComposableCache_inner_TLBuffer_auto_out_1_c_valid),
    .auto_out_1_c_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_1_c_bits_opcode),
    .auto_out_1_c_bits_param(ComposableCache_inner_TLBuffer_auto_out_1_c_bits_param),
    .auto_out_1_c_bits_size(ComposableCache_inner_TLBuffer_auto_out_1_c_bits_size),
    .auto_out_1_c_bits_source(ComposableCache_inner_TLBuffer_auto_out_1_c_bits_source),
    .auto_out_1_c_bits_address(ComposableCache_inner_TLBuffer_auto_out_1_c_bits_address),
    .auto_out_1_c_bits_data(ComposableCache_inner_TLBuffer_auto_out_1_c_bits_data),
    .auto_out_1_c_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_1_c_bits_corrupt),
    .auto_out_1_d_ready(ComposableCache_inner_TLBuffer_auto_out_1_d_ready),
    .auto_out_1_d_valid(ComposableCache_inner_TLBuffer_auto_out_1_d_valid),
    .auto_out_1_d_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_1_d_bits_opcode),
    .auto_out_1_d_bits_param(ComposableCache_inner_TLBuffer_auto_out_1_d_bits_param),
    .auto_out_1_d_bits_size(ComposableCache_inner_TLBuffer_auto_out_1_d_bits_size),
    .auto_out_1_d_bits_source(ComposableCache_inner_TLBuffer_auto_out_1_d_bits_source),
    .auto_out_1_d_bits_sink(ComposableCache_inner_TLBuffer_auto_out_1_d_bits_sink),
    .auto_out_1_d_bits_denied(ComposableCache_inner_TLBuffer_auto_out_1_d_bits_denied),
    .auto_out_1_d_bits_data(ComposableCache_inner_TLBuffer_auto_out_1_d_bits_data),
    .auto_out_1_d_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_1_d_bits_corrupt),
    .auto_out_1_e_ready(ComposableCache_inner_TLBuffer_auto_out_1_e_ready),
    .auto_out_1_e_valid(ComposableCache_inner_TLBuffer_auto_out_1_e_valid),
    .auto_out_1_e_bits_sink(ComposableCache_inner_TLBuffer_auto_out_1_e_bits_sink),
    .auto_out_0_a_ready(ComposableCache_inner_TLBuffer_auto_out_0_a_ready),
    .auto_out_0_a_valid(ComposableCache_inner_TLBuffer_auto_out_0_a_valid),
    .auto_out_0_a_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_0_a_bits_opcode),
    .auto_out_0_a_bits_param(ComposableCache_inner_TLBuffer_auto_out_0_a_bits_param),
    .auto_out_0_a_bits_size(ComposableCache_inner_TLBuffer_auto_out_0_a_bits_size),
    .auto_out_0_a_bits_source(ComposableCache_inner_TLBuffer_auto_out_0_a_bits_source),
    .auto_out_0_a_bits_address(ComposableCache_inner_TLBuffer_auto_out_0_a_bits_address),
    .auto_out_0_a_bits_mask(ComposableCache_inner_TLBuffer_auto_out_0_a_bits_mask),
    .auto_out_0_a_bits_data(ComposableCache_inner_TLBuffer_auto_out_0_a_bits_data),
    .auto_out_0_a_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_0_a_bits_corrupt),
    .auto_out_0_b_ready(ComposableCache_inner_TLBuffer_auto_out_0_b_ready),
    .auto_out_0_b_valid(ComposableCache_inner_TLBuffer_auto_out_0_b_valid),
    .auto_out_0_b_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_0_b_bits_opcode),
    .auto_out_0_b_bits_param(ComposableCache_inner_TLBuffer_auto_out_0_b_bits_param),
    .auto_out_0_b_bits_size(ComposableCache_inner_TLBuffer_auto_out_0_b_bits_size),
    .auto_out_0_b_bits_source(ComposableCache_inner_TLBuffer_auto_out_0_b_bits_source),
    .auto_out_0_b_bits_address(ComposableCache_inner_TLBuffer_auto_out_0_b_bits_address),
    .auto_out_0_b_bits_mask(ComposableCache_inner_TLBuffer_auto_out_0_b_bits_mask),
    .auto_out_0_b_bits_data(ComposableCache_inner_TLBuffer_auto_out_0_b_bits_data),
    .auto_out_0_b_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_0_b_bits_corrupt),
    .auto_out_0_c_ready(ComposableCache_inner_TLBuffer_auto_out_0_c_ready),
    .auto_out_0_c_valid(ComposableCache_inner_TLBuffer_auto_out_0_c_valid),
    .auto_out_0_c_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_0_c_bits_opcode),
    .auto_out_0_c_bits_param(ComposableCache_inner_TLBuffer_auto_out_0_c_bits_param),
    .auto_out_0_c_bits_size(ComposableCache_inner_TLBuffer_auto_out_0_c_bits_size),
    .auto_out_0_c_bits_source(ComposableCache_inner_TLBuffer_auto_out_0_c_bits_source),
    .auto_out_0_c_bits_address(ComposableCache_inner_TLBuffer_auto_out_0_c_bits_address),
    .auto_out_0_c_bits_data(ComposableCache_inner_TLBuffer_auto_out_0_c_bits_data),
    .auto_out_0_c_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_0_c_bits_corrupt),
    .auto_out_0_d_ready(ComposableCache_inner_TLBuffer_auto_out_0_d_ready),
    .auto_out_0_d_valid(ComposableCache_inner_TLBuffer_auto_out_0_d_valid),
    .auto_out_0_d_bits_opcode(ComposableCache_inner_TLBuffer_auto_out_0_d_bits_opcode),
    .auto_out_0_d_bits_param(ComposableCache_inner_TLBuffer_auto_out_0_d_bits_param),
    .auto_out_0_d_bits_size(ComposableCache_inner_TLBuffer_auto_out_0_d_bits_size),
    .auto_out_0_d_bits_source(ComposableCache_inner_TLBuffer_auto_out_0_d_bits_source),
    .auto_out_0_d_bits_sink(ComposableCache_inner_TLBuffer_auto_out_0_d_bits_sink),
    .auto_out_0_d_bits_denied(ComposableCache_inner_TLBuffer_auto_out_0_d_bits_denied),
    .auto_out_0_d_bits_data(ComposableCache_inner_TLBuffer_auto_out_0_d_bits_data),
    .auto_out_0_d_bits_corrupt(ComposableCache_inner_TLBuffer_auto_out_0_d_bits_corrupt),
    .auto_out_0_e_ready(ComposableCache_inner_TLBuffer_auto_out_0_e_ready),
    .auto_out_0_e_valid(ComposableCache_inner_TLBuffer_auto_out_0_e_valid),
    .auto_out_0_e_bits_sink(ComposableCache_inner_TLBuffer_auto_out_0_e_bits_sink)
  );
  RHEA__TLCacheCork cork ( // @[Configs.scala 246:26]
    .rf_reset(cork_rf_reset),
    .clock(cork_clock),
    .reset(cork_reset),
    .auto_in_3_a_ready(cork_auto_in_3_a_ready),
    .auto_in_3_a_valid(cork_auto_in_3_a_valid),
    .auto_in_3_a_bits_opcode(cork_auto_in_3_a_bits_opcode),
    .auto_in_3_a_bits_param(cork_auto_in_3_a_bits_param),
    .auto_in_3_a_bits_source(cork_auto_in_3_a_bits_source),
    .auto_in_3_a_bits_address(cork_auto_in_3_a_bits_address),
    .auto_in_3_c_ready(cork_auto_in_3_c_ready),
    .auto_in_3_c_valid(cork_auto_in_3_c_valid),
    .auto_in_3_c_bits_opcode(cork_auto_in_3_c_bits_opcode),
    .auto_in_3_c_bits_param(cork_auto_in_3_c_bits_param),
    .auto_in_3_c_bits_source(cork_auto_in_3_c_bits_source),
    .auto_in_3_c_bits_address(cork_auto_in_3_c_bits_address),
    .auto_in_3_c_bits_data(cork_auto_in_3_c_bits_data),
    .auto_in_3_d_ready(cork_auto_in_3_d_ready),
    .auto_in_3_d_valid(cork_auto_in_3_d_valid),
    .auto_in_3_d_bits_opcode(cork_auto_in_3_d_bits_opcode),
    .auto_in_3_d_bits_param(cork_auto_in_3_d_bits_param),
    .auto_in_3_d_bits_size(cork_auto_in_3_d_bits_size),
    .auto_in_3_d_bits_source(cork_auto_in_3_d_bits_source),
    .auto_in_3_d_bits_sink(cork_auto_in_3_d_bits_sink),
    .auto_in_3_d_bits_denied(cork_auto_in_3_d_bits_denied),
    .auto_in_3_d_bits_data(cork_auto_in_3_d_bits_data),
    .auto_in_3_d_bits_corrupt(cork_auto_in_3_d_bits_corrupt),
    .auto_in_3_e_valid(cork_auto_in_3_e_valid),
    .auto_in_3_e_bits_sink(cork_auto_in_3_e_bits_sink),
    .auto_in_2_a_ready(cork_auto_in_2_a_ready),
    .auto_in_2_a_valid(cork_auto_in_2_a_valid),
    .auto_in_2_a_bits_opcode(cork_auto_in_2_a_bits_opcode),
    .auto_in_2_a_bits_param(cork_auto_in_2_a_bits_param),
    .auto_in_2_a_bits_source(cork_auto_in_2_a_bits_source),
    .auto_in_2_a_bits_address(cork_auto_in_2_a_bits_address),
    .auto_in_2_c_ready(cork_auto_in_2_c_ready),
    .auto_in_2_c_valid(cork_auto_in_2_c_valid),
    .auto_in_2_c_bits_opcode(cork_auto_in_2_c_bits_opcode),
    .auto_in_2_c_bits_param(cork_auto_in_2_c_bits_param),
    .auto_in_2_c_bits_source(cork_auto_in_2_c_bits_source),
    .auto_in_2_c_bits_address(cork_auto_in_2_c_bits_address),
    .auto_in_2_c_bits_data(cork_auto_in_2_c_bits_data),
    .auto_in_2_d_ready(cork_auto_in_2_d_ready),
    .auto_in_2_d_valid(cork_auto_in_2_d_valid),
    .auto_in_2_d_bits_opcode(cork_auto_in_2_d_bits_opcode),
    .auto_in_2_d_bits_param(cork_auto_in_2_d_bits_param),
    .auto_in_2_d_bits_size(cork_auto_in_2_d_bits_size),
    .auto_in_2_d_bits_source(cork_auto_in_2_d_bits_source),
    .auto_in_2_d_bits_sink(cork_auto_in_2_d_bits_sink),
    .auto_in_2_d_bits_denied(cork_auto_in_2_d_bits_denied),
    .auto_in_2_d_bits_data(cork_auto_in_2_d_bits_data),
    .auto_in_2_d_bits_corrupt(cork_auto_in_2_d_bits_corrupt),
    .auto_in_2_e_valid(cork_auto_in_2_e_valid),
    .auto_in_2_e_bits_sink(cork_auto_in_2_e_bits_sink),
    .auto_in_1_a_ready(cork_auto_in_1_a_ready),
    .auto_in_1_a_valid(cork_auto_in_1_a_valid),
    .auto_in_1_a_bits_opcode(cork_auto_in_1_a_bits_opcode),
    .auto_in_1_a_bits_param(cork_auto_in_1_a_bits_param),
    .auto_in_1_a_bits_source(cork_auto_in_1_a_bits_source),
    .auto_in_1_a_bits_address(cork_auto_in_1_a_bits_address),
    .auto_in_1_c_ready(cork_auto_in_1_c_ready),
    .auto_in_1_c_valid(cork_auto_in_1_c_valid),
    .auto_in_1_c_bits_opcode(cork_auto_in_1_c_bits_opcode),
    .auto_in_1_c_bits_param(cork_auto_in_1_c_bits_param),
    .auto_in_1_c_bits_source(cork_auto_in_1_c_bits_source),
    .auto_in_1_c_bits_address(cork_auto_in_1_c_bits_address),
    .auto_in_1_c_bits_data(cork_auto_in_1_c_bits_data),
    .auto_in_1_d_ready(cork_auto_in_1_d_ready),
    .auto_in_1_d_valid(cork_auto_in_1_d_valid),
    .auto_in_1_d_bits_opcode(cork_auto_in_1_d_bits_opcode),
    .auto_in_1_d_bits_param(cork_auto_in_1_d_bits_param),
    .auto_in_1_d_bits_size(cork_auto_in_1_d_bits_size),
    .auto_in_1_d_bits_source(cork_auto_in_1_d_bits_source),
    .auto_in_1_d_bits_sink(cork_auto_in_1_d_bits_sink),
    .auto_in_1_d_bits_denied(cork_auto_in_1_d_bits_denied),
    .auto_in_1_d_bits_data(cork_auto_in_1_d_bits_data),
    .auto_in_1_d_bits_corrupt(cork_auto_in_1_d_bits_corrupt),
    .auto_in_1_e_valid(cork_auto_in_1_e_valid),
    .auto_in_1_e_bits_sink(cork_auto_in_1_e_bits_sink),
    .auto_in_0_a_ready(cork_auto_in_0_a_ready),
    .auto_in_0_a_valid(cork_auto_in_0_a_valid),
    .auto_in_0_a_bits_opcode(cork_auto_in_0_a_bits_opcode),
    .auto_in_0_a_bits_param(cork_auto_in_0_a_bits_param),
    .auto_in_0_a_bits_source(cork_auto_in_0_a_bits_source),
    .auto_in_0_a_bits_address(cork_auto_in_0_a_bits_address),
    .auto_in_0_c_ready(cork_auto_in_0_c_ready),
    .auto_in_0_c_valid(cork_auto_in_0_c_valid),
    .auto_in_0_c_bits_opcode(cork_auto_in_0_c_bits_opcode),
    .auto_in_0_c_bits_param(cork_auto_in_0_c_bits_param),
    .auto_in_0_c_bits_source(cork_auto_in_0_c_bits_source),
    .auto_in_0_c_bits_address(cork_auto_in_0_c_bits_address),
    .auto_in_0_c_bits_data(cork_auto_in_0_c_bits_data),
    .auto_in_0_d_ready(cork_auto_in_0_d_ready),
    .auto_in_0_d_valid(cork_auto_in_0_d_valid),
    .auto_in_0_d_bits_opcode(cork_auto_in_0_d_bits_opcode),
    .auto_in_0_d_bits_param(cork_auto_in_0_d_bits_param),
    .auto_in_0_d_bits_size(cork_auto_in_0_d_bits_size),
    .auto_in_0_d_bits_source(cork_auto_in_0_d_bits_source),
    .auto_in_0_d_bits_sink(cork_auto_in_0_d_bits_sink),
    .auto_in_0_d_bits_denied(cork_auto_in_0_d_bits_denied),
    .auto_in_0_d_bits_data(cork_auto_in_0_d_bits_data),
    .auto_in_0_d_bits_corrupt(cork_auto_in_0_d_bits_corrupt),
    .auto_in_0_e_valid(cork_auto_in_0_e_valid),
    .auto_in_0_e_bits_sink(cork_auto_in_0_e_bits_sink),
    .auto_out_3_a_ready(cork_auto_out_3_a_ready),
    .auto_out_3_a_valid(cork_auto_out_3_a_valid),
    .auto_out_3_a_bits_opcode(cork_auto_out_3_a_bits_opcode),
    .auto_out_3_a_bits_param(cork_auto_out_3_a_bits_param),
    .auto_out_3_a_bits_size(cork_auto_out_3_a_bits_size),
    .auto_out_3_a_bits_source(cork_auto_out_3_a_bits_source),
    .auto_out_3_a_bits_address(cork_auto_out_3_a_bits_address),
    .auto_out_3_a_bits_user_amba_prot_bufferable(cork_auto_out_3_a_bits_user_amba_prot_bufferable),
    .auto_out_3_a_bits_user_amba_prot_modifiable(cork_auto_out_3_a_bits_user_amba_prot_modifiable),
    .auto_out_3_a_bits_user_amba_prot_readalloc(cork_auto_out_3_a_bits_user_amba_prot_readalloc),
    .auto_out_3_a_bits_user_amba_prot_writealloc(cork_auto_out_3_a_bits_user_amba_prot_writealloc),
    .auto_out_3_a_bits_user_amba_prot_privileged(cork_auto_out_3_a_bits_user_amba_prot_privileged),
    .auto_out_3_a_bits_user_amba_prot_secure(cork_auto_out_3_a_bits_user_amba_prot_secure),
    .auto_out_3_a_bits_mask(cork_auto_out_3_a_bits_mask),
    .auto_out_3_a_bits_data(cork_auto_out_3_a_bits_data),
    .auto_out_3_d_ready(cork_auto_out_3_d_ready),
    .auto_out_3_d_valid(cork_auto_out_3_d_valid),
    .auto_out_3_d_bits_opcode(cork_auto_out_3_d_bits_opcode),
    .auto_out_3_d_bits_size(cork_auto_out_3_d_bits_size),
    .auto_out_3_d_bits_source(cork_auto_out_3_d_bits_source),
    .auto_out_3_d_bits_denied(cork_auto_out_3_d_bits_denied),
    .auto_out_3_d_bits_data(cork_auto_out_3_d_bits_data),
    .auto_out_3_d_bits_corrupt(cork_auto_out_3_d_bits_corrupt),
    .auto_out_2_a_ready(cork_auto_out_2_a_ready),
    .auto_out_2_a_valid(cork_auto_out_2_a_valid),
    .auto_out_2_a_bits_opcode(cork_auto_out_2_a_bits_opcode),
    .auto_out_2_a_bits_param(cork_auto_out_2_a_bits_param),
    .auto_out_2_a_bits_size(cork_auto_out_2_a_bits_size),
    .auto_out_2_a_bits_source(cork_auto_out_2_a_bits_source),
    .auto_out_2_a_bits_address(cork_auto_out_2_a_bits_address),
    .auto_out_2_a_bits_user_amba_prot_bufferable(cork_auto_out_2_a_bits_user_amba_prot_bufferable),
    .auto_out_2_a_bits_user_amba_prot_modifiable(cork_auto_out_2_a_bits_user_amba_prot_modifiable),
    .auto_out_2_a_bits_user_amba_prot_readalloc(cork_auto_out_2_a_bits_user_amba_prot_readalloc),
    .auto_out_2_a_bits_user_amba_prot_writealloc(cork_auto_out_2_a_bits_user_amba_prot_writealloc),
    .auto_out_2_a_bits_user_amba_prot_privileged(cork_auto_out_2_a_bits_user_amba_prot_privileged),
    .auto_out_2_a_bits_user_amba_prot_secure(cork_auto_out_2_a_bits_user_amba_prot_secure),
    .auto_out_2_a_bits_mask(cork_auto_out_2_a_bits_mask),
    .auto_out_2_a_bits_data(cork_auto_out_2_a_bits_data),
    .auto_out_2_d_ready(cork_auto_out_2_d_ready),
    .auto_out_2_d_valid(cork_auto_out_2_d_valid),
    .auto_out_2_d_bits_opcode(cork_auto_out_2_d_bits_opcode),
    .auto_out_2_d_bits_size(cork_auto_out_2_d_bits_size),
    .auto_out_2_d_bits_source(cork_auto_out_2_d_bits_source),
    .auto_out_2_d_bits_denied(cork_auto_out_2_d_bits_denied),
    .auto_out_2_d_bits_data(cork_auto_out_2_d_bits_data),
    .auto_out_2_d_bits_corrupt(cork_auto_out_2_d_bits_corrupt),
    .auto_out_1_a_ready(cork_auto_out_1_a_ready),
    .auto_out_1_a_valid(cork_auto_out_1_a_valid),
    .auto_out_1_a_bits_opcode(cork_auto_out_1_a_bits_opcode),
    .auto_out_1_a_bits_param(cork_auto_out_1_a_bits_param),
    .auto_out_1_a_bits_size(cork_auto_out_1_a_bits_size),
    .auto_out_1_a_bits_source(cork_auto_out_1_a_bits_source),
    .auto_out_1_a_bits_address(cork_auto_out_1_a_bits_address),
    .auto_out_1_a_bits_user_amba_prot_bufferable(cork_auto_out_1_a_bits_user_amba_prot_bufferable),
    .auto_out_1_a_bits_user_amba_prot_modifiable(cork_auto_out_1_a_bits_user_amba_prot_modifiable),
    .auto_out_1_a_bits_user_amba_prot_readalloc(cork_auto_out_1_a_bits_user_amba_prot_readalloc),
    .auto_out_1_a_bits_user_amba_prot_writealloc(cork_auto_out_1_a_bits_user_amba_prot_writealloc),
    .auto_out_1_a_bits_user_amba_prot_privileged(cork_auto_out_1_a_bits_user_amba_prot_privileged),
    .auto_out_1_a_bits_user_amba_prot_secure(cork_auto_out_1_a_bits_user_amba_prot_secure),
    .auto_out_1_a_bits_mask(cork_auto_out_1_a_bits_mask),
    .auto_out_1_a_bits_data(cork_auto_out_1_a_bits_data),
    .auto_out_1_d_ready(cork_auto_out_1_d_ready),
    .auto_out_1_d_valid(cork_auto_out_1_d_valid),
    .auto_out_1_d_bits_opcode(cork_auto_out_1_d_bits_opcode),
    .auto_out_1_d_bits_size(cork_auto_out_1_d_bits_size),
    .auto_out_1_d_bits_source(cork_auto_out_1_d_bits_source),
    .auto_out_1_d_bits_denied(cork_auto_out_1_d_bits_denied),
    .auto_out_1_d_bits_data(cork_auto_out_1_d_bits_data),
    .auto_out_1_d_bits_corrupt(cork_auto_out_1_d_bits_corrupt),
    .auto_out_0_a_ready(cork_auto_out_0_a_ready),
    .auto_out_0_a_valid(cork_auto_out_0_a_valid),
    .auto_out_0_a_bits_opcode(cork_auto_out_0_a_bits_opcode),
    .auto_out_0_a_bits_param(cork_auto_out_0_a_bits_param),
    .auto_out_0_a_bits_size(cork_auto_out_0_a_bits_size),
    .auto_out_0_a_bits_source(cork_auto_out_0_a_bits_source),
    .auto_out_0_a_bits_address(cork_auto_out_0_a_bits_address),
    .auto_out_0_a_bits_user_amba_prot_bufferable(cork_auto_out_0_a_bits_user_amba_prot_bufferable),
    .auto_out_0_a_bits_user_amba_prot_modifiable(cork_auto_out_0_a_bits_user_amba_prot_modifiable),
    .auto_out_0_a_bits_user_amba_prot_readalloc(cork_auto_out_0_a_bits_user_amba_prot_readalloc),
    .auto_out_0_a_bits_user_amba_prot_writealloc(cork_auto_out_0_a_bits_user_amba_prot_writealloc),
    .auto_out_0_a_bits_user_amba_prot_privileged(cork_auto_out_0_a_bits_user_amba_prot_privileged),
    .auto_out_0_a_bits_user_amba_prot_secure(cork_auto_out_0_a_bits_user_amba_prot_secure),
    .auto_out_0_a_bits_mask(cork_auto_out_0_a_bits_mask),
    .auto_out_0_a_bits_data(cork_auto_out_0_a_bits_data),
    .auto_out_0_d_ready(cork_auto_out_0_d_ready),
    .auto_out_0_d_valid(cork_auto_out_0_d_valid),
    .auto_out_0_d_bits_opcode(cork_auto_out_0_d_bits_opcode),
    .auto_out_0_d_bits_size(cork_auto_out_0_d_bits_size),
    .auto_out_0_d_bits_source(cork_auto_out_0_d_bits_source),
    .auto_out_0_d_bits_denied(cork_auto_out_0_d_bits_denied),
    .auto_out_0_d_bits_data(cork_auto_out_0_d_bits_data),
    .auto_out_0_d_bits_corrupt(cork_auto_out_0_d_bits_corrupt)
  );
  RHEA__BankBinder binder ( // @[BankBinder.scala 67:28]
    .rf_reset(binder_rf_reset),
    .clock(binder_clock),
    .reset(binder_reset),
    .auto_in_3_a_ready(binder_auto_in_3_a_ready),
    .auto_in_3_a_valid(binder_auto_in_3_a_valid),
    .auto_in_3_a_bits_opcode(binder_auto_in_3_a_bits_opcode),
    .auto_in_3_a_bits_param(binder_auto_in_3_a_bits_param),
    .auto_in_3_a_bits_size(binder_auto_in_3_a_bits_size),
    .auto_in_3_a_bits_source(binder_auto_in_3_a_bits_source),
    .auto_in_3_a_bits_address(binder_auto_in_3_a_bits_address),
    .auto_in_3_a_bits_user_amba_prot_bufferable(binder_auto_in_3_a_bits_user_amba_prot_bufferable),
    .auto_in_3_a_bits_user_amba_prot_modifiable(binder_auto_in_3_a_bits_user_amba_prot_modifiable),
    .auto_in_3_a_bits_user_amba_prot_readalloc(binder_auto_in_3_a_bits_user_amba_prot_readalloc),
    .auto_in_3_a_bits_user_amba_prot_writealloc(binder_auto_in_3_a_bits_user_amba_prot_writealloc),
    .auto_in_3_a_bits_user_amba_prot_privileged(binder_auto_in_3_a_bits_user_amba_prot_privileged),
    .auto_in_3_a_bits_user_amba_prot_secure(binder_auto_in_3_a_bits_user_amba_prot_secure),
    .auto_in_3_a_bits_mask(binder_auto_in_3_a_bits_mask),
    .auto_in_3_a_bits_data(binder_auto_in_3_a_bits_data),
    .auto_in_3_d_ready(binder_auto_in_3_d_ready),
    .auto_in_3_d_valid(binder_auto_in_3_d_valid),
    .auto_in_3_d_bits_opcode(binder_auto_in_3_d_bits_opcode),
    .auto_in_3_d_bits_size(binder_auto_in_3_d_bits_size),
    .auto_in_3_d_bits_source(binder_auto_in_3_d_bits_source),
    .auto_in_3_d_bits_denied(binder_auto_in_3_d_bits_denied),
    .auto_in_3_d_bits_data(binder_auto_in_3_d_bits_data),
    .auto_in_3_d_bits_corrupt(binder_auto_in_3_d_bits_corrupt),
    .auto_in_2_a_ready(binder_auto_in_2_a_ready),
    .auto_in_2_a_valid(binder_auto_in_2_a_valid),
    .auto_in_2_a_bits_opcode(binder_auto_in_2_a_bits_opcode),
    .auto_in_2_a_bits_param(binder_auto_in_2_a_bits_param),
    .auto_in_2_a_bits_size(binder_auto_in_2_a_bits_size),
    .auto_in_2_a_bits_source(binder_auto_in_2_a_bits_source),
    .auto_in_2_a_bits_address(binder_auto_in_2_a_bits_address),
    .auto_in_2_a_bits_user_amba_prot_bufferable(binder_auto_in_2_a_bits_user_amba_prot_bufferable),
    .auto_in_2_a_bits_user_amba_prot_modifiable(binder_auto_in_2_a_bits_user_amba_prot_modifiable),
    .auto_in_2_a_bits_user_amba_prot_readalloc(binder_auto_in_2_a_bits_user_amba_prot_readalloc),
    .auto_in_2_a_bits_user_amba_prot_writealloc(binder_auto_in_2_a_bits_user_amba_prot_writealloc),
    .auto_in_2_a_bits_user_amba_prot_privileged(binder_auto_in_2_a_bits_user_amba_prot_privileged),
    .auto_in_2_a_bits_user_amba_prot_secure(binder_auto_in_2_a_bits_user_amba_prot_secure),
    .auto_in_2_a_bits_mask(binder_auto_in_2_a_bits_mask),
    .auto_in_2_a_bits_data(binder_auto_in_2_a_bits_data),
    .auto_in_2_d_ready(binder_auto_in_2_d_ready),
    .auto_in_2_d_valid(binder_auto_in_2_d_valid),
    .auto_in_2_d_bits_opcode(binder_auto_in_2_d_bits_opcode),
    .auto_in_2_d_bits_size(binder_auto_in_2_d_bits_size),
    .auto_in_2_d_bits_source(binder_auto_in_2_d_bits_source),
    .auto_in_2_d_bits_denied(binder_auto_in_2_d_bits_denied),
    .auto_in_2_d_bits_data(binder_auto_in_2_d_bits_data),
    .auto_in_2_d_bits_corrupt(binder_auto_in_2_d_bits_corrupt),
    .auto_in_1_a_ready(binder_auto_in_1_a_ready),
    .auto_in_1_a_valid(binder_auto_in_1_a_valid),
    .auto_in_1_a_bits_opcode(binder_auto_in_1_a_bits_opcode),
    .auto_in_1_a_bits_param(binder_auto_in_1_a_bits_param),
    .auto_in_1_a_bits_size(binder_auto_in_1_a_bits_size),
    .auto_in_1_a_bits_source(binder_auto_in_1_a_bits_source),
    .auto_in_1_a_bits_address(binder_auto_in_1_a_bits_address),
    .auto_in_1_a_bits_user_amba_prot_bufferable(binder_auto_in_1_a_bits_user_amba_prot_bufferable),
    .auto_in_1_a_bits_user_amba_prot_modifiable(binder_auto_in_1_a_bits_user_amba_prot_modifiable),
    .auto_in_1_a_bits_user_amba_prot_readalloc(binder_auto_in_1_a_bits_user_amba_prot_readalloc),
    .auto_in_1_a_bits_user_amba_prot_writealloc(binder_auto_in_1_a_bits_user_amba_prot_writealloc),
    .auto_in_1_a_bits_user_amba_prot_privileged(binder_auto_in_1_a_bits_user_amba_prot_privileged),
    .auto_in_1_a_bits_user_amba_prot_secure(binder_auto_in_1_a_bits_user_amba_prot_secure),
    .auto_in_1_a_bits_mask(binder_auto_in_1_a_bits_mask),
    .auto_in_1_a_bits_data(binder_auto_in_1_a_bits_data),
    .auto_in_1_d_ready(binder_auto_in_1_d_ready),
    .auto_in_1_d_valid(binder_auto_in_1_d_valid),
    .auto_in_1_d_bits_opcode(binder_auto_in_1_d_bits_opcode),
    .auto_in_1_d_bits_size(binder_auto_in_1_d_bits_size),
    .auto_in_1_d_bits_source(binder_auto_in_1_d_bits_source),
    .auto_in_1_d_bits_denied(binder_auto_in_1_d_bits_denied),
    .auto_in_1_d_bits_data(binder_auto_in_1_d_bits_data),
    .auto_in_1_d_bits_corrupt(binder_auto_in_1_d_bits_corrupt),
    .auto_in_0_a_ready(binder_auto_in_0_a_ready),
    .auto_in_0_a_valid(binder_auto_in_0_a_valid),
    .auto_in_0_a_bits_opcode(binder_auto_in_0_a_bits_opcode),
    .auto_in_0_a_bits_param(binder_auto_in_0_a_bits_param),
    .auto_in_0_a_bits_size(binder_auto_in_0_a_bits_size),
    .auto_in_0_a_bits_source(binder_auto_in_0_a_bits_source),
    .auto_in_0_a_bits_address(binder_auto_in_0_a_bits_address),
    .auto_in_0_a_bits_user_amba_prot_bufferable(binder_auto_in_0_a_bits_user_amba_prot_bufferable),
    .auto_in_0_a_bits_user_amba_prot_modifiable(binder_auto_in_0_a_bits_user_amba_prot_modifiable),
    .auto_in_0_a_bits_user_amba_prot_readalloc(binder_auto_in_0_a_bits_user_amba_prot_readalloc),
    .auto_in_0_a_bits_user_amba_prot_writealloc(binder_auto_in_0_a_bits_user_amba_prot_writealloc),
    .auto_in_0_a_bits_user_amba_prot_privileged(binder_auto_in_0_a_bits_user_amba_prot_privileged),
    .auto_in_0_a_bits_user_amba_prot_secure(binder_auto_in_0_a_bits_user_amba_prot_secure),
    .auto_in_0_a_bits_mask(binder_auto_in_0_a_bits_mask),
    .auto_in_0_a_bits_data(binder_auto_in_0_a_bits_data),
    .auto_in_0_d_ready(binder_auto_in_0_d_ready),
    .auto_in_0_d_valid(binder_auto_in_0_d_valid),
    .auto_in_0_d_bits_opcode(binder_auto_in_0_d_bits_opcode),
    .auto_in_0_d_bits_size(binder_auto_in_0_d_bits_size),
    .auto_in_0_d_bits_source(binder_auto_in_0_d_bits_source),
    .auto_in_0_d_bits_denied(binder_auto_in_0_d_bits_denied),
    .auto_in_0_d_bits_data(binder_auto_in_0_d_bits_data),
    .auto_in_0_d_bits_corrupt(binder_auto_in_0_d_bits_corrupt),
    .auto_out_3_a_ready(binder_auto_out_3_a_ready),
    .auto_out_3_a_valid(binder_auto_out_3_a_valid),
    .auto_out_3_a_bits_opcode(binder_auto_out_3_a_bits_opcode),
    .auto_out_3_a_bits_param(binder_auto_out_3_a_bits_param),
    .auto_out_3_a_bits_size(binder_auto_out_3_a_bits_size),
    .auto_out_3_a_bits_source(binder_auto_out_3_a_bits_source),
    .auto_out_3_a_bits_address(binder_auto_out_3_a_bits_address),
    .auto_out_3_a_bits_user_amba_prot_bufferable(binder_auto_out_3_a_bits_user_amba_prot_bufferable),
    .auto_out_3_a_bits_user_amba_prot_modifiable(binder_auto_out_3_a_bits_user_amba_prot_modifiable),
    .auto_out_3_a_bits_user_amba_prot_readalloc(binder_auto_out_3_a_bits_user_amba_prot_readalloc),
    .auto_out_3_a_bits_user_amba_prot_writealloc(binder_auto_out_3_a_bits_user_amba_prot_writealloc),
    .auto_out_3_a_bits_user_amba_prot_privileged(binder_auto_out_3_a_bits_user_amba_prot_privileged),
    .auto_out_3_a_bits_user_amba_prot_secure(binder_auto_out_3_a_bits_user_amba_prot_secure),
    .auto_out_3_a_bits_mask(binder_auto_out_3_a_bits_mask),
    .auto_out_3_a_bits_data(binder_auto_out_3_a_bits_data),
    .auto_out_3_d_ready(binder_auto_out_3_d_ready),
    .auto_out_3_d_valid(binder_auto_out_3_d_valid),
    .auto_out_3_d_bits_opcode(binder_auto_out_3_d_bits_opcode),
    .auto_out_3_d_bits_size(binder_auto_out_3_d_bits_size),
    .auto_out_3_d_bits_source(binder_auto_out_3_d_bits_source),
    .auto_out_3_d_bits_denied(binder_auto_out_3_d_bits_denied),
    .auto_out_3_d_bits_data(binder_auto_out_3_d_bits_data),
    .auto_out_3_d_bits_corrupt(binder_auto_out_3_d_bits_corrupt),
    .auto_out_2_a_ready(binder_auto_out_2_a_ready),
    .auto_out_2_a_valid(binder_auto_out_2_a_valid),
    .auto_out_2_a_bits_opcode(binder_auto_out_2_a_bits_opcode),
    .auto_out_2_a_bits_param(binder_auto_out_2_a_bits_param),
    .auto_out_2_a_bits_size(binder_auto_out_2_a_bits_size),
    .auto_out_2_a_bits_source(binder_auto_out_2_a_bits_source),
    .auto_out_2_a_bits_address(binder_auto_out_2_a_bits_address),
    .auto_out_2_a_bits_user_amba_prot_bufferable(binder_auto_out_2_a_bits_user_amba_prot_bufferable),
    .auto_out_2_a_bits_user_amba_prot_modifiable(binder_auto_out_2_a_bits_user_amba_prot_modifiable),
    .auto_out_2_a_bits_user_amba_prot_readalloc(binder_auto_out_2_a_bits_user_amba_prot_readalloc),
    .auto_out_2_a_bits_user_amba_prot_writealloc(binder_auto_out_2_a_bits_user_amba_prot_writealloc),
    .auto_out_2_a_bits_user_amba_prot_privileged(binder_auto_out_2_a_bits_user_amba_prot_privileged),
    .auto_out_2_a_bits_user_amba_prot_secure(binder_auto_out_2_a_bits_user_amba_prot_secure),
    .auto_out_2_a_bits_mask(binder_auto_out_2_a_bits_mask),
    .auto_out_2_a_bits_data(binder_auto_out_2_a_bits_data),
    .auto_out_2_d_ready(binder_auto_out_2_d_ready),
    .auto_out_2_d_valid(binder_auto_out_2_d_valid),
    .auto_out_2_d_bits_opcode(binder_auto_out_2_d_bits_opcode),
    .auto_out_2_d_bits_size(binder_auto_out_2_d_bits_size),
    .auto_out_2_d_bits_source(binder_auto_out_2_d_bits_source),
    .auto_out_2_d_bits_denied(binder_auto_out_2_d_bits_denied),
    .auto_out_2_d_bits_data(binder_auto_out_2_d_bits_data),
    .auto_out_2_d_bits_corrupt(binder_auto_out_2_d_bits_corrupt),
    .auto_out_1_a_ready(binder_auto_out_1_a_ready),
    .auto_out_1_a_valid(binder_auto_out_1_a_valid),
    .auto_out_1_a_bits_opcode(binder_auto_out_1_a_bits_opcode),
    .auto_out_1_a_bits_param(binder_auto_out_1_a_bits_param),
    .auto_out_1_a_bits_size(binder_auto_out_1_a_bits_size),
    .auto_out_1_a_bits_source(binder_auto_out_1_a_bits_source),
    .auto_out_1_a_bits_address(binder_auto_out_1_a_bits_address),
    .auto_out_1_a_bits_user_amba_prot_bufferable(binder_auto_out_1_a_bits_user_amba_prot_bufferable),
    .auto_out_1_a_bits_user_amba_prot_modifiable(binder_auto_out_1_a_bits_user_amba_prot_modifiable),
    .auto_out_1_a_bits_user_amba_prot_readalloc(binder_auto_out_1_a_bits_user_amba_prot_readalloc),
    .auto_out_1_a_bits_user_amba_prot_writealloc(binder_auto_out_1_a_bits_user_amba_prot_writealloc),
    .auto_out_1_a_bits_user_amba_prot_privileged(binder_auto_out_1_a_bits_user_amba_prot_privileged),
    .auto_out_1_a_bits_user_amba_prot_secure(binder_auto_out_1_a_bits_user_amba_prot_secure),
    .auto_out_1_a_bits_mask(binder_auto_out_1_a_bits_mask),
    .auto_out_1_a_bits_data(binder_auto_out_1_a_bits_data),
    .auto_out_1_d_ready(binder_auto_out_1_d_ready),
    .auto_out_1_d_valid(binder_auto_out_1_d_valid),
    .auto_out_1_d_bits_opcode(binder_auto_out_1_d_bits_opcode),
    .auto_out_1_d_bits_size(binder_auto_out_1_d_bits_size),
    .auto_out_1_d_bits_source(binder_auto_out_1_d_bits_source),
    .auto_out_1_d_bits_denied(binder_auto_out_1_d_bits_denied),
    .auto_out_1_d_bits_data(binder_auto_out_1_d_bits_data),
    .auto_out_1_d_bits_corrupt(binder_auto_out_1_d_bits_corrupt),
    .auto_out_0_a_ready(binder_auto_out_0_a_ready),
    .auto_out_0_a_valid(binder_auto_out_0_a_valid),
    .auto_out_0_a_bits_opcode(binder_auto_out_0_a_bits_opcode),
    .auto_out_0_a_bits_param(binder_auto_out_0_a_bits_param),
    .auto_out_0_a_bits_size(binder_auto_out_0_a_bits_size),
    .auto_out_0_a_bits_source(binder_auto_out_0_a_bits_source),
    .auto_out_0_a_bits_address(binder_auto_out_0_a_bits_address),
    .auto_out_0_a_bits_user_amba_prot_bufferable(binder_auto_out_0_a_bits_user_amba_prot_bufferable),
    .auto_out_0_a_bits_user_amba_prot_modifiable(binder_auto_out_0_a_bits_user_amba_prot_modifiable),
    .auto_out_0_a_bits_user_amba_prot_readalloc(binder_auto_out_0_a_bits_user_amba_prot_readalloc),
    .auto_out_0_a_bits_user_amba_prot_writealloc(binder_auto_out_0_a_bits_user_amba_prot_writealloc),
    .auto_out_0_a_bits_user_amba_prot_privileged(binder_auto_out_0_a_bits_user_amba_prot_privileged),
    .auto_out_0_a_bits_user_amba_prot_secure(binder_auto_out_0_a_bits_user_amba_prot_secure),
    .auto_out_0_a_bits_mask(binder_auto_out_0_a_bits_mask),
    .auto_out_0_a_bits_data(binder_auto_out_0_a_bits_data),
    .auto_out_0_d_ready(binder_auto_out_0_d_ready),
    .auto_out_0_d_valid(binder_auto_out_0_d_valid),
    .auto_out_0_d_bits_opcode(binder_auto_out_0_d_bits_opcode),
    .auto_out_0_d_bits_size(binder_auto_out_0_d_bits_size),
    .auto_out_0_d_bits_source(binder_auto_out_0_d_bits_source),
    .auto_out_0_d_bits_denied(binder_auto_out_0_d_bits_denied),
    .auto_out_0_d_bits_data(binder_auto_out_0_d_bits_data),
    .auto_out_0_d_bits_corrupt(binder_auto_out_0_d_bits_corrupt)
  );
  assign subsystem_l2_clock_groups_auto_out_1_member_subsystem_mbus_address_adjuster_1_clock =
    subsystem_l2_clock_groups_auto_in_member_subsystem_l2_2_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_l2_clock_groups_auto_out_1_member_subsystem_mbus_address_adjuster_1_reset =
    subsystem_l2_clock_groups_auto_in_member_subsystem_l2_2_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_l2_clock_groups_auto_out_0_member_subsystem_l2_0_clock =
    subsystem_l2_clock_groups_auto_in_member_subsystem_l2_0_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_l2_clock_groups_auto_out_0_member_subsystem_l2_0_reset =
    subsystem_l2_clock_groups_auto_in_member_subsystem_l2_0_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign clockGroup_auto_out_clock = clockGroup_auto_in_member_subsystem_l2_0_clock; // @[ClockGroup.scala 8:11 LazyModule.scala 388:16]
  assign clockGroup_auto_out_reset = clockGroup_auto_in_member_subsystem_l2_0_reset; // @[ClockGroup.scala 8:11 LazyModule.scala 388:16]
  assign fixedClockNode_auto_out_clock = fixedClockNode_auto_in_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign fixedClockNode_auto_out_reset = fixedClockNode_auto_in_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign l2_rf_reset = rf_reset;
  assign filter_auto_in_3_a_ready = filter_auto_out_3_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_b_valid = filter_auto_out_3_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_b_bits_opcode = filter_auto_out_3_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_b_bits_param = filter_auto_out_3_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_b_bits_size = filter_auto_out_3_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_b_bits_source = filter_auto_out_3_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_b_bits_address = filter_auto_out_3_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_b_bits_mask = filter_auto_out_3_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_b_bits_data = filter_auto_out_3_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_b_bits_corrupt = filter_auto_out_3_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_c_ready = filter_auto_out_3_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_d_valid = filter_auto_out_3_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_d_bits_opcode = filter_auto_out_3_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_d_bits_param = filter_auto_out_3_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_d_bits_size = filter_auto_out_3_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_d_bits_source = filter_auto_out_3_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_d_bits_sink = filter_auto_out_3_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_d_bits_denied = filter_auto_out_3_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_d_bits_data = filter_auto_out_3_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_d_bits_corrupt = filter_auto_out_3_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_3_e_ready = filter_auto_out_3_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_a_ready = filter_auto_out_2_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_b_valid = filter_auto_out_2_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_b_bits_opcode = filter_auto_out_2_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_b_bits_param = filter_auto_out_2_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_b_bits_size = filter_auto_out_2_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_b_bits_source = filter_auto_out_2_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_b_bits_address = filter_auto_out_2_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_b_bits_mask = filter_auto_out_2_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_b_bits_data = filter_auto_out_2_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_b_bits_corrupt = filter_auto_out_2_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_c_ready = filter_auto_out_2_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_d_valid = filter_auto_out_2_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_d_bits_opcode = filter_auto_out_2_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_d_bits_param = filter_auto_out_2_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_d_bits_size = filter_auto_out_2_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_d_bits_source = filter_auto_out_2_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_d_bits_sink = filter_auto_out_2_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_d_bits_denied = filter_auto_out_2_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_d_bits_data = filter_auto_out_2_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_d_bits_corrupt = filter_auto_out_2_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_2_e_ready = filter_auto_out_2_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_a_ready = filter_auto_out_1_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_b_valid = filter_auto_out_1_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_b_bits_opcode = filter_auto_out_1_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_b_bits_param = filter_auto_out_1_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_b_bits_size = filter_auto_out_1_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_b_bits_source = filter_auto_out_1_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_b_bits_address = filter_auto_out_1_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_b_bits_mask = filter_auto_out_1_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_b_bits_data = filter_auto_out_1_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_b_bits_corrupt = filter_auto_out_1_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_c_ready = filter_auto_out_1_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_d_valid = filter_auto_out_1_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_d_bits_opcode = filter_auto_out_1_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_d_bits_param = filter_auto_out_1_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_d_bits_size = filter_auto_out_1_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_d_bits_source = filter_auto_out_1_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_d_bits_sink = filter_auto_out_1_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_d_bits_denied = filter_auto_out_1_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_d_bits_data = filter_auto_out_1_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_d_bits_corrupt = filter_auto_out_1_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_1_e_ready = filter_auto_out_1_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_a_ready = filter_auto_out_0_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_b_valid = filter_auto_out_0_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_b_bits_opcode = filter_auto_out_0_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_b_bits_param = filter_auto_out_0_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_b_bits_size = filter_auto_out_0_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_b_bits_source = filter_auto_out_0_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_b_bits_address = filter_auto_out_0_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_b_bits_mask = filter_auto_out_0_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_b_bits_data = filter_auto_out_0_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_b_bits_corrupt = filter_auto_out_0_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_c_ready = filter_auto_out_0_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_d_valid = filter_auto_out_0_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_d_bits_opcode = filter_auto_out_0_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_d_bits_param = filter_auto_out_0_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_d_bits_size = filter_auto_out_0_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_d_bits_source = filter_auto_out_0_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_d_bits_sink = filter_auto_out_0_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_d_bits_denied = filter_auto_out_0_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_d_bits_data = filter_auto_out_0_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_d_bits_corrupt = filter_auto_out_0_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_in_0_e_ready = filter_auto_out_0_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign filter_auto_out_3_a_valid = filter_auto_in_3_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_a_bits_opcode = filter_auto_in_3_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_a_bits_param = filter_auto_in_3_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_a_bits_size = filter_auto_in_3_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_a_bits_source = filter_auto_in_3_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_a_bits_address = filter_auto_in_3_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_a_bits_mask = filter_auto_in_3_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_a_bits_data = filter_auto_in_3_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_a_bits_corrupt = filter_auto_in_3_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_b_ready = filter_auto_in_3_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_c_valid = filter_auto_in_3_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_c_bits_opcode = filter_auto_in_3_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_c_bits_param = filter_auto_in_3_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_c_bits_size = filter_auto_in_3_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_c_bits_source = filter_auto_in_3_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_c_bits_address = filter_auto_in_3_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_c_bits_data = filter_auto_in_3_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_c_bits_corrupt = filter_auto_in_3_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_d_ready = filter_auto_in_3_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_e_valid = filter_auto_in_3_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_3_e_bits_sink = filter_auto_in_3_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_a_valid = filter_auto_in_2_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_a_bits_opcode = filter_auto_in_2_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_a_bits_param = filter_auto_in_2_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_a_bits_size = filter_auto_in_2_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_a_bits_source = filter_auto_in_2_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_a_bits_address = filter_auto_in_2_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_a_bits_mask = filter_auto_in_2_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_a_bits_data = filter_auto_in_2_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_a_bits_corrupt = filter_auto_in_2_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_b_ready = filter_auto_in_2_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_c_valid = filter_auto_in_2_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_c_bits_opcode = filter_auto_in_2_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_c_bits_param = filter_auto_in_2_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_c_bits_size = filter_auto_in_2_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_c_bits_source = filter_auto_in_2_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_c_bits_address = filter_auto_in_2_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_c_bits_data = filter_auto_in_2_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_c_bits_corrupt = filter_auto_in_2_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_d_ready = filter_auto_in_2_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_e_valid = filter_auto_in_2_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_2_e_bits_sink = filter_auto_in_2_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_a_valid = filter_auto_in_1_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_a_bits_opcode = filter_auto_in_1_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_a_bits_param = filter_auto_in_1_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_a_bits_size = filter_auto_in_1_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_a_bits_source = filter_auto_in_1_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_a_bits_address = filter_auto_in_1_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_a_bits_mask = filter_auto_in_1_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_a_bits_data = filter_auto_in_1_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_a_bits_corrupt = filter_auto_in_1_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_b_ready = filter_auto_in_1_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_c_valid = filter_auto_in_1_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_c_bits_opcode = filter_auto_in_1_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_c_bits_param = filter_auto_in_1_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_c_bits_size = filter_auto_in_1_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_c_bits_source = filter_auto_in_1_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_c_bits_address = filter_auto_in_1_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_c_bits_data = filter_auto_in_1_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_c_bits_corrupt = filter_auto_in_1_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_d_ready = filter_auto_in_1_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_e_valid = filter_auto_in_1_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_1_e_bits_sink = filter_auto_in_1_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_a_valid = filter_auto_in_0_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_a_bits_opcode = filter_auto_in_0_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_a_bits_param = filter_auto_in_0_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_a_bits_size = filter_auto_in_0_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_a_bits_source = filter_auto_in_0_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_a_bits_address = filter_auto_in_0_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_a_bits_mask = filter_auto_in_0_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_a_bits_data = filter_auto_in_0_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_a_bits_corrupt = filter_auto_in_0_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_b_ready = filter_auto_in_0_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_c_valid = filter_auto_in_0_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_c_bits_opcode = filter_auto_in_0_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_c_bits_param = filter_auto_in_0_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_c_bits_size = filter_auto_in_0_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_c_bits_source = filter_auto_in_0_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_c_bits_address = filter_auto_in_0_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_c_bits_data = filter_auto_in_0_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_c_bits_corrupt = filter_auto_in_0_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_d_ready = filter_auto_in_0_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_e_valid = filter_auto_in_0_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign filter_auto_out_0_e_bits_sink = filter_auto_in_0_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_inner_TLBuffer_rf_reset = rf_reset;
  assign ComposableCache_outer_TLBuffer_auto_in_3_a_ready = ComposableCache_outer_TLBuffer_auto_out_3_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_3_c_ready = ComposableCache_outer_TLBuffer_auto_out_3_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_3_d_valid = ComposableCache_outer_TLBuffer_auto_out_3_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_3_d_bits_opcode =
    ComposableCache_outer_TLBuffer_auto_out_3_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_3_d_bits_param = ComposableCache_outer_TLBuffer_auto_out_3_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_3_d_bits_size = ComposableCache_outer_TLBuffer_auto_out_3_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_3_d_bits_source =
    ComposableCache_outer_TLBuffer_auto_out_3_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_3_d_bits_sink = ComposableCache_outer_TLBuffer_auto_out_3_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_3_d_bits_denied =
    ComposableCache_outer_TLBuffer_auto_out_3_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_3_d_bits_data = ComposableCache_outer_TLBuffer_auto_out_3_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_3_d_bits_corrupt =
    ComposableCache_outer_TLBuffer_auto_out_3_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_2_a_ready = ComposableCache_outer_TLBuffer_auto_out_2_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_2_c_ready = ComposableCache_outer_TLBuffer_auto_out_2_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_2_d_valid = ComposableCache_outer_TLBuffer_auto_out_2_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_2_d_bits_opcode =
    ComposableCache_outer_TLBuffer_auto_out_2_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_2_d_bits_param = ComposableCache_outer_TLBuffer_auto_out_2_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_2_d_bits_size = ComposableCache_outer_TLBuffer_auto_out_2_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_2_d_bits_source =
    ComposableCache_outer_TLBuffer_auto_out_2_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_2_d_bits_sink = ComposableCache_outer_TLBuffer_auto_out_2_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_2_d_bits_denied =
    ComposableCache_outer_TLBuffer_auto_out_2_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_2_d_bits_data = ComposableCache_outer_TLBuffer_auto_out_2_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_2_d_bits_corrupt =
    ComposableCache_outer_TLBuffer_auto_out_2_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_1_a_ready = ComposableCache_outer_TLBuffer_auto_out_1_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_1_c_ready = ComposableCache_outer_TLBuffer_auto_out_1_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_1_d_valid = ComposableCache_outer_TLBuffer_auto_out_1_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_1_d_bits_opcode =
    ComposableCache_outer_TLBuffer_auto_out_1_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_1_d_bits_param = ComposableCache_outer_TLBuffer_auto_out_1_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_1_d_bits_size = ComposableCache_outer_TLBuffer_auto_out_1_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_1_d_bits_source =
    ComposableCache_outer_TLBuffer_auto_out_1_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_1_d_bits_sink = ComposableCache_outer_TLBuffer_auto_out_1_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_1_d_bits_denied =
    ComposableCache_outer_TLBuffer_auto_out_1_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_1_d_bits_data = ComposableCache_outer_TLBuffer_auto_out_1_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_1_d_bits_corrupt =
    ComposableCache_outer_TLBuffer_auto_out_1_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_0_a_ready = ComposableCache_outer_TLBuffer_auto_out_0_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_0_c_ready = ComposableCache_outer_TLBuffer_auto_out_0_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_0_d_valid = ComposableCache_outer_TLBuffer_auto_out_0_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_0_d_bits_opcode =
    ComposableCache_outer_TLBuffer_auto_out_0_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_0_d_bits_param = ComposableCache_outer_TLBuffer_auto_out_0_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_0_d_bits_size = ComposableCache_outer_TLBuffer_auto_out_0_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_0_d_bits_source =
    ComposableCache_outer_TLBuffer_auto_out_0_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_0_d_bits_sink = ComposableCache_outer_TLBuffer_auto_out_0_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_0_d_bits_denied =
    ComposableCache_outer_TLBuffer_auto_out_0_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_0_d_bits_data = ComposableCache_outer_TLBuffer_auto_out_0_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_in_0_d_bits_corrupt =
    ComposableCache_outer_TLBuffer_auto_out_0_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign ComposableCache_outer_TLBuffer_auto_out_3_a_valid = ComposableCache_outer_TLBuffer_auto_in_3_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_a_bits_opcode =
    ComposableCache_outer_TLBuffer_auto_in_3_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_a_bits_param = ComposableCache_outer_TLBuffer_auto_in_3_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_a_bits_source =
    ComposableCache_outer_TLBuffer_auto_in_3_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_a_bits_address =
    ComposableCache_outer_TLBuffer_auto_in_3_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_c_valid = ComposableCache_outer_TLBuffer_auto_in_3_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_c_bits_opcode =
    ComposableCache_outer_TLBuffer_auto_in_3_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_c_bits_param = ComposableCache_outer_TLBuffer_auto_in_3_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_c_bits_source =
    ComposableCache_outer_TLBuffer_auto_in_3_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_c_bits_address =
    ComposableCache_outer_TLBuffer_auto_in_3_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_c_bits_data = ComposableCache_outer_TLBuffer_auto_in_3_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_d_ready = ComposableCache_outer_TLBuffer_auto_in_3_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_e_valid = ComposableCache_outer_TLBuffer_auto_in_3_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_e_bits_sink = ComposableCache_outer_TLBuffer_auto_in_3_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_a_valid = ComposableCache_outer_TLBuffer_auto_in_2_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_a_bits_opcode =
    ComposableCache_outer_TLBuffer_auto_in_2_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_a_bits_param = ComposableCache_outer_TLBuffer_auto_in_2_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_a_bits_source =
    ComposableCache_outer_TLBuffer_auto_in_2_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_a_bits_address =
    ComposableCache_outer_TLBuffer_auto_in_2_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_c_valid = ComposableCache_outer_TLBuffer_auto_in_2_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_c_bits_opcode =
    ComposableCache_outer_TLBuffer_auto_in_2_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_c_bits_param = ComposableCache_outer_TLBuffer_auto_in_2_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_c_bits_source =
    ComposableCache_outer_TLBuffer_auto_in_2_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_c_bits_address =
    ComposableCache_outer_TLBuffer_auto_in_2_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_c_bits_data = ComposableCache_outer_TLBuffer_auto_in_2_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_d_ready = ComposableCache_outer_TLBuffer_auto_in_2_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_e_valid = ComposableCache_outer_TLBuffer_auto_in_2_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_e_bits_sink = ComposableCache_outer_TLBuffer_auto_in_2_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_a_valid = ComposableCache_outer_TLBuffer_auto_in_1_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_a_bits_opcode =
    ComposableCache_outer_TLBuffer_auto_in_1_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_a_bits_param = ComposableCache_outer_TLBuffer_auto_in_1_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_a_bits_source =
    ComposableCache_outer_TLBuffer_auto_in_1_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_a_bits_address =
    ComposableCache_outer_TLBuffer_auto_in_1_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_c_valid = ComposableCache_outer_TLBuffer_auto_in_1_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_c_bits_opcode =
    ComposableCache_outer_TLBuffer_auto_in_1_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_c_bits_param = ComposableCache_outer_TLBuffer_auto_in_1_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_c_bits_source =
    ComposableCache_outer_TLBuffer_auto_in_1_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_c_bits_address =
    ComposableCache_outer_TLBuffer_auto_in_1_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_c_bits_data = ComposableCache_outer_TLBuffer_auto_in_1_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_d_ready = ComposableCache_outer_TLBuffer_auto_in_1_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_e_valid = ComposableCache_outer_TLBuffer_auto_in_1_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_e_bits_sink = ComposableCache_outer_TLBuffer_auto_in_1_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_a_valid = ComposableCache_outer_TLBuffer_auto_in_0_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_a_bits_opcode =
    ComposableCache_outer_TLBuffer_auto_in_0_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_a_bits_param = ComposableCache_outer_TLBuffer_auto_in_0_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_a_bits_source =
    ComposableCache_outer_TLBuffer_auto_in_0_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_a_bits_address =
    ComposableCache_outer_TLBuffer_auto_in_0_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_c_valid = ComposableCache_outer_TLBuffer_auto_in_0_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_c_bits_opcode =
    ComposableCache_outer_TLBuffer_auto_in_0_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_c_bits_param = ComposableCache_outer_TLBuffer_auto_in_0_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_c_bits_source =
    ComposableCache_outer_TLBuffer_auto_in_0_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_c_bits_address =
    ComposableCache_outer_TLBuffer_auto_in_0_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_c_bits_data = ComposableCache_outer_TLBuffer_auto_in_0_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_d_ready = ComposableCache_outer_TLBuffer_auto_in_0_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_e_valid = ComposableCache_outer_TLBuffer_auto_in_0_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_e_bits_sink = ComposableCache_outer_TLBuffer_auto_in_0_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign cork_rf_reset = rf_reset;
  assign coherent_jbar_auto_in_3_a_ready = coherent_jbar_auto_out_3_a_ready; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_3_b_valid = coherent_jbar_auto_out_3_b_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_in_3_b_bits_opcode = coherent_jbar_auto_out_3_b_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_3_b_bits_param = coherent_jbar_auto_out_3_b_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_3_b_bits_size = coherent_jbar_auto_out_3_b_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_3_b_bits_source = coherent_jbar_auto_out_3_b_bits_source; // @[Xbar.scala 228:69]
  assign coherent_jbar_auto_in_3_b_bits_address = coherent_jbar_auto_out_3_b_bits_address; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_3_b_bits_mask = coherent_jbar_auto_out_3_b_bits_mask; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_3_b_bits_data = coherent_jbar_auto_out_3_b_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_3_b_bits_corrupt = coherent_jbar_auto_out_3_b_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_3_c_ready = coherent_jbar_auto_out_3_c_ready; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_3_d_valid = coherent_jbar_auto_out_3_d_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_in_3_d_bits_opcode = coherent_jbar_auto_out_3_d_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_3_d_bits_param = coherent_jbar_auto_out_3_d_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_3_d_bits_size = coherent_jbar_auto_out_3_d_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_3_d_bits_source = coherent_jbar_auto_out_3_d_bits_source; // @[Xbar.scala 228:69]
  assign coherent_jbar_auto_in_3_d_bits_sink = coherent_jbar_auto_out_3_d_bits_sink; // @[Xbar.scala 323:53]
  assign coherent_jbar_auto_in_3_d_bits_denied = coherent_jbar_auto_out_3_d_bits_denied; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_3_d_bits_data = coherent_jbar_auto_out_3_d_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_3_d_bits_corrupt = coherent_jbar_auto_out_3_d_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_3_e_ready = coherent_jbar_auto_out_3_e_ready; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_a_ready = coherent_jbar_auto_out_2_a_ready; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_b_valid = coherent_jbar_auto_out_2_b_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_in_2_b_bits_opcode = coherent_jbar_auto_out_2_b_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_b_bits_param = coherent_jbar_auto_out_2_b_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_b_bits_size = coherent_jbar_auto_out_2_b_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_b_bits_source = coherent_jbar_auto_out_2_b_bits_source; // @[Xbar.scala 228:69]
  assign coherent_jbar_auto_in_2_b_bits_address = coherent_jbar_auto_out_2_b_bits_address; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_b_bits_mask = coherent_jbar_auto_out_2_b_bits_mask; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_b_bits_data = coherent_jbar_auto_out_2_b_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_b_bits_corrupt = coherent_jbar_auto_out_2_b_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_c_ready = coherent_jbar_auto_out_2_c_ready; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_d_valid = coherent_jbar_auto_out_2_d_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_in_2_d_bits_opcode = coherent_jbar_auto_out_2_d_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_d_bits_param = coherent_jbar_auto_out_2_d_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_d_bits_size = coherent_jbar_auto_out_2_d_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_d_bits_source = coherent_jbar_auto_out_2_d_bits_source; // @[Xbar.scala 228:69]
  assign coherent_jbar_auto_in_2_d_bits_sink = coherent_jbar_auto_out_2_d_bits_sink; // @[Xbar.scala 323:53]
  assign coherent_jbar_auto_in_2_d_bits_denied = coherent_jbar_auto_out_2_d_bits_denied; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_d_bits_data = coherent_jbar_auto_out_2_d_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_d_bits_corrupt = coherent_jbar_auto_out_2_d_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_2_e_ready = coherent_jbar_auto_out_2_e_ready; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_a_ready = coherent_jbar_auto_out_1_a_ready; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_b_valid = coherent_jbar_auto_out_1_b_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_in_1_b_bits_opcode = coherent_jbar_auto_out_1_b_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_b_bits_param = coherent_jbar_auto_out_1_b_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_b_bits_size = coherent_jbar_auto_out_1_b_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_b_bits_source = coherent_jbar_auto_out_1_b_bits_source; // @[Xbar.scala 228:69]
  assign coherent_jbar_auto_in_1_b_bits_address = coherent_jbar_auto_out_1_b_bits_address; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_b_bits_mask = coherent_jbar_auto_out_1_b_bits_mask; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_b_bits_data = coherent_jbar_auto_out_1_b_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_b_bits_corrupt = coherent_jbar_auto_out_1_b_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_c_ready = coherent_jbar_auto_out_1_c_ready; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_d_valid = coherent_jbar_auto_out_1_d_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_in_1_d_bits_opcode = coherent_jbar_auto_out_1_d_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_d_bits_param = coherent_jbar_auto_out_1_d_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_d_bits_size = coherent_jbar_auto_out_1_d_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_d_bits_source = coherent_jbar_auto_out_1_d_bits_source; // @[Xbar.scala 228:69]
  assign coherent_jbar_auto_in_1_d_bits_sink = coherent_jbar_auto_out_1_d_bits_sink; // @[Xbar.scala 323:53]
  assign coherent_jbar_auto_in_1_d_bits_denied = coherent_jbar_auto_out_1_d_bits_denied; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_d_bits_data = coherent_jbar_auto_out_1_d_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_d_bits_corrupt = coherent_jbar_auto_out_1_d_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_1_e_ready = coherent_jbar_auto_out_1_e_ready; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_a_ready = coherent_jbar_auto_out_0_a_ready; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_b_valid = coherent_jbar_auto_out_0_b_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_in_0_b_bits_opcode = coherent_jbar_auto_out_0_b_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_b_bits_param = coherent_jbar_auto_out_0_b_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_b_bits_size = coherent_jbar_auto_out_0_b_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_b_bits_source = coherent_jbar_auto_out_0_b_bits_source; // @[Xbar.scala 228:69]
  assign coherent_jbar_auto_in_0_b_bits_address = coherent_jbar_auto_out_0_b_bits_address; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_b_bits_mask = coherent_jbar_auto_out_0_b_bits_mask; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_b_bits_data = coherent_jbar_auto_out_0_b_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_b_bits_corrupt = coherent_jbar_auto_out_0_b_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_c_ready = coherent_jbar_auto_out_0_c_ready; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_d_valid = coherent_jbar_auto_out_0_d_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_in_0_d_bits_opcode = coherent_jbar_auto_out_0_d_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_d_bits_param = coherent_jbar_auto_out_0_d_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_d_bits_size = coherent_jbar_auto_out_0_d_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_d_bits_source = coherent_jbar_auto_out_0_d_bits_source; // @[Xbar.scala 228:69]
  assign coherent_jbar_auto_in_0_d_bits_sink = coherent_jbar_auto_out_0_d_bits_sink; // @[Xbar.scala 323:53]
  assign coherent_jbar_auto_in_0_d_bits_denied = coherent_jbar_auto_out_0_d_bits_denied; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_d_bits_data = coherent_jbar_auto_out_0_d_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_d_bits_corrupt = coherent_jbar_auto_out_0_d_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_in_0_e_ready = coherent_jbar_auto_out_0_e_ready; // @[Nodes.scala 1447:13 LazyModule.scala 390:12]
  assign coherent_jbar_auto_out_3_a_valid = coherent_jbar_auto_in_3_a_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_out_3_a_bits_opcode = coherent_jbar_auto_in_3_a_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_3_a_bits_param = coherent_jbar_auto_in_3_a_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_3_a_bits_size = coherent_jbar_auto_in_3_a_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_3_a_bits_source = coherent_jbar_auto_in_3_a_bits_source; // @[Xbar.scala 237:55]
  assign coherent_jbar_auto_out_3_a_bits_address = coherent_jbar_auto_in_3_a_bits_address; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_3_a_bits_mask = coherent_jbar_auto_in_3_a_bits_mask; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_3_a_bits_data = coherent_jbar_auto_in_3_a_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_3_a_bits_corrupt = coherent_jbar_auto_in_3_a_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_3_b_ready = coherent_jbar_auto_in_3_b_ready; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_3_c_valid = coherent_jbar_auto_in_3_c_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_out_3_c_bits_opcode = coherent_jbar_auto_in_3_c_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_3_c_bits_param = coherent_jbar_auto_in_3_c_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_3_c_bits_size = coherent_jbar_auto_in_3_c_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_3_c_bits_source = coherent_jbar_auto_in_3_c_bits_source; // @[Xbar.scala 259:55]
  assign coherent_jbar_auto_out_3_c_bits_address = coherent_jbar_auto_in_3_c_bits_address; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_3_c_bits_data = coherent_jbar_auto_in_3_c_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_3_c_bits_corrupt = coherent_jbar_auto_in_3_c_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_3_d_ready = coherent_jbar_auto_in_3_d_ready; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_3_e_valid = coherent_jbar_auto_in_3_e_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_out_3_e_bits_sink = coherent_jbar_auto_in_3_e_bits_sink; // @[Xbar.scala 228:69]
  assign coherent_jbar_auto_out_2_a_valid = coherent_jbar_auto_in_2_a_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_out_2_a_bits_opcode = coherent_jbar_auto_in_2_a_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_2_a_bits_param = coherent_jbar_auto_in_2_a_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_2_a_bits_size = coherent_jbar_auto_in_2_a_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_2_a_bits_source = coherent_jbar_auto_in_2_a_bits_source; // @[Xbar.scala 237:55]
  assign coherent_jbar_auto_out_2_a_bits_address = coherent_jbar_auto_in_2_a_bits_address; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_2_a_bits_mask = coherent_jbar_auto_in_2_a_bits_mask; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_2_a_bits_data = coherent_jbar_auto_in_2_a_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_2_a_bits_corrupt = coherent_jbar_auto_in_2_a_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_2_b_ready = coherent_jbar_auto_in_2_b_ready; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_2_c_valid = coherent_jbar_auto_in_2_c_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_out_2_c_bits_opcode = coherent_jbar_auto_in_2_c_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_2_c_bits_param = coherent_jbar_auto_in_2_c_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_2_c_bits_size = coherent_jbar_auto_in_2_c_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_2_c_bits_source = coherent_jbar_auto_in_2_c_bits_source; // @[Xbar.scala 259:55]
  assign coherent_jbar_auto_out_2_c_bits_address = coherent_jbar_auto_in_2_c_bits_address; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_2_c_bits_data = coherent_jbar_auto_in_2_c_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_2_c_bits_corrupt = coherent_jbar_auto_in_2_c_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_2_d_ready = coherent_jbar_auto_in_2_d_ready; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_2_e_valid = coherent_jbar_auto_in_2_e_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_out_2_e_bits_sink = coherent_jbar_auto_in_2_e_bits_sink; // @[Xbar.scala 228:69]
  assign coherent_jbar_auto_out_1_a_valid = coherent_jbar_auto_in_1_a_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_out_1_a_bits_opcode = coherent_jbar_auto_in_1_a_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_1_a_bits_param = coherent_jbar_auto_in_1_a_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_1_a_bits_size = coherent_jbar_auto_in_1_a_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_1_a_bits_source = coherent_jbar_auto_in_1_a_bits_source; // @[Xbar.scala 237:55]
  assign coherent_jbar_auto_out_1_a_bits_address = coherent_jbar_auto_in_1_a_bits_address; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_1_a_bits_mask = coherent_jbar_auto_in_1_a_bits_mask; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_1_a_bits_data = coherent_jbar_auto_in_1_a_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_1_a_bits_corrupt = coherent_jbar_auto_in_1_a_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_1_b_ready = coherent_jbar_auto_in_1_b_ready; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_1_c_valid = coherent_jbar_auto_in_1_c_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_out_1_c_bits_opcode = coherent_jbar_auto_in_1_c_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_1_c_bits_param = coherent_jbar_auto_in_1_c_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_1_c_bits_size = coherent_jbar_auto_in_1_c_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_1_c_bits_source = coherent_jbar_auto_in_1_c_bits_source; // @[Xbar.scala 259:55]
  assign coherent_jbar_auto_out_1_c_bits_address = coherent_jbar_auto_in_1_c_bits_address; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_1_c_bits_data = coherent_jbar_auto_in_1_c_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_1_c_bits_corrupt = coherent_jbar_auto_in_1_c_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_1_d_ready = coherent_jbar_auto_in_1_d_ready; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_1_e_valid = coherent_jbar_auto_in_1_e_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_out_1_e_bits_sink = coherent_jbar_auto_in_1_e_bits_sink; // @[Xbar.scala 228:69]
  assign coherent_jbar_auto_out_0_a_valid = coherent_jbar_auto_in_0_a_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_out_0_a_bits_opcode = coherent_jbar_auto_in_0_a_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_0_a_bits_param = coherent_jbar_auto_in_0_a_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_0_a_bits_size = coherent_jbar_auto_in_0_a_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_0_a_bits_source = coherent_jbar_auto_in_0_a_bits_source; // @[Xbar.scala 237:55]
  assign coherent_jbar_auto_out_0_a_bits_address = coherent_jbar_auto_in_0_a_bits_address; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_0_a_bits_mask = coherent_jbar_auto_in_0_a_bits_mask; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_0_a_bits_data = coherent_jbar_auto_in_0_a_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_0_a_bits_corrupt = coherent_jbar_auto_in_0_a_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_0_b_ready = coherent_jbar_auto_in_0_b_ready; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_0_c_valid = coherent_jbar_auto_in_0_c_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_out_0_c_bits_opcode = coherent_jbar_auto_in_0_c_bits_opcode; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_0_c_bits_param = coherent_jbar_auto_in_0_c_bits_param; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_0_c_bits_size = coherent_jbar_auto_in_0_c_bits_size; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_0_c_bits_source = coherent_jbar_auto_in_0_c_bits_source; // @[Xbar.scala 259:55]
  assign coherent_jbar_auto_out_0_c_bits_address = coherent_jbar_auto_in_0_c_bits_address; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_0_c_bits_data = coherent_jbar_auto_in_0_c_bits_data; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_0_c_bits_corrupt = coherent_jbar_auto_in_0_c_bits_corrupt; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_0_d_ready = coherent_jbar_auto_in_0_d_ready; // @[Nodes.scala 1447:13 LazyModule.scala 388:16]
  assign coherent_jbar_auto_out_0_e_valid = coherent_jbar_auto_in_0_e_valid; // @[ReadyValidCancel.scala 21:38]
  assign coherent_jbar_auto_out_0_e_bits_sink = coherent_jbar_auto_in_0_e_bits_sink; // @[Xbar.scala 228:69]
  assign binder_rf_reset = rf_reset;
  assign buffer_auto_in_3_a_ready = buffer_auto_out_3_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_b_valid = buffer_auto_out_3_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_b_bits_opcode = buffer_auto_out_3_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_b_bits_param = buffer_auto_out_3_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_b_bits_size = buffer_auto_out_3_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_b_bits_source = buffer_auto_out_3_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_b_bits_address = buffer_auto_out_3_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_b_bits_mask = buffer_auto_out_3_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_b_bits_data = buffer_auto_out_3_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_b_bits_corrupt = buffer_auto_out_3_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_c_ready = buffer_auto_out_3_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_valid = buffer_auto_out_3_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_bits_opcode = buffer_auto_out_3_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_bits_param = buffer_auto_out_3_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_bits_size = buffer_auto_out_3_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_bits_source = buffer_auto_out_3_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_bits_sink = buffer_auto_out_3_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_bits_denied = buffer_auto_out_3_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_bits_data = buffer_auto_out_3_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_bits_corrupt = buffer_auto_out_3_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_e_ready = buffer_auto_out_3_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_a_ready = buffer_auto_out_2_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_b_valid = buffer_auto_out_2_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_b_bits_opcode = buffer_auto_out_2_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_b_bits_param = buffer_auto_out_2_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_b_bits_size = buffer_auto_out_2_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_b_bits_source = buffer_auto_out_2_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_b_bits_address = buffer_auto_out_2_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_b_bits_mask = buffer_auto_out_2_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_b_bits_data = buffer_auto_out_2_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_b_bits_corrupt = buffer_auto_out_2_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_c_ready = buffer_auto_out_2_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_valid = buffer_auto_out_2_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_bits_opcode = buffer_auto_out_2_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_bits_param = buffer_auto_out_2_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_bits_size = buffer_auto_out_2_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_bits_source = buffer_auto_out_2_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_bits_sink = buffer_auto_out_2_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_bits_denied = buffer_auto_out_2_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_bits_data = buffer_auto_out_2_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_bits_corrupt = buffer_auto_out_2_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_e_ready = buffer_auto_out_2_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_a_ready = buffer_auto_out_1_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_b_valid = buffer_auto_out_1_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_b_bits_opcode = buffer_auto_out_1_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_b_bits_param = buffer_auto_out_1_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_b_bits_size = buffer_auto_out_1_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_b_bits_source = buffer_auto_out_1_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_b_bits_address = buffer_auto_out_1_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_b_bits_mask = buffer_auto_out_1_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_b_bits_data = buffer_auto_out_1_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_b_bits_corrupt = buffer_auto_out_1_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_c_ready = buffer_auto_out_1_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_valid = buffer_auto_out_1_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_bits_opcode = buffer_auto_out_1_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_bits_param = buffer_auto_out_1_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_bits_size = buffer_auto_out_1_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_bits_source = buffer_auto_out_1_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_bits_sink = buffer_auto_out_1_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_bits_denied = buffer_auto_out_1_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_bits_data = buffer_auto_out_1_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_bits_corrupt = buffer_auto_out_1_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_e_ready = buffer_auto_out_1_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_a_ready = buffer_auto_out_0_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_b_valid = buffer_auto_out_0_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_b_bits_opcode = buffer_auto_out_0_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_b_bits_param = buffer_auto_out_0_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_b_bits_size = buffer_auto_out_0_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_b_bits_source = buffer_auto_out_0_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_b_bits_address = buffer_auto_out_0_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_b_bits_mask = buffer_auto_out_0_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_b_bits_data = buffer_auto_out_0_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_b_bits_corrupt = buffer_auto_out_0_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_c_ready = buffer_auto_out_0_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_valid = buffer_auto_out_0_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_bits_opcode = buffer_auto_out_0_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_bits_param = buffer_auto_out_0_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_bits_size = buffer_auto_out_0_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_bits_source = buffer_auto_out_0_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_bits_sink = buffer_auto_out_0_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_bits_denied = buffer_auto_out_0_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_bits_data = buffer_auto_out_0_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_bits_corrupt = buffer_auto_out_0_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_e_ready = buffer_auto_out_0_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_3_a_valid = buffer_auto_in_3_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_opcode = buffer_auto_in_3_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_param = buffer_auto_in_3_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_size = buffer_auto_in_3_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_source = buffer_auto_in_3_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_address = buffer_auto_in_3_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_mask = buffer_auto_in_3_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_data = buffer_auto_in_3_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_corrupt = buffer_auto_in_3_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_b_ready = buffer_auto_in_3_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_c_valid = buffer_auto_in_3_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_c_bits_opcode = buffer_auto_in_3_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_c_bits_param = buffer_auto_in_3_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_c_bits_size = buffer_auto_in_3_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_c_bits_source = buffer_auto_in_3_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_c_bits_address = buffer_auto_in_3_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_c_bits_data = buffer_auto_in_3_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_c_bits_corrupt = buffer_auto_in_3_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_d_ready = buffer_auto_in_3_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_e_valid = buffer_auto_in_3_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_e_bits_sink = buffer_auto_in_3_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_valid = buffer_auto_in_2_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_opcode = buffer_auto_in_2_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_param = buffer_auto_in_2_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_size = buffer_auto_in_2_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_source = buffer_auto_in_2_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_address = buffer_auto_in_2_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_mask = buffer_auto_in_2_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_data = buffer_auto_in_2_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_corrupt = buffer_auto_in_2_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_b_ready = buffer_auto_in_2_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_c_valid = buffer_auto_in_2_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_c_bits_opcode = buffer_auto_in_2_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_c_bits_param = buffer_auto_in_2_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_c_bits_size = buffer_auto_in_2_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_c_bits_source = buffer_auto_in_2_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_c_bits_address = buffer_auto_in_2_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_c_bits_data = buffer_auto_in_2_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_c_bits_corrupt = buffer_auto_in_2_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_d_ready = buffer_auto_in_2_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_e_valid = buffer_auto_in_2_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_e_bits_sink = buffer_auto_in_2_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_valid = buffer_auto_in_1_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_opcode = buffer_auto_in_1_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_param = buffer_auto_in_1_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_size = buffer_auto_in_1_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_source = buffer_auto_in_1_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_address = buffer_auto_in_1_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_mask = buffer_auto_in_1_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_data = buffer_auto_in_1_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_corrupt = buffer_auto_in_1_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_b_ready = buffer_auto_in_1_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_c_valid = buffer_auto_in_1_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_c_bits_opcode = buffer_auto_in_1_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_c_bits_param = buffer_auto_in_1_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_c_bits_size = buffer_auto_in_1_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_c_bits_source = buffer_auto_in_1_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_c_bits_address = buffer_auto_in_1_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_c_bits_data = buffer_auto_in_1_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_c_bits_corrupt = buffer_auto_in_1_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_d_ready = buffer_auto_in_1_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_e_valid = buffer_auto_in_1_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_e_bits_sink = buffer_auto_in_1_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_valid = buffer_auto_in_0_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_opcode = buffer_auto_in_0_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_param = buffer_auto_in_0_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_size = buffer_auto_in_0_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_source = buffer_auto_in_0_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_address = buffer_auto_in_0_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_mask = buffer_auto_in_0_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_data = buffer_auto_in_0_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_corrupt = buffer_auto_in_0_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_b_ready = buffer_auto_in_0_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_c_valid = buffer_auto_in_0_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_c_bits_opcode = buffer_auto_in_0_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_c_bits_param = buffer_auto_in_0_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_c_bits_size = buffer_auto_in_0_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_c_bits_source = buffer_auto_in_0_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_c_bits_address = buffer_auto_in_0_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_c_bits_data = buffer_auto_in_0_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_c_bits_corrupt = buffer_auto_in_0_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_d_ready = buffer_auto_in_0_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_e_valid = buffer_auto_in_0_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_e_bits_sink = buffer_auto_in_0_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_denied; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_denied; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_denied; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_denied; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_bits_corrupt; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_secure; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_mask; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_3_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_secure; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_mask; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_2_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_secure; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_mask; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_1_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_valid; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_param; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_secure; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_mask; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_in_0_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_ready; // @[LazyModule.scala 388:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_a_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_3_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_a_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_2_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_a_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_1_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_a_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_widget_auto_out_0_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_user_amba_prot_secure; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_user_amba_prot_secure; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_user_amba_prot_secure; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_valid =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_param =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_address =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_user_amba_prot_bufferable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_user_amba_prot_modifiable =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_user_amba_prot_readalloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_user_amba_prot_writealloc =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_user_amba_prot_privileged =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_user_amba_prot_secure =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_user_amba_prot_secure; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_mask =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_ready =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_ready; // @[LazyModule.scala 390:12]
  assign auto_l2_in_a_ready = l2_auto_in_4_a_ready; // @[LazyModule.scala 388:16]
  assign auto_l2_in_d_valid = l2_auto_in_4_d_valid; // @[LazyModule.scala 388:16]
  assign auto_l2_in_d_bits_opcode = l2_auto_in_4_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_l2_in_d_bits_size = l2_auto_in_4_d_bits_size; // @[LazyModule.scala 388:16]
  assign auto_l2_in_d_bits_source = l2_auto_in_4_d_bits_source; // @[LazyModule.scala 388:16]
  assign auto_l2_in_d_bits_data = l2_auto_in_4_d_bits_data; // @[LazyModule.scala 388:16]
  assign auto_l2_side_in_a_ready = l2_auto_side_in_a_ready; // @[LazyModule.scala 388:16]
  assign auto_l2_side_in_d_valid = l2_auto_side_in_d_valid; // @[LazyModule.scala 388:16]
  assign auto_l2_side_in_d_bits_opcode = l2_auto_side_in_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_l2_side_in_d_bits_size = l2_auto_side_in_d_bits_size; // @[LazyModule.scala 388:16]
  assign auto_l2_side_in_d_bits_source = l2_auto_side_in_d_bits_source; // @[LazyModule.scala 388:16]
  assign auto_l2_side_in_d_bits_data = l2_auto_side_in_d_bits_data; // @[LazyModule.scala 388:16]
  assign auto_l2_side_in_d_bits_corrupt = l2_auto_side_in_d_bits_corrupt; // @[LazyModule.scala 388:16]
  assign auto_subsystem_l2_clock_groups_out_member_subsystem_mbus_address_adjuster_1_clock =
    subsystem_l2_clock_groups_auto_out_1_member_subsystem_mbus_address_adjuster_1_clock; // @[LazyModule.scala 390:12]
  assign auto_subsystem_l2_clock_groups_out_member_subsystem_mbus_address_adjuster_1_reset =
    subsystem_l2_clock_groups_auto_out_1_member_subsystem_mbus_address_adjuster_1_reset; // @[LazyModule.scala 390:12]
  assign auto_bus_xing_in_3_a_ready = buffer_auto_in_3_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_b_valid = buffer_auto_in_3_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_b_bits_opcode = buffer_auto_in_3_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_b_bits_param = buffer_auto_in_3_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_b_bits_size = buffer_auto_in_3_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_b_bits_source = buffer_auto_in_3_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_b_bits_address = buffer_auto_in_3_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_b_bits_mask = buffer_auto_in_3_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_b_bits_data = buffer_auto_in_3_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_b_bits_corrupt = buffer_auto_in_3_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_c_ready = buffer_auto_in_3_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_valid = buffer_auto_in_3_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_bits_opcode = buffer_auto_in_3_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_bits_param = buffer_auto_in_3_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_bits_size = buffer_auto_in_3_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_bits_source = buffer_auto_in_3_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_bits_sink = buffer_auto_in_3_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_bits_denied = buffer_auto_in_3_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_bits_data = buffer_auto_in_3_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_bits_corrupt = buffer_auto_in_3_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_e_ready = buffer_auto_in_3_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_a_ready = buffer_auto_in_2_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_b_valid = buffer_auto_in_2_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_b_bits_opcode = buffer_auto_in_2_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_b_bits_param = buffer_auto_in_2_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_b_bits_size = buffer_auto_in_2_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_b_bits_source = buffer_auto_in_2_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_b_bits_address = buffer_auto_in_2_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_b_bits_mask = buffer_auto_in_2_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_b_bits_data = buffer_auto_in_2_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_b_bits_corrupt = buffer_auto_in_2_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_c_ready = buffer_auto_in_2_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_valid = buffer_auto_in_2_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_bits_opcode = buffer_auto_in_2_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_bits_param = buffer_auto_in_2_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_bits_size = buffer_auto_in_2_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_bits_source = buffer_auto_in_2_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_bits_sink = buffer_auto_in_2_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_bits_denied = buffer_auto_in_2_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_bits_data = buffer_auto_in_2_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_bits_corrupt = buffer_auto_in_2_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_e_ready = buffer_auto_in_2_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_a_ready = buffer_auto_in_1_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_b_valid = buffer_auto_in_1_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_b_bits_opcode = buffer_auto_in_1_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_b_bits_param = buffer_auto_in_1_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_b_bits_size = buffer_auto_in_1_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_b_bits_source = buffer_auto_in_1_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_b_bits_address = buffer_auto_in_1_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_b_bits_mask = buffer_auto_in_1_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_b_bits_data = buffer_auto_in_1_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_b_bits_corrupt = buffer_auto_in_1_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_c_ready = buffer_auto_in_1_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_valid = buffer_auto_in_1_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_bits_opcode = buffer_auto_in_1_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_bits_param = buffer_auto_in_1_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_bits_size = buffer_auto_in_1_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_bits_source = buffer_auto_in_1_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_bits_sink = buffer_auto_in_1_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_bits_denied = buffer_auto_in_1_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_bits_data = buffer_auto_in_1_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_bits_corrupt = buffer_auto_in_1_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_e_ready = buffer_auto_in_1_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_a_ready = buffer_auto_in_0_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_b_valid = buffer_auto_in_0_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_b_bits_opcode = buffer_auto_in_0_b_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_b_bits_param = buffer_auto_in_0_b_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_b_bits_size = buffer_auto_in_0_b_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_b_bits_source = buffer_auto_in_0_b_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_b_bits_address = buffer_auto_in_0_b_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_b_bits_mask = buffer_auto_in_0_b_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_b_bits_data = buffer_auto_in_0_b_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_b_bits_corrupt = buffer_auto_in_0_b_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_c_ready = buffer_auto_in_0_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_valid = buffer_auto_in_0_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_bits_opcode = buffer_auto_in_0_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_bits_param = buffer_auto_in_0_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_bits_size = buffer_auto_in_0_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_bits_source = buffer_auto_in_0_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_bits_sink = buffer_auto_in_0_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_bits_denied = buffer_auto_in_0_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_bits_data = buffer_auto_in_0_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_bits_corrupt = buffer_auto_in_0_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_e_ready = buffer_auto_in_0_e_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign subsystem_l2_clock_groups_auto_in_member_subsystem_l2_2_clock =
    auto_subsystem_l2_clock_groups_in_member_subsystem_l2_2_clock; // @[LazyModule.scala 388:16]
  assign subsystem_l2_clock_groups_auto_in_member_subsystem_l2_2_reset =
    auto_subsystem_l2_clock_groups_in_member_subsystem_l2_2_reset; // @[LazyModule.scala 388:16]
  assign subsystem_l2_clock_groups_auto_in_member_subsystem_l2_0_clock =
    auto_subsystem_l2_clock_groups_in_member_subsystem_l2_0_clock; // @[LazyModule.scala 388:16]
  assign subsystem_l2_clock_groups_auto_in_member_subsystem_l2_0_reset =
    auto_subsystem_l2_clock_groups_in_member_subsystem_l2_0_reset; // @[LazyModule.scala 388:16]
  assign clockGroup_auto_in_member_subsystem_l2_0_clock =
    subsystem_l2_clock_groups_auto_out_0_member_subsystem_l2_0_clock; // @[LazyModule.scala 377:16]
  assign clockGroup_auto_in_member_subsystem_l2_0_reset =
    subsystem_l2_clock_groups_auto_out_0_member_subsystem_l2_0_reset; // @[LazyModule.scala 377:16]
  assign fixedClockNode_auto_in_clock = clockGroup_auto_out_clock; // @[LazyModule.scala 377:16]
  assign fixedClockNode_auto_in_reset = clockGroup_auto_out_reset; // @[LazyModule.scala 377:16]
  assign l2_clock = fixedClockNode_auto_out_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign l2_reset = fixedClockNode_auto_out_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign l2_auto_in_4_a_valid = auto_l2_in_a_valid; // @[LazyModule.scala 388:16]
  assign l2_auto_in_4_a_bits_opcode = auto_l2_in_a_bits_opcode; // @[LazyModule.scala 388:16]
  assign l2_auto_in_4_a_bits_param = auto_l2_in_a_bits_param; // @[LazyModule.scala 388:16]
  assign l2_auto_in_4_a_bits_size = auto_l2_in_a_bits_size; // @[LazyModule.scala 388:16]
  assign l2_auto_in_4_a_bits_source = auto_l2_in_a_bits_source; // @[LazyModule.scala 388:16]
  assign l2_auto_in_4_a_bits_address = auto_l2_in_a_bits_address; // @[LazyModule.scala 388:16]
  assign l2_auto_in_4_a_bits_mask = auto_l2_in_a_bits_mask; // @[LazyModule.scala 388:16]
  assign l2_auto_in_4_a_bits_data = auto_l2_in_a_bits_data; // @[LazyModule.scala 388:16]
  assign l2_auto_in_4_a_bits_corrupt = auto_l2_in_a_bits_corrupt; // @[LazyModule.scala 388:16]
  assign l2_auto_in_4_d_ready = auto_l2_in_d_ready; // @[LazyModule.scala 388:16]
  assign l2_auto_side_in_a_valid = auto_l2_side_in_a_valid; // @[LazyModule.scala 388:16]
  assign l2_auto_side_in_a_bits_opcode = auto_l2_side_in_a_bits_opcode; // @[LazyModule.scala 388:16]
  assign l2_auto_side_in_a_bits_param = auto_l2_side_in_a_bits_param; // @[LazyModule.scala 388:16]
  assign l2_auto_side_in_a_bits_size = auto_l2_side_in_a_bits_size; // @[LazyModule.scala 388:16]
  assign l2_auto_side_in_a_bits_source = auto_l2_side_in_a_bits_source; // @[LazyModule.scala 388:16]
  assign l2_auto_side_in_a_bits_address = auto_l2_side_in_a_bits_address; // @[LazyModule.scala 388:16]
  assign l2_auto_side_in_a_bits_mask = auto_l2_side_in_a_bits_mask; // @[LazyModule.scala 388:16]
  assign l2_auto_side_in_a_bits_data = auto_l2_side_in_a_bits_data; // @[LazyModule.scala 388:16]
  assign l2_auto_side_in_a_bits_corrupt = auto_l2_side_in_a_bits_corrupt; // @[LazyModule.scala 388:16]
  assign l2_auto_side_in_d_ready = auto_l2_side_in_d_ready; // @[LazyModule.scala 388:16]
  assign l2_auto_in_3_a_valid = ComposableCache_inner_TLBuffer_auto_out_3_a_valid; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_a_bits_opcode = ComposableCache_inner_TLBuffer_auto_out_3_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_a_bits_param = ComposableCache_inner_TLBuffer_auto_out_3_a_bits_param; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_a_bits_size = ComposableCache_inner_TLBuffer_auto_out_3_a_bits_size; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_a_bits_source = ComposableCache_inner_TLBuffer_auto_out_3_a_bits_source; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_a_bits_address = ComposableCache_inner_TLBuffer_auto_out_3_a_bits_address; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_a_bits_mask = ComposableCache_inner_TLBuffer_auto_out_3_a_bits_mask; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_a_bits_data = ComposableCache_inner_TLBuffer_auto_out_3_a_bits_data; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_a_bits_corrupt = ComposableCache_inner_TLBuffer_auto_out_3_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_b_ready = ComposableCache_inner_TLBuffer_auto_out_3_b_ready; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_c_valid = ComposableCache_inner_TLBuffer_auto_out_3_c_valid; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_c_bits_opcode = ComposableCache_inner_TLBuffer_auto_out_3_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_c_bits_param = ComposableCache_inner_TLBuffer_auto_out_3_c_bits_param; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_c_bits_size = ComposableCache_inner_TLBuffer_auto_out_3_c_bits_size; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_c_bits_source = ComposableCache_inner_TLBuffer_auto_out_3_c_bits_source; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_c_bits_address = ComposableCache_inner_TLBuffer_auto_out_3_c_bits_address; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_c_bits_data = ComposableCache_inner_TLBuffer_auto_out_3_c_bits_data; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_c_bits_corrupt = ComposableCache_inner_TLBuffer_auto_out_3_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_d_ready = ComposableCache_inner_TLBuffer_auto_out_3_d_ready; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_e_valid = ComposableCache_inner_TLBuffer_auto_out_3_e_valid; // @[LazyModule.scala 375:16]
  assign l2_auto_in_3_e_bits_sink = ComposableCache_inner_TLBuffer_auto_out_3_e_bits_sink; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_a_valid = ComposableCache_inner_TLBuffer_auto_out_2_a_valid; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_a_bits_opcode = ComposableCache_inner_TLBuffer_auto_out_2_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_a_bits_param = ComposableCache_inner_TLBuffer_auto_out_2_a_bits_param; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_a_bits_size = ComposableCache_inner_TLBuffer_auto_out_2_a_bits_size; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_a_bits_source = ComposableCache_inner_TLBuffer_auto_out_2_a_bits_source; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_a_bits_address = ComposableCache_inner_TLBuffer_auto_out_2_a_bits_address; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_a_bits_mask = ComposableCache_inner_TLBuffer_auto_out_2_a_bits_mask; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_a_bits_data = ComposableCache_inner_TLBuffer_auto_out_2_a_bits_data; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_a_bits_corrupt = ComposableCache_inner_TLBuffer_auto_out_2_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_b_ready = ComposableCache_inner_TLBuffer_auto_out_2_b_ready; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_c_valid = ComposableCache_inner_TLBuffer_auto_out_2_c_valid; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_c_bits_opcode = ComposableCache_inner_TLBuffer_auto_out_2_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_c_bits_param = ComposableCache_inner_TLBuffer_auto_out_2_c_bits_param; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_c_bits_size = ComposableCache_inner_TLBuffer_auto_out_2_c_bits_size; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_c_bits_source = ComposableCache_inner_TLBuffer_auto_out_2_c_bits_source; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_c_bits_address = ComposableCache_inner_TLBuffer_auto_out_2_c_bits_address; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_c_bits_data = ComposableCache_inner_TLBuffer_auto_out_2_c_bits_data; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_c_bits_corrupt = ComposableCache_inner_TLBuffer_auto_out_2_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_d_ready = ComposableCache_inner_TLBuffer_auto_out_2_d_ready; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_e_valid = ComposableCache_inner_TLBuffer_auto_out_2_e_valid; // @[LazyModule.scala 375:16]
  assign l2_auto_in_2_e_bits_sink = ComposableCache_inner_TLBuffer_auto_out_2_e_bits_sink; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_a_valid = ComposableCache_inner_TLBuffer_auto_out_1_a_valid; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_a_bits_opcode = ComposableCache_inner_TLBuffer_auto_out_1_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_a_bits_param = ComposableCache_inner_TLBuffer_auto_out_1_a_bits_param; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_a_bits_size = ComposableCache_inner_TLBuffer_auto_out_1_a_bits_size; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_a_bits_source = ComposableCache_inner_TLBuffer_auto_out_1_a_bits_source; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_a_bits_address = ComposableCache_inner_TLBuffer_auto_out_1_a_bits_address; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_a_bits_mask = ComposableCache_inner_TLBuffer_auto_out_1_a_bits_mask; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_a_bits_data = ComposableCache_inner_TLBuffer_auto_out_1_a_bits_data; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_a_bits_corrupt = ComposableCache_inner_TLBuffer_auto_out_1_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_b_ready = ComposableCache_inner_TLBuffer_auto_out_1_b_ready; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_c_valid = ComposableCache_inner_TLBuffer_auto_out_1_c_valid; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_c_bits_opcode = ComposableCache_inner_TLBuffer_auto_out_1_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_c_bits_param = ComposableCache_inner_TLBuffer_auto_out_1_c_bits_param; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_c_bits_size = ComposableCache_inner_TLBuffer_auto_out_1_c_bits_size; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_c_bits_source = ComposableCache_inner_TLBuffer_auto_out_1_c_bits_source; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_c_bits_address = ComposableCache_inner_TLBuffer_auto_out_1_c_bits_address; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_c_bits_data = ComposableCache_inner_TLBuffer_auto_out_1_c_bits_data; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_c_bits_corrupt = ComposableCache_inner_TLBuffer_auto_out_1_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_d_ready = ComposableCache_inner_TLBuffer_auto_out_1_d_ready; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_e_valid = ComposableCache_inner_TLBuffer_auto_out_1_e_valid; // @[LazyModule.scala 375:16]
  assign l2_auto_in_1_e_bits_sink = ComposableCache_inner_TLBuffer_auto_out_1_e_bits_sink; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_a_valid = ComposableCache_inner_TLBuffer_auto_out_0_a_valid; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_a_bits_opcode = ComposableCache_inner_TLBuffer_auto_out_0_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_a_bits_param = ComposableCache_inner_TLBuffer_auto_out_0_a_bits_param; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_a_bits_size = ComposableCache_inner_TLBuffer_auto_out_0_a_bits_size; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_a_bits_source = ComposableCache_inner_TLBuffer_auto_out_0_a_bits_source; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_a_bits_address = ComposableCache_inner_TLBuffer_auto_out_0_a_bits_address; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_a_bits_mask = ComposableCache_inner_TLBuffer_auto_out_0_a_bits_mask; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_a_bits_data = ComposableCache_inner_TLBuffer_auto_out_0_a_bits_data; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_a_bits_corrupt = ComposableCache_inner_TLBuffer_auto_out_0_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_b_ready = ComposableCache_inner_TLBuffer_auto_out_0_b_ready; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_c_valid = ComposableCache_inner_TLBuffer_auto_out_0_c_valid; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_c_bits_opcode = ComposableCache_inner_TLBuffer_auto_out_0_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_c_bits_param = ComposableCache_inner_TLBuffer_auto_out_0_c_bits_param; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_c_bits_size = ComposableCache_inner_TLBuffer_auto_out_0_c_bits_size; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_c_bits_source = ComposableCache_inner_TLBuffer_auto_out_0_c_bits_source; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_c_bits_address = ComposableCache_inner_TLBuffer_auto_out_0_c_bits_address; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_c_bits_data = ComposableCache_inner_TLBuffer_auto_out_0_c_bits_data; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_c_bits_corrupt = ComposableCache_inner_TLBuffer_auto_out_0_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_d_ready = ComposableCache_inner_TLBuffer_auto_out_0_d_ready; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_e_valid = ComposableCache_inner_TLBuffer_auto_out_0_e_valid; // @[LazyModule.scala 375:16]
  assign l2_auto_in_0_e_bits_sink = ComposableCache_inner_TLBuffer_auto_out_0_e_bits_sink; // @[LazyModule.scala 375:16]
  assign l2_auto_out_3_a_ready = ComposableCache_outer_TLBuffer_auto_in_3_a_ready; // @[LazyModule.scala 377:16]
  assign l2_auto_out_3_c_ready = ComposableCache_outer_TLBuffer_auto_in_3_c_ready; // @[LazyModule.scala 377:16]
  assign l2_auto_out_3_d_valid = ComposableCache_outer_TLBuffer_auto_in_3_d_valid; // @[LazyModule.scala 377:16]
  assign l2_auto_out_3_d_bits_opcode = ComposableCache_outer_TLBuffer_auto_in_3_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign l2_auto_out_3_d_bits_param = ComposableCache_outer_TLBuffer_auto_in_3_d_bits_param; // @[LazyModule.scala 377:16]
  assign l2_auto_out_3_d_bits_size = ComposableCache_outer_TLBuffer_auto_in_3_d_bits_size; // @[LazyModule.scala 377:16]
  assign l2_auto_out_3_d_bits_source = ComposableCache_outer_TLBuffer_auto_in_3_d_bits_source; // @[LazyModule.scala 377:16]
  assign l2_auto_out_3_d_bits_sink = ComposableCache_outer_TLBuffer_auto_in_3_d_bits_sink; // @[LazyModule.scala 377:16]
  assign l2_auto_out_3_d_bits_denied = ComposableCache_outer_TLBuffer_auto_in_3_d_bits_denied; // @[LazyModule.scala 377:16]
  assign l2_auto_out_3_d_bits_data = ComposableCache_outer_TLBuffer_auto_in_3_d_bits_data; // @[LazyModule.scala 377:16]
  assign l2_auto_out_3_d_bits_corrupt = ComposableCache_outer_TLBuffer_auto_in_3_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign l2_auto_out_2_a_ready = ComposableCache_outer_TLBuffer_auto_in_2_a_ready; // @[LazyModule.scala 377:16]
  assign l2_auto_out_2_c_ready = ComposableCache_outer_TLBuffer_auto_in_2_c_ready; // @[LazyModule.scala 377:16]
  assign l2_auto_out_2_d_valid = ComposableCache_outer_TLBuffer_auto_in_2_d_valid; // @[LazyModule.scala 377:16]
  assign l2_auto_out_2_d_bits_opcode = ComposableCache_outer_TLBuffer_auto_in_2_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign l2_auto_out_2_d_bits_param = ComposableCache_outer_TLBuffer_auto_in_2_d_bits_param; // @[LazyModule.scala 377:16]
  assign l2_auto_out_2_d_bits_size = ComposableCache_outer_TLBuffer_auto_in_2_d_bits_size; // @[LazyModule.scala 377:16]
  assign l2_auto_out_2_d_bits_source = ComposableCache_outer_TLBuffer_auto_in_2_d_bits_source; // @[LazyModule.scala 377:16]
  assign l2_auto_out_2_d_bits_sink = ComposableCache_outer_TLBuffer_auto_in_2_d_bits_sink; // @[LazyModule.scala 377:16]
  assign l2_auto_out_2_d_bits_denied = ComposableCache_outer_TLBuffer_auto_in_2_d_bits_denied; // @[LazyModule.scala 377:16]
  assign l2_auto_out_2_d_bits_data = ComposableCache_outer_TLBuffer_auto_in_2_d_bits_data; // @[LazyModule.scala 377:16]
  assign l2_auto_out_2_d_bits_corrupt = ComposableCache_outer_TLBuffer_auto_in_2_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign l2_auto_out_1_a_ready = ComposableCache_outer_TLBuffer_auto_in_1_a_ready; // @[LazyModule.scala 377:16]
  assign l2_auto_out_1_c_ready = ComposableCache_outer_TLBuffer_auto_in_1_c_ready; // @[LazyModule.scala 377:16]
  assign l2_auto_out_1_d_valid = ComposableCache_outer_TLBuffer_auto_in_1_d_valid; // @[LazyModule.scala 377:16]
  assign l2_auto_out_1_d_bits_opcode = ComposableCache_outer_TLBuffer_auto_in_1_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign l2_auto_out_1_d_bits_param = ComposableCache_outer_TLBuffer_auto_in_1_d_bits_param; // @[LazyModule.scala 377:16]
  assign l2_auto_out_1_d_bits_size = ComposableCache_outer_TLBuffer_auto_in_1_d_bits_size; // @[LazyModule.scala 377:16]
  assign l2_auto_out_1_d_bits_source = ComposableCache_outer_TLBuffer_auto_in_1_d_bits_source; // @[LazyModule.scala 377:16]
  assign l2_auto_out_1_d_bits_sink = ComposableCache_outer_TLBuffer_auto_in_1_d_bits_sink; // @[LazyModule.scala 377:16]
  assign l2_auto_out_1_d_bits_denied = ComposableCache_outer_TLBuffer_auto_in_1_d_bits_denied; // @[LazyModule.scala 377:16]
  assign l2_auto_out_1_d_bits_data = ComposableCache_outer_TLBuffer_auto_in_1_d_bits_data; // @[LazyModule.scala 377:16]
  assign l2_auto_out_1_d_bits_corrupt = ComposableCache_outer_TLBuffer_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign l2_auto_out_0_a_ready = ComposableCache_outer_TLBuffer_auto_in_0_a_ready; // @[LazyModule.scala 377:16]
  assign l2_auto_out_0_c_ready = ComposableCache_outer_TLBuffer_auto_in_0_c_ready; // @[LazyModule.scala 377:16]
  assign l2_auto_out_0_d_valid = ComposableCache_outer_TLBuffer_auto_in_0_d_valid; // @[LazyModule.scala 377:16]
  assign l2_auto_out_0_d_bits_opcode = ComposableCache_outer_TLBuffer_auto_in_0_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign l2_auto_out_0_d_bits_param = ComposableCache_outer_TLBuffer_auto_in_0_d_bits_param; // @[LazyModule.scala 377:16]
  assign l2_auto_out_0_d_bits_size = ComposableCache_outer_TLBuffer_auto_in_0_d_bits_size; // @[LazyModule.scala 377:16]
  assign l2_auto_out_0_d_bits_source = ComposableCache_outer_TLBuffer_auto_in_0_d_bits_source; // @[LazyModule.scala 377:16]
  assign l2_auto_out_0_d_bits_sink = ComposableCache_outer_TLBuffer_auto_in_0_d_bits_sink; // @[LazyModule.scala 377:16]
  assign l2_auto_out_0_d_bits_denied = ComposableCache_outer_TLBuffer_auto_in_0_d_bits_denied; // @[LazyModule.scala 377:16]
  assign l2_auto_out_0_d_bits_data = ComposableCache_outer_TLBuffer_auto_in_0_d_bits_data; // @[LazyModule.scala 377:16]
  assign l2_auto_out_0_d_bits_corrupt = ComposableCache_outer_TLBuffer_auto_in_0_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign filter_auto_in_3_a_valid = coherent_jbar_auto_out_3_a_valid; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_a_bits_opcode = coherent_jbar_auto_out_3_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_a_bits_param = coherent_jbar_auto_out_3_a_bits_param; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_a_bits_size = coherent_jbar_auto_out_3_a_bits_size; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_a_bits_source = coherent_jbar_auto_out_3_a_bits_source; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_a_bits_address = coherent_jbar_auto_out_3_a_bits_address; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_a_bits_mask = coherent_jbar_auto_out_3_a_bits_mask; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_a_bits_data = coherent_jbar_auto_out_3_a_bits_data; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_a_bits_corrupt = coherent_jbar_auto_out_3_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_b_ready = coherent_jbar_auto_out_3_b_ready; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_c_valid = coherent_jbar_auto_out_3_c_valid; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_c_bits_opcode = coherent_jbar_auto_out_3_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_c_bits_param = coherent_jbar_auto_out_3_c_bits_param; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_c_bits_size = coherent_jbar_auto_out_3_c_bits_size; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_c_bits_source = coherent_jbar_auto_out_3_c_bits_source; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_c_bits_address = coherent_jbar_auto_out_3_c_bits_address; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_c_bits_data = coherent_jbar_auto_out_3_c_bits_data; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_c_bits_corrupt = coherent_jbar_auto_out_3_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_d_ready = coherent_jbar_auto_out_3_d_ready; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_e_valid = coherent_jbar_auto_out_3_e_valid; // @[LazyModule.scala 375:16]
  assign filter_auto_in_3_e_bits_sink = coherent_jbar_auto_out_3_e_bits_sink; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_a_valid = coherent_jbar_auto_out_2_a_valid; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_a_bits_opcode = coherent_jbar_auto_out_2_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_a_bits_param = coherent_jbar_auto_out_2_a_bits_param; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_a_bits_size = coherent_jbar_auto_out_2_a_bits_size; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_a_bits_source = coherent_jbar_auto_out_2_a_bits_source; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_a_bits_address = coherent_jbar_auto_out_2_a_bits_address; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_a_bits_mask = coherent_jbar_auto_out_2_a_bits_mask; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_a_bits_data = coherent_jbar_auto_out_2_a_bits_data; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_a_bits_corrupt = coherent_jbar_auto_out_2_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_b_ready = coherent_jbar_auto_out_2_b_ready; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_c_valid = coherent_jbar_auto_out_2_c_valid; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_c_bits_opcode = coherent_jbar_auto_out_2_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_c_bits_param = coherent_jbar_auto_out_2_c_bits_param; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_c_bits_size = coherent_jbar_auto_out_2_c_bits_size; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_c_bits_source = coherent_jbar_auto_out_2_c_bits_source; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_c_bits_address = coherent_jbar_auto_out_2_c_bits_address; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_c_bits_data = coherent_jbar_auto_out_2_c_bits_data; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_c_bits_corrupt = coherent_jbar_auto_out_2_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_d_ready = coherent_jbar_auto_out_2_d_ready; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_e_valid = coherent_jbar_auto_out_2_e_valid; // @[LazyModule.scala 375:16]
  assign filter_auto_in_2_e_bits_sink = coherent_jbar_auto_out_2_e_bits_sink; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_a_valid = coherent_jbar_auto_out_1_a_valid; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_a_bits_opcode = coherent_jbar_auto_out_1_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_a_bits_param = coherent_jbar_auto_out_1_a_bits_param; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_a_bits_size = coherent_jbar_auto_out_1_a_bits_size; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_a_bits_source = coherent_jbar_auto_out_1_a_bits_source; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_a_bits_address = coherent_jbar_auto_out_1_a_bits_address; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_a_bits_mask = coherent_jbar_auto_out_1_a_bits_mask; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_a_bits_data = coherent_jbar_auto_out_1_a_bits_data; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_a_bits_corrupt = coherent_jbar_auto_out_1_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_b_ready = coherent_jbar_auto_out_1_b_ready; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_c_valid = coherent_jbar_auto_out_1_c_valid; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_c_bits_opcode = coherent_jbar_auto_out_1_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_c_bits_param = coherent_jbar_auto_out_1_c_bits_param; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_c_bits_size = coherent_jbar_auto_out_1_c_bits_size; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_c_bits_source = coherent_jbar_auto_out_1_c_bits_source; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_c_bits_address = coherent_jbar_auto_out_1_c_bits_address; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_c_bits_data = coherent_jbar_auto_out_1_c_bits_data; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_c_bits_corrupt = coherent_jbar_auto_out_1_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_d_ready = coherent_jbar_auto_out_1_d_ready; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_e_valid = coherent_jbar_auto_out_1_e_valid; // @[LazyModule.scala 375:16]
  assign filter_auto_in_1_e_bits_sink = coherent_jbar_auto_out_1_e_bits_sink; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_a_valid = coherent_jbar_auto_out_0_a_valid; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_a_bits_opcode = coherent_jbar_auto_out_0_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_a_bits_param = coherent_jbar_auto_out_0_a_bits_param; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_a_bits_size = coherent_jbar_auto_out_0_a_bits_size; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_a_bits_source = coherent_jbar_auto_out_0_a_bits_source; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_a_bits_address = coherent_jbar_auto_out_0_a_bits_address; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_a_bits_mask = coherent_jbar_auto_out_0_a_bits_mask; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_a_bits_data = coherent_jbar_auto_out_0_a_bits_data; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_a_bits_corrupt = coherent_jbar_auto_out_0_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_b_ready = coherent_jbar_auto_out_0_b_ready; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_c_valid = coherent_jbar_auto_out_0_c_valid; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_c_bits_opcode = coherent_jbar_auto_out_0_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_c_bits_param = coherent_jbar_auto_out_0_c_bits_param; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_c_bits_size = coherent_jbar_auto_out_0_c_bits_size; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_c_bits_source = coherent_jbar_auto_out_0_c_bits_source; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_c_bits_address = coherent_jbar_auto_out_0_c_bits_address; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_c_bits_data = coherent_jbar_auto_out_0_c_bits_data; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_c_bits_corrupt = coherent_jbar_auto_out_0_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_d_ready = coherent_jbar_auto_out_0_d_ready; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_e_valid = coherent_jbar_auto_out_0_e_valid; // @[LazyModule.scala 375:16]
  assign filter_auto_in_0_e_bits_sink = coherent_jbar_auto_out_0_e_bits_sink; // @[LazyModule.scala 375:16]
  assign filter_auto_out_3_a_ready = ComposableCache_inner_TLBuffer_auto_in_3_a_ready; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_b_valid = ComposableCache_inner_TLBuffer_auto_in_3_b_valid; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_b_bits_opcode = ComposableCache_inner_TLBuffer_auto_in_3_b_bits_opcode; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_b_bits_param = ComposableCache_inner_TLBuffer_auto_in_3_b_bits_param; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_b_bits_size = ComposableCache_inner_TLBuffer_auto_in_3_b_bits_size; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_b_bits_source = ComposableCache_inner_TLBuffer_auto_in_3_b_bits_source; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_b_bits_address = ComposableCache_inner_TLBuffer_auto_in_3_b_bits_address; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_b_bits_mask = ComposableCache_inner_TLBuffer_auto_in_3_b_bits_mask; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_b_bits_data = ComposableCache_inner_TLBuffer_auto_in_3_b_bits_data; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_b_bits_corrupt = ComposableCache_inner_TLBuffer_auto_in_3_b_bits_corrupt; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_c_ready = ComposableCache_inner_TLBuffer_auto_in_3_c_ready; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_d_valid = ComposableCache_inner_TLBuffer_auto_in_3_d_valid; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_d_bits_opcode = ComposableCache_inner_TLBuffer_auto_in_3_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_d_bits_param = ComposableCache_inner_TLBuffer_auto_in_3_d_bits_param; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_d_bits_size = ComposableCache_inner_TLBuffer_auto_in_3_d_bits_size; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_d_bits_source = ComposableCache_inner_TLBuffer_auto_in_3_d_bits_source; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_d_bits_sink = ComposableCache_inner_TLBuffer_auto_in_3_d_bits_sink; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_d_bits_denied = ComposableCache_inner_TLBuffer_auto_in_3_d_bits_denied; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_d_bits_data = ComposableCache_inner_TLBuffer_auto_in_3_d_bits_data; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_d_bits_corrupt = ComposableCache_inner_TLBuffer_auto_in_3_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign filter_auto_out_3_e_ready = ComposableCache_inner_TLBuffer_auto_in_3_e_ready; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_a_ready = ComposableCache_inner_TLBuffer_auto_in_2_a_ready; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_b_valid = ComposableCache_inner_TLBuffer_auto_in_2_b_valid; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_b_bits_opcode = ComposableCache_inner_TLBuffer_auto_in_2_b_bits_opcode; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_b_bits_param = ComposableCache_inner_TLBuffer_auto_in_2_b_bits_param; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_b_bits_size = ComposableCache_inner_TLBuffer_auto_in_2_b_bits_size; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_b_bits_source = ComposableCache_inner_TLBuffer_auto_in_2_b_bits_source; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_b_bits_address = ComposableCache_inner_TLBuffer_auto_in_2_b_bits_address; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_b_bits_mask = ComposableCache_inner_TLBuffer_auto_in_2_b_bits_mask; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_b_bits_data = ComposableCache_inner_TLBuffer_auto_in_2_b_bits_data; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_b_bits_corrupt = ComposableCache_inner_TLBuffer_auto_in_2_b_bits_corrupt; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_c_ready = ComposableCache_inner_TLBuffer_auto_in_2_c_ready; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_d_valid = ComposableCache_inner_TLBuffer_auto_in_2_d_valid; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_d_bits_opcode = ComposableCache_inner_TLBuffer_auto_in_2_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_d_bits_param = ComposableCache_inner_TLBuffer_auto_in_2_d_bits_param; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_d_bits_size = ComposableCache_inner_TLBuffer_auto_in_2_d_bits_size; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_d_bits_source = ComposableCache_inner_TLBuffer_auto_in_2_d_bits_source; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_d_bits_sink = ComposableCache_inner_TLBuffer_auto_in_2_d_bits_sink; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_d_bits_denied = ComposableCache_inner_TLBuffer_auto_in_2_d_bits_denied; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_d_bits_data = ComposableCache_inner_TLBuffer_auto_in_2_d_bits_data; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_d_bits_corrupt = ComposableCache_inner_TLBuffer_auto_in_2_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign filter_auto_out_2_e_ready = ComposableCache_inner_TLBuffer_auto_in_2_e_ready; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_a_ready = ComposableCache_inner_TLBuffer_auto_in_1_a_ready; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_b_valid = ComposableCache_inner_TLBuffer_auto_in_1_b_valid; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_b_bits_opcode = ComposableCache_inner_TLBuffer_auto_in_1_b_bits_opcode; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_b_bits_param = ComposableCache_inner_TLBuffer_auto_in_1_b_bits_param; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_b_bits_size = ComposableCache_inner_TLBuffer_auto_in_1_b_bits_size; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_b_bits_source = ComposableCache_inner_TLBuffer_auto_in_1_b_bits_source; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_b_bits_address = ComposableCache_inner_TLBuffer_auto_in_1_b_bits_address; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_b_bits_mask = ComposableCache_inner_TLBuffer_auto_in_1_b_bits_mask; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_b_bits_data = ComposableCache_inner_TLBuffer_auto_in_1_b_bits_data; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_b_bits_corrupt = ComposableCache_inner_TLBuffer_auto_in_1_b_bits_corrupt; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_c_ready = ComposableCache_inner_TLBuffer_auto_in_1_c_ready; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_d_valid = ComposableCache_inner_TLBuffer_auto_in_1_d_valid; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_d_bits_opcode = ComposableCache_inner_TLBuffer_auto_in_1_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_d_bits_param = ComposableCache_inner_TLBuffer_auto_in_1_d_bits_param; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_d_bits_size = ComposableCache_inner_TLBuffer_auto_in_1_d_bits_size; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_d_bits_source = ComposableCache_inner_TLBuffer_auto_in_1_d_bits_source; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_d_bits_sink = ComposableCache_inner_TLBuffer_auto_in_1_d_bits_sink; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_d_bits_denied = ComposableCache_inner_TLBuffer_auto_in_1_d_bits_denied; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_d_bits_data = ComposableCache_inner_TLBuffer_auto_in_1_d_bits_data; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_d_bits_corrupt = ComposableCache_inner_TLBuffer_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign filter_auto_out_1_e_ready = ComposableCache_inner_TLBuffer_auto_in_1_e_ready; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_a_ready = ComposableCache_inner_TLBuffer_auto_in_0_a_ready; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_b_valid = ComposableCache_inner_TLBuffer_auto_in_0_b_valid; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_b_bits_opcode = ComposableCache_inner_TLBuffer_auto_in_0_b_bits_opcode; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_b_bits_param = ComposableCache_inner_TLBuffer_auto_in_0_b_bits_param; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_b_bits_size = ComposableCache_inner_TLBuffer_auto_in_0_b_bits_size; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_b_bits_source = ComposableCache_inner_TLBuffer_auto_in_0_b_bits_source; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_b_bits_address = ComposableCache_inner_TLBuffer_auto_in_0_b_bits_address; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_b_bits_mask = ComposableCache_inner_TLBuffer_auto_in_0_b_bits_mask; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_b_bits_data = ComposableCache_inner_TLBuffer_auto_in_0_b_bits_data; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_b_bits_corrupt = ComposableCache_inner_TLBuffer_auto_in_0_b_bits_corrupt; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_c_ready = ComposableCache_inner_TLBuffer_auto_in_0_c_ready; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_d_valid = ComposableCache_inner_TLBuffer_auto_in_0_d_valid; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_d_bits_opcode = ComposableCache_inner_TLBuffer_auto_in_0_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_d_bits_param = ComposableCache_inner_TLBuffer_auto_in_0_d_bits_param; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_d_bits_size = ComposableCache_inner_TLBuffer_auto_in_0_d_bits_size; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_d_bits_source = ComposableCache_inner_TLBuffer_auto_in_0_d_bits_source; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_d_bits_sink = ComposableCache_inner_TLBuffer_auto_in_0_d_bits_sink; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_d_bits_denied = ComposableCache_inner_TLBuffer_auto_in_0_d_bits_denied; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_d_bits_data = ComposableCache_inner_TLBuffer_auto_in_0_d_bits_data; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_d_bits_corrupt = ComposableCache_inner_TLBuffer_auto_in_0_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign filter_auto_out_0_e_ready = ComposableCache_inner_TLBuffer_auto_in_0_e_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_clock = fixedClockNode_auto_out_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_reset = fixedClockNode_auto_out_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_a_valid = filter_auto_out_3_a_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_a_bits_opcode = filter_auto_out_3_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_a_bits_param = filter_auto_out_3_a_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_a_bits_size = filter_auto_out_3_a_bits_size; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_a_bits_source = filter_auto_out_3_a_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_a_bits_address = filter_auto_out_3_a_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_a_bits_mask = filter_auto_out_3_a_bits_mask; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_a_bits_data = filter_auto_out_3_a_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_a_bits_corrupt = filter_auto_out_3_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_b_ready = filter_auto_out_3_b_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_c_valid = filter_auto_out_3_c_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_c_bits_opcode = filter_auto_out_3_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_c_bits_param = filter_auto_out_3_c_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_c_bits_size = filter_auto_out_3_c_bits_size; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_c_bits_source = filter_auto_out_3_c_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_c_bits_address = filter_auto_out_3_c_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_c_bits_data = filter_auto_out_3_c_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_c_bits_corrupt = filter_auto_out_3_c_bits_corrupt; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_d_ready = filter_auto_out_3_d_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_e_valid = filter_auto_out_3_e_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_3_e_bits_sink = filter_auto_out_3_e_bits_sink; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_a_valid = filter_auto_out_2_a_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_a_bits_opcode = filter_auto_out_2_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_a_bits_param = filter_auto_out_2_a_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_a_bits_size = filter_auto_out_2_a_bits_size; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_a_bits_source = filter_auto_out_2_a_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_a_bits_address = filter_auto_out_2_a_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_a_bits_mask = filter_auto_out_2_a_bits_mask; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_a_bits_data = filter_auto_out_2_a_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_a_bits_corrupt = filter_auto_out_2_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_b_ready = filter_auto_out_2_b_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_c_valid = filter_auto_out_2_c_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_c_bits_opcode = filter_auto_out_2_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_c_bits_param = filter_auto_out_2_c_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_c_bits_size = filter_auto_out_2_c_bits_size; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_c_bits_source = filter_auto_out_2_c_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_c_bits_address = filter_auto_out_2_c_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_c_bits_data = filter_auto_out_2_c_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_c_bits_corrupt = filter_auto_out_2_c_bits_corrupt; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_d_ready = filter_auto_out_2_d_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_e_valid = filter_auto_out_2_e_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_2_e_bits_sink = filter_auto_out_2_e_bits_sink; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_a_valid = filter_auto_out_1_a_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_a_bits_opcode = filter_auto_out_1_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_a_bits_param = filter_auto_out_1_a_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_a_bits_size = filter_auto_out_1_a_bits_size; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_a_bits_source = filter_auto_out_1_a_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_a_bits_address = filter_auto_out_1_a_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_a_bits_mask = filter_auto_out_1_a_bits_mask; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_a_bits_data = filter_auto_out_1_a_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_a_bits_corrupt = filter_auto_out_1_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_b_ready = filter_auto_out_1_b_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_c_valid = filter_auto_out_1_c_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_c_bits_opcode = filter_auto_out_1_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_c_bits_param = filter_auto_out_1_c_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_c_bits_size = filter_auto_out_1_c_bits_size; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_c_bits_source = filter_auto_out_1_c_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_c_bits_address = filter_auto_out_1_c_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_c_bits_data = filter_auto_out_1_c_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_c_bits_corrupt = filter_auto_out_1_c_bits_corrupt; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_d_ready = filter_auto_out_1_d_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_e_valid = filter_auto_out_1_e_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_1_e_bits_sink = filter_auto_out_1_e_bits_sink; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_a_valid = filter_auto_out_0_a_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_a_bits_opcode = filter_auto_out_0_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_a_bits_param = filter_auto_out_0_a_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_a_bits_size = filter_auto_out_0_a_bits_size; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_a_bits_source = filter_auto_out_0_a_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_a_bits_address = filter_auto_out_0_a_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_a_bits_mask = filter_auto_out_0_a_bits_mask; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_a_bits_data = filter_auto_out_0_a_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_a_bits_corrupt = filter_auto_out_0_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_b_ready = filter_auto_out_0_b_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_c_valid = filter_auto_out_0_c_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_c_bits_opcode = filter_auto_out_0_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_c_bits_param = filter_auto_out_0_c_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_c_bits_size = filter_auto_out_0_c_bits_size; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_c_bits_source = filter_auto_out_0_c_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_c_bits_address = filter_auto_out_0_c_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_c_bits_data = filter_auto_out_0_c_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_c_bits_corrupt = filter_auto_out_0_c_bits_corrupt; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_d_ready = filter_auto_out_0_d_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_e_valid = filter_auto_out_0_e_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_in_0_e_bits_sink = filter_auto_out_0_e_bits_sink; // @[LazyModule.scala 377:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_a_ready = l2_auto_in_3_a_ready; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_b_valid = l2_auto_in_3_b_valid; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_b_bits_opcode = l2_auto_in_3_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_b_bits_param = l2_auto_in_3_b_bits_param; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_b_bits_size = l2_auto_in_3_b_bits_size; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_b_bits_source = l2_auto_in_3_b_bits_source; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_b_bits_address = l2_auto_in_3_b_bits_address; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_b_bits_mask = l2_auto_in_3_b_bits_mask; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_b_bits_data = l2_auto_in_3_b_bits_data; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_b_bits_corrupt = l2_auto_in_3_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_c_ready = l2_auto_in_3_c_ready; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_d_valid = l2_auto_in_3_d_valid; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_d_bits_opcode = l2_auto_in_3_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_d_bits_param = l2_auto_in_3_d_bits_param; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_d_bits_size = l2_auto_in_3_d_bits_size; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_d_bits_source = l2_auto_in_3_d_bits_source; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_d_bits_sink = l2_auto_in_3_d_bits_sink; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_d_bits_denied = l2_auto_in_3_d_bits_denied; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_d_bits_data = l2_auto_in_3_d_bits_data; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_d_bits_corrupt = l2_auto_in_3_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_3_e_ready = l2_auto_in_3_e_ready; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_a_ready = l2_auto_in_2_a_ready; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_b_valid = l2_auto_in_2_b_valid; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_b_bits_opcode = l2_auto_in_2_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_b_bits_param = l2_auto_in_2_b_bits_param; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_b_bits_size = l2_auto_in_2_b_bits_size; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_b_bits_source = l2_auto_in_2_b_bits_source; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_b_bits_address = l2_auto_in_2_b_bits_address; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_b_bits_mask = l2_auto_in_2_b_bits_mask; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_b_bits_data = l2_auto_in_2_b_bits_data; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_b_bits_corrupt = l2_auto_in_2_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_c_ready = l2_auto_in_2_c_ready; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_d_valid = l2_auto_in_2_d_valid; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_d_bits_opcode = l2_auto_in_2_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_d_bits_param = l2_auto_in_2_d_bits_param; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_d_bits_size = l2_auto_in_2_d_bits_size; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_d_bits_source = l2_auto_in_2_d_bits_source; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_d_bits_sink = l2_auto_in_2_d_bits_sink; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_d_bits_denied = l2_auto_in_2_d_bits_denied; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_d_bits_data = l2_auto_in_2_d_bits_data; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_d_bits_corrupt = l2_auto_in_2_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_2_e_ready = l2_auto_in_2_e_ready; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_a_ready = l2_auto_in_1_a_ready; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_b_valid = l2_auto_in_1_b_valid; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_b_bits_opcode = l2_auto_in_1_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_b_bits_param = l2_auto_in_1_b_bits_param; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_b_bits_size = l2_auto_in_1_b_bits_size; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_b_bits_source = l2_auto_in_1_b_bits_source; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_b_bits_address = l2_auto_in_1_b_bits_address; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_b_bits_mask = l2_auto_in_1_b_bits_mask; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_b_bits_data = l2_auto_in_1_b_bits_data; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_b_bits_corrupt = l2_auto_in_1_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_c_ready = l2_auto_in_1_c_ready; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_d_valid = l2_auto_in_1_d_valid; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_d_bits_opcode = l2_auto_in_1_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_d_bits_param = l2_auto_in_1_d_bits_param; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_d_bits_size = l2_auto_in_1_d_bits_size; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_d_bits_source = l2_auto_in_1_d_bits_source; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_d_bits_sink = l2_auto_in_1_d_bits_sink; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_d_bits_denied = l2_auto_in_1_d_bits_denied; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_d_bits_data = l2_auto_in_1_d_bits_data; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_d_bits_corrupt = l2_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_1_e_ready = l2_auto_in_1_e_ready; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_a_ready = l2_auto_in_0_a_ready; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_b_valid = l2_auto_in_0_b_valid; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_b_bits_opcode = l2_auto_in_0_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_b_bits_param = l2_auto_in_0_b_bits_param; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_b_bits_size = l2_auto_in_0_b_bits_size; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_b_bits_source = l2_auto_in_0_b_bits_source; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_b_bits_address = l2_auto_in_0_b_bits_address; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_b_bits_mask = l2_auto_in_0_b_bits_mask; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_b_bits_data = l2_auto_in_0_b_bits_data; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_b_bits_corrupt = l2_auto_in_0_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_c_ready = l2_auto_in_0_c_ready; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_d_valid = l2_auto_in_0_d_valid; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_d_bits_opcode = l2_auto_in_0_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_d_bits_param = l2_auto_in_0_d_bits_param; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_d_bits_size = l2_auto_in_0_d_bits_size; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_d_bits_source = l2_auto_in_0_d_bits_source; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_d_bits_sink = l2_auto_in_0_d_bits_sink; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_d_bits_denied = l2_auto_in_0_d_bits_denied; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_d_bits_data = l2_auto_in_0_d_bits_data; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_d_bits_corrupt = l2_auto_in_0_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign ComposableCache_inner_TLBuffer_auto_out_0_e_ready = l2_auto_in_0_e_ready; // @[LazyModule.scala 375:16]
  assign ComposableCache_outer_TLBuffer_auto_in_3_a_valid = l2_auto_out_3_a_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_3_a_bits_opcode = l2_auto_out_3_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_3_a_bits_param = l2_auto_out_3_a_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_3_a_bits_source = l2_auto_out_3_a_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_3_a_bits_address = l2_auto_out_3_a_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_3_c_valid = l2_auto_out_3_c_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_3_c_bits_opcode = l2_auto_out_3_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_3_c_bits_param = l2_auto_out_3_c_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_3_c_bits_source = l2_auto_out_3_c_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_3_c_bits_address = l2_auto_out_3_c_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_3_c_bits_data = l2_auto_out_3_c_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_3_d_ready = l2_auto_out_3_d_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_3_e_valid = l2_auto_out_3_e_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_3_e_bits_sink = l2_auto_out_3_e_bits_sink; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_2_a_valid = l2_auto_out_2_a_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_2_a_bits_opcode = l2_auto_out_2_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_2_a_bits_param = l2_auto_out_2_a_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_2_a_bits_source = l2_auto_out_2_a_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_2_a_bits_address = l2_auto_out_2_a_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_2_c_valid = l2_auto_out_2_c_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_2_c_bits_opcode = l2_auto_out_2_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_2_c_bits_param = l2_auto_out_2_c_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_2_c_bits_source = l2_auto_out_2_c_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_2_c_bits_address = l2_auto_out_2_c_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_2_c_bits_data = l2_auto_out_2_c_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_2_d_ready = l2_auto_out_2_d_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_2_e_valid = l2_auto_out_2_e_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_2_e_bits_sink = l2_auto_out_2_e_bits_sink; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_1_a_valid = l2_auto_out_1_a_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_1_a_bits_opcode = l2_auto_out_1_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_1_a_bits_param = l2_auto_out_1_a_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_1_a_bits_source = l2_auto_out_1_a_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_1_a_bits_address = l2_auto_out_1_a_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_1_c_valid = l2_auto_out_1_c_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_1_c_bits_opcode = l2_auto_out_1_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_1_c_bits_param = l2_auto_out_1_c_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_1_c_bits_source = l2_auto_out_1_c_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_1_c_bits_address = l2_auto_out_1_c_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_1_c_bits_data = l2_auto_out_1_c_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_1_d_ready = l2_auto_out_1_d_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_1_e_valid = l2_auto_out_1_e_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_1_e_bits_sink = l2_auto_out_1_e_bits_sink; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_0_a_valid = l2_auto_out_0_a_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_0_a_bits_opcode = l2_auto_out_0_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_0_a_bits_param = l2_auto_out_0_a_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_0_a_bits_source = l2_auto_out_0_a_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_0_a_bits_address = l2_auto_out_0_a_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_0_c_valid = l2_auto_out_0_c_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_0_c_bits_opcode = l2_auto_out_0_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_0_c_bits_param = l2_auto_out_0_c_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_0_c_bits_source = l2_auto_out_0_c_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_0_c_bits_address = l2_auto_out_0_c_bits_address; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_0_c_bits_data = l2_auto_out_0_c_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_0_d_ready = l2_auto_out_0_d_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_0_e_valid = l2_auto_out_0_e_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_in_0_e_bits_sink = l2_auto_out_0_e_bits_sink; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_a_ready = cork_auto_in_3_a_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_c_ready = cork_auto_in_3_c_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_d_valid = cork_auto_in_3_d_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_d_bits_opcode = cork_auto_in_3_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_d_bits_param = cork_auto_in_3_d_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_d_bits_size = cork_auto_in_3_d_bits_size; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_d_bits_source = cork_auto_in_3_d_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_d_bits_sink = cork_auto_in_3_d_bits_sink; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_d_bits_denied = cork_auto_in_3_d_bits_denied; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_d_bits_data = cork_auto_in_3_d_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_3_d_bits_corrupt = cork_auto_in_3_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_a_ready = cork_auto_in_2_a_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_c_ready = cork_auto_in_2_c_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_d_valid = cork_auto_in_2_d_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_d_bits_opcode = cork_auto_in_2_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_d_bits_param = cork_auto_in_2_d_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_d_bits_size = cork_auto_in_2_d_bits_size; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_d_bits_source = cork_auto_in_2_d_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_d_bits_sink = cork_auto_in_2_d_bits_sink; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_d_bits_denied = cork_auto_in_2_d_bits_denied; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_d_bits_data = cork_auto_in_2_d_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_2_d_bits_corrupt = cork_auto_in_2_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_a_ready = cork_auto_in_1_a_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_c_ready = cork_auto_in_1_c_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_d_valid = cork_auto_in_1_d_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_d_bits_opcode = cork_auto_in_1_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_d_bits_param = cork_auto_in_1_d_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_d_bits_size = cork_auto_in_1_d_bits_size; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_d_bits_source = cork_auto_in_1_d_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_d_bits_sink = cork_auto_in_1_d_bits_sink; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_d_bits_denied = cork_auto_in_1_d_bits_denied; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_d_bits_data = cork_auto_in_1_d_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_1_d_bits_corrupt = cork_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_a_ready = cork_auto_in_0_a_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_c_ready = cork_auto_in_0_c_ready; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_d_valid = cork_auto_in_0_d_valid; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_d_bits_opcode = cork_auto_in_0_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_d_bits_param = cork_auto_in_0_d_bits_param; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_d_bits_size = cork_auto_in_0_d_bits_size; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_d_bits_source = cork_auto_in_0_d_bits_source; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_d_bits_sink = cork_auto_in_0_d_bits_sink; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_d_bits_denied = cork_auto_in_0_d_bits_denied; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_d_bits_data = cork_auto_in_0_d_bits_data; // @[LazyModule.scala 377:16]
  assign ComposableCache_outer_TLBuffer_auto_out_0_d_bits_corrupt = cork_auto_in_0_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign cork_clock = fixedClockNode_auto_out_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign cork_reset = fixedClockNode_auto_out_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign cork_auto_in_3_a_valid = ComposableCache_outer_TLBuffer_auto_out_3_a_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_in_3_a_bits_opcode = ComposableCache_outer_TLBuffer_auto_out_3_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign cork_auto_in_3_a_bits_param = ComposableCache_outer_TLBuffer_auto_out_3_a_bits_param; // @[LazyModule.scala 377:16]
  assign cork_auto_in_3_a_bits_source = ComposableCache_outer_TLBuffer_auto_out_3_a_bits_source; // @[LazyModule.scala 377:16]
  assign cork_auto_in_3_a_bits_address = ComposableCache_outer_TLBuffer_auto_out_3_a_bits_address; // @[LazyModule.scala 377:16]
  assign cork_auto_in_3_c_valid = ComposableCache_outer_TLBuffer_auto_out_3_c_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_in_3_c_bits_opcode = ComposableCache_outer_TLBuffer_auto_out_3_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign cork_auto_in_3_c_bits_param = ComposableCache_outer_TLBuffer_auto_out_3_c_bits_param; // @[LazyModule.scala 377:16]
  assign cork_auto_in_3_c_bits_source = ComposableCache_outer_TLBuffer_auto_out_3_c_bits_source; // @[LazyModule.scala 377:16]
  assign cork_auto_in_3_c_bits_address = ComposableCache_outer_TLBuffer_auto_out_3_c_bits_address; // @[LazyModule.scala 377:16]
  assign cork_auto_in_3_c_bits_data = ComposableCache_outer_TLBuffer_auto_out_3_c_bits_data; // @[LazyModule.scala 377:16]
  assign cork_auto_in_3_d_ready = ComposableCache_outer_TLBuffer_auto_out_3_d_ready; // @[LazyModule.scala 377:16]
  assign cork_auto_in_3_e_valid = ComposableCache_outer_TLBuffer_auto_out_3_e_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_in_3_e_bits_sink = ComposableCache_outer_TLBuffer_auto_out_3_e_bits_sink; // @[LazyModule.scala 377:16]
  assign cork_auto_in_2_a_valid = ComposableCache_outer_TLBuffer_auto_out_2_a_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_in_2_a_bits_opcode = ComposableCache_outer_TLBuffer_auto_out_2_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign cork_auto_in_2_a_bits_param = ComposableCache_outer_TLBuffer_auto_out_2_a_bits_param; // @[LazyModule.scala 377:16]
  assign cork_auto_in_2_a_bits_source = ComposableCache_outer_TLBuffer_auto_out_2_a_bits_source; // @[LazyModule.scala 377:16]
  assign cork_auto_in_2_a_bits_address = ComposableCache_outer_TLBuffer_auto_out_2_a_bits_address; // @[LazyModule.scala 377:16]
  assign cork_auto_in_2_c_valid = ComposableCache_outer_TLBuffer_auto_out_2_c_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_in_2_c_bits_opcode = ComposableCache_outer_TLBuffer_auto_out_2_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign cork_auto_in_2_c_bits_param = ComposableCache_outer_TLBuffer_auto_out_2_c_bits_param; // @[LazyModule.scala 377:16]
  assign cork_auto_in_2_c_bits_source = ComposableCache_outer_TLBuffer_auto_out_2_c_bits_source; // @[LazyModule.scala 377:16]
  assign cork_auto_in_2_c_bits_address = ComposableCache_outer_TLBuffer_auto_out_2_c_bits_address; // @[LazyModule.scala 377:16]
  assign cork_auto_in_2_c_bits_data = ComposableCache_outer_TLBuffer_auto_out_2_c_bits_data; // @[LazyModule.scala 377:16]
  assign cork_auto_in_2_d_ready = ComposableCache_outer_TLBuffer_auto_out_2_d_ready; // @[LazyModule.scala 377:16]
  assign cork_auto_in_2_e_valid = ComposableCache_outer_TLBuffer_auto_out_2_e_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_in_2_e_bits_sink = ComposableCache_outer_TLBuffer_auto_out_2_e_bits_sink; // @[LazyModule.scala 377:16]
  assign cork_auto_in_1_a_valid = ComposableCache_outer_TLBuffer_auto_out_1_a_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_in_1_a_bits_opcode = ComposableCache_outer_TLBuffer_auto_out_1_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign cork_auto_in_1_a_bits_param = ComposableCache_outer_TLBuffer_auto_out_1_a_bits_param; // @[LazyModule.scala 377:16]
  assign cork_auto_in_1_a_bits_source = ComposableCache_outer_TLBuffer_auto_out_1_a_bits_source; // @[LazyModule.scala 377:16]
  assign cork_auto_in_1_a_bits_address = ComposableCache_outer_TLBuffer_auto_out_1_a_bits_address; // @[LazyModule.scala 377:16]
  assign cork_auto_in_1_c_valid = ComposableCache_outer_TLBuffer_auto_out_1_c_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_in_1_c_bits_opcode = ComposableCache_outer_TLBuffer_auto_out_1_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign cork_auto_in_1_c_bits_param = ComposableCache_outer_TLBuffer_auto_out_1_c_bits_param; // @[LazyModule.scala 377:16]
  assign cork_auto_in_1_c_bits_source = ComposableCache_outer_TLBuffer_auto_out_1_c_bits_source; // @[LazyModule.scala 377:16]
  assign cork_auto_in_1_c_bits_address = ComposableCache_outer_TLBuffer_auto_out_1_c_bits_address; // @[LazyModule.scala 377:16]
  assign cork_auto_in_1_c_bits_data = ComposableCache_outer_TLBuffer_auto_out_1_c_bits_data; // @[LazyModule.scala 377:16]
  assign cork_auto_in_1_d_ready = ComposableCache_outer_TLBuffer_auto_out_1_d_ready; // @[LazyModule.scala 377:16]
  assign cork_auto_in_1_e_valid = ComposableCache_outer_TLBuffer_auto_out_1_e_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_in_1_e_bits_sink = ComposableCache_outer_TLBuffer_auto_out_1_e_bits_sink; // @[LazyModule.scala 377:16]
  assign cork_auto_in_0_a_valid = ComposableCache_outer_TLBuffer_auto_out_0_a_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_in_0_a_bits_opcode = ComposableCache_outer_TLBuffer_auto_out_0_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign cork_auto_in_0_a_bits_param = ComposableCache_outer_TLBuffer_auto_out_0_a_bits_param; // @[LazyModule.scala 377:16]
  assign cork_auto_in_0_a_bits_source = ComposableCache_outer_TLBuffer_auto_out_0_a_bits_source; // @[LazyModule.scala 377:16]
  assign cork_auto_in_0_a_bits_address = ComposableCache_outer_TLBuffer_auto_out_0_a_bits_address; // @[LazyModule.scala 377:16]
  assign cork_auto_in_0_c_valid = ComposableCache_outer_TLBuffer_auto_out_0_c_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_in_0_c_bits_opcode = ComposableCache_outer_TLBuffer_auto_out_0_c_bits_opcode; // @[LazyModule.scala 377:16]
  assign cork_auto_in_0_c_bits_param = ComposableCache_outer_TLBuffer_auto_out_0_c_bits_param; // @[LazyModule.scala 377:16]
  assign cork_auto_in_0_c_bits_source = ComposableCache_outer_TLBuffer_auto_out_0_c_bits_source; // @[LazyModule.scala 377:16]
  assign cork_auto_in_0_c_bits_address = ComposableCache_outer_TLBuffer_auto_out_0_c_bits_address; // @[LazyModule.scala 377:16]
  assign cork_auto_in_0_c_bits_data = ComposableCache_outer_TLBuffer_auto_out_0_c_bits_data; // @[LazyModule.scala 377:16]
  assign cork_auto_in_0_d_ready = ComposableCache_outer_TLBuffer_auto_out_0_d_ready; // @[LazyModule.scala 377:16]
  assign cork_auto_in_0_e_valid = ComposableCache_outer_TLBuffer_auto_out_0_e_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_in_0_e_bits_sink = ComposableCache_outer_TLBuffer_auto_out_0_e_bits_sink; // @[LazyModule.scala 377:16]
  assign cork_auto_out_3_a_ready = binder_auto_in_3_a_ready; // @[LazyModule.scala 377:16]
  assign cork_auto_out_3_d_valid = binder_auto_in_3_d_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_out_3_d_bits_opcode = binder_auto_in_3_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign cork_auto_out_3_d_bits_size = binder_auto_in_3_d_bits_size; // @[LazyModule.scala 377:16]
  assign cork_auto_out_3_d_bits_source = binder_auto_in_3_d_bits_source; // @[LazyModule.scala 377:16]
  assign cork_auto_out_3_d_bits_denied = binder_auto_in_3_d_bits_denied; // @[LazyModule.scala 377:16]
  assign cork_auto_out_3_d_bits_data = binder_auto_in_3_d_bits_data; // @[LazyModule.scala 377:16]
  assign cork_auto_out_3_d_bits_corrupt = binder_auto_in_3_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign cork_auto_out_2_a_ready = binder_auto_in_2_a_ready; // @[LazyModule.scala 377:16]
  assign cork_auto_out_2_d_valid = binder_auto_in_2_d_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_out_2_d_bits_opcode = binder_auto_in_2_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign cork_auto_out_2_d_bits_size = binder_auto_in_2_d_bits_size; // @[LazyModule.scala 377:16]
  assign cork_auto_out_2_d_bits_source = binder_auto_in_2_d_bits_source; // @[LazyModule.scala 377:16]
  assign cork_auto_out_2_d_bits_denied = binder_auto_in_2_d_bits_denied; // @[LazyModule.scala 377:16]
  assign cork_auto_out_2_d_bits_data = binder_auto_in_2_d_bits_data; // @[LazyModule.scala 377:16]
  assign cork_auto_out_2_d_bits_corrupt = binder_auto_in_2_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign cork_auto_out_1_a_ready = binder_auto_in_1_a_ready; // @[LazyModule.scala 377:16]
  assign cork_auto_out_1_d_valid = binder_auto_in_1_d_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_out_1_d_bits_opcode = binder_auto_in_1_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign cork_auto_out_1_d_bits_size = binder_auto_in_1_d_bits_size; // @[LazyModule.scala 377:16]
  assign cork_auto_out_1_d_bits_source = binder_auto_in_1_d_bits_source; // @[LazyModule.scala 377:16]
  assign cork_auto_out_1_d_bits_denied = binder_auto_in_1_d_bits_denied; // @[LazyModule.scala 377:16]
  assign cork_auto_out_1_d_bits_data = binder_auto_in_1_d_bits_data; // @[LazyModule.scala 377:16]
  assign cork_auto_out_1_d_bits_corrupt = binder_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign cork_auto_out_0_a_ready = binder_auto_in_0_a_ready; // @[LazyModule.scala 377:16]
  assign cork_auto_out_0_d_valid = binder_auto_in_0_d_valid; // @[LazyModule.scala 377:16]
  assign cork_auto_out_0_d_bits_opcode = binder_auto_in_0_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign cork_auto_out_0_d_bits_size = binder_auto_in_0_d_bits_size; // @[LazyModule.scala 377:16]
  assign cork_auto_out_0_d_bits_source = binder_auto_in_0_d_bits_source; // @[LazyModule.scala 377:16]
  assign cork_auto_out_0_d_bits_denied = binder_auto_in_0_d_bits_denied; // @[LazyModule.scala 377:16]
  assign cork_auto_out_0_d_bits_data = binder_auto_in_0_d_bits_data; // @[LazyModule.scala 377:16]
  assign cork_auto_out_0_d_bits_corrupt = binder_auto_in_0_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coherent_jbar_auto_in_3_a_valid = buffer_auto_out_3_a_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_a_bits_opcode = buffer_auto_out_3_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_a_bits_param = buffer_auto_out_3_a_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_a_bits_size = buffer_auto_out_3_a_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_a_bits_source = buffer_auto_out_3_a_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_a_bits_address = buffer_auto_out_3_a_bits_address; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_a_bits_mask = buffer_auto_out_3_a_bits_mask; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_a_bits_data = buffer_auto_out_3_a_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_a_bits_corrupt = buffer_auto_out_3_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_b_ready = buffer_auto_out_3_b_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_c_valid = buffer_auto_out_3_c_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_c_bits_opcode = buffer_auto_out_3_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_c_bits_param = buffer_auto_out_3_c_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_c_bits_size = buffer_auto_out_3_c_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_c_bits_source = buffer_auto_out_3_c_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_c_bits_address = buffer_auto_out_3_c_bits_address; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_c_bits_data = buffer_auto_out_3_c_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_c_bits_corrupt = buffer_auto_out_3_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_d_ready = buffer_auto_out_3_d_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_e_valid = buffer_auto_out_3_e_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_3_e_bits_sink = buffer_auto_out_3_e_bits_sink; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_a_valid = buffer_auto_out_2_a_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_a_bits_opcode = buffer_auto_out_2_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_a_bits_param = buffer_auto_out_2_a_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_a_bits_size = buffer_auto_out_2_a_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_a_bits_source = buffer_auto_out_2_a_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_a_bits_address = buffer_auto_out_2_a_bits_address; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_a_bits_mask = buffer_auto_out_2_a_bits_mask; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_a_bits_data = buffer_auto_out_2_a_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_a_bits_corrupt = buffer_auto_out_2_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_b_ready = buffer_auto_out_2_b_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_c_valid = buffer_auto_out_2_c_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_c_bits_opcode = buffer_auto_out_2_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_c_bits_param = buffer_auto_out_2_c_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_c_bits_size = buffer_auto_out_2_c_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_c_bits_source = buffer_auto_out_2_c_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_c_bits_address = buffer_auto_out_2_c_bits_address; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_c_bits_data = buffer_auto_out_2_c_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_c_bits_corrupt = buffer_auto_out_2_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_d_ready = buffer_auto_out_2_d_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_e_valid = buffer_auto_out_2_e_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_2_e_bits_sink = buffer_auto_out_2_e_bits_sink; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_a_valid = buffer_auto_out_1_a_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_a_bits_opcode = buffer_auto_out_1_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_a_bits_param = buffer_auto_out_1_a_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_a_bits_size = buffer_auto_out_1_a_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_a_bits_source = buffer_auto_out_1_a_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_a_bits_address = buffer_auto_out_1_a_bits_address; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_a_bits_mask = buffer_auto_out_1_a_bits_mask; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_a_bits_data = buffer_auto_out_1_a_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_a_bits_corrupt = buffer_auto_out_1_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_b_ready = buffer_auto_out_1_b_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_c_valid = buffer_auto_out_1_c_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_c_bits_opcode = buffer_auto_out_1_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_c_bits_param = buffer_auto_out_1_c_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_c_bits_size = buffer_auto_out_1_c_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_c_bits_source = buffer_auto_out_1_c_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_c_bits_address = buffer_auto_out_1_c_bits_address; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_c_bits_data = buffer_auto_out_1_c_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_c_bits_corrupt = buffer_auto_out_1_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_d_ready = buffer_auto_out_1_d_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_e_valid = buffer_auto_out_1_e_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_1_e_bits_sink = buffer_auto_out_1_e_bits_sink; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_a_valid = buffer_auto_out_0_a_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_a_bits_opcode = buffer_auto_out_0_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_a_bits_param = buffer_auto_out_0_a_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_a_bits_size = buffer_auto_out_0_a_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_a_bits_source = buffer_auto_out_0_a_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_a_bits_address = buffer_auto_out_0_a_bits_address; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_a_bits_mask = buffer_auto_out_0_a_bits_mask; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_a_bits_data = buffer_auto_out_0_a_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_a_bits_corrupt = buffer_auto_out_0_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_b_ready = buffer_auto_out_0_b_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_c_valid = buffer_auto_out_0_c_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_c_bits_opcode = buffer_auto_out_0_c_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_c_bits_param = buffer_auto_out_0_c_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_c_bits_size = buffer_auto_out_0_c_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_c_bits_source = buffer_auto_out_0_c_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_c_bits_address = buffer_auto_out_0_c_bits_address; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_c_bits_data = buffer_auto_out_0_c_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_c_bits_corrupt = buffer_auto_out_0_c_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_d_ready = buffer_auto_out_0_d_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_e_valid = buffer_auto_out_0_e_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_in_0_e_bits_sink = buffer_auto_out_0_e_bits_sink; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_a_ready = filter_auto_in_3_a_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_b_valid = filter_auto_in_3_b_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_b_bits_opcode = filter_auto_in_3_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_b_bits_param = filter_auto_in_3_b_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_b_bits_size = filter_auto_in_3_b_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_b_bits_source = filter_auto_in_3_b_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_b_bits_address = filter_auto_in_3_b_bits_address; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_b_bits_mask = filter_auto_in_3_b_bits_mask; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_b_bits_data = filter_auto_in_3_b_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_b_bits_corrupt = filter_auto_in_3_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_c_ready = filter_auto_in_3_c_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_d_valid = filter_auto_in_3_d_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_d_bits_opcode = filter_auto_in_3_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_d_bits_param = filter_auto_in_3_d_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_d_bits_size = filter_auto_in_3_d_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_d_bits_source = filter_auto_in_3_d_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_d_bits_sink = filter_auto_in_3_d_bits_sink; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_d_bits_denied = filter_auto_in_3_d_bits_denied; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_d_bits_data = filter_auto_in_3_d_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_d_bits_corrupt = filter_auto_in_3_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_3_e_ready = filter_auto_in_3_e_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_a_ready = filter_auto_in_2_a_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_b_valid = filter_auto_in_2_b_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_b_bits_opcode = filter_auto_in_2_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_b_bits_param = filter_auto_in_2_b_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_b_bits_size = filter_auto_in_2_b_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_b_bits_source = filter_auto_in_2_b_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_b_bits_address = filter_auto_in_2_b_bits_address; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_b_bits_mask = filter_auto_in_2_b_bits_mask; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_b_bits_data = filter_auto_in_2_b_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_b_bits_corrupt = filter_auto_in_2_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_c_ready = filter_auto_in_2_c_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_d_valid = filter_auto_in_2_d_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_d_bits_opcode = filter_auto_in_2_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_d_bits_param = filter_auto_in_2_d_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_d_bits_size = filter_auto_in_2_d_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_d_bits_source = filter_auto_in_2_d_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_d_bits_sink = filter_auto_in_2_d_bits_sink; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_d_bits_denied = filter_auto_in_2_d_bits_denied; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_d_bits_data = filter_auto_in_2_d_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_d_bits_corrupt = filter_auto_in_2_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_2_e_ready = filter_auto_in_2_e_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_a_ready = filter_auto_in_1_a_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_b_valid = filter_auto_in_1_b_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_b_bits_opcode = filter_auto_in_1_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_b_bits_param = filter_auto_in_1_b_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_b_bits_size = filter_auto_in_1_b_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_b_bits_source = filter_auto_in_1_b_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_b_bits_address = filter_auto_in_1_b_bits_address; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_b_bits_mask = filter_auto_in_1_b_bits_mask; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_b_bits_data = filter_auto_in_1_b_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_b_bits_corrupt = filter_auto_in_1_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_c_ready = filter_auto_in_1_c_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_d_valid = filter_auto_in_1_d_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_d_bits_opcode = filter_auto_in_1_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_d_bits_param = filter_auto_in_1_d_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_d_bits_size = filter_auto_in_1_d_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_d_bits_source = filter_auto_in_1_d_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_d_bits_sink = filter_auto_in_1_d_bits_sink; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_d_bits_denied = filter_auto_in_1_d_bits_denied; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_d_bits_data = filter_auto_in_1_d_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_d_bits_corrupt = filter_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_1_e_ready = filter_auto_in_1_e_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_a_ready = filter_auto_in_0_a_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_b_valid = filter_auto_in_0_b_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_b_bits_opcode = filter_auto_in_0_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_b_bits_param = filter_auto_in_0_b_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_b_bits_size = filter_auto_in_0_b_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_b_bits_source = filter_auto_in_0_b_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_b_bits_address = filter_auto_in_0_b_bits_address; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_b_bits_mask = filter_auto_in_0_b_bits_mask; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_b_bits_data = filter_auto_in_0_b_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_b_bits_corrupt = filter_auto_in_0_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_c_ready = filter_auto_in_0_c_ready; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_d_valid = filter_auto_in_0_d_valid; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_d_bits_opcode = filter_auto_in_0_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_d_bits_param = filter_auto_in_0_d_bits_param; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_d_bits_size = filter_auto_in_0_d_bits_size; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_d_bits_source = filter_auto_in_0_d_bits_source; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_d_bits_sink = filter_auto_in_0_d_bits_sink; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_d_bits_denied = filter_auto_in_0_d_bits_denied; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_d_bits_data = filter_auto_in_0_d_bits_data; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_d_bits_corrupt = filter_auto_in_0_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coherent_jbar_auto_out_0_e_ready = filter_auto_in_0_e_ready; // @[LazyModule.scala 375:16]
  assign binder_clock = fixedClockNode_auto_out_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign binder_reset = fixedClockNode_auto_out_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign binder_auto_in_3_a_valid = cork_auto_out_3_a_valid; // @[LazyModule.scala 377:16]
  assign binder_auto_in_3_a_bits_opcode = cork_auto_out_3_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign binder_auto_in_3_a_bits_param = cork_auto_out_3_a_bits_param; // @[LazyModule.scala 377:16]
  assign binder_auto_in_3_a_bits_size = cork_auto_out_3_a_bits_size; // @[LazyModule.scala 377:16]
  assign binder_auto_in_3_a_bits_source = cork_auto_out_3_a_bits_source; // @[LazyModule.scala 377:16]
  assign binder_auto_in_3_a_bits_address = cork_auto_out_3_a_bits_address; // @[LazyModule.scala 377:16]
  assign binder_auto_in_3_a_bits_user_amba_prot_bufferable = cork_auto_out_3_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign binder_auto_in_3_a_bits_user_amba_prot_modifiable = cork_auto_out_3_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign binder_auto_in_3_a_bits_user_amba_prot_readalloc = cork_auto_out_3_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign binder_auto_in_3_a_bits_user_amba_prot_writealloc = cork_auto_out_3_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign binder_auto_in_3_a_bits_user_amba_prot_privileged = cork_auto_out_3_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign binder_auto_in_3_a_bits_user_amba_prot_secure = cork_auto_out_3_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign binder_auto_in_3_a_bits_mask = cork_auto_out_3_a_bits_mask; // @[LazyModule.scala 377:16]
  assign binder_auto_in_3_a_bits_data = cork_auto_out_3_a_bits_data; // @[LazyModule.scala 377:16]
  assign binder_auto_in_3_d_ready = cork_auto_out_3_d_ready; // @[LazyModule.scala 377:16]
  assign binder_auto_in_2_a_valid = cork_auto_out_2_a_valid; // @[LazyModule.scala 377:16]
  assign binder_auto_in_2_a_bits_opcode = cork_auto_out_2_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign binder_auto_in_2_a_bits_param = cork_auto_out_2_a_bits_param; // @[LazyModule.scala 377:16]
  assign binder_auto_in_2_a_bits_size = cork_auto_out_2_a_bits_size; // @[LazyModule.scala 377:16]
  assign binder_auto_in_2_a_bits_source = cork_auto_out_2_a_bits_source; // @[LazyModule.scala 377:16]
  assign binder_auto_in_2_a_bits_address = cork_auto_out_2_a_bits_address; // @[LazyModule.scala 377:16]
  assign binder_auto_in_2_a_bits_user_amba_prot_bufferable = cork_auto_out_2_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign binder_auto_in_2_a_bits_user_amba_prot_modifiable = cork_auto_out_2_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign binder_auto_in_2_a_bits_user_amba_prot_readalloc = cork_auto_out_2_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign binder_auto_in_2_a_bits_user_amba_prot_writealloc = cork_auto_out_2_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign binder_auto_in_2_a_bits_user_amba_prot_privileged = cork_auto_out_2_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign binder_auto_in_2_a_bits_user_amba_prot_secure = cork_auto_out_2_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign binder_auto_in_2_a_bits_mask = cork_auto_out_2_a_bits_mask; // @[LazyModule.scala 377:16]
  assign binder_auto_in_2_a_bits_data = cork_auto_out_2_a_bits_data; // @[LazyModule.scala 377:16]
  assign binder_auto_in_2_d_ready = cork_auto_out_2_d_ready; // @[LazyModule.scala 377:16]
  assign binder_auto_in_1_a_valid = cork_auto_out_1_a_valid; // @[LazyModule.scala 377:16]
  assign binder_auto_in_1_a_bits_opcode = cork_auto_out_1_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign binder_auto_in_1_a_bits_param = cork_auto_out_1_a_bits_param; // @[LazyModule.scala 377:16]
  assign binder_auto_in_1_a_bits_size = cork_auto_out_1_a_bits_size; // @[LazyModule.scala 377:16]
  assign binder_auto_in_1_a_bits_source = cork_auto_out_1_a_bits_source; // @[LazyModule.scala 377:16]
  assign binder_auto_in_1_a_bits_address = cork_auto_out_1_a_bits_address; // @[LazyModule.scala 377:16]
  assign binder_auto_in_1_a_bits_user_amba_prot_bufferable = cork_auto_out_1_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign binder_auto_in_1_a_bits_user_amba_prot_modifiable = cork_auto_out_1_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign binder_auto_in_1_a_bits_user_amba_prot_readalloc = cork_auto_out_1_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign binder_auto_in_1_a_bits_user_amba_prot_writealloc = cork_auto_out_1_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign binder_auto_in_1_a_bits_user_amba_prot_privileged = cork_auto_out_1_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign binder_auto_in_1_a_bits_user_amba_prot_secure = cork_auto_out_1_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign binder_auto_in_1_a_bits_mask = cork_auto_out_1_a_bits_mask; // @[LazyModule.scala 377:16]
  assign binder_auto_in_1_a_bits_data = cork_auto_out_1_a_bits_data; // @[LazyModule.scala 377:16]
  assign binder_auto_in_1_d_ready = cork_auto_out_1_d_ready; // @[LazyModule.scala 377:16]
  assign binder_auto_in_0_a_valid = cork_auto_out_0_a_valid; // @[LazyModule.scala 377:16]
  assign binder_auto_in_0_a_bits_opcode = cork_auto_out_0_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign binder_auto_in_0_a_bits_param = cork_auto_out_0_a_bits_param; // @[LazyModule.scala 377:16]
  assign binder_auto_in_0_a_bits_size = cork_auto_out_0_a_bits_size; // @[LazyModule.scala 377:16]
  assign binder_auto_in_0_a_bits_source = cork_auto_out_0_a_bits_source; // @[LazyModule.scala 377:16]
  assign binder_auto_in_0_a_bits_address = cork_auto_out_0_a_bits_address; // @[LazyModule.scala 377:16]
  assign binder_auto_in_0_a_bits_user_amba_prot_bufferable = cork_auto_out_0_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign binder_auto_in_0_a_bits_user_amba_prot_modifiable = cork_auto_out_0_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign binder_auto_in_0_a_bits_user_amba_prot_readalloc = cork_auto_out_0_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign binder_auto_in_0_a_bits_user_amba_prot_writealloc = cork_auto_out_0_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign binder_auto_in_0_a_bits_user_amba_prot_privileged = cork_auto_out_0_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign binder_auto_in_0_a_bits_user_amba_prot_secure = cork_auto_out_0_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign binder_auto_in_0_a_bits_mask = cork_auto_out_0_a_bits_mask; // @[LazyModule.scala 377:16]
  assign binder_auto_in_0_a_bits_data = cork_auto_out_0_a_bits_data; // @[LazyModule.scala 377:16]
  assign binder_auto_in_0_d_ready = cork_auto_out_0_d_ready; // @[LazyModule.scala 377:16]
  assign binder_auto_out_3_a_ready = coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_ready; // @[LazyModule.scala 377:16]
  assign binder_auto_out_3_d_valid = coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_valid; // @[LazyModule.scala 377:16]
  assign binder_auto_out_3_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign binder_auto_out_3_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_size; // @[LazyModule.scala 377:16]
  assign binder_auto_out_3_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_source; // @[LazyModule.scala 377:16]
  assign binder_auto_out_3_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_denied; // @[LazyModule.scala 377:16]
  assign binder_auto_out_3_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_data; // @[LazyModule.scala 377:16]
  assign binder_auto_out_3_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign binder_auto_out_2_a_ready = coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_ready; // @[LazyModule.scala 377:16]
  assign binder_auto_out_2_d_valid = coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_valid; // @[LazyModule.scala 377:16]
  assign binder_auto_out_2_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign binder_auto_out_2_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_size; // @[LazyModule.scala 377:16]
  assign binder_auto_out_2_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_source; // @[LazyModule.scala 377:16]
  assign binder_auto_out_2_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_denied; // @[LazyModule.scala 377:16]
  assign binder_auto_out_2_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_data; // @[LazyModule.scala 377:16]
  assign binder_auto_out_2_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign binder_auto_out_1_a_ready = coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_ready; // @[LazyModule.scala 377:16]
  assign binder_auto_out_1_d_valid = coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_valid; // @[LazyModule.scala 377:16]
  assign binder_auto_out_1_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign binder_auto_out_1_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_size; // @[LazyModule.scala 377:16]
  assign binder_auto_out_1_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_source; // @[LazyModule.scala 377:16]
  assign binder_auto_out_1_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_denied; // @[LazyModule.scala 377:16]
  assign binder_auto_out_1_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_data; // @[LazyModule.scala 377:16]
  assign binder_auto_out_1_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign binder_auto_out_0_a_ready = coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_ready; // @[LazyModule.scala 377:16]
  assign binder_auto_out_0_d_valid = coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_valid; // @[LazyModule.scala 377:16]
  assign binder_auto_out_0_d_bits_opcode =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign binder_auto_out_0_d_bits_size =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_size; // @[LazyModule.scala 377:16]
  assign binder_auto_out_0_d_bits_source =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_source; // @[LazyModule.scala 377:16]
  assign binder_auto_out_0_d_bits_denied =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_denied; // @[LazyModule.scala 377:16]
  assign binder_auto_out_0_d_bits_data =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_data; // @[LazyModule.scala 377:16]
  assign binder_auto_out_0_d_bits_corrupt =
    coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_3_a_valid = auto_bus_xing_in_3_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_opcode = auto_bus_xing_in_3_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_param = auto_bus_xing_in_3_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_size = auto_bus_xing_in_3_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_source = auto_bus_xing_in_3_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_address = auto_bus_xing_in_3_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_mask = auto_bus_xing_in_3_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_data = auto_bus_xing_in_3_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_corrupt = auto_bus_xing_in_3_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_b_ready = auto_bus_xing_in_3_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_c_valid = auto_bus_xing_in_3_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_c_bits_opcode = auto_bus_xing_in_3_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_c_bits_param = auto_bus_xing_in_3_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_c_bits_size = auto_bus_xing_in_3_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_c_bits_source = auto_bus_xing_in_3_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_c_bits_address = auto_bus_xing_in_3_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_c_bits_data = auto_bus_xing_in_3_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_c_bits_corrupt = auto_bus_xing_in_3_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_d_ready = auto_bus_xing_in_3_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_e_valid = auto_bus_xing_in_3_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_e_bits_sink = auto_bus_xing_in_3_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_valid = auto_bus_xing_in_2_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_opcode = auto_bus_xing_in_2_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_param = auto_bus_xing_in_2_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_size = auto_bus_xing_in_2_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_source = auto_bus_xing_in_2_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_address = auto_bus_xing_in_2_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_mask = auto_bus_xing_in_2_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_data = auto_bus_xing_in_2_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_corrupt = auto_bus_xing_in_2_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_b_ready = auto_bus_xing_in_2_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_c_valid = auto_bus_xing_in_2_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_c_bits_opcode = auto_bus_xing_in_2_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_c_bits_param = auto_bus_xing_in_2_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_c_bits_size = auto_bus_xing_in_2_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_c_bits_source = auto_bus_xing_in_2_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_c_bits_address = auto_bus_xing_in_2_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_c_bits_data = auto_bus_xing_in_2_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_c_bits_corrupt = auto_bus_xing_in_2_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_d_ready = auto_bus_xing_in_2_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_e_valid = auto_bus_xing_in_2_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_e_bits_sink = auto_bus_xing_in_2_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_valid = auto_bus_xing_in_1_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_opcode = auto_bus_xing_in_1_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_param = auto_bus_xing_in_1_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_size = auto_bus_xing_in_1_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_source = auto_bus_xing_in_1_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_address = auto_bus_xing_in_1_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_mask = auto_bus_xing_in_1_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_data = auto_bus_xing_in_1_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_corrupt = auto_bus_xing_in_1_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_b_ready = auto_bus_xing_in_1_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_c_valid = auto_bus_xing_in_1_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_c_bits_opcode = auto_bus_xing_in_1_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_c_bits_param = auto_bus_xing_in_1_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_c_bits_size = auto_bus_xing_in_1_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_c_bits_source = auto_bus_xing_in_1_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_c_bits_address = auto_bus_xing_in_1_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_c_bits_data = auto_bus_xing_in_1_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_c_bits_corrupt = auto_bus_xing_in_1_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_d_ready = auto_bus_xing_in_1_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_e_valid = auto_bus_xing_in_1_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_e_bits_sink = auto_bus_xing_in_1_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_valid = auto_bus_xing_in_0_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_opcode = auto_bus_xing_in_0_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_param = auto_bus_xing_in_0_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_size = auto_bus_xing_in_0_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_source = auto_bus_xing_in_0_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_address = auto_bus_xing_in_0_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_mask = auto_bus_xing_in_0_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_data = auto_bus_xing_in_0_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_corrupt = auto_bus_xing_in_0_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_b_ready = auto_bus_xing_in_0_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_c_valid = auto_bus_xing_in_0_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_c_bits_opcode = auto_bus_xing_in_0_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_c_bits_param = auto_bus_xing_in_0_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_c_bits_size = auto_bus_xing_in_0_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_c_bits_source = auto_bus_xing_in_0_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_c_bits_address = auto_bus_xing_in_0_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_c_bits_data = auto_bus_xing_in_0_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_c_bits_corrupt = auto_bus_xing_in_0_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_d_ready = auto_bus_xing_in_0_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_e_valid = auto_bus_xing_in_0_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_e_bits_sink = auto_bus_xing_in_0_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_ready = coherent_jbar_auto_in_3_a_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_b_valid = coherent_jbar_auto_in_3_b_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_b_bits_opcode = coherent_jbar_auto_in_3_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_b_bits_param = coherent_jbar_auto_in_3_b_bits_param; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_b_bits_size = coherent_jbar_auto_in_3_b_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_b_bits_source = coherent_jbar_auto_in_3_b_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_b_bits_address = coherent_jbar_auto_in_3_b_bits_address; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_b_bits_mask = coherent_jbar_auto_in_3_b_bits_mask; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_b_bits_data = coherent_jbar_auto_in_3_b_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_b_bits_corrupt = coherent_jbar_auto_in_3_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_c_ready = coherent_jbar_auto_in_3_c_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_valid = coherent_jbar_auto_in_3_d_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_bits_opcode = coherent_jbar_auto_in_3_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_bits_param = coherent_jbar_auto_in_3_d_bits_param; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_bits_size = coherent_jbar_auto_in_3_d_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_bits_source = coherent_jbar_auto_in_3_d_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_bits_sink = coherent_jbar_auto_in_3_d_bits_sink; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_bits_denied = coherent_jbar_auto_in_3_d_bits_denied; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_bits_data = coherent_jbar_auto_in_3_d_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_bits_corrupt = coherent_jbar_auto_in_3_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_e_ready = coherent_jbar_auto_in_3_e_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_a_ready = coherent_jbar_auto_in_2_a_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_b_valid = coherent_jbar_auto_in_2_b_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_b_bits_opcode = coherent_jbar_auto_in_2_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_b_bits_param = coherent_jbar_auto_in_2_b_bits_param; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_b_bits_size = coherent_jbar_auto_in_2_b_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_b_bits_source = coherent_jbar_auto_in_2_b_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_b_bits_address = coherent_jbar_auto_in_2_b_bits_address; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_b_bits_mask = coherent_jbar_auto_in_2_b_bits_mask; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_b_bits_data = coherent_jbar_auto_in_2_b_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_b_bits_corrupt = coherent_jbar_auto_in_2_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_c_ready = coherent_jbar_auto_in_2_c_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_valid = coherent_jbar_auto_in_2_d_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_bits_opcode = coherent_jbar_auto_in_2_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_bits_param = coherent_jbar_auto_in_2_d_bits_param; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_bits_size = coherent_jbar_auto_in_2_d_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_bits_source = coherent_jbar_auto_in_2_d_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_bits_sink = coherent_jbar_auto_in_2_d_bits_sink; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_bits_denied = coherent_jbar_auto_in_2_d_bits_denied; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_bits_data = coherent_jbar_auto_in_2_d_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_bits_corrupt = coherent_jbar_auto_in_2_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_e_ready = coherent_jbar_auto_in_2_e_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_a_ready = coherent_jbar_auto_in_1_a_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_b_valid = coherent_jbar_auto_in_1_b_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_b_bits_opcode = coherent_jbar_auto_in_1_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_b_bits_param = coherent_jbar_auto_in_1_b_bits_param; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_b_bits_size = coherent_jbar_auto_in_1_b_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_b_bits_source = coherent_jbar_auto_in_1_b_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_b_bits_address = coherent_jbar_auto_in_1_b_bits_address; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_b_bits_mask = coherent_jbar_auto_in_1_b_bits_mask; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_b_bits_data = coherent_jbar_auto_in_1_b_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_b_bits_corrupt = coherent_jbar_auto_in_1_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_c_ready = coherent_jbar_auto_in_1_c_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_valid = coherent_jbar_auto_in_1_d_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_bits_opcode = coherent_jbar_auto_in_1_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_bits_param = coherent_jbar_auto_in_1_d_bits_param; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_bits_size = coherent_jbar_auto_in_1_d_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_bits_source = coherent_jbar_auto_in_1_d_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_bits_sink = coherent_jbar_auto_in_1_d_bits_sink; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_bits_denied = coherent_jbar_auto_in_1_d_bits_denied; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_bits_data = coherent_jbar_auto_in_1_d_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_bits_corrupt = coherent_jbar_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_e_ready = coherent_jbar_auto_in_1_e_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_a_ready = coherent_jbar_auto_in_0_a_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_b_valid = coherent_jbar_auto_in_0_b_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_b_bits_opcode = coherent_jbar_auto_in_0_b_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_b_bits_param = coherent_jbar_auto_in_0_b_bits_param; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_b_bits_size = coherent_jbar_auto_in_0_b_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_b_bits_source = coherent_jbar_auto_in_0_b_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_b_bits_address = coherent_jbar_auto_in_0_b_bits_address; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_b_bits_mask = coherent_jbar_auto_in_0_b_bits_mask; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_b_bits_data = coherent_jbar_auto_in_0_b_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_b_bits_corrupt = coherent_jbar_auto_in_0_b_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_c_ready = coherent_jbar_auto_in_0_c_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_valid = coherent_jbar_auto_in_0_d_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_bits_opcode = coherent_jbar_auto_in_0_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_bits_param = coherent_jbar_auto_in_0_d_bits_param; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_bits_size = coherent_jbar_auto_in_0_d_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_bits_source = coherent_jbar_auto_in_0_d_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_bits_sink = coherent_jbar_auto_in_0_d_bits_sink; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_bits_denied = coherent_jbar_auto_in_0_d_bits_denied; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_bits_data = coherent_jbar_auto_in_0_d_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_bits_corrupt = coherent_jbar_auto_in_0_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_e_ready = coherent_jbar_auto_in_0_e_ready; // @[LazyModule.scala 375:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_valid = binder_auto_out_3_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_opcode =
    binder_auto_out_3_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_param =
    binder_auto_out_3_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_size =
    binder_auto_out_3_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_source =
    binder_auto_out_3_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_address =
    binder_auto_out_3_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_bufferable =
    binder_auto_out_3_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_modifiable =
    binder_auto_out_3_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_readalloc =
    binder_auto_out_3_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_writealloc =
    binder_auto_out_3_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_privileged =
    binder_auto_out_3_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_user_amba_prot_secure =
    binder_auto_out_3_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_mask =
    binder_auto_out_3_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_a_bits_data =
    binder_auto_out_3_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_3_d_ready = binder_auto_out_3_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_valid = binder_auto_out_2_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_opcode =
    binder_auto_out_2_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_param =
    binder_auto_out_2_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_size =
    binder_auto_out_2_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_source =
    binder_auto_out_2_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_address =
    binder_auto_out_2_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_bufferable =
    binder_auto_out_2_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_modifiable =
    binder_auto_out_2_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_readalloc =
    binder_auto_out_2_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_writealloc =
    binder_auto_out_2_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_privileged =
    binder_auto_out_2_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_user_amba_prot_secure =
    binder_auto_out_2_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_mask =
    binder_auto_out_2_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_a_bits_data =
    binder_auto_out_2_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_2_d_ready = binder_auto_out_2_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_valid = binder_auto_out_1_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_opcode =
    binder_auto_out_1_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_param =
    binder_auto_out_1_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_size =
    binder_auto_out_1_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_source =
    binder_auto_out_1_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_address =
    binder_auto_out_1_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_bufferable =
    binder_auto_out_1_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_modifiable =
    binder_auto_out_1_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_readalloc =
    binder_auto_out_1_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_writealloc =
    binder_auto_out_1_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_privileged =
    binder_auto_out_1_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_user_amba_prot_secure =
    binder_auto_out_1_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_mask =
    binder_auto_out_1_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_a_bits_data =
    binder_auto_out_1_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_1_d_ready = binder_auto_out_1_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_valid = binder_auto_out_0_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_opcode =
    binder_auto_out_0_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_param =
    binder_auto_out_0_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_size =
    binder_auto_out_0_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_source =
    binder_auto_out_0_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_address =
    binder_auto_out_0_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_bufferable =
    binder_auto_out_0_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_modifiable =
    binder_auto_out_0_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_readalloc =
    binder_auto_out_0_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_writealloc =
    binder_auto_out_0_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_privileged =
    binder_auto_out_0_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_user_amba_prot_secure =
    binder_auto_out_0_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_mask =
    binder_auto_out_0_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_a_bits_data =
    binder_auto_out_0_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_widget_in_0_d_ready = binder_auto_out_0_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_a_ready =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_valid =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_opcode =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_size =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_source =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_denied =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_bits_denied; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_data =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_3_d_bits_corrupt =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_3_d_bits_corrupt; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_a_ready =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_valid =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_opcode =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_size =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_source =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_denied =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_bits_denied; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_data =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_2_d_bits_corrupt =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_2_d_bits_corrupt; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_a_ready =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_valid =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_opcode =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_size =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_source =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_denied =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_bits_denied; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_data =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_1_d_bits_corrupt =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_1_d_bits_corrupt; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_a_ready =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_valid =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_opcode =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_size =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_source =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_denied =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_bits_denied; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_data =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_bus_named_subsystem_mbus_address_adjuster_auto_bus_xing_out_0_d_bits_corrupt =
    auto_coupler_to_bus_named_subsystem_mbus_address_adjuster_bus_xing_out_0_d_bits_corrupt; // @[LazyModule.scala 390:12]
endmodule
