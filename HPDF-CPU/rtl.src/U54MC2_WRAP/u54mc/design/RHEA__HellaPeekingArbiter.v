//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__HellaPeekingArbiter(
  input        clock,
  input        reset,
  output       io_in_0_ready,
  input        io_in_0_valid,
  input  [4:0] io_in_0_bits_length,
  input  [5:0] io_in_0_bits_slices_0_data,
  input  [1:0] io_in_0_bits_slices_0_mseo,
  input  [5:0] io_in_0_bits_slices_1_data,
  input  [1:0] io_in_0_bits_slices_1_mseo,
  input  [5:0] io_in_0_bits_slices_2_data,
  input  [1:0] io_in_0_bits_slices_2_mseo,
  input  [5:0] io_in_0_bits_slices_3_data,
  input  [1:0] io_in_0_bits_slices_3_mseo,
  input  [5:0] io_in_0_bits_slices_4_data,
  input  [1:0] io_in_0_bits_slices_4_mseo,
  input  [5:0] io_in_0_bits_slices_5_data,
  input  [1:0] io_in_0_bits_slices_5_mseo,
  input  [5:0] io_in_0_bits_slices_6_data,
  input  [1:0] io_in_0_bits_slices_6_mseo,
  input  [5:0] io_in_0_bits_slices_7_data,
  input  [1:0] io_in_0_bits_slices_7_mseo,
  input  [5:0] io_in_0_bits_slices_8_data,
  input  [1:0] io_in_0_bits_slices_8_mseo,
  input  [5:0] io_in_0_bits_slices_9_data,
  input  [1:0] io_in_0_bits_slices_9_mseo,
  input  [5:0] io_in_0_bits_slices_10_data,
  input  [1:0] io_in_0_bits_slices_10_mseo,
  input  [5:0] io_in_0_bits_slices_11_data,
  input  [1:0] io_in_0_bits_slices_11_mseo,
  input  [5:0] io_in_0_bits_slices_12_data,
  input  [1:0] io_in_0_bits_slices_12_mseo,
  input  [5:0] io_in_0_bits_slices_13_data,
  input  [1:0] io_in_0_bits_slices_13_mseo,
  input  [5:0] io_in_0_bits_slices_14_data,
  input  [1:0] io_in_0_bits_slices_14_mseo,
  input  [5:0] io_in_0_bits_slices_15_data,
  input  [1:0] io_in_0_bits_slices_15_mseo,
  input  [5:0] io_in_0_bits_slices_16_data,
  input  [1:0] io_in_0_bits_slices_16_mseo,
  input  [5:0] io_in_0_bits_slices_17_data,
  input  [1:0] io_in_0_bits_slices_17_mseo,
  input  [5:0] io_in_0_bits_slices_18_data,
  input  [1:0] io_in_0_bits_slices_18_mseo,
  input  [5:0] io_in_0_bits_slices_19_data,
  input  [1:0] io_in_0_bits_slices_19_mseo,
  input  [5:0] io_in_0_bits_slices_20_data,
  input  [1:0] io_in_0_bits_slices_20_mseo,
  input  [5:0] io_in_0_bits_slices_21_data,
  input  [1:0] io_in_0_bits_slices_21_mseo,
  input  [5:0] io_in_0_bits_slices_22_data,
  input  [1:0] io_in_0_bits_slices_22_mseo,
  input  [5:0] io_in_0_bits_slices_23_data,
  input  [1:0] io_in_0_bits_slices_23_mseo,
  input  [5:0] io_in_0_bits_slices_24_data,
  input  [1:0] io_in_0_bits_slices_24_mseo,
  output       io_in_1_ready,
  input        io_in_1_valid,
  input  [4:0] io_in_1_bits_length,
  input  [5:0] io_in_1_bits_slices_0_data,
  input  [1:0] io_in_1_bits_slices_0_mseo,
  input  [5:0] io_in_1_bits_slices_1_data,
  input  [1:0] io_in_1_bits_slices_1_mseo,
  input  [5:0] io_in_1_bits_slices_2_data,
  input  [1:0] io_in_1_bits_slices_2_mseo,
  input  [5:0] io_in_1_bits_slices_3_data,
  input  [1:0] io_in_1_bits_slices_3_mseo,
  input  [5:0] io_in_1_bits_slices_4_data,
  input  [1:0] io_in_1_bits_slices_4_mseo,
  input  [5:0] io_in_1_bits_slices_5_data,
  input  [1:0] io_in_1_bits_slices_5_mseo,
  input  [5:0] io_in_1_bits_slices_6_data,
  input  [1:0] io_in_1_bits_slices_6_mseo,
  input  [5:0] io_in_1_bits_slices_7_data,
  input  [1:0] io_in_1_bits_slices_7_mseo,
  input  [5:0] io_in_1_bits_slices_8_data,
  input  [1:0] io_in_1_bits_slices_8_mseo,
  input  [5:0] io_in_1_bits_slices_9_data,
  input  [1:0] io_in_1_bits_slices_9_mseo,
  input  [5:0] io_in_1_bits_slices_10_data,
  input  [1:0] io_in_1_bits_slices_10_mseo,
  input  [5:0] io_in_1_bits_slices_11_data,
  input  [1:0] io_in_1_bits_slices_11_mseo,
  input  [5:0] io_in_1_bits_slices_12_data,
  input  [1:0] io_in_1_bits_slices_12_mseo,
  input  [5:0] io_in_1_bits_slices_13_data,
  input  [1:0] io_in_1_bits_slices_13_mseo,
  input  [5:0] io_in_1_bits_slices_14_data,
  input  [1:0] io_in_1_bits_slices_14_mseo,
  input  [5:0] io_in_1_bits_slices_15_data,
  input  [1:0] io_in_1_bits_slices_15_mseo,
  input  [5:0] io_in_1_bits_slices_16_data,
  input  [1:0] io_in_1_bits_slices_16_mseo,
  input  [5:0] io_in_1_bits_slices_17_data,
  input  [1:0] io_in_1_bits_slices_17_mseo,
  input  [5:0] io_in_1_bits_slices_18_data,
  input  [1:0] io_in_1_bits_slices_18_mseo,
  input  [5:0] io_in_1_bits_slices_19_data,
  input  [1:0] io_in_1_bits_slices_19_mseo,
  input  [5:0] io_in_1_bits_slices_20_data,
  input  [1:0] io_in_1_bits_slices_20_mseo,
  input  [5:0] io_in_1_bits_slices_21_data,
  input  [1:0] io_in_1_bits_slices_21_mseo,
  input  [5:0] io_in_1_bits_slices_22_data,
  input  [1:0] io_in_1_bits_slices_22_mseo,
  input  [5:0] io_in_1_bits_slices_23_data,
  input  [1:0] io_in_1_bits_slices_23_mseo,
  input  [5:0] io_in_1_bits_slices_24_data,
  input  [1:0] io_in_1_bits_slices_24_mseo,
  input        io_out_ready,
  output       io_out_valid,
  output [4:0] io_out_bits_length,
  output [5:0] io_out_bits_slices_0_data,
  output [1:0] io_out_bits_slices_0_mseo,
  output [5:0] io_out_bits_slices_1_data,
  output [1:0] io_out_bits_slices_1_mseo,
  output [5:0] io_out_bits_slices_2_data,
  output [1:0] io_out_bits_slices_2_mseo,
  output [5:0] io_out_bits_slices_3_data,
  output [1:0] io_out_bits_slices_3_mseo,
  output [5:0] io_out_bits_slices_4_data,
  output [1:0] io_out_bits_slices_4_mseo,
  output [5:0] io_out_bits_slices_5_data,
  output [1:0] io_out_bits_slices_5_mseo,
  output [5:0] io_out_bits_slices_6_data,
  output [1:0] io_out_bits_slices_6_mseo,
  output [5:0] io_out_bits_slices_7_data,
  output [1:0] io_out_bits_slices_7_mseo,
  output [5:0] io_out_bits_slices_8_data,
  output [1:0] io_out_bits_slices_8_mseo,
  output [5:0] io_out_bits_slices_9_data,
  output [1:0] io_out_bits_slices_9_mseo,
  output [5:0] io_out_bits_slices_10_data,
  output [1:0] io_out_bits_slices_10_mseo,
  output [5:0] io_out_bits_slices_11_data,
  output [1:0] io_out_bits_slices_11_mseo,
  output [5:0] io_out_bits_slices_12_data,
  output [1:0] io_out_bits_slices_12_mseo,
  output [5:0] io_out_bits_slices_13_data,
  output [1:0] io_out_bits_slices_13_mseo,
  output [5:0] io_out_bits_slices_14_data,
  output [1:0] io_out_bits_slices_14_mseo,
  output [5:0] io_out_bits_slices_15_data,
  output [1:0] io_out_bits_slices_15_mseo,
  output [5:0] io_out_bits_slices_16_data,
  output [1:0] io_out_bits_slices_16_mseo,
  output [5:0] io_out_bits_slices_17_data,
  output [1:0] io_out_bits_slices_17_mseo,
  output [5:0] io_out_bits_slices_18_data,
  output [1:0] io_out_bits_slices_18_mseo,
  output [5:0] io_out_bits_slices_19_data,
  output [1:0] io_out_bits_slices_19_mseo,
  output [5:0] io_out_bits_slices_20_data,
  output [1:0] io_out_bits_slices_20_mseo,
  output [5:0] io_out_bits_slices_21_data,
  output [1:0] io_out_bits_slices_21_mseo,
  output [5:0] io_out_bits_slices_22_data,
  output [1:0] io_out_bits_slices_22_mseo,
  output [5:0] io_out_bits_slices_23_data,
  output [1:0] io_out_bits_slices_23_mseo,
  output [5:0] io_out_bits_slices_24_data,
  output [1:0] io_out_bits_slices_24_mseo
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
`endif // RANDOMIZE_REG_INIT
  reg  lockIdx; // @[Arbiters.scala 26:24]
  wire  _choice_T_1 = lockIdx + 1'h1; // @[Arbiters.scala 31:55]
  wire [1:0] _choice_T_3 = {{1'd0}, _choice_T_1}; // @[Arbiters.scala 22:37]
  wire  _GEN_1 = _choice_T_3[0] ? io_in_1_valid : io_in_0_valid; // @[Arbiters.scala 22:10 Arbiters.scala 22:10]
  wire  _choice_T_29 = _choice_T_1 < 1'h1 ? 1'h1 + _choice_T_1 : _choice_T_1 - 1'h1; // @[Arbiters.scala 22:10]
  wire  choice = _GEN_1 ? _choice_T_3[0] : _choice_T_29; // @[Mux.scala 47:69]
  wire  _T = io_out_ready & io_out_valid; // @[Decoupled.scala 40:37]
  assign io_in_0_ready = io_out_ready & ~choice; // @[Arbiters.scala 40:36]
  assign io_in_1_ready = io_out_ready & choice; // @[Arbiters.scala 40:36]
  assign io_out_valid = choice ? io_in_1_valid : io_in_0_valid; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_length = choice ? io_in_1_bits_length : io_in_0_bits_length; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_0_data = choice ? io_in_1_bits_slices_0_data : io_in_0_bits_slices_0_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_0_mseo = choice ? io_in_1_bits_slices_0_mseo : io_in_0_bits_slices_0_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_1_data = choice ? io_in_1_bits_slices_1_data : io_in_0_bits_slices_1_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_1_mseo = choice ? io_in_1_bits_slices_1_mseo : io_in_0_bits_slices_1_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_2_data = choice ? io_in_1_bits_slices_2_data : io_in_0_bits_slices_2_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_2_mseo = choice ? io_in_1_bits_slices_2_mseo : io_in_0_bits_slices_2_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_3_data = choice ? io_in_1_bits_slices_3_data : io_in_0_bits_slices_3_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_3_mseo = choice ? io_in_1_bits_slices_3_mseo : io_in_0_bits_slices_3_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_4_data = choice ? io_in_1_bits_slices_4_data : io_in_0_bits_slices_4_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_4_mseo = choice ? io_in_1_bits_slices_4_mseo : io_in_0_bits_slices_4_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_5_data = choice ? io_in_1_bits_slices_5_data : io_in_0_bits_slices_5_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_5_mseo = choice ? io_in_1_bits_slices_5_mseo : io_in_0_bits_slices_5_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_6_data = choice ? io_in_1_bits_slices_6_data : io_in_0_bits_slices_6_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_6_mseo = choice ? io_in_1_bits_slices_6_mseo : io_in_0_bits_slices_6_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_7_data = choice ? io_in_1_bits_slices_7_data : io_in_0_bits_slices_7_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_7_mseo = choice ? io_in_1_bits_slices_7_mseo : io_in_0_bits_slices_7_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_8_data = choice ? io_in_1_bits_slices_8_data : io_in_0_bits_slices_8_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_8_mseo = choice ? io_in_1_bits_slices_8_mseo : io_in_0_bits_slices_8_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_9_data = choice ? io_in_1_bits_slices_9_data : io_in_0_bits_slices_9_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_9_mseo = choice ? io_in_1_bits_slices_9_mseo : io_in_0_bits_slices_9_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_10_data = choice ? io_in_1_bits_slices_10_data : io_in_0_bits_slices_10_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_10_mseo = choice ? io_in_1_bits_slices_10_mseo : io_in_0_bits_slices_10_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_11_data = choice ? io_in_1_bits_slices_11_data : io_in_0_bits_slices_11_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_11_mseo = choice ? io_in_1_bits_slices_11_mseo : io_in_0_bits_slices_11_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_12_data = choice ? io_in_1_bits_slices_12_data : io_in_0_bits_slices_12_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_12_mseo = choice ? io_in_1_bits_slices_12_mseo : io_in_0_bits_slices_12_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_13_data = choice ? io_in_1_bits_slices_13_data : io_in_0_bits_slices_13_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_13_mseo = choice ? io_in_1_bits_slices_13_mseo : io_in_0_bits_slices_13_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_14_data = choice ? io_in_1_bits_slices_14_data : io_in_0_bits_slices_14_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_14_mseo = choice ? io_in_1_bits_slices_14_mseo : io_in_0_bits_slices_14_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_15_data = choice ? io_in_1_bits_slices_15_data : io_in_0_bits_slices_15_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_15_mseo = choice ? io_in_1_bits_slices_15_mseo : io_in_0_bits_slices_15_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_16_data = choice ? io_in_1_bits_slices_16_data : io_in_0_bits_slices_16_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_16_mseo = choice ? io_in_1_bits_slices_16_mseo : io_in_0_bits_slices_16_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_17_data = choice ? io_in_1_bits_slices_17_data : io_in_0_bits_slices_17_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_17_mseo = choice ? io_in_1_bits_slices_17_mseo : io_in_0_bits_slices_17_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_18_data = choice ? io_in_1_bits_slices_18_data : io_in_0_bits_slices_18_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_18_mseo = choice ? io_in_1_bits_slices_18_mseo : io_in_0_bits_slices_18_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_19_data = choice ? io_in_1_bits_slices_19_data : io_in_0_bits_slices_19_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_19_mseo = choice ? io_in_1_bits_slices_19_mseo : io_in_0_bits_slices_19_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_20_data = choice ? io_in_1_bits_slices_20_data : io_in_0_bits_slices_20_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_20_mseo = choice ? io_in_1_bits_slices_20_mseo : io_in_0_bits_slices_20_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_21_data = choice ? io_in_1_bits_slices_21_data : io_in_0_bits_slices_21_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_21_mseo = choice ? io_in_1_bits_slices_21_mseo : io_in_0_bits_slices_21_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_22_data = choice ? io_in_1_bits_slices_22_data : io_in_0_bits_slices_22_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_22_mseo = choice ? io_in_1_bits_slices_22_mseo : io_in_0_bits_slices_22_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_23_data = choice ? io_in_1_bits_slices_23_data : io_in_0_bits_slices_23_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_23_mseo = choice ? io_in_1_bits_slices_23_mseo : io_in_0_bits_slices_23_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_24_data = choice ? io_in_1_bits_slices_24_data : io_in_0_bits_slices_24_data; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  assign io_out_bits_slices_24_mseo = choice ? io_in_1_bits_slices_24_mseo : io_in_0_bits_slices_24_mseo; // @[Arbiters.scala 43:16 Arbiters.scala 43:16]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      lockIdx <= 1'h0;
    end else if (_T) begin
      if (_GEN_1) begin
        lockIdx <= _choice_T_3[0];
      end else if (_choice_T_1 < 1'h1) begin
        lockIdx <= 1'h1 + _choice_T_1;
      end else begin
        lockIdx <= _choice_T_1 - 1'h1;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  lockIdx = _RAND_0[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    lockIdx = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
