//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_39_assert(
  input         clock,
  input         reset,
  input         io_in_a_ready,
  input         io_in_a_valid,
  input  [2:0]  io_in_a_bits_opcode,
  input  [2:0]  io_in_a_bits_param,
  input  [3:0]  io_in_a_bits_size,
  input  [6:0]  io_in_a_bits_source,
  input  [29:0] io_in_a_bits_address,
  input  [7:0]  io_in_a_bits_mask,
  input         io_in_a_bits_corrupt,
  input         io_in_d_ready,
  input         io_in_d_valid,
  input  [2:0]  io_in_d_bits_opcode,
  input  [1:0]  io_in_d_bits_param,
  input  [3:0]  io_in_d_bits_size,
  input  [6:0]  io_in_d_bits_source,
  input         io_in_d_bits_sink,
  input         io_in_d_bits_denied,
  input         io_in_d_bits_corrupt
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire  _T_4; // @[Parameters.scala 47:9]
  wire  _T_5; // @[Parameters.scala 47:9]
  wire  _T_9; // @[Parameters.scala 55:32]
  wire  _T_10; // @[Parameters.scala 57:34]
  wire  _T_11; // @[Parameters.scala 55:69]
  wire  _T_12; // @[Parameters.scala 58:20]
  wire  _T_13; // @[Parameters.scala 57:50]
  wire  _T_17; // @[Parameters.scala 55:32]
  wire  _T_22; // @[Parameters.scala 47:9]
  wire  _T_23; // @[Parameters.scala 47:9]
  wire  _T_27; // @[Parameters.scala 55:32]
  wire  _T_29; // @[Parameters.scala 55:69]
  wire  _T_31; // @[Parameters.scala 57:50]
  wire  _T_35; // @[Parameters.scala 55:32]
  wire  _T_40; // @[Parameters.scala 47:9]
  wire  _T_44; // @[Parameters.scala 55:32]
  wire  _T_52; // @[Parameters.scala 55:32]
  wire  _T_60; // @[Parameters.scala 55:32]
  wire  _T_68; // @[Parameters.scala 55:32]
  wire  _T_76; // @[Parameters.scala 55:32]
  wire  _T_82; // @[Parameters.scala 924:46]
  wire  _T_83; // @[Parameters.scala 924:46]
  wire  _T_84; // @[Parameters.scala 924:46]
  wire  _T_85; // @[Parameters.scala 924:46]
  wire  _T_86; // @[Parameters.scala 924:46]
  wire  _T_87; // @[Parameters.scala 924:46]
  wire  _T_88; // @[Parameters.scala 924:46]
  wire  _T_89; // @[Parameters.scala 924:46]
  wire  _T_90; // @[Parameters.scala 924:46]
  wire  _T_91; // @[Parameters.scala 924:46]
  wire  _T_92; // @[Parameters.scala 924:46]
  wire  _T_93; // @[Parameters.scala 924:46]
  wire  _T_94; // @[Parameters.scala 924:46]
  wire [22:0] _T_96; // @[package.scala 189:77]
  wire [7:0] _T_98; // @[package.scala 189:46]
  wire [29:0] _GEN_18; // @[Edges.scala 22:16]
  wire [29:0] _T_99; // @[Edges.scala 22:16]
  wire  _T_100; // @[Edges.scala 22:24]
  wire [3:0] _T_103; // @[OneHot.scala 65:12]
  wire [2:0] _T_105; // @[Misc.scala 201:81]
  wire  _T_106; // @[Misc.scala 205:21]
  wire  _T_109; // @[Misc.scala 210:20]
  wire  _T_111; // @[Misc.scala 214:38]
  wire  _T_112; // @[Misc.scala 214:29]
  wire  _T_114; // @[Misc.scala 214:38]
  wire  _T_115; // @[Misc.scala 214:29]
  wire  _T_118; // @[Misc.scala 210:20]
  wire  _T_119; // @[Misc.scala 213:27]
  wire  _T_120; // @[Misc.scala 214:38]
  wire  _T_121; // @[Misc.scala 214:29]
  wire  _T_122; // @[Misc.scala 213:27]
  wire  _T_123; // @[Misc.scala 214:38]
  wire  _T_124; // @[Misc.scala 214:29]
  wire  _T_125; // @[Misc.scala 213:27]
  wire  _T_126; // @[Misc.scala 214:38]
  wire  _T_127; // @[Misc.scala 214:29]
  wire  _T_128; // @[Misc.scala 213:27]
  wire  _T_129; // @[Misc.scala 214:38]
  wire  _T_130; // @[Misc.scala 214:29]
  wire  _T_133; // @[Misc.scala 210:20]
  wire  _T_134; // @[Misc.scala 213:27]
  wire  _T_135; // @[Misc.scala 214:38]
  wire  _T_136; // @[Misc.scala 214:29]
  wire  _T_137; // @[Misc.scala 213:27]
  wire  _T_138; // @[Misc.scala 214:38]
  wire  _T_139; // @[Misc.scala 214:29]
  wire  _T_140; // @[Misc.scala 213:27]
  wire  _T_141; // @[Misc.scala 214:38]
  wire  _T_142; // @[Misc.scala 214:29]
  wire  _T_143; // @[Misc.scala 213:27]
  wire  _T_144; // @[Misc.scala 214:38]
  wire  _T_145; // @[Misc.scala 214:29]
  wire  _T_146; // @[Misc.scala 213:27]
  wire  _T_147; // @[Misc.scala 214:38]
  wire  _T_148; // @[Misc.scala 214:29]
  wire  _T_149; // @[Misc.scala 213:27]
  wire  _T_150; // @[Misc.scala 214:38]
  wire  _T_151; // @[Misc.scala 214:29]
  wire  _T_152; // @[Misc.scala 213:27]
  wire  _T_153; // @[Misc.scala 214:38]
  wire  _T_154; // @[Misc.scala 214:29]
  wire  _T_155; // @[Misc.scala 213:27]
  wire  _T_156; // @[Misc.scala 214:38]
  wire  _T_157; // @[Misc.scala 214:29]
  wire [7:0] _T_164; // @[Cat.scala 29:58]
  wire [30:0] _T_168; // @[Parameters.scala 137:49]
  wire  _T_356; // @[Monitor.scala 79:25]
  wire [29:0] _T_358; // @[Parameters.scala 137:31]
  wire [30:0] _T_359; // @[Parameters.scala 137:49]
  wire [29:0] _T_363; // @[Parameters.scala 137:31]
  wire [30:0] _T_364; // @[Parameters.scala 137:49]
  wire [30:0] _T_366; // @[Parameters.scala 137:52]
  wire  _T_367; // @[Parameters.scala 137:67]
  wire [29:0] _T_368; // @[Parameters.scala 137:31]
  wire [30:0] _T_369; // @[Parameters.scala 137:49]
  wire [30:0] _T_371; // @[Parameters.scala 137:52]
  wire  _T_372; // @[Parameters.scala 137:67]
  wire [30:0] _T_376; // @[Parameters.scala 137:52]
  wire  _T_377; // @[Parameters.scala 137:67]
  wire [29:0] _T_378; // @[Parameters.scala 137:31]
  wire [30:0] _T_379; // @[Parameters.scala 137:49]
  wire [30:0] _T_381; // @[Parameters.scala 137:52]
  wire  _T_382; // @[Parameters.scala 137:67]
  wire [29:0] _T_383; // @[Parameters.scala 137:31]
  wire [30:0] _T_384; // @[Parameters.scala 137:49]
  wire [29:0] _T_388; // @[Parameters.scala 137:31]
  wire [30:0] _T_389; // @[Parameters.scala 137:49]
  wire [30:0] _T_391; // @[Parameters.scala 137:52]
  wire  _T_392; // @[Parameters.scala 137:67]
  wire [29:0] _T_393; // @[Parameters.scala 137:31]
  wire [30:0] _T_394; // @[Parameters.scala 137:49]
  wire [30:0] _T_396; // @[Parameters.scala 137:52]
  wire  _T_397; // @[Parameters.scala 137:67]
  wire  _T_409; // @[Monitor.scala 44:11]
  wire  _T_488; // @[Parameters.scala 92:48]
  wire  _T_492; // @[Mux.scala 27:72]
  wire  _T_493; // @[Mux.scala 27:72]
  wire  _T_496; // @[Mux.scala 27:72]
  wire  _T_497; // @[Mux.scala 27:72]
  wire  _T_506; // @[Mux.scala 27:72]
  wire  _T_509; // @[Mux.scala 27:72]
  wire  _T_510; // @[Mux.scala 27:72]
  wire  _T_521; // @[Monitor.scala 44:11]
  wire  _T_522; // @[Monitor.scala 44:11]
  wire  _T_524; // @[Monitor.scala 44:11]
  wire  _T_525; // @[Monitor.scala 44:11]
  wire  _T_528; // @[Monitor.scala 44:11]
  wire  _T_529; // @[Monitor.scala 44:11]
  wire  _T_531; // @[Monitor.scala 44:11]
  wire  _T_532; // @[Monitor.scala 44:11]
  wire  _T_533; // @[Bundles.scala 110:27]
  wire  _T_535; // @[Monitor.scala 44:11]
  wire  _T_536; // @[Monitor.scala 44:11]
  wire [7:0] _T_537; // @[Monitor.scala 86:18]
  wire  _T_538; // @[Monitor.scala 86:31]
  wire  _T_540; // @[Monitor.scala 44:11]
  wire  _T_541; // @[Monitor.scala 44:11]
  wire  _T_542; // @[Monitor.scala 87:18]
  wire  _T_544; // @[Monitor.scala 44:11]
  wire  _T_545; // @[Monitor.scala 44:11]
  wire  _T_546; // @[Monitor.scala 90:25]
  wire  _T_727; // @[Monitor.scala 97:31]
  wire  _T_729; // @[Monitor.scala 44:11]
  wire  _T_730; // @[Monitor.scala 44:11]
  wire  _T_740; // @[Monitor.scala 102:25]
  wire  _T_742; // @[Parameters.scala 93:42]
  wire [30:0] _T_748; // @[Parameters.scala 137:52]
  wire  _T_749; // @[Parameters.scala 137:67]
  wire  _T_750; // @[Parameters.scala 551:56]
  wire  _T_752; // @[Parameters.scala 93:42]
  wire [30:0] _T_778; // @[Parameters.scala 137:52]
  wire  _T_779; // @[Parameters.scala 137:67]
  wire  _T_790; // @[Parameters.scala 552:42]
  wire  _T_791; // @[Parameters.scala 552:42]
  wire  _T_792; // @[Parameters.scala 552:42]
  wire  _T_793; // @[Parameters.scala 552:42]
  wire  _T_794; // @[Parameters.scala 552:42]
  wire  _T_795; // @[Parameters.scala 552:42]
  wire  _T_796; // @[Parameters.scala 551:56]
  wire  _T_798; // @[Parameters.scala 553:30]
  wire  _T_800; // @[Monitor.scala 44:11]
  wire  _T_801; // @[Monitor.scala 44:11]
  wire  _T_808; // @[Monitor.scala 106:31]
  wire  _T_810; // @[Monitor.scala 44:11]
  wire  _T_811; // @[Monitor.scala 44:11]
  wire  _T_812; // @[Monitor.scala 107:30]
  wire  _T_814; // @[Monitor.scala 44:11]
  wire  _T_815; // @[Monitor.scala 44:11]
  wire  _T_820; // @[Monitor.scala 111:25]
  wire  _T_896; // @[Monitor.scala 119:25]
  wire [7:0] _T_968; // @[Monitor.scala 124:33]
  wire [7:0] _T_969; // @[Monitor.scala 124:31]
  wire  _T_970; // @[Monitor.scala 124:40]
  wire  _T_972; // @[Monitor.scala 44:11]
  wire  _T_973; // @[Monitor.scala 44:11]
  wire  _T_974; // @[Monitor.scala 127:25]
  wire  _T_976; // @[Parameters.scala 93:42]
  wire  _T_984; // @[Parameters.scala 551:56]
  wire  _T_1031; // @[Monitor.scala 44:11]
  wire  _T_1032; // @[Monitor.scala 44:11]
  wire  _T_1039; // @[Bundles.scala 140:33]
  wire  _T_1041; // @[Monitor.scala 44:11]
  wire  _T_1042; // @[Monitor.scala 44:11]
  wire  _T_1047; // @[Monitor.scala 135:25]
  wire  _T_1112; // @[Bundles.scala 147:30]
  wire  _T_1114; // @[Monitor.scala 44:11]
  wire  _T_1115; // @[Monitor.scala 44:11]
  wire  _T_1120; // @[Monitor.scala 143:25]
  wire  _T_1177; // @[Monitor.scala 44:11]
  wire  _T_1178; // @[Monitor.scala 44:11]
  wire  _T_1185; // @[Bundles.scala 160:28]
  wire  _T_1187; // @[Monitor.scala 44:11]
  wire  _T_1188; // @[Monitor.scala 44:11]
  wire  _T_1197; // @[Bundles.scala 44:24]
  wire  _T_1199; // @[Monitor.scala 51:11]
  wire  _T_1200; // @[Monitor.scala 51:11]
  wire  _T_1201; // @[Parameters.scala 47:9]
  wire  _T_1202; // @[Parameters.scala 47:9]
  wire  _T_1206; // @[Parameters.scala 55:32]
  wire  _T_1207; // @[Parameters.scala 57:34]
  wire  _T_1208; // @[Parameters.scala 55:69]
  wire  _T_1209; // @[Parameters.scala 58:20]
  wire  _T_1210; // @[Parameters.scala 57:50]
  wire  _T_1214; // @[Parameters.scala 55:32]
  wire  _T_1219; // @[Parameters.scala 47:9]
  wire  _T_1220; // @[Parameters.scala 47:9]
  wire  _T_1224; // @[Parameters.scala 55:32]
  wire  _T_1226; // @[Parameters.scala 55:69]
  wire  _T_1228; // @[Parameters.scala 57:50]
  wire  _T_1232; // @[Parameters.scala 55:32]
  wire  _T_1237; // @[Parameters.scala 47:9]
  wire  _T_1241; // @[Parameters.scala 55:32]
  wire  _T_1249; // @[Parameters.scala 55:32]
  wire  _T_1257; // @[Parameters.scala 55:32]
  wire  _T_1265; // @[Parameters.scala 55:32]
  wire  _T_1273; // @[Parameters.scala 55:32]
  wire  _T_1279; // @[Parameters.scala 924:46]
  wire  _T_1280; // @[Parameters.scala 924:46]
  wire  _T_1281; // @[Parameters.scala 924:46]
  wire  _T_1282; // @[Parameters.scala 924:46]
  wire  _T_1283; // @[Parameters.scala 924:46]
  wire  _T_1284; // @[Parameters.scala 924:46]
  wire  _T_1285; // @[Parameters.scala 924:46]
  wire  _T_1286; // @[Parameters.scala 924:46]
  wire  _T_1287; // @[Parameters.scala 924:46]
  wire  _T_1288; // @[Parameters.scala 924:46]
  wire  _T_1289; // @[Parameters.scala 924:46]
  wire  _T_1290; // @[Parameters.scala 924:46]
  wire  _T_1291; // @[Parameters.scala 924:46]
  wire  _T_1293; // @[Monitor.scala 307:25]
  wire  _T_1295; // @[Monitor.scala 51:11]
  wire  _T_1296; // @[Monitor.scala 51:11]
  wire  _T_1297; // @[Monitor.scala 309:27]
  wire  _T_1299; // @[Monitor.scala 51:11]
  wire  _T_1300; // @[Monitor.scala 51:11]
  wire  _T_1301; // @[Monitor.scala 310:28]
  wire  _T_1303; // @[Monitor.scala 51:11]
  wire  _T_1304; // @[Monitor.scala 51:11]
  wire  _T_1305; // @[Monitor.scala 311:15]
  wire  _T_1307; // @[Monitor.scala 51:11]
  wire  _T_1308; // @[Monitor.scala 51:11]
  wire  _T_1309; // @[Monitor.scala 312:15]
  wire  _T_1311; // @[Monitor.scala 51:11]
  wire  _T_1312; // @[Monitor.scala 51:11]
  wire  _T_1313; // @[Monitor.scala 315:25]
  wire  _T_1324; // @[Bundles.scala 104:26]
  wire  _T_1326; // @[Monitor.scala 51:11]
  wire  _T_1327; // @[Monitor.scala 51:11]
  wire  _T_1328; // @[Monitor.scala 320:28]
  wire  _T_1330; // @[Monitor.scala 51:11]
  wire  _T_1331; // @[Monitor.scala 51:11]
  wire  _T_1341; // @[Monitor.scala 325:25]
  wire  _T_1361; // @[Monitor.scala 331:30]
  wire  _T_1363; // @[Monitor.scala 51:11]
  wire  _T_1364; // @[Monitor.scala 51:11]
  wire  _T_1370; // @[Monitor.scala 335:25]
  wire  _T_1387; // @[Monitor.scala 343:25]
  wire  _T_1405; // @[Monitor.scala 351:25]
  wire  _T_1437; // @[Decoupled.scala 40:37]
  wire  _T_1444; // @[Edges.scala 93:28]
  reg [4:0] _T_1446; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire [4:0] _T_1448; // @[Edges.scala 231:28]
  wire  _T_1449; // @[Edges.scala 232:25]
  reg [2:0] _T_1457; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [2:0] _T_1458; // @[Monitor.scala 382:22]
  reg [31:0] _RAND_2;
  reg [3:0] _T_1459; // @[Monitor.scala 383:22]
  reg [31:0] _RAND_3;
  reg [6:0] _T_1460; // @[Monitor.scala 384:22]
  reg [31:0] _RAND_4;
  reg [29:0] _T_1461; // @[Monitor.scala 385:22]
  reg [31:0] _RAND_5;
  wire  _T_1462; // @[Monitor.scala 386:22]
  wire  _T_1463; // @[Monitor.scala 386:19]
  wire  _T_1464; // @[Monitor.scala 387:32]
  wire  _T_1466; // @[Monitor.scala 44:11]
  wire  _T_1467; // @[Monitor.scala 44:11]
  wire  _T_1468; // @[Monitor.scala 388:32]
  wire  _T_1470; // @[Monitor.scala 44:11]
  wire  _T_1471; // @[Monitor.scala 44:11]
  wire  _T_1472; // @[Monitor.scala 389:32]
  wire  _T_1474; // @[Monitor.scala 44:11]
  wire  _T_1475; // @[Monitor.scala 44:11]
  wire  _T_1476; // @[Monitor.scala 390:32]
  wire  _T_1478; // @[Monitor.scala 44:11]
  wire  _T_1479; // @[Monitor.scala 44:11]
  wire  _T_1480; // @[Monitor.scala 391:32]
  wire  _T_1482; // @[Monitor.scala 44:11]
  wire  _T_1483; // @[Monitor.scala 44:11]
  wire  _T_1485; // @[Monitor.scala 393:20]
  wire  _T_1486; // @[Decoupled.scala 40:37]
  wire [22:0] _T_1488; // @[package.scala 189:77]
  wire [7:0] _T_1490; // @[package.scala 189:46]
  reg [4:0] _T_1494; // @[Edges.scala 230:27]
  reg [31:0] _RAND_6;
  wire [4:0] _T_1496; // @[Edges.scala 231:28]
  wire  _T_1497; // @[Edges.scala 232:25]
  reg [2:0] _T_1505; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_7;
  reg [1:0] _T_1506; // @[Monitor.scala 533:22]
  reg [31:0] _RAND_8;
  reg [3:0] _T_1507; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_9;
  reg [6:0] _T_1508; // @[Monitor.scala 535:22]
  reg [31:0] _RAND_10;
  reg  _T_1509; // @[Monitor.scala 536:22]
  reg [31:0] _RAND_11;
  reg  _T_1510; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_12;
  wire  _T_1511; // @[Monitor.scala 538:22]
  wire  _T_1512; // @[Monitor.scala 538:19]
  wire  _T_1513; // @[Monitor.scala 539:29]
  wire  _T_1515; // @[Monitor.scala 51:11]
  wire  _T_1516; // @[Monitor.scala 51:11]
  wire  _T_1517; // @[Monitor.scala 540:29]
  wire  _T_1519; // @[Monitor.scala 51:11]
  wire  _T_1520; // @[Monitor.scala 51:11]
  wire  _T_1521; // @[Monitor.scala 541:29]
  wire  _T_1523; // @[Monitor.scala 51:11]
  wire  _T_1524; // @[Monitor.scala 51:11]
  wire  _T_1525; // @[Monitor.scala 542:29]
  wire  _T_1527; // @[Monitor.scala 51:11]
  wire  _T_1528; // @[Monitor.scala 51:11]
  wire  _T_1529; // @[Monitor.scala 543:29]
  wire  _T_1531; // @[Monitor.scala 51:11]
  wire  _T_1532; // @[Monitor.scala 51:11]
  wire  _T_1533; // @[Monitor.scala 544:29]
  wire  _T_1535; // @[Monitor.scala 51:11]
  wire  _T_1536; // @[Monitor.scala 51:11]
  wire  _T_1538; // @[Monitor.scala 546:20]
  reg [113:0] _T_1539; // @[Monitor.scala 568:27]
  reg [127:0] _RAND_13;
  reg [4:0] _T_1549; // @[Edges.scala 230:27]
  reg [31:0] _RAND_14;
  wire [4:0] _T_1551; // @[Edges.scala 231:28]
  wire  _T_1552; // @[Edges.scala 232:25]
  reg [4:0] _T_1568; // @[Edges.scala 230:27]
  reg [31:0] _RAND_15;
  wire [4:0] _T_1570; // @[Edges.scala 231:28]
  wire  _T_1571; // @[Edges.scala 232:25]
  wire  _T_1581; // @[Monitor.scala 574:27]
  wire [127:0] _T_1583; // @[OneHot.scala 58:35]
  wire [113:0] _T_1584; // @[Monitor.scala 576:23]
  wire  _T_1586; // @[Monitor.scala 576:14]
  wire  _T_1588; // @[Monitor.scala 576:13]
  wire  _T_1589; // @[Monitor.scala 576:13]
  wire [127:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_1593; // @[Monitor.scala 581:27]
  wire  _T_1595; // @[Monitor.scala 581:75]
  wire  _T_1596; // @[Monitor.scala 581:72]
  wire [127:0] _T_1597; // @[OneHot.scala 58:35]
  wire [113:0] _T_1598; // @[Monitor.scala 583:21]
  wire [113:0] _T_1599; // @[Monitor.scala 583:32]
  wire  _T_1602; // @[Monitor.scala 51:11]
  wire  _T_1603; // @[Monitor.scala 51:11]
  wire [127:0] _GEN_16; // @[Monitor.scala 581:91]
  wire [113:0] _T_1604; // @[Monitor.scala 590:27]
  wire [113:0] _T_1605; // @[Monitor.scala 590:38]
  wire [113:0] _T_1606; // @[Monitor.scala 590:36]
  reg [31:0] _T_1607; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_16;
  wire  _T_1608; // @[Monitor.scala 595:23]
  wire  _T_1609; // @[Monitor.scala 595:13]
  wire  _T_1610; // @[Monitor.scala 595:36]
  wire  _T_1611; // @[Monitor.scala 595:27]
  wire  _T_1612; // @[Monitor.scala 595:56]
  wire  _T_1613; // @[Monitor.scala 595:44]
  wire  _T_1615; // @[Monitor.scala 595:12]
  wire  _T_1616; // @[Monitor.scala 595:12]
  wire [31:0] _T_1618; // @[Monitor.scala 597:26]
  wire  _T_1621; // @[Monitor.scala 598:27]
  wire  _GEN_19; // @[Monitor.scala 44:11]
  wire  _GEN_35; // @[Monitor.scala 44:11]
  wire  _GEN_53; // @[Monitor.scala 44:11]
  wire  _GEN_65; // @[Monitor.scala 44:11]
  wire  _GEN_75; // @[Monitor.scala 44:11]
  wire  _GEN_85; // @[Monitor.scala 44:11]
  wire  _GEN_95; // @[Monitor.scala 44:11]
  wire  _GEN_105; // @[Monitor.scala 44:11]
  wire  _GEN_117; // @[Monitor.scala 51:11]
  wire  _GEN_127; // @[Monitor.scala 51:11]
  wire  _GEN_139; // @[Monitor.scala 51:11]
  wire  _GEN_151; // @[Monitor.scala 51:11]
  wire  _GEN_157; // @[Monitor.scala 51:11]
  wire  _GEN_163; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_4 = io_in_a_bits_source == 7'h60; // @[Parameters.scala 47:9]
  assign _T_5 = io_in_a_bits_source == 7'h61; // @[Parameters.scala 47:9]
  assign _T_9 = io_in_a_bits_source[6:4] == 3'h6; // @[Parameters.scala 55:32]
  assign _T_10 = 4'h2 <= io_in_a_bits_source[3:0]; // @[Parameters.scala 57:34]
  assign _T_11 = _T_9 & _T_10; // @[Parameters.scala 55:69]
  assign _T_12 = io_in_a_bits_source[3:0] <= 4'h8; // @[Parameters.scala 58:20]
  assign _T_13 = _T_11 & _T_12; // @[Parameters.scala 57:50]
  assign _T_17 = io_in_a_bits_source[6:1] == 6'h38; // @[Parameters.scala 55:32]
  assign _T_22 = io_in_a_bits_source == 7'h40; // @[Parameters.scala 47:9]
  assign _T_23 = io_in_a_bits_source == 7'h41; // @[Parameters.scala 47:9]
  assign _T_27 = io_in_a_bits_source[6:4] == 3'h4; // @[Parameters.scala 55:32]
  assign _T_29 = _T_27 & _T_10; // @[Parameters.scala 55:69]
  assign _T_31 = _T_29 & _T_12; // @[Parameters.scala 57:50]
  assign _T_35 = io_in_a_bits_source[6:1] == 6'h28; // @[Parameters.scala 55:32]
  assign _T_40 = io_in_a_bits_source == 7'h24; // @[Parameters.scala 47:9]
  assign _T_44 = io_in_a_bits_source[6:3] == 4'h0; // @[Parameters.scala 55:32]
  assign _T_52 = io_in_a_bits_source[6:3] == 4'h1; // @[Parameters.scala 55:32]
  assign _T_60 = io_in_a_bits_source[6:3] == 4'h2; // @[Parameters.scala 55:32]
  assign _T_68 = io_in_a_bits_source[6:3] == 4'h3; // @[Parameters.scala 55:32]
  assign _T_76 = io_in_a_bits_source[6:2] == 5'h8; // @[Parameters.scala 55:32]
  assign _T_82 = _T_4 | _T_5; // @[Parameters.scala 924:46]
  assign _T_83 = _T_82 | _T_13; // @[Parameters.scala 924:46]
  assign _T_84 = _T_83 | _T_17; // @[Parameters.scala 924:46]
  assign _T_85 = _T_84 | _T_22; // @[Parameters.scala 924:46]
  assign _T_86 = _T_85 | _T_23; // @[Parameters.scala 924:46]
  assign _T_87 = _T_86 | _T_31; // @[Parameters.scala 924:46]
  assign _T_88 = _T_87 | _T_35; // @[Parameters.scala 924:46]
  assign _T_89 = _T_88 | _T_40; // @[Parameters.scala 924:46]
  assign _T_90 = _T_89 | _T_44; // @[Parameters.scala 924:46]
  assign _T_91 = _T_90 | _T_52; // @[Parameters.scala 924:46]
  assign _T_92 = _T_91 | _T_60; // @[Parameters.scala 924:46]
  assign _T_93 = _T_92 | _T_68; // @[Parameters.scala 924:46]
  assign _T_94 = _T_93 | _T_76; // @[Parameters.scala 924:46]
  assign _T_96 = 23'hff << io_in_a_bits_size; // @[package.scala 189:77]
  assign _T_98 = ~_T_96[7:0]; // @[package.scala 189:46]
  assign _GEN_18 = {{22'd0}, _T_98}; // @[Edges.scala 22:16]
  assign _T_99 = io_in_a_bits_address & _GEN_18; // @[Edges.scala 22:16]
  assign _T_100 = _T_99 == 30'h0; // @[Edges.scala 22:24]
  assign _T_103 = 4'h1 << io_in_a_bits_size[1:0]; // @[OneHot.scala 65:12]
  assign _T_105 = _T_103[2:0] | 3'h1; // @[Misc.scala 201:81]
  assign _T_106 = io_in_a_bits_size >= 4'h3; // @[Misc.scala 205:21]
  assign _T_109 = ~io_in_a_bits_address[2]; // @[Misc.scala 210:20]
  assign _T_111 = _T_105[2] & _T_109; // @[Misc.scala 214:38]
  assign _T_112 = _T_106 | _T_111; // @[Misc.scala 214:29]
  assign _T_114 = _T_105[2] & io_in_a_bits_address[2]; // @[Misc.scala 214:38]
  assign _T_115 = _T_106 | _T_114; // @[Misc.scala 214:29]
  assign _T_118 = ~io_in_a_bits_address[1]; // @[Misc.scala 210:20]
  assign _T_119 = _T_109 & _T_118; // @[Misc.scala 213:27]
  assign _T_120 = _T_105[1] & _T_119; // @[Misc.scala 214:38]
  assign _T_121 = _T_112 | _T_120; // @[Misc.scala 214:29]
  assign _T_122 = _T_109 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_123 = _T_105[1] & _T_122; // @[Misc.scala 214:38]
  assign _T_124 = _T_112 | _T_123; // @[Misc.scala 214:29]
  assign _T_125 = io_in_a_bits_address[2] & _T_118; // @[Misc.scala 213:27]
  assign _T_126 = _T_105[1] & _T_125; // @[Misc.scala 214:38]
  assign _T_127 = _T_115 | _T_126; // @[Misc.scala 214:29]
  assign _T_128 = io_in_a_bits_address[2] & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_129 = _T_105[1] & _T_128; // @[Misc.scala 214:38]
  assign _T_130 = _T_115 | _T_129; // @[Misc.scala 214:29]
  assign _T_133 = ~io_in_a_bits_address[0]; // @[Misc.scala 210:20]
  assign _T_134 = _T_119 & _T_133; // @[Misc.scala 213:27]
  assign _T_135 = _T_105[0] & _T_134; // @[Misc.scala 214:38]
  assign _T_136 = _T_121 | _T_135; // @[Misc.scala 214:29]
  assign _T_137 = _T_119 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_138 = _T_105[0] & _T_137; // @[Misc.scala 214:38]
  assign _T_139 = _T_121 | _T_138; // @[Misc.scala 214:29]
  assign _T_140 = _T_122 & _T_133; // @[Misc.scala 213:27]
  assign _T_141 = _T_105[0] & _T_140; // @[Misc.scala 214:38]
  assign _T_142 = _T_124 | _T_141; // @[Misc.scala 214:29]
  assign _T_143 = _T_122 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_144 = _T_105[0] & _T_143; // @[Misc.scala 214:38]
  assign _T_145 = _T_124 | _T_144; // @[Misc.scala 214:29]
  assign _T_146 = _T_125 & _T_133; // @[Misc.scala 213:27]
  assign _T_147 = _T_105[0] & _T_146; // @[Misc.scala 214:38]
  assign _T_148 = _T_127 | _T_147; // @[Misc.scala 214:29]
  assign _T_149 = _T_125 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_150 = _T_105[0] & _T_149; // @[Misc.scala 214:38]
  assign _T_151 = _T_127 | _T_150; // @[Misc.scala 214:29]
  assign _T_152 = _T_128 & _T_133; // @[Misc.scala 213:27]
  assign _T_153 = _T_105[0] & _T_152; // @[Misc.scala 214:38]
  assign _T_154 = _T_130 | _T_153; // @[Misc.scala 214:29]
  assign _T_155 = _T_128 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_156 = _T_105[0] & _T_155; // @[Misc.scala 214:38]
  assign _T_157 = _T_130 | _T_156; // @[Misc.scala 214:29]
  assign _T_164 = {_T_157,_T_154,_T_151,_T_148,_T_145,_T_142,_T_139,_T_136}; // @[Cat.scala 29:58]
  assign _T_168 = {1'b0,$signed(io_in_a_bits_address)}; // @[Parameters.scala 137:49]
  assign _T_356 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_358 = io_in_a_bits_address ^ 30'h3000; // @[Parameters.scala 137:31]
  assign _T_359 = {1'b0,$signed(_T_358)}; // @[Parameters.scala 137:49]
  assign _T_363 = io_in_a_bits_address ^ 30'hc000000; // @[Parameters.scala 137:31]
  assign _T_364 = {1'b0,$signed(_T_363)}; // @[Parameters.scala 137:49]
  assign _T_366 = $signed(_T_364) & -31'sh4000000; // @[Parameters.scala 137:52]
  assign _T_367 = $signed(_T_366) == 31'sh0; // @[Parameters.scala 137:67]
  assign _T_368 = io_in_a_bits_address ^ 30'h2000000; // @[Parameters.scala 137:31]
  assign _T_369 = {1'b0,$signed(_T_368)}; // @[Parameters.scala 137:49]
  assign _T_371 = $signed(_T_369) & -31'sh10000; // @[Parameters.scala 137:52]
  assign _T_372 = $signed(_T_371) == 31'sh0; // @[Parameters.scala 137:67]
  assign _T_376 = $signed(_T_168) & -31'sh10001000; // @[Parameters.scala 137:52]
  assign _T_377 = $signed(_T_376) == 31'sh0; // @[Parameters.scala 137:67]
  assign _T_378 = io_in_a_bits_address ^ 30'h20000000; // @[Parameters.scala 137:31]
  assign _T_379 = {1'b0,$signed(_T_378)}; // @[Parameters.scala 137:49]
  assign _T_381 = $signed(_T_379) & -31'sh20000000; // @[Parameters.scala 137:52]
  assign _T_382 = $signed(_T_381) == 31'sh0; // @[Parameters.scala 137:67]
  assign _T_383 = io_in_a_bits_address ^ 30'h10001000; // @[Parameters.scala 137:31]
  assign _T_384 = {1'b0,$signed(_T_383)}; // @[Parameters.scala 137:49]
  assign _T_388 = io_in_a_bits_address ^ 30'h4000; // @[Parameters.scala 137:31]
  assign _T_389 = {1'b0,$signed(_T_388)}; // @[Parameters.scala 137:49]
  assign _T_391 = $signed(_T_389) & -31'sh1000; // @[Parameters.scala 137:52]
  assign _T_392 = $signed(_T_391) == 31'sh0; // @[Parameters.scala 137:67]
  assign _T_393 = io_in_a_bits_address ^ 30'h2010000; // @[Parameters.scala 137:31]
  assign _T_394 = {1'b0,$signed(_T_393)}; // @[Parameters.scala 137:49]
  assign _T_396 = $signed(_T_394) & -31'sh4000; // @[Parameters.scala 137:52]
  assign _T_397 = $signed(_T_396) == 31'sh0; // @[Parameters.scala 137:67]
  assign _T_409 = ~reset; // @[Monitor.scala 44:11]
  assign _T_488 = 4'h6 == io_in_a_bits_size; // @[Parameters.scala 92:48]
  assign _T_492 = _T_4 & _T_488; // @[Mux.scala 27:72]
  assign _T_493 = _T_5 & _T_488; // @[Mux.scala 27:72]
  assign _T_496 = _T_22 & _T_488; // @[Mux.scala 27:72]
  assign _T_497 = _T_23 & _T_488; // @[Mux.scala 27:72]
  assign _T_506 = _T_492 | _T_493; // @[Mux.scala 27:72]
  assign _T_509 = _T_506 | _T_496; // @[Mux.scala 27:72]
  assign _T_510 = _T_509 | _T_497; // @[Mux.scala 27:72]
  assign _T_521 = _T_510 | reset; // @[Monitor.scala 44:11]
  assign _T_522 = ~_T_521; // @[Monitor.scala 44:11]
  assign _T_524 = _T_94 | reset; // @[Monitor.scala 44:11]
  assign _T_525 = ~_T_524; // @[Monitor.scala 44:11]
  assign _T_528 = _T_106 | reset; // @[Monitor.scala 44:11]
  assign _T_529 = ~_T_528; // @[Monitor.scala 44:11]
  assign _T_531 = _T_100 | reset; // @[Monitor.scala 44:11]
  assign _T_532 = ~_T_531; // @[Monitor.scala 44:11]
  assign _T_533 = io_in_a_bits_param <= 3'h2; // @[Bundles.scala 110:27]
  assign _T_535 = _T_533 | reset; // @[Monitor.scala 44:11]
  assign _T_536 = ~_T_535; // @[Monitor.scala 44:11]
  assign _T_537 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_538 = _T_537 == 8'h0; // @[Monitor.scala 86:31]
  assign _T_540 = _T_538 | reset; // @[Monitor.scala 44:11]
  assign _T_541 = ~_T_540; // @[Monitor.scala 44:11]
  assign _T_542 = ~io_in_a_bits_corrupt; // @[Monitor.scala 87:18]
  assign _T_544 = _T_542 | reset; // @[Monitor.scala 44:11]
  assign _T_545 = ~_T_544; // @[Monitor.scala 44:11]
  assign _T_546 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_727 = io_in_a_bits_param != 3'h0; // @[Monitor.scala 97:31]
  assign _T_729 = _T_727 | reset; // @[Monitor.scala 44:11]
  assign _T_730 = ~_T_729; // @[Monitor.scala 44:11]
  assign _T_740 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_742 = io_in_a_bits_size <= 4'h8; // @[Parameters.scala 93:42]
  assign _T_748 = $signed(_T_359) & -31'sh1000; // @[Parameters.scala 137:52]
  assign _T_749 = $signed(_T_748) == 31'sh0; // @[Parameters.scala 137:67]
  assign _T_750 = _T_742 & _T_749; // @[Parameters.scala 551:56]
  assign _T_752 = io_in_a_bits_size <= 4'h6; // @[Parameters.scala 93:42]
  assign _T_778 = $signed(_T_384) & -31'sh3000; // @[Parameters.scala 137:52]
  assign _T_779 = $signed(_T_778) == 31'sh0; // @[Parameters.scala 137:67]
  assign _T_790 = _T_367 | _T_372; // @[Parameters.scala 552:42]
  assign _T_791 = _T_790 | _T_377; // @[Parameters.scala 552:42]
  assign _T_792 = _T_791 | _T_382; // @[Parameters.scala 552:42]
  assign _T_793 = _T_792 | _T_779; // @[Parameters.scala 552:42]
  assign _T_794 = _T_793 | _T_392; // @[Parameters.scala 552:42]
  assign _T_795 = _T_794 | _T_397; // @[Parameters.scala 552:42]
  assign _T_796 = _T_752 & _T_795; // @[Parameters.scala 551:56]
  assign _T_798 = _T_750 | _T_796; // @[Parameters.scala 553:30]
  assign _T_800 = _T_798 | reset; // @[Monitor.scala 44:11]
  assign _T_801 = ~_T_800; // @[Monitor.scala 44:11]
  assign _T_808 = io_in_a_bits_param == 3'h0; // @[Monitor.scala 106:31]
  assign _T_810 = _T_808 | reset; // @[Monitor.scala 44:11]
  assign _T_811 = ~_T_810; // @[Monitor.scala 44:11]
  assign _T_812 = io_in_a_bits_mask == _T_164; // @[Monitor.scala 107:30]
  assign _T_814 = _T_812 | reset; // @[Monitor.scala 44:11]
  assign _T_815 = ~_T_814; // @[Monitor.scala 44:11]
  assign _T_820 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_896 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_968 = ~_T_164; // @[Monitor.scala 124:33]
  assign _T_969 = io_in_a_bits_mask & _T_968; // @[Monitor.scala 124:31]
  assign _T_970 = _T_969 == 8'h0; // @[Monitor.scala 124:40]
  assign _T_972 = _T_970 | reset; // @[Monitor.scala 44:11]
  assign _T_973 = ~_T_972; // @[Monitor.scala 44:11]
  assign _T_974 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_976 = io_in_a_bits_size <= 4'h3; // @[Parameters.scala 93:42]
  assign _T_984 = _T_976 & _T_749; // @[Parameters.scala 551:56]
  assign _T_1031 = _T_984 | reset; // @[Monitor.scala 44:11]
  assign _T_1032 = ~_T_1031; // @[Monitor.scala 44:11]
  assign _T_1039 = io_in_a_bits_param <= 3'h4; // @[Bundles.scala 140:33]
  assign _T_1041 = _T_1039 | reset; // @[Monitor.scala 44:11]
  assign _T_1042 = ~_T_1041; // @[Monitor.scala 44:11]
  assign _T_1047 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_1112 = io_in_a_bits_param <= 3'h3; // @[Bundles.scala 147:30]
  assign _T_1114 = _T_1112 | reset; // @[Monitor.scala 44:11]
  assign _T_1115 = ~_T_1114; // @[Monitor.scala 44:11]
  assign _T_1120 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_1177 = _T_750 | reset; // @[Monitor.scala 44:11]
  assign _T_1178 = ~_T_1177; // @[Monitor.scala 44:11]
  assign _T_1185 = io_in_a_bits_param <= 3'h1; // @[Bundles.scala 160:28]
  assign _T_1187 = _T_1185 | reset; // @[Monitor.scala 44:11]
  assign _T_1188 = ~_T_1187; // @[Monitor.scala 44:11]
  assign _T_1197 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_1199 = _T_1197 | reset; // @[Monitor.scala 51:11]
  assign _T_1200 = ~_T_1199; // @[Monitor.scala 51:11]
  assign _T_1201 = io_in_d_bits_source == 7'h60; // @[Parameters.scala 47:9]
  assign _T_1202 = io_in_d_bits_source == 7'h61; // @[Parameters.scala 47:9]
  assign _T_1206 = io_in_d_bits_source[6:4] == 3'h6; // @[Parameters.scala 55:32]
  assign _T_1207 = 4'h2 <= io_in_d_bits_source[3:0]; // @[Parameters.scala 57:34]
  assign _T_1208 = _T_1206 & _T_1207; // @[Parameters.scala 55:69]
  assign _T_1209 = io_in_d_bits_source[3:0] <= 4'h8; // @[Parameters.scala 58:20]
  assign _T_1210 = _T_1208 & _T_1209; // @[Parameters.scala 57:50]
  assign _T_1214 = io_in_d_bits_source[6:1] == 6'h38; // @[Parameters.scala 55:32]
  assign _T_1219 = io_in_d_bits_source == 7'h40; // @[Parameters.scala 47:9]
  assign _T_1220 = io_in_d_bits_source == 7'h41; // @[Parameters.scala 47:9]
  assign _T_1224 = io_in_d_bits_source[6:4] == 3'h4; // @[Parameters.scala 55:32]
  assign _T_1226 = _T_1224 & _T_1207; // @[Parameters.scala 55:69]
  assign _T_1228 = _T_1226 & _T_1209; // @[Parameters.scala 57:50]
  assign _T_1232 = io_in_d_bits_source[6:1] == 6'h28; // @[Parameters.scala 55:32]
  assign _T_1237 = io_in_d_bits_source == 7'h24; // @[Parameters.scala 47:9]
  assign _T_1241 = io_in_d_bits_source[6:3] == 4'h0; // @[Parameters.scala 55:32]
  assign _T_1249 = io_in_d_bits_source[6:3] == 4'h1; // @[Parameters.scala 55:32]
  assign _T_1257 = io_in_d_bits_source[6:3] == 4'h2; // @[Parameters.scala 55:32]
  assign _T_1265 = io_in_d_bits_source[6:3] == 4'h3; // @[Parameters.scala 55:32]
  assign _T_1273 = io_in_d_bits_source[6:2] == 5'h8; // @[Parameters.scala 55:32]
  assign _T_1279 = _T_1201 | _T_1202; // @[Parameters.scala 924:46]
  assign _T_1280 = _T_1279 | _T_1210; // @[Parameters.scala 924:46]
  assign _T_1281 = _T_1280 | _T_1214; // @[Parameters.scala 924:46]
  assign _T_1282 = _T_1281 | _T_1219; // @[Parameters.scala 924:46]
  assign _T_1283 = _T_1282 | _T_1220; // @[Parameters.scala 924:46]
  assign _T_1284 = _T_1283 | _T_1228; // @[Parameters.scala 924:46]
  assign _T_1285 = _T_1284 | _T_1232; // @[Parameters.scala 924:46]
  assign _T_1286 = _T_1285 | _T_1237; // @[Parameters.scala 924:46]
  assign _T_1287 = _T_1286 | _T_1241; // @[Parameters.scala 924:46]
  assign _T_1288 = _T_1287 | _T_1249; // @[Parameters.scala 924:46]
  assign _T_1289 = _T_1288 | _T_1257; // @[Parameters.scala 924:46]
  assign _T_1290 = _T_1289 | _T_1265; // @[Parameters.scala 924:46]
  assign _T_1291 = _T_1290 | _T_1273; // @[Parameters.scala 924:46]
  assign _T_1293 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_1295 = _T_1291 | reset; // @[Monitor.scala 51:11]
  assign _T_1296 = ~_T_1295; // @[Monitor.scala 51:11]
  assign _T_1297 = io_in_d_bits_size >= 4'h3; // @[Monitor.scala 309:27]
  assign _T_1299 = _T_1297 | reset; // @[Monitor.scala 51:11]
  assign _T_1300 = ~_T_1299; // @[Monitor.scala 51:11]
  assign _T_1301 = io_in_d_bits_param == 2'h0; // @[Monitor.scala 310:28]
  assign _T_1303 = _T_1301 | reset; // @[Monitor.scala 51:11]
  assign _T_1304 = ~_T_1303; // @[Monitor.scala 51:11]
  assign _T_1305 = ~io_in_d_bits_corrupt; // @[Monitor.scala 311:15]
  assign _T_1307 = _T_1305 | reset; // @[Monitor.scala 51:11]
  assign _T_1308 = ~_T_1307; // @[Monitor.scala 51:11]
  assign _T_1309 = ~io_in_d_bits_denied; // @[Monitor.scala 312:15]
  assign _T_1311 = _T_1309 | reset; // @[Monitor.scala 51:11]
  assign _T_1312 = ~_T_1311; // @[Monitor.scala 51:11]
  assign _T_1313 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_1324 = io_in_d_bits_param <= 2'h2; // @[Bundles.scala 104:26]
  assign _T_1326 = _T_1324 | reset; // @[Monitor.scala 51:11]
  assign _T_1327 = ~_T_1326; // @[Monitor.scala 51:11]
  assign _T_1328 = io_in_d_bits_param != 2'h2; // @[Monitor.scala 320:28]
  assign _T_1330 = _T_1328 | reset; // @[Monitor.scala 51:11]
  assign _T_1331 = ~_T_1330; // @[Monitor.scala 51:11]
  assign _T_1341 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_1361 = _T_1309 | io_in_d_bits_corrupt; // @[Monitor.scala 331:30]
  assign _T_1363 = _T_1361 | reset; // @[Monitor.scala 51:11]
  assign _T_1364 = ~_T_1363; // @[Monitor.scala 51:11]
  assign _T_1370 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_1387 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_1405 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_1437 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_1444 = ~io_in_a_bits_opcode[2]; // @[Edges.scala 93:28]
  assign _T_1448 = _T_1446 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1449 = _T_1446 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1462 = ~_T_1449; // @[Monitor.scala 386:22]
  assign _T_1463 = io_in_a_valid & _T_1462; // @[Monitor.scala 386:19]
  assign _T_1464 = io_in_a_bits_opcode == _T_1457; // @[Monitor.scala 387:32]
  assign _T_1466 = _T_1464 | reset; // @[Monitor.scala 44:11]
  assign _T_1467 = ~_T_1466; // @[Monitor.scala 44:11]
  assign _T_1468 = io_in_a_bits_param == _T_1458; // @[Monitor.scala 388:32]
  assign _T_1470 = _T_1468 | reset; // @[Monitor.scala 44:11]
  assign _T_1471 = ~_T_1470; // @[Monitor.scala 44:11]
  assign _T_1472 = io_in_a_bits_size == _T_1459; // @[Monitor.scala 389:32]
  assign _T_1474 = _T_1472 | reset; // @[Monitor.scala 44:11]
  assign _T_1475 = ~_T_1474; // @[Monitor.scala 44:11]
  assign _T_1476 = io_in_a_bits_source == _T_1460; // @[Monitor.scala 390:32]
  assign _T_1478 = _T_1476 | reset; // @[Monitor.scala 44:11]
  assign _T_1479 = ~_T_1478; // @[Monitor.scala 44:11]
  assign _T_1480 = io_in_a_bits_address == _T_1461; // @[Monitor.scala 391:32]
  assign _T_1482 = _T_1480 | reset; // @[Monitor.scala 44:11]
  assign _T_1483 = ~_T_1482; // @[Monitor.scala 44:11]
  assign _T_1485 = _T_1437 & _T_1449; // @[Monitor.scala 393:20]
  assign _T_1486 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_1488 = 23'hff << io_in_d_bits_size; // @[package.scala 189:77]
  assign _T_1490 = ~_T_1488[7:0]; // @[package.scala 189:46]
  assign _T_1496 = _T_1494 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1497 = _T_1494 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1511 = ~_T_1497; // @[Monitor.scala 538:22]
  assign _T_1512 = io_in_d_valid & _T_1511; // @[Monitor.scala 538:19]
  assign _T_1513 = io_in_d_bits_opcode == _T_1505; // @[Monitor.scala 539:29]
  assign _T_1515 = _T_1513 | reset; // @[Monitor.scala 51:11]
  assign _T_1516 = ~_T_1515; // @[Monitor.scala 51:11]
  assign _T_1517 = io_in_d_bits_param == _T_1506; // @[Monitor.scala 540:29]
  assign _T_1519 = _T_1517 | reset; // @[Monitor.scala 51:11]
  assign _T_1520 = ~_T_1519; // @[Monitor.scala 51:11]
  assign _T_1521 = io_in_d_bits_size == _T_1507; // @[Monitor.scala 541:29]
  assign _T_1523 = _T_1521 | reset; // @[Monitor.scala 51:11]
  assign _T_1524 = ~_T_1523; // @[Monitor.scala 51:11]
  assign _T_1525 = io_in_d_bits_source == _T_1508; // @[Monitor.scala 542:29]
  assign _T_1527 = _T_1525 | reset; // @[Monitor.scala 51:11]
  assign _T_1528 = ~_T_1527; // @[Monitor.scala 51:11]
  assign _T_1529 = io_in_d_bits_sink == _T_1509; // @[Monitor.scala 543:29]
  assign _T_1531 = _T_1529 | reset; // @[Monitor.scala 51:11]
  assign _T_1532 = ~_T_1531; // @[Monitor.scala 51:11]
  assign _T_1533 = io_in_d_bits_denied == _T_1510; // @[Monitor.scala 544:29]
  assign _T_1535 = _T_1533 | reset; // @[Monitor.scala 51:11]
  assign _T_1536 = ~_T_1535; // @[Monitor.scala 51:11]
  assign _T_1538 = _T_1486 & _T_1497; // @[Monitor.scala 546:20]
  assign _T_1551 = _T_1549 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1552 = _T_1549 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1570 = _T_1568 - 5'h1; // @[Edges.scala 231:28]
  assign _T_1571 = _T_1568 == 5'h0; // @[Edges.scala 232:25]
  assign _T_1581 = _T_1437 & _T_1552; // @[Monitor.scala 574:27]
  assign _T_1583 = 128'h1 << io_in_a_bits_source; // @[OneHot.scala 58:35]
  assign _T_1584 = _T_1539 >> io_in_a_bits_source; // @[Monitor.scala 576:23]
  assign _T_1586 = ~_T_1584[0]; // @[Monitor.scala 576:14]
  assign _T_1588 = _T_1586 | reset; // @[Monitor.scala 576:13]
  assign _T_1589 = ~_T_1588; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_1581 ? _T_1583 : 128'h0; // @[Monitor.scala 574:72]
  assign _T_1593 = _T_1486 & _T_1571; // @[Monitor.scala 581:27]
  assign _T_1595 = ~_T_1293; // @[Monitor.scala 581:75]
  assign _T_1596 = _T_1593 & _T_1595; // @[Monitor.scala 581:72]
  assign _T_1597 = 128'h1 << io_in_d_bits_source; // @[OneHot.scala 58:35]
  assign _T_1598 = _GEN_15[113:0] | _T_1539; // @[Monitor.scala 583:21]
  assign _T_1599 = _T_1598 >> io_in_d_bits_source; // @[Monitor.scala 583:32]
  assign _T_1602 = _T_1599[0] | reset; // @[Monitor.scala 51:11]
  assign _T_1603 = ~_T_1602; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_1596 ? _T_1597 : 128'h0; // @[Monitor.scala 581:91]
  assign _T_1604 = _T_1539 | _GEN_15[113:0]; // @[Monitor.scala 590:27]
  assign _T_1605 = ~_GEN_16[113:0]; // @[Monitor.scala 590:38]
  assign _T_1606 = _T_1604 & _T_1605; // @[Monitor.scala 590:36]
  assign _T_1608 = _T_1539 != 114'h0; // @[Monitor.scala 595:23]
  assign _T_1609 = ~_T_1608; // @[Monitor.scala 595:13]
  assign _T_1610 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_1611 = _T_1609 | _T_1610; // @[Monitor.scala 595:27]
  assign _T_1612 = _T_1607 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_1613 = _T_1611 | _T_1612; // @[Monitor.scala 595:44]
  assign _T_1615 = _T_1613 | reset; // @[Monitor.scala 595:12]
  assign _T_1616 = ~_T_1615; // @[Monitor.scala 595:12]
  assign _T_1618 = _T_1607 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_1621 = _T_1437 | _T_1486; // @[Monitor.scala 598:27]
  assign _GEN_19 = io_in_a_valid & _T_356; // @[Monitor.scala 44:11]
  assign _GEN_35 = io_in_a_valid & _T_546; // @[Monitor.scala 44:11]
  assign _GEN_53 = io_in_a_valid & _T_740; // @[Monitor.scala 44:11]
  assign _GEN_65 = io_in_a_valid & _T_820; // @[Monitor.scala 44:11]
  assign _GEN_75 = io_in_a_valid & _T_896; // @[Monitor.scala 44:11]
  assign _GEN_85 = io_in_a_valid & _T_974; // @[Monitor.scala 44:11]
  assign _GEN_95 = io_in_a_valid & _T_1047; // @[Monitor.scala 44:11]
  assign _GEN_105 = io_in_a_valid & _T_1120; // @[Monitor.scala 44:11]
  assign _GEN_117 = io_in_d_valid & _T_1293; // @[Monitor.scala 51:11]
  assign _GEN_127 = io_in_d_valid & _T_1313; // @[Monitor.scala 51:11]
  assign _GEN_139 = io_in_d_valid & _T_1341; // @[Monitor.scala 51:11]
  assign _GEN_151 = io_in_d_valid & _T_1370; // @[Monitor.scala 51:11]
  assign _GEN_157 = io_in_d_valid & _T_1387; // @[Monitor.scala 51:11]
  assign _GEN_163 = io_in_d_valid & _T_1405; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_1446 = _RAND_0[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_1457 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_1458 = _RAND_2[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_1459 = _RAND_3[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_1460 = _RAND_4[6:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  _T_1461 = _RAND_5[29:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_1494 = _RAND_6[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_1505 = _RAND_7[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_1506 = _RAND_8[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_1507 = _RAND_9[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_1508 = _RAND_10[6:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_1509 = _RAND_11[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_1510 = _RAND_12[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {4{`RANDOM}};
  _T_1539 = _RAND_13[113:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_14 = {1{`RANDOM}};
  _T_1549 = _RAND_14[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_15 = {1{`RANDOM}};
  _T_1568 = _RAND_15[4:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_16 = {1{`RANDOM}};
  _T_1607 = _RAND_16[31:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_1446 <= 5'h0;
    end else if (_T_1437) begin
      if (_T_1449) begin
        if (_T_1444) begin
          _T_1446 <= _T_98[7:3];
        end else begin
          _T_1446 <= 5'h0;
        end
      end else begin
        _T_1446 <= _T_1448;
      end
    end
    if (_T_1485) begin
      _T_1457 <= io_in_a_bits_opcode;
    end
    if (_T_1485) begin
      _T_1458 <= io_in_a_bits_param;
    end
    if (_T_1485) begin
      _T_1459 <= io_in_a_bits_size;
    end
    if (_T_1485) begin
      _T_1460 <= io_in_a_bits_source;
    end
    if (_T_1485) begin
      _T_1461 <= io_in_a_bits_address;
    end
    if (reset) begin
      _T_1494 <= 5'h0;
    end else if (_T_1486) begin
      if (_T_1497) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1494 <= _T_1490[7:3];
        end else begin
          _T_1494 <= 5'h0;
        end
      end else begin
        _T_1494 <= _T_1496;
      end
    end
    if (_T_1538) begin
      _T_1505 <= io_in_d_bits_opcode;
    end
    if (_T_1538) begin
      _T_1506 <= io_in_d_bits_param;
    end
    if (_T_1538) begin
      _T_1507 <= io_in_d_bits_size;
    end
    if (_T_1538) begin
      _T_1508 <= io_in_d_bits_source;
    end
    if (_T_1538) begin
      _T_1509 <= io_in_d_bits_sink;
    end
    if (_T_1538) begin
      _T_1510 <= io_in_d_bits_denied;
    end
    if (reset) begin
      _T_1539 <= 114'h0;
    end else begin
      _T_1539 <= _T_1606;
    end
    if (reset) begin
      _T_1549 <= 5'h0;
    end else if (_T_1437) begin
      if (_T_1552) begin
        if (_T_1444) begin
          _T_1549 <= _T_98[7:3];
        end else begin
          _T_1549 <= 5'h0;
        end
      end else begin
        _T_1549 <= _T_1551;
      end
    end
    if (reset) begin
      _T_1568 <= 5'h0;
    end else if (_T_1486) begin
      if (_T_1571) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1568 <= _T_1490[7:3];
        end else begin
          _T_1568 <= 5'h0;
        end
      end else begin
        _T_1568 <= _T_1570;
      end
    end
    if (reset) begin
      _T_1607 <= 32'h0;
    end else if (_T_1621) begin
      _T_1607 <= 32'h0;
    end else begin
      _T_1607 <= _T_1618;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_409) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_409) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_522) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_522) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_525) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid source ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_525) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_529) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock smaller than a beat (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_529) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_532) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_532) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_536) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid grow param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_536) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_541) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_541) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_545) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock is corrupt (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_545) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_409) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_409) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_522) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_522) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_525) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid source ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_525) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_529) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm smaller than a beat (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_529) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_532) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_532) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_536) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid grow param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_536) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_730) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_730) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_541) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_541) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_545) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm is corrupt (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_545) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_801) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_801) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_525) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid source ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_525) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_532) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_532) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_811) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_811) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_815) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_815) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_545) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get is corrupt (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_545) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_801) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_801) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_525) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid source ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_525) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_532) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_532) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_811) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_811) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_815) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_815) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_801) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_801) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_525) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid source ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_525) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_532) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_532) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_811) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_811) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_973) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial contains invalid mask (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_973) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_1032) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_1032) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_525) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid source ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_525) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_532) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_532) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_1042) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid opcode param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_1042) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_815) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_815) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_1032) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_1032) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_525) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid source ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_525) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_532) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_532) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_1115) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid opcode param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_1115) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_815) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_815) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_1178) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_1178) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_525) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid source ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_525) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_532) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_532) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_1188) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid opcode param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_1188) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_815) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_815) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_545) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint is corrupt (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_545) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_1200) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_1200) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_1296) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck carries invalid source ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_1296) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_1300) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_1300) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_1304) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseeAck carries invalid param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_1304) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_1308) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is corrupt (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_1308) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_1312) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is denied (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_1312) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_1296) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid source ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_1296) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_409) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid sink ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_409) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_1300) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_1300) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_1327) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid cap param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_1327) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_1331) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries toN param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_1331) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_1308) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is corrupt (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_1308) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_139 & _T_1296) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid source ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_139 & _T_1296) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_139 & _T_409) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid sink ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_139 & _T_409) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_139 & _T_1300) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_139 & _T_1300) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_139 & _T_1327) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid cap param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_139 & _T_1327) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_139 & _T_1331) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries toN param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_139 & _T_1331) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_139 & _T_1364) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied but not corrupt (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_139 & _T_1364) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_151 & _T_1296) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid source ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_151 & _T_1296) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_151 & _T_1304) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_151 & _T_1304) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_151 & _T_1308) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is corrupt (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_151 & _T_1308) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_157 & _T_1296) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid source ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_157 & _T_1296) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_157 & _T_1304) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_157 & _T_1304) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_157 & _T_1364) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied but not corrupt (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_157 & _T_1364) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_163 & _T_1296) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid source ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_163 & _T_1296) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_163 & _T_1304) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid param (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_163 & _T_1304) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_163 & _T_1308) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is corrupt (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_163 & _T_1308) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1463 & _T_1467) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1463 & _T_1467) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1463 & _T_1471) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel param changed within multibeat operation (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1463 & _T_1471) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1463 & _T_1475) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel size changed within multibeat operation (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1463 & _T_1475) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1463 & _T_1479) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel source changed within multibeat operation (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1463 & _T_1479) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1463 & _T_1483) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1463 & _T_1483) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1512 & _T_1516) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1512 & _T_1516) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1512 & _T_1520) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel param changed within multibeat operation (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1512 & _T_1520) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1512 & _T_1524) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1512 & _T_1524) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1512 & _T_1528) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel source changed within multibeat operation (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1512 & _T_1528) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1512 & _T_1532) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel sink changed with multibeat operation (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1512 & _T_1532) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1512 & _T_1536) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1512 & _T_1536) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1581 & _T_1589) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1581 & _T_1589) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1596 & _T_1603) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1596 & _T_1603) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1616) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at PeripheryBus.scala:42:7)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1616) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
