//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_25_assert(
  input         clock,
  input         reset,
  input         io_in_a_ready,
  input         io_in_a_valid,
  input  [1:0]  io_in_a_bits_source,
  input  [35:0] io_in_a_bits_address,
  input         io_in_d_valid,
  input  [2:0]  io_in_d_bits_opcode,
  input  [1:0]  io_in_d_bits_param,
  input  [3:0]  io_in_d_bits_size,
  input  [1:0]  io_in_d_bits_source,
  input  [3:0]  io_in_d_bits_sink,
  input         io_in_d_bits_denied,
  input         io_in_d_bits_corrupt
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire [35:0] _T_17; // @[Edges.scala 22:16]
  wire  _T_18; // @[Edges.scala 22:24]
  wire [36:0] _T_62; // @[Parameters.scala 137:49]
  wire [35:0] _T_72; // @[Parameters.scala 137:31]
  wire [36:0] _T_73; // @[Parameters.scala 137:49]
  wire [36:0] _T_75; // @[Parameters.scala 137:52]
  wire  _T_76; // @[Parameters.scala 137:67]
  wire [35:0] _T_77; // @[Parameters.scala 137:31]
  wire [36:0] _T_78; // @[Parameters.scala 137:49]
  wire [35:0] _T_82; // @[Parameters.scala 137:31]
  wire [36:0] _T_83; // @[Parameters.scala 137:49]
  wire [35:0] _T_87; // @[Parameters.scala 137:31]
  wire [36:0] _T_88; // @[Parameters.scala 137:49]
  wire [36:0] _T_90; // @[Parameters.scala 137:52]
  wire  _T_91; // @[Parameters.scala 137:67]
  wire [35:0] _T_92; // @[Parameters.scala 137:31]
  wire [36:0] _T_93; // @[Parameters.scala 137:49]
  wire [36:0] _T_95; // @[Parameters.scala 137:52]
  wire  _T_96; // @[Parameters.scala 137:67]
  wire [36:0] _T_100; // @[Parameters.scala 137:52]
  wire  _T_101; // @[Parameters.scala 137:67]
  wire [35:0] _T_102; // @[Parameters.scala 137:31]
  wire [36:0] _T_103; // @[Parameters.scala 137:49]
  wire [36:0] _T_105; // @[Parameters.scala 137:52]
  wire  _T_106; // @[Parameters.scala 137:67]
  wire [35:0] _T_107; // @[Parameters.scala 137:31]
  wire [36:0] _T_108; // @[Parameters.scala 137:49]
  wire [35:0] _T_112; // @[Parameters.scala 137:31]
  wire [36:0] _T_113; // @[Parameters.scala 137:49]
  wire [36:0] _T_115; // @[Parameters.scala 137:52]
  wire  _T_116; // @[Parameters.scala 137:67]
  wire [35:0] _T_117; // @[Parameters.scala 137:31]
  wire [36:0] _T_118; // @[Parameters.scala 137:49]
  wire [36:0] _T_120; // @[Parameters.scala 137:52]
  wire  _T_121; // @[Parameters.scala 137:67]
  wire [35:0] _T_139; // @[Parameters.scala 137:31]
  wire [36:0] _T_140; // @[Parameters.scala 137:49]
  wire [36:0] _T_142; // @[Parameters.scala 137:52]
  wire  _T_143; // @[Parameters.scala 137:67]
  wire  _T_162; // @[Monitor.scala 44:11]
  wire  _T_163; // @[Monitor.scala 44:11]
  wire [36:0] _T_301; // @[Parameters.scala 137:52]
  wire  _T_302; // @[Parameters.scala 137:67]
  wire [36:0] _T_331; // @[Parameters.scala 137:52]
  wire  _T_332; // @[Parameters.scala 137:67]
  wire [36:0] _T_360; // @[Parameters.scala 137:52]
  wire  _T_361; // @[Parameters.scala 137:67]
  wire  _T_446; // @[Parameters.scala 552:42]
  wire  _T_447; // @[Parameters.scala 552:42]
  wire  _T_448; // @[Parameters.scala 552:42]
  wire  _T_449; // @[Parameters.scala 552:42]
  wire  _T_450; // @[Parameters.scala 552:42]
  wire  _T_451; // @[Parameters.scala 552:42]
  wire  _T_452; // @[Parameters.scala 552:42]
  wire  _T_453; // @[Parameters.scala 552:42]
  wire  _T_466; // @[Parameters.scala 553:30]
  wire  _T_467; // @[Parameters.scala 553:30]
  wire  _T_469; // @[Monitor.scala 44:11]
  wire  _T_470; // @[Monitor.scala 44:11]
  wire  _T_874; // @[Bundles.scala 44:24]
  wire  _T_876; // @[Monitor.scala 51:11]
  wire  _T_877; // @[Monitor.scala 51:11]
  wire  _T_888; // @[Monitor.scala 307:25]
  wire  _T_892; // @[Monitor.scala 309:27]
  wire  _T_894; // @[Monitor.scala 51:11]
  wire  _T_895; // @[Monitor.scala 51:11]
  wire  _T_896; // @[Monitor.scala 310:28]
  wire  _T_898; // @[Monitor.scala 51:11]
  wire  _T_899; // @[Monitor.scala 51:11]
  wire  _T_900; // @[Monitor.scala 311:15]
  wire  _T_902; // @[Monitor.scala 51:11]
  wire  _T_903; // @[Monitor.scala 51:11]
  wire  _T_904; // @[Monitor.scala 312:15]
  wire  _T_906; // @[Monitor.scala 51:11]
  wire  _T_907; // @[Monitor.scala 51:11]
  wire  _T_908; // @[Monitor.scala 315:25]
  wire  _T_919; // @[Bundles.scala 104:26]
  wire  _T_921; // @[Monitor.scala 51:11]
  wire  _T_922; // @[Monitor.scala 51:11]
  wire  _T_923; // @[Monitor.scala 320:28]
  wire  _T_925; // @[Monitor.scala 51:11]
  wire  _T_926; // @[Monitor.scala 51:11]
  wire  _T_936; // @[Monitor.scala 325:25]
  wire  _T_956; // @[Monitor.scala 331:30]
  wire  _T_958; // @[Monitor.scala 51:11]
  wire  _T_959; // @[Monitor.scala 51:11]
  wire  _T_965; // @[Monitor.scala 335:25]
  wire  _T_982; // @[Monitor.scala 343:25]
  wire  _T_1000; // @[Monitor.scala 351:25]
  wire  _T_1032; // @[Decoupled.scala 40:37]
  reg [5:0] _T_1041; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire [5:0] _T_1043; // @[Edges.scala 231:28]
  wire  _T_1044; // @[Edges.scala 232:25]
  reg [1:0] _T_1055; // @[Monitor.scala 384:22]
  reg [31:0] _RAND_1;
  reg [35:0] _T_1056; // @[Monitor.scala 385:22]
  reg [63:0] _RAND_2;
  wire  _T_1057; // @[Monitor.scala 386:22]
  wire  _T_1058; // @[Monitor.scala 386:19]
  wire  _T_1071; // @[Monitor.scala 390:32]
  wire  _T_1073; // @[Monitor.scala 44:11]
  wire  _T_1074; // @[Monitor.scala 44:11]
  wire  _T_1075; // @[Monitor.scala 391:32]
  wire  _T_1077; // @[Monitor.scala 44:11]
  wire  _T_1078; // @[Monitor.scala 44:11]
  wire  _T_1080; // @[Monitor.scala 393:20]
  wire [22:0] _T_1083; // @[package.scala 189:77]
  wire [7:0] _T_1085; // @[package.scala 189:46]
  reg [5:0] _T_1089; // @[Edges.scala 230:27]
  reg [31:0] _RAND_3;
  wire [5:0] _T_1091; // @[Edges.scala 231:28]
  wire  _T_1092; // @[Edges.scala 232:25]
  reg [2:0] _T_1100; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_4;
  reg [1:0] _T_1101; // @[Monitor.scala 533:22]
  reg [31:0] _RAND_5;
  reg [3:0] _T_1102; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_6;
  reg [1:0] _T_1103; // @[Monitor.scala 535:22]
  reg [31:0] _RAND_7;
  reg [3:0] _T_1104; // @[Monitor.scala 536:22]
  reg [31:0] _RAND_8;
  reg  _T_1105; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_9;
  wire  _T_1106; // @[Monitor.scala 538:22]
  wire  _T_1107; // @[Monitor.scala 538:19]
  wire  _T_1108; // @[Monitor.scala 539:29]
  wire  _T_1110; // @[Monitor.scala 51:11]
  wire  _T_1111; // @[Monitor.scala 51:11]
  wire  _T_1112; // @[Monitor.scala 540:29]
  wire  _T_1114; // @[Monitor.scala 51:11]
  wire  _T_1115; // @[Monitor.scala 51:11]
  wire  _T_1116; // @[Monitor.scala 541:29]
  wire  _T_1118; // @[Monitor.scala 51:11]
  wire  _T_1119; // @[Monitor.scala 51:11]
  wire  _T_1120; // @[Monitor.scala 542:29]
  wire  _T_1122; // @[Monitor.scala 51:11]
  wire  _T_1123; // @[Monitor.scala 51:11]
  wire  _T_1124; // @[Monitor.scala 543:29]
  wire  _T_1126; // @[Monitor.scala 51:11]
  wire  _T_1127; // @[Monitor.scala 51:11]
  wire  _T_1128; // @[Monitor.scala 544:29]
  wire  _T_1130; // @[Monitor.scala 51:11]
  wire  _T_1131; // @[Monitor.scala 51:11]
  wire  _T_1133; // @[Monitor.scala 546:20]
  reg [3:0] _T_1134; // @[Monitor.scala 568:27]
  reg [31:0] _RAND_10;
  reg [5:0] _T_1144; // @[Edges.scala 230:27]
  reg [31:0] _RAND_11;
  wire [5:0] _T_1146; // @[Edges.scala 231:28]
  wire  _T_1147; // @[Edges.scala 232:25]
  reg [5:0] _T_1163; // @[Edges.scala 230:27]
  reg [31:0] _RAND_12;
  wire [5:0] _T_1165; // @[Edges.scala 231:28]
  wire  _T_1166; // @[Edges.scala 232:25]
  wire  _T_1176; // @[Monitor.scala 574:27]
  wire [3:0] _T_1178; // @[OneHot.scala 58:35]
  wire [3:0] _T_1179; // @[Monitor.scala 576:23]
  wire  _T_1181; // @[Monitor.scala 576:14]
  wire  _T_1183; // @[Monitor.scala 576:13]
  wire  _T_1184; // @[Monitor.scala 576:13]
  wire [3:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_1188; // @[Monitor.scala 581:27]
  wire  _T_1190; // @[Monitor.scala 581:75]
  wire  _T_1191; // @[Monitor.scala 581:72]
  wire [3:0] _T_1192; // @[OneHot.scala 58:35]
  wire [3:0] _T_1193; // @[Monitor.scala 583:21]
  wire [3:0] _T_1194; // @[Monitor.scala 583:32]
  wire  _T_1197; // @[Monitor.scala 51:11]
  wire  _T_1198; // @[Monitor.scala 51:11]
  wire [3:0] _GEN_16; // @[Monitor.scala 581:91]
  wire  _T_1199; // @[Monitor.scala 587:20]
  wire  _T_1200; // @[Monitor.scala 587:40]
  wire  _T_1201; // @[Monitor.scala 587:33]
  wire  _T_1202; // @[Monitor.scala 587:30]
  wire  _T_1204; // @[Monitor.scala 51:11]
  wire  _T_1205; // @[Monitor.scala 51:11]
  wire [3:0] _T_1206; // @[Monitor.scala 590:27]
  wire [3:0] _T_1207; // @[Monitor.scala 590:38]
  wire [3:0] _T_1208; // @[Monitor.scala 590:36]
  reg [31:0] _T_1209; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_13;
  wire  _T_1210; // @[Monitor.scala 595:23]
  wire  _T_1211; // @[Monitor.scala 595:13]
  wire  _T_1212; // @[Monitor.scala 595:36]
  wire  _T_1213; // @[Monitor.scala 595:27]
  wire  _T_1214; // @[Monitor.scala 595:56]
  wire  _T_1215; // @[Monitor.scala 595:44]
  wire  _T_1217; // @[Monitor.scala 595:12]
  wire  _T_1218; // @[Monitor.scala 595:12]
  wire [31:0] _T_1220; // @[Monitor.scala 597:26]
  wire  _T_1223; // @[Monitor.scala 598:27]
  wire  _GEN_18; // @[Monitor.scala 51:11]
  wire  _GEN_26; // @[Monitor.scala 51:11]
  wire  _GEN_34; // @[Monitor.scala 51:11]
  wire  _GEN_42; // @[Monitor.scala 51:11]
  wire  _GEN_46; // @[Monitor.scala 51:11]
  wire  _GEN_50; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_17 = io_in_a_bits_address & 36'h3; // @[Edges.scala 22:16]
  assign _T_18 = _T_17 == 36'h0; // @[Edges.scala 22:24]
  assign _T_62 = {1'b0,$signed(io_in_a_bits_address)}; // @[Parameters.scala 137:49]
  assign _T_72 = io_in_a_bits_address ^ 36'h400000000; // @[Parameters.scala 137:31]
  assign _T_73 = {1'b0,$signed(_T_72)}; // @[Parameters.scala 137:49]
  assign _T_75 = $signed(_T_73) & -37'sh400000000; // @[Parameters.scala 137:52]
  assign _T_76 = $signed(_T_75) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_77 = io_in_a_bits_address ^ 36'h8000000; // @[Parameters.scala 137:31]
  assign _T_78 = {1'b0,$signed(_T_77)}; // @[Parameters.scala 137:49]
  assign _T_82 = io_in_a_bits_address ^ 36'h3000; // @[Parameters.scala 137:31]
  assign _T_83 = {1'b0,$signed(_T_82)}; // @[Parameters.scala 137:49]
  assign _T_87 = io_in_a_bits_address ^ 36'hc000000; // @[Parameters.scala 137:31]
  assign _T_88 = {1'b0,$signed(_T_87)}; // @[Parameters.scala 137:49]
  assign _T_90 = $signed(_T_88) & -37'sh4000000; // @[Parameters.scala 137:52]
  assign _T_91 = $signed(_T_90) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_92 = io_in_a_bits_address ^ 36'h2000000; // @[Parameters.scala 137:31]
  assign _T_93 = {1'b0,$signed(_T_92)}; // @[Parameters.scala 137:49]
  assign _T_95 = $signed(_T_93) & -37'sh10000; // @[Parameters.scala 137:52]
  assign _T_96 = $signed(_T_95) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_100 = $signed(_T_62) & -37'sh10001000; // @[Parameters.scala 137:52]
  assign _T_101 = $signed(_T_100) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_102 = io_in_a_bits_address ^ 36'h20000000; // @[Parameters.scala 137:31]
  assign _T_103 = {1'b0,$signed(_T_102)}; // @[Parameters.scala 137:49]
  assign _T_105 = $signed(_T_103) & -37'sh20000000; // @[Parameters.scala 137:52]
  assign _T_106 = $signed(_T_105) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_107 = io_in_a_bits_address ^ 36'h10001000; // @[Parameters.scala 137:31]
  assign _T_108 = {1'b0,$signed(_T_107)}; // @[Parameters.scala 137:49]
  assign _T_112 = io_in_a_bits_address ^ 36'h4000; // @[Parameters.scala 137:31]
  assign _T_113 = {1'b0,$signed(_T_112)}; // @[Parameters.scala 137:49]
  assign _T_115 = $signed(_T_113) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_116 = $signed(_T_115) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_117 = io_in_a_bits_address ^ 36'h2010000; // @[Parameters.scala 137:31]
  assign _T_118 = {1'b0,$signed(_T_117)}; // @[Parameters.scala 137:49]
  assign _T_120 = $signed(_T_118) & -37'sh4000; // @[Parameters.scala 137:52]
  assign _T_121 = $signed(_T_120) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_139 = io_in_a_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  assign _T_140 = {1'b0,$signed(_T_139)}; // @[Parameters.scala 137:49]
  assign _T_142 = $signed(_T_140) & -37'sh800000000; // @[Parameters.scala 137:52]
  assign _T_143 = $signed(_T_142) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_162 = _T_18 | reset; // @[Monitor.scala 44:11]
  assign _T_163 = ~_T_162; // @[Monitor.scala 44:11]
  assign _T_301 = $signed(_T_78) & -37'sh2200000; // @[Parameters.scala 137:52]
  assign _T_302 = $signed(_T_301) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_331 = $signed(_T_108) & -37'sh3000; // @[Parameters.scala 137:52]
  assign _T_332 = $signed(_T_331) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_360 = $signed(_T_83) & -37'sh1000; // @[Parameters.scala 137:52]
  assign _T_361 = $signed(_T_360) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_446 = _T_302 | _T_143; // @[Parameters.scala 552:42]
  assign _T_447 = _T_446 | _T_91; // @[Parameters.scala 552:42]
  assign _T_448 = _T_447 | _T_96; // @[Parameters.scala 552:42]
  assign _T_449 = _T_448 | _T_101; // @[Parameters.scala 552:42]
  assign _T_450 = _T_449 | _T_106; // @[Parameters.scala 552:42]
  assign _T_451 = _T_450 | _T_332; // @[Parameters.scala 552:42]
  assign _T_452 = _T_451 | _T_116; // @[Parameters.scala 552:42]
  assign _T_453 = _T_452 | _T_121; // @[Parameters.scala 552:42]
  assign _T_466 = _T_76 | _T_453; // @[Parameters.scala 553:30]
  assign _T_467 = _T_466 | _T_361; // @[Parameters.scala 553:30]
  assign _T_469 = _T_467 | reset; // @[Monitor.scala 44:11]
  assign _T_470 = ~_T_469; // @[Monitor.scala 44:11]
  assign _T_874 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_876 = _T_874 | reset; // @[Monitor.scala 51:11]
  assign _T_877 = ~_T_876; // @[Monitor.scala 51:11]
  assign _T_888 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_892 = io_in_d_bits_size >= 4'h2; // @[Monitor.scala 309:27]
  assign _T_894 = _T_892 | reset; // @[Monitor.scala 51:11]
  assign _T_895 = ~_T_894; // @[Monitor.scala 51:11]
  assign _T_896 = io_in_d_bits_param == 2'h0; // @[Monitor.scala 310:28]
  assign _T_898 = _T_896 | reset; // @[Monitor.scala 51:11]
  assign _T_899 = ~_T_898; // @[Monitor.scala 51:11]
  assign _T_900 = ~io_in_d_bits_corrupt; // @[Monitor.scala 311:15]
  assign _T_902 = _T_900 | reset; // @[Monitor.scala 51:11]
  assign _T_903 = ~_T_902; // @[Monitor.scala 51:11]
  assign _T_904 = ~io_in_d_bits_denied; // @[Monitor.scala 312:15]
  assign _T_906 = _T_904 | reset; // @[Monitor.scala 51:11]
  assign _T_907 = ~_T_906; // @[Monitor.scala 51:11]
  assign _T_908 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_919 = io_in_d_bits_param <= 2'h2; // @[Bundles.scala 104:26]
  assign _T_921 = _T_919 | reset; // @[Monitor.scala 51:11]
  assign _T_922 = ~_T_921; // @[Monitor.scala 51:11]
  assign _T_923 = io_in_d_bits_param != 2'h2; // @[Monitor.scala 320:28]
  assign _T_925 = _T_923 | reset; // @[Monitor.scala 51:11]
  assign _T_926 = ~_T_925; // @[Monitor.scala 51:11]
  assign _T_936 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_956 = _T_904 | io_in_d_bits_corrupt; // @[Monitor.scala 331:30]
  assign _T_958 = _T_956 | reset; // @[Monitor.scala 51:11]
  assign _T_959 = ~_T_958; // @[Monitor.scala 51:11]
  assign _T_965 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_982 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_1000 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_1032 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_1043 = _T_1041 - 6'h1; // @[Edges.scala 231:28]
  assign _T_1044 = _T_1041 == 6'h0; // @[Edges.scala 232:25]
  assign _T_1057 = ~_T_1044; // @[Monitor.scala 386:22]
  assign _T_1058 = io_in_a_valid & _T_1057; // @[Monitor.scala 386:19]
  assign _T_1071 = io_in_a_bits_source == _T_1055; // @[Monitor.scala 390:32]
  assign _T_1073 = _T_1071 | reset; // @[Monitor.scala 44:11]
  assign _T_1074 = ~_T_1073; // @[Monitor.scala 44:11]
  assign _T_1075 = io_in_a_bits_address == _T_1056; // @[Monitor.scala 391:32]
  assign _T_1077 = _T_1075 | reset; // @[Monitor.scala 44:11]
  assign _T_1078 = ~_T_1077; // @[Monitor.scala 44:11]
  assign _T_1080 = _T_1032 & _T_1044; // @[Monitor.scala 393:20]
  assign _T_1083 = 23'hff << io_in_d_bits_size; // @[package.scala 189:77]
  assign _T_1085 = ~_T_1083[7:0]; // @[package.scala 189:46]
  assign _T_1091 = _T_1089 - 6'h1; // @[Edges.scala 231:28]
  assign _T_1092 = _T_1089 == 6'h0; // @[Edges.scala 232:25]
  assign _T_1106 = ~_T_1092; // @[Monitor.scala 538:22]
  assign _T_1107 = io_in_d_valid & _T_1106; // @[Monitor.scala 538:19]
  assign _T_1108 = io_in_d_bits_opcode == _T_1100; // @[Monitor.scala 539:29]
  assign _T_1110 = _T_1108 | reset; // @[Monitor.scala 51:11]
  assign _T_1111 = ~_T_1110; // @[Monitor.scala 51:11]
  assign _T_1112 = io_in_d_bits_param == _T_1101; // @[Monitor.scala 540:29]
  assign _T_1114 = _T_1112 | reset; // @[Monitor.scala 51:11]
  assign _T_1115 = ~_T_1114; // @[Monitor.scala 51:11]
  assign _T_1116 = io_in_d_bits_size == _T_1102; // @[Monitor.scala 541:29]
  assign _T_1118 = _T_1116 | reset; // @[Monitor.scala 51:11]
  assign _T_1119 = ~_T_1118; // @[Monitor.scala 51:11]
  assign _T_1120 = io_in_d_bits_source == _T_1103; // @[Monitor.scala 542:29]
  assign _T_1122 = _T_1120 | reset; // @[Monitor.scala 51:11]
  assign _T_1123 = ~_T_1122; // @[Monitor.scala 51:11]
  assign _T_1124 = io_in_d_bits_sink == _T_1104; // @[Monitor.scala 543:29]
  assign _T_1126 = _T_1124 | reset; // @[Monitor.scala 51:11]
  assign _T_1127 = ~_T_1126; // @[Monitor.scala 51:11]
  assign _T_1128 = io_in_d_bits_denied == _T_1105; // @[Monitor.scala 544:29]
  assign _T_1130 = _T_1128 | reset; // @[Monitor.scala 51:11]
  assign _T_1131 = ~_T_1130; // @[Monitor.scala 51:11]
  assign _T_1133 = io_in_d_valid & _T_1092; // @[Monitor.scala 546:20]
  assign _T_1146 = _T_1144 - 6'h1; // @[Edges.scala 231:28]
  assign _T_1147 = _T_1144 == 6'h0; // @[Edges.scala 232:25]
  assign _T_1165 = _T_1163 - 6'h1; // @[Edges.scala 231:28]
  assign _T_1166 = _T_1163 == 6'h0; // @[Edges.scala 232:25]
  assign _T_1176 = _T_1032 & _T_1147; // @[Monitor.scala 574:27]
  assign _T_1178 = 4'h1 << io_in_a_bits_source; // @[OneHot.scala 58:35]
  assign _T_1179 = _T_1134 >> io_in_a_bits_source; // @[Monitor.scala 576:23]
  assign _T_1181 = ~_T_1179[0]; // @[Monitor.scala 576:14]
  assign _T_1183 = _T_1181 | reset; // @[Monitor.scala 576:13]
  assign _T_1184 = ~_T_1183; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_1176 ? _T_1178 : 4'h0; // @[Monitor.scala 574:72]
  assign _T_1188 = io_in_d_valid & _T_1166; // @[Monitor.scala 581:27]
  assign _T_1190 = ~_T_888; // @[Monitor.scala 581:75]
  assign _T_1191 = _T_1188 & _T_1190; // @[Monitor.scala 581:72]
  assign _T_1192 = 4'h1 << io_in_d_bits_source; // @[OneHot.scala 58:35]
  assign _T_1193 = _GEN_15 | _T_1134; // @[Monitor.scala 583:21]
  assign _T_1194 = _T_1193 >> io_in_d_bits_source; // @[Monitor.scala 583:32]
  assign _T_1197 = _T_1194[0] | reset; // @[Monitor.scala 51:11]
  assign _T_1198 = ~_T_1197; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_1191 ? _T_1192 : 4'h0; // @[Monitor.scala 581:91]
  assign _T_1199 = _GEN_15 != _GEN_16; // @[Monitor.scala 587:20]
  assign _T_1200 = _GEN_15 != 4'h0; // @[Monitor.scala 587:40]
  assign _T_1201 = ~_T_1200; // @[Monitor.scala 587:33]
  assign _T_1202 = _T_1199 | _T_1201; // @[Monitor.scala 587:30]
  assign _T_1204 = _T_1202 | reset; // @[Monitor.scala 51:11]
  assign _T_1205 = ~_T_1204; // @[Monitor.scala 51:11]
  assign _T_1206 = _T_1134 | _GEN_15; // @[Monitor.scala 590:27]
  assign _T_1207 = ~_GEN_16; // @[Monitor.scala 590:38]
  assign _T_1208 = _T_1206 & _T_1207; // @[Monitor.scala 590:36]
  assign _T_1210 = _T_1134 != 4'h0; // @[Monitor.scala 595:23]
  assign _T_1211 = ~_T_1210; // @[Monitor.scala 595:13]
  assign _T_1212 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_1213 = _T_1211 | _T_1212; // @[Monitor.scala 595:27]
  assign _T_1214 = _T_1209 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_1215 = _T_1213 | _T_1214; // @[Monitor.scala 595:44]
  assign _T_1217 = _T_1215 | reset; // @[Monitor.scala 595:12]
  assign _T_1218 = ~_T_1217; // @[Monitor.scala 595:12]
  assign _T_1220 = _T_1209 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_1223 = _T_1032 | io_in_d_valid; // @[Monitor.scala 598:27]
  assign _GEN_18 = io_in_d_valid & _T_888; // @[Monitor.scala 51:11]
  assign _GEN_26 = io_in_d_valid & _T_908; // @[Monitor.scala 51:11]
  assign _GEN_34 = io_in_d_valid & _T_936; // @[Monitor.scala 51:11]
  assign _GEN_42 = io_in_d_valid & _T_965; // @[Monitor.scala 51:11]
  assign _GEN_46 = io_in_d_valid & _T_982; // @[Monitor.scala 51:11]
  assign _GEN_50 = io_in_d_valid & _T_1000; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_1041 = _RAND_0[5:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_1055 = _RAND_1[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {2{`RANDOM}};
  _T_1056 = _RAND_2[35:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_1089 = _RAND_3[5:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_1100 = _RAND_4[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  _T_1101 = _RAND_5[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_1102 = _RAND_6[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_1103 = _RAND_7[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_1104 = _RAND_8[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_1105 = _RAND_9[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_1134 = _RAND_10[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_1144 = _RAND_11[5:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_1163 = _RAND_12[5:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  _T_1209 = _RAND_13[31:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_1041 <= 6'h0;
    end else if (_T_1032) begin
      if (_T_1044) begin
        _T_1041 <= 6'h0;
      end else begin
        _T_1041 <= _T_1043;
      end
    end
    if (_T_1080) begin
      _T_1055 <= io_in_a_bits_source;
    end
    if (_T_1080) begin
      _T_1056 <= io_in_a_bits_address;
    end
    if (reset) begin
      _T_1089 <= 6'h0;
    end else if (io_in_d_valid) begin
      if (_T_1092) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1089 <= _T_1085[7:2];
        end else begin
          _T_1089 <= 6'h0;
        end
      end else begin
        _T_1089 <= _T_1091;
      end
    end
    if (_T_1133) begin
      _T_1100 <= io_in_d_bits_opcode;
    end
    if (_T_1133) begin
      _T_1101 <= io_in_d_bits_param;
    end
    if (_T_1133) begin
      _T_1102 <= io_in_d_bits_size;
    end
    if (_T_1133) begin
      _T_1103 <= io_in_d_bits_source;
    end
    if (_T_1133) begin
      _T_1104 <= io_in_d_bits_sink;
    end
    if (_T_1133) begin
      _T_1105 <= io_in_d_bits_denied;
    end
    if (reset) begin
      _T_1134 <= 4'h0;
    end else begin
      _T_1134 <= _T_1208;
    end
    if (reset) begin
      _T_1144 <= 6'h0;
    end else if (_T_1032) begin
      if (_T_1147) begin
        _T_1144 <= 6'h0;
      end else begin
        _T_1144 <= _T_1146;
      end
    end
    if (reset) begin
      _T_1163 <= 6'h0;
    end else if (io_in_d_valid) begin
      if (_T_1166) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_1163 <= _T_1085[7:2];
        end else begin
          _T_1163 <= 6'h0;
        end
      end else begin
        _T_1163 <= _T_1165;
      end
    end
    if (reset) begin
      _T_1209 <= 32'h0;
    end else if (_T_1223) begin
      _T_1209 <= 32'h0;
    end else begin
      _T_1209 <= _T_1220;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_a_valid & _T_470) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at Periphery.scala:79:82)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_a_valid & _T_470) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_a_valid & _T_163) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at Periphery.scala:79:82)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_a_valid & _T_163) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_877) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_877) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_895) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_895) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_899) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseeAck carries invalid param (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_899) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_903) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is corrupt (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_903) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_18 & _T_907) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is denied (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_18 & _T_907) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_895) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_895) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_922) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid cap param (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_922) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_926) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries toN param (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_926) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_26 & _T_903) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is corrupt (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_26 & _T_903) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_34 & _T_895) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_34 & _T_895) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_34 & _T_922) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid cap param (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_34 & _T_922) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_34 & _T_926) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries toN param (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_34 & _T_926) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_34 & _T_959) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied but not corrupt (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_34 & _T_959) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_42 & _T_899) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid param (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_42 & _T_899) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_42 & _T_903) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is corrupt (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_42 & _T_903) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_46 & _T_899) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid param (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_46 & _T_899) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_46 & _T_959) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied but not corrupt (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_46 & _T_959) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_50 & _T_899) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid param (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_50 & _T_899) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_50 & _T_903) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is corrupt (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_50 & _T_903) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1058 & _T_1074) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel source changed within multibeat operation (connected at Periphery.scala:79:82)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1058 & _T_1074) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1058 & _T_1078) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at Periphery.scala:79:82)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1058 & _T_1078) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1107 & _T_1111) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1107 & _T_1111) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1107 & _T_1115) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel param changed within multibeat operation (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1107 & _T_1115) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1107 & _T_1119) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1107 & _T_1119) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1107 & _T_1123) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel source changed within multibeat operation (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1107 & _T_1123) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1107 & _T_1127) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel sink changed with multibeat operation (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1107 & _T_1127) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1107 & _T_1131) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1107 & _T_1131) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1176 & _T_1184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at Periphery.scala:79:82)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1176 & _T_1184) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1191 & _T_1198) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1191 & _T_1198) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1205) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' and 'D' concurrent, despite minlatency 4 (connected at Periphery.scala:79:82)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1205) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1218) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at Periphery.scala:79:82)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1218) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
