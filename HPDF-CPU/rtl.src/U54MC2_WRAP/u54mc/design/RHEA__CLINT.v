//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__CLINT(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_int_out_1_0,
  output        auto_int_out_1_1,
  output        auto_int_out_0_0,
  output        auto_int_out_0_1,
  output        auto_in_a_ready,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [2:0]  auto_in_a_bits_param,
  input  [1:0]  auto_in_a_bits_size,
  input  [10:0] auto_in_a_bits_source,
  input  [25:0] auto_in_a_bits_address,
  input  [7:0]  auto_in_a_bits_mask,
  input  [63:0] auto_in_a_bits_data,
  input         auto_in_a_bits_corrupt,
  input         auto_in_d_ready,
  output        auto_in_d_valid,
  output [2:0]  auto_in_d_bits_opcode,
  output [1:0]  auto_in_d_bits_size,
  output [10:0] auto_in_d_bits_source,
  output [63:0] auto_in_d_bits_data,
  input         io_rtcTick
);
`ifdef RANDOMIZE_REG_INIT
  reg [63:0] _RAND_0;
  reg [63:0] _RAND_1;
  reg [63:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
`endif // RANDOMIZE_REG_INIT
  reg [63:0] time_; // @[CLINT.scala 71:23]
  wire [63:0] _time_T_1 = time_ + 64'h1; // @[CLINT.scala 72:38]
  reg [63:0] timecmp_0; // @[CLINT.scala 75:41]
  reg [63:0] timecmp_1; // @[CLINT.scala 75:41]
  reg  ipi_0; // @[CLINT.scala 76:41]
  reg  ipi_1; // @[CLINT.scala 76:41]
  reg  bundleOut_0_0_r; // @[Reg.scala 15:16]
  reg  bundleOut_0_1_r; // @[Reg.scala 15:16]
  reg  bundleOut_1_0_r; // @[Reg.scala 15:16]
  reg  bundleOut_1_1_r; // @[Reg.scala 15:16]
  wire [7:0] oldBytes__0 = timecmp_0[7:0]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes__1 = timecmp_0[15:8]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes__2 = timecmp_0[23:16]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes__3 = timecmp_0[31:24]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes__4 = timecmp_0[39:32]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes__5 = timecmp_0[47:40]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes__6 = timecmp_0[55:48]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes__7 = timecmp_0[63:56]; // @[RegField.scala 151:53]
  wire  in_bits_read = auto_in_a_bits_opcode == 3'h4; // @[CLINT.scala 54:44]
  wire [12:0] in_bits_index = auto_in_a_bits_address[15:3]; // @[CLINT.scala 54:44 CLINT.scala 54:44]
  wire  out_iindex_hi = in_bits_index[11]; // @[CLINT.scala 54:44]
  wire  out_iindex_lo = in_bits_index[0]; // @[CLINT.scala 54:44]
  wire [1:0] out_iindex = {out_iindex_hi,out_iindex_lo}; // @[Cat.scala 30:58]
  wire [12:0] out_findex = in_bits_index & 13'h17fe; // @[CLINT.scala 54:44]
  wire  _out_T_2 = out_findex == 13'h0; // @[CLINT.scala 54:44]
  wire  _out_T_4 = out_findex == 13'h17fe; // @[CLINT.scala 54:44]
  wire  _out_wofireMux_T = auto_in_a_valid & auto_in_d_ready; // @[CLINT.scala 54:44]
  wire [3:0] _out_backSel_T = 4'h1 << out_iindex; // @[OneHot.scala 58:35]
  wire  out_backSel_2 = _out_backSel_T[2]; // @[CLINT.scala 54:44]
  wire  out_woready_0 = _out_wofireMux_T & ~in_bits_read & out_backSel_2 & _out_T_2; // @[CLINT.scala 54:44]
  wire [7:0] out_backMask_hi_hi_hi = auto_in_a_bits_mask[7] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_hi_lo = auto_in_a_bits_mask[6] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_lo_hi = auto_in_a_bits_mask[5] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_lo_lo = auto_in_a_bits_mask[4] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_hi_hi = auto_in_a_bits_mask[3] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_hi_lo = auto_in_a_bits_mask[2] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_lo_hi = auto_in_a_bits_mask[1] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_lo_lo = auto_in_a_bits_mask[0] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [63:0] out_backMask = {out_backMask_hi_hi_hi,out_backMask_hi_hi_lo,out_backMask_hi_lo_hi,out_backMask_hi_lo_lo,
    out_backMask_lo_hi_hi,out_backMask_lo_hi_lo,out_backMask_lo_lo_hi,out_backMask_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [7:0] _out_womask_T = out_backMask[7:0]; // @[CLINT.scala 54:44]
  wire  out_womask = &out_backMask[7:0]; // @[CLINT.scala 54:44]
  wire  out_f_woready = out_woready_0 & out_womask; // @[CLINT.scala 54:44]
  wire [7:0] _out_womask_T_1 = out_backMask[15:8]; // @[CLINT.scala 54:44]
  wire  out_womask_1 = &out_backMask[15:8]; // @[CLINT.scala 54:44]
  wire  out_f_woready_1 = out_woready_0 & out_womask_1; // @[CLINT.scala 54:44]
  wire [7:0] _out_womask_T_2 = out_backMask[23:16]; // @[CLINT.scala 54:44]
  wire  out_womask_2 = &out_backMask[23:16]; // @[CLINT.scala 54:44]
  wire  out_f_woready_2 = out_woready_0 & out_womask_2; // @[CLINT.scala 54:44]
  wire [7:0] _out_womask_T_3 = out_backMask[31:24]; // @[CLINT.scala 54:44]
  wire  out_womask_3 = &out_backMask[31:24]; // @[CLINT.scala 54:44]
  wire  out_f_woready_3 = out_woready_0 & out_womask_3; // @[CLINT.scala 54:44]
  wire [7:0] _out_womask_T_4 = out_backMask[39:32]; // @[CLINT.scala 54:44]
  wire  out_womask_4 = &out_backMask[39:32]; // @[CLINT.scala 54:44]
  wire  out_f_woready_4 = out_woready_0 & out_womask_4; // @[CLINT.scala 54:44]
  wire [7:0] _out_womask_T_5 = out_backMask[47:40]; // @[CLINT.scala 54:44]
  wire  out_womask_5 = &out_backMask[47:40]; // @[CLINT.scala 54:44]
  wire  out_f_woready_5 = out_woready_0 & out_womask_5; // @[CLINT.scala 54:44]
  wire [7:0] _out_womask_T_6 = out_backMask[55:48]; // @[CLINT.scala 54:44]
  wire  out_womask_6 = &out_backMask[55:48]; // @[CLINT.scala 54:44]
  wire  out_f_woready_6 = out_woready_0 & out_womask_6; // @[CLINT.scala 54:44]
  wire [7:0] _out_womask_T_7 = out_backMask[63:56]; // @[CLINT.scala 54:44]
  wire  out_womask_7 = &out_backMask[63:56]; // @[CLINT.scala 54:44]
  wire  out_f_woready_7 = out_woready_0 & out_womask_7; // @[CLINT.scala 54:44]
  wire [7:0] newBytes__1 = out_f_woready_1 ? auto_in_a_bits_data[15:8] : oldBytes__1; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes__0 = out_f_woready ? auto_in_a_bits_data[7:0] : oldBytes__0; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes__3 = out_f_woready_3 ? auto_in_a_bits_data[31:24] : oldBytes__3; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes__2 = out_f_woready_2 ? auto_in_a_bits_data[23:16] : oldBytes__2; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes__5 = out_f_woready_5 ? auto_in_a_bits_data[47:40] : oldBytes__5; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes__4 = out_f_woready_4 ? auto_in_a_bits_data[39:32] : oldBytes__4; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes__7 = out_f_woready_7 ? auto_in_a_bits_data[63:56] : oldBytes__7; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes__6 = out_f_woready_6 ? auto_in_a_bits_data[55:48] : oldBytes__6; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [63:0] _timecmp_0_T = {newBytes__7,newBytes__6,newBytes__5,newBytes__4,newBytes__3,newBytes__2,newBytes__1,
    newBytes__0}; // @[RegField.scala 154:52]
  wire [7:0] oldBytes_1_0 = timecmp_1[7:0]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes_1_1 = timecmp_1[15:8]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes_1_2 = timecmp_1[23:16]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes_1_3 = timecmp_1[31:24]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes_1_4 = timecmp_1[39:32]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes_1_5 = timecmp_1[47:40]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes_1_6 = timecmp_1[55:48]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes_1_7 = timecmp_1[63:56]; // @[RegField.scala 151:53]
  wire  out_backSel_3 = _out_backSel_T[3]; // @[CLINT.scala 54:44]
  wire  out_woready_8 = _out_wofireMux_T & ~in_bits_read & out_backSel_3 & _out_T_2; // @[CLINT.scala 54:44]
  wire  out_f_woready_8 = out_woready_8 & out_womask; // @[CLINT.scala 54:44]
  wire  out_f_woready_9 = out_woready_8 & out_womask_1; // @[CLINT.scala 54:44]
  wire  out_f_woready_10 = out_woready_8 & out_womask_2; // @[CLINT.scala 54:44]
  wire  out_f_woready_11 = out_woready_8 & out_womask_3; // @[CLINT.scala 54:44]
  wire  out_f_woready_12 = out_woready_8 & out_womask_4; // @[CLINT.scala 54:44]
  wire  out_f_woready_13 = out_woready_8 & out_womask_5; // @[CLINT.scala 54:44]
  wire  out_f_woready_14 = out_woready_8 & out_womask_6; // @[CLINT.scala 54:44]
  wire  out_f_woready_15 = out_woready_8 & out_womask_7; // @[CLINT.scala 54:44]
  wire [7:0] newBytes_1_1 = out_f_woready_9 ? auto_in_a_bits_data[15:8] : oldBytes_1_1; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes_1_0 = out_f_woready_8 ? auto_in_a_bits_data[7:0] : oldBytes_1_0; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes_1_3 = out_f_woready_11 ? auto_in_a_bits_data[31:24] : oldBytes_1_3; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes_1_2 = out_f_woready_10 ? auto_in_a_bits_data[23:16] : oldBytes_1_2; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes_1_5 = out_f_woready_13 ? auto_in_a_bits_data[47:40] : oldBytes_1_5; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes_1_4 = out_f_woready_12 ? auto_in_a_bits_data[39:32] : oldBytes_1_4; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes_1_7 = out_f_woready_15 ? auto_in_a_bits_data[63:56] : oldBytes_1_7; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes_1_6 = out_f_woready_14 ? auto_in_a_bits_data[55:48] : oldBytes_1_6; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [63:0] _timecmp_1_T = {newBytes_1_7,newBytes_1_6,newBytes_1_5,newBytes_1_4,newBytes_1_3,newBytes_1_2,newBytes_1_1
    ,newBytes_1_0}; // @[RegField.scala 154:52]
  wire [7:0] oldBytes_2_0 = time_[7:0]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes_2_1 = time_[15:8]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes_2_2 = time_[23:16]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes_2_3 = time_[31:24]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes_2_4 = time_[39:32]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes_2_5 = time_[47:40]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes_2_6 = time_[55:48]; // @[RegField.scala 151:53]
  wire [7:0] oldBytes_2_7 = time_[63:56]; // @[RegField.scala 151:53]
  wire  out_backSel_1 = _out_backSel_T[1]; // @[CLINT.scala 54:44]
  wire  out_woready_16 = _out_wofireMux_T & ~in_bits_read & out_backSel_1 & _out_T_4; // @[CLINT.scala 54:44]
  wire  out_f_woready_16 = out_woready_16 & out_womask; // @[CLINT.scala 54:44]
  wire  out_f_woready_17 = out_woready_16 & out_womask_1; // @[CLINT.scala 54:44]
  wire  out_f_woready_18 = out_woready_16 & out_womask_2; // @[CLINT.scala 54:44]
  wire  out_f_woready_19 = out_woready_16 & out_womask_3; // @[CLINT.scala 54:44]
  wire  out_f_woready_20 = out_woready_16 & out_womask_4; // @[CLINT.scala 54:44]
  wire  out_f_woready_21 = out_woready_16 & out_womask_5; // @[CLINT.scala 54:44]
  wire  out_f_woready_22 = out_woready_16 & out_womask_6; // @[CLINT.scala 54:44]
  wire  out_f_woready_23 = out_woready_16 & out_womask_7; // @[CLINT.scala 54:44]
  wire [7:0] newBytes_2_1 = out_f_woready_17 ? auto_in_a_bits_data[15:8] : oldBytes_2_1; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes_2_0 = out_f_woready_16 ? auto_in_a_bits_data[7:0] : oldBytes_2_0; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes_2_3 = out_f_woready_19 ? auto_in_a_bits_data[31:24] : oldBytes_2_3; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes_2_2 = out_f_woready_18 ? auto_in_a_bits_data[23:16] : oldBytes_2_2; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes_2_5 = out_f_woready_21 ? auto_in_a_bits_data[47:40] : oldBytes_2_5; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes_2_4 = out_f_woready_20 ? auto_in_a_bits_data[39:32] : oldBytes_2_4; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes_2_7 = out_f_woready_23 ? auto_in_a_bits_data[63:56] : oldBytes_2_7; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [7:0] newBytes_2_6 = out_f_woready_22 ? auto_in_a_bits_data[55:48] : oldBytes_2_6; // @[RegField.scala 158:20 RegField.scala 158:33]
  wire [63:0] _time_T_2 = {newBytes_2_7,newBytes_2_6,newBytes_2_5,newBytes_2_4,newBytes_2_3,newBytes_2_2,newBytes_2_1,
    newBytes_2_0}; // @[RegField.scala 154:52]
  wire [63:0] out_prepend_6 = {oldBytes__7,oldBytes__6,oldBytes__5,oldBytes__4,oldBytes__3,oldBytes__2,oldBytes__1,
    oldBytes__0}; // @[Cat.scala 30:58]
  wire [63:0] out_prepend_13 = {oldBytes_1_7,oldBytes_1_6,oldBytes_1_5,oldBytes_1_4,oldBytes_1_3,oldBytes_1_2,
    oldBytes_1_1,oldBytes_1_0}; // @[Cat.scala 30:58]
  wire [63:0] out_prepend_20 = {oldBytes_2_7,oldBytes_2_6,oldBytes_2_5,oldBytes_2_4,oldBytes_2_3,oldBytes_2_2,
    oldBytes_2_1,oldBytes_2_0}; // @[Cat.scala 30:58]
  wire  _out_rimask_T_24 = out_backMask[0]; // @[CLINT.scala 54:44]
  wire  out_wimask_24 = &out_backMask[0]; // @[CLINT.scala 54:44]
  wire  out_frontSel_0 = _out_backSel_T[0]; // @[CLINT.scala 54:44]
  wire  out_wivalid_24 = _out_wofireMux_T & ~in_bits_read & out_frontSel_0 & _out_T_2; // @[CLINT.scala 54:44]
  wire  out_f_wivalid_24 = out_wivalid_24 & out_wimask_24; // @[CLINT.scala 54:44]
  wire [1:0] out_prepend_21 = {1'h0,ipi_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_22 = {{30'd0}, out_prepend_21}; // @[CLINT.scala 54:44]
  wire  _out_rimask_T_26 = out_backMask[32]; // @[CLINT.scala 54:44]
  wire  out_wimask_26 = &out_backMask[32]; // @[CLINT.scala 54:44]
  wire  out_f_wivalid_26 = out_wivalid_24 & out_wimask_26; // @[CLINT.scala 54:44]
  wire [33:0] out_prepend_23 = {1'h0,ipi_1,out_prepend_lo_22}; // @[Cat.scala 30:58]
  wire [63:0] _out_T_526 = {{30'd0}, out_prepend_23}; // @[CLINT.scala 54:44]
  wire  _GEN_51 = 2'h1 == out_iindex ? _out_T_4 : _out_T_2; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_52 = 2'h2 == out_iindex ? _out_T_2 : _GEN_51; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_53 = 2'h3 == out_iindex ? _out_T_2 : _GEN_52; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire [63:0] _GEN_55 = 2'h1 == out_iindex ? out_prepend_20 : _out_T_526; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire [63:0] _GEN_56 = 2'h2 == out_iindex ? out_prepend_6 : _GEN_55; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire [63:0] _GEN_57 = 2'h3 == out_iindex ? out_prepend_13 : _GEN_56; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  assign auto_int_out_1_0 = bundleOut_1_0_r; // @[Nodes.scala 1617:13 CLINT.scala 80:24]
  assign auto_int_out_1_1 = bundleOut_1_1_r; // @[Nodes.scala 1617:13 CLINT.scala 81:24]
  assign auto_int_out_0_0 = bundleOut_0_0_r; // @[Nodes.scala 1617:13 CLINT.scala 80:24]
  assign auto_int_out_0_1 = bundleOut_0_1_r; // @[Nodes.scala 1617:13 CLINT.scala 81:24]
  assign auto_in_a_ready = auto_in_d_ready; // @[CLINT.scala 54:44]
  assign auto_in_d_valid = auto_in_a_valid; // @[CLINT.scala 54:44]
  assign auto_in_d_bits_opcode = {{2'd0}, in_bits_read}; // @[CLINT.scala 54:44 CLINT.scala 54:44]
  assign auto_in_d_bits_size = auto_in_a_bits_size; // @[CLINT.scala 54:44 LazyModule.scala 388:16]
  assign auto_in_d_bits_source = auto_in_a_bits_source; // @[CLINT.scala 54:44 LazyModule.scala 388:16]
  assign auto_in_d_bits_data = _GEN_53 ? _GEN_57 : 64'h0; // @[CLINT.scala 54:44]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      time_ <= 64'h0;
    end else if (out_f_woready_16 | out_f_woready_17 | out_f_woready_18 | out_f_woready_19 | out_f_woready_20 |
      out_f_woready_21 | out_f_woready_22 | out_f_woready_23) begin
      time_ <= _time_T_2;
    end else if (io_rtcTick) begin
      time_ <= _time_T_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      timecmp_0 <= 64'h0;
    end else if (out_f_woready | out_f_woready_1 | out_f_woready_2 | out_f_woready_3 | out_f_woready_4 | out_f_woready_5
       | out_f_woready_6 | out_f_woready_7) begin
      timecmp_0 <= _timecmp_0_T;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      timecmp_1 <= 64'h0;
    end else if (out_f_woready_8 | out_f_woready_9 | out_f_woready_10 | out_f_woready_11 | out_f_woready_12 |
      out_f_woready_13 | out_f_woready_14 | out_f_woready_15) begin
      timecmp_1 <= _timecmp_1_T;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      ipi_0 <= 1'h0;
    end else if (out_f_wivalid_24) begin
      ipi_0 <= auto_in_a_bits_data[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      ipi_1 <= 1'h0;
    end else if (out_f_wivalid_26) begin
      ipi_1 <= auto_in_a_bits_data[32];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleOut_0_0_r <= 1'h0;
    end else begin
      bundleOut_0_0_r <= ipi_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleOut_0_1_r <= 1'h0;
    end else begin
      bundleOut_0_1_r <= time_ >= timecmp_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleOut_1_0_r <= 1'h0;
    end else begin
      bundleOut_1_0_r <= ipi_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleOut_1_1_r <= 1'h0;
    end else begin
      bundleOut_1_1_r <= time_ >= timecmp_1;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {2{`RANDOM}};
  time_ = _RAND_0[63:0];
  _RAND_1 = {2{`RANDOM}};
  timecmp_0 = _RAND_1[63:0];
  _RAND_2 = {2{`RANDOM}};
  timecmp_1 = _RAND_2[63:0];
  _RAND_3 = {1{`RANDOM}};
  ipi_0 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  ipi_1 = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  bundleOut_0_0_r = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  bundleOut_0_1_r = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  bundleOut_1_0_r = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  bundleOut_1_1_r = _RAND_8[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    time_ = 64'h0;
  end
  if (rf_reset) begin
    timecmp_0 = 64'h0;
  end
  if (rf_reset) begin
    timecmp_1 = 64'h0;
  end
  if (reset) begin
    ipi_0 = 1'h0;
  end
  if (reset) begin
    ipi_1 = 1'h0;
  end
  if (rf_reset) begin
    bundleOut_0_0_r = 1'h0;
  end
  if (rf_reset) begin
    bundleOut_0_1_r = 1'h0;
  end
  if (rf_reset) begin
    bundleOut_1_0_r = 1'h0;
  end
  if (rf_reset) begin
    bundleOut_1_1_r = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
