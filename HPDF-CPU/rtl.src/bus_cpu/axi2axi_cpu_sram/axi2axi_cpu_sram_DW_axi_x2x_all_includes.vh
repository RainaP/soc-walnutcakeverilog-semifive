/* ---------------------------------------------------------------------
// ------------------------------------------------------------------------------
// 
// Copyright 2006 - 2020 Synopsys, INC.
// 
// This Synopsys IP and all associated documentation are proprietary to
// Synopsys, Inc. and may only be used pursuant to the terms and conditions of a
// written license agreement with Synopsys, Inc. All other use, reproduction,
// modification, or distribution of the Synopsys IP or the associated
// documentation is strictly prohibited.
// 
// Component Name   : DW_axi_x2x
// Component Version: 1.08a
// Release Type     : GA
// ------------------------------------------------------------------------------

// 
// Release version :  1.08a
// File Version     :        $Revision: #1 $ 
// Revision: $Id: //dwh/DW_ocb/DW_axi_x2x/amba_dev/src/DW_axi_x2x_all_includes.vh#1 $ 
// ---------------------------------------------------------------------
*/

//==============================================================================
// Start Guard: prevent re-compilation of includes
//==============================================================================
  `define axi2axi_cpu_sram___GUARD__DW_AXI_X2X_ALL_INCLUDES__VH__
`include "./axi2axi_cpu_sram/axi2axi_cpu_sram_DW_axi_x2x_cc_constants.vh"
`include "./axi2axi_cpu_sram/axi2axi_cpu_sram_DW_axi_x2x_constants.vh"
`include "./axi2axi_cpu_sram/axi2axi_cpu_sram_DW_axi_x2x_bcm_params.vh"
//==============================================================================
// End Guard
//==============================================================================  
