//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLTraceEncoder_1(
  input         clock,
  input         reset,
  input         auto_teSinkXbar_out_a_ready,
  output        auto_teSinkXbar_out_a_valid,
  output [31:0] auto_teSinkXbar_out_a_bits_address,
  output [31:0] auto_teSinkXbar_out_a_bits_data,
  input         auto_teSinkXbar_out_d_valid,
  input         auto_teSinkXbar_out_d_bits_denied,
  input  [39:0] auto_traceTimestamp_timestamp_sink_in,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_0_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_0_action,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_1_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_1_action,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_2_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_2_action,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_3_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_3_action,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_4_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_4_action,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_5_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_5_action,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_6_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_6_action,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_7_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_7_action,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_8_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_8_action,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_9_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_9_action,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_10_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_10_action,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_11_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_11_action,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_12_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_12_action,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_13_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_13_action,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_14_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_14_action,
  input         auto_traceFrontend_traceTrigBlock_bpwatch_in_15_valid_0,
  input  [2:0]  auto_traceFrontend_traceTrigBlock_bpwatch_in_15_action,
  input         auto_traceFrontend_traceInBlock_trace_legacy_in_0_valid,
  input  [39:0] auto_traceFrontend_traceInBlock_trace_legacy_in_0_iaddr,
  input  [31:0] auto_traceFrontend_traceInBlock_trace_legacy_in_0_insn,
  input  [2:0]  auto_traceFrontend_traceInBlock_trace_legacy_in_0_priv,
  input         auto_traceFrontend_traceInBlock_trace_legacy_in_0_exception,
  input         auto_traceFrontend_traceInBlock_trace_legacy_in_0_interrupt,
  output        auto_traceFrontend_trace_aux_out_stall,
  output        auto_reg_in_a_ready,
  input         auto_reg_in_a_valid,
  input  [2:0]  auto_reg_in_a_bits_opcode,
  input  [2:0]  auto_reg_in_a_bits_param,
  input  [1:0]  auto_reg_in_a_bits_size,
  input  [10:0] auto_reg_in_a_bits_source,
  input  [28:0] auto_reg_in_a_bits_address,
  input  [7:0]  auto_reg_in_a_bits_mask,
  input  [63:0] auto_reg_in_a_bits_data,
  input         auto_reg_in_a_bits_corrupt,
  input         auto_reg_in_d_ready,
  output        auto_reg_in_d_valid,
  output [2:0]  auto_reg_in_d_bits_opcode,
  output [1:0]  auto_reg_in_d_bits_size,
  output [10:0] auto_reg_in_d_bits_source,
  output [63:0] auto_reg_in_d_bits_data,
  input         io_clock,
  input         io_reset,
  input         hartIsInReset
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
`endif // RANDOMIZE_REG_INIT
  wire  rf_reset;
  wire  traceFrontend_clock; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_reset; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_0_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_0_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_1_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_1_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_2_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_2_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_3_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_3_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_4_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_4_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_5_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_5_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_6_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_6_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_7_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_7_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_8_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_8_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_9_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_9_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_10_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_10_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_11_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_11_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_12_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_12_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_13_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_13_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_14_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_14_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceTrigBlock_bpwatch_in_15_valid_0; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceTrigBlock_bpwatch_in_15_action; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceInBlock_trace_legacy_in_0_valid; // @[TraceEncoder.scala 237:34]
  wire [39:0] traceFrontend_auto_traceInBlock_trace_legacy_in_0_iaddr; // @[TraceEncoder.scala 237:34]
  wire [31:0] traceFrontend_auto_traceInBlock_trace_legacy_in_0_insn; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_auto_traceInBlock_trace_legacy_in_0_priv; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceInBlock_trace_legacy_in_0_exception; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_traceInBlock_trace_legacy_in_0_interrupt; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_auto_trace_aux_out_stall; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_teActive; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_tlClock; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_tlReset; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_hartIsInReset; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_ingControlAsync_mem_0_teEnable; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_ingControlAsync_mem_0_teTracing; // @[TraceEncoder.scala 237:34]
  wire [2:0] traceFrontend_io_ingControlAsync_mem_0_teInstruction; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_ingControlAsync_mem_0_teStallEnable; // @[TraceEncoder.scala 237:34]
  wire [3:0] traceFrontend_io_ingControlAsync_mem_0_teSyncMaxInst; // @[TraceEncoder.scala 237:34]
  wire [3:0] traceFrontend_io_ingControlAsync_mem_0_teSyncMaxBTM; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_ingControlAsync_ridx; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_ingControlAsync_widx; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_traceOn; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_teIdle; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_traceStall; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_pcActive; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_pcCapture_valid; // @[TraceEncoder.scala 237:34]
  wire [38:0] traceFrontend_io_pcCapture_bits; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_pcSample_valid; // @[TraceEncoder.scala 237:34]
  wire [38:0] traceFrontend_io_pcSample_bits; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_btmEvent_ready; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_btmEvent_valid; // @[TraceEncoder.scala 237:34]
  wire [4:0] traceFrontend_io_btmEvent_bits_reason; // @[TraceEncoder.scala 237:34]
  wire [38:0] traceFrontend_io_btmEvent_bits_iaddr; // @[TraceEncoder.scala 237:34]
  wire [13:0] traceFrontend_io_btmEvent_bits_icnt; // @[TraceEncoder.scala 237:34]
  wire [31:0] traceFrontend_io_btmEvent_bits_hist; // @[TraceEncoder.scala 237:34]
  wire [1:0] traceFrontend_io_btmEvent_bits_hisv; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_0; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_1; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_2; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_3; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_4; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_5; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_6; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_7; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_8; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_9; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_10; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_11; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_12; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_13; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_14; // @[TraceEncoder.scala 237:34]
  wire  traceFrontend_io_wpControl_mem_0_15; // @[TraceEncoder.scala 237:34]
  wire  traceSink_clock; // @[TraceEncoder.scala 238:34]
  wire  traceSink_reset; // @[TraceEncoder.scala 238:34]
  wire  traceSink_auto_sink_out_a_ready; // @[TraceEncoder.scala 238:34]
  wire  traceSink_auto_sink_out_a_valid; // @[TraceEncoder.scala 238:34]
  wire [31:0] traceSink_auto_sink_out_a_bits_address; // @[TraceEncoder.scala 238:34]
  wire [31:0] traceSink_auto_sink_out_a_bits_data; // @[TraceEncoder.scala 238:34]
  wire  traceSink_auto_sink_out_d_valid; // @[TraceEncoder.scala 238:34]
  wire  traceSink_auto_sink_out_d_bits_denied; // @[TraceEncoder.scala 238:34]
  wire  traceSink_io_teActive; // @[TraceEncoder.scala 238:34]
  wire [3:0] traceSink_io_teControl_teSink; // @[TraceEncoder.scala 238:34]
  wire  traceSink_io_stream_ready; // @[TraceEncoder.scala 238:34]
  wire  traceSink_io_stream_valid; // @[TraceEncoder.scala 238:34]
  wire [31:0] traceSink_io_stream_bits_data; // @[TraceEncoder.scala 238:34]
  wire  traceSink_io_sinkError; // @[TraceEncoder.scala 238:34]
  wire [39:0] traceTimestamp_auto_timestamp_sink_in; // @[TraceEncoder.scala 240:58]
  wire [39:0] traceTimestamp_io_timestamp; // @[TraceEncoder.scala 240:58]
  wire [1:0] traceTimestamp_io_tsControlIn_tsBranch; // @[TraceEncoder.scala 240:58]
  wire  traceTimestamp_io_tsControlIn_tsEnable; // @[TraceEncoder.scala 240:58]
  wire [1:0] traceTimestamp_io_tsControl_tsBranch; // @[TraceEncoder.scala 240:58]
  wire  traceTimestamp_io_tsControl_tsEnable; // @[TraceEncoder.scala 240:58]
  wire  teSinkXbar_auto_in_a_ready;
  wire  teSinkXbar_auto_in_a_valid;
  wire [31:0] teSinkXbar_auto_in_a_bits_address;
  wire [31:0] teSinkXbar_auto_in_a_bits_data;
  wire  teSinkXbar_auto_in_d_valid;
  wire  teSinkXbar_auto_in_d_bits_denied;
  wire  teSinkXbar_auto_out_a_ready;
  wire  teSinkXbar_auto_out_a_valid;
  wire [31:0] teSinkXbar_auto_out_a_bits_address;
  wire [31:0] teSinkXbar_auto_out_a_bits_data;
  wire  teSinkXbar_auto_out_d_valid;
  wire  teSinkXbar_auto_out_d_bits_denied;
  wire  traceBTM_rf_reset; // @[TraceEncoder.scala 288:29]
  wire  traceBTM_clock; // @[TraceEncoder.scala 288:29]
  wire  traceBTM_reset; // @[TraceEncoder.scala 288:29]
  wire  traceBTM_io_teActive; // @[TraceEncoder.scala 288:29]
  wire [3:0] traceBTM_io_teControl_teSyncMaxBTM; // @[TraceEncoder.scala 288:29]
  wire  traceBTM_io_teControl_teInhibitSrc; // @[TraceEncoder.scala 288:29]
  wire [2:0] traceBTM_io_teControl_teInstruction; // @[TraceEncoder.scala 288:29]
  wire  traceBTM_io_teControl_teEnable; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_tsControl_tsBranch; // @[TraceEncoder.scala 288:29]
  wire  traceBTM_io_tsControl_tsEnable; // @[TraceEncoder.scala 288:29]
  wire  traceBTM_io_btmEvent_ready; // @[TraceEncoder.scala 288:29]
  wire  traceBTM_io_btmEvent_valid; // @[TraceEncoder.scala 288:29]
  wire [4:0] traceBTM_io_btmEvent_bits_reason; // @[TraceEncoder.scala 288:29]
  wire [38:0] traceBTM_io_btmEvent_bits_iaddr; // @[TraceEncoder.scala 288:29]
  wire [13:0] traceBTM_io_btmEvent_bits_icnt; // @[TraceEncoder.scala 288:29]
  wire [31:0] traceBTM_io_btmEvent_bits_hist; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btmEvent_bits_hisv; // @[TraceEncoder.scala 288:29]
  wire  traceBTM_io_btm_ready; // @[TraceEncoder.scala 288:29]
  wire  traceBTM_io_btm_valid; // @[TraceEncoder.scala 288:29]
  wire [4:0] traceBTM_io_btm_bits_length; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_timestamp; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_0_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_0_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_1_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_1_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_2_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_2_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_3_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_3_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_4_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_4_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_5_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_5_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_6_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_6_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_7_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_7_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_8_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_8_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_9_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_9_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_10_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_10_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_11_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_11_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_12_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_12_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_13_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_13_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_14_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_14_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_15_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_15_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_16_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_16_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_17_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_17_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_18_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_18_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_19_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_19_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_20_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_20_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_21_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_21_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_22_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_22_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_23_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_23_mseo; // @[TraceEncoder.scala 288:29]
  wire [5:0] traceBTM_io_btm_bits_slices_24_data; // @[TraceEncoder.scala 288:29]
  wire [1:0] traceBTM_io_btm_bits_slices_24_mseo; // @[TraceEncoder.scala 288:29]
  wire  tracePacker_clock; // @[TraceEncoder.scala 289:29]
  wire  tracePacker_reset; // @[TraceEncoder.scala 289:29]
  wire  tracePacker_io_teActive; // @[TraceEncoder.scala 289:29]
  wire  tracePacker_io_btm_ready; // @[TraceEncoder.scala 289:29]
  wire  tracePacker_io_btm_valid; // @[TraceEncoder.scala 289:29]
  wire [4:0] tracePacker_io_btm_bits_length; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_timestamp; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_0_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_0_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_1_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_1_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_2_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_2_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_3_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_3_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_4_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_4_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_5_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_5_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_6_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_6_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_7_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_7_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_8_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_8_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_9_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_9_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_10_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_10_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_11_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_11_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_12_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_12_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_13_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_13_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_14_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_14_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_15_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_15_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_16_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_16_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_17_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_17_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_18_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_18_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_19_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_19_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_20_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_20_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_21_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_21_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_22_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_22_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_23_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_23_mseo; // @[TraceEncoder.scala 289:29]
  wire [5:0] tracePacker_io_btm_bits_slices_24_data; // @[TraceEncoder.scala 289:29]
  wire [1:0] tracePacker_io_btm_bits_slices_24_mseo; // @[TraceEncoder.scala 289:29]
  wire [39:0] tracePacker_io_timestamp; // @[TraceEncoder.scala 289:29]
  wire  tracePacker_io_stream_ready; // @[TraceEncoder.scala 289:29]
  wire  tracePacker_io_stream_valid; // @[TraceEncoder.scala 289:29]
  wire [31:0] tracePacker_io_stream_bits_data; // @[TraceEncoder.scala 289:29]
  wire  traceGapper_io_streamIn_ready; // @[TraceEncoder.scala 290:29]
  wire  traceGapper_io_streamIn_valid; // @[TraceEncoder.scala 290:29]
  wire [31:0] traceGapper_io_streamIn_bits_data; // @[TraceEncoder.scala 290:29]
  wire  traceGapper_io_streamOut_ready; // @[TraceEncoder.scala 290:29]
  wire  traceGapper_io_streamOut_valid; // @[TraceEncoder.scala 290:29]
  wire [31:0] traceGapper_io_streamOut_bits_data; // @[TraceEncoder.scala 290:29]
  wire  tracePCSample_clock; // @[TraceEncoder.scala 291:54]
  wire  tracePCSample_reset; // @[TraceEncoder.scala 291:54]
  wire  tracePCSample_io_pcControlReg_pcActive; // @[TraceEncoder.scala 291:54]
  wire  tracePCSample_io_pcSampleRdEn; // @[TraceEncoder.scala 291:54]
  wire  tracePCSample_io_pcSampleHRdEn; // @[TraceEncoder.scala 291:54]
  wire  tracePCSample_io_ingPcCapture_valid; // @[TraceEncoder.scala 291:54]
  wire [38:0] tracePCSample_io_ingPcCapture_bits; // @[TraceEncoder.scala 291:54]
  wire  tracePCSample_io_ingPcSample_valid; // @[TraceEncoder.scala 291:54]
  wire [38:0] tracePCSample_io_ingPcSample_bits; // @[TraceEncoder.scala 291:54]
  wire  tracePCSample_io_pcCaptureValid; // @[TraceEncoder.scala 291:54]
  wire  tracePCSample_io_pcSampleValid; // @[TraceEncoder.scala 291:54]
  wire [38:0] tracePCSample_io_pcCapture; // @[TraceEncoder.scala 291:54]
  wire [38:0] tracePCSample_io_pcSample; // @[TraceEncoder.scala 291:54]
  wire  tracePCSample_io_pcActive; // @[TraceEncoder.scala 291:54]
  wire  traceFrontend_io_ingControlAsync_source_rf_reset; // @[AsyncQueue.scala 216:24]
  wire  traceFrontend_io_ingControlAsync_source_clock; // @[AsyncQueue.scala 216:24]
  wire  traceFrontend_io_ingControlAsync_source_reset; // @[AsyncQueue.scala 216:24]
  wire  traceFrontend_io_ingControlAsync_source_io_enq_ready; // @[AsyncQueue.scala 216:24]
  wire  traceFrontend_io_ingControlAsync_source_io_enq_valid; // @[AsyncQueue.scala 216:24]
  wire  traceFrontend_io_ingControlAsync_source_io_enq_bits_teEnable; // @[AsyncQueue.scala 216:24]
  wire  traceFrontend_io_ingControlAsync_source_io_enq_bits_teTracing; // @[AsyncQueue.scala 216:24]
  wire [2:0] traceFrontend_io_ingControlAsync_source_io_enq_bits_teInstruction; // @[AsyncQueue.scala 216:24]
  wire  traceFrontend_io_ingControlAsync_source_io_enq_bits_teStallEnable; // @[AsyncQueue.scala 216:24]
  wire [3:0] traceFrontend_io_ingControlAsync_source_io_enq_bits_teSyncMaxInst; // @[AsyncQueue.scala 216:24]
  wire [3:0] traceFrontend_io_ingControlAsync_source_io_enq_bits_teSyncMaxBTM; // @[AsyncQueue.scala 216:24]
  wire  traceFrontend_io_ingControlAsync_source_io_async_mem_0_teEnable; // @[AsyncQueue.scala 216:24]
  wire  traceFrontend_io_ingControlAsync_source_io_async_mem_0_teTracing; // @[AsyncQueue.scala 216:24]
  wire [2:0] traceFrontend_io_ingControlAsync_source_io_async_mem_0_teInstruction; // @[AsyncQueue.scala 216:24]
  wire  traceFrontend_io_ingControlAsync_source_io_async_mem_0_teStallEnable; // @[AsyncQueue.scala 216:24]
  wire [3:0] traceFrontend_io_ingControlAsync_source_io_async_mem_0_teSyncMaxInst; // @[AsyncQueue.scala 216:24]
  wire [3:0] traceFrontend_io_ingControlAsync_source_io_async_mem_0_teSyncMaxBTM; // @[AsyncQueue.scala 216:24]
  wire  traceFrontend_io_ingControlAsync_source_io_async_ridx; // @[AsyncQueue.scala 216:24]
  wire  traceFrontend_io_ingControlAsync_source_io_async_widx; // @[AsyncQueue.scala 216:24]
  reg [3:0] teControlReg_teSink; // @[TraceEncoder.scala 301:37]
  reg  teControlReg_teSinkError; // @[TraceEncoder.scala 301:37]
  reg [3:0] teControlReg_teSyncMaxInst; // @[TraceEncoder.scala 301:37]
  reg [3:0] teControlReg_teSyncMaxBTM; // @[TraceEncoder.scala 301:37]
  reg  teControlReg_teInhibitSrc; // @[TraceEncoder.scala 301:37]
  reg  teControlReg_teStallEnable; // @[TraceEncoder.scala 301:37]
  reg  teControlReg_teStallOccurred; // @[TraceEncoder.scala 301:37]
  reg [2:0] teControlReg_teInstruction; // @[TraceEncoder.scala 301:37]
  reg  teControlReg_teTracing; // @[TraceEncoder.scala 301:37]
  reg  teControlReg_teEnable; // @[TraceEncoder.scala 301:37]
  reg  teControlReg_teActive; // @[TraceEncoder.scala 301:37]
  wire  teControlWrData_teActive = auto_reg_in_a_bits_data[0]; // @[TraceEncoder.scala 222:31]
  wire  _T = ~teControlWrData_teActive; // @[TraceEncoder.scala 329:26]
  wire  in_bits_read = auto_reg_in_a_bits_opcode == 3'h4; // @[TraceEncoder.scala 222:31]
  wire [8:0] in_bits_index = auto_reg_in_a_bits_address[11:3]; // @[TraceEncoder.scala 222:31 TraceEncoder.scala 222:31]
  wire  out_iindex_hi_hi = in_bits_index[5]; // @[TraceEncoder.scala 222:31]
  wire  out_iindex_hi_lo = in_bits_index[3]; // @[TraceEncoder.scala 222:31]
  wire  out_iindex_lo_hi = in_bits_index[1]; // @[TraceEncoder.scala 222:31]
  wire  out_iindex_lo_lo = in_bits_index[0]; // @[TraceEncoder.scala 222:31]
  wire [3:0] out_iindex = {out_iindex_hi_hi,out_iindex_hi_lo,out_iindex_lo_hi,out_iindex_lo_lo}; // @[Cat.scala 30:58]
  wire [8:0] out_findex = in_bits_index & 9'h1d4; // @[TraceEncoder.scala 222:31]
  wire  _out_T_12 = out_findex == 9'h4; // @[TraceEncoder.scala 222:31]
  wire  _out_T_4 = out_findex == 9'h0; // @[TraceEncoder.scala 222:31]
  wire [15:0] _out_backSel_T = 16'h1 << out_iindex; // @[OneHot.scala 58:35]
  wire  out_backSel_0 = _out_backSel_T[0]; // @[TraceEncoder.scala 222:31]
  wire  out_woready_0 = auto_reg_in_a_valid & auto_reg_in_d_ready & ~in_bits_read & out_backSel_0 & _out_T_4; // @[TraceEncoder.scala 222:31]
  wire [7:0] out_backMask_hi_hi_hi = auto_reg_in_a_bits_mask[7] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_hi_lo = auto_reg_in_a_bits_mask[6] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_lo_hi = auto_reg_in_a_bits_mask[5] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_lo_lo = auto_reg_in_a_bits_mask[4] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_hi_hi = auto_reg_in_a_bits_mask[3] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_hi_lo = auto_reg_in_a_bits_mask[2] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_lo_hi = auto_reg_in_a_bits_mask[1] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_lo_lo = auto_reg_in_a_bits_mask[0] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [63:0] out_backMask = {out_backMask_hi_hi_hi,out_backMask_hi_hi_lo,out_backMask_hi_lo_hi,out_backMask_hi_lo_lo,
    out_backMask_lo_hi_hi,out_backMask_lo_hi_lo,out_backMask_lo_lo_hi,out_backMask_lo_lo_lo}; // @[Cat.scala 30:58]
  wire  _out_womask_T = out_backMask[0]; // @[TraceEncoder.scala 222:31]
  wire  out_womask = &out_backMask[0]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready = out_woready_0 & out_womask; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_1 = out_backMask[1]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_1 = &out_backMask[1]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_1 = out_woready_0 & out_womask_1; // @[TraceEncoder.scala 222:31]
  wire  teControlWrData_teEnable = auto_reg_in_a_bits_data[1]; // @[TraceEncoder.scala 222:31]
  wire  teControlWrData_teTracing = auto_reg_in_a_bits_data[2]; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_7 = out_backMask[12]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_7 = &out_backMask[12]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_7 = out_woready_0 & out_womask_7; // @[TraceEncoder.scala 222:31]
  wire [2:0] teControlWrData_teInstruction = auto_reg_in_a_bits_data[6:4]; // @[TraceEncoder.scala 222:31]
  wire [2:0] _out_womask_T_4 = out_backMask[6:4]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_4 = &out_backMask[6:4]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_4 = out_woready_0 & out_womask_4; // @[TraceEncoder.scala 222:31]
  wire  teControlWrData_teStallOccurred = auto_reg_in_a_bits_data[12]; // @[TraceEncoder.scala 222:31]
  wire [3:0] teControlWrData_teSyncMaxBTM = auto_reg_in_a_bits_data[19:16]; // @[TraceEncoder.scala 222:31]
  wire [3:0] _out_womask_T_11 = out_backMask[19:16]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_11 = &out_backMask[19:16]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_11 = out_woready_0 & out_womask_11; // @[TraceEncoder.scala 222:31]
  wire [3:0] teControlWrData_teSyncMaxInst = auto_reg_in_a_bits_data[23:20]; // @[TraceEncoder.scala 222:31]
  wire [3:0] _out_womask_T_12 = out_backMask[23:20]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_12 = &out_backMask[23:20]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_12 = out_woready_0 & out_womask_12; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_14 = out_backMask[27]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_14 = &out_backMask[27]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_14 = out_woready_0 & out_womask_14; // @[TraceEncoder.scala 222:31]
  wire  teControlWrData_teSinkError = auto_reg_in_a_bits_data[27]; // @[TraceEncoder.scala 222:31]
  wire  _GEN_8 = traceSink_io_sinkError | teControlReg_teSinkError; // @[TraceEncoder.scala 360:49 TraceEncoder.scala 360:76 TraceEncoder.scala 301:37]
  wire [3:0] teControlWrData_teSink = auto_reg_in_a_bits_data[31:28]; // @[TraceEncoder.scala 222:31]
  wire [3:0] _out_womask_T_15 = out_backMask[31:28]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_15 = &out_backMask[31:28]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_15 = out_woready_0 & out_womask_15; // @[TraceEncoder.scala 222:31]
  wire  _GEN_18 = out_f_woready & ~teControlWrData_teActive ? 1'h0 : teControlReg_teStallOccurred; // @[TraceEncoder.scala 329:53 TraceEncoder.scala 330:20]
  reg  ingControlValid; // @[TraceEncoder.scala 474:37]
  reg  traceOnReg; // @[TraceEncoder.scala 475:37]
  wire  _T_16 = ~teControlReg_teActive; // @[TraceEncoder.scala 476:11]
  wire  _T_22 = tracePacker_io_btm_valid & tracePacker_io_btm_ready & tracePacker_io_btm_bits_slices_0_data == 6'h8; // @[TraceEncoder.scala 494:64]
  wire  _ingControlValid_T_2 = out_f_woready_1 | out_f_woready_7 | out_f_woready_4 | out_f_woready_12; // @[TraceEncoder.scala 502:75]
  wire  ingControl_ready = traceFrontend_io_ingControlAsync_source_io_enq_ready; // @[TraceEncoder.scala 473:26 AsyncQueue.scala 217:19]
  wire  _ingControlValid_T_8 = ingControlValid & ~ingControl_ready; // @[TraceEncoder.scala 504:26]
  wire  _ingControlValid_T_9 = _ingControlValid_T_2 | _ingControlValid_T_8; // @[TraceEncoder.scala 503:114]
  wire  _traceFrontend_io_teIdle_T_2 = ~traceBTM_io_btmEvent_valid & ~tracePacker_io_stream_valid; // @[TraceEncoder.scala 607:67]
  reg  wpControlReg_0; // @[TraceFrontend.scala 117:36]
  reg  wpControlReg_1; // @[TraceFrontend.scala 117:36]
  reg  wpControlReg_2; // @[TraceFrontend.scala 117:36]
  reg  wpControlReg_3; // @[TraceFrontend.scala 117:36]
  reg  wpControlReg_4; // @[TraceFrontend.scala 117:36]
  reg  wpControlReg_5; // @[TraceFrontend.scala 117:36]
  reg  wpControlReg_6; // @[TraceFrontend.scala 117:36]
  reg  wpControlReg_7; // @[TraceFrontend.scala 117:36]
  reg  wpControlReg_8; // @[TraceFrontend.scala 117:36]
  reg  wpControlReg_9; // @[TraceFrontend.scala 117:36]
  reg  wpControlReg_10; // @[TraceFrontend.scala 117:36]
  reg  wpControlReg_11; // @[TraceFrontend.scala 117:36]
  reg  wpControlReg_12; // @[TraceFrontend.scala 117:36]
  reg  wpControlReg_13; // @[TraceFrontend.scala 117:36]
  reg  wpControlReg_14; // @[TraceFrontend.scala 117:36]
  reg  wpControlReg_15; // @[TraceFrontend.scala 117:36]
  wire  out_backSel_7 = _out_backSel_T[7]; // @[TraceEncoder.scala 222:31]
  wire  out_woready_34 = auto_reg_in_a_valid & auto_reg_in_d_ready & ~in_bits_read & out_backSel_7 & _out_T_4; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_34 = out_woready_34 & out_womask; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_35 = out_woready_34 & out_womask_1; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_36 = out_backMask[2]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_36 = &out_backMask[2]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_36 = out_woready_34 & out_womask_36; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_37 = out_backMask[3]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_37 = &out_backMask[3]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_37 = out_woready_34 & out_womask_37; // @[TraceEncoder.scala 222:31]
  wire  wpControlWrData_3 = auto_reg_in_a_bits_data[3]; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_38 = out_backMask[4]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_38 = &out_backMask[4]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_38 = out_woready_34 & out_womask_38; // @[TraceEncoder.scala 222:31]
  wire  wpControlWrData_4 = auto_reg_in_a_bits_data[4]; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_39 = out_backMask[5]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_39 = &out_backMask[5]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_39 = out_woready_34 & out_womask_39; // @[TraceEncoder.scala 222:31]
  wire  wpControlWrData_5 = auto_reg_in_a_bits_data[5]; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_40 = out_backMask[6]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_40 = &out_backMask[6]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_40 = out_woready_34 & out_womask_40; // @[TraceEncoder.scala 222:31]
  wire  wpControlWrData_6 = auto_reg_in_a_bits_data[6]; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_41 = out_backMask[7]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_41 = &out_backMask[7]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_41 = out_woready_34 & out_womask_41; // @[TraceEncoder.scala 222:31]
  wire  wpControlWrData_7 = auto_reg_in_a_bits_data[7]; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_42 = out_backMask[8]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_42 = &out_backMask[8]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_42 = out_woready_34 & out_womask_42; // @[TraceEncoder.scala 222:31]
  wire  wpControlWrData_8 = auto_reg_in_a_bits_data[8]; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_43 = out_backMask[9]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_43 = &out_backMask[9]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_43 = out_woready_34 & out_womask_43; // @[TraceEncoder.scala 222:31]
  wire  wpControlWrData_9 = auto_reg_in_a_bits_data[9]; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_44 = out_backMask[10]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_44 = &out_backMask[10]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_44 = out_woready_34 & out_womask_44; // @[TraceEncoder.scala 222:31]
  wire  wpControlWrData_10 = auto_reg_in_a_bits_data[10]; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_45 = out_backMask[11]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_45 = &out_backMask[11]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_45 = out_woready_34 & out_womask_45; // @[TraceEncoder.scala 222:31]
  wire  wpControlWrData_11 = auto_reg_in_a_bits_data[11]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_46 = out_woready_34 & out_womask_7; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_47 = out_backMask[13]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_47 = &out_backMask[13]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_47 = out_woready_34 & out_womask_47; // @[TraceEncoder.scala 222:31]
  wire  wpControlWrData_13 = auto_reg_in_a_bits_data[13]; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_48 = out_backMask[14]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_48 = &out_backMask[14]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_48 = out_woready_34 & out_womask_48; // @[TraceEncoder.scala 222:31]
  wire  wpControlWrData_14 = auto_reg_in_a_bits_data[14]; // @[TraceEncoder.scala 222:31]
  wire  _out_womask_T_49 = out_backMask[15]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_49 = &out_backMask[15]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_49 = out_woready_34 & out_womask_49; // @[TraceEncoder.scala 222:31]
  wire  wpControlWrData_15 = auto_reg_in_a_bits_data[15]; // @[TraceEncoder.scala 222:31]
  reg [1:0] tsControlReg_tsBranch; // @[TraceTimestamp.scala 19:34]
  reg  tsControlReg_tsEnable; // @[TraceTimestamp.scala 19:34]
  reg  tsControlReg_tsActive; // @[TraceTimestamp.scala 19:34]
  wire [1:0] tsControlWrData_tsBranch = auto_reg_in_a_bits_data[17:16]; // @[TraceEncoder.scala 222:31]
  wire  out_backSel_4 = _out_backSel_T[4]; // @[TraceEncoder.scala 222:31]
  wire  out_woready_59 = auto_reg_in_a_valid & auto_reg_in_d_ready & ~in_bits_read & out_backSel_4 & _out_T_4; // @[TraceEncoder.scala 222:31]
  wire [1:0] _out_womask_T_59 = out_backMask[17:16]; // @[TraceEncoder.scala 222:31]
  wire  out_womask_59 = &out_backMask[17:16]; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_59 = out_woready_59 & out_womask_59; // @[TraceEncoder.scala 222:31]
  wire  out_f_woready_50 = out_woready_59 & out_womask; // @[TraceEncoder.scala 222:31]
  reg  pcControlReg_pcActive; // @[TracePCSample.scala 18:31]
  wire [62:0] out_prepend_hi = {{24'd0}, tracePCSample_io_pcCapture};
  wire [31:0] out_prepend_lo_28 = out_prepend_hi[62:31]; // @[TracePCSample.scala 30:34]
  wire  _out_rifireMux_T_1 = auto_reg_in_a_valid & auto_reg_in_d_ready & in_bits_read; // @[TraceEncoder.scala 222:31]
  wire [7:0] out_prepend_4 = {1'h0,teControlReg_teInstruction,_traceFrontend_io_teIdle_T_2,teControlReg_teTracing,
    teControlReg_teEnable,teControlReg_teActive}; // @[Cat.scala 30:58]
  wire [8:0] out_prepend_lo_5 = {{1'd0}, out_prepend_4}; // @[TraceEncoder.scala 222:31]
  wire [9:0] out_prepend_5 = {1'h0,out_prepend_lo_5}; // @[Cat.scala 30:58]
  wire [11:0] out_prepend_lo_6 = {{2'd0}, out_prepend_5}; // @[TraceEncoder.scala 222:31]
  wire  out_f_wivalid_8 = out_woready_0 & out_womask_47; // @[TraceEncoder.scala 222:31]
  wire  out_f_wivalid_10 = out_woready_0 & out_womask_49; // @[TraceEncoder.scala 222:31]
  wire [31:0] out_prepend_14 = {teControlReg_teSink,teControlReg_teSinkError,3'h1,teControlReg_teSyncMaxInst,
    teControlReg_teSyncMaxBTM,teControlReg_teInhibitSrc,1'h0,teControlReg_teStallEnable,teControlReg_teStallOccurred,
    out_prepend_lo_6}; // @[Cat.scala 30:58]
  wire [41:0] out_prepend_21 = {1'h0,1'h1,1'h0,1'h0,1'h0,1'h0,4'h0,out_prepend_14}; // @[Cat.scala 30:58]
  wire [47:0] out_prepend_lo_22 = {{6'd0}, out_prepend_21}; // @[TraceEncoder.scala 222:31]
  wire [63:0] out_prepend_27 = {4'h1,1'h0,3'h1,4'h1,2'h1,2'h0,out_prepend_lo_22}; // @[Cat.scala 30:58]
  wire  out_rimask_29 = |out_backMask[31:0]; // @[TraceEncoder.scala 222:31]
  wire [31:0] _out_T_468 = {{24'd0}, traceTimestamp_io_timestamp[39:32]}; // @[TraceEncoder.scala 222:31]
  wire  out_frontSel_8 = _out_backSel_T[8]; // @[TraceEncoder.scala 222:31]
  wire  out_wivalid_30 = auto_reg_in_a_valid & auto_reg_in_d_ready & ~in_bits_read & out_frontSel_8 & _out_T_4; // @[TraceEncoder.scala 222:31]
  wire  out_f_wivalid_30 = out_wivalid_30 & out_womask; // @[TraceEncoder.scala 222:31]
  wire  out_rimask_33 = |out_backMask[63:33]; // @[TraceEncoder.scala 222:31]
  wire [95:0] out_prepend_29 = {out_prepend_hi,tracePCSample_io_pcCaptureValid,out_prepend_lo_28}; // @[Cat.scala 30:58]
  wire [9:0] out_prepend_38 = {wpControlReg_9,wpControlReg_8,wpControlReg_7,wpControlReg_6,wpControlReg_5,wpControlReg_4
    ,wpControlReg_3,wpControlReg_2,wpControlReg_1,wpControlReg_0}; // @[Cat.scala 30:58]
  wire [15:0] out_prepend_44 = {wpControlReg_15,wpControlReg_14,wpControlReg_13,wpControlReg_12,wpControlReg_11,
    wpControlReg_10,out_prepend_38}; // @[Cat.scala 30:58]
  wire [8:0] out_prepend_50 = {1'h0,1'h0,3'h4,1'h0,1'h0,1'h0,tsControlReg_tsActive}; // @[Cat.scala 30:58]
  wire [9:0] out_prepend_lo_51 = {{1'd0}, out_prepend_50}; // @[TraceEncoder.scala 222:31]
  wire [10:0] out_prepend_51 = {1'h0,out_prepend_lo_51}; // @[Cat.scala 30:58]
  wire [14:0] out_prepend_lo_52 = {{4'd0}, out_prepend_51}; // @[TraceEncoder.scala 222:31]
  wire  out_f_wivalid_58 = out_woready_59 & out_womask_49; // @[TraceEncoder.scala 222:31]
  wire [20:0] out_prepend_56 = {1'h0,1'h0,1'h0,tsControlReg_tsBranch,tsControlReg_tsEnable,out_prepend_lo_52}; // @[Cat.scala 30:58]
  wire [23:0] out_prepend_lo_57 = {{3'd0}, out_prepend_56}; // @[TraceEncoder.scala 222:31]
  wire [71:0] out_prepend_58 = {traceTimestamp_io_timestamp,8'h28,out_prepend_lo_57}; // @[Cat.scala 30:58]
  wire  out_frontSel_15 = _out_backSel_T[15]; // @[TraceEncoder.scala 222:31]
  wire  out_rivalid_65 = _out_rifireMux_T_1 & out_frontSel_15 & _out_T_12; // @[TraceEncoder.scala 222:31]
  wire [62:0] out_prepend_hi_1 = {{24'd0}, tracePCSample_io_pcSample};
  wire [31:0] out_f_data = out_prepend_hi_1[62:31]; // @[TracePCSample.scala 38:85]
  wire [95:0] out_prepend_60 = {out_prepend_hi_1,tracePCSample_io_pcSampleValid,out_f_data}; // @[Cat.scala 30:58]
  wire  _out_out_bits_data_T = 4'h0 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_1 = 4'h4 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_2 = 4'h5 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_3 = 4'h7 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_4 = 4'h8 == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_5 = 4'hb == out_iindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_6 = 4'hf == out_iindex; // @[Conditional.scala 37:30]
  wire  _GEN_149 = _out_out_bits_data_T_6 ? _out_T_12 : 1'h1; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_150 = _out_out_bits_data_T_5 ? _out_T_12 : _GEN_149; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_151 = _out_out_bits_data_T_4 ? _out_T_4 : _GEN_150; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_152 = _out_out_bits_data_T_3 ? _out_T_4 : _GEN_151; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_153 = _out_out_bits_data_T_2 ? _out_T_4 : _GEN_152; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_154 = _out_out_bits_data_T_1 ? _out_T_4 : _GEN_153; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  out_out_bits_data_out = _out_out_bits_data_T ? _out_T_4 : _GEN_154; // @[Conditional.scala 40:58 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_156 = _out_out_bits_data_T_6 ? out_prepend_60[63:0] : 64'h0; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_157 = _out_out_bits_data_T_5 ? out_prepend_29[63:0] : _GEN_156; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_158 = _out_out_bits_data_T_4 ? {{63'd0}, pcControlReg_pcActive} : _GEN_157; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_159 = _out_out_bits_data_T_3 ? {{48'd0}, out_prepend_44} : _GEN_158; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_160 = _out_out_bits_data_T_2 ? {{32'd0}, _out_T_468} : _GEN_159; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_161 = _out_out_bits_data_T_1 ? out_prepend_58[63:0] : _GEN_160; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] out_out_bits_data_out_1 = _out_out_bits_data_T ? out_prepend_27 : _GEN_161; // @[Conditional.scala 40:58 MuxLiteral.scala 53:32]
  RHEA__TraceFrontend traceFrontend ( // @[TraceEncoder.scala 237:34]
    .clock(traceFrontend_clock),
    .reset(traceFrontend_reset),
    .auto_traceTrigBlock_bpwatch_in_0_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_0_valid_0),
    .auto_traceTrigBlock_bpwatch_in_0_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_0_action),
    .auto_traceTrigBlock_bpwatch_in_1_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_1_valid_0),
    .auto_traceTrigBlock_bpwatch_in_1_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_1_action),
    .auto_traceTrigBlock_bpwatch_in_2_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_2_valid_0),
    .auto_traceTrigBlock_bpwatch_in_2_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_2_action),
    .auto_traceTrigBlock_bpwatch_in_3_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_3_valid_0),
    .auto_traceTrigBlock_bpwatch_in_3_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_3_action),
    .auto_traceTrigBlock_bpwatch_in_4_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_4_valid_0),
    .auto_traceTrigBlock_bpwatch_in_4_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_4_action),
    .auto_traceTrigBlock_bpwatch_in_5_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_5_valid_0),
    .auto_traceTrigBlock_bpwatch_in_5_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_5_action),
    .auto_traceTrigBlock_bpwatch_in_6_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_6_valid_0),
    .auto_traceTrigBlock_bpwatch_in_6_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_6_action),
    .auto_traceTrigBlock_bpwatch_in_7_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_7_valid_0),
    .auto_traceTrigBlock_bpwatch_in_7_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_7_action),
    .auto_traceTrigBlock_bpwatch_in_8_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_8_valid_0),
    .auto_traceTrigBlock_bpwatch_in_8_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_8_action),
    .auto_traceTrigBlock_bpwatch_in_9_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_9_valid_0),
    .auto_traceTrigBlock_bpwatch_in_9_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_9_action),
    .auto_traceTrigBlock_bpwatch_in_10_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_10_valid_0),
    .auto_traceTrigBlock_bpwatch_in_10_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_10_action),
    .auto_traceTrigBlock_bpwatch_in_11_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_11_valid_0),
    .auto_traceTrigBlock_bpwatch_in_11_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_11_action),
    .auto_traceTrigBlock_bpwatch_in_12_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_12_valid_0),
    .auto_traceTrigBlock_bpwatch_in_12_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_12_action),
    .auto_traceTrigBlock_bpwatch_in_13_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_13_valid_0),
    .auto_traceTrigBlock_bpwatch_in_13_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_13_action),
    .auto_traceTrigBlock_bpwatch_in_14_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_14_valid_0),
    .auto_traceTrigBlock_bpwatch_in_14_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_14_action),
    .auto_traceTrigBlock_bpwatch_in_15_valid_0(traceFrontend_auto_traceTrigBlock_bpwatch_in_15_valid_0),
    .auto_traceTrigBlock_bpwatch_in_15_action(traceFrontend_auto_traceTrigBlock_bpwatch_in_15_action),
    .auto_traceInBlock_trace_legacy_in_0_valid(traceFrontend_auto_traceInBlock_trace_legacy_in_0_valid),
    .auto_traceInBlock_trace_legacy_in_0_iaddr(traceFrontend_auto_traceInBlock_trace_legacy_in_0_iaddr),
    .auto_traceInBlock_trace_legacy_in_0_insn(traceFrontend_auto_traceInBlock_trace_legacy_in_0_insn),
    .auto_traceInBlock_trace_legacy_in_0_priv(traceFrontend_auto_traceInBlock_trace_legacy_in_0_priv),
    .auto_traceInBlock_trace_legacy_in_0_exception(traceFrontend_auto_traceInBlock_trace_legacy_in_0_exception),
    .auto_traceInBlock_trace_legacy_in_0_interrupt(traceFrontend_auto_traceInBlock_trace_legacy_in_0_interrupt),
    .auto_trace_aux_out_stall(traceFrontend_auto_trace_aux_out_stall),
    .io_teActive(traceFrontend_io_teActive),
    .io_tlClock(traceFrontend_io_tlClock),
    .io_tlReset(traceFrontend_io_tlReset),
    .io_hartIsInReset(traceFrontend_io_hartIsInReset),
    .io_ingControlAsync_mem_0_teEnable(traceFrontend_io_ingControlAsync_mem_0_teEnable),
    .io_ingControlAsync_mem_0_teTracing(traceFrontend_io_ingControlAsync_mem_0_teTracing),
    .io_ingControlAsync_mem_0_teInstruction(traceFrontend_io_ingControlAsync_mem_0_teInstruction),
    .io_ingControlAsync_mem_0_teStallEnable(traceFrontend_io_ingControlAsync_mem_0_teStallEnable),
    .io_ingControlAsync_mem_0_teSyncMaxInst(traceFrontend_io_ingControlAsync_mem_0_teSyncMaxInst),
    .io_ingControlAsync_mem_0_teSyncMaxBTM(traceFrontend_io_ingControlAsync_mem_0_teSyncMaxBTM),
    .io_ingControlAsync_ridx(traceFrontend_io_ingControlAsync_ridx),
    .io_ingControlAsync_widx(traceFrontend_io_ingControlAsync_widx),
    .io_traceOn(traceFrontend_io_traceOn),
    .io_teIdle(traceFrontend_io_teIdle),
    .io_traceStall(traceFrontend_io_traceStall),
    .io_pcActive(traceFrontend_io_pcActive),
    .io_pcCapture_valid(traceFrontend_io_pcCapture_valid),
    .io_pcCapture_bits(traceFrontend_io_pcCapture_bits),
    .io_pcSample_valid(traceFrontend_io_pcSample_valid),
    .io_pcSample_bits(traceFrontend_io_pcSample_bits),
    .io_btmEvent_ready(traceFrontend_io_btmEvent_ready),
    .io_btmEvent_valid(traceFrontend_io_btmEvent_valid),
    .io_btmEvent_bits_reason(traceFrontend_io_btmEvent_bits_reason),
    .io_btmEvent_bits_iaddr(traceFrontend_io_btmEvent_bits_iaddr),
    .io_btmEvent_bits_icnt(traceFrontend_io_btmEvent_bits_icnt),
    .io_btmEvent_bits_hist(traceFrontend_io_btmEvent_bits_hist),
    .io_btmEvent_bits_hisv(traceFrontend_io_btmEvent_bits_hisv),
    .io_wpControl_mem_0_0(traceFrontend_io_wpControl_mem_0_0),
    .io_wpControl_mem_0_1(traceFrontend_io_wpControl_mem_0_1),
    .io_wpControl_mem_0_2(traceFrontend_io_wpControl_mem_0_2),
    .io_wpControl_mem_0_3(traceFrontend_io_wpControl_mem_0_3),
    .io_wpControl_mem_0_4(traceFrontend_io_wpControl_mem_0_4),
    .io_wpControl_mem_0_5(traceFrontend_io_wpControl_mem_0_5),
    .io_wpControl_mem_0_6(traceFrontend_io_wpControl_mem_0_6),
    .io_wpControl_mem_0_7(traceFrontend_io_wpControl_mem_0_7),
    .io_wpControl_mem_0_8(traceFrontend_io_wpControl_mem_0_8),
    .io_wpControl_mem_0_9(traceFrontend_io_wpControl_mem_0_9),
    .io_wpControl_mem_0_10(traceFrontend_io_wpControl_mem_0_10),
    .io_wpControl_mem_0_11(traceFrontend_io_wpControl_mem_0_11),
    .io_wpControl_mem_0_12(traceFrontend_io_wpControl_mem_0_12),
    .io_wpControl_mem_0_13(traceFrontend_io_wpControl_mem_0_13),
    .io_wpControl_mem_0_14(traceFrontend_io_wpControl_mem_0_14),
    .io_wpControl_mem_0_15(traceFrontend_io_wpControl_mem_0_15)
  );
  RHEA__TLTraceSink traceSink ( // @[TraceEncoder.scala 238:34]
    .clock(traceSink_clock),
    .reset(traceSink_reset),
    .auto_sink_out_a_ready(traceSink_auto_sink_out_a_ready),
    .auto_sink_out_a_valid(traceSink_auto_sink_out_a_valid),
    .auto_sink_out_a_bits_address(traceSink_auto_sink_out_a_bits_address),
    .auto_sink_out_a_bits_data(traceSink_auto_sink_out_a_bits_data),
    .auto_sink_out_d_valid(traceSink_auto_sink_out_d_valid),
    .auto_sink_out_d_bits_denied(traceSink_auto_sink_out_d_bits_denied),
    .io_teActive(traceSink_io_teActive),
    .io_teControl_teSink(traceSink_io_teControl_teSink),
    .io_stream_ready(traceSink_io_stream_ready),
    .io_stream_valid(traceSink_io_stream_valid),
    .io_stream_bits_data(traceSink_io_stream_bits_data),
    .io_sinkError(traceSink_io_sinkError)
  );
  RHEA__TraceTimestamp_1 traceTimestamp ( // @[TraceEncoder.scala 240:58]
    .auto_timestamp_sink_in(traceTimestamp_auto_timestamp_sink_in),
    .io_timestamp(traceTimestamp_io_timestamp),
    .io_tsControlIn_tsBranch(traceTimestamp_io_tsControlIn_tsBranch),
    .io_tsControlIn_tsEnable(traceTimestamp_io_tsControlIn_tsEnable),
    .io_tsControl_tsBranch(traceTimestamp_io_tsControl_tsBranch),
    .io_tsControl_tsEnable(traceTimestamp_io_tsControl_tsEnable)
  );
  RHEA__TraceBTM_1 traceBTM ( // @[TraceEncoder.scala 288:29]
    .rf_reset(traceBTM_rf_reset),
    .clock(traceBTM_clock),
    .reset(traceBTM_reset),
    .io_teActive(traceBTM_io_teActive),
    .io_teControl_teSyncMaxBTM(traceBTM_io_teControl_teSyncMaxBTM),
    .io_teControl_teInhibitSrc(traceBTM_io_teControl_teInhibitSrc),
    .io_teControl_teInstruction(traceBTM_io_teControl_teInstruction),
    .io_teControl_teEnable(traceBTM_io_teControl_teEnable),
    .io_tsControl_tsBranch(traceBTM_io_tsControl_tsBranch),
    .io_tsControl_tsEnable(traceBTM_io_tsControl_tsEnable),
    .io_btmEvent_ready(traceBTM_io_btmEvent_ready),
    .io_btmEvent_valid(traceBTM_io_btmEvent_valid),
    .io_btmEvent_bits_reason(traceBTM_io_btmEvent_bits_reason),
    .io_btmEvent_bits_iaddr(traceBTM_io_btmEvent_bits_iaddr),
    .io_btmEvent_bits_icnt(traceBTM_io_btmEvent_bits_icnt),
    .io_btmEvent_bits_hist(traceBTM_io_btmEvent_bits_hist),
    .io_btmEvent_bits_hisv(traceBTM_io_btmEvent_bits_hisv),
    .io_btm_ready(traceBTM_io_btm_ready),
    .io_btm_valid(traceBTM_io_btm_valid),
    .io_btm_bits_length(traceBTM_io_btm_bits_length),
    .io_btm_bits_timestamp(traceBTM_io_btm_bits_timestamp),
    .io_btm_bits_slices_0_data(traceBTM_io_btm_bits_slices_0_data),
    .io_btm_bits_slices_0_mseo(traceBTM_io_btm_bits_slices_0_mseo),
    .io_btm_bits_slices_1_data(traceBTM_io_btm_bits_slices_1_data),
    .io_btm_bits_slices_1_mseo(traceBTM_io_btm_bits_slices_1_mseo),
    .io_btm_bits_slices_2_data(traceBTM_io_btm_bits_slices_2_data),
    .io_btm_bits_slices_2_mseo(traceBTM_io_btm_bits_slices_2_mseo),
    .io_btm_bits_slices_3_data(traceBTM_io_btm_bits_slices_3_data),
    .io_btm_bits_slices_3_mseo(traceBTM_io_btm_bits_slices_3_mseo),
    .io_btm_bits_slices_4_data(traceBTM_io_btm_bits_slices_4_data),
    .io_btm_bits_slices_4_mseo(traceBTM_io_btm_bits_slices_4_mseo),
    .io_btm_bits_slices_5_data(traceBTM_io_btm_bits_slices_5_data),
    .io_btm_bits_slices_5_mseo(traceBTM_io_btm_bits_slices_5_mseo),
    .io_btm_bits_slices_6_data(traceBTM_io_btm_bits_slices_6_data),
    .io_btm_bits_slices_6_mseo(traceBTM_io_btm_bits_slices_6_mseo),
    .io_btm_bits_slices_7_data(traceBTM_io_btm_bits_slices_7_data),
    .io_btm_bits_slices_7_mseo(traceBTM_io_btm_bits_slices_7_mseo),
    .io_btm_bits_slices_8_data(traceBTM_io_btm_bits_slices_8_data),
    .io_btm_bits_slices_8_mseo(traceBTM_io_btm_bits_slices_8_mseo),
    .io_btm_bits_slices_9_data(traceBTM_io_btm_bits_slices_9_data),
    .io_btm_bits_slices_9_mseo(traceBTM_io_btm_bits_slices_9_mseo),
    .io_btm_bits_slices_10_data(traceBTM_io_btm_bits_slices_10_data),
    .io_btm_bits_slices_10_mseo(traceBTM_io_btm_bits_slices_10_mseo),
    .io_btm_bits_slices_11_data(traceBTM_io_btm_bits_slices_11_data),
    .io_btm_bits_slices_11_mseo(traceBTM_io_btm_bits_slices_11_mseo),
    .io_btm_bits_slices_12_data(traceBTM_io_btm_bits_slices_12_data),
    .io_btm_bits_slices_12_mseo(traceBTM_io_btm_bits_slices_12_mseo),
    .io_btm_bits_slices_13_data(traceBTM_io_btm_bits_slices_13_data),
    .io_btm_bits_slices_13_mseo(traceBTM_io_btm_bits_slices_13_mseo),
    .io_btm_bits_slices_14_data(traceBTM_io_btm_bits_slices_14_data),
    .io_btm_bits_slices_14_mseo(traceBTM_io_btm_bits_slices_14_mseo),
    .io_btm_bits_slices_15_data(traceBTM_io_btm_bits_slices_15_data),
    .io_btm_bits_slices_15_mseo(traceBTM_io_btm_bits_slices_15_mseo),
    .io_btm_bits_slices_16_data(traceBTM_io_btm_bits_slices_16_data),
    .io_btm_bits_slices_16_mseo(traceBTM_io_btm_bits_slices_16_mseo),
    .io_btm_bits_slices_17_data(traceBTM_io_btm_bits_slices_17_data),
    .io_btm_bits_slices_17_mseo(traceBTM_io_btm_bits_slices_17_mseo),
    .io_btm_bits_slices_18_data(traceBTM_io_btm_bits_slices_18_data),
    .io_btm_bits_slices_18_mseo(traceBTM_io_btm_bits_slices_18_mseo),
    .io_btm_bits_slices_19_data(traceBTM_io_btm_bits_slices_19_data),
    .io_btm_bits_slices_19_mseo(traceBTM_io_btm_bits_slices_19_mseo),
    .io_btm_bits_slices_20_data(traceBTM_io_btm_bits_slices_20_data),
    .io_btm_bits_slices_20_mseo(traceBTM_io_btm_bits_slices_20_mseo),
    .io_btm_bits_slices_21_data(traceBTM_io_btm_bits_slices_21_data),
    .io_btm_bits_slices_21_mseo(traceBTM_io_btm_bits_slices_21_mseo),
    .io_btm_bits_slices_22_data(traceBTM_io_btm_bits_slices_22_data),
    .io_btm_bits_slices_22_mseo(traceBTM_io_btm_bits_slices_22_mseo),
    .io_btm_bits_slices_23_data(traceBTM_io_btm_bits_slices_23_data),
    .io_btm_bits_slices_23_mseo(traceBTM_io_btm_bits_slices_23_mseo),
    .io_btm_bits_slices_24_data(traceBTM_io_btm_bits_slices_24_data),
    .io_btm_bits_slices_24_mseo(traceBTM_io_btm_bits_slices_24_mseo)
  );
  RHEA__TracePacker tracePacker ( // @[TraceEncoder.scala 289:29]
    .clock(tracePacker_clock),
    .reset(tracePacker_reset),
    .io_teActive(tracePacker_io_teActive),
    .io_btm_ready(tracePacker_io_btm_ready),
    .io_btm_valid(tracePacker_io_btm_valid),
    .io_btm_bits_length(tracePacker_io_btm_bits_length),
    .io_btm_bits_timestamp(tracePacker_io_btm_bits_timestamp),
    .io_btm_bits_slices_0_data(tracePacker_io_btm_bits_slices_0_data),
    .io_btm_bits_slices_0_mseo(tracePacker_io_btm_bits_slices_0_mseo),
    .io_btm_bits_slices_1_data(tracePacker_io_btm_bits_slices_1_data),
    .io_btm_bits_slices_1_mseo(tracePacker_io_btm_bits_slices_1_mseo),
    .io_btm_bits_slices_2_data(tracePacker_io_btm_bits_slices_2_data),
    .io_btm_bits_slices_2_mseo(tracePacker_io_btm_bits_slices_2_mseo),
    .io_btm_bits_slices_3_data(tracePacker_io_btm_bits_slices_3_data),
    .io_btm_bits_slices_3_mseo(tracePacker_io_btm_bits_slices_3_mseo),
    .io_btm_bits_slices_4_data(tracePacker_io_btm_bits_slices_4_data),
    .io_btm_bits_slices_4_mseo(tracePacker_io_btm_bits_slices_4_mseo),
    .io_btm_bits_slices_5_data(tracePacker_io_btm_bits_slices_5_data),
    .io_btm_bits_slices_5_mseo(tracePacker_io_btm_bits_slices_5_mseo),
    .io_btm_bits_slices_6_data(tracePacker_io_btm_bits_slices_6_data),
    .io_btm_bits_slices_6_mseo(tracePacker_io_btm_bits_slices_6_mseo),
    .io_btm_bits_slices_7_data(tracePacker_io_btm_bits_slices_7_data),
    .io_btm_bits_slices_7_mseo(tracePacker_io_btm_bits_slices_7_mseo),
    .io_btm_bits_slices_8_data(tracePacker_io_btm_bits_slices_8_data),
    .io_btm_bits_slices_8_mseo(tracePacker_io_btm_bits_slices_8_mseo),
    .io_btm_bits_slices_9_data(tracePacker_io_btm_bits_slices_9_data),
    .io_btm_bits_slices_9_mseo(tracePacker_io_btm_bits_slices_9_mseo),
    .io_btm_bits_slices_10_data(tracePacker_io_btm_bits_slices_10_data),
    .io_btm_bits_slices_10_mseo(tracePacker_io_btm_bits_slices_10_mseo),
    .io_btm_bits_slices_11_data(tracePacker_io_btm_bits_slices_11_data),
    .io_btm_bits_slices_11_mseo(tracePacker_io_btm_bits_slices_11_mseo),
    .io_btm_bits_slices_12_data(tracePacker_io_btm_bits_slices_12_data),
    .io_btm_bits_slices_12_mseo(tracePacker_io_btm_bits_slices_12_mseo),
    .io_btm_bits_slices_13_data(tracePacker_io_btm_bits_slices_13_data),
    .io_btm_bits_slices_13_mseo(tracePacker_io_btm_bits_slices_13_mseo),
    .io_btm_bits_slices_14_data(tracePacker_io_btm_bits_slices_14_data),
    .io_btm_bits_slices_14_mseo(tracePacker_io_btm_bits_slices_14_mseo),
    .io_btm_bits_slices_15_data(tracePacker_io_btm_bits_slices_15_data),
    .io_btm_bits_slices_15_mseo(tracePacker_io_btm_bits_slices_15_mseo),
    .io_btm_bits_slices_16_data(tracePacker_io_btm_bits_slices_16_data),
    .io_btm_bits_slices_16_mseo(tracePacker_io_btm_bits_slices_16_mseo),
    .io_btm_bits_slices_17_data(tracePacker_io_btm_bits_slices_17_data),
    .io_btm_bits_slices_17_mseo(tracePacker_io_btm_bits_slices_17_mseo),
    .io_btm_bits_slices_18_data(tracePacker_io_btm_bits_slices_18_data),
    .io_btm_bits_slices_18_mseo(tracePacker_io_btm_bits_slices_18_mseo),
    .io_btm_bits_slices_19_data(tracePacker_io_btm_bits_slices_19_data),
    .io_btm_bits_slices_19_mseo(tracePacker_io_btm_bits_slices_19_mseo),
    .io_btm_bits_slices_20_data(tracePacker_io_btm_bits_slices_20_data),
    .io_btm_bits_slices_20_mseo(tracePacker_io_btm_bits_slices_20_mseo),
    .io_btm_bits_slices_21_data(tracePacker_io_btm_bits_slices_21_data),
    .io_btm_bits_slices_21_mseo(tracePacker_io_btm_bits_slices_21_mseo),
    .io_btm_bits_slices_22_data(tracePacker_io_btm_bits_slices_22_data),
    .io_btm_bits_slices_22_mseo(tracePacker_io_btm_bits_slices_22_mseo),
    .io_btm_bits_slices_23_data(tracePacker_io_btm_bits_slices_23_data),
    .io_btm_bits_slices_23_mseo(tracePacker_io_btm_bits_slices_23_mseo),
    .io_btm_bits_slices_24_data(tracePacker_io_btm_bits_slices_24_data),
    .io_btm_bits_slices_24_mseo(tracePacker_io_btm_bits_slices_24_mseo),
    .io_timestamp(tracePacker_io_timestamp),
    .io_stream_ready(tracePacker_io_stream_ready),
    .io_stream_valid(tracePacker_io_stream_valid),
    .io_stream_bits_data(tracePacker_io_stream_bits_data)
  );
  RHEA__TraceGapper traceGapper ( // @[TraceEncoder.scala 290:29]
    .io_streamIn_ready(traceGapper_io_streamIn_ready),
    .io_streamIn_valid(traceGapper_io_streamIn_valid),
    .io_streamIn_bits_data(traceGapper_io_streamIn_bits_data),
    .io_streamOut_ready(traceGapper_io_streamOut_ready),
    .io_streamOut_valid(traceGapper_io_streamOut_valid),
    .io_streamOut_bits_data(traceGapper_io_streamOut_bits_data)
  );
  RHEA__TracePCSample tracePCSample ( // @[TraceEncoder.scala 291:54]
    .clock(tracePCSample_clock),
    .reset(tracePCSample_reset),
    .io_pcControlReg_pcActive(tracePCSample_io_pcControlReg_pcActive),
    .io_pcSampleRdEn(tracePCSample_io_pcSampleRdEn),
    .io_pcSampleHRdEn(tracePCSample_io_pcSampleHRdEn),
    .io_ingPcCapture_valid(tracePCSample_io_ingPcCapture_valid),
    .io_ingPcCapture_bits(tracePCSample_io_ingPcCapture_bits),
    .io_ingPcSample_valid(tracePCSample_io_ingPcSample_valid),
    .io_ingPcSample_bits(tracePCSample_io_ingPcSample_bits),
    .io_pcCaptureValid(tracePCSample_io_pcCaptureValid),
    .io_pcSampleValid(tracePCSample_io_pcSampleValid),
    .io_pcCapture(tracePCSample_io_pcCapture),
    .io_pcSample(tracePCSample_io_pcSample),
    .io_pcActive(tracePCSample_io_pcActive)
  );
  RHEA__AsyncQueueSource_5 traceFrontend_io_ingControlAsync_source ( // @[AsyncQueue.scala 216:24]
    .rf_reset(traceFrontend_io_ingControlAsync_source_rf_reset),
    .clock(traceFrontend_io_ingControlAsync_source_clock),
    .reset(traceFrontend_io_ingControlAsync_source_reset),
    .io_enq_ready(traceFrontend_io_ingControlAsync_source_io_enq_ready),
    .io_enq_valid(traceFrontend_io_ingControlAsync_source_io_enq_valid),
    .io_enq_bits_teEnable(traceFrontend_io_ingControlAsync_source_io_enq_bits_teEnable),
    .io_enq_bits_teTracing(traceFrontend_io_ingControlAsync_source_io_enq_bits_teTracing),
    .io_enq_bits_teInstruction(traceFrontend_io_ingControlAsync_source_io_enq_bits_teInstruction),
    .io_enq_bits_teStallEnable(traceFrontend_io_ingControlAsync_source_io_enq_bits_teStallEnable),
    .io_enq_bits_teSyncMaxInst(traceFrontend_io_ingControlAsync_source_io_enq_bits_teSyncMaxInst),
    .io_enq_bits_teSyncMaxBTM(traceFrontend_io_ingControlAsync_source_io_enq_bits_teSyncMaxBTM),
    .io_async_mem_0_teEnable(traceFrontend_io_ingControlAsync_source_io_async_mem_0_teEnable),
    .io_async_mem_0_teTracing(traceFrontend_io_ingControlAsync_source_io_async_mem_0_teTracing),
    .io_async_mem_0_teInstruction(traceFrontend_io_ingControlAsync_source_io_async_mem_0_teInstruction),
    .io_async_mem_0_teStallEnable(traceFrontend_io_ingControlAsync_source_io_async_mem_0_teStallEnable),
    .io_async_mem_0_teSyncMaxInst(traceFrontend_io_ingControlAsync_source_io_async_mem_0_teSyncMaxInst),
    .io_async_mem_0_teSyncMaxBTM(traceFrontend_io_ingControlAsync_source_io_async_mem_0_teSyncMaxBTM),
    .io_async_ridx(traceFrontend_io_ingControlAsync_source_io_async_ridx),
    .io_async_widx(traceFrontend_io_ingControlAsync_source_io_async_widx)
  );
  assign teSinkXbar_auto_in_a_ready = teSinkXbar_auto_out_a_ready; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign teSinkXbar_auto_in_d_valid = teSinkXbar_auto_out_d_valid; // @[ReadyValidCancel.scala 21:38]
  assign teSinkXbar_auto_in_d_bits_denied = teSinkXbar_auto_out_d_bits_denied; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign teSinkXbar_auto_out_a_valid = teSinkXbar_auto_in_a_valid; // @[ReadyValidCancel.scala 21:38]
  assign teSinkXbar_auto_out_a_bits_address = teSinkXbar_auto_in_a_bits_address; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign teSinkXbar_auto_out_a_bits_data = teSinkXbar_auto_in_a_bits_data; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign traceBTM_rf_reset = rf_reset;
  assign traceFrontend_io_ingControlAsync_source_rf_reset = rf_reset;
  assign rf_reset = reset;
  assign auto_teSinkXbar_out_a_valid = teSinkXbar_auto_out_a_valid; // @[LazyModule.scala 390:12]
  assign auto_teSinkXbar_out_a_bits_address = teSinkXbar_auto_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_teSinkXbar_out_a_bits_data = teSinkXbar_auto_out_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_traceFrontend_trace_aux_out_stall = traceFrontend_auto_trace_aux_out_stall; // @[LazyModule.scala 390:12]
  assign auto_reg_in_a_ready = auto_reg_in_d_ready; // @[TraceEncoder.scala 222:31]
  assign auto_reg_in_d_valid = auto_reg_in_a_valid; // @[TraceEncoder.scala 222:31]
  assign auto_reg_in_d_bits_opcode = {{2'd0}, in_bits_read}; // @[TraceEncoder.scala 222:31 TraceEncoder.scala 222:31]
  assign auto_reg_in_d_bits_size = auto_reg_in_a_bits_size; // @[TraceEncoder.scala 222:31 LazyModule.scala 388:16]
  assign auto_reg_in_d_bits_source = auto_reg_in_a_bits_source; // @[TraceEncoder.scala 222:31 LazyModule.scala 388:16]
  assign auto_reg_in_d_bits_data = out_out_bits_data_out ? out_out_bits_data_out_1 : 64'h0; // @[TraceEncoder.scala 222:31]
  assign traceFrontend_clock = io_clock; // @[TraceEncoder.scala 596:32]
  assign traceFrontend_reset = io_reset; // @[TraceEncoder.scala 597:32]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_0_valid_0 = auto_traceFrontend_traceTrigBlock_bpwatch_in_0_valid_0
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_0_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_0_action; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_1_valid_0 = auto_traceFrontend_traceTrigBlock_bpwatch_in_1_valid_0
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_1_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_1_action; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_2_valid_0 = auto_traceFrontend_traceTrigBlock_bpwatch_in_2_valid_0
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_2_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_2_action; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_3_valid_0 = auto_traceFrontend_traceTrigBlock_bpwatch_in_3_valid_0
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_3_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_3_action; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_4_valid_0 = auto_traceFrontend_traceTrigBlock_bpwatch_in_4_valid_0
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_4_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_4_action; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_5_valid_0 = auto_traceFrontend_traceTrigBlock_bpwatch_in_5_valid_0
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_5_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_5_action; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_6_valid_0 = auto_traceFrontend_traceTrigBlock_bpwatch_in_6_valid_0
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_6_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_6_action; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_7_valid_0 = auto_traceFrontend_traceTrigBlock_bpwatch_in_7_valid_0
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_7_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_7_action; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_8_valid_0 = auto_traceFrontend_traceTrigBlock_bpwatch_in_8_valid_0
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_8_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_8_action; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_9_valid_0 = auto_traceFrontend_traceTrigBlock_bpwatch_in_9_valid_0
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_9_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_9_action; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_10_valid_0 =
    auto_traceFrontend_traceTrigBlock_bpwatch_in_10_valid_0; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_10_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_10_action
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_11_valid_0 =
    auto_traceFrontend_traceTrigBlock_bpwatch_in_11_valid_0; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_11_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_11_action
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_12_valid_0 =
    auto_traceFrontend_traceTrigBlock_bpwatch_in_12_valid_0; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_12_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_12_action
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_13_valid_0 =
    auto_traceFrontend_traceTrigBlock_bpwatch_in_13_valid_0; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_13_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_13_action
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_14_valid_0 =
    auto_traceFrontend_traceTrigBlock_bpwatch_in_14_valid_0; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_14_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_14_action
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_15_valid_0 =
    auto_traceFrontend_traceTrigBlock_bpwatch_in_15_valid_0; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceTrigBlock_bpwatch_in_15_action = auto_traceFrontend_traceTrigBlock_bpwatch_in_15_action
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceInBlock_trace_legacy_in_0_valid =
    auto_traceFrontend_traceInBlock_trace_legacy_in_0_valid; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceInBlock_trace_legacy_in_0_iaddr =
    auto_traceFrontend_traceInBlock_trace_legacy_in_0_iaddr; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceInBlock_trace_legacy_in_0_insn = auto_traceFrontend_traceInBlock_trace_legacy_in_0_insn
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceInBlock_trace_legacy_in_0_priv = auto_traceFrontend_traceInBlock_trace_legacy_in_0_priv
    ; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceInBlock_trace_legacy_in_0_exception =
    auto_traceFrontend_traceInBlock_trace_legacy_in_0_exception; // @[LazyModule.scala 388:16]
  assign traceFrontend_auto_traceInBlock_trace_legacy_in_0_interrupt =
    auto_traceFrontend_traceInBlock_trace_legacy_in_0_interrupt; // @[LazyModule.scala 388:16]
  assign traceFrontend_io_teActive = teControlReg_teActive; // @[TraceEncoder.scala 600:38]
  assign traceFrontend_io_tlClock = clock; // @[TraceEncoder.scala 598:37]
  assign traceFrontend_io_tlReset = reset; // @[TraceEncoder.scala 599:37]
  assign traceFrontend_io_hartIsInReset = hartIsInReset; // @[TraceEncoder.scala 601:43]
  assign traceFrontend_io_ingControlAsync_mem_0_teEnable =
    traceFrontend_io_ingControlAsync_source_io_async_mem_0_teEnable; // @[TraceEncoder.scala 603:51]
  assign traceFrontend_io_ingControlAsync_mem_0_teTracing =
    traceFrontend_io_ingControlAsync_source_io_async_mem_0_teTracing; // @[TraceEncoder.scala 603:51]
  assign traceFrontend_io_ingControlAsync_mem_0_teInstruction =
    traceFrontend_io_ingControlAsync_source_io_async_mem_0_teInstruction; // @[TraceEncoder.scala 603:51]
  assign traceFrontend_io_ingControlAsync_mem_0_teStallEnable =
    traceFrontend_io_ingControlAsync_source_io_async_mem_0_teStallEnable; // @[TraceEncoder.scala 603:51]
  assign traceFrontend_io_ingControlAsync_mem_0_teSyncMaxInst =
    traceFrontend_io_ingControlAsync_source_io_async_mem_0_teSyncMaxInst; // @[TraceEncoder.scala 603:51]
  assign traceFrontend_io_ingControlAsync_mem_0_teSyncMaxBTM =
    traceFrontend_io_ingControlAsync_source_io_async_mem_0_teSyncMaxBTM; // @[TraceEncoder.scala 603:51]
  assign traceFrontend_io_ingControlAsync_widx = traceFrontend_io_ingControlAsync_source_io_async_widx; // @[TraceEncoder.scala 603:51]
  assign traceFrontend_io_teIdle = ~traceBTM_io_btmEvent_valid & ~tracePacker_io_stream_valid; // @[TraceEncoder.scala 607:67]
  assign traceFrontend_io_pcActive = tracePCSample_io_pcActive; // @[TraceEncoder.scala 608:38]
  assign traceFrontend_io_btmEvent_ready = traceBTM_io_btmEvent_ready; // @[TraceEncoder.scala 630:26]
  assign traceFrontend_io_wpControl_mem_0_0 = wpControlReg_0; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceFrontend_io_wpControl_mem_0_1 = wpControlReg_1; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceFrontend_io_wpControl_mem_0_2 = wpControlReg_2; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceFrontend_io_wpControl_mem_0_3 = wpControlReg_3; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceFrontend_io_wpControl_mem_0_4 = wpControlReg_4; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceFrontend_io_wpControl_mem_0_5 = wpControlReg_5; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceFrontend_io_wpControl_mem_0_6 = wpControlReg_6; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceFrontend_io_wpControl_mem_0_7 = wpControlReg_7; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceFrontend_io_wpControl_mem_0_8 = wpControlReg_8; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceFrontend_io_wpControl_mem_0_9 = wpControlReg_9; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceFrontend_io_wpControl_mem_0_10 = wpControlReg_10; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceFrontend_io_wpControl_mem_0_11 = wpControlReg_11; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceFrontend_io_wpControl_mem_0_12 = wpControlReg_12; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceFrontend_io_wpControl_mem_0_13 = wpControlReg_13; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceFrontend_io_wpControl_mem_0_14 = wpControlReg_14; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceFrontend_io_wpControl_mem_0_15 = wpControlReg_15; // @[TraceFrontend.scala 129:27 TraceFrontend.scala 130:22]
  assign traceSink_clock = clock;
  assign traceSink_reset = reset;
  assign traceSink_auto_sink_out_a_ready = teSinkXbar_auto_in_a_ready; // @[LazyModule.scala 377:16]
  assign traceSink_auto_sink_out_d_valid = teSinkXbar_auto_in_d_valid; // @[LazyModule.scala 377:16]
  assign traceSink_auto_sink_out_d_bits_denied = teSinkXbar_auto_in_d_bits_denied; // @[LazyModule.scala 377:16]
  assign traceSink_io_teActive = teControlReg_teActive; // @[TraceEncoder.scala 645:34]
  assign traceSink_io_teControl_teSink = teControlReg_teSink; // @[TraceEncoder.scala 646:35]
  assign traceSink_io_stream_valid = traceGapper_io_streamOut_valid; // @[TraceEncoder.scala 647:32]
  assign traceSink_io_stream_bits_data = traceGapper_io_streamOut_bits_data; // @[TraceEncoder.scala 647:32]
  assign traceTimestamp_auto_timestamp_sink_in = auto_traceTimestamp_timestamp_sink_in; // @[LazyModule.scala 388:16]
  assign traceTimestamp_io_tsControlIn_tsBranch = tsControlReg_tsBranch; // @[TraceTimestamp.scala 43:32]
  assign traceTimestamp_io_tsControlIn_tsEnable = tsControlReg_tsEnable; // @[TraceTimestamp.scala 43:32]
  assign teSinkXbar_auto_in_a_valid = traceSink_auto_sink_out_a_valid; // @[LazyModule.scala 377:16]
  assign teSinkXbar_auto_in_a_bits_address = traceSink_auto_sink_out_a_bits_address; // @[LazyModule.scala 377:16]
  assign teSinkXbar_auto_in_a_bits_data = traceSink_auto_sink_out_a_bits_data; // @[LazyModule.scala 377:16]
  assign teSinkXbar_auto_out_a_ready = auto_teSinkXbar_out_a_ready; // @[LazyModule.scala 390:12]
  assign teSinkXbar_auto_out_d_valid = auto_teSinkXbar_out_d_valid; // @[LazyModule.scala 390:12]
  assign teSinkXbar_auto_out_d_bits_denied = auto_teSinkXbar_out_d_bits_denied; // @[LazyModule.scala 390:12]
  assign traceBTM_clock = clock;
  assign traceBTM_reset = reset;
  assign traceBTM_io_teActive = teControlReg_teActive; // @[TraceEncoder.scala 627:26]
  assign traceBTM_io_teControl_teSyncMaxBTM = teControlReg_teSyncMaxBTM; // @[TraceEncoder.scala 628:27]
  assign traceBTM_io_teControl_teInhibitSrc = teControlReg_teInhibitSrc; // @[TraceEncoder.scala 628:27]
  assign traceBTM_io_teControl_teInstruction = teControlReg_teInstruction; // @[TraceEncoder.scala 628:27]
  assign traceBTM_io_teControl_teEnable = teControlReg_teEnable; // @[TraceEncoder.scala 628:27]
  assign traceBTM_io_tsControl_tsBranch = traceTimestamp_io_tsControl_tsBranch; // @[TraceEncoder.scala 629:27]
  assign traceBTM_io_tsControl_tsEnable = traceTimestamp_io_tsControl_tsEnable; // @[TraceEncoder.scala 629:27]
  assign traceBTM_io_btmEvent_valid = traceFrontend_io_btmEvent_valid; // @[TraceEncoder.scala 630:26]
  assign traceBTM_io_btmEvent_bits_reason = traceFrontend_io_btmEvent_bits_reason; // @[TraceEncoder.scala 630:26]
  assign traceBTM_io_btmEvent_bits_iaddr = traceFrontend_io_btmEvent_bits_iaddr; // @[TraceEncoder.scala 630:26]
  assign traceBTM_io_btmEvent_bits_icnt = traceFrontend_io_btmEvent_bits_icnt; // @[TraceEncoder.scala 630:26]
  assign traceBTM_io_btmEvent_bits_hist = traceFrontend_io_btmEvent_bits_hist; // @[TraceEncoder.scala 630:26]
  assign traceBTM_io_btmEvent_bits_hisv = traceFrontend_io_btmEvent_bits_hisv; // @[TraceEncoder.scala 630:26]
  assign traceBTM_io_btm_ready = tracePacker_io_btm_ready; // @[TraceEncoder.scala 633:24]
  assign tracePacker_clock = clock;
  assign tracePacker_reset = reset;
  assign tracePacker_io_teActive = teControlReg_teActive; // @[TraceEncoder.scala 632:29]
  assign tracePacker_io_btm_valid = traceBTM_io_btm_valid; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_length = traceBTM_io_btm_bits_length; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_timestamp = traceBTM_io_btm_bits_timestamp; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_0_data = traceBTM_io_btm_bits_slices_0_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_0_mseo = traceBTM_io_btm_bits_slices_0_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_1_data = traceBTM_io_btm_bits_slices_1_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_1_mseo = traceBTM_io_btm_bits_slices_1_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_2_data = traceBTM_io_btm_bits_slices_2_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_2_mseo = traceBTM_io_btm_bits_slices_2_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_3_data = traceBTM_io_btm_bits_slices_3_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_3_mseo = traceBTM_io_btm_bits_slices_3_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_4_data = traceBTM_io_btm_bits_slices_4_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_4_mseo = traceBTM_io_btm_bits_slices_4_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_5_data = traceBTM_io_btm_bits_slices_5_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_5_mseo = traceBTM_io_btm_bits_slices_5_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_6_data = traceBTM_io_btm_bits_slices_6_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_6_mseo = traceBTM_io_btm_bits_slices_6_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_7_data = traceBTM_io_btm_bits_slices_7_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_7_mseo = traceBTM_io_btm_bits_slices_7_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_8_data = traceBTM_io_btm_bits_slices_8_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_8_mseo = traceBTM_io_btm_bits_slices_8_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_9_data = traceBTM_io_btm_bits_slices_9_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_9_mseo = traceBTM_io_btm_bits_slices_9_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_10_data = traceBTM_io_btm_bits_slices_10_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_10_mseo = traceBTM_io_btm_bits_slices_10_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_11_data = traceBTM_io_btm_bits_slices_11_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_11_mseo = traceBTM_io_btm_bits_slices_11_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_12_data = traceBTM_io_btm_bits_slices_12_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_12_mseo = traceBTM_io_btm_bits_slices_12_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_13_data = traceBTM_io_btm_bits_slices_13_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_13_mseo = traceBTM_io_btm_bits_slices_13_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_14_data = traceBTM_io_btm_bits_slices_14_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_14_mseo = traceBTM_io_btm_bits_slices_14_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_15_data = traceBTM_io_btm_bits_slices_15_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_15_mseo = traceBTM_io_btm_bits_slices_15_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_16_data = traceBTM_io_btm_bits_slices_16_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_16_mseo = traceBTM_io_btm_bits_slices_16_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_17_data = traceBTM_io_btm_bits_slices_17_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_17_mseo = traceBTM_io_btm_bits_slices_17_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_18_data = traceBTM_io_btm_bits_slices_18_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_18_mseo = traceBTM_io_btm_bits_slices_18_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_19_data = traceBTM_io_btm_bits_slices_19_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_19_mseo = traceBTM_io_btm_bits_slices_19_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_20_data = traceBTM_io_btm_bits_slices_20_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_20_mseo = traceBTM_io_btm_bits_slices_20_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_21_data = traceBTM_io_btm_bits_slices_21_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_21_mseo = traceBTM_io_btm_bits_slices_21_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_22_data = traceBTM_io_btm_bits_slices_22_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_22_mseo = traceBTM_io_btm_bits_slices_22_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_23_data = traceBTM_io_btm_bits_slices_23_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_23_mseo = traceBTM_io_btm_bits_slices_23_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_24_data = traceBTM_io_btm_bits_slices_24_data; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_btm_bits_slices_24_mseo = traceBTM_io_btm_bits_slices_24_mseo; // @[TraceEncoder.scala 633:24]
  assign tracePacker_io_timestamp = traceTimestamp_io_timestamp; // @[TraceEncoder.scala 635:30]
  assign tracePacker_io_stream_ready = traceGapper_io_streamIn_ready; // @[TraceEncoder.scala 643:29]
  assign traceGapper_io_streamIn_valid = tracePacker_io_stream_valid; // @[TraceEncoder.scala 643:29]
  assign traceGapper_io_streamIn_bits_data = tracePacker_io_stream_bits_data; // @[TraceEncoder.scala 643:29]
  assign traceGapper_io_streamOut_ready = traceSink_io_stream_ready; // @[TraceEncoder.scala 647:32]
  assign tracePCSample_clock = clock;
  assign tracePCSample_reset = reset;
  assign tracePCSample_io_pcControlReg_pcActive = pcControlReg_pcActive; // @[TracePCSample.scala 19:28]
  assign tracePCSample_io_pcSampleRdEn = out_rivalid_65 & out_rimask_33; // @[TraceEncoder.scala 222:31]
  assign tracePCSample_io_pcSampleHRdEn = out_rivalid_65 & out_rimask_29; // @[TraceEncoder.scala 222:31]
  assign tracePCSample_io_ingPcCapture_valid = traceFrontend_io_pcCapture_valid; // @[TraceEncoder.scala 672:25]
  assign tracePCSample_io_ingPcCapture_bits = traceFrontend_io_pcCapture_bits; // @[TraceEncoder.scala 672:25]
  assign tracePCSample_io_ingPcSample_valid = traceFrontend_io_pcSample_valid; // @[TraceEncoder.scala 673:25]
  assign tracePCSample_io_ingPcSample_bits = traceFrontend_io_pcSample_bits; // @[TraceEncoder.scala 673:25]
  assign traceFrontend_io_ingControlAsync_source_clock = clock;
  assign traceFrontend_io_ingControlAsync_source_reset = reset;
  assign traceFrontend_io_ingControlAsync_source_io_enq_valid = ingControlValid; // @[TraceEncoder.scala 473:26 TraceEncoder.scala 513:22]
  assign traceFrontend_io_ingControlAsync_source_io_enq_bits_teEnable = teControlReg_teEnable; // @[TraceEncoder.scala 473:26 TraceEncoder.scala 507:30]
  assign traceFrontend_io_ingControlAsync_source_io_enq_bits_teTracing = teControlReg_teTracing; // @[TraceEncoder.scala 473:26 TraceEncoder.scala 508:31]
  assign traceFrontend_io_ingControlAsync_source_io_enq_bits_teInstruction = teControlReg_teInstruction; // @[TraceEncoder.scala 473:26 TraceEncoder.scala 509:35]
  assign traceFrontend_io_ingControlAsync_source_io_enq_bits_teStallEnable = teControlReg_teStallEnable; // @[TraceEncoder.scala 473:26 TraceEncoder.scala 510:35]
  assign traceFrontend_io_ingControlAsync_source_io_enq_bits_teSyncMaxInst = teControlReg_teSyncMaxInst; // @[TraceEncoder.scala 473:26 TraceEncoder.scala 511:35]
  assign traceFrontend_io_ingControlAsync_source_io_enq_bits_teSyncMaxBTM = teControlReg_teSyncMaxBTM; // @[TraceEncoder.scala 473:26 TraceEncoder.scala 512:35]
  assign traceFrontend_io_ingControlAsync_source_io_async_ridx = traceFrontend_io_ingControlAsync_ridx; // @[TraceEncoder.scala 603:51]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teSink <= 4'h8;
    end else if (out_f_woready & ~teControlWrData_teActive) begin
      teControlReg_teSink <= 4'h8;
    end else if (out_f_woready_15 & teControlWrData_teSink == 4'h8) begin
      teControlReg_teSink <= teControlWrData_teSink;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teSinkError <= 1'h0;
    end else if (out_f_woready & ~teControlWrData_teActive) begin
      teControlReg_teSinkError <= 1'h0;
    end else if (out_f_woready_14 & teControlWrData_teSinkError) begin
      teControlReg_teSinkError <= 1'h0;
    end else begin
      teControlReg_teSinkError <= _GEN_8;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teSyncMaxInst <= 4'ha;
    end else if (out_f_woready & ~teControlWrData_teActive) begin
      teControlReg_teSyncMaxInst <= 4'ha;
    end else if (out_f_woready_12 & teControlWrData_teSyncMaxInst <= 4'ha) begin
      teControlReg_teSyncMaxInst <= teControlWrData_teSyncMaxInst;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teSyncMaxBTM <= 4'h3;
    end else if (out_f_woready & ~teControlWrData_teActive) begin
      teControlReg_teSyncMaxBTM <= 4'h3;
    end else if (out_f_woready_11 & teControlWrData_teSyncMaxBTM <= 4'hb) begin
      teControlReg_teSyncMaxBTM <= teControlWrData_teSyncMaxBTM;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teInhibitSrc <= 1'h0;
    end else if (out_f_wivalid_10) begin
      teControlReg_teInhibitSrc <= wpControlWrData_15;
    end else if (out_f_woready & ~teControlWrData_teActive) begin
      teControlReg_teInhibitSrc <= 1'h0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teStallEnable <= 1'h0;
    end else if (out_f_wivalid_8) begin
      teControlReg_teStallEnable <= wpControlWrData_13;
    end else if (out_f_woready & ~teControlWrData_teActive) begin
      teControlReg_teStallEnable <= 1'h0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teStallOccurred <= 1'h0;
    end else begin
      teControlReg_teStallOccurred <= _T_22 | traceFrontend_io_traceStall | _GEN_18;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teInstruction <= 3'h0;
    end else if (out_f_woready & ~teControlWrData_teActive) begin
      teControlReg_teInstruction <= 3'h0;
    end else if (out_f_woready_4 & teControlWrData_teInstruction[2:1] == 2'h3) begin
      teControlReg_teInstruction <= teControlWrData_teInstruction;
    end else if (out_f_woready_4 & (teControlWrData_teInstruction == 3'h0 | teControlWrData_teInstruction == 3'h1)
      ) begin
      teControlReg_teInstruction <= teControlWrData_teInstruction;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teTracing <= 1'h0;
    end else if (traceFrontend_io_traceOn ^ traceOnReg) begin
      teControlReg_teTracing <= traceFrontend_io_traceOn;
    end else if (out_f_woready & ~teControlWrData_teActive) begin
      teControlReg_teTracing <= 1'h0;
    end else if (out_f_woready_7) begin
      teControlReg_teTracing <= teControlWrData_teTracing & teControlWrData_teEnable;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teEnable <= 1'h0;
    end else if (out_f_woready & ~teControlWrData_teActive) begin
      teControlReg_teEnable <= 1'h0;
    end else if (out_f_woready_1) begin
      teControlReg_teEnable <= teControlWrData_teEnable;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      teControlReg_teActive <= 1'h0;
    end else if (out_f_woready & ~teControlWrData_teActive) begin
      teControlReg_teActive <= 1'h0;
    end else if (out_f_woready) begin
      teControlReg_teActive <= teControlWrData_teActive;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      ingControlValid <= 1'h0;
    end else if (_T_16) begin
      ingControlValid <= 1'h0;
    end else begin
      ingControlValid <= _ingControlValid_T_9;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      traceOnReg <= 1'h0;
    end else if (_T_16) begin
      traceOnReg <= 1'h0;
    end else begin
      traceOnReg <= traceFrontend_io_traceOn;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_0 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_0 <= 1'h0;
    end else if (out_f_woready_34) begin
      wpControlReg_0 <= teControlWrData_teActive;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_1 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_1 <= 1'h0;
    end else if (out_f_woready_35) begin
      wpControlReg_1 <= teControlWrData_teEnable;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_2 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_2 <= 1'h0;
    end else if (out_f_woready_36) begin
      wpControlReg_2 <= teControlWrData_teTracing;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_3 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_3 <= 1'h0;
    end else if (out_f_woready_37) begin
      wpControlReg_3 <= wpControlWrData_3;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_4 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_4 <= 1'h0;
    end else if (out_f_woready_38) begin
      wpControlReg_4 <= wpControlWrData_4;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_5 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_5 <= 1'h0;
    end else if (out_f_woready_39) begin
      wpControlReg_5 <= wpControlWrData_5;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_6 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_6 <= 1'h0;
    end else if (out_f_woready_40) begin
      wpControlReg_6 <= wpControlWrData_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_7 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_7 <= 1'h0;
    end else if (out_f_woready_41) begin
      wpControlReg_7 <= wpControlWrData_7;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_8 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_8 <= 1'h0;
    end else if (out_f_woready_42) begin
      wpControlReg_8 <= wpControlWrData_8;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_9 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_9 <= 1'h0;
    end else if (out_f_woready_43) begin
      wpControlReg_9 <= wpControlWrData_9;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_10 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_10 <= 1'h0;
    end else if (out_f_woready_44) begin
      wpControlReg_10 <= wpControlWrData_10;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_11 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_11 <= 1'h0;
    end else if (out_f_woready_45) begin
      wpControlReg_11 <= wpControlWrData_11;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_12 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_12 <= 1'h0;
    end else if (out_f_woready_46) begin
      wpControlReg_12 <= teControlWrData_teStallOccurred;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_13 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_13 <= 1'h0;
    end else if (out_f_woready_47) begin
      wpControlReg_13 <= wpControlWrData_13;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_14 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_14 <= 1'h0;
    end else if (out_f_woready_48) begin
      wpControlReg_14 <= wpControlWrData_14;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wpControlReg_15 <= 1'h0;
    end else if (_T_16) begin
      wpControlReg_15 <= 1'h0;
    end else if (out_f_woready_49) begin
      wpControlReg_15 <= wpControlWrData_15;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      tsControlReg_tsBranch <= 2'h0;
    end else if (_T_16 | out_f_woready_50 & _T) begin
      tsControlReg_tsBranch <= 2'h0;
    end else if (out_f_woready_59 & tsControlWrData_tsBranch != 2'h2) begin
      tsControlReg_tsBranch <= tsControlWrData_tsBranch;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      tsControlReg_tsEnable <= 1'h0;
    end else if (out_f_wivalid_58) begin
      tsControlReg_tsEnable <= wpControlWrData_15;
    end else if (_T_16 | out_f_woready_50 & _T) begin
      tsControlReg_tsEnable <= 1'h0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      tsControlReg_tsActive <= 1'h0;
    end else if (_T_16 | out_f_woready_50 & _T) begin
      tsControlReg_tsActive <= 1'h0;
    end else if (out_f_woready_50) begin
      tsControlReg_tsActive <= teControlWrData_teActive;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      pcControlReg_pcActive <= 1'h0;
    end else if (out_f_wivalid_30) begin
      pcControlReg_pcActive <= teControlWrData_teActive;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  teControlReg_teSink = _RAND_0[3:0];
  _RAND_1 = {1{`RANDOM}};
  teControlReg_teSinkError = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  teControlReg_teSyncMaxInst = _RAND_2[3:0];
  _RAND_3 = {1{`RANDOM}};
  teControlReg_teSyncMaxBTM = _RAND_3[3:0];
  _RAND_4 = {1{`RANDOM}};
  teControlReg_teInhibitSrc = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  teControlReg_teStallEnable = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  teControlReg_teStallOccurred = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  teControlReg_teInstruction = _RAND_7[2:0];
  _RAND_8 = {1{`RANDOM}};
  teControlReg_teTracing = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  teControlReg_teEnable = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  teControlReg_teActive = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  ingControlValid = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  traceOnReg = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  wpControlReg_0 = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  wpControlReg_1 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  wpControlReg_2 = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  wpControlReg_3 = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  wpControlReg_4 = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  wpControlReg_5 = _RAND_18[0:0];
  _RAND_19 = {1{`RANDOM}};
  wpControlReg_6 = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  wpControlReg_7 = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  wpControlReg_8 = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  wpControlReg_9 = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  wpControlReg_10 = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  wpControlReg_11 = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  wpControlReg_12 = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  wpControlReg_13 = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  wpControlReg_14 = _RAND_27[0:0];
  _RAND_28 = {1{`RANDOM}};
  wpControlReg_15 = _RAND_28[0:0];
  _RAND_29 = {1{`RANDOM}};
  tsControlReg_tsBranch = _RAND_29[1:0];
  _RAND_30 = {1{`RANDOM}};
  tsControlReg_tsEnable = _RAND_30[0:0];
  _RAND_31 = {1{`RANDOM}};
  tsControlReg_tsActive = _RAND_31[0:0];
  _RAND_32 = {1{`RANDOM}};
  pcControlReg_pcActive = _RAND_32[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    teControlReg_teSink = 4'h8;
  end
  if (reset) begin
    teControlReg_teSinkError = 1'h0;
  end
  if (reset) begin
    teControlReg_teSyncMaxInst = 4'ha;
  end
  if (reset) begin
    teControlReg_teSyncMaxBTM = 4'h3;
  end
  if (reset) begin
    teControlReg_teInhibitSrc = 1'h0;
  end
  if (reset) begin
    teControlReg_teStallEnable = 1'h0;
  end
  if (reset) begin
    teControlReg_teStallOccurred = 1'h0;
  end
  if (reset) begin
    teControlReg_teInstruction = 3'h0;
  end
  if (reset) begin
    teControlReg_teTracing = 1'h0;
  end
  if (reset) begin
    teControlReg_teEnable = 1'h0;
  end
  if (reset) begin
    teControlReg_teActive = 1'h0;
  end
  if (reset) begin
    ingControlValid = 1'h0;
  end
  if (reset) begin
    traceOnReg = 1'h0;
  end
  if (reset) begin
    wpControlReg_0 = 1'h0;
  end
  if (reset) begin
    wpControlReg_1 = 1'h0;
  end
  if (reset) begin
    wpControlReg_2 = 1'h0;
  end
  if (reset) begin
    wpControlReg_3 = 1'h0;
  end
  if (reset) begin
    wpControlReg_4 = 1'h0;
  end
  if (reset) begin
    wpControlReg_5 = 1'h0;
  end
  if (reset) begin
    wpControlReg_6 = 1'h0;
  end
  if (reset) begin
    wpControlReg_7 = 1'h0;
  end
  if (reset) begin
    wpControlReg_8 = 1'h0;
  end
  if (reset) begin
    wpControlReg_9 = 1'h0;
  end
  if (reset) begin
    wpControlReg_10 = 1'h0;
  end
  if (reset) begin
    wpControlReg_11 = 1'h0;
  end
  if (reset) begin
    wpControlReg_12 = 1'h0;
  end
  if (reset) begin
    wpControlReg_13 = 1'h0;
  end
  if (reset) begin
    wpControlReg_14 = 1'h0;
  end
  if (reset) begin
    wpControlReg_15 = 1'h0;
  end
  if (reset) begin
    tsControlReg_tsBranch = 2'h0;
  end
  if (reset) begin
    tsControlReg_tsEnable = 1'h0;
  end
  if (reset) begin
    tsControlReg_tsActive = 1'h0;
  end
  if (reset) begin
    pcControlReg_pcActive = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
