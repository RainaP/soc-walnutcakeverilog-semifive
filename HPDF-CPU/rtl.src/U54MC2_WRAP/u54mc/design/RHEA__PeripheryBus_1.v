//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__PeripheryBus_1(
  input         rf_reset,
  input         auto_coupler_to_testIndicator_control_xing_out_a_ready,
  output        auto_coupler_to_testIndicator_control_xing_out_a_valid,
  output [2:0]  auto_coupler_to_testIndicator_control_xing_out_a_bits_opcode,
  output [2:0]  auto_coupler_to_testIndicator_control_xing_out_a_bits_param,
  output [1:0]  auto_coupler_to_testIndicator_control_xing_out_a_bits_size,
  output [11:0] auto_coupler_to_testIndicator_control_xing_out_a_bits_source,
  output [14:0] auto_coupler_to_testIndicator_control_xing_out_a_bits_address,
  output [3:0]  auto_coupler_to_testIndicator_control_xing_out_a_bits_mask,
  output [31:0] auto_coupler_to_testIndicator_control_xing_out_a_bits_data,
  output        auto_coupler_to_testIndicator_control_xing_out_a_bits_corrupt,
  output        auto_coupler_to_testIndicator_control_xing_out_d_ready,
  input         auto_coupler_to_testIndicator_control_xing_out_d_valid,
  input  [2:0]  auto_coupler_to_testIndicator_control_xing_out_d_bits_opcode,
  input  [1:0]  auto_coupler_to_testIndicator_control_xing_out_d_bits_size,
  input  [11:0] auto_coupler_to_testIndicator_control_xing_out_d_bits_source,
  input  [31:0] auto_coupler_to_testIndicator_control_xing_out_d_bits_data,
  input         auto_coupler_to_tracefunnel_fragmenter_out_a_ready,
  output        auto_coupler_to_tracefunnel_fragmenter_out_a_valid,
  output [2:0]  auto_coupler_to_tracefunnel_fragmenter_out_a_bits_opcode,
  output [2:0]  auto_coupler_to_tracefunnel_fragmenter_out_a_bits_param,
  output [1:0]  auto_coupler_to_tracefunnel_fragmenter_out_a_bits_size,
  output [10:0] auto_coupler_to_tracefunnel_fragmenter_out_a_bits_source,
  output [28:0] auto_coupler_to_tracefunnel_fragmenter_out_a_bits_address,
  output [7:0]  auto_coupler_to_tracefunnel_fragmenter_out_a_bits_mask,
  output [63:0] auto_coupler_to_tracefunnel_fragmenter_out_a_bits_data,
  output        auto_coupler_to_tracefunnel_fragmenter_out_a_bits_corrupt,
  output        auto_coupler_to_tracefunnel_fragmenter_out_d_ready,
  input         auto_coupler_to_tracefunnel_fragmenter_out_d_valid,
  input  [2:0]  auto_coupler_to_tracefunnel_fragmenter_out_d_bits_opcode,
  input  [1:0]  auto_coupler_to_tracefunnel_fragmenter_out_d_bits_size,
  input  [10:0] auto_coupler_to_tracefunnel_fragmenter_out_d_bits_source,
  input  [63:0] auto_coupler_to_tracefunnel_fragmenter_out_d_bits_data,
  input         auto_coupler_to_traceencoder_fragmenter_out_1_a_ready,
  output        auto_coupler_to_traceencoder_fragmenter_out_1_a_valid,
  output [2:0]  auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_opcode,
  output [2:0]  auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_param,
  output [1:0]  auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_size,
  output [10:0] auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_source,
  output [28:0] auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_address,
  output [7:0]  auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_mask,
  output [63:0] auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_data,
  output        auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_corrupt,
  output        auto_coupler_to_traceencoder_fragmenter_out_1_d_ready,
  input         auto_coupler_to_traceencoder_fragmenter_out_1_d_valid,
  input  [2:0]  auto_coupler_to_traceencoder_fragmenter_out_1_d_bits_opcode,
  input  [1:0]  auto_coupler_to_traceencoder_fragmenter_out_1_d_bits_size,
  input  [10:0] auto_coupler_to_traceencoder_fragmenter_out_1_d_bits_source,
  input  [63:0] auto_coupler_to_traceencoder_fragmenter_out_1_d_bits_data,
  input         auto_coupler_to_traceencoder_fragmenter_out_0_a_ready,
  output        auto_coupler_to_traceencoder_fragmenter_out_0_a_valid,
  output [2:0]  auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_opcode,
  output [2:0]  auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_param,
  output [1:0]  auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_size,
  output [10:0] auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_source,
  output [28:0] auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_address,
  output [7:0]  auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_mask,
  output [63:0] auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_data,
  output        auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_corrupt,
  output        auto_coupler_to_traceencoder_fragmenter_out_0_d_ready,
  input         auto_coupler_to_traceencoder_fragmenter_out_0_d_valid,
  input  [2:0]  auto_coupler_to_traceencoder_fragmenter_out_0_d_bits_opcode,
  input  [1:0]  auto_coupler_to_traceencoder_fragmenter_out_0_d_bits_size,
  input  [10:0] auto_coupler_to_traceencoder_fragmenter_out_0_d_bits_source,
  input  [63:0] auto_coupler_to_traceencoder_fragmenter_out_0_d_bits_data,
  input         auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_aw_ready,
  output        auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_aw_valid,
  output [31:0] auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_aw_bits_addr,
  output [7:0]  auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_aw_bits_len,
  output [2:0]  auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_aw_bits_size,
  output [3:0]  auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_aw_bits_cache,
  output [2:0]  auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_aw_bits_prot,
  input         auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_w_ready,
  output        auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_w_valid,
  output [63:0] auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_w_bits_data,
  output [7:0]  auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_w_bits_strb,
  output        auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_w_bits_last,
  output        auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_b_ready,
  input         auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_b_valid,
  input  [1:0]  auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_b_bits_resp,
  input         auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_ar_ready,
  output        auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_ar_valid,
  output [31:0] auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_ar_bits_addr,
  output [7:0]  auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_ar_bits_len,
  output [2:0]  auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_ar_bits_size,
  output [3:0]  auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_ar_bits_cache,
  output [2:0]  auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_ar_bits_prot,
  output        auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_r_ready,
  input         auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_r_valid,
  input  [63:0] auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_r_bits_data,
  input  [1:0]  auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_r_bits_resp,
  input         auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_r_bits_last,
  input         auto_coupler_to_debug_fragmenter_out_a_ready,
  output        auto_coupler_to_debug_fragmenter_out_a_valid,
  output [2:0]  auto_coupler_to_debug_fragmenter_out_a_bits_opcode,
  output [2:0]  auto_coupler_to_debug_fragmenter_out_a_bits_param,
  output [1:0]  auto_coupler_to_debug_fragmenter_out_a_bits_size,
  output [10:0] auto_coupler_to_debug_fragmenter_out_a_bits_source,
  output [11:0] auto_coupler_to_debug_fragmenter_out_a_bits_address,
  output [7:0]  auto_coupler_to_debug_fragmenter_out_a_bits_mask,
  output [63:0] auto_coupler_to_debug_fragmenter_out_a_bits_data,
  output        auto_coupler_to_debug_fragmenter_out_a_bits_corrupt,
  output        auto_coupler_to_debug_fragmenter_out_d_ready,
  input         auto_coupler_to_debug_fragmenter_out_d_valid,
  input  [2:0]  auto_coupler_to_debug_fragmenter_out_d_bits_opcode,
  input  [1:0]  auto_coupler_to_debug_fragmenter_out_d_bits_size,
  input  [10:0] auto_coupler_to_debug_fragmenter_out_d_bits_source,
  input  [63:0] auto_coupler_to_debug_fragmenter_out_d_bits_data,
  input         auto_coupler_to_clint_fragmenter_out_a_ready,
  output        auto_coupler_to_clint_fragmenter_out_a_valid,
  output [2:0]  auto_coupler_to_clint_fragmenter_out_a_bits_opcode,
  output [2:0]  auto_coupler_to_clint_fragmenter_out_a_bits_param,
  output [1:0]  auto_coupler_to_clint_fragmenter_out_a_bits_size,
  output [10:0] auto_coupler_to_clint_fragmenter_out_a_bits_source,
  output [25:0] auto_coupler_to_clint_fragmenter_out_a_bits_address,
  output [7:0]  auto_coupler_to_clint_fragmenter_out_a_bits_mask,
  output [63:0] auto_coupler_to_clint_fragmenter_out_a_bits_data,
  output        auto_coupler_to_clint_fragmenter_out_a_bits_corrupt,
  output        auto_coupler_to_clint_fragmenter_out_d_ready,
  input         auto_coupler_to_clint_fragmenter_out_d_valid,
  input  [2:0]  auto_coupler_to_clint_fragmenter_out_d_bits_opcode,
  input  [1:0]  auto_coupler_to_clint_fragmenter_out_d_bits_size,
  input  [10:0] auto_coupler_to_clint_fragmenter_out_d_bits_source,
  input  [63:0] auto_coupler_to_clint_fragmenter_out_d_bits_data,
  input         auto_coupler_to_plic_fragmenter_out_a_ready,
  output        auto_coupler_to_plic_fragmenter_out_a_valid,
  output [2:0]  auto_coupler_to_plic_fragmenter_out_a_bits_opcode,
  output [2:0]  auto_coupler_to_plic_fragmenter_out_a_bits_param,
  output [1:0]  auto_coupler_to_plic_fragmenter_out_a_bits_size,
  output [10:0] auto_coupler_to_plic_fragmenter_out_a_bits_source,
  output [27:0] auto_coupler_to_plic_fragmenter_out_a_bits_address,
  output [7:0]  auto_coupler_to_plic_fragmenter_out_a_bits_mask,
  output [63:0] auto_coupler_to_plic_fragmenter_out_a_bits_data,
  output        auto_coupler_to_plic_fragmenter_out_a_bits_corrupt,
  output        auto_coupler_to_plic_fragmenter_out_d_ready,
  input         auto_coupler_to_plic_fragmenter_out_d_valid,
  input  [2:0]  auto_coupler_to_plic_fragmenter_out_d_bits_opcode,
  input  [1:0]  auto_coupler_to_plic_fragmenter_out_d_bits_size,
  input  [10:0] auto_coupler_to_plic_fragmenter_out_d_bits_source,
  input  [63:0] auto_coupler_to_plic_fragmenter_out_d_bits_data,
  input         auto_coupler_to_l2_ctrl_buffer_out_a_ready,
  output        auto_coupler_to_l2_ctrl_buffer_out_a_valid,
  output [2:0]  auto_coupler_to_l2_ctrl_buffer_out_a_bits_opcode,
  output [2:0]  auto_coupler_to_l2_ctrl_buffer_out_a_bits_param,
  output [1:0]  auto_coupler_to_l2_ctrl_buffer_out_a_bits_size,
  output [10:0] auto_coupler_to_l2_ctrl_buffer_out_a_bits_source,
  output [25:0] auto_coupler_to_l2_ctrl_buffer_out_a_bits_address,
  output [7:0]  auto_coupler_to_l2_ctrl_buffer_out_a_bits_mask,
  output [63:0] auto_coupler_to_l2_ctrl_buffer_out_a_bits_data,
  output        auto_coupler_to_l2_ctrl_buffer_out_a_bits_corrupt,
  output        auto_coupler_to_l2_ctrl_buffer_out_d_ready,
  input         auto_coupler_to_l2_ctrl_buffer_out_d_valid,
  input  [2:0]  auto_coupler_to_l2_ctrl_buffer_out_d_bits_opcode,
  input  [1:0]  auto_coupler_to_l2_ctrl_buffer_out_d_bits_size,
  input  [10:0] auto_coupler_to_l2_ctrl_buffer_out_d_bits_source,
  input  [63:0] auto_coupler_to_l2_ctrl_buffer_out_d_bits_data,
  output        auto_fixedClockNode_out_1_clock,
  output        auto_fixedClockNode_out_1_reset,
  output        auto_fixedClockNode_out_0_clock,
  output        auto_fixedClockNode_out_0_reset,
  input         auto_subsystem_cbus_clock_groups_in_member_subsystem_cbus_0_clock,
  input         auto_subsystem_cbus_clock_groups_in_member_subsystem_cbus_0_reset,
  output        auto_bus_xing_in_a_ready,
  input         auto_bus_xing_in_a_valid,
  input  [2:0]  auto_bus_xing_in_a_bits_opcode,
  input  [2:0]  auto_bus_xing_in_a_bits_param,
  input  [3:0]  auto_bus_xing_in_a_bits_size,
  input  [6:0]  auto_bus_xing_in_a_bits_source,
  input  [31:0] auto_bus_xing_in_a_bits_address,
  input         auto_bus_xing_in_a_bits_user_amba_prot_bufferable,
  input         auto_bus_xing_in_a_bits_user_amba_prot_modifiable,
  input         auto_bus_xing_in_a_bits_user_amba_prot_readalloc,
  input         auto_bus_xing_in_a_bits_user_amba_prot_writealloc,
  input         auto_bus_xing_in_a_bits_user_amba_prot_privileged,
  input         auto_bus_xing_in_a_bits_user_amba_prot_secure,
  input         auto_bus_xing_in_a_bits_user_amba_prot_fetch,
  input  [7:0]  auto_bus_xing_in_a_bits_mask,
  input  [63:0] auto_bus_xing_in_a_bits_data,
  input         auto_bus_xing_in_a_bits_corrupt,
  input         auto_bus_xing_in_d_ready,
  output        auto_bus_xing_in_d_valid,
  output [2:0]  auto_bus_xing_in_d_bits_opcode,
  output [1:0]  auto_bus_xing_in_d_bits_param,
  output [3:0]  auto_bus_xing_in_d_bits_size,
  output [6:0]  auto_bus_xing_in_d_bits_source,
  output        auto_bus_xing_in_d_bits_sink,
  output        auto_bus_xing_in_d_bits_denied,
  output [63:0] auto_bus_xing_in_d_bits_data,
  output        auto_bus_xing_in_d_bits_corrupt,
  output        clock,
  output        reset
);
  wire  subsystem_cbus_clock_groups_auto_in_member_subsystem_cbus_0_clock;
  wire  subsystem_cbus_clock_groups_auto_in_member_subsystem_cbus_0_reset;
  wire  subsystem_cbus_clock_groups_auto_out_0_member_subsystem_cbus_0_clock;
  wire  subsystem_cbus_clock_groups_auto_out_0_member_subsystem_cbus_0_reset;
  wire  clockGroup_auto_in_member_subsystem_cbus_0_clock;
  wire  clockGroup_auto_in_member_subsystem_cbus_0_reset;
  wire  clockGroup_auto_out_clock;
  wire  clockGroup_auto_out_reset;
  wire  fixedClockNode_auto_in_clock; // @[ClockGroup.scala 106:107]
  wire  fixedClockNode_auto_in_reset; // @[ClockGroup.scala 106:107]
  wire  fixedClockNode_auto_out_2_clock; // @[ClockGroup.scala 106:107]
  wire  fixedClockNode_auto_out_2_reset; // @[ClockGroup.scala 106:107]
  wire  fixedClockNode_auto_out_1_clock; // @[ClockGroup.scala 106:107]
  wire  fixedClockNode_auto_out_1_reset; // @[ClockGroup.scala 106:107]
  wire  fixedClockNode_auto_out_0_clock; // @[ClockGroup.scala 106:107]
  wire  fixedClockNode_auto_out_0_reset; // @[ClockGroup.scala 106:107]
  wire  fixer_rf_reset; // @[PeripheryBus.scala 47:33]
  wire  fixer_clock; // @[PeripheryBus.scala 47:33]
  wire  fixer_reset; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_in_a_ready; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_in_a_valid; // @[PeripheryBus.scala 47:33]
  wire [2:0] fixer_auto_in_a_bits_opcode; // @[PeripheryBus.scala 47:33]
  wire [2:0] fixer_auto_in_a_bits_param; // @[PeripheryBus.scala 47:33]
  wire [3:0] fixer_auto_in_a_bits_size; // @[PeripheryBus.scala 47:33]
  wire [6:0] fixer_auto_in_a_bits_source; // @[PeripheryBus.scala 47:33]
  wire [31:0] fixer_auto_in_a_bits_address; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_in_a_bits_user_amba_prot_bufferable; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_in_a_bits_user_amba_prot_modifiable; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_in_a_bits_user_amba_prot_readalloc; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_in_a_bits_user_amba_prot_writealloc; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_in_a_bits_user_amba_prot_privileged; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_in_a_bits_user_amba_prot_secure; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_in_a_bits_user_amba_prot_fetch; // @[PeripheryBus.scala 47:33]
  wire [7:0] fixer_auto_in_a_bits_mask; // @[PeripheryBus.scala 47:33]
  wire [63:0] fixer_auto_in_a_bits_data; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_in_a_bits_corrupt; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_in_d_ready; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_in_d_valid; // @[PeripheryBus.scala 47:33]
  wire [2:0] fixer_auto_in_d_bits_opcode; // @[PeripheryBus.scala 47:33]
  wire [1:0] fixer_auto_in_d_bits_param; // @[PeripheryBus.scala 47:33]
  wire [3:0] fixer_auto_in_d_bits_size; // @[PeripheryBus.scala 47:33]
  wire [6:0] fixer_auto_in_d_bits_source; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_in_d_bits_sink; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_in_d_bits_denied; // @[PeripheryBus.scala 47:33]
  wire [63:0] fixer_auto_in_d_bits_data; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_in_d_bits_corrupt; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_out_a_ready; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_out_a_valid; // @[PeripheryBus.scala 47:33]
  wire [2:0] fixer_auto_out_a_bits_opcode; // @[PeripheryBus.scala 47:33]
  wire [2:0] fixer_auto_out_a_bits_param; // @[PeripheryBus.scala 47:33]
  wire [3:0] fixer_auto_out_a_bits_size; // @[PeripheryBus.scala 47:33]
  wire [6:0] fixer_auto_out_a_bits_source; // @[PeripheryBus.scala 47:33]
  wire [31:0] fixer_auto_out_a_bits_address; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_out_a_bits_user_amba_prot_bufferable; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_out_a_bits_user_amba_prot_modifiable; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_out_a_bits_user_amba_prot_readalloc; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_out_a_bits_user_amba_prot_writealloc; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_out_a_bits_user_amba_prot_privileged; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_out_a_bits_user_amba_prot_secure; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_out_a_bits_user_amba_prot_fetch; // @[PeripheryBus.scala 47:33]
  wire [7:0] fixer_auto_out_a_bits_mask; // @[PeripheryBus.scala 47:33]
  wire [63:0] fixer_auto_out_a_bits_data; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_out_a_bits_corrupt; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_out_d_ready; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_out_d_valid; // @[PeripheryBus.scala 47:33]
  wire [2:0] fixer_auto_out_d_bits_opcode; // @[PeripheryBus.scala 47:33]
  wire [1:0] fixer_auto_out_d_bits_param; // @[PeripheryBus.scala 47:33]
  wire [3:0] fixer_auto_out_d_bits_size; // @[PeripheryBus.scala 47:33]
  wire [6:0] fixer_auto_out_d_bits_source; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_out_d_bits_sink; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_out_d_bits_denied; // @[PeripheryBus.scala 47:33]
  wire [63:0] fixer_auto_out_d_bits_data; // @[PeripheryBus.scala 47:33]
  wire  fixer_auto_out_d_bits_corrupt; // @[PeripheryBus.scala 47:33]
  wire  in_xbar_auto_in_a_ready;
  wire  in_xbar_auto_in_a_valid;
  wire [2:0] in_xbar_auto_in_a_bits_opcode;
  wire [2:0] in_xbar_auto_in_a_bits_param;
  wire [3:0] in_xbar_auto_in_a_bits_size;
  wire [6:0] in_xbar_auto_in_a_bits_source;
  wire [31:0] in_xbar_auto_in_a_bits_address;
  wire  in_xbar_auto_in_a_bits_user_amba_prot_bufferable;
  wire  in_xbar_auto_in_a_bits_user_amba_prot_modifiable;
  wire  in_xbar_auto_in_a_bits_user_amba_prot_readalloc;
  wire  in_xbar_auto_in_a_bits_user_amba_prot_writealloc;
  wire  in_xbar_auto_in_a_bits_user_amba_prot_privileged;
  wire  in_xbar_auto_in_a_bits_user_amba_prot_secure;
  wire  in_xbar_auto_in_a_bits_user_amba_prot_fetch;
  wire [7:0] in_xbar_auto_in_a_bits_mask;
  wire [63:0] in_xbar_auto_in_a_bits_data;
  wire  in_xbar_auto_in_a_bits_corrupt;
  wire  in_xbar_auto_in_d_ready;
  wire  in_xbar_auto_in_d_valid;
  wire [2:0] in_xbar_auto_in_d_bits_opcode;
  wire [1:0] in_xbar_auto_in_d_bits_param;
  wire [3:0] in_xbar_auto_in_d_bits_size;
  wire [6:0] in_xbar_auto_in_d_bits_source;
  wire  in_xbar_auto_in_d_bits_sink;
  wire  in_xbar_auto_in_d_bits_denied;
  wire [63:0] in_xbar_auto_in_d_bits_data;
  wire  in_xbar_auto_in_d_bits_corrupt;
  wire  in_xbar_auto_out_a_ready;
  wire  in_xbar_auto_out_a_valid;
  wire [2:0] in_xbar_auto_out_a_bits_opcode;
  wire [2:0] in_xbar_auto_out_a_bits_param;
  wire [3:0] in_xbar_auto_out_a_bits_size;
  wire [6:0] in_xbar_auto_out_a_bits_source;
  wire [31:0] in_xbar_auto_out_a_bits_address;
  wire  in_xbar_auto_out_a_bits_user_amba_prot_bufferable;
  wire  in_xbar_auto_out_a_bits_user_amba_prot_modifiable;
  wire  in_xbar_auto_out_a_bits_user_amba_prot_readalloc;
  wire  in_xbar_auto_out_a_bits_user_amba_prot_writealloc;
  wire  in_xbar_auto_out_a_bits_user_amba_prot_privileged;
  wire  in_xbar_auto_out_a_bits_user_amba_prot_secure;
  wire  in_xbar_auto_out_a_bits_user_amba_prot_fetch;
  wire [7:0] in_xbar_auto_out_a_bits_mask;
  wire [63:0] in_xbar_auto_out_a_bits_data;
  wire  in_xbar_auto_out_a_bits_corrupt;
  wire  in_xbar_auto_out_d_ready;
  wire  in_xbar_auto_out_d_valid;
  wire [2:0] in_xbar_auto_out_d_bits_opcode;
  wire [1:0] in_xbar_auto_out_d_bits_param;
  wire [3:0] in_xbar_auto_out_d_bits_size;
  wire [6:0] in_xbar_auto_out_d_bits_source;
  wire  in_xbar_auto_out_d_bits_sink;
  wire  in_xbar_auto_out_d_bits_denied;
  wire [63:0] in_xbar_auto_out_d_bits_data;
  wire  in_xbar_auto_out_d_bits_corrupt;
  wire  out_xbar_rf_reset; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_clock; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_reset; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_in_a_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_in_a_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_in_a_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_in_a_bits_param; // @[PeripheryBus.scala 50:30]
  wire [3:0] out_xbar_auto_in_a_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_in_a_bits_source; // @[PeripheryBus.scala 50:30]
  wire [31:0] out_xbar_auto_in_a_bits_address; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_in_a_bits_user_amba_prot_bufferable; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_in_a_bits_user_amba_prot_modifiable; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_in_a_bits_user_amba_prot_readalloc; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_in_a_bits_user_amba_prot_writealloc; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_in_a_bits_user_amba_prot_privileged; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_in_a_bits_user_amba_prot_secure; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_in_a_bits_user_amba_prot_fetch; // @[PeripheryBus.scala 50:30]
  wire [7:0] out_xbar_auto_in_a_bits_mask; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_in_a_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_in_a_bits_corrupt; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_in_d_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_in_d_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_in_d_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [1:0] out_xbar_auto_in_d_bits_param; // @[PeripheryBus.scala 50:30]
  wire [3:0] out_xbar_auto_in_d_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_in_d_bits_source; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_in_d_bits_sink; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_in_d_bits_denied; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_in_d_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_in_d_bits_corrupt; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_9_a_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_9_a_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_9_a_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_9_a_bits_param; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_9_a_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_9_a_bits_source; // @[PeripheryBus.scala 50:30]
  wire [14:0] out_xbar_auto_out_9_a_bits_address; // @[PeripheryBus.scala 50:30]
  wire [7:0] out_xbar_auto_out_9_a_bits_mask; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_9_a_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_9_a_bits_corrupt; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_9_d_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_9_d_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_9_d_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_9_d_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_9_d_bits_source; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_9_d_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_8_a_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_8_a_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_8_a_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_8_a_bits_param; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_8_a_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_8_a_bits_source; // @[PeripheryBus.scala 50:30]
  wire [28:0] out_xbar_auto_out_8_a_bits_address; // @[PeripheryBus.scala 50:30]
  wire [7:0] out_xbar_auto_out_8_a_bits_mask; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_8_a_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_8_a_bits_corrupt; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_8_d_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_8_d_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_8_d_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_8_d_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_8_d_bits_source; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_8_d_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_7_a_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_7_a_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_7_a_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_7_a_bits_param; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_7_a_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_7_a_bits_source; // @[PeripheryBus.scala 50:30]
  wire [28:0] out_xbar_auto_out_7_a_bits_address; // @[PeripheryBus.scala 50:30]
  wire [7:0] out_xbar_auto_out_7_a_bits_mask; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_7_a_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_7_a_bits_corrupt; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_7_d_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_7_d_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_7_d_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_7_d_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_7_d_bits_source; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_7_d_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_6_a_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_6_a_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_6_a_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_6_a_bits_param; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_6_a_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_6_a_bits_source; // @[PeripheryBus.scala 50:30]
  wire [28:0] out_xbar_auto_out_6_a_bits_address; // @[PeripheryBus.scala 50:30]
  wire [7:0] out_xbar_auto_out_6_a_bits_mask; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_6_a_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_6_a_bits_corrupt; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_6_d_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_6_d_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_6_d_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_6_d_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_6_d_bits_source; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_6_d_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_5_a_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_5_a_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_5_a_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_5_a_bits_param; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_5_a_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_5_a_bits_source; // @[PeripheryBus.scala 50:30]
  wire [31:0] out_xbar_auto_out_5_a_bits_address; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_5_a_bits_user_amba_prot_bufferable; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_5_a_bits_user_amba_prot_modifiable; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_5_a_bits_user_amba_prot_readalloc; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_5_a_bits_user_amba_prot_writealloc; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_5_a_bits_user_amba_prot_privileged; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_5_a_bits_user_amba_prot_secure; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_5_a_bits_user_amba_prot_fetch; // @[PeripheryBus.scala 50:30]
  wire [7:0] out_xbar_auto_out_5_a_bits_mask; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_5_a_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_5_a_bits_corrupt; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_5_d_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_5_d_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_5_d_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_5_d_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_5_d_bits_source; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_5_d_bits_denied; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_5_d_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_5_d_bits_corrupt; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_4_a_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_4_a_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_4_a_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_4_a_bits_param; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_4_a_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_4_a_bits_source; // @[PeripheryBus.scala 50:30]
  wire [11:0] out_xbar_auto_out_4_a_bits_address; // @[PeripheryBus.scala 50:30]
  wire [7:0] out_xbar_auto_out_4_a_bits_mask; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_4_a_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_4_a_bits_corrupt; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_4_d_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_4_d_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_4_d_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_4_d_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_4_d_bits_source; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_4_d_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_3_a_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_3_a_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_3_a_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_3_a_bits_param; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_3_a_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_3_a_bits_source; // @[PeripheryBus.scala 50:30]
  wire [25:0] out_xbar_auto_out_3_a_bits_address; // @[PeripheryBus.scala 50:30]
  wire [7:0] out_xbar_auto_out_3_a_bits_mask; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_3_a_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_3_a_bits_corrupt; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_3_d_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_3_d_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_3_d_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_3_d_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_3_d_bits_source; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_3_d_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_2_a_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_2_a_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_2_a_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_2_a_bits_param; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_2_a_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_2_a_bits_source; // @[PeripheryBus.scala 50:30]
  wire [27:0] out_xbar_auto_out_2_a_bits_address; // @[PeripheryBus.scala 50:30]
  wire [7:0] out_xbar_auto_out_2_a_bits_mask; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_2_a_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_2_a_bits_corrupt; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_2_d_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_2_d_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_2_d_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_2_d_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_2_d_bits_source; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_2_d_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_1_a_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_1_a_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_1_a_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_1_a_bits_param; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_1_a_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_1_a_bits_source; // @[PeripheryBus.scala 50:30]
  wire [25:0] out_xbar_auto_out_1_a_bits_address; // @[PeripheryBus.scala 50:30]
  wire [7:0] out_xbar_auto_out_1_a_bits_mask; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_1_a_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_1_a_bits_corrupt; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_1_d_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_1_d_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_1_d_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_1_d_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_1_d_bits_source; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_1_d_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_0_a_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_0_a_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_0_a_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_0_a_bits_param; // @[PeripheryBus.scala 50:30]
  wire [3:0] out_xbar_auto_out_0_a_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_0_a_bits_source; // @[PeripheryBus.scala 50:30]
  wire [13:0] out_xbar_auto_out_0_a_bits_address; // @[PeripheryBus.scala 50:30]
  wire [7:0] out_xbar_auto_out_0_a_bits_mask; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_0_a_bits_corrupt; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_0_d_ready; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_0_d_valid; // @[PeripheryBus.scala 50:30]
  wire [2:0] out_xbar_auto_out_0_d_bits_opcode; // @[PeripheryBus.scala 50:30]
  wire [1:0] out_xbar_auto_out_0_d_bits_param; // @[PeripheryBus.scala 50:30]
  wire [3:0] out_xbar_auto_out_0_d_bits_size; // @[PeripheryBus.scala 50:30]
  wire [6:0] out_xbar_auto_out_0_d_bits_source; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_0_d_bits_sink; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_0_d_bits_denied; // @[PeripheryBus.scala 50:30]
  wire [63:0] out_xbar_auto_out_0_d_bits_data; // @[PeripheryBus.scala 50:30]
  wire  out_xbar_auto_out_0_d_bits_corrupt; // @[PeripheryBus.scala 50:30]
  wire  buffer_rf_reset; // @[Buffer.scala 68:28]
  wire  buffer_clock; // @[Buffer.scala 68:28]
  wire  buffer_reset; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_in_a_bits_opcode; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_in_a_bits_param; // @[Buffer.scala 68:28]
  wire [3:0] buffer_auto_in_a_bits_size; // @[Buffer.scala 68:28]
  wire [6:0] buffer_auto_in_a_bits_source; // @[Buffer.scala 68:28]
  wire [31:0] buffer_auto_in_a_bits_address; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_user_amba_prot_bufferable; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_user_amba_prot_modifiable; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_user_amba_prot_readalloc; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_user_amba_prot_writealloc; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_user_amba_prot_privileged; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_user_amba_prot_secure; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_user_amba_prot_fetch; // @[Buffer.scala 68:28]
  wire [7:0] buffer_auto_in_a_bits_mask; // @[Buffer.scala 68:28]
  wire [63:0] buffer_auto_in_a_bits_data; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_corrupt; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_d_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_d_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_in_d_bits_opcode; // @[Buffer.scala 68:28]
  wire [1:0] buffer_auto_in_d_bits_param; // @[Buffer.scala 68:28]
  wire [3:0] buffer_auto_in_d_bits_size; // @[Buffer.scala 68:28]
  wire [6:0] buffer_auto_in_d_bits_source; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_d_bits_sink; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_d_bits_denied; // @[Buffer.scala 68:28]
  wire [63:0] buffer_auto_in_d_bits_data; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_d_bits_corrupt; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_out_a_bits_opcode; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_out_a_bits_param; // @[Buffer.scala 68:28]
  wire [3:0] buffer_auto_out_a_bits_size; // @[Buffer.scala 68:28]
  wire [6:0] buffer_auto_out_a_bits_source; // @[Buffer.scala 68:28]
  wire [31:0] buffer_auto_out_a_bits_address; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_user_amba_prot_bufferable; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_user_amba_prot_modifiable; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_user_amba_prot_readalloc; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_user_amba_prot_writealloc; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_user_amba_prot_privileged; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_user_amba_prot_secure; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_user_amba_prot_fetch; // @[Buffer.scala 68:28]
  wire [7:0] buffer_auto_out_a_bits_mask; // @[Buffer.scala 68:28]
  wire [63:0] buffer_auto_out_a_bits_data; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_corrupt; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_d_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_d_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_out_d_bits_opcode; // @[Buffer.scala 68:28]
  wire [1:0] buffer_auto_out_d_bits_param; // @[Buffer.scala 68:28]
  wire [3:0] buffer_auto_out_d_bits_size; // @[Buffer.scala 68:28]
  wire [6:0] buffer_auto_out_d_bits_source; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_d_bits_sink; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_d_bits_denied; // @[Buffer.scala 68:28]
  wire [63:0] buffer_auto_out_d_bits_data; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_d_bits_corrupt; // @[Buffer.scala 68:28]
  wire  atomics_rf_reset; // @[AtomicAutomata.scala 283:29]
  wire  atomics_clock; // @[AtomicAutomata.scala 283:29]
  wire  atomics_reset; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_in_a_ready; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_in_a_valid; // @[AtomicAutomata.scala 283:29]
  wire [2:0] atomics_auto_in_a_bits_opcode; // @[AtomicAutomata.scala 283:29]
  wire [2:0] atomics_auto_in_a_bits_param; // @[AtomicAutomata.scala 283:29]
  wire [3:0] atomics_auto_in_a_bits_size; // @[AtomicAutomata.scala 283:29]
  wire [6:0] atomics_auto_in_a_bits_source; // @[AtomicAutomata.scala 283:29]
  wire [31:0] atomics_auto_in_a_bits_address; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_in_a_bits_user_amba_prot_bufferable; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_in_a_bits_user_amba_prot_modifiable; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_in_a_bits_user_amba_prot_readalloc; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_in_a_bits_user_amba_prot_writealloc; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_in_a_bits_user_amba_prot_privileged; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_in_a_bits_user_amba_prot_secure; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_in_a_bits_user_amba_prot_fetch; // @[AtomicAutomata.scala 283:29]
  wire [7:0] atomics_auto_in_a_bits_mask; // @[AtomicAutomata.scala 283:29]
  wire [63:0] atomics_auto_in_a_bits_data; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_in_a_bits_corrupt; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_in_d_ready; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_in_d_valid; // @[AtomicAutomata.scala 283:29]
  wire [2:0] atomics_auto_in_d_bits_opcode; // @[AtomicAutomata.scala 283:29]
  wire [1:0] atomics_auto_in_d_bits_param; // @[AtomicAutomata.scala 283:29]
  wire [3:0] atomics_auto_in_d_bits_size; // @[AtomicAutomata.scala 283:29]
  wire [6:0] atomics_auto_in_d_bits_source; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_in_d_bits_sink; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_in_d_bits_denied; // @[AtomicAutomata.scala 283:29]
  wire [63:0] atomics_auto_in_d_bits_data; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_in_d_bits_corrupt; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_out_a_ready; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_out_a_valid; // @[AtomicAutomata.scala 283:29]
  wire [2:0] atomics_auto_out_a_bits_opcode; // @[AtomicAutomata.scala 283:29]
  wire [2:0] atomics_auto_out_a_bits_param; // @[AtomicAutomata.scala 283:29]
  wire [3:0] atomics_auto_out_a_bits_size; // @[AtomicAutomata.scala 283:29]
  wire [6:0] atomics_auto_out_a_bits_source; // @[AtomicAutomata.scala 283:29]
  wire [31:0] atomics_auto_out_a_bits_address; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_out_a_bits_user_amba_prot_bufferable; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_out_a_bits_user_amba_prot_modifiable; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_out_a_bits_user_amba_prot_readalloc; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_out_a_bits_user_amba_prot_writealloc; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_out_a_bits_user_amba_prot_privileged; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_out_a_bits_user_amba_prot_secure; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_out_a_bits_user_amba_prot_fetch; // @[AtomicAutomata.scala 283:29]
  wire [7:0] atomics_auto_out_a_bits_mask; // @[AtomicAutomata.scala 283:29]
  wire [63:0] atomics_auto_out_a_bits_data; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_out_a_bits_corrupt; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_out_d_ready; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_out_d_valid; // @[AtomicAutomata.scala 283:29]
  wire [2:0] atomics_auto_out_d_bits_opcode; // @[AtomicAutomata.scala 283:29]
  wire [1:0] atomics_auto_out_d_bits_param; // @[AtomicAutomata.scala 283:29]
  wire [3:0] atomics_auto_out_d_bits_size; // @[AtomicAutomata.scala 283:29]
  wire [6:0] atomics_auto_out_d_bits_source; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_out_d_bits_sink; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_out_d_bits_denied; // @[AtomicAutomata.scala 283:29]
  wire [63:0] atomics_auto_out_d_bits_data; // @[AtomicAutomata.scala 283:29]
  wire  atomics_auto_out_d_bits_corrupt; // @[AtomicAutomata.scala 283:29]
  wire  wrapped_error_device_rf_reset; // @[LazyModule.scala 524:27]
  wire  wrapped_error_device_clock; // @[LazyModule.scala 524:27]
  wire  wrapped_error_device_reset; // @[LazyModule.scala 524:27]
  wire  wrapped_error_device_auto_buffer_in_a_ready; // @[LazyModule.scala 524:27]
  wire  wrapped_error_device_auto_buffer_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] wrapped_error_device_auto_buffer_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] wrapped_error_device_auto_buffer_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] wrapped_error_device_auto_buffer_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] wrapped_error_device_auto_buffer_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [13:0] wrapped_error_device_auto_buffer_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] wrapped_error_device_auto_buffer_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire  wrapped_error_device_auto_buffer_in_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  wrapped_error_device_auto_buffer_in_d_ready; // @[LazyModule.scala 524:27]
  wire  wrapped_error_device_auto_buffer_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] wrapped_error_device_auto_buffer_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] wrapped_error_device_auto_buffer_in_d_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] wrapped_error_device_auto_buffer_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] wrapped_error_device_auto_buffer_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire  wrapped_error_device_auto_buffer_in_d_bits_sink; // @[LazyModule.scala 524:27]
  wire  wrapped_error_device_auto_buffer_in_d_bits_denied; // @[LazyModule.scala 524:27]
  wire [63:0] wrapped_error_device_auto_buffer_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  wrapped_error_device_auto_buffer_in_d_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_ctrl_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_ctrl_clock; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_ctrl_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_ctrl_auto_buffer_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_ctrl_auto_buffer_out_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_ctrl_auto_buffer_out_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_ctrl_auto_buffer_out_a_bits_param; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_l2_ctrl_auto_buffer_out_a_bits_size; // @[LazyModule.scala 524:27]
  wire [10:0] coupler_to_l2_ctrl_auto_buffer_out_a_bits_source; // @[LazyModule.scala 524:27]
  wire [25:0] coupler_to_l2_ctrl_auto_buffer_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_l2_ctrl_auto_buffer_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_l2_ctrl_auto_buffer_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_ctrl_auto_buffer_out_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_ctrl_auto_buffer_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_ctrl_auto_buffer_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_ctrl_auto_buffer_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_l2_ctrl_auto_buffer_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [10:0] coupler_to_l2_ctrl_auto_buffer_out_d_bits_source; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_l2_ctrl_auto_buffer_out_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_ctrl_auto_tl_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_ctrl_auto_tl_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_ctrl_auto_tl_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_ctrl_auto_tl_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_ctrl_auto_tl_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_l2_ctrl_auto_tl_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [25:0] coupler_to_l2_ctrl_auto_tl_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_l2_ctrl_auto_tl_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_l2_ctrl_auto_tl_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_ctrl_auto_tl_in_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_ctrl_auto_tl_in_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_l2_ctrl_auto_tl_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_ctrl_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_l2_ctrl_auto_tl_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_l2_ctrl_auto_tl_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_l2_ctrl_auto_tl_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  buffer_1_auto_in_a_ready;
  wire  buffer_1_auto_in_a_valid;
  wire [2:0] buffer_1_auto_in_a_bits_opcode;
  wire [2:0] buffer_1_auto_in_a_bits_param;
  wire [3:0] buffer_1_auto_in_a_bits_size;
  wire [6:0] buffer_1_auto_in_a_bits_source;
  wire [31:0] buffer_1_auto_in_a_bits_address;
  wire  buffer_1_auto_in_a_bits_user_amba_prot_bufferable;
  wire  buffer_1_auto_in_a_bits_user_amba_prot_modifiable;
  wire  buffer_1_auto_in_a_bits_user_amba_prot_readalloc;
  wire  buffer_1_auto_in_a_bits_user_amba_prot_writealloc;
  wire  buffer_1_auto_in_a_bits_user_amba_prot_privileged;
  wire  buffer_1_auto_in_a_bits_user_amba_prot_secure;
  wire  buffer_1_auto_in_a_bits_user_amba_prot_fetch;
  wire [7:0] buffer_1_auto_in_a_bits_mask;
  wire [63:0] buffer_1_auto_in_a_bits_data;
  wire  buffer_1_auto_in_a_bits_corrupt;
  wire  buffer_1_auto_in_d_ready;
  wire  buffer_1_auto_in_d_valid;
  wire [2:0] buffer_1_auto_in_d_bits_opcode;
  wire [1:0] buffer_1_auto_in_d_bits_param;
  wire [3:0] buffer_1_auto_in_d_bits_size;
  wire [6:0] buffer_1_auto_in_d_bits_source;
  wire  buffer_1_auto_in_d_bits_sink;
  wire  buffer_1_auto_in_d_bits_denied;
  wire [63:0] buffer_1_auto_in_d_bits_data;
  wire  buffer_1_auto_in_d_bits_corrupt;
  wire  buffer_1_auto_out_a_ready;
  wire  buffer_1_auto_out_a_valid;
  wire [2:0] buffer_1_auto_out_a_bits_opcode;
  wire [2:0] buffer_1_auto_out_a_bits_param;
  wire [3:0] buffer_1_auto_out_a_bits_size;
  wire [6:0] buffer_1_auto_out_a_bits_source;
  wire [31:0] buffer_1_auto_out_a_bits_address;
  wire  buffer_1_auto_out_a_bits_user_amba_prot_bufferable;
  wire  buffer_1_auto_out_a_bits_user_amba_prot_modifiable;
  wire  buffer_1_auto_out_a_bits_user_amba_prot_readalloc;
  wire  buffer_1_auto_out_a_bits_user_amba_prot_writealloc;
  wire  buffer_1_auto_out_a_bits_user_amba_prot_privileged;
  wire  buffer_1_auto_out_a_bits_user_amba_prot_secure;
  wire  buffer_1_auto_out_a_bits_user_amba_prot_fetch;
  wire [7:0] buffer_1_auto_out_a_bits_mask;
  wire [63:0] buffer_1_auto_out_a_bits_data;
  wire  buffer_1_auto_out_a_bits_corrupt;
  wire  buffer_1_auto_out_d_ready;
  wire  buffer_1_auto_out_d_valid;
  wire [2:0] buffer_1_auto_out_d_bits_opcode;
  wire [1:0] buffer_1_auto_out_d_bits_param;
  wire [3:0] buffer_1_auto_out_d_bits_size;
  wire [6:0] buffer_1_auto_out_d_bits_source;
  wire  buffer_1_auto_out_d_bits_sink;
  wire  buffer_1_auto_out_d_bits_denied;
  wire [63:0] buffer_1_auto_out_d_bits_data;
  wire  buffer_1_auto_out_d_bits_corrupt;
  wire  coupler_to_plic_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_plic_clock; // @[LazyModule.scala 524:27]
  wire  coupler_to_plic_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_plic_auto_fragmenter_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_plic_auto_fragmenter_out_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_plic_auto_fragmenter_out_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_plic_auto_fragmenter_out_a_bits_param; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_plic_auto_fragmenter_out_a_bits_size; // @[LazyModule.scala 524:27]
  wire [10:0] coupler_to_plic_auto_fragmenter_out_a_bits_source; // @[LazyModule.scala 524:27]
  wire [27:0] coupler_to_plic_auto_fragmenter_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_plic_auto_fragmenter_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_plic_auto_fragmenter_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_plic_auto_fragmenter_out_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_plic_auto_fragmenter_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_plic_auto_fragmenter_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_plic_auto_fragmenter_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_plic_auto_fragmenter_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [10:0] coupler_to_plic_auto_fragmenter_out_d_bits_source; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_plic_auto_fragmenter_out_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_plic_auto_tl_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_plic_auto_tl_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_plic_auto_tl_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_plic_auto_tl_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_plic_auto_tl_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_plic_auto_tl_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [27:0] coupler_to_plic_auto_tl_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_plic_auto_tl_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_plic_auto_tl_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_plic_auto_tl_in_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_plic_auto_tl_in_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_plic_auto_tl_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_plic_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_plic_auto_tl_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_plic_auto_tl_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_plic_auto_tl_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_clint_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_clint_clock; // @[LazyModule.scala 524:27]
  wire  coupler_to_clint_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_clint_auto_fragmenter_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_clint_auto_fragmenter_out_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_clint_auto_fragmenter_out_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_clint_auto_fragmenter_out_a_bits_param; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_clint_auto_fragmenter_out_a_bits_size; // @[LazyModule.scala 524:27]
  wire [10:0] coupler_to_clint_auto_fragmenter_out_a_bits_source; // @[LazyModule.scala 524:27]
  wire [25:0] coupler_to_clint_auto_fragmenter_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_clint_auto_fragmenter_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_clint_auto_fragmenter_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_clint_auto_fragmenter_out_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_clint_auto_fragmenter_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_clint_auto_fragmenter_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_clint_auto_fragmenter_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_clint_auto_fragmenter_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [10:0] coupler_to_clint_auto_fragmenter_out_d_bits_source; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_clint_auto_fragmenter_out_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_clint_auto_tl_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_clint_auto_tl_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_clint_auto_tl_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_clint_auto_tl_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_clint_auto_tl_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_clint_auto_tl_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [25:0] coupler_to_clint_auto_tl_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_clint_auto_tl_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_clint_auto_tl_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_clint_auto_tl_in_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_clint_auto_tl_in_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_clint_auto_tl_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_clint_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_clint_auto_tl_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_clint_auto_tl_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_clint_auto_tl_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_debug_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_debug_clock; // @[LazyModule.scala 524:27]
  wire  coupler_to_debug_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_debug_auto_fragmenter_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_debug_auto_fragmenter_out_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_debug_auto_fragmenter_out_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_debug_auto_fragmenter_out_a_bits_param; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_debug_auto_fragmenter_out_a_bits_size; // @[LazyModule.scala 524:27]
  wire [10:0] coupler_to_debug_auto_fragmenter_out_a_bits_source; // @[LazyModule.scala 524:27]
  wire [11:0] coupler_to_debug_auto_fragmenter_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_debug_auto_fragmenter_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_debug_auto_fragmenter_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_debug_auto_fragmenter_out_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_debug_auto_fragmenter_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_debug_auto_fragmenter_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_debug_auto_fragmenter_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_debug_auto_fragmenter_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [10:0] coupler_to_debug_auto_fragmenter_out_d_bits_source; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_debug_auto_fragmenter_out_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_debug_auto_tl_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_debug_auto_tl_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_debug_auto_tl_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_debug_auto_tl_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_debug_auto_tl_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_debug_auto_tl_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [11:0] coupler_to_debug_auto_tl_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_debug_auto_tl_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_debug_auto_tl_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_debug_auto_tl_in_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_debug_auto_tl_in_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_debug_auto_tl_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_debug_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_debug_auto_tl_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_debug_auto_tl_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_debug_auto_tl_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_clock; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_valid; // @[LazyModule.scala 524:27]
  wire [31:0] coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_addr; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_len; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_size; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_cache; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_prot; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_w_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_w_valid; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_w_bits_data; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_w_bits_strb; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_w_bits_last; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_b_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_b_valid; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_b_bits_resp; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_valid; // @[LazyModule.scala 524:27]
  wire [31:0] coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_addr; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_len; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_size; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_cache; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_prot; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_r_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_r_valid; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_r_bits_data; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_r_bits_resp; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_r_bits_last; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_tl_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_tl_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [31:0] coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_secure; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_tl_in_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_tl_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_denied; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_clock; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_auto_fragmenter_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_auto_fragmenter_out_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_auto_fragmenter_out_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_auto_fragmenter_out_a_bits_param; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_traceencoder_auto_fragmenter_out_a_bits_size; // @[LazyModule.scala 524:27]
  wire [10:0] coupler_to_traceencoder_auto_fragmenter_out_a_bits_source; // @[LazyModule.scala 524:27]
  wire [28:0] coupler_to_traceencoder_auto_fragmenter_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_traceencoder_auto_fragmenter_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_traceencoder_auto_fragmenter_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_auto_fragmenter_out_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_auto_fragmenter_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_auto_fragmenter_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_auto_fragmenter_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_traceencoder_auto_fragmenter_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [10:0] coupler_to_traceencoder_auto_fragmenter_out_d_bits_source; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_traceencoder_auto_fragmenter_out_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_auto_tl_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_auto_tl_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_auto_tl_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_auto_tl_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_auto_tl_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_traceencoder_auto_tl_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [28:0] coupler_to_traceencoder_auto_tl_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_traceencoder_auto_tl_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_traceencoder_auto_tl_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_auto_tl_in_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_auto_tl_in_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_auto_tl_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_auto_tl_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_traceencoder_auto_tl_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_traceencoder_auto_tl_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_1_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_1_clock; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_1_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_1_auto_fragmenter_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_1_auto_fragmenter_out_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_param; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_size; // @[LazyModule.scala 524:27]
  wire [10:0] coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_source; // @[LazyModule.scala 524:27]
  wire [28:0] coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_1_auto_fragmenter_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_1_auto_fragmenter_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_1_auto_fragmenter_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_traceencoder_1_auto_fragmenter_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [10:0] coupler_to_traceencoder_1_auto_fragmenter_out_d_bits_source; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_traceencoder_1_auto_fragmenter_out_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_1_auto_tl_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_1_auto_tl_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_1_auto_tl_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_1_auto_tl_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_1_auto_tl_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_traceencoder_1_auto_tl_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [28:0] coupler_to_traceencoder_1_auto_tl_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_traceencoder_1_auto_tl_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_traceencoder_1_auto_tl_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_1_auto_tl_in_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_1_auto_tl_in_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_traceencoder_1_auto_tl_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_1_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_traceencoder_1_auto_tl_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_traceencoder_1_auto_tl_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_traceencoder_1_auto_tl_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_tracefunnel_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_tracefunnel_clock; // @[LazyModule.scala 524:27]
  wire  coupler_to_tracefunnel_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_tracefunnel_auto_fragmenter_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_tracefunnel_auto_fragmenter_out_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_tracefunnel_auto_fragmenter_out_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_tracefunnel_auto_fragmenter_out_a_bits_param; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_tracefunnel_auto_fragmenter_out_a_bits_size; // @[LazyModule.scala 524:27]
  wire [10:0] coupler_to_tracefunnel_auto_fragmenter_out_a_bits_source; // @[LazyModule.scala 524:27]
  wire [28:0] coupler_to_tracefunnel_auto_fragmenter_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_tracefunnel_auto_fragmenter_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_tracefunnel_auto_fragmenter_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_tracefunnel_auto_fragmenter_out_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_tracefunnel_auto_fragmenter_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_tracefunnel_auto_fragmenter_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_tracefunnel_auto_fragmenter_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_tracefunnel_auto_fragmenter_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [10:0] coupler_to_tracefunnel_auto_fragmenter_out_d_bits_source; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_tracefunnel_auto_fragmenter_out_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_tracefunnel_auto_tl_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_tracefunnel_auto_tl_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_tracefunnel_auto_tl_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_tracefunnel_auto_tl_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_tracefunnel_auto_tl_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_tracefunnel_auto_tl_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [28:0] coupler_to_tracefunnel_auto_tl_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_tracefunnel_auto_tl_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_tracefunnel_auto_tl_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_tracefunnel_auto_tl_in_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_tracefunnel_auto_tl_in_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_tracefunnel_auto_tl_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_tracefunnel_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_tracefunnel_auto_tl_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_tracefunnel_auto_tl_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_tracefunnel_auto_tl_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_testIndicator_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_testIndicator_clock; // @[LazyModule.scala 524:27]
  wire  coupler_to_testIndicator_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_testIndicator_auto_control_xing_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_testIndicator_auto_control_xing_out_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_testIndicator_auto_control_xing_out_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_testIndicator_auto_control_xing_out_a_bits_param; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_testIndicator_auto_control_xing_out_a_bits_size; // @[LazyModule.scala 524:27]
  wire [11:0] coupler_to_testIndicator_auto_control_xing_out_a_bits_source; // @[LazyModule.scala 524:27]
  wire [14:0] coupler_to_testIndicator_auto_control_xing_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_to_testIndicator_auto_control_xing_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [31:0] coupler_to_testIndicator_auto_control_xing_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_testIndicator_auto_control_xing_out_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_testIndicator_auto_control_xing_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_testIndicator_auto_control_xing_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_testIndicator_auto_control_xing_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_testIndicator_auto_control_xing_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [11:0] coupler_to_testIndicator_auto_control_xing_out_d_bits_source; // @[LazyModule.scala 524:27]
  wire [31:0] coupler_to_testIndicator_auto_control_xing_out_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_testIndicator_auto_tl_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_testIndicator_auto_tl_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_testIndicator_auto_tl_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_testIndicator_auto_tl_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_testIndicator_auto_tl_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_testIndicator_auto_tl_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [14:0] coupler_to_testIndicator_auto_tl_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_testIndicator_auto_tl_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_testIndicator_auto_tl_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_testIndicator_auto_tl_in_a_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_testIndicator_auto_tl_in_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_testIndicator_auto_tl_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_testIndicator_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_testIndicator_auto_tl_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [6:0] coupler_to_testIndicator_auto_tl_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_to_testIndicator_auto_tl_in_d_bits_data; // @[LazyModule.scala 524:27]
  RHEA__FixedClockBroadcast_3 fixedClockNode ( // @[ClockGroup.scala 106:107]
    .auto_in_clock(fixedClockNode_auto_in_clock),
    .auto_in_reset(fixedClockNode_auto_in_reset),
    .auto_out_2_clock(fixedClockNode_auto_out_2_clock),
    .auto_out_2_reset(fixedClockNode_auto_out_2_reset),
    .auto_out_1_clock(fixedClockNode_auto_out_1_clock),
    .auto_out_1_reset(fixedClockNode_auto_out_1_reset),
    .auto_out_0_clock(fixedClockNode_auto_out_0_clock),
    .auto_out_0_reset(fixedClockNode_auto_out_0_reset)
  );
  RHEA__TLFIFOFixer_4 fixer ( // @[PeripheryBus.scala 47:33]
    .rf_reset(fixer_rf_reset),
    .clock(fixer_clock),
    .reset(fixer_reset),
    .auto_in_a_ready(fixer_auto_in_a_ready),
    .auto_in_a_valid(fixer_auto_in_a_valid),
    .auto_in_a_bits_opcode(fixer_auto_in_a_bits_opcode),
    .auto_in_a_bits_param(fixer_auto_in_a_bits_param),
    .auto_in_a_bits_size(fixer_auto_in_a_bits_size),
    .auto_in_a_bits_source(fixer_auto_in_a_bits_source),
    .auto_in_a_bits_address(fixer_auto_in_a_bits_address),
    .auto_in_a_bits_user_amba_prot_bufferable(fixer_auto_in_a_bits_user_amba_prot_bufferable),
    .auto_in_a_bits_user_amba_prot_modifiable(fixer_auto_in_a_bits_user_amba_prot_modifiable),
    .auto_in_a_bits_user_amba_prot_readalloc(fixer_auto_in_a_bits_user_amba_prot_readalloc),
    .auto_in_a_bits_user_amba_prot_writealloc(fixer_auto_in_a_bits_user_amba_prot_writealloc),
    .auto_in_a_bits_user_amba_prot_privileged(fixer_auto_in_a_bits_user_amba_prot_privileged),
    .auto_in_a_bits_user_amba_prot_secure(fixer_auto_in_a_bits_user_amba_prot_secure),
    .auto_in_a_bits_user_amba_prot_fetch(fixer_auto_in_a_bits_user_amba_prot_fetch),
    .auto_in_a_bits_mask(fixer_auto_in_a_bits_mask),
    .auto_in_a_bits_data(fixer_auto_in_a_bits_data),
    .auto_in_a_bits_corrupt(fixer_auto_in_a_bits_corrupt),
    .auto_in_d_ready(fixer_auto_in_d_ready),
    .auto_in_d_valid(fixer_auto_in_d_valid),
    .auto_in_d_bits_opcode(fixer_auto_in_d_bits_opcode),
    .auto_in_d_bits_param(fixer_auto_in_d_bits_param),
    .auto_in_d_bits_size(fixer_auto_in_d_bits_size),
    .auto_in_d_bits_source(fixer_auto_in_d_bits_source),
    .auto_in_d_bits_sink(fixer_auto_in_d_bits_sink),
    .auto_in_d_bits_denied(fixer_auto_in_d_bits_denied),
    .auto_in_d_bits_data(fixer_auto_in_d_bits_data),
    .auto_in_d_bits_corrupt(fixer_auto_in_d_bits_corrupt),
    .auto_out_a_ready(fixer_auto_out_a_ready),
    .auto_out_a_valid(fixer_auto_out_a_valid),
    .auto_out_a_bits_opcode(fixer_auto_out_a_bits_opcode),
    .auto_out_a_bits_param(fixer_auto_out_a_bits_param),
    .auto_out_a_bits_size(fixer_auto_out_a_bits_size),
    .auto_out_a_bits_source(fixer_auto_out_a_bits_source),
    .auto_out_a_bits_address(fixer_auto_out_a_bits_address),
    .auto_out_a_bits_user_amba_prot_bufferable(fixer_auto_out_a_bits_user_amba_prot_bufferable),
    .auto_out_a_bits_user_amba_prot_modifiable(fixer_auto_out_a_bits_user_amba_prot_modifiable),
    .auto_out_a_bits_user_amba_prot_readalloc(fixer_auto_out_a_bits_user_amba_prot_readalloc),
    .auto_out_a_bits_user_amba_prot_writealloc(fixer_auto_out_a_bits_user_amba_prot_writealloc),
    .auto_out_a_bits_user_amba_prot_privileged(fixer_auto_out_a_bits_user_amba_prot_privileged),
    .auto_out_a_bits_user_amba_prot_secure(fixer_auto_out_a_bits_user_amba_prot_secure),
    .auto_out_a_bits_user_amba_prot_fetch(fixer_auto_out_a_bits_user_amba_prot_fetch),
    .auto_out_a_bits_mask(fixer_auto_out_a_bits_mask),
    .auto_out_a_bits_data(fixer_auto_out_a_bits_data),
    .auto_out_a_bits_corrupt(fixer_auto_out_a_bits_corrupt),
    .auto_out_d_ready(fixer_auto_out_d_ready),
    .auto_out_d_valid(fixer_auto_out_d_valid),
    .auto_out_d_bits_opcode(fixer_auto_out_d_bits_opcode),
    .auto_out_d_bits_param(fixer_auto_out_d_bits_param),
    .auto_out_d_bits_size(fixer_auto_out_d_bits_size),
    .auto_out_d_bits_source(fixer_auto_out_d_bits_source),
    .auto_out_d_bits_sink(fixer_auto_out_d_bits_sink),
    .auto_out_d_bits_denied(fixer_auto_out_d_bits_denied),
    .auto_out_d_bits_data(fixer_auto_out_d_bits_data),
    .auto_out_d_bits_corrupt(fixer_auto_out_d_bits_corrupt)
  );
  RHEA__TLXbar_5 out_xbar ( // @[PeripheryBus.scala 50:30]
    .rf_reset(out_xbar_rf_reset),
    .clock(out_xbar_clock),
    .reset(out_xbar_reset),
    .auto_in_a_ready(out_xbar_auto_in_a_ready),
    .auto_in_a_valid(out_xbar_auto_in_a_valid),
    .auto_in_a_bits_opcode(out_xbar_auto_in_a_bits_opcode),
    .auto_in_a_bits_param(out_xbar_auto_in_a_bits_param),
    .auto_in_a_bits_size(out_xbar_auto_in_a_bits_size),
    .auto_in_a_bits_source(out_xbar_auto_in_a_bits_source),
    .auto_in_a_bits_address(out_xbar_auto_in_a_bits_address),
    .auto_in_a_bits_user_amba_prot_bufferable(out_xbar_auto_in_a_bits_user_amba_prot_bufferable),
    .auto_in_a_bits_user_amba_prot_modifiable(out_xbar_auto_in_a_bits_user_amba_prot_modifiable),
    .auto_in_a_bits_user_amba_prot_readalloc(out_xbar_auto_in_a_bits_user_amba_prot_readalloc),
    .auto_in_a_bits_user_amba_prot_writealloc(out_xbar_auto_in_a_bits_user_amba_prot_writealloc),
    .auto_in_a_bits_user_amba_prot_privileged(out_xbar_auto_in_a_bits_user_amba_prot_privileged),
    .auto_in_a_bits_user_amba_prot_secure(out_xbar_auto_in_a_bits_user_amba_prot_secure),
    .auto_in_a_bits_user_amba_prot_fetch(out_xbar_auto_in_a_bits_user_amba_prot_fetch),
    .auto_in_a_bits_mask(out_xbar_auto_in_a_bits_mask),
    .auto_in_a_bits_data(out_xbar_auto_in_a_bits_data),
    .auto_in_a_bits_corrupt(out_xbar_auto_in_a_bits_corrupt),
    .auto_in_d_ready(out_xbar_auto_in_d_ready),
    .auto_in_d_valid(out_xbar_auto_in_d_valid),
    .auto_in_d_bits_opcode(out_xbar_auto_in_d_bits_opcode),
    .auto_in_d_bits_param(out_xbar_auto_in_d_bits_param),
    .auto_in_d_bits_size(out_xbar_auto_in_d_bits_size),
    .auto_in_d_bits_source(out_xbar_auto_in_d_bits_source),
    .auto_in_d_bits_sink(out_xbar_auto_in_d_bits_sink),
    .auto_in_d_bits_denied(out_xbar_auto_in_d_bits_denied),
    .auto_in_d_bits_data(out_xbar_auto_in_d_bits_data),
    .auto_in_d_bits_corrupt(out_xbar_auto_in_d_bits_corrupt),
    .auto_out_9_a_ready(out_xbar_auto_out_9_a_ready),
    .auto_out_9_a_valid(out_xbar_auto_out_9_a_valid),
    .auto_out_9_a_bits_opcode(out_xbar_auto_out_9_a_bits_opcode),
    .auto_out_9_a_bits_param(out_xbar_auto_out_9_a_bits_param),
    .auto_out_9_a_bits_size(out_xbar_auto_out_9_a_bits_size),
    .auto_out_9_a_bits_source(out_xbar_auto_out_9_a_bits_source),
    .auto_out_9_a_bits_address(out_xbar_auto_out_9_a_bits_address),
    .auto_out_9_a_bits_mask(out_xbar_auto_out_9_a_bits_mask),
    .auto_out_9_a_bits_data(out_xbar_auto_out_9_a_bits_data),
    .auto_out_9_a_bits_corrupt(out_xbar_auto_out_9_a_bits_corrupt),
    .auto_out_9_d_ready(out_xbar_auto_out_9_d_ready),
    .auto_out_9_d_valid(out_xbar_auto_out_9_d_valid),
    .auto_out_9_d_bits_opcode(out_xbar_auto_out_9_d_bits_opcode),
    .auto_out_9_d_bits_size(out_xbar_auto_out_9_d_bits_size),
    .auto_out_9_d_bits_source(out_xbar_auto_out_9_d_bits_source),
    .auto_out_9_d_bits_data(out_xbar_auto_out_9_d_bits_data),
    .auto_out_8_a_ready(out_xbar_auto_out_8_a_ready),
    .auto_out_8_a_valid(out_xbar_auto_out_8_a_valid),
    .auto_out_8_a_bits_opcode(out_xbar_auto_out_8_a_bits_opcode),
    .auto_out_8_a_bits_param(out_xbar_auto_out_8_a_bits_param),
    .auto_out_8_a_bits_size(out_xbar_auto_out_8_a_bits_size),
    .auto_out_8_a_bits_source(out_xbar_auto_out_8_a_bits_source),
    .auto_out_8_a_bits_address(out_xbar_auto_out_8_a_bits_address),
    .auto_out_8_a_bits_mask(out_xbar_auto_out_8_a_bits_mask),
    .auto_out_8_a_bits_data(out_xbar_auto_out_8_a_bits_data),
    .auto_out_8_a_bits_corrupt(out_xbar_auto_out_8_a_bits_corrupt),
    .auto_out_8_d_ready(out_xbar_auto_out_8_d_ready),
    .auto_out_8_d_valid(out_xbar_auto_out_8_d_valid),
    .auto_out_8_d_bits_opcode(out_xbar_auto_out_8_d_bits_opcode),
    .auto_out_8_d_bits_size(out_xbar_auto_out_8_d_bits_size),
    .auto_out_8_d_bits_source(out_xbar_auto_out_8_d_bits_source),
    .auto_out_8_d_bits_data(out_xbar_auto_out_8_d_bits_data),
    .auto_out_7_a_ready(out_xbar_auto_out_7_a_ready),
    .auto_out_7_a_valid(out_xbar_auto_out_7_a_valid),
    .auto_out_7_a_bits_opcode(out_xbar_auto_out_7_a_bits_opcode),
    .auto_out_7_a_bits_param(out_xbar_auto_out_7_a_bits_param),
    .auto_out_7_a_bits_size(out_xbar_auto_out_7_a_bits_size),
    .auto_out_7_a_bits_source(out_xbar_auto_out_7_a_bits_source),
    .auto_out_7_a_bits_address(out_xbar_auto_out_7_a_bits_address),
    .auto_out_7_a_bits_mask(out_xbar_auto_out_7_a_bits_mask),
    .auto_out_7_a_bits_data(out_xbar_auto_out_7_a_bits_data),
    .auto_out_7_a_bits_corrupt(out_xbar_auto_out_7_a_bits_corrupt),
    .auto_out_7_d_ready(out_xbar_auto_out_7_d_ready),
    .auto_out_7_d_valid(out_xbar_auto_out_7_d_valid),
    .auto_out_7_d_bits_opcode(out_xbar_auto_out_7_d_bits_opcode),
    .auto_out_7_d_bits_size(out_xbar_auto_out_7_d_bits_size),
    .auto_out_7_d_bits_source(out_xbar_auto_out_7_d_bits_source),
    .auto_out_7_d_bits_data(out_xbar_auto_out_7_d_bits_data),
    .auto_out_6_a_ready(out_xbar_auto_out_6_a_ready),
    .auto_out_6_a_valid(out_xbar_auto_out_6_a_valid),
    .auto_out_6_a_bits_opcode(out_xbar_auto_out_6_a_bits_opcode),
    .auto_out_6_a_bits_param(out_xbar_auto_out_6_a_bits_param),
    .auto_out_6_a_bits_size(out_xbar_auto_out_6_a_bits_size),
    .auto_out_6_a_bits_source(out_xbar_auto_out_6_a_bits_source),
    .auto_out_6_a_bits_address(out_xbar_auto_out_6_a_bits_address),
    .auto_out_6_a_bits_mask(out_xbar_auto_out_6_a_bits_mask),
    .auto_out_6_a_bits_data(out_xbar_auto_out_6_a_bits_data),
    .auto_out_6_a_bits_corrupt(out_xbar_auto_out_6_a_bits_corrupt),
    .auto_out_6_d_ready(out_xbar_auto_out_6_d_ready),
    .auto_out_6_d_valid(out_xbar_auto_out_6_d_valid),
    .auto_out_6_d_bits_opcode(out_xbar_auto_out_6_d_bits_opcode),
    .auto_out_6_d_bits_size(out_xbar_auto_out_6_d_bits_size),
    .auto_out_6_d_bits_source(out_xbar_auto_out_6_d_bits_source),
    .auto_out_6_d_bits_data(out_xbar_auto_out_6_d_bits_data),
    .auto_out_5_a_ready(out_xbar_auto_out_5_a_ready),
    .auto_out_5_a_valid(out_xbar_auto_out_5_a_valid),
    .auto_out_5_a_bits_opcode(out_xbar_auto_out_5_a_bits_opcode),
    .auto_out_5_a_bits_param(out_xbar_auto_out_5_a_bits_param),
    .auto_out_5_a_bits_size(out_xbar_auto_out_5_a_bits_size),
    .auto_out_5_a_bits_source(out_xbar_auto_out_5_a_bits_source),
    .auto_out_5_a_bits_address(out_xbar_auto_out_5_a_bits_address),
    .auto_out_5_a_bits_user_amba_prot_bufferable(out_xbar_auto_out_5_a_bits_user_amba_prot_bufferable),
    .auto_out_5_a_bits_user_amba_prot_modifiable(out_xbar_auto_out_5_a_bits_user_amba_prot_modifiable),
    .auto_out_5_a_bits_user_amba_prot_readalloc(out_xbar_auto_out_5_a_bits_user_amba_prot_readalloc),
    .auto_out_5_a_bits_user_amba_prot_writealloc(out_xbar_auto_out_5_a_bits_user_amba_prot_writealloc),
    .auto_out_5_a_bits_user_amba_prot_privileged(out_xbar_auto_out_5_a_bits_user_amba_prot_privileged),
    .auto_out_5_a_bits_user_amba_prot_secure(out_xbar_auto_out_5_a_bits_user_amba_prot_secure),
    .auto_out_5_a_bits_user_amba_prot_fetch(out_xbar_auto_out_5_a_bits_user_amba_prot_fetch),
    .auto_out_5_a_bits_mask(out_xbar_auto_out_5_a_bits_mask),
    .auto_out_5_a_bits_data(out_xbar_auto_out_5_a_bits_data),
    .auto_out_5_a_bits_corrupt(out_xbar_auto_out_5_a_bits_corrupt),
    .auto_out_5_d_ready(out_xbar_auto_out_5_d_ready),
    .auto_out_5_d_valid(out_xbar_auto_out_5_d_valid),
    .auto_out_5_d_bits_opcode(out_xbar_auto_out_5_d_bits_opcode),
    .auto_out_5_d_bits_size(out_xbar_auto_out_5_d_bits_size),
    .auto_out_5_d_bits_source(out_xbar_auto_out_5_d_bits_source),
    .auto_out_5_d_bits_denied(out_xbar_auto_out_5_d_bits_denied),
    .auto_out_5_d_bits_data(out_xbar_auto_out_5_d_bits_data),
    .auto_out_5_d_bits_corrupt(out_xbar_auto_out_5_d_bits_corrupt),
    .auto_out_4_a_ready(out_xbar_auto_out_4_a_ready),
    .auto_out_4_a_valid(out_xbar_auto_out_4_a_valid),
    .auto_out_4_a_bits_opcode(out_xbar_auto_out_4_a_bits_opcode),
    .auto_out_4_a_bits_param(out_xbar_auto_out_4_a_bits_param),
    .auto_out_4_a_bits_size(out_xbar_auto_out_4_a_bits_size),
    .auto_out_4_a_bits_source(out_xbar_auto_out_4_a_bits_source),
    .auto_out_4_a_bits_address(out_xbar_auto_out_4_a_bits_address),
    .auto_out_4_a_bits_mask(out_xbar_auto_out_4_a_bits_mask),
    .auto_out_4_a_bits_data(out_xbar_auto_out_4_a_bits_data),
    .auto_out_4_a_bits_corrupt(out_xbar_auto_out_4_a_bits_corrupt),
    .auto_out_4_d_ready(out_xbar_auto_out_4_d_ready),
    .auto_out_4_d_valid(out_xbar_auto_out_4_d_valid),
    .auto_out_4_d_bits_opcode(out_xbar_auto_out_4_d_bits_opcode),
    .auto_out_4_d_bits_size(out_xbar_auto_out_4_d_bits_size),
    .auto_out_4_d_bits_source(out_xbar_auto_out_4_d_bits_source),
    .auto_out_4_d_bits_data(out_xbar_auto_out_4_d_bits_data),
    .auto_out_3_a_ready(out_xbar_auto_out_3_a_ready),
    .auto_out_3_a_valid(out_xbar_auto_out_3_a_valid),
    .auto_out_3_a_bits_opcode(out_xbar_auto_out_3_a_bits_opcode),
    .auto_out_3_a_bits_param(out_xbar_auto_out_3_a_bits_param),
    .auto_out_3_a_bits_size(out_xbar_auto_out_3_a_bits_size),
    .auto_out_3_a_bits_source(out_xbar_auto_out_3_a_bits_source),
    .auto_out_3_a_bits_address(out_xbar_auto_out_3_a_bits_address),
    .auto_out_3_a_bits_mask(out_xbar_auto_out_3_a_bits_mask),
    .auto_out_3_a_bits_data(out_xbar_auto_out_3_a_bits_data),
    .auto_out_3_a_bits_corrupt(out_xbar_auto_out_3_a_bits_corrupt),
    .auto_out_3_d_ready(out_xbar_auto_out_3_d_ready),
    .auto_out_3_d_valid(out_xbar_auto_out_3_d_valid),
    .auto_out_3_d_bits_opcode(out_xbar_auto_out_3_d_bits_opcode),
    .auto_out_3_d_bits_size(out_xbar_auto_out_3_d_bits_size),
    .auto_out_3_d_bits_source(out_xbar_auto_out_3_d_bits_source),
    .auto_out_3_d_bits_data(out_xbar_auto_out_3_d_bits_data),
    .auto_out_2_a_ready(out_xbar_auto_out_2_a_ready),
    .auto_out_2_a_valid(out_xbar_auto_out_2_a_valid),
    .auto_out_2_a_bits_opcode(out_xbar_auto_out_2_a_bits_opcode),
    .auto_out_2_a_bits_param(out_xbar_auto_out_2_a_bits_param),
    .auto_out_2_a_bits_size(out_xbar_auto_out_2_a_bits_size),
    .auto_out_2_a_bits_source(out_xbar_auto_out_2_a_bits_source),
    .auto_out_2_a_bits_address(out_xbar_auto_out_2_a_bits_address),
    .auto_out_2_a_bits_mask(out_xbar_auto_out_2_a_bits_mask),
    .auto_out_2_a_bits_data(out_xbar_auto_out_2_a_bits_data),
    .auto_out_2_a_bits_corrupt(out_xbar_auto_out_2_a_bits_corrupt),
    .auto_out_2_d_ready(out_xbar_auto_out_2_d_ready),
    .auto_out_2_d_valid(out_xbar_auto_out_2_d_valid),
    .auto_out_2_d_bits_opcode(out_xbar_auto_out_2_d_bits_opcode),
    .auto_out_2_d_bits_size(out_xbar_auto_out_2_d_bits_size),
    .auto_out_2_d_bits_source(out_xbar_auto_out_2_d_bits_source),
    .auto_out_2_d_bits_data(out_xbar_auto_out_2_d_bits_data),
    .auto_out_1_a_ready(out_xbar_auto_out_1_a_ready),
    .auto_out_1_a_valid(out_xbar_auto_out_1_a_valid),
    .auto_out_1_a_bits_opcode(out_xbar_auto_out_1_a_bits_opcode),
    .auto_out_1_a_bits_param(out_xbar_auto_out_1_a_bits_param),
    .auto_out_1_a_bits_size(out_xbar_auto_out_1_a_bits_size),
    .auto_out_1_a_bits_source(out_xbar_auto_out_1_a_bits_source),
    .auto_out_1_a_bits_address(out_xbar_auto_out_1_a_bits_address),
    .auto_out_1_a_bits_mask(out_xbar_auto_out_1_a_bits_mask),
    .auto_out_1_a_bits_data(out_xbar_auto_out_1_a_bits_data),
    .auto_out_1_a_bits_corrupt(out_xbar_auto_out_1_a_bits_corrupt),
    .auto_out_1_d_ready(out_xbar_auto_out_1_d_ready),
    .auto_out_1_d_valid(out_xbar_auto_out_1_d_valid),
    .auto_out_1_d_bits_opcode(out_xbar_auto_out_1_d_bits_opcode),
    .auto_out_1_d_bits_size(out_xbar_auto_out_1_d_bits_size),
    .auto_out_1_d_bits_source(out_xbar_auto_out_1_d_bits_source),
    .auto_out_1_d_bits_data(out_xbar_auto_out_1_d_bits_data),
    .auto_out_0_a_ready(out_xbar_auto_out_0_a_ready),
    .auto_out_0_a_valid(out_xbar_auto_out_0_a_valid),
    .auto_out_0_a_bits_opcode(out_xbar_auto_out_0_a_bits_opcode),
    .auto_out_0_a_bits_param(out_xbar_auto_out_0_a_bits_param),
    .auto_out_0_a_bits_size(out_xbar_auto_out_0_a_bits_size),
    .auto_out_0_a_bits_source(out_xbar_auto_out_0_a_bits_source),
    .auto_out_0_a_bits_address(out_xbar_auto_out_0_a_bits_address),
    .auto_out_0_a_bits_mask(out_xbar_auto_out_0_a_bits_mask),
    .auto_out_0_a_bits_corrupt(out_xbar_auto_out_0_a_bits_corrupt),
    .auto_out_0_d_ready(out_xbar_auto_out_0_d_ready),
    .auto_out_0_d_valid(out_xbar_auto_out_0_d_valid),
    .auto_out_0_d_bits_opcode(out_xbar_auto_out_0_d_bits_opcode),
    .auto_out_0_d_bits_param(out_xbar_auto_out_0_d_bits_param),
    .auto_out_0_d_bits_size(out_xbar_auto_out_0_d_bits_size),
    .auto_out_0_d_bits_source(out_xbar_auto_out_0_d_bits_source),
    .auto_out_0_d_bits_sink(out_xbar_auto_out_0_d_bits_sink),
    .auto_out_0_d_bits_denied(out_xbar_auto_out_0_d_bits_denied),
    .auto_out_0_d_bits_data(out_xbar_auto_out_0_d_bits_data),
    .auto_out_0_d_bits_corrupt(out_xbar_auto_out_0_d_bits_corrupt)
  );
  RHEA__TLBuffer_6 buffer ( // @[Buffer.scala 68:28]
    .rf_reset(buffer_rf_reset),
    .clock(buffer_clock),
    .reset(buffer_reset),
    .auto_in_a_ready(buffer_auto_in_a_ready),
    .auto_in_a_valid(buffer_auto_in_a_valid),
    .auto_in_a_bits_opcode(buffer_auto_in_a_bits_opcode),
    .auto_in_a_bits_param(buffer_auto_in_a_bits_param),
    .auto_in_a_bits_size(buffer_auto_in_a_bits_size),
    .auto_in_a_bits_source(buffer_auto_in_a_bits_source),
    .auto_in_a_bits_address(buffer_auto_in_a_bits_address),
    .auto_in_a_bits_user_amba_prot_bufferable(buffer_auto_in_a_bits_user_amba_prot_bufferable),
    .auto_in_a_bits_user_amba_prot_modifiable(buffer_auto_in_a_bits_user_amba_prot_modifiable),
    .auto_in_a_bits_user_amba_prot_readalloc(buffer_auto_in_a_bits_user_amba_prot_readalloc),
    .auto_in_a_bits_user_amba_prot_writealloc(buffer_auto_in_a_bits_user_amba_prot_writealloc),
    .auto_in_a_bits_user_amba_prot_privileged(buffer_auto_in_a_bits_user_amba_prot_privileged),
    .auto_in_a_bits_user_amba_prot_secure(buffer_auto_in_a_bits_user_amba_prot_secure),
    .auto_in_a_bits_user_amba_prot_fetch(buffer_auto_in_a_bits_user_amba_prot_fetch),
    .auto_in_a_bits_mask(buffer_auto_in_a_bits_mask),
    .auto_in_a_bits_data(buffer_auto_in_a_bits_data),
    .auto_in_a_bits_corrupt(buffer_auto_in_a_bits_corrupt),
    .auto_in_d_ready(buffer_auto_in_d_ready),
    .auto_in_d_valid(buffer_auto_in_d_valid),
    .auto_in_d_bits_opcode(buffer_auto_in_d_bits_opcode),
    .auto_in_d_bits_param(buffer_auto_in_d_bits_param),
    .auto_in_d_bits_size(buffer_auto_in_d_bits_size),
    .auto_in_d_bits_source(buffer_auto_in_d_bits_source),
    .auto_in_d_bits_sink(buffer_auto_in_d_bits_sink),
    .auto_in_d_bits_denied(buffer_auto_in_d_bits_denied),
    .auto_in_d_bits_data(buffer_auto_in_d_bits_data),
    .auto_in_d_bits_corrupt(buffer_auto_in_d_bits_corrupt),
    .auto_out_a_ready(buffer_auto_out_a_ready),
    .auto_out_a_valid(buffer_auto_out_a_valid),
    .auto_out_a_bits_opcode(buffer_auto_out_a_bits_opcode),
    .auto_out_a_bits_param(buffer_auto_out_a_bits_param),
    .auto_out_a_bits_size(buffer_auto_out_a_bits_size),
    .auto_out_a_bits_source(buffer_auto_out_a_bits_source),
    .auto_out_a_bits_address(buffer_auto_out_a_bits_address),
    .auto_out_a_bits_user_amba_prot_bufferable(buffer_auto_out_a_bits_user_amba_prot_bufferable),
    .auto_out_a_bits_user_amba_prot_modifiable(buffer_auto_out_a_bits_user_amba_prot_modifiable),
    .auto_out_a_bits_user_amba_prot_readalloc(buffer_auto_out_a_bits_user_amba_prot_readalloc),
    .auto_out_a_bits_user_amba_prot_writealloc(buffer_auto_out_a_bits_user_amba_prot_writealloc),
    .auto_out_a_bits_user_amba_prot_privileged(buffer_auto_out_a_bits_user_amba_prot_privileged),
    .auto_out_a_bits_user_amba_prot_secure(buffer_auto_out_a_bits_user_amba_prot_secure),
    .auto_out_a_bits_user_amba_prot_fetch(buffer_auto_out_a_bits_user_amba_prot_fetch),
    .auto_out_a_bits_mask(buffer_auto_out_a_bits_mask),
    .auto_out_a_bits_data(buffer_auto_out_a_bits_data),
    .auto_out_a_bits_corrupt(buffer_auto_out_a_bits_corrupt),
    .auto_out_d_ready(buffer_auto_out_d_ready),
    .auto_out_d_valid(buffer_auto_out_d_valid),
    .auto_out_d_bits_opcode(buffer_auto_out_d_bits_opcode),
    .auto_out_d_bits_param(buffer_auto_out_d_bits_param),
    .auto_out_d_bits_size(buffer_auto_out_d_bits_size),
    .auto_out_d_bits_source(buffer_auto_out_d_bits_source),
    .auto_out_d_bits_sink(buffer_auto_out_d_bits_sink),
    .auto_out_d_bits_denied(buffer_auto_out_d_bits_denied),
    .auto_out_d_bits_data(buffer_auto_out_d_bits_data),
    .auto_out_d_bits_corrupt(buffer_auto_out_d_bits_corrupt)
  );
  RHEA__TLAtomicAutomata_1 atomics ( // @[AtomicAutomata.scala 283:29]
    .rf_reset(atomics_rf_reset),
    .clock(atomics_clock),
    .reset(atomics_reset),
    .auto_in_a_ready(atomics_auto_in_a_ready),
    .auto_in_a_valid(atomics_auto_in_a_valid),
    .auto_in_a_bits_opcode(atomics_auto_in_a_bits_opcode),
    .auto_in_a_bits_param(atomics_auto_in_a_bits_param),
    .auto_in_a_bits_size(atomics_auto_in_a_bits_size),
    .auto_in_a_bits_source(atomics_auto_in_a_bits_source),
    .auto_in_a_bits_address(atomics_auto_in_a_bits_address),
    .auto_in_a_bits_user_amba_prot_bufferable(atomics_auto_in_a_bits_user_amba_prot_bufferable),
    .auto_in_a_bits_user_amba_prot_modifiable(atomics_auto_in_a_bits_user_amba_prot_modifiable),
    .auto_in_a_bits_user_amba_prot_readalloc(atomics_auto_in_a_bits_user_amba_prot_readalloc),
    .auto_in_a_bits_user_amba_prot_writealloc(atomics_auto_in_a_bits_user_amba_prot_writealloc),
    .auto_in_a_bits_user_amba_prot_privileged(atomics_auto_in_a_bits_user_amba_prot_privileged),
    .auto_in_a_bits_user_amba_prot_secure(atomics_auto_in_a_bits_user_amba_prot_secure),
    .auto_in_a_bits_user_amba_prot_fetch(atomics_auto_in_a_bits_user_amba_prot_fetch),
    .auto_in_a_bits_mask(atomics_auto_in_a_bits_mask),
    .auto_in_a_bits_data(atomics_auto_in_a_bits_data),
    .auto_in_a_bits_corrupt(atomics_auto_in_a_bits_corrupt),
    .auto_in_d_ready(atomics_auto_in_d_ready),
    .auto_in_d_valid(atomics_auto_in_d_valid),
    .auto_in_d_bits_opcode(atomics_auto_in_d_bits_opcode),
    .auto_in_d_bits_param(atomics_auto_in_d_bits_param),
    .auto_in_d_bits_size(atomics_auto_in_d_bits_size),
    .auto_in_d_bits_source(atomics_auto_in_d_bits_source),
    .auto_in_d_bits_sink(atomics_auto_in_d_bits_sink),
    .auto_in_d_bits_denied(atomics_auto_in_d_bits_denied),
    .auto_in_d_bits_data(atomics_auto_in_d_bits_data),
    .auto_in_d_bits_corrupt(atomics_auto_in_d_bits_corrupt),
    .auto_out_a_ready(atomics_auto_out_a_ready),
    .auto_out_a_valid(atomics_auto_out_a_valid),
    .auto_out_a_bits_opcode(atomics_auto_out_a_bits_opcode),
    .auto_out_a_bits_param(atomics_auto_out_a_bits_param),
    .auto_out_a_bits_size(atomics_auto_out_a_bits_size),
    .auto_out_a_bits_source(atomics_auto_out_a_bits_source),
    .auto_out_a_bits_address(atomics_auto_out_a_bits_address),
    .auto_out_a_bits_user_amba_prot_bufferable(atomics_auto_out_a_bits_user_amba_prot_bufferable),
    .auto_out_a_bits_user_amba_prot_modifiable(atomics_auto_out_a_bits_user_amba_prot_modifiable),
    .auto_out_a_bits_user_amba_prot_readalloc(atomics_auto_out_a_bits_user_amba_prot_readalloc),
    .auto_out_a_bits_user_amba_prot_writealloc(atomics_auto_out_a_bits_user_amba_prot_writealloc),
    .auto_out_a_bits_user_amba_prot_privileged(atomics_auto_out_a_bits_user_amba_prot_privileged),
    .auto_out_a_bits_user_amba_prot_secure(atomics_auto_out_a_bits_user_amba_prot_secure),
    .auto_out_a_bits_user_amba_prot_fetch(atomics_auto_out_a_bits_user_amba_prot_fetch),
    .auto_out_a_bits_mask(atomics_auto_out_a_bits_mask),
    .auto_out_a_bits_data(atomics_auto_out_a_bits_data),
    .auto_out_a_bits_corrupt(atomics_auto_out_a_bits_corrupt),
    .auto_out_d_ready(atomics_auto_out_d_ready),
    .auto_out_d_valid(atomics_auto_out_d_valid),
    .auto_out_d_bits_opcode(atomics_auto_out_d_bits_opcode),
    .auto_out_d_bits_param(atomics_auto_out_d_bits_param),
    .auto_out_d_bits_size(atomics_auto_out_d_bits_size),
    .auto_out_d_bits_source(atomics_auto_out_d_bits_source),
    .auto_out_d_bits_sink(atomics_auto_out_d_bits_sink),
    .auto_out_d_bits_denied(atomics_auto_out_d_bits_denied),
    .auto_out_d_bits_data(atomics_auto_out_d_bits_data),
    .auto_out_d_bits_corrupt(atomics_auto_out_d_bits_corrupt)
  );
  RHEA__ErrorDeviceWrapper wrapped_error_device ( // @[LazyModule.scala 524:27]
    .rf_reset(wrapped_error_device_rf_reset),
    .clock(wrapped_error_device_clock),
    .reset(wrapped_error_device_reset),
    .auto_buffer_in_a_ready(wrapped_error_device_auto_buffer_in_a_ready),
    .auto_buffer_in_a_valid(wrapped_error_device_auto_buffer_in_a_valid),
    .auto_buffer_in_a_bits_opcode(wrapped_error_device_auto_buffer_in_a_bits_opcode),
    .auto_buffer_in_a_bits_param(wrapped_error_device_auto_buffer_in_a_bits_param),
    .auto_buffer_in_a_bits_size(wrapped_error_device_auto_buffer_in_a_bits_size),
    .auto_buffer_in_a_bits_source(wrapped_error_device_auto_buffer_in_a_bits_source),
    .auto_buffer_in_a_bits_address(wrapped_error_device_auto_buffer_in_a_bits_address),
    .auto_buffer_in_a_bits_mask(wrapped_error_device_auto_buffer_in_a_bits_mask),
    .auto_buffer_in_a_bits_corrupt(wrapped_error_device_auto_buffer_in_a_bits_corrupt),
    .auto_buffer_in_d_ready(wrapped_error_device_auto_buffer_in_d_ready),
    .auto_buffer_in_d_valid(wrapped_error_device_auto_buffer_in_d_valid),
    .auto_buffer_in_d_bits_opcode(wrapped_error_device_auto_buffer_in_d_bits_opcode),
    .auto_buffer_in_d_bits_param(wrapped_error_device_auto_buffer_in_d_bits_param),
    .auto_buffer_in_d_bits_size(wrapped_error_device_auto_buffer_in_d_bits_size),
    .auto_buffer_in_d_bits_source(wrapped_error_device_auto_buffer_in_d_bits_source),
    .auto_buffer_in_d_bits_sink(wrapped_error_device_auto_buffer_in_d_bits_sink),
    .auto_buffer_in_d_bits_denied(wrapped_error_device_auto_buffer_in_d_bits_denied),
    .auto_buffer_in_d_bits_data(wrapped_error_device_auto_buffer_in_d_bits_data),
    .auto_buffer_in_d_bits_corrupt(wrapped_error_device_auto_buffer_in_d_bits_corrupt)
  );
  RHEA__TLInterconnectCoupler_10 coupler_to_l2_ctrl ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_to_l2_ctrl_rf_reset),
    .clock(coupler_to_l2_ctrl_clock),
    .reset(coupler_to_l2_ctrl_reset),
    .auto_buffer_out_a_ready(coupler_to_l2_ctrl_auto_buffer_out_a_ready),
    .auto_buffer_out_a_valid(coupler_to_l2_ctrl_auto_buffer_out_a_valid),
    .auto_buffer_out_a_bits_opcode(coupler_to_l2_ctrl_auto_buffer_out_a_bits_opcode),
    .auto_buffer_out_a_bits_param(coupler_to_l2_ctrl_auto_buffer_out_a_bits_param),
    .auto_buffer_out_a_bits_size(coupler_to_l2_ctrl_auto_buffer_out_a_bits_size),
    .auto_buffer_out_a_bits_source(coupler_to_l2_ctrl_auto_buffer_out_a_bits_source),
    .auto_buffer_out_a_bits_address(coupler_to_l2_ctrl_auto_buffer_out_a_bits_address),
    .auto_buffer_out_a_bits_mask(coupler_to_l2_ctrl_auto_buffer_out_a_bits_mask),
    .auto_buffer_out_a_bits_data(coupler_to_l2_ctrl_auto_buffer_out_a_bits_data),
    .auto_buffer_out_a_bits_corrupt(coupler_to_l2_ctrl_auto_buffer_out_a_bits_corrupt),
    .auto_buffer_out_d_ready(coupler_to_l2_ctrl_auto_buffer_out_d_ready),
    .auto_buffer_out_d_valid(coupler_to_l2_ctrl_auto_buffer_out_d_valid),
    .auto_buffer_out_d_bits_opcode(coupler_to_l2_ctrl_auto_buffer_out_d_bits_opcode),
    .auto_buffer_out_d_bits_size(coupler_to_l2_ctrl_auto_buffer_out_d_bits_size),
    .auto_buffer_out_d_bits_source(coupler_to_l2_ctrl_auto_buffer_out_d_bits_source),
    .auto_buffer_out_d_bits_data(coupler_to_l2_ctrl_auto_buffer_out_d_bits_data),
    .auto_tl_in_a_ready(coupler_to_l2_ctrl_auto_tl_in_a_ready),
    .auto_tl_in_a_valid(coupler_to_l2_ctrl_auto_tl_in_a_valid),
    .auto_tl_in_a_bits_opcode(coupler_to_l2_ctrl_auto_tl_in_a_bits_opcode),
    .auto_tl_in_a_bits_param(coupler_to_l2_ctrl_auto_tl_in_a_bits_param),
    .auto_tl_in_a_bits_size(coupler_to_l2_ctrl_auto_tl_in_a_bits_size),
    .auto_tl_in_a_bits_source(coupler_to_l2_ctrl_auto_tl_in_a_bits_source),
    .auto_tl_in_a_bits_address(coupler_to_l2_ctrl_auto_tl_in_a_bits_address),
    .auto_tl_in_a_bits_mask(coupler_to_l2_ctrl_auto_tl_in_a_bits_mask),
    .auto_tl_in_a_bits_data(coupler_to_l2_ctrl_auto_tl_in_a_bits_data),
    .auto_tl_in_a_bits_corrupt(coupler_to_l2_ctrl_auto_tl_in_a_bits_corrupt),
    .auto_tl_in_d_ready(coupler_to_l2_ctrl_auto_tl_in_d_ready),
    .auto_tl_in_d_valid(coupler_to_l2_ctrl_auto_tl_in_d_valid),
    .auto_tl_in_d_bits_opcode(coupler_to_l2_ctrl_auto_tl_in_d_bits_opcode),
    .auto_tl_in_d_bits_size(coupler_to_l2_ctrl_auto_tl_in_d_bits_size),
    .auto_tl_in_d_bits_source(coupler_to_l2_ctrl_auto_tl_in_d_bits_source),
    .auto_tl_in_d_bits_data(coupler_to_l2_ctrl_auto_tl_in_d_bits_data)
  );
  RHEA__TLInterconnectCoupler_12 coupler_to_plic ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_to_plic_rf_reset),
    .clock(coupler_to_plic_clock),
    .reset(coupler_to_plic_reset),
    .auto_fragmenter_out_a_ready(coupler_to_plic_auto_fragmenter_out_a_ready),
    .auto_fragmenter_out_a_valid(coupler_to_plic_auto_fragmenter_out_a_valid),
    .auto_fragmenter_out_a_bits_opcode(coupler_to_plic_auto_fragmenter_out_a_bits_opcode),
    .auto_fragmenter_out_a_bits_param(coupler_to_plic_auto_fragmenter_out_a_bits_param),
    .auto_fragmenter_out_a_bits_size(coupler_to_plic_auto_fragmenter_out_a_bits_size),
    .auto_fragmenter_out_a_bits_source(coupler_to_plic_auto_fragmenter_out_a_bits_source),
    .auto_fragmenter_out_a_bits_address(coupler_to_plic_auto_fragmenter_out_a_bits_address),
    .auto_fragmenter_out_a_bits_mask(coupler_to_plic_auto_fragmenter_out_a_bits_mask),
    .auto_fragmenter_out_a_bits_data(coupler_to_plic_auto_fragmenter_out_a_bits_data),
    .auto_fragmenter_out_a_bits_corrupt(coupler_to_plic_auto_fragmenter_out_a_bits_corrupt),
    .auto_fragmenter_out_d_ready(coupler_to_plic_auto_fragmenter_out_d_ready),
    .auto_fragmenter_out_d_valid(coupler_to_plic_auto_fragmenter_out_d_valid),
    .auto_fragmenter_out_d_bits_opcode(coupler_to_plic_auto_fragmenter_out_d_bits_opcode),
    .auto_fragmenter_out_d_bits_size(coupler_to_plic_auto_fragmenter_out_d_bits_size),
    .auto_fragmenter_out_d_bits_source(coupler_to_plic_auto_fragmenter_out_d_bits_source),
    .auto_fragmenter_out_d_bits_data(coupler_to_plic_auto_fragmenter_out_d_bits_data),
    .auto_tl_in_a_ready(coupler_to_plic_auto_tl_in_a_ready),
    .auto_tl_in_a_valid(coupler_to_plic_auto_tl_in_a_valid),
    .auto_tl_in_a_bits_opcode(coupler_to_plic_auto_tl_in_a_bits_opcode),
    .auto_tl_in_a_bits_param(coupler_to_plic_auto_tl_in_a_bits_param),
    .auto_tl_in_a_bits_size(coupler_to_plic_auto_tl_in_a_bits_size),
    .auto_tl_in_a_bits_source(coupler_to_plic_auto_tl_in_a_bits_source),
    .auto_tl_in_a_bits_address(coupler_to_plic_auto_tl_in_a_bits_address),
    .auto_tl_in_a_bits_mask(coupler_to_plic_auto_tl_in_a_bits_mask),
    .auto_tl_in_a_bits_data(coupler_to_plic_auto_tl_in_a_bits_data),
    .auto_tl_in_a_bits_corrupt(coupler_to_plic_auto_tl_in_a_bits_corrupt),
    .auto_tl_in_d_ready(coupler_to_plic_auto_tl_in_d_ready),
    .auto_tl_in_d_valid(coupler_to_plic_auto_tl_in_d_valid),
    .auto_tl_in_d_bits_opcode(coupler_to_plic_auto_tl_in_d_bits_opcode),
    .auto_tl_in_d_bits_size(coupler_to_plic_auto_tl_in_d_bits_size),
    .auto_tl_in_d_bits_source(coupler_to_plic_auto_tl_in_d_bits_source),
    .auto_tl_in_d_bits_data(coupler_to_plic_auto_tl_in_d_bits_data)
  );
  RHEA__TLInterconnectCoupler_13 coupler_to_clint ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_to_clint_rf_reset),
    .clock(coupler_to_clint_clock),
    .reset(coupler_to_clint_reset),
    .auto_fragmenter_out_a_ready(coupler_to_clint_auto_fragmenter_out_a_ready),
    .auto_fragmenter_out_a_valid(coupler_to_clint_auto_fragmenter_out_a_valid),
    .auto_fragmenter_out_a_bits_opcode(coupler_to_clint_auto_fragmenter_out_a_bits_opcode),
    .auto_fragmenter_out_a_bits_param(coupler_to_clint_auto_fragmenter_out_a_bits_param),
    .auto_fragmenter_out_a_bits_size(coupler_to_clint_auto_fragmenter_out_a_bits_size),
    .auto_fragmenter_out_a_bits_source(coupler_to_clint_auto_fragmenter_out_a_bits_source),
    .auto_fragmenter_out_a_bits_address(coupler_to_clint_auto_fragmenter_out_a_bits_address),
    .auto_fragmenter_out_a_bits_mask(coupler_to_clint_auto_fragmenter_out_a_bits_mask),
    .auto_fragmenter_out_a_bits_data(coupler_to_clint_auto_fragmenter_out_a_bits_data),
    .auto_fragmenter_out_a_bits_corrupt(coupler_to_clint_auto_fragmenter_out_a_bits_corrupt),
    .auto_fragmenter_out_d_ready(coupler_to_clint_auto_fragmenter_out_d_ready),
    .auto_fragmenter_out_d_valid(coupler_to_clint_auto_fragmenter_out_d_valid),
    .auto_fragmenter_out_d_bits_opcode(coupler_to_clint_auto_fragmenter_out_d_bits_opcode),
    .auto_fragmenter_out_d_bits_size(coupler_to_clint_auto_fragmenter_out_d_bits_size),
    .auto_fragmenter_out_d_bits_source(coupler_to_clint_auto_fragmenter_out_d_bits_source),
    .auto_fragmenter_out_d_bits_data(coupler_to_clint_auto_fragmenter_out_d_bits_data),
    .auto_tl_in_a_ready(coupler_to_clint_auto_tl_in_a_ready),
    .auto_tl_in_a_valid(coupler_to_clint_auto_tl_in_a_valid),
    .auto_tl_in_a_bits_opcode(coupler_to_clint_auto_tl_in_a_bits_opcode),
    .auto_tl_in_a_bits_param(coupler_to_clint_auto_tl_in_a_bits_param),
    .auto_tl_in_a_bits_size(coupler_to_clint_auto_tl_in_a_bits_size),
    .auto_tl_in_a_bits_source(coupler_to_clint_auto_tl_in_a_bits_source),
    .auto_tl_in_a_bits_address(coupler_to_clint_auto_tl_in_a_bits_address),
    .auto_tl_in_a_bits_mask(coupler_to_clint_auto_tl_in_a_bits_mask),
    .auto_tl_in_a_bits_data(coupler_to_clint_auto_tl_in_a_bits_data),
    .auto_tl_in_a_bits_corrupt(coupler_to_clint_auto_tl_in_a_bits_corrupt),
    .auto_tl_in_d_ready(coupler_to_clint_auto_tl_in_d_ready),
    .auto_tl_in_d_valid(coupler_to_clint_auto_tl_in_d_valid),
    .auto_tl_in_d_bits_opcode(coupler_to_clint_auto_tl_in_d_bits_opcode),
    .auto_tl_in_d_bits_size(coupler_to_clint_auto_tl_in_d_bits_size),
    .auto_tl_in_d_bits_source(coupler_to_clint_auto_tl_in_d_bits_source),
    .auto_tl_in_d_bits_data(coupler_to_clint_auto_tl_in_d_bits_data)
  );
  RHEA__TLInterconnectCoupler_14 coupler_to_debug ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_to_debug_rf_reset),
    .clock(coupler_to_debug_clock),
    .reset(coupler_to_debug_reset),
    .auto_fragmenter_out_a_ready(coupler_to_debug_auto_fragmenter_out_a_ready),
    .auto_fragmenter_out_a_valid(coupler_to_debug_auto_fragmenter_out_a_valid),
    .auto_fragmenter_out_a_bits_opcode(coupler_to_debug_auto_fragmenter_out_a_bits_opcode),
    .auto_fragmenter_out_a_bits_param(coupler_to_debug_auto_fragmenter_out_a_bits_param),
    .auto_fragmenter_out_a_bits_size(coupler_to_debug_auto_fragmenter_out_a_bits_size),
    .auto_fragmenter_out_a_bits_source(coupler_to_debug_auto_fragmenter_out_a_bits_source),
    .auto_fragmenter_out_a_bits_address(coupler_to_debug_auto_fragmenter_out_a_bits_address),
    .auto_fragmenter_out_a_bits_mask(coupler_to_debug_auto_fragmenter_out_a_bits_mask),
    .auto_fragmenter_out_a_bits_data(coupler_to_debug_auto_fragmenter_out_a_bits_data),
    .auto_fragmenter_out_a_bits_corrupt(coupler_to_debug_auto_fragmenter_out_a_bits_corrupt),
    .auto_fragmenter_out_d_ready(coupler_to_debug_auto_fragmenter_out_d_ready),
    .auto_fragmenter_out_d_valid(coupler_to_debug_auto_fragmenter_out_d_valid),
    .auto_fragmenter_out_d_bits_opcode(coupler_to_debug_auto_fragmenter_out_d_bits_opcode),
    .auto_fragmenter_out_d_bits_size(coupler_to_debug_auto_fragmenter_out_d_bits_size),
    .auto_fragmenter_out_d_bits_source(coupler_to_debug_auto_fragmenter_out_d_bits_source),
    .auto_fragmenter_out_d_bits_data(coupler_to_debug_auto_fragmenter_out_d_bits_data),
    .auto_tl_in_a_ready(coupler_to_debug_auto_tl_in_a_ready),
    .auto_tl_in_a_valid(coupler_to_debug_auto_tl_in_a_valid),
    .auto_tl_in_a_bits_opcode(coupler_to_debug_auto_tl_in_a_bits_opcode),
    .auto_tl_in_a_bits_param(coupler_to_debug_auto_tl_in_a_bits_param),
    .auto_tl_in_a_bits_size(coupler_to_debug_auto_tl_in_a_bits_size),
    .auto_tl_in_a_bits_source(coupler_to_debug_auto_tl_in_a_bits_source),
    .auto_tl_in_a_bits_address(coupler_to_debug_auto_tl_in_a_bits_address),
    .auto_tl_in_a_bits_mask(coupler_to_debug_auto_tl_in_a_bits_mask),
    .auto_tl_in_a_bits_data(coupler_to_debug_auto_tl_in_a_bits_data),
    .auto_tl_in_a_bits_corrupt(coupler_to_debug_auto_tl_in_a_bits_corrupt),
    .auto_tl_in_d_ready(coupler_to_debug_auto_tl_in_d_ready),
    .auto_tl_in_d_valid(coupler_to_debug_auto_tl_in_d_valid),
    .auto_tl_in_d_bits_opcode(coupler_to_debug_auto_tl_in_d_bits_opcode),
    .auto_tl_in_d_bits_size(coupler_to_debug_auto_tl_in_d_bits_size),
    .auto_tl_in_d_bits_source(coupler_to_debug_auto_tl_in_d_bits_source),
    .auto_tl_in_d_bits_data(coupler_to_debug_auto_tl_in_d_bits_data)
  );
  RHEA__TLInterconnectCoupler_17 coupler_to_port_named_axi4_periph_port ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_to_port_named_axi4_periph_port_rf_reset),
    .clock(coupler_to_port_named_axi4_periph_port_clock),
    .reset(coupler_to_port_named_axi4_periph_port_reset),
    .auto_bridge_for_axi4_periph_port_axi4_out_aw_ready(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_ready),
    .auto_bridge_for_axi4_periph_port_axi4_out_aw_valid(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_valid),
    .auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_addr(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_addr),
    .auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_len(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_len),
    .auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_size(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_size),
    .auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_cache(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_cache),
    .auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_prot(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_prot),
    .auto_bridge_for_axi4_periph_port_axi4_out_w_ready(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_w_ready),
    .auto_bridge_for_axi4_periph_port_axi4_out_w_valid(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_w_valid),
    .auto_bridge_for_axi4_periph_port_axi4_out_w_bits_data(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_w_bits_data),
    .auto_bridge_for_axi4_periph_port_axi4_out_w_bits_strb(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_w_bits_strb),
    .auto_bridge_for_axi4_periph_port_axi4_out_w_bits_last(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_w_bits_last),
    .auto_bridge_for_axi4_periph_port_axi4_out_b_ready(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_b_ready),
    .auto_bridge_for_axi4_periph_port_axi4_out_b_valid(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_b_valid),
    .auto_bridge_for_axi4_periph_port_axi4_out_b_bits_resp(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_b_bits_resp),
    .auto_bridge_for_axi4_periph_port_axi4_out_ar_ready(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_ready),
    .auto_bridge_for_axi4_periph_port_axi4_out_ar_valid(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_valid),
    .auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_addr(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_addr),
    .auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_len(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_len),
    .auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_size(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_size),
    .auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_cache(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_cache),
    .auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_prot(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_prot),
    .auto_bridge_for_axi4_periph_port_axi4_out_r_ready(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_r_ready),
    .auto_bridge_for_axi4_periph_port_axi4_out_r_valid(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_r_valid),
    .auto_bridge_for_axi4_periph_port_axi4_out_r_bits_data(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_r_bits_data),
    .auto_bridge_for_axi4_periph_port_axi4_out_r_bits_resp(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_r_bits_resp),
    .auto_bridge_for_axi4_periph_port_axi4_out_r_bits_last(
      coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_r_bits_last),
    .auto_tl_in_a_ready(coupler_to_port_named_axi4_periph_port_auto_tl_in_a_ready),
    .auto_tl_in_a_valid(coupler_to_port_named_axi4_periph_port_auto_tl_in_a_valid),
    .auto_tl_in_a_bits_opcode(coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_opcode),
    .auto_tl_in_a_bits_param(coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_param),
    .auto_tl_in_a_bits_size(coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_size),
    .auto_tl_in_a_bits_source(coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_source),
    .auto_tl_in_a_bits_address(coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_address),
    .auto_tl_in_a_bits_user_amba_prot_bufferable(
      coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_bufferable),
    .auto_tl_in_a_bits_user_amba_prot_modifiable(
      coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_modifiable),
    .auto_tl_in_a_bits_user_amba_prot_readalloc(
      coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_readalloc),
    .auto_tl_in_a_bits_user_amba_prot_writealloc(
      coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_writealloc),
    .auto_tl_in_a_bits_user_amba_prot_privileged(
      coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_privileged),
    .auto_tl_in_a_bits_user_amba_prot_secure(
      coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_secure),
    .auto_tl_in_a_bits_user_amba_prot_fetch(
      coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_fetch),
    .auto_tl_in_a_bits_mask(coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_mask),
    .auto_tl_in_a_bits_data(coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_data),
    .auto_tl_in_a_bits_corrupt(coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_corrupt),
    .auto_tl_in_d_ready(coupler_to_port_named_axi4_periph_port_auto_tl_in_d_ready),
    .auto_tl_in_d_valid(coupler_to_port_named_axi4_periph_port_auto_tl_in_d_valid),
    .auto_tl_in_d_bits_opcode(coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_opcode),
    .auto_tl_in_d_bits_size(coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_size),
    .auto_tl_in_d_bits_source(coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_source),
    .auto_tl_in_d_bits_denied(coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_denied),
    .auto_tl_in_d_bits_data(coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_data),
    .auto_tl_in_d_bits_corrupt(coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_corrupt)
  );
  RHEA__TLInterconnectCoupler_18 coupler_to_traceencoder ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_to_traceencoder_rf_reset),
    .clock(coupler_to_traceencoder_clock),
    .reset(coupler_to_traceencoder_reset),
    .auto_fragmenter_out_a_ready(coupler_to_traceencoder_auto_fragmenter_out_a_ready),
    .auto_fragmenter_out_a_valid(coupler_to_traceencoder_auto_fragmenter_out_a_valid),
    .auto_fragmenter_out_a_bits_opcode(coupler_to_traceencoder_auto_fragmenter_out_a_bits_opcode),
    .auto_fragmenter_out_a_bits_param(coupler_to_traceencoder_auto_fragmenter_out_a_bits_param),
    .auto_fragmenter_out_a_bits_size(coupler_to_traceencoder_auto_fragmenter_out_a_bits_size),
    .auto_fragmenter_out_a_bits_source(coupler_to_traceencoder_auto_fragmenter_out_a_bits_source),
    .auto_fragmenter_out_a_bits_address(coupler_to_traceencoder_auto_fragmenter_out_a_bits_address),
    .auto_fragmenter_out_a_bits_mask(coupler_to_traceencoder_auto_fragmenter_out_a_bits_mask),
    .auto_fragmenter_out_a_bits_data(coupler_to_traceencoder_auto_fragmenter_out_a_bits_data),
    .auto_fragmenter_out_a_bits_corrupt(coupler_to_traceencoder_auto_fragmenter_out_a_bits_corrupt),
    .auto_fragmenter_out_d_ready(coupler_to_traceencoder_auto_fragmenter_out_d_ready),
    .auto_fragmenter_out_d_valid(coupler_to_traceencoder_auto_fragmenter_out_d_valid),
    .auto_fragmenter_out_d_bits_opcode(coupler_to_traceencoder_auto_fragmenter_out_d_bits_opcode),
    .auto_fragmenter_out_d_bits_size(coupler_to_traceencoder_auto_fragmenter_out_d_bits_size),
    .auto_fragmenter_out_d_bits_source(coupler_to_traceencoder_auto_fragmenter_out_d_bits_source),
    .auto_fragmenter_out_d_bits_data(coupler_to_traceencoder_auto_fragmenter_out_d_bits_data),
    .auto_tl_in_a_ready(coupler_to_traceencoder_auto_tl_in_a_ready),
    .auto_tl_in_a_valid(coupler_to_traceencoder_auto_tl_in_a_valid),
    .auto_tl_in_a_bits_opcode(coupler_to_traceencoder_auto_tl_in_a_bits_opcode),
    .auto_tl_in_a_bits_param(coupler_to_traceencoder_auto_tl_in_a_bits_param),
    .auto_tl_in_a_bits_size(coupler_to_traceencoder_auto_tl_in_a_bits_size),
    .auto_tl_in_a_bits_source(coupler_to_traceencoder_auto_tl_in_a_bits_source),
    .auto_tl_in_a_bits_address(coupler_to_traceencoder_auto_tl_in_a_bits_address),
    .auto_tl_in_a_bits_mask(coupler_to_traceencoder_auto_tl_in_a_bits_mask),
    .auto_tl_in_a_bits_data(coupler_to_traceencoder_auto_tl_in_a_bits_data),
    .auto_tl_in_a_bits_corrupt(coupler_to_traceencoder_auto_tl_in_a_bits_corrupt),
    .auto_tl_in_d_ready(coupler_to_traceencoder_auto_tl_in_d_ready),
    .auto_tl_in_d_valid(coupler_to_traceencoder_auto_tl_in_d_valid),
    .auto_tl_in_d_bits_opcode(coupler_to_traceencoder_auto_tl_in_d_bits_opcode),
    .auto_tl_in_d_bits_size(coupler_to_traceencoder_auto_tl_in_d_bits_size),
    .auto_tl_in_d_bits_source(coupler_to_traceencoder_auto_tl_in_d_bits_source),
    .auto_tl_in_d_bits_data(coupler_to_traceencoder_auto_tl_in_d_bits_data)
  );
  RHEA__TLInterconnectCoupler_19 coupler_to_traceencoder_1 ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_to_traceencoder_1_rf_reset),
    .clock(coupler_to_traceencoder_1_clock),
    .reset(coupler_to_traceencoder_1_reset),
    .auto_fragmenter_out_a_ready(coupler_to_traceencoder_1_auto_fragmenter_out_a_ready),
    .auto_fragmenter_out_a_valid(coupler_to_traceencoder_1_auto_fragmenter_out_a_valid),
    .auto_fragmenter_out_a_bits_opcode(coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_opcode),
    .auto_fragmenter_out_a_bits_param(coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_param),
    .auto_fragmenter_out_a_bits_size(coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_size),
    .auto_fragmenter_out_a_bits_source(coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_source),
    .auto_fragmenter_out_a_bits_address(coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_address),
    .auto_fragmenter_out_a_bits_mask(coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_mask),
    .auto_fragmenter_out_a_bits_data(coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_data),
    .auto_fragmenter_out_a_bits_corrupt(coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_corrupt),
    .auto_fragmenter_out_d_ready(coupler_to_traceencoder_1_auto_fragmenter_out_d_ready),
    .auto_fragmenter_out_d_valid(coupler_to_traceencoder_1_auto_fragmenter_out_d_valid),
    .auto_fragmenter_out_d_bits_opcode(coupler_to_traceencoder_1_auto_fragmenter_out_d_bits_opcode),
    .auto_fragmenter_out_d_bits_size(coupler_to_traceencoder_1_auto_fragmenter_out_d_bits_size),
    .auto_fragmenter_out_d_bits_source(coupler_to_traceencoder_1_auto_fragmenter_out_d_bits_source),
    .auto_fragmenter_out_d_bits_data(coupler_to_traceencoder_1_auto_fragmenter_out_d_bits_data),
    .auto_tl_in_a_ready(coupler_to_traceencoder_1_auto_tl_in_a_ready),
    .auto_tl_in_a_valid(coupler_to_traceencoder_1_auto_tl_in_a_valid),
    .auto_tl_in_a_bits_opcode(coupler_to_traceencoder_1_auto_tl_in_a_bits_opcode),
    .auto_tl_in_a_bits_param(coupler_to_traceencoder_1_auto_tl_in_a_bits_param),
    .auto_tl_in_a_bits_size(coupler_to_traceencoder_1_auto_tl_in_a_bits_size),
    .auto_tl_in_a_bits_source(coupler_to_traceencoder_1_auto_tl_in_a_bits_source),
    .auto_tl_in_a_bits_address(coupler_to_traceencoder_1_auto_tl_in_a_bits_address),
    .auto_tl_in_a_bits_mask(coupler_to_traceencoder_1_auto_tl_in_a_bits_mask),
    .auto_tl_in_a_bits_data(coupler_to_traceencoder_1_auto_tl_in_a_bits_data),
    .auto_tl_in_a_bits_corrupt(coupler_to_traceencoder_1_auto_tl_in_a_bits_corrupt),
    .auto_tl_in_d_ready(coupler_to_traceencoder_1_auto_tl_in_d_ready),
    .auto_tl_in_d_valid(coupler_to_traceencoder_1_auto_tl_in_d_valid),
    .auto_tl_in_d_bits_opcode(coupler_to_traceencoder_1_auto_tl_in_d_bits_opcode),
    .auto_tl_in_d_bits_size(coupler_to_traceencoder_1_auto_tl_in_d_bits_size),
    .auto_tl_in_d_bits_source(coupler_to_traceencoder_1_auto_tl_in_d_bits_source),
    .auto_tl_in_d_bits_data(coupler_to_traceencoder_1_auto_tl_in_d_bits_data)
  );
  RHEA__TLInterconnectCoupler_20 coupler_to_tracefunnel ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_to_tracefunnel_rf_reset),
    .clock(coupler_to_tracefunnel_clock),
    .reset(coupler_to_tracefunnel_reset),
    .auto_fragmenter_out_a_ready(coupler_to_tracefunnel_auto_fragmenter_out_a_ready),
    .auto_fragmenter_out_a_valid(coupler_to_tracefunnel_auto_fragmenter_out_a_valid),
    .auto_fragmenter_out_a_bits_opcode(coupler_to_tracefunnel_auto_fragmenter_out_a_bits_opcode),
    .auto_fragmenter_out_a_bits_param(coupler_to_tracefunnel_auto_fragmenter_out_a_bits_param),
    .auto_fragmenter_out_a_bits_size(coupler_to_tracefunnel_auto_fragmenter_out_a_bits_size),
    .auto_fragmenter_out_a_bits_source(coupler_to_tracefunnel_auto_fragmenter_out_a_bits_source),
    .auto_fragmenter_out_a_bits_address(coupler_to_tracefunnel_auto_fragmenter_out_a_bits_address),
    .auto_fragmenter_out_a_bits_mask(coupler_to_tracefunnel_auto_fragmenter_out_a_bits_mask),
    .auto_fragmenter_out_a_bits_data(coupler_to_tracefunnel_auto_fragmenter_out_a_bits_data),
    .auto_fragmenter_out_a_bits_corrupt(coupler_to_tracefunnel_auto_fragmenter_out_a_bits_corrupt),
    .auto_fragmenter_out_d_ready(coupler_to_tracefunnel_auto_fragmenter_out_d_ready),
    .auto_fragmenter_out_d_valid(coupler_to_tracefunnel_auto_fragmenter_out_d_valid),
    .auto_fragmenter_out_d_bits_opcode(coupler_to_tracefunnel_auto_fragmenter_out_d_bits_opcode),
    .auto_fragmenter_out_d_bits_size(coupler_to_tracefunnel_auto_fragmenter_out_d_bits_size),
    .auto_fragmenter_out_d_bits_source(coupler_to_tracefunnel_auto_fragmenter_out_d_bits_source),
    .auto_fragmenter_out_d_bits_data(coupler_to_tracefunnel_auto_fragmenter_out_d_bits_data),
    .auto_tl_in_a_ready(coupler_to_tracefunnel_auto_tl_in_a_ready),
    .auto_tl_in_a_valid(coupler_to_tracefunnel_auto_tl_in_a_valid),
    .auto_tl_in_a_bits_opcode(coupler_to_tracefunnel_auto_tl_in_a_bits_opcode),
    .auto_tl_in_a_bits_param(coupler_to_tracefunnel_auto_tl_in_a_bits_param),
    .auto_tl_in_a_bits_size(coupler_to_tracefunnel_auto_tl_in_a_bits_size),
    .auto_tl_in_a_bits_source(coupler_to_tracefunnel_auto_tl_in_a_bits_source),
    .auto_tl_in_a_bits_address(coupler_to_tracefunnel_auto_tl_in_a_bits_address),
    .auto_tl_in_a_bits_mask(coupler_to_tracefunnel_auto_tl_in_a_bits_mask),
    .auto_tl_in_a_bits_data(coupler_to_tracefunnel_auto_tl_in_a_bits_data),
    .auto_tl_in_a_bits_corrupt(coupler_to_tracefunnel_auto_tl_in_a_bits_corrupt),
    .auto_tl_in_d_ready(coupler_to_tracefunnel_auto_tl_in_d_ready),
    .auto_tl_in_d_valid(coupler_to_tracefunnel_auto_tl_in_d_valid),
    .auto_tl_in_d_bits_opcode(coupler_to_tracefunnel_auto_tl_in_d_bits_opcode),
    .auto_tl_in_d_bits_size(coupler_to_tracefunnel_auto_tl_in_d_bits_size),
    .auto_tl_in_d_bits_source(coupler_to_tracefunnel_auto_tl_in_d_bits_source),
    .auto_tl_in_d_bits_data(coupler_to_tracefunnel_auto_tl_in_d_bits_data)
  );
  RHEA__TLInterconnectCoupler_21 coupler_to_testIndicator ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_to_testIndicator_rf_reset),
    .clock(coupler_to_testIndicator_clock),
    .reset(coupler_to_testIndicator_reset),
    .auto_control_xing_out_a_ready(coupler_to_testIndicator_auto_control_xing_out_a_ready),
    .auto_control_xing_out_a_valid(coupler_to_testIndicator_auto_control_xing_out_a_valid),
    .auto_control_xing_out_a_bits_opcode(coupler_to_testIndicator_auto_control_xing_out_a_bits_opcode),
    .auto_control_xing_out_a_bits_param(coupler_to_testIndicator_auto_control_xing_out_a_bits_param),
    .auto_control_xing_out_a_bits_size(coupler_to_testIndicator_auto_control_xing_out_a_bits_size),
    .auto_control_xing_out_a_bits_source(coupler_to_testIndicator_auto_control_xing_out_a_bits_source),
    .auto_control_xing_out_a_bits_address(coupler_to_testIndicator_auto_control_xing_out_a_bits_address),
    .auto_control_xing_out_a_bits_mask(coupler_to_testIndicator_auto_control_xing_out_a_bits_mask),
    .auto_control_xing_out_a_bits_data(coupler_to_testIndicator_auto_control_xing_out_a_bits_data),
    .auto_control_xing_out_a_bits_corrupt(coupler_to_testIndicator_auto_control_xing_out_a_bits_corrupt),
    .auto_control_xing_out_d_ready(coupler_to_testIndicator_auto_control_xing_out_d_ready),
    .auto_control_xing_out_d_valid(coupler_to_testIndicator_auto_control_xing_out_d_valid),
    .auto_control_xing_out_d_bits_opcode(coupler_to_testIndicator_auto_control_xing_out_d_bits_opcode),
    .auto_control_xing_out_d_bits_size(coupler_to_testIndicator_auto_control_xing_out_d_bits_size),
    .auto_control_xing_out_d_bits_source(coupler_to_testIndicator_auto_control_xing_out_d_bits_source),
    .auto_control_xing_out_d_bits_data(coupler_to_testIndicator_auto_control_xing_out_d_bits_data),
    .auto_tl_in_a_ready(coupler_to_testIndicator_auto_tl_in_a_ready),
    .auto_tl_in_a_valid(coupler_to_testIndicator_auto_tl_in_a_valid),
    .auto_tl_in_a_bits_opcode(coupler_to_testIndicator_auto_tl_in_a_bits_opcode),
    .auto_tl_in_a_bits_param(coupler_to_testIndicator_auto_tl_in_a_bits_param),
    .auto_tl_in_a_bits_size(coupler_to_testIndicator_auto_tl_in_a_bits_size),
    .auto_tl_in_a_bits_source(coupler_to_testIndicator_auto_tl_in_a_bits_source),
    .auto_tl_in_a_bits_address(coupler_to_testIndicator_auto_tl_in_a_bits_address),
    .auto_tl_in_a_bits_mask(coupler_to_testIndicator_auto_tl_in_a_bits_mask),
    .auto_tl_in_a_bits_data(coupler_to_testIndicator_auto_tl_in_a_bits_data),
    .auto_tl_in_a_bits_corrupt(coupler_to_testIndicator_auto_tl_in_a_bits_corrupt),
    .auto_tl_in_d_ready(coupler_to_testIndicator_auto_tl_in_d_ready),
    .auto_tl_in_d_valid(coupler_to_testIndicator_auto_tl_in_d_valid),
    .auto_tl_in_d_bits_opcode(coupler_to_testIndicator_auto_tl_in_d_bits_opcode),
    .auto_tl_in_d_bits_size(coupler_to_testIndicator_auto_tl_in_d_bits_size),
    .auto_tl_in_d_bits_source(coupler_to_testIndicator_auto_tl_in_d_bits_source),
    .auto_tl_in_d_bits_data(coupler_to_testIndicator_auto_tl_in_d_bits_data)
  );
  assign subsystem_cbus_clock_groups_auto_out_0_member_subsystem_cbus_0_clock =
    subsystem_cbus_clock_groups_auto_in_member_subsystem_cbus_0_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_cbus_clock_groups_auto_out_0_member_subsystem_cbus_0_reset =
    subsystem_cbus_clock_groups_auto_in_member_subsystem_cbus_0_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign clockGroup_auto_out_clock = clockGroup_auto_in_member_subsystem_cbus_0_clock; // @[ClockGroup.scala 8:11 LazyModule.scala 388:16]
  assign clockGroup_auto_out_reset = clockGroup_auto_in_member_subsystem_cbus_0_reset; // @[ClockGroup.scala 8:11 LazyModule.scala 388:16]
  assign fixer_rf_reset = rf_reset;
  assign in_xbar_auto_in_a_ready = in_xbar_auto_out_a_ready; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign in_xbar_auto_in_d_valid = in_xbar_auto_out_d_valid; // @[ReadyValidCancel.scala 21:38]
  assign in_xbar_auto_in_d_bits_opcode = in_xbar_auto_out_d_bits_opcode; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign in_xbar_auto_in_d_bits_param = in_xbar_auto_out_d_bits_param; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign in_xbar_auto_in_d_bits_size = in_xbar_auto_out_d_bits_size; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign in_xbar_auto_in_d_bits_source = in_xbar_auto_out_d_bits_source; // @[Xbar.scala 228:69]
  assign in_xbar_auto_in_d_bits_sink = in_xbar_auto_out_d_bits_sink; // @[Xbar.scala 323:53]
  assign in_xbar_auto_in_d_bits_denied = in_xbar_auto_out_d_bits_denied; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign in_xbar_auto_in_d_bits_data = in_xbar_auto_out_d_bits_data; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign in_xbar_auto_in_d_bits_corrupt = in_xbar_auto_out_d_bits_corrupt; // @[Nodes.scala 1617:13 LazyModule.scala 390:12]
  assign in_xbar_auto_out_a_valid = in_xbar_auto_in_a_valid; // @[ReadyValidCancel.scala 21:38]
  assign in_xbar_auto_out_a_bits_opcode = in_xbar_auto_in_a_bits_opcode; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign in_xbar_auto_out_a_bits_param = in_xbar_auto_in_a_bits_param; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign in_xbar_auto_out_a_bits_size = in_xbar_auto_in_a_bits_size; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign in_xbar_auto_out_a_bits_source = in_xbar_auto_in_a_bits_source; // @[Xbar.scala 237:55]
  assign in_xbar_auto_out_a_bits_address = in_xbar_auto_in_a_bits_address; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign in_xbar_auto_out_a_bits_user_amba_prot_bufferable = in_xbar_auto_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign in_xbar_auto_out_a_bits_user_amba_prot_modifiable = in_xbar_auto_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign in_xbar_auto_out_a_bits_user_amba_prot_readalloc = in_xbar_auto_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign in_xbar_auto_out_a_bits_user_amba_prot_writealloc = in_xbar_auto_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign in_xbar_auto_out_a_bits_user_amba_prot_privileged = in_xbar_auto_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign in_xbar_auto_out_a_bits_user_amba_prot_secure = in_xbar_auto_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign in_xbar_auto_out_a_bits_user_amba_prot_fetch = in_xbar_auto_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign in_xbar_auto_out_a_bits_mask = in_xbar_auto_in_a_bits_mask; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign in_xbar_auto_out_a_bits_data = in_xbar_auto_in_a_bits_data; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign in_xbar_auto_out_a_bits_corrupt = in_xbar_auto_in_a_bits_corrupt; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign in_xbar_auto_out_d_ready = in_xbar_auto_in_d_ready; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign out_xbar_rf_reset = rf_reset;
  assign buffer_rf_reset = rf_reset;
  assign atomics_rf_reset = rf_reset;
  assign wrapped_error_device_rf_reset = rf_reset;
  assign coupler_to_l2_ctrl_rf_reset = rf_reset;
  assign buffer_1_auto_in_a_ready = buffer_1_auto_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_1_auto_in_d_valid = buffer_1_auto_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_1_auto_in_d_bits_opcode = buffer_1_auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_1_auto_in_d_bits_param = buffer_1_auto_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_1_auto_in_d_bits_size = buffer_1_auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_1_auto_in_d_bits_source = buffer_1_auto_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_1_auto_in_d_bits_sink = buffer_1_auto_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_1_auto_in_d_bits_denied = buffer_1_auto_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_1_auto_in_d_bits_data = buffer_1_auto_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_1_auto_in_d_bits_corrupt = buffer_1_auto_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_1_auto_out_a_valid = buffer_1_auto_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_bits_opcode = buffer_1_auto_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_bits_param = buffer_1_auto_in_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_bits_size = buffer_1_auto_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_bits_source = buffer_1_auto_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_bits_address = buffer_1_auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_bits_user_amba_prot_bufferable = buffer_1_auto_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_bits_user_amba_prot_modifiable = buffer_1_auto_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_bits_user_amba_prot_readalloc = buffer_1_auto_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_bits_user_amba_prot_writealloc = buffer_1_auto_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_bits_user_amba_prot_privileged = buffer_1_auto_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_bits_user_amba_prot_secure = buffer_1_auto_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_bits_user_amba_prot_fetch = buffer_1_auto_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_bits_mask = buffer_1_auto_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_bits_data = buffer_1_auto_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_bits_corrupt = buffer_1_auto_in_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_d_ready = buffer_1_auto_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_plic_rf_reset = rf_reset;
  assign coupler_to_clint_rf_reset = rf_reset;
  assign coupler_to_debug_rf_reset = rf_reset;
  assign coupler_to_port_named_axi4_periph_port_rf_reset = rf_reset;
  assign coupler_to_traceencoder_rf_reset = rf_reset;
  assign coupler_to_traceencoder_1_rf_reset = rf_reset;
  assign coupler_to_tracefunnel_rf_reset = rf_reset;
  assign coupler_to_testIndicator_rf_reset = rf_reset;
  assign auto_coupler_to_testIndicator_control_xing_out_a_valid = coupler_to_testIndicator_auto_control_xing_out_a_valid
    ; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_testIndicator_control_xing_out_a_bits_opcode =
    coupler_to_testIndicator_auto_control_xing_out_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_testIndicator_control_xing_out_a_bits_param =
    coupler_to_testIndicator_auto_control_xing_out_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_testIndicator_control_xing_out_a_bits_size =
    coupler_to_testIndicator_auto_control_xing_out_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_testIndicator_control_xing_out_a_bits_source =
    coupler_to_testIndicator_auto_control_xing_out_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_testIndicator_control_xing_out_a_bits_address =
    coupler_to_testIndicator_auto_control_xing_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_testIndicator_control_xing_out_a_bits_mask =
    coupler_to_testIndicator_auto_control_xing_out_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_testIndicator_control_xing_out_a_bits_data =
    coupler_to_testIndicator_auto_control_xing_out_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_testIndicator_control_xing_out_a_bits_corrupt =
    coupler_to_testIndicator_auto_control_xing_out_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_testIndicator_control_xing_out_d_ready = coupler_to_testIndicator_auto_control_xing_out_d_ready
    ; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_tracefunnel_fragmenter_out_a_valid = coupler_to_tracefunnel_auto_fragmenter_out_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_tracefunnel_fragmenter_out_a_bits_opcode =
    coupler_to_tracefunnel_auto_fragmenter_out_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_tracefunnel_fragmenter_out_a_bits_param =
    coupler_to_tracefunnel_auto_fragmenter_out_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_tracefunnel_fragmenter_out_a_bits_size = coupler_to_tracefunnel_auto_fragmenter_out_a_bits_size
    ; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_tracefunnel_fragmenter_out_a_bits_source =
    coupler_to_tracefunnel_auto_fragmenter_out_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_tracefunnel_fragmenter_out_a_bits_address =
    coupler_to_tracefunnel_auto_fragmenter_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_tracefunnel_fragmenter_out_a_bits_mask = coupler_to_tracefunnel_auto_fragmenter_out_a_bits_mask
    ; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_tracefunnel_fragmenter_out_a_bits_data = coupler_to_tracefunnel_auto_fragmenter_out_a_bits_data
    ; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_tracefunnel_fragmenter_out_a_bits_corrupt =
    coupler_to_tracefunnel_auto_fragmenter_out_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_tracefunnel_fragmenter_out_d_ready = coupler_to_tracefunnel_auto_fragmenter_out_d_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_1_a_valid = coupler_to_traceencoder_1_auto_fragmenter_out_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_opcode =
    coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_param =
    coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_size =
    coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_source =
    coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_address =
    coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_mask =
    coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_data =
    coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_1_a_bits_corrupt =
    coupler_to_traceencoder_1_auto_fragmenter_out_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_1_d_ready = coupler_to_traceencoder_1_auto_fragmenter_out_d_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_0_a_valid = coupler_to_traceencoder_auto_fragmenter_out_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_opcode =
    coupler_to_traceencoder_auto_fragmenter_out_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_param =
    coupler_to_traceencoder_auto_fragmenter_out_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_size =
    coupler_to_traceencoder_auto_fragmenter_out_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_source =
    coupler_to_traceencoder_auto_fragmenter_out_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_address =
    coupler_to_traceencoder_auto_fragmenter_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_mask =
    coupler_to_traceencoder_auto_fragmenter_out_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_data =
    coupler_to_traceencoder_auto_fragmenter_out_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_0_a_bits_corrupt =
    coupler_to_traceencoder_auto_fragmenter_out_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_traceencoder_fragmenter_out_0_d_ready = coupler_to_traceencoder_auto_fragmenter_out_d_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_aw_valid =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_aw_bits_addr =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_addr; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_aw_bits_len =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_len; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_aw_bits_size =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_aw_bits_cache =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_cache; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_aw_bits_prot =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_bits_prot; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_w_valid =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_w_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_w_bits_data =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_w_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_w_bits_strb =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_w_bits_strb; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_w_bits_last =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_w_bits_last; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_b_ready =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_b_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_ar_valid =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_ar_bits_addr =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_addr; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_ar_bits_len =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_len; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_ar_bits_size =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_ar_bits_cache =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_cache; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_ar_bits_prot =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_bits_prot; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_r_ready =
    coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_r_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_debug_fragmenter_out_a_valid = coupler_to_debug_auto_fragmenter_out_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_debug_fragmenter_out_a_bits_opcode = coupler_to_debug_auto_fragmenter_out_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_debug_fragmenter_out_a_bits_param = coupler_to_debug_auto_fragmenter_out_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_debug_fragmenter_out_a_bits_size = coupler_to_debug_auto_fragmenter_out_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_debug_fragmenter_out_a_bits_source = coupler_to_debug_auto_fragmenter_out_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_debug_fragmenter_out_a_bits_address = coupler_to_debug_auto_fragmenter_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_debug_fragmenter_out_a_bits_mask = coupler_to_debug_auto_fragmenter_out_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_debug_fragmenter_out_a_bits_data = coupler_to_debug_auto_fragmenter_out_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_debug_fragmenter_out_a_bits_corrupt = coupler_to_debug_auto_fragmenter_out_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_debug_fragmenter_out_d_ready = coupler_to_debug_auto_fragmenter_out_d_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_clint_fragmenter_out_a_valid = coupler_to_clint_auto_fragmenter_out_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_clint_fragmenter_out_a_bits_opcode = coupler_to_clint_auto_fragmenter_out_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_clint_fragmenter_out_a_bits_param = coupler_to_clint_auto_fragmenter_out_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_clint_fragmenter_out_a_bits_size = coupler_to_clint_auto_fragmenter_out_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_clint_fragmenter_out_a_bits_source = coupler_to_clint_auto_fragmenter_out_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_clint_fragmenter_out_a_bits_address = coupler_to_clint_auto_fragmenter_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_clint_fragmenter_out_a_bits_mask = coupler_to_clint_auto_fragmenter_out_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_clint_fragmenter_out_a_bits_data = coupler_to_clint_auto_fragmenter_out_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_clint_fragmenter_out_a_bits_corrupt = coupler_to_clint_auto_fragmenter_out_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_clint_fragmenter_out_d_ready = coupler_to_clint_auto_fragmenter_out_d_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_plic_fragmenter_out_a_valid = coupler_to_plic_auto_fragmenter_out_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_plic_fragmenter_out_a_bits_opcode = coupler_to_plic_auto_fragmenter_out_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_plic_fragmenter_out_a_bits_param = coupler_to_plic_auto_fragmenter_out_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_plic_fragmenter_out_a_bits_size = coupler_to_plic_auto_fragmenter_out_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_plic_fragmenter_out_a_bits_source = coupler_to_plic_auto_fragmenter_out_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_plic_fragmenter_out_a_bits_address = coupler_to_plic_auto_fragmenter_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_plic_fragmenter_out_a_bits_mask = coupler_to_plic_auto_fragmenter_out_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_plic_fragmenter_out_a_bits_data = coupler_to_plic_auto_fragmenter_out_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_plic_fragmenter_out_a_bits_corrupt = coupler_to_plic_auto_fragmenter_out_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_plic_fragmenter_out_d_ready = coupler_to_plic_auto_fragmenter_out_d_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_ctrl_buffer_out_a_valid = coupler_to_l2_ctrl_auto_buffer_out_a_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_ctrl_buffer_out_a_bits_opcode = coupler_to_l2_ctrl_auto_buffer_out_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_ctrl_buffer_out_a_bits_param = coupler_to_l2_ctrl_auto_buffer_out_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_ctrl_buffer_out_a_bits_size = coupler_to_l2_ctrl_auto_buffer_out_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_ctrl_buffer_out_a_bits_source = coupler_to_l2_ctrl_auto_buffer_out_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_ctrl_buffer_out_a_bits_address = coupler_to_l2_ctrl_auto_buffer_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_ctrl_buffer_out_a_bits_mask = coupler_to_l2_ctrl_auto_buffer_out_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_ctrl_buffer_out_a_bits_data = coupler_to_l2_ctrl_auto_buffer_out_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_ctrl_buffer_out_a_bits_corrupt = coupler_to_l2_ctrl_auto_buffer_out_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_l2_ctrl_buffer_out_d_ready = coupler_to_l2_ctrl_auto_buffer_out_d_ready; // @[LazyModule.scala 390:12]
  assign auto_fixedClockNode_out_1_clock = fixedClockNode_auto_out_2_clock; // @[LazyModule.scala 390:12]
  assign auto_fixedClockNode_out_1_reset = fixedClockNode_auto_out_2_reset; // @[LazyModule.scala 390:12]
  assign auto_fixedClockNode_out_0_clock = fixedClockNode_auto_out_1_clock; // @[LazyModule.scala 390:12]
  assign auto_fixedClockNode_out_0_reset = fixedClockNode_auto_out_1_reset; // @[LazyModule.scala 390:12]
  assign auto_bus_xing_in_a_ready = buffer_1_auto_in_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_valid = buffer_1_auto_in_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_opcode = buffer_1_auto_in_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_param = buffer_1_auto_in_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_size = buffer_1_auto_in_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_source = buffer_1_auto_in_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_sink = buffer_1_auto_in_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_denied = buffer_1_auto_in_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_data = buffer_1_auto_in_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_d_bits_corrupt = buffer_1_auto_in_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign subsystem_cbus_clock_groups_auto_in_member_subsystem_cbus_0_clock =
    auto_subsystem_cbus_clock_groups_in_member_subsystem_cbus_0_clock; // @[LazyModule.scala 388:16]
  assign subsystem_cbus_clock_groups_auto_in_member_subsystem_cbus_0_reset =
    auto_subsystem_cbus_clock_groups_in_member_subsystem_cbus_0_reset; // @[LazyModule.scala 388:16]
  assign clockGroup_auto_in_member_subsystem_cbus_0_clock =
    subsystem_cbus_clock_groups_auto_out_0_member_subsystem_cbus_0_clock; // @[LazyModule.scala 377:16]
  assign clockGroup_auto_in_member_subsystem_cbus_0_reset =
    subsystem_cbus_clock_groups_auto_out_0_member_subsystem_cbus_0_reset; // @[LazyModule.scala 377:16]
  assign fixedClockNode_auto_in_clock = clockGroup_auto_out_clock; // @[LazyModule.scala 377:16]
  assign fixedClockNode_auto_in_reset = clockGroup_auto_out_reset; // @[LazyModule.scala 377:16]
  assign fixer_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign fixer_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign fixer_auto_in_a_valid = buffer_auto_out_a_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_opcode = buffer_auto_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_param = buffer_auto_out_a_bits_param; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_size = buffer_auto_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_source = buffer_auto_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_address = buffer_auto_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_user_amba_prot_bufferable = buffer_auto_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_user_amba_prot_modifiable = buffer_auto_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_user_amba_prot_readalloc = buffer_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_user_amba_prot_writealloc = buffer_auto_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_user_amba_prot_privileged = buffer_auto_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_user_amba_prot_secure = buffer_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_user_amba_prot_fetch = buffer_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_mask = buffer_auto_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_data = buffer_auto_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_corrupt = buffer_auto_out_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_d_ready = buffer_auto_out_d_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_a_ready = out_xbar_auto_in_a_ready; // @[LazyModule.scala 377:16]
  assign fixer_auto_out_d_valid = out_xbar_auto_in_d_valid; // @[LazyModule.scala 377:16]
  assign fixer_auto_out_d_bits_opcode = out_xbar_auto_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign fixer_auto_out_d_bits_param = out_xbar_auto_in_d_bits_param; // @[LazyModule.scala 377:16]
  assign fixer_auto_out_d_bits_size = out_xbar_auto_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign fixer_auto_out_d_bits_source = out_xbar_auto_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign fixer_auto_out_d_bits_sink = out_xbar_auto_in_d_bits_sink; // @[LazyModule.scala 377:16]
  assign fixer_auto_out_d_bits_denied = out_xbar_auto_in_d_bits_denied; // @[LazyModule.scala 377:16]
  assign fixer_auto_out_d_bits_data = out_xbar_auto_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign fixer_auto_out_d_bits_corrupt = out_xbar_auto_in_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign in_xbar_auto_in_a_valid = buffer_1_auto_out_a_valid; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_a_bits_opcode = buffer_1_auto_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_a_bits_param = buffer_1_auto_out_a_bits_param; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_a_bits_size = buffer_1_auto_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_a_bits_source = buffer_1_auto_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_a_bits_address = buffer_1_auto_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_a_bits_user_amba_prot_bufferable = buffer_1_auto_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_a_bits_user_amba_prot_modifiable = buffer_1_auto_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_a_bits_user_amba_prot_readalloc = buffer_1_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_a_bits_user_amba_prot_writealloc = buffer_1_auto_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_a_bits_user_amba_prot_privileged = buffer_1_auto_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_a_bits_user_amba_prot_secure = buffer_1_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_a_bits_user_amba_prot_fetch = buffer_1_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_a_bits_mask = buffer_1_auto_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_a_bits_data = buffer_1_auto_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_a_bits_corrupt = buffer_1_auto_out_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_in_d_ready = buffer_1_auto_out_d_ready; // @[LazyModule.scala 375:16]
  assign in_xbar_auto_out_a_ready = atomics_auto_in_a_ready; // @[LazyModule.scala 377:16]
  assign in_xbar_auto_out_d_valid = atomics_auto_in_d_valid; // @[LazyModule.scala 377:16]
  assign in_xbar_auto_out_d_bits_opcode = atomics_auto_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign in_xbar_auto_out_d_bits_param = atomics_auto_in_d_bits_param; // @[LazyModule.scala 377:16]
  assign in_xbar_auto_out_d_bits_size = atomics_auto_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign in_xbar_auto_out_d_bits_source = atomics_auto_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign in_xbar_auto_out_d_bits_sink = atomics_auto_in_d_bits_sink; // @[LazyModule.scala 377:16]
  assign in_xbar_auto_out_d_bits_denied = atomics_auto_in_d_bits_denied; // @[LazyModule.scala 377:16]
  assign in_xbar_auto_out_d_bits_data = atomics_auto_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign in_xbar_auto_out_d_bits_corrupt = atomics_auto_in_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign out_xbar_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign out_xbar_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign out_xbar_auto_in_a_valid = fixer_auto_out_a_valid; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_a_bits_opcode = fixer_auto_out_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_a_bits_param = fixer_auto_out_a_bits_param; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_a_bits_size = fixer_auto_out_a_bits_size; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_a_bits_source = fixer_auto_out_a_bits_source; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_a_bits_address = fixer_auto_out_a_bits_address; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_a_bits_user_amba_prot_bufferable = fixer_auto_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_a_bits_user_amba_prot_modifiable = fixer_auto_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_a_bits_user_amba_prot_readalloc = fixer_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_a_bits_user_amba_prot_writealloc = fixer_auto_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_a_bits_user_amba_prot_privileged = fixer_auto_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_a_bits_user_amba_prot_secure = fixer_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_a_bits_user_amba_prot_fetch = fixer_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_a_bits_mask = fixer_auto_out_a_bits_mask; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_a_bits_data = fixer_auto_out_a_bits_data; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_a_bits_corrupt = fixer_auto_out_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_in_d_ready = fixer_auto_out_d_ready; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_9_a_ready = coupler_to_testIndicator_auto_tl_in_a_ready; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_9_d_valid = coupler_to_testIndicator_auto_tl_in_d_valid; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_9_d_bits_opcode = coupler_to_testIndicator_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_9_d_bits_size = coupler_to_testIndicator_auto_tl_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_9_d_bits_source = coupler_to_testIndicator_auto_tl_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_9_d_bits_data = coupler_to_testIndicator_auto_tl_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_8_a_ready = coupler_to_tracefunnel_auto_tl_in_a_ready; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_8_d_valid = coupler_to_tracefunnel_auto_tl_in_d_valid; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_8_d_bits_opcode = coupler_to_tracefunnel_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_8_d_bits_size = coupler_to_tracefunnel_auto_tl_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_8_d_bits_source = coupler_to_tracefunnel_auto_tl_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_8_d_bits_data = coupler_to_tracefunnel_auto_tl_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_7_a_ready = coupler_to_traceencoder_1_auto_tl_in_a_ready; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_7_d_valid = coupler_to_traceencoder_1_auto_tl_in_d_valid; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_7_d_bits_opcode = coupler_to_traceencoder_1_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_7_d_bits_size = coupler_to_traceencoder_1_auto_tl_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_7_d_bits_source = coupler_to_traceencoder_1_auto_tl_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_7_d_bits_data = coupler_to_traceencoder_1_auto_tl_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_6_a_ready = coupler_to_traceencoder_auto_tl_in_a_ready; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_6_d_valid = coupler_to_traceencoder_auto_tl_in_d_valid; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_6_d_bits_opcode = coupler_to_traceencoder_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_6_d_bits_size = coupler_to_traceencoder_auto_tl_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_6_d_bits_source = coupler_to_traceencoder_auto_tl_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_6_d_bits_data = coupler_to_traceencoder_auto_tl_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_5_a_ready = coupler_to_port_named_axi4_periph_port_auto_tl_in_a_ready; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_5_d_valid = coupler_to_port_named_axi4_periph_port_auto_tl_in_d_valid; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_5_d_bits_opcode = coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_5_d_bits_size = coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_5_d_bits_source = coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_5_d_bits_denied = coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_denied; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_5_d_bits_data = coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_5_d_bits_corrupt = coupler_to_port_named_axi4_periph_port_auto_tl_in_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_4_a_ready = coupler_to_debug_auto_tl_in_a_ready; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_4_d_valid = coupler_to_debug_auto_tl_in_d_valid; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_4_d_bits_opcode = coupler_to_debug_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_4_d_bits_size = coupler_to_debug_auto_tl_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_4_d_bits_source = coupler_to_debug_auto_tl_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_4_d_bits_data = coupler_to_debug_auto_tl_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_3_a_ready = coupler_to_clint_auto_tl_in_a_ready; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_3_d_valid = coupler_to_clint_auto_tl_in_d_valid; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_3_d_bits_opcode = coupler_to_clint_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_3_d_bits_size = coupler_to_clint_auto_tl_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_3_d_bits_source = coupler_to_clint_auto_tl_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_3_d_bits_data = coupler_to_clint_auto_tl_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_2_a_ready = coupler_to_plic_auto_tl_in_a_ready; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_2_d_valid = coupler_to_plic_auto_tl_in_d_valid; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_2_d_bits_opcode = coupler_to_plic_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_2_d_bits_size = coupler_to_plic_auto_tl_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_2_d_bits_source = coupler_to_plic_auto_tl_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_2_d_bits_data = coupler_to_plic_auto_tl_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_1_a_ready = coupler_to_l2_ctrl_auto_tl_in_a_ready; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_1_d_valid = coupler_to_l2_ctrl_auto_tl_in_d_valid; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_1_d_bits_opcode = coupler_to_l2_ctrl_auto_tl_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_1_d_bits_size = coupler_to_l2_ctrl_auto_tl_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_1_d_bits_source = coupler_to_l2_ctrl_auto_tl_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_1_d_bits_data = coupler_to_l2_ctrl_auto_tl_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_0_a_ready = wrapped_error_device_auto_buffer_in_a_ready; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_0_d_valid = wrapped_error_device_auto_buffer_in_d_valid; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_0_d_bits_opcode = wrapped_error_device_auto_buffer_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_0_d_bits_param = wrapped_error_device_auto_buffer_in_d_bits_param; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_0_d_bits_size = wrapped_error_device_auto_buffer_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_0_d_bits_source = wrapped_error_device_auto_buffer_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_0_d_bits_sink = wrapped_error_device_auto_buffer_in_d_bits_sink; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_0_d_bits_denied = wrapped_error_device_auto_buffer_in_d_bits_denied; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_0_d_bits_data = wrapped_error_device_auto_buffer_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign out_xbar_auto_out_0_d_bits_corrupt = wrapped_error_device_auto_buffer_in_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign buffer_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign buffer_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign buffer_auto_in_a_valid = atomics_auto_out_a_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_opcode = atomics_auto_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_param = atomics_auto_out_a_bits_param; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_size = atomics_auto_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_source = atomics_auto_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_address = atomics_auto_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_bufferable = atomics_auto_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_modifiable = atomics_auto_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_readalloc = atomics_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_writealloc = atomics_auto_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_privileged = atomics_auto_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_secure = atomics_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_fetch = atomics_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_mask = atomics_auto_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_data = atomics_auto_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_corrupt = atomics_auto_out_a_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_d_ready = atomics_auto_out_d_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_a_ready = fixer_auto_in_a_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_valid = fixer_auto_in_d_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_opcode = fixer_auto_in_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_param = fixer_auto_in_d_bits_param; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_size = fixer_auto_in_d_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_source = fixer_auto_in_d_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_sink = fixer_auto_in_d_bits_sink; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_denied = fixer_auto_in_d_bits_denied; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_data = fixer_auto_in_d_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_corrupt = fixer_auto_in_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign atomics_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign atomics_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign atomics_auto_in_a_valid = in_xbar_auto_out_a_valid; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_a_bits_opcode = in_xbar_auto_out_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_a_bits_param = in_xbar_auto_out_a_bits_param; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_a_bits_size = in_xbar_auto_out_a_bits_size; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_a_bits_source = in_xbar_auto_out_a_bits_source; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_a_bits_address = in_xbar_auto_out_a_bits_address; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_a_bits_user_amba_prot_bufferable = in_xbar_auto_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_a_bits_user_amba_prot_modifiable = in_xbar_auto_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_a_bits_user_amba_prot_readalloc = in_xbar_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_a_bits_user_amba_prot_writealloc = in_xbar_auto_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_a_bits_user_amba_prot_privileged = in_xbar_auto_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_a_bits_user_amba_prot_secure = in_xbar_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_a_bits_user_amba_prot_fetch = in_xbar_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_a_bits_mask = in_xbar_auto_out_a_bits_mask; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_a_bits_data = in_xbar_auto_out_a_bits_data; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_a_bits_corrupt = in_xbar_auto_out_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign atomics_auto_in_d_ready = in_xbar_auto_out_d_ready; // @[LazyModule.scala 377:16]
  assign atomics_auto_out_a_ready = buffer_auto_in_a_ready; // @[LazyModule.scala 375:16]
  assign atomics_auto_out_d_valid = buffer_auto_in_d_valid; // @[LazyModule.scala 375:16]
  assign atomics_auto_out_d_bits_opcode = buffer_auto_in_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign atomics_auto_out_d_bits_param = buffer_auto_in_d_bits_param; // @[LazyModule.scala 375:16]
  assign atomics_auto_out_d_bits_size = buffer_auto_in_d_bits_size; // @[LazyModule.scala 375:16]
  assign atomics_auto_out_d_bits_source = buffer_auto_in_d_bits_source; // @[LazyModule.scala 375:16]
  assign atomics_auto_out_d_bits_sink = buffer_auto_in_d_bits_sink; // @[LazyModule.scala 375:16]
  assign atomics_auto_out_d_bits_denied = buffer_auto_in_d_bits_denied; // @[LazyModule.scala 375:16]
  assign atomics_auto_out_d_bits_data = buffer_auto_in_d_bits_data; // @[LazyModule.scala 375:16]
  assign atomics_auto_out_d_bits_corrupt = buffer_auto_in_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign wrapped_error_device_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign wrapped_error_device_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign wrapped_error_device_auto_buffer_in_a_valid = out_xbar_auto_out_0_a_valid; // @[LazyModule.scala 377:16]
  assign wrapped_error_device_auto_buffer_in_a_bits_opcode = out_xbar_auto_out_0_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign wrapped_error_device_auto_buffer_in_a_bits_param = out_xbar_auto_out_0_a_bits_param; // @[LazyModule.scala 377:16]
  assign wrapped_error_device_auto_buffer_in_a_bits_size = out_xbar_auto_out_0_a_bits_size; // @[LazyModule.scala 377:16]
  assign wrapped_error_device_auto_buffer_in_a_bits_source = out_xbar_auto_out_0_a_bits_source; // @[LazyModule.scala 377:16]
  assign wrapped_error_device_auto_buffer_in_a_bits_address = out_xbar_auto_out_0_a_bits_address; // @[LazyModule.scala 377:16]
  assign wrapped_error_device_auto_buffer_in_a_bits_mask = out_xbar_auto_out_0_a_bits_mask; // @[LazyModule.scala 377:16]
  assign wrapped_error_device_auto_buffer_in_a_bits_corrupt = out_xbar_auto_out_0_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign wrapped_error_device_auto_buffer_in_d_ready = out_xbar_auto_out_0_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_ctrl_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_l2_ctrl_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_l2_ctrl_auto_buffer_out_a_ready = auto_coupler_to_l2_ctrl_buffer_out_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_l2_ctrl_auto_buffer_out_d_valid = auto_coupler_to_l2_ctrl_buffer_out_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_l2_ctrl_auto_buffer_out_d_bits_opcode = auto_coupler_to_l2_ctrl_buffer_out_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_l2_ctrl_auto_buffer_out_d_bits_size = auto_coupler_to_l2_ctrl_buffer_out_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_l2_ctrl_auto_buffer_out_d_bits_source = auto_coupler_to_l2_ctrl_buffer_out_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_l2_ctrl_auto_buffer_out_d_bits_data = auto_coupler_to_l2_ctrl_buffer_out_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_l2_ctrl_auto_tl_in_a_valid = out_xbar_auto_out_1_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_ctrl_auto_tl_in_a_bits_opcode = out_xbar_auto_out_1_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_ctrl_auto_tl_in_a_bits_param = out_xbar_auto_out_1_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_ctrl_auto_tl_in_a_bits_size = out_xbar_auto_out_1_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_ctrl_auto_tl_in_a_bits_source = out_xbar_auto_out_1_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_ctrl_auto_tl_in_a_bits_address = out_xbar_auto_out_1_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_ctrl_auto_tl_in_a_bits_mask = out_xbar_auto_out_1_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_ctrl_auto_tl_in_a_bits_data = out_xbar_auto_out_1_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_ctrl_auto_tl_in_a_bits_corrupt = out_xbar_auto_out_1_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_l2_ctrl_auto_tl_in_d_ready = out_xbar_auto_out_1_d_ready; // @[LazyModule.scala 377:16]
  assign buffer_1_auto_in_a_valid = auto_bus_xing_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_a_bits_opcode = auto_bus_xing_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_a_bits_param = auto_bus_xing_in_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_a_bits_size = auto_bus_xing_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_a_bits_source = auto_bus_xing_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_a_bits_address = auto_bus_xing_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_a_bits_user_amba_prot_bufferable = auto_bus_xing_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_a_bits_user_amba_prot_modifiable = auto_bus_xing_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_a_bits_user_amba_prot_readalloc = auto_bus_xing_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_a_bits_user_amba_prot_writealloc = auto_bus_xing_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_a_bits_user_amba_prot_privileged = auto_bus_xing_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_a_bits_user_amba_prot_secure = auto_bus_xing_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_a_bits_user_amba_prot_fetch = auto_bus_xing_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_a_bits_mask = auto_bus_xing_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_a_bits_data = auto_bus_xing_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_a_bits_corrupt = auto_bus_xing_in_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_in_d_ready = auto_bus_xing_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_1_auto_out_a_ready = in_xbar_auto_in_a_ready; // @[LazyModule.scala 375:16]
  assign buffer_1_auto_out_d_valid = in_xbar_auto_in_d_valid; // @[LazyModule.scala 375:16]
  assign buffer_1_auto_out_d_bits_opcode = in_xbar_auto_in_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_1_auto_out_d_bits_param = in_xbar_auto_in_d_bits_param; // @[LazyModule.scala 375:16]
  assign buffer_1_auto_out_d_bits_size = in_xbar_auto_in_d_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_1_auto_out_d_bits_source = in_xbar_auto_in_d_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_1_auto_out_d_bits_sink = in_xbar_auto_in_d_bits_sink; // @[LazyModule.scala 375:16]
  assign buffer_1_auto_out_d_bits_denied = in_xbar_auto_in_d_bits_denied; // @[LazyModule.scala 375:16]
  assign buffer_1_auto_out_d_bits_data = in_xbar_auto_in_d_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_1_auto_out_d_bits_corrupt = in_xbar_auto_in_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coupler_to_plic_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_plic_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_plic_auto_fragmenter_out_a_ready = auto_coupler_to_plic_fragmenter_out_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_plic_auto_fragmenter_out_d_valid = auto_coupler_to_plic_fragmenter_out_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_plic_auto_fragmenter_out_d_bits_opcode = auto_coupler_to_plic_fragmenter_out_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_plic_auto_fragmenter_out_d_bits_size = auto_coupler_to_plic_fragmenter_out_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_plic_auto_fragmenter_out_d_bits_source = auto_coupler_to_plic_fragmenter_out_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_plic_auto_fragmenter_out_d_bits_data = auto_coupler_to_plic_fragmenter_out_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_plic_auto_tl_in_a_valid = out_xbar_auto_out_2_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_plic_auto_tl_in_a_bits_opcode = out_xbar_auto_out_2_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_plic_auto_tl_in_a_bits_param = out_xbar_auto_out_2_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_plic_auto_tl_in_a_bits_size = out_xbar_auto_out_2_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_plic_auto_tl_in_a_bits_source = out_xbar_auto_out_2_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_plic_auto_tl_in_a_bits_address = out_xbar_auto_out_2_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_plic_auto_tl_in_a_bits_mask = out_xbar_auto_out_2_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_plic_auto_tl_in_a_bits_data = out_xbar_auto_out_2_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_plic_auto_tl_in_a_bits_corrupt = out_xbar_auto_out_2_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_plic_auto_tl_in_d_ready = out_xbar_auto_out_2_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_clint_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_clint_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_clint_auto_fragmenter_out_a_ready = auto_coupler_to_clint_fragmenter_out_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_clint_auto_fragmenter_out_d_valid = auto_coupler_to_clint_fragmenter_out_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_clint_auto_fragmenter_out_d_bits_opcode = auto_coupler_to_clint_fragmenter_out_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_clint_auto_fragmenter_out_d_bits_size = auto_coupler_to_clint_fragmenter_out_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_clint_auto_fragmenter_out_d_bits_source = auto_coupler_to_clint_fragmenter_out_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_clint_auto_fragmenter_out_d_bits_data = auto_coupler_to_clint_fragmenter_out_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_clint_auto_tl_in_a_valid = out_xbar_auto_out_3_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_clint_auto_tl_in_a_bits_opcode = out_xbar_auto_out_3_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_clint_auto_tl_in_a_bits_param = out_xbar_auto_out_3_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_clint_auto_tl_in_a_bits_size = out_xbar_auto_out_3_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_clint_auto_tl_in_a_bits_source = out_xbar_auto_out_3_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_clint_auto_tl_in_a_bits_address = out_xbar_auto_out_3_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_clint_auto_tl_in_a_bits_mask = out_xbar_auto_out_3_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_clint_auto_tl_in_a_bits_data = out_xbar_auto_out_3_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_clint_auto_tl_in_a_bits_corrupt = out_xbar_auto_out_3_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_clint_auto_tl_in_d_ready = out_xbar_auto_out_3_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_debug_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_debug_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_debug_auto_fragmenter_out_a_ready = auto_coupler_to_debug_fragmenter_out_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_debug_auto_fragmenter_out_d_valid = auto_coupler_to_debug_fragmenter_out_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_debug_auto_fragmenter_out_d_bits_opcode = auto_coupler_to_debug_fragmenter_out_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_debug_auto_fragmenter_out_d_bits_size = auto_coupler_to_debug_fragmenter_out_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_debug_auto_fragmenter_out_d_bits_source = auto_coupler_to_debug_fragmenter_out_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_debug_auto_fragmenter_out_d_bits_data = auto_coupler_to_debug_fragmenter_out_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_debug_auto_tl_in_a_valid = out_xbar_auto_out_4_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_debug_auto_tl_in_a_bits_opcode = out_xbar_auto_out_4_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_debug_auto_tl_in_a_bits_param = out_xbar_auto_out_4_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_debug_auto_tl_in_a_bits_size = out_xbar_auto_out_4_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_debug_auto_tl_in_a_bits_source = out_xbar_auto_out_4_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_debug_auto_tl_in_a_bits_address = out_xbar_auto_out_4_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_debug_auto_tl_in_a_bits_mask = out_xbar_auto_out_4_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_debug_auto_tl_in_a_bits_data = out_xbar_auto_out_4_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_debug_auto_tl_in_a_bits_corrupt = out_xbar_auto_out_4_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_debug_auto_tl_in_d_ready = out_xbar_auto_out_4_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_port_named_axi4_periph_port_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_aw_ready =
    auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_aw_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_w_ready =
    auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_w_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_b_valid =
    auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_b_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_b_bits_resp =
    auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_b_bits_resp; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_ar_ready =
    auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_ar_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_r_valid =
    auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_r_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_r_bits_data =
    auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_r_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_r_bits_resp =
    auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_r_bits_resp; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_periph_port_auto_bridge_for_axi4_periph_port_axi4_out_r_bits_last =
    auto_coupler_to_port_named_axi4_periph_port_bridge_for_axi4_periph_port_axi4_out_r_bits_last; // @[LazyModule.scala 390:12]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_valid = out_xbar_auto_out_5_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_opcode = out_xbar_auto_out_5_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_param = out_xbar_auto_out_5_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_size = out_xbar_auto_out_5_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_source = out_xbar_auto_out_5_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_address = out_xbar_auto_out_5_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_bufferable =
    out_xbar_auto_out_5_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_modifiable =
    out_xbar_auto_out_5_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_readalloc =
    out_xbar_auto_out_5_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_writealloc =
    out_xbar_auto_out_5_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_privileged =
    out_xbar_auto_out_5_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_secure =
    out_xbar_auto_out_5_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_user_amba_prot_fetch =
    out_xbar_auto_out_5_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_mask = out_xbar_auto_out_5_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_data = out_xbar_auto_out_5_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_a_bits_corrupt = out_xbar_auto_out_5_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_port_named_axi4_periph_port_auto_tl_in_d_ready = out_xbar_auto_out_5_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_traceencoder_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_traceencoder_auto_fragmenter_out_a_ready = auto_coupler_to_traceencoder_fragmenter_out_0_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_traceencoder_auto_fragmenter_out_d_valid = auto_coupler_to_traceencoder_fragmenter_out_0_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_traceencoder_auto_fragmenter_out_d_bits_opcode =
    auto_coupler_to_traceencoder_fragmenter_out_0_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_traceencoder_auto_fragmenter_out_d_bits_size =
    auto_coupler_to_traceencoder_fragmenter_out_0_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_traceencoder_auto_fragmenter_out_d_bits_source =
    auto_coupler_to_traceencoder_fragmenter_out_0_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_traceencoder_auto_fragmenter_out_d_bits_data =
    auto_coupler_to_traceencoder_fragmenter_out_0_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_traceencoder_auto_tl_in_a_valid = out_xbar_auto_out_6_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_auto_tl_in_a_bits_opcode = out_xbar_auto_out_6_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_auto_tl_in_a_bits_param = out_xbar_auto_out_6_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_auto_tl_in_a_bits_size = out_xbar_auto_out_6_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_auto_tl_in_a_bits_source = out_xbar_auto_out_6_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_auto_tl_in_a_bits_address = out_xbar_auto_out_6_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_auto_tl_in_a_bits_mask = out_xbar_auto_out_6_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_auto_tl_in_a_bits_data = out_xbar_auto_out_6_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_auto_tl_in_a_bits_corrupt = out_xbar_auto_out_6_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_auto_tl_in_d_ready = out_xbar_auto_out_6_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_1_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_traceencoder_1_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_traceencoder_1_auto_fragmenter_out_a_ready = auto_coupler_to_traceencoder_fragmenter_out_1_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_traceencoder_1_auto_fragmenter_out_d_valid = auto_coupler_to_traceencoder_fragmenter_out_1_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_traceencoder_1_auto_fragmenter_out_d_bits_opcode =
    auto_coupler_to_traceencoder_fragmenter_out_1_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_traceencoder_1_auto_fragmenter_out_d_bits_size =
    auto_coupler_to_traceencoder_fragmenter_out_1_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_traceencoder_1_auto_fragmenter_out_d_bits_source =
    auto_coupler_to_traceencoder_fragmenter_out_1_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_traceencoder_1_auto_fragmenter_out_d_bits_data =
    auto_coupler_to_traceencoder_fragmenter_out_1_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_traceencoder_1_auto_tl_in_a_valid = out_xbar_auto_out_7_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_1_auto_tl_in_a_bits_opcode = out_xbar_auto_out_7_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_1_auto_tl_in_a_bits_param = out_xbar_auto_out_7_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_1_auto_tl_in_a_bits_size = out_xbar_auto_out_7_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_1_auto_tl_in_a_bits_source = out_xbar_auto_out_7_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_1_auto_tl_in_a_bits_address = out_xbar_auto_out_7_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_1_auto_tl_in_a_bits_mask = out_xbar_auto_out_7_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_1_auto_tl_in_a_bits_data = out_xbar_auto_out_7_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_1_auto_tl_in_a_bits_corrupt = out_xbar_auto_out_7_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_traceencoder_1_auto_tl_in_d_ready = out_xbar_auto_out_7_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_tracefunnel_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_tracefunnel_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_tracefunnel_auto_fragmenter_out_a_ready = auto_coupler_to_tracefunnel_fragmenter_out_a_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_tracefunnel_auto_fragmenter_out_d_valid = auto_coupler_to_tracefunnel_fragmenter_out_d_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_tracefunnel_auto_fragmenter_out_d_bits_opcode =
    auto_coupler_to_tracefunnel_fragmenter_out_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_tracefunnel_auto_fragmenter_out_d_bits_size = auto_coupler_to_tracefunnel_fragmenter_out_d_bits_size
    ; // @[LazyModule.scala 390:12]
  assign coupler_to_tracefunnel_auto_fragmenter_out_d_bits_source =
    auto_coupler_to_tracefunnel_fragmenter_out_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_tracefunnel_auto_fragmenter_out_d_bits_data = auto_coupler_to_tracefunnel_fragmenter_out_d_bits_data
    ; // @[LazyModule.scala 390:12]
  assign coupler_to_tracefunnel_auto_tl_in_a_valid = out_xbar_auto_out_8_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_tracefunnel_auto_tl_in_a_bits_opcode = out_xbar_auto_out_8_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_tracefunnel_auto_tl_in_a_bits_param = out_xbar_auto_out_8_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_tracefunnel_auto_tl_in_a_bits_size = out_xbar_auto_out_8_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_tracefunnel_auto_tl_in_a_bits_source = out_xbar_auto_out_8_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_tracefunnel_auto_tl_in_a_bits_address = out_xbar_auto_out_8_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_tracefunnel_auto_tl_in_a_bits_mask = out_xbar_auto_out_8_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_tracefunnel_auto_tl_in_a_bits_data = out_xbar_auto_out_8_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_tracefunnel_auto_tl_in_a_bits_corrupt = out_xbar_auto_out_8_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_tracefunnel_auto_tl_in_d_ready = out_xbar_auto_out_8_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_testIndicator_clock = fixedClockNode_auto_out_0_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_testIndicator_reset = fixedClockNode_auto_out_0_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_testIndicator_auto_control_xing_out_a_ready = auto_coupler_to_testIndicator_control_xing_out_a_ready
    ; // @[LazyModule.scala 390:12]
  assign coupler_to_testIndicator_auto_control_xing_out_d_valid = auto_coupler_to_testIndicator_control_xing_out_d_valid
    ; // @[LazyModule.scala 390:12]
  assign coupler_to_testIndicator_auto_control_xing_out_d_bits_opcode =
    auto_coupler_to_testIndicator_control_xing_out_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign coupler_to_testIndicator_auto_control_xing_out_d_bits_size =
    auto_coupler_to_testIndicator_control_xing_out_d_bits_size; // @[LazyModule.scala 390:12]
  assign coupler_to_testIndicator_auto_control_xing_out_d_bits_source =
    auto_coupler_to_testIndicator_control_xing_out_d_bits_source; // @[LazyModule.scala 390:12]
  assign coupler_to_testIndicator_auto_control_xing_out_d_bits_data =
    auto_coupler_to_testIndicator_control_xing_out_d_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_testIndicator_auto_tl_in_a_valid = out_xbar_auto_out_9_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_testIndicator_auto_tl_in_a_bits_opcode = out_xbar_auto_out_9_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign coupler_to_testIndicator_auto_tl_in_a_bits_param = out_xbar_auto_out_9_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_testIndicator_auto_tl_in_a_bits_size = out_xbar_auto_out_9_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_testIndicator_auto_tl_in_a_bits_source = out_xbar_auto_out_9_a_bits_source; // @[LazyModule.scala 377:16]
  assign coupler_to_testIndicator_auto_tl_in_a_bits_address = out_xbar_auto_out_9_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_testIndicator_auto_tl_in_a_bits_mask = out_xbar_auto_out_9_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_testIndicator_auto_tl_in_a_bits_data = out_xbar_auto_out_9_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_testIndicator_auto_tl_in_a_bits_corrupt = out_xbar_auto_out_9_a_bits_corrupt; // @[LazyModule.scala 377:16]
  assign coupler_to_testIndicator_auto_tl_in_d_ready = out_xbar_auto_out_9_d_ready; // @[LazyModule.scala 377:16]
endmodule
