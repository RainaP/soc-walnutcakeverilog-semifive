//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__OptimizationBarrier(
  input  [24:0] io_x_ppn,
  input         io_x_u,
  input         io_x_ae,
  input         io_x_sw,
  input         io_x_sx,
  input         io_x_sr,
  input         io_x_pw,
  input         io_x_px,
  input         io_x_pr,
  input         io_x_ppp,
  input         io_x_pal,
  input         io_x_paa,
  input         io_x_eff,
  input         io_x_c,
  output [24:0] io_y_ppn,
  output        io_y_u,
  output        io_y_ae,
  output        io_y_sw,
  output        io_y_sx,
  output        io_y_sr,
  output        io_y_pw,
  output        io_y_px,
  output        io_y_pr,
  output        io_y_ppp,
  output        io_y_pal,
  output        io_y_paa,
  output        io_y_eff,
  output        io_y_c
);
  assign io_y_ppn = io_x_ppn; // @[package.scala 276:12]
  assign io_y_u = io_x_u; // @[package.scala 276:12]
  assign io_y_ae = io_x_ae; // @[package.scala 276:12]
  assign io_y_sw = io_x_sw; // @[package.scala 276:12]
  assign io_y_sx = io_x_sx; // @[package.scala 276:12]
  assign io_y_sr = io_x_sr; // @[package.scala 276:12]
  assign io_y_pw = io_x_pw; // @[package.scala 276:12]
  assign io_y_px = io_x_px; // @[package.scala 276:12]
  assign io_y_pr = io_x_pr; // @[package.scala 276:12]
  assign io_y_ppp = io_x_ppp; // @[package.scala 276:12]
  assign io_y_pal = io_x_pal; // @[package.scala 276:12]
  assign io_y_paa = io_x_paa; // @[package.scala 276:12]
  assign io_y_eff = io_x_eff; // @[package.scala 276:12]
  assign io_y_c = io_x_c; // @[package.scala 276:12]
endmodule
