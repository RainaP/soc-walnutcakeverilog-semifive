//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__CSRFile(
  input         rf_reset,
  input         clock,
  input         reset,
  input         io_ungated_clock,
  input         io_interrupts_debug,
  input         io_interrupts_mtip,
  input         io_interrupts_msip,
  input         io_interrupts_meip,
  input         io_interrupts_seip,
  input         io_interrupts_lip_0,
  input         io_interrupts_lip_1,
  input         io_interrupts_lip_2,
  input         io_interrupts_lip_3,
  input         io_interrupts_lip_4,
  input         io_interrupts_lip_5,
  input         io_interrupts_lip_6,
  input         io_interrupts_lip_7,
  input         io_interrupts_lip_8,
  input         io_interrupts_lip_9,
  input         io_interrupts_lip_10,
  input         io_interrupts_lip_11,
  input         io_interrupts_lip_12,
  input         io_interrupts_lip_13,
  input         io_interrupts_lip_14,
  input         io_interrupts_lip_15,
  input         io_interrupts_nmi_rnmi,
  input  [36:0] io_interrupts_nmi_rnmi_interrupt_vector,
  input  [36:0] io_interrupts_nmi_rnmi_exception_vector,
  input  [1:0]  io_hartid,
  input  [11:0] io_rw_addr,
  input  [2:0]  io_rw_cmd,
  output [63:0] io_rw_rdata,
  input  [63:0] io_rw_wdata,
  input  [11:0] io_decode_0_csr,
  output        io_decode_0_fp_illegal,
  output        io_decode_0_fp_csr,
  output        io_decode_0_read_illegal,
  output        io_decode_0_write_illegal,
  output        io_decode_0_write_flush,
  output        io_decode_0_system_illegal,
  output        io_csr_stall,
  output        io_eret,
  output        io_singleStep,
  output        io_status_debug,
  output        io_status_cease,
  output        io_status_wfi,
  output [31:0] io_status_isa,
  output [1:0]  io_status_dprv,
  output [1:0]  io_status_prv,
  output        io_status_sd,
  output [26:0] io_status_zero2,
  output [1:0]  io_status_sxl,
  output [1:0]  io_status_uxl,
  output        io_status_sd_rv32,
  output [7:0]  io_status_zero1,
  output        io_status_tsr,
  output        io_status_tw,
  output        io_status_tvm,
  output        io_status_mxr,
  output        io_status_sum,
  output        io_status_mprv,
  output [1:0]  io_status_xs,
  output [1:0]  io_status_fs,
  output [1:0]  io_status_mpp,
  output [1:0]  io_status_vs,
  output        io_status_spp,
  output        io_status_mpie,
  output        io_status_hpie,
  output        io_status_spie,
  output        io_status_upie,
  output        io_status_mie,
  output        io_status_hie,
  output        io_status_sie,
  output        io_status_uie,
  output [3:0]  io_ptbr_mode,
  output [43:0] io_ptbr_ppn,
  output [39:0] io_evec,
  input         io_exception,
  input         io_retire,
  input  [63:0] io_cause,
  input  [39:0] io_pc,
  input  [39:0] io_tval,
  output [63:0] io_time,
  output [2:0]  io_fcsr_rm,
  input         io_fcsr_flags_valid,
  input  [4:0]  io_fcsr_flags_bits,
  output        io_interrupt,
  output [63:0] io_interrupt_cause,
  output [2:0]  io_bp_0_control_action,
  output        io_bp_0_control_chain,
  output [1:0]  io_bp_0_control_tmatch,
  output        io_bp_0_control_m,
  output        io_bp_0_control_s,
  output        io_bp_0_control_u,
  output        io_bp_0_control_x,
  output        io_bp_0_control_w,
  output        io_bp_0_control_r,
  output [38:0] io_bp_0_address,
  output [2:0]  io_bp_1_control_action,
  output        io_bp_1_control_chain,
  output [1:0]  io_bp_1_control_tmatch,
  output        io_bp_1_control_m,
  output        io_bp_1_control_s,
  output        io_bp_1_control_u,
  output        io_bp_1_control_x,
  output        io_bp_1_control_w,
  output        io_bp_1_control_r,
  output [38:0] io_bp_1_address,
  output [2:0]  io_bp_2_control_action,
  output        io_bp_2_control_chain,
  output [1:0]  io_bp_2_control_tmatch,
  output        io_bp_2_control_m,
  output        io_bp_2_control_s,
  output        io_bp_2_control_u,
  output        io_bp_2_control_x,
  output        io_bp_2_control_w,
  output        io_bp_2_control_r,
  output [38:0] io_bp_2_address,
  output [2:0]  io_bp_3_control_action,
  output        io_bp_3_control_chain,
  output [1:0]  io_bp_3_control_tmatch,
  output        io_bp_3_control_m,
  output        io_bp_3_control_s,
  output        io_bp_3_control_u,
  output        io_bp_3_control_x,
  output        io_bp_3_control_w,
  output        io_bp_3_control_r,
  output [38:0] io_bp_3_address,
  output [2:0]  io_bp_4_control_action,
  output        io_bp_4_control_chain,
  output [1:0]  io_bp_4_control_tmatch,
  output        io_bp_4_control_m,
  output        io_bp_4_control_s,
  output        io_bp_4_control_u,
  output        io_bp_4_control_x,
  output        io_bp_4_control_w,
  output        io_bp_4_control_r,
  output [38:0] io_bp_4_address,
  output [2:0]  io_bp_5_control_action,
  output        io_bp_5_control_chain,
  output [1:0]  io_bp_5_control_tmatch,
  output        io_bp_5_control_m,
  output        io_bp_5_control_s,
  output        io_bp_5_control_u,
  output        io_bp_5_control_x,
  output        io_bp_5_control_w,
  output        io_bp_5_control_r,
  output [38:0] io_bp_5_address,
  output [2:0]  io_bp_6_control_action,
  output        io_bp_6_control_chain,
  output [1:0]  io_bp_6_control_tmatch,
  output        io_bp_6_control_m,
  output        io_bp_6_control_s,
  output        io_bp_6_control_u,
  output        io_bp_6_control_x,
  output        io_bp_6_control_w,
  output        io_bp_6_control_r,
  output [38:0] io_bp_6_address,
  output [2:0]  io_bp_7_control_action,
  output        io_bp_7_control_chain,
  output [1:0]  io_bp_7_control_tmatch,
  output        io_bp_7_control_m,
  output        io_bp_7_control_s,
  output        io_bp_7_control_u,
  output        io_bp_7_control_x,
  output        io_bp_7_control_w,
  output        io_bp_7_control_r,
  output [38:0] io_bp_7_address,
  output [2:0]  io_bp_8_control_action,
  output        io_bp_8_control_chain,
  output [1:0]  io_bp_8_control_tmatch,
  output        io_bp_8_control_m,
  output        io_bp_8_control_s,
  output        io_bp_8_control_u,
  output        io_bp_8_control_x,
  output        io_bp_8_control_w,
  output        io_bp_8_control_r,
  output [38:0] io_bp_8_address,
  output [2:0]  io_bp_9_control_action,
  output        io_bp_9_control_chain,
  output [1:0]  io_bp_9_control_tmatch,
  output        io_bp_9_control_m,
  output        io_bp_9_control_s,
  output        io_bp_9_control_u,
  output        io_bp_9_control_x,
  output        io_bp_9_control_w,
  output        io_bp_9_control_r,
  output [38:0] io_bp_9_address,
  output [2:0]  io_bp_10_control_action,
  output        io_bp_10_control_chain,
  output [1:0]  io_bp_10_control_tmatch,
  output        io_bp_10_control_m,
  output        io_bp_10_control_s,
  output        io_bp_10_control_u,
  output        io_bp_10_control_x,
  output        io_bp_10_control_w,
  output        io_bp_10_control_r,
  output [38:0] io_bp_10_address,
  output [2:0]  io_bp_11_control_action,
  output        io_bp_11_control_chain,
  output [1:0]  io_bp_11_control_tmatch,
  output        io_bp_11_control_m,
  output        io_bp_11_control_s,
  output        io_bp_11_control_u,
  output        io_bp_11_control_x,
  output        io_bp_11_control_w,
  output        io_bp_11_control_r,
  output [38:0] io_bp_11_address,
  output [2:0]  io_bp_12_control_action,
  output        io_bp_12_control_chain,
  output [1:0]  io_bp_12_control_tmatch,
  output        io_bp_12_control_m,
  output        io_bp_12_control_s,
  output        io_bp_12_control_u,
  output        io_bp_12_control_x,
  output        io_bp_12_control_w,
  output        io_bp_12_control_r,
  output [38:0] io_bp_12_address,
  output [2:0]  io_bp_13_control_action,
  output        io_bp_13_control_chain,
  output [1:0]  io_bp_13_control_tmatch,
  output        io_bp_13_control_m,
  output        io_bp_13_control_s,
  output        io_bp_13_control_u,
  output        io_bp_13_control_x,
  output        io_bp_13_control_w,
  output        io_bp_13_control_r,
  output [38:0] io_bp_13_address,
  output [2:0]  io_bp_14_control_action,
  output        io_bp_14_control_chain,
  output [1:0]  io_bp_14_control_tmatch,
  output        io_bp_14_control_m,
  output        io_bp_14_control_s,
  output        io_bp_14_control_u,
  output        io_bp_14_control_x,
  output        io_bp_14_control_w,
  output        io_bp_14_control_r,
  output [38:0] io_bp_14_address,
  output [2:0]  io_bp_15_control_action,
  output [1:0]  io_bp_15_control_tmatch,
  output        io_bp_15_control_m,
  output        io_bp_15_control_s,
  output        io_bp_15_control_u,
  output        io_bp_15_control_x,
  output        io_bp_15_control_w,
  output        io_bp_15_control_r,
  output [38:0] io_bp_15_address,
  output        io_pmp_0_cfg_l,
  output [1:0]  io_pmp_0_cfg_a,
  output        io_pmp_0_cfg_x,
  output        io_pmp_0_cfg_w,
  output        io_pmp_0_cfg_r,
  output [34:0] io_pmp_0_addr,
  output [36:0] io_pmp_0_mask,
  output        io_pmp_1_cfg_l,
  output [1:0]  io_pmp_1_cfg_a,
  output        io_pmp_1_cfg_x,
  output        io_pmp_1_cfg_w,
  output        io_pmp_1_cfg_r,
  output [34:0] io_pmp_1_addr,
  output [36:0] io_pmp_1_mask,
  output        io_pmp_2_cfg_l,
  output [1:0]  io_pmp_2_cfg_a,
  output        io_pmp_2_cfg_x,
  output        io_pmp_2_cfg_w,
  output        io_pmp_2_cfg_r,
  output [34:0] io_pmp_2_addr,
  output [36:0] io_pmp_2_mask,
  output        io_pmp_3_cfg_l,
  output [1:0]  io_pmp_3_cfg_a,
  output        io_pmp_3_cfg_x,
  output        io_pmp_3_cfg_w,
  output        io_pmp_3_cfg_r,
  output [34:0] io_pmp_3_addr,
  output [36:0] io_pmp_3_mask,
  output        io_pmp_4_cfg_l,
  output [1:0]  io_pmp_4_cfg_a,
  output        io_pmp_4_cfg_x,
  output        io_pmp_4_cfg_w,
  output        io_pmp_4_cfg_r,
  output [34:0] io_pmp_4_addr,
  output [36:0] io_pmp_4_mask,
  output        io_pmp_5_cfg_l,
  output [1:0]  io_pmp_5_cfg_a,
  output        io_pmp_5_cfg_x,
  output        io_pmp_5_cfg_w,
  output        io_pmp_5_cfg_r,
  output [34:0] io_pmp_5_addr,
  output [36:0] io_pmp_5_mask,
  output        io_pmp_6_cfg_l,
  output [1:0]  io_pmp_6_cfg_a,
  output        io_pmp_6_cfg_x,
  output        io_pmp_6_cfg_w,
  output        io_pmp_6_cfg_r,
  output [34:0] io_pmp_6_addr,
  output [36:0] io_pmp_6_mask,
  output        io_pmp_7_cfg_l,
  output [1:0]  io_pmp_7_cfg_a,
  output        io_pmp_7_cfg_x,
  output        io_pmp_7_cfg_w,
  output        io_pmp_7_cfg_r,
  output [34:0] io_pmp_7_addr,
  output [36:0] io_pmp_7_mask,
  output [63:0] io_counters_0_eventSel,
  input         io_counters_0_inc,
  output [63:0] io_counters_1_eventSel,
  input         io_counters_1_inc,
  output [63:0] io_counters_2_eventSel,
  input         io_counters_2_inc,
  output [63:0] io_counters_3_eventSel,
  input         io_counters_3_inc,
  output        io_inhibit_cycle,
  input  [31:0] io_inst_0,
  output        io_trace_0_valid,
  output [39:0] io_trace_0_iaddr,
  output [31:0] io_trace_0_insn,
  output [2:0]  io_trace_0_priv,
  output        io_trace_0_exception,
  output        io_trace_0_interrupt,
  output [63:0] io_trace_0_cause,
  output        io_customCSRs_0_wen,
  output [63:0] io_customCSRs_0_value,
  output [63:0] io_customCSRs_1_value
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [63:0] _RAND_37;
  reg [63:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [63:0] _RAND_48;
  reg [63:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [31:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [31:0] _RAND_59;
  reg [31:0] _RAND_60;
  reg [63:0] _RAND_61;
  reg [31:0] _RAND_62;
  reg [31:0] _RAND_63;
  reg [31:0] _RAND_64;
  reg [31:0] _RAND_65;
  reg [31:0] _RAND_66;
  reg [31:0] _RAND_67;
  reg [31:0] _RAND_68;
  reg [31:0] _RAND_69;
  reg [31:0] _RAND_70;
  reg [31:0] _RAND_71;
  reg [63:0] _RAND_72;
  reg [31:0] _RAND_73;
  reg [31:0] _RAND_74;
  reg [31:0] _RAND_75;
  reg [31:0] _RAND_76;
  reg [31:0] _RAND_77;
  reg [31:0] _RAND_78;
  reg [31:0] _RAND_79;
  reg [31:0] _RAND_80;
  reg [31:0] _RAND_81;
  reg [31:0] _RAND_82;
  reg [63:0] _RAND_83;
  reg [31:0] _RAND_84;
  reg [31:0] _RAND_85;
  reg [31:0] _RAND_86;
  reg [31:0] _RAND_87;
  reg [31:0] _RAND_88;
  reg [31:0] _RAND_89;
  reg [31:0] _RAND_90;
  reg [31:0] _RAND_91;
  reg [31:0] _RAND_92;
  reg [31:0] _RAND_93;
  reg [63:0] _RAND_94;
  reg [31:0] _RAND_95;
  reg [31:0] _RAND_96;
  reg [31:0] _RAND_97;
  reg [31:0] _RAND_98;
  reg [31:0] _RAND_99;
  reg [31:0] _RAND_100;
  reg [31:0] _RAND_101;
  reg [31:0] _RAND_102;
  reg [31:0] _RAND_103;
  reg [31:0] _RAND_104;
  reg [63:0] _RAND_105;
  reg [31:0] _RAND_106;
  reg [31:0] _RAND_107;
  reg [31:0] _RAND_108;
  reg [31:0] _RAND_109;
  reg [31:0] _RAND_110;
  reg [31:0] _RAND_111;
  reg [31:0] _RAND_112;
  reg [31:0] _RAND_113;
  reg [31:0] _RAND_114;
  reg [31:0] _RAND_115;
  reg [63:0] _RAND_116;
  reg [31:0] _RAND_117;
  reg [31:0] _RAND_118;
  reg [31:0] _RAND_119;
  reg [31:0] _RAND_120;
  reg [31:0] _RAND_121;
  reg [31:0] _RAND_122;
  reg [31:0] _RAND_123;
  reg [31:0] _RAND_124;
  reg [31:0] _RAND_125;
  reg [31:0] _RAND_126;
  reg [63:0] _RAND_127;
  reg [31:0] _RAND_128;
  reg [31:0] _RAND_129;
  reg [31:0] _RAND_130;
  reg [31:0] _RAND_131;
  reg [31:0] _RAND_132;
  reg [31:0] _RAND_133;
  reg [31:0] _RAND_134;
  reg [31:0] _RAND_135;
  reg [31:0] _RAND_136;
  reg [31:0] _RAND_137;
  reg [63:0] _RAND_138;
  reg [31:0] _RAND_139;
  reg [31:0] _RAND_140;
  reg [31:0] _RAND_141;
  reg [31:0] _RAND_142;
  reg [31:0] _RAND_143;
  reg [31:0] _RAND_144;
  reg [31:0] _RAND_145;
  reg [31:0] _RAND_146;
  reg [31:0] _RAND_147;
  reg [31:0] _RAND_148;
  reg [63:0] _RAND_149;
  reg [31:0] _RAND_150;
  reg [31:0] _RAND_151;
  reg [31:0] _RAND_152;
  reg [31:0] _RAND_153;
  reg [31:0] _RAND_154;
  reg [31:0] _RAND_155;
  reg [31:0] _RAND_156;
  reg [31:0] _RAND_157;
  reg [31:0] _RAND_158;
  reg [31:0] _RAND_159;
  reg [63:0] _RAND_160;
  reg [31:0] _RAND_161;
  reg [31:0] _RAND_162;
  reg [31:0] _RAND_163;
  reg [31:0] _RAND_164;
  reg [31:0] _RAND_165;
  reg [31:0] _RAND_166;
  reg [31:0] _RAND_167;
  reg [31:0] _RAND_168;
  reg [31:0] _RAND_169;
  reg [31:0] _RAND_170;
  reg [63:0] _RAND_171;
  reg [31:0] _RAND_172;
  reg [31:0] _RAND_173;
  reg [31:0] _RAND_174;
  reg [31:0] _RAND_175;
  reg [31:0] _RAND_176;
  reg [31:0] _RAND_177;
  reg [31:0] _RAND_178;
  reg [31:0] _RAND_179;
  reg [31:0] _RAND_180;
  reg [31:0] _RAND_181;
  reg [63:0] _RAND_182;
  reg [31:0] _RAND_183;
  reg [31:0] _RAND_184;
  reg [31:0] _RAND_185;
  reg [31:0] _RAND_186;
  reg [31:0] _RAND_187;
  reg [31:0] _RAND_188;
  reg [31:0] _RAND_189;
  reg [31:0] _RAND_190;
  reg [31:0] _RAND_191;
  reg [31:0] _RAND_192;
  reg [63:0] _RAND_193;
  reg [31:0] _RAND_194;
  reg [31:0] _RAND_195;
  reg [31:0] _RAND_196;
  reg [31:0] _RAND_197;
  reg [31:0] _RAND_198;
  reg [31:0] _RAND_199;
  reg [31:0] _RAND_200;
  reg [31:0] _RAND_201;
  reg [31:0] _RAND_202;
  reg [31:0] _RAND_203;
  reg [63:0] _RAND_204;
  reg [31:0] _RAND_205;
  reg [31:0] _RAND_206;
  reg [31:0] _RAND_207;
  reg [31:0] _RAND_208;
  reg [31:0] _RAND_209;
  reg [31:0] _RAND_210;
  reg [31:0] _RAND_211;
  reg [31:0] _RAND_212;
  reg [31:0] _RAND_213;
  reg [31:0] _RAND_214;
  reg [63:0] _RAND_215;
  reg [31:0] _RAND_216;
  reg [31:0] _RAND_217;
  reg [31:0] _RAND_218;
  reg [31:0] _RAND_219;
  reg [31:0] _RAND_220;
  reg [31:0] _RAND_221;
  reg [31:0] _RAND_222;
  reg [31:0] _RAND_223;
  reg [31:0] _RAND_224;
  reg [63:0] _RAND_225;
  reg [31:0] _RAND_226;
  reg [31:0] _RAND_227;
  reg [31:0] _RAND_228;
  reg [31:0] _RAND_229;
  reg [31:0] _RAND_230;
  reg [63:0] _RAND_231;
  reg [31:0] _RAND_232;
  reg [31:0] _RAND_233;
  reg [31:0] _RAND_234;
  reg [31:0] _RAND_235;
  reg [31:0] _RAND_236;
  reg [63:0] _RAND_237;
  reg [31:0] _RAND_238;
  reg [31:0] _RAND_239;
  reg [31:0] _RAND_240;
  reg [31:0] _RAND_241;
  reg [31:0] _RAND_242;
  reg [63:0] _RAND_243;
  reg [31:0] _RAND_244;
  reg [31:0] _RAND_245;
  reg [31:0] _RAND_246;
  reg [31:0] _RAND_247;
  reg [31:0] _RAND_248;
  reg [63:0] _RAND_249;
  reg [31:0] _RAND_250;
  reg [31:0] _RAND_251;
  reg [31:0] _RAND_252;
  reg [31:0] _RAND_253;
  reg [31:0] _RAND_254;
  reg [63:0] _RAND_255;
  reg [31:0] _RAND_256;
  reg [31:0] _RAND_257;
  reg [31:0] _RAND_258;
  reg [31:0] _RAND_259;
  reg [31:0] _RAND_260;
  reg [63:0] _RAND_261;
  reg [31:0] _RAND_262;
  reg [31:0] _RAND_263;
  reg [31:0] _RAND_264;
  reg [31:0] _RAND_265;
  reg [31:0] _RAND_266;
  reg [63:0] _RAND_267;
  reg [31:0] _RAND_268;
  reg [31:0] _RAND_269;
  reg [31:0] _RAND_270;
  reg [31:0] _RAND_271;
  reg [31:0] _RAND_272;
  reg [63:0] _RAND_273;
  reg [63:0] _RAND_274;
  reg [31:0] _RAND_275;
  reg [31:0] _RAND_276;
  reg [31:0] _RAND_277;
  reg [63:0] _RAND_278;
  reg [63:0] _RAND_279;
  reg [63:0] _RAND_280;
  reg [63:0] _RAND_281;
  reg [63:0] _RAND_282;
  reg [63:0] _RAND_283;
  reg [63:0] _RAND_284;
  reg [63:0] _RAND_285;
  reg [31:0] _RAND_286;
  reg [31:0] _RAND_287;
  reg [63:0] _RAND_288;
  reg [63:0] _RAND_289;
  reg [63:0] _RAND_290;
  reg [63:0] _RAND_291;
  reg [63:0] _RAND_292;
  reg [31:0] _RAND_293;
  reg [63:0] _RAND_294;
  reg [31:0] _RAND_295;
  reg [31:0] _RAND_296;
  reg [31:0] _RAND_297;
  reg [31:0] _RAND_298;
  reg [31:0] _RAND_299;
  reg [63:0] _RAND_300;
  reg [31:0] _RAND_301;
  reg [63:0] _RAND_302;
  reg [63:0] _RAND_303;
  reg [63:0] _RAND_304;
  reg [63:0] _RAND_305;
  reg [63:0] _RAND_306;
  reg [31:0] _RAND_307;
  reg [63:0] _RAND_308;
  reg [31:0] _RAND_309;
  reg [63:0] _RAND_310;
  reg [31:0] _RAND_311;
  reg [63:0] _RAND_312;
  reg [31:0] _RAND_313;
  reg [63:0] _RAND_314;
  reg [63:0] _RAND_315;
  reg [63:0] _RAND_316;
  reg [63:0] _RAND_317;
  reg [31:0] _RAND_318;
  reg [31:0] _RAND_319;
`endif // RANDOMIZE_REG_INIT
  reg [31:0] reg_mstatus_isa; // @[CSR.scala 318:24]
  reg [1:0] reg_mstatus_dprv; // @[CSR.scala 318:24]
  reg [1:0] reg_mstatus_prv; // @[CSR.scala 318:24]
  reg  reg_mstatus_sd; // @[CSR.scala 318:24]
  reg [26:0] reg_mstatus_zero2; // @[CSR.scala 318:24]
  reg [1:0] reg_mstatus_sxl; // @[CSR.scala 318:24]
  reg [1:0] reg_mstatus_uxl; // @[CSR.scala 318:24]
  reg  reg_mstatus_sd_rv32; // @[CSR.scala 318:24]
  reg [7:0] reg_mstatus_zero1; // @[CSR.scala 318:24]
  reg  reg_mstatus_tsr; // @[CSR.scala 318:24]
  reg  reg_mstatus_tw; // @[CSR.scala 318:24]
  reg  reg_mstatus_tvm; // @[CSR.scala 318:24]
  reg  reg_mstatus_mxr; // @[CSR.scala 318:24]
  reg  reg_mstatus_sum; // @[CSR.scala 318:24]
  reg  reg_mstatus_mprv; // @[CSR.scala 318:24]
  reg [1:0] reg_mstatus_xs; // @[CSR.scala 318:24]
  reg [1:0] reg_mstatus_fs; // @[CSR.scala 318:24]
  reg [1:0] reg_mstatus_mpp; // @[CSR.scala 318:24]
  reg [1:0] reg_mstatus_vs; // @[CSR.scala 318:24]
  reg  reg_mstatus_spp; // @[CSR.scala 318:24]
  reg  reg_mstatus_mpie; // @[CSR.scala 318:24]
  reg  reg_mstatus_hpie; // @[CSR.scala 318:24]
  reg  reg_mstatus_spie; // @[CSR.scala 318:24]
  reg  reg_mstatus_upie; // @[CSR.scala 318:24]
  reg  reg_mstatus_mie; // @[CSR.scala 318:24]
  reg  reg_mstatus_hie; // @[CSR.scala 318:24]
  reg  reg_mstatus_sie; // @[CSR.scala 318:24]
  reg  reg_mstatus_uie; // @[CSR.scala 318:24]
  wire  system_insn = io_rw_cmd == 3'h4; // @[CSR.scala 639:31]
  wire [31:0] _T_219 = {io_rw_addr, 20'h0}; // @[CSR.scala 652:28]
  wire [31:0] _T_226 = _T_219 & 32'h92400000; // @[Decode.scala 14:65]
  wire  _T_227 = _T_226 == 32'h10000000; // @[Decode.scala 14:121]
  wire [31:0] _T_228 = _T_219 & 32'ha0400000; // @[Decode.scala 14:65]
  wire  _T_229 = _T_228 == 32'h20000000; // @[Decode.scala 14:121]
  wire  _T_231 = _T_227 | _T_229; // @[Decode.scala 15:30]
  wire  insn_ret = system_insn & _T_231; // @[CSR.scala 652:95]
  reg [1:0] reg_dcsr_prv; // @[CSR.scala 326:21]
  reg [1:0] reg_mnstatus_mpp; // @[CSR.scala 405:25]
  wire [1:0] _GEN_454 = io_rw_addr[10] & ~io_rw_addr[7] ? reg_mnstatus_mpp : reg_mstatus_mpp; // @[CSR.scala 822:69 CSR.scala 823:15 CSR.scala 830:15]
  wire [1:0] _GEN_460 = io_rw_addr[10] & io_rw_addr[7] ? reg_dcsr_prv : _GEN_454; // @[CSR.scala 818:70 CSR.scala 819:15]
  wire [1:0] ret_prv = ~io_rw_addr[9] ? {{1'd0}, reg_mstatus_spp} : _GEN_460; // @[CSR.scala 812:52 CSR.scala 816:15]
  wire [31:0] _T_220 = _T_219 & 32'h10100000; // @[Decode.scala 14:65]
  wire  _T_221 = _T_220 == 32'h0; // @[Decode.scala 14:121]
  wire  insn_call = system_insn & _T_221; // @[CSR.scala 652:95]
  wire  _T_224 = _T_220 == 32'h100000; // @[Decode.scala 14:121]
  wire  insn_break = system_insn & _T_224; // @[CSR.scala 652:95]
  wire  _exception_T = insn_call | insn_break; // @[CSR.scala 737:29]
  wire  exception = insn_call | insn_break | io_exception; // @[CSR.scala 737:43]
  reg  reg_singleStepped; // @[CSR.scala 371:30]
  wire [3:0] _GEN_28 = {{2'd0}, reg_mstatus_prv}; // @[CSR.scala 688:36]
  wire [3:0] _cause_T_1 = _GEN_28 + 4'h8; // @[CSR.scala 688:36]
  wire [63:0] _cause_T_2 = insn_break ? 64'h3 : io_cause; // @[CSR.scala 689:14]
  wire [63:0] cause = insn_call ? {{60'd0}, _cause_T_1} : _cause_T_2; // @[CSR.scala 688:8]
  wire [7:0] cause_lsbs = cause[7:0]; // @[CSR.scala 690:25]
  wire  _causeIsDebugInt_T_1 = cause_lsbs == 8'he; // @[CSR.scala 691:53]
  wire  causeIsDebugInt = cause[63] & cause_lsbs == 8'he; // @[CSR.scala 691:39]
  wire  _causeIsDebugTrigger_T_1 = ~cause[63]; // @[CSR.scala 692:29]
  wire  causeIsDebugTrigger = ~cause[63] & _causeIsDebugInt_T_1; // @[CSR.scala 692:44]
  reg  reg_dcsr_ebreakm; // @[CSR.scala 326:21]
  reg  reg_dcsr_ebreakh; // @[CSR.scala 326:21]
  reg  reg_dcsr_ebreaks; // @[CSR.scala 326:21]
  reg  reg_dcsr_ebreaku; // @[CSR.scala 326:21]
  wire [3:0] _causeIsDebugBreak_T_3 = {reg_dcsr_ebreakm,reg_dcsr_ebreakh,reg_dcsr_ebreaks,reg_dcsr_ebreaku}; // @[Cat.scala 30:58]
  wire [3:0] _causeIsDebugBreak_T_4 = _causeIsDebugBreak_T_3 >> reg_mstatus_prv; // @[CSR.scala 693:134]
  wire  causeIsDebugBreak = _causeIsDebugTrigger_T_1 & insn_break & _causeIsDebugBreak_T_4[0]; // @[CSR.scala 693:56]
  reg  reg_debug; // @[CSR.scala 367:22]
  wire  trapToDebug = reg_singleStepped | causeIsDebugInt | causeIsDebugTrigger | causeIsDebugBreak | reg_debug; // @[CSR.scala 694:123]
  wire  _T_304 = ~reg_debug; // @[CSR.scala 754:13]
  wire [1:0] _GEN_372 = ~reg_debug ? 2'h3 : reg_mstatus_prv; // @[CSR.scala 754:25 CSR.scala 759:17]
  wire  _causeIsRnmiInt_T_2 = cause[63] & cause[62]; // @[CSR.scala 712:38]
  wire  _causeIsRnmiInt_T_4 = cause_lsbs == 8'hc; // @[CSR.scala 712:105]
  wire  causeIsNmi = cause[63] & cause[62] & (cause_lsbs == 8'hd | cause_lsbs == 8'hc); // @[CSR.scala 712:55]
  reg  nmie; // @[CSR.scala 406:26]
  wire [1:0] _GEN_377 = nmie ? 2'h3 : reg_mstatus_prv; // @[CSR.scala 762:24 CSR.scala 767:17]
  wire  _delegate_T = reg_mstatus_prv <= 2'h1; // @[CSR.scala 698:59]
  reg [63:0] reg_mideleg; // @[CSR.scala 382:18]
  wire [63:0] read_mideleg = reg_mideleg & 64'hffff0222; // @[CSR.scala 383:36]
  wire [63:0] _delegate_T_3 = read_mideleg >> cause_lsbs; // @[CSR.scala 698:102]
  reg [63:0] reg_medeleg; // @[CSR.scala 386:18]
  wire [63:0] read_medeleg = reg_medeleg & 64'hb15d; // @[CSR.scala 387:36]
  wire [63:0] _delegate_T_5 = read_medeleg >> cause_lsbs; // @[CSR.scala 698:128]
  wire  _delegate_T_7 = cause[63] ? _delegate_T_3[0] : _delegate_T_5[0]; // @[CSR.scala 698:74]
  wire  delegate = reg_mstatus_prv <= 2'h1 & _delegate_T_7; // @[CSR.scala 698:68]
  wire [1:0] _GEN_385 = delegate & nmie ? 2'h1 : 2'h3; // @[CSR.scala 769:35 CSR.scala 777:15 CSR.scala 786:15]
  wire [1:0] _GEN_396 = causeIsNmi ? _GEN_377 : _GEN_385; // @[CSR.scala 761:31]
  wire [1:0] _GEN_414 = trapToDebug ? _GEN_372 : _GEN_396; // @[CSR.scala 753:24]
  wire [1:0] _GEN_436 = exception ? _GEN_414 : reg_mstatus_prv; // @[CSR.scala 752:20]
  wire [1:0] new_prv = insn_ret ? ret_prv : _GEN_436; // @[CSR.scala 810:19 CSR.scala 834:13]
  reg [1:0] reg_dcsr_xdebugver; // @[CSR.scala 326:21]
  reg [1:0] reg_dcsr_zero4; // @[CSR.scala 326:21]
  reg [11:0] reg_dcsr_zero3; // @[CSR.scala 326:21]
  reg  reg_dcsr_zero2; // @[CSR.scala 326:21]
  reg  reg_dcsr_stopcycle; // @[CSR.scala 326:21]
  reg  reg_dcsr_stoptime; // @[CSR.scala 326:21]
  reg [2:0] reg_dcsr_cause; // @[CSR.scala 326:21]
  reg [2:0] reg_dcsr_zero1; // @[CSR.scala 326:21]
  reg  reg_dcsr_step; // @[CSR.scala 326:21]
  reg [39:0] reg_dpc; // @[CSR.scala 368:20]
  reg [63:0] reg_dscratch; // @[CSR.scala 369:25]
  reg [3:0] reg_tselect; // @[CSR.scala 376:24]
  reg  reg_bp_0_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_0_control_action; // @[CSR.scala 377:19]
  reg  reg_bp_0_control_chain; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_0_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_0_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_0_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_0_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_0_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_0_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_0_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_0_address; // @[CSR.scala 377:19]
  reg  reg_bp_1_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_1_control_action; // @[CSR.scala 377:19]
  reg  reg_bp_1_control_chain; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_1_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_1_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_1_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_1_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_1_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_1_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_1_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_1_address; // @[CSR.scala 377:19]
  reg  reg_bp_2_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_2_control_action; // @[CSR.scala 377:19]
  reg  reg_bp_2_control_chain; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_2_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_2_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_2_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_2_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_2_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_2_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_2_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_2_address; // @[CSR.scala 377:19]
  reg  reg_bp_3_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_3_control_action; // @[CSR.scala 377:19]
  reg  reg_bp_3_control_chain; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_3_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_3_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_3_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_3_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_3_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_3_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_3_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_3_address; // @[CSR.scala 377:19]
  reg  reg_bp_4_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_4_control_action; // @[CSR.scala 377:19]
  reg  reg_bp_4_control_chain; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_4_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_4_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_4_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_4_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_4_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_4_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_4_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_4_address; // @[CSR.scala 377:19]
  reg  reg_bp_5_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_5_control_action; // @[CSR.scala 377:19]
  reg  reg_bp_5_control_chain; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_5_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_5_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_5_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_5_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_5_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_5_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_5_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_5_address; // @[CSR.scala 377:19]
  reg  reg_bp_6_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_6_control_action; // @[CSR.scala 377:19]
  reg  reg_bp_6_control_chain; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_6_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_6_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_6_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_6_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_6_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_6_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_6_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_6_address; // @[CSR.scala 377:19]
  reg  reg_bp_7_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_7_control_action; // @[CSR.scala 377:19]
  reg  reg_bp_7_control_chain; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_7_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_7_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_7_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_7_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_7_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_7_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_7_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_7_address; // @[CSR.scala 377:19]
  reg  reg_bp_8_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_8_control_action; // @[CSR.scala 377:19]
  reg  reg_bp_8_control_chain; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_8_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_8_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_8_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_8_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_8_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_8_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_8_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_8_address; // @[CSR.scala 377:19]
  reg  reg_bp_9_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_9_control_action; // @[CSR.scala 377:19]
  reg  reg_bp_9_control_chain; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_9_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_9_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_9_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_9_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_9_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_9_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_9_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_9_address; // @[CSR.scala 377:19]
  reg  reg_bp_10_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_10_control_action; // @[CSR.scala 377:19]
  reg  reg_bp_10_control_chain; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_10_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_10_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_10_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_10_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_10_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_10_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_10_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_10_address; // @[CSR.scala 377:19]
  reg  reg_bp_11_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_11_control_action; // @[CSR.scala 377:19]
  reg  reg_bp_11_control_chain; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_11_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_11_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_11_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_11_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_11_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_11_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_11_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_11_address; // @[CSR.scala 377:19]
  reg  reg_bp_12_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_12_control_action; // @[CSR.scala 377:19]
  reg  reg_bp_12_control_chain; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_12_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_12_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_12_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_12_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_12_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_12_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_12_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_12_address; // @[CSR.scala 377:19]
  reg  reg_bp_13_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_13_control_action; // @[CSR.scala 377:19]
  reg  reg_bp_13_control_chain; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_13_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_13_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_13_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_13_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_13_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_13_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_13_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_13_address; // @[CSR.scala 377:19]
  reg  reg_bp_14_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_14_control_action; // @[CSR.scala 377:19]
  reg  reg_bp_14_control_chain; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_14_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_14_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_14_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_14_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_14_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_14_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_14_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_14_address; // @[CSR.scala 377:19]
  reg  reg_bp_15_control_dmode; // @[CSR.scala 377:19]
  reg [2:0] reg_bp_15_control_action; // @[CSR.scala 377:19]
  reg [1:0] reg_bp_15_control_tmatch; // @[CSR.scala 377:19]
  reg  reg_bp_15_control_m; // @[CSR.scala 377:19]
  reg  reg_bp_15_control_s; // @[CSR.scala 377:19]
  reg  reg_bp_15_control_u; // @[CSR.scala 377:19]
  reg  reg_bp_15_control_x; // @[CSR.scala 377:19]
  reg  reg_bp_15_control_w; // @[CSR.scala 377:19]
  reg  reg_bp_15_control_r; // @[CSR.scala 377:19]
  reg [38:0] reg_bp_15_address; // @[CSR.scala 377:19]
  reg  reg_pmp_0_cfg_l; // @[CSR.scala 378:20]
  reg [1:0] reg_pmp_0_cfg_a; // @[CSR.scala 378:20]
  reg  reg_pmp_0_cfg_x; // @[CSR.scala 378:20]
  reg  reg_pmp_0_cfg_w; // @[CSR.scala 378:20]
  reg  reg_pmp_0_cfg_r; // @[CSR.scala 378:20]
  reg [34:0] reg_pmp_0_addr; // @[CSR.scala 378:20]
  reg  reg_pmp_1_cfg_l; // @[CSR.scala 378:20]
  reg [1:0] reg_pmp_1_cfg_a; // @[CSR.scala 378:20]
  reg  reg_pmp_1_cfg_x; // @[CSR.scala 378:20]
  reg  reg_pmp_1_cfg_w; // @[CSR.scala 378:20]
  reg  reg_pmp_1_cfg_r; // @[CSR.scala 378:20]
  reg [34:0] reg_pmp_1_addr; // @[CSR.scala 378:20]
  reg  reg_pmp_2_cfg_l; // @[CSR.scala 378:20]
  reg [1:0] reg_pmp_2_cfg_a; // @[CSR.scala 378:20]
  reg  reg_pmp_2_cfg_x; // @[CSR.scala 378:20]
  reg  reg_pmp_2_cfg_w; // @[CSR.scala 378:20]
  reg  reg_pmp_2_cfg_r; // @[CSR.scala 378:20]
  reg [34:0] reg_pmp_2_addr; // @[CSR.scala 378:20]
  reg  reg_pmp_3_cfg_l; // @[CSR.scala 378:20]
  reg [1:0] reg_pmp_3_cfg_a; // @[CSR.scala 378:20]
  reg  reg_pmp_3_cfg_x; // @[CSR.scala 378:20]
  reg  reg_pmp_3_cfg_w; // @[CSR.scala 378:20]
  reg  reg_pmp_3_cfg_r; // @[CSR.scala 378:20]
  reg [34:0] reg_pmp_3_addr; // @[CSR.scala 378:20]
  reg  reg_pmp_4_cfg_l; // @[CSR.scala 378:20]
  reg [1:0] reg_pmp_4_cfg_a; // @[CSR.scala 378:20]
  reg  reg_pmp_4_cfg_x; // @[CSR.scala 378:20]
  reg  reg_pmp_4_cfg_w; // @[CSR.scala 378:20]
  reg  reg_pmp_4_cfg_r; // @[CSR.scala 378:20]
  reg [34:0] reg_pmp_4_addr; // @[CSR.scala 378:20]
  reg  reg_pmp_5_cfg_l; // @[CSR.scala 378:20]
  reg [1:0] reg_pmp_5_cfg_a; // @[CSR.scala 378:20]
  reg  reg_pmp_5_cfg_x; // @[CSR.scala 378:20]
  reg  reg_pmp_5_cfg_w; // @[CSR.scala 378:20]
  reg  reg_pmp_5_cfg_r; // @[CSR.scala 378:20]
  reg [34:0] reg_pmp_5_addr; // @[CSR.scala 378:20]
  reg  reg_pmp_6_cfg_l; // @[CSR.scala 378:20]
  reg [1:0] reg_pmp_6_cfg_a; // @[CSR.scala 378:20]
  reg  reg_pmp_6_cfg_x; // @[CSR.scala 378:20]
  reg  reg_pmp_6_cfg_w; // @[CSR.scala 378:20]
  reg  reg_pmp_6_cfg_r; // @[CSR.scala 378:20]
  reg [34:0] reg_pmp_6_addr; // @[CSR.scala 378:20]
  reg  reg_pmp_7_cfg_l; // @[CSR.scala 378:20]
  reg [1:0] reg_pmp_7_cfg_a; // @[CSR.scala 378:20]
  reg  reg_pmp_7_cfg_x; // @[CSR.scala 378:20]
  reg  reg_pmp_7_cfg_w; // @[CSR.scala 378:20]
  reg  reg_pmp_7_cfg_r; // @[CSR.scala 378:20]
  reg [34:0] reg_pmp_7_addr; // @[CSR.scala 378:20]
  reg [63:0] reg_mie; // @[CSR.scala 380:20]
  reg  reg_mip_seip; // @[CSR.scala 389:20]
  reg  reg_mip_stip; // @[CSR.scala 389:20]
  reg  reg_mip_ssip; // @[CSR.scala 389:20]
  reg [39:0] reg_mepc; // @[CSR.scala 390:21]
  reg [63:0] reg_mcause; // @[CSR.scala 391:27]
  reg [39:0] reg_mtval; // @[CSR.scala 392:22]
  reg [63:0] reg_mscratch; // @[CSR.scala 393:25]
  reg [36:0] reg_mtvec; // @[CSR.scala 396:27]
  reg [63:0] reg_mnscratch; // @[CSR.scala 402:26]
  reg [39:0] reg_mnepc; // @[CSR.scala 403:22]
  reg [63:0] reg_mncause; // @[CSR.scala 404:28]
  reg [31:0] reg_mcounteren; // @[CSR.scala 411:18]
  wire [31:0] read_mcounteren = reg_mcounteren & 32'h7f; // @[CSR.scala 412:30]
  reg [31:0] reg_scounteren; // @[CSR.scala 415:18]
  wire [31:0] read_scounteren = reg_scounteren & 32'h7f; // @[CSR.scala 416:36]
  reg [39:0] reg_sepc; // @[CSR.scala 419:21]
  reg [63:0] reg_scause; // @[CSR.scala 420:23]
  reg [39:0] reg_stval; // @[CSR.scala 421:22]
  reg [63:0] reg_sscratch; // @[CSR.scala 422:25]
  reg [38:0] reg_stvec; // @[CSR.scala 423:22]
  reg [3:0] reg_satp_mode; // @[CSR.scala 424:21]
  reg [43:0] reg_satp_ppn; // @[CSR.scala 424:21]
  reg  reg_wfi; // @[CSR.scala 425:50]
  reg [4:0] reg_fflags; // @[CSR.scala 427:23]
  reg [2:0] reg_frm; // @[CSR.scala 428:20]
  reg [6:0] reg_mcountinhibit; // @[CSR.scala 434:34]
  wire  x67 = reg_mcountinhibit[2]; // @[CSR.scala 436:75]
  reg [5:0] value_lo; // @[Counters.scala 45:37]
  wire [5:0] _GEN_31 = {{5'd0}, io_retire}; // @[Counters.scala 46:33]
  wire [6:0] nextSmall = value_lo + _GEN_31; // @[Counters.scala 46:33]
  wire  _T_5 = ~x67; // @[Counters.scala 47:9]
  wire [6:0] _GEN_0 = ~x67 ? nextSmall : {{1'd0}, value_lo}; // @[Counters.scala 47:19 Counters.scala 47:27 Counters.scala 45:37]
  reg [57:0] value_hi; // @[Counters.scala 50:27]
  wire [57:0] _large_r_T_1 = value_hi + 58'h1; // @[Counters.scala 51:55]
  wire [57:0] _GEN_1 = nextSmall[6] & _T_5 ? _large_r_T_1 : value_hi; // @[Counters.scala 51:46 Counters.scala 51:50 Counters.scala 50:27]
  wire [63:0] value = {value_hi,value_lo}; // @[Cat.scala 30:58]
  wire  x74 = ~io_csr_stall; // @[CSR.scala 438:56]
  reg [5:0] value_lo_1; // @[Counters.scala 45:37]
  wire [5:0] _GEN_50 = {{5'd0}, x74}; // @[Counters.scala 46:33]
  wire [6:0] nextSmall_1 = value_lo_1 + _GEN_50; // @[Counters.scala 46:33]
  wire  _T_6 = ~reg_mcountinhibit[0]; // @[Counters.scala 47:9]
  wire [6:0] _GEN_2 = ~reg_mcountinhibit[0] ? nextSmall_1 : {{1'd0}, value_lo_1}; // @[Counters.scala 47:19 Counters.scala 47:27 Counters.scala 45:37]
  reg [57:0] value_hi_1; // @[Counters.scala 50:27]
  wire [57:0] _large_r_T_3 = value_hi_1 + 58'h1; // @[Counters.scala 51:55]
  wire [57:0] _GEN_3 = nextSmall_1[6] & _T_6 ? _large_r_T_3 : value_hi_1; // @[Counters.scala 51:46 Counters.scala 51:50 Counters.scala 50:27]
  wire [63:0] value_1 = {value_hi_1,value_lo_1}; // @[Cat.scala 30:58]
  reg [63:0] reg_hpmevent_0; // @[CSR.scala 439:46]
  reg [63:0] reg_hpmevent_1; // @[CSR.scala 439:46]
  reg [63:0] reg_hpmevent_2; // @[CSR.scala 439:46]
  reg [63:0] reg_hpmevent_3; // @[CSR.scala 439:46]
  reg [5:0] value_lo_2; // @[Counters.scala 45:72]
  wire [5:0] _GEN_53 = {{5'd0}, io_counters_0_inc}; // @[Counters.scala 46:33]
  wire [6:0] nextSmall_2 = value_lo_2 + _GEN_53; // @[Counters.scala 46:33]
  wire  _T_8 = ~reg_mcountinhibit[3]; // @[Counters.scala 47:9]
  wire [6:0] _GEN_4 = ~reg_mcountinhibit[3] ? nextSmall_2 : {{1'd0}, value_lo_2}; // @[Counters.scala 47:19 Counters.scala 47:27 Counters.scala 45:72]
  reg [33:0] value_hi_2; // @[Counters.scala 50:70]
  wire [33:0] _large_r_T_5 = value_hi_2 + 34'h1; // @[Counters.scala 51:55]
  wire [33:0] _GEN_5 = nextSmall_2[6] & _T_8 ? _large_r_T_5 : value_hi_2; // @[Counters.scala 51:46 Counters.scala 51:50 Counters.scala 50:70]
  wire [39:0] value_2 = {value_hi_2,value_lo_2}; // @[Cat.scala 30:58]
  reg [5:0] value_lo_3; // @[Counters.scala 45:72]
  wire [5:0] _GEN_72 = {{5'd0}, io_counters_1_inc}; // @[Counters.scala 46:33]
  wire [6:0] nextSmall_3 = value_lo_3 + _GEN_72; // @[Counters.scala 46:33]
  wire  _T_10 = ~reg_mcountinhibit[4]; // @[Counters.scala 47:9]
  wire [6:0] _GEN_6 = ~reg_mcountinhibit[4] ? nextSmall_3 : {{1'd0}, value_lo_3}; // @[Counters.scala 47:19 Counters.scala 47:27 Counters.scala 45:72]
  reg [33:0] value_hi_3; // @[Counters.scala 50:70]
  wire [33:0] _large_r_T_7 = value_hi_3 + 34'h1; // @[Counters.scala 51:55]
  wire [33:0] _GEN_7 = nextSmall_3[6] & _T_10 ? _large_r_T_7 : value_hi_3; // @[Counters.scala 51:46 Counters.scala 51:50 Counters.scala 50:70]
  wire [39:0] value_3 = {value_hi_3,value_lo_3}; // @[Cat.scala 30:58]
  reg [5:0] value_lo_4; // @[Counters.scala 45:72]
  wire [5:0] _GEN_75 = {{5'd0}, io_counters_2_inc}; // @[Counters.scala 46:33]
  wire [6:0] nextSmall_4 = value_lo_4 + _GEN_75; // @[Counters.scala 46:33]
  wire  _T_12 = ~reg_mcountinhibit[5]; // @[Counters.scala 47:9]
  wire [6:0] _GEN_8 = ~reg_mcountinhibit[5] ? nextSmall_4 : {{1'd0}, value_lo_4}; // @[Counters.scala 47:19 Counters.scala 47:27 Counters.scala 45:72]
  reg [33:0] value_hi_4; // @[Counters.scala 50:70]
  wire [33:0] _large_r_T_9 = value_hi_4 + 34'h1; // @[Counters.scala 51:55]
  wire [33:0] _GEN_9 = nextSmall_4[6] & _T_12 ? _large_r_T_9 : value_hi_4; // @[Counters.scala 51:46 Counters.scala 51:50 Counters.scala 50:70]
  wire [39:0] value_4 = {value_hi_4,value_lo_4}; // @[Cat.scala 30:58]
  reg [5:0] value_lo_5; // @[Counters.scala 45:72]
  wire [5:0] _GEN_94 = {{5'd0}, io_counters_3_inc}; // @[Counters.scala 46:33]
  wire [6:0] nextSmall_5 = value_lo_5 + _GEN_94; // @[Counters.scala 46:33]
  wire  _T_14 = ~reg_mcountinhibit[6]; // @[Counters.scala 47:9]
  wire [6:0] _GEN_10 = ~reg_mcountinhibit[6] ? nextSmall_5 : {{1'd0}, value_lo_5}; // @[Counters.scala 47:19 Counters.scala 47:27 Counters.scala 45:72]
  reg [33:0] value_hi_5; // @[Counters.scala 50:70]
  wire [33:0] _large_r_T_11 = value_hi_5 + 34'h1; // @[Counters.scala 51:55]
  wire [33:0] _GEN_11 = nextSmall_5[6] & _T_14 ? _large_r_T_11 : value_hi_5; // @[Counters.scala 51:46 Counters.scala 51:50 Counters.scala 50:70]
  wire [39:0] value_5 = {value_hi_5,value_lo_5}; // @[Cat.scala 30:58]
  wire  mip_seip = reg_mip_seip | io_interrupts_seip; // @[CSR.scala 450:57]
  wire [7:0] read_mip_lo_lo = {io_interrupts_mtip,1'h0,reg_mip_stip,1'h0,io_interrupts_msip,1'h0,reg_mip_ssip,1'h0}; // @[CSR.scala 452:22]
  wire [15:0] read_mip_lo = {4'h0,io_interrupts_meip,1'h0,mip_seip,1'h0,read_mip_lo_lo}; // @[CSR.scala 452:22]
  wire [7:0] read_mip_hi_lo = {io_interrupts_lip_7,io_interrupts_lip_6,io_interrupts_lip_5,io_interrupts_lip_4,
    io_interrupts_lip_3,io_interrupts_lip_2,io_interrupts_lip_1,io_interrupts_lip_0}; // @[CSR.scala 452:22]
  wire [31:0] _read_mip_T = {io_interrupts_lip_15,io_interrupts_lip_14,io_interrupts_lip_13,io_interrupts_lip_12,
    io_interrupts_lip_11,io_interrupts_lip_10,io_interrupts_lip_9,io_interrupts_lip_8,read_mip_hi_lo,read_mip_lo}; // @[CSR.scala 452:22]
  wire [31:0] read_mip = _read_mip_T & 32'hffff0aaa; // @[CSR.scala 452:29]
  wire [63:0] _GEN_97 = {{32'd0}, read_mip}; // @[CSR.scala 457:56]
  wire [63:0] pending_interrupts = _GEN_97 & reg_mie; // @[CSR.scala 457:56]
  wire [14:0] d_interrupts = {io_interrupts_debug, 14'h0}; // @[CSR.scala 458:42]
  wire  _T_15 = io_interrupts_nmi_rnmi & nmie; // @[CSR.scala 460:17]
  wire [13:0] nmi_interrupts = {_T_15, 13'h0}; // @[CSR.scala 460:31]
  wire  nmiFlag = ~io_interrupts_debug & io_interrupts_nmi_rnmi & nmie; // @[CSR.scala 462:37]
  wire [63:0] _m_interrupts_T_3 = ~pending_interrupts; // @[CSR.scala 463:83]
  wire [63:0] _m_interrupts_T_4 = _m_interrupts_T_3 | read_mideleg; // @[CSR.scala 463:103]
  wire [63:0] _m_interrupts_T_5 = ~_m_interrupts_T_4; // @[CSR.scala 463:81]
  wire [63:0] m_interrupts = nmie & (_delegate_T | reg_mstatus_mie) ? _m_interrupts_T_5 : 64'h0; // @[CSR.scala 463:25]
  wire [63:0] _s_interrupts_T_5 = pending_interrupts & read_mideleg; // @[CSR.scala 464:130]
  wire [63:0] s_interrupts = nmie & (reg_mstatus_prv < 2'h1 | reg_mstatus_prv == 2'h1 & reg_mstatus_sie) ?
    _s_interrupts_T_5 : 64'h0; // @[CSR.scala 464:25]
  wire  _any_T_95 = d_interrupts[14] | d_interrupts[13] | d_interrupts[12] | d_interrupts[11] | d_interrupts[3] |
    d_interrupts[7] | d_interrupts[9] | d_interrupts[1] | d_interrupts[5] | d_interrupts[8] | d_interrupts[0] |
    d_interrupts[4] | nmi_interrupts[13] | nmi_interrupts[12] | nmi_interrupts[11] | nmi_interrupts[3]; // @[CSR.scala 1176:90]
  wire  _any_T_110 = _any_T_95 | nmi_interrupts[7] | nmi_interrupts[9] | nmi_interrupts[1] | nmi_interrupts[5] |
    nmi_interrupts[8] | nmi_interrupts[0] | nmi_interrupts[4] | m_interrupts[31] | m_interrupts[30] | m_interrupts[29]
     | m_interrupts[28] | m_interrupts[27] | m_interrupts[26] | m_interrupts[25] | m_interrupts[24]; // @[CSR.scala 1176:90]
  wire  _any_T_125 = _any_T_110 | m_interrupts[23] | m_interrupts[22] | m_interrupts[21] | m_interrupts[20] |
    m_interrupts[19] | m_interrupts[18] | m_interrupts[17] | m_interrupts[16] | m_interrupts[15] | m_interrupts[14] |
    m_interrupts[13] | m_interrupts[12] | m_interrupts[11] | m_interrupts[3] | m_interrupts[7]; // @[CSR.scala 1176:90]
  wire  _any_T_140 = _any_T_125 | m_interrupts[9] | m_interrupts[1] | m_interrupts[5] | m_interrupts[8] | m_interrupts[0
    ] | m_interrupts[4] | s_interrupts[31] | s_interrupts[30] | s_interrupts[29] | s_interrupts[28] | s_interrupts[27]
     | s_interrupts[26] | s_interrupts[25] | s_interrupts[24] | s_interrupts[23]; // @[CSR.scala 1176:90]
  wire  _any_T_155 = _any_T_140 | s_interrupts[22] | s_interrupts[21] | s_interrupts[20] | s_interrupts[19] |
    s_interrupts[18] | s_interrupts[17] | s_interrupts[16] | s_interrupts[15] | s_interrupts[14] | s_interrupts[13] |
    s_interrupts[12] | s_interrupts[11] | s_interrupts[3] | s_interrupts[7] | s_interrupts[9]; // @[CSR.scala 1176:90]
  wire  anyInterrupt = _any_T_155 | s_interrupts[1] | s_interrupts[5] | s_interrupts[8] | s_interrupts[0] | s_interrupts
    [4]; // @[CSR.scala 1176:90]
  wire [2:0] _which_T_81 = s_interrupts[0] ? 3'h0 : 3'h4; // @[Mux.scala 47:69]
  wire [3:0] _which_T_82 = s_interrupts[8] ? 4'h8 : {{1'd0}, _which_T_81}; // @[Mux.scala 47:69]
  wire [3:0] _which_T_83 = s_interrupts[5] ? 4'h5 : _which_T_82; // @[Mux.scala 47:69]
  wire [3:0] _which_T_84 = s_interrupts[1] ? 4'h1 : _which_T_83; // @[Mux.scala 47:69]
  wire [3:0] _which_T_85 = s_interrupts[9] ? 4'h9 : _which_T_84; // @[Mux.scala 47:69]
  wire [3:0] _which_T_86 = s_interrupts[7] ? 4'h7 : _which_T_85; // @[Mux.scala 47:69]
  wire [3:0] _which_T_87 = s_interrupts[3] ? 4'h3 : _which_T_86; // @[Mux.scala 47:69]
  wire [3:0] _which_T_88 = s_interrupts[11] ? 4'hb : _which_T_87; // @[Mux.scala 47:69]
  wire [3:0] _which_T_89 = s_interrupts[12] ? 4'hc : _which_T_88; // @[Mux.scala 47:69]
  wire [3:0] _which_T_90 = s_interrupts[13] ? 4'hd : _which_T_89; // @[Mux.scala 47:69]
  wire [3:0] _which_T_91 = s_interrupts[14] ? 4'he : _which_T_90; // @[Mux.scala 47:69]
  wire [3:0] _which_T_92 = s_interrupts[15] ? 4'hf : _which_T_91; // @[Mux.scala 47:69]
  wire [4:0] _which_T_93 = s_interrupts[16] ? 5'h10 : {{1'd0}, _which_T_92}; // @[Mux.scala 47:69]
  wire [4:0] _which_T_94 = s_interrupts[17] ? 5'h11 : _which_T_93; // @[Mux.scala 47:69]
  wire [4:0] _which_T_95 = s_interrupts[18] ? 5'h12 : _which_T_94; // @[Mux.scala 47:69]
  wire [4:0] _which_T_96 = s_interrupts[19] ? 5'h13 : _which_T_95; // @[Mux.scala 47:69]
  wire [4:0] _which_T_97 = s_interrupts[20] ? 5'h14 : _which_T_96; // @[Mux.scala 47:69]
  wire [4:0] _which_T_98 = s_interrupts[21] ? 5'h15 : _which_T_97; // @[Mux.scala 47:69]
  wire [4:0] _which_T_99 = s_interrupts[22] ? 5'h16 : _which_T_98; // @[Mux.scala 47:69]
  wire [4:0] _which_T_100 = s_interrupts[23] ? 5'h17 : _which_T_99; // @[Mux.scala 47:69]
  wire [4:0] _which_T_101 = s_interrupts[24] ? 5'h18 : _which_T_100; // @[Mux.scala 47:69]
  wire [4:0] _which_T_102 = s_interrupts[25] ? 5'h19 : _which_T_101; // @[Mux.scala 47:69]
  wire [4:0] _which_T_103 = s_interrupts[26] ? 5'h1a : _which_T_102; // @[Mux.scala 47:69]
  wire [4:0] _which_T_104 = s_interrupts[27] ? 5'h1b : _which_T_103; // @[Mux.scala 47:69]
  wire [4:0] _which_T_105 = s_interrupts[28] ? 5'h1c : _which_T_104; // @[Mux.scala 47:69]
  wire [4:0] _which_T_106 = s_interrupts[29] ? 5'h1d : _which_T_105; // @[Mux.scala 47:69]
  wire [4:0] _which_T_107 = s_interrupts[30] ? 5'h1e : _which_T_106; // @[Mux.scala 47:69]
  wire [4:0] _which_T_108 = s_interrupts[31] ? 5'h1f : _which_T_107; // @[Mux.scala 47:69]
  wire [4:0] _which_T_109 = m_interrupts[4] ? 5'h4 : _which_T_108; // @[Mux.scala 47:69]
  wire [4:0] _which_T_110 = m_interrupts[0] ? 5'h0 : _which_T_109; // @[Mux.scala 47:69]
  wire [4:0] _which_T_111 = m_interrupts[8] ? 5'h8 : _which_T_110; // @[Mux.scala 47:69]
  wire [4:0] _which_T_112 = m_interrupts[5] ? 5'h5 : _which_T_111; // @[Mux.scala 47:69]
  wire [4:0] _which_T_113 = m_interrupts[1] ? 5'h1 : _which_T_112; // @[Mux.scala 47:69]
  wire [4:0] _which_T_114 = m_interrupts[9] ? 5'h9 : _which_T_113; // @[Mux.scala 47:69]
  wire [4:0] _which_T_115 = m_interrupts[7] ? 5'h7 : _which_T_114; // @[Mux.scala 47:69]
  wire [4:0] _which_T_116 = m_interrupts[3] ? 5'h3 : _which_T_115; // @[Mux.scala 47:69]
  wire [4:0] _which_T_117 = m_interrupts[11] ? 5'hb : _which_T_116; // @[Mux.scala 47:69]
  wire [4:0] _which_T_118 = m_interrupts[12] ? 5'hc : _which_T_117; // @[Mux.scala 47:69]
  wire [4:0] _which_T_119 = m_interrupts[13] ? 5'hd : _which_T_118; // @[Mux.scala 47:69]
  wire [4:0] _which_T_120 = m_interrupts[14] ? 5'he : _which_T_119; // @[Mux.scala 47:69]
  wire [4:0] _which_T_121 = m_interrupts[15] ? 5'hf : _which_T_120; // @[Mux.scala 47:69]
  wire [4:0] _which_T_122 = m_interrupts[16] ? 5'h10 : _which_T_121; // @[Mux.scala 47:69]
  wire [4:0] _which_T_123 = m_interrupts[17] ? 5'h11 : _which_T_122; // @[Mux.scala 47:69]
  wire [4:0] _which_T_124 = m_interrupts[18] ? 5'h12 : _which_T_123; // @[Mux.scala 47:69]
  wire [4:0] _which_T_125 = m_interrupts[19] ? 5'h13 : _which_T_124; // @[Mux.scala 47:69]
  wire [4:0] _which_T_126 = m_interrupts[20] ? 5'h14 : _which_T_125; // @[Mux.scala 47:69]
  wire [4:0] _which_T_127 = m_interrupts[21] ? 5'h15 : _which_T_126; // @[Mux.scala 47:69]
  wire [4:0] _which_T_128 = m_interrupts[22] ? 5'h16 : _which_T_127; // @[Mux.scala 47:69]
  wire [4:0] _which_T_129 = m_interrupts[23] ? 5'h17 : _which_T_128; // @[Mux.scala 47:69]
  wire [4:0] _which_T_130 = m_interrupts[24] ? 5'h18 : _which_T_129; // @[Mux.scala 47:69]
  wire [4:0] _which_T_131 = m_interrupts[25] ? 5'h19 : _which_T_130; // @[Mux.scala 47:69]
  wire [4:0] _which_T_132 = m_interrupts[26] ? 5'h1a : _which_T_131; // @[Mux.scala 47:69]
  wire [4:0] _which_T_133 = m_interrupts[27] ? 5'h1b : _which_T_132; // @[Mux.scala 47:69]
  wire [4:0] _which_T_134 = m_interrupts[28] ? 5'h1c : _which_T_133; // @[Mux.scala 47:69]
  wire [4:0] _which_T_135 = m_interrupts[29] ? 5'h1d : _which_T_134; // @[Mux.scala 47:69]
  wire [4:0] _which_T_136 = m_interrupts[30] ? 5'h1e : _which_T_135; // @[Mux.scala 47:69]
  wire [4:0] _which_T_137 = m_interrupts[31] ? 5'h1f : _which_T_136; // @[Mux.scala 47:69]
  wire [4:0] _which_T_138 = nmi_interrupts[4] ? 5'h4 : _which_T_137; // @[Mux.scala 47:69]
  wire [4:0] _which_T_139 = nmi_interrupts[0] ? 5'h0 : _which_T_138; // @[Mux.scala 47:69]
  wire [4:0] _which_T_140 = nmi_interrupts[8] ? 5'h8 : _which_T_139; // @[Mux.scala 47:69]
  wire [4:0] _which_T_141 = nmi_interrupts[5] ? 5'h5 : _which_T_140; // @[Mux.scala 47:69]
  wire [4:0] _which_T_142 = nmi_interrupts[1] ? 5'h1 : _which_T_141; // @[Mux.scala 47:69]
  wire [4:0] _which_T_143 = nmi_interrupts[9] ? 5'h9 : _which_T_142; // @[Mux.scala 47:69]
  wire [4:0] _which_T_144 = nmi_interrupts[7] ? 5'h7 : _which_T_143; // @[Mux.scala 47:69]
  wire [4:0] _which_T_145 = nmi_interrupts[3] ? 5'h3 : _which_T_144; // @[Mux.scala 47:69]
  wire [4:0] _which_T_146 = nmi_interrupts[11] ? 5'hb : _which_T_145; // @[Mux.scala 47:69]
  wire [4:0] _which_T_147 = nmi_interrupts[12] ? 5'hc : _which_T_146; // @[Mux.scala 47:69]
  wire [4:0] _which_T_148 = nmi_interrupts[13] ? 5'hd : _which_T_147; // @[Mux.scala 47:69]
  wire [4:0] _which_T_149 = d_interrupts[4] ? 5'h4 : _which_T_148; // @[Mux.scala 47:69]
  wire [4:0] _which_T_150 = d_interrupts[0] ? 5'h0 : _which_T_149; // @[Mux.scala 47:69]
  wire [4:0] _which_T_151 = d_interrupts[8] ? 5'h8 : _which_T_150; // @[Mux.scala 47:69]
  wire [4:0] _which_T_152 = d_interrupts[5] ? 5'h5 : _which_T_151; // @[Mux.scala 47:69]
  wire [4:0] _which_T_153 = d_interrupts[1] ? 5'h1 : _which_T_152; // @[Mux.scala 47:69]
  wire [4:0] _which_T_154 = d_interrupts[9] ? 5'h9 : _which_T_153; // @[Mux.scala 47:69]
  wire [4:0] _which_T_155 = d_interrupts[7] ? 5'h7 : _which_T_154; // @[Mux.scala 47:69]
  wire [4:0] _which_T_156 = d_interrupts[3] ? 5'h3 : _which_T_155; // @[Mux.scala 47:69]
  wire [4:0] _which_T_157 = d_interrupts[11] ? 5'hb : _which_T_156; // @[Mux.scala 47:69]
  wire [4:0] _which_T_158 = d_interrupts[12] ? 5'hc : _which_T_157; // @[Mux.scala 47:69]
  wire [4:0] _which_T_159 = d_interrupts[13] ? 5'hd : _which_T_158; // @[Mux.scala 47:69]
  wire [4:0] whichInterrupt = d_interrupts[14] ? 5'he : _which_T_159; // @[Mux.scala 47:69]
  wire [62:0] _interruptCause_T = {nmiFlag, 62'h0}; // @[CSR.scala 467:54]
  wire [63:0] _GEN_116 = {{1'd0}, _interruptCause_T}; // @[CSR.scala 467:43]
  wire [63:0] _interruptCause_T_2 = 64'h8000000000000000 + _GEN_116; // @[CSR.scala 467:43]
  wire [63:0] _GEN_119 = {{59'd0}, whichInterrupt}; // @[CSR.scala 467:67]
  wire  _io_interrupt_T = ~io_singleStep; // @[CSR.scala 468:36]
  wire  pmp_mask_base_lo = reg_pmp_0_cfg_a[0]; // @[PMP.scala 59:31]
  wire [35:0] pmp_mask_base = {reg_pmp_0_addr,pmp_mask_base_lo}; // @[Cat.scala 30:58]
  wire [35:0] _pmp_mask_T_1 = pmp_mask_base + 36'h1; // @[PMP.scala 60:23]
  wire [35:0] _pmp_mask_T_2 = ~_pmp_mask_T_1; // @[PMP.scala 60:16]
  wire [35:0] pmp_mask_hi = pmp_mask_base & _pmp_mask_T_2; // @[PMP.scala 60:14]
  wire [37:0] _pmp_mask_T_3 = {pmp_mask_hi,2'h3}; // @[Cat.scala 30:58]
  wire  pmp_mask_base_lo_1 = reg_pmp_1_cfg_a[0]; // @[PMP.scala 59:31]
  wire [35:0] pmp_mask_base_1 = {reg_pmp_1_addr,pmp_mask_base_lo_1}; // @[Cat.scala 30:58]
  wire [35:0] _pmp_mask_T_5 = pmp_mask_base_1 + 36'h1; // @[PMP.scala 60:23]
  wire [35:0] _pmp_mask_T_6 = ~_pmp_mask_T_5; // @[PMP.scala 60:16]
  wire [35:0] pmp_mask_hi_1 = pmp_mask_base_1 & _pmp_mask_T_6; // @[PMP.scala 60:14]
  wire [37:0] _pmp_mask_T_7 = {pmp_mask_hi_1,2'h3}; // @[Cat.scala 30:58]
  wire  pmp_mask_base_lo_2 = reg_pmp_2_cfg_a[0]; // @[PMP.scala 59:31]
  wire [35:0] pmp_mask_base_2 = {reg_pmp_2_addr,pmp_mask_base_lo_2}; // @[Cat.scala 30:58]
  wire [35:0] _pmp_mask_T_9 = pmp_mask_base_2 + 36'h1; // @[PMP.scala 60:23]
  wire [35:0] _pmp_mask_T_10 = ~_pmp_mask_T_9; // @[PMP.scala 60:16]
  wire [35:0] pmp_mask_hi_2 = pmp_mask_base_2 & _pmp_mask_T_10; // @[PMP.scala 60:14]
  wire [37:0] _pmp_mask_T_11 = {pmp_mask_hi_2,2'h3}; // @[Cat.scala 30:58]
  wire  pmp_mask_base_lo_3 = reg_pmp_3_cfg_a[0]; // @[PMP.scala 59:31]
  wire [35:0] pmp_mask_base_3 = {reg_pmp_3_addr,pmp_mask_base_lo_3}; // @[Cat.scala 30:58]
  wire [35:0] _pmp_mask_T_13 = pmp_mask_base_3 + 36'h1; // @[PMP.scala 60:23]
  wire [35:0] _pmp_mask_T_14 = ~_pmp_mask_T_13; // @[PMP.scala 60:16]
  wire [35:0] pmp_mask_hi_3 = pmp_mask_base_3 & _pmp_mask_T_14; // @[PMP.scala 60:14]
  wire [37:0] _pmp_mask_T_15 = {pmp_mask_hi_3,2'h3}; // @[Cat.scala 30:58]
  wire  pmp_mask_base_lo_4 = reg_pmp_4_cfg_a[0]; // @[PMP.scala 59:31]
  wire [35:0] pmp_mask_base_4 = {reg_pmp_4_addr,pmp_mask_base_lo_4}; // @[Cat.scala 30:58]
  wire [35:0] _pmp_mask_T_17 = pmp_mask_base_4 + 36'h1; // @[PMP.scala 60:23]
  wire [35:0] _pmp_mask_T_18 = ~_pmp_mask_T_17; // @[PMP.scala 60:16]
  wire [35:0] pmp_mask_hi_4 = pmp_mask_base_4 & _pmp_mask_T_18; // @[PMP.scala 60:14]
  wire [37:0] _pmp_mask_T_19 = {pmp_mask_hi_4,2'h3}; // @[Cat.scala 30:58]
  wire  pmp_mask_base_lo_5 = reg_pmp_5_cfg_a[0]; // @[PMP.scala 59:31]
  wire [35:0] pmp_mask_base_5 = {reg_pmp_5_addr,pmp_mask_base_lo_5}; // @[Cat.scala 30:58]
  wire [35:0] _pmp_mask_T_21 = pmp_mask_base_5 + 36'h1; // @[PMP.scala 60:23]
  wire [35:0] _pmp_mask_T_22 = ~_pmp_mask_T_21; // @[PMP.scala 60:16]
  wire [35:0] pmp_mask_hi_5 = pmp_mask_base_5 & _pmp_mask_T_22; // @[PMP.scala 60:14]
  wire [37:0] _pmp_mask_T_23 = {pmp_mask_hi_5,2'h3}; // @[Cat.scala 30:58]
  wire  pmp_mask_base_lo_6 = reg_pmp_6_cfg_a[0]; // @[PMP.scala 59:31]
  wire [35:0] pmp_mask_base_6 = {reg_pmp_6_addr,pmp_mask_base_lo_6}; // @[Cat.scala 30:58]
  wire [35:0] _pmp_mask_T_25 = pmp_mask_base_6 + 36'h1; // @[PMP.scala 60:23]
  wire [35:0] _pmp_mask_T_26 = ~_pmp_mask_T_25; // @[PMP.scala 60:16]
  wire [35:0] pmp_mask_hi_6 = pmp_mask_base_6 & _pmp_mask_T_26; // @[PMP.scala 60:14]
  wire [37:0] _pmp_mask_T_27 = {pmp_mask_hi_6,2'h3}; // @[Cat.scala 30:58]
  wire  pmp_mask_base_lo_7 = reg_pmp_7_cfg_a[0]; // @[PMP.scala 59:31]
  wire [35:0] pmp_mask_base_7 = {reg_pmp_7_addr,pmp_mask_base_lo_7}; // @[Cat.scala 30:58]
  wire [35:0] _pmp_mask_T_29 = pmp_mask_base_7 + 36'h1; // @[PMP.scala 60:23]
  wire [35:0] _pmp_mask_T_30 = ~_pmp_mask_T_29; // @[PMP.scala 60:16]
  wire [35:0] pmp_mask_hi_7 = pmp_mask_base_7 & _pmp_mask_T_30; // @[PMP.scala 60:14]
  wire [37:0] _pmp_mask_T_31 = {pmp_mask_hi_7,2'h3}; // @[Cat.scala 30:58]
  reg [63:0] reg_misa; // @[CSR.scala 489:21]
  wire [6:0] read_mstatus_lo_lo = {io_status_hpie,io_status_spie,io_status_upie,io_status_mie,io_status_hie,
    io_status_sie,io_status_uie}; // @[CSR.scala 490:38]
  wire [18:0] read_mstatus_lo = {io_status_sum,io_status_mprv,io_status_xs,io_status_fs,io_status_mpp,io_status_vs,
    io_status_spp,io_status_mpie,read_mstatus_lo_lo}; // @[CSR.scala 490:38]
  wire [16:0] read_mstatus_hi_lo = {io_status_sxl,io_status_uxl,io_status_sd_rv32,io_status_zero1,io_status_tsr,
    io_status_tw,io_status_tvm,io_status_mxr}; // @[CSR.scala 490:38]
  wire [102:0] _read_mstatus_T = {io_status_debug,io_status_cease,io_status_wfi,io_status_isa,io_status_dprv,
    io_status_prv,io_status_sd,io_status_zero2,read_mstatus_hi_lo,read_mstatus_lo}; // @[CSR.scala 490:38]
  wire [63:0] read_mstatus = _read_mstatus_T[63:0]; // @[CSR.scala 490:40]
  wire [7:0] _read_mtvec_T_1 = reg_mtvec[0] ? 8'hfe : 8'h2; // @[CSR.scala 1205:39]
  wire [36:0] _read_mtvec_T_3 = {{29'd0}, _read_mtvec_T_1}; // @[package.scala 165:41]
  wire [36:0] _read_mtvec_T_4 = ~_read_mtvec_T_3; // @[package.scala 165:37]
  wire [36:0] read_mtvec_lo = reg_mtvec & _read_mtvec_T_4; // @[package.scala 165:35]
  wire [63:0] read_mtvec = {27'h0,read_mtvec_lo}; // @[Cat.scala 30:58]
  wire [7:0] _read_stvec_T_1 = reg_stvec[0] ? 8'hfe : 8'h2; // @[CSR.scala 1205:39]
  wire [38:0] _read_stvec_T_3 = {{31'd0}, _read_stvec_T_1}; // @[package.scala 165:41]
  wire [38:0] _read_stvec_T_4 = ~_read_stvec_T_3; // @[package.scala 165:37]
  wire [38:0] read_stvec_lo = reg_stvec & _read_stvec_T_4; // @[package.scala 165:35]
  wire [24:0] read_stvec_hi = read_stvec_lo[38] ? 25'h1ffffff : 25'h0; // @[Bitwise.scala 72:12]
  wire [63:0] read_stvec = {read_stvec_hi,read_stvec_lo}; // @[Cat.scala 30:58]
  wire  _GEN_35 = 4'h1 == reg_tselect ? reg_bp_1_control_dmode : reg_bp_0_control_dmode; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [2:0] _GEN_38 = 4'h1 == reg_tselect ? reg_bp_1_control_action : reg_bp_0_control_action; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_39 = 4'h1 == reg_tselect ? reg_bp_1_control_chain : reg_bp_0_control_chain; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [1:0] _GEN_41 = 4'h1 == reg_tselect ? reg_bp_1_control_tmatch : reg_bp_0_control_tmatch; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_42 = 4'h1 == reg_tselect ? reg_bp_1_control_m : reg_bp_0_control_m; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_44 = 4'h1 == reg_tselect ? reg_bp_1_control_s : reg_bp_0_control_s; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_45 = 4'h1 == reg_tselect ? reg_bp_1_control_u : reg_bp_0_control_u; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_46 = 4'h1 == reg_tselect ? reg_bp_1_control_x : reg_bp_0_control_x; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_47 = 4'h1 == reg_tselect ? reg_bp_1_control_w : reg_bp_0_control_w; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_48 = 4'h1 == reg_tselect ? reg_bp_1_control_r : reg_bp_0_control_r; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [38:0] _GEN_49 = 4'h1 == reg_tselect ? reg_bp_1_address : reg_bp_0_address; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_57 = 4'h2 == reg_tselect ? reg_bp_2_control_dmode : _GEN_35; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [2:0] _GEN_60 = 4'h2 == reg_tselect ? reg_bp_2_control_action : _GEN_38; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_61 = 4'h2 == reg_tselect ? reg_bp_2_control_chain : _GEN_39; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [1:0] _GEN_63 = 4'h2 == reg_tselect ? reg_bp_2_control_tmatch : _GEN_41; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_64 = 4'h2 == reg_tselect ? reg_bp_2_control_m : _GEN_42; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_66 = 4'h2 == reg_tselect ? reg_bp_2_control_s : _GEN_44; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_67 = 4'h2 == reg_tselect ? reg_bp_2_control_u : _GEN_45; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_68 = 4'h2 == reg_tselect ? reg_bp_2_control_x : _GEN_46; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_69 = 4'h2 == reg_tselect ? reg_bp_2_control_w : _GEN_47; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_70 = 4'h2 == reg_tselect ? reg_bp_2_control_r : _GEN_48; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [38:0] _GEN_71 = 4'h2 == reg_tselect ? reg_bp_2_address : _GEN_49; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_79 = 4'h3 == reg_tselect ? reg_bp_3_control_dmode : _GEN_57; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [2:0] _GEN_82 = 4'h3 == reg_tselect ? reg_bp_3_control_action : _GEN_60; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_83 = 4'h3 == reg_tselect ? reg_bp_3_control_chain : _GEN_61; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [1:0] _GEN_85 = 4'h3 == reg_tselect ? reg_bp_3_control_tmatch : _GEN_63; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_86 = 4'h3 == reg_tselect ? reg_bp_3_control_m : _GEN_64; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_88 = 4'h3 == reg_tselect ? reg_bp_3_control_s : _GEN_66; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_89 = 4'h3 == reg_tselect ? reg_bp_3_control_u : _GEN_67; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_90 = 4'h3 == reg_tselect ? reg_bp_3_control_x : _GEN_68; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_91 = 4'h3 == reg_tselect ? reg_bp_3_control_w : _GEN_69; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_92 = 4'h3 == reg_tselect ? reg_bp_3_control_r : _GEN_70; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [38:0] _GEN_93 = 4'h3 == reg_tselect ? reg_bp_3_address : _GEN_71; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_101 = 4'h4 == reg_tselect ? reg_bp_4_control_dmode : _GEN_79; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [2:0] _GEN_104 = 4'h4 == reg_tselect ? reg_bp_4_control_action : _GEN_82; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_105 = 4'h4 == reg_tselect ? reg_bp_4_control_chain : _GEN_83; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [1:0] _GEN_107 = 4'h4 == reg_tselect ? reg_bp_4_control_tmatch : _GEN_85; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_108 = 4'h4 == reg_tselect ? reg_bp_4_control_m : _GEN_86; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_110 = 4'h4 == reg_tselect ? reg_bp_4_control_s : _GEN_88; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_111 = 4'h4 == reg_tselect ? reg_bp_4_control_u : _GEN_89; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_112 = 4'h4 == reg_tselect ? reg_bp_4_control_x : _GEN_90; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_113 = 4'h4 == reg_tselect ? reg_bp_4_control_w : _GEN_91; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_114 = 4'h4 == reg_tselect ? reg_bp_4_control_r : _GEN_92; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [38:0] _GEN_115 = 4'h4 == reg_tselect ? reg_bp_4_address : _GEN_93; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_123 = 4'h5 == reg_tselect ? reg_bp_5_control_dmode : _GEN_101; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [2:0] _GEN_126 = 4'h5 == reg_tselect ? reg_bp_5_control_action : _GEN_104; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_127 = 4'h5 == reg_tselect ? reg_bp_5_control_chain : _GEN_105; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [1:0] _GEN_129 = 4'h5 == reg_tselect ? reg_bp_5_control_tmatch : _GEN_107; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_130 = 4'h5 == reg_tselect ? reg_bp_5_control_m : _GEN_108; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_132 = 4'h5 == reg_tselect ? reg_bp_5_control_s : _GEN_110; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_133 = 4'h5 == reg_tselect ? reg_bp_5_control_u : _GEN_111; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_134 = 4'h5 == reg_tselect ? reg_bp_5_control_x : _GEN_112; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_135 = 4'h5 == reg_tselect ? reg_bp_5_control_w : _GEN_113; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_136 = 4'h5 == reg_tselect ? reg_bp_5_control_r : _GEN_114; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [38:0] _GEN_137 = 4'h5 == reg_tselect ? reg_bp_5_address : _GEN_115; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_145 = 4'h6 == reg_tselect ? reg_bp_6_control_dmode : _GEN_123; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [2:0] _GEN_148 = 4'h6 == reg_tselect ? reg_bp_6_control_action : _GEN_126; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_149 = 4'h6 == reg_tselect ? reg_bp_6_control_chain : _GEN_127; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [1:0] _GEN_151 = 4'h6 == reg_tselect ? reg_bp_6_control_tmatch : _GEN_129; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_152 = 4'h6 == reg_tselect ? reg_bp_6_control_m : _GEN_130; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_154 = 4'h6 == reg_tselect ? reg_bp_6_control_s : _GEN_132; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_155 = 4'h6 == reg_tselect ? reg_bp_6_control_u : _GEN_133; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_156 = 4'h6 == reg_tselect ? reg_bp_6_control_x : _GEN_134; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_157 = 4'h6 == reg_tselect ? reg_bp_6_control_w : _GEN_135; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_158 = 4'h6 == reg_tselect ? reg_bp_6_control_r : _GEN_136; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [38:0] _GEN_159 = 4'h6 == reg_tselect ? reg_bp_6_address : _GEN_137; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_167 = 4'h7 == reg_tselect ? reg_bp_7_control_dmode : _GEN_145; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [2:0] _GEN_170 = 4'h7 == reg_tselect ? reg_bp_7_control_action : _GEN_148; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_171 = 4'h7 == reg_tselect ? reg_bp_7_control_chain : _GEN_149; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [1:0] _GEN_173 = 4'h7 == reg_tselect ? reg_bp_7_control_tmatch : _GEN_151; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_174 = 4'h7 == reg_tselect ? reg_bp_7_control_m : _GEN_152; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_176 = 4'h7 == reg_tselect ? reg_bp_7_control_s : _GEN_154; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_177 = 4'h7 == reg_tselect ? reg_bp_7_control_u : _GEN_155; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_178 = 4'h7 == reg_tselect ? reg_bp_7_control_x : _GEN_156; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_179 = 4'h7 == reg_tselect ? reg_bp_7_control_w : _GEN_157; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_180 = 4'h7 == reg_tselect ? reg_bp_7_control_r : _GEN_158; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [38:0] _GEN_181 = 4'h7 == reg_tselect ? reg_bp_7_address : _GEN_159; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_189 = 4'h8 == reg_tselect ? reg_bp_8_control_dmode : _GEN_167; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [2:0] _GEN_192 = 4'h8 == reg_tselect ? reg_bp_8_control_action : _GEN_170; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_193 = 4'h8 == reg_tselect ? reg_bp_8_control_chain : _GEN_171; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [1:0] _GEN_195 = 4'h8 == reg_tselect ? reg_bp_8_control_tmatch : _GEN_173; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_196 = 4'h8 == reg_tselect ? reg_bp_8_control_m : _GEN_174; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_198 = 4'h8 == reg_tselect ? reg_bp_8_control_s : _GEN_176; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_199 = 4'h8 == reg_tselect ? reg_bp_8_control_u : _GEN_177; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_200 = 4'h8 == reg_tselect ? reg_bp_8_control_x : _GEN_178; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_201 = 4'h8 == reg_tselect ? reg_bp_8_control_w : _GEN_179; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_202 = 4'h8 == reg_tselect ? reg_bp_8_control_r : _GEN_180; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [38:0] _GEN_203 = 4'h8 == reg_tselect ? reg_bp_8_address : _GEN_181; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_211 = 4'h9 == reg_tselect ? reg_bp_9_control_dmode : _GEN_189; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [2:0] _GEN_214 = 4'h9 == reg_tselect ? reg_bp_9_control_action : _GEN_192; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_215 = 4'h9 == reg_tselect ? reg_bp_9_control_chain : _GEN_193; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [1:0] _GEN_217 = 4'h9 == reg_tselect ? reg_bp_9_control_tmatch : _GEN_195; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_218 = 4'h9 == reg_tselect ? reg_bp_9_control_m : _GEN_196; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_220 = 4'h9 == reg_tselect ? reg_bp_9_control_s : _GEN_198; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_221 = 4'h9 == reg_tselect ? reg_bp_9_control_u : _GEN_199; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_222 = 4'h9 == reg_tselect ? reg_bp_9_control_x : _GEN_200; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_223 = 4'h9 == reg_tselect ? reg_bp_9_control_w : _GEN_201; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_224 = 4'h9 == reg_tselect ? reg_bp_9_control_r : _GEN_202; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [38:0] _GEN_225 = 4'h9 == reg_tselect ? reg_bp_9_address : _GEN_203; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_233 = 4'ha == reg_tselect ? reg_bp_10_control_dmode : _GEN_211; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [2:0] _GEN_236 = 4'ha == reg_tselect ? reg_bp_10_control_action : _GEN_214; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_237 = 4'ha == reg_tselect ? reg_bp_10_control_chain : _GEN_215; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [1:0] _GEN_239 = 4'ha == reg_tselect ? reg_bp_10_control_tmatch : _GEN_217; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_240 = 4'ha == reg_tselect ? reg_bp_10_control_m : _GEN_218; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_242 = 4'ha == reg_tselect ? reg_bp_10_control_s : _GEN_220; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_243 = 4'ha == reg_tselect ? reg_bp_10_control_u : _GEN_221; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_244 = 4'ha == reg_tselect ? reg_bp_10_control_x : _GEN_222; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_245 = 4'ha == reg_tselect ? reg_bp_10_control_w : _GEN_223; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_246 = 4'ha == reg_tselect ? reg_bp_10_control_r : _GEN_224; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [38:0] _GEN_247 = 4'ha == reg_tselect ? reg_bp_10_address : _GEN_225; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_255 = 4'hb == reg_tselect ? reg_bp_11_control_dmode : _GEN_233; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [2:0] _GEN_258 = 4'hb == reg_tselect ? reg_bp_11_control_action : _GEN_236; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_259 = 4'hb == reg_tselect ? reg_bp_11_control_chain : _GEN_237; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [1:0] _GEN_261 = 4'hb == reg_tselect ? reg_bp_11_control_tmatch : _GEN_239; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_262 = 4'hb == reg_tselect ? reg_bp_11_control_m : _GEN_240; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_264 = 4'hb == reg_tselect ? reg_bp_11_control_s : _GEN_242; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_265 = 4'hb == reg_tselect ? reg_bp_11_control_u : _GEN_243; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_266 = 4'hb == reg_tselect ? reg_bp_11_control_x : _GEN_244; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_267 = 4'hb == reg_tselect ? reg_bp_11_control_w : _GEN_245; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_268 = 4'hb == reg_tselect ? reg_bp_11_control_r : _GEN_246; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [38:0] _GEN_269 = 4'hb == reg_tselect ? reg_bp_11_address : _GEN_247; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_277 = 4'hc == reg_tselect ? reg_bp_12_control_dmode : _GEN_255; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [2:0] _GEN_280 = 4'hc == reg_tselect ? reg_bp_12_control_action : _GEN_258; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_281 = 4'hc == reg_tselect ? reg_bp_12_control_chain : _GEN_259; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [1:0] _GEN_283 = 4'hc == reg_tselect ? reg_bp_12_control_tmatch : _GEN_261; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_284 = 4'hc == reg_tselect ? reg_bp_12_control_m : _GEN_262; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_286 = 4'hc == reg_tselect ? reg_bp_12_control_s : _GEN_264; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_287 = 4'hc == reg_tselect ? reg_bp_12_control_u : _GEN_265; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_288 = 4'hc == reg_tselect ? reg_bp_12_control_x : _GEN_266; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_289 = 4'hc == reg_tselect ? reg_bp_12_control_w : _GEN_267; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_290 = 4'hc == reg_tselect ? reg_bp_12_control_r : _GEN_268; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [38:0] _GEN_291 = 4'hc == reg_tselect ? reg_bp_12_address : _GEN_269; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_299 = 4'hd == reg_tselect ? reg_bp_13_control_dmode : _GEN_277; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [2:0] _GEN_302 = 4'hd == reg_tselect ? reg_bp_13_control_action : _GEN_280; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_303 = 4'hd == reg_tselect ? reg_bp_13_control_chain : _GEN_281; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [1:0] _GEN_305 = 4'hd == reg_tselect ? reg_bp_13_control_tmatch : _GEN_283; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_306 = 4'hd == reg_tselect ? reg_bp_13_control_m : _GEN_284; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_308 = 4'hd == reg_tselect ? reg_bp_13_control_s : _GEN_286; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_309 = 4'hd == reg_tselect ? reg_bp_13_control_u : _GEN_287; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_310 = 4'hd == reg_tselect ? reg_bp_13_control_x : _GEN_288; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_311 = 4'hd == reg_tselect ? reg_bp_13_control_w : _GEN_289; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_312 = 4'hd == reg_tselect ? reg_bp_13_control_r : _GEN_290; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [38:0] _GEN_313 = 4'hd == reg_tselect ? reg_bp_13_address : _GEN_291; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_321 = 4'he == reg_tselect ? reg_bp_14_control_dmode : _GEN_299; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [2:0] _GEN_324 = 4'he == reg_tselect ? reg_bp_14_control_action : _GEN_302; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_325 = 4'he == reg_tselect ? reg_bp_14_control_chain : _GEN_303; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [1:0] _GEN_327 = 4'he == reg_tselect ? reg_bp_14_control_tmatch : _GEN_305; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_328 = 4'he == reg_tselect ? reg_bp_14_control_m : _GEN_306; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_330 = 4'he == reg_tselect ? reg_bp_14_control_s : _GEN_308; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_331 = 4'he == reg_tselect ? reg_bp_14_control_u : _GEN_309; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_332 = 4'he == reg_tselect ? reg_bp_14_control_x : _GEN_310; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_333 = 4'he == reg_tselect ? reg_bp_14_control_w : _GEN_311; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_334 = 4'he == reg_tselect ? reg_bp_14_control_r : _GEN_312; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [38:0] _GEN_335 = 4'he == reg_tselect ? reg_bp_14_address : _GEN_313; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_343 = 4'hf == reg_tselect ? reg_bp_15_control_dmode : _GEN_321; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [2:0] _GEN_346 = 4'hf == reg_tselect ? reg_bp_15_control_action : _GEN_324; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_347 = 4'hf == reg_tselect ? 1'h0 : _GEN_325; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [1:0] _GEN_349 = 4'hf == reg_tselect ? reg_bp_15_control_tmatch : _GEN_327; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_350 = 4'hf == reg_tselect ? reg_bp_15_control_m : _GEN_328; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_352 = 4'hf == reg_tselect ? reg_bp_15_control_s : _GEN_330; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_353 = 4'hf == reg_tselect ? reg_bp_15_control_u : _GEN_331; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_354 = 4'hf == reg_tselect ? reg_bp_15_control_x : _GEN_332; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_355 = 4'hf == reg_tselect ? reg_bp_15_control_w : _GEN_333; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire  _GEN_356 = 4'hf == reg_tselect ? reg_bp_15_control_r : _GEN_334; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [38:0] _GEN_357 = 4'hf == reg_tselect ? reg_bp_15_address : _GEN_335; // @[CSR.scala 496:48 CSR.scala 496:48]
  wire [6:0] lo_2 = {_GEN_350,1'h0,_GEN_352,_GEN_353,_GEN_354,_GEN_355,_GEN_356}; // @[CSR.scala 496:48]
  wire [63:0] _T_21 = {4'h2,_GEN_343,44'h10000000000,_GEN_346,_GEN_347,2'h0,_GEN_349,lo_2}; // @[CSR.scala 496:48]
  wire [24:0] hi_3 = _GEN_357[38] ? 25'h1ffffff : 25'h0; // @[Bitwise.scala 72:12]
  wire [63:0] _T_24 = {hi_3,_GEN_357}; // @[Cat.scala 30:58]
  wire [39:0] _T_26 = ~reg_mepc; // @[CSR.scala 1204:28]
  wire [1:0] _T_28 = reg_misa[2] ? 2'h1 : 2'h3; // @[CSR.scala 1204:36]
  wire [39:0] _GEN_138 = {{38'd0}, _T_28}; // @[CSR.scala 1204:31]
  wire [39:0] _T_29 = _T_26 | _GEN_138; // @[CSR.scala 1204:31]
  wire [39:0] lo_4 = ~_T_29; // @[CSR.scala 1204:26]
  wire [23:0] hi_5 = lo_4[39] ? 24'hffffff : 24'h0; // @[Bitwise.scala 72:12]
  wire [63:0] _T_32 = {hi_5,lo_4}; // @[Cat.scala 30:58]
  wire [23:0] hi_6 = reg_mtval[39] ? 24'hffffff : 24'h0; // @[Bitwise.scala 72:12]
  wire [63:0] _T_35 = {hi_6,reg_mtval}; // @[Cat.scala 30:58]
  wire [11:0] lo_5 = {reg_dcsr_zero2,reg_dcsr_stopcycle,reg_dcsr_stoptime,reg_dcsr_cause,reg_dcsr_zero1,reg_dcsr_step,
    reg_dcsr_prv}; // @[CSR.scala 511:27]
  wire [31:0] _T_36 = {reg_dcsr_xdebugver,reg_dcsr_zero4,reg_dcsr_zero3,reg_dcsr_ebreakm,reg_dcsr_ebreakh,
    reg_dcsr_ebreaks,reg_dcsr_ebreaku,lo_5}; // @[CSR.scala 511:27]
  wire [39:0] _T_37 = ~reg_dpc; // @[CSR.scala 1204:28]
  wire [39:0] _T_40 = _T_37 | _GEN_138; // @[CSR.scala 1204:31]
  wire [39:0] lo_6 = ~_T_40; // @[CSR.scala 1204:26]
  wire [23:0] hi_8 = lo_6[39] ? 24'hffffff : 24'h0; // @[Bitwise.scala 72:12]
  wire [63:0] _T_43 = {hi_8,lo_6}; // @[Cat.scala 30:58]
  wire [39:0] _T_44 = ~reg_mnepc; // @[CSR.scala 1204:28]
  wire [39:0] _T_47 = _T_44 | _GEN_138; // @[CSR.scala 1204:31]
  wire [39:0] lo_7 = ~_T_47; // @[CSR.scala 1204:26]
  wire [23:0] hi_9 = lo_7[39] ? 24'hffffff : 24'h0; // @[Bitwise.scala 72:12]
  wire [63:0] _T_50 = {hi_9,lo_7}; // @[Cat.scala 30:58]
  wire [102:0] _T_51 = {84'h0,6'h0,reg_mnstatus_mpp,2'h0,2'h0,2'h0,1'h0,nmie,3'h0}; // @[CSR.scala 523:36]
  wire [7:0] read_fcsr = {reg_frm,reg_fflags}; // @[Cat.scala 30:58]
  wire [63:0] read_sie = reg_mie & read_mideleg; // @[CSR.scala 584:28]
  wire [63:0] read_sip = _GEN_97 & read_mideleg; // @[CSR.scala 585:29]
  wire [6:0] lo_lo_5 = {1'h0,io_status_spie,2'h0,1'h0,io_status_sie,1'h0}; // @[CSR.scala 599:57]
  wire [18:0] lo_9 = {io_status_sum,1'h0,io_status_xs,io_status_fs,2'h0,io_status_vs,io_status_spp,1'h0,lo_lo_5}; // @[CSR.scala 599:57]
  wire [16:0] hi_lo_5 = {2'h0,io_status_uxl,io_status_sd_rv32,8'h0,2'h0,1'h0,io_status_mxr}; // @[CSR.scala 599:57]
  wire [102:0] _T_52 = {35'h0,4'h0,io_status_sd,27'h0,hi_lo_5,lo_9}; // @[CSR.scala 599:57]
  wire [23:0] hi_12 = reg_stval[39] ? 24'hffffff : 24'h0; // @[Bitwise.scala 72:12]
  wire [63:0] _T_56 = {hi_12,reg_stval}; // @[Cat.scala 30:58]
  wire [63:0] _T_57 = {reg_satp_mode,16'h0,reg_satp_ppn}; // @[CSR.scala 605:43]
  wire [39:0] _T_58 = ~reg_sepc; // @[CSR.scala 1204:28]
  wire [39:0] _T_61 = _T_58 | _GEN_138; // @[CSR.scala 1204:31]
  wire [39:0] lo_10 = ~_T_61; // @[CSR.scala 1204:26]
  wire [23:0] hi_14 = lo_10[39] ? 24'hffffff : 24'h0; // @[Bitwise.scala 72:12]
  wire [63:0] _T_64 = {hi_14,lo_10}; // @[Cat.scala 30:58]
  wire [7:0] lo_lo_lo_4 = {reg_pmp_0_cfg_l,2'h0,reg_pmp_0_cfg_a,reg_pmp_0_cfg_x,reg_pmp_0_cfg_w,reg_pmp_0_cfg_r}; // @[package.scala 36:38]
  wire [7:0] lo_hi_lo_6 = {reg_pmp_2_cfg_l,2'h0,reg_pmp_2_cfg_a,reg_pmp_2_cfg_x,reg_pmp_2_cfg_w,reg_pmp_2_cfg_r}; // @[package.scala 36:38]
  wire [7:0] hi_lo_lo_5 = {reg_pmp_4_cfg_l,2'h0,reg_pmp_4_cfg_a,reg_pmp_4_cfg_x,reg_pmp_4_cfg_w,reg_pmp_4_cfg_r}; // @[package.scala 36:38]
  wire [7:0] hi_hi_lo_6 = {reg_pmp_6_cfg_l,2'h0,reg_pmp_6_cfg_a,reg_pmp_6_cfg_x,reg_pmp_6_cfg_w,reg_pmp_6_cfg_r}; // @[package.scala 36:38]
  wire [15:0] lo_lo_6 = {reg_pmp_1_cfg_l,2'h0,reg_pmp_1_cfg_a,reg_pmp_1_cfg_x,reg_pmp_1_cfg_w,reg_pmp_1_cfg_r,lo_lo_lo_4
    }; // @[Cat.scala 30:58]
  wire [31:0] lo_19 = {reg_pmp_3_cfg_l,2'h0,reg_pmp_3_cfg_a,reg_pmp_3_cfg_x,reg_pmp_3_cfg_w,reg_pmp_3_cfg_r,lo_hi_lo_6,
    lo_lo_6}; // @[Cat.scala 30:58]
  wire [15:0] hi_lo_6 = {reg_pmp_5_cfg_l,2'h0,reg_pmp_5_cfg_a,reg_pmp_5_cfg_x,reg_pmp_5_cfg_w,reg_pmp_5_cfg_r,hi_lo_lo_5
    }; // @[Cat.scala 30:58]
  wire [63:0] _T_65 = {reg_pmp_7_cfg_l,2'h0,reg_pmp_7_cfg_a,reg_pmp_7_cfg_x,reg_pmp_7_cfg_w,reg_pmp_7_cfg_r,hi_hi_lo_6,
    hi_lo_6,lo_19}; // @[Cat.scala 30:58]
  reg [63:0] reg_custom_0; // @[CSR.scala 628:43]
  reg [63:0] reg_custom_1; // @[CSR.scala 628:43]
  wire  _T_67 = io_rw_addr == 12'h7a0; // @[CSR.scala 636:73]
  wire  _T_68 = io_rw_addr == 12'h7a1; // @[CSR.scala 636:73]
  wire  _T_69 = io_rw_addr == 12'h7a2; // @[CSR.scala 636:73]
  wire  _T_71 = io_rw_addr == 12'h301; // @[CSR.scala 636:73]
  wire  _T_72 = io_rw_addr == 12'h300; // @[CSR.scala 636:73]
  wire  _T_73 = io_rw_addr == 12'h305; // @[CSR.scala 636:73]
  wire  _T_74 = io_rw_addr == 12'h344; // @[CSR.scala 636:73]
  wire  _T_75 = io_rw_addr == 12'h304; // @[CSR.scala 636:73]
  wire  _T_76 = io_rw_addr == 12'h340; // @[CSR.scala 636:73]
  wire  _T_77 = io_rw_addr == 12'h341; // @[CSR.scala 636:73]
  wire  _T_78 = io_rw_addr == 12'h343; // @[CSR.scala 636:73]
  wire  _T_79 = io_rw_addr == 12'h342; // @[CSR.scala 636:73]
  wire  _T_80 = io_rw_addr == 12'hf14; // @[CSR.scala 636:73]
  wire  _T_81 = io_rw_addr == 12'h7b0; // @[CSR.scala 636:73]
  wire  _T_82 = io_rw_addr == 12'h7b1; // @[CSR.scala 636:73]
  wire  _T_83 = io_rw_addr == 12'h7b2; // @[CSR.scala 636:73]
  wire  _T_84 = io_rw_addr == 12'h350; // @[CSR.scala 636:73]
  wire  _T_85 = io_rw_addr == 12'h351; // @[CSR.scala 636:73]
  wire  _T_86 = io_rw_addr == 12'h352; // @[CSR.scala 636:73]
  wire  _T_87 = io_rw_addr == 12'h353; // @[CSR.scala 636:73]
  wire  _T_88 = io_rw_addr == 12'h1; // @[CSR.scala 636:73]
  wire  _T_89 = io_rw_addr == 12'h2; // @[CSR.scala 636:73]
  wire  _T_90 = io_rw_addr == 12'h3; // @[CSR.scala 636:73]
  wire  _T_91 = io_rw_addr == 12'h320; // @[CSR.scala 636:73]
  wire  _T_92 = io_rw_addr == 12'hb00; // @[CSR.scala 636:73]
  wire  _T_93 = io_rw_addr == 12'hb02; // @[CSR.scala 636:73]
  wire  _T_94 = io_rw_addr == 12'h323; // @[CSR.scala 636:73]
  wire  _T_95 = io_rw_addr == 12'hb03; // @[CSR.scala 636:73]
  wire  _T_96 = io_rw_addr == 12'hc03; // @[CSR.scala 636:73]
  wire  _T_97 = io_rw_addr == 12'h324; // @[CSR.scala 636:73]
  wire  _T_98 = io_rw_addr == 12'hb04; // @[CSR.scala 636:73]
  wire  _T_99 = io_rw_addr == 12'hc04; // @[CSR.scala 636:73]
  wire  _T_100 = io_rw_addr == 12'h325; // @[CSR.scala 636:73]
  wire  _T_101 = io_rw_addr == 12'hb05; // @[CSR.scala 636:73]
  wire  _T_102 = io_rw_addr == 12'hc05; // @[CSR.scala 636:73]
  wire  _T_103 = io_rw_addr == 12'h326; // @[CSR.scala 636:73]
  wire  _T_104 = io_rw_addr == 12'hb06; // @[CSR.scala 636:73]
  wire  _T_105 = io_rw_addr == 12'hc06; // @[CSR.scala 636:73]
  wire  _T_181 = io_rw_addr == 12'h306; // @[CSR.scala 636:73]
  wire  _T_182 = io_rw_addr == 12'hc00; // @[CSR.scala 636:73]
  wire  _T_183 = io_rw_addr == 12'hc02; // @[CSR.scala 636:73]
  wire  _T_184 = io_rw_addr == 12'h100; // @[CSR.scala 636:73]
  wire  _T_185 = io_rw_addr == 12'h144; // @[CSR.scala 636:73]
  wire  _T_186 = io_rw_addr == 12'h104; // @[CSR.scala 636:73]
  wire  _T_187 = io_rw_addr == 12'h140; // @[CSR.scala 636:73]
  wire  _T_188 = io_rw_addr == 12'h142; // @[CSR.scala 636:73]
  wire  _T_189 = io_rw_addr == 12'h143; // @[CSR.scala 636:73]
  wire  _T_190 = io_rw_addr == 12'h180; // @[CSR.scala 636:73]
  wire  _T_191 = io_rw_addr == 12'h141; // @[CSR.scala 636:73]
  wire  _T_192 = io_rw_addr == 12'h105; // @[CSR.scala 636:73]
  wire  _T_193 = io_rw_addr == 12'h106; // @[CSR.scala 636:73]
  wire  _T_194 = io_rw_addr == 12'h303; // @[CSR.scala 636:73]
  wire  _T_195 = io_rw_addr == 12'h302; // @[CSR.scala 636:73]
  wire  _T_196 = io_rw_addr == 12'h3a0; // @[CSR.scala 636:73]
  wire  _T_198 = io_rw_addr == 12'h3b0; // @[CSR.scala 636:73]
  wire  _T_199 = io_rw_addr == 12'h3b1; // @[CSR.scala 636:73]
  wire  _T_200 = io_rw_addr == 12'h3b2; // @[CSR.scala 636:73]
  wire  _T_201 = io_rw_addr == 12'h3b3; // @[CSR.scala 636:73]
  wire  _T_202 = io_rw_addr == 12'h3b4; // @[CSR.scala 636:73]
  wire  _T_203 = io_rw_addr == 12'h3b5; // @[CSR.scala 636:73]
  wire  _T_204 = io_rw_addr == 12'h3b6; // @[CSR.scala 636:73]
  wire  _T_205 = io_rw_addr == 12'h3b7; // @[CSR.scala 636:73]
  wire  _T_214 = io_rw_addr == 12'h7c0; // @[CSR.scala 636:73]
  wire  _T_215 = io_rw_addr == 12'h7c1; // @[CSR.scala 636:73]
  wire  _T_216 = io_rw_addr == 12'hf12; // @[CSR.scala 636:73]
  wire  _T_217 = io_rw_addr == 12'hf11; // @[CSR.scala 636:73]
  wire  _T_218 = io_rw_addr == 12'hf13; // @[CSR.scala 636:73]
  wire [63:0] _wdata_T_1 = io_rw_cmd[1] ? io_rw_rdata : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _wdata_T_2 = _wdata_T_1 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _wdata_T_5 = &io_rw_cmd[1:0] ? io_rw_wdata : 64'h0; // @[CSR.scala 1182:49]
  wire [63:0] _wdata_T_6 = ~_wdata_T_5; // @[CSR.scala 1182:45]
  wire [63:0] wdata = _wdata_T_2 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire [31:0] _T_232 = _T_219 & 32'ha0200000; // @[Decode.scala 14:65]
  wire  _T_233 = _T_232 == 32'h20000000; // @[Decode.scala 14:121]
  wire [31:0] _T_235 = _T_219 & 32'h32200000; // @[Decode.scala 14:65]
  wire  _T_236 = _T_235 == 32'h10000000; // @[Decode.scala 14:121]
  wire  insn_cease = system_insn & _T_233; // @[CSR.scala 652:95]
  wire  insn_wfi = system_insn & _T_236; // @[CSR.scala 652:95]
  wire [31:0] _T_247 = {io_decode_0_csr, 20'h0}; // @[CSR.scala 659:30]
  wire [31:0] _T_254 = _T_247 & 32'h92400000; // @[Decode.scala 14:65]
  wire  _T_255 = _T_254 == 32'h10000000; // @[Decode.scala 14:121]
  wire [31:0] _T_256 = _T_247 & 32'ha0400000; // @[Decode.scala 14:65]
  wire  _T_257 = _T_256 == 32'h20000000; // @[Decode.scala 14:121]
  wire  is_ret = _T_255 | _T_257; // @[Decode.scala 15:30]
  wire [31:0] _T_263 = _T_247 & 32'h32200000; // @[Decode.scala 14:65]
  wire  is_wfi = _T_263 == 32'h10000000; // @[Decode.scala 14:121]
  wire [31:0] _T_266 = _T_247 & 32'h42000000; // @[Decode.scala 14:65]
  wire  is_sfence = _T_266 == 32'h2000000; // @[Decode.scala 14:121]
  wire  _allow_wfi_T = reg_mstatus_prv > 2'h1; // @[CSR.scala 661:63]
  wire  allow_wfi = reg_mstatus_prv > 2'h1 | ~reg_mstatus_tw; // @[CSR.scala 661:71]
  wire  allow_sfence_vma = _allow_wfi_T | ~reg_mstatus_tvm; // @[CSR.scala 662:70]
  wire  allow_sret = _allow_wfi_T | ~reg_mstatus_tsr; // @[CSR.scala 663:72]
  wire [4:0] counter_addr = io_decode_0_csr[4:0]; // @[CSR.scala 664:34]
  wire [31:0] _allow_counter_T_1 = read_mcounteren >> counter_addr; // @[CSR.scala 665:68]
  wire [31:0] _allow_counter_T_6 = read_scounteren >> counter_addr; // @[CSR.scala 666:71]
  wire  _allow_counter_T_8 = reg_mstatus_prv >= 2'h1 | _allow_counter_T_6[0]; // @[CSR.scala 666:53]
  wire  allow_counter = (_allow_wfi_T | _allow_counter_T_1[0]) & _allow_counter_T_8; // @[CSR.scala 665:84]
  wire [11:0] _io_decode_0_fp_csr_T = io_decode_0_csr & 12'h900; // @[Decode.scala 14:65]
  wire  _io_decode_0_read_illegal_T_1 = reg_mstatus_prv < io_decode_0_csr[9:8]; // @[CSR.scala 671:44]
  wire  _io_decode_0_read_illegal_T_17 = io_decode_0_csr == 12'h7b1; // @[CSR.scala 655:99]
  wire  _io_decode_0_read_illegal_T_125 = io_decode_0_csr == 12'h180; // @[CSR.scala 655:99]
  wire  _io_decode_0_read_illegal_T_168 = io_decode_0_csr == 12'h7a0 | io_decode_0_csr == 12'h7a1 | io_decode_0_csr == 12'h7a2
     | io_decode_0_csr == 12'h7a3 | io_decode_0_csr == 12'h301 | io_decode_0_csr == 12'h300 | io_decode_0_csr == 12'h305
     | io_decode_0_csr == 12'h344 | io_decode_0_csr == 12'h304 | io_decode_0_csr == 12'h340 | io_decode_0_csr == 12'h341
     | io_decode_0_csr == 12'h343 | io_decode_0_csr == 12'h342 | io_decode_0_csr == 12'hf14 | io_decode_0_csr == 12'h7b0
     | _io_decode_0_read_illegal_T_17; // @[CSR.scala 655:115]
  wire  _io_decode_0_read_illegal_T_183 = _io_decode_0_read_illegal_T_168 | io_decode_0_csr == 12'h7b2 | io_decode_0_csr
     == 12'h350 | io_decode_0_csr == 12'h351 | io_decode_0_csr == 12'h352 | io_decode_0_csr == 12'h353 | io_decode_0_csr
     == 12'h1 | io_decode_0_csr == 12'h2 | io_decode_0_csr == 12'h3 | io_decode_0_csr == 12'h320 | io_decode_0_csr == 12'hb00
     | io_decode_0_csr == 12'hb02 | io_decode_0_csr == 12'h323 | io_decode_0_csr == 12'hb03 | io_decode_0_csr == 12'hc03
     | io_decode_0_csr == 12'h324; // @[CSR.scala 655:115]
  wire  _io_decode_0_read_illegal_T_198 = _io_decode_0_read_illegal_T_183 | io_decode_0_csr == 12'hb04 | io_decode_0_csr
     == 12'hc04 | io_decode_0_csr == 12'h325 | io_decode_0_csr == 12'hb05 | io_decode_0_csr == 12'hc05 | io_decode_0_csr
     == 12'h326 | io_decode_0_csr == 12'hb06 | io_decode_0_csr == 12'hc06 | io_decode_0_csr == 12'h327 | io_decode_0_csr
     == 12'hb07 | io_decode_0_csr == 12'hc07 | io_decode_0_csr == 12'h328 | io_decode_0_csr == 12'hb08 | io_decode_0_csr
     == 12'hc08 | io_decode_0_csr == 12'h329; // @[CSR.scala 655:115]
  wire  _io_decode_0_read_illegal_T_213 = _io_decode_0_read_illegal_T_198 | io_decode_0_csr == 12'hb09 | io_decode_0_csr
     == 12'hc09 | io_decode_0_csr == 12'h32a | io_decode_0_csr == 12'hb0a | io_decode_0_csr == 12'hc0a | io_decode_0_csr
     == 12'h32b | io_decode_0_csr == 12'hb0b | io_decode_0_csr == 12'hc0b | io_decode_0_csr == 12'h32c | io_decode_0_csr
     == 12'hb0c | io_decode_0_csr == 12'hc0c | io_decode_0_csr == 12'h32d | io_decode_0_csr == 12'hb0d | io_decode_0_csr
     == 12'hc0d | io_decode_0_csr == 12'h32e; // @[CSR.scala 655:115]
  wire  _io_decode_0_read_illegal_T_228 = _io_decode_0_read_illegal_T_213 | io_decode_0_csr == 12'hb0e | io_decode_0_csr
     == 12'hc0e | io_decode_0_csr == 12'h32f | io_decode_0_csr == 12'hb0f | io_decode_0_csr == 12'hc0f | io_decode_0_csr
     == 12'h330 | io_decode_0_csr == 12'hb10 | io_decode_0_csr == 12'hc10 | io_decode_0_csr == 12'h331 | io_decode_0_csr
     == 12'hb11 | io_decode_0_csr == 12'hc11 | io_decode_0_csr == 12'h332 | io_decode_0_csr == 12'hb12 | io_decode_0_csr
     == 12'hc12 | io_decode_0_csr == 12'h333; // @[CSR.scala 655:115]
  wire  _io_decode_0_read_illegal_T_243 = _io_decode_0_read_illegal_T_228 | io_decode_0_csr == 12'hb13 | io_decode_0_csr
     == 12'hc13 | io_decode_0_csr == 12'h334 | io_decode_0_csr == 12'hb14 | io_decode_0_csr == 12'hc14 | io_decode_0_csr
     == 12'h335 | io_decode_0_csr == 12'hb15 | io_decode_0_csr == 12'hc15 | io_decode_0_csr == 12'h336 | io_decode_0_csr
     == 12'hb16 | io_decode_0_csr == 12'hc16 | io_decode_0_csr == 12'h337 | io_decode_0_csr == 12'hb17 | io_decode_0_csr
     == 12'hc17 | io_decode_0_csr == 12'h338; // @[CSR.scala 655:115]
  wire  _io_decode_0_read_illegal_T_258 = _io_decode_0_read_illegal_T_243 | io_decode_0_csr == 12'hb18 | io_decode_0_csr
     == 12'hc18 | io_decode_0_csr == 12'h339 | io_decode_0_csr == 12'hb19 | io_decode_0_csr == 12'hc19 | io_decode_0_csr
     == 12'h33a | io_decode_0_csr == 12'hb1a | io_decode_0_csr == 12'hc1a | io_decode_0_csr == 12'h33b | io_decode_0_csr
     == 12'hb1b | io_decode_0_csr == 12'hc1b | io_decode_0_csr == 12'h33c | io_decode_0_csr == 12'hb1c | io_decode_0_csr
     == 12'hc1c | io_decode_0_csr == 12'h33d; // @[CSR.scala 655:115]
  wire  _io_decode_0_read_illegal_T_273 = _io_decode_0_read_illegal_T_258 | io_decode_0_csr == 12'hb1d | io_decode_0_csr
     == 12'hc1d | io_decode_0_csr == 12'h33e | io_decode_0_csr == 12'hb1e | io_decode_0_csr == 12'hc1e | io_decode_0_csr
     == 12'h33f | io_decode_0_csr == 12'hb1f | io_decode_0_csr == 12'hc1f | io_decode_0_csr == 12'h306 | io_decode_0_csr
     == 12'hc00 | io_decode_0_csr == 12'hc02 | io_decode_0_csr == 12'h100 | io_decode_0_csr == 12'h144 | io_decode_0_csr
     == 12'h104 | io_decode_0_csr == 12'h140; // @[CSR.scala 655:115]
  wire  _io_decode_0_read_illegal_T_288 = _io_decode_0_read_illegal_T_273 | io_decode_0_csr == 12'h142 | io_decode_0_csr
     == 12'h143 | io_decode_0_csr == 12'h180 | io_decode_0_csr == 12'h141 | io_decode_0_csr == 12'h105 | io_decode_0_csr
     == 12'h106 | io_decode_0_csr == 12'h303 | io_decode_0_csr == 12'h302 | io_decode_0_csr == 12'h3a0 | io_decode_0_csr
     == 12'h3a2 | io_decode_0_csr == 12'h3b0 | io_decode_0_csr == 12'h3b1 | io_decode_0_csr == 12'h3b2 | io_decode_0_csr
     == 12'h3b3 | io_decode_0_csr == 12'h3b4; // @[CSR.scala 655:115]
  wire  _io_decode_0_read_illegal_T_303 = _io_decode_0_read_illegal_T_288 | io_decode_0_csr == 12'h3b5 | io_decode_0_csr
     == 12'h3b6 | io_decode_0_csr == 12'h3b7 | io_decode_0_csr == 12'h3b8 | io_decode_0_csr == 12'h3b9 | io_decode_0_csr
     == 12'h3ba | io_decode_0_csr == 12'h3bb | io_decode_0_csr == 12'h3bc | io_decode_0_csr == 12'h3bd | io_decode_0_csr
     == 12'h3be | io_decode_0_csr == 12'h3bf | io_decode_0_csr == 12'h7c0 | io_decode_0_csr == 12'h7c1 | io_decode_0_csr
     == 12'hf12 | io_decode_0_csr == 12'hf11; // @[CSR.scala 655:115]
  wire  _io_decode_0_read_illegal_T_304 = _io_decode_0_read_illegal_T_303 | io_decode_0_csr == 12'hf13; // @[CSR.scala 655:115]
  wire  _io_decode_0_read_illegal_T_305 = ~_io_decode_0_read_illegal_T_304; // @[CSR.scala 672:7]
  wire  _io_decode_0_read_illegal_T_306 = reg_mstatus_prv < io_decode_0_csr[9:8] | _io_decode_0_read_illegal_T_305; // @[CSR.scala 671:62]
  wire  _io_decode_0_read_illegal_T_308 = ~allow_sfence_vma; // @[CSR.scala 673:35]
  wire  _io_decode_0_read_illegal_T_309 = _io_decode_0_read_illegal_T_125 & ~allow_sfence_vma; // @[CSR.scala 673:32]
  wire  _io_decode_0_read_illegal_T_310 = _io_decode_0_read_illegal_T_306 | _io_decode_0_read_illegal_T_309; // @[CSR.scala 672:32]
  wire  _io_decode_0_read_illegal_T_313 = io_decode_0_csr >= 12'hc00 & io_decode_0_csr < 12'hc20; // @[package.scala 204:55]
  wire  _io_decode_0_read_illegal_T_316 = io_decode_0_csr >= 12'hc80 & io_decode_0_csr < 12'hca0; // @[package.scala 204:55]
  wire  _io_decode_0_read_illegal_T_319 = (_io_decode_0_read_illegal_T_313 | _io_decode_0_read_illegal_T_316) & ~
    allow_counter; // @[CSR.scala 674:130]
  wire  _io_decode_0_read_illegal_T_320 = _io_decode_0_read_illegal_T_310 | _io_decode_0_read_illegal_T_319; // @[CSR.scala 673:53]
  wire [11:0] _io_decode_0_read_illegal_T_321 = io_decode_0_csr & 12'hc10; // @[Decode.scala 14:65]
  wire  _io_decode_0_read_illegal_T_322 = _io_decode_0_read_illegal_T_321 == 12'h410; // @[Decode.scala 14:121]
  wire  _io_decode_0_read_illegal_T_326 = _io_decode_0_read_illegal_T_322 & _T_304; // @[CSR.scala 675:42]
  wire  _io_decode_0_read_illegal_T_327 = _io_decode_0_read_illegal_T_320 | _io_decode_0_read_illegal_T_326; // @[CSR.scala 674:148]
  wire  _io_decode_0_read_illegal_T_330 = io_decode_0_fp_csr & io_decode_0_fp_illegal; // @[CSR.scala 677:21]
  wire  _io_decode_0_system_illegal_T_3 = is_wfi & ~allow_wfi; // @[CSR.scala 681:14]
  wire  _io_decode_0_system_illegal_T_4 = _io_decode_0_read_illegal_T_1 | _io_decode_0_system_illegal_T_3; // @[CSR.scala 680:64]
  wire  _io_decode_0_system_illegal_T_6 = is_ret & ~allow_sret; // @[CSR.scala 682:14]
  wire  _io_decode_0_system_illegal_T_7 = _io_decode_0_system_illegal_T_4 | _io_decode_0_system_illegal_T_6; // @[CSR.scala 681:28]
  wire  _io_decode_0_system_illegal_T_13 = is_ret & io_decode_0_csr[10] & io_decode_0_csr[7] & _T_304; // @[CSR.scala 683:49]
  wire  _io_decode_0_system_illegal_T_14 = _io_decode_0_system_illegal_T_7 | _io_decode_0_system_illegal_T_13; // @[CSR.scala 682:29]
  wire  _io_decode_0_system_illegal_T_16 = is_sfence & _io_decode_0_read_illegal_T_308; // @[CSR.scala 684:17]
  wire [11:0] _debugTVec_T = insn_break ? 12'h800 : 12'h808; // @[CSR.scala 697:37]
  wire [11:0] debugTVec = reg_debug ? _debugTVec_T : 12'h800; // @[CSR.scala 697:22]
  wire [63:0] notDebugTVec_base = delegate ? read_stvec : read_mtvec; // @[CSR.scala 705:19]
  wire [7:0] notDebugTVec_interruptVec_lo = {cause[5:0], 2'h0}; // @[CSR.scala 706:59]
  wire [55:0] notDebugTVec_interruptVec_hi = notDebugTVec_base[63:8]; // @[CSR.scala 707:33]
  wire [63:0] notDebugTVec_interruptVec = {notDebugTVec_interruptVec_hi,notDebugTVec_interruptVec_lo}; // @[Cat.scala 30:58]
  wire  notDebugTVec_doVector = notDebugTVec_base[0] & cause[63] & cause_lsbs[7:6] == 2'h0; // @[CSR.scala 708:55]
  wire [63:0] _notDebugTVec_T_1 = {notDebugTVec_base[63:2], 2'h0}; // @[CSR.scala 709:56]
  wire [63:0] notDebugTVec = notDebugTVec_doVector ? notDebugTVec_interruptVec : _notDebugTVec_T_1; // @[CSR.scala 709:8]
  wire  causeIsRnmiBEU = _causeIsRnmiInt_T_2 & _causeIsRnmiInt_T_4; // @[CSR.scala 713:55]
  wire  trapToNmiXcpt = ~nmie; // @[CSR.scala 718:37]
  wire  trapToNmi = causeIsNmi | trapToNmiXcpt; // @[CSR.scala 719:32]
  wire [36:0] _nmiTVec_T = causeIsNmi ? io_interrupts_nmi_rnmi_interrupt_vector :
    io_interrupts_nmi_rnmi_exception_vector; // @[CSR.scala 720:21]
  wire [36:0] nmiTVec = {_nmiTVec_T[36:1], 1'h0}; // @[CSR.scala 720:62]
  wire [63:0] _tvec_T = trapToNmi ? {{27'd0}, nmiTVec} : notDebugTVec; // @[CSR.scala 722:45]
  wire [63:0] tvec = trapToDebug ? {{52'd0}, debugTVec} : _tvec_T; // @[CSR.scala 722:17]
  reg [1:0] io_status_dprv_REG; // @[CSR.scala 733:24]
  wire  _GEN_364 = insn_wfi & _io_interrupt_T & _T_304 | reg_wfi; // @[CSR.scala 740:51 CSR.scala 740:61 CSR.scala 425:50]
  wire  _GEN_366 = io_retire | exception | reg_singleStepped; // @[CSR.scala 743:36 CSR.scala 743:56 CSR.scala 371:30]
  wire [39:0] _epc_T = ~io_pc; // @[CSR.scala 1203:28]
  wire [39:0] _epc_T_1 = _epc_T | 40'h1; // @[CSR.scala 1203:31]
  wire [39:0] epc = ~_epc_T_1; // @[CSR.scala 1203:26]
  wire [1:0] _reg_dcsr_cause_T = causeIsDebugTrigger ? 2'h2 : 2'h1; // @[CSR.scala 757:86]
  wire [1:0] _reg_dcsr_cause_T_1 = causeIsDebugInt ? 2'h3 : _reg_dcsr_cause_T; // @[CSR.scala 757:56]
  wire [2:0] _reg_dcsr_cause_T_2 = reg_singleStepped ? 3'h4 : {{1'd0}, _reg_dcsr_cause_T_1}; // @[CSR.scala 757:30]
  wire  _GEN_368 = ~reg_debug | reg_debug; // @[CSR.scala 754:25 CSR.scala 755:19 CSR.scala 367:22]
  wire [39:0] _GEN_369 = ~reg_debug ? epc : reg_dpc; // @[CSR.scala 754:25 CSR.scala 756:17 CSR.scala 368:20]
  wire [1:0] _GEN_371 = ~reg_debug ? reg_mstatus_prv : reg_dcsr_prv; // @[CSR.scala 754:25 CSR.scala 758:22 CSR.scala 326:21]
  wire [1:0] _reg_mncause_T = causeIsRnmiBEU ? 2'h3 : 2'h2; // @[CSR.scala 765:55]
  wire [63:0] _GEN_185 = {{62'd0}, _reg_mncause_T}; // @[CSR.scala 765:50]
  wire [63:0] _reg_mncause_T_1 = 64'h8000000000000000 | _GEN_185; // @[CSR.scala 765:50]
  wire  _GEN_373 = nmie ? 1'h0 : nmie; // @[CSR.scala 762:24 CSR.scala 763:19 CSR.scala 406:26]
  wire [39:0] _GEN_374 = nmie ? epc : reg_mnepc; // @[CSR.scala 762:24 CSR.scala 764:19 CSR.scala 403:22]
  wire [63:0] _GEN_375 = nmie ? _reg_mncause_T_1 : reg_mncause; // @[CSR.scala 762:24 CSR.scala 765:21 CSR.scala 404:28]
  wire [1:0] _GEN_376 = nmie ? reg_mstatus_prv : reg_mnstatus_mpp; // @[CSR.scala 762:24 CSR.scala 766:26 CSR.scala 405:25]
  wire [39:0] _GEN_378 = delegate & nmie ? epc : reg_sepc; // @[CSR.scala 769:35 CSR.scala 770:16 CSR.scala 419:21]
  wire [63:0] _GEN_379 = delegate & nmie ? cause : reg_scause; // @[CSR.scala 769:35 CSR.scala 771:18 CSR.scala 420:23]
  wire [39:0] _GEN_381 = delegate & nmie ? io_tval : reg_stval; // @[CSR.scala 769:35 CSR.scala 773:17 CSR.scala 421:22]
  wire  _GEN_382 = delegate & nmie ? reg_mstatus_sie : reg_mstatus_spie; // @[CSR.scala 769:35 CSR.scala 774:24 CSR.scala 318:24]
  wire [1:0] _GEN_383 = delegate & nmie ? reg_mstatus_prv : {{1'd0}, reg_mstatus_spp}; // @[CSR.scala 769:35 CSR.scala 775:23 CSR.scala 318:24]
  wire  _GEN_384 = delegate & nmie ? 1'h0 : reg_mstatus_sie; // @[CSR.scala 769:35 CSR.scala 776:23 CSR.scala 318:24]
  wire [39:0] _GEN_386 = delegate & nmie ? reg_mepc : epc; // @[CSR.scala 769:35 CSR.scala 390:21 CSR.scala 779:16]
  wire [63:0] _GEN_387 = delegate & nmie ? reg_mcause : cause; // @[CSR.scala 769:35 CSR.scala 391:27 CSR.scala 780:18]
  wire [39:0] _GEN_388 = delegate & nmie ? reg_mtval : io_tval; // @[CSR.scala 769:35 CSR.scala 392:22 CSR.scala 782:17]
  wire  _GEN_389 = delegate & nmie ? reg_mstatus_mpie : reg_mstatus_mie; // @[CSR.scala 769:35 CSR.scala 318:24 CSR.scala 783:24]
  wire [1:0] _GEN_390 = delegate & nmie ? reg_mstatus_mpp : reg_mstatus_prv; // @[CSR.scala 769:35 CSR.scala 318:24 CSR.scala 784:23]
  wire  _GEN_391 = delegate & nmie & reg_mstatus_mie; // @[CSR.scala 769:35 CSR.scala 318:24 CSR.scala 785:23]
  wire  _GEN_392 = causeIsNmi ? _GEN_373 : nmie; // @[CSR.scala 761:31 CSR.scala 406:26]
  wire [39:0] _GEN_393 = causeIsNmi ? _GEN_374 : reg_mnepc; // @[CSR.scala 761:31 CSR.scala 403:22]
  wire [63:0] _GEN_394 = causeIsNmi ? _GEN_375 : reg_mncause; // @[CSR.scala 761:31 CSR.scala 404:28]
  wire [1:0] _GEN_395 = causeIsNmi ? _GEN_376 : reg_mnstatus_mpp; // @[CSR.scala 761:31 CSR.scala 405:25]
  wire [39:0] _GEN_397 = causeIsNmi ? reg_sepc : _GEN_378; // @[CSR.scala 761:31 CSR.scala 419:21]
  wire [63:0] _GEN_398 = causeIsNmi ? reg_scause : _GEN_379; // @[CSR.scala 761:31 CSR.scala 420:23]
  wire [39:0] _GEN_400 = causeIsNmi ? reg_stval : _GEN_381; // @[CSR.scala 761:31 CSR.scala 421:22]
  wire  _GEN_401 = causeIsNmi ? reg_mstatus_spie : _GEN_382; // @[CSR.scala 761:31 CSR.scala 318:24]
  wire [1:0] _GEN_402 = causeIsNmi ? {{1'd0}, reg_mstatus_spp} : _GEN_383; // @[CSR.scala 761:31 CSR.scala 318:24]
  wire  _GEN_403 = causeIsNmi ? reg_mstatus_sie : _GEN_384; // @[CSR.scala 761:31 CSR.scala 318:24]
  wire [39:0] _GEN_404 = causeIsNmi ? reg_mepc : _GEN_386; // @[CSR.scala 761:31 CSR.scala 390:21]
  wire [63:0] _GEN_405 = causeIsNmi ? reg_mcause : _GEN_387; // @[CSR.scala 761:31 CSR.scala 391:27]
  wire [39:0] _GEN_406 = causeIsNmi ? reg_mtval : _GEN_388; // @[CSR.scala 761:31 CSR.scala 392:22]
  wire  _GEN_407 = causeIsNmi ? reg_mstatus_mpie : _GEN_389; // @[CSR.scala 761:31 CSR.scala 318:24]
  wire [1:0] _GEN_408 = causeIsNmi ? reg_mstatus_mpp : _GEN_390; // @[CSR.scala 761:31 CSR.scala 318:24]
  wire  _GEN_409 = causeIsNmi ? reg_mstatus_mie : _GEN_391; // @[CSR.scala 761:31 CSR.scala 318:24]
  wire  _GEN_410 = trapToDebug ? _GEN_368 : reg_debug; // @[CSR.scala 753:24 CSR.scala 367:22]
  wire [39:0] _GEN_411 = trapToDebug ? _GEN_369 : reg_dpc; // @[CSR.scala 753:24 CSR.scala 368:20]
  wire [1:0] _GEN_413 = trapToDebug ? _GEN_371 : reg_dcsr_prv; // @[CSR.scala 753:24 CSR.scala 326:21]
  wire  _GEN_415 = trapToDebug ? nmie : _GEN_392; // @[CSR.scala 753:24 CSR.scala 406:26]
  wire [39:0] _GEN_416 = trapToDebug ? reg_mnepc : _GEN_393; // @[CSR.scala 753:24 CSR.scala 403:22]
  wire [63:0] _GEN_417 = trapToDebug ? reg_mncause : _GEN_394; // @[CSR.scala 753:24 CSR.scala 404:28]
  wire [1:0] _GEN_418 = trapToDebug ? reg_mnstatus_mpp : _GEN_395; // @[CSR.scala 753:24 CSR.scala 405:25]
  wire [39:0] _GEN_419 = trapToDebug ? reg_sepc : _GEN_397; // @[CSR.scala 753:24 CSR.scala 419:21]
  wire [63:0] _GEN_420 = trapToDebug ? reg_scause : _GEN_398; // @[CSR.scala 753:24 CSR.scala 420:23]
  wire [39:0] _GEN_422 = trapToDebug ? reg_stval : _GEN_400; // @[CSR.scala 753:24 CSR.scala 421:22]
  wire  _GEN_423 = trapToDebug ? reg_mstatus_spie : _GEN_401; // @[CSR.scala 753:24 CSR.scala 318:24]
  wire [1:0] _GEN_424 = trapToDebug ? {{1'd0}, reg_mstatus_spp} : _GEN_402; // @[CSR.scala 753:24 CSR.scala 318:24]
  wire  _GEN_425 = trapToDebug ? reg_mstatus_sie : _GEN_403; // @[CSR.scala 753:24 CSR.scala 318:24]
  wire [39:0] _GEN_426 = trapToDebug ? reg_mepc : _GEN_404; // @[CSR.scala 753:24 CSR.scala 390:21]
  wire [63:0] _GEN_427 = trapToDebug ? reg_mcause : _GEN_405; // @[CSR.scala 753:24 CSR.scala 391:27]
  wire [39:0] _GEN_428 = trapToDebug ? reg_mtval : _GEN_406; // @[CSR.scala 753:24 CSR.scala 392:22]
  wire  _GEN_429 = trapToDebug ? reg_mstatus_mpie : _GEN_407; // @[CSR.scala 753:24 CSR.scala 318:24]
  wire [1:0] _GEN_430 = trapToDebug ? reg_mstatus_mpp : _GEN_408; // @[CSR.scala 753:24 CSR.scala 318:24]
  wire  _GEN_431 = trapToDebug ? reg_mstatus_mie : _GEN_409; // @[CSR.scala 753:24 CSR.scala 318:24]
  wire  _GEN_432 = exception ? _GEN_410 : reg_debug; // @[CSR.scala 752:20 CSR.scala 367:22]
  wire [39:0] _GEN_433 = exception ? _GEN_411 : reg_dpc; // @[CSR.scala 752:20 CSR.scala 368:20]
  wire [1:0] _GEN_435 = exception ? _GEN_413 : reg_dcsr_prv; // @[CSR.scala 752:20 CSR.scala 326:21]
  wire  _GEN_437 = exception ? _GEN_415 : nmie; // @[CSR.scala 752:20 CSR.scala 406:26]
  wire [39:0] _GEN_438 = exception ? _GEN_416 : reg_mnepc; // @[CSR.scala 752:20 CSR.scala 403:22]
  wire [63:0] _GEN_439 = exception ? _GEN_417 : reg_mncause; // @[CSR.scala 752:20 CSR.scala 404:28]
  wire [1:0] _GEN_440 = exception ? _GEN_418 : reg_mnstatus_mpp; // @[CSR.scala 752:20 CSR.scala 405:25]
  wire [39:0] _GEN_441 = exception ? _GEN_419 : reg_sepc; // @[CSR.scala 752:20 CSR.scala 419:21]
  wire [63:0] _GEN_442 = exception ? _GEN_420 : reg_scause; // @[CSR.scala 752:20 CSR.scala 420:23]
  wire [39:0] _GEN_444 = exception ? _GEN_422 : reg_stval; // @[CSR.scala 752:20 CSR.scala 421:22]
  wire  _GEN_445 = exception ? _GEN_423 : reg_mstatus_spie; // @[CSR.scala 752:20 CSR.scala 318:24]
  wire [1:0] _GEN_446 = exception ? _GEN_424 : {{1'd0}, reg_mstatus_spp}; // @[CSR.scala 752:20 CSR.scala 318:24]
  wire  _GEN_447 = exception ? _GEN_425 : reg_mstatus_sie; // @[CSR.scala 752:20 CSR.scala 318:24]
  wire [39:0] _GEN_448 = exception ? _GEN_426 : reg_mepc; // @[CSR.scala 752:20 CSR.scala 390:21]
  wire [63:0] _GEN_449 = exception ? _GEN_427 : reg_mcause; // @[CSR.scala 752:20 CSR.scala 391:27]
  wire [39:0] _GEN_450 = exception ? _GEN_428 : reg_mtval; // @[CSR.scala 752:20 CSR.scala 392:22]
  wire  _GEN_451 = exception ? _GEN_429 : reg_mstatus_mpie; // @[CSR.scala 752:20 CSR.scala 318:24]
  wire [1:0] _GEN_452 = exception ? _GEN_430 : reg_mstatus_mpp; // @[CSR.scala 752:20 CSR.scala 318:24]
  wire  _GEN_453 = exception ? _GEN_431 : reg_mstatus_mie; // @[CSR.scala 752:20 CSR.scala 318:24]
  wire  _GEN_455 = io_rw_addr[10] & ~io_rw_addr[7] | _GEN_437; // @[CSR.scala 822:69 CSR.scala 824:17]
  wire [39:0] _GEN_456 = io_rw_addr[10] & ~io_rw_addr[7] ? lo_7 : lo_4; // @[CSR.scala 822:69 CSR.scala 825:15 CSR.scala 831:15]
  wire  _GEN_457 = io_rw_addr[10] & ~io_rw_addr[7] ? _GEN_453 : reg_mstatus_mpie; // @[CSR.scala 822:69 CSR.scala 827:23]
  wire  _GEN_458 = io_rw_addr[10] & ~io_rw_addr[7] ? _GEN_451 : 1'h1; // @[CSR.scala 822:69 CSR.scala 828:24]
  wire [1:0] _GEN_459 = io_rw_addr[10] & ~io_rw_addr[7] ? _GEN_452 : 2'h0; // @[CSR.scala 822:69 CSR.scala 829:23]
  wire [39:0] _GEN_462 = io_rw_addr[10] & io_rw_addr[7] ? lo_6 : _GEN_456; // @[CSR.scala 818:70 CSR.scala 821:15]
  wire  _GEN_463 = io_rw_addr[10] & io_rw_addr[7] ? _GEN_437 : _GEN_455; // @[CSR.scala 818:70]
  wire  _GEN_464 = io_rw_addr[10] & io_rw_addr[7] ? _GEN_453 : _GEN_457; // @[CSR.scala 818:70]
  wire  _GEN_465 = io_rw_addr[10] & io_rw_addr[7] ? _GEN_451 : _GEN_458; // @[CSR.scala 818:70]
  wire [1:0] _GEN_466 = io_rw_addr[10] & io_rw_addr[7] ? _GEN_452 : _GEN_459; // @[CSR.scala 818:70]
  wire  _GEN_467 = ~io_rw_addr[9] ? reg_mstatus_spie : _GEN_447; // @[CSR.scala 812:52 CSR.scala 813:23]
  wire  _GEN_468 = ~io_rw_addr[9] | _GEN_445; // @[CSR.scala 812:52 CSR.scala 814:24]
  wire [1:0] _GEN_469 = ~io_rw_addr[9] ? 2'h0 : _GEN_446; // @[CSR.scala 812:52 CSR.scala 815:23]
  wire [39:0] _GEN_471 = ~io_rw_addr[9] ? lo_10 : _GEN_462; // @[CSR.scala 812:52 CSR.scala 817:15]
  wire  _GEN_473 = ~io_rw_addr[9] ? _GEN_437 : _GEN_463; // @[CSR.scala 812:52]
  wire  _GEN_474 = ~io_rw_addr[9] ? _GEN_453 : _GEN_464; // @[CSR.scala 812:52]
  wire  _GEN_475 = ~io_rw_addr[9] ? _GEN_451 : _GEN_465; // @[CSR.scala 812:52]
  wire [1:0] _GEN_476 = ~io_rw_addr[9] ? _GEN_452 : _GEN_466; // @[CSR.scala 812:52]
  wire  _GEN_477 = ret_prv < 2'h3 ? 1'h0 : reg_mstatus_mprv; // @[CSR.scala 835:41 CSR.scala 836:24 CSR.scala 318:24]
  wire  _GEN_478 = insn_ret ? _GEN_467 : _GEN_447; // @[CSR.scala 810:19]
  wire  _GEN_479 = insn_ret ? _GEN_468 : _GEN_445; // @[CSR.scala 810:19]
  wire [1:0] _GEN_480 = insn_ret ? _GEN_469 : _GEN_446; // @[CSR.scala 810:19]
  wire [63:0] _GEN_481 = insn_ret ? {{24'd0}, _GEN_471} : tvec; // @[CSR.scala 810:19 CSR.scala 723:11]
  wire  _GEN_483 = insn_ret ? _GEN_473 : _GEN_437; // @[CSR.scala 810:19]
  wire  _GEN_484 = insn_ret ? _GEN_474 : _GEN_453; // @[CSR.scala 810:19]
  wire  _GEN_485 = insn_ret ? _GEN_475 : _GEN_451; // @[CSR.scala 810:19]
  wire [1:0] _GEN_486 = insn_ret ? _GEN_476 : _GEN_452; // @[CSR.scala 810:19]
  wire  _GEN_488 = insn_ret ? _GEN_477 : reg_mstatus_mprv; // @[CSR.scala 810:19 CSR.scala 318:24]
  reg  io_status_cease_r; // @[Reg.scala 27:20]
  wire  _GEN_489 = insn_cease | io_status_cease_r; // @[Reg.scala 28:19 Reg.scala 28:23 Reg.scala 27:20]
  wire [3:0] _io_rw_rdata_T = _T_67 ? reg_tselect : 4'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_1 = _T_68 ? _T_21 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_2 = _T_69 ? _T_24 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_4 = _T_71 ? reg_misa : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_5 = _T_72 ? read_mstatus : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_6 = _T_73 ? read_mtvec : 64'h0; // @[Mux.scala 27:72]
  wire [31:0] _io_rw_rdata_T_7 = _T_74 ? read_mip : 32'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_8 = _T_75 ? reg_mie : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_9 = _T_76 ? reg_mscratch : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_10 = _T_77 ? _T_32 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_11 = _T_78 ? _T_35 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_12 = _T_79 ? reg_mcause : 64'h0; // @[Mux.scala 27:72]
  wire [1:0] _io_rw_rdata_T_13 = _T_80 ? io_hartid : 2'h0; // @[Mux.scala 27:72]
  wire [31:0] _io_rw_rdata_T_14 = _T_81 ? _T_36 : 32'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_15 = _T_82 ? _T_43 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_16 = _T_83 ? reg_dscratch : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_17 = _T_84 ? reg_mnscratch : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_18 = _T_85 ? _T_50 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_19 = _T_86 ? reg_mncause : 64'h0; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_20 = _T_87 ? _T_51 : 103'h0; // @[Mux.scala 27:72]
  wire [4:0] _io_rw_rdata_T_21 = _T_88 ? reg_fflags : 5'h0; // @[Mux.scala 27:72]
  wire [2:0] _io_rw_rdata_T_22 = _T_89 ? reg_frm : 3'h0; // @[Mux.scala 27:72]
  wire [7:0] _io_rw_rdata_T_23 = _T_90 ? read_fcsr : 8'h0; // @[Mux.scala 27:72]
  wire [6:0] _io_rw_rdata_T_24 = _T_91 ? reg_mcountinhibit : 7'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_25 = _T_92 ? value_1 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_26 = _T_93 ? value : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_27 = _T_94 ? reg_hpmevent_0 : 64'h0; // @[Mux.scala 27:72]
  wire [39:0] _io_rw_rdata_T_28 = _T_95 ? value_2 : 40'h0; // @[Mux.scala 27:72]
  wire [39:0] _io_rw_rdata_T_29 = _T_96 ? value_2 : 40'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_30 = _T_97 ? reg_hpmevent_1 : 64'h0; // @[Mux.scala 27:72]
  wire [39:0] _io_rw_rdata_T_31 = _T_98 ? value_3 : 40'h0; // @[Mux.scala 27:72]
  wire [39:0] _io_rw_rdata_T_32 = _T_99 ? value_3 : 40'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_33 = _T_100 ? reg_hpmevent_2 : 64'h0; // @[Mux.scala 27:72]
  wire [39:0] _io_rw_rdata_T_34 = _T_101 ? value_4 : 40'h0; // @[Mux.scala 27:72]
  wire [39:0] _io_rw_rdata_T_35 = _T_102 ? value_4 : 40'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_36 = _T_103 ? reg_hpmevent_3 : 64'h0; // @[Mux.scala 27:72]
  wire [39:0] _io_rw_rdata_T_37 = _T_104 ? value_5 : 40'h0; // @[Mux.scala 27:72]
  wire [39:0] _io_rw_rdata_T_38 = _T_105 ? value_5 : 40'h0; // @[Mux.scala 27:72]
  wire [31:0] _io_rw_rdata_T_114 = _T_181 ? read_mcounteren : 32'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_115 = _T_182 ? value_1 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_116 = _T_183 ? value : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_117 = _T_184 ? _T_52[63:0] : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_118 = _T_185 ? read_sip : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_119 = _T_186 ? read_sie : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_120 = _T_187 ? reg_sscratch : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_121 = _T_188 ? reg_scause : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_122 = _T_189 ? _T_56 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_123 = _T_190 ? _T_57 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_124 = _T_191 ? _T_64 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_125 = _T_192 ? read_stvec : 64'h0; // @[Mux.scala 27:72]
  wire [31:0] _io_rw_rdata_T_126 = _T_193 ? read_scounteren : 32'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_127 = _T_194 ? read_mideleg : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_128 = _T_195 ? read_medeleg : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_129 = _T_196 ? _T_65 : 64'h0; // @[Mux.scala 27:72]
  wire [34:0] _io_rw_rdata_T_131 = _T_198 ? reg_pmp_0_addr : 35'h0; // @[Mux.scala 27:72]
  wire [34:0] _io_rw_rdata_T_132 = _T_199 ? reg_pmp_1_addr : 35'h0; // @[Mux.scala 27:72]
  wire [34:0] _io_rw_rdata_T_133 = _T_200 ? reg_pmp_2_addr : 35'h0; // @[Mux.scala 27:72]
  wire [34:0] _io_rw_rdata_T_134 = _T_201 ? reg_pmp_3_addr : 35'h0; // @[Mux.scala 27:72]
  wire [34:0] _io_rw_rdata_T_135 = _T_202 ? reg_pmp_4_addr : 35'h0; // @[Mux.scala 27:72]
  wire [34:0] _io_rw_rdata_T_136 = _T_203 ? reg_pmp_5_addr : 35'h0; // @[Mux.scala 27:72]
  wire [34:0] _io_rw_rdata_T_137 = _T_204 ? reg_pmp_6_addr : 35'h0; // @[Mux.scala 27:72]
  wire [34:0] _io_rw_rdata_T_138 = _T_205 ? reg_pmp_7_addr : 35'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_147 = _T_214 ? reg_custom_0 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_148 = _T_215 ? reg_custom_1 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_149 = _T_216 ? 64'h1 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_150 = _T_217 ? 64'h489 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_151 = _T_218 ? 64'h4210324 : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _GEN_248 = {{60'd0}, _io_rw_rdata_T}; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_152 = _GEN_248 | _io_rw_rdata_T_1; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_153 = _io_rw_rdata_T_152 | _io_rw_rdata_T_2; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_155 = _io_rw_rdata_T_153 | _io_rw_rdata_T_4; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_156 = _io_rw_rdata_T_155 | _io_rw_rdata_T_5; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_157 = _io_rw_rdata_T_156 | _io_rw_rdata_T_6; // @[Mux.scala 27:72]
  wire [63:0] _GEN_251 = {{32'd0}, _io_rw_rdata_T_7}; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_158 = _io_rw_rdata_T_157 | _GEN_251; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_159 = _io_rw_rdata_T_158 | _io_rw_rdata_T_8; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_160 = _io_rw_rdata_T_159 | _io_rw_rdata_T_9; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_161 = _io_rw_rdata_T_160 | _io_rw_rdata_T_10; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_162 = _io_rw_rdata_T_161 | _io_rw_rdata_T_11; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_163 = _io_rw_rdata_T_162 | _io_rw_rdata_T_12; // @[Mux.scala 27:72]
  wire [63:0] _GEN_270 = {{62'd0}, _io_rw_rdata_T_13}; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_164 = _io_rw_rdata_T_163 | _GEN_270; // @[Mux.scala 27:72]
  wire [63:0] _GEN_273 = {{32'd0}, _io_rw_rdata_T_14}; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_165 = _io_rw_rdata_T_164 | _GEN_273; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_166 = _io_rw_rdata_T_165 | _io_rw_rdata_T_15; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_167 = _io_rw_rdata_T_166 | _io_rw_rdata_T_16; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_168 = _io_rw_rdata_T_167 | _io_rw_rdata_T_17; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_169 = _io_rw_rdata_T_168 | _io_rw_rdata_T_18; // @[Mux.scala 27:72]
  wire [63:0] _io_rw_rdata_T_170 = _io_rw_rdata_T_169 | _io_rw_rdata_T_19; // @[Mux.scala 27:72]
  wire [102:0] _GEN_292 = {{39'd0}, _io_rw_rdata_T_170}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_171 = _GEN_292 | _io_rw_rdata_T_20; // @[Mux.scala 27:72]
  wire [102:0] _GEN_295 = {{98'd0}, _io_rw_rdata_T_21}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_172 = _io_rw_rdata_T_171 | _GEN_295; // @[Mux.scala 27:72]
  wire [102:0] _GEN_314 = {{100'd0}, _io_rw_rdata_T_22}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_173 = _io_rw_rdata_T_172 | _GEN_314; // @[Mux.scala 27:72]
  wire [102:0] _GEN_317 = {{95'd0}, _io_rw_rdata_T_23}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_174 = _io_rw_rdata_T_173 | _GEN_317; // @[Mux.scala 27:72]
  wire [102:0] _GEN_336 = {{96'd0}, _io_rw_rdata_T_24}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_175 = _io_rw_rdata_T_174 | _GEN_336; // @[Mux.scala 27:72]
  wire [102:0] _GEN_339 = {{39'd0}, _io_rw_rdata_T_25}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_176 = _io_rw_rdata_T_175 | _GEN_339; // @[Mux.scala 27:72]
  wire [102:0] _GEN_358 = {{39'd0}, _io_rw_rdata_T_26}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_177 = _io_rw_rdata_T_176 | _GEN_358; // @[Mux.scala 27:72]
  wire [102:0] _GEN_361 = {{39'd0}, _io_rw_rdata_T_27}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_178 = _io_rw_rdata_T_177 | _GEN_361; // @[Mux.scala 27:72]
  wire [102:0] _GEN_380 = {{63'd0}, _io_rw_rdata_T_28}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_179 = _io_rw_rdata_T_178 | _GEN_380; // @[Mux.scala 27:72]
  wire [102:0] _GEN_399 = {{63'd0}, _io_rw_rdata_T_29}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_180 = _io_rw_rdata_T_179 | _GEN_399; // @[Mux.scala 27:72]
  wire [102:0] _GEN_421 = {{39'd0}, _io_rw_rdata_T_30}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_181 = _io_rw_rdata_T_180 | _GEN_421; // @[Mux.scala 27:72]
  wire [102:0] _GEN_443 = {{63'd0}, _io_rw_rdata_T_31}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_182 = _io_rw_rdata_T_181 | _GEN_443; // @[Mux.scala 27:72]
  wire [102:0] _GEN_491 = {{63'd0}, _io_rw_rdata_T_32}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_183 = _io_rw_rdata_T_182 | _GEN_491; // @[Mux.scala 27:72]
  wire [102:0] _GEN_539 = {{39'd0}, _io_rw_rdata_T_33}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_184 = _io_rw_rdata_T_183 | _GEN_539; // @[Mux.scala 27:72]
  wire [102:0] _GEN_541 = {{63'd0}, _io_rw_rdata_T_34}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_185 = _io_rw_rdata_T_184 | _GEN_541; // @[Mux.scala 27:72]
  wire [102:0] _GEN_543 = {{63'd0}, _io_rw_rdata_T_35}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_186 = _io_rw_rdata_T_185 | _GEN_543; // @[Mux.scala 27:72]
  wire [102:0] _GEN_578 = {{39'd0}, _io_rw_rdata_T_36}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_187 = _io_rw_rdata_T_186 | _GEN_578; // @[Mux.scala 27:72]
  wire [102:0] _GEN_580 = {{63'd0}, _io_rw_rdata_T_37}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_188 = _io_rw_rdata_T_187 | _GEN_580; // @[Mux.scala 27:72]
  wire [102:0] _GEN_581 = {{63'd0}, _io_rw_rdata_T_38}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_189 = _io_rw_rdata_T_188 | _GEN_581; // @[Mux.scala 27:72]
  wire [102:0] _GEN_584 = {{71'd0}, _io_rw_rdata_T_114}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_265 = _io_rw_rdata_T_189 | _GEN_584; // @[Mux.scala 27:72]
  wire [102:0] _GEN_587 = {{39'd0}, _io_rw_rdata_T_115}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_266 = _io_rw_rdata_T_265 | _GEN_587; // @[Mux.scala 27:72]
  wire [102:0] _GEN_594 = {{39'd0}, _io_rw_rdata_T_116}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_267 = _io_rw_rdata_T_266 | _GEN_594; // @[Mux.scala 27:72]
  wire [102:0] _GEN_596 = {{39'd0}, _io_rw_rdata_T_117}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_268 = _io_rw_rdata_T_267 | _GEN_596; // @[Mux.scala 27:72]
  wire [102:0] _GEN_597 = {{39'd0}, _io_rw_rdata_T_118}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_269 = _io_rw_rdata_T_268 | _GEN_597; // @[Mux.scala 27:72]
  wire [102:0] _GEN_600 = {{39'd0}, _io_rw_rdata_T_119}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_270 = _io_rw_rdata_T_269 | _GEN_600; // @[Mux.scala 27:72]
  wire [102:0] _GEN_603 = {{39'd0}, _io_rw_rdata_T_120}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_271 = _io_rw_rdata_T_270 | _GEN_603; // @[Mux.scala 27:72]
  wire [102:0] _GEN_611 = {{39'd0}, _io_rw_rdata_T_121}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_272 = _io_rw_rdata_T_271 | _GEN_611; // @[Mux.scala 27:72]
  wire [102:0] _GEN_613 = {{39'd0}, _io_rw_rdata_T_122}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_273 = _io_rw_rdata_T_272 | _GEN_613; // @[Mux.scala 27:72]
  wire [102:0] _GEN_614 = {{39'd0}, _io_rw_rdata_T_123}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_274 = _io_rw_rdata_T_273 | _GEN_614; // @[Mux.scala 27:72]
  wire [102:0] _GEN_617 = {{39'd0}, _io_rw_rdata_T_124}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_275 = _io_rw_rdata_T_274 | _GEN_617; // @[Mux.scala 27:72]
  wire [102:0] _GEN_620 = {{39'd0}, _io_rw_rdata_T_125}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_276 = _io_rw_rdata_T_275 | _GEN_620; // @[Mux.scala 27:72]
  wire [102:0] _GEN_627 = {{71'd0}, _io_rw_rdata_T_126}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_277 = _io_rw_rdata_T_276 | _GEN_627; // @[Mux.scala 27:72]
  wire [102:0] _GEN_629 = {{39'd0}, _io_rw_rdata_T_127}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_278 = _io_rw_rdata_T_277 | _GEN_629; // @[Mux.scala 27:72]
  wire [102:0] _GEN_630 = {{39'd0}, _io_rw_rdata_T_128}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_279 = _io_rw_rdata_T_278 | _GEN_630; // @[Mux.scala 27:72]
  wire [102:0] _GEN_633 = {{39'd0}, _io_rw_rdata_T_129}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_280 = _io_rw_rdata_T_279 | _GEN_633; // @[Mux.scala 27:72]
  wire [102:0] _GEN_636 = {{68'd0}, _io_rw_rdata_T_131}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_282 = _io_rw_rdata_T_280 | _GEN_636; // @[Mux.scala 27:72]
  wire [102:0] _GEN_644 = {{68'd0}, _io_rw_rdata_T_132}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_283 = _io_rw_rdata_T_282 | _GEN_644; // @[Mux.scala 27:72]
  wire [102:0] _GEN_646 = {{68'd0}, _io_rw_rdata_T_133}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_284 = _io_rw_rdata_T_283 | _GEN_646; // @[Mux.scala 27:72]
  wire [102:0] _GEN_647 = {{68'd0}, _io_rw_rdata_T_134}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_285 = _io_rw_rdata_T_284 | _GEN_647; // @[Mux.scala 27:72]
  wire [102:0] _GEN_650 = {{68'd0}, _io_rw_rdata_T_135}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_286 = _io_rw_rdata_T_285 | _GEN_650; // @[Mux.scala 27:72]
  wire [102:0] _GEN_653 = {{68'd0}, _io_rw_rdata_T_136}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_287 = _io_rw_rdata_T_286 | _GEN_653; // @[Mux.scala 27:72]
  wire [102:0] _GEN_660 = {{68'd0}, _io_rw_rdata_T_137}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_288 = _io_rw_rdata_T_287 | _GEN_660; // @[Mux.scala 27:72]
  wire [102:0] _GEN_662 = {{68'd0}, _io_rw_rdata_T_138}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_289 = _io_rw_rdata_T_288 | _GEN_662; // @[Mux.scala 27:72]
  wire [102:0] _GEN_663 = {{39'd0}, _io_rw_rdata_T_147}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_298 = _io_rw_rdata_T_289 | _GEN_663; // @[Mux.scala 27:72]
  wire [102:0] _GEN_666 = {{39'd0}, _io_rw_rdata_T_148}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_299 = _io_rw_rdata_T_298 | _GEN_666; // @[Mux.scala 27:72]
  wire [102:0] _GEN_669 = {{39'd0}, _io_rw_rdata_T_149}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_300 = _io_rw_rdata_T_299 | _GEN_669; // @[Mux.scala 27:72]
  wire [102:0] _GEN_677 = {{39'd0}, _io_rw_rdata_T_150}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_301 = _io_rw_rdata_T_300 | _GEN_677; // @[Mux.scala 27:72]
  wire [102:0] _GEN_679 = {{39'd0}, _io_rw_rdata_T_151}; // @[Mux.scala 27:72]
  wire [102:0] _io_rw_rdata_T_302 = _io_rw_rdata_T_301 | _GEN_679; // @[Mux.scala 27:72]
  wire  _T_690 = io_rw_cmd == 3'h5; // @[package.scala 15:47]
  wire  _T_691 = io_rw_cmd == 3'h6; // @[package.scala 15:47]
  wire  _T_692 = io_rw_cmd == 3'h7; // @[package.scala 15:47]
  wire [4:0] _lo_T = reg_fflags | io_fcsr_flags_bits; // @[CSR.scala 880:30]
  wire [4:0] _GEN_490 = io_fcsr_flags_valid ? _lo_T : reg_fflags; // @[CSR.scala 879:30 CSR.scala 880:16 CSR.scala 427:23]
  wire  csr_wen = _T_691 | _T_692 | _T_690; // @[package.scala 72:59]
  wire [102:0] _new_mstatus_WIRE = {{39'd0}, wdata};
  wire  new_mstatus_sie = _new_mstatus_WIRE[1]; // @[CSR.scala 895:47]
  wire  new_mstatus_mie = _new_mstatus_WIRE[3]; // @[CSR.scala 895:47]
  wire  new_mstatus_spie = _new_mstatus_WIRE[5]; // @[CSR.scala 895:47]
  wire  new_mstatus_mpie = _new_mstatus_WIRE[7]; // @[CSR.scala 895:47]
  wire  new_mstatus_spp = _new_mstatus_WIRE[8]; // @[CSR.scala 895:47]
  wire [1:0] new_mstatus_mpp = _new_mstatus_WIRE[12:11]; // @[CSR.scala 895:47]
  wire [1:0] new_mstatus_fs = _new_mstatus_WIRE[14:13]; // @[CSR.scala 895:47]
  wire  new_mstatus_mprv = _new_mstatus_WIRE[17]; // @[CSR.scala 895:47]
  wire  new_mstatus_sum = _new_mstatus_WIRE[18]; // @[CSR.scala 895:47]
  wire  new_mstatus_mxr = _new_mstatus_WIRE[19]; // @[CSR.scala 895:47]
  wire  new_mstatus_tvm = _new_mstatus_WIRE[20]; // @[CSR.scala 895:47]
  wire  new_mstatus_tw = _new_mstatus_WIRE[21]; // @[CSR.scala 895:47]
  wire  new_mstatus_tsr = _new_mstatus_WIRE[22]; // @[CSR.scala 895:47]
  wire  _reg_mstatus_fs_T = |new_mstatus_fs; // @[CSR.scala 1207:73]
  wire [1:0] _reg_mstatus_fs_T_2 = _reg_mstatus_fs_T ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] _GEN_496 = _T_72 ? {{1'd0}, new_mstatus_spp} : _GEN_480; // @[CSR.scala 894:39 CSR.scala 903:27]
  wire  f = wdata[5]; // @[CSR.scala 921:20]
  wire [63:0] _reg_misa_T = ~wdata; // @[CSR.scala 925:25]
  wire  _reg_misa_T_1 = ~f; // @[CSR.scala 925:35]
  wire [3:0] _reg_misa_T_2 = {_reg_misa_T_1, 3'h0}; // @[CSR.scala 925:38]
  wire [63:0] _GEN_680 = {{60'd0}, _reg_misa_T_2}; // @[CSR.scala 925:32]
  wire [63:0] _reg_misa_T_3 = _reg_misa_T | _GEN_680; // @[CSR.scala 925:32]
  wire [63:0] _reg_misa_T_4 = ~_reg_misa_T_3; // @[CSR.scala 925:23]
  wire [63:0] _reg_misa_T_5 = _reg_misa_T_4 & 64'h102d; // @[CSR.scala 925:55]
  wire [63:0] _reg_misa_T_7 = reg_misa & 64'hffffffffffffefd2; // @[CSR.scala 925:73]
  wire [63:0] _reg_misa_T_8 = _reg_misa_T_5 | _reg_misa_T_7; // @[CSR.scala 925:62]
  wire [15:0] new_mip_lo = {4'h0,2'h0,reg_mip_seip,1'h0,2'h0,reg_mip_stip,1'h0,2'h0,reg_mip_ssip,1'h0}; // @[CSR.scala 933:59]
  wire [31:0] _new_mip_T = {16'h0,new_mip_lo}; // @[CSR.scala 933:59]
  wire [31:0] _new_mip_T_2 = io_rw_cmd[1] ? _new_mip_T : 32'h0; // @[CSR.scala 1182:9]
  wire [63:0] _GEN_683 = {{32'd0}, _new_mip_T_2}; // @[CSR.scala 1182:34]
  wire [63:0] _new_mip_T_3 = _GEN_683 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _new_mip_T_8 = _new_mip_T_3 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  new_mip_ssip = _new_mip_T_8[1]; // @[CSR.scala 933:88]
  wire  new_mip_stip = _new_mip_T_8[5]; // @[CSR.scala 933:88]
  wire  new_mip_seip = _new_mip_T_8[9]; // @[CSR.scala 933:88]
  wire [63:0] _reg_mie_T = wdata & 64'hffff0aaa; // @[CSR.scala 940:59]
  wire [63:0] _reg_mepc_T_1 = _reg_misa_T | 64'h1; // @[CSR.scala 1203:31]
  wire [63:0] _reg_mepc_T_2 = ~_reg_mepc_T_1; // @[CSR.scala 1203:26]
  wire [63:0] _GEN_512 = _T_77 ? _reg_mepc_T_2 : {{24'd0}, _GEN_448}; // @[CSR.scala 941:40 CSR.scala 941:51]
  wire [63:0] _GEN_514 = _T_73 ? wdata : {{27'd0}, reg_mtvec}; // @[CSR.scala 944:40 CSR.scala 944:52 CSR.scala 396:27]
  wire [63:0] _reg_mcause_T = wdata & 64'h800000000000001f; // @[CSR.scala 945:62]
  wire [63:0] _GEN_518 = _T_85 ? _reg_mepc_T_2 : {{24'd0}, _GEN_438}; // @[CSR.scala 951:43 CSR.scala 951:55]
  wire [63:0] _reg_mncause_T_2 = wdata & 64'h8000000000000003; // @[CSR.scala 952:66]
  wire  _GEN_521 = _T_87 ? nmie | new_mstatus_mie : _GEN_483; // @[CSR.scala 953:43 CSR.scala 955:19]
  wire [39:0] _GEN_522 = _T_95 ? wdata[39:0] : {{33'd0}, _GEN_4}; // @[CSR.scala 1200:31 Counters.scala 65:11]
  wire [63:0] _reg_hpmevent_0_T = wdata & 64'h3ffff03; // @[Events.scala 33:14]
  wire [39:0] _GEN_525 = _T_98 ? wdata[39:0] : {{33'd0}, _GEN_6}; // @[CSR.scala 1200:31 Counters.scala 65:11]
  wire [39:0] _GEN_528 = _T_101 ? wdata[39:0] : {{33'd0}, _GEN_8}; // @[CSR.scala 1200:31 Counters.scala 65:11]
  wire [39:0] _GEN_531 = _T_104 ? wdata[39:0] : {{33'd0}, _GEN_10}; // @[CSR.scala 1200:31 Counters.scala 65:11]
  wire [63:0] _reg_mcountinhibit_T_1 = wdata & 64'hfffffffffffffffd; // @[CSR.scala 964:76]
  wire [63:0] _GEN_534 = _T_91 ? _reg_mcountinhibit_T_1 : {{57'd0}, reg_mcountinhibit}; // @[CSR.scala 964:47 CSR.scala 964:67 CSR.scala 434:34]
  wire [63:0] _GEN_535 = _T_92 ? wdata : {{57'd0}, _GEN_2}; // @[CSR.scala 1200:31 Counters.scala 65:11]
  wire [63:0] _GEN_537 = _T_93 ? wdata : {{57'd0}, _GEN_0}; // @[CSR.scala 1200:31 Counters.scala 65:11]
  wire [63:0] _GEN_540 = _T_88 ? wdata : {{59'd0}, _GEN_490}; // @[CSR.scala 970:40 CSR.scala 970:75]
  wire [63:0] _GEN_542 = _T_89 ? wdata : {{61'd0}, reg_frm}; // @[CSR.scala 971:40 CSR.scala 971:72 CSR.scala 428:20]
  wire [63:0] _GEN_544 = _T_90 ? wdata : _GEN_540; // @[CSR.scala 972:38 CSR.scala 974:20]
  wire [63:0] _GEN_545 = _T_90 ? {{5'd0}, wdata[63:5]} : _GEN_542; // @[CSR.scala 972:38 CSR.scala 975:17]
  wire [1:0] new_dcsr_prv = wdata[1:0]; // @[CSR.scala 980:43]
  wire  new_dcsr_step = wdata[2]; // @[CSR.scala 980:43]
  wire  new_dcsr_ebreaku = wdata[12]; // @[CSR.scala 980:43]
  wire  new_dcsr_ebreaks = wdata[13]; // @[CSR.scala 980:43]
  wire  new_dcsr_ebreakm = wdata[15]; // @[CSR.scala 980:43]
  wire [63:0] _GEN_551 = _T_82 ? _reg_mepc_T_2 : {{24'd0}, _GEN_433}; // @[CSR.scala 987:42 CSR.scala 987:52]
  wire [1:0] _GEN_555 = _T_184 ? {{1'd0}, new_mstatus_spp} : _GEN_496; // @[CSR.scala 994:41 CSR.scala 998:25]
  wire [63:0] _new_sip_T = ~read_mideleg; // @[CSR.scala 1007:54]
  wire [63:0] _new_sip_T_1 = _GEN_97 & _new_sip_T; // @[CSR.scala 1007:52]
  wire [63:0] _new_sip_T_2 = wdata & read_mideleg; // @[CSR.scala 1007:78]
  wire [63:0] _new_sip_T_3 = _new_sip_T_1 | _new_sip_T_2; // @[CSR.scala 1007:69]
  wire  new_sip_ssip = _new_sip_T_3[1]; // @[CSR.scala 1007:41]
  wire [43:0] new_satp_ppn = wdata[43:0]; // @[CSR.scala 1012:45]
  wire [3:0] new_satp_mode = wdata[63:60]; // @[CSR.scala 1012:45]
  wire  _T_3131 = new_satp_mode == 4'h0; // @[package.scala 15:47]
  wire  _T_3132 = new_satp_mode == 4'h8; // @[package.scala 15:47]
  wire  _T_3133 = _T_3131 | _T_3132; // @[package.scala 72:59]
  wire [3:0] _reg_satp_mode_T = new_satp_mode & 4'h8; // @[CSR.scala 1015:44]
  wire [63:0] _reg_mie_T_2 = reg_mie & _new_sip_T; // @[CSR.scala 1021:64]
  wire [63:0] _reg_mie_T_4 = _reg_mie_T_2 | _new_sip_T_2; // @[CSR.scala 1021:81]
  wire [63:0] _GEN_567 = _T_191 ? _reg_mepc_T_2 : {{24'd0}, _GEN_441}; // @[CSR.scala 1023:42 CSR.scala 1023:53]
  wire [63:0] _GEN_568 = _T_192 ? wdata : {{25'd0}, reg_stvec}; // @[CSR.scala 1024:42 CSR.scala 1024:54 CSR.scala 423:22]
  wire [63:0] _GEN_573 = _T_193 ? wdata : {{32'd0}, reg_scounteren}; // @[CSR.scala 1029:44 CSR.scala 1029:61 CSR.scala 415:18]
  wire [63:0] _GEN_574 = _T_181 ? wdata : {{32'd0}, reg_mcounteren}; // @[CSR.scala 1032:44 CSR.scala 1032:61 CSR.scala 411:18]
  wire [63:0] _GEN_575 = _T_67 ? wdata : {{60'd0}, reg_tselect}; // @[CSR.scala 1035:41 CSR.scala 1035:55 CSR.scala 376:24]
  wire [63:0] _GEN_576 = _T_69 ? wdata : {{25'd0}, reg_bp_0_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo = {reg_bp_0_control_m,1'h0,reg_bp_0_control_s,reg_bp_0_control_u,reg_bp_0_control_x,
    reg_bp_0_control_w,reg_bp_0_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T = {4'h2,reg_bp_0_control_dmode,44'h10000000000,reg_bp_0_control_action,reg_bp_0_control_chain,2'h0
    ,reg_bp_0_control_tmatch,newBPC_lo}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_2 = io_rw_cmd[1] ? _newBPC_T : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_3 = _newBPC_T_2 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_8 = _newBPC_T_3 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  newBPC_chain = _newBPC_T_8[11]; // @[CSR.scala 1057:96]
  wire [2:0] newBPC_action = _newBPC_T_8[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_dmode = _newBPC_T_8[59]; // @[CSR.scala 1057:96]
  wire  dMode = newBPC_dmode & reg_debug; // @[CSR.scala 1058:38]
  wire [2:0] _GEN_577 = dMode | newBPC_action > 3'h1 ? newBPC_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire  _reg_bp_0_control_chain_T_1 = ~reg_bp_1_control_chain; // @[CSR.scala 1061:49]
  wire  _reg_bp_0_control_chain_T_3 = ~reg_bp_1_control_dmode; // @[CSR.scala 1061:88]
  wire [63:0] _GEN_593 = 4'h0 == reg_tselect & (~reg_bp_0_control_dmode | reg_debug) ? _GEN_576 : {{25'd0},
    reg_bp_0_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire [63:0] _GEN_609 = _T_69 ? wdata : {{25'd0}, reg_bp_1_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo_1 = {reg_bp_1_control_m,1'h0,reg_bp_1_control_s,reg_bp_1_control_u,reg_bp_1_control_x,
    reg_bp_1_control_w,reg_bp_1_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_24 = {4'h2,reg_bp_1_control_dmode,44'h10000000000,reg_bp_1_control_action,reg_bp_1_control_chain
    ,2'h0,reg_bp_1_control_tmatch,newBPC_lo_1}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_26 = io_rw_cmd[1] ? _newBPC_T_24 : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_27 = _newBPC_T_26 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_32 = _newBPC_T_27 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  newBPC_1_chain = _newBPC_T_32[11]; // @[CSR.scala 1057:96]
  wire [2:0] newBPC_1_action = _newBPC_T_32[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_1_dmode = _newBPC_T_32[59]; // @[CSR.scala 1057:96]
  wire  dMode_1 = newBPC_1_dmode & reg_debug & (reg_bp_0_control_dmode | ~reg_bp_0_control_chain); // @[CSR.scala 1058:51]
  wire [2:0] _GEN_610 = dMode_1 | newBPC_1_action > 3'h1 ? newBPC_1_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire  _reg_bp_1_control_chain_T_3 = ~reg_bp_2_control_dmode; // @[CSR.scala 1061:88]
  wire [63:0] _GEN_626 = 4'h1 == reg_tselect & (_reg_bp_0_control_chain_T_3 | reg_debug) ? _GEN_609 : {{25'd0},
    reg_bp_1_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire [63:0] _GEN_642 = _T_69 ? wdata : {{25'd0}, reg_bp_2_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo_2 = {reg_bp_2_control_m,1'h0,reg_bp_2_control_s,reg_bp_2_control_u,reg_bp_2_control_x,
    reg_bp_2_control_w,reg_bp_2_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_48 = {4'h2,reg_bp_2_control_dmode,44'h10000000000,reg_bp_2_control_action,reg_bp_2_control_chain
    ,2'h0,reg_bp_2_control_tmatch,newBPC_lo_2}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_50 = io_rw_cmd[1] ? _newBPC_T_48 : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_51 = _newBPC_T_50 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_56 = _newBPC_T_51 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  newBPC_2_chain = _newBPC_T_56[11]; // @[CSR.scala 1057:96]
  wire [2:0] newBPC_2_action = _newBPC_T_56[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_2_dmode = _newBPC_T_56[59]; // @[CSR.scala 1057:96]
  wire  dMode_2 = newBPC_2_dmode & reg_debug & (reg_bp_1_control_dmode | _reg_bp_0_control_chain_T_1); // @[CSR.scala 1058:51]
  wire [2:0] _GEN_643 = dMode_2 | newBPC_2_action > 3'h1 ? newBPC_2_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire  _reg_bp_2_control_chain_T_3 = ~reg_bp_3_control_dmode; // @[CSR.scala 1061:88]
  wire [63:0] _GEN_659 = 4'h2 == reg_tselect & (_reg_bp_1_control_chain_T_3 | reg_debug) ? _GEN_642 : {{25'd0},
    reg_bp_2_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire [63:0] _GEN_675 = _T_69 ? wdata : {{25'd0}, reg_bp_3_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo_3 = {reg_bp_3_control_m,1'h0,reg_bp_3_control_s,reg_bp_3_control_u,reg_bp_3_control_x,
    reg_bp_3_control_w,reg_bp_3_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_72 = {4'h2,reg_bp_3_control_dmode,44'h10000000000,reg_bp_3_control_action,reg_bp_3_control_chain
    ,2'h0,reg_bp_3_control_tmatch,newBPC_lo_3}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_74 = io_rw_cmd[1] ? _newBPC_T_72 : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_75 = _newBPC_T_74 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_80 = _newBPC_T_75 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  newBPC_3_chain = _newBPC_T_80[11]; // @[CSR.scala 1057:96]
  wire [2:0] newBPC_3_action = _newBPC_T_80[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_3_dmode = _newBPC_T_80[59]; // @[CSR.scala 1057:96]
  wire  dMode_3 = newBPC_3_dmode & reg_debug & (reg_bp_2_control_dmode | ~reg_bp_2_control_chain); // @[CSR.scala 1058:51]
  wire [2:0] _GEN_676 = dMode_3 | newBPC_3_action > 3'h1 ? newBPC_3_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire  _reg_bp_3_control_chain_T_3 = ~reg_bp_4_control_dmode; // @[CSR.scala 1061:88]
  wire [63:0] _GEN_692 = 4'h3 == reg_tselect & (_reg_bp_2_control_chain_T_3 | reg_debug) ? _GEN_675 : {{25'd0},
    reg_bp_3_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire [63:0] _GEN_708 = _T_69 ? wdata : {{25'd0}, reg_bp_4_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo_4 = {reg_bp_4_control_m,1'h0,reg_bp_4_control_s,reg_bp_4_control_u,reg_bp_4_control_x,
    reg_bp_4_control_w,reg_bp_4_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_96 = {4'h2,reg_bp_4_control_dmode,44'h10000000000,reg_bp_4_control_action,reg_bp_4_control_chain
    ,2'h0,reg_bp_4_control_tmatch,newBPC_lo_4}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_98 = io_rw_cmd[1] ? _newBPC_T_96 : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_99 = _newBPC_T_98 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_104 = _newBPC_T_99 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  newBPC_4_chain = _newBPC_T_104[11]; // @[CSR.scala 1057:96]
  wire [2:0] newBPC_4_action = _newBPC_T_104[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_4_dmode = _newBPC_T_104[59]; // @[CSR.scala 1057:96]
  wire  dMode_4 = newBPC_4_dmode & reg_debug & (reg_bp_3_control_dmode | ~reg_bp_3_control_chain); // @[CSR.scala 1058:51]
  wire [2:0] _GEN_709 = dMode_4 | newBPC_4_action > 3'h1 ? newBPC_4_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire  _reg_bp_4_control_chain_T_3 = ~reg_bp_5_control_dmode; // @[CSR.scala 1061:88]
  wire [63:0] _GEN_725 = 4'h4 == reg_tselect & (_reg_bp_3_control_chain_T_3 | reg_debug) ? _GEN_708 : {{25'd0},
    reg_bp_4_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire [63:0] _GEN_741 = _T_69 ? wdata : {{25'd0}, reg_bp_5_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo_5 = {reg_bp_5_control_m,1'h0,reg_bp_5_control_s,reg_bp_5_control_u,reg_bp_5_control_x,
    reg_bp_5_control_w,reg_bp_5_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_120 = {4'h2,reg_bp_5_control_dmode,44'h10000000000,reg_bp_5_control_action,
    reg_bp_5_control_chain,2'h0,reg_bp_5_control_tmatch,newBPC_lo_5}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_122 = io_rw_cmd[1] ? _newBPC_T_120 : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_123 = _newBPC_T_122 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_128 = _newBPC_T_123 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  newBPC_5_chain = _newBPC_T_128[11]; // @[CSR.scala 1057:96]
  wire [2:0] newBPC_5_action = _newBPC_T_128[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_5_dmode = _newBPC_T_128[59]; // @[CSR.scala 1057:96]
  wire  dMode_5 = newBPC_5_dmode & reg_debug & (reg_bp_4_control_dmode | ~reg_bp_4_control_chain); // @[CSR.scala 1058:51]
  wire [2:0] _GEN_742 = dMode_5 | newBPC_5_action > 3'h1 ? newBPC_5_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire  _reg_bp_5_control_chain_T_3 = ~reg_bp_6_control_dmode; // @[CSR.scala 1061:88]
  wire [63:0] _GEN_758 = 4'h5 == reg_tselect & (_reg_bp_4_control_chain_T_3 | reg_debug) ? _GEN_741 : {{25'd0},
    reg_bp_5_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire [63:0] _GEN_774 = _T_69 ? wdata : {{25'd0}, reg_bp_6_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo_6 = {reg_bp_6_control_m,1'h0,reg_bp_6_control_s,reg_bp_6_control_u,reg_bp_6_control_x,
    reg_bp_6_control_w,reg_bp_6_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_144 = {4'h2,reg_bp_6_control_dmode,44'h10000000000,reg_bp_6_control_action,
    reg_bp_6_control_chain,2'h0,reg_bp_6_control_tmatch,newBPC_lo_6}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_146 = io_rw_cmd[1] ? _newBPC_T_144 : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_147 = _newBPC_T_146 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_152 = _newBPC_T_147 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  newBPC_6_chain = _newBPC_T_152[11]; // @[CSR.scala 1057:96]
  wire [2:0] newBPC_6_action = _newBPC_T_152[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_6_dmode = _newBPC_T_152[59]; // @[CSR.scala 1057:96]
  wire  dMode_6 = newBPC_6_dmode & reg_debug & (reg_bp_5_control_dmode | ~reg_bp_5_control_chain); // @[CSR.scala 1058:51]
  wire [2:0] _GEN_775 = dMode_6 | newBPC_6_action > 3'h1 ? newBPC_6_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire  _reg_bp_6_control_chain_T_3 = ~reg_bp_7_control_dmode; // @[CSR.scala 1061:88]
  wire [63:0] _GEN_791 = 4'h6 == reg_tselect & (_reg_bp_5_control_chain_T_3 | reg_debug) ? _GEN_774 : {{25'd0},
    reg_bp_6_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire [63:0] _GEN_807 = _T_69 ? wdata : {{25'd0}, reg_bp_7_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo_7 = {reg_bp_7_control_m,1'h0,reg_bp_7_control_s,reg_bp_7_control_u,reg_bp_7_control_x,
    reg_bp_7_control_w,reg_bp_7_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_168 = {4'h2,reg_bp_7_control_dmode,44'h10000000000,reg_bp_7_control_action,
    reg_bp_7_control_chain,2'h0,reg_bp_7_control_tmatch,newBPC_lo_7}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_170 = io_rw_cmd[1] ? _newBPC_T_168 : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_171 = _newBPC_T_170 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_176 = _newBPC_T_171 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  newBPC_7_chain = _newBPC_T_176[11]; // @[CSR.scala 1057:96]
  wire [2:0] newBPC_7_action = _newBPC_T_176[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_7_dmode = _newBPC_T_176[59]; // @[CSR.scala 1057:96]
  wire  dMode_7 = newBPC_7_dmode & reg_debug & (reg_bp_6_control_dmode | ~reg_bp_6_control_chain); // @[CSR.scala 1058:51]
  wire [2:0] _GEN_808 = dMode_7 | newBPC_7_action > 3'h1 ? newBPC_7_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire  _reg_bp_7_control_chain_T_3 = ~reg_bp_8_control_dmode; // @[CSR.scala 1061:88]
  wire [63:0] _GEN_824 = 4'h7 == reg_tselect & (_reg_bp_6_control_chain_T_3 | reg_debug) ? _GEN_807 : {{25'd0},
    reg_bp_7_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire [63:0] _GEN_840 = _T_69 ? wdata : {{25'd0}, reg_bp_8_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo_8 = {reg_bp_8_control_m,1'h0,reg_bp_8_control_s,reg_bp_8_control_u,reg_bp_8_control_x,
    reg_bp_8_control_w,reg_bp_8_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_192 = {4'h2,reg_bp_8_control_dmode,44'h10000000000,reg_bp_8_control_action,
    reg_bp_8_control_chain,2'h0,reg_bp_8_control_tmatch,newBPC_lo_8}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_194 = io_rw_cmd[1] ? _newBPC_T_192 : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_195 = _newBPC_T_194 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_200 = _newBPC_T_195 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  newBPC_8_chain = _newBPC_T_200[11]; // @[CSR.scala 1057:96]
  wire [2:0] newBPC_8_action = _newBPC_T_200[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_8_dmode = _newBPC_T_200[59]; // @[CSR.scala 1057:96]
  wire  dMode_8 = newBPC_8_dmode & reg_debug & (reg_bp_7_control_dmode | ~reg_bp_7_control_chain); // @[CSR.scala 1058:51]
  wire [2:0] _GEN_841 = dMode_8 | newBPC_8_action > 3'h1 ? newBPC_8_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire  _reg_bp_8_control_chain_T_3 = ~reg_bp_9_control_dmode; // @[CSR.scala 1061:88]
  wire [63:0] _GEN_857 = 4'h8 == reg_tselect & (_reg_bp_7_control_chain_T_3 | reg_debug) ? _GEN_840 : {{25'd0},
    reg_bp_8_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire [63:0] _GEN_873 = _T_69 ? wdata : {{25'd0}, reg_bp_9_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo_9 = {reg_bp_9_control_m,1'h0,reg_bp_9_control_s,reg_bp_9_control_u,reg_bp_9_control_x,
    reg_bp_9_control_w,reg_bp_9_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_216 = {4'h2,reg_bp_9_control_dmode,44'h10000000000,reg_bp_9_control_action,
    reg_bp_9_control_chain,2'h0,reg_bp_9_control_tmatch,newBPC_lo_9}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_218 = io_rw_cmd[1] ? _newBPC_T_216 : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_219 = _newBPC_T_218 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_224 = _newBPC_T_219 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  newBPC_9_chain = _newBPC_T_224[11]; // @[CSR.scala 1057:96]
  wire [2:0] newBPC_9_action = _newBPC_T_224[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_9_dmode = _newBPC_T_224[59]; // @[CSR.scala 1057:96]
  wire  dMode_9 = newBPC_9_dmode & reg_debug & (reg_bp_8_control_dmode | ~reg_bp_8_control_chain); // @[CSR.scala 1058:51]
  wire [2:0] _GEN_874 = dMode_9 | newBPC_9_action > 3'h1 ? newBPC_9_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire  _reg_bp_9_control_chain_T_3 = ~reg_bp_10_control_dmode; // @[CSR.scala 1061:88]
  wire [63:0] _GEN_890 = 4'h9 == reg_tselect & (_reg_bp_8_control_chain_T_3 | reg_debug) ? _GEN_873 : {{25'd0},
    reg_bp_9_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire [63:0] _GEN_906 = _T_69 ? wdata : {{25'd0}, reg_bp_10_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo_10 = {reg_bp_10_control_m,1'h0,reg_bp_10_control_s,reg_bp_10_control_u,reg_bp_10_control_x,
    reg_bp_10_control_w,reg_bp_10_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_240 = {4'h2,reg_bp_10_control_dmode,44'h10000000000,reg_bp_10_control_action,
    reg_bp_10_control_chain,2'h0,reg_bp_10_control_tmatch,newBPC_lo_10}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_242 = io_rw_cmd[1] ? _newBPC_T_240 : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_243 = _newBPC_T_242 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_248 = _newBPC_T_243 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  newBPC_10_chain = _newBPC_T_248[11]; // @[CSR.scala 1057:96]
  wire [2:0] newBPC_10_action = _newBPC_T_248[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_10_dmode = _newBPC_T_248[59]; // @[CSR.scala 1057:96]
  wire  dMode_10 = newBPC_10_dmode & reg_debug & (reg_bp_9_control_dmode | ~reg_bp_9_control_chain); // @[CSR.scala 1058:51]
  wire [2:0] _GEN_907 = dMode_10 | newBPC_10_action > 3'h1 ? newBPC_10_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire  _reg_bp_10_control_chain_T_3 = ~reg_bp_11_control_dmode; // @[CSR.scala 1061:88]
  wire [63:0] _GEN_923 = 4'ha == reg_tselect & (_reg_bp_9_control_chain_T_3 | reg_debug) ? _GEN_906 : {{25'd0},
    reg_bp_10_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire [63:0] _GEN_939 = _T_69 ? wdata : {{25'd0}, reg_bp_11_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo_11 = {reg_bp_11_control_m,1'h0,reg_bp_11_control_s,reg_bp_11_control_u,reg_bp_11_control_x,
    reg_bp_11_control_w,reg_bp_11_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_264 = {4'h2,reg_bp_11_control_dmode,44'h10000000000,reg_bp_11_control_action,
    reg_bp_11_control_chain,2'h0,reg_bp_11_control_tmatch,newBPC_lo_11}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_266 = io_rw_cmd[1] ? _newBPC_T_264 : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_267 = _newBPC_T_266 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_272 = _newBPC_T_267 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  newBPC_11_chain = _newBPC_T_272[11]; // @[CSR.scala 1057:96]
  wire [2:0] newBPC_11_action = _newBPC_T_272[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_11_dmode = _newBPC_T_272[59]; // @[CSR.scala 1057:96]
  wire  dMode_11 = newBPC_11_dmode & reg_debug & (reg_bp_10_control_dmode | ~reg_bp_10_control_chain); // @[CSR.scala 1058:51]
  wire [2:0] _GEN_940 = dMode_11 | newBPC_11_action > 3'h1 ? newBPC_11_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire  _reg_bp_11_control_chain_T_3 = ~reg_bp_12_control_dmode; // @[CSR.scala 1061:88]
  wire [63:0] _GEN_956 = 4'hb == reg_tselect & (_reg_bp_10_control_chain_T_3 | reg_debug) ? _GEN_939 : {{25'd0},
    reg_bp_11_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire [63:0] _GEN_972 = _T_69 ? wdata : {{25'd0}, reg_bp_12_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo_12 = {reg_bp_12_control_m,1'h0,reg_bp_12_control_s,reg_bp_12_control_u,reg_bp_12_control_x,
    reg_bp_12_control_w,reg_bp_12_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_288 = {4'h2,reg_bp_12_control_dmode,44'h10000000000,reg_bp_12_control_action,
    reg_bp_12_control_chain,2'h0,reg_bp_12_control_tmatch,newBPC_lo_12}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_290 = io_rw_cmd[1] ? _newBPC_T_288 : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_291 = _newBPC_T_290 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_296 = _newBPC_T_291 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  newBPC_12_chain = _newBPC_T_296[11]; // @[CSR.scala 1057:96]
  wire [2:0] newBPC_12_action = _newBPC_T_296[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_12_dmode = _newBPC_T_296[59]; // @[CSR.scala 1057:96]
  wire  dMode_12 = newBPC_12_dmode & reg_debug & (reg_bp_11_control_dmode | ~reg_bp_11_control_chain); // @[CSR.scala 1058:51]
  wire [2:0] _GEN_973 = dMode_12 | newBPC_12_action > 3'h1 ? newBPC_12_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire  _reg_bp_12_control_chain_T_3 = ~reg_bp_13_control_dmode; // @[CSR.scala 1061:88]
  wire [63:0] _GEN_989 = 4'hc == reg_tselect & (_reg_bp_11_control_chain_T_3 | reg_debug) ? _GEN_972 : {{25'd0},
    reg_bp_12_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire [63:0] _GEN_1005 = _T_69 ? wdata : {{25'd0}, reg_bp_13_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo_13 = {reg_bp_13_control_m,1'h0,reg_bp_13_control_s,reg_bp_13_control_u,reg_bp_13_control_x,
    reg_bp_13_control_w,reg_bp_13_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_312 = {4'h2,reg_bp_13_control_dmode,44'h10000000000,reg_bp_13_control_action,
    reg_bp_13_control_chain,2'h0,reg_bp_13_control_tmatch,newBPC_lo_13}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_314 = io_rw_cmd[1] ? _newBPC_T_312 : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_315 = _newBPC_T_314 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_320 = _newBPC_T_315 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  newBPC_13_chain = _newBPC_T_320[11]; // @[CSR.scala 1057:96]
  wire [2:0] newBPC_13_action = _newBPC_T_320[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_13_dmode = _newBPC_T_320[59]; // @[CSR.scala 1057:96]
  wire  dMode_13 = newBPC_13_dmode & reg_debug & (reg_bp_12_control_dmode | ~reg_bp_12_control_chain); // @[CSR.scala 1058:51]
  wire [2:0] _GEN_1006 = dMode_13 | newBPC_13_action > 3'h1 ? newBPC_13_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire  _reg_bp_13_control_chain_T_3 = ~reg_bp_14_control_dmode; // @[CSR.scala 1061:88]
  wire [63:0] _GEN_1022 = 4'hd == reg_tselect & (_reg_bp_12_control_chain_T_3 | reg_debug) ? _GEN_1005 : {{25'd0},
    reg_bp_13_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire [63:0] _GEN_1038 = _T_69 ? wdata : {{25'd0}, reg_bp_14_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo_14 = {reg_bp_14_control_m,1'h0,reg_bp_14_control_s,reg_bp_14_control_u,reg_bp_14_control_x,
    reg_bp_14_control_w,reg_bp_14_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_336 = {4'h2,reg_bp_14_control_dmode,44'h10000000000,reg_bp_14_control_action,
    reg_bp_14_control_chain,2'h0,reg_bp_14_control_tmatch,newBPC_lo_14}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_338 = io_rw_cmd[1] ? _newBPC_T_336 : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_339 = _newBPC_T_338 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_344 = _newBPC_T_339 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire  newBPC_14_chain = _newBPC_T_344[11]; // @[CSR.scala 1057:96]
  wire [2:0] newBPC_14_action = _newBPC_T_344[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_14_dmode = _newBPC_T_344[59]; // @[CSR.scala 1057:96]
  wire  _dMode_T_43 = ~reg_bp_13_control_chain; // @[CSR.scala 1058:68]
  wire  dMode_14 = newBPC_14_dmode & reg_debug & (reg_bp_13_control_dmode | ~reg_bp_13_control_chain); // @[CSR.scala 1058:51]
  wire [2:0] _GEN_1039 = dMode_14 | newBPC_14_action > 3'h1 ? newBPC_14_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire  _reg_bp_14_control_chain_T_3 = ~reg_bp_15_control_dmode; // @[CSR.scala 1061:88]
  wire [63:0] _GEN_1055 = 4'he == reg_tselect & (_reg_bp_13_control_chain_T_3 | reg_debug) ? _GEN_1038 : {{25'd0},
    reg_bp_14_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire [63:0] _GEN_1071 = _T_69 ? wdata : {{25'd0}, reg_bp_15_address}; // @[CSR.scala 1039:44 CSR.scala 1039:57 CSR.scala 377:19]
  wire [6:0] newBPC_lo_15 = {reg_bp_15_control_m,1'h0,reg_bp_15_control_s,reg_bp_15_control_u,reg_bp_15_control_x,
    reg_bp_15_control_w,reg_bp_15_control_r}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_360 = {4'h2,reg_bp_15_control_dmode,44'h10000000000,reg_bp_15_control_action,1'h0,2'h0,
    reg_bp_15_control_tmatch,newBPC_lo_15}; // @[CSR.scala 1057:67]
  wire [63:0] _newBPC_T_362 = io_rw_cmd[1] ? _newBPC_T_360 : 64'h0; // @[CSR.scala 1182:9]
  wire [63:0] _newBPC_T_363 = _newBPC_T_362 | io_rw_wdata; // @[CSR.scala 1182:34]
  wire [63:0] _newBPC_T_368 = _newBPC_T_363 & _wdata_T_6; // @[CSR.scala 1182:43]
  wire [2:0] newBPC_15_action = _newBPC_T_368[14:12]; // @[CSR.scala 1057:96]
  wire  newBPC_15_dmode = _newBPC_T_368[59]; // @[CSR.scala 1057:96]
  wire  dMode_15 = newBPC_15_dmode & reg_debug & (reg_bp_14_control_dmode | ~reg_bp_14_control_chain); // @[CSR.scala 1058:51]
  wire [2:0] _GEN_1072 = dMode_15 | newBPC_15_action > 3'h1 ? newBPC_15_action : 3'h0; // @[CSR.scala 1060:51 CSR.scala 1060:71 CSR.scala 1060:120]
  wire [63:0] _GEN_1088 = 4'hf == reg_tselect & (_reg_bp_14_control_chain_T_3 | reg_debug) ? _GEN_1071 : {{25'd0},
    reg_bp_15_address}; // @[CSR.scala 1038:70 CSR.scala 377:19]
  wire  newCfg_r = wdata[0]; // @[CSR.scala 1071:46]
  wire  newCfg_w = wdata[1]; // @[CSR.scala 1071:46]
  wire  newCfg_x = wdata[2]; // @[CSR.scala 1071:46]
  wire [1:0] newCfg_a = wdata[4:3]; // @[CSR.scala 1071:46]
  wire  newCfg_l = wdata[7]; // @[CSR.scala 1071:46]
  wire  _T_3235 = ~reg_pmp_1_cfg_a[1] & pmp_mask_base_lo_1; // @[PMP.scala 49:20]
  wire  _T_3237 = reg_pmp_0_cfg_l | reg_pmp_1_cfg_l & _T_3235; // @[PMP.scala 51:44]
  wire [63:0] _GEN_1110 = _T_198 & ~_T_3237 ? wdata : {{29'd0}, reg_pmp_0_addr}; // @[CSR.scala 1079:71 CSR.scala 1080:18 CSR.scala 378:20]
  wire  newCfg_1_r = wdata[8]; // @[CSR.scala 1071:46]
  wire  newCfg_1_w = wdata[9]; // @[CSR.scala 1071:46]
  wire  newCfg_1_x = wdata[10]; // @[CSR.scala 1071:46]
  wire [1:0] newCfg_1_a = wdata[12:11]; // @[CSR.scala 1071:46]
  wire  newCfg_1_l = wdata[15]; // @[CSR.scala 1071:46]
  wire  _T_3245 = ~reg_pmp_2_cfg_a[1] & pmp_mask_base_lo_2; // @[PMP.scala 49:20]
  wire  _T_3247 = reg_pmp_1_cfg_l | reg_pmp_2_cfg_l & _T_3245; // @[PMP.scala 51:44]
  wire [63:0] _GEN_1117 = _T_199 & ~_T_3247 ? wdata : {{29'd0}, reg_pmp_1_addr}; // @[CSR.scala 1079:71 CSR.scala 1080:18 CSR.scala 378:20]
  wire  newCfg_2_r = wdata[16]; // @[CSR.scala 1071:46]
  wire  newCfg_2_w = wdata[17]; // @[CSR.scala 1071:46]
  wire  newCfg_2_x = wdata[18]; // @[CSR.scala 1071:46]
  wire [1:0] newCfg_2_a = wdata[20:19]; // @[CSR.scala 1071:46]
  wire  newCfg_2_l = wdata[23]; // @[CSR.scala 1071:46]
  wire  _T_3255 = ~reg_pmp_3_cfg_a[1] & pmp_mask_base_lo_3; // @[PMP.scala 49:20]
  wire  _T_3257 = reg_pmp_2_cfg_l | reg_pmp_3_cfg_l & _T_3255; // @[PMP.scala 51:44]
  wire [63:0] _GEN_1124 = _T_200 & ~_T_3257 ? wdata : {{29'd0}, reg_pmp_2_addr}; // @[CSR.scala 1079:71 CSR.scala 1080:18 CSR.scala 378:20]
  wire  newCfg_3_r = wdata[24]; // @[CSR.scala 1071:46]
  wire  newCfg_3_w = wdata[25]; // @[CSR.scala 1071:46]
  wire  newCfg_3_x = wdata[26]; // @[CSR.scala 1071:46]
  wire [1:0] newCfg_3_a = wdata[28:27]; // @[CSR.scala 1071:46]
  wire  newCfg_3_l = wdata[31]; // @[CSR.scala 1071:46]
  wire  _T_3265 = ~reg_pmp_4_cfg_a[1] & pmp_mask_base_lo_4; // @[PMP.scala 49:20]
  wire  _T_3267 = reg_pmp_3_cfg_l | reg_pmp_4_cfg_l & _T_3265; // @[PMP.scala 51:44]
  wire [63:0] _GEN_1131 = _T_201 & ~_T_3267 ? wdata : {{29'd0}, reg_pmp_3_addr}; // @[CSR.scala 1079:71 CSR.scala 1080:18 CSR.scala 378:20]
  wire  newCfg_4_r = wdata[32]; // @[CSR.scala 1071:46]
  wire  newCfg_4_w = wdata[33]; // @[CSR.scala 1071:46]
  wire  newCfg_4_x = wdata[34]; // @[CSR.scala 1071:46]
  wire [1:0] newCfg_4_a = wdata[36:35]; // @[CSR.scala 1071:46]
  wire  newCfg_4_l = wdata[39]; // @[CSR.scala 1071:46]
  wire  _T_3275 = ~reg_pmp_5_cfg_a[1] & pmp_mask_base_lo_5; // @[PMP.scala 49:20]
  wire  _T_3277 = reg_pmp_4_cfg_l | reg_pmp_5_cfg_l & _T_3275; // @[PMP.scala 51:44]
  wire [63:0] _GEN_1138 = _T_202 & ~_T_3277 ? wdata : {{29'd0}, reg_pmp_4_addr}; // @[CSR.scala 1079:71 CSR.scala 1080:18 CSR.scala 378:20]
  wire  newCfg_5_r = wdata[40]; // @[CSR.scala 1071:46]
  wire  newCfg_5_w = wdata[41]; // @[CSR.scala 1071:46]
  wire  newCfg_5_x = wdata[42]; // @[CSR.scala 1071:46]
  wire [1:0] newCfg_5_a = wdata[44:43]; // @[CSR.scala 1071:46]
  wire  newCfg_5_l = wdata[47]; // @[CSR.scala 1071:46]
  wire  _T_3285 = ~reg_pmp_6_cfg_a[1] & pmp_mask_base_lo_6; // @[PMP.scala 49:20]
  wire  _T_3287 = reg_pmp_5_cfg_l | reg_pmp_6_cfg_l & _T_3285; // @[PMP.scala 51:44]
  wire [63:0] _GEN_1145 = _T_203 & ~_T_3287 ? wdata : {{29'd0}, reg_pmp_5_addr}; // @[CSR.scala 1079:71 CSR.scala 1080:18 CSR.scala 378:20]
  wire  newCfg_6_r = wdata[48]; // @[CSR.scala 1071:46]
  wire  newCfg_6_w = wdata[49]; // @[CSR.scala 1071:46]
  wire  newCfg_6_x = wdata[50]; // @[CSR.scala 1071:46]
  wire [1:0] newCfg_6_a = wdata[52:51]; // @[CSR.scala 1071:46]
  wire  newCfg_6_l = wdata[55]; // @[CSR.scala 1071:46]
  wire  _T_3295 = ~reg_pmp_7_cfg_a[1] & pmp_mask_base_lo_7; // @[PMP.scala 49:20]
  wire  _T_3297 = reg_pmp_6_cfg_l | reg_pmp_7_cfg_l & _T_3295; // @[PMP.scala 51:44]
  wire [63:0] _GEN_1152 = _T_204 & ~_T_3297 ? wdata : {{29'd0}, reg_pmp_6_addr}; // @[CSR.scala 1079:71 CSR.scala 1080:18 CSR.scala 378:20]
  wire  newCfg_7_r = wdata[56]; // @[CSR.scala 1071:46]
  wire  newCfg_7_w = wdata[57]; // @[CSR.scala 1071:46]
  wire  newCfg_7_x = wdata[58]; // @[CSR.scala 1071:46]
  wire [1:0] newCfg_7_a = wdata[60:59]; // @[CSR.scala 1071:46]
  wire  newCfg_7_l = wdata[63]; // @[CSR.scala 1071:46]
  wire  _T_3307 = reg_pmp_7_cfg_l | reg_pmp_7_cfg_l & _T_3295; // @[PMP.scala 51:44]
  wire [63:0] _GEN_1159 = _T_205 & ~_T_3307 ? wdata : {{29'd0}, reg_pmp_7_addr}; // @[CSR.scala 1079:71 CSR.scala 1080:18 CSR.scala 378:20]
  wire [63:0] _reg_custom_0_T = wdata & 64'h1; // @[CSR.scala 1086:23]
  wire [63:0] _reg_custom_0_T_2 = reg_custom_0 & 64'hfffffffffffffffe; // @[CSR.scala 1086:38]
  wire [63:0] _reg_custom_0_T_3 = _reg_custom_0_T | _reg_custom_0_T_2; // @[CSR.scala 1086:31]
  wire [63:0] _reg_custom_1_T = wdata & 64'h2020f; // @[CSR.scala 1086:23]
  wire [63:0] _reg_custom_1_T_2 = reg_custom_1 & 64'hfffffffffffdfdf0; // @[CSR.scala 1086:38]
  wire [63:0] _reg_custom_1_T_3 = _reg_custom_1_T | _reg_custom_1_T_2; // @[CSR.scala 1086:31]
  wire [1:0] _GEN_1174 = csr_wen ? _GEN_555 : _GEN_480; // @[CSR.scala 893:18]
  wire [63:0] _GEN_1189 = csr_wen ? _GEN_512 : {{24'd0}, _GEN_448}; // @[CSR.scala 893:18]
  wire [63:0] _GEN_1191 = csr_wen ? _GEN_514 : {{27'd0}, reg_mtvec}; // @[CSR.scala 893:18 CSR.scala 396:27]
  wire [63:0] _GEN_1195 = csr_wen ? _GEN_518 : {{24'd0}, _GEN_438}; // @[CSR.scala 893:18]
  wire  _GEN_1198 = csr_wen ? _GEN_521 : _GEN_483; // @[CSR.scala 893:18]
  wire [39:0] _GEN_1199 = csr_wen ? _GEN_522 : {{33'd0}, _GEN_4}; // @[CSR.scala 893:18]
  wire [39:0] _GEN_1202 = csr_wen ? _GEN_525 : {{33'd0}, _GEN_6}; // @[CSR.scala 893:18]
  wire [39:0] _GEN_1205 = csr_wen ? _GEN_528 : {{33'd0}, _GEN_8}; // @[CSR.scala 893:18]
  wire [39:0] _GEN_1208 = csr_wen ? _GEN_531 : {{33'd0}, _GEN_10}; // @[CSR.scala 893:18]
  wire [63:0] _GEN_1211 = csr_wen ? _GEN_534 : {{57'd0}, reg_mcountinhibit}; // @[CSR.scala 893:18 CSR.scala 434:34]
  wire [63:0] _GEN_1212 = csr_wen ? _GEN_535 : {{57'd0}, _GEN_2}; // @[CSR.scala 893:18]
  wire [63:0] _GEN_1214 = csr_wen ? _GEN_537 : {{57'd0}, _GEN_0}; // @[CSR.scala 893:18]
  wire [63:0] _GEN_1217 = csr_wen ? _GEN_544 : {{59'd0}, _GEN_490}; // @[CSR.scala 893:18]
  wire [63:0] _GEN_1218 = csr_wen ? _GEN_545 : {{61'd0}, reg_frm}; // @[CSR.scala 893:18 CSR.scala 428:20]
  wire [63:0] _GEN_1224 = csr_wen ? _GEN_551 : {{24'd0}, _GEN_433}; // @[CSR.scala 893:18]
  wire [63:0] _GEN_1229 = csr_wen ? _GEN_567 : {{24'd0}, _GEN_441}; // @[CSR.scala 893:18]
  wire [63:0] _GEN_1230 = csr_wen ? _GEN_568 : {{25'd0}, reg_stvec}; // @[CSR.scala 893:18 CSR.scala 423:22]
  wire [63:0] _GEN_1235 = csr_wen ? _GEN_573 : {{32'd0}, reg_scounteren}; // @[CSR.scala 893:18 CSR.scala 415:18]
  wire [63:0] _GEN_1236 = csr_wen ? _GEN_574 : {{32'd0}, reg_mcounteren}; // @[CSR.scala 893:18 CSR.scala 411:18]
  wire [63:0] _GEN_1237 = csr_wen ? _GEN_575 : {{60'd0}, reg_tselect}; // @[CSR.scala 893:18 CSR.scala 376:24]
  wire [63:0] _GEN_1238 = csr_wen ? _GEN_593 : {{25'd0}, reg_bp_0_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1254 = csr_wen ? _GEN_626 : {{25'd0}, reg_bp_1_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1270 = csr_wen ? _GEN_659 : {{25'd0}, reg_bp_2_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1286 = csr_wen ? _GEN_692 : {{25'd0}, reg_bp_3_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1302 = csr_wen ? _GEN_725 : {{25'd0}, reg_bp_4_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1318 = csr_wen ? _GEN_758 : {{25'd0}, reg_bp_5_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1334 = csr_wen ? _GEN_791 : {{25'd0}, reg_bp_6_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1350 = csr_wen ? _GEN_824 : {{25'd0}, reg_bp_7_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1366 = csr_wen ? _GEN_857 : {{25'd0}, reg_bp_8_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1382 = csr_wen ? _GEN_890 : {{25'd0}, reg_bp_9_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1398 = csr_wen ? _GEN_923 : {{25'd0}, reg_bp_10_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1414 = csr_wen ? _GEN_956 : {{25'd0}, reg_bp_11_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1430 = csr_wen ? _GEN_989 : {{25'd0}, reg_bp_12_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1446 = csr_wen ? _GEN_1022 : {{25'd0}, reg_bp_13_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1462 = csr_wen ? _GEN_1055 : {{25'd0}, reg_bp_14_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1478 = csr_wen ? _GEN_1088 : {{25'd0}, reg_bp_15_address}; // @[CSR.scala 893:18 CSR.scala 377:19]
  wire [63:0] _GEN_1500 = csr_wen ? _GEN_1110 : {{29'd0}, reg_pmp_0_addr}; // @[CSR.scala 893:18 CSR.scala 378:20]
  wire [63:0] _GEN_1507 = csr_wen ? _GEN_1117 : {{29'd0}, reg_pmp_1_addr}; // @[CSR.scala 893:18 CSR.scala 378:20]
  wire [63:0] _GEN_1514 = csr_wen ? _GEN_1124 : {{29'd0}, reg_pmp_2_addr}; // @[CSR.scala 893:18 CSR.scala 378:20]
  wire [63:0] _GEN_1521 = csr_wen ? _GEN_1131 : {{29'd0}, reg_pmp_3_addr}; // @[CSR.scala 893:18 CSR.scala 378:20]
  wire [63:0] _GEN_1528 = csr_wen ? _GEN_1138 : {{29'd0}, reg_pmp_4_addr}; // @[CSR.scala 893:18 CSR.scala 378:20]
  wire [63:0] _GEN_1535 = csr_wen ? _GEN_1145 : {{29'd0}, reg_pmp_5_addr}; // @[CSR.scala 893:18 CSR.scala 378:20]
  wire [63:0] _GEN_1542 = csr_wen ? _GEN_1152 : {{29'd0}, reg_pmp_6_addr}; // @[CSR.scala 893:18 CSR.scala 378:20]
  wire [63:0] _GEN_1549 = csr_wen ? _GEN_1159 : {{29'd0}, reg_pmp_7_addr}; // @[CSR.scala 893:18 CSR.scala 378:20]
  assign io_rw_rdata = _io_rw_rdata_T_302[63:0]; // @[CSR.scala 851:15]
  assign io_decode_0_fp_illegal = io_status_fs == 2'h0 | ~reg_misa[5]; // @[CSR.scala 667:45]
  assign io_decode_0_fp_csr = _io_decode_0_fp_csr_T == 12'h0; // @[Decode.scala 14:121]
  assign io_decode_0_read_illegal = _io_decode_0_read_illegal_T_327 | _io_decode_0_read_illegal_T_330; // @[CSR.scala 676:68]
  assign io_decode_0_write_illegal = &io_decode_0_csr[11:10]; // @[CSR.scala 678:47]
  assign io_decode_0_write_flush = ~(io_decode_0_csr >= 12'h340 & io_decode_0_csr <= 12'h343 | io_decode_0_csr >= 12'h140
     & io_decode_0_csr <= 12'h143); // @[CSR.scala 679:27]
  assign io_decode_0_system_illegal = _io_decode_0_system_illegal_T_14 | _io_decode_0_system_illegal_T_16; // @[CSR.scala 683:63]
  assign io_csr_stall = reg_wfi | io_status_cease; // @[CSR.scala 841:27]
  assign io_eret = _exception_T | insn_ret; // @[CSR.scala 725:38]
  assign io_singleStep = reg_dcsr_step & _T_304; // @[CSR.scala 726:34]
  assign io_status_debug = reg_debug; // @[CSR.scala 729:19]
  assign io_status_cease = io_status_cease_r; // @[CSR.scala 842:19]
  assign io_status_wfi = reg_wfi; // @[CSR.scala 843:17]
  assign io_status_isa = reg_misa[31:0]; // @[CSR.scala 730:17]
  assign io_status_dprv = io_status_dprv_REG; // @[CSR.scala 733:18]
  assign io_status_prv = reg_mstatus_prv; // @[CSR.scala 727:13]
  assign io_status_sd = &io_status_fs | &io_status_xs | &io_status_vs; // @[CSR.scala 728:58]
  assign io_status_zero2 = reg_mstatus_zero2; // @[CSR.scala 727:13]
  assign io_status_sxl = 2'h2; // @[CSR.scala 732:17]
  assign io_status_uxl = 2'h2; // @[CSR.scala 731:17]
  assign io_status_sd_rv32 = reg_mstatus_sd_rv32; // @[CSR.scala 727:13]
  assign io_status_zero1 = reg_mstatus_zero1; // @[CSR.scala 727:13]
  assign io_status_tsr = reg_mstatus_tsr; // @[CSR.scala 727:13]
  assign io_status_tw = reg_mstatus_tw; // @[CSR.scala 727:13]
  assign io_status_tvm = reg_mstatus_tvm; // @[CSR.scala 727:13]
  assign io_status_mxr = reg_mstatus_mxr; // @[CSR.scala 727:13]
  assign io_status_sum = reg_mstatus_sum; // @[CSR.scala 727:13]
  assign io_status_mprv = reg_mstatus_mprv; // @[CSR.scala 727:13]
  assign io_status_xs = reg_mstatus_xs; // @[CSR.scala 727:13]
  assign io_status_fs = reg_mstatus_fs; // @[CSR.scala 727:13]
  assign io_status_mpp = reg_mstatus_mpp; // @[CSR.scala 727:13]
  assign io_status_vs = reg_mstatus_vs; // @[CSR.scala 727:13]
  assign io_status_spp = reg_mstatus_spp; // @[CSR.scala 727:13]
  assign io_status_mpie = reg_mstatus_mpie; // @[CSR.scala 727:13]
  assign io_status_hpie = reg_mstatus_hpie; // @[CSR.scala 727:13]
  assign io_status_spie = reg_mstatus_spie; // @[CSR.scala 727:13]
  assign io_status_upie = reg_mstatus_upie; // @[CSR.scala 727:13]
  assign io_status_mie = reg_mstatus_mie; // @[CSR.scala 727:13]
  assign io_status_hie = reg_mstatus_hie; // @[CSR.scala 727:13]
  assign io_status_sie = reg_mstatus_sie; // @[CSR.scala 727:13]
  assign io_status_uie = reg_mstatus_uie; // @[CSR.scala 727:13]
  assign io_ptbr_mode = reg_satp_mode; // @[CSR.scala 724:11]
  assign io_ptbr_ppn = reg_satp_ppn; // @[CSR.scala 724:11]
  assign io_evec = _GEN_481[39:0];
  assign io_time = value_1; // @[CSR.scala 840:11]
  assign io_fcsr_rm = reg_frm; // @[CSR.scala 878:14]
  assign io_interrupt = (anyInterrupt & ~io_singleStep | reg_singleStepped) & ~(reg_debug | io_status_cease); // @[CSR.scala 468:73]
  assign io_interrupt_cause = _interruptCause_T_2 + _GEN_119; // @[CSR.scala 467:67]
  assign io_bp_0_control_action = reg_bp_0_control_action; // @[CSR.scala 470:9]
  assign io_bp_0_control_chain = reg_bp_0_control_chain; // @[CSR.scala 470:9]
  assign io_bp_0_control_tmatch = reg_bp_0_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_0_control_m = reg_bp_0_control_m; // @[CSR.scala 470:9]
  assign io_bp_0_control_s = reg_bp_0_control_s; // @[CSR.scala 470:9]
  assign io_bp_0_control_u = reg_bp_0_control_u; // @[CSR.scala 470:9]
  assign io_bp_0_control_x = reg_bp_0_control_x; // @[CSR.scala 470:9]
  assign io_bp_0_control_w = reg_bp_0_control_w; // @[CSR.scala 470:9]
  assign io_bp_0_control_r = reg_bp_0_control_r; // @[CSR.scala 470:9]
  assign io_bp_0_address = reg_bp_0_address; // @[CSR.scala 470:9]
  assign io_bp_1_control_action = reg_bp_1_control_action; // @[CSR.scala 470:9]
  assign io_bp_1_control_chain = reg_bp_1_control_chain; // @[CSR.scala 470:9]
  assign io_bp_1_control_tmatch = reg_bp_1_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_1_control_m = reg_bp_1_control_m; // @[CSR.scala 470:9]
  assign io_bp_1_control_s = reg_bp_1_control_s; // @[CSR.scala 470:9]
  assign io_bp_1_control_u = reg_bp_1_control_u; // @[CSR.scala 470:9]
  assign io_bp_1_control_x = reg_bp_1_control_x; // @[CSR.scala 470:9]
  assign io_bp_1_control_w = reg_bp_1_control_w; // @[CSR.scala 470:9]
  assign io_bp_1_control_r = reg_bp_1_control_r; // @[CSR.scala 470:9]
  assign io_bp_1_address = reg_bp_1_address; // @[CSR.scala 470:9]
  assign io_bp_2_control_action = reg_bp_2_control_action; // @[CSR.scala 470:9]
  assign io_bp_2_control_chain = reg_bp_2_control_chain; // @[CSR.scala 470:9]
  assign io_bp_2_control_tmatch = reg_bp_2_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_2_control_m = reg_bp_2_control_m; // @[CSR.scala 470:9]
  assign io_bp_2_control_s = reg_bp_2_control_s; // @[CSR.scala 470:9]
  assign io_bp_2_control_u = reg_bp_2_control_u; // @[CSR.scala 470:9]
  assign io_bp_2_control_x = reg_bp_2_control_x; // @[CSR.scala 470:9]
  assign io_bp_2_control_w = reg_bp_2_control_w; // @[CSR.scala 470:9]
  assign io_bp_2_control_r = reg_bp_2_control_r; // @[CSR.scala 470:9]
  assign io_bp_2_address = reg_bp_2_address; // @[CSR.scala 470:9]
  assign io_bp_3_control_action = reg_bp_3_control_action; // @[CSR.scala 470:9]
  assign io_bp_3_control_chain = reg_bp_3_control_chain; // @[CSR.scala 470:9]
  assign io_bp_3_control_tmatch = reg_bp_3_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_3_control_m = reg_bp_3_control_m; // @[CSR.scala 470:9]
  assign io_bp_3_control_s = reg_bp_3_control_s; // @[CSR.scala 470:9]
  assign io_bp_3_control_u = reg_bp_3_control_u; // @[CSR.scala 470:9]
  assign io_bp_3_control_x = reg_bp_3_control_x; // @[CSR.scala 470:9]
  assign io_bp_3_control_w = reg_bp_3_control_w; // @[CSR.scala 470:9]
  assign io_bp_3_control_r = reg_bp_3_control_r; // @[CSR.scala 470:9]
  assign io_bp_3_address = reg_bp_3_address; // @[CSR.scala 470:9]
  assign io_bp_4_control_action = reg_bp_4_control_action; // @[CSR.scala 470:9]
  assign io_bp_4_control_chain = reg_bp_4_control_chain; // @[CSR.scala 470:9]
  assign io_bp_4_control_tmatch = reg_bp_4_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_4_control_m = reg_bp_4_control_m; // @[CSR.scala 470:9]
  assign io_bp_4_control_s = reg_bp_4_control_s; // @[CSR.scala 470:9]
  assign io_bp_4_control_u = reg_bp_4_control_u; // @[CSR.scala 470:9]
  assign io_bp_4_control_x = reg_bp_4_control_x; // @[CSR.scala 470:9]
  assign io_bp_4_control_w = reg_bp_4_control_w; // @[CSR.scala 470:9]
  assign io_bp_4_control_r = reg_bp_4_control_r; // @[CSR.scala 470:9]
  assign io_bp_4_address = reg_bp_4_address; // @[CSR.scala 470:9]
  assign io_bp_5_control_action = reg_bp_5_control_action; // @[CSR.scala 470:9]
  assign io_bp_5_control_chain = reg_bp_5_control_chain; // @[CSR.scala 470:9]
  assign io_bp_5_control_tmatch = reg_bp_5_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_5_control_m = reg_bp_5_control_m; // @[CSR.scala 470:9]
  assign io_bp_5_control_s = reg_bp_5_control_s; // @[CSR.scala 470:9]
  assign io_bp_5_control_u = reg_bp_5_control_u; // @[CSR.scala 470:9]
  assign io_bp_5_control_x = reg_bp_5_control_x; // @[CSR.scala 470:9]
  assign io_bp_5_control_w = reg_bp_5_control_w; // @[CSR.scala 470:9]
  assign io_bp_5_control_r = reg_bp_5_control_r; // @[CSR.scala 470:9]
  assign io_bp_5_address = reg_bp_5_address; // @[CSR.scala 470:9]
  assign io_bp_6_control_action = reg_bp_6_control_action; // @[CSR.scala 470:9]
  assign io_bp_6_control_chain = reg_bp_6_control_chain; // @[CSR.scala 470:9]
  assign io_bp_6_control_tmatch = reg_bp_6_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_6_control_m = reg_bp_6_control_m; // @[CSR.scala 470:9]
  assign io_bp_6_control_s = reg_bp_6_control_s; // @[CSR.scala 470:9]
  assign io_bp_6_control_u = reg_bp_6_control_u; // @[CSR.scala 470:9]
  assign io_bp_6_control_x = reg_bp_6_control_x; // @[CSR.scala 470:9]
  assign io_bp_6_control_w = reg_bp_6_control_w; // @[CSR.scala 470:9]
  assign io_bp_6_control_r = reg_bp_6_control_r; // @[CSR.scala 470:9]
  assign io_bp_6_address = reg_bp_6_address; // @[CSR.scala 470:9]
  assign io_bp_7_control_action = reg_bp_7_control_action; // @[CSR.scala 470:9]
  assign io_bp_7_control_chain = reg_bp_7_control_chain; // @[CSR.scala 470:9]
  assign io_bp_7_control_tmatch = reg_bp_7_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_7_control_m = reg_bp_7_control_m; // @[CSR.scala 470:9]
  assign io_bp_7_control_s = reg_bp_7_control_s; // @[CSR.scala 470:9]
  assign io_bp_7_control_u = reg_bp_7_control_u; // @[CSR.scala 470:9]
  assign io_bp_7_control_x = reg_bp_7_control_x; // @[CSR.scala 470:9]
  assign io_bp_7_control_w = reg_bp_7_control_w; // @[CSR.scala 470:9]
  assign io_bp_7_control_r = reg_bp_7_control_r; // @[CSR.scala 470:9]
  assign io_bp_7_address = reg_bp_7_address; // @[CSR.scala 470:9]
  assign io_bp_8_control_action = reg_bp_8_control_action; // @[CSR.scala 470:9]
  assign io_bp_8_control_chain = reg_bp_8_control_chain; // @[CSR.scala 470:9]
  assign io_bp_8_control_tmatch = reg_bp_8_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_8_control_m = reg_bp_8_control_m; // @[CSR.scala 470:9]
  assign io_bp_8_control_s = reg_bp_8_control_s; // @[CSR.scala 470:9]
  assign io_bp_8_control_u = reg_bp_8_control_u; // @[CSR.scala 470:9]
  assign io_bp_8_control_x = reg_bp_8_control_x; // @[CSR.scala 470:9]
  assign io_bp_8_control_w = reg_bp_8_control_w; // @[CSR.scala 470:9]
  assign io_bp_8_control_r = reg_bp_8_control_r; // @[CSR.scala 470:9]
  assign io_bp_8_address = reg_bp_8_address; // @[CSR.scala 470:9]
  assign io_bp_9_control_action = reg_bp_9_control_action; // @[CSR.scala 470:9]
  assign io_bp_9_control_chain = reg_bp_9_control_chain; // @[CSR.scala 470:9]
  assign io_bp_9_control_tmatch = reg_bp_9_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_9_control_m = reg_bp_9_control_m; // @[CSR.scala 470:9]
  assign io_bp_9_control_s = reg_bp_9_control_s; // @[CSR.scala 470:9]
  assign io_bp_9_control_u = reg_bp_9_control_u; // @[CSR.scala 470:9]
  assign io_bp_9_control_x = reg_bp_9_control_x; // @[CSR.scala 470:9]
  assign io_bp_9_control_w = reg_bp_9_control_w; // @[CSR.scala 470:9]
  assign io_bp_9_control_r = reg_bp_9_control_r; // @[CSR.scala 470:9]
  assign io_bp_9_address = reg_bp_9_address; // @[CSR.scala 470:9]
  assign io_bp_10_control_action = reg_bp_10_control_action; // @[CSR.scala 470:9]
  assign io_bp_10_control_chain = reg_bp_10_control_chain; // @[CSR.scala 470:9]
  assign io_bp_10_control_tmatch = reg_bp_10_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_10_control_m = reg_bp_10_control_m; // @[CSR.scala 470:9]
  assign io_bp_10_control_s = reg_bp_10_control_s; // @[CSR.scala 470:9]
  assign io_bp_10_control_u = reg_bp_10_control_u; // @[CSR.scala 470:9]
  assign io_bp_10_control_x = reg_bp_10_control_x; // @[CSR.scala 470:9]
  assign io_bp_10_control_w = reg_bp_10_control_w; // @[CSR.scala 470:9]
  assign io_bp_10_control_r = reg_bp_10_control_r; // @[CSR.scala 470:9]
  assign io_bp_10_address = reg_bp_10_address; // @[CSR.scala 470:9]
  assign io_bp_11_control_action = reg_bp_11_control_action; // @[CSR.scala 470:9]
  assign io_bp_11_control_chain = reg_bp_11_control_chain; // @[CSR.scala 470:9]
  assign io_bp_11_control_tmatch = reg_bp_11_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_11_control_m = reg_bp_11_control_m; // @[CSR.scala 470:9]
  assign io_bp_11_control_s = reg_bp_11_control_s; // @[CSR.scala 470:9]
  assign io_bp_11_control_u = reg_bp_11_control_u; // @[CSR.scala 470:9]
  assign io_bp_11_control_x = reg_bp_11_control_x; // @[CSR.scala 470:9]
  assign io_bp_11_control_w = reg_bp_11_control_w; // @[CSR.scala 470:9]
  assign io_bp_11_control_r = reg_bp_11_control_r; // @[CSR.scala 470:9]
  assign io_bp_11_address = reg_bp_11_address; // @[CSR.scala 470:9]
  assign io_bp_12_control_action = reg_bp_12_control_action; // @[CSR.scala 470:9]
  assign io_bp_12_control_chain = reg_bp_12_control_chain; // @[CSR.scala 470:9]
  assign io_bp_12_control_tmatch = reg_bp_12_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_12_control_m = reg_bp_12_control_m; // @[CSR.scala 470:9]
  assign io_bp_12_control_s = reg_bp_12_control_s; // @[CSR.scala 470:9]
  assign io_bp_12_control_u = reg_bp_12_control_u; // @[CSR.scala 470:9]
  assign io_bp_12_control_x = reg_bp_12_control_x; // @[CSR.scala 470:9]
  assign io_bp_12_control_w = reg_bp_12_control_w; // @[CSR.scala 470:9]
  assign io_bp_12_control_r = reg_bp_12_control_r; // @[CSR.scala 470:9]
  assign io_bp_12_address = reg_bp_12_address; // @[CSR.scala 470:9]
  assign io_bp_13_control_action = reg_bp_13_control_action; // @[CSR.scala 470:9]
  assign io_bp_13_control_chain = reg_bp_13_control_chain; // @[CSR.scala 470:9]
  assign io_bp_13_control_tmatch = reg_bp_13_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_13_control_m = reg_bp_13_control_m; // @[CSR.scala 470:9]
  assign io_bp_13_control_s = reg_bp_13_control_s; // @[CSR.scala 470:9]
  assign io_bp_13_control_u = reg_bp_13_control_u; // @[CSR.scala 470:9]
  assign io_bp_13_control_x = reg_bp_13_control_x; // @[CSR.scala 470:9]
  assign io_bp_13_control_w = reg_bp_13_control_w; // @[CSR.scala 470:9]
  assign io_bp_13_control_r = reg_bp_13_control_r; // @[CSR.scala 470:9]
  assign io_bp_13_address = reg_bp_13_address; // @[CSR.scala 470:9]
  assign io_bp_14_control_action = reg_bp_14_control_action; // @[CSR.scala 470:9]
  assign io_bp_14_control_chain = reg_bp_14_control_chain; // @[CSR.scala 470:9]
  assign io_bp_14_control_tmatch = reg_bp_14_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_14_control_m = reg_bp_14_control_m; // @[CSR.scala 470:9]
  assign io_bp_14_control_s = reg_bp_14_control_s; // @[CSR.scala 470:9]
  assign io_bp_14_control_u = reg_bp_14_control_u; // @[CSR.scala 470:9]
  assign io_bp_14_control_x = reg_bp_14_control_x; // @[CSR.scala 470:9]
  assign io_bp_14_control_w = reg_bp_14_control_w; // @[CSR.scala 470:9]
  assign io_bp_14_control_r = reg_bp_14_control_r; // @[CSR.scala 470:9]
  assign io_bp_14_address = reg_bp_14_address; // @[CSR.scala 470:9]
  assign io_bp_15_control_action = reg_bp_15_control_action; // @[CSR.scala 470:9]
  assign io_bp_15_control_tmatch = reg_bp_15_control_tmatch; // @[CSR.scala 470:9]
  assign io_bp_15_control_m = reg_bp_15_control_m; // @[CSR.scala 470:9]
  assign io_bp_15_control_s = reg_bp_15_control_s; // @[CSR.scala 470:9]
  assign io_bp_15_control_u = reg_bp_15_control_u; // @[CSR.scala 470:9]
  assign io_bp_15_control_x = reg_bp_15_control_x; // @[CSR.scala 470:9]
  assign io_bp_15_control_w = reg_bp_15_control_w; // @[CSR.scala 470:9]
  assign io_bp_15_control_r = reg_bp_15_control_r; // @[CSR.scala 470:9]
  assign io_bp_15_address = reg_bp_15_address; // @[CSR.scala 470:9]
  assign io_pmp_0_cfg_l = reg_pmp_0_cfg_l; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_0_cfg_a = reg_pmp_0_cfg_a; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_0_cfg_x = reg_pmp_0_cfg_x; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_0_cfg_w = reg_pmp_0_cfg_w; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_0_cfg_r = reg_pmp_0_cfg_r; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_0_addr = reg_pmp_0_addr; // @[PMP.scala 26:19 PMP.scala 28:14]
  assign io_pmp_0_mask = _pmp_mask_T_3[36:0]; // @[PMP.scala 26:19 PMP.scala 29:14]
  assign io_pmp_1_cfg_l = reg_pmp_1_cfg_l; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_1_cfg_a = reg_pmp_1_cfg_a; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_1_cfg_x = reg_pmp_1_cfg_x; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_1_cfg_w = reg_pmp_1_cfg_w; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_1_cfg_r = reg_pmp_1_cfg_r; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_1_addr = reg_pmp_1_addr; // @[PMP.scala 26:19 PMP.scala 28:14]
  assign io_pmp_1_mask = _pmp_mask_T_7[36:0]; // @[PMP.scala 26:19 PMP.scala 29:14]
  assign io_pmp_2_cfg_l = reg_pmp_2_cfg_l; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_2_cfg_a = reg_pmp_2_cfg_a; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_2_cfg_x = reg_pmp_2_cfg_x; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_2_cfg_w = reg_pmp_2_cfg_w; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_2_cfg_r = reg_pmp_2_cfg_r; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_2_addr = reg_pmp_2_addr; // @[PMP.scala 26:19 PMP.scala 28:14]
  assign io_pmp_2_mask = _pmp_mask_T_11[36:0]; // @[PMP.scala 26:19 PMP.scala 29:14]
  assign io_pmp_3_cfg_l = reg_pmp_3_cfg_l; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_3_cfg_a = reg_pmp_3_cfg_a; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_3_cfg_x = reg_pmp_3_cfg_x; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_3_cfg_w = reg_pmp_3_cfg_w; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_3_cfg_r = reg_pmp_3_cfg_r; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_3_addr = reg_pmp_3_addr; // @[PMP.scala 26:19 PMP.scala 28:14]
  assign io_pmp_3_mask = _pmp_mask_T_15[36:0]; // @[PMP.scala 26:19 PMP.scala 29:14]
  assign io_pmp_4_cfg_l = reg_pmp_4_cfg_l; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_4_cfg_a = reg_pmp_4_cfg_a; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_4_cfg_x = reg_pmp_4_cfg_x; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_4_cfg_w = reg_pmp_4_cfg_w; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_4_cfg_r = reg_pmp_4_cfg_r; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_4_addr = reg_pmp_4_addr; // @[PMP.scala 26:19 PMP.scala 28:14]
  assign io_pmp_4_mask = _pmp_mask_T_19[36:0]; // @[PMP.scala 26:19 PMP.scala 29:14]
  assign io_pmp_5_cfg_l = reg_pmp_5_cfg_l; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_5_cfg_a = reg_pmp_5_cfg_a; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_5_cfg_x = reg_pmp_5_cfg_x; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_5_cfg_w = reg_pmp_5_cfg_w; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_5_cfg_r = reg_pmp_5_cfg_r; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_5_addr = reg_pmp_5_addr; // @[PMP.scala 26:19 PMP.scala 28:14]
  assign io_pmp_5_mask = _pmp_mask_T_23[36:0]; // @[PMP.scala 26:19 PMP.scala 29:14]
  assign io_pmp_6_cfg_l = reg_pmp_6_cfg_l; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_6_cfg_a = reg_pmp_6_cfg_a; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_6_cfg_x = reg_pmp_6_cfg_x; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_6_cfg_w = reg_pmp_6_cfg_w; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_6_cfg_r = reg_pmp_6_cfg_r; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_6_addr = reg_pmp_6_addr; // @[PMP.scala 26:19 PMP.scala 28:14]
  assign io_pmp_6_mask = _pmp_mask_T_27[36:0]; // @[PMP.scala 26:19 PMP.scala 29:14]
  assign io_pmp_7_cfg_l = reg_pmp_7_cfg_l; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_7_cfg_a = reg_pmp_7_cfg_a; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_7_cfg_x = reg_pmp_7_cfg_x; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_7_cfg_w = reg_pmp_7_cfg_w; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_7_cfg_r = reg_pmp_7_cfg_r; // @[PMP.scala 26:19 PMP.scala 27:13]
  assign io_pmp_7_addr = reg_pmp_7_addr; // @[PMP.scala 26:19 PMP.scala 28:14]
  assign io_pmp_7_mask = _pmp_mask_T_31[36:0]; // @[PMP.scala 26:19 PMP.scala 29:14]
  assign io_counters_0_eventSel = reg_hpmevent_0; // @[CSR.scala 440:72]
  assign io_counters_1_eventSel = reg_hpmevent_1; // @[CSR.scala 440:72]
  assign io_counters_2_eventSel = reg_hpmevent_2; // @[CSR.scala 440:72]
  assign io_counters_3_eventSel = reg_hpmevent_3; // @[CSR.scala 440:72]
  assign io_inhibit_cycle = reg_mcountinhibit[0]; // @[CSR.scala 435:40]
  assign io_trace_0_valid = io_retire > 1'h0 | io_trace_0_exception; // @[CSR.scala 1161:30]
  assign io_trace_0_iaddr = io_pc; // @[CSR.scala 1163:13]
  assign io_trace_0_insn = io_inst_0; // @[CSR.scala 1162:12]
  assign io_trace_0_priv = {reg_debug,reg_mstatus_prv}; // @[Cat.scala 30:58]
  assign io_trace_0_exception = exception; // @[CSR.scala 1160:35]
  assign io_trace_0_interrupt = cause[63]; // @[CSR.scala 1166:25]
  assign io_trace_0_cause = insn_call ? {{60'd0}, _cause_T_1} : _cause_T_2; // @[CSR.scala 688:8]
  assign io_customCSRs_0_wen = csr_wen & _T_214; // @[CSR.scala 893:18 CSR.scala 846:12]
  assign io_customCSRs_0_value = reg_custom_0; // @[CSR.scala 848:14]
  assign io_customCSRs_1_value = reg_custom_1; // @[CSR.scala 848:14]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_isa <= 32'h0;
    end else if (reset) begin
      reg_mstatus_isa <= 32'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_dprv <= 2'h0;
    end else if (reset) begin
      reg_mstatus_dprv <= 2'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_prv <= 2'h0;
    end else if (reset) begin
      reg_mstatus_prv <= 2'h3;
    end else if (new_prv == 2'h2) begin
      reg_mstatus_prv <= 2'h0;
    end else if (insn_ret) begin
      if (~io_rw_addr[9]) begin
        reg_mstatus_prv <= {{1'd0}, reg_mstatus_spp};
      end else begin
        reg_mstatus_prv <= _GEN_460;
      end
    end else if (exception) begin
      reg_mstatus_prv <= _GEN_414;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_sd <= 1'h0;
    end else if (reset) begin
      reg_mstatus_sd <= 1'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_zero2 <= 27'h0;
    end else if (reset) begin
      reg_mstatus_zero2 <= 27'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_sxl <= 2'h0;
    end else if (reset) begin
      reg_mstatus_sxl <= 2'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_uxl <= 2'h0;
    end else if (reset) begin
      reg_mstatus_uxl <= 2'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_sd_rv32 <= 1'h0;
    end else if (reset) begin
      reg_mstatus_sd_rv32 <= 1'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_zero1 <= 8'h0;
    end else if (reset) begin
      reg_mstatus_zero1 <= 8'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_tsr <= 1'h0;
    end else if (reset) begin
      reg_mstatus_tsr <= 1'h0;
    end else if (csr_wen) begin
      if (_T_72) begin
        reg_mstatus_tsr <= new_mstatus_tsr;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_tw <= 1'h0;
    end else if (reset) begin
      reg_mstatus_tw <= 1'h0;
    end else if (csr_wen) begin
      if (_T_72) begin
        reg_mstatus_tw <= new_mstatus_tw;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_tvm <= 1'h0;
    end else if (reset) begin
      reg_mstatus_tvm <= 1'h0;
    end else if (csr_wen) begin
      if (_T_72) begin
        reg_mstatus_tvm <= new_mstatus_tvm;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_mxr <= 1'h0;
    end else if (reset) begin
      reg_mstatus_mxr <= 1'h0;
    end else if (csr_wen) begin
      if (_T_184) begin
        reg_mstatus_mxr <= new_mstatus_mxr;
      end else if (_T_72) begin
        reg_mstatus_mxr <= new_mstatus_mxr;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_sum <= 1'h0;
    end else if (reset) begin
      reg_mstatus_sum <= 1'h0;
    end else if (csr_wen) begin
      if (_T_184) begin
        reg_mstatus_sum <= new_mstatus_sum;
      end else if (_T_72) begin
        reg_mstatus_sum <= new_mstatus_sum;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_mprv <= 1'h0;
    end else if (reset) begin
      reg_mstatus_mprv <= 1'h0;
    end else if (csr_wen) begin
      if (_T_72) begin
        reg_mstatus_mprv <= new_mstatus_mprv;
      end else begin
        reg_mstatus_mprv <= _GEN_488;
      end
    end else begin
      reg_mstatus_mprv <= _GEN_488;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_xs <= 2'h0;
    end else if (reset) begin
      reg_mstatus_xs <= 2'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_fs <= 2'h0;
    end else if (reset) begin
      reg_mstatus_fs <= 2'h0;
    end else if (csr_wen) begin
      if (_T_184) begin
        reg_mstatus_fs <= _reg_mstatus_fs_T_2;
      end else if (_T_72) begin
        reg_mstatus_fs <= _reg_mstatus_fs_T_2;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_mpp <= 2'h0;
    end else if (reset) begin
      reg_mstatus_mpp <= 2'h3;
    end else if (csr_wen) begin
      if (_T_72) begin
        if (new_mstatus_mpp == 2'h2) begin
          reg_mstatus_mpp <= 2'h0;
        end else begin
          reg_mstatus_mpp <= new_mstatus_mpp;
        end
      end else begin
        reg_mstatus_mpp <= _GEN_486;
      end
    end else begin
      reg_mstatus_mpp <= _GEN_486;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_vs <= 2'h0;
    end else if (reset) begin
      reg_mstatus_vs <= 2'h0;
    end else if (csr_wen) begin
      if (_T_184) begin
        reg_mstatus_vs <= 2'h0;
      end else if (_T_72) begin
        reg_mstatus_vs <= 2'h0;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_spp <= 1'h0;
    end else if (reset) begin
      reg_mstatus_spp <= 1'h0;
    end else begin
      reg_mstatus_spp <= _GEN_1174[0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_mpie <= 1'h0;
    end else if (reset) begin
      reg_mstatus_mpie <= 1'h0;
    end else if (csr_wen) begin
      if (_T_72) begin
        reg_mstatus_mpie <= new_mstatus_mpie;
      end else begin
        reg_mstatus_mpie <= _GEN_485;
      end
    end else begin
      reg_mstatus_mpie <= _GEN_485;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_hpie <= 1'h0;
    end else if (reset) begin
      reg_mstatus_hpie <= 1'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_spie <= 1'h0;
    end else if (reset) begin
      reg_mstatus_spie <= 1'h0;
    end else if (csr_wen) begin
      if (_T_184) begin
        reg_mstatus_spie <= new_mstatus_spie;
      end else if (_T_72) begin
        reg_mstatus_spie <= new_mstatus_spie;
      end else begin
        reg_mstatus_spie <= _GEN_479;
      end
    end else begin
      reg_mstatus_spie <= _GEN_479;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_upie <= 1'h0;
    end else if (reset) begin
      reg_mstatus_upie <= 1'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_mie <= 1'h0;
    end else if (reset) begin
      reg_mstatus_mie <= 1'h0;
    end else if (csr_wen) begin
      if (_T_72) begin
        reg_mstatus_mie <= new_mstatus_mie;
      end else begin
        reg_mstatus_mie <= _GEN_484;
      end
    end else begin
      reg_mstatus_mie <= _GEN_484;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_hie <= 1'h0;
    end else if (reset) begin
      reg_mstatus_hie <= 1'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_sie <= 1'h0;
    end else if (reset) begin
      reg_mstatus_sie <= 1'h0;
    end else if (csr_wen) begin
      if (_T_184) begin
        reg_mstatus_sie <= new_mstatus_sie;
      end else if (_T_72) begin
        reg_mstatus_sie <= new_mstatus_sie;
      end else begin
        reg_mstatus_sie <= _GEN_478;
      end
    end else begin
      reg_mstatus_sie <= _GEN_478;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mstatus_uie <= 1'h0;
    end else if (reset) begin
      reg_mstatus_uie <= 1'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dcsr_prv <= 2'h0;
    end else if (reset) begin
      reg_dcsr_prv <= 2'h3;
    end else if (csr_wen) begin
      if (_T_81) begin
        if (new_dcsr_prv == 2'h2) begin
          reg_dcsr_prv <= 2'h0;
        end else begin
          reg_dcsr_prv <= new_dcsr_prv;
        end
      end else begin
        reg_dcsr_prv <= _GEN_435;
      end
    end else begin
      reg_dcsr_prv <= _GEN_435;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mnstatus_mpp <= 2'h0;
    end else if (reset) begin
      reg_mnstatus_mpp <= 2'h3;
    end else if (csr_wen) begin
      if (_T_87) begin
        if (new_mstatus_mpp == 2'h2) begin
          reg_mnstatus_mpp <= 2'h0;
        end else begin
          reg_mnstatus_mpp <= new_mstatus_mpp;
        end
      end else begin
        reg_mnstatus_mpp <= _GEN_440;
      end
    end else begin
      reg_mnstatus_mpp <= _GEN_440;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_singleStepped <= 1'h0;
    end else if (_io_interrupt_T) begin
      reg_singleStepped <= 1'h0;
    end else begin
      reg_singleStepped <= _GEN_366;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dcsr_ebreakm <= 1'h0;
    end else if (reset) begin
      reg_dcsr_ebreakm <= 1'h0;
    end else if (csr_wen) begin
      if (_T_81) begin
        reg_dcsr_ebreakm <= new_dcsr_ebreakm;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dcsr_ebreakh <= 1'h0;
    end else if (reset) begin
      reg_dcsr_ebreakh <= 1'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dcsr_ebreaks <= 1'h0;
    end else if (reset) begin
      reg_dcsr_ebreaks <= 1'h0;
    end else if (csr_wen) begin
      if (_T_81) begin
        reg_dcsr_ebreaks <= new_dcsr_ebreaks;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dcsr_ebreaku <= 1'h0;
    end else if (reset) begin
      reg_dcsr_ebreaku <= 1'h0;
    end else if (csr_wen) begin
      if (_T_81) begin
        reg_dcsr_ebreaku <= new_dcsr_ebreaku;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_debug <= 1'h0;
    end else if (reset) begin
      reg_debug <= 1'h0;
    end else if (insn_ret) begin
      if (~io_rw_addr[9]) begin
        reg_debug <= _GEN_432;
      end else if (io_rw_addr[10] & io_rw_addr[7]) begin
        reg_debug <= 1'h0;
      end else begin
        reg_debug <= _GEN_432;
      end
    end else begin
      reg_debug <= _GEN_432;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      nmie <= 1'h0;
    end else begin
      nmie <= reset | _GEN_1198;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mideleg <= 64'h0;
    end else if (csr_wen) begin
      if (_T_194) begin
        reg_mideleg <= wdata;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_medeleg <= 64'h0;
    end else if (csr_wen) begin
      if (_T_195) begin
        reg_medeleg <= wdata;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dcsr_xdebugver <= 2'h0;
    end else if (reset) begin
      reg_dcsr_xdebugver <= 2'h1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dcsr_zero4 <= 2'h0;
    end else if (reset) begin
      reg_dcsr_zero4 <= 2'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dcsr_zero3 <= 12'h0;
    end else if (reset) begin
      reg_dcsr_zero3 <= 12'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dcsr_zero2 <= 1'h0;
    end else if (reset) begin
      reg_dcsr_zero2 <= 1'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dcsr_stopcycle <= 1'h0;
    end else if (reset) begin
      reg_dcsr_stopcycle <= 1'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dcsr_stoptime <= 1'h0;
    end else if (reset) begin
      reg_dcsr_stoptime <= 1'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dcsr_cause <= 3'h0;
    end else if (reset) begin
      reg_dcsr_cause <= 3'h0;
    end else if (exception) begin
      if (trapToDebug) begin
        if (~reg_debug) begin
          reg_dcsr_cause <= _reg_dcsr_cause_T_2;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dcsr_zero1 <= 3'h0;
    end else if (reset) begin
      reg_dcsr_zero1 <= 3'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dcsr_step <= 1'h0;
    end else if (reset) begin
      reg_dcsr_step <= 1'h0;
    end else if (csr_wen) begin
      if (_T_81) begin
        reg_dcsr_step <= new_dcsr_step;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dpc <= 40'h0;
    end else begin
      reg_dpc <= _GEN_1224[39:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_dscratch <= 64'h0;
    end else if (csr_wen) begin
      if (_T_83) begin
        reg_dscratch <= wdata;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_tselect <= 4'h0;
    end else begin
      reg_tselect <= _GEN_1237[3:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_0_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_0_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'h0 == reg_tselect & (~reg_bp_0_control_dmode | reg_debug)) begin
        if (_T_68) begin
          reg_bp_0_control_dmode <= dMode;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_0_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_0_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'h0 == reg_tselect & (~reg_bp_0_control_dmode | reg_debug)) begin
        if (_T_68) begin
          reg_bp_0_control_action <= _GEN_577;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_0_control_chain <= 1'h0;
    end else if (reset) begin
      reg_bp_0_control_chain <= 1'h0;
    end else if (csr_wen) begin
      if (4'h0 == reg_tselect & (~reg_bp_0_control_dmode | reg_debug)) begin
        if (_T_68) begin
          reg_bp_0_control_chain <= newBPC_chain & ~reg_bp_1_control_chain & (dMode | ~reg_bp_1_control_dmode);
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_0_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'h0 == reg_tselect & (~reg_bp_0_control_dmode | reg_debug)) begin
        if (_T_68) begin
          reg_bp_0_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_0_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'h0 == reg_tselect & (~reg_bp_0_control_dmode | reg_debug)) begin
        if (_T_68) begin
          reg_bp_0_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_0_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'h0 == reg_tselect & (~reg_bp_0_control_dmode | reg_debug)) begin
        if (_T_68) begin
          reg_bp_0_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_0_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'h0 == reg_tselect & (~reg_bp_0_control_dmode | reg_debug)) begin
        if (_T_68) begin
          reg_bp_0_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_0_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_0_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'h0 == reg_tselect & (~reg_bp_0_control_dmode | reg_debug)) begin
        if (_T_68) begin
          reg_bp_0_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_0_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_0_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'h0 == reg_tselect & (~reg_bp_0_control_dmode | reg_debug)) begin
        if (_T_68) begin
          reg_bp_0_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_0_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_0_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'h0 == reg_tselect & (~reg_bp_0_control_dmode | reg_debug)) begin
        if (_T_68) begin
          reg_bp_0_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_0_address <= 39'h0;
    end else begin
      reg_bp_0_address <= _GEN_1238[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_1_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_1_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'h1 == reg_tselect & (_reg_bp_0_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_1_control_dmode <= dMode_1;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_1_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_1_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'h1 == reg_tselect & (_reg_bp_0_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_1_control_action <= _GEN_610;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_1_control_chain <= 1'h0;
    end else if (reset) begin
      reg_bp_1_control_chain <= 1'h0;
    end else if (csr_wen) begin
      if (4'h1 == reg_tselect & (_reg_bp_0_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_1_control_chain <= newBPC_1_chain & ~(reg_bp_0_control_chain | reg_bp_2_control_chain) & (dMode_1 | ~
            reg_bp_2_control_dmode);
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_1_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'h1 == reg_tselect & (_reg_bp_0_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_1_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_1_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'h1 == reg_tselect & (_reg_bp_0_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_1_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_1_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'h1 == reg_tselect & (_reg_bp_0_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_1_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_1_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'h1 == reg_tselect & (_reg_bp_0_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_1_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_1_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_1_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'h1 == reg_tselect & (_reg_bp_0_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_1_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_1_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_1_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'h1 == reg_tselect & (_reg_bp_0_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_1_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_1_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_1_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'h1 == reg_tselect & (_reg_bp_0_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_1_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_1_address <= 39'h0;
    end else begin
      reg_bp_1_address <= _GEN_1254[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_2_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_2_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'h2 == reg_tselect & (_reg_bp_1_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_2_control_dmode <= dMode_2;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_2_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_2_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'h2 == reg_tselect & (_reg_bp_1_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_2_control_action <= _GEN_643;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_2_control_chain <= 1'h0;
    end else if (reset) begin
      reg_bp_2_control_chain <= 1'h0;
    end else if (csr_wen) begin
      if (4'h2 == reg_tselect & (_reg_bp_1_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_2_control_chain <= newBPC_2_chain & ~(reg_bp_1_control_chain | reg_bp_3_control_chain) & (dMode_2 | ~
            reg_bp_3_control_dmode);
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_2_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'h2 == reg_tselect & (_reg_bp_1_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_2_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_2_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'h2 == reg_tselect & (_reg_bp_1_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_2_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_2_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'h2 == reg_tselect & (_reg_bp_1_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_2_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_2_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'h2 == reg_tselect & (_reg_bp_1_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_2_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_2_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_2_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'h2 == reg_tselect & (_reg_bp_1_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_2_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_2_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_2_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'h2 == reg_tselect & (_reg_bp_1_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_2_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_2_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_2_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'h2 == reg_tselect & (_reg_bp_1_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_2_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_2_address <= 39'h0;
    end else begin
      reg_bp_2_address <= _GEN_1270[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_3_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_3_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'h3 == reg_tselect & (_reg_bp_2_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_3_control_dmode <= dMode_3;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_3_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_3_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'h3 == reg_tselect & (_reg_bp_2_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_3_control_action <= _GEN_676;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_3_control_chain <= 1'h0;
    end else if (reset) begin
      reg_bp_3_control_chain <= 1'h0;
    end else if (csr_wen) begin
      if (4'h3 == reg_tselect & (_reg_bp_2_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_3_control_chain <= newBPC_3_chain & ~(reg_bp_2_control_chain | reg_bp_4_control_chain) & (dMode_3 | ~
            reg_bp_4_control_dmode);
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_3_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'h3 == reg_tselect & (_reg_bp_2_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_3_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_3_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'h3 == reg_tselect & (_reg_bp_2_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_3_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_3_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'h3 == reg_tselect & (_reg_bp_2_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_3_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_3_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'h3 == reg_tselect & (_reg_bp_2_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_3_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_3_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_3_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'h3 == reg_tselect & (_reg_bp_2_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_3_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_3_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_3_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'h3 == reg_tselect & (_reg_bp_2_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_3_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_3_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_3_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'h3 == reg_tselect & (_reg_bp_2_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_3_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_3_address <= 39'h0;
    end else begin
      reg_bp_3_address <= _GEN_1286[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_4_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_4_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'h4 == reg_tselect & (_reg_bp_3_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_4_control_dmode <= dMode_4;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_4_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_4_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'h4 == reg_tselect & (_reg_bp_3_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_4_control_action <= _GEN_709;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_4_control_chain <= 1'h0;
    end else if (reset) begin
      reg_bp_4_control_chain <= 1'h0;
    end else if (csr_wen) begin
      if (4'h4 == reg_tselect & (_reg_bp_3_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_4_control_chain <= newBPC_4_chain & ~(reg_bp_3_control_chain | reg_bp_5_control_chain) & (dMode_4 | ~
            reg_bp_5_control_dmode);
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_4_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'h4 == reg_tselect & (_reg_bp_3_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_4_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_4_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'h4 == reg_tselect & (_reg_bp_3_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_4_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_4_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'h4 == reg_tselect & (_reg_bp_3_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_4_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_4_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'h4 == reg_tselect & (_reg_bp_3_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_4_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_4_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_4_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'h4 == reg_tselect & (_reg_bp_3_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_4_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_4_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_4_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'h4 == reg_tselect & (_reg_bp_3_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_4_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_4_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_4_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'h4 == reg_tselect & (_reg_bp_3_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_4_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_4_address <= 39'h0;
    end else begin
      reg_bp_4_address <= _GEN_1302[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_5_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_5_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'h5 == reg_tselect & (_reg_bp_4_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_5_control_dmode <= dMode_5;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_5_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_5_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'h5 == reg_tselect & (_reg_bp_4_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_5_control_action <= _GEN_742;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_5_control_chain <= 1'h0;
    end else if (reset) begin
      reg_bp_5_control_chain <= 1'h0;
    end else if (csr_wen) begin
      if (4'h5 == reg_tselect & (_reg_bp_4_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_5_control_chain <= newBPC_5_chain & ~(reg_bp_4_control_chain | reg_bp_6_control_chain) & (dMode_5 | ~
            reg_bp_6_control_dmode);
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_5_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'h5 == reg_tselect & (_reg_bp_4_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_5_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_5_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'h5 == reg_tselect & (_reg_bp_4_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_5_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_5_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'h5 == reg_tselect & (_reg_bp_4_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_5_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_5_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'h5 == reg_tselect & (_reg_bp_4_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_5_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_5_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_5_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'h5 == reg_tselect & (_reg_bp_4_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_5_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_5_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_5_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'h5 == reg_tselect & (_reg_bp_4_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_5_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_5_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_5_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'h5 == reg_tselect & (_reg_bp_4_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_5_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_5_address <= 39'h0;
    end else begin
      reg_bp_5_address <= _GEN_1318[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_6_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_6_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'h6 == reg_tselect & (_reg_bp_5_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_6_control_dmode <= dMode_6;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_6_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_6_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'h6 == reg_tselect & (_reg_bp_5_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_6_control_action <= _GEN_775;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_6_control_chain <= 1'h0;
    end else if (reset) begin
      reg_bp_6_control_chain <= 1'h0;
    end else if (csr_wen) begin
      if (4'h6 == reg_tselect & (_reg_bp_5_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_6_control_chain <= newBPC_6_chain & ~(reg_bp_5_control_chain | reg_bp_7_control_chain) & (dMode_6 | ~
            reg_bp_7_control_dmode);
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_6_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'h6 == reg_tselect & (_reg_bp_5_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_6_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_6_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'h6 == reg_tselect & (_reg_bp_5_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_6_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_6_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'h6 == reg_tselect & (_reg_bp_5_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_6_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_6_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'h6 == reg_tselect & (_reg_bp_5_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_6_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_6_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_6_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'h6 == reg_tselect & (_reg_bp_5_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_6_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_6_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_6_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'h6 == reg_tselect & (_reg_bp_5_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_6_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_6_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_6_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'h6 == reg_tselect & (_reg_bp_5_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_6_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_6_address <= 39'h0;
    end else begin
      reg_bp_6_address <= _GEN_1334[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_7_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_7_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'h7 == reg_tselect & (_reg_bp_6_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_7_control_dmode <= dMode_7;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_7_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_7_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'h7 == reg_tselect & (_reg_bp_6_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_7_control_action <= _GEN_808;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_7_control_chain <= 1'h0;
    end else if (reset) begin
      reg_bp_7_control_chain <= 1'h0;
    end else if (csr_wen) begin
      if (4'h7 == reg_tselect & (_reg_bp_6_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_7_control_chain <= newBPC_7_chain & ~(reg_bp_6_control_chain | reg_bp_8_control_chain) & (dMode_7 | ~
            reg_bp_8_control_dmode);
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_7_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'h7 == reg_tselect & (_reg_bp_6_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_7_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_7_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'h7 == reg_tselect & (_reg_bp_6_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_7_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_7_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'h7 == reg_tselect & (_reg_bp_6_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_7_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_7_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'h7 == reg_tselect & (_reg_bp_6_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_7_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_7_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_7_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'h7 == reg_tselect & (_reg_bp_6_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_7_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_7_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_7_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'h7 == reg_tselect & (_reg_bp_6_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_7_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_7_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_7_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'h7 == reg_tselect & (_reg_bp_6_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_7_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_7_address <= 39'h0;
    end else begin
      reg_bp_7_address <= _GEN_1350[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_8_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_8_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'h8 == reg_tselect & (_reg_bp_7_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_8_control_dmode <= dMode_8;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_8_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_8_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'h8 == reg_tselect & (_reg_bp_7_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_8_control_action <= _GEN_841;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_8_control_chain <= 1'h0;
    end else if (reset) begin
      reg_bp_8_control_chain <= 1'h0;
    end else if (csr_wen) begin
      if (4'h8 == reg_tselect & (_reg_bp_7_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_8_control_chain <= newBPC_8_chain & ~(reg_bp_7_control_chain | reg_bp_9_control_chain) & (dMode_8 | ~
            reg_bp_9_control_dmode);
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_8_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'h8 == reg_tselect & (_reg_bp_7_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_8_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_8_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'h8 == reg_tselect & (_reg_bp_7_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_8_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_8_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'h8 == reg_tselect & (_reg_bp_7_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_8_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_8_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'h8 == reg_tselect & (_reg_bp_7_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_8_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_8_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_8_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'h8 == reg_tselect & (_reg_bp_7_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_8_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_8_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_8_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'h8 == reg_tselect & (_reg_bp_7_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_8_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_8_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_8_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'h8 == reg_tselect & (_reg_bp_7_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_8_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_8_address <= 39'h0;
    end else begin
      reg_bp_8_address <= _GEN_1366[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_9_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_9_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'h9 == reg_tselect & (_reg_bp_8_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_9_control_dmode <= dMode_9;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_9_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_9_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'h9 == reg_tselect & (_reg_bp_8_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_9_control_action <= _GEN_874;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_9_control_chain <= 1'h0;
    end else if (reset) begin
      reg_bp_9_control_chain <= 1'h0;
    end else if (csr_wen) begin
      if (4'h9 == reg_tselect & (_reg_bp_8_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_9_control_chain <= newBPC_9_chain & ~(reg_bp_8_control_chain | reg_bp_10_control_chain) & (dMode_9 | ~
            reg_bp_10_control_dmode);
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_9_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'h9 == reg_tselect & (_reg_bp_8_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_9_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_9_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'h9 == reg_tselect & (_reg_bp_8_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_9_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_9_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'h9 == reg_tselect & (_reg_bp_8_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_9_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_9_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'h9 == reg_tselect & (_reg_bp_8_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_9_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_9_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_9_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'h9 == reg_tselect & (_reg_bp_8_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_9_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_9_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_9_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'h9 == reg_tselect & (_reg_bp_8_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_9_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_9_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_9_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'h9 == reg_tselect & (_reg_bp_8_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_9_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_9_address <= 39'h0;
    end else begin
      reg_bp_9_address <= _GEN_1382[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_10_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_10_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'ha == reg_tselect & (_reg_bp_9_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_10_control_dmode <= dMode_10;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_10_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_10_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'ha == reg_tselect & (_reg_bp_9_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_10_control_action <= _GEN_907;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_10_control_chain <= 1'h0;
    end else if (reset) begin
      reg_bp_10_control_chain <= 1'h0;
    end else if (csr_wen) begin
      if (4'ha == reg_tselect & (_reg_bp_9_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_10_control_chain <= newBPC_10_chain & ~(reg_bp_9_control_chain | reg_bp_11_control_chain) & (dMode_10
             | ~reg_bp_11_control_dmode);
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_10_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'ha == reg_tselect & (_reg_bp_9_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_10_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_10_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'ha == reg_tselect & (_reg_bp_9_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_10_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_10_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'ha == reg_tselect & (_reg_bp_9_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_10_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_10_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'ha == reg_tselect & (_reg_bp_9_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_10_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_10_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_10_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'ha == reg_tselect & (_reg_bp_9_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_10_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_10_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_10_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'ha == reg_tselect & (_reg_bp_9_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_10_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_10_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_10_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'ha == reg_tselect & (_reg_bp_9_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_10_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_10_address <= 39'h0;
    end else begin
      reg_bp_10_address <= _GEN_1398[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_11_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_11_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'hb == reg_tselect & (_reg_bp_10_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_11_control_dmode <= dMode_11;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_11_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_11_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'hb == reg_tselect & (_reg_bp_10_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_11_control_action <= _GEN_940;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_11_control_chain <= 1'h0;
    end else if (reset) begin
      reg_bp_11_control_chain <= 1'h0;
    end else if (csr_wen) begin
      if (4'hb == reg_tselect & (_reg_bp_10_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_11_control_chain <= newBPC_11_chain & ~(reg_bp_10_control_chain | reg_bp_12_control_chain) & (dMode_11
             | ~reg_bp_12_control_dmode);
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_11_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'hb == reg_tselect & (_reg_bp_10_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_11_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_11_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'hb == reg_tselect & (_reg_bp_10_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_11_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_11_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'hb == reg_tselect & (_reg_bp_10_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_11_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_11_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'hb == reg_tselect & (_reg_bp_10_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_11_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_11_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_11_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'hb == reg_tselect & (_reg_bp_10_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_11_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_11_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_11_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'hb == reg_tselect & (_reg_bp_10_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_11_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_11_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_11_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'hb == reg_tselect & (_reg_bp_10_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_11_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_11_address <= 39'h0;
    end else begin
      reg_bp_11_address <= _GEN_1414[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_12_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_12_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'hc == reg_tselect & (_reg_bp_11_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_12_control_dmode <= dMode_12;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_12_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_12_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'hc == reg_tselect & (_reg_bp_11_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_12_control_action <= _GEN_973;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_12_control_chain <= 1'h0;
    end else if (reset) begin
      reg_bp_12_control_chain <= 1'h0;
    end else if (csr_wen) begin
      if (4'hc == reg_tselect & (_reg_bp_11_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_12_control_chain <= newBPC_12_chain & ~(reg_bp_11_control_chain | reg_bp_13_control_chain) & (dMode_12
             | ~reg_bp_13_control_dmode);
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_12_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'hc == reg_tselect & (_reg_bp_11_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_12_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_12_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'hc == reg_tselect & (_reg_bp_11_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_12_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_12_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'hc == reg_tselect & (_reg_bp_11_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_12_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_12_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'hc == reg_tselect & (_reg_bp_11_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_12_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_12_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_12_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'hc == reg_tselect & (_reg_bp_11_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_12_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_12_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_12_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'hc == reg_tselect & (_reg_bp_11_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_12_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_12_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_12_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'hc == reg_tselect & (_reg_bp_11_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_12_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_12_address <= 39'h0;
    end else begin
      reg_bp_12_address <= _GEN_1430[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_13_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_13_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'hd == reg_tselect & (_reg_bp_12_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_13_control_dmode <= dMode_13;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_13_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_13_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'hd == reg_tselect & (_reg_bp_12_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_13_control_action <= _GEN_1006;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_13_control_chain <= 1'h0;
    end else if (reset) begin
      reg_bp_13_control_chain <= 1'h0;
    end else if (csr_wen) begin
      if (4'hd == reg_tselect & (_reg_bp_12_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_13_control_chain <= newBPC_13_chain & ~(reg_bp_12_control_chain | reg_bp_14_control_chain) & (dMode_13
             | ~reg_bp_14_control_dmode);
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_13_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'hd == reg_tselect & (_reg_bp_12_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_13_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_13_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'hd == reg_tselect & (_reg_bp_12_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_13_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_13_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'hd == reg_tselect & (_reg_bp_12_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_13_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_13_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'hd == reg_tselect & (_reg_bp_12_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_13_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_13_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_13_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'hd == reg_tselect & (_reg_bp_12_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_13_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_13_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_13_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'hd == reg_tselect & (_reg_bp_12_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_13_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_13_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_13_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'hd == reg_tselect & (_reg_bp_12_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_13_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_13_address <= 39'h0;
    end else begin
      reg_bp_13_address <= _GEN_1446[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_14_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_14_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'he == reg_tselect & (_reg_bp_13_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_14_control_dmode <= dMode_14;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_14_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_14_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'he == reg_tselect & (_reg_bp_13_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_14_control_action <= _GEN_1039;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_14_control_chain <= 1'h0;
    end else if (reset) begin
      reg_bp_14_control_chain <= 1'h0;
    end else if (csr_wen) begin
      if (4'he == reg_tselect & (_reg_bp_13_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_14_control_chain <= newBPC_14_chain & _dMode_T_43 & (dMode_14 | ~reg_bp_15_control_dmode);
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_14_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'he == reg_tselect & (_reg_bp_13_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_14_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_14_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'he == reg_tselect & (_reg_bp_13_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_14_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_14_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'he == reg_tselect & (_reg_bp_13_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_14_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_14_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'he == reg_tselect & (_reg_bp_13_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_14_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_14_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_14_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'he == reg_tselect & (_reg_bp_13_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_14_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_14_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_14_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'he == reg_tselect & (_reg_bp_13_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_14_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_14_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_14_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'he == reg_tselect & (_reg_bp_13_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_14_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_14_address <= 39'h0;
    end else begin
      reg_bp_14_address <= _GEN_1462[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_15_control_dmode <= 1'h0;
    end else if (reset) begin
      reg_bp_15_control_dmode <= 1'h0;
    end else if (csr_wen) begin
      if (4'hf == reg_tselect & (_reg_bp_14_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_15_control_dmode <= dMode_15;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_15_control_action <= 3'h0;
    end else if (reset) begin
      reg_bp_15_control_action <= 3'h0;
    end else if (csr_wen) begin
      if (4'hf == reg_tselect & (_reg_bp_14_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_15_control_action <= _GEN_1072;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_15_control_tmatch <= 2'h0;
    end else if (csr_wen) begin
      if (4'hf == reg_tselect & (_reg_bp_14_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_15_control_tmatch <= wdata[8:7];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_15_control_m <= 1'h0;
    end else if (csr_wen) begin
      if (4'hf == reg_tselect & (_reg_bp_14_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_15_control_m <= wdata[6];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_15_control_s <= 1'h0;
    end else if (csr_wen) begin
      if (4'hf == reg_tselect & (_reg_bp_14_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_15_control_s <= wdata[4];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_15_control_u <= 1'h0;
    end else if (csr_wen) begin
      if (4'hf == reg_tselect & (_reg_bp_14_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_15_control_u <= wdata[3];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_15_control_x <= 1'h0;
    end else if (reset) begin
      reg_bp_15_control_x <= 1'h0;
    end else if (csr_wen) begin
      if (4'hf == reg_tselect & (_reg_bp_14_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_15_control_x <= wdata[2];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_15_control_w <= 1'h0;
    end else if (reset) begin
      reg_bp_15_control_w <= 1'h0;
    end else if (csr_wen) begin
      if (4'hf == reg_tselect & (_reg_bp_14_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_15_control_w <= wdata[1];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_15_control_r <= 1'h0;
    end else if (reset) begin
      reg_bp_15_control_r <= 1'h0;
    end else if (csr_wen) begin
      if (4'hf == reg_tselect & (_reg_bp_14_control_chain_T_3 | reg_debug)) begin
        if (_T_68) begin
          reg_bp_15_control_r <= wdata[0];
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_bp_15_address <= 39'h0;
    end else begin
      reg_bp_15_address <= _GEN_1478[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_0_cfg_l <= 1'h0;
    end else if (reset) begin
      reg_pmp_0_cfg_l <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_0_cfg_l) begin
        reg_pmp_0_cfg_l <= newCfg_l;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_0_cfg_a <= 2'h0;
    end else if (reset) begin
      reg_pmp_0_cfg_a <= 2'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_0_cfg_l) begin
        reg_pmp_0_cfg_a <= newCfg_a;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_0_cfg_x <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_0_cfg_l) begin
        reg_pmp_0_cfg_x <= newCfg_x;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_0_cfg_w <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_0_cfg_l) begin
        reg_pmp_0_cfg_w <= newCfg_w & newCfg_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_0_cfg_r <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_0_cfg_l) begin
        reg_pmp_0_cfg_r <= newCfg_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_0_addr <= 35'h0;
    end else begin
      reg_pmp_0_addr <= _GEN_1500[34:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_1_cfg_l <= 1'h0;
    end else if (reset) begin
      reg_pmp_1_cfg_l <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_1_cfg_l) begin
        reg_pmp_1_cfg_l <= newCfg_1_l;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_1_cfg_a <= 2'h0;
    end else if (reset) begin
      reg_pmp_1_cfg_a <= 2'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_1_cfg_l) begin
        reg_pmp_1_cfg_a <= newCfg_1_a;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_1_cfg_x <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_1_cfg_l) begin
        reg_pmp_1_cfg_x <= newCfg_1_x;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_1_cfg_w <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_1_cfg_l) begin
        reg_pmp_1_cfg_w <= newCfg_1_w & newCfg_1_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_1_cfg_r <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_1_cfg_l) begin
        reg_pmp_1_cfg_r <= newCfg_1_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_1_addr <= 35'h0;
    end else begin
      reg_pmp_1_addr <= _GEN_1507[34:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_2_cfg_l <= 1'h0;
    end else if (reset) begin
      reg_pmp_2_cfg_l <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_2_cfg_l) begin
        reg_pmp_2_cfg_l <= newCfg_2_l;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_2_cfg_a <= 2'h0;
    end else if (reset) begin
      reg_pmp_2_cfg_a <= 2'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_2_cfg_l) begin
        reg_pmp_2_cfg_a <= newCfg_2_a;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_2_cfg_x <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_2_cfg_l) begin
        reg_pmp_2_cfg_x <= newCfg_2_x;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_2_cfg_w <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_2_cfg_l) begin
        reg_pmp_2_cfg_w <= newCfg_2_w & newCfg_2_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_2_cfg_r <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_2_cfg_l) begin
        reg_pmp_2_cfg_r <= newCfg_2_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_2_addr <= 35'h0;
    end else begin
      reg_pmp_2_addr <= _GEN_1514[34:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_3_cfg_l <= 1'h0;
    end else if (reset) begin
      reg_pmp_3_cfg_l <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_3_cfg_l) begin
        reg_pmp_3_cfg_l <= newCfg_3_l;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_3_cfg_a <= 2'h0;
    end else if (reset) begin
      reg_pmp_3_cfg_a <= 2'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_3_cfg_l) begin
        reg_pmp_3_cfg_a <= newCfg_3_a;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_3_cfg_x <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_3_cfg_l) begin
        reg_pmp_3_cfg_x <= newCfg_3_x;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_3_cfg_w <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_3_cfg_l) begin
        reg_pmp_3_cfg_w <= newCfg_3_w & newCfg_3_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_3_cfg_r <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_3_cfg_l) begin
        reg_pmp_3_cfg_r <= newCfg_3_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_3_addr <= 35'h0;
    end else begin
      reg_pmp_3_addr <= _GEN_1521[34:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_4_cfg_l <= 1'h0;
    end else if (reset) begin
      reg_pmp_4_cfg_l <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_4_cfg_l) begin
        reg_pmp_4_cfg_l <= newCfg_4_l;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_4_cfg_a <= 2'h0;
    end else if (reset) begin
      reg_pmp_4_cfg_a <= 2'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_4_cfg_l) begin
        reg_pmp_4_cfg_a <= newCfg_4_a;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_4_cfg_x <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_4_cfg_l) begin
        reg_pmp_4_cfg_x <= newCfg_4_x;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_4_cfg_w <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_4_cfg_l) begin
        reg_pmp_4_cfg_w <= newCfg_4_w & newCfg_4_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_4_cfg_r <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_4_cfg_l) begin
        reg_pmp_4_cfg_r <= newCfg_4_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_4_addr <= 35'h0;
    end else begin
      reg_pmp_4_addr <= _GEN_1528[34:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_5_cfg_l <= 1'h0;
    end else if (reset) begin
      reg_pmp_5_cfg_l <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_5_cfg_l) begin
        reg_pmp_5_cfg_l <= newCfg_5_l;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_5_cfg_a <= 2'h0;
    end else if (reset) begin
      reg_pmp_5_cfg_a <= 2'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_5_cfg_l) begin
        reg_pmp_5_cfg_a <= newCfg_5_a;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_5_cfg_x <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_5_cfg_l) begin
        reg_pmp_5_cfg_x <= newCfg_5_x;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_5_cfg_w <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_5_cfg_l) begin
        reg_pmp_5_cfg_w <= newCfg_5_w & newCfg_5_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_5_cfg_r <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_5_cfg_l) begin
        reg_pmp_5_cfg_r <= newCfg_5_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_5_addr <= 35'h0;
    end else begin
      reg_pmp_5_addr <= _GEN_1535[34:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_6_cfg_l <= 1'h0;
    end else if (reset) begin
      reg_pmp_6_cfg_l <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_6_cfg_l) begin
        reg_pmp_6_cfg_l <= newCfg_6_l;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_6_cfg_a <= 2'h0;
    end else if (reset) begin
      reg_pmp_6_cfg_a <= 2'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_6_cfg_l) begin
        reg_pmp_6_cfg_a <= newCfg_6_a;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_6_cfg_x <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_6_cfg_l) begin
        reg_pmp_6_cfg_x <= newCfg_6_x;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_6_cfg_w <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_6_cfg_l) begin
        reg_pmp_6_cfg_w <= newCfg_6_w & newCfg_6_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_6_cfg_r <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_6_cfg_l) begin
        reg_pmp_6_cfg_r <= newCfg_6_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_6_addr <= 35'h0;
    end else begin
      reg_pmp_6_addr <= _GEN_1542[34:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_7_cfg_l <= 1'h0;
    end else if (reset) begin
      reg_pmp_7_cfg_l <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_7_cfg_l) begin
        reg_pmp_7_cfg_l <= newCfg_7_l;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_7_cfg_a <= 2'h0;
    end else if (reset) begin
      reg_pmp_7_cfg_a <= 2'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_7_cfg_l) begin
        reg_pmp_7_cfg_a <= newCfg_7_a;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_7_cfg_x <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_7_cfg_l) begin
        reg_pmp_7_cfg_x <= newCfg_7_x;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_7_cfg_w <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_7_cfg_l) begin
        reg_pmp_7_cfg_w <= newCfg_7_w & newCfg_7_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_7_cfg_r <= 1'h0;
    end else if (csr_wen) begin
      if (_T_196 & ~reg_pmp_7_cfg_l) begin
        reg_pmp_7_cfg_r <= newCfg_7_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_pmp_7_addr <= 35'h0;
    end else begin
      reg_pmp_7_addr <= _GEN_1549[34:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mie <= 64'h0;
    end else if (csr_wen) begin
      if (_T_186) begin
        reg_mie <= _reg_mie_T_4;
      end else if (_T_75) begin
        reg_mie <= _reg_mie_T;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mip_seip <= 1'h0;
    end else if (csr_wen) begin
      if (_T_74) begin
        reg_mip_seip <= new_mip_seip;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mip_stip <= 1'h0;
    end else if (csr_wen) begin
      if (_T_74) begin
        reg_mip_stip <= new_mip_stip;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mip_ssip <= 1'h0;
    end else if (csr_wen) begin
      if (_T_185) begin
        reg_mip_ssip <= new_sip_ssip;
      end else if (_T_74) begin
        reg_mip_ssip <= new_mip_ssip;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mepc <= 40'h0;
    end else begin
      reg_mepc <= _GEN_1189[39:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mcause <= 64'h0;
    end else if (reset) begin
      reg_mcause <= 64'h0;
    end else if (csr_wen) begin
      if (_T_79) begin
        reg_mcause <= _reg_mcause_T;
      end else begin
        reg_mcause <= _GEN_449;
      end
    end else begin
      reg_mcause <= _GEN_449;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mtval <= 40'h0;
    end else if (csr_wen) begin
      if (_T_78) begin
        reg_mtval <= wdata[39:0];
      end else begin
        reg_mtval <= _GEN_450;
      end
    end else begin
      reg_mtval <= _GEN_450;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mscratch <= 64'h0;
    end else if (csr_wen) begin
      if (_T_76) begin
        reg_mscratch <= wdata;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mtvec <= 37'h0;
    end else if (reset) begin
      reg_mtvec <= 37'h0;
    end else begin
      reg_mtvec <= _GEN_1191[36:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mnscratch <= 64'h0;
    end else if (csr_wen) begin
      if (_T_84) begin
        reg_mnscratch <= wdata;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mnepc <= 40'h0;
    end else begin
      reg_mnepc <= _GEN_1195[39:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mncause <= 64'h0;
    end else if (reset) begin
      reg_mncause <= 64'h0;
    end else if (csr_wen) begin
      if (_T_86) begin
        reg_mncause <= _reg_mncause_T_2;
      end else begin
        reg_mncause <= _GEN_439;
      end
    end else begin
      reg_mncause <= _GEN_439;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mcounteren <= 32'h0;
    end else begin
      reg_mcounteren <= _GEN_1236[31:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_scounteren <= 32'h0;
    end else begin
      reg_scounteren <= _GEN_1235[31:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_sepc <= 40'h0;
    end else begin
      reg_sepc <= _GEN_1229[39:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_scause <= 64'h0;
    end else if (csr_wen) begin
      if (_T_188) begin
        reg_scause <= _reg_mcause_T;
      end else begin
        reg_scause <= _GEN_442;
      end
    end else begin
      reg_scause <= _GEN_442;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_stval <= 40'h0;
    end else if (csr_wen) begin
      if (_T_189) begin
        reg_stval <= wdata[39:0];
      end else begin
        reg_stval <= _GEN_444;
      end
    end else begin
      reg_stval <= _GEN_444;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_sscratch <= 64'h0;
    end else if (csr_wen) begin
      if (_T_187) begin
        reg_sscratch <= wdata;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_stvec <= 39'h0;
    end else begin
      reg_stvec <= _GEN_1230[38:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_satp_mode <= 4'h0;
    end else if (csr_wen) begin
      if (_T_190) begin
        if (_T_3133) begin
          reg_satp_mode <= _reg_satp_mode_T;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_satp_ppn <= 44'h0;
    end else if (csr_wen) begin
      if (_T_190) begin
        if (_T_3133) begin
          reg_satp_ppn <= {{19'd0}, new_satp_ppn[24:0]};
        end
      end
    end
  end
  always @(posedge io_ungated_clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_wfi <= 1'h0;
    end else if (reset) begin
      reg_wfi <= 1'h0;
    end else if (|pending_interrupts | io_interrupts_debug | exception | io_interrupts_nmi_rnmi) begin
      reg_wfi <= 1'h0;
    end else begin
      reg_wfi <= _GEN_364;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_fflags <= 5'h0;
    end else begin
      reg_fflags <= _GEN_1217[4:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_frm <= 3'h0;
    end else begin
      reg_frm <= _GEN_1218[2:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_mcountinhibit <= 7'h0;
    end else if (reset) begin
      reg_mcountinhibit <= 7'h0;
    end else begin
      reg_mcountinhibit <= _GEN_1211[6:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      value_lo <= 6'h0;
    end else if (reset) begin
      value_lo <= 6'h0;
    end else begin
      value_lo <= _GEN_1214[5:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      value_hi <= 58'h0;
    end else if (reset) begin
      value_hi <= 58'h0;
    end else if (csr_wen) begin
      if (_T_93) begin
        value_hi <= wdata[63:6];
      end else begin
        value_hi <= _GEN_1;
      end
    end else begin
      value_hi <= _GEN_1;
    end
  end
  always @(posedge io_ungated_clock or posedge rf_reset) begin
    if (rf_reset) begin
      value_lo_1 <= 6'h0;
    end else if (reset) begin
      value_lo_1 <= 6'h0;
    end else begin
      value_lo_1 <= _GEN_1212[5:0];
    end
  end
  always @(posedge io_ungated_clock or posedge rf_reset) begin
    if (rf_reset) begin
      value_hi_1 <= 58'h0;
    end else if (reset) begin
      value_hi_1 <= 58'h0;
    end else if (csr_wen) begin
      if (_T_92) begin
        value_hi_1 <= wdata[63:6];
      end else begin
        value_hi_1 <= _GEN_3;
      end
    end else begin
      value_hi_1 <= _GEN_3;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_hpmevent_0 <= 64'h0;
    end else if (reset) begin
      reg_hpmevent_0 <= 64'h0;
    end else if (csr_wen) begin
      if (_T_94) begin
        reg_hpmevent_0 <= _reg_hpmevent_0_T;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_hpmevent_1 <= 64'h0;
    end else if (reset) begin
      reg_hpmevent_1 <= 64'h0;
    end else if (csr_wen) begin
      if (_T_97) begin
        reg_hpmevent_1 <= _reg_hpmevent_0_T;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_hpmevent_2 <= 64'h0;
    end else if (reset) begin
      reg_hpmevent_2 <= 64'h0;
    end else if (csr_wen) begin
      if (_T_100) begin
        reg_hpmevent_2 <= _reg_hpmevent_0_T;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_hpmevent_3 <= 64'h0;
    end else if (reset) begin
      reg_hpmevent_3 <= 64'h0;
    end else if (csr_wen) begin
      if (_T_103) begin
        reg_hpmevent_3 <= _reg_hpmevent_0_T;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      value_lo_2 <= 6'h0;
    end else begin
      value_lo_2 <= _GEN_1199[5:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      value_hi_2 <= 34'h0;
    end else if (csr_wen) begin
      if (_T_95) begin
        value_hi_2 <= wdata[39:6];
      end else begin
        value_hi_2 <= _GEN_5;
      end
    end else begin
      value_hi_2 <= _GEN_5;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      value_lo_3 <= 6'h0;
    end else begin
      value_lo_3 <= _GEN_1202[5:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      value_hi_3 <= 34'h0;
    end else if (csr_wen) begin
      if (_T_98) begin
        value_hi_3 <= wdata[39:6];
      end else begin
        value_hi_3 <= _GEN_7;
      end
    end else begin
      value_hi_3 <= _GEN_7;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      value_lo_4 <= 6'h0;
    end else begin
      value_lo_4 <= _GEN_1205[5:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      value_hi_4 <= 34'h0;
    end else if (csr_wen) begin
      if (_T_101) begin
        value_hi_4 <= wdata[39:6];
      end else begin
        value_hi_4 <= _GEN_9;
      end
    end else begin
      value_hi_4 <= _GEN_9;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      value_lo_5 <= 6'h0;
    end else begin
      value_lo_5 <= _GEN_1208[5:0];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      value_hi_5 <= 34'h0;
    end else if (csr_wen) begin
      if (_T_104) begin
        value_hi_5 <= wdata[39:6];
      end else begin
        value_hi_5 <= _GEN_11;
      end
    end else begin
      value_hi_5 <= _GEN_11;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_misa <= 64'h0;
    end else if (reset) begin
      reg_misa <= 64'h800000000094112d;
    end else if (csr_wen) begin
      if (_T_71) begin
        if (~io_pc[1] | wdata[2]) begin
          reg_misa <= _reg_misa_T_8;
        end
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_custom_0 <= 64'h0;
    end else if (reset) begin
      reg_custom_0 <= 64'h0;
    end else if (csr_wen) begin
      if (_T_214) begin
        reg_custom_0 <= _reg_custom_0_T_3;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      reg_custom_1 <= 64'h0;
    end else if (reset) begin
      reg_custom_1 <= 64'h2020f;
    end else if (csr_wen) begin
      if (_T_215) begin
        reg_custom_1 <= _reg_custom_1_T_3;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      io_status_dprv_REG <= 2'h0;
    end else if (reg_mstatus_mprv & _T_304) begin
      io_status_dprv_REG <= reg_mstatus_mpp;
    end else begin
      io_status_dprv_REG <= reg_mstatus_prv;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      io_status_cease_r <= 1'h0;
    end else if (reset) begin
      io_status_cease_r <= 1'h0;
    end else begin
      io_status_cease_r <= _GEN_489;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  reg_mstatus_isa = _RAND_0[31:0];
  _RAND_1 = {1{`RANDOM}};
  reg_mstatus_dprv = _RAND_1[1:0];
  _RAND_2 = {1{`RANDOM}};
  reg_mstatus_prv = _RAND_2[1:0];
  _RAND_3 = {1{`RANDOM}};
  reg_mstatus_sd = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  reg_mstatus_zero2 = _RAND_4[26:0];
  _RAND_5 = {1{`RANDOM}};
  reg_mstatus_sxl = _RAND_5[1:0];
  _RAND_6 = {1{`RANDOM}};
  reg_mstatus_uxl = _RAND_6[1:0];
  _RAND_7 = {1{`RANDOM}};
  reg_mstatus_sd_rv32 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  reg_mstatus_zero1 = _RAND_8[7:0];
  _RAND_9 = {1{`RANDOM}};
  reg_mstatus_tsr = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  reg_mstatus_tw = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  reg_mstatus_tvm = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  reg_mstatus_mxr = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  reg_mstatus_sum = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  reg_mstatus_mprv = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  reg_mstatus_xs = _RAND_15[1:0];
  _RAND_16 = {1{`RANDOM}};
  reg_mstatus_fs = _RAND_16[1:0];
  _RAND_17 = {1{`RANDOM}};
  reg_mstatus_mpp = _RAND_17[1:0];
  _RAND_18 = {1{`RANDOM}};
  reg_mstatus_vs = _RAND_18[1:0];
  _RAND_19 = {1{`RANDOM}};
  reg_mstatus_spp = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  reg_mstatus_mpie = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  reg_mstatus_hpie = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  reg_mstatus_spie = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  reg_mstatus_upie = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  reg_mstatus_mie = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  reg_mstatus_hie = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  reg_mstatus_sie = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  reg_mstatus_uie = _RAND_27[0:0];
  _RAND_28 = {1{`RANDOM}};
  reg_dcsr_prv = _RAND_28[1:0];
  _RAND_29 = {1{`RANDOM}};
  reg_mnstatus_mpp = _RAND_29[1:0];
  _RAND_30 = {1{`RANDOM}};
  reg_singleStepped = _RAND_30[0:0];
  _RAND_31 = {1{`RANDOM}};
  reg_dcsr_ebreakm = _RAND_31[0:0];
  _RAND_32 = {1{`RANDOM}};
  reg_dcsr_ebreakh = _RAND_32[0:0];
  _RAND_33 = {1{`RANDOM}};
  reg_dcsr_ebreaks = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  reg_dcsr_ebreaku = _RAND_34[0:0];
  _RAND_35 = {1{`RANDOM}};
  reg_debug = _RAND_35[0:0];
  _RAND_36 = {1{`RANDOM}};
  nmie = _RAND_36[0:0];
  _RAND_37 = {2{`RANDOM}};
  reg_mideleg = _RAND_37[63:0];
  _RAND_38 = {2{`RANDOM}};
  reg_medeleg = _RAND_38[63:0];
  _RAND_39 = {1{`RANDOM}};
  reg_dcsr_xdebugver = _RAND_39[1:0];
  _RAND_40 = {1{`RANDOM}};
  reg_dcsr_zero4 = _RAND_40[1:0];
  _RAND_41 = {1{`RANDOM}};
  reg_dcsr_zero3 = _RAND_41[11:0];
  _RAND_42 = {1{`RANDOM}};
  reg_dcsr_zero2 = _RAND_42[0:0];
  _RAND_43 = {1{`RANDOM}};
  reg_dcsr_stopcycle = _RAND_43[0:0];
  _RAND_44 = {1{`RANDOM}};
  reg_dcsr_stoptime = _RAND_44[0:0];
  _RAND_45 = {1{`RANDOM}};
  reg_dcsr_cause = _RAND_45[2:0];
  _RAND_46 = {1{`RANDOM}};
  reg_dcsr_zero1 = _RAND_46[2:0];
  _RAND_47 = {1{`RANDOM}};
  reg_dcsr_step = _RAND_47[0:0];
  _RAND_48 = {2{`RANDOM}};
  reg_dpc = _RAND_48[39:0];
  _RAND_49 = {2{`RANDOM}};
  reg_dscratch = _RAND_49[63:0];
  _RAND_50 = {1{`RANDOM}};
  reg_tselect = _RAND_50[3:0];
  _RAND_51 = {1{`RANDOM}};
  reg_bp_0_control_dmode = _RAND_51[0:0];
  _RAND_52 = {1{`RANDOM}};
  reg_bp_0_control_action = _RAND_52[2:0];
  _RAND_53 = {1{`RANDOM}};
  reg_bp_0_control_chain = _RAND_53[0:0];
  _RAND_54 = {1{`RANDOM}};
  reg_bp_0_control_tmatch = _RAND_54[1:0];
  _RAND_55 = {1{`RANDOM}};
  reg_bp_0_control_m = _RAND_55[0:0];
  _RAND_56 = {1{`RANDOM}};
  reg_bp_0_control_s = _RAND_56[0:0];
  _RAND_57 = {1{`RANDOM}};
  reg_bp_0_control_u = _RAND_57[0:0];
  _RAND_58 = {1{`RANDOM}};
  reg_bp_0_control_x = _RAND_58[0:0];
  _RAND_59 = {1{`RANDOM}};
  reg_bp_0_control_w = _RAND_59[0:0];
  _RAND_60 = {1{`RANDOM}};
  reg_bp_0_control_r = _RAND_60[0:0];
  _RAND_61 = {2{`RANDOM}};
  reg_bp_0_address = _RAND_61[38:0];
  _RAND_62 = {1{`RANDOM}};
  reg_bp_1_control_dmode = _RAND_62[0:0];
  _RAND_63 = {1{`RANDOM}};
  reg_bp_1_control_action = _RAND_63[2:0];
  _RAND_64 = {1{`RANDOM}};
  reg_bp_1_control_chain = _RAND_64[0:0];
  _RAND_65 = {1{`RANDOM}};
  reg_bp_1_control_tmatch = _RAND_65[1:0];
  _RAND_66 = {1{`RANDOM}};
  reg_bp_1_control_m = _RAND_66[0:0];
  _RAND_67 = {1{`RANDOM}};
  reg_bp_1_control_s = _RAND_67[0:0];
  _RAND_68 = {1{`RANDOM}};
  reg_bp_1_control_u = _RAND_68[0:0];
  _RAND_69 = {1{`RANDOM}};
  reg_bp_1_control_x = _RAND_69[0:0];
  _RAND_70 = {1{`RANDOM}};
  reg_bp_1_control_w = _RAND_70[0:0];
  _RAND_71 = {1{`RANDOM}};
  reg_bp_1_control_r = _RAND_71[0:0];
  _RAND_72 = {2{`RANDOM}};
  reg_bp_1_address = _RAND_72[38:0];
  _RAND_73 = {1{`RANDOM}};
  reg_bp_2_control_dmode = _RAND_73[0:0];
  _RAND_74 = {1{`RANDOM}};
  reg_bp_2_control_action = _RAND_74[2:0];
  _RAND_75 = {1{`RANDOM}};
  reg_bp_2_control_chain = _RAND_75[0:0];
  _RAND_76 = {1{`RANDOM}};
  reg_bp_2_control_tmatch = _RAND_76[1:0];
  _RAND_77 = {1{`RANDOM}};
  reg_bp_2_control_m = _RAND_77[0:0];
  _RAND_78 = {1{`RANDOM}};
  reg_bp_2_control_s = _RAND_78[0:0];
  _RAND_79 = {1{`RANDOM}};
  reg_bp_2_control_u = _RAND_79[0:0];
  _RAND_80 = {1{`RANDOM}};
  reg_bp_2_control_x = _RAND_80[0:0];
  _RAND_81 = {1{`RANDOM}};
  reg_bp_2_control_w = _RAND_81[0:0];
  _RAND_82 = {1{`RANDOM}};
  reg_bp_2_control_r = _RAND_82[0:0];
  _RAND_83 = {2{`RANDOM}};
  reg_bp_2_address = _RAND_83[38:0];
  _RAND_84 = {1{`RANDOM}};
  reg_bp_3_control_dmode = _RAND_84[0:0];
  _RAND_85 = {1{`RANDOM}};
  reg_bp_3_control_action = _RAND_85[2:0];
  _RAND_86 = {1{`RANDOM}};
  reg_bp_3_control_chain = _RAND_86[0:0];
  _RAND_87 = {1{`RANDOM}};
  reg_bp_3_control_tmatch = _RAND_87[1:0];
  _RAND_88 = {1{`RANDOM}};
  reg_bp_3_control_m = _RAND_88[0:0];
  _RAND_89 = {1{`RANDOM}};
  reg_bp_3_control_s = _RAND_89[0:0];
  _RAND_90 = {1{`RANDOM}};
  reg_bp_3_control_u = _RAND_90[0:0];
  _RAND_91 = {1{`RANDOM}};
  reg_bp_3_control_x = _RAND_91[0:0];
  _RAND_92 = {1{`RANDOM}};
  reg_bp_3_control_w = _RAND_92[0:0];
  _RAND_93 = {1{`RANDOM}};
  reg_bp_3_control_r = _RAND_93[0:0];
  _RAND_94 = {2{`RANDOM}};
  reg_bp_3_address = _RAND_94[38:0];
  _RAND_95 = {1{`RANDOM}};
  reg_bp_4_control_dmode = _RAND_95[0:0];
  _RAND_96 = {1{`RANDOM}};
  reg_bp_4_control_action = _RAND_96[2:0];
  _RAND_97 = {1{`RANDOM}};
  reg_bp_4_control_chain = _RAND_97[0:0];
  _RAND_98 = {1{`RANDOM}};
  reg_bp_4_control_tmatch = _RAND_98[1:0];
  _RAND_99 = {1{`RANDOM}};
  reg_bp_4_control_m = _RAND_99[0:0];
  _RAND_100 = {1{`RANDOM}};
  reg_bp_4_control_s = _RAND_100[0:0];
  _RAND_101 = {1{`RANDOM}};
  reg_bp_4_control_u = _RAND_101[0:0];
  _RAND_102 = {1{`RANDOM}};
  reg_bp_4_control_x = _RAND_102[0:0];
  _RAND_103 = {1{`RANDOM}};
  reg_bp_4_control_w = _RAND_103[0:0];
  _RAND_104 = {1{`RANDOM}};
  reg_bp_4_control_r = _RAND_104[0:0];
  _RAND_105 = {2{`RANDOM}};
  reg_bp_4_address = _RAND_105[38:0];
  _RAND_106 = {1{`RANDOM}};
  reg_bp_5_control_dmode = _RAND_106[0:0];
  _RAND_107 = {1{`RANDOM}};
  reg_bp_5_control_action = _RAND_107[2:0];
  _RAND_108 = {1{`RANDOM}};
  reg_bp_5_control_chain = _RAND_108[0:0];
  _RAND_109 = {1{`RANDOM}};
  reg_bp_5_control_tmatch = _RAND_109[1:0];
  _RAND_110 = {1{`RANDOM}};
  reg_bp_5_control_m = _RAND_110[0:0];
  _RAND_111 = {1{`RANDOM}};
  reg_bp_5_control_s = _RAND_111[0:0];
  _RAND_112 = {1{`RANDOM}};
  reg_bp_5_control_u = _RAND_112[0:0];
  _RAND_113 = {1{`RANDOM}};
  reg_bp_5_control_x = _RAND_113[0:0];
  _RAND_114 = {1{`RANDOM}};
  reg_bp_5_control_w = _RAND_114[0:0];
  _RAND_115 = {1{`RANDOM}};
  reg_bp_5_control_r = _RAND_115[0:0];
  _RAND_116 = {2{`RANDOM}};
  reg_bp_5_address = _RAND_116[38:0];
  _RAND_117 = {1{`RANDOM}};
  reg_bp_6_control_dmode = _RAND_117[0:0];
  _RAND_118 = {1{`RANDOM}};
  reg_bp_6_control_action = _RAND_118[2:0];
  _RAND_119 = {1{`RANDOM}};
  reg_bp_6_control_chain = _RAND_119[0:0];
  _RAND_120 = {1{`RANDOM}};
  reg_bp_6_control_tmatch = _RAND_120[1:0];
  _RAND_121 = {1{`RANDOM}};
  reg_bp_6_control_m = _RAND_121[0:0];
  _RAND_122 = {1{`RANDOM}};
  reg_bp_6_control_s = _RAND_122[0:0];
  _RAND_123 = {1{`RANDOM}};
  reg_bp_6_control_u = _RAND_123[0:0];
  _RAND_124 = {1{`RANDOM}};
  reg_bp_6_control_x = _RAND_124[0:0];
  _RAND_125 = {1{`RANDOM}};
  reg_bp_6_control_w = _RAND_125[0:0];
  _RAND_126 = {1{`RANDOM}};
  reg_bp_6_control_r = _RAND_126[0:0];
  _RAND_127 = {2{`RANDOM}};
  reg_bp_6_address = _RAND_127[38:0];
  _RAND_128 = {1{`RANDOM}};
  reg_bp_7_control_dmode = _RAND_128[0:0];
  _RAND_129 = {1{`RANDOM}};
  reg_bp_7_control_action = _RAND_129[2:0];
  _RAND_130 = {1{`RANDOM}};
  reg_bp_7_control_chain = _RAND_130[0:0];
  _RAND_131 = {1{`RANDOM}};
  reg_bp_7_control_tmatch = _RAND_131[1:0];
  _RAND_132 = {1{`RANDOM}};
  reg_bp_7_control_m = _RAND_132[0:0];
  _RAND_133 = {1{`RANDOM}};
  reg_bp_7_control_s = _RAND_133[0:0];
  _RAND_134 = {1{`RANDOM}};
  reg_bp_7_control_u = _RAND_134[0:0];
  _RAND_135 = {1{`RANDOM}};
  reg_bp_7_control_x = _RAND_135[0:0];
  _RAND_136 = {1{`RANDOM}};
  reg_bp_7_control_w = _RAND_136[0:0];
  _RAND_137 = {1{`RANDOM}};
  reg_bp_7_control_r = _RAND_137[0:0];
  _RAND_138 = {2{`RANDOM}};
  reg_bp_7_address = _RAND_138[38:0];
  _RAND_139 = {1{`RANDOM}};
  reg_bp_8_control_dmode = _RAND_139[0:0];
  _RAND_140 = {1{`RANDOM}};
  reg_bp_8_control_action = _RAND_140[2:0];
  _RAND_141 = {1{`RANDOM}};
  reg_bp_8_control_chain = _RAND_141[0:0];
  _RAND_142 = {1{`RANDOM}};
  reg_bp_8_control_tmatch = _RAND_142[1:0];
  _RAND_143 = {1{`RANDOM}};
  reg_bp_8_control_m = _RAND_143[0:0];
  _RAND_144 = {1{`RANDOM}};
  reg_bp_8_control_s = _RAND_144[0:0];
  _RAND_145 = {1{`RANDOM}};
  reg_bp_8_control_u = _RAND_145[0:0];
  _RAND_146 = {1{`RANDOM}};
  reg_bp_8_control_x = _RAND_146[0:0];
  _RAND_147 = {1{`RANDOM}};
  reg_bp_8_control_w = _RAND_147[0:0];
  _RAND_148 = {1{`RANDOM}};
  reg_bp_8_control_r = _RAND_148[0:0];
  _RAND_149 = {2{`RANDOM}};
  reg_bp_8_address = _RAND_149[38:0];
  _RAND_150 = {1{`RANDOM}};
  reg_bp_9_control_dmode = _RAND_150[0:0];
  _RAND_151 = {1{`RANDOM}};
  reg_bp_9_control_action = _RAND_151[2:0];
  _RAND_152 = {1{`RANDOM}};
  reg_bp_9_control_chain = _RAND_152[0:0];
  _RAND_153 = {1{`RANDOM}};
  reg_bp_9_control_tmatch = _RAND_153[1:0];
  _RAND_154 = {1{`RANDOM}};
  reg_bp_9_control_m = _RAND_154[0:0];
  _RAND_155 = {1{`RANDOM}};
  reg_bp_9_control_s = _RAND_155[0:0];
  _RAND_156 = {1{`RANDOM}};
  reg_bp_9_control_u = _RAND_156[0:0];
  _RAND_157 = {1{`RANDOM}};
  reg_bp_9_control_x = _RAND_157[0:0];
  _RAND_158 = {1{`RANDOM}};
  reg_bp_9_control_w = _RAND_158[0:0];
  _RAND_159 = {1{`RANDOM}};
  reg_bp_9_control_r = _RAND_159[0:0];
  _RAND_160 = {2{`RANDOM}};
  reg_bp_9_address = _RAND_160[38:0];
  _RAND_161 = {1{`RANDOM}};
  reg_bp_10_control_dmode = _RAND_161[0:0];
  _RAND_162 = {1{`RANDOM}};
  reg_bp_10_control_action = _RAND_162[2:0];
  _RAND_163 = {1{`RANDOM}};
  reg_bp_10_control_chain = _RAND_163[0:0];
  _RAND_164 = {1{`RANDOM}};
  reg_bp_10_control_tmatch = _RAND_164[1:0];
  _RAND_165 = {1{`RANDOM}};
  reg_bp_10_control_m = _RAND_165[0:0];
  _RAND_166 = {1{`RANDOM}};
  reg_bp_10_control_s = _RAND_166[0:0];
  _RAND_167 = {1{`RANDOM}};
  reg_bp_10_control_u = _RAND_167[0:0];
  _RAND_168 = {1{`RANDOM}};
  reg_bp_10_control_x = _RAND_168[0:0];
  _RAND_169 = {1{`RANDOM}};
  reg_bp_10_control_w = _RAND_169[0:0];
  _RAND_170 = {1{`RANDOM}};
  reg_bp_10_control_r = _RAND_170[0:0];
  _RAND_171 = {2{`RANDOM}};
  reg_bp_10_address = _RAND_171[38:0];
  _RAND_172 = {1{`RANDOM}};
  reg_bp_11_control_dmode = _RAND_172[0:0];
  _RAND_173 = {1{`RANDOM}};
  reg_bp_11_control_action = _RAND_173[2:0];
  _RAND_174 = {1{`RANDOM}};
  reg_bp_11_control_chain = _RAND_174[0:0];
  _RAND_175 = {1{`RANDOM}};
  reg_bp_11_control_tmatch = _RAND_175[1:0];
  _RAND_176 = {1{`RANDOM}};
  reg_bp_11_control_m = _RAND_176[0:0];
  _RAND_177 = {1{`RANDOM}};
  reg_bp_11_control_s = _RAND_177[0:0];
  _RAND_178 = {1{`RANDOM}};
  reg_bp_11_control_u = _RAND_178[0:0];
  _RAND_179 = {1{`RANDOM}};
  reg_bp_11_control_x = _RAND_179[0:0];
  _RAND_180 = {1{`RANDOM}};
  reg_bp_11_control_w = _RAND_180[0:0];
  _RAND_181 = {1{`RANDOM}};
  reg_bp_11_control_r = _RAND_181[0:0];
  _RAND_182 = {2{`RANDOM}};
  reg_bp_11_address = _RAND_182[38:0];
  _RAND_183 = {1{`RANDOM}};
  reg_bp_12_control_dmode = _RAND_183[0:0];
  _RAND_184 = {1{`RANDOM}};
  reg_bp_12_control_action = _RAND_184[2:0];
  _RAND_185 = {1{`RANDOM}};
  reg_bp_12_control_chain = _RAND_185[0:0];
  _RAND_186 = {1{`RANDOM}};
  reg_bp_12_control_tmatch = _RAND_186[1:0];
  _RAND_187 = {1{`RANDOM}};
  reg_bp_12_control_m = _RAND_187[0:0];
  _RAND_188 = {1{`RANDOM}};
  reg_bp_12_control_s = _RAND_188[0:0];
  _RAND_189 = {1{`RANDOM}};
  reg_bp_12_control_u = _RAND_189[0:0];
  _RAND_190 = {1{`RANDOM}};
  reg_bp_12_control_x = _RAND_190[0:0];
  _RAND_191 = {1{`RANDOM}};
  reg_bp_12_control_w = _RAND_191[0:0];
  _RAND_192 = {1{`RANDOM}};
  reg_bp_12_control_r = _RAND_192[0:0];
  _RAND_193 = {2{`RANDOM}};
  reg_bp_12_address = _RAND_193[38:0];
  _RAND_194 = {1{`RANDOM}};
  reg_bp_13_control_dmode = _RAND_194[0:0];
  _RAND_195 = {1{`RANDOM}};
  reg_bp_13_control_action = _RAND_195[2:0];
  _RAND_196 = {1{`RANDOM}};
  reg_bp_13_control_chain = _RAND_196[0:0];
  _RAND_197 = {1{`RANDOM}};
  reg_bp_13_control_tmatch = _RAND_197[1:0];
  _RAND_198 = {1{`RANDOM}};
  reg_bp_13_control_m = _RAND_198[0:0];
  _RAND_199 = {1{`RANDOM}};
  reg_bp_13_control_s = _RAND_199[0:0];
  _RAND_200 = {1{`RANDOM}};
  reg_bp_13_control_u = _RAND_200[0:0];
  _RAND_201 = {1{`RANDOM}};
  reg_bp_13_control_x = _RAND_201[0:0];
  _RAND_202 = {1{`RANDOM}};
  reg_bp_13_control_w = _RAND_202[0:0];
  _RAND_203 = {1{`RANDOM}};
  reg_bp_13_control_r = _RAND_203[0:0];
  _RAND_204 = {2{`RANDOM}};
  reg_bp_13_address = _RAND_204[38:0];
  _RAND_205 = {1{`RANDOM}};
  reg_bp_14_control_dmode = _RAND_205[0:0];
  _RAND_206 = {1{`RANDOM}};
  reg_bp_14_control_action = _RAND_206[2:0];
  _RAND_207 = {1{`RANDOM}};
  reg_bp_14_control_chain = _RAND_207[0:0];
  _RAND_208 = {1{`RANDOM}};
  reg_bp_14_control_tmatch = _RAND_208[1:0];
  _RAND_209 = {1{`RANDOM}};
  reg_bp_14_control_m = _RAND_209[0:0];
  _RAND_210 = {1{`RANDOM}};
  reg_bp_14_control_s = _RAND_210[0:0];
  _RAND_211 = {1{`RANDOM}};
  reg_bp_14_control_u = _RAND_211[0:0];
  _RAND_212 = {1{`RANDOM}};
  reg_bp_14_control_x = _RAND_212[0:0];
  _RAND_213 = {1{`RANDOM}};
  reg_bp_14_control_w = _RAND_213[0:0];
  _RAND_214 = {1{`RANDOM}};
  reg_bp_14_control_r = _RAND_214[0:0];
  _RAND_215 = {2{`RANDOM}};
  reg_bp_14_address = _RAND_215[38:0];
  _RAND_216 = {1{`RANDOM}};
  reg_bp_15_control_dmode = _RAND_216[0:0];
  _RAND_217 = {1{`RANDOM}};
  reg_bp_15_control_action = _RAND_217[2:0];
  _RAND_218 = {1{`RANDOM}};
  reg_bp_15_control_tmatch = _RAND_218[1:0];
  _RAND_219 = {1{`RANDOM}};
  reg_bp_15_control_m = _RAND_219[0:0];
  _RAND_220 = {1{`RANDOM}};
  reg_bp_15_control_s = _RAND_220[0:0];
  _RAND_221 = {1{`RANDOM}};
  reg_bp_15_control_u = _RAND_221[0:0];
  _RAND_222 = {1{`RANDOM}};
  reg_bp_15_control_x = _RAND_222[0:0];
  _RAND_223 = {1{`RANDOM}};
  reg_bp_15_control_w = _RAND_223[0:0];
  _RAND_224 = {1{`RANDOM}};
  reg_bp_15_control_r = _RAND_224[0:0];
  _RAND_225 = {2{`RANDOM}};
  reg_bp_15_address = _RAND_225[38:0];
  _RAND_226 = {1{`RANDOM}};
  reg_pmp_0_cfg_l = _RAND_226[0:0];
  _RAND_227 = {1{`RANDOM}};
  reg_pmp_0_cfg_a = _RAND_227[1:0];
  _RAND_228 = {1{`RANDOM}};
  reg_pmp_0_cfg_x = _RAND_228[0:0];
  _RAND_229 = {1{`RANDOM}};
  reg_pmp_0_cfg_w = _RAND_229[0:0];
  _RAND_230 = {1{`RANDOM}};
  reg_pmp_0_cfg_r = _RAND_230[0:0];
  _RAND_231 = {2{`RANDOM}};
  reg_pmp_0_addr = _RAND_231[34:0];
  _RAND_232 = {1{`RANDOM}};
  reg_pmp_1_cfg_l = _RAND_232[0:0];
  _RAND_233 = {1{`RANDOM}};
  reg_pmp_1_cfg_a = _RAND_233[1:0];
  _RAND_234 = {1{`RANDOM}};
  reg_pmp_1_cfg_x = _RAND_234[0:0];
  _RAND_235 = {1{`RANDOM}};
  reg_pmp_1_cfg_w = _RAND_235[0:0];
  _RAND_236 = {1{`RANDOM}};
  reg_pmp_1_cfg_r = _RAND_236[0:0];
  _RAND_237 = {2{`RANDOM}};
  reg_pmp_1_addr = _RAND_237[34:0];
  _RAND_238 = {1{`RANDOM}};
  reg_pmp_2_cfg_l = _RAND_238[0:0];
  _RAND_239 = {1{`RANDOM}};
  reg_pmp_2_cfg_a = _RAND_239[1:0];
  _RAND_240 = {1{`RANDOM}};
  reg_pmp_2_cfg_x = _RAND_240[0:0];
  _RAND_241 = {1{`RANDOM}};
  reg_pmp_2_cfg_w = _RAND_241[0:0];
  _RAND_242 = {1{`RANDOM}};
  reg_pmp_2_cfg_r = _RAND_242[0:0];
  _RAND_243 = {2{`RANDOM}};
  reg_pmp_2_addr = _RAND_243[34:0];
  _RAND_244 = {1{`RANDOM}};
  reg_pmp_3_cfg_l = _RAND_244[0:0];
  _RAND_245 = {1{`RANDOM}};
  reg_pmp_3_cfg_a = _RAND_245[1:0];
  _RAND_246 = {1{`RANDOM}};
  reg_pmp_3_cfg_x = _RAND_246[0:0];
  _RAND_247 = {1{`RANDOM}};
  reg_pmp_3_cfg_w = _RAND_247[0:0];
  _RAND_248 = {1{`RANDOM}};
  reg_pmp_3_cfg_r = _RAND_248[0:0];
  _RAND_249 = {2{`RANDOM}};
  reg_pmp_3_addr = _RAND_249[34:0];
  _RAND_250 = {1{`RANDOM}};
  reg_pmp_4_cfg_l = _RAND_250[0:0];
  _RAND_251 = {1{`RANDOM}};
  reg_pmp_4_cfg_a = _RAND_251[1:0];
  _RAND_252 = {1{`RANDOM}};
  reg_pmp_4_cfg_x = _RAND_252[0:0];
  _RAND_253 = {1{`RANDOM}};
  reg_pmp_4_cfg_w = _RAND_253[0:0];
  _RAND_254 = {1{`RANDOM}};
  reg_pmp_4_cfg_r = _RAND_254[0:0];
  _RAND_255 = {2{`RANDOM}};
  reg_pmp_4_addr = _RAND_255[34:0];
  _RAND_256 = {1{`RANDOM}};
  reg_pmp_5_cfg_l = _RAND_256[0:0];
  _RAND_257 = {1{`RANDOM}};
  reg_pmp_5_cfg_a = _RAND_257[1:0];
  _RAND_258 = {1{`RANDOM}};
  reg_pmp_5_cfg_x = _RAND_258[0:0];
  _RAND_259 = {1{`RANDOM}};
  reg_pmp_5_cfg_w = _RAND_259[0:0];
  _RAND_260 = {1{`RANDOM}};
  reg_pmp_5_cfg_r = _RAND_260[0:0];
  _RAND_261 = {2{`RANDOM}};
  reg_pmp_5_addr = _RAND_261[34:0];
  _RAND_262 = {1{`RANDOM}};
  reg_pmp_6_cfg_l = _RAND_262[0:0];
  _RAND_263 = {1{`RANDOM}};
  reg_pmp_6_cfg_a = _RAND_263[1:0];
  _RAND_264 = {1{`RANDOM}};
  reg_pmp_6_cfg_x = _RAND_264[0:0];
  _RAND_265 = {1{`RANDOM}};
  reg_pmp_6_cfg_w = _RAND_265[0:0];
  _RAND_266 = {1{`RANDOM}};
  reg_pmp_6_cfg_r = _RAND_266[0:0];
  _RAND_267 = {2{`RANDOM}};
  reg_pmp_6_addr = _RAND_267[34:0];
  _RAND_268 = {1{`RANDOM}};
  reg_pmp_7_cfg_l = _RAND_268[0:0];
  _RAND_269 = {1{`RANDOM}};
  reg_pmp_7_cfg_a = _RAND_269[1:0];
  _RAND_270 = {1{`RANDOM}};
  reg_pmp_7_cfg_x = _RAND_270[0:0];
  _RAND_271 = {1{`RANDOM}};
  reg_pmp_7_cfg_w = _RAND_271[0:0];
  _RAND_272 = {1{`RANDOM}};
  reg_pmp_7_cfg_r = _RAND_272[0:0];
  _RAND_273 = {2{`RANDOM}};
  reg_pmp_7_addr = _RAND_273[34:0];
  _RAND_274 = {2{`RANDOM}};
  reg_mie = _RAND_274[63:0];
  _RAND_275 = {1{`RANDOM}};
  reg_mip_seip = _RAND_275[0:0];
  _RAND_276 = {1{`RANDOM}};
  reg_mip_stip = _RAND_276[0:0];
  _RAND_277 = {1{`RANDOM}};
  reg_mip_ssip = _RAND_277[0:0];
  _RAND_278 = {2{`RANDOM}};
  reg_mepc = _RAND_278[39:0];
  _RAND_279 = {2{`RANDOM}};
  reg_mcause = _RAND_279[63:0];
  _RAND_280 = {2{`RANDOM}};
  reg_mtval = _RAND_280[39:0];
  _RAND_281 = {2{`RANDOM}};
  reg_mscratch = _RAND_281[63:0];
  _RAND_282 = {2{`RANDOM}};
  reg_mtvec = _RAND_282[36:0];
  _RAND_283 = {2{`RANDOM}};
  reg_mnscratch = _RAND_283[63:0];
  _RAND_284 = {2{`RANDOM}};
  reg_mnepc = _RAND_284[39:0];
  _RAND_285 = {2{`RANDOM}};
  reg_mncause = _RAND_285[63:0];
  _RAND_286 = {1{`RANDOM}};
  reg_mcounteren = _RAND_286[31:0];
  _RAND_287 = {1{`RANDOM}};
  reg_scounteren = _RAND_287[31:0];
  _RAND_288 = {2{`RANDOM}};
  reg_sepc = _RAND_288[39:0];
  _RAND_289 = {2{`RANDOM}};
  reg_scause = _RAND_289[63:0];
  _RAND_290 = {2{`RANDOM}};
  reg_stval = _RAND_290[39:0];
  _RAND_291 = {2{`RANDOM}};
  reg_sscratch = _RAND_291[63:0];
  _RAND_292 = {2{`RANDOM}};
  reg_stvec = _RAND_292[38:0];
  _RAND_293 = {1{`RANDOM}};
  reg_satp_mode = _RAND_293[3:0];
  _RAND_294 = {2{`RANDOM}};
  reg_satp_ppn = _RAND_294[43:0];
  _RAND_295 = {1{`RANDOM}};
  reg_wfi = _RAND_295[0:0];
  _RAND_296 = {1{`RANDOM}};
  reg_fflags = _RAND_296[4:0];
  _RAND_297 = {1{`RANDOM}};
  reg_frm = _RAND_297[2:0];
  _RAND_298 = {1{`RANDOM}};
  reg_mcountinhibit = _RAND_298[6:0];
  _RAND_299 = {1{`RANDOM}};
  value_lo = _RAND_299[5:0];
  _RAND_300 = {2{`RANDOM}};
  value_hi = _RAND_300[57:0];
  _RAND_301 = {1{`RANDOM}};
  value_lo_1 = _RAND_301[5:0];
  _RAND_302 = {2{`RANDOM}};
  value_hi_1 = _RAND_302[57:0];
  _RAND_303 = {2{`RANDOM}};
  reg_hpmevent_0 = _RAND_303[63:0];
  _RAND_304 = {2{`RANDOM}};
  reg_hpmevent_1 = _RAND_304[63:0];
  _RAND_305 = {2{`RANDOM}};
  reg_hpmevent_2 = _RAND_305[63:0];
  _RAND_306 = {2{`RANDOM}};
  reg_hpmevent_3 = _RAND_306[63:0];
  _RAND_307 = {1{`RANDOM}};
  value_lo_2 = _RAND_307[5:0];
  _RAND_308 = {2{`RANDOM}};
  value_hi_2 = _RAND_308[33:0];
  _RAND_309 = {1{`RANDOM}};
  value_lo_3 = _RAND_309[5:0];
  _RAND_310 = {2{`RANDOM}};
  value_hi_3 = _RAND_310[33:0];
  _RAND_311 = {1{`RANDOM}};
  value_lo_4 = _RAND_311[5:0];
  _RAND_312 = {2{`RANDOM}};
  value_hi_4 = _RAND_312[33:0];
  _RAND_313 = {1{`RANDOM}};
  value_lo_5 = _RAND_313[5:0];
  _RAND_314 = {2{`RANDOM}};
  value_hi_5 = _RAND_314[33:0];
  _RAND_315 = {2{`RANDOM}};
  reg_misa = _RAND_315[63:0];
  _RAND_316 = {2{`RANDOM}};
  reg_custom_0 = _RAND_316[63:0];
  _RAND_317 = {2{`RANDOM}};
  reg_custom_1 = _RAND_317[63:0];
  _RAND_318 = {1{`RANDOM}};
  io_status_dprv_REG = _RAND_318[1:0];
  _RAND_319 = {1{`RANDOM}};
  io_status_cease_r = _RAND_319[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    reg_mstatus_isa = 32'h0;
  end
  if (rf_reset) begin
    reg_mstatus_dprv = 2'h0;
  end
  if (rf_reset) begin
    reg_mstatus_prv = 2'h0;
  end
  if (rf_reset) begin
    reg_mstatus_sd = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_zero2 = 27'h0;
  end
  if (rf_reset) begin
    reg_mstatus_sxl = 2'h0;
  end
  if (rf_reset) begin
    reg_mstatus_uxl = 2'h0;
  end
  if (rf_reset) begin
    reg_mstatus_sd_rv32 = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_zero1 = 8'h0;
  end
  if (rf_reset) begin
    reg_mstatus_tsr = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_tw = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_tvm = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_mxr = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_sum = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_mprv = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_xs = 2'h0;
  end
  if (rf_reset) begin
    reg_mstatus_fs = 2'h0;
  end
  if (rf_reset) begin
    reg_mstatus_mpp = 2'h0;
  end
  if (rf_reset) begin
    reg_mstatus_vs = 2'h0;
  end
  if (rf_reset) begin
    reg_mstatus_spp = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_mpie = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_hpie = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_spie = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_upie = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_mie = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_hie = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_sie = 1'h0;
  end
  if (rf_reset) begin
    reg_mstatus_uie = 1'h0;
  end
  if (rf_reset) begin
    reg_dcsr_prv = 2'h0;
  end
  if (rf_reset) begin
    reg_mnstatus_mpp = 2'h0;
  end
  if (rf_reset) begin
    reg_singleStepped = 1'h0;
  end
  if (rf_reset) begin
    reg_dcsr_ebreakm = 1'h0;
  end
  if (rf_reset) begin
    reg_dcsr_ebreakh = 1'h0;
  end
  if (rf_reset) begin
    reg_dcsr_ebreaks = 1'h0;
  end
  if (rf_reset) begin
    reg_dcsr_ebreaku = 1'h0;
  end
  if (rf_reset) begin
    reg_debug = 1'h0;
  end
  if (rf_reset) begin
    nmie = 1'h0;
  end
  if (rf_reset) begin
    reg_mideleg = 64'h0;
  end
  if (rf_reset) begin
    reg_medeleg = 64'h0;
  end
  if (rf_reset) begin
    reg_dcsr_xdebugver = 2'h0;
  end
  if (rf_reset) begin
    reg_dcsr_zero4 = 2'h0;
  end
  if (rf_reset) begin
    reg_dcsr_zero3 = 12'h0;
  end
  if (rf_reset) begin
    reg_dcsr_zero2 = 1'h0;
  end
  if (rf_reset) begin
    reg_dcsr_stopcycle = 1'h0;
  end
  if (rf_reset) begin
    reg_dcsr_stoptime = 1'h0;
  end
  if (rf_reset) begin
    reg_dcsr_cause = 3'h0;
  end
  if (rf_reset) begin
    reg_dcsr_zero1 = 3'h0;
  end
  if (rf_reset) begin
    reg_dcsr_step = 1'h0;
  end
  if (rf_reset) begin
    reg_dpc = 40'h0;
  end
  if (rf_reset) begin
    reg_dscratch = 64'h0;
  end
  if (rf_reset) begin
    reg_tselect = 4'h0;
  end
  if (rf_reset) begin
    reg_bp_0_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_0_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_0_control_chain = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_0_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_0_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_0_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_0_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_0_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_0_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_0_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_0_address = 39'h0;
  end
  if (rf_reset) begin
    reg_bp_1_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_1_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_1_control_chain = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_1_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_1_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_1_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_1_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_1_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_1_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_1_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_1_address = 39'h0;
  end
  if (rf_reset) begin
    reg_bp_2_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_2_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_2_control_chain = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_2_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_2_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_2_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_2_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_2_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_2_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_2_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_2_address = 39'h0;
  end
  if (rf_reset) begin
    reg_bp_3_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_3_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_3_control_chain = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_3_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_3_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_3_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_3_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_3_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_3_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_3_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_3_address = 39'h0;
  end
  if (rf_reset) begin
    reg_bp_4_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_4_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_4_control_chain = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_4_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_4_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_4_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_4_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_4_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_4_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_4_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_4_address = 39'h0;
  end
  if (rf_reset) begin
    reg_bp_5_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_5_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_5_control_chain = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_5_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_5_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_5_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_5_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_5_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_5_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_5_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_5_address = 39'h0;
  end
  if (rf_reset) begin
    reg_bp_6_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_6_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_6_control_chain = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_6_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_6_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_6_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_6_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_6_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_6_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_6_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_6_address = 39'h0;
  end
  if (rf_reset) begin
    reg_bp_7_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_7_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_7_control_chain = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_7_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_7_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_7_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_7_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_7_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_7_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_7_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_7_address = 39'h0;
  end
  if (rf_reset) begin
    reg_bp_8_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_8_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_8_control_chain = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_8_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_8_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_8_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_8_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_8_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_8_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_8_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_8_address = 39'h0;
  end
  if (rf_reset) begin
    reg_bp_9_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_9_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_9_control_chain = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_9_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_9_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_9_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_9_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_9_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_9_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_9_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_9_address = 39'h0;
  end
  if (rf_reset) begin
    reg_bp_10_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_10_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_10_control_chain = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_10_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_10_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_10_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_10_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_10_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_10_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_10_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_10_address = 39'h0;
  end
  if (rf_reset) begin
    reg_bp_11_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_11_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_11_control_chain = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_11_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_11_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_11_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_11_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_11_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_11_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_11_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_11_address = 39'h0;
  end
  if (rf_reset) begin
    reg_bp_12_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_12_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_12_control_chain = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_12_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_12_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_12_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_12_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_12_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_12_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_12_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_12_address = 39'h0;
  end
  if (rf_reset) begin
    reg_bp_13_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_13_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_13_control_chain = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_13_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_13_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_13_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_13_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_13_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_13_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_13_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_13_address = 39'h0;
  end
  if (rf_reset) begin
    reg_bp_14_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_14_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_14_control_chain = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_14_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_14_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_14_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_14_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_14_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_14_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_14_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_14_address = 39'h0;
  end
  if (rf_reset) begin
    reg_bp_15_control_dmode = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_15_control_action = 3'h0;
  end
  if (rf_reset) begin
    reg_bp_15_control_tmatch = 2'h0;
  end
  if (rf_reset) begin
    reg_bp_15_control_m = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_15_control_s = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_15_control_u = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_15_control_x = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_15_control_w = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_15_control_r = 1'h0;
  end
  if (rf_reset) begin
    reg_bp_15_address = 39'h0;
  end
  if (rf_reset) begin
    reg_pmp_0_cfg_l = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_0_cfg_a = 2'h0;
  end
  if (rf_reset) begin
    reg_pmp_0_cfg_x = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_0_cfg_w = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_0_cfg_r = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_0_addr = 35'h0;
  end
  if (rf_reset) begin
    reg_pmp_1_cfg_l = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_1_cfg_a = 2'h0;
  end
  if (rf_reset) begin
    reg_pmp_1_cfg_x = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_1_cfg_w = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_1_cfg_r = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_1_addr = 35'h0;
  end
  if (rf_reset) begin
    reg_pmp_2_cfg_l = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_2_cfg_a = 2'h0;
  end
  if (rf_reset) begin
    reg_pmp_2_cfg_x = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_2_cfg_w = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_2_cfg_r = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_2_addr = 35'h0;
  end
  if (rf_reset) begin
    reg_pmp_3_cfg_l = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_3_cfg_a = 2'h0;
  end
  if (rf_reset) begin
    reg_pmp_3_cfg_x = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_3_cfg_w = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_3_cfg_r = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_3_addr = 35'h0;
  end
  if (rf_reset) begin
    reg_pmp_4_cfg_l = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_4_cfg_a = 2'h0;
  end
  if (rf_reset) begin
    reg_pmp_4_cfg_x = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_4_cfg_w = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_4_cfg_r = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_4_addr = 35'h0;
  end
  if (rf_reset) begin
    reg_pmp_5_cfg_l = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_5_cfg_a = 2'h0;
  end
  if (rf_reset) begin
    reg_pmp_5_cfg_x = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_5_cfg_w = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_5_cfg_r = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_5_addr = 35'h0;
  end
  if (rf_reset) begin
    reg_pmp_6_cfg_l = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_6_cfg_a = 2'h0;
  end
  if (rf_reset) begin
    reg_pmp_6_cfg_x = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_6_cfg_w = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_6_cfg_r = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_6_addr = 35'h0;
  end
  if (rf_reset) begin
    reg_pmp_7_cfg_l = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_7_cfg_a = 2'h0;
  end
  if (rf_reset) begin
    reg_pmp_7_cfg_x = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_7_cfg_w = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_7_cfg_r = 1'h0;
  end
  if (rf_reset) begin
    reg_pmp_7_addr = 35'h0;
  end
  if (rf_reset) begin
    reg_mie = 64'h0;
  end
  if (rf_reset) begin
    reg_mip_seip = 1'h0;
  end
  if (rf_reset) begin
    reg_mip_stip = 1'h0;
  end
  if (rf_reset) begin
    reg_mip_ssip = 1'h0;
  end
  if (rf_reset) begin
    reg_mepc = 40'h0;
  end
  if (rf_reset) begin
    reg_mcause = 64'h0;
  end
  if (rf_reset) begin
    reg_mtval = 40'h0;
  end
  if (rf_reset) begin
    reg_mscratch = 64'h0;
  end
  if (rf_reset) begin
    reg_mtvec = 37'h0;
  end
  if (rf_reset) begin
    reg_mnscratch = 64'h0;
  end
  if (rf_reset) begin
    reg_mnepc = 40'h0;
  end
  if (rf_reset) begin
    reg_mncause = 64'h0;
  end
  if (rf_reset) begin
    reg_mcounteren = 32'h0;
  end
  if (rf_reset) begin
    reg_scounteren = 32'h0;
  end
  if (rf_reset) begin
    reg_sepc = 40'h0;
  end
  if (rf_reset) begin
    reg_scause = 64'h0;
  end
  if (rf_reset) begin
    reg_stval = 40'h0;
  end
  if (rf_reset) begin
    reg_sscratch = 64'h0;
  end
  if (rf_reset) begin
    reg_stvec = 39'h0;
  end
  if (rf_reset) begin
    reg_satp_mode = 4'h0;
  end
  if (rf_reset) begin
    reg_satp_ppn = 44'h0;
  end
  if (rf_reset) begin
    reg_wfi = 1'h0;
  end
  if (rf_reset) begin
    reg_fflags = 5'h0;
  end
  if (rf_reset) begin
    reg_frm = 3'h0;
  end
  if (rf_reset) begin
    reg_mcountinhibit = 7'h0;
  end
  if (rf_reset) begin
    value_lo = 6'h0;
  end
  if (rf_reset) begin
    value_hi = 58'h0;
  end
  if (rf_reset) begin
    value_lo_1 = 6'h0;
  end
  if (rf_reset) begin
    value_hi_1 = 58'h0;
  end
  if (rf_reset) begin
    reg_hpmevent_0 = 64'h0;
  end
  if (rf_reset) begin
    reg_hpmevent_1 = 64'h0;
  end
  if (rf_reset) begin
    reg_hpmevent_2 = 64'h0;
  end
  if (rf_reset) begin
    reg_hpmevent_3 = 64'h0;
  end
  if (rf_reset) begin
    value_lo_2 = 6'h0;
  end
  if (rf_reset) begin
    value_hi_2 = 34'h0;
  end
  if (rf_reset) begin
    value_lo_3 = 6'h0;
  end
  if (rf_reset) begin
    value_hi_3 = 34'h0;
  end
  if (rf_reset) begin
    value_lo_4 = 6'h0;
  end
  if (rf_reset) begin
    value_hi_4 = 34'h0;
  end
  if (rf_reset) begin
    value_lo_5 = 6'h0;
  end
  if (rf_reset) begin
    value_hi_5 = 34'h0;
  end
  if (rf_reset) begin
    reg_misa = 64'h0;
  end
  if (rf_reset) begin
    reg_custom_0 = 64'h0;
  end
  if (rf_reset) begin
    reg_custom_1 = 64'h0;
  end
  if (rf_reset) begin
    io_status_dprv_REG = 2'h0;
  end
  if (rf_reset) begin
    io_status_cease_r = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
