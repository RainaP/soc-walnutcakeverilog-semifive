//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_Frontend_assert(
  input   clock,
  input   reset,
  input   io_cpu_might_request,
  input   io_cpu_req_valid,
  input   io_cpu_sfence_valid,
  input   io_cpu_btb_update_valid,
  input   io_cpu_bht_update_valid,
  input   io_cpu_flush_icache,
  input   clock_gate_0_out,
  input   s2_speculative,
  input   s2_partial_insn_valid,
  input   _T_107,
  input   _T_51,
  input   icache_io_s2_kill
);
  wire  _T_2; // @[Frontend.scala 90:29]
  wire  _T_3; // @[Frontend.scala 90:52]
  wire  _T_4; // @[Frontend.scala 90:75]
  wire  _T_5; // @[Frontend.scala 90:102]
  wire  _T_6; // @[Frontend.scala 90:10]
  wire  _T_7; // @[Frontend.scala 90:130]
  wire  _T_9; // @[Frontend.scala 90:9]
  wire  _T_10; // @[Frontend.scala 90:9]
  wire  _T_77; // @[Frontend.scala 177:27]
  wire  _T_78; // @[Frontend.scala 177:113]
  wire  _T_79; // @[Frontend.scala 177:110]
  wire  _T_80; // @[Frontend.scala 177:10]
  wire  _T_82; // @[Frontend.scala 177:9]
  wire  _T_83; // @[Frontend.scala 177:9]
  wire  _T_654; // @[Frontend.scala 322:12]
  wire  _T_656; // @[Frontend.scala 322:35]
  wire  _T_658; // @[Frontend.scala 322:11]
  wire  _T_659; // @[Frontend.scala 322:11]
  assign _T_2 = io_cpu_req_valid | io_cpu_sfence_valid; // @[Frontend.scala 90:29]
  assign _T_3 = _T_2 | io_cpu_flush_icache; // @[Frontend.scala 90:52]
  assign _T_4 = _T_3 | io_cpu_bht_update_valid; // @[Frontend.scala 90:75]
  assign _T_5 = _T_4 | io_cpu_btb_update_valid; // @[Frontend.scala 90:102]
  assign _T_6 = ~_T_5; // @[Frontend.scala 90:10]
  assign _T_7 = _T_6 | io_cpu_might_request; // @[Frontend.scala 90:130]
  assign _T_9 = _T_7 | reset; // @[Frontend.scala 90:9]
  assign _T_10 = ~_T_9; // @[Frontend.scala 90:9]
  assign _T_77 = s2_speculative & _T_51; // @[Frontend.scala 177:27]
  assign _T_78 = ~icache_io_s2_kill; // @[Frontend.scala 177:113]
  assign _T_79 = _T_77 & _T_78; // @[Frontend.scala 177:110]
  assign _T_80 = ~_T_79; // @[Frontend.scala 177:10]
  assign _T_82 = _T_80 | reset; // @[Frontend.scala 177:9]
  assign _T_83 = ~_T_82; // @[Frontend.scala 177:9]
  assign _T_654 = ~s2_partial_insn_valid; // @[Frontend.scala 322:12]
  assign _T_656 = _T_654 | _T_107; // @[Frontend.scala 322:35]
  assign _T_658 = _T_656 | reset; // @[Frontend.scala 322:11]
  assign _T_659 = ~_T_658; // @[Frontend.scala 322:11]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_10) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Frontend.scala:90 assert(!(io.cpu.req.valid || io.cpu.sfence.valid || io.cpu.flush_icache || io.cpu.bht_update.valid || io.cpu.btb_update.valid) || io.cpu.might_request)\n"); // @[Frontend.scala 90:9]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_10) begin
          $fatal; // @[Frontend.scala 90:9]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end
  always @(posedge clock_gate_0_out) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_83) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Frontend.scala:177 assert(!(s2_speculative && io.ptw.customCSRs.asInstanceOf[RocketCustomCSRs].disableSpeculativeICacheRefill && !icache.io.s2_kill))\n"); // @[Frontend.scala 177:9]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_83) begin
          $fatal; // @[Frontend.scala 177:9]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_659) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Frontend.scala:322 assert(!s2_partial_insn_valid || fq.io.enq.bits.mask(0))\n"); // @[Frontend.scala 322:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_659) begin
          $fatal; // @[Frontend.scala 322:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
