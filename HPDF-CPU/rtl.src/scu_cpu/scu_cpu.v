//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade 
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module scu_cpu (
	 input         CLK__OSC
	,input         CLK__SUBSYSTEM
	,input         RSTN__SUBSYSTEM
	,output        CLK__CPU
	,output        RSTN__CPU
	,output        CLK__CPUCORE0
	,output        RSTN__CPUCORE0
	,output        CLK__CPUCORE1
	,output        RSTN__CPUCORE1
	,output        CLK__CBUS
	,output        RSTN__CBUS
	,output        CLK__DBUS
	,output        RSTN__DBUS
	,output        CLK__FBUS
	,output        RSTN__FBUS
	,output        CLK__ROM
	,output        RSTN__ROM
	,output        CLK__SRAM
	,output        RSTN__SRAM
	,output        CLK__DEBUG
	,output        RSTN__DEBUG
	,output        CLK__TRACE_COM
	,output        RSTN__TRACE_COM
	,output        CLK__TRACE_0
	,output        RSTN__TRACE_0
	,output        CLK__TRACE_1
	,output        RSTN__TRACE_1
	,output        CLK__TS
	,output        RSTN__TS
	,output        CLK__FBUS_RX
	,output        CLK__DBUS_TX
	,output        CLK__CBUS_TX
	,output        RSTN__FBUS_RX
	,output        RSTN__DBUS_TX
	,output        RSTN__CBUS_TX
	,input  [31:0] APB3__CBUS__PADDR
	,input         DFT__SCAN_CLK
	,output [ 9:0] SFR__SYSPORT_AWUSER
	,output [ 9:0] SFR__SYSPORT_ARUSER
	,output        SFR__SUPPRESS_STRB_ON_CORRUPT
	,output        SFR__RTC_TOGGLE
	,output        SFR__RESETCTRL_HARTISINRESET_1
	,output        SFR__RESETCTRL_HARTISINRESET_0
	,output        SFR__PSD_TEST_CLOCK_ENABLE
	,output [ 9:0] SFR__MEMPORT_AWUSER
	,output [ 9:0] SFR__MEMPORT_ARUSER
	,output [ 3:0] SFR__DEBUG_SYSTEMJTAG_VERSION
	,output [15:0] SFR__DEBUG_SYSTEMJTAG_PART_NUMBER
	,output [10:0] SFR__DEBUG_SYSTEMJTAG_MFR_ID
	,output        SFR__DEBUG_DMACTIVEACK
	,input         SFR__WFI_FROM_TILE_1
	,input         SFR__WFI_FROM_TILE_0
	,input         SFR__HALT_FROM_TILE_1
	,input         SFR__HALT_FROM_TILE_0
	,input         SFR__DEBUG_NDRESET
	,input         SFR__DEBUG_FROM_TILE_1
	,input         SFR__DEBUG_FROM_TILE_0
	,input         SFR__DEBUG_DMACTIVE
	,input         SFR__CEASE_FROM_TILE_1
	,input         SFR__CEASE_FROM_TILE_0
	,output        APB3__CBUS__PSLVERR
	,output [31:0] APB3__CBUS__PRDATA
	,output        APB3__CBUS__PREADY
	,input         APB3__CBUS__PWRITE
	,input  [31:0] APB3__CBUS__PWDATA
	,input         APB3__CBUS__PSEL
	,input         APB3__CBUS__PENABLE
	,output [ 7:0] DFT__PLL_CPU_OUT
	,input         DFT__PLL_CPU
	,input  [52:0] DFT__PLL_CPU_IN
	,input         DFT__SCAN_MODE
	,input         DFT__SCAN_RSTN
	,input         DFT__BIST_MODE
);
	wire       SCR_CPU__PLL_CPU__SCR__PLL_CPU_VCO_BOOST;
	wire [2:0] SCR_CPU__PLL_CPU__SCR__PLL_CPU_S;
	wire [3:0] SCR_CPU__PLL_CPU__SCR__PLL_CPU_RSEL;
	wire       SCR_CPU__PLL_CPU__SCR__PLL_CPU_RESETB;
	wire       SCR_CPU__PLL_CPU__SCR__PLL_CPU_PBIAS_CTRL_EN;
	wire       SCR_CPU__PLL_CPU__SCR__PLL_CPU_PBIAS_CTRL;
	wire [5:0] SCR_CPU__PLL_CPU__SCR__PLL_CPU_P;
	wire [9:0] SCR_CPU__PLL_CPU__SCR__PLL_CPU_M;
	wire       SCR_CPU__PLL_CPU__SCR__PLL_CPU_LRD_EN;
	wire       SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK_EN;
	wire [1:0] SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK_CON_REV;
	wire [1:0] SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK_CON_OUT;
	wire [1:0] SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK_CON_IN;
	wire [1:0] SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK_CON_DLY;
	wire [1:0] SCR_CPU__PLL_CPU__SCR__PLL_CPU_ICP;
	wire       SCR_CPU__PLL_CPU__SCR__PLL_CPU_FSEL;
	wire       SCR_CPU__PLL_CPU__SCR__PLL_CPU_FOUT_MASK;
	wire       SCR_CPU__PLL_CPU__SCR__PLL_CPU_FEED_EN;
	wire [4:0] SCR_CPU__PLL_CPU__SCR__PLL_CPU_EXTAFC;
	wire       SCR_CPU__PLL_CPU__SCR__PLL_CPU_BYPASS;
	wire       SCR_CPU__PLL_CPU__SCR__PLL_CPU_AFC_ENB;
	wire       SCR_CPU__PLL_CPU__SCR__PLL_CPU_AFCINIT_SEL;
	wire       SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK;
	wire [4:0] SCR_CPU__PLL_CPU__SCR__PLL_CPU_AFC_CODE;
	wire       SCR_CPU__GLCM_CPUCORE__SCR__CLKSEL0;
	wire       SCR_CPU__GLCM_DEBUG__SCR__CLKSEL1;
	wire       SCR_CPU__GLCM_TS__SCR__CLKSEL2;
	wire [7:0] SCR_CPU__CLKDIV_CPUCORE__SCR__CLKDIV0;
	wire [7:0] SCR_CPU__CLKDIV_DEBUG__SCR__CLKDIV1;
	wire [7:0] SCR_CPU__CLKDIV_TS__SCR__CLKDIV2;
	wire [2:0] SCR_CPU__CRG_CPUCORE__SFR__SCU__CLKDIS;
	wire [2:0] SCR_CPU__CRG_CPUCORE__SFR__SCU__RSTREQN;
	wire [7:0] SCR_CPU__CRG_CPUCORE__SCR__RSTCYCLE0;
	wire [6:0] SCR_CPU__CRG_BUS__SFR__SCU__CLKDIS;
	wire [6:0] SCR_CPU__CRG_BUS__SFR__SCU__RSTREQN;
	wire [7:0] SCR_CPU__CRG_BUS__SCR__RSTCYCLE1;
	wire [1:0] SCR_CPU__CRG_MEM__SFR__SCU__CLKDIS;
	wire [1:0] SCR_CPU__CRG_MEM__SFR__SCU__RSTREQN;
	wire [7:0] SCR_CPU__CRG_MEM__SCR__RSTCYCLE2;
	wire [3:0] SCR_CPU__CRG_DEBUG__SFR__SCU__CLKDIS;
	wire [3:0] SCR_CPU__CRG_DEBUG__SFR__SCU__RSTREQN;
	wire [7:0] SCR_CPU__CRG_DEBUG__SCR__RSTCYCLE3;
	wire       SCR_CPU__CRG_TS__SFR__SCU__CLKDIS;
	wire       SCR_CPU__CRG_TS__SFR__SCU__RSTREQN;
	wire [7:0] SCR_CPU__CRG_TS__SCR__RSTCYCLE4;
	wire       SCR_CPU__CRG_CPUCORE__RSTACK__IN;
	wire       SCR_CPU__CRG_BUS__RSTACK__IN;
	wire       SCR_CPU__CRG_MEM__RSTACK__IN;
	wire       SCR_CPU__CRG_DEBUG__RSTACK__IN;
	wire       SCR_CPU__CRG_TS__RSTACK__IN;
	wire       GLCM_CPUCORE__PLL_CPU__CLK__IN1;
	wire       GLCM_CPUCORE__CLKDIV_CPUCORE__CLK__OUT;
	wire       CLKDIV_CPUCORE__CRG_CPUCORE__CLK__OUT;
	wire       CRG_CPUCORE__CLKDIV_BUS__CLK__IN;
	wire       CLKDIV_BUS__CRG_BUS__CLK__OUT;
	wire       CRG_BUS__SCR_CPU__PCLK;
	wire       CRG_BUS__SCR_CPU__PRESETn;
	wire       CLKDIV_MEM__CRG_MEM__CLK__OUT;
	wire       GLCM_DEBUG__CLKDIV_DEBUG__CLK__OUT;
	wire       CLKDIV_DEBUG__CRG_DEBUG__CLK__OUT;
	wire       GLCM_TS__CLKDIV_TS__CLK__OUT;
	wire       CLKDIV_TS__CRG_TS__CLK__OUT;

	assign CLK__CBUS = CRG_BUS__SCR_CPU__PCLK;
	assign RSTN__CBUS = CRG_BUS__SCR_CPU__PRESETn;

	wire [  9:  3] __unconnected__SCR_CPU__SCR__CLKDIS0_msb_9_lsb_3;
	scr_cpu_wrap
	SCR_CPU (
		 .SFR__SYSPORT_AWUSER              (SFR__SYSPORT_AWUSER)
		,.SFR__SYSPORT_ARUSER              (SFR__SYSPORT_ARUSER)
		,.SFR__SUPPRESS_STRB_ON_CORRUPT    (SFR__SUPPRESS_STRB_ON_CORRUPT)
		,.SFR__RTC_TOGGLE                  (SFR__RTC_TOGGLE)
		,.SFR__RESETCTRL_HARTISINRESET_1   (SFR__RESETCTRL_HARTISINRESET_1)
		,.SFR__RESETCTRL_HARTISINRESET_0   (SFR__RESETCTRL_HARTISINRESET_0)
		,.SFR__PSD_TEST_CLOCK_ENABLE       (SFR__PSD_TEST_CLOCK_ENABLE)
		,.SFR__MEMPORT_AWUSER              (SFR__MEMPORT_AWUSER)
		,.SFR__MEMPORT_ARUSER              (SFR__MEMPORT_ARUSER)
		,.SFR__DEBUG_SYSTEMJTAG_VERSION    (SFR__DEBUG_SYSTEMJTAG_VERSION)
		,.SFR__DEBUG_SYSTEMJTAG_PART_NUMBER(SFR__DEBUG_SYSTEMJTAG_PART_NUMBER)
		,.SFR__DEBUG_SYSTEMJTAG_MFR_ID     (SFR__DEBUG_SYSTEMJTAG_MFR_ID)
		,.SFR__DEBUG_DMACTIVEACK           (SFR__DEBUG_DMACTIVEACK)
		,.SCR__RSTREQN0                    ({
			SCR_CPU__CRG_TS__SFR__SCU__RSTREQN
			,SCR_CPU__CRG_DEBUG__SFR__SCU__RSTREQN
			,SCR_CPU__CRG_MEM__SFR__SCU__RSTREQN
			,SCR_CPU__CRG_BUS__SFR__SCU__RSTREQN
			,SCR_CPU__CRG_CPUCORE__SFR__SCU__RSTREQN})
		,.SCR__RSTCYCLE4                   (SCR_CPU__CRG_TS__SCR__RSTCYCLE4)
		,.SCR__RSTCYCLE3                   (SCR_CPU__CRG_DEBUG__SCR__RSTCYCLE3)
		,.SCR__RSTCYCLE2                   (SCR_CPU__CRG_MEM__SCR__RSTCYCLE2)
		,.SCR__RSTCYCLE1                   (SCR_CPU__CRG_BUS__SCR__RSTCYCLE1)
		,.SCR__RSTCYCLE0                   (SCR_CPU__CRG_CPUCORE__SCR__RSTCYCLE0)
		,.SCR__PLL_CPU_VCO_BOOST           (SCR_CPU__PLL_CPU__SCR__PLL_CPU_VCO_BOOST)
		,.SCR__PLL_CPU_S                   (SCR_CPU__PLL_CPU__SCR__PLL_CPU_S)
		,.SCR__PLL_CPU_RSEL                (SCR_CPU__PLL_CPU__SCR__PLL_CPU_RSEL)
		,.SCR__PLL_CPU_RESETB              (SCR_CPU__PLL_CPU__SCR__PLL_CPU_RESETB)
		,.SCR__PLL_CPU_PBIAS_CTRL_EN       (SCR_CPU__PLL_CPU__SCR__PLL_CPU_PBIAS_CTRL_EN)
		,.SCR__PLL_CPU_PBIAS_CTRL          (SCR_CPU__PLL_CPU__SCR__PLL_CPU_PBIAS_CTRL)
		,.SCR__PLL_CPU_P                   (SCR_CPU__PLL_CPU__SCR__PLL_CPU_P)
		,.SCR__PLL_CPU_M                   (SCR_CPU__PLL_CPU__SCR__PLL_CPU_M)
		,.SCR__PLL_CPU_LRD_EN              (SCR_CPU__PLL_CPU__SCR__PLL_CPU_LRD_EN)
		,.SCR__PLL_CPU_LOCK_EN             (SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK_EN)
		,.SCR__PLL_CPU_LOCK_CON_REV        (SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK_CON_REV)
		,.SCR__PLL_CPU_LOCK_CON_OUT        (SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK_CON_OUT)
		,.SCR__PLL_CPU_LOCK_CON_IN         (SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK_CON_IN)
		,.SCR__PLL_CPU_LOCK_CON_DLY        (SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK_CON_DLY)
		,.SCR__PLL_CPU_ICP                 (SCR_CPU__PLL_CPU__SCR__PLL_CPU_ICP)
		,.SCR__PLL_CPU_FSEL                (SCR_CPU__PLL_CPU__SCR__PLL_CPU_FSEL)
		,.SCR__PLL_CPU_FOUT_MASK           (SCR_CPU__PLL_CPU__SCR__PLL_CPU_FOUT_MASK)
		,.SCR__PLL_CPU_FEED_EN             (SCR_CPU__PLL_CPU__SCR__PLL_CPU_FEED_EN)
		,.SCR__PLL_CPU_EXTAFC              (SCR_CPU__PLL_CPU__SCR__PLL_CPU_EXTAFC)
		,.SCR__PLL_CPU_BYPASS              (SCR_CPU__PLL_CPU__SCR__PLL_CPU_BYPASS)
		,.SCR__PLL_CPU_AFC_ENB             (SCR_CPU__PLL_CPU__SCR__PLL_CPU_AFC_ENB)
		,.SCR__PLL_CPU_AFCINIT_SEL         (SCR_CPU__PLL_CPU__SCR__PLL_CPU_AFCINIT_SEL)
		,.SCR__CLKSEL2                     (SCR_CPU__GLCM_TS__SCR__CLKSEL2)
		,.SCR__CLKSEL1                     (SCR_CPU__GLCM_DEBUG__SCR__CLKSEL1)
		,.SCR__CLKSEL0                     (SCR_CPU__GLCM_CPUCORE__SCR__CLKSEL0)
		,.SCR__CLKDIV2                     (SCR_CPU__CLKDIV_TS__SCR__CLKDIV2)
		,.SCR__CLKDIV1                     (SCR_CPU__CLKDIV_DEBUG__SCR__CLKDIV1)
		,.SCR__CLKDIV0                     (SCR_CPU__CLKDIV_CPUCORE__SCR__CLKDIV0)
		,.SCR__CLKDIS0                     ({
			SCR_CPU__CRG_TS__SFR__SCU__CLKDIS
			,SCR_CPU__CRG_DEBUG__SFR__SCU__CLKDIS
			,SCR_CPU__CRG_MEM__SFR__SCU__CLKDIS
			,SCR_CPU__CRG_BUS__SFR__SCU__CLKDIS
			,SCR_CPU__CRG_CPUCORE__SFR__SCU__CLKDIS})
		,.PSLVERR                          (APB3__CBUS__PSLVERR)
		,.PRDATA                           (APB3__CBUS__PRDATA)
		,.PREADY                           (APB3__CBUS__PREADY)
		,.SFR__WFI_FROM_TILE_1             (SFR__WFI_FROM_TILE_1)
		,.SFR__WFI_FROM_TILE_0             (SFR__WFI_FROM_TILE_0)
		,.SFR__HALT_FROM_TILE_1            (SFR__HALT_FROM_TILE_1)
		,.SFR__HALT_FROM_TILE_0            (SFR__HALT_FROM_TILE_0)
		,.SFR__DEBUG_NDRESET               (SFR__DEBUG_NDRESET)
		,.SFR__DEBUG_FROM_TILE_1           (SFR__DEBUG_FROM_TILE_1)
		,.SFR__DEBUG_FROM_TILE_0           (SFR__DEBUG_FROM_TILE_0)
		,.SFR__DEBUG_DMACTIVE              (SFR__DEBUG_DMACTIVE)
		,.SFR__CEASE_FROM_TILE_1           (SFR__CEASE_FROM_TILE_1)
		,.SFR__CEASE_FROM_TILE_0           (SFR__CEASE_FROM_TILE_0)
		,.SCR__VERSION3                    (32'h0)
		,.SCR__VERSION2                    (32'h0)
		,.SCR__VERSION1                    (32'h0)
		,.SCR__VERSION0                    (32'h0)
		,.SCR__PLL_CPU_LOCK                (SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK)
		,.SCR__PLL_CPU_AFC_CODE            (SCR_CPU__PLL_CPU__SCR__PLL_CPU_AFC_CODE)
		,.PWRITE                           (APB3__CBUS__PWRITE)
		,.PWDATA                           (APB3__CBUS__PWDATA)
		,.PSTRB                            (4'hf)
		,.PSEL                             (APB3__CBUS__PSEL)
		,.PRESETn                          (CRG_BUS__SCR_CPU__PRESETn)
		,.PPROT                            (3'h0)
		,.PENABLE                          (APB3__CBUS__PENABLE)
		,.PCLK                             (CRG_BUS__SCR_CPU__PCLK)
		,.PADDR                            (APB3__CBUS__PADDR[11:0])
		,.RSTACK                           ({
			SCR_CPU__CRG_TS__RSTACK__IN
			,SCR_CPU__CRG_DEBUG__RSTACK__IN
			,SCR_CPU__CRG_MEM__RSTACK__IN
			,SCR_CPU__CRG_BUS__RSTACK__IN
			,SCR_CPU__CRG_CPUCORE__RSTACK__IN})
	);

	pll1426xe_wrap
	PLL_CPU (
		 .CLK__FOUT         (GLCM_CPUCORE__PLL_CPU__CLK__IN1)
		,.TEST__PLL_OUT     (DFT__PLL_CPU_OUT)
		,.REG__AFC_CODE     (SCR_CPU__PLL_CPU__SCR__PLL_CPU_AFC_CODE)
		,.REG__LOCK         (SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK)
		,.REG__FEED_OUT     ()
		,.CLK__FIN          (CLK__OSC)
		,.TEST__SCAN        (DFT__SCAN_MODE)
		,.TEST__PLL         (DFT__PLL_CPU)
		,.TEST__PLL_IN      (DFT__PLL_CPU_IN)
		,.REG__P            (SCR_CPU__PLL_CPU__SCR__PLL_CPU_P)
		,.REG__M            (SCR_CPU__PLL_CPU__SCR__PLL_CPU_M)
		,.REG__S            (SCR_CPU__PLL_CPU__SCR__PLL_CPU_S)
		,.REG__ICP          (SCR_CPU__PLL_CPU__SCR__PLL_CPU_ICP)
		,.REG__RSEL         (SCR_CPU__PLL_CPU__SCR__PLL_CPU_RSEL)
		,.REG__EXTAFC       (SCR_CPU__PLL_CPU__SCR__PLL_CPU_EXTAFC)
		,.REG__LOCK_CON_DLY (SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK_CON_DLY)
		,.REG__LOCK_CON_REV (SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK_CON_REV)
		,.REG__LOCK_CON_IN  (SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK_CON_IN)
		,.REG__LOCK_CON_OUT (SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK_CON_OUT)
		,.REG__RESETB       (SCR_CPU__PLL_CPU__SCR__PLL_CPU_RESETB)
		,.REG__BYPASS       (SCR_CPU__PLL_CPU__SCR__PLL_CPU_BYPASS)
		,.REG__AFC_ENB      (SCR_CPU__PLL_CPU__SCR__PLL_CPU_AFC_ENB)
		,.REG__FSEL         (SCR_CPU__PLL_CPU__SCR__PLL_CPU_FSEL)
		,.REG__FEED_EN      (SCR_CPU__PLL_CPU__SCR__PLL_CPU_FEED_EN)
		,.REG__LOCK_EN      (SCR_CPU__PLL_CPU__SCR__PLL_CPU_LOCK_EN)
		,.REG__AFCINIT_SEL  (SCR_CPU__PLL_CPU__SCR__PLL_CPU_AFCINIT_SEL)
		,.REG__LRD_EN       (SCR_CPU__PLL_CPU__SCR__PLL_CPU_LRD_EN)
		,.REG__FOUT_MASK    (SCR_CPU__PLL_CPU__SCR__PLL_CPU_FOUT_MASK)
		,.REG__VCO_BOOST    (SCR_CPU__PLL_CPU__SCR__PLL_CPU_VCO_BOOST)
		,.REG__PBIAS_CTRL_EN(SCR_CPU__PLL_CPU__SCR__PLL_CPU_PBIAS_CTRL_EN)
		,.REG__PBIAS_CTRL   (SCR_CPU__PLL_CPU__SCR__PLL_CPU_PBIAS_CTRL)
	);

	SemiGLCM
	GLCM_CPUCORE (
		 .RSTN    (RSTN__SUBSYSTEM)
		,.CLK__IN0(CLK__SUBSYSTEM)
		,.CLK__IN1(GLCM_CPUCORE__PLL_CPU__CLK__IN1)
		,.CLKSEL  (SCR_CPU__GLCM_CPUCORE__SCR__CLKSEL0)
		,.CLK__OUT(GLCM_CPUCORE__CLKDIV_CPUCORE__CLK__OUT)
	);

	ClockDivider #(
		 .LOG2N(8)
	) CLKDIV_CPUCORE (
		 .CLK__SFR      (CRG_BUS__SCR_CPU__PCLK)
		,.RSTN__SFR     (CRG_BUS__SCR_CPU__PRESETn)
		,.SFR__DIV      (SCR_CPU__CLKDIV_CPUCORE__SCR__CLKDIV0)
		,.CLK__IN       (GLCM_CPUCORE__CLKDIV_CPUCORE__CLK__OUT)
		,.CLK__OUT      (CLKDIV_CPUCORE__CRG_CPUCORE__CLK__OUT)
		,.DFT__SCAN_MODE(DFT__SCAN_MODE)
		,.DFT__SCAN_RSTN(DFT__SCAN_RSTN)
		,.DFT__SCAN_DIV (8'h0)
	);

	wire           __opened__CRG_CPUCORE__RSTN__OUT_bit_0;
	SemiCRG #(
		 .LOG2N(8)
		,.NUM_RESET(3)
		,.NUM_CLK(3)
	) CRG_CPUCORE (
		 .RSTN__SCU         (RSTN__SUBSYSTEM)
		,.CLK__SFR          (CRG_BUS__SCR_CPU__PCLK)
		,.SFR__SCU__CLKDIS  ({SCR_CPU__CRG_CPUCORE__SFR__SCU__CLKDIS[2:1],1'b0})
		,.SFR__SCU__RSTREQN (SCR_CPU__CRG_CPUCORE__SFR__SCU__RSTREQN)
		,.SFR__SCU__RSTCYCLE(SCR_CPU__CRG_CPUCORE__SCR__RSTCYCLE0)
		,.CLK__IN           (CLKDIV_CPUCORE__CRG_CPUCORE__CLK__OUT)
		,.CLK__OUT          ({
			CLK__CPUCORE1
			,CLK__CPUCORE0
			,CRG_CPUCORE__CLKDIV_BUS__CLK__IN})
		,.RSTN__OUT         ({
			RSTN__CPUCORE1
			,RSTN__CPUCORE0
			,__opened__CRG_CPUCORE__RSTN__OUT_bit_0})
		,.RSTACK__IN        (SCR_CPU__CRG_CPUCORE__RSTACK__IN)
		,.DFT__BIST_MODE    (DFT__BIST_MODE)
		,.DFT__BIST_CLK     ()
		,.DFT__SCAN_MODE    (DFT__SCAN_MODE)
		,.DFT__SCAN_RSTN    (DFT__SCAN_RSTN)
	);

	ClockDivider #(
		 .LOG2N(8)
	) CLKDIV_BUS (
		 .CLK__SFR      (CRG_BUS__SCR_CPU__PCLK)
		,.RSTN__SFR     (CRG_BUS__SCR_CPU__PRESETn)
		,.SFR__DIV      (8'h2)
		,.CLK__IN       (CRG_CPUCORE__CLKDIV_BUS__CLK__IN)
		,.CLK__OUT      (CLKDIV_BUS__CRG_BUS__CLK__OUT)
		,.DFT__SCAN_MODE(DFT__SCAN_MODE)
		,.DFT__SCAN_RSTN(DFT__SCAN_RSTN)
		,.DFT__SCAN_DIV (8'h0)
	);

	wire [  6:  1] CRG_BUS__SFR__SCU__CLKDIS_msb_6_lsb_1;
	SemiCRG #(
		 .LOG2N(8)
		,.NUM_RESET(7)
		,.NUM_CLK(7)
	) CRG_BUS (
		 .RSTN__SCU         (RSTN__SUBSYSTEM)
		,.CLK__SFR          (CRG_BUS__SCR_CPU__PCLK)
		,.SFR__SCU__CLKDIS  ({SCR_CPU__CRG_BUS__SFR__SCU__CLKDIS[6:1]
			,1'h0})
		,.SFR__SCU__RSTREQN (SCR_CPU__CRG_BUS__SFR__SCU__RSTREQN)
		,.SFR__SCU__RSTCYCLE(SCR_CPU__CRG_BUS__SCR__RSTCYCLE1)
		,.CLK__IN           (CLKDIV_BUS__CRG_BUS__CLK__OUT)
		,.CLK__OUT          ({
			CLK__FBUS_RX
			,CLK__FBUS
			,CLK__DBUS_TX
			,CLK__DBUS
			,CLK__CBUS_TX
			,CLK__CPU
			,CRG_BUS__SCR_CPU__PCLK})
		,.RSTN__OUT         ({
			RSTN__FBUS_RX
			,RSTN__FBUS
			,RSTN__DBUS_TX
			,RSTN__DBUS
			,RSTN__CBUS_TX
			,RSTN__CPU
			,CRG_BUS__SCR_CPU__PRESETn})
		,.RSTACK__IN        (SCR_CPU__CRG_BUS__RSTACK__IN)
		,.DFT__BIST_MODE    (DFT__BIST_MODE)
		,.DFT__BIST_CLK     ()
		,.DFT__SCAN_MODE    (DFT__SCAN_MODE)
		,.DFT__SCAN_RSTN    (DFT__SCAN_RSTN)
	);

	ClockDivider #(
		 .LOG2N(8)
	) CLKDIV_MEM (
		 .CLK__SFR      (CRG_BUS__SCR_CPU__PCLK)
		,.RSTN__SFR     (CRG_BUS__SCR_CPU__PRESETn)
		,.SFR__DIV      (8'h4)
		,.CLK__IN       (CRG_CPUCORE__CLKDIV_BUS__CLK__IN)
		,.CLK__OUT      (CLKDIV_MEM__CRG_MEM__CLK__OUT)
		,.DFT__SCAN_MODE(DFT__SCAN_MODE)
		,.DFT__SCAN_RSTN(DFT__SCAN_RSTN)
		,.DFT__SCAN_DIV (8'h0)
	);

	SemiCRG #(
		 .LOG2N(8)
		,.NUM_RESET(2)
		,.NUM_CLK(2)
	) CRG_MEM (
		 .RSTN__SCU         (RSTN__SUBSYSTEM)
		,.CLK__SFR          (CRG_BUS__SCR_CPU__PCLK)
		,.SFR__SCU__CLKDIS  (SCR_CPU__CRG_MEM__SFR__SCU__CLKDIS)
		,.SFR__SCU__RSTREQN (SCR_CPU__CRG_MEM__SFR__SCU__RSTREQN)
		,.SFR__SCU__RSTCYCLE(SCR_CPU__CRG_MEM__SCR__RSTCYCLE2)
		,.CLK__IN           (CLKDIV_MEM__CRG_MEM__CLK__OUT)
		,.CLK__OUT          ({
			CLK__SRAM
			,CLK__ROM})
		,.RSTN__OUT         ({
			RSTN__SRAM
			,RSTN__ROM})
		,.RSTACK__IN        (SCR_CPU__CRG_MEM__RSTACK__IN)
		,.DFT__BIST_MODE    (DFT__BIST_MODE)
		,.DFT__BIST_CLK     ()
		,.DFT__SCAN_MODE    (DFT__SCAN_MODE)
		,.DFT__SCAN_RSTN    (DFT__SCAN_RSTN)
	);

	SemiGLCM
	GLCM_DEBUG (
		 .RSTN    (RSTN__SUBSYSTEM)
		,.CLK__IN0(CLK__SUBSYSTEM)
		,.CLK__IN1(GLCM_CPUCORE__PLL_CPU__CLK__IN1)
		,.CLKSEL  (SCR_CPU__GLCM_DEBUG__SCR__CLKSEL1)
		,.CLK__OUT(GLCM_DEBUG__CLKDIV_DEBUG__CLK__OUT)
	);

	ClockDivider #(
		 .LOG2N(8)
	) CLKDIV_DEBUG (
		 .CLK__SFR      (CRG_BUS__SCR_CPU__PCLK)
		,.RSTN__SFR     (CRG_BUS__SCR_CPU__PRESETn)
		,.SFR__DIV      (SCR_CPU__CLKDIV_DEBUG__SCR__CLKDIV1)
		,.CLK__IN       (GLCM_DEBUG__CLKDIV_DEBUG__CLK__OUT)
		,.CLK__OUT      (CLKDIV_DEBUG__CRG_DEBUG__CLK__OUT)
		,.DFT__SCAN_MODE(DFT__SCAN_MODE)
		,.DFT__SCAN_RSTN(DFT__SCAN_RSTN)
		,.DFT__SCAN_DIV (8'h0)
	);

	SemiCRG #(
		 .LOG2N(8)
		,.NUM_RESET(4)
		,.NUM_CLK(4)
	) CRG_DEBUG (
		 .RSTN__SCU         (RSTN__SUBSYSTEM)
		,.CLK__SFR          (CRG_BUS__SCR_CPU__PCLK)
		,.SFR__SCU__CLKDIS  (SCR_CPU__CRG_DEBUG__SFR__SCU__CLKDIS)
		,.SFR__SCU__RSTREQN (SCR_CPU__CRG_DEBUG__SFR__SCU__RSTREQN)
		,.SFR__SCU__RSTCYCLE(SCR_CPU__CRG_DEBUG__SCR__RSTCYCLE3)
		,.CLK__IN           (CLKDIV_DEBUG__CRG_DEBUG__CLK__OUT)
		,.CLK__OUT          ({
			CLK__TRACE_1
			,CLK__TRACE_0
			,CLK__TRACE_COM
			,CLK__DEBUG})
		,.RSTN__OUT         ({
			RSTN__TRACE_1
			,RSTN__TRACE_0
			,RSTN__TRACE_COM
			,RSTN__DEBUG})
		,.RSTACK__IN        (SCR_CPU__CRG_DEBUG__RSTACK__IN)
		,.DFT__BIST_MODE    (DFT__BIST_MODE)
		,.DFT__BIST_CLK     ()
		,.DFT__SCAN_MODE    (DFT__SCAN_MODE)
		,.DFT__SCAN_RSTN    (DFT__SCAN_RSTN)
	);

	SemiGLCM
	GLCM_TS (
		 .RSTN    (RSTN__SUBSYSTEM)
		,.CLK__IN0(CLK__SUBSYSTEM)
		,.CLK__IN1(GLCM_CPUCORE__PLL_CPU__CLK__IN1)
		,.CLKSEL  (SCR_CPU__GLCM_TS__SCR__CLKSEL2)
		,.CLK__OUT(GLCM_TS__CLKDIV_TS__CLK__OUT)
	);

	ClockDivider #(
		 .LOG2N(8)
	) CLKDIV_TS (
		 .CLK__SFR      (CRG_BUS__SCR_CPU__PCLK)
		,.RSTN__SFR     (CRG_BUS__SCR_CPU__PRESETn)
		,.SFR__DIV      (SCR_CPU__CLKDIV_TS__SCR__CLKDIV2)
		,.CLK__IN       (GLCM_TS__CLKDIV_TS__CLK__OUT)
		,.CLK__OUT      (CLKDIV_TS__CRG_TS__CLK__OUT)
		,.DFT__SCAN_MODE(DFT__SCAN_MODE)
		,.DFT__SCAN_RSTN(DFT__SCAN_RSTN)
		,.DFT__SCAN_DIV (8'h0)
	);

	SemiCRG #(
		 .LOG2N(8)
		,.NUM_RESET(1)
		,.NUM_CLK(1)
	) CRG_TS (
		 .RSTN__SCU         (RSTN__SUBSYSTEM)
		,.CLK__SFR          (CRG_BUS__SCR_CPU__PCLK)
		,.SFR__SCU__CLKDIS  (SCR_CPU__CRG_TS__SFR__SCU__CLKDIS)
		,.SFR__SCU__RSTREQN (SCR_CPU__CRG_TS__SFR__SCU__RSTREQN)
		,.SFR__SCU__RSTCYCLE(SCR_CPU__CRG_TS__SCR__RSTCYCLE4)
		,.CLK__IN           (CLKDIV_TS__CRG_TS__CLK__OUT)
		,.CLK__OUT          (CLK__TS)
		,.RSTN__OUT         (RSTN__TS)
		,.RSTACK__IN        (SCR_CPU__CRG_TS__RSTACK__IN)
		,.DFT__BIST_MODE    (DFT__BIST_MODE)
		,.DFT__BIST_CLK     ()
		,.DFT__SCAN_MODE    (DFT__SCAN_MODE)
		,.DFT__SCAN_RSTN    (DFT__SCAN_RSTN)
	);

endmodule 
