//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_AXI4UserYanker_2_assert(
  input        clock,
  input        reset,
  input        auto_out_b_valid,
  input  [3:0] auto_out_b_bits_id,
  input        auto_out_r_valid,
  input  [3:0] auto_out_r_bits_id,
  input        QueueCompatibility_15_io_deq_valid,
  input        QueueCompatibility_31_io_deq_valid,
  input        QueueCompatibility_14_io_deq_valid,
  input        QueueCompatibility_30_io_deq_valid,
  input        QueueCompatibility_13_io_deq_valid,
  input        QueueCompatibility_29_io_deq_valid,
  input        QueueCompatibility_12_io_deq_valid,
  input        QueueCompatibility_28_io_deq_valid,
  input        QueueCompatibility_11_io_deq_valid,
  input        QueueCompatibility_27_io_deq_valid,
  input        QueueCompatibility_10_io_deq_valid,
  input        QueueCompatibility_26_io_deq_valid,
  input        QueueCompatibility_9_io_deq_valid,
  input        QueueCompatibility_25_io_deq_valid,
  input        QueueCompatibility_8_io_deq_valid,
  input        QueueCompatibility_24_io_deq_valid,
  input        QueueCompatibility_7_io_deq_valid,
  input        QueueCompatibility_23_io_deq_valid,
  input        QueueCompatibility_6_io_deq_valid,
  input        QueueCompatibility_22_io_deq_valid,
  input        QueueCompatibility_5_io_deq_valid,
  input        QueueCompatibility_21_io_deq_valid,
  input        QueueCompatibility_4_io_deq_valid,
  input        QueueCompatibility_20_io_deq_valid,
  input        QueueCompatibility_3_io_deq_valid,
  input        QueueCompatibility_19_io_deq_valid,
  input        QueueCompatibility_2_io_deq_valid,
  input        QueueCompatibility_18_io_deq_valid,
  input        QueueCompatibility_1_io_deq_valid,
  input        QueueCompatibility_io_deq_valid,
  input        QueueCompatibility_17_io_deq_valid,
  input        QueueCompatibility_16_io_deq_valid
);
  wire  _T_7; // @[UserYanker.scala 55:15]
  wire  _T_5_0; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _T_5_1; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_17; // @[UserYanker.scala 55:28]
  wire  _T_5_2; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_18; // @[UserYanker.scala 55:28]
  wire  _T_5_3; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_19; // @[UserYanker.scala 55:28]
  wire  _T_5_4; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_20; // @[UserYanker.scala 55:28]
  wire  _T_5_5; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_21; // @[UserYanker.scala 55:28]
  wire  _T_5_6; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_22; // @[UserYanker.scala 55:28]
  wire  _T_5_7; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_23; // @[UserYanker.scala 55:28]
  wire  _T_5_8; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_24; // @[UserYanker.scala 55:28]
  wire  _T_5_9; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_25; // @[UserYanker.scala 55:28]
  wire  _T_5_10; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_26; // @[UserYanker.scala 55:28]
  wire  _T_5_11; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_27; // @[UserYanker.scala 55:28]
  wire  _T_5_12; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_28; // @[UserYanker.scala 55:28]
  wire  _T_5_13; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_29; // @[UserYanker.scala 55:28]
  wire  _T_5_14; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_30; // @[UserYanker.scala 55:28]
  wire  _T_5_15; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  wire  _GEN_31; // @[UserYanker.scala 55:28]
  wire  _T_8; // @[UserYanker.scala 55:28]
  wire  _T_10; // @[UserYanker.scala 55:14]
  wire  _T_11; // @[UserYanker.scala 55:14]
  wire  _T_135; // @[UserYanker.scala 76:15]
  wire  _T_133_0; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _T_133_1; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_81; // @[UserYanker.scala 76:28]
  wire  _T_133_2; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_82; // @[UserYanker.scala 76:28]
  wire  _T_133_3; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_83; // @[UserYanker.scala 76:28]
  wire  _T_133_4; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_84; // @[UserYanker.scala 76:28]
  wire  _T_133_5; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_85; // @[UserYanker.scala 76:28]
  wire  _T_133_6; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_86; // @[UserYanker.scala 76:28]
  wire  _T_133_7; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_87; // @[UserYanker.scala 76:28]
  wire  _T_133_8; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_88; // @[UserYanker.scala 76:28]
  wire  _T_133_9; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_89; // @[UserYanker.scala 76:28]
  wire  _T_133_10; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_90; // @[UserYanker.scala 76:28]
  wire  _T_133_11; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_91; // @[UserYanker.scala 76:28]
  wire  _T_133_12; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_92; // @[UserYanker.scala 76:28]
  wire  _T_133_13; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_93; // @[UserYanker.scala 76:28]
  wire  _T_133_14; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_94; // @[UserYanker.scala 76:28]
  wire  _T_133_15; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  wire  _GEN_95; // @[UserYanker.scala 76:28]
  wire  _T_136; // @[UserYanker.scala 76:28]
  wire  _T_138; // @[UserYanker.scala 76:14]
  wire  _T_139; // @[UserYanker.scala 76:14]
  assign _T_7 = ~auto_out_r_valid; // @[UserYanker.scala 55:15]
  assign _T_5_0 = QueueCompatibility_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _T_5_1 = QueueCompatibility_1_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_17 = 4'h1 == auto_out_r_bits_id ? _T_5_1 : _T_5_0; // @[UserYanker.scala 55:28]
  assign _T_5_2 = QueueCompatibility_2_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_18 = 4'h2 == auto_out_r_bits_id ? _T_5_2 : _GEN_17; // @[UserYanker.scala 55:28]
  assign _T_5_3 = QueueCompatibility_3_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_19 = 4'h3 == auto_out_r_bits_id ? _T_5_3 : _GEN_18; // @[UserYanker.scala 55:28]
  assign _T_5_4 = QueueCompatibility_4_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_20 = 4'h4 == auto_out_r_bits_id ? _T_5_4 : _GEN_19; // @[UserYanker.scala 55:28]
  assign _T_5_5 = QueueCompatibility_5_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_21 = 4'h5 == auto_out_r_bits_id ? _T_5_5 : _GEN_20; // @[UserYanker.scala 55:28]
  assign _T_5_6 = QueueCompatibility_6_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_22 = 4'h6 == auto_out_r_bits_id ? _T_5_6 : _GEN_21; // @[UserYanker.scala 55:28]
  assign _T_5_7 = QueueCompatibility_7_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_23 = 4'h7 == auto_out_r_bits_id ? _T_5_7 : _GEN_22; // @[UserYanker.scala 55:28]
  assign _T_5_8 = QueueCompatibility_8_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_24 = 4'h8 == auto_out_r_bits_id ? _T_5_8 : _GEN_23; // @[UserYanker.scala 55:28]
  assign _T_5_9 = QueueCompatibility_9_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_25 = 4'h9 == auto_out_r_bits_id ? _T_5_9 : _GEN_24; // @[UserYanker.scala 55:28]
  assign _T_5_10 = QueueCompatibility_10_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_26 = 4'ha == auto_out_r_bits_id ? _T_5_10 : _GEN_25; // @[UserYanker.scala 55:28]
  assign _T_5_11 = QueueCompatibility_11_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_27 = 4'hb == auto_out_r_bits_id ? _T_5_11 : _GEN_26; // @[UserYanker.scala 55:28]
  assign _T_5_12 = QueueCompatibility_12_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_28 = 4'hc == auto_out_r_bits_id ? _T_5_12 : _GEN_27; // @[UserYanker.scala 55:28]
  assign _T_5_13 = QueueCompatibility_13_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_29 = 4'hd == auto_out_r_bits_id ? _T_5_13 : _GEN_28; // @[UserYanker.scala 55:28]
  assign _T_5_14 = QueueCompatibility_14_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_30 = 4'he == auto_out_r_bits_id ? _T_5_14 : _GEN_29; // @[UserYanker.scala 55:28]
  assign _T_5_15 = QueueCompatibility_15_io_deq_valid; // @[UserYanker.scala 53:24 UserYanker.scala 53:24]
  assign _GEN_31 = 4'hf == auto_out_r_bits_id ? _T_5_15 : _GEN_30; // @[UserYanker.scala 55:28]
  assign _T_8 = _T_7 | _GEN_31; // @[UserYanker.scala 55:28]
  assign _T_10 = _T_8 | reset; // @[UserYanker.scala 55:14]
  assign _T_11 = ~_T_10; // @[UserYanker.scala 55:14]
  assign _T_135 = ~auto_out_b_valid; // @[UserYanker.scala 76:15]
  assign _T_133_0 = QueueCompatibility_16_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _T_133_1 = QueueCompatibility_17_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_81 = 4'h1 == auto_out_b_bits_id ? _T_133_1 : _T_133_0; // @[UserYanker.scala 76:28]
  assign _T_133_2 = QueueCompatibility_18_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_82 = 4'h2 == auto_out_b_bits_id ? _T_133_2 : _GEN_81; // @[UserYanker.scala 76:28]
  assign _T_133_3 = QueueCompatibility_19_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_83 = 4'h3 == auto_out_b_bits_id ? _T_133_3 : _GEN_82; // @[UserYanker.scala 76:28]
  assign _T_133_4 = QueueCompatibility_20_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_84 = 4'h4 == auto_out_b_bits_id ? _T_133_4 : _GEN_83; // @[UserYanker.scala 76:28]
  assign _T_133_5 = QueueCompatibility_21_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_85 = 4'h5 == auto_out_b_bits_id ? _T_133_5 : _GEN_84; // @[UserYanker.scala 76:28]
  assign _T_133_6 = QueueCompatibility_22_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_86 = 4'h6 == auto_out_b_bits_id ? _T_133_6 : _GEN_85; // @[UserYanker.scala 76:28]
  assign _T_133_7 = QueueCompatibility_23_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_87 = 4'h7 == auto_out_b_bits_id ? _T_133_7 : _GEN_86; // @[UserYanker.scala 76:28]
  assign _T_133_8 = QueueCompatibility_24_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_88 = 4'h8 == auto_out_b_bits_id ? _T_133_8 : _GEN_87; // @[UserYanker.scala 76:28]
  assign _T_133_9 = QueueCompatibility_25_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_89 = 4'h9 == auto_out_b_bits_id ? _T_133_9 : _GEN_88; // @[UserYanker.scala 76:28]
  assign _T_133_10 = QueueCompatibility_26_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_90 = 4'ha == auto_out_b_bits_id ? _T_133_10 : _GEN_89; // @[UserYanker.scala 76:28]
  assign _T_133_11 = QueueCompatibility_27_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_91 = 4'hb == auto_out_b_bits_id ? _T_133_11 : _GEN_90; // @[UserYanker.scala 76:28]
  assign _T_133_12 = QueueCompatibility_28_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_92 = 4'hc == auto_out_b_bits_id ? _T_133_12 : _GEN_91; // @[UserYanker.scala 76:28]
  assign _T_133_13 = QueueCompatibility_29_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_93 = 4'hd == auto_out_b_bits_id ? _T_133_13 : _GEN_92; // @[UserYanker.scala 76:28]
  assign _T_133_14 = QueueCompatibility_30_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_94 = 4'he == auto_out_b_bits_id ? _T_133_14 : _GEN_93; // @[UserYanker.scala 76:28]
  assign _T_133_15 = QueueCompatibility_31_io_deq_valid; // @[UserYanker.scala 74:24 UserYanker.scala 74:24]
  assign _GEN_95 = 4'hf == auto_out_b_bits_id ? _T_133_15 : _GEN_94; // @[UserYanker.scala 76:28]
  assign _T_136 = _T_135 | _GEN_95; // @[UserYanker.scala 76:28]
  assign _T_138 = _T_136 | reset; // @[UserYanker.scala 76:14]
  assign _T_139 = ~_T_138; // @[UserYanker.scala 76:14]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_11) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at UserYanker.scala:55 assert (!out.r.valid || r_valid) // Q must be ready faster than the response\n"); // @[UserYanker.scala 55:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_11) begin
          $fatal; // @[UserYanker.scala 55:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_139) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at UserYanker.scala:76 assert (!out.b.valid || b_valid) // Q must be ready faster than the response\n"); // @[UserYanker.scala 76:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_139) begin
          $fatal; // @[UserYanker.scala 76:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
