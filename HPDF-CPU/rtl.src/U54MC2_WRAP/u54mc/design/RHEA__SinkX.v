//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__SinkX(
  input         rf_reset,
  input         clock,
  input         reset,
  input         io_req_ready,
  output        io_req_valid,
  output [17:0] io_req_bits_tag,
  output [9:0]  io_req_bits_set,
  output        io_x_ready,
  input         io_x_valid,
  input  [35:0] io_x_bits_address
);
  wire  x_rf_reset; // @[Decoupled.scala 296:21]
  wire  x_clock; // @[Decoupled.scala 296:21]
  wire  x_reset; // @[Decoupled.scala 296:21]
  wire  x_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  x_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [35:0] x_io_enq_bits_address; // @[Decoupled.scala 296:21]
  wire  x_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  x_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [35:0] x_io_deq_bits_address; // @[Decoupled.scala 296:21]
  wire  offset_lo_lo_lo_lo_lo = x_io_deq_bits_address[0]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_lo_lo_hi = x_io_deq_bits_address[1]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_lo_hi_lo = x_io_deq_bits_address[2]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_lo_hi_hi = x_io_deq_bits_address[3]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_hi_lo_lo = x_io_deq_bits_address[4]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_hi_lo_hi = x_io_deq_bits_address[5]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_hi_hi_lo = x_io_deq_bits_address[8]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_hi_hi_hi = x_io_deq_bits_address[9]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_lo_lo_lo = x_io_deq_bits_address[10]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_lo_lo_hi = x_io_deq_bits_address[11]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_lo_hi_lo = x_io_deq_bits_address[12]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_lo_hi_hi = x_io_deq_bits_address[13]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_hi_lo_lo = x_io_deq_bits_address[14]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_hi_lo_hi = x_io_deq_bits_address[15]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_hi_hi_lo = x_io_deq_bits_address[16]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_hi_hi_hi_lo = x_io_deq_bits_address[17]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_hi_hi_hi_hi = x_io_deq_bits_address[18]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_lo_lo_lo = x_io_deq_bits_address[19]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_lo_lo_hi = x_io_deq_bits_address[20]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_lo_hi_lo = x_io_deq_bits_address[21]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_lo_hi_hi = x_io_deq_bits_address[22]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_hi_lo_lo = x_io_deq_bits_address[23]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_hi_lo_hi = x_io_deq_bits_address[24]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_hi_hi_lo = x_io_deq_bits_address[25]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_hi_hi_hi = x_io_deq_bits_address[26]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_lo_lo_lo = x_io_deq_bits_address[27]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_lo_lo_hi = x_io_deq_bits_address[28]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_lo_hi_lo = x_io_deq_bits_address[29]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_lo_hi_hi = x_io_deq_bits_address[30]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_hi_lo_lo = x_io_deq_bits_address[31]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_hi_lo_hi = x_io_deq_bits_address[32]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_hi_hi_lo = x_io_deq_bits_address[33]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_hi_hi_hi_lo = x_io_deq_bits_address[34]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_hi_hi_hi_hi = x_io_deq_bits_address[35]; // @[Parameters.scala 227:47]
  wire [7:0] offset_lo_lo = {offset_lo_lo_hi_hi_hi,offset_lo_lo_hi_hi_lo,offset_lo_lo_hi_lo_hi,offset_lo_lo_hi_lo_lo,
    offset_lo_lo_lo_hi_hi,offset_lo_lo_lo_hi_lo,offset_lo_lo_lo_lo_hi,offset_lo_lo_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [16:0] offset_lo = {offset_lo_hi_hi_hi_hi_hi,offset_lo_hi_hi_hi_hi_lo,offset_lo_hi_hi_hi_lo,offset_lo_hi_hi_lo_hi
    ,offset_lo_hi_hi_lo_lo,offset_lo_hi_lo_hi_hi,offset_lo_hi_lo_hi_lo,offset_lo_hi_lo_lo_hi,offset_lo_hi_lo_lo_lo,
    offset_lo_lo}; // @[Cat.scala 30:58]
  wire [7:0] offset_hi_lo = {offset_hi_lo_hi_hi_hi,offset_hi_lo_hi_hi_lo,offset_hi_lo_hi_lo_hi,offset_hi_lo_hi_lo_lo,
    offset_hi_lo_lo_hi_hi,offset_hi_lo_lo_hi_lo,offset_hi_lo_lo_lo_hi,offset_hi_lo_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [16:0] offset_hi = {offset_hi_hi_hi_hi_hi_hi,offset_hi_hi_hi_hi_hi_lo,offset_hi_hi_hi_hi_lo,offset_hi_hi_hi_lo_hi
    ,offset_hi_hi_hi_lo_lo,offset_hi_hi_lo_hi_hi,offset_hi_hi_lo_hi_lo,offset_hi_hi_lo_lo_hi,offset_hi_hi_lo_lo_lo,
    offset_hi_lo}; // @[Cat.scala 30:58]
  wire [33:0] offset = {offset_hi,offset_lo}; // @[Cat.scala 30:58]
  wire [27:0] set = offset[33:6]; // @[Parameters.scala 228:22]
  RHEA__Queue_126 x ( // @[Decoupled.scala 296:21]
    .rf_reset(x_rf_reset),
    .clock(x_clock),
    .reset(x_reset),
    .io_enq_ready(x_io_enq_ready),
    .io_enq_valid(x_io_enq_valid),
    .io_enq_bits_address(x_io_enq_bits_address),
    .io_deq_ready(x_io_deq_ready),
    .io_deq_valid(x_io_deq_valid),
    .io_deq_bits_address(x_io_deq_bits_address)
  );
  assign x_rf_reset = rf_reset;
  assign io_req_valid = x_io_deq_valid; // @[SinkX.scala 43:16]
  assign io_req_bits_tag = set[27:10]; // @[Parameters.scala 229:19]
  assign io_req_bits_set = set[9:0]; // @[Parameters.scala 230:28]
  assign io_x_ready = x_io_enq_ready; // @[Decoupled.scala 299:17]
  assign x_clock = clock;
  assign x_reset = reset;
  assign x_io_enq_valid = io_x_valid; // @[Decoupled.scala 297:22]
  assign x_io_enq_bits_address = io_x_bits_address; // @[Decoupled.scala 298:21]
  assign x_io_deq_ready = io_req_ready; // @[SinkX.scala 42:11]
endmodule
