//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__OptimizationBarrier_118(
  input  [53:0] io_x_ppn,
  input         io_x_d,
  input         io_x_a,
  input         io_x_g,
  input         io_x_u,
  input         io_x_x,
  input         io_x_w,
  input         io_x_r,
  input         io_x_v,
  output [53:0] io_y_ppn,
  output        io_y_d,
  output        io_y_a,
  output        io_y_g,
  output        io_y_u,
  output        io_y_x,
  output        io_y_w,
  output        io_y_r,
  output        io_y_v
);
  assign io_y_ppn = io_x_ppn; // @[package.scala 276:12]
  assign io_y_d = io_x_d; // @[package.scala 276:12]
  assign io_y_a = io_x_a; // @[package.scala 276:12]
  assign io_y_g = io_x_g; // @[package.scala 276:12]
  assign io_y_u = io_x_u; // @[package.scala 276:12]
  assign io_y_x = io_x_x; // @[package.scala 276:12]
  assign io_y_w = io_x_w; // @[package.scala 276:12]
  assign io_y_r = io_x_r; // @[package.scala 276:12]
  assign io_y_v = io_x_v; // @[package.scala 276:12]
endmodule
