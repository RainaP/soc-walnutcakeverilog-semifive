//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLCacheCork(
  input          rf_reset,
  input          clock,
  input          reset,
  output         auto_in_3_a_ready,
  input          auto_in_3_a_valid,
  input  [2:0]   auto_in_3_a_bits_opcode,
  input  [2:0]   auto_in_3_a_bits_param,
  input  [2:0]   auto_in_3_a_bits_source,
  input  [35:0]  auto_in_3_a_bits_address,
  output         auto_in_3_c_ready,
  input          auto_in_3_c_valid,
  input  [2:0]   auto_in_3_c_bits_opcode,
  input  [2:0]   auto_in_3_c_bits_param,
  input  [2:0]   auto_in_3_c_bits_source,
  input  [35:0]  auto_in_3_c_bits_address,
  input  [127:0] auto_in_3_c_bits_data,
  input          auto_in_3_d_ready,
  output         auto_in_3_d_valid,
  output [2:0]   auto_in_3_d_bits_opcode,
  output [1:0]   auto_in_3_d_bits_param,
  output [2:0]   auto_in_3_d_bits_size,
  output [2:0]   auto_in_3_d_bits_source,
  output [2:0]   auto_in_3_d_bits_sink,
  output         auto_in_3_d_bits_denied,
  output [127:0] auto_in_3_d_bits_data,
  output         auto_in_3_d_bits_corrupt,
  input          auto_in_3_e_valid,
  input  [2:0]   auto_in_3_e_bits_sink,
  output         auto_in_2_a_ready,
  input          auto_in_2_a_valid,
  input  [2:0]   auto_in_2_a_bits_opcode,
  input  [2:0]   auto_in_2_a_bits_param,
  input  [2:0]   auto_in_2_a_bits_source,
  input  [35:0]  auto_in_2_a_bits_address,
  output         auto_in_2_c_ready,
  input          auto_in_2_c_valid,
  input  [2:0]   auto_in_2_c_bits_opcode,
  input  [2:0]   auto_in_2_c_bits_param,
  input  [2:0]   auto_in_2_c_bits_source,
  input  [35:0]  auto_in_2_c_bits_address,
  input  [127:0] auto_in_2_c_bits_data,
  input          auto_in_2_d_ready,
  output         auto_in_2_d_valid,
  output [2:0]   auto_in_2_d_bits_opcode,
  output [1:0]   auto_in_2_d_bits_param,
  output [2:0]   auto_in_2_d_bits_size,
  output [2:0]   auto_in_2_d_bits_source,
  output [2:0]   auto_in_2_d_bits_sink,
  output         auto_in_2_d_bits_denied,
  output [127:0] auto_in_2_d_bits_data,
  output         auto_in_2_d_bits_corrupt,
  input          auto_in_2_e_valid,
  input  [2:0]   auto_in_2_e_bits_sink,
  output         auto_in_1_a_ready,
  input          auto_in_1_a_valid,
  input  [2:0]   auto_in_1_a_bits_opcode,
  input  [2:0]   auto_in_1_a_bits_param,
  input  [2:0]   auto_in_1_a_bits_source,
  input  [35:0]  auto_in_1_a_bits_address,
  output         auto_in_1_c_ready,
  input          auto_in_1_c_valid,
  input  [2:0]   auto_in_1_c_bits_opcode,
  input  [2:0]   auto_in_1_c_bits_param,
  input  [2:0]   auto_in_1_c_bits_source,
  input  [35:0]  auto_in_1_c_bits_address,
  input  [127:0] auto_in_1_c_bits_data,
  input          auto_in_1_d_ready,
  output         auto_in_1_d_valid,
  output [2:0]   auto_in_1_d_bits_opcode,
  output [1:0]   auto_in_1_d_bits_param,
  output [2:0]   auto_in_1_d_bits_size,
  output [2:0]   auto_in_1_d_bits_source,
  output [2:0]   auto_in_1_d_bits_sink,
  output         auto_in_1_d_bits_denied,
  output [127:0] auto_in_1_d_bits_data,
  output         auto_in_1_d_bits_corrupt,
  input          auto_in_1_e_valid,
  input  [2:0]   auto_in_1_e_bits_sink,
  output         auto_in_0_a_ready,
  input          auto_in_0_a_valid,
  input  [2:0]   auto_in_0_a_bits_opcode,
  input  [2:0]   auto_in_0_a_bits_param,
  input  [2:0]   auto_in_0_a_bits_source,
  input  [35:0]  auto_in_0_a_bits_address,
  output         auto_in_0_c_ready,
  input          auto_in_0_c_valid,
  input  [2:0]   auto_in_0_c_bits_opcode,
  input  [2:0]   auto_in_0_c_bits_param,
  input  [2:0]   auto_in_0_c_bits_source,
  input  [35:0]  auto_in_0_c_bits_address,
  input  [127:0] auto_in_0_c_bits_data,
  input          auto_in_0_d_ready,
  output         auto_in_0_d_valid,
  output [2:0]   auto_in_0_d_bits_opcode,
  output [1:0]   auto_in_0_d_bits_param,
  output [2:0]   auto_in_0_d_bits_size,
  output [2:0]   auto_in_0_d_bits_source,
  output [2:0]   auto_in_0_d_bits_sink,
  output         auto_in_0_d_bits_denied,
  output [127:0] auto_in_0_d_bits_data,
  output         auto_in_0_d_bits_corrupt,
  input          auto_in_0_e_valid,
  input  [2:0]   auto_in_0_e_bits_sink,
  input          auto_out_3_a_ready,
  output         auto_out_3_a_valid,
  output [2:0]   auto_out_3_a_bits_opcode,
  output [2:0]   auto_out_3_a_bits_param,
  output [2:0]   auto_out_3_a_bits_size,
  output [3:0]   auto_out_3_a_bits_source,
  output [35:0]  auto_out_3_a_bits_address,
  output         auto_out_3_a_bits_user_amba_prot_bufferable,
  output         auto_out_3_a_bits_user_amba_prot_modifiable,
  output         auto_out_3_a_bits_user_amba_prot_readalloc,
  output         auto_out_3_a_bits_user_amba_prot_writealloc,
  output         auto_out_3_a_bits_user_amba_prot_privileged,
  output         auto_out_3_a_bits_user_amba_prot_secure,
  output [15:0]  auto_out_3_a_bits_mask,
  output [127:0] auto_out_3_a_bits_data,
  output         auto_out_3_d_ready,
  input          auto_out_3_d_valid,
  input  [2:0]   auto_out_3_d_bits_opcode,
  input  [2:0]   auto_out_3_d_bits_size,
  input  [3:0]   auto_out_3_d_bits_source,
  input          auto_out_3_d_bits_denied,
  input  [127:0] auto_out_3_d_bits_data,
  input          auto_out_3_d_bits_corrupt,
  input          auto_out_2_a_ready,
  output         auto_out_2_a_valid,
  output [2:0]   auto_out_2_a_bits_opcode,
  output [2:0]   auto_out_2_a_bits_param,
  output [2:0]   auto_out_2_a_bits_size,
  output [3:0]   auto_out_2_a_bits_source,
  output [35:0]  auto_out_2_a_bits_address,
  output         auto_out_2_a_bits_user_amba_prot_bufferable,
  output         auto_out_2_a_bits_user_amba_prot_modifiable,
  output         auto_out_2_a_bits_user_amba_prot_readalloc,
  output         auto_out_2_a_bits_user_amba_prot_writealloc,
  output         auto_out_2_a_bits_user_amba_prot_privileged,
  output         auto_out_2_a_bits_user_amba_prot_secure,
  output [15:0]  auto_out_2_a_bits_mask,
  output [127:0] auto_out_2_a_bits_data,
  output         auto_out_2_d_ready,
  input          auto_out_2_d_valid,
  input  [2:0]   auto_out_2_d_bits_opcode,
  input  [2:0]   auto_out_2_d_bits_size,
  input  [3:0]   auto_out_2_d_bits_source,
  input          auto_out_2_d_bits_denied,
  input  [127:0] auto_out_2_d_bits_data,
  input          auto_out_2_d_bits_corrupt,
  input          auto_out_1_a_ready,
  output         auto_out_1_a_valid,
  output [2:0]   auto_out_1_a_bits_opcode,
  output [2:0]   auto_out_1_a_bits_param,
  output [2:0]   auto_out_1_a_bits_size,
  output [3:0]   auto_out_1_a_bits_source,
  output [35:0]  auto_out_1_a_bits_address,
  output         auto_out_1_a_bits_user_amba_prot_bufferable,
  output         auto_out_1_a_bits_user_amba_prot_modifiable,
  output         auto_out_1_a_bits_user_amba_prot_readalloc,
  output         auto_out_1_a_bits_user_amba_prot_writealloc,
  output         auto_out_1_a_bits_user_amba_prot_privileged,
  output         auto_out_1_a_bits_user_amba_prot_secure,
  output [15:0]  auto_out_1_a_bits_mask,
  output [127:0] auto_out_1_a_bits_data,
  output         auto_out_1_d_ready,
  input          auto_out_1_d_valid,
  input  [2:0]   auto_out_1_d_bits_opcode,
  input  [2:0]   auto_out_1_d_bits_size,
  input  [3:0]   auto_out_1_d_bits_source,
  input          auto_out_1_d_bits_denied,
  input  [127:0] auto_out_1_d_bits_data,
  input          auto_out_1_d_bits_corrupt,
  input          auto_out_0_a_ready,
  output         auto_out_0_a_valid,
  output [2:0]   auto_out_0_a_bits_opcode,
  output [2:0]   auto_out_0_a_bits_param,
  output [2:0]   auto_out_0_a_bits_size,
  output [3:0]   auto_out_0_a_bits_source,
  output [35:0]  auto_out_0_a_bits_address,
  output         auto_out_0_a_bits_user_amba_prot_bufferable,
  output         auto_out_0_a_bits_user_amba_prot_modifiable,
  output         auto_out_0_a_bits_user_amba_prot_readalloc,
  output         auto_out_0_a_bits_user_amba_prot_writealloc,
  output         auto_out_0_a_bits_user_amba_prot_privileged,
  output         auto_out_0_a_bits_user_amba_prot_secure,
  output [15:0]  auto_out_0_a_bits_mask,
  output [127:0] auto_out_0_a_bits_data,
  output         auto_out_0_d_ready,
  input          auto_out_0_d_valid,
  input  [2:0]   auto_out_0_d_bits_opcode,
  input  [2:0]   auto_out_0_d_bits_size,
  input  [3:0]   auto_out_0_d_bits_source,
  input          auto_out_0_d_bits_denied,
  input  [127:0] auto_out_0_d_bits_data,
  input          auto_out_0_d_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
`endif // RANDOMIZE_REG_INIT
  wire  pool_rf_reset; // @[CacheCork.scala 117:26]
  wire  pool_clock; // @[CacheCork.scala 117:26]
  wire  pool_reset; // @[CacheCork.scala 117:26]
  wire  pool_io_free_valid; // @[CacheCork.scala 117:26]
  wire [2:0] pool_io_free_bits; // @[CacheCork.scala 117:26]
  wire  pool_io_alloc_ready; // @[CacheCork.scala 117:26]
  wire  pool_io_alloc_valid; // @[CacheCork.scala 117:26]
  wire [2:0] pool_io_alloc_bits; // @[CacheCork.scala 117:26]
  wire  q_rf_reset; // @[Decoupled.scala 296:21]
  wire  q_clock; // @[Decoupled.scala 296:21]
  wire  q_reset; // @[Decoupled.scala 296:21]
  wire  q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_io_enq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_io_enq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_io_deq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_io_deq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  q_1_rf_reset; // @[Decoupled.scala 296:21]
  wire  q_1_clock; // @[Decoupled.scala 296:21]
  wire  q_1_reset; // @[Decoupled.scala 296:21]
  wire  q_1_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  q_1_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_1_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_1_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_1_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_1_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_1_io_enq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_1_io_enq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_1_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_1_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  q_1_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  q_1_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_1_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_1_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_1_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_1_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_1_io_deq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_1_io_deq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_1_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_1_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  pool_1_rf_reset; // @[CacheCork.scala 117:26]
  wire  pool_1_clock; // @[CacheCork.scala 117:26]
  wire  pool_1_reset; // @[CacheCork.scala 117:26]
  wire  pool_1_io_free_valid; // @[CacheCork.scala 117:26]
  wire [2:0] pool_1_io_free_bits; // @[CacheCork.scala 117:26]
  wire  pool_1_io_alloc_ready; // @[CacheCork.scala 117:26]
  wire  pool_1_io_alloc_valid; // @[CacheCork.scala 117:26]
  wire [2:0] pool_1_io_alloc_bits; // @[CacheCork.scala 117:26]
  wire  q_2_rf_reset; // @[Decoupled.scala 296:21]
  wire  q_2_clock; // @[Decoupled.scala 296:21]
  wire  q_2_reset; // @[Decoupled.scala 296:21]
  wire  q_2_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  q_2_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_2_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_2_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_2_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_2_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_2_io_enq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_2_io_enq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_2_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_2_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  q_2_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  q_2_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_2_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_2_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_2_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_2_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_2_io_deq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_2_io_deq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_2_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_2_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  q_3_rf_reset; // @[Decoupled.scala 296:21]
  wire  q_3_clock; // @[Decoupled.scala 296:21]
  wire  q_3_reset; // @[Decoupled.scala 296:21]
  wire  q_3_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  q_3_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_3_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_3_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_3_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_3_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_3_io_enq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_3_io_enq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_3_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_3_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  q_3_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  q_3_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_3_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_3_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_3_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_3_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_3_io_deq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_3_io_deq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_3_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_3_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  pool_2_rf_reset; // @[CacheCork.scala 117:26]
  wire  pool_2_clock; // @[CacheCork.scala 117:26]
  wire  pool_2_reset; // @[CacheCork.scala 117:26]
  wire  pool_2_io_free_valid; // @[CacheCork.scala 117:26]
  wire [2:0] pool_2_io_free_bits; // @[CacheCork.scala 117:26]
  wire  pool_2_io_alloc_ready; // @[CacheCork.scala 117:26]
  wire  pool_2_io_alloc_valid; // @[CacheCork.scala 117:26]
  wire [2:0] pool_2_io_alloc_bits; // @[CacheCork.scala 117:26]
  wire  q_4_rf_reset; // @[Decoupled.scala 296:21]
  wire  q_4_clock; // @[Decoupled.scala 296:21]
  wire  q_4_reset; // @[Decoupled.scala 296:21]
  wire  q_4_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  q_4_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_4_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_4_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_4_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_4_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_4_io_enq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_4_io_enq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_4_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_4_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  q_4_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  q_4_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_4_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_4_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_4_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_4_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_4_io_deq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_4_io_deq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_4_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_4_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  q_5_rf_reset; // @[Decoupled.scala 296:21]
  wire  q_5_clock; // @[Decoupled.scala 296:21]
  wire  q_5_reset; // @[Decoupled.scala 296:21]
  wire  q_5_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  q_5_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_5_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_5_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_5_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_5_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_5_io_enq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_5_io_enq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_5_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_5_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  q_5_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  q_5_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_5_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_5_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_5_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_5_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_5_io_deq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_5_io_deq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_5_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_5_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  pool_3_rf_reset; // @[CacheCork.scala 117:26]
  wire  pool_3_clock; // @[CacheCork.scala 117:26]
  wire  pool_3_reset; // @[CacheCork.scala 117:26]
  wire  pool_3_io_free_valid; // @[CacheCork.scala 117:26]
  wire [2:0] pool_3_io_free_bits; // @[CacheCork.scala 117:26]
  wire  pool_3_io_alloc_ready; // @[CacheCork.scala 117:26]
  wire  pool_3_io_alloc_valid; // @[CacheCork.scala 117:26]
  wire [2:0] pool_3_io_alloc_bits; // @[CacheCork.scala 117:26]
  wire  q_6_rf_reset; // @[Decoupled.scala 296:21]
  wire  q_6_clock; // @[Decoupled.scala 296:21]
  wire  q_6_reset; // @[Decoupled.scala 296:21]
  wire  q_6_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  q_6_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_6_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_6_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_6_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_6_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_6_io_enq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_6_io_enq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_6_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_6_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  q_6_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  q_6_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_6_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_6_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_6_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_6_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_6_io_deq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_6_io_deq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_6_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_6_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  q_7_rf_reset; // @[Decoupled.scala 296:21]
  wire  q_7_clock; // @[Decoupled.scala 296:21]
  wire  q_7_reset; // @[Decoupled.scala 296:21]
  wire  q_7_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  q_7_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_7_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_7_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_7_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_7_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_7_io_enq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_7_io_enq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_7_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_7_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  q_7_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  q_7_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] q_7_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [1:0] q_7_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] q_7_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [2:0] q_7_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [2:0] q_7_io_deq_bits_sink; // @[Decoupled.scala 296:21]
  wire  q_7_io_deq_bits_denied; // @[Decoupled.scala 296:21]
  wire [127:0] q_7_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  q_7_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  isPut = auto_in_0_a_bits_opcode == 3'h0 | auto_in_0_a_bits_opcode == 3'h1; // @[CacheCork.scala 66:54]
  wire  _toD_T = auto_in_0_a_bits_opcode == 3'h6; // @[CacheCork.scala 67:37]
  wire  _toD_T_3 = auto_in_0_a_bits_opcode == 3'h7; // @[CacheCork.scala 68:37]
  wire  toD = auto_in_0_a_bits_opcode == 3'h6 & auto_in_0_a_bits_param == 3'h2 | _toD_T_3; // @[CacheCork.scala 67:97]
  wire  a_d_ready = q_1_io_enq_ready; // @[CacheCork.scala 65:23 Decoupled.scala 299:17]
  reg [1:0] beatsLeft; // @[Arbiter.scala 87:30]
  wire  idle = beatsLeft == 2'h0; // @[Arbiter.scala 88:28]
  wire  a_a_valid = auto_in_0_a_valid & ~toD; // @[CacheCork.scala 71:33]
  wire  _c_a_valid_T = auto_in_0_c_bits_opcode == 3'h7; // @[CacheCork.scala 92:53]
  wire  c_a_valid = auto_in_0_c_valid & _c_a_valid_T; // @[CacheCork.scala 92:33]
  wire [1:0] _readys_T = {a_a_valid,c_a_valid}; // @[Cat.scala 30:58]
  wire [2:0] _readys_T_1 = {_readys_T, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_T_3 = _readys_T | _readys_T_1[1:0]; // @[package.scala 244:43]
  wire [2:0] _readys_T_5 = {_readys_T_3, 1'h0}; // @[Arbiter.scala 16:78]
  wire [1:0] _readys_T_7 = ~_readys_T_5[1:0]; // @[Arbiter.scala 16:61]
  wire  readys__1 = _readys_T_7[1]; // @[Arbiter.scala 95:86]
  reg  state__1; // @[Arbiter.scala 116:26]
  wire  allowed__1 = idle ? readys__1 : state__1; // @[Arbiter.scala 121:24]
  wire  out_1_ready = auto_out_0_a_ready & allowed__1; // @[Arbiter.scala 123:31]
  wire [3:0] _a_a_bits_source_T = {auto_in_0_a_bits_source, 1'h0}; // @[CacheCork.scala 73:45]
  wire [3:0] _GEN_108 = {{3'd0}, isPut}; // @[CacheCork.scala 73:50]
  wire [3:0] _a_a_bits_source_T_2 = _a_a_bits_source_T | _GEN_108; // @[CacheCork.scala 73:50]
  wire [3:0] _a_a_bits_source_T_4 = _a_a_bits_source_T | 4'h1; // @[CacheCork.scala 79:52]
  wire [2:0] a_a_bits_opcode = _toD_T | _toD_T_3 ? 3'h4 : auto_in_0_a_bits_opcode; // @[CacheCork.scala 76:86 CacheCork.scala 77:27 CacheCork.scala 72:18]
  wire [2:0] a_a_bits_param = _toD_T | _toD_T_3 ? 3'h0 : auto_in_0_a_bits_param; // @[CacheCork.scala 76:86 CacheCork.scala 78:27 CacheCork.scala 72:18]
  wire [3:0] a_a_bits_source = _toD_T | _toD_T_3 ? _a_a_bits_source_T_4 : _a_a_bits_source_T_2; // @[CacheCork.scala 76:86 CacheCork.scala 79:27 CacheCork.scala 73:25]
  wire [3:0] c_a_bits_a_source = {auto_in_0_c_bits_source, 1'h0}; // @[CacheCork.scala 94:41]
  wire  _c_d_valid_T = auto_in_0_c_bits_opcode == 3'h6; // @[CacheCork.scala 103:53]
  wire  c_d_ready = q_io_enq_ready; // @[CacheCork.scala 102:23 Decoupled.scala 299:17]
  wire  readys__0 = _readys_T_7[0]; // @[Arbiter.scala 95:86]
  reg  state__0; // @[Arbiter.scala 116:26]
  wire  allowed__0 = idle ? readys__0 : state__0; // @[Arbiter.scala 121:24]
  wire  out_ready = auto_out_0_a_ready & allowed__0; // @[Arbiter.scala 123:31]
  reg [1:0] d_first_counter; // @[Edges.scala 228:27]
  wire  d_first = d_first_counter == 2'h0; // @[Edges.scala 230:25]
  reg [1:0] beatsLeft_1; // @[Arbiter.scala 87:30]
  wire  idle_1 = beatsLeft_1 == 2'h0; // @[Arbiter.scala 88:28]
  wire  out_5_earlyValid = q_1_io_deq_valid; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 71:20]
  wire  out_4_earlyValid = q_io_deq_valid; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 71:20]
  wire [2:0] _readys_T_10 = {out_5_earlyValid,out_4_earlyValid,auto_out_0_d_valid}; // @[Cat.scala 30:58]
  wire [3:0] _readys_T_11 = {_readys_T_10, 1'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_T_13 = _readys_T_10 | _readys_T_11[2:0]; // @[package.scala 244:43]
  wire [4:0] _readys_T_14 = {_readys_T_13, 2'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_T_16 = _readys_T_13 | _readys_T_14[2:0]; // @[package.scala 244:43]
  wire [3:0] _readys_T_18 = {_readys_T_16, 1'h0}; // @[Arbiter.scala 16:78]
  wire [2:0] _readys_T_20 = ~_readys_T_18[2:0]; // @[Arbiter.scala 16:61]
  wire  readys_1_0 = _readys_T_20[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_1_0 = readys_1_0 & auto_out_0_d_valid; // @[Arbiter.scala 97:79]
  reg  state_1_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_1_0 = idle_1 ? earlyWinner_1_0 : state_1_0; // @[Arbiter.scala 117:30]
  wire [2:0] _GEN_24 = auto_out_0_d_bits_opcode == 3'h1 & auto_out_0_d_bits_source[0] ? 3'h5 : auto_out_0_d_bits_opcode; // @[CacheCork.scala 152:76 CacheCork.scala 153:27 CacheCork.scala 132:13]
  wire [2:0] d_d_bits_opcode = auto_out_0_d_bits_opcode == 3'h0 & ~auto_out_0_d_bits_source[0] ? 3'h6 : _GEN_24; // @[CacheCork.scala 156:73 CacheCork.scala 157:27]
  wire [2:0] _T_162 = muxStateEarly_1_0 ? d_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire  readys_1_1 = _readys_T_20[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_1_1 = readys_1_1 & out_4_earlyValid; // @[Arbiter.scala 97:79]
  reg  state_1_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_1_1 = idle_1 ? earlyWinner_1_1 : state_1_1; // @[Arbiter.scala 117:30]
  wire [2:0] out_4_bits_opcode = q_io_deq_bits_opcode; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_163 = muxStateEarly_1_1 ? out_4_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_165 = _T_162 | _T_163; // @[Mux.scala 27:72]
  wire  readys_1_2 = _readys_T_20[2]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_1_2 = readys_1_2 & out_5_earlyValid; // @[Arbiter.scala 97:79]
  reg  state_1_2; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_1_2 = idle_1 ? earlyWinner_1_2 : state_1_2; // @[Arbiter.scala 117:30]
  wire [2:0] out_5_bits_opcode = q_1_io_deq_bits_opcode; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_164 = muxStateEarly_1_2 ? out_5_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] sink_ACancel_1_bits_opcode = _T_165 | _T_164; // @[Mux.scala 27:72]
  wire  d_grant = sink_ACancel_1_bits_opcode == 3'h5 | sink_ACancel_1_bits_opcode == 3'h4; // @[CacheCork.scala 123:54]
  wire  _in_d_ready_T_3 = pool_io_alloc_valid | ~d_first | ~d_grant; // @[CacheCork.scala 126:70]
  wire  in_d_ready = auto_in_0_d_ready & _in_d_ready_T_3; // @[CacheCork.scala 126:34]
  wire  _sink_ACancel_earlyValid_T_6 = auto_out_0_d_valid | out_4_earlyValid | out_5_earlyValid; // @[Arbiter.scala 125:56]
  wire  _sink_ACancel_earlyValid_T_11 = state_1_0 & auto_out_0_d_valid | state_1_1 & out_4_earlyValid | state_1_2 &
    out_5_earlyValid; // @[Mux.scala 27:72]
  wire  sink_ACancel_1_earlyValid = idle_1 ? _sink_ACancel_earlyValid_T_6 : _sink_ACancel_earlyValid_T_11; // @[Arbiter.scala 125:29]
  wire  _d_first_T = in_d_ready & sink_ACancel_1_earlyValid; // @[Decoupled.scala 40:37]
  wire [2:0] _T_152 = muxStateEarly_1_0 ? auto_out_0_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] out_4_bits_size = q_io_deq_bits_size; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_153 = muxStateEarly_1_1 ? out_4_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_155 = _T_152 | _T_153; // @[Mux.scala 27:72]
  wire [2:0] out_5_bits_size = q_1_io_deq_bits_size; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_154 = muxStateEarly_1_2 ? out_5_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] sink_ACancel_1_bits_size = _T_155 | _T_154; // @[Mux.scala 27:72]
  wire [12:0] _d_first_beats1_decode_T_1 = 13'h3f << sink_ACancel_1_bits_size; // @[package.scala 234:77]
  wire [5:0] _d_first_beats1_decode_T_3 = ~_d_first_beats1_decode_T_1[5:0]; // @[package.scala 234:46]
  wire [1:0] d_first_beats1_decode = _d_first_beats1_decode_T_3[5:4]; // @[Edges.scala 219:59]
  wire  d_first_beats1_opdata = sink_ACancel_1_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [1:0] d_first_counter1 = d_first_counter - 2'h1; // @[Edges.scala 229:28]
  wire  bundleIn_0_d_valid = sink_ACancel_1_earlyValid & _in_d_ready_T_3; // @[CacheCork.scala 125:34]
  wire  _pool_io_alloc_ready_T = auto_in_0_d_ready & bundleIn_0_d_valid; // @[Decoupled.scala 40:37]
  reg [2:0] bundleIn_0_d_bits_sink_r; // @[Reg.scala 15:16]
  wire [2:0] d_d_bits_source = auto_out_0_d_bits_source[3:1]; // @[CacheCork.scala 133:46]
  wire  opdata_1 = ~a_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [1:0] _T_24 = opdata_1 ? 2'h3 : 2'h0; // @[Edges.scala 220:14]
  wire  latch = idle & auto_out_0_a_ready; // @[Arbiter.scala 89:24]
  wire  earlyWinner__0 = readys__0 & c_a_valid; // @[Arbiter.scala 97:79]
  wire  earlyWinner__1 = readys__1 & a_a_valid; // @[Arbiter.scala 97:79]
  wire  _T_35 = c_a_valid | a_a_valid; // @[Arbiter.scala 107:36]
  wire [1:0] maskedBeats_0 = earlyWinner__0 ? 2'h3 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_1 = earlyWinner__1 ? _T_24 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] initBeats = maskedBeats_0 | maskedBeats_1; // @[Arbiter.scala 112:44]
  wire  muxStateEarly__0 = idle ? earlyWinner__0 : state__0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly__1 = idle ? earlyWinner__1 : state__1; // @[Arbiter.scala 117:30]
  wire  _sink_ACancel_earlyValid_T_3 = state__0 & c_a_valid | state__1 & a_a_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_earlyValid = idle ? _T_35 : _sink_ACancel_earlyValid_T_3; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_2 = auto_out_0_a_ready & sink_ACancel_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [1:0] _GEN_109 = {{1'd0}, _beatsLeft_T_2}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_4 = beatsLeft - _GEN_109; // @[Arbiter.scala 113:52]
  wire [15:0] _T_55 = muxStateEarly__0 ? 16'hffff : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_56 = muxStateEarly__1 ? 16'hffff : 16'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_79 = muxStateEarly__0 ? auto_in_0_c_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_80 = muxStateEarly__1 ? auto_in_0_a_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_82 = muxStateEarly__0 ? c_a_bits_a_source : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_83 = muxStateEarly__1 ? a_a_bits_source : 4'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_85 = muxStateEarly__0 ? 3'h6 : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_86 = muxStateEarly__1 ? 3'h6 : 3'h0; // @[Mux.scala 27:72]
  wire [12:0] _decode_T_9 = 13'h3f << auto_out_0_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _decode_T_11 = ~_decode_T_9[5:0]; // @[package.scala 234:46]
  wire [1:0] decode_2 = _decode_T_11[5:4]; // @[Edges.scala 219:59]
  wire  opdata_2 = d_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire  latch_1 = idle_1 & in_d_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _GEN_110 = {{1'd0}, _d_first_T}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_10 = beatsLeft_1 - _GEN_110; // @[Arbiter.scala 113:52]
  wire  allowed_1_0 = idle_1 ? readys_1_0 : state_1_0; // @[Arbiter.scala 121:24]
  wire  allowed_1_1 = idle_1 ? readys_1_1 : state_1_1; // @[Arbiter.scala 121:24]
  wire  allowed_1_2 = idle_1 ? readys_1_2 : state_1_2; // @[Arbiter.scala 121:24]
  wire  out_4_bits_corrupt = q_io_deq_bits_corrupt; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  out_5_bits_corrupt = q_1_io_deq_bits_corrupt; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  _T_129 = muxStateEarly_1_2 & out_5_bits_corrupt; // @[Mux.scala 27:72]
  wire  _T_130 = muxStateEarly_1_0 & auto_out_0_d_bits_corrupt | muxStateEarly_1_1 & out_4_bits_corrupt; // @[Mux.scala 27:72]
  wire [127:0] _T_132 = muxStateEarly_1_0 ? auto_out_0_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] out_4_bits_data = q_io_deq_bits_data; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [127:0] _T_133 = muxStateEarly_1_1 ? out_4_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] out_5_bits_data = q_1_io_deq_bits_data; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [127:0] _T_134 = muxStateEarly_1_2 ? out_5_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_135 = _T_132 | _T_133; // @[Mux.scala 27:72]
  wire  out_4_bits_denied = q_io_deq_bits_denied; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  out_5_bits_denied = q_1_io_deq_bits_denied; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  _T_139 = muxStateEarly_1_2 & out_5_bits_denied; // @[Mux.scala 27:72]
  wire  _T_140 = muxStateEarly_1_0 & auto_out_0_d_bits_denied | muxStateEarly_1_1 & out_4_bits_denied; // @[Mux.scala 27:72]
  wire [2:0] _T_147 = muxStateEarly_1_0 ? d_d_bits_source : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] out_4_bits_source = q_io_deq_bits_source; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_148 = muxStateEarly_1_1 ? out_4_bits_source : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] out_5_bits_source = q_1_io_deq_bits_source; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_149 = muxStateEarly_1_2 ? out_5_bits_source : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_150 = _T_147 | _T_148; // @[Mux.scala 27:72]
  wire [1:0] out_4_bits_param = q_io_deq_bits_param; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [1:0] _T_158 = muxStateEarly_1_1 ? out_4_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] out_5_bits_param = q_1_io_deq_bits_param; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [1:0] _T_159 = muxStateEarly_1_2 ? out_5_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire  isPut_1 = auto_in_1_a_bits_opcode == 3'h0 | auto_in_1_a_bits_opcode == 3'h1; // @[CacheCork.scala 66:54]
  wire  _toD_T_4 = auto_in_1_a_bits_opcode == 3'h6; // @[CacheCork.scala 67:37]
  wire  _toD_T_7 = auto_in_1_a_bits_opcode == 3'h7; // @[CacheCork.scala 68:37]
  wire  toD_1 = auto_in_1_a_bits_opcode == 3'h6 & auto_in_1_a_bits_param == 3'h2 | _toD_T_7; // @[CacheCork.scala 67:97]
  wire  a_d_1_ready = q_3_io_enq_ready; // @[CacheCork.scala 65:23 Decoupled.scala 299:17]
  reg [1:0] beatsLeft_2; // @[Arbiter.scala 87:30]
  wire  idle_2 = beatsLeft_2 == 2'h0; // @[Arbiter.scala 88:28]
  wire  a_a_1_valid = auto_in_1_a_valid & ~toD_1; // @[CacheCork.scala 71:33]
  wire  _c_a_valid_T_2 = auto_in_1_c_bits_opcode == 3'h7; // @[CacheCork.scala 92:53]
  wire  c_a_1_valid = auto_in_1_c_valid & _c_a_valid_T_2; // @[CacheCork.scala 92:33]
  wire [1:0] _readys_T_24 = {a_a_1_valid,c_a_1_valid}; // @[Cat.scala 30:58]
  wire [2:0] _readys_T_25 = {_readys_T_24, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_T_27 = _readys_T_24 | _readys_T_25[1:0]; // @[package.scala 244:43]
  wire [2:0] _readys_T_29 = {_readys_T_27, 1'h0}; // @[Arbiter.scala 16:78]
  wire [1:0] _readys_T_31 = ~_readys_T_29[1:0]; // @[Arbiter.scala 16:61]
  wire  readys_2_1 = _readys_T_31[1]; // @[Arbiter.scala 95:86]
  reg  state_2_1; // @[Arbiter.scala 116:26]
  wire  allowed_2_1 = idle_2 ? readys_2_1 : state_2_1; // @[Arbiter.scala 121:24]
  wire  out_8_ready = auto_out_1_a_ready & allowed_2_1; // @[Arbiter.scala 123:31]
  wire [3:0] _a_a_bits_source_T_5 = {auto_in_1_a_bits_source, 1'h0}; // @[CacheCork.scala 73:45]
  wire [3:0] _GEN_111 = {{3'd0}, isPut_1}; // @[CacheCork.scala 73:50]
  wire [3:0] _a_a_bits_source_T_7 = _a_a_bits_source_T_5 | _GEN_111; // @[CacheCork.scala 73:50]
  wire [3:0] _a_a_bits_source_T_9 = _a_a_bits_source_T_5 | 4'h1; // @[CacheCork.scala 79:52]
  wire [2:0] a_a_1_bits_opcode = _toD_T_4 | _toD_T_7 ? 3'h4 : auto_in_1_a_bits_opcode; // @[CacheCork.scala 76:86 CacheCork.scala 77:27 CacheCork.scala 72:18]
  wire [2:0] a_a_1_bits_param = _toD_T_4 | _toD_T_7 ? 3'h0 : auto_in_1_a_bits_param; // @[CacheCork.scala 76:86 CacheCork.scala 78:27 CacheCork.scala 72:18]
  wire [3:0] a_a_1_bits_source = _toD_T_4 | _toD_T_7 ? _a_a_bits_source_T_9 : _a_a_bits_source_T_7; // @[CacheCork.scala 76:86 CacheCork.scala 79:27 CacheCork.scala 73:25]
  wire [3:0] c_a_bits_a_1_source = {auto_in_1_c_bits_source, 1'h0}; // @[CacheCork.scala 94:41]
  wire  _c_d_valid_T_2 = auto_in_1_c_bits_opcode == 3'h6; // @[CacheCork.scala 103:53]
  wire  c_d_1_ready = q_2_io_enq_ready; // @[CacheCork.scala 102:23 Decoupled.scala 299:17]
  wire  readys_2_0 = _readys_T_31[0]; // @[Arbiter.scala 95:86]
  reg  state_2_0; // @[Arbiter.scala 116:26]
  wire  allowed_2_0 = idle_2 ? readys_2_0 : state_2_0; // @[Arbiter.scala 121:24]
  wire  out_7_ready = auto_out_1_a_ready & allowed_2_0; // @[Arbiter.scala 123:31]
  reg [1:0] d_first_counter_1; // @[Edges.scala 228:27]
  wire  d_first_1 = d_first_counter_1 == 2'h0; // @[Edges.scala 230:25]
  reg [1:0] beatsLeft_3; // @[Arbiter.scala 87:30]
  wire  idle_3 = beatsLeft_3 == 2'h0; // @[Arbiter.scala 88:28]
  wire  out_12_earlyValid = q_3_io_deq_valid; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 71:20]
  wire  out_11_earlyValid = q_2_io_deq_valid; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 71:20]
  wire [2:0] _readys_T_34 = {out_12_earlyValid,out_11_earlyValid,auto_out_1_d_valid}; // @[Cat.scala 30:58]
  wire [3:0] _readys_T_35 = {_readys_T_34, 1'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_T_37 = _readys_T_34 | _readys_T_35[2:0]; // @[package.scala 244:43]
  wire [4:0] _readys_T_38 = {_readys_T_37, 2'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_T_40 = _readys_T_37 | _readys_T_38[2:0]; // @[package.scala 244:43]
  wire [3:0] _readys_T_42 = {_readys_T_40, 1'h0}; // @[Arbiter.scala 16:78]
  wire [2:0] _readys_T_44 = ~_readys_T_42[2:0]; // @[Arbiter.scala 16:61]
  wire  readys_3_0 = _readys_T_44[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_3_0 = readys_3_0 & auto_out_1_d_valid; // @[Arbiter.scala 97:79]
  reg  state_3_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_3_0 = idle_3 ? earlyWinner_3_0 : state_3_0; // @[Arbiter.scala 117:30]
  wire [2:0] _GEN_51 = auto_out_1_d_bits_opcode == 3'h1 & auto_out_1_d_bits_source[0] ? 3'h5 : auto_out_1_d_bits_opcode; // @[CacheCork.scala 152:76 CacheCork.scala 153:27 CacheCork.scala 132:13]
  wire [2:0] d_d_1_bits_opcode = auto_out_1_d_bits_opcode == 3'h0 & ~auto_out_1_d_bits_source[0] ? 3'h6 : _GEN_51; // @[CacheCork.scala 156:73 CacheCork.scala 157:27]
  wire [2:0] _T_329 = muxStateEarly_3_0 ? d_d_1_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire  readys_3_1 = _readys_T_44[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_3_1 = readys_3_1 & out_11_earlyValid; // @[Arbiter.scala 97:79]
  reg  state_3_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_3_1 = idle_3 ? earlyWinner_3_1 : state_3_1; // @[Arbiter.scala 117:30]
  wire [2:0] out_11_bits_opcode = q_2_io_deq_bits_opcode; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_330 = muxStateEarly_3_1 ? out_11_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_332 = _T_329 | _T_330; // @[Mux.scala 27:72]
  wire  readys_3_2 = _readys_T_44[2]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_3_2 = readys_3_2 & out_12_earlyValid; // @[Arbiter.scala 97:79]
  reg  state_3_2; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_3_2 = idle_3 ? earlyWinner_3_2 : state_3_2; // @[Arbiter.scala 117:30]
  wire [2:0] out_12_bits_opcode = q_3_io_deq_bits_opcode; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_331 = muxStateEarly_3_2 ? out_12_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] sink_ACancel_3_bits_opcode = _T_332 | _T_331; // @[Mux.scala 27:72]
  wire  d_grant_1 = sink_ACancel_3_bits_opcode == 3'h5 | sink_ACancel_3_bits_opcode == 3'h4; // @[CacheCork.scala 123:54]
  wire  _in_d_ready_T_8 = pool_1_io_alloc_valid | ~d_first_1 | ~d_grant_1; // @[CacheCork.scala 126:70]
  wire  in_d_1_ready = auto_in_1_d_ready & _in_d_ready_T_8; // @[CacheCork.scala 126:34]
  wire  _sink_ACancel_earlyValid_T_19 = auto_out_1_d_valid | out_11_earlyValid | out_12_earlyValid; // @[Arbiter.scala 125:56]
  wire  _sink_ACancel_earlyValid_T_24 = state_3_0 & auto_out_1_d_valid | state_3_1 & out_11_earlyValid | state_3_2 &
    out_12_earlyValid; // @[Mux.scala 27:72]
  wire  sink_ACancel_3_earlyValid = idle_3 ? _sink_ACancel_earlyValid_T_19 : _sink_ACancel_earlyValid_T_24; // @[Arbiter.scala 125:29]
  wire  _d_first_T_1 = in_d_1_ready & sink_ACancel_3_earlyValid; // @[Decoupled.scala 40:37]
  wire [2:0] _T_319 = muxStateEarly_3_0 ? auto_out_1_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] out_11_bits_size = q_2_io_deq_bits_size; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_320 = muxStateEarly_3_1 ? out_11_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_322 = _T_319 | _T_320; // @[Mux.scala 27:72]
  wire [2:0] out_12_bits_size = q_3_io_deq_bits_size; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_321 = muxStateEarly_3_2 ? out_12_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] sink_ACancel_3_bits_size = _T_322 | _T_321; // @[Mux.scala 27:72]
  wire [12:0] _d_first_beats1_decode_T_5 = 13'h3f << sink_ACancel_3_bits_size; // @[package.scala 234:77]
  wire [5:0] _d_first_beats1_decode_T_7 = ~_d_first_beats1_decode_T_5[5:0]; // @[package.scala 234:46]
  wire [1:0] d_first_beats1_decode_1 = _d_first_beats1_decode_T_7[5:4]; // @[Edges.scala 219:59]
  wire  d_first_beats1_opdata_1 = sink_ACancel_3_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [1:0] d_first_counter1_1 = d_first_counter_1 - 2'h1; // @[Edges.scala 229:28]
  wire  bundleIn_1_d_valid = sink_ACancel_3_earlyValid & _in_d_ready_T_8; // @[CacheCork.scala 125:34]
  wire  _pool_io_alloc_ready_T_3 = auto_in_1_d_ready & bundleIn_1_d_valid; // @[Decoupled.scala 40:37]
  reg [2:0] bundleIn_1_d_bits_sink_r; // @[Reg.scala 15:16]
  wire [2:0] d_d_1_bits_source = auto_out_1_d_bits_source[3:1]; // @[CacheCork.scala 133:46]
  wire  opdata_4 = ~a_a_1_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [1:0] _T_191 = opdata_4 ? 2'h3 : 2'h0; // @[Edges.scala 220:14]
  wire  latch_2 = idle_2 & auto_out_1_a_ready; // @[Arbiter.scala 89:24]
  wire  earlyWinner_2_0 = readys_2_0 & c_a_1_valid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_2_1 = readys_2_1 & a_a_1_valid; // @[Arbiter.scala 97:79]
  wire  _T_202 = c_a_1_valid | a_a_1_valid; // @[Arbiter.scala 107:36]
  wire [1:0] maskedBeats_0_2 = earlyWinner_2_0 ? 2'h3 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_1_2 = earlyWinner_2_1 ? _T_191 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] initBeats_2 = maskedBeats_0_2 | maskedBeats_1_2; // @[Arbiter.scala 112:44]
  wire  muxStateEarly_2_0 = idle_2 ? earlyWinner_2_0 : state_2_0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_2_1 = idle_2 ? earlyWinner_2_1 : state_2_1; // @[Arbiter.scala 117:30]
  wire  _sink_ACancel_earlyValid_T_16 = state_2_0 & c_a_1_valid | state_2_1 & a_a_1_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_2_earlyValid = idle_2 ? _T_202 : _sink_ACancel_earlyValid_T_16; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_14 = auto_out_1_a_ready & sink_ACancel_2_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [1:0] _GEN_112 = {{1'd0}, _beatsLeft_T_14}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_16 = beatsLeft_2 - _GEN_112; // @[Arbiter.scala 113:52]
  wire [15:0] _T_222 = muxStateEarly_2_0 ? 16'hffff : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_223 = muxStateEarly_2_1 ? 16'hffff : 16'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_246 = muxStateEarly_2_0 ? auto_in_1_c_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_247 = muxStateEarly_2_1 ? auto_in_1_a_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_249 = muxStateEarly_2_0 ? c_a_bits_a_1_source : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_250 = muxStateEarly_2_1 ? a_a_1_bits_source : 4'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_252 = muxStateEarly_2_0 ? 3'h6 : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_253 = muxStateEarly_2_1 ? 3'h6 : 3'h0; // @[Mux.scala 27:72]
  wire [12:0] _decode_T_21 = 13'h3f << auto_out_1_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _decode_T_23 = ~_decode_T_21[5:0]; // @[package.scala 234:46]
  wire [1:0] decode_5 = _decode_T_23[5:4]; // @[Edges.scala 219:59]
  wire  opdata_5 = d_d_1_bits_opcode[0]; // @[Edges.scala 105:36]
  wire  latch_3 = idle_3 & in_d_1_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _GEN_113 = {{1'd0}, _d_first_T_1}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_22 = beatsLeft_3 - _GEN_113; // @[Arbiter.scala 113:52]
  wire  allowed_3_0 = idle_3 ? readys_3_0 : state_3_0; // @[Arbiter.scala 121:24]
  wire  allowed_3_1 = idle_3 ? readys_3_1 : state_3_1; // @[Arbiter.scala 121:24]
  wire  allowed_3_2 = idle_3 ? readys_3_2 : state_3_2; // @[Arbiter.scala 121:24]
  wire  out_11_bits_corrupt = q_2_io_deq_bits_corrupt; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  out_12_bits_corrupt = q_3_io_deq_bits_corrupt; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  _T_296 = muxStateEarly_3_2 & out_12_bits_corrupt; // @[Mux.scala 27:72]
  wire  _T_297 = muxStateEarly_3_0 & auto_out_1_d_bits_corrupt | muxStateEarly_3_1 & out_11_bits_corrupt; // @[Mux.scala 27:72]
  wire [127:0] _T_299 = muxStateEarly_3_0 ? auto_out_1_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] out_11_bits_data = q_2_io_deq_bits_data; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [127:0] _T_300 = muxStateEarly_3_1 ? out_11_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] out_12_bits_data = q_3_io_deq_bits_data; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [127:0] _T_301 = muxStateEarly_3_2 ? out_12_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_302 = _T_299 | _T_300; // @[Mux.scala 27:72]
  wire  out_11_bits_denied = q_2_io_deq_bits_denied; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  out_12_bits_denied = q_3_io_deq_bits_denied; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  _T_306 = muxStateEarly_3_2 & out_12_bits_denied; // @[Mux.scala 27:72]
  wire  _T_307 = muxStateEarly_3_0 & auto_out_1_d_bits_denied | muxStateEarly_3_1 & out_11_bits_denied; // @[Mux.scala 27:72]
  wire [2:0] _T_314 = muxStateEarly_3_0 ? d_d_1_bits_source : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] out_11_bits_source = q_2_io_deq_bits_source; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_315 = muxStateEarly_3_1 ? out_11_bits_source : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] out_12_bits_source = q_3_io_deq_bits_source; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_316 = muxStateEarly_3_2 ? out_12_bits_source : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_317 = _T_314 | _T_315; // @[Mux.scala 27:72]
  wire [1:0] out_11_bits_param = q_2_io_deq_bits_param; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [1:0] _T_325 = muxStateEarly_3_1 ? out_11_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] out_12_bits_param = q_3_io_deq_bits_param; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [1:0] _T_326 = muxStateEarly_3_2 ? out_12_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire  isPut_2 = auto_in_2_a_bits_opcode == 3'h0 | auto_in_2_a_bits_opcode == 3'h1; // @[CacheCork.scala 66:54]
  wire  _toD_T_8 = auto_in_2_a_bits_opcode == 3'h6; // @[CacheCork.scala 67:37]
  wire  _toD_T_11 = auto_in_2_a_bits_opcode == 3'h7; // @[CacheCork.scala 68:37]
  wire  toD_2 = auto_in_2_a_bits_opcode == 3'h6 & auto_in_2_a_bits_param == 3'h2 | _toD_T_11; // @[CacheCork.scala 67:97]
  wire  a_d_2_ready = q_5_io_enq_ready; // @[CacheCork.scala 65:23 Decoupled.scala 299:17]
  reg [1:0] beatsLeft_4; // @[Arbiter.scala 87:30]
  wire  idle_4 = beatsLeft_4 == 2'h0; // @[Arbiter.scala 88:28]
  wire  a_a_2_valid = auto_in_2_a_valid & ~toD_2; // @[CacheCork.scala 71:33]
  wire  _c_a_valid_T_4 = auto_in_2_c_bits_opcode == 3'h7; // @[CacheCork.scala 92:53]
  wire  c_a_2_valid = auto_in_2_c_valid & _c_a_valid_T_4; // @[CacheCork.scala 92:33]
  wire [1:0] _readys_T_48 = {a_a_2_valid,c_a_2_valid}; // @[Cat.scala 30:58]
  wire [2:0] _readys_T_49 = {_readys_T_48, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_T_51 = _readys_T_48 | _readys_T_49[1:0]; // @[package.scala 244:43]
  wire [2:0] _readys_T_53 = {_readys_T_51, 1'h0}; // @[Arbiter.scala 16:78]
  wire [1:0] _readys_T_55 = ~_readys_T_53[1:0]; // @[Arbiter.scala 16:61]
  wire  readys_4_1 = _readys_T_55[1]; // @[Arbiter.scala 95:86]
  reg  state_4_1; // @[Arbiter.scala 116:26]
  wire  allowed_4_1 = idle_4 ? readys_4_1 : state_4_1; // @[Arbiter.scala 121:24]
  wire  out_15_ready = auto_out_2_a_ready & allowed_4_1; // @[Arbiter.scala 123:31]
  wire [3:0] _a_a_bits_source_T_10 = {auto_in_2_a_bits_source, 1'h0}; // @[CacheCork.scala 73:45]
  wire [3:0] _GEN_114 = {{3'd0}, isPut_2}; // @[CacheCork.scala 73:50]
  wire [3:0] _a_a_bits_source_T_12 = _a_a_bits_source_T_10 | _GEN_114; // @[CacheCork.scala 73:50]
  wire [3:0] _a_a_bits_source_T_14 = _a_a_bits_source_T_10 | 4'h1; // @[CacheCork.scala 79:52]
  wire [2:0] a_a_2_bits_opcode = _toD_T_8 | _toD_T_11 ? 3'h4 : auto_in_2_a_bits_opcode; // @[CacheCork.scala 76:86 CacheCork.scala 77:27 CacheCork.scala 72:18]
  wire [2:0] a_a_2_bits_param = _toD_T_8 | _toD_T_11 ? 3'h0 : auto_in_2_a_bits_param; // @[CacheCork.scala 76:86 CacheCork.scala 78:27 CacheCork.scala 72:18]
  wire [3:0] a_a_2_bits_source = _toD_T_8 | _toD_T_11 ? _a_a_bits_source_T_14 : _a_a_bits_source_T_12; // @[CacheCork.scala 76:86 CacheCork.scala 79:27 CacheCork.scala 73:25]
  wire [3:0] c_a_bits_a_2_source = {auto_in_2_c_bits_source, 1'h0}; // @[CacheCork.scala 94:41]
  wire  _c_d_valid_T_4 = auto_in_2_c_bits_opcode == 3'h6; // @[CacheCork.scala 103:53]
  wire  c_d_2_ready = q_4_io_enq_ready; // @[CacheCork.scala 102:23 Decoupled.scala 299:17]
  wire  readys_4_0 = _readys_T_55[0]; // @[Arbiter.scala 95:86]
  reg  state_4_0; // @[Arbiter.scala 116:26]
  wire  allowed_4_0 = idle_4 ? readys_4_0 : state_4_0; // @[Arbiter.scala 121:24]
  wire  out_14_ready = auto_out_2_a_ready & allowed_4_0; // @[Arbiter.scala 123:31]
  reg [1:0] d_first_counter_2; // @[Edges.scala 228:27]
  wire  d_first_2 = d_first_counter_2 == 2'h0; // @[Edges.scala 230:25]
  reg [1:0] beatsLeft_5; // @[Arbiter.scala 87:30]
  wire  idle_5 = beatsLeft_5 == 2'h0; // @[Arbiter.scala 88:28]
  wire  out_19_earlyValid = q_5_io_deq_valid; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 71:20]
  wire  out_18_earlyValid = q_4_io_deq_valid; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 71:20]
  wire [2:0] _readys_T_58 = {out_19_earlyValid,out_18_earlyValid,auto_out_2_d_valid}; // @[Cat.scala 30:58]
  wire [3:0] _readys_T_59 = {_readys_T_58, 1'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_T_61 = _readys_T_58 | _readys_T_59[2:0]; // @[package.scala 244:43]
  wire [4:0] _readys_T_62 = {_readys_T_61, 2'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_T_64 = _readys_T_61 | _readys_T_62[2:0]; // @[package.scala 244:43]
  wire [3:0] _readys_T_66 = {_readys_T_64, 1'h0}; // @[Arbiter.scala 16:78]
  wire [2:0] _readys_T_68 = ~_readys_T_66[2:0]; // @[Arbiter.scala 16:61]
  wire  readys_5_0 = _readys_T_68[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_5_0 = readys_5_0 & auto_out_2_d_valid; // @[Arbiter.scala 97:79]
  reg  state_5_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_5_0 = idle_5 ? earlyWinner_5_0 : state_5_0; // @[Arbiter.scala 117:30]
  wire [2:0] _GEN_78 = auto_out_2_d_bits_opcode == 3'h1 & auto_out_2_d_bits_source[0] ? 3'h5 : auto_out_2_d_bits_opcode; // @[CacheCork.scala 152:76 CacheCork.scala 153:27 CacheCork.scala 132:13]
  wire [2:0] d_d_2_bits_opcode = auto_out_2_d_bits_opcode == 3'h0 & ~auto_out_2_d_bits_source[0] ? 3'h6 : _GEN_78; // @[CacheCork.scala 156:73 CacheCork.scala 157:27]
  wire [2:0] _T_496 = muxStateEarly_5_0 ? d_d_2_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire  readys_5_1 = _readys_T_68[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_5_1 = readys_5_1 & out_18_earlyValid; // @[Arbiter.scala 97:79]
  reg  state_5_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_5_1 = idle_5 ? earlyWinner_5_1 : state_5_1; // @[Arbiter.scala 117:30]
  wire [2:0] out_18_bits_opcode = q_4_io_deq_bits_opcode; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_497 = muxStateEarly_5_1 ? out_18_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_499 = _T_496 | _T_497; // @[Mux.scala 27:72]
  wire  readys_5_2 = _readys_T_68[2]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_5_2 = readys_5_2 & out_19_earlyValid; // @[Arbiter.scala 97:79]
  reg  state_5_2; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_5_2 = idle_5 ? earlyWinner_5_2 : state_5_2; // @[Arbiter.scala 117:30]
  wire [2:0] out_19_bits_opcode = q_5_io_deq_bits_opcode; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_498 = muxStateEarly_5_2 ? out_19_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] sink_ACancel_5_bits_opcode = _T_499 | _T_498; // @[Mux.scala 27:72]
  wire  d_grant_2 = sink_ACancel_5_bits_opcode == 3'h5 | sink_ACancel_5_bits_opcode == 3'h4; // @[CacheCork.scala 123:54]
  wire  _in_d_ready_T_13 = pool_2_io_alloc_valid | ~d_first_2 | ~d_grant_2; // @[CacheCork.scala 126:70]
  wire  in_d_2_ready = auto_in_2_d_ready & _in_d_ready_T_13; // @[CacheCork.scala 126:34]
  wire  _sink_ACancel_earlyValid_T_32 = auto_out_2_d_valid | out_18_earlyValid | out_19_earlyValid; // @[Arbiter.scala 125:56]
  wire  _sink_ACancel_earlyValid_T_37 = state_5_0 & auto_out_2_d_valid | state_5_1 & out_18_earlyValid | state_5_2 &
    out_19_earlyValid; // @[Mux.scala 27:72]
  wire  sink_ACancel_5_earlyValid = idle_5 ? _sink_ACancel_earlyValid_T_32 : _sink_ACancel_earlyValid_T_37; // @[Arbiter.scala 125:29]
  wire  _d_first_T_2 = in_d_2_ready & sink_ACancel_5_earlyValid; // @[Decoupled.scala 40:37]
  wire [2:0] _T_486 = muxStateEarly_5_0 ? auto_out_2_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] out_18_bits_size = q_4_io_deq_bits_size; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_487 = muxStateEarly_5_1 ? out_18_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_489 = _T_486 | _T_487; // @[Mux.scala 27:72]
  wire [2:0] out_19_bits_size = q_5_io_deq_bits_size; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_488 = muxStateEarly_5_2 ? out_19_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] sink_ACancel_5_bits_size = _T_489 | _T_488; // @[Mux.scala 27:72]
  wire [12:0] _d_first_beats1_decode_T_9 = 13'h3f << sink_ACancel_5_bits_size; // @[package.scala 234:77]
  wire [5:0] _d_first_beats1_decode_T_11 = ~_d_first_beats1_decode_T_9[5:0]; // @[package.scala 234:46]
  wire [1:0] d_first_beats1_decode_2 = _d_first_beats1_decode_T_11[5:4]; // @[Edges.scala 219:59]
  wire  d_first_beats1_opdata_2 = sink_ACancel_5_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [1:0] d_first_counter1_2 = d_first_counter_2 - 2'h1; // @[Edges.scala 229:28]
  wire  bundleIn_2_d_valid = sink_ACancel_5_earlyValid & _in_d_ready_T_13; // @[CacheCork.scala 125:34]
  wire  _pool_io_alloc_ready_T_6 = auto_in_2_d_ready & bundleIn_2_d_valid; // @[Decoupled.scala 40:37]
  reg [2:0] bundleIn_2_d_bits_sink_r; // @[Reg.scala 15:16]
  wire [2:0] d_d_2_bits_source = auto_out_2_d_bits_source[3:1]; // @[CacheCork.scala 133:46]
  wire  opdata_7 = ~a_a_2_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [1:0] _T_358 = opdata_7 ? 2'h3 : 2'h0; // @[Edges.scala 220:14]
  wire  latch_4 = idle_4 & auto_out_2_a_ready; // @[Arbiter.scala 89:24]
  wire  earlyWinner_4_0 = readys_4_0 & c_a_2_valid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_4_1 = readys_4_1 & a_a_2_valid; // @[Arbiter.scala 97:79]
  wire  _T_369 = c_a_2_valid | a_a_2_valid; // @[Arbiter.scala 107:36]
  wire [1:0] maskedBeats_0_4 = earlyWinner_4_0 ? 2'h3 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_1_4 = earlyWinner_4_1 ? _T_358 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] initBeats_4 = maskedBeats_0_4 | maskedBeats_1_4; // @[Arbiter.scala 112:44]
  wire  muxStateEarly_4_0 = idle_4 ? earlyWinner_4_0 : state_4_0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_4_1 = idle_4 ? earlyWinner_4_1 : state_4_1; // @[Arbiter.scala 117:30]
  wire  _sink_ACancel_earlyValid_T_29 = state_4_0 & c_a_2_valid | state_4_1 & a_a_2_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_4_earlyValid = idle_4 ? _T_369 : _sink_ACancel_earlyValid_T_29; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_26 = auto_out_2_a_ready & sink_ACancel_4_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [1:0] _GEN_115 = {{1'd0}, _beatsLeft_T_26}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_28 = beatsLeft_4 - _GEN_115; // @[Arbiter.scala 113:52]
  wire [15:0] _T_389 = muxStateEarly_4_0 ? 16'hffff : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_390 = muxStateEarly_4_1 ? 16'hffff : 16'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_413 = muxStateEarly_4_0 ? auto_in_2_c_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_414 = muxStateEarly_4_1 ? auto_in_2_a_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_416 = muxStateEarly_4_0 ? c_a_bits_a_2_source : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_417 = muxStateEarly_4_1 ? a_a_2_bits_source : 4'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_419 = muxStateEarly_4_0 ? 3'h6 : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_420 = muxStateEarly_4_1 ? 3'h6 : 3'h0; // @[Mux.scala 27:72]
  wire [12:0] _decode_T_33 = 13'h3f << auto_out_2_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _decode_T_35 = ~_decode_T_33[5:0]; // @[package.scala 234:46]
  wire [1:0] decode_8 = _decode_T_35[5:4]; // @[Edges.scala 219:59]
  wire  opdata_8 = d_d_2_bits_opcode[0]; // @[Edges.scala 105:36]
  wire  latch_5 = idle_5 & in_d_2_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _GEN_116 = {{1'd0}, _d_first_T_2}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_34 = beatsLeft_5 - _GEN_116; // @[Arbiter.scala 113:52]
  wire  allowed_5_0 = idle_5 ? readys_5_0 : state_5_0; // @[Arbiter.scala 121:24]
  wire  allowed_5_1 = idle_5 ? readys_5_1 : state_5_1; // @[Arbiter.scala 121:24]
  wire  allowed_5_2 = idle_5 ? readys_5_2 : state_5_2; // @[Arbiter.scala 121:24]
  wire  out_18_bits_corrupt = q_4_io_deq_bits_corrupt; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  out_19_bits_corrupt = q_5_io_deq_bits_corrupt; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  _T_463 = muxStateEarly_5_2 & out_19_bits_corrupt; // @[Mux.scala 27:72]
  wire  _T_464 = muxStateEarly_5_0 & auto_out_2_d_bits_corrupt | muxStateEarly_5_1 & out_18_bits_corrupt; // @[Mux.scala 27:72]
  wire [127:0] _T_466 = muxStateEarly_5_0 ? auto_out_2_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] out_18_bits_data = q_4_io_deq_bits_data; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [127:0] _T_467 = muxStateEarly_5_1 ? out_18_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] out_19_bits_data = q_5_io_deq_bits_data; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [127:0] _T_468 = muxStateEarly_5_2 ? out_19_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_469 = _T_466 | _T_467; // @[Mux.scala 27:72]
  wire  out_18_bits_denied = q_4_io_deq_bits_denied; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  out_19_bits_denied = q_5_io_deq_bits_denied; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  _T_473 = muxStateEarly_5_2 & out_19_bits_denied; // @[Mux.scala 27:72]
  wire  _T_474 = muxStateEarly_5_0 & auto_out_2_d_bits_denied | muxStateEarly_5_1 & out_18_bits_denied; // @[Mux.scala 27:72]
  wire [2:0] _T_481 = muxStateEarly_5_0 ? d_d_2_bits_source : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] out_18_bits_source = q_4_io_deq_bits_source; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_482 = muxStateEarly_5_1 ? out_18_bits_source : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] out_19_bits_source = q_5_io_deq_bits_source; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_483 = muxStateEarly_5_2 ? out_19_bits_source : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_484 = _T_481 | _T_482; // @[Mux.scala 27:72]
  wire [1:0] out_18_bits_param = q_4_io_deq_bits_param; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [1:0] _T_492 = muxStateEarly_5_1 ? out_18_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] out_19_bits_param = q_5_io_deq_bits_param; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [1:0] _T_493 = muxStateEarly_5_2 ? out_19_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire  isPut_3 = auto_in_3_a_bits_opcode == 3'h0 | auto_in_3_a_bits_opcode == 3'h1; // @[CacheCork.scala 66:54]
  wire  _toD_T_12 = auto_in_3_a_bits_opcode == 3'h6; // @[CacheCork.scala 67:37]
  wire  _toD_T_15 = auto_in_3_a_bits_opcode == 3'h7; // @[CacheCork.scala 68:37]
  wire  toD_3 = auto_in_3_a_bits_opcode == 3'h6 & auto_in_3_a_bits_param == 3'h2 | _toD_T_15; // @[CacheCork.scala 67:97]
  wire  a_d_3_ready = q_7_io_enq_ready; // @[CacheCork.scala 65:23 Decoupled.scala 299:17]
  reg [1:0] beatsLeft_6; // @[Arbiter.scala 87:30]
  wire  idle_6 = beatsLeft_6 == 2'h0; // @[Arbiter.scala 88:28]
  wire  a_a_3_valid = auto_in_3_a_valid & ~toD_3; // @[CacheCork.scala 71:33]
  wire  _c_a_valid_T_6 = auto_in_3_c_bits_opcode == 3'h7; // @[CacheCork.scala 92:53]
  wire  c_a_3_valid = auto_in_3_c_valid & _c_a_valid_T_6; // @[CacheCork.scala 92:33]
  wire [1:0] _readys_T_72 = {a_a_3_valid,c_a_3_valid}; // @[Cat.scala 30:58]
  wire [2:0] _readys_T_73 = {_readys_T_72, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _readys_T_75 = _readys_T_72 | _readys_T_73[1:0]; // @[package.scala 244:43]
  wire [2:0] _readys_T_77 = {_readys_T_75, 1'h0}; // @[Arbiter.scala 16:78]
  wire [1:0] _readys_T_79 = ~_readys_T_77[1:0]; // @[Arbiter.scala 16:61]
  wire  readys_6_1 = _readys_T_79[1]; // @[Arbiter.scala 95:86]
  reg  state_6_1; // @[Arbiter.scala 116:26]
  wire  allowed_6_1 = idle_6 ? readys_6_1 : state_6_1; // @[Arbiter.scala 121:24]
  wire  out_22_ready = auto_out_3_a_ready & allowed_6_1; // @[Arbiter.scala 123:31]
  wire [3:0] _a_a_bits_source_T_15 = {auto_in_3_a_bits_source, 1'h0}; // @[CacheCork.scala 73:45]
  wire [3:0] _GEN_117 = {{3'd0}, isPut_3}; // @[CacheCork.scala 73:50]
  wire [3:0] _a_a_bits_source_T_17 = _a_a_bits_source_T_15 | _GEN_117; // @[CacheCork.scala 73:50]
  wire [3:0] _a_a_bits_source_T_19 = _a_a_bits_source_T_15 | 4'h1; // @[CacheCork.scala 79:52]
  wire [2:0] a_a_3_bits_opcode = _toD_T_12 | _toD_T_15 ? 3'h4 : auto_in_3_a_bits_opcode; // @[CacheCork.scala 76:86 CacheCork.scala 77:27 CacheCork.scala 72:18]
  wire [2:0] a_a_3_bits_param = _toD_T_12 | _toD_T_15 ? 3'h0 : auto_in_3_a_bits_param; // @[CacheCork.scala 76:86 CacheCork.scala 78:27 CacheCork.scala 72:18]
  wire [3:0] a_a_3_bits_source = _toD_T_12 | _toD_T_15 ? _a_a_bits_source_T_19 : _a_a_bits_source_T_17; // @[CacheCork.scala 76:86 CacheCork.scala 79:27 CacheCork.scala 73:25]
  wire [3:0] c_a_bits_a_3_source = {auto_in_3_c_bits_source, 1'h0}; // @[CacheCork.scala 94:41]
  wire  _c_d_valid_T_6 = auto_in_3_c_bits_opcode == 3'h6; // @[CacheCork.scala 103:53]
  wire  c_d_3_ready = q_6_io_enq_ready; // @[CacheCork.scala 102:23 Decoupled.scala 299:17]
  wire  readys_6_0 = _readys_T_79[0]; // @[Arbiter.scala 95:86]
  reg  state_6_0; // @[Arbiter.scala 116:26]
  wire  allowed_6_0 = idle_6 ? readys_6_0 : state_6_0; // @[Arbiter.scala 121:24]
  wire  out_21_ready = auto_out_3_a_ready & allowed_6_0; // @[Arbiter.scala 123:31]
  reg [1:0] d_first_counter_3; // @[Edges.scala 228:27]
  wire  d_first_3 = d_first_counter_3 == 2'h0; // @[Edges.scala 230:25]
  reg [1:0] beatsLeft_7; // @[Arbiter.scala 87:30]
  wire  idle_7 = beatsLeft_7 == 2'h0; // @[Arbiter.scala 88:28]
  wire  out_26_earlyValid = q_7_io_deq_valid; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 71:20]
  wire  out_25_earlyValid = q_6_io_deq_valid; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 71:20]
  wire [2:0] _readys_T_82 = {out_26_earlyValid,out_25_earlyValid,auto_out_3_d_valid}; // @[Cat.scala 30:58]
  wire [3:0] _readys_T_83 = {_readys_T_82, 1'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_T_85 = _readys_T_82 | _readys_T_83[2:0]; // @[package.scala 244:43]
  wire [4:0] _readys_T_86 = {_readys_T_85, 2'h0}; // @[package.scala 244:48]
  wire [2:0] _readys_T_88 = _readys_T_85 | _readys_T_86[2:0]; // @[package.scala 244:43]
  wire [3:0] _readys_T_90 = {_readys_T_88, 1'h0}; // @[Arbiter.scala 16:78]
  wire [2:0] _readys_T_92 = ~_readys_T_90[2:0]; // @[Arbiter.scala 16:61]
  wire  readys_7_0 = _readys_T_92[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_7_0 = readys_7_0 & auto_out_3_d_valid; // @[Arbiter.scala 97:79]
  reg  state_7_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_7_0 = idle_7 ? earlyWinner_7_0 : state_7_0; // @[Arbiter.scala 117:30]
  wire [2:0] _GEN_105 = auto_out_3_d_bits_opcode == 3'h1 & auto_out_3_d_bits_source[0] ? 3'h5 : auto_out_3_d_bits_opcode
    ; // @[CacheCork.scala 152:76 CacheCork.scala 153:27 CacheCork.scala 132:13]
  wire [2:0] d_d_3_bits_opcode = auto_out_3_d_bits_opcode == 3'h0 & ~auto_out_3_d_bits_source[0] ? 3'h6 : _GEN_105; // @[CacheCork.scala 156:73 CacheCork.scala 157:27]
  wire [2:0] _T_663 = muxStateEarly_7_0 ? d_d_3_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire  readys_7_1 = _readys_T_92[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_7_1 = readys_7_1 & out_25_earlyValid; // @[Arbiter.scala 97:79]
  reg  state_7_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_7_1 = idle_7 ? earlyWinner_7_1 : state_7_1; // @[Arbiter.scala 117:30]
  wire [2:0] out_25_bits_opcode = q_6_io_deq_bits_opcode; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_664 = muxStateEarly_7_1 ? out_25_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_666 = _T_663 | _T_664; // @[Mux.scala 27:72]
  wire  readys_7_2 = _readys_T_92[2]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_7_2 = readys_7_2 & out_26_earlyValid; // @[Arbiter.scala 97:79]
  reg  state_7_2; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_7_2 = idle_7 ? earlyWinner_7_2 : state_7_2; // @[Arbiter.scala 117:30]
  wire [2:0] out_26_bits_opcode = q_7_io_deq_bits_opcode; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_665 = muxStateEarly_7_2 ? out_26_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] sink_ACancel_7_bits_opcode = _T_666 | _T_665; // @[Mux.scala 27:72]
  wire  d_grant_3 = sink_ACancel_7_bits_opcode == 3'h5 | sink_ACancel_7_bits_opcode == 3'h4; // @[CacheCork.scala 123:54]
  wire  _in_d_ready_T_18 = pool_3_io_alloc_valid | ~d_first_3 | ~d_grant_3; // @[CacheCork.scala 126:70]
  wire  in_d_3_ready = auto_in_3_d_ready & _in_d_ready_T_18; // @[CacheCork.scala 126:34]
  wire  _sink_ACancel_earlyValid_T_45 = auto_out_3_d_valid | out_25_earlyValid | out_26_earlyValid; // @[Arbiter.scala 125:56]
  wire  _sink_ACancel_earlyValid_T_50 = state_7_0 & auto_out_3_d_valid | state_7_1 & out_25_earlyValid | state_7_2 &
    out_26_earlyValid; // @[Mux.scala 27:72]
  wire  sink_ACancel_7_earlyValid = idle_7 ? _sink_ACancel_earlyValid_T_45 : _sink_ACancel_earlyValid_T_50; // @[Arbiter.scala 125:29]
  wire  _d_first_T_3 = in_d_3_ready & sink_ACancel_7_earlyValid; // @[Decoupled.scala 40:37]
  wire [2:0] _T_653 = muxStateEarly_7_0 ? auto_out_3_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] out_25_bits_size = q_6_io_deq_bits_size; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_654 = muxStateEarly_7_1 ? out_25_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_656 = _T_653 | _T_654; // @[Mux.scala 27:72]
  wire [2:0] out_26_bits_size = q_7_io_deq_bits_size; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_655 = muxStateEarly_7_2 ? out_26_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] sink_ACancel_7_bits_size = _T_656 | _T_655; // @[Mux.scala 27:72]
  wire [12:0] _d_first_beats1_decode_T_13 = 13'h3f << sink_ACancel_7_bits_size; // @[package.scala 234:77]
  wire [5:0] _d_first_beats1_decode_T_15 = ~_d_first_beats1_decode_T_13[5:0]; // @[package.scala 234:46]
  wire [1:0] d_first_beats1_decode_3 = _d_first_beats1_decode_T_15[5:4]; // @[Edges.scala 219:59]
  wire  d_first_beats1_opdata_3 = sink_ACancel_7_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [1:0] d_first_counter1_3 = d_first_counter_3 - 2'h1; // @[Edges.scala 229:28]
  wire  bundleIn_3_d_valid = sink_ACancel_7_earlyValid & _in_d_ready_T_18; // @[CacheCork.scala 125:34]
  wire  _pool_io_alloc_ready_T_9 = auto_in_3_d_ready & bundleIn_3_d_valid; // @[Decoupled.scala 40:37]
  reg [2:0] bundleIn_3_d_bits_sink_r; // @[Reg.scala 15:16]
  wire [2:0] d_d_3_bits_source = auto_out_3_d_bits_source[3:1]; // @[CacheCork.scala 133:46]
  wire  opdata_10 = ~a_a_3_bits_opcode[2]; // @[Edges.scala 91:28]
  wire [1:0] _T_525 = opdata_10 ? 2'h3 : 2'h0; // @[Edges.scala 220:14]
  wire  latch_6 = idle_6 & auto_out_3_a_ready; // @[Arbiter.scala 89:24]
  wire  earlyWinner_6_0 = readys_6_0 & c_a_3_valid; // @[Arbiter.scala 97:79]
  wire  earlyWinner_6_1 = readys_6_1 & a_a_3_valid; // @[Arbiter.scala 97:79]
  wire  _T_536 = c_a_3_valid | a_a_3_valid; // @[Arbiter.scala 107:36]
  wire [1:0] maskedBeats_0_6 = earlyWinner_6_0 ? 2'h3 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] maskedBeats_1_6 = earlyWinner_6_1 ? _T_525 : 2'h0; // @[Arbiter.scala 111:73]
  wire [1:0] initBeats_6 = maskedBeats_0_6 | maskedBeats_1_6; // @[Arbiter.scala 112:44]
  wire  muxStateEarly_6_0 = idle_6 ? earlyWinner_6_0 : state_6_0; // @[Arbiter.scala 117:30]
  wire  muxStateEarly_6_1 = idle_6 ? earlyWinner_6_1 : state_6_1; // @[Arbiter.scala 117:30]
  wire  _sink_ACancel_earlyValid_T_42 = state_6_0 & c_a_3_valid | state_6_1 & a_a_3_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_6_earlyValid = idle_6 ? _T_536 : _sink_ACancel_earlyValid_T_42; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_38 = auto_out_3_a_ready & sink_ACancel_6_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [1:0] _GEN_118 = {{1'd0}, _beatsLeft_T_38}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_40 = beatsLeft_6 - _GEN_118; // @[Arbiter.scala 113:52]
  wire [15:0] _T_556 = muxStateEarly_6_0 ? 16'hffff : 16'h0; // @[Mux.scala 27:72]
  wire [15:0] _T_557 = muxStateEarly_6_1 ? 16'hffff : 16'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_580 = muxStateEarly_6_0 ? auto_in_3_c_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [35:0] _T_581 = muxStateEarly_6_1 ? auto_in_3_a_bits_address : 36'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_583 = muxStateEarly_6_0 ? c_a_bits_a_3_source : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_584 = muxStateEarly_6_1 ? a_a_3_bits_source : 4'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_586 = muxStateEarly_6_0 ? 3'h6 : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_587 = muxStateEarly_6_1 ? 3'h6 : 3'h0; // @[Mux.scala 27:72]
  wire [12:0] _decode_T_45 = 13'h3f << auto_out_3_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _decode_T_47 = ~_decode_T_45[5:0]; // @[package.scala 234:46]
  wire [1:0] decode_11 = _decode_T_47[5:4]; // @[Edges.scala 219:59]
  wire  opdata_11 = d_d_3_bits_opcode[0]; // @[Edges.scala 105:36]
  wire  latch_7 = idle_7 & in_d_3_ready; // @[Arbiter.scala 89:24]
  wire [1:0] _GEN_119 = {{1'd0}, _d_first_T_3}; // @[Arbiter.scala 113:52]
  wire [1:0] _beatsLeft_T_46 = beatsLeft_7 - _GEN_119; // @[Arbiter.scala 113:52]
  wire  allowed_7_0 = idle_7 ? readys_7_0 : state_7_0; // @[Arbiter.scala 121:24]
  wire  allowed_7_1 = idle_7 ? readys_7_1 : state_7_1; // @[Arbiter.scala 121:24]
  wire  allowed_7_2 = idle_7 ? readys_7_2 : state_7_2; // @[Arbiter.scala 121:24]
  wire  out_25_bits_corrupt = q_6_io_deq_bits_corrupt; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  out_26_bits_corrupt = q_7_io_deq_bits_corrupt; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  _T_630 = muxStateEarly_7_2 & out_26_bits_corrupt; // @[Mux.scala 27:72]
  wire  _T_631 = muxStateEarly_7_0 & auto_out_3_d_bits_corrupt | muxStateEarly_7_1 & out_25_bits_corrupt; // @[Mux.scala 27:72]
  wire [127:0] _T_633 = muxStateEarly_7_0 ? auto_out_3_d_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] out_25_bits_data = q_6_io_deq_bits_data; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [127:0] _T_634 = muxStateEarly_7_1 ? out_25_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] out_26_bits_data = q_7_io_deq_bits_data; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [127:0] _T_635 = muxStateEarly_7_2 ? out_26_bits_data : 128'h0; // @[Mux.scala 27:72]
  wire [127:0] _T_636 = _T_633 | _T_634; // @[Mux.scala 27:72]
  wire  out_25_bits_denied = q_6_io_deq_bits_denied; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  out_26_bits_denied = q_7_io_deq_bits_denied; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire  _T_640 = muxStateEarly_7_2 & out_26_bits_denied; // @[Mux.scala 27:72]
  wire  _T_641 = muxStateEarly_7_0 & auto_out_3_d_bits_denied | muxStateEarly_7_1 & out_25_bits_denied; // @[Mux.scala 27:72]
  wire [2:0] _T_648 = muxStateEarly_7_0 ? d_d_3_bits_source : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] out_25_bits_source = q_6_io_deq_bits_source; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_649 = muxStateEarly_7_1 ? out_25_bits_source : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] out_26_bits_source = q_7_io_deq_bits_source; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [2:0] _T_650 = muxStateEarly_7_2 ? out_26_bits_source : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_651 = _T_648 | _T_649; // @[Mux.scala 27:72]
  wire [1:0] out_25_bits_param = q_6_io_deq_bits_param; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [1:0] _T_659 = muxStateEarly_7_1 ? out_25_bits_param : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] out_26_bits_param = q_7_io_deq_bits_param; // @[ReadyValidCancel.scala 70:19 ReadyValidCancel.scala 73:14]
  wire [1:0] _T_660 = muxStateEarly_7_2 ? out_26_bits_param : 2'h0; // @[Mux.scala 27:72]
  RHEA__IDPool pool ( // @[CacheCork.scala 117:26]
    .rf_reset(pool_rf_reset),
    .clock(pool_clock),
    .reset(pool_reset),
    .io_free_valid(pool_io_free_valid),
    .io_free_bits(pool_io_free_bits),
    .io_alloc_ready(pool_io_alloc_ready),
    .io_alloc_valid(pool_io_alloc_valid),
    .io_alloc_bits(pool_io_alloc_bits)
  );
  RHEA__Queue_125 q ( // @[Decoupled.scala 296:21]
    .rf_reset(q_rf_reset),
    .clock(q_clock),
    .reset(q_reset),
    .io_enq_ready(q_io_enq_ready),
    .io_enq_valid(q_io_enq_valid),
    .io_enq_bits_opcode(q_io_enq_bits_opcode),
    .io_enq_bits_param(q_io_enq_bits_param),
    .io_enq_bits_size(q_io_enq_bits_size),
    .io_enq_bits_source(q_io_enq_bits_source),
    .io_enq_bits_sink(q_io_enq_bits_sink),
    .io_enq_bits_denied(q_io_enq_bits_denied),
    .io_enq_bits_data(q_io_enq_bits_data),
    .io_enq_bits_corrupt(q_io_enq_bits_corrupt),
    .io_deq_ready(q_io_deq_ready),
    .io_deq_valid(q_io_deq_valid),
    .io_deq_bits_opcode(q_io_deq_bits_opcode),
    .io_deq_bits_param(q_io_deq_bits_param),
    .io_deq_bits_size(q_io_deq_bits_size),
    .io_deq_bits_source(q_io_deq_bits_source),
    .io_deq_bits_sink(q_io_deq_bits_sink),
    .io_deq_bits_denied(q_io_deq_bits_denied),
    .io_deq_bits_data(q_io_deq_bits_data),
    .io_deq_bits_corrupt(q_io_deq_bits_corrupt)
  );
  RHEA__Queue_125 q_1 ( // @[Decoupled.scala 296:21]
    .rf_reset(q_1_rf_reset),
    .clock(q_1_clock),
    .reset(q_1_reset),
    .io_enq_ready(q_1_io_enq_ready),
    .io_enq_valid(q_1_io_enq_valid),
    .io_enq_bits_opcode(q_1_io_enq_bits_opcode),
    .io_enq_bits_param(q_1_io_enq_bits_param),
    .io_enq_bits_size(q_1_io_enq_bits_size),
    .io_enq_bits_source(q_1_io_enq_bits_source),
    .io_enq_bits_sink(q_1_io_enq_bits_sink),
    .io_enq_bits_denied(q_1_io_enq_bits_denied),
    .io_enq_bits_data(q_1_io_enq_bits_data),
    .io_enq_bits_corrupt(q_1_io_enq_bits_corrupt),
    .io_deq_ready(q_1_io_deq_ready),
    .io_deq_valid(q_1_io_deq_valid),
    .io_deq_bits_opcode(q_1_io_deq_bits_opcode),
    .io_deq_bits_param(q_1_io_deq_bits_param),
    .io_deq_bits_size(q_1_io_deq_bits_size),
    .io_deq_bits_source(q_1_io_deq_bits_source),
    .io_deq_bits_sink(q_1_io_deq_bits_sink),
    .io_deq_bits_denied(q_1_io_deq_bits_denied),
    .io_deq_bits_data(q_1_io_deq_bits_data),
    .io_deq_bits_corrupt(q_1_io_deq_bits_corrupt)
  );
  RHEA__IDPool pool_1 ( // @[CacheCork.scala 117:26]
    .rf_reset(pool_1_rf_reset),
    .clock(pool_1_clock),
    .reset(pool_1_reset),
    .io_free_valid(pool_1_io_free_valid),
    .io_free_bits(pool_1_io_free_bits),
    .io_alloc_ready(pool_1_io_alloc_ready),
    .io_alloc_valid(pool_1_io_alloc_valid),
    .io_alloc_bits(pool_1_io_alloc_bits)
  );
  RHEA__Queue_125 q_2 ( // @[Decoupled.scala 296:21]
    .rf_reset(q_2_rf_reset),
    .clock(q_2_clock),
    .reset(q_2_reset),
    .io_enq_ready(q_2_io_enq_ready),
    .io_enq_valid(q_2_io_enq_valid),
    .io_enq_bits_opcode(q_2_io_enq_bits_opcode),
    .io_enq_bits_param(q_2_io_enq_bits_param),
    .io_enq_bits_size(q_2_io_enq_bits_size),
    .io_enq_bits_source(q_2_io_enq_bits_source),
    .io_enq_bits_sink(q_2_io_enq_bits_sink),
    .io_enq_bits_denied(q_2_io_enq_bits_denied),
    .io_enq_bits_data(q_2_io_enq_bits_data),
    .io_enq_bits_corrupt(q_2_io_enq_bits_corrupt),
    .io_deq_ready(q_2_io_deq_ready),
    .io_deq_valid(q_2_io_deq_valid),
    .io_deq_bits_opcode(q_2_io_deq_bits_opcode),
    .io_deq_bits_param(q_2_io_deq_bits_param),
    .io_deq_bits_size(q_2_io_deq_bits_size),
    .io_deq_bits_source(q_2_io_deq_bits_source),
    .io_deq_bits_sink(q_2_io_deq_bits_sink),
    .io_deq_bits_denied(q_2_io_deq_bits_denied),
    .io_deq_bits_data(q_2_io_deq_bits_data),
    .io_deq_bits_corrupt(q_2_io_deq_bits_corrupt)
  );
  RHEA__Queue_125 q_3 ( // @[Decoupled.scala 296:21]
    .rf_reset(q_3_rf_reset),
    .clock(q_3_clock),
    .reset(q_3_reset),
    .io_enq_ready(q_3_io_enq_ready),
    .io_enq_valid(q_3_io_enq_valid),
    .io_enq_bits_opcode(q_3_io_enq_bits_opcode),
    .io_enq_bits_param(q_3_io_enq_bits_param),
    .io_enq_bits_size(q_3_io_enq_bits_size),
    .io_enq_bits_source(q_3_io_enq_bits_source),
    .io_enq_bits_sink(q_3_io_enq_bits_sink),
    .io_enq_bits_denied(q_3_io_enq_bits_denied),
    .io_enq_bits_data(q_3_io_enq_bits_data),
    .io_enq_bits_corrupt(q_3_io_enq_bits_corrupt),
    .io_deq_ready(q_3_io_deq_ready),
    .io_deq_valid(q_3_io_deq_valid),
    .io_deq_bits_opcode(q_3_io_deq_bits_opcode),
    .io_deq_bits_param(q_3_io_deq_bits_param),
    .io_deq_bits_size(q_3_io_deq_bits_size),
    .io_deq_bits_source(q_3_io_deq_bits_source),
    .io_deq_bits_sink(q_3_io_deq_bits_sink),
    .io_deq_bits_denied(q_3_io_deq_bits_denied),
    .io_deq_bits_data(q_3_io_deq_bits_data),
    .io_deq_bits_corrupt(q_3_io_deq_bits_corrupt)
  );
  RHEA__IDPool pool_2 ( // @[CacheCork.scala 117:26]
    .rf_reset(pool_2_rf_reset),
    .clock(pool_2_clock),
    .reset(pool_2_reset),
    .io_free_valid(pool_2_io_free_valid),
    .io_free_bits(pool_2_io_free_bits),
    .io_alloc_ready(pool_2_io_alloc_ready),
    .io_alloc_valid(pool_2_io_alloc_valid),
    .io_alloc_bits(pool_2_io_alloc_bits)
  );
  RHEA__Queue_125 q_4 ( // @[Decoupled.scala 296:21]
    .rf_reset(q_4_rf_reset),
    .clock(q_4_clock),
    .reset(q_4_reset),
    .io_enq_ready(q_4_io_enq_ready),
    .io_enq_valid(q_4_io_enq_valid),
    .io_enq_bits_opcode(q_4_io_enq_bits_opcode),
    .io_enq_bits_param(q_4_io_enq_bits_param),
    .io_enq_bits_size(q_4_io_enq_bits_size),
    .io_enq_bits_source(q_4_io_enq_bits_source),
    .io_enq_bits_sink(q_4_io_enq_bits_sink),
    .io_enq_bits_denied(q_4_io_enq_bits_denied),
    .io_enq_bits_data(q_4_io_enq_bits_data),
    .io_enq_bits_corrupt(q_4_io_enq_bits_corrupt),
    .io_deq_ready(q_4_io_deq_ready),
    .io_deq_valid(q_4_io_deq_valid),
    .io_deq_bits_opcode(q_4_io_deq_bits_opcode),
    .io_deq_bits_param(q_4_io_deq_bits_param),
    .io_deq_bits_size(q_4_io_deq_bits_size),
    .io_deq_bits_source(q_4_io_deq_bits_source),
    .io_deq_bits_sink(q_4_io_deq_bits_sink),
    .io_deq_bits_denied(q_4_io_deq_bits_denied),
    .io_deq_bits_data(q_4_io_deq_bits_data),
    .io_deq_bits_corrupt(q_4_io_deq_bits_corrupt)
  );
  RHEA__Queue_125 q_5 ( // @[Decoupled.scala 296:21]
    .rf_reset(q_5_rf_reset),
    .clock(q_5_clock),
    .reset(q_5_reset),
    .io_enq_ready(q_5_io_enq_ready),
    .io_enq_valid(q_5_io_enq_valid),
    .io_enq_bits_opcode(q_5_io_enq_bits_opcode),
    .io_enq_bits_param(q_5_io_enq_bits_param),
    .io_enq_bits_size(q_5_io_enq_bits_size),
    .io_enq_bits_source(q_5_io_enq_bits_source),
    .io_enq_bits_sink(q_5_io_enq_bits_sink),
    .io_enq_bits_denied(q_5_io_enq_bits_denied),
    .io_enq_bits_data(q_5_io_enq_bits_data),
    .io_enq_bits_corrupt(q_5_io_enq_bits_corrupt),
    .io_deq_ready(q_5_io_deq_ready),
    .io_deq_valid(q_5_io_deq_valid),
    .io_deq_bits_opcode(q_5_io_deq_bits_opcode),
    .io_deq_bits_param(q_5_io_deq_bits_param),
    .io_deq_bits_size(q_5_io_deq_bits_size),
    .io_deq_bits_source(q_5_io_deq_bits_source),
    .io_deq_bits_sink(q_5_io_deq_bits_sink),
    .io_deq_bits_denied(q_5_io_deq_bits_denied),
    .io_deq_bits_data(q_5_io_deq_bits_data),
    .io_deq_bits_corrupt(q_5_io_deq_bits_corrupt)
  );
  RHEA__IDPool pool_3 ( // @[CacheCork.scala 117:26]
    .rf_reset(pool_3_rf_reset),
    .clock(pool_3_clock),
    .reset(pool_3_reset),
    .io_free_valid(pool_3_io_free_valid),
    .io_free_bits(pool_3_io_free_bits),
    .io_alloc_ready(pool_3_io_alloc_ready),
    .io_alloc_valid(pool_3_io_alloc_valid),
    .io_alloc_bits(pool_3_io_alloc_bits)
  );
  RHEA__Queue_125 q_6 ( // @[Decoupled.scala 296:21]
    .rf_reset(q_6_rf_reset),
    .clock(q_6_clock),
    .reset(q_6_reset),
    .io_enq_ready(q_6_io_enq_ready),
    .io_enq_valid(q_6_io_enq_valid),
    .io_enq_bits_opcode(q_6_io_enq_bits_opcode),
    .io_enq_bits_param(q_6_io_enq_bits_param),
    .io_enq_bits_size(q_6_io_enq_bits_size),
    .io_enq_bits_source(q_6_io_enq_bits_source),
    .io_enq_bits_sink(q_6_io_enq_bits_sink),
    .io_enq_bits_denied(q_6_io_enq_bits_denied),
    .io_enq_bits_data(q_6_io_enq_bits_data),
    .io_enq_bits_corrupt(q_6_io_enq_bits_corrupt),
    .io_deq_ready(q_6_io_deq_ready),
    .io_deq_valid(q_6_io_deq_valid),
    .io_deq_bits_opcode(q_6_io_deq_bits_opcode),
    .io_deq_bits_param(q_6_io_deq_bits_param),
    .io_deq_bits_size(q_6_io_deq_bits_size),
    .io_deq_bits_source(q_6_io_deq_bits_source),
    .io_deq_bits_sink(q_6_io_deq_bits_sink),
    .io_deq_bits_denied(q_6_io_deq_bits_denied),
    .io_deq_bits_data(q_6_io_deq_bits_data),
    .io_deq_bits_corrupt(q_6_io_deq_bits_corrupt)
  );
  RHEA__Queue_125 q_7 ( // @[Decoupled.scala 296:21]
    .rf_reset(q_7_rf_reset),
    .clock(q_7_clock),
    .reset(q_7_reset),
    .io_enq_ready(q_7_io_enq_ready),
    .io_enq_valid(q_7_io_enq_valid),
    .io_enq_bits_opcode(q_7_io_enq_bits_opcode),
    .io_enq_bits_param(q_7_io_enq_bits_param),
    .io_enq_bits_size(q_7_io_enq_bits_size),
    .io_enq_bits_source(q_7_io_enq_bits_source),
    .io_enq_bits_sink(q_7_io_enq_bits_sink),
    .io_enq_bits_denied(q_7_io_enq_bits_denied),
    .io_enq_bits_data(q_7_io_enq_bits_data),
    .io_enq_bits_corrupt(q_7_io_enq_bits_corrupt),
    .io_deq_ready(q_7_io_deq_ready),
    .io_deq_valid(q_7_io_deq_valid),
    .io_deq_bits_opcode(q_7_io_deq_bits_opcode),
    .io_deq_bits_param(q_7_io_deq_bits_param),
    .io_deq_bits_size(q_7_io_deq_bits_size),
    .io_deq_bits_source(q_7_io_deq_bits_source),
    .io_deq_bits_sink(q_7_io_deq_bits_sink),
    .io_deq_bits_denied(q_7_io_deq_bits_denied),
    .io_deq_bits_data(q_7_io_deq_bits_data),
    .io_deq_bits_corrupt(q_7_io_deq_bits_corrupt)
  );
  assign pool_rf_reset = rf_reset;
  assign q_rf_reset = rf_reset;
  assign q_1_rf_reset = rf_reset;
  assign pool_1_rf_reset = rf_reset;
  assign q_2_rf_reset = rf_reset;
  assign q_3_rf_reset = rf_reset;
  assign pool_2_rf_reset = rf_reset;
  assign q_4_rf_reset = rf_reset;
  assign q_5_rf_reset = rf_reset;
  assign pool_3_rf_reset = rf_reset;
  assign q_6_rf_reset = rf_reset;
  assign q_7_rf_reset = rf_reset;
  assign auto_in_3_a_ready = toD_3 ? a_d_3_ready : out_22_ready; // @[CacheCork.scala 69:26]
  assign auto_in_3_c_ready = _c_d_valid_T_6 ? c_d_3_ready : out_21_ready; // @[CacheCork.scala 107:26]
  assign auto_in_3_d_valid = sink_ACancel_7_earlyValid & _in_d_ready_T_18; // @[CacheCork.scala 125:34]
  assign auto_in_3_d_bits_opcode = _T_666 | _T_665; // @[Mux.scala 27:72]
  assign auto_in_3_d_bits_param = _T_659 | _T_660; // @[Mux.scala 27:72]
  assign auto_in_3_d_bits_size = _T_656 | _T_655; // @[Mux.scala 27:72]
  assign auto_in_3_d_bits_source = _T_651 | _T_650; // @[Mux.scala 27:72]
  assign auto_in_3_d_bits_sink = d_first_3 ? pool_3_io_alloc_bits : bundleIn_3_d_bits_sink_r; // @[package.scala 79:42]
  assign auto_in_3_d_bits_denied = _T_641 | _T_640; // @[Mux.scala 27:72]
  assign auto_in_3_d_bits_data = _T_636 | _T_635; // @[Mux.scala 27:72]
  assign auto_in_3_d_bits_corrupt = _T_631 | _T_630; // @[Mux.scala 27:72]
  assign auto_in_2_a_ready = toD_2 ? a_d_2_ready : out_15_ready; // @[CacheCork.scala 69:26]
  assign auto_in_2_c_ready = _c_d_valid_T_4 ? c_d_2_ready : out_14_ready; // @[CacheCork.scala 107:26]
  assign auto_in_2_d_valid = sink_ACancel_5_earlyValid & _in_d_ready_T_13; // @[CacheCork.scala 125:34]
  assign auto_in_2_d_bits_opcode = _T_499 | _T_498; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_param = _T_492 | _T_493; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_size = _T_489 | _T_488; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_source = _T_484 | _T_483; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_sink = d_first_2 ? pool_2_io_alloc_bits : bundleIn_2_d_bits_sink_r; // @[package.scala 79:42]
  assign auto_in_2_d_bits_denied = _T_474 | _T_473; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_data = _T_469 | _T_468; // @[Mux.scala 27:72]
  assign auto_in_2_d_bits_corrupt = _T_464 | _T_463; // @[Mux.scala 27:72]
  assign auto_in_1_a_ready = toD_1 ? a_d_1_ready : out_8_ready; // @[CacheCork.scala 69:26]
  assign auto_in_1_c_ready = _c_d_valid_T_2 ? c_d_1_ready : out_7_ready; // @[CacheCork.scala 107:26]
  assign auto_in_1_d_valid = sink_ACancel_3_earlyValid & _in_d_ready_T_8; // @[CacheCork.scala 125:34]
  assign auto_in_1_d_bits_opcode = _T_332 | _T_331; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_param = _T_325 | _T_326; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_size = _T_322 | _T_321; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_source = _T_317 | _T_316; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_sink = d_first_1 ? pool_1_io_alloc_bits : bundleIn_1_d_bits_sink_r; // @[package.scala 79:42]
  assign auto_in_1_d_bits_denied = _T_307 | _T_306; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_data = _T_302 | _T_301; // @[Mux.scala 27:72]
  assign auto_in_1_d_bits_corrupt = _T_297 | _T_296; // @[Mux.scala 27:72]
  assign auto_in_0_a_ready = toD ? a_d_ready : out_1_ready; // @[CacheCork.scala 69:26]
  assign auto_in_0_c_ready = _c_d_valid_T ? c_d_ready : out_ready; // @[CacheCork.scala 107:26]
  assign auto_in_0_d_valid = sink_ACancel_1_earlyValid & _in_d_ready_T_3; // @[CacheCork.scala 125:34]
  assign auto_in_0_d_bits_opcode = _T_165 | _T_164; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_param = _T_158 | _T_159; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_size = _T_155 | _T_154; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_source = _T_150 | _T_149; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_sink = d_first ? pool_io_alloc_bits : bundleIn_0_d_bits_sink_r; // @[package.scala 79:42]
  assign auto_in_0_d_bits_denied = _T_140 | _T_139; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_data = _T_135 | _T_134; // @[Mux.scala 27:72]
  assign auto_in_0_d_bits_corrupt = _T_130 | _T_129; // @[Mux.scala 27:72]
  assign auto_out_3_a_valid = idle_6 ? _T_536 : _sink_ACancel_earlyValid_T_42; // @[Arbiter.scala 125:29]
  assign auto_out_3_a_bits_opcode = muxStateEarly_6_1 ? a_a_3_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_param = muxStateEarly_6_1 ? a_a_3_bits_param : 3'h0; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_size = _T_586 | _T_587; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_source = _T_583 | _T_584; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_address = _T_580 | _T_581; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_user_amba_prot_bufferable = muxStateEarly_6_0 | muxStateEarly_6_1; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_user_amba_prot_modifiable = muxStateEarly_6_0 | muxStateEarly_6_1; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_user_amba_prot_readalloc = muxStateEarly_6_0 | muxStateEarly_6_1; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_user_amba_prot_writealloc = muxStateEarly_6_0 | muxStateEarly_6_1; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_user_amba_prot_privileged = muxStateEarly_6_0 | muxStateEarly_6_1; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_user_amba_prot_secure = muxStateEarly_6_0 | muxStateEarly_6_1; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_mask = _T_556 | _T_557; // @[Mux.scala 27:72]
  assign auto_out_3_a_bits_data = muxStateEarly_6_0 ? auto_in_3_c_bits_data : 128'h0; // @[Mux.scala 27:72]
  assign auto_out_3_d_ready = in_d_3_ready & allowed_7_0; // @[Arbiter.scala 123:31]
  assign auto_out_2_a_valid = idle_4 ? _T_369 : _sink_ACancel_earlyValid_T_29; // @[Arbiter.scala 125:29]
  assign auto_out_2_a_bits_opcode = muxStateEarly_4_1 ? a_a_2_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_param = muxStateEarly_4_1 ? a_a_2_bits_param : 3'h0; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_size = _T_419 | _T_420; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_source = _T_416 | _T_417; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_address = _T_413 | _T_414; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_user_amba_prot_bufferable = muxStateEarly_4_0 | muxStateEarly_4_1; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_user_amba_prot_modifiable = muxStateEarly_4_0 | muxStateEarly_4_1; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_user_amba_prot_readalloc = muxStateEarly_4_0 | muxStateEarly_4_1; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_user_amba_prot_writealloc = muxStateEarly_4_0 | muxStateEarly_4_1; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_user_amba_prot_privileged = muxStateEarly_4_0 | muxStateEarly_4_1; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_user_amba_prot_secure = muxStateEarly_4_0 | muxStateEarly_4_1; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_mask = _T_389 | _T_390; // @[Mux.scala 27:72]
  assign auto_out_2_a_bits_data = muxStateEarly_4_0 ? auto_in_2_c_bits_data : 128'h0; // @[Mux.scala 27:72]
  assign auto_out_2_d_ready = in_d_2_ready & allowed_5_0; // @[Arbiter.scala 123:31]
  assign auto_out_1_a_valid = idle_2 ? _T_202 : _sink_ACancel_earlyValid_T_16; // @[Arbiter.scala 125:29]
  assign auto_out_1_a_bits_opcode = muxStateEarly_2_1 ? a_a_1_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_param = muxStateEarly_2_1 ? a_a_1_bits_param : 3'h0; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_size = _T_252 | _T_253; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_source = _T_249 | _T_250; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_address = _T_246 | _T_247; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_bufferable = muxStateEarly_2_0 | muxStateEarly_2_1; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_modifiable = muxStateEarly_2_0 | muxStateEarly_2_1; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_readalloc = muxStateEarly_2_0 | muxStateEarly_2_1; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_writealloc = muxStateEarly_2_0 | muxStateEarly_2_1; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_privileged = muxStateEarly_2_0 | muxStateEarly_2_1; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_user_amba_prot_secure = muxStateEarly_2_0 | muxStateEarly_2_1; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_mask = _T_222 | _T_223; // @[Mux.scala 27:72]
  assign auto_out_1_a_bits_data = muxStateEarly_2_0 ? auto_in_1_c_bits_data : 128'h0; // @[Mux.scala 27:72]
  assign auto_out_1_d_ready = in_d_1_ready & allowed_3_0; // @[Arbiter.scala 123:31]
  assign auto_out_0_a_valid = idle ? _T_35 : _sink_ACancel_earlyValid_T_3; // @[Arbiter.scala 125:29]
  assign auto_out_0_a_bits_opcode = muxStateEarly__1 ? a_a_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_param = muxStateEarly__1 ? a_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_size = _T_85 | _T_86; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_source = _T_82 | _T_83; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_address = _T_79 | _T_80; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_user_amba_prot_bufferable = muxStateEarly__0 | muxStateEarly__1; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_user_amba_prot_modifiable = muxStateEarly__0 | muxStateEarly__1; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_user_amba_prot_readalloc = muxStateEarly__0 | muxStateEarly__1; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_user_amba_prot_writealloc = muxStateEarly__0 | muxStateEarly__1; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_user_amba_prot_privileged = muxStateEarly__0 | muxStateEarly__1; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_user_amba_prot_secure = muxStateEarly__0 | muxStateEarly__1; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_mask = _T_55 | _T_56; // @[Mux.scala 27:72]
  assign auto_out_0_a_bits_data = muxStateEarly__0 ? auto_in_0_c_bits_data : 128'h0; // @[Mux.scala 27:72]
  assign auto_out_0_d_ready = in_d_ready & allowed_1_0; // @[Arbiter.scala 123:31]
  assign pool_clock = clock;
  assign pool_reset = reset;
  assign pool_io_free_valid = auto_in_0_e_valid; // @[Decoupled.scala 40:37]
  assign pool_io_free_bits = auto_in_0_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign pool_io_alloc_ready = _pool_io_alloc_ready_T & d_first & d_grant; // @[CacheCork.scala 124:55]
  assign q_clock = clock;
  assign q_reset = reset;
  assign q_io_enq_valid = auto_in_0_c_valid & _c_d_valid_T; // @[CacheCork.scala 103:33]
  assign q_io_enq_bits_opcode = 3'h6; // @[Edges.scala 642:17 Edges.scala 643:15]
  assign q_io_enq_bits_param = 2'h0; // @[Edges.scala 642:17 Edges.scala 644:15]
  assign q_io_enq_bits_size = 3'h6; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_io_enq_bits_source = auto_in_0_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_io_enq_bits_sink = 3'h0; // @[Edges.scala 642:17 Edges.scala 647:15]
  assign q_io_enq_bits_denied = 1'h0; // @[Edges.scala 642:17 Edges.scala 648:15]
  assign q_io_enq_bits_data = 128'h0; // @[Edges.scala 642:17 Edges.scala 649:15]
  assign q_io_enq_bits_corrupt = 1'h0; // @[Edges.scala 642:17 Edges.scala 650:15]
  assign q_io_deq_ready = in_d_ready & allowed_1_1; // @[Arbiter.scala 123:31]
  assign q_1_clock = clock;
  assign q_1_reset = reset;
  assign q_1_io_enq_valid = auto_in_0_a_valid & toD; // @[CacheCork.scala 83:33]
  assign q_1_io_enq_bits_opcode = 3'h4; // @[Edges.scala 614:17 Edges.scala 615:15]
  assign q_1_io_enq_bits_param = 2'h0; // @[Edges.scala 614:17 Edges.scala 616:15]
  assign q_1_io_enq_bits_size = 3'h6; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_1_io_enq_bits_source = auto_in_0_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_1_io_enq_bits_sink = 3'h0; // @[Edges.scala 614:17 Edges.scala 619:15]
  assign q_1_io_enq_bits_denied = 1'h0; // @[Edges.scala 614:17 Edges.scala 620:15]
  assign q_1_io_enq_bits_data = 128'h0; // @[Edges.scala 614:17 Edges.scala 621:15]
  assign q_1_io_enq_bits_corrupt = 1'h0; // @[Edges.scala 614:17 Edges.scala 622:15]
  assign q_1_io_deq_ready = in_d_ready & allowed_1_2; // @[Arbiter.scala 123:31]
  assign pool_1_clock = clock;
  assign pool_1_reset = reset;
  assign pool_1_io_free_valid = auto_in_1_e_valid; // @[Decoupled.scala 40:37]
  assign pool_1_io_free_bits = auto_in_1_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign pool_1_io_alloc_ready = _pool_io_alloc_ready_T_3 & d_first_1 & d_grant_1; // @[CacheCork.scala 124:55]
  assign q_2_clock = clock;
  assign q_2_reset = reset;
  assign q_2_io_enq_valid = auto_in_1_c_valid & _c_d_valid_T_2; // @[CacheCork.scala 103:33]
  assign q_2_io_enq_bits_opcode = 3'h6; // @[Edges.scala 642:17 Edges.scala 643:15]
  assign q_2_io_enq_bits_param = 2'h0; // @[Edges.scala 642:17 Edges.scala 644:15]
  assign q_2_io_enq_bits_size = 3'h6; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_2_io_enq_bits_source = auto_in_1_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_2_io_enq_bits_sink = 3'h0; // @[Edges.scala 642:17 Edges.scala 647:15]
  assign q_2_io_enq_bits_denied = 1'h0; // @[Edges.scala 642:17 Edges.scala 648:15]
  assign q_2_io_enq_bits_data = 128'h0; // @[Edges.scala 642:17 Edges.scala 649:15]
  assign q_2_io_enq_bits_corrupt = 1'h0; // @[Edges.scala 642:17 Edges.scala 650:15]
  assign q_2_io_deq_ready = in_d_1_ready & allowed_3_1; // @[Arbiter.scala 123:31]
  assign q_3_clock = clock;
  assign q_3_reset = reset;
  assign q_3_io_enq_valid = auto_in_1_a_valid & toD_1; // @[CacheCork.scala 83:33]
  assign q_3_io_enq_bits_opcode = 3'h4; // @[Edges.scala 614:17 Edges.scala 615:15]
  assign q_3_io_enq_bits_param = 2'h0; // @[Edges.scala 614:17 Edges.scala 616:15]
  assign q_3_io_enq_bits_size = 3'h6; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_3_io_enq_bits_source = auto_in_1_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_3_io_enq_bits_sink = 3'h0; // @[Edges.scala 614:17 Edges.scala 619:15]
  assign q_3_io_enq_bits_denied = 1'h0; // @[Edges.scala 614:17 Edges.scala 620:15]
  assign q_3_io_enq_bits_data = 128'h0; // @[Edges.scala 614:17 Edges.scala 621:15]
  assign q_3_io_enq_bits_corrupt = 1'h0; // @[Edges.scala 614:17 Edges.scala 622:15]
  assign q_3_io_deq_ready = in_d_1_ready & allowed_3_2; // @[Arbiter.scala 123:31]
  assign pool_2_clock = clock;
  assign pool_2_reset = reset;
  assign pool_2_io_free_valid = auto_in_2_e_valid; // @[Decoupled.scala 40:37]
  assign pool_2_io_free_bits = auto_in_2_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign pool_2_io_alloc_ready = _pool_io_alloc_ready_T_6 & d_first_2 & d_grant_2; // @[CacheCork.scala 124:55]
  assign q_4_clock = clock;
  assign q_4_reset = reset;
  assign q_4_io_enq_valid = auto_in_2_c_valid & _c_d_valid_T_4; // @[CacheCork.scala 103:33]
  assign q_4_io_enq_bits_opcode = 3'h6; // @[Edges.scala 642:17 Edges.scala 643:15]
  assign q_4_io_enq_bits_param = 2'h0; // @[Edges.scala 642:17 Edges.scala 644:15]
  assign q_4_io_enq_bits_size = 3'h6; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_4_io_enq_bits_source = auto_in_2_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_4_io_enq_bits_sink = 3'h0; // @[Edges.scala 642:17 Edges.scala 647:15]
  assign q_4_io_enq_bits_denied = 1'h0; // @[Edges.scala 642:17 Edges.scala 648:15]
  assign q_4_io_enq_bits_data = 128'h0; // @[Edges.scala 642:17 Edges.scala 649:15]
  assign q_4_io_enq_bits_corrupt = 1'h0; // @[Edges.scala 642:17 Edges.scala 650:15]
  assign q_4_io_deq_ready = in_d_2_ready & allowed_5_1; // @[Arbiter.scala 123:31]
  assign q_5_clock = clock;
  assign q_5_reset = reset;
  assign q_5_io_enq_valid = auto_in_2_a_valid & toD_2; // @[CacheCork.scala 83:33]
  assign q_5_io_enq_bits_opcode = 3'h4; // @[Edges.scala 614:17 Edges.scala 615:15]
  assign q_5_io_enq_bits_param = 2'h0; // @[Edges.scala 614:17 Edges.scala 616:15]
  assign q_5_io_enq_bits_size = 3'h6; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_5_io_enq_bits_source = auto_in_2_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_5_io_enq_bits_sink = 3'h0; // @[Edges.scala 614:17 Edges.scala 619:15]
  assign q_5_io_enq_bits_denied = 1'h0; // @[Edges.scala 614:17 Edges.scala 620:15]
  assign q_5_io_enq_bits_data = 128'h0; // @[Edges.scala 614:17 Edges.scala 621:15]
  assign q_5_io_enq_bits_corrupt = 1'h0; // @[Edges.scala 614:17 Edges.scala 622:15]
  assign q_5_io_deq_ready = in_d_2_ready & allowed_5_2; // @[Arbiter.scala 123:31]
  assign pool_3_clock = clock;
  assign pool_3_reset = reset;
  assign pool_3_io_free_valid = auto_in_3_e_valid; // @[Decoupled.scala 40:37]
  assign pool_3_io_free_bits = auto_in_3_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign pool_3_io_alloc_ready = _pool_io_alloc_ready_T_9 & d_first_3 & d_grant_3; // @[CacheCork.scala 124:55]
  assign q_6_clock = clock;
  assign q_6_reset = reset;
  assign q_6_io_enq_valid = auto_in_3_c_valid & _c_d_valid_T_6; // @[CacheCork.scala 103:33]
  assign q_6_io_enq_bits_opcode = 3'h6; // @[Edges.scala 642:17 Edges.scala 643:15]
  assign q_6_io_enq_bits_param = 2'h0; // @[Edges.scala 642:17 Edges.scala 644:15]
  assign q_6_io_enq_bits_size = 3'h6; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_6_io_enq_bits_source = auto_in_3_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_6_io_enq_bits_sink = 3'h0; // @[Edges.scala 642:17 Edges.scala 647:15]
  assign q_6_io_enq_bits_denied = 1'h0; // @[Edges.scala 642:17 Edges.scala 648:15]
  assign q_6_io_enq_bits_data = 128'h0; // @[Edges.scala 642:17 Edges.scala 649:15]
  assign q_6_io_enq_bits_corrupt = 1'h0; // @[Edges.scala 642:17 Edges.scala 650:15]
  assign q_6_io_deq_ready = in_d_3_ready & allowed_7_1; // @[Arbiter.scala 123:31]
  assign q_7_clock = clock;
  assign q_7_reset = reset;
  assign q_7_io_enq_valid = auto_in_3_a_valid & toD_3; // @[CacheCork.scala 83:33]
  assign q_7_io_enq_bits_opcode = 3'h4; // @[Edges.scala 614:17 Edges.scala 615:15]
  assign q_7_io_enq_bits_param = 2'h0; // @[Edges.scala 614:17 Edges.scala 616:15]
  assign q_7_io_enq_bits_size = 3'h6; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_7_io_enq_bits_source = auto_in_3_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign q_7_io_enq_bits_sink = 3'h0; // @[Edges.scala 614:17 Edges.scala 619:15]
  assign q_7_io_enq_bits_denied = 1'h0; // @[Edges.scala 614:17 Edges.scala 620:15]
  assign q_7_io_enq_bits_data = 128'h0; // @[Edges.scala 614:17 Edges.scala 621:15]
  assign q_7_io_enq_bits_corrupt = 1'h0; // @[Edges.scala 614:17 Edges.scala 622:15]
  assign q_7_io_deq_ready = in_d_3_ready & allowed_7_2; // @[Arbiter.scala 123:31]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft <= 2'h0;
    end else if (latch) begin
      beatsLeft <= initBeats;
    end else begin
      beatsLeft <= _beatsLeft_T_4;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state__1 <= 1'h0;
    end else if (idle) begin
      state__1 <= earlyWinner__1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state__0 <= 1'h0;
    end else if (idle) begin
      state__0 <= earlyWinner__0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      d_first_counter <= 2'h0;
    end else if (_d_first_T) begin
      if (d_first) begin
        if (d_first_beats1_opdata) begin
          d_first_counter <= d_first_beats1_decode;
        end else begin
          d_first_counter <= 2'h0;
        end
      end else begin
        d_first_counter <= d_first_counter1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_1 <= 2'h0;
    end else if (latch_1) begin
      if (earlyWinner_1_0) begin
        if (opdata_2) begin
          beatsLeft_1 <= decode_2;
        end else begin
          beatsLeft_1 <= 2'h0;
        end
      end else begin
        beatsLeft_1 <= 2'h0;
      end
    end else begin
      beatsLeft_1 <= _beatsLeft_T_10;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_1_0 <= 1'h0;
    end else if (idle_1) begin
      state_1_0 <= earlyWinner_1_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_1_1 <= 1'h0;
    end else if (idle_1) begin
      state_1_1 <= earlyWinner_1_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_1_2 <= 1'h0;
    end else if (idle_1) begin
      state_1_2 <= earlyWinner_1_2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleIn_0_d_bits_sink_r <= 3'h0;
    end else if (d_first) begin
      bundleIn_0_d_bits_sink_r <= pool_io_alloc_bits;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_2 <= 2'h0;
    end else if (latch_2) begin
      beatsLeft_2 <= initBeats_2;
    end else begin
      beatsLeft_2 <= _beatsLeft_T_16;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_2_1 <= 1'h0;
    end else if (idle_2) begin
      state_2_1 <= earlyWinner_2_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_2_0 <= 1'h0;
    end else if (idle_2) begin
      state_2_0 <= earlyWinner_2_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      d_first_counter_1 <= 2'h0;
    end else if (_d_first_T_1) begin
      if (d_first_1) begin
        if (d_first_beats1_opdata_1) begin
          d_first_counter_1 <= d_first_beats1_decode_1;
        end else begin
          d_first_counter_1 <= 2'h0;
        end
      end else begin
        d_first_counter_1 <= d_first_counter1_1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_3 <= 2'h0;
    end else if (latch_3) begin
      if (earlyWinner_3_0) begin
        if (opdata_5) begin
          beatsLeft_3 <= decode_5;
        end else begin
          beatsLeft_3 <= 2'h0;
        end
      end else begin
        beatsLeft_3 <= 2'h0;
      end
    end else begin
      beatsLeft_3 <= _beatsLeft_T_22;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_3_0 <= 1'h0;
    end else if (idle_3) begin
      state_3_0 <= earlyWinner_3_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_3_1 <= 1'h0;
    end else if (idle_3) begin
      state_3_1 <= earlyWinner_3_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_3_2 <= 1'h0;
    end else if (idle_3) begin
      state_3_2 <= earlyWinner_3_2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleIn_1_d_bits_sink_r <= 3'h0;
    end else if (d_first_1) begin
      bundleIn_1_d_bits_sink_r <= pool_1_io_alloc_bits;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_4 <= 2'h0;
    end else if (latch_4) begin
      beatsLeft_4 <= initBeats_4;
    end else begin
      beatsLeft_4 <= _beatsLeft_T_28;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_4_1 <= 1'h0;
    end else if (idle_4) begin
      state_4_1 <= earlyWinner_4_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_4_0 <= 1'h0;
    end else if (idle_4) begin
      state_4_0 <= earlyWinner_4_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      d_first_counter_2 <= 2'h0;
    end else if (_d_first_T_2) begin
      if (d_first_2) begin
        if (d_first_beats1_opdata_2) begin
          d_first_counter_2 <= d_first_beats1_decode_2;
        end else begin
          d_first_counter_2 <= 2'h0;
        end
      end else begin
        d_first_counter_2 <= d_first_counter1_2;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_5 <= 2'h0;
    end else if (latch_5) begin
      if (earlyWinner_5_0) begin
        if (opdata_8) begin
          beatsLeft_5 <= decode_8;
        end else begin
          beatsLeft_5 <= 2'h0;
        end
      end else begin
        beatsLeft_5 <= 2'h0;
      end
    end else begin
      beatsLeft_5 <= _beatsLeft_T_34;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_5_0 <= 1'h0;
    end else if (idle_5) begin
      state_5_0 <= earlyWinner_5_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_5_1 <= 1'h0;
    end else if (idle_5) begin
      state_5_1 <= earlyWinner_5_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_5_2 <= 1'h0;
    end else if (idle_5) begin
      state_5_2 <= earlyWinner_5_2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleIn_2_d_bits_sink_r <= 3'h0;
    end else if (d_first_2) begin
      bundleIn_2_d_bits_sink_r <= pool_2_io_alloc_bits;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_6 <= 2'h0;
    end else if (latch_6) begin
      beatsLeft_6 <= initBeats_6;
    end else begin
      beatsLeft_6 <= _beatsLeft_T_40;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_6_1 <= 1'h0;
    end else if (idle_6) begin
      state_6_1 <= earlyWinner_6_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_6_0 <= 1'h0;
    end else if (idle_6) begin
      state_6_0 <= earlyWinner_6_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      d_first_counter_3 <= 2'h0;
    end else if (_d_first_T_3) begin
      if (d_first_3) begin
        if (d_first_beats1_opdata_3) begin
          d_first_counter_3 <= d_first_beats1_decode_3;
        end else begin
          d_first_counter_3 <= 2'h0;
        end
      end else begin
        d_first_counter_3 <= d_first_counter1_3;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft_7 <= 2'h0;
    end else if (latch_7) begin
      if (earlyWinner_7_0) begin
        if (opdata_11) begin
          beatsLeft_7 <= decode_11;
        end else begin
          beatsLeft_7 <= 2'h0;
        end
      end else begin
        beatsLeft_7 <= 2'h0;
      end
    end else begin
      beatsLeft_7 <= _beatsLeft_T_46;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_7_0 <= 1'h0;
    end else if (idle_7) begin
      state_7_0 <= earlyWinner_7_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_7_1 <= 1'h0;
    end else if (idle_7) begin
      state_7_1 <= earlyWinner_7_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_7_2 <= 1'h0;
    end else if (idle_7) begin
      state_7_2 <= earlyWinner_7_2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bundleIn_3_d_bits_sink_r <= 3'h0;
    end else if (d_first_3) begin
      bundleIn_3_d_bits_sink_r <= pool_3_io_alloc_bits;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  beatsLeft = _RAND_0[1:0];
  _RAND_1 = {1{`RANDOM}};
  state__1 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  state__0 = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  d_first_counter = _RAND_3[1:0];
  _RAND_4 = {1{`RANDOM}};
  beatsLeft_1 = _RAND_4[1:0];
  _RAND_5 = {1{`RANDOM}};
  state_1_0 = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  state_1_1 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  state_1_2 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  bundleIn_0_d_bits_sink_r = _RAND_8[2:0];
  _RAND_9 = {1{`RANDOM}};
  beatsLeft_2 = _RAND_9[1:0];
  _RAND_10 = {1{`RANDOM}};
  state_2_1 = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  state_2_0 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  d_first_counter_1 = _RAND_12[1:0];
  _RAND_13 = {1{`RANDOM}};
  beatsLeft_3 = _RAND_13[1:0];
  _RAND_14 = {1{`RANDOM}};
  state_3_0 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  state_3_1 = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  state_3_2 = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  bundleIn_1_d_bits_sink_r = _RAND_17[2:0];
  _RAND_18 = {1{`RANDOM}};
  beatsLeft_4 = _RAND_18[1:0];
  _RAND_19 = {1{`RANDOM}};
  state_4_1 = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  state_4_0 = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  d_first_counter_2 = _RAND_21[1:0];
  _RAND_22 = {1{`RANDOM}};
  beatsLeft_5 = _RAND_22[1:0];
  _RAND_23 = {1{`RANDOM}};
  state_5_0 = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  state_5_1 = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  state_5_2 = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  bundleIn_2_d_bits_sink_r = _RAND_26[2:0];
  _RAND_27 = {1{`RANDOM}};
  beatsLeft_6 = _RAND_27[1:0];
  _RAND_28 = {1{`RANDOM}};
  state_6_1 = _RAND_28[0:0];
  _RAND_29 = {1{`RANDOM}};
  state_6_0 = _RAND_29[0:0];
  _RAND_30 = {1{`RANDOM}};
  d_first_counter_3 = _RAND_30[1:0];
  _RAND_31 = {1{`RANDOM}};
  beatsLeft_7 = _RAND_31[1:0];
  _RAND_32 = {1{`RANDOM}};
  state_7_0 = _RAND_32[0:0];
  _RAND_33 = {1{`RANDOM}};
  state_7_1 = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  state_7_2 = _RAND_34[0:0];
  _RAND_35 = {1{`RANDOM}};
  bundleIn_3_d_bits_sink_r = _RAND_35[2:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    beatsLeft = 2'h0;
  end
  if (reset) begin
    state__1 = 1'h0;
  end
  if (reset) begin
    state__0 = 1'h0;
  end
  if (reset) begin
    d_first_counter = 2'h0;
  end
  if (reset) begin
    beatsLeft_1 = 2'h0;
  end
  if (reset) begin
    state_1_0 = 1'h0;
  end
  if (reset) begin
    state_1_1 = 1'h0;
  end
  if (reset) begin
    state_1_2 = 1'h0;
  end
  if (rf_reset) begin
    bundleIn_0_d_bits_sink_r = 3'h0;
  end
  if (reset) begin
    beatsLeft_2 = 2'h0;
  end
  if (reset) begin
    state_2_1 = 1'h0;
  end
  if (reset) begin
    state_2_0 = 1'h0;
  end
  if (reset) begin
    d_first_counter_1 = 2'h0;
  end
  if (reset) begin
    beatsLeft_3 = 2'h0;
  end
  if (reset) begin
    state_3_0 = 1'h0;
  end
  if (reset) begin
    state_3_1 = 1'h0;
  end
  if (reset) begin
    state_3_2 = 1'h0;
  end
  if (rf_reset) begin
    bundleIn_1_d_bits_sink_r = 3'h0;
  end
  if (reset) begin
    beatsLeft_4 = 2'h0;
  end
  if (reset) begin
    state_4_1 = 1'h0;
  end
  if (reset) begin
    state_4_0 = 1'h0;
  end
  if (reset) begin
    d_first_counter_2 = 2'h0;
  end
  if (reset) begin
    beatsLeft_5 = 2'h0;
  end
  if (reset) begin
    state_5_0 = 1'h0;
  end
  if (reset) begin
    state_5_1 = 1'h0;
  end
  if (reset) begin
    state_5_2 = 1'h0;
  end
  if (rf_reset) begin
    bundleIn_2_d_bits_sink_r = 3'h0;
  end
  if (reset) begin
    beatsLeft_6 = 2'h0;
  end
  if (reset) begin
    state_6_1 = 1'h0;
  end
  if (reset) begin
    state_6_0 = 1'h0;
  end
  if (reset) begin
    d_first_counter_3 = 2'h0;
  end
  if (reset) begin
    beatsLeft_7 = 2'h0;
  end
  if (reset) begin
    state_7_0 = 1'h0;
  end
  if (reset) begin
    state_7_1 = 1'h0;
  end
  if (reset) begin
    state_7_2 = 1'h0;
  end
  if (rf_reset) begin
    bundleIn_3_d_bits_sink_r = 3'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
