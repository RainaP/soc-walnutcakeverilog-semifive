//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLInterconnectCoupler_9(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_widget_in_a_ready,
  input         auto_widget_in_a_valid,
  input  [1:0]  auto_widget_in_a_bits_source,
  input  [36:0] auto_widget_in_a_bits_address,
  input  [31:0] auto_widget_in_a_bits_data,
  output        auto_widget_in_d_valid,
  output [1:0]  auto_widget_in_d_bits_source,
  output        auto_widget_in_d_bits_denied,
  input         auto_tl_out_a_ready,
  output        auto_tl_out_a_valid,
  output [1:0]  auto_tl_out_a_bits_source,
  output [36:0] auto_tl_out_a_bits_address,
  output [7:0]  auto_tl_out_a_bits_mask,
  output [63:0] auto_tl_out_a_bits_data,
  output        auto_tl_out_d_ready,
  input         auto_tl_out_d_valid,
  input  [2:0]  auto_tl_out_d_bits_opcode,
  input  [1:0]  auto_tl_out_d_bits_param,
  input  [3:0]  auto_tl_out_d_bits_size,
  input  [1:0]  auto_tl_out_d_bits_source,
  input  [4:0]  auto_tl_out_d_bits_sink,
  input         auto_tl_out_d_bits_denied,
  input         auto_tl_out_d_bits_corrupt
);
  wire  fixer_rf_reset; // @[FIFOFixer.scala 144:27]
  wire  fixer_clock; // @[FIFOFixer.scala 144:27]
  wire  fixer_reset; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_a_valid; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_in_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_in_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire [7:0] fixer_auto_in_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [63:0] fixer_auto_in_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_in_d_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_in_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_d_bits_sink; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_a_valid; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_out_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_out_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire [7:0] fixer_auto_out_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [63:0] fixer_auto_out_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_out_d_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_out_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_d_bits_sink; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  widget_rf_reset; // @[WidthWidget.scala 219:28]
  wire  widget_clock; // @[WidthWidget.scala 219:28]
  wire  widget_reset; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_a_ready; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_a_valid; // @[WidthWidget.scala 219:28]
  wire [1:0] widget_auto_in_a_bits_source; // @[WidthWidget.scala 219:28]
  wire [36:0] widget_auto_in_a_bits_address; // @[WidthWidget.scala 219:28]
  wire [31:0] widget_auto_in_a_bits_data; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_d_valid; // @[WidthWidget.scala 219:28]
  wire [1:0] widget_auto_in_d_bits_source; // @[WidthWidget.scala 219:28]
  wire  widget_auto_in_d_bits_denied; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_a_ready; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_a_valid; // @[WidthWidget.scala 219:28]
  wire [1:0] widget_auto_out_a_bits_source; // @[WidthWidget.scala 219:28]
  wire [36:0] widget_auto_out_a_bits_address; // @[WidthWidget.scala 219:28]
  wire [7:0] widget_auto_out_a_bits_mask; // @[WidthWidget.scala 219:28]
  wire [63:0] widget_auto_out_a_bits_data; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_d_ready; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_d_valid; // @[WidthWidget.scala 219:28]
  wire [2:0] widget_auto_out_d_bits_opcode; // @[WidthWidget.scala 219:28]
  wire [1:0] widget_auto_out_d_bits_param; // @[WidthWidget.scala 219:28]
  wire [3:0] widget_auto_out_d_bits_size; // @[WidthWidget.scala 219:28]
  wire [1:0] widget_auto_out_d_bits_source; // @[WidthWidget.scala 219:28]
  wire [4:0] widget_auto_out_d_bits_sink; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_d_bits_denied; // @[WidthWidget.scala 219:28]
  wire  widget_auto_out_d_bits_corrupt; // @[WidthWidget.scala 219:28]
  RHEA__TLFIFOFixer_3 fixer ( // @[FIFOFixer.scala 144:27]
    .rf_reset(fixer_rf_reset),
    .clock(fixer_clock),
    .reset(fixer_reset),
    .auto_in_a_ready(fixer_auto_in_a_ready),
    .auto_in_a_valid(fixer_auto_in_a_valid),
    .auto_in_a_bits_source(fixer_auto_in_a_bits_source),
    .auto_in_a_bits_address(fixer_auto_in_a_bits_address),
    .auto_in_a_bits_mask(fixer_auto_in_a_bits_mask),
    .auto_in_a_bits_data(fixer_auto_in_a_bits_data),
    .auto_in_d_ready(fixer_auto_in_d_ready),
    .auto_in_d_valid(fixer_auto_in_d_valid),
    .auto_in_d_bits_opcode(fixer_auto_in_d_bits_opcode),
    .auto_in_d_bits_param(fixer_auto_in_d_bits_param),
    .auto_in_d_bits_size(fixer_auto_in_d_bits_size),
    .auto_in_d_bits_source(fixer_auto_in_d_bits_source),
    .auto_in_d_bits_sink(fixer_auto_in_d_bits_sink),
    .auto_in_d_bits_denied(fixer_auto_in_d_bits_denied),
    .auto_in_d_bits_corrupt(fixer_auto_in_d_bits_corrupt),
    .auto_out_a_ready(fixer_auto_out_a_ready),
    .auto_out_a_valid(fixer_auto_out_a_valid),
    .auto_out_a_bits_source(fixer_auto_out_a_bits_source),
    .auto_out_a_bits_address(fixer_auto_out_a_bits_address),
    .auto_out_a_bits_mask(fixer_auto_out_a_bits_mask),
    .auto_out_a_bits_data(fixer_auto_out_a_bits_data),
    .auto_out_d_ready(fixer_auto_out_d_ready),
    .auto_out_d_valid(fixer_auto_out_d_valid),
    .auto_out_d_bits_opcode(fixer_auto_out_d_bits_opcode),
    .auto_out_d_bits_param(fixer_auto_out_d_bits_param),
    .auto_out_d_bits_size(fixer_auto_out_d_bits_size),
    .auto_out_d_bits_source(fixer_auto_out_d_bits_source),
    .auto_out_d_bits_sink(fixer_auto_out_d_bits_sink),
    .auto_out_d_bits_denied(fixer_auto_out_d_bits_denied),
    .auto_out_d_bits_corrupt(fixer_auto_out_d_bits_corrupt)
  );
  RHEA__TLWidthWidget_6 widget ( // @[WidthWidget.scala 219:28]
    .rf_reset(widget_rf_reset),
    .clock(widget_clock),
    .reset(widget_reset),
    .auto_in_a_ready(widget_auto_in_a_ready),
    .auto_in_a_valid(widget_auto_in_a_valid),
    .auto_in_a_bits_source(widget_auto_in_a_bits_source),
    .auto_in_a_bits_address(widget_auto_in_a_bits_address),
    .auto_in_a_bits_data(widget_auto_in_a_bits_data),
    .auto_in_d_valid(widget_auto_in_d_valid),
    .auto_in_d_bits_source(widget_auto_in_d_bits_source),
    .auto_in_d_bits_denied(widget_auto_in_d_bits_denied),
    .auto_out_a_ready(widget_auto_out_a_ready),
    .auto_out_a_valid(widget_auto_out_a_valid),
    .auto_out_a_bits_source(widget_auto_out_a_bits_source),
    .auto_out_a_bits_address(widget_auto_out_a_bits_address),
    .auto_out_a_bits_mask(widget_auto_out_a_bits_mask),
    .auto_out_a_bits_data(widget_auto_out_a_bits_data),
    .auto_out_d_ready(widget_auto_out_d_ready),
    .auto_out_d_valid(widget_auto_out_d_valid),
    .auto_out_d_bits_opcode(widget_auto_out_d_bits_opcode),
    .auto_out_d_bits_param(widget_auto_out_d_bits_param),
    .auto_out_d_bits_size(widget_auto_out_d_bits_size),
    .auto_out_d_bits_source(widget_auto_out_d_bits_source),
    .auto_out_d_bits_sink(widget_auto_out_d_bits_sink),
    .auto_out_d_bits_denied(widget_auto_out_d_bits_denied),
    .auto_out_d_bits_corrupt(widget_auto_out_d_bits_corrupt)
  );
  assign fixer_rf_reset = rf_reset;
  assign widget_rf_reset = rf_reset;
  assign auto_widget_in_a_ready = widget_auto_in_a_ready; // @[LazyModule.scala 388:16]
  assign auto_widget_in_d_valid = widget_auto_in_d_valid; // @[LazyModule.scala 388:16]
  assign auto_widget_in_d_bits_source = widget_auto_in_d_bits_source; // @[LazyModule.scala 388:16]
  assign auto_widget_in_d_bits_denied = widget_auto_in_d_bits_denied; // @[LazyModule.scala 388:16]
  assign auto_tl_out_a_valid = fixer_auto_out_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_source = fixer_auto_out_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_address = fixer_auto_out_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_mask = fixer_auto_out_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_data = fixer_auto_out_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_d_ready = fixer_auto_out_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign fixer_clock = clock;
  assign fixer_reset = reset;
  assign fixer_auto_in_a_valid = widget_auto_out_a_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_source = widget_auto_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_address = widget_auto_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_mask = widget_auto_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_data = widget_auto_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_d_ready = widget_auto_out_d_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_a_ready = auto_tl_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign fixer_auto_out_d_valid = auto_tl_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign fixer_auto_out_d_bits_opcode = auto_tl_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign fixer_auto_out_d_bits_param = auto_tl_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign fixer_auto_out_d_bits_size = auto_tl_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign fixer_auto_out_d_bits_source = auto_tl_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign fixer_auto_out_d_bits_sink = auto_tl_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign fixer_auto_out_d_bits_denied = auto_tl_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign fixer_auto_out_d_bits_corrupt = auto_tl_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_clock = clock;
  assign widget_reset = reset;
  assign widget_auto_in_a_valid = auto_widget_in_a_valid; // @[LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_source = auto_widget_in_a_bits_source; // @[LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_address = auto_widget_in_a_bits_address; // @[LazyModule.scala 388:16]
  assign widget_auto_in_a_bits_data = auto_widget_in_a_bits_data; // @[LazyModule.scala 388:16]
  assign widget_auto_out_a_ready = fixer_auto_in_a_ready; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_valid = fixer_auto_in_d_valid; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_opcode = fixer_auto_in_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_param = fixer_auto_in_d_bits_param; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_size = fixer_auto_in_d_bits_size; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_source = fixer_auto_in_d_bits_source; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_sink = fixer_auto_in_d_bits_sink; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_denied = fixer_auto_in_d_bits_denied; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_corrupt = fixer_auto_in_d_bits_corrupt; // @[LazyModule.scala 375:16]
endmodule
