//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_67_assert(
  input         clock,
  input         reset,
  input         io_in_a_ready,
  input         io_in_a_valid,
  input  [2:0]  io_in_a_bits_opcode,
  input  [2:0]  io_in_a_bits_param,
  input  [1:0]  io_in_a_bits_size,
  input  [10:0] io_in_a_bits_source,
  input  [11:0] io_in_a_bits_address,
  input  [7:0]  io_in_a_bits_mask,
  input         io_in_a_bits_corrupt,
  input         io_in_d_ready,
  input         io_in_d_valid,
  input  [2:0]  io_in_d_bits_opcode,
  input  [1:0]  io_in_d_bits_size,
  input  [10:0] io_in_d_bits_source
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire  _T_10; // @[Parameters.scala 58:20]
  wire [5:0] _T_14; // @[package.scala 189:77]
  wire [2:0] _T_16; // @[package.scala 189:46]
  wire [11:0] _GEN_18; // @[Edges.scala 22:16]
  wire [11:0] _T_17; // @[Edges.scala 22:16]
  wire  _T_18; // @[Edges.scala 22:24]
  wire [2:0] _T_19; // @[Misc.scala 201:34]
  wire [3:0] _T_21; // @[OneHot.scala 65:12]
  wire [2:0] _T_23; // @[Misc.scala 201:81]
  wire  _T_24; // @[Misc.scala 205:21]
  wire  _T_27; // @[Misc.scala 210:20]
  wire  _T_29; // @[Misc.scala 214:38]
  wire  _T_30; // @[Misc.scala 214:29]
  wire  _T_32; // @[Misc.scala 214:38]
  wire  _T_33; // @[Misc.scala 214:29]
  wire  _T_36; // @[Misc.scala 210:20]
  wire  _T_37; // @[Misc.scala 213:27]
  wire  _T_38; // @[Misc.scala 214:38]
  wire  _T_39; // @[Misc.scala 214:29]
  wire  _T_40; // @[Misc.scala 213:27]
  wire  _T_41; // @[Misc.scala 214:38]
  wire  _T_42; // @[Misc.scala 214:29]
  wire  _T_43; // @[Misc.scala 213:27]
  wire  _T_44; // @[Misc.scala 214:38]
  wire  _T_45; // @[Misc.scala 214:29]
  wire  _T_46; // @[Misc.scala 213:27]
  wire  _T_47; // @[Misc.scala 214:38]
  wire  _T_48; // @[Misc.scala 214:29]
  wire  _T_51; // @[Misc.scala 210:20]
  wire  _T_52; // @[Misc.scala 213:27]
  wire  _T_53; // @[Misc.scala 214:38]
  wire  _T_54; // @[Misc.scala 214:29]
  wire  _T_55; // @[Misc.scala 213:27]
  wire  _T_56; // @[Misc.scala 214:38]
  wire  _T_57; // @[Misc.scala 214:29]
  wire  _T_58; // @[Misc.scala 213:27]
  wire  _T_59; // @[Misc.scala 214:38]
  wire  _T_60; // @[Misc.scala 214:29]
  wire  _T_61; // @[Misc.scala 213:27]
  wire  _T_62; // @[Misc.scala 214:38]
  wire  _T_63; // @[Misc.scala 214:29]
  wire  _T_64; // @[Misc.scala 213:27]
  wire  _T_65; // @[Misc.scala 214:38]
  wire  _T_66; // @[Misc.scala 214:29]
  wire  _T_67; // @[Misc.scala 213:27]
  wire  _T_68; // @[Misc.scala 214:38]
  wire  _T_69; // @[Misc.scala 214:29]
  wire  _T_70; // @[Misc.scala 213:27]
  wire  _T_71; // @[Misc.scala 214:38]
  wire  _T_72; // @[Misc.scala 214:29]
  wire  _T_73; // @[Misc.scala 213:27]
  wire  _T_74; // @[Misc.scala 214:38]
  wire  _T_75; // @[Misc.scala 214:29]
  wire [7:0] _T_82; // @[Cat.scala 29:58]
  wire [12:0] _T_93; // @[Parameters.scala 137:49]
  wire  _T_101; // @[Monitor.scala 79:25]
  wire [12:0] _T_106; // @[Parameters.scala 137:52]
  wire  _T_107; // @[Parameters.scala 137:67]
  wire  _T_112; // @[Monitor.scala 44:11]
  wire  _T_117; // @[Monitor.scala 44:11]
  wire  _T_118; // @[Monitor.scala 44:11]
  wire  _T_121; // @[Monitor.scala 44:11]
  wire  _T_122; // @[Monitor.scala 44:11]
  wire  _T_124; // @[Monitor.scala 44:11]
  wire  _T_125; // @[Monitor.scala 44:11]
  wire  _T_126; // @[Bundles.scala 110:27]
  wire  _T_128; // @[Monitor.scala 44:11]
  wire  _T_129; // @[Monitor.scala 44:11]
  wire [7:0] _T_130; // @[Monitor.scala 86:18]
  wire  _T_131; // @[Monitor.scala 86:31]
  wire  _T_133; // @[Monitor.scala 44:11]
  wire  _T_134; // @[Monitor.scala 44:11]
  wire  _T_135; // @[Monitor.scala 87:18]
  wire  _T_137; // @[Monitor.scala 44:11]
  wire  _T_138; // @[Monitor.scala 44:11]
  wire  _T_139; // @[Monitor.scala 90:25]
  wire  _T_168; // @[Monitor.scala 97:31]
  wire  _T_170; // @[Monitor.scala 44:11]
  wire  _T_171; // @[Monitor.scala 44:11]
  wire  _T_181; // @[Monitor.scala 102:25]
  wire  _T_194; // @[Monitor.scala 44:11]
  wire  _T_195; // @[Monitor.scala 44:11]
  wire  _T_202; // @[Monitor.scala 106:31]
  wire  _T_204; // @[Monitor.scala 44:11]
  wire  _T_205; // @[Monitor.scala 44:11]
  wire  _T_206; // @[Monitor.scala 107:30]
  wire  _T_208; // @[Monitor.scala 44:11]
  wire  _T_209; // @[Monitor.scala 44:11]
  wire  _T_214; // @[Monitor.scala 111:25]
  wire  _T_243; // @[Monitor.scala 119:25]
  wire [7:0] _T_268; // @[Monitor.scala 124:33]
  wire [7:0] _T_269; // @[Monitor.scala 124:31]
  wire  _T_270; // @[Monitor.scala 124:40]
  wire  _T_272; // @[Monitor.scala 44:11]
  wire  _T_273; // @[Monitor.scala 44:11]
  wire  _T_274; // @[Monitor.scala 127:25]
  wire  _T_292; // @[Bundles.scala 140:33]
  wire  _T_294; // @[Monitor.scala 44:11]
  wire  _T_295; // @[Monitor.scala 44:11]
  wire  _T_300; // @[Monitor.scala 135:25]
  wire  _T_318; // @[Bundles.scala 147:30]
  wire  _T_320; // @[Monitor.scala 44:11]
  wire  _T_321; // @[Monitor.scala 44:11]
  wire  _T_326; // @[Monitor.scala 143:25]
  wire  _T_344; // @[Bundles.scala 160:28]
  wire  _T_346; // @[Monitor.scala 44:11]
  wire  _T_347; // @[Monitor.scala 44:11]
  wire  _T_356; // @[Bundles.scala 44:24]
  wire  _T_358; // @[Monitor.scala 51:11]
  wire  _T_359; // @[Monitor.scala 51:11]
  wire  _T_366; // @[Parameters.scala 58:20]
  wire  _T_370; // @[Monitor.scala 307:25]
  wire  _T_372; // @[Monitor.scala 51:11]
  wire  _T_373; // @[Monitor.scala 51:11]
  wire  _T_374; // @[Monitor.scala 309:27]
  wire  _T_376; // @[Monitor.scala 51:11]
  wire  _T_377; // @[Monitor.scala 51:11]
  wire  _T_390; // @[Monitor.scala 315:25]
  wire  _T_418; // @[Monitor.scala 325:25]
  wire  _T_447; // @[Monitor.scala 335:25]
  wire  _T_464; // @[Monitor.scala 343:25]
  wire  _T_482; // @[Monitor.scala 351:25]
  wire  _T_514; // @[Decoupled.scala 40:37]
  reg  _T_523; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire  _T_525; // @[Edges.scala 231:28]
  wire  _T_526; // @[Edges.scala 232:25]
  reg [2:0] _T_534; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [2:0] _T_535; // @[Monitor.scala 382:22]
  reg [31:0] _RAND_2;
  reg [1:0] _T_536; // @[Monitor.scala 383:22]
  reg [31:0] _RAND_3;
  reg [10:0] _T_537; // @[Monitor.scala 384:22]
  reg [31:0] _RAND_4;
  reg [11:0] _T_538; // @[Monitor.scala 385:22]
  reg [31:0] _RAND_5;
  wire  _T_539; // @[Monitor.scala 386:22]
  wire  _T_540; // @[Monitor.scala 386:19]
  wire  _T_541; // @[Monitor.scala 387:32]
  wire  _T_543; // @[Monitor.scala 44:11]
  wire  _T_544; // @[Monitor.scala 44:11]
  wire  _T_545; // @[Monitor.scala 388:32]
  wire  _T_547; // @[Monitor.scala 44:11]
  wire  _T_548; // @[Monitor.scala 44:11]
  wire  _T_549; // @[Monitor.scala 389:32]
  wire  _T_551; // @[Monitor.scala 44:11]
  wire  _T_552; // @[Monitor.scala 44:11]
  wire  _T_553; // @[Monitor.scala 390:32]
  wire  _T_555; // @[Monitor.scala 44:11]
  wire  _T_556; // @[Monitor.scala 44:11]
  wire  _T_557; // @[Monitor.scala 391:32]
  wire  _T_559; // @[Monitor.scala 44:11]
  wire  _T_560; // @[Monitor.scala 44:11]
  wire  _T_562; // @[Monitor.scala 393:20]
  wire  _T_563; // @[Decoupled.scala 40:37]
  reg  _T_571; // @[Edges.scala 230:27]
  reg [31:0] _RAND_6;
  wire  _T_573; // @[Edges.scala 231:28]
  wire  _T_574; // @[Edges.scala 232:25]
  reg [2:0] _T_582; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_7;
  reg [1:0] _T_584; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_8;
  reg [10:0] _T_585; // @[Monitor.scala 535:22]
  reg [31:0] _RAND_9;
  wire  _T_588; // @[Monitor.scala 538:22]
  wire  _T_589; // @[Monitor.scala 538:19]
  wire  _T_590; // @[Monitor.scala 539:29]
  wire  _T_592; // @[Monitor.scala 51:11]
  wire  _T_593; // @[Monitor.scala 51:11]
  wire  _T_598; // @[Monitor.scala 541:29]
  wire  _T_600; // @[Monitor.scala 51:11]
  wire  _T_601; // @[Monitor.scala 51:11]
  wire  _T_602; // @[Monitor.scala 542:29]
  wire  _T_604; // @[Monitor.scala 51:11]
  wire  _T_605; // @[Monitor.scala 51:11]
  wire  _T_615; // @[Monitor.scala 546:20]
  reg [1823:0] _T_616; // @[Monitor.scala 568:27]
  reg [1823:0] _RAND_10;
  reg  _T_626; // @[Edges.scala 230:27]
  reg [31:0] _RAND_11;
  wire  _T_628; // @[Edges.scala 231:28]
  wire  _T_629; // @[Edges.scala 232:25]
  reg  _T_645; // @[Edges.scala 230:27]
  reg [31:0] _RAND_12;
  wire  _T_647; // @[Edges.scala 231:28]
  wire  _T_648; // @[Edges.scala 232:25]
  wire  _T_658; // @[Monitor.scala 574:27]
  wire [2047:0] _T_660; // @[OneHot.scala 58:35]
  wire [1823:0] _T_661; // @[Monitor.scala 576:23]
  wire  _T_663; // @[Monitor.scala 576:14]
  wire  _T_665; // @[Monitor.scala 576:13]
  wire  _T_666; // @[Monitor.scala 576:13]
  wire [2047:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_670; // @[Monitor.scala 581:27]
  wire  _T_672; // @[Monitor.scala 581:75]
  wire  _T_673; // @[Monitor.scala 581:72]
  wire [2047:0] _T_674; // @[OneHot.scala 58:35]
  wire [1823:0] _T_675; // @[Monitor.scala 583:21]
  wire [1823:0] _T_676; // @[Monitor.scala 583:32]
  wire  _T_679; // @[Monitor.scala 51:11]
  wire  _T_680; // @[Monitor.scala 51:11]
  wire [2047:0] _GEN_16; // @[Monitor.scala 581:91]
  wire [1823:0] _T_681; // @[Monitor.scala 590:27]
  wire [1823:0] _T_682; // @[Monitor.scala 590:38]
  wire [1823:0] _T_683; // @[Monitor.scala 590:36]
  reg [31:0] _T_684; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_13;
  wire  _T_685; // @[Monitor.scala 595:23]
  wire  _T_686; // @[Monitor.scala 595:13]
  wire  _T_687; // @[Monitor.scala 595:36]
  wire  _T_688; // @[Monitor.scala 595:27]
  wire  _T_689; // @[Monitor.scala 595:56]
  wire  _T_690; // @[Monitor.scala 595:44]
  wire  _T_692; // @[Monitor.scala 595:12]
  wire  _T_693; // @[Monitor.scala 595:12]
  wire [31:0] _T_695; // @[Monitor.scala 597:26]
  wire  _T_698; // @[Monitor.scala 598:27]
  wire  _GEN_19; // @[Monitor.scala 44:11]
  wire  _GEN_35; // @[Monitor.scala 44:11]
  wire  _GEN_53; // @[Monitor.scala 44:11]
  wire  _GEN_65; // @[Monitor.scala 44:11]
  wire  _GEN_75; // @[Monitor.scala 44:11]
  wire  _GEN_85; // @[Monitor.scala 44:11]
  wire  _GEN_95; // @[Monitor.scala 44:11]
  wire  _GEN_105; // @[Monitor.scala 44:11]
  wire  _GEN_117; // @[Monitor.scala 51:11]
  wire  _GEN_121; // @[Monitor.scala 51:11]
  wire  _GEN_127; // @[Monitor.scala 51:11]
  wire  _GEN_133; // @[Monitor.scala 51:11]
  wire  _GEN_135; // @[Monitor.scala 51:11]
  wire  _GEN_137; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_10 = io_in_a_bits_source <= 11'h71f; // @[Parameters.scala 58:20]
  assign _T_14 = 6'h7 << io_in_a_bits_size; // @[package.scala 189:77]
  assign _T_16 = ~_T_14[2:0]; // @[package.scala 189:46]
  assign _GEN_18 = {{9'd0}, _T_16}; // @[Edges.scala 22:16]
  assign _T_17 = io_in_a_bits_address & _GEN_18; // @[Edges.scala 22:16]
  assign _T_18 = _T_17 == 12'h0; // @[Edges.scala 22:24]
  assign _T_19 = {{1'd0}, io_in_a_bits_size}; // @[Misc.scala 201:34]
  assign _T_21 = 4'h1 << _T_19[1:0]; // @[OneHot.scala 65:12]
  assign _T_23 = _T_21[2:0] | 3'h1; // @[Misc.scala 201:81]
  assign _T_24 = io_in_a_bits_size >= 2'h3; // @[Misc.scala 205:21]
  assign _T_27 = ~io_in_a_bits_address[2]; // @[Misc.scala 210:20]
  assign _T_29 = _T_23[2] & _T_27; // @[Misc.scala 214:38]
  assign _T_30 = _T_24 | _T_29; // @[Misc.scala 214:29]
  assign _T_32 = _T_23[2] & io_in_a_bits_address[2]; // @[Misc.scala 214:38]
  assign _T_33 = _T_24 | _T_32; // @[Misc.scala 214:29]
  assign _T_36 = ~io_in_a_bits_address[1]; // @[Misc.scala 210:20]
  assign _T_37 = _T_27 & _T_36; // @[Misc.scala 213:27]
  assign _T_38 = _T_23[1] & _T_37; // @[Misc.scala 214:38]
  assign _T_39 = _T_30 | _T_38; // @[Misc.scala 214:29]
  assign _T_40 = _T_27 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_41 = _T_23[1] & _T_40; // @[Misc.scala 214:38]
  assign _T_42 = _T_30 | _T_41; // @[Misc.scala 214:29]
  assign _T_43 = io_in_a_bits_address[2] & _T_36; // @[Misc.scala 213:27]
  assign _T_44 = _T_23[1] & _T_43; // @[Misc.scala 214:38]
  assign _T_45 = _T_33 | _T_44; // @[Misc.scala 214:29]
  assign _T_46 = io_in_a_bits_address[2] & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_47 = _T_23[1] & _T_46; // @[Misc.scala 214:38]
  assign _T_48 = _T_33 | _T_47; // @[Misc.scala 214:29]
  assign _T_51 = ~io_in_a_bits_address[0]; // @[Misc.scala 210:20]
  assign _T_52 = _T_37 & _T_51; // @[Misc.scala 213:27]
  assign _T_53 = _T_23[0] & _T_52; // @[Misc.scala 214:38]
  assign _T_54 = _T_39 | _T_53; // @[Misc.scala 214:29]
  assign _T_55 = _T_37 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_56 = _T_23[0] & _T_55; // @[Misc.scala 214:38]
  assign _T_57 = _T_39 | _T_56; // @[Misc.scala 214:29]
  assign _T_58 = _T_40 & _T_51; // @[Misc.scala 213:27]
  assign _T_59 = _T_23[0] & _T_58; // @[Misc.scala 214:38]
  assign _T_60 = _T_42 | _T_59; // @[Misc.scala 214:29]
  assign _T_61 = _T_40 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_62 = _T_23[0] & _T_61; // @[Misc.scala 214:38]
  assign _T_63 = _T_42 | _T_62; // @[Misc.scala 214:29]
  assign _T_64 = _T_43 & _T_51; // @[Misc.scala 213:27]
  assign _T_65 = _T_23[0] & _T_64; // @[Misc.scala 214:38]
  assign _T_66 = _T_45 | _T_65; // @[Misc.scala 214:29]
  assign _T_67 = _T_43 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_68 = _T_23[0] & _T_67; // @[Misc.scala 214:38]
  assign _T_69 = _T_45 | _T_68; // @[Misc.scala 214:29]
  assign _T_70 = _T_46 & _T_51; // @[Misc.scala 213:27]
  assign _T_71 = _T_23[0] & _T_70; // @[Misc.scala 214:38]
  assign _T_72 = _T_48 | _T_71; // @[Misc.scala 214:29]
  assign _T_73 = _T_46 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_74 = _T_23[0] & _T_73; // @[Misc.scala 214:38]
  assign _T_75 = _T_48 | _T_74; // @[Misc.scala 214:29]
  assign _T_82 = {_T_75,_T_72,_T_69,_T_66,_T_63,_T_60,_T_57,_T_54}; // @[Cat.scala 29:58]
  assign _T_93 = {1'b0,$signed(io_in_a_bits_address)}; // @[Parameters.scala 137:49]
  assign _T_101 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_106 = $signed(_T_93) & -13'sh1000; // @[Parameters.scala 137:52]
  assign _T_107 = $signed(_T_106) == 13'sh0; // @[Parameters.scala 137:67]
  assign _T_112 = ~reset; // @[Monitor.scala 44:11]
  assign _T_117 = _T_10 | reset; // @[Monitor.scala 44:11]
  assign _T_118 = ~_T_117; // @[Monitor.scala 44:11]
  assign _T_121 = _T_24 | reset; // @[Monitor.scala 44:11]
  assign _T_122 = ~_T_121; // @[Monitor.scala 44:11]
  assign _T_124 = _T_18 | reset; // @[Monitor.scala 44:11]
  assign _T_125 = ~_T_124; // @[Monitor.scala 44:11]
  assign _T_126 = io_in_a_bits_param <= 3'h2; // @[Bundles.scala 110:27]
  assign _T_128 = _T_126 | reset; // @[Monitor.scala 44:11]
  assign _T_129 = ~_T_128; // @[Monitor.scala 44:11]
  assign _T_130 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_131 = _T_130 == 8'h0; // @[Monitor.scala 86:31]
  assign _T_133 = _T_131 | reset; // @[Monitor.scala 44:11]
  assign _T_134 = ~_T_133; // @[Monitor.scala 44:11]
  assign _T_135 = ~io_in_a_bits_corrupt; // @[Monitor.scala 87:18]
  assign _T_137 = _T_135 | reset; // @[Monitor.scala 44:11]
  assign _T_138 = ~_T_137; // @[Monitor.scala 44:11]
  assign _T_139 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_168 = io_in_a_bits_param != 3'h0; // @[Monitor.scala 97:31]
  assign _T_170 = _T_168 | reset; // @[Monitor.scala 44:11]
  assign _T_171 = ~_T_170; // @[Monitor.scala 44:11]
  assign _T_181 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_194 = _T_107 | reset; // @[Monitor.scala 44:11]
  assign _T_195 = ~_T_194; // @[Monitor.scala 44:11]
  assign _T_202 = io_in_a_bits_param == 3'h0; // @[Monitor.scala 106:31]
  assign _T_204 = _T_202 | reset; // @[Monitor.scala 44:11]
  assign _T_205 = ~_T_204; // @[Monitor.scala 44:11]
  assign _T_206 = io_in_a_bits_mask == _T_82; // @[Monitor.scala 107:30]
  assign _T_208 = _T_206 | reset; // @[Monitor.scala 44:11]
  assign _T_209 = ~_T_208; // @[Monitor.scala 44:11]
  assign _T_214 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_243 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_268 = ~_T_82; // @[Monitor.scala 124:33]
  assign _T_269 = io_in_a_bits_mask & _T_268; // @[Monitor.scala 124:31]
  assign _T_270 = _T_269 == 8'h0; // @[Monitor.scala 124:40]
  assign _T_272 = _T_270 | reset; // @[Monitor.scala 44:11]
  assign _T_273 = ~_T_272; // @[Monitor.scala 44:11]
  assign _T_274 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_292 = io_in_a_bits_param <= 3'h4; // @[Bundles.scala 140:33]
  assign _T_294 = _T_292 | reset; // @[Monitor.scala 44:11]
  assign _T_295 = ~_T_294; // @[Monitor.scala 44:11]
  assign _T_300 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_318 = io_in_a_bits_param <= 3'h3; // @[Bundles.scala 147:30]
  assign _T_320 = _T_318 | reset; // @[Monitor.scala 44:11]
  assign _T_321 = ~_T_320; // @[Monitor.scala 44:11]
  assign _T_326 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_344 = io_in_a_bits_param <= 3'h1; // @[Bundles.scala 160:28]
  assign _T_346 = _T_344 | reset; // @[Monitor.scala 44:11]
  assign _T_347 = ~_T_346; // @[Monitor.scala 44:11]
  assign _T_356 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_358 = _T_356 | reset; // @[Monitor.scala 51:11]
  assign _T_359 = ~_T_358; // @[Monitor.scala 51:11]
  assign _T_366 = io_in_d_bits_source <= 11'h71f; // @[Parameters.scala 58:20]
  assign _T_370 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_372 = _T_366 | reset; // @[Monitor.scala 51:11]
  assign _T_373 = ~_T_372; // @[Monitor.scala 51:11]
  assign _T_374 = io_in_d_bits_size >= 2'h3; // @[Monitor.scala 309:27]
  assign _T_376 = _T_374 | reset; // @[Monitor.scala 51:11]
  assign _T_377 = ~_T_376; // @[Monitor.scala 51:11]
  assign _T_390 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_418 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_447 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_464 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_482 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_514 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_525 = _T_523 - 1'h1; // @[Edges.scala 231:28]
  assign _T_526 = ~_T_523; // @[Edges.scala 232:25]
  assign _T_539 = ~_T_526; // @[Monitor.scala 386:22]
  assign _T_540 = io_in_a_valid & _T_539; // @[Monitor.scala 386:19]
  assign _T_541 = io_in_a_bits_opcode == _T_534; // @[Monitor.scala 387:32]
  assign _T_543 = _T_541 | reset; // @[Monitor.scala 44:11]
  assign _T_544 = ~_T_543; // @[Monitor.scala 44:11]
  assign _T_545 = io_in_a_bits_param == _T_535; // @[Monitor.scala 388:32]
  assign _T_547 = _T_545 | reset; // @[Monitor.scala 44:11]
  assign _T_548 = ~_T_547; // @[Monitor.scala 44:11]
  assign _T_549 = io_in_a_bits_size == _T_536; // @[Monitor.scala 389:32]
  assign _T_551 = _T_549 | reset; // @[Monitor.scala 44:11]
  assign _T_552 = ~_T_551; // @[Monitor.scala 44:11]
  assign _T_553 = io_in_a_bits_source == _T_537; // @[Monitor.scala 390:32]
  assign _T_555 = _T_553 | reset; // @[Monitor.scala 44:11]
  assign _T_556 = ~_T_555; // @[Monitor.scala 44:11]
  assign _T_557 = io_in_a_bits_address == _T_538; // @[Monitor.scala 391:32]
  assign _T_559 = _T_557 | reset; // @[Monitor.scala 44:11]
  assign _T_560 = ~_T_559; // @[Monitor.scala 44:11]
  assign _T_562 = _T_514 & _T_526; // @[Monitor.scala 393:20]
  assign _T_563 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_573 = _T_571 - 1'h1; // @[Edges.scala 231:28]
  assign _T_574 = ~_T_571; // @[Edges.scala 232:25]
  assign _T_588 = ~_T_574; // @[Monitor.scala 538:22]
  assign _T_589 = io_in_d_valid & _T_588; // @[Monitor.scala 538:19]
  assign _T_590 = io_in_d_bits_opcode == _T_582; // @[Monitor.scala 539:29]
  assign _T_592 = _T_590 | reset; // @[Monitor.scala 51:11]
  assign _T_593 = ~_T_592; // @[Monitor.scala 51:11]
  assign _T_598 = io_in_d_bits_size == _T_584; // @[Monitor.scala 541:29]
  assign _T_600 = _T_598 | reset; // @[Monitor.scala 51:11]
  assign _T_601 = ~_T_600; // @[Monitor.scala 51:11]
  assign _T_602 = io_in_d_bits_source == _T_585; // @[Monitor.scala 542:29]
  assign _T_604 = _T_602 | reset; // @[Monitor.scala 51:11]
  assign _T_605 = ~_T_604; // @[Monitor.scala 51:11]
  assign _T_615 = _T_563 & _T_574; // @[Monitor.scala 546:20]
  assign _T_628 = _T_626 - 1'h1; // @[Edges.scala 231:28]
  assign _T_629 = ~_T_626; // @[Edges.scala 232:25]
  assign _T_647 = _T_645 - 1'h1; // @[Edges.scala 231:28]
  assign _T_648 = ~_T_645; // @[Edges.scala 232:25]
  assign _T_658 = _T_514 & _T_629; // @[Monitor.scala 574:27]
  assign _T_660 = 2048'h1 << io_in_a_bits_source; // @[OneHot.scala 58:35]
  assign _T_661 = _T_616 >> io_in_a_bits_source; // @[Monitor.scala 576:23]
  assign _T_663 = ~_T_661[0]; // @[Monitor.scala 576:14]
  assign _T_665 = _T_663 | reset; // @[Monitor.scala 576:13]
  assign _T_666 = ~_T_665; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_658 ? _T_660 : 2048'h0; // @[Monitor.scala 574:72]
  assign _T_670 = _T_563 & _T_648; // @[Monitor.scala 581:27]
  assign _T_672 = ~_T_370; // @[Monitor.scala 581:75]
  assign _T_673 = _T_670 & _T_672; // @[Monitor.scala 581:72]
  assign _T_674 = 2048'h1 << io_in_d_bits_source; // @[OneHot.scala 58:35]
  assign _T_675 = _GEN_15[1823:0] | _T_616; // @[Monitor.scala 583:21]
  assign _T_676 = _T_675 >> io_in_d_bits_source; // @[Monitor.scala 583:32]
  assign _T_679 = _T_676[0] | reset; // @[Monitor.scala 51:11]
  assign _T_680 = ~_T_679; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_673 ? _T_674 : 2048'h0; // @[Monitor.scala 581:91]
  assign _T_681 = _T_616 | _GEN_15[1823:0]; // @[Monitor.scala 590:27]
  assign _T_682 = ~_GEN_16[1823:0]; // @[Monitor.scala 590:38]
  assign _T_683 = _T_681 & _T_682; // @[Monitor.scala 590:36]
  assign _T_685 = _T_616 != 1824'h0; // @[Monitor.scala 595:23]
  assign _T_686 = ~_T_685; // @[Monitor.scala 595:13]
  assign _T_687 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_688 = _T_686 | _T_687; // @[Monitor.scala 595:27]
  assign _T_689 = _T_684 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_690 = _T_688 | _T_689; // @[Monitor.scala 595:44]
  assign _T_692 = _T_690 | reset; // @[Monitor.scala 595:12]
  assign _T_693 = ~_T_692; // @[Monitor.scala 595:12]
  assign _T_695 = _T_684 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_698 = _T_514 | _T_563; // @[Monitor.scala 598:27]
  assign _GEN_19 = io_in_a_valid & _T_101; // @[Monitor.scala 44:11]
  assign _GEN_35 = io_in_a_valid & _T_139; // @[Monitor.scala 44:11]
  assign _GEN_53 = io_in_a_valid & _T_181; // @[Monitor.scala 44:11]
  assign _GEN_65 = io_in_a_valid & _T_214; // @[Monitor.scala 44:11]
  assign _GEN_75 = io_in_a_valid & _T_243; // @[Monitor.scala 44:11]
  assign _GEN_85 = io_in_a_valid & _T_274; // @[Monitor.scala 44:11]
  assign _GEN_95 = io_in_a_valid & _T_300; // @[Monitor.scala 44:11]
  assign _GEN_105 = io_in_a_valid & _T_326; // @[Monitor.scala 44:11]
  assign _GEN_117 = io_in_d_valid & _T_370; // @[Monitor.scala 51:11]
  assign _GEN_121 = io_in_d_valid & _T_390; // @[Monitor.scala 51:11]
  assign _GEN_127 = io_in_d_valid & _T_418; // @[Monitor.scala 51:11]
  assign _GEN_133 = io_in_d_valid & _T_447; // @[Monitor.scala 51:11]
  assign _GEN_135 = io_in_d_valid & _T_464; // @[Monitor.scala 51:11]
  assign _GEN_137 = io_in_d_valid & _T_482; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_523 = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_534 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_535 = _RAND_2[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_536 = _RAND_3[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_537 = _RAND_4[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  _T_538 = _RAND_5[11:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_571 = _RAND_6[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_582 = _RAND_7[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_584 = _RAND_8[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_585 = _RAND_9[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {57{`RANDOM}};
  _T_616 = _RAND_10[1823:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_626 = _RAND_11[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_645 = _RAND_12[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  _T_684 = _RAND_13[31:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_523 <= 1'h0;
    end else if (_T_514) begin
      if (_T_526) begin
        _T_523 <= 1'h0;
      end else begin
        _T_523 <= _T_525;
      end
    end
    if (_T_562) begin
      _T_534 <= io_in_a_bits_opcode;
    end
    if (_T_562) begin
      _T_535 <= io_in_a_bits_param;
    end
    if (_T_562) begin
      _T_536 <= io_in_a_bits_size;
    end
    if (_T_562) begin
      _T_537 <= io_in_a_bits_source;
    end
    if (_T_562) begin
      _T_538 <= io_in_a_bits_address;
    end
    if (reset) begin
      _T_571 <= 1'h0;
    end else if (_T_563) begin
      if (_T_574) begin
        _T_571 <= 1'h0;
      end else begin
        _T_571 <= _T_573;
      end
    end
    if (_T_615) begin
      _T_582 <= io_in_d_bits_opcode;
    end
    if (_T_615) begin
      _T_584 <= io_in_d_bits_size;
    end
    if (_T_615) begin
      _T_585 <= io_in_d_bits_source;
    end
    if (reset) begin
      _T_616 <= 1824'h0;
    end else begin
      _T_616 <= _T_683;
    end
    if (reset) begin
      _T_626 <= 1'h0;
    end else if (_T_514) begin
      if (_T_629) begin
        _T_626 <= 1'h0;
      end else begin
        _T_626 <= _T_628;
      end
    end
    if (reset) begin
      _T_645 <= 1'h0;
    end else if (_T_563) begin
      if (_T_648) begin
        _T_645 <= 1'h0;
      end else begin
        _T_645 <= _T_647;
      end
    end
    if (reset) begin
      _T_684 <= 32'h0;
    end else if (_T_698) begin
      _T_684 <= 32'h0;
    end else begin
      _T_684 <= _T_695;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_112) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_112) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid source ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_122) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock smaller than a beat (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_122) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_129) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid grow param (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_129) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_134) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_134) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_138) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock is corrupt (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_138) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_112) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_112) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid source ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_122) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm smaller than a beat (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_122) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_129) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid grow param (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_129) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_171) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_171) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_134) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_134) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_138) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm is corrupt (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_138) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_195) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_195) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid source ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_205) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid param (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_205) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_209) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_209) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_138) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get is corrupt (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_138) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_195) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_195) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid source ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_205) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid param (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_205) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_209) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_209) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_195) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_195) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid source ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_205) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid param (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_205) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_273) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial contains invalid mask (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_273) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_112) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid source ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_295) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid opcode param (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_295) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_209) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_209) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_112) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid source ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_321) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid opcode param (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_321) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_209) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_209) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_112) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_118) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid source ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_118) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_125) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_125) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_347) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid opcode param (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_347) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_209) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_209) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_138) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint is corrupt (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_138) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_359) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_359) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_373) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck carries invalid source ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_373) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_377) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_377) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_121 & _T_373) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid source ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_121 & _T_373) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_121 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid sink ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_121 & _T_112) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_121 & _T_377) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_121 & _T_377) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_373) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid source ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_373) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_112) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid sink ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_112) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_377) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_377) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_133 & _T_373) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid source ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_133 & _T_373) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_135 & _T_373) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid source ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_135 & _T_373) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_137 & _T_373) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid source ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_137 & _T_373) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_540 & _T_544) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_540 & _T_544) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_540 & _T_548) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel param changed within multibeat operation (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_540 & _T_548) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_540 & _T_552) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel size changed within multibeat operation (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_540 & _T_552) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_540 & _T_556) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel source changed within multibeat operation (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_540 & _T_556) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_540 & _T_560) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at Periphery.scala:84:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_540 & _T_560) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_589 & _T_593) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_589 & _T_593) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_589 & _T_601) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_589 & _T_601) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_589 & _T_605) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel source changed within multibeat operation (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_589 & _T_605) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_658 & _T_666) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at Periphery.scala:84:16)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_658 & _T_666) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_673 & _T_680) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at Periphery.scala:84:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_673 & _T_680) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_693) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at Periphery.scala:84:16)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_693) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
