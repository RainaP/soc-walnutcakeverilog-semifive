//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_66_assert(
  input        clock,
  input        reset,
  input        io_in_a_ready,
  input        io_in_a_valid,
  input  [2:0] io_in_a_bits_opcode,
  input  [2:0] io_in_a_bits_param,
  input  [1:0] io_in_a_bits_size,
  input        io_in_a_bits_source,
  input  [8:0] io_in_a_bits_address,
  input  [3:0] io_in_a_bits_mask,
  input        io_in_a_bits_corrupt,
  input        io_in_d_ready,
  input        io_in_d_valid,
  input  [2:0] io_in_d_bits_opcode,
  input  [1:0] io_in_d_bits_size,
  input        io_in_d_bits_source
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire  _T_4; // @[Parameters.scala 47:9]
  wire [4:0] _T_7; // @[package.scala 189:77]
  wire [1:0] _T_9; // @[package.scala 189:46]
  wire [8:0] _GEN_18; // @[Edges.scala 22:16]
  wire [8:0] _T_10; // @[Edges.scala 22:16]
  wire  _T_11; // @[Edges.scala 22:24]
  wire [1:0] _T_14; // @[OneHot.scala 65:12]
  wire [1:0] _T_16; // @[Misc.scala 201:81]
  wire  _T_17; // @[Misc.scala 205:21]
  wire  _T_20; // @[Misc.scala 210:20]
  wire  _T_22; // @[Misc.scala 214:38]
  wire  _T_23; // @[Misc.scala 214:29]
  wire  _T_25; // @[Misc.scala 214:38]
  wire  _T_26; // @[Misc.scala 214:29]
  wire  _T_29; // @[Misc.scala 210:20]
  wire  _T_30; // @[Misc.scala 213:27]
  wire  _T_31; // @[Misc.scala 214:38]
  wire  _T_32; // @[Misc.scala 214:29]
  wire  _T_33; // @[Misc.scala 213:27]
  wire  _T_34; // @[Misc.scala 214:38]
  wire  _T_35; // @[Misc.scala 214:29]
  wire  _T_36; // @[Misc.scala 213:27]
  wire  _T_37; // @[Misc.scala 214:38]
  wire  _T_38; // @[Misc.scala 214:29]
  wire  _T_39; // @[Misc.scala 213:27]
  wire  _T_40; // @[Misc.scala 214:38]
  wire  _T_41; // @[Misc.scala 214:29]
  wire [3:0] _T_44; // @[Cat.scala 29:58]
  wire [9:0] _T_48; // @[Parameters.scala 137:49]
  wire  _T_56; // @[Monitor.scala 79:25]
  wire [9:0] _T_61; // @[Parameters.scala 137:52]
  wire  _T_62; // @[Parameters.scala 137:67]
  wire [8:0] _T_63; // @[Parameters.scala 137:31]
  wire [9:0] _T_64; // @[Parameters.scala 137:49]
  wire [9:0] _T_66; // @[Parameters.scala 137:52]
  wire  _T_67; // @[Parameters.scala 137:67]
  wire [8:0] _T_68; // @[Parameters.scala 137:31]
  wire [9:0] _T_69; // @[Parameters.scala 137:49]
  wire [9:0] _T_71; // @[Parameters.scala 137:52]
  wire  _T_72; // @[Parameters.scala 137:67]
  wire [8:0] _T_73; // @[Parameters.scala 137:31]
  wire [9:0] _T_74; // @[Parameters.scala 137:49]
  wire [9:0] _T_76; // @[Parameters.scala 137:52]
  wire  _T_77; // @[Parameters.scala 137:67]
  wire [8:0] _T_78; // @[Parameters.scala 137:31]
  wire [9:0] _T_79; // @[Parameters.scala 137:49]
  wire [9:0] _T_81; // @[Parameters.scala 137:52]
  wire  _T_82; // @[Parameters.scala 137:67]
  wire [8:0] _T_83; // @[Parameters.scala 137:31]
  wire [9:0] _T_84; // @[Parameters.scala 137:49]
  wire [9:0] _T_86; // @[Parameters.scala 137:52]
  wire  _T_87; // @[Parameters.scala 137:67]
  wire  _T_88; // @[Parameters.scala 552:42]
  wire  _T_89; // @[Parameters.scala 552:42]
  wire  _T_90; // @[Parameters.scala 552:42]
  wire  _T_91; // @[Parameters.scala 552:42]
  wire  _T_92; // @[Parameters.scala 552:42]
  wire  _T_97; // @[Monitor.scala 44:11]
  wire  _T_102; // @[Monitor.scala 44:11]
  wire  _T_103; // @[Monitor.scala 44:11]
  wire  _T_106; // @[Monitor.scala 44:11]
  wire  _T_107; // @[Monitor.scala 44:11]
  wire  _T_109; // @[Monitor.scala 44:11]
  wire  _T_110; // @[Monitor.scala 44:11]
  wire  _T_111; // @[Bundles.scala 110:27]
  wire  _T_113; // @[Monitor.scala 44:11]
  wire  _T_114; // @[Monitor.scala 44:11]
  wire [3:0] _T_115; // @[Monitor.scala 86:18]
  wire  _T_116; // @[Monitor.scala 86:31]
  wire  _T_118; // @[Monitor.scala 44:11]
  wire  _T_119; // @[Monitor.scala 44:11]
  wire  _T_120; // @[Monitor.scala 87:18]
  wire  _T_122; // @[Monitor.scala 44:11]
  wire  _T_123; // @[Monitor.scala 44:11]
  wire  _T_124; // @[Monitor.scala 90:25]
  wire  _T_183; // @[Monitor.scala 97:31]
  wire  _T_185; // @[Monitor.scala 44:11]
  wire  _T_186; // @[Monitor.scala 44:11]
  wire  _T_196; // @[Monitor.scala 102:25]
  wire  _T_198; // @[Parameters.scala 93:42]
  wire  _T_236; // @[Parameters.scala 551:56]
  wire  _T_239; // @[Monitor.scala 44:11]
  wire  _T_240; // @[Monitor.scala 44:11]
  wire  _T_247; // @[Monitor.scala 106:31]
  wire  _T_249; // @[Monitor.scala 44:11]
  wire  _T_250; // @[Monitor.scala 44:11]
  wire  _T_251; // @[Monitor.scala 107:30]
  wire  _T_253; // @[Monitor.scala 44:11]
  wire  _T_254; // @[Monitor.scala 44:11]
  wire  _T_259; // @[Monitor.scala 111:25]
  wire  _T_318; // @[Monitor.scala 119:25]
  wire [3:0] _T_373; // @[Monitor.scala 124:33]
  wire [3:0] _T_374; // @[Monitor.scala 124:31]
  wire  _T_375; // @[Monitor.scala 124:40]
  wire  _T_377; // @[Monitor.scala 44:11]
  wire  _T_378; // @[Monitor.scala 44:11]
  wire  _T_379; // @[Monitor.scala 127:25]
  wire  _T_427; // @[Bundles.scala 140:33]
  wire  _T_429; // @[Monitor.scala 44:11]
  wire  _T_430; // @[Monitor.scala 44:11]
  wire  _T_435; // @[Monitor.scala 135:25]
  wire  _T_483; // @[Bundles.scala 147:30]
  wire  _T_485; // @[Monitor.scala 44:11]
  wire  _T_486; // @[Monitor.scala 44:11]
  wire  _T_491; // @[Monitor.scala 143:25]
  wire  _T_539; // @[Bundles.scala 160:28]
  wire  _T_541; // @[Monitor.scala 44:11]
  wire  _T_542; // @[Monitor.scala 44:11]
  wire  _T_551; // @[Bundles.scala 44:24]
  wire  _T_553; // @[Monitor.scala 51:11]
  wire  _T_554; // @[Monitor.scala 51:11]
  wire  _T_555; // @[Parameters.scala 47:9]
  wire  _T_558; // @[Monitor.scala 307:25]
  wire  _T_560; // @[Monitor.scala 51:11]
  wire  _T_561; // @[Monitor.scala 51:11]
  wire  _T_562; // @[Monitor.scala 309:27]
  wire  _T_564; // @[Monitor.scala 51:11]
  wire  _T_565; // @[Monitor.scala 51:11]
  wire  _T_578; // @[Monitor.scala 315:25]
  wire  _T_606; // @[Monitor.scala 325:25]
  wire  _T_635; // @[Monitor.scala 335:25]
  wire  _T_652; // @[Monitor.scala 343:25]
  wire  _T_670; // @[Monitor.scala 351:25]
  wire  _T_702; // @[Decoupled.scala 40:37]
  reg  _T_711; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire  _T_713; // @[Edges.scala 231:28]
  wire  _T_714; // @[Edges.scala 232:25]
  reg [2:0] _T_722; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [2:0] _T_723; // @[Monitor.scala 382:22]
  reg [31:0] _RAND_2;
  reg [1:0] _T_724; // @[Monitor.scala 383:22]
  reg [31:0] _RAND_3;
  reg  _T_725; // @[Monitor.scala 384:22]
  reg [31:0] _RAND_4;
  reg [8:0] _T_726; // @[Monitor.scala 385:22]
  reg [31:0] _RAND_5;
  wire  _T_727; // @[Monitor.scala 386:22]
  wire  _T_728; // @[Monitor.scala 386:19]
  wire  _T_729; // @[Monitor.scala 387:32]
  wire  _T_731; // @[Monitor.scala 44:11]
  wire  _T_732; // @[Monitor.scala 44:11]
  wire  _T_733; // @[Monitor.scala 388:32]
  wire  _T_735; // @[Monitor.scala 44:11]
  wire  _T_736; // @[Monitor.scala 44:11]
  wire  _T_737; // @[Monitor.scala 389:32]
  wire  _T_739; // @[Monitor.scala 44:11]
  wire  _T_740; // @[Monitor.scala 44:11]
  wire  _T_741; // @[Monitor.scala 390:32]
  wire  _T_743; // @[Monitor.scala 44:11]
  wire  _T_744; // @[Monitor.scala 44:11]
  wire  _T_745; // @[Monitor.scala 391:32]
  wire  _T_747; // @[Monitor.scala 44:11]
  wire  _T_748; // @[Monitor.scala 44:11]
  wire  _T_750; // @[Monitor.scala 393:20]
  wire  _T_751; // @[Decoupled.scala 40:37]
  reg  _T_759; // @[Edges.scala 230:27]
  reg [31:0] _RAND_6;
  wire  _T_761; // @[Edges.scala 231:28]
  wire  _T_762; // @[Edges.scala 232:25]
  reg [2:0] _T_770; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_7;
  reg [1:0] _T_772; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_8;
  reg  _T_773; // @[Monitor.scala 535:22]
  reg [31:0] _RAND_9;
  wire  _T_776; // @[Monitor.scala 538:22]
  wire  _T_777; // @[Monitor.scala 538:19]
  wire  _T_778; // @[Monitor.scala 539:29]
  wire  _T_780; // @[Monitor.scala 51:11]
  wire  _T_781; // @[Monitor.scala 51:11]
  wire  _T_786; // @[Monitor.scala 541:29]
  wire  _T_788; // @[Monitor.scala 51:11]
  wire  _T_789; // @[Monitor.scala 51:11]
  wire  _T_790; // @[Monitor.scala 542:29]
  wire  _T_792; // @[Monitor.scala 51:11]
  wire  _T_793; // @[Monitor.scala 51:11]
  wire  _T_803; // @[Monitor.scala 546:20]
  reg  _T_804; // @[Monitor.scala 568:27]
  reg [31:0] _RAND_10;
  reg  _T_814; // @[Edges.scala 230:27]
  reg [31:0] _RAND_11;
  wire  _T_816; // @[Edges.scala 231:28]
  wire  _T_817; // @[Edges.scala 232:25]
  reg  _T_833; // @[Edges.scala 230:27]
  reg [31:0] _RAND_12;
  wire  _T_835; // @[Edges.scala 231:28]
  wire  _T_836; // @[Edges.scala 232:25]
  wire  _T_846; // @[Monitor.scala 574:27]
  wire [1:0] _T_848; // @[OneHot.scala 58:35]
  wire  _T_849; // @[Monitor.scala 576:23]
  wire  _T_851; // @[Monitor.scala 576:14]
  wire  _T_853; // @[Monitor.scala 576:13]
  wire  _T_854; // @[Monitor.scala 576:13]
  wire [1:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_858; // @[Monitor.scala 581:27]
  wire  _T_860; // @[Monitor.scala 581:75]
  wire  _T_861; // @[Monitor.scala 581:72]
  wire [1:0] _T_862; // @[OneHot.scala 58:35]
  wire  _T_863; // @[Monitor.scala 583:21]
  wire  _T_864; // @[Monitor.scala 583:32]
  wire  _T_867; // @[Monitor.scala 51:11]
  wire  _T_868; // @[Monitor.scala 51:11]
  wire [1:0] _GEN_16; // @[Monitor.scala 581:91]
  wire  _T_869; // @[Monitor.scala 590:27]
  wire  _T_870; // @[Monitor.scala 590:38]
  wire  _T_871; // @[Monitor.scala 590:36]
  reg [31:0] _T_872; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_13;
  wire  _T_874; // @[Monitor.scala 595:13]
  wire  _T_875; // @[Monitor.scala 595:36]
  wire  _T_876; // @[Monitor.scala 595:27]
  wire  _T_877; // @[Monitor.scala 595:56]
  wire  _T_878; // @[Monitor.scala 595:44]
  wire  _T_880; // @[Monitor.scala 595:12]
  wire  _T_881; // @[Monitor.scala 595:12]
  wire [31:0] _T_883; // @[Monitor.scala 597:26]
  wire  _T_886; // @[Monitor.scala 598:27]
  wire  _GEN_19; // @[Monitor.scala 44:11]
  wire  _GEN_35; // @[Monitor.scala 44:11]
  wire  _GEN_53; // @[Monitor.scala 44:11]
  wire  _GEN_65; // @[Monitor.scala 44:11]
  wire  _GEN_75; // @[Monitor.scala 44:11]
  wire  _GEN_85; // @[Monitor.scala 44:11]
  wire  _GEN_95; // @[Monitor.scala 44:11]
  wire  _GEN_105; // @[Monitor.scala 44:11]
  wire  _GEN_117; // @[Monitor.scala 51:11]
  wire  _GEN_121; // @[Monitor.scala 51:11]
  wire  _GEN_127; // @[Monitor.scala 51:11]
  wire  _GEN_133; // @[Monitor.scala 51:11]
  wire  _GEN_135; // @[Monitor.scala 51:11]
  wire  _GEN_137; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_4 = ~io_in_a_bits_source; // @[Parameters.scala 47:9]
  assign _T_7 = 5'h3 << io_in_a_bits_size; // @[package.scala 189:77]
  assign _T_9 = ~_T_7[1:0]; // @[package.scala 189:46]
  assign _GEN_18 = {{7'd0}, _T_9}; // @[Edges.scala 22:16]
  assign _T_10 = io_in_a_bits_address & _GEN_18; // @[Edges.scala 22:16]
  assign _T_11 = _T_10 == 9'h0; // @[Edges.scala 22:24]
  assign _T_14 = 2'h1 << io_in_a_bits_size[0]; // @[OneHot.scala 65:12]
  assign _T_16 = _T_14 | 2'h1; // @[Misc.scala 201:81]
  assign _T_17 = io_in_a_bits_size >= 2'h2; // @[Misc.scala 205:21]
  assign _T_20 = ~io_in_a_bits_address[1]; // @[Misc.scala 210:20]
  assign _T_22 = _T_16[1] & _T_20; // @[Misc.scala 214:38]
  assign _T_23 = _T_17 | _T_22; // @[Misc.scala 214:29]
  assign _T_25 = _T_16[1] & io_in_a_bits_address[1]; // @[Misc.scala 214:38]
  assign _T_26 = _T_17 | _T_25; // @[Misc.scala 214:29]
  assign _T_29 = ~io_in_a_bits_address[0]; // @[Misc.scala 210:20]
  assign _T_30 = _T_20 & _T_29; // @[Misc.scala 213:27]
  assign _T_31 = _T_16[0] & _T_30; // @[Misc.scala 214:38]
  assign _T_32 = _T_23 | _T_31; // @[Misc.scala 214:29]
  assign _T_33 = _T_20 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_34 = _T_16[0] & _T_33; // @[Misc.scala 214:38]
  assign _T_35 = _T_23 | _T_34; // @[Misc.scala 214:29]
  assign _T_36 = io_in_a_bits_address[1] & _T_29; // @[Misc.scala 213:27]
  assign _T_37 = _T_16[0] & _T_36; // @[Misc.scala 214:38]
  assign _T_38 = _T_26 | _T_37; // @[Misc.scala 214:29]
  assign _T_39 = io_in_a_bits_address[1] & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_40 = _T_16[0] & _T_39; // @[Misc.scala 214:38]
  assign _T_41 = _T_26 | _T_40; // @[Misc.scala 214:29]
  assign _T_44 = {_T_41,_T_38,_T_35,_T_32}; // @[Cat.scala 29:58]
  assign _T_48 = {1'b0,$signed(io_in_a_bits_address)}; // @[Parameters.scala 137:49]
  assign _T_56 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_61 = $signed(_T_48) & -10'sh40; // @[Parameters.scala 137:52]
  assign _T_62 = $signed(_T_61) == 10'sh0; // @[Parameters.scala 137:67]
  assign _T_63 = io_in_a_bits_address ^ 9'h44; // @[Parameters.scala 137:31]
  assign _T_64 = {1'b0,$signed(_T_63)}; // @[Parameters.scala 137:49]
  assign _T_66 = $signed(_T_64) & -10'sh4; // @[Parameters.scala 137:52]
  assign _T_67 = $signed(_T_66) == 10'sh0; // @[Parameters.scala 137:67]
  assign _T_68 = io_in_a_bits_address ^ 9'h48; // @[Parameters.scala 137:31]
  assign _T_69 = {1'b0,$signed(_T_68)}; // @[Parameters.scala 137:49]
  assign _T_71 = $signed(_T_69) & -10'sh18; // @[Parameters.scala 137:52]
  assign _T_72 = $signed(_T_71) == 10'sh0; // @[Parameters.scala 137:67]
  assign _T_73 = io_in_a_bits_address ^ 9'h60; // @[Parameters.scala 137:31]
  assign _T_74 = {1'b0,$signed(_T_73)}; // @[Parameters.scala 137:49]
  assign _T_76 = $signed(_T_74) & -10'sh20; // @[Parameters.scala 137:52]
  assign _T_77 = $signed(_T_76) == 10'sh0; // @[Parameters.scala 137:67]
  assign _T_78 = io_in_a_bits_address ^ 9'h80; // @[Parameters.scala 137:31]
  assign _T_79 = {1'b0,$signed(_T_78)}; // @[Parameters.scala 137:49]
  assign _T_81 = $signed(_T_79) & -10'sh80; // @[Parameters.scala 137:52]
  assign _T_82 = $signed(_T_81) == 10'sh0; // @[Parameters.scala 137:67]
  assign _T_83 = io_in_a_bits_address ^ 9'h100; // @[Parameters.scala 137:31]
  assign _T_84 = {1'b0,$signed(_T_83)}; // @[Parameters.scala 137:49]
  assign _T_86 = $signed(_T_84) & -10'sh100; // @[Parameters.scala 137:52]
  assign _T_87 = $signed(_T_86) == 10'sh0; // @[Parameters.scala 137:67]
  assign _T_88 = _T_62 | _T_67; // @[Parameters.scala 552:42]
  assign _T_89 = _T_88 | _T_72; // @[Parameters.scala 552:42]
  assign _T_90 = _T_89 | _T_77; // @[Parameters.scala 552:42]
  assign _T_91 = _T_90 | _T_82; // @[Parameters.scala 552:42]
  assign _T_92 = _T_91 | _T_87; // @[Parameters.scala 552:42]
  assign _T_97 = ~reset; // @[Monitor.scala 44:11]
  assign _T_102 = _T_4 | reset; // @[Monitor.scala 44:11]
  assign _T_103 = ~_T_102; // @[Monitor.scala 44:11]
  assign _T_106 = _T_17 | reset; // @[Monitor.scala 44:11]
  assign _T_107 = ~_T_106; // @[Monitor.scala 44:11]
  assign _T_109 = _T_11 | reset; // @[Monitor.scala 44:11]
  assign _T_110 = ~_T_109; // @[Monitor.scala 44:11]
  assign _T_111 = io_in_a_bits_param <= 3'h2; // @[Bundles.scala 110:27]
  assign _T_113 = _T_111 | reset; // @[Monitor.scala 44:11]
  assign _T_114 = ~_T_113; // @[Monitor.scala 44:11]
  assign _T_115 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_116 = _T_115 == 4'h0; // @[Monitor.scala 86:31]
  assign _T_118 = _T_116 | reset; // @[Monitor.scala 44:11]
  assign _T_119 = ~_T_118; // @[Monitor.scala 44:11]
  assign _T_120 = ~io_in_a_bits_corrupt; // @[Monitor.scala 87:18]
  assign _T_122 = _T_120 | reset; // @[Monitor.scala 44:11]
  assign _T_123 = ~_T_122; // @[Monitor.scala 44:11]
  assign _T_124 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_183 = io_in_a_bits_param != 3'h0; // @[Monitor.scala 97:31]
  assign _T_185 = _T_183 | reset; // @[Monitor.scala 44:11]
  assign _T_186 = ~_T_185; // @[Monitor.scala 44:11]
  assign _T_196 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_198 = io_in_a_bits_size <= 2'h2; // @[Parameters.scala 93:42]
  assign _T_236 = _T_198 & _T_92; // @[Parameters.scala 551:56]
  assign _T_239 = _T_236 | reset; // @[Monitor.scala 44:11]
  assign _T_240 = ~_T_239; // @[Monitor.scala 44:11]
  assign _T_247 = io_in_a_bits_param == 3'h0; // @[Monitor.scala 106:31]
  assign _T_249 = _T_247 | reset; // @[Monitor.scala 44:11]
  assign _T_250 = ~_T_249; // @[Monitor.scala 44:11]
  assign _T_251 = io_in_a_bits_mask == _T_44; // @[Monitor.scala 107:30]
  assign _T_253 = _T_251 | reset; // @[Monitor.scala 44:11]
  assign _T_254 = ~_T_253; // @[Monitor.scala 44:11]
  assign _T_259 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_318 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_373 = ~_T_44; // @[Monitor.scala 124:33]
  assign _T_374 = io_in_a_bits_mask & _T_373; // @[Monitor.scala 124:31]
  assign _T_375 = _T_374 == 4'h0; // @[Monitor.scala 124:40]
  assign _T_377 = _T_375 | reset; // @[Monitor.scala 44:11]
  assign _T_378 = ~_T_377; // @[Monitor.scala 44:11]
  assign _T_379 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_427 = io_in_a_bits_param <= 3'h4; // @[Bundles.scala 140:33]
  assign _T_429 = _T_427 | reset; // @[Monitor.scala 44:11]
  assign _T_430 = ~_T_429; // @[Monitor.scala 44:11]
  assign _T_435 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_483 = io_in_a_bits_param <= 3'h3; // @[Bundles.scala 147:30]
  assign _T_485 = _T_483 | reset; // @[Monitor.scala 44:11]
  assign _T_486 = ~_T_485; // @[Monitor.scala 44:11]
  assign _T_491 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_539 = io_in_a_bits_param <= 3'h1; // @[Bundles.scala 160:28]
  assign _T_541 = _T_539 | reset; // @[Monitor.scala 44:11]
  assign _T_542 = ~_T_541; // @[Monitor.scala 44:11]
  assign _T_551 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_553 = _T_551 | reset; // @[Monitor.scala 51:11]
  assign _T_554 = ~_T_553; // @[Monitor.scala 51:11]
  assign _T_555 = ~io_in_d_bits_source; // @[Parameters.scala 47:9]
  assign _T_558 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_560 = _T_555 | reset; // @[Monitor.scala 51:11]
  assign _T_561 = ~_T_560; // @[Monitor.scala 51:11]
  assign _T_562 = io_in_d_bits_size >= 2'h2; // @[Monitor.scala 309:27]
  assign _T_564 = _T_562 | reset; // @[Monitor.scala 51:11]
  assign _T_565 = ~_T_564; // @[Monitor.scala 51:11]
  assign _T_578 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_606 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_635 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_652 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_670 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_702 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_713 = _T_711 - 1'h1; // @[Edges.scala 231:28]
  assign _T_714 = ~_T_711; // @[Edges.scala 232:25]
  assign _T_727 = ~_T_714; // @[Monitor.scala 386:22]
  assign _T_728 = io_in_a_valid & _T_727; // @[Monitor.scala 386:19]
  assign _T_729 = io_in_a_bits_opcode == _T_722; // @[Monitor.scala 387:32]
  assign _T_731 = _T_729 | reset; // @[Monitor.scala 44:11]
  assign _T_732 = ~_T_731; // @[Monitor.scala 44:11]
  assign _T_733 = io_in_a_bits_param == _T_723; // @[Monitor.scala 388:32]
  assign _T_735 = _T_733 | reset; // @[Monitor.scala 44:11]
  assign _T_736 = ~_T_735; // @[Monitor.scala 44:11]
  assign _T_737 = io_in_a_bits_size == _T_724; // @[Monitor.scala 389:32]
  assign _T_739 = _T_737 | reset; // @[Monitor.scala 44:11]
  assign _T_740 = ~_T_739; // @[Monitor.scala 44:11]
  assign _T_741 = io_in_a_bits_source == _T_725; // @[Monitor.scala 390:32]
  assign _T_743 = _T_741 | reset; // @[Monitor.scala 44:11]
  assign _T_744 = ~_T_743; // @[Monitor.scala 44:11]
  assign _T_745 = io_in_a_bits_address == _T_726; // @[Monitor.scala 391:32]
  assign _T_747 = _T_745 | reset; // @[Monitor.scala 44:11]
  assign _T_748 = ~_T_747; // @[Monitor.scala 44:11]
  assign _T_750 = _T_702 & _T_714; // @[Monitor.scala 393:20]
  assign _T_751 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_761 = _T_759 - 1'h1; // @[Edges.scala 231:28]
  assign _T_762 = ~_T_759; // @[Edges.scala 232:25]
  assign _T_776 = ~_T_762; // @[Monitor.scala 538:22]
  assign _T_777 = io_in_d_valid & _T_776; // @[Monitor.scala 538:19]
  assign _T_778 = io_in_d_bits_opcode == _T_770; // @[Monitor.scala 539:29]
  assign _T_780 = _T_778 | reset; // @[Monitor.scala 51:11]
  assign _T_781 = ~_T_780; // @[Monitor.scala 51:11]
  assign _T_786 = io_in_d_bits_size == _T_772; // @[Monitor.scala 541:29]
  assign _T_788 = _T_786 | reset; // @[Monitor.scala 51:11]
  assign _T_789 = ~_T_788; // @[Monitor.scala 51:11]
  assign _T_790 = io_in_d_bits_source == _T_773; // @[Monitor.scala 542:29]
  assign _T_792 = _T_790 | reset; // @[Monitor.scala 51:11]
  assign _T_793 = ~_T_792; // @[Monitor.scala 51:11]
  assign _T_803 = _T_751 & _T_762; // @[Monitor.scala 546:20]
  assign _T_816 = _T_814 - 1'h1; // @[Edges.scala 231:28]
  assign _T_817 = ~_T_814; // @[Edges.scala 232:25]
  assign _T_835 = _T_833 - 1'h1; // @[Edges.scala 231:28]
  assign _T_836 = ~_T_833; // @[Edges.scala 232:25]
  assign _T_846 = _T_702 & _T_817; // @[Monitor.scala 574:27]
  assign _T_848 = 2'h1 << io_in_a_bits_source; // @[OneHot.scala 58:35]
  assign _T_849 = _T_804 >> io_in_a_bits_source; // @[Monitor.scala 576:23]
  assign _T_851 = ~_T_849; // @[Monitor.scala 576:14]
  assign _T_853 = _T_851 | reset; // @[Monitor.scala 576:13]
  assign _T_854 = ~_T_853; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_846 ? _T_848 : 2'h0; // @[Monitor.scala 574:72]
  assign _T_858 = _T_751 & _T_836; // @[Monitor.scala 581:27]
  assign _T_860 = ~_T_558; // @[Monitor.scala 581:75]
  assign _T_861 = _T_858 & _T_860; // @[Monitor.scala 581:72]
  assign _T_862 = 2'h1 << io_in_d_bits_source; // @[OneHot.scala 58:35]
  assign _T_863 = _GEN_15[0] | _T_804; // @[Monitor.scala 583:21]
  assign _T_864 = _T_863 >> io_in_d_bits_source; // @[Monitor.scala 583:32]
  assign _T_867 = _T_864 | reset; // @[Monitor.scala 51:11]
  assign _T_868 = ~_T_867; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_861 ? _T_862 : 2'h0; // @[Monitor.scala 581:91]
  assign _T_869 = _T_804 | _GEN_15[0]; // @[Monitor.scala 590:27]
  assign _T_870 = ~_GEN_16[0]; // @[Monitor.scala 590:38]
  assign _T_871 = _T_869 & _T_870; // @[Monitor.scala 590:36]
  assign _T_874 = ~_T_804; // @[Monitor.scala 595:13]
  assign _T_875 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_876 = _T_874 | _T_875; // @[Monitor.scala 595:27]
  assign _T_877 = _T_872 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_878 = _T_876 | _T_877; // @[Monitor.scala 595:44]
  assign _T_880 = _T_878 | reset; // @[Monitor.scala 595:12]
  assign _T_881 = ~_T_880; // @[Monitor.scala 595:12]
  assign _T_883 = _T_872 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_886 = _T_702 | _T_751; // @[Monitor.scala 598:27]
  assign _GEN_19 = io_in_a_valid & _T_56; // @[Monitor.scala 44:11]
  assign _GEN_35 = io_in_a_valid & _T_124; // @[Monitor.scala 44:11]
  assign _GEN_53 = io_in_a_valid & _T_196; // @[Monitor.scala 44:11]
  assign _GEN_65 = io_in_a_valid & _T_259; // @[Monitor.scala 44:11]
  assign _GEN_75 = io_in_a_valid & _T_318; // @[Monitor.scala 44:11]
  assign _GEN_85 = io_in_a_valid & _T_379; // @[Monitor.scala 44:11]
  assign _GEN_95 = io_in_a_valid & _T_435; // @[Monitor.scala 44:11]
  assign _GEN_105 = io_in_a_valid & _T_491; // @[Monitor.scala 44:11]
  assign _GEN_117 = io_in_d_valid & _T_558; // @[Monitor.scala 51:11]
  assign _GEN_121 = io_in_d_valid & _T_578; // @[Monitor.scala 51:11]
  assign _GEN_127 = io_in_d_valid & _T_606; // @[Monitor.scala 51:11]
  assign _GEN_133 = io_in_d_valid & _T_635; // @[Monitor.scala 51:11]
  assign _GEN_135 = io_in_d_valid & _T_652; // @[Monitor.scala 51:11]
  assign _GEN_137 = io_in_d_valid & _T_670; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_711 = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_722 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_723 = _RAND_2[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_724 = _RAND_3[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_725 = _RAND_4[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  _T_726 = _RAND_5[8:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_759 = _RAND_6[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_770 = _RAND_7[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_772 = _RAND_8[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_773 = _RAND_9[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_804 = _RAND_10[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_814 = _RAND_11[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_833 = _RAND_12[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  _T_872 = _RAND_13[31:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_711 <= 1'h0;
    end else if (_T_702) begin
      if (_T_714) begin
        _T_711 <= 1'h0;
      end else begin
        _T_711 <= _T_713;
      end
    end
    if (_T_750) begin
      _T_722 <= io_in_a_bits_opcode;
    end
    if (_T_750) begin
      _T_723 <= io_in_a_bits_param;
    end
    if (_T_750) begin
      _T_724 <= io_in_a_bits_size;
    end
    if (_T_750) begin
      _T_725 <= io_in_a_bits_source;
    end
    if (_T_750) begin
      _T_726 <= io_in_a_bits_address;
    end
    if (reset) begin
      _T_759 <= 1'h0;
    end else if (_T_751) begin
      if (_T_762) begin
        _T_759 <= 1'h0;
      end else begin
        _T_759 <= _T_761;
      end
    end
    if (_T_803) begin
      _T_770 <= io_in_d_bits_opcode;
    end
    if (_T_803) begin
      _T_772 <= io_in_d_bits_size;
    end
    if (_T_803) begin
      _T_773 <= io_in_d_bits_source;
    end
    if (reset) begin
      _T_804 <= 1'h0;
    end else begin
      _T_804 <= _T_871;
    end
    if (reset) begin
      _T_814 <= 1'h0;
    end else if (_T_702) begin
      if (_T_817) begin
        _T_814 <= 1'h0;
      end else begin
        _T_814 <= _T_816;
      end
    end
    if (reset) begin
      _T_833 <= 1'h0;
    end else if (_T_751) begin
      if (_T_836) begin
        _T_833 <= 1'h0;
      end else begin
        _T_833 <= _T_835;
      end
    end
    if (reset) begin
      _T_872 <= 32'h0;
    end else if (_T_886) begin
      _T_872 <= 32'h0;
    end else begin
      _T_872 <= _T_883;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_97) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_97) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_103) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid source ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_103) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_107) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock smaller than a beat (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_107) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_114) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid grow param (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_114) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_119) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_119) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_123) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock is corrupt (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_123) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_97) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_97) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_103) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid source ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_103) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_107) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm smaller than a beat (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_107) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_114) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid grow param (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_114) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_186) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_186) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_119) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_119) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_35 & _T_123) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm is corrupt (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_35 & _T_123) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_240) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_240) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_103) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid source ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_103) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_250) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid param (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_250) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_254) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_254) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_53 & _T_123) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get is corrupt (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_53 & _T_123) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_240) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_240) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_103) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid source ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_103) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_250) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid param (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_250) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_65 & _T_254) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_65 & _T_254) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_240) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_240) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_103) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid source ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_103) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_250) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid param (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_250) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_378) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial contains invalid mask (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_378) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_97) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_103) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid source ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_103) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_430) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid opcode param (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_430) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_85 & _T_254) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_85 & _T_254) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_97) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_103) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid source ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_103) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_486) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid opcode param (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_486) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_95 & _T_254) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_95 & _T_254) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_97) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_103) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid source ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_103) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_110) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_110) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_542) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid opcode param (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_542) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_254) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_254) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_105 & _T_123) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint is corrupt (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_105 & _T_123) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_554) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_554) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_561) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck carries invalid source ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_561) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_117 & _T_565) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_117 & _T_565) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_121 & _T_561) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid source ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_121 & _T_561) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_121 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid sink ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_121 & _T_97) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_121 & _T_565) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_121 & _T_565) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_561) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid source ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_561) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_97) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid sink ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_97) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_127 & _T_565) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_127 & _T_565) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_133 & _T_561) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid source ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_133 & _T_561) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_135 & _T_561) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid source ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_135 & _T_561) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_137 & _T_561) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid source ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_137 & _T_561) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_728 & _T_732) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_728 & _T_732) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_728 & _T_736) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel param changed within multibeat operation (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_728 & _T_736) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_728 & _T_740) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel size changed within multibeat operation (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_728 & _T_740) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_728 & _T_744) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel source changed within multibeat operation (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_728 & _T_744) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_728 & _T_748) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at Debug.scala:1688:19)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_728 & _T_748) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_777 & _T_781) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_777 & _T_781) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_777 & _T_789) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_777 & _T_789) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_777 & _T_793) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel source changed within multibeat operation (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_777 & _T_793) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_846 & _T_854) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at Debug.scala:1688:19)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_846 & _T_854) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_861 & _T_868) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at Debug.scala:1688:19)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_861 & _T_868) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_881) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at Debug.scala:1688:19)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_881) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
