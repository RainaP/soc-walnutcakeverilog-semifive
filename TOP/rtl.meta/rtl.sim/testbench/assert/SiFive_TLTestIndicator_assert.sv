//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLTestIndicator_assert(
  input         _T_10_bits_read,
  input         Queue_io_enq_ready,
  input         buffer_auto_out_a_valid,
  input  [2:0]  buffer_auto_out_a_bits_opcode,
  input  [1:0]  buffer_auto_out_a_bits_size,
  input  [11:0] buffer_auto_out_a_bits_source,
  input  [14:0] buffer_auto_out_a_bits_address,
  input  [3:0]  buffer_auto_out_a_bits_mask,
  input         buffer_auto_out_d_ready,
  input         Queue_io_deq_valid,
  input  [1:0]  Queue_io_deq_bits_extra_tlrr_extra_size,
  input  [11:0] Queue_io_deq_bits_extra_tlrr_extra_source,
  input  [2:0]  buffer_auto_out_a_bits_param,
  input         buffer_auto_out_a_bits_corrupt
);
  wire  TLMonitor_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [11:0] TLMonitor_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [14:0] TLMonitor_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [11:0] TLMonitor_io_in_d_bits_source; // @[Nodes.scala 25:25]
  SiFive_TLMonitor_84_assert TLMonitor ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_clock),
    .io_in_a_ready(TLMonitor_io_in_a_ready),
    .io_in_a_valid(TLMonitor_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_io_in_a_bits_corrupt),
    .io_in_d_ready(TLMonitor_io_in_d_ready),
    .io_in_d_valid(TLMonitor_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_io_in_d_bits_opcode),
    .io_in_d_bits_size(TLMonitor_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_io_in_d_bits_source)
  );
  assign TLMonitor_clock = 1'h0;
  assign TLMonitor_io_in_a_ready = Queue_io_enq_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_valid = buffer_auto_out_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_opcode = buffer_auto_out_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_param = buffer_auto_out_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_size = buffer_auto_out_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_source = buffer_auto_out_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_address = buffer_auto_out_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_mask = buffer_auto_out_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_corrupt = buffer_auto_out_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_ready = buffer_auto_out_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_valid = Queue_io_deq_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_opcode = {{2'd0}, _T_10_bits_read}; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_size = Queue_io_deq_bits_extra_tlrr_extra_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_source = Queue_io_deq_bits_extra_tlrr_extra_source; // @[Nodes.scala 26:19]

endmodule
