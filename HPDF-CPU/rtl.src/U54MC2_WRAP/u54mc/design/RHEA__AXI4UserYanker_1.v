//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__AXI4UserYanker_1(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_aw_ready,
  input         auto_in_aw_valid,
  input  [1:0]  auto_in_aw_bits_id,
  input  [36:0] auto_in_aw_bits_addr,
  input  [7:0]  auto_in_aw_bits_len,
  input  [2:0]  auto_in_aw_bits_size,
  input  [3:0]  auto_in_aw_bits_cache,
  input  [2:0]  auto_in_aw_bits_prot,
  input  [13:0] auto_in_aw_bits_echo_extra_id,
  input         auto_in_aw_bits_echo_real_last,
  output        auto_in_w_ready,
  input         auto_in_w_valid,
  input  [63:0] auto_in_w_bits_data,
  input  [7:0]  auto_in_w_bits_strb,
  input         auto_in_w_bits_last,
  input         auto_in_b_ready,
  output        auto_in_b_valid,
  output [1:0]  auto_in_b_bits_id,
  output [1:0]  auto_in_b_bits_resp,
  output [13:0] auto_in_b_bits_echo_extra_id,
  output        auto_in_b_bits_echo_real_last,
  output        auto_in_ar_ready,
  input         auto_in_ar_valid,
  input  [1:0]  auto_in_ar_bits_id,
  input  [36:0] auto_in_ar_bits_addr,
  input  [7:0]  auto_in_ar_bits_len,
  input  [2:0]  auto_in_ar_bits_size,
  input  [3:0]  auto_in_ar_bits_cache,
  input  [2:0]  auto_in_ar_bits_prot,
  input  [13:0] auto_in_ar_bits_echo_extra_id,
  input         auto_in_ar_bits_echo_real_last,
  input         auto_in_r_ready,
  output        auto_in_r_valid,
  output [1:0]  auto_in_r_bits_id,
  output [63:0] auto_in_r_bits_data,
  output [1:0]  auto_in_r_bits_resp,
  output [13:0] auto_in_r_bits_echo_extra_id,
  output        auto_in_r_bits_echo_real_last,
  output        auto_in_r_bits_last,
  input         auto_out_aw_ready,
  output        auto_out_aw_valid,
  output [1:0]  auto_out_aw_bits_id,
  output [36:0] auto_out_aw_bits_addr,
  output [7:0]  auto_out_aw_bits_len,
  output [2:0]  auto_out_aw_bits_size,
  output [3:0]  auto_out_aw_bits_cache,
  output [2:0]  auto_out_aw_bits_prot,
  input         auto_out_w_ready,
  output        auto_out_w_valid,
  output [63:0] auto_out_w_bits_data,
  output [7:0]  auto_out_w_bits_strb,
  output        auto_out_w_bits_last,
  output        auto_out_b_ready,
  input         auto_out_b_valid,
  input  [1:0]  auto_out_b_bits_id,
  input  [1:0]  auto_out_b_bits_resp,
  input         auto_out_ar_ready,
  output        auto_out_ar_valid,
  output [1:0]  auto_out_ar_bits_id,
  output [36:0] auto_out_ar_bits_addr,
  output [7:0]  auto_out_ar_bits_len,
  output [2:0]  auto_out_ar_bits_size,
  output [3:0]  auto_out_ar_bits_cache,
  output [2:0]  auto_out_ar_bits_prot,
  output        auto_out_r_ready,
  input         auto_out_r_valid,
  input  [1:0]  auto_out_r_bits_id,
  input  [63:0] auto_out_r_bits_data,
  input  [1:0]  auto_out_r_bits_resp,
  input         auto_out_r_bits_last
);
  wire  QueueCompatibility_rf_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_clock; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_io_enq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_io_enq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_io_enq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_io_enq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_io_deq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_io_deq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_io_deq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_io_deq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_rf_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_clock; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_io_enq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_io_enq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_1_io_enq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_io_enq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_io_deq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_io_deq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_1_io_deq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_1_io_deq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_2_rf_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_2_clock; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_2_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_2_io_enq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_2_io_enq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_2_io_enq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_2_io_enq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_2_io_deq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_2_io_deq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_2_io_deq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_2_io_deq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_3_rf_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_3_clock; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_3_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_3_io_enq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_3_io_enq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_3_io_enq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_3_io_enq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_3_io_deq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_3_io_deq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_3_io_deq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_3_io_deq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_4_rf_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_4_clock; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_4_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_4_io_enq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_4_io_enq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_4_io_enq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_4_io_enq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_4_io_deq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_4_io_deq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_4_io_deq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_4_io_deq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_5_rf_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_5_clock; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_5_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_5_io_enq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_5_io_enq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_5_io_enq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_5_io_enq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_5_io_deq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_5_io_deq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_5_io_deq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_5_io_deq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_6_rf_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_6_clock; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_6_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_6_io_enq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_6_io_enq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_6_io_enq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_6_io_enq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_6_io_deq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_6_io_deq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_6_io_deq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_6_io_deq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_7_rf_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_7_clock; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_7_reset; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_7_io_enq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_7_io_enq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_7_io_enq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_7_io_enq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_7_io_deq_ready; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_7_io_deq_valid; // @[UserYanker.scala 55:17]
  wire [13:0] QueueCompatibility_7_io_deq_bits_extra_id; // @[UserYanker.scala 55:17]
  wire  QueueCompatibility_7_io_deq_bits_real_last; // @[UserYanker.scala 55:17]
  wire  _ar_ready_WIRE_0 = QueueCompatibility_io_enq_ready; // @[UserYanker.scala 63:25 UserYanker.scala 63:25]
  wire  _ar_ready_WIRE_1 = QueueCompatibility_1_io_enq_ready; // @[UserYanker.scala 63:25 UserYanker.scala 63:25]
  wire  _GEN_1 = 2'h1 == auto_in_ar_bits_id ? _ar_ready_WIRE_1 : _ar_ready_WIRE_0; // @[UserYanker.scala 64:36 UserYanker.scala 64:36]
  wire  _ar_ready_WIRE_2 = QueueCompatibility_2_io_enq_ready; // @[UserYanker.scala 63:25 UserYanker.scala 63:25]
  wire  _GEN_2 = 2'h2 == auto_in_ar_bits_id ? _ar_ready_WIRE_2 : _GEN_1; // @[UserYanker.scala 64:36 UserYanker.scala 64:36]
  wire  _ar_ready_WIRE_3 = QueueCompatibility_3_io_enq_ready; // @[UserYanker.scala 63:25 UserYanker.scala 63:25]
  wire  _GEN_3 = 2'h3 == auto_in_ar_bits_id ? _ar_ready_WIRE_3 : _GEN_2; // @[UserYanker.scala 64:36 UserYanker.scala 64:36]
  wire [13:0] _r_bits_WIRE_0_extra_id = QueueCompatibility_io_deq_bits_extra_id; // @[UserYanker.scala 70:23 UserYanker.scala 70:23]
  wire  _r_bits_WIRE_0_real_last = QueueCompatibility_io_deq_bits_real_last; // @[UserYanker.scala 70:23 UserYanker.scala 70:23]
  wire [13:0] _r_bits_WIRE_1_extra_id = QueueCompatibility_1_io_deq_bits_extra_id; // @[UserYanker.scala 70:23 UserYanker.scala 70:23]
  wire [13:0] _GEN_10 = 2'h1 == auto_out_r_bits_id ? _r_bits_WIRE_1_extra_id : _r_bits_WIRE_0_extra_id; // @[BundleMap.scala 247:19 BundleMap.scala 247:19]
  wire  _r_bits_WIRE_1_real_last = QueueCompatibility_1_io_deq_bits_real_last; // @[UserYanker.scala 70:23 UserYanker.scala 70:23]
  wire  _GEN_11 = 2'h1 == auto_out_r_bits_id ? _r_bits_WIRE_1_real_last : _r_bits_WIRE_0_real_last; // @[BundleMap.scala 247:19 BundleMap.scala 247:19]
  wire [13:0] _r_bits_WIRE_2_extra_id = QueueCompatibility_2_io_deq_bits_extra_id; // @[UserYanker.scala 70:23 UserYanker.scala 70:23]
  wire [13:0] _GEN_12 = 2'h2 == auto_out_r_bits_id ? _r_bits_WIRE_2_extra_id : _GEN_10; // @[BundleMap.scala 247:19 BundleMap.scala 247:19]
  wire  _r_bits_WIRE_2_real_last = QueueCompatibility_2_io_deq_bits_real_last; // @[UserYanker.scala 70:23 UserYanker.scala 70:23]
  wire  _GEN_13 = 2'h2 == auto_out_r_bits_id ? _r_bits_WIRE_2_real_last : _GEN_11; // @[BundleMap.scala 247:19 BundleMap.scala 247:19]
  wire [13:0] _r_bits_WIRE_3_extra_id = QueueCompatibility_3_io_deq_bits_extra_id; // @[UserYanker.scala 70:23 UserYanker.scala 70:23]
  wire  _r_bits_WIRE_3_real_last = QueueCompatibility_3_io_deq_bits_real_last; // @[UserYanker.scala 70:23 UserYanker.scala 70:23]
  wire [3:0] _arsel_T = 4'h1 << auto_in_ar_bits_id; // @[OneHot.scala 65:12]
  wire  arsel_0 = _arsel_T[0]; // @[UserYanker.scala 75:55]
  wire  arsel_1 = _arsel_T[1]; // @[UserYanker.scala 75:55]
  wire  arsel_2 = _arsel_T[2]; // @[UserYanker.scala 75:55]
  wire  arsel_3 = _arsel_T[3]; // @[UserYanker.scala 75:55]
  wire [3:0] _rsel_T = 4'h1 << auto_out_r_bits_id; // @[OneHot.scala 65:12]
  wire  rsel_0 = _rsel_T[0]; // @[UserYanker.scala 76:55]
  wire  rsel_1 = _rsel_T[1]; // @[UserYanker.scala 76:55]
  wire  rsel_2 = _rsel_T[2]; // @[UserYanker.scala 76:55]
  wire  rsel_3 = _rsel_T[3]; // @[UserYanker.scala 76:55]
  wire  _aw_ready_WIRE_0 = QueueCompatibility_4_io_enq_ready; // @[UserYanker.scala 84:25 UserYanker.scala 84:25]
  wire  _aw_ready_WIRE_1 = QueueCompatibility_5_io_enq_ready; // @[UserYanker.scala 84:25 UserYanker.scala 84:25]
  wire  _GEN_17 = 2'h1 == auto_in_aw_bits_id ? _aw_ready_WIRE_1 : _aw_ready_WIRE_0; // @[UserYanker.scala 85:36 UserYanker.scala 85:36]
  wire  _aw_ready_WIRE_2 = QueueCompatibility_6_io_enq_ready; // @[UserYanker.scala 84:25 UserYanker.scala 84:25]
  wire  _GEN_18 = 2'h2 == auto_in_aw_bits_id ? _aw_ready_WIRE_2 : _GEN_17; // @[UserYanker.scala 85:36 UserYanker.scala 85:36]
  wire  _aw_ready_WIRE_3 = QueueCompatibility_7_io_enq_ready; // @[UserYanker.scala 84:25 UserYanker.scala 84:25]
  wire  _GEN_19 = 2'h3 == auto_in_aw_bits_id ? _aw_ready_WIRE_3 : _GEN_18; // @[UserYanker.scala 85:36 UserYanker.scala 85:36]
  wire [13:0] _b_bits_WIRE_0_extra_id = QueueCompatibility_4_io_deq_bits_extra_id; // @[UserYanker.scala 91:23 UserYanker.scala 91:23]
  wire  _b_bits_WIRE_0_real_last = QueueCompatibility_4_io_deq_bits_real_last; // @[UserYanker.scala 91:23 UserYanker.scala 91:23]
  wire [13:0] _b_bits_WIRE_1_extra_id = QueueCompatibility_5_io_deq_bits_extra_id; // @[UserYanker.scala 91:23 UserYanker.scala 91:23]
  wire [13:0] _GEN_26 = 2'h1 == auto_out_b_bits_id ? _b_bits_WIRE_1_extra_id : _b_bits_WIRE_0_extra_id; // @[BundleMap.scala 247:19 BundleMap.scala 247:19]
  wire  _b_bits_WIRE_1_real_last = QueueCompatibility_5_io_deq_bits_real_last; // @[UserYanker.scala 91:23 UserYanker.scala 91:23]
  wire  _GEN_27 = 2'h1 == auto_out_b_bits_id ? _b_bits_WIRE_1_real_last : _b_bits_WIRE_0_real_last; // @[BundleMap.scala 247:19 BundleMap.scala 247:19]
  wire [13:0] _b_bits_WIRE_2_extra_id = QueueCompatibility_6_io_deq_bits_extra_id; // @[UserYanker.scala 91:23 UserYanker.scala 91:23]
  wire [13:0] _GEN_28 = 2'h2 == auto_out_b_bits_id ? _b_bits_WIRE_2_extra_id : _GEN_26; // @[BundleMap.scala 247:19 BundleMap.scala 247:19]
  wire  _b_bits_WIRE_2_real_last = QueueCompatibility_6_io_deq_bits_real_last; // @[UserYanker.scala 91:23 UserYanker.scala 91:23]
  wire  _GEN_29 = 2'h2 == auto_out_b_bits_id ? _b_bits_WIRE_2_real_last : _GEN_27; // @[BundleMap.scala 247:19 BundleMap.scala 247:19]
  wire [13:0] _b_bits_WIRE_3_extra_id = QueueCompatibility_7_io_deq_bits_extra_id; // @[UserYanker.scala 91:23 UserYanker.scala 91:23]
  wire  _b_bits_WIRE_3_real_last = QueueCompatibility_7_io_deq_bits_real_last; // @[UserYanker.scala 91:23 UserYanker.scala 91:23]
  wire [3:0] _awsel_T = 4'h1 << auto_in_aw_bits_id; // @[OneHot.scala 65:12]
  wire  awsel_0 = _awsel_T[0]; // @[UserYanker.scala 96:55]
  wire  awsel_1 = _awsel_T[1]; // @[UserYanker.scala 96:55]
  wire  awsel_2 = _awsel_T[2]; // @[UserYanker.scala 96:55]
  wire  awsel_3 = _awsel_T[3]; // @[UserYanker.scala 96:55]
  wire [3:0] _bsel_T = 4'h1 << auto_out_b_bits_id; // @[OneHot.scala 65:12]
  wire  bsel_0 = _bsel_T[0]; // @[UserYanker.scala 97:55]
  wire  bsel_1 = _bsel_T[1]; // @[UserYanker.scala 97:55]
  wire  bsel_2 = _bsel_T[2]; // @[UserYanker.scala 97:55]
  wire  bsel_3 = _bsel_T[3]; // @[UserYanker.scala 97:55]
  RHEA__QueueCompatibility_32 QueueCompatibility ( // @[UserYanker.scala 55:17]
    .rf_reset(QueueCompatibility_rf_reset),
    .clock(QueueCompatibility_clock),
    .reset(QueueCompatibility_reset),
    .io_enq_ready(QueueCompatibility_io_enq_ready),
    .io_enq_valid(QueueCompatibility_io_enq_valid),
    .io_enq_bits_extra_id(QueueCompatibility_io_enq_bits_extra_id),
    .io_enq_bits_real_last(QueueCompatibility_io_enq_bits_real_last),
    .io_deq_ready(QueueCompatibility_io_deq_ready),
    .io_deq_valid(QueueCompatibility_io_deq_valid),
    .io_deq_bits_extra_id(QueueCompatibility_io_deq_bits_extra_id),
    .io_deq_bits_real_last(QueueCompatibility_io_deq_bits_real_last)
  );
  RHEA__QueueCompatibility_32 QueueCompatibility_1 ( // @[UserYanker.scala 55:17]
    .rf_reset(QueueCompatibility_1_rf_reset),
    .clock(QueueCompatibility_1_clock),
    .reset(QueueCompatibility_1_reset),
    .io_enq_ready(QueueCompatibility_1_io_enq_ready),
    .io_enq_valid(QueueCompatibility_1_io_enq_valid),
    .io_enq_bits_extra_id(QueueCompatibility_1_io_enq_bits_extra_id),
    .io_enq_bits_real_last(QueueCompatibility_1_io_enq_bits_real_last),
    .io_deq_ready(QueueCompatibility_1_io_deq_ready),
    .io_deq_valid(QueueCompatibility_1_io_deq_valid),
    .io_deq_bits_extra_id(QueueCompatibility_1_io_deq_bits_extra_id),
    .io_deq_bits_real_last(QueueCompatibility_1_io_deq_bits_real_last)
  );
  RHEA__QueueCompatibility_32 QueueCompatibility_2 ( // @[UserYanker.scala 55:17]
    .rf_reset(QueueCompatibility_2_rf_reset),
    .clock(QueueCompatibility_2_clock),
    .reset(QueueCompatibility_2_reset),
    .io_enq_ready(QueueCompatibility_2_io_enq_ready),
    .io_enq_valid(QueueCompatibility_2_io_enq_valid),
    .io_enq_bits_extra_id(QueueCompatibility_2_io_enq_bits_extra_id),
    .io_enq_bits_real_last(QueueCompatibility_2_io_enq_bits_real_last),
    .io_deq_ready(QueueCompatibility_2_io_deq_ready),
    .io_deq_valid(QueueCompatibility_2_io_deq_valid),
    .io_deq_bits_extra_id(QueueCompatibility_2_io_deq_bits_extra_id),
    .io_deq_bits_real_last(QueueCompatibility_2_io_deq_bits_real_last)
  );
  RHEA__QueueCompatibility_32 QueueCompatibility_3 ( // @[UserYanker.scala 55:17]
    .rf_reset(QueueCompatibility_3_rf_reset),
    .clock(QueueCompatibility_3_clock),
    .reset(QueueCompatibility_3_reset),
    .io_enq_ready(QueueCompatibility_3_io_enq_ready),
    .io_enq_valid(QueueCompatibility_3_io_enq_valid),
    .io_enq_bits_extra_id(QueueCompatibility_3_io_enq_bits_extra_id),
    .io_enq_bits_real_last(QueueCompatibility_3_io_enq_bits_real_last),
    .io_deq_ready(QueueCompatibility_3_io_deq_ready),
    .io_deq_valid(QueueCompatibility_3_io_deq_valid),
    .io_deq_bits_extra_id(QueueCompatibility_3_io_deq_bits_extra_id),
    .io_deq_bits_real_last(QueueCompatibility_3_io_deq_bits_real_last)
  );
  RHEA__QueueCompatibility_32 QueueCompatibility_4 ( // @[UserYanker.scala 55:17]
    .rf_reset(QueueCompatibility_4_rf_reset),
    .clock(QueueCompatibility_4_clock),
    .reset(QueueCompatibility_4_reset),
    .io_enq_ready(QueueCompatibility_4_io_enq_ready),
    .io_enq_valid(QueueCompatibility_4_io_enq_valid),
    .io_enq_bits_extra_id(QueueCompatibility_4_io_enq_bits_extra_id),
    .io_enq_bits_real_last(QueueCompatibility_4_io_enq_bits_real_last),
    .io_deq_ready(QueueCompatibility_4_io_deq_ready),
    .io_deq_valid(QueueCompatibility_4_io_deq_valid),
    .io_deq_bits_extra_id(QueueCompatibility_4_io_deq_bits_extra_id),
    .io_deq_bits_real_last(QueueCompatibility_4_io_deq_bits_real_last)
  );
  RHEA__QueueCompatibility_32 QueueCompatibility_5 ( // @[UserYanker.scala 55:17]
    .rf_reset(QueueCompatibility_5_rf_reset),
    .clock(QueueCompatibility_5_clock),
    .reset(QueueCompatibility_5_reset),
    .io_enq_ready(QueueCompatibility_5_io_enq_ready),
    .io_enq_valid(QueueCompatibility_5_io_enq_valid),
    .io_enq_bits_extra_id(QueueCompatibility_5_io_enq_bits_extra_id),
    .io_enq_bits_real_last(QueueCompatibility_5_io_enq_bits_real_last),
    .io_deq_ready(QueueCompatibility_5_io_deq_ready),
    .io_deq_valid(QueueCompatibility_5_io_deq_valid),
    .io_deq_bits_extra_id(QueueCompatibility_5_io_deq_bits_extra_id),
    .io_deq_bits_real_last(QueueCompatibility_5_io_deq_bits_real_last)
  );
  RHEA__QueueCompatibility_32 QueueCompatibility_6 ( // @[UserYanker.scala 55:17]
    .rf_reset(QueueCompatibility_6_rf_reset),
    .clock(QueueCompatibility_6_clock),
    .reset(QueueCompatibility_6_reset),
    .io_enq_ready(QueueCompatibility_6_io_enq_ready),
    .io_enq_valid(QueueCompatibility_6_io_enq_valid),
    .io_enq_bits_extra_id(QueueCompatibility_6_io_enq_bits_extra_id),
    .io_enq_bits_real_last(QueueCompatibility_6_io_enq_bits_real_last),
    .io_deq_ready(QueueCompatibility_6_io_deq_ready),
    .io_deq_valid(QueueCompatibility_6_io_deq_valid),
    .io_deq_bits_extra_id(QueueCompatibility_6_io_deq_bits_extra_id),
    .io_deq_bits_real_last(QueueCompatibility_6_io_deq_bits_real_last)
  );
  RHEA__QueueCompatibility_32 QueueCompatibility_7 ( // @[UserYanker.scala 55:17]
    .rf_reset(QueueCompatibility_7_rf_reset),
    .clock(QueueCompatibility_7_clock),
    .reset(QueueCompatibility_7_reset),
    .io_enq_ready(QueueCompatibility_7_io_enq_ready),
    .io_enq_valid(QueueCompatibility_7_io_enq_valid),
    .io_enq_bits_extra_id(QueueCompatibility_7_io_enq_bits_extra_id),
    .io_enq_bits_real_last(QueueCompatibility_7_io_enq_bits_real_last),
    .io_deq_ready(QueueCompatibility_7_io_deq_ready),
    .io_deq_valid(QueueCompatibility_7_io_deq_valid),
    .io_deq_bits_extra_id(QueueCompatibility_7_io_deq_bits_extra_id),
    .io_deq_bits_real_last(QueueCompatibility_7_io_deq_bits_real_last)
  );
  assign QueueCompatibility_rf_reset = rf_reset;
  assign QueueCompatibility_1_rf_reset = rf_reset;
  assign QueueCompatibility_2_rf_reset = rf_reset;
  assign QueueCompatibility_3_rf_reset = rf_reset;
  assign QueueCompatibility_4_rf_reset = rf_reset;
  assign QueueCompatibility_5_rf_reset = rf_reset;
  assign QueueCompatibility_6_rf_reset = rf_reset;
  assign QueueCompatibility_7_rf_reset = rf_reset;
  assign auto_in_aw_ready = auto_out_aw_ready & _GEN_19; // @[UserYanker.scala 85:36]
  assign auto_in_w_ready = auto_out_w_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_b_valid = auto_out_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_b_bits_id = auto_out_b_bits_id; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_b_bits_resp = auto_out_b_bits_resp; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_b_bits_echo_extra_id = 2'h3 == auto_out_b_bits_id ? _b_bits_WIRE_3_extra_id : _GEN_28; // @[BundleMap.scala 247:19 BundleMap.scala 247:19]
  assign auto_in_b_bits_echo_real_last = 2'h3 == auto_out_b_bits_id ? _b_bits_WIRE_3_real_last : _GEN_29; // @[BundleMap.scala 247:19 BundleMap.scala 247:19]
  assign auto_in_ar_ready = auto_out_ar_ready & _GEN_3; // @[UserYanker.scala 64:36]
  assign auto_in_r_valid = auto_out_r_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_bits_id = auto_out_r_bits_id; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_bits_data = auto_out_r_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_bits_resp = auto_out_r_bits_resp; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_r_bits_echo_extra_id = 2'h3 == auto_out_r_bits_id ? _r_bits_WIRE_3_extra_id : _GEN_12; // @[BundleMap.scala 247:19 BundleMap.scala 247:19]
  assign auto_in_r_bits_echo_real_last = 2'h3 == auto_out_r_bits_id ? _r_bits_WIRE_3_real_last : _GEN_13; // @[BundleMap.scala 247:19 BundleMap.scala 247:19]
  assign auto_in_r_bits_last = auto_out_r_bits_last; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_out_aw_valid = auto_in_aw_valid & _GEN_19; // @[UserYanker.scala 86:36]
  assign auto_out_aw_bits_id = auto_in_aw_bits_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_addr = auto_in_aw_bits_addr; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_len = auto_in_aw_bits_len; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_size = auto_in_aw_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_cache = auto_in_aw_bits_cache; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_aw_bits_prot = auto_in_aw_bits_prot; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_w_valid = auto_in_w_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_w_bits_data = auto_in_w_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_w_bits_strb = auto_in_w_bits_strb; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_w_bits_last = auto_in_w_bits_last; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_b_ready = auto_in_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_valid = auto_in_ar_valid & _GEN_3; // @[UserYanker.scala 65:36]
  assign auto_out_ar_bits_id = auto_in_ar_bits_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_addr = auto_in_ar_bits_addr; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_len = auto_in_ar_bits_len; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_size = auto_in_ar_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_cache = auto_in_ar_bits_cache; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_ar_bits_prot = auto_in_ar_bits_prot; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_r_ready = auto_in_r_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_clock = clock;
  assign QueueCompatibility_reset = reset;
  assign QueueCompatibility_io_enq_valid = auto_in_ar_valid & auto_out_ar_ready & arsel_0; // @[UserYanker.scala 79:53]
  assign QueueCompatibility_io_enq_bits_extra_id = auto_in_ar_bits_echo_extra_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_io_enq_bits_real_last = auto_in_ar_bits_echo_real_last; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_io_deq_ready = auto_out_r_valid & auto_in_r_ready & rsel_0 & auto_out_r_bits_last; // @[UserYanker.scala 78:58]
  assign QueueCompatibility_1_clock = clock;
  assign QueueCompatibility_1_reset = reset;
  assign QueueCompatibility_1_io_enq_valid = auto_in_ar_valid & auto_out_ar_ready & arsel_1; // @[UserYanker.scala 79:53]
  assign QueueCompatibility_1_io_enq_bits_extra_id = auto_in_ar_bits_echo_extra_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_1_io_enq_bits_real_last = auto_in_ar_bits_echo_real_last; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_1_io_deq_ready = auto_out_r_valid & auto_in_r_ready & rsel_1 & auto_out_r_bits_last; // @[UserYanker.scala 78:58]
  assign QueueCompatibility_2_clock = clock;
  assign QueueCompatibility_2_reset = reset;
  assign QueueCompatibility_2_io_enq_valid = auto_in_ar_valid & auto_out_ar_ready & arsel_2; // @[UserYanker.scala 79:53]
  assign QueueCompatibility_2_io_enq_bits_extra_id = auto_in_ar_bits_echo_extra_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_2_io_enq_bits_real_last = auto_in_ar_bits_echo_real_last; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_2_io_deq_ready = auto_out_r_valid & auto_in_r_ready & rsel_2 & auto_out_r_bits_last; // @[UserYanker.scala 78:58]
  assign QueueCompatibility_3_clock = clock;
  assign QueueCompatibility_3_reset = reset;
  assign QueueCompatibility_3_io_enq_valid = auto_in_ar_valid & auto_out_ar_ready & arsel_3; // @[UserYanker.scala 79:53]
  assign QueueCompatibility_3_io_enq_bits_extra_id = auto_in_ar_bits_echo_extra_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_3_io_enq_bits_real_last = auto_in_ar_bits_echo_real_last; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_3_io_deq_ready = auto_out_r_valid & auto_in_r_ready & rsel_3 & auto_out_r_bits_last; // @[UserYanker.scala 78:58]
  assign QueueCompatibility_4_clock = clock;
  assign QueueCompatibility_4_reset = reset;
  assign QueueCompatibility_4_io_enq_valid = auto_in_aw_valid & auto_out_aw_ready & awsel_0; // @[UserYanker.scala 100:53]
  assign QueueCompatibility_4_io_enq_bits_extra_id = auto_in_aw_bits_echo_extra_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_4_io_enq_bits_real_last = auto_in_aw_bits_echo_real_last; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_4_io_deq_ready = auto_out_b_valid & auto_in_b_ready & bsel_0; // @[UserYanker.scala 99:53]
  assign QueueCompatibility_5_clock = clock;
  assign QueueCompatibility_5_reset = reset;
  assign QueueCompatibility_5_io_enq_valid = auto_in_aw_valid & auto_out_aw_ready & awsel_1; // @[UserYanker.scala 100:53]
  assign QueueCompatibility_5_io_enq_bits_extra_id = auto_in_aw_bits_echo_extra_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_5_io_enq_bits_real_last = auto_in_aw_bits_echo_real_last; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_5_io_deq_ready = auto_out_b_valid & auto_in_b_ready & bsel_1; // @[UserYanker.scala 99:53]
  assign QueueCompatibility_6_clock = clock;
  assign QueueCompatibility_6_reset = reset;
  assign QueueCompatibility_6_io_enq_valid = auto_in_aw_valid & auto_out_aw_ready & awsel_2; // @[UserYanker.scala 100:53]
  assign QueueCompatibility_6_io_enq_bits_extra_id = auto_in_aw_bits_echo_extra_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_6_io_enq_bits_real_last = auto_in_aw_bits_echo_real_last; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_6_io_deq_ready = auto_out_b_valid & auto_in_b_ready & bsel_2; // @[UserYanker.scala 99:53]
  assign QueueCompatibility_7_clock = clock;
  assign QueueCompatibility_7_reset = reset;
  assign QueueCompatibility_7_io_enq_valid = auto_in_aw_valid & auto_out_aw_ready & awsel_3; // @[UserYanker.scala 100:53]
  assign QueueCompatibility_7_io_enq_bits_extra_id = auto_in_aw_bits_echo_extra_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_7_io_enq_bits_real_last = auto_in_aw_bits_echo_real_last; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign QueueCompatibility_7_io_deq_ready = auto_out_b_valid & auto_in_b_ready & bsel_3; // @[UserYanker.scala 99:53]
endmodule
