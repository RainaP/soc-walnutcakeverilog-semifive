//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__SinkA(
  input          rf_reset,
  input          clock,
  input          reset,
  input          io_req_ready,
  output         io_req_valid,
  output [2:0]   io_req_bits_opcode,
  output [2:0]   io_req_bits_param,
  output [2:0]   io_req_bits_size,
  output [6:0]   io_req_bits_source,
  output [17:0]  io_req_bits_tag,
  output [5:0]   io_req_bits_offset,
  output [4:0]   io_req_bits_put,
  output [9:0]   io_req_bits_set,
  output         io_a_ready,
  input          io_a_valid,
  input  [2:0]   io_a_bits_opcode,
  input  [2:0]   io_a_bits_param,
  input  [2:0]   io_a_bits_size,
  input  [6:0]   io_a_bits_source,
  input  [35:0]  io_a_bits_address,
  input  [15:0]  io_a_bits_mask,
  input  [127:0] io_a_bits_data,
  input          io_a_bits_corrupt,
  output         io_pb_pop_ready,
  input          io_pb_pop_valid,
  input  [4:0]   io_pb_pop_bits_index,
  input          io_pb_pop_bits_last,
  output [127:0] io_pb_beat_data,
  output [15:0]  io_pb_beat_mask,
  output         io_pb_beat_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
`endif // RANDOMIZE_REG_INIT
  wire  putbuffer_rf_reset; // @[SinkA.scala 54:25]
  wire  putbuffer_clock; // @[SinkA.scala 54:25]
  wire  putbuffer_reset; // @[SinkA.scala 54:25]
  wire  putbuffer_io_push_ready; // @[SinkA.scala 54:25]
  wire  putbuffer_io_push_valid; // @[SinkA.scala 54:25]
  wire [4:0] putbuffer_io_push_bits_index; // @[SinkA.scala 54:25]
  wire [127:0] putbuffer_io_push_bits_data_data; // @[SinkA.scala 54:25]
  wire [15:0] putbuffer_io_push_bits_data_mask; // @[SinkA.scala 54:25]
  wire  putbuffer_io_push_bits_data_corrupt; // @[SinkA.scala 54:25]
  wire [23:0] putbuffer_io_valid; // @[SinkA.scala 54:25]
  wire  putbuffer_io_pop_valid; // @[SinkA.scala 54:25]
  wire [4:0] putbuffer_io_pop_bits; // @[SinkA.scala 54:25]
  wire [127:0] putbuffer_io_data_data; // @[SinkA.scala 54:25]
  wire [15:0] putbuffer_io_data_mask; // @[SinkA.scala 54:25]
  wire  putbuffer_io_data_corrupt; // @[SinkA.scala 54:25]
  reg [23:0] lists; // @[SinkA.scala 55:22]
  reg [1:0] first_counter; // @[Edges.scala 228:27]
  wire  first = first_counter == 2'h0; // @[Edges.scala 230:25]
  wire  _T_9 = io_a_valid & first; // @[SinkA.scala 84:17]
  wire  hasData = ~io_a_bits_opcode[2]; // @[Edges.scala 91:28]
  wire  req_block = first & ~io_req_ready; // @[SinkA.scala 73:25]
  wire  _T_11 = ~req_block; // @[SinkA.scala 84:40]
  wire  buf_block = hasData & ~putbuffer_io_push_ready; // @[SinkA.scala 74:27]
  wire  _T_13 = ~buf_block; // @[SinkA.scala 84:54]
  wire [23:0] _freeOH_T = ~lists; // @[SinkA.scala 62:25]
  wire [24:0] _freeOH_T_1 = {_freeOH_T, 1'h0}; // @[package.scala 244:48]
  wire [23:0] _freeOH_T_3 = _freeOH_T | _freeOH_T_1[23:0]; // @[package.scala 244:43]
  wire [25:0] _freeOH_T_4 = {_freeOH_T_3, 2'h0}; // @[package.scala 244:48]
  wire [23:0] _freeOH_T_6 = _freeOH_T_3 | _freeOH_T_4[23:0]; // @[package.scala 244:43]
  wire [27:0] _freeOH_T_7 = {_freeOH_T_6, 4'h0}; // @[package.scala 244:48]
  wire [23:0] _freeOH_T_9 = _freeOH_T_6 | _freeOH_T_7[23:0]; // @[package.scala 244:43]
  wire [31:0] _freeOH_T_10 = {_freeOH_T_9, 8'h0}; // @[package.scala 244:48]
  wire [23:0] _freeOH_T_12 = _freeOH_T_9 | _freeOH_T_10[23:0]; // @[package.scala 244:43]
  wire [39:0] _freeOH_T_13 = {_freeOH_T_12, 16'h0}; // @[package.scala 244:48]
  wire [23:0] _freeOH_T_15 = _freeOH_T_12 | _freeOH_T_13[23:0]; // @[package.scala 244:43]
  wire [24:0] _freeOH_T_17 = {_freeOH_T_15, 1'h0}; // @[SinkA.scala 62:33]
  wire [24:0] _freeOH_T_18 = ~_freeOH_T_17; // @[SinkA.scala 62:16]
  wire [24:0] _GEN_4 = {{1'd0}, _freeOH_T}; // @[SinkA.scala 62:39]
  wire [24:0] freeOH = _freeOH_T_18 & _GEN_4; // @[SinkA.scala 62:39]
  wire [24:0] _GEN_1 = io_a_valid & first & hasData & ~req_block & ~buf_block ? freeOH : 25'h0; // @[SinkA.scala 84:66 SinkA.scala 84:78]
  wire [23:0] lists_set = _GEN_1[23:0];
  wire [23:0] _lists_T = lists | lists_set; // @[SinkA.scala 59:19]
  wire  _T_15 = io_pb_pop_ready & io_pb_pop_valid; // @[Decoupled.scala 40:37]
  wire [31:0] _lists_clr_T = 32'h1 << io_pb_pop_bits_index; // @[OneHot.scala 65:12]
  wire [23:0] lists_clr = _T_15 & io_pb_pop_bits_last ? _lists_clr_T[23:0] : 24'h0; // @[SinkA.scala 112:50 SinkA.scala 113:15]
  wire [23:0] _lists_T_1 = ~lists_clr; // @[SinkA.scala 59:34]
  wire  free = ~(&lists); // @[SinkA.scala 61:14]
  wire [8:0] freeIdx_hi = freeOH[24:16]; // @[OneHot.scala 30:18]
  wire [15:0] freeIdx_lo = freeOH[15:0]; // @[OneHot.scala 31:18]
  wire  freeIdx_hi_1 = |freeIdx_hi; // @[OneHot.scala 32:14]
  wire [15:0] _GEN_5 = {{7'd0}, freeIdx_hi}; // @[OneHot.scala 32:28]
  wire [15:0] _freeIdx_T = _GEN_5 | freeIdx_lo; // @[OneHot.scala 32:28]
  wire [7:0] freeIdx_hi_2 = _freeIdx_T[15:8]; // @[OneHot.scala 30:18]
  wire [7:0] freeIdx_lo_1 = _freeIdx_T[7:0]; // @[OneHot.scala 31:18]
  wire  freeIdx_hi_3 = |freeIdx_hi_2; // @[OneHot.scala 32:14]
  wire [7:0] _freeIdx_T_1 = freeIdx_hi_2 | freeIdx_lo_1; // @[OneHot.scala 32:28]
  wire [3:0] freeIdx_hi_4 = _freeIdx_T_1[7:4]; // @[OneHot.scala 30:18]
  wire [3:0] freeIdx_lo_2 = _freeIdx_T_1[3:0]; // @[OneHot.scala 31:18]
  wire  freeIdx_hi_5 = |freeIdx_hi_4; // @[OneHot.scala 32:14]
  wire [3:0] _freeIdx_T_2 = freeIdx_hi_4 | freeIdx_lo_2; // @[OneHot.scala 32:28]
  wire [1:0] freeIdx_hi_6 = _freeIdx_T_2[3:2]; // @[OneHot.scala 30:18]
  wire [1:0] freeIdx_lo_3 = _freeIdx_T_2[1:0]; // @[OneHot.scala 31:18]
  wire  freeIdx_hi_7 = |freeIdx_hi_6; // @[OneHot.scala 32:14]
  wire [1:0] _freeIdx_T_3 = freeIdx_hi_6 | freeIdx_lo_3; // @[OneHot.scala 32:28]
  wire  freeIdx_lo_4 = _freeIdx_T_3[1]; // @[CircuitMath.scala 30:8]
  wire [4:0] freeIdx = {freeIdx_hi_1,freeIdx_hi_3,freeIdx_hi_5,freeIdx_hi_7,freeIdx_lo_4}; // @[Cat.scala 30:58]
  wire  _first_T = io_a_ready & io_a_valid; // @[Decoupled.scala 40:37]
  wire [12:0] _first_beats1_decode_T_1 = 13'h3f << io_a_bits_size; // @[package.scala 234:77]
  wire [5:0] _first_beats1_decode_T_3 = ~_first_beats1_decode_T_1[5:0]; // @[package.scala 234:46]
  wire [1:0] first_beats1_decode = _first_beats1_decode_T_3[5:4]; // @[Edges.scala 219:59]
  wire [1:0] first_counter1 = first_counter - 2'h1; // @[Edges.scala 229:28]
  wire  set_block = hasData & first & ~free; // @[SinkA.scala 75:36]
  wire  _io_a_ready_T_3 = ~set_block; // @[SinkA.scala 81:42]
  wire  offset_lo_lo_lo_lo_lo = io_a_bits_address[0]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_lo_lo_hi = io_a_bits_address[1]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_lo_hi_lo = io_a_bits_address[2]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_lo_hi_hi = io_a_bits_address[3]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_hi_lo_lo = io_a_bits_address[4]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_hi_lo_hi = io_a_bits_address[5]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_hi_hi_lo = io_a_bits_address[8]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_hi_hi_hi = io_a_bits_address[9]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_lo_lo_lo = io_a_bits_address[10]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_lo_lo_hi = io_a_bits_address[11]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_lo_hi_lo = io_a_bits_address[12]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_lo_hi_hi = io_a_bits_address[13]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_hi_lo_lo = io_a_bits_address[14]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_hi_lo_hi = io_a_bits_address[15]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_hi_hi_lo = io_a_bits_address[16]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_hi_hi_hi_lo = io_a_bits_address[17]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_hi_hi_hi_hi = io_a_bits_address[18]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_lo_lo_lo = io_a_bits_address[19]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_lo_lo_hi = io_a_bits_address[20]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_lo_hi_lo = io_a_bits_address[21]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_lo_hi_hi = io_a_bits_address[22]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_hi_lo_lo = io_a_bits_address[23]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_hi_lo_hi = io_a_bits_address[24]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_hi_hi_lo = io_a_bits_address[25]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_hi_hi_hi = io_a_bits_address[26]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_lo_lo_lo = io_a_bits_address[27]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_lo_lo_hi = io_a_bits_address[28]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_lo_hi_lo = io_a_bits_address[29]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_lo_hi_hi = io_a_bits_address[30]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_hi_lo_lo = io_a_bits_address[31]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_hi_lo_hi = io_a_bits_address[32]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_hi_hi_lo = io_a_bits_address[33]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_hi_hi_hi_lo = io_a_bits_address[34]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_hi_hi_hi_hi = io_a_bits_address[35]; // @[Parameters.scala 227:47]
  wire [7:0] offset_lo_lo = {offset_lo_lo_hi_hi_hi,offset_lo_lo_hi_hi_lo,offset_lo_lo_hi_lo_hi,offset_lo_lo_hi_lo_lo,
    offset_lo_lo_lo_hi_hi,offset_lo_lo_lo_hi_lo,offset_lo_lo_lo_lo_hi,offset_lo_lo_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [16:0] offset_lo = {offset_lo_hi_hi_hi_hi_hi,offset_lo_hi_hi_hi_hi_lo,offset_lo_hi_hi_hi_lo,offset_lo_hi_hi_lo_hi
    ,offset_lo_hi_hi_lo_lo,offset_lo_hi_lo_hi_hi,offset_lo_hi_lo_hi_lo,offset_lo_hi_lo_lo_hi,offset_lo_hi_lo_lo_lo,
    offset_lo_lo}; // @[Cat.scala 30:58]
  wire [7:0] offset_hi_lo = {offset_hi_lo_hi_hi_hi,offset_hi_lo_hi_hi_lo,offset_hi_lo_hi_lo_hi,offset_hi_lo_hi_lo_lo,
    offset_hi_lo_lo_hi_hi,offset_hi_lo_lo_hi_lo,offset_hi_lo_lo_lo_hi,offset_hi_lo_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [16:0] offset_hi = {offset_hi_hi_hi_hi_hi_hi,offset_hi_hi_hi_hi_hi_lo,offset_hi_hi_hi_hi_lo,offset_hi_hi_hi_lo_hi
    ,offset_hi_hi_hi_lo_lo,offset_hi_hi_lo_hi_hi,offset_hi_hi_lo_hi_lo,offset_hi_hi_lo_lo_hi,offset_hi_hi_lo_lo_lo,
    offset_hi_lo}; // @[Cat.scala 30:58]
  wire [33:0] offset = {offset_hi,offset_lo}; // @[Cat.scala 30:58]
  wire [27:0] set = offset[33:6]; // @[Parameters.scala 228:22]
  reg [4:0] put_r; // @[Reg.scala 15:16]
  wire [23:0] _io_pb_pop_ready_T = putbuffer_io_valid >> io_pb_pop_bits_index; // @[SinkA.scala 109:40]
  RHEA__ListBuffer putbuffer ( // @[SinkA.scala 54:25]
    .rf_reset(putbuffer_rf_reset),
    .clock(putbuffer_clock),
    .reset(putbuffer_reset),
    .io_push_ready(putbuffer_io_push_ready),
    .io_push_valid(putbuffer_io_push_valid),
    .io_push_bits_index(putbuffer_io_push_bits_index),
    .io_push_bits_data_data(putbuffer_io_push_bits_data_data),
    .io_push_bits_data_mask(putbuffer_io_push_bits_data_mask),
    .io_push_bits_data_corrupt(putbuffer_io_push_bits_data_corrupt),
    .io_valid(putbuffer_io_valid),
    .io_pop_valid(putbuffer_io_pop_valid),
    .io_pop_bits(putbuffer_io_pop_bits),
    .io_data_data(putbuffer_io_data_data),
    .io_data_mask(putbuffer_io_data_mask),
    .io_data_corrupt(putbuffer_io_data_corrupt)
  );
  assign putbuffer_rf_reset = rf_reset;
  assign io_req_valid = _T_9 & _T_13 & _io_a_ready_T_3; // @[SinkA.scala 82:50]
  assign io_req_bits_opcode = io_a_bits_opcode; // @[SinkA.scala 91:22]
  assign io_req_bits_param = io_a_bits_param; // @[SinkA.scala 92:22]
  assign io_req_bits_size = io_a_bits_size; // @[SinkA.scala 93:22]
  assign io_req_bits_source = io_a_bits_source; // @[SinkA.scala 94:22]
  assign io_req_bits_tag = set[27:10]; // @[Parameters.scala 229:19]
  assign io_req_bits_offset = offset[5:0]; // @[Parameters.scala 230:50]
  assign io_req_bits_put = first ? freeIdx : put_r; // @[SinkA.scala 87:16]
  assign io_req_bits_set = set[9:0]; // @[Parameters.scala 230:28]
  assign io_a_ready = _T_11 & _T_13 & ~set_block; // @[SinkA.scala 81:39]
  assign io_pb_pop_ready = _io_pb_pop_ready_T[0]; // @[SinkA.scala 109:40]
  assign io_pb_beat_data = putbuffer_io_data_data; // @[SinkA.scala 110:14]
  assign io_pb_beat_mask = putbuffer_io_data_mask; // @[SinkA.scala 110:14]
  assign io_pb_beat_corrupt = putbuffer_io_data_corrupt; // @[SinkA.scala 110:14]
  assign putbuffer_clock = clock;
  assign putbuffer_reset = reset;
  assign putbuffer_io_push_valid = io_a_valid & hasData & _T_11 & _io_a_ready_T_3; // @[SinkA.scala 83:63]
  assign putbuffer_io_push_bits_index = first ? freeIdx : put_r; // @[SinkA.scala 87:16]
  assign putbuffer_io_push_bits_data_data = io_a_bits_data; // @[SinkA.scala 102:39]
  assign putbuffer_io_push_bits_data_mask = io_a_bits_mask; // @[SinkA.scala 103:39]
  assign putbuffer_io_push_bits_data_corrupt = io_a_bits_corrupt; // @[SinkA.scala 104:39]
  assign putbuffer_io_pop_valid = io_pb_pop_ready & io_pb_pop_valid; // @[Decoupled.scala 40:37]
  assign putbuffer_io_pop_bits = io_pb_pop_bits_index; // @[SinkA.scala 107:25]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      lists <= 24'h0;
    end else begin
      lists <= _lists_T & _lists_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      first_counter <= 2'h0;
    end else if (_first_T) begin
      if (first) begin
        if (hasData) begin
          first_counter <= first_beats1_decode;
        end else begin
          first_counter <= 2'h0;
        end
      end else begin
        first_counter <= first_counter1;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      put_r <= 5'h0;
    end else if (first) begin
      put_r <= freeIdx;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  lists = _RAND_0[23:0];
  _RAND_1 = {1{`RANDOM}};
  first_counter = _RAND_1[1:0];
  _RAND_2 = {1{`RANDOM}};
  put_r = _RAND_2[4:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    lists = 24'h0;
  end
  if (reset) begin
    first_counter = 2'h0;
  end
  if (rf_reset) begin
    put_r = 5'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
