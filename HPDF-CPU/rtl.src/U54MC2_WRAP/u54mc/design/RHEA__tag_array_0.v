//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__tag_array_0(
  input  [6:0]  RW0_addr,
  input         RW0_en,
  input         RW0_clk,
  input         RW0_wmode,
  input  [25:0] RW0_wdata_0,
  input  [25:0] RW0_wdata_1,
  input  [25:0] RW0_wdata_2,
  input  [25:0] RW0_wdata_3,
  output [25:0] RW0_rdata_0,
  output [25:0] RW0_rdata_1,
  output [25:0] RW0_rdata_2,
  output [25:0] RW0_rdata_3,
  input         RW0_wmask_0,
  input         RW0_wmask_1,
  input         RW0_wmask_2,
  input         RW0_wmask_3
);
  wire [6:0] tag_array_0_ext_RW0_addr;
  wire  tag_array_0_ext_RW0_en;
  wire  tag_array_0_ext_RW0_clk;
  wire  tag_array_0_ext_RW0_wmode;
  wire [103:0] tag_array_0_ext_RW0_wdata;
  wire [103:0] tag_array_0_ext_RW0_rdata;
  wire [3:0] tag_array_0_ext_RW0_wmask;
  wire [51:0] _GEN_0 = {RW0_wdata_3,RW0_wdata_2};
  wire [51:0] _GEN_1 = {RW0_wdata_1,RW0_wdata_0};
  wire [1:0] _GEN_2 = {RW0_wmask_3,RW0_wmask_2};
  wire [1:0] _GEN_3 = {RW0_wmask_1,RW0_wmask_0};
  RHEA__tag_array_0_ext tag_array_0_ext (
    .RW0_addr(tag_array_0_ext_RW0_addr),
    .RW0_en(tag_array_0_ext_RW0_en),
    .RW0_clk(tag_array_0_ext_RW0_clk),
    .RW0_wmode(tag_array_0_ext_RW0_wmode),
    .RW0_wdata(tag_array_0_ext_RW0_wdata),
    .RW0_rdata(tag_array_0_ext_RW0_rdata),
    .RW0_wmask(tag_array_0_ext_RW0_wmask)
  );
  assign tag_array_0_ext_RW0_clk = RW0_clk;
  assign tag_array_0_ext_RW0_en = RW0_en;
  assign tag_array_0_ext_RW0_addr = RW0_addr;
  assign RW0_rdata_0 = tag_array_0_ext_RW0_rdata[25:0];
  assign RW0_rdata_1 = tag_array_0_ext_RW0_rdata[51:26];
  assign RW0_rdata_2 = tag_array_0_ext_RW0_rdata[77:52];
  assign RW0_rdata_3 = tag_array_0_ext_RW0_rdata[103:78];
  assign tag_array_0_ext_RW0_wmode = RW0_wmode;
  assign tag_array_0_ext_RW0_wdata = {_GEN_0,_GEN_1};
  assign tag_array_0_ext_RW0_wmask = {_GEN_2,_GEN_3};
endmodule
