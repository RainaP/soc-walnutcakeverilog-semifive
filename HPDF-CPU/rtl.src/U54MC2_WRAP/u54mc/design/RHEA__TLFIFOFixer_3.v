//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLFIFOFixer_3(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_a_ready,
  input         auto_in_a_valid,
  input  [1:0]  auto_in_a_bits_source,
  input  [36:0] auto_in_a_bits_address,
  input  [7:0]  auto_in_a_bits_mask,
  input  [63:0] auto_in_a_bits_data,
  input         auto_in_d_ready,
  output        auto_in_d_valid,
  output [2:0]  auto_in_d_bits_opcode,
  output [1:0]  auto_in_d_bits_param,
  output [3:0]  auto_in_d_bits_size,
  output [1:0]  auto_in_d_bits_source,
  output [4:0]  auto_in_d_bits_sink,
  output        auto_in_d_bits_denied,
  output        auto_in_d_bits_corrupt,
  input         auto_out_a_ready,
  output        auto_out_a_valid,
  output [1:0]  auto_out_a_bits_source,
  output [36:0] auto_out_a_bits_address,
  output [7:0]  auto_out_a_bits_mask,
  output [63:0] auto_out_a_bits_data,
  output        auto_out_d_ready,
  input         auto_out_d_valid,
  input  [2:0]  auto_out_d_bits_opcode,
  input  [1:0]  auto_out_d_bits_param,
  input  [3:0]  auto_out_d_bits_size,
  input  [1:0]  auto_out_d_bits_source,
  input  [4:0]  auto_out_d_bits_sink,
  input         auto_out_d_bits_denied,
  input         auto_out_d_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
`endif // RANDOMIZE_REG_INIT
  wire [36:0] _a_id_T = auto_in_a_bits_address ^ 37'h8000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_id_T_1 = {1'b0,$signed(_a_id_T)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_id_T_3 = $signed(_a_id_T_1) & 38'sh188e000000; // @[Parameters.scala 137:52]
  wire  _a_id_T_4 = $signed(_a_id_T_3) == 38'sh0; // @[Parameters.scala 137:67]
  wire [37:0] _a_id_T_6 = {1'b0,$signed(auto_in_a_bits_address)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_id_T_8 = $signed(_a_id_T_6) & 38'sh188c000000; // @[Parameters.scala 137:52]
  wire  _a_id_T_9 = $signed(_a_id_T_8) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _a_id_T_10 = auto_in_a_bits_address ^ 37'hc000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_id_T_11 = {1'b0,$signed(_a_id_T_10)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_id_T_13 = $signed(_a_id_T_11) & 38'sh188c000000; // @[Parameters.scala 137:52]
  wire  _a_id_T_14 = $signed(_a_id_T_13) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _a_id_T_15 = auto_in_a_bits_address ^ 37'h80000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_id_T_16 = {1'b0,$signed(_a_id_T_15)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_id_T_18 = $signed(_a_id_T_16) & 38'sh1880000000; // @[Parameters.scala 137:52]
  wire  _a_id_T_19 = $signed(_a_id_T_18) == 38'sh0; // @[Parameters.scala 137:67]
  wire [36:0] _a_id_T_20 = auto_in_a_bits_address ^ 37'h1000000000; // @[Parameters.scala 137:31]
  wire [37:0] _a_id_T_21 = {1'b0,$signed(_a_id_T_20)}; // @[Parameters.scala 137:49]
  wire [37:0] _a_id_T_23 = $signed(_a_id_T_21) & 38'sh1000000000; // @[Parameters.scala 137:52]
  wire  _a_id_T_24 = $signed(_a_id_T_23) == 38'sh0; // @[Parameters.scala 137:67]
  wire  _a_id_T_27 = _a_id_T_9 | _a_id_T_14 | _a_id_T_19 | _a_id_T_24; // @[Parameters.scala 630:89]
  wire [1:0] _a_id_T_40 = _a_id_T_27 ? 2'h2 : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _GEN_21 = {{1'd0}, _a_id_T_4}; // @[Mux.scala 27:72]
  wire [1:0] a_id = _GEN_21 | _a_id_T_40; // @[Mux.scala 27:72]
  wire  a_noDomain = a_id == 2'h0; // @[FIFOFixer.scala 55:29]
  reg [4:0] a_first_counter; // @[Edges.scala 228:27]
  wire  a_first = a_first_counter == 5'h0; // @[Edges.scala 230:25]
  reg  flight_0; // @[FIFOFixer.scala 71:27]
  reg  flight_1; // @[FIFOFixer.scala 71:27]
  reg  flight_2; // @[FIFOFixer.scala 71:27]
  reg  flight_3; // @[FIFOFixer.scala 71:27]
  wire  _stalls_T_3 = flight_0 | flight_1 | flight_2 | flight_3; // @[FIFOFixer.scala 80:44]
  reg [1:0] stalls_id; // @[Reg.scala 15:16]
  wire  stalls_0 = a_first & _stalls_T_3 & (a_noDomain | stalls_id != a_id); // @[FIFOFixer.scala 80:50]
  wire  _bundleIn_0_a_ready_T = ~stalls_0; // @[FIFOFixer.scala 88:50]
  wire  bundleIn_0_a_ready = auto_out_a_ready & _bundleIn_0_a_ready_T; // @[FIFOFixer.scala 88:33]
  wire  _a_first_T = bundleIn_0_a_ready & auto_in_a_valid; // @[Decoupled.scala 40:37]
  wire [4:0] a_first_counter1 = a_first_counter - 5'h1; // @[Edges.scala 229:28]
  wire  _d_first_T = auto_in_d_ready & auto_out_d_valid; // @[Decoupled.scala 40:37]
  wire [22:0] _d_first_beats1_decode_T_1 = 23'hff << auto_out_d_bits_size; // @[package.scala 234:77]
  wire [7:0] _d_first_beats1_decode_T_3 = ~_d_first_beats1_decode_T_1[7:0]; // @[package.scala 234:46]
  wire [4:0] d_first_beats1_decode = _d_first_beats1_decode_T_3[7:3]; // @[Edges.scala 219:59]
  wire  d_first_beats1_opdata = auto_out_d_bits_opcode[0]; // @[Edges.scala 105:36]
  reg [4:0] d_first_counter; // @[Edges.scala 228:27]
  wire [4:0] d_first_counter1 = d_first_counter - 5'h1; // @[Edges.scala 229:28]
  wire  d_first_first = d_first_counter == 5'h0; // @[Edges.scala 230:25]
  wire  d_first = d_first_first & auto_out_d_bits_opcode != 3'h6; // @[FIFOFixer.scala 67:42]
  wire  _T_1 = a_first & _a_first_T; // @[FIFOFixer.scala 72:21]
  wire  _GEN_6 = _T_1 ? 2'h0 == auto_in_a_bits_source | flight_0 : flight_0; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_7 = _T_1 ? 2'h1 == auto_in_a_bits_source | flight_1 : flight_1; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_8 = _T_1 ? 2'h2 == auto_in_a_bits_source | flight_2 : flight_2; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _GEN_9 = _T_1 ? 2'h3 == auto_in_a_bits_source | flight_3 : flight_3; // @[FIFOFixer.scala 72:37 FIFOFixer.scala 71:27]
  wire  _T_3 = d_first & _d_first_T; // @[FIFOFixer.scala 73:21]
  assign auto_in_a_ready = auto_out_a_ready & _bundleIn_0_a_ready_T; // @[FIFOFixer.scala 88:33]
  assign auto_in_d_valid = auto_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_opcode = auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_param = auto_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_size = auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_source = auto_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_sink = auto_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_denied = auto_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_in_d_bits_corrupt = auto_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign auto_out_a_valid = auto_in_a_valid & _bundleIn_0_a_ready_T; // @[FIFOFixer.scala 87:33]
  assign auto_out_a_bits_source = auto_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_a_bits_data = auto_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_out_d_ready = auto_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      a_first_counter <= 5'h0;
    end else if (_a_first_T) begin
      if (a_first) begin
        a_first_counter <= 5'h0;
      end else begin
        a_first_counter <= a_first_counter1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_0 <= 1'h0;
    end else if (_T_3) begin
      if (2'h0 == auto_out_d_bits_source) begin
        flight_0 <= 1'h0;
      end else begin
        flight_0 <= _GEN_6;
      end
    end else begin
      flight_0 <= _GEN_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_1 <= 1'h0;
    end else if (_T_3) begin
      if (2'h1 == auto_out_d_bits_source) begin
        flight_1 <= 1'h0;
      end else begin
        flight_1 <= _GEN_7;
      end
    end else begin
      flight_1 <= _GEN_7;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_2 <= 1'h0;
    end else if (_T_3) begin
      if (2'h2 == auto_out_d_bits_source) begin
        flight_2 <= 1'h0;
      end else begin
        flight_2 <= _GEN_8;
      end
    end else begin
      flight_2 <= _GEN_8;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flight_3 <= 1'h0;
    end else if (_T_3) begin
      if (2'h3 == auto_out_d_bits_source) begin
        flight_3 <= 1'h0;
      end else begin
        flight_3 <= _GEN_9;
      end
    end else begin
      flight_3 <= _GEN_9;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      stalls_id <= 2'h0;
    end else if (_a_first_T) begin
      stalls_id <= a_id;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      d_first_counter <= 5'h0;
    end else if (_d_first_T) begin
      if (d_first_first) begin
        if (d_first_beats1_opdata) begin
          d_first_counter <= d_first_beats1_decode;
        end else begin
          d_first_counter <= 5'h0;
        end
      end else begin
        d_first_counter <= d_first_counter1;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  a_first_counter = _RAND_0[4:0];
  _RAND_1 = {1{`RANDOM}};
  flight_0 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  flight_1 = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  flight_2 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  flight_3 = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  stalls_id = _RAND_5[1:0];
  _RAND_6 = {1{`RANDOM}};
  d_first_counter = _RAND_6[4:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    a_first_counter = 5'h0;
  end
  if (reset) begin
    flight_0 = 1'h0;
  end
  if (reset) begin
    flight_1 = 1'h0;
  end
  if (reset) begin
    flight_2 = 1'h0;
  end
  if (reset) begin
    flight_3 = 1'h0;
  end
  if (rf_reset) begin
    stalls_id = 2'h0;
  end
  if (reset) begin
    d_first_counter = 5'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
