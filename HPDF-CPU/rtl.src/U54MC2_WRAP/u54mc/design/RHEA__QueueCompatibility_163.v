//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__QueueCompatibility_163(
  input          rf_reset,
  input          clock,
  input          reset,
  output         io_enq_ready,
  input          io_enq_valid,
  input  [127:0] io_enq_bits_data,
  input          io_deq_ready,
  output         io_deq_valid,
  output [127:0] io_deq_bits_data
);
`ifdef RANDOMIZE_REG_INIT
  reg [127:0] _RAND_0;
  reg [127:0] _RAND_1;
  reg [127:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
`endif // RANDOMIZE_REG_INIT
  reg [127:0] ram_0_data; // @[Decoupled.scala 218:16]
  reg [127:0] ram_1_data; // @[Decoupled.scala 218:16]
  reg [127:0] ram_2_data; // @[Decoupled.scala 218:16]
  reg [1:0] value_1; // @[Counter.scala 60:40]
  wire [127:0] _GEN_3 = 2'h1 == value_1 ? ram_1_data : ram_0_data; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [127:0] ramIntf_io_deq_bits_MPORT_data_data = 2'h2 == value_1 ? ram_2_data : _GEN_3; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  reg [1:0] value; // @[Counter.scala 60:40]
  wire  ptr_match = value == value_1; // @[Decoupled.scala 223:33]
  reg  maybe_full; // @[Decoupled.scala 221:27]
  wire  empty = ptr_match & ~maybe_full; // @[Decoupled.scala 224:25]
  wire  _do_enq_T = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  wire  _GEN_49 = io_deq_ready ? 1'h0 : _do_enq_T; // @[Decoupled.scala 249:27 Decoupled.scala 249:36]
  wire  do_enq = empty ? _GEN_49 : _do_enq_T; // @[Decoupled.scala 246:18]
  wire  full = ptr_match & maybe_full; // @[Decoupled.scala 225:24]
  wire  _do_deq_T = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  wire  wrap = value == 2'h2; // @[Counter.scala 72:24]
  wire [1:0] _value_T_1 = value + 2'h1; // @[Counter.scala 76:24]
  wire  wrap_1 = value_1 == 2'h2; // @[Counter.scala 72:24]
  wire [1:0] _value_T_3 = value_1 + 2'h1; // @[Counter.scala 76:24]
  wire  do_deq = empty ? 1'h0 : _do_deq_T; // @[Decoupled.scala 246:18 Decoupled.scala 248:14]
  assign io_enq_ready = ~full; // @[Decoupled.scala 241:19]
  assign io_deq_valid = io_enq_valid | ~empty; // @[Decoupled.scala 245:25 Decoupled.scala 245:40 Decoupled.scala 240:16]
  assign io_deq_bits_data = empty ? io_enq_bits_data : ramIntf_io_deq_bits_MPORT_data_data; // @[Decoupled.scala 246:18 Decoupled.scala 247:19 Decoupled.scala 242:15]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_data <= 128'h0;
    end else if (do_enq) begin
      if (2'h0 == value) begin
        ram_0_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_data <= 128'h0;
    end else if (do_enq) begin
      if (2'h1 == value) begin
        ram_1_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_data <= 128'h0;
    end else if (do_enq) begin
      if (2'h2 == value) begin
        ram_2_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      value_1 <= 2'h0;
    end else if (do_deq) begin
      if (wrap_1) begin
        value_1 <= 2'h0;
      end else begin
        value_1 <= _value_T_3;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      value <= 2'h0;
    end else if (do_enq) begin
      if (wrap) begin
        value <= 2'h0;
      end else begin
        value <= _value_T_1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      maybe_full <= 1'h0;
    end else if (do_enq != do_deq) begin
      if (empty) begin
        if (io_deq_ready) begin
          maybe_full <= 1'h0;
        end else begin
          maybe_full <= _do_enq_T;
        end
      end else begin
        maybe_full <= _do_enq_T;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {4{`RANDOM}};
  ram_0_data = _RAND_0[127:0];
  _RAND_1 = {4{`RANDOM}};
  ram_1_data = _RAND_1[127:0];
  _RAND_2 = {4{`RANDOM}};
  ram_2_data = _RAND_2[127:0];
  _RAND_3 = {1{`RANDOM}};
  value_1 = _RAND_3[1:0];
  _RAND_4 = {1{`RANDOM}};
  value = _RAND_4[1:0];
  _RAND_5 = {1{`RANDOM}};
  maybe_full = _RAND_5[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ram_0_data = 128'h0;
  end
  if (rf_reset) begin
    ram_1_data = 128'h0;
  end
  if (rf_reset) begin
    ram_2_data = 128'h0;
  end
  if (reset) begin
    value_1 = 2'h0;
  end
  if (reset) begin
    value = 2'h0;
  end
  if (reset) begin
    maybe_full = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
