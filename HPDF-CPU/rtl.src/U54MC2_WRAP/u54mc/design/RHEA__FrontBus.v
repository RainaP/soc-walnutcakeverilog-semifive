//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__FrontBus(
  input         rf_reset,
  output        auto_coupler_from_trace_sba_widget_in_a_ready,
  input         auto_coupler_from_trace_sba_widget_in_a_valid,
  input  [1:0]  auto_coupler_from_trace_sba_widget_in_a_bits_source,
  input  [36:0] auto_coupler_from_trace_sba_widget_in_a_bits_address,
  input  [31:0] auto_coupler_from_trace_sba_widget_in_a_bits_data,
  output        auto_coupler_from_trace_sba_widget_in_d_valid,
  output [1:0]  auto_coupler_from_trace_sba_widget_in_d_bits_source,
  output        auto_coupler_from_trace_sba_widget_in_d_bits_denied,
  output        auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_ready,
  input         auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_valid,
  input  [15:0] auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_bits_id,
  input  [36:0] auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_bits_addr,
  input  [7:0]  auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_bits_len,
  input  [2:0]  auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_bits_size,
  input  [1:0]  auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_bits_burst,
  input  [3:0]  auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_bits_cache,
  input  [2:0]  auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_bits_prot,
  output        auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_w_ready,
  input         auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_w_valid,
  input  [63:0] auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_w_bits_data,
  input  [7:0]  auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_w_bits_strb,
  input         auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_w_bits_last,
  input         auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_b_ready,
  output        auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_b_valid,
  output [15:0] auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_b_bits_id,
  output [1:0]  auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_b_bits_resp,
  output        auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_ready,
  input         auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_valid,
  input  [15:0] auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_bits_id,
  input  [36:0] auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_bits_addr,
  input  [7:0]  auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_bits_len,
  input  [2:0]  auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_bits_size,
  input  [1:0]  auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_bits_burst,
  input  [3:0]  auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_bits_cache,
  input  [2:0]  auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_bits_prot,
  input         auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_r_ready,
  output        auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_r_valid,
  output [15:0] auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_r_bits_id,
  output [63:0] auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_r_bits_data,
  output [1:0]  auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_r_bits_resp,
  output        auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_r_bits_last,
  output        auto_coupler_from_debug_sb_widget_in_a_ready,
  input         auto_coupler_from_debug_sb_widget_in_a_valid,
  input  [2:0]  auto_coupler_from_debug_sb_widget_in_a_bits_opcode,
  input  [3:0]  auto_coupler_from_debug_sb_widget_in_a_bits_size,
  input  [36:0] auto_coupler_from_debug_sb_widget_in_a_bits_address,
  input  [7:0]  auto_coupler_from_debug_sb_widget_in_a_bits_data,
  input         auto_coupler_from_debug_sb_widget_in_d_ready,
  output        auto_coupler_from_debug_sb_widget_in_d_valid,
  output        auto_coupler_from_debug_sb_widget_in_d_bits_denied,
  output [7:0]  auto_coupler_from_debug_sb_widget_in_d_bits_data,
  output        auto_coupler_from_debug_sb_widget_in_d_bits_corrupt,
  input         auto_subsystem_fbus_clock_groups_in_member_subsystem_fbus_0_clock,
  input         auto_subsystem_fbus_clock_groups_in_member_subsystem_fbus_0_reset,
  input         auto_bus_xing_out_a_ready,
  output        auto_bus_xing_out_a_valid,
  output [2:0]  auto_bus_xing_out_a_bits_opcode,
  output [3:0]  auto_bus_xing_out_a_bits_size,
  output [5:0]  auto_bus_xing_out_a_bits_source,
  output [36:0] auto_bus_xing_out_a_bits_address,
  output        auto_bus_xing_out_a_bits_user_amba_prot_bufferable,
  output        auto_bus_xing_out_a_bits_user_amba_prot_modifiable,
  output        auto_bus_xing_out_a_bits_user_amba_prot_readalloc,
  output        auto_bus_xing_out_a_bits_user_amba_prot_writealloc,
  output        auto_bus_xing_out_a_bits_user_amba_prot_privileged,
  output        auto_bus_xing_out_a_bits_user_amba_prot_secure,
  output        auto_bus_xing_out_a_bits_user_amba_prot_fetch,
  output [7:0]  auto_bus_xing_out_a_bits_mask,
  output [63:0] auto_bus_xing_out_a_bits_data,
  output        auto_bus_xing_out_d_ready,
  input         auto_bus_xing_out_d_valid,
  input  [2:0]  auto_bus_xing_out_d_bits_opcode,
  input  [1:0]  auto_bus_xing_out_d_bits_param,
  input  [3:0]  auto_bus_xing_out_d_bits_size,
  input  [5:0]  auto_bus_xing_out_d_bits_source,
  input  [4:0]  auto_bus_xing_out_d_bits_sink,
  input         auto_bus_xing_out_d_bits_denied,
  input  [63:0] auto_bus_xing_out_d_bits_data,
  input         auto_bus_xing_out_d_bits_corrupt
);
  wire  subsystem_fbus_clock_groups_auto_in_member_subsystem_fbus_0_clock;
  wire  subsystem_fbus_clock_groups_auto_in_member_subsystem_fbus_0_reset;
  wire  subsystem_fbus_clock_groups_auto_out_member_subsystem_fbus_0_clock;
  wire  subsystem_fbus_clock_groups_auto_out_member_subsystem_fbus_0_reset;
  wire  clockGroup_auto_in_member_subsystem_fbus_0_clock;
  wire  clockGroup_auto_in_member_subsystem_fbus_0_reset;
  wire  clockGroup_auto_out_clock;
  wire  clockGroup_auto_out_reset;
  wire  fixedClockNode_auto_in_clock;
  wire  fixedClockNode_auto_in_reset;
  wire  fixedClockNode_auto_out_clock;
  wire  fixedClockNode_auto_out_reset;
  wire  subsystem_fbus_xbar_rf_reset; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_clock; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_reset; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_2_a_ready; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_2_a_valid; // @[BusWrapper.scala 357:32]
  wire [1:0] subsystem_fbus_xbar_auto_in_2_a_bits_source; // @[BusWrapper.scala 357:32]
  wire [36:0] subsystem_fbus_xbar_auto_in_2_a_bits_address; // @[BusWrapper.scala 357:32]
  wire [7:0] subsystem_fbus_xbar_auto_in_2_a_bits_mask; // @[BusWrapper.scala 357:32]
  wire [63:0] subsystem_fbus_xbar_auto_in_2_a_bits_data; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_2_d_ready; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_2_d_valid; // @[BusWrapper.scala 357:32]
  wire [2:0] subsystem_fbus_xbar_auto_in_2_d_bits_opcode; // @[BusWrapper.scala 357:32]
  wire [1:0] subsystem_fbus_xbar_auto_in_2_d_bits_param; // @[BusWrapper.scala 357:32]
  wire [3:0] subsystem_fbus_xbar_auto_in_2_d_bits_size; // @[BusWrapper.scala 357:32]
  wire [1:0] subsystem_fbus_xbar_auto_in_2_d_bits_source; // @[BusWrapper.scala 357:32]
  wire [4:0] subsystem_fbus_xbar_auto_in_2_d_bits_sink; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_2_d_bits_denied; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_2_d_bits_corrupt; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_1_a_ready; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_1_a_valid; // @[BusWrapper.scala 357:32]
  wire [2:0] subsystem_fbus_xbar_auto_in_1_a_bits_opcode; // @[BusWrapper.scala 357:32]
  wire [3:0] subsystem_fbus_xbar_auto_in_1_a_bits_size; // @[BusWrapper.scala 357:32]
  wire [4:0] subsystem_fbus_xbar_auto_in_1_a_bits_source; // @[BusWrapper.scala 357:32]
  wire [36:0] subsystem_fbus_xbar_auto_in_1_a_bits_address; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_bufferable; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_modifiable; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_readalloc; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_writealloc; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_privileged; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_secure; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_fetch; // @[BusWrapper.scala 357:32]
  wire [7:0] subsystem_fbus_xbar_auto_in_1_a_bits_mask; // @[BusWrapper.scala 357:32]
  wire [63:0] subsystem_fbus_xbar_auto_in_1_a_bits_data; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_1_d_ready; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_1_d_valid; // @[BusWrapper.scala 357:32]
  wire [2:0] subsystem_fbus_xbar_auto_in_1_d_bits_opcode; // @[BusWrapper.scala 357:32]
  wire [1:0] subsystem_fbus_xbar_auto_in_1_d_bits_param; // @[BusWrapper.scala 357:32]
  wire [3:0] subsystem_fbus_xbar_auto_in_1_d_bits_size; // @[BusWrapper.scala 357:32]
  wire [4:0] subsystem_fbus_xbar_auto_in_1_d_bits_source; // @[BusWrapper.scala 357:32]
  wire [4:0] subsystem_fbus_xbar_auto_in_1_d_bits_sink; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_1_d_bits_denied; // @[BusWrapper.scala 357:32]
  wire [63:0] subsystem_fbus_xbar_auto_in_1_d_bits_data; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_1_d_bits_corrupt; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_0_a_ready; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_0_a_valid; // @[BusWrapper.scala 357:32]
  wire [2:0] subsystem_fbus_xbar_auto_in_0_a_bits_opcode; // @[BusWrapper.scala 357:32]
  wire [3:0] subsystem_fbus_xbar_auto_in_0_a_bits_size; // @[BusWrapper.scala 357:32]
  wire [36:0] subsystem_fbus_xbar_auto_in_0_a_bits_address; // @[BusWrapper.scala 357:32]
  wire [7:0] subsystem_fbus_xbar_auto_in_0_a_bits_mask; // @[BusWrapper.scala 357:32]
  wire [63:0] subsystem_fbus_xbar_auto_in_0_a_bits_data; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_0_d_ready; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_0_d_valid; // @[BusWrapper.scala 357:32]
  wire [2:0] subsystem_fbus_xbar_auto_in_0_d_bits_opcode; // @[BusWrapper.scala 357:32]
  wire [1:0] subsystem_fbus_xbar_auto_in_0_d_bits_param; // @[BusWrapper.scala 357:32]
  wire [3:0] subsystem_fbus_xbar_auto_in_0_d_bits_size; // @[BusWrapper.scala 357:32]
  wire [4:0] subsystem_fbus_xbar_auto_in_0_d_bits_sink; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_0_d_bits_denied; // @[BusWrapper.scala 357:32]
  wire [63:0] subsystem_fbus_xbar_auto_in_0_d_bits_data; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_in_0_d_bits_corrupt; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_out_a_ready; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_out_a_valid; // @[BusWrapper.scala 357:32]
  wire [2:0] subsystem_fbus_xbar_auto_out_a_bits_opcode; // @[BusWrapper.scala 357:32]
  wire [3:0] subsystem_fbus_xbar_auto_out_a_bits_size; // @[BusWrapper.scala 357:32]
  wire [5:0] subsystem_fbus_xbar_auto_out_a_bits_source; // @[BusWrapper.scala 357:32]
  wire [36:0] subsystem_fbus_xbar_auto_out_a_bits_address; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_bufferable; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_modifiable; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_readalloc; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_writealloc; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_privileged; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_secure; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_fetch; // @[BusWrapper.scala 357:32]
  wire [7:0] subsystem_fbus_xbar_auto_out_a_bits_mask; // @[BusWrapper.scala 357:32]
  wire [63:0] subsystem_fbus_xbar_auto_out_a_bits_data; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_out_d_ready; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_out_d_valid; // @[BusWrapper.scala 357:32]
  wire [2:0] subsystem_fbus_xbar_auto_out_d_bits_opcode; // @[BusWrapper.scala 357:32]
  wire [1:0] subsystem_fbus_xbar_auto_out_d_bits_param; // @[BusWrapper.scala 357:32]
  wire [3:0] subsystem_fbus_xbar_auto_out_d_bits_size; // @[BusWrapper.scala 357:32]
  wire [5:0] subsystem_fbus_xbar_auto_out_d_bits_source; // @[BusWrapper.scala 357:32]
  wire [4:0] subsystem_fbus_xbar_auto_out_d_bits_sink; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_out_d_bits_denied; // @[BusWrapper.scala 357:32]
  wire [63:0] subsystem_fbus_xbar_auto_out_d_bits_data; // @[BusWrapper.scala 357:32]
  wire  subsystem_fbus_xbar_auto_out_d_bits_corrupt; // @[BusWrapper.scala 357:32]
  wire  buffer_rf_reset; // @[Buffer.scala 68:28]
  wire  buffer_clock; // @[Buffer.scala 68:28]
  wire  buffer_reset; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_in_a_bits_opcode; // @[Buffer.scala 68:28]
  wire [3:0] buffer_auto_in_a_bits_size; // @[Buffer.scala 68:28]
  wire [5:0] buffer_auto_in_a_bits_source; // @[Buffer.scala 68:28]
  wire [36:0] buffer_auto_in_a_bits_address; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_user_amba_prot_bufferable; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_user_amba_prot_modifiable; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_user_amba_prot_readalloc; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_user_amba_prot_writealloc; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_user_amba_prot_privileged; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_user_amba_prot_secure; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_bits_user_amba_prot_fetch; // @[Buffer.scala 68:28]
  wire [7:0] buffer_auto_in_a_bits_mask; // @[Buffer.scala 68:28]
  wire [63:0] buffer_auto_in_a_bits_data; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_d_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_d_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_in_d_bits_opcode; // @[Buffer.scala 68:28]
  wire [1:0] buffer_auto_in_d_bits_param; // @[Buffer.scala 68:28]
  wire [3:0] buffer_auto_in_d_bits_size; // @[Buffer.scala 68:28]
  wire [5:0] buffer_auto_in_d_bits_source; // @[Buffer.scala 68:28]
  wire [4:0] buffer_auto_in_d_bits_sink; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_d_bits_denied; // @[Buffer.scala 68:28]
  wire [63:0] buffer_auto_in_d_bits_data; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_d_bits_corrupt; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_out_a_bits_opcode; // @[Buffer.scala 68:28]
  wire [3:0] buffer_auto_out_a_bits_size; // @[Buffer.scala 68:28]
  wire [5:0] buffer_auto_out_a_bits_source; // @[Buffer.scala 68:28]
  wire [36:0] buffer_auto_out_a_bits_address; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_user_amba_prot_bufferable; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_user_amba_prot_modifiable; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_user_amba_prot_readalloc; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_user_amba_prot_writealloc; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_user_amba_prot_privileged; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_user_amba_prot_secure; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_bits_user_amba_prot_fetch; // @[Buffer.scala 68:28]
  wire [7:0] buffer_auto_out_a_bits_mask; // @[Buffer.scala 68:28]
  wire [63:0] buffer_auto_out_a_bits_data; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_d_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_d_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_out_d_bits_opcode; // @[Buffer.scala 68:28]
  wire [1:0] buffer_auto_out_d_bits_param; // @[Buffer.scala 68:28]
  wire [3:0] buffer_auto_out_d_bits_size; // @[Buffer.scala 68:28]
  wire [5:0] buffer_auto_out_d_bits_source; // @[Buffer.scala 68:28]
  wire [4:0] buffer_auto_out_d_bits_sink; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_d_bits_denied; // @[Buffer.scala 68:28]
  wire [63:0] buffer_auto_out_d_bits_data; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_d_bits_corrupt; // @[Buffer.scala 68:28]
  wire  coupler_from_debug_sb_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_from_debug_sb_clock; // @[LazyModule.scala 524:27]
  wire  coupler_from_debug_sb_reset; // @[LazyModule.scala 524:27]
  wire  coupler_from_debug_sb_auto_widget_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_debug_sb_auto_widget_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_debug_sb_auto_widget_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_debug_sb_auto_widget_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_debug_sb_auto_widget_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_from_debug_sb_auto_widget_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_debug_sb_auto_widget_in_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_debug_sb_auto_widget_in_d_valid; // @[LazyModule.scala 524:27]
  wire  coupler_from_debug_sb_auto_widget_in_d_bits_denied; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_from_debug_sb_auto_widget_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_debug_sb_auto_widget_in_d_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_debug_sb_auto_tl_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_debug_sb_auto_tl_out_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_debug_sb_auto_tl_out_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_debug_sb_auto_tl_out_a_bits_size; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_debug_sb_auto_tl_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_from_debug_sb_auto_tl_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_from_debug_sb_auto_tl_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_debug_sb_auto_tl_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_debug_sb_auto_tl_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_debug_sb_auto_tl_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_debug_sb_auto_tl_out_d_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_debug_sb_auto_tl_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_debug_sb_auto_tl_out_d_bits_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_debug_sb_auto_tl_out_d_bits_denied; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_from_debug_sb_auto_tl_out_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_debug_sb_auto_tl_out_d_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_clock; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_reset; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_valid; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_id; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_addr; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_len; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_size; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_burst; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_cache; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_prot; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_w_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_w_valid; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_w_bits_data; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_w_bits_strb; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_w_bits_last; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_b_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_b_valid; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_b_bits_id; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_b_bits_resp; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_valid; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_id; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_addr; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_len; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_size; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_burst; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_cache; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_prot; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_valid; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_bits_id; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_bits_data; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_bits_resp; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_bits_last; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_tl_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_tl_out_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_tl_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_tl_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_source; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_denied; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_from_trace_sba_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_from_trace_sba_clock; // @[LazyModule.scala 524:27]
  wire  coupler_from_trace_sba_reset; // @[LazyModule.scala 524:27]
  wire  coupler_from_trace_sba_auto_widget_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_trace_sba_auto_widget_in_a_valid; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_trace_sba_auto_widget_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_trace_sba_auto_widget_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire [31:0] coupler_from_trace_sba_auto_widget_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_trace_sba_auto_widget_in_d_valid; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_trace_sba_auto_widget_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire  coupler_from_trace_sba_auto_widget_in_d_bits_denied; // @[LazyModule.scala 524:27]
  wire  coupler_from_trace_sba_auto_tl_out_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_trace_sba_auto_tl_out_a_valid; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_trace_sba_auto_tl_out_a_bits_source; // @[LazyModule.scala 524:27]
  wire [36:0] coupler_from_trace_sba_auto_tl_out_a_bits_address; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_from_trace_sba_auto_tl_out_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [63:0] coupler_from_trace_sba_auto_tl_out_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_from_trace_sba_auto_tl_out_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_from_trace_sba_auto_tl_out_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_from_trace_sba_auto_tl_out_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_trace_sba_auto_tl_out_d_bits_param; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_from_trace_sba_auto_tl_out_d_bits_size; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_from_trace_sba_auto_tl_out_d_bits_source; // @[LazyModule.scala 524:27]
  wire [4:0] coupler_from_trace_sba_auto_tl_out_d_bits_sink; // @[LazyModule.scala 524:27]
  wire  coupler_from_trace_sba_auto_tl_out_d_bits_denied; // @[LazyModule.scala 524:27]
  wire  coupler_from_trace_sba_auto_tl_out_d_bits_corrupt; // @[LazyModule.scala 524:27]
  RHEA__TLXbar_3 subsystem_fbus_xbar ( // @[BusWrapper.scala 357:32]
    .rf_reset(subsystem_fbus_xbar_rf_reset),
    .clock(subsystem_fbus_xbar_clock),
    .reset(subsystem_fbus_xbar_reset),
    .auto_in_2_a_ready(subsystem_fbus_xbar_auto_in_2_a_ready),
    .auto_in_2_a_valid(subsystem_fbus_xbar_auto_in_2_a_valid),
    .auto_in_2_a_bits_source(subsystem_fbus_xbar_auto_in_2_a_bits_source),
    .auto_in_2_a_bits_address(subsystem_fbus_xbar_auto_in_2_a_bits_address),
    .auto_in_2_a_bits_mask(subsystem_fbus_xbar_auto_in_2_a_bits_mask),
    .auto_in_2_a_bits_data(subsystem_fbus_xbar_auto_in_2_a_bits_data),
    .auto_in_2_d_ready(subsystem_fbus_xbar_auto_in_2_d_ready),
    .auto_in_2_d_valid(subsystem_fbus_xbar_auto_in_2_d_valid),
    .auto_in_2_d_bits_opcode(subsystem_fbus_xbar_auto_in_2_d_bits_opcode),
    .auto_in_2_d_bits_param(subsystem_fbus_xbar_auto_in_2_d_bits_param),
    .auto_in_2_d_bits_size(subsystem_fbus_xbar_auto_in_2_d_bits_size),
    .auto_in_2_d_bits_source(subsystem_fbus_xbar_auto_in_2_d_bits_source),
    .auto_in_2_d_bits_sink(subsystem_fbus_xbar_auto_in_2_d_bits_sink),
    .auto_in_2_d_bits_denied(subsystem_fbus_xbar_auto_in_2_d_bits_denied),
    .auto_in_2_d_bits_corrupt(subsystem_fbus_xbar_auto_in_2_d_bits_corrupt),
    .auto_in_1_a_ready(subsystem_fbus_xbar_auto_in_1_a_ready),
    .auto_in_1_a_valid(subsystem_fbus_xbar_auto_in_1_a_valid),
    .auto_in_1_a_bits_opcode(subsystem_fbus_xbar_auto_in_1_a_bits_opcode),
    .auto_in_1_a_bits_size(subsystem_fbus_xbar_auto_in_1_a_bits_size),
    .auto_in_1_a_bits_source(subsystem_fbus_xbar_auto_in_1_a_bits_source),
    .auto_in_1_a_bits_address(subsystem_fbus_xbar_auto_in_1_a_bits_address),
    .auto_in_1_a_bits_user_amba_prot_bufferable(subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_bufferable),
    .auto_in_1_a_bits_user_amba_prot_modifiable(subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_modifiable),
    .auto_in_1_a_bits_user_amba_prot_readalloc(subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_readalloc),
    .auto_in_1_a_bits_user_amba_prot_writealloc(subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_writealloc),
    .auto_in_1_a_bits_user_amba_prot_privileged(subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_privileged),
    .auto_in_1_a_bits_user_amba_prot_secure(subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_secure),
    .auto_in_1_a_bits_user_amba_prot_fetch(subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_fetch),
    .auto_in_1_a_bits_mask(subsystem_fbus_xbar_auto_in_1_a_bits_mask),
    .auto_in_1_a_bits_data(subsystem_fbus_xbar_auto_in_1_a_bits_data),
    .auto_in_1_d_ready(subsystem_fbus_xbar_auto_in_1_d_ready),
    .auto_in_1_d_valid(subsystem_fbus_xbar_auto_in_1_d_valid),
    .auto_in_1_d_bits_opcode(subsystem_fbus_xbar_auto_in_1_d_bits_opcode),
    .auto_in_1_d_bits_param(subsystem_fbus_xbar_auto_in_1_d_bits_param),
    .auto_in_1_d_bits_size(subsystem_fbus_xbar_auto_in_1_d_bits_size),
    .auto_in_1_d_bits_source(subsystem_fbus_xbar_auto_in_1_d_bits_source),
    .auto_in_1_d_bits_sink(subsystem_fbus_xbar_auto_in_1_d_bits_sink),
    .auto_in_1_d_bits_denied(subsystem_fbus_xbar_auto_in_1_d_bits_denied),
    .auto_in_1_d_bits_data(subsystem_fbus_xbar_auto_in_1_d_bits_data),
    .auto_in_1_d_bits_corrupt(subsystem_fbus_xbar_auto_in_1_d_bits_corrupt),
    .auto_in_0_a_ready(subsystem_fbus_xbar_auto_in_0_a_ready),
    .auto_in_0_a_valid(subsystem_fbus_xbar_auto_in_0_a_valid),
    .auto_in_0_a_bits_opcode(subsystem_fbus_xbar_auto_in_0_a_bits_opcode),
    .auto_in_0_a_bits_size(subsystem_fbus_xbar_auto_in_0_a_bits_size),
    .auto_in_0_a_bits_address(subsystem_fbus_xbar_auto_in_0_a_bits_address),
    .auto_in_0_a_bits_mask(subsystem_fbus_xbar_auto_in_0_a_bits_mask),
    .auto_in_0_a_bits_data(subsystem_fbus_xbar_auto_in_0_a_bits_data),
    .auto_in_0_d_ready(subsystem_fbus_xbar_auto_in_0_d_ready),
    .auto_in_0_d_valid(subsystem_fbus_xbar_auto_in_0_d_valid),
    .auto_in_0_d_bits_opcode(subsystem_fbus_xbar_auto_in_0_d_bits_opcode),
    .auto_in_0_d_bits_param(subsystem_fbus_xbar_auto_in_0_d_bits_param),
    .auto_in_0_d_bits_size(subsystem_fbus_xbar_auto_in_0_d_bits_size),
    .auto_in_0_d_bits_sink(subsystem_fbus_xbar_auto_in_0_d_bits_sink),
    .auto_in_0_d_bits_denied(subsystem_fbus_xbar_auto_in_0_d_bits_denied),
    .auto_in_0_d_bits_data(subsystem_fbus_xbar_auto_in_0_d_bits_data),
    .auto_in_0_d_bits_corrupt(subsystem_fbus_xbar_auto_in_0_d_bits_corrupt),
    .auto_out_a_ready(subsystem_fbus_xbar_auto_out_a_ready),
    .auto_out_a_valid(subsystem_fbus_xbar_auto_out_a_valid),
    .auto_out_a_bits_opcode(subsystem_fbus_xbar_auto_out_a_bits_opcode),
    .auto_out_a_bits_size(subsystem_fbus_xbar_auto_out_a_bits_size),
    .auto_out_a_bits_source(subsystem_fbus_xbar_auto_out_a_bits_source),
    .auto_out_a_bits_address(subsystem_fbus_xbar_auto_out_a_bits_address),
    .auto_out_a_bits_user_amba_prot_bufferable(subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_bufferable),
    .auto_out_a_bits_user_amba_prot_modifiable(subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_modifiable),
    .auto_out_a_bits_user_amba_prot_readalloc(subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_readalloc),
    .auto_out_a_bits_user_amba_prot_writealloc(subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_writealloc),
    .auto_out_a_bits_user_amba_prot_privileged(subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_privileged),
    .auto_out_a_bits_user_amba_prot_secure(subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_secure),
    .auto_out_a_bits_user_amba_prot_fetch(subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_fetch),
    .auto_out_a_bits_mask(subsystem_fbus_xbar_auto_out_a_bits_mask),
    .auto_out_a_bits_data(subsystem_fbus_xbar_auto_out_a_bits_data),
    .auto_out_d_ready(subsystem_fbus_xbar_auto_out_d_ready),
    .auto_out_d_valid(subsystem_fbus_xbar_auto_out_d_valid),
    .auto_out_d_bits_opcode(subsystem_fbus_xbar_auto_out_d_bits_opcode),
    .auto_out_d_bits_param(subsystem_fbus_xbar_auto_out_d_bits_param),
    .auto_out_d_bits_size(subsystem_fbus_xbar_auto_out_d_bits_size),
    .auto_out_d_bits_source(subsystem_fbus_xbar_auto_out_d_bits_source),
    .auto_out_d_bits_sink(subsystem_fbus_xbar_auto_out_d_bits_sink),
    .auto_out_d_bits_denied(subsystem_fbus_xbar_auto_out_d_bits_denied),
    .auto_out_d_bits_data(subsystem_fbus_xbar_auto_out_d_bits_data),
    .auto_out_d_bits_corrupt(subsystem_fbus_xbar_auto_out_d_bits_corrupt)
  );
  RHEA__TLBuffer_4 buffer ( // @[Buffer.scala 68:28]
    .rf_reset(buffer_rf_reset),
    .clock(buffer_clock),
    .reset(buffer_reset),
    .auto_in_a_ready(buffer_auto_in_a_ready),
    .auto_in_a_valid(buffer_auto_in_a_valid),
    .auto_in_a_bits_opcode(buffer_auto_in_a_bits_opcode),
    .auto_in_a_bits_size(buffer_auto_in_a_bits_size),
    .auto_in_a_bits_source(buffer_auto_in_a_bits_source),
    .auto_in_a_bits_address(buffer_auto_in_a_bits_address),
    .auto_in_a_bits_user_amba_prot_bufferable(buffer_auto_in_a_bits_user_amba_prot_bufferable),
    .auto_in_a_bits_user_amba_prot_modifiable(buffer_auto_in_a_bits_user_amba_prot_modifiable),
    .auto_in_a_bits_user_amba_prot_readalloc(buffer_auto_in_a_bits_user_amba_prot_readalloc),
    .auto_in_a_bits_user_amba_prot_writealloc(buffer_auto_in_a_bits_user_amba_prot_writealloc),
    .auto_in_a_bits_user_amba_prot_privileged(buffer_auto_in_a_bits_user_amba_prot_privileged),
    .auto_in_a_bits_user_amba_prot_secure(buffer_auto_in_a_bits_user_amba_prot_secure),
    .auto_in_a_bits_user_amba_prot_fetch(buffer_auto_in_a_bits_user_amba_prot_fetch),
    .auto_in_a_bits_mask(buffer_auto_in_a_bits_mask),
    .auto_in_a_bits_data(buffer_auto_in_a_bits_data),
    .auto_in_d_ready(buffer_auto_in_d_ready),
    .auto_in_d_valid(buffer_auto_in_d_valid),
    .auto_in_d_bits_opcode(buffer_auto_in_d_bits_opcode),
    .auto_in_d_bits_param(buffer_auto_in_d_bits_param),
    .auto_in_d_bits_size(buffer_auto_in_d_bits_size),
    .auto_in_d_bits_source(buffer_auto_in_d_bits_source),
    .auto_in_d_bits_sink(buffer_auto_in_d_bits_sink),
    .auto_in_d_bits_denied(buffer_auto_in_d_bits_denied),
    .auto_in_d_bits_data(buffer_auto_in_d_bits_data),
    .auto_in_d_bits_corrupt(buffer_auto_in_d_bits_corrupt),
    .auto_out_a_ready(buffer_auto_out_a_ready),
    .auto_out_a_valid(buffer_auto_out_a_valid),
    .auto_out_a_bits_opcode(buffer_auto_out_a_bits_opcode),
    .auto_out_a_bits_size(buffer_auto_out_a_bits_size),
    .auto_out_a_bits_source(buffer_auto_out_a_bits_source),
    .auto_out_a_bits_address(buffer_auto_out_a_bits_address),
    .auto_out_a_bits_user_amba_prot_bufferable(buffer_auto_out_a_bits_user_amba_prot_bufferable),
    .auto_out_a_bits_user_amba_prot_modifiable(buffer_auto_out_a_bits_user_amba_prot_modifiable),
    .auto_out_a_bits_user_amba_prot_readalloc(buffer_auto_out_a_bits_user_amba_prot_readalloc),
    .auto_out_a_bits_user_amba_prot_writealloc(buffer_auto_out_a_bits_user_amba_prot_writealloc),
    .auto_out_a_bits_user_amba_prot_privileged(buffer_auto_out_a_bits_user_amba_prot_privileged),
    .auto_out_a_bits_user_amba_prot_secure(buffer_auto_out_a_bits_user_amba_prot_secure),
    .auto_out_a_bits_user_amba_prot_fetch(buffer_auto_out_a_bits_user_amba_prot_fetch),
    .auto_out_a_bits_mask(buffer_auto_out_a_bits_mask),
    .auto_out_a_bits_data(buffer_auto_out_a_bits_data),
    .auto_out_d_ready(buffer_auto_out_d_ready),
    .auto_out_d_valid(buffer_auto_out_d_valid),
    .auto_out_d_bits_opcode(buffer_auto_out_d_bits_opcode),
    .auto_out_d_bits_param(buffer_auto_out_d_bits_param),
    .auto_out_d_bits_size(buffer_auto_out_d_bits_size),
    .auto_out_d_bits_source(buffer_auto_out_d_bits_source),
    .auto_out_d_bits_sink(buffer_auto_out_d_bits_sink),
    .auto_out_d_bits_denied(buffer_auto_out_d_bits_denied),
    .auto_out_d_bits_data(buffer_auto_out_d_bits_data),
    .auto_out_d_bits_corrupt(buffer_auto_out_d_bits_corrupt)
  );
  RHEA__TLInterconnectCoupler_7 coupler_from_debug_sb ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_from_debug_sb_rf_reset),
    .clock(coupler_from_debug_sb_clock),
    .reset(coupler_from_debug_sb_reset),
    .auto_widget_in_a_ready(coupler_from_debug_sb_auto_widget_in_a_ready),
    .auto_widget_in_a_valid(coupler_from_debug_sb_auto_widget_in_a_valid),
    .auto_widget_in_a_bits_opcode(coupler_from_debug_sb_auto_widget_in_a_bits_opcode),
    .auto_widget_in_a_bits_size(coupler_from_debug_sb_auto_widget_in_a_bits_size),
    .auto_widget_in_a_bits_address(coupler_from_debug_sb_auto_widget_in_a_bits_address),
    .auto_widget_in_a_bits_data(coupler_from_debug_sb_auto_widget_in_a_bits_data),
    .auto_widget_in_d_ready(coupler_from_debug_sb_auto_widget_in_d_ready),
    .auto_widget_in_d_valid(coupler_from_debug_sb_auto_widget_in_d_valid),
    .auto_widget_in_d_bits_denied(coupler_from_debug_sb_auto_widget_in_d_bits_denied),
    .auto_widget_in_d_bits_data(coupler_from_debug_sb_auto_widget_in_d_bits_data),
    .auto_widget_in_d_bits_corrupt(coupler_from_debug_sb_auto_widget_in_d_bits_corrupt),
    .auto_tl_out_a_ready(coupler_from_debug_sb_auto_tl_out_a_ready),
    .auto_tl_out_a_valid(coupler_from_debug_sb_auto_tl_out_a_valid),
    .auto_tl_out_a_bits_opcode(coupler_from_debug_sb_auto_tl_out_a_bits_opcode),
    .auto_tl_out_a_bits_size(coupler_from_debug_sb_auto_tl_out_a_bits_size),
    .auto_tl_out_a_bits_address(coupler_from_debug_sb_auto_tl_out_a_bits_address),
    .auto_tl_out_a_bits_mask(coupler_from_debug_sb_auto_tl_out_a_bits_mask),
    .auto_tl_out_a_bits_data(coupler_from_debug_sb_auto_tl_out_a_bits_data),
    .auto_tl_out_d_ready(coupler_from_debug_sb_auto_tl_out_d_ready),
    .auto_tl_out_d_valid(coupler_from_debug_sb_auto_tl_out_d_valid),
    .auto_tl_out_d_bits_opcode(coupler_from_debug_sb_auto_tl_out_d_bits_opcode),
    .auto_tl_out_d_bits_param(coupler_from_debug_sb_auto_tl_out_d_bits_param),
    .auto_tl_out_d_bits_size(coupler_from_debug_sb_auto_tl_out_d_bits_size),
    .auto_tl_out_d_bits_sink(coupler_from_debug_sb_auto_tl_out_d_bits_sink),
    .auto_tl_out_d_bits_denied(coupler_from_debug_sb_auto_tl_out_d_bits_denied),
    .auto_tl_out_d_bits_data(coupler_from_debug_sb_auto_tl_out_d_bits_data),
    .auto_tl_out_d_bits_corrupt(coupler_from_debug_sb_auto_tl_out_d_bits_corrupt)
  );
  RHEA__TLInterconnectCoupler_8 coupler_from_port_named_axi4_front_port ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_from_port_named_axi4_front_port_rf_reset),
    .clock(coupler_from_port_named_axi4_front_port_clock),
    .reset(coupler_from_port_named_axi4_front_port_reset),
    .auto_bridge_for_axi4_front_port_axi4_in_aw_ready(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_ready),
    .auto_bridge_for_axi4_front_port_axi4_in_aw_valid(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_valid),
    .auto_bridge_for_axi4_front_port_axi4_in_aw_bits_id(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_id),
    .auto_bridge_for_axi4_front_port_axi4_in_aw_bits_addr(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_addr),
    .auto_bridge_for_axi4_front_port_axi4_in_aw_bits_len(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_len),
    .auto_bridge_for_axi4_front_port_axi4_in_aw_bits_size(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_size),
    .auto_bridge_for_axi4_front_port_axi4_in_aw_bits_burst(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_burst),
    .auto_bridge_for_axi4_front_port_axi4_in_aw_bits_cache(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_cache),
    .auto_bridge_for_axi4_front_port_axi4_in_aw_bits_prot(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_prot),
    .auto_bridge_for_axi4_front_port_axi4_in_w_ready(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_w_ready),
    .auto_bridge_for_axi4_front_port_axi4_in_w_valid(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_w_valid),
    .auto_bridge_for_axi4_front_port_axi4_in_w_bits_data(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_w_bits_data),
    .auto_bridge_for_axi4_front_port_axi4_in_w_bits_strb(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_w_bits_strb),
    .auto_bridge_for_axi4_front_port_axi4_in_w_bits_last(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_w_bits_last),
    .auto_bridge_for_axi4_front_port_axi4_in_b_ready(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_b_ready),
    .auto_bridge_for_axi4_front_port_axi4_in_b_valid(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_b_valid),
    .auto_bridge_for_axi4_front_port_axi4_in_b_bits_id(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_b_bits_id),
    .auto_bridge_for_axi4_front_port_axi4_in_b_bits_resp(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_b_bits_resp),
    .auto_bridge_for_axi4_front_port_axi4_in_ar_ready(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_ready),
    .auto_bridge_for_axi4_front_port_axi4_in_ar_valid(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_valid),
    .auto_bridge_for_axi4_front_port_axi4_in_ar_bits_id(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_id),
    .auto_bridge_for_axi4_front_port_axi4_in_ar_bits_addr(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_addr),
    .auto_bridge_for_axi4_front_port_axi4_in_ar_bits_len(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_len),
    .auto_bridge_for_axi4_front_port_axi4_in_ar_bits_size(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_size),
    .auto_bridge_for_axi4_front_port_axi4_in_ar_bits_burst(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_burst),
    .auto_bridge_for_axi4_front_port_axi4_in_ar_bits_cache(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_cache),
    .auto_bridge_for_axi4_front_port_axi4_in_ar_bits_prot(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_prot),
    .auto_bridge_for_axi4_front_port_axi4_in_r_ready(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_ready),
    .auto_bridge_for_axi4_front_port_axi4_in_r_valid(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_valid),
    .auto_bridge_for_axi4_front_port_axi4_in_r_bits_id(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_bits_id),
    .auto_bridge_for_axi4_front_port_axi4_in_r_bits_data(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_bits_data),
    .auto_bridge_for_axi4_front_port_axi4_in_r_bits_resp(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_bits_resp),
    .auto_bridge_for_axi4_front_port_axi4_in_r_bits_last(
      coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_bits_last),
    .auto_tl_out_a_ready(coupler_from_port_named_axi4_front_port_auto_tl_out_a_ready),
    .auto_tl_out_a_valid(coupler_from_port_named_axi4_front_port_auto_tl_out_a_valid),
    .auto_tl_out_a_bits_opcode(coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_opcode),
    .auto_tl_out_a_bits_size(coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_size),
    .auto_tl_out_a_bits_source(coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_source),
    .auto_tl_out_a_bits_address(coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_address),
    .auto_tl_out_a_bits_user_amba_prot_bufferable(
      coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_bufferable),
    .auto_tl_out_a_bits_user_amba_prot_modifiable(
      coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_modifiable),
    .auto_tl_out_a_bits_user_amba_prot_readalloc(
      coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_readalloc),
    .auto_tl_out_a_bits_user_amba_prot_writealloc(
      coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_writealloc),
    .auto_tl_out_a_bits_user_amba_prot_privileged(
      coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_privileged),
    .auto_tl_out_a_bits_user_amba_prot_secure(
      coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_secure),
    .auto_tl_out_a_bits_user_amba_prot_fetch(
      coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_fetch),
    .auto_tl_out_a_bits_mask(coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_mask),
    .auto_tl_out_a_bits_data(coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_data),
    .auto_tl_out_d_ready(coupler_from_port_named_axi4_front_port_auto_tl_out_d_ready),
    .auto_tl_out_d_valid(coupler_from_port_named_axi4_front_port_auto_tl_out_d_valid),
    .auto_tl_out_d_bits_opcode(coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_opcode),
    .auto_tl_out_d_bits_param(coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_param),
    .auto_tl_out_d_bits_size(coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_size),
    .auto_tl_out_d_bits_source(coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_source),
    .auto_tl_out_d_bits_sink(coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_sink),
    .auto_tl_out_d_bits_denied(coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_denied),
    .auto_tl_out_d_bits_data(coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_data),
    .auto_tl_out_d_bits_corrupt(coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_corrupt)
  );
  RHEA__TLInterconnectCoupler_9 coupler_from_trace_sba ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_from_trace_sba_rf_reset),
    .clock(coupler_from_trace_sba_clock),
    .reset(coupler_from_trace_sba_reset),
    .auto_widget_in_a_ready(coupler_from_trace_sba_auto_widget_in_a_ready),
    .auto_widget_in_a_valid(coupler_from_trace_sba_auto_widget_in_a_valid),
    .auto_widget_in_a_bits_source(coupler_from_trace_sba_auto_widget_in_a_bits_source),
    .auto_widget_in_a_bits_address(coupler_from_trace_sba_auto_widget_in_a_bits_address),
    .auto_widget_in_a_bits_data(coupler_from_trace_sba_auto_widget_in_a_bits_data),
    .auto_widget_in_d_valid(coupler_from_trace_sba_auto_widget_in_d_valid),
    .auto_widget_in_d_bits_source(coupler_from_trace_sba_auto_widget_in_d_bits_source),
    .auto_widget_in_d_bits_denied(coupler_from_trace_sba_auto_widget_in_d_bits_denied),
    .auto_tl_out_a_ready(coupler_from_trace_sba_auto_tl_out_a_ready),
    .auto_tl_out_a_valid(coupler_from_trace_sba_auto_tl_out_a_valid),
    .auto_tl_out_a_bits_source(coupler_from_trace_sba_auto_tl_out_a_bits_source),
    .auto_tl_out_a_bits_address(coupler_from_trace_sba_auto_tl_out_a_bits_address),
    .auto_tl_out_a_bits_mask(coupler_from_trace_sba_auto_tl_out_a_bits_mask),
    .auto_tl_out_a_bits_data(coupler_from_trace_sba_auto_tl_out_a_bits_data),
    .auto_tl_out_d_ready(coupler_from_trace_sba_auto_tl_out_d_ready),
    .auto_tl_out_d_valid(coupler_from_trace_sba_auto_tl_out_d_valid),
    .auto_tl_out_d_bits_opcode(coupler_from_trace_sba_auto_tl_out_d_bits_opcode),
    .auto_tl_out_d_bits_param(coupler_from_trace_sba_auto_tl_out_d_bits_param),
    .auto_tl_out_d_bits_size(coupler_from_trace_sba_auto_tl_out_d_bits_size),
    .auto_tl_out_d_bits_source(coupler_from_trace_sba_auto_tl_out_d_bits_source),
    .auto_tl_out_d_bits_sink(coupler_from_trace_sba_auto_tl_out_d_bits_sink),
    .auto_tl_out_d_bits_denied(coupler_from_trace_sba_auto_tl_out_d_bits_denied),
    .auto_tl_out_d_bits_corrupt(coupler_from_trace_sba_auto_tl_out_d_bits_corrupt)
  );
  assign subsystem_fbus_clock_groups_auto_out_member_subsystem_fbus_0_clock =
    subsystem_fbus_clock_groups_auto_in_member_subsystem_fbus_0_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_fbus_clock_groups_auto_out_member_subsystem_fbus_0_reset =
    subsystem_fbus_clock_groups_auto_in_member_subsystem_fbus_0_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign clockGroup_auto_out_clock = clockGroup_auto_in_member_subsystem_fbus_0_clock; // @[ClockGroup.scala 8:11 LazyModule.scala 388:16]
  assign clockGroup_auto_out_reset = clockGroup_auto_in_member_subsystem_fbus_0_reset; // @[ClockGroup.scala 8:11 LazyModule.scala 388:16]
  assign fixedClockNode_auto_out_clock = fixedClockNode_auto_in_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign fixedClockNode_auto_out_reset = fixedClockNode_auto_in_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_fbus_xbar_rf_reset = rf_reset;
  assign buffer_rf_reset = rf_reset;
  assign coupler_from_debug_sb_rf_reset = rf_reset;
  assign coupler_from_port_named_axi4_front_port_rf_reset = rf_reset;
  assign coupler_from_trace_sba_rf_reset = rf_reset;
  assign auto_coupler_from_trace_sba_widget_in_a_ready = coupler_from_trace_sba_auto_widget_in_a_ready; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_trace_sba_widget_in_d_valid = coupler_from_trace_sba_auto_widget_in_d_valid; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_trace_sba_widget_in_d_bits_source = coupler_from_trace_sba_auto_widget_in_d_bits_source; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_trace_sba_widget_in_d_bits_denied = coupler_from_trace_sba_auto_widget_in_d_bits_denied; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_ready =
    coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_ready; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_w_ready =
    coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_w_ready; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_b_valid =
    coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_b_valid; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_b_bits_id =
    coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_b_bits_id; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_b_bits_resp =
    coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_b_bits_resp; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_ready =
    coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_ready; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_r_valid =
    coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_valid; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_r_bits_id =
    coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_bits_id; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_r_bits_data =
    coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_bits_data; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_r_bits_resp =
    coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_bits_resp; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_r_bits_last =
    coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_bits_last; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_debug_sb_widget_in_a_ready = coupler_from_debug_sb_auto_widget_in_a_ready; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_debug_sb_widget_in_d_valid = coupler_from_debug_sb_auto_widget_in_d_valid; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_debug_sb_widget_in_d_bits_denied = coupler_from_debug_sb_auto_widget_in_d_bits_denied; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_debug_sb_widget_in_d_bits_data = coupler_from_debug_sb_auto_widget_in_d_bits_data; // @[LazyModule.scala 388:16]
  assign auto_coupler_from_debug_sb_widget_in_d_bits_corrupt = coupler_from_debug_sb_auto_widget_in_d_bits_corrupt; // @[LazyModule.scala 388:16]
  assign auto_bus_xing_out_a_valid = buffer_auto_out_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bus_xing_out_a_bits_opcode = buffer_auto_out_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bus_xing_out_a_bits_size = buffer_auto_out_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bus_xing_out_a_bits_source = buffer_auto_out_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bus_xing_out_a_bits_address = buffer_auto_out_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bus_xing_out_a_bits_user_amba_prot_bufferable = buffer_auto_out_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bus_xing_out_a_bits_user_amba_prot_modifiable = buffer_auto_out_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bus_xing_out_a_bits_user_amba_prot_readalloc = buffer_auto_out_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bus_xing_out_a_bits_user_amba_prot_writealloc = buffer_auto_out_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bus_xing_out_a_bits_user_amba_prot_privileged = buffer_auto_out_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bus_xing_out_a_bits_user_amba_prot_secure = buffer_auto_out_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bus_xing_out_a_bits_user_amba_prot_fetch = buffer_auto_out_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bus_xing_out_a_bits_mask = buffer_auto_out_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bus_xing_out_a_bits_data = buffer_auto_out_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_bus_xing_out_d_ready = buffer_auto_out_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign subsystem_fbus_clock_groups_auto_in_member_subsystem_fbus_0_clock =
    auto_subsystem_fbus_clock_groups_in_member_subsystem_fbus_0_clock; // @[LazyModule.scala 388:16]
  assign subsystem_fbus_clock_groups_auto_in_member_subsystem_fbus_0_reset =
    auto_subsystem_fbus_clock_groups_in_member_subsystem_fbus_0_reset; // @[LazyModule.scala 388:16]
  assign clockGroup_auto_in_member_subsystem_fbus_0_clock =
    subsystem_fbus_clock_groups_auto_out_member_subsystem_fbus_0_clock; // @[LazyModule.scala 377:16]
  assign clockGroup_auto_in_member_subsystem_fbus_0_reset =
    subsystem_fbus_clock_groups_auto_out_member_subsystem_fbus_0_reset; // @[LazyModule.scala 377:16]
  assign fixedClockNode_auto_in_clock = clockGroup_auto_out_clock; // @[LazyModule.scala 377:16]
  assign fixedClockNode_auto_in_reset = clockGroup_auto_out_reset; // @[LazyModule.scala 377:16]
  assign subsystem_fbus_xbar_clock = fixedClockNode_auto_out_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_reset = fixedClockNode_auto_out_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_2_a_valid = coupler_from_trace_sba_auto_tl_out_a_valid; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_2_a_bits_source = coupler_from_trace_sba_auto_tl_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_2_a_bits_address = coupler_from_trace_sba_auto_tl_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_2_a_bits_mask = coupler_from_trace_sba_auto_tl_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_2_a_bits_data = coupler_from_trace_sba_auto_tl_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_2_d_ready = coupler_from_trace_sba_auto_tl_out_d_ready; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_1_a_valid = coupler_from_port_named_axi4_front_port_auto_tl_out_a_valid; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_1_a_bits_opcode = coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_opcode
    ; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_1_a_bits_size = coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_1_a_bits_source = coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_source
    ; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_1_a_bits_address =
    coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_bufferable =
    coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_modifiable =
    coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_readalloc =
    coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_writealloc =
    coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_privileged =
    coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_secure =
    coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_1_a_bits_user_amba_prot_fetch =
    coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_1_a_bits_mask = coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_1_a_bits_data = coupler_from_port_named_axi4_front_port_auto_tl_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_1_d_ready = coupler_from_port_named_axi4_front_port_auto_tl_out_d_ready; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_0_a_valid = coupler_from_debug_sb_auto_tl_out_a_valid; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_0_a_bits_opcode = coupler_from_debug_sb_auto_tl_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_0_a_bits_size = coupler_from_debug_sb_auto_tl_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_0_a_bits_address = coupler_from_debug_sb_auto_tl_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_0_a_bits_mask = coupler_from_debug_sb_auto_tl_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_0_a_bits_data = coupler_from_debug_sb_auto_tl_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_in_0_d_ready = coupler_from_debug_sb_auto_tl_out_d_ready; // @[LazyModule.scala 375:16]
  assign subsystem_fbus_xbar_auto_out_a_ready = buffer_auto_in_a_ready; // @[LazyModule.scala 377:16]
  assign subsystem_fbus_xbar_auto_out_d_valid = buffer_auto_in_d_valid; // @[LazyModule.scala 377:16]
  assign subsystem_fbus_xbar_auto_out_d_bits_opcode = buffer_auto_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign subsystem_fbus_xbar_auto_out_d_bits_param = buffer_auto_in_d_bits_param; // @[LazyModule.scala 377:16]
  assign subsystem_fbus_xbar_auto_out_d_bits_size = buffer_auto_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign subsystem_fbus_xbar_auto_out_d_bits_source = buffer_auto_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign subsystem_fbus_xbar_auto_out_d_bits_sink = buffer_auto_in_d_bits_sink; // @[LazyModule.scala 377:16]
  assign subsystem_fbus_xbar_auto_out_d_bits_denied = buffer_auto_in_d_bits_denied; // @[LazyModule.scala 377:16]
  assign subsystem_fbus_xbar_auto_out_d_bits_data = buffer_auto_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign subsystem_fbus_xbar_auto_out_d_bits_corrupt = buffer_auto_in_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign buffer_clock = fixedClockNode_auto_out_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign buffer_reset = fixedClockNode_auto_out_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign buffer_auto_in_a_valid = subsystem_fbus_xbar_auto_out_a_valid; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_a_bits_opcode = subsystem_fbus_xbar_auto_out_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_a_bits_size = subsystem_fbus_xbar_auto_out_a_bits_size; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_a_bits_source = subsystem_fbus_xbar_auto_out_a_bits_source; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_a_bits_address = subsystem_fbus_xbar_auto_out_a_bits_address; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_a_bits_user_amba_prot_bufferable = subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_bufferable
    ; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_a_bits_user_amba_prot_modifiable = subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_modifiable
    ; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_a_bits_user_amba_prot_readalloc = subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_a_bits_user_amba_prot_writealloc = subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_writealloc
    ; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_a_bits_user_amba_prot_privileged = subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_privileged
    ; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_a_bits_user_amba_prot_secure = subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_a_bits_user_amba_prot_fetch = subsystem_fbus_xbar_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_a_bits_mask = subsystem_fbus_xbar_auto_out_a_bits_mask; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_a_bits_data = subsystem_fbus_xbar_auto_out_a_bits_data; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_d_ready = subsystem_fbus_xbar_auto_out_d_ready; // @[LazyModule.scala 377:16]
  assign buffer_auto_out_a_ready = auto_bus_xing_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_valid = auto_bus_xing_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_opcode = auto_bus_xing_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_param = auto_bus_xing_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_size = auto_bus_xing_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_source = auto_bus_xing_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_sink = auto_bus_xing_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_denied = auto_bus_xing_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_data = auto_bus_xing_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_corrupt = auto_bus_xing_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign coupler_from_debug_sb_clock = fixedClockNode_auto_out_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_from_debug_sb_reset = fixedClockNode_auto_out_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_from_debug_sb_auto_widget_in_a_valid = auto_coupler_from_debug_sb_widget_in_a_valid; // @[LazyModule.scala 388:16]
  assign coupler_from_debug_sb_auto_widget_in_a_bits_opcode = auto_coupler_from_debug_sb_widget_in_a_bits_opcode; // @[LazyModule.scala 388:16]
  assign coupler_from_debug_sb_auto_widget_in_a_bits_size = auto_coupler_from_debug_sb_widget_in_a_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_from_debug_sb_auto_widget_in_a_bits_address = auto_coupler_from_debug_sb_widget_in_a_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_from_debug_sb_auto_widget_in_a_bits_data = auto_coupler_from_debug_sb_widget_in_a_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_from_debug_sb_auto_widget_in_d_ready = auto_coupler_from_debug_sb_widget_in_d_ready; // @[LazyModule.scala 388:16]
  assign coupler_from_debug_sb_auto_tl_out_a_ready = subsystem_fbus_xbar_auto_in_0_a_ready; // @[LazyModule.scala 375:16]
  assign coupler_from_debug_sb_auto_tl_out_d_valid = subsystem_fbus_xbar_auto_in_0_d_valid; // @[LazyModule.scala 375:16]
  assign coupler_from_debug_sb_auto_tl_out_d_bits_opcode = subsystem_fbus_xbar_auto_in_0_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign coupler_from_debug_sb_auto_tl_out_d_bits_param = subsystem_fbus_xbar_auto_in_0_d_bits_param; // @[LazyModule.scala 375:16]
  assign coupler_from_debug_sb_auto_tl_out_d_bits_size = subsystem_fbus_xbar_auto_in_0_d_bits_size; // @[LazyModule.scala 375:16]
  assign coupler_from_debug_sb_auto_tl_out_d_bits_sink = subsystem_fbus_xbar_auto_in_0_d_bits_sink; // @[LazyModule.scala 375:16]
  assign coupler_from_debug_sb_auto_tl_out_d_bits_denied = subsystem_fbus_xbar_auto_in_0_d_bits_denied; // @[LazyModule.scala 375:16]
  assign coupler_from_debug_sb_auto_tl_out_d_bits_data = subsystem_fbus_xbar_auto_in_0_d_bits_data; // @[LazyModule.scala 375:16]
  assign coupler_from_debug_sb_auto_tl_out_d_bits_corrupt = subsystem_fbus_xbar_auto_in_0_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coupler_from_port_named_axi4_front_port_clock = fixedClockNode_auto_out_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_from_port_named_axi4_front_port_reset = fixedClockNode_auto_out_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_valid =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_valid; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_id =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_bits_id; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_addr =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_bits_addr; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_len =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_bits_len; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_size =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_burst =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_bits_burst; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_cache =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_bits_cache; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_aw_bits_prot =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_aw_bits_prot; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_w_valid =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_w_valid; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_w_bits_data =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_w_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_w_bits_strb =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_w_bits_strb; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_w_bits_last =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_w_bits_last; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_b_ready =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_b_ready; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_valid =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_valid; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_id =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_bits_id; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_addr =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_bits_addr; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_len =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_bits_len; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_size =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_bits_size; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_burst =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_bits_burst; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_cache =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_bits_cache; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_ar_bits_prot =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_ar_bits_prot; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_bridge_for_axi4_front_port_axi4_in_r_ready =
    auto_coupler_from_port_named_axi4_front_port_bridge_for_axi4_front_port_axi4_in_r_ready; // @[LazyModule.scala 388:16]
  assign coupler_from_port_named_axi4_front_port_auto_tl_out_a_ready = subsystem_fbus_xbar_auto_in_1_a_ready; // @[LazyModule.scala 375:16]
  assign coupler_from_port_named_axi4_front_port_auto_tl_out_d_valid = subsystem_fbus_xbar_auto_in_1_d_valid; // @[LazyModule.scala 375:16]
  assign coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_opcode = subsystem_fbus_xbar_auto_in_1_d_bits_opcode
    ; // @[LazyModule.scala 375:16]
  assign coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_param = subsystem_fbus_xbar_auto_in_1_d_bits_param; // @[LazyModule.scala 375:16]
  assign coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_size = subsystem_fbus_xbar_auto_in_1_d_bits_size; // @[LazyModule.scala 375:16]
  assign coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_source = subsystem_fbus_xbar_auto_in_1_d_bits_source
    ; // @[LazyModule.scala 375:16]
  assign coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_sink = subsystem_fbus_xbar_auto_in_1_d_bits_sink; // @[LazyModule.scala 375:16]
  assign coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_denied = subsystem_fbus_xbar_auto_in_1_d_bits_denied
    ; // @[LazyModule.scala 375:16]
  assign coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_data = subsystem_fbus_xbar_auto_in_1_d_bits_data; // @[LazyModule.scala 375:16]
  assign coupler_from_port_named_axi4_front_port_auto_tl_out_d_bits_corrupt =
    subsystem_fbus_xbar_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coupler_from_trace_sba_clock = fixedClockNode_auto_out_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_from_trace_sba_reset = fixedClockNode_auto_out_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_from_trace_sba_auto_widget_in_a_valid = auto_coupler_from_trace_sba_widget_in_a_valid; // @[LazyModule.scala 388:16]
  assign coupler_from_trace_sba_auto_widget_in_a_bits_source = auto_coupler_from_trace_sba_widget_in_a_bits_source; // @[LazyModule.scala 388:16]
  assign coupler_from_trace_sba_auto_widget_in_a_bits_address = auto_coupler_from_trace_sba_widget_in_a_bits_address; // @[LazyModule.scala 388:16]
  assign coupler_from_trace_sba_auto_widget_in_a_bits_data = auto_coupler_from_trace_sba_widget_in_a_bits_data; // @[LazyModule.scala 388:16]
  assign coupler_from_trace_sba_auto_tl_out_a_ready = subsystem_fbus_xbar_auto_in_2_a_ready; // @[LazyModule.scala 375:16]
  assign coupler_from_trace_sba_auto_tl_out_d_valid = subsystem_fbus_xbar_auto_in_2_d_valid; // @[LazyModule.scala 375:16]
  assign coupler_from_trace_sba_auto_tl_out_d_bits_opcode = subsystem_fbus_xbar_auto_in_2_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign coupler_from_trace_sba_auto_tl_out_d_bits_param = subsystem_fbus_xbar_auto_in_2_d_bits_param; // @[LazyModule.scala 375:16]
  assign coupler_from_trace_sba_auto_tl_out_d_bits_size = subsystem_fbus_xbar_auto_in_2_d_bits_size; // @[LazyModule.scala 375:16]
  assign coupler_from_trace_sba_auto_tl_out_d_bits_source = subsystem_fbus_xbar_auto_in_2_d_bits_source; // @[LazyModule.scala 375:16]
  assign coupler_from_trace_sba_auto_tl_out_d_bits_sink = subsystem_fbus_xbar_auto_in_2_d_bits_sink; // @[LazyModule.scala 375:16]
  assign coupler_from_trace_sba_auto_tl_out_d_bits_denied = subsystem_fbus_xbar_auto_in_2_d_bits_denied; // @[LazyModule.scala 375:16]
  assign coupler_from_trace_sba_auto_tl_out_d_bits_corrupt = subsystem_fbus_xbar_auto_in_2_d_bits_corrupt; // @[LazyModule.scala 375:16]
endmodule
