//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__SinkC(
  input          rf_reset,
  input          clock,
  input          reset,
  input          io_req_ready,
  output         io_req_valid,
  output [2:0]   io_req_bits_opcode,
  output [2:0]   io_req_bits_param,
  output [2:0]   io_req_bits_size,
  output [6:0]   io_req_bits_source,
  output [17:0]  io_req_bits_tag,
  output [5:0]   io_req_bits_offset,
  output [4:0]   io_req_bits_put,
  output [9:0]   io_req_bits_set,
  output         io_resp_valid,
  output         io_resp_bits_last,
  output [9:0]   io_resp_bits_set,
  output [17:0]  io_resp_bits_tag,
  output [6:0]   io_resp_bits_source,
  output [2:0]   io_resp_bits_param,
  output         io_resp_bits_data,
  output         io_c_ready,
  input          io_c_valid,
  input  [2:0]   io_c_bits_opcode,
  input  [2:0]   io_c_bits_param,
  input  [2:0]   io_c_bits_size,
  input  [6:0]   io_c_bits_source,
  input  [35:0]  io_c_bits_address,
  input  [127:0] io_c_bits_data,
  input          io_c_bits_corrupt,
  output [9:0]   io_set,
  input  [2:0]   io_way,
  input          io_bs_adr_ready,
  output         io_bs_adr_valid,
  output         io_bs_adr_bits_noop,
  output [2:0]   io_bs_adr_bits_way,
  output [9:0]   io_bs_adr_bits_set,
  output [1:0]   io_bs_adr_bits_beat,
  output [127:0] io_bs_dat_data,
  output         io_bs_dat_poison,
  output         io_rel_pop_ready,
  input          io_rel_pop_valid,
  input  [4:0]   io_rel_pop_bits_index,
  input          io_rel_pop_bits_last,
  output [127:0] io_rel_beat_data,
  output         io_rel_beat_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [127:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
`endif // RANDOMIZE_REG_INIT
  wire  c_rf_reset; // @[Decoupled.scala 296:21]
  wire  c_clock; // @[Decoupled.scala 296:21]
  wire  c_reset; // @[Decoupled.scala 296:21]
  wire  c_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  c_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] c_io_enq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] c_io_enq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] c_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] c_io_enq_bits_source; // @[Decoupled.scala 296:21]
  wire [35:0] c_io_enq_bits_address; // @[Decoupled.scala 296:21]
  wire [127:0] c_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire  c_io_enq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  c_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  c_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [2:0] c_io_deq_bits_opcode; // @[Decoupled.scala 296:21]
  wire [2:0] c_io_deq_bits_param; // @[Decoupled.scala 296:21]
  wire [2:0] c_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [6:0] c_io_deq_bits_source; // @[Decoupled.scala 296:21]
  wire [35:0] c_io_deq_bits_address; // @[Decoupled.scala 296:21]
  wire [127:0] c_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire  c_io_deq_bits_corrupt; // @[Decoupled.scala 296:21]
  wire  io_bs_adr_q_rf_reset; // @[Decoupled.scala 296:21]
  wire  io_bs_adr_q_clock; // @[Decoupled.scala 296:21]
  wire  io_bs_adr_q_reset; // @[Decoupled.scala 296:21]
  wire  io_bs_adr_q_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  io_bs_adr_q_io_enq_valid; // @[Decoupled.scala 296:21]
  wire  io_bs_adr_q_io_enq_bits_noop; // @[Decoupled.scala 296:21]
  wire [2:0] io_bs_adr_q_io_enq_bits_way; // @[Decoupled.scala 296:21]
  wire [9:0] io_bs_adr_q_io_enq_bits_set; // @[Decoupled.scala 296:21]
  wire [1:0] io_bs_adr_q_io_enq_bits_beat; // @[Decoupled.scala 296:21]
  wire  io_bs_adr_q_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  io_bs_adr_q_io_deq_valid; // @[Decoupled.scala 296:21]
  wire  io_bs_adr_q_io_deq_bits_noop; // @[Decoupled.scala 296:21]
  wire [2:0] io_bs_adr_q_io_deq_bits_way; // @[Decoupled.scala 296:21]
  wire [9:0] io_bs_adr_q_io_deq_bits_set; // @[Decoupled.scala 296:21]
  wire [1:0] io_bs_adr_q_io_deq_bits_beat; // @[Decoupled.scala 296:21]
  wire  putbuffer_rf_reset; // @[SinkC.scala 112:27]
  wire  putbuffer_clock; // @[SinkC.scala 112:27]
  wire  putbuffer_reset; // @[SinkC.scala 112:27]
  wire  putbuffer_io_push_ready; // @[SinkC.scala 112:27]
  wire  putbuffer_io_push_valid; // @[SinkC.scala 112:27]
  wire  putbuffer_io_push_bits_index; // @[SinkC.scala 112:27]
  wire [127:0] putbuffer_io_push_bits_data_data; // @[SinkC.scala 112:27]
  wire  putbuffer_io_push_bits_data_corrupt; // @[SinkC.scala 112:27]
  wire [1:0] putbuffer_io_valid; // @[SinkC.scala 112:27]
  wire  putbuffer_io_pop_valid; // @[SinkC.scala 112:27]
  wire  putbuffer_io_pop_bits; // @[SinkC.scala 112:27]
  wire [127:0] putbuffer_io_data_data; // @[SinkC.scala 112:27]
  wire  putbuffer_io_data_corrupt; // @[SinkC.scala 112:27]
  wire  offset_lo_lo_lo_lo_lo = c_io_deq_bits_address[0]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_lo_lo_hi = c_io_deq_bits_address[1]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_lo_hi_lo = c_io_deq_bits_address[2]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_lo_hi_hi = c_io_deq_bits_address[3]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_hi_lo_lo = c_io_deq_bits_address[4]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_hi_lo_hi = c_io_deq_bits_address[5]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_hi_hi_lo = c_io_deq_bits_address[8]; // @[Parameters.scala 227:47]
  wire  offset_lo_lo_hi_hi_hi = c_io_deq_bits_address[9]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_lo_lo_lo = c_io_deq_bits_address[10]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_lo_lo_hi = c_io_deq_bits_address[11]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_lo_hi_lo = c_io_deq_bits_address[12]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_lo_hi_hi = c_io_deq_bits_address[13]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_hi_lo_lo = c_io_deq_bits_address[14]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_hi_lo_hi = c_io_deq_bits_address[15]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_hi_hi_lo = c_io_deq_bits_address[16]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_hi_hi_hi_lo = c_io_deq_bits_address[17]; // @[Parameters.scala 227:47]
  wire  offset_lo_hi_hi_hi_hi_hi = c_io_deq_bits_address[18]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_lo_lo_lo = c_io_deq_bits_address[19]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_lo_lo_hi = c_io_deq_bits_address[20]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_lo_hi_lo = c_io_deq_bits_address[21]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_lo_hi_hi = c_io_deq_bits_address[22]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_hi_lo_lo = c_io_deq_bits_address[23]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_hi_lo_hi = c_io_deq_bits_address[24]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_hi_hi_lo = c_io_deq_bits_address[25]; // @[Parameters.scala 227:47]
  wire  offset_hi_lo_hi_hi_hi = c_io_deq_bits_address[26]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_lo_lo_lo = c_io_deq_bits_address[27]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_lo_lo_hi = c_io_deq_bits_address[28]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_lo_hi_lo = c_io_deq_bits_address[29]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_lo_hi_hi = c_io_deq_bits_address[30]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_hi_lo_lo = c_io_deq_bits_address[31]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_hi_lo_hi = c_io_deq_bits_address[32]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_hi_hi_lo = c_io_deq_bits_address[33]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_hi_hi_hi_lo = c_io_deq_bits_address[34]; // @[Parameters.scala 227:47]
  wire  offset_hi_hi_hi_hi_hi_hi = c_io_deq_bits_address[35]; // @[Parameters.scala 227:47]
  wire [7:0] offset_lo_lo = {offset_lo_lo_hi_hi_hi,offset_lo_lo_hi_hi_lo,offset_lo_lo_hi_lo_hi,offset_lo_lo_hi_lo_lo,
    offset_lo_lo_lo_hi_hi,offset_lo_lo_lo_hi_lo,offset_lo_lo_lo_lo_hi,offset_lo_lo_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [16:0] offset_lo = {offset_lo_hi_hi_hi_hi_hi,offset_lo_hi_hi_hi_hi_lo,offset_lo_hi_hi_hi_lo,offset_lo_hi_hi_lo_hi
    ,offset_lo_hi_hi_lo_lo,offset_lo_hi_lo_hi_hi,offset_lo_hi_lo_hi_lo,offset_lo_hi_lo_lo_hi,offset_lo_hi_lo_lo_lo,
    offset_lo_lo}; // @[Cat.scala 30:58]
  wire [7:0] offset_hi_lo = {offset_hi_lo_hi_hi_hi,offset_hi_lo_hi_hi_lo,offset_hi_lo_hi_lo_hi,offset_hi_lo_hi_lo_lo,
    offset_hi_lo_lo_hi_hi,offset_hi_lo_lo_hi_lo,offset_hi_lo_lo_lo_hi,offset_hi_lo_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [16:0] offset_hi = {offset_hi_hi_hi_hi_hi_hi,offset_hi_hi_hi_hi_hi_lo,offset_hi_hi_hi_hi_lo,offset_hi_hi_hi_lo_hi
    ,offset_hi_hi_hi_lo_lo,offset_hi_hi_lo_hi_hi,offset_hi_hi_lo_hi_lo,offset_hi_hi_lo_lo_hi,offset_hi_hi_lo_lo_lo,
    offset_hi_lo}; // @[Cat.scala 30:58]
  wire [33:0] offset = {offset_hi,offset_lo}; // @[Cat.scala 30:58]
  wire [27:0] set = offset[33:6]; // @[Parameters.scala 228:22]
  wire [9:0] set_1 = set[9:0]; // @[Parameters.scala 230:28]
  wire  _T = c_io_deq_ready & c_io_deq_valid; // @[Decoupled.scala 40:37]
  wire [12:0] _beats1_decode_T_1 = 13'h3f << c_io_deq_bits_size; // @[package.scala 234:77]
  wire [5:0] _beats1_decode_T_3 = ~_beats1_decode_T_1[5:0]; // @[package.scala 234:46]
  wire [1:0] beats1_decode = _beats1_decode_T_3[5:4]; // @[Edges.scala 219:59]
  wire  beats1_opdata = c_io_deq_bits_opcode[0]; // @[Edges.scala 101:36]
  wire [1:0] beats1 = beats1_opdata ? beats1_decode : 2'h0; // @[Edges.scala 220:14]
  reg [1:0] counter; // @[Edges.scala 228:27]
  wire [1:0] counter1 = counter - 2'h1; // @[Edges.scala 229:28]
  wire  first = counter == 2'h0; // @[Edges.scala 230:25]
  wire  last = counter == 2'h1 | beats1 == 2'h0; // @[Edges.scala 231:37]
  wire [1:0] _count_T = ~counter1; // @[Edges.scala 233:27]
  wire [1:0] beat = beats1 & _count_T; // @[Edges.scala 233:25]
  wire  raw_resp = c_io_deq_bits_opcode == 3'h4 | c_io_deq_bits_opcode == 3'h5; // @[SinkC.scala 76:58]
  reg  resp_r; // @[Reg.scala 15:16]
  wire  _GEN_1 = c_io_deq_valid ? raw_resp : resp_r; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  reg [9:0] io_set_r; // @[Reg.scala 15:16]
  wire  bs_adr_ready = io_bs_adr_q_io_enq_ready; // @[SinkC.scala 92:22 Decoupled.scala 299:17]
  wire  bs_adr_valid = _GEN_1 & (~first | c_io_deq_valid & beats1_opdata); // @[SinkC.scala 96:30]
  wire  _io_bs_dat_data_T = bs_adr_ready & bs_adr_valid; // @[Decoupled.scala 40:37]
  reg [127:0] io_bs_dat_data_r; // @[Reg.scala 15:16]
  reg  io_bs_dat_poison_r; // @[Reg.scala 15:16]
  wire [1:0] _GEN_9 = {{1'd0}, bs_adr_ready}; // @[SinkC.scala 100:59]
  wire [1:0] _bs_adr_bits_beat_T_1 = beat + _GEN_9; // @[SinkC.scala 100:59]
  reg [1:0] bs_adr_bits_beat_r; // @[Reg.scala 15:16]
  wire  _io_resp_valid_T_4 = ~beats1_opdata | bs_adr_ready; // @[SinkC.scala 104:70]
  reg [1:0] lists; // @[SinkC.scala 113:24]
  wire  _T_21 = ~_GEN_1 & c_io_deq_valid; // @[SinkC.scala 135:17]
  wire  _T_22 = ~_GEN_1 & c_io_deq_valid & first; // @[SinkC.scala 135:28]
  wire  req_block = first & ~io_req_ready; // @[SinkC.scala 123:27]
  wire  _T_24 = ~req_block; // @[SinkC.scala 135:51]
  wire  buf_block = beats1_opdata & ~putbuffer_io_push_ready; // @[SinkC.scala 124:29]
  wire  _T_26 = ~buf_block; // @[SinkC.scala 135:65]
  wire [1:0] _freeOH_T = ~lists; // @[SinkC.scala 120:27]
  wire [2:0] _freeOH_T_1 = {_freeOH_T, 1'h0}; // @[package.scala 244:48]
  wire [1:0] _freeOH_T_3 = _freeOH_T | _freeOH_T_1[1:0]; // @[package.scala 244:43]
  wire [2:0] _freeOH_T_5 = {_freeOH_T_3, 1'h0}; // @[SinkC.scala 120:35]
  wire [2:0] _freeOH_T_6 = ~_freeOH_T_5; // @[SinkC.scala 120:18]
  wire [2:0] _GEN_10 = {{1'd0}, _freeOH_T}; // @[SinkC.scala 120:41]
  wire [2:0] freeOH = _freeOH_T_6 & _GEN_10; // @[SinkC.scala 120:41]
  wire [2:0] _GEN_6 = ~_GEN_1 & c_io_deq_valid & first & beats1_opdata & ~req_block & ~buf_block ? freeOH : 3'h0; // @[SinkC.scala 135:77 SinkC.scala 135:89]
  wire [1:0] lists_set = _GEN_6[1:0];
  wire [1:0] _lists_T = lists | lists_set; // @[SinkC.scala 117:21]
  wire  _T_28 = io_rel_pop_ready & io_rel_pop_valid; // @[Decoupled.scala 40:37]
  wire  lists_clr_shiftAmount = io_rel_pop_bits_index[0]; // @[OneHot.scala 64:49]
  wire [1:0] _lists_clr_T = 2'h1 << lists_clr_shiftAmount; // @[OneHot.scala 65:12]
  wire [1:0] lists_clr = _T_28 & io_rel_pop_bits_last ? _lists_clr_T : 2'h0; // @[SinkC.scala 161:54 SinkC.scala 162:17]
  wire [1:0] _lists_T_1 = ~lists_clr; // @[SinkC.scala 117:36]
  wire  free = ~(&lists); // @[SinkC.scala 119:16]
  wire  freeIdx_hi = freeOH[2]; // @[OneHot.scala 30:18]
  wire [1:0] freeIdx_lo = freeOH[1:0]; // @[OneHot.scala 31:18]
  wire  freeIdx_hi_1 = |freeIdx_hi; // @[OneHot.scala 32:14]
  wire [1:0] _GEN_11 = {{1'd0}, freeIdx_hi}; // @[OneHot.scala 32:28]
  wire [1:0] _freeIdx_T = _GEN_11 | freeIdx_lo; // @[OneHot.scala 32:28]
  wire  freeIdx_lo_1 = _freeIdx_T[1]; // @[CircuitMath.scala 30:8]
  wire [1:0] freeIdx = {freeIdx_hi_1,freeIdx_lo_1}; // @[Cat.scala 30:58]
  wire  set_block = beats1_opdata & first & ~free; // @[SinkC.scala 125:38]
  wire  _c_io_deq_ready_T_5 = ~set_block; // @[SinkC.scala 131:84]
  reg [1:0] put_r; // @[Reg.scala 15:16]
  wire [1:0] _GEN_7 = first ? freeIdx : put_r; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire [1:0] _io_rel_pop_ready_T = putbuffer_io_valid >> io_rel_pop_bits_index; // @[SinkC.scala 158:43]
  RHEA__Queue_123 c ( // @[Decoupled.scala 296:21]
    .rf_reset(c_rf_reset),
    .clock(c_clock),
    .reset(c_reset),
    .io_enq_ready(c_io_enq_ready),
    .io_enq_valid(c_io_enq_valid),
    .io_enq_bits_opcode(c_io_enq_bits_opcode),
    .io_enq_bits_param(c_io_enq_bits_param),
    .io_enq_bits_size(c_io_enq_bits_size),
    .io_enq_bits_source(c_io_enq_bits_source),
    .io_enq_bits_address(c_io_enq_bits_address),
    .io_enq_bits_data(c_io_enq_bits_data),
    .io_enq_bits_corrupt(c_io_enq_bits_corrupt),
    .io_deq_ready(c_io_deq_ready),
    .io_deq_valid(c_io_deq_valid),
    .io_deq_bits_opcode(c_io_deq_bits_opcode),
    .io_deq_bits_param(c_io_deq_bits_param),
    .io_deq_bits_size(c_io_deq_bits_size),
    .io_deq_bits_source(c_io_deq_bits_source),
    .io_deq_bits_address(c_io_deq_bits_address),
    .io_deq_bits_data(c_io_deq_bits_data),
    .io_deq_bits_corrupt(c_io_deq_bits_corrupt)
  );
  RHEA__Queue_124 io_bs_adr_q ( // @[Decoupled.scala 296:21]
    .rf_reset(io_bs_adr_q_rf_reset),
    .clock(io_bs_adr_q_clock),
    .reset(io_bs_adr_q_reset),
    .io_enq_ready(io_bs_adr_q_io_enq_ready),
    .io_enq_valid(io_bs_adr_q_io_enq_valid),
    .io_enq_bits_noop(io_bs_adr_q_io_enq_bits_noop),
    .io_enq_bits_way(io_bs_adr_q_io_enq_bits_way),
    .io_enq_bits_set(io_bs_adr_q_io_enq_bits_set),
    .io_enq_bits_beat(io_bs_adr_q_io_enq_bits_beat),
    .io_deq_ready(io_bs_adr_q_io_deq_ready),
    .io_deq_valid(io_bs_adr_q_io_deq_valid),
    .io_deq_bits_noop(io_bs_adr_q_io_deq_bits_noop),
    .io_deq_bits_way(io_bs_adr_q_io_deq_bits_way),
    .io_deq_bits_set(io_bs_adr_q_io_deq_bits_set),
    .io_deq_bits_beat(io_bs_adr_q_io_deq_bits_beat)
  );
  RHEA__ListBuffer_1 putbuffer ( // @[SinkC.scala 112:27]
    .rf_reset(putbuffer_rf_reset),
    .clock(putbuffer_clock),
    .reset(putbuffer_reset),
    .io_push_ready(putbuffer_io_push_ready),
    .io_push_valid(putbuffer_io_push_valid),
    .io_push_bits_index(putbuffer_io_push_bits_index),
    .io_push_bits_data_data(putbuffer_io_push_bits_data_data),
    .io_push_bits_data_corrupt(putbuffer_io_push_bits_data_corrupt),
    .io_valid(putbuffer_io_valid),
    .io_pop_valid(putbuffer_io_pop_valid),
    .io_pop_bits(putbuffer_io_pop_bits),
    .io_data_data(putbuffer_io_data_data),
    .io_data_corrupt(putbuffer_io_data_corrupt)
  );
  assign c_rf_reset = rf_reset;
  assign io_bs_adr_q_rf_reset = rf_reset;
  assign putbuffer_rf_reset = rf_reset;
  assign io_req_valid = _T_22 & _T_26 & _c_io_deq_ready_T_5; // @[SinkC.scala 133:61]
  assign io_req_bits_opcode = c_io_deq_bits_opcode; // @[SinkC.scala 141:24]
  assign io_req_bits_param = c_io_deq_bits_param; // @[SinkC.scala 142:24]
  assign io_req_bits_size = c_io_deq_bits_size; // @[SinkC.scala 143:24]
  assign io_req_bits_source = c_io_deq_bits_source; // @[SinkC.scala 144:24]
  assign io_req_bits_tag = set[27:10]; // @[Parameters.scala 229:19]
  assign io_req_bits_offset = offset[5:0]; // @[Parameters.scala 230:50]
  assign io_req_bits_put = {{3'd0}, _GEN_7}; // @[SinkC.scala 137:18]
  assign io_req_bits_set = set[9:0]; // @[Parameters.scala 230:28]
  assign io_resp_valid = _GEN_1 & c_io_deq_valid & (first | last) & (~beats1_opdata | bs_adr_ready); // @[SinkC.scala 104:57]
  assign io_resp_bits_last = counter == 2'h1 | beats1 == 2'h0; // @[Edges.scala 231:37]
  assign io_resp_bits_set = set[9:0]; // @[Parameters.scala 230:28]
  assign io_resp_bits_tag = set[27:10]; // @[Parameters.scala 229:19]
  assign io_resp_bits_source = c_io_deq_bits_source; // @[SinkC.scala 108:25]
  assign io_resp_bits_param = c_io_deq_bits_param; // @[SinkC.scala 109:25]
  assign io_resp_bits_data = c_io_deq_bits_opcode[0]; // @[Edges.scala 101:36]
  assign io_c_ready = c_io_enq_ready; // @[Decoupled.scala 299:17]
  assign io_set = c_io_deq_valid ? set_1 : io_set_r; // @[SinkC.scala 88:18]
  assign io_bs_adr_valid = io_bs_adr_q_io_deq_valid; // @[SinkC.scala 93:15]
  assign io_bs_adr_bits_noop = io_bs_adr_q_io_deq_bits_noop; // @[SinkC.scala 93:15]
  assign io_bs_adr_bits_way = io_bs_adr_q_io_deq_bits_way; // @[SinkC.scala 93:15]
  assign io_bs_adr_bits_set = io_bs_adr_q_io_deq_bits_set; // @[SinkC.scala 93:15]
  assign io_bs_adr_bits_beat = io_bs_adr_q_io_deq_bits_beat; // @[SinkC.scala 93:15]
  assign io_bs_dat_data = io_bs_dat_data_r; // @[SinkC.scala 94:22]
  assign io_bs_dat_poison = io_bs_dat_poison_r; // @[SinkC.scala 95:22]
  assign io_rel_pop_ready = _io_rel_pop_ready_T[0]; // @[SinkC.scala 158:43]
  assign io_rel_beat_data = putbuffer_io_data_data; // @[SinkC.scala 159:17]
  assign io_rel_beat_corrupt = putbuffer_io_data_corrupt; // @[SinkC.scala 159:17]
  assign c_clock = clock;
  assign c_reset = reset;
  assign c_io_enq_valid = io_c_valid; // @[Decoupled.scala 297:22]
  assign c_io_enq_bits_opcode = io_c_bits_opcode; // @[Decoupled.scala 298:21]
  assign c_io_enq_bits_param = io_c_bits_param; // @[Decoupled.scala 298:21]
  assign c_io_enq_bits_size = io_c_bits_size; // @[Decoupled.scala 298:21]
  assign c_io_enq_bits_source = io_c_bits_source; // @[Decoupled.scala 298:21]
  assign c_io_enq_bits_address = io_c_bits_address; // @[Decoupled.scala 298:21]
  assign c_io_enq_bits_data = io_c_bits_data; // @[Decoupled.scala 298:21]
  assign c_io_enq_bits_corrupt = io_c_bits_corrupt; // @[Decoupled.scala 298:21]
  assign c_io_deq_ready = raw_resp ? _io_resp_valid_T_4 : _T_24 & _T_26 & ~set_block; // @[SinkC.scala 131:19]
  assign io_bs_adr_q_clock = clock;
  assign io_bs_adr_q_reset = reset;
  assign io_bs_adr_q_io_enq_valid = _GEN_1 & (~first | c_io_deq_valid & beats1_opdata); // @[SinkC.scala 96:30]
  assign io_bs_adr_q_io_enq_bits_noop = ~c_io_deq_valid; // @[SinkC.scala 97:25]
  assign io_bs_adr_q_io_enq_bits_way = io_way; // @[SinkC.scala 92:22 SinkC.scala 98:22]
  assign io_bs_adr_q_io_enq_bits_set = io_set; // @[SinkC.scala 92:22 SinkC.scala 99:22]
  assign io_bs_adr_q_io_enq_bits_beat = c_io_deq_valid ? beat : bs_adr_bits_beat_r; // @[SinkC.scala 100:28]
  assign io_bs_adr_q_io_deq_ready = io_bs_adr_ready; // @[SinkC.scala 93:15]
  assign putbuffer_clock = clock;
  assign putbuffer_reset = reset;
  assign putbuffer_io_push_valid = _T_21 & beats1_opdata & _T_24 & _c_io_deq_ready_T_5; // @[SinkC.scala 134:74]
  assign putbuffer_io_push_bits_index = _GEN_7[0]; // @[SinkC.scala 151:34]
  assign putbuffer_io_push_bits_data_data = c_io_deq_bits_data; // @[SinkC.scala 152:41]
  assign putbuffer_io_push_bits_data_corrupt = c_io_deq_bits_corrupt; // @[SinkC.scala 153:41]
  assign putbuffer_io_pop_valid = io_rel_pop_ready & io_rel_pop_valid; // @[Decoupled.scala 40:37]
  assign putbuffer_io_pop_bits = io_rel_pop_bits_index[0]; // @[SinkC.scala 156:27]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      counter <= 2'h0;
    end else if (_T) begin
      if (first) begin
        if (beats1_opdata) begin
          counter <= beats1_decode;
        end else begin
          counter <= 2'h0;
        end
      end else begin
        counter <= counter1;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      resp_r <= 1'h0;
    end else if (c_io_deq_valid) begin
      resp_r <= raw_resp;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      io_set_r <= 10'h0;
    end else if (c_io_deq_valid) begin
      io_set_r <= set_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      io_bs_dat_data_r <= 128'h0;
    end else if (_io_bs_dat_data_T) begin
      io_bs_dat_data_r <= c_io_deq_bits_data;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      io_bs_dat_poison_r <= 1'h0;
    end else if (_io_bs_dat_data_T) begin
      io_bs_dat_poison_r <= c_io_deq_bits_corrupt;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bs_adr_bits_beat_r <= 2'h0;
    end else if (c_io_deq_valid) begin
      bs_adr_bits_beat_r <= _bs_adr_bits_beat_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      lists <= 2'h0;
    end else begin
      lists <= _lists_T & _lists_T_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      put_r <= 2'h0;
    end else if (first) begin
      put_r <= freeIdx;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  counter = _RAND_0[1:0];
  _RAND_1 = {1{`RANDOM}};
  resp_r = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  io_set_r = _RAND_2[9:0];
  _RAND_3 = {4{`RANDOM}};
  io_bs_dat_data_r = _RAND_3[127:0];
  _RAND_4 = {1{`RANDOM}};
  io_bs_dat_poison_r = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  bs_adr_bits_beat_r = _RAND_5[1:0];
  _RAND_6 = {1{`RANDOM}};
  lists = _RAND_6[1:0];
  _RAND_7 = {1{`RANDOM}};
  put_r = _RAND_7[1:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    counter = 2'h0;
  end
  if (rf_reset) begin
    resp_r = 1'h0;
  end
  if (rf_reset) begin
    io_set_r = 10'h0;
  end
  if (rf_reset) begin
    io_bs_dat_data_r = 128'h0;
  end
  if (rf_reset) begin
    io_bs_dat_poison_r = 1'h0;
  end
  if (rf_reset) begin
    bs_adr_bits_beat_r = 2'h0;
  end
  if (reset) begin
    lists = 2'h0;
  end
  if (rf_reset) begin
    put_r = 2'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
