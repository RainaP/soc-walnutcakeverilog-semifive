//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_RocketTile_assert(
  input   clock,
  input   reset,
  input   _T_35,
  input   _T_37
);
  reg  _T_41; // @[Interrupts.scala 108:31]
  reg [31:0] _RAND_0;
  wire  _T_43; // @[Interrupts.scala 109:27]
  wire  _T_44; // @[Interrupts.scala 109:14]
  wire  _T_46; // @[Interrupts.scala 109:13]
  wire  _T_47; // @[Interrupts.scala 109:13]
  assign _T_43 = _T_41 & _T_37; // @[Interrupts.scala 109:27]
  assign _T_44 = ~_T_43; // @[Interrupts.scala 109:14]
  assign _T_46 = _T_44 | reset; // @[Interrupts.scala 109:13]
  assign _T_47 = ~_T_46; // @[Interrupts.scala 109:13]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_41 = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_41 <= 1'h0;
    end else begin
      _T_41 <= _T_35;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_47) begin
          $fwrite(32'h80000002,"Assertion Failed: CEASE line can not glitch once raised\n    at Interrupts.scala:109 assert(!(prev_cease & !cease), \"CEASE line can not glitch once raised\")\n"); // @[Interrupts.scala 109:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_47) begin
          $fatal; // @[Interrupts.scala 109:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
