module SemiAPB3Decoder (/*AUTOARG*/
   // Outputs
   APB3__BUS__PRDATA, APB3__BUS__PREADY, APB3__BUS__PSEL0,
   APB3__BUS__PSEL1, APB3__BUS__PSEL2, APB3__BUS__PSEL3,
   APB3__BUS__PSEL4, APB3__BUS__PSEL5, APB3__BUS__PSEL6,
   APB3__BUS__PSEL7, APB3__BUS__PSEL8, APB3__BUS__PSEL9,
   APB3__BUS__PSEL10, APB3__BUS__PSEL11, APB3__BUS__PSEL12,
   APB3__BUS__PSEL13, APB3__BUS__PSEL14, APB3__BUS__PSEL15,
   // Inputs
   CLK__BUS, RSTN__BUS, APB3__BUS__PADDR, APB3__BUS__PSEL,
   APB3__BUS__PRDATA0, APB3__BUS__PRDATA1, APB3__BUS__PRDATA2,
   APB3__BUS__PRDATA3, APB3__BUS__PRDATA4, APB3__BUS__PRDATA5,
   APB3__BUS__PRDATA6, APB3__BUS__PRDATA7, APB3__BUS__PRDATA8,
   APB3__BUS__PRDATA9, APB3__BUS__PRDATA10, APB3__BUS__PRDATA11,
   APB3__BUS__PRDATA12, APB3__BUS__PRDATA13, APB3__BUS__PRDATA14,
   APB3__BUS__PRDATA15, APB3__BUS__PREADY0, APB3__BUS__PREADY1,
   APB3__BUS__PREADY2, APB3__BUS__PREADY3, APB3__BUS__PREADY4,
   APB3__BUS__PREADY5, APB3__BUS__PREADY6, APB3__BUS__PREADY7,
   APB3__BUS__PREADY8, APB3__BUS__PREADY9, APB3__BUS__PREADY10,
   APB3__BUS__PREADY11, APB3__BUS__PREADY12, APB3__BUS__PREADY13,
   APB3__BUS__PREADY14, APB3__BUS__PREADY15
   );

    parameter GRANULE_KB = 4;
    input  wire        CLK__BUS;
    input  wire        RSTN__BUS;
    input  wire [31:0] APB3__BUS__PADDR;
    input  wire        APB3__BUS__PSEL;
    output wire [31:0] APB3__BUS__PRDATA;
    output wire        APB3__BUS__PREADY;
    output wire        APB3__BUS__PSEL0;
    output wire        APB3__BUS__PSEL1;
    output wire        APB3__BUS__PSEL2;
    output wire        APB3__BUS__PSEL3;
    output wire        APB3__BUS__PSEL4;
    output wire        APB3__BUS__PSEL5;
    output wire        APB3__BUS__PSEL6;
    output wire        APB3__BUS__PSEL7;
    output wire        APB3__BUS__PSEL8;
    output wire        APB3__BUS__PSEL9;
    output wire        APB3__BUS__PSEL10;
    output wire        APB3__BUS__PSEL11;
    output wire        APB3__BUS__PSEL12;
    output wire        APB3__BUS__PSEL13;
    output wire        APB3__BUS__PSEL14;
    output wire        APB3__BUS__PSEL15;
    input  wire [31:0] APB3__BUS__PRDATA0;
    input  wire [31:0] APB3__BUS__PRDATA1;
    input  wire [31:0] APB3__BUS__PRDATA2;
    input  wire [31:0] APB3__BUS__PRDATA3;
    input  wire [31:0] APB3__BUS__PRDATA4;
    input  wire [31:0] APB3__BUS__PRDATA5;
    input  wire [31:0] APB3__BUS__PRDATA6;
    input  wire [31:0] APB3__BUS__PRDATA7;
    input  wire [31:0] APB3__BUS__PRDATA8;
    input  wire [31:0] APB3__BUS__PRDATA9;
    input  wire [31:0] APB3__BUS__PRDATA10;
    input  wire [31:0] APB3__BUS__PRDATA11;
    input  wire [31:0] APB3__BUS__PRDATA12;
    input  wire [31:0] APB3__BUS__PRDATA13;
    input  wire [31:0] APB3__BUS__PRDATA14;
    input  wire [31:0] APB3__BUS__PRDATA15;
    input  wire        APB3__BUS__PREADY0;
    input  wire        APB3__BUS__PREADY1;
    input  wire        APB3__BUS__PREADY2;
    input  wire        APB3__BUS__PREADY3;
    input  wire        APB3__BUS__PREADY4;
    input  wire        APB3__BUS__PREADY5;
    input  wire        APB3__BUS__PREADY6;
    input  wire        APB3__BUS__PREADY7;
    input  wire        APB3__BUS__PREADY8;
    input  wire        APB3__BUS__PREADY9;
    input  wire        APB3__BUS__PREADY10;
    input  wire        APB3__BUS__PREADY11;
    input  wire        APB3__BUS__PREADY12;
    input  wire        APB3__BUS__PREADY13;
    input  wire        APB3__BUS__PREADY14;
    input  wire        APB3__BUS__PREADY15;

    reg [3:0] sel;
    generate
        if (GRANULE_KB==4) begin
            always @(*) begin
                sel = 0;
                case (APB3__BUS__PADDR[15:12])
                    4'd0 : sel = 0;
                    4'd1 : sel = 1;
                    4'd2 : sel = 2;
                    4'd3 : sel = 3;
                    4'd4 : sel = 4;
                    4'd5 : sel = 5;
                    4'd6 : sel = 6;
                    4'd7 : sel = 7;
                    4'd8 : sel = 8;
                    4'd9 : sel = 9;
                    4'd10 : sel = 10;
                    4'd11 : sel = 11;
                    4'd12 : sel = 12;
                    4'd13 : sel = 13;
                    4'd14 : sel = 14;
                    4'd15 : sel = 15;
                endcase
            end
        end
        else if (GRANULE_KB==64) begin
            always @(*) begin
                sel = 0;
                case (APB3__BUS__PADDR[19:16])
                    4'd0 : sel = 0;
                    4'd1 : sel = 1;
                    4'd2 : sel = 2;
                    4'd3 : sel = 3;
                    4'd4 : sel = 4;
                    4'd5 : sel = 5;
                    4'd6 : sel = 6;
                    4'd7 : sel = 7;
                    4'd8 : sel = 8;
                    4'd9 : sel = 9;
                    4'd10 : sel = 10;
                    4'd11 : sel = 11;
                    4'd12 : sel = 12;
                    4'd13 : sel = 13;
                    4'd14 : sel = 14;
                    4'd15 : sel = 15;
                endcase
            end
        end
    endgenerate
    assign APB3__BUS__PSEL0 = APB3__BUS__PSEL & (sel==0);
    assign APB3__BUS__PSEL1 = APB3__BUS__PSEL & (sel==1);
    assign APB3__BUS__PSEL2 = APB3__BUS__PSEL & (sel==2);
    assign APB3__BUS__PSEL3 = APB3__BUS__PSEL & (sel==3);
    assign APB3__BUS__PSEL4 = APB3__BUS__PSEL & (sel==4);
    assign APB3__BUS__PSEL5 = APB3__BUS__PSEL & (sel==5);
    assign APB3__BUS__PSEL6 = APB3__BUS__PSEL & (sel==6);
    assign APB3__BUS__PSEL7 = APB3__BUS__PSEL & (sel==7);
    assign APB3__BUS__PSEL8 = APB3__BUS__PSEL & (sel==8);
    assign APB3__BUS__PSEL9 = APB3__BUS__PSEL & (sel==9);
    assign APB3__BUS__PSEL10 = APB3__BUS__PSEL & (sel==10);
    assign APB3__BUS__PSEL11 = APB3__BUS__PSEL & (sel==11);
    assign APB3__BUS__PSEL12 = APB3__BUS__PSEL & (sel==12);
    assign APB3__BUS__PSEL13 = APB3__BUS__PSEL & (sel==13);
    assign APB3__BUS__PSEL14 = APB3__BUS__PSEL & (sel==14);
    assign APB3__BUS__PSEL15 = APB3__BUS__PSEL & (sel==15);
    assign APB3__BUS__PRDATA = (sel==0) ? APB3__BUS__PRDATA0
                             : (sel==1) ? APB3__BUS__PRDATA1
                             : (sel==2) ? APB3__BUS__PRDATA2
                             : (sel==3) ? APB3__BUS__PRDATA3
                             : (sel==4) ? APB3__BUS__PRDATA4
                             : (sel==5) ? APB3__BUS__PRDATA5
                             : (sel==6) ? APB3__BUS__PRDATA6
                             : (sel==7) ? APB3__BUS__PRDATA7
                             : (sel==8) ? APB3__BUS__PRDATA8
                             : (sel==9) ? APB3__BUS__PRDATA9
                             : (sel==10) ? APB3__BUS__PRDATA10
                             : (sel==11) ? APB3__BUS__PRDATA11
                             : (sel==12) ? APB3__BUS__PRDATA12
                             : (sel==13) ? APB3__BUS__PRDATA13
                             : (sel==14) ? APB3__BUS__PRDATA14
                             : (sel==15) ? APB3__BUS__PRDATA15
                             :             APB3__BUS__PRDATA0;
    assign APB3__BUS__PREADY = (sel==0) ? APB3__BUS__PREADY0
                             : (sel==1) ? APB3__BUS__PREADY1
                             : (sel==2) ? APB3__BUS__PREADY2
                             : (sel==3) ? APB3__BUS__PREADY3
                             : (sel==4) ? APB3__BUS__PREADY4
                             : (sel==5) ? APB3__BUS__PREADY5
                             : (sel==6) ? APB3__BUS__PREADY6
                             : (sel==7) ? APB3__BUS__PREADY7
                             : (sel==8) ? APB3__BUS__PREADY8
                             : (sel==9) ? APB3__BUS__PREADY9
                             : (sel==10) ? APB3__BUS__PREADY10
                             : (sel==11) ? APB3__BUS__PREADY11
                             : (sel==12) ? APB3__BUS__PREADY12
                             : (sel==13) ? APB3__BUS__PREADY13
                             : (sel==14) ? APB3__BUS__PREADY14
                             : (sel==15) ? APB3__BUS__PREADY15
                             :             APB3__BUS__PREADY0;
endmodule
