//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__SourceD(
  input          rf_reset,
  input          clock,
  input          reset,
  output         io_req_ready,
  input          io_req_valid,
  input          io_req_bits_prio_0,
  input          io_req_bits_prio_2,
  input  [2:0]   io_req_bits_opcode,
  input  [2:0]   io_req_bits_param,
  input  [2:0]   io_req_bits_size,
  input  [6:0]   io_req_bits_source,
  input  [5:0]   io_req_bits_offset,
  input  [4:0]   io_req_bits_put,
  input  [9:0]   io_req_bits_set,
  input  [2:0]   io_req_bits_sink,
  input  [2:0]   io_req_bits_way,
  input          io_req_bits_bad,
  input          io_req_bits_user_hit_cache,
  input          io_d_ready,
  output         io_d_valid,
  output [2:0]   io_d_bits_opcode,
  output [1:0]   io_d_bits_param,
  output [2:0]   io_d_bits_size,
  output [6:0]   io_d_bits_source,
  output [2:0]   io_d_bits_sink,
  output         io_d_bits_denied,
  output [127:0] io_d_bits_data,
  output         io_d_bits_corrupt,
  input          io_pb_pop_ready,
  output         io_pb_pop_valid,
  output [4:0]   io_pb_pop_bits_index,
  output         io_pb_pop_bits_last,
  input  [127:0] io_pb_beat_data,
  input  [15:0]  io_pb_beat_mask,
  input          io_pb_beat_corrupt,
  input          io_rel_pop_ready,
  output         io_rel_pop_valid,
  output [4:0]   io_rel_pop_bits_index,
  output         io_rel_pop_bits_last,
  input  [127:0] io_rel_beat_data,
  input          io_rel_beat_corrupt,
  input          io_bs_radr_ready,
  output         io_bs_radr_valid,
  output [2:0]   io_bs_radr_bits_way,
  output [9:0]   io_bs_radr_bits_set,
  output [1:0]   io_bs_radr_bits_beat,
  output [1:0]   io_bs_radr_bits_mask,
  input  [127:0] io_bs_rdat_data,
  input          io_bs_wadr_ready,
  output         io_bs_wadr_valid,
  output [2:0]   io_bs_wadr_bits_way,
  output [9:0]   io_bs_wadr_bits_set,
  output [1:0]   io_bs_wadr_bits_beat,
  output [1:0]   io_bs_wadr_bits_mask,
  output [127:0] io_bs_wdat_data,
  output         io_bs_wdat_poison,
  input  [9:0]   io_evict_req_set,
  input  [2:0]   io_evict_req_way,
  output         io_evict_safe,
  input  [9:0]   io_grant_req_set,
  input  [2:0]   io_grant_req_way,
  output         io_grant_safe,
  output         io_corrected_valid,
  output         io_uncorrected_valid
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [127:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [31:0] _RAND_59;
  reg [31:0] _RAND_60;
  reg [31:0] _RAND_61;
  reg [31:0] _RAND_62;
  reg [31:0] _RAND_63;
  reg [31:0] _RAND_64;
  reg [31:0] _RAND_65;
  reg [127:0] _RAND_66;
  reg [31:0] _RAND_67;
  reg [31:0] _RAND_68;
  reg [31:0] _RAND_69;
  reg [31:0] _RAND_70;
  reg [31:0] _RAND_71;
  reg [127:0] _RAND_72;
  reg [31:0] _RAND_73;
  reg [31:0] _RAND_74;
  reg [31:0] _RAND_75;
  reg [31:0] _RAND_76;
  reg [31:0] _RAND_77;
  reg [31:0] _RAND_78;
  reg [31:0] _RAND_79;
  reg [31:0] _RAND_80;
  reg [31:0] _RAND_81;
  reg [127:0] _RAND_82;
  reg [31:0] _RAND_83;
  reg [127:0] _RAND_84;
  reg [31:0] _RAND_85;
  reg [31:0] _RAND_86;
  reg [31:0] _RAND_87;
  reg [31:0] _RAND_88;
  reg [31:0] _RAND_89;
  reg [127:0] _RAND_90;
  reg [31:0] _RAND_91;
  reg [31:0] _RAND_92;
  reg [31:0] _RAND_93;
  reg [31:0] _RAND_94;
  reg [31:0] _RAND_95;
  reg [31:0] _RAND_96;
  reg [127:0] _RAND_97;
  reg [31:0] _RAND_98;
  reg [31:0] _RAND_99;
  reg [31:0] _RAND_100;
  reg [127:0] _RAND_101;
  reg [31:0] _RAND_102;
`endif // RANDOMIZE_REG_INIT
  wire  queue_rf_reset; // @[SourceD.scala 131:21]
  wire  queue_clock; // @[SourceD.scala 131:21]
  wire  queue_reset; // @[SourceD.scala 131:21]
  wire  queue_io_enq_ready; // @[SourceD.scala 131:21]
  wire  queue_io_enq_valid; // @[SourceD.scala 131:21]
  wire [127:0] queue_io_enq_bits_data; // @[SourceD.scala 131:21]
  wire  queue_io_deq_ready; // @[SourceD.scala 131:21]
  wire  queue_io_deq_valid; // @[SourceD.scala 131:21]
  wire [127:0] queue_io_deq_bits_data; // @[SourceD.scala 131:21]
  wire  atomics_io_write; // @[SourceD.scala 308:23]
  wire [2:0] atomics_io_a_opcode; // @[SourceD.scala 308:23]
  wire [2:0] atomics_io_a_param; // @[SourceD.scala 308:23]
  wire [15:0] atomics_io_a_mask; // @[SourceD.scala 308:23]
  wire [127:0] atomics_io_a_data; // @[SourceD.scala 308:23]
  wire [127:0] atomics_io_data_in; // @[SourceD.scala 308:23]
  wire [127:0] atomics_io_data_out; // @[SourceD.scala 308:23]
  reg  busy; // @[SourceD.scala 94:21]
  reg  s1_block_r; // @[SourceD.scala 95:27]
  reg [1:0] s1_counter; // @[SourceD.scala 96:27]
  wire  _s1_req_reg_T = ~busy; // @[SourceD.scala 97:43]
  wire  _s1_req_reg_T_1 = _s1_req_reg_T & io_req_valid; // @[SourceD.scala 97:49]
  reg  s1_req_reg_prio_0; // @[Reg.scala 15:16]
  reg  s1_req_reg_prio_2; // @[Reg.scala 15:16]
  reg [2:0] s1_req_reg_opcode; // @[Reg.scala 15:16]
  reg [2:0] s1_req_reg_param; // @[Reg.scala 15:16]
  reg [2:0] s1_req_reg_size; // @[Reg.scala 15:16]
  reg [6:0] s1_req_reg_source; // @[Reg.scala 15:16]
  reg [5:0] s1_req_reg_offset; // @[Reg.scala 15:16]
  reg [4:0] s1_req_reg_put; // @[Reg.scala 15:16]
  reg [9:0] s1_req_reg_set; // @[Reg.scala 15:16]
  reg [2:0] s1_req_reg_sink; // @[Reg.scala 15:16]
  reg [2:0] s1_req_reg_way; // @[Reg.scala 15:16]
  reg  s1_req_reg_bad; // @[Reg.scala 15:16]
  wire  s1_req_prio_0 = _s1_req_reg_T ? io_req_bits_prio_0 : s1_req_reg_prio_0; // @[SourceD.scala 98:19]
  wire [2:0] s1_req_opcode = _s1_req_reg_T ? io_req_bits_opcode : s1_req_reg_opcode; // @[SourceD.scala 98:19]
  wire [2:0] s1_req_param = _s1_req_reg_T ? io_req_bits_param : s1_req_reg_param; // @[SourceD.scala 98:19]
  wire [2:0] s1_req_size = _s1_req_reg_T ? io_req_bits_size : s1_req_reg_size; // @[SourceD.scala 98:19]
  wire [5:0] s1_req_offset = _s1_req_reg_T ? io_req_bits_offset : s1_req_reg_offset; // @[SourceD.scala 98:19]
  wire [9:0] s1_req_set = _s1_req_reg_T ? io_req_bits_set : s1_req_reg_set; // @[SourceD.scala 98:19]
  wire [2:0] s1_req_way = _s1_req_reg_T ? io_req_bits_way : s1_req_reg_way; // @[SourceD.scala 98:19]
  wire  _s1_latch_bypass_T = busy | io_req_valid; // @[SourceD.scala 100:40]
  reg  s2_full; // @[SourceD.scala 158:24]
  wire  _s2_ready_T = ~s2_full; // @[SourceD.scala 195:15]
  reg  s3_full; // @[SourceD.scala 201:24]
  wire  _s3_ready_T = ~s3_full; // @[SourceD.scala 286:15]
  reg  s3_retires; // @[Reg.scala 15:16]
  reg  s4_full; // @[SourceD.scala 292:24]
  wire  _s4_ready_T_1 = ~s4_full; // @[SourceD.scala 344:30]
  reg  s4_need_bs; // @[Reg.scala 15:16]
  wire  _s4_ready_T_4 = ~s4_need_bs; // @[SourceD.scala 344:62]
  wire  s4_ready = ~s3_retires | ~s4_full | io_bs_wadr_ready | ~s4_need_bs; // @[SourceD.scala 344:59]
  reg  s3_valid_d; // @[SourceD.scala 202:27]
  wire  _s3_ready_T_2 = ~s3_valid_d | io_d_ready; // @[SourceD.scala 286:53]
  wire  s3_ready = _s3_ready_T | s4_ready & (~s3_valid_d | io_d_ready); // @[SourceD.scala 286:24]
  reg  s2_valid_pb; // @[SourceD.scala 159:28]
  reg  s2_req_prio_0; // @[Reg.scala 15:16]
  wire  pb_ready = s2_req_prio_0 ? io_pb_pop_ready : io_rel_pop_ready; // @[SourceD.scala 186:21]
  wire  _s2_ready_T_2 = ~s2_valid_pb | pb_ready; // @[SourceD.scala 195:54]
  wire  s2_ready = ~s2_full | s3_ready & (~s2_valid_pb | pb_ready); // @[SourceD.scala 195:24]
  reg  s1_latch_bypass; // @[SourceD.scala 100:32]
  reg [1:0] s1_bypass_r; // @[Reg.scala 15:16]
  reg [9:0] s2_req_set; // @[Reg.scala 15:16]
  reg [2:0] s2_req_way; // @[Reg.scala 15:16]
  reg [1:0] s2_beat; // @[Reg.scala 15:16]
  wire [1:0] s1_beat = s1_req_offset[5:4] | s1_counter; // @[SourceD.scala 113:56]
  reg  s2_retires; // @[Reg.scala 15:16]
  wire  s1_2_match = s2_req_set == s1_req_set & s2_req_way == s1_req_way & s2_beat == s1_beat & s2_full & s2_retires; // @[SourceD.scala 422:110]
  reg [2:0] s2_req_size; // @[Reg.scala 15:16]
  wire  _s1_2_bypass_T = s2_req_size >= 3'h4; // @[Misc.scala 211:21]
  reg [5:0] s2_req_offset; // @[Reg.scala 15:16]
  wire  s1_2_bypass_bit = s2_req_offset[3]; // @[Misc.scala 215:26]
  wire  s1_2_bypass_hi = _s1_2_bypass_T | s1_2_bypass_bit; // @[Misc.scala 220:29]
  wire  s1_2_bypass_nbit = ~s1_2_bypass_bit; // @[Misc.scala 216:20]
  wire  s1_2_bypass_lo = _s1_2_bypass_T | s1_2_bypass_nbit; // @[Misc.scala 220:29]
  wire [1:0] _s1_2_bypass_T_1 = {s1_2_bypass_hi,s1_2_bypass_lo}; // @[Cat.scala 30:58]
  wire [1:0] s1_2_bypass = s1_2_match ? _s1_2_bypass_T_1 : 2'h0; // @[SourceD.scala 434:24]
  reg [9:0] s3_req_set; // @[Reg.scala 15:16]
  reg [2:0] s3_req_way; // @[Reg.scala 15:16]
  reg [1:0] s3_beat; // @[Reg.scala 15:16]
  wire  s1_3_match = s3_req_set == s1_req_set & s3_req_way == s1_req_way & s3_beat == s1_beat & s3_full & s3_retires; // @[SourceD.scala 423:110]
  reg [2:0] s3_req_size; // @[Reg.scala 15:16]
  wire  _s1_3_bypass_T = s3_req_size >= 3'h4; // @[Misc.scala 211:21]
  reg [5:0] s3_req_offset; // @[Reg.scala 15:16]
  wire  s1_3_bypass_bit = s3_req_offset[3]; // @[Misc.scala 215:26]
  wire  s1_3_bypass_hi = _s1_3_bypass_T | s1_3_bypass_bit; // @[Misc.scala 220:29]
  wire  s1_3_bypass_nbit = ~s1_3_bypass_bit; // @[Misc.scala 216:20]
  wire  s1_3_bypass_lo = _s1_3_bypass_T | s1_3_bypass_nbit; // @[Misc.scala 220:29]
  wire [1:0] _s1_3_bypass_T_1 = {s1_3_bypass_hi,s1_3_bypass_lo}; // @[Cat.scala 30:58]
  wire [1:0] s1_3_bypass = s1_3_match ? _s1_3_bypass_T_1 : 2'h0; // @[SourceD.scala 435:24]
  wire [1:0] _s1_x_bypass_T = s1_2_bypass | s1_3_bypass; // @[SourceD.scala 438:30]
  reg [9:0] s4_req_set; // @[Reg.scala 15:16]
  reg [2:0] s4_req_way; // @[Reg.scala 15:16]
  reg [1:0] s4_beat; // @[Reg.scala 15:16]
  wire  s1_4_match = s4_req_set == s1_req_set & s4_req_way == s1_req_way & s4_beat == s1_beat & s4_full; // @[SourceD.scala 424:99]
  reg [2:0] s4_req_size; // @[Reg.scala 15:16]
  wire  _s1_4_bypass_T = s4_req_size >= 3'h4; // @[Misc.scala 211:21]
  reg [5:0] s4_req_offset; // @[Reg.scala 15:16]
  wire  s1_4_bypass_bit = s4_req_offset[3]; // @[Misc.scala 215:26]
  wire  s1_4_bypass_hi = _s1_4_bypass_T | s1_4_bypass_bit; // @[Misc.scala 220:29]
  wire  s1_4_bypass_nbit = ~s1_4_bypass_bit; // @[Misc.scala 216:20]
  wire  s1_4_bypass_lo = _s1_4_bypass_T | s1_4_bypass_nbit; // @[Misc.scala 220:29]
  wire [1:0] _s1_4_bypass_T_1 = {s1_4_bypass_hi,s1_4_bypass_lo}; // @[Cat.scala 30:58]
  wire [1:0] s1_4_bypass = s1_4_match ? _s1_4_bypass_T_1 : 2'h0; // @[SourceD.scala 436:24]
  wire [1:0] s1_x_bypass = _s1_x_bypass_T | s1_4_bypass; // @[SourceD.scala 438:44]
  wire [1:0] _GEN_18 = s1_latch_bypass ? s1_x_bypass : s1_bypass_r; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire  _s1_mask_T = s1_req_size >= 3'h4; // @[Misc.scala 211:21]
  wire  s1_mask_bit = s1_req_offset[3]; // @[Misc.scala 215:26]
  wire  s1_mask_nbit = ~s1_mask_bit; // @[Misc.scala 216:20]
  wire  s1_mask_lo = _s1_mask_T | s1_mask_nbit; // @[Misc.scala 220:29]
  wire  s1_mask_hi = _s1_mask_T | s1_mask_bit; // @[Misc.scala 220:29]
  wire [1:0] _s1_mask_T_1 = {s1_mask_hi,s1_mask_lo}; // @[Cat.scala 30:58]
  wire [1:0] _s1_mask_T_2 = ~_GEN_18; // @[SourceD.scala 102:78]
  wire [1:0] s1_mask = _s1_mask_T_1 & _s1_mask_T_2; // @[SourceD.scala 102:76]
  wire  _s1_grant_T = s1_req_opcode == 3'h6; // @[SourceD.scala 103:33]
  wire  s1_grant = s1_req_opcode == 3'h6 & s1_req_param == 3'h2 | s1_req_opcode == 3'h7; // @[SourceD.scala 103:76]
  wire  _s1_need_r_T_8 = s1_req_opcode != 3'h0 | s1_req_size < 3'h3; // @[SourceD.scala 105:50]
  wire  s1_need_r = |s1_mask & s1_req_prio_0 & s1_req_opcode != 3'h5 & ~s1_grant & _s1_need_r_T_8; // @[SourceD.scala 104:88]
  wire  s1_valid_r = _s1_latch_bypass_T & s1_need_r & ~s1_block_r; // @[SourceD.scala 106:56]
  wire  s1_need_pb = s1_req_prio_0 ? ~s1_req_opcode[2] : s1_req_opcode[0]; // @[SourceD.scala 107:23]
  wire  s1_single = s1_req_prio_0 ? s1_req_opcode == 3'h5 | s1_grant : _s1_grant_T; // @[SourceD.scala 108:22]
  wire  s1_retires = ~s1_single; // @[SourceD.scala 109:20]
  wire [12:0] _s1_beats1_T_1 = 13'h3f << s1_req_size; // @[package.scala 234:77]
  wire [5:0] _s1_beats1_T_3 = ~_s1_beats1_T_1[5:0]; // @[package.scala 234:46]
  wire [1:0] s1_beats1 = s1_single ? 2'h0 : _s1_beats1_T_3[5:4]; // @[SourceD.scala 112:22]
  wire  s1_last = s1_counter == s1_beats1; // @[SourceD.scala 114:28]
  wire  s1_first = s1_counter == 2'h0; // @[SourceD.scala 115:29]
  wire  _queue_io_enq_valid_T = io_bs_radr_ready & io_bs_radr_valid; // @[Decoupled.scala 40:37]
  reg  queue_io_enq_valid_REG; // @[SourceD.scala 132:40]
  reg  queue_io_enq_valid_REG_1; // @[SourceD.scala 132:32]
  wire  _GEN_19 = _queue_io_enq_valid_T | s1_block_r; // @[SourceD.scala 138:28 SourceD.scala 138:41 SourceD.scala 95:27]
  wire  _GEN_20 = io_req_valid | busy; // @[SourceD.scala 139:23 SourceD.scala 139:30 SourceD.scala 94:21]
  wire  s1_valid = _s1_latch_bypass_T & (~s1_valid_r | io_bs_radr_ready); // @[SourceD.scala 152:38]
  wire  _T_23 = s1_valid & s2_ready; // @[SourceD.scala 140:18]
  wire [1:0] _s1_counter_T_1 = s1_counter + 2'h1; // @[SourceD.scala 141:30]
  reg [1:0] s2_bypass; // @[Reg.scala 15:16]
  reg  s2_req_prio_2; // @[Reg.scala 15:16]
  reg [2:0] s2_req_opcode; // @[Reg.scala 15:16]
  reg [2:0] s2_req_param; // @[Reg.scala 15:16]
  reg [6:0] s2_req_source; // @[Reg.scala 15:16]
  reg [4:0] s2_req_put; // @[Reg.scala 15:16]
  reg [2:0] s2_req_sink; // @[Reg.scala 15:16]
  reg  s2_req_bad; // @[Reg.scala 15:16]
  reg  s2_last; // @[Reg.scala 15:16]
  reg  s2_need_r; // @[Reg.scala 15:16]
  reg  s2_need_pb; // @[Reg.scala 15:16]
  wire  _s2_need_d_T_1 = ~s1_need_pb | s1_first; // @[SourceD.scala 167:41]
  reg  s2_need_d; // @[Reg.scala 15:16]
  reg [127:0] s2_pdata_r_data; // @[Reg.scala 15:16]
  reg [15:0] s2_pdata_r_mask; // @[Reg.scala 15:16]
  reg  s2_pdata_r_corrupt; // @[Reg.scala 15:16]
  wire  s2_valid = s2_full & _s2_ready_T_2; // @[SourceD.scala 194:23]
  wire  _T_36 = s2_valid & s3_ready; // @[SourceD.scala 188:18]
  wire  _GEN_55 = _T_36 ? 1'h0 : s2_full; // @[SourceD.scala 188:31 SourceD.scala 188:41 SourceD.scala 158:24]
  wire [1:0] _GEN_58 = _T_36 ? s2_beat : s3_beat; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  reg [1:0] s3_bypass; // @[Reg.scala 15:16]
  reg  s3_req_prio_0; // @[Reg.scala 15:16]
  reg  s3_req_prio_2; // @[Reg.scala 15:16]
  reg [2:0] s3_req_opcode; // @[Reg.scala 15:16]
  reg [2:0] s3_req_param; // @[Reg.scala 15:16]
  reg [6:0] s3_req_source; // @[Reg.scala 15:16]
  reg [2:0] s3_req_sink; // @[Reg.scala 15:16]
  reg  s3_req_bad; // @[Reg.scala 15:16]
  wire [2:0] _GEN_64 = _T_36 ? s2_req_way : s3_req_way; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire [9:0] _GEN_66 = _T_36 ? s2_req_set : s3_req_set; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  reg [127:0] s3_pdata_data; // @[Reg.scala 15:16]
  reg [15:0] s3_pdata_mask; // @[Reg.scala 15:16]
  reg  s3_pdata_corrupt; // @[Reg.scala 15:16]
  reg  s3_need_pb; // @[Reg.scala 15:16]
  reg  s3_need_r; // @[Reg.scala 15:16]
  wire  _s3_acq_T = s3_req_opcode == 3'h6; // @[SourceD.scala 215:30]
  wire  _s3_acq_T_1 = s3_req_opcode == 3'h7; // @[SourceD.scala 215:64]
  wire  s3_acq = _s3_acq_T | _s3_acq_T_1; // @[SourceD.scala 215:47]
  reg [1:0] s3_bypass_data_REG; // @[SourceD.scala 403:19]
  reg [127:0] s3_bypass_data_REG_1; // @[SourceD.scala 403:66]
  wire [63:0] s3_bypass_data_hi_2 = s3_bypass_data_REG[1] ? atomics_io_data_out[127:64] : s3_bypass_data_REG_1[127:64]; // @[SourceD.scala 224:75]
  wire [63:0] s3_bypass_data_lo_2 = s3_bypass_data_REG[0] ? atomics_io_data_out[63:0] : s3_bypass_data_REG_1[63:0]; // @[SourceD.scala 224:75]
  wire [127:0] s3_bypass_data = {s3_bypass_data_hi_2,s3_bypass_data_lo_2}; // @[Cat.scala 30:58]
  wire [63:0] s3_rdata_lo = s3_bypass[0] ? s3_bypass_data[63:0] : queue_io_deq_bits_data[63:0]; // @[SourceD.scala 224:75]
  wire [63:0] s3_rdata_hi = s3_bypass[1] ? s3_bypass_data[127:64] : queue_io_deq_bits_data[127:64]; // @[SourceD.scala 224:75]
  wire [127:0] s3_rdata = {s3_rdata_hi,s3_rdata_lo}; // @[Cat.scala 30:58]
  reg [1:0] s3_bypass_corrupts_REG; // @[SourceD.scala 414:28]
  reg  s4_rdata_corrupt; // @[Reg.scala 15:16]
  reg  s4_need_data; // @[Reg.scala 15:16]
  reg  s4_need_pb; // @[Reg.scala 15:16]
  reg  s4_pdata_corrupt; // @[Reg.scala 15:16]
  wire  s4_corrupt = s4_rdata_corrupt & s4_need_data | s4_need_pb & s4_pdata_corrupt; // @[SourceD.scala 306:55]
  wire [1:0] _s4_corrupts_T = {s4_corrupt,s4_corrupt}; // @[SourceD.scala 408:94]
  wire  s3_valid = s3_full & _s3_ready_T_2; // @[SourceD.scala 285:23]
  wire  s4_latch = s3_valid & s3_retires & s4_ready; // @[SourceD.scala 291:41]
  wire [2:0] pre_s4_req_size = s4_latch ? s3_req_size : s4_req_size; // @[SourceD.scala 371:24]
  wire  _pre_s4_chunk_mask_T = pre_s4_req_size >= 3'h4; // @[Misc.scala 211:21]
  wire [5:0] pre_s4_req_offset = s4_latch ? s3_req_offset : s4_req_offset; // @[SourceD.scala 371:24]
  wire  pre_s4_chunk_mask_bit = pre_s4_req_offset[3]; // @[Misc.scala 215:26]
  wire  pre_s4_chunk_mask_hi = _pre_s4_chunk_mask_T | pre_s4_chunk_mask_bit; // @[Misc.scala 220:29]
  wire  pre_s4_chunk_mask_nbit = ~pre_s4_chunk_mask_bit; // @[Misc.scala 216:20]
  wire  pre_s4_chunk_mask_lo = _pre_s4_chunk_mask_T | pre_s4_chunk_mask_nbit; // @[Misc.scala 220:29]
  wire [1:0] pre_s4_chunk_mask = {pre_s4_chunk_mask_hi,pre_s4_chunk_mask_lo}; // @[Cat.scala 30:58]
  wire [1:0] s4_corrupts = _s4_corrupts_T & pre_s4_chunk_mask; // @[SourceD.scala 408:101]
  reg [1:0] s3_bypass_corrupts_REG_1; // @[SourceD.scala 414:67]
  wire  s3_bypass_corrupts_hi_2 = s3_bypass_corrupts_REG[1] ? s4_corrupts[1] : s3_bypass_corrupts_REG_1[1]; // @[SourceD.scala 229:72]
  wire  s3_bypass_corrupts_lo_2 = s3_bypass_corrupts_REG[0] ? s4_corrupts[0] : s3_bypass_corrupts_REG_1[0]; // @[SourceD.scala 229:72]
  wire [1:0] s3_bypass_corrupts = {s3_bypass_corrupts_hi_2,s3_bypass_corrupts_lo_2}; // @[Cat.scala 30:58]
  wire  s3_corrupt_lo = s3_bypass[0] & s3_bypass_corrupts[0]; // @[SourceD.scala 229:72]
  wire  s3_corrupt_hi = s3_bypass[1] & s3_bypass_corrupts[1]; // @[SourceD.scala 229:72]
  wire [1:0] _s3_corrupt_T_6 = {s3_corrupt_hi,s3_corrupt_lo}; // @[Cat.scala 30:58]
  wire  s3_corrupt = |_s3_corrupt_T_6; // @[SourceD.scala 231:82]
  wire [2:0] grant = s3_req_param == 3'h2 ? 3'h4 : 3'h5; // @[SourceD.scala 234:18]
  wire  _s3_need_data_T = s3_req_opcode == 3'h2; // @[package.scala 15:47]
  wire  _s3_need_data_T_1 = s3_req_opcode == 3'h3; // @[package.scala 15:47]
  wire  _s3_need_data_T_2 = s3_req_opcode == 3'h4; // @[package.scala 15:47]
  wire  _s3_need_data_T_4 = _s3_need_data_T | _s3_need_data_T_1 | _s3_need_data_T_2; // @[package.scala 72:59]
  wire  _s3_need_data_T_7 = _s3_acq_T & s3_req_param != 3'h2; // @[SourceD.scala 238:58]
  wire  _s3_need_data_T_8 = _s3_need_data_T_4 | _s3_need_data_T_7; // @[SourceD.scala 237:87]
  wire  s3_need_data = s3_req_prio_0 & _s3_need_data_T_8; // @[SourceD.scala 236:37]
  wire [2:0] _GEN_87 = 3'h2 == s3_req_opcode ? 3'h1 : 3'h0; // @[SourceD.scala 246:24 SourceD.scala 246:24]
  wire [2:0] _GEN_88 = 3'h3 == s3_req_opcode ? 3'h1 : _GEN_87; // @[SourceD.scala 246:24 SourceD.scala 246:24]
  wire [2:0] _GEN_89 = 3'h4 == s3_req_opcode ? 3'h1 : _GEN_88; // @[SourceD.scala 246:24 SourceD.scala 246:24]
  wire [2:0] _GEN_90 = 3'h5 == s3_req_opcode ? 3'h2 : _GEN_89; // @[SourceD.scala 246:24 SourceD.scala 246:24]
  wire [2:0] _GEN_91 = 3'h6 == s3_req_opcode ? grant : _GEN_90; // @[SourceD.scala 246:24 SourceD.scala 246:24]
  wire [2:0] _GEN_92 = 3'h7 == s3_req_opcode ? 3'h4 : _GEN_91; // @[SourceD.scala 246:24 SourceD.scala 246:24]
  wire [2:0] d_bits_opcode = s3_req_prio_0 ? _GEN_92 : 3'h6; // @[SourceD.scala 246:24]
  wire [1:0] _d_bits_param_T_2 = s3_req_param != 3'h0 ? 2'h0 : 2'h1; // @[SourceD.scala 247:54]
  wire  d_bits_corrupt = (s3_req_bad | s3_corrupt & s3_need_data) & d_bits_opcode[0]; // @[SourceD.scala 253:66]
  wire  old_d_bits_corrupt = s3_req_bad & d_bits_opcode[0]; // @[SourceD.scala 255:70]
  wire  d_bits_corrupt_diff = old_d_bits_corrupt ^ d_bits_corrupt; // @[SourceD.scala 256:48]
  wire  _io_corrected_valid_T = s3_valid & s4_ready; // @[SourceD.scala 267:36]
  wire  _GEN_94 = _io_corrected_valid_T ? 1'h0 : s3_full; // @[SourceD.scala 279:31 SourceD.scala 279:41 SourceD.scala 201:24]
  wire [1:0] _GEN_97 = s4_latch ? s3_beat : s4_beat; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  reg  s4_req_prio_2; // @[Reg.scala 15:16]
  reg [2:0] s4_req_param; // @[Reg.scala 15:16]
  wire [2:0] _GEN_105 = s4_latch ? s3_req_way : s4_req_way; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire [9:0] _GEN_107 = s4_latch ? s3_req_set : s4_req_set; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  reg [2:0] s4_adjusted_opcode; // @[Reg.scala 15:16]
  reg [127:0] s4_pdata_data; // @[Reg.scala 15:16]
  reg [15:0] s4_pdata_mask; // @[Reg.scala 15:16]
  reg [127:0] s4_rdata; // @[Reg.scala 15:16]
  wire  io_bs_wadr_bits_mask_lo_1 = s4_pdata_mask[0] | s4_pdata_mask[1] | s4_pdata_mask[2] | s4_pdata_mask[3] |
    s4_pdata_mask[4] | s4_pdata_mask[5] | s4_pdata_mask[6] | s4_pdata_mask[7]; // @[SourceD.scala 326:65]
  wire  io_bs_wadr_bits_mask_hi_1 = s4_pdata_mask[8] | s4_pdata_mask[9] | s4_pdata_mask[10] | s4_pdata_mask[11] |
    s4_pdata_mask[12] | s4_pdata_mask[13] | s4_pdata_mask[14] | s4_pdata_mask[15]; // @[SourceD.scala 326:65]
  wire  _T_123 = io_bs_wadr_ready | _s4_ready_T_4; // @[SourceD.scala 341:26]
  wire  _GEN_128 = io_bs_wadr_ready | _s4_ready_T_4 ? 1'h0 : s4_full; // @[SourceD.scala 341:42 SourceD.scala 341:52 SourceD.scala 292:24]
  wire  retire = s4_full & _T_123; // @[SourceD.scala 351:24]
  reg [2:0] s5_req_size; // @[Reg.scala 15:16]
  reg [5:0] s5_req_offset; // @[Reg.scala 15:16]
  reg [9:0] s5_req_set; // @[Reg.scala 15:16]
  reg [2:0] s5_req_way; // @[Reg.scala 15:16]
  wire [2:0] _GEN_134 = retire ? s4_req_way : s5_req_way; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire [9:0] _GEN_136 = retire ? s4_req_set : s5_req_set; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire [5:0] _GEN_138 = retire ? s4_req_offset : s5_req_offset; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire [2:0] _GEN_141 = retire ? s4_req_size : s5_req_size; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  reg [1:0] s5_beat; // @[Reg.scala 15:16]
  wire [1:0] _GEN_148 = retire ? s4_beat : s5_beat; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  reg [127:0] s5_dat; // @[Reg.scala 15:16]
  wire [127:0] _GEN_149 = retire ? atomics_io_data_out : s5_dat; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  reg  s5_corrupt; // @[Reg.scala 15:16]
  wire  _GEN_150 = retire ? s4_corrupt : s5_corrupt; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  reg [2:0] s6_req_size; // @[Reg.scala 15:16]
  reg [5:0] s6_req_offset; // @[Reg.scala 15:16]
  reg [9:0] s6_req_set; // @[Reg.scala 15:16]
  reg [2:0] s6_req_way; // @[Reg.scala 15:16]
  wire [2:0] _GEN_155 = retire ? s5_req_way : s6_req_way; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire [9:0] _GEN_157 = retire ? s5_req_set : s6_req_set; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire [5:0] _GEN_159 = retire ? s5_req_offset : s6_req_offset; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire [2:0] _GEN_162 = retire ? s5_req_size : s6_req_size; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  reg [1:0] s6_beat; // @[Reg.scala 15:16]
  wire [1:0] _GEN_169 = retire ? s5_beat : s6_beat; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  reg [127:0] s6_dat; // @[Reg.scala 15:16]
  wire [127:0] _GEN_170 = retire ? s5_dat : s6_dat; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  reg  s6_corrupt; // @[Reg.scala 15:16]
  wire  _GEN_171 = retire ? s5_corrupt : s6_corrupt; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  reg [2:0] s7_req_size; // @[Reg.scala 15:16]
  reg [5:0] s7_req_offset; // @[Reg.scala 15:16]
  wire [5:0] _GEN_180 = retire ? s6_req_offset : s7_req_offset; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire [2:0] _GEN_183 = retire ? s6_req_size : s7_req_size; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  reg [127:0] s7_dat; // @[Reg.scala 15:16]
  wire [127:0] _GEN_190 = retire ? s6_dat : s7_dat; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  reg  s7_corrupt; // @[Reg.scala 15:16]
  wire  _GEN_191 = retire ? s6_corrupt : s7_corrupt; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire  pre_s4_full = s4_latch | ~_T_123 & s4_full; // @[SourceD.scala 386:30]
  wire  pre_s3_4_match = _GEN_107 == _GEN_66 & _GEN_105 == _GEN_64 & _GEN_97 == _GEN_58 & pre_s4_full; // @[SourceD.scala 388:127]
  wire  pre_s3_5_match = _GEN_136 == _GEN_66 & _GEN_134 == _GEN_64 & _GEN_148 == _GEN_58; // @[SourceD.scala 389:96]
  wire  pre_s3_6_match = _GEN_157 == _GEN_66 & _GEN_155 == _GEN_64 & _GEN_169 == _GEN_58; // @[SourceD.scala 390:96]
  wire  _pre_s5_chunk_mask_T = _GEN_141 >= 3'h4; // @[Misc.scala 211:21]
  wire  pre_s5_chunk_mask_bit = _GEN_138[3]; // @[Misc.scala 215:26]
  wire  pre_s5_chunk_mask_nbit = ~pre_s5_chunk_mask_bit; // @[Misc.scala 216:20]
  wire  pre_s5_chunk_mask_lo = _pre_s5_chunk_mask_T | pre_s5_chunk_mask_nbit; // @[Misc.scala 220:29]
  wire  pre_s5_chunk_mask_hi = _pre_s5_chunk_mask_T | pre_s5_chunk_mask_bit; // @[Misc.scala 220:29]
  wire [1:0] pre_s5_chunk_mask = {pre_s5_chunk_mask_hi,pre_s5_chunk_mask_lo}; // @[Cat.scala 30:58]
  wire  _pre_s6_chunk_mask_T = _GEN_162 >= 3'h4; // @[Misc.scala 211:21]
  wire  pre_s6_chunk_mask_bit = _GEN_159[3]; // @[Misc.scala 215:26]
  wire  pre_s6_chunk_mask_nbit = ~pre_s6_chunk_mask_bit; // @[Misc.scala 216:20]
  wire  pre_s6_chunk_mask_lo = _pre_s6_chunk_mask_T | pre_s6_chunk_mask_nbit; // @[Misc.scala 220:29]
  wire  pre_s6_chunk_mask_hi = _pre_s6_chunk_mask_T | pre_s6_chunk_mask_bit; // @[Misc.scala 220:29]
  wire [1:0] pre_s6_chunk_mask = {pre_s6_chunk_mask_hi,pre_s6_chunk_mask_lo}; // @[Cat.scala 30:58]
  wire  _pre_s7_chunk_mask_T = _GEN_183 >= 3'h4; // @[Misc.scala 211:21]
  wire  pre_s7_chunk_mask_bit = _GEN_180[3]; // @[Misc.scala 215:26]
  wire  pre_s7_chunk_mask_nbit = ~pre_s7_chunk_mask_bit; // @[Misc.scala 216:20]
  wire  pre_s7_chunk_mask_lo = _pre_s7_chunk_mask_T | pre_s7_chunk_mask_nbit; // @[Misc.scala 220:29]
  wire  pre_s7_chunk_mask_hi = _pre_s7_chunk_mask_T | pre_s7_chunk_mask_bit; // @[Misc.scala 220:29]
  wire [1:0] pre_s7_chunk_mask = {pre_s7_chunk_mask_hi,pre_s7_chunk_mask_lo}; // @[Cat.scala 30:58]
  wire [1:0] pre_s3_5_bypass = pre_s3_5_match ? pre_s5_chunk_mask : 2'h0; // @[SourceD.scala 399:28]
  wire [1:0] pre_s3_6_bypass = pre_s3_6_match ? pre_s6_chunk_mask : 2'h0; // @[SourceD.scala 400:28]
  wire [63:0] s3_bypass_data_lo = pre_s3_6_bypass[0] ? _GEN_170[63:0] : _GEN_190[63:0]; // @[SourceD.scala 224:75]
  wire [63:0] s3_bypass_data_hi = pre_s3_6_bypass[1] ? _GEN_170[127:64] : _GEN_190[127:64]; // @[SourceD.scala 224:75]
  wire [127:0] _s3_bypass_data_T_6 = {s3_bypass_data_hi,s3_bypass_data_lo}; // @[Cat.scala 30:58]
  wire [63:0] s3_bypass_data_lo_1 = pre_s3_5_bypass[0] ? _GEN_149[63:0] : _s3_bypass_data_T_6[63:0]; // @[SourceD.scala 224:75]
  wire [63:0] s3_bypass_data_hi_1 = pre_s3_5_bypass[1] ? _GEN_149[127:64] : _s3_bypass_data_T_6[127:64]; // @[SourceD.scala 224:75]
  wire [1:0] _pre_s5_corrupts_T = {_GEN_150,_GEN_150}; // @[SourceD.scala 409:94]
  wire [1:0] pre_s5_corrupts = _pre_s5_corrupts_T & pre_s5_chunk_mask; // @[SourceD.scala 409:101]
  wire [1:0] _pre_s6_corrupts_T = {_GEN_171,_GEN_171}; // @[SourceD.scala 410:94]
  wire [1:0] pre_s6_corrupts = _pre_s6_corrupts_T & pre_s6_chunk_mask; // @[SourceD.scala 410:101]
  wire [1:0] _pre_s7_corrupts_T = {_GEN_191,_GEN_191}; // @[SourceD.scala 411:94]
  wire [1:0] pre_s7_corrupts = _pre_s7_corrupts_T & pre_s7_chunk_mask; // @[SourceD.scala 411:101]
  wire  s3_bypass_corrupts_lo = pre_s3_6_bypass[0] ? pre_s6_corrupts[0] : pre_s7_corrupts[0]; // @[SourceD.scala 229:72]
  wire  s3_bypass_corrupts_hi = pre_s3_6_bypass[1] ? pre_s6_corrupts[1] : pre_s7_corrupts[1]; // @[SourceD.scala 229:72]
  wire [1:0] _s3_bypass_corrupts_T_6 = {s3_bypass_corrupts_hi,s3_bypass_corrupts_lo}; // @[Cat.scala 30:58]
  wire  s3_bypass_corrupts_lo_1 = pre_s3_5_bypass[0] ? pre_s5_corrupts[0] : _s3_bypass_corrupts_T_6[0]; // @[SourceD.scala 229:72]
  wire  s3_bypass_corrupts_hi_1 = pre_s3_5_bypass[1] ? pre_s5_corrupts[1] : _s3_bypass_corrupts_T_6[1]; // @[SourceD.scala 229:72]
  wire  _io_evict_safe_T_9 = _s2_ready_T | io_evict_req_way != s2_req_way | io_evict_req_set != s2_req_set; // @[SourceD.scala 455:54]
  wire  _io_evict_safe_T_10 = (_s1_req_reg_T | io_evict_req_way != s1_req_reg_way | io_evict_req_set != s1_req_reg_set)
     & _io_evict_safe_T_9; // @[SourceD.scala 454:94]
  wire  _io_evict_safe_T_15 = _s3_ready_T | io_evict_req_way != s3_req_way | io_evict_req_set != s3_req_set; // @[SourceD.scala 456:54]
  wire  _io_evict_safe_T_16 = _io_evict_safe_T_10 & _io_evict_safe_T_15; // @[SourceD.scala 455:90]
  wire  _io_evict_safe_T_21 = _s4_ready_T_1 | io_evict_req_way != s4_req_way | io_evict_req_set != s4_req_set; // @[SourceD.scala 457:54]
  wire  _io_grant_safe_T_9 = _s2_ready_T | io_grant_req_way != s2_req_way | io_grant_req_set != s2_req_set; // @[SourceD.scala 462:54]
  wire  _io_grant_safe_T_10 = (_s1_req_reg_T | io_grant_req_way != s1_req_reg_way | io_grant_req_set != s1_req_reg_set)
     & _io_grant_safe_T_9; // @[SourceD.scala 461:94]
  wire  _io_grant_safe_T_15 = _s3_ready_T | io_grant_req_way != s3_req_way | io_grant_req_set != s3_req_set; // @[SourceD.scala 463:54]
  wire  _io_grant_safe_T_16 = _io_grant_safe_T_10 & _io_grant_safe_T_15; // @[SourceD.scala 462:90]
  wire  _io_grant_safe_T_21 = _s4_ready_T_1 | io_grant_req_way != s4_req_way | io_grant_req_set != s4_req_set; // @[SourceD.scala 464:54]
  RHEA__QueueCompatibility_163 queue ( // @[SourceD.scala 131:21]
    .rf_reset(queue_rf_reset),
    .clock(queue_clock),
    .reset(queue_reset),
    .io_enq_ready(queue_io_enq_ready),
    .io_enq_valid(queue_io_enq_valid),
    .io_enq_bits_data(queue_io_enq_bits_data),
    .io_deq_ready(queue_io_deq_ready),
    .io_deq_valid(queue_io_deq_valid),
    .io_deq_bits_data(queue_io_deq_bits_data)
  );
  RHEA__Atomics atomics ( // @[SourceD.scala 308:23]
    .io_write(atomics_io_write),
    .io_a_opcode(atomics_io_a_opcode),
    .io_a_param(atomics_io_a_param),
    .io_a_mask(atomics_io_a_mask),
    .io_a_data(atomics_io_a_data),
    .io_data_in(atomics_io_data_in),
    .io_data_out(atomics_io_data_out)
  );
  assign queue_rf_reset = rf_reset;
  assign io_req_ready = ~busy; // @[SourceD.scala 151:19]
  assign io_d_valid = s3_valid_d; // @[SourceD.scala 242:15 SourceD.scala 245:11]
  assign io_d_bits_opcode = s3_req_prio_0 ? _GEN_92 : 3'h6; // @[SourceD.scala 246:24]
  assign io_d_bits_param = s3_req_prio_0 & s3_acq ? _d_bits_param_T_2 : 2'h0; // @[SourceD.scala 247:24]
  assign io_d_bits_size = s3_req_size; // @[SourceD.scala 242:15 SourceD.scala 248:18]
  assign io_d_bits_source = s3_req_source; // @[SourceD.scala 242:15 SourceD.scala 249:18]
  assign io_d_bits_sink = s3_req_sink; // @[SourceD.scala 242:15 SourceD.scala 250:18]
  assign io_d_bits_denied = s3_req_bad; // @[SourceD.scala 242:15 SourceD.scala 251:18]
  assign io_d_bits_data = {s3_rdata_hi,s3_rdata_lo}; // @[Cat.scala 30:58]
  assign io_d_bits_corrupt = (s3_req_bad | s3_corrupt & s3_need_data) & d_bits_opcode[0]; // @[SourceD.scala 253:66]
  assign io_pb_pop_valid = s2_valid_pb & s2_req_prio_0; // @[SourceD.scala 175:34]
  assign io_pb_pop_bits_index = s2_req_put; // @[SourceD.scala 176:24]
  assign io_pb_pop_bits_last = s2_last; // @[SourceD.scala 177:24]
  assign io_rel_pop_valid = s2_valid_pb & ~s2_req_prio_0; // @[SourceD.scala 178:35]
  assign io_rel_pop_bits_index = s2_req_put; // @[SourceD.scala 179:25]
  assign io_rel_pop_bits_last = s2_last; // @[SourceD.scala 180:25]
  assign io_bs_radr_valid = _s1_latch_bypass_T & s1_need_r & ~s1_block_r; // @[SourceD.scala 106:56]
  assign io_bs_radr_bits_way = _s1_req_reg_T ? io_req_bits_way : s1_req_reg_way; // @[SourceD.scala 98:19]
  assign io_bs_radr_bits_set = _s1_req_reg_T ? io_req_bits_set : s1_req_reg_set; // @[SourceD.scala 98:19]
  assign io_bs_radr_bits_beat = s1_req_offset[5:4] | s1_counter; // @[SourceD.scala 113:56]
  assign io_bs_radr_bits_mask = _s1_mask_T_1 & _s1_mask_T_2; // @[SourceD.scala 102:76]
  assign io_bs_wadr_valid = s4_full & s4_need_bs; // @[SourceD.scala 319:31]
  assign io_bs_wadr_bits_way = s4_req_way; // @[SourceD.scala 321:24]
  assign io_bs_wadr_bits_set = s4_req_set; // @[SourceD.scala 322:24]
  assign io_bs_wadr_bits_beat = s4_beat; // @[SourceD.scala 323:24]
  assign io_bs_wadr_bits_mask = {io_bs_wadr_bits_mask_hi_1,io_bs_wadr_bits_mask_lo_1}; // @[Cat.scala 30:58]
  assign io_bs_wdat_data = atomics_io_data_out; // @[SourceD.scala 327:19]
  assign io_bs_wdat_poison = s4_rdata_corrupt & s4_need_data | s4_need_pb & s4_pdata_corrupt; // @[SourceD.scala 306:55]
  assign io_evict_safe = _io_evict_safe_T_16 & _io_evict_safe_T_21; // @[SourceD.scala 456:90]
  assign io_grant_safe = _io_grant_safe_T_16 & _io_grant_safe_T_21; // @[SourceD.scala 463:90]
  assign io_corrected_valid = 1'h0; // @[SourceD.scala 267:61]
  assign io_uncorrected_valid = 1'h0; // @[SourceD.scala 268:75]
  assign queue_clock = clock;
  assign queue_reset = reset;
  assign queue_io_enq_valid = queue_io_enq_valid_REG_1; // @[SourceD.scala 132:22]
  assign queue_io_enq_bits_data = io_bs_rdat_data; // @[SourceD.scala 133:21]
  assign queue_io_deq_ready = _io_corrected_valid_T & s3_need_r; // @[SourceD.scala 275:46]
  assign atomics_io_write = s4_req_prio_2; // @[SourceD.scala 309:24]
  assign atomics_io_a_opcode = s4_adjusted_opcode; // @[SourceD.scala 310:24]
  assign atomics_io_a_param = s4_req_param; // @[SourceD.scala 311:24]
  assign atomics_io_a_mask = s4_pdata_mask; // @[SourceD.scala 315:24]
  assign atomics_io_a_data = s4_pdata_data; // @[SourceD.scala 316:24]
  assign atomics_io_data_in = s4_rdata; // @[SourceD.scala 317:24]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      busy <= 1'h0;
    end else if (_T_23) begin
      if (s1_last) begin
        busy <= 1'h0;
      end else begin
        busy <= _GEN_20;
      end
    end else begin
      busy <= _GEN_20;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s1_block_r <= 1'h0;
    end else if (_T_23) begin
      s1_block_r <= 1'h0;
    end else begin
      s1_block_r <= _GEN_19;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s1_counter <= 2'h0;
    end else if (_T_23) begin
      if (s1_last) begin
        s1_counter <= 2'h0;
      end else begin
        s1_counter <= _s1_counter_T_1;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_req_reg_prio_0 <= 1'h0;
    end else if (_s1_req_reg_T_1) begin
      s1_req_reg_prio_0 <= io_req_bits_prio_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_req_reg_prio_2 <= 1'h0;
    end else if (_s1_req_reg_T_1) begin
      s1_req_reg_prio_2 <= io_req_bits_prio_2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_req_reg_opcode <= 3'h0;
    end else if (_s1_req_reg_T_1) begin
      s1_req_reg_opcode <= io_req_bits_opcode;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_req_reg_param <= 3'h0;
    end else if (_s1_req_reg_T_1) begin
      s1_req_reg_param <= io_req_bits_param;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_req_reg_size <= 3'h0;
    end else if (_s1_req_reg_T_1) begin
      s1_req_reg_size <= io_req_bits_size;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_req_reg_source <= 7'h0;
    end else if (_s1_req_reg_T_1) begin
      s1_req_reg_source <= io_req_bits_source;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_req_reg_offset <= 6'h0;
    end else if (_s1_req_reg_T_1) begin
      s1_req_reg_offset <= io_req_bits_offset;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_req_reg_put <= 5'h0;
    end else if (_s1_req_reg_T_1) begin
      s1_req_reg_put <= io_req_bits_put;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_req_reg_set <= 10'h0;
    end else if (_s1_req_reg_T_1) begin
      s1_req_reg_set <= io_req_bits_set;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_req_reg_sink <= 3'h0;
    end else if (_s1_req_reg_T_1) begin
      s1_req_reg_sink <= io_req_bits_sink;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_req_reg_way <= 3'h0;
    end else if (_s1_req_reg_T_1) begin
      s1_req_reg_way <= io_req_bits_way;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_req_reg_bad <= 1'h0;
    end else if (_s1_req_reg_T_1) begin
      s1_req_reg_bad <= io_req_bits_bad;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s2_full <= 1'h0;
    end else begin
      s2_full <= _T_23 | _GEN_55;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s3_full <= 1'h0;
    end else begin
      s3_full <= _T_36 | _GEN_94;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_retires <= 1'h0;
    end else if (_T_36) begin
      s3_retires <= s2_retires;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s4_full <= 1'h0;
    end else begin
      s4_full <= s4_latch | _GEN_128;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_need_bs <= 1'h0;
    end else if (s4_latch) begin
      s4_need_bs <= s3_need_pb;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s3_valid_d <= 1'h0;
    end else if (_T_36) begin
      s3_valid_d <= s2_need_d;
    end else if (io_d_ready) begin
      s3_valid_d <= 1'h0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      s2_valid_pb <= 1'h0;
    end else if (_T_23) begin
      if (s1_req_prio_0) begin
        s2_valid_pb <= ~s1_req_opcode[2];
      end else begin
        s2_valid_pb <= s1_req_opcode[0];
      end
    end else if (pb_ready) begin
      s2_valid_pb <= 1'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_prio_0 <= 1'h0;
    end else if (_T_23) begin
      if (_s1_req_reg_T) begin
        s2_req_prio_0 <= io_req_bits_prio_0;
      end else begin
        s2_req_prio_0 <= s1_req_reg_prio_0;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_latch_bypass <= 1'h0;
    end else begin
      s1_latch_bypass <= ~_s1_latch_bypass_T | s2_ready;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_bypass_r <= 2'h0;
    end else if (s1_latch_bypass) begin
      s1_bypass_r <= s1_x_bypass;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_set <= 10'h0;
    end else if (_T_23) begin
      if (_s1_req_reg_T) begin
        s2_req_set <= io_req_bits_set;
      end else begin
        s2_req_set <= s1_req_reg_set;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_way <= 3'h0;
    end else if (_T_23) begin
      if (_s1_req_reg_T) begin
        s2_req_way <= io_req_bits_way;
      end else begin
        s2_req_way <= s1_req_reg_way;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_beat <= 2'h0;
    end else if (_T_23) begin
      s2_beat <= s1_beat;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_retires <= 1'h0;
    end else if (_T_23) begin
      s2_retires <= s1_retires;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_size <= 3'h0;
    end else if (_T_23) begin
      if (_s1_req_reg_T) begin
        s2_req_size <= io_req_bits_size;
      end else begin
        s2_req_size <= s1_req_reg_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_offset <= 6'h0;
    end else if (_T_23) begin
      if (_s1_req_reg_T) begin
        s2_req_offset <= io_req_bits_offset;
      end else begin
        s2_req_offset <= s1_req_reg_offset;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_set <= 10'h0;
    end else if (_T_36) begin
      s3_req_set <= s2_req_set;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_way <= 3'h0;
    end else if (_T_36) begin
      s3_req_way <= s2_req_way;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_beat <= 2'h0;
    end else if (_T_36) begin
      s3_beat <= s2_beat;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_size <= 3'h0;
    end else if (_T_36) begin
      s3_req_size <= s2_req_size;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_offset <= 6'h0;
    end else if (_T_36) begin
      s3_req_offset <= s2_req_offset;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_req_set <= 10'h0;
    end else if (s4_latch) begin
      s4_req_set <= s3_req_set;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_req_way <= 3'h0;
    end else if (s4_latch) begin
      s4_req_way <= s3_req_way;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_beat <= 2'h0;
    end else if (s4_latch) begin
      s4_beat <= s3_beat;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_req_size <= 3'h0;
    end else if (s4_latch) begin
      s4_req_size <= s3_req_size;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_req_offset <= 6'h0;
    end else if (s4_latch) begin
      s4_req_offset <= s3_req_offset;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      queue_io_enq_valid_REG <= 1'h0;
    end else begin
      queue_io_enq_valid_REG <= io_bs_radr_ready & io_bs_radr_valid;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      queue_io_enq_valid_REG_1 <= 1'h0;
    end else begin
      queue_io_enq_valid_REG_1 <= queue_io_enq_valid_REG;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_bypass <= 2'h0;
    end else if (_T_23) begin
      if (s1_latch_bypass) begin
        s2_bypass <= s1_x_bypass;
      end else begin
        s2_bypass <= s1_bypass_r;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_prio_2 <= 1'h0;
    end else if (_T_23) begin
      if (_s1_req_reg_T) begin
        s2_req_prio_2 <= io_req_bits_prio_2;
      end else begin
        s2_req_prio_2 <= s1_req_reg_prio_2;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_opcode <= 3'h0;
    end else if (_T_23) begin
      if (_s1_req_reg_T) begin
        s2_req_opcode <= io_req_bits_opcode;
      end else begin
        s2_req_opcode <= s1_req_reg_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_param <= 3'h0;
    end else if (_T_23) begin
      if (_s1_req_reg_T) begin
        s2_req_param <= io_req_bits_param;
      end else begin
        s2_req_param <= s1_req_reg_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_source <= 7'h0;
    end else if (_T_23) begin
      if (_s1_req_reg_T) begin
        s2_req_source <= io_req_bits_source;
      end else begin
        s2_req_source <= s1_req_reg_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_put <= 5'h0;
    end else if (_T_23) begin
      if (_s1_req_reg_T) begin
        s2_req_put <= io_req_bits_put;
      end else begin
        s2_req_put <= s1_req_reg_put;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_sink <= 3'h0;
    end else if (_T_23) begin
      if (_s1_req_reg_T) begin
        s2_req_sink <= io_req_bits_sink;
      end else begin
        s2_req_sink <= s1_req_reg_sink;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_req_bad <= 1'h0;
    end else if (_T_23) begin
      if (_s1_req_reg_T) begin
        s2_req_bad <= io_req_bits_bad;
      end else begin
        s2_req_bad <= s1_req_reg_bad;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_last <= 1'h0;
    end else if (_T_23) begin
      s2_last <= s1_last;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_need_r <= 1'h0;
    end else if (_T_23) begin
      s2_need_r <= s1_need_r;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_need_pb <= 1'h0;
    end else if (_T_23) begin
      if (s1_req_prio_0) begin
        s2_need_pb <= ~s1_req_opcode[2];
      end else begin
        s2_need_pb <= s1_req_opcode[0];
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_need_d <= 1'h0;
    end else if (_T_23) begin
      s2_need_d <= _s2_need_d_T_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_pdata_r_data <= 128'h0;
    end else if (s2_valid_pb) begin
      if (s2_req_prio_0) begin
        s2_pdata_r_data <= io_pb_beat_data;
      end else begin
        s2_pdata_r_data <= io_rel_beat_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_pdata_r_mask <= 16'h0;
    end else if (s2_valid_pb) begin
      if (s2_req_prio_0) begin
        s2_pdata_r_mask <= io_pb_beat_mask;
      end else begin
        s2_pdata_r_mask <= 16'hffff;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_pdata_r_corrupt <= 1'h0;
    end else if (s2_valid_pb) begin
      if (s2_req_prio_0) begin
        s2_pdata_r_corrupt <= io_pb_beat_corrupt;
      end else begin
        s2_pdata_r_corrupt <= io_rel_beat_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_bypass <= 2'h0;
    end else if (_T_36) begin
      s3_bypass <= s2_bypass;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_prio_0 <= 1'h0;
    end else if (_T_36) begin
      s3_req_prio_0 <= s2_req_prio_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_prio_2 <= 1'h0;
    end else if (_T_36) begin
      s3_req_prio_2 <= s2_req_prio_2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_opcode <= 3'h0;
    end else if (_T_36) begin
      s3_req_opcode <= s2_req_opcode;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_param <= 3'h0;
    end else if (_T_36) begin
      s3_req_param <= s2_req_param;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_source <= 7'h0;
    end else if (_T_36) begin
      s3_req_source <= s2_req_source;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_sink <= 3'h0;
    end else if (_T_36) begin
      s3_req_sink <= s2_req_sink;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_req_bad <= 1'h0;
    end else if (_T_36) begin
      s3_req_bad <= s2_req_bad;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_pdata_data <= 128'h0;
    end else if (_T_36) begin
      if (s2_valid_pb) begin
        if (s2_req_prio_0) begin
          s3_pdata_data <= io_pb_beat_data;
        end else begin
          s3_pdata_data <= io_rel_beat_data;
        end
      end else begin
        s3_pdata_data <= s2_pdata_r_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_pdata_mask <= 16'h0;
    end else if (_T_36) begin
      if (s2_valid_pb) begin
        if (s2_req_prio_0) begin
          s3_pdata_mask <= io_pb_beat_mask;
        end else begin
          s3_pdata_mask <= 16'hffff;
        end
      end else begin
        s3_pdata_mask <= s2_pdata_r_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_pdata_corrupt <= 1'h0;
    end else if (_T_36) begin
      if (s2_valid_pb) begin
        if (s2_req_prio_0) begin
          s3_pdata_corrupt <= io_pb_beat_corrupt;
        end else begin
          s3_pdata_corrupt <= io_rel_beat_corrupt;
        end
      end else begin
        s3_pdata_corrupt <= s2_pdata_r_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_need_pb <= 1'h0;
    end else if (_T_36) begin
      s3_need_pb <= s2_need_pb;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_need_r <= 1'h0;
    end else if (_T_36) begin
      s3_need_r <= s2_need_r;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_bypass_data_REG <= 2'h0;
    end else if (pre_s3_4_match) begin
      s3_bypass_data_REG <= pre_s4_chunk_mask;
    end else begin
      s3_bypass_data_REG <= 2'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_bypass_data_REG_1 <= 128'h0;
    end else begin
      s3_bypass_data_REG_1 <= {s3_bypass_data_hi_1,s3_bypass_data_lo_1};
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_bypass_corrupts_REG <= 2'h0;
    end else if (pre_s3_4_match) begin
      s3_bypass_corrupts_REG <= pre_s4_chunk_mask;
    end else begin
      s3_bypass_corrupts_REG <= 2'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_rdata_corrupt <= 1'h0;
    end else if (s4_latch) begin
      s4_rdata_corrupt <= s3_corrupt;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_need_data <= 1'h0;
    end else if (s4_latch) begin
      s4_need_data <= s3_need_data;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_need_pb <= 1'h0;
    end else if (s4_latch) begin
      s4_need_pb <= s3_need_pb;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_pdata_corrupt <= 1'h0;
    end else if (s4_latch) begin
      s4_pdata_corrupt <= s3_pdata_corrupt;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s3_bypass_corrupts_REG_1 <= 2'h0;
    end else begin
      s3_bypass_corrupts_REG_1 <= {s3_bypass_corrupts_hi_1,s3_bypass_corrupts_lo_1};
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_req_prio_2 <= 1'h0;
    end else if (s4_latch) begin
      s4_req_prio_2 <= s3_req_prio_2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_req_param <= 3'h0;
    end else if (s4_latch) begin
      s4_req_param <= s3_req_param;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_adjusted_opcode <= 3'h0;
    end else if (s4_latch) begin
      if (s3_req_bad) begin
        s4_adjusted_opcode <= 3'h4;
      end else begin
        s4_adjusted_opcode <= s3_req_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_pdata_data <= 128'h0;
    end else if (s4_latch) begin
      s4_pdata_data <= s3_pdata_data;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_pdata_mask <= 16'h0;
    end else if (s4_latch) begin
      s4_pdata_mask <= s3_pdata_mask;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s4_rdata <= 128'h0;
    end else if (s4_latch) begin
      s4_rdata <= s3_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s5_req_size <= 3'h0;
    end else if (retire) begin
      s5_req_size <= s4_req_size;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s5_req_offset <= 6'h0;
    end else if (retire) begin
      s5_req_offset <= s4_req_offset;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s5_req_set <= 10'h0;
    end else if (retire) begin
      s5_req_set <= s4_req_set;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s5_req_way <= 3'h0;
    end else if (retire) begin
      s5_req_way <= s4_req_way;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s5_beat <= 2'h0;
    end else if (retire) begin
      s5_beat <= s4_beat;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s5_dat <= 128'h0;
    end else if (retire) begin
      s5_dat <= atomics_io_data_out;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s5_corrupt <= 1'h0;
    end else if (retire) begin
      s5_corrupt <= s4_corrupt;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s6_req_size <= 3'h0;
    end else if (retire) begin
      s6_req_size <= s5_req_size;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s6_req_offset <= 6'h0;
    end else if (retire) begin
      s6_req_offset <= s5_req_offset;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s6_req_set <= 10'h0;
    end else if (retire) begin
      s6_req_set <= s5_req_set;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s6_req_way <= 3'h0;
    end else if (retire) begin
      s6_req_way <= s5_req_way;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s6_beat <= 2'h0;
    end else if (retire) begin
      s6_beat <= s5_beat;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s6_dat <= 128'h0;
    end else if (retire) begin
      s6_dat <= s5_dat;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s6_corrupt <= 1'h0;
    end else if (retire) begin
      s6_corrupt <= s5_corrupt;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s7_req_size <= 3'h0;
    end else if (retire) begin
      s7_req_size <= s6_req_size;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s7_req_offset <= 6'h0;
    end else if (retire) begin
      s7_req_offset <= s6_req_offset;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s7_dat <= 128'h0;
    end else if (retire) begin
      s7_dat <= s6_dat;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s7_corrupt <= 1'h0;
    end else if (retire) begin
      s7_corrupt <= s6_corrupt;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  busy = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  s1_block_r = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  s1_counter = _RAND_2[1:0];
  _RAND_3 = {1{`RANDOM}};
  s1_req_reg_prio_0 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  s1_req_reg_prio_2 = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  s1_req_reg_opcode = _RAND_5[2:0];
  _RAND_6 = {1{`RANDOM}};
  s1_req_reg_param = _RAND_6[2:0];
  _RAND_7 = {1{`RANDOM}};
  s1_req_reg_size = _RAND_7[2:0];
  _RAND_8 = {1{`RANDOM}};
  s1_req_reg_source = _RAND_8[6:0];
  _RAND_9 = {1{`RANDOM}};
  s1_req_reg_offset = _RAND_9[5:0];
  _RAND_10 = {1{`RANDOM}};
  s1_req_reg_put = _RAND_10[4:0];
  _RAND_11 = {1{`RANDOM}};
  s1_req_reg_set = _RAND_11[9:0];
  _RAND_12 = {1{`RANDOM}};
  s1_req_reg_sink = _RAND_12[2:0];
  _RAND_13 = {1{`RANDOM}};
  s1_req_reg_way = _RAND_13[2:0];
  _RAND_14 = {1{`RANDOM}};
  s1_req_reg_bad = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  s2_full = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  s3_full = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  s3_retires = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  s4_full = _RAND_18[0:0];
  _RAND_19 = {1{`RANDOM}};
  s4_need_bs = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  s3_valid_d = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  s2_valid_pb = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  s2_req_prio_0 = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  s1_latch_bypass = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  s1_bypass_r = _RAND_24[1:0];
  _RAND_25 = {1{`RANDOM}};
  s2_req_set = _RAND_25[9:0];
  _RAND_26 = {1{`RANDOM}};
  s2_req_way = _RAND_26[2:0];
  _RAND_27 = {1{`RANDOM}};
  s2_beat = _RAND_27[1:0];
  _RAND_28 = {1{`RANDOM}};
  s2_retires = _RAND_28[0:0];
  _RAND_29 = {1{`RANDOM}};
  s2_req_size = _RAND_29[2:0];
  _RAND_30 = {1{`RANDOM}};
  s2_req_offset = _RAND_30[5:0];
  _RAND_31 = {1{`RANDOM}};
  s3_req_set = _RAND_31[9:0];
  _RAND_32 = {1{`RANDOM}};
  s3_req_way = _RAND_32[2:0];
  _RAND_33 = {1{`RANDOM}};
  s3_beat = _RAND_33[1:0];
  _RAND_34 = {1{`RANDOM}};
  s3_req_size = _RAND_34[2:0];
  _RAND_35 = {1{`RANDOM}};
  s3_req_offset = _RAND_35[5:0];
  _RAND_36 = {1{`RANDOM}};
  s4_req_set = _RAND_36[9:0];
  _RAND_37 = {1{`RANDOM}};
  s4_req_way = _RAND_37[2:0];
  _RAND_38 = {1{`RANDOM}};
  s4_beat = _RAND_38[1:0];
  _RAND_39 = {1{`RANDOM}};
  s4_req_size = _RAND_39[2:0];
  _RAND_40 = {1{`RANDOM}};
  s4_req_offset = _RAND_40[5:0];
  _RAND_41 = {1{`RANDOM}};
  queue_io_enq_valid_REG = _RAND_41[0:0];
  _RAND_42 = {1{`RANDOM}};
  queue_io_enq_valid_REG_1 = _RAND_42[0:0];
  _RAND_43 = {1{`RANDOM}};
  s2_bypass = _RAND_43[1:0];
  _RAND_44 = {1{`RANDOM}};
  s2_req_prio_2 = _RAND_44[0:0];
  _RAND_45 = {1{`RANDOM}};
  s2_req_opcode = _RAND_45[2:0];
  _RAND_46 = {1{`RANDOM}};
  s2_req_param = _RAND_46[2:0];
  _RAND_47 = {1{`RANDOM}};
  s2_req_source = _RAND_47[6:0];
  _RAND_48 = {1{`RANDOM}};
  s2_req_put = _RAND_48[4:0];
  _RAND_49 = {1{`RANDOM}};
  s2_req_sink = _RAND_49[2:0];
  _RAND_50 = {1{`RANDOM}};
  s2_req_bad = _RAND_50[0:0];
  _RAND_51 = {1{`RANDOM}};
  s2_last = _RAND_51[0:0];
  _RAND_52 = {1{`RANDOM}};
  s2_need_r = _RAND_52[0:0];
  _RAND_53 = {1{`RANDOM}};
  s2_need_pb = _RAND_53[0:0];
  _RAND_54 = {1{`RANDOM}};
  s2_need_d = _RAND_54[0:0];
  _RAND_55 = {4{`RANDOM}};
  s2_pdata_r_data = _RAND_55[127:0];
  _RAND_56 = {1{`RANDOM}};
  s2_pdata_r_mask = _RAND_56[15:0];
  _RAND_57 = {1{`RANDOM}};
  s2_pdata_r_corrupt = _RAND_57[0:0];
  _RAND_58 = {1{`RANDOM}};
  s3_bypass = _RAND_58[1:0];
  _RAND_59 = {1{`RANDOM}};
  s3_req_prio_0 = _RAND_59[0:0];
  _RAND_60 = {1{`RANDOM}};
  s3_req_prio_2 = _RAND_60[0:0];
  _RAND_61 = {1{`RANDOM}};
  s3_req_opcode = _RAND_61[2:0];
  _RAND_62 = {1{`RANDOM}};
  s3_req_param = _RAND_62[2:0];
  _RAND_63 = {1{`RANDOM}};
  s3_req_source = _RAND_63[6:0];
  _RAND_64 = {1{`RANDOM}};
  s3_req_sink = _RAND_64[2:0];
  _RAND_65 = {1{`RANDOM}};
  s3_req_bad = _RAND_65[0:0];
  _RAND_66 = {4{`RANDOM}};
  s3_pdata_data = _RAND_66[127:0];
  _RAND_67 = {1{`RANDOM}};
  s3_pdata_mask = _RAND_67[15:0];
  _RAND_68 = {1{`RANDOM}};
  s3_pdata_corrupt = _RAND_68[0:0];
  _RAND_69 = {1{`RANDOM}};
  s3_need_pb = _RAND_69[0:0];
  _RAND_70 = {1{`RANDOM}};
  s3_need_r = _RAND_70[0:0];
  _RAND_71 = {1{`RANDOM}};
  s3_bypass_data_REG = _RAND_71[1:0];
  _RAND_72 = {4{`RANDOM}};
  s3_bypass_data_REG_1 = _RAND_72[127:0];
  _RAND_73 = {1{`RANDOM}};
  s3_bypass_corrupts_REG = _RAND_73[1:0];
  _RAND_74 = {1{`RANDOM}};
  s4_rdata_corrupt = _RAND_74[0:0];
  _RAND_75 = {1{`RANDOM}};
  s4_need_data = _RAND_75[0:0];
  _RAND_76 = {1{`RANDOM}};
  s4_need_pb = _RAND_76[0:0];
  _RAND_77 = {1{`RANDOM}};
  s4_pdata_corrupt = _RAND_77[0:0];
  _RAND_78 = {1{`RANDOM}};
  s3_bypass_corrupts_REG_1 = _RAND_78[1:0];
  _RAND_79 = {1{`RANDOM}};
  s4_req_prio_2 = _RAND_79[0:0];
  _RAND_80 = {1{`RANDOM}};
  s4_req_param = _RAND_80[2:0];
  _RAND_81 = {1{`RANDOM}};
  s4_adjusted_opcode = _RAND_81[2:0];
  _RAND_82 = {4{`RANDOM}};
  s4_pdata_data = _RAND_82[127:0];
  _RAND_83 = {1{`RANDOM}};
  s4_pdata_mask = _RAND_83[15:0];
  _RAND_84 = {4{`RANDOM}};
  s4_rdata = _RAND_84[127:0];
  _RAND_85 = {1{`RANDOM}};
  s5_req_size = _RAND_85[2:0];
  _RAND_86 = {1{`RANDOM}};
  s5_req_offset = _RAND_86[5:0];
  _RAND_87 = {1{`RANDOM}};
  s5_req_set = _RAND_87[9:0];
  _RAND_88 = {1{`RANDOM}};
  s5_req_way = _RAND_88[2:0];
  _RAND_89 = {1{`RANDOM}};
  s5_beat = _RAND_89[1:0];
  _RAND_90 = {4{`RANDOM}};
  s5_dat = _RAND_90[127:0];
  _RAND_91 = {1{`RANDOM}};
  s5_corrupt = _RAND_91[0:0];
  _RAND_92 = {1{`RANDOM}};
  s6_req_size = _RAND_92[2:0];
  _RAND_93 = {1{`RANDOM}};
  s6_req_offset = _RAND_93[5:0];
  _RAND_94 = {1{`RANDOM}};
  s6_req_set = _RAND_94[9:0];
  _RAND_95 = {1{`RANDOM}};
  s6_req_way = _RAND_95[2:0];
  _RAND_96 = {1{`RANDOM}};
  s6_beat = _RAND_96[1:0];
  _RAND_97 = {4{`RANDOM}};
  s6_dat = _RAND_97[127:0];
  _RAND_98 = {1{`RANDOM}};
  s6_corrupt = _RAND_98[0:0];
  _RAND_99 = {1{`RANDOM}};
  s7_req_size = _RAND_99[2:0];
  _RAND_100 = {1{`RANDOM}};
  s7_req_offset = _RAND_100[5:0];
  _RAND_101 = {4{`RANDOM}};
  s7_dat = _RAND_101[127:0];
  _RAND_102 = {1{`RANDOM}};
  s7_corrupt = _RAND_102[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    busy = 1'h0;
  end
  if (reset) begin
    s1_block_r = 1'h0;
  end
  if (reset) begin
    s1_counter = 2'h0;
  end
  if (rf_reset) begin
    s1_req_reg_prio_0 = 1'h0;
  end
  if (rf_reset) begin
    s1_req_reg_prio_2 = 1'h0;
  end
  if (rf_reset) begin
    s1_req_reg_opcode = 3'h0;
  end
  if (rf_reset) begin
    s1_req_reg_param = 3'h0;
  end
  if (rf_reset) begin
    s1_req_reg_size = 3'h0;
  end
  if (rf_reset) begin
    s1_req_reg_source = 7'h0;
  end
  if (rf_reset) begin
    s1_req_reg_offset = 6'h0;
  end
  if (rf_reset) begin
    s1_req_reg_put = 5'h0;
  end
  if (rf_reset) begin
    s1_req_reg_set = 10'h0;
  end
  if (rf_reset) begin
    s1_req_reg_sink = 3'h0;
  end
  if (rf_reset) begin
    s1_req_reg_way = 3'h0;
  end
  if (rf_reset) begin
    s1_req_reg_bad = 1'h0;
  end
  if (reset) begin
    s2_full = 1'h0;
  end
  if (reset) begin
    s3_full = 1'h0;
  end
  if (rf_reset) begin
    s3_retires = 1'h0;
  end
  if (reset) begin
    s4_full = 1'h0;
  end
  if (rf_reset) begin
    s4_need_bs = 1'h0;
  end
  if (reset) begin
    s3_valid_d = 1'h0;
  end
  if (reset) begin
    s2_valid_pb = 1'h0;
  end
  if (rf_reset) begin
    s2_req_prio_0 = 1'h0;
  end
  if (rf_reset) begin
    s1_latch_bypass = 1'h0;
  end
  if (rf_reset) begin
    s1_bypass_r = 2'h0;
  end
  if (rf_reset) begin
    s2_req_set = 10'h0;
  end
  if (rf_reset) begin
    s2_req_way = 3'h0;
  end
  if (rf_reset) begin
    s2_beat = 2'h0;
  end
  if (rf_reset) begin
    s2_retires = 1'h0;
  end
  if (rf_reset) begin
    s2_req_size = 3'h0;
  end
  if (rf_reset) begin
    s2_req_offset = 6'h0;
  end
  if (rf_reset) begin
    s3_req_set = 10'h0;
  end
  if (rf_reset) begin
    s3_req_way = 3'h0;
  end
  if (rf_reset) begin
    s3_beat = 2'h0;
  end
  if (rf_reset) begin
    s3_req_size = 3'h0;
  end
  if (rf_reset) begin
    s3_req_offset = 6'h0;
  end
  if (rf_reset) begin
    s4_req_set = 10'h0;
  end
  if (rf_reset) begin
    s4_req_way = 3'h0;
  end
  if (rf_reset) begin
    s4_beat = 2'h0;
  end
  if (rf_reset) begin
    s4_req_size = 3'h0;
  end
  if (rf_reset) begin
    s4_req_offset = 6'h0;
  end
  if (reset) begin
    queue_io_enq_valid_REG = 1'h0;
  end
  if (reset) begin
    queue_io_enq_valid_REG_1 = 1'h0;
  end
  if (rf_reset) begin
    s2_bypass = 2'h0;
  end
  if (rf_reset) begin
    s2_req_prio_2 = 1'h0;
  end
  if (rf_reset) begin
    s2_req_opcode = 3'h0;
  end
  if (rf_reset) begin
    s2_req_param = 3'h0;
  end
  if (rf_reset) begin
    s2_req_source = 7'h0;
  end
  if (rf_reset) begin
    s2_req_put = 5'h0;
  end
  if (rf_reset) begin
    s2_req_sink = 3'h0;
  end
  if (rf_reset) begin
    s2_req_bad = 1'h0;
  end
  if (rf_reset) begin
    s2_last = 1'h0;
  end
  if (rf_reset) begin
    s2_need_r = 1'h0;
  end
  if (rf_reset) begin
    s2_need_pb = 1'h0;
  end
  if (rf_reset) begin
    s2_need_d = 1'h0;
  end
  if (rf_reset) begin
    s2_pdata_r_data = 128'h0;
  end
  if (rf_reset) begin
    s2_pdata_r_mask = 16'h0;
  end
  if (rf_reset) begin
    s2_pdata_r_corrupt = 1'h0;
  end
  if (rf_reset) begin
    s3_bypass = 2'h0;
  end
  if (rf_reset) begin
    s3_req_prio_0 = 1'h0;
  end
  if (rf_reset) begin
    s3_req_prio_2 = 1'h0;
  end
  if (rf_reset) begin
    s3_req_opcode = 3'h0;
  end
  if (rf_reset) begin
    s3_req_param = 3'h0;
  end
  if (rf_reset) begin
    s3_req_source = 7'h0;
  end
  if (rf_reset) begin
    s3_req_sink = 3'h0;
  end
  if (rf_reset) begin
    s3_req_bad = 1'h0;
  end
  if (rf_reset) begin
    s3_pdata_data = 128'h0;
  end
  if (rf_reset) begin
    s3_pdata_mask = 16'h0;
  end
  if (rf_reset) begin
    s3_pdata_corrupt = 1'h0;
  end
  if (rf_reset) begin
    s3_need_pb = 1'h0;
  end
  if (rf_reset) begin
    s3_need_r = 1'h0;
  end
  if (rf_reset) begin
    s3_bypass_data_REG = 2'h0;
  end
  if (rf_reset) begin
    s3_bypass_data_REG_1 = 128'h0;
  end
  if (rf_reset) begin
    s3_bypass_corrupts_REG = 2'h0;
  end
  if (rf_reset) begin
    s4_rdata_corrupt = 1'h0;
  end
  if (rf_reset) begin
    s4_need_data = 1'h0;
  end
  if (rf_reset) begin
    s4_need_pb = 1'h0;
  end
  if (rf_reset) begin
    s4_pdata_corrupt = 1'h0;
  end
  if (rf_reset) begin
    s3_bypass_corrupts_REG_1 = 2'h0;
  end
  if (rf_reset) begin
    s4_req_prio_2 = 1'h0;
  end
  if (rf_reset) begin
    s4_req_param = 3'h0;
  end
  if (rf_reset) begin
    s4_adjusted_opcode = 3'h0;
  end
  if (rf_reset) begin
    s4_pdata_data = 128'h0;
  end
  if (rf_reset) begin
    s4_pdata_mask = 16'h0;
  end
  if (rf_reset) begin
    s4_rdata = 128'h0;
  end
  if (rf_reset) begin
    s5_req_size = 3'h0;
  end
  if (rf_reset) begin
    s5_req_offset = 6'h0;
  end
  if (rf_reset) begin
    s5_req_set = 10'h0;
  end
  if (rf_reset) begin
    s5_req_way = 3'h0;
  end
  if (rf_reset) begin
    s5_beat = 2'h0;
  end
  if (rf_reset) begin
    s5_dat = 128'h0;
  end
  if (rf_reset) begin
    s5_corrupt = 1'h0;
  end
  if (rf_reset) begin
    s6_req_size = 3'h0;
  end
  if (rf_reset) begin
    s6_req_offset = 6'h0;
  end
  if (rf_reset) begin
    s6_req_set = 10'h0;
  end
  if (rf_reset) begin
    s6_req_way = 3'h0;
  end
  if (rf_reset) begin
    s6_beat = 2'h0;
  end
  if (rf_reset) begin
    s6_dat = 128'h0;
  end
  if (rf_reset) begin
    s6_corrupt = 1'h0;
  end
  if (rf_reset) begin
    s7_req_size = 3'h0;
  end
  if (rf_reset) begin
    s7_req_offset = 6'h0;
  end
  if (rf_reset) begin
    s7_dat = 128'h0;
  end
  if (rf_reset) begin
    s7_corrupt = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
