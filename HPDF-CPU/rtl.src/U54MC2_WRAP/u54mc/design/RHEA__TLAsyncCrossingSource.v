//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLAsyncCrossingSource(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_a_ready,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [8:0]  auto_in_a_bits_address,
  input  [31:0] auto_in_a_bits_data,
  input         auto_in_d_ready,
  output        auto_in_d_valid,
  output [2:0]  auto_in_d_bits_opcode,
  output [1:0]  auto_in_d_bits_param,
  output [1:0]  auto_in_d_bits_size,
  output        auto_in_d_bits_source,
  output        auto_in_d_bits_sink,
  output        auto_in_d_bits_denied,
  output [31:0] auto_in_d_bits_data,
  output        auto_in_d_bits_corrupt,
  output [2:0]  auto_out_a_mem_0_opcode,
  output [8:0]  auto_out_a_mem_0_address,
  output [31:0] auto_out_a_mem_0_data,
  input         auto_out_a_ridx,
  output        auto_out_a_widx,
  input  [2:0]  auto_out_d_mem_0_opcode,
  input  [1:0]  auto_out_d_mem_0_size,
  input         auto_out_d_mem_0_source,
  input  [31:0] auto_out_d_mem_0_data,
  output        auto_out_d_ridx,
  input         auto_out_d_widx
);
  wire  bundleOut_0_a_source_rf_reset; // @[AsyncQueue.scala 216:24]
  wire  bundleOut_0_a_source_clock; // @[AsyncQueue.scala 216:24]
  wire  bundleOut_0_a_source_reset; // @[AsyncQueue.scala 216:24]
  wire  bundleOut_0_a_source_io_enq_ready; // @[AsyncQueue.scala 216:24]
  wire  bundleOut_0_a_source_io_enq_valid; // @[AsyncQueue.scala 216:24]
  wire [2:0] bundleOut_0_a_source_io_enq_bits_opcode; // @[AsyncQueue.scala 216:24]
  wire [8:0] bundleOut_0_a_source_io_enq_bits_address; // @[AsyncQueue.scala 216:24]
  wire [31:0] bundleOut_0_a_source_io_enq_bits_data; // @[AsyncQueue.scala 216:24]
  wire [2:0] bundleOut_0_a_source_io_async_mem_0_opcode; // @[AsyncQueue.scala 216:24]
  wire [8:0] bundleOut_0_a_source_io_async_mem_0_address; // @[AsyncQueue.scala 216:24]
  wire [31:0] bundleOut_0_a_source_io_async_mem_0_data; // @[AsyncQueue.scala 216:24]
  wire  bundleOut_0_a_source_io_async_ridx; // @[AsyncQueue.scala 216:24]
  wire  bundleOut_0_a_source_io_async_widx; // @[AsyncQueue.scala 216:24]
  wire  bundleIn_0_d_sink_rf_reset; // @[AsyncQueue.scala 207:22]
  wire  bundleIn_0_d_sink_clock; // @[AsyncQueue.scala 207:22]
  wire  bundleIn_0_d_sink_reset; // @[AsyncQueue.scala 207:22]
  wire  bundleIn_0_d_sink_io_deq_ready; // @[AsyncQueue.scala 207:22]
  wire  bundleIn_0_d_sink_io_deq_valid; // @[AsyncQueue.scala 207:22]
  wire [2:0] bundleIn_0_d_sink_io_deq_bits_opcode; // @[AsyncQueue.scala 207:22]
  wire [1:0] bundleIn_0_d_sink_io_deq_bits_param; // @[AsyncQueue.scala 207:22]
  wire [1:0] bundleIn_0_d_sink_io_deq_bits_size; // @[AsyncQueue.scala 207:22]
  wire  bundleIn_0_d_sink_io_deq_bits_source; // @[AsyncQueue.scala 207:22]
  wire  bundleIn_0_d_sink_io_deq_bits_sink; // @[AsyncQueue.scala 207:22]
  wire  bundleIn_0_d_sink_io_deq_bits_denied; // @[AsyncQueue.scala 207:22]
  wire [31:0] bundleIn_0_d_sink_io_deq_bits_data; // @[AsyncQueue.scala 207:22]
  wire  bundleIn_0_d_sink_io_deq_bits_corrupt; // @[AsyncQueue.scala 207:22]
  wire [2:0] bundleIn_0_d_sink_io_async_mem_0_opcode; // @[AsyncQueue.scala 207:22]
  wire [1:0] bundleIn_0_d_sink_io_async_mem_0_size; // @[AsyncQueue.scala 207:22]
  wire  bundleIn_0_d_sink_io_async_mem_0_source; // @[AsyncQueue.scala 207:22]
  wire [31:0] bundleIn_0_d_sink_io_async_mem_0_data; // @[AsyncQueue.scala 207:22]
  wire  bundleIn_0_d_sink_io_async_ridx; // @[AsyncQueue.scala 207:22]
  wire  bundleIn_0_d_sink_io_async_widx; // @[AsyncQueue.scala 207:22]
  RHEA__AsyncQueueSource bundleOut_0_a_source ( // @[AsyncQueue.scala 216:24]
    .rf_reset(bundleOut_0_a_source_rf_reset),
    .clock(bundleOut_0_a_source_clock),
    .reset(bundleOut_0_a_source_reset),
    .io_enq_ready(bundleOut_0_a_source_io_enq_ready),
    .io_enq_valid(bundleOut_0_a_source_io_enq_valid),
    .io_enq_bits_opcode(bundleOut_0_a_source_io_enq_bits_opcode),
    .io_enq_bits_address(bundleOut_0_a_source_io_enq_bits_address),
    .io_enq_bits_data(bundleOut_0_a_source_io_enq_bits_data),
    .io_async_mem_0_opcode(bundleOut_0_a_source_io_async_mem_0_opcode),
    .io_async_mem_0_address(bundleOut_0_a_source_io_async_mem_0_address),
    .io_async_mem_0_data(bundleOut_0_a_source_io_async_mem_0_data),
    .io_async_ridx(bundleOut_0_a_source_io_async_ridx),
    .io_async_widx(bundleOut_0_a_source_io_async_widx)
  );
  RHEA__AsyncQueueSink bundleIn_0_d_sink ( // @[AsyncQueue.scala 207:22]
    .rf_reset(bundleIn_0_d_sink_rf_reset),
    .clock(bundleIn_0_d_sink_clock),
    .reset(bundleIn_0_d_sink_reset),
    .io_deq_ready(bundleIn_0_d_sink_io_deq_ready),
    .io_deq_valid(bundleIn_0_d_sink_io_deq_valid),
    .io_deq_bits_opcode(bundleIn_0_d_sink_io_deq_bits_opcode),
    .io_deq_bits_param(bundleIn_0_d_sink_io_deq_bits_param),
    .io_deq_bits_size(bundleIn_0_d_sink_io_deq_bits_size),
    .io_deq_bits_source(bundleIn_0_d_sink_io_deq_bits_source),
    .io_deq_bits_sink(bundleIn_0_d_sink_io_deq_bits_sink),
    .io_deq_bits_denied(bundleIn_0_d_sink_io_deq_bits_denied),
    .io_deq_bits_data(bundleIn_0_d_sink_io_deq_bits_data),
    .io_deq_bits_corrupt(bundleIn_0_d_sink_io_deq_bits_corrupt),
    .io_async_mem_0_opcode(bundleIn_0_d_sink_io_async_mem_0_opcode),
    .io_async_mem_0_size(bundleIn_0_d_sink_io_async_mem_0_size),
    .io_async_mem_0_source(bundleIn_0_d_sink_io_async_mem_0_source),
    .io_async_mem_0_data(bundleIn_0_d_sink_io_async_mem_0_data),
    .io_async_ridx(bundleIn_0_d_sink_io_async_ridx),
    .io_async_widx(bundleIn_0_d_sink_io_async_widx)
  );
  assign bundleOut_0_a_source_rf_reset = rf_reset;
  assign bundleIn_0_d_sink_rf_reset = rf_reset;
  assign auto_in_a_ready = bundleOut_0_a_source_io_enq_ready; // @[AsyncCrossing.scala 17:31 AsyncQueue.scala 217:19]
  assign auto_in_d_valid = bundleIn_0_d_sink_io_deq_valid; // @[AsyncCrossing.scala 17:31 AsyncCrossing.scala 26:12]
  assign auto_in_d_bits_opcode = bundleIn_0_d_sink_io_deq_bits_opcode; // @[AsyncCrossing.scala 17:31 AsyncCrossing.scala 26:12]
  assign auto_in_d_bits_param = bundleIn_0_d_sink_io_deq_bits_param; // @[AsyncCrossing.scala 17:31 AsyncCrossing.scala 26:12]
  assign auto_in_d_bits_size = bundleIn_0_d_sink_io_deq_bits_size; // @[AsyncCrossing.scala 17:31 AsyncCrossing.scala 26:12]
  assign auto_in_d_bits_source = bundleIn_0_d_sink_io_deq_bits_source; // @[AsyncCrossing.scala 17:31 AsyncCrossing.scala 26:12]
  assign auto_in_d_bits_sink = bundleIn_0_d_sink_io_deq_bits_sink; // @[AsyncCrossing.scala 17:31 AsyncCrossing.scala 26:12]
  assign auto_in_d_bits_denied = bundleIn_0_d_sink_io_deq_bits_denied; // @[AsyncCrossing.scala 17:31 AsyncCrossing.scala 26:12]
  assign auto_in_d_bits_data = bundleIn_0_d_sink_io_deq_bits_data; // @[AsyncCrossing.scala 17:31 AsyncCrossing.scala 26:12]
  assign auto_in_d_bits_corrupt = bundleIn_0_d_sink_io_deq_bits_corrupt; // @[AsyncCrossing.scala 17:31 AsyncCrossing.scala 26:12]
  assign auto_out_a_mem_0_opcode = bundleOut_0_a_source_io_async_mem_0_opcode; // @[AsyncCrossing.scala 17:31 AsyncCrossing.scala 25:13]
  assign auto_out_a_mem_0_address = bundleOut_0_a_source_io_async_mem_0_address; // @[AsyncCrossing.scala 17:31 AsyncCrossing.scala 25:13]
  assign auto_out_a_mem_0_data = bundleOut_0_a_source_io_async_mem_0_data; // @[AsyncCrossing.scala 17:31 AsyncCrossing.scala 25:13]
  assign auto_out_a_widx = bundleOut_0_a_source_io_async_widx; // @[AsyncCrossing.scala 17:31 AsyncCrossing.scala 25:13]
  assign auto_out_d_ridx = bundleIn_0_d_sink_io_async_ridx; // @[AsyncCrossing.scala 17:31 AsyncQueue.scala 208:19]
  assign bundleOut_0_a_source_clock = clock;
  assign bundleOut_0_a_source_reset = reset;
  assign bundleOut_0_a_source_io_enq_valid = auto_in_a_valid; // @[AsyncCrossing.scala 17:31 LazyModule.scala 388:16]
  assign bundleOut_0_a_source_io_enq_bits_opcode = auto_in_a_bits_opcode; // @[AsyncCrossing.scala 17:31 LazyModule.scala 388:16]
  assign bundleOut_0_a_source_io_enq_bits_address = auto_in_a_bits_address; // @[AsyncCrossing.scala 17:31 LazyModule.scala 388:16]
  assign bundleOut_0_a_source_io_enq_bits_data = auto_in_a_bits_data; // @[AsyncCrossing.scala 17:31 LazyModule.scala 388:16]
  assign bundleOut_0_a_source_io_async_ridx = auto_out_a_ridx; // @[AsyncCrossing.scala 17:31 LazyModule.scala 390:12]
  assign bundleIn_0_d_sink_clock = clock;
  assign bundleIn_0_d_sink_reset = reset;
  assign bundleIn_0_d_sink_io_deq_ready = auto_in_d_ready; // @[AsyncCrossing.scala 17:31 LazyModule.scala 388:16]
  assign bundleIn_0_d_sink_io_async_mem_0_opcode = auto_out_d_mem_0_opcode; // @[AsyncCrossing.scala 17:31 LazyModule.scala 390:12]
  assign bundleIn_0_d_sink_io_async_mem_0_size = auto_out_d_mem_0_size; // @[AsyncCrossing.scala 17:31 LazyModule.scala 390:12]
  assign bundleIn_0_d_sink_io_async_mem_0_source = auto_out_d_mem_0_source; // @[AsyncCrossing.scala 17:31 LazyModule.scala 390:12]
  assign bundleIn_0_d_sink_io_async_mem_0_data = auto_out_d_mem_0_data; // @[AsyncCrossing.scala 17:31 LazyModule.scala 390:12]
  assign bundleIn_0_d_sink_io_async_widx = auto_out_d_widx; // @[AsyncCrossing.scala 17:31 LazyModule.scala 390:12]
endmodule
