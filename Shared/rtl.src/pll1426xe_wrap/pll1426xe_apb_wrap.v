module pll1426xe_apb_wrap ( /*AUTOARG*/
   // Outputs
   APB3__BUS__PRDATA, APB3__BUS__PREADY, APB3__BUS__PSLVERR,
   CLK__FOUT, TEST__PLL__OUT,
   // Inputs
   CLK__BUS, CLK__FIN, RSTN__BUS, APB3__BUS__PADDR, APB3__BUS__PSEL,
   APB3__BUS__PENABLE, APB3__BUS__PWRITE, APB3__BUS__PWDATA,
   TEST__SCAN, TEST__PLL, TEST__PLL__IN
   );

input           CLK__BUS;
input           CLK__FIN;
input           RSTN__BUS;
input  [31:0]   APB3__BUS__PADDR;
input           APB3__BUS__PSEL;
input           APB3__BUS__PENABLE;
input           APB3__BUS__PWRITE;
input  [31:0]   APB3__BUS__PWDATA;
output [31:0]   APB3__BUS__PRDATA;
output          APB3__BUS__PREADY;
output          APB3__BUS__PSLVERR;
output          CLK__FOUT;
input           TEST__SCAN;
input           TEST__PLL;
input  [52:0]   TEST__PLL__IN;
output [7:0]    TEST__PLL__OUT;

wire [5:0]  PLL__P; 
wire [9:0]  PLL__M; 
wire [2:0]  PLL__S; 
wire [1:0]  PLL__ICP; 
wire [3:0]  PLL__RSEL; 
wire [4:0]  PLL__EXTAFC; 
wire [1:0]  PLL__LOCK_CON_DLY; 
wire [1:0]  PLL__LOCK_CON_REV; 
wire [1:0]  PLL__LOCK_CON_IN; 
wire [1:0]  PLL__LOCK_CON_OUT; 
wire        PLL__RESETB; 
wire        PLL__BYPASS; 
wire        PLL__AFC_ENB; 
wire        PLL__FSEL; 
wire        PLL__FEED_EN; 
wire        PLL__LOCK_EN; 
wire        PLL__AFCINIT_SEL; 
wire        PLL__LRD_EN; 
wire        PLL__FOUT_MASK; 
wire        PLL__VCO_BOOST; 
wire        PLL__PBIAS_CTRL_EN; 
wire        PLL__PBIAS_CTRL; 
wire [4:0]  PLL__AFC_CODE;
wire        PLL__LOCK;
wire        PLL__FEED_OUT;

wire [5:0]  REG__BUS__P; 
wire [9:0]  REG__BUS__M; 
wire [2:0]  REG__BUS__S; 
wire [1:0]  REG__BUS__ICP; 
wire [3:0]  REG__BUS__RSEL; 
wire [4:0]  REG__BUS__EXTAFC; 
wire [1:0]  REG__BUS__LOCK_CON_DLY; 
wire [1:0]  REG__BUS__LOCK_CON_REV; 
wire [1:0]  REG__BUS__LOCK_CON_IN; 
wire [1:0]  REG__BUS__LOCK_CON_OUT; 
wire        REG__BUS__RESETB; 
wire        REG__BUS__BYPASS; 
wire        REG__BUS__AFC_ENB; 
wire        REG__BUS__FSEL; 
wire        REG__BUS__FEED_EN; 
wire        REG__BUS__LOCK_EN; 
wire        REG__BUS__AFCINIT_SEL; 
wire        REG__BUS__LRD_EN; 
wire        REG__BUS__FOUT_MASK; 
wire        REG__BUS__VCO_BOOST; 
wire        REG__BUS__PBIAS_CTRL_EN; 
wire        REG__BUS__PBIAS_CTRL; 
wire [4:0]  REG__BUS__AFC_CODE;
wire        REG__BUS__LOCK;

/*AUTOWIRE*/
// Beginning of automatic wires (for undeclared instantiated-module outputs)
wire [4:0]		REG__AFC_CODE;		// From pll1426xe_wrap of pll1426xe_wrap.v
wire			REG__FEED_OUT;		// From pll1426xe_wrap of pll1426xe_wrap.v
wire			REG__LOCK;		// From pll1426xe_wrap of pll1426xe_wrap.v
wire [7:0]		TEST__PLL_OUT;		// From pll1426xe_wrap of pll1426xe_wrap.v
// End of automatics
/*AUTOREG*/
// Beginning of automatic regs (for this module's undeclared outputs)
reg [31:0]		APB3__BUS__PRDATA;
reg			APB3__BUS__PREADY;
reg			APB3__BUS__PSLVERR;
reg			CLK__FOUT;
reg [7:0]		TEST__PLL__OUT;
// End of automatics

pll1426xe_wrap_reg uREG_0 (
    .CLK__BUS                   (CLK__BUS), // input         
    .RSTN__BUS                  (RSTN__BUS), // input         
    .APB3__BUS__PADDR           (APB3__BUS__PADDR), // input [31:0] 
    .APB3__BUS__PENABLE         (APB3__BUS__PENABLE), // input        
    .APB3__BUS__PSEL            (APB3__BUS__PSEL), // input        
    .APB3__BUS__PWDATA          (APB3__BUS__PWDATA), // input [31:0] 
    .APB3__BUS__PWRITE          (APB3__BUS__PWRITE), // input        
    .APB3__BUS__PRDATA          (APB3__BUS__PRDATA), // output  [31:0] 
    .APB3__BUS__PREADY          (APB3__BUS__PREADY), // output         
    .APB3__BUS__PSLVERR         (APB3__BUS__PSLVERR), // output         
    .REG__BUS__P                (REG__BUS__P), // output [5:0]
    .REG__BUS__M                (REG__BUS__M), // output [9:0]
    .REG__BUS__S                (REG__BUS__S), // output [2:0]
    .REG__BUS__ICP              (REG__BUS__ICP), // output [1:0]
    .REG__BUS__RSEL             (REG__BUS__RSEL), // output [3:0]
    .REG__BUS__EXTAFC           (REG__BUS__EXTAFC), // output [4:0]
    .REG__BUS__LOCK_CON_DLY     (REG__BUS__LOCK_CON_DLY), // output [1:0]
    .REG__BUS__LOCK_CON_REV     (REG__BUS__LOCK_CON_REV), // output [1:0]
    .REG__BUS__LOCK_CON_IN      (REG__BUS__LOCK_CON_IN), // output [1:0]
    .REG__BUS__LOCK_CON_OUT     (REG__BUS__LOCK_CON_OUT), // output [1:0]
    .REG__BUS__RESETB           (REG__BUS__RESETB), // output      
    .REG__BUS__BYPASS           (REG__BUS__BYPASS), // output      
    .REG__BUS__AFC_ENB          (REG__BUS__AFC_ENB), // output      
    .REG__BUS__FSEL             (REG__BUS__FSEL), // output      
    .REG__BUS__FEED_EN          (REG__BUS__FEED_EN), // output      
    .REG__BUS__LOCK_EN          (REG__BUS__LOCK_EN), // output      
    .REG__BUS__AFCINIT_SEL      (REG__BUS__AFCINIT_SEL), // output      
    .REG__BUS__LRD_EN           (REG__BUS__LRD_EN), // output      
    .REG__BUS__FOUT_MASK        (REG__BUS__FOUT_MASK), // output      
    .REG__BUS__VCO_BOOST        (REG__BUS__VCO_BOOST), // output      
    .REG__BUS__PBIAS_CTRL_EN    (REG__BUS__PBIAS_CTRL_EN), // output      
    .REG__BUS__PBIAS_CTRL       (REG__BUS__PBIAS_CTRL), // output      
    .REG__BUS__AFC_CODE         (REG__BUS__AFC_CODE), // input [4:0]
    .REG__BUS__LOCK             (REG__BUS__LOCK) // input
);

/*pll1426xe_wrap AUTO_TEMPLATE (
	.REG__AFC_CODE	(REG__AFC_CODE[]),
	.REG__LOCK	    (REG__LOCK[]),
	.REG__FEED_OUT	(REG__FEED_OUT[]),
    .REG__\(.*\)        (REG__BUS__\1[]),
);
*/
pll1426xe_wrap pll1426xe_wrap (/*AUTOINST*/
			       // Outputs
			       .TEST__PLL_OUT	(TEST__PLL_OUT[7:0]),
			       .REG__AFC_CODE	(REG__AFC_CODE[4:0]), // Templated
			       .REG__LOCK	(REG__LOCK),	 // Templated
			       .REG__FEED_OUT	(REG__FEED_OUT), // Templated
			       // Inputs
			       .TEST__SCAN	(TEST__SCAN),
			       .TEST__PLL	(TEST__PLL),
			       .TEST__PLL_IN	(TEST__PLL_IN[52:0]),
			       .REG__P		(REG__BUS__P[5:0]), // Templated
			       .REG__M		(REG__BUS__M[9:0]), // Templated
			       .REG__S		(REG__BUS__S[2:0]), // Templated
			       .REG__ICP	(REG__BUS__ICP[1:0]), // Templated
			       .REG__RSEL	(REG__BUS__RSEL[3:0]), // Templated
			       .REG__EXTAFC	(REG__BUS__EXTAFC[4:0]), // Templated
			       .REG__LOCK_CON_DLY(REG__BUS__LOCK_CON_DLY[1:0]), // Templated
			       .REG__LOCK_CON_REV(REG__BUS__LOCK_CON_REV[1:0]), // Templated
			       .REG__LOCK_CON_IN(REG__BUS__LOCK_CON_IN[1:0]), // Templated
			       .REG__LOCK_CON_OUT(REG__BUS__LOCK_CON_OUT[1:0]), // Templated
			       .REG__RESETB	(REG__BUS__RESETB), // Templated
			       .REG__BYPASS	(REG__BUS__BYPASS), // Templated
			       .REG__AFC_ENB	(REG__BUS__AFC_ENB), // Templated
			       .REG__FSEL	(REG__BUS__FSEL), // Templated
			       .REG__FEED_EN	(REG__BUS__FEED_EN), // Templated
			       .REG__LOCK_EN	(REG__BUS__LOCK_EN), // Templated
			       .REG__AFCINIT_SEL(REG__BUS__AFCINIT_SEL), // Templated
			       .REG__LRD_EN	(REG__BUS__LRD_EN), // Templated
			       .REG__FOUT_MASK	(REG__BUS__FOUT_MASK), // Templated
			       .REG__VCO_BOOST	(REG__BUS__VCO_BOOST), // Templated
			       .REG__PBIAS_CTRL_EN(REG__BUS__PBIAS_CTRL_EN), // Templated
			       .REG__PBIAS_CTRL	(REG__BUS__PBIAS_CTRL)); // Templated
QuasiStaticSynchronizer #(.N(5)) SYNC_AFC_CODE (
    .M_clk(CLK__FIN), .M_rst_n(RSTN__BUS), .DataIn(REG__AFC_CODE), .S_clk(CLK__BUS), .S_rst_n(RSTN__BUS), .DataOut(REG__BUS__AFC_CODE));
QuasiStaticSynchronizer #(.N(1)) SYNC_LOCK (
    .M_clk(CLK__FIN), .M_rst_n(RSTN__BUS), .DataIn(REG__LOCK), .S_clk(CLK__BUS), .S_rst_n(RSTN__BUS), .DataOut(REG__BUS__LOCK));
//SemiDataSynchronizer #(.WIDTH(1)) SYNC_1 (.CLK(CLK__BUS), .RSTN(RSTN__BUS), .DIN(PLL__LOCK),     .DOUT(REG__BUS__LOCK));
endmodule
// Local Variables:
// verilog-library-directories:(".")
// verilog-auto-inst-param-value:t
// verilog-library-files:("./pll1426xe_wrap.v")
// End:
// vim:tabstop=4:shiftwidth=4
