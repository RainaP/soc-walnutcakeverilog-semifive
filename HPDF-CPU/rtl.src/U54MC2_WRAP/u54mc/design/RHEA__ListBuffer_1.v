//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__ListBuffer_1(
  input          rf_reset,
  input          clock,
  input          reset,
  output         io_push_ready,
  input          io_push_valid,
  input          io_push_bits_index,
  input  [127:0] io_push_bits_data_data,
  input          io_push_bits_data_corrupt,
  output [1:0]   io_valid,
  input          io_pop_valid,
  input          io_pop_bits,
  output [127:0] io_data_data,
  output         io_data_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [127:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [127:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [127:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [127:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [127:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [127:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [127:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [127:0] _RAND_28;
  reg [31:0] _RAND_29;
`endif // RANDOMIZE_REG_INIT
  reg [1:0] valid; // @[ListBuffer.scala 49:22]
  reg [2:0] head_0; // @[ListBuffer.scala 50:18]
  reg [2:0] head_1; // @[ListBuffer.scala 50:18]
  wire [2:0] headIntf_pop_head_data = io_pop_bits ? head_1 : head_0; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire  tailIntf_MPORT_3_en = io_push_ready & io_push_valid; // @[Decoupled.scala 40:37]
  wire [1:0] _push_valid_T = valid >> io_push_bits_index; // @[ListBuffer.scala 65:25]
  wire  push_valid = _push_valid_T[0]; // @[ListBuffer.scala 65:25]
  reg [7:0] used; // @[ListBuffer.scala 52:22]
  wire [7:0] _freeOH_T = ~used; // @[ListBuffer.scala 56:25]
  wire [8:0] _freeOH_T_1 = {_freeOH_T, 1'h0}; // @[package.scala 244:48]
  wire [7:0] _freeOH_T_3 = _freeOH_T | _freeOH_T_1[7:0]; // @[package.scala 244:43]
  wire [9:0] _freeOH_T_4 = {_freeOH_T_3, 2'h0}; // @[package.scala 244:48]
  wire [7:0] _freeOH_T_6 = _freeOH_T_3 | _freeOH_T_4[7:0]; // @[package.scala 244:43]
  wire [11:0] _freeOH_T_7 = {_freeOH_T_6, 4'h0}; // @[package.scala 244:48]
  wire [7:0] _freeOH_T_9 = _freeOH_T_6 | _freeOH_T_7[7:0]; // @[package.scala 244:43]
  wire [8:0] _freeOH_T_11 = {_freeOH_T_9, 1'h0}; // @[ListBuffer.scala 56:32]
  wire [8:0] _freeOH_T_12 = ~_freeOH_T_11; // @[ListBuffer.scala 56:16]
  wire [8:0] _GEN_121 = {{1'd0}, _freeOH_T}; // @[ListBuffer.scala 56:38]
  wire [8:0] freeOH = _freeOH_T_12 & _GEN_121; // @[ListBuffer.scala 56:38]
  wire  freeIdx_hi = freeOH[8]; // @[OneHot.scala 30:18]
  wire  freeIdx_hi_1 = |freeIdx_hi; // @[OneHot.scala 32:14]
  wire [7:0] freeIdx_lo = freeOH[7:0]; // @[OneHot.scala 31:18]
  wire [7:0] _GEN_126 = {{7'd0}, freeIdx_hi}; // @[OneHot.scala 32:28]
  wire [7:0] _freeIdx_T = _GEN_126 | freeIdx_lo; // @[OneHot.scala 32:28]
  wire [3:0] freeIdx_hi_2 = _freeIdx_T[7:4]; // @[OneHot.scala 30:18]
  wire  freeIdx_hi_3 = |freeIdx_hi_2; // @[OneHot.scala 32:14]
  wire [3:0] freeIdx_lo_1 = _freeIdx_T[3:0]; // @[OneHot.scala 31:18]
  wire [3:0] _freeIdx_T_1 = freeIdx_hi_2 | freeIdx_lo_1; // @[OneHot.scala 32:28]
  wire [1:0] freeIdx_hi_4 = _freeIdx_T_1[3:2]; // @[OneHot.scala 30:18]
  wire  freeIdx_hi_5 = |freeIdx_hi_4; // @[OneHot.scala 32:14]
  wire [1:0] freeIdx_lo_2 = _freeIdx_T_1[1:0]; // @[OneHot.scala 31:18]
  wire [1:0] _freeIdx_T_2 = freeIdx_hi_4 | freeIdx_lo_2; // @[OneHot.scala 32:28]
  wire  freeIdx_lo_3 = _freeIdx_T_2[1]; // @[CircuitMath.scala 30:8]
  wire [3:0] freeIdx = {freeIdx_hi_1,freeIdx_hi_3,freeIdx_hi_5,freeIdx_lo_3}; // @[Cat.scala 30:58]
  wire [2:0] headIntf_MPORT_2_data = freeIdx[2:0]; // @[ListBuffer.scala 50:18]
  wire [2:0] _GEN_2 = ~io_push_bits_index ? headIntf_MPORT_2_data : head_0; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [2:0] _GEN_3 = io_push_bits_index ? headIntf_MPORT_2_data : head_1; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire  _GEN_127 = push_valid ? 1'h0 : 1'h1; // @[ListBuffer.scala 72:23 ListBuffer.scala 50:18]
  wire  headIntf_MPORT_2_en = tailIntf_MPORT_3_en & _GEN_127; // @[ListBuffer.scala 68:25 ListBuffer.scala 50:18]
  wire [2:0] _GEN_6 = headIntf_MPORT_2_en ? _GEN_2 : head_0; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire [2:0] _GEN_7 = headIntf_MPORT_2_en ? _GEN_3 : head_1; // @[ListBuffer.scala 50:18 ListBuffer.scala 50:18]
  wire  _T_11 = tailIntf_MPORT_3_en & push_valid; // @[ListBuffer.scala 95:48]
  reg [2:0] tail_1; // @[ListBuffer.scala 51:18]
  reg [2:0] tail_0; // @[ListBuffer.scala 51:18]
  wire [2:0] tailIntf_push_tail_data = io_push_bits_index ? tail_1 : tail_0; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  reg [2:0] next_7; // @[ListBuffer.scala 53:18]
  reg [2:0] next_6; // @[ListBuffer.scala 53:18]
  reg [2:0] next_5; // @[ListBuffer.scala 53:18]
  reg [2:0] next_4; // @[ListBuffer.scala 53:18]
  reg [2:0] next_3; // @[ListBuffer.scala 53:18]
  reg [2:0] next_2; // @[ListBuffer.scala 53:18]
  reg [2:0] next_1; // @[ListBuffer.scala 53:18]
  reg [2:0] next_0; // @[ListBuffer.scala 53:18]
  wire [2:0] _GEN_25 = 3'h1 == headIntf_pop_head_data ? next_1 : next_0; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [2:0] _GEN_26 = 3'h2 == headIntf_pop_head_data ? next_2 : _GEN_25; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [2:0] _GEN_27 = 3'h3 == headIntf_pop_head_data ? next_3 : _GEN_26; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [2:0] _GEN_28 = 3'h4 == headIntf_pop_head_data ? next_4 : _GEN_27; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [2:0] _GEN_29 = 3'h5 == headIntf_pop_head_data ? next_5 : _GEN_28; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [2:0] _GEN_30 = 3'h6 == headIntf_pop_head_data ? next_6 : _GEN_29; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [2:0] nextIntf_MPORT_5_data = 3'h7 == headIntf_pop_head_data ? next_7 : _GEN_30; // @[ListBuffer.scala 53:18 ListBuffer.scala 53:18]
  wire [3:0] _T_14 = tailIntf_MPORT_3_en & push_valid & tailIntf_push_tail_data == headIntf_pop_head_data ? freeIdx : {{
    1'd0}, nextIntf_MPORT_5_data}; // @[ListBuffer.scala 95:32]
  wire [2:0] headIntf_MPORT_6_data = _T_14[2:0]; // @[ListBuffer.scala 50:18]
  wire [2:0] tailIntf_MPORT_4_data = io_pop_bits ? tail_1 : tail_0; // @[ListBuffer.scala 51:18 ListBuffer.scala 51:18]
  reg [127:0] data_0_data; // @[ListBuffer.scala 54:18]
  reg  data_0_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_1_data; // @[ListBuffer.scala 54:18]
  reg  data_1_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_2_data; // @[ListBuffer.scala 54:18]
  reg  data_2_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_3_data; // @[ListBuffer.scala 54:18]
  reg  data_3_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_4_data; // @[ListBuffer.scala 54:18]
  reg  data_4_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_5_data; // @[ListBuffer.scala 54:18]
  reg  data_5_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_6_data; // @[ListBuffer.scala 54:18]
  reg  data_6_corrupt; // @[ListBuffer.scala 54:18]
  reg [127:0] data_7_data; // @[ListBuffer.scala 54:18]
  reg  data_7_corrupt; // @[ListBuffer.scala 54:18]
  wire [127:0] _GEN_58 = 3'h1 == headIntf_pop_head_data ? data_1_data : data_0_data; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_59 = 3'h1 == headIntf_pop_head_data ? data_1_corrupt : data_0_corrupt; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_60 = 3'h2 == headIntf_pop_head_data ? data_2_data : _GEN_58; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_61 = 3'h2 == headIntf_pop_head_data ? data_2_corrupt : _GEN_59; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_62 = 3'h3 == headIntf_pop_head_data ? data_3_data : _GEN_60; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_63 = 3'h3 == headIntf_pop_head_data ? data_3_corrupt : _GEN_61; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_64 = 3'h4 == headIntf_pop_head_data ? data_4_data : _GEN_62; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_65 = 3'h4 == headIntf_pop_head_data ? data_4_corrupt : _GEN_63; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_66 = 3'h5 == headIntf_pop_head_data ? data_5_data : _GEN_64; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_67 = 3'h5 == headIntf_pop_head_data ? data_5_corrupt : _GEN_65; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [127:0] _GEN_68 = 3'h6 == headIntf_pop_head_data ? data_6_data : _GEN_66; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire  _GEN_69 = 3'h6 == headIntf_pop_head_data ? data_6_corrupt : _GEN_67; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  wire [1:0] _valid_set_T = 2'h1 << io_push_bits_index; // @[OneHot.scala 65:12]
  wire [1:0] valid_set = tailIntf_MPORT_3_en ? _valid_set_T : 2'h0; // @[ListBuffer.scala 68:25 ListBuffer.scala 69:15]
  wire [8:0] _GEN_131 = tailIntf_MPORT_3_en ? freeOH : 9'h0; // @[ListBuffer.scala 68:25 ListBuffer.scala 70:14]
  wire [7:0] _used_clr_T = 8'h1 << headIntf_pop_head_data; // @[OneHot.scala 65:12]
  wire [1:0] _valid_clr_T = 2'h1 << io_pop_bits; // @[OneHot.scala 65:12]
  wire [1:0] _GEN_150 = headIntf_pop_head_data == tailIntf_MPORT_4_data ? _valid_clr_T : 2'h0; // @[ListBuffer.scala 92:48 ListBuffer.scala 93:17]
  wire [7:0] used_clr = io_pop_valid ? _used_clr_T : 8'h0; // @[ListBuffer.scala 90:24 ListBuffer.scala 91:14]
  wire [1:0] valid_clr = io_pop_valid ? _GEN_150 : 2'h0; // @[ListBuffer.scala 90:24]
  wire [7:0] _used_T = ~used_clr; // @[ListBuffer.scala 100:23]
  wire [7:0] _used_T_1 = used & _used_T; // @[ListBuffer.scala 100:21]
  wire [7:0] used_set = _GEN_131[7:0];
  wire [1:0] _valid_T = ~valid_clr; // @[ListBuffer.scala 101:23]
  wire [1:0] _valid_T_1 = valid & _valid_T; // @[ListBuffer.scala 101:21]
  assign io_push_ready = ~(&used); // @[ListBuffer.scala 67:20]
  assign io_valid = valid; // @[ListBuffer.scala 85:12]
  assign io_data_data = 3'h7 == headIntf_pop_head_data ? data_7_data : _GEN_68; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  assign io_data_corrupt = 3'h7 == headIntf_pop_head_data ? data_7_corrupt : _GEN_69; // @[ListBuffer.scala 54:18 ListBuffer.scala 54:18]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid <= 2'h0;
    end else begin
      valid <= _valid_T_1 | valid_set;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_0 <= 3'h0;
    end else if (io_pop_valid) begin
      if (~io_pop_bits) begin
        head_0 <= headIntf_MPORT_6_data;
      end else begin
        head_0 <= _GEN_6;
      end
    end else begin
      head_0 <= _GEN_6;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      head_1 <= 3'h0;
    end else if (io_pop_valid) begin
      if (io_pop_bits) begin
        head_1 <= headIntf_MPORT_6_data;
      end else begin
        head_1 <= _GEN_7;
      end
    end else begin
      head_1 <= _GEN_7;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      used <= 8'h0;
    end else begin
      used <= _used_T_1 | used_set;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_1 <= 3'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (io_push_bits_index) begin
        tail_1 <= headIntf_MPORT_2_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      tail_0 <= 3'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (~io_push_bits_index) begin
        tail_0 <= headIntf_MPORT_2_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_7 <= 3'h0;
    end else if (_T_11) begin
      if (3'h7 == tailIntf_push_tail_data) begin
        next_7 <= headIntf_MPORT_2_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_6 <= 3'h0;
    end else if (_T_11) begin
      if (3'h6 == tailIntf_push_tail_data) begin
        next_6 <= headIntf_MPORT_2_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_5 <= 3'h0;
    end else if (_T_11) begin
      if (3'h5 == tailIntf_push_tail_data) begin
        next_5 <= headIntf_MPORT_2_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_4 <= 3'h0;
    end else if (_T_11) begin
      if (3'h4 == tailIntf_push_tail_data) begin
        next_4 <= headIntf_MPORT_2_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_3 <= 3'h0;
    end else if (_T_11) begin
      if (3'h3 == tailIntf_push_tail_data) begin
        next_3 <= headIntf_MPORT_2_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_2 <= 3'h0;
    end else if (_T_11) begin
      if (3'h2 == tailIntf_push_tail_data) begin
        next_2 <= headIntf_MPORT_2_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_1 <= 3'h0;
    end else if (_T_11) begin
      if (3'h1 == tailIntf_push_tail_data) begin
        next_1 <= headIntf_MPORT_2_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      next_0 <= 3'h0;
    end else if (_T_11) begin
      if (3'h0 == tailIntf_push_tail_data) begin
        next_0 <= headIntf_MPORT_2_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_0_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h0 == headIntf_MPORT_2_data) begin
        data_0_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_0_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h0 == headIntf_MPORT_2_data) begin
        data_0_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_1_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h1 == headIntf_MPORT_2_data) begin
        data_1_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_1_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h1 == headIntf_MPORT_2_data) begin
        data_1_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_2_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h2 == headIntf_MPORT_2_data) begin
        data_2_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_2_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h2 == headIntf_MPORT_2_data) begin
        data_2_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_3_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h3 == headIntf_MPORT_2_data) begin
        data_3_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_3_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h3 == headIntf_MPORT_2_data) begin
        data_3_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_4_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h4 == headIntf_MPORT_2_data) begin
        data_4_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_4_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h4 == headIntf_MPORT_2_data) begin
        data_4_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_5_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h5 == headIntf_MPORT_2_data) begin
        data_5_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_5_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h5 == headIntf_MPORT_2_data) begin
        data_5_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_6_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h6 == headIntf_MPORT_2_data) begin
        data_6_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_6_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h6 == headIntf_MPORT_2_data) begin
        data_6_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_7_data <= 128'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h7 == headIntf_MPORT_2_data) begin
        data_7_data <= io_push_bits_data_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      data_7_corrupt <= 1'h0;
    end else if (tailIntf_MPORT_3_en) begin
      if (3'h7 == headIntf_MPORT_2_data) begin
        data_7_corrupt <= io_push_bits_data_corrupt;
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  valid = _RAND_0[1:0];
  _RAND_1 = {1{`RANDOM}};
  head_0 = _RAND_1[2:0];
  _RAND_2 = {1{`RANDOM}};
  head_1 = _RAND_2[2:0];
  _RAND_3 = {1{`RANDOM}};
  used = _RAND_3[7:0];
  _RAND_4 = {1{`RANDOM}};
  tail_1 = _RAND_4[2:0];
  _RAND_5 = {1{`RANDOM}};
  tail_0 = _RAND_5[2:0];
  _RAND_6 = {1{`RANDOM}};
  next_7 = _RAND_6[2:0];
  _RAND_7 = {1{`RANDOM}};
  next_6 = _RAND_7[2:0];
  _RAND_8 = {1{`RANDOM}};
  next_5 = _RAND_8[2:0];
  _RAND_9 = {1{`RANDOM}};
  next_4 = _RAND_9[2:0];
  _RAND_10 = {1{`RANDOM}};
  next_3 = _RAND_10[2:0];
  _RAND_11 = {1{`RANDOM}};
  next_2 = _RAND_11[2:0];
  _RAND_12 = {1{`RANDOM}};
  next_1 = _RAND_12[2:0];
  _RAND_13 = {1{`RANDOM}};
  next_0 = _RAND_13[2:0];
  _RAND_14 = {4{`RANDOM}};
  data_0_data = _RAND_14[127:0];
  _RAND_15 = {1{`RANDOM}};
  data_0_corrupt = _RAND_15[0:0];
  _RAND_16 = {4{`RANDOM}};
  data_1_data = _RAND_16[127:0];
  _RAND_17 = {1{`RANDOM}};
  data_1_corrupt = _RAND_17[0:0];
  _RAND_18 = {4{`RANDOM}};
  data_2_data = _RAND_18[127:0];
  _RAND_19 = {1{`RANDOM}};
  data_2_corrupt = _RAND_19[0:0];
  _RAND_20 = {4{`RANDOM}};
  data_3_data = _RAND_20[127:0];
  _RAND_21 = {1{`RANDOM}};
  data_3_corrupt = _RAND_21[0:0];
  _RAND_22 = {4{`RANDOM}};
  data_4_data = _RAND_22[127:0];
  _RAND_23 = {1{`RANDOM}};
  data_4_corrupt = _RAND_23[0:0];
  _RAND_24 = {4{`RANDOM}};
  data_5_data = _RAND_24[127:0];
  _RAND_25 = {1{`RANDOM}};
  data_5_corrupt = _RAND_25[0:0];
  _RAND_26 = {4{`RANDOM}};
  data_6_data = _RAND_26[127:0];
  _RAND_27 = {1{`RANDOM}};
  data_6_corrupt = _RAND_27[0:0];
  _RAND_28 = {4{`RANDOM}};
  data_7_data = _RAND_28[127:0];
  _RAND_29 = {1{`RANDOM}};
  data_7_corrupt = _RAND_29[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    valid = 2'h0;
  end
  if (rf_reset) begin
    head_0 = 3'h0;
  end
  if (rf_reset) begin
    head_1 = 3'h0;
  end
  if (reset) begin
    used = 8'h0;
  end
  if (rf_reset) begin
    tail_1 = 3'h0;
  end
  if (rf_reset) begin
    tail_0 = 3'h0;
  end
  if (rf_reset) begin
    next_7 = 3'h0;
  end
  if (rf_reset) begin
    next_6 = 3'h0;
  end
  if (rf_reset) begin
    next_5 = 3'h0;
  end
  if (rf_reset) begin
    next_4 = 3'h0;
  end
  if (rf_reset) begin
    next_3 = 3'h0;
  end
  if (rf_reset) begin
    next_2 = 3'h0;
  end
  if (rf_reset) begin
    next_1 = 3'h0;
  end
  if (rf_reset) begin
    next_0 = 3'h0;
  end
  if (rf_reset) begin
    data_0_data = 128'h0;
  end
  if (rf_reset) begin
    data_0_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_1_data = 128'h0;
  end
  if (rf_reset) begin
    data_1_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_2_data = 128'h0;
  end
  if (rf_reset) begin
    data_2_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_3_data = 128'h0;
  end
  if (rf_reset) begin
    data_3_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_4_data = 128'h0;
  end
  if (rf_reset) begin
    data_4_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_5_data = 128'h0;
  end
  if (rf_reset) begin
    data_5_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_6_data = 128'h0;
  end
  if (rf_reset) begin
    data_6_corrupt = 1'h0;
  end
  if (rf_reset) begin
    data_7_data = 128'h0;
  end
  if (rf_reset) begin
    data_7_corrupt = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
