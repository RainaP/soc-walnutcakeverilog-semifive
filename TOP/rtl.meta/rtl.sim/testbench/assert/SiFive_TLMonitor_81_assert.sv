//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_81_assert(
  input         clock,
  input         reset,
  input         io_in_a_ready,
  input         io_in_a_valid,
  input  [31:0] io_in_a_bits_address,
  input         io_in_d_valid,
  input         io_in_d_bits_denied
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire [31:0] _T_17; // @[Edges.scala 22:16]
  wire  _T_18; // @[Edges.scala 22:24]
  wire [31:0] _T_72; // @[Parameters.scala 137:31]
  wire [32:0] _T_73; // @[Parameters.scala 137:49]
  wire [32:0] _T_75; // @[Parameters.scala 137:52]
  wire  _T_76; // @[Parameters.scala 137:67]
  wire  _T_93; // @[Monitor.scala 44:11]
  wire  _T_94; // @[Monitor.scala 44:11]
  wire  _T_191; // @[Monitor.scala 44:11]
  wire  _T_192; // @[Monitor.scala 44:11]
  wire  _T_475; // @[Decoupled.scala 40:37]
  reg  _T_484; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire  _T_486; // @[Edges.scala 231:28]
  wire  _T_487; // @[Edges.scala 232:25]
  reg [31:0] _T_499; // @[Monitor.scala 385:22]
  reg [31:0] _RAND_1;
  wire  _T_500; // @[Monitor.scala 386:22]
  wire  _T_501; // @[Monitor.scala 386:19]
  wire  _T_518; // @[Monitor.scala 391:32]
  wire  _T_520; // @[Monitor.scala 44:11]
  wire  _T_521; // @[Monitor.scala 44:11]
  wire  _T_523; // @[Monitor.scala 393:20]
  reg  _T_532; // @[Edges.scala 230:27]
  reg [31:0] _RAND_2;
  wire  _T_534; // @[Edges.scala 231:28]
  wire  _T_535; // @[Edges.scala 232:25]
  reg  _T_548; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_3;
  wire  _T_549; // @[Monitor.scala 538:22]
  wire  _T_550; // @[Monitor.scala 538:19]
  wire  _T_571; // @[Monitor.scala 544:29]
  wire  _T_573; // @[Monitor.scala 51:11]
  wire  _T_574; // @[Monitor.scala 51:11]
  wire  _T_576; // @[Monitor.scala 546:20]
  reg [1:0] _T_577; // @[Monitor.scala 568:27]
  reg [31:0] _RAND_4;
  reg  _T_587; // @[Edges.scala 230:27]
  reg [31:0] _RAND_5;
  wire  _T_589; // @[Edges.scala 231:28]
  wire  _T_590; // @[Edges.scala 232:25]
  reg  _T_606; // @[Edges.scala 230:27]
  reg [31:0] _RAND_6;
  wire  _T_608; // @[Edges.scala 231:28]
  wire  _T_609; // @[Edges.scala 232:25]
  wire  _T_619; // @[Monitor.scala 574:27]
  wire  _T_624; // @[Monitor.scala 576:14]
  wire  _T_626; // @[Monitor.scala 576:13]
  wire  _T_627; // @[Monitor.scala 576:13]
  wire [1:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_631; // @[Monitor.scala 581:27]
  wire [1:0] _T_636; // @[Monitor.scala 583:21]
  wire  _T_640; // @[Monitor.scala 51:11]
  wire  _T_641; // @[Monitor.scala 51:11]
  wire [1:0] _GEN_16; // @[Monitor.scala 581:91]
  wire [1:0] _T_642; // @[Monitor.scala 590:27]
  wire [1:0] _T_643; // @[Monitor.scala 590:38]
  wire [1:0] _T_644; // @[Monitor.scala 590:36]
  reg [31:0] _T_645; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_7;
  wire  _T_646; // @[Monitor.scala 595:23]
  wire  _T_647; // @[Monitor.scala 595:13]
  wire  _T_648; // @[Monitor.scala 595:36]
  wire  _T_649; // @[Monitor.scala 595:27]
  wire  _T_650; // @[Monitor.scala 595:56]
  wire  _T_651; // @[Monitor.scala 595:44]
  wire  _T_653; // @[Monitor.scala 595:12]
  wire  _T_654; // @[Monitor.scala 595:12]
  wire [31:0] _T_656; // @[Monitor.scala 597:26]
  wire  _T_659; // @[Monitor.scala 598:27]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_17 = io_in_a_bits_address & 32'h3; // @[Edges.scala 22:16]
  assign _T_18 = _T_17 == 32'h0; // @[Edges.scala 22:24]
  assign _T_72 = io_in_a_bits_address ^ 32'h80000000; // @[Parameters.scala 137:31]
  assign _T_73 = {1'b0,$signed(_T_72)}; // @[Parameters.scala 137:49]
  assign _T_75 = $signed(_T_73) & -33'sh100; // @[Parameters.scala 137:52]
  assign _T_76 = $signed(_T_75) == 33'sh0; // @[Parameters.scala 137:67]
  assign _T_93 = _T_18 | reset; // @[Monitor.scala 44:11]
  assign _T_94 = ~_T_93; // @[Monitor.scala 44:11]
  assign _T_191 = _T_76 | reset; // @[Monitor.scala 44:11]
  assign _T_192 = ~_T_191; // @[Monitor.scala 44:11]
  assign _T_475 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_486 = _T_484 - 1'h1; // @[Edges.scala 231:28]
  assign _T_487 = ~_T_484; // @[Edges.scala 232:25]
  assign _T_500 = ~_T_487; // @[Monitor.scala 386:22]
  assign _T_501 = io_in_a_valid & _T_500; // @[Monitor.scala 386:19]
  assign _T_518 = io_in_a_bits_address == _T_499; // @[Monitor.scala 391:32]
  assign _T_520 = _T_518 | reset; // @[Monitor.scala 44:11]
  assign _T_521 = ~_T_520; // @[Monitor.scala 44:11]
  assign _T_523 = _T_475 & _T_487; // @[Monitor.scala 393:20]
  assign _T_534 = _T_532 - 1'h1; // @[Edges.scala 231:28]
  assign _T_535 = ~_T_532; // @[Edges.scala 232:25]
  assign _T_549 = ~_T_535; // @[Monitor.scala 538:22]
  assign _T_550 = io_in_d_valid & _T_549; // @[Monitor.scala 538:19]
  assign _T_571 = io_in_d_bits_denied == _T_548; // @[Monitor.scala 544:29]
  assign _T_573 = _T_571 | reset; // @[Monitor.scala 51:11]
  assign _T_574 = ~_T_573; // @[Monitor.scala 51:11]
  assign _T_576 = io_in_d_valid & _T_535; // @[Monitor.scala 546:20]
  assign _T_589 = _T_587 - 1'h1; // @[Edges.scala 231:28]
  assign _T_590 = ~_T_587; // @[Edges.scala 232:25]
  assign _T_608 = _T_606 - 1'h1; // @[Edges.scala 231:28]
  assign _T_609 = ~_T_606; // @[Edges.scala 232:25]
  assign _T_619 = _T_475 & _T_590; // @[Monitor.scala 574:27]
  assign _T_624 = ~_T_577[0]; // @[Monitor.scala 576:14]
  assign _T_626 = _T_624 | reset; // @[Monitor.scala 576:13]
  assign _T_627 = ~_T_626; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_619 ? 2'h1 : 2'h0; // @[Monitor.scala 574:72]
  assign _T_631 = io_in_d_valid & _T_609; // @[Monitor.scala 581:27]
  assign _T_636 = _GEN_15 | _T_577; // @[Monitor.scala 583:21]
  assign _T_640 = _T_636[0] | reset; // @[Monitor.scala 51:11]
  assign _T_641 = ~_T_640; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_631 ? 2'h1 : 2'h0; // @[Monitor.scala 581:91]
  assign _T_642 = _T_577 | _GEN_15; // @[Monitor.scala 590:27]
  assign _T_643 = ~_GEN_16; // @[Monitor.scala 590:38]
  assign _T_644 = _T_642 & _T_643; // @[Monitor.scala 590:36]
  assign _T_646 = _T_577 != 2'h0; // @[Monitor.scala 595:23]
  assign _T_647 = ~_T_646; // @[Monitor.scala 595:13]
  assign _T_648 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_649 = _T_647 | _T_648; // @[Monitor.scala 595:27]
  assign _T_650 = _T_645 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_651 = _T_649 | _T_650; // @[Monitor.scala 595:44]
  assign _T_653 = _T_651 | reset; // @[Monitor.scala 595:12]
  assign _T_654 = ~_T_653; // @[Monitor.scala 595:12]
  assign _T_656 = _T_645 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_659 = _T_475 | io_in_d_valid; // @[Monitor.scala 598:27]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_484 = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_499 = _RAND_1[31:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_532 = _RAND_2[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_548 = _RAND_3[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_577 = _RAND_4[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  _T_587 = _RAND_5[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_606 = _RAND_6[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_645 = _RAND_7[31:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_484 <= 1'h0;
    end else if (_T_475) begin
      if (_T_487) begin
        _T_484 <= 1'h0;
      end else begin
        _T_484 <= _T_486;
      end
    end
    if (_T_523) begin
      _T_499 <= io_in_a_bits_address;
    end
    if (reset) begin
      _T_532 <= 1'h0;
    end else if (io_in_d_valid) begin
      if (_T_535) begin
        _T_532 <= 1'h0;
      end else begin
        _T_532 <= _T_534;
      end
    end
    if (_T_576) begin
      _T_548 <= io_in_d_bits_denied;
    end
    if (reset) begin
      _T_577 <= 2'h0;
    end else begin
      _T_577 <= _T_644;
    end
    if (reset) begin
      _T_587 <= 1'h0;
    end else if (_T_475) begin
      if (_T_590) begin
        _T_587 <= 1'h0;
      end else begin
        _T_587 <= _T_589;
      end
    end
    if (reset) begin
      _T_606 <= 1'h0;
    end else if (io_in_d_valid) begin
      if (_T_609) begin
        _T_606 <= 1'h0;
      end else begin
        _T_606 <= _T_608;
      end
    end
    if (reset) begin
      _T_645 <= 32'h0;
    end else if (_T_659) begin
      _T_645 <= 32'h0;
    end else begin
      _T_645 <= _T_656;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_a_valid & _T_192) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at Periphery.scala:73:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_a_valid & _T_192) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_a_valid & _T_94) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at Periphery.scala:73:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_a_valid & _T_94) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_501 & _T_521) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at Periphery.scala:73:16)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_501 & _T_521) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_550 & _T_574) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at Periphery.scala:73:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_550 & _T_574) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_619 & _T_627) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at Periphery.scala:73:16)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_619 & _T_627) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_631 & _T_641) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at Periphery.scala:73:16)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_631 & _T_641) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_654) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at Periphery.scala:73:16)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_654) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
