//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_Rocket_assert(
  input         reset,
  input  [1:0]  io_hartid,
  input         rocket_clock_gate_out,
  input         ex_ctrl_rxs2,
  input         wb_ctrl_wfd,
  input         wb_ctrl_wxd,
  input         ex_pc_valid,
  input         id_ctrl_rxs1,
  input         _T_351,
  input  [31:0] _T_352,
  input         id_ctrl_rfs3,
  input  [4:0]  wb_waddr,
  input         clock_en,
  input         _T_748,
  input  [63:0] rf_wdata,
  input         mem_pc_valid,
  input         _T_1005,
  input         _T_1600,
  input         coreMonitorBundle_wrenx,
  input  [4:0]  coreMonitorBundle_rd0src,
  input  [63:0] coreMonitorBundle_rd0val,
  input  [4:0]  coreMonitorBundle_rd1src,
  input  [63:0] coreMonitorBundle_rd1val,
  input  [31:0] coreMonitorBundle_timer,
  input         coreMonitorBundle_valid,
  input  [63:0] coreMonitorBundle_pc,
  input  [31:0] coreMonitorBundle_inst,
  input         csr_io_trace_0_valid,
  input  [63:0] csr_io_time,
  input         d_unpipelined,
  input         d_pipelined,
  input         d_multicycle,
  input  [31:0] ibuf_io_inst_0_bits_inst_bits
);
  wire  PlusArgTimeout_clock; // @[PlusArg.scala 53:11]
  wire  PlusArgTimeout_reset; // @[PlusArg.scala 53:11]
  wire [31:0] PlusArgTimeout_io_count; // @[PlusArg.scala 53:11]
  reg  ex_ctrl_rxs1; // @[RocketCore.scala 182:20]
  reg [31:0] _RAND_0;
  reg  ex_ctrl_rfs1; // @[RocketCore.scala 182:20]
  reg [31:0] _RAND_1;
  reg  ex_ctrl_rfs2; // @[RocketCore.scala 182:20]
  reg [31:0] _RAND_2;
  reg  mem_ctrl_rxs2; // @[RocketCore.scala 183:21]
  reg [31:0] _RAND_3;
  reg  mem_ctrl_rxs1; // @[RocketCore.scala 183:21]
  reg [31:0] _RAND_4;
  reg  mem_ctrl_rfs1; // @[RocketCore.scala 183:21]
  reg [31:0] _RAND_5;
  reg  mem_ctrl_rfs2; // @[RocketCore.scala 183:21]
  reg [31:0] _RAND_6;
  reg  wb_ctrl_rxs2; // @[RocketCore.scala 184:20]
  reg [31:0] _RAND_7;
  reg  wb_ctrl_rxs1; // @[RocketCore.scala 184:20]
  reg [31:0] _RAND_8;
  reg  wb_ctrl_rfs1; // @[RocketCore.scala 184:20]
  reg [31:0] _RAND_9;
  reg  wb_ctrl_rfs2; // @[RocketCore.scala 184:20]
  reg [31:0] _RAND_10;
  wire  _T_353; // @[Decode.scala 14:121]
  wire  _T_357; // @[Decode.scala 15:30]
  wire  id_ctrl_rfs1; // @[Decode.scala 15:30]
  wire [31:0] _T_359; // @[Decode.scala 14:65]
  wire  _T_360; // @[Decode.scala 14:121]
  wire [31:0] _T_361; // @[Decode.scala 14:65]
  wire  _T_362; // @[Decode.scala 14:121]
  wire [31:0] _T_363; // @[Decode.scala 14:65]
  wire  _T_364; // @[Decode.scala 14:121]
  wire  _T_366; // @[Decode.scala 15:30]
  wire  _T_367; // @[Decode.scala 15:30]
  wire  id_ctrl_rfs2; // @[Decode.scala 15:30]
  wire [1:0] _T_535; // @[Bitwise.scala 47:55]
  wire [1:0] _GEN_316; // @[Bitwise.scala 47:55]
  wire [2:0] _T_537; // @[Bitwise.scala 47:55]
  wire  _T_539; // @[RocketCore.scala 284:83]
  wire  _T_541; // @[RocketCore.scala 284:11]
  wire  _T_542; // @[RocketCore.scala 284:11]
  wire  _T_623; // @[RocketCore.scala 968:12]
  wire [4:0] _T_642; // @[RocketCore.scala 351:36]
  wire [63:0] _GEN_292; // @[RocketCore.scala 1016:29]
  wire  _T_1615; // @[RocketCore.scala 865:12]
  wire  _T_1616; // @[RocketCore.scala 865:58]
  wire  _T_1618; // @[RocketCore.scala 865:11]
  wire  _T_1619; // @[RocketCore.scala 865:11]
  wire  _T_2198; // @[RocketCore.scala 923:26]
  wire [4:0] _T_2199; // @[RocketCore.scala 923:13]
  wire [63:0] _T_2200; // @[RocketCore.scala 924:13]
  wire  _T_2201; // @[RocketCore.scala 926:27]
  wire [4:0] _T_2202; // @[RocketCore.scala 926:13]
  wire [63:0] _T_2204; // @[RocketCore.scala 927:13]
  wire  _T_2205; // @[RocketCore.scala 928:27]
  wire [4:0] _T_2206; // @[RocketCore.scala 928:13]
  wire [63:0] _T_2208; // @[RocketCore.scala 929:13]
  SiFive_PlusArgTimeout_assert PlusArgTimeout ( // @[PlusArg.scala 53:11]
    .clock(PlusArgTimeout_clock),
    .reset(PlusArgTimeout_reset),
    .io_count(PlusArgTimeout_io_count)
  );
  assign _T_353 = _T_352 == 32'h40; // @[Decode.scala 14:121]
  assign _T_357 = _T_351 | _T_353; // @[Decode.scala 15:30]
  assign id_ctrl_rfs1 = _T_357 | id_ctrl_rfs3; // @[Decode.scala 15:30]
  assign _T_359 = ibuf_io_inst_0_bits_inst_bits & 32'h7c; // @[Decode.scala 14:65]
  assign _T_360 = _T_359 == 32'h24; // @[Decode.scala 14:121]
  assign _T_361 = ibuf_io_inst_0_bits_inst_bits & 32'h40000060; // @[Decode.scala 14:65]
  assign _T_362 = _T_361 == 32'h40; // @[Decode.scala 14:121]
  assign _T_363 = ibuf_io_inst_0_bits_inst_bits & 32'h90000060; // @[Decode.scala 14:65]
  assign _T_364 = _T_363 == 32'h10000040; // @[Decode.scala 14:121]
  assign _T_366 = _T_360 | _T_362; // @[Decode.scala 15:30]
  assign _T_367 = _T_366 | id_ctrl_rfs3; // @[Decode.scala 15:30]
  assign id_ctrl_rfs2 = _T_367 | _T_364; // @[Decode.scala 15:30]
  assign _T_535 = d_pipelined + d_multicycle; // @[Bitwise.scala 47:55]
  assign _GEN_316 = {{1'd0}, d_unpipelined}; // @[Bitwise.scala 47:55]
  assign _T_537 = _GEN_316 + _T_535; // @[Bitwise.scala 47:55]
  assign _T_539 = _T_537[1:0] <= 2'h1; // @[RocketCore.scala 284:83]
  assign _T_541 = _T_539 | reset; // @[RocketCore.scala 284:11]
  assign _T_542 = ~_T_541; // @[RocketCore.scala 284:11]
  assign _T_623 = ~reset; // @[RocketCore.scala 968:12]
  assign _T_642 = wb_waddr; // @[RocketCore.scala 351:36]
  assign _GEN_292 = rf_wdata; // @[RocketCore.scala 1016:29]
  assign _T_1615 = ~_T_1600; // @[RocketCore.scala 865:12]
  assign _T_1616 = _T_1615 | clock_en; // @[RocketCore.scala 865:58]
  assign _T_1618 = _T_1616 | reset; // @[RocketCore.scala 865:11]
  assign _T_1619 = ~_T_1618; // @[RocketCore.scala 865:11]
  assign _T_2198 = wb_ctrl_wxd | wb_ctrl_wfd; // @[RocketCore.scala 923:26]
  assign _T_2199 = _T_2198 ? _T_642 : 5'h0; // @[RocketCore.scala 923:13]
  assign _T_2200 = coreMonitorBundle_wrenx ? _GEN_292 : 64'h0; // @[RocketCore.scala 924:13]
  assign _T_2201 = wb_ctrl_rxs1 | wb_ctrl_rfs1; // @[RocketCore.scala 926:27]
  assign _T_2202 = _T_2201 ? coreMonitorBundle_rd0src : 5'h0; // @[RocketCore.scala 926:13]
  assign _T_2204 = _T_2201 ? coreMonitorBundle_rd0val : 64'h0; // @[RocketCore.scala 927:13]
  assign _T_2205 = wb_ctrl_rxs2 | wb_ctrl_rfs2; // @[RocketCore.scala 928:27]
  assign _T_2206 = _T_2205 ? coreMonitorBundle_rd1src : 5'h0; // @[RocketCore.scala 928:13]
  assign _T_2208 = _T_2205 ? coreMonitorBundle_rd1val : 64'h0; // @[RocketCore.scala 929:13]
  assign PlusArgTimeout_clock = rocket_clock_gate_out;
  assign PlusArgTimeout_reset = reset;
  assign PlusArgTimeout_io_count = csr_io_time[31:0]; // @[PlusArg.scala 53:82]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ex_ctrl_rxs1 = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  ex_ctrl_rfs1 = _RAND_1[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  ex_ctrl_rfs2 = _RAND_2[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  mem_ctrl_rxs2 = _RAND_3[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  mem_ctrl_rxs1 = _RAND_4[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  mem_ctrl_rfs1 = _RAND_5[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  mem_ctrl_rfs2 = _RAND_6[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  wb_ctrl_rxs2 = _RAND_7[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  wb_ctrl_rxs1 = _RAND_8[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  wb_ctrl_rfs1 = _RAND_9[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  wb_ctrl_rfs2 = _RAND_10[0:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge rocket_clock_gate_out) begin
    if (_T_748) begin
      ex_ctrl_rxs1 <= id_ctrl_rxs1;
    end
    if (_T_748) begin
      ex_ctrl_rfs1 <= id_ctrl_rfs1;
    end
    if (_T_748) begin
      ex_ctrl_rfs2 <= id_ctrl_rfs2;
    end
    if (!(_T_1005)) begin
      if (ex_pc_valid) begin
        mem_ctrl_rxs2 <= ex_ctrl_rxs2;
      end
    end
    if (!(_T_1005)) begin
      if (ex_pc_valid) begin
        mem_ctrl_rxs1 <= ex_ctrl_rxs1;
      end
    end
    if (!(_T_1005)) begin
      if (ex_pc_valid) begin
        mem_ctrl_rfs1 <= ex_ctrl_rfs1;
      end
    end
    if (!(_T_1005)) begin
      if (ex_pc_valid) begin
        mem_ctrl_rfs2 <= ex_ctrl_rfs2;
      end
    end
    if (mem_pc_valid) begin
      wb_ctrl_rxs2 <= mem_ctrl_rxs2;
    end
    if (mem_pc_valid) begin
      wb_ctrl_rxs1 <= mem_ctrl_rxs1;
    end
    if (mem_pc_valid) begin
      wb_ctrl_rfs1 <= mem_ctrl_rfs1;
    end
    if (mem_pc_valid) begin
      wb_ctrl_rfs2 <= mem_ctrl_rfs2;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_542) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at RocketCore.scala:284 assert(PopCount(d.io.unpipelined :: d.io.pipelined :: d.io.multicycle :: Nil) <= 1)\n"); // @[RocketCore.scala 284:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_542) begin
          $fatal; // @[RocketCore.scala 284:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1619) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at RocketCore.scala:865 assert(!(ex_pc_valid || mem_pc_valid || wb_pc_valid) || clock_en)\n"); // @[RocketCore.scala 865:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1619) begin
          $fatal; // @[RocketCore.scala 865:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (csr_io_trace_0_valid & _T_623) begin
          $fwrite(32'h80000002,"C%d: %d [%d] pc=[%x] W[r%d=%x][%d] R[r%d=%x] R[r%d=%x] inst=[%x] DASM(%x)\n",io_hartid,coreMonitorBundle_timer,coreMonitorBundle_valid,coreMonitorBundle_pc,_T_2199,_T_2200,coreMonitorBundle_wrenx,_T_2202,_T_2204,_T_2206,_T_2208,coreMonitorBundle_inst,coreMonitorBundle_inst); // @[RocketCore.scala 920:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
