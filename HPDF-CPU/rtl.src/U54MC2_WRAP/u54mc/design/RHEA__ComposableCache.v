//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__ComposableCache(
  input          rf_reset,
  input          clock,
  input          reset,
  output         auto_in_4_a_ready,
  input          auto_in_4_a_valid,
  input  [2:0]   auto_in_4_a_bits_opcode,
  input  [2:0]   auto_in_4_a_bits_param,
  input  [1:0]   auto_in_4_a_bits_size,
  input  [10:0]  auto_in_4_a_bits_source,
  input  [25:0]  auto_in_4_a_bits_address,
  input  [7:0]   auto_in_4_a_bits_mask,
  input  [63:0]  auto_in_4_a_bits_data,
  input          auto_in_4_a_bits_corrupt,
  input          auto_in_4_d_ready,
  output         auto_in_4_d_valid,
  output [2:0]   auto_in_4_d_bits_opcode,
  output [1:0]   auto_in_4_d_bits_size,
  output [10:0]  auto_in_4_d_bits_source,
  output [63:0]  auto_in_4_d_bits_data,
  output         auto_side_in_a_ready,
  input          auto_side_in_a_valid,
  input  [2:0]   auto_side_in_a_bits_opcode,
  input  [2:0]   auto_side_in_a_bits_param,
  input  [2:0]   auto_side_in_a_bits_size,
  input  [9:0]   auto_side_in_a_bits_source,
  input  [27:0]  auto_side_in_a_bits_address,
  input  [15:0]  auto_side_in_a_bits_mask,
  input  [127:0] auto_side_in_a_bits_data,
  input          auto_side_in_a_bits_corrupt,
  input          auto_side_in_d_ready,
  output         auto_side_in_d_valid,
  output [2:0]   auto_side_in_d_bits_opcode,
  output [2:0]   auto_side_in_d_bits_size,
  output [9:0]   auto_side_in_d_bits_source,
  output [127:0] auto_side_in_d_bits_data,
  output         auto_side_in_d_bits_corrupt,
  output         auto_in_3_a_ready,
  input          auto_in_3_a_valid,
  input  [2:0]   auto_in_3_a_bits_opcode,
  input  [2:0]   auto_in_3_a_bits_param,
  input  [2:0]   auto_in_3_a_bits_size,
  input  [6:0]   auto_in_3_a_bits_source,
  input  [35:0]  auto_in_3_a_bits_address,
  input  [15:0]  auto_in_3_a_bits_mask,
  input  [127:0] auto_in_3_a_bits_data,
  input          auto_in_3_a_bits_corrupt,
  input          auto_in_3_b_ready,
  output         auto_in_3_b_valid,
  output [2:0]   auto_in_3_b_bits_opcode,
  output [1:0]   auto_in_3_b_bits_param,
  output [2:0]   auto_in_3_b_bits_size,
  output [6:0]   auto_in_3_b_bits_source,
  output [35:0]  auto_in_3_b_bits_address,
  output [15:0]  auto_in_3_b_bits_mask,
  output [127:0] auto_in_3_b_bits_data,
  output         auto_in_3_b_bits_corrupt,
  output         auto_in_3_c_ready,
  input          auto_in_3_c_valid,
  input  [2:0]   auto_in_3_c_bits_opcode,
  input  [2:0]   auto_in_3_c_bits_param,
  input  [2:0]   auto_in_3_c_bits_size,
  input  [6:0]   auto_in_3_c_bits_source,
  input  [35:0]  auto_in_3_c_bits_address,
  input  [127:0] auto_in_3_c_bits_data,
  input          auto_in_3_c_bits_corrupt,
  input          auto_in_3_d_ready,
  output         auto_in_3_d_valid,
  output [2:0]   auto_in_3_d_bits_opcode,
  output [1:0]   auto_in_3_d_bits_param,
  output [2:0]   auto_in_3_d_bits_size,
  output [6:0]   auto_in_3_d_bits_source,
  output [2:0]   auto_in_3_d_bits_sink,
  output         auto_in_3_d_bits_denied,
  output [127:0] auto_in_3_d_bits_data,
  output         auto_in_3_d_bits_corrupt,
  output         auto_in_3_e_ready,
  input          auto_in_3_e_valid,
  input  [2:0]   auto_in_3_e_bits_sink,
  output         auto_in_2_a_ready,
  input          auto_in_2_a_valid,
  input  [2:0]   auto_in_2_a_bits_opcode,
  input  [2:0]   auto_in_2_a_bits_param,
  input  [2:0]   auto_in_2_a_bits_size,
  input  [6:0]   auto_in_2_a_bits_source,
  input  [35:0]  auto_in_2_a_bits_address,
  input  [15:0]  auto_in_2_a_bits_mask,
  input  [127:0] auto_in_2_a_bits_data,
  input          auto_in_2_a_bits_corrupt,
  input          auto_in_2_b_ready,
  output         auto_in_2_b_valid,
  output [2:0]   auto_in_2_b_bits_opcode,
  output [1:0]   auto_in_2_b_bits_param,
  output [2:0]   auto_in_2_b_bits_size,
  output [6:0]   auto_in_2_b_bits_source,
  output [35:0]  auto_in_2_b_bits_address,
  output [15:0]  auto_in_2_b_bits_mask,
  output [127:0] auto_in_2_b_bits_data,
  output         auto_in_2_b_bits_corrupt,
  output         auto_in_2_c_ready,
  input          auto_in_2_c_valid,
  input  [2:0]   auto_in_2_c_bits_opcode,
  input  [2:0]   auto_in_2_c_bits_param,
  input  [2:0]   auto_in_2_c_bits_size,
  input  [6:0]   auto_in_2_c_bits_source,
  input  [35:0]  auto_in_2_c_bits_address,
  input  [127:0] auto_in_2_c_bits_data,
  input          auto_in_2_c_bits_corrupt,
  input          auto_in_2_d_ready,
  output         auto_in_2_d_valid,
  output [2:0]   auto_in_2_d_bits_opcode,
  output [1:0]   auto_in_2_d_bits_param,
  output [2:0]   auto_in_2_d_bits_size,
  output [6:0]   auto_in_2_d_bits_source,
  output [2:0]   auto_in_2_d_bits_sink,
  output         auto_in_2_d_bits_denied,
  output [127:0] auto_in_2_d_bits_data,
  output         auto_in_2_d_bits_corrupt,
  output         auto_in_2_e_ready,
  input          auto_in_2_e_valid,
  input  [2:0]   auto_in_2_e_bits_sink,
  output         auto_in_1_a_ready,
  input          auto_in_1_a_valid,
  input  [2:0]   auto_in_1_a_bits_opcode,
  input  [2:0]   auto_in_1_a_bits_param,
  input  [2:0]   auto_in_1_a_bits_size,
  input  [6:0]   auto_in_1_a_bits_source,
  input  [35:0]  auto_in_1_a_bits_address,
  input  [15:0]  auto_in_1_a_bits_mask,
  input  [127:0] auto_in_1_a_bits_data,
  input          auto_in_1_a_bits_corrupt,
  input          auto_in_1_b_ready,
  output         auto_in_1_b_valid,
  output [2:0]   auto_in_1_b_bits_opcode,
  output [1:0]   auto_in_1_b_bits_param,
  output [2:0]   auto_in_1_b_bits_size,
  output [6:0]   auto_in_1_b_bits_source,
  output [35:0]  auto_in_1_b_bits_address,
  output [15:0]  auto_in_1_b_bits_mask,
  output [127:0] auto_in_1_b_bits_data,
  output         auto_in_1_b_bits_corrupt,
  output         auto_in_1_c_ready,
  input          auto_in_1_c_valid,
  input  [2:0]   auto_in_1_c_bits_opcode,
  input  [2:0]   auto_in_1_c_bits_param,
  input  [2:0]   auto_in_1_c_bits_size,
  input  [6:0]   auto_in_1_c_bits_source,
  input  [35:0]  auto_in_1_c_bits_address,
  input  [127:0] auto_in_1_c_bits_data,
  input          auto_in_1_c_bits_corrupt,
  input          auto_in_1_d_ready,
  output         auto_in_1_d_valid,
  output [2:0]   auto_in_1_d_bits_opcode,
  output [1:0]   auto_in_1_d_bits_param,
  output [2:0]   auto_in_1_d_bits_size,
  output [6:0]   auto_in_1_d_bits_source,
  output [2:0]   auto_in_1_d_bits_sink,
  output         auto_in_1_d_bits_denied,
  output [127:0] auto_in_1_d_bits_data,
  output         auto_in_1_d_bits_corrupt,
  output         auto_in_1_e_ready,
  input          auto_in_1_e_valid,
  input  [2:0]   auto_in_1_e_bits_sink,
  output         auto_in_0_a_ready,
  input          auto_in_0_a_valid,
  input  [2:0]   auto_in_0_a_bits_opcode,
  input  [2:0]   auto_in_0_a_bits_param,
  input  [2:0]   auto_in_0_a_bits_size,
  input  [6:0]   auto_in_0_a_bits_source,
  input  [35:0]  auto_in_0_a_bits_address,
  input  [15:0]  auto_in_0_a_bits_mask,
  input  [127:0] auto_in_0_a_bits_data,
  input          auto_in_0_a_bits_corrupt,
  input          auto_in_0_b_ready,
  output         auto_in_0_b_valid,
  output [2:0]   auto_in_0_b_bits_opcode,
  output [1:0]   auto_in_0_b_bits_param,
  output [2:0]   auto_in_0_b_bits_size,
  output [6:0]   auto_in_0_b_bits_source,
  output [35:0]  auto_in_0_b_bits_address,
  output [15:0]  auto_in_0_b_bits_mask,
  output [127:0] auto_in_0_b_bits_data,
  output         auto_in_0_b_bits_corrupt,
  output         auto_in_0_c_ready,
  input          auto_in_0_c_valid,
  input  [2:0]   auto_in_0_c_bits_opcode,
  input  [2:0]   auto_in_0_c_bits_param,
  input  [2:0]   auto_in_0_c_bits_size,
  input  [6:0]   auto_in_0_c_bits_source,
  input  [35:0]  auto_in_0_c_bits_address,
  input  [127:0] auto_in_0_c_bits_data,
  input          auto_in_0_c_bits_corrupt,
  input          auto_in_0_d_ready,
  output         auto_in_0_d_valid,
  output [2:0]   auto_in_0_d_bits_opcode,
  output [1:0]   auto_in_0_d_bits_param,
  output [2:0]   auto_in_0_d_bits_size,
  output [6:0]   auto_in_0_d_bits_source,
  output [2:0]   auto_in_0_d_bits_sink,
  output         auto_in_0_d_bits_denied,
  output [127:0] auto_in_0_d_bits_data,
  output         auto_in_0_d_bits_corrupt,
  output         auto_in_0_e_ready,
  input          auto_in_0_e_valid,
  input  [2:0]   auto_in_0_e_bits_sink,
  input          auto_out_3_a_ready,
  output         auto_out_3_a_valid,
  output [2:0]   auto_out_3_a_bits_opcode,
  output [2:0]   auto_out_3_a_bits_param,
  output [2:0]   auto_out_3_a_bits_source,
  output [35:0]  auto_out_3_a_bits_address,
  input          auto_out_3_c_ready,
  output         auto_out_3_c_valid,
  output [2:0]   auto_out_3_c_bits_opcode,
  output [2:0]   auto_out_3_c_bits_param,
  output [2:0]   auto_out_3_c_bits_source,
  output [35:0]  auto_out_3_c_bits_address,
  output [127:0] auto_out_3_c_bits_data,
  output         auto_out_3_d_ready,
  input          auto_out_3_d_valid,
  input  [2:0]   auto_out_3_d_bits_opcode,
  input  [1:0]   auto_out_3_d_bits_param,
  input  [2:0]   auto_out_3_d_bits_size,
  input  [2:0]   auto_out_3_d_bits_source,
  input  [2:0]   auto_out_3_d_bits_sink,
  input          auto_out_3_d_bits_denied,
  input  [127:0] auto_out_3_d_bits_data,
  input          auto_out_3_d_bits_corrupt,
  output         auto_out_3_e_valid,
  output [2:0]   auto_out_3_e_bits_sink,
  input          auto_out_2_a_ready,
  output         auto_out_2_a_valid,
  output [2:0]   auto_out_2_a_bits_opcode,
  output [2:0]   auto_out_2_a_bits_param,
  output [2:0]   auto_out_2_a_bits_source,
  output [35:0]  auto_out_2_a_bits_address,
  input          auto_out_2_c_ready,
  output         auto_out_2_c_valid,
  output [2:0]   auto_out_2_c_bits_opcode,
  output [2:0]   auto_out_2_c_bits_param,
  output [2:0]   auto_out_2_c_bits_source,
  output [35:0]  auto_out_2_c_bits_address,
  output [127:0] auto_out_2_c_bits_data,
  output         auto_out_2_d_ready,
  input          auto_out_2_d_valid,
  input  [2:0]   auto_out_2_d_bits_opcode,
  input  [1:0]   auto_out_2_d_bits_param,
  input  [2:0]   auto_out_2_d_bits_size,
  input  [2:0]   auto_out_2_d_bits_source,
  input  [2:0]   auto_out_2_d_bits_sink,
  input          auto_out_2_d_bits_denied,
  input  [127:0] auto_out_2_d_bits_data,
  input          auto_out_2_d_bits_corrupt,
  output         auto_out_2_e_valid,
  output [2:0]   auto_out_2_e_bits_sink,
  input          auto_out_1_a_ready,
  output         auto_out_1_a_valid,
  output [2:0]   auto_out_1_a_bits_opcode,
  output [2:0]   auto_out_1_a_bits_param,
  output [2:0]   auto_out_1_a_bits_source,
  output [35:0]  auto_out_1_a_bits_address,
  input          auto_out_1_c_ready,
  output         auto_out_1_c_valid,
  output [2:0]   auto_out_1_c_bits_opcode,
  output [2:0]   auto_out_1_c_bits_param,
  output [2:0]   auto_out_1_c_bits_source,
  output [35:0]  auto_out_1_c_bits_address,
  output [127:0] auto_out_1_c_bits_data,
  output         auto_out_1_d_ready,
  input          auto_out_1_d_valid,
  input  [2:0]   auto_out_1_d_bits_opcode,
  input  [1:0]   auto_out_1_d_bits_param,
  input  [2:0]   auto_out_1_d_bits_size,
  input  [2:0]   auto_out_1_d_bits_source,
  input  [2:0]   auto_out_1_d_bits_sink,
  input          auto_out_1_d_bits_denied,
  input  [127:0] auto_out_1_d_bits_data,
  input          auto_out_1_d_bits_corrupt,
  output         auto_out_1_e_valid,
  output [2:0]   auto_out_1_e_bits_sink,
  input          auto_out_0_a_ready,
  output         auto_out_0_a_valid,
  output [2:0]   auto_out_0_a_bits_opcode,
  output [2:0]   auto_out_0_a_bits_param,
  output [2:0]   auto_out_0_a_bits_source,
  output [35:0]  auto_out_0_a_bits_address,
  input          auto_out_0_c_ready,
  output         auto_out_0_c_valid,
  output [2:0]   auto_out_0_c_bits_opcode,
  output [2:0]   auto_out_0_c_bits_param,
  output [2:0]   auto_out_0_c_bits_source,
  output [35:0]  auto_out_0_c_bits_address,
  output [127:0] auto_out_0_c_bits_data,
  output         auto_out_0_d_ready,
  input          auto_out_0_d_valid,
  input  [2:0]   auto_out_0_d_bits_opcode,
  input  [1:0]   auto_out_0_d_bits_param,
  input  [2:0]   auto_out_0_d_bits_size,
  input  [2:0]   auto_out_0_d_bits_source,
  input  [2:0]   auto_out_0_d_bits_sink,
  input          auto_out_0_d_bits_denied,
  input  [127:0] auto_out_0_d_bits_data,
  input          auto_out_0_d_bits_corrupt,
  output         auto_out_0_e_valid,
  output [2:0]   auto_out_0_e_bits_sink
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [31:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [31:0] _RAND_59;
  reg [31:0] _RAND_60;
  reg [31:0] _RAND_61;
  reg [31:0] _RAND_62;
  reg [31:0] _RAND_63;
  reg [31:0] _RAND_64;
  reg [31:0] _RAND_65;
  reg [31:0] _RAND_66;
  reg [31:0] _RAND_67;
  reg [31:0] _RAND_68;
  reg [31:0] _RAND_69;
  reg [31:0] _RAND_70;
  reg [31:0] _RAND_71;
  reg [31:0] _RAND_72;
  reg [31:0] _RAND_73;
  reg [31:0] _RAND_74;
  reg [31:0] _RAND_75;
  reg [31:0] _RAND_76;
  reg [31:0] _RAND_77;
  reg [31:0] _RAND_78;
  reg [31:0] _RAND_79;
  reg [31:0] _RAND_80;
  reg [31:0] _RAND_81;
  reg [31:0] _RAND_82;
  reg [31:0] _RAND_83;
  reg [31:0] _RAND_84;
  reg [31:0] _RAND_85;
  reg [31:0] _RAND_86;
  reg [31:0] _RAND_87;
  reg [31:0] _RAND_88;
  reg [31:0] _RAND_89;
  reg [31:0] _RAND_90;
  reg [31:0] _RAND_91;
  reg [31:0] _RAND_92;
  reg [31:0] _RAND_93;
  reg [31:0] _RAND_94;
  reg [31:0] _RAND_95;
  reg [31:0] _RAND_96;
  reg [31:0] _RAND_97;
  reg [31:0] _RAND_98;
  reg [31:0] _RAND_99;
  reg [31:0] _RAND_100;
  reg [31:0] _RAND_101;
  reg [31:0] _RAND_102;
  reg [31:0] _RAND_103;
  reg [31:0] _RAND_104;
  reg [31:0] _RAND_105;
  reg [31:0] _RAND_106;
  reg [31:0] _RAND_107;
  reg [31:0] _RAND_108;
  reg [31:0] _RAND_109;
  reg [31:0] _RAND_110;
  reg [31:0] _RAND_111;
  reg [31:0] _RAND_112;
  reg [31:0] _RAND_113;
  reg [31:0] _RAND_114;
  reg [31:0] _RAND_115;
  reg [31:0] _RAND_116;
  reg [31:0] _RAND_117;
  reg [31:0] _RAND_118;
  reg [31:0] _RAND_119;
  reg [31:0] _RAND_120;
  reg [31:0] _RAND_121;
  reg [31:0] _RAND_122;
  reg [31:0] _RAND_123;
  reg [31:0] _RAND_124;
  reg [31:0] _RAND_125;
  reg [31:0] _RAND_126;
  reg [31:0] _RAND_127;
  reg [31:0] _RAND_128;
  reg [31:0] _RAND_129;
  reg [31:0] _RAND_130;
  reg [31:0] _RAND_131;
  reg [31:0] _RAND_132;
  reg [31:0] _RAND_133;
  reg [31:0] _RAND_134;
  reg [31:0] _RAND_135;
  reg [31:0] _RAND_136;
  reg [31:0] _RAND_137;
  reg [31:0] _RAND_138;
  reg [31:0] _RAND_139;
  reg [31:0] _RAND_140;
  reg [31:0] _RAND_141;
  reg [31:0] _RAND_142;
  reg [31:0] _RAND_143;
  reg [31:0] _RAND_144;
  reg [63:0] _RAND_145;
  reg [31:0] _RAND_146;
  reg [63:0] _RAND_147;
  reg [31:0] _RAND_148;
  reg [31:0] _RAND_149;
  reg [31:0] _RAND_150;
  reg [31:0] _RAND_151;
`endif // RANDOMIZE_REG_INIT
  wire  out_back_rf_reset; // @[Decoupled.scala 296:21]
  wire  out_back_clock; // @[Decoupled.scala 296:21]
  wire  out_back_reset; // @[Decoupled.scala 296:21]
  wire  out_back_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  out_back_io_enq_valid; // @[Decoupled.scala 296:21]
  wire  out_back_io_enq_bits_read; // @[Decoupled.scala 296:21]
  wire [10:0] out_back_io_enq_bits_index; // @[Decoupled.scala 296:21]
  wire [63:0] out_back_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire [7:0] out_back_io_enq_bits_mask; // @[Decoupled.scala 296:21]
  wire [10:0] out_back_io_enq_bits_extra_tlrr_extra_source; // @[Decoupled.scala 296:21]
  wire [1:0] out_back_io_enq_bits_extra_tlrr_extra_size; // @[Decoupled.scala 296:21]
  wire  out_back_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  out_back_io_deq_valid; // @[Decoupled.scala 296:21]
  wire  out_back_io_deq_bits_read; // @[Decoupled.scala 296:21]
  wire [10:0] out_back_io_deq_bits_index; // @[Decoupled.scala 296:21]
  wire [63:0] out_back_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire [7:0] out_back_io_deq_bits_mask; // @[Decoupled.scala 296:21]
  wire [10:0] out_back_io_deq_bits_extra_tlrr_extra_source; // @[Decoupled.scala 296:21]
  wire [1:0] out_back_io_deq_bits_extra_tlrr_extra_size; // @[Decoupled.scala 296:21]
  wire  mods_0_rf_reset; // @[ComposableCache.scala 408:29]
  wire  mods_0_clock; // @[ComposableCache.scala 408:29]
  wire  mods_0_reset; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_in_a_ready; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_in_a_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_in_a_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_in_a_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_in_a_bits_size; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_0_io_in_a_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_0_io_in_a_bits_address; // @[ComposableCache.scala 408:29]
  wire [15:0] mods_0_io_in_a_bits_mask; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_0_io_in_a_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_in_a_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_in_b_ready; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_in_b_valid; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_0_io_in_b_bits_param; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_0_io_in_b_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_0_io_in_b_bits_address; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_in_c_ready; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_in_c_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_in_c_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_in_c_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_in_c_bits_size; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_0_io_in_c_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_0_io_in_c_bits_address; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_0_io_in_c_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_in_c_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_in_d_ready; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_in_d_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_in_d_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_0_io_in_d_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_in_d_bits_size; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_0_io_in_d_bits_source; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_in_d_bits_sink; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_in_d_bits_denied; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_0_io_in_d_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_in_d_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_in_e_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_in_e_bits_sink; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_out_a_ready; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_out_a_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_out_a_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_out_a_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_out_a_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_0_io_out_a_bits_address; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_out_c_ready; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_out_c_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_out_c_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_out_c_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_out_c_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_0_io_out_c_bits_address; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_0_io_out_c_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_out_d_ready; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_out_d_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_out_d_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_0_io_out_d_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_out_d_bits_size; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_out_d_bits_source; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_out_d_bits_sink; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_out_d_bits_denied; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_0_io_out_d_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_out_d_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_out_e_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_out_e_bits_sink; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_0_io_ways_0; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_0_io_ways_1; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_0_io_ways_2; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_0_io_ways_3; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_0_io_ways_4; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_0_io_ways_5; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_0_io_ways_6; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_0_io_ways_7; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_0_io_ways_8; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_0_io_ways_9; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_0_io_ways_10; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_0_io_ways_11; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_0_io_ways_12; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_0_io_ways_13; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_0_io_divs_0; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_0_io_divs_1; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_0_io_divs_2; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_0_io_divs_3; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_0_io_divs_4; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_0_io_divs_5; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_0_io_divs_6; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_0_io_divs_7; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_0_io_divs_8; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_0_io_divs_9; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_0_io_divs_10; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_0_io_divs_11; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_0_io_divs_12; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_0_io_divs_13; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_req_ready; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_req_valid; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_0_io_req_bits_address; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_resp_ready; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_resp_valid; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_side_adr_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_0_io_side_adr_bits_way; // @[ComposableCache.scala 408:29]
  wire [9:0] mods_0_io_side_adr_bits_set; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_0_io_side_adr_bits_beat; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_0_io_side_adr_bits_mask; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_side_adr_bits_write; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_0_io_side_wdat_data; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_side_wdat_poison; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_0_io_side_rdat_data; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_error_ready; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_error_valid; // @[ComposableCache.scala 408:29]
  wire  mods_0_io_error_bits_dir; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_0_io_error_bits_bit; // @[ComposableCache.scala 408:29]
  wire  mods_1_rf_reset; // @[ComposableCache.scala 408:29]
  wire  mods_1_clock; // @[ComposableCache.scala 408:29]
  wire  mods_1_reset; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_in_a_ready; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_in_a_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_in_a_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_in_a_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_in_a_bits_size; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_1_io_in_a_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_1_io_in_a_bits_address; // @[ComposableCache.scala 408:29]
  wire [15:0] mods_1_io_in_a_bits_mask; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_1_io_in_a_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_in_a_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_in_b_ready; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_in_b_valid; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_1_io_in_b_bits_param; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_1_io_in_b_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_1_io_in_b_bits_address; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_in_c_ready; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_in_c_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_in_c_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_in_c_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_in_c_bits_size; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_1_io_in_c_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_1_io_in_c_bits_address; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_1_io_in_c_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_in_c_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_in_d_ready; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_in_d_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_in_d_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_1_io_in_d_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_in_d_bits_size; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_1_io_in_d_bits_source; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_in_d_bits_sink; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_in_d_bits_denied; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_1_io_in_d_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_in_d_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_in_e_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_in_e_bits_sink; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_out_a_ready; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_out_a_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_out_a_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_out_a_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_out_a_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_1_io_out_a_bits_address; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_out_c_ready; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_out_c_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_out_c_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_out_c_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_out_c_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_1_io_out_c_bits_address; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_1_io_out_c_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_out_d_ready; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_out_d_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_out_d_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_1_io_out_d_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_out_d_bits_size; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_out_d_bits_source; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_out_d_bits_sink; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_out_d_bits_denied; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_1_io_out_d_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_out_d_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_out_e_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_out_e_bits_sink; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_1_io_ways_0; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_1_io_ways_1; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_1_io_ways_2; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_1_io_ways_3; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_1_io_ways_4; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_1_io_ways_5; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_1_io_ways_6; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_1_io_ways_7; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_1_io_ways_8; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_1_io_ways_9; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_1_io_ways_10; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_1_io_ways_11; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_1_io_ways_12; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_1_io_ways_13; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_1_io_divs_0; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_1_io_divs_1; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_1_io_divs_2; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_1_io_divs_3; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_1_io_divs_4; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_1_io_divs_5; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_1_io_divs_6; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_1_io_divs_7; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_1_io_divs_8; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_1_io_divs_9; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_1_io_divs_10; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_1_io_divs_11; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_1_io_divs_12; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_1_io_divs_13; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_req_ready; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_req_valid; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_1_io_req_bits_address; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_resp_ready; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_resp_valid; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_side_adr_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_1_io_side_adr_bits_way; // @[ComposableCache.scala 408:29]
  wire [9:0] mods_1_io_side_adr_bits_set; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_1_io_side_adr_bits_beat; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_1_io_side_adr_bits_mask; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_side_adr_bits_write; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_1_io_side_wdat_data; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_side_wdat_poison; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_1_io_side_rdat_data; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_error_ready; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_error_valid; // @[ComposableCache.scala 408:29]
  wire  mods_1_io_error_bits_dir; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_1_io_error_bits_bit; // @[ComposableCache.scala 408:29]
  wire  mods_2_rf_reset; // @[ComposableCache.scala 408:29]
  wire  mods_2_clock; // @[ComposableCache.scala 408:29]
  wire  mods_2_reset; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_in_a_ready; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_in_a_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_in_a_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_in_a_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_in_a_bits_size; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_2_io_in_a_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_2_io_in_a_bits_address; // @[ComposableCache.scala 408:29]
  wire [15:0] mods_2_io_in_a_bits_mask; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_2_io_in_a_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_in_a_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_in_b_ready; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_in_b_valid; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_2_io_in_b_bits_param; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_2_io_in_b_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_2_io_in_b_bits_address; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_in_c_ready; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_in_c_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_in_c_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_in_c_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_in_c_bits_size; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_2_io_in_c_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_2_io_in_c_bits_address; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_2_io_in_c_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_in_c_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_in_d_ready; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_in_d_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_in_d_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_2_io_in_d_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_in_d_bits_size; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_2_io_in_d_bits_source; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_in_d_bits_sink; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_in_d_bits_denied; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_2_io_in_d_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_in_d_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_in_e_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_in_e_bits_sink; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_out_a_ready; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_out_a_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_out_a_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_out_a_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_out_a_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_2_io_out_a_bits_address; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_out_c_ready; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_out_c_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_out_c_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_out_c_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_out_c_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_2_io_out_c_bits_address; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_2_io_out_c_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_out_d_ready; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_out_d_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_out_d_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_2_io_out_d_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_out_d_bits_size; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_out_d_bits_source; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_out_d_bits_sink; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_out_d_bits_denied; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_2_io_out_d_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_out_d_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_out_e_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_out_e_bits_sink; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_2_io_ways_0; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_2_io_ways_1; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_2_io_ways_2; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_2_io_ways_3; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_2_io_ways_4; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_2_io_ways_5; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_2_io_ways_6; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_2_io_ways_7; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_2_io_ways_8; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_2_io_ways_9; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_2_io_ways_10; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_2_io_ways_11; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_2_io_ways_12; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_2_io_ways_13; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_2_io_divs_0; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_2_io_divs_1; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_2_io_divs_2; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_2_io_divs_3; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_2_io_divs_4; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_2_io_divs_5; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_2_io_divs_6; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_2_io_divs_7; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_2_io_divs_8; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_2_io_divs_9; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_2_io_divs_10; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_2_io_divs_11; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_2_io_divs_12; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_2_io_divs_13; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_req_ready; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_req_valid; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_2_io_req_bits_address; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_resp_ready; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_resp_valid; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_side_adr_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_2_io_side_adr_bits_way; // @[ComposableCache.scala 408:29]
  wire [9:0] mods_2_io_side_adr_bits_set; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_2_io_side_adr_bits_beat; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_2_io_side_adr_bits_mask; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_side_adr_bits_write; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_2_io_side_wdat_data; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_side_wdat_poison; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_2_io_side_rdat_data; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_error_ready; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_error_valid; // @[ComposableCache.scala 408:29]
  wire  mods_2_io_error_bits_dir; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_2_io_error_bits_bit; // @[ComposableCache.scala 408:29]
  wire  mods_3_rf_reset; // @[ComposableCache.scala 408:29]
  wire  mods_3_clock; // @[ComposableCache.scala 408:29]
  wire  mods_3_reset; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_in_a_ready; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_in_a_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_in_a_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_in_a_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_in_a_bits_size; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_3_io_in_a_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_3_io_in_a_bits_address; // @[ComposableCache.scala 408:29]
  wire [15:0] mods_3_io_in_a_bits_mask; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_3_io_in_a_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_in_a_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_in_b_ready; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_in_b_valid; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_3_io_in_b_bits_param; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_3_io_in_b_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_3_io_in_b_bits_address; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_in_c_ready; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_in_c_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_in_c_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_in_c_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_in_c_bits_size; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_3_io_in_c_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_3_io_in_c_bits_address; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_3_io_in_c_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_in_c_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_in_d_ready; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_in_d_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_in_d_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_3_io_in_d_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_in_d_bits_size; // @[ComposableCache.scala 408:29]
  wire [6:0] mods_3_io_in_d_bits_source; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_in_d_bits_sink; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_in_d_bits_denied; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_3_io_in_d_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_in_d_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_in_e_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_in_e_bits_sink; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_out_a_ready; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_out_a_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_out_a_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_out_a_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_out_a_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_3_io_out_a_bits_address; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_out_c_ready; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_out_c_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_out_c_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_out_c_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_out_c_bits_source; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_3_io_out_c_bits_address; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_3_io_out_c_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_out_d_ready; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_out_d_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_out_d_bits_opcode; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_3_io_out_d_bits_param; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_out_d_bits_size; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_out_d_bits_source; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_out_d_bits_sink; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_out_d_bits_denied; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_3_io_out_d_bits_data; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_out_d_bits_corrupt; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_out_e_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_out_e_bits_sink; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_3_io_ways_0; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_3_io_ways_1; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_3_io_ways_2; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_3_io_ways_3; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_3_io_ways_4; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_3_io_ways_5; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_3_io_ways_6; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_3_io_ways_7; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_3_io_ways_8; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_3_io_ways_9; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_3_io_ways_10; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_3_io_ways_11; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_3_io_ways_12; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_3_io_ways_13; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_3_io_divs_0; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_3_io_divs_1; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_3_io_divs_2; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_3_io_divs_3; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_3_io_divs_4; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_3_io_divs_5; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_3_io_divs_6; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_3_io_divs_7; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_3_io_divs_8; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_3_io_divs_9; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_3_io_divs_10; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_3_io_divs_11; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_3_io_divs_12; // @[ComposableCache.scala 408:29]
  wire [10:0] mods_3_io_divs_13; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_req_ready; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_req_valid; // @[ComposableCache.scala 408:29]
  wire [35:0] mods_3_io_req_bits_address; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_resp_ready; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_resp_valid; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_side_adr_valid; // @[ComposableCache.scala 408:29]
  wire [2:0] mods_3_io_side_adr_bits_way; // @[ComposableCache.scala 408:29]
  wire [9:0] mods_3_io_side_adr_bits_set; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_3_io_side_adr_bits_beat; // @[ComposableCache.scala 408:29]
  wire [1:0] mods_3_io_side_adr_bits_mask; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_side_adr_bits_write; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_3_io_side_wdat_data; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_side_wdat_poison; // @[ComposableCache.scala 408:29]
  wire [127:0] mods_3_io_side_rdat_data; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_error_ready; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_error_valid; // @[ComposableCache.scala 408:29]
  wire  mods_3_io_error_bits_dir; // @[ComposableCache.scala 408:29]
  wire [7:0] mods_3_io_error_bits_bit; // @[ComposableCache.scala 408:29]
  wire  sideband_rf_reset; // @[ComposableCache.scala 473:28]
  wire  sideband_clock; // @[ComposableCache.scala 473:28]
  wire  sideband_reset; // @[ComposableCache.scala 473:28]
  wire  sideband_io_a_ready; // @[ComposableCache.scala 473:28]
  wire  sideband_io_a_valid; // @[ComposableCache.scala 473:28]
  wire [2:0] sideband_io_a_bits_opcode; // @[ComposableCache.scala 473:28]
  wire [2:0] sideband_io_a_bits_param; // @[ComposableCache.scala 473:28]
  wire [2:0] sideband_io_a_bits_size; // @[ComposableCache.scala 473:28]
  wire [9:0] sideband_io_a_bits_source; // @[ComposableCache.scala 473:28]
  wire [27:0] sideband_io_a_bits_address; // @[ComposableCache.scala 473:28]
  wire [15:0] sideband_io_a_bits_mask; // @[ComposableCache.scala 473:28]
  wire [127:0] sideband_io_a_bits_data; // @[ComposableCache.scala 473:28]
  wire  sideband_io_a_bits_corrupt; // @[ComposableCache.scala 473:28]
  wire  sideband_io_d_ready; // @[ComposableCache.scala 473:28]
  wire  sideband_io_d_valid; // @[ComposableCache.scala 473:28]
  wire [2:0] sideband_io_d_bits_opcode; // @[ComposableCache.scala 473:28]
  wire [2:0] sideband_io_d_bits_size; // @[ComposableCache.scala 473:28]
  wire [9:0] sideband_io_d_bits_source; // @[ComposableCache.scala 473:28]
  wire [127:0] sideband_io_d_bits_data; // @[ComposableCache.scala 473:28]
  wire  sideband_io_d_bits_corrupt; // @[ComposableCache.scala 473:28]
  wire [2:0] sideband_io_threshold; // @[ComposableCache.scala 473:28]
  wire  sideband_io_bs_adr_0_valid; // @[ComposableCache.scala 473:28]
  wire [2:0] sideband_io_bs_adr_0_bits_way; // @[ComposableCache.scala 473:28]
  wire [9:0] sideband_io_bs_adr_0_bits_set; // @[ComposableCache.scala 473:28]
  wire [1:0] sideband_io_bs_adr_0_bits_beat; // @[ComposableCache.scala 473:28]
  wire [1:0] sideband_io_bs_adr_0_bits_mask; // @[ComposableCache.scala 473:28]
  wire  sideband_io_bs_adr_0_bits_write; // @[ComposableCache.scala 473:28]
  wire  sideband_io_bs_adr_1_valid; // @[ComposableCache.scala 473:28]
  wire [2:0] sideband_io_bs_adr_1_bits_way; // @[ComposableCache.scala 473:28]
  wire [9:0] sideband_io_bs_adr_1_bits_set; // @[ComposableCache.scala 473:28]
  wire [1:0] sideband_io_bs_adr_1_bits_beat; // @[ComposableCache.scala 473:28]
  wire [1:0] sideband_io_bs_adr_1_bits_mask; // @[ComposableCache.scala 473:28]
  wire  sideband_io_bs_adr_1_bits_write; // @[ComposableCache.scala 473:28]
  wire  sideband_io_bs_adr_2_valid; // @[ComposableCache.scala 473:28]
  wire [2:0] sideband_io_bs_adr_2_bits_way; // @[ComposableCache.scala 473:28]
  wire [9:0] sideband_io_bs_adr_2_bits_set; // @[ComposableCache.scala 473:28]
  wire [1:0] sideband_io_bs_adr_2_bits_beat; // @[ComposableCache.scala 473:28]
  wire [1:0] sideband_io_bs_adr_2_bits_mask; // @[ComposableCache.scala 473:28]
  wire  sideband_io_bs_adr_2_bits_write; // @[ComposableCache.scala 473:28]
  wire  sideband_io_bs_adr_3_valid; // @[ComposableCache.scala 473:28]
  wire [2:0] sideband_io_bs_adr_3_bits_way; // @[ComposableCache.scala 473:28]
  wire [9:0] sideband_io_bs_adr_3_bits_set; // @[ComposableCache.scala 473:28]
  wire [1:0] sideband_io_bs_adr_3_bits_beat; // @[ComposableCache.scala 473:28]
  wire [1:0] sideband_io_bs_adr_3_bits_mask; // @[ComposableCache.scala 473:28]
  wire  sideband_io_bs_adr_3_bits_write; // @[ComposableCache.scala 473:28]
  wire [127:0] sideband_io_bs_wdat_0_data; // @[ComposableCache.scala 473:28]
  wire  sideband_io_bs_wdat_0_poison; // @[ComposableCache.scala 473:28]
  wire [127:0] sideband_io_bs_wdat_1_data; // @[ComposableCache.scala 473:28]
  wire  sideband_io_bs_wdat_1_poison; // @[ComposableCache.scala 473:28]
  wire [127:0] sideband_io_bs_wdat_2_data; // @[ComposableCache.scala 473:28]
  wire  sideband_io_bs_wdat_2_poison; // @[ComposableCache.scala 473:28]
  wire [127:0] sideband_io_bs_wdat_3_data; // @[ComposableCache.scala 473:28]
  wire  sideband_io_bs_wdat_3_poison; // @[ComposableCache.scala 473:28]
  wire [127:0] sideband_io_bs_rdat_0_data; // @[ComposableCache.scala 473:28]
  wire [127:0] sideband_io_bs_rdat_1_data; // @[ComposableCache.scala 473:28]
  wire [127:0] sideband_io_bs_rdat_2_data; // @[ComposableCache.scala 473:28]
  wire [127:0] sideband_io_bs_rdat_3_data; // @[ComposableCache.scala 473:28]
  wire  sideband_io_uncorrected_valid; // @[ComposableCache.scala 473:28]
  wire [35:0] sideband_io_uncorrected_bits; // @[ComposableCache.scala 473:28]
  reg [2:0] wayEnable; // @[ComposableCache.scala 194:24]
  reg  wayMap_0_0; // @[ComposableCache.scala 209:88]
  reg  wayMap_0_1; // @[ComposableCache.scala 209:88]
  reg  wayMap_0_2; // @[ComposableCache.scala 209:88]
  reg  wayMap_0_3; // @[ComposableCache.scala 209:88]
  reg  wayMap_0_4; // @[ComposableCache.scala 209:88]
  reg  wayMap_0_5; // @[ComposableCache.scala 209:88]
  reg  wayMap_0_6; // @[ComposableCache.scala 209:88]
  reg  wayMap_0_7; // @[ComposableCache.scala 209:88]
  reg  wayMap_1_0; // @[ComposableCache.scala 209:88]
  reg  wayMap_1_1; // @[ComposableCache.scala 209:88]
  reg  wayMap_1_2; // @[ComposableCache.scala 209:88]
  reg  wayMap_1_3; // @[ComposableCache.scala 209:88]
  reg  wayMap_1_4; // @[ComposableCache.scala 209:88]
  reg  wayMap_1_5; // @[ComposableCache.scala 209:88]
  reg  wayMap_1_6; // @[ComposableCache.scala 209:88]
  reg  wayMap_1_7; // @[ComposableCache.scala 209:88]
  reg  wayMap_2_0; // @[ComposableCache.scala 209:88]
  reg  wayMap_2_1; // @[ComposableCache.scala 209:88]
  reg  wayMap_2_2; // @[ComposableCache.scala 209:88]
  reg  wayMap_2_3; // @[ComposableCache.scala 209:88]
  reg  wayMap_2_4; // @[ComposableCache.scala 209:88]
  reg  wayMap_2_5; // @[ComposableCache.scala 209:88]
  reg  wayMap_2_6; // @[ComposableCache.scala 209:88]
  reg  wayMap_2_7; // @[ComposableCache.scala 209:88]
  reg  wayMap_3_0; // @[ComposableCache.scala 209:88]
  reg  wayMap_3_1; // @[ComposableCache.scala 209:88]
  reg  wayMap_3_2; // @[ComposableCache.scala 209:88]
  reg  wayMap_3_3; // @[ComposableCache.scala 209:88]
  reg  wayMap_3_4; // @[ComposableCache.scala 209:88]
  reg  wayMap_3_5; // @[ComposableCache.scala 209:88]
  reg  wayMap_3_6; // @[ComposableCache.scala 209:88]
  reg  wayMap_3_7; // @[ComposableCache.scala 209:88]
  reg  wayMap_4_0; // @[ComposableCache.scala 209:88]
  reg  wayMap_4_1; // @[ComposableCache.scala 209:88]
  reg  wayMap_4_2; // @[ComposableCache.scala 209:88]
  reg  wayMap_4_3; // @[ComposableCache.scala 209:88]
  reg  wayMap_4_4; // @[ComposableCache.scala 209:88]
  reg  wayMap_4_5; // @[ComposableCache.scala 209:88]
  reg  wayMap_4_6; // @[ComposableCache.scala 209:88]
  reg  wayMap_4_7; // @[ComposableCache.scala 209:88]
  reg  wayMap_5_0; // @[ComposableCache.scala 209:88]
  reg  wayMap_5_1; // @[ComposableCache.scala 209:88]
  reg  wayMap_5_2; // @[ComposableCache.scala 209:88]
  reg  wayMap_5_3; // @[ComposableCache.scala 209:88]
  reg  wayMap_5_4; // @[ComposableCache.scala 209:88]
  reg  wayMap_5_5; // @[ComposableCache.scala 209:88]
  reg  wayMap_5_6; // @[ComposableCache.scala 209:88]
  reg  wayMap_5_7; // @[ComposableCache.scala 209:88]
  reg  wayMap_6_0; // @[ComposableCache.scala 209:88]
  reg  wayMap_6_1; // @[ComposableCache.scala 209:88]
  reg  wayMap_6_2; // @[ComposableCache.scala 209:88]
  reg  wayMap_6_3; // @[ComposableCache.scala 209:88]
  reg  wayMap_6_4; // @[ComposableCache.scala 209:88]
  reg  wayMap_6_5; // @[ComposableCache.scala 209:88]
  reg  wayMap_6_6; // @[ComposableCache.scala 209:88]
  reg  wayMap_6_7; // @[ComposableCache.scala 209:88]
  reg  wayMap_7_0; // @[ComposableCache.scala 209:88]
  reg  wayMap_7_1; // @[ComposableCache.scala 209:88]
  reg  wayMap_7_2; // @[ComposableCache.scala 209:88]
  reg  wayMap_7_3; // @[ComposableCache.scala 209:88]
  reg  wayMap_7_4; // @[ComposableCache.scala 209:88]
  reg  wayMap_7_5; // @[ComposableCache.scala 209:88]
  reg  wayMap_7_6; // @[ComposableCache.scala 209:88]
  reg  wayMap_7_7; // @[ComposableCache.scala 209:88]
  reg  wayMap_8_0; // @[ComposableCache.scala 209:88]
  reg  wayMap_8_1; // @[ComposableCache.scala 209:88]
  reg  wayMap_8_2; // @[ComposableCache.scala 209:88]
  reg  wayMap_8_3; // @[ComposableCache.scala 209:88]
  reg  wayMap_8_4; // @[ComposableCache.scala 209:88]
  reg  wayMap_8_5; // @[ComposableCache.scala 209:88]
  reg  wayMap_8_6; // @[ComposableCache.scala 209:88]
  reg  wayMap_8_7; // @[ComposableCache.scala 209:88]
  reg  wayMap_9_0; // @[ComposableCache.scala 209:88]
  reg  wayMap_9_1; // @[ComposableCache.scala 209:88]
  reg  wayMap_9_2; // @[ComposableCache.scala 209:88]
  reg  wayMap_9_3; // @[ComposableCache.scala 209:88]
  reg  wayMap_9_4; // @[ComposableCache.scala 209:88]
  reg  wayMap_9_5; // @[ComposableCache.scala 209:88]
  reg  wayMap_9_6; // @[ComposableCache.scala 209:88]
  reg  wayMap_9_7; // @[ComposableCache.scala 209:88]
  reg  wayMap_10_0; // @[ComposableCache.scala 209:88]
  reg  wayMap_10_1; // @[ComposableCache.scala 209:88]
  reg  wayMap_10_2; // @[ComposableCache.scala 209:88]
  reg  wayMap_10_3; // @[ComposableCache.scala 209:88]
  reg  wayMap_10_4; // @[ComposableCache.scala 209:88]
  reg  wayMap_10_5; // @[ComposableCache.scala 209:88]
  reg  wayMap_10_6; // @[ComposableCache.scala 209:88]
  reg  wayMap_10_7; // @[ComposableCache.scala 209:88]
  reg  wayMap_11_0; // @[ComposableCache.scala 209:88]
  reg  wayMap_11_1; // @[ComposableCache.scala 209:88]
  reg  wayMap_11_2; // @[ComposableCache.scala 209:88]
  reg  wayMap_11_3; // @[ComposableCache.scala 209:88]
  reg  wayMap_11_4; // @[ComposableCache.scala 209:88]
  reg  wayMap_11_5; // @[ComposableCache.scala 209:88]
  reg  wayMap_11_6; // @[ComposableCache.scala 209:88]
  reg  wayMap_11_7; // @[ComposableCache.scala 209:88]
  reg  wayMap_12_0; // @[ComposableCache.scala 209:88]
  reg  wayMap_12_1; // @[ComposableCache.scala 209:88]
  reg  wayMap_12_2; // @[ComposableCache.scala 209:88]
  reg  wayMap_12_3; // @[ComposableCache.scala 209:88]
  reg  wayMap_12_4; // @[ComposableCache.scala 209:88]
  reg  wayMap_12_5; // @[ComposableCache.scala 209:88]
  reg  wayMap_12_6; // @[ComposableCache.scala 209:88]
  reg  wayMap_12_7; // @[ComposableCache.scala 209:88]
  reg  wayMap_13_0; // @[ComposableCache.scala 209:88]
  reg  wayMap_13_1; // @[ComposableCache.scala 209:88]
  reg  wayMap_13_2; // @[ComposableCache.scala 209:88]
  reg  wayMap_13_3; // @[ComposableCache.scala 209:88]
  reg  wayMap_13_4; // @[ComposableCache.scala 209:88]
  reg  wayMap_13_5; // @[ComposableCache.scala 209:88]
  reg  wayMap_13_6; // @[ComposableCache.scala 209:88]
  reg  wayMap_13_7; // @[ComposableCache.scala 209:88]
  wire [14:0] _wayMask_T_1 = 15'hff << wayEnable; // @[package.scala 234:77]
  wire [7:0] wayMask_hi = ~_wayMask_T_1[7:0]; // @[package.scala 234:46]
  wire [8:0] wayMask = {wayMask_hi,1'h1}; // @[Cat.scala 30:58]
  wire [7:0] _ways_T = {wayMap_0_7,wayMap_0_6,wayMap_0_5,wayMap_0_4,wayMap_0_3,wayMap_0_2,wayMap_0_1,wayMap_0_0}; // @[Cat.scala 30:58]
  wire [8:0] _GEN_877 = {{1'd0}, _ways_T}; // @[ComposableCache.scala 214:80]
  wire [8:0] _ways_T_1 = _GEN_877 & wayMask; // @[ComposableCache.scala 214:80]
  wire [8:0] ways_0 = _ways_T_1 == 9'h0 ? 9'h1 : _ways_T_1; // @[ComposableCache.scala 212:33]
  wire [7:0] _ways_T_4 = {wayMap_1_7,wayMap_1_6,wayMap_1_5,wayMap_1_4,wayMap_1_3,wayMap_1_2,wayMap_1_1,wayMap_1_0}; // @[Cat.scala 30:58]
  wire [8:0] _GEN_878 = {{1'd0}, _ways_T_4}; // @[ComposableCache.scala 214:80]
  wire [8:0] _ways_T_5 = _GEN_878 & wayMask; // @[ComposableCache.scala 214:80]
  wire [8:0] ways_1 = _ways_T_5 == 9'h0 ? 9'h1 : _ways_T_5; // @[ComposableCache.scala 212:33]
  wire [7:0] _ways_T_8 = {wayMap_2_7,wayMap_2_6,wayMap_2_5,wayMap_2_4,wayMap_2_3,wayMap_2_2,wayMap_2_1,wayMap_2_0}; // @[Cat.scala 30:58]
  wire [8:0] _GEN_879 = {{1'd0}, _ways_T_8}; // @[ComposableCache.scala 214:80]
  wire [8:0] _ways_T_9 = _GEN_879 & wayMask; // @[ComposableCache.scala 214:80]
  wire [8:0] ways_2 = _ways_T_9 == 9'h0 ? 9'h1 : _ways_T_9; // @[ComposableCache.scala 212:33]
  wire [7:0] _ways_T_12 = {wayMap_3_7,wayMap_3_6,wayMap_3_5,wayMap_3_4,wayMap_3_3,wayMap_3_2,wayMap_3_1,wayMap_3_0}; // @[Cat.scala 30:58]
  wire [8:0] _GEN_880 = {{1'd0}, _ways_T_12}; // @[ComposableCache.scala 214:80]
  wire [8:0] _ways_T_13 = _GEN_880 & wayMask; // @[ComposableCache.scala 214:80]
  wire [8:0] ways_3 = _ways_T_13 == 9'h0 ? 9'h1 : _ways_T_13; // @[ComposableCache.scala 212:33]
  wire [7:0] _ways_T_16 = {wayMap_4_7,wayMap_4_6,wayMap_4_5,wayMap_4_4,wayMap_4_3,wayMap_4_2,wayMap_4_1,wayMap_4_0}; // @[Cat.scala 30:58]
  wire [8:0] _GEN_881 = {{1'd0}, _ways_T_16}; // @[ComposableCache.scala 214:80]
  wire [8:0] _ways_T_17 = _GEN_881 & wayMask; // @[ComposableCache.scala 214:80]
  wire [8:0] ways_4 = _ways_T_17 == 9'h0 ? 9'h1 : _ways_T_17; // @[ComposableCache.scala 212:33]
  wire [7:0] _ways_T_20 = {wayMap_5_7,wayMap_5_6,wayMap_5_5,wayMap_5_4,wayMap_5_3,wayMap_5_2,wayMap_5_1,wayMap_5_0}; // @[Cat.scala 30:58]
  wire [8:0] _GEN_882 = {{1'd0}, _ways_T_20}; // @[ComposableCache.scala 214:80]
  wire [8:0] _ways_T_21 = _GEN_882 & wayMask; // @[ComposableCache.scala 214:80]
  wire [8:0] ways_5 = _ways_T_21 == 9'h0 ? 9'h1 : _ways_T_21; // @[ComposableCache.scala 212:33]
  wire [7:0] _ways_T_24 = {wayMap_6_7,wayMap_6_6,wayMap_6_5,wayMap_6_4,wayMap_6_3,wayMap_6_2,wayMap_6_1,wayMap_6_0}; // @[Cat.scala 30:58]
  wire [8:0] _GEN_883 = {{1'd0}, _ways_T_24}; // @[ComposableCache.scala 214:80]
  wire [8:0] _ways_T_25 = _GEN_883 & wayMask; // @[ComposableCache.scala 214:80]
  wire [8:0] ways_6 = _ways_T_25 == 9'h0 ? 9'h1 : _ways_T_25; // @[ComposableCache.scala 212:33]
  wire [7:0] _ways_T_28 = {wayMap_7_7,wayMap_7_6,wayMap_7_5,wayMap_7_4,wayMap_7_3,wayMap_7_2,wayMap_7_1,wayMap_7_0}; // @[Cat.scala 30:58]
  wire [8:0] _GEN_884 = {{1'd0}, _ways_T_28}; // @[ComposableCache.scala 214:80]
  wire [8:0] _ways_T_29 = _GEN_884 & wayMask; // @[ComposableCache.scala 214:80]
  wire [8:0] ways_7 = _ways_T_29 == 9'h0 ? 9'h1 : _ways_T_29; // @[ComposableCache.scala 212:33]
  wire [7:0] _ways_T_32 = {wayMap_8_7,wayMap_8_6,wayMap_8_5,wayMap_8_4,wayMap_8_3,wayMap_8_2,wayMap_8_1,wayMap_8_0}; // @[Cat.scala 30:58]
  wire [8:0] _GEN_885 = {{1'd0}, _ways_T_32}; // @[ComposableCache.scala 214:80]
  wire [8:0] _ways_T_33 = _GEN_885 & wayMask; // @[ComposableCache.scala 214:80]
  wire [8:0] ways_8 = _ways_T_33 == 9'h0 ? 9'h1 : _ways_T_33; // @[ComposableCache.scala 212:33]
  wire [7:0] _ways_T_36 = {wayMap_9_7,wayMap_9_6,wayMap_9_5,wayMap_9_4,wayMap_9_3,wayMap_9_2,wayMap_9_1,wayMap_9_0}; // @[Cat.scala 30:58]
  wire [8:0] _GEN_886 = {{1'd0}, _ways_T_36}; // @[ComposableCache.scala 214:80]
  wire [8:0] _ways_T_37 = _GEN_886 & wayMask; // @[ComposableCache.scala 214:80]
  wire [8:0] ways_9 = _ways_T_37 == 9'h0 ? 9'h1 : _ways_T_37; // @[ComposableCache.scala 212:33]
  wire [7:0] _ways_T_40 = {wayMap_10_7,wayMap_10_6,wayMap_10_5,wayMap_10_4,wayMap_10_3,wayMap_10_2,wayMap_10_1,
    wayMap_10_0}; // @[Cat.scala 30:58]
  wire [8:0] _GEN_887 = {{1'd0}, _ways_T_40}; // @[ComposableCache.scala 214:80]
  wire [8:0] _ways_T_41 = _GEN_887 & wayMask; // @[ComposableCache.scala 214:80]
  wire [8:0] ways_10 = _ways_T_41 == 9'h0 ? 9'h1 : _ways_T_41; // @[ComposableCache.scala 212:33]
  wire [7:0] _ways_T_44 = {wayMap_11_7,wayMap_11_6,wayMap_11_5,wayMap_11_4,wayMap_11_3,wayMap_11_2,wayMap_11_1,
    wayMap_11_0}; // @[Cat.scala 30:58]
  wire [8:0] _GEN_888 = {{1'd0}, _ways_T_44}; // @[ComposableCache.scala 214:80]
  wire [8:0] _ways_T_45 = _GEN_888 & wayMask; // @[ComposableCache.scala 214:80]
  wire [8:0] ways_11 = _ways_T_45 == 9'h0 ? 9'h1 : _ways_T_45; // @[ComposableCache.scala 212:33]
  wire [7:0] _ways_T_48 = {wayMap_12_7,wayMap_12_6,wayMap_12_5,wayMap_12_4,wayMap_12_3,wayMap_12_2,wayMap_12_1,
    wayMap_12_0}; // @[Cat.scala 30:58]
  wire [8:0] _GEN_889 = {{1'd0}, _ways_T_48}; // @[ComposableCache.scala 214:80]
  wire [8:0] _ways_T_49 = _GEN_889 & wayMask; // @[ComposableCache.scala 214:80]
  wire [8:0] ways_12 = _ways_T_49 == 9'h0 ? 9'h1 : _ways_T_49; // @[ComposableCache.scala 212:33]
  wire [7:0] _ways_T_52 = {wayMap_13_7,wayMap_13_6,wayMap_13_5,wayMap_13_4,wayMap_13_3,wayMap_13_2,wayMap_13_1,
    wayMap_13_0}; // @[Cat.scala 30:58]
  wire [8:0] _GEN_890 = {{1'd0}, _ways_T_52}; // @[ComposableCache.scala 214:80]
  wire [8:0] _ways_T_53 = _GEN_890 & wayMask; // @[ComposableCache.scala 214:80]
  wire [8:0] ways_13 = _ways_T_53 == 9'h0 ? 9'h1 : _ways_T_53; // @[ComposableCache.scala 212:33]
  reg [8:0] waysReg_0; // @[ComposableCache.scala 215:40]
  reg [8:0] waysReg_1; // @[ComposableCache.scala 215:40]
  reg [8:0] waysReg_2; // @[ComposableCache.scala 215:40]
  reg [8:0] waysReg_3; // @[ComposableCache.scala 215:40]
  reg [8:0] waysReg_4; // @[ComposableCache.scala 215:40]
  reg [8:0] waysReg_5; // @[ComposableCache.scala 215:40]
  reg [8:0] waysReg_6; // @[ComposableCache.scala 215:40]
  reg [8:0] waysReg_7; // @[ComposableCache.scala 215:40]
  reg [8:0] waysReg_8; // @[ComposableCache.scala 215:40]
  reg [8:0] waysReg_9; // @[ComposableCache.scala 215:40]
  reg [8:0] waysReg_10; // @[ComposableCache.scala 215:40]
  reg [8:0] waysReg_11; // @[ComposableCache.scala 215:40]
  reg [8:0] waysReg_12; // @[ComposableCache.scala 215:40]
  reg [8:0] waysReg_13; // @[ComposableCache.scala 215:40]
  wire [1:0] _divisors_T_9 = ways_0[0] + ways_0[1]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_11 = ways_0[2] + ways_0[3]; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_13 = _divisors_T_9 + _divisors_T_11; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_15 = ways_0[4] + ways_0[5]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_17 = ways_0[7] + ways_0[8]; // @[Bitwise.scala 47:55]
  wire [1:0] _GEN_891 = {{1'd0}, ways_0[6]}; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_19 = _GEN_891 + _divisors_T_17; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_21 = _divisors_T_15 + _divisors_T_19[1:0]; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_23 = _divisors_T_13 + _divisors_T_21; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_26 = _divisors_T_23 - 4'h1; // @[ComposableCache.scala 217:80]
  wire [1:0] _divisors_T_37 = ways_1[0] + ways_1[1]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_39 = ways_1[2] + ways_1[3]; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_41 = _divisors_T_37 + _divisors_T_39; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_43 = ways_1[4] + ways_1[5]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_45 = ways_1[7] + ways_1[8]; // @[Bitwise.scala 47:55]
  wire [1:0] _GEN_892 = {{1'd0}, ways_1[6]}; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_47 = _GEN_892 + _divisors_T_45; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_49 = _divisors_T_43 + _divisors_T_47[1:0]; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_51 = _divisors_T_41 + _divisors_T_49; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_54 = _divisors_T_51 - 4'h1; // @[ComposableCache.scala 217:80]
  wire [1:0] _divisors_T_65 = ways_2[0] + ways_2[1]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_67 = ways_2[2] + ways_2[3]; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_69 = _divisors_T_65 + _divisors_T_67; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_71 = ways_2[4] + ways_2[5]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_73 = ways_2[7] + ways_2[8]; // @[Bitwise.scala 47:55]
  wire [1:0] _GEN_893 = {{1'd0}, ways_2[6]}; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_75 = _GEN_893 + _divisors_T_73; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_77 = _divisors_T_71 + _divisors_T_75[1:0]; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_79 = _divisors_T_69 + _divisors_T_77; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_82 = _divisors_T_79 - 4'h1; // @[ComposableCache.scala 217:80]
  wire [1:0] _divisors_T_93 = ways_3[0] + ways_3[1]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_95 = ways_3[2] + ways_3[3]; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_97 = _divisors_T_93 + _divisors_T_95; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_99 = ways_3[4] + ways_3[5]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_101 = ways_3[7] + ways_3[8]; // @[Bitwise.scala 47:55]
  wire [1:0] _GEN_894 = {{1'd0}, ways_3[6]}; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_103 = _GEN_894 + _divisors_T_101; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_105 = _divisors_T_99 + _divisors_T_103[1:0]; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_107 = _divisors_T_97 + _divisors_T_105; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_110 = _divisors_T_107 - 4'h1; // @[ComposableCache.scala 217:80]
  wire [1:0] _divisors_T_121 = ways_4[0] + ways_4[1]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_123 = ways_4[2] + ways_4[3]; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_125 = _divisors_T_121 + _divisors_T_123; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_127 = ways_4[4] + ways_4[5]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_129 = ways_4[7] + ways_4[8]; // @[Bitwise.scala 47:55]
  wire [1:0] _GEN_895 = {{1'd0}, ways_4[6]}; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_131 = _GEN_895 + _divisors_T_129; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_133 = _divisors_T_127 + _divisors_T_131[1:0]; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_135 = _divisors_T_125 + _divisors_T_133; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_138 = _divisors_T_135 - 4'h1; // @[ComposableCache.scala 217:80]
  wire [1:0] _divisors_T_149 = ways_5[0] + ways_5[1]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_151 = ways_5[2] + ways_5[3]; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_153 = _divisors_T_149 + _divisors_T_151; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_155 = ways_5[4] + ways_5[5]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_157 = ways_5[7] + ways_5[8]; // @[Bitwise.scala 47:55]
  wire [1:0] _GEN_896 = {{1'd0}, ways_5[6]}; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_159 = _GEN_896 + _divisors_T_157; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_161 = _divisors_T_155 + _divisors_T_159[1:0]; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_163 = _divisors_T_153 + _divisors_T_161; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_166 = _divisors_T_163 - 4'h1; // @[ComposableCache.scala 217:80]
  wire [1:0] _divisors_T_177 = ways_6[0] + ways_6[1]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_179 = ways_6[2] + ways_6[3]; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_181 = _divisors_T_177 + _divisors_T_179; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_183 = ways_6[4] + ways_6[5]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_185 = ways_6[7] + ways_6[8]; // @[Bitwise.scala 47:55]
  wire [1:0] _GEN_897 = {{1'd0}, ways_6[6]}; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_187 = _GEN_897 + _divisors_T_185; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_189 = _divisors_T_183 + _divisors_T_187[1:0]; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_191 = _divisors_T_181 + _divisors_T_189; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_194 = _divisors_T_191 - 4'h1; // @[ComposableCache.scala 217:80]
  wire [1:0] _divisors_T_205 = ways_7[0] + ways_7[1]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_207 = ways_7[2] + ways_7[3]; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_209 = _divisors_T_205 + _divisors_T_207; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_211 = ways_7[4] + ways_7[5]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_213 = ways_7[7] + ways_7[8]; // @[Bitwise.scala 47:55]
  wire [1:0] _GEN_898 = {{1'd0}, ways_7[6]}; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_215 = _GEN_898 + _divisors_T_213; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_217 = _divisors_T_211 + _divisors_T_215[1:0]; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_219 = _divisors_T_209 + _divisors_T_217; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_222 = _divisors_T_219 - 4'h1; // @[ComposableCache.scala 217:80]
  wire [1:0] _divisors_T_233 = ways_8[0] + ways_8[1]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_235 = ways_8[2] + ways_8[3]; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_237 = _divisors_T_233 + _divisors_T_235; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_239 = ways_8[4] + ways_8[5]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_241 = ways_8[7] + ways_8[8]; // @[Bitwise.scala 47:55]
  wire [1:0] _GEN_899 = {{1'd0}, ways_8[6]}; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_243 = _GEN_899 + _divisors_T_241; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_245 = _divisors_T_239 + _divisors_T_243[1:0]; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_247 = _divisors_T_237 + _divisors_T_245; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_250 = _divisors_T_247 - 4'h1; // @[ComposableCache.scala 217:80]
  wire [1:0] _divisors_T_261 = ways_9[0] + ways_9[1]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_263 = ways_9[2] + ways_9[3]; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_265 = _divisors_T_261 + _divisors_T_263; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_267 = ways_9[4] + ways_9[5]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_269 = ways_9[7] + ways_9[8]; // @[Bitwise.scala 47:55]
  wire [1:0] _GEN_900 = {{1'd0}, ways_9[6]}; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_271 = _GEN_900 + _divisors_T_269; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_273 = _divisors_T_267 + _divisors_T_271[1:0]; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_275 = _divisors_T_265 + _divisors_T_273; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_278 = _divisors_T_275 - 4'h1; // @[ComposableCache.scala 217:80]
  wire [1:0] _divisors_T_289 = ways_10[0] + ways_10[1]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_291 = ways_10[2] + ways_10[3]; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_293 = _divisors_T_289 + _divisors_T_291; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_295 = ways_10[4] + ways_10[5]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_297 = ways_10[7] + ways_10[8]; // @[Bitwise.scala 47:55]
  wire [1:0] _GEN_901 = {{1'd0}, ways_10[6]}; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_299 = _GEN_901 + _divisors_T_297; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_301 = _divisors_T_295 + _divisors_T_299[1:0]; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_303 = _divisors_T_293 + _divisors_T_301; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_306 = _divisors_T_303 - 4'h1; // @[ComposableCache.scala 217:80]
  wire [1:0] _divisors_T_317 = ways_11[0] + ways_11[1]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_319 = ways_11[2] + ways_11[3]; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_321 = _divisors_T_317 + _divisors_T_319; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_323 = ways_11[4] + ways_11[5]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_325 = ways_11[7] + ways_11[8]; // @[Bitwise.scala 47:55]
  wire [1:0] _GEN_902 = {{1'd0}, ways_11[6]}; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_327 = _GEN_902 + _divisors_T_325; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_329 = _divisors_T_323 + _divisors_T_327[1:0]; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_331 = _divisors_T_321 + _divisors_T_329; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_334 = _divisors_T_331 - 4'h1; // @[ComposableCache.scala 217:80]
  wire [1:0] _divisors_T_345 = ways_12[0] + ways_12[1]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_347 = ways_12[2] + ways_12[3]; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_349 = _divisors_T_345 + _divisors_T_347; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_351 = ways_12[4] + ways_12[5]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_353 = ways_12[7] + ways_12[8]; // @[Bitwise.scala 47:55]
  wire [1:0] _GEN_903 = {{1'd0}, ways_12[6]}; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_355 = _GEN_903 + _divisors_T_353; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_357 = _divisors_T_351 + _divisors_T_355[1:0]; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_359 = _divisors_T_349 + _divisors_T_357; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_362 = _divisors_T_359 - 4'h1; // @[ComposableCache.scala 217:80]
  wire [1:0] _divisors_T_373 = ways_13[0] + ways_13[1]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_375 = ways_13[2] + ways_13[3]; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_377 = _divisors_T_373 + _divisors_T_375; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_379 = ways_13[4] + ways_13[5]; // @[Bitwise.scala 47:55]
  wire [1:0] _divisors_T_381 = ways_13[7] + ways_13[8]; // @[Bitwise.scala 47:55]
  wire [1:0] _GEN_904 = {{1'd0}, ways_13[6]}; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_383 = _GEN_904 + _divisors_T_381; // @[Bitwise.scala 47:55]
  wire [2:0] _divisors_T_385 = _divisors_T_379 + _divisors_T_383[1:0]; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_387 = _divisors_T_377 + _divisors_T_385; // @[Bitwise.scala 47:55]
  wire [3:0] _divisors_T_390 = _divisors_T_387 - 4'h1; // @[ComposableCache.scala 217:80]
  wire [10:0] _GEN_1 = 3'h1 == _divisors_T_26[2:0] ? 11'h200 : 11'h400; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_2 = 3'h2 == _divisors_T_26[2:0] ? 11'h156 : _GEN_1; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_3 = 3'h3 == _divisors_T_26[2:0] ? 11'h100 : _GEN_2; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_9 = 3'h1 == _divisors_T_54[2:0] ? 11'h200 : 11'h400; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_10 = 3'h2 == _divisors_T_54[2:0] ? 11'h156 : _GEN_9; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_11 = 3'h3 == _divisors_T_54[2:0] ? 11'h100 : _GEN_10; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_17 = 3'h1 == _divisors_T_82[2:0] ? 11'h200 : 11'h400; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_18 = 3'h2 == _divisors_T_82[2:0] ? 11'h156 : _GEN_17; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_19 = 3'h3 == _divisors_T_82[2:0] ? 11'h100 : _GEN_18; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_25 = 3'h1 == _divisors_T_110[2:0] ? 11'h200 : 11'h400; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_26 = 3'h2 == _divisors_T_110[2:0] ? 11'h156 : _GEN_25; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_27 = 3'h3 == _divisors_T_110[2:0] ? 11'h100 : _GEN_26; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_33 = 3'h1 == _divisors_T_138[2:0] ? 11'h200 : 11'h400; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_34 = 3'h2 == _divisors_T_138[2:0] ? 11'h156 : _GEN_33; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_35 = 3'h3 == _divisors_T_138[2:0] ? 11'h100 : _GEN_34; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_41 = 3'h1 == _divisors_T_166[2:0] ? 11'h200 : 11'h400; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_42 = 3'h2 == _divisors_T_166[2:0] ? 11'h156 : _GEN_41; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_43 = 3'h3 == _divisors_T_166[2:0] ? 11'h100 : _GEN_42; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_49 = 3'h1 == _divisors_T_194[2:0] ? 11'h200 : 11'h400; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_50 = 3'h2 == _divisors_T_194[2:0] ? 11'h156 : _GEN_49; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_51 = 3'h3 == _divisors_T_194[2:0] ? 11'h100 : _GEN_50; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_57 = 3'h1 == _divisors_T_222[2:0] ? 11'h200 : 11'h400; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_58 = 3'h2 == _divisors_T_222[2:0] ? 11'h156 : _GEN_57; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_59 = 3'h3 == _divisors_T_222[2:0] ? 11'h100 : _GEN_58; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_65 = 3'h1 == _divisors_T_250[2:0] ? 11'h200 : 11'h400; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_66 = 3'h2 == _divisors_T_250[2:0] ? 11'h156 : _GEN_65; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_67 = 3'h3 == _divisors_T_250[2:0] ? 11'h100 : _GEN_66; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_73 = 3'h1 == _divisors_T_278[2:0] ? 11'h200 : 11'h400; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_74 = 3'h2 == _divisors_T_278[2:0] ? 11'h156 : _GEN_73; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_75 = 3'h3 == _divisors_T_278[2:0] ? 11'h100 : _GEN_74; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_81 = 3'h1 == _divisors_T_306[2:0] ? 11'h200 : 11'h400; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_82 = 3'h2 == _divisors_T_306[2:0] ? 11'h156 : _GEN_81; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_83 = 3'h3 == _divisors_T_306[2:0] ? 11'h100 : _GEN_82; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_89 = 3'h1 == _divisors_T_334[2:0] ? 11'h200 : 11'h400; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_90 = 3'h2 == _divisors_T_334[2:0] ? 11'h156 : _GEN_89; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_91 = 3'h3 == _divisors_T_334[2:0] ? 11'h100 : _GEN_90; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_97 = 3'h1 == _divisors_T_362[2:0] ? 11'h200 : 11'h400; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_98 = 3'h2 == _divisors_T_362[2:0] ? 11'h156 : _GEN_97; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_99 = 3'h3 == _divisors_T_362[2:0] ? 11'h100 : _GEN_98; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_105 = 3'h1 == _divisors_T_390[2:0] ? 11'h200 : 11'h400; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_106 = 3'h2 == _divisors_T_390[2:0] ? 11'h156 : _GEN_105; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  wire [10:0] _GEN_107 = 3'h3 == _divisors_T_390[2:0] ? 11'h100 : _GEN_106; // @[ComposableCache.scala 217:45 ComposableCache.scala 217:45]
  reg [10:0] divisors_0; // @[ComposableCache.scala 217:41]
  reg [10:0] divisors_1; // @[ComposableCache.scala 217:41]
  reg [10:0] divisors_2; // @[ComposableCache.scala 217:41]
  reg [10:0] divisors_3; // @[ComposableCache.scala 217:41]
  reg [10:0] divisors_4; // @[ComposableCache.scala 217:41]
  reg [10:0] divisors_5; // @[ComposableCache.scala 217:41]
  reg [10:0] divisors_6; // @[ComposableCache.scala 217:41]
  reg [10:0] divisors_7; // @[ComposableCache.scala 217:41]
  reg [10:0] divisors_8; // @[ComposableCache.scala 217:41]
  reg [10:0] divisors_9; // @[ComposableCache.scala 217:41]
  reg [10:0] divisors_10; // @[ComposableCache.scala 217:41]
  reg [10:0] divisors_11; // @[ComposableCache.scala 217:41]
  reg [10:0] divisors_12; // @[ComposableCache.scala 217:41]
  reg [10:0] divisors_13; // @[ComposableCache.scala 217:41]
  reg [31:0] dirNumberCorrected; // @[ComposableCache.scala 220:40]
  reg [31:0] dirNumberUncorrected; // @[ComposableCache.scala 221:40]
  reg [31:0] datNumberCorrected; // @[ComposableCache.scala 222:40]
  reg [31:0] datNumberUncorrected; // @[ComposableCache.scala 223:40]
  reg [63:0] datAddressUncorrected; // @[ComposableCache.scala 227:36]
  wire  _T = 1'h1; // @[ComposableCache.scala 243:14]
  wire  _T_3 = 1'h0; // @[ComposableCache.scala 243:13]
  reg  flushInValid; // @[ComposableCache.scala 263:33]
  reg [63:0] flushInAddress; // @[ComposableCache.scala 265:29]
  reg  flushOutValid; // @[ComposableCache.scala 267:33]
  wire  _out_wofireMux_T = out_back_io_deq_valid & auto_in_4_d_ready; // @[ComposableCache.scala 141:30]
  wire  out_oindex_hi_hi_hi = out_back_io_deq_bits_index[8]; // @[ComposableCache.scala 141:30]
  wire  out_oindex_hi_hi_lo = out_back_io_deq_bits_index[6]; // @[ComposableCache.scala 141:30]
  wire  out_oindex_hi_lo_hi = out_back_io_deq_bits_index[5]; // @[ComposableCache.scala 141:30]
  wire  out_oindex_hi_lo_lo = out_back_io_deq_bits_index[3]; // @[ComposableCache.scala 141:30]
  wire  out_oindex_lo_hi_hi = out_back_io_deq_bits_index[2]; // @[ComposableCache.scala 141:30]
  wire  out_oindex_lo_hi_lo = out_back_io_deq_bits_index[1]; // @[ComposableCache.scala 141:30]
  wire  out_oindex_lo_lo = out_back_io_deq_bits_index[0]; // @[ComposableCache.scala 141:30]
  wire [6:0] out_oindex = {out_oindex_hi_hi_hi,out_oindex_hi_hi_lo,out_oindex_hi_lo_hi,out_oindex_hi_lo_lo,
    out_oindex_lo_hi_hi,out_oindex_lo_hi_lo,out_oindex_lo_lo}; // @[Cat.scala 30:58]
  wire [127:0] _out_backSel_T = 128'h1 << out_oindex; // @[OneHot.scala 58:35]
  wire  out_backSel_40 = _out_backSel_T[40]; // @[ComposableCache.scala 141:30]
  wire [10:0] out_bindex = out_back_io_deq_bits_index & 11'h690; // @[ComposableCache.scala 141:30]
  wire  _out_T_41 = out_bindex == 11'h0; // @[ComposableCache.scala 141:30]
  wire  out_woready_124 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_40 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire [7:0] out_backMask_hi_hi_hi = out_back_io_deq_bits_mask[7] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_hi_lo = out_back_io_deq_bits_mask[6] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_lo_hi = out_back_io_deq_bits_mask[5] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_lo_lo = out_back_io_deq_bits_mask[4] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_hi_hi = out_back_io_deq_bits_mask[3] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_hi_lo = out_back_io_deq_bits_mask[2] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_lo_hi = out_back_io_deq_bits_mask[1] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_lo_lo = out_back_io_deq_bits_mask[0] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [63:0] out_backMask = {out_backMask_hi_hi_hi,out_backMask_hi_hi_lo,out_backMask_hi_lo_hi,out_backMask_hi_lo_lo,
    out_backMask_lo_hi_hi,out_backMask_lo_hi_lo,out_backMask_lo_lo_hi,out_backMask_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [31:0] _out_womask_T_124 = out_backMask[31:0]; // @[ComposableCache.scala 141:30]
  wire  out_womask_124 = &out_backMask[31:0]; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_124 = out_woready_124 & out_womask_124; // @[ComposableCache.scala 141:30]
  wire  out_backSel_32 = _out_backSel_T[32]; // @[ComposableCache.scala 141:30]
  wire  out_woready_91 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_32 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_womask_91 = &out_backMask; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_91 = out_woready_91 & out_womask_91; // @[ComposableCache.scala 141:30]
  wire  flushOutReady = out_f_woready_124 | out_f_woready_91; // @[ComposableCache.scala 279:21 ComposableCache.scala 279:37]
  wire  _GEN_112 = flushOutReady ? 1'h0 : flushOutValid; // @[ComposableCache.scala 270:26 ComposableCache.scala 270:42 ComposableCache.scala 267:33]
  wire [63:0] _flushSelect_T_30 = flushInAddress ^ 64'ha0000c0; // @[Parameters.scala 137:31]
  wire [64:0] _flushSelect_T_31 = {1'b0,$signed(_flushSelect_T_30)}; // @[Parameters.scala 137:49]
  wire [64:0] _flushSelect_T_33 = $signed(_flushSelect_T_31) & -65'sh1fff40; // @[Parameters.scala 137:52]
  wire  _flushSelect_T_34 = $signed(_flushSelect_T_33) == 65'sh0; // @[Parameters.scala 137:67]
  wire [63:0] _flushSelect_T_35 = flushInAddress ^ 64'h8000000c0; // @[Parameters.scala 137:31]
  wire [64:0] _flushSelect_T_36 = {1'b0,$signed(_flushSelect_T_35)}; // @[Parameters.scala 137:49]
  wire [64:0] _flushSelect_T_38 = $signed(_flushSelect_T_36) & -65'sh7ffffff40; // @[Parameters.scala 137:52]
  wire  _flushSelect_T_39 = $signed(_flushSelect_T_38) == 65'sh0; // @[Parameters.scala 137:67]
  wire  flushSelect_3 = _flushSelect_T_34 | _flushSelect_T_39; // @[ComposableCache.scala 413:108]
  wire [63:0] _flushSelect_T_20 = flushInAddress ^ 64'ha000080; // @[Parameters.scala 137:31]
  wire [64:0] _flushSelect_T_21 = {1'b0,$signed(_flushSelect_T_20)}; // @[Parameters.scala 137:49]
  wire [64:0] _flushSelect_T_23 = $signed(_flushSelect_T_21) & -65'sh1fff40; // @[Parameters.scala 137:52]
  wire  _flushSelect_T_24 = $signed(_flushSelect_T_23) == 65'sh0; // @[Parameters.scala 137:67]
  wire [63:0] _flushSelect_T_25 = flushInAddress ^ 64'h800000080; // @[Parameters.scala 137:31]
  wire [64:0] _flushSelect_T_26 = {1'b0,$signed(_flushSelect_T_25)}; // @[Parameters.scala 137:49]
  wire [64:0] _flushSelect_T_28 = $signed(_flushSelect_T_26) & -65'sh7ffffff40; // @[Parameters.scala 137:52]
  wire  _flushSelect_T_29 = $signed(_flushSelect_T_28) == 65'sh0; // @[Parameters.scala 137:67]
  wire  flushSelect_2 = _flushSelect_T_24 | _flushSelect_T_29; // @[ComposableCache.scala 413:108]
  wire [63:0] _flushSelect_T_10 = flushInAddress ^ 64'ha000040; // @[Parameters.scala 137:31]
  wire [64:0] _flushSelect_T_11 = {1'b0,$signed(_flushSelect_T_10)}; // @[Parameters.scala 137:49]
  wire [64:0] _flushSelect_T_13 = $signed(_flushSelect_T_11) & -65'sh1fff40; // @[Parameters.scala 137:52]
  wire  _flushSelect_T_14 = $signed(_flushSelect_T_13) == 65'sh0; // @[Parameters.scala 137:67]
  wire [63:0] _flushSelect_T_15 = flushInAddress ^ 64'h800000040; // @[Parameters.scala 137:31]
  wire [64:0] _flushSelect_T_16 = {1'b0,$signed(_flushSelect_T_15)}; // @[Parameters.scala 137:49]
  wire [64:0] _flushSelect_T_18 = $signed(_flushSelect_T_16) & -65'sh7ffffff40; // @[Parameters.scala 137:52]
  wire  _flushSelect_T_19 = $signed(_flushSelect_T_18) == 65'sh0; // @[Parameters.scala 137:67]
  wire  flushSelect_1 = _flushSelect_T_14 | _flushSelect_T_19; // @[ComposableCache.scala 413:108]
  wire [63:0] _flushSelect_T = flushInAddress ^ 64'ha000000; // @[Parameters.scala 137:31]
  wire [64:0] _flushSelect_T_1 = {1'b0,$signed(_flushSelect_T)}; // @[Parameters.scala 137:49]
  wire [64:0] _flushSelect_T_3 = $signed(_flushSelect_T_1) & -65'sh1fff40; // @[Parameters.scala 137:52]
  wire  _flushSelect_T_4 = $signed(_flushSelect_T_3) == 65'sh0; // @[Parameters.scala 137:67]
  wire [63:0] _flushSelect_T_5 = flushInAddress ^ 64'h800000000; // @[Parameters.scala 137:31]
  wire [64:0] _flushSelect_T_6 = {1'b0,$signed(_flushSelect_T_5)}; // @[Parameters.scala 137:49]
  wire [64:0] _flushSelect_T_8 = $signed(_flushSelect_T_6) & -65'sh7ffffff40; // @[Parameters.scala 137:52]
  wire  _flushSelect_T_9 = $signed(_flushSelect_T_8) == 65'sh0; // @[Parameters.scala 137:67]
  wire  flushSelect = _flushSelect_T_4 | _flushSelect_T_9; // @[ComposableCache.scala 413:108]
  wire  _GEN_809 = flushSelect ? 1'h0 : 1'h1; // @[ComposableCache.scala 414:26 ComposableCache.scala 414:41]
  wire  _GEN_825 = flushSelect_1 ? 1'h0 : _GEN_809; // @[ComposableCache.scala 414:26 ComposableCache.scala 414:41]
  wire  _GEN_841 = flushSelect_2 ? 1'h0 : _GEN_825; // @[ComposableCache.scala 414:26 ComposableCache.scala 414:41]
  wire  flushNoMatch = flushSelect_3 ? 1'h0 : _GEN_841; // @[ComposableCache.scala 414:26 ComposableCache.scala 414:41]
  wire  _T_16 = flushNoMatch & flushInValid; // @[ComposableCache.scala 273:24]
  wire  flushInReady = flushSelect_3 & mods_3_io_req_ready | (flushSelect_2 & mods_2_io_req_ready | (flushSelect_1 &
    mods_1_io_req_ready | (flushSelect & mods_0_io_req_ready | _T_16))); // @[ComposableCache.scala 416:53 ComposableCache.scala 416:68]
  wire  _GEN_113 = flushInReady ? 1'h0 : flushInValid; // @[ComposableCache.scala 271:26 ComposableCache.scala 271:42 ComposableCache.scala 263:33]
  wire  _GEN_115 = flushNoMatch & flushInValid | _GEN_112; // @[ComposableCache.scala 273:41 ComposableCache.scala 275:21]
  reg  injectValid; // @[ComposableCache.scala 303:30]
  reg [7:0] injectBit; // @[ComposableCache.scala 304:30]
  reg  injectType; // @[ComposableCache.scala 305:30]
  wire  injectReady = mods_3_io_error_ready | (mods_2_io_error_ready | (mods_1_io_error_ready | mods_0_io_error_ready)); // @[ComposableCache.scala 428:39 ComposableCache.scala 428:53]
  wire  _GEN_116 = injectReady ? 1'h0 : injectValid; // @[ComposableCache.scala 308:24 ComposableCache.scala 308:38 ComposableCache.scala 303:30]
  wire [31:0] out_prepend_hi_3 = datAddressUncorrected[63:32]; // @[ComposableCache.scala 257:38]
  wire  in_bits_read = auto_in_4_a_bits_opcode == 3'h4; // @[ComposableCache.scala 141:30]
  wire [10:0] in_bits_index = auto_in_4_a_bits_address[13:3]; // @[ComposableCache.scala 141:30 ComposableCache.scala 141:30]
  wire [10:0] out_findex = in_bits_index & 11'h690; // @[ComposableCache.scala 141:30]
  wire  _out_T = out_findex == 11'h0; // @[ComposableCache.scala 141:30]
  wire [7:0] out_frontMask_lo_lo_lo = auto_in_4_a_bits_mask[0] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_frontMask_lo_lo_hi = auto_in_4_a_bits_mask[1] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_frontMask_lo_hi_lo = auto_in_4_a_bits_mask[2] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_frontMask_lo_hi_hi = auto_in_4_a_bits_mask[3] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_frontMask_hi_lo_lo = auto_in_4_a_bits_mask[4] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_frontMask_hi_lo_hi = auto_in_4_a_bits_mask[5] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_frontMask_hi_hi_lo = auto_in_4_a_bits_mask[6] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_frontMask_hi_hi_hi = auto_in_4_a_bits_mask[7] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [63:0] out_frontMask = {out_frontMask_hi_hi_hi,out_frontMask_hi_hi_lo,out_frontMask_hi_lo_hi,
    out_frontMask_hi_lo_lo,out_frontMask_lo_hi_hi,out_frontMask_lo_hi_lo,out_frontMask_lo_lo_hi,out_frontMask_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [7:0] _out_romask_T = out_backMask[7:0]; // @[ComposableCache.scala 141:30]
  wire  out_front_ready = out_back_io_enq_ready; // @[ComposableCache.scala 141:30 Decoupled.scala 299:17]
  wire  _out_rifireMux_T = auto_in_4_a_valid & out_front_ready; // @[ComposableCache.scala 141:30]
  wire  out_iindex_hi_hi_hi = in_bits_index[8]; // @[ComposableCache.scala 141:30]
  wire  out_iindex_hi_hi_lo = in_bits_index[6]; // @[ComposableCache.scala 141:30]
  wire  out_iindex_hi_lo_hi = in_bits_index[5]; // @[ComposableCache.scala 141:30]
  wire  out_iindex_hi_lo_lo = in_bits_index[3]; // @[ComposableCache.scala 141:30]
  wire  out_iindex_lo_hi_hi = in_bits_index[2]; // @[ComposableCache.scala 141:30]
  wire  out_iindex_lo_hi_lo = in_bits_index[1]; // @[ComposableCache.scala 141:30]
  wire  out_iindex_lo_lo = in_bits_index[0]; // @[ComposableCache.scala 141:30]
  wire [6:0] out_iindex = {out_iindex_hi_hi_hi,out_iindex_hi_hi_lo,out_iindex_hi_lo_hi,out_iindex_hi_lo_lo,
    out_iindex_lo_hi_hi,out_iindex_lo_hi_lo,out_iindex_lo_lo}; // @[Cat.scala 30:58]
  wire [127:0] _out_frontSel_T = 128'h1 << out_iindex; // @[OneHot.scala 58:35]
  wire  _out_wifireMux_T_2 = _out_rifireMux_T & ~in_bits_read; // @[ComposableCache.scala 141:30]
  wire  _out_romask_T_4 = out_backMask[0]; // @[ComposableCache.scala 141:30]
  wire  out_womask_4 = &out_backMask[0]; // @[ComposableCache.scala 141:30]
  wire  out_backSel_77 = _out_backSel_T[77]; // @[ComposableCache.scala 141:30]
  wire  out_woready_4 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_77 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_4 = out_woready_4 & out_womask_4; // @[ComposableCache.scala 141:30]
  wire  _out_romask_T_5 = out_backMask[1]; // @[ComposableCache.scala 141:30]
  wire  out_womask_5 = &out_backMask[1]; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_5 = out_woready_4 & out_womask_5; // @[ComposableCache.scala 141:30]
  wire  _out_romask_T_6 = out_backMask[2]; // @[ComposableCache.scala 141:30]
  wire  out_womask_6 = &out_backMask[2]; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_6 = out_woready_4 & out_womask_6; // @[ComposableCache.scala 141:30]
  wire  _out_romask_T_7 = out_backMask[3]; // @[ComposableCache.scala 141:30]
  wire  out_womask_7 = &out_backMask[3]; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_7 = out_woready_4 & out_womask_7; // @[ComposableCache.scala 141:30]
  wire  _out_romask_T_8 = out_backMask[4]; // @[ComposableCache.scala 141:30]
  wire  out_womask_8 = &out_backMask[4]; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_8 = out_woready_4 & out_womask_8; // @[ComposableCache.scala 141:30]
  wire  _out_romask_T_9 = out_backMask[5]; // @[ComposableCache.scala 141:30]
  wire  out_womask_9 = &out_backMask[5]; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_9 = out_woready_4 & out_womask_9; // @[ComposableCache.scala 141:30]
  wire  _out_romask_T_10 = out_backMask[6]; // @[ComposableCache.scala 141:30]
  wire  out_womask_10 = &out_backMask[6]; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_10 = out_woready_4 & out_womask_10; // @[ComposableCache.scala 141:30]
  wire  _out_romask_T_11 = out_backMask[7]; // @[ComposableCache.scala 141:30]
  wire  out_womask_11 = &out_backMask[7]; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_11 = out_woready_4 & out_womask_11; // @[ComposableCache.scala 141:30]
  wire [8:0] out_prepend_10 = {1'h0,wayMap_13_7,wayMap_13_6,wayMap_13_5,wayMap_13_4,wayMap_13_3,wayMap_13_2,wayMap_13_1,
    wayMap_13_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_11 = {{23'd0}, out_prepend_10}; // @[ComposableCache.scala 141:30]
  wire [32:0] out_prepend_11 = {1'h0,out_prepend_lo_11}; // @[Cat.scala 30:58]
  wire [63:0] _out_T_282 = {{31'd0}, out_prepend_11}; // @[ComposableCache.scala 141:30]
  wire [31:0] _out_rimask_T_14 = out_frontMask[31:0]; // @[ComposableCache.scala 141:30]
  wire  out_backSel_65 = _out_backSel_T[65]; // @[ComposableCache.scala 141:30]
  wire  out_woready_15 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_65 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_15 = out_woready_15 & out_womask_4; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_16 = out_woready_15 & out_womask_5; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_17 = out_woready_15 & out_womask_6; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_18 = out_woready_15 & out_womask_7; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_19 = out_woready_15 & out_womask_8; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_20 = out_woready_15 & out_womask_9; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_21 = out_woready_15 & out_womask_10; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_22 = out_woready_15 & out_womask_11; // @[ComposableCache.scala 141:30]
  wire [8:0] out_prepend_19 = {1'h0,wayMap_1_7,wayMap_1_6,wayMap_1_5,wayMap_1_4,wayMap_1_3,wayMap_1_2,wayMap_1_1,
    wayMap_1_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_20 = {{23'd0}, out_prepend_19}; // @[ComposableCache.scala 141:30]
  wire [32:0] out_prepend_20 = {1'h0,out_prepend_lo_20}; // @[Cat.scala 30:58]
  wire [63:0] _out_T_473 = {{31'd0}, out_prepend_20}; // @[ComposableCache.scala 141:30]
  wire  out_backSel_69 = _out_backSel_T[69]; // @[ComposableCache.scala 141:30]
  wire  out_woready_25 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_69 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_25 = out_woready_25 & out_womask_4; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_26 = out_woready_25 & out_womask_5; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_27 = out_woready_25 & out_womask_6; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_28 = out_woready_25 & out_womask_7; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_29 = out_woready_25 & out_womask_8; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_30 = out_woready_25 & out_womask_9; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_31 = out_woready_25 & out_womask_10; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_32 = out_woready_25 & out_womask_11; // @[ComposableCache.scala 141:30]
  wire [8:0] out_prepend_28 = {1'h0,wayMap_5_7,wayMap_5_6,wayMap_5_5,wayMap_5_4,wayMap_5_3,wayMap_5_2,wayMap_5_1,
    wayMap_5_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_29 = {{23'd0}, out_prepend_28}; // @[ComposableCache.scala 141:30]
  wire [32:0] out_prepend_29 = {1'h0,out_prepend_lo_29}; // @[Cat.scala 30:58]
  wire [63:0] _out_T_651 = {{31'd0}, out_prepend_29}; // @[ComposableCache.scala 141:30]
  wire  out_womask_35 = &out_backMask[7:0]; // @[ComposableCache.scala 141:30]
  wire  out_backSel_1 = _out_backSel_T[1]; // @[ComposableCache.scala 141:30]
  wire  out_woready_35 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_1 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_35 = out_woready_35 & out_womask_35; // @[ComposableCache.scala 141:30]
  wire [7:0] _out_T_655 = out_back_io_deq_bits_data[7:0] > 8'h7 ? 8'h7 : out_back_io_deq_bits_data[7:0]; // @[ComposableCache.scala 198:24]
  wire [7:0] _GEN_905 = {{5'd0}, wayEnable}; // @[ComposableCache.scala 199:30]
  wire [7:0] _GEN_142 = out_f_woready_35 & _out_T_655 > _GEN_905 ? _out_T_655 : {{5'd0}, wayEnable}; // @[ComposableCache.scala 199:37 ComposableCache.scala 199:43 ComposableCache.scala 194:24]
  wire  out_backSel_73 = _out_backSel_T[73]; // @[ComposableCache.scala 141:30]
  wire  out_woready_36 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_73 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_36 = out_woready_36 & out_womask_4; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_37 = out_woready_36 & out_womask_5; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_38 = out_woready_36 & out_womask_6; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_39 = out_woready_36 & out_womask_7; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_40 = out_woready_36 & out_womask_8; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_41 = out_woready_36 & out_womask_9; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_42 = out_woready_36 & out_womask_10; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_43 = out_woready_36 & out_womask_11; // @[ComposableCache.scala 141:30]
  wire [8:0] out_prepend_37 = {1'h0,wayMap_9_7,wayMap_9_6,wayMap_9_5,wayMap_9_4,wayMap_9_3,wayMap_9_2,wayMap_9_1,
    wayMap_9_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_38 = {{23'd0}, out_prepend_37}; // @[ComposableCache.scala 141:30]
  wire [32:0] out_prepend_38 = {1'h0,out_prepend_lo_38}; // @[Cat.scala 30:58]
  wire [63:0] _out_T_852 = {{31'd0}, out_prepend_38}; // @[ComposableCache.scala 141:30]
  wire  out_backSel_68 = _out_backSel_T[68]; // @[ComposableCache.scala 141:30]
  wire  out_woready_46 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_68 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_46 = out_woready_46 & out_womask_4; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_47 = out_woready_46 & out_womask_5; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_48 = out_woready_46 & out_womask_6; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_49 = out_woready_46 & out_womask_7; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_50 = out_woready_46 & out_womask_8; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_51 = out_woready_46 & out_womask_9; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_52 = out_woready_46 & out_womask_10; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_53 = out_woready_46 & out_womask_11; // @[ComposableCache.scala 141:30]
  wire [8:0] out_prepend_46 = {1'h0,wayMap_4_7,wayMap_4_6,wayMap_4_5,wayMap_4_4,wayMap_4_3,wayMap_4_2,wayMap_4_1,
    wayMap_4_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_47 = {{23'd0}, out_prepend_46}; // @[ComposableCache.scala 141:30]
  wire [32:0] out_prepend_47 = {1'h0,out_prepend_lo_47}; // @[Cat.scala 30:58]
  wire [63:0] _out_T_1030 = {{31'd0}, out_prepend_47}; // @[ComposableCache.scala 141:30]
  wire  out_backSel_64 = _out_backSel_T[64]; // @[ComposableCache.scala 141:30]
  wire  out_woready_57 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_64 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_57 = out_woready_57 & out_womask_4; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_58 = out_woready_57 & out_womask_5; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_59 = out_woready_57 & out_womask_6; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_60 = out_woready_57 & out_womask_7; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_61 = out_woready_57 & out_womask_8; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_62 = out_woready_57 & out_womask_9; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_63 = out_woready_57 & out_womask_10; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_64 = out_woready_57 & out_womask_11; // @[ComposableCache.scala 141:30]
  wire [8:0] out_prepend_55 = {1'h0,wayMap_0_7,wayMap_0_6,wayMap_0_5,wayMap_0_4,wayMap_0_3,wayMap_0_2,wayMap_0_1,
    wayMap_0_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_56 = {{23'd0}, out_prepend_55}; // @[ComposableCache.scala 141:30]
  wire [32:0] out_prepend_56 = {1'h0,out_prepend_lo_56}; // @[Cat.scala 30:58]
  wire [63:0] _out_T_1221 = {{31'd0}, out_prepend_56}; // @[ComposableCache.scala 141:30]
  wire  out_backSel_74 = _out_backSel_T[74]; // @[ComposableCache.scala 141:30]
  wire  out_woready_68 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_74 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_68 = out_woready_68 & out_womask_4; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_69 = out_woready_68 & out_womask_5; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_70 = out_woready_68 & out_womask_6; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_71 = out_woready_68 & out_womask_7; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_72 = out_woready_68 & out_womask_8; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_73 = out_woready_68 & out_womask_9; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_74 = out_woready_68 & out_womask_10; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_75 = out_woready_68 & out_womask_11; // @[ComposableCache.scala 141:30]
  wire [8:0] out_prepend_64 = {1'h0,wayMap_10_7,wayMap_10_6,wayMap_10_5,wayMap_10_4,wayMap_10_3,wayMap_10_2,wayMap_10_1,
    wayMap_10_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_65 = {{23'd0}, out_prepend_64}; // @[ComposableCache.scala 141:30]
  wire [32:0] out_prepend_65 = {1'h0,out_prepend_lo_65}; // @[Cat.scala 30:58]
  wire [63:0] _out_T_1412 = {{31'd0}, out_prepend_65}; // @[ComposableCache.scala 141:30]
  wire  out_backSel_72 = _out_backSel_T[72]; // @[ComposableCache.scala 141:30]
  wire  out_woready_80 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_72 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_80 = out_woready_80 & out_womask_4; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_81 = out_woready_80 & out_womask_5; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_82 = out_woready_80 & out_womask_6; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_83 = out_woready_80 & out_womask_7; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_84 = out_woready_80 & out_womask_8; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_85 = out_woready_80 & out_womask_9; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_86 = out_woready_80 & out_womask_10; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_87 = out_woready_80 & out_womask_11; // @[ComposableCache.scala 141:30]
  wire [8:0] out_prepend_74 = {1'h0,wayMap_8_7,wayMap_8_6,wayMap_8_5,wayMap_8_4,wayMap_8_3,wayMap_8_2,wayMap_8_1,
    wayMap_8_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_75 = {{23'd0}, out_prepend_74}; // @[ComposableCache.scala 141:30]
  wire [32:0] out_prepend_75 = {1'h0,out_prepend_lo_75}; // @[Cat.scala 30:58]
  wire [63:0] _out_T_1616 = {{31'd0}, out_prepend_75}; // @[ComposableCache.scala 141:30]
  wire  out_wimask_91 = &out_frontMask; // @[ComposableCache.scala 141:30]
  wire  out_frontSel_32 = _out_frontSel_T[32]; // @[ComposableCache.scala 141:30]
  wire  out_wivalid_91 = _out_wifireMux_T_2 & out_frontSel_32 & _out_T; // @[ComposableCache.scala 141:30]
  wire  out_f_wivalid_91 = out_wivalid_91 & out_wimask_91; // @[ComposableCache.scala 141:30]
  wire  _GEN_187 = out_f_wivalid_91 | _GEN_113; // @[ComposableCache.scala 287:21 ComposableCache.scala 287:36]
  wire  _out_T_1632 = ~flushInValid; // @[ComposableCache.scala 288:23]
  wire  _out_T_1633 = out_f_wivalid_91 & ~flushInValid; // @[ComposableCache.scala 288:20]
  wire  out_wifireMux_out_32 = _out_T_1632 | ~out_wimask_91; // @[ComposableCache.scala 141:30]
  wire  out_wofireMux_out_32 = flushOutValid | ~out_womask_91; // @[ComposableCache.scala 141:30]
  wire [31:0] out_prepend_lo_76 = datAddressUncorrected[31:0]; // @[ComposableCache.scala 141:30]
  wire [63:0] out_prepend_76 = {out_prepend_hi_3,out_prepend_lo_76}; // @[Cat.scala 30:58]
  wire  out_backSel_67 = _out_backSel_T[67]; // @[ComposableCache.scala 141:30]
  wire  out_woready_94 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_67 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_94 = out_woready_94 & out_womask_4; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_95 = out_woready_94 & out_womask_5; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_96 = out_woready_94 & out_womask_6; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_97 = out_woready_94 & out_womask_7; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_98 = out_woready_94 & out_womask_8; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_99 = out_woready_94 & out_womask_9; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_100 = out_woready_94 & out_womask_10; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_101 = out_woready_94 & out_womask_11; // @[ComposableCache.scala 141:30]
  wire [8:0] out_prepend_84 = {1'h0,wayMap_3_7,wayMap_3_6,wayMap_3_5,wayMap_3_4,wayMap_3_3,wayMap_3_2,wayMap_3_1,
    wayMap_3_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_85 = {{23'd0}, out_prepend_84}; // @[ComposableCache.scala 141:30]
  wire [32:0] out_prepend_85 = {1'h0,out_prepend_lo_85}; // @[Cat.scala 30:58]
  wire [63:0] _out_T_1850 = {{31'd0}, out_prepend_85}; // @[ComposableCache.scala 141:30]
  wire  out_backSel_75 = _out_backSel_T[75]; // @[ComposableCache.scala 141:30]
  wire  out_woready_104 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_75 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_104 = out_woready_104 & out_womask_4; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_105 = out_woready_104 & out_womask_5; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_106 = out_woready_104 & out_womask_6; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_107 = out_woready_104 & out_womask_7; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_108 = out_woready_104 & out_womask_8; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_109 = out_woready_104 & out_womask_9; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_110 = out_woready_104 & out_womask_10; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_111 = out_woready_104 & out_womask_11; // @[ComposableCache.scala 141:30]
  wire [8:0] out_prepend_93 = {1'h0,wayMap_11_7,wayMap_11_6,wayMap_11_5,wayMap_11_4,wayMap_11_3,wayMap_11_2,wayMap_11_1,
    wayMap_11_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_94 = {{23'd0}, out_prepend_93}; // @[ComposableCache.scala 141:30]
  wire [32:0] out_prepend_94 = {1'h0,out_prepend_lo_94}; // @[Cat.scala 30:58]
  wire [63:0] _out_T_2028 = {{31'd0}, out_prepend_94}; // @[ComposableCache.scala 141:30]
  wire  out_backSel_71 = _out_backSel_T[71]; // @[ComposableCache.scala 141:30]
  wire  out_woready_114 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_71 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_114 = out_woready_114 & out_womask_4; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_115 = out_woready_114 & out_womask_5; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_116 = out_woready_114 & out_womask_6; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_117 = out_woready_114 & out_womask_7; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_118 = out_woready_114 & out_womask_8; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_119 = out_woready_114 & out_womask_9; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_120 = out_woready_114 & out_womask_10; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_121 = out_woready_114 & out_womask_11; // @[ComposableCache.scala 141:30]
  wire [8:0] out_prepend_102 = {1'h0,wayMap_7_7,wayMap_7_6,wayMap_7_5,wayMap_7_4,wayMap_7_3,wayMap_7_2,wayMap_7_1,
    wayMap_7_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_103 = {{23'd0}, out_prepend_102}; // @[ComposableCache.scala 141:30]
  wire [32:0] out_prepend_103 = {1'h0,out_prepend_lo_103}; // @[Cat.scala 30:58]
  wire [63:0] _out_T_2206 = {{31'd0}, out_prepend_103}; // @[ComposableCache.scala 141:30]
  wire  out_wimask_124 = &out_frontMask[31:0]; // @[ComposableCache.scala 141:30]
  wire  out_frontSel_40 = _out_frontSel_T[40]; // @[ComposableCache.scala 141:30]
  wire  out_wivalid_124 = _out_wifireMux_T_2 & out_frontSel_40 & _out_T; // @[ComposableCache.scala 141:30]
  wire  out_f_wivalid_124 = out_wivalid_124 & out_wimask_124; // @[ComposableCache.scala 141:30]
  wire  _out_T_2210 = out_f_wivalid_124 & _out_T_1632; // @[ComposableCache.scala 281:20]
  wire [35:0] _out_flushInAddress_T = {auto_in_4_a_bits_data[31:0], 4'h0}; // @[ComposableCache.scala 281:63]
  wire  out_wifireMux_out_40 = _out_T_1632 | ~out_wimask_124; // @[ComposableCache.scala 141:30]
  wire  out_wofireMux_out_40 = flushOutValid | ~out_womask_124; // @[ComposableCache.scala 141:30]
  wire  out_backSel_66 = _out_backSel_T[66]; // @[ComposableCache.scala 141:30]
  wire  out_woready_127 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_66 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_127 = out_woready_127 & out_womask_4; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_128 = out_woready_127 & out_womask_5; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_129 = out_woready_127 & out_womask_6; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_130 = out_woready_127 & out_womask_7; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_131 = out_woready_127 & out_womask_8; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_132 = out_woready_127 & out_womask_9; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_133 = out_woready_127 & out_womask_10; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_134 = out_woready_127 & out_womask_11; // @[ComposableCache.scala 141:30]
  wire [8:0] out_prepend_112 = {1'h0,wayMap_2_7,wayMap_2_6,wayMap_2_5,wayMap_2_4,wayMap_2_3,wayMap_2_2,wayMap_2_1,
    wayMap_2_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_113 = {{23'd0}, out_prepend_112}; // @[ComposableCache.scala 141:30]
  wire [32:0] out_prepend_113 = {1'h0,out_prepend_lo_113}; // @[Cat.scala 30:58]
  wire [63:0] _out_T_2427 = {{31'd0}, out_prepend_113}; // @[ComposableCache.scala 141:30]
  wire  out_backSel_8 = _out_backSel_T[8]; // @[ComposableCache.scala 141:30]
  wire  out_woready_137 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_8 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_137 = out_woready_137 & out_womask_35; // @[ComposableCache.scala 141:30]
  wire [8:0] out_prepend_114 = {1'h0,injectBit}; // @[Cat.scala 30:58]
  wire [15:0] out_prepend_lo_115 = {{7'd0}, out_prepend_114}; // @[ComposableCache.scala 141:30]
  wire  _out_romask_T_139 = out_backMask[16]; // @[ComposableCache.scala 141:30]
  wire  out_womask_139 = &out_backMask[16]; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_139 = out_woready_137 & out_womask_139; // @[ComposableCache.scala 141:30]
  wire [16:0] out_prepend_115 = {injectType,out_prepend_lo_115}; // @[Cat.scala 30:58]
  wire  out_backSel_70 = _out_backSel_T[70]; // @[ComposableCache.scala 141:30]
  wire  out_woready_142 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_70 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_142 = out_woready_142 & out_womask_4; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_143 = out_woready_142 & out_womask_5; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_144 = out_woready_142 & out_womask_6; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_145 = out_woready_142 & out_womask_7; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_146 = out_woready_142 & out_womask_8; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_147 = out_woready_142 & out_womask_9; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_148 = out_woready_142 & out_womask_10; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_149 = out_woready_142 & out_womask_11; // @[ComposableCache.scala 141:30]
  wire [8:0] out_prepend_124 = {1'h0,wayMap_6_7,wayMap_6_6,wayMap_6_5,wayMap_6_4,wayMap_6_3,wayMap_6_2,wayMap_6_1,
    wayMap_6_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_125 = {{23'd0}, out_prepend_124}; // @[ComposableCache.scala 141:30]
  wire [32:0] out_prepend_125 = {1'h0,out_prepend_lo_125}; // @[Cat.scala 30:58]
  wire [63:0] _out_T_2682 = {{31'd0}, out_prepend_125}; // @[ComposableCache.scala 141:30]
  wire  out_backSel_76 = _out_backSel_T[76]; // @[ComposableCache.scala 141:30]
  wire  out_woready_152 = _out_wofireMux_T & ~out_back_io_deq_bits_read & out_backSel_76 & _out_T_41; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_152 = out_woready_152 & out_womask_4; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_153 = out_woready_152 & out_womask_5; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_154 = out_woready_152 & out_womask_6; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_155 = out_woready_152 & out_womask_7; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_156 = out_woready_152 & out_womask_8; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_157 = out_woready_152 & out_womask_9; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_158 = out_woready_152 & out_womask_10; // @[ComposableCache.scala 141:30]
  wire  out_f_woready_159 = out_woready_152 & out_womask_11; // @[ComposableCache.scala 141:30]
  wire [8:0] out_prepend_133 = {1'h0,wayMap_12_7,wayMap_12_6,wayMap_12_5,wayMap_12_4,wayMap_12_3,wayMap_12_2,wayMap_12_1
    ,wayMap_12_0}; // @[Cat.scala 30:58]
  wire [31:0] out_prepend_lo_134 = {{23'd0}, out_prepend_133}; // @[ComposableCache.scala 141:30]
  wire [32:0] out_prepend_134 = {1'h0,out_prepend_lo_134}; // @[Cat.scala 30:58]
  wire [63:0] _out_T_2860 = {{31'd0}, out_prepend_134}; // @[ComposableCache.scala 141:30]
  wire  _out_wifireMux_T_134 = out_wifireMux_out_32 | ~_out_T; // @[ComposableCache.scala 141:30]
  wire  _out_wifireMux_T_166 = out_wifireMux_out_40 | ~_out_T; // @[ComposableCache.scala 141:30]
  wire  _GEN_403 = 7'h20 == out_iindex ? _out_wifireMux_T_134 : 1'h1; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_411 = 7'h28 == out_iindex ? _out_wifireMux_T_166 : 7'h27 == out_iindex | (7'h26 == out_iindex | (7'h25 ==
    out_iindex | (7'h24 == out_iindex | (7'h23 == out_iindex | (7'h22 == out_iindex | (7'h21 == out_iindex | _GEN_403)))
    ))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_426 = 7'h37 == out_iindex | (7'h36 == out_iindex | (7'h35 == out_iindex | (7'h34 == out_iindex | (7'h33 ==
    out_iindex | (7'h32 == out_iindex | (7'h31 == out_iindex | (7'h30 == out_iindex | (7'h2f == out_iindex | (7'h2e ==
    out_iindex | (7'h2d == out_iindex | (7'h2c == out_iindex | (7'h2b == out_iindex | (7'h2a == out_iindex | (7'h29 ==
    out_iindex | _GEN_411)))))))))))))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_441 = 7'h46 == out_iindex | (7'h45 == out_iindex | (7'h44 == out_iindex | (7'h43 == out_iindex | (7'h42 ==
    out_iindex | (7'h41 == out_iindex | (7'h40 == out_iindex | (7'h3f == out_iindex | (7'h3e == out_iindex | (7'h3d ==
    out_iindex | (7'h3c == out_iindex | (7'h3b == out_iindex | (7'h3a == out_iindex | (7'h39 == out_iindex | (7'h38 ==
    out_iindex | _GEN_426)))))))))))))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_456 = 7'h55 == out_iindex | (7'h54 == out_iindex | (7'h53 == out_iindex | (7'h52 == out_iindex | (7'h51 ==
    out_iindex | (7'h50 == out_iindex | (7'h4f == out_iindex | (7'h4e == out_iindex | (7'h4d == out_iindex | (7'h4c ==
    out_iindex | (7'h4b == out_iindex | (7'h4a == out_iindex | (7'h49 == out_iindex | (7'h48 == out_iindex | (7'h47 ==
    out_iindex | _GEN_441)))))))))))))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_471 = 7'h64 == out_iindex | (7'h63 == out_iindex | (7'h62 == out_iindex | (7'h61 == out_iindex | (7'h60 ==
    out_iindex | (7'h5f == out_iindex | (7'h5e == out_iindex | (7'h5d == out_iindex | (7'h5c == out_iindex | (7'h5b ==
    out_iindex | (7'h5a == out_iindex | (7'h59 == out_iindex | (7'h58 == out_iindex | (7'h57 == out_iindex | (7'h56 ==
    out_iindex | _GEN_456)))))))))))))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_486 = 7'h73 == out_iindex | (7'h72 == out_iindex | (7'h71 == out_iindex | (7'h70 == out_iindex | (7'h6f ==
    out_iindex | (7'h6e == out_iindex | (7'h6d == out_iindex | (7'h6c == out_iindex | (7'h6b == out_iindex | (7'h6a ==
    out_iindex | (7'h69 == out_iindex | (7'h68 == out_iindex | (7'h67 == out_iindex | (7'h66 == out_iindex | (7'h65 ==
    out_iindex | _GEN_471)))))))))))))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  out_wifireMux = 7'h7f == out_iindex | (7'h7e == out_iindex | (7'h7d == out_iindex | (7'h7c == out_iindex | (7'h7b
     == out_iindex | (7'h7a == out_iindex | (7'h79 == out_iindex | (7'h78 == out_iindex | (7'h77 == out_iindex | (7'h76
     == out_iindex | (7'h75 == out_iindex | (7'h74 == out_iindex | _GEN_486))))))))))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _out_wofireMux_T_134 = out_wofireMux_out_32 | ~_out_T_41; // @[ComposableCache.scala 141:30]
  wire  _out_wofireMux_T_166 = out_wofireMux_out_40 | ~_out_T_41; // @[ComposableCache.scala 141:30]
  wire  _GEN_659 = 7'h20 == out_oindex ? _out_wofireMux_T_134 : 1'h1; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_667 = 7'h28 == out_oindex ? _out_wofireMux_T_166 : 7'h27 == out_oindex | (7'h26 == out_oindex | (7'h25 ==
    out_oindex | (7'h24 == out_oindex | (7'h23 == out_oindex | (7'h22 == out_oindex | (7'h21 == out_oindex | _GEN_659)))
    ))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_682 = 7'h37 == out_oindex | (7'h36 == out_oindex | (7'h35 == out_oindex | (7'h34 == out_oindex | (7'h33 ==
    out_oindex | (7'h32 == out_oindex | (7'h31 == out_oindex | (7'h30 == out_oindex | (7'h2f == out_oindex | (7'h2e ==
    out_oindex | (7'h2d == out_oindex | (7'h2c == out_oindex | (7'h2b == out_oindex | (7'h2a == out_oindex | (7'h29 ==
    out_oindex | _GEN_667)))))))))))))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_1030 = 7'h40 == out_oindex; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_1031 = 7'h41 == out_oindex; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_1032 = 7'h42 == out_oindex; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_1033 = 7'h43 == out_oindex; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_1034 = 7'h44 == out_oindex; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_1035 = 7'h45 == out_oindex; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_1036 = 7'h46 == out_oindex; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_697 = 7'h46 == out_oindex | (7'h45 == out_oindex | (7'h44 == out_oindex | (7'h43 == out_oindex | (7'h42 ==
    out_oindex | (7'h41 == out_oindex | (7'h40 == out_oindex | (7'h3f == out_oindex | (7'h3e == out_oindex | (7'h3d ==
    out_oindex | (7'h3c == out_oindex | (7'h3b == out_oindex | (7'h3a == out_oindex | (7'h39 == out_oindex | (7'h38 ==
    out_oindex | _GEN_682)))))))))))))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_1037 = 7'h47 == out_oindex; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_1038 = 7'h48 == out_oindex; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_1039 = 7'h49 == out_oindex; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_1040 = 7'h4a == out_oindex; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_1041 = 7'h4b == out_oindex; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_1042 = 7'h4c == out_oindex; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_1043 = 7'h4d == out_oindex; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_712 = 7'h55 == out_oindex | (7'h54 == out_oindex | (7'h53 == out_oindex | (7'h52 == out_oindex | (7'h51 ==
    out_oindex | (7'h50 == out_oindex | (7'h4f == out_oindex | (7'h4e == out_oindex | (7'h4d == out_oindex | (7'h4c ==
    out_oindex | (7'h4b == out_oindex | (7'h4a == out_oindex | (7'h49 == out_oindex | (7'h48 == out_oindex | (7'h47 ==
    out_oindex | _GEN_697)))))))))))))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_727 = 7'h64 == out_oindex | (7'h63 == out_oindex | (7'h62 == out_oindex | (7'h61 == out_oindex | (7'h60 ==
    out_oindex | (7'h5f == out_oindex | (7'h5e == out_oindex | (7'h5d == out_oindex | (7'h5c == out_oindex | (7'h5b ==
    out_oindex | (7'h5a == out_oindex | (7'h59 == out_oindex | (7'h58 == out_oindex | (7'h57 == out_oindex | (7'h56 ==
    out_oindex | _GEN_712)))))))))))))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  _GEN_742 = 7'h73 == out_oindex | (7'h72 == out_oindex | (7'h71 == out_oindex | (7'h70 == out_oindex | (7'h6f ==
    out_oindex | (7'h6e == out_oindex | (7'h6d == out_oindex | (7'h6c == out_oindex | (7'h6b == out_oindex | (7'h6a ==
    out_oindex | (7'h69 == out_oindex | (7'h68 == out_oindex | (7'h67 == out_oindex | (7'h66 == out_oindex | (7'h65 ==
    out_oindex | _GEN_727)))))))))))))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  out_wofireMux = 7'h7f == out_oindex | (7'h7e == out_oindex | (7'h7d == out_oindex | (7'h7c == out_oindex | (7'h7b
     == out_oindex | (7'h7a == out_oindex | (7'h79 == out_oindex | (7'h78 == out_oindex | (7'h77 == out_oindex | (7'h76
     == out_oindex | (7'h75 == out_oindex | (7'h74 == out_oindex | _GEN_742))))))))))); // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  out_iready = in_bits_read | out_wifireMux; // @[ComposableCache.scala 141:30]
  wire  out_oready = out_back_io_deq_bits_read | out_wofireMux; // @[ComposableCache.scala 141:30]
  wire  _out_out_bits_data_T = 7'h0 == out_oindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_1 = 7'h1 == out_oindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_2 = 7'h8 == out_oindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_3 = 7'h10 == out_oindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_4 = 7'h11 == out_oindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_5 = 7'h14 == out_oindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_6 = 7'h15 == out_oindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_7 = 7'h18 == out_oindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_8 = 7'h19 == out_oindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_9 = 7'h1c == out_oindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_10 = 7'h1d == out_oindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_11 = 7'h20 == out_oindex; // @[Conditional.scala 37:30]
  wire  _out_out_bits_data_T_12 = 7'h28 == out_oindex; // @[Conditional.scala 37:30]
  wire  _GEN_755 = _GEN_1043 ? _out_T_41 : 1'h1; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_756 = _GEN_1042 ? _out_T_41 : _GEN_755; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_757 = _GEN_1041 ? _out_T_41 : _GEN_756; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_758 = _GEN_1040 ? _out_T_41 : _GEN_757; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_759 = _GEN_1039 ? _out_T_41 : _GEN_758; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_760 = _GEN_1038 ? _out_T_41 : _GEN_759; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_761 = _GEN_1037 ? _out_T_41 : _GEN_760; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_762 = _GEN_1036 ? _out_T_41 : _GEN_761; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_763 = _GEN_1035 ? _out_T_41 : _GEN_762; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_764 = _GEN_1034 ? _out_T_41 : _GEN_763; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_765 = _GEN_1033 ? _out_T_41 : _GEN_764; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_766 = _GEN_1032 ? _out_T_41 : _GEN_765; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_767 = _GEN_1031 ? _out_T_41 : _GEN_766; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_768 = _GEN_1030 ? _out_T_41 : _GEN_767; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_769 = _out_out_bits_data_T_12 ? _out_T_41 : _GEN_768; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_770 = _out_out_bits_data_T_11 ? _out_T_41 : _GEN_769; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_771 = _out_out_bits_data_T_10 ? _out_T_41 : _GEN_770; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_772 = _out_out_bits_data_T_9 ? _out_T_41 : _GEN_771; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_773 = _out_out_bits_data_T_8 ? _out_T_41 : _GEN_772; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_774 = _out_out_bits_data_T_7 ? _out_T_41 : _GEN_773; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_775 = _out_out_bits_data_T_6 ? _out_T_41 : _GEN_774; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_776 = _out_out_bits_data_T_5 ? _out_T_41 : _GEN_775; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_777 = _out_out_bits_data_T_4 ? _out_T_41 : _GEN_776; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_778 = _out_out_bits_data_T_3 ? _out_T_41 : _GEN_777; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_779 = _out_out_bits_data_T_2 ? _out_T_41 : _GEN_778; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  _GEN_780 = _out_out_bits_data_T_1 ? _out_T_41 : _GEN_779; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire  out_out_bits_data_out = _out_out_bits_data_T ? _out_T_41 : _GEN_780; // @[Conditional.scala 40:58 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_782 = _GEN_1043 ? _out_T_282 : 64'h0; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_783 = _GEN_1042 ? _out_T_2860 : _GEN_782; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_784 = _GEN_1041 ? _out_T_2028 : _GEN_783; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_785 = _GEN_1040 ? _out_T_1412 : _GEN_784; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_786 = _GEN_1039 ? _out_T_852 : _GEN_785; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_787 = _GEN_1038 ? _out_T_1616 : _GEN_786; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_788 = _GEN_1037 ? _out_T_2206 : _GEN_787; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_789 = _GEN_1036 ? _out_T_2682 : _GEN_788; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_790 = _GEN_1035 ? _out_T_651 : _GEN_789; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_791 = _GEN_1034 ? _out_T_1030 : _GEN_790; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_792 = _GEN_1033 ? _out_T_1850 : _GEN_791; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_793 = _GEN_1032 ? _out_T_2427 : _GEN_792; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_794 = _GEN_1031 ? _out_T_473 : _GEN_793; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_795 = _GEN_1030 ? _out_T_1221 : _GEN_794; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_796 = _out_out_bits_data_T_12 ? 64'h0 : _GEN_795; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_797 = _out_out_bits_data_T_11 ? 64'h0 : _GEN_796; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_798 = _out_out_bits_data_T_10 ? {{32'd0}, datNumberUncorrected} : _GEN_797; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_799 = _out_out_bits_data_T_9 ? out_prepend_76 : _GEN_798; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_800 = _out_out_bits_data_T_8 ? {{32'd0}, datNumberCorrected} : _GEN_799; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_801 = _out_out_bits_data_T_7 ? 64'h0 : _GEN_800; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_802 = _out_out_bits_data_T_6 ? {{32'd0}, dirNumberUncorrected} : _GEN_801; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_803 = _out_out_bits_data_T_5 ? 64'h0 : _GEN_802; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_804 = _out_out_bits_data_T_4 ? {{32'd0}, dirNumberCorrected} : _GEN_803; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_805 = _out_out_bits_data_T_3 ? 64'h0 : _GEN_804; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_806 = _out_out_bits_data_T_2 ? {{47'd0}, out_prepend_115} : _GEN_805; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] _GEN_807 = _out_out_bits_data_T_1 ? {{56'd0}, _GEN_905} : _GEN_806; // @[Conditional.scala 39:67 MuxLiteral.scala 53:32]
  wire [63:0] out_out_bits_data_out_1 = _out_out_bits_data_T ? 64'h60a0804 : _GEN_807; // @[Conditional.scala 40:58 MuxLiteral.scala 53:32]
  wire  out_bits_read = out_back_io_deq_bits_read; // @[ComposableCache.scala 141:30 ComposableCache.scala 141:30]
  wire [32:0] _dirNumberCorrected_T_6 = {{1'd0}, dirNumberCorrected}; // @[ComposableCache.scala 498:52]
  wire [32:0] _dirNumberUncorrected_T_6 = {{1'd0}, dirNumberUncorrected}; // @[ComposableCache.scala 499:52]
  wire [32:0] _datNumberCorrected_T_16 = {{1'd0}, datNumberCorrected}; // @[ComposableCache.scala 500:52]
  wire [1:0] _datNumberUncorrected_T = {{1'd0}, sideband_io_uncorrected_valid}; // @[Bitwise.scala 47:55]
  wire [2:0] _datNumberUncorrected_T_4 = {{1'd0}, _datNumberUncorrected_T}; // @[Bitwise.scala 47:55]
  wire [3:0] _datNumberUncorrected_T_14 = {{1'd0}, _datNumberUncorrected_T_4}; // @[Bitwise.scala 47:55]
  wire [31:0] _GEN_1094 = {{28'd0}, _datNumberUncorrected_T_14}; // @[ComposableCache.scala 501:52]
  wire  bundleIn_0_a_ready = mods_0_io_in_a_ready; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_0_a_valid = auto_in_0_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_0_a_bits_opcode = auto_in_0_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_0_a_bits_param = auto_in_0_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_0_a_bits_size = auto_in_0_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [6:0] bundleIn_0_a_bits_source = auto_in_0_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [35:0] bundleIn_0_a_bits_address = auto_in_0_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [15:0] bundleIn_0_a_bits_mask = auto_in_0_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [127:0] bundleIn_0_a_bits_data = auto_in_0_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_0_a_bits_corrupt = auto_in_0_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_0_b_ready = auto_in_0_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_0_b_valid = mods_0_io_in_b_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_0_b_bits_opcode = 3'h6; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [1:0] bundleIn_0_b_bits_param = mods_0_io_in_b_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_0_b_bits_size = 3'h6; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [6:0] bundleIn_0_b_bits_source = mods_0_io_in_b_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [35:0] bundleIn_0_b_bits_address = mods_0_io_in_b_bits_address; // @[Parameters.scala 260:14]
  wire [15:0] bundleIn_0_b_bits_mask = 16'hffff; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [127:0] bundleIn_0_b_bits_data = 128'h0; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_0_b_bits_corrupt = 1'h0; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_0_c_ready = mods_0_io_in_c_ready; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_0_c_valid = auto_in_0_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_0_c_bits_opcode = auto_in_0_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_0_c_bits_param = auto_in_0_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_0_c_bits_size = auto_in_0_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [6:0] bundleIn_0_c_bits_source = auto_in_0_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [35:0] bundleIn_0_c_bits_address = auto_in_0_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [127:0] bundleIn_0_c_bits_data = auto_in_0_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_0_c_bits_corrupt = auto_in_0_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_0_d_ready = auto_in_0_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_0_d_valid = mods_0_io_in_d_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_0_d_bits_opcode = mods_0_io_in_d_bits_opcode; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [1:0] bundleIn_0_d_bits_param = mods_0_io_in_d_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_0_d_bits_size = mods_0_io_in_d_bits_size; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [6:0] bundleIn_0_d_bits_source = mods_0_io_in_d_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_0_d_bits_sink = mods_0_io_in_d_bits_sink; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_0_d_bits_denied = mods_0_io_in_d_bits_denied; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [127:0] bundleIn_0_d_bits_data = mods_0_io_in_d_bits_data; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_0_d_bits_corrupt = mods_0_io_in_d_bits_corrupt; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_0_e_ready = 1'h1; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_0_e_valid = auto_in_0_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_0_e_bits_sink = auto_in_0_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_1_a_ready = mods_1_io_in_a_ready; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_1_a_valid = auto_in_1_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_1_a_bits_opcode = auto_in_1_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_1_a_bits_param = auto_in_1_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_1_a_bits_size = auto_in_1_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [6:0] bundleIn_1_a_bits_source = auto_in_1_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [35:0] bundleIn_1_a_bits_address = auto_in_1_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [15:0] bundleIn_1_a_bits_mask = auto_in_1_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [127:0] bundleIn_1_a_bits_data = auto_in_1_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_1_a_bits_corrupt = auto_in_1_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_1_b_ready = auto_in_1_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_1_b_valid = mods_1_io_in_b_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_1_b_bits_opcode = 3'h6; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [1:0] bundleIn_1_b_bits_param = mods_1_io_in_b_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_1_b_bits_size = 3'h6; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [6:0] bundleIn_1_b_bits_source = mods_1_io_in_b_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [35:0] bundleIn_1_b_bits_address = mods_1_io_in_b_bits_address | 36'h40; // @[Parameters.scala 260:14]
  wire [15:0] bundleIn_1_b_bits_mask = 16'hffff; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [127:0] bundleIn_1_b_bits_data = 128'h0; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_1_b_bits_corrupt = 1'h0; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_1_c_ready = mods_1_io_in_c_ready; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_1_c_valid = auto_in_1_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_1_c_bits_opcode = auto_in_1_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_1_c_bits_param = auto_in_1_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_1_c_bits_size = auto_in_1_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [6:0] bundleIn_1_c_bits_source = auto_in_1_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [35:0] bundleIn_1_c_bits_address = auto_in_1_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [127:0] bundleIn_1_c_bits_data = auto_in_1_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_1_c_bits_corrupt = auto_in_1_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_1_d_ready = auto_in_1_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_1_d_valid = mods_1_io_in_d_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_1_d_bits_opcode = mods_1_io_in_d_bits_opcode; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [1:0] bundleIn_1_d_bits_param = mods_1_io_in_d_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_1_d_bits_size = mods_1_io_in_d_bits_size; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [6:0] bundleIn_1_d_bits_source = mods_1_io_in_d_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_1_d_bits_sink = mods_1_io_in_d_bits_sink; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_1_d_bits_denied = mods_1_io_in_d_bits_denied; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [127:0] bundleIn_1_d_bits_data = mods_1_io_in_d_bits_data; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_1_d_bits_corrupt = mods_1_io_in_d_bits_corrupt; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_1_e_ready = 1'h1; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_1_e_valid = auto_in_1_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_1_e_bits_sink = auto_in_1_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_2_a_ready = mods_2_io_in_a_ready; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_2_a_valid = auto_in_2_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_2_a_bits_opcode = auto_in_2_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_2_a_bits_param = auto_in_2_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_2_a_bits_size = auto_in_2_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [6:0] bundleIn_2_a_bits_source = auto_in_2_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [35:0] bundleIn_2_a_bits_address = auto_in_2_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [15:0] bundleIn_2_a_bits_mask = auto_in_2_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [127:0] bundleIn_2_a_bits_data = auto_in_2_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_2_a_bits_corrupt = auto_in_2_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_2_b_ready = auto_in_2_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_2_b_valid = mods_2_io_in_b_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_2_b_bits_opcode = 3'h6; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [1:0] bundleIn_2_b_bits_param = mods_2_io_in_b_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_2_b_bits_size = 3'h6; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [6:0] bundleIn_2_b_bits_source = mods_2_io_in_b_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [35:0] bundleIn_2_b_bits_address = mods_2_io_in_b_bits_address | 36'h80; // @[Parameters.scala 260:14]
  wire [15:0] bundleIn_2_b_bits_mask = 16'hffff; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [127:0] bundleIn_2_b_bits_data = 128'h0; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_2_b_bits_corrupt = 1'h0; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_2_c_ready = mods_2_io_in_c_ready; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_2_c_valid = auto_in_2_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_2_c_bits_opcode = auto_in_2_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_2_c_bits_param = auto_in_2_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_2_c_bits_size = auto_in_2_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [6:0] bundleIn_2_c_bits_source = auto_in_2_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [35:0] bundleIn_2_c_bits_address = auto_in_2_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [127:0] bundleIn_2_c_bits_data = auto_in_2_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_2_c_bits_corrupt = auto_in_2_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_2_d_ready = auto_in_2_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_2_d_valid = mods_2_io_in_d_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_2_d_bits_opcode = mods_2_io_in_d_bits_opcode; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [1:0] bundleIn_2_d_bits_param = mods_2_io_in_d_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_2_d_bits_size = mods_2_io_in_d_bits_size; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [6:0] bundleIn_2_d_bits_source = mods_2_io_in_d_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_2_d_bits_sink = mods_2_io_in_d_bits_sink; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_2_d_bits_denied = mods_2_io_in_d_bits_denied; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [127:0] bundleIn_2_d_bits_data = mods_2_io_in_d_bits_data; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_2_d_bits_corrupt = mods_2_io_in_d_bits_corrupt; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_2_e_ready = 1'h1; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_2_e_valid = auto_in_2_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_2_e_bits_sink = auto_in_2_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_3_a_ready = mods_3_io_in_a_ready; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_3_a_valid = auto_in_3_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_3_a_bits_opcode = auto_in_3_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_3_a_bits_param = auto_in_3_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_3_a_bits_size = auto_in_3_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [6:0] bundleIn_3_a_bits_source = auto_in_3_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [35:0] bundleIn_3_a_bits_address = auto_in_3_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [15:0] bundleIn_3_a_bits_mask = auto_in_3_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [127:0] bundleIn_3_a_bits_data = auto_in_3_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_3_a_bits_corrupt = auto_in_3_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_3_b_ready = auto_in_3_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_3_b_valid = mods_3_io_in_b_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_3_b_bits_opcode = 3'h6; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [1:0] bundleIn_3_b_bits_param = mods_3_io_in_b_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_3_b_bits_size = 3'h6; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [6:0] bundleIn_3_b_bits_source = mods_3_io_in_b_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [35:0] bundleIn_3_b_bits_address = mods_3_io_in_b_bits_address | 36'hc0; // @[Parameters.scala 260:14]
  wire [15:0] bundleIn_3_b_bits_mask = 16'hffff; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [127:0] bundleIn_3_b_bits_data = 128'h0; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_3_b_bits_corrupt = 1'h0; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_3_c_ready = mods_3_io_in_c_ready; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_3_c_valid = auto_in_3_c_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_3_c_bits_opcode = auto_in_3_c_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_3_c_bits_param = auto_in_3_c_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_3_c_bits_size = auto_in_3_c_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [6:0] bundleIn_3_c_bits_source = auto_in_3_c_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [35:0] bundleIn_3_c_bits_address = auto_in_3_c_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [127:0] bundleIn_3_c_bits_data = auto_in_3_c_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_3_c_bits_corrupt = auto_in_3_c_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_3_d_ready = auto_in_3_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire  bundleIn_3_d_valid = mods_3_io_in_d_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_3_d_bits_opcode = mods_3_io_in_d_bits_opcode; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [1:0] bundleIn_3_d_bits_param = mods_3_io_in_d_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_3_d_bits_size = mods_3_io_in_d_bits_size; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [6:0] bundleIn_3_d_bits_source = mods_3_io_in_d_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [2:0] bundleIn_3_d_bits_sink = mods_3_io_in_d_bits_sink; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_3_d_bits_denied = mods_3_io_in_d_bits_denied; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire [127:0] bundleIn_3_d_bits_data = mods_3_io_in_d_bits_data; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_3_d_bits_corrupt = mods_3_io_in_d_bits_corrupt; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_3_e_ready = 1'h1; // @[Nodes.scala 1529:13 ComposableCache.scala 410:23]
  wire  bundleIn_3_e_valid = auto_in_3_e_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  wire [2:0] bundleIn_3_e_bits_sink = auto_in_3_e_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  RHEA__Queue_119 out_back ( // @[Decoupled.scala 296:21]
    .rf_reset(out_back_rf_reset),
    .clock(out_back_clock),
    .reset(out_back_reset),
    .io_enq_ready(out_back_io_enq_ready),
    .io_enq_valid(out_back_io_enq_valid),
    .io_enq_bits_read(out_back_io_enq_bits_read),
    .io_enq_bits_index(out_back_io_enq_bits_index),
    .io_enq_bits_data(out_back_io_enq_bits_data),
    .io_enq_bits_mask(out_back_io_enq_bits_mask),
    .io_enq_bits_extra_tlrr_extra_source(out_back_io_enq_bits_extra_tlrr_extra_source),
    .io_enq_bits_extra_tlrr_extra_size(out_back_io_enq_bits_extra_tlrr_extra_size),
    .io_deq_ready(out_back_io_deq_ready),
    .io_deq_valid(out_back_io_deq_valid),
    .io_deq_bits_read(out_back_io_deq_bits_read),
    .io_deq_bits_index(out_back_io_deq_bits_index),
    .io_deq_bits_data(out_back_io_deq_bits_data),
    .io_deq_bits_mask(out_back_io_deq_bits_mask),
    .io_deq_bits_extra_tlrr_extra_source(out_back_io_deq_bits_extra_tlrr_extra_source),
    .io_deq_bits_extra_tlrr_extra_size(out_back_io_deq_bits_extra_tlrr_extra_size)
  );
  RHEA__Scheduler mods_0 ( // @[ComposableCache.scala 408:29]
    .rf_reset(mods_0_rf_reset),
    .clock(mods_0_clock),
    .reset(mods_0_reset),
    .io_in_a_ready(mods_0_io_in_a_ready),
    .io_in_a_valid(mods_0_io_in_a_valid),
    .io_in_a_bits_opcode(mods_0_io_in_a_bits_opcode),
    .io_in_a_bits_param(mods_0_io_in_a_bits_param),
    .io_in_a_bits_size(mods_0_io_in_a_bits_size),
    .io_in_a_bits_source(mods_0_io_in_a_bits_source),
    .io_in_a_bits_address(mods_0_io_in_a_bits_address),
    .io_in_a_bits_mask(mods_0_io_in_a_bits_mask),
    .io_in_a_bits_data(mods_0_io_in_a_bits_data),
    .io_in_a_bits_corrupt(mods_0_io_in_a_bits_corrupt),
    .io_in_b_ready(mods_0_io_in_b_ready),
    .io_in_b_valid(mods_0_io_in_b_valid),
    .io_in_b_bits_param(mods_0_io_in_b_bits_param),
    .io_in_b_bits_source(mods_0_io_in_b_bits_source),
    .io_in_b_bits_address(mods_0_io_in_b_bits_address),
    .io_in_c_ready(mods_0_io_in_c_ready),
    .io_in_c_valid(mods_0_io_in_c_valid),
    .io_in_c_bits_opcode(mods_0_io_in_c_bits_opcode),
    .io_in_c_bits_param(mods_0_io_in_c_bits_param),
    .io_in_c_bits_size(mods_0_io_in_c_bits_size),
    .io_in_c_bits_source(mods_0_io_in_c_bits_source),
    .io_in_c_bits_address(mods_0_io_in_c_bits_address),
    .io_in_c_bits_data(mods_0_io_in_c_bits_data),
    .io_in_c_bits_corrupt(mods_0_io_in_c_bits_corrupt),
    .io_in_d_ready(mods_0_io_in_d_ready),
    .io_in_d_valid(mods_0_io_in_d_valid),
    .io_in_d_bits_opcode(mods_0_io_in_d_bits_opcode),
    .io_in_d_bits_param(mods_0_io_in_d_bits_param),
    .io_in_d_bits_size(mods_0_io_in_d_bits_size),
    .io_in_d_bits_source(mods_0_io_in_d_bits_source),
    .io_in_d_bits_sink(mods_0_io_in_d_bits_sink),
    .io_in_d_bits_denied(mods_0_io_in_d_bits_denied),
    .io_in_d_bits_data(mods_0_io_in_d_bits_data),
    .io_in_d_bits_corrupt(mods_0_io_in_d_bits_corrupt),
    .io_in_e_valid(mods_0_io_in_e_valid),
    .io_in_e_bits_sink(mods_0_io_in_e_bits_sink),
    .io_out_a_ready(mods_0_io_out_a_ready),
    .io_out_a_valid(mods_0_io_out_a_valid),
    .io_out_a_bits_opcode(mods_0_io_out_a_bits_opcode),
    .io_out_a_bits_param(mods_0_io_out_a_bits_param),
    .io_out_a_bits_source(mods_0_io_out_a_bits_source),
    .io_out_a_bits_address(mods_0_io_out_a_bits_address),
    .io_out_c_ready(mods_0_io_out_c_ready),
    .io_out_c_valid(mods_0_io_out_c_valid),
    .io_out_c_bits_opcode(mods_0_io_out_c_bits_opcode),
    .io_out_c_bits_param(mods_0_io_out_c_bits_param),
    .io_out_c_bits_source(mods_0_io_out_c_bits_source),
    .io_out_c_bits_address(mods_0_io_out_c_bits_address),
    .io_out_c_bits_data(mods_0_io_out_c_bits_data),
    .io_out_d_ready(mods_0_io_out_d_ready),
    .io_out_d_valid(mods_0_io_out_d_valid),
    .io_out_d_bits_opcode(mods_0_io_out_d_bits_opcode),
    .io_out_d_bits_param(mods_0_io_out_d_bits_param),
    .io_out_d_bits_size(mods_0_io_out_d_bits_size),
    .io_out_d_bits_source(mods_0_io_out_d_bits_source),
    .io_out_d_bits_sink(mods_0_io_out_d_bits_sink),
    .io_out_d_bits_denied(mods_0_io_out_d_bits_denied),
    .io_out_d_bits_data(mods_0_io_out_d_bits_data),
    .io_out_d_bits_corrupt(mods_0_io_out_d_bits_corrupt),
    .io_out_e_valid(mods_0_io_out_e_valid),
    .io_out_e_bits_sink(mods_0_io_out_e_bits_sink),
    .io_ways_0(mods_0_io_ways_0),
    .io_ways_1(mods_0_io_ways_1),
    .io_ways_2(mods_0_io_ways_2),
    .io_ways_3(mods_0_io_ways_3),
    .io_ways_4(mods_0_io_ways_4),
    .io_ways_5(mods_0_io_ways_5),
    .io_ways_6(mods_0_io_ways_6),
    .io_ways_7(mods_0_io_ways_7),
    .io_ways_8(mods_0_io_ways_8),
    .io_ways_9(mods_0_io_ways_9),
    .io_ways_10(mods_0_io_ways_10),
    .io_ways_11(mods_0_io_ways_11),
    .io_ways_12(mods_0_io_ways_12),
    .io_ways_13(mods_0_io_ways_13),
    .io_divs_0(mods_0_io_divs_0),
    .io_divs_1(mods_0_io_divs_1),
    .io_divs_2(mods_0_io_divs_2),
    .io_divs_3(mods_0_io_divs_3),
    .io_divs_4(mods_0_io_divs_4),
    .io_divs_5(mods_0_io_divs_5),
    .io_divs_6(mods_0_io_divs_6),
    .io_divs_7(mods_0_io_divs_7),
    .io_divs_8(mods_0_io_divs_8),
    .io_divs_9(mods_0_io_divs_9),
    .io_divs_10(mods_0_io_divs_10),
    .io_divs_11(mods_0_io_divs_11),
    .io_divs_12(mods_0_io_divs_12),
    .io_divs_13(mods_0_io_divs_13),
    .io_req_ready(mods_0_io_req_ready),
    .io_req_valid(mods_0_io_req_valid),
    .io_req_bits_address(mods_0_io_req_bits_address),
    .io_resp_ready(mods_0_io_resp_ready),
    .io_resp_valid(mods_0_io_resp_valid),
    .io_side_adr_valid(mods_0_io_side_adr_valid),
    .io_side_adr_bits_way(mods_0_io_side_adr_bits_way),
    .io_side_adr_bits_set(mods_0_io_side_adr_bits_set),
    .io_side_adr_bits_beat(mods_0_io_side_adr_bits_beat),
    .io_side_adr_bits_mask(mods_0_io_side_adr_bits_mask),
    .io_side_adr_bits_write(mods_0_io_side_adr_bits_write),
    .io_side_wdat_data(mods_0_io_side_wdat_data),
    .io_side_wdat_poison(mods_0_io_side_wdat_poison),
    .io_side_rdat_data(mods_0_io_side_rdat_data),
    .io_error_ready(mods_0_io_error_ready),
    .io_error_valid(mods_0_io_error_valid),
    .io_error_bits_dir(mods_0_io_error_bits_dir),
    .io_error_bits_bit(mods_0_io_error_bits_bit)
  );
  RHEA__Scheduler mods_1 ( // @[ComposableCache.scala 408:29]
    .rf_reset(mods_1_rf_reset),
    .clock(mods_1_clock),
    .reset(mods_1_reset),
    .io_in_a_ready(mods_1_io_in_a_ready),
    .io_in_a_valid(mods_1_io_in_a_valid),
    .io_in_a_bits_opcode(mods_1_io_in_a_bits_opcode),
    .io_in_a_bits_param(mods_1_io_in_a_bits_param),
    .io_in_a_bits_size(mods_1_io_in_a_bits_size),
    .io_in_a_bits_source(mods_1_io_in_a_bits_source),
    .io_in_a_bits_address(mods_1_io_in_a_bits_address),
    .io_in_a_bits_mask(mods_1_io_in_a_bits_mask),
    .io_in_a_bits_data(mods_1_io_in_a_bits_data),
    .io_in_a_bits_corrupt(mods_1_io_in_a_bits_corrupt),
    .io_in_b_ready(mods_1_io_in_b_ready),
    .io_in_b_valid(mods_1_io_in_b_valid),
    .io_in_b_bits_param(mods_1_io_in_b_bits_param),
    .io_in_b_bits_source(mods_1_io_in_b_bits_source),
    .io_in_b_bits_address(mods_1_io_in_b_bits_address),
    .io_in_c_ready(mods_1_io_in_c_ready),
    .io_in_c_valid(mods_1_io_in_c_valid),
    .io_in_c_bits_opcode(mods_1_io_in_c_bits_opcode),
    .io_in_c_bits_param(mods_1_io_in_c_bits_param),
    .io_in_c_bits_size(mods_1_io_in_c_bits_size),
    .io_in_c_bits_source(mods_1_io_in_c_bits_source),
    .io_in_c_bits_address(mods_1_io_in_c_bits_address),
    .io_in_c_bits_data(mods_1_io_in_c_bits_data),
    .io_in_c_bits_corrupt(mods_1_io_in_c_bits_corrupt),
    .io_in_d_ready(mods_1_io_in_d_ready),
    .io_in_d_valid(mods_1_io_in_d_valid),
    .io_in_d_bits_opcode(mods_1_io_in_d_bits_opcode),
    .io_in_d_bits_param(mods_1_io_in_d_bits_param),
    .io_in_d_bits_size(mods_1_io_in_d_bits_size),
    .io_in_d_bits_source(mods_1_io_in_d_bits_source),
    .io_in_d_bits_sink(mods_1_io_in_d_bits_sink),
    .io_in_d_bits_denied(mods_1_io_in_d_bits_denied),
    .io_in_d_bits_data(mods_1_io_in_d_bits_data),
    .io_in_d_bits_corrupt(mods_1_io_in_d_bits_corrupt),
    .io_in_e_valid(mods_1_io_in_e_valid),
    .io_in_e_bits_sink(mods_1_io_in_e_bits_sink),
    .io_out_a_ready(mods_1_io_out_a_ready),
    .io_out_a_valid(mods_1_io_out_a_valid),
    .io_out_a_bits_opcode(mods_1_io_out_a_bits_opcode),
    .io_out_a_bits_param(mods_1_io_out_a_bits_param),
    .io_out_a_bits_source(mods_1_io_out_a_bits_source),
    .io_out_a_bits_address(mods_1_io_out_a_bits_address),
    .io_out_c_ready(mods_1_io_out_c_ready),
    .io_out_c_valid(mods_1_io_out_c_valid),
    .io_out_c_bits_opcode(mods_1_io_out_c_bits_opcode),
    .io_out_c_bits_param(mods_1_io_out_c_bits_param),
    .io_out_c_bits_source(mods_1_io_out_c_bits_source),
    .io_out_c_bits_address(mods_1_io_out_c_bits_address),
    .io_out_c_bits_data(mods_1_io_out_c_bits_data),
    .io_out_d_ready(mods_1_io_out_d_ready),
    .io_out_d_valid(mods_1_io_out_d_valid),
    .io_out_d_bits_opcode(mods_1_io_out_d_bits_opcode),
    .io_out_d_bits_param(mods_1_io_out_d_bits_param),
    .io_out_d_bits_size(mods_1_io_out_d_bits_size),
    .io_out_d_bits_source(mods_1_io_out_d_bits_source),
    .io_out_d_bits_sink(mods_1_io_out_d_bits_sink),
    .io_out_d_bits_denied(mods_1_io_out_d_bits_denied),
    .io_out_d_bits_data(mods_1_io_out_d_bits_data),
    .io_out_d_bits_corrupt(mods_1_io_out_d_bits_corrupt),
    .io_out_e_valid(mods_1_io_out_e_valid),
    .io_out_e_bits_sink(mods_1_io_out_e_bits_sink),
    .io_ways_0(mods_1_io_ways_0),
    .io_ways_1(mods_1_io_ways_1),
    .io_ways_2(mods_1_io_ways_2),
    .io_ways_3(mods_1_io_ways_3),
    .io_ways_4(mods_1_io_ways_4),
    .io_ways_5(mods_1_io_ways_5),
    .io_ways_6(mods_1_io_ways_6),
    .io_ways_7(mods_1_io_ways_7),
    .io_ways_8(mods_1_io_ways_8),
    .io_ways_9(mods_1_io_ways_9),
    .io_ways_10(mods_1_io_ways_10),
    .io_ways_11(mods_1_io_ways_11),
    .io_ways_12(mods_1_io_ways_12),
    .io_ways_13(mods_1_io_ways_13),
    .io_divs_0(mods_1_io_divs_0),
    .io_divs_1(mods_1_io_divs_1),
    .io_divs_2(mods_1_io_divs_2),
    .io_divs_3(mods_1_io_divs_3),
    .io_divs_4(mods_1_io_divs_4),
    .io_divs_5(mods_1_io_divs_5),
    .io_divs_6(mods_1_io_divs_6),
    .io_divs_7(mods_1_io_divs_7),
    .io_divs_8(mods_1_io_divs_8),
    .io_divs_9(mods_1_io_divs_9),
    .io_divs_10(mods_1_io_divs_10),
    .io_divs_11(mods_1_io_divs_11),
    .io_divs_12(mods_1_io_divs_12),
    .io_divs_13(mods_1_io_divs_13),
    .io_req_ready(mods_1_io_req_ready),
    .io_req_valid(mods_1_io_req_valid),
    .io_req_bits_address(mods_1_io_req_bits_address),
    .io_resp_ready(mods_1_io_resp_ready),
    .io_resp_valid(mods_1_io_resp_valid),
    .io_side_adr_valid(mods_1_io_side_adr_valid),
    .io_side_adr_bits_way(mods_1_io_side_adr_bits_way),
    .io_side_adr_bits_set(mods_1_io_side_adr_bits_set),
    .io_side_adr_bits_beat(mods_1_io_side_adr_bits_beat),
    .io_side_adr_bits_mask(mods_1_io_side_adr_bits_mask),
    .io_side_adr_bits_write(mods_1_io_side_adr_bits_write),
    .io_side_wdat_data(mods_1_io_side_wdat_data),
    .io_side_wdat_poison(mods_1_io_side_wdat_poison),
    .io_side_rdat_data(mods_1_io_side_rdat_data),
    .io_error_ready(mods_1_io_error_ready),
    .io_error_valid(mods_1_io_error_valid),
    .io_error_bits_dir(mods_1_io_error_bits_dir),
    .io_error_bits_bit(mods_1_io_error_bits_bit)
  );
  RHEA__Scheduler mods_2 ( // @[ComposableCache.scala 408:29]
    .rf_reset(mods_2_rf_reset),
    .clock(mods_2_clock),
    .reset(mods_2_reset),
    .io_in_a_ready(mods_2_io_in_a_ready),
    .io_in_a_valid(mods_2_io_in_a_valid),
    .io_in_a_bits_opcode(mods_2_io_in_a_bits_opcode),
    .io_in_a_bits_param(mods_2_io_in_a_bits_param),
    .io_in_a_bits_size(mods_2_io_in_a_bits_size),
    .io_in_a_bits_source(mods_2_io_in_a_bits_source),
    .io_in_a_bits_address(mods_2_io_in_a_bits_address),
    .io_in_a_bits_mask(mods_2_io_in_a_bits_mask),
    .io_in_a_bits_data(mods_2_io_in_a_bits_data),
    .io_in_a_bits_corrupt(mods_2_io_in_a_bits_corrupt),
    .io_in_b_ready(mods_2_io_in_b_ready),
    .io_in_b_valid(mods_2_io_in_b_valid),
    .io_in_b_bits_param(mods_2_io_in_b_bits_param),
    .io_in_b_bits_source(mods_2_io_in_b_bits_source),
    .io_in_b_bits_address(mods_2_io_in_b_bits_address),
    .io_in_c_ready(mods_2_io_in_c_ready),
    .io_in_c_valid(mods_2_io_in_c_valid),
    .io_in_c_bits_opcode(mods_2_io_in_c_bits_opcode),
    .io_in_c_bits_param(mods_2_io_in_c_bits_param),
    .io_in_c_bits_size(mods_2_io_in_c_bits_size),
    .io_in_c_bits_source(mods_2_io_in_c_bits_source),
    .io_in_c_bits_address(mods_2_io_in_c_bits_address),
    .io_in_c_bits_data(mods_2_io_in_c_bits_data),
    .io_in_c_bits_corrupt(mods_2_io_in_c_bits_corrupt),
    .io_in_d_ready(mods_2_io_in_d_ready),
    .io_in_d_valid(mods_2_io_in_d_valid),
    .io_in_d_bits_opcode(mods_2_io_in_d_bits_opcode),
    .io_in_d_bits_param(mods_2_io_in_d_bits_param),
    .io_in_d_bits_size(mods_2_io_in_d_bits_size),
    .io_in_d_bits_source(mods_2_io_in_d_bits_source),
    .io_in_d_bits_sink(mods_2_io_in_d_bits_sink),
    .io_in_d_bits_denied(mods_2_io_in_d_bits_denied),
    .io_in_d_bits_data(mods_2_io_in_d_bits_data),
    .io_in_d_bits_corrupt(mods_2_io_in_d_bits_corrupt),
    .io_in_e_valid(mods_2_io_in_e_valid),
    .io_in_e_bits_sink(mods_2_io_in_e_bits_sink),
    .io_out_a_ready(mods_2_io_out_a_ready),
    .io_out_a_valid(mods_2_io_out_a_valid),
    .io_out_a_bits_opcode(mods_2_io_out_a_bits_opcode),
    .io_out_a_bits_param(mods_2_io_out_a_bits_param),
    .io_out_a_bits_source(mods_2_io_out_a_bits_source),
    .io_out_a_bits_address(mods_2_io_out_a_bits_address),
    .io_out_c_ready(mods_2_io_out_c_ready),
    .io_out_c_valid(mods_2_io_out_c_valid),
    .io_out_c_bits_opcode(mods_2_io_out_c_bits_opcode),
    .io_out_c_bits_param(mods_2_io_out_c_bits_param),
    .io_out_c_bits_source(mods_2_io_out_c_bits_source),
    .io_out_c_bits_address(mods_2_io_out_c_bits_address),
    .io_out_c_bits_data(mods_2_io_out_c_bits_data),
    .io_out_d_ready(mods_2_io_out_d_ready),
    .io_out_d_valid(mods_2_io_out_d_valid),
    .io_out_d_bits_opcode(mods_2_io_out_d_bits_opcode),
    .io_out_d_bits_param(mods_2_io_out_d_bits_param),
    .io_out_d_bits_size(mods_2_io_out_d_bits_size),
    .io_out_d_bits_source(mods_2_io_out_d_bits_source),
    .io_out_d_bits_sink(mods_2_io_out_d_bits_sink),
    .io_out_d_bits_denied(mods_2_io_out_d_bits_denied),
    .io_out_d_bits_data(mods_2_io_out_d_bits_data),
    .io_out_d_bits_corrupt(mods_2_io_out_d_bits_corrupt),
    .io_out_e_valid(mods_2_io_out_e_valid),
    .io_out_e_bits_sink(mods_2_io_out_e_bits_sink),
    .io_ways_0(mods_2_io_ways_0),
    .io_ways_1(mods_2_io_ways_1),
    .io_ways_2(mods_2_io_ways_2),
    .io_ways_3(mods_2_io_ways_3),
    .io_ways_4(mods_2_io_ways_4),
    .io_ways_5(mods_2_io_ways_5),
    .io_ways_6(mods_2_io_ways_6),
    .io_ways_7(mods_2_io_ways_7),
    .io_ways_8(mods_2_io_ways_8),
    .io_ways_9(mods_2_io_ways_9),
    .io_ways_10(mods_2_io_ways_10),
    .io_ways_11(mods_2_io_ways_11),
    .io_ways_12(mods_2_io_ways_12),
    .io_ways_13(mods_2_io_ways_13),
    .io_divs_0(mods_2_io_divs_0),
    .io_divs_1(mods_2_io_divs_1),
    .io_divs_2(mods_2_io_divs_2),
    .io_divs_3(mods_2_io_divs_3),
    .io_divs_4(mods_2_io_divs_4),
    .io_divs_5(mods_2_io_divs_5),
    .io_divs_6(mods_2_io_divs_6),
    .io_divs_7(mods_2_io_divs_7),
    .io_divs_8(mods_2_io_divs_8),
    .io_divs_9(mods_2_io_divs_9),
    .io_divs_10(mods_2_io_divs_10),
    .io_divs_11(mods_2_io_divs_11),
    .io_divs_12(mods_2_io_divs_12),
    .io_divs_13(mods_2_io_divs_13),
    .io_req_ready(mods_2_io_req_ready),
    .io_req_valid(mods_2_io_req_valid),
    .io_req_bits_address(mods_2_io_req_bits_address),
    .io_resp_ready(mods_2_io_resp_ready),
    .io_resp_valid(mods_2_io_resp_valid),
    .io_side_adr_valid(mods_2_io_side_adr_valid),
    .io_side_adr_bits_way(mods_2_io_side_adr_bits_way),
    .io_side_adr_bits_set(mods_2_io_side_adr_bits_set),
    .io_side_adr_bits_beat(mods_2_io_side_adr_bits_beat),
    .io_side_adr_bits_mask(mods_2_io_side_adr_bits_mask),
    .io_side_adr_bits_write(mods_2_io_side_adr_bits_write),
    .io_side_wdat_data(mods_2_io_side_wdat_data),
    .io_side_wdat_poison(mods_2_io_side_wdat_poison),
    .io_side_rdat_data(mods_2_io_side_rdat_data),
    .io_error_ready(mods_2_io_error_ready),
    .io_error_valid(mods_2_io_error_valid),
    .io_error_bits_dir(mods_2_io_error_bits_dir),
    .io_error_bits_bit(mods_2_io_error_bits_bit)
  );
  RHEA__Scheduler mods_3 ( // @[ComposableCache.scala 408:29]
    .rf_reset(mods_3_rf_reset),
    .clock(mods_3_clock),
    .reset(mods_3_reset),
    .io_in_a_ready(mods_3_io_in_a_ready),
    .io_in_a_valid(mods_3_io_in_a_valid),
    .io_in_a_bits_opcode(mods_3_io_in_a_bits_opcode),
    .io_in_a_bits_param(mods_3_io_in_a_bits_param),
    .io_in_a_bits_size(mods_3_io_in_a_bits_size),
    .io_in_a_bits_source(mods_3_io_in_a_bits_source),
    .io_in_a_bits_address(mods_3_io_in_a_bits_address),
    .io_in_a_bits_mask(mods_3_io_in_a_bits_mask),
    .io_in_a_bits_data(mods_3_io_in_a_bits_data),
    .io_in_a_bits_corrupt(mods_3_io_in_a_bits_corrupt),
    .io_in_b_ready(mods_3_io_in_b_ready),
    .io_in_b_valid(mods_3_io_in_b_valid),
    .io_in_b_bits_param(mods_3_io_in_b_bits_param),
    .io_in_b_bits_source(mods_3_io_in_b_bits_source),
    .io_in_b_bits_address(mods_3_io_in_b_bits_address),
    .io_in_c_ready(mods_3_io_in_c_ready),
    .io_in_c_valid(mods_3_io_in_c_valid),
    .io_in_c_bits_opcode(mods_3_io_in_c_bits_opcode),
    .io_in_c_bits_param(mods_3_io_in_c_bits_param),
    .io_in_c_bits_size(mods_3_io_in_c_bits_size),
    .io_in_c_bits_source(mods_3_io_in_c_bits_source),
    .io_in_c_bits_address(mods_3_io_in_c_bits_address),
    .io_in_c_bits_data(mods_3_io_in_c_bits_data),
    .io_in_c_bits_corrupt(mods_3_io_in_c_bits_corrupt),
    .io_in_d_ready(mods_3_io_in_d_ready),
    .io_in_d_valid(mods_3_io_in_d_valid),
    .io_in_d_bits_opcode(mods_3_io_in_d_bits_opcode),
    .io_in_d_bits_param(mods_3_io_in_d_bits_param),
    .io_in_d_bits_size(mods_3_io_in_d_bits_size),
    .io_in_d_bits_source(mods_3_io_in_d_bits_source),
    .io_in_d_bits_sink(mods_3_io_in_d_bits_sink),
    .io_in_d_bits_denied(mods_3_io_in_d_bits_denied),
    .io_in_d_bits_data(mods_3_io_in_d_bits_data),
    .io_in_d_bits_corrupt(mods_3_io_in_d_bits_corrupt),
    .io_in_e_valid(mods_3_io_in_e_valid),
    .io_in_e_bits_sink(mods_3_io_in_e_bits_sink),
    .io_out_a_ready(mods_3_io_out_a_ready),
    .io_out_a_valid(mods_3_io_out_a_valid),
    .io_out_a_bits_opcode(mods_3_io_out_a_bits_opcode),
    .io_out_a_bits_param(mods_3_io_out_a_bits_param),
    .io_out_a_bits_source(mods_3_io_out_a_bits_source),
    .io_out_a_bits_address(mods_3_io_out_a_bits_address),
    .io_out_c_ready(mods_3_io_out_c_ready),
    .io_out_c_valid(mods_3_io_out_c_valid),
    .io_out_c_bits_opcode(mods_3_io_out_c_bits_opcode),
    .io_out_c_bits_param(mods_3_io_out_c_bits_param),
    .io_out_c_bits_source(mods_3_io_out_c_bits_source),
    .io_out_c_bits_address(mods_3_io_out_c_bits_address),
    .io_out_c_bits_data(mods_3_io_out_c_bits_data),
    .io_out_d_ready(mods_3_io_out_d_ready),
    .io_out_d_valid(mods_3_io_out_d_valid),
    .io_out_d_bits_opcode(mods_3_io_out_d_bits_opcode),
    .io_out_d_bits_param(mods_3_io_out_d_bits_param),
    .io_out_d_bits_size(mods_3_io_out_d_bits_size),
    .io_out_d_bits_source(mods_3_io_out_d_bits_source),
    .io_out_d_bits_sink(mods_3_io_out_d_bits_sink),
    .io_out_d_bits_denied(mods_3_io_out_d_bits_denied),
    .io_out_d_bits_data(mods_3_io_out_d_bits_data),
    .io_out_d_bits_corrupt(mods_3_io_out_d_bits_corrupt),
    .io_out_e_valid(mods_3_io_out_e_valid),
    .io_out_e_bits_sink(mods_3_io_out_e_bits_sink),
    .io_ways_0(mods_3_io_ways_0),
    .io_ways_1(mods_3_io_ways_1),
    .io_ways_2(mods_3_io_ways_2),
    .io_ways_3(mods_3_io_ways_3),
    .io_ways_4(mods_3_io_ways_4),
    .io_ways_5(mods_3_io_ways_5),
    .io_ways_6(mods_3_io_ways_6),
    .io_ways_7(mods_3_io_ways_7),
    .io_ways_8(mods_3_io_ways_8),
    .io_ways_9(mods_3_io_ways_9),
    .io_ways_10(mods_3_io_ways_10),
    .io_ways_11(mods_3_io_ways_11),
    .io_ways_12(mods_3_io_ways_12),
    .io_ways_13(mods_3_io_ways_13),
    .io_divs_0(mods_3_io_divs_0),
    .io_divs_1(mods_3_io_divs_1),
    .io_divs_2(mods_3_io_divs_2),
    .io_divs_3(mods_3_io_divs_3),
    .io_divs_4(mods_3_io_divs_4),
    .io_divs_5(mods_3_io_divs_5),
    .io_divs_6(mods_3_io_divs_6),
    .io_divs_7(mods_3_io_divs_7),
    .io_divs_8(mods_3_io_divs_8),
    .io_divs_9(mods_3_io_divs_9),
    .io_divs_10(mods_3_io_divs_10),
    .io_divs_11(mods_3_io_divs_11),
    .io_divs_12(mods_3_io_divs_12),
    .io_divs_13(mods_3_io_divs_13),
    .io_req_ready(mods_3_io_req_ready),
    .io_req_valid(mods_3_io_req_valid),
    .io_req_bits_address(mods_3_io_req_bits_address),
    .io_resp_ready(mods_3_io_resp_ready),
    .io_resp_valid(mods_3_io_resp_valid),
    .io_side_adr_valid(mods_3_io_side_adr_valid),
    .io_side_adr_bits_way(mods_3_io_side_adr_bits_way),
    .io_side_adr_bits_set(mods_3_io_side_adr_bits_set),
    .io_side_adr_bits_beat(mods_3_io_side_adr_bits_beat),
    .io_side_adr_bits_mask(mods_3_io_side_adr_bits_mask),
    .io_side_adr_bits_write(mods_3_io_side_adr_bits_write),
    .io_side_wdat_data(mods_3_io_side_wdat_data),
    .io_side_wdat_poison(mods_3_io_side_wdat_poison),
    .io_side_rdat_data(mods_3_io_side_rdat_data),
    .io_error_ready(mods_3_io_error_ready),
    .io_error_valid(mods_3_io_error_valid),
    .io_error_bits_dir(mods_3_io_error_bits_dir),
    .io_error_bits_bit(mods_3_io_error_bits_bit)
  );
  RHEA__Sideband sideband ( // @[ComposableCache.scala 473:28]
    .rf_reset(sideband_rf_reset),
    .clock(sideband_clock),
    .reset(sideband_reset),
    .io_a_ready(sideband_io_a_ready),
    .io_a_valid(sideband_io_a_valid),
    .io_a_bits_opcode(sideband_io_a_bits_opcode),
    .io_a_bits_param(sideband_io_a_bits_param),
    .io_a_bits_size(sideband_io_a_bits_size),
    .io_a_bits_source(sideband_io_a_bits_source),
    .io_a_bits_address(sideband_io_a_bits_address),
    .io_a_bits_mask(sideband_io_a_bits_mask),
    .io_a_bits_data(sideband_io_a_bits_data),
    .io_a_bits_corrupt(sideband_io_a_bits_corrupt),
    .io_d_ready(sideband_io_d_ready),
    .io_d_valid(sideband_io_d_valid),
    .io_d_bits_opcode(sideband_io_d_bits_opcode),
    .io_d_bits_size(sideband_io_d_bits_size),
    .io_d_bits_source(sideband_io_d_bits_source),
    .io_d_bits_data(sideband_io_d_bits_data),
    .io_d_bits_corrupt(sideband_io_d_bits_corrupt),
    .io_threshold(sideband_io_threshold),
    .io_bs_adr_0_valid(sideband_io_bs_adr_0_valid),
    .io_bs_adr_0_bits_way(sideband_io_bs_adr_0_bits_way),
    .io_bs_adr_0_bits_set(sideband_io_bs_adr_0_bits_set),
    .io_bs_adr_0_bits_beat(sideband_io_bs_adr_0_bits_beat),
    .io_bs_adr_0_bits_mask(sideband_io_bs_adr_0_bits_mask),
    .io_bs_adr_0_bits_write(sideband_io_bs_adr_0_bits_write),
    .io_bs_adr_1_valid(sideband_io_bs_adr_1_valid),
    .io_bs_adr_1_bits_way(sideband_io_bs_adr_1_bits_way),
    .io_bs_adr_1_bits_set(sideband_io_bs_adr_1_bits_set),
    .io_bs_adr_1_bits_beat(sideband_io_bs_adr_1_bits_beat),
    .io_bs_adr_1_bits_mask(sideband_io_bs_adr_1_bits_mask),
    .io_bs_adr_1_bits_write(sideband_io_bs_adr_1_bits_write),
    .io_bs_adr_2_valid(sideband_io_bs_adr_2_valid),
    .io_bs_adr_2_bits_way(sideband_io_bs_adr_2_bits_way),
    .io_bs_adr_2_bits_set(sideband_io_bs_adr_2_bits_set),
    .io_bs_adr_2_bits_beat(sideband_io_bs_adr_2_bits_beat),
    .io_bs_adr_2_bits_mask(sideband_io_bs_adr_2_bits_mask),
    .io_bs_adr_2_bits_write(sideband_io_bs_adr_2_bits_write),
    .io_bs_adr_3_valid(sideband_io_bs_adr_3_valid),
    .io_bs_adr_3_bits_way(sideband_io_bs_adr_3_bits_way),
    .io_bs_adr_3_bits_set(sideband_io_bs_adr_3_bits_set),
    .io_bs_adr_3_bits_beat(sideband_io_bs_adr_3_bits_beat),
    .io_bs_adr_3_bits_mask(sideband_io_bs_adr_3_bits_mask),
    .io_bs_adr_3_bits_write(sideband_io_bs_adr_3_bits_write),
    .io_bs_wdat_0_data(sideband_io_bs_wdat_0_data),
    .io_bs_wdat_0_poison(sideband_io_bs_wdat_0_poison),
    .io_bs_wdat_1_data(sideband_io_bs_wdat_1_data),
    .io_bs_wdat_1_poison(sideband_io_bs_wdat_1_poison),
    .io_bs_wdat_2_data(sideband_io_bs_wdat_2_data),
    .io_bs_wdat_2_poison(sideband_io_bs_wdat_2_poison),
    .io_bs_wdat_3_data(sideband_io_bs_wdat_3_data),
    .io_bs_wdat_3_poison(sideband_io_bs_wdat_3_poison),
    .io_bs_rdat_0_data(sideband_io_bs_rdat_0_data),
    .io_bs_rdat_1_data(sideband_io_bs_rdat_1_data),
    .io_bs_rdat_2_data(sideband_io_bs_rdat_2_data),
    .io_bs_rdat_3_data(sideband_io_bs_rdat_3_data),
    .io_uncorrected_valid(sideband_io_uncorrected_valid),
    .io_uncorrected_bits(sideband_io_uncorrected_bits)
  );
  assign out_back_rf_reset = rf_reset;
  assign mods_0_rf_reset = rf_reset;
  assign mods_1_rf_reset = rf_reset;
  assign mods_2_rf_reset = rf_reset;
  assign mods_3_rf_reset = rf_reset;
  assign sideband_rf_reset = rf_reset;
  assign auto_in_4_a_ready = out_front_ready & out_iready; // @[ComposableCache.scala 141:30]
  assign auto_in_4_d_valid = out_back_io_deq_valid & out_oready; // @[ComposableCache.scala 141:30]
  assign auto_in_4_d_bits_opcode = {{2'd0}, out_bits_read}; // @[ComposableCache.scala 141:30 ComposableCache.scala 141:30]
  assign auto_in_4_d_bits_size = out_back_io_deq_bits_extra_tlrr_extra_size; // @[ComposableCache.scala 141:30 ComposableCache.scala 141:30]
  assign auto_in_4_d_bits_source = out_back_io_deq_bits_extra_tlrr_extra_source; // @[ComposableCache.scala 141:30 ComposableCache.scala 141:30]
  assign auto_in_4_d_bits_data = out_out_bits_data_out ? out_out_bits_data_out_1 : 64'h0; // @[ComposableCache.scala 141:30]
  assign auto_side_in_a_ready = sideband_io_a_ready; // @[ComposableCache.scala 125:51 ComposableCache.scala 474:21]
  assign auto_side_in_d_valid = sideband_io_d_valid; // @[ComposableCache.scala 125:51 ComposableCache.scala 475:12]
  assign auto_side_in_d_bits_opcode = sideband_io_d_bits_opcode; // @[ComposableCache.scala 125:51 ComposableCache.scala 475:12]
  assign auto_side_in_d_bits_size = sideband_io_d_bits_size; // @[ComposableCache.scala 125:51 ComposableCache.scala 475:12]
  assign auto_side_in_d_bits_source = sideband_io_d_bits_source; // @[ComposableCache.scala 125:51 ComposableCache.scala 475:12]
  assign auto_side_in_d_bits_data = sideband_io_d_bits_data; // @[ComposableCache.scala 125:51 ComposableCache.scala 475:12]
  assign auto_side_in_d_bits_corrupt = sideband_io_d_bits_corrupt; // @[ComposableCache.scala 125:51 ComposableCache.scala 475:12]
  assign auto_in_3_a_ready = bundleIn_3_a_ready; // @[LazyModule.scala 388:16]
  assign auto_in_3_b_valid = bundleIn_3_b_valid; // @[LazyModule.scala 388:16]
  assign auto_in_3_b_bits_opcode = bundleIn_0_b_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_in_3_b_bits_param = bundleIn_3_b_bits_param; // @[LazyModule.scala 388:16]
  assign auto_in_3_b_bits_size = bundleIn_0_b_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_in_3_b_bits_source = bundleIn_3_b_bits_source; // @[LazyModule.scala 388:16]
  assign auto_in_3_b_bits_address = bundleIn_3_b_bits_address; // @[LazyModule.scala 388:16]
  assign auto_in_3_b_bits_mask = bundleIn_0_b_bits_mask; // @[LazyModule.scala 388:16]
  assign auto_in_3_b_bits_data = bundleIn_0_b_bits_data; // @[LazyModule.scala 388:16]
  assign auto_in_3_b_bits_corrupt = _T_3; // @[LazyModule.scala 388:16]
  assign auto_in_3_c_ready = bundleIn_3_c_ready; // @[LazyModule.scala 388:16]
  assign auto_in_3_d_valid = bundleIn_3_d_valid; // @[LazyModule.scala 388:16]
  assign auto_in_3_d_bits_opcode = bundleIn_3_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_in_3_d_bits_param = bundleIn_3_d_bits_param; // @[LazyModule.scala 388:16]
  assign auto_in_3_d_bits_size = bundleIn_3_d_bits_size; // @[LazyModule.scala 388:16]
  assign auto_in_3_d_bits_source = bundleIn_3_d_bits_source; // @[LazyModule.scala 388:16]
  assign auto_in_3_d_bits_sink = bundleIn_3_d_bits_sink; // @[LazyModule.scala 388:16]
  assign auto_in_3_d_bits_denied = bundleIn_3_d_bits_denied; // @[LazyModule.scala 388:16]
  assign auto_in_3_d_bits_data = bundleIn_3_d_bits_data; // @[LazyModule.scala 388:16]
  assign auto_in_3_d_bits_corrupt = bundleIn_3_d_bits_corrupt; // @[LazyModule.scala 388:16]
  assign auto_in_3_e_ready = _T; // @[LazyModule.scala 388:16]
  assign auto_in_2_a_ready = bundleIn_2_a_ready; // @[LazyModule.scala 388:16]
  assign auto_in_2_b_valid = bundleIn_2_b_valid; // @[LazyModule.scala 388:16]
  assign auto_in_2_b_bits_opcode = bundleIn_0_b_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_in_2_b_bits_param = bundleIn_2_b_bits_param; // @[LazyModule.scala 388:16]
  assign auto_in_2_b_bits_size = bundleIn_0_b_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_in_2_b_bits_source = bundleIn_2_b_bits_source; // @[LazyModule.scala 388:16]
  assign auto_in_2_b_bits_address = bundleIn_2_b_bits_address; // @[LazyModule.scala 388:16]
  assign auto_in_2_b_bits_mask = bundleIn_0_b_bits_mask; // @[LazyModule.scala 388:16]
  assign auto_in_2_b_bits_data = bundleIn_0_b_bits_data; // @[LazyModule.scala 388:16]
  assign auto_in_2_b_bits_corrupt = _T_3; // @[LazyModule.scala 388:16]
  assign auto_in_2_c_ready = bundleIn_2_c_ready; // @[LazyModule.scala 388:16]
  assign auto_in_2_d_valid = bundleIn_2_d_valid; // @[LazyModule.scala 388:16]
  assign auto_in_2_d_bits_opcode = bundleIn_2_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_in_2_d_bits_param = bundleIn_2_d_bits_param; // @[LazyModule.scala 388:16]
  assign auto_in_2_d_bits_size = bundleIn_2_d_bits_size; // @[LazyModule.scala 388:16]
  assign auto_in_2_d_bits_source = bundleIn_2_d_bits_source; // @[LazyModule.scala 388:16]
  assign auto_in_2_d_bits_sink = bundleIn_2_d_bits_sink; // @[LazyModule.scala 388:16]
  assign auto_in_2_d_bits_denied = bundleIn_2_d_bits_denied; // @[LazyModule.scala 388:16]
  assign auto_in_2_d_bits_data = bundleIn_2_d_bits_data; // @[LazyModule.scala 388:16]
  assign auto_in_2_d_bits_corrupt = bundleIn_2_d_bits_corrupt; // @[LazyModule.scala 388:16]
  assign auto_in_2_e_ready = _T; // @[LazyModule.scala 388:16]
  assign auto_in_1_a_ready = bundleIn_1_a_ready; // @[LazyModule.scala 388:16]
  assign auto_in_1_b_valid = bundleIn_1_b_valid; // @[LazyModule.scala 388:16]
  assign auto_in_1_b_bits_opcode = bundleIn_0_b_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_in_1_b_bits_param = bundleIn_1_b_bits_param; // @[LazyModule.scala 388:16]
  assign auto_in_1_b_bits_size = bundleIn_0_b_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_in_1_b_bits_source = bundleIn_1_b_bits_source; // @[LazyModule.scala 388:16]
  assign auto_in_1_b_bits_address = bundleIn_1_b_bits_address; // @[LazyModule.scala 388:16]
  assign auto_in_1_b_bits_mask = bundleIn_0_b_bits_mask; // @[LazyModule.scala 388:16]
  assign auto_in_1_b_bits_data = bundleIn_0_b_bits_data; // @[LazyModule.scala 388:16]
  assign auto_in_1_b_bits_corrupt = _T_3; // @[LazyModule.scala 388:16]
  assign auto_in_1_c_ready = bundleIn_1_c_ready; // @[LazyModule.scala 388:16]
  assign auto_in_1_d_valid = bundleIn_1_d_valid; // @[LazyModule.scala 388:16]
  assign auto_in_1_d_bits_opcode = bundleIn_1_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_in_1_d_bits_param = bundleIn_1_d_bits_param; // @[LazyModule.scala 388:16]
  assign auto_in_1_d_bits_size = bundleIn_1_d_bits_size; // @[LazyModule.scala 388:16]
  assign auto_in_1_d_bits_source = bundleIn_1_d_bits_source; // @[LazyModule.scala 388:16]
  assign auto_in_1_d_bits_sink = bundleIn_1_d_bits_sink; // @[LazyModule.scala 388:16]
  assign auto_in_1_d_bits_denied = bundleIn_1_d_bits_denied; // @[LazyModule.scala 388:16]
  assign auto_in_1_d_bits_data = bundleIn_1_d_bits_data; // @[LazyModule.scala 388:16]
  assign auto_in_1_d_bits_corrupt = bundleIn_1_d_bits_corrupt; // @[LazyModule.scala 388:16]
  assign auto_in_1_e_ready = _T; // @[LazyModule.scala 388:16]
  assign auto_in_0_a_ready = bundleIn_0_a_ready; // @[LazyModule.scala 388:16]
  assign auto_in_0_b_valid = bundleIn_0_b_valid; // @[LazyModule.scala 388:16]
  assign auto_in_0_b_bits_opcode = bundleIn_0_b_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_in_0_b_bits_param = bundleIn_0_b_bits_param; // @[LazyModule.scala 388:16]
  assign auto_in_0_b_bits_size = bundleIn_0_b_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_in_0_b_bits_source = bundleIn_0_b_bits_source; // @[LazyModule.scala 388:16]
  assign auto_in_0_b_bits_address = bundleIn_0_b_bits_address; // @[LazyModule.scala 388:16]
  assign auto_in_0_b_bits_mask = bundleIn_0_b_bits_mask; // @[LazyModule.scala 388:16]
  assign auto_in_0_b_bits_data = bundleIn_0_b_bits_data; // @[LazyModule.scala 388:16]
  assign auto_in_0_b_bits_corrupt = _T_3; // @[LazyModule.scala 388:16]
  assign auto_in_0_c_ready = bundleIn_0_c_ready; // @[LazyModule.scala 388:16]
  assign auto_in_0_d_valid = bundleIn_0_d_valid; // @[LazyModule.scala 388:16]
  assign auto_in_0_d_bits_opcode = bundleIn_0_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_in_0_d_bits_param = bundleIn_0_d_bits_param; // @[LazyModule.scala 388:16]
  assign auto_in_0_d_bits_size = bundleIn_0_d_bits_size; // @[LazyModule.scala 388:16]
  assign auto_in_0_d_bits_source = bundleIn_0_d_bits_source; // @[LazyModule.scala 388:16]
  assign auto_in_0_d_bits_sink = bundleIn_0_d_bits_sink; // @[LazyModule.scala 388:16]
  assign auto_in_0_d_bits_denied = bundleIn_0_d_bits_denied; // @[LazyModule.scala 388:16]
  assign auto_in_0_d_bits_data = bundleIn_0_d_bits_data; // @[LazyModule.scala 388:16]
  assign auto_in_0_d_bits_corrupt = bundleIn_0_d_bits_corrupt; // @[LazyModule.scala 388:16]
  assign auto_in_0_e_ready = _T; // @[LazyModule.scala 388:16]
  assign auto_out_3_a_valid = mods_3_io_out_a_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_3_a_bits_opcode = mods_3_io_out_a_bits_opcode; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_3_a_bits_param = mods_3_io_out_a_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_3_a_bits_source = mods_3_io_out_a_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_3_a_bits_address = mods_3_io_out_a_bits_address | 36'hc0; // @[Parameters.scala 260:14]
  assign auto_out_3_c_valid = mods_3_io_out_c_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_3_c_bits_opcode = mods_3_io_out_c_bits_opcode; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_3_c_bits_param = mods_3_io_out_c_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_3_c_bits_source = mods_3_io_out_c_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_3_c_bits_address = mods_3_io_out_c_bits_address | 36'hc0; // @[Parameters.scala 260:14]
  assign auto_out_3_c_bits_data = mods_3_io_out_c_bits_data; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_3_d_ready = mods_3_io_out_d_ready; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_3_e_valid = mods_3_io_out_e_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_3_e_bits_sink = mods_3_io_out_e_bits_sink; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_2_a_valid = mods_2_io_out_a_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_2_a_bits_opcode = mods_2_io_out_a_bits_opcode; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_2_a_bits_param = mods_2_io_out_a_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_2_a_bits_source = mods_2_io_out_a_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_2_a_bits_address = mods_2_io_out_a_bits_address | 36'h80; // @[Parameters.scala 260:14]
  assign auto_out_2_c_valid = mods_2_io_out_c_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_2_c_bits_opcode = mods_2_io_out_c_bits_opcode; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_2_c_bits_param = mods_2_io_out_c_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_2_c_bits_source = mods_2_io_out_c_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_2_c_bits_address = mods_2_io_out_c_bits_address | 36'h80; // @[Parameters.scala 260:14]
  assign auto_out_2_c_bits_data = mods_2_io_out_c_bits_data; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_2_d_ready = mods_2_io_out_d_ready; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_2_e_valid = mods_2_io_out_e_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_2_e_bits_sink = mods_2_io_out_e_bits_sink; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_1_a_valid = mods_1_io_out_a_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_1_a_bits_opcode = mods_1_io_out_a_bits_opcode; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_1_a_bits_param = mods_1_io_out_a_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_1_a_bits_source = mods_1_io_out_a_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_1_a_bits_address = mods_1_io_out_a_bits_address | 36'h40; // @[Parameters.scala 260:14]
  assign auto_out_1_c_valid = mods_1_io_out_c_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_1_c_bits_opcode = mods_1_io_out_c_bits_opcode; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_1_c_bits_param = mods_1_io_out_c_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_1_c_bits_source = mods_1_io_out_c_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_1_c_bits_address = mods_1_io_out_c_bits_address | 36'h40; // @[Parameters.scala 260:14]
  assign auto_out_1_c_bits_data = mods_1_io_out_c_bits_data; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_1_d_ready = mods_1_io_out_d_ready; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_1_e_valid = mods_1_io_out_e_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_1_e_bits_sink = mods_1_io_out_e_bits_sink; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_0_a_valid = mods_0_io_out_a_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_0_a_bits_opcode = mods_0_io_out_a_bits_opcode; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_0_a_bits_param = mods_0_io_out_a_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_0_a_bits_source = mods_0_io_out_a_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_0_a_bits_address = mods_0_io_out_a_bits_address; // @[Parameters.scala 260:14]
  assign auto_out_0_c_valid = mods_0_io_out_c_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_0_c_bits_opcode = mods_0_io_out_c_bits_opcode; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_0_c_bits_param = mods_0_io_out_c_bits_param; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_0_c_bits_source = mods_0_io_out_c_bits_source; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_0_c_bits_address = mods_0_io_out_c_bits_address; // @[Parameters.scala 260:14]
  assign auto_out_0_c_bits_data = mods_0_io_out_c_bits_data; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_0_d_ready = mods_0_io_out_d_ready; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_0_e_valid = mods_0_io_out_e_valid; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign auto_out_0_e_bits_sink = mods_0_io_out_e_bits_sink; // @[Nodes.scala 1529:13 ComposableCache.scala 411:11]
  assign out_back_clock = clock;
  assign out_back_reset = reset;
  assign out_back_io_enq_valid = auto_in_4_a_valid & out_iready; // @[ComposableCache.scala 141:30]
  assign out_back_io_enq_bits_read = auto_in_4_a_bits_opcode == 3'h4; // @[ComposableCache.scala 141:30]
  assign out_back_io_enq_bits_index = auto_in_4_a_bits_address[13:3]; // @[ComposableCache.scala 141:30 ComposableCache.scala 141:30]
  assign out_back_io_enq_bits_data = auto_in_4_a_bits_data; // @[ComposableCache.scala 141:30 LazyModule.scala 388:16]
  assign out_back_io_enq_bits_mask = auto_in_4_a_bits_mask; // @[ComposableCache.scala 141:30 LazyModule.scala 388:16]
  assign out_back_io_enq_bits_extra_tlrr_extra_source = auto_in_4_a_bits_source; // @[ComposableCache.scala 141:30 LazyModule.scala 388:16]
  assign out_back_io_enq_bits_extra_tlrr_extra_size = auto_in_4_a_bits_size; // @[ComposableCache.scala 141:30 LazyModule.scala 388:16]
  assign out_back_io_deq_ready = auto_in_4_d_ready & out_oready; // @[ComposableCache.scala 141:30]
  assign mods_0_clock = clock;
  assign mods_0_reset = reset;
  assign mods_0_io_in_a_valid = bundleIn_0_a_valid; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_a_bits_opcode = bundleIn_0_a_bits_opcode; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_a_bits_param = bundleIn_0_a_bits_param; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_a_bits_size = bundleIn_0_a_bits_size; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_a_bits_source = bundleIn_0_a_bits_source; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_a_bits_address = bundleIn_0_a_bits_address; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_a_bits_mask = bundleIn_0_a_bits_mask; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_a_bits_data = bundleIn_0_a_bits_data; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_a_bits_corrupt = bundleIn_0_a_bits_corrupt; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_b_ready = bundleIn_0_b_ready; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_c_valid = bundleIn_0_c_valid; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_c_bits_opcode = bundleIn_0_c_bits_opcode; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_c_bits_param = bundleIn_0_c_bits_param; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_c_bits_size = bundleIn_0_c_bits_size; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_c_bits_source = bundleIn_0_c_bits_source; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_c_bits_address = bundleIn_0_c_bits_address; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_c_bits_data = bundleIn_0_c_bits_data; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_c_bits_corrupt = bundleIn_0_c_bits_corrupt; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_d_ready = bundleIn_0_d_ready; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_e_valid = bundleIn_0_e_valid; // @[ComposableCache.scala 410:23]
  assign mods_0_io_in_e_bits_sink = bundleIn_0_e_bits_sink; // @[ComposableCache.scala 410:23]
  assign mods_0_io_out_a_ready = auto_out_0_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_0_io_out_c_ready = auto_out_0_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_0_io_out_d_valid = auto_out_0_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_0_io_out_d_bits_opcode = auto_out_0_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_0_io_out_d_bits_param = auto_out_0_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_0_io_out_d_bits_size = auto_out_0_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_0_io_out_d_bits_source = auto_out_0_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_0_io_out_d_bits_sink = auto_out_0_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_0_io_out_d_bits_denied = auto_out_0_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_0_io_out_d_bits_data = auto_out_0_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_0_io_out_d_bits_corrupt = auto_out_0_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_0_io_ways_0 = waysReg_0[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_0_io_ways_1 = waysReg_1[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_0_io_ways_2 = waysReg_2[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_0_io_ways_3 = waysReg_3[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_0_io_ways_4 = waysReg_4[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_0_io_ways_5 = waysReg_5[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_0_io_ways_6 = waysReg_6[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_0_io_ways_7 = waysReg_7[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_0_io_ways_8 = waysReg_8[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_0_io_ways_9 = waysReg_9[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_0_io_ways_10 = waysReg_10[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_0_io_ways_11 = waysReg_11[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_0_io_ways_12 = waysReg_12[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_0_io_ways_13 = waysReg_13[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_0_io_divs_0 = divisors_0; // @[ComposableCache.scala 439:25]
  assign mods_0_io_divs_1 = divisors_1; // @[ComposableCache.scala 439:25]
  assign mods_0_io_divs_2 = divisors_2; // @[ComposableCache.scala 439:25]
  assign mods_0_io_divs_3 = divisors_3; // @[ComposableCache.scala 439:25]
  assign mods_0_io_divs_4 = divisors_4; // @[ComposableCache.scala 439:25]
  assign mods_0_io_divs_5 = divisors_5; // @[ComposableCache.scala 439:25]
  assign mods_0_io_divs_6 = divisors_6; // @[ComposableCache.scala 439:25]
  assign mods_0_io_divs_7 = divisors_7; // @[ComposableCache.scala 439:25]
  assign mods_0_io_divs_8 = divisors_8; // @[ComposableCache.scala 439:25]
  assign mods_0_io_divs_9 = divisors_9; // @[ComposableCache.scala 439:25]
  assign mods_0_io_divs_10 = divisors_10; // @[ComposableCache.scala 439:25]
  assign mods_0_io_divs_11 = divisors_11; // @[ComposableCache.scala 439:25]
  assign mods_0_io_divs_12 = divisors_12; // @[ComposableCache.scala 439:25]
  assign mods_0_io_divs_13 = divisors_13; // @[ComposableCache.scala 439:25]
  assign mods_0_io_req_valid = flushInValid & flushSelect; // @[ComposableCache.scala 420:46]
  assign mods_0_io_req_bits_address = flushInAddress[35:0]; // @[ComposableCache.scala 421:37]
  assign mods_0_io_resp_ready = ~flushOutValid; // @[ComposableCache.scala 422:34]
  assign mods_0_io_side_adr_valid = sideband_io_bs_adr_0_valid; // @[ComposableCache.scala 481:24]
  assign mods_0_io_side_adr_bits_way = sideband_io_bs_adr_0_bits_way; // @[ComposableCache.scala 481:24]
  assign mods_0_io_side_adr_bits_set = sideband_io_bs_adr_0_bits_set; // @[ComposableCache.scala 481:24]
  assign mods_0_io_side_adr_bits_beat = sideband_io_bs_adr_0_bits_beat; // @[ComposableCache.scala 481:24]
  assign mods_0_io_side_adr_bits_mask = sideband_io_bs_adr_0_bits_mask; // @[ComposableCache.scala 481:24]
  assign mods_0_io_side_adr_bits_write = sideband_io_bs_adr_0_bits_write; // @[ComposableCache.scala 481:24]
  assign mods_0_io_side_wdat_data = sideband_io_bs_wdat_0_data; // @[ComposableCache.scala 482:24]
  assign mods_0_io_side_wdat_poison = sideband_io_bs_wdat_0_poison; // @[ComposableCache.scala 482:24]
  assign mods_0_io_error_valid = injectValid; // @[ComposableCache.scala 425:32]
  assign mods_0_io_error_bits_dir = injectType; // @[ComposableCache.scala 426:48]
  assign mods_0_io_error_bits_bit = injectBit; // @[ComposableCache.scala 427:35]
  assign mods_1_clock = clock;
  assign mods_1_reset = reset;
  assign mods_1_io_in_a_valid = bundleIn_1_a_valid; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_a_bits_opcode = bundleIn_1_a_bits_opcode; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_a_bits_param = bundleIn_1_a_bits_param; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_a_bits_size = bundleIn_1_a_bits_size; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_a_bits_source = bundleIn_1_a_bits_source; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_a_bits_address = bundleIn_1_a_bits_address; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_a_bits_mask = bundleIn_1_a_bits_mask; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_a_bits_data = bundleIn_1_a_bits_data; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_a_bits_corrupt = bundleIn_1_a_bits_corrupt; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_b_ready = bundleIn_1_b_ready; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_c_valid = bundleIn_1_c_valid; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_c_bits_opcode = bundleIn_1_c_bits_opcode; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_c_bits_param = bundleIn_1_c_bits_param; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_c_bits_size = bundleIn_1_c_bits_size; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_c_bits_source = bundleIn_1_c_bits_source; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_c_bits_address = bundleIn_1_c_bits_address; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_c_bits_data = bundleIn_1_c_bits_data; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_c_bits_corrupt = bundleIn_1_c_bits_corrupt; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_d_ready = bundleIn_1_d_ready; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_e_valid = bundleIn_1_e_valid; // @[ComposableCache.scala 410:23]
  assign mods_1_io_in_e_bits_sink = bundleIn_1_e_bits_sink; // @[ComposableCache.scala 410:23]
  assign mods_1_io_out_a_ready = auto_out_1_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_1_io_out_c_ready = auto_out_1_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_1_io_out_d_valid = auto_out_1_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_1_io_out_d_bits_opcode = auto_out_1_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_1_io_out_d_bits_param = auto_out_1_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_1_io_out_d_bits_size = auto_out_1_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_1_io_out_d_bits_source = auto_out_1_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_1_io_out_d_bits_sink = auto_out_1_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_1_io_out_d_bits_denied = auto_out_1_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_1_io_out_d_bits_data = auto_out_1_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_1_io_out_d_bits_corrupt = auto_out_1_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_1_io_ways_0 = waysReg_0[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_1_io_ways_1 = waysReg_1[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_1_io_ways_2 = waysReg_2[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_1_io_ways_3 = waysReg_3[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_1_io_ways_4 = waysReg_4[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_1_io_ways_5 = waysReg_5[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_1_io_ways_6 = waysReg_6[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_1_io_ways_7 = waysReg_7[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_1_io_ways_8 = waysReg_8[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_1_io_ways_9 = waysReg_9[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_1_io_ways_10 = waysReg_10[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_1_io_ways_11 = waysReg_11[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_1_io_ways_12 = waysReg_12[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_1_io_ways_13 = waysReg_13[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_1_io_divs_0 = divisors_0; // @[ComposableCache.scala 439:25]
  assign mods_1_io_divs_1 = divisors_1; // @[ComposableCache.scala 439:25]
  assign mods_1_io_divs_2 = divisors_2; // @[ComposableCache.scala 439:25]
  assign mods_1_io_divs_3 = divisors_3; // @[ComposableCache.scala 439:25]
  assign mods_1_io_divs_4 = divisors_4; // @[ComposableCache.scala 439:25]
  assign mods_1_io_divs_5 = divisors_5; // @[ComposableCache.scala 439:25]
  assign mods_1_io_divs_6 = divisors_6; // @[ComposableCache.scala 439:25]
  assign mods_1_io_divs_7 = divisors_7; // @[ComposableCache.scala 439:25]
  assign mods_1_io_divs_8 = divisors_8; // @[ComposableCache.scala 439:25]
  assign mods_1_io_divs_9 = divisors_9; // @[ComposableCache.scala 439:25]
  assign mods_1_io_divs_10 = divisors_10; // @[ComposableCache.scala 439:25]
  assign mods_1_io_divs_11 = divisors_11; // @[ComposableCache.scala 439:25]
  assign mods_1_io_divs_12 = divisors_12; // @[ComposableCache.scala 439:25]
  assign mods_1_io_divs_13 = divisors_13; // @[ComposableCache.scala 439:25]
  assign mods_1_io_req_valid = flushInValid & flushSelect_1; // @[ComposableCache.scala 420:46]
  assign mods_1_io_req_bits_address = flushInAddress[35:0]; // @[ComposableCache.scala 421:37]
  assign mods_1_io_resp_ready = ~flushOutValid; // @[ComposableCache.scala 422:34]
  assign mods_1_io_side_adr_valid = sideband_io_bs_adr_1_valid; // @[ComposableCache.scala 481:24]
  assign mods_1_io_side_adr_bits_way = sideband_io_bs_adr_1_bits_way; // @[ComposableCache.scala 481:24]
  assign mods_1_io_side_adr_bits_set = sideband_io_bs_adr_1_bits_set; // @[ComposableCache.scala 481:24]
  assign mods_1_io_side_adr_bits_beat = sideband_io_bs_adr_1_bits_beat; // @[ComposableCache.scala 481:24]
  assign mods_1_io_side_adr_bits_mask = sideband_io_bs_adr_1_bits_mask; // @[ComposableCache.scala 481:24]
  assign mods_1_io_side_adr_bits_write = sideband_io_bs_adr_1_bits_write; // @[ComposableCache.scala 481:24]
  assign mods_1_io_side_wdat_data = sideband_io_bs_wdat_1_data; // @[ComposableCache.scala 482:24]
  assign mods_1_io_side_wdat_poison = sideband_io_bs_wdat_1_poison; // @[ComposableCache.scala 482:24]
  assign mods_1_io_error_valid = injectValid; // @[ComposableCache.scala 425:32]
  assign mods_1_io_error_bits_dir = injectType; // @[ComposableCache.scala 426:48]
  assign mods_1_io_error_bits_bit = injectBit; // @[ComposableCache.scala 427:35]
  assign mods_2_clock = clock;
  assign mods_2_reset = reset;
  assign mods_2_io_in_a_valid = bundleIn_2_a_valid; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_a_bits_opcode = bundleIn_2_a_bits_opcode; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_a_bits_param = bundleIn_2_a_bits_param; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_a_bits_size = bundleIn_2_a_bits_size; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_a_bits_source = bundleIn_2_a_bits_source; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_a_bits_address = bundleIn_2_a_bits_address; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_a_bits_mask = bundleIn_2_a_bits_mask; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_a_bits_data = bundleIn_2_a_bits_data; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_a_bits_corrupt = bundleIn_2_a_bits_corrupt; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_b_ready = bundleIn_2_b_ready; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_c_valid = bundleIn_2_c_valid; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_c_bits_opcode = bundleIn_2_c_bits_opcode; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_c_bits_param = bundleIn_2_c_bits_param; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_c_bits_size = bundleIn_2_c_bits_size; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_c_bits_source = bundleIn_2_c_bits_source; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_c_bits_address = bundleIn_2_c_bits_address; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_c_bits_data = bundleIn_2_c_bits_data; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_c_bits_corrupt = bundleIn_2_c_bits_corrupt; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_d_ready = bundleIn_2_d_ready; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_e_valid = bundleIn_2_e_valid; // @[ComposableCache.scala 410:23]
  assign mods_2_io_in_e_bits_sink = bundleIn_2_e_bits_sink; // @[ComposableCache.scala 410:23]
  assign mods_2_io_out_a_ready = auto_out_2_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_2_io_out_c_ready = auto_out_2_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_2_io_out_d_valid = auto_out_2_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_2_io_out_d_bits_opcode = auto_out_2_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_2_io_out_d_bits_param = auto_out_2_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_2_io_out_d_bits_size = auto_out_2_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_2_io_out_d_bits_source = auto_out_2_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_2_io_out_d_bits_sink = auto_out_2_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_2_io_out_d_bits_denied = auto_out_2_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_2_io_out_d_bits_data = auto_out_2_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_2_io_out_d_bits_corrupt = auto_out_2_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_2_io_ways_0 = waysReg_0[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_2_io_ways_1 = waysReg_1[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_2_io_ways_2 = waysReg_2[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_2_io_ways_3 = waysReg_3[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_2_io_ways_4 = waysReg_4[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_2_io_ways_5 = waysReg_5[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_2_io_ways_6 = waysReg_6[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_2_io_ways_7 = waysReg_7[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_2_io_ways_8 = waysReg_8[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_2_io_ways_9 = waysReg_9[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_2_io_ways_10 = waysReg_10[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_2_io_ways_11 = waysReg_11[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_2_io_ways_12 = waysReg_12[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_2_io_ways_13 = waysReg_13[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_2_io_divs_0 = divisors_0; // @[ComposableCache.scala 439:25]
  assign mods_2_io_divs_1 = divisors_1; // @[ComposableCache.scala 439:25]
  assign mods_2_io_divs_2 = divisors_2; // @[ComposableCache.scala 439:25]
  assign mods_2_io_divs_3 = divisors_3; // @[ComposableCache.scala 439:25]
  assign mods_2_io_divs_4 = divisors_4; // @[ComposableCache.scala 439:25]
  assign mods_2_io_divs_5 = divisors_5; // @[ComposableCache.scala 439:25]
  assign mods_2_io_divs_6 = divisors_6; // @[ComposableCache.scala 439:25]
  assign mods_2_io_divs_7 = divisors_7; // @[ComposableCache.scala 439:25]
  assign mods_2_io_divs_8 = divisors_8; // @[ComposableCache.scala 439:25]
  assign mods_2_io_divs_9 = divisors_9; // @[ComposableCache.scala 439:25]
  assign mods_2_io_divs_10 = divisors_10; // @[ComposableCache.scala 439:25]
  assign mods_2_io_divs_11 = divisors_11; // @[ComposableCache.scala 439:25]
  assign mods_2_io_divs_12 = divisors_12; // @[ComposableCache.scala 439:25]
  assign mods_2_io_divs_13 = divisors_13; // @[ComposableCache.scala 439:25]
  assign mods_2_io_req_valid = flushInValid & flushSelect_2; // @[ComposableCache.scala 420:46]
  assign mods_2_io_req_bits_address = flushInAddress[35:0]; // @[ComposableCache.scala 421:37]
  assign mods_2_io_resp_ready = ~flushOutValid; // @[ComposableCache.scala 422:34]
  assign mods_2_io_side_adr_valid = sideband_io_bs_adr_2_valid; // @[ComposableCache.scala 481:24]
  assign mods_2_io_side_adr_bits_way = sideband_io_bs_adr_2_bits_way; // @[ComposableCache.scala 481:24]
  assign mods_2_io_side_adr_bits_set = sideband_io_bs_adr_2_bits_set; // @[ComposableCache.scala 481:24]
  assign mods_2_io_side_adr_bits_beat = sideband_io_bs_adr_2_bits_beat; // @[ComposableCache.scala 481:24]
  assign mods_2_io_side_adr_bits_mask = sideband_io_bs_adr_2_bits_mask; // @[ComposableCache.scala 481:24]
  assign mods_2_io_side_adr_bits_write = sideband_io_bs_adr_2_bits_write; // @[ComposableCache.scala 481:24]
  assign mods_2_io_side_wdat_data = sideband_io_bs_wdat_2_data; // @[ComposableCache.scala 482:24]
  assign mods_2_io_side_wdat_poison = sideband_io_bs_wdat_2_poison; // @[ComposableCache.scala 482:24]
  assign mods_2_io_error_valid = injectValid; // @[ComposableCache.scala 425:32]
  assign mods_2_io_error_bits_dir = injectType; // @[ComposableCache.scala 426:48]
  assign mods_2_io_error_bits_bit = injectBit; // @[ComposableCache.scala 427:35]
  assign mods_3_clock = clock;
  assign mods_3_reset = reset;
  assign mods_3_io_in_a_valid = bundleIn_3_a_valid; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_a_bits_opcode = bundleIn_3_a_bits_opcode; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_a_bits_param = bundleIn_3_a_bits_param; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_a_bits_size = bundleIn_3_a_bits_size; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_a_bits_source = bundleIn_3_a_bits_source; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_a_bits_address = bundleIn_3_a_bits_address; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_a_bits_mask = bundleIn_3_a_bits_mask; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_a_bits_data = bundleIn_3_a_bits_data; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_a_bits_corrupt = bundleIn_3_a_bits_corrupt; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_b_ready = bundleIn_3_b_ready; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_c_valid = bundleIn_3_c_valid; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_c_bits_opcode = bundleIn_3_c_bits_opcode; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_c_bits_param = bundleIn_3_c_bits_param; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_c_bits_size = bundleIn_3_c_bits_size; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_c_bits_source = bundleIn_3_c_bits_source; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_c_bits_address = bundleIn_3_c_bits_address; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_c_bits_data = bundleIn_3_c_bits_data; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_c_bits_corrupt = bundleIn_3_c_bits_corrupt; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_d_ready = bundleIn_3_d_ready; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_e_valid = bundleIn_3_e_valid; // @[ComposableCache.scala 410:23]
  assign mods_3_io_in_e_bits_sink = bundleIn_3_e_bits_sink; // @[ComposableCache.scala 410:23]
  assign mods_3_io_out_a_ready = auto_out_3_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_3_io_out_c_ready = auto_out_3_c_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_3_io_out_d_valid = auto_out_3_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_3_io_out_d_bits_opcode = auto_out_3_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_3_io_out_d_bits_param = auto_out_3_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_3_io_out_d_bits_size = auto_out_3_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_3_io_out_d_bits_source = auto_out_3_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_3_io_out_d_bits_sink = auto_out_3_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_3_io_out_d_bits_denied = auto_out_3_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_3_io_out_d_bits_data = auto_out_3_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_3_io_out_d_bits_corrupt = auto_out_3_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign mods_3_io_ways_0 = waysReg_0[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_3_io_ways_1 = waysReg_1[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_3_io_ways_2 = waysReg_2[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_3_io_ways_3 = waysReg_3[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_3_io_ways_4 = waysReg_4[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_3_io_ways_5 = waysReg_5[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_3_io_ways_6 = waysReg_6[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_3_io_ways_7 = waysReg_7[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_3_io_ways_8 = waysReg_8[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_3_io_ways_9 = waysReg_9[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_3_io_ways_10 = waysReg_10[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_3_io_ways_11 = waysReg_11[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_3_io_ways_12 = waysReg_12[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_3_io_ways_13 = waysReg_13[7:0]; // @[ComposableCache.scala 438:25]
  assign mods_3_io_divs_0 = divisors_0; // @[ComposableCache.scala 439:25]
  assign mods_3_io_divs_1 = divisors_1; // @[ComposableCache.scala 439:25]
  assign mods_3_io_divs_2 = divisors_2; // @[ComposableCache.scala 439:25]
  assign mods_3_io_divs_3 = divisors_3; // @[ComposableCache.scala 439:25]
  assign mods_3_io_divs_4 = divisors_4; // @[ComposableCache.scala 439:25]
  assign mods_3_io_divs_5 = divisors_5; // @[ComposableCache.scala 439:25]
  assign mods_3_io_divs_6 = divisors_6; // @[ComposableCache.scala 439:25]
  assign mods_3_io_divs_7 = divisors_7; // @[ComposableCache.scala 439:25]
  assign mods_3_io_divs_8 = divisors_8; // @[ComposableCache.scala 439:25]
  assign mods_3_io_divs_9 = divisors_9; // @[ComposableCache.scala 439:25]
  assign mods_3_io_divs_10 = divisors_10; // @[ComposableCache.scala 439:25]
  assign mods_3_io_divs_11 = divisors_11; // @[ComposableCache.scala 439:25]
  assign mods_3_io_divs_12 = divisors_12; // @[ComposableCache.scala 439:25]
  assign mods_3_io_divs_13 = divisors_13; // @[ComposableCache.scala 439:25]
  assign mods_3_io_req_valid = flushInValid & flushSelect_3; // @[ComposableCache.scala 420:46]
  assign mods_3_io_req_bits_address = flushInAddress[35:0]; // @[ComposableCache.scala 421:37]
  assign mods_3_io_resp_ready = ~flushOutValid; // @[ComposableCache.scala 422:34]
  assign mods_3_io_side_adr_valid = sideband_io_bs_adr_3_valid; // @[ComposableCache.scala 481:24]
  assign mods_3_io_side_adr_bits_way = sideband_io_bs_adr_3_bits_way; // @[ComposableCache.scala 481:24]
  assign mods_3_io_side_adr_bits_set = sideband_io_bs_adr_3_bits_set; // @[ComposableCache.scala 481:24]
  assign mods_3_io_side_adr_bits_beat = sideband_io_bs_adr_3_bits_beat; // @[ComposableCache.scala 481:24]
  assign mods_3_io_side_adr_bits_mask = sideband_io_bs_adr_3_bits_mask; // @[ComposableCache.scala 481:24]
  assign mods_3_io_side_adr_bits_write = sideband_io_bs_adr_3_bits_write; // @[ComposableCache.scala 481:24]
  assign mods_3_io_side_wdat_data = sideband_io_bs_wdat_3_data; // @[ComposableCache.scala 482:24]
  assign mods_3_io_side_wdat_poison = sideband_io_bs_wdat_3_poison; // @[ComposableCache.scala 482:24]
  assign mods_3_io_error_valid = injectValid; // @[ComposableCache.scala 425:32]
  assign mods_3_io_error_bits_dir = injectType; // @[ComposableCache.scala 426:48]
  assign mods_3_io_error_bits_bit = injectBit; // @[ComposableCache.scala 427:35]
  assign sideband_clock = clock;
  assign sideband_reset = reset;
  assign sideband_io_a_valid = auto_side_in_a_valid; // @[ComposableCache.scala 125:51 LazyModule.scala 388:16]
  assign sideband_io_a_bits_opcode = auto_side_in_a_bits_opcode; // @[ComposableCache.scala 125:51 LazyModule.scala 388:16]
  assign sideband_io_a_bits_param = auto_side_in_a_bits_param; // @[ComposableCache.scala 125:51 LazyModule.scala 388:16]
  assign sideband_io_a_bits_size = auto_side_in_a_bits_size; // @[ComposableCache.scala 125:51 LazyModule.scala 388:16]
  assign sideband_io_a_bits_source = auto_side_in_a_bits_source; // @[ComposableCache.scala 125:51 LazyModule.scala 388:16]
  assign sideband_io_a_bits_address = auto_side_in_a_bits_address; // @[ComposableCache.scala 125:51 LazyModule.scala 388:16]
  assign sideband_io_a_bits_mask = auto_side_in_a_bits_mask; // @[ComposableCache.scala 125:51 LazyModule.scala 388:16]
  assign sideband_io_a_bits_data = auto_side_in_a_bits_data; // @[ComposableCache.scala 125:51 LazyModule.scala 388:16]
  assign sideband_io_a_bits_corrupt = auto_side_in_a_bits_corrupt; // @[ComposableCache.scala 125:51 LazyModule.scala 388:16]
  assign sideband_io_d_ready = auto_side_in_d_ready; // @[ComposableCache.scala 125:51 LazyModule.scala 388:16]
  assign sideband_io_threshold = wayEnable; // @[ComposableCache.scala 479:29]
  assign sideband_io_bs_rdat_0_data = mods_0_io_side_rdat_data; // @[ComposableCache.scala 483:32]
  assign sideband_io_bs_rdat_1_data = mods_1_io_side_rdat_data; // @[ComposableCache.scala 483:32]
  assign sideband_io_bs_rdat_2_data = mods_2_io_side_rdat_data; // @[ComposableCache.scala 483:32]
  assign sideband_io_bs_rdat_3_data = mods_3_io_side_rdat_data; // @[ComposableCache.scala 483:32]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayEnable <= 3'h0;
    end else begin
      wayEnable <= _GEN_142[2:0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_0_0 <= 1'h1;
    end else if (out_f_woready_57) begin
      wayMap_0_0 <= out_back_io_deq_bits_data[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_0_1 <= 1'h1;
    end else if (out_f_woready_58) begin
      wayMap_0_1 <= out_back_io_deq_bits_data[1];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_0_2 <= 1'h1;
    end else if (out_f_woready_59) begin
      wayMap_0_2 <= out_back_io_deq_bits_data[2];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_0_3 <= 1'h1;
    end else if (out_f_woready_60) begin
      wayMap_0_3 <= out_back_io_deq_bits_data[3];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_0_4 <= 1'h1;
    end else if (out_f_woready_61) begin
      wayMap_0_4 <= out_back_io_deq_bits_data[4];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_0_5 <= 1'h1;
    end else if (out_f_woready_62) begin
      wayMap_0_5 <= out_back_io_deq_bits_data[5];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_0_6 <= 1'h1;
    end else if (out_f_woready_63) begin
      wayMap_0_6 <= out_back_io_deq_bits_data[6];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_0_7 <= 1'h1;
    end else if (out_f_woready_64) begin
      wayMap_0_7 <= out_back_io_deq_bits_data[7];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_1_0 <= 1'h1;
    end else if (out_f_woready_15) begin
      wayMap_1_0 <= out_back_io_deq_bits_data[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_1_1 <= 1'h1;
    end else if (out_f_woready_16) begin
      wayMap_1_1 <= out_back_io_deq_bits_data[1];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_1_2 <= 1'h1;
    end else if (out_f_woready_17) begin
      wayMap_1_2 <= out_back_io_deq_bits_data[2];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_1_3 <= 1'h1;
    end else if (out_f_woready_18) begin
      wayMap_1_3 <= out_back_io_deq_bits_data[3];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_1_4 <= 1'h1;
    end else if (out_f_woready_19) begin
      wayMap_1_4 <= out_back_io_deq_bits_data[4];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_1_5 <= 1'h1;
    end else if (out_f_woready_20) begin
      wayMap_1_5 <= out_back_io_deq_bits_data[5];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_1_6 <= 1'h1;
    end else if (out_f_woready_21) begin
      wayMap_1_6 <= out_back_io_deq_bits_data[6];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_1_7 <= 1'h1;
    end else if (out_f_woready_22) begin
      wayMap_1_7 <= out_back_io_deq_bits_data[7];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_2_0 <= 1'h1;
    end else if (out_f_woready_127) begin
      wayMap_2_0 <= out_back_io_deq_bits_data[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_2_1 <= 1'h1;
    end else if (out_f_woready_128) begin
      wayMap_2_1 <= out_back_io_deq_bits_data[1];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_2_2 <= 1'h1;
    end else if (out_f_woready_129) begin
      wayMap_2_2 <= out_back_io_deq_bits_data[2];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_2_3 <= 1'h1;
    end else if (out_f_woready_130) begin
      wayMap_2_3 <= out_back_io_deq_bits_data[3];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_2_4 <= 1'h1;
    end else if (out_f_woready_131) begin
      wayMap_2_4 <= out_back_io_deq_bits_data[4];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_2_5 <= 1'h1;
    end else if (out_f_woready_132) begin
      wayMap_2_5 <= out_back_io_deq_bits_data[5];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_2_6 <= 1'h1;
    end else if (out_f_woready_133) begin
      wayMap_2_6 <= out_back_io_deq_bits_data[6];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_2_7 <= 1'h1;
    end else if (out_f_woready_134) begin
      wayMap_2_7 <= out_back_io_deq_bits_data[7];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_3_0 <= 1'h1;
    end else if (out_f_woready_94) begin
      wayMap_3_0 <= out_back_io_deq_bits_data[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_3_1 <= 1'h1;
    end else if (out_f_woready_95) begin
      wayMap_3_1 <= out_back_io_deq_bits_data[1];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_3_2 <= 1'h1;
    end else if (out_f_woready_96) begin
      wayMap_3_2 <= out_back_io_deq_bits_data[2];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_3_3 <= 1'h1;
    end else if (out_f_woready_97) begin
      wayMap_3_3 <= out_back_io_deq_bits_data[3];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_3_4 <= 1'h1;
    end else if (out_f_woready_98) begin
      wayMap_3_4 <= out_back_io_deq_bits_data[4];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_3_5 <= 1'h1;
    end else if (out_f_woready_99) begin
      wayMap_3_5 <= out_back_io_deq_bits_data[5];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_3_6 <= 1'h1;
    end else if (out_f_woready_100) begin
      wayMap_3_6 <= out_back_io_deq_bits_data[6];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_3_7 <= 1'h1;
    end else if (out_f_woready_101) begin
      wayMap_3_7 <= out_back_io_deq_bits_data[7];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_4_0 <= 1'h1;
    end else if (out_f_woready_46) begin
      wayMap_4_0 <= out_back_io_deq_bits_data[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_4_1 <= 1'h1;
    end else if (out_f_woready_47) begin
      wayMap_4_1 <= out_back_io_deq_bits_data[1];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_4_2 <= 1'h1;
    end else if (out_f_woready_48) begin
      wayMap_4_2 <= out_back_io_deq_bits_data[2];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_4_3 <= 1'h1;
    end else if (out_f_woready_49) begin
      wayMap_4_3 <= out_back_io_deq_bits_data[3];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_4_4 <= 1'h1;
    end else if (out_f_woready_50) begin
      wayMap_4_4 <= out_back_io_deq_bits_data[4];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_4_5 <= 1'h1;
    end else if (out_f_woready_51) begin
      wayMap_4_5 <= out_back_io_deq_bits_data[5];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_4_6 <= 1'h1;
    end else if (out_f_woready_52) begin
      wayMap_4_6 <= out_back_io_deq_bits_data[6];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_4_7 <= 1'h1;
    end else if (out_f_woready_53) begin
      wayMap_4_7 <= out_back_io_deq_bits_data[7];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_5_0 <= 1'h1;
    end else if (out_f_woready_25) begin
      wayMap_5_0 <= out_back_io_deq_bits_data[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_5_1 <= 1'h1;
    end else if (out_f_woready_26) begin
      wayMap_5_1 <= out_back_io_deq_bits_data[1];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_5_2 <= 1'h1;
    end else if (out_f_woready_27) begin
      wayMap_5_2 <= out_back_io_deq_bits_data[2];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_5_3 <= 1'h1;
    end else if (out_f_woready_28) begin
      wayMap_5_3 <= out_back_io_deq_bits_data[3];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_5_4 <= 1'h1;
    end else if (out_f_woready_29) begin
      wayMap_5_4 <= out_back_io_deq_bits_data[4];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_5_5 <= 1'h1;
    end else if (out_f_woready_30) begin
      wayMap_5_5 <= out_back_io_deq_bits_data[5];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_5_6 <= 1'h1;
    end else if (out_f_woready_31) begin
      wayMap_5_6 <= out_back_io_deq_bits_data[6];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_5_7 <= 1'h1;
    end else if (out_f_woready_32) begin
      wayMap_5_7 <= out_back_io_deq_bits_data[7];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_6_0 <= 1'h1;
    end else if (out_f_woready_142) begin
      wayMap_6_0 <= out_back_io_deq_bits_data[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_6_1 <= 1'h1;
    end else if (out_f_woready_143) begin
      wayMap_6_1 <= out_back_io_deq_bits_data[1];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_6_2 <= 1'h1;
    end else if (out_f_woready_144) begin
      wayMap_6_2 <= out_back_io_deq_bits_data[2];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_6_3 <= 1'h1;
    end else if (out_f_woready_145) begin
      wayMap_6_3 <= out_back_io_deq_bits_data[3];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_6_4 <= 1'h1;
    end else if (out_f_woready_146) begin
      wayMap_6_4 <= out_back_io_deq_bits_data[4];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_6_5 <= 1'h1;
    end else if (out_f_woready_147) begin
      wayMap_6_5 <= out_back_io_deq_bits_data[5];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_6_6 <= 1'h1;
    end else if (out_f_woready_148) begin
      wayMap_6_6 <= out_back_io_deq_bits_data[6];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_6_7 <= 1'h1;
    end else if (out_f_woready_149) begin
      wayMap_6_7 <= out_back_io_deq_bits_data[7];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_7_0 <= 1'h1;
    end else if (out_f_woready_114) begin
      wayMap_7_0 <= out_back_io_deq_bits_data[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_7_1 <= 1'h1;
    end else if (out_f_woready_115) begin
      wayMap_7_1 <= out_back_io_deq_bits_data[1];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_7_2 <= 1'h1;
    end else if (out_f_woready_116) begin
      wayMap_7_2 <= out_back_io_deq_bits_data[2];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_7_3 <= 1'h1;
    end else if (out_f_woready_117) begin
      wayMap_7_3 <= out_back_io_deq_bits_data[3];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_7_4 <= 1'h1;
    end else if (out_f_woready_118) begin
      wayMap_7_4 <= out_back_io_deq_bits_data[4];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_7_5 <= 1'h1;
    end else if (out_f_woready_119) begin
      wayMap_7_5 <= out_back_io_deq_bits_data[5];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_7_6 <= 1'h1;
    end else if (out_f_woready_120) begin
      wayMap_7_6 <= out_back_io_deq_bits_data[6];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_7_7 <= 1'h1;
    end else if (out_f_woready_121) begin
      wayMap_7_7 <= out_back_io_deq_bits_data[7];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_8_0 <= 1'h1;
    end else if (out_f_woready_80) begin
      wayMap_8_0 <= out_back_io_deq_bits_data[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_8_1 <= 1'h1;
    end else if (out_f_woready_81) begin
      wayMap_8_1 <= out_back_io_deq_bits_data[1];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_8_2 <= 1'h1;
    end else if (out_f_woready_82) begin
      wayMap_8_2 <= out_back_io_deq_bits_data[2];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_8_3 <= 1'h1;
    end else if (out_f_woready_83) begin
      wayMap_8_3 <= out_back_io_deq_bits_data[3];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_8_4 <= 1'h1;
    end else if (out_f_woready_84) begin
      wayMap_8_4 <= out_back_io_deq_bits_data[4];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_8_5 <= 1'h1;
    end else if (out_f_woready_85) begin
      wayMap_8_5 <= out_back_io_deq_bits_data[5];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_8_6 <= 1'h1;
    end else if (out_f_woready_86) begin
      wayMap_8_6 <= out_back_io_deq_bits_data[6];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_8_7 <= 1'h1;
    end else if (out_f_woready_87) begin
      wayMap_8_7 <= out_back_io_deq_bits_data[7];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_9_0 <= 1'h1;
    end else if (out_f_woready_36) begin
      wayMap_9_0 <= out_back_io_deq_bits_data[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_9_1 <= 1'h1;
    end else if (out_f_woready_37) begin
      wayMap_9_1 <= out_back_io_deq_bits_data[1];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_9_2 <= 1'h1;
    end else if (out_f_woready_38) begin
      wayMap_9_2 <= out_back_io_deq_bits_data[2];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_9_3 <= 1'h1;
    end else if (out_f_woready_39) begin
      wayMap_9_3 <= out_back_io_deq_bits_data[3];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_9_4 <= 1'h1;
    end else if (out_f_woready_40) begin
      wayMap_9_4 <= out_back_io_deq_bits_data[4];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_9_5 <= 1'h1;
    end else if (out_f_woready_41) begin
      wayMap_9_5 <= out_back_io_deq_bits_data[5];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_9_6 <= 1'h1;
    end else if (out_f_woready_42) begin
      wayMap_9_6 <= out_back_io_deq_bits_data[6];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_9_7 <= 1'h1;
    end else if (out_f_woready_43) begin
      wayMap_9_7 <= out_back_io_deq_bits_data[7];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_10_0 <= 1'h1;
    end else if (out_f_woready_68) begin
      wayMap_10_0 <= out_back_io_deq_bits_data[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_10_1 <= 1'h1;
    end else if (out_f_woready_69) begin
      wayMap_10_1 <= out_back_io_deq_bits_data[1];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_10_2 <= 1'h1;
    end else if (out_f_woready_70) begin
      wayMap_10_2 <= out_back_io_deq_bits_data[2];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_10_3 <= 1'h1;
    end else if (out_f_woready_71) begin
      wayMap_10_3 <= out_back_io_deq_bits_data[3];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_10_4 <= 1'h1;
    end else if (out_f_woready_72) begin
      wayMap_10_4 <= out_back_io_deq_bits_data[4];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_10_5 <= 1'h1;
    end else if (out_f_woready_73) begin
      wayMap_10_5 <= out_back_io_deq_bits_data[5];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_10_6 <= 1'h1;
    end else if (out_f_woready_74) begin
      wayMap_10_6 <= out_back_io_deq_bits_data[6];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_10_7 <= 1'h1;
    end else if (out_f_woready_75) begin
      wayMap_10_7 <= out_back_io_deq_bits_data[7];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_11_0 <= 1'h1;
    end else if (out_f_woready_104) begin
      wayMap_11_0 <= out_back_io_deq_bits_data[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_11_1 <= 1'h1;
    end else if (out_f_woready_105) begin
      wayMap_11_1 <= out_back_io_deq_bits_data[1];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_11_2 <= 1'h1;
    end else if (out_f_woready_106) begin
      wayMap_11_2 <= out_back_io_deq_bits_data[2];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_11_3 <= 1'h1;
    end else if (out_f_woready_107) begin
      wayMap_11_3 <= out_back_io_deq_bits_data[3];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_11_4 <= 1'h1;
    end else if (out_f_woready_108) begin
      wayMap_11_4 <= out_back_io_deq_bits_data[4];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_11_5 <= 1'h1;
    end else if (out_f_woready_109) begin
      wayMap_11_5 <= out_back_io_deq_bits_data[5];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_11_6 <= 1'h1;
    end else if (out_f_woready_110) begin
      wayMap_11_6 <= out_back_io_deq_bits_data[6];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_11_7 <= 1'h1;
    end else if (out_f_woready_111) begin
      wayMap_11_7 <= out_back_io_deq_bits_data[7];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_12_0 <= 1'h1;
    end else if (out_f_woready_152) begin
      wayMap_12_0 <= out_back_io_deq_bits_data[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_12_1 <= 1'h1;
    end else if (out_f_woready_153) begin
      wayMap_12_1 <= out_back_io_deq_bits_data[1];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_12_2 <= 1'h1;
    end else if (out_f_woready_154) begin
      wayMap_12_2 <= out_back_io_deq_bits_data[2];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_12_3 <= 1'h1;
    end else if (out_f_woready_155) begin
      wayMap_12_3 <= out_back_io_deq_bits_data[3];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_12_4 <= 1'h1;
    end else if (out_f_woready_156) begin
      wayMap_12_4 <= out_back_io_deq_bits_data[4];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_12_5 <= 1'h1;
    end else if (out_f_woready_157) begin
      wayMap_12_5 <= out_back_io_deq_bits_data[5];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_12_6 <= 1'h1;
    end else if (out_f_woready_158) begin
      wayMap_12_6 <= out_back_io_deq_bits_data[6];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_12_7 <= 1'h1;
    end else if (out_f_woready_159) begin
      wayMap_12_7 <= out_back_io_deq_bits_data[7];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_13_0 <= 1'h1;
    end else if (out_f_woready_4) begin
      wayMap_13_0 <= out_back_io_deq_bits_data[0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_13_1 <= 1'h1;
    end else if (out_f_woready_5) begin
      wayMap_13_1 <= out_back_io_deq_bits_data[1];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_13_2 <= 1'h1;
    end else if (out_f_woready_6) begin
      wayMap_13_2 <= out_back_io_deq_bits_data[2];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_13_3 <= 1'h1;
    end else if (out_f_woready_7) begin
      wayMap_13_3 <= out_back_io_deq_bits_data[3];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_13_4 <= 1'h1;
    end else if (out_f_woready_8) begin
      wayMap_13_4 <= out_back_io_deq_bits_data[4];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_13_5 <= 1'h1;
    end else if (out_f_woready_9) begin
      wayMap_13_5 <= out_back_io_deq_bits_data[5];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_13_6 <= 1'h1;
    end else if (out_f_woready_10) begin
      wayMap_13_6 <= out_back_io_deq_bits_data[6];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      wayMap_13_7 <= 1'h1;
    end else if (out_f_woready_11) begin
      wayMap_13_7 <= out_back_io_deq_bits_data[7];
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      waysReg_0 <= 9'h0;
    end else if (_ways_T_1 == 9'h0) begin
      waysReg_0 <= 9'h1;
    end else begin
      waysReg_0 <= _ways_T_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      waysReg_1 <= 9'h0;
    end else if (_ways_T_5 == 9'h0) begin
      waysReg_1 <= 9'h1;
    end else begin
      waysReg_1 <= _ways_T_5;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      waysReg_2 <= 9'h0;
    end else if (_ways_T_9 == 9'h0) begin
      waysReg_2 <= 9'h1;
    end else begin
      waysReg_2 <= _ways_T_9;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      waysReg_3 <= 9'h0;
    end else if (_ways_T_13 == 9'h0) begin
      waysReg_3 <= 9'h1;
    end else begin
      waysReg_3 <= _ways_T_13;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      waysReg_4 <= 9'h0;
    end else if (_ways_T_17 == 9'h0) begin
      waysReg_4 <= 9'h1;
    end else begin
      waysReg_4 <= _ways_T_17;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      waysReg_5 <= 9'h0;
    end else if (_ways_T_21 == 9'h0) begin
      waysReg_5 <= 9'h1;
    end else begin
      waysReg_5 <= _ways_T_21;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      waysReg_6 <= 9'h0;
    end else if (_ways_T_25 == 9'h0) begin
      waysReg_6 <= 9'h1;
    end else begin
      waysReg_6 <= _ways_T_25;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      waysReg_7 <= 9'h0;
    end else if (_ways_T_29 == 9'h0) begin
      waysReg_7 <= 9'h1;
    end else begin
      waysReg_7 <= _ways_T_29;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      waysReg_8 <= 9'h0;
    end else if (_ways_T_33 == 9'h0) begin
      waysReg_8 <= 9'h1;
    end else begin
      waysReg_8 <= _ways_T_33;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      waysReg_9 <= 9'h0;
    end else if (_ways_T_37 == 9'h0) begin
      waysReg_9 <= 9'h1;
    end else begin
      waysReg_9 <= _ways_T_37;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      waysReg_10 <= 9'h0;
    end else if (_ways_T_41 == 9'h0) begin
      waysReg_10 <= 9'h1;
    end else begin
      waysReg_10 <= _ways_T_41;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      waysReg_11 <= 9'h0;
    end else if (_ways_T_45 == 9'h0) begin
      waysReg_11 <= 9'h1;
    end else begin
      waysReg_11 <= _ways_T_45;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      waysReg_12 <= 9'h0;
    end else if (_ways_T_49 == 9'h0) begin
      waysReg_12 <= 9'h1;
    end else begin
      waysReg_12 <= _ways_T_49;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      waysReg_13 <= 9'h0;
    end else if (_ways_T_53 == 9'h0) begin
      waysReg_13 <= 9'h1;
    end else begin
      waysReg_13 <= _ways_T_53;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      divisors_0 <= 11'h0;
    end else if (3'h7 == _divisors_T_26[2:0]) begin
      divisors_0 <= 11'h80;
    end else if (3'h6 == _divisors_T_26[2:0]) begin
      divisors_0 <= 11'h93;
    end else if (3'h5 == _divisors_T_26[2:0]) begin
      divisors_0 <= 11'hab;
    end else if (3'h4 == _divisors_T_26[2:0]) begin
      divisors_0 <= 11'hcd;
    end else begin
      divisors_0 <= _GEN_3;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      divisors_1 <= 11'h0;
    end else if (3'h7 == _divisors_T_54[2:0]) begin
      divisors_1 <= 11'h80;
    end else if (3'h6 == _divisors_T_54[2:0]) begin
      divisors_1 <= 11'h93;
    end else if (3'h5 == _divisors_T_54[2:0]) begin
      divisors_1 <= 11'hab;
    end else if (3'h4 == _divisors_T_54[2:0]) begin
      divisors_1 <= 11'hcd;
    end else begin
      divisors_1 <= _GEN_11;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      divisors_2 <= 11'h0;
    end else if (3'h7 == _divisors_T_82[2:0]) begin
      divisors_2 <= 11'h80;
    end else if (3'h6 == _divisors_T_82[2:0]) begin
      divisors_2 <= 11'h93;
    end else if (3'h5 == _divisors_T_82[2:0]) begin
      divisors_2 <= 11'hab;
    end else if (3'h4 == _divisors_T_82[2:0]) begin
      divisors_2 <= 11'hcd;
    end else begin
      divisors_2 <= _GEN_19;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      divisors_3 <= 11'h0;
    end else if (3'h7 == _divisors_T_110[2:0]) begin
      divisors_3 <= 11'h80;
    end else if (3'h6 == _divisors_T_110[2:0]) begin
      divisors_3 <= 11'h93;
    end else if (3'h5 == _divisors_T_110[2:0]) begin
      divisors_3 <= 11'hab;
    end else if (3'h4 == _divisors_T_110[2:0]) begin
      divisors_3 <= 11'hcd;
    end else begin
      divisors_3 <= _GEN_27;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      divisors_4 <= 11'h0;
    end else if (3'h7 == _divisors_T_138[2:0]) begin
      divisors_4 <= 11'h80;
    end else if (3'h6 == _divisors_T_138[2:0]) begin
      divisors_4 <= 11'h93;
    end else if (3'h5 == _divisors_T_138[2:0]) begin
      divisors_4 <= 11'hab;
    end else if (3'h4 == _divisors_T_138[2:0]) begin
      divisors_4 <= 11'hcd;
    end else begin
      divisors_4 <= _GEN_35;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      divisors_5 <= 11'h0;
    end else if (3'h7 == _divisors_T_166[2:0]) begin
      divisors_5 <= 11'h80;
    end else if (3'h6 == _divisors_T_166[2:0]) begin
      divisors_5 <= 11'h93;
    end else if (3'h5 == _divisors_T_166[2:0]) begin
      divisors_5 <= 11'hab;
    end else if (3'h4 == _divisors_T_166[2:0]) begin
      divisors_5 <= 11'hcd;
    end else begin
      divisors_5 <= _GEN_43;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      divisors_6 <= 11'h0;
    end else if (3'h7 == _divisors_T_194[2:0]) begin
      divisors_6 <= 11'h80;
    end else if (3'h6 == _divisors_T_194[2:0]) begin
      divisors_6 <= 11'h93;
    end else if (3'h5 == _divisors_T_194[2:0]) begin
      divisors_6 <= 11'hab;
    end else if (3'h4 == _divisors_T_194[2:0]) begin
      divisors_6 <= 11'hcd;
    end else begin
      divisors_6 <= _GEN_51;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      divisors_7 <= 11'h0;
    end else if (3'h7 == _divisors_T_222[2:0]) begin
      divisors_7 <= 11'h80;
    end else if (3'h6 == _divisors_T_222[2:0]) begin
      divisors_7 <= 11'h93;
    end else if (3'h5 == _divisors_T_222[2:0]) begin
      divisors_7 <= 11'hab;
    end else if (3'h4 == _divisors_T_222[2:0]) begin
      divisors_7 <= 11'hcd;
    end else begin
      divisors_7 <= _GEN_59;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      divisors_8 <= 11'h0;
    end else if (3'h7 == _divisors_T_250[2:0]) begin
      divisors_8 <= 11'h80;
    end else if (3'h6 == _divisors_T_250[2:0]) begin
      divisors_8 <= 11'h93;
    end else if (3'h5 == _divisors_T_250[2:0]) begin
      divisors_8 <= 11'hab;
    end else if (3'h4 == _divisors_T_250[2:0]) begin
      divisors_8 <= 11'hcd;
    end else begin
      divisors_8 <= _GEN_67;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      divisors_9 <= 11'h0;
    end else if (3'h7 == _divisors_T_278[2:0]) begin
      divisors_9 <= 11'h80;
    end else if (3'h6 == _divisors_T_278[2:0]) begin
      divisors_9 <= 11'h93;
    end else if (3'h5 == _divisors_T_278[2:0]) begin
      divisors_9 <= 11'hab;
    end else if (3'h4 == _divisors_T_278[2:0]) begin
      divisors_9 <= 11'hcd;
    end else begin
      divisors_9 <= _GEN_75;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      divisors_10 <= 11'h0;
    end else if (3'h7 == _divisors_T_306[2:0]) begin
      divisors_10 <= 11'h80;
    end else if (3'h6 == _divisors_T_306[2:0]) begin
      divisors_10 <= 11'h93;
    end else if (3'h5 == _divisors_T_306[2:0]) begin
      divisors_10 <= 11'hab;
    end else if (3'h4 == _divisors_T_306[2:0]) begin
      divisors_10 <= 11'hcd;
    end else begin
      divisors_10 <= _GEN_83;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      divisors_11 <= 11'h0;
    end else if (3'h7 == _divisors_T_334[2:0]) begin
      divisors_11 <= 11'h80;
    end else if (3'h6 == _divisors_T_334[2:0]) begin
      divisors_11 <= 11'h93;
    end else if (3'h5 == _divisors_T_334[2:0]) begin
      divisors_11 <= 11'hab;
    end else if (3'h4 == _divisors_T_334[2:0]) begin
      divisors_11 <= 11'hcd;
    end else begin
      divisors_11 <= _GEN_91;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      divisors_12 <= 11'h0;
    end else if (3'h7 == _divisors_T_362[2:0]) begin
      divisors_12 <= 11'h80;
    end else if (3'h6 == _divisors_T_362[2:0]) begin
      divisors_12 <= 11'h93;
    end else if (3'h5 == _divisors_T_362[2:0]) begin
      divisors_12 <= 11'hab;
    end else if (3'h4 == _divisors_T_362[2:0]) begin
      divisors_12 <= 11'hcd;
    end else begin
      divisors_12 <= _GEN_99;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      divisors_13 <= 11'h0;
    end else if (3'h7 == _divisors_T_390[2:0]) begin
      divisors_13 <= 11'h80;
    end else if (3'h6 == _divisors_T_390[2:0]) begin
      divisors_13 <= 11'h93;
    end else if (3'h5 == _divisors_T_390[2:0]) begin
      divisors_13 <= 11'hab;
    end else if (3'h4 == _divisors_T_390[2:0]) begin
      divisors_13 <= 11'hcd;
    end else begin
      divisors_13 <= _GEN_107;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      dirNumberCorrected <= 32'h0;
    end else begin
      dirNumberCorrected <= _dirNumberCorrected_T_6[31:0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      dirNumberUncorrected <= 32'h0;
    end else begin
      dirNumberUncorrected <= _dirNumberUncorrected_T_6[31:0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      datNumberCorrected <= 32'h0;
    end else begin
      datNumberCorrected <= _datNumberCorrected_T_16[31:0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      datNumberUncorrected <= 32'h0;
    end else begin
      datNumberUncorrected <= datNumberUncorrected + _GEN_1094;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      datAddressUncorrected <= 64'h0;
    end else if (sideband_io_uncorrected_valid) begin
      datAddressUncorrected <= {{28'd0}, sideband_io_uncorrected_bits};
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flushInValid <= 1'h0;
    end else begin
      flushInValid <= out_f_wivalid_124 | _GEN_187;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      flushInAddress <= 64'h0;
    end else if (_out_T_2210) begin
      flushInAddress <= {{28'd0}, _out_flushInAddress_T};
    end else if (_out_T_1633) begin
      flushInAddress <= auto_in_4_a_bits_data;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      flushOutValid <= 1'h0;
    end else begin
      flushOutValid <= mods_3_io_resp_valid | (mods_2_io_resp_valid | (mods_1_io_resp_valid | (mods_0_io_resp_valid |
        _GEN_115)));
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      injectValid <= 1'h0;
    end else begin
      injectValid <= out_f_woready_137 | _GEN_116;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      injectBit <= 8'h0;
    end else if (out_f_woready_137) begin
      injectBit <= out_back_io_deq_bits_data[7:0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      injectType <= 1'h0;
    end else if (out_f_woready_139) begin
      injectType <= out_back_io_deq_bits_data[16];
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  wayEnable = _RAND_0[2:0];
  _RAND_1 = {1{`RANDOM}};
  wayMap_0_0 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  wayMap_0_1 = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  wayMap_0_2 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  wayMap_0_3 = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  wayMap_0_4 = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  wayMap_0_5 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  wayMap_0_6 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  wayMap_0_7 = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  wayMap_1_0 = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  wayMap_1_1 = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  wayMap_1_2 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  wayMap_1_3 = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  wayMap_1_4 = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  wayMap_1_5 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  wayMap_1_6 = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  wayMap_1_7 = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  wayMap_2_0 = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  wayMap_2_1 = _RAND_18[0:0];
  _RAND_19 = {1{`RANDOM}};
  wayMap_2_2 = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  wayMap_2_3 = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  wayMap_2_4 = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  wayMap_2_5 = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  wayMap_2_6 = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  wayMap_2_7 = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  wayMap_3_0 = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  wayMap_3_1 = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  wayMap_3_2 = _RAND_27[0:0];
  _RAND_28 = {1{`RANDOM}};
  wayMap_3_3 = _RAND_28[0:0];
  _RAND_29 = {1{`RANDOM}};
  wayMap_3_4 = _RAND_29[0:0];
  _RAND_30 = {1{`RANDOM}};
  wayMap_3_5 = _RAND_30[0:0];
  _RAND_31 = {1{`RANDOM}};
  wayMap_3_6 = _RAND_31[0:0];
  _RAND_32 = {1{`RANDOM}};
  wayMap_3_7 = _RAND_32[0:0];
  _RAND_33 = {1{`RANDOM}};
  wayMap_4_0 = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  wayMap_4_1 = _RAND_34[0:0];
  _RAND_35 = {1{`RANDOM}};
  wayMap_4_2 = _RAND_35[0:0];
  _RAND_36 = {1{`RANDOM}};
  wayMap_4_3 = _RAND_36[0:0];
  _RAND_37 = {1{`RANDOM}};
  wayMap_4_4 = _RAND_37[0:0];
  _RAND_38 = {1{`RANDOM}};
  wayMap_4_5 = _RAND_38[0:0];
  _RAND_39 = {1{`RANDOM}};
  wayMap_4_6 = _RAND_39[0:0];
  _RAND_40 = {1{`RANDOM}};
  wayMap_4_7 = _RAND_40[0:0];
  _RAND_41 = {1{`RANDOM}};
  wayMap_5_0 = _RAND_41[0:0];
  _RAND_42 = {1{`RANDOM}};
  wayMap_5_1 = _RAND_42[0:0];
  _RAND_43 = {1{`RANDOM}};
  wayMap_5_2 = _RAND_43[0:0];
  _RAND_44 = {1{`RANDOM}};
  wayMap_5_3 = _RAND_44[0:0];
  _RAND_45 = {1{`RANDOM}};
  wayMap_5_4 = _RAND_45[0:0];
  _RAND_46 = {1{`RANDOM}};
  wayMap_5_5 = _RAND_46[0:0];
  _RAND_47 = {1{`RANDOM}};
  wayMap_5_6 = _RAND_47[0:0];
  _RAND_48 = {1{`RANDOM}};
  wayMap_5_7 = _RAND_48[0:0];
  _RAND_49 = {1{`RANDOM}};
  wayMap_6_0 = _RAND_49[0:0];
  _RAND_50 = {1{`RANDOM}};
  wayMap_6_1 = _RAND_50[0:0];
  _RAND_51 = {1{`RANDOM}};
  wayMap_6_2 = _RAND_51[0:0];
  _RAND_52 = {1{`RANDOM}};
  wayMap_6_3 = _RAND_52[0:0];
  _RAND_53 = {1{`RANDOM}};
  wayMap_6_4 = _RAND_53[0:0];
  _RAND_54 = {1{`RANDOM}};
  wayMap_6_5 = _RAND_54[0:0];
  _RAND_55 = {1{`RANDOM}};
  wayMap_6_6 = _RAND_55[0:0];
  _RAND_56 = {1{`RANDOM}};
  wayMap_6_7 = _RAND_56[0:0];
  _RAND_57 = {1{`RANDOM}};
  wayMap_7_0 = _RAND_57[0:0];
  _RAND_58 = {1{`RANDOM}};
  wayMap_7_1 = _RAND_58[0:0];
  _RAND_59 = {1{`RANDOM}};
  wayMap_7_2 = _RAND_59[0:0];
  _RAND_60 = {1{`RANDOM}};
  wayMap_7_3 = _RAND_60[0:0];
  _RAND_61 = {1{`RANDOM}};
  wayMap_7_4 = _RAND_61[0:0];
  _RAND_62 = {1{`RANDOM}};
  wayMap_7_5 = _RAND_62[0:0];
  _RAND_63 = {1{`RANDOM}};
  wayMap_7_6 = _RAND_63[0:0];
  _RAND_64 = {1{`RANDOM}};
  wayMap_7_7 = _RAND_64[0:0];
  _RAND_65 = {1{`RANDOM}};
  wayMap_8_0 = _RAND_65[0:0];
  _RAND_66 = {1{`RANDOM}};
  wayMap_8_1 = _RAND_66[0:0];
  _RAND_67 = {1{`RANDOM}};
  wayMap_8_2 = _RAND_67[0:0];
  _RAND_68 = {1{`RANDOM}};
  wayMap_8_3 = _RAND_68[0:0];
  _RAND_69 = {1{`RANDOM}};
  wayMap_8_4 = _RAND_69[0:0];
  _RAND_70 = {1{`RANDOM}};
  wayMap_8_5 = _RAND_70[0:0];
  _RAND_71 = {1{`RANDOM}};
  wayMap_8_6 = _RAND_71[0:0];
  _RAND_72 = {1{`RANDOM}};
  wayMap_8_7 = _RAND_72[0:0];
  _RAND_73 = {1{`RANDOM}};
  wayMap_9_0 = _RAND_73[0:0];
  _RAND_74 = {1{`RANDOM}};
  wayMap_9_1 = _RAND_74[0:0];
  _RAND_75 = {1{`RANDOM}};
  wayMap_9_2 = _RAND_75[0:0];
  _RAND_76 = {1{`RANDOM}};
  wayMap_9_3 = _RAND_76[0:0];
  _RAND_77 = {1{`RANDOM}};
  wayMap_9_4 = _RAND_77[0:0];
  _RAND_78 = {1{`RANDOM}};
  wayMap_9_5 = _RAND_78[0:0];
  _RAND_79 = {1{`RANDOM}};
  wayMap_9_6 = _RAND_79[0:0];
  _RAND_80 = {1{`RANDOM}};
  wayMap_9_7 = _RAND_80[0:0];
  _RAND_81 = {1{`RANDOM}};
  wayMap_10_0 = _RAND_81[0:0];
  _RAND_82 = {1{`RANDOM}};
  wayMap_10_1 = _RAND_82[0:0];
  _RAND_83 = {1{`RANDOM}};
  wayMap_10_2 = _RAND_83[0:0];
  _RAND_84 = {1{`RANDOM}};
  wayMap_10_3 = _RAND_84[0:0];
  _RAND_85 = {1{`RANDOM}};
  wayMap_10_4 = _RAND_85[0:0];
  _RAND_86 = {1{`RANDOM}};
  wayMap_10_5 = _RAND_86[0:0];
  _RAND_87 = {1{`RANDOM}};
  wayMap_10_6 = _RAND_87[0:0];
  _RAND_88 = {1{`RANDOM}};
  wayMap_10_7 = _RAND_88[0:0];
  _RAND_89 = {1{`RANDOM}};
  wayMap_11_0 = _RAND_89[0:0];
  _RAND_90 = {1{`RANDOM}};
  wayMap_11_1 = _RAND_90[0:0];
  _RAND_91 = {1{`RANDOM}};
  wayMap_11_2 = _RAND_91[0:0];
  _RAND_92 = {1{`RANDOM}};
  wayMap_11_3 = _RAND_92[0:0];
  _RAND_93 = {1{`RANDOM}};
  wayMap_11_4 = _RAND_93[0:0];
  _RAND_94 = {1{`RANDOM}};
  wayMap_11_5 = _RAND_94[0:0];
  _RAND_95 = {1{`RANDOM}};
  wayMap_11_6 = _RAND_95[0:0];
  _RAND_96 = {1{`RANDOM}};
  wayMap_11_7 = _RAND_96[0:0];
  _RAND_97 = {1{`RANDOM}};
  wayMap_12_0 = _RAND_97[0:0];
  _RAND_98 = {1{`RANDOM}};
  wayMap_12_1 = _RAND_98[0:0];
  _RAND_99 = {1{`RANDOM}};
  wayMap_12_2 = _RAND_99[0:0];
  _RAND_100 = {1{`RANDOM}};
  wayMap_12_3 = _RAND_100[0:0];
  _RAND_101 = {1{`RANDOM}};
  wayMap_12_4 = _RAND_101[0:0];
  _RAND_102 = {1{`RANDOM}};
  wayMap_12_5 = _RAND_102[0:0];
  _RAND_103 = {1{`RANDOM}};
  wayMap_12_6 = _RAND_103[0:0];
  _RAND_104 = {1{`RANDOM}};
  wayMap_12_7 = _RAND_104[0:0];
  _RAND_105 = {1{`RANDOM}};
  wayMap_13_0 = _RAND_105[0:0];
  _RAND_106 = {1{`RANDOM}};
  wayMap_13_1 = _RAND_106[0:0];
  _RAND_107 = {1{`RANDOM}};
  wayMap_13_2 = _RAND_107[0:0];
  _RAND_108 = {1{`RANDOM}};
  wayMap_13_3 = _RAND_108[0:0];
  _RAND_109 = {1{`RANDOM}};
  wayMap_13_4 = _RAND_109[0:0];
  _RAND_110 = {1{`RANDOM}};
  wayMap_13_5 = _RAND_110[0:0];
  _RAND_111 = {1{`RANDOM}};
  wayMap_13_6 = _RAND_111[0:0];
  _RAND_112 = {1{`RANDOM}};
  wayMap_13_7 = _RAND_112[0:0];
  _RAND_113 = {1{`RANDOM}};
  waysReg_0 = _RAND_113[8:0];
  _RAND_114 = {1{`RANDOM}};
  waysReg_1 = _RAND_114[8:0];
  _RAND_115 = {1{`RANDOM}};
  waysReg_2 = _RAND_115[8:0];
  _RAND_116 = {1{`RANDOM}};
  waysReg_3 = _RAND_116[8:0];
  _RAND_117 = {1{`RANDOM}};
  waysReg_4 = _RAND_117[8:0];
  _RAND_118 = {1{`RANDOM}};
  waysReg_5 = _RAND_118[8:0];
  _RAND_119 = {1{`RANDOM}};
  waysReg_6 = _RAND_119[8:0];
  _RAND_120 = {1{`RANDOM}};
  waysReg_7 = _RAND_120[8:0];
  _RAND_121 = {1{`RANDOM}};
  waysReg_8 = _RAND_121[8:0];
  _RAND_122 = {1{`RANDOM}};
  waysReg_9 = _RAND_122[8:0];
  _RAND_123 = {1{`RANDOM}};
  waysReg_10 = _RAND_123[8:0];
  _RAND_124 = {1{`RANDOM}};
  waysReg_11 = _RAND_124[8:0];
  _RAND_125 = {1{`RANDOM}};
  waysReg_12 = _RAND_125[8:0];
  _RAND_126 = {1{`RANDOM}};
  waysReg_13 = _RAND_126[8:0];
  _RAND_127 = {1{`RANDOM}};
  divisors_0 = _RAND_127[10:0];
  _RAND_128 = {1{`RANDOM}};
  divisors_1 = _RAND_128[10:0];
  _RAND_129 = {1{`RANDOM}};
  divisors_2 = _RAND_129[10:0];
  _RAND_130 = {1{`RANDOM}};
  divisors_3 = _RAND_130[10:0];
  _RAND_131 = {1{`RANDOM}};
  divisors_4 = _RAND_131[10:0];
  _RAND_132 = {1{`RANDOM}};
  divisors_5 = _RAND_132[10:0];
  _RAND_133 = {1{`RANDOM}};
  divisors_6 = _RAND_133[10:0];
  _RAND_134 = {1{`RANDOM}};
  divisors_7 = _RAND_134[10:0];
  _RAND_135 = {1{`RANDOM}};
  divisors_8 = _RAND_135[10:0];
  _RAND_136 = {1{`RANDOM}};
  divisors_9 = _RAND_136[10:0];
  _RAND_137 = {1{`RANDOM}};
  divisors_10 = _RAND_137[10:0];
  _RAND_138 = {1{`RANDOM}};
  divisors_11 = _RAND_138[10:0];
  _RAND_139 = {1{`RANDOM}};
  divisors_12 = _RAND_139[10:0];
  _RAND_140 = {1{`RANDOM}};
  divisors_13 = _RAND_140[10:0];
  _RAND_141 = {1{`RANDOM}};
  dirNumberCorrected = _RAND_141[31:0];
  _RAND_142 = {1{`RANDOM}};
  dirNumberUncorrected = _RAND_142[31:0];
  _RAND_143 = {1{`RANDOM}};
  datNumberCorrected = _RAND_143[31:0];
  _RAND_144 = {1{`RANDOM}};
  datNumberUncorrected = _RAND_144[31:0];
  _RAND_145 = {2{`RANDOM}};
  datAddressUncorrected = _RAND_145[63:0];
  _RAND_146 = {1{`RANDOM}};
  flushInValid = _RAND_146[0:0];
  _RAND_147 = {2{`RANDOM}};
  flushInAddress = _RAND_147[63:0];
  _RAND_148 = {1{`RANDOM}};
  flushOutValid = _RAND_148[0:0];
  _RAND_149 = {1{`RANDOM}};
  injectValid = _RAND_149[0:0];
  _RAND_150 = {1{`RANDOM}};
  injectBit = _RAND_150[7:0];
  _RAND_151 = {1{`RANDOM}};
  injectType = _RAND_151[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    wayEnable = 3'h0;
  end
  if (reset) begin
    wayMap_0_0 = 1'h1;
  end
  if (reset) begin
    wayMap_0_1 = 1'h1;
  end
  if (reset) begin
    wayMap_0_2 = 1'h1;
  end
  if (reset) begin
    wayMap_0_3 = 1'h1;
  end
  if (reset) begin
    wayMap_0_4 = 1'h1;
  end
  if (reset) begin
    wayMap_0_5 = 1'h1;
  end
  if (reset) begin
    wayMap_0_6 = 1'h1;
  end
  if (reset) begin
    wayMap_0_7 = 1'h1;
  end
  if (reset) begin
    wayMap_1_0 = 1'h1;
  end
  if (reset) begin
    wayMap_1_1 = 1'h1;
  end
  if (reset) begin
    wayMap_1_2 = 1'h1;
  end
  if (reset) begin
    wayMap_1_3 = 1'h1;
  end
  if (reset) begin
    wayMap_1_4 = 1'h1;
  end
  if (reset) begin
    wayMap_1_5 = 1'h1;
  end
  if (reset) begin
    wayMap_1_6 = 1'h1;
  end
  if (reset) begin
    wayMap_1_7 = 1'h1;
  end
  if (reset) begin
    wayMap_2_0 = 1'h1;
  end
  if (reset) begin
    wayMap_2_1 = 1'h1;
  end
  if (reset) begin
    wayMap_2_2 = 1'h1;
  end
  if (reset) begin
    wayMap_2_3 = 1'h1;
  end
  if (reset) begin
    wayMap_2_4 = 1'h1;
  end
  if (reset) begin
    wayMap_2_5 = 1'h1;
  end
  if (reset) begin
    wayMap_2_6 = 1'h1;
  end
  if (reset) begin
    wayMap_2_7 = 1'h1;
  end
  if (reset) begin
    wayMap_3_0 = 1'h1;
  end
  if (reset) begin
    wayMap_3_1 = 1'h1;
  end
  if (reset) begin
    wayMap_3_2 = 1'h1;
  end
  if (reset) begin
    wayMap_3_3 = 1'h1;
  end
  if (reset) begin
    wayMap_3_4 = 1'h1;
  end
  if (reset) begin
    wayMap_3_5 = 1'h1;
  end
  if (reset) begin
    wayMap_3_6 = 1'h1;
  end
  if (reset) begin
    wayMap_3_7 = 1'h1;
  end
  if (reset) begin
    wayMap_4_0 = 1'h1;
  end
  if (reset) begin
    wayMap_4_1 = 1'h1;
  end
  if (reset) begin
    wayMap_4_2 = 1'h1;
  end
  if (reset) begin
    wayMap_4_3 = 1'h1;
  end
  if (reset) begin
    wayMap_4_4 = 1'h1;
  end
  if (reset) begin
    wayMap_4_5 = 1'h1;
  end
  if (reset) begin
    wayMap_4_6 = 1'h1;
  end
  if (reset) begin
    wayMap_4_7 = 1'h1;
  end
  if (reset) begin
    wayMap_5_0 = 1'h1;
  end
  if (reset) begin
    wayMap_5_1 = 1'h1;
  end
  if (reset) begin
    wayMap_5_2 = 1'h1;
  end
  if (reset) begin
    wayMap_5_3 = 1'h1;
  end
  if (reset) begin
    wayMap_5_4 = 1'h1;
  end
  if (reset) begin
    wayMap_5_5 = 1'h1;
  end
  if (reset) begin
    wayMap_5_6 = 1'h1;
  end
  if (reset) begin
    wayMap_5_7 = 1'h1;
  end
  if (reset) begin
    wayMap_6_0 = 1'h1;
  end
  if (reset) begin
    wayMap_6_1 = 1'h1;
  end
  if (reset) begin
    wayMap_6_2 = 1'h1;
  end
  if (reset) begin
    wayMap_6_3 = 1'h1;
  end
  if (reset) begin
    wayMap_6_4 = 1'h1;
  end
  if (reset) begin
    wayMap_6_5 = 1'h1;
  end
  if (reset) begin
    wayMap_6_6 = 1'h1;
  end
  if (reset) begin
    wayMap_6_7 = 1'h1;
  end
  if (reset) begin
    wayMap_7_0 = 1'h1;
  end
  if (reset) begin
    wayMap_7_1 = 1'h1;
  end
  if (reset) begin
    wayMap_7_2 = 1'h1;
  end
  if (reset) begin
    wayMap_7_3 = 1'h1;
  end
  if (reset) begin
    wayMap_7_4 = 1'h1;
  end
  if (reset) begin
    wayMap_7_5 = 1'h1;
  end
  if (reset) begin
    wayMap_7_6 = 1'h1;
  end
  if (reset) begin
    wayMap_7_7 = 1'h1;
  end
  if (reset) begin
    wayMap_8_0 = 1'h1;
  end
  if (reset) begin
    wayMap_8_1 = 1'h1;
  end
  if (reset) begin
    wayMap_8_2 = 1'h1;
  end
  if (reset) begin
    wayMap_8_3 = 1'h1;
  end
  if (reset) begin
    wayMap_8_4 = 1'h1;
  end
  if (reset) begin
    wayMap_8_5 = 1'h1;
  end
  if (reset) begin
    wayMap_8_6 = 1'h1;
  end
  if (reset) begin
    wayMap_8_7 = 1'h1;
  end
  if (reset) begin
    wayMap_9_0 = 1'h1;
  end
  if (reset) begin
    wayMap_9_1 = 1'h1;
  end
  if (reset) begin
    wayMap_9_2 = 1'h1;
  end
  if (reset) begin
    wayMap_9_3 = 1'h1;
  end
  if (reset) begin
    wayMap_9_4 = 1'h1;
  end
  if (reset) begin
    wayMap_9_5 = 1'h1;
  end
  if (reset) begin
    wayMap_9_6 = 1'h1;
  end
  if (reset) begin
    wayMap_9_7 = 1'h1;
  end
  if (reset) begin
    wayMap_10_0 = 1'h1;
  end
  if (reset) begin
    wayMap_10_1 = 1'h1;
  end
  if (reset) begin
    wayMap_10_2 = 1'h1;
  end
  if (reset) begin
    wayMap_10_3 = 1'h1;
  end
  if (reset) begin
    wayMap_10_4 = 1'h1;
  end
  if (reset) begin
    wayMap_10_5 = 1'h1;
  end
  if (reset) begin
    wayMap_10_6 = 1'h1;
  end
  if (reset) begin
    wayMap_10_7 = 1'h1;
  end
  if (reset) begin
    wayMap_11_0 = 1'h1;
  end
  if (reset) begin
    wayMap_11_1 = 1'h1;
  end
  if (reset) begin
    wayMap_11_2 = 1'h1;
  end
  if (reset) begin
    wayMap_11_3 = 1'h1;
  end
  if (reset) begin
    wayMap_11_4 = 1'h1;
  end
  if (reset) begin
    wayMap_11_5 = 1'h1;
  end
  if (reset) begin
    wayMap_11_6 = 1'h1;
  end
  if (reset) begin
    wayMap_11_7 = 1'h1;
  end
  if (reset) begin
    wayMap_12_0 = 1'h1;
  end
  if (reset) begin
    wayMap_12_1 = 1'h1;
  end
  if (reset) begin
    wayMap_12_2 = 1'h1;
  end
  if (reset) begin
    wayMap_12_3 = 1'h1;
  end
  if (reset) begin
    wayMap_12_4 = 1'h1;
  end
  if (reset) begin
    wayMap_12_5 = 1'h1;
  end
  if (reset) begin
    wayMap_12_6 = 1'h1;
  end
  if (reset) begin
    wayMap_12_7 = 1'h1;
  end
  if (reset) begin
    wayMap_13_0 = 1'h1;
  end
  if (reset) begin
    wayMap_13_1 = 1'h1;
  end
  if (reset) begin
    wayMap_13_2 = 1'h1;
  end
  if (reset) begin
    wayMap_13_3 = 1'h1;
  end
  if (reset) begin
    wayMap_13_4 = 1'h1;
  end
  if (reset) begin
    wayMap_13_5 = 1'h1;
  end
  if (reset) begin
    wayMap_13_6 = 1'h1;
  end
  if (reset) begin
    wayMap_13_7 = 1'h1;
  end
  if (rf_reset) begin
    waysReg_0 = 9'h0;
  end
  if (rf_reset) begin
    waysReg_1 = 9'h0;
  end
  if (rf_reset) begin
    waysReg_2 = 9'h0;
  end
  if (rf_reset) begin
    waysReg_3 = 9'h0;
  end
  if (rf_reset) begin
    waysReg_4 = 9'h0;
  end
  if (rf_reset) begin
    waysReg_5 = 9'h0;
  end
  if (rf_reset) begin
    waysReg_6 = 9'h0;
  end
  if (rf_reset) begin
    waysReg_7 = 9'h0;
  end
  if (rf_reset) begin
    waysReg_8 = 9'h0;
  end
  if (rf_reset) begin
    waysReg_9 = 9'h0;
  end
  if (rf_reset) begin
    waysReg_10 = 9'h0;
  end
  if (rf_reset) begin
    waysReg_11 = 9'h0;
  end
  if (rf_reset) begin
    waysReg_12 = 9'h0;
  end
  if (rf_reset) begin
    waysReg_13 = 9'h0;
  end
  if (rf_reset) begin
    divisors_0 = 11'h0;
  end
  if (rf_reset) begin
    divisors_1 = 11'h0;
  end
  if (rf_reset) begin
    divisors_2 = 11'h0;
  end
  if (rf_reset) begin
    divisors_3 = 11'h0;
  end
  if (rf_reset) begin
    divisors_4 = 11'h0;
  end
  if (rf_reset) begin
    divisors_5 = 11'h0;
  end
  if (rf_reset) begin
    divisors_6 = 11'h0;
  end
  if (rf_reset) begin
    divisors_7 = 11'h0;
  end
  if (rf_reset) begin
    divisors_8 = 11'h0;
  end
  if (rf_reset) begin
    divisors_9 = 11'h0;
  end
  if (rf_reset) begin
    divisors_10 = 11'h0;
  end
  if (rf_reset) begin
    divisors_11 = 11'h0;
  end
  if (rf_reset) begin
    divisors_12 = 11'h0;
  end
  if (rf_reset) begin
    divisors_13 = 11'h0;
  end
  if (reset) begin
    dirNumberCorrected = 32'h0;
  end
  if (reset) begin
    dirNumberUncorrected = 32'h0;
  end
  if (reset) begin
    datNumberCorrected = 32'h0;
  end
  if (reset) begin
    datNumberUncorrected = 32'h0;
  end
  if (rf_reset) begin
    datAddressUncorrected = 64'h0;
  end
  if (reset) begin
    flushInValid = 1'h0;
  end
  if (rf_reset) begin
    flushInAddress = 64'h0;
  end
  if (reset) begin
    flushOutValid = 1'h0;
  end
  if (reset) begin
    injectValid = 1'h0;
  end
  if (reset) begin
    injectBit = 8'h0;
  end
  if (reset) begin
    injectType = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
