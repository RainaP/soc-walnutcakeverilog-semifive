//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__QueueCompatibility_9(
  input        rf_reset,
  input        clock,
  input        reset,
  output       io_enq_ready,
  input        io_enq_valid,
  input  [3:0] io_enq_bits_tl_state_size,
  input  [6:0] io_enq_bits_tl_state_source,
  input        io_deq_ready,
  output       io_deq_valid,
  output [3:0] io_deq_bits_tl_state_size,
  output [6:0] io_deq_bits_tl_state_source
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
`endif // RANDOMIZE_REG_INIT
  reg [3:0] ram_0_tl_state_size; // @[Decoupled.scala 218:16]
  reg [6:0] ram_0_tl_state_source; // @[Decoupled.scala 218:16]
  reg [3:0] ram_1_tl_state_size; // @[Decoupled.scala 218:16]
  reg [6:0] ram_1_tl_state_source; // @[Decoupled.scala 218:16]
  reg [3:0] ram_2_tl_state_size; // @[Decoupled.scala 218:16]
  reg [6:0] ram_2_tl_state_source; // @[Decoupled.scala 218:16]
  reg [3:0] ram_3_tl_state_size; // @[Decoupled.scala 218:16]
  reg [6:0] ram_3_tl_state_source; // @[Decoupled.scala 218:16]
  reg [1:0] deq_ptr_value; // @[Counter.scala 60:40]
  wire [3:0] _GEN_2 = 2'h1 == deq_ptr_value ? ram_1_tl_state_size : ram_0_tl_state_size; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [6:0] _GEN_3 = 2'h1 == deq_ptr_value ? ram_1_tl_state_source : ram_0_tl_state_source; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [3:0] _GEN_4 = 2'h2 == deq_ptr_value ? ram_2_tl_state_size : _GEN_2; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire [6:0] _GEN_5 = 2'h2 == deq_ptr_value ? ram_2_tl_state_source : _GEN_3; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  wire  do_enq = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  reg [1:0] enq_ptr_value; // @[Counter.scala 60:40]
  reg  maybe_full; // @[Decoupled.scala 221:27]
  wire  ptr_match = enq_ptr_value == deq_ptr_value; // @[Decoupled.scala 223:33]
  wire  empty = ptr_match & ~maybe_full; // @[Decoupled.scala 224:25]
  wire  full = ptr_match & maybe_full; // @[Decoupled.scala 225:24]
  wire  do_deq = io_deq_ready & io_deq_valid; // @[Decoupled.scala 40:37]
  wire [1:0] _value_T_1 = enq_ptr_value + 2'h1; // @[Counter.scala 76:24]
  wire [1:0] _value_T_3 = deq_ptr_value + 2'h1; // @[Counter.scala 76:24]
  assign io_enq_ready = ~full; // @[Decoupled.scala 241:19]
  assign io_deq_valid = ~empty; // @[Decoupled.scala 240:19]
  assign io_deq_bits_tl_state_size = 2'h3 == deq_ptr_value ? ram_3_tl_state_size : _GEN_4; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  assign io_deq_bits_tl_state_source = 2'h3 == deq_ptr_value ? ram_3_tl_state_source : _GEN_5; // @[Decoupled.scala 218:16 Decoupled.scala 218:16]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_tl_state_size <= 4'h0;
    end else if (do_enq) begin
      if (2'h0 == enq_ptr_value) begin
        ram_0_tl_state_size <= io_enq_bits_tl_state_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_0_tl_state_source <= 7'h0;
    end else if (do_enq) begin
      if (2'h0 == enq_ptr_value) begin
        ram_0_tl_state_source <= io_enq_bits_tl_state_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_tl_state_size <= 4'h0;
    end else if (do_enq) begin
      if (2'h1 == enq_ptr_value) begin
        ram_1_tl_state_size <= io_enq_bits_tl_state_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_1_tl_state_source <= 7'h0;
    end else if (do_enq) begin
      if (2'h1 == enq_ptr_value) begin
        ram_1_tl_state_source <= io_enq_bits_tl_state_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_tl_state_size <= 4'h0;
    end else if (do_enq) begin
      if (2'h2 == enq_ptr_value) begin
        ram_2_tl_state_size <= io_enq_bits_tl_state_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_2_tl_state_source <= 7'h0;
    end else if (do_enq) begin
      if (2'h2 == enq_ptr_value) begin
        ram_2_tl_state_source <= io_enq_bits_tl_state_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_tl_state_size <= 4'h0;
    end else if (do_enq) begin
      if (2'h3 == enq_ptr_value) begin
        ram_3_tl_state_size <= io_enq_bits_tl_state_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      ram_3_tl_state_source <= 7'h0;
    end else if (do_enq) begin
      if (2'h3 == enq_ptr_value) begin
        ram_3_tl_state_source <= io_enq_bits_tl_state_source;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      deq_ptr_value <= 2'h0;
    end else if (do_deq) begin
      deq_ptr_value <= _value_T_3;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      enq_ptr_value <= 2'h0;
    end else if (do_enq) begin
      enq_ptr_value <= _value_T_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      maybe_full <= 1'h0;
    end else if (do_enq != do_deq) begin
      maybe_full <= do_enq;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  ram_0_tl_state_size = _RAND_0[3:0];
  _RAND_1 = {1{`RANDOM}};
  ram_0_tl_state_source = _RAND_1[6:0];
  _RAND_2 = {1{`RANDOM}};
  ram_1_tl_state_size = _RAND_2[3:0];
  _RAND_3 = {1{`RANDOM}};
  ram_1_tl_state_source = _RAND_3[6:0];
  _RAND_4 = {1{`RANDOM}};
  ram_2_tl_state_size = _RAND_4[3:0];
  _RAND_5 = {1{`RANDOM}};
  ram_2_tl_state_source = _RAND_5[6:0];
  _RAND_6 = {1{`RANDOM}};
  ram_3_tl_state_size = _RAND_6[3:0];
  _RAND_7 = {1{`RANDOM}};
  ram_3_tl_state_source = _RAND_7[6:0];
  _RAND_8 = {1{`RANDOM}};
  deq_ptr_value = _RAND_8[1:0];
  _RAND_9 = {1{`RANDOM}};
  enq_ptr_value = _RAND_9[1:0];
  _RAND_10 = {1{`RANDOM}};
  maybe_full = _RAND_10[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    ram_0_tl_state_size = 4'h0;
  end
  if (rf_reset) begin
    ram_0_tl_state_source = 7'h0;
  end
  if (rf_reset) begin
    ram_1_tl_state_size = 4'h0;
  end
  if (rf_reset) begin
    ram_1_tl_state_source = 7'h0;
  end
  if (rf_reset) begin
    ram_2_tl_state_size = 4'h0;
  end
  if (rf_reset) begin
    ram_2_tl_state_source = 7'h0;
  end
  if (rf_reset) begin
    ram_3_tl_state_size = 4'h0;
  end
  if (rf_reset) begin
    ram_3_tl_state_source = 7'h0;
  end
  if (reset) begin
    deq_ptr_value = 2'h0;
  end
  if (reset) begin
    enq_ptr_value = 2'h0;
  end
  if (reset) begin
    maybe_full = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
