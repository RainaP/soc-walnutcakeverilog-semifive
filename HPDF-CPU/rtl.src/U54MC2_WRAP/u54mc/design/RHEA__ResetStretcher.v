//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__ResetStretcher(
  input   auto_reset_stretcher_in_clock,
  input   auto_reset_stretcher_in_reset,
  output  auto_reset_stretcher_out_clock,
  output  auto_reset_stretcher_out_reset
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
`endif // RANDOMIZE_REG_INIT
  reg [3:0] bundleOut_0_reset_count; // @[ResetStretcher.scala 21:28]
  reg  bundleOut_0_reset_resetout; // @[ResetStretcher.scala 22:31]
  wire [3:0] _bundleOut_0_reset_count_T_1 = bundleOut_0_reset_count + 4'h1; // @[ResetStretcher.scala 24:26]
  assign auto_reset_stretcher_out_clock = auto_reset_stretcher_in_clock; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign auto_reset_stretcher_out_reset = bundleOut_0_reset_resetout; // @[Nodes.scala 1529:13 ResetStretcher.scala 20:17]
  always @(posedge auto_reset_stretcher_in_clock or posedge auto_reset_stretcher_in_reset) begin
    if (auto_reset_stretcher_in_reset) begin
      bundleOut_0_reset_count <= 4'h0;
    end else if (bundleOut_0_reset_resetout) begin
      bundleOut_0_reset_count <= _bundleOut_0_reset_count_T_1;
    end
  end
  always @(posedge auto_reset_stretcher_in_clock or posedge auto_reset_stretcher_in_reset) begin
    if (auto_reset_stretcher_in_reset) begin
      bundleOut_0_reset_resetout <= 1'h1;
    end else if (bundleOut_0_reset_resetout) begin
      bundleOut_0_reset_resetout <= bundleOut_0_reset_count < 4'hf;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  bundleOut_0_reset_count = _RAND_0[3:0];
  _RAND_1 = {1{`RANDOM}};
  bundleOut_0_reset_resetout = _RAND_1[0:0];
`endif // RANDOMIZE_REG_INIT
  if (auto_reset_stretcher_in_reset) begin
    bundleOut_0_reset_count = 4'h0;
  end
  if (auto_reset_stretcher_in_reset) begin
    bundleOut_0_reset_resetout = 1'h1;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
