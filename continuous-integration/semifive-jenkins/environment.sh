#!/bin/bash
unset MODULEPATH

# Source SemiFive Tools
if [[ -f /semifive/tools/Modules/default/init/zsh ]]; then
	     source /semifive/tools/Modules/default/init/zsh
     fi
     if [[ -f /semifive/tools/Modules/default/init/bash_completion ]]; then
	          bash /semifive/tools/Modules/default/init/bash_completion
	  fi

module load sifive/wit/0.14.1
module load synopsys/vcs/P-2019.06-SP2-12
module load synopsys/verdi/Q-2020.03-SP2-3
