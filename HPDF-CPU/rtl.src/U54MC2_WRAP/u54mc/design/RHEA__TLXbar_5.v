//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLXbar_5(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_a_ready,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [2:0]  auto_in_a_bits_param,
  input  [3:0]  auto_in_a_bits_size,
  input  [6:0]  auto_in_a_bits_source,
  input  [31:0] auto_in_a_bits_address,
  input         auto_in_a_bits_user_amba_prot_bufferable,
  input         auto_in_a_bits_user_amba_prot_modifiable,
  input         auto_in_a_bits_user_amba_prot_readalloc,
  input         auto_in_a_bits_user_amba_prot_writealloc,
  input         auto_in_a_bits_user_amba_prot_privileged,
  input         auto_in_a_bits_user_amba_prot_secure,
  input         auto_in_a_bits_user_amba_prot_fetch,
  input  [7:0]  auto_in_a_bits_mask,
  input  [63:0] auto_in_a_bits_data,
  input         auto_in_a_bits_corrupt,
  input         auto_in_d_ready,
  output        auto_in_d_valid,
  output [2:0]  auto_in_d_bits_opcode,
  output [1:0]  auto_in_d_bits_param,
  output [3:0]  auto_in_d_bits_size,
  output [6:0]  auto_in_d_bits_source,
  output        auto_in_d_bits_sink,
  output        auto_in_d_bits_denied,
  output [63:0] auto_in_d_bits_data,
  output        auto_in_d_bits_corrupt,
  input         auto_out_9_a_ready,
  output        auto_out_9_a_valid,
  output [2:0]  auto_out_9_a_bits_opcode,
  output [2:0]  auto_out_9_a_bits_param,
  output [2:0]  auto_out_9_a_bits_size,
  output [6:0]  auto_out_9_a_bits_source,
  output [14:0] auto_out_9_a_bits_address,
  output [7:0]  auto_out_9_a_bits_mask,
  output [63:0] auto_out_9_a_bits_data,
  output        auto_out_9_a_bits_corrupt,
  output        auto_out_9_d_ready,
  input         auto_out_9_d_valid,
  input  [2:0]  auto_out_9_d_bits_opcode,
  input  [2:0]  auto_out_9_d_bits_size,
  input  [6:0]  auto_out_9_d_bits_source,
  input  [63:0] auto_out_9_d_bits_data,
  input         auto_out_8_a_ready,
  output        auto_out_8_a_valid,
  output [2:0]  auto_out_8_a_bits_opcode,
  output [2:0]  auto_out_8_a_bits_param,
  output [2:0]  auto_out_8_a_bits_size,
  output [6:0]  auto_out_8_a_bits_source,
  output [28:0] auto_out_8_a_bits_address,
  output [7:0]  auto_out_8_a_bits_mask,
  output [63:0] auto_out_8_a_bits_data,
  output        auto_out_8_a_bits_corrupt,
  output        auto_out_8_d_ready,
  input         auto_out_8_d_valid,
  input  [2:0]  auto_out_8_d_bits_opcode,
  input  [2:0]  auto_out_8_d_bits_size,
  input  [6:0]  auto_out_8_d_bits_source,
  input  [63:0] auto_out_8_d_bits_data,
  input         auto_out_7_a_ready,
  output        auto_out_7_a_valid,
  output [2:0]  auto_out_7_a_bits_opcode,
  output [2:0]  auto_out_7_a_bits_param,
  output [2:0]  auto_out_7_a_bits_size,
  output [6:0]  auto_out_7_a_bits_source,
  output [28:0] auto_out_7_a_bits_address,
  output [7:0]  auto_out_7_a_bits_mask,
  output [63:0] auto_out_7_a_bits_data,
  output        auto_out_7_a_bits_corrupt,
  output        auto_out_7_d_ready,
  input         auto_out_7_d_valid,
  input  [2:0]  auto_out_7_d_bits_opcode,
  input  [2:0]  auto_out_7_d_bits_size,
  input  [6:0]  auto_out_7_d_bits_source,
  input  [63:0] auto_out_7_d_bits_data,
  input         auto_out_6_a_ready,
  output        auto_out_6_a_valid,
  output [2:0]  auto_out_6_a_bits_opcode,
  output [2:0]  auto_out_6_a_bits_param,
  output [2:0]  auto_out_6_a_bits_size,
  output [6:0]  auto_out_6_a_bits_source,
  output [28:0] auto_out_6_a_bits_address,
  output [7:0]  auto_out_6_a_bits_mask,
  output [63:0] auto_out_6_a_bits_data,
  output        auto_out_6_a_bits_corrupt,
  output        auto_out_6_d_ready,
  input         auto_out_6_d_valid,
  input  [2:0]  auto_out_6_d_bits_opcode,
  input  [2:0]  auto_out_6_d_bits_size,
  input  [6:0]  auto_out_6_d_bits_source,
  input  [63:0] auto_out_6_d_bits_data,
  input         auto_out_5_a_ready,
  output        auto_out_5_a_valid,
  output [2:0]  auto_out_5_a_bits_opcode,
  output [2:0]  auto_out_5_a_bits_param,
  output [2:0]  auto_out_5_a_bits_size,
  output [6:0]  auto_out_5_a_bits_source,
  output [31:0] auto_out_5_a_bits_address,
  output        auto_out_5_a_bits_user_amba_prot_bufferable,
  output        auto_out_5_a_bits_user_amba_prot_modifiable,
  output        auto_out_5_a_bits_user_amba_prot_readalloc,
  output        auto_out_5_a_bits_user_amba_prot_writealloc,
  output        auto_out_5_a_bits_user_amba_prot_privileged,
  output        auto_out_5_a_bits_user_amba_prot_secure,
  output        auto_out_5_a_bits_user_amba_prot_fetch,
  output [7:0]  auto_out_5_a_bits_mask,
  output [63:0] auto_out_5_a_bits_data,
  output        auto_out_5_a_bits_corrupt,
  output        auto_out_5_d_ready,
  input         auto_out_5_d_valid,
  input  [2:0]  auto_out_5_d_bits_opcode,
  input  [2:0]  auto_out_5_d_bits_size,
  input  [6:0]  auto_out_5_d_bits_source,
  input         auto_out_5_d_bits_denied,
  input  [63:0] auto_out_5_d_bits_data,
  input         auto_out_5_d_bits_corrupt,
  input         auto_out_4_a_ready,
  output        auto_out_4_a_valid,
  output [2:0]  auto_out_4_a_bits_opcode,
  output [2:0]  auto_out_4_a_bits_param,
  output [2:0]  auto_out_4_a_bits_size,
  output [6:0]  auto_out_4_a_bits_source,
  output [11:0] auto_out_4_a_bits_address,
  output [7:0]  auto_out_4_a_bits_mask,
  output [63:0] auto_out_4_a_bits_data,
  output        auto_out_4_a_bits_corrupt,
  output        auto_out_4_d_ready,
  input         auto_out_4_d_valid,
  input  [2:0]  auto_out_4_d_bits_opcode,
  input  [2:0]  auto_out_4_d_bits_size,
  input  [6:0]  auto_out_4_d_bits_source,
  input  [63:0] auto_out_4_d_bits_data,
  input         auto_out_3_a_ready,
  output        auto_out_3_a_valid,
  output [2:0]  auto_out_3_a_bits_opcode,
  output [2:0]  auto_out_3_a_bits_param,
  output [2:0]  auto_out_3_a_bits_size,
  output [6:0]  auto_out_3_a_bits_source,
  output [25:0] auto_out_3_a_bits_address,
  output [7:0]  auto_out_3_a_bits_mask,
  output [63:0] auto_out_3_a_bits_data,
  output        auto_out_3_a_bits_corrupt,
  output        auto_out_3_d_ready,
  input         auto_out_3_d_valid,
  input  [2:0]  auto_out_3_d_bits_opcode,
  input  [2:0]  auto_out_3_d_bits_size,
  input  [6:0]  auto_out_3_d_bits_source,
  input  [63:0] auto_out_3_d_bits_data,
  input         auto_out_2_a_ready,
  output        auto_out_2_a_valid,
  output [2:0]  auto_out_2_a_bits_opcode,
  output [2:0]  auto_out_2_a_bits_param,
  output [2:0]  auto_out_2_a_bits_size,
  output [6:0]  auto_out_2_a_bits_source,
  output [27:0] auto_out_2_a_bits_address,
  output [7:0]  auto_out_2_a_bits_mask,
  output [63:0] auto_out_2_a_bits_data,
  output        auto_out_2_a_bits_corrupt,
  output        auto_out_2_d_ready,
  input         auto_out_2_d_valid,
  input  [2:0]  auto_out_2_d_bits_opcode,
  input  [2:0]  auto_out_2_d_bits_size,
  input  [6:0]  auto_out_2_d_bits_source,
  input  [63:0] auto_out_2_d_bits_data,
  input         auto_out_1_a_ready,
  output        auto_out_1_a_valid,
  output [2:0]  auto_out_1_a_bits_opcode,
  output [2:0]  auto_out_1_a_bits_param,
  output [2:0]  auto_out_1_a_bits_size,
  output [6:0]  auto_out_1_a_bits_source,
  output [25:0] auto_out_1_a_bits_address,
  output [7:0]  auto_out_1_a_bits_mask,
  output [63:0] auto_out_1_a_bits_data,
  output        auto_out_1_a_bits_corrupt,
  output        auto_out_1_d_ready,
  input         auto_out_1_d_valid,
  input  [2:0]  auto_out_1_d_bits_opcode,
  input  [2:0]  auto_out_1_d_bits_size,
  input  [6:0]  auto_out_1_d_bits_source,
  input  [63:0] auto_out_1_d_bits_data,
  input         auto_out_0_a_ready,
  output        auto_out_0_a_valid,
  output [2:0]  auto_out_0_a_bits_opcode,
  output [2:0]  auto_out_0_a_bits_param,
  output [3:0]  auto_out_0_a_bits_size,
  output [6:0]  auto_out_0_a_bits_source,
  output [13:0] auto_out_0_a_bits_address,
  output [7:0]  auto_out_0_a_bits_mask,
  output        auto_out_0_a_bits_corrupt,
  output        auto_out_0_d_ready,
  input         auto_out_0_d_valid,
  input  [2:0]  auto_out_0_d_bits_opcode,
  input  [1:0]  auto_out_0_d_bits_param,
  input  [3:0]  auto_out_0_d_bits_size,
  input  [6:0]  auto_out_0_d_bits_source,
  input         auto_out_0_d_bits_sink,
  input         auto_out_0_d_bits_denied,
  input  [63:0] auto_out_0_d_bits_data,
  input         auto_out_0_d_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
`endif // RANDOMIZE_REG_INIT
  reg [4:0] beatsLeft; // @[Arbiter.scala 87:30]
  wire  idle = beatsLeft == 5'h0; // @[Arbiter.scala 88:28]
  wire [9:0] readys_filter_lo = {auto_out_9_d_valid,auto_out_8_d_valid,auto_out_7_d_valid,auto_out_6_d_valid,
    auto_out_5_d_valid,auto_out_4_d_valid,auto_out_3_d_valid,auto_out_2_d_valid,auto_out_1_d_valid,auto_out_0_d_valid}; // @[Cat.scala 30:58]
  reg [9:0] readys_mask; // @[Arbiter.scala 23:23]
  wire [9:0] _readys_filter_T = ~readys_mask; // @[Arbiter.scala 24:30]
  wire [9:0] readys_filter_hi = readys_filter_lo & _readys_filter_T; // @[Arbiter.scala 24:28]
  wire [19:0] readys_filter = {readys_filter_hi,readys_filter_lo}; // @[Cat.scala 30:58]
  wire [19:0] _GEN_1 = {{1'd0}, readys_filter[19:1]}; // @[package.scala 253:43]
  wire [19:0] _readys_unready_T_1 = readys_filter | _GEN_1; // @[package.scala 253:43]
  wire [19:0] _GEN_2 = {{2'd0}, _readys_unready_T_1[19:2]}; // @[package.scala 253:43]
  wire [19:0] _readys_unready_T_3 = _readys_unready_T_1 | _GEN_2; // @[package.scala 253:43]
  wire [19:0] _GEN_3 = {{4'd0}, _readys_unready_T_3[19:4]}; // @[package.scala 253:43]
  wire [19:0] _readys_unready_T_5 = _readys_unready_T_3 | _GEN_3; // @[package.scala 253:43]
  wire [19:0] _GEN_4 = {{8'd0}, _readys_unready_T_5[19:8]}; // @[package.scala 253:43]
  wire [19:0] _readys_unready_T_7 = _readys_unready_T_5 | _GEN_4; // @[package.scala 253:43]
  wire [19:0] _readys_unready_T_10 = {readys_mask, 10'h0}; // @[Arbiter.scala 25:66]
  wire [19:0] _GEN_5 = {{1'd0}, _readys_unready_T_7[19:1]}; // @[Arbiter.scala 25:58]
  wire [19:0] readys_unready = _GEN_5 | _readys_unready_T_10; // @[Arbiter.scala 25:58]
  wire [9:0] _readys_readys_T_2 = readys_unready[19:10] & readys_unready[9:0]; // @[Arbiter.scala 26:39]
  wire [9:0] readys_readys = ~_readys_readys_T_2; // @[Arbiter.scala 26:18]
  wire  readys_0 = readys_readys[0]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_0 = readys_0 & auto_out_0_d_valid; // @[Arbiter.scala 97:79]
  reg  state_0; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_0 = idle ? earlyWinner_0 : state_0; // @[Arbiter.scala 117:30]
  wire [6:0] _T_164 = muxStateEarly_0 ? auto_out_0_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire  readys_1 = readys_readys[1]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_1 = readys_1 & auto_out_1_d_valid; // @[Arbiter.scala 97:79]
  reg  state_1; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_1 = idle ? earlyWinner_1 : state_1; // @[Arbiter.scala 117:30]
  wire [6:0] _T_165 = muxStateEarly_1 ? auto_out_1_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_174 = _T_164 | _T_165; // @[Mux.scala 27:72]
  wire  readys_2 = readys_readys[2]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_2 = readys_2 & auto_out_2_d_valid; // @[Arbiter.scala 97:79]
  reg  state_2; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_2 = idle ? earlyWinner_2 : state_2; // @[Arbiter.scala 117:30]
  wire [6:0] _T_166 = muxStateEarly_2 ? auto_out_2_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_175 = _T_174 | _T_166; // @[Mux.scala 27:72]
  wire  readys_3 = readys_readys[3]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_3 = readys_3 & auto_out_3_d_valid; // @[Arbiter.scala 97:79]
  reg  state_3; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_3 = idle ? earlyWinner_3 : state_3; // @[Arbiter.scala 117:30]
  wire [6:0] _T_167 = muxStateEarly_3 ? auto_out_3_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_176 = _T_175 | _T_167; // @[Mux.scala 27:72]
  wire  readys_4 = readys_readys[4]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_4 = readys_4 & auto_out_4_d_valid; // @[Arbiter.scala 97:79]
  reg  state_4; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_4 = idle ? earlyWinner_4 : state_4; // @[Arbiter.scala 117:30]
  wire [6:0] _T_168 = muxStateEarly_4 ? auto_out_4_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_177 = _T_176 | _T_168; // @[Mux.scala 27:72]
  wire  readys_5 = readys_readys[5]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_5 = readys_5 & auto_out_5_d_valid; // @[Arbiter.scala 97:79]
  reg  state_5; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_5 = idle ? earlyWinner_5 : state_5; // @[Arbiter.scala 117:30]
  wire [6:0] _T_169 = muxStateEarly_5 ? auto_out_5_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_178 = _T_177 | _T_169; // @[Mux.scala 27:72]
  wire  readys_6 = readys_readys[6]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_6 = readys_6 & auto_out_6_d_valid; // @[Arbiter.scala 97:79]
  reg  state_6; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_6 = idle ? earlyWinner_6 : state_6; // @[Arbiter.scala 117:30]
  wire [6:0] _T_170 = muxStateEarly_6 ? auto_out_6_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_179 = _T_178 | _T_170; // @[Mux.scala 27:72]
  wire  readys_7 = readys_readys[7]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_7 = readys_7 & auto_out_7_d_valid; // @[Arbiter.scala 97:79]
  reg  state_7; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_7 = idle ? earlyWinner_7 : state_7; // @[Arbiter.scala 117:30]
  wire [6:0] _T_171 = muxStateEarly_7 ? auto_out_7_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_180 = _T_179 | _T_171; // @[Mux.scala 27:72]
  wire  readys_8 = readys_readys[8]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_8 = readys_8 & auto_out_8_d_valid; // @[Arbiter.scala 97:79]
  reg  state_8; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_8 = idle ? earlyWinner_8 : state_8; // @[Arbiter.scala 117:30]
  wire [6:0] _T_172 = muxStateEarly_8 ? auto_out_8_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _T_181 = _T_180 | _T_172; // @[Mux.scala 27:72]
  wire  readys_9 = readys_readys[9]; // @[Arbiter.scala 95:86]
  wire  earlyWinner_9 = readys_9 & auto_out_9_d_valid; // @[Arbiter.scala 97:79]
  reg  state_9; // @[Arbiter.scala 116:26]
  wire  muxStateEarly_9 = idle ? earlyWinner_9 : state_9; // @[Arbiter.scala 117:30]
  wire [6:0] _T_173 = muxStateEarly_9 ? auto_out_9_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [31:0] _requestAIO_T = auto_in_a_bits_address ^ 32'h3000; // @[Parameters.scala 137:31]
  wire [32:0] _requestAIO_T_1 = {1'b0,$signed(_requestAIO_T)}; // @[Parameters.scala 137:49]
  wire [32:0] _requestAIO_T_3 = $signed(_requestAIO_T_1) & 33'sh9a017000; // @[Parameters.scala 137:52]
  wire  requestAIO_0_0 = $signed(_requestAIO_T_3) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _requestAIO_T_5 = auto_in_a_bits_address ^ 32'h2010000; // @[Parameters.scala 137:31]
  wire [32:0] _requestAIO_T_6 = {1'b0,$signed(_requestAIO_T_5)}; // @[Parameters.scala 137:49]
  wire [32:0] _requestAIO_T_8 = $signed(_requestAIO_T_6) & 33'sh9a014000; // @[Parameters.scala 137:52]
  wire  requestAIO_0_1 = $signed(_requestAIO_T_8) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _requestAIO_T_10 = auto_in_a_bits_address ^ 32'h8000000; // @[Parameters.scala 137:31]
  wire [32:0] _requestAIO_T_11 = {1'b0,$signed(_requestAIO_T_10)}; // @[Parameters.scala 137:49]
  wire [32:0] _requestAIO_T_13 = $signed(_requestAIO_T_11) & 33'sh98000000; // @[Parameters.scala 137:52]
  wire  requestAIO_0_2 = $signed(_requestAIO_T_13) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _requestAIO_T_15 = auto_in_a_bits_address ^ 32'h2000000; // @[Parameters.scala 137:31]
  wire [32:0] _requestAIO_T_16 = {1'b0,$signed(_requestAIO_T_15)}; // @[Parameters.scala 137:49]
  wire [32:0] _requestAIO_T_18 = $signed(_requestAIO_T_16) & 33'sh9a010000; // @[Parameters.scala 137:52]
  wire  requestAIO_0_3 = $signed(_requestAIO_T_18) == 33'sh0; // @[Parameters.scala 137:67]
  wire [32:0] _requestAIO_T_21 = {1'b0,$signed(auto_in_a_bits_address)}; // @[Parameters.scala 137:49]
  wire [32:0] _requestAIO_T_23 = $signed(_requestAIO_T_21) & 33'sh9a017000; // @[Parameters.scala 137:52]
  wire  requestAIO_0_4 = $signed(_requestAIO_T_23) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _requestAIO_T_25 = auto_in_a_bits_address ^ 32'h80000000; // @[Parameters.scala 137:31]
  wire [32:0] _requestAIO_T_26 = {1'b0,$signed(_requestAIO_T_25)}; // @[Parameters.scala 137:49]
  wire [32:0] _requestAIO_T_28 = $signed(_requestAIO_T_26) & 33'sh80000000; // @[Parameters.scala 137:52]
  wire  requestAIO_0_5 = $signed(_requestAIO_T_28) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _requestAIO_T_30 = auto_in_a_bits_address ^ 32'h10000000; // @[Parameters.scala 137:31]
  wire [32:0] _requestAIO_T_31 = {1'b0,$signed(_requestAIO_T_30)}; // @[Parameters.scala 137:49]
  wire [32:0] _requestAIO_T_33 = $signed(_requestAIO_T_31) & 33'sh9a017000; // @[Parameters.scala 137:52]
  wire  requestAIO_0_6 = $signed(_requestAIO_T_33) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _requestAIO_T_35 = auto_in_a_bits_address ^ 32'h10001000; // @[Parameters.scala 137:31]
  wire [32:0] _requestAIO_T_36 = {1'b0,$signed(_requestAIO_T_35)}; // @[Parameters.scala 137:49]
  wire [32:0] _requestAIO_T_38 = $signed(_requestAIO_T_36) & 33'sh9a017000; // @[Parameters.scala 137:52]
  wire  requestAIO_0_7 = $signed(_requestAIO_T_38) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _requestAIO_T_40 = auto_in_a_bits_address ^ 32'h10003000; // @[Parameters.scala 137:31]
  wire [32:0] _requestAIO_T_41 = {1'b0,$signed(_requestAIO_T_40)}; // @[Parameters.scala 137:49]
  wire [32:0] _requestAIO_T_43 = $signed(_requestAIO_T_41) & 33'sh9a017000; // @[Parameters.scala 137:52]
  wire  requestAIO_0_8 = $signed(_requestAIO_T_43) == 33'sh0; // @[Parameters.scala 137:67]
  wire [31:0] _requestAIO_T_45 = auto_in_a_bits_address ^ 32'h4000; // @[Parameters.scala 137:31]
  wire [32:0] _requestAIO_T_46 = {1'b0,$signed(_requestAIO_T_45)}; // @[Parameters.scala 137:49]
  wire [32:0] _requestAIO_T_48 = $signed(_requestAIO_T_46) & 33'sh9a017000; // @[Parameters.scala 137:52]
  wire  requestAIO_0_9 = $signed(_requestAIO_T_48) == 33'sh0; // @[Parameters.scala 137:67]
  wire [22:0] _beatsDO_decode_T_1 = 23'hff << auto_out_0_d_bits_size; // @[package.scala 234:77]
  wire [7:0] _beatsDO_decode_T_3 = ~_beatsDO_decode_T_1[7:0]; // @[package.scala 234:46]
  wire [4:0] beatsDO_decode = _beatsDO_decode_T_3[7:3]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata = auto_out_0_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [4:0] beatsDO_0 = beatsDO_opdata ? beatsDO_decode : 5'h0; // @[Edges.scala 220:14]
  wire [3:0] out_1_1_d_bits_size = {{1'd0}, auto_out_1_d_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [20:0] _beatsDO_decode_T_5 = 21'h3f << out_1_1_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_7 = ~_beatsDO_decode_T_5[5:0]; // @[package.scala 234:46]
  wire [2:0] beatsDO_decode_1 = _beatsDO_decode_T_7[5:3]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_1 = auto_out_1_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [2:0] beatsDO_1 = beatsDO_opdata_1 ? beatsDO_decode_1 : 3'h0; // @[Edges.scala 220:14]
  wire [3:0] out_1_2_d_bits_size = {{1'd0}, auto_out_2_d_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [20:0] _beatsDO_decode_T_9 = 21'h3f << out_1_2_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_11 = ~_beatsDO_decode_T_9[5:0]; // @[package.scala 234:46]
  wire [2:0] beatsDO_decode_2 = _beatsDO_decode_T_11[5:3]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_2 = auto_out_2_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [2:0] beatsDO_2 = beatsDO_opdata_2 ? beatsDO_decode_2 : 3'h0; // @[Edges.scala 220:14]
  wire [3:0] out_1_3_d_bits_size = {{1'd0}, auto_out_3_d_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [20:0] _beatsDO_decode_T_13 = 21'h3f << out_1_3_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_15 = ~_beatsDO_decode_T_13[5:0]; // @[package.scala 234:46]
  wire [2:0] beatsDO_decode_3 = _beatsDO_decode_T_15[5:3]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_3 = auto_out_3_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [2:0] beatsDO_3 = beatsDO_opdata_3 ? beatsDO_decode_3 : 3'h0; // @[Edges.scala 220:14]
  wire [3:0] out_1_4_d_bits_size = {{1'd0}, auto_out_4_d_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [20:0] _beatsDO_decode_T_17 = 21'h3f << out_1_4_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_19 = ~_beatsDO_decode_T_17[5:0]; // @[package.scala 234:46]
  wire [2:0] beatsDO_decode_4 = _beatsDO_decode_T_19[5:3]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_4 = auto_out_4_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [2:0] beatsDO_4 = beatsDO_opdata_4 ? beatsDO_decode_4 : 3'h0; // @[Edges.scala 220:14]
  wire [3:0] out_1_5_d_bits_size = {{1'd0}, auto_out_5_d_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [20:0] _beatsDO_decode_T_21 = 21'h3f << out_1_5_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_23 = ~_beatsDO_decode_T_21[5:0]; // @[package.scala 234:46]
  wire [2:0] beatsDO_decode_5 = _beatsDO_decode_T_23[5:3]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_5 = auto_out_5_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [2:0] beatsDO_5 = beatsDO_opdata_5 ? beatsDO_decode_5 : 3'h0; // @[Edges.scala 220:14]
  wire [3:0] out_1_6_d_bits_size = {{1'd0}, auto_out_6_d_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [20:0] _beatsDO_decode_T_25 = 21'h3f << out_1_6_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_27 = ~_beatsDO_decode_T_25[5:0]; // @[package.scala 234:46]
  wire [2:0] beatsDO_decode_6 = _beatsDO_decode_T_27[5:3]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_6 = auto_out_6_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [2:0] beatsDO_6 = beatsDO_opdata_6 ? beatsDO_decode_6 : 3'h0; // @[Edges.scala 220:14]
  wire [3:0] out_1_7_d_bits_size = {{1'd0}, auto_out_7_d_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [20:0] _beatsDO_decode_T_29 = 21'h3f << out_1_7_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_31 = ~_beatsDO_decode_T_29[5:0]; // @[package.scala 234:46]
  wire [2:0] beatsDO_decode_7 = _beatsDO_decode_T_31[5:3]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_7 = auto_out_7_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [2:0] beatsDO_7 = beatsDO_opdata_7 ? beatsDO_decode_7 : 3'h0; // @[Edges.scala 220:14]
  wire [3:0] out_1_8_d_bits_size = {{1'd0}, auto_out_8_d_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [20:0] _beatsDO_decode_T_33 = 21'h3f << out_1_8_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_35 = ~_beatsDO_decode_T_33[5:0]; // @[package.scala 234:46]
  wire [2:0] beatsDO_decode_8 = _beatsDO_decode_T_35[5:3]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_8 = auto_out_8_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [2:0] beatsDO_8 = beatsDO_opdata_8 ? beatsDO_decode_8 : 3'h0; // @[Edges.scala 220:14]
  wire [3:0] out_1_9_d_bits_size = {{1'd0}, auto_out_9_d_bits_size}; // @[Xbar.scala 288:19 BundleMap.scala 247:19]
  wire [20:0] _beatsDO_decode_T_37 = 21'h3f << out_1_9_d_bits_size; // @[package.scala 234:77]
  wire [5:0] _beatsDO_decode_T_39 = ~_beatsDO_decode_T_37[5:0]; // @[package.scala 234:46]
  wire [2:0] beatsDO_decode_9 = _beatsDO_decode_T_39[5:3]; // @[Edges.scala 219:59]
  wire  beatsDO_opdata_9 = auto_out_9_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire [2:0] beatsDO_9 = beatsDO_opdata_9 ? beatsDO_decode_9 : 3'h0; // @[Edges.scala 220:14]
  wire  _portsAOI_in_0_a_ready_T_9 = requestAIO_0_9 & auto_out_9_a_ready; // @[Mux.scala 27:72]
  wire  _portsAOI_in_0_a_ready_T_17 = requestAIO_0_0 & auto_out_0_a_ready | requestAIO_0_1 & auto_out_1_a_ready |
    requestAIO_0_2 & auto_out_2_a_ready | requestAIO_0_3 & auto_out_3_a_ready | requestAIO_0_4 & auto_out_4_a_ready |
    requestAIO_0_5 & auto_out_5_a_ready | requestAIO_0_6 & auto_out_6_a_ready | requestAIO_0_7 & auto_out_7_a_ready |
    requestAIO_0_8 & auto_out_8_a_ready; // @[Mux.scala 27:72]
  wire  latch = idle & auto_in_d_ready; // @[Arbiter.scala 89:24]
  wire [9:0] _readys_mask_T = readys_readys & readys_filter_lo; // @[Arbiter.scala 28:29]
  wire [10:0] _readys_mask_T_1 = {_readys_mask_T, 1'h0}; // @[package.scala 244:48]
  wire [9:0] _readys_mask_T_3 = _readys_mask_T | _readys_mask_T_1[9:0]; // @[package.scala 244:43]
  wire [11:0] _readys_mask_T_4 = {_readys_mask_T_3, 2'h0}; // @[package.scala 244:48]
  wire [9:0] _readys_mask_T_6 = _readys_mask_T_3 | _readys_mask_T_4[9:0]; // @[package.scala 244:43]
  wire [13:0] _readys_mask_T_7 = {_readys_mask_T_6, 4'h0}; // @[package.scala 244:48]
  wire [9:0] _readys_mask_T_9 = _readys_mask_T_6 | _readys_mask_T_7[9:0]; // @[package.scala 244:43]
  wire [17:0] _readys_mask_T_10 = {_readys_mask_T_9, 8'h0}; // @[package.scala 244:48]
  wire [9:0] _readys_mask_T_12 = _readys_mask_T_9 | _readys_mask_T_10[9:0]; // @[package.scala 244:43]
  wire  _T_50 = auto_out_0_d_valid | auto_out_1_d_valid | auto_out_2_d_valid | auto_out_3_d_valid | auto_out_4_d_valid
     | auto_out_5_d_valid | auto_out_6_d_valid | auto_out_7_d_valid | auto_out_8_d_valid | auto_out_9_d_valid; // @[Arbiter.scala 107:36]
  wire [4:0] maskedBeats_0 = earlyWinner_0 ? beatsDO_0 : 5'h0; // @[Arbiter.scala 111:73]
  wire [2:0] maskedBeats_1 = earlyWinner_1 ? beatsDO_1 : 3'h0; // @[Arbiter.scala 111:73]
  wire [2:0] maskedBeats_2 = earlyWinner_2 ? beatsDO_2 : 3'h0; // @[Arbiter.scala 111:73]
  wire [2:0] maskedBeats_3 = earlyWinner_3 ? beatsDO_3 : 3'h0; // @[Arbiter.scala 111:73]
  wire [2:0] maskedBeats_4 = earlyWinner_4 ? beatsDO_4 : 3'h0; // @[Arbiter.scala 111:73]
  wire [2:0] maskedBeats_5 = earlyWinner_5 ? beatsDO_5 : 3'h0; // @[Arbiter.scala 111:73]
  wire [2:0] maskedBeats_6 = earlyWinner_6 ? beatsDO_6 : 3'h0; // @[Arbiter.scala 111:73]
  wire [2:0] maskedBeats_7 = earlyWinner_7 ? beatsDO_7 : 3'h0; // @[Arbiter.scala 111:73]
  wire [2:0] maskedBeats_8 = earlyWinner_8 ? beatsDO_8 : 3'h0; // @[Arbiter.scala 111:73]
  wire [2:0] maskedBeats_9 = earlyWinner_9 ? beatsDO_9 : 3'h0; // @[Arbiter.scala 111:73]
  wire [4:0] _GEN_6 = {{2'd0}, maskedBeats_1}; // @[Arbiter.scala 112:44]
  wire [4:0] _initBeats_T = maskedBeats_0 | _GEN_6; // @[Arbiter.scala 112:44]
  wire [4:0] _GEN_7 = {{2'd0}, maskedBeats_2}; // @[Arbiter.scala 112:44]
  wire [4:0] _initBeats_T_1 = _initBeats_T | _GEN_7; // @[Arbiter.scala 112:44]
  wire [4:0] _GEN_8 = {{2'd0}, maskedBeats_3}; // @[Arbiter.scala 112:44]
  wire [4:0] _initBeats_T_2 = _initBeats_T_1 | _GEN_8; // @[Arbiter.scala 112:44]
  wire [4:0] _GEN_9 = {{2'd0}, maskedBeats_4}; // @[Arbiter.scala 112:44]
  wire [4:0] _initBeats_T_3 = _initBeats_T_2 | _GEN_9; // @[Arbiter.scala 112:44]
  wire [4:0] _GEN_10 = {{2'd0}, maskedBeats_5}; // @[Arbiter.scala 112:44]
  wire [4:0] _initBeats_T_4 = _initBeats_T_3 | _GEN_10; // @[Arbiter.scala 112:44]
  wire [4:0] _GEN_11 = {{2'd0}, maskedBeats_6}; // @[Arbiter.scala 112:44]
  wire [4:0] _initBeats_T_5 = _initBeats_T_4 | _GEN_11; // @[Arbiter.scala 112:44]
  wire [4:0] _GEN_12 = {{2'd0}, maskedBeats_7}; // @[Arbiter.scala 112:44]
  wire [4:0] _initBeats_T_6 = _initBeats_T_5 | _GEN_12; // @[Arbiter.scala 112:44]
  wire [4:0] _GEN_13 = {{2'd0}, maskedBeats_8}; // @[Arbiter.scala 112:44]
  wire [4:0] _initBeats_T_7 = _initBeats_T_6 | _GEN_13; // @[Arbiter.scala 112:44]
  wire [4:0] _GEN_14 = {{2'd0}, maskedBeats_9}; // @[Arbiter.scala 112:44]
  wire [4:0] initBeats = _initBeats_T_7 | _GEN_14; // @[Arbiter.scala 112:44]
  wire  _sink_ACancel_earlyValid_T_27 = state_0 & auto_out_0_d_valid | state_1 & auto_out_1_d_valid | state_2 &
    auto_out_2_d_valid | state_3 & auto_out_3_d_valid | state_4 & auto_out_4_d_valid | state_5 & auto_out_5_d_valid |
    state_6 & auto_out_6_d_valid | state_7 & auto_out_7_d_valid | state_8 & auto_out_8_d_valid | state_9 &
    auto_out_9_d_valid; // @[Mux.scala 27:72]
  wire  sink_ACancel_21_earlyValid = idle ? _T_50 : _sink_ACancel_earlyValid_T_27; // @[Arbiter.scala 125:29]
  wire  _beatsLeft_T_2 = auto_in_d_ready & sink_ACancel_21_earlyValid; // @[ReadyValidCancel.scala 50:33]
  wire [4:0] _GEN_15 = {{4'd0}, _beatsLeft_T_2}; // @[Arbiter.scala 113:52]
  wire [4:0] _beatsLeft_T_4 = beatsLeft - _GEN_15; // @[Arbiter.scala 113:52]
  wire  allowed_0 = idle ? readys_0 : state_0; // @[Arbiter.scala 121:24]
  wire  allowed_1 = idle ? readys_1 : state_1; // @[Arbiter.scala 121:24]
  wire  allowed_2 = idle ? readys_2 : state_2; // @[Arbiter.scala 121:24]
  wire  allowed_3 = idle ? readys_3 : state_3; // @[Arbiter.scala 121:24]
  wire  allowed_4 = idle ? readys_4 : state_4; // @[Arbiter.scala 121:24]
  wire  allowed_5 = idle ? readys_5 : state_5; // @[Arbiter.scala 121:24]
  wire  allowed_6 = idle ? readys_6 : state_6; // @[Arbiter.scala 121:24]
  wire  allowed_7 = idle ? readys_7 : state_7; // @[Arbiter.scala 121:24]
  wire  allowed_8 = idle ? readys_8 : state_8; // @[Arbiter.scala 121:24]
  wire  allowed_9 = idle ? readys_9 : state_9; // @[Arbiter.scala 121:24]
  wire  _T_88 = muxStateEarly_0 & auto_out_0_d_bits_corrupt; // @[Mux.scala 27:72]
  wire  _T_93 = muxStateEarly_5 & auto_out_5_d_bits_corrupt; // @[Mux.scala 27:72]
  wire [63:0] _T_107 = muxStateEarly_0 ? auto_out_0_d_bits_data : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _T_108 = muxStateEarly_1 ? auto_out_1_d_bits_data : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _T_109 = muxStateEarly_2 ? auto_out_2_d_bits_data : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _T_110 = muxStateEarly_3 ? auto_out_3_d_bits_data : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _T_111 = muxStateEarly_4 ? auto_out_4_d_bits_data : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _T_112 = muxStateEarly_5 ? auto_out_5_d_bits_data : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _T_113 = muxStateEarly_6 ? auto_out_6_d_bits_data : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _T_114 = muxStateEarly_7 ? auto_out_7_d_bits_data : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _T_115 = muxStateEarly_8 ? auto_out_8_d_bits_data : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _T_116 = muxStateEarly_9 ? auto_out_9_d_bits_data : 64'h0; // @[Mux.scala 27:72]
  wire [63:0] _T_117 = _T_107 | _T_108; // @[Mux.scala 27:72]
  wire [63:0] _T_118 = _T_117 | _T_109; // @[Mux.scala 27:72]
  wire [63:0] _T_119 = _T_118 | _T_110; // @[Mux.scala 27:72]
  wire [63:0] _T_120 = _T_119 | _T_111; // @[Mux.scala 27:72]
  wire [63:0] _T_121 = _T_120 | _T_112; // @[Mux.scala 27:72]
  wire [63:0] _T_122 = _T_121 | _T_113; // @[Mux.scala 27:72]
  wire [63:0] _T_123 = _T_122 | _T_114; // @[Mux.scala 27:72]
  wire [63:0] _T_124 = _T_123 | _T_115; // @[Mux.scala 27:72]
  wire  _T_126 = muxStateEarly_0 & auto_out_0_d_bits_denied; // @[Mux.scala 27:72]
  wire  _T_131 = muxStateEarly_5 & auto_out_5_d_bits_denied; // @[Mux.scala 27:72]
  wire [3:0] _T_183 = muxStateEarly_0 ? auto_out_0_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_184 = muxStateEarly_1 ? out_1_1_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_185 = muxStateEarly_2 ? out_1_2_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_186 = muxStateEarly_3 ? out_1_3_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_187 = muxStateEarly_4 ? out_1_4_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_188 = muxStateEarly_5 ? out_1_5_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_189 = muxStateEarly_6 ? out_1_6_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_190 = muxStateEarly_7 ? out_1_7_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_191 = muxStateEarly_8 ? out_1_8_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_192 = muxStateEarly_9 ? out_1_9_d_bits_size : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _T_193 = _T_183 | _T_184; // @[Mux.scala 27:72]
  wire [3:0] _T_194 = _T_193 | _T_185; // @[Mux.scala 27:72]
  wire [3:0] _T_195 = _T_194 | _T_186; // @[Mux.scala 27:72]
  wire [3:0] _T_196 = _T_195 | _T_187; // @[Mux.scala 27:72]
  wire [3:0] _T_197 = _T_196 | _T_188; // @[Mux.scala 27:72]
  wire [3:0] _T_198 = _T_197 | _T_189; // @[Mux.scala 27:72]
  wire [3:0] _T_199 = _T_198 | _T_190; // @[Mux.scala 27:72]
  wire [3:0] _T_200 = _T_199 | _T_191; // @[Mux.scala 27:72]
  wire [2:0] _T_221 = muxStateEarly_0 ? auto_out_0_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_222 = muxStateEarly_1 ? auto_out_1_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_223 = muxStateEarly_2 ? auto_out_2_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_224 = muxStateEarly_3 ? auto_out_3_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_225 = muxStateEarly_4 ? auto_out_4_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_226 = muxStateEarly_5 ? auto_out_5_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_227 = muxStateEarly_6 ? auto_out_6_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_228 = muxStateEarly_7 ? auto_out_7_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_229 = muxStateEarly_8 ? auto_out_8_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_230 = muxStateEarly_9 ? auto_out_9_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _T_231 = _T_221 | _T_222; // @[Mux.scala 27:72]
  wire [2:0] _T_232 = _T_231 | _T_223; // @[Mux.scala 27:72]
  wire [2:0] _T_233 = _T_232 | _T_224; // @[Mux.scala 27:72]
  wire [2:0] _T_234 = _T_233 | _T_225; // @[Mux.scala 27:72]
  wire [2:0] _T_235 = _T_234 | _T_226; // @[Mux.scala 27:72]
  wire [2:0] _T_236 = _T_235 | _T_227; // @[Mux.scala 27:72]
  wire [2:0] _T_237 = _T_236 | _T_228; // @[Mux.scala 27:72]
  wire [2:0] _T_238 = _T_237 | _T_229; // @[Mux.scala 27:72]
  assign auto_in_a_ready = _portsAOI_in_0_a_ready_T_17 | _portsAOI_in_0_a_ready_T_9; // @[Mux.scala 27:72]
  assign auto_in_d_valid = idle ? _T_50 : _sink_ACancel_earlyValid_T_27; // @[Arbiter.scala 125:29]
  assign auto_in_d_bits_opcode = _T_238 | _T_230; // @[Mux.scala 27:72]
  assign auto_in_d_bits_param = muxStateEarly_0 ? auto_out_0_d_bits_param : 2'h0; // @[Mux.scala 27:72]
  assign auto_in_d_bits_size = _T_200 | _T_192; // @[Mux.scala 27:72]
  assign auto_in_d_bits_source = _T_181 | _T_173; // @[Mux.scala 27:72]
  assign auto_in_d_bits_sink = muxStateEarly_0 & auto_out_0_d_bits_sink; // @[Mux.scala 27:72]
  assign auto_in_d_bits_denied = _T_126 | _T_131; // @[Mux.scala 27:72]
  assign auto_in_d_bits_data = _T_124 | _T_116; // @[Mux.scala 27:72]
  assign auto_in_d_bits_corrupt = _T_88 | _T_93; // @[Mux.scala 27:72]
  assign auto_out_9_a_valid = auto_in_a_valid & requestAIO_0_9; // @[Xbar.scala 428:50]
  assign auto_out_9_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_9_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_9_a_bits_size = auto_in_a_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_9_a_bits_source = auto_in_a_bits_source; // @[Xbar.scala 237:55]
  assign auto_out_9_a_bits_address = auto_in_a_bits_address[14:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_9_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_9_a_bits_data = auto_in_a_bits_data; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_9_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_9_d_ready = auto_in_d_ready & allowed_9; // @[Arbiter.scala 123:31]
  assign auto_out_8_a_valid = auto_in_a_valid & requestAIO_0_8; // @[Xbar.scala 428:50]
  assign auto_out_8_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_8_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_8_a_bits_size = auto_in_a_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_8_a_bits_source = auto_in_a_bits_source; // @[Xbar.scala 237:55]
  assign auto_out_8_a_bits_address = auto_in_a_bits_address[28:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_8_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_8_a_bits_data = auto_in_a_bits_data; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_8_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_8_d_ready = auto_in_d_ready & allowed_8; // @[Arbiter.scala 123:31]
  assign auto_out_7_a_valid = auto_in_a_valid & requestAIO_0_7; // @[Xbar.scala 428:50]
  assign auto_out_7_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_7_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_7_a_bits_size = auto_in_a_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_7_a_bits_source = auto_in_a_bits_source; // @[Xbar.scala 237:55]
  assign auto_out_7_a_bits_address = auto_in_a_bits_address[28:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_7_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_7_a_bits_data = auto_in_a_bits_data; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_7_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_7_d_ready = auto_in_d_ready & allowed_7; // @[Arbiter.scala 123:31]
  assign auto_out_6_a_valid = auto_in_a_valid & requestAIO_0_6; // @[Xbar.scala 428:50]
  assign auto_out_6_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_6_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_6_a_bits_size = auto_in_a_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_6_a_bits_source = auto_in_a_bits_source; // @[Xbar.scala 237:55]
  assign auto_out_6_a_bits_address = auto_in_a_bits_address[28:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_6_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_6_a_bits_data = auto_in_a_bits_data; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_6_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_6_d_ready = auto_in_d_ready & allowed_6; // @[Arbiter.scala 123:31]
  assign auto_out_5_a_valid = auto_in_a_valid & requestAIO_0_5; // @[Xbar.scala 428:50]
  assign auto_out_5_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_5_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_5_a_bits_size = auto_in_a_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_5_a_bits_source = auto_in_a_bits_source; // @[Xbar.scala 237:55]
  assign auto_out_5_a_bits_address = auto_in_a_bits_address; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_5_a_bits_user_amba_prot_bufferable = auto_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_5_a_bits_user_amba_prot_modifiable = auto_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_5_a_bits_user_amba_prot_readalloc = auto_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_5_a_bits_user_amba_prot_writealloc = auto_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_5_a_bits_user_amba_prot_privileged = auto_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_5_a_bits_user_amba_prot_secure = auto_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_5_a_bits_user_amba_prot_fetch = auto_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_5_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_5_a_bits_data = auto_in_a_bits_data; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_5_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_5_d_ready = auto_in_d_ready & allowed_5; // @[Arbiter.scala 123:31]
  assign auto_out_4_a_valid = auto_in_a_valid & requestAIO_0_4; // @[Xbar.scala 428:50]
  assign auto_out_4_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_4_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_4_a_bits_size = auto_in_a_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_4_a_bits_source = auto_in_a_bits_source; // @[Xbar.scala 237:55]
  assign auto_out_4_a_bits_address = auto_in_a_bits_address[11:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_4_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_4_a_bits_data = auto_in_a_bits_data; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_4_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_4_d_ready = auto_in_d_ready & allowed_4; // @[Arbiter.scala 123:31]
  assign auto_out_3_a_valid = auto_in_a_valid & requestAIO_0_3; // @[Xbar.scala 428:50]
  assign auto_out_3_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_size = auto_in_a_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_3_a_bits_source = auto_in_a_bits_source; // @[Xbar.scala 237:55]
  assign auto_out_3_a_bits_address = auto_in_a_bits_address[25:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_3_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_data = auto_in_a_bits_data; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_3_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_3_d_ready = auto_in_d_ready & allowed_3; // @[Arbiter.scala 123:31]
  assign auto_out_2_a_valid = auto_in_a_valid & requestAIO_0_2; // @[Xbar.scala 428:50]
  assign auto_out_2_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_size = auto_in_a_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_2_a_bits_source = auto_in_a_bits_source; // @[Xbar.scala 237:55]
  assign auto_out_2_a_bits_address = auto_in_a_bits_address[27:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_2_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_data = auto_in_a_bits_data; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_2_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_2_d_ready = auto_in_d_ready & allowed_2; // @[Arbiter.scala 123:31]
  assign auto_out_1_a_valid = auto_in_a_valid & requestAIO_0_1; // @[Xbar.scala 428:50]
  assign auto_out_1_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_size = auto_in_a_bits_size[2:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_1_a_bits_source = auto_in_a_bits_source; // @[Xbar.scala 237:55]
  assign auto_out_1_a_bits_address = auto_in_a_bits_address[25:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_1_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_data = auto_in_a_bits_data; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_1_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_1_d_ready = auto_in_d_ready & allowed_1; // @[Arbiter.scala 123:31]
  assign auto_out_0_a_valid = auto_in_a_valid & requestAIO_0_0; // @[Xbar.scala 428:50]
  assign auto_out_0_a_bits_opcode = auto_in_a_bits_opcode; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_param = auto_in_a_bits_param; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_size = auto_in_a_bits_size; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_source = auto_in_a_bits_source; // @[Xbar.scala 237:55]
  assign auto_out_0_a_bits_address = auto_in_a_bits_address[13:0]; // @[Xbar.scala 132:50 BundleMap.scala 247:19]
  assign auto_out_0_a_bits_mask = auto_in_a_bits_mask; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_0_a_bits_corrupt = auto_in_a_bits_corrupt; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign auto_out_0_d_ready = auto_in_d_ready & allowed_0; // @[Arbiter.scala 123:31]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      beatsLeft <= 5'h0;
    end else if (latch) begin
      beatsLeft <= initBeats;
    end else begin
      beatsLeft <= _beatsLeft_T_4;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      readys_mask <= 10'h3ff;
    end else if (latch & |readys_filter_lo) begin
      readys_mask <= _readys_mask_T_12;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_0 <= 1'h0;
    end else if (idle) begin
      state_0 <= earlyWinner_0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_1 <= 1'h0;
    end else if (idle) begin
      state_1 <= earlyWinner_1;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_2 <= 1'h0;
    end else if (idle) begin
      state_2 <= earlyWinner_2;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_3 <= 1'h0;
    end else if (idle) begin
      state_3 <= earlyWinner_3;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_4 <= 1'h0;
    end else if (idle) begin
      state_4 <= earlyWinner_4;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_5 <= 1'h0;
    end else if (idle) begin
      state_5 <= earlyWinner_5;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_6 <= 1'h0;
    end else if (idle) begin
      state_6 <= earlyWinner_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_7 <= 1'h0;
    end else if (idle) begin
      state_7 <= earlyWinner_7;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_8 <= 1'h0;
    end else if (idle) begin
      state_8 <= earlyWinner_8;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      state_9 <= 1'h0;
    end else if (idle) begin
      state_9 <= earlyWinner_9;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  beatsLeft = _RAND_0[4:0];
  _RAND_1 = {1{`RANDOM}};
  readys_mask = _RAND_1[9:0];
  _RAND_2 = {1{`RANDOM}};
  state_0 = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  state_1 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  state_2 = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  state_3 = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  state_4 = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  state_5 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  state_6 = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  state_7 = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  state_8 = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  state_9 = _RAND_11[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    beatsLeft = 5'h0;
  end
  if (reset) begin
    readys_mask = 10'h3ff;
  end
  if (reset) begin
    state_0 = 1'h0;
  end
  if (reset) begin
    state_1 = 1'h0;
  end
  if (reset) begin
    state_2 = 1'h0;
  end
  if (reset) begin
    state_3 = 1'h0;
  end
  if (reset) begin
    state_4 = 1'h0;
  end
  if (reset) begin
    state_5 = 1'h0;
  end
  if (reset) begin
    state_6 = 1'h0;
  end
  if (reset) begin
    state_7 = 1'h0;
  end
  if (reset) begin
    state_8 = 1'h0;
  end
  if (reset) begin
    state_9 = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
