//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_Scheduler_assert(
  input   clock,
  input   reset,
  input   _T_1200,
  input   _T_1703,
  input   bankedStore_io_side_adr_ready,
  input   abc_mshrs_0_io_status_valid,
  input   abc_mshrs_1_io_status_valid,
  input   abc_mshrs_2_io_status_valid,
  input   abc_mshrs_3_io_status_valid,
  input   abc_mshrs_4_io_status_valid,
  input   abc_mshrs_5_io_status_valid,
  input   abc_mshrs_6_io_status_valid,
  input   abc_mshrs_7_io_status_valid
);
  wire  _T_1697; // @[Scheduler.scala 322:12]
  wire  _T_1698; // @[Scheduler.scala 322:12]
  wire  _T_1815; // @[Scheduler.scala 403:10]
  wire  _T_1816; // @[Scheduler.scala 403:10]
  wire [1:0] _T_2047; // @[Bitwise.scala 47:55]
  wire [1:0] _T_2049; // @[Bitwise.scala 47:55]
  wire [2:0] _T_2051; // @[Bitwise.scala 47:55]
  wire [1:0] _T_2053; // @[Bitwise.scala 47:55]
  wire [1:0] _T_2055; // @[Bitwise.scala 47:55]
  wire [2:0] _T_2057; // @[Bitwise.scala 47:55]
  wire [3:0] abcMshrsUsed; // @[Bitwise.scala 47:55]
  reg [3:0] schedule_d_bits_user_q_fullness; // @[Scheduler.scala 540:45]
  reg [31:0] _RAND_0;
  wire  _T_2061; // @[Scheduler.scala 541:33]
  reg  _T_2062; // @[Scheduler.scala 541:19]
  reg [31:0] _RAND_1;
  wire  _T_2063; // @[Scheduler.scala 541:107]
  wire  _T_2064; // @[Scheduler.scala 541:71]
  wire  _T_2066; // @[Scheduler.scala 541:10]
  wire  _T_2067; // @[Scheduler.scala 541:10]
  wire  _T_2068; // @[Scheduler.scala 542:33]
  reg  _T_2069; // @[Scheduler.scala 542:19]
  reg [31:0] _RAND_2;
  wire  _T_2070; // @[Scheduler.scala 542:107]
  wire  _T_2071; // @[Scheduler.scala 542:71]
  wire  _T_2073; // @[Scheduler.scala 542:10]
  wire  _T_2074; // @[Scheduler.scala 542:10]
  assign _T_1697 = _T_1200 | reset; // @[Scheduler.scala 322:12]
  assign _T_1698 = ~_T_1697; // @[Scheduler.scala 322:12]
  assign _T_1815 = bankedStore_io_side_adr_ready | reset; // @[Scheduler.scala 403:10]
  assign _T_1816 = ~_T_1815; // @[Scheduler.scala 403:10]
  assign _T_2047 = abc_mshrs_0_io_status_valid + abc_mshrs_1_io_status_valid; // @[Bitwise.scala 47:55]
  assign _T_2049 = abc_mshrs_2_io_status_valid + abc_mshrs_3_io_status_valid; // @[Bitwise.scala 47:55]
  assign _T_2051 = _T_2047 + _T_2049; // @[Bitwise.scala 47:55]
  assign _T_2053 = abc_mshrs_4_io_status_valid + abc_mshrs_5_io_status_valid; // @[Bitwise.scala 47:55]
  assign _T_2055 = abc_mshrs_6_io_status_valid + abc_mshrs_7_io_status_valid; // @[Bitwise.scala 47:55]
  assign _T_2057 = _T_2053 + _T_2055; // @[Bitwise.scala 47:55]
  assign abcMshrsUsed = _T_2051 + _T_2057; // @[Bitwise.scala 47:55]
  assign _T_2061 = abcMshrsUsed != 4'h0; // @[Scheduler.scala 541:33]
  assign _T_2063 = schedule_d_bits_user_q_fullness == 4'h0; // @[Scheduler.scala 541:107]
  assign _T_2064 = _T_2062 | _T_2063; // @[Scheduler.scala 541:71]
  assign _T_2066 = _T_2064 | reset; // @[Scheduler.scala 541:10]
  assign _T_2067 = ~_T_2066; // @[Scheduler.scala 541:10]
  assign _T_2068 = abcMshrsUsed != 4'h8; // @[Scheduler.scala 542:33]
  assign _T_2070 = schedule_d_bits_user_q_fullness == 4'hf; // @[Scheduler.scala 542:107]
  assign _T_2071 = _T_2069 | _T_2070; // @[Scheduler.scala 542:71]
  assign _T_2073 = _T_2071 | reset; // @[Scheduler.scala 542:10]
  assign _T_2074 = ~_T_2073; // @[Scheduler.scala 542:10]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  schedule_d_bits_user_q_fullness = _RAND_0[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_2062 = _RAND_1[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_2069 = _RAND_2[0:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      schedule_d_bits_user_q_fullness <= 4'h0;
    end else if (4'h8 == abcMshrsUsed) begin
      schedule_d_bits_user_q_fullness <= 4'hf;
    end else if (4'h7 == abcMshrsUsed) begin
      schedule_d_bits_user_q_fullness <= 4'hd;
    end else if (4'h6 == abcMshrsUsed) begin
      schedule_d_bits_user_q_fullness <= 4'hb;
    end else if (4'h5 == abcMshrsUsed) begin
      schedule_d_bits_user_q_fullness <= 4'h9;
    end else if (4'h4 == abcMshrsUsed) begin
      schedule_d_bits_user_q_fullness <= 4'h7;
    end else if (4'h3 == abcMshrsUsed) begin
      schedule_d_bits_user_q_fullness <= 4'h5;
    end else if (4'h2 == abcMshrsUsed) begin
      schedule_d_bits_user_q_fullness <= 4'h3;
    end else if (4'h1 == abcMshrsUsed) begin
      schedule_d_bits_user_q_fullness <= 4'h1;
    end else begin
      schedule_d_bits_user_q_fullness <= 4'h0;
    end
    if (reset) begin
      _T_2062 <= 1'h0;
    end else begin
      _T_2062 <= _T_2061;
    end
    _T_2069 <= reset | _T_2068;
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1703 & _T_1698) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Scheduler.scala:330 assert (!request.bits.prio(0))\n"); // @[Scheduler.scala 330:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1703 & _T_1698) begin
          $fatal; // @[Scheduler.scala 330:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1816) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Scheduler.scala:403 assert (bankedStore.io.side_adr.ready)\n"); // @[Scheduler.scala 403:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1816) begin
          $fatal; // @[Scheduler.scala 403:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2067) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Scheduler.scala:541 assert ( RegNext(abcMshrsUsed =/= 0.U,                    false.B)  || (schedule.d.bits.user.q_fullness === 0.U )) // boundary check\n"); // @[Scheduler.scala 541:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2067) begin
          $fatal; // @[Scheduler.scala 541:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2074) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Scheduler.scala:542 assert ( RegNext(abcMshrsUsed =/= params.abcMshrs.asUInt, true.B )  || (schedule.d.bits.user.q_fullness === 15.U)) // boundary check\n"); // @[Scheduler.scala 542:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2074) begin
          $fatal; // @[Scheduler.scala 542:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
