//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TraceIngressInner(
  input         clock,
  input         reset,
  input         io_traceOnCore,
  output        io_traceOn,
  input  [4:0]  io_btmEventRational_bits0_reason,
  input  [38:0] io_btmEventRational_bits0_iaddr,
  input  [13:0] io_btmEventRational_bits0_icnt,
  input  [31:0] io_btmEventRational_bits0_hist,
  input  [1:0]  io_btmEventRational_bits0_hisv,
  input  [4:0]  io_btmEventRational_bits1_reason,
  input  [38:0] io_btmEventRational_bits1_iaddr,
  input  [13:0] io_btmEventRational_bits1_icnt,
  input  [31:0] io_btmEventRational_bits1_hist,
  input  [1:0]  io_btmEventRational_bits1_hisv,
  input         io_btmEventRational_valid,
  input  [1:0]  io_btmEventRational_source,
  output        io_btmEventRational_ready,
  output [1:0]  io_btmEventRational_sink,
  input         io_btmEvent_ready,
  output        io_btmEvent_valid,
  output [4:0]  io_btmEvent_bits_reason,
  output [38:0] io_btmEvent_bits_iaddr,
  output [13:0] io_btmEvent_bits_icnt,
  output [31:0] io_btmEvent_bits_hist,
  output [1:0]  io_btmEvent_bits_hisv,
  input  [38:0] io_pcCaptureAsync_mem_0,
  output        io_pcCaptureAsync_ridx,
  input         io_pcCaptureAsync_widx,
  input  [38:0] io_pcSampleAsync_mem_0,
  output        io_pcSampleAsync_ridx,
  input         io_pcSampleAsync_widx,
  output        io_pcCapture_valid,
  output [38:0] io_pcCapture_bits,
  output        io_pcSample_valid,
  output [38:0] io_pcSample_bits
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
`endif // RANDOMIZE_REG_INIT
  wire  rf_reset;
  wire  io_btmEvent_sink_rf_reset; // @[RationalCrossing.scala 167:22]
  wire  io_btmEvent_sink_clock; // @[RationalCrossing.scala 167:22]
  wire  io_btmEvent_sink_reset; // @[RationalCrossing.scala 167:22]
  wire [4:0] io_btmEvent_sink_io_enq_bits0_reason; // @[RationalCrossing.scala 167:22]
  wire [38:0] io_btmEvent_sink_io_enq_bits0_iaddr; // @[RationalCrossing.scala 167:22]
  wire [13:0] io_btmEvent_sink_io_enq_bits0_icnt; // @[RationalCrossing.scala 167:22]
  wire [31:0] io_btmEvent_sink_io_enq_bits0_hist; // @[RationalCrossing.scala 167:22]
  wire [1:0] io_btmEvent_sink_io_enq_bits0_hisv; // @[RationalCrossing.scala 167:22]
  wire [4:0] io_btmEvent_sink_io_enq_bits1_reason; // @[RationalCrossing.scala 167:22]
  wire [38:0] io_btmEvent_sink_io_enq_bits1_iaddr; // @[RationalCrossing.scala 167:22]
  wire [13:0] io_btmEvent_sink_io_enq_bits1_icnt; // @[RationalCrossing.scala 167:22]
  wire [31:0] io_btmEvent_sink_io_enq_bits1_hist; // @[RationalCrossing.scala 167:22]
  wire [1:0] io_btmEvent_sink_io_enq_bits1_hisv; // @[RationalCrossing.scala 167:22]
  wire  io_btmEvent_sink_io_enq_valid; // @[RationalCrossing.scala 167:22]
  wire [1:0] io_btmEvent_sink_io_enq_source; // @[RationalCrossing.scala 167:22]
  wire  io_btmEvent_sink_io_enq_ready; // @[RationalCrossing.scala 167:22]
  wire [1:0] io_btmEvent_sink_io_enq_sink; // @[RationalCrossing.scala 167:22]
  wire  io_btmEvent_sink_io_deq_ready; // @[RationalCrossing.scala 167:22]
  wire  io_btmEvent_sink_io_deq_valid; // @[RationalCrossing.scala 167:22]
  wire [4:0] io_btmEvent_sink_io_deq_bits_reason; // @[RationalCrossing.scala 167:22]
  wire [38:0] io_btmEvent_sink_io_deq_bits_iaddr; // @[RationalCrossing.scala 167:22]
  wire [13:0] io_btmEvent_sink_io_deq_bits_icnt; // @[RationalCrossing.scala 167:22]
  wire [31:0] io_btmEvent_sink_io_deq_bits_hist; // @[RationalCrossing.scala 167:22]
  wire [1:0] io_btmEvent_sink_io_deq_bits_hisv; // @[RationalCrossing.scala 167:22]
  wire  io_pcCapture_sink_rf_reset; // @[AsyncQueue.scala 207:22]
  wire  io_pcCapture_sink_clock; // @[AsyncQueue.scala 207:22]
  wire  io_pcCapture_sink_reset; // @[AsyncQueue.scala 207:22]
  wire  io_pcCapture_sink_io_deq_valid; // @[AsyncQueue.scala 207:22]
  wire [38:0] io_pcCapture_sink_io_deq_bits; // @[AsyncQueue.scala 207:22]
  wire [38:0] io_pcCapture_sink_io_async_mem_0; // @[AsyncQueue.scala 207:22]
  wire  io_pcCapture_sink_io_async_ridx; // @[AsyncQueue.scala 207:22]
  wire  io_pcCapture_sink_io_async_widx; // @[AsyncQueue.scala 207:22]
  wire  io_pcSample_sink_rf_reset; // @[AsyncQueue.scala 207:22]
  wire  io_pcSample_sink_clock; // @[AsyncQueue.scala 207:22]
  wire  io_pcSample_sink_reset; // @[AsyncQueue.scala 207:22]
  wire  io_pcSample_sink_io_deq_valid; // @[AsyncQueue.scala 207:22]
  wire [38:0] io_pcSample_sink_io_deq_bits; // @[AsyncQueue.scala 207:22]
  wire [38:0] io_pcSample_sink_io_async_mem_0; // @[AsyncQueue.scala 207:22]
  wire  io_pcSample_sink_io_async_ridx; // @[AsyncQueue.scala 207:22]
  wire  io_pcSample_sink_io_async_widx; // @[AsyncQueue.scala 207:22]
  reg  io_traceOn_REG; // @[TraceIngress.scala 575:26]
  RHEA__RationalCrossingSink_10 io_btmEvent_sink ( // @[RationalCrossing.scala 167:22]
    .rf_reset(io_btmEvent_sink_rf_reset),
    .clock(io_btmEvent_sink_clock),
    .reset(io_btmEvent_sink_reset),
    .io_enq_bits0_reason(io_btmEvent_sink_io_enq_bits0_reason),
    .io_enq_bits0_iaddr(io_btmEvent_sink_io_enq_bits0_iaddr),
    .io_enq_bits0_icnt(io_btmEvent_sink_io_enq_bits0_icnt),
    .io_enq_bits0_hist(io_btmEvent_sink_io_enq_bits0_hist),
    .io_enq_bits0_hisv(io_btmEvent_sink_io_enq_bits0_hisv),
    .io_enq_bits1_reason(io_btmEvent_sink_io_enq_bits1_reason),
    .io_enq_bits1_iaddr(io_btmEvent_sink_io_enq_bits1_iaddr),
    .io_enq_bits1_icnt(io_btmEvent_sink_io_enq_bits1_icnt),
    .io_enq_bits1_hist(io_btmEvent_sink_io_enq_bits1_hist),
    .io_enq_bits1_hisv(io_btmEvent_sink_io_enq_bits1_hisv),
    .io_enq_valid(io_btmEvent_sink_io_enq_valid),
    .io_enq_source(io_btmEvent_sink_io_enq_source),
    .io_enq_ready(io_btmEvent_sink_io_enq_ready),
    .io_enq_sink(io_btmEvent_sink_io_enq_sink),
    .io_deq_ready(io_btmEvent_sink_io_deq_ready),
    .io_deq_valid(io_btmEvent_sink_io_deq_valid),
    .io_deq_bits_reason(io_btmEvent_sink_io_deq_bits_reason),
    .io_deq_bits_iaddr(io_btmEvent_sink_io_deq_bits_iaddr),
    .io_deq_bits_icnt(io_btmEvent_sink_io_deq_bits_icnt),
    .io_deq_bits_hist(io_btmEvent_sink_io_deq_bits_hist),
    .io_deq_bits_hisv(io_btmEvent_sink_io_deq_bits_hisv)
  );
  RHEA__AsyncQueueSink_3 io_pcCapture_sink ( // @[AsyncQueue.scala 207:22]
    .rf_reset(io_pcCapture_sink_rf_reset),
    .clock(io_pcCapture_sink_clock),
    .reset(io_pcCapture_sink_reset),
    .io_deq_valid(io_pcCapture_sink_io_deq_valid),
    .io_deq_bits(io_pcCapture_sink_io_deq_bits),
    .io_async_mem_0(io_pcCapture_sink_io_async_mem_0),
    .io_async_ridx(io_pcCapture_sink_io_async_ridx),
    .io_async_widx(io_pcCapture_sink_io_async_widx)
  );
  RHEA__AsyncQueueSink_3 io_pcSample_sink ( // @[AsyncQueue.scala 207:22]
    .rf_reset(io_pcSample_sink_rf_reset),
    .clock(io_pcSample_sink_clock),
    .reset(io_pcSample_sink_reset),
    .io_deq_valid(io_pcSample_sink_io_deq_valid),
    .io_deq_bits(io_pcSample_sink_io_deq_bits),
    .io_async_mem_0(io_pcSample_sink_io_async_mem_0),
    .io_async_ridx(io_pcSample_sink_io_async_ridx),
    .io_async_widx(io_pcSample_sink_io_async_widx)
  );
  assign io_btmEvent_sink_rf_reset = rf_reset;
  assign io_pcCapture_sink_rf_reset = rf_reset;
  assign io_pcSample_sink_rf_reset = rf_reset;
  assign rf_reset = reset;
  assign io_traceOn = io_traceOn_REG; // @[TraceIngress.scala 575:16]
  assign io_btmEventRational_ready = io_btmEvent_sink_io_enq_ready; // @[RationalCrossing.scala 168:17]
  assign io_btmEventRational_sink = io_btmEvent_sink_io_enq_sink; // @[RationalCrossing.scala 168:17]
  assign io_btmEvent_valid = io_btmEvent_sink_io_deq_valid; // @[TraceIngress.scala 589:21]
  assign io_btmEvent_bits_reason = io_btmEvent_sink_io_deq_bits_reason; // @[TraceIngress.scala 589:21]
  assign io_btmEvent_bits_iaddr = io_btmEvent_sink_io_deq_bits_iaddr; // @[TraceIngress.scala 589:21]
  assign io_btmEvent_bits_icnt = io_btmEvent_sink_io_deq_bits_icnt; // @[TraceIngress.scala 589:21]
  assign io_btmEvent_bits_hist = io_btmEvent_sink_io_deq_bits_hist; // @[TraceIngress.scala 589:21]
  assign io_btmEvent_bits_hisv = io_btmEvent_sink_io_deq_bits_hisv; // @[TraceIngress.scala 589:21]
  assign io_pcCaptureAsync_ridx = io_pcCapture_sink_io_async_ridx; // @[AsyncQueue.scala 208:19]
  assign io_pcSampleAsync_ridx = io_pcSample_sink_io_async_ridx; // @[AsyncQueue.scala 208:19]
  assign io_pcCapture_valid = io_pcCapture_sink_io_deq_valid; // @[TraceIngress.scala 597:22]
  assign io_pcCapture_bits = io_pcCapture_sink_io_deq_bits; // @[TraceIngress.scala 597:22]
  assign io_pcSample_valid = io_pcSample_sink_io_deq_valid; // @[TraceIngress.scala 598:22]
  assign io_pcSample_bits = io_pcSample_sink_io_deq_bits; // @[TraceIngress.scala 598:22]
  assign io_btmEvent_sink_clock = clock;
  assign io_btmEvent_sink_reset = reset;
  assign io_btmEvent_sink_io_enq_bits0_reason = io_btmEventRational_bits0_reason; // @[RationalCrossing.scala 168:17]
  assign io_btmEvent_sink_io_enq_bits0_iaddr = io_btmEventRational_bits0_iaddr; // @[RationalCrossing.scala 168:17]
  assign io_btmEvent_sink_io_enq_bits0_icnt = io_btmEventRational_bits0_icnt; // @[RationalCrossing.scala 168:17]
  assign io_btmEvent_sink_io_enq_bits0_hist = io_btmEventRational_bits0_hist; // @[RationalCrossing.scala 168:17]
  assign io_btmEvent_sink_io_enq_bits0_hisv = io_btmEventRational_bits0_hisv; // @[RationalCrossing.scala 168:17]
  assign io_btmEvent_sink_io_enq_bits1_reason = io_btmEventRational_bits1_reason; // @[RationalCrossing.scala 168:17]
  assign io_btmEvent_sink_io_enq_bits1_iaddr = io_btmEventRational_bits1_iaddr; // @[RationalCrossing.scala 168:17]
  assign io_btmEvent_sink_io_enq_bits1_icnt = io_btmEventRational_bits1_icnt; // @[RationalCrossing.scala 168:17]
  assign io_btmEvent_sink_io_enq_bits1_hist = io_btmEventRational_bits1_hist; // @[RationalCrossing.scala 168:17]
  assign io_btmEvent_sink_io_enq_bits1_hisv = io_btmEventRational_bits1_hisv; // @[RationalCrossing.scala 168:17]
  assign io_btmEvent_sink_io_enq_valid = io_btmEventRational_valid; // @[RationalCrossing.scala 168:17]
  assign io_btmEvent_sink_io_enq_source = io_btmEventRational_source; // @[RationalCrossing.scala 168:17]
  assign io_btmEvent_sink_io_deq_ready = io_btmEvent_ready; // @[TraceIngress.scala 589:21]
  assign io_pcCapture_sink_clock = clock;
  assign io_pcCapture_sink_reset = reset;
  assign io_pcCapture_sink_io_async_mem_0 = io_pcCaptureAsync_mem_0; // @[AsyncQueue.scala 208:19]
  assign io_pcCapture_sink_io_async_widx = io_pcCaptureAsync_widx; // @[AsyncQueue.scala 208:19]
  assign io_pcSample_sink_clock = clock;
  assign io_pcSample_sink_reset = reset;
  assign io_pcSample_sink_io_async_mem_0 = io_pcSampleAsync_mem_0; // @[AsyncQueue.scala 208:19]
  assign io_pcSample_sink_io_async_widx = io_pcSampleAsync_widx; // @[AsyncQueue.scala 208:19]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      io_traceOn_REG <= 1'h0;
    end else begin
      io_traceOn_REG <= io_traceOnCore;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  io_traceOn_REG = _RAND_0[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    io_traceOn_REG = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
