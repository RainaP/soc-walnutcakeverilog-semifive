//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_34_assert(
  input         clock,
  input         reset,
  input         io_in_a_ready,
  input         io_in_a_valid,
  input  [2:0]  io_in_a_bits_opcode,
  input  [2:0]  io_in_a_bits_param,
  input  [2:0]  io_in_a_bits_size,
  input  [3:0]  io_in_a_bits_source,
  input  [35:0] io_in_a_bits_address,
  input  [15:0] io_in_a_bits_mask,
  input         io_in_a_bits_corrupt,
  input         io_in_d_ready,
  input         io_in_d_valid,
  input  [2:0]  io_in_d_bits_opcode,
  input  [2:0]  io_in_d_bits_size,
  input  [3:0]  io_in_d_bits_source,
  input         io_in_d_bits_denied,
  input         io_in_d_bits_corrupt
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire [12:0] _T_14; // @[package.scala 189:77]
  wire [5:0] _T_16; // @[package.scala 189:46]
  wire [35:0] _GEN_18; // @[Edges.scala 22:16]
  wire [35:0] _T_17; // @[Edges.scala 22:16]
  wire  _T_18; // @[Edges.scala 22:24]
  wire [3:0] _T_19; // @[Misc.scala 201:34]
  wire [3:0] _T_21; // @[OneHot.scala 65:12]
  wire [3:0] _T_23; // @[Misc.scala 201:81]
  wire  _T_24; // @[Misc.scala 205:21]
  wire  _T_27; // @[Misc.scala 210:20]
  wire  _T_29; // @[Misc.scala 214:38]
  wire  _T_30; // @[Misc.scala 214:29]
  wire  _T_32; // @[Misc.scala 214:38]
  wire  _T_33; // @[Misc.scala 214:29]
  wire  _T_36; // @[Misc.scala 210:20]
  wire  _T_37; // @[Misc.scala 213:27]
  wire  _T_38; // @[Misc.scala 214:38]
  wire  _T_39; // @[Misc.scala 214:29]
  wire  _T_40; // @[Misc.scala 213:27]
  wire  _T_41; // @[Misc.scala 214:38]
  wire  _T_42; // @[Misc.scala 214:29]
  wire  _T_43; // @[Misc.scala 213:27]
  wire  _T_44; // @[Misc.scala 214:38]
  wire  _T_45; // @[Misc.scala 214:29]
  wire  _T_46; // @[Misc.scala 213:27]
  wire  _T_47; // @[Misc.scala 214:38]
  wire  _T_48; // @[Misc.scala 214:29]
  wire  _T_51; // @[Misc.scala 210:20]
  wire  _T_52; // @[Misc.scala 213:27]
  wire  _T_53; // @[Misc.scala 214:38]
  wire  _T_54; // @[Misc.scala 214:29]
  wire  _T_55; // @[Misc.scala 213:27]
  wire  _T_56; // @[Misc.scala 214:38]
  wire  _T_57; // @[Misc.scala 214:29]
  wire  _T_58; // @[Misc.scala 213:27]
  wire  _T_59; // @[Misc.scala 214:38]
  wire  _T_60; // @[Misc.scala 214:29]
  wire  _T_61; // @[Misc.scala 213:27]
  wire  _T_62; // @[Misc.scala 214:38]
  wire  _T_63; // @[Misc.scala 214:29]
  wire  _T_64; // @[Misc.scala 213:27]
  wire  _T_65; // @[Misc.scala 214:38]
  wire  _T_66; // @[Misc.scala 214:29]
  wire  _T_67; // @[Misc.scala 213:27]
  wire  _T_68; // @[Misc.scala 214:38]
  wire  _T_69; // @[Misc.scala 214:29]
  wire  _T_70; // @[Misc.scala 213:27]
  wire  _T_71; // @[Misc.scala 214:38]
  wire  _T_72; // @[Misc.scala 214:29]
  wire  _T_73; // @[Misc.scala 213:27]
  wire  _T_74; // @[Misc.scala 214:38]
  wire  _T_75; // @[Misc.scala 214:29]
  wire  _T_78; // @[Misc.scala 210:20]
  wire  _T_79; // @[Misc.scala 213:27]
  wire  _T_80; // @[Misc.scala 214:38]
  wire  _T_81; // @[Misc.scala 214:29]
  wire  _T_82; // @[Misc.scala 213:27]
  wire  _T_83; // @[Misc.scala 214:38]
  wire  _T_84; // @[Misc.scala 214:29]
  wire  _T_85; // @[Misc.scala 213:27]
  wire  _T_86; // @[Misc.scala 214:38]
  wire  _T_87; // @[Misc.scala 214:29]
  wire  _T_88; // @[Misc.scala 213:27]
  wire  _T_89; // @[Misc.scala 214:38]
  wire  _T_90; // @[Misc.scala 214:29]
  wire  _T_91; // @[Misc.scala 213:27]
  wire  _T_92; // @[Misc.scala 214:38]
  wire  _T_93; // @[Misc.scala 214:29]
  wire  _T_94; // @[Misc.scala 213:27]
  wire  _T_95; // @[Misc.scala 214:38]
  wire  _T_96; // @[Misc.scala 214:29]
  wire  _T_97; // @[Misc.scala 213:27]
  wire  _T_98; // @[Misc.scala 214:38]
  wire  _T_99; // @[Misc.scala 214:29]
  wire  _T_100; // @[Misc.scala 213:27]
  wire  _T_101; // @[Misc.scala 214:38]
  wire  _T_102; // @[Misc.scala 214:29]
  wire  _T_103; // @[Misc.scala 213:27]
  wire  _T_104; // @[Misc.scala 214:38]
  wire  _T_105; // @[Misc.scala 214:29]
  wire  _T_106; // @[Misc.scala 213:27]
  wire  _T_107; // @[Misc.scala 214:38]
  wire  _T_108; // @[Misc.scala 214:29]
  wire  _T_109; // @[Misc.scala 213:27]
  wire  _T_110; // @[Misc.scala 214:38]
  wire  _T_111; // @[Misc.scala 214:29]
  wire  _T_112; // @[Misc.scala 213:27]
  wire  _T_113; // @[Misc.scala 214:38]
  wire  _T_114; // @[Misc.scala 214:29]
  wire  _T_115; // @[Misc.scala 213:27]
  wire  _T_116; // @[Misc.scala 214:38]
  wire  _T_117; // @[Misc.scala 214:29]
  wire  _T_118; // @[Misc.scala 213:27]
  wire  _T_119; // @[Misc.scala 214:38]
  wire  _T_120; // @[Misc.scala 214:29]
  wire  _T_121; // @[Misc.scala 213:27]
  wire  _T_122; // @[Misc.scala 214:38]
  wire  _T_123; // @[Misc.scala 214:29]
  wire  _T_124; // @[Misc.scala 213:27]
  wire  _T_125; // @[Misc.scala 214:38]
  wire  _T_126; // @[Misc.scala 214:29]
  wire [7:0] _T_133; // @[Cat.scala 29:58]
  wire [15:0] _T_141; // @[Cat.scala 29:58]
  wire  _T_160; // @[Monitor.scala 79:25]
  wire [35:0] _T_162; // @[Parameters.scala 137:31]
  wire [36:0] _T_163; // @[Parameters.scala 137:49]
  wire [36:0] _T_165; // @[Parameters.scala 137:52]
  wire  _T_166; // @[Parameters.scala 137:67]
  wire  _T_171; // @[Monitor.scala 44:11]
  wire  _T_180; // @[Monitor.scala 44:11]
  wire  _T_181; // @[Monitor.scala 44:11]
  wire  _T_183; // @[Monitor.scala 44:11]
  wire  _T_184; // @[Monitor.scala 44:11]
  wire  _T_185; // @[Bundles.scala 110:27]
  wire  _T_187; // @[Monitor.scala 44:11]
  wire  _T_188; // @[Monitor.scala 44:11]
  wire [15:0] _T_189; // @[Monitor.scala 86:18]
  wire  _T_190; // @[Monitor.scala 86:31]
  wire  _T_192; // @[Monitor.scala 44:11]
  wire  _T_193; // @[Monitor.scala 44:11]
  wire  _T_194; // @[Monitor.scala 87:18]
  wire  _T_196; // @[Monitor.scala 44:11]
  wire  _T_197; // @[Monitor.scala 44:11]
  wire  _T_198; // @[Monitor.scala 90:25]
  wire  _T_227; // @[Monitor.scala 97:31]
  wire  _T_229; // @[Monitor.scala 44:11]
  wire  _T_230; // @[Monitor.scala 44:11]
  wire  _T_240; // @[Monitor.scala 102:25]
  wire  _T_242; // @[Parameters.scala 93:42]
  wire  _T_250; // @[Parameters.scala 551:56]
  wire  _T_253; // @[Monitor.scala 44:11]
  wire  _T_254; // @[Monitor.scala 44:11]
  wire  _T_261; // @[Monitor.scala 106:31]
  wire  _T_263; // @[Monitor.scala 44:11]
  wire  _T_264; // @[Monitor.scala 44:11]
  wire  _T_265; // @[Monitor.scala 107:30]
  wire  _T_267; // @[Monitor.scala 44:11]
  wire  _T_268; // @[Monitor.scala 44:11]
  wire  _T_273; // @[Monitor.scala 111:25]
  wire  _T_302; // @[Monitor.scala 119:25]
  wire [15:0] _T_327; // @[Monitor.scala 124:33]
  wire [15:0] _T_328; // @[Monitor.scala 124:31]
  wire  _T_329; // @[Monitor.scala 124:40]
  wire  _T_331; // @[Monitor.scala 44:11]
  wire  _T_332; // @[Monitor.scala 44:11]
  wire  _T_333; // @[Monitor.scala 127:25]
  wire  _T_351; // @[Bundles.scala 140:33]
  wire  _T_353; // @[Monitor.scala 44:11]
  wire  _T_354; // @[Monitor.scala 44:11]
  wire  _T_359; // @[Monitor.scala 135:25]
  wire  _T_377; // @[Bundles.scala 147:30]
  wire  _T_379; // @[Monitor.scala 44:11]
  wire  _T_380; // @[Monitor.scala 44:11]
  wire  _T_385; // @[Monitor.scala 143:25]
  wire  _T_403; // @[Bundles.scala 160:28]
  wire  _T_405; // @[Monitor.scala 44:11]
  wire  _T_406; // @[Monitor.scala 44:11]
  wire  _T_415; // @[Bundles.scala 44:24]
  wire  _T_417; // @[Monitor.scala 51:11]
  wire  _T_418; // @[Monitor.scala 51:11]
  wire  _T_429; // @[Monitor.scala 307:25]
  wire  _T_433; // @[Monitor.scala 309:27]
  wire  _T_435; // @[Monitor.scala 51:11]
  wire  _T_436; // @[Monitor.scala 51:11]
  wire  _T_441; // @[Monitor.scala 311:15]
  wire  _T_443; // @[Monitor.scala 51:11]
  wire  _T_444; // @[Monitor.scala 51:11]
  wire  _T_445; // @[Monitor.scala 312:15]
  wire  _T_447; // @[Monitor.scala 51:11]
  wire  _T_448; // @[Monitor.scala 51:11]
  wire  _T_449; // @[Monitor.scala 315:25]
  wire  _T_477; // @[Monitor.scala 325:25]
  wire  _T_497; // @[Monitor.scala 331:30]
  wire  _T_499; // @[Monitor.scala 51:11]
  wire  _T_500; // @[Monitor.scala 51:11]
  wire  _T_506; // @[Monitor.scala 335:25]
  wire  _T_523; // @[Monitor.scala 343:25]
  wire  _T_541; // @[Monitor.scala 351:25]
  wire  _T_573; // @[Decoupled.scala 40:37]
  wire  _T_580; // @[Edges.scala 93:28]
  reg [1:0] _T_582; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire [1:0] _T_584; // @[Edges.scala 231:28]
  wire  _T_585; // @[Edges.scala 232:25]
  reg [2:0] _T_593; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [2:0] _T_594; // @[Monitor.scala 382:22]
  reg [31:0] _RAND_2;
  reg [2:0] _T_595; // @[Monitor.scala 383:22]
  reg [31:0] _RAND_3;
  reg [3:0] _T_596; // @[Monitor.scala 384:22]
  reg [31:0] _RAND_4;
  reg [35:0] _T_597; // @[Monitor.scala 385:22]
  reg [63:0] _RAND_5;
  wire  _T_598; // @[Monitor.scala 386:22]
  wire  _T_599; // @[Monitor.scala 386:19]
  wire  _T_600; // @[Monitor.scala 387:32]
  wire  _T_602; // @[Monitor.scala 44:11]
  wire  _T_603; // @[Monitor.scala 44:11]
  wire  _T_604; // @[Monitor.scala 388:32]
  wire  _T_606; // @[Monitor.scala 44:11]
  wire  _T_607; // @[Monitor.scala 44:11]
  wire  _T_608; // @[Monitor.scala 389:32]
  wire  _T_610; // @[Monitor.scala 44:11]
  wire  _T_611; // @[Monitor.scala 44:11]
  wire  _T_612; // @[Monitor.scala 390:32]
  wire  _T_614; // @[Monitor.scala 44:11]
  wire  _T_615; // @[Monitor.scala 44:11]
  wire  _T_616; // @[Monitor.scala 391:32]
  wire  _T_618; // @[Monitor.scala 44:11]
  wire  _T_619; // @[Monitor.scala 44:11]
  wire  _T_621; // @[Monitor.scala 393:20]
  wire  _T_622; // @[Decoupled.scala 40:37]
  wire [12:0] _T_624; // @[package.scala 189:77]
  wire [5:0] _T_626; // @[package.scala 189:46]
  reg [1:0] _T_630; // @[Edges.scala 230:27]
  reg [31:0] _RAND_6;
  wire [1:0] _T_632; // @[Edges.scala 231:28]
  wire  _T_633; // @[Edges.scala 232:25]
  reg [2:0] _T_641; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_7;
  reg [2:0] _T_643; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_8;
  reg [3:0] _T_644; // @[Monitor.scala 535:22]
  reg [31:0] _RAND_9;
  reg  _T_646; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_10;
  wire  _T_647; // @[Monitor.scala 538:22]
  wire  _T_648; // @[Monitor.scala 538:19]
  wire  _T_649; // @[Monitor.scala 539:29]
  wire  _T_651; // @[Monitor.scala 51:11]
  wire  _T_652; // @[Monitor.scala 51:11]
  wire  _T_657; // @[Monitor.scala 541:29]
  wire  _T_659; // @[Monitor.scala 51:11]
  wire  _T_660; // @[Monitor.scala 51:11]
  wire  _T_661; // @[Monitor.scala 542:29]
  wire  _T_663; // @[Monitor.scala 51:11]
  wire  _T_664; // @[Monitor.scala 51:11]
  wire  _T_669; // @[Monitor.scala 544:29]
  wire  _T_671; // @[Monitor.scala 51:11]
  wire  _T_672; // @[Monitor.scala 51:11]
  wire  _T_674; // @[Monitor.scala 546:20]
  reg [15:0] _T_675; // @[Monitor.scala 568:27]
  reg [31:0] _RAND_11;
  reg [1:0] _T_685; // @[Edges.scala 230:27]
  reg [31:0] _RAND_12;
  wire [1:0] _T_687; // @[Edges.scala 231:28]
  wire  _T_688; // @[Edges.scala 232:25]
  reg [1:0] _T_704; // @[Edges.scala 230:27]
  reg [31:0] _RAND_13;
  wire [1:0] _T_706; // @[Edges.scala 231:28]
  wire  _T_707; // @[Edges.scala 232:25]
  wire  _T_717; // @[Monitor.scala 574:27]
  wire [15:0] _T_719; // @[OneHot.scala 58:35]
  wire [15:0] _T_720; // @[Monitor.scala 576:23]
  wire  _T_722; // @[Monitor.scala 576:14]
  wire  _T_724; // @[Monitor.scala 576:13]
  wire  _T_725; // @[Monitor.scala 576:13]
  wire [15:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_729; // @[Monitor.scala 581:27]
  wire  _T_731; // @[Monitor.scala 581:75]
  wire  _T_732; // @[Monitor.scala 581:72]
  wire [15:0] _T_733; // @[OneHot.scala 58:35]
  wire [15:0] _T_734; // @[Monitor.scala 583:21]
  wire [15:0] _T_735; // @[Monitor.scala 583:32]
  wire  _T_738; // @[Monitor.scala 51:11]
  wire  _T_739; // @[Monitor.scala 51:11]
  wire [15:0] _GEN_16; // @[Monitor.scala 581:91]
  wire  _T_740; // @[Monitor.scala 587:20]
  wire  _T_741; // @[Monitor.scala 587:40]
  wire  _T_742; // @[Monitor.scala 587:33]
  wire  _T_743; // @[Monitor.scala 587:30]
  wire  _T_745; // @[Monitor.scala 51:11]
  wire  _T_746; // @[Monitor.scala 51:11]
  wire [15:0] _T_747; // @[Monitor.scala 590:27]
  wire [15:0] _T_748; // @[Monitor.scala 590:38]
  wire [15:0] _T_749; // @[Monitor.scala 590:36]
  reg [31:0] _T_750; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_14;
  wire  _T_751; // @[Monitor.scala 595:23]
  wire  _T_752; // @[Monitor.scala 595:13]
  wire  _T_753; // @[Monitor.scala 595:36]
  wire  _T_754; // @[Monitor.scala 595:27]
  wire  _T_755; // @[Monitor.scala 595:56]
  wire  _T_756; // @[Monitor.scala 595:44]
  wire  _T_758; // @[Monitor.scala 595:12]
  wire  _T_759; // @[Monitor.scala 595:12]
  wire [31:0] _T_761; // @[Monitor.scala 597:26]
  wire  _T_764; // @[Monitor.scala 598:27]
  wire  _GEN_19; // @[Monitor.scala 44:11]
  wire  _GEN_33; // @[Monitor.scala 44:11]
  wire  _GEN_49; // @[Monitor.scala 44:11]
  wire  _GEN_59; // @[Monitor.scala 44:11]
  wire  _GEN_67; // @[Monitor.scala 44:11]
  wire  _GEN_75; // @[Monitor.scala 44:11]
  wire  _GEN_83; // @[Monitor.scala 44:11]
  wire  _GEN_91; // @[Monitor.scala 44:11]
  wire  _GEN_101; // @[Monitor.scala 51:11]
  wire  _GEN_107; // @[Monitor.scala 51:11]
  wire  _GEN_113; // @[Monitor.scala 51:11]
  wire  _GEN_119; // @[Monitor.scala 51:11]
  wire  _GEN_121; // @[Monitor.scala 51:11]
  wire  _GEN_123; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_14 = 13'h3f << io_in_a_bits_size; // @[package.scala 189:77]
  assign _T_16 = ~_T_14[5:0]; // @[package.scala 189:46]
  assign _GEN_18 = {{30'd0}, _T_16}; // @[Edges.scala 22:16]
  assign _T_17 = io_in_a_bits_address & _GEN_18; // @[Edges.scala 22:16]
  assign _T_18 = _T_17 == 36'h0; // @[Edges.scala 22:24]
  assign _T_19 = {{1'd0}, io_in_a_bits_size}; // @[Misc.scala 201:34]
  assign _T_21 = 4'h1 << _T_19[1:0]; // @[OneHot.scala 65:12]
  assign _T_23 = _T_21 | 4'h1; // @[Misc.scala 201:81]
  assign _T_24 = io_in_a_bits_size >= 3'h4; // @[Misc.scala 205:21]
  assign _T_27 = ~io_in_a_bits_address[3]; // @[Misc.scala 210:20]
  assign _T_29 = _T_23[3] & _T_27; // @[Misc.scala 214:38]
  assign _T_30 = _T_24 | _T_29; // @[Misc.scala 214:29]
  assign _T_32 = _T_23[3] & io_in_a_bits_address[3]; // @[Misc.scala 214:38]
  assign _T_33 = _T_24 | _T_32; // @[Misc.scala 214:29]
  assign _T_36 = ~io_in_a_bits_address[2]; // @[Misc.scala 210:20]
  assign _T_37 = _T_27 & _T_36; // @[Misc.scala 213:27]
  assign _T_38 = _T_23[2] & _T_37; // @[Misc.scala 214:38]
  assign _T_39 = _T_30 | _T_38; // @[Misc.scala 214:29]
  assign _T_40 = _T_27 & io_in_a_bits_address[2]; // @[Misc.scala 213:27]
  assign _T_41 = _T_23[2] & _T_40; // @[Misc.scala 214:38]
  assign _T_42 = _T_30 | _T_41; // @[Misc.scala 214:29]
  assign _T_43 = io_in_a_bits_address[3] & _T_36; // @[Misc.scala 213:27]
  assign _T_44 = _T_23[2] & _T_43; // @[Misc.scala 214:38]
  assign _T_45 = _T_33 | _T_44; // @[Misc.scala 214:29]
  assign _T_46 = io_in_a_bits_address[3] & io_in_a_bits_address[2]; // @[Misc.scala 213:27]
  assign _T_47 = _T_23[2] & _T_46; // @[Misc.scala 214:38]
  assign _T_48 = _T_33 | _T_47; // @[Misc.scala 214:29]
  assign _T_51 = ~io_in_a_bits_address[1]; // @[Misc.scala 210:20]
  assign _T_52 = _T_37 & _T_51; // @[Misc.scala 213:27]
  assign _T_53 = _T_23[1] & _T_52; // @[Misc.scala 214:38]
  assign _T_54 = _T_39 | _T_53; // @[Misc.scala 214:29]
  assign _T_55 = _T_37 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_56 = _T_23[1] & _T_55; // @[Misc.scala 214:38]
  assign _T_57 = _T_39 | _T_56; // @[Misc.scala 214:29]
  assign _T_58 = _T_40 & _T_51; // @[Misc.scala 213:27]
  assign _T_59 = _T_23[1] & _T_58; // @[Misc.scala 214:38]
  assign _T_60 = _T_42 | _T_59; // @[Misc.scala 214:29]
  assign _T_61 = _T_40 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_62 = _T_23[1] & _T_61; // @[Misc.scala 214:38]
  assign _T_63 = _T_42 | _T_62; // @[Misc.scala 214:29]
  assign _T_64 = _T_43 & _T_51; // @[Misc.scala 213:27]
  assign _T_65 = _T_23[1] & _T_64; // @[Misc.scala 214:38]
  assign _T_66 = _T_45 | _T_65; // @[Misc.scala 214:29]
  assign _T_67 = _T_43 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_68 = _T_23[1] & _T_67; // @[Misc.scala 214:38]
  assign _T_69 = _T_45 | _T_68; // @[Misc.scala 214:29]
  assign _T_70 = _T_46 & _T_51; // @[Misc.scala 213:27]
  assign _T_71 = _T_23[1] & _T_70; // @[Misc.scala 214:38]
  assign _T_72 = _T_48 | _T_71; // @[Misc.scala 214:29]
  assign _T_73 = _T_46 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_74 = _T_23[1] & _T_73; // @[Misc.scala 214:38]
  assign _T_75 = _T_48 | _T_74; // @[Misc.scala 214:29]
  assign _T_78 = ~io_in_a_bits_address[0]; // @[Misc.scala 210:20]
  assign _T_79 = _T_52 & _T_78; // @[Misc.scala 213:27]
  assign _T_80 = _T_23[0] & _T_79; // @[Misc.scala 214:38]
  assign _T_81 = _T_54 | _T_80; // @[Misc.scala 214:29]
  assign _T_82 = _T_52 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_83 = _T_23[0] & _T_82; // @[Misc.scala 214:38]
  assign _T_84 = _T_54 | _T_83; // @[Misc.scala 214:29]
  assign _T_85 = _T_55 & _T_78; // @[Misc.scala 213:27]
  assign _T_86 = _T_23[0] & _T_85; // @[Misc.scala 214:38]
  assign _T_87 = _T_57 | _T_86; // @[Misc.scala 214:29]
  assign _T_88 = _T_55 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_89 = _T_23[0] & _T_88; // @[Misc.scala 214:38]
  assign _T_90 = _T_57 | _T_89; // @[Misc.scala 214:29]
  assign _T_91 = _T_58 & _T_78; // @[Misc.scala 213:27]
  assign _T_92 = _T_23[0] & _T_91; // @[Misc.scala 214:38]
  assign _T_93 = _T_60 | _T_92; // @[Misc.scala 214:29]
  assign _T_94 = _T_58 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_95 = _T_23[0] & _T_94; // @[Misc.scala 214:38]
  assign _T_96 = _T_60 | _T_95; // @[Misc.scala 214:29]
  assign _T_97 = _T_61 & _T_78; // @[Misc.scala 213:27]
  assign _T_98 = _T_23[0] & _T_97; // @[Misc.scala 214:38]
  assign _T_99 = _T_63 | _T_98; // @[Misc.scala 214:29]
  assign _T_100 = _T_61 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_101 = _T_23[0] & _T_100; // @[Misc.scala 214:38]
  assign _T_102 = _T_63 | _T_101; // @[Misc.scala 214:29]
  assign _T_103 = _T_64 & _T_78; // @[Misc.scala 213:27]
  assign _T_104 = _T_23[0] & _T_103; // @[Misc.scala 214:38]
  assign _T_105 = _T_66 | _T_104; // @[Misc.scala 214:29]
  assign _T_106 = _T_64 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_107 = _T_23[0] & _T_106; // @[Misc.scala 214:38]
  assign _T_108 = _T_66 | _T_107; // @[Misc.scala 214:29]
  assign _T_109 = _T_67 & _T_78; // @[Misc.scala 213:27]
  assign _T_110 = _T_23[0] & _T_109; // @[Misc.scala 214:38]
  assign _T_111 = _T_69 | _T_110; // @[Misc.scala 214:29]
  assign _T_112 = _T_67 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_113 = _T_23[0] & _T_112; // @[Misc.scala 214:38]
  assign _T_114 = _T_69 | _T_113; // @[Misc.scala 214:29]
  assign _T_115 = _T_70 & _T_78; // @[Misc.scala 213:27]
  assign _T_116 = _T_23[0] & _T_115; // @[Misc.scala 214:38]
  assign _T_117 = _T_72 | _T_116; // @[Misc.scala 214:29]
  assign _T_118 = _T_70 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_119 = _T_23[0] & _T_118; // @[Misc.scala 214:38]
  assign _T_120 = _T_72 | _T_119; // @[Misc.scala 214:29]
  assign _T_121 = _T_73 & _T_78; // @[Misc.scala 213:27]
  assign _T_122 = _T_23[0] & _T_121; // @[Misc.scala 214:38]
  assign _T_123 = _T_75 | _T_122; // @[Misc.scala 214:29]
  assign _T_124 = _T_73 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_125 = _T_23[0] & _T_124; // @[Misc.scala 214:38]
  assign _T_126 = _T_75 | _T_125; // @[Misc.scala 214:29]
  assign _T_133 = {_T_102,_T_99,_T_96,_T_93,_T_90,_T_87,_T_84,_T_81}; // @[Cat.scala 29:58]
  assign _T_141 = {_T_126,_T_123,_T_120,_T_117,_T_114,_T_111,_T_108,_T_105,_T_133}; // @[Cat.scala 29:58]
  assign _T_160 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_162 = io_in_a_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  assign _T_163 = {1'b0,$signed(_T_162)}; // @[Parameters.scala 137:49]
  assign _T_165 = $signed(_T_163) & -37'sh800000000; // @[Parameters.scala 137:52]
  assign _T_166 = $signed(_T_165) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_171 = ~reset; // @[Monitor.scala 44:11]
  assign _T_180 = _T_24 | reset; // @[Monitor.scala 44:11]
  assign _T_181 = ~_T_180; // @[Monitor.scala 44:11]
  assign _T_183 = _T_18 | reset; // @[Monitor.scala 44:11]
  assign _T_184 = ~_T_183; // @[Monitor.scala 44:11]
  assign _T_185 = io_in_a_bits_param <= 3'h2; // @[Bundles.scala 110:27]
  assign _T_187 = _T_185 | reset; // @[Monitor.scala 44:11]
  assign _T_188 = ~_T_187; // @[Monitor.scala 44:11]
  assign _T_189 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_190 = _T_189 == 16'h0; // @[Monitor.scala 86:31]
  assign _T_192 = _T_190 | reset; // @[Monitor.scala 44:11]
  assign _T_193 = ~_T_192; // @[Monitor.scala 44:11]
  assign _T_194 = ~io_in_a_bits_corrupt; // @[Monitor.scala 87:18]
  assign _T_196 = _T_194 | reset; // @[Monitor.scala 44:11]
  assign _T_197 = ~_T_196; // @[Monitor.scala 44:11]
  assign _T_198 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_227 = io_in_a_bits_param != 3'h0; // @[Monitor.scala 97:31]
  assign _T_229 = _T_227 | reset; // @[Monitor.scala 44:11]
  assign _T_230 = ~_T_229; // @[Monitor.scala 44:11]
  assign _T_240 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_242 = io_in_a_bits_size <= 3'h6; // @[Parameters.scala 93:42]
  assign _T_250 = _T_242 & _T_166; // @[Parameters.scala 551:56]
  assign _T_253 = _T_250 | reset; // @[Monitor.scala 44:11]
  assign _T_254 = ~_T_253; // @[Monitor.scala 44:11]
  assign _T_261 = io_in_a_bits_param == 3'h0; // @[Monitor.scala 106:31]
  assign _T_263 = _T_261 | reset; // @[Monitor.scala 44:11]
  assign _T_264 = ~_T_263; // @[Monitor.scala 44:11]
  assign _T_265 = io_in_a_bits_mask == _T_141; // @[Monitor.scala 107:30]
  assign _T_267 = _T_265 | reset; // @[Monitor.scala 44:11]
  assign _T_268 = ~_T_267; // @[Monitor.scala 44:11]
  assign _T_273 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_302 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_327 = ~_T_141; // @[Monitor.scala 124:33]
  assign _T_328 = io_in_a_bits_mask & _T_327; // @[Monitor.scala 124:31]
  assign _T_329 = _T_328 == 16'h0; // @[Monitor.scala 124:40]
  assign _T_331 = _T_329 | reset; // @[Monitor.scala 44:11]
  assign _T_332 = ~_T_331; // @[Monitor.scala 44:11]
  assign _T_333 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_351 = io_in_a_bits_param <= 3'h4; // @[Bundles.scala 140:33]
  assign _T_353 = _T_351 | reset; // @[Monitor.scala 44:11]
  assign _T_354 = ~_T_353; // @[Monitor.scala 44:11]
  assign _T_359 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_377 = io_in_a_bits_param <= 3'h3; // @[Bundles.scala 147:30]
  assign _T_379 = _T_377 | reset; // @[Monitor.scala 44:11]
  assign _T_380 = ~_T_379; // @[Monitor.scala 44:11]
  assign _T_385 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_403 = io_in_a_bits_param <= 3'h1; // @[Bundles.scala 160:28]
  assign _T_405 = _T_403 | reset; // @[Monitor.scala 44:11]
  assign _T_406 = ~_T_405; // @[Monitor.scala 44:11]
  assign _T_415 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_417 = _T_415 | reset; // @[Monitor.scala 51:11]
  assign _T_418 = ~_T_417; // @[Monitor.scala 51:11]
  assign _T_429 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_433 = io_in_d_bits_size >= 3'h4; // @[Monitor.scala 309:27]
  assign _T_435 = _T_433 | reset; // @[Monitor.scala 51:11]
  assign _T_436 = ~_T_435; // @[Monitor.scala 51:11]
  assign _T_441 = ~io_in_d_bits_corrupt; // @[Monitor.scala 311:15]
  assign _T_443 = _T_441 | reset; // @[Monitor.scala 51:11]
  assign _T_444 = ~_T_443; // @[Monitor.scala 51:11]
  assign _T_445 = ~io_in_d_bits_denied; // @[Monitor.scala 312:15]
  assign _T_447 = _T_445 | reset; // @[Monitor.scala 51:11]
  assign _T_448 = ~_T_447; // @[Monitor.scala 51:11]
  assign _T_449 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_477 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_497 = _T_445 | io_in_d_bits_corrupt; // @[Monitor.scala 331:30]
  assign _T_499 = _T_497 | reset; // @[Monitor.scala 51:11]
  assign _T_500 = ~_T_499; // @[Monitor.scala 51:11]
  assign _T_506 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_523 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_541 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_573 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_580 = ~io_in_a_bits_opcode[2]; // @[Edges.scala 93:28]
  assign _T_584 = _T_582 - 2'h1; // @[Edges.scala 231:28]
  assign _T_585 = _T_582 == 2'h0; // @[Edges.scala 232:25]
  assign _T_598 = ~_T_585; // @[Monitor.scala 386:22]
  assign _T_599 = io_in_a_valid & _T_598; // @[Monitor.scala 386:19]
  assign _T_600 = io_in_a_bits_opcode == _T_593; // @[Monitor.scala 387:32]
  assign _T_602 = _T_600 | reset; // @[Monitor.scala 44:11]
  assign _T_603 = ~_T_602; // @[Monitor.scala 44:11]
  assign _T_604 = io_in_a_bits_param == _T_594; // @[Monitor.scala 388:32]
  assign _T_606 = _T_604 | reset; // @[Monitor.scala 44:11]
  assign _T_607 = ~_T_606; // @[Monitor.scala 44:11]
  assign _T_608 = io_in_a_bits_size == _T_595; // @[Monitor.scala 389:32]
  assign _T_610 = _T_608 | reset; // @[Monitor.scala 44:11]
  assign _T_611 = ~_T_610; // @[Monitor.scala 44:11]
  assign _T_612 = io_in_a_bits_source == _T_596; // @[Monitor.scala 390:32]
  assign _T_614 = _T_612 | reset; // @[Monitor.scala 44:11]
  assign _T_615 = ~_T_614; // @[Monitor.scala 44:11]
  assign _T_616 = io_in_a_bits_address == _T_597; // @[Monitor.scala 391:32]
  assign _T_618 = _T_616 | reset; // @[Monitor.scala 44:11]
  assign _T_619 = ~_T_618; // @[Monitor.scala 44:11]
  assign _T_621 = _T_573 & _T_585; // @[Monitor.scala 393:20]
  assign _T_622 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_624 = 13'h3f << io_in_d_bits_size; // @[package.scala 189:77]
  assign _T_626 = ~_T_624[5:0]; // @[package.scala 189:46]
  assign _T_632 = _T_630 - 2'h1; // @[Edges.scala 231:28]
  assign _T_633 = _T_630 == 2'h0; // @[Edges.scala 232:25]
  assign _T_647 = ~_T_633; // @[Monitor.scala 538:22]
  assign _T_648 = io_in_d_valid & _T_647; // @[Monitor.scala 538:19]
  assign _T_649 = io_in_d_bits_opcode == _T_641; // @[Monitor.scala 539:29]
  assign _T_651 = _T_649 | reset; // @[Monitor.scala 51:11]
  assign _T_652 = ~_T_651; // @[Monitor.scala 51:11]
  assign _T_657 = io_in_d_bits_size == _T_643; // @[Monitor.scala 541:29]
  assign _T_659 = _T_657 | reset; // @[Monitor.scala 51:11]
  assign _T_660 = ~_T_659; // @[Monitor.scala 51:11]
  assign _T_661 = io_in_d_bits_source == _T_644; // @[Monitor.scala 542:29]
  assign _T_663 = _T_661 | reset; // @[Monitor.scala 51:11]
  assign _T_664 = ~_T_663; // @[Monitor.scala 51:11]
  assign _T_669 = io_in_d_bits_denied == _T_646; // @[Monitor.scala 544:29]
  assign _T_671 = _T_669 | reset; // @[Monitor.scala 51:11]
  assign _T_672 = ~_T_671; // @[Monitor.scala 51:11]
  assign _T_674 = _T_622 & _T_633; // @[Monitor.scala 546:20]
  assign _T_687 = _T_685 - 2'h1; // @[Edges.scala 231:28]
  assign _T_688 = _T_685 == 2'h0; // @[Edges.scala 232:25]
  assign _T_706 = _T_704 - 2'h1; // @[Edges.scala 231:28]
  assign _T_707 = _T_704 == 2'h0; // @[Edges.scala 232:25]
  assign _T_717 = _T_573 & _T_688; // @[Monitor.scala 574:27]
  assign _T_719 = 16'h1 << io_in_a_bits_source; // @[OneHot.scala 58:35]
  assign _T_720 = _T_675 >> io_in_a_bits_source; // @[Monitor.scala 576:23]
  assign _T_722 = ~_T_720[0]; // @[Monitor.scala 576:14]
  assign _T_724 = _T_722 | reset; // @[Monitor.scala 576:13]
  assign _T_725 = ~_T_724; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_717 ? _T_719 : 16'h0; // @[Monitor.scala 574:72]
  assign _T_729 = _T_622 & _T_707; // @[Monitor.scala 581:27]
  assign _T_731 = ~_T_429; // @[Monitor.scala 581:75]
  assign _T_732 = _T_729 & _T_731; // @[Monitor.scala 581:72]
  assign _T_733 = 16'h1 << io_in_d_bits_source; // @[OneHot.scala 58:35]
  assign _T_734 = _GEN_15 | _T_675; // @[Monitor.scala 583:21]
  assign _T_735 = _T_734 >> io_in_d_bits_source; // @[Monitor.scala 583:32]
  assign _T_738 = _T_735[0] | reset; // @[Monitor.scala 51:11]
  assign _T_739 = ~_T_738; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_732 ? _T_733 : 16'h0; // @[Monitor.scala 581:91]
  assign _T_740 = _GEN_15 != _GEN_16; // @[Monitor.scala 587:20]
  assign _T_741 = _GEN_15 != 16'h0; // @[Monitor.scala 587:40]
  assign _T_742 = ~_T_741; // @[Monitor.scala 587:33]
  assign _T_743 = _T_740 | _T_742; // @[Monitor.scala 587:30]
  assign _T_745 = _T_743 | reset; // @[Monitor.scala 51:11]
  assign _T_746 = ~_T_745; // @[Monitor.scala 51:11]
  assign _T_747 = _T_675 | _GEN_15; // @[Monitor.scala 590:27]
  assign _T_748 = ~_GEN_16; // @[Monitor.scala 590:38]
  assign _T_749 = _T_747 & _T_748; // @[Monitor.scala 590:36]
  assign _T_751 = _T_675 != 16'h0; // @[Monitor.scala 595:23]
  assign _T_752 = ~_T_751; // @[Monitor.scala 595:13]
  assign _T_753 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_754 = _T_752 | _T_753; // @[Monitor.scala 595:27]
  assign _T_755 = _T_750 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_756 = _T_754 | _T_755; // @[Monitor.scala 595:44]
  assign _T_758 = _T_756 | reset; // @[Monitor.scala 595:12]
  assign _T_759 = ~_T_758; // @[Monitor.scala 595:12]
  assign _T_761 = _T_750 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_764 = _T_573 | _T_622; // @[Monitor.scala 598:27]
  assign _GEN_19 = io_in_a_valid & _T_160; // @[Monitor.scala 44:11]
  assign _GEN_33 = io_in_a_valid & _T_198; // @[Monitor.scala 44:11]
  assign _GEN_49 = io_in_a_valid & _T_240; // @[Monitor.scala 44:11]
  assign _GEN_59 = io_in_a_valid & _T_273; // @[Monitor.scala 44:11]
  assign _GEN_67 = io_in_a_valid & _T_302; // @[Monitor.scala 44:11]
  assign _GEN_75 = io_in_a_valid & _T_333; // @[Monitor.scala 44:11]
  assign _GEN_83 = io_in_a_valid & _T_359; // @[Monitor.scala 44:11]
  assign _GEN_91 = io_in_a_valid & _T_385; // @[Monitor.scala 44:11]
  assign _GEN_101 = io_in_d_valid & _T_429; // @[Monitor.scala 51:11]
  assign _GEN_107 = io_in_d_valid & _T_449; // @[Monitor.scala 51:11]
  assign _GEN_113 = io_in_d_valid & _T_477; // @[Monitor.scala 51:11]
  assign _GEN_119 = io_in_d_valid & _T_506; // @[Monitor.scala 51:11]
  assign _GEN_121 = io_in_d_valid & _T_523; // @[Monitor.scala 51:11]
  assign _GEN_123 = io_in_d_valid & _T_541; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_582 = _RAND_0[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_593 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_594 = _RAND_2[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_595 = _RAND_3[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_596 = _RAND_4[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {2{`RANDOM}};
  _T_597 = _RAND_5[35:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_630 = _RAND_6[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_641 = _RAND_7[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_643 = _RAND_8[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_644 = _RAND_9[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_646 = _RAND_10[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_675 = _RAND_11[15:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_685 = _RAND_12[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  _T_704 = _RAND_13[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_14 = {1{`RANDOM}};
  _T_750 = _RAND_14[31:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_582 <= 2'h0;
    end else if (_T_573) begin
      if (_T_585) begin
        if (_T_580) begin
          _T_582 <= _T_16[5:4];
        end else begin
          _T_582 <= 2'h0;
        end
      end else begin
        _T_582 <= _T_584;
      end
    end
    if (_T_621) begin
      _T_593 <= io_in_a_bits_opcode;
    end
    if (_T_621) begin
      _T_594 <= io_in_a_bits_param;
    end
    if (_T_621) begin
      _T_595 <= io_in_a_bits_size;
    end
    if (_T_621) begin
      _T_596 <= io_in_a_bits_source;
    end
    if (_T_621) begin
      _T_597 <= io_in_a_bits_address;
    end
    if (reset) begin
      _T_630 <= 2'h0;
    end else if (_T_622) begin
      if (_T_633) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_630 <= _T_626[5:4];
        end else begin
          _T_630 <= 2'h0;
        end
      end else begin
        _T_630 <= _T_632;
      end
    end
    if (_T_674) begin
      _T_641 <= io_in_d_bits_opcode;
    end
    if (_T_674) begin
      _T_643 <= io_in_d_bits_size;
    end
    if (_T_674) begin
      _T_644 <= io_in_d_bits_source;
    end
    if (_T_674) begin
      _T_646 <= io_in_d_bits_denied;
    end
    if (reset) begin
      _T_675 <= 16'h0;
    end else begin
      _T_675 <= _T_749;
    end
    if (reset) begin
      _T_685 <= 2'h0;
    end else if (_T_573) begin
      if (_T_688) begin
        if (_T_580) begin
          _T_685 <= _T_16[5:4];
        end else begin
          _T_685 <= 2'h0;
        end
      end else begin
        _T_685 <= _T_687;
      end
    end
    if (reset) begin
      _T_704 <= 2'h0;
    end else if (_T_622) begin
      if (_T_707) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_704 <= _T_626[5:4];
        end else begin
          _T_704 <= 2'h0;
        end
      end else begin
        _T_704 <= _T_706;
      end
    end
    if (reset) begin
      _T_750 <= 32'h0;
    end else if (_T_764) begin
      _T_750 <= 32'h0;
    end else begin
      _T_750 <= _T_761;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_171) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_171) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_171) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_171) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_181) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock smaller than a beat (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_181) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_184) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_188) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid grow param (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_188) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_193) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_193) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_197) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock is corrupt (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_197) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_171) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_171) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_171) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_171) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_181) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm smaller than a beat (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_181) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_184) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_188) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid grow param (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_188) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_230) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_230) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_193) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_193) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_197) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm is corrupt (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_197) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_254) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_254) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_184) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_264) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid param (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_264) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_268) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_268) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_197) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get is corrupt (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_197) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_59 & _T_254) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_59 & _T_254) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_59 & _T_184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_59 & _T_184) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_59 & _T_264) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid param (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_59 & _T_264) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_59 & _T_268) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_59 & _T_268) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_67 & _T_254) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_67 & _T_254) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_67 & _T_184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_67 & _T_184) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_67 & _T_264) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid param (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_67 & _T_264) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_67 & _T_332) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial contains invalid mask (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_67 & _T_332) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_171) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_171) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_184) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_354) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid opcode param (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_354) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_268) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_268) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_171) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_171) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_184) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_380) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid opcode param (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_380) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_268) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_268) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_91 & _T_171) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_91 & _T_171) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_91 & _T_184) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_91 & _T_184) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_91 & _T_406) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid opcode param (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_91 & _T_406) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_91 & _T_268) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_91 & _T_268) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_91 & _T_197) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint is corrupt (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_91 & _T_197) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_418) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_418) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_101 & _T_436) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_101 & _T_436) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_101 & _T_444) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is corrupt (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_101 & _T_444) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_101 & _T_448) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is denied (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_101 & _T_448) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_107 & _T_171) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid sink ID (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_107 & _T_171) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_107 & _T_436) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_107 & _T_436) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_107 & _T_444) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is corrupt (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_107 & _T_444) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_113 & _T_171) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid sink ID (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_113 & _T_171) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_113 & _T_436) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_113 & _T_436) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_113 & _T_500) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied but not corrupt (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_113 & _T_500) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_119 & _T_444) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is corrupt (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_119 & _T_444) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_121 & _T_500) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied but not corrupt (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_121 & _T_500) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_123 & _T_444) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is corrupt (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_123 & _T_444) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_599 & _T_603) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_599 & _T_603) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_599 & _T_607) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel param changed within multibeat operation (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_599 & _T_607) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_599 & _T_611) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel size changed within multibeat operation (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_599 & _T_611) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_599 & _T_615) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel source changed within multibeat operation (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_599 & _T_615) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_599 & _T_619) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_599 & _T_619) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_648 & _T_652) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_648 & _T_652) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_648 & _T_660) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_648 & _T_660) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_648 & _T_664) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel source changed within multibeat operation (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_648 & _T_664) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_648 & _T_672) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_648 & _T_672) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_717 & _T_725) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_717 & _T_725) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_732 & _T_739) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_732 & _T_739) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_746) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' and 'D' concurrent, despite minlatency 2 (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_746) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_759) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at MemoryBus.scala:68:103)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_759) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
