//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_SourceB_assert(
  input        clock,
  input        reset,
  input        io_req_valid,
  input  [3:0] io_req_bits_clients
);
  wire  _T_31; // @[SourceB.scala 61:13]
  wire  _T_32; // @[SourceB.scala 61:50]
  wire  _T_33; // @[SourceB.scala 61:27]
  wire  _T_35; // @[SourceB.scala 61:12]
  wire  _T_36; // @[SourceB.scala 61:12]
  assign _T_31 = ~io_req_valid; // @[SourceB.scala 61:13]
  assign _T_32 = io_req_bits_clients != 4'h0; // @[SourceB.scala 61:50]
  assign _T_33 = _T_31 | _T_32; // @[SourceB.scala 61:27]
  assign _T_35 = _T_33 | reset; // @[SourceB.scala 61:12]
  assign _T_36 = ~_T_35; // @[SourceB.scala 61:12]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_36) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at SourceB.scala:61 assert (!io.req.valid || io.req.bits.clients =/= UInt(0))\n"); // @[SourceB.scala 61:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_36) begin
          $fatal; // @[SourceB.scala 61:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
