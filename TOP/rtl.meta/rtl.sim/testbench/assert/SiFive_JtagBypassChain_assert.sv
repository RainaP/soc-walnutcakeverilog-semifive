//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_JtagBypassChain_assert(
  input   clock,
  input   reset,
  input   io_chainIn_shift,
  input   io_chainIn_capture,
  input   io_chainIn_update
);
  wire  _T_2; // @[JtagShifter.scala 73:31]
  wire  _T_3; // @[JtagShifter.scala 73:10]
  wire  _T_4; // @[JtagShifter.scala 74:31]
  wire  _T_5; // @[JtagShifter.scala 74:10]
  wire  _T_6; // @[JtagShifter.scala 74:7]
  wire  _T_7; // @[JtagShifter.scala 75:30]
  wire  _T_8; // @[JtagShifter.scala 75:10]
  wire  _T_9; // @[JtagShifter.scala 75:7]
  wire  _T_11; // @[JtagShifter.scala 73:9]
  wire  _T_12; // @[JtagShifter.scala 73:9]
  assign _T_2 = io_chainIn_capture & io_chainIn_update; // @[JtagShifter.scala 73:31]
  assign _T_3 = ~_T_2; // @[JtagShifter.scala 73:10]
  assign _T_4 = io_chainIn_capture & io_chainIn_shift; // @[JtagShifter.scala 74:31]
  assign _T_5 = ~_T_4; // @[JtagShifter.scala 74:10]
  assign _T_6 = _T_3 & _T_5; // @[JtagShifter.scala 74:7]
  assign _T_7 = io_chainIn_update & io_chainIn_shift; // @[JtagShifter.scala 75:30]
  assign _T_8 = ~_T_7; // @[JtagShifter.scala 75:10]
  assign _T_9 = _T_6 & _T_8; // @[JtagShifter.scala 75:7]
  assign _T_11 = _T_9 | reset; // @[JtagShifter.scala 73:9]
  assign _T_12 = ~_T_11; // @[JtagShifter.scala 73:9]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_12) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at JtagShifter.scala:73 assert(!(io.chainIn.capture && io.chainIn.update)\n"); // @[JtagShifter.scala 73:9]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_12) begin
          $fatal; // @[JtagShifter.scala 73:9]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
