//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__AXI4ToTLBridgeContents(
  input         rf_reset,
  input         clock,
  input         reset,
  input         auto_tl_out_a_ready,
  output        auto_tl_out_a_valid,
  output [2:0]  auto_tl_out_a_bits_opcode,
  output [3:0]  auto_tl_out_a_bits_size,
  output [4:0]  auto_tl_out_a_bits_source,
  output [36:0] auto_tl_out_a_bits_address,
  output        auto_tl_out_a_bits_user_amba_prot_bufferable,
  output        auto_tl_out_a_bits_user_amba_prot_modifiable,
  output        auto_tl_out_a_bits_user_amba_prot_readalloc,
  output        auto_tl_out_a_bits_user_amba_prot_writealloc,
  output        auto_tl_out_a_bits_user_amba_prot_privileged,
  output        auto_tl_out_a_bits_user_amba_prot_secure,
  output        auto_tl_out_a_bits_user_amba_prot_fetch,
  output [7:0]  auto_tl_out_a_bits_mask,
  output [63:0] auto_tl_out_a_bits_data,
  output        auto_tl_out_d_ready,
  input         auto_tl_out_d_valid,
  input  [2:0]  auto_tl_out_d_bits_opcode,
  input  [1:0]  auto_tl_out_d_bits_param,
  input  [3:0]  auto_tl_out_d_bits_size,
  input  [4:0]  auto_tl_out_d_bits_source,
  input  [4:0]  auto_tl_out_d_bits_sink,
  input         auto_tl_out_d_bits_denied,
  input  [63:0] auto_tl_out_d_bits_data,
  input         auto_tl_out_d_bits_corrupt,
  output        auto_axi4_in_aw_ready,
  input         auto_axi4_in_aw_valid,
  input  [15:0] auto_axi4_in_aw_bits_id,
  input  [36:0] auto_axi4_in_aw_bits_addr,
  input  [7:0]  auto_axi4_in_aw_bits_len,
  input  [2:0]  auto_axi4_in_aw_bits_size,
  input  [1:0]  auto_axi4_in_aw_bits_burst,
  input  [3:0]  auto_axi4_in_aw_bits_cache,
  input  [2:0]  auto_axi4_in_aw_bits_prot,
  output        auto_axi4_in_w_ready,
  input         auto_axi4_in_w_valid,
  input  [63:0] auto_axi4_in_w_bits_data,
  input  [7:0]  auto_axi4_in_w_bits_strb,
  input         auto_axi4_in_w_bits_last,
  input         auto_axi4_in_b_ready,
  output        auto_axi4_in_b_valid,
  output [15:0] auto_axi4_in_b_bits_id,
  output [1:0]  auto_axi4_in_b_bits_resp,
  output        auto_axi4_in_ar_ready,
  input         auto_axi4_in_ar_valid,
  input  [15:0] auto_axi4_in_ar_bits_id,
  input  [36:0] auto_axi4_in_ar_bits_addr,
  input  [7:0]  auto_axi4_in_ar_bits_len,
  input  [2:0]  auto_axi4_in_ar_bits_size,
  input  [1:0]  auto_axi4_in_ar_bits_burst,
  input  [3:0]  auto_axi4_in_ar_bits_cache,
  input  [2:0]  auto_axi4_in_ar_bits_prot,
  input         auto_axi4_in_r_ready,
  output        auto_axi4_in_r_valid,
  output [15:0] auto_axi4_in_r_bits_id,
  output [63:0] auto_axi4_in_r_bits_data,
  output [1:0]  auto_axi4_in_r_bits_resp,
  output        auto_axi4_in_r_bits_last
);
  wire  buffer_auto_in_a_ready;
  wire  buffer_auto_in_a_valid;
  wire [2:0] buffer_auto_in_a_bits_opcode;
  wire [3:0] buffer_auto_in_a_bits_size;
  wire [4:0] buffer_auto_in_a_bits_source;
  wire [36:0] buffer_auto_in_a_bits_address;
  wire  buffer_auto_in_a_bits_user_amba_prot_bufferable;
  wire  buffer_auto_in_a_bits_user_amba_prot_modifiable;
  wire  buffer_auto_in_a_bits_user_amba_prot_readalloc;
  wire  buffer_auto_in_a_bits_user_amba_prot_writealloc;
  wire  buffer_auto_in_a_bits_user_amba_prot_privileged;
  wire  buffer_auto_in_a_bits_user_amba_prot_secure;
  wire  buffer_auto_in_a_bits_user_amba_prot_fetch;
  wire [7:0] buffer_auto_in_a_bits_mask;
  wire [63:0] buffer_auto_in_a_bits_data;
  wire  buffer_auto_in_d_ready;
  wire  buffer_auto_in_d_valid;
  wire [2:0] buffer_auto_in_d_bits_opcode;
  wire [1:0] buffer_auto_in_d_bits_param;
  wire [3:0] buffer_auto_in_d_bits_size;
  wire [4:0] buffer_auto_in_d_bits_source;
  wire [4:0] buffer_auto_in_d_bits_sink;
  wire  buffer_auto_in_d_bits_denied;
  wire [63:0] buffer_auto_in_d_bits_data;
  wire  buffer_auto_in_d_bits_corrupt;
  wire  buffer_auto_out_a_ready;
  wire  buffer_auto_out_a_valid;
  wire [2:0] buffer_auto_out_a_bits_opcode;
  wire [3:0] buffer_auto_out_a_bits_size;
  wire [4:0] buffer_auto_out_a_bits_source;
  wire [36:0] buffer_auto_out_a_bits_address;
  wire  buffer_auto_out_a_bits_user_amba_prot_bufferable;
  wire  buffer_auto_out_a_bits_user_amba_prot_modifiable;
  wire  buffer_auto_out_a_bits_user_amba_prot_readalloc;
  wire  buffer_auto_out_a_bits_user_amba_prot_writealloc;
  wire  buffer_auto_out_a_bits_user_amba_prot_privileged;
  wire  buffer_auto_out_a_bits_user_amba_prot_secure;
  wire  buffer_auto_out_a_bits_user_amba_prot_fetch;
  wire [7:0] buffer_auto_out_a_bits_mask;
  wire [63:0] buffer_auto_out_a_bits_data;
  wire  buffer_auto_out_d_ready;
  wire  buffer_auto_out_d_valid;
  wire [2:0] buffer_auto_out_d_bits_opcode;
  wire [1:0] buffer_auto_out_d_bits_param;
  wire [3:0] buffer_auto_out_d_bits_size;
  wire [4:0] buffer_auto_out_d_bits_source;
  wire [4:0] buffer_auto_out_d_bits_sink;
  wire  buffer_auto_out_d_bits_denied;
  wire [63:0] buffer_auto_out_d_bits_data;
  wire  buffer_auto_out_d_bits_corrupt;
  wire  fixer_rf_reset; // @[FIFOFixer.scala 144:27]
  wire  fixer_clock; // @[FIFOFixer.scala 144:27]
  wire  fixer_reset; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_in_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_a_bits_user_amba_prot_fetch; // @[FIFOFixer.scala 144:27]
  wire [7:0] fixer_auto_in_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [63:0] fixer_auto_in_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_in_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [63:0] fixer_auto_in_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [36:0] fixer_auto_out_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_a_bits_user_amba_prot_fetch; // @[FIFOFixer.scala 144:27]
  wire [7:0] fixer_auto_out_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [63:0] fixer_auto_out_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [1:0] fixer_auto_out_d_bits_param; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire [4:0] fixer_auto_out_d_bits_sink; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [63:0] fixer_auto_out_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  widget_auto_in_a_ready;
  wire  widget_auto_in_a_valid;
  wire [2:0] widget_auto_in_a_bits_opcode;
  wire [3:0] widget_auto_in_a_bits_size;
  wire [4:0] widget_auto_in_a_bits_source;
  wire [36:0] widget_auto_in_a_bits_address;
  wire  widget_auto_in_a_bits_user_amba_prot_bufferable;
  wire  widget_auto_in_a_bits_user_amba_prot_modifiable;
  wire  widget_auto_in_a_bits_user_amba_prot_readalloc;
  wire  widget_auto_in_a_bits_user_amba_prot_writealloc;
  wire  widget_auto_in_a_bits_user_amba_prot_privileged;
  wire  widget_auto_in_a_bits_user_amba_prot_secure;
  wire  widget_auto_in_a_bits_user_amba_prot_fetch;
  wire [7:0] widget_auto_in_a_bits_mask;
  wire [63:0] widget_auto_in_a_bits_data;
  wire  widget_auto_in_d_ready;
  wire  widget_auto_in_d_valid;
  wire [2:0] widget_auto_in_d_bits_opcode;
  wire [3:0] widget_auto_in_d_bits_size;
  wire [4:0] widget_auto_in_d_bits_source;
  wire  widget_auto_in_d_bits_denied;
  wire [63:0] widget_auto_in_d_bits_data;
  wire  widget_auto_in_d_bits_corrupt;
  wire  widget_auto_out_a_ready;
  wire  widget_auto_out_a_valid;
  wire [2:0] widget_auto_out_a_bits_opcode;
  wire [3:0] widget_auto_out_a_bits_size;
  wire [4:0] widget_auto_out_a_bits_source;
  wire [36:0] widget_auto_out_a_bits_address;
  wire  widget_auto_out_a_bits_user_amba_prot_bufferable;
  wire  widget_auto_out_a_bits_user_amba_prot_modifiable;
  wire  widget_auto_out_a_bits_user_amba_prot_readalloc;
  wire  widget_auto_out_a_bits_user_amba_prot_writealloc;
  wire  widget_auto_out_a_bits_user_amba_prot_privileged;
  wire  widget_auto_out_a_bits_user_amba_prot_secure;
  wire  widget_auto_out_a_bits_user_amba_prot_fetch;
  wire [7:0] widget_auto_out_a_bits_mask;
  wire [63:0] widget_auto_out_a_bits_data;
  wire  widget_auto_out_d_ready;
  wire  widget_auto_out_d_valid;
  wire [2:0] widget_auto_out_d_bits_opcode;
  wire [3:0] widget_auto_out_d_bits_size;
  wire [4:0] widget_auto_out_d_bits_source;
  wire  widget_auto_out_d_bits_denied;
  wire [63:0] widget_auto_out_d_bits_data;
  wire  widget_auto_out_d_bits_corrupt;
  wire  axi42tl_rf_reset; // @[ToTL.scala 217:29]
  wire  axi42tl_clock; // @[ToTL.scala 217:29]
  wire  axi42tl_reset; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_in_aw_ready; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_in_aw_valid; // @[ToTL.scala 217:29]
  wire [1:0] axi42tl_auto_in_aw_bits_id; // @[ToTL.scala 217:29]
  wire [36:0] axi42tl_auto_in_aw_bits_addr; // @[ToTL.scala 217:29]
  wire [7:0] axi42tl_auto_in_aw_bits_len; // @[ToTL.scala 217:29]
  wire [2:0] axi42tl_auto_in_aw_bits_size; // @[ToTL.scala 217:29]
  wire [3:0] axi42tl_auto_in_aw_bits_cache; // @[ToTL.scala 217:29]
  wire [2:0] axi42tl_auto_in_aw_bits_prot; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_in_w_ready; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_in_w_valid; // @[ToTL.scala 217:29]
  wire [63:0] axi42tl_auto_in_w_bits_data; // @[ToTL.scala 217:29]
  wire [7:0] axi42tl_auto_in_w_bits_strb; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_in_w_bits_last; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_in_b_ready; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_in_b_valid; // @[ToTL.scala 217:29]
  wire [1:0] axi42tl_auto_in_b_bits_id; // @[ToTL.scala 217:29]
  wire [1:0] axi42tl_auto_in_b_bits_resp; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_in_ar_ready; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_in_ar_valid; // @[ToTL.scala 217:29]
  wire [1:0] axi42tl_auto_in_ar_bits_id; // @[ToTL.scala 217:29]
  wire [36:0] axi42tl_auto_in_ar_bits_addr; // @[ToTL.scala 217:29]
  wire [7:0] axi42tl_auto_in_ar_bits_len; // @[ToTL.scala 217:29]
  wire [2:0] axi42tl_auto_in_ar_bits_size; // @[ToTL.scala 217:29]
  wire [3:0] axi42tl_auto_in_ar_bits_cache; // @[ToTL.scala 217:29]
  wire [2:0] axi42tl_auto_in_ar_bits_prot; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_in_r_ready; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_in_r_valid; // @[ToTL.scala 217:29]
  wire [1:0] axi42tl_auto_in_r_bits_id; // @[ToTL.scala 217:29]
  wire [63:0] axi42tl_auto_in_r_bits_data; // @[ToTL.scala 217:29]
  wire [1:0] axi42tl_auto_in_r_bits_resp; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_in_r_bits_last; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_out_a_ready; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_out_a_valid; // @[ToTL.scala 217:29]
  wire [2:0] axi42tl_auto_out_a_bits_opcode; // @[ToTL.scala 217:29]
  wire [3:0] axi42tl_auto_out_a_bits_size; // @[ToTL.scala 217:29]
  wire [4:0] axi42tl_auto_out_a_bits_source; // @[ToTL.scala 217:29]
  wire [36:0] axi42tl_auto_out_a_bits_address; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_out_a_bits_user_amba_prot_bufferable; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_out_a_bits_user_amba_prot_modifiable; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_out_a_bits_user_amba_prot_readalloc; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_out_a_bits_user_amba_prot_writealloc; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_out_a_bits_user_amba_prot_privileged; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_out_a_bits_user_amba_prot_secure; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_out_a_bits_user_amba_prot_fetch; // @[ToTL.scala 217:29]
  wire [7:0] axi42tl_auto_out_a_bits_mask; // @[ToTL.scala 217:29]
  wire [63:0] axi42tl_auto_out_a_bits_data; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_out_d_ready; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_out_d_valid; // @[ToTL.scala 217:29]
  wire [2:0] axi42tl_auto_out_d_bits_opcode; // @[ToTL.scala 217:29]
  wire [3:0] axi42tl_auto_out_d_bits_size; // @[ToTL.scala 217:29]
  wire [4:0] axi42tl_auto_out_d_bits_source; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_out_d_bits_denied; // @[ToTL.scala 217:29]
  wire [63:0] axi42tl_auto_out_d_bits_data; // @[ToTL.scala 217:29]
  wire  axi42tl_auto_out_d_bits_corrupt; // @[ToTL.scala 217:29]
  wire  axi4yank_rf_reset; // @[UserYanker.scala 113:30]
  wire  axi4yank_clock; // @[UserYanker.scala 113:30]
  wire  axi4yank_reset; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_aw_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_aw_valid; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_in_aw_bits_id; // @[UserYanker.scala 113:30]
  wire [36:0] axi4yank_auto_in_aw_bits_addr; // @[UserYanker.scala 113:30]
  wire [7:0] axi4yank_auto_in_aw_bits_len; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_in_aw_bits_size; // @[UserYanker.scala 113:30]
  wire [3:0] axi4yank_auto_in_aw_bits_cache; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_in_aw_bits_prot; // @[UserYanker.scala 113:30]
  wire [13:0] axi4yank_auto_in_aw_bits_echo_extra_id; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_aw_bits_echo_real_last; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_w_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_w_valid; // @[UserYanker.scala 113:30]
  wire [63:0] axi4yank_auto_in_w_bits_data; // @[UserYanker.scala 113:30]
  wire [7:0] axi4yank_auto_in_w_bits_strb; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_w_bits_last; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_b_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_b_valid; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_in_b_bits_id; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_in_b_bits_resp; // @[UserYanker.scala 113:30]
  wire [13:0] axi4yank_auto_in_b_bits_echo_extra_id; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_b_bits_echo_real_last; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_ar_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_ar_valid; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_in_ar_bits_id; // @[UserYanker.scala 113:30]
  wire [36:0] axi4yank_auto_in_ar_bits_addr; // @[UserYanker.scala 113:30]
  wire [7:0] axi4yank_auto_in_ar_bits_len; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_in_ar_bits_size; // @[UserYanker.scala 113:30]
  wire [3:0] axi4yank_auto_in_ar_bits_cache; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_in_ar_bits_prot; // @[UserYanker.scala 113:30]
  wire [13:0] axi4yank_auto_in_ar_bits_echo_extra_id; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_ar_bits_echo_real_last; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_r_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_r_valid; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_in_r_bits_id; // @[UserYanker.scala 113:30]
  wire [63:0] axi4yank_auto_in_r_bits_data; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_in_r_bits_resp; // @[UserYanker.scala 113:30]
  wire [13:0] axi4yank_auto_in_r_bits_echo_extra_id; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_r_bits_echo_real_last; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_in_r_bits_last; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_aw_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_aw_valid; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_out_aw_bits_id; // @[UserYanker.scala 113:30]
  wire [36:0] axi4yank_auto_out_aw_bits_addr; // @[UserYanker.scala 113:30]
  wire [7:0] axi4yank_auto_out_aw_bits_len; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_out_aw_bits_size; // @[UserYanker.scala 113:30]
  wire [3:0] axi4yank_auto_out_aw_bits_cache; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_out_aw_bits_prot; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_w_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_w_valid; // @[UserYanker.scala 113:30]
  wire [63:0] axi4yank_auto_out_w_bits_data; // @[UserYanker.scala 113:30]
  wire [7:0] axi4yank_auto_out_w_bits_strb; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_w_bits_last; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_b_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_b_valid; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_out_b_bits_id; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_out_b_bits_resp; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_ar_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_ar_valid; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_out_ar_bits_id; // @[UserYanker.scala 113:30]
  wire [36:0] axi4yank_auto_out_ar_bits_addr; // @[UserYanker.scala 113:30]
  wire [7:0] axi4yank_auto_out_ar_bits_len; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_out_ar_bits_size; // @[UserYanker.scala 113:30]
  wire [3:0] axi4yank_auto_out_ar_bits_cache; // @[UserYanker.scala 113:30]
  wire [2:0] axi4yank_auto_out_ar_bits_prot; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_r_ready; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_r_valid; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_out_r_bits_id; // @[UserYanker.scala 113:30]
  wire [63:0] axi4yank_auto_out_r_bits_data; // @[UserYanker.scala 113:30]
  wire [1:0] axi4yank_auto_out_r_bits_resp; // @[UserYanker.scala 113:30]
  wire  axi4yank_auto_out_r_bits_last; // @[UserYanker.scala 113:30]
  wire  axi4frag_rf_reset; // @[Fragmenter.scala 205:30]
  wire  axi4frag_clock; // @[Fragmenter.scala 205:30]
  wire  axi4frag_reset; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_in_aw_ready; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_in_aw_valid; // @[Fragmenter.scala 205:30]
  wire [1:0] axi4frag_auto_in_aw_bits_id; // @[Fragmenter.scala 205:30]
  wire [36:0] axi4frag_auto_in_aw_bits_addr; // @[Fragmenter.scala 205:30]
  wire [7:0] axi4frag_auto_in_aw_bits_len; // @[Fragmenter.scala 205:30]
  wire [2:0] axi4frag_auto_in_aw_bits_size; // @[Fragmenter.scala 205:30]
  wire [1:0] axi4frag_auto_in_aw_bits_burst; // @[Fragmenter.scala 205:30]
  wire [3:0] axi4frag_auto_in_aw_bits_cache; // @[Fragmenter.scala 205:30]
  wire [2:0] axi4frag_auto_in_aw_bits_prot; // @[Fragmenter.scala 205:30]
  wire [13:0] axi4frag_auto_in_aw_bits_echo_extra_id; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_in_w_ready; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_in_w_valid; // @[Fragmenter.scala 205:30]
  wire [63:0] axi4frag_auto_in_w_bits_data; // @[Fragmenter.scala 205:30]
  wire [7:0] axi4frag_auto_in_w_bits_strb; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_in_w_bits_last; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_in_b_ready; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_in_b_valid; // @[Fragmenter.scala 205:30]
  wire [1:0] axi4frag_auto_in_b_bits_id; // @[Fragmenter.scala 205:30]
  wire [1:0] axi4frag_auto_in_b_bits_resp; // @[Fragmenter.scala 205:30]
  wire [13:0] axi4frag_auto_in_b_bits_echo_extra_id; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_in_ar_ready; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_in_ar_valid; // @[Fragmenter.scala 205:30]
  wire [1:0] axi4frag_auto_in_ar_bits_id; // @[Fragmenter.scala 205:30]
  wire [36:0] axi4frag_auto_in_ar_bits_addr; // @[Fragmenter.scala 205:30]
  wire [7:0] axi4frag_auto_in_ar_bits_len; // @[Fragmenter.scala 205:30]
  wire [2:0] axi4frag_auto_in_ar_bits_size; // @[Fragmenter.scala 205:30]
  wire [1:0] axi4frag_auto_in_ar_bits_burst; // @[Fragmenter.scala 205:30]
  wire [3:0] axi4frag_auto_in_ar_bits_cache; // @[Fragmenter.scala 205:30]
  wire [2:0] axi4frag_auto_in_ar_bits_prot; // @[Fragmenter.scala 205:30]
  wire [13:0] axi4frag_auto_in_ar_bits_echo_extra_id; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_in_r_ready; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_in_r_valid; // @[Fragmenter.scala 205:30]
  wire [1:0] axi4frag_auto_in_r_bits_id; // @[Fragmenter.scala 205:30]
  wire [63:0] axi4frag_auto_in_r_bits_data; // @[Fragmenter.scala 205:30]
  wire [1:0] axi4frag_auto_in_r_bits_resp; // @[Fragmenter.scala 205:30]
  wire [13:0] axi4frag_auto_in_r_bits_echo_extra_id; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_in_r_bits_last; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_aw_ready; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_aw_valid; // @[Fragmenter.scala 205:30]
  wire [1:0] axi4frag_auto_out_aw_bits_id; // @[Fragmenter.scala 205:30]
  wire [36:0] axi4frag_auto_out_aw_bits_addr; // @[Fragmenter.scala 205:30]
  wire [7:0] axi4frag_auto_out_aw_bits_len; // @[Fragmenter.scala 205:30]
  wire [2:0] axi4frag_auto_out_aw_bits_size; // @[Fragmenter.scala 205:30]
  wire [3:0] axi4frag_auto_out_aw_bits_cache; // @[Fragmenter.scala 205:30]
  wire [2:0] axi4frag_auto_out_aw_bits_prot; // @[Fragmenter.scala 205:30]
  wire [13:0] axi4frag_auto_out_aw_bits_echo_extra_id; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_aw_bits_echo_real_last; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_w_ready; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_w_valid; // @[Fragmenter.scala 205:30]
  wire [63:0] axi4frag_auto_out_w_bits_data; // @[Fragmenter.scala 205:30]
  wire [7:0] axi4frag_auto_out_w_bits_strb; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_w_bits_last; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_b_ready; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_b_valid; // @[Fragmenter.scala 205:30]
  wire [1:0] axi4frag_auto_out_b_bits_id; // @[Fragmenter.scala 205:30]
  wire [1:0] axi4frag_auto_out_b_bits_resp; // @[Fragmenter.scala 205:30]
  wire [13:0] axi4frag_auto_out_b_bits_echo_extra_id; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_b_bits_echo_real_last; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_ar_ready; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_ar_valid; // @[Fragmenter.scala 205:30]
  wire [1:0] axi4frag_auto_out_ar_bits_id; // @[Fragmenter.scala 205:30]
  wire [36:0] axi4frag_auto_out_ar_bits_addr; // @[Fragmenter.scala 205:30]
  wire [7:0] axi4frag_auto_out_ar_bits_len; // @[Fragmenter.scala 205:30]
  wire [2:0] axi4frag_auto_out_ar_bits_size; // @[Fragmenter.scala 205:30]
  wire [3:0] axi4frag_auto_out_ar_bits_cache; // @[Fragmenter.scala 205:30]
  wire [2:0] axi4frag_auto_out_ar_bits_prot; // @[Fragmenter.scala 205:30]
  wire [13:0] axi4frag_auto_out_ar_bits_echo_extra_id; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_ar_bits_echo_real_last; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_r_ready; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_r_valid; // @[Fragmenter.scala 205:30]
  wire [1:0] axi4frag_auto_out_r_bits_id; // @[Fragmenter.scala 205:30]
  wire [63:0] axi4frag_auto_out_r_bits_data; // @[Fragmenter.scala 205:30]
  wire [1:0] axi4frag_auto_out_r_bits_resp; // @[Fragmenter.scala 205:30]
  wire [13:0] axi4frag_auto_out_r_bits_echo_extra_id; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_r_bits_echo_real_last; // @[Fragmenter.scala 205:30]
  wire  axi4frag_auto_out_r_bits_last; // @[Fragmenter.scala 205:30]
  wire  axi4index_auto_in_aw_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_aw_valid; // @[IdIndexer.scala 107:31]
  wire [15:0] axi4index_auto_in_aw_bits_id; // @[IdIndexer.scala 107:31]
  wire [36:0] axi4index_auto_in_aw_bits_addr; // @[IdIndexer.scala 107:31]
  wire [7:0] axi4index_auto_in_aw_bits_len; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_in_aw_bits_size; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_in_aw_bits_burst; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_in_aw_bits_cache; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_in_aw_bits_prot; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_w_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_w_valid; // @[IdIndexer.scala 107:31]
  wire [63:0] axi4index_auto_in_w_bits_data; // @[IdIndexer.scala 107:31]
  wire [7:0] axi4index_auto_in_w_bits_strb; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_w_bits_last; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_b_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_b_valid; // @[IdIndexer.scala 107:31]
  wire [15:0] axi4index_auto_in_b_bits_id; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_in_b_bits_resp; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_ar_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_ar_valid; // @[IdIndexer.scala 107:31]
  wire [15:0] axi4index_auto_in_ar_bits_id; // @[IdIndexer.scala 107:31]
  wire [36:0] axi4index_auto_in_ar_bits_addr; // @[IdIndexer.scala 107:31]
  wire [7:0] axi4index_auto_in_ar_bits_len; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_in_ar_bits_size; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_in_ar_bits_burst; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_in_ar_bits_cache; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_in_ar_bits_prot; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_r_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_r_valid; // @[IdIndexer.scala 107:31]
  wire [15:0] axi4index_auto_in_r_bits_id; // @[IdIndexer.scala 107:31]
  wire [63:0] axi4index_auto_in_r_bits_data; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_in_r_bits_resp; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_in_r_bits_last; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_aw_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_aw_valid; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_out_aw_bits_id; // @[IdIndexer.scala 107:31]
  wire [36:0] axi4index_auto_out_aw_bits_addr; // @[IdIndexer.scala 107:31]
  wire [7:0] axi4index_auto_out_aw_bits_len; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_out_aw_bits_size; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_out_aw_bits_burst; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_out_aw_bits_cache; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_out_aw_bits_prot; // @[IdIndexer.scala 107:31]
  wire [13:0] axi4index_auto_out_aw_bits_echo_extra_id; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_w_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_w_valid; // @[IdIndexer.scala 107:31]
  wire [63:0] axi4index_auto_out_w_bits_data; // @[IdIndexer.scala 107:31]
  wire [7:0] axi4index_auto_out_w_bits_strb; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_w_bits_last; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_b_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_b_valid; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_out_b_bits_id; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_out_b_bits_resp; // @[IdIndexer.scala 107:31]
  wire [13:0] axi4index_auto_out_b_bits_echo_extra_id; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_ar_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_ar_valid; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_out_ar_bits_id; // @[IdIndexer.scala 107:31]
  wire [36:0] axi4index_auto_out_ar_bits_addr; // @[IdIndexer.scala 107:31]
  wire [7:0] axi4index_auto_out_ar_bits_len; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_out_ar_bits_size; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_out_ar_bits_burst; // @[IdIndexer.scala 107:31]
  wire [3:0] axi4index_auto_out_ar_bits_cache; // @[IdIndexer.scala 107:31]
  wire [2:0] axi4index_auto_out_ar_bits_prot; // @[IdIndexer.scala 107:31]
  wire [13:0] axi4index_auto_out_ar_bits_echo_extra_id; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_r_ready; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_r_valid; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_out_r_bits_id; // @[IdIndexer.scala 107:31]
  wire [63:0] axi4index_auto_out_r_bits_data; // @[IdIndexer.scala 107:31]
  wire [1:0] axi4index_auto_out_r_bits_resp; // @[IdIndexer.scala 107:31]
  wire [13:0] axi4index_auto_out_r_bits_echo_extra_id; // @[IdIndexer.scala 107:31]
  wire  axi4index_auto_out_r_bits_last; // @[IdIndexer.scala 107:31]
  wire  axi4buf_rf_reset; // @[Buffer.scala 58:29]
  wire  axi4buf_clock; // @[Buffer.scala 58:29]
  wire  axi4buf_reset; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_aw_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_aw_valid; // @[Buffer.scala 58:29]
  wire [15:0] axi4buf_auto_in_aw_bits_id; // @[Buffer.scala 58:29]
  wire [36:0] axi4buf_auto_in_aw_bits_addr; // @[Buffer.scala 58:29]
  wire [7:0] axi4buf_auto_in_aw_bits_len; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_in_aw_bits_size; // @[Buffer.scala 58:29]
  wire [1:0] axi4buf_auto_in_aw_bits_burst; // @[Buffer.scala 58:29]
  wire [3:0] axi4buf_auto_in_aw_bits_cache; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_in_aw_bits_prot; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_w_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_w_valid; // @[Buffer.scala 58:29]
  wire [63:0] axi4buf_auto_in_w_bits_data; // @[Buffer.scala 58:29]
  wire [7:0] axi4buf_auto_in_w_bits_strb; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_w_bits_last; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_b_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_b_valid; // @[Buffer.scala 58:29]
  wire [15:0] axi4buf_auto_in_b_bits_id; // @[Buffer.scala 58:29]
  wire [1:0] axi4buf_auto_in_b_bits_resp; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_ar_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_ar_valid; // @[Buffer.scala 58:29]
  wire [15:0] axi4buf_auto_in_ar_bits_id; // @[Buffer.scala 58:29]
  wire [36:0] axi4buf_auto_in_ar_bits_addr; // @[Buffer.scala 58:29]
  wire [7:0] axi4buf_auto_in_ar_bits_len; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_in_ar_bits_size; // @[Buffer.scala 58:29]
  wire [1:0] axi4buf_auto_in_ar_bits_burst; // @[Buffer.scala 58:29]
  wire [3:0] axi4buf_auto_in_ar_bits_cache; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_in_ar_bits_prot; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_r_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_r_valid; // @[Buffer.scala 58:29]
  wire [15:0] axi4buf_auto_in_r_bits_id; // @[Buffer.scala 58:29]
  wire [63:0] axi4buf_auto_in_r_bits_data; // @[Buffer.scala 58:29]
  wire [1:0] axi4buf_auto_in_r_bits_resp; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_in_r_bits_last; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_aw_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_aw_valid; // @[Buffer.scala 58:29]
  wire [15:0] axi4buf_auto_out_aw_bits_id; // @[Buffer.scala 58:29]
  wire [36:0] axi4buf_auto_out_aw_bits_addr; // @[Buffer.scala 58:29]
  wire [7:0] axi4buf_auto_out_aw_bits_len; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_out_aw_bits_size; // @[Buffer.scala 58:29]
  wire [1:0] axi4buf_auto_out_aw_bits_burst; // @[Buffer.scala 58:29]
  wire [3:0] axi4buf_auto_out_aw_bits_cache; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_out_aw_bits_prot; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_w_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_w_valid; // @[Buffer.scala 58:29]
  wire [63:0] axi4buf_auto_out_w_bits_data; // @[Buffer.scala 58:29]
  wire [7:0] axi4buf_auto_out_w_bits_strb; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_w_bits_last; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_b_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_b_valid; // @[Buffer.scala 58:29]
  wire [15:0] axi4buf_auto_out_b_bits_id; // @[Buffer.scala 58:29]
  wire [1:0] axi4buf_auto_out_b_bits_resp; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_ar_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_ar_valid; // @[Buffer.scala 58:29]
  wire [15:0] axi4buf_auto_out_ar_bits_id; // @[Buffer.scala 58:29]
  wire [36:0] axi4buf_auto_out_ar_bits_addr; // @[Buffer.scala 58:29]
  wire [7:0] axi4buf_auto_out_ar_bits_len; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_out_ar_bits_size; // @[Buffer.scala 58:29]
  wire [1:0] axi4buf_auto_out_ar_bits_burst; // @[Buffer.scala 58:29]
  wire [3:0] axi4buf_auto_out_ar_bits_cache; // @[Buffer.scala 58:29]
  wire [2:0] axi4buf_auto_out_ar_bits_prot; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_r_ready; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_r_valid; // @[Buffer.scala 58:29]
  wire [15:0] axi4buf_auto_out_r_bits_id; // @[Buffer.scala 58:29]
  wire [63:0] axi4buf_auto_out_r_bits_data; // @[Buffer.scala 58:29]
  wire [1:0] axi4buf_auto_out_r_bits_resp; // @[Buffer.scala 58:29]
  wire  axi4buf_auto_out_r_bits_last; // @[Buffer.scala 58:29]
  RHEA__TLFIFOFixer_2 fixer ( // @[FIFOFixer.scala 144:27]
    .rf_reset(fixer_rf_reset),
    .clock(fixer_clock),
    .reset(fixer_reset),
    .auto_in_a_ready(fixer_auto_in_a_ready),
    .auto_in_a_valid(fixer_auto_in_a_valid),
    .auto_in_a_bits_opcode(fixer_auto_in_a_bits_opcode),
    .auto_in_a_bits_size(fixer_auto_in_a_bits_size),
    .auto_in_a_bits_source(fixer_auto_in_a_bits_source),
    .auto_in_a_bits_address(fixer_auto_in_a_bits_address),
    .auto_in_a_bits_user_amba_prot_bufferable(fixer_auto_in_a_bits_user_amba_prot_bufferable),
    .auto_in_a_bits_user_amba_prot_modifiable(fixer_auto_in_a_bits_user_amba_prot_modifiable),
    .auto_in_a_bits_user_amba_prot_readalloc(fixer_auto_in_a_bits_user_amba_prot_readalloc),
    .auto_in_a_bits_user_amba_prot_writealloc(fixer_auto_in_a_bits_user_amba_prot_writealloc),
    .auto_in_a_bits_user_amba_prot_privileged(fixer_auto_in_a_bits_user_amba_prot_privileged),
    .auto_in_a_bits_user_amba_prot_secure(fixer_auto_in_a_bits_user_amba_prot_secure),
    .auto_in_a_bits_user_amba_prot_fetch(fixer_auto_in_a_bits_user_amba_prot_fetch),
    .auto_in_a_bits_mask(fixer_auto_in_a_bits_mask),
    .auto_in_a_bits_data(fixer_auto_in_a_bits_data),
    .auto_in_d_ready(fixer_auto_in_d_ready),
    .auto_in_d_valid(fixer_auto_in_d_valid),
    .auto_in_d_bits_opcode(fixer_auto_in_d_bits_opcode),
    .auto_in_d_bits_size(fixer_auto_in_d_bits_size),
    .auto_in_d_bits_source(fixer_auto_in_d_bits_source),
    .auto_in_d_bits_denied(fixer_auto_in_d_bits_denied),
    .auto_in_d_bits_data(fixer_auto_in_d_bits_data),
    .auto_in_d_bits_corrupt(fixer_auto_in_d_bits_corrupt),
    .auto_out_a_ready(fixer_auto_out_a_ready),
    .auto_out_a_valid(fixer_auto_out_a_valid),
    .auto_out_a_bits_opcode(fixer_auto_out_a_bits_opcode),
    .auto_out_a_bits_size(fixer_auto_out_a_bits_size),
    .auto_out_a_bits_source(fixer_auto_out_a_bits_source),
    .auto_out_a_bits_address(fixer_auto_out_a_bits_address),
    .auto_out_a_bits_user_amba_prot_bufferable(fixer_auto_out_a_bits_user_amba_prot_bufferable),
    .auto_out_a_bits_user_amba_prot_modifiable(fixer_auto_out_a_bits_user_amba_prot_modifiable),
    .auto_out_a_bits_user_amba_prot_readalloc(fixer_auto_out_a_bits_user_amba_prot_readalloc),
    .auto_out_a_bits_user_amba_prot_writealloc(fixer_auto_out_a_bits_user_amba_prot_writealloc),
    .auto_out_a_bits_user_amba_prot_privileged(fixer_auto_out_a_bits_user_amba_prot_privileged),
    .auto_out_a_bits_user_amba_prot_secure(fixer_auto_out_a_bits_user_amba_prot_secure),
    .auto_out_a_bits_user_amba_prot_fetch(fixer_auto_out_a_bits_user_amba_prot_fetch),
    .auto_out_a_bits_mask(fixer_auto_out_a_bits_mask),
    .auto_out_a_bits_data(fixer_auto_out_a_bits_data),
    .auto_out_d_ready(fixer_auto_out_d_ready),
    .auto_out_d_valid(fixer_auto_out_d_valid),
    .auto_out_d_bits_opcode(fixer_auto_out_d_bits_opcode),
    .auto_out_d_bits_param(fixer_auto_out_d_bits_param),
    .auto_out_d_bits_size(fixer_auto_out_d_bits_size),
    .auto_out_d_bits_source(fixer_auto_out_d_bits_source),
    .auto_out_d_bits_sink(fixer_auto_out_d_bits_sink),
    .auto_out_d_bits_denied(fixer_auto_out_d_bits_denied),
    .auto_out_d_bits_data(fixer_auto_out_d_bits_data),
    .auto_out_d_bits_corrupt(fixer_auto_out_d_bits_corrupt)
  );
  RHEA__AXI4ToTL axi42tl ( // @[ToTL.scala 217:29]
    .rf_reset(axi42tl_rf_reset),
    .clock(axi42tl_clock),
    .reset(axi42tl_reset),
    .auto_in_aw_ready(axi42tl_auto_in_aw_ready),
    .auto_in_aw_valid(axi42tl_auto_in_aw_valid),
    .auto_in_aw_bits_id(axi42tl_auto_in_aw_bits_id),
    .auto_in_aw_bits_addr(axi42tl_auto_in_aw_bits_addr),
    .auto_in_aw_bits_len(axi42tl_auto_in_aw_bits_len),
    .auto_in_aw_bits_size(axi42tl_auto_in_aw_bits_size),
    .auto_in_aw_bits_cache(axi42tl_auto_in_aw_bits_cache),
    .auto_in_aw_bits_prot(axi42tl_auto_in_aw_bits_prot),
    .auto_in_w_ready(axi42tl_auto_in_w_ready),
    .auto_in_w_valid(axi42tl_auto_in_w_valid),
    .auto_in_w_bits_data(axi42tl_auto_in_w_bits_data),
    .auto_in_w_bits_strb(axi42tl_auto_in_w_bits_strb),
    .auto_in_w_bits_last(axi42tl_auto_in_w_bits_last),
    .auto_in_b_ready(axi42tl_auto_in_b_ready),
    .auto_in_b_valid(axi42tl_auto_in_b_valid),
    .auto_in_b_bits_id(axi42tl_auto_in_b_bits_id),
    .auto_in_b_bits_resp(axi42tl_auto_in_b_bits_resp),
    .auto_in_ar_ready(axi42tl_auto_in_ar_ready),
    .auto_in_ar_valid(axi42tl_auto_in_ar_valid),
    .auto_in_ar_bits_id(axi42tl_auto_in_ar_bits_id),
    .auto_in_ar_bits_addr(axi42tl_auto_in_ar_bits_addr),
    .auto_in_ar_bits_len(axi42tl_auto_in_ar_bits_len),
    .auto_in_ar_bits_size(axi42tl_auto_in_ar_bits_size),
    .auto_in_ar_bits_cache(axi42tl_auto_in_ar_bits_cache),
    .auto_in_ar_bits_prot(axi42tl_auto_in_ar_bits_prot),
    .auto_in_r_ready(axi42tl_auto_in_r_ready),
    .auto_in_r_valid(axi42tl_auto_in_r_valid),
    .auto_in_r_bits_id(axi42tl_auto_in_r_bits_id),
    .auto_in_r_bits_data(axi42tl_auto_in_r_bits_data),
    .auto_in_r_bits_resp(axi42tl_auto_in_r_bits_resp),
    .auto_in_r_bits_last(axi42tl_auto_in_r_bits_last),
    .auto_out_a_ready(axi42tl_auto_out_a_ready),
    .auto_out_a_valid(axi42tl_auto_out_a_valid),
    .auto_out_a_bits_opcode(axi42tl_auto_out_a_bits_opcode),
    .auto_out_a_bits_size(axi42tl_auto_out_a_bits_size),
    .auto_out_a_bits_source(axi42tl_auto_out_a_bits_source),
    .auto_out_a_bits_address(axi42tl_auto_out_a_bits_address),
    .auto_out_a_bits_user_amba_prot_bufferable(axi42tl_auto_out_a_bits_user_amba_prot_bufferable),
    .auto_out_a_bits_user_amba_prot_modifiable(axi42tl_auto_out_a_bits_user_amba_prot_modifiable),
    .auto_out_a_bits_user_amba_prot_readalloc(axi42tl_auto_out_a_bits_user_amba_prot_readalloc),
    .auto_out_a_bits_user_amba_prot_writealloc(axi42tl_auto_out_a_bits_user_amba_prot_writealloc),
    .auto_out_a_bits_user_amba_prot_privileged(axi42tl_auto_out_a_bits_user_amba_prot_privileged),
    .auto_out_a_bits_user_amba_prot_secure(axi42tl_auto_out_a_bits_user_amba_prot_secure),
    .auto_out_a_bits_user_amba_prot_fetch(axi42tl_auto_out_a_bits_user_amba_prot_fetch),
    .auto_out_a_bits_mask(axi42tl_auto_out_a_bits_mask),
    .auto_out_a_bits_data(axi42tl_auto_out_a_bits_data),
    .auto_out_d_ready(axi42tl_auto_out_d_ready),
    .auto_out_d_valid(axi42tl_auto_out_d_valid),
    .auto_out_d_bits_opcode(axi42tl_auto_out_d_bits_opcode),
    .auto_out_d_bits_size(axi42tl_auto_out_d_bits_size),
    .auto_out_d_bits_source(axi42tl_auto_out_d_bits_source),
    .auto_out_d_bits_denied(axi42tl_auto_out_d_bits_denied),
    .auto_out_d_bits_data(axi42tl_auto_out_d_bits_data),
    .auto_out_d_bits_corrupt(axi42tl_auto_out_d_bits_corrupt)
  );
  RHEA__AXI4UserYanker_1 axi4yank ( // @[UserYanker.scala 113:30]
    .rf_reset(axi4yank_rf_reset),
    .clock(axi4yank_clock),
    .reset(axi4yank_reset),
    .auto_in_aw_ready(axi4yank_auto_in_aw_ready),
    .auto_in_aw_valid(axi4yank_auto_in_aw_valid),
    .auto_in_aw_bits_id(axi4yank_auto_in_aw_bits_id),
    .auto_in_aw_bits_addr(axi4yank_auto_in_aw_bits_addr),
    .auto_in_aw_bits_len(axi4yank_auto_in_aw_bits_len),
    .auto_in_aw_bits_size(axi4yank_auto_in_aw_bits_size),
    .auto_in_aw_bits_cache(axi4yank_auto_in_aw_bits_cache),
    .auto_in_aw_bits_prot(axi4yank_auto_in_aw_bits_prot),
    .auto_in_aw_bits_echo_extra_id(axi4yank_auto_in_aw_bits_echo_extra_id),
    .auto_in_aw_bits_echo_real_last(axi4yank_auto_in_aw_bits_echo_real_last),
    .auto_in_w_ready(axi4yank_auto_in_w_ready),
    .auto_in_w_valid(axi4yank_auto_in_w_valid),
    .auto_in_w_bits_data(axi4yank_auto_in_w_bits_data),
    .auto_in_w_bits_strb(axi4yank_auto_in_w_bits_strb),
    .auto_in_w_bits_last(axi4yank_auto_in_w_bits_last),
    .auto_in_b_ready(axi4yank_auto_in_b_ready),
    .auto_in_b_valid(axi4yank_auto_in_b_valid),
    .auto_in_b_bits_id(axi4yank_auto_in_b_bits_id),
    .auto_in_b_bits_resp(axi4yank_auto_in_b_bits_resp),
    .auto_in_b_bits_echo_extra_id(axi4yank_auto_in_b_bits_echo_extra_id),
    .auto_in_b_bits_echo_real_last(axi4yank_auto_in_b_bits_echo_real_last),
    .auto_in_ar_ready(axi4yank_auto_in_ar_ready),
    .auto_in_ar_valid(axi4yank_auto_in_ar_valid),
    .auto_in_ar_bits_id(axi4yank_auto_in_ar_bits_id),
    .auto_in_ar_bits_addr(axi4yank_auto_in_ar_bits_addr),
    .auto_in_ar_bits_len(axi4yank_auto_in_ar_bits_len),
    .auto_in_ar_bits_size(axi4yank_auto_in_ar_bits_size),
    .auto_in_ar_bits_cache(axi4yank_auto_in_ar_bits_cache),
    .auto_in_ar_bits_prot(axi4yank_auto_in_ar_bits_prot),
    .auto_in_ar_bits_echo_extra_id(axi4yank_auto_in_ar_bits_echo_extra_id),
    .auto_in_ar_bits_echo_real_last(axi4yank_auto_in_ar_bits_echo_real_last),
    .auto_in_r_ready(axi4yank_auto_in_r_ready),
    .auto_in_r_valid(axi4yank_auto_in_r_valid),
    .auto_in_r_bits_id(axi4yank_auto_in_r_bits_id),
    .auto_in_r_bits_data(axi4yank_auto_in_r_bits_data),
    .auto_in_r_bits_resp(axi4yank_auto_in_r_bits_resp),
    .auto_in_r_bits_echo_extra_id(axi4yank_auto_in_r_bits_echo_extra_id),
    .auto_in_r_bits_echo_real_last(axi4yank_auto_in_r_bits_echo_real_last),
    .auto_in_r_bits_last(axi4yank_auto_in_r_bits_last),
    .auto_out_aw_ready(axi4yank_auto_out_aw_ready),
    .auto_out_aw_valid(axi4yank_auto_out_aw_valid),
    .auto_out_aw_bits_id(axi4yank_auto_out_aw_bits_id),
    .auto_out_aw_bits_addr(axi4yank_auto_out_aw_bits_addr),
    .auto_out_aw_bits_len(axi4yank_auto_out_aw_bits_len),
    .auto_out_aw_bits_size(axi4yank_auto_out_aw_bits_size),
    .auto_out_aw_bits_cache(axi4yank_auto_out_aw_bits_cache),
    .auto_out_aw_bits_prot(axi4yank_auto_out_aw_bits_prot),
    .auto_out_w_ready(axi4yank_auto_out_w_ready),
    .auto_out_w_valid(axi4yank_auto_out_w_valid),
    .auto_out_w_bits_data(axi4yank_auto_out_w_bits_data),
    .auto_out_w_bits_strb(axi4yank_auto_out_w_bits_strb),
    .auto_out_w_bits_last(axi4yank_auto_out_w_bits_last),
    .auto_out_b_ready(axi4yank_auto_out_b_ready),
    .auto_out_b_valid(axi4yank_auto_out_b_valid),
    .auto_out_b_bits_id(axi4yank_auto_out_b_bits_id),
    .auto_out_b_bits_resp(axi4yank_auto_out_b_bits_resp),
    .auto_out_ar_ready(axi4yank_auto_out_ar_ready),
    .auto_out_ar_valid(axi4yank_auto_out_ar_valid),
    .auto_out_ar_bits_id(axi4yank_auto_out_ar_bits_id),
    .auto_out_ar_bits_addr(axi4yank_auto_out_ar_bits_addr),
    .auto_out_ar_bits_len(axi4yank_auto_out_ar_bits_len),
    .auto_out_ar_bits_size(axi4yank_auto_out_ar_bits_size),
    .auto_out_ar_bits_cache(axi4yank_auto_out_ar_bits_cache),
    .auto_out_ar_bits_prot(axi4yank_auto_out_ar_bits_prot),
    .auto_out_r_ready(axi4yank_auto_out_r_ready),
    .auto_out_r_valid(axi4yank_auto_out_r_valid),
    .auto_out_r_bits_id(axi4yank_auto_out_r_bits_id),
    .auto_out_r_bits_data(axi4yank_auto_out_r_bits_data),
    .auto_out_r_bits_resp(axi4yank_auto_out_r_bits_resp),
    .auto_out_r_bits_last(axi4yank_auto_out_r_bits_last)
  );
  RHEA__AXI4Fragmenter axi4frag ( // @[Fragmenter.scala 205:30]
    .rf_reset(axi4frag_rf_reset),
    .clock(axi4frag_clock),
    .reset(axi4frag_reset),
    .auto_in_aw_ready(axi4frag_auto_in_aw_ready),
    .auto_in_aw_valid(axi4frag_auto_in_aw_valid),
    .auto_in_aw_bits_id(axi4frag_auto_in_aw_bits_id),
    .auto_in_aw_bits_addr(axi4frag_auto_in_aw_bits_addr),
    .auto_in_aw_bits_len(axi4frag_auto_in_aw_bits_len),
    .auto_in_aw_bits_size(axi4frag_auto_in_aw_bits_size),
    .auto_in_aw_bits_burst(axi4frag_auto_in_aw_bits_burst),
    .auto_in_aw_bits_cache(axi4frag_auto_in_aw_bits_cache),
    .auto_in_aw_bits_prot(axi4frag_auto_in_aw_bits_prot),
    .auto_in_aw_bits_echo_extra_id(axi4frag_auto_in_aw_bits_echo_extra_id),
    .auto_in_w_ready(axi4frag_auto_in_w_ready),
    .auto_in_w_valid(axi4frag_auto_in_w_valid),
    .auto_in_w_bits_data(axi4frag_auto_in_w_bits_data),
    .auto_in_w_bits_strb(axi4frag_auto_in_w_bits_strb),
    .auto_in_w_bits_last(axi4frag_auto_in_w_bits_last),
    .auto_in_b_ready(axi4frag_auto_in_b_ready),
    .auto_in_b_valid(axi4frag_auto_in_b_valid),
    .auto_in_b_bits_id(axi4frag_auto_in_b_bits_id),
    .auto_in_b_bits_resp(axi4frag_auto_in_b_bits_resp),
    .auto_in_b_bits_echo_extra_id(axi4frag_auto_in_b_bits_echo_extra_id),
    .auto_in_ar_ready(axi4frag_auto_in_ar_ready),
    .auto_in_ar_valid(axi4frag_auto_in_ar_valid),
    .auto_in_ar_bits_id(axi4frag_auto_in_ar_bits_id),
    .auto_in_ar_bits_addr(axi4frag_auto_in_ar_bits_addr),
    .auto_in_ar_bits_len(axi4frag_auto_in_ar_bits_len),
    .auto_in_ar_bits_size(axi4frag_auto_in_ar_bits_size),
    .auto_in_ar_bits_burst(axi4frag_auto_in_ar_bits_burst),
    .auto_in_ar_bits_cache(axi4frag_auto_in_ar_bits_cache),
    .auto_in_ar_bits_prot(axi4frag_auto_in_ar_bits_prot),
    .auto_in_ar_bits_echo_extra_id(axi4frag_auto_in_ar_bits_echo_extra_id),
    .auto_in_r_ready(axi4frag_auto_in_r_ready),
    .auto_in_r_valid(axi4frag_auto_in_r_valid),
    .auto_in_r_bits_id(axi4frag_auto_in_r_bits_id),
    .auto_in_r_bits_data(axi4frag_auto_in_r_bits_data),
    .auto_in_r_bits_resp(axi4frag_auto_in_r_bits_resp),
    .auto_in_r_bits_echo_extra_id(axi4frag_auto_in_r_bits_echo_extra_id),
    .auto_in_r_bits_last(axi4frag_auto_in_r_bits_last),
    .auto_out_aw_ready(axi4frag_auto_out_aw_ready),
    .auto_out_aw_valid(axi4frag_auto_out_aw_valid),
    .auto_out_aw_bits_id(axi4frag_auto_out_aw_bits_id),
    .auto_out_aw_bits_addr(axi4frag_auto_out_aw_bits_addr),
    .auto_out_aw_bits_len(axi4frag_auto_out_aw_bits_len),
    .auto_out_aw_bits_size(axi4frag_auto_out_aw_bits_size),
    .auto_out_aw_bits_cache(axi4frag_auto_out_aw_bits_cache),
    .auto_out_aw_bits_prot(axi4frag_auto_out_aw_bits_prot),
    .auto_out_aw_bits_echo_extra_id(axi4frag_auto_out_aw_bits_echo_extra_id),
    .auto_out_aw_bits_echo_real_last(axi4frag_auto_out_aw_bits_echo_real_last),
    .auto_out_w_ready(axi4frag_auto_out_w_ready),
    .auto_out_w_valid(axi4frag_auto_out_w_valid),
    .auto_out_w_bits_data(axi4frag_auto_out_w_bits_data),
    .auto_out_w_bits_strb(axi4frag_auto_out_w_bits_strb),
    .auto_out_w_bits_last(axi4frag_auto_out_w_bits_last),
    .auto_out_b_ready(axi4frag_auto_out_b_ready),
    .auto_out_b_valid(axi4frag_auto_out_b_valid),
    .auto_out_b_bits_id(axi4frag_auto_out_b_bits_id),
    .auto_out_b_bits_resp(axi4frag_auto_out_b_bits_resp),
    .auto_out_b_bits_echo_extra_id(axi4frag_auto_out_b_bits_echo_extra_id),
    .auto_out_b_bits_echo_real_last(axi4frag_auto_out_b_bits_echo_real_last),
    .auto_out_ar_ready(axi4frag_auto_out_ar_ready),
    .auto_out_ar_valid(axi4frag_auto_out_ar_valid),
    .auto_out_ar_bits_id(axi4frag_auto_out_ar_bits_id),
    .auto_out_ar_bits_addr(axi4frag_auto_out_ar_bits_addr),
    .auto_out_ar_bits_len(axi4frag_auto_out_ar_bits_len),
    .auto_out_ar_bits_size(axi4frag_auto_out_ar_bits_size),
    .auto_out_ar_bits_cache(axi4frag_auto_out_ar_bits_cache),
    .auto_out_ar_bits_prot(axi4frag_auto_out_ar_bits_prot),
    .auto_out_ar_bits_echo_extra_id(axi4frag_auto_out_ar_bits_echo_extra_id),
    .auto_out_ar_bits_echo_real_last(axi4frag_auto_out_ar_bits_echo_real_last),
    .auto_out_r_ready(axi4frag_auto_out_r_ready),
    .auto_out_r_valid(axi4frag_auto_out_r_valid),
    .auto_out_r_bits_id(axi4frag_auto_out_r_bits_id),
    .auto_out_r_bits_data(axi4frag_auto_out_r_bits_data),
    .auto_out_r_bits_resp(axi4frag_auto_out_r_bits_resp),
    .auto_out_r_bits_echo_extra_id(axi4frag_auto_out_r_bits_echo_extra_id),
    .auto_out_r_bits_echo_real_last(axi4frag_auto_out_r_bits_echo_real_last),
    .auto_out_r_bits_last(axi4frag_auto_out_r_bits_last)
  );
  RHEA__AXI4IdIndexer_1 axi4index ( // @[IdIndexer.scala 107:31]
    .auto_in_aw_ready(axi4index_auto_in_aw_ready),
    .auto_in_aw_valid(axi4index_auto_in_aw_valid),
    .auto_in_aw_bits_id(axi4index_auto_in_aw_bits_id),
    .auto_in_aw_bits_addr(axi4index_auto_in_aw_bits_addr),
    .auto_in_aw_bits_len(axi4index_auto_in_aw_bits_len),
    .auto_in_aw_bits_size(axi4index_auto_in_aw_bits_size),
    .auto_in_aw_bits_burst(axi4index_auto_in_aw_bits_burst),
    .auto_in_aw_bits_cache(axi4index_auto_in_aw_bits_cache),
    .auto_in_aw_bits_prot(axi4index_auto_in_aw_bits_prot),
    .auto_in_w_ready(axi4index_auto_in_w_ready),
    .auto_in_w_valid(axi4index_auto_in_w_valid),
    .auto_in_w_bits_data(axi4index_auto_in_w_bits_data),
    .auto_in_w_bits_strb(axi4index_auto_in_w_bits_strb),
    .auto_in_w_bits_last(axi4index_auto_in_w_bits_last),
    .auto_in_b_ready(axi4index_auto_in_b_ready),
    .auto_in_b_valid(axi4index_auto_in_b_valid),
    .auto_in_b_bits_id(axi4index_auto_in_b_bits_id),
    .auto_in_b_bits_resp(axi4index_auto_in_b_bits_resp),
    .auto_in_ar_ready(axi4index_auto_in_ar_ready),
    .auto_in_ar_valid(axi4index_auto_in_ar_valid),
    .auto_in_ar_bits_id(axi4index_auto_in_ar_bits_id),
    .auto_in_ar_bits_addr(axi4index_auto_in_ar_bits_addr),
    .auto_in_ar_bits_len(axi4index_auto_in_ar_bits_len),
    .auto_in_ar_bits_size(axi4index_auto_in_ar_bits_size),
    .auto_in_ar_bits_burst(axi4index_auto_in_ar_bits_burst),
    .auto_in_ar_bits_cache(axi4index_auto_in_ar_bits_cache),
    .auto_in_ar_bits_prot(axi4index_auto_in_ar_bits_prot),
    .auto_in_r_ready(axi4index_auto_in_r_ready),
    .auto_in_r_valid(axi4index_auto_in_r_valid),
    .auto_in_r_bits_id(axi4index_auto_in_r_bits_id),
    .auto_in_r_bits_data(axi4index_auto_in_r_bits_data),
    .auto_in_r_bits_resp(axi4index_auto_in_r_bits_resp),
    .auto_in_r_bits_last(axi4index_auto_in_r_bits_last),
    .auto_out_aw_ready(axi4index_auto_out_aw_ready),
    .auto_out_aw_valid(axi4index_auto_out_aw_valid),
    .auto_out_aw_bits_id(axi4index_auto_out_aw_bits_id),
    .auto_out_aw_bits_addr(axi4index_auto_out_aw_bits_addr),
    .auto_out_aw_bits_len(axi4index_auto_out_aw_bits_len),
    .auto_out_aw_bits_size(axi4index_auto_out_aw_bits_size),
    .auto_out_aw_bits_burst(axi4index_auto_out_aw_bits_burst),
    .auto_out_aw_bits_cache(axi4index_auto_out_aw_bits_cache),
    .auto_out_aw_bits_prot(axi4index_auto_out_aw_bits_prot),
    .auto_out_aw_bits_echo_extra_id(axi4index_auto_out_aw_bits_echo_extra_id),
    .auto_out_w_ready(axi4index_auto_out_w_ready),
    .auto_out_w_valid(axi4index_auto_out_w_valid),
    .auto_out_w_bits_data(axi4index_auto_out_w_bits_data),
    .auto_out_w_bits_strb(axi4index_auto_out_w_bits_strb),
    .auto_out_w_bits_last(axi4index_auto_out_w_bits_last),
    .auto_out_b_ready(axi4index_auto_out_b_ready),
    .auto_out_b_valid(axi4index_auto_out_b_valid),
    .auto_out_b_bits_id(axi4index_auto_out_b_bits_id),
    .auto_out_b_bits_resp(axi4index_auto_out_b_bits_resp),
    .auto_out_b_bits_echo_extra_id(axi4index_auto_out_b_bits_echo_extra_id),
    .auto_out_ar_ready(axi4index_auto_out_ar_ready),
    .auto_out_ar_valid(axi4index_auto_out_ar_valid),
    .auto_out_ar_bits_id(axi4index_auto_out_ar_bits_id),
    .auto_out_ar_bits_addr(axi4index_auto_out_ar_bits_addr),
    .auto_out_ar_bits_len(axi4index_auto_out_ar_bits_len),
    .auto_out_ar_bits_size(axi4index_auto_out_ar_bits_size),
    .auto_out_ar_bits_burst(axi4index_auto_out_ar_bits_burst),
    .auto_out_ar_bits_cache(axi4index_auto_out_ar_bits_cache),
    .auto_out_ar_bits_prot(axi4index_auto_out_ar_bits_prot),
    .auto_out_ar_bits_echo_extra_id(axi4index_auto_out_ar_bits_echo_extra_id),
    .auto_out_r_ready(axi4index_auto_out_r_ready),
    .auto_out_r_valid(axi4index_auto_out_r_valid),
    .auto_out_r_bits_id(axi4index_auto_out_r_bits_id),
    .auto_out_r_bits_data(axi4index_auto_out_r_bits_data),
    .auto_out_r_bits_resp(axi4index_auto_out_r_bits_resp),
    .auto_out_r_bits_echo_extra_id(axi4index_auto_out_r_bits_echo_extra_id),
    .auto_out_r_bits_last(axi4index_auto_out_r_bits_last)
  );
  RHEA__AXI4Buffer_1 axi4buf ( // @[Buffer.scala 58:29]
    .rf_reset(axi4buf_rf_reset),
    .clock(axi4buf_clock),
    .reset(axi4buf_reset),
    .auto_in_aw_ready(axi4buf_auto_in_aw_ready),
    .auto_in_aw_valid(axi4buf_auto_in_aw_valid),
    .auto_in_aw_bits_id(axi4buf_auto_in_aw_bits_id),
    .auto_in_aw_bits_addr(axi4buf_auto_in_aw_bits_addr),
    .auto_in_aw_bits_len(axi4buf_auto_in_aw_bits_len),
    .auto_in_aw_bits_size(axi4buf_auto_in_aw_bits_size),
    .auto_in_aw_bits_burst(axi4buf_auto_in_aw_bits_burst),
    .auto_in_aw_bits_cache(axi4buf_auto_in_aw_bits_cache),
    .auto_in_aw_bits_prot(axi4buf_auto_in_aw_bits_prot),
    .auto_in_w_ready(axi4buf_auto_in_w_ready),
    .auto_in_w_valid(axi4buf_auto_in_w_valid),
    .auto_in_w_bits_data(axi4buf_auto_in_w_bits_data),
    .auto_in_w_bits_strb(axi4buf_auto_in_w_bits_strb),
    .auto_in_w_bits_last(axi4buf_auto_in_w_bits_last),
    .auto_in_b_ready(axi4buf_auto_in_b_ready),
    .auto_in_b_valid(axi4buf_auto_in_b_valid),
    .auto_in_b_bits_id(axi4buf_auto_in_b_bits_id),
    .auto_in_b_bits_resp(axi4buf_auto_in_b_bits_resp),
    .auto_in_ar_ready(axi4buf_auto_in_ar_ready),
    .auto_in_ar_valid(axi4buf_auto_in_ar_valid),
    .auto_in_ar_bits_id(axi4buf_auto_in_ar_bits_id),
    .auto_in_ar_bits_addr(axi4buf_auto_in_ar_bits_addr),
    .auto_in_ar_bits_len(axi4buf_auto_in_ar_bits_len),
    .auto_in_ar_bits_size(axi4buf_auto_in_ar_bits_size),
    .auto_in_ar_bits_burst(axi4buf_auto_in_ar_bits_burst),
    .auto_in_ar_bits_cache(axi4buf_auto_in_ar_bits_cache),
    .auto_in_ar_bits_prot(axi4buf_auto_in_ar_bits_prot),
    .auto_in_r_ready(axi4buf_auto_in_r_ready),
    .auto_in_r_valid(axi4buf_auto_in_r_valid),
    .auto_in_r_bits_id(axi4buf_auto_in_r_bits_id),
    .auto_in_r_bits_data(axi4buf_auto_in_r_bits_data),
    .auto_in_r_bits_resp(axi4buf_auto_in_r_bits_resp),
    .auto_in_r_bits_last(axi4buf_auto_in_r_bits_last),
    .auto_out_aw_ready(axi4buf_auto_out_aw_ready),
    .auto_out_aw_valid(axi4buf_auto_out_aw_valid),
    .auto_out_aw_bits_id(axi4buf_auto_out_aw_bits_id),
    .auto_out_aw_bits_addr(axi4buf_auto_out_aw_bits_addr),
    .auto_out_aw_bits_len(axi4buf_auto_out_aw_bits_len),
    .auto_out_aw_bits_size(axi4buf_auto_out_aw_bits_size),
    .auto_out_aw_bits_burst(axi4buf_auto_out_aw_bits_burst),
    .auto_out_aw_bits_cache(axi4buf_auto_out_aw_bits_cache),
    .auto_out_aw_bits_prot(axi4buf_auto_out_aw_bits_prot),
    .auto_out_w_ready(axi4buf_auto_out_w_ready),
    .auto_out_w_valid(axi4buf_auto_out_w_valid),
    .auto_out_w_bits_data(axi4buf_auto_out_w_bits_data),
    .auto_out_w_bits_strb(axi4buf_auto_out_w_bits_strb),
    .auto_out_w_bits_last(axi4buf_auto_out_w_bits_last),
    .auto_out_b_ready(axi4buf_auto_out_b_ready),
    .auto_out_b_valid(axi4buf_auto_out_b_valid),
    .auto_out_b_bits_id(axi4buf_auto_out_b_bits_id),
    .auto_out_b_bits_resp(axi4buf_auto_out_b_bits_resp),
    .auto_out_ar_ready(axi4buf_auto_out_ar_ready),
    .auto_out_ar_valid(axi4buf_auto_out_ar_valid),
    .auto_out_ar_bits_id(axi4buf_auto_out_ar_bits_id),
    .auto_out_ar_bits_addr(axi4buf_auto_out_ar_bits_addr),
    .auto_out_ar_bits_len(axi4buf_auto_out_ar_bits_len),
    .auto_out_ar_bits_size(axi4buf_auto_out_ar_bits_size),
    .auto_out_ar_bits_burst(axi4buf_auto_out_ar_bits_burst),
    .auto_out_ar_bits_cache(axi4buf_auto_out_ar_bits_cache),
    .auto_out_ar_bits_prot(axi4buf_auto_out_ar_bits_prot),
    .auto_out_r_ready(axi4buf_auto_out_r_ready),
    .auto_out_r_valid(axi4buf_auto_out_r_valid),
    .auto_out_r_bits_id(axi4buf_auto_out_r_bits_id),
    .auto_out_r_bits_data(axi4buf_auto_out_r_bits_data),
    .auto_out_r_bits_resp(axi4buf_auto_out_r_bits_resp),
    .auto_out_r_bits_last(axi4buf_auto_out_r_bits_last)
  );
  assign buffer_auto_in_a_ready = buffer_auto_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_valid = buffer_auto_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_opcode = buffer_auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_param = buffer_auto_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_size = buffer_auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_source = buffer_auto_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_sink = buffer_auto_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_denied = buffer_auto_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_data = buffer_auto_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_corrupt = buffer_auto_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_a_valid = buffer_auto_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_opcode = buffer_auto_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_size = buffer_auto_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_source = buffer_auto_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_address = buffer_auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_bufferable = buffer_auto_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_modifiable = buffer_auto_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_readalloc = buffer_auto_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_writealloc = buffer_auto_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_privileged = buffer_auto_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_secure = buffer_auto_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_user_amba_prot_fetch = buffer_auto_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_mask = buffer_auto_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_data = buffer_auto_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_d_ready = buffer_auto_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fixer_rf_reset = rf_reset;
  assign widget_auto_in_a_ready = widget_auto_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_in_d_valid = widget_auto_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_in_d_bits_opcode = widget_auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_in_d_bits_size = widget_auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_in_d_bits_source = widget_auto_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_in_d_bits_denied = widget_auto_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_in_d_bits_data = widget_auto_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_in_d_bits_corrupt = widget_auto_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign widget_auto_out_a_valid = widget_auto_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_opcode = widget_auto_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_size = widget_auto_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_source = widget_auto_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_address = widget_auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_user_amba_prot_bufferable = widget_auto_in_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_user_amba_prot_modifiable = widget_auto_in_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_user_amba_prot_readalloc = widget_auto_in_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_user_amba_prot_writealloc = widget_auto_in_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_user_amba_prot_privileged = widget_auto_in_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_user_amba_prot_secure = widget_auto_in_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_user_amba_prot_fetch = widget_auto_in_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_mask = widget_auto_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_a_bits_data = widget_auto_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign widget_auto_out_d_ready = widget_auto_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi42tl_rf_reset = rf_reset;
  assign axi4yank_rf_reset = rf_reset;
  assign axi4frag_rf_reset = rf_reset;
  assign axi4buf_rf_reset = rf_reset;
  assign auto_tl_out_a_valid = buffer_auto_out_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_opcode = buffer_auto_out_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_size = buffer_auto_out_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_source = buffer_auto_out_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_address = buffer_auto_out_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_user_amba_prot_bufferable = buffer_auto_out_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_user_amba_prot_modifiable = buffer_auto_out_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_user_amba_prot_readalloc = buffer_auto_out_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_user_amba_prot_writealloc = buffer_auto_out_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_user_amba_prot_privileged = buffer_auto_out_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_user_amba_prot_secure = buffer_auto_out_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_user_amba_prot_fetch = buffer_auto_out_a_bits_user_amba_prot_fetch; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_mask = buffer_auto_out_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_a_bits_data = buffer_auto_out_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_tl_out_d_ready = buffer_auto_out_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 375:16]
  assign auto_axi4_in_aw_ready = axi4buf_auto_in_aw_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_axi4_in_w_ready = axi4buf_auto_in_w_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_axi4_in_b_valid = axi4buf_auto_in_b_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_axi4_in_b_bits_id = axi4buf_auto_in_b_bits_id; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_axi4_in_b_bits_resp = axi4buf_auto_in_b_bits_resp; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_axi4_in_ar_ready = axi4buf_auto_in_ar_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_axi4_in_r_valid = axi4buf_auto_in_r_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_axi4_in_r_bits_id = axi4buf_auto_in_r_bits_id; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_axi4_in_r_bits_data = axi4buf_auto_in_r_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_axi4_in_r_bits_resp = axi4buf_auto_in_r_bits_resp; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_axi4_in_r_bits_last = axi4buf_auto_in_r_bits_last; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign buffer_auto_in_a_valid = fixer_auto_out_a_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_opcode = fixer_auto_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_size = fixer_auto_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_source = fixer_auto_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_address = fixer_auto_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_bufferable = fixer_auto_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_modifiable = fixer_auto_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_readalloc = fixer_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_writealloc = fixer_auto_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_privileged = fixer_auto_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_secure = fixer_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_user_amba_prot_fetch = fixer_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_mask = fixer_auto_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_a_bits_data = fixer_auto_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_in_d_ready = fixer_auto_out_d_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_a_ready = auto_tl_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_valid = auto_tl_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_opcode = auto_tl_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_param = auto_tl_out_d_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_size = auto_tl_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_source = auto_tl_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_sink = auto_tl_out_d_bits_sink; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_denied = auto_tl_out_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_data = auto_tl_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_d_bits_corrupt = auto_tl_out_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign fixer_clock = clock;
  assign fixer_reset = reset;
  assign fixer_auto_in_a_valid = widget_auto_out_a_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_opcode = widget_auto_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_size = widget_auto_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_source = widget_auto_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_address = widget_auto_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_user_amba_prot_bufferable = widget_auto_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_user_amba_prot_modifiable = widget_auto_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_user_amba_prot_readalloc = widget_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_user_amba_prot_writealloc = widget_auto_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_user_amba_prot_privileged = widget_auto_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_user_amba_prot_secure = widget_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_user_amba_prot_fetch = widget_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_mask = widget_auto_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_a_bits_data = widget_auto_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_d_ready = widget_auto_out_d_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_a_ready = buffer_auto_in_a_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_d_valid = buffer_auto_in_d_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_d_bits_opcode = buffer_auto_in_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_d_bits_param = buffer_auto_in_d_bits_param; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_d_bits_size = buffer_auto_in_d_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_d_bits_source = buffer_auto_in_d_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_d_bits_sink = buffer_auto_in_d_bits_sink; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_d_bits_denied = buffer_auto_in_d_bits_denied; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_d_bits_data = buffer_auto_in_d_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_d_bits_corrupt = buffer_auto_in_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_valid = axi42tl_auto_out_a_valid; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_opcode = axi42tl_auto_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_size = axi42tl_auto_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_source = axi42tl_auto_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_address = axi42tl_auto_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_user_amba_prot_bufferable = axi42tl_auto_out_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_user_amba_prot_modifiable = axi42tl_auto_out_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_user_amba_prot_readalloc = axi42tl_auto_out_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_user_amba_prot_writealloc = axi42tl_auto_out_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_user_amba_prot_privileged = axi42tl_auto_out_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_user_amba_prot_secure = axi42tl_auto_out_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_user_amba_prot_fetch = axi42tl_auto_out_a_bits_user_amba_prot_fetch; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_mask = axi42tl_auto_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign widget_auto_in_a_bits_data = axi42tl_auto_out_a_bits_data; // @[LazyModule.scala 375:16]
  assign widget_auto_in_d_ready = axi42tl_auto_out_d_ready; // @[LazyModule.scala 375:16]
  assign widget_auto_out_a_ready = fixer_auto_in_a_ready; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_valid = fixer_auto_in_d_valid; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_opcode = fixer_auto_in_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_size = fixer_auto_in_d_bits_size; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_source = fixer_auto_in_d_bits_source; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_denied = fixer_auto_in_d_bits_denied; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_data = fixer_auto_in_d_bits_data; // @[LazyModule.scala 375:16]
  assign widget_auto_out_d_bits_corrupt = fixer_auto_in_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign axi42tl_clock = clock;
  assign axi42tl_reset = reset;
  assign axi42tl_auto_in_aw_valid = axi4yank_auto_out_aw_valid; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_aw_bits_id = axi4yank_auto_out_aw_bits_id; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_aw_bits_addr = axi4yank_auto_out_aw_bits_addr; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_aw_bits_len = axi4yank_auto_out_aw_bits_len; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_aw_bits_size = axi4yank_auto_out_aw_bits_size; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_aw_bits_cache = axi4yank_auto_out_aw_bits_cache; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_aw_bits_prot = axi4yank_auto_out_aw_bits_prot; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_w_valid = axi4yank_auto_out_w_valid; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_w_bits_data = axi4yank_auto_out_w_bits_data; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_w_bits_strb = axi4yank_auto_out_w_bits_strb; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_w_bits_last = axi4yank_auto_out_w_bits_last; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_b_ready = axi4yank_auto_out_b_ready; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_ar_valid = axi4yank_auto_out_ar_valid; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_ar_bits_id = axi4yank_auto_out_ar_bits_id; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_ar_bits_addr = axi4yank_auto_out_ar_bits_addr; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_ar_bits_len = axi4yank_auto_out_ar_bits_len; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_ar_bits_size = axi4yank_auto_out_ar_bits_size; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_ar_bits_cache = axi4yank_auto_out_ar_bits_cache; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_ar_bits_prot = axi4yank_auto_out_ar_bits_prot; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_in_r_ready = axi4yank_auto_out_r_ready; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_out_a_ready = widget_auto_in_a_ready; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_out_d_valid = widget_auto_in_d_valid; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_out_d_bits_opcode = widget_auto_in_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_out_d_bits_size = widget_auto_in_d_bits_size; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_out_d_bits_source = widget_auto_in_d_bits_source; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_out_d_bits_denied = widget_auto_in_d_bits_denied; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_out_d_bits_data = widget_auto_in_d_bits_data; // @[LazyModule.scala 375:16]
  assign axi42tl_auto_out_d_bits_corrupt = widget_auto_in_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign axi4yank_clock = clock;
  assign axi4yank_reset = reset;
  assign axi4yank_auto_in_aw_valid = axi4frag_auto_out_aw_valid; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_aw_bits_id = axi4frag_auto_out_aw_bits_id; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_aw_bits_addr = axi4frag_auto_out_aw_bits_addr; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_aw_bits_len = axi4frag_auto_out_aw_bits_len; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_aw_bits_size = axi4frag_auto_out_aw_bits_size; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_aw_bits_cache = axi4frag_auto_out_aw_bits_cache; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_aw_bits_prot = axi4frag_auto_out_aw_bits_prot; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_aw_bits_echo_extra_id = axi4frag_auto_out_aw_bits_echo_extra_id; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_aw_bits_echo_real_last = axi4frag_auto_out_aw_bits_echo_real_last; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_w_valid = axi4frag_auto_out_w_valid; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_w_bits_data = axi4frag_auto_out_w_bits_data; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_w_bits_strb = axi4frag_auto_out_w_bits_strb; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_w_bits_last = axi4frag_auto_out_w_bits_last; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_b_ready = axi4frag_auto_out_b_ready; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_valid = axi4frag_auto_out_ar_valid; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_bits_id = axi4frag_auto_out_ar_bits_id; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_bits_addr = axi4frag_auto_out_ar_bits_addr; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_bits_len = axi4frag_auto_out_ar_bits_len; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_bits_size = axi4frag_auto_out_ar_bits_size; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_bits_cache = axi4frag_auto_out_ar_bits_cache; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_bits_prot = axi4frag_auto_out_ar_bits_prot; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_bits_echo_extra_id = axi4frag_auto_out_ar_bits_echo_extra_id; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_ar_bits_echo_real_last = axi4frag_auto_out_ar_bits_echo_real_last; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_in_r_ready = axi4frag_auto_out_r_ready; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_aw_ready = axi42tl_auto_in_aw_ready; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_w_ready = axi42tl_auto_in_w_ready; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_b_valid = axi42tl_auto_in_b_valid; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_b_bits_id = axi42tl_auto_in_b_bits_id; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_b_bits_resp = axi42tl_auto_in_b_bits_resp; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_ar_ready = axi42tl_auto_in_ar_ready; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_r_valid = axi42tl_auto_in_r_valid; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_r_bits_id = axi42tl_auto_in_r_bits_id; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_r_bits_data = axi42tl_auto_in_r_bits_data; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_r_bits_resp = axi42tl_auto_in_r_bits_resp; // @[LazyModule.scala 375:16]
  assign axi4yank_auto_out_r_bits_last = axi42tl_auto_in_r_bits_last; // @[LazyModule.scala 375:16]
  assign axi4frag_clock = clock;
  assign axi4frag_reset = reset;
  assign axi4frag_auto_in_aw_valid = axi4index_auto_out_aw_valid; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_aw_bits_id = axi4index_auto_out_aw_bits_id; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_aw_bits_addr = axi4index_auto_out_aw_bits_addr; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_aw_bits_len = axi4index_auto_out_aw_bits_len; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_aw_bits_size = axi4index_auto_out_aw_bits_size; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_aw_bits_burst = axi4index_auto_out_aw_bits_burst; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_aw_bits_cache = axi4index_auto_out_aw_bits_cache; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_aw_bits_prot = axi4index_auto_out_aw_bits_prot; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_aw_bits_echo_extra_id = axi4index_auto_out_aw_bits_echo_extra_id; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_w_valid = axi4index_auto_out_w_valid; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_w_bits_data = axi4index_auto_out_w_bits_data; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_w_bits_strb = axi4index_auto_out_w_bits_strb; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_w_bits_last = axi4index_auto_out_w_bits_last; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_b_ready = axi4index_auto_out_b_ready; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_ar_valid = axi4index_auto_out_ar_valid; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_ar_bits_id = axi4index_auto_out_ar_bits_id; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_ar_bits_addr = axi4index_auto_out_ar_bits_addr; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_ar_bits_len = axi4index_auto_out_ar_bits_len; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_ar_bits_size = axi4index_auto_out_ar_bits_size; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_ar_bits_burst = axi4index_auto_out_ar_bits_burst; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_ar_bits_cache = axi4index_auto_out_ar_bits_cache; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_ar_bits_prot = axi4index_auto_out_ar_bits_prot; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_ar_bits_echo_extra_id = axi4index_auto_out_ar_bits_echo_extra_id; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_in_r_ready = axi4index_auto_out_r_ready; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_out_aw_ready = axi4yank_auto_in_aw_ready; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_out_w_ready = axi4yank_auto_in_w_ready; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_out_b_valid = axi4yank_auto_in_b_valid; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_out_b_bits_id = axi4yank_auto_in_b_bits_id; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_out_b_bits_resp = axi4yank_auto_in_b_bits_resp; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_out_b_bits_echo_extra_id = axi4yank_auto_in_b_bits_echo_extra_id; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_out_b_bits_echo_real_last = axi4yank_auto_in_b_bits_echo_real_last; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_out_ar_ready = axi4yank_auto_in_ar_ready; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_out_r_valid = axi4yank_auto_in_r_valid; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_out_r_bits_id = axi4yank_auto_in_r_bits_id; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_out_r_bits_data = axi4yank_auto_in_r_bits_data; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_out_r_bits_resp = axi4yank_auto_in_r_bits_resp; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_out_r_bits_echo_extra_id = axi4yank_auto_in_r_bits_echo_extra_id; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_out_r_bits_echo_real_last = axi4yank_auto_in_r_bits_echo_real_last; // @[LazyModule.scala 375:16]
  assign axi4frag_auto_out_r_bits_last = axi4yank_auto_in_r_bits_last; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_valid = axi4buf_auto_out_aw_valid; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_bits_id = axi4buf_auto_out_aw_bits_id; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_bits_addr = axi4buf_auto_out_aw_bits_addr; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_bits_len = axi4buf_auto_out_aw_bits_len; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_bits_size = axi4buf_auto_out_aw_bits_size; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_bits_burst = axi4buf_auto_out_aw_bits_burst; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_bits_cache = axi4buf_auto_out_aw_bits_cache; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_aw_bits_prot = axi4buf_auto_out_aw_bits_prot; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_w_valid = axi4buf_auto_out_w_valid; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_w_bits_data = axi4buf_auto_out_w_bits_data; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_w_bits_strb = axi4buf_auto_out_w_bits_strb; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_w_bits_last = axi4buf_auto_out_w_bits_last; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_b_ready = axi4buf_auto_out_b_ready; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_valid = axi4buf_auto_out_ar_valid; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_bits_id = axi4buf_auto_out_ar_bits_id; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_bits_addr = axi4buf_auto_out_ar_bits_addr; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_bits_len = axi4buf_auto_out_ar_bits_len; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_bits_size = axi4buf_auto_out_ar_bits_size; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_bits_burst = axi4buf_auto_out_ar_bits_burst; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_bits_cache = axi4buf_auto_out_ar_bits_cache; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_ar_bits_prot = axi4buf_auto_out_ar_bits_prot; // @[LazyModule.scala 375:16]
  assign axi4index_auto_in_r_ready = axi4buf_auto_out_r_ready; // @[LazyModule.scala 375:16]
  assign axi4index_auto_out_aw_ready = axi4frag_auto_in_aw_ready; // @[LazyModule.scala 375:16]
  assign axi4index_auto_out_w_ready = axi4frag_auto_in_w_ready; // @[LazyModule.scala 375:16]
  assign axi4index_auto_out_b_valid = axi4frag_auto_in_b_valid; // @[LazyModule.scala 375:16]
  assign axi4index_auto_out_b_bits_id = axi4frag_auto_in_b_bits_id; // @[LazyModule.scala 375:16]
  assign axi4index_auto_out_b_bits_resp = axi4frag_auto_in_b_bits_resp; // @[LazyModule.scala 375:16]
  assign axi4index_auto_out_b_bits_echo_extra_id = axi4frag_auto_in_b_bits_echo_extra_id; // @[LazyModule.scala 375:16]
  assign axi4index_auto_out_ar_ready = axi4frag_auto_in_ar_ready; // @[LazyModule.scala 375:16]
  assign axi4index_auto_out_r_valid = axi4frag_auto_in_r_valid; // @[LazyModule.scala 375:16]
  assign axi4index_auto_out_r_bits_id = axi4frag_auto_in_r_bits_id; // @[LazyModule.scala 375:16]
  assign axi4index_auto_out_r_bits_data = axi4frag_auto_in_r_bits_data; // @[LazyModule.scala 375:16]
  assign axi4index_auto_out_r_bits_resp = axi4frag_auto_in_r_bits_resp; // @[LazyModule.scala 375:16]
  assign axi4index_auto_out_r_bits_echo_extra_id = axi4frag_auto_in_r_bits_echo_extra_id; // @[LazyModule.scala 375:16]
  assign axi4index_auto_out_r_bits_last = axi4frag_auto_in_r_bits_last; // @[LazyModule.scala 375:16]
  assign axi4buf_clock = clock;
  assign axi4buf_reset = reset;
  assign axi4buf_auto_in_aw_valid = auto_axi4_in_aw_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_aw_bits_id = auto_axi4_in_aw_bits_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_aw_bits_addr = auto_axi4_in_aw_bits_addr; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_aw_bits_len = auto_axi4_in_aw_bits_len; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_aw_bits_size = auto_axi4_in_aw_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_aw_bits_burst = auto_axi4_in_aw_bits_burst; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_aw_bits_cache = auto_axi4_in_aw_bits_cache; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_aw_bits_prot = auto_axi4_in_aw_bits_prot; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_w_valid = auto_axi4_in_w_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_w_bits_data = auto_axi4_in_w_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_w_bits_strb = auto_axi4_in_w_bits_strb; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_w_bits_last = auto_axi4_in_w_bits_last; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_b_ready = auto_axi4_in_b_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_ar_valid = auto_axi4_in_ar_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_ar_bits_id = auto_axi4_in_ar_bits_id; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_ar_bits_addr = auto_axi4_in_ar_bits_addr; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_ar_bits_len = auto_axi4_in_ar_bits_len; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_ar_bits_size = auto_axi4_in_ar_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_ar_bits_burst = auto_axi4_in_ar_bits_burst; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_ar_bits_cache = auto_axi4_in_ar_bits_cache; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_ar_bits_prot = auto_axi4_in_ar_bits_prot; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_in_r_ready = auto_axi4_in_r_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign axi4buf_auto_out_aw_ready = axi4index_auto_in_aw_ready; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_out_w_ready = axi4index_auto_in_w_ready; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_out_b_valid = axi4index_auto_in_b_valid; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_out_b_bits_id = axi4index_auto_in_b_bits_id; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_out_b_bits_resp = axi4index_auto_in_b_bits_resp; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_out_ar_ready = axi4index_auto_in_ar_ready; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_out_r_valid = axi4index_auto_in_r_valid; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_out_r_bits_id = axi4index_auto_in_r_bits_id; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_out_r_bits_data = axi4index_auto_in_r_bits_data; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_out_r_bits_resp = axi4index_auto_in_r_bits_resp; // @[LazyModule.scala 375:16]
  assign axi4buf_auto_out_r_bits_last = axi4index_auto_in_r_bits_last; // @[LazyModule.scala 375:16]
endmodule
