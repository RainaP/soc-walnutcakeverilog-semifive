//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_AXI4Fragmenter_assert(
  input        clock,
  input        reset,
  input  [8:0] _T_288,
  input        _T_302,
  input        _T_290,
  input        Queue_2_io_deq_bits_last
);
  wire  _T_289; // @[Fragmenter.scala 168:27]
  wire  _T_294; // @[Fragmenter.scala 170:15]
  wire  _T_295; // @[Fragmenter.scala 170:39]
  wire  _T_296; // @[Fragmenter.scala 170:29]
  wire  _T_298; // @[Fragmenter.scala 170:14]
  wire  _T_299; // @[Fragmenter.scala 170:14]
  wire  _T_306; // @[Fragmenter.scala 179:15]
  wire  _T_273_bits_last; // @[Decoupled.scala 308:19 Decoupled.scala 309:14]
  wire  _T_307; // @[Fragmenter.scala 179:31]
  wire  _T_308; // @[Fragmenter.scala 179:28]
  wire  _T_309; // @[Fragmenter.scala 179:47]
  wire  _T_311; // @[Fragmenter.scala 179:14]
  wire  _T_312; // @[Fragmenter.scala 179:14]
  assign _T_289 = _T_288 == 9'h1; // @[Fragmenter.scala 168:27]
  assign _T_294 = ~_T_290; // @[Fragmenter.scala 170:15]
  assign _T_295 = _T_288 != 9'h0; // @[Fragmenter.scala 170:39]
  assign _T_296 = _T_294 | _T_295; // @[Fragmenter.scala 170:29]
  assign _T_298 = _T_296 | reset; // @[Fragmenter.scala 170:14]
  assign _T_299 = ~_T_298; // @[Fragmenter.scala 170:14]
  assign _T_306 = ~_T_302; // @[Fragmenter.scala 179:15]
  assign _T_273_bits_last = Queue_2_io_deq_bits_last; // @[Decoupled.scala 308:19 Decoupled.scala 309:14]
  assign _T_307 = ~_T_273_bits_last; // @[Fragmenter.scala 179:31]
  assign _T_308 = _T_306 | _T_307; // @[Fragmenter.scala 179:28]
  assign _T_309 = _T_308 | _T_289; // @[Fragmenter.scala 179:47]
  assign _T_311 = _T_309 | reset; // @[Fragmenter.scala 179:14]
  assign _T_312 = ~_T_311; // @[Fragmenter.scala 179:14]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_299) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Fragmenter.scala:170 assert (!out.w.fire() || w_todo =/= UInt(0)) // underflow impossible\n"); // @[Fragmenter.scala 170:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_299) begin
          $fatal; // @[Fragmenter.scala 170:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_312) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Fragmenter.scala:179 assert (!out.w.valid || !in_w.bits.last || w_last)\n"); // @[Fragmenter.scala 179:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_312) begin
          $fatal; // @[Fragmenter.scala 179:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
