//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_SourceC_assert(
  input        clock,
  input        reset,
  input        room,
  input        s3_valid,
  input        queue_io_enq_ready,
  input  [3:0] queue_io_count
);
  wire  _T_16; // @[SourceC.scala 70:35]
  wire  _T_17; // @[SourceC.scala 70:16]
  wire  _T_19; // @[SourceC.scala 70:10]
  wire  _T_20; // @[SourceC.scala 70:10]
  wire  c_ready; // @[SourceC.scala 111:15 SourceC.scala 133:16]
  wire  _T_345; // @[SourceC.scala 130:10]
  wire  _T_346; // @[SourceC.scala 130:19]
  wire  _T_348; // @[SourceC.scala 130:9]
  wire  _T_349; // @[SourceC.scala 130:9]
  assign _T_16 = queue_io_count <= 4'h1; // @[SourceC.scala 70:35]
  assign _T_17 = room == _T_16; // @[SourceC.scala 70:16]
  assign _T_19 = _T_17 | reset; // @[SourceC.scala 70:10]
  assign _T_20 = ~_T_19; // @[SourceC.scala 70:10]
  assign c_ready = queue_io_enq_ready; // @[SourceC.scala 111:15 SourceC.scala 133:16]
  assign _T_345 = ~s3_valid; // @[SourceC.scala 130:10]
  assign _T_346 = _T_345 | c_ready; // @[SourceC.scala 130:19]
  assign _T_348 = _T_346 | reset; // @[SourceC.scala 130:9]
  assign _T_349 = ~_T_348; // @[SourceC.scala 130:9]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_20) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at SourceC.scala:70 assert (room === queue.io.count <= UInt(1))\n"); // @[SourceC.scala 70:10]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_20) begin
          $fatal; // @[SourceC.scala 70:10]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_349) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at SourceC.scala:130 assert(!c.valid || c.ready)\n"); // @[SourceC.scala 130:9]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_349) begin
          $fatal; // @[SourceC.scala 130:9]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
