//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__PTW(
  input         rf_reset,
  input         clock,
  input         reset,
  output        io_requestor_0_req_ready,
  input         io_requestor_0_req_valid,
  input  [26:0] io_requestor_0_req_bits_bits_addr,
  output        io_requestor_0_resp_valid,
  output        io_requestor_0_resp_bits_ae,
  output [53:0] io_requestor_0_resp_bits_pte_ppn,
  output        io_requestor_0_resp_bits_pte_d,
  output        io_requestor_0_resp_bits_pte_a,
  output        io_requestor_0_resp_bits_pte_g,
  output        io_requestor_0_resp_bits_pte_u,
  output        io_requestor_0_resp_bits_pte_x,
  output        io_requestor_0_resp_bits_pte_w,
  output        io_requestor_0_resp_bits_pte_r,
  output        io_requestor_0_resp_bits_pte_v,
  output [1:0]  io_requestor_0_resp_bits_level,
  output        io_requestor_0_resp_bits_homogeneous,
  output [3:0]  io_requestor_0_ptbr_mode,
  output        io_requestor_0_status_debug,
  output [1:0]  io_requestor_0_status_dprv,
  output        io_requestor_0_status_mxr,
  output        io_requestor_0_status_sum,
  output        io_requestor_0_pmp_0_cfg_l,
  output [1:0]  io_requestor_0_pmp_0_cfg_a,
  output        io_requestor_0_pmp_0_cfg_x,
  output        io_requestor_0_pmp_0_cfg_w,
  output        io_requestor_0_pmp_0_cfg_r,
  output [34:0] io_requestor_0_pmp_0_addr,
  output [36:0] io_requestor_0_pmp_0_mask,
  output        io_requestor_0_pmp_1_cfg_l,
  output [1:0]  io_requestor_0_pmp_1_cfg_a,
  output        io_requestor_0_pmp_1_cfg_x,
  output        io_requestor_0_pmp_1_cfg_w,
  output        io_requestor_0_pmp_1_cfg_r,
  output [34:0] io_requestor_0_pmp_1_addr,
  output [36:0] io_requestor_0_pmp_1_mask,
  output        io_requestor_0_pmp_2_cfg_l,
  output [1:0]  io_requestor_0_pmp_2_cfg_a,
  output        io_requestor_0_pmp_2_cfg_x,
  output        io_requestor_0_pmp_2_cfg_w,
  output        io_requestor_0_pmp_2_cfg_r,
  output [34:0] io_requestor_0_pmp_2_addr,
  output [36:0] io_requestor_0_pmp_2_mask,
  output        io_requestor_0_pmp_3_cfg_l,
  output [1:0]  io_requestor_0_pmp_3_cfg_a,
  output        io_requestor_0_pmp_3_cfg_x,
  output        io_requestor_0_pmp_3_cfg_w,
  output        io_requestor_0_pmp_3_cfg_r,
  output [34:0] io_requestor_0_pmp_3_addr,
  output [36:0] io_requestor_0_pmp_3_mask,
  output        io_requestor_0_pmp_4_cfg_l,
  output [1:0]  io_requestor_0_pmp_4_cfg_a,
  output        io_requestor_0_pmp_4_cfg_x,
  output        io_requestor_0_pmp_4_cfg_w,
  output        io_requestor_0_pmp_4_cfg_r,
  output [34:0] io_requestor_0_pmp_4_addr,
  output [36:0] io_requestor_0_pmp_4_mask,
  output        io_requestor_0_pmp_5_cfg_l,
  output [1:0]  io_requestor_0_pmp_5_cfg_a,
  output        io_requestor_0_pmp_5_cfg_x,
  output        io_requestor_0_pmp_5_cfg_w,
  output        io_requestor_0_pmp_5_cfg_r,
  output [34:0] io_requestor_0_pmp_5_addr,
  output [36:0] io_requestor_0_pmp_5_mask,
  output        io_requestor_0_pmp_6_cfg_l,
  output [1:0]  io_requestor_0_pmp_6_cfg_a,
  output        io_requestor_0_pmp_6_cfg_x,
  output        io_requestor_0_pmp_6_cfg_w,
  output        io_requestor_0_pmp_6_cfg_r,
  output [34:0] io_requestor_0_pmp_6_addr,
  output [36:0] io_requestor_0_pmp_6_mask,
  output        io_requestor_0_pmp_7_cfg_l,
  output [1:0]  io_requestor_0_pmp_7_cfg_a,
  output        io_requestor_0_pmp_7_cfg_x,
  output        io_requestor_0_pmp_7_cfg_w,
  output        io_requestor_0_pmp_7_cfg_r,
  output [34:0] io_requestor_0_pmp_7_addr,
  output [36:0] io_requestor_0_pmp_7_mask,
  output [63:0] io_requestor_0_customCSRs_csrs_1_value,
  output        io_requestor_1_req_ready,
  input         io_requestor_1_req_valid,
  input         io_requestor_1_req_bits_valid,
  input  [26:0] io_requestor_1_req_bits_bits_addr,
  output        io_requestor_1_resp_valid,
  output        io_requestor_1_resp_bits_ae,
  output [53:0] io_requestor_1_resp_bits_pte_ppn,
  output        io_requestor_1_resp_bits_pte_d,
  output        io_requestor_1_resp_bits_pte_a,
  output        io_requestor_1_resp_bits_pte_g,
  output        io_requestor_1_resp_bits_pte_u,
  output        io_requestor_1_resp_bits_pte_x,
  output        io_requestor_1_resp_bits_pte_w,
  output        io_requestor_1_resp_bits_pte_r,
  output        io_requestor_1_resp_bits_pte_v,
  output [1:0]  io_requestor_1_resp_bits_level,
  output        io_requestor_1_resp_bits_homogeneous,
  output [3:0]  io_requestor_1_ptbr_mode,
  output        io_requestor_1_status_debug,
  output [1:0]  io_requestor_1_status_prv,
  output        io_requestor_1_pmp_0_cfg_l,
  output [1:0]  io_requestor_1_pmp_0_cfg_a,
  output        io_requestor_1_pmp_0_cfg_x,
  output        io_requestor_1_pmp_0_cfg_w,
  output        io_requestor_1_pmp_0_cfg_r,
  output [34:0] io_requestor_1_pmp_0_addr,
  output [36:0] io_requestor_1_pmp_0_mask,
  output        io_requestor_1_pmp_1_cfg_l,
  output [1:0]  io_requestor_1_pmp_1_cfg_a,
  output        io_requestor_1_pmp_1_cfg_x,
  output        io_requestor_1_pmp_1_cfg_w,
  output        io_requestor_1_pmp_1_cfg_r,
  output [34:0] io_requestor_1_pmp_1_addr,
  output [36:0] io_requestor_1_pmp_1_mask,
  output        io_requestor_1_pmp_2_cfg_l,
  output [1:0]  io_requestor_1_pmp_2_cfg_a,
  output        io_requestor_1_pmp_2_cfg_x,
  output        io_requestor_1_pmp_2_cfg_w,
  output        io_requestor_1_pmp_2_cfg_r,
  output [34:0] io_requestor_1_pmp_2_addr,
  output [36:0] io_requestor_1_pmp_2_mask,
  output        io_requestor_1_pmp_3_cfg_l,
  output [1:0]  io_requestor_1_pmp_3_cfg_a,
  output        io_requestor_1_pmp_3_cfg_x,
  output        io_requestor_1_pmp_3_cfg_w,
  output        io_requestor_1_pmp_3_cfg_r,
  output [34:0] io_requestor_1_pmp_3_addr,
  output [36:0] io_requestor_1_pmp_3_mask,
  output        io_requestor_1_pmp_4_cfg_l,
  output [1:0]  io_requestor_1_pmp_4_cfg_a,
  output        io_requestor_1_pmp_4_cfg_x,
  output        io_requestor_1_pmp_4_cfg_w,
  output        io_requestor_1_pmp_4_cfg_r,
  output [34:0] io_requestor_1_pmp_4_addr,
  output [36:0] io_requestor_1_pmp_4_mask,
  output        io_requestor_1_pmp_5_cfg_l,
  output [1:0]  io_requestor_1_pmp_5_cfg_a,
  output        io_requestor_1_pmp_5_cfg_x,
  output        io_requestor_1_pmp_5_cfg_w,
  output        io_requestor_1_pmp_5_cfg_r,
  output [34:0] io_requestor_1_pmp_5_addr,
  output [36:0] io_requestor_1_pmp_5_mask,
  output        io_requestor_1_pmp_6_cfg_l,
  output [1:0]  io_requestor_1_pmp_6_cfg_a,
  output        io_requestor_1_pmp_6_cfg_x,
  output        io_requestor_1_pmp_6_cfg_w,
  output        io_requestor_1_pmp_6_cfg_r,
  output [34:0] io_requestor_1_pmp_6_addr,
  output [36:0] io_requestor_1_pmp_6_mask,
  output        io_requestor_1_pmp_7_cfg_l,
  output [1:0]  io_requestor_1_pmp_7_cfg_a,
  output        io_requestor_1_pmp_7_cfg_x,
  output        io_requestor_1_pmp_7_cfg_w,
  output        io_requestor_1_pmp_7_cfg_r,
  output [34:0] io_requestor_1_pmp_7_addr,
  output [36:0] io_requestor_1_pmp_7_mask,
  output        io_requestor_1_customCSRs_csrs_0_wen,
  output [63:0] io_requestor_1_customCSRs_csrs_0_value,
  output [63:0] io_requestor_1_customCSRs_csrs_1_value,
  input         io_mem_req_ready,
  output        io_mem_req_valid,
  output [39:0] io_mem_req_bits_addr,
  output [39:0] io_mem_req_bits_idx,
  output        io_mem_s1_kill,
  input         io_mem_s2_nack,
  input         io_mem_resp_valid,
  input  [63:0] io_mem_resp_bits_data,
  input         io_mem_s2_xcpt_ae_ld,
  input  [3:0]  io_dpath_ptbr_mode,
  input  [43:0] io_dpath_ptbr_ppn,
  input         io_dpath_sfence_valid,
  input         io_dpath_sfence_bits_rs1,
  input         io_dpath_sfence_bits_rs2,
  input  [38:0] io_dpath_sfence_bits_addr,
  input         io_dpath_status_debug,
  input  [1:0]  io_dpath_status_dprv,
  input  [1:0]  io_dpath_status_prv,
  input         io_dpath_status_mxr,
  input         io_dpath_status_sum,
  input         io_dpath_pmp_0_cfg_l,
  input  [1:0]  io_dpath_pmp_0_cfg_a,
  input         io_dpath_pmp_0_cfg_x,
  input         io_dpath_pmp_0_cfg_w,
  input         io_dpath_pmp_0_cfg_r,
  input  [34:0] io_dpath_pmp_0_addr,
  input  [36:0] io_dpath_pmp_0_mask,
  input         io_dpath_pmp_1_cfg_l,
  input  [1:0]  io_dpath_pmp_1_cfg_a,
  input         io_dpath_pmp_1_cfg_x,
  input         io_dpath_pmp_1_cfg_w,
  input         io_dpath_pmp_1_cfg_r,
  input  [34:0] io_dpath_pmp_1_addr,
  input  [36:0] io_dpath_pmp_1_mask,
  input         io_dpath_pmp_2_cfg_l,
  input  [1:0]  io_dpath_pmp_2_cfg_a,
  input         io_dpath_pmp_2_cfg_x,
  input         io_dpath_pmp_2_cfg_w,
  input         io_dpath_pmp_2_cfg_r,
  input  [34:0] io_dpath_pmp_2_addr,
  input  [36:0] io_dpath_pmp_2_mask,
  input         io_dpath_pmp_3_cfg_l,
  input  [1:0]  io_dpath_pmp_3_cfg_a,
  input         io_dpath_pmp_3_cfg_x,
  input         io_dpath_pmp_3_cfg_w,
  input         io_dpath_pmp_3_cfg_r,
  input  [34:0] io_dpath_pmp_3_addr,
  input  [36:0] io_dpath_pmp_3_mask,
  input         io_dpath_pmp_4_cfg_l,
  input  [1:0]  io_dpath_pmp_4_cfg_a,
  input         io_dpath_pmp_4_cfg_x,
  input         io_dpath_pmp_4_cfg_w,
  input         io_dpath_pmp_4_cfg_r,
  input  [34:0] io_dpath_pmp_4_addr,
  input  [36:0] io_dpath_pmp_4_mask,
  input         io_dpath_pmp_5_cfg_l,
  input  [1:0]  io_dpath_pmp_5_cfg_a,
  input         io_dpath_pmp_5_cfg_x,
  input         io_dpath_pmp_5_cfg_w,
  input         io_dpath_pmp_5_cfg_r,
  input  [34:0] io_dpath_pmp_5_addr,
  input  [36:0] io_dpath_pmp_5_mask,
  input         io_dpath_pmp_6_cfg_l,
  input  [1:0]  io_dpath_pmp_6_cfg_a,
  input         io_dpath_pmp_6_cfg_x,
  input         io_dpath_pmp_6_cfg_w,
  input         io_dpath_pmp_6_cfg_r,
  input  [34:0] io_dpath_pmp_6_addr,
  input  [36:0] io_dpath_pmp_6_mask,
  input         io_dpath_pmp_7_cfg_l,
  input  [1:0]  io_dpath_pmp_7_cfg_a,
  input         io_dpath_pmp_7_cfg_x,
  input         io_dpath_pmp_7_cfg_w,
  input         io_dpath_pmp_7_cfg_r,
  input  [34:0] io_dpath_pmp_7_addr,
  input  [36:0] io_dpath_pmp_7_mask,
  output        io_dpath_perf_l2miss,
  output        io_dpath_perf_l2hit,
  output        io_dpath_perf_pte_miss,
  output        io_dpath_perf_pte_hit,
  input         io_dpath_customCSRs_csrs_0_wen,
  input  [63:0] io_dpath_customCSRs_csrs_0_value,
  input  [63:0] io_dpath_customCSRs_csrs_1_value,
  output        io_dpath_clock_enabled,
  input         psd_test_clock_enable
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [63:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [63:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [63:0] _RAND_22;
  reg [63:0] _RAND_23;
  reg [63:0] _RAND_24;
  reg [63:0] _RAND_25;
  reg [63:0] _RAND_26;
  reg [63:0] _RAND_27;
  reg [63:0] _RAND_28;
  reg [63:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [127:0] _RAND_39;
  reg [127:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [63:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
`endif // RANDOMIZE_REG_INIT
  wire  arb_io_in_0_ready; // @[PTW.scala 112:19]
  wire  arb_io_in_0_valid; // @[PTW.scala 112:19]
  wire [26:0] arb_io_in_0_bits_bits_addr; // @[PTW.scala 112:19]
  wire  arb_io_in_1_ready; // @[PTW.scala 112:19]
  wire  arb_io_in_1_valid; // @[PTW.scala 112:19]
  wire  arb_io_in_1_bits_valid; // @[PTW.scala 112:19]
  wire [26:0] arb_io_in_1_bits_bits_addr; // @[PTW.scala 112:19]
  wire  arb_io_out_ready; // @[PTW.scala 112:19]
  wire  arb_io_out_valid; // @[PTW.scala 112:19]
  wire  arb_io_out_bits_valid; // @[PTW.scala 112:19]
  wire [26:0] arb_io_out_bits_bits_addr; // @[PTW.scala 112:19]
  wire  arb_io_chosen; // @[PTW.scala 112:19]
  wire  gated_clock_ptw_clock_gate_in; // @[ClockGate.scala 24:20]
  wire  gated_clock_ptw_clock_gate_test_en; // @[ClockGate.scala 24:20]
  wire  gated_clock_ptw_clock_gate_en; // @[ClockGate.scala 24:20]
  wire  gated_clock_ptw_clock_gate_out; // @[ClockGate.scala 24:20]
  wire [6:0] l2_tlb_ram_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  l2_tlb_ram_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  l2_tlb_ram_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  l2_tlb_ram_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [51:0] l2_tlb_ram_RW0_wdata_0; // @[DescribedSRAM.scala 18:26]
  wire [51:0] l2_tlb_ram_RW0_rdata_0; // @[DescribedSRAM.scala 18:26]
  wire [2:0] state_barrier_io_x; // @[package.scala 271:25]
  wire [2:0] state_barrier_io_y; // @[package.scala 271:25]
  wire [53:0] r_pte_barrier_io_x_ppn; // @[package.scala 271:25]
  wire  r_pte_barrier_io_x_d; // @[package.scala 271:25]
  wire  r_pte_barrier_io_x_a; // @[package.scala 271:25]
  wire  r_pte_barrier_io_x_g; // @[package.scala 271:25]
  wire  r_pte_barrier_io_x_u; // @[package.scala 271:25]
  wire  r_pte_barrier_io_x_x; // @[package.scala 271:25]
  wire  r_pte_barrier_io_x_w; // @[package.scala 271:25]
  wire  r_pte_barrier_io_x_r; // @[package.scala 271:25]
  wire  r_pte_barrier_io_x_v; // @[package.scala 271:25]
  wire [53:0] r_pte_barrier_io_y_ppn; // @[package.scala 271:25]
  wire  r_pte_barrier_io_y_d; // @[package.scala 271:25]
  wire  r_pte_barrier_io_y_a; // @[package.scala 271:25]
  wire  r_pte_barrier_io_y_g; // @[package.scala 271:25]
  wire  r_pte_barrier_io_y_u; // @[package.scala 271:25]
  wire  r_pte_barrier_io_y_x; // @[package.scala 271:25]
  wire  r_pte_barrier_io_y_w; // @[package.scala 271:25]
  wire  r_pte_barrier_io_y_r; // @[package.scala 271:25]
  wire  r_pte_barrier_io_y_v; // @[package.scala 271:25]
  reg [2:0] state; // @[PTW.scala 109:18]
  reg  l2_refill; // @[PTW.scala 203:26]
  wire  _arb_io_out_ready_T_1 = ~l2_refill; // @[PTW.scala 114:46]
  reg  resp_valid_0; // @[PTW.scala 116:23]
  reg  resp_valid_1; // @[PTW.scala 116:23]
  wire  _clock_en_T = state != 3'h0; // @[PTW.scala 118:24]
  reg  invalidated; // @[PTW.scala 125:24]
  reg [1:0] count; // @[PTW.scala 126:18]
  reg  resp_ae; // @[PTW.scala 127:24]
  reg [26:0] r_req_addr; // @[PTW.scala 130:18]
  reg  r_req_dest; // @[PTW.scala 131:23]
  reg [53:0] r_pte_ppn; // @[PTW.scala 132:18]
  reg  r_pte_d; // @[PTW.scala 132:18]
  reg  r_pte_a; // @[PTW.scala 132:18]
  reg  r_pte_g; // @[PTW.scala 132:18]
  reg  r_pte_u; // @[PTW.scala 132:18]
  reg  r_pte_x; // @[PTW.scala 132:18]
  reg  r_pte_w; // @[PTW.scala 132:18]
  reg  r_pte_r; // @[PTW.scala 132:18]
  reg  r_pte_v; // @[PTW.scala 132:18]
  reg  mem_resp_valid; // @[PTW.scala 134:31]
  reg [63:0] mem_resp_data; // @[PTW.scala 135:30]
  wire  tmp_v = mem_resp_data[0]; // @[PTW.scala 146:33]
  wire  tmp_r = mem_resp_data[1]; // @[PTW.scala 146:33]
  wire  tmp_w = mem_resp_data[2]; // @[PTW.scala 146:33]
  wire  tmp_x = mem_resp_data[3]; // @[PTW.scala 146:33]
  wire  tmp_u = mem_resp_data[4]; // @[PTW.scala 146:33]
  wire  tmp_g = mem_resp_data[5]; // @[PTW.scala 146:33]
  wire  tmp_a = mem_resp_data[6]; // @[PTW.scala 146:33]
  wire  tmp_d = mem_resp_data[7]; // @[PTW.scala 146:33]
  wire [53:0] tmp_ppn = mem_resp_data[63:10]; // @[PTW.scala 146:33]
  wire  _GEN_0 = count <= 2'h0 & tmp_ppn[17:9] != 9'h0 ? 1'h0 : tmp_v; // @[PTW.scala 152:102 PTW.scala 152:110]
  wire  _GEN_1 = count <= 2'h1 & tmp_ppn[8:0] != 9'h0 ? 1'h0 : _GEN_0; // @[PTW.scala 152:102 PTW.scala 152:110]
  wire  res_v = tmp_r | tmp_w | tmp_x ? _GEN_1 : tmp_v; // @[PTW.scala 149:36]
  wire  invalid_paddr = tmp_ppn[53:25] != 29'h0; // @[PTW.scala 154:32]
  wire  _traverse_T_5 = res_v & ~tmp_r & ~tmp_w & ~tmp_x; // @[PTW.scala 72:45]
  wire  _traverse_T_6 = ~invalid_paddr; // @[PTW.scala 156:33]
  wire  _traverse_T_8 = count < 2'h2; // @[PTW.scala 156:57]
  wire  traverse = _traverse_T_5 & _traverse_T_6 & count < 2'h2; // @[PTW.scala 156:48]
  wire [8:0] vpn_idxs_0 = r_req_addr[26:18]; // @[PTW.scala 158:60]
  wire [8:0] vpn_idxs_1 = r_req_addr[17:9]; // @[PTW.scala 158:90]
  wire [8:0] vpn_idxs_2 = r_req_addr[8:0]; // @[PTW.scala 158:90]
  wire  _pte_addr_vpn_idx_T = count == 2'h1; // @[package.scala 32:86]
  wire [8:0] _pte_addr_vpn_idx_T_1 = _pte_addr_vpn_idx_T ? vpn_idxs_1 : vpn_idxs_0; // @[package.scala 32:76]
  wire  _pte_addr_vpn_idx_T_2 = count == 2'h2; // @[package.scala 32:86]
  wire [8:0] _pte_addr_vpn_idx_T_3 = _pte_addr_vpn_idx_T_2 ? vpn_idxs_2 : _pte_addr_vpn_idx_T_1; // @[package.scala 32:76]
  wire [8:0] vpn_idx = count == 2'h3 ? vpn_idxs_2 : _pte_addr_vpn_idx_T_3; // @[package.scala 32:76]
  wire [62:0] _pte_addr_T = {r_pte_ppn,vpn_idx}; // @[Cat.scala 30:58]
  wire [65:0] pte_addr = {_pte_addr_T, 3'h0}; // @[PTW.scala 160:29]
  wire [35:0] fragmented_superpage_ppn_choices_hi = r_pte_ppn[53:18]; // @[PTW.scala 163:69]
  wire [17:0] fragmented_superpage_ppn_choices_lo = r_req_addr[17:0]; // @[PTW.scala 163:99]
  wire [53:0] choices_0 = {fragmented_superpage_ppn_choices_hi,fragmented_superpage_ppn_choices_lo}; // @[Cat.scala 30:58]
  wire [44:0] fragmented_superpage_ppn_choices_hi_1 = r_pte_ppn[53:9]; // @[PTW.scala 163:69]
  wire [53:0] choices_1 = {fragmented_superpage_ppn_choices_hi_1,vpn_idxs_2}; // @[Cat.scala 30:58]
  wire  fragmented_superpage_ppn_truncIdx = count[0]; // @[package.scala 31:49]
  wire [53:0] fragmented_superpage_ppn = fragmented_superpage_ppn_truncIdx ? choices_1 : choices_0; // @[package.scala 32:76]
  wire  _T_22 = arb_io_out_ready & arb_io_out_valid; // @[Decoupled.scala 40:37]
  reg [6:0] state_reg; // @[Replacement.scala 168:70]
  reg [7:0] valid; // @[PTW.scala 175:24]
  reg [36:0] tags_0; // @[PTW.scala 176:19]
  reg [36:0] tags_1; // @[PTW.scala 176:19]
  reg [36:0] tags_2; // @[PTW.scala 176:19]
  reg [36:0] tags_3; // @[PTW.scala 176:19]
  reg [36:0] tags_4; // @[PTW.scala 176:19]
  reg [36:0] tags_5; // @[PTW.scala 176:19]
  reg [36:0] tags_6; // @[PTW.scala 176:19]
  reg [36:0] tags_7; // @[PTW.scala 176:19]
  reg [24:0] data_0; // @[PTW.scala 177:19]
  reg [24:0] data_1; // @[PTW.scala 177:19]
  reg [24:0] data_2; // @[PTW.scala 177:19]
  reg [24:0] data_3; // @[PTW.scala 177:19]
  reg [24:0] data_4; // @[PTW.scala 177:19]
  reg [24:0] data_5; // @[PTW.scala 177:19]
  reg [24:0] data_6; // @[PTW.scala 177:19]
  reg [24:0] data_7; // @[PTW.scala 177:19]
  wire [65:0] _GEN_41 = {{29'd0}, tags_0}; // @[PTW.scala 179:27]
  wire  lo_lo_lo = _GEN_41 == pte_addr; // @[PTW.scala 179:27]
  wire [65:0] _GEN_45 = {{29'd0}, tags_1}; // @[PTW.scala 179:27]
  wire  lo_lo_hi = _GEN_45 == pte_addr; // @[PTW.scala 179:27]
  wire [65:0] _GEN_47 = {{29'd0}, tags_2}; // @[PTW.scala 179:27]
  wire  lo_hi_lo = _GEN_47 == pte_addr; // @[PTW.scala 179:27]
  wire [65:0] _GEN_55 = {{29'd0}, tags_3}; // @[PTW.scala 179:27]
  wire  lo_hi_hi = _GEN_55 == pte_addr; // @[PTW.scala 179:27]
  wire [65:0] _GEN_60 = {{29'd0}, tags_4}; // @[PTW.scala 179:27]
  wire  hi_lo_lo = _GEN_60 == pte_addr; // @[PTW.scala 179:27]
  wire [65:0] _GEN_61 = {{29'd0}, tags_5}; // @[PTW.scala 179:27]
  wire  hi_lo_hi = _GEN_61 == pte_addr; // @[PTW.scala 179:27]
  wire [65:0] _GEN_62 = {{29'd0}, tags_6}; // @[PTW.scala 179:27]
  wire  hi_hi_lo = _GEN_62 == pte_addr; // @[PTW.scala 179:27]
  wire [65:0] _GEN_63 = {{29'd0}, tags_7}; // @[PTW.scala 179:27]
  wire  hi_hi_hi = _GEN_63 == pte_addr; // @[PTW.scala 179:27]
  wire [7:0] _T_23 = {hi_hi_hi,hi_hi_lo,hi_lo_hi,hi_lo_lo,lo_hi_hi,lo_hi_lo,lo_lo_hi,lo_lo_lo}; // @[Cat.scala 30:58]
  wire [7:0] hits = _T_23 & valid; // @[PTW.scala 179:48]
  wire  hit = |hits; // @[PTW.scala 180:20]
  wire  _T_27 = ~invalidated; // @[PTW.scala 181:49]
  wire  _T_29 = &valid; // @[PTW.scala 182:25]
  wire  hi_1 = state_reg[6]; // @[Replacement.scala 243:38]
  wire [2:0] left_subtree_state = state_reg[5:3]; // @[package.scala 154:13]
  wire [2:0] right_subtree_state = state_reg[2:0]; // @[Replacement.scala 245:38]
  wire  hi_2 = left_subtree_state[2]; // @[Replacement.scala 243:38]
  wire  left_subtree_state_1 = left_subtree_state[1]; // @[package.scala 154:13]
  wire  right_subtree_state_1 = left_subtree_state[0]; // @[Replacement.scala 245:38]
  wire  lo_1 = hi_2 ? left_subtree_state_1 : right_subtree_state_1; // @[Replacement.scala 250:16]
  wire [1:0] _T_32 = {hi_2,lo_1}; // @[Cat.scala 30:58]
  wire  hi_3 = right_subtree_state[2]; // @[Replacement.scala 243:38]
  wire  left_subtree_state_2 = right_subtree_state[1]; // @[package.scala 154:13]
  wire  right_subtree_state_2 = right_subtree_state[0]; // @[Replacement.scala 245:38]
  wire  lo_2 = hi_3 ? left_subtree_state_2 : right_subtree_state_2; // @[Replacement.scala 250:16]
  wire [1:0] _T_35 = {hi_3,lo_2}; // @[Cat.scala 30:58]
  wire [1:0] lo_3 = hi_1 ? _T_32 : _T_35; // @[Replacement.scala 250:16]
  wire [2:0] _T_36 = {hi_1,lo_3}; // @[Cat.scala 30:58]
  wire [7:0] _T_37 = ~valid; // @[PTW.scala 182:57]
  wire [2:0] _T_46 = _T_37[6] ? 3'h6 : 3'h7; // @[Mux.scala 47:69]
  wire [2:0] _T_47 = _T_37[5] ? 3'h5 : _T_46; // @[Mux.scala 47:69]
  wire [2:0] _T_48 = _T_37[4] ? 3'h4 : _T_47; // @[Mux.scala 47:69]
  wire [2:0] _T_49 = _T_37[3] ? 3'h3 : _T_48; // @[Mux.scala 47:69]
  wire [2:0] _T_50 = _T_37[2] ? 3'h2 : _T_49; // @[Mux.scala 47:69]
  wire [2:0] _T_51 = _T_37[1] ? 3'h1 : _T_50; // @[Mux.scala 47:69]
  wire [2:0] _T_52 = _T_37[0] ? 3'h0 : _T_51; // @[Mux.scala 47:69]
  wire [2:0] r = _T_29 ? _T_36 : _T_52; // @[PTW.scala 182:18]
  wire [7:0] _T_53 = 8'h1 << r; // @[OneHot.scala 58:35]
  wire [7:0] _T_54 = valid | _T_53; // @[PTW.scala 183:22]
  wire [53:0] res_ppn = {{29'd0}, tmp_ppn[24:0]};
  wire  _T_55 = state == 3'h1; // @[PTW.scala 187:24]
  wire  _T_56 = hit & _T_55; // @[PTW.scala 187:15]
  wire [3:0] hi_4 = hits[7:4]; // @[OneHot.scala 30:18]
  wire [3:0] lo_4 = hits[3:0]; // @[OneHot.scala 31:18]
  wire  hi_5 = |hi_4; // @[OneHot.scala 32:14]
  wire [3:0] _T_57 = hi_4 | lo_4; // @[OneHot.scala 32:28]
  wire [1:0] hi_6 = _T_57[3:2]; // @[OneHot.scala 30:18]
  wire [1:0] lo_5 = _T_57[1:0]; // @[OneHot.scala 31:18]
  wire  hi_7 = |hi_6; // @[OneHot.scala 32:14]
  wire [1:0] _T_58 = hi_6 | lo_5; // @[OneHot.scala 32:28]
  wire  lo_6 = _T_58[1]; // @[CircuitMath.scala 30:8]
  wire [2:0] state_reg_touch_way_sized = {hi_5,hi_7,lo_6}; // @[Cat.scala 30:58]
  wire  state_reg_hi_hi = ~state_reg_touch_way_sized[2]; // @[Replacement.scala 196:33]
  wire  state_reg_hi_hi_1 = ~state_reg_touch_way_sized[1]; // @[Replacement.scala 196:33]
  wire  _state_reg_T_3 = ~state_reg_touch_way_sized[0]; // @[Replacement.scala 218:7]
  wire  state_reg_hi_lo = state_reg_hi_hi_1 ? left_subtree_state_1 : _state_reg_T_3; // @[Replacement.scala 203:16]
  wire  state_reg_lo = state_reg_hi_hi_1 ? _state_reg_T_3 : right_subtree_state_1; // @[Replacement.scala 206:16]
  wire [2:0] _state_reg_T_7 = {state_reg_hi_hi_1,state_reg_hi_lo,state_reg_lo}; // @[Cat.scala 30:58]
  wire [2:0] state_reg_hi_lo_1 = state_reg_hi_hi ? left_subtree_state : _state_reg_T_7; // @[Replacement.scala 203:16]
  wire  state_reg_hi_lo_2 = state_reg_hi_hi_1 ? left_subtree_state_2 : _state_reg_T_3; // @[Replacement.scala 203:16]
  wire  state_reg_lo_1 = state_reg_hi_hi_1 ? _state_reg_T_3 : right_subtree_state_2; // @[Replacement.scala 206:16]
  wire [2:0] _state_reg_T_15 = {state_reg_hi_hi_1,state_reg_hi_lo_2,state_reg_lo_1}; // @[Cat.scala 30:58]
  wire [2:0] state_reg_lo_2 = state_reg_hi_hi ? _state_reg_T_15 : right_subtree_state; // @[Replacement.scala 206:16]
  wire [6:0] _state_reg_T_16 = {state_reg_hi_hi,state_reg_hi_lo_1,state_reg_lo_2}; // @[Cat.scala 30:58]
  wire  pte_cache_hit = hit & _traverse_T_8; // @[PTW.scala 195:10]
  wire [24:0] _T_88 = hits[0] ? data_0 : 25'h0; // @[Mux.scala 27:72]
  wire [24:0] _T_89 = hits[1] ? data_1 : 25'h0; // @[Mux.scala 27:72]
  wire [24:0] _T_90 = hits[2] ? data_2 : 25'h0; // @[Mux.scala 27:72]
  wire [24:0] _T_91 = hits[3] ? data_3 : 25'h0; // @[Mux.scala 27:72]
  wire [24:0] _T_92 = hits[4] ? data_4 : 25'h0; // @[Mux.scala 27:72]
  wire [24:0] _T_93 = hits[5] ? data_5 : 25'h0; // @[Mux.scala 27:72]
  wire [24:0] _T_94 = hits[6] ? data_6 : 25'h0; // @[Mux.scala 27:72]
  wire [24:0] _T_95 = hits[7] ? data_7 : 25'h0; // @[Mux.scala 27:72]
  wire [24:0] _T_96 = _T_88 | _T_89; // @[Mux.scala 27:72]
  wire [24:0] _T_97 = _T_96 | _T_90; // @[Mux.scala 27:72]
  wire [24:0] _T_98 = _T_97 | _T_91; // @[Mux.scala 27:72]
  wire [24:0] _T_99 = _T_98 | _T_92; // @[Mux.scala 27:72]
  wire [24:0] _T_100 = _T_99 | _T_93; // @[Mux.scala 27:72]
  wire [24:0] _T_101 = _T_100 | _T_94; // @[Mux.scala 27:72]
  wire [24:0] pte_cache_data = _T_101 | _T_95; // @[Mux.scala 27:72]
  reg  pte_hit; // @[PTW.scala 197:24]
  reg [127:0] g_0; // @[PTW.scala 225:16]
  reg [127:0] valid_1_0; // @[PTW.scala 226:24]
  wire [19:0] r_tag = r_req_addr[26:7]; // @[package.scala 154:13]
  wire [6:0] r_idx = r_req_addr[6:0]; // @[package.scala 154:13]
  wire [127:0] _T_109 = valid_1_0 >> r_idx; // @[PTW.scala 228:34]
  wire  r_valid_vec = _T_109[0]; // @[PTW.scala 228:34]
  wire  _T_111 = l2_refill & _T_27; // @[PTW.scala 229:21]
  wire [24:0] entry_ppn = r_pte_ppn[24:0]; // @[PTW.scala 230:23 PTW.scala 233:13]
  wire [50:0] lo_9 = {r_tag,entry_ppn,r_pte_d,r_pte_a,r_pte_u,r_pte_x,r_pte_w,r_pte_r}; // @[PTW.scala 235:78]
  wire  hi_9 = ^lo_9; // @[ECC.scala 71:59]
  wire [127:0] mask = 128'h1 << r_idx; // @[OneHot.scala 58:35]
  wire [127:0] _T_114 = valid_1_0 | mask; // @[PTW.scala 240:36]
  wire [127:0] _T_115 = g_0 | mask; // @[PTW.scala 241:41]
  wire [127:0] _T_116 = ~mask; // @[PTW.scala 241:58]
  wire [127:0] _T_117 = g_0 & _T_116; // @[PTW.scala 241:56]
  wire [127:0] _T_120 = 128'h1 << io_dpath_sfence_bits_addr[18:12]; // @[OneHot.scala 58:35]
  wire [127:0] _T_121 = ~_T_120; // @[PTW.scala 248:54]
  wire [127:0] _T_122 = valid_1_0 & _T_121; // @[PTW.scala 248:52]
  wire [127:0] _T_123 = valid_1_0 & g_0; // @[PTW.scala 249:52]
  wire [127:0] _T_124 = io_dpath_sfence_bits_rs2 ? _T_123 : 128'h0; // @[PTW.scala 249:14]
  wire  s0_valid = _arb_io_out_ready_T_1 & _T_22; // @[PTW.scala 253:31]
  reg  s1_valid; // @[PTW.scala 254:27]
  reg  s2_valid; // @[PTW.scala 255:27]
  reg [51:0] r_1; // @[Reg.scala 15:16]
  wire [50:0] uncorrected = r_1[50:0]; // @[ECC.scala 74:24]
  wire  uncorrectable = ^r_1; // @[ECC.scala 77:27]
  reg  s2_valid_vec; // @[Reg.scala 15:16]
  wire [127:0] _T_132 = g_0 >> r_idx; // @[PTW.scala 259:41]
  reg  s2_g_vec_0; // @[Reg.scala 15:16]
  wire  s2_error = s2_valid_vec & uncorrectable; // @[PTW.scala 260:81]
  wire  s2_entry_vec_0_r = uncorrected[0]; // @[PTW.scala 263:59]
  wire  s2_entry_vec_0_w = uncorrected[1]; // @[PTW.scala 263:59]
  wire  s2_entry_vec_0_x = uncorrected[2]; // @[PTW.scala 263:59]
  wire  s2_entry_vec_0_u = uncorrected[3]; // @[PTW.scala 263:59]
  wire  s2_entry_vec_0_a = uncorrected[4]; // @[PTW.scala 263:59]
  wire  s2_entry_vec_0_d = uncorrected[5]; // @[PTW.scala 263:59]
  wire [24:0] s2_entry_vec_0_ppn = uncorrected[30:6]; // @[PTW.scala 263:59]
  wire [19:0] s2_entry_vec_0_tag = uncorrected[50:31]; // @[PTW.scala 263:59]
  wire  s2_hit_vec_0 = s2_valid_vec & r_tag == s2_entry_vec_0_tag; // @[PTW.scala 264:83]
  wire  s2_hit = s2_valid & s2_hit_vec_0; // @[PTW.scala 265:27]
  wire [65:0] _pmaPgLevelHomogeneous_T = pte_addr ^ 66'h80000000; // @[Parameters.scala 137:31]
  wire [66:0] _pmaPgLevelHomogeneous_T_1 = {1'b0,$signed(_pmaPgLevelHomogeneous_T)}; // @[Parameters.scala 137:49]
  wire [66:0] _pmaPgLevelHomogeneous_T_3 = $signed(_pmaPgLevelHomogeneous_T_1) & -67'sh80000000; // @[Parameters.scala 137:52]
  wire  _pmaPgLevelHomogeneous_T_4 = $signed(_pmaPgLevelHomogeneous_T_3) == 67'sh0; // @[Parameters.scala 137:67]
  wire [65:0] _pmaPgLevelHomogeneous_T_5 = pte_addr ^ 66'h800000000; // @[Parameters.scala 137:31]
  wire [66:0] _pmaPgLevelHomogeneous_T_6 = {1'b0,$signed(_pmaPgLevelHomogeneous_T_5)}; // @[Parameters.scala 137:49]
  wire [66:0] _pmaPgLevelHomogeneous_T_8 = $signed(_pmaPgLevelHomogeneous_T_6) & -67'sh800000000; // @[Parameters.scala 137:52]
  wire  _pmaPgLevelHomogeneous_T_9 = $signed(_pmaPgLevelHomogeneous_T_8) == 67'sh0; // @[Parameters.scala 137:67]
  wire [65:0] _pmaPgLevelHomogeneous_T_10 = pte_addr ^ 66'h1000000000; // @[Parameters.scala 137:31]
  wire [66:0] _pmaPgLevelHomogeneous_T_11 = {1'b0,$signed(_pmaPgLevelHomogeneous_T_10)}; // @[Parameters.scala 137:49]
  wire [66:0] _pmaPgLevelHomogeneous_T_13 = $signed(_pmaPgLevelHomogeneous_T_11) & -67'sh1000000000; // @[Parameters.scala 137:52]
  wire  _pmaPgLevelHomogeneous_T_14 = $signed(_pmaPgLevelHomogeneous_T_13) == 67'sh0; // @[Parameters.scala 137:67]
  wire  pmaPgLevelHomogeneous_0 = _pmaPgLevelHomogeneous_T_4 | _pmaPgLevelHomogeneous_T_9 | _pmaPgLevelHomogeneous_T_14; // @[TLBPermissions.scala 98:65]
  wire [65:0] _pmaPgLevelHomogeneous_T_40 = pte_addr ^ 66'h8000000; // @[Parameters.scala 137:31]
  wire [66:0] _pmaPgLevelHomogeneous_T_41 = {1'b0,$signed(_pmaPgLevelHomogeneous_T_40)}; // @[Parameters.scala 137:49]
  wire [66:0] _pmaPgLevelHomogeneous_T_43 = $signed(_pmaPgLevelHomogeneous_T_41) & -67'sh2200000; // @[Parameters.scala 137:52]
  wire  _pmaPgLevelHomogeneous_T_44 = $signed(_pmaPgLevelHomogeneous_T_43) == 67'sh0; // @[Parameters.scala 137:67]
  wire [65:0] _pmaPgLevelHomogeneous_T_45 = pte_addr ^ 66'hc000000; // @[Parameters.scala 137:31]
  wire [66:0] _pmaPgLevelHomogeneous_T_46 = {1'b0,$signed(_pmaPgLevelHomogeneous_T_45)}; // @[Parameters.scala 137:49]
  wire [66:0] _pmaPgLevelHomogeneous_T_48 = $signed(_pmaPgLevelHomogeneous_T_46) & -67'sh4000000; // @[Parameters.scala 137:52]
  wire  _pmaPgLevelHomogeneous_T_49 = $signed(_pmaPgLevelHomogeneous_T_48) == 67'sh0; // @[Parameters.scala 137:67]
  wire  pmaPgLevelHomogeneous_1 = _pmaPgLevelHomogeneous_T_44 | _pmaPgLevelHomogeneous_T_49 | _pmaPgLevelHomogeneous_T_4
     | _pmaPgLevelHomogeneous_T_9 | _pmaPgLevelHomogeneous_T_14; // @[TLBPermissions.scala 98:65]
  wire [66:0] _pmaPgLevelHomogeneous_T_105 = {1'b0,$signed(pte_addr)}; // @[Parameters.scala 137:49]
  wire [66:0] _pmaPgLevelHomogeneous_T_107 = $signed(_pmaPgLevelHomogeneous_T_105) & -67'sh5000; // @[Parameters.scala 137:52]
  wire  _pmaPgLevelHomogeneous_T_108 = $signed(_pmaPgLevelHomogeneous_T_107) == 67'sh0; // @[Parameters.scala 137:67]
  wire [65:0] _pmaPgLevelHomogeneous_T_109 = pte_addr ^ 66'h3000; // @[Parameters.scala 137:31]
  wire [66:0] _pmaPgLevelHomogeneous_T_110 = {1'b0,$signed(_pmaPgLevelHomogeneous_T_109)}; // @[Parameters.scala 137:49]
  wire [66:0] _pmaPgLevelHomogeneous_T_112 = $signed(_pmaPgLevelHomogeneous_T_110) & -67'sh10001000; // @[Parameters.scala 137:52]
  wire  _pmaPgLevelHomogeneous_T_113 = $signed(_pmaPgLevelHomogeneous_T_112) == 67'sh0; // @[Parameters.scala 137:67]
  wire [65:0] _pmaPgLevelHomogeneous_T_114 = pte_addr ^ 66'h2000000; // @[Parameters.scala 137:31]
  wire [66:0] _pmaPgLevelHomogeneous_T_115 = {1'b0,$signed(_pmaPgLevelHomogeneous_T_114)}; // @[Parameters.scala 137:49]
  wire [66:0] _pmaPgLevelHomogeneous_T_117 = $signed(_pmaPgLevelHomogeneous_T_115) & -67'sh10000; // @[Parameters.scala 137:52]
  wire  _pmaPgLevelHomogeneous_T_118 = $signed(_pmaPgLevelHomogeneous_T_117) == 67'sh0; // @[Parameters.scala 137:67]
  wire [65:0] _pmaPgLevelHomogeneous_T_119 = pte_addr ^ 66'h2010000; // @[Parameters.scala 137:31]
  wire [66:0] _pmaPgLevelHomogeneous_T_120 = {1'b0,$signed(_pmaPgLevelHomogeneous_T_119)}; // @[Parameters.scala 137:49]
  wire [66:0] _pmaPgLevelHomogeneous_T_122 = $signed(_pmaPgLevelHomogeneous_T_120) & -67'sh4000; // @[Parameters.scala 137:52]
  wire  _pmaPgLevelHomogeneous_T_123 = $signed(_pmaPgLevelHomogeneous_T_122) == 67'sh0; // @[Parameters.scala 137:67]
  wire [65:0] _pmaPgLevelHomogeneous_T_134 = pte_addr ^ 66'h10000000; // @[Parameters.scala 137:31]
  wire [66:0] _pmaPgLevelHomogeneous_T_135 = {1'b0,$signed(_pmaPgLevelHomogeneous_T_134)}; // @[Parameters.scala 137:49]
  wire [66:0] _pmaPgLevelHomogeneous_T_137 = $signed(_pmaPgLevelHomogeneous_T_135) & -67'sh2000; // @[Parameters.scala 137:52]
  wire  _pmaPgLevelHomogeneous_T_138 = $signed(_pmaPgLevelHomogeneous_T_137) == 67'sh0; // @[Parameters.scala 137:67]
  wire  pmaPgLevelHomogeneous_2 = _pmaPgLevelHomogeneous_T_108 | _pmaPgLevelHomogeneous_T_113 |
    _pmaPgLevelHomogeneous_T_118 | _pmaPgLevelHomogeneous_T_123 | _pmaPgLevelHomogeneous_T_44 |
    _pmaPgLevelHomogeneous_T_49 | _pmaPgLevelHomogeneous_T_138 | _pmaPgLevelHomogeneous_T_4 | _pmaPgLevelHomogeneous_T_9
     | _pmaPgLevelHomogeneous_T_14; // @[TLBPermissions.scala 98:65]
  wire  _pmaHomogeneous_T_1 = _pte_addr_vpn_idx_T ? pmaPgLevelHomogeneous_1 : pmaPgLevelHomogeneous_0; // @[package.scala 32:76]
  wire  _pmaHomogeneous_T_3 = _pte_addr_vpn_idx_T_2 ? pmaPgLevelHomogeneous_2 : _pmaHomogeneous_T_1; // @[package.scala 32:76]
  wire  pmaHomogeneous = count == 2'h3 ? pmaPgLevelHomogeneous_2 : _pmaHomogeneous_T_3; // @[package.scala 32:76]
  wire [65:0] _pmpHomogeneous_T_1 = {pte_addr[65:12], 12'h0}; // @[PTW.scala 311:92]
  wire  _pmpHomogeneous_maskHomogeneous_T_4 = _pte_addr_vpn_idx_T ? io_dpath_pmp_0_mask[20] : io_dpath_pmp_0_mask[29]; // @[package.scala 32:76]
  wire  _pmpHomogeneous_maskHomogeneous_T_6 = _pte_addr_vpn_idx_T_2 ? io_dpath_pmp_0_mask[11] :
    _pmpHomogeneous_maskHomogeneous_T_4; // @[package.scala 32:76]
  wire  pmpHomogeneous_maskHomogeneous = count == 2'h3 ? io_dpath_pmp_0_mask[11] : _pmpHomogeneous_maskHomogeneous_T_6; // @[package.scala 32:76]
  wire [36:0] _pmpHomogeneous_T_3 = {io_dpath_pmp_0_addr, 2'h0}; // @[PMP.scala 62:36]
  wire [36:0] _pmpHomogeneous_T_4 = ~_pmpHomogeneous_T_3; // @[PMP.scala 62:29]
  wire [36:0] _pmpHomogeneous_T_5 = _pmpHomogeneous_T_4 | 37'h3; // @[PMP.scala 62:48]
  wire [36:0] _pmpHomogeneous_T_6 = ~_pmpHomogeneous_T_5; // @[PMP.scala 62:27]
  wire [65:0] _GEN_64 = {{29'd0}, _pmpHomogeneous_T_6}; // @[PMP.scala 100:53]
  wire [65:0] _pmpHomogeneous_T_7 = _pmpHomogeneous_T_1 ^ _GEN_64; // @[PMP.scala 100:53]
  wire  _pmpHomogeneous_T_9 = _pmpHomogeneous_T_7[65:30] != 36'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_16 = _pmpHomogeneous_T_7[65:21] != 45'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_23 = _pmpHomogeneous_T_7[65:12] != 54'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_25 = _pte_addr_vpn_idx_T ? _pmpHomogeneous_T_16 : _pmpHomogeneous_T_9; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_27 = _pte_addr_vpn_idx_T_2 ? _pmpHomogeneous_T_23 : _pmpHomogeneous_T_25; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_29 = count == 2'h3 ? _pmpHomogeneous_T_23 : _pmpHomogeneous_T_27; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_30 = pmpHomogeneous_maskHomogeneous | _pmpHomogeneous_T_29; // @[PMP.scala 100:21]
  wire  pmpHomogeneous_beginsAfterUpper = ~(_pmpHomogeneous_T_1 < _GEN_64); // @[PMP.scala 109:28]
  wire [36:0] _pmpHomogeneous_pgMask_T_1 = _pte_addr_vpn_idx_T ? 37'h1fffe00000 : 37'h1fc0000000; // @[package.scala 32:76]
  wire [36:0] _pmpHomogeneous_pgMask_T_3 = _pte_addr_vpn_idx_T_2 ? 37'h1ffffff000 : _pmpHomogeneous_pgMask_T_1; // @[package.scala 32:76]
  wire [36:0] pmpHomogeneous_pgMask = count == 2'h3 ? 37'h1ffffff000 : _pmpHomogeneous_pgMask_T_3; // @[package.scala 32:76]
  wire [65:0] _GEN_68 = {{29'd0}, pmpHomogeneous_pgMask}; // @[PMP.scala 112:30]
  wire [65:0] _pmpHomogeneous_endsBeforeLower_T = _pmpHomogeneous_T_1 & _GEN_68; // @[PMP.scala 112:30]
  wire [36:0] _pmpHomogeneous_endsBeforeUpper_T_5 = _pmpHomogeneous_T_6 & pmpHomogeneous_pgMask; // @[PMP.scala 113:53]
  wire [65:0] _GEN_70 = {{29'd0}, _pmpHomogeneous_endsBeforeUpper_T_5}; // @[PMP.scala 113:40]
  wire  pmpHomogeneous_endsBeforeUpper = _pmpHomogeneous_endsBeforeLower_T < _GEN_70; // @[PMP.scala 113:40]
  wire  _pmpHomogeneous_T_35 = pmpHomogeneous_beginsAfterUpper | pmpHomogeneous_endsBeforeUpper; // @[PMP.scala 115:41]
  wire  _pmpHomogeneous_T_37 = io_dpath_pmp_0_cfg_a[1] ? _pmpHomogeneous_T_30 : ~io_dpath_pmp_0_cfg_a[0] |
    _pmpHomogeneous_T_35; // @[PMP.scala 120:8]
  wire  _pmpHomogeneous_maskHomogeneous_T_12 = _pte_addr_vpn_idx_T ? io_dpath_pmp_1_mask[20] : io_dpath_pmp_1_mask[29]; // @[package.scala 32:76]
  wire  _pmpHomogeneous_maskHomogeneous_T_14 = _pte_addr_vpn_idx_T_2 ? io_dpath_pmp_1_mask[11] :
    _pmpHomogeneous_maskHomogeneous_T_12; // @[package.scala 32:76]
  wire  pmpHomogeneous_maskHomogeneous_1 = count == 2'h3 ? io_dpath_pmp_1_mask[11] :
    _pmpHomogeneous_maskHomogeneous_T_14; // @[package.scala 32:76]
  wire [36:0] _pmpHomogeneous_T_40 = {io_dpath_pmp_1_addr, 2'h0}; // @[PMP.scala 62:36]
  wire [36:0] _pmpHomogeneous_T_41 = ~_pmpHomogeneous_T_40; // @[PMP.scala 62:29]
  wire [36:0] _pmpHomogeneous_T_42 = _pmpHomogeneous_T_41 | 37'h3; // @[PMP.scala 62:48]
  wire [36:0] _pmpHomogeneous_T_43 = ~_pmpHomogeneous_T_42; // @[PMP.scala 62:27]
  wire [65:0] _GEN_71 = {{29'd0}, _pmpHomogeneous_T_43}; // @[PMP.scala 100:53]
  wire [65:0] _pmpHomogeneous_T_44 = _pmpHomogeneous_T_1 ^ _GEN_71; // @[PMP.scala 100:53]
  wire  _pmpHomogeneous_T_46 = _pmpHomogeneous_T_44[65:30] != 36'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_53 = _pmpHomogeneous_T_44[65:21] != 45'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_60 = _pmpHomogeneous_T_44[65:12] != 54'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_62 = _pte_addr_vpn_idx_T ? _pmpHomogeneous_T_53 : _pmpHomogeneous_T_46; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_64 = _pte_addr_vpn_idx_T_2 ? _pmpHomogeneous_T_60 : _pmpHomogeneous_T_62; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_66 = count == 2'h3 ? _pmpHomogeneous_T_60 : _pmpHomogeneous_T_64; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_67 = pmpHomogeneous_maskHomogeneous_1 | _pmpHomogeneous_T_66; // @[PMP.scala 100:21]
  wire  pmpHomogeneous_beginsAfterUpper_1 = ~(_pmpHomogeneous_T_1 < _GEN_71); // @[PMP.scala 109:28]
  wire [36:0] _pmpHomogeneous_endsBeforeUpper_T_11 = _pmpHomogeneous_T_43 & pmpHomogeneous_pgMask; // @[PMP.scala 113:53]
  wire [65:0] _GEN_79 = {{29'd0}, _pmpHomogeneous_endsBeforeUpper_T_11}; // @[PMP.scala 113:40]
  wire  pmpHomogeneous_endsBeforeUpper_1 = _pmpHomogeneous_endsBeforeLower_T < _GEN_79; // @[PMP.scala 113:40]
  wire  _pmpHomogeneous_T_72 = pmpHomogeneous_endsBeforeUpper | pmpHomogeneous_beginsAfterUpper_1 |
    pmpHomogeneous_beginsAfterUpper & pmpHomogeneous_endsBeforeUpper_1; // @[PMP.scala 115:41]
  wire  _pmpHomogeneous_T_74 = io_dpath_pmp_1_cfg_a[1] ? _pmpHomogeneous_T_67 : ~io_dpath_pmp_1_cfg_a[0] |
    _pmpHomogeneous_T_72; // @[PMP.scala 120:8]
  wire  _pmpHomogeneous_maskHomogeneous_T_20 = _pte_addr_vpn_idx_T ? io_dpath_pmp_2_mask[20] : io_dpath_pmp_2_mask[29]; // @[package.scala 32:76]
  wire  _pmpHomogeneous_maskHomogeneous_T_22 = _pte_addr_vpn_idx_T_2 ? io_dpath_pmp_2_mask[11] :
    _pmpHomogeneous_maskHomogeneous_T_20; // @[package.scala 32:76]
  wire  pmpHomogeneous_maskHomogeneous_2 = count == 2'h3 ? io_dpath_pmp_2_mask[11] :
    _pmpHomogeneous_maskHomogeneous_T_22; // @[package.scala 32:76]
  wire [36:0] _pmpHomogeneous_T_77 = {io_dpath_pmp_2_addr, 2'h0}; // @[PMP.scala 62:36]
  wire [36:0] _pmpHomogeneous_T_78 = ~_pmpHomogeneous_T_77; // @[PMP.scala 62:29]
  wire [36:0] _pmpHomogeneous_T_79 = _pmpHomogeneous_T_78 | 37'h3; // @[PMP.scala 62:48]
  wire [36:0] _pmpHomogeneous_T_80 = ~_pmpHomogeneous_T_79; // @[PMP.scala 62:27]
  wire [65:0] _GEN_80 = {{29'd0}, _pmpHomogeneous_T_80}; // @[PMP.scala 100:53]
  wire [65:0] _pmpHomogeneous_T_81 = _pmpHomogeneous_T_1 ^ _GEN_80; // @[PMP.scala 100:53]
  wire  _pmpHomogeneous_T_83 = _pmpHomogeneous_T_81[65:30] != 36'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_90 = _pmpHomogeneous_T_81[65:21] != 45'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_97 = _pmpHomogeneous_T_81[65:12] != 54'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_99 = _pte_addr_vpn_idx_T ? _pmpHomogeneous_T_90 : _pmpHomogeneous_T_83; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_101 = _pte_addr_vpn_idx_T_2 ? _pmpHomogeneous_T_97 : _pmpHomogeneous_T_99; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_103 = count == 2'h3 ? _pmpHomogeneous_T_97 : _pmpHomogeneous_T_101; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_104 = pmpHomogeneous_maskHomogeneous_2 | _pmpHomogeneous_T_103; // @[PMP.scala 100:21]
  wire  pmpHomogeneous_beginsAfterUpper_2 = ~(_pmpHomogeneous_T_1 < _GEN_80); // @[PMP.scala 109:28]
  wire [36:0] _pmpHomogeneous_endsBeforeUpper_T_17 = _pmpHomogeneous_T_80 & pmpHomogeneous_pgMask; // @[PMP.scala 113:53]
  wire [65:0] _GEN_88 = {{29'd0}, _pmpHomogeneous_endsBeforeUpper_T_17}; // @[PMP.scala 113:40]
  wire  pmpHomogeneous_endsBeforeUpper_2 = _pmpHomogeneous_endsBeforeLower_T < _GEN_88; // @[PMP.scala 113:40]
  wire  _pmpHomogeneous_T_109 = pmpHomogeneous_endsBeforeUpper_1 | pmpHomogeneous_beginsAfterUpper_2 |
    pmpHomogeneous_beginsAfterUpper_1 & pmpHomogeneous_endsBeforeUpper_2; // @[PMP.scala 115:41]
  wire  _pmpHomogeneous_T_111 = io_dpath_pmp_2_cfg_a[1] ? _pmpHomogeneous_T_104 : ~io_dpath_pmp_2_cfg_a[0] |
    _pmpHomogeneous_T_109; // @[PMP.scala 120:8]
  wire  _pmpHomogeneous_maskHomogeneous_T_28 = _pte_addr_vpn_idx_T ? io_dpath_pmp_3_mask[20] : io_dpath_pmp_3_mask[29]; // @[package.scala 32:76]
  wire  _pmpHomogeneous_maskHomogeneous_T_30 = _pte_addr_vpn_idx_T_2 ? io_dpath_pmp_3_mask[11] :
    _pmpHomogeneous_maskHomogeneous_T_28; // @[package.scala 32:76]
  wire  pmpHomogeneous_maskHomogeneous_3 = count == 2'h3 ? io_dpath_pmp_3_mask[11] :
    _pmpHomogeneous_maskHomogeneous_T_30; // @[package.scala 32:76]
  wire [36:0] _pmpHomogeneous_T_114 = {io_dpath_pmp_3_addr, 2'h0}; // @[PMP.scala 62:36]
  wire [36:0] _pmpHomogeneous_T_115 = ~_pmpHomogeneous_T_114; // @[PMP.scala 62:29]
  wire [36:0] _pmpHomogeneous_T_116 = _pmpHomogeneous_T_115 | 37'h3; // @[PMP.scala 62:48]
  wire [36:0] _pmpHomogeneous_T_117 = ~_pmpHomogeneous_T_116; // @[PMP.scala 62:27]
  wire [65:0] _GEN_89 = {{29'd0}, _pmpHomogeneous_T_117}; // @[PMP.scala 100:53]
  wire [65:0] _pmpHomogeneous_T_118 = _pmpHomogeneous_T_1 ^ _GEN_89; // @[PMP.scala 100:53]
  wire  _pmpHomogeneous_T_120 = _pmpHomogeneous_T_118[65:30] != 36'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_127 = _pmpHomogeneous_T_118[65:21] != 45'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_134 = _pmpHomogeneous_T_118[65:12] != 54'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_136 = _pte_addr_vpn_idx_T ? _pmpHomogeneous_T_127 : _pmpHomogeneous_T_120; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_138 = _pte_addr_vpn_idx_T_2 ? _pmpHomogeneous_T_134 : _pmpHomogeneous_T_136; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_140 = count == 2'h3 ? _pmpHomogeneous_T_134 : _pmpHomogeneous_T_138; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_141 = pmpHomogeneous_maskHomogeneous_3 | _pmpHomogeneous_T_140; // @[PMP.scala 100:21]
  wire  pmpHomogeneous_beginsAfterUpper_3 = ~(_pmpHomogeneous_T_1 < _GEN_89); // @[PMP.scala 109:28]
  wire [36:0] _pmpHomogeneous_endsBeforeUpper_T_23 = _pmpHomogeneous_T_117 & pmpHomogeneous_pgMask; // @[PMP.scala 113:53]
  wire [65:0] _GEN_97 = {{29'd0}, _pmpHomogeneous_endsBeforeUpper_T_23}; // @[PMP.scala 113:40]
  wire  pmpHomogeneous_endsBeforeUpper_3 = _pmpHomogeneous_endsBeforeLower_T < _GEN_97; // @[PMP.scala 113:40]
  wire  _pmpHomogeneous_T_146 = pmpHomogeneous_endsBeforeUpper_2 | pmpHomogeneous_beginsAfterUpper_3 |
    pmpHomogeneous_beginsAfterUpper_2 & pmpHomogeneous_endsBeforeUpper_3; // @[PMP.scala 115:41]
  wire  _pmpHomogeneous_T_148 = io_dpath_pmp_3_cfg_a[1] ? _pmpHomogeneous_T_141 : ~io_dpath_pmp_3_cfg_a[0] |
    _pmpHomogeneous_T_146; // @[PMP.scala 120:8]
  wire  _pmpHomogeneous_maskHomogeneous_T_36 = _pte_addr_vpn_idx_T ? io_dpath_pmp_4_mask[20] : io_dpath_pmp_4_mask[29]; // @[package.scala 32:76]
  wire  _pmpHomogeneous_maskHomogeneous_T_38 = _pte_addr_vpn_idx_T_2 ? io_dpath_pmp_4_mask[11] :
    _pmpHomogeneous_maskHomogeneous_T_36; // @[package.scala 32:76]
  wire  pmpHomogeneous_maskHomogeneous_4 = count == 2'h3 ? io_dpath_pmp_4_mask[11] :
    _pmpHomogeneous_maskHomogeneous_T_38; // @[package.scala 32:76]
  wire [36:0] _pmpHomogeneous_T_151 = {io_dpath_pmp_4_addr, 2'h0}; // @[PMP.scala 62:36]
  wire [36:0] _pmpHomogeneous_T_152 = ~_pmpHomogeneous_T_151; // @[PMP.scala 62:29]
  wire [36:0] _pmpHomogeneous_T_153 = _pmpHomogeneous_T_152 | 37'h3; // @[PMP.scala 62:48]
  wire [36:0] _pmpHomogeneous_T_154 = ~_pmpHomogeneous_T_153; // @[PMP.scala 62:27]
  wire [65:0] _GEN_98 = {{29'd0}, _pmpHomogeneous_T_154}; // @[PMP.scala 100:53]
  wire [65:0] _pmpHomogeneous_T_155 = _pmpHomogeneous_T_1 ^ _GEN_98; // @[PMP.scala 100:53]
  wire  _pmpHomogeneous_T_157 = _pmpHomogeneous_T_155[65:30] != 36'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_164 = _pmpHomogeneous_T_155[65:21] != 45'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_171 = _pmpHomogeneous_T_155[65:12] != 54'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_173 = _pte_addr_vpn_idx_T ? _pmpHomogeneous_T_164 : _pmpHomogeneous_T_157; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_175 = _pte_addr_vpn_idx_T_2 ? _pmpHomogeneous_T_171 : _pmpHomogeneous_T_173; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_177 = count == 2'h3 ? _pmpHomogeneous_T_171 : _pmpHomogeneous_T_175; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_178 = pmpHomogeneous_maskHomogeneous_4 | _pmpHomogeneous_T_177; // @[PMP.scala 100:21]
  wire  pmpHomogeneous_beginsAfterUpper_4 = ~(_pmpHomogeneous_T_1 < _GEN_98); // @[PMP.scala 109:28]
  wire [36:0] _pmpHomogeneous_endsBeforeUpper_T_29 = _pmpHomogeneous_T_154 & pmpHomogeneous_pgMask; // @[PMP.scala 113:53]
  wire [65:0] _GEN_106 = {{29'd0}, _pmpHomogeneous_endsBeforeUpper_T_29}; // @[PMP.scala 113:40]
  wire  pmpHomogeneous_endsBeforeUpper_4 = _pmpHomogeneous_endsBeforeLower_T < _GEN_106; // @[PMP.scala 113:40]
  wire  _pmpHomogeneous_T_183 = pmpHomogeneous_endsBeforeUpper_3 | pmpHomogeneous_beginsAfterUpper_4 |
    pmpHomogeneous_beginsAfterUpper_3 & pmpHomogeneous_endsBeforeUpper_4; // @[PMP.scala 115:41]
  wire  _pmpHomogeneous_T_185 = io_dpath_pmp_4_cfg_a[1] ? _pmpHomogeneous_T_178 : ~io_dpath_pmp_4_cfg_a[0] |
    _pmpHomogeneous_T_183; // @[PMP.scala 120:8]
  wire  _pmpHomogeneous_maskHomogeneous_T_44 = _pte_addr_vpn_idx_T ? io_dpath_pmp_5_mask[20] : io_dpath_pmp_5_mask[29]; // @[package.scala 32:76]
  wire  _pmpHomogeneous_maskHomogeneous_T_46 = _pte_addr_vpn_idx_T_2 ? io_dpath_pmp_5_mask[11] :
    _pmpHomogeneous_maskHomogeneous_T_44; // @[package.scala 32:76]
  wire  pmpHomogeneous_maskHomogeneous_5 = count == 2'h3 ? io_dpath_pmp_5_mask[11] :
    _pmpHomogeneous_maskHomogeneous_T_46; // @[package.scala 32:76]
  wire [36:0] _pmpHomogeneous_T_188 = {io_dpath_pmp_5_addr, 2'h0}; // @[PMP.scala 62:36]
  wire [36:0] _pmpHomogeneous_T_189 = ~_pmpHomogeneous_T_188; // @[PMP.scala 62:29]
  wire [36:0] _pmpHomogeneous_T_190 = _pmpHomogeneous_T_189 | 37'h3; // @[PMP.scala 62:48]
  wire [36:0] _pmpHomogeneous_T_191 = ~_pmpHomogeneous_T_190; // @[PMP.scala 62:27]
  wire [65:0] _GEN_107 = {{29'd0}, _pmpHomogeneous_T_191}; // @[PMP.scala 100:53]
  wire [65:0] _pmpHomogeneous_T_192 = _pmpHomogeneous_T_1 ^ _GEN_107; // @[PMP.scala 100:53]
  wire  _pmpHomogeneous_T_194 = _pmpHomogeneous_T_192[65:30] != 36'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_201 = _pmpHomogeneous_T_192[65:21] != 45'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_208 = _pmpHomogeneous_T_192[65:12] != 54'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_210 = _pte_addr_vpn_idx_T ? _pmpHomogeneous_T_201 : _pmpHomogeneous_T_194; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_212 = _pte_addr_vpn_idx_T_2 ? _pmpHomogeneous_T_208 : _pmpHomogeneous_T_210; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_214 = count == 2'h3 ? _pmpHomogeneous_T_208 : _pmpHomogeneous_T_212; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_215 = pmpHomogeneous_maskHomogeneous_5 | _pmpHomogeneous_T_214; // @[PMP.scala 100:21]
  wire  pmpHomogeneous_beginsAfterUpper_5 = ~(_pmpHomogeneous_T_1 < _GEN_107); // @[PMP.scala 109:28]
  wire [36:0] _pmpHomogeneous_endsBeforeUpper_T_35 = _pmpHomogeneous_T_191 & pmpHomogeneous_pgMask; // @[PMP.scala 113:53]
  wire [65:0] _GEN_115 = {{29'd0}, _pmpHomogeneous_endsBeforeUpper_T_35}; // @[PMP.scala 113:40]
  wire  pmpHomogeneous_endsBeforeUpper_5 = _pmpHomogeneous_endsBeforeLower_T < _GEN_115; // @[PMP.scala 113:40]
  wire  _pmpHomogeneous_T_220 = pmpHomogeneous_endsBeforeUpper_4 | pmpHomogeneous_beginsAfterUpper_5 |
    pmpHomogeneous_beginsAfterUpper_4 & pmpHomogeneous_endsBeforeUpper_5; // @[PMP.scala 115:41]
  wire  _pmpHomogeneous_T_222 = io_dpath_pmp_5_cfg_a[1] ? _pmpHomogeneous_T_215 : ~io_dpath_pmp_5_cfg_a[0] |
    _pmpHomogeneous_T_220; // @[PMP.scala 120:8]
  wire  _pmpHomogeneous_maskHomogeneous_T_52 = _pte_addr_vpn_idx_T ? io_dpath_pmp_6_mask[20] : io_dpath_pmp_6_mask[29]; // @[package.scala 32:76]
  wire  _pmpHomogeneous_maskHomogeneous_T_54 = _pte_addr_vpn_idx_T_2 ? io_dpath_pmp_6_mask[11] :
    _pmpHomogeneous_maskHomogeneous_T_52; // @[package.scala 32:76]
  wire  pmpHomogeneous_maskHomogeneous_6 = count == 2'h3 ? io_dpath_pmp_6_mask[11] :
    _pmpHomogeneous_maskHomogeneous_T_54; // @[package.scala 32:76]
  wire [36:0] _pmpHomogeneous_T_225 = {io_dpath_pmp_6_addr, 2'h0}; // @[PMP.scala 62:36]
  wire [36:0] _pmpHomogeneous_T_226 = ~_pmpHomogeneous_T_225; // @[PMP.scala 62:29]
  wire [36:0] _pmpHomogeneous_T_227 = _pmpHomogeneous_T_226 | 37'h3; // @[PMP.scala 62:48]
  wire [36:0] _pmpHomogeneous_T_228 = ~_pmpHomogeneous_T_227; // @[PMP.scala 62:27]
  wire [65:0] _GEN_116 = {{29'd0}, _pmpHomogeneous_T_228}; // @[PMP.scala 100:53]
  wire [65:0] _pmpHomogeneous_T_229 = _pmpHomogeneous_T_1 ^ _GEN_116; // @[PMP.scala 100:53]
  wire  _pmpHomogeneous_T_231 = _pmpHomogeneous_T_229[65:30] != 36'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_238 = _pmpHomogeneous_T_229[65:21] != 45'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_245 = _pmpHomogeneous_T_229[65:12] != 54'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_247 = _pte_addr_vpn_idx_T ? _pmpHomogeneous_T_238 : _pmpHomogeneous_T_231; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_249 = _pte_addr_vpn_idx_T_2 ? _pmpHomogeneous_T_245 : _pmpHomogeneous_T_247; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_251 = count == 2'h3 ? _pmpHomogeneous_T_245 : _pmpHomogeneous_T_249; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_252 = pmpHomogeneous_maskHomogeneous_6 | _pmpHomogeneous_T_251; // @[PMP.scala 100:21]
  wire  pmpHomogeneous_beginsAfterUpper_6 = ~(_pmpHomogeneous_T_1 < _GEN_116); // @[PMP.scala 109:28]
  wire [36:0] _pmpHomogeneous_endsBeforeUpper_T_41 = _pmpHomogeneous_T_228 & pmpHomogeneous_pgMask; // @[PMP.scala 113:53]
  wire [65:0] _GEN_124 = {{29'd0}, _pmpHomogeneous_endsBeforeUpper_T_41}; // @[PMP.scala 113:40]
  wire  pmpHomogeneous_endsBeforeUpper_6 = _pmpHomogeneous_endsBeforeLower_T < _GEN_124; // @[PMP.scala 113:40]
  wire  _pmpHomogeneous_T_257 = pmpHomogeneous_endsBeforeUpper_5 | pmpHomogeneous_beginsAfterUpper_6 |
    pmpHomogeneous_beginsAfterUpper_5 & pmpHomogeneous_endsBeforeUpper_6; // @[PMP.scala 115:41]
  wire  _pmpHomogeneous_T_259 = io_dpath_pmp_6_cfg_a[1] ? _pmpHomogeneous_T_252 : ~io_dpath_pmp_6_cfg_a[0] |
    _pmpHomogeneous_T_257; // @[PMP.scala 120:8]
  wire  _pmpHomogeneous_maskHomogeneous_T_60 = _pte_addr_vpn_idx_T ? io_dpath_pmp_7_mask[20] : io_dpath_pmp_7_mask[29]; // @[package.scala 32:76]
  wire  _pmpHomogeneous_maskHomogeneous_T_62 = _pte_addr_vpn_idx_T_2 ? io_dpath_pmp_7_mask[11] :
    _pmpHomogeneous_maskHomogeneous_T_60; // @[package.scala 32:76]
  wire  pmpHomogeneous_maskHomogeneous_7 = count == 2'h3 ? io_dpath_pmp_7_mask[11] :
    _pmpHomogeneous_maskHomogeneous_T_62; // @[package.scala 32:76]
  wire [36:0] _pmpHomogeneous_T_262 = {io_dpath_pmp_7_addr, 2'h0}; // @[PMP.scala 62:36]
  wire [36:0] _pmpHomogeneous_T_263 = ~_pmpHomogeneous_T_262; // @[PMP.scala 62:29]
  wire [36:0] _pmpHomogeneous_T_264 = _pmpHomogeneous_T_263 | 37'h3; // @[PMP.scala 62:48]
  wire [36:0] _pmpHomogeneous_T_265 = ~_pmpHomogeneous_T_264; // @[PMP.scala 62:27]
  wire [65:0] _GEN_125 = {{29'd0}, _pmpHomogeneous_T_265}; // @[PMP.scala 100:53]
  wire [65:0] _pmpHomogeneous_T_266 = _pmpHomogeneous_T_1 ^ _GEN_125; // @[PMP.scala 100:53]
  wire  _pmpHomogeneous_T_268 = _pmpHomogeneous_T_266[65:30] != 36'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_275 = _pmpHomogeneous_T_266[65:21] != 45'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_282 = _pmpHomogeneous_T_266[65:12] != 54'h0; // @[PMP.scala 100:78]
  wire  _pmpHomogeneous_T_284 = _pte_addr_vpn_idx_T ? _pmpHomogeneous_T_275 : _pmpHomogeneous_T_268; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_286 = _pte_addr_vpn_idx_T_2 ? _pmpHomogeneous_T_282 : _pmpHomogeneous_T_284; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_288 = count == 2'h3 ? _pmpHomogeneous_T_282 : _pmpHomogeneous_T_286; // @[package.scala 32:76]
  wire  _pmpHomogeneous_T_289 = pmpHomogeneous_maskHomogeneous_7 | _pmpHomogeneous_T_288; // @[PMP.scala 100:21]
  wire  pmpHomogeneous_beginsAfterUpper_7 = ~(_pmpHomogeneous_T_1 < _GEN_125); // @[PMP.scala 109:28]
  wire [36:0] _pmpHomogeneous_endsBeforeUpper_T_47 = _pmpHomogeneous_T_265 & pmpHomogeneous_pgMask; // @[PMP.scala 113:53]
  wire [65:0] _GEN_133 = {{29'd0}, _pmpHomogeneous_endsBeforeUpper_T_47}; // @[PMP.scala 113:40]
  wire  pmpHomogeneous_endsBeforeUpper_7 = _pmpHomogeneous_endsBeforeLower_T < _GEN_133; // @[PMP.scala 113:40]
  wire  _pmpHomogeneous_T_294 = pmpHomogeneous_endsBeforeUpper_6 | pmpHomogeneous_beginsAfterUpper_7 |
    pmpHomogeneous_beginsAfterUpper_6 & pmpHomogeneous_endsBeforeUpper_7; // @[PMP.scala 115:41]
  wire  _pmpHomogeneous_T_296 = io_dpath_pmp_7_cfg_a[1] ? _pmpHomogeneous_T_289 : ~io_dpath_pmp_7_cfg_a[0] |
    _pmpHomogeneous_T_294; // @[PMP.scala 120:8]
  wire  pmpHomogeneous = _pmpHomogeneous_T_37 & _pmpHomogeneous_T_74 & _pmpHomogeneous_T_111 & _pmpHomogeneous_T_148 &
    _pmpHomogeneous_T_185 & _pmpHomogeneous_T_222 & _pmpHomogeneous_T_259 & _pmpHomogeneous_T_296; // @[PMP.scala 140:10]
  wire  homogeneous = pmaHomogeneous & pmpHomogeneous; // @[PTW.scala 312:36]
  wire  _T_155 = 3'h0 == state; // @[Conditional.scala 37:30]
  wire [2:0] _next_state_T = arb_io_out_bits_valid ? 3'h1 : 3'h0; // @[PTW.scala 334:26]
  wire [2:0] _GEN_316 = _T_22 ? _next_state_T : state; // @[PTW.scala 333:32 PTW.scala 334:20]
  wire  _T_157 = 3'h1 == state; // @[Conditional.scala 37:30]
  wire [1:0] _count_T_3 = count + 2'h1; // @[PTW.scala 340:24]
  wire [2:0] _next_state_T_1 = io_mem_req_ready ? 3'h2 : 3'h1; // @[PTW.scala 343:26]
  wire [1:0] _GEN_317 = pte_cache_hit ? _count_T_3 : count; // @[PTW.scala 339:28 PTW.scala 340:15 PTW.scala 126:18]
  wire [2:0] _GEN_319 = pte_cache_hit ? state : _next_state_T_1; // @[PTW.scala 339:28 PTW.scala 343:20]
  wire  _T_158 = 3'h2 == state; // @[Conditional.scala 37:30]
  wire [2:0] _next_state_T_2 = s2_hit ? 3'h1 : 3'h4; // @[PTW.scala 348:24]
  wire  _T_159 = 3'h4 == state; // @[Conditional.scala 37:30]
  wire  _GEN_320 = ~r_req_dest; // @[PTW.scala 356:32 PTW.scala 356:32 PTW.scala 116:23]
  wire [2:0] _GEN_323 = io_mem_s2_xcpt_ae_ld ? 3'h0 : 3'h5; // @[PTW.scala 353:35 PTW.scala 355:20 PTW.scala 351:18]
  wire  _GEN_324 = io_mem_s2_xcpt_ae_ld & _GEN_320; // @[PTW.scala 353:35 PTW.scala 116:23]
  wire  _GEN_325 = io_mem_s2_xcpt_ae_ld & r_req_dest; // @[PTW.scala 353:35 PTW.scala 116:23]
  wire  _T_162 = 3'h7 == state; // @[Conditional.scala 37:30]
  wire  _T_165 = ~homogeneous; // @[PTW.scala 363:13]
  wire [1:0] _GEN_328 = _T_165 ? 2'h2 : count; // @[PTW.scala 363:27 PTW.scala 364:15 PTW.scala 126:18]
  wire [2:0] _GEN_330 = _T_162 ? 3'h0 : state; // @[Conditional.scala 39:67 PTW.scala 360:18]
  wire  _GEN_331 = _T_162 & _GEN_320; // @[Conditional.scala 39:67 PTW.scala 116:23]
  wire  _GEN_332 = _T_162 & r_req_dest; // @[Conditional.scala 39:67 PTW.scala 116:23]
  wire [1:0] _GEN_334 = _T_162 ? _GEN_328 : count; // @[Conditional.scala 39:67 PTW.scala 126:18]
  wire [2:0] _GEN_336 = _T_159 ? _GEN_323 : _GEN_330; // @[Conditional.scala 39:67]
  wire  _GEN_337 = _T_159 & _traverse_T_8; // @[Conditional.scala 39:67 PTW.scala 352:30 PTW.scala 198:26]
  wire  _GEN_339 = _T_159 ? _GEN_324 : _GEN_331; // @[Conditional.scala 39:67]
  wire  _GEN_340 = _T_159 ? _GEN_325 : _GEN_332; // @[Conditional.scala 39:67]
  wire [1:0] _GEN_341 = _T_159 ? count : _GEN_334; // @[Conditional.scala 39:67 PTW.scala 126:18]
  wire [2:0] _GEN_343 = _T_158 ? _next_state_T_2 : _GEN_336; // @[Conditional.scala 39:67 PTW.scala 348:18]
  wire  _GEN_344 = _T_158 ? 1'h0 : _GEN_337; // @[Conditional.scala 39:67 PTW.scala 198:26]
  wire  _GEN_345 = _T_158 ? 1'h0 : _T_159 & io_mem_s2_xcpt_ae_ld; // @[Conditional.scala 39:67 PTW.scala 127:24]
  wire  _GEN_346 = _T_158 ? 1'h0 : _GEN_339; // @[Conditional.scala 39:67 PTW.scala 116:23]
  wire  _GEN_347 = _T_158 ? 1'h0 : _GEN_340; // @[Conditional.scala 39:67 PTW.scala 116:23]
  wire [1:0] _GEN_348 = _T_158 ? count : _GEN_341; // @[Conditional.scala 39:67 PTW.scala 126:18]
  wire [1:0] _GEN_350 = _T_157 ? _GEN_317 : _GEN_348; // @[Conditional.scala 39:67]
  wire  _GEN_351 = _T_157 & pte_cache_hit; // @[Conditional.scala 39:67 PTW.scala 197:24]
  wire [2:0] _GEN_352 = _T_157 ? _GEN_319 : _GEN_343; // @[Conditional.scala 39:67]
  wire  _GEN_353 = _T_157 ? 1'h0 : _GEN_344; // @[Conditional.scala 39:67 PTW.scala 198:26]
  wire  _GEN_354 = _T_157 ? 1'h0 : _GEN_345; // @[Conditional.scala 39:67 PTW.scala 127:24]
  wire  _GEN_355 = _T_157 ? 1'h0 : _GEN_346; // @[Conditional.scala 39:67 PTW.scala 116:23]
  wire  _GEN_356 = _T_157 ? 1'h0 : _GEN_347; // @[Conditional.scala 39:67 PTW.scala 116:23]
  wire [2:0] _GEN_358 = _T_155 ? _GEN_316 : _GEN_352; // @[Conditional.scala 40:58]
  wire [1:0] _GEN_359 = _T_155 ? 2'h0 : _GEN_350; // @[Conditional.scala 40:58 PTW.scala 336:13]
  wire  _GEN_362 = _T_155 ? 1'h0 : _GEN_354; // @[Conditional.scala 40:58 PTW.scala 127:24]
  wire  _GEN_363 = _T_155 ? 1'h0 : _GEN_355; // @[Conditional.scala 40:58 PTW.scala 116:23]
  wire  _GEN_364 = _T_155 ? 1'h0 : _GEN_356; // @[Conditional.scala 40:58 PTW.scala 116:23]
  wire  _r_pte_T_1 = s2_hit & ~s2_error; // @[PTW.scala 377:16]
  wire  _r_pte_T_2 = state == 3'h7; // @[PTW.scala 378:15]
  wire [53:0] pte_2_ppn = {{10'd0}, io_dpath_ptbr_ppn};
  wire [53:0] _r_pte_T_8_ppn = _T_22 ? pte_2_ppn : r_pte_ppn; // @[PTW.scala 380:8]
  wire [53:0] pte_1_ppn = {{29'd0}, pte_cache_data};
  wire [53:0] _r_pte_T_9_ppn = _T_55 & pte_cache_hit ? pte_1_ppn : _r_pte_T_8_ppn; // @[PTW.scala 379:8]
  wire  _r_pte_T_9_d = _T_55 & pte_cache_hit ? s2_entry_vec_0_d : r_pte_d; // @[PTW.scala 379:8]
  wire  _r_pte_T_9_a = _T_55 & pte_cache_hit ? s2_entry_vec_0_a : r_pte_a; // @[PTW.scala 379:8]
  wire  _r_pte_T_9_g = _T_55 & pte_cache_hit ? s2_g_vec_0 : r_pte_g; // @[PTW.scala 379:8]
  wire  _r_pte_T_9_u = _T_55 & pte_cache_hit ? s2_entry_vec_0_u : r_pte_u; // @[PTW.scala 379:8]
  wire  _r_pte_T_9_x = _T_55 & pte_cache_hit ? s2_entry_vec_0_x : r_pte_x; // @[PTW.scala 379:8]
  wire  _r_pte_T_9_w = _T_55 & pte_cache_hit ? s2_entry_vec_0_w : r_pte_w; // @[PTW.scala 379:8]
  wire  _r_pte_T_9_r = _T_55 & pte_cache_hit ? s2_entry_vec_0_r : r_pte_r; // @[PTW.scala 379:8]
  wire  _r_pte_T_9_v = _T_55 & pte_cache_hit | r_pte_v; // @[PTW.scala 379:8]
  wire [53:0] _r_pte_T_10_ppn = _r_pte_T_2 & _T_165 ? fragmented_superpage_ppn : _r_pte_T_9_ppn; // @[PTW.scala 378:8]
  wire  _r_pte_T_10_d = _r_pte_T_2 & _T_165 ? r_pte_d : _r_pte_T_9_d; // @[PTW.scala 378:8]
  wire  _r_pte_T_10_a = _r_pte_T_2 & _T_165 ? r_pte_a : _r_pte_T_9_a; // @[PTW.scala 378:8]
  wire  _r_pte_T_10_g = _r_pte_T_2 & _T_165 ? r_pte_g : _r_pte_T_9_g; // @[PTW.scala 378:8]
  wire  _r_pte_T_10_u = _r_pte_T_2 & _T_165 ? r_pte_u : _r_pte_T_9_u; // @[PTW.scala 378:8]
  wire  _r_pte_T_10_x = _r_pte_T_2 & _T_165 ? r_pte_x : _r_pte_T_9_x; // @[PTW.scala 378:8]
  wire  _r_pte_T_10_w = _r_pte_T_2 & _T_165 ? r_pte_w : _r_pte_T_9_w; // @[PTW.scala 378:8]
  wire  _r_pte_T_10_r = _r_pte_T_2 & _T_165 ? r_pte_r : _r_pte_T_9_r; // @[PTW.scala 378:8]
  wire  _r_pte_T_10_v = _r_pte_T_2 & _T_165 ? r_pte_v : _r_pte_T_9_v; // @[PTW.scala 378:8]
  wire [53:0] s2_pte_ppn = {{29'd0}, s2_entry_vec_0_ppn}; // @[PTW.scala 273:22 PTW.scala 274:14]
  wire [53:0] _r_pte_T_11_ppn = _r_pte_T_1 ? s2_pte_ppn : _r_pte_T_10_ppn; // @[PTW.scala 377:8]
  wire  _r_pte_T_11_d = _r_pte_T_1 ? s2_entry_vec_0_d : _r_pte_T_10_d; // @[PTW.scala 377:8]
  wire  _r_pte_T_11_a = _r_pte_T_1 ? s2_entry_vec_0_a : _r_pte_T_10_a; // @[PTW.scala 377:8]
  wire  _r_pte_T_11_g = _r_pte_T_1 ? s2_g_vec_0 : _r_pte_T_10_g; // @[PTW.scala 377:8]
  wire  _r_pte_T_11_u = _r_pte_T_1 ? s2_entry_vec_0_u : _r_pte_T_10_u; // @[PTW.scala 377:8]
  wire  _r_pte_T_11_x = _r_pte_T_1 ? s2_entry_vec_0_x : _r_pte_T_10_x; // @[PTW.scala 377:8]
  wire  _r_pte_T_11_w = _r_pte_T_1 ? s2_entry_vec_0_w : _r_pte_T_10_w; // @[PTW.scala 377:8]
  wire  _r_pte_T_11_r = _r_pte_T_1 ? s2_entry_vec_0_r : _r_pte_T_10_r; // @[PTW.scala 377:8]
  wire  _r_pte_T_11_v = _r_pte_T_1 | _r_pte_T_10_v; // @[PTW.scala 377:8]
  wire  _GEN_366 = _GEN_320 | _GEN_363; // @[PTW.scala 386:28 PTW.scala 386:28]
  wire  _GEN_367 = r_req_dest | _GEN_364; // @[PTW.scala 386:28 PTW.scala 386:28]
  wire [2:0] _GEN_368 = _r_pte_T_1 ? 3'h0 : _GEN_358; // @[PTW.scala 383:30 PTW.scala 385:16]
  wire  _GEN_369 = _r_pte_T_1 ? _GEN_366 : _GEN_363; // @[PTW.scala 383:30]
  wire  _GEN_370 = _r_pte_T_1 ? _GEN_367 : _GEN_364; // @[PTW.scala 383:30]
  wire  _GEN_371 = _r_pte_T_1 ? 1'h0 : _GEN_362; // @[PTW.scala 383:30 PTW.scala 387:13]
  wire [1:0] _GEN_372 = _r_pte_T_1 ? 2'h2 : _GEN_359; // @[PTW.scala 383:30 PTW.scala 388:11]
  wire  ae = res_v & invalid_paddr; // @[PTW.scala 397:22]
  wire  _GEN_373 = _GEN_320 | _GEN_369; // @[PTW.scala 403:32 PTW.scala 403:32]
  wire  _GEN_374 = r_req_dest | _GEN_370; // @[PTW.scala 403:32 PTW.scala 403:32]
  wire [2:0] _GEN_378 = traverse ? 3'h1 : 3'h0; // @[PTW.scala 392:21 PTW.scala 393:18]
  wire  _GEN_380 = traverse ? 1'h0 : res_v & _traverse_T_6 & _pte_addr_vpn_idx_T_2; // @[PTW.scala 392:21 PTW.scala 203:26 PTW.scala 396:17]
  wire [2:0] _GEN_384 = mem_resp_valid ? _GEN_378 : _GEN_368; // @[PTW.scala 390:25]
  RHEA__Arbiter arb ( // @[PTW.scala 112:19]
    .io_in_0_ready(arb_io_in_0_ready),
    .io_in_0_valid(arb_io_in_0_valid),
    .io_in_0_bits_bits_addr(arb_io_in_0_bits_bits_addr),
    .io_in_1_ready(arb_io_in_1_ready),
    .io_in_1_valid(arb_io_in_1_valid),
    .io_in_1_bits_valid(arb_io_in_1_bits_valid),
    .io_in_1_bits_bits_addr(arb_io_in_1_bits_bits_addr),
    .io_out_ready(arb_io_out_ready),
    .io_out_valid(arb_io_out_valid),
    .io_out_bits_valid(arb_io_out_bits_valid),
    .io_out_bits_bits_addr(arb_io_out_bits_bits_addr),
    .io_chosen(arb_io_chosen)
  );
  EICG_wrapper gated_clock_ptw_clock_gate ( // @[ClockGate.scala 24:20]
    .in(gated_clock_ptw_clock_gate_in),
    .test_en(gated_clock_ptw_clock_gate_test_en),
    .en(gated_clock_ptw_clock_gate_en),
    .out(gated_clock_ptw_clock_gate_out)
  );
  RHEA__l2_tlb_ram l2_tlb_ram ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(l2_tlb_ram_RW0_addr),
    .RW0_en(l2_tlb_ram_RW0_en),
    .RW0_clk(l2_tlb_ram_RW0_clk),
    .RW0_wmode(l2_tlb_ram_RW0_wmode),
    .RW0_wdata_0(l2_tlb_ram_RW0_wdata_0),
    .RW0_rdata_0(l2_tlb_ram_RW0_rdata_0)
  );
  RHEA__OptimizationBarrier_117 state_barrier ( // @[package.scala 271:25]
    .io_x(state_barrier_io_x),
    .io_y(state_barrier_io_y)
  );
  RHEA__OptimizationBarrier_118 r_pte_barrier ( // @[package.scala 271:25]
    .io_x_ppn(r_pte_barrier_io_x_ppn),
    .io_x_d(r_pte_barrier_io_x_d),
    .io_x_a(r_pte_barrier_io_x_a),
    .io_x_g(r_pte_barrier_io_x_g),
    .io_x_u(r_pte_barrier_io_x_u),
    .io_x_x(r_pte_barrier_io_x_x),
    .io_x_w(r_pte_barrier_io_x_w),
    .io_x_r(r_pte_barrier_io_x_r),
    .io_x_v(r_pte_barrier_io_x_v),
    .io_y_ppn(r_pte_barrier_io_y_ppn),
    .io_y_d(r_pte_barrier_io_y_d),
    .io_y_a(r_pte_barrier_io_y_a),
    .io_y_g(r_pte_barrier_io_y_g),
    .io_y_u(r_pte_barrier_io_y_u),
    .io_y_x(r_pte_barrier_io_y_x),
    .io_y_w(r_pte_barrier_io_y_w),
    .io_y_r(r_pte_barrier_io_y_r),
    .io_y_v(r_pte_barrier_io_y_v)
  );
  assign io_requestor_0_req_ready = arb_io_in_0_ready; // @[PTW.scala 113:13]
  assign io_requestor_0_resp_valid = resp_valid_0; // @[PTW.scala 315:32]
  assign io_requestor_0_resp_bits_ae = resp_ae; // @[PTW.scala 316:34]
  assign io_requestor_0_resp_bits_pte_ppn = r_pte_ppn; // @[PTW.scala 317:35]
  assign io_requestor_0_resp_bits_pte_d = r_pte_d; // @[PTW.scala 317:35]
  assign io_requestor_0_resp_bits_pte_a = r_pte_a; // @[PTW.scala 317:35]
  assign io_requestor_0_resp_bits_pte_g = r_pte_g; // @[PTW.scala 317:35]
  assign io_requestor_0_resp_bits_pte_u = r_pte_u; // @[PTW.scala 317:35]
  assign io_requestor_0_resp_bits_pte_x = r_pte_x; // @[PTW.scala 317:35]
  assign io_requestor_0_resp_bits_pte_w = r_pte_w; // @[PTW.scala 317:35]
  assign io_requestor_0_resp_bits_pte_r = r_pte_r; // @[PTW.scala 317:35]
  assign io_requestor_0_resp_bits_pte_v = r_pte_v; // @[PTW.scala 317:35]
  assign io_requestor_0_resp_bits_level = count; // @[PTW.scala 318:37]
  assign io_requestor_0_resp_bits_homogeneous = pmaHomogeneous & pmpHomogeneous; // @[PTW.scala 312:36]
  assign io_requestor_0_ptbr_mode = io_dpath_ptbr_mode; // @[PTW.scala 321:26]
  assign io_requestor_0_status_debug = io_dpath_status_debug; // @[PTW.scala 323:28]
  assign io_requestor_0_status_dprv = io_dpath_status_dprv; // @[PTW.scala 323:28]
  assign io_requestor_0_status_mxr = io_dpath_status_mxr; // @[PTW.scala 323:28]
  assign io_requestor_0_status_sum = io_dpath_status_sum; // @[PTW.scala 323:28]
  assign io_requestor_0_pmp_0_cfg_l = io_dpath_pmp_0_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_0_cfg_a = io_dpath_pmp_0_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_0_cfg_x = io_dpath_pmp_0_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_0_cfg_w = io_dpath_pmp_0_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_0_cfg_r = io_dpath_pmp_0_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_0_addr = io_dpath_pmp_0_addr; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_0_mask = io_dpath_pmp_0_mask; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_1_cfg_l = io_dpath_pmp_1_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_1_cfg_a = io_dpath_pmp_1_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_1_cfg_x = io_dpath_pmp_1_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_1_cfg_w = io_dpath_pmp_1_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_1_cfg_r = io_dpath_pmp_1_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_1_addr = io_dpath_pmp_1_addr; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_1_mask = io_dpath_pmp_1_mask; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_2_cfg_l = io_dpath_pmp_2_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_2_cfg_a = io_dpath_pmp_2_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_2_cfg_x = io_dpath_pmp_2_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_2_cfg_w = io_dpath_pmp_2_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_2_cfg_r = io_dpath_pmp_2_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_2_addr = io_dpath_pmp_2_addr; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_2_mask = io_dpath_pmp_2_mask; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_3_cfg_l = io_dpath_pmp_3_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_3_cfg_a = io_dpath_pmp_3_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_3_cfg_x = io_dpath_pmp_3_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_3_cfg_w = io_dpath_pmp_3_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_3_cfg_r = io_dpath_pmp_3_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_3_addr = io_dpath_pmp_3_addr; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_3_mask = io_dpath_pmp_3_mask; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_4_cfg_l = io_dpath_pmp_4_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_4_cfg_a = io_dpath_pmp_4_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_4_cfg_x = io_dpath_pmp_4_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_4_cfg_w = io_dpath_pmp_4_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_4_cfg_r = io_dpath_pmp_4_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_4_addr = io_dpath_pmp_4_addr; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_4_mask = io_dpath_pmp_4_mask; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_5_cfg_l = io_dpath_pmp_5_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_5_cfg_a = io_dpath_pmp_5_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_5_cfg_x = io_dpath_pmp_5_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_5_cfg_w = io_dpath_pmp_5_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_5_cfg_r = io_dpath_pmp_5_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_5_addr = io_dpath_pmp_5_addr; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_5_mask = io_dpath_pmp_5_mask; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_6_cfg_l = io_dpath_pmp_6_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_6_cfg_a = io_dpath_pmp_6_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_6_cfg_x = io_dpath_pmp_6_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_6_cfg_w = io_dpath_pmp_6_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_6_cfg_r = io_dpath_pmp_6_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_6_addr = io_dpath_pmp_6_addr; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_6_mask = io_dpath_pmp_6_mask; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_7_cfg_l = io_dpath_pmp_7_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_7_cfg_a = io_dpath_pmp_7_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_7_cfg_x = io_dpath_pmp_7_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_7_cfg_w = io_dpath_pmp_7_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_7_cfg_r = io_dpath_pmp_7_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_7_addr = io_dpath_pmp_7_addr; // @[PTW.scala 324:25]
  assign io_requestor_0_pmp_7_mask = io_dpath_pmp_7_mask; // @[PTW.scala 324:25]
  assign io_requestor_0_customCSRs_csrs_1_value = io_dpath_customCSRs_csrs_1_value; // @[PTW.scala 322:32]
  assign io_requestor_1_req_ready = arb_io_in_1_ready; // @[PTW.scala 113:13]
  assign io_requestor_1_resp_valid = resp_valid_1; // @[PTW.scala 315:32]
  assign io_requestor_1_resp_bits_ae = resp_ae; // @[PTW.scala 316:34]
  assign io_requestor_1_resp_bits_pte_ppn = r_pte_ppn; // @[PTW.scala 317:35]
  assign io_requestor_1_resp_bits_pte_d = r_pte_d; // @[PTW.scala 317:35]
  assign io_requestor_1_resp_bits_pte_a = r_pte_a; // @[PTW.scala 317:35]
  assign io_requestor_1_resp_bits_pte_g = r_pte_g; // @[PTW.scala 317:35]
  assign io_requestor_1_resp_bits_pte_u = r_pte_u; // @[PTW.scala 317:35]
  assign io_requestor_1_resp_bits_pte_x = r_pte_x; // @[PTW.scala 317:35]
  assign io_requestor_1_resp_bits_pte_w = r_pte_w; // @[PTW.scala 317:35]
  assign io_requestor_1_resp_bits_pte_r = r_pte_r; // @[PTW.scala 317:35]
  assign io_requestor_1_resp_bits_pte_v = r_pte_v; // @[PTW.scala 317:35]
  assign io_requestor_1_resp_bits_level = count; // @[PTW.scala 318:37]
  assign io_requestor_1_resp_bits_homogeneous = pmaHomogeneous & pmpHomogeneous; // @[PTW.scala 312:36]
  assign io_requestor_1_ptbr_mode = io_dpath_ptbr_mode; // @[PTW.scala 321:26]
  assign io_requestor_1_status_debug = io_dpath_status_debug; // @[PTW.scala 323:28]
  assign io_requestor_1_status_prv = io_dpath_status_prv; // @[PTW.scala 323:28]
  assign io_requestor_1_pmp_0_cfg_l = io_dpath_pmp_0_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_0_cfg_a = io_dpath_pmp_0_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_0_cfg_x = io_dpath_pmp_0_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_0_cfg_w = io_dpath_pmp_0_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_0_cfg_r = io_dpath_pmp_0_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_0_addr = io_dpath_pmp_0_addr; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_0_mask = io_dpath_pmp_0_mask; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_1_cfg_l = io_dpath_pmp_1_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_1_cfg_a = io_dpath_pmp_1_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_1_cfg_x = io_dpath_pmp_1_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_1_cfg_w = io_dpath_pmp_1_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_1_cfg_r = io_dpath_pmp_1_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_1_addr = io_dpath_pmp_1_addr; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_1_mask = io_dpath_pmp_1_mask; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_2_cfg_l = io_dpath_pmp_2_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_2_cfg_a = io_dpath_pmp_2_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_2_cfg_x = io_dpath_pmp_2_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_2_cfg_w = io_dpath_pmp_2_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_2_cfg_r = io_dpath_pmp_2_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_2_addr = io_dpath_pmp_2_addr; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_2_mask = io_dpath_pmp_2_mask; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_3_cfg_l = io_dpath_pmp_3_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_3_cfg_a = io_dpath_pmp_3_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_3_cfg_x = io_dpath_pmp_3_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_3_cfg_w = io_dpath_pmp_3_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_3_cfg_r = io_dpath_pmp_3_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_3_addr = io_dpath_pmp_3_addr; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_3_mask = io_dpath_pmp_3_mask; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_4_cfg_l = io_dpath_pmp_4_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_4_cfg_a = io_dpath_pmp_4_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_4_cfg_x = io_dpath_pmp_4_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_4_cfg_w = io_dpath_pmp_4_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_4_cfg_r = io_dpath_pmp_4_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_4_addr = io_dpath_pmp_4_addr; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_4_mask = io_dpath_pmp_4_mask; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_5_cfg_l = io_dpath_pmp_5_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_5_cfg_a = io_dpath_pmp_5_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_5_cfg_x = io_dpath_pmp_5_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_5_cfg_w = io_dpath_pmp_5_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_5_cfg_r = io_dpath_pmp_5_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_5_addr = io_dpath_pmp_5_addr; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_5_mask = io_dpath_pmp_5_mask; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_6_cfg_l = io_dpath_pmp_6_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_6_cfg_a = io_dpath_pmp_6_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_6_cfg_x = io_dpath_pmp_6_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_6_cfg_w = io_dpath_pmp_6_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_6_cfg_r = io_dpath_pmp_6_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_6_addr = io_dpath_pmp_6_addr; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_6_mask = io_dpath_pmp_6_mask; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_7_cfg_l = io_dpath_pmp_7_cfg_l; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_7_cfg_a = io_dpath_pmp_7_cfg_a; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_7_cfg_x = io_dpath_pmp_7_cfg_x; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_7_cfg_w = io_dpath_pmp_7_cfg_w; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_7_cfg_r = io_dpath_pmp_7_cfg_r; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_7_addr = io_dpath_pmp_7_addr; // @[PTW.scala 324:25]
  assign io_requestor_1_pmp_7_mask = io_dpath_pmp_7_mask; // @[PTW.scala 324:25]
  assign io_requestor_1_customCSRs_csrs_0_wen = io_dpath_customCSRs_csrs_0_wen; // @[PTW.scala 322:32]
  assign io_requestor_1_customCSRs_csrs_0_value = io_dpath_customCSRs_csrs_0_value; // @[PTW.scala 322:32]
  assign io_requestor_1_customCSRs_csrs_1_value = io_dpath_customCSRs_csrs_1_value; // @[PTW.scala 322:32]
  assign io_mem_req_valid = _T_55 | state == 3'h3; // @[PTW.scala 289:39]
  assign io_mem_req_bits_addr = pte_addr[39:0]; // @[PTW.scala 294:24]
  assign io_mem_req_bits_idx = pte_addr[39:0]; // @[PTW.scala 295:33]
  assign io_mem_s1_kill = s2_hit | state != 3'h2; // @[PTW.scala 297:28]
  assign io_dpath_perf_l2miss = s2_valid & ~s2_hit_vec_0; // @[PTW.scala 266:38]
  assign io_dpath_perf_l2hit = s2_valid & s2_hit_vec_0; // @[PTW.scala 265:27]
  assign io_dpath_perf_pte_miss = _T_155 ? 1'h0 : _GEN_353; // @[Conditional.scala 40:58 PTW.scala 198:26]
  assign io_dpath_perf_pte_hit = pte_hit & _T_55 & ~io_dpath_perf_l2hit; // @[PTW.scala 199:57]
  assign io_dpath_clock_enabled = state != 3'h0 | l2_refill | arb_io_out_valid | io_dpath_sfence_valid |
    io_dpath_customCSRs_csrs_1_value[0]; // @[PTW.scala 118:99]
  assign arb_io_in_0_valid = io_requestor_0_req_valid; // @[PTW.scala 113:13]
  assign arb_io_in_0_bits_bits_addr = io_requestor_0_req_bits_bits_addr; // @[PTW.scala 113:13]
  assign arb_io_in_1_valid = io_requestor_1_req_valid; // @[PTW.scala 113:13]
  assign arb_io_in_1_bits_valid = io_requestor_1_req_bits_valid; // @[PTW.scala 113:13]
  assign arb_io_in_1_bits_bits_addr = io_requestor_1_req_bits_bits_addr; // @[PTW.scala 113:13]
  assign arb_io_out_ready = state == 3'h0 & ~l2_refill; // @[PTW.scala 114:43]
  assign gated_clock_ptw_clock_gate_in = clock; // @[ClockGate.scala 26:14]
  assign gated_clock_ptw_clock_gate_test_en = psd_test_clock_enable;
  assign gated_clock_ptw_clock_gate_en = state != 3'h0 | l2_refill | arb_io_out_valid | io_dpath_sfence_valid |
    io_dpath_customCSRs_csrs_1_value[0]; // @[PTW.scala 118:99]
  assign l2_tlb_ram_RW0_wdata_0 = {hi_9,lo_9}; // @[Cat.scala 30:58]
  assign state_barrier_io_x = io_mem_s2_nack ? 3'h1 : _GEN_384; // @[PTW.scala 407:25 PTW.scala 409:16]
  assign r_pte_barrier_io_x_ppn = mem_resp_valid ? res_ppn : _r_pte_T_11_ppn; // @[PTW.scala 376:8]
  assign r_pte_barrier_io_x_d = mem_resp_valid ? tmp_d : _r_pte_T_11_d; // @[PTW.scala 376:8]
  assign r_pte_barrier_io_x_a = mem_resp_valid ? tmp_a : _r_pte_T_11_a; // @[PTW.scala 376:8]
  assign r_pte_barrier_io_x_g = mem_resp_valid ? tmp_g : _r_pte_T_11_g; // @[PTW.scala 376:8]
  assign r_pte_barrier_io_x_u = mem_resp_valid ? tmp_u : _r_pte_T_11_u; // @[PTW.scala 376:8]
  assign r_pte_barrier_io_x_x = mem_resp_valid ? tmp_x : _r_pte_T_11_x; // @[PTW.scala 376:8]
  assign r_pte_barrier_io_x_w = mem_resp_valid ? tmp_w : _r_pte_T_11_w; // @[PTW.scala 376:8]
  assign r_pte_barrier_io_x_r = mem_resp_valid ? tmp_r : _r_pte_T_11_r; // @[PTW.scala 376:8]
  assign r_pte_barrier_io_x_v = mem_resp_valid ? res_v : _r_pte_T_11_v; // @[PTW.scala 376:8]
  assign l2_tlb_ram_RW0_wmode = l2_refill;
  assign l2_tlb_ram_RW0_clk = gated_clock_ptw_clock_gate_out;
  assign l2_tlb_ram_RW0_en = s0_valid | _T_111;
  assign l2_tlb_ram_RW0_addr = _T_111 ? r_idx : arb_io_out_bits_bits_addr[6:0];
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      state <= 3'h0;
    end else if (reset) begin
      state <= 3'h0;
    end else begin
      state <= state_barrier_io_y;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      l2_refill <= 1'h0;
    end else begin
      l2_refill <= mem_resp_valid & _GEN_380;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      resp_valid_0 <= 1'h0;
    end else if (mem_resp_valid) begin
      if (traverse) begin
        resp_valid_0 <= _GEN_369;
      end else begin
        resp_valid_0 <= _GEN_373;
      end
    end else begin
      resp_valid_0 <= _GEN_369;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      resp_valid_1 <= 1'h0;
    end else if (mem_resp_valid) begin
      if (traverse) begin
        resp_valid_1 <= _GEN_370;
      end else begin
        resp_valid_1 <= _GEN_374;
      end
    end else begin
      resp_valid_1 <= _GEN_370;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      invalidated <= 1'h0;
    end else begin
      invalidated <= io_dpath_sfence_valid | invalidated & _clock_en_T;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      count <= 2'h0;
    end else if (mem_resp_valid) begin
      if (traverse) begin
        count <= _count_T_3;
      end else begin
        count <= _GEN_372;
      end
    end else begin
      count <= _GEN_372;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      resp_ae <= 1'h0;
    end else if (mem_resp_valid) begin
      if (traverse) begin
        resp_ae <= _GEN_371;
      end else begin
        resp_ae <= ae;
      end
    end else begin
      resp_ae <= _GEN_371;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      r_req_addr <= 27'h0;
    end else if (_T_22) begin
      r_req_addr <= arb_io_out_bits_bits_addr;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      r_req_dest <= 1'h0;
    end else if (_T_22) begin
      r_req_dest <= arb_io_chosen;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      r_pte_ppn <= 54'h0;
    end else begin
      r_pte_ppn <= r_pte_barrier_io_y_ppn;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      r_pte_d <= 1'h0;
    end else begin
      r_pte_d <= r_pte_barrier_io_y_d;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      r_pte_a <= 1'h0;
    end else begin
      r_pte_a <= r_pte_barrier_io_y_a;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      r_pte_g <= 1'h0;
    end else begin
      r_pte_g <= r_pte_barrier_io_y_g;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      r_pte_u <= 1'h0;
    end else begin
      r_pte_u <= r_pte_barrier_io_y_u;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      r_pte_x <= 1'h0;
    end else begin
      r_pte_x <= r_pte_barrier_io_y_x;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      r_pte_w <= 1'h0;
    end else begin
      r_pte_w <= r_pte_barrier_io_y_w;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      r_pte_r <= 1'h0;
    end else begin
      r_pte_r <= r_pte_barrier_io_y_r;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      r_pte_v <= 1'h0;
    end else begin
      r_pte_v <= r_pte_barrier_io_y_v;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_resp_valid <= 1'h0;
    end else begin
      mem_resp_valid <= io_mem_resp_valid;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      mem_resp_data <= 64'h0;
    end else begin
      mem_resp_data <= io_mem_resp_bits_data;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      state_reg <= 7'h0;
    end else if (reset) begin
      state_reg <= 7'h0;
    end else if (_T_56) begin
      state_reg <= _state_reg_T_16;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      valid <= 8'h0;
    end else if (reset) begin
      valid <= 8'h0;
    end else if (io_dpath_sfence_valid & ~io_dpath_sfence_bits_rs1) begin
      valid <= 8'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      valid <= _T_54;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      tags_0 <= 37'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h0 == r) begin
        tags_0 <= pte_addr[36:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      tags_1 <= 37'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h1 == r) begin
        tags_1 <= pte_addr[36:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      tags_2 <= 37'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h2 == r) begin
        tags_2 <= pte_addr[36:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      tags_3 <= 37'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h3 == r) begin
        tags_3 <= pte_addr[36:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      tags_4 <= 37'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h4 == r) begin
        tags_4 <= pte_addr[36:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      tags_5 <= 37'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h5 == r) begin
        tags_5 <= pte_addr[36:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      tags_6 <= 37'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h6 == r) begin
        tags_6 <= pte_addr[36:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      tags_7 <= 37'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h7 == r) begin
        tags_7 <= pte_addr[36:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      data_0 <= 25'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h0 == r) begin
        data_0 <= res_ppn[24:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      data_1 <= 25'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h1 == r) begin
        data_1 <= res_ppn[24:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      data_2 <= 25'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h2 == r) begin
        data_2 <= res_ppn[24:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      data_3 <= 25'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h3 == r) begin
        data_3 <= res_ppn[24:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      data_4 <= 25'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h4 == r) begin
        data_4 <= res_ppn[24:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      data_5 <= 25'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h5 == r) begin
        data_5 <= res_ppn[24:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      data_6 <= 25'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h6 == r) begin
        data_6 <= res_ppn[24:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      data_7 <= 25'h0;
    end else if (mem_resp_valid & traverse & ~hit & ~invalidated) begin
      if (3'h7 == r) begin
        data_7 <= res_ppn[24:0];
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      pte_hit <= 1'h0;
    end else if (_T_155) begin
      pte_hit <= 1'h0;
    end else begin
      pte_hit <= _GEN_351;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      g_0 <= 128'h0;
    end else if (l2_refill & _T_27) begin
      if (r_pte_g) begin
        g_0 <= _T_115;
      end else begin
        g_0 <= _T_117;
      end
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      valid_1_0 <= 128'h0;
    end else if (reset) begin
      valid_1_0 <= 128'h0;
    end else if (s2_valid & s2_error) begin
      valid_1_0 <= 128'h0;
    end else if (io_dpath_sfence_valid) begin
      if (io_dpath_sfence_bits_rs1) begin
        valid_1_0 <= _T_122;
      end else begin
        valid_1_0 <= _T_124;
      end
    end else if (l2_refill & _T_27) begin
      valid_1_0 <= _T_114;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      s1_valid <= 1'h0;
    end else begin
      s1_valid <= s0_valid & arb_io_out_bits_valid;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      s2_valid <= 1'h0;
    end else begin
      s2_valid <= s1_valid;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      r_1 <= 52'h0;
    end else if (s1_valid) begin
      r_1 <= l2_tlb_ram_RW0_rdata_0;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      s2_valid_vec <= 1'h0;
    end else if (s1_valid) begin
      s2_valid_vec <= r_valid_vec;
    end
  end
  always @(posedge gated_clock_ptw_clock_gate_out or posedge rf_reset) begin
    if (rf_reset) begin
      s2_g_vec_0 <= 1'h0;
    end else if (s1_valid) begin
      s2_g_vec_0 <= _T_132[0];
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  state = _RAND_0[2:0];
  _RAND_1 = {1{`RANDOM}};
  l2_refill = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  resp_valid_0 = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  resp_valid_1 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  invalidated = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  count = _RAND_5[1:0];
  _RAND_6 = {1{`RANDOM}};
  resp_ae = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  r_req_addr = _RAND_7[26:0];
  _RAND_8 = {1{`RANDOM}};
  r_req_dest = _RAND_8[0:0];
  _RAND_9 = {2{`RANDOM}};
  r_pte_ppn = _RAND_9[53:0];
  _RAND_10 = {1{`RANDOM}};
  r_pte_d = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  r_pte_a = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  r_pte_g = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  r_pte_u = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  r_pte_x = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  r_pte_w = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  r_pte_r = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  r_pte_v = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  mem_resp_valid = _RAND_18[0:0];
  _RAND_19 = {2{`RANDOM}};
  mem_resp_data = _RAND_19[63:0];
  _RAND_20 = {1{`RANDOM}};
  state_reg = _RAND_20[6:0];
  _RAND_21 = {1{`RANDOM}};
  valid = _RAND_21[7:0];
  _RAND_22 = {2{`RANDOM}};
  tags_0 = _RAND_22[36:0];
  _RAND_23 = {2{`RANDOM}};
  tags_1 = _RAND_23[36:0];
  _RAND_24 = {2{`RANDOM}};
  tags_2 = _RAND_24[36:0];
  _RAND_25 = {2{`RANDOM}};
  tags_3 = _RAND_25[36:0];
  _RAND_26 = {2{`RANDOM}};
  tags_4 = _RAND_26[36:0];
  _RAND_27 = {2{`RANDOM}};
  tags_5 = _RAND_27[36:0];
  _RAND_28 = {2{`RANDOM}};
  tags_6 = _RAND_28[36:0];
  _RAND_29 = {2{`RANDOM}};
  tags_7 = _RAND_29[36:0];
  _RAND_30 = {1{`RANDOM}};
  data_0 = _RAND_30[24:0];
  _RAND_31 = {1{`RANDOM}};
  data_1 = _RAND_31[24:0];
  _RAND_32 = {1{`RANDOM}};
  data_2 = _RAND_32[24:0];
  _RAND_33 = {1{`RANDOM}};
  data_3 = _RAND_33[24:0];
  _RAND_34 = {1{`RANDOM}};
  data_4 = _RAND_34[24:0];
  _RAND_35 = {1{`RANDOM}};
  data_5 = _RAND_35[24:0];
  _RAND_36 = {1{`RANDOM}};
  data_6 = _RAND_36[24:0];
  _RAND_37 = {1{`RANDOM}};
  data_7 = _RAND_37[24:0];
  _RAND_38 = {1{`RANDOM}};
  pte_hit = _RAND_38[0:0];
  _RAND_39 = {4{`RANDOM}};
  g_0 = _RAND_39[127:0];
  _RAND_40 = {4{`RANDOM}};
  valid_1_0 = _RAND_40[127:0];
  _RAND_41 = {1{`RANDOM}};
  s1_valid = _RAND_41[0:0];
  _RAND_42 = {1{`RANDOM}};
  s2_valid = _RAND_42[0:0];
  _RAND_43 = {2{`RANDOM}};
  r_1 = _RAND_43[51:0];
  _RAND_44 = {1{`RANDOM}};
  s2_valid_vec = _RAND_44[0:0];
  _RAND_45 = {1{`RANDOM}};
  s2_g_vec_0 = _RAND_45[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    state = 3'h0;
  end
  if (rf_reset) begin
    l2_refill = 1'h0;
  end
  if (rf_reset) begin
    resp_valid_0 = 1'h0;
  end
  if (rf_reset) begin
    resp_valid_1 = 1'h0;
  end
  if (rf_reset) begin
    invalidated = 1'h0;
  end
  if (rf_reset) begin
    count = 2'h0;
  end
  if (rf_reset) begin
    resp_ae = 1'h0;
  end
  if (rf_reset) begin
    r_req_addr = 27'h0;
  end
  if (rf_reset) begin
    r_req_dest = 1'h0;
  end
  if (rf_reset) begin
    r_pte_ppn = 54'h0;
  end
  if (rf_reset) begin
    r_pte_d = 1'h0;
  end
  if (rf_reset) begin
    r_pte_a = 1'h0;
  end
  if (rf_reset) begin
    r_pte_g = 1'h0;
  end
  if (rf_reset) begin
    r_pte_u = 1'h0;
  end
  if (rf_reset) begin
    r_pte_x = 1'h0;
  end
  if (rf_reset) begin
    r_pte_w = 1'h0;
  end
  if (rf_reset) begin
    r_pte_r = 1'h0;
  end
  if (rf_reset) begin
    r_pte_v = 1'h0;
  end
  if (rf_reset) begin
    mem_resp_valid = 1'h0;
  end
  if (rf_reset) begin
    mem_resp_data = 64'h0;
  end
  if (rf_reset) begin
    state_reg = 7'h0;
  end
  if (rf_reset) begin
    valid = 8'h0;
  end
  if (rf_reset) begin
    tags_0 = 37'h0;
  end
  if (rf_reset) begin
    tags_1 = 37'h0;
  end
  if (rf_reset) begin
    tags_2 = 37'h0;
  end
  if (rf_reset) begin
    tags_3 = 37'h0;
  end
  if (rf_reset) begin
    tags_4 = 37'h0;
  end
  if (rf_reset) begin
    tags_5 = 37'h0;
  end
  if (rf_reset) begin
    tags_6 = 37'h0;
  end
  if (rf_reset) begin
    tags_7 = 37'h0;
  end
  if (rf_reset) begin
    data_0 = 25'h0;
  end
  if (rf_reset) begin
    data_1 = 25'h0;
  end
  if (rf_reset) begin
    data_2 = 25'h0;
  end
  if (rf_reset) begin
    data_3 = 25'h0;
  end
  if (rf_reset) begin
    data_4 = 25'h0;
  end
  if (rf_reset) begin
    data_5 = 25'h0;
  end
  if (rf_reset) begin
    data_6 = 25'h0;
  end
  if (rf_reset) begin
    data_7 = 25'h0;
  end
  if (rf_reset) begin
    pte_hit = 1'h0;
  end
  if (rf_reset) begin
    g_0 = 128'h0;
  end
  if (rf_reset) begin
    valid_1_0 = 128'h0;
  end
  if (rf_reset) begin
    s1_valid = 1'h0;
  end
  if (rf_reset) begin
    s2_valid = 1'h0;
  end
  if (rf_reset) begin
    r_1 = 52'h0;
  end
  if (rf_reset) begin
    s2_valid_vec = 1'h0;
  end
  if (rf_reset) begin
    s2_g_vec_0 = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
