//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TraceSRAM_0(
  input  [5:0] RW0_addr,
  input        RW0_en,
  input        RW0_clk,
  input        RW0_wmode,
  input  [7:0] RW0_wdata_0,
  input  [7:0] RW0_wdata_1,
  input  [7:0] RW0_wdata_2,
  input  [7:0] RW0_wdata_3,
  output [7:0] RW0_rdata_0,
  output [7:0] RW0_rdata_1,
  output [7:0] RW0_rdata_2,
  output [7:0] RW0_rdata_3
);
  wire [5:0] TraceSRAM_0_ext_RW0_addr;
  wire  TraceSRAM_0_ext_RW0_en;
  wire  TraceSRAM_0_ext_RW0_clk;
  wire  TraceSRAM_0_ext_RW0_wmode;
  wire [31:0] TraceSRAM_0_ext_RW0_wdata;
  wire [31:0] TraceSRAM_0_ext_RW0_rdata;
  wire [3:0] TraceSRAM_0_ext_RW0_wmask;
  wire [15:0] _GEN_0 = {RW0_wdata_3,RW0_wdata_2};
  wire [15:0] _GEN_1 = {RW0_wdata_1,RW0_wdata_0};
  RHEA__TraceSRAM_0_ext TraceSRAM_0_ext (
    .RW0_addr(TraceSRAM_0_ext_RW0_addr),
    .RW0_en(TraceSRAM_0_ext_RW0_en),
    .RW0_clk(TraceSRAM_0_ext_RW0_clk),
    .RW0_wmode(TraceSRAM_0_ext_RW0_wmode),
    .RW0_wdata(TraceSRAM_0_ext_RW0_wdata),
    .RW0_rdata(TraceSRAM_0_ext_RW0_rdata),
    .RW0_wmask(TraceSRAM_0_ext_RW0_wmask)
  );
  assign TraceSRAM_0_ext_RW0_clk = RW0_clk;
  assign TraceSRAM_0_ext_RW0_en = RW0_en;
  assign TraceSRAM_0_ext_RW0_addr = RW0_addr;
  assign RW0_rdata_0 = TraceSRAM_0_ext_RW0_rdata[7:0];
  assign RW0_rdata_1 = TraceSRAM_0_ext_RW0_rdata[15:8];
  assign RW0_rdata_2 = TraceSRAM_0_ext_RW0_rdata[23:16];
  assign RW0_rdata_3 = TraceSRAM_0_ext_RW0_rdata[31:24];
  assign TraceSRAM_0_ext_RW0_wmode = RW0_wmode;
  assign TraceSRAM_0_ext_RW0_wdata = {_GEN_0,_GEN_1};
  assign TraceSRAM_0_ext_RW0_wmask = 4'hf;
endmodule
