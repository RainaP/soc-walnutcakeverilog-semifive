//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLTestIndicator(
  input         rf_reset,
  output        auto_control_xing_in_a_ready,
  input         auto_control_xing_in_a_valid,
  input  [2:0]  auto_control_xing_in_a_bits_opcode,
  input  [2:0]  auto_control_xing_in_a_bits_param,
  input  [1:0]  auto_control_xing_in_a_bits_size,
  input  [11:0] auto_control_xing_in_a_bits_source,
  input  [14:0] auto_control_xing_in_a_bits_address,
  input  [3:0]  auto_control_xing_in_a_bits_mask,
  input  [31:0] auto_control_xing_in_a_bits_data,
  input         auto_control_xing_in_a_bits_corrupt,
  input         auto_control_xing_in_d_ready,
  output        auto_control_xing_in_d_valid,
  output [2:0]  auto_control_xing_in_d_bits_opcode,
  output [1:0]  auto_control_xing_in_d_bits_size,
  output [11:0] auto_control_xing_in_d_bits_source,
  output [31:0] auto_control_xing_in_d_bits_data,
  input         clock,
  input         reset
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
`endif // RANDOMIZE_REG_INIT
  wire  buffer_auto_in_a_ready;
  wire  buffer_auto_in_a_valid;
  wire [2:0] buffer_auto_in_a_bits_opcode;
  wire [1:0] buffer_auto_in_a_bits_size;
  wire [11:0] buffer_auto_in_a_bits_source;
  wire [14:0] buffer_auto_in_a_bits_address;
  wire [3:0] buffer_auto_in_a_bits_mask;
  wire [31:0] buffer_auto_in_a_bits_data;
  wire  buffer_auto_in_d_ready;
  wire  buffer_auto_in_d_valid;
  wire [2:0] buffer_auto_in_d_bits_opcode;
  wire [1:0] buffer_auto_in_d_bits_size;
  wire [11:0] buffer_auto_in_d_bits_source;
  wire [31:0] buffer_auto_in_d_bits_data;
  wire  buffer_auto_out_a_ready;
  wire  buffer_auto_out_a_valid;
  wire [2:0] buffer_auto_out_a_bits_opcode;
  wire [1:0] buffer_auto_out_a_bits_size;
  wire [11:0] buffer_auto_out_a_bits_source;
  wire [14:0] buffer_auto_out_a_bits_address;
  wire [3:0] buffer_auto_out_a_bits_mask;
  wire [31:0] buffer_auto_out_a_bits_data;
  wire  buffer_auto_out_d_ready;
  wire  buffer_auto_out_d_valid;
  wire [2:0] buffer_auto_out_d_bits_opcode;
  wire [1:0] buffer_auto_out_d_bits_size;
  wire [11:0] buffer_auto_out_d_bits_source;
  wire [31:0] buffer_auto_out_d_bits_data;
  wire  out_back_rf_reset; // @[Decoupled.scala 296:21]
  wire  out_back_clock; // @[Decoupled.scala 296:21]
  wire  out_back_reset; // @[Decoupled.scala 296:21]
  wire  out_back_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  out_back_io_enq_valid; // @[Decoupled.scala 296:21]
  wire  out_back_io_enq_bits_read; // @[Decoupled.scala 296:21]
  wire [9:0] out_back_io_enq_bits_index; // @[Decoupled.scala 296:21]
  wire [31:0] out_back_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire [3:0] out_back_io_enq_bits_mask; // @[Decoupled.scala 296:21]
  wire [11:0] out_back_io_enq_bits_extra_tlrr_extra_source; // @[Decoupled.scala 296:21]
  wire [1:0] out_back_io_enq_bits_extra_tlrr_extra_size; // @[Decoupled.scala 296:21]
  wire  out_back_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  out_back_io_deq_valid; // @[Decoupled.scala 296:21]
  wire  out_back_io_deq_bits_read; // @[Decoupled.scala 296:21]
  wire [9:0] out_back_io_deq_bits_index; // @[Decoupled.scala 296:21]
  wire [31:0] out_back_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire [3:0] out_back_io_deq_bits_mask; // @[Decoupled.scala 296:21]
  wire [11:0] out_back_io_deq_bits_extra_tlrr_extra_source; // @[Decoupled.scala 296:21]
  wire [1:0] out_back_io_deq_bits_extra_tlrr_extra_size; // @[Decoupled.scala 296:21]
  reg [31:0] status_regs_0; // @[TestIndicator.scala 58:32]
  wire [15:0] hi = status_regs_0[31:16]; // @[TestIndicator.scala 70:67]
  wire [31:0] _T = {hi,hi}; // @[Cat.scala 30:58]
  wire [9:0] out_bindex = out_back_io_deq_bits_index & 10'h3bf; // @[RegisterRouter.scala 204:45]
  wire  _out_T_1 = out_bindex == 10'h0; // @[RegisterRouter.scala 204:45]
  wire [7:0] out_backMask_lo_lo = out_back_io_deq_bits_mask[0] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_lo_hi = out_back_io_deq_bits_mask[1] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_lo = out_back_io_deq_bits_mask[2] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] out_backMask_hi_hi = out_back_io_deq_bits_mask[3] ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [31:0] out_backMask = {out_backMask_hi_hi,out_backMask_hi_lo,out_backMask_lo_hi,out_backMask_lo_lo}; // @[Cat.scala 30:58]
  wire  out_womask = &out_backMask; // @[RegisterRouter.scala 204:45]
  wire  out_oindex = out_back_io_deq_bits_index[6]; // @[RegisterRouter.scala 204:45]
  wire [1:0] _out_backSel_T = 2'h1 << out_oindex; // @[OneHot.scala 58:35]
  wire  out_backSel_1 = _out_backSel_T[1]; // @[RegisterRouter.scala 204:45]
  wire  out_woready_0 = out_back_io_deq_valid & buffer_auto_out_d_ready & ~out_back_io_deq_bits_read & out_backSel_1 &
    out_bindex == 10'h0; // @[RegisterRouter.scala 204:45]
  wire  out_f_woready = out_woready_0 & out_womask; // @[RegisterRouter.scala 204:45]
  wire [31:0] _out_T_4 = out_back_io_deq_bits_data; // @[RegisterRouter.scala 204:45]
  wire [15:0] out_status_regs_0_hi = _out_T_4[31:16] | _out_T_4[15:0]; // @[TestIndicator.scala 67:56]
  wire [15:0] out_status_regs_0_lo = status_regs_0[15:0]; // @[TestIndicator.scala 67:87]
  wire [31:0] _out_status_regs_0_T_2 = {out_status_regs_0_hi,out_status_regs_0_lo}; // @[Cat.scala 30:58]
  wire  out_backSel_0 = _out_backSel_T[0]; // @[RegisterRouter.scala 204:45]
  wire  out_woready_1 = out_back_io_deq_valid & buffer_auto_out_d_ready & ~out_back_io_deq_bits_read & out_backSel_0 &
    out_bindex == 10'h0; // @[RegisterRouter.scala 204:45]
  wire  out_f_woready_1 = out_woready_1 & out_womask; // @[RegisterRouter.scala 204:45]
  wire  _GEN_11 = out_oindex ? _out_T_1 : _out_T_1; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire [31:0] _GEN_13 = out_oindex ? _T : status_regs_0; // @[MuxLiteral.scala 48:10 MuxLiteral.scala 48:10]
  wire  out_bits_read = out_back_io_deq_bits_read; // @[RegisterRouter.scala 204:45 RegisterRouter.scala 204:45]
  RHEA__Queue_172 out_back ( // @[Decoupled.scala 296:21]
    .rf_reset(out_back_rf_reset),
    .clock(out_back_clock),
    .reset(out_back_reset),
    .io_enq_ready(out_back_io_enq_ready),
    .io_enq_valid(out_back_io_enq_valid),
    .io_enq_bits_read(out_back_io_enq_bits_read),
    .io_enq_bits_index(out_back_io_enq_bits_index),
    .io_enq_bits_data(out_back_io_enq_bits_data),
    .io_enq_bits_mask(out_back_io_enq_bits_mask),
    .io_enq_bits_extra_tlrr_extra_source(out_back_io_enq_bits_extra_tlrr_extra_source),
    .io_enq_bits_extra_tlrr_extra_size(out_back_io_enq_bits_extra_tlrr_extra_size),
    .io_deq_ready(out_back_io_deq_ready),
    .io_deq_valid(out_back_io_deq_valid),
    .io_deq_bits_read(out_back_io_deq_bits_read),
    .io_deq_bits_index(out_back_io_deq_bits_index),
    .io_deq_bits_data(out_back_io_deq_bits_data),
    .io_deq_bits_mask(out_back_io_deq_bits_mask),
    .io_deq_bits_extra_tlrr_extra_source(out_back_io_deq_bits_extra_tlrr_extra_source),
    .io_deq_bits_extra_tlrr_extra_size(out_back_io_deq_bits_extra_tlrr_extra_size)
  );
  assign buffer_auto_in_a_ready = buffer_auto_out_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_valid = buffer_auto_out_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_opcode = buffer_auto_out_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_size = buffer_auto_out_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_source = buffer_auto_out_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_d_bits_data = buffer_auto_out_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_a_valid = buffer_auto_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_opcode = buffer_auto_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_size = buffer_auto_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_source = buffer_auto_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_address = buffer_auto_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_mask = buffer_auto_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_bits_data = buffer_auto_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_d_ready = buffer_auto_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign out_back_rf_reset = rf_reset;
  assign auto_control_xing_in_a_ready = buffer_auto_in_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_control_xing_in_d_valid = buffer_auto_in_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_control_xing_in_d_bits_opcode = buffer_auto_in_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_control_xing_in_d_bits_size = buffer_auto_in_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_control_xing_in_d_bits_source = buffer_auto_in_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_control_xing_in_d_bits_data = buffer_auto_in_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign buffer_auto_in_a_valid = auto_control_xing_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_opcode = auto_control_xing_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_size = auto_control_xing_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_source = auto_control_xing_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_address = auto_control_xing_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_mask = auto_control_xing_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_data = auto_control_xing_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_d_ready = auto_control_xing_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_a_ready = out_back_io_enq_ready; // @[RegisterRouter.scala 204:45 Decoupled.scala 299:17]
  assign buffer_auto_out_d_valid = out_back_io_deq_valid; // @[RegisterRouter.scala 204:45]
  assign buffer_auto_out_d_bits_opcode = {{2'd0}, out_bits_read}; // @[RegisterRouter.scala 204:45 RegisterRouter.scala 204:45]
  assign buffer_auto_out_d_bits_size = out_back_io_deq_bits_extra_tlrr_extra_size; // @[RegisterRouter.scala 204:45 RegisterRouter.scala 204:45]
  assign buffer_auto_out_d_bits_source = out_back_io_deq_bits_extra_tlrr_extra_source; // @[RegisterRouter.scala 204:45 RegisterRouter.scala 204:45]
  assign buffer_auto_out_d_bits_data = _GEN_11 ? _GEN_13 : 32'h0; // @[RegisterRouter.scala 204:45]
  assign out_back_clock = clock;
  assign out_back_reset = reset;
  assign out_back_io_enq_valid = buffer_auto_out_a_valid; // @[RegisterRouter.scala 204:45]
  assign out_back_io_enq_bits_read = buffer_auto_out_a_bits_opcode == 3'h4; // @[RegisterRouter.scala 204:45]
  assign out_back_io_enq_bits_index = buffer_auto_out_a_bits_address[11:2]; // @[RegisterRouter.scala 204:45 RegisterRouter.scala 204:45]
  assign out_back_io_enq_bits_data = buffer_auto_out_a_bits_data; // @[RegisterRouter.scala 204:45 LazyModule.scala 375:16]
  assign out_back_io_enq_bits_mask = buffer_auto_out_a_bits_mask; // @[RegisterRouter.scala 204:45 LazyModule.scala 375:16]
  assign out_back_io_enq_bits_extra_tlrr_extra_source = buffer_auto_out_a_bits_source; // @[RegisterRouter.scala 204:45 LazyModule.scala 375:16]
  assign out_back_io_enq_bits_extra_tlrr_extra_size = buffer_auto_out_a_bits_size; // @[RegisterRouter.scala 204:45 LazyModule.scala 375:16]
  assign out_back_io_deq_ready = buffer_auto_out_d_ready; // @[RegisterRouter.scala 204:45]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      status_regs_0 <= 32'h0;
    end else if (out_f_woready_1) begin
      status_regs_0 <= _out_T_4;
    end else if (out_f_woready) begin
      status_regs_0 <= _out_status_regs_0_T_2;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  status_regs_0 = _RAND_0[31:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    status_regs_0 = 32'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
