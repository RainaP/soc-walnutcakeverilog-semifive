//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_MemTap_0(
  output [64:0] mem_0,
  output [64:0] mem_1,
  output [64:0] mem_2,
  output [64:0] mem_3,
  output [64:0] mem_4,
  output [64:0] mem_5,
  output [64:0] mem_6,
  output [64:0] mem_7,
  output [64:0] mem_8,
  output [64:0] mem_9,
  output [64:0] mem_10,
  output [64:0] mem_11,
  output [64:0] mem_12,
  output [64:0] mem_13,
  output [64:0] mem_14,
  output [64:0] mem_15,
  output [64:0] mem_16,
  output [64:0] mem_17,
  output [64:0] mem_18,
  output [64:0] mem_19,
  output [64:0] mem_20,
  output [64:0] mem_21,
  output [64:0] mem_22,
  output [64:0] mem_23,
  output [64:0] mem_24,
  output [64:0] mem_25,
  output [64:0] mem_26,
  output [64:0] mem_27,
  output [64:0] mem_28,
  output [64:0] mem_29,
  output [64:0] mem_30,
  output [64:0] mem_31
);
  assign mem_0 = rocket.fpuOpt.regfile[0];
  assign mem_1 = rocket.fpuOpt.regfile[1];
  assign mem_2 = rocket.fpuOpt.regfile[2];
  assign mem_3 = rocket.fpuOpt.regfile[3];
  assign mem_4 = rocket.fpuOpt.regfile[4];
  assign mem_5 = rocket.fpuOpt.regfile[5];
  assign mem_6 = rocket.fpuOpt.regfile[6];
  assign mem_7 = rocket.fpuOpt.regfile[7];
  assign mem_8 = rocket.fpuOpt.regfile[8];
  assign mem_9 = rocket.fpuOpt.regfile[9];
  assign mem_10 = rocket.fpuOpt.regfile[10];
  assign mem_11 = rocket.fpuOpt.regfile[11];
  assign mem_12 = rocket.fpuOpt.regfile[12];
  assign mem_13 = rocket.fpuOpt.regfile[13];
  assign mem_14 = rocket.fpuOpt.regfile[14];
  assign mem_15 = rocket.fpuOpt.regfile[15];
  assign mem_16 = rocket.fpuOpt.regfile[16];
  assign mem_17 = rocket.fpuOpt.regfile[17];
  assign mem_18 = rocket.fpuOpt.regfile[18];
  assign mem_19 = rocket.fpuOpt.regfile[19];
  assign mem_20 = rocket.fpuOpt.regfile[20];
  assign mem_21 = rocket.fpuOpt.regfile[21];
  assign mem_22 = rocket.fpuOpt.regfile[22];
  assign mem_23 = rocket.fpuOpt.regfile[23];
  assign mem_24 = rocket.fpuOpt.regfile[24];
  assign mem_25 = rocket.fpuOpt.regfile[25];
  assign mem_26 = rocket.fpuOpt.regfile[26];
  assign mem_27 = rocket.fpuOpt.regfile[27];
  assign mem_28 = rocket.fpuOpt.regfile[28];
  assign mem_29 = rocket.fpuOpt.regfile[29];
  assign mem_30 = rocket.fpuOpt.regfile[30];
  assign mem_31 = rocket.fpuOpt.regfile[31];
endmodule
