//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__l2_tlb_ram(
  input  [6:0]  RW0_addr,
  input         RW0_en,
  input         RW0_clk,
  input         RW0_wmode,
  input  [51:0] RW0_wdata_0,
  output [51:0] RW0_rdata_0
);
  wire [6:0] l2_tlb_ram_ext_RW0_addr;
  wire  l2_tlb_ram_ext_RW0_en;
  wire  l2_tlb_ram_ext_RW0_clk;
  wire  l2_tlb_ram_ext_RW0_wmode;
  wire [51:0] l2_tlb_ram_ext_RW0_wdata;
  wire [51:0] l2_tlb_ram_ext_RW0_rdata;
  RHEA__l2_tlb_ram_ext l2_tlb_ram_ext (
    .RW0_addr(l2_tlb_ram_ext_RW0_addr),
    .RW0_en(l2_tlb_ram_ext_RW0_en),
    .RW0_clk(l2_tlb_ram_ext_RW0_clk),
    .RW0_wmode(l2_tlb_ram_ext_RW0_wmode),
    .RW0_wdata(l2_tlb_ram_ext_RW0_wdata),
    .RW0_rdata(l2_tlb_ram_ext_RW0_rdata)
  );
  assign l2_tlb_ram_ext_RW0_clk = RW0_clk;
  assign l2_tlb_ram_ext_RW0_en = RW0_en;
  assign l2_tlb_ram_ext_RW0_addr = RW0_addr;
  assign RW0_rdata_0 = l2_tlb_ram_ext_RW0_rdata;
  assign l2_tlb_ram_ext_RW0_wmode = RW0_wmode;
  assign l2_tlb_ram_ext_RW0_wdata = RW0_wdata_0;
endmodule
