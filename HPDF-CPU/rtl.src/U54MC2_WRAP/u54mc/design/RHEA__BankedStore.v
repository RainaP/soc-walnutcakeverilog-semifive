//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__BankedStore(
  input          rf_reset,
  input          clock,
  input          reset,
  output         io_side_adr_ready,
  input          io_side_adr_valid,
  input  [2:0]   io_side_adr_bits_way,
  input  [9:0]   io_side_adr_bits_set,
  input  [1:0]   io_side_adr_bits_beat,
  input  [1:0]   io_side_adr_bits_mask,
  input          io_side_adr_bits_write,
  input  [127:0] io_side_wdat_data,
  input          io_side_wdat_poison,
  output [127:0] io_side_rdat_data,
  output         io_sinkC_adr_ready,
  input          io_sinkC_adr_valid,
  input          io_sinkC_adr_bits_noop,
  input  [2:0]   io_sinkC_adr_bits_way,
  input  [9:0]   io_sinkC_adr_bits_set,
  input  [1:0]   io_sinkC_adr_bits_beat,
  input  [127:0] io_sinkC_dat_data,
  input          io_sinkC_dat_poison,
  output         io_sinkD_adr_ready,
  input          io_sinkD_adr_valid,
  input          io_sinkD_adr_bits_noop,
  input  [2:0]   io_sinkD_adr_bits_way,
  input  [9:0]   io_sinkD_adr_bits_set,
  input  [1:0]   io_sinkD_adr_bits_beat,
  input  [127:0] io_sinkD_dat_data,
  output         io_sourceC_adr_ready,
  input          io_sourceC_adr_valid,
  input  [2:0]   io_sourceC_adr_bits_way,
  input  [9:0]   io_sourceC_adr_bits_set,
  input  [1:0]   io_sourceC_adr_bits_beat,
  output [127:0] io_sourceC_dat_data,
  output         io_sourceD_radr_ready,
  input          io_sourceD_radr_valid,
  input  [2:0]   io_sourceD_radr_bits_way,
  input  [9:0]   io_sourceD_radr_bits_set,
  input  [1:0]   io_sourceD_radr_bits_beat,
  input  [1:0]   io_sourceD_radr_bits_mask,
  output [127:0] io_sourceD_rdat_data,
  output         io_sourceD_wadr_ready,
  input          io_sourceD_wadr_valid,
  input  [2:0]   io_sourceD_wadr_bits_way,
  input  [9:0]   io_sourceD_wadr_bits_set,
  input  [1:0]   io_sourceD_wadr_bits_beat,
  input  [1:0]   io_sourceD_wadr_bits_mask,
  input  [127:0] io_sourceD_wdat_data,
  input          io_sourceD_wdat_poison,
  output         io_error_ready,
  input          io_error_valid,
  input  [7:0]   io_error_bits_bit
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [63:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [63:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [63:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [63:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [63:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [63:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [63:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [63:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [63:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [63:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [63:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [63:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [63:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [63:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [63:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [63:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
`endif // RANDOMIZE_REG_INIT
  wire [11:0] cc_banks_0_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_0_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_0_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_0_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_0_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_0_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [11:0] cc_banks_1_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_1_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_1_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_1_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_1_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_1_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [11:0] cc_banks_2_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_2_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_2_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_2_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_2_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_2_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [11:0] cc_banks_3_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_3_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_3_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_3_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_3_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_3_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [11:0] cc_banks_4_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_4_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_4_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_4_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_4_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_4_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [11:0] cc_banks_5_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_5_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_5_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_5_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_5_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_5_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [11:0] cc_banks_6_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_6_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_6_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_6_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_6_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_6_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [11:0] cc_banks_7_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_7_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_7_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_7_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_7_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_7_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [11:0] cc_banks_8_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_8_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_8_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_8_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_8_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_8_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [11:0] cc_banks_9_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_9_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_9_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_9_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_9_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_9_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [11:0] cc_banks_10_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_10_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_10_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_10_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_10_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_10_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [11:0] cc_banks_11_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_11_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_11_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_11_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_11_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_11_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [11:0] cc_banks_12_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_12_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_12_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_12_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_12_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_12_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [11:0] cc_banks_13_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_13_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_13_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_13_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_13_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_13_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [11:0] cc_banks_14_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_14_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_14_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_14_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_14_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_14_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [11:0] cc_banks_15_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_15_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_15_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  cc_banks_15_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_15_RW0_wdata; // @[DescribedSRAM.scala 18:26]
  wire [63:0] cc_banks_15_RW0_rdata; // @[DescribedSRAM.scala 18:26]
  wire [255:0] _error_T = 256'h1 << io_error_bits_bit; // @[BankedStore.scala 139:40]
  wire [63:0] error = io_error_valid ? _error_T[63:0] : 64'h0; // @[BankedStore.scala 139:18]
  wire [63:0] side_req_words_data = io_side_wdat_data[63:0]; // @[BankedStore.scala 147:19]
  wire [63:0] side_req_words_0 = side_req_words_data ^ error; // @[BankedStore.scala 149:66]
  wire [63:0] side_req_words_data_1 = io_side_wdat_data[127:64]; // @[BankedStore.scala 147:19]
  wire [63:0] side_req_words_1 = side_req_words_data_1 ^ error; // @[BankedStore.scala 149:66]
  wire [14:0] side_req_a = {io_side_adr_bits_way,io_side_adr_bits_set,io_side_adr_bits_beat}; // @[Cat.scala 30:58]
  wire [2:0] side_req_select_shiftAmount = side_req_a[2:0]; // @[BankedStore.scala 155:28]
  wire [7:0] side_req_select = 8'h1 << side_req_select_shiftAmount; // @[OneHot.scala 65:12]
  wire [7:0] _side_req_io_side_adr_ready_T_1 = 8'hff >> side_req_select_shiftAmount; // @[BankedStore.scala 157:21]
  wire [11:0] reqs_0_index = side_req_a[14:3]; // @[BankedStore.scala 160:23]
  wire [1:0] side_req_out_bankSel_lo_lo_lo = side_req_select[0] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] side_req_out_bankSel_lo_lo_hi = side_req_select[1] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] side_req_out_bankSel_lo_hi_lo = side_req_select[2] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] side_req_out_bankSel_lo_hi_hi = side_req_select[3] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] side_req_out_bankSel_hi_lo_lo = side_req_select[4] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] side_req_out_bankSel_hi_lo_hi = side_req_select[5] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] side_req_out_bankSel_hi_hi_lo = side_req_select[6] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] side_req_out_bankSel_hi_hi_hi = side_req_select[7] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [15:0] _side_req_out_bankSel_T_16 = {side_req_out_bankSel_hi_hi_hi,side_req_out_bankSel_hi_hi_lo,
    side_req_out_bankSel_hi_lo_hi,side_req_out_bankSel_hi_lo_lo,side_req_out_bankSel_lo_hi_hi,
    side_req_out_bankSel_lo_hi_lo,side_req_out_bankSel_lo_lo_hi,side_req_out_bankSel_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [15:0] _side_req_out_bankSel_T_17 = {io_side_adr_bits_mask,io_side_adr_bits_mask,io_side_adr_bits_mask,
    io_side_adr_bits_mask,io_side_adr_bits_mask,io_side_adr_bits_mask,io_side_adr_bits_mask,io_side_adr_bits_mask}; // @[Cat.scala 30:58]
  wire [15:0] _side_req_out_bankSel_T_18 = _side_req_out_bankSel_T_16 & _side_req_out_bankSel_T_17; // @[BankedStore.scala 161:65]
  wire [15:0] reqs_0_bankSel = io_side_adr_valid ? _side_req_out_bankSel_T_18 : 16'h0; // @[BankedStore.scala 161:24]
  wire [63:0] sinkC_req_words_data = io_sinkC_dat_data[63:0]; // @[BankedStore.scala 147:19]
  wire [63:0] sinkC_req_words_0 = sinkC_req_words_data ^ error; // @[BankedStore.scala 149:66]
  wire [63:0] sinkC_req_words_data_1 = io_sinkC_dat_data[127:64]; // @[BankedStore.scala 147:19]
  wire [63:0] sinkC_req_words_1 = sinkC_req_words_data_1 ^ error; // @[BankedStore.scala 149:66]
  wire [14:0] sinkC_req_a = {io_sinkC_adr_bits_way,io_sinkC_adr_bits_set,io_sinkC_adr_bits_beat}; // @[Cat.scala 30:58]
  wire [2:0] sinkC_req_select_shiftAmount = sinkC_req_a[2:0]; // @[BankedStore.scala 155:28]
  wire [7:0] sinkC_req_select = 8'h1 << sinkC_req_select_shiftAmount; // @[OneHot.scala 65:12]
  wire  sinkC_req_ready_lo_lo_lo = ~(|reqs_0_bankSel[1:0]); // @[BankedStore.scala 156:58]
  wire  sinkC_req_ready_lo_lo_hi = ~(|reqs_0_bankSel[3:2]); // @[BankedStore.scala 156:58]
  wire  sinkC_req_ready_lo_hi_lo = ~(|reqs_0_bankSel[5:4]); // @[BankedStore.scala 156:58]
  wire  sinkC_req_ready_lo_hi_hi = ~(|reqs_0_bankSel[7:6]); // @[BankedStore.scala 156:58]
  wire  sinkC_req_ready_hi_lo_lo = ~(|reqs_0_bankSel[9:8]); // @[BankedStore.scala 156:58]
  wire  sinkC_req_ready_hi_lo_hi = ~(|reqs_0_bankSel[11:10]); // @[BankedStore.scala 156:58]
  wire  sinkC_req_ready_hi_hi_lo = ~(|reqs_0_bankSel[13:12]); // @[BankedStore.scala 156:58]
  wire  sinkC_req_ready_hi_hi_hi = ~(|reqs_0_bankSel[15:14]); // @[BankedStore.scala 156:58]
  wire [7:0] sinkC_req_ready = {sinkC_req_ready_hi_hi_hi,sinkC_req_ready_hi_hi_lo,sinkC_req_ready_hi_lo_hi,
    sinkC_req_ready_hi_lo_lo,sinkC_req_ready_lo_hi_hi,sinkC_req_ready_lo_hi_lo,sinkC_req_ready_lo_lo_hi,
    sinkC_req_ready_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [7:0] _sinkC_req_io_sinkC_adr_ready_T_1 = sinkC_req_ready >> sinkC_req_select_shiftAmount; // @[BankedStore.scala 157:21]
  wire [11:0] reqs_1_index = sinkC_req_a[14:3]; // @[BankedStore.scala 160:23]
  wire [1:0] sinkC_req_out_bankSel_lo_lo_lo = sinkC_req_select[0] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkC_req_out_bankSel_lo_lo_hi = sinkC_req_select[1] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkC_req_out_bankSel_lo_hi_lo = sinkC_req_select[2] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkC_req_out_bankSel_lo_hi_hi = sinkC_req_select[3] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkC_req_out_bankSel_hi_lo_lo = sinkC_req_select[4] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkC_req_out_bankSel_hi_lo_hi = sinkC_req_select[5] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkC_req_out_bankSel_hi_hi_lo = sinkC_req_select[6] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkC_req_out_bankSel_hi_hi_hi = sinkC_req_select[7] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [15:0] _sinkC_req_out_bankSel_T_16 = {sinkC_req_out_bankSel_hi_hi_hi,sinkC_req_out_bankSel_hi_hi_lo,
    sinkC_req_out_bankSel_hi_lo_hi,sinkC_req_out_bankSel_hi_lo_lo,sinkC_req_out_bankSel_lo_hi_hi,
    sinkC_req_out_bankSel_lo_hi_lo,sinkC_req_out_bankSel_lo_lo_hi,sinkC_req_out_bankSel_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [15:0] reqs_1_bankSel = io_sinkC_adr_valid ? _sinkC_req_out_bankSel_T_16 : 16'h0; // @[BankedStore.scala 161:24]
  wire [1:0] sinkC_req_out_bankEn_lo_lo_lo = sinkC_req_ready[0] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkC_req_out_bankEn_lo_lo_hi = sinkC_req_ready[1] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkC_req_out_bankEn_lo_hi_lo = sinkC_req_ready[2] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkC_req_out_bankEn_lo_hi_hi = sinkC_req_ready[3] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkC_req_out_bankEn_hi_lo_lo = sinkC_req_ready[4] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkC_req_out_bankEn_hi_lo_hi = sinkC_req_ready[5] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkC_req_out_bankEn_hi_hi_lo = sinkC_req_ready[6] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkC_req_out_bankEn_hi_hi_hi = sinkC_req_ready[7] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [15:0] _sinkC_req_out_bankEn_T_16 = {sinkC_req_out_bankEn_hi_hi_hi,sinkC_req_out_bankEn_hi_hi_lo,
    sinkC_req_out_bankEn_hi_lo_hi,sinkC_req_out_bankEn_hi_lo_lo,sinkC_req_out_bankEn_lo_hi_hi,
    sinkC_req_out_bankEn_lo_hi_lo,sinkC_req_out_bankEn_lo_lo_hi,sinkC_req_out_bankEn_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [15:0] _sinkC_req_out_bankEn_T_17 = reqs_1_bankSel & _sinkC_req_out_bankEn_T_16; // @[BankedStore.scala 162:59]
  wire [15:0] reqs_1_bankEn = io_sinkC_adr_bits_noop ? 16'h0 : _sinkC_req_out_bankEn_T_17; // @[BankedStore.scala 162:24]
  wire [63:0] sinkD_req_words_data = io_sinkD_dat_data[63:0]; // @[BankedStore.scala 147:19]
  wire [63:0] sinkD_req_words_0 = sinkD_req_words_data ^ error; // @[BankedStore.scala 149:66]
  wire [63:0] sinkD_req_words_data_1 = io_sinkD_dat_data[127:64]; // @[BankedStore.scala 147:19]
  wire [63:0] sinkD_req_words_1 = sinkD_req_words_data_1 ^ error; // @[BankedStore.scala 149:66]
  wire [14:0] sinkD_req_a = {io_sinkD_adr_bits_way,io_sinkD_adr_bits_set,io_sinkD_adr_bits_beat}; // @[Cat.scala 30:58]
  wire [2:0] sinkD_req_select_shiftAmount = sinkD_req_a[2:0]; // @[BankedStore.scala 155:28]
  wire [7:0] sinkD_req_select = 8'h1 << sinkD_req_select_shiftAmount; // @[OneHot.scala 65:12]
  wire [14:0] sourceC_req_a = {io_sourceC_adr_bits_way,io_sourceC_adr_bits_set,io_sourceC_adr_bits_beat}; // @[Cat.scala 30:58]
  wire [2:0] sourceC_req_select_shiftAmount = sourceC_req_a[2:0]; // @[BankedStore.scala 155:28]
  wire [7:0] sourceC_req_select = 8'h1 << sourceC_req_select_shiftAmount; // @[OneHot.scala 65:12]
  wire [1:0] sourceC_req_out_bankSel_hi_hi_hi = sourceC_req_select[7] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceC_req_out_bankSel_hi_hi_lo = sourceC_req_select[6] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceC_req_out_bankSel_hi_lo_hi = sourceC_req_select[5] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceC_req_out_bankSel_hi_lo_lo = sourceC_req_select[4] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceC_req_out_bankSel_lo_hi_hi = sourceC_req_select[3] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceC_req_out_bankSel_lo_hi_lo = sourceC_req_select[2] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceC_req_out_bankSel_lo_lo_hi = sourceC_req_select[1] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceC_req_out_bankSel_lo_lo_lo = sourceC_req_select[0] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [15:0] _sourceC_req_out_bankSel_T_16 = {sourceC_req_out_bankSel_hi_hi_hi,sourceC_req_out_bankSel_hi_hi_lo,
    sourceC_req_out_bankSel_hi_lo_hi,sourceC_req_out_bankSel_hi_lo_lo,sourceC_req_out_bankSel_lo_hi_hi,
    sourceC_req_out_bankSel_lo_hi_lo,sourceC_req_out_bankSel_lo_lo_hi,sourceC_req_out_bankSel_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [15:0] reqs_2_bankSel = io_sourceC_adr_valid ? _sourceC_req_out_bankSel_T_16 : 16'h0; // @[BankedStore.scala 161:24]
  wire [15:0] reqs_2_bankSum = reqs_1_bankSel | reqs_0_bankSel; // @[BankedStore.scala 188:17]
  wire [15:0] reqs_3_bankSum = reqs_2_bankSel | reqs_2_bankSum; // @[BankedStore.scala 188:17]
  wire  sinkD_req_ready_lo_lo_lo = ~(|reqs_3_bankSum[1:0]); // @[BankedStore.scala 156:58]
  wire  sinkD_req_ready_lo_lo_hi = ~(|reqs_3_bankSum[3:2]); // @[BankedStore.scala 156:58]
  wire  sinkD_req_ready_lo_hi_lo = ~(|reqs_3_bankSum[5:4]); // @[BankedStore.scala 156:58]
  wire  sinkD_req_ready_lo_hi_hi = ~(|reqs_3_bankSum[7:6]); // @[BankedStore.scala 156:58]
  wire  sinkD_req_ready_hi_lo_lo = ~(|reqs_3_bankSum[9:8]); // @[BankedStore.scala 156:58]
  wire  sinkD_req_ready_hi_lo_hi = ~(|reqs_3_bankSum[11:10]); // @[BankedStore.scala 156:58]
  wire  sinkD_req_ready_hi_hi_lo = ~(|reqs_3_bankSum[13:12]); // @[BankedStore.scala 156:58]
  wire  sinkD_req_ready_hi_hi_hi = ~(|reqs_3_bankSum[15:14]); // @[BankedStore.scala 156:58]
  wire [7:0] sinkD_req_ready = {sinkD_req_ready_hi_hi_hi,sinkD_req_ready_hi_hi_lo,sinkD_req_ready_hi_lo_hi,
    sinkD_req_ready_hi_lo_lo,sinkD_req_ready_lo_hi_hi,sinkD_req_ready_lo_hi_lo,sinkD_req_ready_lo_lo_hi,
    sinkD_req_ready_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [7:0] _sinkD_req_io_sinkD_adr_ready_T_1 = sinkD_req_ready >> sinkD_req_select_shiftAmount; // @[BankedStore.scala 157:21]
  wire [11:0] reqs_3_index = sinkD_req_a[14:3]; // @[BankedStore.scala 160:23]
  wire [1:0] sinkD_req_out_bankSel_lo_lo_lo = sinkD_req_select[0] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkD_req_out_bankSel_lo_lo_hi = sinkD_req_select[1] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkD_req_out_bankSel_lo_hi_lo = sinkD_req_select[2] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkD_req_out_bankSel_lo_hi_hi = sinkD_req_select[3] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkD_req_out_bankSel_hi_lo_lo = sinkD_req_select[4] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkD_req_out_bankSel_hi_lo_hi = sinkD_req_select[5] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkD_req_out_bankSel_hi_hi_lo = sinkD_req_select[6] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkD_req_out_bankSel_hi_hi_hi = sinkD_req_select[7] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [15:0] _sinkD_req_out_bankSel_T_16 = {sinkD_req_out_bankSel_hi_hi_hi,sinkD_req_out_bankSel_hi_hi_lo,
    sinkD_req_out_bankSel_hi_lo_hi,sinkD_req_out_bankSel_hi_lo_lo,sinkD_req_out_bankSel_lo_hi_hi,
    sinkD_req_out_bankSel_lo_hi_lo,sinkD_req_out_bankSel_lo_lo_hi,sinkD_req_out_bankSel_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [15:0] reqs_3_bankSel = io_sinkD_adr_valid ? _sinkD_req_out_bankSel_T_16 : 16'h0; // @[BankedStore.scala 161:24]
  wire [1:0] sinkD_req_out_bankEn_lo_lo_lo = sinkD_req_ready[0] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkD_req_out_bankEn_lo_lo_hi = sinkD_req_ready[1] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkD_req_out_bankEn_lo_hi_lo = sinkD_req_ready[2] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkD_req_out_bankEn_lo_hi_hi = sinkD_req_ready[3] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkD_req_out_bankEn_hi_lo_lo = sinkD_req_ready[4] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkD_req_out_bankEn_hi_lo_hi = sinkD_req_ready[5] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkD_req_out_bankEn_hi_hi_lo = sinkD_req_ready[6] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sinkD_req_out_bankEn_hi_hi_hi = sinkD_req_ready[7] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [15:0] _sinkD_req_out_bankEn_T_16 = {sinkD_req_out_bankEn_hi_hi_hi,sinkD_req_out_bankEn_hi_hi_lo,
    sinkD_req_out_bankEn_hi_lo_hi,sinkD_req_out_bankEn_hi_lo_lo,sinkD_req_out_bankEn_lo_hi_hi,
    sinkD_req_out_bankEn_lo_hi_lo,sinkD_req_out_bankEn_lo_lo_hi,sinkD_req_out_bankEn_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [15:0] _sinkD_req_out_bankEn_T_17 = reqs_3_bankSel & _sinkD_req_out_bankEn_T_16; // @[BankedStore.scala 162:59]
  wire [15:0] reqs_3_bankEn = io_sinkD_adr_bits_noop ? 16'h0 : _sinkD_req_out_bankEn_T_17; // @[BankedStore.scala 162:24]
  wire  sourceC_req_ready_lo_lo_lo = ~(|reqs_2_bankSum[1:0]); // @[BankedStore.scala 156:58]
  wire  sourceC_req_ready_lo_lo_hi = ~(|reqs_2_bankSum[3:2]); // @[BankedStore.scala 156:58]
  wire  sourceC_req_ready_lo_hi_lo = ~(|reqs_2_bankSum[5:4]); // @[BankedStore.scala 156:58]
  wire  sourceC_req_ready_lo_hi_hi = ~(|reqs_2_bankSum[7:6]); // @[BankedStore.scala 156:58]
  wire  sourceC_req_ready_hi_lo_lo = ~(|reqs_2_bankSum[9:8]); // @[BankedStore.scala 156:58]
  wire  sourceC_req_ready_hi_lo_hi = ~(|reqs_2_bankSum[11:10]); // @[BankedStore.scala 156:58]
  wire  sourceC_req_ready_hi_hi_lo = ~(|reqs_2_bankSum[13:12]); // @[BankedStore.scala 156:58]
  wire  sourceC_req_ready_hi_hi_hi = ~(|reqs_2_bankSum[15:14]); // @[BankedStore.scala 156:58]
  wire [7:0] sourceC_req_ready = {sourceC_req_ready_hi_hi_hi,sourceC_req_ready_hi_hi_lo,sourceC_req_ready_hi_lo_hi,
    sourceC_req_ready_hi_lo_lo,sourceC_req_ready_lo_hi_hi,sourceC_req_ready_lo_hi_lo,sourceC_req_ready_lo_lo_hi,
    sourceC_req_ready_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [7:0] _sourceC_req_io_sourceC_adr_ready_T_1 = sourceC_req_ready >> sourceC_req_select_shiftAmount; // @[BankedStore.scala 157:21]
  wire [11:0] reqs_2_index = sourceC_req_a[14:3]; // @[BankedStore.scala 160:23]
  wire [1:0] sourceC_req_out_bankEn_lo_lo_lo = sourceC_req_ready[0] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceC_req_out_bankEn_lo_lo_hi = sourceC_req_ready[1] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceC_req_out_bankEn_lo_hi_lo = sourceC_req_ready[2] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceC_req_out_bankEn_lo_hi_hi = sourceC_req_ready[3] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceC_req_out_bankEn_hi_lo_lo = sourceC_req_ready[4] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceC_req_out_bankEn_hi_lo_hi = sourceC_req_ready[5] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceC_req_out_bankEn_hi_hi_lo = sourceC_req_ready[6] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceC_req_out_bankEn_hi_hi_hi = sourceC_req_ready[7] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [15:0] _sourceC_req_out_bankEn_T_16 = {sourceC_req_out_bankEn_hi_hi_hi,sourceC_req_out_bankEn_hi_hi_lo,
    sourceC_req_out_bankEn_hi_lo_hi,sourceC_req_out_bankEn_hi_lo_lo,sourceC_req_out_bankEn_lo_hi_hi,
    sourceC_req_out_bankEn_lo_hi_lo,sourceC_req_out_bankEn_lo_lo_hi,sourceC_req_out_bankEn_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [15:0] reqs_2_bankEn = reqs_2_bankSel & _sourceC_req_out_bankEn_T_16; // @[BankedStore.scala 162:59]
  wire [14:0] sourceD_rreq_a = {io_sourceD_radr_bits_way,io_sourceD_radr_bits_set,io_sourceD_radr_bits_beat}; // @[Cat.scala 30:58]
  wire [2:0] sourceD_rreq_select_shiftAmount = sourceD_rreq_a[2:0]; // @[BankedStore.scala 155:28]
  wire [7:0] sourceD_rreq_select = 8'h1 << sourceD_rreq_select_shiftAmount; // @[OneHot.scala 65:12]
  wire [14:0] sourceD_wreq_a = {io_sourceD_wadr_bits_way,io_sourceD_wadr_bits_set,io_sourceD_wadr_bits_beat}; // @[Cat.scala 30:58]
  wire [2:0] sourceD_wreq_select_shiftAmount = sourceD_wreq_a[2:0]; // @[BankedStore.scala 155:28]
  wire [7:0] sourceD_wreq_select = 8'h1 << sourceD_wreq_select_shiftAmount; // @[OneHot.scala 65:12]
  wire [1:0] sourceD_wreq_out_bankSel_hi_hi_hi = sourceD_wreq_select[7] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_wreq_out_bankSel_hi_hi_lo = sourceD_wreq_select[6] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_wreq_out_bankSel_hi_lo_hi = sourceD_wreq_select[5] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_wreq_out_bankSel_hi_lo_lo = sourceD_wreq_select[4] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_wreq_out_bankSel_lo_hi_hi = sourceD_wreq_select[3] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_wreq_out_bankSel_lo_hi_lo = sourceD_wreq_select[2] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_wreq_out_bankSel_lo_lo_hi = sourceD_wreq_select[1] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_wreq_out_bankSel_lo_lo_lo = sourceD_wreq_select[0] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [15:0] _sourceD_wreq_out_bankSel_T_16 = {sourceD_wreq_out_bankSel_hi_hi_hi,sourceD_wreq_out_bankSel_hi_hi_lo,
    sourceD_wreq_out_bankSel_hi_lo_hi,sourceD_wreq_out_bankSel_hi_lo_lo,sourceD_wreq_out_bankSel_lo_hi_hi,
    sourceD_wreq_out_bankSel_lo_hi_lo,sourceD_wreq_out_bankSel_lo_lo_hi,sourceD_wreq_out_bankSel_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [15:0] _sourceD_wreq_out_bankSel_T_17 = {io_sourceD_wadr_bits_mask,io_sourceD_wadr_bits_mask,
    io_sourceD_wadr_bits_mask,io_sourceD_wadr_bits_mask,io_sourceD_wadr_bits_mask,io_sourceD_wadr_bits_mask,
    io_sourceD_wadr_bits_mask,io_sourceD_wadr_bits_mask}; // @[Cat.scala 30:58]
  wire [15:0] _sourceD_wreq_out_bankSel_T_18 = _sourceD_wreq_out_bankSel_T_16 & _sourceD_wreq_out_bankSel_T_17; // @[BankedStore.scala 161:65]
  wire [15:0] reqs_4_bankSel = io_sourceD_wadr_valid ? _sourceD_wreq_out_bankSel_T_18 : 16'h0; // @[BankedStore.scala 161:24]
  wire [15:0] reqs_4_bankSum = reqs_3_bankSel | reqs_3_bankSum; // @[BankedStore.scala 188:17]
  wire [15:0] reqs_5_bankSum = reqs_4_bankSel | reqs_4_bankSum; // @[BankedStore.scala 188:17]
  wire [1:0] _sourceD_rreq_ready_T_1 = reqs_5_bankSum[1:0] & io_sourceD_radr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_rreq_ready_lo_lo_lo = ~(|_sourceD_rreq_ready_T_1); // @[BankedStore.scala 156:58]
  wire [1:0] _sourceD_rreq_ready_T_4 = reqs_5_bankSum[3:2] & io_sourceD_radr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_rreq_ready_lo_lo_hi = ~(|_sourceD_rreq_ready_T_4); // @[BankedStore.scala 156:58]
  wire [1:0] _sourceD_rreq_ready_T_7 = reqs_5_bankSum[5:4] & io_sourceD_radr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_rreq_ready_lo_hi_lo = ~(|_sourceD_rreq_ready_T_7); // @[BankedStore.scala 156:58]
  wire [1:0] _sourceD_rreq_ready_T_10 = reqs_5_bankSum[7:6] & io_sourceD_radr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_rreq_ready_lo_hi_hi = ~(|_sourceD_rreq_ready_T_10); // @[BankedStore.scala 156:58]
  wire [1:0] _sourceD_rreq_ready_T_13 = reqs_5_bankSum[9:8] & io_sourceD_radr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_rreq_ready_hi_lo_lo = ~(|_sourceD_rreq_ready_T_13); // @[BankedStore.scala 156:58]
  wire [1:0] _sourceD_rreq_ready_T_16 = reqs_5_bankSum[11:10] & io_sourceD_radr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_rreq_ready_hi_lo_hi = ~(|_sourceD_rreq_ready_T_16); // @[BankedStore.scala 156:58]
  wire [1:0] _sourceD_rreq_ready_T_19 = reqs_5_bankSum[13:12] & io_sourceD_radr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_rreq_ready_hi_hi_lo = ~(|_sourceD_rreq_ready_T_19); // @[BankedStore.scala 156:58]
  wire [1:0] _sourceD_rreq_ready_T_22 = reqs_5_bankSum[15:14] & io_sourceD_radr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_rreq_ready_hi_hi_hi = ~(|_sourceD_rreq_ready_T_22); // @[BankedStore.scala 156:58]
  wire [7:0] sourceD_rreq_ready = {sourceD_rreq_ready_hi_hi_hi,sourceD_rreq_ready_hi_hi_lo,sourceD_rreq_ready_hi_lo_hi,
    sourceD_rreq_ready_hi_lo_lo,sourceD_rreq_ready_lo_hi_hi,sourceD_rreq_ready_lo_hi_lo,sourceD_rreq_ready_lo_lo_hi,
    sourceD_rreq_ready_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [7:0] _sourceD_rreq_io_sourceD_radr_ready_T_1 = sourceD_rreq_ready >> sourceD_rreq_select_shiftAmount; // @[BankedStore.scala 157:21]
  wire [11:0] reqs_5_index = sourceD_rreq_a[14:3]; // @[BankedStore.scala 160:23]
  wire [1:0] sourceD_rreq_out_bankSel_lo_lo_lo = sourceD_rreq_select[0] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_rreq_out_bankSel_lo_lo_hi = sourceD_rreq_select[1] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_rreq_out_bankSel_lo_hi_lo = sourceD_rreq_select[2] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_rreq_out_bankSel_lo_hi_hi = sourceD_rreq_select[3] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_rreq_out_bankSel_hi_lo_lo = sourceD_rreq_select[4] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_rreq_out_bankSel_hi_lo_hi = sourceD_rreq_select[5] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_rreq_out_bankSel_hi_hi_lo = sourceD_rreq_select[6] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_rreq_out_bankSel_hi_hi_hi = sourceD_rreq_select[7] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [15:0] _sourceD_rreq_out_bankSel_T_16 = {sourceD_rreq_out_bankSel_hi_hi_hi,sourceD_rreq_out_bankSel_hi_hi_lo,
    sourceD_rreq_out_bankSel_hi_lo_hi,sourceD_rreq_out_bankSel_hi_lo_lo,sourceD_rreq_out_bankSel_lo_hi_hi,
    sourceD_rreq_out_bankSel_lo_hi_lo,sourceD_rreq_out_bankSel_lo_lo_hi,sourceD_rreq_out_bankSel_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [15:0] _sourceD_rreq_out_bankSel_T_17 = {io_sourceD_radr_bits_mask,io_sourceD_radr_bits_mask,
    io_sourceD_radr_bits_mask,io_sourceD_radr_bits_mask,io_sourceD_radr_bits_mask,io_sourceD_radr_bits_mask,
    io_sourceD_radr_bits_mask,io_sourceD_radr_bits_mask}; // @[Cat.scala 30:58]
  wire [15:0] _sourceD_rreq_out_bankSel_T_18 = _sourceD_rreq_out_bankSel_T_16 & _sourceD_rreq_out_bankSel_T_17; // @[BankedStore.scala 161:65]
  wire [15:0] reqs_5_bankSel = io_sourceD_radr_valid ? _sourceD_rreq_out_bankSel_T_18 : 16'h0; // @[BankedStore.scala 161:24]
  wire [1:0] sourceD_rreq_out_bankEn_lo_lo_lo = sourceD_rreq_ready[0] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_rreq_out_bankEn_lo_lo_hi = sourceD_rreq_ready[1] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_rreq_out_bankEn_lo_hi_lo = sourceD_rreq_ready[2] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_rreq_out_bankEn_lo_hi_hi = sourceD_rreq_ready[3] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_rreq_out_bankEn_hi_lo_lo = sourceD_rreq_ready[4] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_rreq_out_bankEn_hi_lo_hi = sourceD_rreq_ready[5] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_rreq_out_bankEn_hi_hi_lo = sourceD_rreq_ready[6] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_rreq_out_bankEn_hi_hi_hi = sourceD_rreq_ready[7] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [15:0] _sourceD_rreq_out_bankEn_T_16 = {sourceD_rreq_out_bankEn_hi_hi_hi,sourceD_rreq_out_bankEn_hi_hi_lo,
    sourceD_rreq_out_bankEn_hi_lo_hi,sourceD_rreq_out_bankEn_hi_lo_lo,sourceD_rreq_out_bankEn_lo_hi_hi,
    sourceD_rreq_out_bankEn_lo_hi_lo,sourceD_rreq_out_bankEn_lo_lo_hi,sourceD_rreq_out_bankEn_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [15:0] reqs_5_bankEn = reqs_5_bankSel & _sourceD_rreq_out_bankEn_T_16; // @[BankedStore.scala 162:59]
  wire [63:0] sourceD_wreq_words_data = io_sourceD_wdat_data[63:0]; // @[BankedStore.scala 147:19]
  wire [63:0] sourceD_wreq_words_0 = sourceD_wreq_words_data ^ error; // @[BankedStore.scala 149:66]
  wire [63:0] sourceD_wreq_words_data_1 = io_sourceD_wdat_data[127:64]; // @[BankedStore.scala 147:19]
  wire [63:0] sourceD_wreq_words_1 = sourceD_wreq_words_data_1 ^ error; // @[BankedStore.scala 149:66]
  wire [1:0] _sourceD_wreq_ready_T_1 = reqs_4_bankSum[1:0] & io_sourceD_wadr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_wreq_ready_lo_lo_lo = ~(|_sourceD_wreq_ready_T_1); // @[BankedStore.scala 156:58]
  wire [1:0] _sourceD_wreq_ready_T_4 = reqs_4_bankSum[3:2] & io_sourceD_wadr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_wreq_ready_lo_lo_hi = ~(|_sourceD_wreq_ready_T_4); // @[BankedStore.scala 156:58]
  wire [1:0] _sourceD_wreq_ready_T_7 = reqs_4_bankSum[5:4] & io_sourceD_wadr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_wreq_ready_lo_hi_lo = ~(|_sourceD_wreq_ready_T_7); // @[BankedStore.scala 156:58]
  wire [1:0] _sourceD_wreq_ready_T_10 = reqs_4_bankSum[7:6] & io_sourceD_wadr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_wreq_ready_lo_hi_hi = ~(|_sourceD_wreq_ready_T_10); // @[BankedStore.scala 156:58]
  wire [1:0] _sourceD_wreq_ready_T_13 = reqs_4_bankSum[9:8] & io_sourceD_wadr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_wreq_ready_hi_lo_lo = ~(|_sourceD_wreq_ready_T_13); // @[BankedStore.scala 156:58]
  wire [1:0] _sourceD_wreq_ready_T_16 = reqs_4_bankSum[11:10] & io_sourceD_wadr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_wreq_ready_hi_lo_hi = ~(|_sourceD_wreq_ready_T_16); // @[BankedStore.scala 156:58]
  wire [1:0] _sourceD_wreq_ready_T_19 = reqs_4_bankSum[13:12] & io_sourceD_wadr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_wreq_ready_hi_hi_lo = ~(|_sourceD_wreq_ready_T_19); // @[BankedStore.scala 156:58]
  wire [1:0] _sourceD_wreq_ready_T_22 = reqs_4_bankSum[15:14] & io_sourceD_wadr_bits_mask; // @[BankedStore.scala 156:96]
  wire  sourceD_wreq_ready_hi_hi_hi = ~(|_sourceD_wreq_ready_T_22); // @[BankedStore.scala 156:58]
  wire [7:0] sourceD_wreq_ready = {sourceD_wreq_ready_hi_hi_hi,sourceD_wreq_ready_hi_hi_lo,sourceD_wreq_ready_hi_lo_hi,
    sourceD_wreq_ready_hi_lo_lo,sourceD_wreq_ready_lo_hi_hi,sourceD_wreq_ready_lo_hi_lo,sourceD_wreq_ready_lo_lo_hi,
    sourceD_wreq_ready_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [7:0] _sourceD_wreq_io_sourceD_wadr_ready_T_1 = sourceD_wreq_ready >> sourceD_wreq_select_shiftAmount; // @[BankedStore.scala 157:21]
  wire [11:0] reqs_4_index = sourceD_wreq_a[14:3]; // @[BankedStore.scala 160:23]
  wire [1:0] sourceD_wreq_out_bankEn_lo_lo_lo = sourceD_wreq_ready[0] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_wreq_out_bankEn_lo_lo_hi = sourceD_wreq_ready[1] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_wreq_out_bankEn_lo_hi_lo = sourceD_wreq_ready[2] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_wreq_out_bankEn_lo_hi_hi = sourceD_wreq_ready[3] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_wreq_out_bankEn_hi_lo_lo = sourceD_wreq_ready[4] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_wreq_out_bankEn_hi_lo_hi = sourceD_wreq_ready[5] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_wreq_out_bankEn_hi_hi_lo = sourceD_wreq_ready[6] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [1:0] sourceD_wreq_out_bankEn_hi_hi_hi = sourceD_wreq_ready[7] ? 2'h3 : 2'h0; // @[Bitwise.scala 72:12]
  wire [15:0] _sourceD_wreq_out_bankEn_T_16 = {sourceD_wreq_out_bankEn_hi_hi_hi,sourceD_wreq_out_bankEn_hi_hi_lo,
    sourceD_wreq_out_bankEn_hi_lo_hi,sourceD_wreq_out_bankEn_hi_lo_lo,sourceD_wreq_out_bankEn_lo_hi_hi,
    sourceD_wreq_out_bankEn_lo_hi_lo,sourceD_wreq_out_bankEn_lo_lo_hi,sourceD_wreq_out_bankEn_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [15:0] reqs_4_bankEn = reqs_4_bankSel & _sourceD_wreq_out_bankEn_T_16; // @[BankedStore.scala 162:59]
  wire  regout_en = reqs_0_bankSel[0] | reqs_1_bankEn[0] | reqs_2_bankEn[0] | reqs_3_bankEn[0] | reqs_4_bankEn[0] |
    reqs_5_bankEn[0]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1 = reqs_1_bankSel[0]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2 = reqs_2_bankSel[0]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3 = reqs_3_bankSel[0]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4 = reqs_4_bankSel[0]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_2 = regout_sel_2 ? 1'h0 : regout_sel_3 | regout_sel_4; // @[Mux.scala 47:69]
  wire  regout_wen = reqs_0_bankSel[0] ? io_side_adr_bits_write : regout_sel_1 | _regout_wen_T_2; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T = regout_sel_4 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_1 = regout_sel_3 ? reqs_3_index : _regout_idx_T; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_2 = regout_sel_2 ? reqs_2_index : _regout_idx_T_1; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_3 = regout_sel_1 ? reqs_1_index : _regout_idx_T_2; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T = regout_sel_4 ? sourceD_wreq_words_0 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_1 = regout_sel_3 ? sinkD_req_words_0 : _regout_data_T; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_2 = regout_sel_2 ? error : _regout_data_T_1; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_3 = regout_sel_1 ? sinkC_req_words_0 : _regout_data_T_2; // @[Mux.scala 47:69]
  wire  _regout_T = regout_wen & regout_en; // @[BankedStore.scala 199:15]
  wire  _regout_T_2 = ~regout_wen & regout_en; // @[BankedStore.scala 203:32]
  reg  regout_REG; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r; // @[Reg.scala 15:16]
  wire  regout_en_1 = reqs_0_bankSel[1] | reqs_1_bankEn[1] | reqs_2_bankEn[1] | reqs_3_bankEn[1] | reqs_4_bankEn[1] |
    reqs_5_bankEn[1]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1_1 = reqs_1_bankSel[1]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2_1 = reqs_2_bankSel[1]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3_1 = reqs_3_bankSel[1]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4_1 = reqs_4_bankSel[1]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_6 = regout_sel_2_1 ? 1'h0 : regout_sel_3_1 | regout_sel_4_1; // @[Mux.scala 47:69]
  wire  regout_wen_1 = reqs_0_bankSel[1] ? io_side_adr_bits_write : regout_sel_1_1 | _regout_wen_T_6; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_4 = regout_sel_4_1 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_5 = regout_sel_3_1 ? reqs_3_index : _regout_idx_T_4; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_6 = regout_sel_2_1 ? reqs_2_index : _regout_idx_T_5; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_7 = regout_sel_1_1 ? reqs_1_index : _regout_idx_T_6; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_4 = regout_sel_4_1 ? sourceD_wreq_words_1 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_5 = regout_sel_3_1 ? sinkD_req_words_1 : _regout_data_T_4; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_6 = regout_sel_2_1 ? error : _regout_data_T_5; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_7 = regout_sel_1_1 ? sinkC_req_words_1 : _regout_data_T_6; // @[Mux.scala 47:69]
  wire  _regout_T_7 = regout_wen_1 & regout_en_1; // @[BankedStore.scala 199:15]
  wire  _regout_T_9 = ~regout_wen_1 & regout_en_1; // @[BankedStore.scala 203:32]
  reg  regout_REG_1; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r_1; // @[Reg.scala 15:16]
  wire  regout_en_2 = reqs_0_bankSel[2] | reqs_1_bankEn[2] | reqs_2_bankEn[2] | reqs_3_bankEn[2] | reqs_4_bankEn[2] |
    reqs_5_bankEn[2]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1_2 = reqs_1_bankSel[2]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2_2 = reqs_2_bankSel[2]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3_2 = reqs_3_bankSel[2]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4_2 = reqs_4_bankSel[2]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_10 = regout_sel_2_2 ? 1'h0 : regout_sel_3_2 | regout_sel_4_2; // @[Mux.scala 47:69]
  wire  regout_wen_2 = reqs_0_bankSel[2] ? io_side_adr_bits_write : regout_sel_1_2 | _regout_wen_T_10; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_8 = regout_sel_4_2 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_9 = regout_sel_3_2 ? reqs_3_index : _regout_idx_T_8; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_10 = regout_sel_2_2 ? reqs_2_index : _regout_idx_T_9; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_11 = regout_sel_1_2 ? reqs_1_index : _regout_idx_T_10; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_8 = regout_sel_4_2 ? sourceD_wreq_words_0 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_9 = regout_sel_3_2 ? sinkD_req_words_0 : _regout_data_T_8; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_10 = regout_sel_2_2 ? error : _regout_data_T_9; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_11 = regout_sel_1_2 ? sinkC_req_words_0 : _regout_data_T_10; // @[Mux.scala 47:69]
  wire  _regout_T_14 = regout_wen_2 & regout_en_2; // @[BankedStore.scala 199:15]
  wire  _regout_T_16 = ~regout_wen_2 & regout_en_2; // @[BankedStore.scala 203:32]
  reg  regout_REG_2; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r_2; // @[Reg.scala 15:16]
  wire  regout_en_3 = reqs_0_bankSel[3] | reqs_1_bankEn[3] | reqs_2_bankEn[3] | reqs_3_bankEn[3] | reqs_4_bankEn[3] |
    reqs_5_bankEn[3]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1_3 = reqs_1_bankSel[3]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2_3 = reqs_2_bankSel[3]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3_3 = reqs_3_bankSel[3]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4_3 = reqs_4_bankSel[3]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_14 = regout_sel_2_3 ? 1'h0 : regout_sel_3_3 | regout_sel_4_3; // @[Mux.scala 47:69]
  wire  regout_wen_3 = reqs_0_bankSel[3] ? io_side_adr_bits_write : regout_sel_1_3 | _regout_wen_T_14; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_12 = regout_sel_4_3 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_13 = regout_sel_3_3 ? reqs_3_index : _regout_idx_T_12; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_14 = regout_sel_2_3 ? reqs_2_index : _regout_idx_T_13; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_15 = regout_sel_1_3 ? reqs_1_index : _regout_idx_T_14; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_12 = regout_sel_4_3 ? sourceD_wreq_words_1 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_13 = regout_sel_3_3 ? sinkD_req_words_1 : _regout_data_T_12; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_14 = regout_sel_2_3 ? error : _regout_data_T_13; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_15 = regout_sel_1_3 ? sinkC_req_words_1 : _regout_data_T_14; // @[Mux.scala 47:69]
  wire  _regout_T_21 = regout_wen_3 & regout_en_3; // @[BankedStore.scala 199:15]
  wire  _regout_T_23 = ~regout_wen_3 & regout_en_3; // @[BankedStore.scala 203:32]
  reg  regout_REG_3; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r_3; // @[Reg.scala 15:16]
  wire  regout_en_4 = reqs_0_bankSel[4] | reqs_1_bankEn[4] | reqs_2_bankEn[4] | reqs_3_bankEn[4] | reqs_4_bankEn[4] |
    reqs_5_bankEn[4]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1_4 = reqs_1_bankSel[4]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2_4 = reqs_2_bankSel[4]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3_4 = reqs_3_bankSel[4]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4_4 = reqs_4_bankSel[4]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_18 = regout_sel_2_4 ? 1'h0 : regout_sel_3_4 | regout_sel_4_4; // @[Mux.scala 47:69]
  wire  regout_wen_4 = reqs_0_bankSel[4] ? io_side_adr_bits_write : regout_sel_1_4 | _regout_wen_T_18; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_16 = regout_sel_4_4 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_17 = regout_sel_3_4 ? reqs_3_index : _regout_idx_T_16; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_18 = regout_sel_2_4 ? reqs_2_index : _regout_idx_T_17; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_19 = regout_sel_1_4 ? reqs_1_index : _regout_idx_T_18; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_16 = regout_sel_4_4 ? sourceD_wreq_words_0 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_17 = regout_sel_3_4 ? sinkD_req_words_0 : _regout_data_T_16; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_18 = regout_sel_2_4 ? error : _regout_data_T_17; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_19 = regout_sel_1_4 ? sinkC_req_words_0 : _regout_data_T_18; // @[Mux.scala 47:69]
  wire  _regout_T_28 = regout_wen_4 & regout_en_4; // @[BankedStore.scala 199:15]
  wire  _regout_T_30 = ~regout_wen_4 & regout_en_4; // @[BankedStore.scala 203:32]
  reg  regout_REG_4; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r_4; // @[Reg.scala 15:16]
  wire  regout_en_5 = reqs_0_bankSel[5] | reqs_1_bankEn[5] | reqs_2_bankEn[5] | reqs_3_bankEn[5] | reqs_4_bankEn[5] |
    reqs_5_bankEn[5]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1_5 = reqs_1_bankSel[5]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2_5 = reqs_2_bankSel[5]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3_5 = reqs_3_bankSel[5]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4_5 = reqs_4_bankSel[5]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_22 = regout_sel_2_5 ? 1'h0 : regout_sel_3_5 | regout_sel_4_5; // @[Mux.scala 47:69]
  wire  regout_wen_5 = reqs_0_bankSel[5] ? io_side_adr_bits_write : regout_sel_1_5 | _regout_wen_T_22; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_20 = regout_sel_4_5 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_21 = regout_sel_3_5 ? reqs_3_index : _regout_idx_T_20; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_22 = regout_sel_2_5 ? reqs_2_index : _regout_idx_T_21; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_23 = regout_sel_1_5 ? reqs_1_index : _regout_idx_T_22; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_20 = regout_sel_4_5 ? sourceD_wreq_words_1 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_21 = regout_sel_3_5 ? sinkD_req_words_1 : _regout_data_T_20; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_22 = regout_sel_2_5 ? error : _regout_data_T_21; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_23 = regout_sel_1_5 ? sinkC_req_words_1 : _regout_data_T_22; // @[Mux.scala 47:69]
  wire  _regout_T_35 = regout_wen_5 & regout_en_5; // @[BankedStore.scala 199:15]
  wire  _regout_T_37 = ~regout_wen_5 & regout_en_5; // @[BankedStore.scala 203:32]
  reg  regout_REG_5; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r_5; // @[Reg.scala 15:16]
  wire  regout_en_6 = reqs_0_bankSel[6] | reqs_1_bankEn[6] | reqs_2_bankEn[6] | reqs_3_bankEn[6] | reqs_4_bankEn[6] |
    reqs_5_bankEn[6]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1_6 = reqs_1_bankSel[6]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2_6 = reqs_2_bankSel[6]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3_6 = reqs_3_bankSel[6]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4_6 = reqs_4_bankSel[6]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_26 = regout_sel_2_6 ? 1'h0 : regout_sel_3_6 | regout_sel_4_6; // @[Mux.scala 47:69]
  wire  regout_wen_6 = reqs_0_bankSel[6] ? io_side_adr_bits_write : regout_sel_1_6 | _regout_wen_T_26; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_24 = regout_sel_4_6 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_25 = regout_sel_3_6 ? reqs_3_index : _regout_idx_T_24; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_26 = regout_sel_2_6 ? reqs_2_index : _regout_idx_T_25; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_27 = regout_sel_1_6 ? reqs_1_index : _regout_idx_T_26; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_24 = regout_sel_4_6 ? sourceD_wreq_words_0 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_25 = regout_sel_3_6 ? sinkD_req_words_0 : _regout_data_T_24; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_26 = regout_sel_2_6 ? error : _regout_data_T_25; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_27 = regout_sel_1_6 ? sinkC_req_words_0 : _regout_data_T_26; // @[Mux.scala 47:69]
  wire  _regout_T_42 = regout_wen_6 & regout_en_6; // @[BankedStore.scala 199:15]
  wire  _regout_T_44 = ~regout_wen_6 & regout_en_6; // @[BankedStore.scala 203:32]
  reg  regout_REG_6; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r_6; // @[Reg.scala 15:16]
  wire  regout_en_7 = reqs_0_bankSel[7] | reqs_1_bankEn[7] | reqs_2_bankEn[7] | reqs_3_bankEn[7] | reqs_4_bankEn[7] |
    reqs_5_bankEn[7]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1_7 = reqs_1_bankSel[7]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2_7 = reqs_2_bankSel[7]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3_7 = reqs_3_bankSel[7]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4_7 = reqs_4_bankSel[7]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_30 = regout_sel_2_7 ? 1'h0 : regout_sel_3_7 | regout_sel_4_7; // @[Mux.scala 47:69]
  wire  regout_wen_7 = reqs_0_bankSel[7] ? io_side_adr_bits_write : regout_sel_1_7 | _regout_wen_T_30; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_28 = regout_sel_4_7 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_29 = regout_sel_3_7 ? reqs_3_index : _regout_idx_T_28; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_30 = regout_sel_2_7 ? reqs_2_index : _regout_idx_T_29; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_31 = regout_sel_1_7 ? reqs_1_index : _regout_idx_T_30; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_28 = regout_sel_4_7 ? sourceD_wreq_words_1 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_29 = regout_sel_3_7 ? sinkD_req_words_1 : _regout_data_T_28; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_30 = regout_sel_2_7 ? error : _regout_data_T_29; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_31 = regout_sel_1_7 ? sinkC_req_words_1 : _regout_data_T_30; // @[Mux.scala 47:69]
  wire  _regout_T_49 = regout_wen_7 & regout_en_7; // @[BankedStore.scala 199:15]
  wire  _regout_T_51 = ~regout_wen_7 & regout_en_7; // @[BankedStore.scala 203:32]
  reg  regout_REG_7; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r_7; // @[Reg.scala 15:16]
  wire  regout_en_8 = reqs_0_bankSel[8] | reqs_1_bankEn[8] | reqs_2_bankEn[8] | reqs_3_bankEn[8] | reqs_4_bankEn[8] |
    reqs_5_bankEn[8]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1_8 = reqs_1_bankSel[8]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2_8 = reqs_2_bankSel[8]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3_8 = reqs_3_bankSel[8]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4_8 = reqs_4_bankSel[8]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_34 = regout_sel_2_8 ? 1'h0 : regout_sel_3_8 | regout_sel_4_8; // @[Mux.scala 47:69]
  wire  regout_wen_8 = reqs_0_bankSel[8] ? io_side_adr_bits_write : regout_sel_1_8 | _regout_wen_T_34; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_32 = regout_sel_4_8 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_33 = regout_sel_3_8 ? reqs_3_index : _regout_idx_T_32; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_34 = regout_sel_2_8 ? reqs_2_index : _regout_idx_T_33; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_35 = regout_sel_1_8 ? reqs_1_index : _regout_idx_T_34; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_32 = regout_sel_4_8 ? sourceD_wreq_words_0 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_33 = regout_sel_3_8 ? sinkD_req_words_0 : _regout_data_T_32; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_34 = regout_sel_2_8 ? error : _regout_data_T_33; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_35 = regout_sel_1_8 ? sinkC_req_words_0 : _regout_data_T_34; // @[Mux.scala 47:69]
  wire  _regout_T_56 = regout_wen_8 & regout_en_8; // @[BankedStore.scala 199:15]
  wire  _regout_T_58 = ~regout_wen_8 & regout_en_8; // @[BankedStore.scala 203:32]
  reg  regout_REG_8; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r_8; // @[Reg.scala 15:16]
  wire  regout_en_9 = reqs_0_bankSel[9] | reqs_1_bankEn[9] | reqs_2_bankEn[9] | reqs_3_bankEn[9] | reqs_4_bankEn[9] |
    reqs_5_bankEn[9]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1_9 = reqs_1_bankSel[9]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2_9 = reqs_2_bankSel[9]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3_9 = reqs_3_bankSel[9]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4_9 = reqs_4_bankSel[9]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_38 = regout_sel_2_9 ? 1'h0 : regout_sel_3_9 | regout_sel_4_9; // @[Mux.scala 47:69]
  wire  regout_wen_9 = reqs_0_bankSel[9] ? io_side_adr_bits_write : regout_sel_1_9 | _regout_wen_T_38; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_36 = regout_sel_4_9 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_37 = regout_sel_3_9 ? reqs_3_index : _regout_idx_T_36; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_38 = regout_sel_2_9 ? reqs_2_index : _regout_idx_T_37; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_39 = regout_sel_1_9 ? reqs_1_index : _regout_idx_T_38; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_36 = regout_sel_4_9 ? sourceD_wreq_words_1 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_37 = regout_sel_3_9 ? sinkD_req_words_1 : _regout_data_T_36; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_38 = regout_sel_2_9 ? error : _regout_data_T_37; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_39 = regout_sel_1_9 ? sinkC_req_words_1 : _regout_data_T_38; // @[Mux.scala 47:69]
  wire  _regout_T_63 = regout_wen_9 & regout_en_9; // @[BankedStore.scala 199:15]
  wire  _regout_T_65 = ~regout_wen_9 & regout_en_9; // @[BankedStore.scala 203:32]
  reg  regout_REG_9; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r_9; // @[Reg.scala 15:16]
  wire  regout_en_10 = reqs_0_bankSel[10] | reqs_1_bankEn[10] | reqs_2_bankEn[10] | reqs_3_bankEn[10] | reqs_4_bankEn[10
    ] | reqs_5_bankEn[10]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1_10 = reqs_1_bankSel[10]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2_10 = reqs_2_bankSel[10]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3_10 = reqs_3_bankSel[10]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4_10 = reqs_4_bankSel[10]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_42 = regout_sel_2_10 ? 1'h0 : regout_sel_3_10 | regout_sel_4_10; // @[Mux.scala 47:69]
  wire  regout_wen_10 = reqs_0_bankSel[10] ? io_side_adr_bits_write : regout_sel_1_10 | _regout_wen_T_42; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_40 = regout_sel_4_10 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_41 = regout_sel_3_10 ? reqs_3_index : _regout_idx_T_40; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_42 = regout_sel_2_10 ? reqs_2_index : _regout_idx_T_41; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_43 = regout_sel_1_10 ? reqs_1_index : _regout_idx_T_42; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_40 = regout_sel_4_10 ? sourceD_wreq_words_0 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_41 = regout_sel_3_10 ? sinkD_req_words_0 : _regout_data_T_40; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_42 = regout_sel_2_10 ? error : _regout_data_T_41; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_43 = regout_sel_1_10 ? sinkC_req_words_0 : _regout_data_T_42; // @[Mux.scala 47:69]
  wire  _regout_T_70 = regout_wen_10 & regout_en_10; // @[BankedStore.scala 199:15]
  wire  _regout_T_72 = ~regout_wen_10 & regout_en_10; // @[BankedStore.scala 203:32]
  reg  regout_REG_10; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r_10; // @[Reg.scala 15:16]
  wire  regout_en_11 = reqs_0_bankSel[11] | reqs_1_bankEn[11] | reqs_2_bankEn[11] | reqs_3_bankEn[11] | reqs_4_bankEn[11
    ] | reqs_5_bankEn[11]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1_11 = reqs_1_bankSel[11]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2_11 = reqs_2_bankSel[11]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3_11 = reqs_3_bankSel[11]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4_11 = reqs_4_bankSel[11]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_46 = regout_sel_2_11 ? 1'h0 : regout_sel_3_11 | regout_sel_4_11; // @[Mux.scala 47:69]
  wire  regout_wen_11 = reqs_0_bankSel[11] ? io_side_adr_bits_write : regout_sel_1_11 | _regout_wen_T_46; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_44 = regout_sel_4_11 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_45 = regout_sel_3_11 ? reqs_3_index : _regout_idx_T_44; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_46 = regout_sel_2_11 ? reqs_2_index : _regout_idx_T_45; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_47 = regout_sel_1_11 ? reqs_1_index : _regout_idx_T_46; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_44 = regout_sel_4_11 ? sourceD_wreq_words_1 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_45 = regout_sel_3_11 ? sinkD_req_words_1 : _regout_data_T_44; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_46 = regout_sel_2_11 ? error : _regout_data_T_45; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_47 = regout_sel_1_11 ? sinkC_req_words_1 : _regout_data_T_46; // @[Mux.scala 47:69]
  wire  _regout_T_77 = regout_wen_11 & regout_en_11; // @[BankedStore.scala 199:15]
  wire  _regout_T_79 = ~regout_wen_11 & regout_en_11; // @[BankedStore.scala 203:32]
  reg  regout_REG_11; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r_11; // @[Reg.scala 15:16]
  wire  regout_en_12 = reqs_0_bankSel[12] | reqs_1_bankEn[12] | reqs_2_bankEn[12] | reqs_3_bankEn[12] | reqs_4_bankEn[12
    ] | reqs_5_bankEn[12]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1_12 = reqs_1_bankSel[12]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2_12 = reqs_2_bankSel[12]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3_12 = reqs_3_bankSel[12]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4_12 = reqs_4_bankSel[12]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_50 = regout_sel_2_12 ? 1'h0 : regout_sel_3_12 | regout_sel_4_12; // @[Mux.scala 47:69]
  wire  regout_wen_12 = reqs_0_bankSel[12] ? io_side_adr_bits_write : regout_sel_1_12 | _regout_wen_T_50; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_48 = regout_sel_4_12 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_49 = regout_sel_3_12 ? reqs_3_index : _regout_idx_T_48; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_50 = regout_sel_2_12 ? reqs_2_index : _regout_idx_T_49; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_51 = regout_sel_1_12 ? reqs_1_index : _regout_idx_T_50; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_48 = regout_sel_4_12 ? sourceD_wreq_words_0 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_49 = regout_sel_3_12 ? sinkD_req_words_0 : _regout_data_T_48; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_50 = regout_sel_2_12 ? error : _regout_data_T_49; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_51 = regout_sel_1_12 ? sinkC_req_words_0 : _regout_data_T_50; // @[Mux.scala 47:69]
  wire  _regout_T_84 = regout_wen_12 & regout_en_12; // @[BankedStore.scala 199:15]
  wire  _regout_T_86 = ~regout_wen_12 & regout_en_12; // @[BankedStore.scala 203:32]
  reg  regout_REG_12; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r_12; // @[Reg.scala 15:16]
  wire  regout_en_13 = reqs_0_bankSel[13] | reqs_1_bankEn[13] | reqs_2_bankEn[13] | reqs_3_bankEn[13] | reqs_4_bankEn[13
    ] | reqs_5_bankEn[13]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1_13 = reqs_1_bankSel[13]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2_13 = reqs_2_bankSel[13]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3_13 = reqs_3_bankSel[13]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4_13 = reqs_4_bankSel[13]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_54 = regout_sel_2_13 ? 1'h0 : regout_sel_3_13 | regout_sel_4_13; // @[Mux.scala 47:69]
  wire  regout_wen_13 = reqs_0_bankSel[13] ? io_side_adr_bits_write : regout_sel_1_13 | _regout_wen_T_54; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_52 = regout_sel_4_13 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_53 = regout_sel_3_13 ? reqs_3_index : _regout_idx_T_52; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_54 = regout_sel_2_13 ? reqs_2_index : _regout_idx_T_53; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_55 = regout_sel_1_13 ? reqs_1_index : _regout_idx_T_54; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_52 = regout_sel_4_13 ? sourceD_wreq_words_1 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_53 = regout_sel_3_13 ? sinkD_req_words_1 : _regout_data_T_52; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_54 = regout_sel_2_13 ? error : _regout_data_T_53; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_55 = regout_sel_1_13 ? sinkC_req_words_1 : _regout_data_T_54; // @[Mux.scala 47:69]
  wire  _regout_T_91 = regout_wen_13 & regout_en_13; // @[BankedStore.scala 199:15]
  wire  _regout_T_93 = ~regout_wen_13 & regout_en_13; // @[BankedStore.scala 203:32]
  reg  regout_REG_13; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r_13; // @[Reg.scala 15:16]
  wire  regout_en_14 = reqs_0_bankSel[14] | reqs_1_bankEn[14] | reqs_2_bankEn[14] | reqs_3_bankEn[14] | reqs_4_bankEn[14
    ] | reqs_5_bankEn[14]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1_14 = reqs_1_bankSel[14]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2_14 = reqs_2_bankSel[14]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3_14 = reqs_3_bankSel[14]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4_14 = reqs_4_bankSel[14]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_58 = regout_sel_2_14 ? 1'h0 : regout_sel_3_14 | regout_sel_4_14; // @[Mux.scala 47:69]
  wire  regout_wen_14 = reqs_0_bankSel[14] ? io_side_adr_bits_write : regout_sel_1_14 | _regout_wen_T_58; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_56 = regout_sel_4_14 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_57 = regout_sel_3_14 ? reqs_3_index : _regout_idx_T_56; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_58 = regout_sel_2_14 ? reqs_2_index : _regout_idx_T_57; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_59 = regout_sel_1_14 ? reqs_1_index : _regout_idx_T_58; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_56 = regout_sel_4_14 ? sourceD_wreq_words_0 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_57 = regout_sel_3_14 ? sinkD_req_words_0 : _regout_data_T_56; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_58 = regout_sel_2_14 ? error : _regout_data_T_57; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_59 = regout_sel_1_14 ? sinkC_req_words_0 : _regout_data_T_58; // @[Mux.scala 47:69]
  wire  _regout_T_98 = regout_wen_14 & regout_en_14; // @[BankedStore.scala 199:15]
  wire  _GEN_153 = regout_wen_14 & regout_en_14 | (regout_wen_13 & regout_en_13 | (regout_wen_12 & regout_en_12 | (
    regout_wen_11 & regout_en_11 | (regout_wen_10 & regout_en_10 | (regout_wen_9 & regout_en_9 | (regout_wen_8 &
    regout_en_8 | (regout_wen_7 & regout_en_7 | (regout_wen_6 & regout_en_6 | (regout_wen_5 & regout_en_5 | (
    regout_wen_4 & regout_en_4 | (regout_wen_3 & regout_en_3 | (regout_wen_2 & regout_en_2 | (regout_wen_1 & regout_en_1
     | regout_wen & regout_en))))))))))))); // @[BankedStore.scala 199:22 BankedStore.scala 200:22]
  wire  _regout_T_100 = ~regout_wen_14 & regout_en_14; // @[BankedStore.scala 203:32]
  reg  regout_REG_14; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r_14; // @[Reg.scala 15:16]
  wire  regout_en_15 = reqs_0_bankSel[15] | reqs_1_bankEn[15] | reqs_2_bankEn[15] | reqs_3_bankEn[15] | reqs_4_bankEn[15
    ] | reqs_5_bankEn[15]; // @[BankedStore.scala 193:45]
  wire  regout_sel_1_15 = reqs_1_bankSel[15]; // @[BankedStore.scala 194:33]
  wire  regout_sel_2_15 = reqs_2_bankSel[15]; // @[BankedStore.scala 194:33]
  wire  regout_sel_3_15 = reqs_3_bankSel[15]; // @[BankedStore.scala 194:33]
  wire  regout_sel_4_15 = reqs_4_bankSel[15]; // @[BankedStore.scala 194:33]
  wire  _regout_wen_T_62 = regout_sel_2_15 ? 1'h0 : regout_sel_3_15 | regout_sel_4_15; // @[Mux.scala 47:69]
  wire  regout_wen_15 = reqs_0_bankSel[15] ? io_side_adr_bits_write : regout_sel_1_15 | _regout_wen_T_62; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_60 = regout_sel_4_15 ? reqs_4_index : reqs_5_index; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_61 = regout_sel_3_15 ? reqs_3_index : _regout_idx_T_60; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_62 = regout_sel_2_15 ? reqs_2_index : _regout_idx_T_61; // @[Mux.scala 47:69]
  wire [11:0] _regout_idx_T_63 = regout_sel_1_15 ? reqs_1_index : _regout_idx_T_62; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_60 = regout_sel_4_15 ? sourceD_wreq_words_1 : error; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_61 = regout_sel_3_15 ? sinkD_req_words_1 : _regout_data_T_60; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_62 = regout_sel_2_15 ? error : _regout_data_T_61; // @[Mux.scala 47:69]
  wire [63:0] _regout_data_T_63 = regout_sel_1_15 ? sinkC_req_words_1 : _regout_data_T_62; // @[Mux.scala 47:69]
  wire  _regout_T_105 = regout_wen_15 & regout_en_15; // @[BankedStore.scala 199:15]
  wire  _regout_T_107 = ~regout_wen_15 & regout_en_15; // @[BankedStore.scala 203:32]
  reg  regout_REG_15; // @[BankedStore.scala 203:47]
  reg [63:0] regout_r_15; // @[Reg.scala 15:16]
  reg [15:0] regsel_side_REG; // @[BankedStore.scala 206:39]
  reg [15:0] regsel_side; // @[BankedStore.scala 206:31]
  reg [15:0] regsel_sourceC_REG; // @[BankedStore.scala 207:39]
  reg [15:0] regsel_sourceC; // @[BankedStore.scala 207:31]
  reg [15:0] regsel_sourceD_REG; // @[BankedStore.scala 208:39]
  reg [15:0] regsel_sourceD; // @[BankedStore.scala 208:31]
  wire [63:0] _T_7 = regsel_sourceC[0] ? regout_r : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_9 = regsel_sourceC[1] ? regout_r_1 : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_11 = regsel_sourceC[2] ? regout_r_2 : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_13 = regsel_sourceC[3] ? regout_r_3 : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_15 = regsel_sourceC[4] ? regout_r_4 : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_17 = regsel_sourceC[5] ? regout_r_5 : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_19 = regsel_sourceC[6] ? regout_r_6 : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_21 = regsel_sourceC[7] ? regout_r_7 : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_23 = regsel_sourceC[8] ? regout_r_8 : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_25 = regsel_sourceC[9] ? regout_r_9 : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_27 = regsel_sourceC[10] ? regout_r_10 : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_29 = regsel_sourceC[11] ? regout_r_11 : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_31 = regsel_sourceC[12] ? regout_r_12 : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_33 = regsel_sourceC[13] ? regout_r_13 : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_35 = regsel_sourceC[14] ? regout_r_14 : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_37 = regsel_sourceC[15] ? regout_r_15 : 64'h0; // @[BankedStore.scala 211:23]
  wire [63:0] _T_38 = _T_7 | _T_11; // @[BankedStore.scala 212:97]
  wire [63:0] _T_39 = _T_38 | _T_15; // @[BankedStore.scala 212:97]
  wire [63:0] _T_40 = _T_39 | _T_19; // @[BankedStore.scala 212:97]
  wire [63:0] _T_41 = _T_40 | _T_23; // @[BankedStore.scala 212:97]
  wire [63:0] _T_42 = _T_41 | _T_27; // @[BankedStore.scala 212:97]
  wire [63:0] _T_43 = _T_42 | _T_31; // @[BankedStore.scala 212:97]
  wire [63:0] io_sourceC_dat_data_lo = _T_43 | _T_35; // @[BankedStore.scala 212:97]
  wire [63:0] _T_45 = _T_9 | _T_13; // @[BankedStore.scala 212:97]
  wire [63:0] _T_46 = _T_45 | _T_17; // @[BankedStore.scala 212:97]
  wire [63:0] _T_47 = _T_46 | _T_21; // @[BankedStore.scala 212:97]
  wire [63:0] _T_48 = _T_47 | _T_25; // @[BankedStore.scala 212:97]
  wire [63:0] _T_49 = _T_48 | _T_29; // @[BankedStore.scala 212:97]
  wire [63:0] _T_50 = _T_49 | _T_33; // @[BankedStore.scala 212:97]
  wire [63:0] io_sourceC_dat_data_hi = _T_50 | _T_37; // @[BankedStore.scala 212:97]
  wire [63:0] _T_53 = regsel_sourceD[0] ? regout_r : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_55 = regsel_sourceD[1] ? regout_r_1 : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_57 = regsel_sourceD[2] ? regout_r_2 : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_59 = regsel_sourceD[3] ? regout_r_3 : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_61 = regsel_sourceD[4] ? regout_r_4 : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_63 = regsel_sourceD[5] ? regout_r_5 : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_65 = regsel_sourceD[6] ? regout_r_6 : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_67 = regsel_sourceD[7] ? regout_r_7 : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_69 = regsel_sourceD[8] ? regout_r_8 : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_71 = regsel_sourceD[9] ? regout_r_9 : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_73 = regsel_sourceD[10] ? regout_r_10 : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_75 = regsel_sourceD[11] ? regout_r_11 : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_77 = regsel_sourceD[12] ? regout_r_12 : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_79 = regsel_sourceD[13] ? regout_r_13 : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_81 = regsel_sourceD[14] ? regout_r_14 : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_83 = regsel_sourceD[15] ? regout_r_15 : 64'h0; // @[BankedStore.scala 220:23]
  wire [63:0] _T_84 = _T_53 | _T_57; // @[BankedStore.scala 221:97]
  wire [63:0] _T_85 = _T_84 | _T_61; // @[BankedStore.scala 221:97]
  wire [63:0] _T_86 = _T_85 | _T_65; // @[BankedStore.scala 221:97]
  wire [63:0] _T_87 = _T_86 | _T_69; // @[BankedStore.scala 221:97]
  wire [63:0] _T_88 = _T_87 | _T_73; // @[BankedStore.scala 221:97]
  wire [63:0] _T_89 = _T_88 | _T_77; // @[BankedStore.scala 221:97]
  wire [63:0] io_sourceD_rdat_data_lo = _T_89 | _T_81; // @[BankedStore.scala 221:97]
  wire [63:0] _T_91 = _T_55 | _T_59; // @[BankedStore.scala 221:97]
  wire [63:0] _T_92 = _T_91 | _T_63; // @[BankedStore.scala 221:97]
  wire [63:0] _T_93 = _T_92 | _T_67; // @[BankedStore.scala 221:97]
  wire [63:0] _T_94 = _T_93 | _T_71; // @[BankedStore.scala 221:97]
  wire [63:0] _T_95 = _T_94 | _T_75; // @[BankedStore.scala 221:97]
  wire [63:0] _T_96 = _T_95 | _T_79; // @[BankedStore.scala 221:97]
  wire [63:0] io_sourceD_rdat_data_hi = _T_96 | _T_83; // @[BankedStore.scala 221:97]
  wire [63:0] _T_99 = regsel_side[0] ? regout_r : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_101 = regsel_side[1] ? regout_r_1 : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_103 = regsel_side[2] ? regout_r_2 : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_105 = regsel_side[3] ? regout_r_3 : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_107 = regsel_side[4] ? regout_r_4 : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_109 = regsel_side[5] ? regout_r_5 : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_111 = regsel_side[6] ? regout_r_6 : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_113 = regsel_side[7] ? regout_r_7 : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_115 = regsel_side[8] ? regout_r_8 : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_117 = regsel_side[9] ? regout_r_9 : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_119 = regsel_side[10] ? regout_r_10 : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_121 = regsel_side[11] ? regout_r_11 : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_123 = regsel_side[12] ? regout_r_12 : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_125 = regsel_side[13] ? regout_r_13 : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_127 = regsel_side[14] ? regout_r_14 : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_129 = regsel_side[15] ? regout_r_15 : 64'h0; // @[BankedStore.scala 229:23]
  wire [63:0] _T_130 = _T_99 | _T_103; // @[BankedStore.scala 230:97]
  wire [63:0] _T_131 = _T_130 | _T_107; // @[BankedStore.scala 230:97]
  wire [63:0] _T_132 = _T_131 | _T_111; // @[BankedStore.scala 230:97]
  wire [63:0] _T_133 = _T_132 | _T_115; // @[BankedStore.scala 230:97]
  wire [63:0] _T_134 = _T_133 | _T_119; // @[BankedStore.scala 230:97]
  wire [63:0] _T_135 = _T_134 | _T_123; // @[BankedStore.scala 230:97]
  wire [63:0] io_side_rdat_data_lo = _T_135 | _T_127; // @[BankedStore.scala 230:97]
  wire [63:0] _T_137 = _T_101 | _T_105; // @[BankedStore.scala 230:97]
  wire [63:0] _T_138 = _T_137 | _T_109; // @[BankedStore.scala 230:97]
  wire [63:0] _T_139 = _T_138 | _T_113; // @[BankedStore.scala 230:97]
  wire [63:0] _T_140 = _T_139 | _T_117; // @[BankedStore.scala 230:97]
  wire [63:0] _T_141 = _T_140 | _T_121; // @[BankedStore.scala 230:97]
  wire [63:0] _T_142 = _T_141 | _T_125; // @[BankedStore.scala 230:97]
  wire [63:0] io_side_rdat_data_hi = _T_142 | _T_129; // @[BankedStore.scala 230:97]
  RHEA__cc_banks_0 cc_banks_0 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_0_RW0_addr),
    .RW0_en(cc_banks_0_RW0_en),
    .RW0_clk(cc_banks_0_RW0_clk),
    .RW0_wmode(cc_banks_0_RW0_wmode),
    .RW0_wdata(cc_banks_0_RW0_wdata),
    .RW0_rdata(cc_banks_0_RW0_rdata)
  );
  RHEA__cc_banks_0 cc_banks_1 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_1_RW0_addr),
    .RW0_en(cc_banks_1_RW0_en),
    .RW0_clk(cc_banks_1_RW0_clk),
    .RW0_wmode(cc_banks_1_RW0_wmode),
    .RW0_wdata(cc_banks_1_RW0_wdata),
    .RW0_rdata(cc_banks_1_RW0_rdata)
  );
  RHEA__cc_banks_0 cc_banks_2 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_2_RW0_addr),
    .RW0_en(cc_banks_2_RW0_en),
    .RW0_clk(cc_banks_2_RW0_clk),
    .RW0_wmode(cc_banks_2_RW0_wmode),
    .RW0_wdata(cc_banks_2_RW0_wdata),
    .RW0_rdata(cc_banks_2_RW0_rdata)
  );
  RHEA__cc_banks_0 cc_banks_3 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_3_RW0_addr),
    .RW0_en(cc_banks_3_RW0_en),
    .RW0_clk(cc_banks_3_RW0_clk),
    .RW0_wmode(cc_banks_3_RW0_wmode),
    .RW0_wdata(cc_banks_3_RW0_wdata),
    .RW0_rdata(cc_banks_3_RW0_rdata)
  );
  RHEA__cc_banks_0 cc_banks_4 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_4_RW0_addr),
    .RW0_en(cc_banks_4_RW0_en),
    .RW0_clk(cc_banks_4_RW0_clk),
    .RW0_wmode(cc_banks_4_RW0_wmode),
    .RW0_wdata(cc_banks_4_RW0_wdata),
    .RW0_rdata(cc_banks_4_RW0_rdata)
  );
  RHEA__cc_banks_0 cc_banks_5 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_5_RW0_addr),
    .RW0_en(cc_banks_5_RW0_en),
    .RW0_clk(cc_banks_5_RW0_clk),
    .RW0_wmode(cc_banks_5_RW0_wmode),
    .RW0_wdata(cc_banks_5_RW0_wdata),
    .RW0_rdata(cc_banks_5_RW0_rdata)
  );
  RHEA__cc_banks_0 cc_banks_6 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_6_RW0_addr),
    .RW0_en(cc_banks_6_RW0_en),
    .RW0_clk(cc_banks_6_RW0_clk),
    .RW0_wmode(cc_banks_6_RW0_wmode),
    .RW0_wdata(cc_banks_6_RW0_wdata),
    .RW0_rdata(cc_banks_6_RW0_rdata)
  );
  RHEA__cc_banks_0 cc_banks_7 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_7_RW0_addr),
    .RW0_en(cc_banks_7_RW0_en),
    .RW0_clk(cc_banks_7_RW0_clk),
    .RW0_wmode(cc_banks_7_RW0_wmode),
    .RW0_wdata(cc_banks_7_RW0_wdata),
    .RW0_rdata(cc_banks_7_RW0_rdata)
  );
  RHEA__cc_banks_0 cc_banks_8 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_8_RW0_addr),
    .RW0_en(cc_banks_8_RW0_en),
    .RW0_clk(cc_banks_8_RW0_clk),
    .RW0_wmode(cc_banks_8_RW0_wmode),
    .RW0_wdata(cc_banks_8_RW0_wdata),
    .RW0_rdata(cc_banks_8_RW0_rdata)
  );
  RHEA__cc_banks_0 cc_banks_9 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_9_RW0_addr),
    .RW0_en(cc_banks_9_RW0_en),
    .RW0_clk(cc_banks_9_RW0_clk),
    .RW0_wmode(cc_banks_9_RW0_wmode),
    .RW0_wdata(cc_banks_9_RW0_wdata),
    .RW0_rdata(cc_banks_9_RW0_rdata)
  );
  RHEA__cc_banks_0 cc_banks_10 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_10_RW0_addr),
    .RW0_en(cc_banks_10_RW0_en),
    .RW0_clk(cc_banks_10_RW0_clk),
    .RW0_wmode(cc_banks_10_RW0_wmode),
    .RW0_wdata(cc_banks_10_RW0_wdata),
    .RW0_rdata(cc_banks_10_RW0_rdata)
  );
  RHEA__cc_banks_0 cc_banks_11 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_11_RW0_addr),
    .RW0_en(cc_banks_11_RW0_en),
    .RW0_clk(cc_banks_11_RW0_clk),
    .RW0_wmode(cc_banks_11_RW0_wmode),
    .RW0_wdata(cc_banks_11_RW0_wdata),
    .RW0_rdata(cc_banks_11_RW0_rdata)
  );
  RHEA__cc_banks_0 cc_banks_12 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_12_RW0_addr),
    .RW0_en(cc_banks_12_RW0_en),
    .RW0_clk(cc_banks_12_RW0_clk),
    .RW0_wmode(cc_banks_12_RW0_wmode),
    .RW0_wdata(cc_banks_12_RW0_wdata),
    .RW0_rdata(cc_banks_12_RW0_rdata)
  );
  RHEA__cc_banks_0 cc_banks_13 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_13_RW0_addr),
    .RW0_en(cc_banks_13_RW0_en),
    .RW0_clk(cc_banks_13_RW0_clk),
    .RW0_wmode(cc_banks_13_RW0_wmode),
    .RW0_wdata(cc_banks_13_RW0_wdata),
    .RW0_rdata(cc_banks_13_RW0_rdata)
  );
  RHEA__cc_banks_0 cc_banks_14 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_14_RW0_addr),
    .RW0_en(cc_banks_14_RW0_en),
    .RW0_clk(cc_banks_14_RW0_clk),
    .RW0_wmode(cc_banks_14_RW0_wmode),
    .RW0_wdata(cc_banks_14_RW0_wdata),
    .RW0_rdata(cc_banks_14_RW0_rdata)
  );
  RHEA__cc_banks_0 cc_banks_15 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(cc_banks_15_RW0_addr),
    .RW0_en(cc_banks_15_RW0_en),
    .RW0_clk(cc_banks_15_RW0_clk),
    .RW0_wmode(cc_banks_15_RW0_wmode),
    .RW0_wdata(cc_banks_15_RW0_wdata),
    .RW0_rdata(cc_banks_15_RW0_rdata)
  );
  assign io_side_adr_ready = _side_req_io_side_adr_ready_T_1[0]; // @[BankedStore.scala 157:21]
  assign io_side_rdat_data = {io_side_rdat_data_hi,io_side_rdat_data_lo}; // @[Cat.scala 30:58]
  assign io_sinkC_adr_ready = _sinkC_req_io_sinkC_adr_ready_T_1[0]; // @[BankedStore.scala 157:21]
  assign io_sinkD_adr_ready = _sinkD_req_io_sinkD_adr_ready_T_1[0]; // @[BankedStore.scala 157:21]
  assign io_sourceC_adr_ready = _sourceC_req_io_sourceC_adr_ready_T_1[0]; // @[BankedStore.scala 157:21]
  assign io_sourceC_dat_data = {io_sourceC_dat_data_hi,io_sourceC_dat_data_lo}; // @[Cat.scala 30:58]
  assign io_sourceD_radr_ready = _sourceD_rreq_io_sourceD_radr_ready_T_1[0]; // @[BankedStore.scala 157:21]
  assign io_sourceD_rdat_data = {io_sourceD_rdat_data_hi,io_sourceD_rdat_data_lo}; // @[Cat.scala 30:58]
  assign io_sourceD_wadr_ready = _sourceD_wreq_io_sourceD_wadr_ready_T_1[0]; // @[BankedStore.scala 157:21]
  assign io_error_ready = regout_wen_15 & regout_en_15 | _GEN_153; // @[BankedStore.scala 199:22 BankedStore.scala 200:22]
  assign cc_banks_0_RW0_wdata = reqs_0_bankSel[0] ? side_req_words_0 : _regout_data_T_3; // @[Mux.scala 47:69]
  assign cc_banks_1_RW0_wdata = reqs_0_bankSel[1] ? side_req_words_1 : _regout_data_T_7; // @[Mux.scala 47:69]
  assign cc_banks_2_RW0_wdata = reqs_0_bankSel[2] ? side_req_words_0 : _regout_data_T_11; // @[Mux.scala 47:69]
  assign cc_banks_3_RW0_wdata = reqs_0_bankSel[3] ? side_req_words_1 : _regout_data_T_15; // @[Mux.scala 47:69]
  assign cc_banks_4_RW0_wdata = reqs_0_bankSel[4] ? side_req_words_0 : _regout_data_T_19; // @[Mux.scala 47:69]
  assign cc_banks_5_RW0_wdata = reqs_0_bankSel[5] ? side_req_words_1 : _regout_data_T_23; // @[Mux.scala 47:69]
  assign cc_banks_6_RW0_wdata = reqs_0_bankSel[6] ? side_req_words_0 : _regout_data_T_27; // @[Mux.scala 47:69]
  assign cc_banks_7_RW0_wdata = reqs_0_bankSel[7] ? side_req_words_1 : _regout_data_T_31; // @[Mux.scala 47:69]
  assign cc_banks_8_RW0_wdata = reqs_0_bankSel[8] ? side_req_words_0 : _regout_data_T_35; // @[Mux.scala 47:69]
  assign cc_banks_9_RW0_wdata = reqs_0_bankSel[9] ? side_req_words_1 : _regout_data_T_39; // @[Mux.scala 47:69]
  assign cc_banks_10_RW0_wdata = reqs_0_bankSel[10] ? side_req_words_0 : _regout_data_T_43; // @[Mux.scala 47:69]
  assign cc_banks_11_RW0_wdata = reqs_0_bankSel[11] ? side_req_words_1 : _regout_data_T_47; // @[Mux.scala 47:69]
  assign cc_banks_12_RW0_wdata = reqs_0_bankSel[12] ? side_req_words_0 : _regout_data_T_51; // @[Mux.scala 47:69]
  assign cc_banks_13_RW0_wdata = reqs_0_bankSel[13] ? side_req_words_1 : _regout_data_T_55; // @[Mux.scala 47:69]
  assign cc_banks_14_RW0_wdata = reqs_0_bankSel[14] ? side_req_words_0 : _regout_data_T_59; // @[Mux.scala 47:69]
  assign cc_banks_15_RW0_wdata = reqs_0_bankSel[15] ? side_req_words_1 : _regout_data_T_63; // @[Mux.scala 47:69]
  assign cc_banks_0_RW0_wmode = reqs_0_bankSel[0] ? io_side_adr_bits_write : regout_sel_1 | _regout_wen_T_2; // @[Mux.scala 47:69]
  assign cc_banks_0_RW0_clk = clock;
  assign cc_banks_0_RW0_en = _regout_T_2 | _regout_T;
  assign cc_banks_0_RW0_addr = reqs_0_bankSel[0] ? reqs_0_index : _regout_idx_T_3; // @[Mux.scala 47:69]
  assign cc_banks_1_RW0_wmode = reqs_0_bankSel[1] ? io_side_adr_bits_write : regout_sel_1_1 | _regout_wen_T_6; // @[Mux.scala 47:69]
  assign cc_banks_1_RW0_clk = clock;
  assign cc_banks_1_RW0_en = _regout_T_9 | _regout_T_7;
  assign cc_banks_1_RW0_addr = reqs_0_bankSel[1] ? reqs_0_index : _regout_idx_T_7; // @[Mux.scala 47:69]
  assign cc_banks_2_RW0_wmode = reqs_0_bankSel[2] ? io_side_adr_bits_write : regout_sel_1_2 | _regout_wen_T_10; // @[Mux.scala 47:69]
  assign cc_banks_2_RW0_clk = clock;
  assign cc_banks_2_RW0_en = _regout_T_16 | _regout_T_14;
  assign cc_banks_2_RW0_addr = reqs_0_bankSel[2] ? reqs_0_index : _regout_idx_T_11; // @[Mux.scala 47:69]
  assign cc_banks_3_RW0_wmode = reqs_0_bankSel[3] ? io_side_adr_bits_write : regout_sel_1_3 | _regout_wen_T_14; // @[Mux.scala 47:69]
  assign cc_banks_3_RW0_clk = clock;
  assign cc_banks_3_RW0_en = _regout_T_23 | _regout_T_21;
  assign cc_banks_3_RW0_addr = reqs_0_bankSel[3] ? reqs_0_index : _regout_idx_T_15; // @[Mux.scala 47:69]
  assign cc_banks_4_RW0_wmode = reqs_0_bankSel[4] ? io_side_adr_bits_write : regout_sel_1_4 | _regout_wen_T_18; // @[Mux.scala 47:69]
  assign cc_banks_4_RW0_clk = clock;
  assign cc_banks_4_RW0_en = _regout_T_30 | _regout_T_28;
  assign cc_banks_4_RW0_addr = reqs_0_bankSel[4] ? reqs_0_index : _regout_idx_T_19; // @[Mux.scala 47:69]
  assign cc_banks_5_RW0_wmode = reqs_0_bankSel[5] ? io_side_adr_bits_write : regout_sel_1_5 | _regout_wen_T_22; // @[Mux.scala 47:69]
  assign cc_banks_5_RW0_clk = clock;
  assign cc_banks_5_RW0_en = _regout_T_37 | _regout_T_35;
  assign cc_banks_5_RW0_addr = reqs_0_bankSel[5] ? reqs_0_index : _regout_idx_T_23; // @[Mux.scala 47:69]
  assign cc_banks_6_RW0_wmode = reqs_0_bankSel[6] ? io_side_adr_bits_write : regout_sel_1_6 | _regout_wen_T_26; // @[Mux.scala 47:69]
  assign cc_banks_6_RW0_clk = clock;
  assign cc_banks_6_RW0_en = _regout_T_44 | _regout_T_42;
  assign cc_banks_6_RW0_addr = reqs_0_bankSel[6] ? reqs_0_index : _regout_idx_T_27; // @[Mux.scala 47:69]
  assign cc_banks_7_RW0_wmode = reqs_0_bankSel[7] ? io_side_adr_bits_write : regout_sel_1_7 | _regout_wen_T_30; // @[Mux.scala 47:69]
  assign cc_banks_7_RW0_clk = clock;
  assign cc_banks_7_RW0_en = _regout_T_51 | _regout_T_49;
  assign cc_banks_7_RW0_addr = reqs_0_bankSel[7] ? reqs_0_index : _regout_idx_T_31; // @[Mux.scala 47:69]
  assign cc_banks_8_RW0_wmode = reqs_0_bankSel[8] ? io_side_adr_bits_write : regout_sel_1_8 | _regout_wen_T_34; // @[Mux.scala 47:69]
  assign cc_banks_8_RW0_clk = clock;
  assign cc_banks_8_RW0_en = _regout_T_58 | _regout_T_56;
  assign cc_banks_8_RW0_addr = reqs_0_bankSel[8] ? reqs_0_index : _regout_idx_T_35; // @[Mux.scala 47:69]
  assign cc_banks_9_RW0_wmode = reqs_0_bankSel[9] ? io_side_adr_bits_write : regout_sel_1_9 | _regout_wen_T_38; // @[Mux.scala 47:69]
  assign cc_banks_9_RW0_clk = clock;
  assign cc_banks_9_RW0_en = _regout_T_65 | _regout_T_63;
  assign cc_banks_9_RW0_addr = reqs_0_bankSel[9] ? reqs_0_index : _regout_idx_T_39; // @[Mux.scala 47:69]
  assign cc_banks_10_RW0_wmode = reqs_0_bankSel[10] ? io_side_adr_bits_write : regout_sel_1_10 | _regout_wen_T_42; // @[Mux.scala 47:69]
  assign cc_banks_10_RW0_clk = clock;
  assign cc_banks_10_RW0_en = _regout_T_72 | _regout_T_70;
  assign cc_banks_10_RW0_addr = reqs_0_bankSel[10] ? reqs_0_index : _regout_idx_T_43; // @[Mux.scala 47:69]
  assign cc_banks_11_RW0_wmode = reqs_0_bankSel[11] ? io_side_adr_bits_write : regout_sel_1_11 | _regout_wen_T_46; // @[Mux.scala 47:69]
  assign cc_banks_11_RW0_clk = clock;
  assign cc_banks_11_RW0_en = _regout_T_79 | _regout_T_77;
  assign cc_banks_11_RW0_addr = reqs_0_bankSel[11] ? reqs_0_index : _regout_idx_T_47; // @[Mux.scala 47:69]
  assign cc_banks_12_RW0_wmode = reqs_0_bankSel[12] ? io_side_adr_bits_write : regout_sel_1_12 | _regout_wen_T_50; // @[Mux.scala 47:69]
  assign cc_banks_12_RW0_clk = clock;
  assign cc_banks_12_RW0_en = _regout_T_86 | _regout_T_84;
  assign cc_banks_12_RW0_addr = reqs_0_bankSel[12] ? reqs_0_index : _regout_idx_T_51; // @[Mux.scala 47:69]
  assign cc_banks_13_RW0_wmode = reqs_0_bankSel[13] ? io_side_adr_bits_write : regout_sel_1_13 | _regout_wen_T_54; // @[Mux.scala 47:69]
  assign cc_banks_13_RW0_clk = clock;
  assign cc_banks_13_RW0_en = _regout_T_93 | _regout_T_91;
  assign cc_banks_13_RW0_addr = reqs_0_bankSel[13] ? reqs_0_index : _regout_idx_T_55; // @[Mux.scala 47:69]
  assign cc_banks_14_RW0_wmode = reqs_0_bankSel[14] ? io_side_adr_bits_write : regout_sel_1_14 | _regout_wen_T_58; // @[Mux.scala 47:69]
  assign cc_banks_14_RW0_clk = clock;
  assign cc_banks_14_RW0_en = _regout_T_100 | _regout_T_98;
  assign cc_banks_14_RW0_addr = reqs_0_bankSel[14] ? reqs_0_index : _regout_idx_T_59; // @[Mux.scala 47:69]
  assign cc_banks_15_RW0_wmode = reqs_0_bankSel[15] ? io_side_adr_bits_write : regout_sel_1_15 | _regout_wen_T_62; // @[Mux.scala 47:69]
  assign cc_banks_15_RW0_clk = clock;
  assign cc_banks_15_RW0_en = _regout_T_107 | _regout_T_105;
  assign cc_banks_15_RW0_addr = reqs_0_bankSel[15] ? reqs_0_index : _regout_idx_T_63; // @[Mux.scala 47:69]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG <= 1'h0;
    end else begin
      regout_REG <= ~regout_wen & regout_en;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r <= 64'h0;
    end else if (regout_REG) begin
      regout_r <= cc_banks_0_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG_1 <= 1'h0;
    end else begin
      regout_REG_1 <= ~regout_wen_1 & regout_en_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r_1 <= 64'h0;
    end else if (regout_REG_1) begin
      regout_r_1 <= cc_banks_1_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG_2 <= 1'h0;
    end else begin
      regout_REG_2 <= ~regout_wen_2 & regout_en_2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r_2 <= 64'h0;
    end else if (regout_REG_2) begin
      regout_r_2 <= cc_banks_2_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG_3 <= 1'h0;
    end else begin
      regout_REG_3 <= ~regout_wen_3 & regout_en_3;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r_3 <= 64'h0;
    end else if (regout_REG_3) begin
      regout_r_3 <= cc_banks_3_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG_4 <= 1'h0;
    end else begin
      regout_REG_4 <= ~regout_wen_4 & regout_en_4;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r_4 <= 64'h0;
    end else if (regout_REG_4) begin
      regout_r_4 <= cc_banks_4_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG_5 <= 1'h0;
    end else begin
      regout_REG_5 <= ~regout_wen_5 & regout_en_5;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r_5 <= 64'h0;
    end else if (regout_REG_5) begin
      regout_r_5 <= cc_banks_5_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG_6 <= 1'h0;
    end else begin
      regout_REG_6 <= ~regout_wen_6 & regout_en_6;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r_6 <= 64'h0;
    end else if (regout_REG_6) begin
      regout_r_6 <= cc_banks_6_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG_7 <= 1'h0;
    end else begin
      regout_REG_7 <= ~regout_wen_7 & regout_en_7;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r_7 <= 64'h0;
    end else if (regout_REG_7) begin
      regout_r_7 <= cc_banks_7_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG_8 <= 1'h0;
    end else begin
      regout_REG_8 <= ~regout_wen_8 & regout_en_8;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r_8 <= 64'h0;
    end else if (regout_REG_8) begin
      regout_r_8 <= cc_banks_8_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG_9 <= 1'h0;
    end else begin
      regout_REG_9 <= ~regout_wen_9 & regout_en_9;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r_9 <= 64'h0;
    end else if (regout_REG_9) begin
      regout_r_9 <= cc_banks_9_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG_10 <= 1'h0;
    end else begin
      regout_REG_10 <= ~regout_wen_10 & regout_en_10;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r_10 <= 64'h0;
    end else if (regout_REG_10) begin
      regout_r_10 <= cc_banks_10_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG_11 <= 1'h0;
    end else begin
      regout_REG_11 <= ~regout_wen_11 & regout_en_11;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r_11 <= 64'h0;
    end else if (regout_REG_11) begin
      regout_r_11 <= cc_banks_11_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG_12 <= 1'h0;
    end else begin
      regout_REG_12 <= ~regout_wen_12 & regout_en_12;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r_12 <= 64'h0;
    end else if (regout_REG_12) begin
      regout_r_12 <= cc_banks_12_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG_13 <= 1'h0;
    end else begin
      regout_REG_13 <= ~regout_wen_13 & regout_en_13;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r_13 <= 64'h0;
    end else if (regout_REG_13) begin
      regout_r_13 <= cc_banks_13_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG_14 <= 1'h0;
    end else begin
      regout_REG_14 <= ~regout_wen_14 & regout_en_14;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r_14 <= 64'h0;
    end else if (regout_REG_14) begin
      regout_r_14 <= cc_banks_14_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_REG_15 <= 1'h0;
    end else begin
      regout_REG_15 <= ~regout_wen_15 & regout_en_15;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regout_r_15 <= 64'h0;
    end else if (regout_REG_15) begin
      regout_r_15 <= cc_banks_15_RW0_rdata;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regsel_side_REG <= 16'h0;
    end else if (io_side_adr_valid) begin
      regsel_side_REG <= _side_req_out_bankSel_T_18;
    end else begin
      regsel_side_REG <= 16'h0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regsel_side <= 16'h0;
    end else begin
      regsel_side <= regsel_side_REG;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regsel_sourceC_REG <= 16'h0;
    end else begin
      regsel_sourceC_REG <= reqs_2_bankSel & _sourceC_req_out_bankEn_T_16;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regsel_sourceC <= 16'h0;
    end else begin
      regsel_sourceC <= regsel_sourceC_REG;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regsel_sourceD_REG <= 16'h0;
    end else begin
      regsel_sourceD_REG <= reqs_5_bankSel & _sourceD_rreq_out_bankEn_T_16;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      regsel_sourceD <= 16'h0;
    end else begin
      regsel_sourceD <= regsel_sourceD_REG;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  regout_REG = _RAND_0[0:0];
  _RAND_1 = {2{`RANDOM}};
  regout_r = _RAND_1[63:0];
  _RAND_2 = {1{`RANDOM}};
  regout_REG_1 = _RAND_2[0:0];
  _RAND_3 = {2{`RANDOM}};
  regout_r_1 = _RAND_3[63:0];
  _RAND_4 = {1{`RANDOM}};
  regout_REG_2 = _RAND_4[0:0];
  _RAND_5 = {2{`RANDOM}};
  regout_r_2 = _RAND_5[63:0];
  _RAND_6 = {1{`RANDOM}};
  regout_REG_3 = _RAND_6[0:0];
  _RAND_7 = {2{`RANDOM}};
  regout_r_3 = _RAND_7[63:0];
  _RAND_8 = {1{`RANDOM}};
  regout_REG_4 = _RAND_8[0:0];
  _RAND_9 = {2{`RANDOM}};
  regout_r_4 = _RAND_9[63:0];
  _RAND_10 = {1{`RANDOM}};
  regout_REG_5 = _RAND_10[0:0];
  _RAND_11 = {2{`RANDOM}};
  regout_r_5 = _RAND_11[63:0];
  _RAND_12 = {1{`RANDOM}};
  regout_REG_6 = _RAND_12[0:0];
  _RAND_13 = {2{`RANDOM}};
  regout_r_6 = _RAND_13[63:0];
  _RAND_14 = {1{`RANDOM}};
  regout_REG_7 = _RAND_14[0:0];
  _RAND_15 = {2{`RANDOM}};
  regout_r_7 = _RAND_15[63:0];
  _RAND_16 = {1{`RANDOM}};
  regout_REG_8 = _RAND_16[0:0];
  _RAND_17 = {2{`RANDOM}};
  regout_r_8 = _RAND_17[63:0];
  _RAND_18 = {1{`RANDOM}};
  regout_REG_9 = _RAND_18[0:0];
  _RAND_19 = {2{`RANDOM}};
  regout_r_9 = _RAND_19[63:0];
  _RAND_20 = {1{`RANDOM}};
  regout_REG_10 = _RAND_20[0:0];
  _RAND_21 = {2{`RANDOM}};
  regout_r_10 = _RAND_21[63:0];
  _RAND_22 = {1{`RANDOM}};
  regout_REG_11 = _RAND_22[0:0];
  _RAND_23 = {2{`RANDOM}};
  regout_r_11 = _RAND_23[63:0];
  _RAND_24 = {1{`RANDOM}};
  regout_REG_12 = _RAND_24[0:0];
  _RAND_25 = {2{`RANDOM}};
  regout_r_12 = _RAND_25[63:0];
  _RAND_26 = {1{`RANDOM}};
  regout_REG_13 = _RAND_26[0:0];
  _RAND_27 = {2{`RANDOM}};
  regout_r_13 = _RAND_27[63:0];
  _RAND_28 = {1{`RANDOM}};
  regout_REG_14 = _RAND_28[0:0];
  _RAND_29 = {2{`RANDOM}};
  regout_r_14 = _RAND_29[63:0];
  _RAND_30 = {1{`RANDOM}};
  regout_REG_15 = _RAND_30[0:0];
  _RAND_31 = {2{`RANDOM}};
  regout_r_15 = _RAND_31[63:0];
  _RAND_32 = {1{`RANDOM}};
  regsel_side_REG = _RAND_32[15:0];
  _RAND_33 = {1{`RANDOM}};
  regsel_side = _RAND_33[15:0];
  _RAND_34 = {1{`RANDOM}};
  regsel_sourceC_REG = _RAND_34[15:0];
  _RAND_35 = {1{`RANDOM}};
  regsel_sourceC = _RAND_35[15:0];
  _RAND_36 = {1{`RANDOM}};
  regsel_sourceD_REG = _RAND_36[15:0];
  _RAND_37 = {1{`RANDOM}};
  regsel_sourceD = _RAND_37[15:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    regout_REG = 1'h0;
  end
  if (rf_reset) begin
    regout_r = 64'h0;
  end
  if (rf_reset) begin
    regout_REG_1 = 1'h0;
  end
  if (rf_reset) begin
    regout_r_1 = 64'h0;
  end
  if (rf_reset) begin
    regout_REG_2 = 1'h0;
  end
  if (rf_reset) begin
    regout_r_2 = 64'h0;
  end
  if (rf_reset) begin
    regout_REG_3 = 1'h0;
  end
  if (rf_reset) begin
    regout_r_3 = 64'h0;
  end
  if (rf_reset) begin
    regout_REG_4 = 1'h0;
  end
  if (rf_reset) begin
    regout_r_4 = 64'h0;
  end
  if (rf_reset) begin
    regout_REG_5 = 1'h0;
  end
  if (rf_reset) begin
    regout_r_5 = 64'h0;
  end
  if (rf_reset) begin
    regout_REG_6 = 1'h0;
  end
  if (rf_reset) begin
    regout_r_6 = 64'h0;
  end
  if (rf_reset) begin
    regout_REG_7 = 1'h0;
  end
  if (rf_reset) begin
    regout_r_7 = 64'h0;
  end
  if (rf_reset) begin
    regout_REG_8 = 1'h0;
  end
  if (rf_reset) begin
    regout_r_8 = 64'h0;
  end
  if (rf_reset) begin
    regout_REG_9 = 1'h0;
  end
  if (rf_reset) begin
    regout_r_9 = 64'h0;
  end
  if (rf_reset) begin
    regout_REG_10 = 1'h0;
  end
  if (rf_reset) begin
    regout_r_10 = 64'h0;
  end
  if (rf_reset) begin
    regout_REG_11 = 1'h0;
  end
  if (rf_reset) begin
    regout_r_11 = 64'h0;
  end
  if (rf_reset) begin
    regout_REG_12 = 1'h0;
  end
  if (rf_reset) begin
    regout_r_12 = 64'h0;
  end
  if (rf_reset) begin
    regout_REG_13 = 1'h0;
  end
  if (rf_reset) begin
    regout_r_13 = 64'h0;
  end
  if (rf_reset) begin
    regout_REG_14 = 1'h0;
  end
  if (rf_reset) begin
    regout_r_14 = 64'h0;
  end
  if (rf_reset) begin
    regout_REG_15 = 1'h0;
  end
  if (rf_reset) begin
    regout_r_15 = 64'h0;
  end
  if (rf_reset) begin
    regsel_side_REG = 16'h0;
  end
  if (rf_reset) begin
    regsel_side = 16'h0;
  end
  if (rf_reset) begin
    regsel_sourceC_REG = 16'h0;
  end
  if (rf_reset) begin
    regsel_sourceC = 16'h0;
  end
  if (rf_reset) begin
    regsel_sourceD_REG = 16'h0;
  end
  if (rf_reset) begin
    regsel_sourceD = 16'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
