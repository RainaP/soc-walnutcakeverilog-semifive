//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLInterconnectCoupler_20(
  input         rf_reset,
  input         clock,
  input         reset,
  input         auto_fragmenter_out_a_ready,
  output        auto_fragmenter_out_a_valid,
  output [2:0]  auto_fragmenter_out_a_bits_opcode,
  output [2:0]  auto_fragmenter_out_a_bits_param,
  output [1:0]  auto_fragmenter_out_a_bits_size,
  output [10:0] auto_fragmenter_out_a_bits_source,
  output [28:0] auto_fragmenter_out_a_bits_address,
  output [7:0]  auto_fragmenter_out_a_bits_mask,
  output [63:0] auto_fragmenter_out_a_bits_data,
  output        auto_fragmenter_out_a_bits_corrupt,
  output        auto_fragmenter_out_d_ready,
  input         auto_fragmenter_out_d_valid,
  input  [2:0]  auto_fragmenter_out_d_bits_opcode,
  input  [1:0]  auto_fragmenter_out_d_bits_size,
  input  [10:0] auto_fragmenter_out_d_bits_source,
  input  [63:0] auto_fragmenter_out_d_bits_data,
  output        auto_tl_in_a_ready,
  input         auto_tl_in_a_valid,
  input  [2:0]  auto_tl_in_a_bits_opcode,
  input  [2:0]  auto_tl_in_a_bits_param,
  input  [2:0]  auto_tl_in_a_bits_size,
  input  [6:0]  auto_tl_in_a_bits_source,
  input  [28:0] auto_tl_in_a_bits_address,
  input  [7:0]  auto_tl_in_a_bits_mask,
  input  [63:0] auto_tl_in_a_bits_data,
  input         auto_tl_in_a_bits_corrupt,
  input         auto_tl_in_d_ready,
  output        auto_tl_in_d_valid,
  output [2:0]  auto_tl_in_d_bits_opcode,
  output [2:0]  auto_tl_in_d_bits_size,
  output [6:0]  auto_tl_in_d_bits_source,
  output [63:0] auto_tl_in_d_bits_data
);
  wire  fragmenter_rf_reset; // @[Fragmenter.scala 333:34]
  wire  fragmenter_clock; // @[Fragmenter.scala 333:34]
  wire  fragmenter_reset; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_a_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_a_bits_param; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_a_bits_size; // @[Fragmenter.scala 333:34]
  wire [6:0] fragmenter_auto_in_a_bits_source; // @[Fragmenter.scala 333:34]
  wire [28:0] fragmenter_auto_in_a_bits_address; // @[Fragmenter.scala 333:34]
  wire [7:0] fragmenter_auto_in_a_bits_mask; // @[Fragmenter.scala 333:34]
  wire [63:0] fragmenter_auto_in_a_bits_data; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_bits_corrupt; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_d_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_d_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_d_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_d_bits_size; // @[Fragmenter.scala 333:34]
  wire [6:0] fragmenter_auto_in_d_bits_source; // @[Fragmenter.scala 333:34]
  wire [63:0] fragmenter_auto_in_d_bits_data; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_a_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_a_bits_param; // @[Fragmenter.scala 333:34]
  wire [1:0] fragmenter_auto_out_a_bits_size; // @[Fragmenter.scala 333:34]
  wire [10:0] fragmenter_auto_out_a_bits_source; // @[Fragmenter.scala 333:34]
  wire [28:0] fragmenter_auto_out_a_bits_address; // @[Fragmenter.scala 333:34]
  wire [7:0] fragmenter_auto_out_a_bits_mask; // @[Fragmenter.scala 333:34]
  wire [63:0] fragmenter_auto_out_a_bits_data; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_bits_corrupt; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_d_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_d_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_d_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [1:0] fragmenter_auto_out_d_bits_size; // @[Fragmenter.scala 333:34]
  wire [10:0] fragmenter_auto_out_d_bits_source; // @[Fragmenter.scala 333:34]
  wire [63:0] fragmenter_auto_out_d_bits_data; // @[Fragmenter.scala 333:34]
  RHEA__TLFragmenter_8 fragmenter ( // @[Fragmenter.scala 333:34]
    .rf_reset(fragmenter_rf_reset),
    .clock(fragmenter_clock),
    .reset(fragmenter_reset),
    .auto_in_a_ready(fragmenter_auto_in_a_ready),
    .auto_in_a_valid(fragmenter_auto_in_a_valid),
    .auto_in_a_bits_opcode(fragmenter_auto_in_a_bits_opcode),
    .auto_in_a_bits_param(fragmenter_auto_in_a_bits_param),
    .auto_in_a_bits_size(fragmenter_auto_in_a_bits_size),
    .auto_in_a_bits_source(fragmenter_auto_in_a_bits_source),
    .auto_in_a_bits_address(fragmenter_auto_in_a_bits_address),
    .auto_in_a_bits_mask(fragmenter_auto_in_a_bits_mask),
    .auto_in_a_bits_data(fragmenter_auto_in_a_bits_data),
    .auto_in_a_bits_corrupt(fragmenter_auto_in_a_bits_corrupt),
    .auto_in_d_ready(fragmenter_auto_in_d_ready),
    .auto_in_d_valid(fragmenter_auto_in_d_valid),
    .auto_in_d_bits_opcode(fragmenter_auto_in_d_bits_opcode),
    .auto_in_d_bits_size(fragmenter_auto_in_d_bits_size),
    .auto_in_d_bits_source(fragmenter_auto_in_d_bits_source),
    .auto_in_d_bits_data(fragmenter_auto_in_d_bits_data),
    .auto_out_a_ready(fragmenter_auto_out_a_ready),
    .auto_out_a_valid(fragmenter_auto_out_a_valid),
    .auto_out_a_bits_opcode(fragmenter_auto_out_a_bits_opcode),
    .auto_out_a_bits_param(fragmenter_auto_out_a_bits_param),
    .auto_out_a_bits_size(fragmenter_auto_out_a_bits_size),
    .auto_out_a_bits_source(fragmenter_auto_out_a_bits_source),
    .auto_out_a_bits_address(fragmenter_auto_out_a_bits_address),
    .auto_out_a_bits_mask(fragmenter_auto_out_a_bits_mask),
    .auto_out_a_bits_data(fragmenter_auto_out_a_bits_data),
    .auto_out_a_bits_corrupt(fragmenter_auto_out_a_bits_corrupt),
    .auto_out_d_ready(fragmenter_auto_out_d_ready),
    .auto_out_d_valid(fragmenter_auto_out_d_valid),
    .auto_out_d_bits_opcode(fragmenter_auto_out_d_bits_opcode),
    .auto_out_d_bits_size(fragmenter_auto_out_d_bits_size),
    .auto_out_d_bits_source(fragmenter_auto_out_d_bits_source),
    .auto_out_d_bits_data(fragmenter_auto_out_d_bits_data)
  );
  assign fragmenter_rf_reset = rf_reset;
  assign auto_fragmenter_out_a_valid = fragmenter_auto_out_a_valid; // @[LazyModule.scala 390:12]
  assign auto_fragmenter_out_a_bits_opcode = fragmenter_auto_out_a_bits_opcode; // @[LazyModule.scala 390:12]
  assign auto_fragmenter_out_a_bits_param = fragmenter_auto_out_a_bits_param; // @[LazyModule.scala 390:12]
  assign auto_fragmenter_out_a_bits_size = fragmenter_auto_out_a_bits_size; // @[LazyModule.scala 390:12]
  assign auto_fragmenter_out_a_bits_source = fragmenter_auto_out_a_bits_source; // @[LazyModule.scala 390:12]
  assign auto_fragmenter_out_a_bits_address = fragmenter_auto_out_a_bits_address; // @[LazyModule.scala 390:12]
  assign auto_fragmenter_out_a_bits_mask = fragmenter_auto_out_a_bits_mask; // @[LazyModule.scala 390:12]
  assign auto_fragmenter_out_a_bits_data = fragmenter_auto_out_a_bits_data; // @[LazyModule.scala 390:12]
  assign auto_fragmenter_out_a_bits_corrupt = fragmenter_auto_out_a_bits_corrupt; // @[LazyModule.scala 390:12]
  assign auto_fragmenter_out_d_ready = fragmenter_auto_out_d_ready; // @[LazyModule.scala 390:12]
  assign auto_tl_in_a_ready = fragmenter_auto_in_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_valid = fragmenter_auto_in_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_opcode = fragmenter_auto_in_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_size = fragmenter_auto_in_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_source = fragmenter_auto_in_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_tl_in_d_bits_data = fragmenter_auto_in_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign fragmenter_clock = clock;
  assign fragmenter_reset = reset;
  assign fragmenter_auto_in_a_valid = auto_tl_in_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_opcode = auto_tl_in_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_param = auto_tl_in_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_size = auto_tl_in_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_source = auto_tl_in_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_address = auto_tl_in_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_mask = auto_tl_in_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_data = auto_tl_in_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_a_bits_corrupt = auto_tl_in_a_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_in_d_ready = auto_tl_in_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign fragmenter_auto_out_a_ready = auto_fragmenter_out_a_ready; // @[LazyModule.scala 390:12]
  assign fragmenter_auto_out_d_valid = auto_fragmenter_out_d_valid; // @[LazyModule.scala 390:12]
  assign fragmenter_auto_out_d_bits_opcode = auto_fragmenter_out_d_bits_opcode; // @[LazyModule.scala 390:12]
  assign fragmenter_auto_out_d_bits_size = auto_fragmenter_out_d_bits_size; // @[LazyModule.scala 390:12]
  assign fragmenter_auto_out_d_bits_source = auto_fragmenter_out_d_bits_source; // @[LazyModule.scala 390:12]
  assign fragmenter_auto_out_d_bits_data = auto_fragmenter_out_d_bits_data; // @[LazyModule.scala 390:12]
endmodule
