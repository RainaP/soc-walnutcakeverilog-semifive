//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TraceGapper_2(
  input         clock,
  input         reset,
  input         io_teActive,
  input         io_disableGapper,
  output        io_streamIn_ready,
  input         io_streamIn_valid,
  input  [31:0] io_streamIn_bits_data,
  input         io_streamOut_ready,
  output        io_streamOut_valid,
  output [31:0] io_streamOut_bits_data,
  input         io_fifoFlush,
  output        io_fifoEmpty
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
`endif // RANDOMIZE_REG_INIT
  wire  _T = ~io_disableGapper; // @[TraceGapper.scala 23:9]
  reg [2:0] holdV; // @[TraceGapper.scala 28:27]
  reg [31:0] hold; // @[TraceGapper.scala 29:27]
  wire [1:0] _inV_T_12 = io_streamIn_bits_data[17:16] == 2'h3 ? 2'h2 : 2'h3; // @[TraceGapper.scala 37:102]
  wire [1:0] _inV_T_13 = io_streamIn_bits_data[9:8] == 2'h3 ? 2'h1 : _inV_T_12; // @[TraceGapper.scala 37:102]
  wire [1:0] _inV_T_14 = io_streamIn_bits_data[1:0] == 2'h3 ? 2'h0 : _inV_T_13; // @[TraceGapper.scala 37:102]
  wire [2:0] inV = 2'h1 + _inV_T_14; // @[TraceGapper.scala 37:16]
  wire [3:0] _inFits_T = holdV + inV; // @[TraceGapper.scala 31:36]
  wire  inFits = _inFits_T <= 4'h4; // @[TraceGapper.scala 31:43]
  wire  sendFull = holdV == 3'h4 | io_streamIn_valid & _inFits_T >= 4'h4; // @[TraceGapper.scala 34:59]
  wire  _sendPart_T = ~io_streamIn_valid; // @[TraceGapper.scala 35:30]
  wire  _sendPart_T_2 = |holdV; // @[TraceGapper.scala 35:72]
  wire  sendPart = _sendPart_T & io_fifoFlush & |holdV; // @[TraceGapper.scala 35:64]
  wire  _io_streamIn_ready_T = io_streamOut_ready & io_streamOut_valid; // @[Decoupled.scala 40:37]
  wire [31:0] _holdNext_T = sendPart ? 32'hffffffff : io_streamIn_bits_data; // @[TraceGapper.scala 42:28]
  wire [6:0] _holdNext_T_1 = 4'h8 * holdV; // @[TraceGapper.scala 42:90]
  wire [158:0] _GEN_12 = {{127'd0}, _holdNext_T}; // @[TraceGapper.scala 42:71]
  wire [158:0] _holdNext_T_2 = _GEN_12 << _holdNext_T_1; // @[TraceGapper.scala 42:71]
  wire [158:0] _GEN_13 = {{127'd0}, hold}; // @[TraceGapper.scala 42:22]
  wire [158:0] _holdNext_T_3 = _GEN_13 | _holdNext_T_2; // @[TraceGapper.scala 42:22]
  wire  _T_3 = _io_streamIn_ready_T & sendFull; // @[TraceGapper.scala 47:37]
  wire  _T_4 = _T_3 & io_streamIn_valid; // @[TraceGapper.scala 47:48]
  wire [63:0] holdNext = _holdNext_T_3[63:0]; // @[TraceGapper.scala 32:24 TraceGapper.scala 42:14]
  wire [3:0] _holdV_T_2 = _inFits_T - 4'h4; // @[TraceGapper.scala 49:29]
  wire  _T_10 = io_streamIn_valid & inFits; // @[TraceGapper.scala 53:35]
  wire [63:0] _GEN_0 = _T_10 ? holdNext : {{32'd0}, hold}; // @[TraceGapper.scala 53:45 TraceGapper.scala 54:12 TraceGapper.scala 29:27]
  wire [3:0] _GEN_1 = _T_10 ? _inFits_T : {{1'd0}, holdV}; // @[TraceGapper.scala 53:45 TraceGapper.scala 55:13 TraceGapper.scala 28:27]
  wire [63:0] _GEN_2 = _io_streamIn_ready_T & (sendFull & _sendPart_T | sendPart) ? 64'h0 : _GEN_0; // @[TraceGapper.scala 50:85 TraceGapper.scala 51:12]
  wire [3:0] _GEN_3 = _io_streamIn_ready_T & (sendFull & _sendPart_T | sendPart) ? 4'h0 : _GEN_1; // @[TraceGapper.scala 50:85 TraceGapper.scala 52:13]
  wire [63:0] _GEN_4 = _T_4 ? {{32'd0}, holdNext[63:32]} : _GEN_2; // @[TraceGapper.scala 47:69 TraceGapper.scala 48:12]
  wire [3:0] _GEN_5 = _T_4 ? _holdV_T_2 : _GEN_3; // @[TraceGapper.scala 47:69 TraceGapper.scala 49:13]
  wire [63:0] _GEN_6 = ~io_teActive ? 64'h0 : _GEN_4; // @[TraceGapper.scala 44:25 TraceGapper.scala 45:12]
  wire [3:0] _GEN_7 = ~io_teActive ? 4'h0 : _GEN_5; // @[TraceGapper.scala 44:25 TraceGapper.scala 46:13]
  wire [63:0] _GEN_11 = _T ? holdNext : {{32'd0}, io_streamIn_bits_data}; // @[TraceGapper.scala 23:28 TraceGapper.scala 60:28 TraceGapper.scala 75:18]
  assign io_streamIn_ready = _T ? _io_streamIn_ready_T | inFits : io_streamOut_ready; // @[TraceGapper.scala 23:28 TraceGapper.scala 39:23 TraceGapper.scala 75:18]
  assign io_streamOut_valid = _T ? sendFull | sendPart : io_streamIn_valid; // @[TraceGapper.scala 23:28 TraceGapper.scala 59:24 TraceGapper.scala 75:18]
  assign io_streamOut_bits_data = _GEN_11[31:0];
  assign io_fifoEmpty = _T ? ~_sendPart_T_2 : 1'h1; // @[TraceGapper.scala 23:28 TraceGapper.scala 40:18 TraceGapper.scala 76:18]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      holdV <= 3'h0;
    end else begin
      holdV <= _GEN_7[2:0];
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      hold <= 32'h0;
    end else begin
      hold <= _GEN_6[31:0];
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  holdV = _RAND_0[2:0];
  _RAND_1 = {1{`RANDOM}};
  hold = _RAND_1[31:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    holdV = 3'h0;
  end
  if (reset) begin
    hold = 32'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
