//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade 
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module peripheral_subsystem (
	 input         I2C_0__IO__SCLI
	,output        I2C_0__IO__SCLOE
	,input         I2C_0__IO__SDIN
	,output        I2C_0__IO__SDOE
	,input         I2C_1__IO__SCLI
	,output        I2C_1__IO__SCLOE
	,input         I2C_1__IO__SDIN
	,output        I2C_1__IO__SDOE
	,input         I2C_2__IO__SCLI
	,output        I2C_2__IO__SCLOE
	,input         I2C_2__IO__SDIN
	,output        I2C_2__IO__SDOE
	,input         I2C_3__IO__SCLI
	,output        I2C_3__IO__SCLOE
	,input         I2C_3__IO__SDIN
	,output        I2C_3__IO__SDOE
	,output        SPI_0__IO__SCK
	,output        SPI_0__IO__SCS
	,output [ 3:0] SPI_0__IO__SDO
	,input  [ 3:0] SPI_0__IO__SDI
	,output [ 3:0] SPI_0__IO__OEN
	,output        SPI_1__IO__SCK
	,output        SPI_1__IO__SCS
	,output [ 3:0] SPI_1__IO__SDO
	,input  [ 3:0] SPI_1__IO__SDI
	,output [ 3:0] SPI_1__IO__OEN
	,output        SPI_2__IO__SCK
	,output        SPI_2__IO__SCS
	,output [ 3:0] SPI_2__IO__SDO
	,input  [ 3:0] SPI_2__IO__SDI
	,output [ 3:0] SPI_2__IO__OEN
	,output        SPI_3__IO__SCK
	,output        SPI_3__IO__SCS
	,output [ 3:0] SPI_3__IO__SDO
	,input  [ 3:0] SPI_3__IO__SDI
	,output [ 3:0] SPI_3__IO__OEN
	,output        SDMMC_0__IO__CLK
	,output        SDMMC_0__IO__CMD
	,input         SDMMC_0__IO__RSP
	,output        SDMMC_0__IO__CMDOEN
	,output [ 7:0] SDMMC_0__IO__DOUT
	,input  [ 7:0] SDMMC_0__IO__DIN
	,output [ 7:0] SDMMC_0__IO__DOEN
	,input         SDMMC_0__IO__DQS
	,output        SDMMC_0__IO__RSTN
	,input  [14:0] OTP_0__IO__ADD
	,input         OTP_0__IO__CEB
	,input         OTP_0__IO__CLE
	,input         OTP_0__IO__DLE
	,input         OTP_0__IO__PGMEN
	,input         OTP_0__IO__READEN
	,input         OTP_0__IO__RSTB
	,input         OTP_0__IO__WEB
	,input         OTP_0__IO__CPUMPEN
	,input         OTP_0__IO__DIN
	,output [31:0] OTP_0__IO__DOUT
	,inout         OTP_0__IO__VGB_ANALOG
	,inout         OTP_0__IO__VTDO_ANALOG
	,inout         OTP_0__IO__VREFM_ANALOG
	,inout         OTP_0__IO__VPP_ANALOG
	,inout  [14:0] PVT_0__IO__VSENSE_TS_ANALOG
	,inout  [14:0] PVT_0__IO__VOL_TS_ANALOG
	,input         PVT_0__IO__VREFT_FLAG_TS
	,input         PVT_0__IO__VBE_FLAG_TS
	,inout  [14:0] PVT_0__IO__IBIAS_TS_ANALOG
	,inout         PVT_0__IO__TEST_OUT_TS_ANALOG
	,input         DFT__SCAN_CLK
	,input         DFT__SCAN_RSTN
	,input         DFT__SCAN_MODE
	,input         SCU_PERI_0__CLK__SUBSYSTEM
	,input         SCU_PERI_0__RSTN__SUBSYSTEM
	,input         SCU_PERI_0__CLK__UART_0
	,input         SCU_PERI_0__CLK__UART_1
	,input         SCU_PERI_0__CLK__RTC_0
	,input         SCU_PERI_0__CLK__SPI_0
	,input         SCU_PERI_0__CLK__SPI_1
	,input         SCU_PERI_0__CLK__SPI_2
	,input         SCU_PERI_0__CLK__SPI_3
	,input         SCU_PERI_0__CLK__I2C_0
	,input         SCU_PERI_0__CLK__I2C_1
	,input         SCU_PERI_0__CLK__I2C_2
	,input         SCU_PERI_0__CLK__I2C_3
	,input         SCU_PERI_0__CLK__I2S_0
	,input         SCU_PERI_0__CLK__OTP_0
	,input         SCU_PERI_0__CLK__SDMMC_0
	,output        PBUS_PERI_0__CLK__RX
	,output        PBUS_PERI_0__RSTN__RX
	,output        MBUS_PERI_0__CLK__TX
	,output        MBUS_PERI_0__RSTN__TX
	,output        PBUS_PERI_0__AXI4__RX__BID
	,output [ 1:0] PBUS_PERI_0__AXI4__RX__BRESP
	,output        PBUS_PERI_0__AXI4__RX__RID
	,output [31:0] PBUS_PERI_0__AXI4__RX__RDATA
	,output [ 1:0] PBUS_PERI_0__AXI4__RX__RRESP
	,output        PBUS_PERI_0__AXI4__RX__AWREADY
	,output        PBUS_PERI_0__AXI4__RX__WREADY
	,output        PBUS_PERI_0__AXI4__RX__BVALID
	,output        PBUS_PERI_0__AXI4__RX__ARREADY
	,output        PBUS_PERI_0__AXI4__RX__RVALID
	,output        PBUS_PERI_0__AXI4__RX__RLAST
	,input  [31:0] PBUS_PERI_0__AXI4__RX__AWADDR
	,input         PBUS_PERI_0__AXI4__RX__AWID
	,input  [ 7:0] PBUS_PERI_0__AXI4__RX__AWLEN
	,input  [ 2:0] PBUS_PERI_0__AXI4__RX__AWSIZE
	,input  [ 1:0] PBUS_PERI_0__AXI4__RX__AWBURST
	,input         PBUS_PERI_0__AXI4__RX__AWLOCK
	,input  [ 3:0] PBUS_PERI_0__AXI4__RX__AWCACHE
	,input  [ 2:0] PBUS_PERI_0__AXI4__RX__AWPROT
	,input  [31:0] PBUS_PERI_0__AXI4__RX__WDATA
	,input  [ 3:0] PBUS_PERI_0__AXI4__RX__WSTRB
	,input         PBUS_PERI_0__AXI4__RX__ARID
	,input  [31:0] PBUS_PERI_0__AXI4__RX__ARADDR
	,input  [ 7:0] PBUS_PERI_0__AXI4__RX__ARLEN
	,input  [ 2:0] PBUS_PERI_0__AXI4__RX__ARSIZE
	,input  [ 1:0] PBUS_PERI_0__AXI4__RX__ARBURST
	,input         PBUS_PERI_0__AXI4__RX__ARLOCK
	,input  [ 3:0] PBUS_PERI_0__AXI4__RX__ARCACHE
	,input  [ 2:0] PBUS_PERI_0__AXI4__RX__ARPROT
	,input         PBUS_PERI_0__AXI4__RX__AWVALID
	,input         PBUS_PERI_0__AXI4__RX__WVALID
	,input         PBUS_PERI_0__AXI4__RX__WLAST
	,input         PBUS_PERI_0__AXI4__RX__BREADY
	,input         PBUS_PERI_0__AXI4__RX__ARVALID
	,input         PBUS_PERI_0__AXI4__RX__RREADY
	,output [35:0] MBUS_PERI_0__AXI4__TX__AWADDR
	,output [ 5:0] MBUS_PERI_0__AXI4__TX__AWID
	,output [ 7:0] MBUS_PERI_0__AXI4__TX__AWLEN
	,output [ 2:0] MBUS_PERI_0__AXI4__TX__AWSIZE
	,output [ 1:0] MBUS_PERI_0__AXI4__TX__AWBURST
	,output        MBUS_PERI_0__AXI4__TX__AWLOCK
	,output [ 3:0] MBUS_PERI_0__AXI4__TX__AWCACHE
	,output [ 2:0] MBUS_PERI_0__AXI4__TX__AWPROT
	,output [ 9:0] MBUS_PERI_0__AXI4__TX__AWUSER
	,output [63:0] MBUS_PERI_0__AXI4__TX__WDATA
	,output [ 7:0] MBUS_PERI_0__AXI4__TX__WSTRB
	,output [ 5:0] MBUS_PERI_0__AXI4__TX__ARID
	,output [35:0] MBUS_PERI_0__AXI4__TX__ARADDR
	,output [ 7:0] MBUS_PERI_0__AXI4__TX__ARLEN
	,output [ 2:0] MBUS_PERI_0__AXI4__TX__ARSIZE
	,output [ 1:0] MBUS_PERI_0__AXI4__TX__ARBURST
	,output        MBUS_PERI_0__AXI4__TX__ARLOCK
	,output [ 3:0] MBUS_PERI_0__AXI4__TX__ARCACHE
	,output [ 2:0] MBUS_PERI_0__AXI4__TX__ARPROT
	,output [ 9:0] MBUS_PERI_0__AXI4__TX__ARUSER
	,output        MBUS_PERI_0__AXI4__TX__AWVALID
	,output        MBUS_PERI_0__AXI4__TX__WVALID
	,output        MBUS_PERI_0__AXI4__TX__WLAST
	,output        MBUS_PERI_0__AXI4__TX__BREADY
	,output        MBUS_PERI_0__AXI4__TX__ARVALID
	,output        MBUS_PERI_0__AXI4__TX__RREADY
	,input  [ 5:0] MBUS_PERI_0__AXI4__TX__BID
	,input  [ 1:0] MBUS_PERI_0__AXI4__TX__BRESP
	,input  [ 5:0] MBUS_PERI_0__AXI4__TX__RID
	,input  [63:0] MBUS_PERI_0__AXI4__TX__RDATA
	,input  [ 1:0] MBUS_PERI_0__AXI4__TX__RRESP
	,input         MBUS_PERI_0__AXI4__TX__AWREADY
	,input         MBUS_PERI_0__AXI4__TX__WREADY
	,input         MBUS_PERI_0__AXI4__TX__BVALID
	,input         MBUS_PERI_0__AXI4__TX__ARREADY
	,input         MBUS_PERI_0__AXI4__TX__RVALID
	,input         MBUS_PERI_0__AXI4__TX__RLAST
	,output        DMA_0__IRQ
	,input  [15:0] GPIO_A__IO__Y
	,output [15:0] GPIO_A__IO__A
	,output [15:0] GPIO_A__IO__OE
	,input  [15:0] GPIO_B__IO__Y
	,output [15:0] GPIO_B__IO__A
	,output [15:0] GPIO_B__IO__OE
	,input  [15:0] GPIO_C__IO__Y
	,output [15:0] GPIO_C__IO__A
	,output [15:0] GPIO_C__IO__OE
	,input  [15:0] GPIO_D__IO__Y
	,output [15:0] GPIO_D__IO__A
	,output [15:0] GPIO_D__IO__OE
	,output        GPIO_0__IRQ
	,output        I2C_0__IRQ
	,output        I2C_1__IRQ
	,output        SPI_0__IRQ
	,output        SPI_1__IRQ
	,output        TIMER_0__IRQ
	,input         UART_0__IO__RX
	,output        UART_0__IO__TX
	,input         UART_0__IO__CTS
	,output        UART_0__IO__RTS
	,output        UART_0__IRQ
	,input         UART_1__IO__RX
	,output        UART_1__IO__TX
	,input         UART_1__IO__CTS
	,output        UART_1__IO__RTS
	,output        UART_1__IRQ
	,output        WDT_0_RSTREQN
	,output        WDT_0__IRQ
);
	assign PBUS_PERI_0__AXI4__RX__AWREADY = 1'h1;
	assign PBUS_PERI_0__AXI4__RX__WREADY = 1'h1;
	assign PBUS_PERI_0__AXI4__RX__ARREADY = 1'h1;
	assign MBUS_PERI_0__AXI4__TX__BREADY = 1'h1;
	assign MBUS_PERI_0__AXI4__TX__RREADY = 1'h1;
	assign I2C_0__IO__SCLOE = 1'h0;
	assign I2C_0__IO__SDOE = 1'h0;
	assign I2C_1__IO__SCLOE = 1'h0;
	assign I2C_1__IO__SDOE = 1'h0;
	assign I2C_2__IO__SCLOE = 1'h0;
	assign I2C_2__IO__SDOE = 1'h0;
	assign I2C_3__IO__SCLOE = 1'h0;
	assign I2C_3__IO__SDOE = 1'h0;
	assign SPI_0__IO__SCK = 1'h0;
	assign SPI_0__IO__SCS = 1'h0;
	assign SPI_0__IO__SDO = 4'h0;
	assign SPI_0__IO__OEN = 4'h0;
	assign SPI_1__IO__SCK = 1'h0;
	assign SPI_1__IO__SCS = 1'h0;
	assign SPI_1__IO__SDO = 4'h0;
	assign SPI_1__IO__OEN = 4'h0;
	assign SPI_2__IO__SCK = 1'h0;
	assign SPI_2__IO__SCS = 1'h0;
	assign SPI_2__IO__SDO = 4'h0;
	assign SPI_2__IO__OEN = 4'h0;
	assign SPI_3__IO__SCK = 1'h0;
	assign SPI_3__IO__SCS = 1'h0;
	assign SPI_3__IO__SDO = 4'h0;
	assign SPI_3__IO__OEN = 4'h0;
	assign SDMMC_0__IO__CLK = 1'h0;
	assign SDMMC_0__IO__CMD = 1'h0;
	assign SDMMC_0__IO__CMDOEN = 1'h0;
	assign SDMMC_0__IO__DOUT = 8'h0;
	assign SDMMC_0__IO__DOEN = 8'h0;
	assign SDMMC_0__IO__RSTN = 1'h0;
	assign OTP_0__IO__DOUT = 32'h0;
	assign PBUS_PERI_0__CLK__RX = 1'h0;
	assign PBUS_PERI_0__RSTN__RX = 1'h0;
	assign MBUS_PERI_0__CLK__TX = 1'h0;
	assign MBUS_PERI_0__RSTN__TX = 1'h0;
	assign PBUS_PERI_0__AXI4__RX__BID = 1'h0;
	assign PBUS_PERI_0__AXI4__RX__BRESP = 2'h0;
	assign PBUS_PERI_0__AXI4__RX__RID = 1'h0;
	assign PBUS_PERI_0__AXI4__RX__RDATA = 32'h0;
	assign PBUS_PERI_0__AXI4__RX__RRESP = 2'h0;
	assign PBUS_PERI_0__AXI4__RX__BVALID = 1'h0;
	assign PBUS_PERI_0__AXI4__RX__RVALID = 1'h0;
	assign PBUS_PERI_0__AXI4__RX__RLAST = 1'h0;
	assign MBUS_PERI_0__AXI4__TX__AWADDR = 36'h0;
	assign MBUS_PERI_0__AXI4__TX__AWID = 6'h0;
	assign MBUS_PERI_0__AXI4__TX__AWLEN = 8'h0;
	assign MBUS_PERI_0__AXI4__TX__AWSIZE = 3'h0;
	assign MBUS_PERI_0__AXI4__TX__AWBURST = 2'h0;
	assign MBUS_PERI_0__AXI4__TX__AWLOCK = 1'h0;
	assign MBUS_PERI_0__AXI4__TX__AWCACHE = 4'h0;
	assign MBUS_PERI_0__AXI4__TX__AWPROT = 3'h0;
	assign MBUS_PERI_0__AXI4__TX__AWUSER = 10'h0;
	assign MBUS_PERI_0__AXI4__TX__WDATA = 64'h0;
	assign MBUS_PERI_0__AXI4__TX__WSTRB = 8'h0;
	assign MBUS_PERI_0__AXI4__TX__ARID = 6'h0;
	assign MBUS_PERI_0__AXI4__TX__ARADDR = 36'h0;
	assign MBUS_PERI_0__AXI4__TX__ARLEN = 8'h0;
	assign MBUS_PERI_0__AXI4__TX__ARSIZE = 3'h0;
	assign MBUS_PERI_0__AXI4__TX__ARBURST = 2'h0;
	assign MBUS_PERI_0__AXI4__TX__ARLOCK = 1'h0;
	assign MBUS_PERI_0__AXI4__TX__ARCACHE = 4'h0;
	assign MBUS_PERI_0__AXI4__TX__ARPROT = 3'h0;
	assign MBUS_PERI_0__AXI4__TX__ARUSER = 10'h0;
	assign MBUS_PERI_0__AXI4__TX__AWVALID = 1'h0;
	assign MBUS_PERI_0__AXI4__TX__WVALID = 1'h0;
	assign MBUS_PERI_0__AXI4__TX__WLAST = 1'h0;
	assign MBUS_PERI_0__AXI4__TX__ARVALID = 1'h0;
	assign DMA_0__IRQ = 1'h0;
	assign GPIO_A__IO__A = 16'h0;
	assign GPIO_A__IO__OE = 16'h0;
	assign GPIO_B__IO__A = 16'h0;
	assign GPIO_B__IO__OE = 16'h0;
	assign GPIO_C__IO__A = 16'h0;
	assign GPIO_C__IO__OE = 16'h0;
	assign GPIO_D__IO__A = 16'h0;
	assign GPIO_D__IO__OE = 16'h0;
	assign GPIO_0__IRQ = 1'h0;
	assign I2C_0__IRQ = 1'h0;
	assign I2C_1__IRQ = 1'h0;
	assign SPI_0__IRQ = 1'h0;
	assign SPI_1__IRQ = 1'h0;
	assign TIMER_0__IRQ = 1'h0;
	assign UART_0__IO__TX = 1'h0;
	assign UART_0__IO__RTS = 1'h0;
	assign UART_0__IRQ = 1'h0;
	assign UART_1__IO__TX = 1'h0;
	assign UART_1__IO__RTS = 1'h0;
	assign UART_1__IRQ = 1'h0;
	assign WDT_0_RSTREQN = 1'h0;
	assign WDT_0__IRQ = 1'h0;

endmodule 
