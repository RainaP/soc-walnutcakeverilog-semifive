//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TracePIB(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_sink_in_a_ready,
  input         auto_sink_in_a_valid,
  input  [2:0]  auto_sink_in_a_bits_opcode,
  input         auto_sink_in_a_bits_source,
  input  [30:0] auto_sink_in_a_bits_address,
  input  [31:0] auto_sink_in_a_bits_data,
  input         auto_sink_in_d_ready,
  output        auto_sink_in_d_valid,
  output        auto_sink_in_d_bits_source,
  output        auto_sink_in_d_bits_denied,
  input  [2:0]  io_pibControl_bits_reserved0,
  input  [12:0] io_pibControl_bits_pibDivider,
  input  [5:0]  io_pibControl_bits_reserved1,
  input         io_pibControl_bits_pibCalibrate,
  input         io_pibControl_bits_pibRefCenter,
  input  [3:0]  io_pibControl_bits_pibMode,
  input  [1:0]  io_pibControl_bits_reserved2,
  input         io_pibControl_bits_pibEnable,
  input         io_pibControl_bits_pibActive,
  output [12:0] io_pibControlSync_pibDivider,
  output        io_pibControlSync_pibCalibrate,
  output        io_pibControlSync_pibRefCenter,
  output [3:0]  io_pibControlSync_pibMode,
  output        io_pibControlSync_pibEnable,
  output        io_pibControlSync_pibActive,
  input         io_streamOut_ready,
  output        io_streamOut_valid,
  output [31:0] io_streamOut_bits
);
  wire  io_streamOut_q_rf_reset; // @[ShiftQueue.scala 60:19]
  wire  io_streamOut_q_clock; // @[ShiftQueue.scala 60:19]
  wire  io_streamOut_q_reset; // @[ShiftQueue.scala 60:19]
  wire  io_streamOut_q_io_enq_ready; // @[ShiftQueue.scala 60:19]
  wire  io_streamOut_q_io_enq_valid; // @[ShiftQueue.scala 60:19]
  wire [31:0] io_streamOut_q_io_enq_bits; // @[ShiftQueue.scala 60:19]
  wire  io_streamOut_q_io_deq_ready; // @[ShiftQueue.scala 60:19]
  wire  io_streamOut_q_io_deq_valid; // @[ShiftQueue.scala 60:19]
  wire [31:0] io_streamOut_q_io_deq_bits; // @[ShiftQueue.scala 60:19]
  wire  _sinkReady_T = |io_pibControl_bits_pibMode; // @[TracePIB.scala 201:69]
  wire  _sinkReady_T_1 = ~(|io_pibControl_bits_pibMode); // @[TracePIB.scala 201:41]
  wire  inDecoupled_ready = io_streamOut_q_io_enq_ready; // @[TracePIB.scala 191:27 ShiftQueue.scala 61:14]
  wire  _sinkReady_T_3 = ~io_pibControl_bits_pibEnable; // @[TracePIB.scala 201:75]
  wire  sinkReady = inDecoupled_ready | ~(|io_pibControl_bits_pibMode) | ~io_pibControl_bits_pibEnable; // @[TracePIB.scala 201:73]
  wire  in_d_valid = auto_sink_in_a_valid & sinkReady; // @[TracePIB.scala 205:30]
  wire  _denied_T_3 = _sinkReady_T_1 | _sinkReady_T_3; // @[TracePIB.scala 206:61]
  RHEA__ShiftQueue_14 io_streamOut_q ( // @[ShiftQueue.scala 60:19]
    .rf_reset(io_streamOut_q_rf_reset),
    .clock(io_streamOut_q_clock),
    .reset(io_streamOut_q_reset),
    .io_enq_ready(io_streamOut_q_io_enq_ready),
    .io_enq_valid(io_streamOut_q_io_enq_valid),
    .io_enq_bits(io_streamOut_q_io_enq_bits),
    .io_deq_ready(io_streamOut_q_io_deq_ready),
    .io_deq_valid(io_streamOut_q_io_deq_valid),
    .io_deq_bits(io_streamOut_q_io_deq_bits)
  );
  assign io_streamOut_q_rf_reset = rf_reset;
  assign auto_sink_in_a_ready = auto_sink_in_d_ready & sinkReady; // @[TracePIB.scala 204:30]
  assign auto_sink_in_d_valid = auto_sink_in_a_valid & sinkReady; // @[TracePIB.scala 205:30]
  assign auto_sink_in_d_bits_source = auto_sink_in_a_bits_source; // @[TracePIB.scala 168:31 LazyModule.scala 388:16]
  assign auto_sink_in_d_bits_denied = in_d_valid & _denied_T_3; // @[TracePIB.scala 206:26]
  assign io_pibControlSync_pibDivider = io_pibControl_bits_pibDivider; // @[TracePIB.scala 222:25]
  assign io_pibControlSync_pibCalibrate = io_pibControl_bits_pibCalibrate; // @[TracePIB.scala 222:25]
  assign io_pibControlSync_pibRefCenter = io_pibControl_bits_pibRefCenter; // @[TracePIB.scala 222:25]
  assign io_pibControlSync_pibMode = io_pibControl_bits_pibMode; // @[TracePIB.scala 222:25]
  assign io_pibControlSync_pibEnable = io_pibControl_bits_pibEnable; // @[TracePIB.scala 222:25]
  assign io_pibControlSync_pibActive = io_pibControl_bits_pibActive; // @[TracePIB.scala 222:25]
  assign io_streamOut_valid = io_streamOut_q_io_deq_valid; // @[TracePIB.scala 221:20]
  assign io_streamOut_bits = io_streamOut_q_io_deq_bits; // @[TracePIB.scala 221:20]
  assign io_streamOut_q_clock = clock;
  assign io_streamOut_q_reset = reset;
  assign io_streamOut_q_io_enq_valid = auto_sink_in_a_valid & auto_sink_in_d_ready & _sinkReady_T &
    io_pibControl_bits_pibEnable; // @[TracePIB.scala 203:83]
  assign io_streamOut_q_io_enq_bits = auto_sink_in_a_bits_data; // @[TracePIB.scala 168:31 LazyModule.scala 388:16]
  assign io_streamOut_q_io_deq_ready = io_streamOut_ready; // @[TracePIB.scala 221:20]
endmodule
