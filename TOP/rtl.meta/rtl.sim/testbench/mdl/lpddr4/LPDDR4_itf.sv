
interface LPDDR4_itf #(
  parameter C = 2,
  parameter D = 16
)(
  wire            CK,
  wire            CKB,
  wire  [1:0]     CKE,
  wire  [C-1:0]   CS,
  wire  [1:0]     ODT,
  wire  [D-1:0]   DQ_A,
  wire  [D-1:0]   DQ_B,
  wire  [1:0]     PDQS_A,
  wire  [1:0]     PDQS_B,
  wire  [1:0]     NDQS_A,
  wire  [1:0]     NDQS_B,
  wire            RESET,
  wire  [5:0]     ADCT,
  wire  [1:0]     DM_A,
  wire  [1:0]     DM_B,
  wire            ZQ
);

  modport port(
    inout CK,
    inout CKB,
    inout CKE,
    inout CS,
    inout ODT,
    inout DQ_A,
    inout DQ_B,
    inout PDQS_A,
    inout PDQS_B,
    inout NDQS_A,
    inout NDQS_B,
    inout RESET,
    inout ADCT,
    inout DM_A,
    inout DM_B,
    inout ZQ
  );

endinterface
