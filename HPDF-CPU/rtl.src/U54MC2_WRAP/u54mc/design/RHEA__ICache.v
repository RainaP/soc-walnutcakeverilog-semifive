//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__ICache(
  input          rf_reset,
  input          clock,
  input          reset,
  input          auto_master_out_a_ready,
  output         auto_master_out_a_valid,
  output [2:0]   auto_master_out_a_bits_opcode,
  output         auto_master_out_a_bits_source,
  output [36:0]  auto_master_out_a_bits_address,
  input          auto_master_out_d_valid,
  input  [2:0]   auto_master_out_d_bits_opcode,
  input  [3:0]   auto_master_out_d_bits_size,
  input  [127:0] auto_master_out_d_bits_data,
  input          auto_master_out_d_bits_corrupt,
  output         io_req_ready,
  input          io_req_valid,
  input  [38:0]  io_req_bits_addr,
  input  [36:0]  io_s1_paddr,
  input          io_s1_kill,
  input          io_s2_kill,
  input          io_s2_prefetch,
  output         io_resp_valid,
  output [31:0]  io_resp_bits_data,
  output         io_resp_bits_ae,
  input          io_invalidate,
  output         io_perf_acquire,
  output         io_keep_clock_enabled
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [63:0] _RAND_1;
  reg [511:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [63:0] _RAND_10;
  reg [63:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
`endif // RANDOMIZE_REG_INIT
  wire  repl_way_v0_prng_rf_reset; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_clock; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_reset; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_increment; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_0; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_1; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_2; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_3; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_4; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_5; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_6; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_7; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_8; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_9; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_10; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_11; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_12; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_13; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_14; // @[PRNG.scala 82:22]
  wire  repl_way_v0_prng_io_out_15; // @[PRNG.scala 82:22]
  wire [6:0] tag_array_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  tag_array_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  tag_array_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  tag_array_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [25:0] tag_array_RW0_wdata_0; // @[DescribedSRAM.scala 18:26]
  wire [25:0] tag_array_RW0_wdata_1; // @[DescribedSRAM.scala 18:26]
  wire [25:0] tag_array_RW0_wdata_2; // @[DescribedSRAM.scala 18:26]
  wire [25:0] tag_array_RW0_wdata_3; // @[DescribedSRAM.scala 18:26]
  wire [25:0] tag_array_RW0_rdata_0; // @[DescribedSRAM.scala 18:26]
  wire [25:0] tag_array_RW0_rdata_1; // @[DescribedSRAM.scala 18:26]
  wire [25:0] tag_array_RW0_rdata_2; // @[DescribedSRAM.scala 18:26]
  wire [25:0] tag_array_RW0_rdata_3; // @[DescribedSRAM.scala 18:26]
  wire  tag_array_RW0_wmask_0; // @[DescribedSRAM.scala 18:26]
  wire  tag_array_RW0_wmask_1; // @[DescribedSRAM.scala 18:26]
  wire  tag_array_RW0_wmask_2; // @[DescribedSRAM.scala 18:26]
  wire  tag_array_RW0_wmask_3; // @[DescribedSRAM.scala 18:26]
  wire [8:0] data_arrays_0_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_0_RW0_wdata_0; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_0_RW0_wdata_1; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_0_RW0_wdata_2; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_0_RW0_wdata_3; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_0_RW0_rdata_0; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_0_RW0_rdata_1; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_0_RW0_rdata_2; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_0_RW0_rdata_3; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_0; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_1; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_2; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_0_RW0_wmask_3; // @[DescribedSRAM.scala 18:26]
  wire [8:0] data_arrays_1_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_1_RW0_wdata_0; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_1_RW0_wdata_1; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_1_RW0_wdata_2; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_1_RW0_wdata_3; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_1_RW0_rdata_0; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_1_RW0_rdata_1; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_1_RW0_rdata_2; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_1_RW0_rdata_3; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_0; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_1; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_2; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_1_RW0_wmask_3; // @[DescribedSRAM.scala 18:26]
  wire [8:0] data_arrays_2_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_2_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_2_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_2_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_2_RW0_wdata_0; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_2_RW0_wdata_1; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_2_RW0_wdata_2; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_2_RW0_wdata_3; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_2_RW0_rdata_0; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_2_RW0_rdata_1; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_2_RW0_rdata_2; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_2_RW0_rdata_3; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_2_RW0_wmask_0; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_2_RW0_wmask_1; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_2_RW0_wmask_2; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_2_RW0_wmask_3; // @[DescribedSRAM.scala 18:26]
  wire [8:0] data_arrays_3_RW0_addr; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_3_RW0_en; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_3_RW0_clk; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_3_RW0_wmode; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_3_RW0_wdata_0; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_3_RW0_wdata_1; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_3_RW0_wdata_2; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_3_RW0_wdata_3; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_3_RW0_rdata_0; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_3_RW0_rdata_1; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_3_RW0_rdata_2; // @[DescribedSRAM.scala 18:26]
  wire [31:0] data_arrays_3_RW0_rdata_3; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_3_RW0_wmask_0; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_3_RW0_wmask_1; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_3_RW0_wmask_2; // @[DescribedSRAM.scala 18:26]
  wire  data_arrays_3_RW0_wmask_3; // @[DescribedSRAM.scala 18:26]
  wire  s0_valid = io_req_ready & io_req_valid; // @[Decoupled.scala 40:37]
  reg  s1_valid; // @[ICache.scala 181:21]
  reg [38:0] s1_vaddr; // @[Reg.scala 15:16]
  reg [511:0] vb_array; // @[ICache.scala 244:21]
  wire  s1_idx_msbs = s1_vaddr[12]; // @[ICache.scala 497:65]
  wire [5:0] s1_idx_lsbs = io_s1_paddr[11:6]; // @[ICache.scala 496:21]
  wire [7:0] _s1_vb_T = {1'h0,s1_idx_msbs,s1_idx_lsbs}; // @[Cat.scala 30:58]
  wire [511:0] _s1_vb_T_1 = vb_array >> _s1_vb_T; // @[ICache.scala 272:25]
  wire  s1_vb = _s1_vb_T_1[0]; // @[ICache.scala 272:25]
  wire [24:0] tag = tag_array_RW0_rdata_0[24:0]; // @[package.scala 154:13]
  wire [24:0] s1_tag = io_s1_paddr[36:12]; // @[ICache.scala 267:91]
  wire  tagMatch = s1_vb & tag == s1_tag; // @[ICache.scala 275:26]
  wire [7:0] _s1_vb_T_4 = {1'h1,s1_idx_msbs,s1_idx_lsbs}; // @[Cat.scala 30:58]
  wire [511:0] _s1_vb_T_5 = vb_array >> _s1_vb_T_4; // @[ICache.scala 272:25]
  wire  s1_vb_1 = _s1_vb_T_5[0]; // @[ICache.scala 272:25]
  wire [24:0] tag_1 = tag_array_RW0_rdata_1[24:0]; // @[package.scala 154:13]
  wire  tagMatch_1 = s1_vb_1 & tag_1 == s1_tag; // @[ICache.scala 275:26]
  wire [8:0] _s1_vb_T_8 = {2'h2,s1_idx_msbs,s1_idx_lsbs}; // @[Cat.scala 30:58]
  wire [511:0] _s1_vb_T_9 = vb_array >> _s1_vb_T_8; // @[ICache.scala 272:25]
  wire  s1_vb_2 = _s1_vb_T_9[0]; // @[ICache.scala 272:25]
  wire [24:0] tag_2 = tag_array_RW0_rdata_2[24:0]; // @[package.scala 154:13]
  wire  tagMatch_2 = s1_vb_2 & tag_2 == s1_tag; // @[ICache.scala 275:26]
  wire [8:0] _s1_vb_T_12 = {2'h3,s1_idx_msbs,s1_idx_lsbs}; // @[Cat.scala 30:58]
  wire [511:0] _s1_vb_T_13 = vb_array >> _s1_vb_T_12; // @[ICache.scala 272:25]
  wire  s1_vb_3 = _s1_vb_T_13[0]; // @[ICache.scala 272:25]
  wire [24:0] tag_3 = tag_array_RW0_rdata_3[24:0]; // @[package.scala 154:13]
  wire  tagMatch_3 = s1_vb_3 & tag_3 == s1_tag; // @[ICache.scala 275:26]
  wire  s1_hit = tagMatch | tagMatch_1 | tagMatch_2 | tagMatch_3; // @[ICache.scala 184:35]
  reg  s2_valid; // @[ICache.scala 186:25]
  reg  s2_hit; // @[ICache.scala 187:23]
  reg  invalidated; // @[ICache.scala 189:24]
  reg  refill_valid; // @[ICache.scala 190:29]
  reg  send_hint; // @[ICache.scala 191:26]
  wire  s2_miss = s2_valid & ~s2_hit & ~io_s2_kill; // @[ICache.scala 194:37]
  reg  s2_request_refill_REG; // @[ICache.scala 196:45]
  wire  s2_request_refill = s2_miss & s2_request_refill_REG; // @[ICache.scala 196:35]
  wire  tl_out_a_valid = send_hint | s2_request_refill; // @[ICache.scala 453:22 ICache.scala 454:22 ICache.scala 431:18]
  wire  _refill_fire_T = auto_master_out_a_ready & tl_out_a_valid; // @[Decoupled.scala 40:37]
  wire  _refill_fire_T_1 = ~send_hint; // @[ICache.scala 192:40]
  wire  refill_fire = _refill_fire_T & _refill_fire_T_1; // @[ICache.scala 192:37]
  reg  hint_outstanding; // @[ICache.scala 193:33]
  wire  s1_can_request_refill = ~(s2_miss | refill_valid); // @[ICache.scala 195:31]
  wire  _refill_paddr_T = s1_valid & s1_can_request_refill; // @[ICache.scala 197:54]
  reg [36:0] refill_paddr; // @[Reg.scala 15:16]
  reg [38:0] refill_vaddr; // @[Reg.scala 15:16]
  wire [24:0] refill_tag = refill_paddr[36:12]; // @[ICache.scala 200:104]
  wire [5:0] refill_idx_lsbs = refill_paddr[11:6]; // @[ICache.scala 496:21]
  wire  refill_idx_msbs = refill_vaddr[12]; // @[ICache.scala 497:65]
  wire [6:0] refill_idx = {refill_idx_msbs,refill_idx_lsbs}; // @[package.scala 213:38]
  wire  refill_one_beat_opdata = auto_master_out_d_bits_opcode[0]; // @[Edges.scala 105:36]
  wire  refill_one_beat = auto_master_out_d_valid & refill_one_beat_opdata; // @[ICache.scala 202:41]
  wire  _io_req_ready_T_2 = ~refill_one_beat; // @[ICache.scala 204:19]
  wire [22:0] _beats1_decode_T_1 = 23'hff << auto_master_out_d_bits_size; // @[package.scala 234:77]
  wire [7:0] _beats1_decode_T_3 = ~_beats1_decode_T_1[7:0]; // @[package.scala 234:46]
  wire [3:0] beats1_decode = _beats1_decode_T_3[7:4]; // @[Edges.scala 219:59]
  wire [3:0] beats1 = refill_one_beat_opdata ? beats1_decode : 4'h0; // @[Edges.scala 220:14]
  reg [3:0] counter; // @[Edges.scala 228:27]
  wire [3:0] counter1 = counter - 4'h1; // @[Edges.scala 229:28]
  wire  first = counter == 4'h0; // @[Edges.scala 230:25]
  wire  last = counter == 4'h1 | beats1 == 4'h0; // @[Edges.scala 231:37]
  wire  d_done = last & auto_master_out_d_valid; // @[Edges.scala 232:22]
  wire [3:0] _count_T = ~counter1; // @[Edges.scala 233:27]
  wire [3:0] refill_cnt = beats1 & _count_T; // @[Edges.scala 233:25]
  wire  refill_done = refill_one_beat & d_done; // @[ICache.scala 208:37]
  wire [7:0] repl_way_v0_lo = {repl_way_v0_prng_io_out_7,repl_way_v0_prng_io_out_6,repl_way_v0_prng_io_out_5,
    repl_way_v0_prng_io_out_4,repl_way_v0_prng_io_out_3,repl_way_v0_prng_io_out_2,repl_way_v0_prng_io_out_1,
    repl_way_v0_prng_io_out_0}; // @[PRNG.scala 86:17]
  wire [15:0] _repl_way_v0_T = {repl_way_v0_prng_io_out_15,repl_way_v0_prng_io_out_14,repl_way_v0_prng_io_out_13,
    repl_way_v0_prng_io_out_12,repl_way_v0_prng_io_out_11,repl_way_v0_prng_io_out_10,repl_way_v0_prng_io_out_9,
    repl_way_v0_prng_io_out_8,repl_way_v0_lo}; // @[PRNG.scala 86:17]
  wire [1:0] repl_way_v0 = _repl_way_v0_T[1:0]; // @[ICache.scala 214:35]
  wire  _tag_rdata_T_2 = ~refill_done & s0_valid; // @[ICache.scala 231:83]
  reg  accruedRefillError; // @[ICache.scala 232:31]
  wire  refillError = auto_master_out_d_bits_corrupt | refill_cnt > 4'h0 & accruedRefillError; // @[ICache.scala 233:43]
  wire [8:0] _vb_array_T = {repl_way_v0,refill_idx_msbs,refill_idx_lsbs}; // @[Cat.scala 30:58]
  wire  _vb_array_T_1 = ~invalidated; // @[ICache.scala 248:75]
  wire [511:0] _vb_array_T_3 = 512'h1 << _vb_array_T; // @[ICache.scala 248:32]
  wire [511:0] _vb_array_T_4 = vb_array | _vb_array_T_3; // @[ICache.scala 248:32]
  wire [511:0] _vb_array_T_5 = ~vb_array; // @[ICache.scala 248:32]
  wire [511:0] _vb_array_T_6 = _vb_array_T_5 | _vb_array_T_3; // @[ICache.scala 248:32]
  wire [511:0] _vb_array_T_7 = ~_vb_array_T_6; // @[ICache.scala 248:32]
  wire  _GEN_30 = io_invalidate | invalidated; // @[ICache.scala 251:21 ICache.scala 253:17 ICache.scala 189:24]
  wire  tl_error = tag_array_RW0_rdata_0[25]; // @[package.scala 154:13]
  wire  s1_tl_error_0 = tagMatch & tl_error; // @[ICache.scala 277:32]
  wire  tl_error_1 = tag_array_RW0_rdata_1[25]; // @[package.scala 154:13]
  wire  s1_tl_error_1 = tagMatch_1 & tl_error_1; // @[ICache.scala 277:32]
  wire  tl_error_2 = tag_array_RW0_rdata_2[25]; // @[package.scala 154:13]
  wire  s1_tl_error_2 = tagMatch_2 & tl_error_2; // @[ICache.scala 277:32]
  wire  tl_error_3 = tag_array_RW0_rdata_3[25]; // @[package.scala 154:13]
  wire  s1_tl_error_3 = tagMatch_3 & tl_error_3; // @[ICache.scala 277:32]
  wire  _s0_ren_T_1 = io_req_bits_addr[3:2] == 2'h0; // @[ICache.scala 295:111]
  wire  s0_ren = s0_valid & _s0_ren_T_1; // @[ICache.scala 297:28]
  wire  wen = refill_one_beat & _vb_array_T_1; // @[ICache.scala 298:32]
  wire [8:0] _mem_idx_T = {refill_idx, 2'h0}; // @[ICache.scala 299:52]
  wire [8:0] _GEN_7 = {{5'd0}, refill_cnt}; // @[ICache.scala 299:79]
  wire [8:0] _mem_idx_T_1 = _mem_idx_T | _GEN_7; // @[ICache.scala 299:79]
  wire  _dout_T_1 = ~wen & s0_ren; // @[ICache.scala 308:46]
  wire [31:0] _GEN_54 = data_arrays_0_RW0_rdata_0; // @[ICache.scala 309:71 ICache.scala 310:15]
  wire [31:0] _GEN_55 = data_arrays_0_RW0_rdata_1; // @[ICache.scala 309:71 ICache.scala 310:15]
  wire [31:0] _GEN_56 = data_arrays_0_RW0_rdata_2; // @[ICache.scala 309:71 ICache.scala 310:15]
  wire [31:0] _GEN_57 = data_arrays_0_RW0_rdata_3; // @[ICache.scala 309:71 ICache.scala 310:15]
  wire  _s0_ren_T_6 = io_req_bits_addr[3:2] == 2'h1; // @[ICache.scala 295:111]
  wire  s0_ren_1 = s0_valid & _s0_ren_T_6; // @[ICache.scala 297:28]
  wire  _dout_T_5 = ~wen & s0_ren_1; // @[ICache.scala 308:46]
  wire  _T_41 = io_s1_paddr[3:2] == 2'h1; // @[ICache.scala 295:111]
  wire  _s0_ren_T_11 = io_req_bits_addr[3:2] == 2'h2; // @[ICache.scala 295:111]
  wire  s0_ren_2 = s0_valid & _s0_ren_T_11; // @[ICache.scala 297:28]
  wire  _dout_T_9 = ~wen & s0_ren_2; // @[ICache.scala 308:46]
  wire  _T_48 = io_s1_paddr[3:2] == 2'h2; // @[ICache.scala 295:111]
  wire  _s0_ren_T_16 = io_req_bits_addr[3:2] == 2'h3; // @[ICache.scala 295:111]
  wire  s0_ren_3 = s0_valid & _s0_ren_T_16; // @[ICache.scala 297:28]
  wire  _dout_T_13 = ~wen & s0_ren_3; // @[ICache.scala 308:46]
  wire  _T_55 = io_s1_paddr[3:2] == 2'h3; // @[ICache.scala 295:111]
  reg  s2_tag_hit_0; // @[Reg.scala 15:16]
  reg  s2_tag_hit_1; // @[Reg.scala 15:16]
  reg  s2_tag_hit_2; // @[Reg.scala 15:16]
  reg  s2_tag_hit_3; // @[Reg.scala 15:16]
  reg [31:0] s2_dout_0; // @[Reg.scala 15:16]
  reg [31:0] s2_dout_1; // @[Reg.scala 15:16]
  reg [31:0] s2_dout_2; // @[Reg.scala 15:16]
  reg [31:0] s2_dout_3; // @[Reg.scala 15:16]
  wire [31:0] _s2_way_mux_T = s2_tag_hit_0 ? s2_dout_0 : 32'h0; // @[Mux.scala 27:72]
  wire [31:0] _s2_way_mux_T_1 = s2_tag_hit_1 ? s2_dout_1 : 32'h0; // @[Mux.scala 27:72]
  wire [31:0] _s2_way_mux_T_2 = s2_tag_hit_2 ? s2_dout_2 : 32'h0; // @[Mux.scala 27:72]
  wire [31:0] _s2_way_mux_T_3 = s2_tag_hit_3 ? s2_dout_3 : 32'h0; // @[Mux.scala 27:72]
  wire [31:0] _s2_way_mux_T_4 = _s2_way_mux_T | _s2_way_mux_T_1; // @[Mux.scala 27:72]
  wire [31:0] _s2_way_mux_T_5 = _s2_way_mux_T_4 | _s2_way_mux_T_2; // @[Mux.scala 27:72]
  wire [3:0] _s2_tl_error_T = {s1_tl_error_3,s1_tl_error_2,s1_tl_error_1,s1_tl_error_0}; // @[ICache.scala 326:43]
  wire  _s2_tl_error_T_1 = |_s2_tl_error_T; // @[ICache.scala 326:50]
  reg  s2_tl_error; // @[Reg.scala 15:16]
  wire [36:0] bundleOut_0_a_bits_a_address = {refill_paddr[36:6], 6'h0}; // @[ICache.scala 434:64]
  wire [6:0] _T_58 = refill_idx_lsbs + 6'h1; // @[ICache.scala 438:84]
  wire  crosses_page = _T_58[6]; // @[package.scala 154:13]
  wire [5:0] bundleOut_0_a_bits_lo = _T_58[5:0]; // @[package.scala 154:13]
  wire  _GEN_155 = send_hint | hint_outstanding; // @[ICache.scala 441:24 ICache.scala 443:26 ICache.scala 193:33]
  wire  _T_62 = auto_master_out_d_valid & _io_req_ready_T_2; // @[ICache.scala 449:27]
  wire [30:0] _bundleOut_0_a_bits_T_2 = {refill_tag,bundleOut_0_a_bits_lo}; // @[Cat.scala 30:58]
  wire [36:0] bundleOut_0_a_bits_a_1_address = {_bundleOut_0_a_bits_T_2, 6'h0}; // @[ICache.scala 457:80]
  wire  _T_73 = ~refill_valid; // @[ICache.scala 464:12]
  wire  _GEN_177 = refill_fire | refill_valid; // @[ICache.scala 487:22 ICache.scala 487:37 ICache.scala 190:29]
  RHEA__MaxPeriodFibonacciLFSR_4 repl_way_v0_prng ( // @[PRNG.scala 82:22]
    .rf_reset(repl_way_v0_prng_rf_reset),
    .clock(repl_way_v0_prng_clock),
    .reset(repl_way_v0_prng_reset),
    .io_increment(repl_way_v0_prng_io_increment),
    .io_out_0(repl_way_v0_prng_io_out_0),
    .io_out_1(repl_way_v0_prng_io_out_1),
    .io_out_2(repl_way_v0_prng_io_out_2),
    .io_out_3(repl_way_v0_prng_io_out_3),
    .io_out_4(repl_way_v0_prng_io_out_4),
    .io_out_5(repl_way_v0_prng_io_out_5),
    .io_out_6(repl_way_v0_prng_io_out_6),
    .io_out_7(repl_way_v0_prng_io_out_7),
    .io_out_8(repl_way_v0_prng_io_out_8),
    .io_out_9(repl_way_v0_prng_io_out_9),
    .io_out_10(repl_way_v0_prng_io_out_10),
    .io_out_11(repl_way_v0_prng_io_out_11),
    .io_out_12(repl_way_v0_prng_io_out_12),
    .io_out_13(repl_way_v0_prng_io_out_13),
    .io_out_14(repl_way_v0_prng_io_out_14),
    .io_out_15(repl_way_v0_prng_io_out_15)
  );
  RHEA__tag_array_0 tag_array ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(tag_array_RW0_addr),
    .RW0_en(tag_array_RW0_en),
    .RW0_clk(tag_array_RW0_clk),
    .RW0_wmode(tag_array_RW0_wmode),
    .RW0_wdata_0(tag_array_RW0_wdata_0),
    .RW0_wdata_1(tag_array_RW0_wdata_1),
    .RW0_wdata_2(tag_array_RW0_wdata_2),
    .RW0_wdata_3(tag_array_RW0_wdata_3),
    .RW0_rdata_0(tag_array_RW0_rdata_0),
    .RW0_rdata_1(tag_array_RW0_rdata_1),
    .RW0_rdata_2(tag_array_RW0_rdata_2),
    .RW0_rdata_3(tag_array_RW0_rdata_3),
    .RW0_wmask_0(tag_array_RW0_wmask_0),
    .RW0_wmask_1(tag_array_RW0_wmask_1),
    .RW0_wmask_2(tag_array_RW0_wmask_2),
    .RW0_wmask_3(tag_array_RW0_wmask_3)
  );
  RHEA__data_arrays_0_0 data_arrays_0 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(data_arrays_0_RW0_addr),
    .RW0_en(data_arrays_0_RW0_en),
    .RW0_clk(data_arrays_0_RW0_clk),
    .RW0_wmode(data_arrays_0_RW0_wmode),
    .RW0_wdata_0(data_arrays_0_RW0_wdata_0),
    .RW0_wdata_1(data_arrays_0_RW0_wdata_1),
    .RW0_wdata_2(data_arrays_0_RW0_wdata_2),
    .RW0_wdata_3(data_arrays_0_RW0_wdata_3),
    .RW0_rdata_0(data_arrays_0_RW0_rdata_0),
    .RW0_rdata_1(data_arrays_0_RW0_rdata_1),
    .RW0_rdata_2(data_arrays_0_RW0_rdata_2),
    .RW0_rdata_3(data_arrays_0_RW0_rdata_3),
    .RW0_wmask_0(data_arrays_0_RW0_wmask_0),
    .RW0_wmask_1(data_arrays_0_RW0_wmask_1),
    .RW0_wmask_2(data_arrays_0_RW0_wmask_2),
    .RW0_wmask_3(data_arrays_0_RW0_wmask_3)
  );
  RHEA__data_arrays_0_0 data_arrays_1 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(data_arrays_1_RW0_addr),
    .RW0_en(data_arrays_1_RW0_en),
    .RW0_clk(data_arrays_1_RW0_clk),
    .RW0_wmode(data_arrays_1_RW0_wmode),
    .RW0_wdata_0(data_arrays_1_RW0_wdata_0),
    .RW0_wdata_1(data_arrays_1_RW0_wdata_1),
    .RW0_wdata_2(data_arrays_1_RW0_wdata_2),
    .RW0_wdata_3(data_arrays_1_RW0_wdata_3),
    .RW0_rdata_0(data_arrays_1_RW0_rdata_0),
    .RW0_rdata_1(data_arrays_1_RW0_rdata_1),
    .RW0_rdata_2(data_arrays_1_RW0_rdata_2),
    .RW0_rdata_3(data_arrays_1_RW0_rdata_3),
    .RW0_wmask_0(data_arrays_1_RW0_wmask_0),
    .RW0_wmask_1(data_arrays_1_RW0_wmask_1),
    .RW0_wmask_2(data_arrays_1_RW0_wmask_2),
    .RW0_wmask_3(data_arrays_1_RW0_wmask_3)
  );
  RHEA__data_arrays_0_0 data_arrays_2 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(data_arrays_2_RW0_addr),
    .RW0_en(data_arrays_2_RW0_en),
    .RW0_clk(data_arrays_2_RW0_clk),
    .RW0_wmode(data_arrays_2_RW0_wmode),
    .RW0_wdata_0(data_arrays_2_RW0_wdata_0),
    .RW0_wdata_1(data_arrays_2_RW0_wdata_1),
    .RW0_wdata_2(data_arrays_2_RW0_wdata_2),
    .RW0_wdata_3(data_arrays_2_RW0_wdata_3),
    .RW0_rdata_0(data_arrays_2_RW0_rdata_0),
    .RW0_rdata_1(data_arrays_2_RW0_rdata_1),
    .RW0_rdata_2(data_arrays_2_RW0_rdata_2),
    .RW0_rdata_3(data_arrays_2_RW0_rdata_3),
    .RW0_wmask_0(data_arrays_2_RW0_wmask_0),
    .RW0_wmask_1(data_arrays_2_RW0_wmask_1),
    .RW0_wmask_2(data_arrays_2_RW0_wmask_2),
    .RW0_wmask_3(data_arrays_2_RW0_wmask_3)
  );
  RHEA__data_arrays_0_0 data_arrays_3 ( // @[DescribedSRAM.scala 18:26]
    .RW0_addr(data_arrays_3_RW0_addr),
    .RW0_en(data_arrays_3_RW0_en),
    .RW0_clk(data_arrays_3_RW0_clk),
    .RW0_wmode(data_arrays_3_RW0_wmode),
    .RW0_wdata_0(data_arrays_3_RW0_wdata_0),
    .RW0_wdata_1(data_arrays_3_RW0_wdata_1),
    .RW0_wdata_2(data_arrays_3_RW0_wdata_2),
    .RW0_wdata_3(data_arrays_3_RW0_wdata_3),
    .RW0_rdata_0(data_arrays_3_RW0_rdata_0),
    .RW0_rdata_1(data_arrays_3_RW0_rdata_1),
    .RW0_rdata_2(data_arrays_3_RW0_rdata_2),
    .RW0_rdata_3(data_arrays_3_RW0_rdata_3),
    .RW0_wmask_0(data_arrays_3_RW0_wmask_0),
    .RW0_wmask_1(data_arrays_3_RW0_wmask_1),
    .RW0_wmask_2(data_arrays_3_RW0_wmask_2),
    .RW0_wmask_3(data_arrays_3_RW0_wmask_3)
  );
  assign repl_way_v0_prng_rf_reset = rf_reset;
  assign auto_master_out_a_valid = send_hint | s2_request_refill; // @[ICache.scala 453:22 ICache.scala 454:22 ICache.scala 431:18]
  assign auto_master_out_a_bits_opcode = send_hint ? 3'h5 : 3'h4; // @[ICache.scala 453:22 ICache.scala 455:21 ICache.scala 432:17]
  assign auto_master_out_a_bits_source = send_hint; // @[ICache.scala 453:22 ICache.scala 455:21 ICache.scala 432:17]
  assign auto_master_out_a_bits_address = send_hint ? bundleOut_0_a_bits_a_1_address : bundleOut_0_a_bits_a_address; // @[ICache.scala 453:22 ICache.scala 455:21 ICache.scala 432:17]
  assign io_req_ready = ~refill_one_beat; // @[ICache.scala 204:19]
  assign io_resp_valid = s2_valid & s2_hit; // @[ICache.scala 352:33]
  assign io_resp_bits_data = _s2_way_mux_T_5 | _s2_way_mux_T_3; // @[Mux.scala 27:72]
  assign io_resp_bits_ae = s2_tl_error; // @[ICache.scala 350:23]
  assign io_perf_acquire = _refill_fire_T & _refill_fire_T_1; // @[ICache.scala 192:37]
  assign io_keep_clock_enabled = s1_valid | s2_valid | refill_valid | send_hint | hint_outstanding; // @[ICache.scala 493:55]
  assign repl_way_v0_prng_clock = clock;
  assign repl_way_v0_prng_reset = reset;
  assign repl_way_v0_prng_io_increment = _refill_fire_T & _refill_fire_T_1; // @[ICache.scala 192:37]
  assign tag_array_RW0_wdata_0 = {refillError,refill_tag}; // @[Cat.scala 30:58]
  assign tag_array_RW0_wdata_1 = {refillError,refill_tag}; // @[Cat.scala 30:58]
  assign tag_array_RW0_wdata_2 = {refillError,refill_tag}; // @[Cat.scala 30:58]
  assign tag_array_RW0_wdata_3 = {refillError,refill_tag}; // @[Cat.scala 30:58]
  assign tag_array_RW0_wmask_0 = repl_way_v0 == 2'h0; // @[ICache.scala 237:88]
  assign tag_array_RW0_wmask_1 = repl_way_v0 == 2'h1; // @[ICache.scala 237:88]
  assign tag_array_RW0_wmask_2 = repl_way_v0 == 2'h2; // @[ICache.scala 237:88]
  assign tag_array_RW0_wmask_3 = repl_way_v0 == 2'h3; // @[ICache.scala 237:88]
  assign data_arrays_0_RW0_wdata_0 = auto_master_out_d_bits_data[31:0]; // @[ICache.scala 304:71]
  assign data_arrays_0_RW0_wdata_1 = auto_master_out_d_bits_data[31:0]; // @[ICache.scala 304:71]
  assign data_arrays_0_RW0_wdata_2 = auto_master_out_d_bits_data[31:0]; // @[ICache.scala 304:71]
  assign data_arrays_0_RW0_wdata_3 = auto_master_out_d_bits_data[31:0]; // @[ICache.scala 304:71]
  assign data_arrays_0_RW0_wmask_0 = repl_way_v0 == 2'h0; // @[ICache.scala 306:93]
  assign data_arrays_0_RW0_wmask_1 = repl_way_v0 == 2'h1; // @[ICache.scala 306:93]
  assign data_arrays_0_RW0_wmask_2 = repl_way_v0 == 2'h2; // @[ICache.scala 306:93]
  assign data_arrays_0_RW0_wmask_3 = repl_way_v0 == 2'h3; // @[ICache.scala 306:93]
  assign data_arrays_1_RW0_wdata_0 = auto_master_out_d_bits_data[63:32]; // @[ICache.scala 304:71]
  assign data_arrays_1_RW0_wdata_1 = auto_master_out_d_bits_data[63:32]; // @[ICache.scala 304:71]
  assign data_arrays_1_RW0_wdata_2 = auto_master_out_d_bits_data[63:32]; // @[ICache.scala 304:71]
  assign data_arrays_1_RW0_wdata_3 = auto_master_out_d_bits_data[63:32]; // @[ICache.scala 304:71]
  assign data_arrays_1_RW0_wmask_0 = repl_way_v0 == 2'h0; // @[ICache.scala 306:93]
  assign data_arrays_1_RW0_wmask_1 = repl_way_v0 == 2'h1; // @[ICache.scala 306:93]
  assign data_arrays_1_RW0_wmask_2 = repl_way_v0 == 2'h2; // @[ICache.scala 306:93]
  assign data_arrays_1_RW0_wmask_3 = repl_way_v0 == 2'h3; // @[ICache.scala 306:93]
  assign data_arrays_2_RW0_wdata_0 = auto_master_out_d_bits_data[95:64]; // @[ICache.scala 304:71]
  assign data_arrays_2_RW0_wdata_1 = auto_master_out_d_bits_data[95:64]; // @[ICache.scala 304:71]
  assign data_arrays_2_RW0_wdata_2 = auto_master_out_d_bits_data[95:64]; // @[ICache.scala 304:71]
  assign data_arrays_2_RW0_wdata_3 = auto_master_out_d_bits_data[95:64]; // @[ICache.scala 304:71]
  assign data_arrays_2_RW0_wmask_0 = repl_way_v0 == 2'h0; // @[ICache.scala 306:93]
  assign data_arrays_2_RW0_wmask_1 = repl_way_v0 == 2'h1; // @[ICache.scala 306:93]
  assign data_arrays_2_RW0_wmask_2 = repl_way_v0 == 2'h2; // @[ICache.scala 306:93]
  assign data_arrays_2_RW0_wmask_3 = repl_way_v0 == 2'h3; // @[ICache.scala 306:93]
  assign data_arrays_3_RW0_wdata_0 = auto_master_out_d_bits_data[127:96]; // @[ICache.scala 304:71]
  assign data_arrays_3_RW0_wdata_1 = auto_master_out_d_bits_data[127:96]; // @[ICache.scala 304:71]
  assign data_arrays_3_RW0_wdata_2 = auto_master_out_d_bits_data[127:96]; // @[ICache.scala 304:71]
  assign data_arrays_3_RW0_wdata_3 = auto_master_out_d_bits_data[127:96]; // @[ICache.scala 304:71]
  assign data_arrays_3_RW0_wmask_0 = repl_way_v0 == 2'h0; // @[ICache.scala 306:93]
  assign data_arrays_3_RW0_wmask_1 = repl_way_v0 == 2'h1; // @[ICache.scala 306:93]
  assign data_arrays_3_RW0_wmask_2 = repl_way_v0 == 2'h2; // @[ICache.scala 306:93]
  assign data_arrays_3_RW0_wmask_3 = repl_way_v0 == 2'h3; // @[ICache.scala 306:93]
  assign tag_array_RW0_wmode = refill_one_beat & d_done; // @[ICache.scala 208:37]
  assign tag_array_RW0_clk = clock;
  assign tag_array_RW0_en = _tag_rdata_T_2 | refill_done;
  assign tag_array_RW0_addr = refill_done ? refill_idx : io_req_bits_addr[12:6];
  assign data_arrays_0_RW0_wmode = refill_one_beat & _vb_array_T_1; // @[ICache.scala 298:32]
  assign data_arrays_0_RW0_clk = clock;
  assign data_arrays_0_RW0_en = _dout_T_1 | wen;
  assign data_arrays_0_RW0_addr = refill_one_beat ? _mem_idx_T_1 : io_req_bits_addr[12:4]; // @[ICache.scala 299:22]
  assign data_arrays_1_RW0_wmode = refill_one_beat & _vb_array_T_1; // @[ICache.scala 298:32]
  assign data_arrays_1_RW0_clk = clock;
  assign data_arrays_1_RW0_en = _dout_T_5 | wen;
  assign data_arrays_1_RW0_addr = refill_one_beat ? _mem_idx_T_1 : io_req_bits_addr[12:4]; // @[ICache.scala 299:22]
  assign data_arrays_2_RW0_wmode = refill_one_beat & _vb_array_T_1; // @[ICache.scala 298:32]
  assign data_arrays_2_RW0_clk = clock;
  assign data_arrays_2_RW0_en = _dout_T_9 | wen;
  assign data_arrays_2_RW0_addr = refill_one_beat ? _mem_idx_T_1 : io_req_bits_addr[12:4]; // @[ICache.scala 299:22]
  assign data_arrays_3_RW0_wmode = refill_one_beat & _vb_array_T_1; // @[ICache.scala 298:32]
  assign data_arrays_3_RW0_clk = clock;
  assign data_arrays_3_RW0_en = _dout_T_13 | wen;
  assign data_arrays_3_RW0_addr = refill_one_beat ? _mem_idx_T_1 : io_req_bits_addr[12:4]; // @[ICache.scala 299:22]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_valid <= 1'h0;
    end else if (reset) begin
      s1_valid <= 1'h0;
    end else begin
      s1_valid <= s0_valid;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s1_vaddr <= 39'h0;
    end else if (s0_valid) begin
      s1_vaddr <= io_req_bits_addr;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      vb_array <= 512'h0;
    end else if (reset) begin
      vb_array <= 512'h0;
    end else if (io_invalidate) begin
      vb_array <= 512'h0;
    end else if (refill_one_beat) begin
      if (refill_done & ~invalidated) begin
        vb_array <= _vb_array_T_4;
      end else begin
        vb_array <= _vb_array_T_7;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_valid <= 1'h0;
    end else if (reset) begin
      s2_valid <= 1'h0;
    end else begin
      s2_valid <= s1_valid & ~io_s1_kill;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_hit <= 1'h0;
    end else begin
      s2_hit <= s1_hit;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      invalidated <= 1'h0;
    end else if (_T_73) begin
      invalidated <= 1'h0;
    end else begin
      invalidated <= _GEN_30;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      refill_valid <= 1'h0;
    end else if (reset) begin
      refill_valid <= 1'h0;
    end else if (refill_done) begin
      refill_valid <= 1'h0;
    end else begin
      refill_valid <= _GEN_177;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      send_hint <= 1'h0;
    end else if (reset) begin
      send_hint <= 1'h0;
    end else if (refill_done) begin
      send_hint <= 1'h0;
    end else if (_refill_fire_T) begin
      if (send_hint) begin
        send_hint <= 1'h0;
      end else begin
        send_hint <= ~hint_outstanding & io_s2_prefetch & ~crosses_page;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_request_refill_REG <= 1'h0;
    end else begin
      s2_request_refill_REG <= ~(s2_miss | refill_valid);
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      hint_outstanding <= 1'h0;
    end else if (reset) begin
      hint_outstanding <= 1'h0;
    end else if (_T_62) begin
      hint_outstanding <= 1'h0;
    end else if (_refill_fire_T) begin
      hint_outstanding <= _GEN_155;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      refill_paddr <= 37'h0;
    end else if (_refill_paddr_T) begin
      refill_paddr <= io_s1_paddr;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      refill_vaddr <= 39'h0;
    end else if (_refill_paddr_T) begin
      refill_vaddr <= s1_vaddr;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      counter <= 4'h0;
    end else if (reset) begin
      counter <= 4'h0;
    end else if (auto_master_out_d_valid) begin
      if (first) begin
        if (refill_one_beat_opdata) begin
          counter <= beats1_decode;
        end else begin
          counter <= 4'h0;
        end
      end else begin
        counter <= counter1;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      accruedRefillError <= 1'h0;
    end else if (refill_one_beat) begin
      accruedRefillError <= refillError;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_tag_hit_0 <= 1'h0;
    end else if (s1_valid) begin
      s2_tag_hit_0 <= tagMatch;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_tag_hit_1 <= 1'h0;
    end else if (s1_valid) begin
      s2_tag_hit_1 <= tagMatch_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_tag_hit_2 <= 1'h0;
    end else if (s1_valid) begin
      s2_tag_hit_2 <= tagMatch_2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_tag_hit_3 <= 1'h0;
    end else if (s1_valid) begin
      s2_tag_hit_3 <= tagMatch_3;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_dout_0 <= 32'h0;
    end else if (s1_valid) begin
      if (_T_55) begin
        s2_dout_0 <= data_arrays_3_RW0_rdata_0;
      end else if (_T_48) begin
        s2_dout_0 <= data_arrays_2_RW0_rdata_0;
      end else if (_T_41) begin
        s2_dout_0 <= data_arrays_1_RW0_rdata_0;
      end else begin
        s2_dout_0 <= _GEN_54;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_dout_1 <= 32'h0;
    end else if (s1_valid) begin
      if (_T_55) begin
        s2_dout_1 <= data_arrays_3_RW0_rdata_1;
      end else if (_T_48) begin
        s2_dout_1 <= data_arrays_2_RW0_rdata_1;
      end else if (_T_41) begin
        s2_dout_1 <= data_arrays_1_RW0_rdata_1;
      end else begin
        s2_dout_1 <= _GEN_55;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_dout_2 <= 32'h0;
    end else if (s1_valid) begin
      if (_T_55) begin
        s2_dout_2 <= data_arrays_3_RW0_rdata_2;
      end else if (_T_48) begin
        s2_dout_2 <= data_arrays_2_RW0_rdata_2;
      end else if (_T_41) begin
        s2_dout_2 <= data_arrays_1_RW0_rdata_2;
      end else begin
        s2_dout_2 <= _GEN_56;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_dout_3 <= 32'h0;
    end else if (s1_valid) begin
      if (_T_55) begin
        s2_dout_3 <= data_arrays_3_RW0_rdata_3;
      end else if (_T_48) begin
        s2_dout_3 <= data_arrays_2_RW0_rdata_3;
      end else if (_T_41) begin
        s2_dout_3 <= data_arrays_1_RW0_rdata_3;
      end else begin
        s2_dout_3 <= _GEN_57;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      s2_tl_error <= 1'h0;
    end else if (s1_valid) begin
      s2_tl_error <= _s2_tl_error_T_1;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  s1_valid = _RAND_0[0:0];
  _RAND_1 = {2{`RANDOM}};
  s1_vaddr = _RAND_1[38:0];
  _RAND_2 = {16{`RANDOM}};
  vb_array = _RAND_2[511:0];
  _RAND_3 = {1{`RANDOM}};
  s2_valid = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  s2_hit = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  invalidated = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  refill_valid = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  send_hint = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  s2_request_refill_REG = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  hint_outstanding = _RAND_9[0:0];
  _RAND_10 = {2{`RANDOM}};
  refill_paddr = _RAND_10[36:0];
  _RAND_11 = {2{`RANDOM}};
  refill_vaddr = _RAND_11[38:0];
  _RAND_12 = {1{`RANDOM}};
  counter = _RAND_12[3:0];
  _RAND_13 = {1{`RANDOM}};
  accruedRefillError = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  s2_tag_hit_0 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  s2_tag_hit_1 = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  s2_tag_hit_2 = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  s2_tag_hit_3 = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  s2_dout_0 = _RAND_18[31:0];
  _RAND_19 = {1{`RANDOM}};
  s2_dout_1 = _RAND_19[31:0];
  _RAND_20 = {1{`RANDOM}};
  s2_dout_2 = _RAND_20[31:0];
  _RAND_21 = {1{`RANDOM}};
  s2_dout_3 = _RAND_21[31:0];
  _RAND_22 = {1{`RANDOM}};
  s2_tl_error = _RAND_22[0:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    s1_valid = 1'h0;
  end
  if (rf_reset) begin
    s1_vaddr = 39'h0;
  end
  if (rf_reset) begin
    vb_array = 512'h0;
  end
  if (rf_reset) begin
    s2_valid = 1'h0;
  end
  if (rf_reset) begin
    s2_hit = 1'h0;
  end
  if (rf_reset) begin
    invalidated = 1'h0;
  end
  if (rf_reset) begin
    refill_valid = 1'h0;
  end
  if (rf_reset) begin
    send_hint = 1'h0;
  end
  if (rf_reset) begin
    s2_request_refill_REG = 1'h0;
  end
  if (rf_reset) begin
    hint_outstanding = 1'h0;
  end
  if (rf_reset) begin
    refill_paddr = 37'h0;
  end
  if (rf_reset) begin
    refill_vaddr = 39'h0;
  end
  if (rf_reset) begin
    counter = 4'h0;
  end
  if (rf_reset) begin
    accruedRefillError = 1'h0;
  end
  if (rf_reset) begin
    s2_tag_hit_0 = 1'h0;
  end
  if (rf_reset) begin
    s2_tag_hit_1 = 1'h0;
  end
  if (rf_reset) begin
    s2_tag_hit_2 = 1'h0;
  end
  if (rf_reset) begin
    s2_tag_hit_3 = 1'h0;
  end
  if (rf_reset) begin
    s2_dout_0 = 32'h0;
  end
  if (rf_reset) begin
    s2_dout_1 = 32'h0;
  end
  if (rf_reset) begin
    s2_dout_2 = 32'h0;
  end
  if (rf_reset) begin
    s2_dout_3 = 32'h0;
  end
  if (rf_reset) begin
    s2_tl_error = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
