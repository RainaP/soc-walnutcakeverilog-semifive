module pll1426xe_wrap_reg (
    input               CLK__BUS                  ,
    input               RSTN__BUS                 ,
    input  [31:0]       APB3__BUS__PADDR          ,
    input               APB3__BUS__PENABLE        ,
    input               APB3__BUS__PSEL           ,
    input  [31:0]       APB3__BUS__PWDATA         ,
    input               APB3__BUS__PWRITE         ,
    output reg [31:0]   APB3__BUS__PRDATA         ,
    output reg          APB3__BUS__PREADY         ,
    output reg          APB3__BUS__PSLVERR        ,
    output reg [5:0]    REG__BUS__P               ,
    output reg [9:0]    REG__BUS__M               ,
    output reg [2:0]    REG__BUS__S               ,
    output reg [1:0]    REG__BUS__ICP             ,
    output reg [3:0]    REG__BUS__RSEL            ,
    output reg [4:0]    REG__BUS__EXTAFC          ,
    output reg [1:0]    REG__BUS__LOCK_CON_DLY    ,
    output reg [1:0]    REG__BUS__LOCK_CON_REV    ,
    output reg [1:0]    REG__BUS__LOCK_CON_IN     ,
    output reg [1:0]    REG__BUS__LOCK_CON_OUT    ,
    output reg          REG__BUS__RESETB          ,
    output reg          REG__BUS__BYPASS          ,
    output reg          REG__BUS__AFC_ENB         ,
    output reg          REG__BUS__FSEL            ,
    output reg          REG__BUS__FEED_EN         ,
    output reg          REG__BUS__LOCK_EN         ,
    output reg          REG__BUS__AFCINIT_SEL     ,
    output reg          REG__BUS__LRD_EN          ,
    output reg          REG__BUS__FOUT_MASK       ,
    output reg          REG__BUS__VCO_BOOST       ,
    output reg          REG__BUS__PBIAS_CTRL_EN   ,
    output reg          REG__BUS__PBIAS_CTRL      ,
    input  [4:0]        REG__BUS__AFC_CODE        ,
    input               REG__BUS__LOCK
);

wire reg_wen_0x0  = (APB3__BUS__PADDR[11:0] == 12'h0)  & APB3__BUS__PENABLE & APB3__BUS__PSEL & APB3__BUS__PWRITE;
wire reg_wen_0x4  = (APB3__BUS__PADDR[11:0] == 12'h4)  & APB3__BUS__PENABLE & APB3__BUS__PSEL & APB3__BUS__PWRITE;
wire reg_wen_0x8  = (APB3__BUS__PADDR[11:0] == 12'h8)  & APB3__BUS__PENABLE & APB3__BUS__PSEL & APB3__BUS__PWRITE;
wire reg_wen_0xc  = (APB3__BUS__PADDR[11:0] == 12'hc)  & APB3__BUS__PENABLE & APB3__BUS__PSEL & APB3__BUS__PWRITE;
wire reg_wen_0x10 = (APB3__BUS__PADDR[11:0] == 12'h10) & APB3__BUS__PENABLE & APB3__BUS__PSEL & APB3__BUS__PWRITE;
wire reg_wen_0x14 = (APB3__BUS__PADDR[11:0] == 12'h14) & APB3__BUS__PENABLE & APB3__BUS__PSEL & APB3__BUS__PWRITE;
wire reg_wen_0x18 = (APB3__BUS__PADDR[11:0] == 12'h18) & APB3__BUS__PENABLE & APB3__BUS__PSEL & APB3__BUS__PWRITE;
wire reg_wen_0x1c = (APB3__BUS__PADDR[11:0] == 12'h1c) & APB3__BUS__PENABLE & APB3__BUS__PSEL & APB3__BUS__PWRITE;
wire reg_wen_0x20 = (APB3__BUS__PADDR[11:0] == 12'h20) & APB3__BUS__PENABLE & APB3__BUS__PSEL & APB3__BUS__PWRITE;
//wire reg_wen_0x24 = (APB3__BUS__PADDR[11:0] == 12'h24) & APB3__BUS__PENABLE & APB3__BUS__PSEL & APB3__BUS__PWRITE;
//wire reg_wen_0x28 = (APB3__BUS__PADDR[11:0] == 12'h28) & APB3__BUS__PENABLE & APB3__BUS__PSEL & APB3__BUS__PWRITE;

wire reg_oen_0x0  = (APB3__BUS__PADDR[11:0] == 12'h0)  & APB3__BUS__PENABLE                   & !APB3__BUS__PWRITE;
wire reg_oen_0x4  = (APB3__BUS__PADDR[11:0] == 12'h4)  & APB3__BUS__PENABLE                   & !APB3__BUS__PWRITE;
wire reg_oen_0x8  = (APB3__BUS__PADDR[11:0] == 12'h8)  & APB3__BUS__PENABLE                   & !APB3__BUS__PWRITE;
wire reg_oen_0xc  = (APB3__BUS__PADDR[11:0] == 12'hc)  & APB3__BUS__PENABLE                   & !APB3__BUS__PWRITE;
wire reg_oen_0x10 = (APB3__BUS__PADDR[11:0] == 12'h10) & APB3__BUS__PENABLE                   & !APB3__BUS__PWRITE;
wire reg_oen_0x14 = (APB3__BUS__PADDR[11:0] == 12'h14) & APB3__BUS__PENABLE                   & !APB3__BUS__PWRITE;
wire reg_oen_0x18 = (APB3__BUS__PADDR[11:0] == 12'h18) & APB3__BUS__PENABLE                   & !APB3__BUS__PWRITE;
wire reg_oen_0x1c = (APB3__BUS__PADDR[11:0] == 12'h1c) & APB3__BUS__PENABLE                   & !APB3__BUS__PWRITE;
wire reg_oen_0x20 = (APB3__BUS__PADDR[11:0] == 12'h20) & APB3__BUS__PENABLE                   & !APB3__BUS__PWRITE;
wire reg_oen_0x24 = (APB3__BUS__PADDR[11:0] == 12'h24) & APB3__BUS__PENABLE                   & !APB3__BUS__PWRITE;
wire reg_oen_0x28 = (APB3__BUS__PADDR[11:0] == 12'h28) & APB3__BUS__PENABLE                   & !APB3__BUS__PWRITE;

// 0x0
wire [31:0] reg_0x0 = 32'h00000000; // IP Version

// 0x4
reg [31:0] reg_0x4;
always @(posedge CLK__BUS or negedge RSTN__BUS) begin
    if (~RSTN__BUS) begin
        REG__BUS__RESETB <= 1'b0;
    end
    else if (reg_wen_0x4) begin
        REG__BUS__RESETB <= APB3__BUS__PWDATA[0];
    end
end
always @(*) begin
    reg_0x4 = 0;
    reg_0x4[0] = REG__BUS__RESETB;
end

// 0x8
reg [31:0] reg_0x8;
always @(posedge CLK__BUS or negedge RSTN__BUS) begin
    if (~RSTN__BUS) begin
        REG__BUS__M <= 10'hFA;
        REG__BUS__P <= 6'hA;
    end
    else if (reg_wen_0x8) begin
        REG__BUS__M <= APB3__BUS__PWDATA[15:6];
        REG__BUS__P <= APB3__BUS__PWDATA[5:0];
    end
end
always @(*) begin
    reg_0x8 = 0;
    reg_0x8[15:6] = REG__BUS__M;
    reg_0x8[5:0] = REG__BUS__P;
end

// 0xc
reg [31:0] reg_0xc;
always @(posedge CLK__BUS or negedge RSTN__BUS) begin
    if (~RSTN__BUS) begin
        REG__BUS__FOUT_MASK  <= 1'b0;
        REG__BUS__S <= 3'h1;
        REG__BUS__BYPASS <= 1'b1;
    end
    else if (reg_wen_0xc) begin
        REG__BUS__FOUT_MASK <= APB3__BUS__PWDATA[4];
        REG__BUS__S <= APB3__BUS__PWDATA[3:1];
        REG__BUS__BYPASS <= APB3__BUS__PWDATA[0];
    end
end
always @(*) begin
    reg_0xc = 0;
    reg_0xc[4] = REG__BUS__FOUT_MASK;
    reg_0xc[3:1] = REG__BUS__S;
    reg_0xc[0] = REG__BUS__BYPASS;
end

// 0x10
reg [31:0] reg_0x10;
always @(posedge CLK__BUS or negedge RSTN__BUS) begin
    if (~RSTN__BUS) begin
        REG__BUS__LOCK_CON_REV <= 2'h0;
        REG__BUS__LOCK_CON_DLY <= 2'h3;
        REG__BUS__LOCK_CON_OUT <= 2'h3;
        REG__BUS__LOCK_CON_IN <= 2'h3;
        REG__BUS__LOCK_EN <= 1'b1;
    end
    else if (reg_wen_0x10) begin
        REG__BUS__LOCK_CON_REV <= APB3__BUS__PWDATA[8:7];
        REG__BUS__LOCK_CON_DLY <= APB3__BUS__PWDATA[6:5];
        REG__BUS__LOCK_CON_OUT <= APB3__BUS__PWDATA[4:3];
        REG__BUS__LOCK_CON_IN <= APB3__BUS__PWDATA[2:1];
        REG__BUS__LOCK_EN <= APB3__BUS__PWDATA[0];
    end
end
always @(*) begin
    reg_0x10 = 0;
    reg_0x10[8:7] = REG__BUS__LOCK_CON_REV;
    reg_0x10[6:5] = REG__BUS__LOCK_CON_DLY;
    reg_0x10[4:3] = REG__BUS__LOCK_CON_OUT;
    reg_0x10[2:1] = REG__BUS__LOCK_CON_IN;
    reg_0x10[0:0] = REG__BUS__LOCK_EN;
end

// 0x14
reg [31:0] reg_0x14;
always @(posedge CLK__BUS or negedge RSTN__BUS) begin
    if (~RSTN__BUS) begin
        REG__BUS__FSEL <= 1'b0;
        REG__BUS__FEED_EN <= 1'b0;
    end
    else if (reg_wen_0x14) begin
        REG__BUS__FSEL <= APB3__BUS__PWDATA[1];
        REG__BUS__FEED_EN <= APB3__BUS__PWDATA[0];
    end
end
always @(*) begin
    reg_0x14 = 0;
    reg_0x14[1] = REG__BUS__FSEL;
    reg_0x14[0] = REG__BUS__FEED_EN;
end

// 0x18
reg [31:0] reg_0x18;
always @(posedge CLK__BUS or negedge RSTN__BUS) begin
    if (~RSTN__BUS) begin
        REG__BUS__EXTAFC <= 4'h0;
        REG__BUS__LRD_EN <= 1'b1;
        REG__BUS__AFCINIT_SEL <= 1'b0;
        REG__BUS__AFC_ENB <= 1'b0;
    end
    else if (reg_wen_0x18) begin
        REG__BUS__EXTAFC <= APB3__BUS__PWDATA[7:3];
        REG__BUS__LRD_EN <= APB3__BUS__PWDATA[2];
        REG__BUS__AFCINIT_SEL <= APB3__BUS__PWDATA[1];
        REG__BUS__AFC_ENB <= APB3__BUS__PWDATA[0];
    end
end
always @(*) begin
    reg_0x18 = 0;
    reg_0x18[7:3] = REG__BUS__EXTAFC;
    reg_0x18[2] = REG__BUS__LRD_EN;
    reg_0x18[1] = REG__BUS__AFCINIT_SEL;
    reg_0x18[0] = REG__BUS__AFC_ENB;
end

// 0x1c
reg [31:0] reg_0x1c;
always @(posedge CLK__BUS or negedge RSTN__BUS) begin
    if (~RSTN__BUS) begin
        REG__BUS__RSEL <= 4'h0;
        REG__BUS__VCO_BOOST <= 1'b0;
        REG__BUS__ICP <= 2'h1;
    end
    else if (reg_wen_0x1c) begin
        REG__BUS__RSEL <= APB3__BUS__PWDATA[7:4];
        REG__BUS__VCO_BOOST <= APB3__BUS__PWDATA[2];
        REG__BUS__ICP <= APB3__BUS__PWDATA[1:0];
    end
end
always @(*) begin
    reg_0x1c = 0;
    reg_0x1c[7:4] = REG__BUS__RSEL;
    reg_0x1c[2:2] = REG__BUS__VCO_BOOST;
    reg_0x1c[1:0] = REG__BUS__ICP;
end

// 0x20
reg [31:0] reg_0x20;
always @(posedge CLK__BUS or negedge RSTN__BUS) begin
    if (~RSTN__BUS) begin
        REG__BUS__PBIAS_CTRL_EN <= 1'b0;
        REG__BUS__PBIAS_CTRL <= 1'b0;
    end
    else if (reg_wen_0x20) begin
        REG__BUS__PBIAS_CTRL_EN <= APB3__BUS__PWDATA[1];
        REG__BUS__PBIAS_CTRL <= APB3__BUS__PWDATA[0];
    end
end
always @(*) begin
    reg_0x20 = 0;
    reg_0x20[1] = REG__BUS__PBIAS_CTRL_EN;
    reg_0x20[0] = REG__BUS__PBIAS_CTRL;
end

// 0x24
wire [31:0] reg_0x24;
assign reg_0x24[31:1] = 0;
assign reg_0x24[0] = REG__BUS__LOCK;

// 0x28
wire [31:0] reg_0x28;
assign reg_0x28[31:5] = 0;
assign reg_0x28[4:0] = REG__BUS__AFC_CODE;


always @(*) begin
    APB3__BUS__PRDATA = 0;
    if (APB3__BUS__PENABLE) begin
        APB3__BUS__PRDATA =   reg_oen_0x0 ? reg_0x0 
                            : reg_oen_0x4 ? reg_0x4
                            : reg_oen_0x8 ? reg_0x8
                            : reg_oen_0xc ? reg_0xc
                            : reg_oen_0x10 ? reg_0x10
                            : reg_oen_0x14 ? reg_0x14
                            : reg_oen_0x18 ? reg_0x18
                            : reg_oen_0x1c ? reg_0x1c
                            : reg_oen_0x20 ? reg_0x20
                            : reg_oen_0x24 ? reg_0x24
                            : reg_oen_0x28 ? reg_0x28
                            : 32'h0;
    end
end

assign APB3__BUS__PREADY = 1'b1;
assign APB3__BUS__PSLVERR = 1'b0;
endmodule
// vim:tabstop=4:shiftwidth=4
