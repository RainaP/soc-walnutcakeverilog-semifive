//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__ShiftQueue_2(
  input          rf_reset,
  input          clock,
  input          reset,
  output         io_enq_ready,
  input          io_enq_valid,
  input  [2:0]   io_enq_bits_opcode,
  input  [1:0]   io_enq_bits_param,
  input  [3:0]   io_enq_bits_size,
  input  [4:0]   io_enq_bits_source,
  input  [36:0]  io_enq_bits_address,
  input  [15:0]  io_enq_bits_mask,
  input  [127:0] io_enq_bits_data,
  input          io_enq_bits_corrupt,
  input          io_deq_ready,
  output         io_deq_valid,
  output [2:0]   io_deq_bits_opcode,
  output [1:0]   io_deq_bits_param,
  output [3:0]   io_deq_bits_size,
  output [4:0]   io_deq_bits_source,
  output [36:0]  io_deq_bits_address,
  output [15:0]  io_deq_bits_mask,
  output [127:0] io_deq_bits_data,
  output         io_deq_bits_corrupt
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [63:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [127:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [63:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [127:0] _RAND_16;
  reg [31:0] _RAND_17;
`endif // RANDOMIZE_REG_INIT
  reg  valid_0; // @[ShiftQueue.scala 21:30]
  reg  valid_1; // @[ShiftQueue.scala 21:30]
  reg [2:0] elts_0_opcode; // @[ShiftQueue.scala 22:25]
  reg [1:0] elts_0_param; // @[ShiftQueue.scala 22:25]
  reg [3:0] elts_0_size; // @[ShiftQueue.scala 22:25]
  reg [4:0] elts_0_source; // @[ShiftQueue.scala 22:25]
  reg [36:0] elts_0_address; // @[ShiftQueue.scala 22:25]
  reg [15:0] elts_0_mask; // @[ShiftQueue.scala 22:25]
  reg [127:0] elts_0_data; // @[ShiftQueue.scala 22:25]
  reg  elts_0_corrupt; // @[ShiftQueue.scala 22:25]
  reg [2:0] elts_1_opcode; // @[ShiftQueue.scala 22:25]
  reg [1:0] elts_1_param; // @[ShiftQueue.scala 22:25]
  reg [3:0] elts_1_size; // @[ShiftQueue.scala 22:25]
  reg [4:0] elts_1_source; // @[ShiftQueue.scala 22:25]
  reg [36:0] elts_1_address; // @[ShiftQueue.scala 22:25]
  reg [15:0] elts_1_mask; // @[ShiftQueue.scala 22:25]
  reg [127:0] elts_1_data; // @[ShiftQueue.scala 22:25]
  reg  elts_1_corrupt; // @[ShiftQueue.scala 22:25]
  wire  _wen_T = io_enq_ready & io_enq_valid; // @[Decoupled.scala 40:37]
  wire  _wen_T_3 = valid_1 | _wen_T; // @[ShiftQueue.scala 30:28]
  wire  _wen_T_7 = _wen_T & ~valid_0; // @[ShiftQueue.scala 31:45]
  wire  wen = io_deq_ready ? _wen_T_3 : _wen_T_7; // @[ShiftQueue.scala 29:10]
  wire  _valid_0_T_6 = _wen_T | valid_0; // @[ShiftQueue.scala 37:45]
  wire  _wen_T_10 = _wen_T & valid_1; // @[ShiftQueue.scala 30:45]
  wire  _wen_T_13 = _wen_T & valid_0; // @[ShiftQueue.scala 31:25]
  wire  _wen_T_15 = _wen_T & valid_0 & ~valid_1; // @[ShiftQueue.scala 31:45]
  wire  wen_1 = io_deq_ready ? _wen_T_10 : _wen_T_15; // @[ShiftQueue.scala 29:10]
  wire  _valid_1_T_6 = _wen_T_13 | valid_1; // @[ShiftQueue.scala 37:45]
  assign io_enq_ready = ~valid_1; // @[ShiftQueue.scala 40:19]
  assign io_deq_valid = valid_0; // @[ShiftQueue.scala 41:16]
  assign io_deq_bits_opcode = elts_0_opcode; // @[ShiftQueue.scala 42:15]
  assign io_deq_bits_param = elts_0_param; // @[ShiftQueue.scala 42:15]
  assign io_deq_bits_size = elts_0_size; // @[ShiftQueue.scala 42:15]
  assign io_deq_bits_source = elts_0_source; // @[ShiftQueue.scala 42:15]
  assign io_deq_bits_address = elts_0_address; // @[ShiftQueue.scala 42:15]
  assign io_deq_bits_mask = elts_0_mask; // @[ShiftQueue.scala 42:15]
  assign io_deq_bits_data = elts_0_data; // @[ShiftQueue.scala 42:15]
  assign io_deq_bits_corrupt = elts_0_corrupt; // @[ShiftQueue.scala 42:15]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid_0 <= 1'h0;
    end else if (io_deq_ready) begin
      valid_0 <= _wen_T_3;
    end else begin
      valid_0 <= _valid_0_T_6;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      valid_1 <= 1'h0;
    end else if (io_deq_ready) begin
      valid_1 <= _wen_T_10;
    end else begin
      valid_1 <= _valid_1_T_6;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_0_opcode <= 3'h0;
    end else if (wen) begin
      if (valid_1) begin
        elts_0_opcode <= elts_1_opcode;
      end else begin
        elts_0_opcode <= io_enq_bits_opcode;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_0_param <= 2'h0;
    end else if (wen) begin
      if (valid_1) begin
        elts_0_param <= elts_1_param;
      end else begin
        elts_0_param <= io_enq_bits_param;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_0_size <= 4'h0;
    end else if (wen) begin
      if (valid_1) begin
        elts_0_size <= elts_1_size;
      end else begin
        elts_0_size <= io_enq_bits_size;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_0_source <= 5'h0;
    end else if (wen) begin
      if (valid_1) begin
        elts_0_source <= elts_1_source;
      end else begin
        elts_0_source <= io_enq_bits_source;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_0_address <= 37'h0;
    end else if (wen) begin
      if (valid_1) begin
        elts_0_address <= elts_1_address;
      end else begin
        elts_0_address <= io_enq_bits_address;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_0_mask <= 16'h0;
    end else if (wen) begin
      if (valid_1) begin
        elts_0_mask <= elts_1_mask;
      end else begin
        elts_0_mask <= io_enq_bits_mask;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_0_data <= 128'h0;
    end else if (wen) begin
      if (valid_1) begin
        elts_0_data <= elts_1_data;
      end else begin
        elts_0_data <= io_enq_bits_data;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_0_corrupt <= 1'h0;
    end else if (wen) begin
      if (valid_1) begin
        elts_0_corrupt <= elts_1_corrupt;
      end else begin
        elts_0_corrupt <= io_enq_bits_corrupt;
      end
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_1_opcode <= 3'h0;
    end else if (wen_1) begin
      elts_1_opcode <= io_enq_bits_opcode;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_1_param <= 2'h0;
    end else if (wen_1) begin
      elts_1_param <= io_enq_bits_param;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_1_size <= 4'h0;
    end else if (wen_1) begin
      elts_1_size <= io_enq_bits_size;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_1_source <= 5'h0;
    end else if (wen_1) begin
      elts_1_source <= io_enq_bits_source;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_1_address <= 37'h0;
    end else if (wen_1) begin
      elts_1_address <= io_enq_bits_address;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_1_mask <= 16'h0;
    end else if (wen_1) begin
      elts_1_mask <= io_enq_bits_mask;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_1_data <= 128'h0;
    end else if (wen_1) begin
      elts_1_data <= io_enq_bits_data;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      elts_1_corrupt <= 1'h0;
    end else if (wen_1) begin
      elts_1_corrupt <= io_enq_bits_corrupt;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  valid_0 = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  valid_1 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  elts_0_opcode = _RAND_2[2:0];
  _RAND_3 = {1{`RANDOM}};
  elts_0_param = _RAND_3[1:0];
  _RAND_4 = {1{`RANDOM}};
  elts_0_size = _RAND_4[3:0];
  _RAND_5 = {1{`RANDOM}};
  elts_0_source = _RAND_5[4:0];
  _RAND_6 = {2{`RANDOM}};
  elts_0_address = _RAND_6[36:0];
  _RAND_7 = {1{`RANDOM}};
  elts_0_mask = _RAND_7[15:0];
  _RAND_8 = {4{`RANDOM}};
  elts_0_data = _RAND_8[127:0];
  _RAND_9 = {1{`RANDOM}};
  elts_0_corrupt = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  elts_1_opcode = _RAND_10[2:0];
  _RAND_11 = {1{`RANDOM}};
  elts_1_param = _RAND_11[1:0];
  _RAND_12 = {1{`RANDOM}};
  elts_1_size = _RAND_12[3:0];
  _RAND_13 = {1{`RANDOM}};
  elts_1_source = _RAND_13[4:0];
  _RAND_14 = {2{`RANDOM}};
  elts_1_address = _RAND_14[36:0];
  _RAND_15 = {1{`RANDOM}};
  elts_1_mask = _RAND_15[15:0];
  _RAND_16 = {4{`RANDOM}};
  elts_1_data = _RAND_16[127:0];
  _RAND_17 = {1{`RANDOM}};
  elts_1_corrupt = _RAND_17[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    valid_0 = 1'h0;
  end
  if (reset) begin
    valid_1 = 1'h0;
  end
  if (rf_reset) begin
    elts_0_opcode = 3'h0;
  end
  if (rf_reset) begin
    elts_0_param = 2'h0;
  end
  if (rf_reset) begin
    elts_0_size = 4'h0;
  end
  if (rf_reset) begin
    elts_0_source = 5'h0;
  end
  if (rf_reset) begin
    elts_0_address = 37'h0;
  end
  if (rf_reset) begin
    elts_0_mask = 16'h0;
  end
  if (rf_reset) begin
    elts_0_data = 128'h0;
  end
  if (rf_reset) begin
    elts_0_corrupt = 1'h0;
  end
  if (rf_reset) begin
    elts_1_opcode = 3'h0;
  end
  if (rf_reset) begin
    elts_1_param = 2'h0;
  end
  if (rf_reset) begin
    elts_1_size = 4'h0;
  end
  if (rf_reset) begin
    elts_1_source = 5'h0;
  end
  if (rf_reset) begin
    elts_1_address = 37'h0;
  end
  if (rf_reset) begin
    elts_1_mask = 16'h0;
  end
  if (rf_reset) begin
    elts_1_data = 128'h0;
  end
  if (rf_reset) begin
    elts_1_corrupt = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
