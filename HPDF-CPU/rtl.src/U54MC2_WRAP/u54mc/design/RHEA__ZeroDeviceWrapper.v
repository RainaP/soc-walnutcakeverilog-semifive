//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__ZeroDeviceWrapper(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_buffer_in_a_ready,
  input         auto_buffer_in_a_valid,
  input  [2:0]  auto_buffer_in_a_bits_opcode,
  input  [2:0]  auto_buffer_in_a_bits_param,
  input  [2:0]  auto_buffer_in_a_bits_size,
  input  [5:0]  auto_buffer_in_a_bits_source,
  input  [27:0] auto_buffer_in_a_bits_address,
  input  [15:0] auto_buffer_in_a_bits_mask,
  input         auto_buffer_in_d_ready,
  output        auto_buffer_in_d_valid,
  output [2:0]  auto_buffer_in_d_bits_opcode,
  output [2:0]  auto_buffer_in_d_bits_size,
  output [5:0]  auto_buffer_in_d_bits_source
);
  wire  zero_rf_reset; // @[CanHaveBuiltInDevices.scala 49:28]
  wire  zero_clock; // @[CanHaveBuiltInDevices.scala 49:28]
  wire  zero_reset; // @[CanHaveBuiltInDevices.scala 49:28]
  wire  zero_auto_in_a_ready; // @[CanHaveBuiltInDevices.scala 49:28]
  wire  zero_auto_in_a_valid; // @[CanHaveBuiltInDevices.scala 49:28]
  wire [2:0] zero_auto_in_a_bits_opcode; // @[CanHaveBuiltInDevices.scala 49:28]
  wire [2:0] zero_auto_in_a_bits_param; // @[CanHaveBuiltInDevices.scala 49:28]
  wire [2:0] zero_auto_in_a_bits_size; // @[CanHaveBuiltInDevices.scala 49:28]
  wire [8:0] zero_auto_in_a_bits_source; // @[CanHaveBuiltInDevices.scala 49:28]
  wire [27:0] zero_auto_in_a_bits_address; // @[CanHaveBuiltInDevices.scala 49:28]
  wire [15:0] zero_auto_in_a_bits_mask; // @[CanHaveBuiltInDevices.scala 49:28]
  wire  zero_auto_in_d_ready; // @[CanHaveBuiltInDevices.scala 49:28]
  wire  zero_auto_in_d_valid; // @[CanHaveBuiltInDevices.scala 49:28]
  wire [2:0] zero_auto_in_d_bits_opcode; // @[CanHaveBuiltInDevices.scala 49:28]
  wire [2:0] zero_auto_in_d_bits_size; // @[CanHaveBuiltInDevices.scala 49:28]
  wire [8:0] zero_auto_in_d_bits_source; // @[CanHaveBuiltInDevices.scala 49:28]
  wire  fragmenter_rf_reset; // @[Fragmenter.scala 333:34]
  wire  fragmenter_clock; // @[Fragmenter.scala 333:34]
  wire  fragmenter_reset; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_a_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_a_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_a_bits_param; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_a_bits_size; // @[Fragmenter.scala 333:34]
  wire [5:0] fragmenter_auto_in_a_bits_source; // @[Fragmenter.scala 333:34]
  wire [27:0] fragmenter_auto_in_a_bits_address; // @[Fragmenter.scala 333:34]
  wire [15:0] fragmenter_auto_in_a_bits_mask; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_d_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_in_d_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_d_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_in_d_bits_size; // @[Fragmenter.scala 333:34]
  wire [5:0] fragmenter_auto_in_d_bits_source; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_a_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_a_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_a_bits_param; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_a_bits_size; // @[Fragmenter.scala 333:34]
  wire [8:0] fragmenter_auto_out_a_bits_source; // @[Fragmenter.scala 333:34]
  wire [27:0] fragmenter_auto_out_a_bits_address; // @[Fragmenter.scala 333:34]
  wire [15:0] fragmenter_auto_out_a_bits_mask; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_d_ready; // @[Fragmenter.scala 333:34]
  wire  fragmenter_auto_out_d_valid; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_d_bits_opcode; // @[Fragmenter.scala 333:34]
  wire [2:0] fragmenter_auto_out_d_bits_size; // @[Fragmenter.scala 333:34]
  wire [8:0] fragmenter_auto_out_d_bits_source; // @[Fragmenter.scala 333:34]
  wire  buffer_rf_reset; // @[Buffer.scala 68:28]
  wire  buffer_clock; // @[Buffer.scala 68:28]
  wire  buffer_reset; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_a_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_in_a_bits_opcode; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_in_a_bits_param; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_in_a_bits_size; // @[Buffer.scala 68:28]
  wire [5:0] buffer_auto_in_a_bits_source; // @[Buffer.scala 68:28]
  wire [27:0] buffer_auto_in_a_bits_address; // @[Buffer.scala 68:28]
  wire [15:0] buffer_auto_in_a_bits_mask; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_d_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_in_d_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_in_d_bits_opcode; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_in_d_bits_size; // @[Buffer.scala 68:28]
  wire [5:0] buffer_auto_in_d_bits_source; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_a_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_out_a_bits_opcode; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_out_a_bits_param; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_out_a_bits_size; // @[Buffer.scala 68:28]
  wire [5:0] buffer_auto_out_a_bits_source; // @[Buffer.scala 68:28]
  wire [27:0] buffer_auto_out_a_bits_address; // @[Buffer.scala 68:28]
  wire [15:0] buffer_auto_out_a_bits_mask; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_d_ready; // @[Buffer.scala 68:28]
  wire  buffer_auto_out_d_valid; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_out_d_bits_opcode; // @[Buffer.scala 68:28]
  wire [2:0] buffer_auto_out_d_bits_size; // @[Buffer.scala 68:28]
  wire [5:0] buffer_auto_out_d_bits_source; // @[Buffer.scala 68:28]
  RHEA__TLZero zero ( // @[CanHaveBuiltInDevices.scala 49:28]
    .rf_reset(zero_rf_reset),
    .clock(zero_clock),
    .reset(zero_reset),
    .auto_in_a_ready(zero_auto_in_a_ready),
    .auto_in_a_valid(zero_auto_in_a_valid),
    .auto_in_a_bits_opcode(zero_auto_in_a_bits_opcode),
    .auto_in_a_bits_param(zero_auto_in_a_bits_param),
    .auto_in_a_bits_size(zero_auto_in_a_bits_size),
    .auto_in_a_bits_source(zero_auto_in_a_bits_source),
    .auto_in_a_bits_address(zero_auto_in_a_bits_address),
    .auto_in_a_bits_mask(zero_auto_in_a_bits_mask),
    .auto_in_d_ready(zero_auto_in_d_ready),
    .auto_in_d_valid(zero_auto_in_d_valid),
    .auto_in_d_bits_opcode(zero_auto_in_d_bits_opcode),
    .auto_in_d_bits_size(zero_auto_in_d_bits_size),
    .auto_in_d_bits_source(zero_auto_in_d_bits_source)
  );
  RHEA__TLFragmenter_10 fragmenter ( // @[Fragmenter.scala 333:34]
    .rf_reset(fragmenter_rf_reset),
    .clock(fragmenter_clock),
    .reset(fragmenter_reset),
    .auto_in_a_ready(fragmenter_auto_in_a_ready),
    .auto_in_a_valid(fragmenter_auto_in_a_valid),
    .auto_in_a_bits_opcode(fragmenter_auto_in_a_bits_opcode),
    .auto_in_a_bits_param(fragmenter_auto_in_a_bits_param),
    .auto_in_a_bits_size(fragmenter_auto_in_a_bits_size),
    .auto_in_a_bits_source(fragmenter_auto_in_a_bits_source),
    .auto_in_a_bits_address(fragmenter_auto_in_a_bits_address),
    .auto_in_a_bits_mask(fragmenter_auto_in_a_bits_mask),
    .auto_in_d_ready(fragmenter_auto_in_d_ready),
    .auto_in_d_valid(fragmenter_auto_in_d_valid),
    .auto_in_d_bits_opcode(fragmenter_auto_in_d_bits_opcode),
    .auto_in_d_bits_size(fragmenter_auto_in_d_bits_size),
    .auto_in_d_bits_source(fragmenter_auto_in_d_bits_source),
    .auto_out_a_ready(fragmenter_auto_out_a_ready),
    .auto_out_a_valid(fragmenter_auto_out_a_valid),
    .auto_out_a_bits_opcode(fragmenter_auto_out_a_bits_opcode),
    .auto_out_a_bits_param(fragmenter_auto_out_a_bits_param),
    .auto_out_a_bits_size(fragmenter_auto_out_a_bits_size),
    .auto_out_a_bits_source(fragmenter_auto_out_a_bits_source),
    .auto_out_a_bits_address(fragmenter_auto_out_a_bits_address),
    .auto_out_a_bits_mask(fragmenter_auto_out_a_bits_mask),
    .auto_out_d_ready(fragmenter_auto_out_d_ready),
    .auto_out_d_valid(fragmenter_auto_out_d_valid),
    .auto_out_d_bits_opcode(fragmenter_auto_out_d_bits_opcode),
    .auto_out_d_bits_size(fragmenter_auto_out_d_bits_size),
    .auto_out_d_bits_source(fragmenter_auto_out_d_bits_source)
  );
  RHEA__TLBuffer_13 buffer ( // @[Buffer.scala 68:28]
    .rf_reset(buffer_rf_reset),
    .clock(buffer_clock),
    .reset(buffer_reset),
    .auto_in_a_ready(buffer_auto_in_a_ready),
    .auto_in_a_valid(buffer_auto_in_a_valid),
    .auto_in_a_bits_opcode(buffer_auto_in_a_bits_opcode),
    .auto_in_a_bits_param(buffer_auto_in_a_bits_param),
    .auto_in_a_bits_size(buffer_auto_in_a_bits_size),
    .auto_in_a_bits_source(buffer_auto_in_a_bits_source),
    .auto_in_a_bits_address(buffer_auto_in_a_bits_address),
    .auto_in_a_bits_mask(buffer_auto_in_a_bits_mask),
    .auto_in_d_ready(buffer_auto_in_d_ready),
    .auto_in_d_valid(buffer_auto_in_d_valid),
    .auto_in_d_bits_opcode(buffer_auto_in_d_bits_opcode),
    .auto_in_d_bits_size(buffer_auto_in_d_bits_size),
    .auto_in_d_bits_source(buffer_auto_in_d_bits_source),
    .auto_out_a_ready(buffer_auto_out_a_ready),
    .auto_out_a_valid(buffer_auto_out_a_valid),
    .auto_out_a_bits_opcode(buffer_auto_out_a_bits_opcode),
    .auto_out_a_bits_param(buffer_auto_out_a_bits_param),
    .auto_out_a_bits_size(buffer_auto_out_a_bits_size),
    .auto_out_a_bits_source(buffer_auto_out_a_bits_source),
    .auto_out_a_bits_address(buffer_auto_out_a_bits_address),
    .auto_out_a_bits_mask(buffer_auto_out_a_bits_mask),
    .auto_out_d_ready(buffer_auto_out_d_ready),
    .auto_out_d_valid(buffer_auto_out_d_valid),
    .auto_out_d_bits_opcode(buffer_auto_out_d_bits_opcode),
    .auto_out_d_bits_size(buffer_auto_out_d_bits_size),
    .auto_out_d_bits_source(buffer_auto_out_d_bits_source)
  );
  assign zero_rf_reset = rf_reset;
  assign fragmenter_rf_reset = rf_reset;
  assign buffer_rf_reset = rf_reset;
  assign auto_buffer_in_a_ready = buffer_auto_in_a_ready; // @[LazyModule.scala 388:16]
  assign auto_buffer_in_d_valid = buffer_auto_in_d_valid; // @[LazyModule.scala 388:16]
  assign auto_buffer_in_d_bits_opcode = buffer_auto_in_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_buffer_in_d_bits_size = buffer_auto_in_d_bits_size; // @[LazyModule.scala 388:16]
  assign auto_buffer_in_d_bits_source = buffer_auto_in_d_bits_source; // @[LazyModule.scala 388:16]
  assign zero_clock = clock;
  assign zero_reset = reset;
  assign zero_auto_in_a_valid = fragmenter_auto_out_a_valid; // @[LazyModule.scala 375:16]
  assign zero_auto_in_a_bits_opcode = fragmenter_auto_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign zero_auto_in_a_bits_param = fragmenter_auto_out_a_bits_param; // @[LazyModule.scala 375:16]
  assign zero_auto_in_a_bits_size = fragmenter_auto_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign zero_auto_in_a_bits_source = fragmenter_auto_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign zero_auto_in_a_bits_address = fragmenter_auto_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign zero_auto_in_a_bits_mask = fragmenter_auto_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign zero_auto_in_d_ready = fragmenter_auto_out_d_ready; // @[LazyModule.scala 375:16]
  assign fragmenter_clock = clock;
  assign fragmenter_reset = reset;
  assign fragmenter_auto_in_a_valid = buffer_auto_out_a_valid; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_opcode = buffer_auto_out_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_param = buffer_auto_out_a_bits_param; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_size = buffer_auto_out_a_bits_size; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_source = buffer_auto_out_a_bits_source; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_address = buffer_auto_out_a_bits_address; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_a_bits_mask = buffer_auto_out_a_bits_mask; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_in_d_ready = buffer_auto_out_d_ready; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_out_a_ready = zero_auto_in_a_ready; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_out_d_valid = zero_auto_in_d_valid; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_out_d_bits_opcode = zero_auto_in_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_out_d_bits_size = zero_auto_in_d_bits_size; // @[LazyModule.scala 375:16]
  assign fragmenter_auto_out_d_bits_source = zero_auto_in_d_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_clock = clock;
  assign buffer_reset = reset;
  assign buffer_auto_in_a_valid = auto_buffer_in_a_valid; // @[LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_opcode = auto_buffer_in_a_bits_opcode; // @[LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_param = auto_buffer_in_a_bits_param; // @[LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_size = auto_buffer_in_a_bits_size; // @[LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_source = auto_buffer_in_a_bits_source; // @[LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_address = auto_buffer_in_a_bits_address; // @[LazyModule.scala 388:16]
  assign buffer_auto_in_a_bits_mask = auto_buffer_in_a_bits_mask; // @[LazyModule.scala 388:16]
  assign buffer_auto_in_d_ready = auto_buffer_in_d_ready; // @[LazyModule.scala 388:16]
  assign buffer_auto_out_a_ready = fragmenter_auto_in_a_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_valid = fragmenter_auto_in_d_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_opcode = fragmenter_auto_in_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_size = fragmenter_auto_in_d_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_d_bits_source = fragmenter_auto_in_d_bits_source; // @[LazyModule.scala 375:16]
endmodule
