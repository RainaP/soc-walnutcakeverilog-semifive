//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__FPToInt(
  input         rf_reset,
  input         clock,
  input         io_in_valid,
  input         io_in_bits_ren2,
  input  [1:0]  io_in_bits_typeTagIn,
  input  [1:0]  io_in_bits_typeTagOut,
  input         io_in_bits_wflags,
  input  [2:0]  io_in_bits_rm,
  input  [1:0]  io_in_bits_typ,
  input  [1:0]  io_in_bits_fmt,
  input  [64:0] io_in_bits_in1,
  input  [64:0] io_in_bits_in2,
  output [2:0]  io_out_bits_in_rm,
  output [64:0] io_out_bits_in_in1,
  output [64:0] io_out_bits_in_in2,
  output        io_out_bits_lt,
  output [63:0] io_out_bits_store,
  output [63:0] io_out_bits_toint,
  output [4:0]  io_out_bits_exc
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [95:0] _RAND_6;
  reg [95:0] _RAND_7;
`endif // RANDOMIZE_REG_INIT
  wire [64:0] dcmp_io_a; // @[FPU.scala 465:20]
  wire [64:0] dcmp_io_b; // @[FPU.scala 465:20]
  wire  dcmp_io_signaling; // @[FPU.scala 465:20]
  wire  dcmp_io_lt; // @[FPU.scala 465:20]
  wire  dcmp_io_eq; // @[FPU.scala 465:20]
  wire [4:0] dcmp_io_exceptionFlags; // @[FPU.scala 465:20]
  wire [64:0] conv_io_in; // @[FPU.scala 496:24]
  wire [2:0] conv_io_roundingMode; // @[FPU.scala 496:24]
  wire  conv_io_signedOut; // @[FPU.scala 496:24]
  wire [63:0] conv_io_out; // @[FPU.scala 496:24]
  wire [2:0] conv_io_intExceptionFlags; // @[FPU.scala 496:24]
  wire [64:0] narrow_io_in; // @[FPU.scala 506:30]
  wire [2:0] narrow_io_roundingMode; // @[FPU.scala 506:30]
  wire  narrow_io_signedOut; // @[FPU.scala 506:30]
  wire [2:0] narrow_io_intExceptionFlags; // @[FPU.scala 506:30]
  reg  in_ren2; // @[Reg.scala 15:16]
  reg [1:0] in_typeTagOut; // @[Reg.scala 15:16]
  reg  in_wflags; // @[Reg.scala 15:16]
  reg [2:0] in_rm; // @[Reg.scala 15:16]
  reg [1:0] in_typ; // @[Reg.scala 15:16]
  reg [1:0] in_fmt; // @[Reg.scala 15:16]
  reg [64:0] in_in1; // @[Reg.scala 15:16]
  reg [64:0] in_in2; // @[Reg.scala 15:16]
  wire [11:0] intin_unrecoded_rawIn_exp = in_in1[63:52]; // @[rawFloatFromRecFN.scala 50:21]
  wire  intin_unrecoded_rawIn_isZero = intin_unrecoded_rawIn_exp[11:9] == 3'h0; // @[rawFloatFromRecFN.scala 51:54]
  wire  intin_unrecoded_rawIn_isSpecial = intin_unrecoded_rawIn_exp[11:10] == 2'h3; // @[rawFloatFromRecFN.scala 52:54]
  wire  intin_unrecoded_rawIn__isNaN = intin_unrecoded_rawIn_isSpecial & intin_unrecoded_rawIn_exp[9]; // @[rawFloatFromRecFN.scala 55:33]
  wire  intin_unrecoded_rawIn__isInf = intin_unrecoded_rawIn_isSpecial & ~intin_unrecoded_rawIn_exp[9]; // @[rawFloatFromRecFN.scala 56:33]
  wire  intin_unrecoded_rawIn__sign = in_in1[64]; // @[rawFloatFromRecFN.scala 58:25]
  wire [12:0] intin_unrecoded_rawIn__sExp = {1'b0,$signed(intin_unrecoded_rawIn_exp)}; // @[rawFloatFromRecFN.scala 59:27]
  wire  intin_unrecoded_rawIn_out_sig_hi_lo = ~intin_unrecoded_rawIn_isZero; // @[rawFloatFromRecFN.scala 60:39]
  wire [51:0] intin_unrecoded_rawIn_out_sig_lo = in_in1[51:0]; // @[rawFloatFromRecFN.scala 60:51]
  wire [53:0] intin_unrecoded_rawIn__sig = {1'h0,intin_unrecoded_rawIn_out_sig_hi_lo,intin_unrecoded_rawIn_out_sig_lo}; // @[Cat.scala 30:58]
  wire  intin_unrecoded_isSubnormal = $signed(intin_unrecoded_rawIn__sExp) < 13'sh402; // @[fNFromRecFN.scala 50:39]
  wire [5:0] intin_unrecoded_denormShiftDist = 6'h1 - intin_unrecoded_rawIn__sExp[5:0]; // @[fNFromRecFN.scala 51:39]
  wire [52:0] _intin_unrecoded_denormFract_T_1 = intin_unrecoded_rawIn__sig[53:1] >> intin_unrecoded_denormShiftDist; // @[fNFromRecFN.scala 52:42]
  wire [51:0] intin_unrecoded_denormFract = _intin_unrecoded_denormFract_T_1[51:0]; // @[fNFromRecFN.scala 52:60]
  wire [10:0] _intin_unrecoded_expOut_T_2 = intin_unrecoded_rawIn__sExp[10:0] - 11'h401; // @[fNFromRecFN.scala 57:45]
  wire [10:0] _intin_unrecoded_expOut_T_3 = intin_unrecoded_isSubnormal ? 11'h0 : _intin_unrecoded_expOut_T_2; // @[fNFromRecFN.scala 55:16]
  wire  _intin_unrecoded_expOut_T_4 = intin_unrecoded_rawIn__isNaN | intin_unrecoded_rawIn__isInf; // @[fNFromRecFN.scala 59:44]
  wire [10:0] _intin_unrecoded_expOut_T_6 = _intin_unrecoded_expOut_T_4 ? 11'h7ff : 11'h0; // @[Bitwise.scala 72:12]
  wire [10:0] intin_unrecoded_hi_lo = _intin_unrecoded_expOut_T_3 | _intin_unrecoded_expOut_T_6; // @[fNFromRecFN.scala 59:15]
  wire [51:0] _intin_unrecoded_fractOut_T_1 = intin_unrecoded_rawIn__isInf ? 52'h0 : intin_unrecoded_rawIn__sig[51:0]; // @[fNFromRecFN.scala 63:20]
  wire [51:0] intin_unrecoded_lo = intin_unrecoded_isSubnormal ? intin_unrecoded_denormFract :
    _intin_unrecoded_fractOut_T_1; // @[fNFromRecFN.scala 61:16]
  wire [63:0] intin_unrecoded = {intin_unrecoded_rawIn__sign,intin_unrecoded_hi_lo,intin_unrecoded_lo}; // @[Cat.scala 30:58]
  wire  intin_prevRecoded_hi_hi = in_in1[31]; // @[FPU.scala 437:10]
  wire  intin_prevRecoded_hi_lo = in_in1[52]; // @[FPU.scala 438:10]
  wire [30:0] intin_prevRecoded_lo = in_in1[30:0]; // @[FPU.scala 439:10]
  wire [32:0] intin_prevRecoded = {intin_prevRecoded_hi_hi,intin_prevRecoded_hi_lo,intin_prevRecoded_lo}; // @[Cat.scala 30:58]
  wire [8:0] intin_prevUnrecoded_rawIn_exp = intin_prevRecoded[31:23]; // @[rawFloatFromRecFN.scala 50:21]
  wire  intin_prevUnrecoded_rawIn_isZero = intin_prevUnrecoded_rawIn_exp[8:6] == 3'h0; // @[rawFloatFromRecFN.scala 51:54]
  wire  intin_prevUnrecoded_rawIn_isSpecial = intin_prevUnrecoded_rawIn_exp[8:7] == 2'h3; // @[rawFloatFromRecFN.scala 52:54]
  wire  intin_prevUnrecoded_rawIn__isNaN = intin_prevUnrecoded_rawIn_isSpecial & intin_prevUnrecoded_rawIn_exp[6]; // @[rawFloatFromRecFN.scala 55:33]
  wire  intin_prevUnrecoded_rawIn__isInf = intin_prevUnrecoded_rawIn_isSpecial & ~intin_prevUnrecoded_rawIn_exp[6]; // @[rawFloatFromRecFN.scala 56:33]
  wire  intin_prevUnrecoded_rawIn__sign = intin_prevRecoded[32]; // @[rawFloatFromRecFN.scala 58:25]
  wire [9:0] intin_prevUnrecoded_rawIn__sExp = {1'b0,$signed(intin_prevUnrecoded_rawIn_exp)}; // @[rawFloatFromRecFN.scala 59:27]
  wire  intin_prevUnrecoded_rawIn_out_sig_hi_lo = ~intin_prevUnrecoded_rawIn_isZero; // @[rawFloatFromRecFN.scala 60:39]
  wire [22:0] intin_prevUnrecoded_rawIn_out_sig_lo = intin_prevRecoded[22:0]; // @[rawFloatFromRecFN.scala 60:51]
  wire [24:0] intin_prevUnrecoded_rawIn__sig = {1'h0,intin_prevUnrecoded_rawIn_out_sig_hi_lo,
    intin_prevUnrecoded_rawIn_out_sig_lo}; // @[Cat.scala 30:58]
  wire  intin_prevUnrecoded_isSubnormal = $signed(intin_prevUnrecoded_rawIn__sExp) < 10'sh82; // @[fNFromRecFN.scala 50:39]
  wire [4:0] intin_prevUnrecoded_denormShiftDist = 5'h1 - intin_prevUnrecoded_rawIn__sExp[4:0]; // @[fNFromRecFN.scala 51:39]
  wire [23:0] _intin_prevUnrecoded_denormFract_T_1 = intin_prevUnrecoded_rawIn__sig[24:1] >>
    intin_prevUnrecoded_denormShiftDist; // @[fNFromRecFN.scala 52:42]
  wire [22:0] intin_prevUnrecoded_denormFract = _intin_prevUnrecoded_denormFract_T_1[22:0]; // @[fNFromRecFN.scala 52:60]
  wire [7:0] _intin_prevUnrecoded_expOut_T_2 = intin_prevUnrecoded_rawIn__sExp[7:0] - 8'h81; // @[fNFromRecFN.scala 57:45]
  wire [7:0] _intin_prevUnrecoded_expOut_T_3 = intin_prevUnrecoded_isSubnormal ? 8'h0 : _intin_prevUnrecoded_expOut_T_2; // @[fNFromRecFN.scala 55:16]
  wire  _intin_prevUnrecoded_expOut_T_4 = intin_prevUnrecoded_rawIn__isNaN | intin_prevUnrecoded_rawIn__isInf; // @[fNFromRecFN.scala 59:44]
  wire [7:0] _intin_prevUnrecoded_expOut_T_6 = _intin_prevUnrecoded_expOut_T_4 ? 8'hff : 8'h0; // @[Bitwise.scala 72:12]
  wire [7:0] intin_prevUnrecoded_hi_lo = _intin_prevUnrecoded_expOut_T_3 | _intin_prevUnrecoded_expOut_T_6; // @[fNFromRecFN.scala 59:15]
  wire [22:0] _intin_prevUnrecoded_fractOut_T_1 = intin_prevUnrecoded_rawIn__isInf ? 23'h0 :
    intin_prevUnrecoded_rawIn__sig[22:0]; // @[fNFromRecFN.scala 63:20]
  wire [22:0] intin_prevUnrecoded_lo = intin_prevUnrecoded_isSubnormal ? intin_prevUnrecoded_denormFract :
    _intin_prevUnrecoded_fractOut_T_1; // @[fNFromRecFN.scala 61:16]
  wire [31:0] intin_prevUnrecoded = {intin_prevUnrecoded_rawIn__sign,intin_prevUnrecoded_hi_lo,intin_prevUnrecoded_lo}; // @[Cat.scala 30:58]
  wire [31:0] intin_hi = intin_unrecoded[63:32]; // @[FPU.scala 441:21]
  wire  _intin_T_1 = &in_in1[63:61]; // @[FPU.scala 243:56]
  wire [31:0] intin_lo = _intin_T_1 ? intin_prevUnrecoded : intin_unrecoded[31:0]; // @[FPU.scala 441:44]
  wire [63:0] intin = {intin_hi,intin_lo}; // @[Cat.scala 30:58]
  wire [31:0] intout_hi = intin[31:0]; // @[FPU.scala 474:75]
  wire [63:0] _intout_T = {intout_hi,intout_hi}; // @[Cat.scala 30:58]
  wire  intout_truncIdx = in_typeTagOut[0]; // @[package.scala 31:49]
  wire [63:0] intout = intout_truncIdx ? intin : _intout_T; // @[package.scala 32:76]
  wire  intType_x6 = in_fmt[0]; // @[FPU.scala 477:35]
  wire  cvtType = in_typ[1]; // @[package.scala 154:13]
  wire  io_out_bits_exc_hi_hi_1 = conv_io_intExceptionFlags[2] | narrow_io_intExceptionFlags[1]; // @[FPU.scala 513:54]
  wire [31:0] toint_hi = conv_io_out[63:32]; // @[FPU.scala 514:53]
  wire  excSign = intin_unrecoded_rawIn__sign & ~_intin_T_1; // @[FPU.scala 511:59]
  wire  excOut_hi = conv_io_signedOut == excSign; // @[FPU.scala 512:46]
  wire  _excOut_T = ~excSign; // @[FPU.scala 512:69]
  wire [30:0] excOut_lo = _excOut_T ? 31'h7fffffff : 31'h0; // @[Bitwise.scala 72:12]
  wire [63:0] _toint_T_10 = {toint_hi,excOut_hi,excOut_lo}; // @[Cat.scala 30:58]
  wire [63:0] _GEN_25 = io_out_bits_exc_hi_hi_1 ? _toint_T_10 : conv_io_out; // @[FPU.scala 514:26 FPU.scala 514:34 FPU.scala 500:13]
  wire [63:0] _GEN_26 = ~cvtType ? _GEN_25 : conv_io_out; // @[FPU.scala 505:30 FPU.scala 500:13]
  wire [2:0] _toint_T_3 = ~in_rm; // @[FPU.scala 489:15]
  wire [1:0] _toint_T_4 = {dcmp_io_lt,dcmp_io_eq}; // @[Cat.scala 30:58]
  wire [2:0] _GEN_0 = {{1'd0}, _toint_T_4}; // @[FPU.scala 489:22]
  wire [2:0] _toint_T_5 = _toint_T_3 & _GEN_0; // @[FPU.scala 489:22]
  wire [63:0] _toint_T_8 = {intout[63:32], 32'h0}; // @[FPU.scala 489:78]
  wire [63:0] _GEN_5 = {{63'd0}, |_toint_T_5}; // @[FPU.scala 489:57]
  wire [63:0] _toint_T_9 = _GEN_5 | _toint_T_8; // @[FPU.scala 489:57]
  wire [63:0] _GEN_29 = ~in_ren2 ? _GEN_26 : _toint_T_9; // @[FPU.scala 493:21 FPU.scala 489:11]
  wire  classify_out_hi_hi_hi_hi_1 = _intin_T_1 & in_in1[51]; // @[FPU.scala 259:24]
  wire  classify_out_hi_hi_hi_lo_1 = _intin_T_1 & ~in_in1[51]; // @[FPU.scala 258:24]
  wire [1:0] classify_out_codeHi_1 = in_in1[63:62]; // @[FPU.scala 249:22]
  wire  classify_out_isSpecial_1 = classify_out_codeHi_1 == 2'h3; // @[FPU.scala 250:28]
  wire  classify_out_isInf_1 = classify_out_isSpecial_1 & ~in_in1[61]; // @[FPU.scala 256:27]
  wire  _classify_out_T_6 = ~intin_unrecoded_rawIn__sign; // @[FPU.scala 261:34]
  wire  classify_out_hi_hi_lo_1 = classify_out_isInf_1 & ~intin_unrecoded_rawIn__sign; // @[FPU.scala 261:31]
  wire  _classify_out_isNormal_T_4 = classify_out_codeHi_1 == 2'h1; // @[FPU.scala 254:27]
  wire  classify_out_isHighSubnormalIn_1 = in_in1[61:52] < 10'h2; // @[FPU.scala 252:55]
  wire  classify_out_isNormal_1 = classify_out_codeHi_1 == 2'h1 & ~classify_out_isHighSubnormalIn_1 |
    classify_out_codeHi_1 == 2'h2; // @[FPU.scala 254:61]
  wire  classify_out_hi_lo_hi_1 = classify_out_isNormal_1 & ~intin_unrecoded_rawIn__sign; // @[FPU.scala 261:50]
  wire  classify_out_isSubnormal_1 = in_in1[63:61] == 3'h1 | _classify_out_isNormal_T_4 &
    classify_out_isHighSubnormalIn_1; // @[FPU.scala 253:40]
  wire  classify_out_hi_lo_lo_1 = classify_out_isSubnormal_1 & _classify_out_T_6; // @[FPU.scala 262:21]
  wire  classify_out_isZero_1 = in_in1[63:61] == 3'h0; // @[FPU.scala 255:23]
  wire  classify_out_lo_hi_hi_hi_1 = classify_out_isZero_1 & _classify_out_T_6; // @[FPU.scala 262:38]
  wire  classify_out_lo_hi_hi_lo_1 = classify_out_isZero_1 & intin_unrecoded_rawIn__sign; // @[FPU.scala 262:55]
  wire  classify_out_lo_hi_lo_1 = classify_out_isSubnormal_1 & intin_unrecoded_rawIn__sign; // @[FPU.scala 263:21]
  wire  classify_out_lo_lo_hi_1 = classify_out_isNormal_1 & intin_unrecoded_rawIn__sign; // @[FPU.scala 263:39]
  wire  classify_out_lo_lo_lo_1 = classify_out_isInf_1 & intin_unrecoded_rawIn__sign; // @[FPU.scala 263:54]
  wire [9:0] _classify_out_T_10 = {classify_out_hi_hi_hi_hi_1,classify_out_hi_hi_hi_lo_1,classify_out_hi_hi_lo_1,
    classify_out_hi_lo_hi_1,classify_out_hi_lo_lo_1,classify_out_lo_hi_hi_hi_1,classify_out_lo_hi_hi_lo_1,
    classify_out_lo_hi_lo_1,classify_out_lo_lo_hi_1,classify_out_lo_lo_lo_1}; // @[Cat.scala 30:58]
  wire [11:0] _classify_out_expOut_commonCase_T_1 = intin_unrecoded_rawIn_exp + 12'h100; // @[FPU.scala 274:31]
  wire [11:0] classify_out_expOut_commonCase = _classify_out_expOut_commonCase_T_1 - 12'h800; // @[FPU.scala 274:48]
  wire [5:0] classify_out_expOut_lo = classify_out_expOut_commonCase[5:0]; // @[FPU.scala 275:65]
  wire [8:0] _classify_out_expOut_T_3 = {intin_unrecoded_rawIn_exp[11:9],classify_out_expOut_lo}; // @[Cat.scala 30:58]
  wire [8:0] classify_out_hi_lo = intin_unrecoded_rawIn_isZero | intin_unrecoded_rawIn_exp[11:9] >= 3'h6 ?
    _classify_out_expOut_T_3 : classify_out_expOut_commonCase[8:0]; // @[FPU.scala 275:10]
  wire [75:0] _classify_out_fractOut_T = {intin_unrecoded_rawIn_out_sig_lo, 24'h0}; // @[FPU.scala 271:28]
  wire [22:0] classify_out_lo = _classify_out_fractOut_T[75:53]; // @[FPU.scala 271:38]
  wire [32:0] _classify_out_T = {intin_unrecoded_rawIn__sign,classify_out_hi_lo,classify_out_lo}; // @[Cat.scala 30:58]
  wire [2:0] classify_out_code = _classify_out_T[31:29]; // @[FPU.scala 248:17]
  wire  classify_out_isNaN = &classify_out_code; // @[FPU.scala 257:22]
  wire  classify_out_hi_hi_hi_hi = classify_out_isNaN & _classify_out_T[22]; // @[FPU.scala 259:24]
  wire  classify_out_hi_hi_hi_lo = classify_out_isNaN & ~_classify_out_T[22]; // @[FPU.scala 258:24]
  wire [1:0] classify_out_codeHi = classify_out_code[2:1]; // @[FPU.scala 249:22]
  wire  classify_out_isSpecial = classify_out_codeHi == 2'h3; // @[FPU.scala 250:28]
  wire  classify_out_isInf = classify_out_isSpecial & ~classify_out_code[0]; // @[FPU.scala 256:27]
  wire  classify_out_sign = _classify_out_T[32]; // @[FPU.scala 247:17]
  wire  _classify_out_T_1 = ~classify_out_sign; // @[FPU.scala 261:34]
  wire  classify_out_hi_hi_lo = classify_out_isInf & ~classify_out_sign; // @[FPU.scala 261:31]
  wire  _classify_out_isNormal_T = classify_out_codeHi == 2'h1; // @[FPU.scala 254:27]
  wire  classify_out_isHighSubnormalIn = _classify_out_T[29:23] < 7'h2; // @[FPU.scala 252:55]
  wire  classify_out_isNormal = classify_out_codeHi == 2'h1 & ~classify_out_isHighSubnormalIn | classify_out_codeHi == 2'h2
    ; // @[FPU.scala 254:61]
  wire  classify_out_hi_lo_hi = classify_out_isNormal & ~classify_out_sign; // @[FPU.scala 261:50]
  wire  classify_out_isSubnormal = classify_out_code == 3'h1 | _classify_out_isNormal_T & classify_out_isHighSubnormalIn
    ; // @[FPU.scala 253:40]
  wire  classify_out_hi_lo_lo = classify_out_isSubnormal & _classify_out_T_1; // @[FPU.scala 262:21]
  wire  classify_out_isZero = classify_out_code == 3'h0; // @[FPU.scala 255:23]
  wire  classify_out_lo_hi_hi_hi = classify_out_isZero & _classify_out_T_1; // @[FPU.scala 262:38]
  wire  classify_out_lo_hi_hi_lo = classify_out_isZero & classify_out_sign; // @[FPU.scala 262:55]
  wire  classify_out_lo_hi_lo = classify_out_isSubnormal & classify_out_sign; // @[FPU.scala 263:21]
  wire  classify_out_lo_lo_hi = classify_out_isNormal & classify_out_sign; // @[FPU.scala 263:39]
  wire  classify_out_lo_lo_lo = classify_out_isInf & classify_out_sign; // @[FPU.scala 263:54]
  wire [9:0] _classify_out_T_5 = {classify_out_hi_hi_hi_hi,classify_out_hi_hi_hi_lo,classify_out_hi_hi_lo,
    classify_out_hi_lo_hi,classify_out_hi_lo_lo,classify_out_lo_hi_hi_hi,classify_out_lo_hi_hi_lo,classify_out_lo_hi_lo,
    classify_out_lo_lo_hi,classify_out_lo_lo_lo}; // @[Cat.scala 30:58]
  wire [9:0] classify_out = intout_truncIdx ? _classify_out_T_10 : _classify_out_T_5; // @[package.scala 32:76]
  wire [63:0] _GEN_8 = {{54'd0}, classify_out}; // @[FPU.scala 484:27]
  wire [63:0] _toint_T_2 = _GEN_8 | _toint_T_8; // @[FPU.scala 484:27]
  wire [63:0] _GEN_23 = in_rm[0] ? _toint_T_2 : intout; // @[FPU.scala 482:19 FPU.scala 484:11]
  wire [63:0] toint = in_wflags ? _GEN_29 : _GEN_23; // @[FPU.scala 488:20]
  wire [31:0] io_out_bits_toint_lo = toint[31:0]; // @[FPU.scala 479:59]
  wire [31:0] io_out_bits_toint_hi = io_out_bits_toint_lo[31] ? 32'hffffffff : 32'h0; // @[Bitwise.scala 72:12]
  wire [63:0] _io_out_bits_toint_T_2 = {io_out_bits_toint_hi,io_out_bits_toint_lo}; // @[Cat.scala 30:58]
  wire  _GEN_28 = ~in_ren2 & cvtType; // @[FPU.scala 493:21 FPU.scala 495:15 FPU.scala 491:13]
  wire  _GEN_24 = in_rm[0] ? 1'h0 : intType_x6; // @[FPU.scala 482:19 FPU.scala 485:13]
  wire  intType = in_wflags ? _GEN_28 : _GEN_24; // @[FPU.scala 488:20]
  wire  io_out_bits_exc_hi_hi = |conv_io_intExceptionFlags[2:1]; // @[FPU.scala 501:62]
  wire  io_out_bits_exc_lo = conv_io_intExceptionFlags[0]; // @[FPU.scala 501:104]
  wire [4:0] _io_out_bits_exc_T_1 = {io_out_bits_exc_hi_hi,3'h0,io_out_bits_exc_lo}; // @[Cat.scala 30:58]
  wire  io_out_bits_exc_lo_1 = ~io_out_bits_exc_hi_hi_1 & io_out_bits_exc_lo; // @[FPU.scala 515:64]
  wire [4:0] _io_out_bits_exc_T_4 = {io_out_bits_exc_hi_hi_1,3'h0,io_out_bits_exc_lo_1}; // @[Cat.scala 30:58]
  wire [4:0] _GEN_27 = ~cvtType ? _io_out_bits_exc_T_4 : _io_out_bits_exc_T_1; // @[FPU.scala 505:30 FPU.scala 515:27 FPU.scala 501:23]
  wire [4:0] _GEN_30 = ~in_ren2 ? _GEN_27 : dcmp_io_exceptionFlags; // @[FPU.scala 493:21 FPU.scala 490:21]
  RHEA__CompareRecFN dcmp ( // @[FPU.scala 465:20]
    .io_a(dcmp_io_a),
    .io_b(dcmp_io_b),
    .io_signaling(dcmp_io_signaling),
    .io_lt(dcmp_io_lt),
    .io_eq(dcmp_io_eq),
    .io_exceptionFlags(dcmp_io_exceptionFlags)
  );
  RHEA__RecFNToIN conv ( // @[FPU.scala 496:24]
    .io_in(conv_io_in),
    .io_roundingMode(conv_io_roundingMode),
    .io_signedOut(conv_io_signedOut),
    .io_out(conv_io_out),
    .io_intExceptionFlags(conv_io_intExceptionFlags)
  );
  RHEA__RecFNToIN_1 narrow ( // @[FPU.scala 506:30]
    .io_in(narrow_io_in),
    .io_roundingMode(narrow_io_roundingMode),
    .io_signedOut(narrow_io_signedOut),
    .io_intExceptionFlags(narrow_io_intExceptionFlags)
  );
  assign io_out_bits_in_rm = in_rm; // @[FPU.scala 523:18]
  assign io_out_bits_in_in1 = in_in1; // @[FPU.scala 523:18]
  assign io_out_bits_in_in2 = in_in2; // @[FPU.scala 523:18]
  assign io_out_bits_lt = dcmp_io_lt | $signed(dcmp_io_a) < 65'sh0 & $signed(dcmp_io_b) >= 65'sh0; // @[FPU.scala 522:32]
  assign io_out_bits_store = intout_truncIdx ? intin : _intout_T; // @[package.scala 32:76]
  assign io_out_bits_toint = intType ? toint : _io_out_bits_toint_T_2; // @[package.scala 32:76]
  assign io_out_bits_exc = in_wflags ? _GEN_30 : 5'h0; // @[FPU.scala 488:20 FPU.scala 480:19]
  assign dcmp_io_a = in_in1; // @[FPU.scala 466:13]
  assign dcmp_io_b = in_in2; // @[FPU.scala 467:13]
  assign dcmp_io_signaling = ~in_rm[1]; // @[FPU.scala 468:24]
  assign conv_io_in = in_in1; // @[FPU.scala 497:18]
  assign conv_io_roundingMode = in_rm; // @[FPU.scala 498:28]
  assign conv_io_signedOut = ~in_typ[0]; // @[FPU.scala 499:28]
  assign narrow_io_in = in_in1; // @[FPU.scala 507:24]
  assign narrow_io_roundingMode = in_rm; // @[FPU.scala 508:34]
  assign narrow_io_signedOut = ~in_typ[0]; // @[FPU.scala 509:34]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      in_ren2 <= 1'h0;
    end else if (io_in_valid) begin
      in_ren2 <= io_in_bits_ren2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      in_typeTagOut <= 2'h0;
    end else if (io_in_valid) begin
      in_typeTagOut <= io_in_bits_typeTagOut;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      in_wflags <= 1'h0;
    end else if (io_in_valid) begin
      in_wflags <= io_in_bits_wflags;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      in_rm <= 3'h0;
    end else if (io_in_valid) begin
      in_rm <= io_in_bits_rm;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      in_typ <= 2'h0;
    end else if (io_in_valid) begin
      in_typ <= io_in_bits_typ;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      in_fmt <= 2'h0;
    end else if (io_in_valid) begin
      in_fmt <= io_in_bits_fmt;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      in_in1 <= 65'h0;
    end else if (io_in_valid) begin
      in_in1 <= io_in_bits_in1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      in_in2 <= 65'h0;
    end else if (io_in_valid) begin
      in_in2 <= io_in_bits_in2;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  in_ren2 = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  in_typeTagOut = _RAND_1[1:0];
  _RAND_2 = {1{`RANDOM}};
  in_wflags = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  in_rm = _RAND_3[2:0];
  _RAND_4 = {1{`RANDOM}};
  in_typ = _RAND_4[1:0];
  _RAND_5 = {1{`RANDOM}};
  in_fmt = _RAND_5[1:0];
  _RAND_6 = {3{`RANDOM}};
  in_in1 = _RAND_6[64:0];
  _RAND_7 = {3{`RANDOM}};
  in_in2 = _RAND_7[64:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    in_ren2 = 1'h0;
  end
  if (rf_reset) begin
    in_typeTagOut = 2'h0;
  end
  if (rf_reset) begin
    in_wflags = 1'h0;
  end
  if (rf_reset) begin
    in_rm = 3'h0;
  end
  if (rf_reset) begin
    in_typ = 2'h0;
  end
  if (rf_reset) begin
    in_fmt = 2'h0;
  end
  if (rf_reset) begin
    in_in1 = 65'h0;
  end
  if (rf_reset) begin
    in_in2 = 65'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
