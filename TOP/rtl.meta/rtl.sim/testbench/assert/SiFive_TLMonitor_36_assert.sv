//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLMonitor_36_assert(
  input         clock,
  input         reset,
  input         io_in_a_ready,
  input         io_in_a_valid,
  input  [2:0]  io_in_a_bits_opcode,
  input  [2:0]  io_in_a_bits_param,
  input  [2:0]  io_in_a_bits_size,
  input  [3:0]  io_in_a_bits_source,
  input  [35:0] io_in_a_bits_address,
  input  [15:0] io_in_a_bits_mask,
  input         io_in_a_bits_corrupt,
  input         io_in_d_ready,
  input         io_in_d_valid,
  input  [2:0]  io_in_d_bits_opcode,
  input  [1:0]  io_in_d_bits_param,
  input  [2:0]  io_in_d_bits_size,
  input  [3:0]  io_in_d_bits_source,
  input         io_in_d_bits_sink,
  input         io_in_d_bits_denied,
  input         io_in_d_bits_corrupt
);
  wire [31:0] plusarg_reader_out; // @[PlusArg.scala 44:11]
  wire [12:0] _T_14; // @[package.scala 189:77]
  wire [5:0] _T_16; // @[package.scala 189:46]
  wire [35:0] _GEN_18; // @[Edges.scala 22:16]
  wire [35:0] _T_17; // @[Edges.scala 22:16]
  wire  _T_18; // @[Edges.scala 22:24]
  wire [3:0] _T_19; // @[Misc.scala 201:34]
  wire [3:0] _T_21; // @[OneHot.scala 65:12]
  wire [3:0] _T_23; // @[Misc.scala 201:81]
  wire  _T_24; // @[Misc.scala 205:21]
  wire  _T_27; // @[Misc.scala 210:20]
  wire  _T_29; // @[Misc.scala 214:38]
  wire  _T_30; // @[Misc.scala 214:29]
  wire  _T_32; // @[Misc.scala 214:38]
  wire  _T_33; // @[Misc.scala 214:29]
  wire  _T_36; // @[Misc.scala 210:20]
  wire  _T_37; // @[Misc.scala 213:27]
  wire  _T_38; // @[Misc.scala 214:38]
  wire  _T_39; // @[Misc.scala 214:29]
  wire  _T_40; // @[Misc.scala 213:27]
  wire  _T_41; // @[Misc.scala 214:38]
  wire  _T_42; // @[Misc.scala 214:29]
  wire  _T_43; // @[Misc.scala 213:27]
  wire  _T_44; // @[Misc.scala 214:38]
  wire  _T_45; // @[Misc.scala 214:29]
  wire  _T_46; // @[Misc.scala 213:27]
  wire  _T_47; // @[Misc.scala 214:38]
  wire  _T_48; // @[Misc.scala 214:29]
  wire  _T_51; // @[Misc.scala 210:20]
  wire  _T_52; // @[Misc.scala 213:27]
  wire  _T_53; // @[Misc.scala 214:38]
  wire  _T_54; // @[Misc.scala 214:29]
  wire  _T_55; // @[Misc.scala 213:27]
  wire  _T_56; // @[Misc.scala 214:38]
  wire  _T_57; // @[Misc.scala 214:29]
  wire  _T_58; // @[Misc.scala 213:27]
  wire  _T_59; // @[Misc.scala 214:38]
  wire  _T_60; // @[Misc.scala 214:29]
  wire  _T_61; // @[Misc.scala 213:27]
  wire  _T_62; // @[Misc.scala 214:38]
  wire  _T_63; // @[Misc.scala 214:29]
  wire  _T_64; // @[Misc.scala 213:27]
  wire  _T_65; // @[Misc.scala 214:38]
  wire  _T_66; // @[Misc.scala 214:29]
  wire  _T_67; // @[Misc.scala 213:27]
  wire  _T_68; // @[Misc.scala 214:38]
  wire  _T_69; // @[Misc.scala 214:29]
  wire  _T_70; // @[Misc.scala 213:27]
  wire  _T_71; // @[Misc.scala 214:38]
  wire  _T_72; // @[Misc.scala 214:29]
  wire  _T_73; // @[Misc.scala 213:27]
  wire  _T_74; // @[Misc.scala 214:38]
  wire  _T_75; // @[Misc.scala 214:29]
  wire  _T_78; // @[Misc.scala 210:20]
  wire  _T_79; // @[Misc.scala 213:27]
  wire  _T_80; // @[Misc.scala 214:38]
  wire  _T_81; // @[Misc.scala 214:29]
  wire  _T_82; // @[Misc.scala 213:27]
  wire  _T_83; // @[Misc.scala 214:38]
  wire  _T_84; // @[Misc.scala 214:29]
  wire  _T_85; // @[Misc.scala 213:27]
  wire  _T_86; // @[Misc.scala 214:38]
  wire  _T_87; // @[Misc.scala 214:29]
  wire  _T_88; // @[Misc.scala 213:27]
  wire  _T_89; // @[Misc.scala 214:38]
  wire  _T_90; // @[Misc.scala 214:29]
  wire  _T_91; // @[Misc.scala 213:27]
  wire  _T_92; // @[Misc.scala 214:38]
  wire  _T_93; // @[Misc.scala 214:29]
  wire  _T_94; // @[Misc.scala 213:27]
  wire  _T_95; // @[Misc.scala 214:38]
  wire  _T_96; // @[Misc.scala 214:29]
  wire  _T_97; // @[Misc.scala 213:27]
  wire  _T_98; // @[Misc.scala 214:38]
  wire  _T_99; // @[Misc.scala 214:29]
  wire  _T_100; // @[Misc.scala 213:27]
  wire  _T_101; // @[Misc.scala 214:38]
  wire  _T_102; // @[Misc.scala 214:29]
  wire  _T_103; // @[Misc.scala 213:27]
  wire  _T_104; // @[Misc.scala 214:38]
  wire  _T_105; // @[Misc.scala 214:29]
  wire  _T_106; // @[Misc.scala 213:27]
  wire  _T_107; // @[Misc.scala 214:38]
  wire  _T_108; // @[Misc.scala 214:29]
  wire  _T_109; // @[Misc.scala 213:27]
  wire  _T_110; // @[Misc.scala 214:38]
  wire  _T_111; // @[Misc.scala 214:29]
  wire  _T_112; // @[Misc.scala 213:27]
  wire  _T_113; // @[Misc.scala 214:38]
  wire  _T_114; // @[Misc.scala 214:29]
  wire  _T_115; // @[Misc.scala 213:27]
  wire  _T_116; // @[Misc.scala 214:38]
  wire  _T_117; // @[Misc.scala 214:29]
  wire  _T_118; // @[Misc.scala 213:27]
  wire  _T_119; // @[Misc.scala 214:38]
  wire  _T_120; // @[Misc.scala 214:29]
  wire  _T_121; // @[Misc.scala 213:27]
  wire  _T_122; // @[Misc.scala 214:38]
  wire  _T_123; // @[Misc.scala 214:29]
  wire  _T_124; // @[Misc.scala 213:27]
  wire  _T_125; // @[Misc.scala 214:38]
  wire  _T_126; // @[Misc.scala 214:29]
  wire [7:0] _T_133; // @[Cat.scala 29:58]
  wire [15:0] _T_141; // @[Cat.scala 29:58]
  wire  _T_160; // @[Monitor.scala 79:25]
  wire [35:0] _T_162; // @[Parameters.scala 137:31]
  wire [36:0] _T_163; // @[Parameters.scala 137:49]
  wire [36:0] _T_165; // @[Parameters.scala 137:52]
  wire  _T_166; // @[Parameters.scala 137:67]
  wire [35:0] _T_167; // @[Parameters.scala 137:31]
  wire [36:0] _T_168; // @[Parameters.scala 137:49]
  wire [36:0] _T_170; // @[Parameters.scala 137:52]
  wire  _T_171; // @[Parameters.scala 137:67]
  wire  _T_172; // @[Parameters.scala 552:42]
  wire  _T_177; // @[Monitor.scala 44:11]
  wire  _T_186; // @[Monitor.scala 44:11]
  wire  _T_187; // @[Monitor.scala 44:11]
  wire  _T_189; // @[Monitor.scala 44:11]
  wire  _T_190; // @[Monitor.scala 44:11]
  wire  _T_191; // @[Bundles.scala 110:27]
  wire  _T_193; // @[Monitor.scala 44:11]
  wire  _T_194; // @[Monitor.scala 44:11]
  wire [15:0] _T_195; // @[Monitor.scala 86:18]
  wire  _T_196; // @[Monitor.scala 86:31]
  wire  _T_198; // @[Monitor.scala 44:11]
  wire  _T_199; // @[Monitor.scala 44:11]
  wire  _T_200; // @[Monitor.scala 87:18]
  wire  _T_202; // @[Monitor.scala 44:11]
  wire  _T_203; // @[Monitor.scala 44:11]
  wire  _T_204; // @[Monitor.scala 90:25]
  wire  _T_239; // @[Monitor.scala 97:31]
  wire  _T_241; // @[Monitor.scala 44:11]
  wire  _T_242; // @[Monitor.scala 44:11]
  wire  _T_252; // @[Monitor.scala 102:25]
  wire  _T_254; // @[Parameters.scala 93:42]
  wire  _T_268; // @[Parameters.scala 551:56]
  wire  _T_271; // @[Monitor.scala 44:11]
  wire  _T_272; // @[Monitor.scala 44:11]
  wire  _T_279; // @[Monitor.scala 106:31]
  wire  _T_281; // @[Monitor.scala 44:11]
  wire  _T_282; // @[Monitor.scala 44:11]
  wire  _T_283; // @[Monitor.scala 107:30]
  wire  _T_285; // @[Monitor.scala 44:11]
  wire  _T_286; // @[Monitor.scala 44:11]
  wire  _T_291; // @[Monitor.scala 111:25]
  wire  _T_326; // @[Monitor.scala 119:25]
  wire [15:0] _T_357; // @[Monitor.scala 124:33]
  wire [15:0] _T_358; // @[Monitor.scala 124:31]
  wire  _T_359; // @[Monitor.scala 124:40]
  wire  _T_361; // @[Monitor.scala 44:11]
  wire  _T_362; // @[Monitor.scala 44:11]
  wire  _T_363; // @[Monitor.scala 127:25]
  wire  _T_365; // @[Parameters.scala 93:42]
  wire  _T_373; // @[Parameters.scala 551:56]
  wire  _T_384; // @[Monitor.scala 44:11]
  wire  _T_385; // @[Monitor.scala 44:11]
  wire  _T_392; // @[Bundles.scala 140:33]
  wire  _T_394; // @[Monitor.scala 44:11]
  wire  _T_395; // @[Monitor.scala 44:11]
  wire  _T_400; // @[Monitor.scala 135:25]
  wire  _T_429; // @[Bundles.scala 147:30]
  wire  _T_431; // @[Monitor.scala 44:11]
  wire  _T_432; // @[Monitor.scala 44:11]
  wire  _T_437; // @[Monitor.scala 143:25]
  wire  _T_447; // @[Parameters.scala 551:56]
  wire  _T_458; // @[Monitor.scala 44:11]
  wire  _T_459; // @[Monitor.scala 44:11]
  wire  _T_466; // @[Bundles.scala 160:28]
  wire  _T_468; // @[Monitor.scala 44:11]
  wire  _T_469; // @[Monitor.scala 44:11]
  wire  _T_478; // @[Bundles.scala 44:24]
  wire  _T_480; // @[Monitor.scala 51:11]
  wire  _T_481; // @[Monitor.scala 51:11]
  wire  _T_492; // @[Monitor.scala 307:25]
  wire  _T_496; // @[Monitor.scala 309:27]
  wire  _T_498; // @[Monitor.scala 51:11]
  wire  _T_499; // @[Monitor.scala 51:11]
  wire  _T_500; // @[Monitor.scala 310:28]
  wire  _T_502; // @[Monitor.scala 51:11]
  wire  _T_503; // @[Monitor.scala 51:11]
  wire  _T_504; // @[Monitor.scala 311:15]
  wire  _T_506; // @[Monitor.scala 51:11]
  wire  _T_507; // @[Monitor.scala 51:11]
  wire  _T_508; // @[Monitor.scala 312:15]
  wire  _T_510; // @[Monitor.scala 51:11]
  wire  _T_511; // @[Monitor.scala 51:11]
  wire  _T_512; // @[Monitor.scala 315:25]
  wire  _T_523; // @[Bundles.scala 104:26]
  wire  _T_525; // @[Monitor.scala 51:11]
  wire  _T_526; // @[Monitor.scala 51:11]
  wire  _T_527; // @[Monitor.scala 320:28]
  wire  _T_529; // @[Monitor.scala 51:11]
  wire  _T_530; // @[Monitor.scala 51:11]
  wire  _T_540; // @[Monitor.scala 325:25]
  wire  _T_560; // @[Monitor.scala 331:30]
  wire  _T_562; // @[Monitor.scala 51:11]
  wire  _T_563; // @[Monitor.scala 51:11]
  wire  _T_569; // @[Monitor.scala 335:25]
  wire  _T_586; // @[Monitor.scala 343:25]
  wire  _T_604; // @[Monitor.scala 351:25]
  wire  _T_636; // @[Decoupled.scala 40:37]
  wire  _T_643; // @[Edges.scala 93:28]
  reg [1:0] _T_645; // @[Edges.scala 230:27]
  reg [31:0] _RAND_0;
  wire [1:0] _T_647; // @[Edges.scala 231:28]
  wire  _T_648; // @[Edges.scala 232:25]
  reg [2:0] _T_656; // @[Monitor.scala 381:22]
  reg [31:0] _RAND_1;
  reg [2:0] _T_657; // @[Monitor.scala 382:22]
  reg [31:0] _RAND_2;
  reg [2:0] _T_658; // @[Monitor.scala 383:22]
  reg [31:0] _RAND_3;
  reg [3:0] _T_659; // @[Monitor.scala 384:22]
  reg [31:0] _RAND_4;
  reg [35:0] _T_660; // @[Monitor.scala 385:22]
  reg [63:0] _RAND_5;
  wire  _T_661; // @[Monitor.scala 386:22]
  wire  _T_662; // @[Monitor.scala 386:19]
  wire  _T_663; // @[Monitor.scala 387:32]
  wire  _T_665; // @[Monitor.scala 44:11]
  wire  _T_666; // @[Monitor.scala 44:11]
  wire  _T_667; // @[Monitor.scala 388:32]
  wire  _T_669; // @[Monitor.scala 44:11]
  wire  _T_670; // @[Monitor.scala 44:11]
  wire  _T_671; // @[Monitor.scala 389:32]
  wire  _T_673; // @[Monitor.scala 44:11]
  wire  _T_674; // @[Monitor.scala 44:11]
  wire  _T_675; // @[Monitor.scala 390:32]
  wire  _T_677; // @[Monitor.scala 44:11]
  wire  _T_678; // @[Monitor.scala 44:11]
  wire  _T_679; // @[Monitor.scala 391:32]
  wire  _T_681; // @[Monitor.scala 44:11]
  wire  _T_682; // @[Monitor.scala 44:11]
  wire  _T_684; // @[Monitor.scala 393:20]
  wire  _T_685; // @[Decoupled.scala 40:37]
  wire [12:0] _T_687; // @[package.scala 189:77]
  wire [5:0] _T_689; // @[package.scala 189:46]
  reg [1:0] _T_693; // @[Edges.scala 230:27]
  reg [31:0] _RAND_6;
  wire [1:0] _T_695; // @[Edges.scala 231:28]
  wire  _T_696; // @[Edges.scala 232:25]
  reg [2:0] _T_704; // @[Monitor.scala 532:22]
  reg [31:0] _RAND_7;
  reg [1:0] _T_705; // @[Monitor.scala 533:22]
  reg [31:0] _RAND_8;
  reg [2:0] _T_706; // @[Monitor.scala 534:22]
  reg [31:0] _RAND_9;
  reg [3:0] _T_707; // @[Monitor.scala 535:22]
  reg [31:0] _RAND_10;
  reg  _T_708; // @[Monitor.scala 536:22]
  reg [31:0] _RAND_11;
  reg  _T_709; // @[Monitor.scala 537:22]
  reg [31:0] _RAND_12;
  wire  _T_710; // @[Monitor.scala 538:22]
  wire  _T_711; // @[Monitor.scala 538:19]
  wire  _T_712; // @[Monitor.scala 539:29]
  wire  _T_714; // @[Monitor.scala 51:11]
  wire  _T_715; // @[Monitor.scala 51:11]
  wire  _T_716; // @[Monitor.scala 540:29]
  wire  _T_718; // @[Monitor.scala 51:11]
  wire  _T_719; // @[Monitor.scala 51:11]
  wire  _T_720; // @[Monitor.scala 541:29]
  wire  _T_722; // @[Monitor.scala 51:11]
  wire  _T_723; // @[Monitor.scala 51:11]
  wire  _T_724; // @[Monitor.scala 542:29]
  wire  _T_726; // @[Monitor.scala 51:11]
  wire  _T_727; // @[Monitor.scala 51:11]
  wire  _T_728; // @[Monitor.scala 543:29]
  wire  _T_730; // @[Monitor.scala 51:11]
  wire  _T_731; // @[Monitor.scala 51:11]
  wire  _T_732; // @[Monitor.scala 544:29]
  wire  _T_734; // @[Monitor.scala 51:11]
  wire  _T_735; // @[Monitor.scala 51:11]
  wire  _T_737; // @[Monitor.scala 546:20]
  reg [15:0] _T_738; // @[Monitor.scala 568:27]
  reg [31:0] _RAND_13;
  reg [1:0] _T_748; // @[Edges.scala 230:27]
  reg [31:0] _RAND_14;
  wire [1:0] _T_750; // @[Edges.scala 231:28]
  wire  _T_751; // @[Edges.scala 232:25]
  reg [1:0] _T_767; // @[Edges.scala 230:27]
  reg [31:0] _RAND_15;
  wire [1:0] _T_769; // @[Edges.scala 231:28]
  wire  _T_770; // @[Edges.scala 232:25]
  wire  _T_780; // @[Monitor.scala 574:27]
  wire [15:0] _T_782; // @[OneHot.scala 58:35]
  wire [15:0] _T_783; // @[Monitor.scala 576:23]
  wire  _T_785; // @[Monitor.scala 576:14]
  wire  _T_787; // @[Monitor.scala 576:13]
  wire  _T_788; // @[Monitor.scala 576:13]
  wire [15:0] _GEN_15; // @[Monitor.scala 574:72]
  wire  _T_792; // @[Monitor.scala 581:27]
  wire  _T_794; // @[Monitor.scala 581:75]
  wire  _T_795; // @[Monitor.scala 581:72]
  wire [15:0] _T_796; // @[OneHot.scala 58:35]
  wire [15:0] _T_797; // @[Monitor.scala 583:21]
  wire [15:0] _T_798; // @[Monitor.scala 583:32]
  wire  _T_801; // @[Monitor.scala 51:11]
  wire  _T_802; // @[Monitor.scala 51:11]
  wire [15:0] _GEN_16; // @[Monitor.scala 581:91]
  wire  _T_803; // @[Monitor.scala 587:20]
  wire  _T_804; // @[Monitor.scala 587:40]
  wire  _T_805; // @[Monitor.scala 587:33]
  wire  _T_806; // @[Monitor.scala 587:30]
  wire  _T_808; // @[Monitor.scala 51:11]
  wire  _T_809; // @[Monitor.scala 51:11]
  wire [15:0] _T_810; // @[Monitor.scala 590:27]
  wire [15:0] _T_811; // @[Monitor.scala 590:38]
  wire [15:0] _T_812; // @[Monitor.scala 590:36]
  reg [31:0] _T_813; // @[Monitor.scala 592:27]
  reg [31:0] _RAND_16;
  wire  _T_814; // @[Monitor.scala 595:23]
  wire  _T_815; // @[Monitor.scala 595:13]
  wire  _T_816; // @[Monitor.scala 595:36]
  wire  _T_817; // @[Monitor.scala 595:27]
  wire  _T_818; // @[Monitor.scala 595:56]
  wire  _T_819; // @[Monitor.scala 595:44]
  wire  _T_821; // @[Monitor.scala 595:12]
  wire  _T_822; // @[Monitor.scala 595:12]
  wire [31:0] _T_824; // @[Monitor.scala 597:26]
  wire  _T_827; // @[Monitor.scala 598:27]
  wire  _GEN_19; // @[Monitor.scala 44:11]
  wire  _GEN_33; // @[Monitor.scala 44:11]
  wire  _GEN_49; // @[Monitor.scala 44:11]
  wire  _GEN_59; // @[Monitor.scala 44:11]
  wire  _GEN_67; // @[Monitor.scala 44:11]
  wire  _GEN_75; // @[Monitor.scala 44:11]
  wire  _GEN_83; // @[Monitor.scala 44:11]
  wire  _GEN_91; // @[Monitor.scala 44:11]
  wire  _GEN_101; // @[Monitor.scala 51:11]
  wire  _GEN_109; // @[Monitor.scala 51:11]
  wire  _GEN_119; // @[Monitor.scala 51:11]
  wire  _GEN_129; // @[Monitor.scala 51:11]
  wire  _GEN_133; // @[Monitor.scala 51:11]
  wire  _GEN_137; // @[Monitor.scala 51:11]
  plusarg_reader #(.FORMAT("tilelink_timeout=%d"), .DEFAULT(0), .WIDTH(32)) plusarg_reader ( // @[PlusArg.scala 44:11]
    .out(plusarg_reader_out)
  );
  assign _T_14 = 13'h3f << io_in_a_bits_size; // @[package.scala 189:77]
  assign _T_16 = ~_T_14[5:0]; // @[package.scala 189:46]
  assign _GEN_18 = {{30'd0}, _T_16}; // @[Edges.scala 22:16]
  assign _T_17 = io_in_a_bits_address & _GEN_18; // @[Edges.scala 22:16]
  assign _T_18 = _T_17 == 36'h0; // @[Edges.scala 22:24]
  assign _T_19 = {{1'd0}, io_in_a_bits_size}; // @[Misc.scala 201:34]
  assign _T_21 = 4'h1 << _T_19[1:0]; // @[OneHot.scala 65:12]
  assign _T_23 = _T_21 | 4'h1; // @[Misc.scala 201:81]
  assign _T_24 = io_in_a_bits_size >= 3'h4; // @[Misc.scala 205:21]
  assign _T_27 = ~io_in_a_bits_address[3]; // @[Misc.scala 210:20]
  assign _T_29 = _T_23[3] & _T_27; // @[Misc.scala 214:38]
  assign _T_30 = _T_24 | _T_29; // @[Misc.scala 214:29]
  assign _T_32 = _T_23[3] & io_in_a_bits_address[3]; // @[Misc.scala 214:38]
  assign _T_33 = _T_24 | _T_32; // @[Misc.scala 214:29]
  assign _T_36 = ~io_in_a_bits_address[2]; // @[Misc.scala 210:20]
  assign _T_37 = _T_27 & _T_36; // @[Misc.scala 213:27]
  assign _T_38 = _T_23[2] & _T_37; // @[Misc.scala 214:38]
  assign _T_39 = _T_30 | _T_38; // @[Misc.scala 214:29]
  assign _T_40 = _T_27 & io_in_a_bits_address[2]; // @[Misc.scala 213:27]
  assign _T_41 = _T_23[2] & _T_40; // @[Misc.scala 214:38]
  assign _T_42 = _T_30 | _T_41; // @[Misc.scala 214:29]
  assign _T_43 = io_in_a_bits_address[3] & _T_36; // @[Misc.scala 213:27]
  assign _T_44 = _T_23[2] & _T_43; // @[Misc.scala 214:38]
  assign _T_45 = _T_33 | _T_44; // @[Misc.scala 214:29]
  assign _T_46 = io_in_a_bits_address[3] & io_in_a_bits_address[2]; // @[Misc.scala 213:27]
  assign _T_47 = _T_23[2] & _T_46; // @[Misc.scala 214:38]
  assign _T_48 = _T_33 | _T_47; // @[Misc.scala 214:29]
  assign _T_51 = ~io_in_a_bits_address[1]; // @[Misc.scala 210:20]
  assign _T_52 = _T_37 & _T_51; // @[Misc.scala 213:27]
  assign _T_53 = _T_23[1] & _T_52; // @[Misc.scala 214:38]
  assign _T_54 = _T_39 | _T_53; // @[Misc.scala 214:29]
  assign _T_55 = _T_37 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_56 = _T_23[1] & _T_55; // @[Misc.scala 214:38]
  assign _T_57 = _T_39 | _T_56; // @[Misc.scala 214:29]
  assign _T_58 = _T_40 & _T_51; // @[Misc.scala 213:27]
  assign _T_59 = _T_23[1] & _T_58; // @[Misc.scala 214:38]
  assign _T_60 = _T_42 | _T_59; // @[Misc.scala 214:29]
  assign _T_61 = _T_40 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_62 = _T_23[1] & _T_61; // @[Misc.scala 214:38]
  assign _T_63 = _T_42 | _T_62; // @[Misc.scala 214:29]
  assign _T_64 = _T_43 & _T_51; // @[Misc.scala 213:27]
  assign _T_65 = _T_23[1] & _T_64; // @[Misc.scala 214:38]
  assign _T_66 = _T_45 | _T_65; // @[Misc.scala 214:29]
  assign _T_67 = _T_43 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_68 = _T_23[1] & _T_67; // @[Misc.scala 214:38]
  assign _T_69 = _T_45 | _T_68; // @[Misc.scala 214:29]
  assign _T_70 = _T_46 & _T_51; // @[Misc.scala 213:27]
  assign _T_71 = _T_23[1] & _T_70; // @[Misc.scala 214:38]
  assign _T_72 = _T_48 | _T_71; // @[Misc.scala 214:29]
  assign _T_73 = _T_46 & io_in_a_bits_address[1]; // @[Misc.scala 213:27]
  assign _T_74 = _T_23[1] & _T_73; // @[Misc.scala 214:38]
  assign _T_75 = _T_48 | _T_74; // @[Misc.scala 214:29]
  assign _T_78 = ~io_in_a_bits_address[0]; // @[Misc.scala 210:20]
  assign _T_79 = _T_52 & _T_78; // @[Misc.scala 213:27]
  assign _T_80 = _T_23[0] & _T_79; // @[Misc.scala 214:38]
  assign _T_81 = _T_54 | _T_80; // @[Misc.scala 214:29]
  assign _T_82 = _T_52 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_83 = _T_23[0] & _T_82; // @[Misc.scala 214:38]
  assign _T_84 = _T_54 | _T_83; // @[Misc.scala 214:29]
  assign _T_85 = _T_55 & _T_78; // @[Misc.scala 213:27]
  assign _T_86 = _T_23[0] & _T_85; // @[Misc.scala 214:38]
  assign _T_87 = _T_57 | _T_86; // @[Misc.scala 214:29]
  assign _T_88 = _T_55 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_89 = _T_23[0] & _T_88; // @[Misc.scala 214:38]
  assign _T_90 = _T_57 | _T_89; // @[Misc.scala 214:29]
  assign _T_91 = _T_58 & _T_78; // @[Misc.scala 213:27]
  assign _T_92 = _T_23[0] & _T_91; // @[Misc.scala 214:38]
  assign _T_93 = _T_60 | _T_92; // @[Misc.scala 214:29]
  assign _T_94 = _T_58 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_95 = _T_23[0] & _T_94; // @[Misc.scala 214:38]
  assign _T_96 = _T_60 | _T_95; // @[Misc.scala 214:29]
  assign _T_97 = _T_61 & _T_78; // @[Misc.scala 213:27]
  assign _T_98 = _T_23[0] & _T_97; // @[Misc.scala 214:38]
  assign _T_99 = _T_63 | _T_98; // @[Misc.scala 214:29]
  assign _T_100 = _T_61 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_101 = _T_23[0] & _T_100; // @[Misc.scala 214:38]
  assign _T_102 = _T_63 | _T_101; // @[Misc.scala 214:29]
  assign _T_103 = _T_64 & _T_78; // @[Misc.scala 213:27]
  assign _T_104 = _T_23[0] & _T_103; // @[Misc.scala 214:38]
  assign _T_105 = _T_66 | _T_104; // @[Misc.scala 214:29]
  assign _T_106 = _T_64 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_107 = _T_23[0] & _T_106; // @[Misc.scala 214:38]
  assign _T_108 = _T_66 | _T_107; // @[Misc.scala 214:29]
  assign _T_109 = _T_67 & _T_78; // @[Misc.scala 213:27]
  assign _T_110 = _T_23[0] & _T_109; // @[Misc.scala 214:38]
  assign _T_111 = _T_69 | _T_110; // @[Misc.scala 214:29]
  assign _T_112 = _T_67 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_113 = _T_23[0] & _T_112; // @[Misc.scala 214:38]
  assign _T_114 = _T_69 | _T_113; // @[Misc.scala 214:29]
  assign _T_115 = _T_70 & _T_78; // @[Misc.scala 213:27]
  assign _T_116 = _T_23[0] & _T_115; // @[Misc.scala 214:38]
  assign _T_117 = _T_72 | _T_116; // @[Misc.scala 214:29]
  assign _T_118 = _T_70 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_119 = _T_23[0] & _T_118; // @[Misc.scala 214:38]
  assign _T_120 = _T_72 | _T_119; // @[Misc.scala 214:29]
  assign _T_121 = _T_73 & _T_78; // @[Misc.scala 213:27]
  assign _T_122 = _T_23[0] & _T_121; // @[Misc.scala 214:38]
  assign _T_123 = _T_75 | _T_122; // @[Misc.scala 214:29]
  assign _T_124 = _T_73 & io_in_a_bits_address[0]; // @[Misc.scala 213:27]
  assign _T_125 = _T_23[0] & _T_124; // @[Misc.scala 214:38]
  assign _T_126 = _T_75 | _T_125; // @[Misc.scala 214:29]
  assign _T_133 = {_T_102,_T_99,_T_96,_T_93,_T_90,_T_87,_T_84,_T_81}; // @[Cat.scala 29:58]
  assign _T_141 = {_T_126,_T_123,_T_120,_T_117,_T_114,_T_111,_T_108,_T_105,_T_133}; // @[Cat.scala 29:58]
  assign _T_160 = io_in_a_bits_opcode == 3'h6; // @[Monitor.scala 79:25]
  assign _T_162 = io_in_a_bits_address ^ 36'ha000000; // @[Parameters.scala 137:31]
  assign _T_163 = {1'b0,$signed(_T_162)}; // @[Parameters.scala 137:49]
  assign _T_165 = $signed(_T_163) & -37'sh200000; // @[Parameters.scala 137:52]
  assign _T_166 = $signed(_T_165) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_167 = io_in_a_bits_address ^ 36'h800000000; // @[Parameters.scala 137:31]
  assign _T_168 = {1'b0,$signed(_T_167)}; // @[Parameters.scala 137:49]
  assign _T_170 = $signed(_T_168) & -37'sh800000000; // @[Parameters.scala 137:52]
  assign _T_171 = $signed(_T_170) == 37'sh0; // @[Parameters.scala 137:67]
  assign _T_172 = _T_166 | _T_171; // @[Parameters.scala 552:42]
  assign _T_177 = ~reset; // @[Monitor.scala 44:11]
  assign _T_186 = _T_24 | reset; // @[Monitor.scala 44:11]
  assign _T_187 = ~_T_186; // @[Monitor.scala 44:11]
  assign _T_189 = _T_18 | reset; // @[Monitor.scala 44:11]
  assign _T_190 = ~_T_189; // @[Monitor.scala 44:11]
  assign _T_191 = io_in_a_bits_param <= 3'h2; // @[Bundles.scala 110:27]
  assign _T_193 = _T_191 | reset; // @[Monitor.scala 44:11]
  assign _T_194 = ~_T_193; // @[Monitor.scala 44:11]
  assign _T_195 = ~io_in_a_bits_mask; // @[Monitor.scala 86:18]
  assign _T_196 = _T_195 == 16'h0; // @[Monitor.scala 86:31]
  assign _T_198 = _T_196 | reset; // @[Monitor.scala 44:11]
  assign _T_199 = ~_T_198; // @[Monitor.scala 44:11]
  assign _T_200 = ~io_in_a_bits_corrupt; // @[Monitor.scala 87:18]
  assign _T_202 = _T_200 | reset; // @[Monitor.scala 44:11]
  assign _T_203 = ~_T_202; // @[Monitor.scala 44:11]
  assign _T_204 = io_in_a_bits_opcode == 3'h7; // @[Monitor.scala 90:25]
  assign _T_239 = io_in_a_bits_param != 3'h0; // @[Monitor.scala 97:31]
  assign _T_241 = _T_239 | reset; // @[Monitor.scala 44:11]
  assign _T_242 = ~_T_241; // @[Monitor.scala 44:11]
  assign _T_252 = io_in_a_bits_opcode == 3'h4; // @[Monitor.scala 102:25]
  assign _T_254 = io_in_a_bits_size <= 3'h6; // @[Parameters.scala 93:42]
  assign _T_268 = _T_254 & _T_172; // @[Parameters.scala 551:56]
  assign _T_271 = _T_268 | reset; // @[Monitor.scala 44:11]
  assign _T_272 = ~_T_271; // @[Monitor.scala 44:11]
  assign _T_279 = io_in_a_bits_param == 3'h0; // @[Monitor.scala 106:31]
  assign _T_281 = _T_279 | reset; // @[Monitor.scala 44:11]
  assign _T_282 = ~_T_281; // @[Monitor.scala 44:11]
  assign _T_283 = io_in_a_bits_mask == _T_141; // @[Monitor.scala 107:30]
  assign _T_285 = _T_283 | reset; // @[Monitor.scala 44:11]
  assign _T_286 = ~_T_285; // @[Monitor.scala 44:11]
  assign _T_291 = io_in_a_bits_opcode == 3'h0; // @[Monitor.scala 111:25]
  assign _T_326 = io_in_a_bits_opcode == 3'h1; // @[Monitor.scala 119:25]
  assign _T_357 = ~_T_141; // @[Monitor.scala 124:33]
  assign _T_358 = io_in_a_bits_mask & _T_357; // @[Monitor.scala 124:31]
  assign _T_359 = _T_358 == 16'h0; // @[Monitor.scala 124:40]
  assign _T_361 = _T_359 | reset; // @[Monitor.scala 44:11]
  assign _T_362 = ~_T_361; // @[Monitor.scala 44:11]
  assign _T_363 = io_in_a_bits_opcode == 3'h2; // @[Monitor.scala 127:25]
  assign _T_365 = io_in_a_bits_size <= 3'h4; // @[Parameters.scala 93:42]
  assign _T_373 = _T_365 & _T_166; // @[Parameters.scala 551:56]
  assign _T_384 = _T_373 | reset; // @[Monitor.scala 44:11]
  assign _T_385 = ~_T_384; // @[Monitor.scala 44:11]
  assign _T_392 = io_in_a_bits_param <= 3'h4; // @[Bundles.scala 140:33]
  assign _T_394 = _T_392 | reset; // @[Monitor.scala 44:11]
  assign _T_395 = ~_T_394; // @[Monitor.scala 44:11]
  assign _T_400 = io_in_a_bits_opcode == 3'h3; // @[Monitor.scala 135:25]
  assign _T_429 = io_in_a_bits_param <= 3'h3; // @[Bundles.scala 147:30]
  assign _T_431 = _T_429 | reset; // @[Monitor.scala 44:11]
  assign _T_432 = ~_T_431; // @[Monitor.scala 44:11]
  assign _T_437 = io_in_a_bits_opcode == 3'h5; // @[Monitor.scala 143:25]
  assign _T_447 = _T_254 & _T_166; // @[Parameters.scala 551:56]
  assign _T_458 = _T_447 | reset; // @[Monitor.scala 44:11]
  assign _T_459 = ~_T_458; // @[Monitor.scala 44:11]
  assign _T_466 = io_in_a_bits_param <= 3'h1; // @[Bundles.scala 160:28]
  assign _T_468 = _T_466 | reset; // @[Monitor.scala 44:11]
  assign _T_469 = ~_T_468; // @[Monitor.scala 44:11]
  assign _T_478 = io_in_d_bits_opcode <= 3'h6; // @[Bundles.scala 44:24]
  assign _T_480 = _T_478 | reset; // @[Monitor.scala 51:11]
  assign _T_481 = ~_T_480; // @[Monitor.scala 51:11]
  assign _T_492 = io_in_d_bits_opcode == 3'h6; // @[Monitor.scala 307:25]
  assign _T_496 = io_in_d_bits_size >= 3'h4; // @[Monitor.scala 309:27]
  assign _T_498 = _T_496 | reset; // @[Monitor.scala 51:11]
  assign _T_499 = ~_T_498; // @[Monitor.scala 51:11]
  assign _T_500 = io_in_d_bits_param == 2'h0; // @[Monitor.scala 310:28]
  assign _T_502 = _T_500 | reset; // @[Monitor.scala 51:11]
  assign _T_503 = ~_T_502; // @[Monitor.scala 51:11]
  assign _T_504 = ~io_in_d_bits_corrupt; // @[Monitor.scala 311:15]
  assign _T_506 = _T_504 | reset; // @[Monitor.scala 51:11]
  assign _T_507 = ~_T_506; // @[Monitor.scala 51:11]
  assign _T_508 = ~io_in_d_bits_denied; // @[Monitor.scala 312:15]
  assign _T_510 = _T_508 | reset; // @[Monitor.scala 51:11]
  assign _T_511 = ~_T_510; // @[Monitor.scala 51:11]
  assign _T_512 = io_in_d_bits_opcode == 3'h4; // @[Monitor.scala 315:25]
  assign _T_523 = io_in_d_bits_param <= 2'h2; // @[Bundles.scala 104:26]
  assign _T_525 = _T_523 | reset; // @[Monitor.scala 51:11]
  assign _T_526 = ~_T_525; // @[Monitor.scala 51:11]
  assign _T_527 = io_in_d_bits_param != 2'h2; // @[Monitor.scala 320:28]
  assign _T_529 = _T_527 | reset; // @[Monitor.scala 51:11]
  assign _T_530 = ~_T_529; // @[Monitor.scala 51:11]
  assign _T_540 = io_in_d_bits_opcode == 3'h5; // @[Monitor.scala 325:25]
  assign _T_560 = _T_508 | io_in_d_bits_corrupt; // @[Monitor.scala 331:30]
  assign _T_562 = _T_560 | reset; // @[Monitor.scala 51:11]
  assign _T_563 = ~_T_562; // @[Monitor.scala 51:11]
  assign _T_569 = io_in_d_bits_opcode == 3'h0; // @[Monitor.scala 335:25]
  assign _T_586 = io_in_d_bits_opcode == 3'h1; // @[Monitor.scala 343:25]
  assign _T_604 = io_in_d_bits_opcode == 3'h2; // @[Monitor.scala 351:25]
  assign _T_636 = io_in_a_ready & io_in_a_valid; // @[Decoupled.scala 40:37]
  assign _T_643 = ~io_in_a_bits_opcode[2]; // @[Edges.scala 93:28]
  assign _T_647 = _T_645 - 2'h1; // @[Edges.scala 231:28]
  assign _T_648 = _T_645 == 2'h0; // @[Edges.scala 232:25]
  assign _T_661 = ~_T_648; // @[Monitor.scala 386:22]
  assign _T_662 = io_in_a_valid & _T_661; // @[Monitor.scala 386:19]
  assign _T_663 = io_in_a_bits_opcode == _T_656; // @[Monitor.scala 387:32]
  assign _T_665 = _T_663 | reset; // @[Monitor.scala 44:11]
  assign _T_666 = ~_T_665; // @[Monitor.scala 44:11]
  assign _T_667 = io_in_a_bits_param == _T_657; // @[Monitor.scala 388:32]
  assign _T_669 = _T_667 | reset; // @[Monitor.scala 44:11]
  assign _T_670 = ~_T_669; // @[Monitor.scala 44:11]
  assign _T_671 = io_in_a_bits_size == _T_658; // @[Monitor.scala 389:32]
  assign _T_673 = _T_671 | reset; // @[Monitor.scala 44:11]
  assign _T_674 = ~_T_673; // @[Monitor.scala 44:11]
  assign _T_675 = io_in_a_bits_source == _T_659; // @[Monitor.scala 390:32]
  assign _T_677 = _T_675 | reset; // @[Monitor.scala 44:11]
  assign _T_678 = ~_T_677; // @[Monitor.scala 44:11]
  assign _T_679 = io_in_a_bits_address == _T_660; // @[Monitor.scala 391:32]
  assign _T_681 = _T_679 | reset; // @[Monitor.scala 44:11]
  assign _T_682 = ~_T_681; // @[Monitor.scala 44:11]
  assign _T_684 = _T_636 & _T_648; // @[Monitor.scala 393:20]
  assign _T_685 = io_in_d_ready & io_in_d_valid; // @[Decoupled.scala 40:37]
  assign _T_687 = 13'h3f << io_in_d_bits_size; // @[package.scala 189:77]
  assign _T_689 = ~_T_687[5:0]; // @[package.scala 189:46]
  assign _T_695 = _T_693 - 2'h1; // @[Edges.scala 231:28]
  assign _T_696 = _T_693 == 2'h0; // @[Edges.scala 232:25]
  assign _T_710 = ~_T_696; // @[Monitor.scala 538:22]
  assign _T_711 = io_in_d_valid & _T_710; // @[Monitor.scala 538:19]
  assign _T_712 = io_in_d_bits_opcode == _T_704; // @[Monitor.scala 539:29]
  assign _T_714 = _T_712 | reset; // @[Monitor.scala 51:11]
  assign _T_715 = ~_T_714; // @[Monitor.scala 51:11]
  assign _T_716 = io_in_d_bits_param == _T_705; // @[Monitor.scala 540:29]
  assign _T_718 = _T_716 | reset; // @[Monitor.scala 51:11]
  assign _T_719 = ~_T_718; // @[Monitor.scala 51:11]
  assign _T_720 = io_in_d_bits_size == _T_706; // @[Monitor.scala 541:29]
  assign _T_722 = _T_720 | reset; // @[Monitor.scala 51:11]
  assign _T_723 = ~_T_722; // @[Monitor.scala 51:11]
  assign _T_724 = io_in_d_bits_source == _T_707; // @[Monitor.scala 542:29]
  assign _T_726 = _T_724 | reset; // @[Monitor.scala 51:11]
  assign _T_727 = ~_T_726; // @[Monitor.scala 51:11]
  assign _T_728 = io_in_d_bits_sink == _T_708; // @[Monitor.scala 543:29]
  assign _T_730 = _T_728 | reset; // @[Monitor.scala 51:11]
  assign _T_731 = ~_T_730; // @[Monitor.scala 51:11]
  assign _T_732 = io_in_d_bits_denied == _T_709; // @[Monitor.scala 544:29]
  assign _T_734 = _T_732 | reset; // @[Monitor.scala 51:11]
  assign _T_735 = ~_T_734; // @[Monitor.scala 51:11]
  assign _T_737 = _T_685 & _T_696; // @[Monitor.scala 546:20]
  assign _T_750 = _T_748 - 2'h1; // @[Edges.scala 231:28]
  assign _T_751 = _T_748 == 2'h0; // @[Edges.scala 232:25]
  assign _T_769 = _T_767 - 2'h1; // @[Edges.scala 231:28]
  assign _T_770 = _T_767 == 2'h0; // @[Edges.scala 232:25]
  assign _T_780 = _T_636 & _T_751; // @[Monitor.scala 574:27]
  assign _T_782 = 16'h1 << io_in_a_bits_source; // @[OneHot.scala 58:35]
  assign _T_783 = _T_738 >> io_in_a_bits_source; // @[Monitor.scala 576:23]
  assign _T_785 = ~_T_783[0]; // @[Monitor.scala 576:14]
  assign _T_787 = _T_785 | reset; // @[Monitor.scala 576:13]
  assign _T_788 = ~_T_787; // @[Monitor.scala 576:13]
  assign _GEN_15 = _T_780 ? _T_782 : 16'h0; // @[Monitor.scala 574:72]
  assign _T_792 = _T_685 & _T_770; // @[Monitor.scala 581:27]
  assign _T_794 = ~_T_492; // @[Monitor.scala 581:75]
  assign _T_795 = _T_792 & _T_794; // @[Monitor.scala 581:72]
  assign _T_796 = 16'h1 << io_in_d_bits_source; // @[OneHot.scala 58:35]
  assign _T_797 = _GEN_15 | _T_738; // @[Monitor.scala 583:21]
  assign _T_798 = _T_797 >> io_in_d_bits_source; // @[Monitor.scala 583:32]
  assign _T_801 = _T_798[0] | reset; // @[Monitor.scala 51:11]
  assign _T_802 = ~_T_801; // @[Monitor.scala 51:11]
  assign _GEN_16 = _T_795 ? _T_796 : 16'h0; // @[Monitor.scala 581:91]
  assign _T_803 = _GEN_15 != _GEN_16; // @[Monitor.scala 587:20]
  assign _T_804 = _GEN_15 != 16'h0; // @[Monitor.scala 587:40]
  assign _T_805 = ~_T_804; // @[Monitor.scala 587:33]
  assign _T_806 = _T_803 | _T_805; // @[Monitor.scala 587:30]
  assign _T_808 = _T_806 | reset; // @[Monitor.scala 51:11]
  assign _T_809 = ~_T_808; // @[Monitor.scala 51:11]
  assign _T_810 = _T_738 | _GEN_15; // @[Monitor.scala 590:27]
  assign _T_811 = ~_GEN_16; // @[Monitor.scala 590:38]
  assign _T_812 = _T_810 & _T_811; // @[Monitor.scala 590:36]
  assign _T_814 = _T_738 != 16'h0; // @[Monitor.scala 595:23]
  assign _T_815 = ~_T_814; // @[Monitor.scala 595:13]
  assign _T_816 = plusarg_reader_out == 32'h0; // @[Monitor.scala 595:36]
  assign _T_817 = _T_815 | _T_816; // @[Monitor.scala 595:27]
  assign _T_818 = _T_813 < plusarg_reader_out; // @[Monitor.scala 595:56]
  assign _T_819 = _T_817 | _T_818; // @[Monitor.scala 595:44]
  assign _T_821 = _T_819 | reset; // @[Monitor.scala 595:12]
  assign _T_822 = ~_T_821; // @[Monitor.scala 595:12]
  assign _T_824 = _T_813 + 32'h1; // @[Monitor.scala 597:26]
  assign _T_827 = _T_636 | _T_685; // @[Monitor.scala 598:27]
  assign _GEN_19 = io_in_a_valid & _T_160; // @[Monitor.scala 44:11]
  assign _GEN_33 = io_in_a_valid & _T_204; // @[Monitor.scala 44:11]
  assign _GEN_49 = io_in_a_valid & _T_252; // @[Monitor.scala 44:11]
  assign _GEN_59 = io_in_a_valid & _T_291; // @[Monitor.scala 44:11]
  assign _GEN_67 = io_in_a_valid & _T_326; // @[Monitor.scala 44:11]
  assign _GEN_75 = io_in_a_valid & _T_363; // @[Monitor.scala 44:11]
  assign _GEN_83 = io_in_a_valid & _T_400; // @[Monitor.scala 44:11]
  assign _GEN_91 = io_in_a_valid & _T_437; // @[Monitor.scala 44:11]
  assign _GEN_101 = io_in_d_valid & _T_492; // @[Monitor.scala 51:11]
  assign _GEN_109 = io_in_d_valid & _T_512; // @[Monitor.scala 51:11]
  assign _GEN_119 = io_in_d_valid & _T_540; // @[Monitor.scala 51:11]
  assign _GEN_129 = io_in_d_valid & _T_569; // @[Monitor.scala 51:11]
  assign _GEN_133 = io_in_d_valid & _T_586; // @[Monitor.scala 51:11]
  assign _GEN_137 = io_in_d_valid & _T_604; // @[Monitor.scala 51:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  _T_645 = _RAND_0[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  _T_656 = _RAND_1[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  _T_657 = _RAND_2[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  _T_658 = _RAND_3[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  _T_659 = _RAND_4[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {2{`RANDOM}};
  _T_660 = _RAND_5[35:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  _T_693 = _RAND_6[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  _T_704 = _RAND_7[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{`RANDOM}};
  _T_705 = _RAND_8[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  _T_706 = _RAND_9[2:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  _T_707 = _RAND_10[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{`RANDOM}};
  _T_708 = _RAND_11[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  _T_709 = _RAND_12[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  _T_738 = _RAND_13[15:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_14 = {1{`RANDOM}};
  _T_748 = _RAND_14[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_15 = {1{`RANDOM}};
  _T_767 = _RAND_15[1:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_16 = {1{`RANDOM}};
  _T_813 = _RAND_16[31:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      _T_645 <= 2'h0;
    end else if (_T_636) begin
      if (_T_648) begin
        if (_T_643) begin
          _T_645 <= _T_16[5:4];
        end else begin
          _T_645 <= 2'h0;
        end
      end else begin
        _T_645 <= _T_647;
      end
    end
    if (_T_684) begin
      _T_656 <= io_in_a_bits_opcode;
    end
    if (_T_684) begin
      _T_657 <= io_in_a_bits_param;
    end
    if (_T_684) begin
      _T_658 <= io_in_a_bits_size;
    end
    if (_T_684) begin
      _T_659 <= io_in_a_bits_source;
    end
    if (_T_684) begin
      _T_660 <= io_in_a_bits_address;
    end
    if (reset) begin
      _T_693 <= 2'h0;
    end else if (_T_685) begin
      if (_T_696) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_693 <= _T_689[5:4];
        end else begin
          _T_693 <= 2'h0;
        end
      end else begin
        _T_693 <= _T_695;
      end
    end
    if (_T_737) begin
      _T_704 <= io_in_d_bits_opcode;
    end
    if (_T_737) begin
      _T_705 <= io_in_d_bits_param;
    end
    if (_T_737) begin
      _T_706 <= io_in_d_bits_size;
    end
    if (_T_737) begin
      _T_707 <= io_in_d_bits_source;
    end
    if (_T_737) begin
      _T_708 <= io_in_d_bits_sink;
    end
    if (_T_737) begin
      _T_709 <= io_in_d_bits_denied;
    end
    if (reset) begin
      _T_738 <= 16'h0;
    end else begin
      _T_738 <= _T_812;
    end
    if (reset) begin
      _T_748 <= 2'h0;
    end else if (_T_636) begin
      if (_T_751) begin
        if (_T_643) begin
          _T_748 <= _T_16[5:4];
        end else begin
          _T_748 <= 2'h0;
        end
      end else begin
        _T_748 <= _T_750;
      end
    end
    if (reset) begin
      _T_767 <= 2'h0;
    end else if (_T_685) begin
      if (_T_770) begin
        if (io_in_d_bits_opcode[0]) begin
          _T_767 <= _T_689[5:4];
        end else begin
          _T_767 <= 2'h0;
        end
      end else begin
        _T_767 <= _T_769;
      end
    end
    if (reset) begin
      _T_813 <= 32'h0;
    end else if (_T_827) begin
      _T_813 <= 32'h0;
    end else begin
      _T_813 <= _T_824;
    end
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_177) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock type unsupported by manager (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_177) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_177) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquireBlock from a client which does not support Probe (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_177) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_187) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock smaller than a beat (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_187) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_190) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock address not aligned to size (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_190) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock carries invalid grow param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_199) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock contains invalid mask (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_199) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_19 & _T_203) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquireBlock is corrupt (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_19 & _T_203) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_177) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm type unsupported by manager (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_177) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_177) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries AcquirePerm from a client which does not support Probe (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_177) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_187) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm smaller than a beat (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_187) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_190) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm address not aligned to size (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_190) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_194) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm carries invalid grow param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_194) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_242) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm requests NtoB (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_242) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_199) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm contains invalid mask (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_199) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_33 & _T_203) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel AcquirePerm is corrupt (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_33 & _T_203) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_272) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Get type unsupported by manager (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_272) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_190) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get address not aligned to size (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_190) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_282) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get carries invalid param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_282) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_286) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get contains invalid mask (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_286) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_49 & _T_203) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Get is corrupt (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_49 & _T_203) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_59 & _T_272) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutFull type unsupported by manager (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_59 & _T_272) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_59 & _T_190) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull address not aligned to size (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_59 & _T_190) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_59 & _T_282) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull carries invalid param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_59 & _T_282) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_59 & _T_286) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutFull contains invalid mask (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_59 & _T_286) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_67 & _T_272) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries PutPartial type unsupported by manager (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_67 & _T_272) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_67 & _T_190) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial address not aligned to size (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_67 & _T_190) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_67 & _T_282) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial carries invalid param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_67 & _T_282) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_67 & _T_362) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel PutPartial contains invalid mask (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_67 & _T_362) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_385) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Arithmetic type unsupported by manager (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_385) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_190) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic address not aligned to size (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_190) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_395) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic carries invalid opcode param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_395) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_75 & _T_286) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Arithmetic contains invalid mask (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_75 & _T_286) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_385) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Logical type unsupported by manager (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_385) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_190) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical address not aligned to size (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_190) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_432) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical carries invalid opcode param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_432) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_83 & _T_286) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Logical contains invalid mask (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_83 & _T_286) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_91 & _T_459) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel carries Hint type unsupported by manager (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_91 & _T_459) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_91 & _T_190) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint address not aligned to size (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_91 & _T_190) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_91 & _T_469) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint carries invalid opcode param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_91 & _T_469) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_91 & _T_286) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint contains invalid mask (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_91 & _T_286) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_91 & _T_203) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel Hint is corrupt (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_91 & _T_203) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_in_d_valid & _T_481) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel has invalid opcode (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_in_d_valid & _T_481) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_101 & _T_499) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck smaller than a beat (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_101 & _T_499) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_101 & _T_503) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseeAck carries invalid param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_101 & _T_503) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_101 & _T_507) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is corrupt (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_101 & _T_507) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_101 & _T_511) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel ReleaseAck is denied (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_101 & _T_511) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_109 & _T_177) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid sink ID (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_109 & _T_177) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_109 & _T_499) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant smaller than a beat (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_109 & _T_499) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_109 & _T_526) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries invalid cap param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_109 & _T_526) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_109 & _T_530) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant carries toN param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_109 & _T_530) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_109 & _T_507) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel Grant is corrupt (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_109 & _T_507) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_119 & _T_177) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid sink ID (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_119 & _T_177) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_119 & _T_499) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData smaller than a beat (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_119 & _T_499) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_119 & _T_526) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries invalid cap param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_119 & _T_526) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_119 & _T_530) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData carries toN param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_119 & _T_530) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_119 & _T_563) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel GrantData is denied but not corrupt (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_119 & _T_563) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_129 & _T_503) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck carries invalid param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_129 & _T_503) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_129 & _T_507) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAck is corrupt (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_129 & _T_507) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_133 & _T_503) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData carries invalid param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_133 & _T_503) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_133 & _T_563) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel AccessAckData is denied but not corrupt (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_133 & _T_563) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_137 & _T_503) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck carries invalid param (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_137 & _T_503) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_GEN_137 & _T_507) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel HintAck is corrupt (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_GEN_137 & _T_507) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_662 & _T_666) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel opcode changed within multibeat operation (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_662 & _T_666) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_662 & _T_670) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel param changed within multibeat operation (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_662 & _T_670) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_662 & _T_674) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel size changed within multibeat operation (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_662 & _T_674) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_662 & _T_678) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel source changed within multibeat operation (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_662 & _T_678) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_662 & _T_682) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel address changed with multibeat operation (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:44 assert(cond, message)\n"); // @[Monitor.scala 44:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_662 & _T_682) begin
          $fatal; // @[Monitor.scala 44:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_711 & _T_715) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel opcode changed within multibeat operation (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_711 & _T_715) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_711 & _T_719) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel param changed within multibeat operation (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_711 & _T_719) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_711 & _T_723) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel size changed within multibeat operation (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_711 & _T_723) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_711 & _T_727) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel source changed within multibeat operation (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_711 & _T_727) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_711 & _T_731) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel sink changed with multibeat operation (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_711 & _T_731) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_711 & _T_735) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel denied changed with multibeat operation (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_711 & _T_735) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_780 & _T_788) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' channel re-used a source ID (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:576 assert(!inflight(bundle.a.bits.source), \"'A' channel re-used a source ID\" + extra)\n"); // @[Monitor.scala 576:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_780 & _T_788) begin
          $fatal; // @[Monitor.scala 576:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_795 & _T_802) begin
          $fwrite(32'h80000002,"Assertion Failed: 'D' channel acknowledged for nothing inflight (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_795 & _T_802) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_809) begin
          $fwrite(32'h80000002,"Assertion Failed: 'A' and 'D' concurrent, despite minlatency 2 (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:51 assert(cond, message)\n"); // @[Monitor.scala 51:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_809) begin
          $fatal; // @[Monitor.scala 51:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_822) begin
          $fwrite(32'h80000002,"Assertion Failed: TileLink timeout expired (connected at BusTopologies.scala:61:91)\n    at Monitor.scala:595 assert (!inflight.orR || limit === 0.U || watchdog < limit, \"TileLink timeout expired\" + extra)\n"); // @[Monitor.scala 595:12]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_822) begin
          $fatal; // @[Monitor.scala 595:12]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
