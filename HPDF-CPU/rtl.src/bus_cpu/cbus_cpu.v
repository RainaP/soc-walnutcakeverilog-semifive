module cbus_cpu (/*AUTOARG*/
   // Outputs
   CPU_0__AXI4__CPU__PERIPORT__AWREADY,
   CPU_0__AXI4__CPU__PERIPORT__WREADY,
   CPU_0__AXI4__CPU__PERIPORT__BID,
   CPU_0__AXI4__CPU__PERIPORT__BVALID,
   CPU_0__AXI4__CPU__PERIPORT__BRESP,
   CPU_0__AXI4__CPU__PERIPORT__ARREADY,
   CPU_0__AXI4__CPU__PERIPORT__RID, CPU_0__AXI4__CPU__PERIPORT__RDATA,
   CPU_0__AXI4__CPU__PERIPORT__RRESP,
   CPU_0__AXI4__CPU__PERIPORT__RLAST,
   CPU_0__AXI4__CPU__PERIPORT__RVALID, AXI4__CBUS__CB2FB__AWID,
   AXI4__CBUS__CB2FB__AWADDR, AXI4__CBUS__CB2FB__AWLEN,
   AXI4__CBUS__CB2FB__AWSIZE, AXI4__CBUS__CB2FB__AWBURST,
   AXI4__CBUS__CB2FB__AWLOCK, AXI4__CBUS__CB2FB__AWCACHE,
   AXI4__CBUS__CB2FB__AWPROT, AXI4__CBUS__CB2FB__AWVALID,
   AXI4__CBUS__CB2FB__WDATA, AXI4__CBUS__CB2FB__WSTRB,
   AXI4__CBUS__CB2FB__WLAST, AXI4__CBUS__CB2FB__WVALID,
   AXI4__CBUS__CB2FB__BREADY, AXI4__CBUS__CB2FB__ARID,
   AXI4__CBUS__CB2FB__ARADDR, AXI4__CBUS__CB2FB__ARLEN,
   AXI4__CBUS__CB2FB__ARSIZE, AXI4__CBUS__CB2FB__ARBURST,
   AXI4__CBUS__CB2FB__ARLOCK, AXI4__CBUS__CB2FB__ARCACHE,
   AXI4__CBUS__CB2FB__ARPROT, AXI4__CBUS__CB2FB__ARVALID,
   AXI4__CBUS__CB2FB__RREADY, AXI4__TX__AWID, AXI4__TX__AWADDR,
   AXI4__TX__AWLEN, AXI4__TX__AWSIZE, AXI4__TX__AWBURST,
   AXI4__TX__AWLOCK, AXI4__TX__AWCACHE, AXI4__TX__AWPROT,
   AXI4__TX__AWVALID, AXI4__TX__WDATA, AXI4__TX__WSTRB,
   AXI4__TX__WLAST, AXI4__TX__WVALID, AXI4__TX__BREADY,
   AXI4__TX__ARID, AXI4__TX__ARADDR, AXI4__TX__ARLEN,
   AXI4__TX__ARSIZE, AXI4__TX__ARBURST, AXI4__TX__ARLOCK,
   AXI4__TX__ARCACHE, AXI4__TX__ARPROT, AXI4__TX__ARVALID,
   AXI4__TX__RREADY, APB3__CBUS__SCU__PADDR, APB3__CBUS__SCU__PWDATA,
   APB3__CBUS__SCU__PENABLE, APB3__CBUS__SCU__PWRITE,
   APB3__CBUS__SCU__PSEL,
   // Inputs
   CLK__TX, RSTN__TX, CLK__CBUS, RSTN__CBUS,
   CPU_0__AXI4__CPU__PERIPORT__AWID,
   CPU_0__AXI4__CPU__PERIPORT__AWADDR,
   CPU_0__AXI4__CPU__PERIPORT__AWLEN,
   CPU_0__AXI4__CPU__PERIPORT__AWSIZE,
   CPU_0__AXI4__CPU__PERIPORT__AWBURST,
   CPU_0__AXI4__CPU__PERIPORT__AWLOCK,
   CPU_0__AXI4__CPU__PERIPORT__AWCACHE,
   CPU_0__AXI4__CPU__PERIPORT__AWPROT,
   CPU_0__AXI4__CPU__PERIPORT__AWVALID,
   CPU_0__AXI4__CPU__PERIPORT__WDATA,
   CPU_0__AXI4__CPU__PERIPORT__WSTRB,
   CPU_0__AXI4__CPU__PERIPORT__WLAST,
   CPU_0__AXI4__CPU__PERIPORT__WVALID,
   CPU_0__AXI4__CPU__PERIPORT__BREADY,
   CPU_0__AXI4__CPU__PERIPORT__ARID,
   CPU_0__AXI4__CPU__PERIPORT__ARADDR,
   CPU_0__AXI4__CPU__PERIPORT__ARLEN,
   CPU_0__AXI4__CPU__PERIPORT__ARSIZE,
   CPU_0__AXI4__CPU__PERIPORT__ARBURST,
   CPU_0__AXI4__CPU__PERIPORT__ARLOCK,
   CPU_0__AXI4__CPU__PERIPORT__ARCACHE,
   CPU_0__AXI4__CPU__PERIPORT__ARPROT,
   CPU_0__AXI4__CPU__PERIPORT__ARVALID,
   CPU_0__AXI4__CPU__PERIPORT__RREADY, AXI4__CBUS__CB2FB__AWREADY,
   AXI4__CBUS__CB2FB__WREADY, AXI4__CBUS__CB2FB__BID,
   AXI4__CBUS__CB2FB__BVALID, AXI4__CBUS__CB2FB__BRESP,
   AXI4__CBUS__CB2FB__ARREADY, AXI4__CBUS__CB2FB__RID,
   AXI4__CBUS__CB2FB__RDATA, AXI4__CBUS__CB2FB__RRESP,
   AXI4__CBUS__CB2FB__RLAST, AXI4__CBUS__CB2FB__RVALID,
   AXI4__TX__AWREADY, AXI4__TX__WREADY, AXI4__TX__BID,
   AXI4__TX__BVALID, AXI4__TX__BRESP, AXI4__TX__ARREADY,
   AXI4__TX__RID, AXI4__TX__RDATA, AXI4__TX__RRESP, AXI4__TX__RLAST,
   AXI4__TX__RVALID, APB3__CBUS__SCU__PRDATA, APB3__CBUS__SCU__PREADY
   );

    parameter   PERI_AW = 32; // PERIPORT
    parameter   PERI_DW = 64;
    parameter   PERI_IW = 1;
    parameter   PERI_UW = 0;

    parameter   FBUS_AW = 32; // TO FBUS
    parameter   FBUS_DW = 64;
    parameter   FBUS_IW = 1;
    parameter   FBUS_UW = 0;

    parameter   CBUS_AW = 32; // TO SoC CBUS
    parameter   CBUS_DW = 32;
    parameter   CBUS_IW = 1;
    parameter   CBUS_UW = 0;

    localparam  PERI_SW = PERI_DW/8;
    localparam  FBUS_SW = FBUS_DW/8;
    localparam  CBUS_SW = CBUS_DW/8;


    input           CLK__TX;
    input           RSTN__TX;
    input           CLK__CBUS;
    input           RSTN__CBUS;

    // Peri Port from CPU
    input   [PERI_IW-1:0] CPU_0__AXI4__CPU__PERIPORT__AWID;
    input   [PERI_AW-1:0] CPU_0__AXI4__CPU__PERIPORT__AWADDR;
    input   [7:0]       CPU_0__AXI4__CPU__PERIPORT__AWLEN;
    input   [2:0]       CPU_0__AXI4__CPU__PERIPORT__AWSIZE;
    input   [1:0]       CPU_0__AXI4__CPU__PERIPORT__AWBURST;
    input   [0:0]       CPU_0__AXI4__CPU__PERIPORT__AWLOCK;
    input   [3:0]       CPU_0__AXI4__CPU__PERIPORT__AWCACHE;
    input   [2:0]       CPU_0__AXI4__CPU__PERIPORT__AWPROT;
    input               CPU_0__AXI4__CPU__PERIPORT__AWVALID;
    output              CPU_0__AXI4__CPU__PERIPORT__AWREADY;
    input   [PERI_DW-1:0] CPU_0__AXI4__CPU__PERIPORT__WDATA;
    input   [PERI_SW-1:0] CPU_0__AXI4__CPU__PERIPORT__WSTRB;
    input               CPU_0__AXI4__CPU__PERIPORT__WLAST;
    input               CPU_0__AXI4__CPU__PERIPORT__WVALID;
    output              CPU_0__AXI4__CPU__PERIPORT__WREADY;
    output  [PERI_IW-1:0] CPU_0__AXI4__CPU__PERIPORT__BID;
    output              CPU_0__AXI4__CPU__PERIPORT__BVALID;
    output  [1:0]       CPU_0__AXI4__CPU__PERIPORT__BRESP;
    input               CPU_0__AXI4__CPU__PERIPORT__BREADY;
    input   [PERI_IW-1:0] CPU_0__AXI4__CPU__PERIPORT__ARID;
    input   [PERI_AW-1:0] CPU_0__AXI4__CPU__PERIPORT__ARADDR;
    input   [7:0]       CPU_0__AXI4__CPU__PERIPORT__ARLEN;
    input   [2:0]       CPU_0__AXI4__CPU__PERIPORT__ARSIZE;
    input   [1:0]       CPU_0__AXI4__CPU__PERIPORT__ARBURST;
    input   [0:0]       CPU_0__AXI4__CPU__PERIPORT__ARLOCK;
    input   [3:0]       CPU_0__AXI4__CPU__PERIPORT__ARCACHE;
    input   [2:0]       CPU_0__AXI4__CPU__PERIPORT__ARPROT;
    input               CPU_0__AXI4__CPU__PERIPORT__ARVALID;
    output              CPU_0__AXI4__CPU__PERIPORT__ARREADY;
    output  [PERI_IW-1:0] CPU_0__AXI4__CPU__PERIPORT__RID;
    output  [PERI_DW-1:0] CPU_0__AXI4__CPU__PERIPORT__RDATA;
    output  [1:0]       CPU_0__AXI4__CPU__PERIPORT__RRESP;
    output              CPU_0__AXI4__CPU__PERIPORT__RLAST;
    output              CPU_0__AXI4__CPU__PERIPORT__RVALID;
    input               CPU_0__AXI4__CPU__PERIPORT__RREADY;

    // CBUS to FBUS bridge AXI
    output  [FBUS_IW-1:0] AXI4__CBUS__CB2FB__AWID;
    output  [FBUS_AW-1:0] AXI4__CBUS__CB2FB__AWADDR;
    output  [7:0]       AXI4__CBUS__CB2FB__AWLEN;
    output  [2:0]       AXI4__CBUS__CB2FB__AWSIZE;
    output  [1:0]       AXI4__CBUS__CB2FB__AWBURST;
    output  [0:0]       AXI4__CBUS__CB2FB__AWLOCK;
    output  [3:0]       AXI4__CBUS__CB2FB__AWCACHE;
    output  [2:0]       AXI4__CBUS__CB2FB__AWPROT;
    output              AXI4__CBUS__CB2FB__AWVALID;
    input               AXI4__CBUS__CB2FB__AWREADY;
    output  [FBUS_DW-1:0] AXI4__CBUS__CB2FB__WDATA;
    output  [FBUS_SW-1:0] AXI4__CBUS__CB2FB__WSTRB;
    output              AXI4__CBUS__CB2FB__WLAST;
    output              AXI4__CBUS__CB2FB__WVALID;
    input               AXI4__CBUS__CB2FB__WREADY;
    input  [FBUS_IW-1:0]  AXI4__CBUS__CB2FB__BID;
    input               AXI4__CBUS__CB2FB__BVALID;
    input  [1:0]        AXI4__CBUS__CB2FB__BRESP;
    output              AXI4__CBUS__CB2FB__BREADY;
    output  [FBUS_IW-1:0] AXI4__CBUS__CB2FB__ARID;
    output  [FBUS_AW-1:0] AXI4__CBUS__CB2FB__ARADDR;
    output  [7:0]       AXI4__CBUS__CB2FB__ARLEN;
    output  [2:0]       AXI4__CBUS__CB2FB__ARSIZE;
    output  [1:0]       AXI4__CBUS__CB2FB__ARBURST;
    output  [0:0]       AXI4__CBUS__CB2FB__ARLOCK;
    output  [3:0]       AXI4__CBUS__CB2FB__ARCACHE;
    output  [2:0]       AXI4__CBUS__CB2FB__ARPROT;
    output              AXI4__CBUS__CB2FB__ARVALID;
    input               AXI4__CBUS__CB2FB__ARREADY;
    input  [FBUS_IW-1:0]  AXI4__CBUS__CB2FB__RID;
    input  [FBUS_DW-1:0]  AXI4__CBUS__CB2FB__RDATA;
    input  [1:0]        AXI4__CBUS__CB2FB__RRESP;
    input               AXI4__CBUS__CB2FB__RLAST;
    input               AXI4__CBUS__CB2FB__RVALID;
    output              AXI4__CBUS__CB2FB__RREADY;

    // AXI4 Master to access CBUS
    output  [CBUS_IW-1:0] AXI4__TX__AWID;
    output  [CBUS_AW-1:0] AXI4__TX__AWADDR;
    output  [7:0]       AXI4__TX__AWLEN;
    output  [2:0]       AXI4__TX__AWSIZE;
    output  [1:0]       AXI4__TX__AWBURST;
    output              AXI4__TX__AWLOCK;
    output  [3:0]       AXI4__TX__AWCACHE;
    output  [2:0]       AXI4__TX__AWPROT;
    output              AXI4__TX__AWVALID;
    input               AXI4__TX__AWREADY;
    output  [CBUS_DW-1:0] AXI4__TX__WDATA;
    output  [CBUS_SW-1:0] AXI4__TX__WSTRB;
    output              AXI4__TX__WLAST;
    output              AXI4__TX__WVALID;
    input               AXI4__TX__WREADY;
    input   [CBUS_IW-1:0] AXI4__TX__BID;
    input               AXI4__TX__BVALID;
    input   [1:0]       AXI4__TX__BRESP;
    output              AXI4__TX__BREADY;
    output  [CBUS_IW-1:0] AXI4__TX__ARID;
    output  [CBUS_AW-1:0] AXI4__TX__ARADDR;
    output  [7:0]       AXI4__TX__ARLEN;
    output  [2:0]       AXI4__TX__ARSIZE;
    output  [1:0]       AXI4__TX__ARBURST;
    output              AXI4__TX__ARLOCK;
    output  [3:0]       AXI4__TX__ARCACHE;
    output  [2:0]       AXI4__TX__ARPROT;
    output              AXI4__TX__ARVALID;
    input               AXI4__TX__ARREADY;
    input   [CBUS_IW-1:0] AXI4__TX__RID;
    input   [CBUS_DW-1:0] AXI4__TX__RDATA;
    input   [1:0]       AXI4__TX__RRESP;
    input               AXI4__TX__RLAST;
    input               AXI4__TX__RVALID;
    output              AXI4__TX__RREADY;

    // APB master to access CPU subsystem SCU
    output [31:0]       APB3__CBUS__SCU__PADDR;
    output [31:0]       APB3__CBUS__SCU__PWDATA;
    output              APB3__CBUS__SCU__PENABLE;
    output              APB3__CBUS__SCU__PWRITE;
    output              APB3__CBUS__SCU__PSEL;
    input [31:0]        APB3__CBUS__SCU__PRDATA;
    input               APB3__CBUS__SCU__PREADY;

`include "./axi_1x3_cpu_pbus/axi_1x3_cpu_pbus_DW_axi_all_includes.vh"
`include "./axi2axi_cpu_pbus/axi2axi_cpu_pbus_DW_axi_x2x_all_includes.vh"
`include "./axi2apb_cpu_cpupbus/axi2apb_cpu_cpupbus_DW_axi_x2p_all_includes.vh"
// Wire & Reg
// {{{
    wor [31:0] NC;
    wire [31:0] APB3__CBUS__S1__PRDATA = 0;
    wire [31:0] APB3__CBUS__S2__PRDATA = 0;
    wire [31:0] APB3__CBUS__S3__PRDATA = 0;
    wire [31:0] APB3__CBUS__S4__PRDATA = 0;
    wire [31:0] APB3__CBUS__S5__PRDATA = 0;
    /*AUTOWIRE*/
    // Beginning of automatic wires (for undeclared instantiated-module outputs)
    wire [`axi2apb_cpu_cpupbus_X2P_APB_ADDR_WIDTH-1:0] APB3__CBUS__PADDR;// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire		APB3__CBUS__PENABLE;	// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire [`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0] APB3__CBUS__PWDATA;// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire		APB3__CBUS__PWRITE;	// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire		APB3__CBUS__S1__PSEL;	// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire		APB3__CBUS__S2__PSEL;	// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire		APB3__CBUS__S3__PSEL;	// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire		APB3__CBUS__S4__PSEL;	// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire		APB3__CBUS__S5__PSEL;	// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire [PERI_AW-1:0]	AXI3__CBUS__SOC__ARADDR;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [1:0]		AXI3__CBUS__SOC__ARBURST;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [3:0]		AXI3__CBUS__SOC__ARCACHE;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [PERI_IW-1:0]	AXI3__CBUS__SOC__ARID;	// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [7:0]		AXI3__CBUS__SOC__ARLEN;	// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [1:0]		AXI3__CBUS__SOC__ARLOCK;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [2:0]		AXI3__CBUS__SOC__ARPROT;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire		AXI3__CBUS__SOC__ARREADY;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [2:0]		AXI3__CBUS__SOC__ARSIZE;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire		AXI3__CBUS__SOC__ARVALID;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [PERI_AW-1:0]	AXI3__CBUS__SOC__AWADDR;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [1:0]		AXI3__CBUS__SOC__AWBURST;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [3:0]		AXI3__CBUS__SOC__AWCACHE;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [PERI_IW-1:0]	AXI3__CBUS__SOC__AWID;	// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [7:0]		AXI3__CBUS__SOC__AWLEN;	// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [1:0]		AXI3__CBUS__SOC__AWLOCK;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [2:0]		AXI3__CBUS__SOC__AWPROT;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire		AXI3__CBUS__SOC__AWREADY;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [2:0]		AXI3__CBUS__SOC__AWSIZE;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire		AXI3__CBUS__SOC__AWVALID;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [`axi2axi_cpu_pbus_X2X_MP_IDW-1:0] AXI3__CBUS__SOC__BID;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire		AXI3__CBUS__SOC__BREADY;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [`axi2axi_cpu_pbus_X2X_BRW-1:0] AXI3__CBUS__SOC__BRESP;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire		AXI3__CBUS__SOC__BVALID;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_MP_DW-1:0] AXI3__CBUS__SOC__RDATA;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_MP_IDW-1:0] AXI3__CBUS__SOC__RID;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire		AXI3__CBUS__SOC__RLAST;	// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire		AXI3__CBUS__SOC__RREADY;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [`axi2axi_cpu_pbus_X2X_RRW-1:0] AXI3__CBUS__SOC__RRESP;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire		AXI3__CBUS__SOC__RVALID;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [PERI_DW-1:0]	AXI3__CBUS__SOC__WDATA;	// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [PERI_IW-1:0]	AXI3__CBUS__SOC__WID;	// From AXI4TO3_CBUS_0 of axi4to3.v
    wire		AXI3__CBUS__SOC__WLAST;	// From AXI4TO3_CBUS_0 of axi4to3.v
    wire		AXI3__CBUS__SOC__WREADY;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [PERI_SW-1:0]	AXI3__CBUS__SOC__WSTRB;	// From AXI4TO3_CBUS_0 of axi4to3.v
    wire		AXI3__CBUS__SOC__WVALID;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [`axi2axi_cpu_pbus_X2X_SP_AW-1:0] AXI3__TX__ARADDR;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_BTW-1:0] AXI3__TX__ARBURST;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_CTW-1:0] AXI3__TX__ARCACHE;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_SP_IDW-1:0] AXI3__TX__ARID;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_SP_BLW-1:0] AXI3__TX__ARLEN;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_LTW-1:0] AXI3__TX__ARLOCK;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_PTW-1:0] AXI3__TX__ARPROT;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire		AXI3__TX__ARREADY;	// From AXI3TO4_CBUS_0 of axi3to4.v
    wire [`axi2axi_cpu_pbus_X2X_BSW-1:0] AXI3__TX__ARSIZE;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire		AXI3__TX__ARVALID;	// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_SP_AW-1:0] AXI3__TX__AWADDR;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_BTW-1:0] AXI3__TX__AWBURST;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_CTW-1:0] AXI3__TX__AWCACHE;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_SP_IDW-1:0] AXI3__TX__AWID;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_SP_BLW-1:0] AXI3__TX__AWLEN;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_LTW-1:0] AXI3__TX__AWLOCK;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_PTW-1:0] AXI3__TX__AWPROT;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire		AXI3__TX__AWREADY;	// From AXI3TO4_CBUS_0 of axi3to4.v
    wire [`axi2axi_cpu_pbus_X2X_BSW-1:0] AXI3__TX__AWSIZE;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire		AXI3__TX__AWVALID;	// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [CBUS_IW-1:0]	AXI3__TX__BID;		// From AXI3TO4_CBUS_0 of axi3to4.v
    wire		AXI3__TX__BREADY;	// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [1:0]		AXI3__TX__BRESP;	// From AXI3TO4_CBUS_0 of axi3to4.v
    wire		AXI3__TX__BVALID;	// From AXI3TO4_CBUS_0 of axi3to4.v
    wire [CBUS_DW-1:0]	AXI3__TX__RDATA;	// From AXI3TO4_CBUS_0 of axi3to4.v
    wire [CBUS_IW-1:0]	AXI3__TX__RID;		// From AXI3TO4_CBUS_0 of axi3to4.v
    wire		AXI3__TX__RLAST;	// From AXI3TO4_CBUS_0 of axi3to4.v
    wire		AXI3__TX__RREADY;	// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [1:0]		AXI3__TX__RRESP;	// From AXI3TO4_CBUS_0 of axi3to4.v
    wire		AXI3__TX__RVALID;	// From AXI3TO4_CBUS_0 of axi3to4.v
    wire [`axi2axi_cpu_pbus_X2X_SP_DW-1:0] AXI3__TX__WDATA;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi2axi_cpu_pbus_X2X_SP_IDW-1:0] AXI3__TX__WID;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire		AXI3__TX__WLAST;	// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire		AXI3__TX__WREADY;	// From AXI3TO4_CBUS_0 of axi3to4.v
    wire [`axi2axi_cpu_pbus_X2X_SP_SW-1:0] AXI3__TX__WSTRB;// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire		AXI3__TX__WVALID;	// From AXI64TO32_CBUS_0 of axi2axi_cpu_pbus_DW_axi_x2x.v
    wire [`axi_1x3_cpu_pbus_AXI_AW-1:0] AXI4__CBUS__LOCAL__ARADDR;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_BTW-1:0] AXI4__CBUS__LOCAL__ARBURST;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_CTW-1:0] AXI4__CBUS__LOCAL__ARCACHE;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_SIDW-1:0] AXI4__CBUS__LOCAL__ARID;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_BLW-1:0] AXI4__CBUS__LOCAL__ARLEN;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_LTW-1:0] AXI4__CBUS__LOCAL__ARLOCK;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_PTW-1:0] AXI4__CBUS__LOCAL__ARPROT;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire		AXI4__CBUS__LOCAL__ARREADY;// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire [`axi_1x3_cpu_pbus_AXI_BSW-1:0] AXI4__CBUS__LOCAL__ARSIZE;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire		AXI4__CBUS__LOCAL__ARVALID;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_AW-1:0] AXI4__CBUS__LOCAL__AWADDR;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_BTW-1:0] AXI4__CBUS__LOCAL__AWBURST;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_CTW-1:0] AXI4__CBUS__LOCAL__AWCACHE;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_SIDW-1:0] AXI4__CBUS__LOCAL__AWID;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_BLW-1:0] AXI4__CBUS__LOCAL__AWLEN;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_LTW-1:0] AXI4__CBUS__LOCAL__AWLOCK;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_PTW-1:0] AXI4__CBUS__LOCAL__AWPROT;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire		AXI4__CBUS__LOCAL__AWREADY;// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire [`axi_1x3_cpu_pbus_AXI_BSW-1:0] AXI4__CBUS__LOCAL__AWSIZE;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire		AXI4__CBUS__LOCAL__AWVALID;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi2apb_cpu_cpupbus_X2P_AXI_SIDW-1:0] AXI4__CBUS__LOCAL__BID;// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire		AXI4__CBUS__LOCAL__BREADY;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [1:0]		AXI4__CBUS__LOCAL__BRESP;// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire		AXI4__CBUS__LOCAL__BVALID;// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire [`axi2apb_cpu_cpupbus_X2P_AXI_DW-1:0] AXI4__CBUS__LOCAL__RDATA;// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire [`axi2apb_cpu_cpupbus_X2P_AXI_SIDW-1:0] AXI4__CBUS__LOCAL__RID;// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire		AXI4__CBUS__LOCAL__RLAST;// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire		AXI4__CBUS__LOCAL__RREADY;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [1:0]		AXI4__CBUS__LOCAL__RRESP;// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire		AXI4__CBUS__LOCAL__RVALID;// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire [`axi_1x3_cpu_pbus_AXI_DW-1:0] AXI4__CBUS__LOCAL__WDATA;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire		AXI4__CBUS__LOCAL__WLAST;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire		AXI4__CBUS__LOCAL__WREADY;// From AXI2APB_CBUS_0 of axi2apb_cpu_cpupbus_DW_axi_x2p.v
    wire [`axi_1x3_cpu_pbus_AXI_SW-1:0] AXI4__CBUS__LOCAL__WSTRB;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire		AXI4__CBUS__LOCAL__WVALID;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_AW-1:0] AXI4__CBUS__SOC__ARADDR;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_BTW-1:0] AXI4__CBUS__SOC__ARBURST;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_CTW-1:0] AXI4__CBUS__SOC__ARCACHE;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_SIDW-1:0] AXI4__CBUS__SOC__ARID;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_BLW-1:0] AXI4__CBUS__SOC__ARLEN;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_LTW-1:0] AXI4__CBUS__SOC__ARLOCK;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_PTW-1:0] AXI4__CBUS__SOC__ARPROT;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire		AXI4__CBUS__SOC__ARREADY;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [`axi_1x3_cpu_pbus_AXI_BSW-1:0] AXI4__CBUS__SOC__ARSIZE;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire		AXI4__CBUS__SOC__ARVALID;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_AW-1:0] AXI4__CBUS__SOC__AWADDR;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_BTW-1:0] AXI4__CBUS__SOC__AWBURST;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_CTW-1:0] AXI4__CBUS__SOC__AWCACHE;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_SIDW-1:0] AXI4__CBUS__SOC__AWID;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_BLW-1:0] AXI4__CBUS__SOC__AWLEN;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_LTW-1:0] AXI4__CBUS__SOC__AWLOCK;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [`axi_1x3_cpu_pbus_AXI_PTW-1:0] AXI4__CBUS__SOC__AWPROT;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire		AXI4__CBUS__SOC__AWREADY;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [`axi_1x3_cpu_pbus_AXI_BSW-1:0] AXI4__CBUS__SOC__AWSIZE;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire		AXI4__CBUS__SOC__AWVALID;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [PERI_IW-1:0]	AXI4__CBUS__SOC__BID;	// From AXI4TO3_CBUS_0 of axi4to3.v
    wire		AXI4__CBUS__SOC__BREADY;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [1:0]		AXI4__CBUS__SOC__BRESP;	// From AXI4TO3_CBUS_0 of axi4to3.v
    wire		AXI4__CBUS__SOC__BVALID;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [PERI_DW-1:0]	AXI4__CBUS__SOC__RDATA;	// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [PERI_IW-1:0]	AXI4__CBUS__SOC__RID;	// From AXI4TO3_CBUS_0 of axi4to3.v
    wire		AXI4__CBUS__SOC__RLAST;	// From AXI4TO3_CBUS_0 of axi4to3.v
    wire		AXI4__CBUS__SOC__RREADY;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire [1:0]		AXI4__CBUS__SOC__RRESP;	// From AXI4TO3_CBUS_0 of axi4to3.v
    wire		AXI4__CBUS__SOC__RVALID;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [`axi_1x3_cpu_pbus_AXI_DW-1:0] AXI4__CBUS__SOC__WDATA;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire		AXI4__CBUS__SOC__WLAST;	// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire		AXI4__CBUS__SOC__WREADY;// From AXI4TO3_CBUS_0 of axi4to3.v
    wire [`axi_1x3_cpu_pbus_AXI_SW-1:0] AXI4__CBUS__SOC__WSTRB;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    wire		AXI4__CBUS__SOC__WVALID;// From AXI_1X3_CBUS_0 of axi_1x3_cpu_pbus_DW_axi.v
    // End of automatics
    /*AUTOREG*/
// }}}

    /* axi_1x3_cpu_pbus_DW_axi AUTO_TEMPLATE (
        .aclk               (CLK__CBUS),
        .aresetn            (RSTN__CBUS),
        .\(.*\)_m1          (CPU_0__AXI4__CPU__PERIPORT__@"(upcase \\"\1\\")"[]),
        .\(.*\)_s1          (AXI4__CBUS__CB2FB__@"(upcase \\"\1\\")"[]),
        .\(.*\)_s2          (AXI4__CBUS__LOCAL__@"(upcase \\"\1\\")"[]),
        .\(.*\)_s3          (AXI4__CBUS__SOC__@"(upcase \\"\1\\")"[]),
        .csysreq            (1'b0),
        .csysack            (),
        .cactive            (),
        .dbg_.*             (),
    );*/
    axi_1x3_cpu_pbus_DW_axi AXI_1X3_CBUS_0 (/*AUTOINST*/
					    // Outputs
					    .awready_m1		(CPU_0__AXI4__CPU__PERIPORT__AWREADY), // Templated
					    .wready_m1		(CPU_0__AXI4__CPU__PERIPORT__WREADY), // Templated
					    .bvalid_m1		(CPU_0__AXI4__CPU__PERIPORT__BVALID), // Templated
					    .bid_m1		(CPU_0__AXI4__CPU__PERIPORT__BID[`axi_1x3_cpu_pbus_AXI_IDW_M1-1:0]), // Templated
					    .bresp_m1		(CPU_0__AXI4__CPU__PERIPORT__BRESP[`axi_1x3_cpu_pbus_AXI_BRW-1:0]), // Templated
					    .arready_m1		(CPU_0__AXI4__CPU__PERIPORT__ARREADY), // Templated
					    .rvalid_m1		(CPU_0__AXI4__CPU__PERIPORT__RVALID), // Templated
					    .rid_m1		(CPU_0__AXI4__CPU__PERIPORT__RID[`axi_1x3_cpu_pbus_AXI_IDW_M1-1:0]), // Templated
					    .rdata_m1		(CPU_0__AXI4__CPU__PERIPORT__RDATA[`axi_1x3_cpu_pbus_AXI_DW-1:0]), // Templated
					    .rlast_m1		(CPU_0__AXI4__CPU__PERIPORT__RLAST), // Templated
					    .rresp_m1		(CPU_0__AXI4__CPU__PERIPORT__RRESP[`axi_1x3_cpu_pbus_AXI_RRW-1:0]), // Templated
					    .awvalid_s1		(AXI4__CBUS__CB2FB__AWVALID), // Templated
					    .awaddr_s1		(AXI4__CBUS__CB2FB__AWADDR[`axi_1x3_cpu_pbus_AXI_AW-1:0]), // Templated
					    .awid_s1		(AXI4__CBUS__CB2FB__AWID[`axi_1x3_cpu_pbus_AXI_SIDW-1:0]), // Templated
					    .awlen_s1		(AXI4__CBUS__CB2FB__AWLEN[`axi_1x3_cpu_pbus_AXI_BLW-1:0]), // Templated
					    .awsize_s1		(AXI4__CBUS__CB2FB__AWSIZE[`axi_1x3_cpu_pbus_AXI_BSW-1:0]), // Templated
					    .awburst_s1		(AXI4__CBUS__CB2FB__AWBURST[`axi_1x3_cpu_pbus_AXI_BTW-1:0]), // Templated
					    .awlock_s1		(AXI4__CBUS__CB2FB__AWLOCK[`axi_1x3_cpu_pbus_AXI_LTW-1:0]), // Templated
					    .awcache_s1		(AXI4__CBUS__CB2FB__AWCACHE[`axi_1x3_cpu_pbus_AXI_CTW-1:0]), // Templated
					    .awprot_s1		(AXI4__CBUS__CB2FB__AWPROT[`axi_1x3_cpu_pbus_AXI_PTW-1:0]), // Templated
					    .wvalid_s1		(AXI4__CBUS__CB2FB__WVALID), // Templated
					    .wdata_s1		(AXI4__CBUS__CB2FB__WDATA[`axi_1x3_cpu_pbus_AXI_DW-1:0]), // Templated
					    .wstrb_s1		(AXI4__CBUS__CB2FB__WSTRB[`axi_1x3_cpu_pbus_AXI_SW-1:0]), // Templated
					    .wlast_s1		(AXI4__CBUS__CB2FB__WLAST), // Templated
					    .bready_s1		(AXI4__CBUS__CB2FB__BREADY), // Templated
					    .arvalid_s1		(AXI4__CBUS__CB2FB__ARVALID), // Templated
					    .arid_s1		(AXI4__CBUS__CB2FB__ARID[`axi_1x3_cpu_pbus_AXI_SIDW-1:0]), // Templated
					    .araddr_s1		(AXI4__CBUS__CB2FB__ARADDR[`axi_1x3_cpu_pbus_AXI_AW-1:0]), // Templated
					    .arlen_s1		(AXI4__CBUS__CB2FB__ARLEN[`axi_1x3_cpu_pbus_AXI_BLW-1:0]), // Templated
					    .arsize_s1		(AXI4__CBUS__CB2FB__ARSIZE[`axi_1x3_cpu_pbus_AXI_BSW-1:0]), // Templated
					    .arburst_s1		(AXI4__CBUS__CB2FB__ARBURST[`axi_1x3_cpu_pbus_AXI_BTW-1:0]), // Templated
					    .arlock_s1		(AXI4__CBUS__CB2FB__ARLOCK[`axi_1x3_cpu_pbus_AXI_LTW-1:0]), // Templated
					    .arcache_s1		(AXI4__CBUS__CB2FB__ARCACHE[`axi_1x3_cpu_pbus_AXI_CTW-1:0]), // Templated
					    .arprot_s1		(AXI4__CBUS__CB2FB__ARPROT[`axi_1x3_cpu_pbus_AXI_PTW-1:0]), // Templated
					    .rready_s1		(AXI4__CBUS__CB2FB__RREADY), // Templated
					    .awvalid_s2		(AXI4__CBUS__LOCAL__AWVALID), // Templated
					    .awaddr_s2		(AXI4__CBUS__LOCAL__AWADDR[`axi_1x3_cpu_pbus_AXI_AW-1:0]), // Templated
					    .awid_s2		(AXI4__CBUS__LOCAL__AWID[`axi_1x3_cpu_pbus_AXI_SIDW-1:0]), // Templated
					    .awlen_s2		(AXI4__CBUS__LOCAL__AWLEN[`axi_1x3_cpu_pbus_AXI_BLW-1:0]), // Templated
					    .awsize_s2		(AXI4__CBUS__LOCAL__AWSIZE[`axi_1x3_cpu_pbus_AXI_BSW-1:0]), // Templated
					    .awburst_s2		(AXI4__CBUS__LOCAL__AWBURST[`axi_1x3_cpu_pbus_AXI_BTW-1:0]), // Templated
					    .awlock_s2		(AXI4__CBUS__LOCAL__AWLOCK[`axi_1x3_cpu_pbus_AXI_LTW-1:0]), // Templated
					    .awcache_s2		(AXI4__CBUS__LOCAL__AWCACHE[`axi_1x3_cpu_pbus_AXI_CTW-1:0]), // Templated
					    .awprot_s2		(AXI4__CBUS__LOCAL__AWPROT[`axi_1x3_cpu_pbus_AXI_PTW-1:0]), // Templated
					    .wvalid_s2		(AXI4__CBUS__LOCAL__WVALID), // Templated
					    .wdata_s2		(AXI4__CBUS__LOCAL__WDATA[`axi_1x3_cpu_pbus_AXI_DW-1:0]), // Templated
					    .wstrb_s2		(AXI4__CBUS__LOCAL__WSTRB[`axi_1x3_cpu_pbus_AXI_SW-1:0]), // Templated
					    .wlast_s2		(AXI4__CBUS__LOCAL__WLAST), // Templated
					    .bready_s2		(AXI4__CBUS__LOCAL__BREADY), // Templated
					    .arvalid_s2		(AXI4__CBUS__LOCAL__ARVALID), // Templated
					    .arid_s2		(AXI4__CBUS__LOCAL__ARID[`axi_1x3_cpu_pbus_AXI_SIDW-1:0]), // Templated
					    .araddr_s2		(AXI4__CBUS__LOCAL__ARADDR[`axi_1x3_cpu_pbus_AXI_AW-1:0]), // Templated
					    .arlen_s2		(AXI4__CBUS__LOCAL__ARLEN[`axi_1x3_cpu_pbus_AXI_BLW-1:0]), // Templated
					    .arsize_s2		(AXI4__CBUS__LOCAL__ARSIZE[`axi_1x3_cpu_pbus_AXI_BSW-1:0]), // Templated
					    .arburst_s2		(AXI4__CBUS__LOCAL__ARBURST[`axi_1x3_cpu_pbus_AXI_BTW-1:0]), // Templated
					    .arlock_s2		(AXI4__CBUS__LOCAL__ARLOCK[`axi_1x3_cpu_pbus_AXI_LTW-1:0]), // Templated
					    .arcache_s2		(AXI4__CBUS__LOCAL__ARCACHE[`axi_1x3_cpu_pbus_AXI_CTW-1:0]), // Templated
					    .arprot_s2		(AXI4__CBUS__LOCAL__ARPROT[`axi_1x3_cpu_pbus_AXI_PTW-1:0]), // Templated
					    .rready_s2		(AXI4__CBUS__LOCAL__RREADY), // Templated
					    .awvalid_s3		(AXI4__CBUS__SOC__AWVALID), // Templated
					    .awaddr_s3		(AXI4__CBUS__SOC__AWADDR[`axi_1x3_cpu_pbus_AXI_AW-1:0]), // Templated
					    .awid_s3		(AXI4__CBUS__SOC__AWID[`axi_1x3_cpu_pbus_AXI_SIDW-1:0]), // Templated
					    .awlen_s3		(AXI4__CBUS__SOC__AWLEN[`axi_1x3_cpu_pbus_AXI_BLW-1:0]), // Templated
					    .awsize_s3		(AXI4__CBUS__SOC__AWSIZE[`axi_1x3_cpu_pbus_AXI_BSW-1:0]), // Templated
					    .awburst_s3		(AXI4__CBUS__SOC__AWBURST[`axi_1x3_cpu_pbus_AXI_BTW-1:0]), // Templated
					    .awlock_s3		(AXI4__CBUS__SOC__AWLOCK[`axi_1x3_cpu_pbus_AXI_LTW-1:0]), // Templated
					    .awcache_s3		(AXI4__CBUS__SOC__AWCACHE[`axi_1x3_cpu_pbus_AXI_CTW-1:0]), // Templated
					    .awprot_s3		(AXI4__CBUS__SOC__AWPROT[`axi_1x3_cpu_pbus_AXI_PTW-1:0]), // Templated
					    .wvalid_s3		(AXI4__CBUS__SOC__WVALID), // Templated
					    .wdata_s3		(AXI4__CBUS__SOC__WDATA[`axi_1x3_cpu_pbus_AXI_DW-1:0]), // Templated
					    .wstrb_s3		(AXI4__CBUS__SOC__WSTRB[`axi_1x3_cpu_pbus_AXI_SW-1:0]), // Templated
					    .wlast_s3		(AXI4__CBUS__SOC__WLAST), // Templated
					    .bready_s3		(AXI4__CBUS__SOC__BREADY), // Templated
					    .arvalid_s3		(AXI4__CBUS__SOC__ARVALID), // Templated
					    .arid_s3		(AXI4__CBUS__SOC__ARID[`axi_1x3_cpu_pbus_AXI_SIDW-1:0]), // Templated
					    .araddr_s3		(AXI4__CBUS__SOC__ARADDR[`axi_1x3_cpu_pbus_AXI_AW-1:0]), // Templated
					    .arlen_s3		(AXI4__CBUS__SOC__ARLEN[`axi_1x3_cpu_pbus_AXI_BLW-1:0]), // Templated
					    .arsize_s3		(AXI4__CBUS__SOC__ARSIZE[`axi_1x3_cpu_pbus_AXI_BSW-1:0]), // Templated
					    .arburst_s3		(AXI4__CBUS__SOC__ARBURST[`axi_1x3_cpu_pbus_AXI_BTW-1:0]), // Templated
					    .arlock_s3		(AXI4__CBUS__SOC__ARLOCK[`axi_1x3_cpu_pbus_AXI_LTW-1:0]), // Templated
					    .arcache_s3		(AXI4__CBUS__SOC__ARCACHE[`axi_1x3_cpu_pbus_AXI_CTW-1:0]), // Templated
					    .arprot_s3		(AXI4__CBUS__SOC__ARPROT[`axi_1x3_cpu_pbus_AXI_PTW-1:0]), // Templated
					    .rready_s3		(AXI4__CBUS__SOC__RREADY), // Templated
					    .dbg_awid_s0	(),		 // Templated
					    .dbg_awaddr_s0	(),		 // Templated
					    .dbg_awlen_s0	(),		 // Templated
					    .dbg_awsize_s0	(),		 // Templated
					    .dbg_awburst_s0	(),		 // Templated
					    .dbg_awlock_s0	(),		 // Templated
					    .dbg_awcache_s0	(),		 // Templated
					    .dbg_awprot_s0	(),		 // Templated
					    .dbg_awvalid_s0	(),		 // Templated
					    .dbg_awready_s0	(),		 // Templated
					    .dbg_wid_s0		(),		 // Templated
					    .dbg_wdata_s0	(),		 // Templated
					    .dbg_wstrb_s0	(),		 // Templated
					    .dbg_wlast_s0	(),		 // Templated
					    .dbg_wvalid_s0	(),		 // Templated
					    .dbg_wready_s0	(),		 // Templated
					    .dbg_bid_s0		(),		 // Templated
					    .dbg_bresp_s0	(),		 // Templated
					    .dbg_bvalid_s0	(),		 // Templated
					    .dbg_bready_s0	(),		 // Templated
					    .dbg_arid_s0	(),		 // Templated
					    .dbg_araddr_s0	(),		 // Templated
					    .dbg_arlen_s0	(),		 // Templated
					    .dbg_arsize_s0	(),		 // Templated
					    .dbg_arburst_s0	(),		 // Templated
					    .dbg_arlock_s0	(),		 // Templated
					    .dbg_arcache_s0	(),		 // Templated
					    .dbg_arprot_s0	(),		 // Templated
					    .dbg_arvalid_s0	(),		 // Templated
					    .dbg_arready_s0	(),		 // Templated
					    .dbg_rid_s0		(),		 // Templated
					    .dbg_rdata_s0	(),		 // Templated
					    .dbg_rresp_s0	(),		 // Templated
					    .dbg_rvalid_s0	(),		 // Templated
					    .dbg_rlast_s0	(),		 // Templated
					    .dbg_rready_s0	(),		 // Templated
					    // Inputs
					    .aclk		(CLK__CBUS),	 // Templated
					    .aresetn		(RSTN__CBUS),	 // Templated
					    .awvalid_m1		(CPU_0__AXI4__CPU__PERIPORT__AWVALID), // Templated
					    .awaddr_m1		(CPU_0__AXI4__CPU__PERIPORT__AWADDR[`axi_1x3_cpu_pbus_AXI_AW-1:0]), // Templated
					    .awid_m1		(CPU_0__AXI4__CPU__PERIPORT__AWID[`axi_1x3_cpu_pbus_AXI_IDW_M1-1:0]), // Templated
					    .awlen_m1		(CPU_0__AXI4__CPU__PERIPORT__AWLEN[`axi_1x3_cpu_pbus_AXI_BLW-1:0]), // Templated
					    .awsize_m1		(CPU_0__AXI4__CPU__PERIPORT__AWSIZE[`axi_1x3_cpu_pbus_AXI_BSW-1:0]), // Templated
					    .awburst_m1		(CPU_0__AXI4__CPU__PERIPORT__AWBURST[`axi_1x3_cpu_pbus_AXI_BTW-1:0]), // Templated
					    .awlock_m1		(CPU_0__AXI4__CPU__PERIPORT__AWLOCK[`axi_1x3_cpu_pbus_AXI_LTW-1:0]), // Templated
					    .awcache_m1		(CPU_0__AXI4__CPU__PERIPORT__AWCACHE[`axi_1x3_cpu_pbus_AXI_CTW-1:0]), // Templated
					    .awprot_m1		(CPU_0__AXI4__CPU__PERIPORT__AWPROT[`axi_1x3_cpu_pbus_AXI_PTW-1:0]), // Templated
					    .wvalid_m1		(CPU_0__AXI4__CPU__PERIPORT__WVALID), // Templated
					    .wdata_m1		(CPU_0__AXI4__CPU__PERIPORT__WDATA[`axi_1x3_cpu_pbus_AXI_DW-1:0]), // Templated
					    .wstrb_m1		(CPU_0__AXI4__CPU__PERIPORT__WSTRB[`axi_1x3_cpu_pbus_AXI_SW-1:0]), // Templated
					    .wlast_m1		(CPU_0__AXI4__CPU__PERIPORT__WLAST), // Templated
					    .bready_m1		(CPU_0__AXI4__CPU__PERIPORT__BREADY), // Templated
					    .arvalid_m1		(CPU_0__AXI4__CPU__PERIPORT__ARVALID), // Templated
					    .arid_m1		(CPU_0__AXI4__CPU__PERIPORT__ARID[`axi_1x3_cpu_pbus_AXI_IDW_M1-1:0]), // Templated
					    .araddr_m1		(CPU_0__AXI4__CPU__PERIPORT__ARADDR[`axi_1x3_cpu_pbus_AXI_AW-1:0]), // Templated
					    .arlen_m1		(CPU_0__AXI4__CPU__PERIPORT__ARLEN[`axi_1x3_cpu_pbus_AXI_BLW-1:0]), // Templated
					    .arsize_m1		(CPU_0__AXI4__CPU__PERIPORT__ARSIZE[`axi_1x3_cpu_pbus_AXI_BSW-1:0]), // Templated
					    .arburst_m1		(CPU_0__AXI4__CPU__PERIPORT__ARBURST[`axi_1x3_cpu_pbus_AXI_BTW-1:0]), // Templated
					    .arlock_m1		(CPU_0__AXI4__CPU__PERIPORT__ARLOCK[`axi_1x3_cpu_pbus_AXI_LTW-1:0]), // Templated
					    .arcache_m1		(CPU_0__AXI4__CPU__PERIPORT__ARCACHE[`axi_1x3_cpu_pbus_AXI_CTW-1:0]), // Templated
					    .arprot_m1		(CPU_0__AXI4__CPU__PERIPORT__ARPROT[`axi_1x3_cpu_pbus_AXI_PTW-1:0]), // Templated
					    .rready_m1		(CPU_0__AXI4__CPU__PERIPORT__RREADY), // Templated
					    .awready_s1		(AXI4__CBUS__CB2FB__AWREADY), // Templated
					    .wready_s1		(AXI4__CBUS__CB2FB__WREADY), // Templated
					    .bvalid_s1		(AXI4__CBUS__CB2FB__BVALID), // Templated
					    .bid_s1		(AXI4__CBUS__CB2FB__BID[`axi_1x3_cpu_pbus_AXI_SIDW-1:0]), // Templated
					    .bresp_s1		(AXI4__CBUS__CB2FB__BRESP[`axi_1x3_cpu_pbus_AXI_BRW-1:0]), // Templated
					    .arready_s1		(AXI4__CBUS__CB2FB__ARREADY), // Templated
					    .rvalid_s1		(AXI4__CBUS__CB2FB__RVALID), // Templated
					    .rid_s1		(AXI4__CBUS__CB2FB__RID[`axi_1x3_cpu_pbus_AXI_SIDW-1:0]), // Templated
					    .rdata_s1		(AXI4__CBUS__CB2FB__RDATA[`axi_1x3_cpu_pbus_AXI_DW-1:0]), // Templated
					    .rlast_s1		(AXI4__CBUS__CB2FB__RLAST), // Templated
					    .rresp_s1		(AXI4__CBUS__CB2FB__RRESP[`axi_1x3_cpu_pbus_AXI_RRW-1:0]), // Templated
					    .awready_s2		(AXI4__CBUS__LOCAL__AWREADY), // Templated
					    .wready_s2		(AXI4__CBUS__LOCAL__WREADY), // Templated
					    .bvalid_s2		(AXI4__CBUS__LOCAL__BVALID), // Templated
					    .bid_s2		(AXI4__CBUS__LOCAL__BID[`axi_1x3_cpu_pbus_AXI_SIDW-1:0]), // Templated
					    .bresp_s2		(AXI4__CBUS__LOCAL__BRESP[`axi_1x3_cpu_pbus_AXI_BRW-1:0]), // Templated
					    .arready_s2		(AXI4__CBUS__LOCAL__ARREADY), // Templated
					    .rvalid_s2		(AXI4__CBUS__LOCAL__RVALID), // Templated
					    .rid_s2		(AXI4__CBUS__LOCAL__RID[`axi_1x3_cpu_pbus_AXI_SIDW-1:0]), // Templated
					    .rdata_s2		(AXI4__CBUS__LOCAL__RDATA[`axi_1x3_cpu_pbus_AXI_DW-1:0]), // Templated
					    .rlast_s2		(AXI4__CBUS__LOCAL__RLAST), // Templated
					    .rresp_s2		(AXI4__CBUS__LOCAL__RRESP[`axi_1x3_cpu_pbus_AXI_RRW-1:0]), // Templated
					    .awready_s3		(AXI4__CBUS__SOC__AWREADY), // Templated
					    .wready_s3		(AXI4__CBUS__SOC__WREADY), // Templated
					    .bvalid_s3		(AXI4__CBUS__SOC__BVALID), // Templated
					    .bid_s3		(AXI4__CBUS__SOC__BID[`axi_1x3_cpu_pbus_AXI_SIDW-1:0]), // Templated
					    .bresp_s3		(AXI4__CBUS__SOC__BRESP[`axi_1x3_cpu_pbus_AXI_BRW-1:0]), // Templated
					    .arready_s3		(AXI4__CBUS__SOC__ARREADY), // Templated
					    .rvalid_s3		(AXI4__CBUS__SOC__RVALID), // Templated
					    .rid_s3		(AXI4__CBUS__SOC__RID[`axi_1x3_cpu_pbus_AXI_SIDW-1:0]), // Templated
					    .rdata_s3		(AXI4__CBUS__SOC__RDATA[`axi_1x3_cpu_pbus_AXI_DW-1:0]), // Templated
					    .rlast_s3		(AXI4__CBUS__SOC__RLAST), // Templated
					    .rresp_s3		(AXI4__CBUS__SOC__RRESP[`axi_1x3_cpu_pbus_AXI_RRW-1:0])); // Templated

    /* axi2apb_cpu_cpupbus_DW_axi_x2p AUTO_TEMPLATE (
        .aclk                   (CLK__CBUS),
        .aresetn                (RSTN__CBUS),
        .\([a|r|w|b]\)\(.*\)    (AXI4__CBUS__LOCAL__@"(upcase \\"\1\\")"@"(upcase \\"\2\\")"[]),
        .pready_s[1|2|3|4|5]    (1'b1),		
		.pslverr_s.		        (1'b0),
        .p\(.*\)_s0             (APB3__CBUS__SCU__P@"(upcase \\"\1\\")"[]),
        .p\(.*\)_s1             (APB3__CBUS__S1__P@"(upcase \\"\1\\")"[]),
        .p\(.*\)_s2             (APB3__CBUS__S2__P@"(upcase \\"\1\\")"[]),
        .p\(.*\)_s3             (APB3__CBUS__S3__P@"(upcase \\"\1\\")"[]),
        .p\(.*\)_s4             (APB3__CBUS__S4__P@"(upcase \\"\1\\")"[]),
        .p\(.*\)_s5             (APB3__CBUS__S5__P@"(upcase \\"\1\\")"[]),
        .p\(.*\)                (APB3__CBUS__P@"(upcase \\"\1\\")"[]),
        .csysreq                (1'b0),
        .csysack                (),
        .cactive                (),
    );*/
    axi2apb_cpu_cpupbus_DW_axi_x2p AXI2APB_CBUS_0 (/*AUTOINST*/
						   // Outputs
						   .arready		(AXI4__CBUS__LOCAL__ARREADY), // Templated
						   .awready		(AXI4__CBUS__LOCAL__AWREADY), // Templated
						   .bid			(AXI4__CBUS__LOCAL__BID[`axi2apb_cpu_cpupbus_X2P_AXI_SIDW-1:0]), // Templated
						   .bresp		(AXI4__CBUS__LOCAL__BRESP[1:0]), // Templated
						   .bvalid		(AXI4__CBUS__LOCAL__BVALID), // Templated
						   .rdata		(AXI4__CBUS__LOCAL__RDATA[`axi2apb_cpu_cpupbus_X2P_AXI_DW-1:0]), // Templated
						   .rid			(AXI4__CBUS__LOCAL__RID[`axi2apb_cpu_cpupbus_X2P_AXI_SIDW-1:0]), // Templated
						   .rlast		(AXI4__CBUS__LOCAL__RLAST), // Templated
						   .rresp		(AXI4__CBUS__LOCAL__RRESP[1:0]), // Templated
						   .rvalid		(AXI4__CBUS__LOCAL__RVALID), // Templated
						   .wready		(AXI4__CBUS__LOCAL__WREADY), // Templated
						   .paddr		(APB3__CBUS__PADDR[`axi2apb_cpu_cpupbus_X2P_APB_ADDR_WIDTH-1:0]), // Templated
						   .penable		(APB3__CBUS__PENABLE), // Templated
						   .psel_s0		(APB3__CBUS__SCU__PSEL), // Templated
						   .psel_s1		(APB3__CBUS__S1__PSEL), // Templated
						   .psel_s2		(APB3__CBUS__S2__PSEL), // Templated
						   .psel_s3		(APB3__CBUS__S3__PSEL), // Templated
						   .psel_s4		(APB3__CBUS__S4__PSEL), // Templated
						   .psel_s5		(APB3__CBUS__S5__PSEL), // Templated
						   .pwdata		(APB3__CBUS__PWDATA[`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0]), // Templated
						   .pwrite		(APB3__CBUS__PWRITE), // Templated
						   // Inputs
						   .aclk		(CLK__CBUS),	 // Templated
						   .aresetn		(RSTN__CBUS),	 // Templated
						   .araddr		(AXI4__CBUS__LOCAL__ARADDR[`axi2apb_cpu_cpupbus_X2P_AXI_AW-1:0]), // Templated
						   .awaddr		(AXI4__CBUS__LOCAL__AWADDR[`axi2apb_cpu_cpupbus_X2P_AXI_AW-1:0]), // Templated
						   .arburst		(AXI4__CBUS__LOCAL__ARBURST[1:0]), // Templated
						   .arid		(AXI4__CBUS__LOCAL__ARID[`axi2apb_cpu_cpupbus_X2P_AXI_SIDW-1:0]), // Templated
						   .arlen		(AXI4__CBUS__LOCAL__ARLEN[`axi2apb_cpu_cpupbus_LEN_WIDTH-1:0]), // Templated
						   .arsize		(AXI4__CBUS__LOCAL__ARSIZE[2:0]), // Templated
						   .arvalid		(AXI4__CBUS__LOCAL__ARVALID), // Templated
						   .awburst		(AXI4__CBUS__LOCAL__AWBURST[1:0]), // Templated
						   .awid		(AXI4__CBUS__LOCAL__AWID[`axi2apb_cpu_cpupbus_X2P_AXI_SIDW-1:0]), // Templated
						   .awlen		(AXI4__CBUS__LOCAL__AWLEN[`axi2apb_cpu_cpupbus_LEN_WIDTH-1:0]), // Templated
						   .arcache		(AXI4__CBUS__LOCAL__ARCACHE[3:0]), // Templated
						   .arprot		(AXI4__CBUS__LOCAL__ARPROT[2:0]), // Templated
						   .arlock		(AXI4__CBUS__LOCAL__ARLOCK[`axi2apb_cpu_cpupbus_X2P_AXI_LTW-1:0]), // Templated
						   .awcache		(AXI4__CBUS__LOCAL__AWCACHE[3:0]), // Templated
						   .awprot		(AXI4__CBUS__LOCAL__AWPROT[2:0]), // Templated
						   .awlock		(AXI4__CBUS__LOCAL__AWLOCK[`axi2apb_cpu_cpupbus_X2P_AXI_LTW-1:0]), // Templated
						   .awsize		(AXI4__CBUS__LOCAL__AWSIZE[2:0]), // Templated
						   .awvalid		(AXI4__CBUS__LOCAL__AWVALID), // Templated
						   .bready		(AXI4__CBUS__LOCAL__BREADY), // Templated
						   .rready		(AXI4__CBUS__LOCAL__RREADY), // Templated
						   .wdata		(AXI4__CBUS__LOCAL__WDATA[`axi2apb_cpu_cpupbus_X2P_AXI_DW-1:0]), // Templated
						   .wlast		(AXI4__CBUS__LOCAL__WLAST), // Templated
						   .wstrb		(AXI4__CBUS__LOCAL__WSTRB[`axi2apb_cpu_cpupbus_X2P_AXI_WSTRB_WIDTH-1:0]), // Templated
						   .wvalid		(AXI4__CBUS__LOCAL__WVALID), // Templated
						   .prdata_s0		(APB3__CBUS__SCU__PRDATA[`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0]), // Templated
						   .prdata_s1		(APB3__CBUS__S1__PRDATA[`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0]), // Templated
						   .prdata_s2		(APB3__CBUS__S2__PRDATA[`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0]), // Templated
						   .prdata_s3		(APB3__CBUS__S3__PRDATA[`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0]), // Templated
						   .prdata_s4		(APB3__CBUS__S4__PRDATA[`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0]), // Templated
						   .prdata_s5		(APB3__CBUS__S5__PRDATA[`axi2apb_cpu_cpupbus_X2P_APB_DATA_WIDTH-1:0]), // Templated
						   .pready_s0		(APB3__CBUS__SCU__PREADY), // Templated
						   .pready_s1		(1'b1),		 // Templated
						   .pready_s2		(1'b1),		 // Templated
						   .pready_s3		(1'b1),		 // Templated
						   .pready_s4		(1'b1),		 // Templated
						   .pready_s5		(1'b1),		 // Templated
						   .pslverr_s0		(1'b0),		 // Templated
						   .pslverr_s1		(1'b0),		 // Templated
						   .pslverr_s2		(1'b0),		 // Templated
						   .pslverr_s3		(1'b0),		 // Templated
						   .pslverr_s4		(1'b0),		 // Templated
						   .pslverr_s5		(1'b0));		 // Templated
    assign APB3__CBUS__SCU__PADDR   = APB3__CBUS__PADDR;
    assign APB3__CBUS__SCU__PWDATA  = APB3__CBUS__PWDATA;
    assign APB3__CBUS__SCU__PWRITE  = APB3__CBUS__PWRITE;
    assign APB3__CBUS__SCU__PENABLE = APB3__CBUS__PENABLE;

    /* axi4to3 AUTO_TEMPLATE (
        .CLK__BUS           (CLK__CBUS),
        .RSTN__BUS          (RSTN__CBUS),
		.AXI4__BUS__.*USER	(),
        .AXI4__BUS__\(.*\)  (AXI4__CBUS__SOC__\1[]),
		.AXI3__BUS__.*USER	(),
        .AXI3__BUS__\(.*\)  (AXI3__CBUS__SOC__\1[]),
    );*/
    axi4to3
        #(.ADDR_WIDTH(PERI_AW),
          .ID_WIDTH(PERI_IW),
          .DATA_WIDTH(PERI_DW),
          .QUEUE_DEPTH(8),
          .USER_WIDTH(PERI_UW),
          .LEN_WIDTH(8),
          .STRB_WIDTH(PERI_SW)) 
        AXI4TO3_CBUS_0 (/*AUTOINST*/
			// Outputs
			.AXI4__BUS__AWREADY(AXI4__CBUS__SOC__AWREADY), // Templated
			.AXI4__BUS__WREADY(AXI4__CBUS__SOC__WREADY), // Templated
			.AXI4__BUS__BID	(AXI4__CBUS__SOC__BID[PERI_IW-1:0]), // Templated
			.AXI4__BUS__BVALID(AXI4__CBUS__SOC__BVALID), // Templated
			.AXI4__BUS__BRESP(AXI4__CBUS__SOC__BRESP[1:0]), // Templated
			.AXI4__BUS__ARREADY(AXI4__CBUS__SOC__ARREADY), // Templated
			.AXI4__BUS__RID	(AXI4__CBUS__SOC__RID[PERI_IW-1:0]), // Templated
			.AXI4__BUS__RDATA(AXI4__CBUS__SOC__RDATA[PERI_DW-1:0]), // Templated
			.AXI4__BUS__RRESP(AXI4__CBUS__SOC__RRESP[1:0]), // Templated
			.AXI4__BUS__RLAST(AXI4__CBUS__SOC__RLAST), // Templated
			.AXI4__BUS__RVALID(AXI4__CBUS__SOC__RVALID), // Templated
			.AXI3__BUS__AWADDR(AXI3__CBUS__SOC__AWADDR[PERI_AW-1:0]), // Templated
			.AXI3__BUS__AWID(AXI3__CBUS__SOC__AWID[PERI_IW-1:0]), // Templated
			.AXI3__BUS__AWLEN(AXI3__CBUS__SOC__AWLEN[7:0]), // Templated
			.AXI3__BUS__AWSIZE(AXI3__CBUS__SOC__AWSIZE[2:0]), // Templated
			.AXI3__BUS__AWBURST(AXI3__CBUS__SOC__AWBURST[1:0]), // Templated
			.AXI3__BUS__AWLOCK(AXI3__CBUS__SOC__AWLOCK[1:0]), // Templated
			.AXI3__BUS__AWCACHE(AXI3__CBUS__SOC__AWCACHE[3:0]), // Templated
			.AXI3__BUS__AWPROT(AXI3__CBUS__SOC__AWPROT[2:0]), // Templated
			.AXI3__BUS__AWUSER(),			 // Templated
			.AXI3__BUS__AWVALID(AXI3__CBUS__SOC__AWVALID), // Templated
			.AXI3__BUS__WID	(AXI3__CBUS__SOC__WID[PERI_IW-1:0]), // Templated
			.AXI3__BUS__WDATA(AXI3__CBUS__SOC__WDATA[PERI_DW-1:0]), // Templated
			.AXI3__BUS__WSTRB(AXI3__CBUS__SOC__WSTRB[PERI_SW-1:0]), // Templated
			.AXI3__BUS__WLAST(AXI3__CBUS__SOC__WLAST), // Templated
			.AXI3__BUS__WVALID(AXI3__CBUS__SOC__WVALID), // Templated
			.AXI3__BUS__BREADY(AXI3__CBUS__SOC__BREADY), // Templated
			.AXI3__BUS__ARID(AXI3__CBUS__SOC__ARID[PERI_IW-1:0]), // Templated
			.AXI3__BUS__ARADDR(AXI3__CBUS__SOC__ARADDR[PERI_AW-1:0]), // Templated
			.AXI3__BUS__ARLEN(AXI3__CBUS__SOC__ARLEN[7:0]), // Templated
			.AXI3__BUS__ARSIZE(AXI3__CBUS__SOC__ARSIZE[2:0]), // Templated
			.AXI3__BUS__ARBURST(AXI3__CBUS__SOC__ARBURST[1:0]), // Templated
			.AXI3__BUS__ARLOCK(AXI3__CBUS__SOC__ARLOCK[1:0]), // Templated
			.AXI3__BUS__ARCACHE(AXI3__CBUS__SOC__ARCACHE[3:0]), // Templated
			.AXI3__BUS__ARPROT(AXI3__CBUS__SOC__ARPROT[2:0]), // Templated
			.AXI3__BUS__ARUSER(),			 // Templated
			.AXI3__BUS__ARVALID(AXI3__CBUS__SOC__ARVALID), // Templated
			.AXI3__BUS__RREADY(AXI3__CBUS__SOC__RREADY), // Templated
			// Inputs
			.CLK__BUS	(CLK__CBUS),		 // Templated
			.RSTN__BUS	(RSTN__CBUS),		 // Templated
			.AXI4__BUS__AWADDR(AXI4__CBUS__SOC__AWADDR[PERI_AW-1:0]), // Templated
			.AXI4__BUS__AWID(AXI4__CBUS__SOC__AWID[PERI_IW-1:0]), // Templated
			.AXI4__BUS__AWLEN(AXI4__CBUS__SOC__AWLEN[7:0]), // Templated
			.AXI4__BUS__AWSIZE(AXI4__CBUS__SOC__AWSIZE[2:0]), // Templated
			.AXI4__BUS__AWBURST(AXI4__CBUS__SOC__AWBURST[1:0]), // Templated
			.AXI4__BUS__AWLOCK(AXI4__CBUS__SOC__AWLOCK), // Templated
			.AXI4__BUS__AWCACHE(AXI4__CBUS__SOC__AWCACHE[3:0]), // Templated
			.AXI4__BUS__AWPROT(AXI4__CBUS__SOC__AWPROT[2:0]), // Templated
			.AXI4__BUS__AWUSER(),			 // Templated
			.AXI4__BUS__AWVALID(AXI4__CBUS__SOC__AWVALID), // Templated
			.AXI4__BUS__WDATA(AXI4__CBUS__SOC__WDATA[PERI_DW-1:0]), // Templated
			.AXI4__BUS__WSTRB(AXI4__CBUS__SOC__WSTRB[PERI_SW-1:0]), // Templated
			.AXI4__BUS__WVALID(AXI4__CBUS__SOC__WVALID), // Templated
			.AXI4__BUS__WLAST(AXI4__CBUS__SOC__WLAST), // Templated
			.AXI4__BUS__BREADY(AXI4__CBUS__SOC__BREADY), // Templated
			.AXI4__BUS__ARID(AXI4__CBUS__SOC__ARID[PERI_IW-1:0]), // Templated
			.AXI4__BUS__ARADDR(AXI4__CBUS__SOC__ARADDR[PERI_AW-1:0]), // Templated
			.AXI4__BUS__ARLEN(AXI4__CBUS__SOC__ARLEN[7:0]), // Templated
			.AXI4__BUS__ARSIZE(AXI4__CBUS__SOC__ARSIZE[2:0]), // Templated
			.AXI4__BUS__ARBURST(AXI4__CBUS__SOC__ARBURST[1:0]), // Templated
			.AXI4__BUS__ARLOCK(AXI4__CBUS__SOC__ARLOCK), // Templated
			.AXI4__BUS__ARCACHE(AXI4__CBUS__SOC__ARCACHE[3:0]), // Templated
			.AXI4__BUS__ARPROT(AXI4__CBUS__SOC__ARPROT[2:0]), // Templated
			.AXI4__BUS__ARUSER(),			 // Templated
			.AXI4__BUS__ARVALID(AXI4__CBUS__SOC__ARVALID), // Templated
			.AXI4__BUS__RREADY(AXI4__CBUS__SOC__RREADY), // Templated
			.AXI3__BUS__AWREADY(AXI3__CBUS__SOC__AWREADY), // Templated
			.AXI3__BUS__WREADY(AXI3__CBUS__SOC__WREADY), // Templated
			.AXI3__BUS__BID	(AXI3__CBUS__SOC__BID[PERI_IW-1:0]), // Templated
			.AXI3__BUS__BRESP(AXI3__CBUS__SOC__BRESP[1:0]), // Templated
			.AXI3__BUS__BVALID(AXI3__CBUS__SOC__BVALID), // Templated
			.AXI3__BUS__ARREADY(AXI3__CBUS__SOC__ARREADY), // Templated
			.AXI3__BUS__RID	(AXI3__CBUS__SOC__RID[PERI_IW-1:0]), // Templated
			.AXI3__BUS__RDATA(AXI3__CBUS__SOC__RDATA[PERI_DW-1:0]), // Templated
			.AXI3__BUS__RRESP(AXI3__CBUS__SOC__RRESP[1:0]), // Templated
			.AXI3__BUS__RLAST(AXI3__CBUS__SOC__RLAST), // Templated
			.AXI3__BUS__RVALID(AXI3__CBUS__SOC__RVALID)); // Templated

    /* axi2axi_cpu_pbus_DW_axi_x2x AUTO_TEMPLATE (
        .aclk_m             (CLK__CBUS),
        .aresetn_m          (RSTN__CBUS),
        .\(.*\)_m           (AXI3__CBUS__SOC__@"(upcase \\"\1\\")"[]),
        .aclk_s             (CLK__TX),
        .aresetn_s          (RSTN__TX),
        .\(.*\)_s           (AXI3__TX__@"(upcase \\"\1\\")"[]),
        .\(.*\)_s1          (AXI3__TX__@"(upcase \\"\1\\")"[]),
        .csysreq            (1'b0),
        .csysack            (),
        .cactive            (),
    );*/
    axi2axi_cpu_pbus_DW_axi_x2x AXI64TO32_CBUS_0 (/*AUTOINST*/
						  // Outputs
						  .awready_m		(AXI3__CBUS__SOC__AWREADY), // Templated
						  .wready_m		(AXI3__CBUS__SOC__WREADY), // Templated
						  .bvalid_m		(AXI3__CBUS__SOC__BVALID), // Templated
						  .bid_m		(AXI3__CBUS__SOC__BID[`axi2axi_cpu_pbus_X2X_MP_IDW-1:0]), // Templated
						  .bresp_m		(AXI3__CBUS__SOC__BRESP[`axi2axi_cpu_pbus_X2X_BRW-1:0]), // Templated
						  .arready_m		(AXI3__CBUS__SOC__ARREADY), // Templated
						  .rvalid_m		(AXI3__CBUS__SOC__RVALID), // Templated
						  .rid_m		(AXI3__CBUS__SOC__RID[`axi2axi_cpu_pbus_X2X_MP_IDW-1:0]), // Templated
						  .rdata_m		(AXI3__CBUS__SOC__RDATA[`axi2axi_cpu_pbus_X2X_MP_DW-1:0]), // Templated
						  .rlast_m		(AXI3__CBUS__SOC__RLAST), // Templated
						  .rresp_m		(AXI3__CBUS__SOC__RRESP[`axi2axi_cpu_pbus_X2X_RRW-1:0]), // Templated
						  .awvalid_s1		(AXI3__TX__AWVALID), // Templated
						  .awaddr_s1		(AXI3__TX__AWADDR[`axi2axi_cpu_pbus_X2X_SP_AW-1:0]), // Templated
						  .awid_s1		(AXI3__TX__AWID[`axi2axi_cpu_pbus_X2X_SP_IDW-1:0]), // Templated
						  .awlen_s1		(AXI3__TX__AWLEN[`axi2axi_cpu_pbus_X2X_SP_BLW-1:0]), // Templated
						  .awsize_s1		(AXI3__TX__AWSIZE[`axi2axi_cpu_pbus_X2X_BSW-1:0]), // Templated
						  .awburst_s1		(AXI3__TX__AWBURST[`axi2axi_cpu_pbus_X2X_BTW-1:0]), // Templated
						  .awlock_s1		(AXI3__TX__AWLOCK[`axi2axi_cpu_pbus_X2X_LTW-1:0]), // Templated
						  .awcache_s1		(AXI3__TX__AWCACHE[`axi2axi_cpu_pbus_X2X_CTW-1:0]), // Templated
						  .awprot_s1		(AXI3__TX__AWPROT[`axi2axi_cpu_pbus_X2X_PTW-1:0]), // Templated
						  .wvalid_s1		(AXI3__TX__WVALID), // Templated
						  .wid_s1		(AXI3__TX__WID[`axi2axi_cpu_pbus_X2X_SP_IDW-1:0]), // Templated
						  .wdata_s1		(AXI3__TX__WDATA[`axi2axi_cpu_pbus_X2X_SP_DW-1:0]), // Templated
						  .wstrb_s1		(AXI3__TX__WSTRB[`axi2axi_cpu_pbus_X2X_SP_SW-1:0]), // Templated
						  .wlast_s1		(AXI3__TX__WLAST), // Templated
						  .bready_s1		(AXI3__TX__BREADY), // Templated
						  .arvalid_s		(AXI3__TX__ARVALID), // Templated
						  .arid_s		(AXI3__TX__ARID[`axi2axi_cpu_pbus_X2X_SP_IDW-1:0]), // Templated
						  .araddr_s		(AXI3__TX__ARADDR[`axi2axi_cpu_pbus_X2X_SP_AW-1:0]), // Templated
						  .arlen_s		(AXI3__TX__ARLEN[`axi2axi_cpu_pbus_X2X_SP_BLW-1:0]), // Templated
						  .arsize_s		(AXI3__TX__ARSIZE[`axi2axi_cpu_pbus_X2X_BSW-1:0]), // Templated
						  .arburst_s		(AXI3__TX__ARBURST[`axi2axi_cpu_pbus_X2X_BTW-1:0]), // Templated
						  .arlock_s		(AXI3__TX__ARLOCK[`axi2axi_cpu_pbus_X2X_LTW-1:0]), // Templated
						  .arcache_s		(AXI3__TX__ARCACHE[`axi2axi_cpu_pbus_X2X_CTW-1:0]), // Templated
						  .arprot_s		(AXI3__TX__ARPROT[`axi2axi_cpu_pbus_X2X_PTW-1:0]), // Templated
						  .rready_s		(AXI3__TX__RREADY), // Templated
						  // Inputs
						  .aclk_m		(CLK__CBUS),	 // Templated
						  .aresetn_m		(RSTN__CBUS),	 // Templated
						  .awvalid_m		(AXI3__CBUS__SOC__AWVALID), // Templated
						  .awaddr_m		(AXI3__CBUS__SOC__AWADDR[`axi2axi_cpu_pbus_X2X_MP_AW-1:0]), // Templated
						  .awid_m		(AXI3__CBUS__SOC__AWID[`axi2axi_cpu_pbus_X2X_MP_IDW-1:0]), // Templated
						  .awlen_m		(AXI3__CBUS__SOC__AWLEN[`axi2axi_cpu_pbus_X2X_MP_BLW-1:0]), // Templated
						  .awsize_m		(AXI3__CBUS__SOC__AWSIZE[`axi2axi_cpu_pbus_X2X_BSW-1:0]), // Templated
						  .awburst_m		(AXI3__CBUS__SOC__AWBURST[`axi2axi_cpu_pbus_X2X_BTW-1:0]), // Templated
						  .awlock_m		(AXI3__CBUS__SOC__AWLOCK[`axi2axi_cpu_pbus_X2X_LTW-1:0]), // Templated
						  .awcache_m		(AXI3__CBUS__SOC__AWCACHE[`axi2axi_cpu_pbus_X2X_CTW-1:0]), // Templated
						  .awprot_m		(AXI3__CBUS__SOC__AWPROT[`axi2axi_cpu_pbus_X2X_PTW-1:0]), // Templated
						  .wvalid_m		(AXI3__CBUS__SOC__WVALID), // Templated
						  .wid_m		(AXI3__CBUS__SOC__WID[`axi2axi_cpu_pbus_X2X_MP_IDW-1:0]), // Templated
						  .wdata_m		(AXI3__CBUS__SOC__WDATA[`axi2axi_cpu_pbus_X2X_MP_DW-1:0]), // Templated
						  .wstrb_m		(AXI3__CBUS__SOC__WSTRB[`axi2axi_cpu_pbus_X2X_MP_SW-1:0]), // Templated
						  .wlast_m		(AXI3__CBUS__SOC__WLAST), // Templated
						  .bready_m		(AXI3__CBUS__SOC__BREADY), // Templated
						  .arvalid_m		(AXI3__CBUS__SOC__ARVALID), // Templated
						  .arid_m		(AXI3__CBUS__SOC__ARID[`axi2axi_cpu_pbus_X2X_MP_IDW-1:0]), // Templated
						  .araddr_m		(AXI3__CBUS__SOC__ARADDR[`axi2axi_cpu_pbus_X2X_MP_AW-1:0]), // Templated
						  .arlen_m		(AXI3__CBUS__SOC__ARLEN[`axi2axi_cpu_pbus_X2X_MP_BLW-1:0]), // Templated
						  .arsize_m		(AXI3__CBUS__SOC__ARSIZE[`axi2axi_cpu_pbus_X2X_BSW-1:0]), // Templated
						  .arburst_m		(AXI3__CBUS__SOC__ARBURST[`axi2axi_cpu_pbus_X2X_BTW-1:0]), // Templated
						  .arlock_m		(AXI3__CBUS__SOC__ARLOCK[`axi2axi_cpu_pbus_X2X_LTW-1:0]), // Templated
						  .arcache_m		(AXI3__CBUS__SOC__ARCACHE[`axi2axi_cpu_pbus_X2X_CTW-1:0]), // Templated
						  .arprot_m		(AXI3__CBUS__SOC__ARPROT[`axi2axi_cpu_pbus_X2X_PTW-1:0]), // Templated
						  .rready_m		(AXI3__CBUS__SOC__RREADY), // Templated
						  .aclk_s		(CLK__TX),	 // Templated
						  .aresetn_s		(RSTN__TX),	 // Templated
						  .awready_s1		(AXI3__TX__AWREADY), // Templated
						  .wready_s1		(AXI3__TX__WREADY), // Templated
						  .bvalid_s1		(AXI3__TX__BVALID), // Templated
						  .bid_s1		(AXI3__TX__BID[`axi2axi_cpu_pbus_X2X_SP_IDW-1:0]), // Templated
						  .bresp_s1		(AXI3__TX__BRESP[`axi2axi_cpu_pbus_X2X_BRW-1:0]), // Templated
						  .arready_s		(AXI3__TX__ARREADY), // Templated
						  .rvalid_s		(AXI3__TX__RVALID), // Templated
						  .rid_s		(AXI3__TX__RID[`axi2axi_cpu_pbus_X2X_SP_IDW-1:0]), // Templated
						  .rdata_s		(AXI3__TX__RDATA[`axi2axi_cpu_pbus_X2X_SP_DW-1:0]), // Templated
						  .rlast_s		(AXI3__TX__RLAST), // Templated
						  .rresp_s		(AXI3__TX__RRESP[`axi2axi_cpu_pbus_X2X_RRW-1:0])); // Templated

    /* axi3to4 AUTO_TEMPLATE (
        .CLK__BUS                  (CLK__TX),
        .RSTN__BUS                 (RSTN__TX),
        .AXI3__BUS__A.USER	       (),
        .AXI3__BUS__\(.*\)         (AXI3__TX__\1[]),
        .AXI4__BUS__A.USER	       (),
        .AXI4__BUS__\(.*\)         (AXI4__TX__\1[]),
    );*/
    axi3to4 #(.ADDR_WIDTH(CBUS_AW),
              .ID_WIDTH(CBUS_IW),
              .DATA_WIDTH(CBUS_DW),
              .USER_WIDTH(CBUS_UW),
              .LEN_WIDTH(8),
              .STRB_WIDTH(CBUS_SW)) AXI3TO4_CBUS_0 (/*AUTOINST*/
						    // Outputs
						    .AXI3__BUS__AWREADY	(AXI3__TX__AWREADY), // Templated
						    .AXI3__BUS__WREADY	(AXI3__TX__WREADY), // Templated
						    .AXI3__BUS__BID	(AXI3__TX__BID[CBUS_IW-1:0]), // Templated
						    .AXI3__BUS__BVALID	(AXI3__TX__BVALID), // Templated
						    .AXI3__BUS__BRESP	(AXI3__TX__BRESP[1:0]), // Templated
						    .AXI3__BUS__ARREADY	(AXI3__TX__ARREADY), // Templated
						    .AXI3__BUS__RID	(AXI3__TX__RID[CBUS_IW-1:0]), // Templated
						    .AXI3__BUS__RDATA	(AXI3__TX__RDATA[CBUS_DW-1:0]), // Templated
						    .AXI3__BUS__RRESP	(AXI3__TX__RRESP[1:0]), // Templated
						    .AXI3__BUS__RLAST	(AXI3__TX__RLAST), // Templated
						    .AXI3__BUS__RVALID	(AXI3__TX__RVALID), // Templated
						    .AXI4__BUS__AWADDR	(AXI4__TX__AWADDR[CBUS_AW-1:0]), // Templated
						    .AXI4__BUS__AWID	(AXI4__TX__AWID[CBUS_IW-1:0]), // Templated
						    .AXI4__BUS__AWLEN	(AXI4__TX__AWLEN[7:0]), // Templated
						    .AXI4__BUS__AWSIZE	(AXI4__TX__AWSIZE[2:0]), // Templated
						    .AXI4__BUS__AWBURST	(AXI4__TX__AWBURST[1:0]), // Templated
						    .AXI4__BUS__AWLOCK	(AXI4__TX__AWLOCK), // Templated
						    .AXI4__BUS__AWCACHE	(AXI4__TX__AWCACHE[3:0]), // Templated
						    .AXI4__BUS__AWPROT	(AXI4__TX__AWPROT[2:0]), // Templated
						    .AXI4__BUS__AWUSER	(),		 // Templated
						    .AXI4__BUS__AWVALID	(AXI4__TX__AWVALID), // Templated
						    .AXI4__BUS__WDATA	(AXI4__TX__WDATA[CBUS_DW-1:0]), // Templated
						    .AXI4__BUS__WSTRB	(AXI4__TX__WSTRB[CBUS_SW-1:0]), // Templated
						    .AXI4__BUS__WLAST	(AXI4__TX__WLAST), // Templated
						    .AXI4__BUS__WVALID	(AXI4__TX__WVALID), // Templated
						    .AXI4__BUS__BREADY	(AXI4__TX__BREADY), // Templated
						    .AXI4__BUS__ARID	(AXI4__TX__ARID[CBUS_IW-1:0]), // Templated
						    .AXI4__BUS__ARADDR	(AXI4__TX__ARADDR[CBUS_AW-1:0]), // Templated
						    .AXI4__BUS__ARLEN	(AXI4__TX__ARLEN[7:0]), // Templated
						    .AXI4__BUS__ARSIZE	(AXI4__TX__ARSIZE[2:0]), // Templated
						    .AXI4__BUS__ARBURST	(AXI4__TX__ARBURST[1:0]), // Templated
						    .AXI4__BUS__ARLOCK	(AXI4__TX__ARLOCK), // Templated
						    .AXI4__BUS__ARCACHE	(AXI4__TX__ARCACHE[3:0]), // Templated
						    .AXI4__BUS__ARPROT	(AXI4__TX__ARPROT[2:0]), // Templated
						    .AXI4__BUS__ARUSER	(),		 // Templated
						    .AXI4__BUS__ARVALID	(AXI4__TX__ARVALID), // Templated
						    .AXI4__BUS__RREADY	(AXI4__TX__RREADY), // Templated
						    // Inputs
						    .CLK__BUS		(CLK__TX),	 // Templated
						    .RSTN__BUS		(RSTN__TX),	 // Templated
						    .AXI3__BUS__AWADDR	(AXI3__TX__AWADDR[CBUS_AW-1:0]), // Templated
						    .AXI3__BUS__AWID	(AXI3__TX__AWID[CBUS_IW-1:0]), // Templated
						    .AXI3__BUS__AWLEN	(AXI3__TX__AWLEN[7:0]), // Templated
						    .AXI3__BUS__AWSIZE	(AXI3__TX__AWSIZE[2:0]), // Templated
						    .AXI3__BUS__AWBURST	(AXI3__TX__AWBURST[1:0]), // Templated
						    .AXI3__BUS__AWLOCK	(AXI3__TX__AWLOCK[1:0]), // Templated
						    .AXI3__BUS__AWCACHE	(AXI3__TX__AWCACHE[3:0]), // Templated
						    .AXI3__BUS__AWPROT	(AXI3__TX__AWPROT[2:0]), // Templated
						    .AXI3__BUS__AWUSER	(),		 // Templated
						    .AXI3__BUS__AWVALID	(AXI3__TX__AWVALID), // Templated
						    .AXI3__BUS__WID	(AXI3__TX__WID[CBUS_IW-1:0]), // Templated
						    .AXI3__BUS__WDATA	(AXI3__TX__WDATA[CBUS_DW-1:0]), // Templated
						    .AXI3__BUS__WSTRB	(AXI3__TX__WSTRB[CBUS_SW-1:0]), // Templated
						    .AXI3__BUS__WVALID	(AXI3__TX__WVALID), // Templated
						    .AXI3__BUS__WLAST	(AXI3__TX__WLAST), // Templated
						    .AXI3__BUS__BREADY	(AXI3__TX__BREADY), // Templated
						    .AXI3__BUS__ARID	(AXI3__TX__ARID[CBUS_IW-1:0]), // Templated
						    .AXI3__BUS__ARADDR	(AXI3__TX__ARADDR[CBUS_AW-1:0]), // Templated
						    .AXI3__BUS__ARLEN	(AXI3__TX__ARLEN[7:0]), // Templated
						    .AXI3__BUS__ARSIZE	(AXI3__TX__ARSIZE[2:0]), // Templated
						    .AXI3__BUS__ARBURST	(AXI3__TX__ARBURST[1:0]), // Templated
						    .AXI3__BUS__ARLOCK	(AXI3__TX__ARLOCK[1:0]), // Templated
						    .AXI3__BUS__ARCACHE	(AXI3__TX__ARCACHE[3:0]), // Templated
						    .AXI3__BUS__ARPROT	(AXI3__TX__ARPROT[2:0]), // Templated
						    .AXI3__BUS__ARUSER	(),		 // Templated
						    .AXI3__BUS__ARVALID	(AXI3__TX__ARVALID), // Templated
						    .AXI3__BUS__RREADY	(AXI3__TX__RREADY), // Templated
						    .AXI4__BUS__AWREADY	(AXI4__TX__AWREADY), // Templated
						    .AXI4__BUS__WREADY	(AXI4__TX__WREADY), // Templated
						    .AXI4__BUS__BID	(AXI4__TX__BID[CBUS_IW-1:0]), // Templated
						    .AXI4__BUS__BRESP	(AXI4__TX__BRESP[1:0]), // Templated
						    .AXI4__BUS__BVALID	(AXI4__TX__BVALID), // Templated
						    .AXI4__BUS__ARREADY	(AXI4__TX__ARREADY), // Templated
						    .AXI4__BUS__RID	(AXI4__TX__RID[CBUS_IW-1:0]), // Templated
						    .AXI4__BUS__RDATA	(AXI4__TX__RDATA[CBUS_DW-1:0]), // Templated
						    .AXI4__BUS__RRESP	(AXI4__TX__RRESP[1:0]), // Templated
						    .AXI4__BUS__RLAST	(AXI4__TX__RLAST), // Templated
						    .AXI4__BUS__RVALID	(AXI4__TX__RVALID)); // Templated


`include "./axi_1x3_cpu_pbus/axi_1x3_cpu_pbus_DW_axi-undef.v"
`include "./axi2axi_cpu_pbus/axi2axi_cpu_pbus_DW_axi_x2x-undef.v"
`include "./axi2apb_cpu_cpupbus/axi2apb_cpu_cpupbus_DW_axi_x2p-undef.v"

endmodule
// Local Variables:
// verilog-library-directories:(".")
// verilog-auto-inst-param-value:t
// verilog-library-files:("../../../Shared/rtl.src/bus_components/axi_converter/axi3to4.v" "../../../Shared/rtl.src/bus_components/axi_converter/axi4to3.v" "./axi_1x3_cpu_pbus/axi_1x3_cpu_pbus_DW_axi.v" "./axi2axi_cpu_pbus/axi2axi_cpu_pbus_DW_axi_x2x.v" "./axi2apb_cpu_cpupbus/axi2apb_cpu_cpupbus_DW_axi_x2p.v")
// End:
// vim:tabstop=4:shiftwidth=4
