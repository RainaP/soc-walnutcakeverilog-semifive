//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_PTW_assert(
  input        reset,
  input        io_mem_s2_nack,
  input        clock_gate_0_out,
  input  [2:0] state,
  input        mem_resp_valid,
  input        _T_116,
  input        _T_1143
);
  wire  _T_1158; // @[PTW.scala 345:37]
  wire  _T_1159; // @[PTW.scala 345:28]
  wire  _T_1161; // @[PTW.scala 345:11]
  wire  _T_1162; // @[PTW.scala 345:11]
  wire  _T_1165; // @[PTW.scala 352:18]
  wire  _T_1167; // @[PTW.scala 352:11]
  wire  _T_1168; // @[PTW.scala 352:11]
  wire  _T_1181; // @[PTW.scala 369:18]
  wire  _T_1183; // @[PTW.scala 369:11]
  wire  _T_1184; // @[PTW.scala 369:11]
  assign _T_1158 = state == 3'h2; // @[PTW.scala 345:37]
  assign _T_1159 = _T_116 | _T_1158; // @[PTW.scala 345:28]
  assign _T_1161 = _T_1159 | reset; // @[PTW.scala 345:11]
  assign _T_1162 = ~_T_1161; // @[PTW.scala 345:11]
  assign _T_1165 = state == 3'h5; // @[PTW.scala 352:18]
  assign _T_1167 = _T_1165 | reset; // @[PTW.scala 352:11]
  assign _T_1168 = ~_T_1167; // @[PTW.scala 352:11]
  assign _T_1181 = state == 3'h4; // @[PTW.scala 369:18]
  assign _T_1183 = _T_1181 | reset; // @[PTW.scala 369:11]
  assign _T_1184 = ~_T_1183; // @[PTW.scala 369:11]
  always @(posedge clock_gate_0_out) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1143 & _T_1162) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at PTW.scala:345 assert(state === s_req || state === s_wait1)\n"); // @[PTW.scala 345:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1143 & _T_1162) begin
          $fatal; // @[PTW.scala 345:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (mem_resp_valid & _T_1168) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at PTW.scala:352 assert(state === s_wait3)\n"); // @[PTW.scala 352:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (mem_resp_valid & _T_1168) begin
          $fatal; // @[PTW.scala 352:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_mem_s2_nack & _T_1184) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at PTW.scala:369 assert(state === s_wait2)\n"); // @[PTW.scala 369:11]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_mem_s2_nack & _T_1184) begin
          $fatal; // @[PTW.scala 369:11]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
