//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__PipelinedMultiplier(
  input         rf_reset,
  input         clock,
  input         reset,
  input         io_req_valid,
  input  [3:0]  io_req_bits_fn,
  input         io_req_bits_dw,
  input  [63:0] io_req_bits_in1,
  input  [63:0] io_req_bits_in2,
  output [63:0] io_resp_bits_data
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [63:0] _RAND_3;
  reg [63:0] _RAND_4;
  reg [63:0] _RAND_5;
`endif // RANDOMIZE_REG_INIT
  reg  inPipe_valid; // @[Valid.scala 117:22]
  reg [3:0] inPipe_bits_fn; // @[Reg.scala 15:16]
  reg  inPipe_bits_dw; // @[Reg.scala 15:16]
  reg [63:0] inPipe_bits_in1; // @[Reg.scala 15:16]
  reg [63:0] inPipe_bits_in2; // @[Reg.scala 15:16]
  wire [3:0] _T = inPipe_bits_fn & 4'h1; // @[Decode.scala 14:65]
  wire  _T_1 = _T == 4'h1; // @[Decode.scala 14:121]
  wire [3:0] _T_2 = inPipe_bits_fn & 4'h2; // @[Decode.scala 14:65]
  wire  _T_3 = _T_2 == 4'h2; // @[Decode.scala 14:121]
  wire  cmdHi = _T_1 | _T_3; // @[Decode.scala 15:30]
  wire  rhsSigned = _T_2 == 4'h0; // @[Decode.scala 14:121]
  wire  _T_9 = _T == 4'h0; // @[Decode.scala 14:121]
  wire  lhsSigned = rhsSigned | _T_9; // @[Decode.scala 15:30]
  wire  cmdHalf = ~inPipe_bits_dw; // @[Multiplier.scala 201:46]
  wire  lhs_hi = lhsSigned & inPipe_bits_in1[63]; // @[Multiplier.scala 203:27]
  wire [64:0] lhs = {lhs_hi,inPipe_bits_in1}; // @[Multiplier.scala 203:65]
  wire  rhs_hi = rhsSigned & inPipe_bits_in2[63]; // @[Multiplier.scala 204:27]
  wire [64:0] rhs = {rhs_hi,inPipe_bits_in2}; // @[Multiplier.scala 204:65]
  wire [129:0] prod = $signed(lhs) * $signed(rhs); // @[Multiplier.scala 205:18]
  wire [31:0] muxed_lo = prod[31:0]; // @[Multiplier.scala 206:67]
  wire [31:0] muxed_hi = muxed_lo[31] ? 32'hffffffff : 32'h0; // @[Bitwise.scala 72:12]
  wire [63:0] _muxed_T_3 = {muxed_hi,muxed_lo}; // @[Cat.scala 30:58]
  reg [63:0] io_resp_bits_data_b; // @[Reg.scala 15:16]
  assign io_resp_bits_data = io_resp_bits_data_b; // @[Valid.scala 112:21 Valid.scala 114:16]
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      inPipe_valid <= 1'h0;
    end else if (reset) begin
      inPipe_valid <= 1'h0;
    end else begin
      inPipe_valid <= io_req_valid;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      inPipe_bits_fn <= 4'h0;
    end else if (io_req_valid) begin
      inPipe_bits_fn <= io_req_bits_fn;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      inPipe_bits_dw <= 1'h0;
    end else if (io_req_valid) begin
      inPipe_bits_dw <= io_req_bits_dw;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      inPipe_bits_in1 <= 64'h0;
    end else if (io_req_valid) begin
      inPipe_bits_in1 <= io_req_bits_in1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      inPipe_bits_in2 <= 64'h0;
    end else if (io_req_valid) begin
      inPipe_bits_in2 <= io_req_bits_in2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      io_resp_bits_data_b <= 64'h0;
    end else if (inPipe_valid) begin
      if (cmdHi) begin
        io_resp_bits_data_b <= prod[127:64];
      end else if (cmdHalf) begin
        io_resp_bits_data_b <= _muxed_T_3;
      end else begin
        io_resp_bits_data_b <= prod[63:0];
      end
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  inPipe_valid = _RAND_0[0:0];
  _RAND_1 = {1{`RANDOM}};
  inPipe_bits_fn = _RAND_1[3:0];
  _RAND_2 = {1{`RANDOM}};
  inPipe_bits_dw = _RAND_2[0:0];
  _RAND_3 = {2{`RANDOM}};
  inPipe_bits_in1 = _RAND_3[63:0];
  _RAND_4 = {2{`RANDOM}};
  inPipe_bits_in2 = _RAND_4[63:0];
  _RAND_5 = {2{`RANDOM}};
  io_resp_bits_data_b = _RAND_5[63:0];
`endif // RANDOMIZE_REG_INIT
  if (rf_reset) begin
    inPipe_valid = 1'h0;
  end
  if (rf_reset) begin
    inPipe_bits_fn = 4'h0;
  end
  if (rf_reset) begin
    inPipe_bits_dw = 1'h0;
  end
  if (rf_reset) begin
    inPipe_bits_in1 = 64'h0;
  end
  if (rf_reset) begin
    inPipe_bits_in2 = 64'h0;
  end
  if (rf_reset) begin
    io_resp_bits_data_b = 64'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
