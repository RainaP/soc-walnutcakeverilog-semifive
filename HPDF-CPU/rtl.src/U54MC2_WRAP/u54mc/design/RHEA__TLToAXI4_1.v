//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TLToAXI4_1(
  input         rf_reset,
  input         clock,
  input         reset,
  output        auto_in_a_ready,
  input         auto_in_a_valid,
  input  [2:0]  auto_in_a_bits_opcode,
  input  [2:0]  auto_in_a_bits_param,
  input  [1:0]  auto_in_a_bits_size,
  input  [10:0] auto_in_a_bits_source,
  input  [31:0] auto_in_a_bits_address,
  input         auto_in_a_bits_user_amba_prot_bufferable,
  input         auto_in_a_bits_user_amba_prot_modifiable,
  input         auto_in_a_bits_user_amba_prot_readalloc,
  input         auto_in_a_bits_user_amba_prot_writealloc,
  input         auto_in_a_bits_user_amba_prot_privileged,
  input         auto_in_a_bits_user_amba_prot_secure,
  input         auto_in_a_bits_user_amba_prot_fetch,
  input  [7:0]  auto_in_a_bits_mask,
  input  [63:0] auto_in_a_bits_data,
  input         auto_in_a_bits_corrupt,
  input         auto_in_d_ready,
  output        auto_in_d_valid,
  output [2:0]  auto_in_d_bits_opcode,
  output [1:0]  auto_in_d_bits_size,
  output [10:0] auto_in_d_bits_source,
  output        auto_in_d_bits_denied,
  output [63:0] auto_in_d_bits_data,
  output        auto_in_d_bits_corrupt,
  input         auto_out_aw_ready,
  output        auto_out_aw_valid,
  output [31:0] auto_out_aw_bits_addr,
  output [7:0]  auto_out_aw_bits_len,
  output [2:0]  auto_out_aw_bits_size,
  output [3:0]  auto_out_aw_bits_cache,
  output [2:0]  auto_out_aw_bits_prot,
  output [3:0]  auto_out_aw_bits_echo_tl_state_size,
  output [10:0] auto_out_aw_bits_echo_tl_state_source,
  input         auto_out_w_ready,
  output        auto_out_w_valid,
  output [63:0] auto_out_w_bits_data,
  output [7:0]  auto_out_w_bits_strb,
  output        auto_out_w_bits_last,
  output        auto_out_b_ready,
  input         auto_out_b_valid,
  input  [1:0]  auto_out_b_bits_resp,
  input  [3:0]  auto_out_b_bits_echo_tl_state_size,
  input  [10:0] auto_out_b_bits_echo_tl_state_source,
  input         auto_out_ar_ready,
  output        auto_out_ar_valid,
  output [31:0] auto_out_ar_bits_addr,
  output [7:0]  auto_out_ar_bits_len,
  output [2:0]  auto_out_ar_bits_size,
  output [3:0]  auto_out_ar_bits_cache,
  output [2:0]  auto_out_ar_bits_prot,
  output [3:0]  auto_out_ar_bits_echo_tl_state_size,
  output [10:0] auto_out_ar_bits_echo_tl_state_source,
  output        auto_out_r_ready,
  input         auto_out_r_valid,
  input  [63:0] auto_out_r_bits_data,
  input  [1:0]  auto_out_r_bits_resp,
  input  [3:0]  auto_out_r_bits_echo_tl_state_size,
  input  [10:0] auto_out_r_bits_echo_tl_state_source,
  input         auto_out_r_bits_last
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
`endif // RANDOMIZE_REG_INIT
  wire  deq_rf_reset; // @[Decoupled.scala 296:21]
  wire  deq_clock; // @[Decoupled.scala 296:21]
  wire  deq_reset; // @[Decoupled.scala 296:21]
  wire  deq_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  deq_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [63:0] deq_io_enq_bits_data; // @[Decoupled.scala 296:21]
  wire [7:0] deq_io_enq_bits_strb; // @[Decoupled.scala 296:21]
  wire  deq_io_enq_bits_last; // @[Decoupled.scala 296:21]
  wire  deq_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  deq_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [63:0] deq_io_deq_bits_data; // @[Decoupled.scala 296:21]
  wire [7:0] deq_io_deq_bits_strb; // @[Decoupled.scala 296:21]
  wire  deq_io_deq_bits_last; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_rf_reset; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_clock; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_reset; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_io_enq_ready; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_io_enq_valid; // @[Decoupled.scala 296:21]
  wire [31:0] queue_arw_deq_io_enq_bits_addr; // @[Decoupled.scala 296:21]
  wire [7:0] queue_arw_deq_io_enq_bits_len; // @[Decoupled.scala 296:21]
  wire [2:0] queue_arw_deq_io_enq_bits_size; // @[Decoupled.scala 296:21]
  wire [3:0] queue_arw_deq_io_enq_bits_cache; // @[Decoupled.scala 296:21]
  wire [2:0] queue_arw_deq_io_enq_bits_prot; // @[Decoupled.scala 296:21]
  wire [3:0] queue_arw_deq_io_enq_bits_echo_tl_state_size; // @[Decoupled.scala 296:21]
  wire [10:0] queue_arw_deq_io_enq_bits_echo_tl_state_source; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_io_enq_bits_wen; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_io_deq_ready; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_io_deq_valid; // @[Decoupled.scala 296:21]
  wire [31:0] queue_arw_deq_io_deq_bits_addr; // @[Decoupled.scala 296:21]
  wire [7:0] queue_arw_deq_io_deq_bits_len; // @[Decoupled.scala 296:21]
  wire [2:0] queue_arw_deq_io_deq_bits_size; // @[Decoupled.scala 296:21]
  wire [3:0] queue_arw_deq_io_deq_bits_cache; // @[Decoupled.scala 296:21]
  wire [2:0] queue_arw_deq_io_deq_bits_prot; // @[Decoupled.scala 296:21]
  wire [3:0] queue_arw_deq_io_deq_bits_echo_tl_state_size; // @[Decoupled.scala 296:21]
  wire [10:0] queue_arw_deq_io_deq_bits_echo_tl_state_source; // @[Decoupled.scala 296:21]
  wire  queue_arw_deq_io_deq_bits_wen; // @[Decoupled.scala 296:21]
  wire  a_isPut = ~auto_in_a_bits_opcode[2]; // @[Edges.scala 91:28]
  reg [10:0] count_1; // @[ToAXI4.scala 250:28]
  wire  idle = count_1 == 11'h0; // @[ToAXI4.scala 252:26]
  reg  write; // @[ToAXI4.scala 251:24]
  wire  mismatch = write != a_isPut; // @[ToAXI4.scala 263:50]
  wire  idStall_0 = ~idle & mismatch | count_1 == 11'h720; // @[ToAXI4.scala 264:34]
  reg  counter; // @[Edges.scala 228:27]
  wire  a_first = ~counter; // @[Edges.scala 230:25]
  wire  stall = idStall_0 & a_first; // @[ToAXI4.scala 197:49]
  wire  _bundleIn_0_a_ready_T = ~stall; // @[ToAXI4.scala 198:21]
  wire  out_arw_ready = queue_arw_deq_io_enq_ready; // @[ToAXI4.scala 149:25 Decoupled.scala 299:17]
  wire  out_w_ready = deq_io_enq_ready; // @[ToAXI4.scala 150:23 Decoupled.scala 299:17]
  wire  _bundleIn_0_a_ready_T_3 = a_isPut ? out_arw_ready & out_w_ready : out_arw_ready; // @[ToAXI4.scala 198:34]
  wire  bundleIn_0_a_ready = _bundleIn_0_a_ready_T & _bundleIn_0_a_ready_T_3; // @[ToAXI4.scala 198:28]
  wire  _T = bundleIn_0_a_ready & auto_in_a_valid; // @[Decoupled.scala 40:37]
  wire  counter1 = counter - 1'h1; // @[Edges.scala 229:28]
  wire  queue_arw_bits_wen = queue_arw_deq_io_deq_bits_wen; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  wire  queue_arw_valid = queue_arw_deq_io_deq_valid; // @[Decoupled.scala 317:19 Decoupled.scala 319:15]
  wire [13:0] _out_arw_bits_len_T_1 = 14'h7ff << auto_in_a_bits_size; // @[package.scala 234:77]
  wire [10:0] _out_arw_bits_len_T_3 = ~_out_arw_bits_len_T_1[10:0]; // @[package.scala 234:46]
  wire [1:0] _out_arw_bits_size_T_1 = auto_in_a_bits_size >= 2'h3 ? 2'h3 : auto_in_a_bits_size; // @[ToAXI4.scala 171:23]
  wire  prot_1 = ~auto_in_a_bits_user_amba_prot_secure; // @[ToAXI4.scala 187:20]
  wire [1:0] out_arw_bits_prot_hi = {auto_in_a_bits_user_amba_prot_fetch,prot_1}; // @[Cat.scala 30:58]
  wire [1:0] out_arw_bits_cache_lo = {auto_in_a_bits_user_amba_prot_modifiable,auto_in_a_bits_user_amba_prot_bufferable}
    ; // @[Cat.scala 30:58]
  wire [1:0] out_arw_bits_cache_hi = {auto_in_a_bits_user_amba_prot_writealloc,auto_in_a_bits_user_amba_prot_readalloc}; // @[Cat.scala 30:58]
  wire  _out_arw_valid_T_1 = _bundleIn_0_a_ready_T & auto_in_a_valid; // @[ToAXI4.scala 199:31]
  wire  _out_arw_valid_T_4 = a_isPut ? out_w_ready : 1'h1; // @[ToAXI4.scala 199:51]
  wire  out_arw_valid = _bundleIn_0_a_ready_T & auto_in_a_valid & _out_arw_valid_T_4; // @[ToAXI4.scala 199:45]
  reg  r_holds_d; // @[ToAXI4.scala 208:30]
  wire  _T_2 = auto_in_d_ready & auto_out_r_valid; // @[Decoupled.scala 40:37]
  wire  r_wins = auto_out_r_valid | r_holds_d; // @[ToAXI4.scala 211:32]
  wire  bundleIn_0_d_valid = r_wins ? auto_out_r_valid : auto_out_b_valid; // @[ToAXI4.scala 215:24]
  reg  r_first; // @[ToAXI4.scala 220:28]
  wire  _r_denied_T = auto_out_r_bits_resp == 2'h3; // @[ToAXI4.scala 222:39]
  reg  r_denied_r; // @[Reg.scala 15:16]
  wire  _GEN_3652 = r_first ? _r_denied_T : r_denied_r; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  wire  r_corrupt = auto_out_r_bits_resp != 2'h0; // @[ToAXI4.scala 223:39]
  wire  b_denied = auto_out_b_bits_resp != 2'h0; // @[ToAXI4.scala 224:39]
  wire  r_d_corrupt = r_corrupt | _GEN_3652; // @[ToAXI4.scala 226:100]
  wire [1:0] r_d_size = auto_out_r_bits_echo_tl_state_size[1:0]; // @[Edges.scala 771:17 Edges.scala 774:15]
  wire [1:0] b_d_size = auto_out_b_bits_echo_tl_state_size[1:0]; // @[Edges.scala 755:17 Edges.scala 758:15]
  wire  d_last = r_wins ? auto_out_r_bits_last : 1'h1; // @[ToAXI4.scala 240:23]
  wire  inc = out_arw_ready & out_arw_valid; // @[Decoupled.scala 40:37]
  wire  _dec_T_1 = auto_in_d_ready & bundleIn_0_d_valid; // @[Decoupled.scala 40:37]
  wire  dec = d_last & _dec_T_1; // @[ToAXI4.scala 255:32]
  wire [10:0] _GEN_3654 = {{10'd0}, inc}; // @[ToAXI4.scala 256:24]
  wire [10:0] _count_T_2 = count_1 + _GEN_3654; // @[ToAXI4.scala 256:24]
  wire [10:0] _GEN_3655 = {{10'd0}, dec}; // @[ToAXI4.scala 256:37]
  RHEA__Queue_30 deq ( // @[Decoupled.scala 296:21]
    .rf_reset(deq_rf_reset),
    .clock(deq_clock),
    .reset(deq_reset),
    .io_enq_ready(deq_io_enq_ready),
    .io_enq_valid(deq_io_enq_valid),
    .io_enq_bits_data(deq_io_enq_bits_data),
    .io_enq_bits_strb(deq_io_enq_bits_strb),
    .io_enq_bits_last(deq_io_enq_bits_last),
    .io_deq_ready(deq_io_deq_ready),
    .io_deq_valid(deq_io_deq_valid),
    .io_deq_bits_data(deq_io_deq_bits_data),
    .io_deq_bits_strb(deq_io_deq_bits_strb),
    .io_deq_bits_last(deq_io_deq_bits_last)
  );
  RHEA__Queue_49 queue_arw_deq ( // @[Decoupled.scala 296:21]
    .rf_reset(queue_arw_deq_rf_reset),
    .clock(queue_arw_deq_clock),
    .reset(queue_arw_deq_reset),
    .io_enq_ready(queue_arw_deq_io_enq_ready),
    .io_enq_valid(queue_arw_deq_io_enq_valid),
    .io_enq_bits_addr(queue_arw_deq_io_enq_bits_addr),
    .io_enq_bits_len(queue_arw_deq_io_enq_bits_len),
    .io_enq_bits_size(queue_arw_deq_io_enq_bits_size),
    .io_enq_bits_cache(queue_arw_deq_io_enq_bits_cache),
    .io_enq_bits_prot(queue_arw_deq_io_enq_bits_prot),
    .io_enq_bits_echo_tl_state_size(queue_arw_deq_io_enq_bits_echo_tl_state_size),
    .io_enq_bits_echo_tl_state_source(queue_arw_deq_io_enq_bits_echo_tl_state_source),
    .io_enq_bits_wen(queue_arw_deq_io_enq_bits_wen),
    .io_deq_ready(queue_arw_deq_io_deq_ready),
    .io_deq_valid(queue_arw_deq_io_deq_valid),
    .io_deq_bits_addr(queue_arw_deq_io_deq_bits_addr),
    .io_deq_bits_len(queue_arw_deq_io_deq_bits_len),
    .io_deq_bits_size(queue_arw_deq_io_deq_bits_size),
    .io_deq_bits_cache(queue_arw_deq_io_deq_bits_cache),
    .io_deq_bits_prot(queue_arw_deq_io_deq_bits_prot),
    .io_deq_bits_echo_tl_state_size(queue_arw_deq_io_deq_bits_echo_tl_state_size),
    .io_deq_bits_echo_tl_state_source(queue_arw_deq_io_deq_bits_echo_tl_state_source),
    .io_deq_bits_wen(queue_arw_deq_io_deq_bits_wen)
  );
  assign deq_rf_reset = rf_reset;
  assign queue_arw_deq_rf_reset = rf_reset;
  assign auto_in_a_ready = _bundleIn_0_a_ready_T & _bundleIn_0_a_ready_T_3; // @[ToAXI4.scala 198:28]
  assign auto_in_d_valid = r_wins ? auto_out_r_valid : auto_out_b_valid; // @[ToAXI4.scala 215:24]
  assign auto_in_d_bits_opcode = r_wins ? 3'h1 : 3'h0; // @[ToAXI4.scala 233:23]
  assign auto_in_d_bits_size = r_wins ? r_d_size : b_d_size; // @[ToAXI4.scala 233:23]
  assign auto_in_d_bits_source = r_wins ? auto_out_r_bits_echo_tl_state_source : auto_out_b_bits_echo_tl_state_source; // @[ToAXI4.scala 233:23]
  assign auto_in_d_bits_denied = r_wins ? _GEN_3652 : b_denied; // @[ToAXI4.scala 233:23]
  assign auto_in_d_bits_data = auto_out_r_bits_data; // @[ToAXI4.scala 64:86 LazyModule.scala 390:12]
  assign auto_in_d_bits_corrupt = r_wins & r_d_corrupt; // @[ToAXI4.scala 233:23]
  assign auto_out_aw_valid = queue_arw_valid & queue_arw_bits_wen; // @[ToAXI4.scala 158:39]
  assign auto_out_aw_bits_addr = queue_arw_deq_io_deq_bits_addr; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_len = queue_arw_deq_io_deq_bits_len; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_size = queue_arw_deq_io_deq_bits_size; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_cache = queue_arw_deq_io_deq_bits_cache; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_prot = queue_arw_deq_io_deq_bits_prot; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_echo_tl_state_size = queue_arw_deq_io_deq_bits_echo_tl_state_size; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_aw_bits_echo_tl_state_source = queue_arw_deq_io_deq_bits_echo_tl_state_source; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_w_valid = deq_io_deq_valid; // @[Decoupled.scala 317:19 Decoupled.scala 319:15]
  assign auto_out_w_bits_data = deq_io_deq_bits_data; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_w_bits_strb = deq_io_deq_bits_strb; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_w_bits_last = deq_io_deq_bits_last; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_b_ready = auto_in_d_ready & ~r_wins; // @[ToAXI4.scala 214:33]
  assign auto_out_ar_valid = queue_arw_valid & ~queue_arw_bits_wen; // @[ToAXI4.scala 157:39]
  assign auto_out_ar_bits_addr = queue_arw_deq_io_deq_bits_addr; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_len = queue_arw_deq_io_deq_bits_len; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_size = queue_arw_deq_io_deq_bits_size; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_cache = queue_arw_deq_io_deq_bits_cache; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_prot = queue_arw_deq_io_deq_bits_prot; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_echo_tl_state_size = queue_arw_deq_io_deq_bits_echo_tl_state_size; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_ar_bits_echo_tl_state_source = queue_arw_deq_io_deq_bits_echo_tl_state_source; // @[Decoupled.scala 317:19 Decoupled.scala 318:14]
  assign auto_out_r_ready = auto_in_d_ready; // @[ToAXI4.scala 64:86 LazyModule.scala 388:16]
  assign deq_clock = clock;
  assign deq_reset = reset;
  assign deq_io_enq_valid = _out_arw_valid_T_1 & a_isPut & out_arw_ready; // @[ToAXI4.scala 201:54]
  assign deq_io_enq_bits_data = auto_in_a_bits_data; // @[ToAXI4.scala 64:86 LazyModule.scala 388:16]
  assign deq_io_enq_bits_strb = auto_in_a_bits_mask; // @[ToAXI4.scala 64:86 LazyModule.scala 388:16]
  assign deq_io_enq_bits_last = 1'h1; // @[Edges.scala 231:37]
  assign deq_io_deq_ready = auto_out_w_ready; // @[ToAXI4.scala 64:86 LazyModule.scala 390:12]
  assign queue_arw_deq_clock = clock;
  assign queue_arw_deq_reset = reset;
  assign queue_arw_deq_io_enq_valid = _bundleIn_0_a_ready_T & auto_in_a_valid & _out_arw_valid_T_4; // @[ToAXI4.scala 199:45]
  assign queue_arw_deq_io_enq_bits_addr = auto_in_a_bits_address; // @[ToAXI4.scala 64:86 LazyModule.scala 388:16]
  assign queue_arw_deq_io_enq_bits_len = _out_arw_bits_len_T_3[10:3]; // @[ToAXI4.scala 170:84]
  assign queue_arw_deq_io_enq_bits_size = {{1'd0}, _out_arw_bits_size_T_1}; // @[ToAXI4.scala 149:25 ToAXI4.scala 171:17]
  assign queue_arw_deq_io_enq_bits_cache = {out_arw_bits_cache_hi,out_arw_bits_cache_lo}; // @[Cat.scala 30:58]
  assign queue_arw_deq_io_enq_bits_prot = {out_arw_bits_prot_hi,auto_in_a_bits_user_amba_prot_privileged}; // @[Cat.scala 30:58]
  assign queue_arw_deq_io_enq_bits_echo_tl_state_size = {{2'd0}, auto_in_a_bits_size}; // @[ToAXI4.scala 149:25 ToAXI4.scala 181:22]
  assign queue_arw_deq_io_enq_bits_echo_tl_state_source = auto_in_a_bits_source; // @[ToAXI4.scala 64:86 LazyModule.scala 388:16]
  assign queue_arw_deq_io_enq_bits_wen = ~auto_in_a_bits_opcode[2]; // @[Edges.scala 91:28]
  assign queue_arw_deq_io_deq_ready = queue_arw_bits_wen ? auto_out_aw_ready : auto_out_ar_ready; // @[ToAXI4.scala 159:29]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      count_1 <= 11'h0;
    end else begin
      count_1 <= _count_T_2 - _GEN_3655;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      write <= 1'h0;
    end else if (inc) begin
      write <= a_isPut;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      counter <= 1'h0;
    end else if (_T) begin
      if (a_first) begin
        counter <= 1'h0;
      end else begin
        counter <= counter1;
      end
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r_holds_d <= 1'h0;
    end else if (_T_2) begin
      r_holds_d <= ~auto_out_r_bits_last;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      r_first <= 1'h1;
    end else if (_T_2) begin
      r_first <= auto_out_r_bits_last;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      r_denied_r <= 1'h0;
    end else if (r_first) begin
      r_denied_r <= _r_denied_T;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  count_1 = _RAND_0[10:0];
  _RAND_1 = {1{`RANDOM}};
  write = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  counter = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  r_holds_d = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  r_first = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  r_denied_r = _RAND_5[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    count_1 = 11'h0;
  end
  if (rf_reset) begin
    write = 1'h0;
  end
  if (reset) begin
    counter = 1'h0;
  end
  if (reset) begin
    r_holds_d = 1'h0;
  end
  if (reset) begin
    r_first = 1'h1;
  end
  if (rf_reset) begin
    r_denied_r = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
