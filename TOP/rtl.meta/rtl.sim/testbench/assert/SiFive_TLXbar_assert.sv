//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLXbar_assert(
  input         clock,
  input         reset,
  input         auto_in_2_a_valid,
  input  [2:0]  auto_in_2_a_bits_opcode,
  input  [2:0]  auto_in_2_a_bits_param,
  input  [3:0]  auto_in_2_a_bits_size,
  input  [5:0]  auto_in_2_a_bits_source,
  input  [35:0] auto_in_2_a_bits_address,
  input  [7:0]  auto_in_2_a_bits_mask,
  input         auto_in_2_a_bits_corrupt,
  input         auto_in_2_d_ready,
  input         auto_in_1_a_valid,
  input  [2:0]  auto_in_1_a_bits_opcode,
  input  [2:0]  auto_in_1_a_bits_param,
  input  [3:0]  auto_in_1_a_bits_size,
  input  [4:0]  auto_in_1_a_bits_source,
  input  [35:0] auto_in_1_a_bits_address,
  input  [7:0]  auto_in_1_a_bits_mask,
  input         auto_in_1_a_bits_corrupt,
  input         auto_in_1_b_ready,
  input         auto_in_1_c_valid,
  input  [2:0]  auto_in_1_c_bits_opcode,
  input  [2:0]  auto_in_1_c_bits_param,
  input  [3:0]  auto_in_1_c_bits_size,
  input  [4:0]  auto_in_1_c_bits_source,
  input  [35:0] auto_in_1_c_bits_address,
  input         auto_in_1_c_bits_corrupt,
  input         auto_in_1_d_ready,
  input         auto_in_1_e_valid,
  input  [3:0]  auto_in_1_e_bits_sink,
  input         auto_in_0_a_valid,
  input  [2:0]  auto_in_0_a_bits_opcode,
  input  [2:0]  auto_in_0_a_bits_param,
  input  [3:0]  auto_in_0_a_bits_size,
  input  [4:0]  auto_in_0_a_bits_source,
  input  [35:0] auto_in_0_a_bits_address,
  input  [7:0]  auto_in_0_a_bits_mask,
  input         auto_in_0_a_bits_corrupt,
  input         auto_in_0_b_ready,
  input         auto_in_0_c_valid,
  input  [2:0]  auto_in_0_c_bits_opcode,
  input  [2:0]  auto_in_0_c_bits_param,
  input  [3:0]  auto_in_0_c_bits_size,
  input  [4:0]  auto_in_0_c_bits_source,
  input  [35:0] auto_in_0_c_bits_address,
  input         auto_in_0_c_bits_corrupt,
  input         auto_in_0_d_ready,
  input         auto_in_0_e_valid,
  input  [3:0]  auto_in_0_e_bits_sink,
  input         auto_out_2_b_valid,
  input  [1:0]  auto_out_2_b_bits_param,
  input  [6:0]  auto_out_2_b_bits_source,
  input  [35:0] auto_out_2_b_bits_address,
  input         auto_out_2_c_ready,
  input         _T_1651,
  input         _T_1693,
  input         _T_1694,
  input         _T_1695,
  input         _T_1696,
  input  [85:0] _T_1796,
  input  [6:0]  _T_1803,
  input         _T_1808,
  input         _T_1850,
  input         _T_1851,
  input         _T_1852,
  input         _T_1853,
  input  [85:0] _T_1953,
  input  [6:0]  _T_1960,
  input         _T_1965,
  input         _T_2007,
  input         _T_2008,
  input         _T_2009,
  input         _T_2010,
  input  [85:0] _T_2110,
  input  [6:0]  _T_2117,
  input         _T_1410,
  input         _T_1440,
  input         _T_1443,
  input         _T_1473_0,
  input         _T_1441,
  input         _T_1444,
  input         _T_1473_1,
  input         _T_303,
  input         _T_311,
  input         _T_585,
  input         _T_587,
  input         _T_602,
  input         _T_604,
  input         _T_619,
  input         _T_621,
  input         _T_1376_0,
  input         _T_1376_1,
  input         _T_871,
  input         _T_872,
  input         _T_873,
  input         _T_893,
  input         _T_1031,
  input         _T_1032,
  input         _T_1033,
  input         _T_1053,
  input         _T_1191,
  input         _T_1192,
  input         _T_1193,
  input         _T_1213,
  input         _T_1344,
  input         _T_1345,
  input         _T_1359,
  input         _T_1458,
  input         _T_1531,
  input         _T_1532,
  input         _T_1533,
  input         _T_1553,
  input         _T_1722,
  input         _T_1759,
  input         _T_1879,
  input         _T_1916,
  input         _T_2036,
  input         _T_2073
);
  wire  TLMonitor_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [4:0] TLMonitor_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_b_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_b_valid; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_b_bits_param; // @[Nodes.scala 25:25]
  wire [4:0] TLMonitor_io_in_b_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_io_in_b_bits_address; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_c_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_c_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_c_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_c_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_c_bits_size; // @[Nodes.scala 25:25]
  wire [4:0] TLMonitor_io_in_c_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_io_in_c_bits_address; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_c_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_io_in_d_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [4:0] TLMonitor_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_d_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_e_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_io_in_e_valid; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_io_in_e_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_1_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [4:0] TLMonitor_1_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_1_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_1_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_b_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_b_valid; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_1_io_in_b_bits_param; // @[Nodes.scala 25:25]
  wire [4:0] TLMonitor_1_io_in_b_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_1_io_in_b_bits_address; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_c_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_c_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_c_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_c_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_1_io_in_c_bits_size; // @[Nodes.scala 25:25]
  wire [4:0] TLMonitor_1_io_in_c_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_1_io_in_c_bits_address; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_c_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_1_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_1_io_in_d_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_1_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [4:0] TLMonitor_1_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_1_io_in_d_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_e_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_1_io_in_e_valid; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_1_io_in_e_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_clock; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_reset; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_a_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_a_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_2_io_in_a_bits_opcode; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_2_io_in_a_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_2_io_in_a_bits_size; // @[Nodes.scala 25:25]
  wire [5:0] TLMonitor_2_io_in_a_bits_source; // @[Nodes.scala 25:25]
  wire [35:0] TLMonitor_2_io_in_a_bits_address; // @[Nodes.scala 25:25]
  wire [7:0] TLMonitor_2_io_in_a_bits_mask; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_a_bits_corrupt; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_d_ready; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_d_valid; // @[Nodes.scala 25:25]
  wire [2:0] TLMonitor_2_io_in_d_bits_opcode; // @[Nodes.scala 25:25]
  wire [1:0] TLMonitor_2_io_in_d_bits_param; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_2_io_in_d_bits_size; // @[Nodes.scala 25:25]
  wire [5:0] TLMonitor_2_io_in_d_bits_source; // @[Nodes.scala 25:25]
  wire [3:0] TLMonitor_2_io_in_d_bits_sink; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_d_bits_denied; // @[Nodes.scala 25:25]
  wire  TLMonitor_2_io_in_d_bits_corrupt; // @[Nodes.scala 25:25]
  wire  _T_876; // @[Arbiter.scala 74:52]
  wire  _T_877; // @[Arbiter.scala 74:52]
  wire  _T_879; // @[Arbiter.scala 75:62]
  wire  _T_882; // @[Arbiter.scala 75:62]
  wire  _T_883; // @[Arbiter.scala 75:59]
  wire  _T_884; // @[Arbiter.scala 75:56]
  wire  _T_885; // @[Arbiter.scala 75:62]
  wire  _T_886; // @[Arbiter.scala 75:59]
  wire  _T_888; // @[Arbiter.scala 75:77]
  wire  _T_890; // @[Arbiter.scala 75:13]
  wire  _T_891; // @[Arbiter.scala 75:13]
  wire  _T_894; // @[Arbiter.scala 77:15]
  wire  _T_897; // @[Arbiter.scala 77:36]
  wire  _T_899; // @[Arbiter.scala 77:14]
  wire  _T_900; // @[Arbiter.scala 77:14]
  wire  _T_1036; // @[Arbiter.scala 74:52]
  wire  _T_1037; // @[Arbiter.scala 74:52]
  wire  _T_1039; // @[Arbiter.scala 75:62]
  wire  _T_1042; // @[Arbiter.scala 75:62]
  wire  _T_1043; // @[Arbiter.scala 75:59]
  wire  _T_1044; // @[Arbiter.scala 75:56]
  wire  _T_1045; // @[Arbiter.scala 75:62]
  wire  _T_1046; // @[Arbiter.scala 75:59]
  wire  _T_1048; // @[Arbiter.scala 75:77]
  wire  _T_1050; // @[Arbiter.scala 75:13]
  wire  _T_1051; // @[Arbiter.scala 75:13]
  wire  _T_1054; // @[Arbiter.scala 77:15]
  wire  _T_1057; // @[Arbiter.scala 77:36]
  wire  _T_1059; // @[Arbiter.scala 77:14]
  wire  _T_1060; // @[Arbiter.scala 77:14]
  wire  _T_1196; // @[Arbiter.scala 74:52]
  wire  _T_1197; // @[Arbiter.scala 74:52]
  wire  _T_1199; // @[Arbiter.scala 75:62]
  wire  _T_1202; // @[Arbiter.scala 75:62]
  wire  _T_1203; // @[Arbiter.scala 75:59]
  wire  _T_1204; // @[Arbiter.scala 75:56]
  wire  _T_1205; // @[Arbiter.scala 75:62]
  wire  _T_1206; // @[Arbiter.scala 75:59]
  wire  _T_1208; // @[Arbiter.scala 75:77]
  wire  _T_1210; // @[Arbiter.scala 75:13]
  wire  _T_1211; // @[Arbiter.scala 75:13]
  wire  _T_1214; // @[Arbiter.scala 77:15]
  wire  _T_1217; // @[Arbiter.scala 77:36]
  wire  _T_1219; // @[Arbiter.scala 77:14]
  wire  _T_1220; // @[Arbiter.scala 77:14]
  wire  _T_1348; // @[Arbiter.scala 74:52]
  wire  _T_1350; // @[Arbiter.scala 75:62]
  wire  _T_1353; // @[Arbiter.scala 75:62]
  wire  _T_1354; // @[Arbiter.scala 75:59]
  wire  _T_1357; // @[Arbiter.scala 75:13]
  wire  _T_1358; // @[Arbiter.scala 75:13]
  wire  _T_1360; // @[Arbiter.scala 77:15]
  wire  _T_1362; // @[Arbiter.scala 77:36]
  wire  _T_1364; // @[Arbiter.scala 77:14]
  wire  _T_1365; // @[Arbiter.scala 77:14]
  wire  _T_1447; // @[Arbiter.scala 74:52]
  wire  _T_1449; // @[Arbiter.scala 75:62]
  wire  _T_1452; // @[Arbiter.scala 75:62]
  wire  _T_1453; // @[Arbiter.scala 75:59]
  wire  _T_1456; // @[Arbiter.scala 75:13]
  wire  _T_1457; // @[Arbiter.scala 75:13]
  wire  _T_1459; // @[Arbiter.scala 77:15]
  wire  _T_1461; // @[Arbiter.scala 77:36]
  wire  _T_1463; // @[Arbiter.scala 77:14]
  wire  _T_1464; // @[Arbiter.scala 77:14]
  wire  _T_1536; // @[Arbiter.scala 74:52]
  wire  _T_1537; // @[Arbiter.scala 74:52]
  wire  _T_1539; // @[Arbiter.scala 75:62]
  wire  _T_1542; // @[Arbiter.scala 75:62]
  wire  _T_1543; // @[Arbiter.scala 75:59]
  wire  _T_1544; // @[Arbiter.scala 75:56]
  wire  _T_1545; // @[Arbiter.scala 75:62]
  wire  _T_1546; // @[Arbiter.scala 75:59]
  wire  _T_1548; // @[Arbiter.scala 75:77]
  wire  _T_1550; // @[Arbiter.scala 75:13]
  wire  _T_1551; // @[Arbiter.scala 75:13]
  wire  _T_1554; // @[Arbiter.scala 77:15]
  wire  _T_1557; // @[Arbiter.scala 77:36]
  wire  _T_1559; // @[Arbiter.scala 77:14]
  wire  _T_1560; // @[Arbiter.scala 77:14]
  wire  _T_1699; // @[Arbiter.scala 74:52]
  wire  _T_1700; // @[Arbiter.scala 74:52]
  wire  _T_1701; // @[Arbiter.scala 74:52]
  wire  _T_1703; // @[Arbiter.scala 75:62]
  wire  _T_1706; // @[Arbiter.scala 75:62]
  wire  _T_1707; // @[Arbiter.scala 75:59]
  wire  _T_1708; // @[Arbiter.scala 75:56]
  wire  _T_1709; // @[Arbiter.scala 75:62]
  wire  _T_1710; // @[Arbiter.scala 75:59]
  wire  _T_1711; // @[Arbiter.scala 75:56]
  wire  _T_1712; // @[Arbiter.scala 75:62]
  wire  _T_1713; // @[Arbiter.scala 75:59]
  wire  _T_1715; // @[Arbiter.scala 75:77]
  wire  _T_1716; // @[Arbiter.scala 75:77]
  wire  _T_1718; // @[Arbiter.scala 75:13]
  wire  _T_1719; // @[Arbiter.scala 75:13]
  wire  _T_1723; // @[Arbiter.scala 77:15]
  wire  _T_1727; // @[Arbiter.scala 77:36]
  wire  _T_1729; // @[Arbiter.scala 77:14]
  wire  _T_1730; // @[Arbiter.scala 77:14]
  wire  _T_1856; // @[Arbiter.scala 74:52]
  wire  _T_1857; // @[Arbiter.scala 74:52]
  wire  _T_1858; // @[Arbiter.scala 74:52]
  wire  _T_1860; // @[Arbiter.scala 75:62]
  wire  _T_1863; // @[Arbiter.scala 75:62]
  wire  _T_1864; // @[Arbiter.scala 75:59]
  wire  _T_1865; // @[Arbiter.scala 75:56]
  wire  _T_1866; // @[Arbiter.scala 75:62]
  wire  _T_1867; // @[Arbiter.scala 75:59]
  wire  _T_1868; // @[Arbiter.scala 75:56]
  wire  _T_1869; // @[Arbiter.scala 75:62]
  wire  _T_1870; // @[Arbiter.scala 75:59]
  wire  _T_1872; // @[Arbiter.scala 75:77]
  wire  _T_1873; // @[Arbiter.scala 75:77]
  wire  _T_1875; // @[Arbiter.scala 75:13]
  wire  _T_1876; // @[Arbiter.scala 75:13]
  wire  _T_1880; // @[Arbiter.scala 77:15]
  wire  _T_1884; // @[Arbiter.scala 77:36]
  wire  _T_1886; // @[Arbiter.scala 77:14]
  wire  _T_1887; // @[Arbiter.scala 77:14]
  wire  _T_2013; // @[Arbiter.scala 74:52]
  wire  _T_2014; // @[Arbiter.scala 74:52]
  wire  _T_2015; // @[Arbiter.scala 74:52]
  wire  _T_2017; // @[Arbiter.scala 75:62]
  wire  _T_2020; // @[Arbiter.scala 75:62]
  wire  _T_2021; // @[Arbiter.scala 75:59]
  wire  _T_2022; // @[Arbiter.scala 75:56]
  wire  _T_2023; // @[Arbiter.scala 75:62]
  wire  _T_2024; // @[Arbiter.scala 75:59]
  wire  _T_2025; // @[Arbiter.scala 75:56]
  wire  _T_2026; // @[Arbiter.scala 75:62]
  wire  _T_2027; // @[Arbiter.scala 75:59]
  wire  _T_2029; // @[Arbiter.scala 75:77]
  wire  _T_2030; // @[Arbiter.scala 75:77]
  wire  _T_2032; // @[Arbiter.scala 75:13]
  wire  _T_2033; // @[Arbiter.scala 75:13]
  wire  _T_2037; // @[Arbiter.scala 77:15]
  wire  _T_2041; // @[Arbiter.scala 77:36]
  wire  _T_2043; // @[Arbiter.scala 77:14]
  wire  _T_2044; // @[Arbiter.scala 77:14]
  SiFive_TLMonitor_assert TLMonitor ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_clock),
    .reset(TLMonitor_reset),
    .io_in_a_ready(TLMonitor_io_in_a_ready),
    .io_in_a_valid(TLMonitor_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_io_in_a_bits_corrupt),
    .io_in_b_ready(TLMonitor_io_in_b_ready),
    .io_in_b_valid(TLMonitor_io_in_b_valid),
    .io_in_b_bits_param(TLMonitor_io_in_b_bits_param),
    .io_in_b_bits_source(TLMonitor_io_in_b_bits_source),
    .io_in_b_bits_address(TLMonitor_io_in_b_bits_address),
    .io_in_c_ready(TLMonitor_io_in_c_ready),
    .io_in_c_valid(TLMonitor_io_in_c_valid),
    .io_in_c_bits_opcode(TLMonitor_io_in_c_bits_opcode),
    .io_in_c_bits_param(TLMonitor_io_in_c_bits_param),
    .io_in_c_bits_size(TLMonitor_io_in_c_bits_size),
    .io_in_c_bits_source(TLMonitor_io_in_c_bits_source),
    .io_in_c_bits_address(TLMonitor_io_in_c_bits_address),
    .io_in_c_bits_corrupt(TLMonitor_io_in_c_bits_corrupt),
    .io_in_d_ready(TLMonitor_io_in_d_ready),
    .io_in_d_valid(TLMonitor_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_io_in_d_bits_opcode),
    .io_in_d_bits_param(TLMonitor_io_in_d_bits_param),
    .io_in_d_bits_size(TLMonitor_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_io_in_d_bits_source),
    .io_in_d_bits_sink(TLMonitor_io_in_d_bits_sink),
    .io_in_d_bits_denied(TLMonitor_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_io_in_d_bits_corrupt),
    .io_in_e_ready(TLMonitor_io_in_e_ready),
    .io_in_e_valid(TLMonitor_io_in_e_valid),
    .io_in_e_bits_sink(TLMonitor_io_in_e_bits_sink)
  );
  SiFive_TLMonitor_assert TLMonitor_1 ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_1_clock),
    .reset(TLMonitor_1_reset),
    .io_in_a_ready(TLMonitor_1_io_in_a_ready),
    .io_in_a_valid(TLMonitor_1_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_1_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_1_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_1_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_1_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_1_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_1_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_1_io_in_a_bits_corrupt),
    .io_in_b_ready(TLMonitor_1_io_in_b_ready),
    .io_in_b_valid(TLMonitor_1_io_in_b_valid),
    .io_in_b_bits_param(TLMonitor_1_io_in_b_bits_param),
    .io_in_b_bits_source(TLMonitor_1_io_in_b_bits_source),
    .io_in_b_bits_address(TLMonitor_1_io_in_b_bits_address),
    .io_in_c_ready(TLMonitor_1_io_in_c_ready),
    .io_in_c_valid(TLMonitor_1_io_in_c_valid),
    .io_in_c_bits_opcode(TLMonitor_1_io_in_c_bits_opcode),
    .io_in_c_bits_param(TLMonitor_1_io_in_c_bits_param),
    .io_in_c_bits_size(TLMonitor_1_io_in_c_bits_size),
    .io_in_c_bits_source(TLMonitor_1_io_in_c_bits_source),
    .io_in_c_bits_address(TLMonitor_1_io_in_c_bits_address),
    .io_in_c_bits_corrupt(TLMonitor_1_io_in_c_bits_corrupt),
    .io_in_d_ready(TLMonitor_1_io_in_d_ready),
    .io_in_d_valid(TLMonitor_1_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_1_io_in_d_bits_opcode),
    .io_in_d_bits_param(TLMonitor_1_io_in_d_bits_param),
    .io_in_d_bits_size(TLMonitor_1_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_1_io_in_d_bits_source),
    .io_in_d_bits_sink(TLMonitor_1_io_in_d_bits_sink),
    .io_in_d_bits_denied(TLMonitor_1_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_1_io_in_d_bits_corrupt),
    .io_in_e_ready(TLMonitor_1_io_in_e_ready),
    .io_in_e_valid(TLMonitor_1_io_in_e_valid),
    .io_in_e_bits_sink(TLMonitor_1_io_in_e_bits_sink)
  );
  SiFive_TLMonitor_2_assert TLMonitor_2 ( // @[Nodes.scala 25:25]
    .clock(TLMonitor_2_clock),
    .reset(TLMonitor_2_reset),
    .io_in_a_ready(TLMonitor_2_io_in_a_ready),
    .io_in_a_valid(TLMonitor_2_io_in_a_valid),
    .io_in_a_bits_opcode(TLMonitor_2_io_in_a_bits_opcode),
    .io_in_a_bits_param(TLMonitor_2_io_in_a_bits_param),
    .io_in_a_bits_size(TLMonitor_2_io_in_a_bits_size),
    .io_in_a_bits_source(TLMonitor_2_io_in_a_bits_source),
    .io_in_a_bits_address(TLMonitor_2_io_in_a_bits_address),
    .io_in_a_bits_mask(TLMonitor_2_io_in_a_bits_mask),
    .io_in_a_bits_corrupt(TLMonitor_2_io_in_a_bits_corrupt),
    .io_in_d_ready(TLMonitor_2_io_in_d_ready),
    .io_in_d_valid(TLMonitor_2_io_in_d_valid),
    .io_in_d_bits_opcode(TLMonitor_2_io_in_d_bits_opcode),
    .io_in_d_bits_param(TLMonitor_2_io_in_d_bits_param),
    .io_in_d_bits_size(TLMonitor_2_io_in_d_bits_size),
    .io_in_d_bits_source(TLMonitor_2_io_in_d_bits_source),
    .io_in_d_bits_sink(TLMonitor_2_io_in_d_bits_sink),
    .io_in_d_bits_denied(TLMonitor_2_io_in_d_bits_denied),
    .io_in_d_bits_corrupt(TLMonitor_2_io_in_d_bits_corrupt)
  );
  assign _T_876 = _T_871 | _T_872; // @[Arbiter.scala 74:52]
  assign _T_877 = _T_876 | _T_873; // @[Arbiter.scala 74:52]
  assign _T_879 = ~_T_871; // @[Arbiter.scala 75:62]
  assign _T_882 = ~_T_872; // @[Arbiter.scala 75:62]
  assign _T_883 = _T_879 | _T_882; // @[Arbiter.scala 75:59]
  assign _T_884 = ~_T_876; // @[Arbiter.scala 75:56]
  assign _T_885 = ~_T_873; // @[Arbiter.scala 75:62]
  assign _T_886 = _T_884 | _T_885; // @[Arbiter.scala 75:59]
  assign _T_888 = _T_883 & _T_886; // @[Arbiter.scala 75:77]
  assign _T_890 = _T_888 | reset; // @[Arbiter.scala 75:13]
  assign _T_891 = ~_T_890; // @[Arbiter.scala 75:13]
  assign _T_894 = ~_T_893; // @[Arbiter.scala 77:15]
  assign _T_897 = _T_894 | _T_877; // @[Arbiter.scala 77:36]
  assign _T_899 = _T_897 | reset; // @[Arbiter.scala 77:14]
  assign _T_900 = ~_T_899; // @[Arbiter.scala 77:14]
  assign _T_1036 = _T_1031 | _T_1032; // @[Arbiter.scala 74:52]
  assign _T_1037 = _T_1036 | _T_1033; // @[Arbiter.scala 74:52]
  assign _T_1039 = ~_T_1031; // @[Arbiter.scala 75:62]
  assign _T_1042 = ~_T_1032; // @[Arbiter.scala 75:62]
  assign _T_1043 = _T_1039 | _T_1042; // @[Arbiter.scala 75:59]
  assign _T_1044 = ~_T_1036; // @[Arbiter.scala 75:56]
  assign _T_1045 = ~_T_1033; // @[Arbiter.scala 75:62]
  assign _T_1046 = _T_1044 | _T_1045; // @[Arbiter.scala 75:59]
  assign _T_1048 = _T_1043 & _T_1046; // @[Arbiter.scala 75:77]
  assign _T_1050 = _T_1048 | reset; // @[Arbiter.scala 75:13]
  assign _T_1051 = ~_T_1050; // @[Arbiter.scala 75:13]
  assign _T_1054 = ~_T_1053; // @[Arbiter.scala 77:15]
  assign _T_1057 = _T_1054 | _T_1037; // @[Arbiter.scala 77:36]
  assign _T_1059 = _T_1057 | reset; // @[Arbiter.scala 77:14]
  assign _T_1060 = ~_T_1059; // @[Arbiter.scala 77:14]
  assign _T_1196 = _T_1191 | _T_1192; // @[Arbiter.scala 74:52]
  assign _T_1197 = _T_1196 | _T_1193; // @[Arbiter.scala 74:52]
  assign _T_1199 = ~_T_1191; // @[Arbiter.scala 75:62]
  assign _T_1202 = ~_T_1192; // @[Arbiter.scala 75:62]
  assign _T_1203 = _T_1199 | _T_1202; // @[Arbiter.scala 75:59]
  assign _T_1204 = ~_T_1196; // @[Arbiter.scala 75:56]
  assign _T_1205 = ~_T_1193; // @[Arbiter.scala 75:62]
  assign _T_1206 = _T_1204 | _T_1205; // @[Arbiter.scala 75:59]
  assign _T_1208 = _T_1203 & _T_1206; // @[Arbiter.scala 75:77]
  assign _T_1210 = _T_1208 | reset; // @[Arbiter.scala 75:13]
  assign _T_1211 = ~_T_1210; // @[Arbiter.scala 75:13]
  assign _T_1214 = ~_T_1213; // @[Arbiter.scala 77:15]
  assign _T_1217 = _T_1214 | _T_1197; // @[Arbiter.scala 77:36]
  assign _T_1219 = _T_1217 | reset; // @[Arbiter.scala 77:14]
  assign _T_1220 = ~_T_1219; // @[Arbiter.scala 77:14]
  assign _T_1348 = _T_1344 | _T_1345; // @[Arbiter.scala 74:52]
  assign _T_1350 = ~_T_1344; // @[Arbiter.scala 75:62]
  assign _T_1353 = ~_T_1345; // @[Arbiter.scala 75:62]
  assign _T_1354 = _T_1350 | _T_1353; // @[Arbiter.scala 75:59]
  assign _T_1357 = _T_1354 | reset; // @[Arbiter.scala 75:13]
  assign _T_1358 = ~_T_1357; // @[Arbiter.scala 75:13]
  assign _T_1360 = ~_T_1359; // @[Arbiter.scala 77:15]
  assign _T_1362 = _T_1360 | _T_1348; // @[Arbiter.scala 77:36]
  assign _T_1364 = _T_1362 | reset; // @[Arbiter.scala 77:14]
  assign _T_1365 = ~_T_1364; // @[Arbiter.scala 77:14]
  assign _T_1447 = _T_1443 | _T_1444; // @[Arbiter.scala 74:52]
  assign _T_1449 = ~_T_1443; // @[Arbiter.scala 75:62]
  assign _T_1452 = ~_T_1444; // @[Arbiter.scala 75:62]
  assign _T_1453 = _T_1449 | _T_1452; // @[Arbiter.scala 75:59]
  assign _T_1456 = _T_1453 | reset; // @[Arbiter.scala 75:13]
  assign _T_1457 = ~_T_1456; // @[Arbiter.scala 75:13]
  assign _T_1459 = ~_T_1458; // @[Arbiter.scala 77:15]
  assign _T_1461 = _T_1459 | _T_1447; // @[Arbiter.scala 77:36]
  assign _T_1463 = _T_1461 | reset; // @[Arbiter.scala 77:14]
  assign _T_1464 = ~_T_1463; // @[Arbiter.scala 77:14]
  assign _T_1536 = _T_1531 | _T_1532; // @[Arbiter.scala 74:52]
  assign _T_1537 = _T_1536 | _T_1533; // @[Arbiter.scala 74:52]
  assign _T_1539 = ~_T_1531; // @[Arbiter.scala 75:62]
  assign _T_1542 = ~_T_1532; // @[Arbiter.scala 75:62]
  assign _T_1543 = _T_1539 | _T_1542; // @[Arbiter.scala 75:59]
  assign _T_1544 = ~_T_1536; // @[Arbiter.scala 75:56]
  assign _T_1545 = ~_T_1533; // @[Arbiter.scala 75:62]
  assign _T_1546 = _T_1544 | _T_1545; // @[Arbiter.scala 75:59]
  assign _T_1548 = _T_1543 & _T_1546; // @[Arbiter.scala 75:77]
  assign _T_1550 = _T_1548 | reset; // @[Arbiter.scala 75:13]
  assign _T_1551 = ~_T_1550; // @[Arbiter.scala 75:13]
  assign _T_1554 = ~_T_1553; // @[Arbiter.scala 77:15]
  assign _T_1557 = _T_1554 | _T_1537; // @[Arbiter.scala 77:36]
  assign _T_1559 = _T_1557 | reset; // @[Arbiter.scala 77:14]
  assign _T_1560 = ~_T_1559; // @[Arbiter.scala 77:14]
  assign _T_1699 = _T_1693 | _T_1694; // @[Arbiter.scala 74:52]
  assign _T_1700 = _T_1699 | _T_1695; // @[Arbiter.scala 74:52]
  assign _T_1701 = _T_1700 | _T_1696; // @[Arbiter.scala 74:52]
  assign _T_1703 = ~_T_1693; // @[Arbiter.scala 75:62]
  assign _T_1706 = ~_T_1694; // @[Arbiter.scala 75:62]
  assign _T_1707 = _T_1703 | _T_1706; // @[Arbiter.scala 75:59]
  assign _T_1708 = ~_T_1699; // @[Arbiter.scala 75:56]
  assign _T_1709 = ~_T_1695; // @[Arbiter.scala 75:62]
  assign _T_1710 = _T_1708 | _T_1709; // @[Arbiter.scala 75:59]
  assign _T_1711 = ~_T_1700; // @[Arbiter.scala 75:56]
  assign _T_1712 = ~_T_1696; // @[Arbiter.scala 75:62]
  assign _T_1713 = _T_1711 | _T_1712; // @[Arbiter.scala 75:59]
  assign _T_1715 = _T_1707 & _T_1710; // @[Arbiter.scala 75:77]
  assign _T_1716 = _T_1715 & _T_1713; // @[Arbiter.scala 75:77]
  assign _T_1718 = _T_1716 | reset; // @[Arbiter.scala 75:13]
  assign _T_1719 = ~_T_1718; // @[Arbiter.scala 75:13]
  assign _T_1723 = ~_T_1722; // @[Arbiter.scala 77:15]
  assign _T_1727 = _T_1723 | _T_1701; // @[Arbiter.scala 77:36]
  assign _T_1729 = _T_1727 | reset; // @[Arbiter.scala 77:14]
  assign _T_1730 = ~_T_1729; // @[Arbiter.scala 77:14]
  assign _T_1856 = _T_1850 | _T_1851; // @[Arbiter.scala 74:52]
  assign _T_1857 = _T_1856 | _T_1852; // @[Arbiter.scala 74:52]
  assign _T_1858 = _T_1857 | _T_1853; // @[Arbiter.scala 74:52]
  assign _T_1860 = ~_T_1850; // @[Arbiter.scala 75:62]
  assign _T_1863 = ~_T_1851; // @[Arbiter.scala 75:62]
  assign _T_1864 = _T_1860 | _T_1863; // @[Arbiter.scala 75:59]
  assign _T_1865 = ~_T_1856; // @[Arbiter.scala 75:56]
  assign _T_1866 = ~_T_1852; // @[Arbiter.scala 75:62]
  assign _T_1867 = _T_1865 | _T_1866; // @[Arbiter.scala 75:59]
  assign _T_1868 = ~_T_1857; // @[Arbiter.scala 75:56]
  assign _T_1869 = ~_T_1853; // @[Arbiter.scala 75:62]
  assign _T_1870 = _T_1868 | _T_1869; // @[Arbiter.scala 75:59]
  assign _T_1872 = _T_1864 & _T_1867; // @[Arbiter.scala 75:77]
  assign _T_1873 = _T_1872 & _T_1870; // @[Arbiter.scala 75:77]
  assign _T_1875 = _T_1873 | reset; // @[Arbiter.scala 75:13]
  assign _T_1876 = ~_T_1875; // @[Arbiter.scala 75:13]
  assign _T_1880 = ~_T_1879; // @[Arbiter.scala 77:15]
  assign _T_1884 = _T_1880 | _T_1858; // @[Arbiter.scala 77:36]
  assign _T_1886 = _T_1884 | reset; // @[Arbiter.scala 77:14]
  assign _T_1887 = ~_T_1886; // @[Arbiter.scala 77:14]
  assign _T_2013 = _T_2007 | _T_2008; // @[Arbiter.scala 74:52]
  assign _T_2014 = _T_2013 | _T_2009; // @[Arbiter.scala 74:52]
  assign _T_2015 = _T_2014 | _T_2010; // @[Arbiter.scala 74:52]
  assign _T_2017 = ~_T_2007; // @[Arbiter.scala 75:62]
  assign _T_2020 = ~_T_2008; // @[Arbiter.scala 75:62]
  assign _T_2021 = _T_2017 | _T_2020; // @[Arbiter.scala 75:59]
  assign _T_2022 = ~_T_2013; // @[Arbiter.scala 75:56]
  assign _T_2023 = ~_T_2009; // @[Arbiter.scala 75:62]
  assign _T_2024 = _T_2022 | _T_2023; // @[Arbiter.scala 75:59]
  assign _T_2025 = ~_T_2014; // @[Arbiter.scala 75:56]
  assign _T_2026 = ~_T_2010; // @[Arbiter.scala 75:62]
  assign _T_2027 = _T_2025 | _T_2026; // @[Arbiter.scala 75:59]
  assign _T_2029 = _T_2021 & _T_2024; // @[Arbiter.scala 75:77]
  assign _T_2030 = _T_2029 & _T_2027; // @[Arbiter.scala 75:77]
  assign _T_2032 = _T_2030 | reset; // @[Arbiter.scala 75:13]
  assign _T_2033 = ~_T_2032; // @[Arbiter.scala 75:13]
  assign _T_2037 = ~_T_2036; // @[Arbiter.scala 77:15]
  assign _T_2041 = _T_2037 | _T_2015; // @[Arbiter.scala 77:36]
  assign _T_2043 = _T_2041 | reset; // @[Arbiter.scala 77:14]
  assign _T_2044 = ~_T_2043; // @[Arbiter.scala 77:14]
  assign TLMonitor_clock = clock;
  assign TLMonitor_reset = reset;
  assign TLMonitor_io_in_a_ready = _T_587 | _T_585; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_valid = auto_in_0_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_opcode = auto_in_0_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_param = auto_in_0_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_size = auto_in_0_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_source = auto_in_0_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_address = auto_in_0_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_mask = auto_in_0_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_a_bits_corrupt = auto_in_0_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_b_ready = auto_in_0_b_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_b_valid = auto_out_2_b_valid & _T_303; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_b_bits_param = auto_out_2_b_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_b_bits_source = auto_out_2_b_bits_source[4:0]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_b_bits_address = auto_out_2_b_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_ready = auto_out_2_c_ready & _T_1376_0; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_valid = auto_in_0_c_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_opcode = auto_in_0_c_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_param = auto_in_0_c_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_size = auto_in_0_c_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_source = auto_in_0_c_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_address = auto_in_0_c_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_c_bits_corrupt = auto_in_0_c_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_ready = auto_in_0_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_valid = _T_1651 ? _T_1722 : _T_1759; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_opcode = _T_1796[85:83]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_param = _T_1796[82:81]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_size = _T_1796[80:77]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_source = _T_1803[4:0]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_sink = _T_1796[69:66]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_denied = _T_1796[65]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_d_bits_corrupt = _T_1796[0]; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_e_ready = _T_1410 ? _T_1440 : _T_1473_0; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_e_valid = auto_in_0_e_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_io_in_e_bits_sink = auto_in_0_e_bits_sink; // @[Nodes.scala 26:19]
  assign TLMonitor_1_clock = clock;
  assign TLMonitor_1_reset = reset;
  assign TLMonitor_1_io_in_a_ready = _T_604 | _T_602; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_valid = auto_in_1_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_opcode = auto_in_1_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_param = auto_in_1_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_size = auto_in_1_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_source = auto_in_1_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_address = auto_in_1_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_mask = auto_in_1_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_a_bits_corrupt = auto_in_1_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_b_ready = auto_in_1_b_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_b_valid = auto_out_2_b_valid & _T_311; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_b_bits_param = auto_out_2_b_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_b_bits_source = auto_out_2_b_bits_source[4:0]; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_b_bits_address = auto_out_2_b_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_c_ready = auto_out_2_c_ready & _T_1376_1; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_c_valid = auto_in_1_c_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_c_bits_opcode = auto_in_1_c_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_c_bits_param = auto_in_1_c_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_c_bits_size = auto_in_1_c_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_c_bits_source = auto_in_1_c_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_c_bits_address = auto_in_1_c_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_c_bits_corrupt = auto_in_1_c_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_ready = auto_in_1_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_valid = _T_1808 ? _T_1879 : _T_1916; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_opcode = _T_1953[85:83]; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_param = _T_1953[82:81]; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_size = _T_1953[80:77]; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_source = _T_1960[4:0]; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_sink = _T_1953[69:66]; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_denied = _T_1953[65]; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_d_bits_corrupt = _T_1953[0]; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_e_ready = _T_1410 ? _T_1441 : _T_1473_1; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_e_valid = auto_in_1_e_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_1_io_in_e_bits_sink = auto_in_1_e_bits_sink; // @[Nodes.scala 26:19]
  assign TLMonitor_2_clock = clock;
  assign TLMonitor_2_reset = reset;
  assign TLMonitor_2_io_in_a_ready = _T_621 | _T_619; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_valid = auto_in_2_a_valid; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_opcode = auto_in_2_a_bits_opcode; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_param = auto_in_2_a_bits_param; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_size = auto_in_2_a_bits_size; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_source = auto_in_2_a_bits_source; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_address = auto_in_2_a_bits_address; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_mask = auto_in_2_a_bits_mask; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_a_bits_corrupt = auto_in_2_a_bits_corrupt; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_ready = auto_in_2_d_ready; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_valid = _T_1965 ? _T_2036 : _T_2073; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_opcode = _T_2110[85:83]; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_param = _T_2110[82:81]; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_size = _T_2110[80:77]; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_source = _T_2117[5:0]; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_sink = _T_2110[69:66]; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_denied = _T_2110[65]; // @[Nodes.scala 26:19]
  assign TLMonitor_2_io_in_d_bits_corrupt = _T_2110[0]; // @[Nodes.scala 26:19]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_891) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_891) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_900) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_900) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1051) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1051) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1060) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1060) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1211) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1211) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1220) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1220) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1358) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1358) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1365) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1365) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1457) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1457) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1464) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1464) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1551) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1551) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1560) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1560) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1719) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1719) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1730) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1730) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1876) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1876) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_1887) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_1887) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2033) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:75 assert((prefixOR zip winner) map { case (p,w) => !p || !w } reduce {_ && _})\n"); // @[Arbiter.scala 75:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2033) begin
          $fatal; // @[Arbiter.scala 75:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (_T_2044) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at Arbiter.scala:77 assert (!valids.reduce(_||_) || winner.reduce(_||_))\n"); // @[Arbiter.scala 77:14]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (_T_2044) begin
          $fatal; // @[Arbiter.scala 77:14]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
