//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__MemoryBus(
  input          rf_reset,
  input          auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_ready,
  output         auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_valid,
  output [7:0]   auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_bits_id,
  output [35:0]  auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_bits_addr,
  output [7:0]   auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_bits_len,
  output [2:0]   auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_bits_size,
  output [3:0]   auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_bits_cache,
  output [2:0]   auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_bits_prot,
  input          auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_w_ready,
  output         auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_w_valid,
  output [127:0] auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_w_bits_data,
  output [15:0]  auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_w_bits_strb,
  output         auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_w_bits_last,
  output         auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_b_ready,
  input          auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_b_valid,
  input  [7:0]   auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_b_bits_id,
  input  [1:0]   auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_b_bits_resp,
  input          auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_ready,
  output         auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_valid,
  output [7:0]   auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_bits_id,
  output [35:0]  auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_bits_addr,
  output [7:0]   auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_bits_len,
  output [2:0]   auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_bits_size,
  output [3:0]   auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_bits_cache,
  output [2:0]   auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_bits_prot,
  output         auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_r_ready,
  input          auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_r_valid,
  input  [7:0]   auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_r_bits_id,
  input  [127:0] auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_r_bits_data,
  input  [1:0]   auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_r_bits_resp,
  input          auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_r_bits_last,
  input          auto_subsystem_mbus_clock_groups_in_member_subsystem_mbus_0_clock,
  input          auto_subsystem_mbus_clock_groups_in_member_subsystem_mbus_0_reset,
  output         auto_bus_xing_in_3_a_ready,
  input          auto_bus_xing_in_3_a_valid,
  input  [2:0]   auto_bus_xing_in_3_a_bits_opcode,
  input  [2:0]   auto_bus_xing_in_3_a_bits_param,
  input  [2:0]   auto_bus_xing_in_3_a_bits_size,
  input  [3:0]   auto_bus_xing_in_3_a_bits_source,
  input  [35:0]  auto_bus_xing_in_3_a_bits_address,
  input          auto_bus_xing_in_3_a_bits_user_amba_prot_bufferable,
  input          auto_bus_xing_in_3_a_bits_user_amba_prot_modifiable,
  input          auto_bus_xing_in_3_a_bits_user_amba_prot_readalloc,
  input          auto_bus_xing_in_3_a_bits_user_amba_prot_writealloc,
  input          auto_bus_xing_in_3_a_bits_user_amba_prot_privileged,
  input          auto_bus_xing_in_3_a_bits_user_amba_prot_secure,
  input  [15:0]  auto_bus_xing_in_3_a_bits_mask,
  input  [127:0] auto_bus_xing_in_3_a_bits_data,
  input          auto_bus_xing_in_3_d_ready,
  output         auto_bus_xing_in_3_d_valid,
  output [2:0]   auto_bus_xing_in_3_d_bits_opcode,
  output [2:0]   auto_bus_xing_in_3_d_bits_size,
  output [3:0]   auto_bus_xing_in_3_d_bits_source,
  output         auto_bus_xing_in_3_d_bits_denied,
  output [127:0] auto_bus_xing_in_3_d_bits_data,
  output         auto_bus_xing_in_3_d_bits_corrupt,
  output         auto_bus_xing_in_2_a_ready,
  input          auto_bus_xing_in_2_a_valid,
  input  [2:0]   auto_bus_xing_in_2_a_bits_opcode,
  input  [2:0]   auto_bus_xing_in_2_a_bits_param,
  input  [2:0]   auto_bus_xing_in_2_a_bits_size,
  input  [3:0]   auto_bus_xing_in_2_a_bits_source,
  input  [35:0]  auto_bus_xing_in_2_a_bits_address,
  input          auto_bus_xing_in_2_a_bits_user_amba_prot_bufferable,
  input          auto_bus_xing_in_2_a_bits_user_amba_prot_modifiable,
  input          auto_bus_xing_in_2_a_bits_user_amba_prot_readalloc,
  input          auto_bus_xing_in_2_a_bits_user_amba_prot_writealloc,
  input          auto_bus_xing_in_2_a_bits_user_amba_prot_privileged,
  input          auto_bus_xing_in_2_a_bits_user_amba_prot_secure,
  input  [15:0]  auto_bus_xing_in_2_a_bits_mask,
  input  [127:0] auto_bus_xing_in_2_a_bits_data,
  input          auto_bus_xing_in_2_d_ready,
  output         auto_bus_xing_in_2_d_valid,
  output [2:0]   auto_bus_xing_in_2_d_bits_opcode,
  output [2:0]   auto_bus_xing_in_2_d_bits_size,
  output [3:0]   auto_bus_xing_in_2_d_bits_source,
  output         auto_bus_xing_in_2_d_bits_denied,
  output [127:0] auto_bus_xing_in_2_d_bits_data,
  output         auto_bus_xing_in_2_d_bits_corrupt,
  output         auto_bus_xing_in_1_a_ready,
  input          auto_bus_xing_in_1_a_valid,
  input  [2:0]   auto_bus_xing_in_1_a_bits_opcode,
  input  [2:0]   auto_bus_xing_in_1_a_bits_param,
  input  [2:0]   auto_bus_xing_in_1_a_bits_size,
  input  [3:0]   auto_bus_xing_in_1_a_bits_source,
  input  [35:0]  auto_bus_xing_in_1_a_bits_address,
  input          auto_bus_xing_in_1_a_bits_user_amba_prot_bufferable,
  input          auto_bus_xing_in_1_a_bits_user_amba_prot_modifiable,
  input          auto_bus_xing_in_1_a_bits_user_amba_prot_readalloc,
  input          auto_bus_xing_in_1_a_bits_user_amba_prot_writealloc,
  input          auto_bus_xing_in_1_a_bits_user_amba_prot_privileged,
  input          auto_bus_xing_in_1_a_bits_user_amba_prot_secure,
  input  [15:0]  auto_bus_xing_in_1_a_bits_mask,
  input  [127:0] auto_bus_xing_in_1_a_bits_data,
  input          auto_bus_xing_in_1_d_ready,
  output         auto_bus_xing_in_1_d_valid,
  output [2:0]   auto_bus_xing_in_1_d_bits_opcode,
  output [2:0]   auto_bus_xing_in_1_d_bits_size,
  output [3:0]   auto_bus_xing_in_1_d_bits_source,
  output         auto_bus_xing_in_1_d_bits_denied,
  output [127:0] auto_bus_xing_in_1_d_bits_data,
  output         auto_bus_xing_in_1_d_bits_corrupt,
  output         auto_bus_xing_in_0_a_ready,
  input          auto_bus_xing_in_0_a_valid,
  input  [2:0]   auto_bus_xing_in_0_a_bits_opcode,
  input  [2:0]   auto_bus_xing_in_0_a_bits_param,
  input  [2:0]   auto_bus_xing_in_0_a_bits_size,
  input  [3:0]   auto_bus_xing_in_0_a_bits_source,
  input  [35:0]  auto_bus_xing_in_0_a_bits_address,
  input          auto_bus_xing_in_0_a_bits_user_amba_prot_bufferable,
  input          auto_bus_xing_in_0_a_bits_user_amba_prot_modifiable,
  input          auto_bus_xing_in_0_a_bits_user_amba_prot_readalloc,
  input          auto_bus_xing_in_0_a_bits_user_amba_prot_writealloc,
  input          auto_bus_xing_in_0_a_bits_user_amba_prot_privileged,
  input          auto_bus_xing_in_0_a_bits_user_amba_prot_secure,
  input  [15:0]  auto_bus_xing_in_0_a_bits_mask,
  input  [127:0] auto_bus_xing_in_0_a_bits_data,
  input          auto_bus_xing_in_0_d_ready,
  output         auto_bus_xing_in_0_d_valid,
  output [2:0]   auto_bus_xing_in_0_d_bits_opcode,
  output [2:0]   auto_bus_xing_in_0_d_bits_size,
  output [3:0]   auto_bus_xing_in_0_d_bits_source,
  output         auto_bus_xing_in_0_d_bits_denied,
  output [127:0] auto_bus_xing_in_0_d_bits_data,
  output         auto_bus_xing_in_0_d_bits_corrupt
);
  wire  subsystem_mbus_clock_groups_auto_in_member_subsystem_mbus_0_clock;
  wire  subsystem_mbus_clock_groups_auto_in_member_subsystem_mbus_0_reset;
  wire  subsystem_mbus_clock_groups_auto_out_member_subsystem_mbus_0_clock;
  wire  subsystem_mbus_clock_groups_auto_out_member_subsystem_mbus_0_reset;
  wire  clockGroup_auto_in_member_subsystem_mbus_0_clock;
  wire  clockGroup_auto_in_member_subsystem_mbus_0_reset;
  wire  clockGroup_auto_out_clock;
  wire  clockGroup_auto_out_reset;
  wire  fixedClockNode_auto_in_clock;
  wire  fixedClockNode_auto_in_reset;
  wire  fixedClockNode_auto_out_clock;
  wire  fixedClockNode_auto_out_reset;
  wire  subsystem_mbus_xbar_rf_reset; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_clock; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_reset; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_3_a_ready; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_3_a_valid; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_3_a_bits_opcode; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_3_a_bits_param; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_3_a_bits_size; // @[MemoryBus.scala 42:32]
  wire [3:0] subsystem_mbus_xbar_auto_in_3_a_bits_source; // @[MemoryBus.scala 42:32]
  wire [35:0] subsystem_mbus_xbar_auto_in_3_a_bits_address; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_bufferable; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_modifiable; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_readalloc; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_writealloc; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_privileged; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_secure; // @[MemoryBus.scala 42:32]
  wire [15:0] subsystem_mbus_xbar_auto_in_3_a_bits_mask; // @[MemoryBus.scala 42:32]
  wire [127:0] subsystem_mbus_xbar_auto_in_3_a_bits_data; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_3_d_ready; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_3_d_valid; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_3_d_bits_opcode; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_3_d_bits_size; // @[MemoryBus.scala 42:32]
  wire [3:0] subsystem_mbus_xbar_auto_in_3_d_bits_source; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_3_d_bits_denied; // @[MemoryBus.scala 42:32]
  wire [127:0] subsystem_mbus_xbar_auto_in_3_d_bits_data; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_3_d_bits_corrupt; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_2_a_ready; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_2_a_valid; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_2_a_bits_opcode; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_2_a_bits_param; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_2_a_bits_size; // @[MemoryBus.scala 42:32]
  wire [3:0] subsystem_mbus_xbar_auto_in_2_a_bits_source; // @[MemoryBus.scala 42:32]
  wire [35:0] subsystem_mbus_xbar_auto_in_2_a_bits_address; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_bufferable; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_modifiable; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_readalloc; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_writealloc; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_privileged; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_secure; // @[MemoryBus.scala 42:32]
  wire [15:0] subsystem_mbus_xbar_auto_in_2_a_bits_mask; // @[MemoryBus.scala 42:32]
  wire [127:0] subsystem_mbus_xbar_auto_in_2_a_bits_data; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_2_d_ready; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_2_d_valid; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_2_d_bits_opcode; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_2_d_bits_size; // @[MemoryBus.scala 42:32]
  wire [3:0] subsystem_mbus_xbar_auto_in_2_d_bits_source; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_2_d_bits_denied; // @[MemoryBus.scala 42:32]
  wire [127:0] subsystem_mbus_xbar_auto_in_2_d_bits_data; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_2_d_bits_corrupt; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_1_a_ready; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_1_a_valid; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_1_a_bits_opcode; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_1_a_bits_param; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_1_a_bits_size; // @[MemoryBus.scala 42:32]
  wire [3:0] subsystem_mbus_xbar_auto_in_1_a_bits_source; // @[MemoryBus.scala 42:32]
  wire [35:0] subsystem_mbus_xbar_auto_in_1_a_bits_address; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_bufferable; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_modifiable; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_readalloc; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_writealloc; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_privileged; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_secure; // @[MemoryBus.scala 42:32]
  wire [15:0] subsystem_mbus_xbar_auto_in_1_a_bits_mask; // @[MemoryBus.scala 42:32]
  wire [127:0] subsystem_mbus_xbar_auto_in_1_a_bits_data; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_1_d_ready; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_1_d_valid; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_1_d_bits_opcode; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_1_d_bits_size; // @[MemoryBus.scala 42:32]
  wire [3:0] subsystem_mbus_xbar_auto_in_1_d_bits_source; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_1_d_bits_denied; // @[MemoryBus.scala 42:32]
  wire [127:0] subsystem_mbus_xbar_auto_in_1_d_bits_data; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_1_d_bits_corrupt; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_0_a_ready; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_0_a_valid; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_0_a_bits_opcode; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_0_a_bits_param; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_0_a_bits_size; // @[MemoryBus.scala 42:32]
  wire [3:0] subsystem_mbus_xbar_auto_in_0_a_bits_source; // @[MemoryBus.scala 42:32]
  wire [35:0] subsystem_mbus_xbar_auto_in_0_a_bits_address; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_bufferable; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_modifiable; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_readalloc; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_writealloc; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_privileged; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_secure; // @[MemoryBus.scala 42:32]
  wire [15:0] subsystem_mbus_xbar_auto_in_0_a_bits_mask; // @[MemoryBus.scala 42:32]
  wire [127:0] subsystem_mbus_xbar_auto_in_0_a_bits_data; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_0_d_ready; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_0_d_valid; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_0_d_bits_opcode; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_in_0_d_bits_size; // @[MemoryBus.scala 42:32]
  wire [3:0] subsystem_mbus_xbar_auto_in_0_d_bits_source; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_0_d_bits_denied; // @[MemoryBus.scala 42:32]
  wire [127:0] subsystem_mbus_xbar_auto_in_0_d_bits_data; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_in_0_d_bits_corrupt; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_1_a_ready; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_1_a_valid; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_out_1_a_bits_opcode; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_out_1_a_bits_param; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_out_1_a_bits_size; // @[MemoryBus.scala 42:32]
  wire [5:0] subsystem_mbus_xbar_auto_out_1_a_bits_source; // @[MemoryBus.scala 42:32]
  wire [35:0] subsystem_mbus_xbar_auto_out_1_a_bits_address; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_bufferable; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_modifiable; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_readalloc; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_writealloc; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_privileged; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_secure; // @[MemoryBus.scala 42:32]
  wire [15:0] subsystem_mbus_xbar_auto_out_1_a_bits_mask; // @[MemoryBus.scala 42:32]
  wire [127:0] subsystem_mbus_xbar_auto_out_1_a_bits_data; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_1_d_ready; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_1_d_valid; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_out_1_d_bits_opcode; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_out_1_d_bits_size; // @[MemoryBus.scala 42:32]
  wire [5:0] subsystem_mbus_xbar_auto_out_1_d_bits_source; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_1_d_bits_denied; // @[MemoryBus.scala 42:32]
  wire [127:0] subsystem_mbus_xbar_auto_out_1_d_bits_data; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_1_d_bits_corrupt; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_0_a_ready; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_0_a_valid; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_out_0_a_bits_opcode; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_out_0_a_bits_param; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_out_0_a_bits_size; // @[MemoryBus.scala 42:32]
  wire [5:0] subsystem_mbus_xbar_auto_out_0_a_bits_source; // @[MemoryBus.scala 42:32]
  wire [27:0] subsystem_mbus_xbar_auto_out_0_a_bits_address; // @[MemoryBus.scala 42:32]
  wire [15:0] subsystem_mbus_xbar_auto_out_0_a_bits_mask; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_0_d_ready; // @[MemoryBus.scala 42:32]
  wire  subsystem_mbus_xbar_auto_out_0_d_valid; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_out_0_d_bits_opcode; // @[MemoryBus.scala 42:32]
  wire [2:0] subsystem_mbus_xbar_auto_out_0_d_bits_size; // @[MemoryBus.scala 42:32]
  wire [5:0] subsystem_mbus_xbar_auto_out_0_d_bits_source; // @[MemoryBus.scala 42:32]
  wire  fixer_rf_reset; // @[FIFOFixer.scala 144:27]
  wire  fixer_clock; // @[FIFOFixer.scala 144:27]
  wire  fixer_reset; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_3_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_3_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_3_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_3_a_bits_param; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_3_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_3_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [35:0] fixer_auto_in_3_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_3_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_3_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_3_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_3_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_3_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_3_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_in_3_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_3_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_3_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_3_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_3_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_3_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_3_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_3_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_3_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_3_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_2_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_2_a_bits_param; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_2_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_2_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [35:0] fixer_auto_in_2_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_in_2_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_2_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_2_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_2_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_2_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_2_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_2_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_1_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_1_a_bits_param; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_1_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_1_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [35:0] fixer_auto_in_1_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_in_1_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_1_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_1_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_1_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_1_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_1_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_1_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_0_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_0_a_bits_param; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_0_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_0_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [35:0] fixer_auto_in_0_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_in_0_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_0_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_0_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_in_0_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_in_0_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_in_0_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_in_0_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_3_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_3_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_3_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_3_a_bits_param; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_3_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_3_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [35:0] fixer_auto_out_3_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_3_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_3_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_3_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_3_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_3_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_3_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_out_3_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_3_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_3_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_3_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_3_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_3_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_3_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_3_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_3_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_3_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_2_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_2_a_bits_param; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_2_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_2_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [35:0] fixer_auto_out_2_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_out_2_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_2_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_2_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_2_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_2_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_2_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_2_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_1_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_1_a_bits_param; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_1_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_1_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [35:0] fixer_auto_out_1_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_out_1_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_1_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_1_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_1_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_1_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_1_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_1_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_0_a_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_0_a_bits_param; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_0_a_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_0_a_bits_source; // @[FIFOFixer.scala 144:27]
  wire [35:0] fixer_auto_out_0_a_bits_address; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_bits_user_amba_prot_bufferable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_bits_user_amba_prot_modifiable; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_bits_user_amba_prot_readalloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_bits_user_amba_prot_writealloc; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_bits_user_amba_prot_privileged; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_a_bits_user_amba_prot_secure; // @[FIFOFixer.scala 144:27]
  wire [15:0] fixer_auto_out_0_a_bits_mask; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_0_a_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_d_ready; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_d_valid; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_0_d_bits_opcode; // @[FIFOFixer.scala 144:27]
  wire [2:0] fixer_auto_out_0_d_bits_size; // @[FIFOFixer.scala 144:27]
  wire [3:0] fixer_auto_out_0_d_bits_source; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_d_bits_denied; // @[FIFOFixer.scala 144:27]
  wire [127:0] fixer_auto_out_0_d_bits_data; // @[FIFOFixer.scala 144:27]
  wire  fixer_auto_out_0_d_bits_corrupt; // @[FIFOFixer.scala 144:27]
  wire  picker_rf_reset; // @[ProbePicker.scala 65:28]
  wire  picker_clock; // @[ProbePicker.scala 65:28]
  wire  picker_reset; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_1_a_ready; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_1_a_valid; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_in_1_a_bits_opcode; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_in_1_a_bits_param; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_in_1_a_bits_size; // @[ProbePicker.scala 65:28]
  wire [5:0] picker_auto_in_1_a_bits_source; // @[ProbePicker.scala 65:28]
  wire [35:0] picker_auto_in_1_a_bits_address; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_1_a_bits_user_amba_prot_bufferable; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_1_a_bits_user_amba_prot_modifiable; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_1_a_bits_user_amba_prot_readalloc; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_1_a_bits_user_amba_prot_writealloc; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_1_a_bits_user_amba_prot_privileged; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_1_a_bits_user_amba_prot_secure; // @[ProbePicker.scala 65:28]
  wire [15:0] picker_auto_in_1_a_bits_mask; // @[ProbePicker.scala 65:28]
  wire [127:0] picker_auto_in_1_a_bits_data; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_1_d_ready; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_1_d_valid; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_in_1_d_bits_opcode; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_in_1_d_bits_size; // @[ProbePicker.scala 65:28]
  wire [5:0] picker_auto_in_1_d_bits_source; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_1_d_bits_denied; // @[ProbePicker.scala 65:28]
  wire [127:0] picker_auto_in_1_d_bits_data; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_1_d_bits_corrupt; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_0_a_ready; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_0_a_valid; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_in_0_a_bits_opcode; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_in_0_a_bits_param; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_in_0_a_bits_size; // @[ProbePicker.scala 65:28]
  wire [5:0] picker_auto_in_0_a_bits_source; // @[ProbePicker.scala 65:28]
  wire [27:0] picker_auto_in_0_a_bits_address; // @[ProbePicker.scala 65:28]
  wire [15:0] picker_auto_in_0_a_bits_mask; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_0_d_ready; // @[ProbePicker.scala 65:28]
  wire  picker_auto_in_0_d_valid; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_in_0_d_bits_opcode; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_in_0_d_bits_size; // @[ProbePicker.scala 65:28]
  wire [5:0] picker_auto_in_0_d_bits_source; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_1_a_ready; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_1_a_valid; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_out_1_a_bits_opcode; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_out_1_a_bits_param; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_out_1_a_bits_size; // @[ProbePicker.scala 65:28]
  wire [5:0] picker_auto_out_1_a_bits_source; // @[ProbePicker.scala 65:28]
  wire [35:0] picker_auto_out_1_a_bits_address; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_1_a_bits_user_amba_prot_bufferable; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_1_a_bits_user_amba_prot_modifiable; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_1_a_bits_user_amba_prot_readalloc; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_1_a_bits_user_amba_prot_writealloc; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_1_a_bits_user_amba_prot_privileged; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_1_a_bits_user_amba_prot_secure; // @[ProbePicker.scala 65:28]
  wire [15:0] picker_auto_out_1_a_bits_mask; // @[ProbePicker.scala 65:28]
  wire [127:0] picker_auto_out_1_a_bits_data; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_1_d_ready; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_1_d_valid; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_out_1_d_bits_opcode; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_out_1_d_bits_size; // @[ProbePicker.scala 65:28]
  wire [5:0] picker_auto_out_1_d_bits_source; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_1_d_bits_denied; // @[ProbePicker.scala 65:28]
  wire [127:0] picker_auto_out_1_d_bits_data; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_1_d_bits_corrupt; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_0_a_ready; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_0_a_valid; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_out_0_a_bits_opcode; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_out_0_a_bits_param; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_out_0_a_bits_size; // @[ProbePicker.scala 65:28]
  wire [5:0] picker_auto_out_0_a_bits_source; // @[ProbePicker.scala 65:28]
  wire [27:0] picker_auto_out_0_a_bits_address; // @[ProbePicker.scala 65:28]
  wire [15:0] picker_auto_out_0_a_bits_mask; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_0_d_ready; // @[ProbePicker.scala 65:28]
  wire  picker_auto_out_0_d_valid; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_out_0_d_bits_opcode; // @[ProbePicker.scala 65:28]
  wire [2:0] picker_auto_out_0_d_bits_size; // @[ProbePicker.scala 65:28]
  wire [5:0] picker_auto_out_0_d_bits_source; // @[ProbePicker.scala 65:28]
  wire  wrapped_zero_device_rf_reset; // @[LazyModule.scala 524:27]
  wire  wrapped_zero_device_clock; // @[LazyModule.scala 524:27]
  wire  wrapped_zero_device_reset; // @[LazyModule.scala 524:27]
  wire  wrapped_zero_device_auto_buffer_in_a_ready; // @[LazyModule.scala 524:27]
  wire  wrapped_zero_device_auto_buffer_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] wrapped_zero_device_auto_buffer_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] wrapped_zero_device_auto_buffer_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [2:0] wrapped_zero_device_auto_buffer_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [5:0] wrapped_zero_device_auto_buffer_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [27:0] wrapped_zero_device_auto_buffer_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire [15:0] wrapped_zero_device_auto_buffer_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire  wrapped_zero_device_auto_buffer_in_d_ready; // @[LazyModule.scala 524:27]
  wire  wrapped_zero_device_auto_buffer_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] wrapped_zero_device_auto_buffer_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] wrapped_zero_device_auto_buffer_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [5:0] wrapped_zero_device_auto_buffer_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire  buffer_auto_in_3_a_ready;
  wire  buffer_auto_in_3_a_valid;
  wire [2:0] buffer_auto_in_3_a_bits_opcode;
  wire [2:0] buffer_auto_in_3_a_bits_param;
  wire [2:0] buffer_auto_in_3_a_bits_size;
  wire [3:0] buffer_auto_in_3_a_bits_source;
  wire [35:0] buffer_auto_in_3_a_bits_address;
  wire  buffer_auto_in_3_a_bits_user_amba_prot_bufferable;
  wire  buffer_auto_in_3_a_bits_user_amba_prot_modifiable;
  wire  buffer_auto_in_3_a_bits_user_amba_prot_readalloc;
  wire  buffer_auto_in_3_a_bits_user_amba_prot_writealloc;
  wire  buffer_auto_in_3_a_bits_user_amba_prot_privileged;
  wire  buffer_auto_in_3_a_bits_user_amba_prot_secure;
  wire [15:0] buffer_auto_in_3_a_bits_mask;
  wire [127:0] buffer_auto_in_3_a_bits_data;
  wire  buffer_auto_in_3_d_ready;
  wire  buffer_auto_in_3_d_valid;
  wire [2:0] buffer_auto_in_3_d_bits_opcode;
  wire [2:0] buffer_auto_in_3_d_bits_size;
  wire [3:0] buffer_auto_in_3_d_bits_source;
  wire  buffer_auto_in_3_d_bits_denied;
  wire [127:0] buffer_auto_in_3_d_bits_data;
  wire  buffer_auto_in_3_d_bits_corrupt;
  wire  buffer_auto_in_2_a_ready;
  wire  buffer_auto_in_2_a_valid;
  wire [2:0] buffer_auto_in_2_a_bits_opcode;
  wire [2:0] buffer_auto_in_2_a_bits_param;
  wire [2:0] buffer_auto_in_2_a_bits_size;
  wire [3:0] buffer_auto_in_2_a_bits_source;
  wire [35:0] buffer_auto_in_2_a_bits_address;
  wire  buffer_auto_in_2_a_bits_user_amba_prot_bufferable;
  wire  buffer_auto_in_2_a_bits_user_amba_prot_modifiable;
  wire  buffer_auto_in_2_a_bits_user_amba_prot_readalloc;
  wire  buffer_auto_in_2_a_bits_user_amba_prot_writealloc;
  wire  buffer_auto_in_2_a_bits_user_amba_prot_privileged;
  wire  buffer_auto_in_2_a_bits_user_amba_prot_secure;
  wire [15:0] buffer_auto_in_2_a_bits_mask;
  wire [127:0] buffer_auto_in_2_a_bits_data;
  wire  buffer_auto_in_2_d_ready;
  wire  buffer_auto_in_2_d_valid;
  wire [2:0] buffer_auto_in_2_d_bits_opcode;
  wire [2:0] buffer_auto_in_2_d_bits_size;
  wire [3:0] buffer_auto_in_2_d_bits_source;
  wire  buffer_auto_in_2_d_bits_denied;
  wire [127:0] buffer_auto_in_2_d_bits_data;
  wire  buffer_auto_in_2_d_bits_corrupt;
  wire  buffer_auto_in_1_a_ready;
  wire  buffer_auto_in_1_a_valid;
  wire [2:0] buffer_auto_in_1_a_bits_opcode;
  wire [2:0] buffer_auto_in_1_a_bits_param;
  wire [2:0] buffer_auto_in_1_a_bits_size;
  wire [3:0] buffer_auto_in_1_a_bits_source;
  wire [35:0] buffer_auto_in_1_a_bits_address;
  wire  buffer_auto_in_1_a_bits_user_amba_prot_bufferable;
  wire  buffer_auto_in_1_a_bits_user_amba_prot_modifiable;
  wire  buffer_auto_in_1_a_bits_user_amba_prot_readalloc;
  wire  buffer_auto_in_1_a_bits_user_amba_prot_writealloc;
  wire  buffer_auto_in_1_a_bits_user_amba_prot_privileged;
  wire  buffer_auto_in_1_a_bits_user_amba_prot_secure;
  wire [15:0] buffer_auto_in_1_a_bits_mask;
  wire [127:0] buffer_auto_in_1_a_bits_data;
  wire  buffer_auto_in_1_d_ready;
  wire  buffer_auto_in_1_d_valid;
  wire [2:0] buffer_auto_in_1_d_bits_opcode;
  wire [2:0] buffer_auto_in_1_d_bits_size;
  wire [3:0] buffer_auto_in_1_d_bits_source;
  wire  buffer_auto_in_1_d_bits_denied;
  wire [127:0] buffer_auto_in_1_d_bits_data;
  wire  buffer_auto_in_1_d_bits_corrupt;
  wire  buffer_auto_in_0_a_ready;
  wire  buffer_auto_in_0_a_valid;
  wire [2:0] buffer_auto_in_0_a_bits_opcode;
  wire [2:0] buffer_auto_in_0_a_bits_param;
  wire [2:0] buffer_auto_in_0_a_bits_size;
  wire [3:0] buffer_auto_in_0_a_bits_source;
  wire [35:0] buffer_auto_in_0_a_bits_address;
  wire  buffer_auto_in_0_a_bits_user_amba_prot_bufferable;
  wire  buffer_auto_in_0_a_bits_user_amba_prot_modifiable;
  wire  buffer_auto_in_0_a_bits_user_amba_prot_readalloc;
  wire  buffer_auto_in_0_a_bits_user_amba_prot_writealloc;
  wire  buffer_auto_in_0_a_bits_user_amba_prot_privileged;
  wire  buffer_auto_in_0_a_bits_user_amba_prot_secure;
  wire [15:0] buffer_auto_in_0_a_bits_mask;
  wire [127:0] buffer_auto_in_0_a_bits_data;
  wire  buffer_auto_in_0_d_ready;
  wire  buffer_auto_in_0_d_valid;
  wire [2:0] buffer_auto_in_0_d_bits_opcode;
  wire [2:0] buffer_auto_in_0_d_bits_size;
  wire [3:0] buffer_auto_in_0_d_bits_source;
  wire  buffer_auto_in_0_d_bits_denied;
  wire [127:0] buffer_auto_in_0_d_bits_data;
  wire  buffer_auto_in_0_d_bits_corrupt;
  wire  buffer_auto_out_3_a_ready;
  wire  buffer_auto_out_3_a_valid;
  wire [2:0] buffer_auto_out_3_a_bits_opcode;
  wire [2:0] buffer_auto_out_3_a_bits_param;
  wire [2:0] buffer_auto_out_3_a_bits_size;
  wire [3:0] buffer_auto_out_3_a_bits_source;
  wire [35:0] buffer_auto_out_3_a_bits_address;
  wire  buffer_auto_out_3_a_bits_user_amba_prot_bufferable;
  wire  buffer_auto_out_3_a_bits_user_amba_prot_modifiable;
  wire  buffer_auto_out_3_a_bits_user_amba_prot_readalloc;
  wire  buffer_auto_out_3_a_bits_user_amba_prot_writealloc;
  wire  buffer_auto_out_3_a_bits_user_amba_prot_privileged;
  wire  buffer_auto_out_3_a_bits_user_amba_prot_secure;
  wire [15:0] buffer_auto_out_3_a_bits_mask;
  wire [127:0] buffer_auto_out_3_a_bits_data;
  wire  buffer_auto_out_3_d_ready;
  wire  buffer_auto_out_3_d_valid;
  wire [2:0] buffer_auto_out_3_d_bits_opcode;
  wire [2:0] buffer_auto_out_3_d_bits_size;
  wire [3:0] buffer_auto_out_3_d_bits_source;
  wire  buffer_auto_out_3_d_bits_denied;
  wire [127:0] buffer_auto_out_3_d_bits_data;
  wire  buffer_auto_out_3_d_bits_corrupt;
  wire  buffer_auto_out_2_a_ready;
  wire  buffer_auto_out_2_a_valid;
  wire [2:0] buffer_auto_out_2_a_bits_opcode;
  wire [2:0] buffer_auto_out_2_a_bits_param;
  wire [2:0] buffer_auto_out_2_a_bits_size;
  wire [3:0] buffer_auto_out_2_a_bits_source;
  wire [35:0] buffer_auto_out_2_a_bits_address;
  wire  buffer_auto_out_2_a_bits_user_amba_prot_bufferable;
  wire  buffer_auto_out_2_a_bits_user_amba_prot_modifiable;
  wire  buffer_auto_out_2_a_bits_user_amba_prot_readalloc;
  wire  buffer_auto_out_2_a_bits_user_amba_prot_writealloc;
  wire  buffer_auto_out_2_a_bits_user_amba_prot_privileged;
  wire  buffer_auto_out_2_a_bits_user_amba_prot_secure;
  wire [15:0] buffer_auto_out_2_a_bits_mask;
  wire [127:0] buffer_auto_out_2_a_bits_data;
  wire  buffer_auto_out_2_d_ready;
  wire  buffer_auto_out_2_d_valid;
  wire [2:0] buffer_auto_out_2_d_bits_opcode;
  wire [2:0] buffer_auto_out_2_d_bits_size;
  wire [3:0] buffer_auto_out_2_d_bits_source;
  wire  buffer_auto_out_2_d_bits_denied;
  wire [127:0] buffer_auto_out_2_d_bits_data;
  wire  buffer_auto_out_2_d_bits_corrupt;
  wire  buffer_auto_out_1_a_ready;
  wire  buffer_auto_out_1_a_valid;
  wire [2:0] buffer_auto_out_1_a_bits_opcode;
  wire [2:0] buffer_auto_out_1_a_bits_param;
  wire [2:0] buffer_auto_out_1_a_bits_size;
  wire [3:0] buffer_auto_out_1_a_bits_source;
  wire [35:0] buffer_auto_out_1_a_bits_address;
  wire  buffer_auto_out_1_a_bits_user_amba_prot_bufferable;
  wire  buffer_auto_out_1_a_bits_user_amba_prot_modifiable;
  wire  buffer_auto_out_1_a_bits_user_amba_prot_readalloc;
  wire  buffer_auto_out_1_a_bits_user_amba_prot_writealloc;
  wire  buffer_auto_out_1_a_bits_user_amba_prot_privileged;
  wire  buffer_auto_out_1_a_bits_user_amba_prot_secure;
  wire [15:0] buffer_auto_out_1_a_bits_mask;
  wire [127:0] buffer_auto_out_1_a_bits_data;
  wire  buffer_auto_out_1_d_ready;
  wire  buffer_auto_out_1_d_valid;
  wire [2:0] buffer_auto_out_1_d_bits_opcode;
  wire [2:0] buffer_auto_out_1_d_bits_size;
  wire [3:0] buffer_auto_out_1_d_bits_source;
  wire  buffer_auto_out_1_d_bits_denied;
  wire [127:0] buffer_auto_out_1_d_bits_data;
  wire  buffer_auto_out_1_d_bits_corrupt;
  wire  buffer_auto_out_0_a_ready;
  wire  buffer_auto_out_0_a_valid;
  wire [2:0] buffer_auto_out_0_a_bits_opcode;
  wire [2:0] buffer_auto_out_0_a_bits_param;
  wire [2:0] buffer_auto_out_0_a_bits_size;
  wire [3:0] buffer_auto_out_0_a_bits_source;
  wire [35:0] buffer_auto_out_0_a_bits_address;
  wire  buffer_auto_out_0_a_bits_user_amba_prot_bufferable;
  wire  buffer_auto_out_0_a_bits_user_amba_prot_modifiable;
  wire  buffer_auto_out_0_a_bits_user_amba_prot_readalloc;
  wire  buffer_auto_out_0_a_bits_user_amba_prot_writealloc;
  wire  buffer_auto_out_0_a_bits_user_amba_prot_privileged;
  wire  buffer_auto_out_0_a_bits_user_amba_prot_secure;
  wire [15:0] buffer_auto_out_0_a_bits_mask;
  wire [127:0] buffer_auto_out_0_a_bits_data;
  wire  buffer_auto_out_0_d_ready;
  wire  buffer_auto_out_0_d_valid;
  wire [2:0] buffer_auto_out_0_d_bits_opcode;
  wire [2:0] buffer_auto_out_0_d_bits_size;
  wire [3:0] buffer_auto_out_0_d_bits_source;
  wire  buffer_auto_out_0_d_bits_denied;
  wire [127:0] buffer_auto_out_0_d_bits_data;
  wire  buffer_auto_out_0_d_bits_corrupt;
  wire  coupler_to_memory_controller_named_axi4_mem_port_rf_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_clock; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_reset; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_param; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_size; // @[LazyModule.scala 524:27]
  wire [5:0] coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_source; // @[LazyModule.scala 524:27]
  wire [35:0] coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_address; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_secure; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_mask; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_valid; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_opcode; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_size; // @[LazyModule.scala 524:27]
  wire [5:0] coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_source; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_denied; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_data; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_corrupt; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_valid; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_id; // @[LazyModule.scala 524:27]
  wire [35:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_addr; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_len; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_size; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_cache; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_prot; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_w_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_w_valid; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_w_bits_data; // @[LazyModule.scala 524:27]
  wire [15:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_w_bits_strb; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_w_bits_last; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_b_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_b_valid; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_b_bits_id; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_b_bits_resp; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_valid; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_id; // @[LazyModule.scala 524:27]
  wire [35:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_addr; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_len; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_size; // @[LazyModule.scala 524:27]
  wire [3:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_cache; // @[LazyModule.scala 524:27]
  wire [2:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_prot; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_ready; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_valid; // @[LazyModule.scala 524:27]
  wire [7:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_bits_id; // @[LazyModule.scala 524:27]
  wire [127:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_bits_data; // @[LazyModule.scala 524:27]
  wire [1:0] coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_bits_resp; // @[LazyModule.scala 524:27]
  wire  coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_bits_last; // @[LazyModule.scala 524:27]
  RHEA__TLXbar_6 subsystem_mbus_xbar ( // @[MemoryBus.scala 42:32]
    .rf_reset(subsystem_mbus_xbar_rf_reset),
    .clock(subsystem_mbus_xbar_clock),
    .reset(subsystem_mbus_xbar_reset),
    .auto_in_3_a_ready(subsystem_mbus_xbar_auto_in_3_a_ready),
    .auto_in_3_a_valid(subsystem_mbus_xbar_auto_in_3_a_valid),
    .auto_in_3_a_bits_opcode(subsystem_mbus_xbar_auto_in_3_a_bits_opcode),
    .auto_in_3_a_bits_param(subsystem_mbus_xbar_auto_in_3_a_bits_param),
    .auto_in_3_a_bits_size(subsystem_mbus_xbar_auto_in_3_a_bits_size),
    .auto_in_3_a_bits_source(subsystem_mbus_xbar_auto_in_3_a_bits_source),
    .auto_in_3_a_bits_address(subsystem_mbus_xbar_auto_in_3_a_bits_address),
    .auto_in_3_a_bits_user_amba_prot_bufferable(subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_bufferable),
    .auto_in_3_a_bits_user_amba_prot_modifiable(subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_modifiable),
    .auto_in_3_a_bits_user_amba_prot_readalloc(subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_readalloc),
    .auto_in_3_a_bits_user_amba_prot_writealloc(subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_writealloc),
    .auto_in_3_a_bits_user_amba_prot_privileged(subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_privileged),
    .auto_in_3_a_bits_user_amba_prot_secure(subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_secure),
    .auto_in_3_a_bits_mask(subsystem_mbus_xbar_auto_in_3_a_bits_mask),
    .auto_in_3_a_bits_data(subsystem_mbus_xbar_auto_in_3_a_bits_data),
    .auto_in_3_d_ready(subsystem_mbus_xbar_auto_in_3_d_ready),
    .auto_in_3_d_valid(subsystem_mbus_xbar_auto_in_3_d_valid),
    .auto_in_3_d_bits_opcode(subsystem_mbus_xbar_auto_in_3_d_bits_opcode),
    .auto_in_3_d_bits_size(subsystem_mbus_xbar_auto_in_3_d_bits_size),
    .auto_in_3_d_bits_source(subsystem_mbus_xbar_auto_in_3_d_bits_source),
    .auto_in_3_d_bits_denied(subsystem_mbus_xbar_auto_in_3_d_bits_denied),
    .auto_in_3_d_bits_data(subsystem_mbus_xbar_auto_in_3_d_bits_data),
    .auto_in_3_d_bits_corrupt(subsystem_mbus_xbar_auto_in_3_d_bits_corrupt),
    .auto_in_2_a_ready(subsystem_mbus_xbar_auto_in_2_a_ready),
    .auto_in_2_a_valid(subsystem_mbus_xbar_auto_in_2_a_valid),
    .auto_in_2_a_bits_opcode(subsystem_mbus_xbar_auto_in_2_a_bits_opcode),
    .auto_in_2_a_bits_param(subsystem_mbus_xbar_auto_in_2_a_bits_param),
    .auto_in_2_a_bits_size(subsystem_mbus_xbar_auto_in_2_a_bits_size),
    .auto_in_2_a_bits_source(subsystem_mbus_xbar_auto_in_2_a_bits_source),
    .auto_in_2_a_bits_address(subsystem_mbus_xbar_auto_in_2_a_bits_address),
    .auto_in_2_a_bits_user_amba_prot_bufferable(subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_bufferable),
    .auto_in_2_a_bits_user_amba_prot_modifiable(subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_modifiable),
    .auto_in_2_a_bits_user_amba_prot_readalloc(subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_readalloc),
    .auto_in_2_a_bits_user_amba_prot_writealloc(subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_writealloc),
    .auto_in_2_a_bits_user_amba_prot_privileged(subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_privileged),
    .auto_in_2_a_bits_user_amba_prot_secure(subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_secure),
    .auto_in_2_a_bits_mask(subsystem_mbus_xbar_auto_in_2_a_bits_mask),
    .auto_in_2_a_bits_data(subsystem_mbus_xbar_auto_in_2_a_bits_data),
    .auto_in_2_d_ready(subsystem_mbus_xbar_auto_in_2_d_ready),
    .auto_in_2_d_valid(subsystem_mbus_xbar_auto_in_2_d_valid),
    .auto_in_2_d_bits_opcode(subsystem_mbus_xbar_auto_in_2_d_bits_opcode),
    .auto_in_2_d_bits_size(subsystem_mbus_xbar_auto_in_2_d_bits_size),
    .auto_in_2_d_bits_source(subsystem_mbus_xbar_auto_in_2_d_bits_source),
    .auto_in_2_d_bits_denied(subsystem_mbus_xbar_auto_in_2_d_bits_denied),
    .auto_in_2_d_bits_data(subsystem_mbus_xbar_auto_in_2_d_bits_data),
    .auto_in_2_d_bits_corrupt(subsystem_mbus_xbar_auto_in_2_d_bits_corrupt),
    .auto_in_1_a_ready(subsystem_mbus_xbar_auto_in_1_a_ready),
    .auto_in_1_a_valid(subsystem_mbus_xbar_auto_in_1_a_valid),
    .auto_in_1_a_bits_opcode(subsystem_mbus_xbar_auto_in_1_a_bits_opcode),
    .auto_in_1_a_bits_param(subsystem_mbus_xbar_auto_in_1_a_bits_param),
    .auto_in_1_a_bits_size(subsystem_mbus_xbar_auto_in_1_a_bits_size),
    .auto_in_1_a_bits_source(subsystem_mbus_xbar_auto_in_1_a_bits_source),
    .auto_in_1_a_bits_address(subsystem_mbus_xbar_auto_in_1_a_bits_address),
    .auto_in_1_a_bits_user_amba_prot_bufferable(subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_bufferable),
    .auto_in_1_a_bits_user_amba_prot_modifiable(subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_modifiable),
    .auto_in_1_a_bits_user_amba_prot_readalloc(subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_readalloc),
    .auto_in_1_a_bits_user_amba_prot_writealloc(subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_writealloc),
    .auto_in_1_a_bits_user_amba_prot_privileged(subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_privileged),
    .auto_in_1_a_bits_user_amba_prot_secure(subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_secure),
    .auto_in_1_a_bits_mask(subsystem_mbus_xbar_auto_in_1_a_bits_mask),
    .auto_in_1_a_bits_data(subsystem_mbus_xbar_auto_in_1_a_bits_data),
    .auto_in_1_d_ready(subsystem_mbus_xbar_auto_in_1_d_ready),
    .auto_in_1_d_valid(subsystem_mbus_xbar_auto_in_1_d_valid),
    .auto_in_1_d_bits_opcode(subsystem_mbus_xbar_auto_in_1_d_bits_opcode),
    .auto_in_1_d_bits_size(subsystem_mbus_xbar_auto_in_1_d_bits_size),
    .auto_in_1_d_bits_source(subsystem_mbus_xbar_auto_in_1_d_bits_source),
    .auto_in_1_d_bits_denied(subsystem_mbus_xbar_auto_in_1_d_bits_denied),
    .auto_in_1_d_bits_data(subsystem_mbus_xbar_auto_in_1_d_bits_data),
    .auto_in_1_d_bits_corrupt(subsystem_mbus_xbar_auto_in_1_d_bits_corrupt),
    .auto_in_0_a_ready(subsystem_mbus_xbar_auto_in_0_a_ready),
    .auto_in_0_a_valid(subsystem_mbus_xbar_auto_in_0_a_valid),
    .auto_in_0_a_bits_opcode(subsystem_mbus_xbar_auto_in_0_a_bits_opcode),
    .auto_in_0_a_bits_param(subsystem_mbus_xbar_auto_in_0_a_bits_param),
    .auto_in_0_a_bits_size(subsystem_mbus_xbar_auto_in_0_a_bits_size),
    .auto_in_0_a_bits_source(subsystem_mbus_xbar_auto_in_0_a_bits_source),
    .auto_in_0_a_bits_address(subsystem_mbus_xbar_auto_in_0_a_bits_address),
    .auto_in_0_a_bits_user_amba_prot_bufferable(subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_bufferable),
    .auto_in_0_a_bits_user_amba_prot_modifiable(subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_modifiable),
    .auto_in_0_a_bits_user_amba_prot_readalloc(subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_readalloc),
    .auto_in_0_a_bits_user_amba_prot_writealloc(subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_writealloc),
    .auto_in_0_a_bits_user_amba_prot_privileged(subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_privileged),
    .auto_in_0_a_bits_user_amba_prot_secure(subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_secure),
    .auto_in_0_a_bits_mask(subsystem_mbus_xbar_auto_in_0_a_bits_mask),
    .auto_in_0_a_bits_data(subsystem_mbus_xbar_auto_in_0_a_bits_data),
    .auto_in_0_d_ready(subsystem_mbus_xbar_auto_in_0_d_ready),
    .auto_in_0_d_valid(subsystem_mbus_xbar_auto_in_0_d_valid),
    .auto_in_0_d_bits_opcode(subsystem_mbus_xbar_auto_in_0_d_bits_opcode),
    .auto_in_0_d_bits_size(subsystem_mbus_xbar_auto_in_0_d_bits_size),
    .auto_in_0_d_bits_source(subsystem_mbus_xbar_auto_in_0_d_bits_source),
    .auto_in_0_d_bits_denied(subsystem_mbus_xbar_auto_in_0_d_bits_denied),
    .auto_in_0_d_bits_data(subsystem_mbus_xbar_auto_in_0_d_bits_data),
    .auto_in_0_d_bits_corrupt(subsystem_mbus_xbar_auto_in_0_d_bits_corrupt),
    .auto_out_1_a_ready(subsystem_mbus_xbar_auto_out_1_a_ready),
    .auto_out_1_a_valid(subsystem_mbus_xbar_auto_out_1_a_valid),
    .auto_out_1_a_bits_opcode(subsystem_mbus_xbar_auto_out_1_a_bits_opcode),
    .auto_out_1_a_bits_param(subsystem_mbus_xbar_auto_out_1_a_bits_param),
    .auto_out_1_a_bits_size(subsystem_mbus_xbar_auto_out_1_a_bits_size),
    .auto_out_1_a_bits_source(subsystem_mbus_xbar_auto_out_1_a_bits_source),
    .auto_out_1_a_bits_address(subsystem_mbus_xbar_auto_out_1_a_bits_address),
    .auto_out_1_a_bits_user_amba_prot_bufferable(subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_bufferable),
    .auto_out_1_a_bits_user_amba_prot_modifiable(subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_modifiable),
    .auto_out_1_a_bits_user_amba_prot_readalloc(subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_readalloc),
    .auto_out_1_a_bits_user_amba_prot_writealloc(subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_writealloc),
    .auto_out_1_a_bits_user_amba_prot_privileged(subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_privileged),
    .auto_out_1_a_bits_user_amba_prot_secure(subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_secure),
    .auto_out_1_a_bits_mask(subsystem_mbus_xbar_auto_out_1_a_bits_mask),
    .auto_out_1_a_bits_data(subsystem_mbus_xbar_auto_out_1_a_bits_data),
    .auto_out_1_d_ready(subsystem_mbus_xbar_auto_out_1_d_ready),
    .auto_out_1_d_valid(subsystem_mbus_xbar_auto_out_1_d_valid),
    .auto_out_1_d_bits_opcode(subsystem_mbus_xbar_auto_out_1_d_bits_opcode),
    .auto_out_1_d_bits_size(subsystem_mbus_xbar_auto_out_1_d_bits_size),
    .auto_out_1_d_bits_source(subsystem_mbus_xbar_auto_out_1_d_bits_source),
    .auto_out_1_d_bits_denied(subsystem_mbus_xbar_auto_out_1_d_bits_denied),
    .auto_out_1_d_bits_data(subsystem_mbus_xbar_auto_out_1_d_bits_data),
    .auto_out_1_d_bits_corrupt(subsystem_mbus_xbar_auto_out_1_d_bits_corrupt),
    .auto_out_0_a_ready(subsystem_mbus_xbar_auto_out_0_a_ready),
    .auto_out_0_a_valid(subsystem_mbus_xbar_auto_out_0_a_valid),
    .auto_out_0_a_bits_opcode(subsystem_mbus_xbar_auto_out_0_a_bits_opcode),
    .auto_out_0_a_bits_param(subsystem_mbus_xbar_auto_out_0_a_bits_param),
    .auto_out_0_a_bits_size(subsystem_mbus_xbar_auto_out_0_a_bits_size),
    .auto_out_0_a_bits_source(subsystem_mbus_xbar_auto_out_0_a_bits_source),
    .auto_out_0_a_bits_address(subsystem_mbus_xbar_auto_out_0_a_bits_address),
    .auto_out_0_a_bits_mask(subsystem_mbus_xbar_auto_out_0_a_bits_mask),
    .auto_out_0_d_ready(subsystem_mbus_xbar_auto_out_0_d_ready),
    .auto_out_0_d_valid(subsystem_mbus_xbar_auto_out_0_d_valid),
    .auto_out_0_d_bits_opcode(subsystem_mbus_xbar_auto_out_0_d_bits_opcode),
    .auto_out_0_d_bits_size(subsystem_mbus_xbar_auto_out_0_d_bits_size),
    .auto_out_0_d_bits_source(subsystem_mbus_xbar_auto_out_0_d_bits_source)
  );
  RHEA__TLFIFOFixer_5 fixer ( // @[FIFOFixer.scala 144:27]
    .rf_reset(fixer_rf_reset),
    .clock(fixer_clock),
    .reset(fixer_reset),
    .auto_in_3_a_ready(fixer_auto_in_3_a_ready),
    .auto_in_3_a_valid(fixer_auto_in_3_a_valid),
    .auto_in_3_a_bits_opcode(fixer_auto_in_3_a_bits_opcode),
    .auto_in_3_a_bits_param(fixer_auto_in_3_a_bits_param),
    .auto_in_3_a_bits_size(fixer_auto_in_3_a_bits_size),
    .auto_in_3_a_bits_source(fixer_auto_in_3_a_bits_source),
    .auto_in_3_a_bits_address(fixer_auto_in_3_a_bits_address),
    .auto_in_3_a_bits_user_amba_prot_bufferable(fixer_auto_in_3_a_bits_user_amba_prot_bufferable),
    .auto_in_3_a_bits_user_amba_prot_modifiable(fixer_auto_in_3_a_bits_user_amba_prot_modifiable),
    .auto_in_3_a_bits_user_amba_prot_readalloc(fixer_auto_in_3_a_bits_user_amba_prot_readalloc),
    .auto_in_3_a_bits_user_amba_prot_writealloc(fixer_auto_in_3_a_bits_user_amba_prot_writealloc),
    .auto_in_3_a_bits_user_amba_prot_privileged(fixer_auto_in_3_a_bits_user_amba_prot_privileged),
    .auto_in_3_a_bits_user_amba_prot_secure(fixer_auto_in_3_a_bits_user_amba_prot_secure),
    .auto_in_3_a_bits_mask(fixer_auto_in_3_a_bits_mask),
    .auto_in_3_a_bits_data(fixer_auto_in_3_a_bits_data),
    .auto_in_3_d_ready(fixer_auto_in_3_d_ready),
    .auto_in_3_d_valid(fixer_auto_in_3_d_valid),
    .auto_in_3_d_bits_opcode(fixer_auto_in_3_d_bits_opcode),
    .auto_in_3_d_bits_size(fixer_auto_in_3_d_bits_size),
    .auto_in_3_d_bits_source(fixer_auto_in_3_d_bits_source),
    .auto_in_3_d_bits_denied(fixer_auto_in_3_d_bits_denied),
    .auto_in_3_d_bits_data(fixer_auto_in_3_d_bits_data),
    .auto_in_3_d_bits_corrupt(fixer_auto_in_3_d_bits_corrupt),
    .auto_in_2_a_ready(fixer_auto_in_2_a_ready),
    .auto_in_2_a_valid(fixer_auto_in_2_a_valid),
    .auto_in_2_a_bits_opcode(fixer_auto_in_2_a_bits_opcode),
    .auto_in_2_a_bits_param(fixer_auto_in_2_a_bits_param),
    .auto_in_2_a_bits_size(fixer_auto_in_2_a_bits_size),
    .auto_in_2_a_bits_source(fixer_auto_in_2_a_bits_source),
    .auto_in_2_a_bits_address(fixer_auto_in_2_a_bits_address),
    .auto_in_2_a_bits_user_amba_prot_bufferable(fixer_auto_in_2_a_bits_user_amba_prot_bufferable),
    .auto_in_2_a_bits_user_amba_prot_modifiable(fixer_auto_in_2_a_bits_user_amba_prot_modifiable),
    .auto_in_2_a_bits_user_amba_prot_readalloc(fixer_auto_in_2_a_bits_user_amba_prot_readalloc),
    .auto_in_2_a_bits_user_amba_prot_writealloc(fixer_auto_in_2_a_bits_user_amba_prot_writealloc),
    .auto_in_2_a_bits_user_amba_prot_privileged(fixer_auto_in_2_a_bits_user_amba_prot_privileged),
    .auto_in_2_a_bits_user_amba_prot_secure(fixer_auto_in_2_a_bits_user_amba_prot_secure),
    .auto_in_2_a_bits_mask(fixer_auto_in_2_a_bits_mask),
    .auto_in_2_a_bits_data(fixer_auto_in_2_a_bits_data),
    .auto_in_2_d_ready(fixer_auto_in_2_d_ready),
    .auto_in_2_d_valid(fixer_auto_in_2_d_valid),
    .auto_in_2_d_bits_opcode(fixer_auto_in_2_d_bits_opcode),
    .auto_in_2_d_bits_size(fixer_auto_in_2_d_bits_size),
    .auto_in_2_d_bits_source(fixer_auto_in_2_d_bits_source),
    .auto_in_2_d_bits_denied(fixer_auto_in_2_d_bits_denied),
    .auto_in_2_d_bits_data(fixer_auto_in_2_d_bits_data),
    .auto_in_2_d_bits_corrupt(fixer_auto_in_2_d_bits_corrupt),
    .auto_in_1_a_ready(fixer_auto_in_1_a_ready),
    .auto_in_1_a_valid(fixer_auto_in_1_a_valid),
    .auto_in_1_a_bits_opcode(fixer_auto_in_1_a_bits_opcode),
    .auto_in_1_a_bits_param(fixer_auto_in_1_a_bits_param),
    .auto_in_1_a_bits_size(fixer_auto_in_1_a_bits_size),
    .auto_in_1_a_bits_source(fixer_auto_in_1_a_bits_source),
    .auto_in_1_a_bits_address(fixer_auto_in_1_a_bits_address),
    .auto_in_1_a_bits_user_amba_prot_bufferable(fixer_auto_in_1_a_bits_user_amba_prot_bufferable),
    .auto_in_1_a_bits_user_amba_prot_modifiable(fixer_auto_in_1_a_bits_user_amba_prot_modifiable),
    .auto_in_1_a_bits_user_amba_prot_readalloc(fixer_auto_in_1_a_bits_user_amba_prot_readalloc),
    .auto_in_1_a_bits_user_amba_prot_writealloc(fixer_auto_in_1_a_bits_user_amba_prot_writealloc),
    .auto_in_1_a_bits_user_amba_prot_privileged(fixer_auto_in_1_a_bits_user_amba_prot_privileged),
    .auto_in_1_a_bits_user_amba_prot_secure(fixer_auto_in_1_a_bits_user_amba_prot_secure),
    .auto_in_1_a_bits_mask(fixer_auto_in_1_a_bits_mask),
    .auto_in_1_a_bits_data(fixer_auto_in_1_a_bits_data),
    .auto_in_1_d_ready(fixer_auto_in_1_d_ready),
    .auto_in_1_d_valid(fixer_auto_in_1_d_valid),
    .auto_in_1_d_bits_opcode(fixer_auto_in_1_d_bits_opcode),
    .auto_in_1_d_bits_size(fixer_auto_in_1_d_bits_size),
    .auto_in_1_d_bits_source(fixer_auto_in_1_d_bits_source),
    .auto_in_1_d_bits_denied(fixer_auto_in_1_d_bits_denied),
    .auto_in_1_d_bits_data(fixer_auto_in_1_d_bits_data),
    .auto_in_1_d_bits_corrupt(fixer_auto_in_1_d_bits_corrupt),
    .auto_in_0_a_ready(fixer_auto_in_0_a_ready),
    .auto_in_0_a_valid(fixer_auto_in_0_a_valid),
    .auto_in_0_a_bits_opcode(fixer_auto_in_0_a_bits_opcode),
    .auto_in_0_a_bits_param(fixer_auto_in_0_a_bits_param),
    .auto_in_0_a_bits_size(fixer_auto_in_0_a_bits_size),
    .auto_in_0_a_bits_source(fixer_auto_in_0_a_bits_source),
    .auto_in_0_a_bits_address(fixer_auto_in_0_a_bits_address),
    .auto_in_0_a_bits_user_amba_prot_bufferable(fixer_auto_in_0_a_bits_user_amba_prot_bufferable),
    .auto_in_0_a_bits_user_amba_prot_modifiable(fixer_auto_in_0_a_bits_user_amba_prot_modifiable),
    .auto_in_0_a_bits_user_amba_prot_readalloc(fixer_auto_in_0_a_bits_user_amba_prot_readalloc),
    .auto_in_0_a_bits_user_amba_prot_writealloc(fixer_auto_in_0_a_bits_user_amba_prot_writealloc),
    .auto_in_0_a_bits_user_amba_prot_privileged(fixer_auto_in_0_a_bits_user_amba_prot_privileged),
    .auto_in_0_a_bits_user_amba_prot_secure(fixer_auto_in_0_a_bits_user_amba_prot_secure),
    .auto_in_0_a_bits_mask(fixer_auto_in_0_a_bits_mask),
    .auto_in_0_a_bits_data(fixer_auto_in_0_a_bits_data),
    .auto_in_0_d_ready(fixer_auto_in_0_d_ready),
    .auto_in_0_d_valid(fixer_auto_in_0_d_valid),
    .auto_in_0_d_bits_opcode(fixer_auto_in_0_d_bits_opcode),
    .auto_in_0_d_bits_size(fixer_auto_in_0_d_bits_size),
    .auto_in_0_d_bits_source(fixer_auto_in_0_d_bits_source),
    .auto_in_0_d_bits_denied(fixer_auto_in_0_d_bits_denied),
    .auto_in_0_d_bits_data(fixer_auto_in_0_d_bits_data),
    .auto_in_0_d_bits_corrupt(fixer_auto_in_0_d_bits_corrupt),
    .auto_out_3_a_ready(fixer_auto_out_3_a_ready),
    .auto_out_3_a_valid(fixer_auto_out_3_a_valid),
    .auto_out_3_a_bits_opcode(fixer_auto_out_3_a_bits_opcode),
    .auto_out_3_a_bits_param(fixer_auto_out_3_a_bits_param),
    .auto_out_3_a_bits_size(fixer_auto_out_3_a_bits_size),
    .auto_out_3_a_bits_source(fixer_auto_out_3_a_bits_source),
    .auto_out_3_a_bits_address(fixer_auto_out_3_a_bits_address),
    .auto_out_3_a_bits_user_amba_prot_bufferable(fixer_auto_out_3_a_bits_user_amba_prot_bufferable),
    .auto_out_3_a_bits_user_amba_prot_modifiable(fixer_auto_out_3_a_bits_user_amba_prot_modifiable),
    .auto_out_3_a_bits_user_amba_prot_readalloc(fixer_auto_out_3_a_bits_user_amba_prot_readalloc),
    .auto_out_3_a_bits_user_amba_prot_writealloc(fixer_auto_out_3_a_bits_user_amba_prot_writealloc),
    .auto_out_3_a_bits_user_amba_prot_privileged(fixer_auto_out_3_a_bits_user_amba_prot_privileged),
    .auto_out_3_a_bits_user_amba_prot_secure(fixer_auto_out_3_a_bits_user_amba_prot_secure),
    .auto_out_3_a_bits_mask(fixer_auto_out_3_a_bits_mask),
    .auto_out_3_a_bits_data(fixer_auto_out_3_a_bits_data),
    .auto_out_3_d_ready(fixer_auto_out_3_d_ready),
    .auto_out_3_d_valid(fixer_auto_out_3_d_valid),
    .auto_out_3_d_bits_opcode(fixer_auto_out_3_d_bits_opcode),
    .auto_out_3_d_bits_size(fixer_auto_out_3_d_bits_size),
    .auto_out_3_d_bits_source(fixer_auto_out_3_d_bits_source),
    .auto_out_3_d_bits_denied(fixer_auto_out_3_d_bits_denied),
    .auto_out_3_d_bits_data(fixer_auto_out_3_d_bits_data),
    .auto_out_3_d_bits_corrupt(fixer_auto_out_3_d_bits_corrupt),
    .auto_out_2_a_ready(fixer_auto_out_2_a_ready),
    .auto_out_2_a_valid(fixer_auto_out_2_a_valid),
    .auto_out_2_a_bits_opcode(fixer_auto_out_2_a_bits_opcode),
    .auto_out_2_a_bits_param(fixer_auto_out_2_a_bits_param),
    .auto_out_2_a_bits_size(fixer_auto_out_2_a_bits_size),
    .auto_out_2_a_bits_source(fixer_auto_out_2_a_bits_source),
    .auto_out_2_a_bits_address(fixer_auto_out_2_a_bits_address),
    .auto_out_2_a_bits_user_amba_prot_bufferable(fixer_auto_out_2_a_bits_user_amba_prot_bufferable),
    .auto_out_2_a_bits_user_amba_prot_modifiable(fixer_auto_out_2_a_bits_user_amba_prot_modifiable),
    .auto_out_2_a_bits_user_amba_prot_readalloc(fixer_auto_out_2_a_bits_user_amba_prot_readalloc),
    .auto_out_2_a_bits_user_amba_prot_writealloc(fixer_auto_out_2_a_bits_user_amba_prot_writealloc),
    .auto_out_2_a_bits_user_amba_prot_privileged(fixer_auto_out_2_a_bits_user_amba_prot_privileged),
    .auto_out_2_a_bits_user_amba_prot_secure(fixer_auto_out_2_a_bits_user_amba_prot_secure),
    .auto_out_2_a_bits_mask(fixer_auto_out_2_a_bits_mask),
    .auto_out_2_a_bits_data(fixer_auto_out_2_a_bits_data),
    .auto_out_2_d_ready(fixer_auto_out_2_d_ready),
    .auto_out_2_d_valid(fixer_auto_out_2_d_valid),
    .auto_out_2_d_bits_opcode(fixer_auto_out_2_d_bits_opcode),
    .auto_out_2_d_bits_size(fixer_auto_out_2_d_bits_size),
    .auto_out_2_d_bits_source(fixer_auto_out_2_d_bits_source),
    .auto_out_2_d_bits_denied(fixer_auto_out_2_d_bits_denied),
    .auto_out_2_d_bits_data(fixer_auto_out_2_d_bits_data),
    .auto_out_2_d_bits_corrupt(fixer_auto_out_2_d_bits_corrupt),
    .auto_out_1_a_ready(fixer_auto_out_1_a_ready),
    .auto_out_1_a_valid(fixer_auto_out_1_a_valid),
    .auto_out_1_a_bits_opcode(fixer_auto_out_1_a_bits_opcode),
    .auto_out_1_a_bits_param(fixer_auto_out_1_a_bits_param),
    .auto_out_1_a_bits_size(fixer_auto_out_1_a_bits_size),
    .auto_out_1_a_bits_source(fixer_auto_out_1_a_bits_source),
    .auto_out_1_a_bits_address(fixer_auto_out_1_a_bits_address),
    .auto_out_1_a_bits_user_amba_prot_bufferable(fixer_auto_out_1_a_bits_user_amba_prot_bufferable),
    .auto_out_1_a_bits_user_amba_prot_modifiable(fixer_auto_out_1_a_bits_user_amba_prot_modifiable),
    .auto_out_1_a_bits_user_amba_prot_readalloc(fixer_auto_out_1_a_bits_user_amba_prot_readalloc),
    .auto_out_1_a_bits_user_amba_prot_writealloc(fixer_auto_out_1_a_bits_user_amba_prot_writealloc),
    .auto_out_1_a_bits_user_amba_prot_privileged(fixer_auto_out_1_a_bits_user_amba_prot_privileged),
    .auto_out_1_a_bits_user_amba_prot_secure(fixer_auto_out_1_a_bits_user_amba_prot_secure),
    .auto_out_1_a_bits_mask(fixer_auto_out_1_a_bits_mask),
    .auto_out_1_a_bits_data(fixer_auto_out_1_a_bits_data),
    .auto_out_1_d_ready(fixer_auto_out_1_d_ready),
    .auto_out_1_d_valid(fixer_auto_out_1_d_valid),
    .auto_out_1_d_bits_opcode(fixer_auto_out_1_d_bits_opcode),
    .auto_out_1_d_bits_size(fixer_auto_out_1_d_bits_size),
    .auto_out_1_d_bits_source(fixer_auto_out_1_d_bits_source),
    .auto_out_1_d_bits_denied(fixer_auto_out_1_d_bits_denied),
    .auto_out_1_d_bits_data(fixer_auto_out_1_d_bits_data),
    .auto_out_1_d_bits_corrupt(fixer_auto_out_1_d_bits_corrupt),
    .auto_out_0_a_ready(fixer_auto_out_0_a_ready),
    .auto_out_0_a_valid(fixer_auto_out_0_a_valid),
    .auto_out_0_a_bits_opcode(fixer_auto_out_0_a_bits_opcode),
    .auto_out_0_a_bits_param(fixer_auto_out_0_a_bits_param),
    .auto_out_0_a_bits_size(fixer_auto_out_0_a_bits_size),
    .auto_out_0_a_bits_source(fixer_auto_out_0_a_bits_source),
    .auto_out_0_a_bits_address(fixer_auto_out_0_a_bits_address),
    .auto_out_0_a_bits_user_amba_prot_bufferable(fixer_auto_out_0_a_bits_user_amba_prot_bufferable),
    .auto_out_0_a_bits_user_amba_prot_modifiable(fixer_auto_out_0_a_bits_user_amba_prot_modifiable),
    .auto_out_0_a_bits_user_amba_prot_readalloc(fixer_auto_out_0_a_bits_user_amba_prot_readalloc),
    .auto_out_0_a_bits_user_amba_prot_writealloc(fixer_auto_out_0_a_bits_user_amba_prot_writealloc),
    .auto_out_0_a_bits_user_amba_prot_privileged(fixer_auto_out_0_a_bits_user_amba_prot_privileged),
    .auto_out_0_a_bits_user_amba_prot_secure(fixer_auto_out_0_a_bits_user_amba_prot_secure),
    .auto_out_0_a_bits_mask(fixer_auto_out_0_a_bits_mask),
    .auto_out_0_a_bits_data(fixer_auto_out_0_a_bits_data),
    .auto_out_0_d_ready(fixer_auto_out_0_d_ready),
    .auto_out_0_d_valid(fixer_auto_out_0_d_valid),
    .auto_out_0_d_bits_opcode(fixer_auto_out_0_d_bits_opcode),
    .auto_out_0_d_bits_size(fixer_auto_out_0_d_bits_size),
    .auto_out_0_d_bits_source(fixer_auto_out_0_d_bits_source),
    .auto_out_0_d_bits_denied(fixer_auto_out_0_d_bits_denied),
    .auto_out_0_d_bits_data(fixer_auto_out_0_d_bits_data),
    .auto_out_0_d_bits_corrupt(fixer_auto_out_0_d_bits_corrupt)
  );
  RHEA__ProbePicker picker ( // @[ProbePicker.scala 65:28]
    .rf_reset(picker_rf_reset),
    .clock(picker_clock),
    .reset(picker_reset),
    .auto_in_1_a_ready(picker_auto_in_1_a_ready),
    .auto_in_1_a_valid(picker_auto_in_1_a_valid),
    .auto_in_1_a_bits_opcode(picker_auto_in_1_a_bits_opcode),
    .auto_in_1_a_bits_param(picker_auto_in_1_a_bits_param),
    .auto_in_1_a_bits_size(picker_auto_in_1_a_bits_size),
    .auto_in_1_a_bits_source(picker_auto_in_1_a_bits_source),
    .auto_in_1_a_bits_address(picker_auto_in_1_a_bits_address),
    .auto_in_1_a_bits_user_amba_prot_bufferable(picker_auto_in_1_a_bits_user_amba_prot_bufferable),
    .auto_in_1_a_bits_user_amba_prot_modifiable(picker_auto_in_1_a_bits_user_amba_prot_modifiable),
    .auto_in_1_a_bits_user_amba_prot_readalloc(picker_auto_in_1_a_bits_user_amba_prot_readalloc),
    .auto_in_1_a_bits_user_amba_prot_writealloc(picker_auto_in_1_a_bits_user_amba_prot_writealloc),
    .auto_in_1_a_bits_user_amba_prot_privileged(picker_auto_in_1_a_bits_user_amba_prot_privileged),
    .auto_in_1_a_bits_user_amba_prot_secure(picker_auto_in_1_a_bits_user_amba_prot_secure),
    .auto_in_1_a_bits_mask(picker_auto_in_1_a_bits_mask),
    .auto_in_1_a_bits_data(picker_auto_in_1_a_bits_data),
    .auto_in_1_d_ready(picker_auto_in_1_d_ready),
    .auto_in_1_d_valid(picker_auto_in_1_d_valid),
    .auto_in_1_d_bits_opcode(picker_auto_in_1_d_bits_opcode),
    .auto_in_1_d_bits_size(picker_auto_in_1_d_bits_size),
    .auto_in_1_d_bits_source(picker_auto_in_1_d_bits_source),
    .auto_in_1_d_bits_denied(picker_auto_in_1_d_bits_denied),
    .auto_in_1_d_bits_data(picker_auto_in_1_d_bits_data),
    .auto_in_1_d_bits_corrupt(picker_auto_in_1_d_bits_corrupt),
    .auto_in_0_a_ready(picker_auto_in_0_a_ready),
    .auto_in_0_a_valid(picker_auto_in_0_a_valid),
    .auto_in_0_a_bits_opcode(picker_auto_in_0_a_bits_opcode),
    .auto_in_0_a_bits_param(picker_auto_in_0_a_bits_param),
    .auto_in_0_a_bits_size(picker_auto_in_0_a_bits_size),
    .auto_in_0_a_bits_source(picker_auto_in_0_a_bits_source),
    .auto_in_0_a_bits_address(picker_auto_in_0_a_bits_address),
    .auto_in_0_a_bits_mask(picker_auto_in_0_a_bits_mask),
    .auto_in_0_d_ready(picker_auto_in_0_d_ready),
    .auto_in_0_d_valid(picker_auto_in_0_d_valid),
    .auto_in_0_d_bits_opcode(picker_auto_in_0_d_bits_opcode),
    .auto_in_0_d_bits_size(picker_auto_in_0_d_bits_size),
    .auto_in_0_d_bits_source(picker_auto_in_0_d_bits_source),
    .auto_out_1_a_ready(picker_auto_out_1_a_ready),
    .auto_out_1_a_valid(picker_auto_out_1_a_valid),
    .auto_out_1_a_bits_opcode(picker_auto_out_1_a_bits_opcode),
    .auto_out_1_a_bits_param(picker_auto_out_1_a_bits_param),
    .auto_out_1_a_bits_size(picker_auto_out_1_a_bits_size),
    .auto_out_1_a_bits_source(picker_auto_out_1_a_bits_source),
    .auto_out_1_a_bits_address(picker_auto_out_1_a_bits_address),
    .auto_out_1_a_bits_user_amba_prot_bufferable(picker_auto_out_1_a_bits_user_amba_prot_bufferable),
    .auto_out_1_a_bits_user_amba_prot_modifiable(picker_auto_out_1_a_bits_user_amba_prot_modifiable),
    .auto_out_1_a_bits_user_amba_prot_readalloc(picker_auto_out_1_a_bits_user_amba_prot_readalloc),
    .auto_out_1_a_bits_user_amba_prot_writealloc(picker_auto_out_1_a_bits_user_amba_prot_writealloc),
    .auto_out_1_a_bits_user_amba_prot_privileged(picker_auto_out_1_a_bits_user_amba_prot_privileged),
    .auto_out_1_a_bits_user_amba_prot_secure(picker_auto_out_1_a_bits_user_amba_prot_secure),
    .auto_out_1_a_bits_mask(picker_auto_out_1_a_bits_mask),
    .auto_out_1_a_bits_data(picker_auto_out_1_a_bits_data),
    .auto_out_1_d_ready(picker_auto_out_1_d_ready),
    .auto_out_1_d_valid(picker_auto_out_1_d_valid),
    .auto_out_1_d_bits_opcode(picker_auto_out_1_d_bits_opcode),
    .auto_out_1_d_bits_size(picker_auto_out_1_d_bits_size),
    .auto_out_1_d_bits_source(picker_auto_out_1_d_bits_source),
    .auto_out_1_d_bits_denied(picker_auto_out_1_d_bits_denied),
    .auto_out_1_d_bits_data(picker_auto_out_1_d_bits_data),
    .auto_out_1_d_bits_corrupt(picker_auto_out_1_d_bits_corrupt),
    .auto_out_0_a_ready(picker_auto_out_0_a_ready),
    .auto_out_0_a_valid(picker_auto_out_0_a_valid),
    .auto_out_0_a_bits_opcode(picker_auto_out_0_a_bits_opcode),
    .auto_out_0_a_bits_param(picker_auto_out_0_a_bits_param),
    .auto_out_0_a_bits_size(picker_auto_out_0_a_bits_size),
    .auto_out_0_a_bits_source(picker_auto_out_0_a_bits_source),
    .auto_out_0_a_bits_address(picker_auto_out_0_a_bits_address),
    .auto_out_0_a_bits_mask(picker_auto_out_0_a_bits_mask),
    .auto_out_0_d_ready(picker_auto_out_0_d_ready),
    .auto_out_0_d_valid(picker_auto_out_0_d_valid),
    .auto_out_0_d_bits_opcode(picker_auto_out_0_d_bits_opcode),
    .auto_out_0_d_bits_size(picker_auto_out_0_d_bits_size),
    .auto_out_0_d_bits_source(picker_auto_out_0_d_bits_source)
  );
  RHEA__ZeroDeviceWrapper wrapped_zero_device ( // @[LazyModule.scala 524:27]
    .rf_reset(wrapped_zero_device_rf_reset),
    .clock(wrapped_zero_device_clock),
    .reset(wrapped_zero_device_reset),
    .auto_buffer_in_a_ready(wrapped_zero_device_auto_buffer_in_a_ready),
    .auto_buffer_in_a_valid(wrapped_zero_device_auto_buffer_in_a_valid),
    .auto_buffer_in_a_bits_opcode(wrapped_zero_device_auto_buffer_in_a_bits_opcode),
    .auto_buffer_in_a_bits_param(wrapped_zero_device_auto_buffer_in_a_bits_param),
    .auto_buffer_in_a_bits_size(wrapped_zero_device_auto_buffer_in_a_bits_size),
    .auto_buffer_in_a_bits_source(wrapped_zero_device_auto_buffer_in_a_bits_source),
    .auto_buffer_in_a_bits_address(wrapped_zero_device_auto_buffer_in_a_bits_address),
    .auto_buffer_in_a_bits_mask(wrapped_zero_device_auto_buffer_in_a_bits_mask),
    .auto_buffer_in_d_ready(wrapped_zero_device_auto_buffer_in_d_ready),
    .auto_buffer_in_d_valid(wrapped_zero_device_auto_buffer_in_d_valid),
    .auto_buffer_in_d_bits_opcode(wrapped_zero_device_auto_buffer_in_d_bits_opcode),
    .auto_buffer_in_d_bits_size(wrapped_zero_device_auto_buffer_in_d_bits_size),
    .auto_buffer_in_d_bits_source(wrapped_zero_device_auto_buffer_in_d_bits_source)
  );
  RHEA__TLInterconnectCoupler_24 coupler_to_memory_controller_named_axi4_mem_port ( // @[LazyModule.scala 524:27]
    .rf_reset(coupler_to_memory_controller_named_axi4_mem_port_rf_reset),
    .clock(coupler_to_memory_controller_named_axi4_mem_port_clock),
    .reset(coupler_to_memory_controller_named_axi4_mem_port_reset),
    .auto_buffer_in_a_ready(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_ready),
    .auto_buffer_in_a_valid(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_valid),
    .auto_buffer_in_a_bits_opcode(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_opcode),
    .auto_buffer_in_a_bits_param(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_param),
    .auto_buffer_in_a_bits_size(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_size),
    .auto_buffer_in_a_bits_source(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_source),
    .auto_buffer_in_a_bits_address(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_address),
    .auto_buffer_in_a_bits_user_amba_prot_bufferable(
      coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_bufferable),
    .auto_buffer_in_a_bits_user_amba_prot_modifiable(
      coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_modifiable),
    .auto_buffer_in_a_bits_user_amba_prot_readalloc(
      coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_readalloc),
    .auto_buffer_in_a_bits_user_amba_prot_writealloc(
      coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_writealloc),
    .auto_buffer_in_a_bits_user_amba_prot_privileged(
      coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_privileged),
    .auto_buffer_in_a_bits_user_amba_prot_secure(
      coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_secure),
    .auto_buffer_in_a_bits_mask(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_mask),
    .auto_buffer_in_a_bits_data(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_data),
    .auto_buffer_in_d_ready(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_ready),
    .auto_buffer_in_d_valid(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_valid),
    .auto_buffer_in_d_bits_opcode(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_opcode),
    .auto_buffer_in_d_bits_size(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_size),
    .auto_buffer_in_d_bits_source(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_source),
    .auto_buffer_in_d_bits_denied(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_denied),
    .auto_buffer_in_d_bits_data(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_data),
    .auto_buffer_in_d_bits_corrupt(coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_corrupt),
    .auto_axi4buf_out_aw_ready(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_ready),
    .auto_axi4buf_out_aw_valid(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_valid),
    .auto_axi4buf_out_aw_bits_id(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_id),
    .auto_axi4buf_out_aw_bits_addr(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_addr),
    .auto_axi4buf_out_aw_bits_len(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_len),
    .auto_axi4buf_out_aw_bits_size(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_size),
    .auto_axi4buf_out_aw_bits_cache(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_cache),
    .auto_axi4buf_out_aw_bits_prot(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_prot),
    .auto_axi4buf_out_w_ready(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_w_ready),
    .auto_axi4buf_out_w_valid(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_w_valid),
    .auto_axi4buf_out_w_bits_data(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_w_bits_data),
    .auto_axi4buf_out_w_bits_strb(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_w_bits_strb),
    .auto_axi4buf_out_w_bits_last(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_w_bits_last),
    .auto_axi4buf_out_b_ready(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_b_ready),
    .auto_axi4buf_out_b_valid(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_b_valid),
    .auto_axi4buf_out_b_bits_id(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_b_bits_id),
    .auto_axi4buf_out_b_bits_resp(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_b_bits_resp),
    .auto_axi4buf_out_ar_ready(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_ready),
    .auto_axi4buf_out_ar_valid(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_valid),
    .auto_axi4buf_out_ar_bits_id(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_id),
    .auto_axi4buf_out_ar_bits_addr(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_addr),
    .auto_axi4buf_out_ar_bits_len(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_len),
    .auto_axi4buf_out_ar_bits_size(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_size),
    .auto_axi4buf_out_ar_bits_cache(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_cache),
    .auto_axi4buf_out_ar_bits_prot(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_prot),
    .auto_axi4buf_out_r_ready(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_ready),
    .auto_axi4buf_out_r_valid(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_valid),
    .auto_axi4buf_out_r_bits_id(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_bits_id),
    .auto_axi4buf_out_r_bits_data(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_bits_data),
    .auto_axi4buf_out_r_bits_resp(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_bits_resp),
    .auto_axi4buf_out_r_bits_last(coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_bits_last)
  );
  assign subsystem_mbus_clock_groups_auto_out_member_subsystem_mbus_0_clock =
    subsystem_mbus_clock_groups_auto_in_member_subsystem_mbus_0_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_mbus_clock_groups_auto_out_member_subsystem_mbus_0_reset =
    subsystem_mbus_clock_groups_auto_in_member_subsystem_mbus_0_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign clockGroup_auto_out_clock = clockGroup_auto_in_member_subsystem_mbus_0_clock; // @[ClockGroup.scala 8:11 LazyModule.scala 388:16]
  assign clockGroup_auto_out_reset = clockGroup_auto_in_member_subsystem_mbus_0_reset; // @[ClockGroup.scala 8:11 LazyModule.scala 388:16]
  assign fixedClockNode_auto_out_clock = fixedClockNode_auto_in_clock; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign fixedClockNode_auto_out_reset = fixedClockNode_auto_in_reset; // @[Nodes.scala 1617:13 LazyModule.scala 388:16]
  assign subsystem_mbus_xbar_rf_reset = rf_reset;
  assign fixer_rf_reset = rf_reset;
  assign picker_rf_reset = rf_reset;
  assign wrapped_zero_device_rf_reset = rf_reset;
  assign buffer_auto_in_3_a_ready = buffer_auto_out_3_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_valid = buffer_auto_out_3_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_bits_opcode = buffer_auto_out_3_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_bits_size = buffer_auto_out_3_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_bits_source = buffer_auto_out_3_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_bits_denied = buffer_auto_out_3_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_bits_data = buffer_auto_out_3_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_3_d_bits_corrupt = buffer_auto_out_3_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_a_ready = buffer_auto_out_2_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_valid = buffer_auto_out_2_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_bits_opcode = buffer_auto_out_2_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_bits_size = buffer_auto_out_2_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_bits_source = buffer_auto_out_2_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_bits_denied = buffer_auto_out_2_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_bits_data = buffer_auto_out_2_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_2_d_bits_corrupt = buffer_auto_out_2_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_a_ready = buffer_auto_out_1_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_valid = buffer_auto_out_1_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_bits_opcode = buffer_auto_out_1_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_bits_size = buffer_auto_out_1_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_bits_source = buffer_auto_out_1_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_bits_denied = buffer_auto_out_1_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_bits_data = buffer_auto_out_1_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_1_d_bits_corrupt = buffer_auto_out_1_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_a_ready = buffer_auto_out_0_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_valid = buffer_auto_out_0_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_bits_opcode = buffer_auto_out_0_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_bits_size = buffer_auto_out_0_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_bits_source = buffer_auto_out_0_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_bits_denied = buffer_auto_out_0_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_bits_data = buffer_auto_out_0_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_in_0_d_bits_corrupt = buffer_auto_out_0_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 390:12]
  assign buffer_auto_out_3_a_valid = buffer_auto_in_3_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_opcode = buffer_auto_in_3_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_param = buffer_auto_in_3_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_size = buffer_auto_in_3_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_source = buffer_auto_in_3_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_address = buffer_auto_in_3_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_user_amba_prot_bufferable = buffer_auto_in_3_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_user_amba_prot_modifiable = buffer_auto_in_3_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_user_amba_prot_readalloc = buffer_auto_in_3_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_user_amba_prot_writealloc = buffer_auto_in_3_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_user_amba_prot_privileged = buffer_auto_in_3_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_user_amba_prot_secure = buffer_auto_in_3_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_mask = buffer_auto_in_3_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_bits_data = buffer_auto_in_3_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_d_ready = buffer_auto_in_3_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_valid = buffer_auto_in_2_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_opcode = buffer_auto_in_2_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_param = buffer_auto_in_2_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_size = buffer_auto_in_2_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_source = buffer_auto_in_2_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_address = buffer_auto_in_2_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_user_amba_prot_bufferable = buffer_auto_in_2_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_user_amba_prot_modifiable = buffer_auto_in_2_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_user_amba_prot_readalloc = buffer_auto_in_2_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_user_amba_prot_writealloc = buffer_auto_in_2_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_user_amba_prot_privileged = buffer_auto_in_2_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_user_amba_prot_secure = buffer_auto_in_2_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_mask = buffer_auto_in_2_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_a_bits_data = buffer_auto_in_2_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_2_d_ready = buffer_auto_in_2_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_valid = buffer_auto_in_1_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_opcode = buffer_auto_in_1_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_param = buffer_auto_in_1_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_size = buffer_auto_in_1_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_source = buffer_auto_in_1_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_address = buffer_auto_in_1_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_user_amba_prot_bufferable = buffer_auto_in_1_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_user_amba_prot_modifiable = buffer_auto_in_1_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_user_amba_prot_readalloc = buffer_auto_in_1_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_user_amba_prot_writealloc = buffer_auto_in_1_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_user_amba_prot_privileged = buffer_auto_in_1_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_user_amba_prot_secure = buffer_auto_in_1_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_mask = buffer_auto_in_1_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_a_bits_data = buffer_auto_in_1_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_1_d_ready = buffer_auto_in_1_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_valid = buffer_auto_in_0_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_opcode = buffer_auto_in_0_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_param = buffer_auto_in_0_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_size = buffer_auto_in_0_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_source = buffer_auto_in_0_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_address = buffer_auto_in_0_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_user_amba_prot_bufferable = buffer_auto_in_0_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_user_amba_prot_modifiable = buffer_auto_in_0_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_user_amba_prot_readalloc = buffer_auto_in_0_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_user_amba_prot_writealloc = buffer_auto_in_0_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_user_amba_prot_privileged = buffer_auto_in_0_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_user_amba_prot_secure = buffer_auto_in_0_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_mask = buffer_auto_in_0_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_a_bits_data = buffer_auto_in_0_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_0_d_ready = buffer_auto_in_0_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_rf_reset = rf_reset;
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_valid =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_bits_id =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_id; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_bits_addr =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_addr; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_bits_len =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_len; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_bits_size =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_bits_cache =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_cache; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_bits_prot =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_bits_prot; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_w_valid =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_w_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_w_bits_data =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_w_bits_data; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_w_bits_strb =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_w_bits_strb; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_w_bits_last =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_w_bits_last; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_b_ready =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_b_ready; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_valid =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_valid; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_bits_id =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_id; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_bits_addr =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_addr; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_bits_len =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_len; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_bits_size =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_size; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_bits_cache =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_cache; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_bits_prot =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_bits_prot; // @[LazyModule.scala 390:12]
  assign auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_r_ready =
    coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_ready; // @[LazyModule.scala 390:12]
  assign auto_bus_xing_in_3_a_ready = buffer_auto_in_3_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_valid = buffer_auto_in_3_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_bits_opcode = buffer_auto_in_3_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_bits_size = buffer_auto_in_3_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_bits_source = buffer_auto_in_3_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_bits_denied = buffer_auto_in_3_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_bits_data = buffer_auto_in_3_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_3_d_bits_corrupt = buffer_auto_in_3_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_a_ready = buffer_auto_in_2_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_valid = buffer_auto_in_2_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_bits_opcode = buffer_auto_in_2_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_bits_size = buffer_auto_in_2_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_bits_source = buffer_auto_in_2_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_bits_denied = buffer_auto_in_2_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_bits_data = buffer_auto_in_2_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_2_d_bits_corrupt = buffer_auto_in_2_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_a_ready = buffer_auto_in_1_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_valid = buffer_auto_in_1_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_bits_opcode = buffer_auto_in_1_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_bits_size = buffer_auto_in_1_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_bits_source = buffer_auto_in_1_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_bits_denied = buffer_auto_in_1_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_bits_data = buffer_auto_in_1_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_1_d_bits_corrupt = buffer_auto_in_1_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_a_ready = buffer_auto_in_0_a_ready; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_valid = buffer_auto_in_0_d_valid; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_bits_opcode = buffer_auto_in_0_d_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_bits_size = buffer_auto_in_0_d_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_bits_source = buffer_auto_in_0_d_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_bits_denied = buffer_auto_in_0_d_bits_denied; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_bits_data = buffer_auto_in_0_d_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign auto_bus_xing_in_0_d_bits_corrupt = buffer_auto_in_0_d_bits_corrupt; // @[Nodes.scala 1529:13 LazyModule.scala 377:16]
  assign subsystem_mbus_clock_groups_auto_in_member_subsystem_mbus_0_clock =
    auto_subsystem_mbus_clock_groups_in_member_subsystem_mbus_0_clock; // @[LazyModule.scala 388:16]
  assign subsystem_mbus_clock_groups_auto_in_member_subsystem_mbus_0_reset =
    auto_subsystem_mbus_clock_groups_in_member_subsystem_mbus_0_reset; // @[LazyModule.scala 388:16]
  assign clockGroup_auto_in_member_subsystem_mbus_0_clock =
    subsystem_mbus_clock_groups_auto_out_member_subsystem_mbus_0_clock; // @[LazyModule.scala 377:16]
  assign clockGroup_auto_in_member_subsystem_mbus_0_reset =
    subsystem_mbus_clock_groups_auto_out_member_subsystem_mbus_0_reset; // @[LazyModule.scala 377:16]
  assign fixedClockNode_auto_in_clock = clockGroup_auto_out_clock; // @[LazyModule.scala 377:16]
  assign fixedClockNode_auto_in_reset = clockGroup_auto_out_reset; // @[LazyModule.scala 377:16]
  assign subsystem_mbus_xbar_clock = fixedClockNode_auto_out_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_reset = fixedClockNode_auto_out_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_3_a_valid = fixer_auto_out_3_a_valid; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_3_a_bits_opcode = fixer_auto_out_3_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_3_a_bits_param = fixer_auto_out_3_a_bits_param; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_3_a_bits_size = fixer_auto_out_3_a_bits_size; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_3_a_bits_source = fixer_auto_out_3_a_bits_source; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_3_a_bits_address = fixer_auto_out_3_a_bits_address; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_bufferable =
    fixer_auto_out_3_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_modifiable =
    fixer_auto_out_3_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_readalloc =
    fixer_auto_out_3_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_writealloc =
    fixer_auto_out_3_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_privileged =
    fixer_auto_out_3_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_3_a_bits_user_amba_prot_secure = fixer_auto_out_3_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_3_a_bits_mask = fixer_auto_out_3_a_bits_mask; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_3_a_bits_data = fixer_auto_out_3_a_bits_data; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_3_d_ready = fixer_auto_out_3_d_ready; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_2_a_valid = fixer_auto_out_2_a_valid; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_2_a_bits_opcode = fixer_auto_out_2_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_2_a_bits_param = fixer_auto_out_2_a_bits_param; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_2_a_bits_size = fixer_auto_out_2_a_bits_size; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_2_a_bits_source = fixer_auto_out_2_a_bits_source; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_2_a_bits_address = fixer_auto_out_2_a_bits_address; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_bufferable =
    fixer_auto_out_2_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_modifiable =
    fixer_auto_out_2_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_readalloc =
    fixer_auto_out_2_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_writealloc =
    fixer_auto_out_2_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_privileged =
    fixer_auto_out_2_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_2_a_bits_user_amba_prot_secure = fixer_auto_out_2_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_2_a_bits_mask = fixer_auto_out_2_a_bits_mask; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_2_a_bits_data = fixer_auto_out_2_a_bits_data; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_2_d_ready = fixer_auto_out_2_d_ready; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_1_a_valid = fixer_auto_out_1_a_valid; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_1_a_bits_opcode = fixer_auto_out_1_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_1_a_bits_param = fixer_auto_out_1_a_bits_param; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_1_a_bits_size = fixer_auto_out_1_a_bits_size; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_1_a_bits_source = fixer_auto_out_1_a_bits_source; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_1_a_bits_address = fixer_auto_out_1_a_bits_address; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_bufferable =
    fixer_auto_out_1_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_modifiable =
    fixer_auto_out_1_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_readalloc =
    fixer_auto_out_1_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_writealloc =
    fixer_auto_out_1_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_privileged =
    fixer_auto_out_1_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_1_a_bits_user_amba_prot_secure = fixer_auto_out_1_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_1_a_bits_mask = fixer_auto_out_1_a_bits_mask; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_1_a_bits_data = fixer_auto_out_1_a_bits_data; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_1_d_ready = fixer_auto_out_1_d_ready; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_0_a_valid = fixer_auto_out_0_a_valid; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_0_a_bits_opcode = fixer_auto_out_0_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_0_a_bits_param = fixer_auto_out_0_a_bits_param; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_0_a_bits_size = fixer_auto_out_0_a_bits_size; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_0_a_bits_source = fixer_auto_out_0_a_bits_source; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_0_a_bits_address = fixer_auto_out_0_a_bits_address; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_bufferable =
    fixer_auto_out_0_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_modifiable =
    fixer_auto_out_0_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_readalloc =
    fixer_auto_out_0_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_writealloc =
    fixer_auto_out_0_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_privileged =
    fixer_auto_out_0_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_0_a_bits_user_amba_prot_secure = fixer_auto_out_0_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_0_a_bits_mask = fixer_auto_out_0_a_bits_mask; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_0_a_bits_data = fixer_auto_out_0_a_bits_data; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_in_0_d_ready = fixer_auto_out_0_d_ready; // @[LazyModule.scala 375:16]
  assign subsystem_mbus_xbar_auto_out_1_a_ready = picker_auto_in_1_a_ready; // @[LazyModule.scala 377:16]
  assign subsystem_mbus_xbar_auto_out_1_d_valid = picker_auto_in_1_d_valid; // @[LazyModule.scala 377:16]
  assign subsystem_mbus_xbar_auto_out_1_d_bits_opcode = picker_auto_in_1_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign subsystem_mbus_xbar_auto_out_1_d_bits_size = picker_auto_in_1_d_bits_size; // @[LazyModule.scala 377:16]
  assign subsystem_mbus_xbar_auto_out_1_d_bits_source = picker_auto_in_1_d_bits_source; // @[LazyModule.scala 377:16]
  assign subsystem_mbus_xbar_auto_out_1_d_bits_denied = picker_auto_in_1_d_bits_denied; // @[LazyModule.scala 377:16]
  assign subsystem_mbus_xbar_auto_out_1_d_bits_data = picker_auto_in_1_d_bits_data; // @[LazyModule.scala 377:16]
  assign subsystem_mbus_xbar_auto_out_1_d_bits_corrupt = picker_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign subsystem_mbus_xbar_auto_out_0_a_ready = picker_auto_in_0_a_ready; // @[LazyModule.scala 377:16]
  assign subsystem_mbus_xbar_auto_out_0_d_valid = picker_auto_in_0_d_valid; // @[LazyModule.scala 377:16]
  assign subsystem_mbus_xbar_auto_out_0_d_bits_opcode = picker_auto_in_0_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign subsystem_mbus_xbar_auto_out_0_d_bits_size = picker_auto_in_0_d_bits_size; // @[LazyModule.scala 377:16]
  assign subsystem_mbus_xbar_auto_out_0_d_bits_source = picker_auto_in_0_d_bits_source; // @[LazyModule.scala 377:16]
  assign fixer_clock = fixedClockNode_auto_out_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign fixer_reset = fixedClockNode_auto_out_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign fixer_auto_in_3_a_valid = buffer_auto_out_3_a_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_3_a_bits_opcode = buffer_auto_out_3_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_3_a_bits_param = buffer_auto_out_3_a_bits_param; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_3_a_bits_size = buffer_auto_out_3_a_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_3_a_bits_source = buffer_auto_out_3_a_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_3_a_bits_address = buffer_auto_out_3_a_bits_address; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_3_a_bits_user_amba_prot_bufferable = buffer_auto_out_3_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_3_a_bits_user_amba_prot_modifiable = buffer_auto_out_3_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_3_a_bits_user_amba_prot_readalloc = buffer_auto_out_3_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_3_a_bits_user_amba_prot_writealloc = buffer_auto_out_3_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_3_a_bits_user_amba_prot_privileged = buffer_auto_out_3_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_3_a_bits_user_amba_prot_secure = buffer_auto_out_3_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_3_a_bits_mask = buffer_auto_out_3_a_bits_mask; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_3_a_bits_data = buffer_auto_out_3_a_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_3_d_ready = buffer_auto_out_3_d_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_valid = buffer_auto_out_2_a_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_opcode = buffer_auto_out_2_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_param = buffer_auto_out_2_a_bits_param; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_size = buffer_auto_out_2_a_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_source = buffer_auto_out_2_a_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_address = buffer_auto_out_2_a_bits_address; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_user_amba_prot_bufferable = buffer_auto_out_2_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_user_amba_prot_modifiable = buffer_auto_out_2_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_user_amba_prot_readalloc = buffer_auto_out_2_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_user_amba_prot_writealloc = buffer_auto_out_2_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_user_amba_prot_privileged = buffer_auto_out_2_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_user_amba_prot_secure = buffer_auto_out_2_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_mask = buffer_auto_out_2_a_bits_mask; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_a_bits_data = buffer_auto_out_2_a_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_2_d_ready = buffer_auto_out_2_d_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_valid = buffer_auto_out_1_a_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_opcode = buffer_auto_out_1_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_param = buffer_auto_out_1_a_bits_param; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_size = buffer_auto_out_1_a_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_source = buffer_auto_out_1_a_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_address = buffer_auto_out_1_a_bits_address; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_user_amba_prot_bufferable = buffer_auto_out_1_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_user_amba_prot_modifiable = buffer_auto_out_1_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_user_amba_prot_readalloc = buffer_auto_out_1_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_user_amba_prot_writealloc = buffer_auto_out_1_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_user_amba_prot_privileged = buffer_auto_out_1_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_user_amba_prot_secure = buffer_auto_out_1_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_mask = buffer_auto_out_1_a_bits_mask; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_a_bits_data = buffer_auto_out_1_a_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_1_d_ready = buffer_auto_out_1_d_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_valid = buffer_auto_out_0_a_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_opcode = buffer_auto_out_0_a_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_param = buffer_auto_out_0_a_bits_param; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_size = buffer_auto_out_0_a_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_source = buffer_auto_out_0_a_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_address = buffer_auto_out_0_a_bits_address; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_user_amba_prot_bufferable = buffer_auto_out_0_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_user_amba_prot_modifiable = buffer_auto_out_0_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_user_amba_prot_readalloc = buffer_auto_out_0_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_user_amba_prot_writealloc = buffer_auto_out_0_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_user_amba_prot_privileged = buffer_auto_out_0_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_user_amba_prot_secure = buffer_auto_out_0_a_bits_user_amba_prot_secure; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_mask = buffer_auto_out_0_a_bits_mask; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_a_bits_data = buffer_auto_out_0_a_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_in_0_d_ready = buffer_auto_out_0_d_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_3_a_ready = subsystem_mbus_xbar_auto_in_3_a_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_3_d_valid = subsystem_mbus_xbar_auto_in_3_d_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_3_d_bits_opcode = subsystem_mbus_xbar_auto_in_3_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_3_d_bits_size = subsystem_mbus_xbar_auto_in_3_d_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_3_d_bits_source = subsystem_mbus_xbar_auto_in_3_d_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_3_d_bits_denied = subsystem_mbus_xbar_auto_in_3_d_bits_denied; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_3_d_bits_data = subsystem_mbus_xbar_auto_in_3_d_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_3_d_bits_corrupt = subsystem_mbus_xbar_auto_in_3_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_a_ready = subsystem_mbus_xbar_auto_in_2_a_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_valid = subsystem_mbus_xbar_auto_in_2_d_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_bits_opcode = subsystem_mbus_xbar_auto_in_2_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_bits_size = subsystem_mbus_xbar_auto_in_2_d_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_bits_source = subsystem_mbus_xbar_auto_in_2_d_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_bits_denied = subsystem_mbus_xbar_auto_in_2_d_bits_denied; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_bits_data = subsystem_mbus_xbar_auto_in_2_d_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_2_d_bits_corrupt = subsystem_mbus_xbar_auto_in_2_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_a_ready = subsystem_mbus_xbar_auto_in_1_a_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_valid = subsystem_mbus_xbar_auto_in_1_d_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_bits_opcode = subsystem_mbus_xbar_auto_in_1_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_bits_size = subsystem_mbus_xbar_auto_in_1_d_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_bits_source = subsystem_mbus_xbar_auto_in_1_d_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_bits_denied = subsystem_mbus_xbar_auto_in_1_d_bits_denied; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_bits_data = subsystem_mbus_xbar_auto_in_1_d_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_1_d_bits_corrupt = subsystem_mbus_xbar_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_a_ready = subsystem_mbus_xbar_auto_in_0_a_ready; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_valid = subsystem_mbus_xbar_auto_in_0_d_valid; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_bits_opcode = subsystem_mbus_xbar_auto_in_0_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_bits_size = subsystem_mbus_xbar_auto_in_0_d_bits_size; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_bits_source = subsystem_mbus_xbar_auto_in_0_d_bits_source; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_bits_denied = subsystem_mbus_xbar_auto_in_0_d_bits_denied; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_bits_data = subsystem_mbus_xbar_auto_in_0_d_bits_data; // @[LazyModule.scala 375:16]
  assign fixer_auto_out_0_d_bits_corrupt = subsystem_mbus_xbar_auto_in_0_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign picker_clock = fixedClockNode_auto_out_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign picker_reset = fixedClockNode_auto_out_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign picker_auto_in_1_a_valid = subsystem_mbus_xbar_auto_out_1_a_valid; // @[LazyModule.scala 377:16]
  assign picker_auto_in_1_a_bits_opcode = subsystem_mbus_xbar_auto_out_1_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign picker_auto_in_1_a_bits_param = subsystem_mbus_xbar_auto_out_1_a_bits_param; // @[LazyModule.scala 377:16]
  assign picker_auto_in_1_a_bits_size = subsystem_mbus_xbar_auto_out_1_a_bits_size; // @[LazyModule.scala 377:16]
  assign picker_auto_in_1_a_bits_source = subsystem_mbus_xbar_auto_out_1_a_bits_source; // @[LazyModule.scala 377:16]
  assign picker_auto_in_1_a_bits_address = subsystem_mbus_xbar_auto_out_1_a_bits_address; // @[LazyModule.scala 377:16]
  assign picker_auto_in_1_a_bits_user_amba_prot_bufferable =
    subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign picker_auto_in_1_a_bits_user_amba_prot_modifiable =
    subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign picker_auto_in_1_a_bits_user_amba_prot_readalloc =
    subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign picker_auto_in_1_a_bits_user_amba_prot_writealloc =
    subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign picker_auto_in_1_a_bits_user_amba_prot_privileged =
    subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign picker_auto_in_1_a_bits_user_amba_prot_secure = subsystem_mbus_xbar_auto_out_1_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign picker_auto_in_1_a_bits_mask = subsystem_mbus_xbar_auto_out_1_a_bits_mask; // @[LazyModule.scala 377:16]
  assign picker_auto_in_1_a_bits_data = subsystem_mbus_xbar_auto_out_1_a_bits_data; // @[LazyModule.scala 377:16]
  assign picker_auto_in_1_d_ready = subsystem_mbus_xbar_auto_out_1_d_ready; // @[LazyModule.scala 377:16]
  assign picker_auto_in_0_a_valid = subsystem_mbus_xbar_auto_out_0_a_valid; // @[LazyModule.scala 377:16]
  assign picker_auto_in_0_a_bits_opcode = subsystem_mbus_xbar_auto_out_0_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign picker_auto_in_0_a_bits_param = subsystem_mbus_xbar_auto_out_0_a_bits_param; // @[LazyModule.scala 377:16]
  assign picker_auto_in_0_a_bits_size = subsystem_mbus_xbar_auto_out_0_a_bits_size; // @[LazyModule.scala 377:16]
  assign picker_auto_in_0_a_bits_source = subsystem_mbus_xbar_auto_out_0_a_bits_source; // @[LazyModule.scala 377:16]
  assign picker_auto_in_0_a_bits_address = subsystem_mbus_xbar_auto_out_0_a_bits_address; // @[LazyModule.scala 377:16]
  assign picker_auto_in_0_a_bits_mask = subsystem_mbus_xbar_auto_out_0_a_bits_mask; // @[LazyModule.scala 377:16]
  assign picker_auto_in_0_d_ready = subsystem_mbus_xbar_auto_out_0_d_ready; // @[LazyModule.scala 377:16]
  assign picker_auto_out_1_a_ready = coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_ready; // @[LazyModule.scala 377:16]
  assign picker_auto_out_1_d_valid = coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_valid; // @[LazyModule.scala 377:16]
  assign picker_auto_out_1_d_bits_opcode = coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_opcode
    ; // @[LazyModule.scala 377:16]
  assign picker_auto_out_1_d_bits_size = coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign picker_auto_out_1_d_bits_source = coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_source
    ; // @[LazyModule.scala 377:16]
  assign picker_auto_out_1_d_bits_denied = coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_denied
    ; // @[LazyModule.scala 377:16]
  assign picker_auto_out_1_d_bits_data = coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_data; // @[LazyModule.scala 377:16]
  assign picker_auto_out_1_d_bits_corrupt =
    coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_bits_corrupt; // @[LazyModule.scala 377:16]
  assign picker_auto_out_0_a_ready = wrapped_zero_device_auto_buffer_in_a_ready; // @[LazyModule.scala 377:16]
  assign picker_auto_out_0_d_valid = wrapped_zero_device_auto_buffer_in_d_valid; // @[LazyModule.scala 377:16]
  assign picker_auto_out_0_d_bits_opcode = wrapped_zero_device_auto_buffer_in_d_bits_opcode; // @[LazyModule.scala 377:16]
  assign picker_auto_out_0_d_bits_size = wrapped_zero_device_auto_buffer_in_d_bits_size; // @[LazyModule.scala 377:16]
  assign picker_auto_out_0_d_bits_source = wrapped_zero_device_auto_buffer_in_d_bits_source; // @[LazyModule.scala 377:16]
  assign wrapped_zero_device_clock = fixedClockNode_auto_out_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign wrapped_zero_device_reset = fixedClockNode_auto_out_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign wrapped_zero_device_auto_buffer_in_a_valid = picker_auto_out_0_a_valid; // @[LazyModule.scala 377:16]
  assign wrapped_zero_device_auto_buffer_in_a_bits_opcode = picker_auto_out_0_a_bits_opcode; // @[LazyModule.scala 377:16]
  assign wrapped_zero_device_auto_buffer_in_a_bits_param = picker_auto_out_0_a_bits_param; // @[LazyModule.scala 377:16]
  assign wrapped_zero_device_auto_buffer_in_a_bits_size = picker_auto_out_0_a_bits_size; // @[LazyModule.scala 377:16]
  assign wrapped_zero_device_auto_buffer_in_a_bits_source = picker_auto_out_0_a_bits_source; // @[LazyModule.scala 377:16]
  assign wrapped_zero_device_auto_buffer_in_a_bits_address = picker_auto_out_0_a_bits_address; // @[LazyModule.scala 377:16]
  assign wrapped_zero_device_auto_buffer_in_a_bits_mask = picker_auto_out_0_a_bits_mask; // @[LazyModule.scala 377:16]
  assign wrapped_zero_device_auto_buffer_in_d_ready = picker_auto_out_0_d_ready; // @[LazyModule.scala 377:16]
  assign buffer_auto_in_3_a_valid = auto_bus_xing_in_3_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_opcode = auto_bus_xing_in_3_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_param = auto_bus_xing_in_3_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_size = auto_bus_xing_in_3_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_source = auto_bus_xing_in_3_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_address = auto_bus_xing_in_3_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_user_amba_prot_bufferable = auto_bus_xing_in_3_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_user_amba_prot_modifiable = auto_bus_xing_in_3_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_user_amba_prot_readalloc = auto_bus_xing_in_3_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_user_amba_prot_writealloc = auto_bus_xing_in_3_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_user_amba_prot_privileged = auto_bus_xing_in_3_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_user_amba_prot_secure = auto_bus_xing_in_3_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_mask = auto_bus_xing_in_3_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_a_bits_data = auto_bus_xing_in_3_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_3_d_ready = auto_bus_xing_in_3_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_valid = auto_bus_xing_in_2_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_opcode = auto_bus_xing_in_2_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_param = auto_bus_xing_in_2_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_size = auto_bus_xing_in_2_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_source = auto_bus_xing_in_2_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_address = auto_bus_xing_in_2_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_user_amba_prot_bufferable = auto_bus_xing_in_2_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_user_amba_prot_modifiable = auto_bus_xing_in_2_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_user_amba_prot_readalloc = auto_bus_xing_in_2_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_user_amba_prot_writealloc = auto_bus_xing_in_2_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_user_amba_prot_privileged = auto_bus_xing_in_2_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_user_amba_prot_secure = auto_bus_xing_in_2_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_mask = auto_bus_xing_in_2_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_a_bits_data = auto_bus_xing_in_2_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_2_d_ready = auto_bus_xing_in_2_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_valid = auto_bus_xing_in_1_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_opcode = auto_bus_xing_in_1_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_param = auto_bus_xing_in_1_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_size = auto_bus_xing_in_1_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_source = auto_bus_xing_in_1_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_address = auto_bus_xing_in_1_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_user_amba_prot_bufferable = auto_bus_xing_in_1_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_user_amba_prot_modifiable = auto_bus_xing_in_1_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_user_amba_prot_readalloc = auto_bus_xing_in_1_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_user_amba_prot_writealloc = auto_bus_xing_in_1_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_user_amba_prot_privileged = auto_bus_xing_in_1_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_user_amba_prot_secure = auto_bus_xing_in_1_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_mask = auto_bus_xing_in_1_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_a_bits_data = auto_bus_xing_in_1_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_1_d_ready = auto_bus_xing_in_1_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_valid = auto_bus_xing_in_0_a_valid; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_opcode = auto_bus_xing_in_0_a_bits_opcode; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_param = auto_bus_xing_in_0_a_bits_param; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_size = auto_bus_xing_in_0_a_bits_size; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_source = auto_bus_xing_in_0_a_bits_source; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_address = auto_bus_xing_in_0_a_bits_address; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_user_amba_prot_bufferable = auto_bus_xing_in_0_a_bits_user_amba_prot_bufferable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_user_amba_prot_modifiable = auto_bus_xing_in_0_a_bits_user_amba_prot_modifiable; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_user_amba_prot_readalloc = auto_bus_xing_in_0_a_bits_user_amba_prot_readalloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_user_amba_prot_writealloc = auto_bus_xing_in_0_a_bits_user_amba_prot_writealloc; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_user_amba_prot_privileged = auto_bus_xing_in_0_a_bits_user_amba_prot_privileged; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_user_amba_prot_secure = auto_bus_xing_in_0_a_bits_user_amba_prot_secure; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_mask = auto_bus_xing_in_0_a_bits_mask; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_a_bits_data = auto_bus_xing_in_0_a_bits_data; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_in_0_d_ready = auto_bus_xing_in_0_d_ready; // @[Nodes.scala 1529:13 LazyModule.scala 388:16]
  assign buffer_auto_out_3_a_ready = fixer_auto_in_3_a_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_valid = fixer_auto_in_3_d_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_bits_opcode = fixer_auto_in_3_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_bits_size = fixer_auto_in_3_d_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_bits_source = fixer_auto_in_3_d_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_bits_denied = fixer_auto_in_3_d_bits_denied; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_bits_data = fixer_auto_in_3_d_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_3_d_bits_corrupt = fixer_auto_in_3_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_a_ready = fixer_auto_in_2_a_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_valid = fixer_auto_in_2_d_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_bits_opcode = fixer_auto_in_2_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_bits_size = fixer_auto_in_2_d_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_bits_source = fixer_auto_in_2_d_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_bits_denied = fixer_auto_in_2_d_bits_denied; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_bits_data = fixer_auto_in_2_d_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_2_d_bits_corrupt = fixer_auto_in_2_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_a_ready = fixer_auto_in_1_a_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_valid = fixer_auto_in_1_d_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_bits_opcode = fixer_auto_in_1_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_bits_size = fixer_auto_in_1_d_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_bits_source = fixer_auto_in_1_d_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_bits_denied = fixer_auto_in_1_d_bits_denied; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_bits_data = fixer_auto_in_1_d_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_1_d_bits_corrupt = fixer_auto_in_1_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_a_ready = fixer_auto_in_0_a_ready; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_valid = fixer_auto_in_0_d_valid; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_bits_opcode = fixer_auto_in_0_d_bits_opcode; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_bits_size = fixer_auto_in_0_d_bits_size; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_bits_source = fixer_auto_in_0_d_bits_source; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_bits_denied = fixer_auto_in_0_d_bits_denied; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_bits_data = fixer_auto_in_0_d_bits_data; // @[LazyModule.scala 375:16]
  assign buffer_auto_out_0_d_bits_corrupt = fixer_auto_in_0_d_bits_corrupt; // @[LazyModule.scala 375:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_clock = fixedClockNode_auto_out_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_reset = fixedClockNode_auto_out_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 375:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_valid = picker_auto_out_1_a_valid; // @[LazyModule.scala 377:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_opcode = picker_auto_out_1_a_bits_opcode
    ; // @[LazyModule.scala 377:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_param = picker_auto_out_1_a_bits_param; // @[LazyModule.scala 377:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_size = picker_auto_out_1_a_bits_size; // @[LazyModule.scala 377:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_source = picker_auto_out_1_a_bits_source
    ; // @[LazyModule.scala 377:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_address =
    picker_auto_out_1_a_bits_address; // @[LazyModule.scala 377:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_bufferable =
    picker_auto_out_1_a_bits_user_amba_prot_bufferable; // @[LazyModule.scala 377:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_modifiable =
    picker_auto_out_1_a_bits_user_amba_prot_modifiable; // @[LazyModule.scala 377:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_readalloc =
    picker_auto_out_1_a_bits_user_amba_prot_readalloc; // @[LazyModule.scala 377:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_writealloc =
    picker_auto_out_1_a_bits_user_amba_prot_writealloc; // @[LazyModule.scala 377:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_privileged =
    picker_auto_out_1_a_bits_user_amba_prot_privileged; // @[LazyModule.scala 377:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_user_amba_prot_secure =
    picker_auto_out_1_a_bits_user_amba_prot_secure; // @[LazyModule.scala 377:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_mask = picker_auto_out_1_a_bits_mask; // @[LazyModule.scala 377:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_a_bits_data = picker_auto_out_1_a_bits_data; // @[LazyModule.scala 377:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_buffer_in_d_ready = picker_auto_out_1_d_ready; // @[LazyModule.scala 377:16]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_aw_ready =
    auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_aw_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_w_ready =
    auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_w_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_b_valid =
    auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_b_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_b_bits_id =
    auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_b_bits_id; // @[LazyModule.scala 390:12]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_b_bits_resp =
    auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_b_bits_resp; // @[LazyModule.scala 390:12]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_ar_ready =
    auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_ar_ready; // @[LazyModule.scala 390:12]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_valid =
    auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_r_valid; // @[LazyModule.scala 390:12]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_bits_id =
    auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_r_bits_id; // @[LazyModule.scala 390:12]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_bits_data =
    auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_r_bits_data; // @[LazyModule.scala 390:12]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_bits_resp =
    auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_r_bits_resp; // @[LazyModule.scala 390:12]
  assign coupler_to_memory_controller_named_axi4_mem_port_auto_axi4buf_out_r_bits_last =
    auto_coupler_to_memory_controller_named_axi4_mem_port_axi4buf_out_r_bits_last; // @[LazyModule.scala 390:12]
endmodule
