//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__TraceBTM_1(
  input         rf_reset,
  input         clock,
  input         reset,
  input         io_teActive,
  input  [3:0]  io_teControl_teSyncMaxBTM,
  input         io_teControl_teInhibitSrc,
  input  [2:0]  io_teControl_teInstruction,
  input         io_teControl_teEnable,
  input  [1:0]  io_tsControl_tsBranch,
  input         io_tsControl_tsEnable,
  output        io_btmEvent_ready,
  input         io_btmEvent_valid,
  input  [4:0]  io_btmEvent_bits_reason,
  input  [38:0] io_btmEvent_bits_iaddr,
  input  [13:0] io_btmEvent_bits_icnt,
  input  [31:0] io_btmEvent_bits_hist,
  input  [1:0]  io_btmEvent_bits_hisv,
  input         io_btm_ready,
  output        io_btm_valid,
  output [4:0]  io_btm_bits_length,
  output [1:0]  io_btm_bits_timestamp,
  output [5:0]  io_btm_bits_slices_0_data,
  output [1:0]  io_btm_bits_slices_0_mseo,
  output [5:0]  io_btm_bits_slices_1_data,
  output [1:0]  io_btm_bits_slices_1_mseo,
  output [5:0]  io_btm_bits_slices_2_data,
  output [1:0]  io_btm_bits_slices_2_mseo,
  output [5:0]  io_btm_bits_slices_3_data,
  output [1:0]  io_btm_bits_slices_3_mseo,
  output [5:0]  io_btm_bits_slices_4_data,
  output [1:0]  io_btm_bits_slices_4_mseo,
  output [5:0]  io_btm_bits_slices_5_data,
  output [1:0]  io_btm_bits_slices_5_mseo,
  output [5:0]  io_btm_bits_slices_6_data,
  output [1:0]  io_btm_bits_slices_6_mseo,
  output [5:0]  io_btm_bits_slices_7_data,
  output [1:0]  io_btm_bits_slices_7_mseo,
  output [5:0]  io_btm_bits_slices_8_data,
  output [1:0]  io_btm_bits_slices_8_mseo,
  output [5:0]  io_btm_bits_slices_9_data,
  output [1:0]  io_btm_bits_slices_9_mseo,
  output [5:0]  io_btm_bits_slices_10_data,
  output [1:0]  io_btm_bits_slices_10_mseo,
  output [5:0]  io_btm_bits_slices_11_data,
  output [1:0]  io_btm_bits_slices_11_mseo,
  output [5:0]  io_btm_bits_slices_12_data,
  output [1:0]  io_btm_bits_slices_12_mseo,
  output [5:0]  io_btm_bits_slices_13_data,
  output [1:0]  io_btm_bits_slices_13_mseo,
  output [5:0]  io_btm_bits_slices_14_data,
  output [1:0]  io_btm_bits_slices_14_mseo,
  output [5:0]  io_btm_bits_slices_15_data,
  output [1:0]  io_btm_bits_slices_15_mseo,
  output [5:0]  io_btm_bits_slices_16_data,
  output [1:0]  io_btm_bits_slices_16_mseo,
  output [5:0]  io_btm_bits_slices_17_data,
  output [1:0]  io_btm_bits_slices_17_mseo,
  output [5:0]  io_btm_bits_slices_18_data,
  output [1:0]  io_btm_bits_slices_18_mseo,
  output [5:0]  io_btm_bits_slices_19_data,
  output [1:0]  io_btm_bits_slices_19_mseo,
  output [5:0]  io_btm_bits_slices_20_data,
  output [1:0]  io_btm_bits_slices_20_mseo,
  output [5:0]  io_btm_bits_slices_21_data,
  output [1:0]  io_btm_bits_slices_21_mseo,
  output [5:0]  io_btm_bits_slices_22_data,
  output [1:0]  io_btm_bits_slices_22_mseo,
  output [5:0]  io_btm_bits_slices_23_data,
  output [1:0]  io_btm_bits_slices_23_mseo,
  output [5:0]  io_btm_bits_slices_24_data,
  output [1:0]  io_btm_bits_slices_24_mseo
);
`ifdef RANDOMIZE_REG_INIT
  reg [63:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
`endif // RANDOMIZE_REG_INIT
  reg [38:0] lastaddr; // @[TraceBTM.scala 105:25]
  wire [38:0] uaddr = io_btmEvent_bits_iaddr ^ lastaddr; // @[TraceBTM.scala 110:33]
  reg [16:0] btmCount; // @[TraceBTM.scala 112:25]
  wire [3:0] _needsync_T_1 = io_teControl_teSyncMaxBTM + 4'h5; // @[TraceBTM.scala 113:75]
  wire [15:0] _needsync_T_2 = 16'h1 << _needsync_T_1; // @[TraceBTM.scala 113:45]
  wire [16:0] _GEN_313 = {{1'd0}, _needsync_T_2}; // @[TraceBTM.scala 113:37]
  wire  needsync = btmCount >= _GEN_313; // @[TraceBTM.scala 113:37]
  wire  histNZ = io_btmEvent_bits_hisv == 2'h1; // @[TraceBTM.scala 152:30]
  wire  _tcode_T_1 = io_btmEvent_bits_reason == 5'h16; // @[TraceBTM.scala 171:33]
  wire  _tcode_T_2 = io_btmEvent_bits_reason == 5'h15 | _tcode_T_1; // @[TraceBTM.scala 170:49]
  wire [4:0] _tcode_T_5 = histNZ ? 5'h1c : 5'h4; // @[TraceBTM.scala 171:55]
  wire  _tcode_T_7 = io_btmEvent_bits_reason == 5'h1e; // @[TraceBTM.scala 173:33]
  wire  _tcode_T_8 = io_btmEvent_bits_reason == 5'h1d | _tcode_T_7; // @[TraceBTM.scala 172:48]
  wire  _tcode_T_9 = io_btmEvent_bits_reason == 5'h2; // @[TraceBTM.scala 174:33]
  wire  _tcode_T_10 = _tcode_T_8 | _tcode_T_9; // @[TraceBTM.scala 173:48]
  wire  _tcode_T_11 = io_btmEvent_bits_reason == 5'h0; // @[TraceBTM.scala 175:33]
  wire  _tcode_T_12 = _tcode_T_10 | _tcode_T_11; // @[TraceBTM.scala 174:49]
  wire  _tcode_T_13 = io_btmEvent_bits_reason == 5'h6; // @[TraceBTM.scala 176:33]
  wire  _tcode_T_14 = _tcode_T_12 | _tcode_T_13; // @[TraceBTM.scala 175:51]
  wire [4:0] _tcode_T_17 = histNZ ? 5'h1d : 5'hc; // @[TraceBTM.scala 176:55]
  wire  _tcode_T_18 = io_btmEvent_bits_reason == 5'h12; // @[TraceBTM.scala 177:33]
  wire  _tcode_T_19 = io_btmEvent_bits_reason == 5'h13; // @[TraceBTM.scala 178:33]
  wire  _tcode_T_20 = io_btmEvent_bits_reason == 5'h12 | _tcode_T_19; // @[TraceBTM.scala 177:51]
  wire  _tcode_T_21 = io_btmEvent_bits_reason == 5'h11; // @[TraceBTM.scala 179:33]
  wire  _tcode_T_22 = _tcode_T_20 | _tcode_T_21; // @[TraceBTM.scala 178:51]
  wire  _tcode_T_24 = io_btmEvent_bits_reason == 5'h19; // @[TraceBTM.scala 180:33]
  wire  _tcode_T_25 = io_btmEvent_bits_reason == 5'h18; // @[TraceBTM.scala 181:33]
  wire  _tcode_T_26 = io_btmEvent_bits_reason == 5'h19 | _tcode_T_25; // @[TraceBTM.scala 180:49]
  wire  _tcode_T_28 = io_btmEvent_bits_reason == 5'h17; // @[TraceBTM.scala 182:33]
  wire  _tcode_T_31 = io_btmEvent_bits_reason == 5'he; // @[TraceBTM.scala 184:33]
  wire  _tcode_T_32 = io_btmEvent_bits_reason == 5'h8 | _tcode_T_31; // @[TraceBTM.scala 183:50]
  wire  _tcode_T_34 = io_btmEvent_bits_reason == 5'hf; // @[TraceBTM.scala 185:33]
  wire [5:0] _tcode_T_37 = needsync ? 6'h23 : 6'h22; // @[TraceBTM.scala 185:55]
  wire [5:0] _tcode_T_38 = _tcode_T_34 ? _tcode_T_37 : 6'h9; // @[Mux.scala 98:16]
  wire [5:0] _tcode_T_39 = _tcode_T_32 ? 6'h22 : _tcode_T_38; // @[Mux.scala 98:16]
  wire [5:0] _tcode_T_40 = _tcode_T_28 ? 6'h8 : _tcode_T_39; // @[Mux.scala 98:16]
  wire [5:0] _tcode_T_41 = _tcode_T_26 ? 6'h1b : _tcode_T_40; // @[Mux.scala 98:16]
  wire [5:0] _tcode_T_42 = _tcode_T_22 ? 6'h21 : _tcode_T_41; // @[Mux.scala 98:16]
  wire [5:0] _tcode_T_43 = _tcode_T_14 ? {{1'd0}, _tcode_T_17} : _tcode_T_42; // @[Mux.scala 98:16]
  wire [5:0] tcode = _tcode_T_2 ? {{1'd0}, _tcode_T_5} : _tcode_T_43; // @[Mux.scala 98:16]
  wire [2:0] _tcSrcBits_T = io_teControl_teInhibitSrc ? 3'h6 : 3'h7; // @[TraceBTM.scala 190:19]
  wire  hasSync = 6'h9 == tcode | 6'hc == tcode | 6'h1d == tcode | 6'h23 == tcode; // @[TraceBTM.scala 29:38]
  wire  _hasIcnt_T_7 = 6'h8 == tcode | 6'h22 == tcode | 6'h23 == tcode; // @[TraceBTM.scala 29:38]
  wire  hasIcnt = ~_hasIcnt_T_7; // @[TraceBTM.scala 195:14]
  wire  _hasAddr_T_7 = 6'h4 == tcode | 6'h22 == tcode | 6'h1c == tcode; // @[TraceBTM.scala 29:38]
  wire  hasAddr = hasSync | _hasAddr_T_7; // @[TraceBTM.scala 196:22]
  wire  _hasHist_T_7 = 6'h1c == tcode | 6'h1d == tcode | 6'h21 == tcode; // @[TraceBTM.scala 29:38]
  wire  hasHist = histNZ & _hasHist_T_7; // @[TraceBTM.scala 197:21]
  wire  _hasPrefix_T_14 = 6'h4 == tcode | 6'hc == tcode | 6'h1c == tcode | 6'h1d == tcode | 6'h21 == tcode; // @[TraceBTM.scala 29:38]
  wire  hasPrefix = io_btmEvent_bits_hisv[1] & _hasPrefix_T_14; // @[TraceBTM.scala 198:24]
  wire [3:0] sync = io_btmEvent_bits_reason[4] ? 4'h2 : io_btmEvent_bits_reason[3:0]; // @[TraceBTM.scala 202:14]
  wire  _btype_T_2 = _tcode_T_1 | _tcode_T_7; // @[TraceBTM.scala 204:58]
  wire [3:0] _evcode_T_4 = _tcode_T_19 ? 4'h8 : 4'h4; // @[Mux.scala 98:16]
  wire [3:0] _evcode_T_5 = _tcode_T_18 ? 4'h0 : _evcode_T_4; // @[Mux.scala 98:16]
  wire [4:0] _evcode_T_6 = hasHist ? 5'h10 : 5'h0; // @[TraceBTM.scala 209:43]
  wire [4:0] _GEN_314 = {{1'd0}, _evcode_T_5}; // @[TraceBTM.scala 209:38]
  wire [4:0] _evcode_T_7 = _GEN_314 | _evcode_T_6; // @[TraceBTM.scala 209:38]
  wire [3:0] _rcode_T_3 = io_btmEvent_bits_hisv[0] ? 4'h9 : 4'h8; // @[TraceBTM.scala 212:62]
  wire [3:0] _rcode_T_4 = io_btmEvent_bits_hisv[1] ? _rcode_T_3 : 4'h1; // @[TraceBTM.scala 212:49]
  wire [3:0] rcode = _tcode_T_24 ? _rcode_T_4 : 4'h0; // @[Mux.scala 98:16]
  wire [31:0] _rdata_T_1 = _tcode_T_25 ? {{18'd0}, io_btmEvent_bits_icnt} : io_btmEvent_bits_hist; // @[TraceBTM.scala 215:15]
  wire  _icntF1_T_1 = tcode == 6'h4; // @[TraceBTM.scala 222:13]
  wire  _icntF1_T_3 = tcode == 6'h1c; // @[TraceBTM.scala 223:13]
  wire  _icntF1_T_4 = _icntF1_T_1 | _icntF1_T_3; // @[TraceBTM.scala 222:36]
  wire [1:0] btype = {{1'd0}, _btype_T_2};
  wire [21:0] _icntF1_T_5 = {io_btmEvent_bits_icnt,btype,tcode}; // @[Cat.scala 30:58]
  wire  _icntF1_T_7 = tcode == 6'hb; // @[TraceBTM.scala 224:13]
  wire  _icntF1_T_9 = tcode == 6'h9; // @[TraceBTM.scala 225:13]
  wire  _icntF1_T_10 = tcode == 6'hb | _icntF1_T_9; // @[TraceBTM.scala 224:38]
  wire [23:0] _icntF1_T_11 = {io_btmEvent_bits_icnt,sync,tcode}; // @[Cat.scala 30:58]
  wire  _icntF1_T_13 = tcode == 6'hc; // @[TraceBTM.scala 226:13]
  wire  _icntF1_T_15 = tcode == 6'h1d; // @[TraceBTM.scala 227:13]
  wire  _icntF1_T_16 = _icntF1_T_13 | _icntF1_T_15; // @[TraceBTM.scala 226:40]
  wire [25:0] _icntF1_T_17 = {io_btmEvent_bits_icnt,btype,sync,tcode}; // @[Cat.scala 30:58]
  wire  _icntF1_T_19 = tcode == 6'h21; // @[TraceBTM.scala 228:13]
  wire [5:0] evcode = {{1'd0}, _evcode_T_7};
  wire [25:0] _icntF1_T_20 = {io_btmEvent_bits_icnt,evcode,tcode}; // @[Cat.scala 30:58]
  wire  _icntF1_T_22 = tcode == 6'h1b; // @[TraceBTM.scala 229:13]
  wire [45:0] rdata = {{14'd0}, _rdata_T_1};
  wire [55:0] _icntF1_T_23 = {rdata,rcode,tcode}; // @[Cat.scala 30:58]
  wire  _icntF1_T_25 = tcode == 6'h8; // @[TraceBTM.scala 230:13]
  wire [9:0] _icntF1_T_26 = {4'h0,tcode}; // @[Cat.scala 30:58]
  wire [19:0] icntF1_x10 = {io_btmEvent_bits_icnt,tcode}; // @[Cat.scala 30:58]
  wire [19:0] _icntF1_T_27 = _icntF1_T_25 ? {{10'd0}, _icntF1_T_26} : icntF1_x10; // @[Mux.scala 98:16]
  wire [55:0] _icntF1_T_28 = _icntF1_T_22 ? _icntF1_T_23 : {{36'd0}, _icntF1_T_27}; // @[Mux.scala 98:16]
  wire [55:0] _icntF1_T_29 = _icntF1_T_19 ? {{30'd0}, _icntF1_T_20} : _icntF1_T_28; // @[Mux.scala 98:16]
  wire [55:0] _icntF1_T_30 = _icntF1_T_16 ? {{30'd0}, _icntF1_T_17} : _icntF1_T_29; // @[Mux.scala 98:16]
  wire [55:0] _icntF1_T_31 = _icntF1_T_10 ? {{32'd0}, _icntF1_T_11} : _icntF1_T_30; // @[Mux.scala 98:16]
  wire [55:0] _icntF1_T_32 = _icntF1_T_4 ? {{34'd0}, _icntF1_T_5} : _icntF1_T_31; // @[Mux.scala 98:16]
  wire [22:0] _icntF2_T_5 = {io_btmEvent_bits_icnt,btype,1'h1,tcode}; // @[Cat.scala 30:58]
  wire [24:0] _icntF2_T_11 = {io_btmEvent_bits_icnt,sync,1'h1,tcode}; // @[Cat.scala 30:58]
  wire [26:0] _icntF2_T_17 = {io_btmEvent_bits_icnt,btype,sync,1'h1,tcode}; // @[Cat.scala 30:58]
  wire [26:0] _icntF2_T_20 = {io_btmEvent_bits_icnt,evcode,1'h1,tcode}; // @[Cat.scala 30:58]
  wire [56:0] _icntF2_T_23 = {rdata,rcode,1'h1,tcode}; // @[Cat.scala 30:58]
  wire [10:0] _icntF2_T_26 = {4'h0,1'h1,tcode}; // @[Cat.scala 30:58]
  wire [20:0] icntF2_x12 = {io_btmEvent_bits_icnt,1'h1,tcode}; // @[Cat.scala 30:58]
  wire [20:0] _icntF2_T_27 = _icntF1_T_25 ? {{10'd0}, _icntF2_T_26} : icntF2_x12; // @[Mux.scala 98:16]
  wire [56:0] _icntF2_T_28 = _icntF1_T_22 ? _icntF2_T_23 : {{36'd0}, _icntF2_T_27}; // @[Mux.scala 98:16]
  wire [56:0] _icntF2_T_29 = _icntF1_T_19 ? {{30'd0}, _icntF2_T_20} : _icntF2_T_28; // @[Mux.scala 98:16]
  wire [56:0] _icntF2_T_30 = _icntF1_T_16 ? {{30'd0}, _icntF2_T_17} : _icntF2_T_29; // @[Mux.scala 98:16]
  wire [56:0] _icntF2_T_31 = _icntF1_T_10 ? {{32'd0}, _icntF2_T_11} : _icntF2_T_30; // @[Mux.scala 98:16]
  wire [56:0] _icntF2_T_32 = _icntF1_T_4 ? {{34'd0}, _icntF2_T_5} : _icntF2_T_31; // @[Mux.scala 98:16]
  wire [45:0] icntF1 = _icntF1_T_32[45:0];
  wire [45:0] icntF2 = _icntF2_T_32[45:0];
  wire [45:0] icntF = io_teControl_teInhibitSrc ? icntF1 : icntF2; // @[TraceBTM.scala 245:15]
  wire [4:0] tcSrcBits = {{2'd0}, _tcSrcBits_T};
  wire [4:0] _fixedBits_T_6 = tcSrcBits + 5'h2; // @[TraceBTM.scala 249:49]
  wire  _fixedBits_T_11 = _icntF1_T_7 | _icntF1_T_25; // @[TraceBTM.scala 250:38]
  wire  _fixedBits_T_14 = _fixedBits_T_11 | _icntF1_T_22; // @[TraceBTM.scala 251:33]
  wire  _fixedBits_T_17 = _fixedBits_T_14 | _icntF1_T_9; // @[TraceBTM.scala 252:36]
  wire [4:0] _fixedBits_T_19 = tcSrcBits + 5'h4; // @[TraceBTM.scala 253:49]
  wire  _fixedBits_T_27 = _icntF1_T_16 | _icntF1_T_19; // @[TraceBTM.scala 255:39]
  wire [4:0] _fixedBits_T_29 = tcSrcBits + 5'h6; // @[TraceBTM.scala 256:49]
  wire [4:0] _fixedBits_T_30 = _fixedBits_T_27 ? _fixedBits_T_29 : tcSrcBits; // @[Mux.scala 98:16]
  wire [4:0] _fixedBits_T_31 = _fixedBits_T_17 ? _fixedBits_T_19 : _fixedBits_T_30; // @[Mux.scala 98:16]
  wire [4:0] fixedBits = _icntF1_T_4 ? _fixedBits_T_6 : _fixedBits_T_31; // @[Mux.scala 98:16]
  wire [38:0] addrSel = hasSync ? io_btmEvent_bits_iaddr : uaddr; // @[TraceBTM.scala 259:17]
  wire  _addrF_T_4 = tcode == 6'h23 | tcode == 6'h22; // @[TraceBTM.scala 261:44]
  wire [50:0] _addrF_T_5 = {addrSel,2'h0,sync,tcode}; // @[Cat.scala 30:58]
  wire [50:0] _addrF_T_6 = tcode == 6'h23 | tcode == 6'h22 ? _addrF_T_5 : {{12'd0}, addrSel}; // @[TraceBTM.scala 261:8]
  wire [51:0] _addrF_T_12 = {addrSel,2'h0,sync,1'h1,tcode}; // @[Cat.scala 30:58]
  wire [51:0] _addrF_T_13 = _addrF_T_4 ? _addrF_T_12 : {{13'd0}, addrSel}; // @[TraceBTM.scala 262:8]
  wire [51:0] _addrF_T_14 = io_teControl_teInhibitSrc ? {{1'd0}, _addrF_T_6} : _addrF_T_13; // @[TraceBTM.scala 260:16]
  wire [4:0] addrFixed = _addrF_T_4 ? _fixedBits_T_29 : 5'h0; // @[TraceBTM.scala 263:19]
  wire [41:0] _prefix_T = {io_btmEvent_bits_hist,3'h4,io_btmEvent_bits_hisv[0],6'h1b}; // @[Cat.scala 30:58]
  wire [42:0] _prefix_T_1 = {io_btmEvent_bits_hist,3'h4,io_btmEvent_bits_hisv[0],7'h5b}; // @[Cat.scala 30:58]
  wire [42:0] _prefix_T_2 = io_teControl_teInhibitSrc ? {{1'd0}, _prefix_T} : _prefix_T_1; // @[TraceBTM.scala 270:16]
  wire [2:0] _prefixEnd_T = hasPrefix ? 3'h4 : 3'h0; // @[TraceBTM.scala 276:19]
  wire  _icntEnd_minLastSlice_T_1 = fixedBits >= 5'h6; // @[TraceBTM.scala 80:17]
  wire [1:0] _icntEnd_minLastSlice_T_3 = fixedBits >= 5'hc ? 2'h2 : {{1'd0}, _icntEnd_minLastSlice_T_1}; // @[TraceBTM.scala 79:24]
  wire [47:0] _icntEnd_lastOnes_WIRE_1 = {{2'd0}, icntF};
  wire [1:0] _icntEnd_lastOnes_T_17 = |_icntEnd_lastOnes_WIRE_1[17:12] ? 2'h2 : {{1'd0}, |_icntEnd_lastOnes_WIRE_1[11:6]
    }; // @[TraceBTM.scala 82:97]
  wire [1:0] _icntEnd_lastOnes_T_18 = |_icntEnd_lastOnes_WIRE_1[23:18] ? 2'h3 : _icntEnd_lastOnes_T_17; // @[TraceBTM.scala 82:97]
  wire [2:0] _icntEnd_lastOnes_T_19 = |_icntEnd_lastOnes_WIRE_1[29:24] ? 3'h4 : {{1'd0}, _icntEnd_lastOnes_T_18}; // @[TraceBTM.scala 82:97]
  wire [2:0] _icntEnd_lastOnes_T_20 = |_icntEnd_lastOnes_WIRE_1[35:30] ? 3'h5 : _icntEnd_lastOnes_T_19; // @[TraceBTM.scala 82:97]
  wire [2:0] _icntEnd_lastOnes_T_21 = |_icntEnd_lastOnes_WIRE_1[41:36] ? 3'h6 : _icntEnd_lastOnes_T_20; // @[TraceBTM.scala 82:97]
  wire [2:0] icntEnd_lastOnes = |_icntEnd_lastOnes_WIRE_1[47:42] ? 3'h7 : _icntEnd_lastOnes_T_21; // @[TraceBTM.scala 82:97]
  wire [2:0] icntEnd_minLastSlice = {{1'd0}, _icntEnd_minLastSlice_T_3};
  wire [2:0] _icntEnd_T_7 = icntEnd_lastOnes < icntEnd_minLastSlice ? icntEnd_minLastSlice : icntEnd_lastOnes; // @[TraceBTM.scala 83:15]
  wire [3:0] _icntEnd_T_8 = _icntEnd_T_7 + 3'h1; // @[TraceBTM.scala 83:33]
  wire [1:0] _icntEnd_T_20 = fixedBits > 5'hc ? 2'h3 : 2'h2; // @[TraceBTM.scala 281:8]
  wire [1:0] _icntEnd_T_21 = hasAddr ? 2'h0 : _icntEnd_T_20; // @[TraceBTM.scala 285:8]
  wire [3:0] _icntEnd_T_22 = hasIcnt ? _icntEnd_T_8 : {{2'd0}, _icntEnd_T_21}; // @[TraceBTM.scala 284:29]
  wire [4:0] prefixEnd = {{2'd0}, _prefixEnd_T};
  wire [4:0] _GEN_315 = {{1'd0}, _icntEnd_T_22}; // @[TraceBTM.scala 284:24]
  wire [4:0] icntEnd = prefixEnd + _GEN_315; // @[TraceBTM.scala 284:24]
  wire  _addrEnd_minLastSlice_T_1 = addrFixed >= 5'h6; // @[TraceBTM.scala 80:17]
  wire [1:0] _addrEnd_minLastSlice_T_3 = addrFixed >= 5'hc ? 2'h2 : {{1'd0}, _addrEnd_minLastSlice_T_1}; // @[TraceBTM.scala 79:24]
  wire [54:0] addrF = {{3'd0}, _addrF_T_14};
  wire [59:0] _addrEnd_lastOnes_WIRE_1 = {{5'd0}, addrF};
  wire [1:0] _addrEnd_lastOnes_T_21 = |_addrEnd_lastOnes_WIRE_1[17:12] ? 2'h2 : {{1'd0}, |_addrEnd_lastOnes_WIRE_1[11:6]
    }; // @[TraceBTM.scala 82:97]
  wire [1:0] _addrEnd_lastOnes_T_22 = |_addrEnd_lastOnes_WIRE_1[23:18] ? 2'h3 : _addrEnd_lastOnes_T_21; // @[TraceBTM.scala 82:97]
  wire [2:0] _addrEnd_lastOnes_T_23 = |_addrEnd_lastOnes_WIRE_1[29:24] ? 3'h4 : {{1'd0}, _addrEnd_lastOnes_T_22}; // @[TraceBTM.scala 82:97]
  wire [2:0] _addrEnd_lastOnes_T_24 = |_addrEnd_lastOnes_WIRE_1[35:30] ? 3'h5 : _addrEnd_lastOnes_T_23; // @[TraceBTM.scala 82:97]
  wire [2:0] _addrEnd_lastOnes_T_25 = |_addrEnd_lastOnes_WIRE_1[41:36] ? 3'h6 : _addrEnd_lastOnes_T_24; // @[TraceBTM.scala 82:97]
  wire [2:0] _addrEnd_lastOnes_T_26 = |_addrEnd_lastOnes_WIRE_1[47:42] ? 3'h7 : _addrEnd_lastOnes_T_25; // @[TraceBTM.scala 82:97]
  wire [3:0] _addrEnd_lastOnes_T_27 = |_addrEnd_lastOnes_WIRE_1[53:48] ? 4'h8 : {{1'd0}, _addrEnd_lastOnes_T_26}; // @[TraceBTM.scala 82:97]
  wire [3:0] addrEnd_lastOnes = |_addrEnd_lastOnes_WIRE_1[59:54] ? 4'h9 : _addrEnd_lastOnes_T_27; // @[TraceBTM.scala 82:97]
  wire [2:0] addrEnd_minLastSlice = {{1'd0}, _addrEnd_minLastSlice_T_3};
  wire [3:0] _GEN_316 = {{1'd0}, addrEnd_minLastSlice}; // @[TraceBTM.scala 83:15]
  wire [3:0] _addrEnd_T_7 = addrEnd_lastOnes < _GEN_316 ? {{1'd0}, addrEnd_minLastSlice} : addrEnd_lastOnes; // @[TraceBTM.scala 83:15]
  wire [4:0] _addrEnd_T_8 = _addrEnd_T_7 + 4'h1; // @[TraceBTM.scala 83:33]
  wire [4:0] _addrEnd_T_10 = icntEnd + _addrEnd_T_8; // @[TraceBTM.scala 289:24]
  wire [4:0] addrEnd = hasAddr ? _addrEnd_T_10 : icntEnd; // @[TraceBTM.scala 288:18 TraceBTM.scala 289:13 TraceBTM.scala 287:11]
  wire [31:0] histF = hasHist ? io_btmEvent_bits_hist : 32'h0; // @[TraceBTM.scala 294:20 TraceBTM.scala 295:13]
  wire [35:0] _histEnd_lastOnes_WIRE_1 = {{4'd0}, histF};
  wire [1:0] _histEnd_lastOnes_T_13 = |_histEnd_lastOnes_WIRE_1[17:12] ? 2'h2 : {{1'd0}, |_histEnd_lastOnes_WIRE_1[11:6]
    }; // @[TraceBTM.scala 71:97]
  wire [1:0] _histEnd_lastOnes_T_14 = |_histEnd_lastOnes_WIRE_1[23:18] ? 2'h3 : _histEnd_lastOnes_T_13; // @[TraceBTM.scala 71:97]
  wire [2:0] _histEnd_lastOnes_T_15 = |_histEnd_lastOnes_WIRE_1[29:24] ? 3'h4 : {{1'd0}, _histEnd_lastOnes_T_14}; // @[TraceBTM.scala 71:97]
  wire [2:0] histEnd_lastOnes = |_histEnd_lastOnes_WIRE_1[35:30] ? 3'h5 : _histEnd_lastOnes_T_15; // @[TraceBTM.scala 71:97]
  wire [3:0] _histEnd_T = histEnd_lastOnes + 3'h1; // @[TraceBTM.scala 72:14]
  wire [4:0] _GEN_317 = {{1'd0}, _histEnd_T}; // @[TraceBTM.scala 296:26]
  wire [4:0] _histEnd_T_2 = addrEnd + _GEN_317; // @[TraceBTM.scala 296:26]
  wire [4:0] histEnd = hasHist ? _histEnd_T_2 : addrEnd; // @[TraceBTM.scala 294:20 TraceBTM.scala 296:15 TraceBTM.scala 292:11]
  wire [1:0] _T_17 = |io_btmEvent_bits_hist[13:8] ? 2'h3 : 2'h2; // @[TraceBTM.scala 298:31]
  wire [1:0] _GEN_3 = 2'h0 == _T_17 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_4 = 2'h1 == _T_17 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_5 = 2'h2 == _T_17 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_6 = 2'h3 == _T_17 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [2:0] _GEN_318 = {{1'd0}, _T_17}; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_7 = 3'h4 == _GEN_318 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_8 = 3'h5 == _GEN_318 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_9 = 3'h6 == _GEN_318 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_10 = 3'h7 == _GEN_318 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [3:0] _GEN_322 = {{2'd0}, _T_17}; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_11 = 4'h8 == _GEN_322 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_12 = 4'h9 == _GEN_322 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_13 = 4'ha == _GEN_322 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_14 = 4'hb == _GEN_322 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_15 = 4'hc == _GEN_322 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_16 = 4'hd == _GEN_322 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_17 = 4'he == _GEN_322 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_18 = 4'hf == _GEN_322 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [4:0] _GEN_330 = {{3'd0}, _T_17}; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_19 = 5'h10 == _GEN_330 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_20 = 5'h11 == _GEN_330 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_21 = 5'h12 == _GEN_330 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_22 = 5'h13 == _GEN_330 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_23 = 5'h14 == _GEN_330 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_24 = 5'h15 == _GEN_330 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_25 = 5'h16 == _GEN_330 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_26 = 5'h17 == _GEN_330 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_27 = 5'h18 == _GEN_330 ? 2'h3 : 2'h0; // @[TraceBTM.scala 298:64 TraceBTM.scala 298:64]
  wire [1:0] _GEN_28 = hasPrefix ? _GEN_3 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_29 = hasPrefix ? _GEN_4 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_30 = hasPrefix ? _GEN_5 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_31 = hasPrefix ? _GEN_6 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_32 = hasPrefix ? _GEN_7 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_33 = hasPrefix ? _GEN_8 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_34 = hasPrefix ? _GEN_9 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_35 = hasPrefix ? _GEN_10 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_36 = hasPrefix ? _GEN_11 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_37 = hasPrefix ? _GEN_12 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_38 = hasPrefix ? _GEN_13 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_39 = hasPrefix ? _GEN_14 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_40 = hasPrefix ? _GEN_15 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_41 = hasPrefix ? _GEN_16 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_42 = hasPrefix ? _GEN_17 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_43 = hasPrefix ? _GEN_18 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_44 = hasPrefix ? _GEN_19 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_45 = hasPrefix ? _GEN_20 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_46 = hasPrefix ? _GEN_21 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_47 = hasPrefix ? _GEN_22 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_48 = hasPrefix ? _GEN_23 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_49 = hasPrefix ? _GEN_24 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_50 = hasPrefix ? _GEN_25 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_51 = hasPrefix ? _GEN_26 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [1:0] _GEN_52 = hasPrefix ? _GEN_27 : 2'h0; // @[TraceBTM.scala 298:22]
  wire [23:0] prefix = _prefix_T_2[23:0];
  wire [4:0] _btm_0_data_T_2 = 5'h0 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_0_data_T_3 = _btm_0_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_0_data_T_4 = icntF >> _btm_0_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_0_data_T_6 = 5'h0 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_0_data_T_7 = _btm_0_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_0_data_T_8 = addrF >> _btm_0_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_0_data_T_10 = 5'h0 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_0_data_T_11 = _btm_0_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_0_data_T_12 = histF >> _btm_0_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_53 = 5'h0 < addrEnd ? _btm_0_data_T_8 : {{23'd0}, _btm_0_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_54 = 5'h0 < icntEnd ? {{9'd0}, _btm_0_data_T_4} : _GEN_53; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_55 = 5'h0 < prefixEnd ? {{31'd0}, prefix} : _GEN_54; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_1_data_T_2 = 5'h1 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_1_data_T_3 = _btm_1_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_1_data_T_4 = icntF >> _btm_1_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_1_data_T_6 = 5'h1 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_1_data_T_7 = _btm_1_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_1_data_T_8 = addrF >> _btm_1_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_1_data_T_10 = 5'h1 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_1_data_T_11 = _btm_1_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_1_data_T_12 = histF >> _btm_1_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_56 = 5'h1 < addrEnd ? _btm_1_data_T_8 : {{23'd0}, _btm_1_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_57 = 5'h1 < icntEnd ? {{9'd0}, _btm_1_data_T_4} : _GEN_56; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_58 = 5'h1 < prefixEnd ? {{37'd0}, prefix[23:6]} : _GEN_57; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_2_data_T_2 = 5'h2 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_2_data_T_3 = _btm_2_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_2_data_T_4 = icntF >> _btm_2_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_2_data_T_6 = 5'h2 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_2_data_T_7 = _btm_2_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_2_data_T_8 = addrF >> _btm_2_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_2_data_T_10 = 5'h2 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_2_data_T_11 = _btm_2_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_2_data_T_12 = histF >> _btm_2_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_59 = 5'h2 < addrEnd ? _btm_2_data_T_8 : {{23'd0}, _btm_2_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_60 = 5'h2 < icntEnd ? {{9'd0}, _btm_2_data_T_4} : _GEN_59; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_61 = 5'h2 < prefixEnd ? {{43'd0}, prefix[23:12]} : _GEN_60; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_3_data_T_2 = 5'h3 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_3_data_T_3 = _btm_3_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_3_data_T_4 = icntF >> _btm_3_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_3_data_T_6 = 5'h3 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_3_data_T_7 = _btm_3_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_3_data_T_8 = addrF >> _btm_3_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_3_data_T_10 = 5'h3 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_3_data_T_11 = _btm_3_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_3_data_T_12 = histF >> _btm_3_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_62 = 5'h3 < addrEnd ? _btm_3_data_T_8 : {{23'd0}, _btm_3_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_63 = 5'h3 < icntEnd ? {{9'd0}, _btm_3_data_T_4} : _GEN_62; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_64 = 5'h3 < prefixEnd ? {{49'd0}, prefix[23:18]} : _GEN_63; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_4_data_T_2 = 5'h4 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_4_data_T_3 = _btm_4_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_4_data_T_4 = icntF >> _btm_4_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_4_data_T_6 = 5'h4 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_4_data_T_7 = _btm_4_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_4_data_T_8 = addrF >> _btm_4_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_4_data_T_10 = 5'h4 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_4_data_T_11 = _btm_4_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_4_data_T_12 = histF >> _btm_4_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_65 = 5'h4 < addrEnd ? _btm_4_data_T_8 : {{23'd0}, _btm_4_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_66 = 5'h4 < icntEnd ? {{9'd0}, _btm_4_data_T_4} : _GEN_65; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_67 = 5'h4 < prefixEnd ? 55'h0 : _GEN_66; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_5_data_T_2 = 5'h5 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_5_data_T_3 = _btm_5_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_5_data_T_4 = icntF >> _btm_5_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_5_data_T_6 = 5'h5 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_5_data_T_7 = _btm_5_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_5_data_T_8 = addrF >> _btm_5_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_5_data_T_10 = 5'h5 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_5_data_T_11 = _btm_5_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_5_data_T_12 = histF >> _btm_5_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_68 = 5'h5 < addrEnd ? _btm_5_data_T_8 : {{23'd0}, _btm_5_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_69 = 5'h5 < icntEnd ? {{9'd0}, _btm_5_data_T_4} : _GEN_68; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_70 = 5'h5 < prefixEnd ? 55'h0 : _GEN_69; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_6_data_T_2 = 5'h6 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_6_data_T_3 = _btm_6_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_6_data_T_4 = icntF >> _btm_6_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_6_data_T_6 = 5'h6 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_6_data_T_7 = _btm_6_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_6_data_T_8 = addrF >> _btm_6_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_6_data_T_10 = 5'h6 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_6_data_T_11 = _btm_6_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_6_data_T_12 = histF >> _btm_6_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_71 = 5'h6 < addrEnd ? _btm_6_data_T_8 : {{23'd0}, _btm_6_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_72 = 5'h6 < icntEnd ? {{9'd0}, _btm_6_data_T_4} : _GEN_71; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_73 = 5'h6 < prefixEnd ? 55'h0 : _GEN_72; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_7_data_T_2 = 5'h7 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_7_data_T_3 = _btm_7_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_7_data_T_4 = icntF >> _btm_7_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_7_data_T_6 = 5'h7 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_7_data_T_7 = _btm_7_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_7_data_T_8 = addrF >> _btm_7_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_7_data_T_10 = 5'h7 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_7_data_T_11 = _btm_7_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_7_data_T_12 = histF >> _btm_7_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_74 = 5'h7 < addrEnd ? _btm_7_data_T_8 : {{23'd0}, _btm_7_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_75 = 5'h7 < icntEnd ? {{9'd0}, _btm_7_data_T_4} : _GEN_74; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_76 = 5'h7 < prefixEnd ? 55'h0 : _GEN_75; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_8_data_T_2 = 5'h8 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_8_data_T_3 = _btm_8_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_8_data_T_4 = icntF >> _btm_8_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_8_data_T_6 = 5'h8 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_8_data_T_7 = _btm_8_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_8_data_T_8 = addrF >> _btm_8_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_8_data_T_10 = 5'h8 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_8_data_T_11 = _btm_8_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_8_data_T_12 = histF >> _btm_8_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_77 = 5'h8 < addrEnd ? _btm_8_data_T_8 : {{23'd0}, _btm_8_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_78 = 5'h8 < icntEnd ? {{9'd0}, _btm_8_data_T_4} : _GEN_77; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_79 = 5'h8 < prefixEnd ? 55'h0 : _GEN_78; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_9_data_T_2 = 5'h9 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_9_data_T_3 = _btm_9_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_9_data_T_4 = icntF >> _btm_9_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_9_data_T_6 = 5'h9 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_9_data_T_7 = _btm_9_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_9_data_T_8 = addrF >> _btm_9_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_9_data_T_10 = 5'h9 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_9_data_T_11 = _btm_9_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_9_data_T_12 = histF >> _btm_9_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_80 = 5'h9 < addrEnd ? _btm_9_data_T_8 : {{23'd0}, _btm_9_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_81 = 5'h9 < icntEnd ? {{9'd0}, _btm_9_data_T_4} : _GEN_80; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_82 = 5'h9 < prefixEnd ? 55'h0 : _GEN_81; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_10_data_T_2 = 5'ha - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_10_data_T_3 = _btm_10_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_10_data_T_4 = icntF >> _btm_10_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_10_data_T_6 = 5'ha - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_10_data_T_7 = _btm_10_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_10_data_T_8 = addrF >> _btm_10_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_10_data_T_10 = 5'ha - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_10_data_T_11 = _btm_10_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_10_data_T_12 = histF >> _btm_10_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_83 = 5'ha < addrEnd ? _btm_10_data_T_8 : {{23'd0}, _btm_10_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_84 = 5'ha < icntEnd ? {{9'd0}, _btm_10_data_T_4} : _GEN_83; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_85 = 5'ha < prefixEnd ? 55'h0 : _GEN_84; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_11_data_T_2 = 5'hb - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_11_data_T_3 = _btm_11_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_11_data_T_4 = icntF >> _btm_11_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_11_data_T_6 = 5'hb - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_11_data_T_7 = _btm_11_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_11_data_T_8 = addrF >> _btm_11_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_11_data_T_10 = 5'hb - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_11_data_T_11 = _btm_11_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_11_data_T_12 = histF >> _btm_11_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_86 = 5'hb < addrEnd ? _btm_11_data_T_8 : {{23'd0}, _btm_11_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_87 = 5'hb < icntEnd ? {{9'd0}, _btm_11_data_T_4} : _GEN_86; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_88 = 5'hb < prefixEnd ? 55'h0 : _GEN_87; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_12_data_T_2 = 5'hc - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_12_data_T_3 = _btm_12_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_12_data_T_4 = icntF >> _btm_12_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_12_data_T_6 = 5'hc - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_12_data_T_7 = _btm_12_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_12_data_T_8 = addrF >> _btm_12_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_12_data_T_10 = 5'hc - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_12_data_T_11 = _btm_12_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_12_data_T_12 = histF >> _btm_12_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_89 = 5'hc < addrEnd ? _btm_12_data_T_8 : {{23'd0}, _btm_12_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_90 = 5'hc < icntEnd ? {{9'd0}, _btm_12_data_T_4} : _GEN_89; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_91 = 5'hc < prefixEnd ? 55'h0 : _GEN_90; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_13_data_T_2 = 5'hd - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_13_data_T_3 = _btm_13_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_13_data_T_4 = icntF >> _btm_13_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_13_data_T_6 = 5'hd - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_13_data_T_7 = _btm_13_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_13_data_T_8 = addrF >> _btm_13_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_13_data_T_10 = 5'hd - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_13_data_T_11 = _btm_13_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_13_data_T_12 = histF >> _btm_13_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_92 = 5'hd < addrEnd ? _btm_13_data_T_8 : {{23'd0}, _btm_13_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_93 = 5'hd < icntEnd ? {{9'd0}, _btm_13_data_T_4} : _GEN_92; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_94 = 5'hd < prefixEnd ? 55'h0 : _GEN_93; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_14_data_T_2 = 5'he - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_14_data_T_3 = _btm_14_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_14_data_T_4 = icntF >> _btm_14_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_14_data_T_6 = 5'he - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_14_data_T_7 = _btm_14_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_14_data_T_8 = addrF >> _btm_14_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_14_data_T_10 = 5'he - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_14_data_T_11 = _btm_14_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_14_data_T_12 = histF >> _btm_14_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_95 = 5'he < addrEnd ? _btm_14_data_T_8 : {{23'd0}, _btm_14_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_96 = 5'he < icntEnd ? {{9'd0}, _btm_14_data_T_4} : _GEN_95; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_97 = 5'he < prefixEnd ? 55'h0 : _GEN_96; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_15_data_T_2 = 5'hf - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_15_data_T_3 = _btm_15_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_15_data_T_4 = icntF >> _btm_15_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_15_data_T_6 = 5'hf - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_15_data_T_7 = _btm_15_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_15_data_T_8 = addrF >> _btm_15_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_15_data_T_10 = 5'hf - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_15_data_T_11 = _btm_15_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_15_data_T_12 = histF >> _btm_15_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_98 = 5'hf < addrEnd ? _btm_15_data_T_8 : {{23'd0}, _btm_15_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_99 = 5'hf < icntEnd ? {{9'd0}, _btm_15_data_T_4} : _GEN_98; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_100 = 5'hf < prefixEnd ? 55'h0 : _GEN_99; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_16_data_T_2 = 5'h10 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_16_data_T_3 = _btm_16_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_16_data_T_4 = icntF >> _btm_16_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_16_data_T_6 = 5'h10 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_16_data_T_7 = _btm_16_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_16_data_T_8 = addrF >> _btm_16_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_16_data_T_10 = 5'h10 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_16_data_T_11 = _btm_16_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_16_data_T_12 = histF >> _btm_16_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_101 = 5'h10 < addrEnd ? _btm_16_data_T_8 : {{23'd0}, _btm_16_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_102 = 5'h10 < icntEnd ? {{9'd0}, _btm_16_data_T_4} : _GEN_101; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_103 = 5'h10 < prefixEnd ? 55'h0 : _GEN_102; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_17_data_T_2 = 5'h11 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_17_data_T_3 = _btm_17_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_17_data_T_4 = icntF >> _btm_17_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_17_data_T_6 = 5'h11 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_17_data_T_7 = _btm_17_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_17_data_T_8 = addrF >> _btm_17_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_17_data_T_10 = 5'h11 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_17_data_T_11 = _btm_17_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_17_data_T_12 = histF >> _btm_17_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_104 = 5'h11 < addrEnd ? _btm_17_data_T_8 : {{23'd0}, _btm_17_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_105 = 5'h11 < icntEnd ? {{9'd0}, _btm_17_data_T_4} : _GEN_104; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_106 = 5'h11 < prefixEnd ? 55'h0 : _GEN_105; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_18_data_T_2 = 5'h12 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_18_data_T_3 = _btm_18_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_18_data_T_4 = icntF >> _btm_18_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_18_data_T_6 = 5'h12 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_18_data_T_7 = _btm_18_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_18_data_T_8 = addrF >> _btm_18_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_18_data_T_10 = 5'h12 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_18_data_T_11 = _btm_18_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_18_data_T_12 = histF >> _btm_18_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_107 = 5'h12 < addrEnd ? _btm_18_data_T_8 : {{23'd0}, _btm_18_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_108 = 5'h12 < icntEnd ? {{9'd0}, _btm_18_data_T_4} : _GEN_107; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_109 = 5'h12 < prefixEnd ? 55'h0 : _GEN_108; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_19_data_T_2 = 5'h13 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_19_data_T_3 = _btm_19_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_19_data_T_4 = icntF >> _btm_19_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_19_data_T_6 = 5'h13 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_19_data_T_7 = _btm_19_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_19_data_T_8 = addrF >> _btm_19_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_19_data_T_10 = 5'h13 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_19_data_T_11 = _btm_19_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_19_data_T_12 = histF >> _btm_19_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_110 = 5'h13 < addrEnd ? _btm_19_data_T_8 : {{23'd0}, _btm_19_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_111 = 5'h13 < icntEnd ? {{9'd0}, _btm_19_data_T_4} : _GEN_110; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_112 = 5'h13 < prefixEnd ? 55'h0 : _GEN_111; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_20_data_T_2 = 5'h14 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_20_data_T_3 = _btm_20_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_20_data_T_4 = icntF >> _btm_20_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_20_data_T_6 = 5'h14 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_20_data_T_7 = _btm_20_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_20_data_T_8 = addrF >> _btm_20_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_20_data_T_10 = 5'h14 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_20_data_T_11 = _btm_20_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_20_data_T_12 = histF >> _btm_20_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_113 = 5'h14 < addrEnd ? _btm_20_data_T_8 : {{23'd0}, _btm_20_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_114 = 5'h14 < icntEnd ? {{9'd0}, _btm_20_data_T_4} : _GEN_113; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_115 = 5'h14 < prefixEnd ? 55'h0 : _GEN_114; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_21_data_T_2 = 5'h15 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_21_data_T_3 = _btm_21_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_21_data_T_4 = icntF >> _btm_21_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_21_data_T_6 = 5'h15 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_21_data_T_7 = _btm_21_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_21_data_T_8 = addrF >> _btm_21_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_21_data_T_10 = 5'h15 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_21_data_T_11 = _btm_21_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_21_data_T_12 = histF >> _btm_21_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_116 = 5'h15 < addrEnd ? _btm_21_data_T_8 : {{23'd0}, _btm_21_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_117 = 5'h15 < icntEnd ? {{9'd0}, _btm_21_data_T_4} : _GEN_116; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_118 = 5'h15 < prefixEnd ? 55'h0 : _GEN_117; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_22_data_T_2 = 5'h16 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_22_data_T_3 = _btm_22_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_22_data_T_4 = icntF >> _btm_22_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_22_data_T_6 = 5'h16 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_22_data_T_7 = _btm_22_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_22_data_T_8 = addrF >> _btm_22_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_22_data_T_10 = 5'h16 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_22_data_T_11 = _btm_22_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_22_data_T_12 = histF >> _btm_22_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_119 = 5'h16 < addrEnd ? _btm_22_data_T_8 : {{23'd0}, _btm_22_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_120 = 5'h16 < icntEnd ? {{9'd0}, _btm_22_data_T_4} : _GEN_119; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_121 = 5'h16 < prefixEnd ? 55'h0 : _GEN_120; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_23_data_T_2 = 5'h17 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_23_data_T_3 = _btm_23_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_23_data_T_4 = icntF >> _btm_23_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_23_data_T_6 = 5'h17 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_23_data_T_7 = _btm_23_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_23_data_T_8 = addrF >> _btm_23_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_23_data_T_10 = 5'h17 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_23_data_T_11 = _btm_23_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_23_data_T_12 = histF >> _btm_23_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_122 = 5'h17 < addrEnd ? _btm_23_data_T_8 : {{23'd0}, _btm_23_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_123 = 5'h17 < icntEnd ? {{9'd0}, _btm_23_data_T_4} : _GEN_122; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_124 = 5'h17 < prefixEnd ? 55'h0 : _GEN_123; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _btm_24_data_T_2 = 5'h18 - prefixEnd; // @[TraceBTM.scala 305:45]
  wire [7:0] _btm_24_data_T_3 = _btm_24_data_T_2 * 3'h6; // @[TraceBTM.scala 305:58]
  wire [45:0] _btm_24_data_T_4 = icntF >> _btm_24_data_T_3; // @[TraceBTM.scala 305:32]
  wire [4:0] _btm_24_data_T_6 = 5'h18 - icntEnd; // @[TraceBTM.scala 307:45]
  wire [7:0] _btm_24_data_T_7 = _btm_24_data_T_6 * 3'h6; // @[TraceBTM.scala 307:56]
  wire [54:0] _btm_24_data_T_8 = addrF >> _btm_24_data_T_7; // @[TraceBTM.scala 307:32]
  wire [4:0] _btm_24_data_T_10 = 5'h18 - addrEnd; // @[TraceBTM.scala 309:45]
  wire [7:0] _btm_24_data_T_11 = _btm_24_data_T_10 * 3'h6; // @[TraceBTM.scala 309:56]
  wire [31:0] _btm_24_data_T_12 = histF >> _btm_24_data_T_11; // @[TraceBTM.scala 309:32]
  wire [54:0] _GEN_125 = 5'h18 < addrEnd ? _btm_24_data_T_8 : {{23'd0}, _btm_24_data_T_12}; // @[TraceBTM.scala 306:36 TraceBTM.scala 307:23 TraceBTM.scala 309:23]
  wire [54:0] _GEN_126 = 5'h18 < icntEnd ? {{9'd0}, _btm_24_data_T_4} : _GEN_125; // @[TraceBTM.scala 304:36 TraceBTM.scala 305:23]
  wire [54:0] _GEN_127 = 5'h18 < prefixEnd ? 55'h0 : _GEN_126; // @[TraceBTM.scala 302:32 TraceBTM.scala 303:23]
  wire [4:0] _T_94 = icntEnd - 5'h1; // @[TraceBTM.scala 313:32]
  wire [1:0] _GEN_128 = 5'h0 == _T_94 ? 2'h1 : _GEN_28; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_129 = 5'h1 == _T_94 ? 2'h1 : _GEN_29; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_130 = 5'h2 == _T_94 ? 2'h1 : _GEN_30; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_131 = 5'h3 == _T_94 ? 2'h1 : _GEN_31; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_132 = 5'h4 == _T_94 ? 2'h1 : _GEN_32; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_133 = 5'h5 == _T_94 ? 2'h1 : _GEN_33; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_134 = 5'h6 == _T_94 ? 2'h1 : _GEN_34; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_135 = 5'h7 == _T_94 ? 2'h1 : _GEN_35; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_136 = 5'h8 == _T_94 ? 2'h1 : _GEN_36; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_137 = 5'h9 == _T_94 ? 2'h1 : _GEN_37; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_138 = 5'ha == _T_94 ? 2'h1 : _GEN_38; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_139 = 5'hb == _T_94 ? 2'h1 : _GEN_39; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_140 = 5'hc == _T_94 ? 2'h1 : _GEN_40; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_141 = 5'hd == _T_94 ? 2'h1 : _GEN_41; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_142 = 5'he == _T_94 ? 2'h1 : _GEN_42; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_143 = 5'hf == _T_94 ? 2'h1 : _GEN_43; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_144 = 5'h10 == _T_94 ? 2'h1 : _GEN_44; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_145 = 5'h11 == _T_94 ? 2'h1 : _GEN_45; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_146 = 5'h12 == _T_94 ? 2'h1 : _GEN_46; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_147 = 5'h13 == _T_94 ? 2'h1 : _GEN_47; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_148 = 5'h14 == _T_94 ? 2'h1 : _GEN_48; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_149 = 5'h15 == _T_94 ? 2'h1 : _GEN_49; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_150 = 5'h16 == _T_94 ? 2'h1 : _GEN_50; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_151 = 5'h17 == _T_94 ? 2'h1 : _GEN_51; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_152 = 5'h18 == _T_94 ? 2'h1 : _GEN_52; // @[TraceBTM.scala 313:44 TraceBTM.scala 313:44]
  wire [1:0] _GEN_153 = hasIcnt ? _GEN_128 : _GEN_28; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_154 = hasIcnt ? _GEN_129 : _GEN_29; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_155 = hasIcnt ? _GEN_130 : _GEN_30; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_156 = hasIcnt ? _GEN_131 : _GEN_31; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_157 = hasIcnt ? _GEN_132 : _GEN_32; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_158 = hasIcnt ? _GEN_133 : _GEN_33; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_159 = hasIcnt ? _GEN_134 : _GEN_34; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_160 = hasIcnt ? _GEN_135 : _GEN_35; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_161 = hasIcnt ? _GEN_136 : _GEN_36; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_162 = hasIcnt ? _GEN_137 : _GEN_37; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_163 = hasIcnt ? _GEN_138 : _GEN_38; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_164 = hasIcnt ? _GEN_139 : _GEN_39; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_165 = hasIcnt ? _GEN_140 : _GEN_40; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_166 = hasIcnt ? _GEN_141 : _GEN_41; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_167 = hasIcnt ? _GEN_142 : _GEN_42; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_168 = hasIcnt ? _GEN_143 : _GEN_43; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_169 = hasIcnt ? _GEN_144 : _GEN_44; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_170 = hasIcnt ? _GEN_145 : _GEN_45; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_171 = hasIcnt ? _GEN_146 : _GEN_46; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_172 = hasIcnt ? _GEN_147 : _GEN_47; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_173 = hasIcnt ? _GEN_148 : _GEN_48; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_174 = hasIcnt ? _GEN_149 : _GEN_49; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_175 = hasIcnt ? _GEN_150 : _GEN_50; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_176 = hasIcnt ? _GEN_151 : _GEN_51; // @[TraceBTM.scala 313:18]
  wire [1:0] _GEN_177 = hasIcnt ? _GEN_152 : _GEN_52; // @[TraceBTM.scala 313:18]
  wire [4:0] _T_96 = addrEnd - 5'h1; // @[TraceBTM.scala 314:32]
  wire [1:0] _GEN_178 = 5'h0 == _T_96 ? 2'h1 : _GEN_153; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_179 = 5'h1 == _T_96 ? 2'h1 : _GEN_154; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_180 = 5'h2 == _T_96 ? 2'h1 : _GEN_155; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_181 = 5'h3 == _T_96 ? 2'h1 : _GEN_156; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_182 = 5'h4 == _T_96 ? 2'h1 : _GEN_157; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_183 = 5'h5 == _T_96 ? 2'h1 : _GEN_158; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_184 = 5'h6 == _T_96 ? 2'h1 : _GEN_159; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_185 = 5'h7 == _T_96 ? 2'h1 : _GEN_160; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_186 = 5'h8 == _T_96 ? 2'h1 : _GEN_161; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_187 = 5'h9 == _T_96 ? 2'h1 : _GEN_162; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_188 = 5'ha == _T_96 ? 2'h1 : _GEN_163; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_189 = 5'hb == _T_96 ? 2'h1 : _GEN_164; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_190 = 5'hc == _T_96 ? 2'h1 : _GEN_165; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_191 = 5'hd == _T_96 ? 2'h1 : _GEN_166; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_192 = 5'he == _T_96 ? 2'h1 : _GEN_167; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_193 = 5'hf == _T_96 ? 2'h1 : _GEN_168; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_194 = 5'h10 == _T_96 ? 2'h1 : _GEN_169; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_195 = 5'h11 == _T_96 ? 2'h1 : _GEN_170; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_196 = 5'h12 == _T_96 ? 2'h1 : _GEN_171; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_197 = 5'h13 == _T_96 ? 2'h1 : _GEN_172; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_198 = 5'h14 == _T_96 ? 2'h1 : _GEN_173; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_199 = 5'h15 == _T_96 ? 2'h1 : _GEN_174; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_200 = 5'h16 == _T_96 ? 2'h1 : _GEN_175; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_201 = 5'h17 == _T_96 ? 2'h1 : _GEN_176; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_202 = 5'h18 == _T_96 ? 2'h1 : _GEN_177; // @[TraceBTM.scala 314:44 TraceBTM.scala 314:44]
  wire [1:0] _GEN_203 = hasAddr ? _GEN_178 : _GEN_153; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_204 = hasAddr ? _GEN_179 : _GEN_154; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_205 = hasAddr ? _GEN_180 : _GEN_155; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_206 = hasAddr ? _GEN_181 : _GEN_156; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_207 = hasAddr ? _GEN_182 : _GEN_157; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_208 = hasAddr ? _GEN_183 : _GEN_158; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_209 = hasAddr ? _GEN_184 : _GEN_159; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_210 = hasAddr ? _GEN_185 : _GEN_160; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_211 = hasAddr ? _GEN_186 : _GEN_161; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_212 = hasAddr ? _GEN_187 : _GEN_162; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_213 = hasAddr ? _GEN_188 : _GEN_163; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_214 = hasAddr ? _GEN_189 : _GEN_164; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_215 = hasAddr ? _GEN_190 : _GEN_165; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_216 = hasAddr ? _GEN_191 : _GEN_166; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_217 = hasAddr ? _GEN_192 : _GEN_167; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_218 = hasAddr ? _GEN_193 : _GEN_168; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_219 = hasAddr ? _GEN_194 : _GEN_169; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_220 = hasAddr ? _GEN_195 : _GEN_170; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_221 = hasAddr ? _GEN_196 : _GEN_171; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_222 = hasAddr ? _GEN_197 : _GEN_172; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_223 = hasAddr ? _GEN_198 : _GEN_173; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_224 = hasAddr ? _GEN_199 : _GEN_174; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_225 = hasAddr ? _GEN_200 : _GEN_175; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_226 = hasAddr ? _GEN_201 : _GEN_176; // @[TraceBTM.scala 314:18]
  wire [1:0] _GEN_227 = hasAddr ? _GEN_202 : _GEN_177; // @[TraceBTM.scala 314:18]
  wire  _T_103 = io_tsControl_tsBranch[0] & (_icntF1_T_3 | _icntF1_T_1); // @[TraceBTM.scala 319:36]
  wire  _T_104 = hasSync | _T_103; // @[TraceBTM.scala 318:18]
  wire  _T_106 = &io_tsControl_tsBranch; // @[TraceBTM.scala 320:38]
  wire  _T_107 = _T_104 | _T_106; // @[TraceBTM.scala 319:104]
  wire  _T_108 = io_tsControl_tsEnable & _T_107; // @[TraceBTM.scala 317:31]
  wire [4:0] _T_110 = histEnd - 5'h1; // @[TraceBTM.scala 321:17]
  wire [1:0] _GEN_228 = 5'h0 == _T_110 ? 2'h1 : _GEN_203; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_229 = 5'h1 == _T_110 ? 2'h1 : _GEN_204; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_230 = 5'h2 == _T_110 ? 2'h1 : _GEN_205; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_231 = 5'h3 == _T_110 ? 2'h1 : _GEN_206; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_232 = 5'h4 == _T_110 ? 2'h1 : _GEN_207; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_233 = 5'h5 == _T_110 ? 2'h1 : _GEN_208; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_234 = 5'h6 == _T_110 ? 2'h1 : _GEN_209; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_235 = 5'h7 == _T_110 ? 2'h1 : _GEN_210; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_236 = 5'h8 == _T_110 ? 2'h1 : _GEN_211; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_237 = 5'h9 == _T_110 ? 2'h1 : _GEN_212; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_238 = 5'ha == _T_110 ? 2'h1 : _GEN_213; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_239 = 5'hb == _T_110 ? 2'h1 : _GEN_214; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_240 = 5'hc == _T_110 ? 2'h1 : _GEN_215; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_241 = 5'hd == _T_110 ? 2'h1 : _GEN_216; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_242 = 5'he == _T_110 ? 2'h1 : _GEN_217; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_243 = 5'hf == _T_110 ? 2'h1 : _GEN_218; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_244 = 5'h10 == _T_110 ? 2'h1 : _GEN_219; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_245 = 5'h11 == _T_110 ? 2'h1 : _GEN_220; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_246 = 5'h12 == _T_110 ? 2'h1 : _GEN_221; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_247 = 5'h13 == _T_110 ? 2'h1 : _GEN_222; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_248 = 5'h14 == _T_110 ? 2'h1 : _GEN_223; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_249 = 5'h15 == _T_110 ? 2'h1 : _GEN_224; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_250 = 5'h16 == _T_110 ? 2'h1 : _GEN_225; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_251 = 5'h17 == _T_110 ? 2'h1 : _GEN_226; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _GEN_252 = 5'h18 == _T_110 ? 2'h1 : _GEN_227; // @[TraceBTM.scala 321:29 TraceBTM.scala 321:29]
  wire [1:0] _io_btm_bits_timestamp_T = {1'h1,hasSync}; // @[Cat.scala 30:58]
  wire [1:0] _GEN_253 = 5'h0 == _T_110 ? 2'h3 : _GEN_203; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_254 = 5'h1 == _T_110 ? 2'h3 : _GEN_204; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_255 = 5'h2 == _T_110 ? 2'h3 : _GEN_205; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_256 = 5'h3 == _T_110 ? 2'h3 : _GEN_206; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_257 = 5'h4 == _T_110 ? 2'h3 : _GEN_207; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_258 = 5'h5 == _T_110 ? 2'h3 : _GEN_208; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_259 = 5'h6 == _T_110 ? 2'h3 : _GEN_209; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_260 = 5'h7 == _T_110 ? 2'h3 : _GEN_210; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_261 = 5'h8 == _T_110 ? 2'h3 : _GEN_211; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_262 = 5'h9 == _T_110 ? 2'h3 : _GEN_212; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_263 = 5'ha == _T_110 ? 2'h3 : _GEN_213; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_264 = 5'hb == _T_110 ? 2'h3 : _GEN_214; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_265 = 5'hc == _T_110 ? 2'h3 : _GEN_215; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_266 = 5'hd == _T_110 ? 2'h3 : _GEN_216; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_267 = 5'he == _T_110 ? 2'h3 : _GEN_217; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_268 = 5'hf == _T_110 ? 2'h3 : _GEN_218; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_269 = 5'h10 == _T_110 ? 2'h3 : _GEN_219; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_270 = 5'h11 == _T_110 ? 2'h3 : _GEN_220; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_271 = 5'h12 == _T_110 ? 2'h3 : _GEN_221; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_272 = 5'h13 == _T_110 ? 2'h3 : _GEN_222; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_273 = 5'h14 == _T_110 ? 2'h3 : _GEN_223; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_274 = 5'h15 == _T_110 ? 2'h3 : _GEN_224; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_275 = 5'h16 == _T_110 ? 2'h3 : _GEN_225; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_276 = 5'h17 == _T_110 ? 2'h3 : _GEN_226; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  wire [1:0] _GEN_277 = 5'h18 == _T_110 ? 2'h3 : _GEN_227; // @[TraceBTM.scala 324:29 TraceBTM.scala 324:29]
  reg  btmSeen; // @[TraceBTM.scala 335:20]
  wire  _T_113 = ~io_teActive; // @[TraceBTM.scala 337:9]
  wire  _T_115 = io_btmEvent_ready & io_btmEvent_valid; // @[Decoupled.scala 40:37]
  wire  _GEN_304 = _T_115 | btmSeen; // @[TraceBTM.scala 341:35 TraceBTM.scala 342:13 TraceBTM.scala 335:20]
  wire [16:0] _btmCount_T_1 = btmCount + 17'h1; // @[TraceBTM.scala 351:28]
  assign io_btmEvent_ready = io_btm_ready; // @[TraceBTM.scala 150:21]
  assign io_btm_valid = io_btmEvent_valid; // @[TraceBTM.scala 149:16]
  assign io_btm_bits_length = hasHist ? _histEnd_T_2 : addrEnd; // @[TraceBTM.scala 294:20 TraceBTM.scala 296:15 TraceBTM.scala 292:11]
  assign io_btm_bits_timestamp = _T_108 ? _io_btm_bits_timestamp_T : 2'h0; // @[TraceBTM.scala 320:46 TraceBTM.scala 322:27 TraceBTM.scala 325:27]
  assign io_btm_bits_slices_0_data = _GEN_55[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_0_mseo = _T_108 ? _GEN_228 : _GEN_253; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_1_data = _GEN_58[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_1_mseo = _T_108 ? _GEN_229 : _GEN_254; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_2_data = _GEN_61[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_2_mseo = _T_108 ? _GEN_230 : _GEN_255; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_3_data = _GEN_64[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_3_mseo = _T_108 ? _GEN_231 : _GEN_256; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_4_data = _GEN_67[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_4_mseo = _T_108 ? _GEN_232 : _GEN_257; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_5_data = _GEN_70[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_5_mseo = _T_108 ? _GEN_233 : _GEN_258; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_6_data = _GEN_73[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_6_mseo = _T_108 ? _GEN_234 : _GEN_259; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_7_data = _GEN_76[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_7_mseo = _T_108 ? _GEN_235 : _GEN_260; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_8_data = _GEN_79[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_8_mseo = _T_108 ? _GEN_236 : _GEN_261; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_9_data = _GEN_82[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_9_mseo = _T_108 ? _GEN_237 : _GEN_262; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_10_data = _GEN_85[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_10_mseo = _T_108 ? _GEN_238 : _GEN_263; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_11_data = _GEN_88[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_11_mseo = _T_108 ? _GEN_239 : _GEN_264; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_12_data = _GEN_91[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_12_mseo = _T_108 ? _GEN_240 : _GEN_265; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_13_data = _GEN_94[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_13_mseo = _T_108 ? _GEN_241 : _GEN_266; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_14_data = _GEN_97[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_14_mseo = _T_108 ? _GEN_242 : _GEN_267; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_15_data = _GEN_100[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_15_mseo = _T_108 ? _GEN_243 : _GEN_268; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_16_data = _GEN_103[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_16_mseo = _T_108 ? _GEN_244 : _GEN_269; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_17_data = _GEN_106[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_17_mseo = _T_108 ? _GEN_245 : _GEN_270; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_18_data = _GEN_109[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_18_mseo = _T_108 ? _GEN_246 : _GEN_271; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_19_data = _GEN_112[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_19_mseo = _T_108 ? _GEN_247 : _GEN_272; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_20_data = _GEN_115[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_20_mseo = _T_108 ? _GEN_248 : _GEN_273; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_21_data = _GEN_118[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_21_mseo = _T_108 ? _GEN_249 : _GEN_274; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_22_data = _GEN_121[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_22_mseo = _T_108 ? _GEN_250 : _GEN_275; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_23_data = _GEN_124[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_23_mseo = _T_108 ? _GEN_251 : _GEN_276; // @[TraceBTM.scala 320:46]
  assign io_btm_bits_slices_24_data = _GEN_127[5:0]; // @[TraceBTM.scala 328:22]
  assign io_btm_bits_slices_24_mseo = _T_108 ? _GEN_252 : _GEN_277; // @[TraceBTM.scala 320:46]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      lastaddr <= 39'h0;
    end else if (_T_113) begin
      lastaddr <= 39'h0;
    end else if (_T_115 & hasAddr) begin
      lastaddr <= io_btmEvent_bits_iaddr;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      btmCount <= 17'h0;
    end else if (_T_113) begin
      btmCount <= 17'h0;
    end else if (_T_115) begin
      if (hasSync) begin
        btmCount <= 17'h0;
      end else begin
        btmCount <= _btmCount_T_1;
      end
    end else if (~btmSeen & io_teControl_teInstruction == 3'h1) begin
      btmCount <= 17'h10000;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      btmSeen <= 1'h0;
    end else if (~io_teActive) begin
      btmSeen <= 1'h0;
    end else if (~io_teControl_teEnable) begin
      btmSeen <= 1'h0;
    end else begin
      btmSeen <= _GEN_304;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {2{`RANDOM}};
  lastaddr = _RAND_0[38:0];
  _RAND_1 = {1{`RANDOM}};
  btmCount = _RAND_1[16:0];
  _RAND_2 = {1{`RANDOM}};
  btmSeen = _RAND_2[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    lastaddr = 39'h0;
  end
  if (reset) begin
    btmCount = 17'h0;
  end
  if (rf_reset) begin
    btmSeen = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
