//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__Scheduler(
  input          rf_reset,
  input          clock,
  input          reset,
  output         io_in_a_ready,
  input          io_in_a_valid,
  input  [2:0]   io_in_a_bits_opcode,
  input  [2:0]   io_in_a_bits_param,
  input  [2:0]   io_in_a_bits_size,
  input  [6:0]   io_in_a_bits_source,
  input  [35:0]  io_in_a_bits_address,
  input  [15:0]  io_in_a_bits_mask,
  input  [127:0] io_in_a_bits_data,
  input          io_in_a_bits_corrupt,
  input          io_in_b_ready,
  output         io_in_b_valid,
  output [1:0]   io_in_b_bits_param,
  output [6:0]   io_in_b_bits_source,
  output [35:0]  io_in_b_bits_address,
  output         io_in_c_ready,
  input          io_in_c_valid,
  input  [2:0]   io_in_c_bits_opcode,
  input  [2:0]   io_in_c_bits_param,
  input  [2:0]   io_in_c_bits_size,
  input  [6:0]   io_in_c_bits_source,
  input  [35:0]  io_in_c_bits_address,
  input  [127:0] io_in_c_bits_data,
  input          io_in_c_bits_corrupt,
  input          io_in_d_ready,
  output         io_in_d_valid,
  output [2:0]   io_in_d_bits_opcode,
  output [1:0]   io_in_d_bits_param,
  output [2:0]   io_in_d_bits_size,
  output [6:0]   io_in_d_bits_source,
  output [2:0]   io_in_d_bits_sink,
  output         io_in_d_bits_denied,
  output [127:0] io_in_d_bits_data,
  output         io_in_d_bits_corrupt,
  input          io_in_e_valid,
  input  [2:0]   io_in_e_bits_sink,
  input          io_out_a_ready,
  output         io_out_a_valid,
  output [2:0]   io_out_a_bits_opcode,
  output [2:0]   io_out_a_bits_param,
  output [2:0]   io_out_a_bits_source,
  output [35:0]  io_out_a_bits_address,
  input          io_out_c_ready,
  output         io_out_c_valid,
  output [2:0]   io_out_c_bits_opcode,
  output [2:0]   io_out_c_bits_param,
  output [2:0]   io_out_c_bits_source,
  output [35:0]  io_out_c_bits_address,
  output [127:0] io_out_c_bits_data,
  output         io_out_d_ready,
  input          io_out_d_valid,
  input  [2:0]   io_out_d_bits_opcode,
  input  [1:0]   io_out_d_bits_param,
  input  [2:0]   io_out_d_bits_size,
  input  [2:0]   io_out_d_bits_source,
  input  [2:0]   io_out_d_bits_sink,
  input          io_out_d_bits_denied,
  input  [127:0] io_out_d_bits_data,
  input          io_out_d_bits_corrupt,
  output         io_out_e_valid,
  output [2:0]   io_out_e_bits_sink,
  input  [7:0]   io_ways_0,
  input  [7:0]   io_ways_1,
  input  [7:0]   io_ways_2,
  input  [7:0]   io_ways_3,
  input  [7:0]   io_ways_4,
  input  [7:0]   io_ways_5,
  input  [7:0]   io_ways_6,
  input  [7:0]   io_ways_7,
  input  [7:0]   io_ways_8,
  input  [7:0]   io_ways_9,
  input  [7:0]   io_ways_10,
  input  [7:0]   io_ways_11,
  input  [7:0]   io_ways_12,
  input  [7:0]   io_ways_13,
  input  [10:0]  io_divs_0,
  input  [10:0]  io_divs_1,
  input  [10:0]  io_divs_2,
  input  [10:0]  io_divs_3,
  input  [10:0]  io_divs_4,
  input  [10:0]  io_divs_5,
  input  [10:0]  io_divs_6,
  input  [10:0]  io_divs_7,
  input  [10:0]  io_divs_8,
  input  [10:0]  io_divs_9,
  input  [10:0]  io_divs_10,
  input  [10:0]  io_divs_11,
  input  [10:0]  io_divs_12,
  input  [10:0]  io_divs_13,
  output         io_req_ready,
  input          io_req_valid,
  input  [35:0]  io_req_bits_address,
  input          io_resp_ready,
  output         io_resp_valid,
  input          io_side_adr_valid,
  input  [2:0]   io_side_adr_bits_way,
  input  [9:0]   io_side_adr_bits_set,
  input  [1:0]   io_side_adr_bits_beat,
  input  [1:0]   io_side_adr_bits_mask,
  input          io_side_adr_bits_write,
  input  [127:0] io_side_wdat_data,
  input          io_side_wdat_poison,
  output [127:0] io_side_rdat_data,
  output         io_error_ready,
  input          io_error_valid,
  input          io_error_bits_dir,
  input  [7:0]   io_error_bits_bit
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [127:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
`endif // RANDOMIZE_REG_INIT
  wire  sourceA_rf_reset; // @[Scheduler.scala 74:23]
  wire  sourceA_clock; // @[Scheduler.scala 74:23]
  wire  sourceA_reset; // @[Scheduler.scala 74:23]
  wire  sourceA_io_req_ready; // @[Scheduler.scala 74:23]
  wire  sourceA_io_req_valid; // @[Scheduler.scala 74:23]
  wire [17:0] sourceA_io_req_bits_tag; // @[Scheduler.scala 74:23]
  wire [9:0] sourceA_io_req_bits_set; // @[Scheduler.scala 74:23]
  wire [2:0] sourceA_io_req_bits_param; // @[Scheduler.scala 74:23]
  wire [2:0] sourceA_io_req_bits_source; // @[Scheduler.scala 74:23]
  wire  sourceA_io_req_bits_block; // @[Scheduler.scala 74:23]
  wire  sourceA_io_a_ready; // @[Scheduler.scala 74:23]
  wire  sourceA_io_a_valid; // @[Scheduler.scala 74:23]
  wire [2:0] sourceA_io_a_bits_opcode; // @[Scheduler.scala 74:23]
  wire [2:0] sourceA_io_a_bits_param; // @[Scheduler.scala 74:23]
  wire [2:0] sourceA_io_a_bits_source; // @[Scheduler.scala 74:23]
  wire [35:0] sourceA_io_a_bits_address; // @[Scheduler.scala 74:23]
  wire  sourceB_rf_reset; // @[Scheduler.scala 75:23]
  wire  sourceB_clock; // @[Scheduler.scala 75:23]
  wire  sourceB_reset; // @[Scheduler.scala 75:23]
  wire  sourceB_io_req_ready; // @[Scheduler.scala 75:23]
  wire  sourceB_io_req_valid; // @[Scheduler.scala 75:23]
  wire [2:0] sourceB_io_req_bits_param; // @[Scheduler.scala 75:23]
  wire [17:0] sourceB_io_req_bits_tag; // @[Scheduler.scala 75:23]
  wire [9:0] sourceB_io_req_bits_set; // @[Scheduler.scala 75:23]
  wire [3:0] sourceB_io_req_bits_clients; // @[Scheduler.scala 75:23]
  wire  sourceB_io_b_ready; // @[Scheduler.scala 75:23]
  wire  sourceB_io_b_valid; // @[Scheduler.scala 75:23]
  wire [1:0] sourceB_io_b_bits_param; // @[Scheduler.scala 75:23]
  wire [6:0] sourceB_io_b_bits_source; // @[Scheduler.scala 75:23]
  wire [35:0] sourceB_io_b_bits_address; // @[Scheduler.scala 75:23]
  wire  sourceC_rf_reset; // @[Scheduler.scala 76:23]
  wire  sourceC_clock; // @[Scheduler.scala 76:23]
  wire  sourceC_reset; // @[Scheduler.scala 76:23]
  wire  sourceC_io_req_ready; // @[Scheduler.scala 76:23]
  wire  sourceC_io_req_valid; // @[Scheduler.scala 76:23]
  wire [2:0] sourceC_io_req_bits_opcode; // @[Scheduler.scala 76:23]
  wire [2:0] sourceC_io_req_bits_param; // @[Scheduler.scala 76:23]
  wire [2:0] sourceC_io_req_bits_source; // @[Scheduler.scala 76:23]
  wire [17:0] sourceC_io_req_bits_tag; // @[Scheduler.scala 76:23]
  wire [9:0] sourceC_io_req_bits_set; // @[Scheduler.scala 76:23]
  wire [2:0] sourceC_io_req_bits_way; // @[Scheduler.scala 76:23]
  wire  sourceC_io_req_bits_dirty; // @[Scheduler.scala 76:23]
  wire  sourceC_io_c_ready; // @[Scheduler.scala 76:23]
  wire  sourceC_io_c_valid; // @[Scheduler.scala 76:23]
  wire [2:0] sourceC_io_c_bits_opcode; // @[Scheduler.scala 76:23]
  wire [2:0] sourceC_io_c_bits_param; // @[Scheduler.scala 76:23]
  wire [2:0] sourceC_io_c_bits_source; // @[Scheduler.scala 76:23]
  wire [35:0] sourceC_io_c_bits_address; // @[Scheduler.scala 76:23]
  wire [127:0] sourceC_io_c_bits_data; // @[Scheduler.scala 76:23]
  wire  sourceC_io_bs_adr_ready; // @[Scheduler.scala 76:23]
  wire  sourceC_io_bs_adr_valid; // @[Scheduler.scala 76:23]
  wire [2:0] sourceC_io_bs_adr_bits_way; // @[Scheduler.scala 76:23]
  wire [9:0] sourceC_io_bs_adr_bits_set; // @[Scheduler.scala 76:23]
  wire [1:0] sourceC_io_bs_adr_bits_beat; // @[Scheduler.scala 76:23]
  wire [127:0] sourceC_io_bs_dat_data; // @[Scheduler.scala 76:23]
  wire [9:0] sourceC_io_evict_req_set; // @[Scheduler.scala 76:23]
  wire [2:0] sourceC_io_evict_req_way; // @[Scheduler.scala 76:23]
  wire  sourceC_io_evict_safe; // @[Scheduler.scala 76:23]
  wire  sourceC_io_corrected_valid; // @[Scheduler.scala 76:23]
  wire  sourceC_io_uncorrected_valid; // @[Scheduler.scala 76:23]
  wire  sourceD_rf_reset; // @[Scheduler.scala 77:23]
  wire  sourceD_clock; // @[Scheduler.scala 77:23]
  wire  sourceD_reset; // @[Scheduler.scala 77:23]
  wire  sourceD_io_req_ready; // @[Scheduler.scala 77:23]
  wire  sourceD_io_req_valid; // @[Scheduler.scala 77:23]
  wire  sourceD_io_req_bits_prio_0; // @[Scheduler.scala 77:23]
  wire  sourceD_io_req_bits_prio_2; // @[Scheduler.scala 77:23]
  wire [2:0] sourceD_io_req_bits_opcode; // @[Scheduler.scala 77:23]
  wire [2:0] sourceD_io_req_bits_param; // @[Scheduler.scala 77:23]
  wire [2:0] sourceD_io_req_bits_size; // @[Scheduler.scala 77:23]
  wire [6:0] sourceD_io_req_bits_source; // @[Scheduler.scala 77:23]
  wire [5:0] sourceD_io_req_bits_offset; // @[Scheduler.scala 77:23]
  wire [4:0] sourceD_io_req_bits_put; // @[Scheduler.scala 77:23]
  wire [9:0] sourceD_io_req_bits_set; // @[Scheduler.scala 77:23]
  wire [2:0] sourceD_io_req_bits_sink; // @[Scheduler.scala 77:23]
  wire [2:0] sourceD_io_req_bits_way; // @[Scheduler.scala 77:23]
  wire  sourceD_io_req_bits_bad; // @[Scheduler.scala 77:23]
  wire  sourceD_io_req_bits_user_hit_cache; // @[Scheduler.scala 77:23]
  wire  sourceD_io_d_ready; // @[Scheduler.scala 77:23]
  wire  sourceD_io_d_valid; // @[Scheduler.scala 77:23]
  wire [2:0] sourceD_io_d_bits_opcode; // @[Scheduler.scala 77:23]
  wire [1:0] sourceD_io_d_bits_param; // @[Scheduler.scala 77:23]
  wire [2:0] sourceD_io_d_bits_size; // @[Scheduler.scala 77:23]
  wire [6:0] sourceD_io_d_bits_source; // @[Scheduler.scala 77:23]
  wire [2:0] sourceD_io_d_bits_sink; // @[Scheduler.scala 77:23]
  wire  sourceD_io_d_bits_denied; // @[Scheduler.scala 77:23]
  wire [127:0] sourceD_io_d_bits_data; // @[Scheduler.scala 77:23]
  wire  sourceD_io_d_bits_corrupt; // @[Scheduler.scala 77:23]
  wire  sourceD_io_pb_pop_ready; // @[Scheduler.scala 77:23]
  wire  sourceD_io_pb_pop_valid; // @[Scheduler.scala 77:23]
  wire [4:0] sourceD_io_pb_pop_bits_index; // @[Scheduler.scala 77:23]
  wire  sourceD_io_pb_pop_bits_last; // @[Scheduler.scala 77:23]
  wire [127:0] sourceD_io_pb_beat_data; // @[Scheduler.scala 77:23]
  wire [15:0] sourceD_io_pb_beat_mask; // @[Scheduler.scala 77:23]
  wire  sourceD_io_pb_beat_corrupt; // @[Scheduler.scala 77:23]
  wire  sourceD_io_rel_pop_ready; // @[Scheduler.scala 77:23]
  wire  sourceD_io_rel_pop_valid; // @[Scheduler.scala 77:23]
  wire [4:0] sourceD_io_rel_pop_bits_index; // @[Scheduler.scala 77:23]
  wire  sourceD_io_rel_pop_bits_last; // @[Scheduler.scala 77:23]
  wire [127:0] sourceD_io_rel_beat_data; // @[Scheduler.scala 77:23]
  wire  sourceD_io_rel_beat_corrupt; // @[Scheduler.scala 77:23]
  wire  sourceD_io_bs_radr_ready; // @[Scheduler.scala 77:23]
  wire  sourceD_io_bs_radr_valid; // @[Scheduler.scala 77:23]
  wire [2:0] sourceD_io_bs_radr_bits_way; // @[Scheduler.scala 77:23]
  wire [9:0] sourceD_io_bs_radr_bits_set; // @[Scheduler.scala 77:23]
  wire [1:0] sourceD_io_bs_radr_bits_beat; // @[Scheduler.scala 77:23]
  wire [1:0] sourceD_io_bs_radr_bits_mask; // @[Scheduler.scala 77:23]
  wire [127:0] sourceD_io_bs_rdat_data; // @[Scheduler.scala 77:23]
  wire  sourceD_io_bs_wadr_ready; // @[Scheduler.scala 77:23]
  wire  sourceD_io_bs_wadr_valid; // @[Scheduler.scala 77:23]
  wire [2:0] sourceD_io_bs_wadr_bits_way; // @[Scheduler.scala 77:23]
  wire [9:0] sourceD_io_bs_wadr_bits_set; // @[Scheduler.scala 77:23]
  wire [1:0] sourceD_io_bs_wadr_bits_beat; // @[Scheduler.scala 77:23]
  wire [1:0] sourceD_io_bs_wadr_bits_mask; // @[Scheduler.scala 77:23]
  wire [127:0] sourceD_io_bs_wdat_data; // @[Scheduler.scala 77:23]
  wire  sourceD_io_bs_wdat_poison; // @[Scheduler.scala 77:23]
  wire [9:0] sourceD_io_evict_req_set; // @[Scheduler.scala 77:23]
  wire [2:0] sourceD_io_evict_req_way; // @[Scheduler.scala 77:23]
  wire  sourceD_io_evict_safe; // @[Scheduler.scala 77:23]
  wire [9:0] sourceD_io_grant_req_set; // @[Scheduler.scala 77:23]
  wire [2:0] sourceD_io_grant_req_way; // @[Scheduler.scala 77:23]
  wire  sourceD_io_grant_safe; // @[Scheduler.scala 77:23]
  wire  sourceD_io_corrected_valid; // @[Scheduler.scala 77:23]
  wire  sourceD_io_uncorrected_valid; // @[Scheduler.scala 77:23]
  wire  sourceE_rf_reset; // @[Scheduler.scala 78:23]
  wire  sourceE_clock; // @[Scheduler.scala 78:23]
  wire  sourceE_reset; // @[Scheduler.scala 78:23]
  wire  sourceE_io_req_ready; // @[Scheduler.scala 78:23]
  wire  sourceE_io_req_valid; // @[Scheduler.scala 78:23]
  wire [2:0] sourceE_io_req_bits_sink; // @[Scheduler.scala 78:23]
  wire  sourceE_io_e_valid; // @[Scheduler.scala 78:23]
  wire [2:0] sourceE_io_e_bits_sink; // @[Scheduler.scala 78:23]
  wire  sourceX_clock; // @[Scheduler.scala 79:23]
  wire  sourceX_reset; // @[Scheduler.scala 79:23]
  wire  sourceX_io_req_ready; // @[Scheduler.scala 79:23]
  wire  sourceX_io_req_valid; // @[Scheduler.scala 79:23]
  wire  sourceX_io_x_ready; // @[Scheduler.scala 79:23]
  wire  sourceX_io_x_valid; // @[Scheduler.scala 79:23]
  wire  sinkA_rf_reset; // @[Scheduler.scala 88:21]
  wire  sinkA_clock; // @[Scheduler.scala 88:21]
  wire  sinkA_reset; // @[Scheduler.scala 88:21]
  wire  sinkA_io_req_ready; // @[Scheduler.scala 88:21]
  wire  sinkA_io_req_valid; // @[Scheduler.scala 88:21]
  wire [2:0] sinkA_io_req_bits_opcode; // @[Scheduler.scala 88:21]
  wire [2:0] sinkA_io_req_bits_param; // @[Scheduler.scala 88:21]
  wire [2:0] sinkA_io_req_bits_size; // @[Scheduler.scala 88:21]
  wire [6:0] sinkA_io_req_bits_source; // @[Scheduler.scala 88:21]
  wire [17:0] sinkA_io_req_bits_tag; // @[Scheduler.scala 88:21]
  wire [5:0] sinkA_io_req_bits_offset; // @[Scheduler.scala 88:21]
  wire [4:0] sinkA_io_req_bits_put; // @[Scheduler.scala 88:21]
  wire [9:0] sinkA_io_req_bits_set; // @[Scheduler.scala 88:21]
  wire  sinkA_io_a_ready; // @[Scheduler.scala 88:21]
  wire  sinkA_io_a_valid; // @[Scheduler.scala 88:21]
  wire [2:0] sinkA_io_a_bits_opcode; // @[Scheduler.scala 88:21]
  wire [2:0] sinkA_io_a_bits_param; // @[Scheduler.scala 88:21]
  wire [2:0] sinkA_io_a_bits_size; // @[Scheduler.scala 88:21]
  wire [6:0] sinkA_io_a_bits_source; // @[Scheduler.scala 88:21]
  wire [35:0] sinkA_io_a_bits_address; // @[Scheduler.scala 88:21]
  wire [15:0] sinkA_io_a_bits_mask; // @[Scheduler.scala 88:21]
  wire [127:0] sinkA_io_a_bits_data; // @[Scheduler.scala 88:21]
  wire  sinkA_io_a_bits_corrupt; // @[Scheduler.scala 88:21]
  wire  sinkA_io_pb_pop_ready; // @[Scheduler.scala 88:21]
  wire  sinkA_io_pb_pop_valid; // @[Scheduler.scala 88:21]
  wire [4:0] sinkA_io_pb_pop_bits_index; // @[Scheduler.scala 88:21]
  wire  sinkA_io_pb_pop_bits_last; // @[Scheduler.scala 88:21]
  wire [127:0] sinkA_io_pb_beat_data; // @[Scheduler.scala 88:21]
  wire [15:0] sinkA_io_pb_beat_mask; // @[Scheduler.scala 88:21]
  wire  sinkA_io_pb_beat_corrupt; // @[Scheduler.scala 88:21]
  wire  sinkC_rf_reset; // @[Scheduler.scala 90:21]
  wire  sinkC_clock; // @[Scheduler.scala 90:21]
  wire  sinkC_reset; // @[Scheduler.scala 90:21]
  wire  sinkC_io_req_ready; // @[Scheduler.scala 90:21]
  wire  sinkC_io_req_valid; // @[Scheduler.scala 90:21]
  wire [2:0] sinkC_io_req_bits_opcode; // @[Scheduler.scala 90:21]
  wire [2:0] sinkC_io_req_bits_param; // @[Scheduler.scala 90:21]
  wire [2:0] sinkC_io_req_bits_size; // @[Scheduler.scala 90:21]
  wire [6:0] sinkC_io_req_bits_source; // @[Scheduler.scala 90:21]
  wire [17:0] sinkC_io_req_bits_tag; // @[Scheduler.scala 90:21]
  wire [5:0] sinkC_io_req_bits_offset; // @[Scheduler.scala 90:21]
  wire [4:0] sinkC_io_req_bits_put; // @[Scheduler.scala 90:21]
  wire [9:0] sinkC_io_req_bits_set; // @[Scheduler.scala 90:21]
  wire  sinkC_io_resp_valid; // @[Scheduler.scala 90:21]
  wire  sinkC_io_resp_bits_last; // @[Scheduler.scala 90:21]
  wire [9:0] sinkC_io_resp_bits_set; // @[Scheduler.scala 90:21]
  wire [17:0] sinkC_io_resp_bits_tag; // @[Scheduler.scala 90:21]
  wire [6:0] sinkC_io_resp_bits_source; // @[Scheduler.scala 90:21]
  wire [2:0] sinkC_io_resp_bits_param; // @[Scheduler.scala 90:21]
  wire  sinkC_io_resp_bits_data; // @[Scheduler.scala 90:21]
  wire  sinkC_io_c_ready; // @[Scheduler.scala 90:21]
  wire  sinkC_io_c_valid; // @[Scheduler.scala 90:21]
  wire [2:0] sinkC_io_c_bits_opcode; // @[Scheduler.scala 90:21]
  wire [2:0] sinkC_io_c_bits_param; // @[Scheduler.scala 90:21]
  wire [2:0] sinkC_io_c_bits_size; // @[Scheduler.scala 90:21]
  wire [6:0] sinkC_io_c_bits_source; // @[Scheduler.scala 90:21]
  wire [35:0] sinkC_io_c_bits_address; // @[Scheduler.scala 90:21]
  wire [127:0] sinkC_io_c_bits_data; // @[Scheduler.scala 90:21]
  wire  sinkC_io_c_bits_corrupt; // @[Scheduler.scala 90:21]
  wire [9:0] sinkC_io_set; // @[Scheduler.scala 90:21]
  wire [2:0] sinkC_io_way; // @[Scheduler.scala 90:21]
  wire  sinkC_io_bs_adr_ready; // @[Scheduler.scala 90:21]
  wire  sinkC_io_bs_adr_valid; // @[Scheduler.scala 90:21]
  wire  sinkC_io_bs_adr_bits_noop; // @[Scheduler.scala 90:21]
  wire [2:0] sinkC_io_bs_adr_bits_way; // @[Scheduler.scala 90:21]
  wire [9:0] sinkC_io_bs_adr_bits_set; // @[Scheduler.scala 90:21]
  wire [1:0] sinkC_io_bs_adr_bits_beat; // @[Scheduler.scala 90:21]
  wire [127:0] sinkC_io_bs_dat_data; // @[Scheduler.scala 90:21]
  wire  sinkC_io_bs_dat_poison; // @[Scheduler.scala 90:21]
  wire  sinkC_io_rel_pop_ready; // @[Scheduler.scala 90:21]
  wire  sinkC_io_rel_pop_valid; // @[Scheduler.scala 90:21]
  wire [4:0] sinkC_io_rel_pop_bits_index; // @[Scheduler.scala 90:21]
  wire  sinkC_io_rel_pop_bits_last; // @[Scheduler.scala 90:21]
  wire [127:0] sinkC_io_rel_beat_data; // @[Scheduler.scala 90:21]
  wire  sinkC_io_rel_beat_corrupt; // @[Scheduler.scala 90:21]
  wire  sinkD_rf_reset; // @[Scheduler.scala 91:21]
  wire  sinkD_clock; // @[Scheduler.scala 91:21]
  wire  sinkD_reset; // @[Scheduler.scala 91:21]
  wire  sinkD_io_resp_valid; // @[Scheduler.scala 91:21]
  wire  sinkD_io_resp_bits_last; // @[Scheduler.scala 91:21]
  wire [2:0] sinkD_io_resp_bits_opcode; // @[Scheduler.scala 91:21]
  wire [2:0] sinkD_io_resp_bits_param; // @[Scheduler.scala 91:21]
  wire [2:0] sinkD_io_resp_bits_source; // @[Scheduler.scala 91:21]
  wire [2:0] sinkD_io_resp_bits_sink; // @[Scheduler.scala 91:21]
  wire  sinkD_io_resp_bits_denied; // @[Scheduler.scala 91:21]
  wire  sinkD_io_d_ready; // @[Scheduler.scala 91:21]
  wire  sinkD_io_d_valid; // @[Scheduler.scala 91:21]
  wire [2:0] sinkD_io_d_bits_opcode; // @[Scheduler.scala 91:21]
  wire [1:0] sinkD_io_d_bits_param; // @[Scheduler.scala 91:21]
  wire [2:0] sinkD_io_d_bits_size; // @[Scheduler.scala 91:21]
  wire [2:0] sinkD_io_d_bits_source; // @[Scheduler.scala 91:21]
  wire [2:0] sinkD_io_d_bits_sink; // @[Scheduler.scala 91:21]
  wire  sinkD_io_d_bits_denied; // @[Scheduler.scala 91:21]
  wire [127:0] sinkD_io_d_bits_data; // @[Scheduler.scala 91:21]
  wire  sinkD_io_d_bits_corrupt; // @[Scheduler.scala 91:21]
  wire [2:0] sinkD_io_source; // @[Scheduler.scala 91:21]
  wire [2:0] sinkD_io_way; // @[Scheduler.scala 91:21]
  wire [9:0] sinkD_io_set; // @[Scheduler.scala 91:21]
  wire  sinkD_io_bs_adr_ready; // @[Scheduler.scala 91:21]
  wire  sinkD_io_bs_adr_valid; // @[Scheduler.scala 91:21]
  wire  sinkD_io_bs_adr_bits_noop; // @[Scheduler.scala 91:21]
  wire [2:0] sinkD_io_bs_adr_bits_way; // @[Scheduler.scala 91:21]
  wire [9:0] sinkD_io_bs_adr_bits_set; // @[Scheduler.scala 91:21]
  wire [1:0] sinkD_io_bs_adr_bits_beat; // @[Scheduler.scala 91:21]
  wire [127:0] sinkD_io_bs_dat_data; // @[Scheduler.scala 91:21]
  wire [9:0] sinkD_io_grant_req_set; // @[Scheduler.scala 91:21]
  wire [2:0] sinkD_io_grant_req_way; // @[Scheduler.scala 91:21]
  wire  sinkD_io_grant_safe; // @[Scheduler.scala 91:21]
  wire  sinkE_io_resp_valid; // @[Scheduler.scala 92:21]
  wire [2:0] sinkE_io_resp_bits_sink; // @[Scheduler.scala 92:21]
  wire  sinkE_io_e_valid; // @[Scheduler.scala 92:21]
  wire [2:0] sinkE_io_e_bits_sink; // @[Scheduler.scala 92:21]
  wire  sinkX_rf_reset; // @[Scheduler.scala 93:21]
  wire  sinkX_clock; // @[Scheduler.scala 93:21]
  wire  sinkX_reset; // @[Scheduler.scala 93:21]
  wire  sinkX_io_req_ready; // @[Scheduler.scala 93:21]
  wire  sinkX_io_req_valid; // @[Scheduler.scala 93:21]
  wire [17:0] sinkX_io_req_bits_tag; // @[Scheduler.scala 93:21]
  wire [9:0] sinkX_io_req_bits_set; // @[Scheduler.scala 93:21]
  wire  sinkX_io_x_ready; // @[Scheduler.scala 93:21]
  wire  sinkX_io_x_valid; // @[Scheduler.scala 93:21]
  wire [35:0] sinkX_io_x_bits_address; // @[Scheduler.scala 93:21]
  wire  directory_rf_reset; // @[Scheduler.scala 102:25]
  wire  directory_clock; // @[Scheduler.scala 102:25]
  wire  directory_reset; // @[Scheduler.scala 102:25]
  wire  directory_io_write_ready; // @[Scheduler.scala 102:25]
  wire  directory_io_write_valid; // @[Scheduler.scala 102:25]
  wire [9:0] directory_io_write_bits_set; // @[Scheduler.scala 102:25]
  wire [2:0] directory_io_write_bits_way; // @[Scheduler.scala 102:25]
  wire  directory_io_write_bits_data_dirty; // @[Scheduler.scala 102:25]
  wire [1:0] directory_io_write_bits_data_state; // @[Scheduler.scala 102:25]
  wire [3:0] directory_io_write_bits_data_clients; // @[Scheduler.scala 102:25]
  wire [17:0] directory_io_write_bits_data_tag; // @[Scheduler.scala 102:25]
  wire  directory_io_read_valid; // @[Scheduler.scala 102:25]
  wire [6:0] directory_io_read_bits_source; // @[Scheduler.scala 102:25]
  wire [9:0] directory_io_read_bits_set; // @[Scheduler.scala 102:25]
  wire [17:0] directory_io_read_bits_tag; // @[Scheduler.scala 102:25]
  wire  directory_io_result_bits_dirty; // @[Scheduler.scala 102:25]
  wire [1:0] directory_io_result_bits_state; // @[Scheduler.scala 102:25]
  wire [3:0] directory_io_result_bits_clients; // @[Scheduler.scala 102:25]
  wire [17:0] directory_io_result_bits_tag; // @[Scheduler.scala 102:25]
  wire  directory_io_result_bits_hit; // @[Scheduler.scala 102:25]
  wire [2:0] directory_io_result_bits_way; // @[Scheduler.scala 102:25]
  wire  directory_io_ready; // @[Scheduler.scala 102:25]
  wire [7:0] directory_io_ways_0; // @[Scheduler.scala 102:25]
  wire [7:0] directory_io_ways_1; // @[Scheduler.scala 102:25]
  wire [7:0] directory_io_ways_2; // @[Scheduler.scala 102:25]
  wire [7:0] directory_io_ways_3; // @[Scheduler.scala 102:25]
  wire [7:0] directory_io_ways_4; // @[Scheduler.scala 102:25]
  wire [7:0] directory_io_ways_5; // @[Scheduler.scala 102:25]
  wire [7:0] directory_io_ways_6; // @[Scheduler.scala 102:25]
  wire [7:0] directory_io_ways_7; // @[Scheduler.scala 102:25]
  wire [7:0] directory_io_ways_8; // @[Scheduler.scala 102:25]
  wire [7:0] directory_io_ways_9; // @[Scheduler.scala 102:25]
  wire [7:0] directory_io_ways_10; // @[Scheduler.scala 102:25]
  wire [7:0] directory_io_ways_11; // @[Scheduler.scala 102:25]
  wire [7:0] directory_io_ways_12; // @[Scheduler.scala 102:25]
  wire [7:0] directory_io_ways_13; // @[Scheduler.scala 102:25]
  wire [10:0] directory_io_divs_0; // @[Scheduler.scala 102:25]
  wire [10:0] directory_io_divs_1; // @[Scheduler.scala 102:25]
  wire [10:0] directory_io_divs_2; // @[Scheduler.scala 102:25]
  wire [10:0] directory_io_divs_3; // @[Scheduler.scala 102:25]
  wire [10:0] directory_io_divs_4; // @[Scheduler.scala 102:25]
  wire [10:0] directory_io_divs_5; // @[Scheduler.scala 102:25]
  wire [10:0] directory_io_divs_6; // @[Scheduler.scala 102:25]
  wire [10:0] directory_io_divs_7; // @[Scheduler.scala 102:25]
  wire [10:0] directory_io_divs_8; // @[Scheduler.scala 102:25]
  wire [10:0] directory_io_divs_9; // @[Scheduler.scala 102:25]
  wire [10:0] directory_io_divs_10; // @[Scheduler.scala 102:25]
  wire [10:0] directory_io_divs_11; // @[Scheduler.scala 102:25]
  wire [10:0] directory_io_divs_12; // @[Scheduler.scala 102:25]
  wire [10:0] directory_io_divs_13; // @[Scheduler.scala 102:25]
  wire  directory_io_corrected_valid; // @[Scheduler.scala 102:25]
  wire  directory_io_error_ready; // @[Scheduler.scala 102:25]
  wire  directory_io_error_valid; // @[Scheduler.scala 102:25]
  wire [7:0] directory_io_error_bits_bit; // @[Scheduler.scala 102:25]
  wire  bankedStore_rf_reset; // @[Scheduler.scala 103:27]
  wire  bankedStore_clock; // @[Scheduler.scala 103:27]
  wire  bankedStore_reset; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_side_adr_ready; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_side_adr_valid; // @[Scheduler.scala 103:27]
  wire [2:0] bankedStore_io_side_adr_bits_way; // @[Scheduler.scala 103:27]
  wire [9:0] bankedStore_io_side_adr_bits_set; // @[Scheduler.scala 103:27]
  wire [1:0] bankedStore_io_side_adr_bits_beat; // @[Scheduler.scala 103:27]
  wire [1:0] bankedStore_io_side_adr_bits_mask; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_side_adr_bits_write; // @[Scheduler.scala 103:27]
  wire [127:0] bankedStore_io_side_wdat_data; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_side_wdat_poison; // @[Scheduler.scala 103:27]
  wire [127:0] bankedStore_io_side_rdat_data; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_sinkC_adr_ready; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_sinkC_adr_valid; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_sinkC_adr_bits_noop; // @[Scheduler.scala 103:27]
  wire [2:0] bankedStore_io_sinkC_adr_bits_way; // @[Scheduler.scala 103:27]
  wire [9:0] bankedStore_io_sinkC_adr_bits_set; // @[Scheduler.scala 103:27]
  wire [1:0] bankedStore_io_sinkC_adr_bits_beat; // @[Scheduler.scala 103:27]
  wire [127:0] bankedStore_io_sinkC_dat_data; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_sinkC_dat_poison; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_sinkD_adr_ready; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_sinkD_adr_valid; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_sinkD_adr_bits_noop; // @[Scheduler.scala 103:27]
  wire [2:0] bankedStore_io_sinkD_adr_bits_way; // @[Scheduler.scala 103:27]
  wire [9:0] bankedStore_io_sinkD_adr_bits_set; // @[Scheduler.scala 103:27]
  wire [1:0] bankedStore_io_sinkD_adr_bits_beat; // @[Scheduler.scala 103:27]
  wire [127:0] bankedStore_io_sinkD_dat_data; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_sourceC_adr_ready; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_sourceC_adr_valid; // @[Scheduler.scala 103:27]
  wire [2:0] bankedStore_io_sourceC_adr_bits_way; // @[Scheduler.scala 103:27]
  wire [9:0] bankedStore_io_sourceC_adr_bits_set; // @[Scheduler.scala 103:27]
  wire [1:0] bankedStore_io_sourceC_adr_bits_beat; // @[Scheduler.scala 103:27]
  wire [127:0] bankedStore_io_sourceC_dat_data; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_sourceD_radr_ready; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_sourceD_radr_valid; // @[Scheduler.scala 103:27]
  wire [2:0] bankedStore_io_sourceD_radr_bits_way; // @[Scheduler.scala 103:27]
  wire [9:0] bankedStore_io_sourceD_radr_bits_set; // @[Scheduler.scala 103:27]
  wire [1:0] bankedStore_io_sourceD_radr_bits_beat; // @[Scheduler.scala 103:27]
  wire [1:0] bankedStore_io_sourceD_radr_bits_mask; // @[Scheduler.scala 103:27]
  wire [127:0] bankedStore_io_sourceD_rdat_data; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_sourceD_wadr_ready; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_sourceD_wadr_valid; // @[Scheduler.scala 103:27]
  wire [2:0] bankedStore_io_sourceD_wadr_bits_way; // @[Scheduler.scala 103:27]
  wire [9:0] bankedStore_io_sourceD_wadr_bits_set; // @[Scheduler.scala 103:27]
  wire [1:0] bankedStore_io_sourceD_wadr_bits_beat; // @[Scheduler.scala 103:27]
  wire [1:0] bankedStore_io_sourceD_wadr_bits_mask; // @[Scheduler.scala 103:27]
  wire [127:0] bankedStore_io_sourceD_wdat_data; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_sourceD_wdat_poison; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_error_ready; // @[Scheduler.scala 103:27]
  wire  bankedStore_io_error_valid; // @[Scheduler.scala 103:27]
  wire [7:0] bankedStore_io_error_bits_bit; // @[Scheduler.scala 103:27]
  wire  requests_rf_reset; // @[Scheduler.scala 104:24]
  wire  requests_clock; // @[Scheduler.scala 104:24]
  wire  requests_reset; // @[Scheduler.scala 104:24]
  wire  requests_io_push_ready; // @[Scheduler.scala 104:24]
  wire  requests_io_push_valid; // @[Scheduler.scala 104:24]
  wire [4:0] requests_io_push_bits_index; // @[Scheduler.scala 104:24]
  wire  requests_io_push_bits_data_prio_0; // @[Scheduler.scala 104:24]
  wire  requests_io_push_bits_data_prio_2; // @[Scheduler.scala 104:24]
  wire  requests_io_push_bits_data_control; // @[Scheduler.scala 104:24]
  wire [2:0] requests_io_push_bits_data_opcode; // @[Scheduler.scala 104:24]
  wire [2:0] requests_io_push_bits_data_param; // @[Scheduler.scala 104:24]
  wire [2:0] requests_io_push_bits_data_size; // @[Scheduler.scala 104:24]
  wire [6:0] requests_io_push_bits_data_source; // @[Scheduler.scala 104:24]
  wire [17:0] requests_io_push_bits_data_tag; // @[Scheduler.scala 104:24]
  wire [5:0] requests_io_push_bits_data_offset; // @[Scheduler.scala 104:24]
  wire [4:0] requests_io_push_bits_data_put; // @[Scheduler.scala 104:24]
  wire [23:0] requests_io_valid; // @[Scheduler.scala 104:24]
  wire  requests_io_pop_valid; // @[Scheduler.scala 104:24]
  wire [4:0] requests_io_pop_bits; // @[Scheduler.scala 104:24]
  wire  requests_io_data_prio_0; // @[Scheduler.scala 104:24]
  wire  requests_io_data_prio_2; // @[Scheduler.scala 104:24]
  wire  requests_io_data_control; // @[Scheduler.scala 104:24]
  wire [2:0] requests_io_data_opcode; // @[Scheduler.scala 104:24]
  wire [2:0] requests_io_data_param; // @[Scheduler.scala 104:24]
  wire [2:0] requests_io_data_size; // @[Scheduler.scala 104:24]
  wire [6:0] requests_io_data_source; // @[Scheduler.scala 104:24]
  wire [17:0] requests_io_data_tag; // @[Scheduler.scala 104:24]
  wire [5:0] requests_io_data_offset; // @[Scheduler.scala 104:24]
  wire [4:0] requests_io_data_put; // @[Scheduler.scala 104:24]
  wire  abc_mshrs_0_rf_reset; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_clock; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_reset; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_allocate_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_allocate_bits_prio_0; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_allocate_bits_prio_2; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_allocate_bits_control; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_allocate_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_allocate_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_allocate_bits_size; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_0_io_allocate_bits_source; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_0_io_allocate_bits_tag; // @[Scheduler.scala 105:48]
  wire [5:0] abc_mshrs_0_io_allocate_bits_offset; // @[Scheduler.scala 105:48]
  wire [4:0] abc_mshrs_0_io_allocate_bits_put; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_0_io_allocate_bits_set; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_allocate_bits_repeat; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_directory_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_directory_bits_dirty; // @[Scheduler.scala 105:48]
  wire [1:0] abc_mshrs_0_io_directory_bits_state; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_0_io_directory_bits_clients; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_0_io_directory_bits_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_directory_bits_hit; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_directory_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_status_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_0_io_status_bits_set; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_0_io_status_bits_tag; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_status_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_status_bits_blockB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_status_bits_nestB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_status_bits_blockC; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_status_bits_nestC; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_ready; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_bits_a_valid; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_0_io_schedule_bits_a_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_0_io_schedule_bits_a_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_schedule_bits_a_bits_param; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_bits_a_bits_block; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_bits_b_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_schedule_bits_b_bits_param; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_0_io_schedule_bits_b_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_0_io_schedule_bits_b_bits_set; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_0_io_schedule_bits_b_bits_clients; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_bits_c_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_schedule_bits_c_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_schedule_bits_c_bits_param; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_0_io_schedule_bits_c_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_0_io_schedule_bits_c_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_schedule_bits_c_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_bits_c_bits_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_bits_d_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_bits_d_bits_prio_0; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_bits_d_bits_prio_2; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_schedule_bits_d_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_schedule_bits_d_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_schedule_bits_d_bits_size; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_0_io_schedule_bits_d_bits_source; // @[Scheduler.scala 105:48]
  wire [5:0] abc_mshrs_0_io_schedule_bits_d_bits_offset; // @[Scheduler.scala 105:48]
  wire [4:0] abc_mshrs_0_io_schedule_bits_d_bits_put; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_0_io_schedule_bits_d_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_schedule_bits_d_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_bits_d_bits_bad; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_bits_d_bits_user_hit_cache; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_bits_e_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_schedule_bits_e_bits_sink; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_bits_x_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_bits_dir_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_0_io_schedule_bits_dir_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_schedule_bits_dir_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 105:48]
  wire [1:0] abc_mshrs_0_io_schedule_bits_dir_bits_data_state; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_0_io_schedule_bits_dir_bits_data_clients; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_0_io_schedule_bits_dir_bits_data_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_schedule_bits_reload; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_sinkc_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_sinkc_bits_last; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_0_io_sinkc_bits_tag; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_0_io_sinkc_bits_source; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_sinkc_bits_param; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_sinkc_bits_data; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_sinkd_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_sinkd_bits_last; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_sinkd_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_sinkd_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_0_io_sinkd_bits_sink; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_sinkd_bits_denied; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_sinke_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_0_io_nestedwb_set; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_0_io_nestedwb_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_nestedwb_b_toN; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_nestedwb_b_toB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_nestedwb_b_clr_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_0_io_nestedwb_c_set_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_rf_reset; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_clock; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_reset; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_allocate_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_allocate_bits_prio_0; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_allocate_bits_prio_2; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_allocate_bits_control; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_allocate_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_allocate_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_allocate_bits_size; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_1_io_allocate_bits_source; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_1_io_allocate_bits_tag; // @[Scheduler.scala 105:48]
  wire [5:0] abc_mshrs_1_io_allocate_bits_offset; // @[Scheduler.scala 105:48]
  wire [4:0] abc_mshrs_1_io_allocate_bits_put; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_1_io_allocate_bits_set; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_allocate_bits_repeat; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_directory_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_directory_bits_dirty; // @[Scheduler.scala 105:48]
  wire [1:0] abc_mshrs_1_io_directory_bits_state; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_1_io_directory_bits_clients; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_1_io_directory_bits_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_directory_bits_hit; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_directory_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_status_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_1_io_status_bits_set; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_1_io_status_bits_tag; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_status_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_status_bits_blockB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_status_bits_nestB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_status_bits_blockC; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_status_bits_nestC; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_ready; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_bits_a_valid; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_1_io_schedule_bits_a_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_1_io_schedule_bits_a_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_schedule_bits_a_bits_param; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_bits_a_bits_block; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_bits_b_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_schedule_bits_b_bits_param; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_1_io_schedule_bits_b_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_1_io_schedule_bits_b_bits_set; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_1_io_schedule_bits_b_bits_clients; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_bits_c_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_schedule_bits_c_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_schedule_bits_c_bits_param; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_1_io_schedule_bits_c_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_1_io_schedule_bits_c_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_schedule_bits_c_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_bits_c_bits_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_bits_d_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_bits_d_bits_prio_0; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_bits_d_bits_prio_2; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_schedule_bits_d_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_schedule_bits_d_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_schedule_bits_d_bits_size; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_1_io_schedule_bits_d_bits_source; // @[Scheduler.scala 105:48]
  wire [5:0] abc_mshrs_1_io_schedule_bits_d_bits_offset; // @[Scheduler.scala 105:48]
  wire [4:0] abc_mshrs_1_io_schedule_bits_d_bits_put; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_1_io_schedule_bits_d_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_schedule_bits_d_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_bits_d_bits_bad; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_bits_d_bits_user_hit_cache; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_bits_e_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_schedule_bits_e_bits_sink; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_bits_x_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_bits_dir_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_1_io_schedule_bits_dir_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_schedule_bits_dir_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 105:48]
  wire [1:0] abc_mshrs_1_io_schedule_bits_dir_bits_data_state; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_1_io_schedule_bits_dir_bits_data_clients; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_1_io_schedule_bits_dir_bits_data_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_schedule_bits_reload; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_sinkc_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_sinkc_bits_last; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_1_io_sinkc_bits_tag; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_1_io_sinkc_bits_source; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_sinkc_bits_param; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_sinkc_bits_data; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_sinkd_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_sinkd_bits_last; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_sinkd_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_sinkd_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_1_io_sinkd_bits_sink; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_sinkd_bits_denied; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_sinke_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_1_io_nestedwb_set; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_1_io_nestedwb_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_nestedwb_b_toN; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_nestedwb_b_toB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_nestedwb_b_clr_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_1_io_nestedwb_c_set_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_rf_reset; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_clock; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_reset; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_allocate_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_allocate_bits_prio_0; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_allocate_bits_prio_2; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_allocate_bits_control; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_allocate_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_allocate_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_allocate_bits_size; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_2_io_allocate_bits_source; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_2_io_allocate_bits_tag; // @[Scheduler.scala 105:48]
  wire [5:0] abc_mshrs_2_io_allocate_bits_offset; // @[Scheduler.scala 105:48]
  wire [4:0] abc_mshrs_2_io_allocate_bits_put; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_2_io_allocate_bits_set; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_allocate_bits_repeat; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_directory_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_directory_bits_dirty; // @[Scheduler.scala 105:48]
  wire [1:0] abc_mshrs_2_io_directory_bits_state; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_2_io_directory_bits_clients; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_2_io_directory_bits_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_directory_bits_hit; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_directory_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_status_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_2_io_status_bits_set; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_2_io_status_bits_tag; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_status_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_status_bits_blockB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_status_bits_nestB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_status_bits_blockC; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_status_bits_nestC; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_ready; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_bits_a_valid; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_2_io_schedule_bits_a_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_2_io_schedule_bits_a_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_schedule_bits_a_bits_param; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_bits_a_bits_block; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_bits_b_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_schedule_bits_b_bits_param; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_2_io_schedule_bits_b_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_2_io_schedule_bits_b_bits_set; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_2_io_schedule_bits_b_bits_clients; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_bits_c_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_schedule_bits_c_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_schedule_bits_c_bits_param; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_2_io_schedule_bits_c_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_2_io_schedule_bits_c_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_schedule_bits_c_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_bits_c_bits_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_bits_d_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_bits_d_bits_prio_0; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_bits_d_bits_prio_2; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_schedule_bits_d_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_schedule_bits_d_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_schedule_bits_d_bits_size; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_2_io_schedule_bits_d_bits_source; // @[Scheduler.scala 105:48]
  wire [5:0] abc_mshrs_2_io_schedule_bits_d_bits_offset; // @[Scheduler.scala 105:48]
  wire [4:0] abc_mshrs_2_io_schedule_bits_d_bits_put; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_2_io_schedule_bits_d_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_schedule_bits_d_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_bits_d_bits_bad; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_bits_d_bits_user_hit_cache; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_bits_e_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_schedule_bits_e_bits_sink; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_bits_x_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_bits_dir_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_2_io_schedule_bits_dir_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_schedule_bits_dir_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 105:48]
  wire [1:0] abc_mshrs_2_io_schedule_bits_dir_bits_data_state; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_2_io_schedule_bits_dir_bits_data_clients; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_2_io_schedule_bits_dir_bits_data_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_schedule_bits_reload; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_sinkc_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_sinkc_bits_last; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_2_io_sinkc_bits_tag; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_2_io_sinkc_bits_source; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_sinkc_bits_param; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_sinkc_bits_data; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_sinkd_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_sinkd_bits_last; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_sinkd_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_sinkd_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_2_io_sinkd_bits_sink; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_sinkd_bits_denied; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_sinke_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_2_io_nestedwb_set; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_2_io_nestedwb_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_nestedwb_b_toN; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_nestedwb_b_toB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_nestedwb_b_clr_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_2_io_nestedwb_c_set_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_rf_reset; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_clock; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_reset; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_allocate_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_allocate_bits_prio_0; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_allocate_bits_prio_2; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_allocate_bits_control; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_allocate_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_allocate_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_allocate_bits_size; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_3_io_allocate_bits_source; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_3_io_allocate_bits_tag; // @[Scheduler.scala 105:48]
  wire [5:0] abc_mshrs_3_io_allocate_bits_offset; // @[Scheduler.scala 105:48]
  wire [4:0] abc_mshrs_3_io_allocate_bits_put; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_3_io_allocate_bits_set; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_allocate_bits_repeat; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_directory_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_directory_bits_dirty; // @[Scheduler.scala 105:48]
  wire [1:0] abc_mshrs_3_io_directory_bits_state; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_3_io_directory_bits_clients; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_3_io_directory_bits_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_directory_bits_hit; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_directory_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_status_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_3_io_status_bits_set; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_3_io_status_bits_tag; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_status_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_status_bits_blockB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_status_bits_nestB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_status_bits_blockC; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_status_bits_nestC; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_ready; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_bits_a_valid; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_3_io_schedule_bits_a_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_3_io_schedule_bits_a_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_schedule_bits_a_bits_param; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_bits_a_bits_block; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_bits_b_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_schedule_bits_b_bits_param; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_3_io_schedule_bits_b_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_3_io_schedule_bits_b_bits_set; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_3_io_schedule_bits_b_bits_clients; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_bits_c_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_schedule_bits_c_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_schedule_bits_c_bits_param; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_3_io_schedule_bits_c_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_3_io_schedule_bits_c_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_schedule_bits_c_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_bits_c_bits_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_bits_d_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_bits_d_bits_prio_0; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_bits_d_bits_prio_2; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_schedule_bits_d_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_schedule_bits_d_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_schedule_bits_d_bits_size; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_3_io_schedule_bits_d_bits_source; // @[Scheduler.scala 105:48]
  wire [5:0] abc_mshrs_3_io_schedule_bits_d_bits_offset; // @[Scheduler.scala 105:48]
  wire [4:0] abc_mshrs_3_io_schedule_bits_d_bits_put; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_3_io_schedule_bits_d_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_schedule_bits_d_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_bits_d_bits_bad; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_bits_d_bits_user_hit_cache; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_bits_e_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_schedule_bits_e_bits_sink; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_bits_x_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_bits_dir_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_3_io_schedule_bits_dir_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_schedule_bits_dir_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 105:48]
  wire [1:0] abc_mshrs_3_io_schedule_bits_dir_bits_data_state; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_3_io_schedule_bits_dir_bits_data_clients; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_3_io_schedule_bits_dir_bits_data_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_schedule_bits_reload; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_sinkc_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_sinkc_bits_last; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_3_io_sinkc_bits_tag; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_3_io_sinkc_bits_source; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_sinkc_bits_param; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_sinkc_bits_data; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_sinkd_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_sinkd_bits_last; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_sinkd_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_sinkd_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_3_io_sinkd_bits_sink; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_sinkd_bits_denied; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_sinke_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_3_io_nestedwb_set; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_3_io_nestedwb_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_nestedwb_b_toN; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_nestedwb_b_toB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_nestedwb_b_clr_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_3_io_nestedwb_c_set_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_rf_reset; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_clock; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_reset; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_allocate_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_allocate_bits_prio_0; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_allocate_bits_prio_2; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_allocate_bits_control; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_allocate_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_allocate_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_allocate_bits_size; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_4_io_allocate_bits_source; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_4_io_allocate_bits_tag; // @[Scheduler.scala 105:48]
  wire [5:0] abc_mshrs_4_io_allocate_bits_offset; // @[Scheduler.scala 105:48]
  wire [4:0] abc_mshrs_4_io_allocate_bits_put; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_4_io_allocate_bits_set; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_allocate_bits_repeat; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_directory_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_directory_bits_dirty; // @[Scheduler.scala 105:48]
  wire [1:0] abc_mshrs_4_io_directory_bits_state; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_4_io_directory_bits_clients; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_4_io_directory_bits_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_directory_bits_hit; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_directory_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_status_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_4_io_status_bits_set; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_4_io_status_bits_tag; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_status_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_status_bits_blockB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_status_bits_nestB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_status_bits_blockC; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_status_bits_nestC; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_ready; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_bits_a_valid; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_4_io_schedule_bits_a_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_4_io_schedule_bits_a_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_schedule_bits_a_bits_param; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_bits_a_bits_block; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_bits_b_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_schedule_bits_b_bits_param; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_4_io_schedule_bits_b_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_4_io_schedule_bits_b_bits_set; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_4_io_schedule_bits_b_bits_clients; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_bits_c_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_schedule_bits_c_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_schedule_bits_c_bits_param; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_4_io_schedule_bits_c_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_4_io_schedule_bits_c_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_schedule_bits_c_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_bits_c_bits_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_bits_d_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_bits_d_bits_prio_0; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_bits_d_bits_prio_2; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_schedule_bits_d_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_schedule_bits_d_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_schedule_bits_d_bits_size; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_4_io_schedule_bits_d_bits_source; // @[Scheduler.scala 105:48]
  wire [5:0] abc_mshrs_4_io_schedule_bits_d_bits_offset; // @[Scheduler.scala 105:48]
  wire [4:0] abc_mshrs_4_io_schedule_bits_d_bits_put; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_4_io_schedule_bits_d_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_schedule_bits_d_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_bits_d_bits_bad; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_bits_d_bits_user_hit_cache; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_bits_e_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_schedule_bits_e_bits_sink; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_bits_x_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_bits_dir_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_4_io_schedule_bits_dir_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_schedule_bits_dir_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 105:48]
  wire [1:0] abc_mshrs_4_io_schedule_bits_dir_bits_data_state; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_4_io_schedule_bits_dir_bits_data_clients; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_4_io_schedule_bits_dir_bits_data_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_schedule_bits_reload; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_sinkc_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_sinkc_bits_last; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_4_io_sinkc_bits_tag; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_4_io_sinkc_bits_source; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_sinkc_bits_param; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_sinkc_bits_data; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_sinkd_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_sinkd_bits_last; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_sinkd_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_sinkd_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_4_io_sinkd_bits_sink; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_sinkd_bits_denied; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_sinke_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_4_io_nestedwb_set; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_4_io_nestedwb_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_nestedwb_b_toN; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_nestedwb_b_toB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_nestedwb_b_clr_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_4_io_nestedwb_c_set_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_rf_reset; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_clock; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_reset; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_allocate_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_allocate_bits_prio_0; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_allocate_bits_prio_2; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_allocate_bits_control; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_allocate_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_allocate_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_allocate_bits_size; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_5_io_allocate_bits_source; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_5_io_allocate_bits_tag; // @[Scheduler.scala 105:48]
  wire [5:0] abc_mshrs_5_io_allocate_bits_offset; // @[Scheduler.scala 105:48]
  wire [4:0] abc_mshrs_5_io_allocate_bits_put; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_5_io_allocate_bits_set; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_allocate_bits_repeat; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_directory_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_directory_bits_dirty; // @[Scheduler.scala 105:48]
  wire [1:0] abc_mshrs_5_io_directory_bits_state; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_5_io_directory_bits_clients; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_5_io_directory_bits_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_directory_bits_hit; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_directory_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_status_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_5_io_status_bits_set; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_5_io_status_bits_tag; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_status_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_status_bits_blockB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_status_bits_nestB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_status_bits_blockC; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_status_bits_nestC; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_ready; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_bits_a_valid; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_5_io_schedule_bits_a_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_5_io_schedule_bits_a_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_schedule_bits_a_bits_param; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_bits_a_bits_block; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_bits_b_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_schedule_bits_b_bits_param; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_5_io_schedule_bits_b_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_5_io_schedule_bits_b_bits_set; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_5_io_schedule_bits_b_bits_clients; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_bits_c_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_schedule_bits_c_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_schedule_bits_c_bits_param; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_5_io_schedule_bits_c_bits_tag; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_5_io_schedule_bits_c_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_schedule_bits_c_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_bits_c_bits_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_bits_d_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_bits_d_bits_prio_0; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_bits_d_bits_prio_2; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_schedule_bits_d_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_schedule_bits_d_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_schedule_bits_d_bits_size; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_5_io_schedule_bits_d_bits_source; // @[Scheduler.scala 105:48]
  wire [5:0] abc_mshrs_5_io_schedule_bits_d_bits_offset; // @[Scheduler.scala 105:48]
  wire [4:0] abc_mshrs_5_io_schedule_bits_d_bits_put; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_5_io_schedule_bits_d_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_schedule_bits_d_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_bits_d_bits_bad; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_bits_d_bits_user_hit_cache; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_bits_e_valid; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_schedule_bits_e_bits_sink; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_bits_x_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_bits_dir_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_5_io_schedule_bits_dir_bits_set; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_schedule_bits_dir_bits_way; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 105:48]
  wire [1:0] abc_mshrs_5_io_schedule_bits_dir_bits_data_state; // @[Scheduler.scala 105:48]
  wire [3:0] abc_mshrs_5_io_schedule_bits_dir_bits_data_clients; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_5_io_schedule_bits_dir_bits_data_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_schedule_bits_reload; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_sinkc_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_sinkc_bits_last; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_5_io_sinkc_bits_tag; // @[Scheduler.scala 105:48]
  wire [6:0] abc_mshrs_5_io_sinkc_bits_source; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_sinkc_bits_param; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_sinkc_bits_data; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_sinkd_valid; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_sinkd_bits_last; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_sinkd_bits_opcode; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_sinkd_bits_param; // @[Scheduler.scala 105:48]
  wire [2:0] abc_mshrs_5_io_sinkd_bits_sink; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_sinkd_bits_denied; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_sinke_valid; // @[Scheduler.scala 105:48]
  wire [9:0] abc_mshrs_5_io_nestedwb_set; // @[Scheduler.scala 105:48]
  wire [17:0] abc_mshrs_5_io_nestedwb_tag; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_nestedwb_b_toN; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_nestedwb_b_toB; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_nestedwb_b_clr_dirty; // @[Scheduler.scala 105:48]
  wire  abc_mshrs_5_io_nestedwb_c_set_dirty; // @[Scheduler.scala 105:48]
  wire  bc_mshr_rf_reset; // @[Scheduler.scala 106:11]
  wire  bc_mshr_clock; // @[Scheduler.scala 106:11]
  wire  bc_mshr_reset; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_allocate_valid; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_allocate_bits_prio_2; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_allocate_bits_control; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_allocate_bits_opcode; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_allocate_bits_param; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_allocate_bits_size; // @[Scheduler.scala 106:11]
  wire [6:0] bc_mshr_io_allocate_bits_source; // @[Scheduler.scala 106:11]
  wire [17:0] bc_mshr_io_allocate_bits_tag; // @[Scheduler.scala 106:11]
  wire [5:0] bc_mshr_io_allocate_bits_offset; // @[Scheduler.scala 106:11]
  wire [4:0] bc_mshr_io_allocate_bits_put; // @[Scheduler.scala 106:11]
  wire [9:0] bc_mshr_io_allocate_bits_set; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_allocate_bits_repeat; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_directory_valid; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_directory_bits_dirty; // @[Scheduler.scala 106:11]
  wire [1:0] bc_mshr_io_directory_bits_state; // @[Scheduler.scala 106:11]
  wire [3:0] bc_mshr_io_directory_bits_clients; // @[Scheduler.scala 106:11]
  wire [17:0] bc_mshr_io_directory_bits_tag; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_directory_bits_hit; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_directory_bits_way; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_status_valid; // @[Scheduler.scala 106:11]
  wire [9:0] bc_mshr_io_status_bits_set; // @[Scheduler.scala 106:11]
  wire [17:0] bc_mshr_io_status_bits_tag; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_status_bits_way; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_status_bits_blockB; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_status_bits_nestB; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_status_bits_blockC; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_status_bits_nestC; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_ready; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_valid; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_bits_a_valid; // @[Scheduler.scala 106:11]
  wire [17:0] bc_mshr_io_schedule_bits_a_bits_tag; // @[Scheduler.scala 106:11]
  wire [9:0] bc_mshr_io_schedule_bits_a_bits_set; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_schedule_bits_a_bits_param; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_bits_a_bits_block; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_bits_b_valid; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_schedule_bits_b_bits_param; // @[Scheduler.scala 106:11]
  wire [17:0] bc_mshr_io_schedule_bits_b_bits_tag; // @[Scheduler.scala 106:11]
  wire [9:0] bc_mshr_io_schedule_bits_b_bits_set; // @[Scheduler.scala 106:11]
  wire [3:0] bc_mshr_io_schedule_bits_b_bits_clients; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_bits_c_valid; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_schedule_bits_c_bits_opcode; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_schedule_bits_c_bits_param; // @[Scheduler.scala 106:11]
  wire [17:0] bc_mshr_io_schedule_bits_c_bits_tag; // @[Scheduler.scala 106:11]
  wire [9:0] bc_mshr_io_schedule_bits_c_bits_set; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_schedule_bits_c_bits_way; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_bits_c_bits_dirty; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_bits_d_valid; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_bits_d_bits_prio_2; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_schedule_bits_d_bits_opcode; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_schedule_bits_d_bits_param; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_schedule_bits_d_bits_size; // @[Scheduler.scala 106:11]
  wire [6:0] bc_mshr_io_schedule_bits_d_bits_source; // @[Scheduler.scala 106:11]
  wire [5:0] bc_mshr_io_schedule_bits_d_bits_offset; // @[Scheduler.scala 106:11]
  wire [4:0] bc_mshr_io_schedule_bits_d_bits_put; // @[Scheduler.scala 106:11]
  wire [9:0] bc_mshr_io_schedule_bits_d_bits_set; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_schedule_bits_d_bits_way; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_bits_d_bits_bad; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_bits_d_bits_user_hit_cache; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_bits_e_valid; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_schedule_bits_e_bits_sink; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_bits_x_valid; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_bits_dir_valid; // @[Scheduler.scala 106:11]
  wire [9:0] bc_mshr_io_schedule_bits_dir_bits_set; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_schedule_bits_dir_bits_way; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 106:11]
  wire [1:0] bc_mshr_io_schedule_bits_dir_bits_data_state; // @[Scheduler.scala 106:11]
  wire [3:0] bc_mshr_io_schedule_bits_dir_bits_data_clients; // @[Scheduler.scala 106:11]
  wire [17:0] bc_mshr_io_schedule_bits_dir_bits_data_tag; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_schedule_bits_reload; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_sinkc_valid; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_sinkc_bits_last; // @[Scheduler.scala 106:11]
  wire [17:0] bc_mshr_io_sinkc_bits_tag; // @[Scheduler.scala 106:11]
  wire [6:0] bc_mshr_io_sinkc_bits_source; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_sinkc_bits_param; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_sinkc_bits_data; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_sinkd_valid; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_sinkd_bits_last; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_sinkd_bits_opcode; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_sinkd_bits_param; // @[Scheduler.scala 106:11]
  wire [2:0] bc_mshr_io_sinkd_bits_sink; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_sinkd_bits_denied; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_sinke_valid; // @[Scheduler.scala 106:11]
  wire [9:0] bc_mshr_io_nestedwb_set; // @[Scheduler.scala 106:11]
  wire [17:0] bc_mshr_io_nestedwb_tag; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_nestedwb_b_toN; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_nestedwb_b_toB; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_nestedwb_b_clr_dirty; // @[Scheduler.scala 106:11]
  wire  bc_mshr_io_nestedwb_c_set_dirty; // @[Scheduler.scala 106:11]
  wire  c_mshr_rf_reset; // @[Scheduler.scala 107:11]
  wire  c_mshr_clock; // @[Scheduler.scala 107:11]
  wire  c_mshr_reset; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_allocate_valid; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_allocate_bits_prio_2; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_allocate_bits_control; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_allocate_bits_opcode; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_allocate_bits_param; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_allocate_bits_size; // @[Scheduler.scala 107:11]
  wire [6:0] c_mshr_io_allocate_bits_source; // @[Scheduler.scala 107:11]
  wire [17:0] c_mshr_io_allocate_bits_tag; // @[Scheduler.scala 107:11]
  wire [5:0] c_mshr_io_allocate_bits_offset; // @[Scheduler.scala 107:11]
  wire [4:0] c_mshr_io_allocate_bits_put; // @[Scheduler.scala 107:11]
  wire [9:0] c_mshr_io_allocate_bits_set; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_allocate_bits_repeat; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_directory_valid; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_directory_bits_dirty; // @[Scheduler.scala 107:11]
  wire [1:0] c_mshr_io_directory_bits_state; // @[Scheduler.scala 107:11]
  wire [3:0] c_mshr_io_directory_bits_clients; // @[Scheduler.scala 107:11]
  wire [17:0] c_mshr_io_directory_bits_tag; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_directory_bits_hit; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_directory_bits_way; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_status_valid; // @[Scheduler.scala 107:11]
  wire [9:0] c_mshr_io_status_bits_set; // @[Scheduler.scala 107:11]
  wire [17:0] c_mshr_io_status_bits_tag; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_status_bits_way; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_status_bits_blockB; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_status_bits_nestB; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_status_bits_blockC; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_status_bits_nestC; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_ready; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_valid; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_bits_a_valid; // @[Scheduler.scala 107:11]
  wire [17:0] c_mshr_io_schedule_bits_a_bits_tag; // @[Scheduler.scala 107:11]
  wire [9:0] c_mshr_io_schedule_bits_a_bits_set; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_schedule_bits_a_bits_param; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_bits_a_bits_block; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_bits_b_valid; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_schedule_bits_b_bits_param; // @[Scheduler.scala 107:11]
  wire [17:0] c_mshr_io_schedule_bits_b_bits_tag; // @[Scheduler.scala 107:11]
  wire [9:0] c_mshr_io_schedule_bits_b_bits_set; // @[Scheduler.scala 107:11]
  wire [3:0] c_mshr_io_schedule_bits_b_bits_clients; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_bits_c_valid; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_schedule_bits_c_bits_opcode; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_schedule_bits_c_bits_param; // @[Scheduler.scala 107:11]
  wire [17:0] c_mshr_io_schedule_bits_c_bits_tag; // @[Scheduler.scala 107:11]
  wire [9:0] c_mshr_io_schedule_bits_c_bits_set; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_schedule_bits_c_bits_way; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_bits_c_bits_dirty; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_bits_d_valid; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_bits_d_bits_prio_2; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_schedule_bits_d_bits_opcode; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_schedule_bits_d_bits_param; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_schedule_bits_d_bits_size; // @[Scheduler.scala 107:11]
  wire [6:0] c_mshr_io_schedule_bits_d_bits_source; // @[Scheduler.scala 107:11]
  wire [5:0] c_mshr_io_schedule_bits_d_bits_offset; // @[Scheduler.scala 107:11]
  wire [4:0] c_mshr_io_schedule_bits_d_bits_put; // @[Scheduler.scala 107:11]
  wire [9:0] c_mshr_io_schedule_bits_d_bits_set; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_schedule_bits_d_bits_way; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_bits_d_bits_bad; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_bits_d_bits_user_hit_cache; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_bits_e_valid; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_schedule_bits_e_bits_sink; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_bits_x_valid; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_bits_dir_valid; // @[Scheduler.scala 107:11]
  wire [9:0] c_mshr_io_schedule_bits_dir_bits_set; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_schedule_bits_dir_bits_way; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 107:11]
  wire [1:0] c_mshr_io_schedule_bits_dir_bits_data_state; // @[Scheduler.scala 107:11]
  wire [3:0] c_mshr_io_schedule_bits_dir_bits_data_clients; // @[Scheduler.scala 107:11]
  wire [17:0] c_mshr_io_schedule_bits_dir_bits_data_tag; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_schedule_bits_reload; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_sinkc_valid; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_sinkc_bits_last; // @[Scheduler.scala 107:11]
  wire [17:0] c_mshr_io_sinkc_bits_tag; // @[Scheduler.scala 107:11]
  wire [6:0] c_mshr_io_sinkc_bits_source; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_sinkc_bits_param; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_sinkc_bits_data; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_sinkd_valid; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_sinkd_bits_last; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_sinkd_bits_opcode; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_sinkd_bits_param; // @[Scheduler.scala 107:11]
  wire [2:0] c_mshr_io_sinkd_bits_sink; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_sinkd_bits_denied; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_sinke_valid; // @[Scheduler.scala 107:11]
  wire [9:0] c_mshr_io_nestedwb_set; // @[Scheduler.scala 107:11]
  wire [17:0] c_mshr_io_nestedwb_tag; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_nestedwb_b_toN; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_nestedwb_b_toB; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_nestedwb_b_clr_dirty; // @[Scheduler.scala 107:11]
  wire  c_mshr_io_nestedwb_c_set_dirty; // @[Scheduler.scala 107:11]
  wire  _mshr_stall_abc_T_3 = c_mshr_io_status_valid & abc_mshrs_0_io_status_bits_set == c_mshr_io_status_bits_set; // @[Scheduler.scala 127:30]
  wire  mshr_stall_0 = bc_mshr_io_status_valid & abc_mshrs_0_io_status_bits_set == bc_mshr_io_status_bits_set |
    _mshr_stall_abc_T_3; // @[Scheduler.scala 126:86]
  wire  _mshr_stall_abc_T_7 = c_mshr_io_status_valid & abc_mshrs_1_io_status_bits_set == c_mshr_io_status_bits_set; // @[Scheduler.scala 127:30]
  wire  mshr_stall_1 = bc_mshr_io_status_valid & abc_mshrs_1_io_status_bits_set == bc_mshr_io_status_bits_set |
    _mshr_stall_abc_T_7; // @[Scheduler.scala 126:86]
  wire  _mshr_stall_abc_T_11 = c_mshr_io_status_valid & abc_mshrs_2_io_status_bits_set == c_mshr_io_status_bits_set; // @[Scheduler.scala 127:30]
  wire  mshr_stall_2 = bc_mshr_io_status_valid & abc_mshrs_2_io_status_bits_set == bc_mshr_io_status_bits_set |
    _mshr_stall_abc_T_11; // @[Scheduler.scala 126:86]
  wire  _mshr_stall_abc_T_15 = c_mshr_io_status_valid & abc_mshrs_3_io_status_bits_set == c_mshr_io_status_bits_set; // @[Scheduler.scala 127:30]
  wire  mshr_stall_3 = bc_mshr_io_status_valid & abc_mshrs_3_io_status_bits_set == bc_mshr_io_status_bits_set |
    _mshr_stall_abc_T_15; // @[Scheduler.scala 126:86]
  wire  _mshr_stall_abc_T_19 = c_mshr_io_status_valid & abc_mshrs_4_io_status_bits_set == c_mshr_io_status_bits_set; // @[Scheduler.scala 127:30]
  wire  mshr_stall_4 = bc_mshr_io_status_valid & abc_mshrs_4_io_status_bits_set == bc_mshr_io_status_bits_set |
    _mshr_stall_abc_T_19; // @[Scheduler.scala 126:86]
  wire  _mshr_stall_abc_T_23 = c_mshr_io_status_valid & abc_mshrs_5_io_status_bits_set == c_mshr_io_status_bits_set; // @[Scheduler.scala 127:30]
  wire  mshr_stall_5 = bc_mshr_io_status_valid & abc_mshrs_5_io_status_bits_set == bc_mshr_io_status_bits_set |
    _mshr_stall_abc_T_23; // @[Scheduler.scala 126:86]
  wire  mshr_stall_6 = c_mshr_io_status_valid & bc_mshr_io_status_bits_set == c_mshr_io_status_bits_set; // @[Scheduler.scala 130:28]
  wire  _mshr_request_T_3 = sourceA_io_req_ready | ~abc_mshrs_0_io_schedule_bits_a_valid; // @[Scheduler.scala 144:29]
  wire  _mshr_request_T_4 = abc_mshrs_0_io_schedule_valid & ~mshr_stall_0 & _mshr_request_T_3; // @[Scheduler.scala 143:31]
  wire  _mshr_request_T_6 = sourceB_io_req_ready | ~abc_mshrs_0_io_schedule_bits_b_valid; // @[Scheduler.scala 145:29]
  wire  _mshr_request_T_7 = _mshr_request_T_4 & _mshr_request_T_6; // @[Scheduler.scala 144:61]
  wire  _mshr_request_T_9 = sourceC_io_req_ready | ~abc_mshrs_0_io_schedule_bits_c_valid; // @[Scheduler.scala 146:29]
  wire  _mshr_request_T_10 = _mshr_request_T_7 & _mshr_request_T_9; // @[Scheduler.scala 145:61]
  wire  _mshr_request_T_12 = sourceD_io_req_ready | ~abc_mshrs_0_io_schedule_bits_d_valid; // @[Scheduler.scala 147:29]
  wire  _mshr_request_T_13 = _mshr_request_T_10 & _mshr_request_T_12; // @[Scheduler.scala 146:61]
  wire  _mshr_request_T_15 = sourceE_io_req_ready | ~abc_mshrs_0_io_schedule_bits_e_valid; // @[Scheduler.scala 148:29]
  wire  _mshr_request_T_16 = _mshr_request_T_13 & _mshr_request_T_15; // @[Scheduler.scala 147:61]
  wire  _mshr_request_T_18 = sourceX_io_req_ready | ~abc_mshrs_0_io_schedule_bits_x_valid; // @[Scheduler.scala 149:29]
  wire  _mshr_request_T_19 = _mshr_request_T_16 & _mshr_request_T_18; // @[Scheduler.scala 148:61]
  wire  _mshr_request_T_21 = directory_io_write_ready | ~abc_mshrs_0_io_schedule_bits_dir_valid; // @[Scheduler.scala 150:33]
  wire  mshr_request_lo_lo_lo = _mshr_request_T_19 & _mshr_request_T_21; // @[Scheduler.scala 149:61]
  wire  _mshr_request_T_25 = sourceA_io_req_ready | ~abc_mshrs_1_io_schedule_bits_a_valid; // @[Scheduler.scala 144:29]
  wire  _mshr_request_T_26 = abc_mshrs_1_io_schedule_valid & ~mshr_stall_1 & _mshr_request_T_25; // @[Scheduler.scala 143:31]
  wire  _mshr_request_T_28 = sourceB_io_req_ready | ~abc_mshrs_1_io_schedule_bits_b_valid; // @[Scheduler.scala 145:29]
  wire  _mshr_request_T_29 = _mshr_request_T_26 & _mshr_request_T_28; // @[Scheduler.scala 144:61]
  wire  _mshr_request_T_31 = sourceC_io_req_ready | ~abc_mshrs_1_io_schedule_bits_c_valid; // @[Scheduler.scala 146:29]
  wire  _mshr_request_T_32 = _mshr_request_T_29 & _mshr_request_T_31; // @[Scheduler.scala 145:61]
  wire  _mshr_request_T_34 = sourceD_io_req_ready | ~abc_mshrs_1_io_schedule_bits_d_valid; // @[Scheduler.scala 147:29]
  wire  _mshr_request_T_35 = _mshr_request_T_32 & _mshr_request_T_34; // @[Scheduler.scala 146:61]
  wire  _mshr_request_T_37 = sourceE_io_req_ready | ~abc_mshrs_1_io_schedule_bits_e_valid; // @[Scheduler.scala 148:29]
  wire  _mshr_request_T_38 = _mshr_request_T_35 & _mshr_request_T_37; // @[Scheduler.scala 147:61]
  wire  _mshr_request_T_40 = sourceX_io_req_ready | ~abc_mshrs_1_io_schedule_bits_x_valid; // @[Scheduler.scala 149:29]
  wire  _mshr_request_T_41 = _mshr_request_T_38 & _mshr_request_T_40; // @[Scheduler.scala 148:61]
  wire  _mshr_request_T_43 = directory_io_write_ready | ~abc_mshrs_1_io_schedule_bits_dir_valid; // @[Scheduler.scala 150:33]
  wire  mshr_request_lo_lo_hi = _mshr_request_T_41 & _mshr_request_T_43; // @[Scheduler.scala 149:61]
  wire  _mshr_request_T_47 = sourceA_io_req_ready | ~abc_mshrs_2_io_schedule_bits_a_valid; // @[Scheduler.scala 144:29]
  wire  _mshr_request_T_48 = abc_mshrs_2_io_schedule_valid & ~mshr_stall_2 & _mshr_request_T_47; // @[Scheduler.scala 143:31]
  wire  _mshr_request_T_50 = sourceB_io_req_ready | ~abc_mshrs_2_io_schedule_bits_b_valid; // @[Scheduler.scala 145:29]
  wire  _mshr_request_T_51 = _mshr_request_T_48 & _mshr_request_T_50; // @[Scheduler.scala 144:61]
  wire  _mshr_request_T_53 = sourceC_io_req_ready | ~abc_mshrs_2_io_schedule_bits_c_valid; // @[Scheduler.scala 146:29]
  wire  _mshr_request_T_54 = _mshr_request_T_51 & _mshr_request_T_53; // @[Scheduler.scala 145:61]
  wire  _mshr_request_T_56 = sourceD_io_req_ready | ~abc_mshrs_2_io_schedule_bits_d_valid; // @[Scheduler.scala 147:29]
  wire  _mshr_request_T_57 = _mshr_request_T_54 & _mshr_request_T_56; // @[Scheduler.scala 146:61]
  wire  _mshr_request_T_59 = sourceE_io_req_ready | ~abc_mshrs_2_io_schedule_bits_e_valid; // @[Scheduler.scala 148:29]
  wire  _mshr_request_T_60 = _mshr_request_T_57 & _mshr_request_T_59; // @[Scheduler.scala 147:61]
  wire  _mshr_request_T_62 = sourceX_io_req_ready | ~abc_mshrs_2_io_schedule_bits_x_valid; // @[Scheduler.scala 149:29]
  wire  _mshr_request_T_63 = _mshr_request_T_60 & _mshr_request_T_62; // @[Scheduler.scala 148:61]
  wire  _mshr_request_T_65 = directory_io_write_ready | ~abc_mshrs_2_io_schedule_bits_dir_valid; // @[Scheduler.scala 150:33]
  wire  mshr_request_lo_hi_lo = _mshr_request_T_63 & _mshr_request_T_65; // @[Scheduler.scala 149:61]
  wire  _mshr_request_T_69 = sourceA_io_req_ready | ~abc_mshrs_3_io_schedule_bits_a_valid; // @[Scheduler.scala 144:29]
  wire  _mshr_request_T_70 = abc_mshrs_3_io_schedule_valid & ~mshr_stall_3 & _mshr_request_T_69; // @[Scheduler.scala 143:31]
  wire  _mshr_request_T_72 = sourceB_io_req_ready | ~abc_mshrs_3_io_schedule_bits_b_valid; // @[Scheduler.scala 145:29]
  wire  _mshr_request_T_73 = _mshr_request_T_70 & _mshr_request_T_72; // @[Scheduler.scala 144:61]
  wire  _mshr_request_T_75 = sourceC_io_req_ready | ~abc_mshrs_3_io_schedule_bits_c_valid; // @[Scheduler.scala 146:29]
  wire  _mshr_request_T_76 = _mshr_request_T_73 & _mshr_request_T_75; // @[Scheduler.scala 145:61]
  wire  _mshr_request_T_78 = sourceD_io_req_ready | ~abc_mshrs_3_io_schedule_bits_d_valid; // @[Scheduler.scala 147:29]
  wire  _mshr_request_T_79 = _mshr_request_T_76 & _mshr_request_T_78; // @[Scheduler.scala 146:61]
  wire  _mshr_request_T_81 = sourceE_io_req_ready | ~abc_mshrs_3_io_schedule_bits_e_valid; // @[Scheduler.scala 148:29]
  wire  _mshr_request_T_82 = _mshr_request_T_79 & _mshr_request_T_81; // @[Scheduler.scala 147:61]
  wire  _mshr_request_T_84 = sourceX_io_req_ready | ~abc_mshrs_3_io_schedule_bits_x_valid; // @[Scheduler.scala 149:29]
  wire  _mshr_request_T_85 = _mshr_request_T_82 & _mshr_request_T_84; // @[Scheduler.scala 148:61]
  wire  _mshr_request_T_87 = directory_io_write_ready | ~abc_mshrs_3_io_schedule_bits_dir_valid; // @[Scheduler.scala 150:33]
  wire  mshr_request_lo_hi_hi = _mshr_request_T_85 & _mshr_request_T_87; // @[Scheduler.scala 149:61]
  wire  _mshr_request_T_91 = sourceA_io_req_ready | ~abc_mshrs_4_io_schedule_bits_a_valid; // @[Scheduler.scala 144:29]
  wire  _mshr_request_T_92 = abc_mshrs_4_io_schedule_valid & ~mshr_stall_4 & _mshr_request_T_91; // @[Scheduler.scala 143:31]
  wire  _mshr_request_T_94 = sourceB_io_req_ready | ~abc_mshrs_4_io_schedule_bits_b_valid; // @[Scheduler.scala 145:29]
  wire  _mshr_request_T_95 = _mshr_request_T_92 & _mshr_request_T_94; // @[Scheduler.scala 144:61]
  wire  _mshr_request_T_97 = sourceC_io_req_ready | ~abc_mshrs_4_io_schedule_bits_c_valid; // @[Scheduler.scala 146:29]
  wire  _mshr_request_T_98 = _mshr_request_T_95 & _mshr_request_T_97; // @[Scheduler.scala 145:61]
  wire  _mshr_request_T_100 = sourceD_io_req_ready | ~abc_mshrs_4_io_schedule_bits_d_valid; // @[Scheduler.scala 147:29]
  wire  _mshr_request_T_101 = _mshr_request_T_98 & _mshr_request_T_100; // @[Scheduler.scala 146:61]
  wire  _mshr_request_T_103 = sourceE_io_req_ready | ~abc_mshrs_4_io_schedule_bits_e_valid; // @[Scheduler.scala 148:29]
  wire  _mshr_request_T_104 = _mshr_request_T_101 & _mshr_request_T_103; // @[Scheduler.scala 147:61]
  wire  _mshr_request_T_106 = sourceX_io_req_ready | ~abc_mshrs_4_io_schedule_bits_x_valid; // @[Scheduler.scala 149:29]
  wire  _mshr_request_T_107 = _mshr_request_T_104 & _mshr_request_T_106; // @[Scheduler.scala 148:61]
  wire  _mshr_request_T_109 = directory_io_write_ready | ~abc_mshrs_4_io_schedule_bits_dir_valid; // @[Scheduler.scala 150:33]
  wire  mshr_request_hi_lo_lo = _mshr_request_T_107 & _mshr_request_T_109; // @[Scheduler.scala 149:61]
  wire  _mshr_request_T_113 = sourceA_io_req_ready | ~abc_mshrs_5_io_schedule_bits_a_valid; // @[Scheduler.scala 144:29]
  wire  _mshr_request_T_114 = abc_mshrs_5_io_schedule_valid & ~mshr_stall_5 & _mshr_request_T_113; // @[Scheduler.scala 143:31]
  wire  _mshr_request_T_116 = sourceB_io_req_ready | ~abc_mshrs_5_io_schedule_bits_b_valid; // @[Scheduler.scala 145:29]
  wire  _mshr_request_T_117 = _mshr_request_T_114 & _mshr_request_T_116; // @[Scheduler.scala 144:61]
  wire  _mshr_request_T_119 = sourceC_io_req_ready | ~abc_mshrs_5_io_schedule_bits_c_valid; // @[Scheduler.scala 146:29]
  wire  _mshr_request_T_120 = _mshr_request_T_117 & _mshr_request_T_119; // @[Scheduler.scala 145:61]
  wire  _mshr_request_T_122 = sourceD_io_req_ready | ~abc_mshrs_5_io_schedule_bits_d_valid; // @[Scheduler.scala 147:29]
  wire  _mshr_request_T_123 = _mshr_request_T_120 & _mshr_request_T_122; // @[Scheduler.scala 146:61]
  wire  _mshr_request_T_125 = sourceE_io_req_ready | ~abc_mshrs_5_io_schedule_bits_e_valid; // @[Scheduler.scala 148:29]
  wire  _mshr_request_T_126 = _mshr_request_T_123 & _mshr_request_T_125; // @[Scheduler.scala 147:61]
  wire  _mshr_request_T_128 = sourceX_io_req_ready | ~abc_mshrs_5_io_schedule_bits_x_valid; // @[Scheduler.scala 149:29]
  wire  _mshr_request_T_129 = _mshr_request_T_126 & _mshr_request_T_128; // @[Scheduler.scala 148:61]
  wire  _mshr_request_T_131 = directory_io_write_ready | ~abc_mshrs_5_io_schedule_bits_dir_valid; // @[Scheduler.scala 150:33]
  wire  mshr_request_hi_lo_hi = _mshr_request_T_129 & _mshr_request_T_131; // @[Scheduler.scala 149:61]
  wire  _mshr_request_T_135 = sourceA_io_req_ready | ~bc_mshr_io_schedule_bits_a_valid; // @[Scheduler.scala 144:29]
  wire  _mshr_request_T_136 = bc_mshr_io_schedule_valid & ~mshr_stall_6 & _mshr_request_T_135; // @[Scheduler.scala 143:31]
  wire  _mshr_request_T_138 = sourceB_io_req_ready | ~bc_mshr_io_schedule_bits_b_valid; // @[Scheduler.scala 145:29]
  wire  _mshr_request_T_139 = _mshr_request_T_136 & _mshr_request_T_138; // @[Scheduler.scala 144:61]
  wire  _mshr_request_T_141 = sourceC_io_req_ready | ~bc_mshr_io_schedule_bits_c_valid; // @[Scheduler.scala 146:29]
  wire  _mshr_request_T_142 = _mshr_request_T_139 & _mshr_request_T_141; // @[Scheduler.scala 145:61]
  wire  _mshr_request_T_144 = sourceD_io_req_ready | ~bc_mshr_io_schedule_bits_d_valid; // @[Scheduler.scala 147:29]
  wire  _mshr_request_T_145 = _mshr_request_T_142 & _mshr_request_T_144; // @[Scheduler.scala 146:61]
  wire  _mshr_request_T_147 = sourceE_io_req_ready | ~bc_mshr_io_schedule_bits_e_valid; // @[Scheduler.scala 148:29]
  wire  _mshr_request_T_148 = _mshr_request_T_145 & _mshr_request_T_147; // @[Scheduler.scala 147:61]
  wire  _mshr_request_T_150 = sourceX_io_req_ready | ~bc_mshr_io_schedule_bits_x_valid; // @[Scheduler.scala 149:29]
  wire  _mshr_request_T_151 = _mshr_request_T_148 & _mshr_request_T_150; // @[Scheduler.scala 148:61]
  wire  _mshr_request_T_153 = directory_io_write_ready | ~bc_mshr_io_schedule_bits_dir_valid; // @[Scheduler.scala 150:33]
  wire  mshr_request_hi_hi_lo = _mshr_request_T_151 & _mshr_request_T_153; // @[Scheduler.scala 149:61]
  wire  _mshr_request_T_157 = sourceA_io_req_ready | ~c_mshr_io_schedule_bits_a_valid; // @[Scheduler.scala 144:29]
  wire  _mshr_request_T_158 = c_mshr_io_schedule_valid & _mshr_request_T_157; // @[Scheduler.scala 143:31]
  wire  _mshr_request_T_160 = sourceB_io_req_ready | ~c_mshr_io_schedule_bits_b_valid; // @[Scheduler.scala 145:29]
  wire  _mshr_request_T_161 = _mshr_request_T_158 & _mshr_request_T_160; // @[Scheduler.scala 144:61]
  wire  _mshr_request_T_163 = sourceC_io_req_ready | ~c_mshr_io_schedule_bits_c_valid; // @[Scheduler.scala 146:29]
  wire  _mshr_request_T_164 = _mshr_request_T_161 & _mshr_request_T_163; // @[Scheduler.scala 145:61]
  wire  _mshr_request_T_166 = sourceD_io_req_ready | ~c_mshr_io_schedule_bits_d_valid; // @[Scheduler.scala 147:29]
  wire  _mshr_request_T_167 = _mshr_request_T_164 & _mshr_request_T_166; // @[Scheduler.scala 146:61]
  wire  _mshr_request_T_169 = sourceE_io_req_ready | ~c_mshr_io_schedule_bits_e_valid; // @[Scheduler.scala 148:29]
  wire  _mshr_request_T_170 = _mshr_request_T_167 & _mshr_request_T_169; // @[Scheduler.scala 147:61]
  wire  _mshr_request_T_172 = sourceX_io_req_ready | ~c_mshr_io_schedule_bits_x_valid; // @[Scheduler.scala 149:29]
  wire  _mshr_request_T_173 = _mshr_request_T_170 & _mshr_request_T_172; // @[Scheduler.scala 148:61]
  wire  _mshr_request_T_175 = directory_io_write_ready | ~c_mshr_io_schedule_bits_dir_valid; // @[Scheduler.scala 150:33]
  wire  mshr_request_hi_hi_hi = _mshr_request_T_173 & _mshr_request_T_175; // @[Scheduler.scala 149:61]
  wire [7:0] mshr_request = {mshr_request_hi_hi_hi,mshr_request_hi_hi_lo,mshr_request_hi_lo_hi,mshr_request_hi_lo_lo,
    mshr_request_lo_hi_hi,mshr_request_lo_hi_lo,mshr_request_lo_lo_hi,mshr_request_lo_lo_lo}; // @[Cat.scala 30:58]
  reg [7:0] robin_filter; // @[Scheduler.scala 154:29]
  wire [7:0] robin_request_lo = mshr_request & robin_filter; // @[Scheduler.scala 155:54]
  wire [15:0] robin_request = {mshr_request_hi_hi_hi,mshr_request_hi_hi_lo,mshr_request_hi_lo_hi,mshr_request_hi_lo_lo,
    mshr_request_lo_hi_hi,mshr_request_lo_hi_lo,mshr_request_lo_lo_hi,mshr_request_lo_lo_lo,robin_request_lo}; // @[Cat.scala 30:58]
  wire [16:0] _mshr_selectOH2_T = {robin_request, 1'h0}; // @[package.scala 244:48]
  wire [15:0] _mshr_selectOH2_T_2 = robin_request | _mshr_selectOH2_T[15:0]; // @[package.scala 244:43]
  wire [17:0] _mshr_selectOH2_T_3 = {_mshr_selectOH2_T_2, 2'h0}; // @[package.scala 244:48]
  wire [15:0] _mshr_selectOH2_T_5 = _mshr_selectOH2_T_2 | _mshr_selectOH2_T_3[15:0]; // @[package.scala 244:43]
  wire [19:0] _mshr_selectOH2_T_6 = {_mshr_selectOH2_T_5, 4'h0}; // @[package.scala 244:48]
  wire [15:0] _mshr_selectOH2_T_8 = _mshr_selectOH2_T_5 | _mshr_selectOH2_T_6[15:0]; // @[package.scala 244:43]
  wire [23:0] _mshr_selectOH2_T_9 = {_mshr_selectOH2_T_8, 8'h0}; // @[package.scala 244:48]
  wire [15:0] _mshr_selectOH2_T_11 = _mshr_selectOH2_T_8 | _mshr_selectOH2_T_9[15:0]; // @[package.scala 244:43]
  wire [16:0] _mshr_selectOH2_T_13 = {_mshr_selectOH2_T_11, 1'h0}; // @[Scheduler.scala 156:48]
  wire [16:0] _mshr_selectOH2_T_14 = ~_mshr_selectOH2_T_13; // @[Scheduler.scala 156:24]
  wire [16:0] _GEN_86 = {{1'd0}, robin_request}; // @[Scheduler.scala 156:54]
  wire [16:0] mshr_selectOH2 = _mshr_selectOH2_T_14 & _GEN_86; // @[Scheduler.scala 156:54]
  wire [7:0] mshr_selectOH = mshr_selectOH2[15:8] | mshr_selectOH2[7:0]; // @[Scheduler.scala 157:70]
  wire [3:0] mshr_select_hi = mshr_selectOH[7:4]; // @[OneHot.scala 30:18]
  wire [3:0] mshr_select_lo = mshr_selectOH[3:0]; // @[OneHot.scala 31:18]
  wire  mshr_select_hi_1 = |mshr_select_hi; // @[OneHot.scala 32:14]
  wire [3:0] _mshr_select_T = mshr_select_hi | mshr_select_lo; // @[OneHot.scala 32:28]
  wire [1:0] mshr_select_hi_2 = _mshr_select_T[3:2]; // @[OneHot.scala 30:18]
  wire [1:0] mshr_select_lo_1 = _mshr_select_T[1:0]; // @[OneHot.scala 31:18]
  wire  mshr_select_hi_3 = |mshr_select_hi_2; // @[OneHot.scala 32:14]
  wire [1:0] _mshr_select_T_1 = mshr_select_hi_2 | mshr_select_lo_1; // @[OneHot.scala 32:28]
  wire  mshr_select_lo_2 = _mshr_select_T_1[1]; // @[CircuitMath.scala 30:8]
  wire [1:0] mshr_select_lo_3 = {mshr_select_hi_3,mshr_select_lo_2}; // @[Cat.scala 30:58]
  wire [2:0] mshr_select = {mshr_select_hi_1,mshr_select_hi_3,mshr_select_lo_2}; // @[Cat.scala 30:58]
  wire  schedule_reload = mshr_selectOH[0] & abc_mshrs_0_io_schedule_bits_reload | mshr_selectOH[1] &
    abc_mshrs_1_io_schedule_bits_reload | mshr_selectOH[2] & abc_mshrs_2_io_schedule_bits_reload | mshr_selectOH[3] &
    abc_mshrs_3_io_schedule_bits_reload | mshr_selectOH[4] & abc_mshrs_4_io_schedule_bits_reload | mshr_selectOH[5] &
    abc_mshrs_5_io_schedule_bits_reload | mshr_selectOH[6] & bc_mshr_io_schedule_bits_reload | mshr_selectOH[7] &
    c_mshr_io_schedule_bits_reload; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_23 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_dir_bits_data_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_24 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_dir_bits_data_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_25 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_dir_bits_data_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_26 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_dir_bits_data_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_27 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_dir_bits_data_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_28 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_dir_bits_data_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_29 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_dir_bits_data_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_30 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_dir_bits_data_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_31 = _schedule_T_23 | _schedule_T_24; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_32 = _schedule_T_31 | _schedule_T_25; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_33 = _schedule_T_32 | _schedule_T_26; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_34 = _schedule_T_33 | _schedule_T_27; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_35 = _schedule_T_34 | _schedule_T_28; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_36 = _schedule_T_35 | _schedule_T_29; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_38 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_dir_bits_data_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_39 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_dir_bits_data_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_40 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_dir_bits_data_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_41 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_dir_bits_data_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_42 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_dir_bits_data_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_43 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_dir_bits_data_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_44 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_dir_bits_data_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_45 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_dir_bits_data_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_46 = _schedule_T_38 | _schedule_T_39; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_47 = _schedule_T_46 | _schedule_T_40; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_48 = _schedule_T_47 | _schedule_T_41; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_49 = _schedule_T_48 | _schedule_T_42; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_50 = _schedule_T_49 | _schedule_T_43; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_51 = _schedule_T_50 | _schedule_T_44; // @[Mux.scala 27:72]
  wire [1:0] _schedule_T_53 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_dir_bits_data_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _schedule_T_54 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_dir_bits_data_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _schedule_T_55 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_dir_bits_data_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _schedule_T_56 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_dir_bits_data_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _schedule_T_57 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_dir_bits_data_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _schedule_T_58 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_dir_bits_data_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _schedule_T_59 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_dir_bits_data_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _schedule_T_60 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_dir_bits_data_state : 2'h0; // @[Mux.scala 27:72]
  wire [1:0] _schedule_T_61 = _schedule_T_53 | _schedule_T_54; // @[Mux.scala 27:72]
  wire [1:0] _schedule_T_62 = _schedule_T_61 | _schedule_T_55; // @[Mux.scala 27:72]
  wire [1:0] _schedule_T_63 = _schedule_T_62 | _schedule_T_56; // @[Mux.scala 27:72]
  wire [1:0] _schedule_T_64 = _schedule_T_63 | _schedule_T_57; // @[Mux.scala 27:72]
  wire [1:0] _schedule_T_65 = _schedule_T_64 | _schedule_T_58; // @[Mux.scala 27:72]
  wire [1:0] _schedule_T_66 = _schedule_T_65 | _schedule_T_59; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_83 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_dir_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_84 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_dir_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_85 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_dir_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_86 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_dir_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_87 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_dir_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_88 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_dir_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_89 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_dir_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_90 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_dir_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_91 = _schedule_T_83 | _schedule_T_84; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_92 = _schedule_T_91 | _schedule_T_85; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_93 = _schedule_T_92 | _schedule_T_86; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_94 = _schedule_T_93 | _schedule_T_87; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_95 = _schedule_T_94 | _schedule_T_88; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_96 = _schedule_T_95 | _schedule_T_89; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_98 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_dir_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_99 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_dir_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_100 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_dir_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_101 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_dir_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_102 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_dir_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_103 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_dir_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_104 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_dir_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_105 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_dir_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_106 = _schedule_T_98 | _schedule_T_99; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_107 = _schedule_T_106 | _schedule_T_100; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_108 = _schedule_T_107 | _schedule_T_101; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_109 = _schedule_T_108 | _schedule_T_102; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_110 = _schedule_T_109 | _schedule_T_103; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_111 = _schedule_T_110 | _schedule_T_104; // @[Mux.scala 27:72]
  wire  _schedule_T_119 = mshr_selectOH[6] & bc_mshr_io_schedule_bits_dir_valid; // @[Mux.scala 27:72]
  wire  _schedule_T_120 = mshr_selectOH[7] & c_mshr_io_schedule_bits_dir_valid; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_158 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_e_bits_sink : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_159 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_e_bits_sink : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_160 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_e_bits_sink : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_161 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_e_bits_sink : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_162 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_e_bits_sink : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_163 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_e_bits_sink : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_164 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_e_bits_sink : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_165 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_e_bits_sink : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_166 = _schedule_T_158 | _schedule_T_159; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_167 = _schedule_T_166 | _schedule_T_160; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_168 = _schedule_T_167 | _schedule_T_161; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_169 = _schedule_T_168 | _schedule_T_162; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_170 = _schedule_T_169 | _schedule_T_163; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_171 = _schedule_T_170 | _schedule_T_164; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_248 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_d_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_249 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_d_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_250 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_d_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_251 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_d_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_252 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_d_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_253 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_d_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_254 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_d_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_255 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_d_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_256 = _schedule_T_248 | _schedule_T_249; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_257 = _schedule_T_256 | _schedule_T_250; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_258 = _schedule_T_257 | _schedule_T_251; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_259 = _schedule_T_258 | _schedule_T_252; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_260 = _schedule_T_259 | _schedule_T_253; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_261 = _schedule_T_260 | _schedule_T_254; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_278 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_d_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_279 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_d_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_280 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_d_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_281 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_d_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_282 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_d_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_283 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_d_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_284 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_d_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_285 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_d_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_286 = _schedule_T_278 | _schedule_T_279; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_287 = _schedule_T_286 | _schedule_T_280; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_288 = _schedule_T_287 | _schedule_T_281; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_289 = _schedule_T_288 | _schedule_T_282; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_290 = _schedule_T_289 | _schedule_T_283; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_291 = _schedule_T_290 | _schedule_T_284; // @[Mux.scala 27:72]
  wire [4:0] _schedule_T_293 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_d_bits_put : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _schedule_T_294 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_d_bits_put : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _schedule_T_295 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_d_bits_put : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _schedule_T_296 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_d_bits_put : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _schedule_T_297 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_d_bits_put : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _schedule_T_298 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_d_bits_put : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _schedule_T_299 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_d_bits_put : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _schedule_T_300 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_d_bits_put : 5'h0; // @[Mux.scala 27:72]
  wire [4:0] _schedule_T_301 = _schedule_T_293 | _schedule_T_294; // @[Mux.scala 27:72]
  wire [4:0] _schedule_T_302 = _schedule_T_301 | _schedule_T_295; // @[Mux.scala 27:72]
  wire [4:0] _schedule_T_303 = _schedule_T_302 | _schedule_T_296; // @[Mux.scala 27:72]
  wire [4:0] _schedule_T_304 = _schedule_T_303 | _schedule_T_297; // @[Mux.scala 27:72]
  wire [4:0] _schedule_T_305 = _schedule_T_304 | _schedule_T_298; // @[Mux.scala 27:72]
  wire [4:0] _schedule_T_306 = _schedule_T_305 | _schedule_T_299; // @[Mux.scala 27:72]
  wire [5:0] _schedule_T_308 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_d_bits_offset : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _schedule_T_309 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_d_bits_offset : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _schedule_T_310 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_d_bits_offset : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _schedule_T_311 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_d_bits_offset : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _schedule_T_312 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_d_bits_offset : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _schedule_T_313 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_d_bits_offset : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _schedule_T_314 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_d_bits_offset : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _schedule_T_315 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_d_bits_offset : 6'h0; // @[Mux.scala 27:72]
  wire [5:0] _schedule_T_316 = _schedule_T_308 | _schedule_T_309; // @[Mux.scala 27:72]
  wire [5:0] _schedule_T_317 = _schedule_T_316 | _schedule_T_310; // @[Mux.scala 27:72]
  wire [5:0] _schedule_T_318 = _schedule_T_317 | _schedule_T_311; // @[Mux.scala 27:72]
  wire [5:0] _schedule_T_319 = _schedule_T_318 | _schedule_T_312; // @[Mux.scala 27:72]
  wire [5:0] _schedule_T_320 = _schedule_T_319 | _schedule_T_313; // @[Mux.scala 27:72]
  wire [5:0] _schedule_T_321 = _schedule_T_320 | _schedule_T_314; // @[Mux.scala 27:72]
  wire [6:0] _schedule_T_338 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _schedule_T_339 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _schedule_T_340 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _schedule_T_341 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _schedule_T_342 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _schedule_T_343 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _schedule_T_344 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _schedule_T_345 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_d_bits_source : 7'h0; // @[Mux.scala 27:72]
  wire [6:0] _schedule_T_346 = _schedule_T_338 | _schedule_T_339; // @[Mux.scala 27:72]
  wire [6:0] _schedule_T_347 = _schedule_T_346 | _schedule_T_340; // @[Mux.scala 27:72]
  wire [6:0] _schedule_T_348 = _schedule_T_347 | _schedule_T_341; // @[Mux.scala 27:72]
  wire [6:0] _schedule_T_349 = _schedule_T_348 | _schedule_T_342; // @[Mux.scala 27:72]
  wire [6:0] _schedule_T_350 = _schedule_T_349 | _schedule_T_343; // @[Mux.scala 27:72]
  wire [6:0] _schedule_T_351 = _schedule_T_350 | _schedule_T_344; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_353 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_354 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_355 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_356 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_357 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_358 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_359 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_360 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_d_bits_size : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_361 = _schedule_T_353 | _schedule_T_354; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_362 = _schedule_T_361 | _schedule_T_355; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_363 = _schedule_T_362 | _schedule_T_356; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_364 = _schedule_T_363 | _schedule_T_357; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_365 = _schedule_T_364 | _schedule_T_358; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_366 = _schedule_T_365 | _schedule_T_359; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_368 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_d_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_369 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_d_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_370 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_d_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_371 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_d_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_372 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_d_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_373 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_d_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_374 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_d_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_375 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_d_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_376 = _schedule_T_368 | _schedule_T_369; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_377 = _schedule_T_376 | _schedule_T_370; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_378 = _schedule_T_377 | _schedule_T_371; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_379 = _schedule_T_378 | _schedule_T_372; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_380 = _schedule_T_379 | _schedule_T_373; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_381 = _schedule_T_380 | _schedule_T_374; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_383 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_384 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_385 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_386 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_387 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_388 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_389 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_390 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_d_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_391 = _schedule_T_383 | _schedule_T_384; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_392 = _schedule_T_391 | _schedule_T_385; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_393 = _schedule_T_392 | _schedule_T_386; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_394 = _schedule_T_393 | _schedule_T_387; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_395 = _schedule_T_394 | _schedule_T_388; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_396 = _schedule_T_395 | _schedule_T_389; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_488 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_c_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_489 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_c_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_490 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_c_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_491 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_c_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_492 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_c_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_493 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_c_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_494 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_c_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_495 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_c_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_496 = _schedule_T_488 | _schedule_T_489; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_497 = _schedule_T_496 | _schedule_T_490; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_498 = _schedule_T_497 | _schedule_T_491; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_499 = _schedule_T_498 | _schedule_T_492; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_500 = _schedule_T_499 | _schedule_T_493; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_501 = _schedule_T_500 | _schedule_T_494; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_503 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_c_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_504 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_c_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_505 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_c_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_506 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_c_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_507 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_c_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_508 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_c_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_509 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_c_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_510 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_c_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_511 = _schedule_T_503 | _schedule_T_504; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_512 = _schedule_T_511 | _schedule_T_505; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_513 = _schedule_T_512 | _schedule_T_506; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_514 = _schedule_T_513 | _schedule_T_507; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_515 = _schedule_T_514 | _schedule_T_508; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_516 = _schedule_T_515 | _schedule_T_509; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_518 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_c_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_519 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_c_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_520 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_c_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_521 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_c_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_522 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_c_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_523 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_c_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_524 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_c_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_525 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_c_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_526 = _schedule_T_518 | _schedule_T_519; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_527 = _schedule_T_526 | _schedule_T_520; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_528 = _schedule_T_527 | _schedule_T_521; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_529 = _schedule_T_528 | _schedule_T_522; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_530 = _schedule_T_529 | _schedule_T_523; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_531 = _schedule_T_530 | _schedule_T_524; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_548 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_549 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_550 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_551 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_552 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_553 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_554 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_555 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_c_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_556 = _schedule_T_548 | _schedule_T_549; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_557 = _schedule_T_556 | _schedule_T_550; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_558 = _schedule_T_557 | _schedule_T_551; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_559 = _schedule_T_558 | _schedule_T_552; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_560 = _schedule_T_559 | _schedule_T_553; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_561 = _schedule_T_560 | _schedule_T_554; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_563 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_564 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_565 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_566 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_567 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_568 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_569 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_570 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_c_bits_opcode : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_571 = _schedule_T_563 | _schedule_T_564; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_572 = _schedule_T_571 | _schedule_T_565; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_573 = _schedule_T_572 | _schedule_T_566; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_574 = _schedule_T_573 | _schedule_T_567; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_575 = _schedule_T_574 | _schedule_T_568; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_576 = _schedule_T_575 | _schedule_T_569; // @[Mux.scala 27:72]
  wire [2:0] schedule_c_bits_opcode = _schedule_T_576 | _schedule_T_570; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_593 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_b_bits_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_594 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_b_bits_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_595 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_b_bits_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_596 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_b_bits_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_597 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_b_bits_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_598 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_b_bits_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_599 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_b_bits_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_600 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_b_bits_clients : 4'h0; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_601 = _schedule_T_593 | _schedule_T_594; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_602 = _schedule_T_601 | _schedule_T_595; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_603 = _schedule_T_602 | _schedule_T_596; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_604 = _schedule_T_603 | _schedule_T_597; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_605 = _schedule_T_604 | _schedule_T_598; // @[Mux.scala 27:72]
  wire [3:0] _schedule_T_606 = _schedule_T_605 | _schedule_T_599; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_608 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_b_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_609 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_b_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_610 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_b_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_611 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_b_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_612 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_b_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_613 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_b_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_614 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_b_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_615 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_b_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_616 = _schedule_T_608 | _schedule_T_609; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_617 = _schedule_T_616 | _schedule_T_610; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_618 = _schedule_T_617 | _schedule_T_611; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_619 = _schedule_T_618 | _schedule_T_612; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_620 = _schedule_T_619 | _schedule_T_613; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_621 = _schedule_T_620 | _schedule_T_614; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_623 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_b_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_624 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_b_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_625 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_b_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_626 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_b_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_627 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_b_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_628 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_b_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_629 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_b_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_630 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_b_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_631 = _schedule_T_623 | _schedule_T_624; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_632 = _schedule_T_631 | _schedule_T_625; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_633 = _schedule_T_632 | _schedule_T_626; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_634 = _schedule_T_633 | _schedule_T_627; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_635 = _schedule_T_634 | _schedule_T_628; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_636 = _schedule_T_635 | _schedule_T_629; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_638 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_b_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_639 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_b_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_640 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_b_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_641 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_b_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_642 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_b_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_643 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_b_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_644 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_b_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_645 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_b_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_646 = _schedule_T_638 | _schedule_T_639; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_647 = _schedule_T_646 | _schedule_T_640; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_648 = _schedule_T_647 | _schedule_T_641; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_649 = _schedule_T_648 | _schedule_T_642; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_650 = _schedule_T_649 | _schedule_T_643; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_651 = _schedule_T_650 | _schedule_T_644; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_698 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_699 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_700 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_701 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_702 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_703 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_704 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_705 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_a_bits_param : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_706 = _schedule_T_698 | _schedule_T_699; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_707 = _schedule_T_706 | _schedule_T_700; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_708 = _schedule_T_707 | _schedule_T_701; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_709 = _schedule_T_708 | _schedule_T_702; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_710 = _schedule_T_709 | _schedule_T_703; // @[Mux.scala 27:72]
  wire [2:0] _schedule_T_711 = _schedule_T_710 | _schedule_T_704; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_713 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_a_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_714 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_a_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_715 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_a_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_716 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_a_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_717 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_a_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_718 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_a_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_719 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_a_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_720 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_a_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_721 = _schedule_T_713 | _schedule_T_714; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_722 = _schedule_T_721 | _schedule_T_715; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_723 = _schedule_T_722 | _schedule_T_716; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_724 = _schedule_T_723 | _schedule_T_717; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_725 = _schedule_T_724 | _schedule_T_718; // @[Mux.scala 27:72]
  wire [9:0] _schedule_T_726 = _schedule_T_725 | _schedule_T_719; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_728 = mshr_selectOH[0] ? abc_mshrs_0_io_schedule_bits_a_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_729 = mshr_selectOH[1] ? abc_mshrs_1_io_schedule_bits_a_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_730 = mshr_selectOH[2] ? abc_mshrs_2_io_schedule_bits_a_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_731 = mshr_selectOH[3] ? abc_mshrs_3_io_schedule_bits_a_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_732 = mshr_selectOH[4] ? abc_mshrs_4_io_schedule_bits_a_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_733 = mshr_selectOH[5] ? abc_mshrs_5_io_schedule_bits_a_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_734 = mshr_selectOH[6] ? bc_mshr_io_schedule_bits_a_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_735 = mshr_selectOH[7] ? c_mshr_io_schedule_bits_a_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_736 = _schedule_T_728 | _schedule_T_729; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_737 = _schedule_T_736 | _schedule_T_730; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_738 = _schedule_T_737 | _schedule_T_731; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_739 = _schedule_T_738 | _schedule_T_732; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_740 = _schedule_T_739 | _schedule_T_733; // @[Mux.scala 27:72]
  wire [17:0] _schedule_T_741 = _schedule_T_740 | _schedule_T_734; // @[Mux.scala 27:72]
  wire [17:0] _scheduleTag_T_8 = mshr_selectOH[0] ? abc_mshrs_0_io_status_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _scheduleTag_T_9 = mshr_selectOH[1] ? abc_mshrs_1_io_status_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _scheduleTag_T_10 = mshr_selectOH[2] ? abc_mshrs_2_io_status_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _scheduleTag_T_11 = mshr_selectOH[3] ? abc_mshrs_3_io_status_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _scheduleTag_T_12 = mshr_selectOH[4] ? abc_mshrs_4_io_status_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _scheduleTag_T_13 = mshr_selectOH[5] ? abc_mshrs_5_io_status_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _scheduleTag_T_14 = mshr_selectOH[6] ? bc_mshr_io_status_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _scheduleTag_T_15 = mshr_selectOH[7] ? c_mshr_io_status_bits_tag : 18'h0; // @[Mux.scala 27:72]
  wire [17:0] _scheduleTag_T_16 = _scheduleTag_T_8 | _scheduleTag_T_9; // @[Mux.scala 27:72]
  wire [17:0] _scheduleTag_T_17 = _scheduleTag_T_16 | _scheduleTag_T_10; // @[Mux.scala 27:72]
  wire [17:0] _scheduleTag_T_18 = _scheduleTag_T_17 | _scheduleTag_T_11; // @[Mux.scala 27:72]
  wire [17:0] _scheduleTag_T_19 = _scheduleTag_T_18 | _scheduleTag_T_12; // @[Mux.scala 27:72]
  wire [17:0] _scheduleTag_T_20 = _scheduleTag_T_19 | _scheduleTag_T_13; // @[Mux.scala 27:72]
  wire [17:0] _scheduleTag_T_21 = _scheduleTag_T_20 | _scheduleTag_T_14; // @[Mux.scala 27:72]
  wire [17:0] scheduleTag = _scheduleTag_T_21 | _scheduleTag_T_15; // @[Mux.scala 27:72]
  wire [9:0] _scheduleSet_T_8 = mshr_selectOH[0] ? abc_mshrs_0_io_status_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _scheduleSet_T_9 = mshr_selectOH[1] ? abc_mshrs_1_io_status_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _scheduleSet_T_10 = mshr_selectOH[2] ? abc_mshrs_2_io_status_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _scheduleSet_T_11 = mshr_selectOH[3] ? abc_mshrs_3_io_status_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _scheduleSet_T_12 = mshr_selectOH[4] ? abc_mshrs_4_io_status_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _scheduleSet_T_13 = mshr_selectOH[5] ? abc_mshrs_5_io_status_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _scheduleSet_T_14 = mshr_selectOH[6] ? bc_mshr_io_status_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _scheduleSet_T_15 = mshr_selectOH[7] ? c_mshr_io_status_bits_set : 10'h0; // @[Mux.scala 27:72]
  wire [9:0] _scheduleSet_T_16 = _scheduleSet_T_8 | _scheduleSet_T_9; // @[Mux.scala 27:72]
  wire [9:0] _scheduleSet_T_17 = _scheduleSet_T_16 | _scheduleSet_T_10; // @[Mux.scala 27:72]
  wire [9:0] _scheduleSet_T_18 = _scheduleSet_T_17 | _scheduleSet_T_11; // @[Mux.scala 27:72]
  wire [9:0] _scheduleSet_T_19 = _scheduleSet_T_18 | _scheduleSet_T_12; // @[Mux.scala 27:72]
  wire [9:0] _scheduleSet_T_20 = _scheduleSet_T_19 | _scheduleSet_T_13; // @[Mux.scala 27:72]
  wire [9:0] _scheduleSet_T_21 = _scheduleSet_T_20 | _scheduleSet_T_14; // @[Mux.scala 27:72]
  wire [9:0] scheduleSet = _scheduleSet_T_21 | _scheduleSet_T_15; // @[Mux.scala 27:72]
  wire [7:0] _GEN_100 = {{1'd0}, mshr_selectOH[7:1]}; // @[package.scala 253:43]
  wire [7:0] _robin_filter_T_1 = mshr_selectOH | _GEN_100; // @[package.scala 253:43]
  wire [7:0] _GEN_101 = {{2'd0}, _robin_filter_T_1[7:2]}; // @[package.scala 253:43]
  wire [7:0] _robin_filter_T_3 = _robin_filter_T_1 | _GEN_101; // @[package.scala 253:43]
  wire [7:0] _GEN_114 = {{4'd0}, _robin_filter_T_3[7:4]}; // @[package.scala 253:43]
  wire [7:0] _robin_filter_T_5 = _robin_filter_T_3 | _GEN_114; // @[package.scala 253:43]
  wire [7:0] _robin_filter_T_7 = ~_robin_filter_T_5; // @[Scheduler.scala 164:47]
  wire  request_valid = directory_io_ready & (sinkA_io_req_valid | sinkX_io_req_valid | sinkC_io_req_valid); // @[Scheduler.scala 192:39]
  wire  _request_bits_T_control = sinkX_io_req_valid; // @[Scheduler.scala 195:22]
  wire [2:0] _request_bits_T_opcode = sinkX_io_req_valid ? 3'h0 : sinkA_io_req_bits_opcode; // @[Scheduler.scala 195:22]
  wire [2:0] _request_bits_T_param = sinkX_io_req_valid ? 3'h0 : sinkA_io_req_bits_param; // @[Scheduler.scala 195:22]
  wire [2:0] _request_bits_T_size = sinkX_io_req_valid ? 3'h6 : sinkA_io_req_bits_size; // @[Scheduler.scala 195:22]
  wire [6:0] _request_bits_T_source = sinkX_io_req_valid ? 7'h0 : sinkA_io_req_bits_source; // @[Scheduler.scala 195:22]
  wire [17:0] _request_bits_T_tag = sinkX_io_req_valid ? sinkX_io_req_bits_tag : sinkA_io_req_bits_tag; // @[Scheduler.scala 195:22]
  wire [5:0] _request_bits_T_offset = sinkX_io_req_valid ? 6'h0 : sinkA_io_req_bits_offset; // @[Scheduler.scala 195:22]
  wire [4:0] _request_bits_T_put = sinkX_io_req_valid ? 5'h0 : sinkA_io_req_bits_put; // @[Scheduler.scala 195:22]
  wire [9:0] _request_bits_T_set = sinkX_io_req_valid ? sinkX_io_req_bits_set : sinkA_io_req_bits_set; // @[Scheduler.scala 195:22]
  wire  request_bits_prio_0 = sinkC_io_req_valid ? 1'h0 : 1'h1; // @[Scheduler.scala 193:22]
  wire  request_bits_prio_2 = sinkC_io_req_valid; // @[Scheduler.scala 193:22]
  wire  request_bits_control = sinkC_io_req_valid ? 1'h0 : _request_bits_T_control; // @[Scheduler.scala 193:22]
  wire [2:0] request_bits_opcode = sinkC_io_req_valid ? sinkC_io_req_bits_opcode : _request_bits_T_opcode; // @[Scheduler.scala 193:22]
  wire [2:0] request_bits_param = sinkC_io_req_valid ? sinkC_io_req_bits_param : _request_bits_T_param; // @[Scheduler.scala 193:22]
  wire [2:0] request_bits_size = sinkC_io_req_valid ? sinkC_io_req_bits_size : _request_bits_T_size; // @[Scheduler.scala 193:22]
  wire [6:0] request_bits_source = sinkC_io_req_valid ? sinkC_io_req_bits_source : _request_bits_T_source; // @[Scheduler.scala 193:22]
  wire [17:0] request_bits_tag = sinkC_io_req_valid ? sinkC_io_req_bits_tag : _request_bits_T_tag; // @[Scheduler.scala 193:22]
  wire [5:0] request_bits_offset = sinkC_io_req_valid ? sinkC_io_req_bits_offset : _request_bits_T_offset; // @[Scheduler.scala 193:22]
  wire [4:0] request_bits_put = sinkC_io_req_valid ? sinkC_io_req_bits_put : _request_bits_T_put; // @[Scheduler.scala 193:22]
  wire [9:0] request_bits_set = sinkC_io_req_valid ? sinkC_io_req_bits_set : _request_bits_T_set; // @[Scheduler.scala 193:22]
  wire  setMatches_hi_hi_hi = c_mshr_io_status_valid & c_mshr_io_status_bits_set == request_bits_set; // @[Scheduler.scala 202:59]
  wire  setMatches_hi_hi_lo = bc_mshr_io_status_valid & bc_mshr_io_status_bits_set == request_bits_set; // @[Scheduler.scala 202:59]
  wire  setMatches_hi_lo_hi = abc_mshrs_5_io_status_valid & abc_mshrs_5_io_status_bits_set == request_bits_set; // @[Scheduler.scala 202:59]
  wire  setMatches_hi_lo_lo = abc_mshrs_4_io_status_valid & abc_mshrs_4_io_status_bits_set == request_bits_set; // @[Scheduler.scala 202:59]
  wire  setMatches_lo_hi_hi = abc_mshrs_3_io_status_valid & abc_mshrs_3_io_status_bits_set == request_bits_set; // @[Scheduler.scala 202:59]
  wire  setMatches_lo_hi_lo = abc_mshrs_2_io_status_valid & abc_mshrs_2_io_status_bits_set == request_bits_set; // @[Scheduler.scala 202:59]
  wire  setMatches_lo_lo_hi = abc_mshrs_1_io_status_valid & abc_mshrs_1_io_status_bits_set == request_bits_set; // @[Scheduler.scala 202:59]
  wire  setMatches_lo_lo_lo = abc_mshrs_0_io_status_valid & abc_mshrs_0_io_status_bits_set == request_bits_set; // @[Scheduler.scala 202:59]
  wire [7:0] setMatches = {setMatches_hi_hi_hi,setMatches_hi_hi_lo,setMatches_hi_lo_hi,setMatches_hi_lo_lo,
    setMatches_lo_hi_hi,setMatches_lo_hi_lo,setMatches_lo_lo_hi,setMatches_lo_lo_lo}; // @[Cat.scala 30:58]
  wire  alloc = ~(|setMatches); // @[Scheduler.scala 203:15]
  wire [23:0] _selected_requests_T = {mshr_selectOH,mshr_selectOH,mshr_selectOH}; // @[Cat.scala 30:58]
  wire [23:0] selected_requests = _selected_requests_T & requests_io_valid; // @[Scheduler.scala 236:76]
  wire  a_pop = |selected_requests[7:0]; // @[Scheduler.scala 237:82]
  wire  b_pop = |selected_requests[15:8]; // @[Scheduler.scala 238:82]
  wire  c_pop = |selected_requests[23:16]; // @[Scheduler.scala 239:82]
  wire  may_pop = a_pop | b_pop | c_pop; // @[Scheduler.scala 242:32]
  wire  _mshr_uses_directory_assuming_no_bypass_T = schedule_reload & may_pop; // @[Scheduler.scala 277:64]
  wire  lb_tag_mismatch = scheduleTag != requests_io_data_tag; // @[Scheduler.scala 276:37]
  wire  mshr_uses_directory_assuming_no_bypass = schedule_reload & may_pop & lb_tag_mismatch; // @[Scheduler.scala 277:75]
  wire  _request_alloc_cases_T = ~mshr_uses_directory_assuming_no_bypass; // @[Scheduler.scala 288:16]
  wire [7:0] mshr_validOH = {c_mshr_io_status_valid,bc_mshr_io_status_valid,abc_mshrs_5_io_status_valid,
    abc_mshrs_4_io_status_valid,abc_mshrs_3_io_status_valid,abc_mshrs_2_io_status_valid,abc_mshrs_1_io_status_valid,
    abc_mshrs_0_io_status_valid}; // @[Cat.scala 30:58]
  wire [7:0] _mshr_free_T = ~mshr_validOH; // @[Scheduler.scala 283:20]
  wire  prioFilter_hi_lo = ~request_bits_prio_0; // @[Scheduler.scala 212:46]
  wire [7:0] prioFilter = {request_bits_prio_2,prioFilter_hi_lo,6'h3f}; // @[Cat.scala 30:58]
  wire [7:0] _mshr_free_T_1 = _mshr_free_T & prioFilter; // @[Scheduler.scala 283:34]
  wire  mshr_free = |_mshr_free_T_1; // @[Scheduler.scala 283:51]
  wire  _request_alloc_cases_T_2 = alloc & ~mshr_uses_directory_assuming_no_bypass & mshr_free; // @[Scheduler.scala 288:56]
  wire  _request_alloc_cases_T_7 = ~c_mshr_io_status_valid; // @[Scheduler.scala 289:87]
  wire  _nestC_T_22 = setMatches[0] & abc_mshrs_0_io_status_bits_nestC | setMatches[1] &
    abc_mshrs_1_io_status_bits_nestC | setMatches[2] & abc_mshrs_2_io_status_bits_nestC | setMatches[3] &
    abc_mshrs_3_io_status_bits_nestC | setMatches[4] & abc_mshrs_4_io_status_bits_nestC | setMatches[5] &
    abc_mshrs_5_io_status_bits_nestC | setMatches[6] & bc_mshr_io_status_bits_nestC | setMatches[7] &
    c_mshr_io_status_bits_nestC; // @[Mux.scala 27:72]
  wire  nestC = _nestC_T_22 & request_bits_prio_2; // @[Scheduler.scala 210:70]
  wire  _request_alloc_cases_T_13 = nestC & _request_alloc_cases_T & _request_alloc_cases_T_7; // @[Scheduler.scala 290:56]
  wire  request_alloc_cases = _request_alloc_cases_T_2 | _request_alloc_cases_T_13; // @[Scheduler.scala 289:112]
  wire [7:0] lowerMatches = setMatches & prioFilter; // @[Scheduler.scala 213:33]
  wire  _blockC_T_22 = setMatches[0] & abc_mshrs_0_io_status_bits_blockC | setMatches[1] &
    abc_mshrs_1_io_status_bits_blockC | setMatches[2] & abc_mshrs_2_io_status_bits_blockC | setMatches[3] &
    abc_mshrs_3_io_status_bits_blockC | setMatches[4] & abc_mshrs_4_io_status_bits_blockC | setMatches[5] &
    abc_mshrs_5_io_status_bits_blockC | setMatches[6] & bc_mshr_io_status_bits_blockC | setMatches[7] &
    c_mshr_io_status_bits_blockC; // @[Mux.scala 27:72]
  wire  blockC = _blockC_T_22 & request_bits_prio_2; // @[Scheduler.scala 206:70]
  wire  queue = |lowerMatches & ~nestC & ~blockC; // @[Scheduler.scala 215:65]
  wire [7:0] _lowerMatches1_T_2 = lowerMatches[6] ? 8'h40 : lowerMatches; // @[Scheduler.scala 231:8]
  wire [7:0] lowerMatches1 = lowerMatches[7] ? 8'h80 : _lowerMatches1_T_2; // @[Scheduler.scala 230:8]
  wire [7:0] _bypassMatches_T = mshr_selectOH & lowerMatches1; // @[Scheduler.scala 240:38]
  wire  _bypassMatches_T_7 = b_pop ? ~b_pop : ~a_pop; // @[Scheduler.scala 241:69]
  wire  _bypassMatches_T_8 = c_pop | request_bits_prio_2 ? ~c_pop : _bypassMatches_T_7; // @[Scheduler.scala 241:26]
  wire  bypassMatches = |_bypassMatches_T & _bypassMatches_T_8; // @[Scheduler.scala 240:61]
  wire  bypassQueue = schedule_reload & bypassMatches; // @[Scheduler.scala 286:37]
  wire  request_ready = request_alloc_cases | queue & (bypassQueue | requests_io_push_ready); // @[Scheduler.scala 291:40]
  wire  _sinkC_io_req_ready_T = directory_io_ready & request_ready; // @[Scheduler.scala 196:44]
  wire  _sinkX_io_req_ready_T_2 = _sinkC_io_req_ready_T & ~sinkC_io_req_valid; // @[Scheduler.scala 198:61]
  wire  _T_11 = request_valid & nestC; // @[Scheduler.scala 223:33]
  wire  _T_14 = request_valid & queue; // @[Scheduler.scala 225:31]
  wire  bypass = _T_14 & bypassMatches; // @[Scheduler.scala 243:39]
  wire  will_reload = schedule_reload & (may_pop | bypass); // @[Scheduler.scala 244:37]
  wire  will_pop = _mshr_uses_directory_assuming_no_bypass_T & ~bypass; // @[Scheduler.scala 245:45]
  wire  a_pop_1 = requests_io_valid[0]; // @[Scheduler.scala 255:34]
  wire  b_pop_1 = requests_io_valid[8]; // @[Scheduler.scala 256:34]
  wire  c_pop_1 = requests_io_valid[16]; // @[Scheduler.scala 257:34]
  wire  _bypassMatches_T_15 = b_pop_1 ? ~b_pop_1 : ~a_pop_1; // @[Scheduler.scala 259:71]
  wire  _bypassMatches_T_16 = c_pop_1 | request_bits_prio_2 ? ~c_pop_1 : _bypassMatches_T_15; // @[Scheduler.scala 259:28]
  wire  bypassMatches_1 = lowerMatches1[0] & _bypassMatches_T_16; // @[Scheduler.scala 258:42]
  wire  may_pop_1 = a_pop_1 | b_pop_1 | c_pop_1; // @[Scheduler.scala 260:34]
  wire  bypass_1 = _T_14 & bypassMatches_1; // @[Scheduler.scala 261:41]
  wire  will_reload_1 = abc_mshrs_0_io_schedule_bits_reload & (may_pop_1 | bypass_1); // @[Scheduler.scala 262:49]
  wire  _mshrs_0_io_allocate_bits_T_prio_0 = bypass_1 ? request_bits_prio_0 : requests_io_data_prio_0; // @[Scheduler.scala 263:30]
  wire  _mshrs_0_io_allocate_bits_T_prio_2 = bypass_1 ? request_bits_prio_2 : requests_io_data_prio_2; // @[Scheduler.scala 263:30]
  wire  _mshrs_0_io_allocate_bits_T_control = bypass_1 ? request_bits_control : requests_io_data_control; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_0_io_allocate_bits_T_opcode = bypass_1 ? request_bits_opcode : requests_io_data_opcode; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_0_io_allocate_bits_T_param = bypass_1 ? request_bits_param : requests_io_data_param; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_0_io_allocate_bits_T_size = bypass_1 ? request_bits_size : requests_io_data_size; // @[Scheduler.scala 263:30]
  wire [6:0] _mshrs_0_io_allocate_bits_T_source = bypass_1 ? request_bits_source : requests_io_data_source; // @[Scheduler.scala 263:30]
  wire [17:0] _mshrs_0_io_allocate_bits_T_tag = bypass_1 ? request_bits_tag : requests_io_data_tag; // @[Scheduler.scala 263:30]
  wire [5:0] _mshrs_0_io_allocate_bits_T_offset = bypass_1 ? request_bits_offset : requests_io_data_offset; // @[Scheduler.scala 263:30]
  wire [4:0] _mshrs_0_io_allocate_bits_T_put = bypass_1 ? request_bits_put : requests_io_data_put; // @[Scheduler.scala 263:30]
  wire  a_pop_2 = requests_io_valid[1]; // @[Scheduler.scala 255:34]
  wire  b_pop_2 = requests_io_valid[9]; // @[Scheduler.scala 256:34]
  wire  c_pop_2 = requests_io_valid[17]; // @[Scheduler.scala 257:34]
  wire  _bypassMatches_T_23 = b_pop_2 ? ~b_pop_2 : ~a_pop_2; // @[Scheduler.scala 259:71]
  wire  _bypassMatches_T_24 = c_pop_2 | request_bits_prio_2 ? ~c_pop_2 : _bypassMatches_T_23; // @[Scheduler.scala 259:28]
  wire  bypassMatches_2 = lowerMatches1[1] & _bypassMatches_T_24; // @[Scheduler.scala 258:42]
  wire  may_pop_2 = a_pop_2 | b_pop_2 | c_pop_2; // @[Scheduler.scala 260:34]
  wire  bypass_2 = _T_14 & bypassMatches_2; // @[Scheduler.scala 261:41]
  wire  will_reload_2 = abc_mshrs_1_io_schedule_bits_reload & (may_pop_2 | bypass_2); // @[Scheduler.scala 262:49]
  wire  _mshrs_1_io_allocate_bits_T_prio_0 = bypass_2 ? request_bits_prio_0 : requests_io_data_prio_0; // @[Scheduler.scala 263:30]
  wire  _mshrs_1_io_allocate_bits_T_prio_2 = bypass_2 ? request_bits_prio_2 : requests_io_data_prio_2; // @[Scheduler.scala 263:30]
  wire  _mshrs_1_io_allocate_bits_T_control = bypass_2 ? request_bits_control : requests_io_data_control; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_1_io_allocate_bits_T_opcode = bypass_2 ? request_bits_opcode : requests_io_data_opcode; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_1_io_allocate_bits_T_param = bypass_2 ? request_bits_param : requests_io_data_param; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_1_io_allocate_bits_T_size = bypass_2 ? request_bits_size : requests_io_data_size; // @[Scheduler.scala 263:30]
  wire [6:0] _mshrs_1_io_allocate_bits_T_source = bypass_2 ? request_bits_source : requests_io_data_source; // @[Scheduler.scala 263:30]
  wire [17:0] _mshrs_1_io_allocate_bits_T_tag = bypass_2 ? request_bits_tag : requests_io_data_tag; // @[Scheduler.scala 263:30]
  wire [5:0] _mshrs_1_io_allocate_bits_T_offset = bypass_2 ? request_bits_offset : requests_io_data_offset; // @[Scheduler.scala 263:30]
  wire [4:0] _mshrs_1_io_allocate_bits_T_put = bypass_2 ? request_bits_put : requests_io_data_put; // @[Scheduler.scala 263:30]
  wire  a_pop_3 = requests_io_valid[2]; // @[Scheduler.scala 255:34]
  wire  b_pop_3 = requests_io_valid[10]; // @[Scheduler.scala 256:34]
  wire  c_pop_3 = requests_io_valid[18]; // @[Scheduler.scala 257:34]
  wire  _bypassMatches_T_31 = b_pop_3 ? ~b_pop_3 : ~a_pop_3; // @[Scheduler.scala 259:71]
  wire  _bypassMatches_T_32 = c_pop_3 | request_bits_prio_2 ? ~c_pop_3 : _bypassMatches_T_31; // @[Scheduler.scala 259:28]
  wire  bypassMatches_3 = lowerMatches1[2] & _bypassMatches_T_32; // @[Scheduler.scala 258:42]
  wire  may_pop_3 = a_pop_3 | b_pop_3 | c_pop_3; // @[Scheduler.scala 260:34]
  wire  bypass_3 = _T_14 & bypassMatches_3; // @[Scheduler.scala 261:41]
  wire  will_reload_3 = abc_mshrs_2_io_schedule_bits_reload & (may_pop_3 | bypass_3); // @[Scheduler.scala 262:49]
  wire  _mshrs_2_io_allocate_bits_T_prio_0 = bypass_3 ? request_bits_prio_0 : requests_io_data_prio_0; // @[Scheduler.scala 263:30]
  wire  _mshrs_2_io_allocate_bits_T_prio_2 = bypass_3 ? request_bits_prio_2 : requests_io_data_prio_2; // @[Scheduler.scala 263:30]
  wire  _mshrs_2_io_allocate_bits_T_control = bypass_3 ? request_bits_control : requests_io_data_control; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_2_io_allocate_bits_T_opcode = bypass_3 ? request_bits_opcode : requests_io_data_opcode; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_2_io_allocate_bits_T_param = bypass_3 ? request_bits_param : requests_io_data_param; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_2_io_allocate_bits_T_size = bypass_3 ? request_bits_size : requests_io_data_size; // @[Scheduler.scala 263:30]
  wire [6:0] _mshrs_2_io_allocate_bits_T_source = bypass_3 ? request_bits_source : requests_io_data_source; // @[Scheduler.scala 263:30]
  wire [17:0] _mshrs_2_io_allocate_bits_T_tag = bypass_3 ? request_bits_tag : requests_io_data_tag; // @[Scheduler.scala 263:30]
  wire [5:0] _mshrs_2_io_allocate_bits_T_offset = bypass_3 ? request_bits_offset : requests_io_data_offset; // @[Scheduler.scala 263:30]
  wire [4:0] _mshrs_2_io_allocate_bits_T_put = bypass_3 ? request_bits_put : requests_io_data_put; // @[Scheduler.scala 263:30]
  wire  a_pop_4 = requests_io_valid[3]; // @[Scheduler.scala 255:34]
  wire  b_pop_4 = requests_io_valid[11]; // @[Scheduler.scala 256:34]
  wire  c_pop_4 = requests_io_valid[19]; // @[Scheduler.scala 257:34]
  wire  _bypassMatches_T_39 = b_pop_4 ? ~b_pop_4 : ~a_pop_4; // @[Scheduler.scala 259:71]
  wire  _bypassMatches_T_40 = c_pop_4 | request_bits_prio_2 ? ~c_pop_4 : _bypassMatches_T_39; // @[Scheduler.scala 259:28]
  wire  bypassMatches_4 = lowerMatches1[3] & _bypassMatches_T_40; // @[Scheduler.scala 258:42]
  wire  may_pop_4 = a_pop_4 | b_pop_4 | c_pop_4; // @[Scheduler.scala 260:34]
  wire  bypass_4 = _T_14 & bypassMatches_4; // @[Scheduler.scala 261:41]
  wire  will_reload_4 = abc_mshrs_3_io_schedule_bits_reload & (may_pop_4 | bypass_4); // @[Scheduler.scala 262:49]
  wire  _mshrs_3_io_allocate_bits_T_prio_0 = bypass_4 ? request_bits_prio_0 : requests_io_data_prio_0; // @[Scheduler.scala 263:30]
  wire  _mshrs_3_io_allocate_bits_T_prio_2 = bypass_4 ? request_bits_prio_2 : requests_io_data_prio_2; // @[Scheduler.scala 263:30]
  wire  _mshrs_3_io_allocate_bits_T_control = bypass_4 ? request_bits_control : requests_io_data_control; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_3_io_allocate_bits_T_opcode = bypass_4 ? request_bits_opcode : requests_io_data_opcode; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_3_io_allocate_bits_T_param = bypass_4 ? request_bits_param : requests_io_data_param; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_3_io_allocate_bits_T_size = bypass_4 ? request_bits_size : requests_io_data_size; // @[Scheduler.scala 263:30]
  wire [6:0] _mshrs_3_io_allocate_bits_T_source = bypass_4 ? request_bits_source : requests_io_data_source; // @[Scheduler.scala 263:30]
  wire [17:0] _mshrs_3_io_allocate_bits_T_tag = bypass_4 ? request_bits_tag : requests_io_data_tag; // @[Scheduler.scala 263:30]
  wire [5:0] _mshrs_3_io_allocate_bits_T_offset = bypass_4 ? request_bits_offset : requests_io_data_offset; // @[Scheduler.scala 263:30]
  wire [4:0] _mshrs_3_io_allocate_bits_T_put = bypass_4 ? request_bits_put : requests_io_data_put; // @[Scheduler.scala 263:30]
  wire  a_pop_5 = requests_io_valid[4]; // @[Scheduler.scala 255:34]
  wire  b_pop_5 = requests_io_valid[12]; // @[Scheduler.scala 256:34]
  wire  c_pop_5 = requests_io_valid[20]; // @[Scheduler.scala 257:34]
  wire  _bypassMatches_T_47 = b_pop_5 ? ~b_pop_5 : ~a_pop_5; // @[Scheduler.scala 259:71]
  wire  _bypassMatches_T_48 = c_pop_5 | request_bits_prio_2 ? ~c_pop_5 : _bypassMatches_T_47; // @[Scheduler.scala 259:28]
  wire  bypassMatches_5 = lowerMatches1[4] & _bypassMatches_T_48; // @[Scheduler.scala 258:42]
  wire  may_pop_5 = a_pop_5 | b_pop_5 | c_pop_5; // @[Scheduler.scala 260:34]
  wire  bypass_5 = _T_14 & bypassMatches_5; // @[Scheduler.scala 261:41]
  wire  will_reload_5 = abc_mshrs_4_io_schedule_bits_reload & (may_pop_5 | bypass_5); // @[Scheduler.scala 262:49]
  wire  _mshrs_4_io_allocate_bits_T_prio_0 = bypass_5 ? request_bits_prio_0 : requests_io_data_prio_0; // @[Scheduler.scala 263:30]
  wire  _mshrs_4_io_allocate_bits_T_prio_2 = bypass_5 ? request_bits_prio_2 : requests_io_data_prio_2; // @[Scheduler.scala 263:30]
  wire  _mshrs_4_io_allocate_bits_T_control = bypass_5 ? request_bits_control : requests_io_data_control; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_4_io_allocate_bits_T_opcode = bypass_5 ? request_bits_opcode : requests_io_data_opcode; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_4_io_allocate_bits_T_param = bypass_5 ? request_bits_param : requests_io_data_param; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_4_io_allocate_bits_T_size = bypass_5 ? request_bits_size : requests_io_data_size; // @[Scheduler.scala 263:30]
  wire [6:0] _mshrs_4_io_allocate_bits_T_source = bypass_5 ? request_bits_source : requests_io_data_source; // @[Scheduler.scala 263:30]
  wire [17:0] _mshrs_4_io_allocate_bits_T_tag = bypass_5 ? request_bits_tag : requests_io_data_tag; // @[Scheduler.scala 263:30]
  wire [5:0] _mshrs_4_io_allocate_bits_T_offset = bypass_5 ? request_bits_offset : requests_io_data_offset; // @[Scheduler.scala 263:30]
  wire [4:0] _mshrs_4_io_allocate_bits_T_put = bypass_5 ? request_bits_put : requests_io_data_put; // @[Scheduler.scala 263:30]
  wire  a_pop_6 = requests_io_valid[5]; // @[Scheduler.scala 255:34]
  wire  b_pop_6 = requests_io_valid[13]; // @[Scheduler.scala 256:34]
  wire  c_pop_6 = requests_io_valid[21]; // @[Scheduler.scala 257:34]
  wire  _bypassMatches_T_55 = b_pop_6 ? ~b_pop_6 : ~a_pop_6; // @[Scheduler.scala 259:71]
  wire  _bypassMatches_T_56 = c_pop_6 | request_bits_prio_2 ? ~c_pop_6 : _bypassMatches_T_55; // @[Scheduler.scala 259:28]
  wire  bypassMatches_6 = lowerMatches1[5] & _bypassMatches_T_56; // @[Scheduler.scala 258:42]
  wire  may_pop_6 = a_pop_6 | b_pop_6 | c_pop_6; // @[Scheduler.scala 260:34]
  wire  bypass_6 = _T_14 & bypassMatches_6; // @[Scheduler.scala 261:41]
  wire  will_reload_6 = abc_mshrs_5_io_schedule_bits_reload & (may_pop_6 | bypass_6); // @[Scheduler.scala 262:49]
  wire  _mshrs_5_io_allocate_bits_T_prio_0 = bypass_6 ? request_bits_prio_0 : requests_io_data_prio_0; // @[Scheduler.scala 263:30]
  wire  _mshrs_5_io_allocate_bits_T_prio_2 = bypass_6 ? request_bits_prio_2 : requests_io_data_prio_2; // @[Scheduler.scala 263:30]
  wire  _mshrs_5_io_allocate_bits_T_control = bypass_6 ? request_bits_control : requests_io_data_control; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_5_io_allocate_bits_T_opcode = bypass_6 ? request_bits_opcode : requests_io_data_opcode; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_5_io_allocate_bits_T_param = bypass_6 ? request_bits_param : requests_io_data_param; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_5_io_allocate_bits_T_size = bypass_6 ? request_bits_size : requests_io_data_size; // @[Scheduler.scala 263:30]
  wire [6:0] _mshrs_5_io_allocate_bits_T_source = bypass_6 ? request_bits_source : requests_io_data_source; // @[Scheduler.scala 263:30]
  wire [17:0] _mshrs_5_io_allocate_bits_T_tag = bypass_6 ? request_bits_tag : requests_io_data_tag; // @[Scheduler.scala 263:30]
  wire [5:0] _mshrs_5_io_allocate_bits_T_offset = bypass_6 ? request_bits_offset : requests_io_data_offset; // @[Scheduler.scala 263:30]
  wire [4:0] _mshrs_5_io_allocate_bits_T_put = bypass_6 ? request_bits_put : requests_io_data_put; // @[Scheduler.scala 263:30]
  wire  a_pop_7 = requests_io_valid[6]; // @[Scheduler.scala 255:34]
  wire  b_pop_7 = requests_io_valid[14]; // @[Scheduler.scala 256:34]
  wire  c_pop_7 = requests_io_valid[22]; // @[Scheduler.scala 257:34]
  wire  _bypassMatches_T_63 = b_pop_7 ? ~b_pop_7 : ~a_pop_7; // @[Scheduler.scala 259:71]
  wire  _bypassMatches_T_64 = c_pop_7 | request_bits_prio_2 ? ~c_pop_7 : _bypassMatches_T_63; // @[Scheduler.scala 259:28]
  wire  bypassMatches_7 = lowerMatches1[6] & _bypassMatches_T_64; // @[Scheduler.scala 258:42]
  wire  may_pop_7 = a_pop_7 | b_pop_7 | c_pop_7; // @[Scheduler.scala 260:34]
  wire  bypass_7 = _T_14 & bypassMatches_7; // @[Scheduler.scala 261:41]
  wire  will_reload_7 = bc_mshr_io_schedule_bits_reload & (may_pop_7 | bypass_7); // @[Scheduler.scala 262:49]
  wire  _mshrs_6_io_allocate_bits_T_prio_2 = bypass_7 ? request_bits_prio_2 : requests_io_data_prio_2; // @[Scheduler.scala 263:30]
  wire  _mshrs_6_io_allocate_bits_T_control = bypass_7 ? request_bits_control : requests_io_data_control; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_6_io_allocate_bits_T_opcode = bypass_7 ? request_bits_opcode : requests_io_data_opcode; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_6_io_allocate_bits_T_param = bypass_7 ? request_bits_param : requests_io_data_param; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_6_io_allocate_bits_T_size = bypass_7 ? request_bits_size : requests_io_data_size; // @[Scheduler.scala 263:30]
  wire [6:0] _mshrs_6_io_allocate_bits_T_source = bypass_7 ? request_bits_source : requests_io_data_source; // @[Scheduler.scala 263:30]
  wire [17:0] _mshrs_6_io_allocate_bits_T_tag = bypass_7 ? request_bits_tag : requests_io_data_tag; // @[Scheduler.scala 263:30]
  wire [5:0] _mshrs_6_io_allocate_bits_T_offset = bypass_7 ? request_bits_offset : requests_io_data_offset; // @[Scheduler.scala 263:30]
  wire [4:0] _mshrs_6_io_allocate_bits_T_put = bypass_7 ? request_bits_put : requests_io_data_put; // @[Scheduler.scala 263:30]
  wire  a_pop_8 = requests_io_valid[7]; // @[Scheduler.scala 255:34]
  wire  b_pop_8 = requests_io_valid[15]; // @[Scheduler.scala 256:34]
  wire  c_pop_8 = requests_io_valid[23]; // @[Scheduler.scala 257:34]
  wire  _bypassMatches_T_71 = b_pop_8 ? ~b_pop_8 : ~a_pop_8; // @[Scheduler.scala 259:71]
  wire  _bypassMatches_T_72 = c_pop_8 | request_bits_prio_2 ? ~c_pop_8 : _bypassMatches_T_71; // @[Scheduler.scala 259:28]
  wire  bypassMatches_8 = lowerMatches1[7] & _bypassMatches_T_72; // @[Scheduler.scala 258:42]
  wire  may_pop_8 = a_pop_8 | b_pop_8 | c_pop_8; // @[Scheduler.scala 260:34]
  wire  bypass_8 = _T_14 & bypassMatches_8; // @[Scheduler.scala 261:41]
  wire  will_reload_8 = c_mshr_io_schedule_bits_reload & (may_pop_8 | bypass_8); // @[Scheduler.scala 262:49]
  wire  _mshrs_7_io_allocate_bits_T_prio_2 = bypass_8 ? request_bits_prio_2 : requests_io_data_prio_2; // @[Scheduler.scala 263:30]
  wire  _mshrs_7_io_allocate_bits_T_control = bypass_8 ? request_bits_control : requests_io_data_control; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_7_io_allocate_bits_T_opcode = bypass_8 ? request_bits_opcode : requests_io_data_opcode; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_7_io_allocate_bits_T_param = bypass_8 ? request_bits_param : requests_io_data_param; // @[Scheduler.scala 263:30]
  wire [2:0] _mshrs_7_io_allocate_bits_T_size = bypass_8 ? request_bits_size : requests_io_data_size; // @[Scheduler.scala 263:30]
  wire [6:0] _mshrs_7_io_allocate_bits_T_source = bypass_8 ? request_bits_source : requests_io_data_source; // @[Scheduler.scala 263:30]
  wire [17:0] _mshrs_7_io_allocate_bits_T_tag = bypass_8 ? request_bits_tag : requests_io_data_tag; // @[Scheduler.scala 263:30]
  wire [5:0] _mshrs_7_io_allocate_bits_T_offset = bypass_8 ? request_bits_offset : requests_io_data_offset; // @[Scheduler.scala 263:30]
  wire [4:0] _mshrs_7_io_allocate_bits_T_put = bypass_8 ? request_bits_put : requests_io_data_put; // @[Scheduler.scala 263:30]
  wire [23:0] _prio_requests_T = ~requests_io_valid; // @[Scheduler.scala 270:25]
  wire [23:0] _GEN_128 = {{8'd0}, requests_io_valid[23:8]}; // @[Scheduler.scala 270:44]
  wire [23:0] _prio_requests_T_2 = _prio_requests_T | _GEN_128; // @[Scheduler.scala 270:44]
  wire [23:0] _GEN_129 = {{16'd0}, requests_io_valid[23:16]}; // @[Scheduler.scala 270:82]
  wire [23:0] _prio_requests_T_4 = _prio_requests_T_2 | _GEN_129; // @[Scheduler.scala 270:82]
  wire [23:0] prio_requests = ~_prio_requests_T_4; // @[Scheduler.scala 270:23]
  wire [23:0] _pop_index_T_1 = _selected_requests_T & prio_requests; // @[Scheduler.scala 271:77]
  wire [7:0] pop_index_hi_1 = _pop_index_T_1[23:16]; // @[OneHot.scala 30:18]
  wire [15:0] pop_index_lo = _pop_index_T_1[15:0]; // @[OneHot.scala 31:18]
  wire  pop_index_hi_2 = |pop_index_hi_1; // @[OneHot.scala 32:14]
  wire [15:0] _GEN_182 = {{8'd0}, pop_index_hi_1}; // @[OneHot.scala 32:28]
  wire [15:0] _pop_index_T_2 = _GEN_182 | pop_index_lo; // @[OneHot.scala 32:28]
  wire [7:0] pop_index_hi_3 = _pop_index_T_2[15:8]; // @[OneHot.scala 30:18]
  wire [7:0] pop_index_lo_1 = _pop_index_T_2[7:0]; // @[OneHot.scala 31:18]
  wire  pop_index_hi_4 = |pop_index_hi_3; // @[OneHot.scala 32:14]
  wire [7:0] _pop_index_T_3 = pop_index_hi_3 | pop_index_lo_1; // @[OneHot.scala 32:28]
  wire [3:0] pop_index_hi_5 = _pop_index_T_3[7:4]; // @[OneHot.scala 30:18]
  wire [3:0] pop_index_lo_2 = _pop_index_T_3[3:0]; // @[OneHot.scala 31:18]
  wire  pop_index_hi_6 = |pop_index_hi_5; // @[OneHot.scala 32:14]
  wire [3:0] _pop_index_T_4 = pop_index_hi_5 | pop_index_lo_2; // @[OneHot.scala 32:28]
  wire [1:0] pop_index_hi_7 = _pop_index_T_4[3:2]; // @[OneHot.scala 30:18]
  wire [1:0] pop_index_lo_3 = _pop_index_T_4[1:0]; // @[OneHot.scala 31:18]
  wire  pop_index_hi_8 = |pop_index_hi_7; // @[OneHot.scala 32:14]
  wire [1:0] _pop_index_T_5 = pop_index_hi_7 | pop_index_lo_3; // @[OneHot.scala 32:28]
  wire  pop_index_lo_4 = _pop_index_T_5[1]; // @[CircuitMath.scala 30:8]
  wire [3:0] pop_index_lo_7 = {pop_index_hi_4,pop_index_hi_6,pop_index_hi_8,pop_index_lo_4}; // @[Cat.scala 30:58]
  wire  mshr_uses_directory_for_lb = will_pop & lb_tag_mismatch; // @[Scheduler.scala 278:45]
  wire [17:0] _mshr_uses_directory_T = bypass ? request_bits_tag : requests_io_data_tag; // @[Scheduler.scala 279:63]
  wire  mshr_uses_directory = will_reload & scheduleTag != _mshr_uses_directory_T; // @[Scheduler.scala 279:41]
  wire  alloc_uses_directory = request_valid & request_alloc_cases; // @[Scheduler.scala 292:44]
  wire [3:0] requests_io_push_bits_index_hi = lowerMatches1[7:4]; // @[OneHot.scala 30:18]
  wire [3:0] requests_io_push_bits_index_lo = lowerMatches1[3:0]; // @[OneHot.scala 31:18]
  wire  requests_io_push_bits_index_hi_1 = |requests_io_push_bits_index_hi; // @[OneHot.scala 32:14]
  wire [3:0] _requests_io_push_bits_index_T_1 = requests_io_push_bits_index_hi | requests_io_push_bits_index_lo; // @[OneHot.scala 32:28]
  wire [1:0] requests_io_push_bits_index_hi_2 = _requests_io_push_bits_index_T_1[3:2]; // @[OneHot.scala 30:18]
  wire [1:0] requests_io_push_bits_index_lo_1 = _requests_io_push_bits_index_T_1[1:0]; // @[OneHot.scala 31:18]
  wire  requests_io_push_bits_index_hi_3 = |requests_io_push_bits_index_hi_2; // @[OneHot.scala 32:14]
  wire [1:0] _requests_io_push_bits_index_T_2 = requests_io_push_bits_index_hi_2 | requests_io_push_bits_index_lo_1; // @[OneHot.scala 32:28]
  wire  requests_io_push_bits_index_lo_2 = _requests_io_push_bits_index_T_2[1]; // @[CircuitMath.scala 30:8]
  wire [2:0] _requests_io_push_bits_index_T_3 = {requests_io_push_bits_index_hi_1,requests_io_push_bits_index_hi_3,
    requests_io_push_bits_index_lo_2}; // @[Cat.scala 30:58]
  wire [23:0] _requests_io_push_bits_index_T_9 = {lowerMatches1, 16'h0}; // @[Scheduler.scala 311:30]
  wire [7:0] requests_io_push_bits_index_hi_10 = _requests_io_push_bits_index_T_9[23:16]; // @[OneHot.scala 30:18]
  wire [15:0] requests_io_push_bits_index_lo_10 = _requests_io_push_bits_index_T_9[15:0]; // @[OneHot.scala 31:18]
  wire  requests_io_push_bits_index_hi_11 = |requests_io_push_bits_index_hi_10; // @[OneHot.scala 32:14]
  wire [15:0] _GEN_183 = {{8'd0}, requests_io_push_bits_index_hi_10}; // @[OneHot.scala 32:28]
  wire [15:0] _requests_io_push_bits_index_T_10 = _GEN_183 | requests_io_push_bits_index_lo_10; // @[OneHot.scala 32:28]
  wire [7:0] requests_io_push_bits_index_hi_12 = _requests_io_push_bits_index_T_10[15:8]; // @[OneHot.scala 30:18]
  wire [7:0] requests_io_push_bits_index_lo_11 = _requests_io_push_bits_index_T_10[7:0]; // @[OneHot.scala 31:18]
  wire  requests_io_push_bits_index_hi_13 = |requests_io_push_bits_index_hi_12; // @[OneHot.scala 32:14]
  wire [7:0] _requests_io_push_bits_index_T_11 = requests_io_push_bits_index_hi_12 | requests_io_push_bits_index_lo_11; // @[OneHot.scala 32:28]
  wire [3:0] requests_io_push_bits_index_hi_14 = _requests_io_push_bits_index_T_11[7:4]; // @[OneHot.scala 30:18]
  wire [3:0] requests_io_push_bits_index_lo_12 = _requests_io_push_bits_index_T_11[3:0]; // @[OneHot.scala 31:18]
  wire  requests_io_push_bits_index_hi_15 = |requests_io_push_bits_index_hi_14; // @[OneHot.scala 32:14]
  wire [3:0] _requests_io_push_bits_index_T_12 = requests_io_push_bits_index_hi_14 | requests_io_push_bits_index_lo_12; // @[OneHot.scala 32:28]
  wire [1:0] requests_io_push_bits_index_hi_16 = _requests_io_push_bits_index_T_12[3:2]; // @[OneHot.scala 30:18]
  wire [1:0] requests_io_push_bits_index_lo_13 = _requests_io_push_bits_index_T_12[1:0]; // @[OneHot.scala 31:18]
  wire  requests_io_push_bits_index_hi_17 = |requests_io_push_bits_index_hi_16; // @[OneHot.scala 32:14]
  wire [1:0] _requests_io_push_bits_index_T_13 = requests_io_push_bits_index_hi_16 | requests_io_push_bits_index_lo_13; // @[OneHot.scala 32:28]
  wire  requests_io_push_bits_index_lo_14 = _requests_io_push_bits_index_T_13[1]; // @[CircuitMath.scala 30:8]
  wire [4:0] _requests_io_push_bits_index_T_14 = {requests_io_push_bits_index_hi_11,requests_io_push_bits_index_hi_13,
    requests_io_push_bits_index_hi_15,requests_io_push_bits_index_hi_17,requests_io_push_bits_index_lo_14}; // @[Cat.scala 30:58]
  wire [2:0] _requests_io_push_bits_index_T_15 = request_bits_prio_0 ? _requests_io_push_bits_index_T_3 : 3'h0; // @[Mux.scala 27:72]
  wire [4:0] _requests_io_push_bits_index_T_17 = request_bits_prio_2 ? _requests_io_push_bits_index_T_14 : 5'h0; // @[Mux.scala 27:72]
  wire [3:0] _requests_io_push_bits_index_T_18 = {{1'd0}, _requests_io_push_bits_index_T_15}; // @[Mux.scala 27:72]
  wire [4:0] _GEN_184 = {{1'd0}, _requests_io_push_bits_index_T_18}; // @[Mux.scala 27:72]
  wire [8:0] _mshr_insertOH_T_1 = {_mshr_free_T, 1'h0}; // @[package.scala 244:48]
  wire [7:0] _mshr_insertOH_T_3 = _mshr_free_T | _mshr_insertOH_T_1[7:0]; // @[package.scala 244:43]
  wire [9:0] _mshr_insertOH_T_4 = {_mshr_insertOH_T_3, 2'h0}; // @[package.scala 244:48]
  wire [7:0] _mshr_insertOH_T_6 = _mshr_insertOH_T_3 | _mshr_insertOH_T_4[7:0]; // @[package.scala 244:43]
  wire [11:0] _mshr_insertOH_T_7 = {_mshr_insertOH_T_6, 4'h0}; // @[package.scala 244:48]
  wire [7:0] _mshr_insertOH_T_9 = _mshr_insertOH_T_6 | _mshr_insertOH_T_7[7:0]; // @[package.scala 244:43]
  wire [8:0] _mshr_insertOH_T_11 = {_mshr_insertOH_T_9, 1'h0}; // @[Scheduler.scala 313:47]
  wire [8:0] _mshr_insertOH_T_12 = ~_mshr_insertOH_T_11; // @[Scheduler.scala 313:23]
  wire [8:0] _GEN_185 = {{1'd0}, _mshr_free_T}; // @[Scheduler.scala 313:53]
  wire [8:0] _mshr_insertOH_T_14 = _mshr_insertOH_T_12 & _GEN_185; // @[Scheduler.scala 313:53]
  wire [8:0] _GEN_186 = {{1'd0}, prioFilter}; // @[Scheduler.scala 313:69]
  wire [8:0] mshr_insertOH = _mshr_insertOH_T_14 & _GEN_186; // @[Scheduler.scala 313:69]
  wire  _GEN_99 = request_valid & alloc & mshr_insertOH[7] & _request_alloc_cases_T | mshr_selectOH[7] & will_reload_8; // @[Scheduler.scala 315:83 Scheduler.scala 316:27 Scheduler.scala 266:25]
  wire  _GEN_102 = request_valid & alloc & mshr_insertOH[7] & _request_alloc_cases_T ? request_bits_prio_2 :
    _mshrs_7_io_allocate_bits_T_prio_2; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  wire  _GEN_103 = request_valid & alloc & mshr_insertOH[7] & _request_alloc_cases_T ? request_bits_control :
    _mshrs_7_io_allocate_bits_T_control; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  wire [2:0] _GEN_104 = request_valid & alloc & mshr_insertOH[7] & _request_alloc_cases_T ? request_bits_opcode :
    _mshrs_7_io_allocate_bits_T_opcode; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  wire [2:0] _GEN_105 = request_valid & alloc & mshr_insertOH[7] & _request_alloc_cases_T ? request_bits_param :
    _mshrs_7_io_allocate_bits_T_param; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  wire [2:0] _GEN_106 = request_valid & alloc & mshr_insertOH[7] & _request_alloc_cases_T ? request_bits_size :
    _mshrs_7_io_allocate_bits_T_size; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  wire [6:0] _GEN_107 = request_valid & alloc & mshr_insertOH[7] & _request_alloc_cases_T ? request_bits_source :
    _mshrs_7_io_allocate_bits_T_source; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  wire [17:0] _GEN_108 = request_valid & alloc & mshr_insertOH[7] & _request_alloc_cases_T ? request_bits_tag :
    _mshrs_7_io_allocate_bits_T_tag; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  wire [5:0] _GEN_109 = request_valid & alloc & mshr_insertOH[7] & _request_alloc_cases_T ? request_bits_offset :
    _mshrs_7_io_allocate_bits_T_offset; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  wire [4:0] _GEN_110 = request_valid & alloc & mshr_insertOH[7] & _request_alloc_cases_T ? request_bits_put :
    _mshrs_7_io_allocate_bits_T_put; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  wire [9:0] _GEN_111 = request_valid & alloc & mshr_insertOH[7] & _request_alloc_cases_T ? request_bits_set :
    c_mshr_io_status_bits_set; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 264:28]
  wire  _GEN_112 = request_valid & alloc & mshr_insertOH[7] & _request_alloc_cases_T ? 1'h0 :
    c_mshr_io_allocate_bits_tag == c_mshr_io_status_bits_tag; // @[Scheduler.scala 315:83 Scheduler.scala 318:33 Scheduler.scala 265:31]
  wire  _T_87 = _T_11 & _request_alloc_cases_T_7 & _request_alloc_cases_T; // @[Scheduler.scala 330:59]
  reg [8:0] directoryFanout; // @[Scheduler.scala 342:46]
  wire  _sinkC_io_way_T_3 = abc_mshrs_0_io_status_valid & abc_mshrs_0_io_status_bits_set == sinkC_io_set; // @[Scheduler.scala 373:50]
  wire  _sinkC_io_way_T_5 = abc_mshrs_1_io_status_valid & abc_mshrs_1_io_status_bits_set == sinkC_io_set; // @[Scheduler.scala 373:50]
  wire  _sinkC_io_way_T_7 = abc_mshrs_2_io_status_valid & abc_mshrs_2_io_status_bits_set == sinkC_io_set; // @[Scheduler.scala 373:50]
  wire  _sinkC_io_way_T_9 = abc_mshrs_3_io_status_valid & abc_mshrs_3_io_status_bits_set == sinkC_io_set; // @[Scheduler.scala 373:50]
  wire  _sinkC_io_way_T_11 = abc_mshrs_4_io_status_valid & abc_mshrs_4_io_status_bits_set == sinkC_io_set; // @[Scheduler.scala 373:50]
  wire  _sinkC_io_way_T_13 = abc_mshrs_5_io_status_valid & abc_mshrs_5_io_status_bits_set == sinkC_io_set; // @[Scheduler.scala 373:50]
  wire [2:0] _sinkC_io_way_T_14 = _sinkC_io_way_T_3 ? abc_mshrs_0_io_status_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _sinkC_io_way_T_15 = _sinkC_io_way_T_5 ? abc_mshrs_1_io_status_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _sinkC_io_way_T_16 = _sinkC_io_way_T_7 ? abc_mshrs_2_io_status_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _sinkC_io_way_T_17 = _sinkC_io_way_T_9 ? abc_mshrs_3_io_status_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _sinkC_io_way_T_18 = _sinkC_io_way_T_11 ? abc_mshrs_4_io_status_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _sinkC_io_way_T_19 = _sinkC_io_way_T_13 ? abc_mshrs_5_io_status_bits_way : 3'h0; // @[Mux.scala 27:72]
  wire [2:0] _sinkC_io_way_T_20 = _sinkC_io_way_T_14 | _sinkC_io_way_T_15; // @[Mux.scala 27:72]
  wire [2:0] _sinkC_io_way_T_21 = _sinkC_io_way_T_20 | _sinkC_io_way_T_16; // @[Mux.scala 27:72]
  wire [2:0] _sinkC_io_way_T_22 = _sinkC_io_way_T_21 | _sinkC_io_way_T_17; // @[Mux.scala 27:72]
  wire [2:0] _sinkC_io_way_T_23 = _sinkC_io_way_T_22 | _sinkC_io_way_T_18; // @[Mux.scala 27:72]
  wire [2:0] _sinkC_io_way_T_24 = _sinkC_io_way_T_23 | _sinkC_io_way_T_19; // @[Mux.scala 27:72]
  wire [2:0] _sinkD_io_way_WIRE_0 = abc_mshrs_0_io_status_bits_way; // @[Scheduler.scala 375:22 Scheduler.scala 375:22]
  wire [2:0] _sinkD_io_way_WIRE_1 = abc_mshrs_1_io_status_bits_way; // @[Scheduler.scala 375:22 Scheduler.scala 375:22]
  wire [2:0] _GEN_142 = 3'h1 == sinkD_io_source ? _sinkD_io_way_WIRE_1 : _sinkD_io_way_WIRE_0; // @[Scheduler.scala 375:16 Scheduler.scala 375:16]
  wire [2:0] _sinkD_io_way_WIRE_2 = abc_mshrs_2_io_status_bits_way; // @[Scheduler.scala 375:22 Scheduler.scala 375:22]
  wire [2:0] _GEN_143 = 3'h2 == sinkD_io_source ? _sinkD_io_way_WIRE_2 : _GEN_142; // @[Scheduler.scala 375:16 Scheduler.scala 375:16]
  wire [2:0] _sinkD_io_way_WIRE_3 = abc_mshrs_3_io_status_bits_way; // @[Scheduler.scala 375:22 Scheduler.scala 375:22]
  wire [2:0] _GEN_144 = 3'h3 == sinkD_io_source ? _sinkD_io_way_WIRE_3 : _GEN_143; // @[Scheduler.scala 375:16 Scheduler.scala 375:16]
  wire [2:0] _sinkD_io_way_WIRE_4 = abc_mshrs_4_io_status_bits_way; // @[Scheduler.scala 375:22 Scheduler.scala 375:22]
  wire [2:0] _GEN_145 = 3'h4 == sinkD_io_source ? _sinkD_io_way_WIRE_4 : _GEN_144; // @[Scheduler.scala 375:16 Scheduler.scala 375:16]
  wire [2:0] _sinkD_io_way_WIRE_5 = abc_mshrs_5_io_status_bits_way; // @[Scheduler.scala 375:22 Scheduler.scala 375:22]
  wire [2:0] _GEN_146 = 3'h5 == sinkD_io_source ? _sinkD_io_way_WIRE_5 : _GEN_145; // @[Scheduler.scala 375:16 Scheduler.scala 375:16]
  wire [2:0] _sinkD_io_way_WIRE_6 = bc_mshr_io_status_bits_way; // @[Scheduler.scala 375:22 Scheduler.scala 375:22]
  wire [2:0] _GEN_147 = 3'h6 == sinkD_io_source ? _sinkD_io_way_WIRE_6 : _GEN_146; // @[Scheduler.scala 375:16 Scheduler.scala 375:16]
  wire [2:0] _sinkD_io_way_WIRE_7 = c_mshr_io_status_bits_way; // @[Scheduler.scala 375:22 Scheduler.scala 375:22]
  wire [9:0] _sinkD_io_set_WIRE_0 = abc_mshrs_0_io_status_bits_set; // @[Scheduler.scala 376:22 Scheduler.scala 376:22]
  wire [9:0] _sinkD_io_set_WIRE_1 = abc_mshrs_1_io_status_bits_set; // @[Scheduler.scala 376:22 Scheduler.scala 376:22]
  wire [9:0] _GEN_150 = 3'h1 == sinkD_io_source ? _sinkD_io_set_WIRE_1 : _sinkD_io_set_WIRE_0; // @[Scheduler.scala 376:16 Scheduler.scala 376:16]
  wire [9:0] _sinkD_io_set_WIRE_2 = abc_mshrs_2_io_status_bits_set; // @[Scheduler.scala 376:22 Scheduler.scala 376:22]
  wire [9:0] _GEN_151 = 3'h2 == sinkD_io_source ? _sinkD_io_set_WIRE_2 : _GEN_150; // @[Scheduler.scala 376:16 Scheduler.scala 376:16]
  wire [9:0] _sinkD_io_set_WIRE_3 = abc_mshrs_3_io_status_bits_set; // @[Scheduler.scala 376:22 Scheduler.scala 376:22]
  wire [9:0] _GEN_152 = 3'h3 == sinkD_io_source ? _sinkD_io_set_WIRE_3 : _GEN_151; // @[Scheduler.scala 376:16 Scheduler.scala 376:16]
  wire [9:0] _sinkD_io_set_WIRE_4 = abc_mshrs_4_io_status_bits_set; // @[Scheduler.scala 376:22 Scheduler.scala 376:22]
  wire [9:0] _GEN_153 = 3'h4 == sinkD_io_source ? _sinkD_io_set_WIRE_4 : _GEN_152; // @[Scheduler.scala 376:16 Scheduler.scala 376:16]
  wire [9:0] _sinkD_io_set_WIRE_5 = abc_mshrs_5_io_status_bits_set; // @[Scheduler.scala 376:22 Scheduler.scala 376:22]
  wire [9:0] _GEN_154 = 3'h5 == sinkD_io_source ? _sinkD_io_set_WIRE_5 : _GEN_153; // @[Scheduler.scala 376:16 Scheduler.scala 376:16]
  wire [9:0] _sinkD_io_set_WIRE_6 = bc_mshr_io_status_bits_set; // @[Scheduler.scala 376:22 Scheduler.scala 376:22]
  wire [9:0] _GEN_155 = 3'h6 == sinkD_io_source ? _sinkD_io_set_WIRE_6 : _GEN_154; // @[Scheduler.scala 376:16 Scheduler.scala 376:16]
  wire [9:0] _sinkD_io_set_WIRE_7 = c_mshr_io_status_bits_set; // @[Scheduler.scala 376:22 Scheduler.scala 376:22]
  reg  bankedStore_io_side_adr_valid_REG; // @[Scheduler.scala 403:43]
  reg [2:0] bankedStore_io_side_adr_bits_r_way; // @[Reg.scala 15:16]
  reg [9:0] bankedStore_io_side_adr_bits_r_set; // @[Reg.scala 15:16]
  reg [1:0] bankedStore_io_side_adr_bits_r_beat; // @[Reg.scala 15:16]
  reg [1:0] bankedStore_io_side_adr_bits_r_mask; // @[Reg.scala 15:16]
  reg  bankedStore_io_side_adr_bits_r_write; // @[Reg.scala 15:16]
  reg [127:0] bankedStore_io_side_wdat_r_data; // @[Reg.scala 15:16]
  reg  bankedStore_io_side_wdat_r_poison; // @[Reg.scala 15:16]
  reg [7:0] REG__0; // @[Scheduler.scala 410:32]
  reg [7:0] REG__1; // @[Scheduler.scala 410:32]
  reg [7:0] REG__2; // @[Scheduler.scala 410:32]
  reg [7:0] REG__3; // @[Scheduler.scala 410:32]
  reg [7:0] REG__4; // @[Scheduler.scala 410:32]
  reg [7:0] REG__5; // @[Scheduler.scala 410:32]
  reg [7:0] REG__6; // @[Scheduler.scala 410:32]
  reg [7:0] REG__7; // @[Scheduler.scala 410:32]
  reg [7:0] REG__8; // @[Scheduler.scala 410:32]
  reg [7:0] REG__9; // @[Scheduler.scala 410:32]
  reg [7:0] REG__10; // @[Scheduler.scala 410:32]
  reg [7:0] REG__11; // @[Scheduler.scala 410:32]
  reg [7:0] REG__12; // @[Scheduler.scala 410:32]
  reg [7:0] REG__13; // @[Scheduler.scala 410:32]
  reg [10:0] REG_1_0; // @[Scheduler.scala 411:32]
  reg [10:0] REG_1_1; // @[Scheduler.scala 411:32]
  reg [10:0] REG_1_2; // @[Scheduler.scala 411:32]
  reg [10:0] REG_1_3; // @[Scheduler.scala 411:32]
  reg [10:0] REG_1_4; // @[Scheduler.scala 411:32]
  reg [10:0] REG_1_5; // @[Scheduler.scala 411:32]
  reg [10:0] REG_1_6; // @[Scheduler.scala 411:32]
  reg [10:0] REG_1_7; // @[Scheduler.scala 411:32]
  reg [10:0] REG_1_8; // @[Scheduler.scala 411:32]
  reg [10:0] REG_1_9; // @[Scheduler.scala 411:32]
  reg [10:0] REG_1_10; // @[Scheduler.scala 411:32]
  reg [10:0] REG_1_11; // @[Scheduler.scala 411:32]
  reg [10:0] REG_1_12; // @[Scheduler.scala 411:32]
  reg [10:0] REG_1_13; // @[Scheduler.scala 411:32]
  reg  injectValid; // @[Scheduler.scala 428:28]
  reg  injectBits_dir; // @[Scheduler.scala 429:24]
  reg [7:0] injectBits_bit; // @[Scheduler.scala 429:24]
  wire  _injectReady_T = injectBits_dir ? directory_io_error_ready : bankedStore_io_error_ready; // @[Scheduler.scala 430:39]
  wire  injectReady = injectValid & _injectReady_T; // @[Scheduler.scala 430:33]
  wire  _GEN_171 = io_error_valid | injectValid; // @[Scheduler.scala 432:25 Scheduler.scala 433:17 Scheduler.scala 428:28]
  reg  io_error_ready_REG; // @[Scheduler.scala 440:28]
  RHEA__SourceA sourceA ( // @[Scheduler.scala 74:23]
    .rf_reset(sourceA_rf_reset),
    .clock(sourceA_clock),
    .reset(sourceA_reset),
    .io_req_ready(sourceA_io_req_ready),
    .io_req_valid(sourceA_io_req_valid),
    .io_req_bits_tag(sourceA_io_req_bits_tag),
    .io_req_bits_set(sourceA_io_req_bits_set),
    .io_req_bits_param(sourceA_io_req_bits_param),
    .io_req_bits_source(sourceA_io_req_bits_source),
    .io_req_bits_block(sourceA_io_req_bits_block),
    .io_a_ready(sourceA_io_a_ready),
    .io_a_valid(sourceA_io_a_valid),
    .io_a_bits_opcode(sourceA_io_a_bits_opcode),
    .io_a_bits_param(sourceA_io_a_bits_param),
    .io_a_bits_source(sourceA_io_a_bits_source),
    .io_a_bits_address(sourceA_io_a_bits_address)
  );
  RHEA__SourceB sourceB ( // @[Scheduler.scala 75:23]
    .rf_reset(sourceB_rf_reset),
    .clock(sourceB_clock),
    .reset(sourceB_reset),
    .io_req_ready(sourceB_io_req_ready),
    .io_req_valid(sourceB_io_req_valid),
    .io_req_bits_param(sourceB_io_req_bits_param),
    .io_req_bits_tag(sourceB_io_req_bits_tag),
    .io_req_bits_set(sourceB_io_req_bits_set),
    .io_req_bits_clients(sourceB_io_req_bits_clients),
    .io_b_ready(sourceB_io_b_ready),
    .io_b_valid(sourceB_io_b_valid),
    .io_b_bits_param(sourceB_io_b_bits_param),
    .io_b_bits_source(sourceB_io_b_bits_source),
    .io_b_bits_address(sourceB_io_b_bits_address)
  );
  RHEA__SourceC sourceC ( // @[Scheduler.scala 76:23]
    .rf_reset(sourceC_rf_reset),
    .clock(sourceC_clock),
    .reset(sourceC_reset),
    .io_req_ready(sourceC_io_req_ready),
    .io_req_valid(sourceC_io_req_valid),
    .io_req_bits_opcode(sourceC_io_req_bits_opcode),
    .io_req_bits_param(sourceC_io_req_bits_param),
    .io_req_bits_source(sourceC_io_req_bits_source),
    .io_req_bits_tag(sourceC_io_req_bits_tag),
    .io_req_bits_set(sourceC_io_req_bits_set),
    .io_req_bits_way(sourceC_io_req_bits_way),
    .io_req_bits_dirty(sourceC_io_req_bits_dirty),
    .io_c_ready(sourceC_io_c_ready),
    .io_c_valid(sourceC_io_c_valid),
    .io_c_bits_opcode(sourceC_io_c_bits_opcode),
    .io_c_bits_param(sourceC_io_c_bits_param),
    .io_c_bits_source(sourceC_io_c_bits_source),
    .io_c_bits_address(sourceC_io_c_bits_address),
    .io_c_bits_data(sourceC_io_c_bits_data),
    .io_bs_adr_ready(sourceC_io_bs_adr_ready),
    .io_bs_adr_valid(sourceC_io_bs_adr_valid),
    .io_bs_adr_bits_way(sourceC_io_bs_adr_bits_way),
    .io_bs_adr_bits_set(sourceC_io_bs_adr_bits_set),
    .io_bs_adr_bits_beat(sourceC_io_bs_adr_bits_beat),
    .io_bs_dat_data(sourceC_io_bs_dat_data),
    .io_evict_req_set(sourceC_io_evict_req_set),
    .io_evict_req_way(sourceC_io_evict_req_way),
    .io_evict_safe(sourceC_io_evict_safe),
    .io_corrected_valid(sourceC_io_corrected_valid),
    .io_uncorrected_valid(sourceC_io_uncorrected_valid)
  );
  RHEA__SourceD sourceD ( // @[Scheduler.scala 77:23]
    .rf_reset(sourceD_rf_reset),
    .clock(sourceD_clock),
    .reset(sourceD_reset),
    .io_req_ready(sourceD_io_req_ready),
    .io_req_valid(sourceD_io_req_valid),
    .io_req_bits_prio_0(sourceD_io_req_bits_prio_0),
    .io_req_bits_prio_2(sourceD_io_req_bits_prio_2),
    .io_req_bits_opcode(sourceD_io_req_bits_opcode),
    .io_req_bits_param(sourceD_io_req_bits_param),
    .io_req_bits_size(sourceD_io_req_bits_size),
    .io_req_bits_source(sourceD_io_req_bits_source),
    .io_req_bits_offset(sourceD_io_req_bits_offset),
    .io_req_bits_put(sourceD_io_req_bits_put),
    .io_req_bits_set(sourceD_io_req_bits_set),
    .io_req_bits_sink(sourceD_io_req_bits_sink),
    .io_req_bits_way(sourceD_io_req_bits_way),
    .io_req_bits_bad(sourceD_io_req_bits_bad),
    .io_req_bits_user_hit_cache(sourceD_io_req_bits_user_hit_cache),
    .io_d_ready(sourceD_io_d_ready),
    .io_d_valid(sourceD_io_d_valid),
    .io_d_bits_opcode(sourceD_io_d_bits_opcode),
    .io_d_bits_param(sourceD_io_d_bits_param),
    .io_d_bits_size(sourceD_io_d_bits_size),
    .io_d_bits_source(sourceD_io_d_bits_source),
    .io_d_bits_sink(sourceD_io_d_bits_sink),
    .io_d_bits_denied(sourceD_io_d_bits_denied),
    .io_d_bits_data(sourceD_io_d_bits_data),
    .io_d_bits_corrupt(sourceD_io_d_bits_corrupt),
    .io_pb_pop_ready(sourceD_io_pb_pop_ready),
    .io_pb_pop_valid(sourceD_io_pb_pop_valid),
    .io_pb_pop_bits_index(sourceD_io_pb_pop_bits_index),
    .io_pb_pop_bits_last(sourceD_io_pb_pop_bits_last),
    .io_pb_beat_data(sourceD_io_pb_beat_data),
    .io_pb_beat_mask(sourceD_io_pb_beat_mask),
    .io_pb_beat_corrupt(sourceD_io_pb_beat_corrupt),
    .io_rel_pop_ready(sourceD_io_rel_pop_ready),
    .io_rel_pop_valid(sourceD_io_rel_pop_valid),
    .io_rel_pop_bits_index(sourceD_io_rel_pop_bits_index),
    .io_rel_pop_bits_last(sourceD_io_rel_pop_bits_last),
    .io_rel_beat_data(sourceD_io_rel_beat_data),
    .io_rel_beat_corrupt(sourceD_io_rel_beat_corrupt),
    .io_bs_radr_ready(sourceD_io_bs_radr_ready),
    .io_bs_radr_valid(sourceD_io_bs_radr_valid),
    .io_bs_radr_bits_way(sourceD_io_bs_radr_bits_way),
    .io_bs_radr_bits_set(sourceD_io_bs_radr_bits_set),
    .io_bs_radr_bits_beat(sourceD_io_bs_radr_bits_beat),
    .io_bs_radr_bits_mask(sourceD_io_bs_radr_bits_mask),
    .io_bs_rdat_data(sourceD_io_bs_rdat_data),
    .io_bs_wadr_ready(sourceD_io_bs_wadr_ready),
    .io_bs_wadr_valid(sourceD_io_bs_wadr_valid),
    .io_bs_wadr_bits_way(sourceD_io_bs_wadr_bits_way),
    .io_bs_wadr_bits_set(sourceD_io_bs_wadr_bits_set),
    .io_bs_wadr_bits_beat(sourceD_io_bs_wadr_bits_beat),
    .io_bs_wadr_bits_mask(sourceD_io_bs_wadr_bits_mask),
    .io_bs_wdat_data(sourceD_io_bs_wdat_data),
    .io_bs_wdat_poison(sourceD_io_bs_wdat_poison),
    .io_evict_req_set(sourceD_io_evict_req_set),
    .io_evict_req_way(sourceD_io_evict_req_way),
    .io_evict_safe(sourceD_io_evict_safe),
    .io_grant_req_set(sourceD_io_grant_req_set),
    .io_grant_req_way(sourceD_io_grant_req_way),
    .io_grant_safe(sourceD_io_grant_safe),
    .io_corrected_valid(sourceD_io_corrected_valid),
    .io_uncorrected_valid(sourceD_io_uncorrected_valid)
  );
  RHEA__SourceE sourceE ( // @[Scheduler.scala 78:23]
    .rf_reset(sourceE_rf_reset),
    .clock(sourceE_clock),
    .reset(sourceE_reset),
    .io_req_ready(sourceE_io_req_ready),
    .io_req_valid(sourceE_io_req_valid),
    .io_req_bits_sink(sourceE_io_req_bits_sink),
    .io_e_valid(sourceE_io_e_valid),
    .io_e_bits_sink(sourceE_io_e_bits_sink)
  );
  RHEA__SourceX sourceX ( // @[Scheduler.scala 79:23]
    .clock(sourceX_clock),
    .reset(sourceX_reset),
    .io_req_ready(sourceX_io_req_ready),
    .io_req_valid(sourceX_io_req_valid),
    .io_x_ready(sourceX_io_x_ready),
    .io_x_valid(sourceX_io_x_valid)
  );
  RHEA__SinkA sinkA ( // @[Scheduler.scala 88:21]
    .rf_reset(sinkA_rf_reset),
    .clock(sinkA_clock),
    .reset(sinkA_reset),
    .io_req_ready(sinkA_io_req_ready),
    .io_req_valid(sinkA_io_req_valid),
    .io_req_bits_opcode(sinkA_io_req_bits_opcode),
    .io_req_bits_param(sinkA_io_req_bits_param),
    .io_req_bits_size(sinkA_io_req_bits_size),
    .io_req_bits_source(sinkA_io_req_bits_source),
    .io_req_bits_tag(sinkA_io_req_bits_tag),
    .io_req_bits_offset(sinkA_io_req_bits_offset),
    .io_req_bits_put(sinkA_io_req_bits_put),
    .io_req_bits_set(sinkA_io_req_bits_set),
    .io_a_ready(sinkA_io_a_ready),
    .io_a_valid(sinkA_io_a_valid),
    .io_a_bits_opcode(sinkA_io_a_bits_opcode),
    .io_a_bits_param(sinkA_io_a_bits_param),
    .io_a_bits_size(sinkA_io_a_bits_size),
    .io_a_bits_source(sinkA_io_a_bits_source),
    .io_a_bits_address(sinkA_io_a_bits_address),
    .io_a_bits_mask(sinkA_io_a_bits_mask),
    .io_a_bits_data(sinkA_io_a_bits_data),
    .io_a_bits_corrupt(sinkA_io_a_bits_corrupt),
    .io_pb_pop_ready(sinkA_io_pb_pop_ready),
    .io_pb_pop_valid(sinkA_io_pb_pop_valid),
    .io_pb_pop_bits_index(sinkA_io_pb_pop_bits_index),
    .io_pb_pop_bits_last(sinkA_io_pb_pop_bits_last),
    .io_pb_beat_data(sinkA_io_pb_beat_data),
    .io_pb_beat_mask(sinkA_io_pb_beat_mask),
    .io_pb_beat_corrupt(sinkA_io_pb_beat_corrupt)
  );
  RHEA__SinkC sinkC ( // @[Scheduler.scala 90:21]
    .rf_reset(sinkC_rf_reset),
    .clock(sinkC_clock),
    .reset(sinkC_reset),
    .io_req_ready(sinkC_io_req_ready),
    .io_req_valid(sinkC_io_req_valid),
    .io_req_bits_opcode(sinkC_io_req_bits_opcode),
    .io_req_bits_param(sinkC_io_req_bits_param),
    .io_req_bits_size(sinkC_io_req_bits_size),
    .io_req_bits_source(sinkC_io_req_bits_source),
    .io_req_bits_tag(sinkC_io_req_bits_tag),
    .io_req_bits_offset(sinkC_io_req_bits_offset),
    .io_req_bits_put(sinkC_io_req_bits_put),
    .io_req_bits_set(sinkC_io_req_bits_set),
    .io_resp_valid(sinkC_io_resp_valid),
    .io_resp_bits_last(sinkC_io_resp_bits_last),
    .io_resp_bits_set(sinkC_io_resp_bits_set),
    .io_resp_bits_tag(sinkC_io_resp_bits_tag),
    .io_resp_bits_source(sinkC_io_resp_bits_source),
    .io_resp_bits_param(sinkC_io_resp_bits_param),
    .io_resp_bits_data(sinkC_io_resp_bits_data),
    .io_c_ready(sinkC_io_c_ready),
    .io_c_valid(sinkC_io_c_valid),
    .io_c_bits_opcode(sinkC_io_c_bits_opcode),
    .io_c_bits_param(sinkC_io_c_bits_param),
    .io_c_bits_size(sinkC_io_c_bits_size),
    .io_c_bits_source(sinkC_io_c_bits_source),
    .io_c_bits_address(sinkC_io_c_bits_address),
    .io_c_bits_data(sinkC_io_c_bits_data),
    .io_c_bits_corrupt(sinkC_io_c_bits_corrupt),
    .io_set(sinkC_io_set),
    .io_way(sinkC_io_way),
    .io_bs_adr_ready(sinkC_io_bs_adr_ready),
    .io_bs_adr_valid(sinkC_io_bs_adr_valid),
    .io_bs_adr_bits_noop(sinkC_io_bs_adr_bits_noop),
    .io_bs_adr_bits_way(sinkC_io_bs_adr_bits_way),
    .io_bs_adr_bits_set(sinkC_io_bs_adr_bits_set),
    .io_bs_adr_bits_beat(sinkC_io_bs_adr_bits_beat),
    .io_bs_dat_data(sinkC_io_bs_dat_data),
    .io_bs_dat_poison(sinkC_io_bs_dat_poison),
    .io_rel_pop_ready(sinkC_io_rel_pop_ready),
    .io_rel_pop_valid(sinkC_io_rel_pop_valid),
    .io_rel_pop_bits_index(sinkC_io_rel_pop_bits_index),
    .io_rel_pop_bits_last(sinkC_io_rel_pop_bits_last),
    .io_rel_beat_data(sinkC_io_rel_beat_data),
    .io_rel_beat_corrupt(sinkC_io_rel_beat_corrupt)
  );
  RHEA__SinkD sinkD ( // @[Scheduler.scala 91:21]
    .rf_reset(sinkD_rf_reset),
    .clock(sinkD_clock),
    .reset(sinkD_reset),
    .io_resp_valid(sinkD_io_resp_valid),
    .io_resp_bits_last(sinkD_io_resp_bits_last),
    .io_resp_bits_opcode(sinkD_io_resp_bits_opcode),
    .io_resp_bits_param(sinkD_io_resp_bits_param),
    .io_resp_bits_source(sinkD_io_resp_bits_source),
    .io_resp_bits_sink(sinkD_io_resp_bits_sink),
    .io_resp_bits_denied(sinkD_io_resp_bits_denied),
    .io_d_ready(sinkD_io_d_ready),
    .io_d_valid(sinkD_io_d_valid),
    .io_d_bits_opcode(sinkD_io_d_bits_opcode),
    .io_d_bits_param(sinkD_io_d_bits_param),
    .io_d_bits_size(sinkD_io_d_bits_size),
    .io_d_bits_source(sinkD_io_d_bits_source),
    .io_d_bits_sink(sinkD_io_d_bits_sink),
    .io_d_bits_denied(sinkD_io_d_bits_denied),
    .io_d_bits_data(sinkD_io_d_bits_data),
    .io_d_bits_corrupt(sinkD_io_d_bits_corrupt),
    .io_source(sinkD_io_source),
    .io_way(sinkD_io_way),
    .io_set(sinkD_io_set),
    .io_bs_adr_ready(sinkD_io_bs_adr_ready),
    .io_bs_adr_valid(sinkD_io_bs_adr_valid),
    .io_bs_adr_bits_noop(sinkD_io_bs_adr_bits_noop),
    .io_bs_adr_bits_way(sinkD_io_bs_adr_bits_way),
    .io_bs_adr_bits_set(sinkD_io_bs_adr_bits_set),
    .io_bs_adr_bits_beat(sinkD_io_bs_adr_bits_beat),
    .io_bs_dat_data(sinkD_io_bs_dat_data),
    .io_grant_req_set(sinkD_io_grant_req_set),
    .io_grant_req_way(sinkD_io_grant_req_way),
    .io_grant_safe(sinkD_io_grant_safe)
  );
  RHEA__SinkE sinkE ( // @[Scheduler.scala 92:21]
    .io_resp_valid(sinkE_io_resp_valid),
    .io_resp_bits_sink(sinkE_io_resp_bits_sink),
    .io_e_valid(sinkE_io_e_valid),
    .io_e_bits_sink(sinkE_io_e_bits_sink)
  );
  RHEA__SinkX sinkX ( // @[Scheduler.scala 93:21]
    .rf_reset(sinkX_rf_reset),
    .clock(sinkX_clock),
    .reset(sinkX_reset),
    .io_req_ready(sinkX_io_req_ready),
    .io_req_valid(sinkX_io_req_valid),
    .io_req_bits_tag(sinkX_io_req_bits_tag),
    .io_req_bits_set(sinkX_io_req_bits_set),
    .io_x_ready(sinkX_io_x_ready),
    .io_x_valid(sinkX_io_x_valid),
    .io_x_bits_address(sinkX_io_x_bits_address)
  );
  RHEA__Directory directory ( // @[Scheduler.scala 102:25]
    .rf_reset(directory_rf_reset),
    .clock(directory_clock),
    .reset(directory_reset),
    .io_write_ready(directory_io_write_ready),
    .io_write_valid(directory_io_write_valid),
    .io_write_bits_set(directory_io_write_bits_set),
    .io_write_bits_way(directory_io_write_bits_way),
    .io_write_bits_data_dirty(directory_io_write_bits_data_dirty),
    .io_write_bits_data_state(directory_io_write_bits_data_state),
    .io_write_bits_data_clients(directory_io_write_bits_data_clients),
    .io_write_bits_data_tag(directory_io_write_bits_data_tag),
    .io_read_valid(directory_io_read_valid),
    .io_read_bits_source(directory_io_read_bits_source),
    .io_read_bits_set(directory_io_read_bits_set),
    .io_read_bits_tag(directory_io_read_bits_tag),
    .io_result_bits_dirty(directory_io_result_bits_dirty),
    .io_result_bits_state(directory_io_result_bits_state),
    .io_result_bits_clients(directory_io_result_bits_clients),
    .io_result_bits_tag(directory_io_result_bits_tag),
    .io_result_bits_hit(directory_io_result_bits_hit),
    .io_result_bits_way(directory_io_result_bits_way),
    .io_ready(directory_io_ready),
    .io_ways_0(directory_io_ways_0),
    .io_ways_1(directory_io_ways_1),
    .io_ways_2(directory_io_ways_2),
    .io_ways_3(directory_io_ways_3),
    .io_ways_4(directory_io_ways_4),
    .io_ways_5(directory_io_ways_5),
    .io_ways_6(directory_io_ways_6),
    .io_ways_7(directory_io_ways_7),
    .io_ways_8(directory_io_ways_8),
    .io_ways_9(directory_io_ways_9),
    .io_ways_10(directory_io_ways_10),
    .io_ways_11(directory_io_ways_11),
    .io_ways_12(directory_io_ways_12),
    .io_ways_13(directory_io_ways_13),
    .io_divs_0(directory_io_divs_0),
    .io_divs_1(directory_io_divs_1),
    .io_divs_2(directory_io_divs_2),
    .io_divs_3(directory_io_divs_3),
    .io_divs_4(directory_io_divs_4),
    .io_divs_5(directory_io_divs_5),
    .io_divs_6(directory_io_divs_6),
    .io_divs_7(directory_io_divs_7),
    .io_divs_8(directory_io_divs_8),
    .io_divs_9(directory_io_divs_9),
    .io_divs_10(directory_io_divs_10),
    .io_divs_11(directory_io_divs_11),
    .io_divs_12(directory_io_divs_12),
    .io_divs_13(directory_io_divs_13),
    .io_corrected_valid(directory_io_corrected_valid),
    .io_error_ready(directory_io_error_ready),
    .io_error_valid(directory_io_error_valid),
    .io_error_bits_bit(directory_io_error_bits_bit)
  );
  RHEA__BankedStore bankedStore ( // @[Scheduler.scala 103:27]
    .rf_reset(bankedStore_rf_reset),
    .clock(bankedStore_clock),
    .reset(bankedStore_reset),
    .io_side_adr_ready(bankedStore_io_side_adr_ready),
    .io_side_adr_valid(bankedStore_io_side_adr_valid),
    .io_side_adr_bits_way(bankedStore_io_side_adr_bits_way),
    .io_side_adr_bits_set(bankedStore_io_side_adr_bits_set),
    .io_side_adr_bits_beat(bankedStore_io_side_adr_bits_beat),
    .io_side_adr_bits_mask(bankedStore_io_side_adr_bits_mask),
    .io_side_adr_bits_write(bankedStore_io_side_adr_bits_write),
    .io_side_wdat_data(bankedStore_io_side_wdat_data),
    .io_side_wdat_poison(bankedStore_io_side_wdat_poison),
    .io_side_rdat_data(bankedStore_io_side_rdat_data),
    .io_sinkC_adr_ready(bankedStore_io_sinkC_adr_ready),
    .io_sinkC_adr_valid(bankedStore_io_sinkC_adr_valid),
    .io_sinkC_adr_bits_noop(bankedStore_io_sinkC_adr_bits_noop),
    .io_sinkC_adr_bits_way(bankedStore_io_sinkC_adr_bits_way),
    .io_sinkC_adr_bits_set(bankedStore_io_sinkC_adr_bits_set),
    .io_sinkC_adr_bits_beat(bankedStore_io_sinkC_adr_bits_beat),
    .io_sinkC_dat_data(bankedStore_io_sinkC_dat_data),
    .io_sinkC_dat_poison(bankedStore_io_sinkC_dat_poison),
    .io_sinkD_adr_ready(bankedStore_io_sinkD_adr_ready),
    .io_sinkD_adr_valid(bankedStore_io_sinkD_adr_valid),
    .io_sinkD_adr_bits_noop(bankedStore_io_sinkD_adr_bits_noop),
    .io_sinkD_adr_bits_way(bankedStore_io_sinkD_adr_bits_way),
    .io_sinkD_adr_bits_set(bankedStore_io_sinkD_adr_bits_set),
    .io_sinkD_adr_bits_beat(bankedStore_io_sinkD_adr_bits_beat),
    .io_sinkD_dat_data(bankedStore_io_sinkD_dat_data),
    .io_sourceC_adr_ready(bankedStore_io_sourceC_adr_ready),
    .io_sourceC_adr_valid(bankedStore_io_sourceC_adr_valid),
    .io_sourceC_adr_bits_way(bankedStore_io_sourceC_adr_bits_way),
    .io_sourceC_adr_bits_set(bankedStore_io_sourceC_adr_bits_set),
    .io_sourceC_adr_bits_beat(bankedStore_io_sourceC_adr_bits_beat),
    .io_sourceC_dat_data(bankedStore_io_sourceC_dat_data),
    .io_sourceD_radr_ready(bankedStore_io_sourceD_radr_ready),
    .io_sourceD_radr_valid(bankedStore_io_sourceD_radr_valid),
    .io_sourceD_radr_bits_way(bankedStore_io_sourceD_radr_bits_way),
    .io_sourceD_radr_bits_set(bankedStore_io_sourceD_radr_bits_set),
    .io_sourceD_radr_bits_beat(bankedStore_io_sourceD_radr_bits_beat),
    .io_sourceD_radr_bits_mask(bankedStore_io_sourceD_radr_bits_mask),
    .io_sourceD_rdat_data(bankedStore_io_sourceD_rdat_data),
    .io_sourceD_wadr_ready(bankedStore_io_sourceD_wadr_ready),
    .io_sourceD_wadr_valid(bankedStore_io_sourceD_wadr_valid),
    .io_sourceD_wadr_bits_way(bankedStore_io_sourceD_wadr_bits_way),
    .io_sourceD_wadr_bits_set(bankedStore_io_sourceD_wadr_bits_set),
    .io_sourceD_wadr_bits_beat(bankedStore_io_sourceD_wadr_bits_beat),
    .io_sourceD_wadr_bits_mask(bankedStore_io_sourceD_wadr_bits_mask),
    .io_sourceD_wdat_data(bankedStore_io_sourceD_wdat_data),
    .io_sourceD_wdat_poison(bankedStore_io_sourceD_wdat_poison),
    .io_error_ready(bankedStore_io_error_ready),
    .io_error_valid(bankedStore_io_error_valid),
    .io_error_bits_bit(bankedStore_io_error_bits_bit)
  );
  RHEA__ListBuffer_2 requests ( // @[Scheduler.scala 104:24]
    .rf_reset(requests_rf_reset),
    .clock(requests_clock),
    .reset(requests_reset),
    .io_push_ready(requests_io_push_ready),
    .io_push_valid(requests_io_push_valid),
    .io_push_bits_index(requests_io_push_bits_index),
    .io_push_bits_data_prio_0(requests_io_push_bits_data_prio_0),
    .io_push_bits_data_prio_2(requests_io_push_bits_data_prio_2),
    .io_push_bits_data_control(requests_io_push_bits_data_control),
    .io_push_bits_data_opcode(requests_io_push_bits_data_opcode),
    .io_push_bits_data_param(requests_io_push_bits_data_param),
    .io_push_bits_data_size(requests_io_push_bits_data_size),
    .io_push_bits_data_source(requests_io_push_bits_data_source),
    .io_push_bits_data_tag(requests_io_push_bits_data_tag),
    .io_push_bits_data_offset(requests_io_push_bits_data_offset),
    .io_push_bits_data_put(requests_io_push_bits_data_put),
    .io_valid(requests_io_valid),
    .io_pop_valid(requests_io_pop_valid),
    .io_pop_bits(requests_io_pop_bits),
    .io_data_prio_0(requests_io_data_prio_0),
    .io_data_prio_2(requests_io_data_prio_2),
    .io_data_control(requests_io_data_control),
    .io_data_opcode(requests_io_data_opcode),
    .io_data_param(requests_io_data_param),
    .io_data_size(requests_io_data_size),
    .io_data_source(requests_io_data_source),
    .io_data_tag(requests_io_data_tag),
    .io_data_offset(requests_io_data_offset),
    .io_data_put(requests_io_data_put)
  );
  RHEA__MSHR abc_mshrs_0 ( // @[Scheduler.scala 105:48]
    .rf_reset(abc_mshrs_0_rf_reset),
    .clock(abc_mshrs_0_clock),
    .reset(abc_mshrs_0_reset),
    .io_allocate_valid(abc_mshrs_0_io_allocate_valid),
    .io_allocate_bits_prio_0(abc_mshrs_0_io_allocate_bits_prio_0),
    .io_allocate_bits_prio_2(abc_mshrs_0_io_allocate_bits_prio_2),
    .io_allocate_bits_control(abc_mshrs_0_io_allocate_bits_control),
    .io_allocate_bits_opcode(abc_mshrs_0_io_allocate_bits_opcode),
    .io_allocate_bits_param(abc_mshrs_0_io_allocate_bits_param),
    .io_allocate_bits_size(abc_mshrs_0_io_allocate_bits_size),
    .io_allocate_bits_source(abc_mshrs_0_io_allocate_bits_source),
    .io_allocate_bits_tag(abc_mshrs_0_io_allocate_bits_tag),
    .io_allocate_bits_offset(abc_mshrs_0_io_allocate_bits_offset),
    .io_allocate_bits_put(abc_mshrs_0_io_allocate_bits_put),
    .io_allocate_bits_set(abc_mshrs_0_io_allocate_bits_set),
    .io_allocate_bits_repeat(abc_mshrs_0_io_allocate_bits_repeat),
    .io_directory_valid(abc_mshrs_0_io_directory_valid),
    .io_directory_bits_dirty(abc_mshrs_0_io_directory_bits_dirty),
    .io_directory_bits_state(abc_mshrs_0_io_directory_bits_state),
    .io_directory_bits_clients(abc_mshrs_0_io_directory_bits_clients),
    .io_directory_bits_tag(abc_mshrs_0_io_directory_bits_tag),
    .io_directory_bits_hit(abc_mshrs_0_io_directory_bits_hit),
    .io_directory_bits_way(abc_mshrs_0_io_directory_bits_way),
    .io_status_valid(abc_mshrs_0_io_status_valid),
    .io_status_bits_set(abc_mshrs_0_io_status_bits_set),
    .io_status_bits_tag(abc_mshrs_0_io_status_bits_tag),
    .io_status_bits_way(abc_mshrs_0_io_status_bits_way),
    .io_status_bits_blockB(abc_mshrs_0_io_status_bits_blockB),
    .io_status_bits_nestB(abc_mshrs_0_io_status_bits_nestB),
    .io_status_bits_blockC(abc_mshrs_0_io_status_bits_blockC),
    .io_status_bits_nestC(abc_mshrs_0_io_status_bits_nestC),
    .io_schedule_ready(abc_mshrs_0_io_schedule_ready),
    .io_schedule_valid(abc_mshrs_0_io_schedule_valid),
    .io_schedule_bits_a_valid(abc_mshrs_0_io_schedule_bits_a_valid),
    .io_schedule_bits_a_bits_tag(abc_mshrs_0_io_schedule_bits_a_bits_tag),
    .io_schedule_bits_a_bits_set(abc_mshrs_0_io_schedule_bits_a_bits_set),
    .io_schedule_bits_a_bits_param(abc_mshrs_0_io_schedule_bits_a_bits_param),
    .io_schedule_bits_a_bits_block(abc_mshrs_0_io_schedule_bits_a_bits_block),
    .io_schedule_bits_b_valid(abc_mshrs_0_io_schedule_bits_b_valid),
    .io_schedule_bits_b_bits_param(abc_mshrs_0_io_schedule_bits_b_bits_param),
    .io_schedule_bits_b_bits_tag(abc_mshrs_0_io_schedule_bits_b_bits_tag),
    .io_schedule_bits_b_bits_set(abc_mshrs_0_io_schedule_bits_b_bits_set),
    .io_schedule_bits_b_bits_clients(abc_mshrs_0_io_schedule_bits_b_bits_clients),
    .io_schedule_bits_c_valid(abc_mshrs_0_io_schedule_bits_c_valid),
    .io_schedule_bits_c_bits_opcode(abc_mshrs_0_io_schedule_bits_c_bits_opcode),
    .io_schedule_bits_c_bits_param(abc_mshrs_0_io_schedule_bits_c_bits_param),
    .io_schedule_bits_c_bits_tag(abc_mshrs_0_io_schedule_bits_c_bits_tag),
    .io_schedule_bits_c_bits_set(abc_mshrs_0_io_schedule_bits_c_bits_set),
    .io_schedule_bits_c_bits_way(abc_mshrs_0_io_schedule_bits_c_bits_way),
    .io_schedule_bits_c_bits_dirty(abc_mshrs_0_io_schedule_bits_c_bits_dirty),
    .io_schedule_bits_d_valid(abc_mshrs_0_io_schedule_bits_d_valid),
    .io_schedule_bits_d_bits_prio_0(abc_mshrs_0_io_schedule_bits_d_bits_prio_0),
    .io_schedule_bits_d_bits_prio_2(abc_mshrs_0_io_schedule_bits_d_bits_prio_2),
    .io_schedule_bits_d_bits_opcode(abc_mshrs_0_io_schedule_bits_d_bits_opcode),
    .io_schedule_bits_d_bits_param(abc_mshrs_0_io_schedule_bits_d_bits_param),
    .io_schedule_bits_d_bits_size(abc_mshrs_0_io_schedule_bits_d_bits_size),
    .io_schedule_bits_d_bits_source(abc_mshrs_0_io_schedule_bits_d_bits_source),
    .io_schedule_bits_d_bits_offset(abc_mshrs_0_io_schedule_bits_d_bits_offset),
    .io_schedule_bits_d_bits_put(abc_mshrs_0_io_schedule_bits_d_bits_put),
    .io_schedule_bits_d_bits_set(abc_mshrs_0_io_schedule_bits_d_bits_set),
    .io_schedule_bits_d_bits_way(abc_mshrs_0_io_schedule_bits_d_bits_way),
    .io_schedule_bits_d_bits_bad(abc_mshrs_0_io_schedule_bits_d_bits_bad),
    .io_schedule_bits_d_bits_user_hit_cache(abc_mshrs_0_io_schedule_bits_d_bits_user_hit_cache),
    .io_schedule_bits_e_valid(abc_mshrs_0_io_schedule_bits_e_valid),
    .io_schedule_bits_e_bits_sink(abc_mshrs_0_io_schedule_bits_e_bits_sink),
    .io_schedule_bits_x_valid(abc_mshrs_0_io_schedule_bits_x_valid),
    .io_schedule_bits_dir_valid(abc_mshrs_0_io_schedule_bits_dir_valid),
    .io_schedule_bits_dir_bits_set(abc_mshrs_0_io_schedule_bits_dir_bits_set),
    .io_schedule_bits_dir_bits_way(abc_mshrs_0_io_schedule_bits_dir_bits_way),
    .io_schedule_bits_dir_bits_data_dirty(abc_mshrs_0_io_schedule_bits_dir_bits_data_dirty),
    .io_schedule_bits_dir_bits_data_state(abc_mshrs_0_io_schedule_bits_dir_bits_data_state),
    .io_schedule_bits_dir_bits_data_clients(abc_mshrs_0_io_schedule_bits_dir_bits_data_clients),
    .io_schedule_bits_dir_bits_data_tag(abc_mshrs_0_io_schedule_bits_dir_bits_data_tag),
    .io_schedule_bits_reload(abc_mshrs_0_io_schedule_bits_reload),
    .io_sinkc_valid(abc_mshrs_0_io_sinkc_valid),
    .io_sinkc_bits_last(abc_mshrs_0_io_sinkc_bits_last),
    .io_sinkc_bits_tag(abc_mshrs_0_io_sinkc_bits_tag),
    .io_sinkc_bits_source(abc_mshrs_0_io_sinkc_bits_source),
    .io_sinkc_bits_param(abc_mshrs_0_io_sinkc_bits_param),
    .io_sinkc_bits_data(abc_mshrs_0_io_sinkc_bits_data),
    .io_sinkd_valid(abc_mshrs_0_io_sinkd_valid),
    .io_sinkd_bits_last(abc_mshrs_0_io_sinkd_bits_last),
    .io_sinkd_bits_opcode(abc_mshrs_0_io_sinkd_bits_opcode),
    .io_sinkd_bits_param(abc_mshrs_0_io_sinkd_bits_param),
    .io_sinkd_bits_sink(abc_mshrs_0_io_sinkd_bits_sink),
    .io_sinkd_bits_denied(abc_mshrs_0_io_sinkd_bits_denied),
    .io_sinke_valid(abc_mshrs_0_io_sinke_valid),
    .io_nestedwb_set(abc_mshrs_0_io_nestedwb_set),
    .io_nestedwb_tag(abc_mshrs_0_io_nestedwb_tag),
    .io_nestedwb_b_toN(abc_mshrs_0_io_nestedwb_b_toN),
    .io_nestedwb_b_toB(abc_mshrs_0_io_nestedwb_b_toB),
    .io_nestedwb_b_clr_dirty(abc_mshrs_0_io_nestedwb_b_clr_dirty),
    .io_nestedwb_c_set_dirty(abc_mshrs_0_io_nestedwb_c_set_dirty)
  );
  RHEA__MSHR abc_mshrs_1 ( // @[Scheduler.scala 105:48]
    .rf_reset(abc_mshrs_1_rf_reset),
    .clock(abc_mshrs_1_clock),
    .reset(abc_mshrs_1_reset),
    .io_allocate_valid(abc_mshrs_1_io_allocate_valid),
    .io_allocate_bits_prio_0(abc_mshrs_1_io_allocate_bits_prio_0),
    .io_allocate_bits_prio_2(abc_mshrs_1_io_allocate_bits_prio_2),
    .io_allocate_bits_control(abc_mshrs_1_io_allocate_bits_control),
    .io_allocate_bits_opcode(abc_mshrs_1_io_allocate_bits_opcode),
    .io_allocate_bits_param(abc_mshrs_1_io_allocate_bits_param),
    .io_allocate_bits_size(abc_mshrs_1_io_allocate_bits_size),
    .io_allocate_bits_source(abc_mshrs_1_io_allocate_bits_source),
    .io_allocate_bits_tag(abc_mshrs_1_io_allocate_bits_tag),
    .io_allocate_bits_offset(abc_mshrs_1_io_allocate_bits_offset),
    .io_allocate_bits_put(abc_mshrs_1_io_allocate_bits_put),
    .io_allocate_bits_set(abc_mshrs_1_io_allocate_bits_set),
    .io_allocate_bits_repeat(abc_mshrs_1_io_allocate_bits_repeat),
    .io_directory_valid(abc_mshrs_1_io_directory_valid),
    .io_directory_bits_dirty(abc_mshrs_1_io_directory_bits_dirty),
    .io_directory_bits_state(abc_mshrs_1_io_directory_bits_state),
    .io_directory_bits_clients(abc_mshrs_1_io_directory_bits_clients),
    .io_directory_bits_tag(abc_mshrs_1_io_directory_bits_tag),
    .io_directory_bits_hit(abc_mshrs_1_io_directory_bits_hit),
    .io_directory_bits_way(abc_mshrs_1_io_directory_bits_way),
    .io_status_valid(abc_mshrs_1_io_status_valid),
    .io_status_bits_set(abc_mshrs_1_io_status_bits_set),
    .io_status_bits_tag(abc_mshrs_1_io_status_bits_tag),
    .io_status_bits_way(abc_mshrs_1_io_status_bits_way),
    .io_status_bits_blockB(abc_mshrs_1_io_status_bits_blockB),
    .io_status_bits_nestB(abc_mshrs_1_io_status_bits_nestB),
    .io_status_bits_blockC(abc_mshrs_1_io_status_bits_blockC),
    .io_status_bits_nestC(abc_mshrs_1_io_status_bits_nestC),
    .io_schedule_ready(abc_mshrs_1_io_schedule_ready),
    .io_schedule_valid(abc_mshrs_1_io_schedule_valid),
    .io_schedule_bits_a_valid(abc_mshrs_1_io_schedule_bits_a_valid),
    .io_schedule_bits_a_bits_tag(abc_mshrs_1_io_schedule_bits_a_bits_tag),
    .io_schedule_bits_a_bits_set(abc_mshrs_1_io_schedule_bits_a_bits_set),
    .io_schedule_bits_a_bits_param(abc_mshrs_1_io_schedule_bits_a_bits_param),
    .io_schedule_bits_a_bits_block(abc_mshrs_1_io_schedule_bits_a_bits_block),
    .io_schedule_bits_b_valid(abc_mshrs_1_io_schedule_bits_b_valid),
    .io_schedule_bits_b_bits_param(abc_mshrs_1_io_schedule_bits_b_bits_param),
    .io_schedule_bits_b_bits_tag(abc_mshrs_1_io_schedule_bits_b_bits_tag),
    .io_schedule_bits_b_bits_set(abc_mshrs_1_io_schedule_bits_b_bits_set),
    .io_schedule_bits_b_bits_clients(abc_mshrs_1_io_schedule_bits_b_bits_clients),
    .io_schedule_bits_c_valid(abc_mshrs_1_io_schedule_bits_c_valid),
    .io_schedule_bits_c_bits_opcode(abc_mshrs_1_io_schedule_bits_c_bits_opcode),
    .io_schedule_bits_c_bits_param(abc_mshrs_1_io_schedule_bits_c_bits_param),
    .io_schedule_bits_c_bits_tag(abc_mshrs_1_io_schedule_bits_c_bits_tag),
    .io_schedule_bits_c_bits_set(abc_mshrs_1_io_schedule_bits_c_bits_set),
    .io_schedule_bits_c_bits_way(abc_mshrs_1_io_schedule_bits_c_bits_way),
    .io_schedule_bits_c_bits_dirty(abc_mshrs_1_io_schedule_bits_c_bits_dirty),
    .io_schedule_bits_d_valid(abc_mshrs_1_io_schedule_bits_d_valid),
    .io_schedule_bits_d_bits_prio_0(abc_mshrs_1_io_schedule_bits_d_bits_prio_0),
    .io_schedule_bits_d_bits_prio_2(abc_mshrs_1_io_schedule_bits_d_bits_prio_2),
    .io_schedule_bits_d_bits_opcode(abc_mshrs_1_io_schedule_bits_d_bits_opcode),
    .io_schedule_bits_d_bits_param(abc_mshrs_1_io_schedule_bits_d_bits_param),
    .io_schedule_bits_d_bits_size(abc_mshrs_1_io_schedule_bits_d_bits_size),
    .io_schedule_bits_d_bits_source(abc_mshrs_1_io_schedule_bits_d_bits_source),
    .io_schedule_bits_d_bits_offset(abc_mshrs_1_io_schedule_bits_d_bits_offset),
    .io_schedule_bits_d_bits_put(abc_mshrs_1_io_schedule_bits_d_bits_put),
    .io_schedule_bits_d_bits_set(abc_mshrs_1_io_schedule_bits_d_bits_set),
    .io_schedule_bits_d_bits_way(abc_mshrs_1_io_schedule_bits_d_bits_way),
    .io_schedule_bits_d_bits_bad(abc_mshrs_1_io_schedule_bits_d_bits_bad),
    .io_schedule_bits_d_bits_user_hit_cache(abc_mshrs_1_io_schedule_bits_d_bits_user_hit_cache),
    .io_schedule_bits_e_valid(abc_mshrs_1_io_schedule_bits_e_valid),
    .io_schedule_bits_e_bits_sink(abc_mshrs_1_io_schedule_bits_e_bits_sink),
    .io_schedule_bits_x_valid(abc_mshrs_1_io_schedule_bits_x_valid),
    .io_schedule_bits_dir_valid(abc_mshrs_1_io_schedule_bits_dir_valid),
    .io_schedule_bits_dir_bits_set(abc_mshrs_1_io_schedule_bits_dir_bits_set),
    .io_schedule_bits_dir_bits_way(abc_mshrs_1_io_schedule_bits_dir_bits_way),
    .io_schedule_bits_dir_bits_data_dirty(abc_mshrs_1_io_schedule_bits_dir_bits_data_dirty),
    .io_schedule_bits_dir_bits_data_state(abc_mshrs_1_io_schedule_bits_dir_bits_data_state),
    .io_schedule_bits_dir_bits_data_clients(abc_mshrs_1_io_schedule_bits_dir_bits_data_clients),
    .io_schedule_bits_dir_bits_data_tag(abc_mshrs_1_io_schedule_bits_dir_bits_data_tag),
    .io_schedule_bits_reload(abc_mshrs_1_io_schedule_bits_reload),
    .io_sinkc_valid(abc_mshrs_1_io_sinkc_valid),
    .io_sinkc_bits_last(abc_mshrs_1_io_sinkc_bits_last),
    .io_sinkc_bits_tag(abc_mshrs_1_io_sinkc_bits_tag),
    .io_sinkc_bits_source(abc_mshrs_1_io_sinkc_bits_source),
    .io_sinkc_bits_param(abc_mshrs_1_io_sinkc_bits_param),
    .io_sinkc_bits_data(abc_mshrs_1_io_sinkc_bits_data),
    .io_sinkd_valid(abc_mshrs_1_io_sinkd_valid),
    .io_sinkd_bits_last(abc_mshrs_1_io_sinkd_bits_last),
    .io_sinkd_bits_opcode(abc_mshrs_1_io_sinkd_bits_opcode),
    .io_sinkd_bits_param(abc_mshrs_1_io_sinkd_bits_param),
    .io_sinkd_bits_sink(abc_mshrs_1_io_sinkd_bits_sink),
    .io_sinkd_bits_denied(abc_mshrs_1_io_sinkd_bits_denied),
    .io_sinke_valid(abc_mshrs_1_io_sinke_valid),
    .io_nestedwb_set(abc_mshrs_1_io_nestedwb_set),
    .io_nestedwb_tag(abc_mshrs_1_io_nestedwb_tag),
    .io_nestedwb_b_toN(abc_mshrs_1_io_nestedwb_b_toN),
    .io_nestedwb_b_toB(abc_mshrs_1_io_nestedwb_b_toB),
    .io_nestedwb_b_clr_dirty(abc_mshrs_1_io_nestedwb_b_clr_dirty),
    .io_nestedwb_c_set_dirty(abc_mshrs_1_io_nestedwb_c_set_dirty)
  );
  RHEA__MSHR abc_mshrs_2 ( // @[Scheduler.scala 105:48]
    .rf_reset(abc_mshrs_2_rf_reset),
    .clock(abc_mshrs_2_clock),
    .reset(abc_mshrs_2_reset),
    .io_allocate_valid(abc_mshrs_2_io_allocate_valid),
    .io_allocate_bits_prio_0(abc_mshrs_2_io_allocate_bits_prio_0),
    .io_allocate_bits_prio_2(abc_mshrs_2_io_allocate_bits_prio_2),
    .io_allocate_bits_control(abc_mshrs_2_io_allocate_bits_control),
    .io_allocate_bits_opcode(abc_mshrs_2_io_allocate_bits_opcode),
    .io_allocate_bits_param(abc_mshrs_2_io_allocate_bits_param),
    .io_allocate_bits_size(abc_mshrs_2_io_allocate_bits_size),
    .io_allocate_bits_source(abc_mshrs_2_io_allocate_bits_source),
    .io_allocate_bits_tag(abc_mshrs_2_io_allocate_bits_tag),
    .io_allocate_bits_offset(abc_mshrs_2_io_allocate_bits_offset),
    .io_allocate_bits_put(abc_mshrs_2_io_allocate_bits_put),
    .io_allocate_bits_set(abc_mshrs_2_io_allocate_bits_set),
    .io_allocate_bits_repeat(abc_mshrs_2_io_allocate_bits_repeat),
    .io_directory_valid(abc_mshrs_2_io_directory_valid),
    .io_directory_bits_dirty(abc_mshrs_2_io_directory_bits_dirty),
    .io_directory_bits_state(abc_mshrs_2_io_directory_bits_state),
    .io_directory_bits_clients(abc_mshrs_2_io_directory_bits_clients),
    .io_directory_bits_tag(abc_mshrs_2_io_directory_bits_tag),
    .io_directory_bits_hit(abc_mshrs_2_io_directory_bits_hit),
    .io_directory_bits_way(abc_mshrs_2_io_directory_bits_way),
    .io_status_valid(abc_mshrs_2_io_status_valid),
    .io_status_bits_set(abc_mshrs_2_io_status_bits_set),
    .io_status_bits_tag(abc_mshrs_2_io_status_bits_tag),
    .io_status_bits_way(abc_mshrs_2_io_status_bits_way),
    .io_status_bits_blockB(abc_mshrs_2_io_status_bits_blockB),
    .io_status_bits_nestB(abc_mshrs_2_io_status_bits_nestB),
    .io_status_bits_blockC(abc_mshrs_2_io_status_bits_blockC),
    .io_status_bits_nestC(abc_mshrs_2_io_status_bits_nestC),
    .io_schedule_ready(abc_mshrs_2_io_schedule_ready),
    .io_schedule_valid(abc_mshrs_2_io_schedule_valid),
    .io_schedule_bits_a_valid(abc_mshrs_2_io_schedule_bits_a_valid),
    .io_schedule_bits_a_bits_tag(abc_mshrs_2_io_schedule_bits_a_bits_tag),
    .io_schedule_bits_a_bits_set(abc_mshrs_2_io_schedule_bits_a_bits_set),
    .io_schedule_bits_a_bits_param(abc_mshrs_2_io_schedule_bits_a_bits_param),
    .io_schedule_bits_a_bits_block(abc_mshrs_2_io_schedule_bits_a_bits_block),
    .io_schedule_bits_b_valid(abc_mshrs_2_io_schedule_bits_b_valid),
    .io_schedule_bits_b_bits_param(abc_mshrs_2_io_schedule_bits_b_bits_param),
    .io_schedule_bits_b_bits_tag(abc_mshrs_2_io_schedule_bits_b_bits_tag),
    .io_schedule_bits_b_bits_set(abc_mshrs_2_io_schedule_bits_b_bits_set),
    .io_schedule_bits_b_bits_clients(abc_mshrs_2_io_schedule_bits_b_bits_clients),
    .io_schedule_bits_c_valid(abc_mshrs_2_io_schedule_bits_c_valid),
    .io_schedule_bits_c_bits_opcode(abc_mshrs_2_io_schedule_bits_c_bits_opcode),
    .io_schedule_bits_c_bits_param(abc_mshrs_2_io_schedule_bits_c_bits_param),
    .io_schedule_bits_c_bits_tag(abc_mshrs_2_io_schedule_bits_c_bits_tag),
    .io_schedule_bits_c_bits_set(abc_mshrs_2_io_schedule_bits_c_bits_set),
    .io_schedule_bits_c_bits_way(abc_mshrs_2_io_schedule_bits_c_bits_way),
    .io_schedule_bits_c_bits_dirty(abc_mshrs_2_io_schedule_bits_c_bits_dirty),
    .io_schedule_bits_d_valid(abc_mshrs_2_io_schedule_bits_d_valid),
    .io_schedule_bits_d_bits_prio_0(abc_mshrs_2_io_schedule_bits_d_bits_prio_0),
    .io_schedule_bits_d_bits_prio_2(abc_mshrs_2_io_schedule_bits_d_bits_prio_2),
    .io_schedule_bits_d_bits_opcode(abc_mshrs_2_io_schedule_bits_d_bits_opcode),
    .io_schedule_bits_d_bits_param(abc_mshrs_2_io_schedule_bits_d_bits_param),
    .io_schedule_bits_d_bits_size(abc_mshrs_2_io_schedule_bits_d_bits_size),
    .io_schedule_bits_d_bits_source(abc_mshrs_2_io_schedule_bits_d_bits_source),
    .io_schedule_bits_d_bits_offset(abc_mshrs_2_io_schedule_bits_d_bits_offset),
    .io_schedule_bits_d_bits_put(abc_mshrs_2_io_schedule_bits_d_bits_put),
    .io_schedule_bits_d_bits_set(abc_mshrs_2_io_schedule_bits_d_bits_set),
    .io_schedule_bits_d_bits_way(abc_mshrs_2_io_schedule_bits_d_bits_way),
    .io_schedule_bits_d_bits_bad(abc_mshrs_2_io_schedule_bits_d_bits_bad),
    .io_schedule_bits_d_bits_user_hit_cache(abc_mshrs_2_io_schedule_bits_d_bits_user_hit_cache),
    .io_schedule_bits_e_valid(abc_mshrs_2_io_schedule_bits_e_valid),
    .io_schedule_bits_e_bits_sink(abc_mshrs_2_io_schedule_bits_e_bits_sink),
    .io_schedule_bits_x_valid(abc_mshrs_2_io_schedule_bits_x_valid),
    .io_schedule_bits_dir_valid(abc_mshrs_2_io_schedule_bits_dir_valid),
    .io_schedule_bits_dir_bits_set(abc_mshrs_2_io_schedule_bits_dir_bits_set),
    .io_schedule_bits_dir_bits_way(abc_mshrs_2_io_schedule_bits_dir_bits_way),
    .io_schedule_bits_dir_bits_data_dirty(abc_mshrs_2_io_schedule_bits_dir_bits_data_dirty),
    .io_schedule_bits_dir_bits_data_state(abc_mshrs_2_io_schedule_bits_dir_bits_data_state),
    .io_schedule_bits_dir_bits_data_clients(abc_mshrs_2_io_schedule_bits_dir_bits_data_clients),
    .io_schedule_bits_dir_bits_data_tag(abc_mshrs_2_io_schedule_bits_dir_bits_data_tag),
    .io_schedule_bits_reload(abc_mshrs_2_io_schedule_bits_reload),
    .io_sinkc_valid(abc_mshrs_2_io_sinkc_valid),
    .io_sinkc_bits_last(abc_mshrs_2_io_sinkc_bits_last),
    .io_sinkc_bits_tag(abc_mshrs_2_io_sinkc_bits_tag),
    .io_sinkc_bits_source(abc_mshrs_2_io_sinkc_bits_source),
    .io_sinkc_bits_param(abc_mshrs_2_io_sinkc_bits_param),
    .io_sinkc_bits_data(abc_mshrs_2_io_sinkc_bits_data),
    .io_sinkd_valid(abc_mshrs_2_io_sinkd_valid),
    .io_sinkd_bits_last(abc_mshrs_2_io_sinkd_bits_last),
    .io_sinkd_bits_opcode(abc_mshrs_2_io_sinkd_bits_opcode),
    .io_sinkd_bits_param(abc_mshrs_2_io_sinkd_bits_param),
    .io_sinkd_bits_sink(abc_mshrs_2_io_sinkd_bits_sink),
    .io_sinkd_bits_denied(abc_mshrs_2_io_sinkd_bits_denied),
    .io_sinke_valid(abc_mshrs_2_io_sinke_valid),
    .io_nestedwb_set(abc_mshrs_2_io_nestedwb_set),
    .io_nestedwb_tag(abc_mshrs_2_io_nestedwb_tag),
    .io_nestedwb_b_toN(abc_mshrs_2_io_nestedwb_b_toN),
    .io_nestedwb_b_toB(abc_mshrs_2_io_nestedwb_b_toB),
    .io_nestedwb_b_clr_dirty(abc_mshrs_2_io_nestedwb_b_clr_dirty),
    .io_nestedwb_c_set_dirty(abc_mshrs_2_io_nestedwb_c_set_dirty)
  );
  RHEA__MSHR abc_mshrs_3 ( // @[Scheduler.scala 105:48]
    .rf_reset(abc_mshrs_3_rf_reset),
    .clock(abc_mshrs_3_clock),
    .reset(abc_mshrs_3_reset),
    .io_allocate_valid(abc_mshrs_3_io_allocate_valid),
    .io_allocate_bits_prio_0(abc_mshrs_3_io_allocate_bits_prio_0),
    .io_allocate_bits_prio_2(abc_mshrs_3_io_allocate_bits_prio_2),
    .io_allocate_bits_control(abc_mshrs_3_io_allocate_bits_control),
    .io_allocate_bits_opcode(abc_mshrs_3_io_allocate_bits_opcode),
    .io_allocate_bits_param(abc_mshrs_3_io_allocate_bits_param),
    .io_allocate_bits_size(abc_mshrs_3_io_allocate_bits_size),
    .io_allocate_bits_source(abc_mshrs_3_io_allocate_bits_source),
    .io_allocate_bits_tag(abc_mshrs_3_io_allocate_bits_tag),
    .io_allocate_bits_offset(abc_mshrs_3_io_allocate_bits_offset),
    .io_allocate_bits_put(abc_mshrs_3_io_allocate_bits_put),
    .io_allocate_bits_set(abc_mshrs_3_io_allocate_bits_set),
    .io_allocate_bits_repeat(abc_mshrs_3_io_allocate_bits_repeat),
    .io_directory_valid(abc_mshrs_3_io_directory_valid),
    .io_directory_bits_dirty(abc_mshrs_3_io_directory_bits_dirty),
    .io_directory_bits_state(abc_mshrs_3_io_directory_bits_state),
    .io_directory_bits_clients(abc_mshrs_3_io_directory_bits_clients),
    .io_directory_bits_tag(abc_mshrs_3_io_directory_bits_tag),
    .io_directory_bits_hit(abc_mshrs_3_io_directory_bits_hit),
    .io_directory_bits_way(abc_mshrs_3_io_directory_bits_way),
    .io_status_valid(abc_mshrs_3_io_status_valid),
    .io_status_bits_set(abc_mshrs_3_io_status_bits_set),
    .io_status_bits_tag(abc_mshrs_3_io_status_bits_tag),
    .io_status_bits_way(abc_mshrs_3_io_status_bits_way),
    .io_status_bits_blockB(abc_mshrs_3_io_status_bits_blockB),
    .io_status_bits_nestB(abc_mshrs_3_io_status_bits_nestB),
    .io_status_bits_blockC(abc_mshrs_3_io_status_bits_blockC),
    .io_status_bits_nestC(abc_mshrs_3_io_status_bits_nestC),
    .io_schedule_ready(abc_mshrs_3_io_schedule_ready),
    .io_schedule_valid(abc_mshrs_3_io_schedule_valid),
    .io_schedule_bits_a_valid(abc_mshrs_3_io_schedule_bits_a_valid),
    .io_schedule_bits_a_bits_tag(abc_mshrs_3_io_schedule_bits_a_bits_tag),
    .io_schedule_bits_a_bits_set(abc_mshrs_3_io_schedule_bits_a_bits_set),
    .io_schedule_bits_a_bits_param(abc_mshrs_3_io_schedule_bits_a_bits_param),
    .io_schedule_bits_a_bits_block(abc_mshrs_3_io_schedule_bits_a_bits_block),
    .io_schedule_bits_b_valid(abc_mshrs_3_io_schedule_bits_b_valid),
    .io_schedule_bits_b_bits_param(abc_mshrs_3_io_schedule_bits_b_bits_param),
    .io_schedule_bits_b_bits_tag(abc_mshrs_3_io_schedule_bits_b_bits_tag),
    .io_schedule_bits_b_bits_set(abc_mshrs_3_io_schedule_bits_b_bits_set),
    .io_schedule_bits_b_bits_clients(abc_mshrs_3_io_schedule_bits_b_bits_clients),
    .io_schedule_bits_c_valid(abc_mshrs_3_io_schedule_bits_c_valid),
    .io_schedule_bits_c_bits_opcode(abc_mshrs_3_io_schedule_bits_c_bits_opcode),
    .io_schedule_bits_c_bits_param(abc_mshrs_3_io_schedule_bits_c_bits_param),
    .io_schedule_bits_c_bits_tag(abc_mshrs_3_io_schedule_bits_c_bits_tag),
    .io_schedule_bits_c_bits_set(abc_mshrs_3_io_schedule_bits_c_bits_set),
    .io_schedule_bits_c_bits_way(abc_mshrs_3_io_schedule_bits_c_bits_way),
    .io_schedule_bits_c_bits_dirty(abc_mshrs_3_io_schedule_bits_c_bits_dirty),
    .io_schedule_bits_d_valid(abc_mshrs_3_io_schedule_bits_d_valid),
    .io_schedule_bits_d_bits_prio_0(abc_mshrs_3_io_schedule_bits_d_bits_prio_0),
    .io_schedule_bits_d_bits_prio_2(abc_mshrs_3_io_schedule_bits_d_bits_prio_2),
    .io_schedule_bits_d_bits_opcode(abc_mshrs_3_io_schedule_bits_d_bits_opcode),
    .io_schedule_bits_d_bits_param(abc_mshrs_3_io_schedule_bits_d_bits_param),
    .io_schedule_bits_d_bits_size(abc_mshrs_3_io_schedule_bits_d_bits_size),
    .io_schedule_bits_d_bits_source(abc_mshrs_3_io_schedule_bits_d_bits_source),
    .io_schedule_bits_d_bits_offset(abc_mshrs_3_io_schedule_bits_d_bits_offset),
    .io_schedule_bits_d_bits_put(abc_mshrs_3_io_schedule_bits_d_bits_put),
    .io_schedule_bits_d_bits_set(abc_mshrs_3_io_schedule_bits_d_bits_set),
    .io_schedule_bits_d_bits_way(abc_mshrs_3_io_schedule_bits_d_bits_way),
    .io_schedule_bits_d_bits_bad(abc_mshrs_3_io_schedule_bits_d_bits_bad),
    .io_schedule_bits_d_bits_user_hit_cache(abc_mshrs_3_io_schedule_bits_d_bits_user_hit_cache),
    .io_schedule_bits_e_valid(abc_mshrs_3_io_schedule_bits_e_valid),
    .io_schedule_bits_e_bits_sink(abc_mshrs_3_io_schedule_bits_e_bits_sink),
    .io_schedule_bits_x_valid(abc_mshrs_3_io_schedule_bits_x_valid),
    .io_schedule_bits_dir_valid(abc_mshrs_3_io_schedule_bits_dir_valid),
    .io_schedule_bits_dir_bits_set(abc_mshrs_3_io_schedule_bits_dir_bits_set),
    .io_schedule_bits_dir_bits_way(abc_mshrs_3_io_schedule_bits_dir_bits_way),
    .io_schedule_bits_dir_bits_data_dirty(abc_mshrs_3_io_schedule_bits_dir_bits_data_dirty),
    .io_schedule_bits_dir_bits_data_state(abc_mshrs_3_io_schedule_bits_dir_bits_data_state),
    .io_schedule_bits_dir_bits_data_clients(abc_mshrs_3_io_schedule_bits_dir_bits_data_clients),
    .io_schedule_bits_dir_bits_data_tag(abc_mshrs_3_io_schedule_bits_dir_bits_data_tag),
    .io_schedule_bits_reload(abc_mshrs_3_io_schedule_bits_reload),
    .io_sinkc_valid(abc_mshrs_3_io_sinkc_valid),
    .io_sinkc_bits_last(abc_mshrs_3_io_sinkc_bits_last),
    .io_sinkc_bits_tag(abc_mshrs_3_io_sinkc_bits_tag),
    .io_sinkc_bits_source(abc_mshrs_3_io_sinkc_bits_source),
    .io_sinkc_bits_param(abc_mshrs_3_io_sinkc_bits_param),
    .io_sinkc_bits_data(abc_mshrs_3_io_sinkc_bits_data),
    .io_sinkd_valid(abc_mshrs_3_io_sinkd_valid),
    .io_sinkd_bits_last(abc_mshrs_3_io_sinkd_bits_last),
    .io_sinkd_bits_opcode(abc_mshrs_3_io_sinkd_bits_opcode),
    .io_sinkd_bits_param(abc_mshrs_3_io_sinkd_bits_param),
    .io_sinkd_bits_sink(abc_mshrs_3_io_sinkd_bits_sink),
    .io_sinkd_bits_denied(abc_mshrs_3_io_sinkd_bits_denied),
    .io_sinke_valid(abc_mshrs_3_io_sinke_valid),
    .io_nestedwb_set(abc_mshrs_3_io_nestedwb_set),
    .io_nestedwb_tag(abc_mshrs_3_io_nestedwb_tag),
    .io_nestedwb_b_toN(abc_mshrs_3_io_nestedwb_b_toN),
    .io_nestedwb_b_toB(abc_mshrs_3_io_nestedwb_b_toB),
    .io_nestedwb_b_clr_dirty(abc_mshrs_3_io_nestedwb_b_clr_dirty),
    .io_nestedwb_c_set_dirty(abc_mshrs_3_io_nestedwb_c_set_dirty)
  );
  RHEA__MSHR abc_mshrs_4 ( // @[Scheduler.scala 105:48]
    .rf_reset(abc_mshrs_4_rf_reset),
    .clock(abc_mshrs_4_clock),
    .reset(abc_mshrs_4_reset),
    .io_allocate_valid(abc_mshrs_4_io_allocate_valid),
    .io_allocate_bits_prio_0(abc_mshrs_4_io_allocate_bits_prio_0),
    .io_allocate_bits_prio_2(abc_mshrs_4_io_allocate_bits_prio_2),
    .io_allocate_bits_control(abc_mshrs_4_io_allocate_bits_control),
    .io_allocate_bits_opcode(abc_mshrs_4_io_allocate_bits_opcode),
    .io_allocate_bits_param(abc_mshrs_4_io_allocate_bits_param),
    .io_allocate_bits_size(abc_mshrs_4_io_allocate_bits_size),
    .io_allocate_bits_source(abc_mshrs_4_io_allocate_bits_source),
    .io_allocate_bits_tag(abc_mshrs_4_io_allocate_bits_tag),
    .io_allocate_bits_offset(abc_mshrs_4_io_allocate_bits_offset),
    .io_allocate_bits_put(abc_mshrs_4_io_allocate_bits_put),
    .io_allocate_bits_set(abc_mshrs_4_io_allocate_bits_set),
    .io_allocate_bits_repeat(abc_mshrs_4_io_allocate_bits_repeat),
    .io_directory_valid(abc_mshrs_4_io_directory_valid),
    .io_directory_bits_dirty(abc_mshrs_4_io_directory_bits_dirty),
    .io_directory_bits_state(abc_mshrs_4_io_directory_bits_state),
    .io_directory_bits_clients(abc_mshrs_4_io_directory_bits_clients),
    .io_directory_bits_tag(abc_mshrs_4_io_directory_bits_tag),
    .io_directory_bits_hit(abc_mshrs_4_io_directory_bits_hit),
    .io_directory_bits_way(abc_mshrs_4_io_directory_bits_way),
    .io_status_valid(abc_mshrs_4_io_status_valid),
    .io_status_bits_set(abc_mshrs_4_io_status_bits_set),
    .io_status_bits_tag(abc_mshrs_4_io_status_bits_tag),
    .io_status_bits_way(abc_mshrs_4_io_status_bits_way),
    .io_status_bits_blockB(abc_mshrs_4_io_status_bits_blockB),
    .io_status_bits_nestB(abc_mshrs_4_io_status_bits_nestB),
    .io_status_bits_blockC(abc_mshrs_4_io_status_bits_blockC),
    .io_status_bits_nestC(abc_mshrs_4_io_status_bits_nestC),
    .io_schedule_ready(abc_mshrs_4_io_schedule_ready),
    .io_schedule_valid(abc_mshrs_4_io_schedule_valid),
    .io_schedule_bits_a_valid(abc_mshrs_4_io_schedule_bits_a_valid),
    .io_schedule_bits_a_bits_tag(abc_mshrs_4_io_schedule_bits_a_bits_tag),
    .io_schedule_bits_a_bits_set(abc_mshrs_4_io_schedule_bits_a_bits_set),
    .io_schedule_bits_a_bits_param(abc_mshrs_4_io_schedule_bits_a_bits_param),
    .io_schedule_bits_a_bits_block(abc_mshrs_4_io_schedule_bits_a_bits_block),
    .io_schedule_bits_b_valid(abc_mshrs_4_io_schedule_bits_b_valid),
    .io_schedule_bits_b_bits_param(abc_mshrs_4_io_schedule_bits_b_bits_param),
    .io_schedule_bits_b_bits_tag(abc_mshrs_4_io_schedule_bits_b_bits_tag),
    .io_schedule_bits_b_bits_set(abc_mshrs_4_io_schedule_bits_b_bits_set),
    .io_schedule_bits_b_bits_clients(abc_mshrs_4_io_schedule_bits_b_bits_clients),
    .io_schedule_bits_c_valid(abc_mshrs_4_io_schedule_bits_c_valid),
    .io_schedule_bits_c_bits_opcode(abc_mshrs_4_io_schedule_bits_c_bits_opcode),
    .io_schedule_bits_c_bits_param(abc_mshrs_4_io_schedule_bits_c_bits_param),
    .io_schedule_bits_c_bits_tag(abc_mshrs_4_io_schedule_bits_c_bits_tag),
    .io_schedule_bits_c_bits_set(abc_mshrs_4_io_schedule_bits_c_bits_set),
    .io_schedule_bits_c_bits_way(abc_mshrs_4_io_schedule_bits_c_bits_way),
    .io_schedule_bits_c_bits_dirty(abc_mshrs_4_io_schedule_bits_c_bits_dirty),
    .io_schedule_bits_d_valid(abc_mshrs_4_io_schedule_bits_d_valid),
    .io_schedule_bits_d_bits_prio_0(abc_mshrs_4_io_schedule_bits_d_bits_prio_0),
    .io_schedule_bits_d_bits_prio_2(abc_mshrs_4_io_schedule_bits_d_bits_prio_2),
    .io_schedule_bits_d_bits_opcode(abc_mshrs_4_io_schedule_bits_d_bits_opcode),
    .io_schedule_bits_d_bits_param(abc_mshrs_4_io_schedule_bits_d_bits_param),
    .io_schedule_bits_d_bits_size(abc_mshrs_4_io_schedule_bits_d_bits_size),
    .io_schedule_bits_d_bits_source(abc_mshrs_4_io_schedule_bits_d_bits_source),
    .io_schedule_bits_d_bits_offset(abc_mshrs_4_io_schedule_bits_d_bits_offset),
    .io_schedule_bits_d_bits_put(abc_mshrs_4_io_schedule_bits_d_bits_put),
    .io_schedule_bits_d_bits_set(abc_mshrs_4_io_schedule_bits_d_bits_set),
    .io_schedule_bits_d_bits_way(abc_mshrs_4_io_schedule_bits_d_bits_way),
    .io_schedule_bits_d_bits_bad(abc_mshrs_4_io_schedule_bits_d_bits_bad),
    .io_schedule_bits_d_bits_user_hit_cache(abc_mshrs_4_io_schedule_bits_d_bits_user_hit_cache),
    .io_schedule_bits_e_valid(abc_mshrs_4_io_schedule_bits_e_valid),
    .io_schedule_bits_e_bits_sink(abc_mshrs_4_io_schedule_bits_e_bits_sink),
    .io_schedule_bits_x_valid(abc_mshrs_4_io_schedule_bits_x_valid),
    .io_schedule_bits_dir_valid(abc_mshrs_4_io_schedule_bits_dir_valid),
    .io_schedule_bits_dir_bits_set(abc_mshrs_4_io_schedule_bits_dir_bits_set),
    .io_schedule_bits_dir_bits_way(abc_mshrs_4_io_schedule_bits_dir_bits_way),
    .io_schedule_bits_dir_bits_data_dirty(abc_mshrs_4_io_schedule_bits_dir_bits_data_dirty),
    .io_schedule_bits_dir_bits_data_state(abc_mshrs_4_io_schedule_bits_dir_bits_data_state),
    .io_schedule_bits_dir_bits_data_clients(abc_mshrs_4_io_schedule_bits_dir_bits_data_clients),
    .io_schedule_bits_dir_bits_data_tag(abc_mshrs_4_io_schedule_bits_dir_bits_data_tag),
    .io_schedule_bits_reload(abc_mshrs_4_io_schedule_bits_reload),
    .io_sinkc_valid(abc_mshrs_4_io_sinkc_valid),
    .io_sinkc_bits_last(abc_mshrs_4_io_sinkc_bits_last),
    .io_sinkc_bits_tag(abc_mshrs_4_io_sinkc_bits_tag),
    .io_sinkc_bits_source(abc_mshrs_4_io_sinkc_bits_source),
    .io_sinkc_bits_param(abc_mshrs_4_io_sinkc_bits_param),
    .io_sinkc_bits_data(abc_mshrs_4_io_sinkc_bits_data),
    .io_sinkd_valid(abc_mshrs_4_io_sinkd_valid),
    .io_sinkd_bits_last(abc_mshrs_4_io_sinkd_bits_last),
    .io_sinkd_bits_opcode(abc_mshrs_4_io_sinkd_bits_opcode),
    .io_sinkd_bits_param(abc_mshrs_4_io_sinkd_bits_param),
    .io_sinkd_bits_sink(abc_mshrs_4_io_sinkd_bits_sink),
    .io_sinkd_bits_denied(abc_mshrs_4_io_sinkd_bits_denied),
    .io_sinke_valid(abc_mshrs_4_io_sinke_valid),
    .io_nestedwb_set(abc_mshrs_4_io_nestedwb_set),
    .io_nestedwb_tag(abc_mshrs_4_io_nestedwb_tag),
    .io_nestedwb_b_toN(abc_mshrs_4_io_nestedwb_b_toN),
    .io_nestedwb_b_toB(abc_mshrs_4_io_nestedwb_b_toB),
    .io_nestedwb_b_clr_dirty(abc_mshrs_4_io_nestedwb_b_clr_dirty),
    .io_nestedwb_c_set_dirty(abc_mshrs_4_io_nestedwb_c_set_dirty)
  );
  RHEA__MSHR abc_mshrs_5 ( // @[Scheduler.scala 105:48]
    .rf_reset(abc_mshrs_5_rf_reset),
    .clock(abc_mshrs_5_clock),
    .reset(abc_mshrs_5_reset),
    .io_allocate_valid(abc_mshrs_5_io_allocate_valid),
    .io_allocate_bits_prio_0(abc_mshrs_5_io_allocate_bits_prio_0),
    .io_allocate_bits_prio_2(abc_mshrs_5_io_allocate_bits_prio_2),
    .io_allocate_bits_control(abc_mshrs_5_io_allocate_bits_control),
    .io_allocate_bits_opcode(abc_mshrs_5_io_allocate_bits_opcode),
    .io_allocate_bits_param(abc_mshrs_5_io_allocate_bits_param),
    .io_allocate_bits_size(abc_mshrs_5_io_allocate_bits_size),
    .io_allocate_bits_source(abc_mshrs_5_io_allocate_bits_source),
    .io_allocate_bits_tag(abc_mshrs_5_io_allocate_bits_tag),
    .io_allocate_bits_offset(abc_mshrs_5_io_allocate_bits_offset),
    .io_allocate_bits_put(abc_mshrs_5_io_allocate_bits_put),
    .io_allocate_bits_set(abc_mshrs_5_io_allocate_bits_set),
    .io_allocate_bits_repeat(abc_mshrs_5_io_allocate_bits_repeat),
    .io_directory_valid(abc_mshrs_5_io_directory_valid),
    .io_directory_bits_dirty(abc_mshrs_5_io_directory_bits_dirty),
    .io_directory_bits_state(abc_mshrs_5_io_directory_bits_state),
    .io_directory_bits_clients(abc_mshrs_5_io_directory_bits_clients),
    .io_directory_bits_tag(abc_mshrs_5_io_directory_bits_tag),
    .io_directory_bits_hit(abc_mshrs_5_io_directory_bits_hit),
    .io_directory_bits_way(abc_mshrs_5_io_directory_bits_way),
    .io_status_valid(abc_mshrs_5_io_status_valid),
    .io_status_bits_set(abc_mshrs_5_io_status_bits_set),
    .io_status_bits_tag(abc_mshrs_5_io_status_bits_tag),
    .io_status_bits_way(abc_mshrs_5_io_status_bits_way),
    .io_status_bits_blockB(abc_mshrs_5_io_status_bits_blockB),
    .io_status_bits_nestB(abc_mshrs_5_io_status_bits_nestB),
    .io_status_bits_blockC(abc_mshrs_5_io_status_bits_blockC),
    .io_status_bits_nestC(abc_mshrs_5_io_status_bits_nestC),
    .io_schedule_ready(abc_mshrs_5_io_schedule_ready),
    .io_schedule_valid(abc_mshrs_5_io_schedule_valid),
    .io_schedule_bits_a_valid(abc_mshrs_5_io_schedule_bits_a_valid),
    .io_schedule_bits_a_bits_tag(abc_mshrs_5_io_schedule_bits_a_bits_tag),
    .io_schedule_bits_a_bits_set(abc_mshrs_5_io_schedule_bits_a_bits_set),
    .io_schedule_bits_a_bits_param(abc_mshrs_5_io_schedule_bits_a_bits_param),
    .io_schedule_bits_a_bits_block(abc_mshrs_5_io_schedule_bits_a_bits_block),
    .io_schedule_bits_b_valid(abc_mshrs_5_io_schedule_bits_b_valid),
    .io_schedule_bits_b_bits_param(abc_mshrs_5_io_schedule_bits_b_bits_param),
    .io_schedule_bits_b_bits_tag(abc_mshrs_5_io_schedule_bits_b_bits_tag),
    .io_schedule_bits_b_bits_set(abc_mshrs_5_io_schedule_bits_b_bits_set),
    .io_schedule_bits_b_bits_clients(abc_mshrs_5_io_schedule_bits_b_bits_clients),
    .io_schedule_bits_c_valid(abc_mshrs_5_io_schedule_bits_c_valid),
    .io_schedule_bits_c_bits_opcode(abc_mshrs_5_io_schedule_bits_c_bits_opcode),
    .io_schedule_bits_c_bits_param(abc_mshrs_5_io_schedule_bits_c_bits_param),
    .io_schedule_bits_c_bits_tag(abc_mshrs_5_io_schedule_bits_c_bits_tag),
    .io_schedule_bits_c_bits_set(abc_mshrs_5_io_schedule_bits_c_bits_set),
    .io_schedule_bits_c_bits_way(abc_mshrs_5_io_schedule_bits_c_bits_way),
    .io_schedule_bits_c_bits_dirty(abc_mshrs_5_io_schedule_bits_c_bits_dirty),
    .io_schedule_bits_d_valid(abc_mshrs_5_io_schedule_bits_d_valid),
    .io_schedule_bits_d_bits_prio_0(abc_mshrs_5_io_schedule_bits_d_bits_prio_0),
    .io_schedule_bits_d_bits_prio_2(abc_mshrs_5_io_schedule_bits_d_bits_prio_2),
    .io_schedule_bits_d_bits_opcode(abc_mshrs_5_io_schedule_bits_d_bits_opcode),
    .io_schedule_bits_d_bits_param(abc_mshrs_5_io_schedule_bits_d_bits_param),
    .io_schedule_bits_d_bits_size(abc_mshrs_5_io_schedule_bits_d_bits_size),
    .io_schedule_bits_d_bits_source(abc_mshrs_5_io_schedule_bits_d_bits_source),
    .io_schedule_bits_d_bits_offset(abc_mshrs_5_io_schedule_bits_d_bits_offset),
    .io_schedule_bits_d_bits_put(abc_mshrs_5_io_schedule_bits_d_bits_put),
    .io_schedule_bits_d_bits_set(abc_mshrs_5_io_schedule_bits_d_bits_set),
    .io_schedule_bits_d_bits_way(abc_mshrs_5_io_schedule_bits_d_bits_way),
    .io_schedule_bits_d_bits_bad(abc_mshrs_5_io_schedule_bits_d_bits_bad),
    .io_schedule_bits_d_bits_user_hit_cache(abc_mshrs_5_io_schedule_bits_d_bits_user_hit_cache),
    .io_schedule_bits_e_valid(abc_mshrs_5_io_schedule_bits_e_valid),
    .io_schedule_bits_e_bits_sink(abc_mshrs_5_io_schedule_bits_e_bits_sink),
    .io_schedule_bits_x_valid(abc_mshrs_5_io_schedule_bits_x_valid),
    .io_schedule_bits_dir_valid(abc_mshrs_5_io_schedule_bits_dir_valid),
    .io_schedule_bits_dir_bits_set(abc_mshrs_5_io_schedule_bits_dir_bits_set),
    .io_schedule_bits_dir_bits_way(abc_mshrs_5_io_schedule_bits_dir_bits_way),
    .io_schedule_bits_dir_bits_data_dirty(abc_mshrs_5_io_schedule_bits_dir_bits_data_dirty),
    .io_schedule_bits_dir_bits_data_state(abc_mshrs_5_io_schedule_bits_dir_bits_data_state),
    .io_schedule_bits_dir_bits_data_clients(abc_mshrs_5_io_schedule_bits_dir_bits_data_clients),
    .io_schedule_bits_dir_bits_data_tag(abc_mshrs_5_io_schedule_bits_dir_bits_data_tag),
    .io_schedule_bits_reload(abc_mshrs_5_io_schedule_bits_reload),
    .io_sinkc_valid(abc_mshrs_5_io_sinkc_valid),
    .io_sinkc_bits_last(abc_mshrs_5_io_sinkc_bits_last),
    .io_sinkc_bits_tag(abc_mshrs_5_io_sinkc_bits_tag),
    .io_sinkc_bits_source(abc_mshrs_5_io_sinkc_bits_source),
    .io_sinkc_bits_param(abc_mshrs_5_io_sinkc_bits_param),
    .io_sinkc_bits_data(abc_mshrs_5_io_sinkc_bits_data),
    .io_sinkd_valid(abc_mshrs_5_io_sinkd_valid),
    .io_sinkd_bits_last(abc_mshrs_5_io_sinkd_bits_last),
    .io_sinkd_bits_opcode(abc_mshrs_5_io_sinkd_bits_opcode),
    .io_sinkd_bits_param(abc_mshrs_5_io_sinkd_bits_param),
    .io_sinkd_bits_sink(abc_mshrs_5_io_sinkd_bits_sink),
    .io_sinkd_bits_denied(abc_mshrs_5_io_sinkd_bits_denied),
    .io_sinke_valid(abc_mshrs_5_io_sinke_valid),
    .io_nestedwb_set(abc_mshrs_5_io_nestedwb_set),
    .io_nestedwb_tag(abc_mshrs_5_io_nestedwb_tag),
    .io_nestedwb_b_toN(abc_mshrs_5_io_nestedwb_b_toN),
    .io_nestedwb_b_toB(abc_mshrs_5_io_nestedwb_b_toB),
    .io_nestedwb_b_clr_dirty(abc_mshrs_5_io_nestedwb_b_clr_dirty),
    .io_nestedwb_c_set_dirty(abc_mshrs_5_io_nestedwb_c_set_dirty)
  );
  RHEA__MSHR_6 bc_mshr ( // @[Scheduler.scala 106:11]
    .rf_reset(bc_mshr_rf_reset),
    .clock(bc_mshr_clock),
    .reset(bc_mshr_reset),
    .io_allocate_valid(bc_mshr_io_allocate_valid),
    .io_allocate_bits_prio_2(bc_mshr_io_allocate_bits_prio_2),
    .io_allocate_bits_control(bc_mshr_io_allocate_bits_control),
    .io_allocate_bits_opcode(bc_mshr_io_allocate_bits_opcode),
    .io_allocate_bits_param(bc_mshr_io_allocate_bits_param),
    .io_allocate_bits_size(bc_mshr_io_allocate_bits_size),
    .io_allocate_bits_source(bc_mshr_io_allocate_bits_source),
    .io_allocate_bits_tag(bc_mshr_io_allocate_bits_tag),
    .io_allocate_bits_offset(bc_mshr_io_allocate_bits_offset),
    .io_allocate_bits_put(bc_mshr_io_allocate_bits_put),
    .io_allocate_bits_set(bc_mshr_io_allocate_bits_set),
    .io_allocate_bits_repeat(bc_mshr_io_allocate_bits_repeat),
    .io_directory_valid(bc_mshr_io_directory_valid),
    .io_directory_bits_dirty(bc_mshr_io_directory_bits_dirty),
    .io_directory_bits_state(bc_mshr_io_directory_bits_state),
    .io_directory_bits_clients(bc_mshr_io_directory_bits_clients),
    .io_directory_bits_tag(bc_mshr_io_directory_bits_tag),
    .io_directory_bits_hit(bc_mshr_io_directory_bits_hit),
    .io_directory_bits_way(bc_mshr_io_directory_bits_way),
    .io_status_valid(bc_mshr_io_status_valid),
    .io_status_bits_set(bc_mshr_io_status_bits_set),
    .io_status_bits_tag(bc_mshr_io_status_bits_tag),
    .io_status_bits_way(bc_mshr_io_status_bits_way),
    .io_status_bits_blockB(bc_mshr_io_status_bits_blockB),
    .io_status_bits_nestB(bc_mshr_io_status_bits_nestB),
    .io_status_bits_blockC(bc_mshr_io_status_bits_blockC),
    .io_status_bits_nestC(bc_mshr_io_status_bits_nestC),
    .io_schedule_ready(bc_mshr_io_schedule_ready),
    .io_schedule_valid(bc_mshr_io_schedule_valid),
    .io_schedule_bits_a_valid(bc_mshr_io_schedule_bits_a_valid),
    .io_schedule_bits_a_bits_tag(bc_mshr_io_schedule_bits_a_bits_tag),
    .io_schedule_bits_a_bits_set(bc_mshr_io_schedule_bits_a_bits_set),
    .io_schedule_bits_a_bits_param(bc_mshr_io_schedule_bits_a_bits_param),
    .io_schedule_bits_a_bits_block(bc_mshr_io_schedule_bits_a_bits_block),
    .io_schedule_bits_b_valid(bc_mshr_io_schedule_bits_b_valid),
    .io_schedule_bits_b_bits_param(bc_mshr_io_schedule_bits_b_bits_param),
    .io_schedule_bits_b_bits_tag(bc_mshr_io_schedule_bits_b_bits_tag),
    .io_schedule_bits_b_bits_set(bc_mshr_io_schedule_bits_b_bits_set),
    .io_schedule_bits_b_bits_clients(bc_mshr_io_schedule_bits_b_bits_clients),
    .io_schedule_bits_c_valid(bc_mshr_io_schedule_bits_c_valid),
    .io_schedule_bits_c_bits_opcode(bc_mshr_io_schedule_bits_c_bits_opcode),
    .io_schedule_bits_c_bits_param(bc_mshr_io_schedule_bits_c_bits_param),
    .io_schedule_bits_c_bits_tag(bc_mshr_io_schedule_bits_c_bits_tag),
    .io_schedule_bits_c_bits_set(bc_mshr_io_schedule_bits_c_bits_set),
    .io_schedule_bits_c_bits_way(bc_mshr_io_schedule_bits_c_bits_way),
    .io_schedule_bits_c_bits_dirty(bc_mshr_io_schedule_bits_c_bits_dirty),
    .io_schedule_bits_d_valid(bc_mshr_io_schedule_bits_d_valid),
    .io_schedule_bits_d_bits_prio_2(bc_mshr_io_schedule_bits_d_bits_prio_2),
    .io_schedule_bits_d_bits_opcode(bc_mshr_io_schedule_bits_d_bits_opcode),
    .io_schedule_bits_d_bits_param(bc_mshr_io_schedule_bits_d_bits_param),
    .io_schedule_bits_d_bits_size(bc_mshr_io_schedule_bits_d_bits_size),
    .io_schedule_bits_d_bits_source(bc_mshr_io_schedule_bits_d_bits_source),
    .io_schedule_bits_d_bits_offset(bc_mshr_io_schedule_bits_d_bits_offset),
    .io_schedule_bits_d_bits_put(bc_mshr_io_schedule_bits_d_bits_put),
    .io_schedule_bits_d_bits_set(bc_mshr_io_schedule_bits_d_bits_set),
    .io_schedule_bits_d_bits_way(bc_mshr_io_schedule_bits_d_bits_way),
    .io_schedule_bits_d_bits_bad(bc_mshr_io_schedule_bits_d_bits_bad),
    .io_schedule_bits_d_bits_user_hit_cache(bc_mshr_io_schedule_bits_d_bits_user_hit_cache),
    .io_schedule_bits_e_valid(bc_mshr_io_schedule_bits_e_valid),
    .io_schedule_bits_e_bits_sink(bc_mshr_io_schedule_bits_e_bits_sink),
    .io_schedule_bits_x_valid(bc_mshr_io_schedule_bits_x_valid),
    .io_schedule_bits_dir_valid(bc_mshr_io_schedule_bits_dir_valid),
    .io_schedule_bits_dir_bits_set(bc_mshr_io_schedule_bits_dir_bits_set),
    .io_schedule_bits_dir_bits_way(bc_mshr_io_schedule_bits_dir_bits_way),
    .io_schedule_bits_dir_bits_data_dirty(bc_mshr_io_schedule_bits_dir_bits_data_dirty),
    .io_schedule_bits_dir_bits_data_state(bc_mshr_io_schedule_bits_dir_bits_data_state),
    .io_schedule_bits_dir_bits_data_clients(bc_mshr_io_schedule_bits_dir_bits_data_clients),
    .io_schedule_bits_dir_bits_data_tag(bc_mshr_io_schedule_bits_dir_bits_data_tag),
    .io_schedule_bits_reload(bc_mshr_io_schedule_bits_reload),
    .io_sinkc_valid(bc_mshr_io_sinkc_valid),
    .io_sinkc_bits_last(bc_mshr_io_sinkc_bits_last),
    .io_sinkc_bits_tag(bc_mshr_io_sinkc_bits_tag),
    .io_sinkc_bits_source(bc_mshr_io_sinkc_bits_source),
    .io_sinkc_bits_param(bc_mshr_io_sinkc_bits_param),
    .io_sinkc_bits_data(bc_mshr_io_sinkc_bits_data),
    .io_sinkd_valid(bc_mshr_io_sinkd_valid),
    .io_sinkd_bits_last(bc_mshr_io_sinkd_bits_last),
    .io_sinkd_bits_opcode(bc_mshr_io_sinkd_bits_opcode),
    .io_sinkd_bits_param(bc_mshr_io_sinkd_bits_param),
    .io_sinkd_bits_sink(bc_mshr_io_sinkd_bits_sink),
    .io_sinkd_bits_denied(bc_mshr_io_sinkd_bits_denied),
    .io_sinke_valid(bc_mshr_io_sinke_valid),
    .io_nestedwb_set(bc_mshr_io_nestedwb_set),
    .io_nestedwb_tag(bc_mshr_io_nestedwb_tag),
    .io_nestedwb_b_toN(bc_mshr_io_nestedwb_b_toN),
    .io_nestedwb_b_toB(bc_mshr_io_nestedwb_b_toB),
    .io_nestedwb_b_clr_dirty(bc_mshr_io_nestedwb_b_clr_dirty),
    .io_nestedwb_c_set_dirty(bc_mshr_io_nestedwb_c_set_dirty)
  );
  RHEA__MSHR_7 c_mshr ( // @[Scheduler.scala 107:11]
    .rf_reset(c_mshr_rf_reset),
    .clock(c_mshr_clock),
    .reset(c_mshr_reset),
    .io_allocate_valid(c_mshr_io_allocate_valid),
    .io_allocate_bits_prio_2(c_mshr_io_allocate_bits_prio_2),
    .io_allocate_bits_control(c_mshr_io_allocate_bits_control),
    .io_allocate_bits_opcode(c_mshr_io_allocate_bits_opcode),
    .io_allocate_bits_param(c_mshr_io_allocate_bits_param),
    .io_allocate_bits_size(c_mshr_io_allocate_bits_size),
    .io_allocate_bits_source(c_mshr_io_allocate_bits_source),
    .io_allocate_bits_tag(c_mshr_io_allocate_bits_tag),
    .io_allocate_bits_offset(c_mshr_io_allocate_bits_offset),
    .io_allocate_bits_put(c_mshr_io_allocate_bits_put),
    .io_allocate_bits_set(c_mshr_io_allocate_bits_set),
    .io_allocate_bits_repeat(c_mshr_io_allocate_bits_repeat),
    .io_directory_valid(c_mshr_io_directory_valid),
    .io_directory_bits_dirty(c_mshr_io_directory_bits_dirty),
    .io_directory_bits_state(c_mshr_io_directory_bits_state),
    .io_directory_bits_clients(c_mshr_io_directory_bits_clients),
    .io_directory_bits_tag(c_mshr_io_directory_bits_tag),
    .io_directory_bits_hit(c_mshr_io_directory_bits_hit),
    .io_directory_bits_way(c_mshr_io_directory_bits_way),
    .io_status_valid(c_mshr_io_status_valid),
    .io_status_bits_set(c_mshr_io_status_bits_set),
    .io_status_bits_tag(c_mshr_io_status_bits_tag),
    .io_status_bits_way(c_mshr_io_status_bits_way),
    .io_status_bits_blockB(c_mshr_io_status_bits_blockB),
    .io_status_bits_nestB(c_mshr_io_status_bits_nestB),
    .io_status_bits_blockC(c_mshr_io_status_bits_blockC),
    .io_status_bits_nestC(c_mshr_io_status_bits_nestC),
    .io_schedule_ready(c_mshr_io_schedule_ready),
    .io_schedule_valid(c_mshr_io_schedule_valid),
    .io_schedule_bits_a_valid(c_mshr_io_schedule_bits_a_valid),
    .io_schedule_bits_a_bits_tag(c_mshr_io_schedule_bits_a_bits_tag),
    .io_schedule_bits_a_bits_set(c_mshr_io_schedule_bits_a_bits_set),
    .io_schedule_bits_a_bits_param(c_mshr_io_schedule_bits_a_bits_param),
    .io_schedule_bits_a_bits_block(c_mshr_io_schedule_bits_a_bits_block),
    .io_schedule_bits_b_valid(c_mshr_io_schedule_bits_b_valid),
    .io_schedule_bits_b_bits_param(c_mshr_io_schedule_bits_b_bits_param),
    .io_schedule_bits_b_bits_tag(c_mshr_io_schedule_bits_b_bits_tag),
    .io_schedule_bits_b_bits_set(c_mshr_io_schedule_bits_b_bits_set),
    .io_schedule_bits_b_bits_clients(c_mshr_io_schedule_bits_b_bits_clients),
    .io_schedule_bits_c_valid(c_mshr_io_schedule_bits_c_valid),
    .io_schedule_bits_c_bits_opcode(c_mshr_io_schedule_bits_c_bits_opcode),
    .io_schedule_bits_c_bits_param(c_mshr_io_schedule_bits_c_bits_param),
    .io_schedule_bits_c_bits_tag(c_mshr_io_schedule_bits_c_bits_tag),
    .io_schedule_bits_c_bits_set(c_mshr_io_schedule_bits_c_bits_set),
    .io_schedule_bits_c_bits_way(c_mshr_io_schedule_bits_c_bits_way),
    .io_schedule_bits_c_bits_dirty(c_mshr_io_schedule_bits_c_bits_dirty),
    .io_schedule_bits_d_valid(c_mshr_io_schedule_bits_d_valid),
    .io_schedule_bits_d_bits_prio_2(c_mshr_io_schedule_bits_d_bits_prio_2),
    .io_schedule_bits_d_bits_opcode(c_mshr_io_schedule_bits_d_bits_opcode),
    .io_schedule_bits_d_bits_param(c_mshr_io_schedule_bits_d_bits_param),
    .io_schedule_bits_d_bits_size(c_mshr_io_schedule_bits_d_bits_size),
    .io_schedule_bits_d_bits_source(c_mshr_io_schedule_bits_d_bits_source),
    .io_schedule_bits_d_bits_offset(c_mshr_io_schedule_bits_d_bits_offset),
    .io_schedule_bits_d_bits_put(c_mshr_io_schedule_bits_d_bits_put),
    .io_schedule_bits_d_bits_set(c_mshr_io_schedule_bits_d_bits_set),
    .io_schedule_bits_d_bits_way(c_mshr_io_schedule_bits_d_bits_way),
    .io_schedule_bits_d_bits_bad(c_mshr_io_schedule_bits_d_bits_bad),
    .io_schedule_bits_d_bits_user_hit_cache(c_mshr_io_schedule_bits_d_bits_user_hit_cache),
    .io_schedule_bits_e_valid(c_mshr_io_schedule_bits_e_valid),
    .io_schedule_bits_e_bits_sink(c_mshr_io_schedule_bits_e_bits_sink),
    .io_schedule_bits_x_valid(c_mshr_io_schedule_bits_x_valid),
    .io_schedule_bits_dir_valid(c_mshr_io_schedule_bits_dir_valid),
    .io_schedule_bits_dir_bits_set(c_mshr_io_schedule_bits_dir_bits_set),
    .io_schedule_bits_dir_bits_way(c_mshr_io_schedule_bits_dir_bits_way),
    .io_schedule_bits_dir_bits_data_dirty(c_mshr_io_schedule_bits_dir_bits_data_dirty),
    .io_schedule_bits_dir_bits_data_state(c_mshr_io_schedule_bits_dir_bits_data_state),
    .io_schedule_bits_dir_bits_data_clients(c_mshr_io_schedule_bits_dir_bits_data_clients),
    .io_schedule_bits_dir_bits_data_tag(c_mshr_io_schedule_bits_dir_bits_data_tag),
    .io_schedule_bits_reload(c_mshr_io_schedule_bits_reload),
    .io_sinkc_valid(c_mshr_io_sinkc_valid),
    .io_sinkc_bits_last(c_mshr_io_sinkc_bits_last),
    .io_sinkc_bits_tag(c_mshr_io_sinkc_bits_tag),
    .io_sinkc_bits_source(c_mshr_io_sinkc_bits_source),
    .io_sinkc_bits_param(c_mshr_io_sinkc_bits_param),
    .io_sinkc_bits_data(c_mshr_io_sinkc_bits_data),
    .io_sinkd_valid(c_mshr_io_sinkd_valid),
    .io_sinkd_bits_last(c_mshr_io_sinkd_bits_last),
    .io_sinkd_bits_opcode(c_mshr_io_sinkd_bits_opcode),
    .io_sinkd_bits_param(c_mshr_io_sinkd_bits_param),
    .io_sinkd_bits_sink(c_mshr_io_sinkd_bits_sink),
    .io_sinkd_bits_denied(c_mshr_io_sinkd_bits_denied),
    .io_sinke_valid(c_mshr_io_sinke_valid),
    .io_nestedwb_set(c_mshr_io_nestedwb_set),
    .io_nestedwb_tag(c_mshr_io_nestedwb_tag),
    .io_nestedwb_b_toN(c_mshr_io_nestedwb_b_toN),
    .io_nestedwb_b_toB(c_mshr_io_nestedwb_b_toB),
    .io_nestedwb_b_clr_dirty(c_mshr_io_nestedwb_b_clr_dirty),
    .io_nestedwb_c_set_dirty(c_mshr_io_nestedwb_c_set_dirty)
  );
  assign sourceA_rf_reset = rf_reset;
  assign sourceB_rf_reset = rf_reset;
  assign sourceC_rf_reset = rf_reset;
  assign sourceD_rf_reset = rf_reset;
  assign sourceE_rf_reset = rf_reset;
  assign sinkA_rf_reset = rf_reset;
  assign sinkC_rf_reset = rf_reset;
  assign sinkD_rf_reset = rf_reset;
  assign sinkX_rf_reset = rf_reset;
  assign directory_rf_reset = rf_reset;
  assign bankedStore_rf_reset = rf_reset;
  assign requests_rf_reset = rf_reset;
  assign abc_mshrs_0_rf_reset = rf_reset;
  assign abc_mshrs_1_rf_reset = rf_reset;
  assign abc_mshrs_2_rf_reset = rf_reset;
  assign abc_mshrs_3_rf_reset = rf_reset;
  assign abc_mshrs_4_rf_reset = rf_reset;
  assign abc_mshrs_5_rf_reset = rf_reset;
  assign bc_mshr_rf_reset = rf_reset;
  assign c_mshr_rf_reset = rf_reset;
  assign io_in_a_ready = sinkA_io_a_ready; // @[Scheduler.scala 95:14]
  assign io_in_b_valid = sourceB_io_b_valid; // @[Scheduler.scala 84:11]
  assign io_in_b_bits_param = sourceB_io_b_bits_param; // @[Scheduler.scala 84:11]
  assign io_in_b_bits_source = sourceB_io_b_bits_source; // @[Scheduler.scala 84:11]
  assign io_in_b_bits_address = sourceB_io_b_bits_address; // @[Scheduler.scala 84:11]
  assign io_in_c_ready = sinkC_io_c_ready; // @[Scheduler.scala 96:14]
  assign io_in_d_valid = sourceD_io_d_valid; // @[Scheduler.scala 85:11]
  assign io_in_d_bits_opcode = sourceD_io_d_bits_opcode; // @[Scheduler.scala 85:11]
  assign io_in_d_bits_param = sourceD_io_d_bits_param; // @[Scheduler.scala 85:11]
  assign io_in_d_bits_size = sourceD_io_d_bits_size; // @[Scheduler.scala 85:11]
  assign io_in_d_bits_source = sourceD_io_d_bits_source; // @[Scheduler.scala 85:11]
  assign io_in_d_bits_sink = sourceD_io_d_bits_sink; // @[Scheduler.scala 85:11]
  assign io_in_d_bits_denied = sourceD_io_d_bits_denied; // @[Scheduler.scala 85:11]
  assign io_in_d_bits_data = sourceD_io_d_bits_data; // @[Scheduler.scala 85:11]
  assign io_in_d_bits_corrupt = sourceD_io_d_bits_corrupt; // @[Scheduler.scala 85:11]
  assign io_out_a_valid = sourceA_io_a_valid; // @[Scheduler.scala 81:12]
  assign io_out_a_bits_opcode = sourceA_io_a_bits_opcode; // @[Scheduler.scala 81:12]
  assign io_out_a_bits_param = sourceA_io_a_bits_param; // @[Scheduler.scala 81:12]
  assign io_out_a_bits_source = sourceA_io_a_bits_source; // @[Scheduler.scala 81:12]
  assign io_out_a_bits_address = sourceA_io_a_bits_address; // @[Scheduler.scala 81:12]
  assign io_out_c_valid = sourceC_io_c_valid; // @[Scheduler.scala 82:12]
  assign io_out_c_bits_opcode = sourceC_io_c_bits_opcode; // @[Scheduler.scala 82:12]
  assign io_out_c_bits_param = sourceC_io_c_bits_param; // @[Scheduler.scala 82:12]
  assign io_out_c_bits_source = sourceC_io_c_bits_source; // @[Scheduler.scala 82:12]
  assign io_out_c_bits_address = sourceC_io_c_bits_address; // @[Scheduler.scala 82:12]
  assign io_out_c_bits_data = sourceC_io_c_bits_data; // @[Scheduler.scala 82:12]
  assign io_out_d_ready = sinkD_io_d_ready; // @[Scheduler.scala 99:14]
  assign io_out_e_valid = sourceE_io_e_valid; // @[Scheduler.scala 83:12]
  assign io_out_e_bits_sink = sourceE_io_e_bits_sink; // @[Scheduler.scala 83:12]
  assign io_req_ready = sinkX_io_x_ready; // @[Scheduler.scala 100:14]
  assign io_resp_valid = sourceX_io_x_valid; // @[Scheduler.scala 86:11]
  assign io_side_rdat_data = bankedStore_io_side_rdat_data; // @[Scheduler.scala 406:16]
  assign io_error_ready = io_error_ready_REG; // @[Scheduler.scala 440:18]
  assign sourceA_clock = clock;
  assign sourceA_reset = reset;
  assign sourceA_io_req_valid = mshr_selectOH[0] & abc_mshrs_0_io_schedule_bits_a_valid | mshr_selectOH[1] &
    abc_mshrs_1_io_schedule_bits_a_valid | mshr_selectOH[2] & abc_mshrs_2_io_schedule_bits_a_valid | mshr_selectOH[3] &
    abc_mshrs_3_io_schedule_bits_a_valid | mshr_selectOH[4] & abc_mshrs_4_io_schedule_bits_a_valid | mshr_selectOH[5] &
    abc_mshrs_5_io_schedule_bits_a_valid | mshr_selectOH[6] & bc_mshr_io_schedule_bits_a_valid | mshr_selectOH[7] &
    c_mshr_io_schedule_bits_a_valid; // @[Mux.scala 27:72]
  assign sourceA_io_req_bits_tag = _schedule_T_741 | _schedule_T_735; // @[Mux.scala 27:72]
  assign sourceA_io_req_bits_set = _schedule_T_726 | _schedule_T_720; // @[Mux.scala 27:72]
  assign sourceA_io_req_bits_param = _schedule_T_711 | _schedule_T_705; // @[Mux.scala 27:72]
  assign sourceA_io_req_bits_source = {mshr_select_hi_1,mshr_select_lo_3}; // @[Cat.scala 30:58]
  assign sourceA_io_req_bits_block = mshr_selectOH[0] & abc_mshrs_0_io_schedule_bits_a_bits_block | mshr_selectOH[1] &
    abc_mshrs_1_io_schedule_bits_a_bits_block | mshr_selectOH[2] & abc_mshrs_2_io_schedule_bits_a_bits_block |
    mshr_selectOH[3] & abc_mshrs_3_io_schedule_bits_a_bits_block | mshr_selectOH[4] &
    abc_mshrs_4_io_schedule_bits_a_bits_block | mshr_selectOH[5] & abc_mshrs_5_io_schedule_bits_a_bits_block |
    mshr_selectOH[6] & bc_mshr_io_schedule_bits_a_bits_block | mshr_selectOH[7] & c_mshr_io_schedule_bits_a_bits_block; // @[Mux.scala 27:72]
  assign sourceA_io_a_ready = io_out_a_ready; // @[Scheduler.scala 81:12]
  assign sourceB_clock = clock;
  assign sourceB_reset = reset;
  assign sourceB_io_req_valid = mshr_selectOH[0] & abc_mshrs_0_io_schedule_bits_b_valid | mshr_selectOH[1] &
    abc_mshrs_1_io_schedule_bits_b_valid | mshr_selectOH[2] & abc_mshrs_2_io_schedule_bits_b_valid | mshr_selectOH[3] &
    abc_mshrs_3_io_schedule_bits_b_valid | mshr_selectOH[4] & abc_mshrs_4_io_schedule_bits_b_valid | mshr_selectOH[5] &
    abc_mshrs_5_io_schedule_bits_b_valid | mshr_selectOH[6] & bc_mshr_io_schedule_bits_b_valid | mshr_selectOH[7] &
    c_mshr_io_schedule_bits_b_valid; // @[Mux.scala 27:72]
  assign sourceB_io_req_bits_param = _schedule_T_651 | _schedule_T_645; // @[Mux.scala 27:72]
  assign sourceB_io_req_bits_tag = _schedule_T_636 | _schedule_T_630; // @[Mux.scala 27:72]
  assign sourceB_io_req_bits_set = _schedule_T_621 | _schedule_T_615; // @[Mux.scala 27:72]
  assign sourceB_io_req_bits_clients = _schedule_T_606 | _schedule_T_600; // @[Mux.scala 27:72]
  assign sourceB_io_b_ready = io_in_b_ready; // @[Scheduler.scala 84:11]
  assign sourceC_clock = clock;
  assign sourceC_reset = reset;
  assign sourceC_io_req_valid = mshr_selectOH[0] & abc_mshrs_0_io_schedule_bits_c_valid | mshr_selectOH[1] &
    abc_mshrs_1_io_schedule_bits_c_valid | mshr_selectOH[2] & abc_mshrs_2_io_schedule_bits_c_valid | mshr_selectOH[3] &
    abc_mshrs_3_io_schedule_bits_c_valid | mshr_selectOH[4] & abc_mshrs_4_io_schedule_bits_c_valid | mshr_selectOH[5] &
    abc_mshrs_5_io_schedule_bits_c_valid | mshr_selectOH[6] & bc_mshr_io_schedule_bits_c_valid | mshr_selectOH[7] &
    c_mshr_io_schedule_bits_c_valid; // @[Mux.scala 27:72]
  assign sourceC_io_req_bits_opcode = _schedule_T_576 | _schedule_T_570; // @[Mux.scala 27:72]
  assign sourceC_io_req_bits_param = _schedule_T_561 | _schedule_T_555; // @[Mux.scala 27:72]
  assign sourceC_io_req_bits_source = schedule_c_bits_opcode[1] ? mshr_select : 3'h0; // @[Scheduler.scala 168:32]
  assign sourceC_io_req_bits_tag = _schedule_T_531 | _schedule_T_525; // @[Mux.scala 27:72]
  assign sourceC_io_req_bits_set = _schedule_T_516 | _schedule_T_510; // @[Mux.scala 27:72]
  assign sourceC_io_req_bits_way = _schedule_T_501 | _schedule_T_495; // @[Mux.scala 27:72]
  assign sourceC_io_req_bits_dirty = mshr_selectOH[0] & abc_mshrs_0_io_schedule_bits_c_bits_dirty | mshr_selectOH[1] &
    abc_mshrs_1_io_schedule_bits_c_bits_dirty | mshr_selectOH[2] & abc_mshrs_2_io_schedule_bits_c_bits_dirty |
    mshr_selectOH[3] & abc_mshrs_3_io_schedule_bits_c_bits_dirty | mshr_selectOH[4] &
    abc_mshrs_4_io_schedule_bits_c_bits_dirty | mshr_selectOH[5] & abc_mshrs_5_io_schedule_bits_c_bits_dirty |
    mshr_selectOH[6] & bc_mshr_io_schedule_bits_c_bits_dirty | mshr_selectOH[7] & c_mshr_io_schedule_bits_c_bits_dirty; // @[Mux.scala 27:72]
  assign sourceC_io_c_ready = io_out_c_ready; // @[Scheduler.scala 82:12]
  assign sourceC_io_bs_adr_ready = bankedStore_io_sourceC_adr_ready; // @[Scheduler.scala 389:30]
  assign sourceC_io_bs_dat_data = bankedStore_io_sourceC_dat_data; // @[Scheduler.scala 393:21]
  assign sourceC_io_evict_safe = sourceD_io_evict_safe; // @[Scheduler.scala 399:25]
  assign sourceD_clock = clock;
  assign sourceD_reset = reset;
  assign sourceD_io_req_valid = mshr_selectOH[0] & abc_mshrs_0_io_schedule_bits_d_valid | mshr_selectOH[1] &
    abc_mshrs_1_io_schedule_bits_d_valid | mshr_selectOH[2] & abc_mshrs_2_io_schedule_bits_d_valid | mshr_selectOH[3] &
    abc_mshrs_3_io_schedule_bits_d_valid | mshr_selectOH[4] & abc_mshrs_4_io_schedule_bits_d_valid | mshr_selectOH[5] &
    abc_mshrs_5_io_schedule_bits_d_valid | mshr_selectOH[6] & bc_mshr_io_schedule_bits_d_valid | mshr_selectOH[7] &
    c_mshr_io_schedule_bits_d_valid; // @[Mux.scala 27:72]
  assign sourceD_io_req_bits_prio_0 = mshr_selectOH[0] & abc_mshrs_0_io_schedule_bits_d_bits_prio_0 | mshr_selectOH[1]
     & abc_mshrs_1_io_schedule_bits_d_bits_prio_0 | mshr_selectOH[2] & abc_mshrs_2_io_schedule_bits_d_bits_prio_0 |
    mshr_selectOH[3] & abc_mshrs_3_io_schedule_bits_d_bits_prio_0 | mshr_selectOH[4] &
    abc_mshrs_4_io_schedule_bits_d_bits_prio_0 | mshr_selectOH[5] & abc_mshrs_5_io_schedule_bits_d_bits_prio_0; // @[Mux.scala 27:72]
  assign sourceD_io_req_bits_prio_2 = mshr_selectOH[0] & abc_mshrs_0_io_schedule_bits_d_bits_prio_2 | mshr_selectOH[1]
     & abc_mshrs_1_io_schedule_bits_d_bits_prio_2 | mshr_selectOH[2] & abc_mshrs_2_io_schedule_bits_d_bits_prio_2 |
    mshr_selectOH[3] & abc_mshrs_3_io_schedule_bits_d_bits_prio_2 | mshr_selectOH[4] &
    abc_mshrs_4_io_schedule_bits_d_bits_prio_2 | mshr_selectOH[5] & abc_mshrs_5_io_schedule_bits_d_bits_prio_2 |
    mshr_selectOH[6] & bc_mshr_io_schedule_bits_d_bits_prio_2 | mshr_selectOH[7] & c_mshr_io_schedule_bits_d_bits_prio_2
    ; // @[Mux.scala 27:72]
  assign sourceD_io_req_bits_opcode = _schedule_T_396 | _schedule_T_390; // @[Mux.scala 27:72]
  assign sourceD_io_req_bits_param = _schedule_T_381 | _schedule_T_375; // @[Mux.scala 27:72]
  assign sourceD_io_req_bits_size = _schedule_T_366 | _schedule_T_360; // @[Mux.scala 27:72]
  assign sourceD_io_req_bits_source = _schedule_T_351 | _schedule_T_345; // @[Mux.scala 27:72]
  assign sourceD_io_req_bits_offset = _schedule_T_321 | _schedule_T_315; // @[Mux.scala 27:72]
  assign sourceD_io_req_bits_put = _schedule_T_306 | _schedule_T_300; // @[Mux.scala 27:72]
  assign sourceD_io_req_bits_set = _schedule_T_291 | _schedule_T_285; // @[Mux.scala 27:72]
  assign sourceD_io_req_bits_sink = {mshr_select_hi_1,mshr_select_lo_3}; // @[Cat.scala 30:58]
  assign sourceD_io_req_bits_way = _schedule_T_261 | _schedule_T_255; // @[Mux.scala 27:72]
  assign sourceD_io_req_bits_bad = mshr_selectOH[0] & abc_mshrs_0_io_schedule_bits_d_bits_bad | mshr_selectOH[1] &
    abc_mshrs_1_io_schedule_bits_d_bits_bad | mshr_selectOH[2] & abc_mshrs_2_io_schedule_bits_d_bits_bad | mshr_selectOH
    [3] & abc_mshrs_3_io_schedule_bits_d_bits_bad | mshr_selectOH[4] & abc_mshrs_4_io_schedule_bits_d_bits_bad |
    mshr_selectOH[5] & abc_mshrs_5_io_schedule_bits_d_bits_bad | mshr_selectOH[6] & bc_mshr_io_schedule_bits_d_bits_bad
     | mshr_selectOH[7] & c_mshr_io_schedule_bits_d_bits_bad; // @[Mux.scala 27:72]
  assign sourceD_io_req_bits_user_hit_cache = mshr_selectOH[0] & abc_mshrs_0_io_schedule_bits_d_bits_user_hit_cache |
    mshr_selectOH[1] & abc_mshrs_1_io_schedule_bits_d_bits_user_hit_cache | mshr_selectOH[2] &
    abc_mshrs_2_io_schedule_bits_d_bits_user_hit_cache | mshr_selectOH[3] &
    abc_mshrs_3_io_schedule_bits_d_bits_user_hit_cache | mshr_selectOH[4] &
    abc_mshrs_4_io_schedule_bits_d_bits_user_hit_cache | mshr_selectOH[5] &
    abc_mshrs_5_io_schedule_bits_d_bits_user_hit_cache | mshr_selectOH[6] &
    bc_mshr_io_schedule_bits_d_bits_user_hit_cache | mshr_selectOH[7] & c_mshr_io_schedule_bits_d_bits_user_hit_cache; // @[Mux.scala 27:72]
  assign sourceD_io_d_ready = io_in_d_ready; // @[Scheduler.scala 85:11]
  assign sourceD_io_pb_pop_ready = sinkA_io_pb_pop_ready; // @[Scheduler.scala 379:19]
  assign sourceD_io_pb_beat_data = sinkA_io_pb_beat_data; // @[Scheduler.scala 380:22]
  assign sourceD_io_pb_beat_mask = sinkA_io_pb_beat_mask; // @[Scheduler.scala 380:22]
  assign sourceD_io_pb_beat_corrupt = sinkA_io_pb_beat_corrupt; // @[Scheduler.scala 380:22]
  assign sourceD_io_rel_pop_ready = sinkC_io_rel_pop_ready; // @[Scheduler.scala 381:20]
  assign sourceD_io_rel_beat_data = sinkC_io_rel_beat_data; // @[Scheduler.scala 382:23]
  assign sourceD_io_rel_beat_corrupt = sinkC_io_rel_beat_corrupt; // @[Scheduler.scala 382:23]
  assign sourceD_io_bs_radr_ready = bankedStore_io_sourceD_radr_ready; // @[Scheduler.scala 390:31]
  assign sourceD_io_bs_rdat_data = bankedStore_io_sourceD_rdat_data; // @[Scheduler.scala 394:22]
  assign sourceD_io_bs_wadr_ready = bankedStore_io_sourceD_wadr_ready; // @[Scheduler.scala 391:31]
  assign sourceD_io_evict_req_set = sourceC_io_evict_req_set; // @[Scheduler.scala 397:24]
  assign sourceD_io_evict_req_way = sourceC_io_evict_req_way; // @[Scheduler.scala 397:24]
  assign sourceD_io_grant_req_set = sinkD_io_grant_req_set; // @[Scheduler.scala 398:24]
  assign sourceD_io_grant_req_way = sinkD_io_grant_req_way; // @[Scheduler.scala 398:24]
  assign sourceE_clock = clock;
  assign sourceE_reset = reset;
  assign sourceE_io_req_valid = mshr_selectOH[0] & abc_mshrs_0_io_schedule_bits_e_valid | mshr_selectOH[1] &
    abc_mshrs_1_io_schedule_bits_e_valid | mshr_selectOH[2] & abc_mshrs_2_io_schedule_bits_e_valid | mshr_selectOH[3] &
    abc_mshrs_3_io_schedule_bits_e_valid | mshr_selectOH[4] & abc_mshrs_4_io_schedule_bits_e_valid | mshr_selectOH[5] &
    abc_mshrs_5_io_schedule_bits_e_valid | mshr_selectOH[6] & bc_mshr_io_schedule_bits_e_valid | mshr_selectOH[7] &
    c_mshr_io_schedule_bits_e_valid; // @[Mux.scala 27:72]
  assign sourceE_io_req_bits_sink = _schedule_T_171 | _schedule_T_165; // @[Mux.scala 27:72]
  assign sourceX_clock = clock;
  assign sourceX_reset = reset;
  assign sourceX_io_req_valid = mshr_selectOH[0] & abc_mshrs_0_io_schedule_bits_x_valid | mshr_selectOH[1] &
    abc_mshrs_1_io_schedule_bits_x_valid | mshr_selectOH[2] & abc_mshrs_2_io_schedule_bits_x_valid | mshr_selectOH[3] &
    abc_mshrs_3_io_schedule_bits_x_valid | mshr_selectOH[4] & abc_mshrs_4_io_schedule_bits_x_valid | mshr_selectOH[5] &
    abc_mshrs_5_io_schedule_bits_x_valid | mshr_selectOH[6] & bc_mshr_io_schedule_bits_x_valid | mshr_selectOH[7] &
    c_mshr_io_schedule_bits_x_valid; // @[Mux.scala 27:72]
  assign sourceX_io_x_ready = io_resp_ready; // @[Scheduler.scala 86:11]
  assign sinkA_clock = clock;
  assign sinkA_reset = reset;
  assign sinkA_io_req_ready = _sinkX_io_req_ready_T_2 & ~sinkX_io_req_valid; // @[Scheduler.scala 199:107]
  assign sinkA_io_a_valid = io_in_a_valid; // @[Scheduler.scala 95:14]
  assign sinkA_io_a_bits_opcode = io_in_a_bits_opcode; // @[Scheduler.scala 95:14]
  assign sinkA_io_a_bits_param = io_in_a_bits_param; // @[Scheduler.scala 95:14]
  assign sinkA_io_a_bits_size = io_in_a_bits_size; // @[Scheduler.scala 95:14]
  assign sinkA_io_a_bits_source = io_in_a_bits_source; // @[Scheduler.scala 95:14]
  assign sinkA_io_a_bits_address = io_in_a_bits_address; // @[Scheduler.scala 95:14]
  assign sinkA_io_a_bits_mask = io_in_a_bits_mask; // @[Scheduler.scala 95:14]
  assign sinkA_io_a_bits_data = io_in_a_bits_data; // @[Scheduler.scala 95:14]
  assign sinkA_io_a_bits_corrupt = io_in_a_bits_corrupt; // @[Scheduler.scala 95:14]
  assign sinkA_io_pb_pop_valid = sourceD_io_pb_pop_valid; // @[Scheduler.scala 379:19]
  assign sinkA_io_pb_pop_bits_index = sourceD_io_pb_pop_bits_index; // @[Scheduler.scala 379:19]
  assign sinkA_io_pb_pop_bits_last = sourceD_io_pb_pop_bits_last; // @[Scheduler.scala 379:19]
  assign sinkC_clock = clock;
  assign sinkC_reset = reset;
  assign sinkC_io_req_ready = directory_io_ready & request_ready; // @[Scheduler.scala 196:44]
  assign sinkC_io_c_valid = io_in_c_valid; // @[Scheduler.scala 96:14]
  assign sinkC_io_c_bits_opcode = io_in_c_bits_opcode; // @[Scheduler.scala 96:14]
  assign sinkC_io_c_bits_param = io_in_c_bits_param; // @[Scheduler.scala 96:14]
  assign sinkC_io_c_bits_size = io_in_c_bits_size; // @[Scheduler.scala 96:14]
  assign sinkC_io_c_bits_source = io_in_c_bits_source; // @[Scheduler.scala 96:14]
  assign sinkC_io_c_bits_address = io_in_c_bits_address; // @[Scheduler.scala 96:14]
  assign sinkC_io_c_bits_data = io_in_c_bits_data; // @[Scheduler.scala 96:14]
  assign sinkC_io_c_bits_corrupt = io_in_c_bits_corrupt; // @[Scheduler.scala 96:14]
  assign sinkC_io_way = bc_mshr_io_status_valid & bc_mshr_io_status_bits_set == sinkC_io_set ?
    bc_mshr_io_status_bits_way : _sinkC_io_way_T_24; // @[Scheduler.scala 371:8]
  assign sinkC_io_bs_adr_ready = bankedStore_io_sinkC_adr_ready; // @[Scheduler.scala 385:28]
  assign sinkC_io_rel_pop_valid = sourceD_io_rel_pop_valid; // @[Scheduler.scala 381:20]
  assign sinkC_io_rel_pop_bits_index = sourceD_io_rel_pop_bits_index; // @[Scheduler.scala 381:20]
  assign sinkC_io_rel_pop_bits_last = sourceD_io_rel_pop_bits_last; // @[Scheduler.scala 381:20]
  assign sinkD_clock = clock;
  assign sinkD_reset = reset;
  assign sinkD_io_d_valid = io_out_d_valid; // @[Scheduler.scala 99:14]
  assign sinkD_io_d_bits_opcode = io_out_d_bits_opcode; // @[Scheduler.scala 99:14]
  assign sinkD_io_d_bits_param = io_out_d_bits_param; // @[Scheduler.scala 99:14]
  assign sinkD_io_d_bits_size = io_out_d_bits_size; // @[Scheduler.scala 99:14]
  assign sinkD_io_d_bits_source = io_out_d_bits_source; // @[Scheduler.scala 99:14]
  assign sinkD_io_d_bits_sink = io_out_d_bits_sink; // @[Scheduler.scala 99:14]
  assign sinkD_io_d_bits_denied = io_out_d_bits_denied; // @[Scheduler.scala 99:14]
  assign sinkD_io_d_bits_data = io_out_d_bits_data; // @[Scheduler.scala 99:14]
  assign sinkD_io_d_bits_corrupt = io_out_d_bits_corrupt; // @[Scheduler.scala 99:14]
  assign sinkD_io_way = 3'h7 == sinkD_io_source ? _sinkD_io_way_WIRE_7 : _GEN_147; // @[Scheduler.scala 375:16 Scheduler.scala 375:16]
  assign sinkD_io_set = 3'h7 == sinkD_io_source ? _sinkD_io_set_WIRE_7 : _GEN_155; // @[Scheduler.scala 376:16 Scheduler.scala 376:16]
  assign sinkD_io_bs_adr_ready = bankedStore_io_sinkD_adr_ready; // @[Scheduler.scala 387:28]
  assign sinkD_io_grant_safe = sourceD_io_grant_safe; // @[Scheduler.scala 400:25]
  assign sinkE_io_e_valid = io_in_e_valid; // @[Scheduler.scala 97:14]
  assign sinkE_io_e_bits_sink = io_in_e_bits_sink; // @[Scheduler.scala 97:14]
  assign sinkX_clock = clock;
  assign sinkX_reset = reset;
  assign sinkX_io_req_ready = _sinkC_io_req_ready_T & ~sinkC_io_req_valid; // @[Scheduler.scala 198:61]
  assign sinkX_io_x_valid = io_req_valid; // @[Scheduler.scala 100:14]
  assign sinkX_io_x_bits_address = io_req_bits_address; // @[Scheduler.scala 100:14]
  assign directory_clock = clock;
  assign directory_reset = reset;
  assign directory_io_write_valid = mshr_selectOH[0] & abc_mshrs_0_io_schedule_bits_dir_valid | mshr_selectOH[1] &
    abc_mshrs_1_io_schedule_bits_dir_valid | mshr_selectOH[2] & abc_mshrs_2_io_schedule_bits_dir_valid | mshr_selectOH[3
    ] & abc_mshrs_3_io_schedule_bits_dir_valid | mshr_selectOH[4] & abc_mshrs_4_io_schedule_bits_dir_valid |
    mshr_selectOH[5] & abc_mshrs_5_io_schedule_bits_dir_valid | mshr_selectOH[6] & bc_mshr_io_schedule_bits_dir_valid |
    mshr_selectOH[7] & c_mshr_io_schedule_bits_dir_valid; // @[Mux.scala 27:72]
  assign directory_io_write_bits_set = _schedule_T_111 | _schedule_T_105; // @[Mux.scala 27:72]
  assign directory_io_write_bits_way = _schedule_T_96 | _schedule_T_90; // @[Mux.scala 27:72]
  assign directory_io_write_bits_data_dirty = mshr_selectOH[0] & abc_mshrs_0_io_schedule_bits_dir_bits_data_dirty |
    mshr_selectOH[1] & abc_mshrs_1_io_schedule_bits_dir_bits_data_dirty | mshr_selectOH[2] &
    abc_mshrs_2_io_schedule_bits_dir_bits_data_dirty | mshr_selectOH[3] &
    abc_mshrs_3_io_schedule_bits_dir_bits_data_dirty | mshr_selectOH[4] &
    abc_mshrs_4_io_schedule_bits_dir_bits_data_dirty | mshr_selectOH[5] &
    abc_mshrs_5_io_schedule_bits_dir_bits_data_dirty | mshr_selectOH[6] & bc_mshr_io_schedule_bits_dir_bits_data_dirty
     | mshr_selectOH[7] & c_mshr_io_schedule_bits_dir_bits_data_dirty; // @[Mux.scala 27:72]
  assign directory_io_write_bits_data_state = _schedule_T_66 | _schedule_T_60; // @[Mux.scala 27:72]
  assign directory_io_write_bits_data_clients = _schedule_T_51 | _schedule_T_45; // @[Mux.scala 27:72]
  assign directory_io_write_bits_data_tag = _schedule_T_36 | _schedule_T_30; // @[Mux.scala 27:72]
  assign directory_io_read_valid = mshr_uses_directory | alloc_uses_directory; // @[Scheduler.scala 295:50]
  assign directory_io_read_bits_source = mshr_uses_directory_for_lb ? requests_io_data_source : request_bits_source; // @[Scheduler.scala 298:39]
  assign directory_io_read_bits_set = mshr_uses_directory_for_lb ? scheduleSet : request_bits_set; // @[Scheduler.scala 296:39]
  assign directory_io_read_bits_tag = mshr_uses_directory_for_lb ? requests_io_data_tag : request_bits_tag; // @[Scheduler.scala 297:39]
  assign directory_io_ways_0 = REG__0; // @[Scheduler.scala 410:22]
  assign directory_io_ways_1 = REG__1; // @[Scheduler.scala 410:22]
  assign directory_io_ways_2 = REG__2; // @[Scheduler.scala 410:22]
  assign directory_io_ways_3 = REG__3; // @[Scheduler.scala 410:22]
  assign directory_io_ways_4 = REG__4; // @[Scheduler.scala 410:22]
  assign directory_io_ways_5 = REG__5; // @[Scheduler.scala 410:22]
  assign directory_io_ways_6 = REG__6; // @[Scheduler.scala 410:22]
  assign directory_io_ways_7 = REG__7; // @[Scheduler.scala 410:22]
  assign directory_io_ways_8 = REG__8; // @[Scheduler.scala 410:22]
  assign directory_io_ways_9 = REG__9; // @[Scheduler.scala 410:22]
  assign directory_io_ways_10 = REG__10; // @[Scheduler.scala 410:22]
  assign directory_io_ways_11 = REG__11; // @[Scheduler.scala 410:22]
  assign directory_io_ways_12 = REG__12; // @[Scheduler.scala 410:22]
  assign directory_io_ways_13 = REG__13; // @[Scheduler.scala 410:22]
  assign directory_io_divs_0 = REG_1_0; // @[Scheduler.scala 411:22]
  assign directory_io_divs_1 = REG_1_1; // @[Scheduler.scala 411:22]
  assign directory_io_divs_2 = REG_1_2; // @[Scheduler.scala 411:22]
  assign directory_io_divs_3 = REG_1_3; // @[Scheduler.scala 411:22]
  assign directory_io_divs_4 = REG_1_4; // @[Scheduler.scala 411:22]
  assign directory_io_divs_5 = REG_1_5; // @[Scheduler.scala 411:22]
  assign directory_io_divs_6 = REG_1_6; // @[Scheduler.scala 411:22]
  assign directory_io_divs_7 = REG_1_7; // @[Scheduler.scala 411:22]
  assign directory_io_divs_8 = REG_1_8; // @[Scheduler.scala 411:22]
  assign directory_io_divs_9 = REG_1_9; // @[Scheduler.scala 411:22]
  assign directory_io_divs_10 = REG_1_10; // @[Scheduler.scala 411:22]
  assign directory_io_divs_11 = REG_1_11; // @[Scheduler.scala 411:22]
  assign directory_io_divs_12 = REG_1_12; // @[Scheduler.scala 411:22]
  assign directory_io_divs_13 = REG_1_13; // @[Scheduler.scala 411:22]
  assign directory_io_error_valid = injectValid & injectBits_dir; // @[Scheduler.scala 441:43]
  assign directory_io_error_bits_bit = injectBits_bit; // @[Scheduler.scala 442:28]
  assign bankedStore_clock = clock;
  assign bankedStore_reset = reset;
  assign bankedStore_io_side_adr_valid = bankedStore_io_side_adr_valid_REG; // @[Scheduler.scala 403:33]
  assign bankedStore_io_side_adr_bits_way = bankedStore_io_side_adr_bits_r_way; // @[Scheduler.scala 404:33]
  assign bankedStore_io_side_adr_bits_set = bankedStore_io_side_adr_bits_r_set; // @[Scheduler.scala 404:33]
  assign bankedStore_io_side_adr_bits_beat = bankedStore_io_side_adr_bits_r_beat; // @[Scheduler.scala 404:33]
  assign bankedStore_io_side_adr_bits_mask = bankedStore_io_side_adr_bits_r_mask; // @[Scheduler.scala 404:33]
  assign bankedStore_io_side_adr_bits_write = bankedStore_io_side_adr_bits_r_write; // @[Scheduler.scala 404:33]
  assign bankedStore_io_side_wdat_data = bankedStore_io_side_wdat_r_data; // @[Scheduler.scala 405:33]
  assign bankedStore_io_side_wdat_poison = bankedStore_io_side_wdat_r_poison; // @[Scheduler.scala 405:33]
  assign bankedStore_io_sinkC_adr_valid = sinkC_io_bs_adr_valid; // @[Scheduler.scala 385:28]
  assign bankedStore_io_sinkC_adr_bits_noop = sinkC_io_bs_adr_bits_noop; // @[Scheduler.scala 385:28]
  assign bankedStore_io_sinkC_adr_bits_way = sinkC_io_bs_adr_bits_way; // @[Scheduler.scala 385:28]
  assign bankedStore_io_sinkC_adr_bits_set = sinkC_io_bs_adr_bits_set; // @[Scheduler.scala 385:28]
  assign bankedStore_io_sinkC_adr_bits_beat = sinkC_io_bs_adr_bits_beat; // @[Scheduler.scala 385:28]
  assign bankedStore_io_sinkC_dat_data = sinkC_io_bs_dat_data; // @[Scheduler.scala 386:28]
  assign bankedStore_io_sinkC_dat_poison = sinkC_io_bs_dat_poison; // @[Scheduler.scala 386:28]
  assign bankedStore_io_sinkD_adr_valid = sinkD_io_bs_adr_valid; // @[Scheduler.scala 387:28]
  assign bankedStore_io_sinkD_adr_bits_noop = sinkD_io_bs_adr_bits_noop; // @[Scheduler.scala 387:28]
  assign bankedStore_io_sinkD_adr_bits_way = sinkD_io_bs_adr_bits_way; // @[Scheduler.scala 387:28]
  assign bankedStore_io_sinkD_adr_bits_set = sinkD_io_bs_adr_bits_set; // @[Scheduler.scala 387:28]
  assign bankedStore_io_sinkD_adr_bits_beat = sinkD_io_bs_adr_bits_beat; // @[Scheduler.scala 387:28]
  assign bankedStore_io_sinkD_dat_data = sinkD_io_bs_dat_data; // @[Scheduler.scala 388:28]
  assign bankedStore_io_sourceC_adr_valid = sourceC_io_bs_adr_valid; // @[Scheduler.scala 389:30]
  assign bankedStore_io_sourceC_adr_bits_way = sourceC_io_bs_adr_bits_way; // @[Scheduler.scala 389:30]
  assign bankedStore_io_sourceC_adr_bits_set = sourceC_io_bs_adr_bits_set; // @[Scheduler.scala 389:30]
  assign bankedStore_io_sourceC_adr_bits_beat = sourceC_io_bs_adr_bits_beat; // @[Scheduler.scala 389:30]
  assign bankedStore_io_sourceD_radr_valid = sourceD_io_bs_radr_valid; // @[Scheduler.scala 390:31]
  assign bankedStore_io_sourceD_radr_bits_way = sourceD_io_bs_radr_bits_way; // @[Scheduler.scala 390:31]
  assign bankedStore_io_sourceD_radr_bits_set = sourceD_io_bs_radr_bits_set; // @[Scheduler.scala 390:31]
  assign bankedStore_io_sourceD_radr_bits_beat = sourceD_io_bs_radr_bits_beat; // @[Scheduler.scala 390:31]
  assign bankedStore_io_sourceD_radr_bits_mask = sourceD_io_bs_radr_bits_mask; // @[Scheduler.scala 390:31]
  assign bankedStore_io_sourceD_wadr_valid = sourceD_io_bs_wadr_valid; // @[Scheduler.scala 391:31]
  assign bankedStore_io_sourceD_wadr_bits_way = sourceD_io_bs_wadr_bits_way; // @[Scheduler.scala 391:31]
  assign bankedStore_io_sourceD_wadr_bits_set = sourceD_io_bs_wadr_bits_set; // @[Scheduler.scala 391:31]
  assign bankedStore_io_sourceD_wadr_bits_beat = sourceD_io_bs_wadr_bits_beat; // @[Scheduler.scala 391:31]
  assign bankedStore_io_sourceD_wadr_bits_mask = sourceD_io_bs_wadr_bits_mask; // @[Scheduler.scala 391:31]
  assign bankedStore_io_sourceD_wdat_data = sourceD_io_bs_wdat_data; // @[Scheduler.scala 392:31]
  assign bankedStore_io_sourceD_wdat_poison = sourceD_io_bs_wdat_poison; // @[Scheduler.scala 392:31]
  assign bankedStore_io_error_valid = injectValid & ~injectBits_dir; // @[Scheduler.scala 443:45]
  assign bankedStore_io_error_bits_bit = injectBits_bit; // @[Scheduler.scala 444:30]
  assign requests_clock = clock;
  assign requests_reset = reset;
  assign requests_io_push_valid = _T_14 & ~bypassQueue; // @[Scheduler.scala 305:52]
  assign requests_io_push_bits_index = _GEN_184 | _requests_io_push_bits_index_T_17; // @[Mux.scala 27:72]
  assign requests_io_push_bits_data_prio_0 = sinkC_io_req_valid ? 1'h0 : 1'h1; // @[Scheduler.scala 193:22]
  assign requests_io_push_bits_data_prio_2 = sinkC_io_req_valid; // @[Scheduler.scala 193:22]
  assign requests_io_push_bits_data_control = sinkC_io_req_valid ? 1'h0 : _request_bits_T_control; // @[Scheduler.scala 193:22]
  assign requests_io_push_bits_data_opcode = sinkC_io_req_valid ? sinkC_io_req_bits_opcode : _request_bits_T_opcode; // @[Scheduler.scala 193:22]
  assign requests_io_push_bits_data_param = sinkC_io_req_valid ? sinkC_io_req_bits_param : _request_bits_T_param; // @[Scheduler.scala 193:22]
  assign requests_io_push_bits_data_size = sinkC_io_req_valid ? sinkC_io_req_bits_size : _request_bits_T_size; // @[Scheduler.scala 193:22]
  assign requests_io_push_bits_data_source = sinkC_io_req_valid ? sinkC_io_req_bits_source : _request_bits_T_source; // @[Scheduler.scala 193:22]
  assign requests_io_push_bits_data_tag = sinkC_io_req_valid ? sinkC_io_req_bits_tag : _request_bits_T_tag; // @[Scheduler.scala 193:22]
  assign requests_io_push_bits_data_offset = sinkC_io_req_valid ? sinkC_io_req_bits_offset : _request_bits_T_offset; // @[Scheduler.scala 193:22]
  assign requests_io_push_bits_data_put = sinkC_io_req_valid ? sinkC_io_req_bits_put : _request_bits_T_put; // @[Scheduler.scala 193:22]
  assign requests_io_pop_valid = _mshr_uses_directory_assuming_no_bypass_T & ~bypass; // @[Scheduler.scala 245:45]
  assign requests_io_pop_bits = {pop_index_hi_2,pop_index_lo_7}; // @[Cat.scala 30:58]
  assign abc_mshrs_0_clock = clock;
  assign abc_mshrs_0_reset = reset;
  assign abc_mshrs_0_io_allocate_valid = request_valid & alloc & mshr_insertOH[0] & _request_alloc_cases_T |
    mshr_selectOH[0] & will_reload_1; // @[Scheduler.scala 315:83 Scheduler.scala 316:27 Scheduler.scala 266:25]
  assign abc_mshrs_0_io_allocate_bits_prio_0 = request_valid & alloc & mshr_insertOH[0] & _request_alloc_cases_T ?
    request_bits_prio_0 : _mshrs_0_io_allocate_bits_T_prio_0; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_0_io_allocate_bits_prio_2 = request_valid & alloc & mshr_insertOH[0] & _request_alloc_cases_T ?
    request_bits_prio_2 : _mshrs_0_io_allocate_bits_T_prio_2; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_0_io_allocate_bits_control = request_valid & alloc & mshr_insertOH[0] & _request_alloc_cases_T ?
    request_bits_control : _mshrs_0_io_allocate_bits_T_control; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_0_io_allocate_bits_opcode = request_valid & alloc & mshr_insertOH[0] & _request_alloc_cases_T ?
    request_bits_opcode : _mshrs_0_io_allocate_bits_T_opcode; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_0_io_allocate_bits_param = request_valid & alloc & mshr_insertOH[0] & _request_alloc_cases_T ?
    request_bits_param : _mshrs_0_io_allocate_bits_T_param; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_0_io_allocate_bits_size = request_valid & alloc & mshr_insertOH[0] & _request_alloc_cases_T ?
    request_bits_size : _mshrs_0_io_allocate_bits_T_size; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_0_io_allocate_bits_source = request_valid & alloc & mshr_insertOH[0] & _request_alloc_cases_T ?
    request_bits_source : _mshrs_0_io_allocate_bits_T_source; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_0_io_allocate_bits_tag = request_valid & alloc & mshr_insertOH[0] & _request_alloc_cases_T ?
    request_bits_tag : _mshrs_0_io_allocate_bits_T_tag; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_0_io_allocate_bits_offset = request_valid & alloc & mshr_insertOH[0] & _request_alloc_cases_T ?
    request_bits_offset : _mshrs_0_io_allocate_bits_T_offset; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_0_io_allocate_bits_put = request_valid & alloc & mshr_insertOH[0] & _request_alloc_cases_T ?
    request_bits_put : _mshrs_0_io_allocate_bits_T_put; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_0_io_allocate_bits_set = request_valid & alloc & mshr_insertOH[0] & _request_alloc_cases_T ?
    request_bits_set : abc_mshrs_0_io_status_bits_set; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 264:28]
  assign abc_mshrs_0_io_allocate_bits_repeat = request_valid & alloc & mshr_insertOH[0] & _request_alloc_cases_T ? 1'h0
     : abc_mshrs_0_io_allocate_bits_tag == abc_mshrs_0_io_status_bits_tag; // @[Scheduler.scala 315:83 Scheduler.scala 318:33 Scheduler.scala 265:31]
  assign abc_mshrs_0_io_directory_valid = directoryFanout[0]; // @[Scheduler.scala 344:44]
  assign abc_mshrs_0_io_directory_bits_dirty = directory_io_result_bits_dirty; // @[Scheduler.scala 345:25]
  assign abc_mshrs_0_io_directory_bits_state = directory_io_result_bits_state; // @[Scheduler.scala 345:25]
  assign abc_mshrs_0_io_directory_bits_clients = directory_io_result_bits_clients; // @[Scheduler.scala 345:25]
  assign abc_mshrs_0_io_directory_bits_tag = directory_io_result_bits_tag; // @[Scheduler.scala 345:25]
  assign abc_mshrs_0_io_directory_bits_hit = directory_io_result_bits_hit; // @[Scheduler.scala 345:25]
  assign abc_mshrs_0_io_directory_bits_way = directory_io_result_bits_way; // @[Scheduler.scala 345:25]
  assign abc_mshrs_0_io_schedule_ready = mshr_selectOH[0]; // @[Scheduler.scala 253:28]
  assign abc_mshrs_0_io_sinkc_valid = sinkC_io_resp_valid & sinkC_io_resp_bits_set == abc_mshrs_0_io_status_bits_set; // @[Scheduler.scala 115:45]
  assign abc_mshrs_0_io_sinkc_bits_last = sinkC_io_resp_bits_last; // @[Scheduler.scala 118:21]
  assign abc_mshrs_0_io_sinkc_bits_tag = sinkC_io_resp_bits_tag; // @[Scheduler.scala 118:21]
  assign abc_mshrs_0_io_sinkc_bits_source = sinkC_io_resp_bits_source; // @[Scheduler.scala 118:21]
  assign abc_mshrs_0_io_sinkc_bits_param = sinkC_io_resp_bits_param; // @[Scheduler.scala 118:21]
  assign abc_mshrs_0_io_sinkc_bits_data = sinkC_io_resp_bits_data; // @[Scheduler.scala 118:21]
  assign abc_mshrs_0_io_sinkd_valid = sinkD_io_resp_valid & sinkD_io_resp_bits_source == 3'h0; // @[Scheduler.scala 116:45]
  assign abc_mshrs_0_io_sinkd_bits_last = sinkD_io_resp_bits_last; // @[Scheduler.scala 119:21]
  assign abc_mshrs_0_io_sinkd_bits_opcode = sinkD_io_resp_bits_opcode; // @[Scheduler.scala 119:21]
  assign abc_mshrs_0_io_sinkd_bits_param = sinkD_io_resp_bits_param; // @[Scheduler.scala 119:21]
  assign abc_mshrs_0_io_sinkd_bits_sink = sinkD_io_resp_bits_sink; // @[Scheduler.scala 119:21]
  assign abc_mshrs_0_io_sinkd_bits_denied = sinkD_io_resp_bits_denied; // @[Scheduler.scala 119:21]
  assign abc_mshrs_0_io_sinke_valid = sinkE_io_resp_valid & sinkE_io_resp_bits_sink == 3'h0; // @[Scheduler.scala 117:45]
  assign abc_mshrs_0_io_nestedwb_set = mshr_selectOH[7] ? c_mshr_io_status_bits_set : bc_mshr_io_status_bits_set; // @[Scheduler.scala 183:24]
  assign abc_mshrs_0_io_nestedwb_tag = mshr_selectOH[7] ? c_mshr_io_status_bits_tag : bc_mshr_io_status_bits_tag; // @[Scheduler.scala 184:24]
  assign abc_mshrs_0_io_nestedwb_b_toN = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h0; // @[Scheduler.scala 185:75]
  assign abc_mshrs_0_io_nestedwb_b_toB = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h1; // @[Scheduler.scala 186:75]
  assign abc_mshrs_0_io_nestedwb_b_clr_dirty = mshr_selectOH[6] & bc_mshr_io_schedule_bits_dir_valid; // @[Scheduler.scala 187:37]
  assign abc_mshrs_0_io_nestedwb_c_set_dirty = _schedule_T_120 & c_mshr_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 188:75]
  assign abc_mshrs_1_clock = clock;
  assign abc_mshrs_1_reset = reset;
  assign abc_mshrs_1_io_allocate_valid = request_valid & alloc & mshr_insertOH[1] & _request_alloc_cases_T |
    mshr_selectOH[1] & will_reload_2; // @[Scheduler.scala 315:83 Scheduler.scala 316:27 Scheduler.scala 266:25]
  assign abc_mshrs_1_io_allocate_bits_prio_0 = request_valid & alloc & mshr_insertOH[1] & _request_alloc_cases_T ?
    request_bits_prio_0 : _mshrs_1_io_allocate_bits_T_prio_0; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_1_io_allocate_bits_prio_2 = request_valid & alloc & mshr_insertOH[1] & _request_alloc_cases_T ?
    request_bits_prio_2 : _mshrs_1_io_allocate_bits_T_prio_2; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_1_io_allocate_bits_control = request_valid & alloc & mshr_insertOH[1] & _request_alloc_cases_T ?
    request_bits_control : _mshrs_1_io_allocate_bits_T_control; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_1_io_allocate_bits_opcode = request_valid & alloc & mshr_insertOH[1] & _request_alloc_cases_T ?
    request_bits_opcode : _mshrs_1_io_allocate_bits_T_opcode; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_1_io_allocate_bits_param = request_valid & alloc & mshr_insertOH[1] & _request_alloc_cases_T ?
    request_bits_param : _mshrs_1_io_allocate_bits_T_param; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_1_io_allocate_bits_size = request_valid & alloc & mshr_insertOH[1] & _request_alloc_cases_T ?
    request_bits_size : _mshrs_1_io_allocate_bits_T_size; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_1_io_allocate_bits_source = request_valid & alloc & mshr_insertOH[1] & _request_alloc_cases_T ?
    request_bits_source : _mshrs_1_io_allocate_bits_T_source; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_1_io_allocate_bits_tag = request_valid & alloc & mshr_insertOH[1] & _request_alloc_cases_T ?
    request_bits_tag : _mshrs_1_io_allocate_bits_T_tag; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_1_io_allocate_bits_offset = request_valid & alloc & mshr_insertOH[1] & _request_alloc_cases_T ?
    request_bits_offset : _mshrs_1_io_allocate_bits_T_offset; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_1_io_allocate_bits_put = request_valid & alloc & mshr_insertOH[1] & _request_alloc_cases_T ?
    request_bits_put : _mshrs_1_io_allocate_bits_T_put; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_1_io_allocate_bits_set = request_valid & alloc & mshr_insertOH[1] & _request_alloc_cases_T ?
    request_bits_set : abc_mshrs_1_io_status_bits_set; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 264:28]
  assign abc_mshrs_1_io_allocate_bits_repeat = request_valid & alloc & mshr_insertOH[1] & _request_alloc_cases_T ? 1'h0
     : abc_mshrs_1_io_allocate_bits_tag == abc_mshrs_1_io_status_bits_tag; // @[Scheduler.scala 315:83 Scheduler.scala 318:33 Scheduler.scala 265:31]
  assign abc_mshrs_1_io_directory_valid = directoryFanout[1]; // @[Scheduler.scala 344:44]
  assign abc_mshrs_1_io_directory_bits_dirty = directory_io_result_bits_dirty; // @[Scheduler.scala 345:25]
  assign abc_mshrs_1_io_directory_bits_state = directory_io_result_bits_state; // @[Scheduler.scala 345:25]
  assign abc_mshrs_1_io_directory_bits_clients = directory_io_result_bits_clients; // @[Scheduler.scala 345:25]
  assign abc_mshrs_1_io_directory_bits_tag = directory_io_result_bits_tag; // @[Scheduler.scala 345:25]
  assign abc_mshrs_1_io_directory_bits_hit = directory_io_result_bits_hit; // @[Scheduler.scala 345:25]
  assign abc_mshrs_1_io_directory_bits_way = directory_io_result_bits_way; // @[Scheduler.scala 345:25]
  assign abc_mshrs_1_io_schedule_ready = mshr_selectOH[1]; // @[Scheduler.scala 253:28]
  assign abc_mshrs_1_io_sinkc_valid = sinkC_io_resp_valid & sinkC_io_resp_bits_set == abc_mshrs_1_io_status_bits_set; // @[Scheduler.scala 115:45]
  assign abc_mshrs_1_io_sinkc_bits_last = sinkC_io_resp_bits_last; // @[Scheduler.scala 118:21]
  assign abc_mshrs_1_io_sinkc_bits_tag = sinkC_io_resp_bits_tag; // @[Scheduler.scala 118:21]
  assign abc_mshrs_1_io_sinkc_bits_source = sinkC_io_resp_bits_source; // @[Scheduler.scala 118:21]
  assign abc_mshrs_1_io_sinkc_bits_param = sinkC_io_resp_bits_param; // @[Scheduler.scala 118:21]
  assign abc_mshrs_1_io_sinkc_bits_data = sinkC_io_resp_bits_data; // @[Scheduler.scala 118:21]
  assign abc_mshrs_1_io_sinkd_valid = sinkD_io_resp_valid & sinkD_io_resp_bits_source == 3'h1; // @[Scheduler.scala 116:45]
  assign abc_mshrs_1_io_sinkd_bits_last = sinkD_io_resp_bits_last; // @[Scheduler.scala 119:21]
  assign abc_mshrs_1_io_sinkd_bits_opcode = sinkD_io_resp_bits_opcode; // @[Scheduler.scala 119:21]
  assign abc_mshrs_1_io_sinkd_bits_param = sinkD_io_resp_bits_param; // @[Scheduler.scala 119:21]
  assign abc_mshrs_1_io_sinkd_bits_sink = sinkD_io_resp_bits_sink; // @[Scheduler.scala 119:21]
  assign abc_mshrs_1_io_sinkd_bits_denied = sinkD_io_resp_bits_denied; // @[Scheduler.scala 119:21]
  assign abc_mshrs_1_io_sinke_valid = sinkE_io_resp_valid & sinkE_io_resp_bits_sink == 3'h1; // @[Scheduler.scala 117:45]
  assign abc_mshrs_1_io_nestedwb_set = mshr_selectOH[7] ? c_mshr_io_status_bits_set : bc_mshr_io_status_bits_set; // @[Scheduler.scala 183:24]
  assign abc_mshrs_1_io_nestedwb_tag = mshr_selectOH[7] ? c_mshr_io_status_bits_tag : bc_mshr_io_status_bits_tag; // @[Scheduler.scala 184:24]
  assign abc_mshrs_1_io_nestedwb_b_toN = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h0; // @[Scheduler.scala 185:75]
  assign abc_mshrs_1_io_nestedwb_b_toB = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h1; // @[Scheduler.scala 186:75]
  assign abc_mshrs_1_io_nestedwb_b_clr_dirty = mshr_selectOH[6] & bc_mshr_io_schedule_bits_dir_valid; // @[Scheduler.scala 187:37]
  assign abc_mshrs_1_io_nestedwb_c_set_dirty = _schedule_T_120 & c_mshr_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 188:75]
  assign abc_mshrs_2_clock = clock;
  assign abc_mshrs_2_reset = reset;
  assign abc_mshrs_2_io_allocate_valid = request_valid & alloc & mshr_insertOH[2] & _request_alloc_cases_T |
    mshr_selectOH[2] & will_reload_3; // @[Scheduler.scala 315:83 Scheduler.scala 316:27 Scheduler.scala 266:25]
  assign abc_mshrs_2_io_allocate_bits_prio_0 = request_valid & alloc & mshr_insertOH[2] & _request_alloc_cases_T ?
    request_bits_prio_0 : _mshrs_2_io_allocate_bits_T_prio_0; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_2_io_allocate_bits_prio_2 = request_valid & alloc & mshr_insertOH[2] & _request_alloc_cases_T ?
    request_bits_prio_2 : _mshrs_2_io_allocate_bits_T_prio_2; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_2_io_allocate_bits_control = request_valid & alloc & mshr_insertOH[2] & _request_alloc_cases_T ?
    request_bits_control : _mshrs_2_io_allocate_bits_T_control; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_2_io_allocate_bits_opcode = request_valid & alloc & mshr_insertOH[2] & _request_alloc_cases_T ?
    request_bits_opcode : _mshrs_2_io_allocate_bits_T_opcode; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_2_io_allocate_bits_param = request_valid & alloc & mshr_insertOH[2] & _request_alloc_cases_T ?
    request_bits_param : _mshrs_2_io_allocate_bits_T_param; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_2_io_allocate_bits_size = request_valid & alloc & mshr_insertOH[2] & _request_alloc_cases_T ?
    request_bits_size : _mshrs_2_io_allocate_bits_T_size; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_2_io_allocate_bits_source = request_valid & alloc & mshr_insertOH[2] & _request_alloc_cases_T ?
    request_bits_source : _mshrs_2_io_allocate_bits_T_source; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_2_io_allocate_bits_tag = request_valid & alloc & mshr_insertOH[2] & _request_alloc_cases_T ?
    request_bits_tag : _mshrs_2_io_allocate_bits_T_tag; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_2_io_allocate_bits_offset = request_valid & alloc & mshr_insertOH[2] & _request_alloc_cases_T ?
    request_bits_offset : _mshrs_2_io_allocate_bits_T_offset; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_2_io_allocate_bits_put = request_valid & alloc & mshr_insertOH[2] & _request_alloc_cases_T ?
    request_bits_put : _mshrs_2_io_allocate_bits_T_put; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_2_io_allocate_bits_set = request_valid & alloc & mshr_insertOH[2] & _request_alloc_cases_T ?
    request_bits_set : abc_mshrs_2_io_status_bits_set; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 264:28]
  assign abc_mshrs_2_io_allocate_bits_repeat = request_valid & alloc & mshr_insertOH[2] & _request_alloc_cases_T ? 1'h0
     : abc_mshrs_2_io_allocate_bits_tag == abc_mshrs_2_io_status_bits_tag; // @[Scheduler.scala 315:83 Scheduler.scala 318:33 Scheduler.scala 265:31]
  assign abc_mshrs_2_io_directory_valid = directoryFanout[2]; // @[Scheduler.scala 344:44]
  assign abc_mshrs_2_io_directory_bits_dirty = directory_io_result_bits_dirty; // @[Scheduler.scala 345:25]
  assign abc_mshrs_2_io_directory_bits_state = directory_io_result_bits_state; // @[Scheduler.scala 345:25]
  assign abc_mshrs_2_io_directory_bits_clients = directory_io_result_bits_clients; // @[Scheduler.scala 345:25]
  assign abc_mshrs_2_io_directory_bits_tag = directory_io_result_bits_tag; // @[Scheduler.scala 345:25]
  assign abc_mshrs_2_io_directory_bits_hit = directory_io_result_bits_hit; // @[Scheduler.scala 345:25]
  assign abc_mshrs_2_io_directory_bits_way = directory_io_result_bits_way; // @[Scheduler.scala 345:25]
  assign abc_mshrs_2_io_schedule_ready = mshr_selectOH[2]; // @[Scheduler.scala 253:28]
  assign abc_mshrs_2_io_sinkc_valid = sinkC_io_resp_valid & sinkC_io_resp_bits_set == abc_mshrs_2_io_status_bits_set; // @[Scheduler.scala 115:45]
  assign abc_mshrs_2_io_sinkc_bits_last = sinkC_io_resp_bits_last; // @[Scheduler.scala 118:21]
  assign abc_mshrs_2_io_sinkc_bits_tag = sinkC_io_resp_bits_tag; // @[Scheduler.scala 118:21]
  assign abc_mshrs_2_io_sinkc_bits_source = sinkC_io_resp_bits_source; // @[Scheduler.scala 118:21]
  assign abc_mshrs_2_io_sinkc_bits_param = sinkC_io_resp_bits_param; // @[Scheduler.scala 118:21]
  assign abc_mshrs_2_io_sinkc_bits_data = sinkC_io_resp_bits_data; // @[Scheduler.scala 118:21]
  assign abc_mshrs_2_io_sinkd_valid = sinkD_io_resp_valid & sinkD_io_resp_bits_source == 3'h2; // @[Scheduler.scala 116:45]
  assign abc_mshrs_2_io_sinkd_bits_last = sinkD_io_resp_bits_last; // @[Scheduler.scala 119:21]
  assign abc_mshrs_2_io_sinkd_bits_opcode = sinkD_io_resp_bits_opcode; // @[Scheduler.scala 119:21]
  assign abc_mshrs_2_io_sinkd_bits_param = sinkD_io_resp_bits_param; // @[Scheduler.scala 119:21]
  assign abc_mshrs_2_io_sinkd_bits_sink = sinkD_io_resp_bits_sink; // @[Scheduler.scala 119:21]
  assign abc_mshrs_2_io_sinkd_bits_denied = sinkD_io_resp_bits_denied; // @[Scheduler.scala 119:21]
  assign abc_mshrs_2_io_sinke_valid = sinkE_io_resp_valid & sinkE_io_resp_bits_sink == 3'h2; // @[Scheduler.scala 117:45]
  assign abc_mshrs_2_io_nestedwb_set = mshr_selectOH[7] ? c_mshr_io_status_bits_set : bc_mshr_io_status_bits_set; // @[Scheduler.scala 183:24]
  assign abc_mshrs_2_io_nestedwb_tag = mshr_selectOH[7] ? c_mshr_io_status_bits_tag : bc_mshr_io_status_bits_tag; // @[Scheduler.scala 184:24]
  assign abc_mshrs_2_io_nestedwb_b_toN = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h0; // @[Scheduler.scala 185:75]
  assign abc_mshrs_2_io_nestedwb_b_toB = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h1; // @[Scheduler.scala 186:75]
  assign abc_mshrs_2_io_nestedwb_b_clr_dirty = mshr_selectOH[6] & bc_mshr_io_schedule_bits_dir_valid; // @[Scheduler.scala 187:37]
  assign abc_mshrs_2_io_nestedwb_c_set_dirty = _schedule_T_120 & c_mshr_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 188:75]
  assign abc_mshrs_3_clock = clock;
  assign abc_mshrs_3_reset = reset;
  assign abc_mshrs_3_io_allocate_valid = request_valid & alloc & mshr_insertOH[3] & _request_alloc_cases_T |
    mshr_selectOH[3] & will_reload_4; // @[Scheduler.scala 315:83 Scheduler.scala 316:27 Scheduler.scala 266:25]
  assign abc_mshrs_3_io_allocate_bits_prio_0 = request_valid & alloc & mshr_insertOH[3] & _request_alloc_cases_T ?
    request_bits_prio_0 : _mshrs_3_io_allocate_bits_T_prio_0; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_3_io_allocate_bits_prio_2 = request_valid & alloc & mshr_insertOH[3] & _request_alloc_cases_T ?
    request_bits_prio_2 : _mshrs_3_io_allocate_bits_T_prio_2; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_3_io_allocate_bits_control = request_valid & alloc & mshr_insertOH[3] & _request_alloc_cases_T ?
    request_bits_control : _mshrs_3_io_allocate_bits_T_control; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_3_io_allocate_bits_opcode = request_valid & alloc & mshr_insertOH[3] & _request_alloc_cases_T ?
    request_bits_opcode : _mshrs_3_io_allocate_bits_T_opcode; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_3_io_allocate_bits_param = request_valid & alloc & mshr_insertOH[3] & _request_alloc_cases_T ?
    request_bits_param : _mshrs_3_io_allocate_bits_T_param; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_3_io_allocate_bits_size = request_valid & alloc & mshr_insertOH[3] & _request_alloc_cases_T ?
    request_bits_size : _mshrs_3_io_allocate_bits_T_size; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_3_io_allocate_bits_source = request_valid & alloc & mshr_insertOH[3] & _request_alloc_cases_T ?
    request_bits_source : _mshrs_3_io_allocate_bits_T_source; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_3_io_allocate_bits_tag = request_valid & alloc & mshr_insertOH[3] & _request_alloc_cases_T ?
    request_bits_tag : _mshrs_3_io_allocate_bits_T_tag; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_3_io_allocate_bits_offset = request_valid & alloc & mshr_insertOH[3] & _request_alloc_cases_T ?
    request_bits_offset : _mshrs_3_io_allocate_bits_T_offset; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_3_io_allocate_bits_put = request_valid & alloc & mshr_insertOH[3] & _request_alloc_cases_T ?
    request_bits_put : _mshrs_3_io_allocate_bits_T_put; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_3_io_allocate_bits_set = request_valid & alloc & mshr_insertOH[3] & _request_alloc_cases_T ?
    request_bits_set : abc_mshrs_3_io_status_bits_set; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 264:28]
  assign abc_mshrs_3_io_allocate_bits_repeat = request_valid & alloc & mshr_insertOH[3] & _request_alloc_cases_T ? 1'h0
     : abc_mshrs_3_io_allocate_bits_tag == abc_mshrs_3_io_status_bits_tag; // @[Scheduler.scala 315:83 Scheduler.scala 318:33 Scheduler.scala 265:31]
  assign abc_mshrs_3_io_directory_valid = directoryFanout[3]; // @[Scheduler.scala 344:44]
  assign abc_mshrs_3_io_directory_bits_dirty = directory_io_result_bits_dirty; // @[Scheduler.scala 345:25]
  assign abc_mshrs_3_io_directory_bits_state = directory_io_result_bits_state; // @[Scheduler.scala 345:25]
  assign abc_mshrs_3_io_directory_bits_clients = directory_io_result_bits_clients; // @[Scheduler.scala 345:25]
  assign abc_mshrs_3_io_directory_bits_tag = directory_io_result_bits_tag; // @[Scheduler.scala 345:25]
  assign abc_mshrs_3_io_directory_bits_hit = directory_io_result_bits_hit; // @[Scheduler.scala 345:25]
  assign abc_mshrs_3_io_directory_bits_way = directory_io_result_bits_way; // @[Scheduler.scala 345:25]
  assign abc_mshrs_3_io_schedule_ready = mshr_selectOH[3]; // @[Scheduler.scala 253:28]
  assign abc_mshrs_3_io_sinkc_valid = sinkC_io_resp_valid & sinkC_io_resp_bits_set == abc_mshrs_3_io_status_bits_set; // @[Scheduler.scala 115:45]
  assign abc_mshrs_3_io_sinkc_bits_last = sinkC_io_resp_bits_last; // @[Scheduler.scala 118:21]
  assign abc_mshrs_3_io_sinkc_bits_tag = sinkC_io_resp_bits_tag; // @[Scheduler.scala 118:21]
  assign abc_mshrs_3_io_sinkc_bits_source = sinkC_io_resp_bits_source; // @[Scheduler.scala 118:21]
  assign abc_mshrs_3_io_sinkc_bits_param = sinkC_io_resp_bits_param; // @[Scheduler.scala 118:21]
  assign abc_mshrs_3_io_sinkc_bits_data = sinkC_io_resp_bits_data; // @[Scheduler.scala 118:21]
  assign abc_mshrs_3_io_sinkd_valid = sinkD_io_resp_valid & sinkD_io_resp_bits_source == 3'h3; // @[Scheduler.scala 116:45]
  assign abc_mshrs_3_io_sinkd_bits_last = sinkD_io_resp_bits_last; // @[Scheduler.scala 119:21]
  assign abc_mshrs_3_io_sinkd_bits_opcode = sinkD_io_resp_bits_opcode; // @[Scheduler.scala 119:21]
  assign abc_mshrs_3_io_sinkd_bits_param = sinkD_io_resp_bits_param; // @[Scheduler.scala 119:21]
  assign abc_mshrs_3_io_sinkd_bits_sink = sinkD_io_resp_bits_sink; // @[Scheduler.scala 119:21]
  assign abc_mshrs_3_io_sinkd_bits_denied = sinkD_io_resp_bits_denied; // @[Scheduler.scala 119:21]
  assign abc_mshrs_3_io_sinke_valid = sinkE_io_resp_valid & sinkE_io_resp_bits_sink == 3'h3; // @[Scheduler.scala 117:45]
  assign abc_mshrs_3_io_nestedwb_set = mshr_selectOH[7] ? c_mshr_io_status_bits_set : bc_mshr_io_status_bits_set; // @[Scheduler.scala 183:24]
  assign abc_mshrs_3_io_nestedwb_tag = mshr_selectOH[7] ? c_mshr_io_status_bits_tag : bc_mshr_io_status_bits_tag; // @[Scheduler.scala 184:24]
  assign abc_mshrs_3_io_nestedwb_b_toN = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h0; // @[Scheduler.scala 185:75]
  assign abc_mshrs_3_io_nestedwb_b_toB = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h1; // @[Scheduler.scala 186:75]
  assign abc_mshrs_3_io_nestedwb_b_clr_dirty = mshr_selectOH[6] & bc_mshr_io_schedule_bits_dir_valid; // @[Scheduler.scala 187:37]
  assign abc_mshrs_3_io_nestedwb_c_set_dirty = _schedule_T_120 & c_mshr_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 188:75]
  assign abc_mshrs_4_clock = clock;
  assign abc_mshrs_4_reset = reset;
  assign abc_mshrs_4_io_allocate_valid = request_valid & alloc & mshr_insertOH[4] & _request_alloc_cases_T |
    mshr_selectOH[4] & will_reload_5; // @[Scheduler.scala 315:83 Scheduler.scala 316:27 Scheduler.scala 266:25]
  assign abc_mshrs_4_io_allocate_bits_prio_0 = request_valid & alloc & mshr_insertOH[4] & _request_alloc_cases_T ?
    request_bits_prio_0 : _mshrs_4_io_allocate_bits_T_prio_0; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_4_io_allocate_bits_prio_2 = request_valid & alloc & mshr_insertOH[4] & _request_alloc_cases_T ?
    request_bits_prio_2 : _mshrs_4_io_allocate_bits_T_prio_2; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_4_io_allocate_bits_control = request_valid & alloc & mshr_insertOH[4] & _request_alloc_cases_T ?
    request_bits_control : _mshrs_4_io_allocate_bits_T_control; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_4_io_allocate_bits_opcode = request_valid & alloc & mshr_insertOH[4] & _request_alloc_cases_T ?
    request_bits_opcode : _mshrs_4_io_allocate_bits_T_opcode; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_4_io_allocate_bits_param = request_valid & alloc & mshr_insertOH[4] & _request_alloc_cases_T ?
    request_bits_param : _mshrs_4_io_allocate_bits_T_param; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_4_io_allocate_bits_size = request_valid & alloc & mshr_insertOH[4] & _request_alloc_cases_T ?
    request_bits_size : _mshrs_4_io_allocate_bits_T_size; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_4_io_allocate_bits_source = request_valid & alloc & mshr_insertOH[4] & _request_alloc_cases_T ?
    request_bits_source : _mshrs_4_io_allocate_bits_T_source; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_4_io_allocate_bits_tag = request_valid & alloc & mshr_insertOH[4] & _request_alloc_cases_T ?
    request_bits_tag : _mshrs_4_io_allocate_bits_T_tag; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_4_io_allocate_bits_offset = request_valid & alloc & mshr_insertOH[4] & _request_alloc_cases_T ?
    request_bits_offset : _mshrs_4_io_allocate_bits_T_offset; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_4_io_allocate_bits_put = request_valid & alloc & mshr_insertOH[4] & _request_alloc_cases_T ?
    request_bits_put : _mshrs_4_io_allocate_bits_T_put; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_4_io_allocate_bits_set = request_valid & alloc & mshr_insertOH[4] & _request_alloc_cases_T ?
    request_bits_set : abc_mshrs_4_io_status_bits_set; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 264:28]
  assign abc_mshrs_4_io_allocate_bits_repeat = request_valid & alloc & mshr_insertOH[4] & _request_alloc_cases_T ? 1'h0
     : abc_mshrs_4_io_allocate_bits_tag == abc_mshrs_4_io_status_bits_tag; // @[Scheduler.scala 315:83 Scheduler.scala 318:33 Scheduler.scala 265:31]
  assign abc_mshrs_4_io_directory_valid = directoryFanout[4]; // @[Scheduler.scala 344:44]
  assign abc_mshrs_4_io_directory_bits_dirty = directory_io_result_bits_dirty; // @[Scheduler.scala 345:25]
  assign abc_mshrs_4_io_directory_bits_state = directory_io_result_bits_state; // @[Scheduler.scala 345:25]
  assign abc_mshrs_4_io_directory_bits_clients = directory_io_result_bits_clients; // @[Scheduler.scala 345:25]
  assign abc_mshrs_4_io_directory_bits_tag = directory_io_result_bits_tag; // @[Scheduler.scala 345:25]
  assign abc_mshrs_4_io_directory_bits_hit = directory_io_result_bits_hit; // @[Scheduler.scala 345:25]
  assign abc_mshrs_4_io_directory_bits_way = directory_io_result_bits_way; // @[Scheduler.scala 345:25]
  assign abc_mshrs_4_io_schedule_ready = mshr_selectOH[4]; // @[Scheduler.scala 253:28]
  assign abc_mshrs_4_io_sinkc_valid = sinkC_io_resp_valid & sinkC_io_resp_bits_set == abc_mshrs_4_io_status_bits_set; // @[Scheduler.scala 115:45]
  assign abc_mshrs_4_io_sinkc_bits_last = sinkC_io_resp_bits_last; // @[Scheduler.scala 118:21]
  assign abc_mshrs_4_io_sinkc_bits_tag = sinkC_io_resp_bits_tag; // @[Scheduler.scala 118:21]
  assign abc_mshrs_4_io_sinkc_bits_source = sinkC_io_resp_bits_source; // @[Scheduler.scala 118:21]
  assign abc_mshrs_4_io_sinkc_bits_param = sinkC_io_resp_bits_param; // @[Scheduler.scala 118:21]
  assign abc_mshrs_4_io_sinkc_bits_data = sinkC_io_resp_bits_data; // @[Scheduler.scala 118:21]
  assign abc_mshrs_4_io_sinkd_valid = sinkD_io_resp_valid & sinkD_io_resp_bits_source == 3'h4; // @[Scheduler.scala 116:45]
  assign abc_mshrs_4_io_sinkd_bits_last = sinkD_io_resp_bits_last; // @[Scheduler.scala 119:21]
  assign abc_mshrs_4_io_sinkd_bits_opcode = sinkD_io_resp_bits_opcode; // @[Scheduler.scala 119:21]
  assign abc_mshrs_4_io_sinkd_bits_param = sinkD_io_resp_bits_param; // @[Scheduler.scala 119:21]
  assign abc_mshrs_4_io_sinkd_bits_sink = sinkD_io_resp_bits_sink; // @[Scheduler.scala 119:21]
  assign abc_mshrs_4_io_sinkd_bits_denied = sinkD_io_resp_bits_denied; // @[Scheduler.scala 119:21]
  assign abc_mshrs_4_io_sinke_valid = sinkE_io_resp_valid & sinkE_io_resp_bits_sink == 3'h4; // @[Scheduler.scala 117:45]
  assign abc_mshrs_4_io_nestedwb_set = mshr_selectOH[7] ? c_mshr_io_status_bits_set : bc_mshr_io_status_bits_set; // @[Scheduler.scala 183:24]
  assign abc_mshrs_4_io_nestedwb_tag = mshr_selectOH[7] ? c_mshr_io_status_bits_tag : bc_mshr_io_status_bits_tag; // @[Scheduler.scala 184:24]
  assign abc_mshrs_4_io_nestedwb_b_toN = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h0; // @[Scheduler.scala 185:75]
  assign abc_mshrs_4_io_nestedwb_b_toB = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h1; // @[Scheduler.scala 186:75]
  assign abc_mshrs_4_io_nestedwb_b_clr_dirty = mshr_selectOH[6] & bc_mshr_io_schedule_bits_dir_valid; // @[Scheduler.scala 187:37]
  assign abc_mshrs_4_io_nestedwb_c_set_dirty = _schedule_T_120 & c_mshr_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 188:75]
  assign abc_mshrs_5_clock = clock;
  assign abc_mshrs_5_reset = reset;
  assign abc_mshrs_5_io_allocate_valid = request_valid & alloc & mshr_insertOH[5] & _request_alloc_cases_T |
    mshr_selectOH[5] & will_reload_6; // @[Scheduler.scala 315:83 Scheduler.scala 316:27 Scheduler.scala 266:25]
  assign abc_mshrs_5_io_allocate_bits_prio_0 = request_valid & alloc & mshr_insertOH[5] & _request_alloc_cases_T ?
    request_bits_prio_0 : _mshrs_5_io_allocate_bits_T_prio_0; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_5_io_allocate_bits_prio_2 = request_valid & alloc & mshr_insertOH[5] & _request_alloc_cases_T ?
    request_bits_prio_2 : _mshrs_5_io_allocate_bits_T_prio_2; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_5_io_allocate_bits_control = request_valid & alloc & mshr_insertOH[5] & _request_alloc_cases_T ?
    request_bits_control : _mshrs_5_io_allocate_bits_T_control; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_5_io_allocate_bits_opcode = request_valid & alloc & mshr_insertOH[5] & _request_alloc_cases_T ?
    request_bits_opcode : _mshrs_5_io_allocate_bits_T_opcode; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_5_io_allocate_bits_param = request_valid & alloc & mshr_insertOH[5] & _request_alloc_cases_T ?
    request_bits_param : _mshrs_5_io_allocate_bits_T_param; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_5_io_allocate_bits_size = request_valid & alloc & mshr_insertOH[5] & _request_alloc_cases_T ?
    request_bits_size : _mshrs_5_io_allocate_bits_T_size; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_5_io_allocate_bits_source = request_valid & alloc & mshr_insertOH[5] & _request_alloc_cases_T ?
    request_bits_source : _mshrs_5_io_allocate_bits_T_source; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_5_io_allocate_bits_tag = request_valid & alloc & mshr_insertOH[5] & _request_alloc_cases_T ?
    request_bits_tag : _mshrs_5_io_allocate_bits_T_tag; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_5_io_allocate_bits_offset = request_valid & alloc & mshr_insertOH[5] & _request_alloc_cases_T ?
    request_bits_offset : _mshrs_5_io_allocate_bits_T_offset; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_5_io_allocate_bits_put = request_valid & alloc & mshr_insertOH[5] & _request_alloc_cases_T ?
    request_bits_put : _mshrs_5_io_allocate_bits_T_put; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign abc_mshrs_5_io_allocate_bits_set = request_valid & alloc & mshr_insertOH[5] & _request_alloc_cases_T ?
    request_bits_set : abc_mshrs_5_io_status_bits_set; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 264:28]
  assign abc_mshrs_5_io_allocate_bits_repeat = request_valid & alloc & mshr_insertOH[5] & _request_alloc_cases_T ? 1'h0
     : abc_mshrs_5_io_allocate_bits_tag == abc_mshrs_5_io_status_bits_tag; // @[Scheduler.scala 315:83 Scheduler.scala 318:33 Scheduler.scala 265:31]
  assign abc_mshrs_5_io_directory_valid = directoryFanout[5]; // @[Scheduler.scala 344:44]
  assign abc_mshrs_5_io_directory_bits_dirty = directory_io_result_bits_dirty; // @[Scheduler.scala 345:25]
  assign abc_mshrs_5_io_directory_bits_state = directory_io_result_bits_state; // @[Scheduler.scala 345:25]
  assign abc_mshrs_5_io_directory_bits_clients = directory_io_result_bits_clients; // @[Scheduler.scala 345:25]
  assign abc_mshrs_5_io_directory_bits_tag = directory_io_result_bits_tag; // @[Scheduler.scala 345:25]
  assign abc_mshrs_5_io_directory_bits_hit = directory_io_result_bits_hit; // @[Scheduler.scala 345:25]
  assign abc_mshrs_5_io_directory_bits_way = directory_io_result_bits_way; // @[Scheduler.scala 345:25]
  assign abc_mshrs_5_io_schedule_ready = mshr_selectOH[5]; // @[Scheduler.scala 253:28]
  assign abc_mshrs_5_io_sinkc_valid = sinkC_io_resp_valid & sinkC_io_resp_bits_set == abc_mshrs_5_io_status_bits_set; // @[Scheduler.scala 115:45]
  assign abc_mshrs_5_io_sinkc_bits_last = sinkC_io_resp_bits_last; // @[Scheduler.scala 118:21]
  assign abc_mshrs_5_io_sinkc_bits_tag = sinkC_io_resp_bits_tag; // @[Scheduler.scala 118:21]
  assign abc_mshrs_5_io_sinkc_bits_source = sinkC_io_resp_bits_source; // @[Scheduler.scala 118:21]
  assign abc_mshrs_5_io_sinkc_bits_param = sinkC_io_resp_bits_param; // @[Scheduler.scala 118:21]
  assign abc_mshrs_5_io_sinkc_bits_data = sinkC_io_resp_bits_data; // @[Scheduler.scala 118:21]
  assign abc_mshrs_5_io_sinkd_valid = sinkD_io_resp_valid & sinkD_io_resp_bits_source == 3'h5; // @[Scheduler.scala 116:45]
  assign abc_mshrs_5_io_sinkd_bits_last = sinkD_io_resp_bits_last; // @[Scheduler.scala 119:21]
  assign abc_mshrs_5_io_sinkd_bits_opcode = sinkD_io_resp_bits_opcode; // @[Scheduler.scala 119:21]
  assign abc_mshrs_5_io_sinkd_bits_param = sinkD_io_resp_bits_param; // @[Scheduler.scala 119:21]
  assign abc_mshrs_5_io_sinkd_bits_sink = sinkD_io_resp_bits_sink; // @[Scheduler.scala 119:21]
  assign abc_mshrs_5_io_sinkd_bits_denied = sinkD_io_resp_bits_denied; // @[Scheduler.scala 119:21]
  assign abc_mshrs_5_io_sinke_valid = sinkE_io_resp_valid & sinkE_io_resp_bits_sink == 3'h5; // @[Scheduler.scala 117:45]
  assign abc_mshrs_5_io_nestedwb_set = mshr_selectOH[7] ? c_mshr_io_status_bits_set : bc_mshr_io_status_bits_set; // @[Scheduler.scala 183:24]
  assign abc_mshrs_5_io_nestedwb_tag = mshr_selectOH[7] ? c_mshr_io_status_bits_tag : bc_mshr_io_status_bits_tag; // @[Scheduler.scala 184:24]
  assign abc_mshrs_5_io_nestedwb_b_toN = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h0; // @[Scheduler.scala 185:75]
  assign abc_mshrs_5_io_nestedwb_b_toB = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h1; // @[Scheduler.scala 186:75]
  assign abc_mshrs_5_io_nestedwb_b_clr_dirty = mshr_selectOH[6] & bc_mshr_io_schedule_bits_dir_valid; // @[Scheduler.scala 187:37]
  assign abc_mshrs_5_io_nestedwb_c_set_dirty = _schedule_T_120 & c_mshr_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 188:75]
  assign bc_mshr_clock = clock;
  assign bc_mshr_reset = reset;
  assign bc_mshr_io_allocate_valid = request_valid & alloc & mshr_insertOH[6] & _request_alloc_cases_T | mshr_selectOH[6
    ] & will_reload_7; // @[Scheduler.scala 315:83 Scheduler.scala 316:27 Scheduler.scala 266:25]
  assign bc_mshr_io_allocate_bits_prio_2 = request_valid & alloc & mshr_insertOH[6] & _request_alloc_cases_T ?
    request_bits_prio_2 : _mshrs_6_io_allocate_bits_T_prio_2; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign bc_mshr_io_allocate_bits_control = request_valid & alloc & mshr_insertOH[6] & _request_alloc_cases_T ?
    request_bits_control : _mshrs_6_io_allocate_bits_T_control; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign bc_mshr_io_allocate_bits_opcode = request_valid & alloc & mshr_insertOH[6] & _request_alloc_cases_T ?
    request_bits_opcode : _mshrs_6_io_allocate_bits_T_opcode; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign bc_mshr_io_allocate_bits_param = request_valid & alloc & mshr_insertOH[6] & _request_alloc_cases_T ?
    request_bits_param : _mshrs_6_io_allocate_bits_T_param; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign bc_mshr_io_allocate_bits_size = request_valid & alloc & mshr_insertOH[6] & _request_alloc_cases_T ?
    request_bits_size : _mshrs_6_io_allocate_bits_T_size; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign bc_mshr_io_allocate_bits_source = request_valid & alloc & mshr_insertOH[6] & _request_alloc_cases_T ?
    request_bits_source : _mshrs_6_io_allocate_bits_T_source; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign bc_mshr_io_allocate_bits_tag = request_valid & alloc & mshr_insertOH[6] & _request_alloc_cases_T ?
    request_bits_tag : _mshrs_6_io_allocate_bits_T_tag; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign bc_mshr_io_allocate_bits_offset = request_valid & alloc & mshr_insertOH[6] & _request_alloc_cases_T ?
    request_bits_offset : _mshrs_6_io_allocate_bits_T_offset; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign bc_mshr_io_allocate_bits_put = request_valid & alloc & mshr_insertOH[6] & _request_alloc_cases_T ?
    request_bits_put : _mshrs_6_io_allocate_bits_T_put; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 263:24]
  assign bc_mshr_io_allocate_bits_set = request_valid & alloc & mshr_insertOH[6] & _request_alloc_cases_T ?
    request_bits_set : bc_mshr_io_status_bits_set; // @[Scheduler.scala 315:83 Scheduler.scala 317:26 Scheduler.scala 264:28]
  assign bc_mshr_io_allocate_bits_repeat = request_valid & alloc & mshr_insertOH[6] & _request_alloc_cases_T ? 1'h0 :
    bc_mshr_io_allocate_bits_tag == bc_mshr_io_status_bits_tag; // @[Scheduler.scala 315:83 Scheduler.scala 318:33 Scheduler.scala 265:31]
  assign bc_mshr_io_directory_valid = directoryFanout[6]; // @[Scheduler.scala 344:44]
  assign bc_mshr_io_directory_bits_dirty = directory_io_result_bits_dirty; // @[Scheduler.scala 345:25]
  assign bc_mshr_io_directory_bits_state = directory_io_result_bits_state; // @[Scheduler.scala 345:25]
  assign bc_mshr_io_directory_bits_clients = directory_io_result_bits_clients; // @[Scheduler.scala 345:25]
  assign bc_mshr_io_directory_bits_tag = directory_io_result_bits_tag; // @[Scheduler.scala 345:25]
  assign bc_mshr_io_directory_bits_hit = directory_io_result_bits_hit; // @[Scheduler.scala 345:25]
  assign bc_mshr_io_directory_bits_way = directory_io_result_bits_way; // @[Scheduler.scala 345:25]
  assign bc_mshr_io_schedule_ready = mshr_selectOH[6]; // @[Scheduler.scala 253:28]
  assign bc_mshr_io_sinkc_valid = sinkC_io_resp_valid & sinkC_io_resp_bits_set == bc_mshr_io_status_bits_set; // @[Scheduler.scala 115:45]
  assign bc_mshr_io_sinkc_bits_last = sinkC_io_resp_bits_last; // @[Scheduler.scala 118:21]
  assign bc_mshr_io_sinkc_bits_tag = sinkC_io_resp_bits_tag; // @[Scheduler.scala 118:21]
  assign bc_mshr_io_sinkc_bits_source = sinkC_io_resp_bits_source; // @[Scheduler.scala 118:21]
  assign bc_mshr_io_sinkc_bits_param = sinkC_io_resp_bits_param; // @[Scheduler.scala 118:21]
  assign bc_mshr_io_sinkc_bits_data = sinkC_io_resp_bits_data; // @[Scheduler.scala 118:21]
  assign bc_mshr_io_sinkd_valid = sinkD_io_resp_valid & sinkD_io_resp_bits_source == 3'h6; // @[Scheduler.scala 116:45]
  assign bc_mshr_io_sinkd_bits_last = sinkD_io_resp_bits_last; // @[Scheduler.scala 119:21]
  assign bc_mshr_io_sinkd_bits_opcode = sinkD_io_resp_bits_opcode; // @[Scheduler.scala 119:21]
  assign bc_mshr_io_sinkd_bits_param = sinkD_io_resp_bits_param; // @[Scheduler.scala 119:21]
  assign bc_mshr_io_sinkd_bits_sink = sinkD_io_resp_bits_sink; // @[Scheduler.scala 119:21]
  assign bc_mshr_io_sinkd_bits_denied = sinkD_io_resp_bits_denied; // @[Scheduler.scala 119:21]
  assign bc_mshr_io_sinke_valid = sinkE_io_resp_valid & sinkE_io_resp_bits_sink == 3'h6; // @[Scheduler.scala 117:45]
  assign bc_mshr_io_nestedwb_set = mshr_selectOH[7] ? c_mshr_io_status_bits_set : bc_mshr_io_status_bits_set; // @[Scheduler.scala 183:24]
  assign bc_mshr_io_nestedwb_tag = mshr_selectOH[7] ? c_mshr_io_status_bits_tag : bc_mshr_io_status_bits_tag; // @[Scheduler.scala 184:24]
  assign bc_mshr_io_nestedwb_b_toN = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h0; // @[Scheduler.scala 185:75]
  assign bc_mshr_io_nestedwb_b_toB = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h1; // @[Scheduler.scala 186:75]
  assign bc_mshr_io_nestedwb_b_clr_dirty = mshr_selectOH[6] & bc_mshr_io_schedule_bits_dir_valid; // @[Scheduler.scala 187:37]
  assign bc_mshr_io_nestedwb_c_set_dirty = _schedule_T_120 & c_mshr_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 188:75]
  assign c_mshr_clock = clock;
  assign c_mshr_reset = reset;
  assign c_mshr_io_allocate_valid = _T_87 | _GEN_99; // @[Scheduler.scala 330:103 Scheduler.scala 331:30]
  assign c_mshr_io_allocate_bits_prio_2 = _T_87 ? request_bits_prio_2 : _GEN_102; // @[Scheduler.scala 330:103 Scheduler.scala 332:29]
  assign c_mshr_io_allocate_bits_control = _T_87 ? request_bits_control : _GEN_103; // @[Scheduler.scala 330:103 Scheduler.scala 332:29]
  assign c_mshr_io_allocate_bits_opcode = _T_87 ? request_bits_opcode : _GEN_104; // @[Scheduler.scala 330:103 Scheduler.scala 332:29]
  assign c_mshr_io_allocate_bits_param = _T_87 ? request_bits_param : _GEN_105; // @[Scheduler.scala 330:103 Scheduler.scala 332:29]
  assign c_mshr_io_allocate_bits_size = _T_87 ? request_bits_size : _GEN_106; // @[Scheduler.scala 330:103 Scheduler.scala 332:29]
  assign c_mshr_io_allocate_bits_source = _T_87 ? request_bits_source : _GEN_107; // @[Scheduler.scala 330:103 Scheduler.scala 332:29]
  assign c_mshr_io_allocate_bits_tag = _T_87 ? request_bits_tag : _GEN_108; // @[Scheduler.scala 330:103 Scheduler.scala 332:29]
  assign c_mshr_io_allocate_bits_offset = _T_87 ? request_bits_offset : _GEN_109; // @[Scheduler.scala 330:103 Scheduler.scala 332:29]
  assign c_mshr_io_allocate_bits_put = _T_87 ? request_bits_put : _GEN_110; // @[Scheduler.scala 330:103 Scheduler.scala 332:29]
  assign c_mshr_io_allocate_bits_set = _T_87 ? request_bits_set : _GEN_111; // @[Scheduler.scala 330:103 Scheduler.scala 332:29]
  assign c_mshr_io_allocate_bits_repeat = _T_87 ? 1'h0 : _GEN_112; // @[Scheduler.scala 330:103 Scheduler.scala 333:36]
  assign c_mshr_io_directory_valid = directoryFanout[7]; // @[Scheduler.scala 344:44]
  assign c_mshr_io_directory_bits_dirty = directory_io_result_bits_dirty; // @[Scheduler.scala 345:25]
  assign c_mshr_io_directory_bits_state = directory_io_result_bits_state; // @[Scheduler.scala 345:25]
  assign c_mshr_io_directory_bits_clients = directory_io_result_bits_clients; // @[Scheduler.scala 345:25]
  assign c_mshr_io_directory_bits_tag = directory_io_result_bits_tag; // @[Scheduler.scala 345:25]
  assign c_mshr_io_directory_bits_hit = directory_io_result_bits_hit; // @[Scheduler.scala 345:25]
  assign c_mshr_io_directory_bits_way = directory_io_result_bits_way; // @[Scheduler.scala 345:25]
  assign c_mshr_io_schedule_ready = mshr_selectOH[7]; // @[Scheduler.scala 253:28]
  assign c_mshr_io_sinkc_valid = sinkC_io_resp_valid & sinkC_io_resp_bits_set == c_mshr_io_status_bits_set; // @[Scheduler.scala 115:45]
  assign c_mshr_io_sinkc_bits_last = sinkC_io_resp_bits_last; // @[Scheduler.scala 118:21]
  assign c_mshr_io_sinkc_bits_tag = sinkC_io_resp_bits_tag; // @[Scheduler.scala 118:21]
  assign c_mshr_io_sinkc_bits_source = sinkC_io_resp_bits_source; // @[Scheduler.scala 118:21]
  assign c_mshr_io_sinkc_bits_param = sinkC_io_resp_bits_param; // @[Scheduler.scala 118:21]
  assign c_mshr_io_sinkc_bits_data = sinkC_io_resp_bits_data; // @[Scheduler.scala 118:21]
  assign c_mshr_io_sinkd_valid = sinkD_io_resp_valid & sinkD_io_resp_bits_source == 3'h7; // @[Scheduler.scala 116:45]
  assign c_mshr_io_sinkd_bits_last = sinkD_io_resp_bits_last; // @[Scheduler.scala 119:21]
  assign c_mshr_io_sinkd_bits_opcode = sinkD_io_resp_bits_opcode; // @[Scheduler.scala 119:21]
  assign c_mshr_io_sinkd_bits_param = sinkD_io_resp_bits_param; // @[Scheduler.scala 119:21]
  assign c_mshr_io_sinkd_bits_sink = sinkD_io_resp_bits_sink; // @[Scheduler.scala 119:21]
  assign c_mshr_io_sinkd_bits_denied = sinkD_io_resp_bits_denied; // @[Scheduler.scala 119:21]
  assign c_mshr_io_sinke_valid = sinkE_io_resp_valid & sinkE_io_resp_bits_sink == 3'h7; // @[Scheduler.scala 117:45]
  assign c_mshr_io_nestedwb_set = mshr_selectOH[7] ? c_mshr_io_status_bits_set : bc_mshr_io_status_bits_set; // @[Scheduler.scala 183:24]
  assign c_mshr_io_nestedwb_tag = mshr_selectOH[7] ? c_mshr_io_status_bits_tag : bc_mshr_io_status_bits_tag; // @[Scheduler.scala 184:24]
  assign c_mshr_io_nestedwb_b_toN = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h0; // @[Scheduler.scala 185:75]
  assign c_mshr_io_nestedwb_b_toB = _schedule_T_119 & bc_mshr_io_schedule_bits_dir_bits_data_state == 2'h1; // @[Scheduler.scala 186:75]
  assign c_mshr_io_nestedwb_b_clr_dirty = mshr_selectOH[6] & bc_mshr_io_schedule_bits_dir_valid; // @[Scheduler.scala 187:37]
  assign c_mshr_io_nestedwb_c_set_dirty = _schedule_T_120 & c_mshr_io_schedule_bits_dir_bits_data_dirty; // @[Scheduler.scala 188:75]
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      robin_filter <= 8'h0;
    end else if (|mshr_request) begin
      robin_filter <= _robin_filter_T_7;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      directoryFanout <= 9'h0;
    end else if (mshr_uses_directory) begin
      directoryFanout <= {{1'd0}, mshr_selectOH};
    end else if (alloc_uses_directory) begin
      if (alloc) begin
        directoryFanout <= mshr_insertOH;
      end else begin
        directoryFanout <= 9'h80;
      end
    end else begin
      directoryFanout <= 9'h0;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      bankedStore_io_side_adr_valid_REG <= 1'h0;
    end else begin
      bankedStore_io_side_adr_valid_REG <= io_side_adr_valid;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bankedStore_io_side_adr_bits_r_way <= 3'h0;
    end else if (io_side_adr_valid) begin
      bankedStore_io_side_adr_bits_r_way <= io_side_adr_bits_way;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bankedStore_io_side_adr_bits_r_set <= 10'h0;
    end else if (io_side_adr_valid) begin
      bankedStore_io_side_adr_bits_r_set <= io_side_adr_bits_set;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bankedStore_io_side_adr_bits_r_beat <= 2'h0;
    end else if (io_side_adr_valid) begin
      bankedStore_io_side_adr_bits_r_beat <= io_side_adr_bits_beat;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bankedStore_io_side_adr_bits_r_mask <= 2'h0;
    end else if (io_side_adr_valid) begin
      bankedStore_io_side_adr_bits_r_mask <= io_side_adr_bits_mask;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bankedStore_io_side_adr_bits_r_write <= 1'h0;
    end else if (io_side_adr_valid) begin
      bankedStore_io_side_adr_bits_r_write <= io_side_adr_bits_write;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bankedStore_io_side_wdat_r_data <= 128'h0;
    end else if (io_side_adr_valid) begin
      bankedStore_io_side_wdat_r_data <= io_side_wdat_data;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      bankedStore_io_side_wdat_r_poison <= 1'h0;
    end else if (io_side_adr_valid) begin
      bankedStore_io_side_wdat_r_poison <= io_side_wdat_poison;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG__0 <= 8'h0;
    end else begin
      REG__0 <= io_ways_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG__1 <= 8'h0;
    end else begin
      REG__1 <= io_ways_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG__2 <= 8'h0;
    end else begin
      REG__2 <= io_ways_2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG__3 <= 8'h0;
    end else begin
      REG__3 <= io_ways_3;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG__4 <= 8'h0;
    end else begin
      REG__4 <= io_ways_4;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG__5 <= 8'h0;
    end else begin
      REG__5 <= io_ways_5;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG__6 <= 8'h0;
    end else begin
      REG__6 <= io_ways_6;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG__7 <= 8'h0;
    end else begin
      REG__7 <= io_ways_7;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG__8 <= 8'h0;
    end else begin
      REG__8 <= io_ways_8;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG__9 <= 8'h0;
    end else begin
      REG__9 <= io_ways_9;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG__10 <= 8'h0;
    end else begin
      REG__10 <= io_ways_10;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG__11 <= 8'h0;
    end else begin
      REG__11 <= io_ways_11;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG__12 <= 8'h0;
    end else begin
      REG__12 <= io_ways_12;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG__13 <= 8'h0;
    end else begin
      REG__13 <= io_ways_13;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG_1_0 <= 11'h0;
    end else begin
      REG_1_0 <= io_divs_0;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG_1_1 <= 11'h0;
    end else begin
      REG_1_1 <= io_divs_1;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG_1_2 <= 11'h0;
    end else begin
      REG_1_2 <= io_divs_2;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG_1_3 <= 11'h0;
    end else begin
      REG_1_3 <= io_divs_3;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG_1_4 <= 11'h0;
    end else begin
      REG_1_4 <= io_divs_4;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG_1_5 <= 11'h0;
    end else begin
      REG_1_5 <= io_divs_5;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG_1_6 <= 11'h0;
    end else begin
      REG_1_6 <= io_divs_6;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG_1_7 <= 11'h0;
    end else begin
      REG_1_7 <= io_divs_7;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG_1_8 <= 11'h0;
    end else begin
      REG_1_8 <= io_divs_8;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG_1_9 <= 11'h0;
    end else begin
      REG_1_9 <= io_divs_9;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG_1_10 <= 11'h0;
    end else begin
      REG_1_10 <= io_divs_10;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG_1_11 <= 11'h0;
    end else begin
      REG_1_11 <= io_divs_11;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG_1_12 <= 11'h0;
    end else begin
      REG_1_12 <= io_divs_12;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      REG_1_13 <= 11'h0;
    end else begin
      REG_1_13 <= io_divs_13;
    end
  end
  always @(posedge clock or posedge reset) begin
    if (reset) begin
      injectValid <= 1'h0;
    end else if (~io_error_valid | injectReady | io_error_ready) begin
      injectValid <= 1'h0;
    end else begin
      injectValid <= _GEN_171;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      injectBits_dir <= 1'h0;
    end else if (io_error_valid) begin
      injectBits_dir <= io_error_bits_dir;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      injectBits_bit <= 8'h0;
    end else if (io_error_valid) begin
      injectBits_bit <= io_error_bits_bit;
    end
  end
  always @(posedge clock or posedge rf_reset) begin
    if (rf_reset) begin
      io_error_ready_REG <= 1'h0;
    end else begin
      io_error_ready_REG <= injectValid & _injectReady_T;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  robin_filter = _RAND_0[7:0];
  _RAND_1 = {1{`RANDOM}};
  directoryFanout = _RAND_1[8:0];
  _RAND_2 = {1{`RANDOM}};
  bankedStore_io_side_adr_valid_REG = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  bankedStore_io_side_adr_bits_r_way = _RAND_3[2:0];
  _RAND_4 = {1{`RANDOM}};
  bankedStore_io_side_adr_bits_r_set = _RAND_4[9:0];
  _RAND_5 = {1{`RANDOM}};
  bankedStore_io_side_adr_bits_r_beat = _RAND_5[1:0];
  _RAND_6 = {1{`RANDOM}};
  bankedStore_io_side_adr_bits_r_mask = _RAND_6[1:0];
  _RAND_7 = {1{`RANDOM}};
  bankedStore_io_side_adr_bits_r_write = _RAND_7[0:0];
  _RAND_8 = {4{`RANDOM}};
  bankedStore_io_side_wdat_r_data = _RAND_8[127:0];
  _RAND_9 = {1{`RANDOM}};
  bankedStore_io_side_wdat_r_poison = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  REG__0 = _RAND_10[7:0];
  _RAND_11 = {1{`RANDOM}};
  REG__1 = _RAND_11[7:0];
  _RAND_12 = {1{`RANDOM}};
  REG__2 = _RAND_12[7:0];
  _RAND_13 = {1{`RANDOM}};
  REG__3 = _RAND_13[7:0];
  _RAND_14 = {1{`RANDOM}};
  REG__4 = _RAND_14[7:0];
  _RAND_15 = {1{`RANDOM}};
  REG__5 = _RAND_15[7:0];
  _RAND_16 = {1{`RANDOM}};
  REG__6 = _RAND_16[7:0];
  _RAND_17 = {1{`RANDOM}};
  REG__7 = _RAND_17[7:0];
  _RAND_18 = {1{`RANDOM}};
  REG__8 = _RAND_18[7:0];
  _RAND_19 = {1{`RANDOM}};
  REG__9 = _RAND_19[7:0];
  _RAND_20 = {1{`RANDOM}};
  REG__10 = _RAND_20[7:0];
  _RAND_21 = {1{`RANDOM}};
  REG__11 = _RAND_21[7:0];
  _RAND_22 = {1{`RANDOM}};
  REG__12 = _RAND_22[7:0];
  _RAND_23 = {1{`RANDOM}};
  REG__13 = _RAND_23[7:0];
  _RAND_24 = {1{`RANDOM}};
  REG_1_0 = _RAND_24[10:0];
  _RAND_25 = {1{`RANDOM}};
  REG_1_1 = _RAND_25[10:0];
  _RAND_26 = {1{`RANDOM}};
  REG_1_2 = _RAND_26[10:0];
  _RAND_27 = {1{`RANDOM}};
  REG_1_3 = _RAND_27[10:0];
  _RAND_28 = {1{`RANDOM}};
  REG_1_4 = _RAND_28[10:0];
  _RAND_29 = {1{`RANDOM}};
  REG_1_5 = _RAND_29[10:0];
  _RAND_30 = {1{`RANDOM}};
  REG_1_6 = _RAND_30[10:0];
  _RAND_31 = {1{`RANDOM}};
  REG_1_7 = _RAND_31[10:0];
  _RAND_32 = {1{`RANDOM}};
  REG_1_8 = _RAND_32[10:0];
  _RAND_33 = {1{`RANDOM}};
  REG_1_9 = _RAND_33[10:0];
  _RAND_34 = {1{`RANDOM}};
  REG_1_10 = _RAND_34[10:0];
  _RAND_35 = {1{`RANDOM}};
  REG_1_11 = _RAND_35[10:0];
  _RAND_36 = {1{`RANDOM}};
  REG_1_12 = _RAND_36[10:0];
  _RAND_37 = {1{`RANDOM}};
  REG_1_13 = _RAND_37[10:0];
  _RAND_38 = {1{`RANDOM}};
  injectValid = _RAND_38[0:0];
  _RAND_39 = {1{`RANDOM}};
  injectBits_dir = _RAND_39[0:0];
  _RAND_40 = {1{`RANDOM}};
  injectBits_bit = _RAND_40[7:0];
  _RAND_41 = {1{`RANDOM}};
  io_error_ready_REG = _RAND_41[0:0];
`endif // RANDOMIZE_REG_INIT
  if (reset) begin
    robin_filter = 8'h0;
  end
  if (reset) begin
    directoryFanout = 9'h0;
  end
  if (reset) begin
    bankedStore_io_side_adr_valid_REG = 1'h0;
  end
  if (rf_reset) begin
    bankedStore_io_side_adr_bits_r_way = 3'h0;
  end
  if (rf_reset) begin
    bankedStore_io_side_adr_bits_r_set = 10'h0;
  end
  if (rf_reset) begin
    bankedStore_io_side_adr_bits_r_beat = 2'h0;
  end
  if (rf_reset) begin
    bankedStore_io_side_adr_bits_r_mask = 2'h0;
  end
  if (rf_reset) begin
    bankedStore_io_side_adr_bits_r_write = 1'h0;
  end
  if (rf_reset) begin
    bankedStore_io_side_wdat_r_data = 128'h0;
  end
  if (rf_reset) begin
    bankedStore_io_side_wdat_r_poison = 1'h0;
  end
  if (rf_reset) begin
    REG__0 = 8'h0;
  end
  if (rf_reset) begin
    REG__1 = 8'h0;
  end
  if (rf_reset) begin
    REG__2 = 8'h0;
  end
  if (rf_reset) begin
    REG__3 = 8'h0;
  end
  if (rf_reset) begin
    REG__4 = 8'h0;
  end
  if (rf_reset) begin
    REG__5 = 8'h0;
  end
  if (rf_reset) begin
    REG__6 = 8'h0;
  end
  if (rf_reset) begin
    REG__7 = 8'h0;
  end
  if (rf_reset) begin
    REG__8 = 8'h0;
  end
  if (rf_reset) begin
    REG__9 = 8'h0;
  end
  if (rf_reset) begin
    REG__10 = 8'h0;
  end
  if (rf_reset) begin
    REG__11 = 8'h0;
  end
  if (rf_reset) begin
    REG__12 = 8'h0;
  end
  if (rf_reset) begin
    REG__13 = 8'h0;
  end
  if (rf_reset) begin
    REG_1_0 = 11'h0;
  end
  if (rf_reset) begin
    REG_1_1 = 11'h0;
  end
  if (rf_reset) begin
    REG_1_2 = 11'h0;
  end
  if (rf_reset) begin
    REG_1_3 = 11'h0;
  end
  if (rf_reset) begin
    REG_1_4 = 11'h0;
  end
  if (rf_reset) begin
    REG_1_5 = 11'h0;
  end
  if (rf_reset) begin
    REG_1_6 = 11'h0;
  end
  if (rf_reset) begin
    REG_1_7 = 11'h0;
  end
  if (rf_reset) begin
    REG_1_8 = 11'h0;
  end
  if (rf_reset) begin
    REG_1_9 = 11'h0;
  end
  if (rf_reset) begin
    REG_1_10 = 11'h0;
  end
  if (rf_reset) begin
    REG_1_11 = 11'h0;
  end
  if (rf_reset) begin
    REG_1_12 = 11'h0;
  end
  if (rf_reset) begin
    REG_1_13 = 11'h0;
  end
  if (reset) begin
    injectValid = 1'h0;
  end
  if (rf_reset) begin
    injectBits_dir = 1'h0;
  end
  if (rf_reset) begin
    injectBits_bit = 8'h0;
  end
  if (rf_reset) begin
    io_error_ready_REG = 1'h0;
  end
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
