//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, f4c204eb-3c4c-42f9-85e5-a50f79bbed88
module RHEA__ClockSinkDomain(
  input         rf_reset,
  input         auto_plic_int_in_0,
  input         auto_plic_int_in_1,
  input         auto_plic_int_in_2,
  input         auto_plic_int_in_3,
  input         auto_plic_int_in_4,
  input         auto_plic_int_in_5,
  input         auto_plic_int_in_6,
  input         auto_plic_int_in_7,
  input         auto_plic_int_in_8,
  input         auto_plic_int_in_9,
  input         auto_plic_int_in_10,
  input         auto_plic_int_in_11,
  input         auto_plic_int_in_12,
  input         auto_plic_int_in_13,
  input         auto_plic_int_in_14,
  input         auto_plic_int_in_15,
  input         auto_plic_int_in_16,
  input         auto_plic_int_in_17,
  input         auto_plic_int_in_18,
  input         auto_plic_int_in_19,
  input         auto_plic_int_in_20,
  input         auto_plic_int_in_21,
  input         auto_plic_int_in_22,
  input         auto_plic_int_in_23,
  input         auto_plic_int_in_24,
  input         auto_plic_int_in_25,
  input         auto_plic_int_in_26,
  input         auto_plic_int_in_27,
  input         auto_plic_int_in_28,
  input         auto_plic_int_in_29,
  input         auto_plic_int_in_30,
  input         auto_plic_int_in_31,
  input         auto_plic_int_in_32,
  input         auto_plic_int_in_33,
  input         auto_plic_int_in_34,
  input         auto_plic_int_in_35,
  input         auto_plic_int_in_36,
  input         auto_plic_int_in_37,
  input         auto_plic_int_in_38,
  input         auto_plic_int_in_39,
  input         auto_plic_int_in_40,
  input         auto_plic_int_in_41,
  input         auto_plic_int_in_42,
  input         auto_plic_int_in_43,
  input         auto_plic_int_in_44,
  input         auto_plic_int_in_45,
  input         auto_plic_int_in_46,
  input         auto_plic_int_in_47,
  input         auto_plic_int_in_48,
  input         auto_plic_int_in_49,
  input         auto_plic_int_in_50,
  input         auto_plic_int_in_51,
  input         auto_plic_int_in_52,
  input         auto_plic_int_in_53,
  input         auto_plic_int_in_54,
  input         auto_plic_int_in_55,
  input         auto_plic_int_in_56,
  input         auto_plic_int_in_57,
  input         auto_plic_int_in_58,
  input         auto_plic_int_in_59,
  input         auto_plic_int_in_60,
  input         auto_plic_int_in_61,
  input         auto_plic_int_in_62,
  input         auto_plic_int_in_63,
  input         auto_plic_int_in_64,
  input         auto_plic_int_in_65,
  input         auto_plic_int_in_66,
  input         auto_plic_int_in_67,
  input         auto_plic_int_in_68,
  input         auto_plic_int_in_69,
  input         auto_plic_int_in_70,
  input         auto_plic_int_in_71,
  input         auto_plic_int_in_72,
  input         auto_plic_int_in_73,
  input         auto_plic_int_in_74,
  input         auto_plic_int_in_75,
  input         auto_plic_int_in_76,
  input         auto_plic_int_in_77,
  input         auto_plic_int_in_78,
  input         auto_plic_int_in_79,
  input         auto_plic_int_in_80,
  input         auto_plic_int_in_81,
  input         auto_plic_int_in_82,
  input         auto_plic_int_in_83,
  input         auto_plic_int_in_84,
  input         auto_plic_int_in_85,
  input         auto_plic_int_in_86,
  input         auto_plic_int_in_87,
  input         auto_plic_int_in_88,
  input         auto_plic_int_in_89,
  input         auto_plic_int_in_90,
  input         auto_plic_int_in_91,
  input         auto_plic_int_in_92,
  input         auto_plic_int_in_93,
  input         auto_plic_int_in_94,
  input         auto_plic_int_in_95,
  input         auto_plic_int_in_96,
  input         auto_plic_int_in_97,
  input         auto_plic_int_in_98,
  input         auto_plic_int_in_99,
  input         auto_plic_int_in_100,
  input         auto_plic_int_in_101,
  input         auto_plic_int_in_102,
  input         auto_plic_int_in_103,
  input         auto_plic_int_in_104,
  input         auto_plic_int_in_105,
  input         auto_plic_int_in_106,
  input         auto_plic_int_in_107,
  input         auto_plic_int_in_108,
  input         auto_plic_int_in_109,
  input         auto_plic_int_in_110,
  input         auto_plic_int_in_111,
  input         auto_plic_int_in_112,
  input         auto_plic_int_in_113,
  input         auto_plic_int_in_114,
  input         auto_plic_int_in_115,
  input         auto_plic_int_in_116,
  input         auto_plic_int_in_117,
  input         auto_plic_int_in_118,
  input         auto_plic_int_in_119,
  input         auto_plic_int_in_120,
  input         auto_plic_int_in_121,
  input         auto_plic_int_in_122,
  input         auto_plic_int_in_123,
  input         auto_plic_int_in_124,
  input         auto_plic_int_in_125,
  input         auto_plic_int_in_126,
  input         auto_plic_int_in_127,
  input         auto_plic_int_in_128,
  input         auto_plic_int_in_129,
  input         auto_plic_int_in_130,
  input         auto_plic_int_in_131,
  input         auto_plic_int_in_132,
  input         auto_plic_int_in_133,
  input         auto_plic_int_in_134,
  input         auto_plic_int_in_135,
  input         auto_plic_int_in_136,
  input         auto_plic_int_in_137,
  input         auto_plic_int_in_138,
  input         auto_plic_int_in_139,
  input         auto_plic_int_in_140,
  input         auto_plic_int_in_141,
  input         auto_plic_int_in_142,
  input         auto_plic_int_in_143,
  input         auto_plic_int_in_144,
  input         auto_plic_int_in_145,
  input         auto_plic_int_in_146,
  input         auto_plic_int_in_147,
  input         auto_plic_int_in_148,
  input         auto_plic_int_in_149,
  input         auto_plic_int_in_150,
  input         auto_plic_int_in_151,
  input         auto_plic_int_in_152,
  input         auto_plic_int_in_153,
  input         auto_plic_int_in_154,
  input         auto_plic_int_in_155,
  input         auto_plic_int_in_156,
  input         auto_plic_int_in_157,
  input         auto_plic_int_in_158,
  input         auto_plic_int_in_159,
  input         auto_plic_int_in_160,
  input         auto_plic_int_in_161,
  input         auto_plic_int_in_162,
  input         auto_plic_int_in_163,
  input         auto_plic_int_in_164,
  input         auto_plic_int_in_165,
  input         auto_plic_int_in_166,
  input         auto_plic_int_in_167,
  input         auto_plic_int_in_168,
  input         auto_plic_int_in_169,
  input         auto_plic_int_in_170,
  input         auto_plic_int_in_171,
  input         auto_plic_int_in_172,
  input         auto_plic_int_in_173,
  input         auto_plic_int_in_174,
  input         auto_plic_int_in_175,
  input         auto_plic_int_in_176,
  input         auto_plic_int_in_177,
  input         auto_plic_int_in_178,
  input         auto_plic_int_in_179,
  input         auto_plic_int_in_180,
  input         auto_plic_int_in_181,
  input         auto_plic_int_in_182,
  input         auto_plic_int_in_183,
  input         auto_plic_int_in_184,
  input         auto_plic_int_in_185,
  input         auto_plic_int_in_186,
  input         auto_plic_int_in_187,
  input         auto_plic_int_in_188,
  input         auto_plic_int_in_189,
  input         auto_plic_int_in_190,
  input         auto_plic_int_in_191,
  input         auto_plic_int_in_192,
  input         auto_plic_int_in_193,
  input         auto_plic_int_in_194,
  input         auto_plic_int_in_195,
  input         auto_plic_int_in_196,
  input         auto_plic_int_in_197,
  input         auto_plic_int_in_198,
  input         auto_plic_int_in_199,
  input         auto_plic_int_in_200,
  input         auto_plic_int_in_201,
  input         auto_plic_int_in_202,
  input         auto_plic_int_in_203,
  input         auto_plic_int_in_204,
  input         auto_plic_int_in_205,
  input         auto_plic_int_in_206,
  input         auto_plic_int_in_207,
  input         auto_plic_int_in_208,
  input         auto_plic_int_in_209,
  input         auto_plic_int_in_210,
  input         auto_plic_int_in_211,
  input         auto_plic_int_in_212,
  input         auto_plic_int_in_213,
  input         auto_plic_int_in_214,
  input         auto_plic_int_in_215,
  input         auto_plic_int_in_216,
  input         auto_plic_int_in_217,
  input         auto_plic_int_in_218,
  input         auto_plic_int_in_219,
  input         auto_plic_int_in_220,
  input         auto_plic_int_in_221,
  input         auto_plic_int_in_222,
  input         auto_plic_int_in_223,
  input         auto_plic_int_in_224,
  input         auto_plic_int_in_225,
  input         auto_plic_int_in_226,
  input         auto_plic_int_in_227,
  input         auto_plic_int_in_228,
  input         auto_plic_int_in_229,
  input         auto_plic_int_in_230,
  input         auto_plic_int_in_231,
  input         auto_plic_int_in_232,
  input         auto_plic_int_in_233,
  input         auto_plic_int_in_234,
  input         auto_plic_int_in_235,
  input         auto_plic_int_in_236,
  input         auto_plic_int_in_237,
  input         auto_plic_int_in_238,
  input         auto_plic_int_in_239,
  input         auto_plic_int_in_240,
  input         auto_plic_int_in_241,
  input         auto_plic_int_in_242,
  input         auto_plic_int_in_243,
  input         auto_plic_int_in_244,
  input         auto_plic_int_in_245,
  input         auto_plic_int_in_246,
  input         auto_plic_int_in_247,
  input         auto_plic_int_in_248,
  input         auto_plic_int_in_249,
  input         auto_plic_int_in_250,
  input         auto_plic_int_in_251,
  input         auto_plic_int_in_252,
  input         auto_plic_int_in_253,
  input         auto_plic_int_in_254,
  input         auto_plic_int_in_255,
  output        auto_plic_int_out_1_0,
  output        auto_plic_int_out_1_1,
  output        auto_plic_int_out_0_0,
  output        auto_plic_int_out_0_1,
  output        auto_plic_in_a_ready,
  input         auto_plic_in_a_valid,
  input  [2:0]  auto_plic_in_a_bits_opcode,
  input  [2:0]  auto_plic_in_a_bits_param,
  input  [1:0]  auto_plic_in_a_bits_size,
  input  [10:0] auto_plic_in_a_bits_source,
  input  [27:0] auto_plic_in_a_bits_address,
  input  [7:0]  auto_plic_in_a_bits_mask,
  input  [63:0] auto_plic_in_a_bits_data,
  input         auto_plic_in_a_bits_corrupt,
  input         auto_plic_in_d_ready,
  output        auto_plic_in_d_valid,
  output [2:0]  auto_plic_in_d_bits_opcode,
  output [1:0]  auto_plic_in_d_bits_size,
  output [10:0] auto_plic_in_d_bits_source,
  output [63:0] auto_plic_in_d_bits_data,
  input         auto_clock_in_clock,
  input         auto_clock_in_reset
);
  wire  plic_rf_reset; // @[Plic.scala 348:46]
  wire  plic_clock; // @[Plic.scala 348:46]
  wire  plic_reset; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_0; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_1; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_2; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_3; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_4; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_5; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_6; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_7; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_8; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_9; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_10; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_11; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_12; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_13; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_14; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_15; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_16; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_17; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_18; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_19; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_20; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_21; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_22; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_23; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_24; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_25; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_26; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_27; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_28; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_29; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_30; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_31; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_32; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_33; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_34; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_35; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_36; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_37; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_38; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_39; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_40; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_41; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_42; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_43; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_44; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_45; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_46; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_47; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_48; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_49; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_50; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_51; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_52; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_53; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_54; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_55; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_56; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_57; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_58; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_59; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_60; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_61; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_62; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_63; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_64; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_65; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_66; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_67; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_68; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_69; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_70; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_71; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_72; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_73; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_74; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_75; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_76; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_77; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_78; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_79; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_80; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_81; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_82; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_83; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_84; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_85; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_86; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_87; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_88; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_89; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_90; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_91; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_92; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_93; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_94; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_95; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_96; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_97; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_98; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_99; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_100; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_101; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_102; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_103; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_104; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_105; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_106; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_107; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_108; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_109; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_110; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_111; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_112; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_113; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_114; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_115; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_116; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_117; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_118; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_119; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_120; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_121; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_122; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_123; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_124; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_125; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_126; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_127; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_128; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_129; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_130; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_131; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_132; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_133; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_134; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_135; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_136; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_137; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_138; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_139; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_140; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_141; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_142; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_143; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_144; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_145; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_146; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_147; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_148; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_149; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_150; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_151; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_152; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_153; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_154; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_155; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_156; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_157; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_158; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_159; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_160; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_161; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_162; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_163; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_164; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_165; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_166; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_167; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_168; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_169; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_170; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_171; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_172; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_173; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_174; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_175; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_176; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_177; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_178; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_179; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_180; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_181; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_182; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_183; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_184; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_185; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_186; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_187; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_188; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_189; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_190; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_191; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_192; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_193; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_194; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_195; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_196; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_197; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_198; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_199; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_200; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_201; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_202; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_203; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_204; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_205; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_206; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_207; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_208; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_209; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_210; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_211; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_212; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_213; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_214; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_215; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_216; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_217; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_218; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_219; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_220; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_221; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_222; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_223; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_224; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_225; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_226; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_227; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_228; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_229; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_230; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_231; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_232; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_233; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_234; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_235; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_236; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_237; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_238; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_239; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_240; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_241; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_242; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_243; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_244; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_245; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_246; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_247; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_248; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_249; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_250; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_251; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_252; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_253; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_254; // @[Plic.scala 348:46]
  wire  plic_auto_int_in_255; // @[Plic.scala 348:46]
  wire  plic_auto_int_out_1_0; // @[Plic.scala 348:46]
  wire  plic_auto_int_out_1_1; // @[Plic.scala 348:46]
  wire  plic_auto_int_out_0_0; // @[Plic.scala 348:46]
  wire  plic_auto_int_out_0_1; // @[Plic.scala 348:46]
  wire  plic_auto_in_a_ready; // @[Plic.scala 348:46]
  wire  plic_auto_in_a_valid; // @[Plic.scala 348:46]
  wire [2:0] plic_auto_in_a_bits_opcode; // @[Plic.scala 348:46]
  wire [2:0] plic_auto_in_a_bits_param; // @[Plic.scala 348:46]
  wire [1:0] plic_auto_in_a_bits_size; // @[Plic.scala 348:46]
  wire [10:0] plic_auto_in_a_bits_source; // @[Plic.scala 348:46]
  wire [27:0] plic_auto_in_a_bits_address; // @[Plic.scala 348:46]
  wire [7:0] plic_auto_in_a_bits_mask; // @[Plic.scala 348:46]
  wire [63:0] plic_auto_in_a_bits_data; // @[Plic.scala 348:46]
  wire  plic_auto_in_a_bits_corrupt; // @[Plic.scala 348:46]
  wire  plic_auto_in_d_ready; // @[Plic.scala 348:46]
  wire  plic_auto_in_d_valid; // @[Plic.scala 348:46]
  wire [2:0] plic_auto_in_d_bits_opcode; // @[Plic.scala 348:46]
  wire [1:0] plic_auto_in_d_bits_size; // @[Plic.scala 348:46]
  wire [10:0] plic_auto_in_d_bits_source; // @[Plic.scala 348:46]
  wire [63:0] plic_auto_in_d_bits_data; // @[Plic.scala 348:46]
  RHEA__TLPLIC plic ( // @[Plic.scala 348:46]
    .rf_reset(plic_rf_reset),
    .clock(plic_clock),
    .reset(plic_reset),
    .auto_int_in_0(plic_auto_int_in_0),
    .auto_int_in_1(plic_auto_int_in_1),
    .auto_int_in_2(plic_auto_int_in_2),
    .auto_int_in_3(plic_auto_int_in_3),
    .auto_int_in_4(plic_auto_int_in_4),
    .auto_int_in_5(plic_auto_int_in_5),
    .auto_int_in_6(plic_auto_int_in_6),
    .auto_int_in_7(plic_auto_int_in_7),
    .auto_int_in_8(plic_auto_int_in_8),
    .auto_int_in_9(plic_auto_int_in_9),
    .auto_int_in_10(plic_auto_int_in_10),
    .auto_int_in_11(plic_auto_int_in_11),
    .auto_int_in_12(plic_auto_int_in_12),
    .auto_int_in_13(plic_auto_int_in_13),
    .auto_int_in_14(plic_auto_int_in_14),
    .auto_int_in_15(plic_auto_int_in_15),
    .auto_int_in_16(plic_auto_int_in_16),
    .auto_int_in_17(plic_auto_int_in_17),
    .auto_int_in_18(plic_auto_int_in_18),
    .auto_int_in_19(plic_auto_int_in_19),
    .auto_int_in_20(plic_auto_int_in_20),
    .auto_int_in_21(plic_auto_int_in_21),
    .auto_int_in_22(plic_auto_int_in_22),
    .auto_int_in_23(plic_auto_int_in_23),
    .auto_int_in_24(plic_auto_int_in_24),
    .auto_int_in_25(plic_auto_int_in_25),
    .auto_int_in_26(plic_auto_int_in_26),
    .auto_int_in_27(plic_auto_int_in_27),
    .auto_int_in_28(plic_auto_int_in_28),
    .auto_int_in_29(plic_auto_int_in_29),
    .auto_int_in_30(plic_auto_int_in_30),
    .auto_int_in_31(plic_auto_int_in_31),
    .auto_int_in_32(plic_auto_int_in_32),
    .auto_int_in_33(plic_auto_int_in_33),
    .auto_int_in_34(plic_auto_int_in_34),
    .auto_int_in_35(plic_auto_int_in_35),
    .auto_int_in_36(plic_auto_int_in_36),
    .auto_int_in_37(plic_auto_int_in_37),
    .auto_int_in_38(plic_auto_int_in_38),
    .auto_int_in_39(plic_auto_int_in_39),
    .auto_int_in_40(plic_auto_int_in_40),
    .auto_int_in_41(plic_auto_int_in_41),
    .auto_int_in_42(plic_auto_int_in_42),
    .auto_int_in_43(plic_auto_int_in_43),
    .auto_int_in_44(plic_auto_int_in_44),
    .auto_int_in_45(plic_auto_int_in_45),
    .auto_int_in_46(plic_auto_int_in_46),
    .auto_int_in_47(plic_auto_int_in_47),
    .auto_int_in_48(plic_auto_int_in_48),
    .auto_int_in_49(plic_auto_int_in_49),
    .auto_int_in_50(plic_auto_int_in_50),
    .auto_int_in_51(plic_auto_int_in_51),
    .auto_int_in_52(plic_auto_int_in_52),
    .auto_int_in_53(plic_auto_int_in_53),
    .auto_int_in_54(plic_auto_int_in_54),
    .auto_int_in_55(plic_auto_int_in_55),
    .auto_int_in_56(plic_auto_int_in_56),
    .auto_int_in_57(plic_auto_int_in_57),
    .auto_int_in_58(plic_auto_int_in_58),
    .auto_int_in_59(plic_auto_int_in_59),
    .auto_int_in_60(plic_auto_int_in_60),
    .auto_int_in_61(plic_auto_int_in_61),
    .auto_int_in_62(plic_auto_int_in_62),
    .auto_int_in_63(plic_auto_int_in_63),
    .auto_int_in_64(plic_auto_int_in_64),
    .auto_int_in_65(plic_auto_int_in_65),
    .auto_int_in_66(plic_auto_int_in_66),
    .auto_int_in_67(plic_auto_int_in_67),
    .auto_int_in_68(plic_auto_int_in_68),
    .auto_int_in_69(plic_auto_int_in_69),
    .auto_int_in_70(plic_auto_int_in_70),
    .auto_int_in_71(plic_auto_int_in_71),
    .auto_int_in_72(plic_auto_int_in_72),
    .auto_int_in_73(plic_auto_int_in_73),
    .auto_int_in_74(plic_auto_int_in_74),
    .auto_int_in_75(plic_auto_int_in_75),
    .auto_int_in_76(plic_auto_int_in_76),
    .auto_int_in_77(plic_auto_int_in_77),
    .auto_int_in_78(plic_auto_int_in_78),
    .auto_int_in_79(plic_auto_int_in_79),
    .auto_int_in_80(plic_auto_int_in_80),
    .auto_int_in_81(plic_auto_int_in_81),
    .auto_int_in_82(plic_auto_int_in_82),
    .auto_int_in_83(plic_auto_int_in_83),
    .auto_int_in_84(plic_auto_int_in_84),
    .auto_int_in_85(plic_auto_int_in_85),
    .auto_int_in_86(plic_auto_int_in_86),
    .auto_int_in_87(plic_auto_int_in_87),
    .auto_int_in_88(plic_auto_int_in_88),
    .auto_int_in_89(plic_auto_int_in_89),
    .auto_int_in_90(plic_auto_int_in_90),
    .auto_int_in_91(plic_auto_int_in_91),
    .auto_int_in_92(plic_auto_int_in_92),
    .auto_int_in_93(plic_auto_int_in_93),
    .auto_int_in_94(plic_auto_int_in_94),
    .auto_int_in_95(plic_auto_int_in_95),
    .auto_int_in_96(plic_auto_int_in_96),
    .auto_int_in_97(plic_auto_int_in_97),
    .auto_int_in_98(plic_auto_int_in_98),
    .auto_int_in_99(plic_auto_int_in_99),
    .auto_int_in_100(plic_auto_int_in_100),
    .auto_int_in_101(plic_auto_int_in_101),
    .auto_int_in_102(plic_auto_int_in_102),
    .auto_int_in_103(plic_auto_int_in_103),
    .auto_int_in_104(plic_auto_int_in_104),
    .auto_int_in_105(plic_auto_int_in_105),
    .auto_int_in_106(plic_auto_int_in_106),
    .auto_int_in_107(plic_auto_int_in_107),
    .auto_int_in_108(plic_auto_int_in_108),
    .auto_int_in_109(plic_auto_int_in_109),
    .auto_int_in_110(plic_auto_int_in_110),
    .auto_int_in_111(plic_auto_int_in_111),
    .auto_int_in_112(plic_auto_int_in_112),
    .auto_int_in_113(plic_auto_int_in_113),
    .auto_int_in_114(plic_auto_int_in_114),
    .auto_int_in_115(plic_auto_int_in_115),
    .auto_int_in_116(plic_auto_int_in_116),
    .auto_int_in_117(plic_auto_int_in_117),
    .auto_int_in_118(plic_auto_int_in_118),
    .auto_int_in_119(plic_auto_int_in_119),
    .auto_int_in_120(plic_auto_int_in_120),
    .auto_int_in_121(plic_auto_int_in_121),
    .auto_int_in_122(plic_auto_int_in_122),
    .auto_int_in_123(plic_auto_int_in_123),
    .auto_int_in_124(plic_auto_int_in_124),
    .auto_int_in_125(plic_auto_int_in_125),
    .auto_int_in_126(plic_auto_int_in_126),
    .auto_int_in_127(plic_auto_int_in_127),
    .auto_int_in_128(plic_auto_int_in_128),
    .auto_int_in_129(plic_auto_int_in_129),
    .auto_int_in_130(plic_auto_int_in_130),
    .auto_int_in_131(plic_auto_int_in_131),
    .auto_int_in_132(plic_auto_int_in_132),
    .auto_int_in_133(plic_auto_int_in_133),
    .auto_int_in_134(plic_auto_int_in_134),
    .auto_int_in_135(plic_auto_int_in_135),
    .auto_int_in_136(plic_auto_int_in_136),
    .auto_int_in_137(plic_auto_int_in_137),
    .auto_int_in_138(plic_auto_int_in_138),
    .auto_int_in_139(plic_auto_int_in_139),
    .auto_int_in_140(plic_auto_int_in_140),
    .auto_int_in_141(plic_auto_int_in_141),
    .auto_int_in_142(plic_auto_int_in_142),
    .auto_int_in_143(plic_auto_int_in_143),
    .auto_int_in_144(plic_auto_int_in_144),
    .auto_int_in_145(plic_auto_int_in_145),
    .auto_int_in_146(plic_auto_int_in_146),
    .auto_int_in_147(plic_auto_int_in_147),
    .auto_int_in_148(plic_auto_int_in_148),
    .auto_int_in_149(plic_auto_int_in_149),
    .auto_int_in_150(plic_auto_int_in_150),
    .auto_int_in_151(plic_auto_int_in_151),
    .auto_int_in_152(plic_auto_int_in_152),
    .auto_int_in_153(plic_auto_int_in_153),
    .auto_int_in_154(plic_auto_int_in_154),
    .auto_int_in_155(plic_auto_int_in_155),
    .auto_int_in_156(plic_auto_int_in_156),
    .auto_int_in_157(plic_auto_int_in_157),
    .auto_int_in_158(plic_auto_int_in_158),
    .auto_int_in_159(plic_auto_int_in_159),
    .auto_int_in_160(plic_auto_int_in_160),
    .auto_int_in_161(plic_auto_int_in_161),
    .auto_int_in_162(plic_auto_int_in_162),
    .auto_int_in_163(plic_auto_int_in_163),
    .auto_int_in_164(plic_auto_int_in_164),
    .auto_int_in_165(plic_auto_int_in_165),
    .auto_int_in_166(plic_auto_int_in_166),
    .auto_int_in_167(plic_auto_int_in_167),
    .auto_int_in_168(plic_auto_int_in_168),
    .auto_int_in_169(plic_auto_int_in_169),
    .auto_int_in_170(plic_auto_int_in_170),
    .auto_int_in_171(plic_auto_int_in_171),
    .auto_int_in_172(plic_auto_int_in_172),
    .auto_int_in_173(plic_auto_int_in_173),
    .auto_int_in_174(plic_auto_int_in_174),
    .auto_int_in_175(plic_auto_int_in_175),
    .auto_int_in_176(plic_auto_int_in_176),
    .auto_int_in_177(plic_auto_int_in_177),
    .auto_int_in_178(plic_auto_int_in_178),
    .auto_int_in_179(plic_auto_int_in_179),
    .auto_int_in_180(plic_auto_int_in_180),
    .auto_int_in_181(plic_auto_int_in_181),
    .auto_int_in_182(plic_auto_int_in_182),
    .auto_int_in_183(plic_auto_int_in_183),
    .auto_int_in_184(plic_auto_int_in_184),
    .auto_int_in_185(plic_auto_int_in_185),
    .auto_int_in_186(plic_auto_int_in_186),
    .auto_int_in_187(plic_auto_int_in_187),
    .auto_int_in_188(plic_auto_int_in_188),
    .auto_int_in_189(plic_auto_int_in_189),
    .auto_int_in_190(plic_auto_int_in_190),
    .auto_int_in_191(plic_auto_int_in_191),
    .auto_int_in_192(plic_auto_int_in_192),
    .auto_int_in_193(plic_auto_int_in_193),
    .auto_int_in_194(plic_auto_int_in_194),
    .auto_int_in_195(plic_auto_int_in_195),
    .auto_int_in_196(plic_auto_int_in_196),
    .auto_int_in_197(plic_auto_int_in_197),
    .auto_int_in_198(plic_auto_int_in_198),
    .auto_int_in_199(plic_auto_int_in_199),
    .auto_int_in_200(plic_auto_int_in_200),
    .auto_int_in_201(plic_auto_int_in_201),
    .auto_int_in_202(plic_auto_int_in_202),
    .auto_int_in_203(plic_auto_int_in_203),
    .auto_int_in_204(plic_auto_int_in_204),
    .auto_int_in_205(plic_auto_int_in_205),
    .auto_int_in_206(plic_auto_int_in_206),
    .auto_int_in_207(plic_auto_int_in_207),
    .auto_int_in_208(plic_auto_int_in_208),
    .auto_int_in_209(plic_auto_int_in_209),
    .auto_int_in_210(plic_auto_int_in_210),
    .auto_int_in_211(plic_auto_int_in_211),
    .auto_int_in_212(plic_auto_int_in_212),
    .auto_int_in_213(plic_auto_int_in_213),
    .auto_int_in_214(plic_auto_int_in_214),
    .auto_int_in_215(plic_auto_int_in_215),
    .auto_int_in_216(plic_auto_int_in_216),
    .auto_int_in_217(plic_auto_int_in_217),
    .auto_int_in_218(plic_auto_int_in_218),
    .auto_int_in_219(plic_auto_int_in_219),
    .auto_int_in_220(plic_auto_int_in_220),
    .auto_int_in_221(plic_auto_int_in_221),
    .auto_int_in_222(plic_auto_int_in_222),
    .auto_int_in_223(plic_auto_int_in_223),
    .auto_int_in_224(plic_auto_int_in_224),
    .auto_int_in_225(plic_auto_int_in_225),
    .auto_int_in_226(plic_auto_int_in_226),
    .auto_int_in_227(plic_auto_int_in_227),
    .auto_int_in_228(plic_auto_int_in_228),
    .auto_int_in_229(plic_auto_int_in_229),
    .auto_int_in_230(plic_auto_int_in_230),
    .auto_int_in_231(plic_auto_int_in_231),
    .auto_int_in_232(plic_auto_int_in_232),
    .auto_int_in_233(plic_auto_int_in_233),
    .auto_int_in_234(plic_auto_int_in_234),
    .auto_int_in_235(plic_auto_int_in_235),
    .auto_int_in_236(plic_auto_int_in_236),
    .auto_int_in_237(plic_auto_int_in_237),
    .auto_int_in_238(plic_auto_int_in_238),
    .auto_int_in_239(plic_auto_int_in_239),
    .auto_int_in_240(plic_auto_int_in_240),
    .auto_int_in_241(plic_auto_int_in_241),
    .auto_int_in_242(plic_auto_int_in_242),
    .auto_int_in_243(plic_auto_int_in_243),
    .auto_int_in_244(plic_auto_int_in_244),
    .auto_int_in_245(plic_auto_int_in_245),
    .auto_int_in_246(plic_auto_int_in_246),
    .auto_int_in_247(plic_auto_int_in_247),
    .auto_int_in_248(plic_auto_int_in_248),
    .auto_int_in_249(plic_auto_int_in_249),
    .auto_int_in_250(plic_auto_int_in_250),
    .auto_int_in_251(plic_auto_int_in_251),
    .auto_int_in_252(plic_auto_int_in_252),
    .auto_int_in_253(plic_auto_int_in_253),
    .auto_int_in_254(plic_auto_int_in_254),
    .auto_int_in_255(plic_auto_int_in_255),
    .auto_int_out_1_0(plic_auto_int_out_1_0),
    .auto_int_out_1_1(plic_auto_int_out_1_1),
    .auto_int_out_0_0(plic_auto_int_out_0_0),
    .auto_int_out_0_1(plic_auto_int_out_0_1),
    .auto_in_a_ready(plic_auto_in_a_ready),
    .auto_in_a_valid(plic_auto_in_a_valid),
    .auto_in_a_bits_opcode(plic_auto_in_a_bits_opcode),
    .auto_in_a_bits_param(plic_auto_in_a_bits_param),
    .auto_in_a_bits_size(plic_auto_in_a_bits_size),
    .auto_in_a_bits_source(plic_auto_in_a_bits_source),
    .auto_in_a_bits_address(plic_auto_in_a_bits_address),
    .auto_in_a_bits_mask(plic_auto_in_a_bits_mask),
    .auto_in_a_bits_data(plic_auto_in_a_bits_data),
    .auto_in_a_bits_corrupt(plic_auto_in_a_bits_corrupt),
    .auto_in_d_ready(plic_auto_in_d_ready),
    .auto_in_d_valid(plic_auto_in_d_valid),
    .auto_in_d_bits_opcode(plic_auto_in_d_bits_opcode),
    .auto_in_d_bits_size(plic_auto_in_d_bits_size),
    .auto_in_d_bits_source(plic_auto_in_d_bits_source),
    .auto_in_d_bits_data(plic_auto_in_d_bits_data)
  );
  assign plic_rf_reset = rf_reset;
  assign auto_plic_int_out_1_0 = plic_auto_int_out_1_0; // @[LazyModule.scala 390:12]
  assign auto_plic_int_out_1_1 = plic_auto_int_out_1_1; // @[LazyModule.scala 390:12]
  assign auto_plic_int_out_0_0 = plic_auto_int_out_0_0; // @[LazyModule.scala 390:12]
  assign auto_plic_int_out_0_1 = plic_auto_int_out_0_1; // @[LazyModule.scala 390:12]
  assign auto_plic_in_a_ready = plic_auto_in_a_ready; // @[LazyModule.scala 388:16]
  assign auto_plic_in_d_valid = plic_auto_in_d_valid; // @[LazyModule.scala 388:16]
  assign auto_plic_in_d_bits_opcode = plic_auto_in_d_bits_opcode; // @[LazyModule.scala 388:16]
  assign auto_plic_in_d_bits_size = plic_auto_in_d_bits_size; // @[LazyModule.scala 388:16]
  assign auto_plic_in_d_bits_source = plic_auto_in_d_bits_source; // @[LazyModule.scala 388:16]
  assign auto_plic_in_d_bits_data = plic_auto_in_d_bits_data; // @[LazyModule.scala 388:16]
  assign plic_clock = auto_clock_in_clock; // @[ClockNodes.scala 22:103 LazyModule.scala 388:16]
  assign plic_reset = auto_clock_in_reset; // @[ClockNodes.scala 22:103 LazyModule.scala 388:16]
  assign plic_auto_int_in_0 = auto_plic_int_in_0; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_1 = auto_plic_int_in_1; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_2 = auto_plic_int_in_2; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_3 = auto_plic_int_in_3; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_4 = auto_plic_int_in_4; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_5 = auto_plic_int_in_5; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_6 = auto_plic_int_in_6; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_7 = auto_plic_int_in_7; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_8 = auto_plic_int_in_8; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_9 = auto_plic_int_in_9; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_10 = auto_plic_int_in_10; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_11 = auto_plic_int_in_11; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_12 = auto_plic_int_in_12; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_13 = auto_plic_int_in_13; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_14 = auto_plic_int_in_14; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_15 = auto_plic_int_in_15; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_16 = auto_plic_int_in_16; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_17 = auto_plic_int_in_17; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_18 = auto_plic_int_in_18; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_19 = auto_plic_int_in_19; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_20 = auto_plic_int_in_20; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_21 = auto_plic_int_in_21; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_22 = auto_plic_int_in_22; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_23 = auto_plic_int_in_23; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_24 = auto_plic_int_in_24; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_25 = auto_plic_int_in_25; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_26 = auto_plic_int_in_26; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_27 = auto_plic_int_in_27; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_28 = auto_plic_int_in_28; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_29 = auto_plic_int_in_29; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_30 = auto_plic_int_in_30; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_31 = auto_plic_int_in_31; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_32 = auto_plic_int_in_32; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_33 = auto_plic_int_in_33; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_34 = auto_plic_int_in_34; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_35 = auto_plic_int_in_35; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_36 = auto_plic_int_in_36; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_37 = auto_plic_int_in_37; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_38 = auto_plic_int_in_38; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_39 = auto_plic_int_in_39; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_40 = auto_plic_int_in_40; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_41 = auto_plic_int_in_41; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_42 = auto_plic_int_in_42; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_43 = auto_plic_int_in_43; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_44 = auto_plic_int_in_44; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_45 = auto_plic_int_in_45; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_46 = auto_plic_int_in_46; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_47 = auto_plic_int_in_47; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_48 = auto_plic_int_in_48; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_49 = auto_plic_int_in_49; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_50 = auto_plic_int_in_50; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_51 = auto_plic_int_in_51; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_52 = auto_plic_int_in_52; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_53 = auto_plic_int_in_53; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_54 = auto_plic_int_in_54; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_55 = auto_plic_int_in_55; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_56 = auto_plic_int_in_56; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_57 = auto_plic_int_in_57; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_58 = auto_plic_int_in_58; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_59 = auto_plic_int_in_59; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_60 = auto_plic_int_in_60; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_61 = auto_plic_int_in_61; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_62 = auto_plic_int_in_62; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_63 = auto_plic_int_in_63; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_64 = auto_plic_int_in_64; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_65 = auto_plic_int_in_65; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_66 = auto_plic_int_in_66; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_67 = auto_plic_int_in_67; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_68 = auto_plic_int_in_68; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_69 = auto_plic_int_in_69; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_70 = auto_plic_int_in_70; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_71 = auto_plic_int_in_71; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_72 = auto_plic_int_in_72; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_73 = auto_plic_int_in_73; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_74 = auto_plic_int_in_74; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_75 = auto_plic_int_in_75; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_76 = auto_plic_int_in_76; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_77 = auto_plic_int_in_77; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_78 = auto_plic_int_in_78; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_79 = auto_plic_int_in_79; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_80 = auto_plic_int_in_80; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_81 = auto_plic_int_in_81; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_82 = auto_plic_int_in_82; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_83 = auto_plic_int_in_83; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_84 = auto_plic_int_in_84; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_85 = auto_plic_int_in_85; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_86 = auto_plic_int_in_86; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_87 = auto_plic_int_in_87; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_88 = auto_plic_int_in_88; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_89 = auto_plic_int_in_89; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_90 = auto_plic_int_in_90; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_91 = auto_plic_int_in_91; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_92 = auto_plic_int_in_92; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_93 = auto_plic_int_in_93; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_94 = auto_plic_int_in_94; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_95 = auto_plic_int_in_95; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_96 = auto_plic_int_in_96; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_97 = auto_plic_int_in_97; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_98 = auto_plic_int_in_98; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_99 = auto_plic_int_in_99; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_100 = auto_plic_int_in_100; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_101 = auto_plic_int_in_101; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_102 = auto_plic_int_in_102; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_103 = auto_plic_int_in_103; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_104 = auto_plic_int_in_104; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_105 = auto_plic_int_in_105; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_106 = auto_plic_int_in_106; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_107 = auto_plic_int_in_107; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_108 = auto_plic_int_in_108; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_109 = auto_plic_int_in_109; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_110 = auto_plic_int_in_110; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_111 = auto_plic_int_in_111; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_112 = auto_plic_int_in_112; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_113 = auto_plic_int_in_113; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_114 = auto_plic_int_in_114; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_115 = auto_plic_int_in_115; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_116 = auto_plic_int_in_116; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_117 = auto_plic_int_in_117; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_118 = auto_plic_int_in_118; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_119 = auto_plic_int_in_119; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_120 = auto_plic_int_in_120; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_121 = auto_plic_int_in_121; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_122 = auto_plic_int_in_122; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_123 = auto_plic_int_in_123; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_124 = auto_plic_int_in_124; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_125 = auto_plic_int_in_125; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_126 = auto_plic_int_in_126; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_127 = auto_plic_int_in_127; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_128 = auto_plic_int_in_128; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_129 = auto_plic_int_in_129; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_130 = auto_plic_int_in_130; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_131 = auto_plic_int_in_131; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_132 = auto_plic_int_in_132; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_133 = auto_plic_int_in_133; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_134 = auto_plic_int_in_134; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_135 = auto_plic_int_in_135; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_136 = auto_plic_int_in_136; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_137 = auto_plic_int_in_137; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_138 = auto_plic_int_in_138; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_139 = auto_plic_int_in_139; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_140 = auto_plic_int_in_140; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_141 = auto_plic_int_in_141; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_142 = auto_plic_int_in_142; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_143 = auto_plic_int_in_143; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_144 = auto_plic_int_in_144; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_145 = auto_plic_int_in_145; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_146 = auto_plic_int_in_146; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_147 = auto_plic_int_in_147; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_148 = auto_plic_int_in_148; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_149 = auto_plic_int_in_149; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_150 = auto_plic_int_in_150; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_151 = auto_plic_int_in_151; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_152 = auto_plic_int_in_152; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_153 = auto_plic_int_in_153; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_154 = auto_plic_int_in_154; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_155 = auto_plic_int_in_155; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_156 = auto_plic_int_in_156; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_157 = auto_plic_int_in_157; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_158 = auto_plic_int_in_158; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_159 = auto_plic_int_in_159; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_160 = auto_plic_int_in_160; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_161 = auto_plic_int_in_161; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_162 = auto_plic_int_in_162; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_163 = auto_plic_int_in_163; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_164 = auto_plic_int_in_164; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_165 = auto_plic_int_in_165; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_166 = auto_plic_int_in_166; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_167 = auto_plic_int_in_167; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_168 = auto_plic_int_in_168; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_169 = auto_plic_int_in_169; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_170 = auto_plic_int_in_170; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_171 = auto_plic_int_in_171; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_172 = auto_plic_int_in_172; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_173 = auto_plic_int_in_173; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_174 = auto_plic_int_in_174; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_175 = auto_plic_int_in_175; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_176 = auto_plic_int_in_176; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_177 = auto_plic_int_in_177; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_178 = auto_plic_int_in_178; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_179 = auto_plic_int_in_179; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_180 = auto_plic_int_in_180; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_181 = auto_plic_int_in_181; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_182 = auto_plic_int_in_182; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_183 = auto_plic_int_in_183; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_184 = auto_plic_int_in_184; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_185 = auto_plic_int_in_185; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_186 = auto_plic_int_in_186; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_187 = auto_plic_int_in_187; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_188 = auto_plic_int_in_188; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_189 = auto_plic_int_in_189; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_190 = auto_plic_int_in_190; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_191 = auto_plic_int_in_191; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_192 = auto_plic_int_in_192; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_193 = auto_plic_int_in_193; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_194 = auto_plic_int_in_194; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_195 = auto_plic_int_in_195; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_196 = auto_plic_int_in_196; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_197 = auto_plic_int_in_197; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_198 = auto_plic_int_in_198; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_199 = auto_plic_int_in_199; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_200 = auto_plic_int_in_200; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_201 = auto_plic_int_in_201; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_202 = auto_plic_int_in_202; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_203 = auto_plic_int_in_203; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_204 = auto_plic_int_in_204; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_205 = auto_plic_int_in_205; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_206 = auto_plic_int_in_206; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_207 = auto_plic_int_in_207; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_208 = auto_plic_int_in_208; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_209 = auto_plic_int_in_209; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_210 = auto_plic_int_in_210; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_211 = auto_plic_int_in_211; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_212 = auto_plic_int_in_212; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_213 = auto_plic_int_in_213; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_214 = auto_plic_int_in_214; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_215 = auto_plic_int_in_215; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_216 = auto_plic_int_in_216; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_217 = auto_plic_int_in_217; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_218 = auto_plic_int_in_218; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_219 = auto_plic_int_in_219; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_220 = auto_plic_int_in_220; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_221 = auto_plic_int_in_221; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_222 = auto_plic_int_in_222; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_223 = auto_plic_int_in_223; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_224 = auto_plic_int_in_224; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_225 = auto_plic_int_in_225; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_226 = auto_plic_int_in_226; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_227 = auto_plic_int_in_227; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_228 = auto_plic_int_in_228; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_229 = auto_plic_int_in_229; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_230 = auto_plic_int_in_230; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_231 = auto_plic_int_in_231; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_232 = auto_plic_int_in_232; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_233 = auto_plic_int_in_233; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_234 = auto_plic_int_in_234; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_235 = auto_plic_int_in_235; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_236 = auto_plic_int_in_236; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_237 = auto_plic_int_in_237; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_238 = auto_plic_int_in_238; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_239 = auto_plic_int_in_239; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_240 = auto_plic_int_in_240; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_241 = auto_plic_int_in_241; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_242 = auto_plic_int_in_242; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_243 = auto_plic_int_in_243; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_244 = auto_plic_int_in_244; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_245 = auto_plic_int_in_245; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_246 = auto_plic_int_in_246; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_247 = auto_plic_int_in_247; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_248 = auto_plic_int_in_248; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_249 = auto_plic_int_in_249; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_250 = auto_plic_int_in_250; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_251 = auto_plic_int_in_251; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_252 = auto_plic_int_in_252; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_253 = auto_plic_int_in_253; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_254 = auto_plic_int_in_254; // @[LazyModule.scala 388:16]
  assign plic_auto_int_in_255 = auto_plic_int_in_255; // @[LazyModule.scala 388:16]
  assign plic_auto_in_a_valid = auto_plic_in_a_valid; // @[LazyModule.scala 388:16]
  assign plic_auto_in_a_bits_opcode = auto_plic_in_a_bits_opcode; // @[LazyModule.scala 388:16]
  assign plic_auto_in_a_bits_param = auto_plic_in_a_bits_param; // @[LazyModule.scala 388:16]
  assign plic_auto_in_a_bits_size = auto_plic_in_a_bits_size; // @[LazyModule.scala 388:16]
  assign plic_auto_in_a_bits_source = auto_plic_in_a_bits_source; // @[LazyModule.scala 388:16]
  assign plic_auto_in_a_bits_address = auto_plic_in_a_bits_address; // @[LazyModule.scala 388:16]
  assign plic_auto_in_a_bits_mask = auto_plic_in_a_bits_mask; // @[LazyModule.scala 388:16]
  assign plic_auto_in_a_bits_data = auto_plic_in_a_bits_data; // @[LazyModule.scala 388:16]
  assign plic_auto_in_a_bits_corrupt = auto_plic_in_a_bits_corrupt; // @[LazyModule.scala 388:16]
  assign plic_auto_in_d_ready = auto_plic_in_d_ready; // @[LazyModule.scala 388:16]
endmodule
