//
// Copyright (c) 2016-2020 SiFive, Inc. -- Proprietary and Confidential
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains the
// property of SiFive, Inc. The intellectual and technical concepts
// contained herein are proprietary to SiFive, Inc. and may be covered
// by U.S. and Foreign Patents, patents in process, and are protected by
// trade secret or copyright law.
//
// This work may not be copied, modified, re-published, uploaded,
// executed, or distributed in any way, in any medium, whether in whole
// or in part, without prior written permission from SiFive, Inc.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes
// information that is confidential and/or proprietary, and is a trade
// secret, of SiFive, Inc.
//
// Instance ID: ba8924b9-c3b4-4c20-bf12-7af889b5fb20, 00000000-0000-0000-0000-000000000000, 5f76b3eb-3bc8-47ab-a276-19761248a159
//VCS coverage exclude_file
module SiFive_TLB_1_assert(
  input         clock,
  input         reset,
  input         io_sfence_valid,
  input         io_sfence_bits_rs1,
  input  [38:0] io_sfence_bits_addr,
  input  [26:0] vpn
);
  wire  _T_2476; // @[TLB.scala 373:14]
  wire  _T_2478; // @[TLB.scala 373:72]
  wire  _T_2479; // @[TLB.scala 373:34]
  wire  _T_2481; // @[TLB.scala 373:13]
  wire  _T_2482; // @[TLB.scala 373:13]
  assign _T_2476 = ~io_sfence_bits_rs1; // @[TLB.scala 373:14]
  assign _T_2478 = io_sfence_bits_addr[38:12] == vpn; // @[TLB.scala 373:72]
  assign _T_2479 = _T_2476 | _T_2478; // @[TLB.scala 373:34]
  assign _T_2481 = _T_2479 | reset; // @[TLB.scala 373:13]
  assign _T_2482 = ~_T_2481; // @[TLB.scala 373:13]
  always @(posedge clock) begin
    `ifndef SYNTHESIS
    `ifdef PRINTF_COND
      if (`PRINTF_COND) begin
    `endif
        if (io_sfence_valid & _T_2482) begin
          $fwrite(32'h80000002,"Assertion Failed\n    at TLB.scala:373 assert(!io.sfence.bits.rs1 || (io.sfence.bits.addr >> pgIdxBits) === vpn)\n"); // @[TLB.scala 373:13]
        end
    `ifdef PRINTF_COND
      end
    `endif
    `endif // SYNTHESIS
    `ifndef SYNTHESIS
    `ifdef STOP_COND
      if (`STOP_COND) begin
    `endif
        if (io_sfence_valid & _T_2482) begin
          $fatal; // @[TLB.scala 373:13]
        end
    `ifdef STOP_COND
      end
    `endif
    `endif // SYNTHESIS
  end

endmodule
